BEGIN TRAN
EXEC sp_msforeachtable 'ALTER TABLE ? NOCHECK CONSTRAINT all'
EXEC sp_msforeachtable 'DROP TABLE ?'
DROP SEQUENCE IF EXISTS dbo.invoice_po_item_id_sequence
DROP SEQUENCE IF EXISTS dbo.invoiceitem_id_sequence
DROP SEQUENCE IF EXISTS dbo.quotation_item_id_sequence
create sequence dbo.invoice_po_item_id_sequence start with 1 increment by 1;
create sequence dbo.invoiceitem_id_sequence start with 1 increment by 1;
create sequence dbo.quotation_item_id_sequence start with 1 increment by 1;

    create table dbo.accessory (
       id int identity not null,
        lastModified datetime2,
        description varchar(255),
        lastModifiedBy int,
        primary key (id)
    );

    create table dbo.accessorytranslation (
       accessoryid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (accessoryid, locale, translation)
    );

    create table dbo.accreditationbody (
       id int identity not null,
        lastModified datetime2,
        shortName nvarchar(25),
        lastModifiedBy int,
        countryid int,
        primary key (id)
    );

    create table dbo.accreditationbodyresource (
       id int identity not null,
        lastModified datetime2,
        resourcePath nvarchar(2000),
        type int,
        lastModifiedBy int,
        accreditationbodyid int,
        primary key (id)
    );

    create table dbo.accreditedlab (
       id int identity not null,
        lastModified datetime2,
        description varchar(50),
        labno varchar(20),
        watermark bit default 0,
        lastModifiedBy int,
        orgid int,
        accreditationbodyid int,
        primary key (id)
    );

    create table dbo.accreditedlabcaltype (
       id int identity not null,
        labeltemplatepath varchar(100),
        accreditedlabid int,
        caltypeid int,
        primary key (id)
    );

    create table dbo.actionoutcome (
       type varchar(31),
        id int identity not null,
        active bit,
        defaultoutcome bit,
        description varchar(200),
        positiveoutcome bit,
        value varchar(255),
        activityid int,
        primary key (id)
    );

    create table dbo.actionoutcometranslation (
       id int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (id, locale, translation)
    );

    create table dbo.additionalcostcontact (
       id int identity not null,
        lastModified datetime2,
        lastModifiedBy int,
        personid int,
        costingid int,
        primary key (id)
    );

    create table dbo.address (
       addrid int identity not null,
        lastModified datetime2,
        active tinyint,
        addr1 varchar(100),
        addr2 varchar(100),
        addr3 varchar(100),
        costcentre varchar(20),
        county varchar(30),
        deactreason varchar(255),
        deacttime datetime2,
        fax varchar(20),
        latitude numeric(9,6),
        longitude numeric(9,6),
        postcode varchar(10),
        telephone varchar(30),
        town varchar(255),
        validatedon datetime2,
        validator int,
        lastModifiedBy int,
        countryid int,
        deflabelprinterid int,
        defprinterid int,
        subdivid int,
        validatedby int,
        primary key (addrid)
    );

    create table dbo.addressfunction (
       addressid int,
        typeid int
    );

    create table dbo.addressplantillassettings (
       id int identity not null,
        lastModified datetime2,
        contract bit default 0,
        formerCustomerId int default 0,
        ftpPassword varchar(100),
        ftpServer varchar(100),
        ftpUser varchar(100),
        metraClient bit default 0,
        nextDateOnCertificate bit default 0,
        sendByFtp bit default 0,
        textToleranceFailure varchar(100),
        textUncertaintyFailure varchar(100),
        lastModifiedBy int,
        orgid int,
        addressId int,
        certificateAddressId int,
        managedByAddressId int,
        primary key (id)
    );

    create table dbo.addresssettingsforallocatedcompany (
       id int identity not null,
        lastModified datetime2,
        lastModifiedBy int,
        orgid int,
        addressid int,
        vatrateid varchar(1),
        primary key (id)
    );

    create table dbo.addresssettingsforallocatedsubdiv (
       id int identity not null,
        lastModified datetime2,
        lastModifiedBy int,
        orgid int,
        addressid int,
        transportinid int,
        transportoutid int,
        primary key (id)
    );

    create table dbo.advesojobItemactivityqueue (
       id int identity not null,
        lastModified datetime2,
        createdon datetime2,
        isReceived bit,
        isSent bit,
        isTreated bit,
        jobcostingfilename varchar(255),
        lastErrorMessage varchar(255),
        lastErrorMessageOn datetime2,
        linkedentityid int,
        linkedentitytype varchar(255),
        modifiedOn datetime2,
        receivedOn datetime2,
        sentOn datetime2,
        treatedOn datetime2,
        lastModifiedBy int,
        jobitemactionid int,
        primary key (id)
    );

    create table dbo.advesooverriddenactivitysetting (
       id int identity not null,
        lastModified datetime2,
        isDisplayedOnJobOrderDetails bit,
        isTransferredToAdveso bit,
        lastModifiedBy int,
        advesotransferableactivityid int,
        subdivid int,
        primary key (id)
    );

    create table dbo.advesotransferableactivity (
       id int identity not null,
        lastModified datetime2,
        isDisplayedOnJobOrderDetails bit,
        isTransferredToAdveso bit,
        lastModifiedBy int,
        itemactivityid int,
        primary key (id)
    );

    create table dbo.aliasboolean (
       id int identity not null,
        alias varchar(255),
        value bit,
        aliasgroupid int,
        primary key (id)
    );

    create table dbo.aliasgroup (
       id int identity not null,
        lastModified datetime2,
        createdOn datetime2,
        defaultBusinessCompany bit,
        description varchar(255),
        name varchar(255),
        lastModifiedBy int,
        orgid int,
        createdby int,
        primary key (id)
    );

    create table dbo.aliasintervalunit (
       id int identity not null,
        alias varchar(255),
        value varchar(255),
        aliasgroupid int,
        primary key (id)
    );

    create table dbo.aliasservicetype (
       id int identity not null,
        alias varchar(255),
        aliasgroupid int,
        servicetypeid int,
        primary key (id)
    );

    create table dbo.alligatorlabelmedia (
       id int identity not null,
        gap numeric(15,5),
        height numeric(15,5),
        name varchar(50),
        paddingbottom numeric(15,5),
        paddingleft numeric(15,5),
        paddingright numeric(15,5),
        paddingtop numeric(15,5),
        rotationangle numeric(15,5),
        width numeric(15,5),
        primary key (id)
    );

    create table dbo.alligatorlabelparameter (
       id int identity not null,
        format varchar(30),
        name varchar(30),
        text varchar(30),
        value varchar(255),
        templateid int,
        primary key (id)
    );

    create table dbo.alligatorlabeltemplate (
       id int identity not null,
        description varchar(100),
        parameterstyle varchar(255),
        templatetype varchar(255),
        templatexml varchar(max),
        primary key (id)
    );

    create table dbo.alligatorlabeltemplaterule (
       id int identity not null,
        coid int,
        templateid int,
        primary key (id)
    );

    create table dbo.alligatorsettings (
       id int identity not null,
        description varchar(100),
        excellocaltempfolder varchar(255),
        excelrelativetempfolder bit,
        excelsavetempcopy bit,
        globaldefault bit,
        uploadenabled bit,
        uploadmoveuponupload bit,
        uploadrawdatafolder varchar(255),
        uploadtempdatafolder varchar(255),
        uploadusewindowscredentials bit,
        uploadwindowsdomain varchar(255),
        uploadwindowspassword varchar(255),
        uploadwindowsusername varchar(255),
        primary key (id)
    );

    create table dbo.alligatorsettingscontact (
       id int identity not null,
        productionsettingsid int,
        testsettingsid int,
        personid int,
        primary key (id)
    );

    create table dbo.alligatorsettingssubdiv (
       id int identity not null,
        productionsettingsid int,
        testsettingsid int,
        subdivid int,
        primary key (id)
    );

    create table dbo.asn (
       id int identity not null,
        lastModified datetime2,
        createdOn datetime2,
        itemsFromJson varchar(255),
        jobtype varchar(255),
        sourceapi varchar(255),
        lastModifiedBy int,
        createdBy int,
        exchangeformatid int,
        exchangeformatfileid int,
        jobid int,
        primary key (id)
    );

    create table dbo.asnitem (
       id int identity not null,
        lastModified datetime2,
        clientref varchar(255),
        comment varchar(255),
        itemindex int,
        ponumber varchar(255),
        status varchar(255),
        lastModifiedBy int,
        analysisresultid int,
        asnid int,
        instrumentid int,
        servicetypeid int,
        primary key (id)
    );

    create table dbo.asnitemanalysisresult (
       id int identity not null,
        lastModified datetime2,
        comment varchar(255),
        nextaction varchar(255),
        lastModifiedBy int,
        jobitemid int,
        primary key (id)
    );

    create table dbo.asset (
       assettype varchar(31),
        id int identity not null,
        lastModified datetime2,
        assetno varchar(30),
        editableaddedon date,
        assetname varchar(150),
        price numeric(10,2),
        purchasedon date,
        qty int,
        sysaddedon date,
        ipaddress varchar(20),
        plantno varchar(50),
        serialno varchar(50),
        networkloc varchar(100),
        lastModifiedBy int,
        custodianid int,
        deptid int,
        ownerid int,
        primaryuserid int,
        statusid int,
        sysaddedbyid int,
        typeid int,
        plantid int,
        physicallocid int,
        supplierid int,
        primary key (id)
    );

    create table dbo.assetnote (
       noteid int identity not null,
        lastModified datetime2,
        active tinyint,
        deactivatedon datetime2,
        label varchar(50),
        note nvarchar(2000),
        publish tinyint,
        seton datetime2,
        lastModifiedBy int,
        deactivatedby int,
        setby int,
        assetid int,
        primary key (noteid)
    );

    create table dbo.assetstatus (
       id int identity not null,
        active bit,
        name varchar(200),
        primary key (id)
    );

    create table dbo.assetstatustranslation (
       id int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (id, locale, translation)
    );

    create table dbo.assettype (
       id int identity not null,
        description varchar(200),
        insttype bit,
        name varchar(100),
        primary key (id)
    );

    create table dbo.assettypetranslation (
       id int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (id, locale, translation)
    );

    create table dbo.audittriggerswitch (
       id int identity not null,
        enabled bit,
        primary key (id)
    );

    create table dbo.avalaraconfig (
       coid int,
        accountid int,
        avalaracode varchar(255),
        avalaraid int,
        environment varchar(255),
        licensekey varchar(255),
        primary key (coid)
    );

    create table dbo.backgroundtask (
       id int identity not null,
        lastModified datetime2,
        autosubmitpolicy varchar(255),
        notified bit,
        submitedon datetime2,
        lastModifiedBy int,
        orgid int,
        clientsubdivid int,
        exchangeformatid int,
        exchangeformatfileid int,
        jobinstanceid int,
        notifiee int,
        submitedby int,
        primary key (id)
    );

    create table dbo.bankaccount (
       id int identity not null,
        lastModified datetime2,
        accountno varchar(50),
        bankname varchar(100),
        iban varchar(34),
        includeOnFactoredInvoices bit,
        includeOnNonFactoredInvoices bit,
        swiftbic varchar(11),
        lastModifiedBy int,
        companyid int,
        currencyid int,
        primary key (id)
    );

    create table dbo.baseinstruction (
       instructionid int identity not null,
        lastModified datetime2,
        includeondelnote bit,
        includeonsupplierdelnote bit,
        instruction varchar(1000),
        instructiontypeid int,
        lastModifiedBy int,
        primary key (instructionid)
    );

    create table dbo.basestatus (
       type varchar(31),
        statusid int identity not null,
        defaultstatus bit,
        description varchar(200),
        name varchar(100),
        accepted bit,
        followingissue bit,
        followingreceipt bit,
        issued bit,
        rejected bit,
        requiringattention bit,
        primary key (statusid)
    );

    create table dbo.basestatusdescriptiontranslation (
       statusid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (statusid, locale, translation)
    );

    create table dbo.basestatusnametranslation (
       statusid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (statusid, locale, translation)
    );

    create table dbo.BATCH_JOB_EXECUTION (
       JOB_EXECUTION_ID int,
        CREATE_TIME datetime2,
        END_TIME datetime2,
        EXIT_CODE varchar(255),
        EXIT_MESSAGE varchar(255),
        JOB_CONFIGURATION_LOCATION varchar(255),
        LAST_UPDATED datetime2,
        START_TIME datetime2,
        STATUS varchar(255),
        VERSION bigint,
        JOB_INSTANCE_ID int,
        primary key (JOB_EXECUTION_ID)
    );

    create table dbo.BATCH_JOB_EXECUTION_CONTEXT (
       JOB_EXECUTION_ID bigint,
        SERIALIZED_CONTEXT varchar(255),
        SHORT_CONTEXT varchar(255),
        primary key (JOB_EXECUTION_ID)
    );

    create table dbo.BATCH_JOB_EXECUTION_PARAMS (
       JOB_EXECUTION_ID bigint,
        DATE_VAL datetime2,
        DOUBLE_VAL double precision,
        KEY_NAME varchar(255),
        LONG_VAL bigint,
        STRING_VAL varchar(255),
        TYPE_CD varchar(255),
        IDENTIFYING char(1),
        primary key (JOB_EXECUTION_ID, DATE_VAL, DOUBLE_VAL, KEY_NAME, LONG_VAL, STRING_VAL, TYPE_CD)
    );

    create table dbo.BATCH_JOB_EXECUTION_SEQ (
       ID bigint identity not null,
        primary key (ID)
    );

    create table dbo.BATCH_JOB_INSTANCE (
       JOB_INSTANCE_ID int,
        JOB_KEY varchar(255),
        JOB_NAME varchar(255),
        VERSION bigint,
        primary key (JOB_INSTANCE_ID)
    );

    create table dbo.BATCH_JOB_SEQ (
       ID bigint identity not null,
        primary key (ID)
    );

    create table dbo.BATCH_STEP_EXECUTION (
       STEP_EXECUTION_ID int,
        COMMIT_COUNT int,
        END_TIME datetime2,
        EXIT_CODE varchar(255),
        EXIT_MESSAGE varchar(2500),
        FILTER_COUNT int,
        LAST_UPDATED datetime2,
        PROCESS_SKIP_COUNT int,
        READ_COUNT int,
        READ_SKIP_COUNT int,
        ROLLBACK_COUNT int,
        START_TIME datetime2,
        STATUS varchar(255),
        STEP_NAME varchar(255),
        VERSION bigint,
        WRITE_COUNT int,
        WRITE_SKIP_COUNT int,
        JOB_EXECUTION_ID int,
        primary key (STEP_EXECUTION_ID)
    );

    create table dbo.BATCH_STEP_EXECUTION_CONTEXT (
       STEP_EXECUTION_ID bigint,
        SERIALIZED_CONTEXT text,
        SHORT_CONTEXT varchar(2500),
        primary key (STEP_EXECUTION_ID)
    );

    create table dbo.BATCH_STEP_EXECUTION_SEQ (
       ID bigint identity not null,
        primary key (ID)
    );

    create table dbo.batchcalibration (
       id int identity not null,
        lastModified datetime2,
        begintime datetime2,
        caldate datetime,
        choosenext bit,
        lastcertify datetime,
        lastdespatch datetime,
        lastinvoice datetime,
        lastlabels datetime,
        passwordaccepted bit,
        lastModifiedBy int,
        jobid int,
        ownerid int,
        processid int,
        primary key (id)
    );

    create table dbo.bpo (
       poid int identity not null,
        lastModified datetime2,
        comment varchar(1000),
        ponumber varchar(60),
        active bit,
        durationfrom datetime,
        durationto datetime,
        expirwarningdate datetime,
        expirwarningsent bit,
        limitamount numeric(10,2),
        lastModifiedBy int,
        orgid int,
        coid int,
        personid int,
        subdivid int,
        primary key (poid)
    );

    create table dbo.businessarea (
       businessareaid int identity not null,
        lastModified datetime2,
        description varchar(250),
        name varchar(60),
        lastModifiedBy int,
        primary key (businessareaid)
    );

    create table dbo.businessareadescriptiontranslation (
       businessareaid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (businessareaid, locale, translation)
    );

    create table dbo.businessareanametranslation (
       businessareaid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (businessareaid, locale, translation)
    );

    create table dbo.businesscompanysettings (
       id int identity not null,
        lastModified datetime2,
        capital varchar(30),
        defaultquotationduration int,
        hourlyrate numeric(10,2),
        interbranchdiscountrate numeric(5,2),
        interbranchmarkuprate numeric(5,2),
        interbranchsalesmode varchar(255),
        legalRegistration1 varchar(100),
        legalRegistration2 varchar(100),
        logo varchar(255),
        usePriceCatalog bit default 1,
        useTaxableOption bit,
        usesPlantillas bit default 0,
        warrantyCalibration int,
        warrantyRepaire int,
        lastModifiedBy int,
        companyid int,
        primary key (id)
    );

    create table dbo.businessdocumentsettings (
       id int identity not null,
        deliveryNoteSignedCertificates bit,
        globalDefault bit,
        invoiceAmountText bit,
        recipientAddressOnLeft bit,
        subdivisionFiscalIdentifier bit,
        businesscompanyid int,
        primary key (id)
    );

    create table dbo.businessemails (
       id int identity not null,
        lastModified datetime2,
        account varchar(255),
        autoService varchar(255),
        collections varchar(255),
        goodsIn varchar(255),
        quality varchar(255),
        quotations varchar(255),
        recall varchar(255),
        sales varchar(255),
        web varchar(255),
        lastModifiedBy int,
        orgid int,
        primary key (id)
    );

    create table dbo.businesssubdivisionsettings (
       id int identity not null,
        lastModified datetime2,
        defaultturnaround int,
        ebitdahourlycostrate numeric(10,2),
        workinstructionreference bit,
        lastModifiedBy int,
        defaultbusinesscontact int,
        subdivid int,
        primary key (id)
    );

    create table dbo.calibration (
       id int identity not null,
        lastModified datetime2,
        batchposition int,
        calclass varchar(255),
        caldate date,
        certtemplate varchar(100),
        completetime datetime2,
        starttime datetime2,
        xindicekey varchar(100),
        lastModifiedBy int,
        orgid int,
        batchid int,
        caladdressid int,
        calprocessid int,
        caltypeid int,
        capabilityId int,
        completedby int,
        startedby int,
        calibrationstatus int,
        workInstructionId int,
        primary key (id)
    );

    create table dbo.calibrationaccreditationlevel (
       id int identity not null,
        accredlevel varchar(255),
        caltypeid int,
        primary key (id)
    );

    create table dbo.calibrationactivitylink (
       id int identity not null,
        calid int,
        jobitemactivityid int,
        primary key (id)
    );

    create table dbo.calibrationpoint (
       id int identity not null,
        lastModified datetime2,
        point numeric(19,4),
        relatedpoint numeric(19,4),
        lastModifiedBy int,
        relateduomid int,
        uomid int,
        psid int,
        primary key (id)
    );

    create table dbo.calibrationpointset (
       id int identity not null,
        lastModified datetime2,
        lastModifiedBy int,
        primary key (id)
    );

    create table dbo.calibrationprocess (
       id int identity not null,
        actionafterissue bit,
        applicationcomponent bit,
        description varchar(200),
        fileextension varchar(6),
        issueimmediately bit,
        process varchar(100),
        quicksign bit,
        signonissue bit,
        signingsupported bit,
        toolcontinuelink varchar(255),
        toollink varchar(255),
        uploadafterissue bit,
        uploadbeforeissue bit,
        primary key (id)
    );

    create table dbo.calibrationrange (
       id int identity not null,
        lastModified datetime2,
        endd double precision,
        maxIsInfinite bit default 0,
        maxvalue nvarchar(2000),
        minvalue nvarchar(2000),
        start double precision,
        relatedpoint numeric(19,2),
        lastModifiedBy int,
        maxuomid int,
        uomid int,
        relateduomid int,
        primary key (id)
    );

    create table dbo.calibrationtype (
       caltypeid int identity not null,
        accreditationspecific bit,
        active bit,
        calibrationWithJudgement bit,
        orderby int,
        recallRequirementType varchar(255),
        servicetypeid int,
        primary key (caltypeid)
    );

    create table dbo.callink (
       id int identity not null,
        lastModified datetime2,
        certissued bit,
        mainitem bit,
        timespent int,
        lastModifiedBy int,
        calid int,
        jobitemid int,
        actionoutcomeid int,
        primary key (id)
    );

    create table dbo.calloffitem (
       id int identity not null,
        lastModified datetime2,
        active bit,
        offdate datetime,
        ondate datetime,
        reason varchar(1000),
        lastModifiedBy int,
        orgid int,
        calledoffby int,
        offitem int,
        onitem int,
        primary key (id)
    );

    create table dbo.calreq (
       type varchar(31),
        id int identity not null,
        lastModified datetime2,
        active tinyint,
        deactivatedon datetime,
        instructions nvarchar(2000),
        publicinstructions nvarchar(2000),
        publish bit,
        lastModifiedBy int,
        deactivatedbyid int,
        pointsetid int,
        calibrationrangeid int,
        jobitemid int,
        compid int,
        modelid int,
        descid int,
        plantid int,
        primary key (id)
    );

    create table dbo.calreqhistory (
       type varchar(31),
        id int identity not null,
        changeon datetime2,
        changebyid int,
        newcalreqid int,
        oldcalreqid int,
        fallfrominst int,
        fallfromji int,
        overrideoninst int,
        overrideonji int,
        primary key (id)
    );

    create table dbo.capability (
       id int identity not null,
        lastModified datetime2,
        active tinyint,
        deactivatedon date,
        description nvarchar(2000),
        estimatedTime int,
        name varchar(200),
        reference varchar(20),
        reqtype varchar(255),
        lastModifiedBy int,
        orgid int,
        calprocesid int,
        deactivatedby int,
        defaddressid int,
        deflocationid int,
        defworkinstructionid int,
        headingid int,
        accreditedlabid int,
        subfamilyid int,
        primary key (id)
    );

    create table dbo.capability_authorization (
       id int identity not null,
        lastModified datetime2,
        accredlevel varchar(255),
        awarded date,
        lastModifiedBy int,
        authorizationFor int,
        awardedby int,
        caltypeid int,
        capabilityId int,
        primary key (id)
    );

    create table dbo.capability_filter (
       id int identity not null,
        lastModified datetime2,
        active bit,
        bestlab bit default 0,
        externalComments nvarchar(2000),
        filtertype varchar(255),
        internalComments nvarchar(2000),
        standardTime int,
        lastModifiedBy int,
        capabilityId int,
        defworkinstructionid int,
        servicetypeid int,
        primary key (id)
    );

    create table dbo.capability_trainingrecord (
       id int identity not null,
        capabilityId int,
        certid int,
        labmanager int,
        trainee int,
        primary key (id)
    );

    create table dbo.capabilitycategory (
       id int identity not null,
        departmentdefault bit,
        description varchar(200),
        name varchar(100),
        splitTraceable bit,
        departmentid int,
        primary key (id)
    );

    create table dbo.catalogprice (
       id int identity not null,
        lastModified datetime2,
        comments varchar(2000),
        costtypeid int,
        fixedPrice numeric(19,2),
        standardTime bigint,
        unit int,
        unitPrice numeric(19,2),
        lastModifiedBy int,
        orgid int,
        modelid int,
        servicetype int,
        primary key (id)
    );

    create table dbo.categorised_capability (
       id int identity not null,
        capabilityId int,
        categoryid int,
        primary key (id)
    );

    create table dbo.certaction (
       id int identity not null,
        action varchar(255),
        actionon datetime2,
        actionbyid int,
        certid int,
        primary key (id)
    );

    create table dbo.certificate (
       certid int identity not null,
        lastModified datetime2,
        adjustment bit,
        caldate datetime,
        calibrationverificationstatus varchar(255),
        certclass varchar(255),
        certdate datetime,
        certpreviewed bit,
        certno varchar(60),
        deviation numeric(15,5),
        doctemplatekey varchar(100),
        duration int,
        optimization bit,
        proctemplatekey varchar(100),
        remarks varchar(1000),
        repair bit,
        restriction bit,
        certstatus varchar(255),
        supplementarycomments varchar(255),
        thirdcertno varchar(30),
        type varchar(255),
        intervalunit varchar(255),
        lastModifiedBy int,
        approvedby int,
        calid int,
        caltype int,
        registeredby int,
        servicetypeid int,
        signedby int,
        supplementaryforid int,
        thirddiv int,
        thirdoutcome int,
        primary key (certid)
    );

    create table dbo.certificatevalidation (
       id int identity not null,
        lastModified datetime2,
        datevalidated date,
        document varchar(200),
        note varchar(1000),
        responsible varchar(100),
        validationstatus varchar(255),
        lastModifiedBy int,
        certid int,
        personid int,
        primary key (id)
    );

    create table dbo.certlink (
       linkid int identity not null,
        lastModified datetime2,
        lastModifiedBy int,
        certid int,
        jobitemid int,
        primary key (linkid)
    );

    create table dbo.certnote (
       noteid int identity not null,
        lastModified datetime2,
        active tinyint,
        deactivatedon datetime2,
        label varchar(50),
        note nvarchar(2000),
        publish tinyint,
        seton datetime2,
        lastModifiedBy int,
        deactivatedby int,
        setby int,
        certid int,
        primary key (noteid)
    );

    create table dbo.change_queue (
       id int identity not null,
        changetype int,
        entitytype int,
        payload varchar(max),
        processed bit,
        primary key (id)
    );

    create table dbo.characteristicdescription (
       characteristicdescriptionid int identity not null,
        lastModified datetime2,
        active bit,
        characteristictype int,
        internalname varchar(255),
        log_createdon datetime2,
        metadata xml,
        name varchar(255),
        orderno int,
        partofkey bit,
        required bit,
        shortname varchar(255),
        tmlid int,
        lastModifiedBy int,
        descriptionid int,
        uomid int,
        primary key (characteristicdescriptionid)
    );

    create table dbo.characteristicdescriptiontranslation (
       characteristicdescriptionid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (characteristicdescriptionid, locale, translation)
    );

    create table dbo.characteristiclibrary (
       characteristiclibraryid int identity not null,
        active bit,
        log_createdon datetime2,
        name nvarchar(2000),
        tmlid int,
        characteristicdescriptionid int,
        primary key (characteristiclibraryid)
    );

    create table dbo.characteristiclibrarytranslation (
       characteristiclibraryid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (characteristiclibraryid, locale, translation)
    );

    create table dbo.characteristicshortnametranslation (
       characteristicdescriptionid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (characteristicdescriptionid, locale, translation)
    );

    create table dbo.checkout (
       id int identity not null,
        checkInDate datetime2,
        checkOutComplet bit,
        checkOutDate datetime2,
        plantid int,
        jobid int,
        techid int,
        primary key (id)
    );

    create table dbo.checkout_confidencecheck (
       confidencecheckid int,
        checkoutid int
    );

    create table dbo.closeddate (
       id int identity not null,
        closeddate date,
        reason varchar(100),
        coid int,
        primary key (id)
    );

    create table dbo.collectedinstrument (
       id int identity not null,
        lastModified datetime2,
        collectstamp datetime2,
        numitems int,
        lastModifiedBy int,
        collectedby int,
        plantid int,
        modelid int,
        scheduleid int,
        primary key (id)
    );

    create table dbo.company (
       coid int identity not null,
        lastModified datetime2,
        companycode varchar(3),
        corole int,
        coname varchar(100),
        createdate date,
        documentlanguage varchar(255),
        fiscalidentifier varchar(255),
        formerid varchar(50),
        groupname varchar(255),
        legalidentifier varchar(255),
        partnercomp bit,
        rate numeric(10,2),
        lastModifiedBy int,
        businesssettingsid int,
        businessareaid int,
        companygroupid int,
        countryid int,
        currencyid int,
        defaultbusinesscontact int,
        defsubdivid int,
        legaladdressid int,
        primary key (coid)
    );

    create table dbo.companyaccreditation (
       id int identity not null,
        lastModified datetime2,
        emailalert bit,
        expirydate date,
        lastemailalert date,
        reason varchar(100),
        lastModifiedBy int,
        orgid int,
        coid int,
        supplierid int,
        primary key (id)
    );

    create table dbo.companygroup (
       id int identity not null,
        lastModified datetime2,
        active bit,
        groupname varchar(75),
        lastModifiedBy int,
        primary key (id)
    );

    create table dbo.companyhistory (
       companyhistoryid int identity not null,
        coname varchar(60),
        validto datetime2,
        changedbyid int,
        coid int,
        primary key (companyhistoryid)
    );

    create table dbo.companyinstructionlink (
       id int identity not null,
        lastModified datetime2,
        issubdivinstruction bit,
        lastModifiedBy int,
        orgid int,
        businesssubdivid int,
        coid int,
        instructionid int,
        primary key (id)
    );

    create table dbo.companylist (
       id int identity not null,
        lastModified datetime2,
        active bit,
        name varchar(50),
        lastModifiedBy int,
        orgid int,
        primary key (id)
    );

    create table dbo.companylistmember (
       id int identity not null,
        lastModified datetime2,
        active bit,
        lastModifiedBy int,
        coid int,
        companylistid int,
        primary key (id)
    );

    create table dbo.companylistusage (
       id int identity not null,
        lastModified datetime2,
        lastModifiedBy int,
        coid int,
        companylistid int,
        primary key (id)
    );

    create table dbo.companysettingsforallocatedcompany (
       id int identity not null,
        lastModified datetime2,
        active bit,
        companyreference varchar(255),
        contract bit default 0,
        creditlimit numeric(10,2),
        deactDate datetime2,
        deactReason varchar(255),
        invoiceFactoring bit,
        onstop bit,
        onstopreason varchar(255),
        onstopsince date,
        paymentterms varchar(255),
        porequired bit,
        requiresuppliercompanylist bit default 0,
        softwareaccountnumber varchar(50),
        status varchar(255),
        syncToPlantillas bit default 0,
        taxable bit,
        lastModifiedBy int,
        orgid int,
        bankaccountid int,
        companyid int,
        invoicefrequencyid int,
        onstopby int,
        paymentmodeid int,
        vatrate varchar(1),
        primary key (id)
    );

    create table dbo.component (
       id int identity not null,
        lastModified datetime2,
        instock int,
        minstocklevel int,
        salescost numeric(10,2),
        stocklastchecked date,
        lastModifiedBy int,
        stocklastcheckedby int,
        descriptionid int,
        mfrid int,
        primary key (id)
    );

    create table dbo.componentdescription (
       descriptionid int identity not null,
        description varchar(100),
        primary key (descriptionid)
    );

    create table dbo.componentdoctype (
       id int identity not null,
        componentId int,
        doctypeId int,
        primary key (id)
    );

    create table dbo.componentdoctypetemplate (
       id int,
        defaulttemplate bit,
        compdoctypeid int,
        templateId int,
        primary key (id)
    );

    create table dbo.componentinventory (
       id int identity not null,
        lastModified datetime2,
        lastModifiedBy int,
        businessSubdivId int,
        componentid int,
        suppliercompanyid int,
        primary key (id)
    );

    create table dbo.confidencecheck (
       id int identity not null,
        description varchar(255),
        performedOn datetime2,
        result varchar(10),
        techid int,
        primary key (id)
    );

    create table dbo.contact (
       personid int identity not null,
        lastModified datetime2,
        active tinyint,
        createdate date,
        creditrating varchar(1),
        deactreason varchar(255),
        deacttime datetime,
        email varchar(200),
        encryptedpassword varchar(255),
        fax varchar(20),
        firstname varchar(30),
        hrid varchar(255),
        lastactivity datetime,
        lastname varchar(30),
        locale varchar(255),
        mobile varchar(20),
        position varchar(40),
        preference varchar(1),
        salescomm varchar(20),
        statusmessage varchar(200),
        telephone varchar(30),
        telephoneext int,
        title varchar(255),
        lastModifiedBy int,
        creditCard_ccid int,
        defaddress int,
        deflocation int,
        defprinterid int,
        discount varchar(4),
        subdivid int,
        groupid int,
        primary key (personid)
    );

    create table dbo.contactinstructionlink (
       id int identity not null,
        lastModified datetime2,
        lastModifiedBy int,
        orgid int,
        personid int,
        instructionid int,
        primary key (id)
    );

    create table dbo.contract (
       id int identity not null,
        lastModified datetime2,
        contractno varchar(255),
        dbmanagement bit,
        description varchar(255),
        duration int,
        durationunit varchar(255),
        enddate date,
        instrumentlisttype varchar(255),
        invoicefrequency int,
        invoicingmethod varchar(255),
        legalContractNo varchar(255),
        outstandingpremium numeric(19,2),
        paymentterms varchar(10),
        premium numeric(19,2),
        software varchar(255),
        startdate date,
        transfertype varchar(255),
        warrantyCalibration int,
        warrantyRepaire int,
        lastModifiedBy int,
        orgid int,
        bpoid int,
        clientcompanyid int,
        clientcontactid int,
        currencyid int,
        invoicingmanagerid int,
        paymentmodeid int,
        quotationId int,
        trescalmanagerid int,
        primary key (id)
    );

    create table dbo.contract_instructions (
       Contract_id int,
        instructions int,
        primary key (Contract_id, instructions)
    );

    create table dbo.contractdemand (
       id int identity not null,
        demandset int,
        contractid int,
        domainid int,
        subfamilyid int,
        primary key (id)
    );

    create table dbo.contractreview (
       id int identity not null,
        lastModified datetime2,
        comments varchar(200),
        demandset int,
        reviewdate datetime2,
        lastModifiedBy int,
        jobid int,
        prebookingjobdetailsid int,
        personid int,
        primary key (id)
    );

    create table dbo.contractreviewitem (
       id int identity not null,
        lastModified datetime2,
        lastModifiedBy int,
        jobitemid int,
        reviewid int,
        primary key (id)
    );

    create table dbo.contractreviewlinkedadjustmentcost (
       id int identity not null,
        adjcostid int,
        primary key (id)
    );

    create table dbo.contractreviewlinkedcalibrationcost (
       id int identity not null,
        calcostid int,
        jobcostcalcostid int,
        quotecalcostid int,
        primary key (id)
    );

    create table dbo.contractreviewlinkedpurchasecost (
       id int identity not null,
        purcostid int,
        quotepurcostid int,
        primary key (id)
    );

    create table dbo.contractreviewlinkedrepaircost (
       id int identity not null,
        repcostid int,
        primary key (id)
    );

    create table dbo.contractservicetypesettings (
       id int identity not null,
        turnaround int,
        contractid int,
        servicetypeid int,
        primary key (id)
    );

    create table dbo.costs_adjustment (
       costtype varchar(31),
        costid int identity not null,
        lastModified datetime2,
        active tinyint,
        autoset tinyint,
        costtypeid int,
        customorderby int,
        discountrate numeric(5,2),
        discountvalue numeric(10,2),
        finalcost numeric(10,2),
        hourlyrate numeric(10,2),
        labourcost numeric(10,2),
        labourtime int,
        totalcost numeric(10,2),
        fixedPrice numeric(19,2),
        costsource varchar(255),
        housecost numeric(10,2),
        tpcostsrc varchar(255),
        thirdcosttotal numeric(10,2),
        tpmanualprice numeric(10,2),
        tpmarkuprate numeric(10,2),
        tpmarkupsource varchar(255),
        tpmarkupvalue numeric(10,2),
        tpcarin numeric(10,2),
        tpcarinmarkupvalue numeric(10,2),
        tpcarmarkuprate numeric(10,2),
        tpcarmarkupsrc varchar(255),
        tpcarout numeric(10,2),
        tpcaroutmarkupvalue numeric(10,2),
        tpcartotal numeric(10,2),
        lastModifiedBy int,
        nominalid int,
        linkedadjcostid int,
        primary key (costid)
    );

    create table dbo.costs_calibration (
       costtype varchar(31),
        costid int identity not null,
        lastModified datetime2,
        active tinyint,
        autoset tinyint,
        costtypeid int,
        customorderby int,
        discountrate numeric(5,2),
        discountvalue numeric(10,2),
        finalcost numeric(10,2),
        hourlyrate numeric(10,2),
        labourcost numeric(10,2),
        labourtime int,
        totalcost numeric(10,2),
        costsource varchar(255),
        housecost numeric(10,2),
        tpcostsrc varchar(255),
        thirdcosttotal numeric(10,2),
        tpmanualprice numeric(10,2),
        tpmarkuprate numeric(10,2),
        tpmarkupsource varchar(255),
        tpmarkupvalue numeric(10,2),
        tpcarin numeric(10,2),
        tpcarinmarkupvalue numeric(10,2),
        tpcarmarkuprate numeric(10,2),
        tpcarmarkupsrc varchar(255),
        tpcarout numeric(10,2),
        tpcaroutmarkupvalue numeric(10,2),
        tpcartotal numeric(10,2),
        lastModifiedBy int,
        linkedcalcostid int,
        currency_currencyid int,
        nominalid int,
        primary key (costid)
    );

    create table dbo.costs_inspection (
       costtype varchar(31),
        costid int identity not null,
        lastModified datetime2,
        active tinyint,
        autoset tinyint,
        costtypeid int,
        customorderby int,
        discountrate numeric(5,2),
        discountvalue numeric(10,2),
        finalcost numeric(10,2),
        hourlyrate numeric(10,2),
        labourcost numeric(10,2),
        labourtime int,
        totalcost numeric(10,2),
        lastModifiedBy int,
        primary key (costid)
    );

    create table dbo.costs_purchase (
       costtype varchar(31),
        costid int identity not null,
        lastModified datetime2,
        active tinyint,
        autoset tinyint,
        costtypeid int,
        customorderby int,
        discountrate numeric(5,2),
        discountvalue numeric(10,2),
        finalcost numeric(10,2),
        hourlyrate numeric(10,2),
        labourcost numeric(10,2),
        labourtime int,
        totalcost numeric(10,2),
        housecost numeric(10,2),
        tpcostsrc varchar(255),
        thirdcosttotal numeric(10,2),
        tpmanualprice numeric(10,2),
        tpmarkuprate numeric(10,2),
        tpmarkupsource varchar(255),
        tpmarkupvalue numeric(10,2),
        tpcarin numeric(10,2),
        tpcarinmarkupvalue numeric(10,2),
        tpcarmarkuprate numeric(10,2),
        tpcarmarkupsrc varchar(255),
        tpcarout numeric(10,2),
        tpcaroutmarkupvalue numeric(10,2),
        tpcartotal numeric(10,2),
        costsource varchar(255),
        lastModifiedBy int,
        linkedsalescostid int,
        nominalid int,
        primary key (costid)
    );

    create table dbo.costs_repair (
       costtype varchar(31),
        costid int identity not null,
        lastModified datetime2,
        active tinyint,
        autoset tinyint,
        costtypeid int,
        customorderby int,
        discountrate numeric(5,2),
        discountvalue numeric(10,2),
        finalcost numeric(10,2),
        hourlyrate numeric(10,2),
        labourcost numeric(10,2),
        labourtime int,
        totalcost numeric(10,2),
        partscost numeric(10,2),
        partsmarkuprate numeric(5,2),
        partsmarkupsource int,
        partsmarkupvalue numeric(10,2),
        partstotal numeric(10,2),
        housecost numeric(10,2),
        tpcostsrc varchar(255),
        thirdcosttotal numeric(10,2),
        tpmanualprice numeric(10,2),
        tpmarkuprate numeric(10,2),
        tpmarkupsource varchar(255),
        tpmarkupvalue numeric(10,2),
        tpcarin numeric(10,2),
        tpcarinmarkupvalue numeric(10,2),
        tpcarmarkuprate numeric(10,2),
        tpcarmarkupsrc varchar(255),
        tpcarout numeric(10,2),
        tpcaroutmarkupvalue numeric(10,2),
        tpcartotal numeric(10,2),
        costsource varchar(255),
        lastModifiedBy int,
        linkedrepaircostid int,
        nominalid int,
        primary key (costid)
    );

    create table dbo.costs_service (
       costtype varchar(31),
        costid int identity not null,
        lastModified datetime2,
        active tinyint,
        autoset tinyint,
        costtypeid int,
        customorderby int,
        discountrate numeric(5,2),
        discountvalue numeric(10,2),
        finalcost numeric(10,2),
        hourlyrate numeric(10,2),
        labourcost numeric(10,2),
        labourtime int,
        totalcost numeric(10,2),
        lastModifiedBy int,
        nominal int,
        primary key (costid)
    );

    create table dbo.country (
       countryid int identity not null,
        addressprintformat varchar(255),
        country varchar(30),
        countrycode varchar(6),
        memberOfUE bit,
        primary key (countryid)
    );

    create table dbo.courier (
       courierid int identity not null,
        lastModified datetime2,
        defaultcourier bit,
        name varchar(100),
        webtrackurl varchar(1000),
        lastModifiedBy int,
        orgid int,
        primary key (courierid)
    );

    create table dbo.courierdespatch (
       courierdespatchid int identity not null,
        lastModified datetime2,
        consignmentno varchar(255),
        creationdate date,
        despatchdate date,
        lastModifiedBy int,
        orgid int,
        cdtypeid int,
        createdbyid int,
        primary key (courierdespatchid)
    );

    create table dbo.courierdespatchtype (
       cdtid int identity not null,
        lastModified datetime2,
        defaultforcourier bit,
        description varchar(255),
        lastModifiedBy int,
        courierid int,
        primary key (cdtid)
    );

    create table dbo.creditcard (
       ccid int identity not null,
        lastModified datetime2,
        cardname varchar(60),
        encryptedcardno varchar(255),
        expirydate varchar(6),
        houseno varchar(4),
        issueno varchar(2),
        postcode varchar(10),
        lastModifiedBy int,
        personid int,
        typeid int,
        primary key (ccid)
    );

    create table dbo.creditcardtype (
       typeid int identity not null,
        description varchar(255),
        name varchar(10),
        primary key (typeid)
    );

    create table dbo.creditnote (
       id int identity not null,
        lastModified datetime2,
        clientref varchar(100),
        duration int,
        expirydate date,
        issued bit,
        issuedate date,
        rate numeric(10,2),
        regdate date,
        reqdate date,
        finalcost numeric(10,2),
        totalcost numeric(10,2),
        vatrate numeric(10,2),
        vatvalue numeric(10,2),
        accountstatus varchar(1),
        creditnoteno varchar(30),
        lastModifiedBy int,
        orgid int,
        createdby int,
        currencyid int,
        issueby int,
        personid int,
        invid int,
        statusid int,
        vatcode varchar(1),
        primary key (id)
    );

    create table dbo.creditnoteaction (
       id int identity not null,
        date datetime2,
        personid int,
        activityid int,
        creditnoteid int,
        statusid int,
        primary key (id)
    );

    create table dbo.creditnoteitem (
       id int identity not null,
        lastModified datetime2,
        finalcost numeric(10,2),
        discountrate numeric(5,2),
        discountvalue numeric(10,2),
        itemno int,
        partofbaseunit bit,
        quantity int,
        totalcost numeric(10,2),
        taxamount numeric(19,2),
        taxable tinyint,
        description varchar(1000),
        lastModifiedBy int,
        shiptoaddressid int,
        orgid int,
        creditnoteid int,
        nominalid int,
        primary key (id)
    );

    create table dbo.creditnotenote (
       noteid int identity not null,
        lastModified datetime2,
        active tinyint,
        deactivatedon datetime2,
        label varchar(50),
        note nvarchar(2000),
        publish tinyint,
        seton datetime2,
        lastModifiedBy int,
        deactivatedby int,
        setby int,
        jobid int,
        primary key (noteid)
    );

    create table dbo.creditnotestatus (
       statusid int identity not null,
        defaultstatus bit,
        description varchar(200),
        name varchar(100),
        oninsert bit,
        onissue bit,
        requiresattention bit,
        actiontype varchar(255),
        nextid int,
        primary key (statusid)
    );

    create table dbo.creditnotestatusdescriptiontranslation (
       statusid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (statusid, locale, translation)
    );

    create table dbo.creditnotestatusnametranslation (
       statusid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (statusid, locale, translation)
    );

    create table dbo.creditnotetax (
       id int identity not null,
        amount numeric(10,2),
        description varchar(100),
        rate numeric(8,6),
        tax numeric(10,2),
        creditnoteid int,
        vatrateid varchar(1),
        primary key (id)
    );

    create table dbo.customquotationcalibrationcondition (
       defcalconid int identity not null,
        lastModified datetime2,
        conditiontext varchar(2000),
        lastModifiedBy int,
        caltypeid int,
        quoteid int,
        primary key (defcalconid)
    );

    create table dbo.customquotationgeneralcondition (
       defgenconid int identity not null,
        lastModified datetime2,
        conditiontext varchar(5000),
        lastModifiedBy int,
        orgid int,
        quoteid int,
        primary key (defgenconid)
    );

    create table dbo.defaultnote (
       defaultnoteid int identity not null,
        lastModified datetime2,
        label varchar(50),
        note varchar(5000),
        publish bit,
        lastModifiedBy int,
        orgid int,
        componentid int,
        primary key (defaultnoteid)
    );

    create table dbo.defaultnotelabeltranslation (
       defaultnoteid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (defaultnoteid, locale, translation)
    );

    create table dbo.defaultnotenotetranslation (
       defaultnoteid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (defaultnoteid, locale, translation)
    );

    create table dbo.defaultpricingremark (
       id int identity not null,
        costType int,
        remark nvarchar(255),
        remarktype int,
        locale varchar(255),
        orgid int,
        primary key (id)
    );

    create table dbo.defaultquotationcalibrationcondition (
       defcalconid int identity not null,
        lastModified datetime2,
        conditiontext varchar(2000),
        lastModifiedBy int,
        orgid int,
        caltypeid int,
        primary key (defcalconid)
    );

    create table dbo.defaultquotationcalibrationconditiontranslation (
       defcalconid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (defcalconid, locale, translation)
    );

    create table dbo.defaultquotationgeneralcondition (
       defgenconid int identity not null,
        lastModified datetime2,
        conditiontext varchar(5000),
        lastModifiedBy int,
        orgid int,
        primary key (defgenconid)
    );

    create table dbo.defaultquotationgeneralconditiontranslation (
       defgenconid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (defgenconid, locale, translation)
    );

    create table dbo.defaultstandard (
       type varchar(31),
        id int identity not null,
        lastModified datetime2,
        enforceuse bit,
        seton date,
        lastModifiedBy int,
        plantid int,
        setby int,
        capabilityId int,
        wiid int,
        primary key (id)
    );

    create table dbo.defect (
       type varchar(31),
        id int identity not null,
        lastModified datetime2,
        description varchar(500),
        calsealbroken bit,
        lastModifiedBy int,
        jobitemid int,
        primary key (id)
    );

    create table dbo.deletedcomponent (
       id int identity not null,
        component varchar(255),
        date datetime2,
        reason varchar(200),
        refno varchar(30),
        personid int,
        primary key (id)
    );

    create table dbo.delivery (
       deliverytype varchar(31),
        deliveryid int identity not null,
        lastModified datetime2,
        creationdate date,
        deliverydate date,
        deliveryno varchar(30),
        destinationReceiptDate datetime2,
        sigcaptured bit,
        signedon datetime,
        signeename varchar(60),
        clientref varchar(100),
        ourref varchar(100),
        lastModifiedBy int,
        orgid int,
        addressid int,
        contactid int,
        courierdespatchid int,
        createdbyid int,
        freehandid int,
        locationid int,
        scheduleid int,
        jobid int,
        primary key (deliveryid)
    );

    create table dbo.deliveryitem (
       deliveryitemtype varchar(31),
        deliveryitemid int identity not null,
        lastModified datetime2,
        accessoryFreeText varchar(255),
        despatched bit,
        despatchedon datetime2,
        itemdescription varchar(255),
        lastModifiedBy int,
        deliveryid int,
        jobitemid int,
        tprequirementid int,
        primary key (deliveryitemid)
    );

    create table dbo.deliveryitemaccessory (
       id int identity not null,
        qty int,
        accessoryId int,
        deliveryitemid int,
        primary key (id)
    );

    create table dbo.deliveryitemnote (
       noteid int identity not null,
        lastModified datetime2,
        active tinyint,
        deactivatedon datetime2,
        label varchar(50),
        note nvarchar(2000),
        publish tinyint,
        seton datetime2,
        lastModifiedBy int,
        deactivatedby int,
        setby int,
        deliveryitemid int,
        primary key (noteid)
    );

    create table dbo.deliverynote (
       noteid int identity not null,
        lastModified datetime2,
        active tinyint,
        deactivatedon datetime2,
        label varchar(50),
        note nvarchar(2000),
        publish tinyint,
        seton datetime2,
        lastModifiedBy int,
        deactivatedby int,
        setby int,
        deliveryid int,
        primary key (noteid)
    );

    create table dbo.demandmodel (
       id int identity not null,
        demand int,
        model int,
        primary key (id)
    );

    create table dbo.department (
       deptid int identity not null,
        name varchar(40),
        shortname varchar(4),
        typeid int,
        addrid int,
        subdivid int,
        primary key (deptid)
    );

    create table dbo.departmentmember (
       id int identity not null,
        lastModified datetime2,
        lastModifiedBy int,
        personid int,
        deptid int,
        role int,
        primary key (id)
    );

    create table dbo.departmentrole (
       id int identity not null,
        active bit,
        description varchar(255),
        manage bit,
        role varchar(20),
        primary key (id)
    );

    create table dbo.description (
       descriptionid int identity not null,
        lastModified datetime2,
        active bit,
        description nvarchar(100),
        tmlid int,
        typology int,
        lastModifiedBy int,
        familyid int,
        primary key (descriptionid)
    );

    create table dbo.descriptiontranslation (
       descriptionid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (descriptionid, locale, translation)
    );

    create table dbo.dimensional_cylindricalstandard (
       id double precision,
        sizeimperial numeric(20,5),
        sizemetric numeric(20,5),
        primary key (id)
    );

    create table dbo.dimensional_thread (
       id int identity not null,
        lastModified datetime2,
        SDatum numeric(20,5),
        ball numeric(20,5),
        depthStep numeric(20,5),
        depthStepLower numeric(20,5),
        depthStepUpper numeric(20,5),
        goeffectivediameter numeric(20,5),
        golowerlimit numeric(20,5),
        goupperlimit numeric(20,5),
        gowornlimit numeric(20,5),
        lead numeric(20,5),
        lengthToGaugePlane numeric(20,5),
        lengthToGaugePlaneLower numeric(20,5),
        lengthToGaugePlaneUpper numeric(20,5),
        maxStep numeric(20,5),
        minStep numeric(20,5),
        nostarts int,
        notgoeffective numeric(20,5),
        notgolowerlimit numeric(20,5),
        notgoupperlimit numeric(20,5),
        notgowornlimit numeric(20,5),
        overallLength numeric(20,5),
        overallLengthB numeric(20,5),
        overallLengthLower numeric(20,5),
        overallLengthUpper numeric(20,5),
        pitch numeric(20,5),
        size varchar(100),
        taperLimit numeric(20,5),
        wornlimit numeric(20,5),
        lastModifiedBy int,
        cylstandid double precision,
        typeid int,
        uomid int,
        wireid int,
        primary key (id)
    );

    create table dbo.dimensional_threadtype (
       id int identity not null,
        type varchar(100),
        primary key (id)
    );

    create table dbo.dimensional_threaduom (
       id int identity not null,
        description varchar(50),
        primary key (id)
    );

    create table dbo.dimensional_wire (
       id int identity not null,
        ba numeric(20,5),
        bapitch numeric(20,5),
        ballno numeric(20,5),
        designation varchar(50),
        eoiso numeric(20,5),
        eounified numeric(20,5),
        eowidth numeric(20,5),
        metricpitch numeric(20,5),
        rubyball numeric(20,5),
        unifiedtpi numeric(20,5),
        whitworthtpi numeric(20,5),
        wire1 numeric(20,5),
        wire2 numeric(20,5),
        wiremean numeric(20,5),
        primary key (id)
    );

    create table dbo.email (
       id int identity not null,
        lastModified datetime2,
        body nvarchar(max),
        bodyxindicekey varchar(100),
        component varchar(255),
        entityid int,
        fromaddress varchar(100),
        publish bit,
        senton datetime,
        subject nvarchar(200),
        lastModifiedBy int,
        sentby int,
        primary key (id)
    );

    create table dbo.emailrecipient (
       id int identity not null,
        lastModified datetime2,
        emailaddress varchar(100),
        type varchar(255),
        lastModifiedBy int,
        emailid int,
        personid int,
        primary key (id)
    );

    create table dbo.emailtemplate (
       id int identity not null,
        lastModified datetime2,
        component int,
        locale varchar(255),
        subject nvarchar(200),
        template nvarchar(2000),
        lastModifiedBy int,
        orgid int,
        subdivid int,
        primary key (id)
    );

    create table dbo.engineerallocation (
       id int identity not null,
        active bit,
        allocatedfor date,
        allocatedon date,
        allocateduntil date,
        estimatedallocationtime int,
        timeofday varchar(255),
        timeofdayuntil varchar(255),
        allocatedbyid int,
        allocatedtoid int,
        deptid int,
        jobitemid int,
        primary key (id)
    );

    create table dbo.engineerallocationmessage (
       id int identity not null,
        message varchar(500),
        seton datetime2,
        allocationid int,
        setbyid int,
        primary key (id)
    );

    create table dbo.event_subscriber (
       id int identity not null,
        model varchar(100),
        primary key (id)
    );

    create table dbo.event_subscription (
       id int identity not null,
        entitytype int,
        subscriberid int,
        primary key (id)
    );

    create table dbo.exception (
       id int identity not null,
        comment varchar(255),
        date datetime,
        exception varchar(1000),
        reviewed bit,
        stacktrace varchar(max),
        url varchar(255),
        userid varchar(20),
        primary key (id)
    );

    create table dbo.exchangeformat (
       id int identity not null,
        lastModified datetime2,
        dateFormat varchar(255),
        deleted bit default 0,
        description varchar(200),
        exchangeFormatLevel varchar(255),
        linestoskip int,
        name varchar(255),
        sheetname varchar(255),
        eftype varchar(255),
        lastModifiedBy int,
        aliasgroupid int,
        businessCompanyid int,
        businessSubdivid int,
        clientCompanyid int,
        primary key (id)
    );

    create table dbo.exchangeformatfieldnamedetails (
       id int identity not null,
        fieldName varchar(255),
        mandatory bit,
        position int,
        templateName varchar(255),
        exchangeFormatid int,
        primary key (id)
    );

    create table dbo.exchangeformatfile (
       type varchar(31),
        id int identity not null,
        lastModified datetime2,
        addedon datetime2,
        fileName varchar(255),
        lastModifiedBy int,
        personid int,
        primary key (id)
    );

    create table dbo.failedLoginAttempt (
       id int identity not null,
        createdAt datetime2,
        userName varchar(255),
        primary key (id)
    );

    create table dbo.faultreport (
       faultrepid int identity not null,
        lastModified datetime2,
        approvaltype varchar(255),
        awaitingfinalisation bit,
        ber bit,
        bercheckcomment varchar(500),
        bercheckon datetime,
        berseton datetime,
        clientalias varchar(255),
        clientapproval bit,
        clientapprovalon datetime,
        clientcomments varchar(500),
        clientoutcome varchar(255),
        clientresponseneeded bit,
        cost numeric(10,2),
        dispensation bit,
        dispensationcomments varchar(255),
        estadjusttime int,
        estdeliverytime int,
        estrepairtime int,
        faultreportnumber varchar(30),
        finaloutcome varchar(255),
        issuedate datetime2,
        leadtime int,
        managercomments varchar(255),
        managervalidation bit,
        managervalidationon datetime2,
        nocostingrequired bit,
        operationbytrescal bit,
        operationinlaboratory bit,
        otherstate varchar(255),
        outcomepreselected bit,
        precert bit,
        prerecord bit,
        requiredeliverytoclient bit,
        sendtoclient bit,
        source varchar(255),
        techniciancomments varchar(2000),
        trescalref varchar(255),
        currencyvalue numeric(19,2),
        version varchar(255),
        lastModifiedBy int,
        orgid int,
        bercheckbyid int,
        bersetbyid int,
        currencyid int,
        clientapprovalbyid int,
        jobitemid int,
        managervalidationbyid int,
        technicianid int,
        primary key (faultrepid)
    );

    create table dbo.faultreportaction (
       id int identity not null,
        lastModified datetime2,
        active bit,
        description nvarchar(1000),
        lastModifiedBy int,
        faultrepid int,
        recordedby int,
        primary key (id)
    );

    create table dbo.faultreportdescription (
       id int identity not null,
        lastModified datetime2,
        active bit,
        description nvarchar(1000),
        lastModifiedBy int,
        faultrepid int,
        recordedby int,
        primary key (id)
    );

    create table dbo.faultreportinstruction (
       id int identity not null,
        lastModified datetime2,
        active bit,
        description nvarchar(1000),
        showontpdelnote bit,
        lastModifiedBy int,
        faultrepid int,
        recordedby int,
        primary key (id)
    );

    create table dbo.faultreportrecommendation (
       faultreportid int,
        recommendation varchar(255)
    );

    create table dbo.faultreportstate (
       faultreportid int,
        state varchar(255)
    );

    create table dbo.freehandcontact (
       id int identity not null,
        lastModified datetime2,
        address varchar(300),
        company varchar(100),
        contact varchar(100),
        subdiv varchar(100),
        telephone varchar(30),
        lastModifiedBy int,
        primary key (id)
    );

    create table dbo.freerepaircomponent (
       id int identity not null,
        addedafterrirvalidation bit,
        cost numeric(19,2),
        name varchar(255),
        quantity int,
        source varchar(255),
        status varchar(255),
        componentid int,
        freerepairoperationid int,
        primary key (id)
    );

    create table dbo.freerepairoperation (
       id int identity not null,
        addedafterrirvalidation bit,
        cost numeric(10,2),
        labourtime int,
        name varchar(255),
        operationstatus varchar(255),
        operationtype varchar(255),
        position int,
        repaircompletionreportid int,
        repairinspectionreportid int,
        tpcoid int,
        primary key (id)
    );

    create table dbo.generalserviceoperation (
       id int identity not null,
        lastModified datetime2,
        clientdecisionneeded bit,
        completedon datetime2,
        startedon datetime2,
        lastModifiedBy int,
        orgid int,
        addressid int,
        caltypeid int,
        capabilityId int,
        completedby int,
        startedby int,
        status int,
        primary key (id)
    );

    create table dbo.groupaccessory (
       id int identity not null,
        quantity int,
        accessory_id int,
        groupid int,
        primary key (id)
    );

    create table dbo.gsodocument (
       id int identity not null,
        lastModified datetime2,
        documentdate datetime2,
        documentnumber varchar(60),
        lastModifiedBy int,
        gsoid int,
        supplementaryforid int,
        primary key (id)
    );

    create table dbo.gsodocumentlink (
       id int identity not null,
        lastModified datetime2,
        lastModifiedBy int,
        gsodocumentid int,
        jobitemid int,
        primary key (id)
    );

    create table dbo.gsolink (
       id int identity not null,
        lastModified datetime2,
        timespent int,
        lastModifiedBy int,
        gsoid int,
        jobitemid int,
        actionoutcomeid int,
        primary key (id)
    );

    create table dbo.hire (
       id int identity not null,
        lastModified datetime2,
        clientref varchar(100),
        duration int,
        expirydate date,
        issued bit,
        issuedate date,
        rate numeric(10,2),
        regdate date,
        reqdate date,
        finalcost numeric(10,2),
        totalcost numeric(10,2),
        vatrate numeric(10,2),
        vatvalue numeric(10,2),
        accountsvarified bit,
        enquiry bit,
        enquirydate date,
        enquiryestduration int,
        hiredate date,
        hireno varchar(30),
        ponumber varchar(50),
        specialinst varchar(200),
        lastModifiedBy int,
        orgid int,
        createdby int,
        currencyid int,
        issueby int,
        personid int,
        addressid int,
        courierdespatchid int,
        freehandid int,
        statusid int,
        transportoptionid int,
        primary key (id)
    );

    create table dbo.hireaccessory (
       hireaccessoryid int identity not null,
        lastModified datetime2,
        active bit,
        item varchar(300),
        lastModifiedBy int,
        hireinstid int,
        statusid int,
        primary key (hireaccessoryid)
    );

    create table dbo.hirecategory (
       id int identity not null,
        categorycode int,
        categoryname varchar(100),
        sectionid int,
        primary key (id)
    );

    create table dbo.hirecrossitem (
       itemid int identity not null,
        lastModified datetime2,
        description varchar(200),
        plantno varchar(50),
        serialno varchar(50),
        lastModifiedBy int,
        primary key (itemid)
    );

    create table dbo.hireinstrument (
       id int identity not null,
        lastModified datetime2,
        lastModifiedBy int,
        plantid int,
        primary key (id)
    );

    create table dbo.hireitem (
       id int identity not null,
        lastModified datetime2,
        finalcost numeric(10,2),
        discountrate numeric(5,2),
        discountvalue numeric(10,2),
        itemno int,
        partofbaseunit bit,
        quantity int,
        totalcost numeric(10,2),
        hirecost numeric(10,2),
        offhire bit,
        offhirecondition varchar(100),
        offhiredate date,
        offhiredays int,
        suspended bit,
        ukascalcost numeric(10,2),
        lastModifiedBy int,
        caltypeid int,
        hireid int,
        itemid int,
        hireinstid int,
        primary key (id)
    );

    create table dbo.hireitemaccessory (
       id int identity not null,
        lastModified datetime2,
        returned bit,
        lastModifiedBy int,
        hireaccessoryid int,
        hireitemid int,
        primary key (id)
    );

    create table dbo.hiremodel (
       id int identity not null,
        lastModified datetime2,
        hirecost numeric(10,2),
        hiremodeldetail varchar(500),
        purchasecost numeric(10,2),
        lastModifiedBy int,
        orgid int,
        modelid int,
        primary key (id)
    );

    create table dbo.hiremodelcategory (
       id int identity not null,
        modelcode int,
        plantcode varchar(3),
        categoryid int,
        primary key (id)
    );

    create table dbo.hiremodelincategory (
       id int identity not null,
        hiremodelid int,
        hiremodelcatid int,
        primary key (id)
    );

    create table dbo.hirenote (
       noteid int identity not null,
        lastModified datetime2,
        active tinyint,
        deactivatedon datetime2,
        label varchar(50),
        note nvarchar(2000),
        publish tinyint,
        seton datetime2,
        lastModifiedBy int,
        deactivatedby int,
        setby int,
        hireid int,
        primary key (noteid)
    );

    create table dbo.hirereplacedinstrument (
       id int identity not null,
        lastModified datetime2,
        reason varchar(400),
        replacedon date,
        lastModifiedBy int,
        hireinstid int,
        hireitemid int,
        replacedbyid int,
        primary key (id)
    );

    create table dbo.hiresection (
       id int identity not null,
        sectioncode int,
        sectionname varchar(100),
        primary key (id)
    );

    create table dbo.hiresuspenditem (
       id int identity not null,
        lastModified datetime2,
        enddate date,
        startdate date,
        lastModifiedBy int,
        hireitemid int,
        primary key (id)
    );

    create table dbo.hook (
       id int identity not null,
        active bit,
        alwayspossible bit,
        beginactivity bit,
        completeactivity bit,
        name varchar(100),
        activityid int,
        primary key (id)
    );

    create table dbo.hookactivity (
       id int identity not null,
        beginactivity bit,
        completeactivity bit,
        activityid int,
        hookid int,
        stateid int,
        primary key (id)
    );

    create table dbo.images (
       type varchar(31),
        id int identity not null,
        lastModified datetime2,
        addedon datetime2,
        description varchar(200),
        fileName varchar(255),
        rfilepath varchar(300),
        lastModifiedBy int,
        personid int,
        jobitemid int,
        modelid int,
        primary key (id)
    );

    create table dbo.imagetag (
       id int identity not null,
        lastModified datetime2,
        description varchar(200),
        tag varchar(100),
        lastModifiedBy int,
        primary key (id)
    );

    create table dbo.instcertlink (
       id int identity not null,
        lastModified datetime2,
        lastModifiedBy int,
        certid int,
        plantid int,
        primary key (id)
    );

    create table dbo.instmodel (
       modelid int identity not null,
        lastModified datetime2,
        addedon date,
        model varchar(100),
        modelmfrtype varchar(255),
        quarantined bit,
        tmlid int,
        lastModifiedBy int,
        addedby int,
        descriptionid int,
        mfrid int,
        modeltypeid int,
        salescategoryid int,
        primary key (modelid)
    );

    create table dbo.instmodeldomain (
       domainid int identity not null,
        lastModified datetime2,
        active bit,
        domaintype int,
        name varchar(255),
        tmlid int,
        lastModifiedBy int,
        primary key (domainid)
    );

    create table dbo.instmodeldomaintranslation (
       domainid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (domainid, locale, translation)
    );

    create table dbo.instmodelfamily (
       familyid int identity not null,
        lastModified datetime2,
        active bit,
        name varchar(255),
        tmlid int,
        lastModifiedBy int,
        domainid int,
        primary key (familyid)
    );

    create table dbo.instmodelfamilytranslation (
       familyid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (familyid, locale, translation)
    );

    create table dbo.instmodeloption (
       instrumentmodeloptionid int identity not null,
        instmodelid int,
        optionid int,
        primary key (instrumentmodeloptionid)
    );

    create table dbo.instmodelsubfamily (
       subfamilyid int identity not null,
        lastModified datetime2,
        active bit,
        name varchar(255),
        tmlid int,
        lastModifiedBy int,
        familyid int,
        primary key (subfamilyid)
    );

    create table dbo.instmodelsubfamilytranslation (
       subfamilyid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (subfamilyid, locale, translation)
    );

    create table dbo.instmodeltranslation (
       modelid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (modelid, locale, translation)
    );

    create table dbo.instrument (
       plantid int identity not null,
        lastModified datetime2,
        addedon date,
        calexpiresafternumberofuses tinyint,
        calfrequency int,
        calintervalunit varchar(255),
        calibrationStandard tinyint,
        clientbarcode nvarchar(50),
        customerdescription nvarchar(2000),
        customermanaged bit,
        customerspecification nvarchar(100),
        defaultClientRef varchar(30),
        firmwareaccesscode varchar(100),
        formerbarcode nvarchar(50),
        freeTextLocation nvarchar(64),
        measurementtype nvarchar(2000),
        modelname varchar(255),
        recalldate date,
        permittednumberofuses int,
        plantno varchar(50),
        recallednotreceived tinyint,
        replacedon date,
        requirement varchar(300),
        scrapped tinyint,
        scrappedon date,
        serialno varchar(50),
        simplifiedPlantNo varchar(255),
        simplifiedSerialNo varchar(255),
        statusid int,
        usessincelastcalibration int,
        lastModifiedBy int,
        addressid int,
        addedby int,
        assetid int,
        baseunitid int,
        capabilityId int,
        coid int,
        personid int,
        defaultservicetype int,
        lastcalid int,
        locationid int,
        mfrid int,
        modelid int,
        replacedby int,
        replacementid int,
        scrappedby int,
        scrappedonji int,
        storageid int,
        threadid int,
        usageid int,
        primary key (plantid)
    );

    create table dbo.instrument_contract (
       contractid int,
        instrumentid int,
        primary key (contractid, instrumentid)
    );

    create table dbo.instrumentcharacteristic (
       id binary(255),
        freeTextValue varchar(255),
        value double precision,
        characteristic int,
        instrument int,
        libraryvalue int,
        uom int,
        primary key (id)
    );

    create table dbo.instrumentcomplementaryfield (
       instrumentComplementaryFieldId int identity not null,
        accreditation varchar(255),
        calibrationstatusoverride varchar(255),
        certclass varchar(255),
        remarks varchar(255),
        customeracceptancecriteria varchar(255),
        deliveryStatus varchar(255),
        firstUseDate datetime2,
        formerBarCode varchar(255),
        formerCharacteristics varchar(255),
        formerFamily varchar(255),
        formerLocalDescription varchar(255),
        formerManufacturer varchar(255),
        formerModel varchar(255),
        formerName varchar(255),
        nextinterimcalibrationdate datetime2,
        nextmaintenancedate datetime2,
        nominalValue numeric(19,2),
        provider varchar(255),
        purchaseCost numeric(19,2),
        purchaseDate datetime2,
        uomid int,
        plantid int,
        primary key (instrumentComplementaryFieldId)
    );

    create table dbo.instrumentfielddefinition (
       instrumentFieldDefinitionid int identity not null,
        fieldtype nvarchar(2000),
        isUpdatable bit,
        name nvarchar(2000),
        coid int,
        primary key (instrumentFieldDefinitionid)
    );

    create table dbo.instrumentfieldlibrary (
       instrumentFieldLibraryid int identity not null,
        name nvarchar(2000),
        instrumentFieldDefinitionid int,
        primary key (instrumentFieldLibraryid)
    );

    create table dbo.instrumenthistory (
       id int identity not null,
        addressupdated tinyint,
        changedate datetime,
        companyupdated tinyint,
        contactupdated tinyint,
        newplantno varchar(100),
        newserialno varchar(100),
        newstatusid int,
        oldplantno varchar(100),
        oldserialno varchar(100),
        oldstatusid int,
        plantnoupdated tinyint,
        serialnoupdated tinyint,
        statusUpdated tinyint,
        changeby int,
        plantid int,
        newaddressid int,
        newcompanyid int,
        newpersonid int,
        oldaddressid int,
        oldcompanyid int,
        oldpersonid int,
        primary key (id)
    );

    create table dbo.instrumentnote (
       noteid int identity not null,
        lastModified datetime2,
        active tinyint,
        deactivatedon datetime2,
        label varchar(50),
        note nvarchar(2000),
        publish tinyint,
        seton datetime2,
        lastModifiedBy int,
        deactivatedby int,
        setby int,
        plantid int,
        primary key (noteid)
    );

    create table dbo.instrumentrange (
       id int identity not null,
        lastModified datetime2,
        endd double precision,
        maxIsInfinite bit default 0,
        maxvalue nvarchar(2000),
        minvalue nvarchar(2000),
        start double precision,
        lastModifiedBy int,
        maxuomid int,
        uomid int,
        plantid int,
        primary key (id)
    );

    create table dbo.instrumentstoragetype (
       typeid int identity not null,
        defaulttype bit,
        fulldescription varchar(200),
        name varchar(50),
        recall bit,
        primary key (typeid)
    );

    create table dbo.instrumentstoragetypedescriptiontranslation (
       typeid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (typeid, locale, translation)
    );

    create table dbo.instrumentstoragetypenametranslation (
       typeid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (typeid, locale, translation)
    );

    create table dbo.instrumentusagetype (
       typeid int identity not null,
        defaulttype bit,
        fulldescription varchar(200),
        name varchar(50),
        recall bit,
        instrumentstatus int,
        primary key (typeid)
    );

    create table dbo.instrumentusagetypedescriptiontranslation (
       typeid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (typeid, locale, translation)
    );

    create table dbo.instrumentusagetypenametranslation (
       typeid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (typeid, locale, translation)
    );

    create table dbo.instrumentvalidationaction (
       id int identity not null,
        lastModified datetime2,
        action varchar(500),
        undertakenon datetime2,
        lastModifiedBy int,
        issueid int,
        personid int,
        primary key (id)
    );

    create table dbo.instrumentvalidationissue (
       id int identity not null,
        lastModified datetime2,
        issue varchar(200),
        raisedon date,
        resolvedon date,
        lastModifiedBy int,
        plantid int,
        raisedby int,
        resolvedby int,
        statusid int,
        primary key (id)
    );

    create table dbo.instrumentvalue (
       instrumentFieldValueid int identity not null,
        plantid int,
        instrumentFieldDefinitionid int,
        primary key (instrumentFieldValueid)
    );

    create table dbo.instrumentvalueboolean (
       value bit,
        instrumentFieldValueid int,
        primary key (instrumentFieldValueid)
    );

    create table dbo.instrumentvaluedatetime (
       value datetime2,
        instrumentFieldValueid int,
        primary key (instrumentFieldValueid)
    );

    create table dbo.instrumentvaluenumeric (
       value double precision,
        instrumentFieldValueid int,
        primary key (instrumentFieldValueid)
    );

    create table dbo.instrumentvalueselection (
       instrumentFieldValueid int,
        instrumentFieldLibraryid int,
        primary key (instrumentFieldValueid)
    );

    create table dbo.instrumentvaluestring (
       value nvarchar(2000),
        instrumentFieldValueid int,
        primary key (instrumentFieldValueid)
    );

    create table dbo.instrumentworkinstruction (
       workInstructionId int,
        plantId int
    );

    create table dbo.invoice (
       id int identity not null,
        lastModified datetime2,
        clientref varchar(100),
        duration int,
        expirydate date,
        issued bit,
        issuedate date,
        rate numeric(10,2),
        regdate date,
        reqdate date,
        finalcost numeric(10,2),
        totalcost numeric(10,2),
        vatrate numeric(10,2),
        vatvalue numeric(10,2),
        accountstatus varchar(1),
        duedate date,
        period int,
        invno varchar(30),
        invoicedate date,
        paymentterms varchar(10),
        pricingtype varchar(255),
        sitecostingnote varchar(1000),
        lastModifiedBy int,
        orgid int,
        createdby int,
        currencyid int,
        issueby int,
        personid int,
        addressid int,
        businesscontactid int,
        coid int,
        paymentmodeid int,
        statusid int,
        typeid int,
        vatcode varchar(1),
        primary key (id)
    );

    create table dbo.invoice_po_item (
       id int,
        invoiceitemid int,
        invpoid int,
        primary key (id)
    );

    create table dbo.invoiceactionlist (
       id int identity not null,
        date datetime2,
        personid int,
        activityid int,
        invoiceid int,
        statusid int,
        primary key (id)
    );

    create table dbo.invoicedelivery (
       id int identity not null,
        deliveryno varchar(255),
        invid int,
        primary key (id)
    );

    create table dbo.invoicedoctypetemplate (
       id int,
        defaulttemplate bit,
        compdoctypeid int,
        templateId int,
        description varchar(500),
        coid int,
        suptemplateid int,
        typeid int,
        primary key (id)
    );

    create table dbo.invoicefrequency (
       id int identity not null,
        code varchar(255),
        description varchar(255),
        primary key (id)
    );

    create table dbo.invoicefrequencytranslation (
       invoicefrequencyid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (invoicefrequencyid, locale, translation)
    );

    create table dbo.invoiceitem (
       id int,
        lastModified datetime2,
        finalcost numeric(10,2),
        discountrate numeric(5,2),
        discountvalue numeric(10,2),
        itemno int,
        partofbaseunit bit,
        quantity int,
        totalcost numeric(10,2),
        taxamount numeric(19,2),
        taxable tinyint,
        breakupcosts tinyint,
        description varchar(500),
        serialno varchar(30),
        lastModifiedBy int,
        shiptoaddressid int,
        adjustmentcost_id int,
        orgid int,
        calcost_id int,
        expenseitem int,
        invoiceid int,
        jobitemid int,
        nominalid int,
        purchasecost_id int,
        repaircost_id int,
        servicecost int,
        primary key (id)
    );

    create table dbo.invoiceitempo (
       id int identity not null,
        lastModified datetime2,
        lastModifiedBy int,
        bpoid int,
        invoiceitemid int,
        invpoid int,
        jobpoid int,
        primary key (id)
    );

    create table dbo.invoicejoblink (
       id int identity not null,
        lastModified datetime2,
        jobno varchar(30),
        systemjob tinyint,
        lastModifiedBy int,
        invoiceid int,
        jobid int,
        primary key (id)
    );

    create table dbo.invoicenote (
       noteid int identity not null,
        lastModified datetime2,
        active tinyint,
        deactivatedon datetime2,
        label varchar(50),
        note nvarchar(2000),
        publish tinyint,
        seton datetime2,
        lastModifiedBy int,
        deactivatedby int,
        setby int,
        invoiceid int,
        primary key (noteid)
    );

    create table dbo.invoicepo (
       poid int identity not null,
        lastModified datetime2,
        comment varchar(1000),
        ponumber varchar(60),
        lastModifiedBy int,
        orgid int,
        invid int,
        primary key (poid)
    );

    create table dbo.invoicestatus (
       statusid int identity not null,
        defaultstatus bit,
        description varchar(200),
        name varchar(100),
        approved bit,
        issued bit,
        oninsert bit,
        onissue bit,
        requiresattention bit,
        potype varchar(255),
        nextid int,
        primary key (statusid)
    );

    create table dbo.invoicestatusdescriptiontranslation (
       statusid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (statusid, locale, translation)
    );

    create table dbo.invoicestatusnametranslation (
       statusid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (statusid, locale, translation)
    );

    create table dbo.invoicetax (
       id int identity not null,
        amount numeric(10,2),
        description varchar(100),
        rate numeric(8,6),
        tax numeric(10,2),
        invoiceid int,
        vatrateid varchar(1),
        primary key (id)
    );

    create table dbo.invoicetype (
       id int identity not null,
        addtoaccount bit,
        deftosinglecost bit,
        includeonjobsheet bit,
        name varchar(200),
        primary key (id)
    );

    create table dbo.invoicetypetranslation (
       id int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (id, locale, translation)
    );

    create table dbo.itemaccessory (
       id int identity not null,
        quantity int,
        accessory_id int,
        faultreportid int,
        jobitemid int,
        primary key (id)
    );

    create table dbo.itemanalysisresult (
       id int identity not null,
        error varchar(255),
        field varchar(255),
        itemIndex int,
        warning varchar(255),
        stepexecutionid int,
        primary key (id)
    );

    create table dbo.itemstate (
       type varchar(31),
        stateid int identity not null,
        active bit,
        description varchar(150),
        retired bit,
        actionoutcometype varchar(255),
        lookupid int,
        primary key (stateid)
    );

    create table dbo.itemstatetranslation (
       stateid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (stateid, locale, translation)
    );

    create table dbo.job (
       jobid int identity not null,
        lastModified datetime2,
        agreeddeldate date,
        clientref varchar(30),
        datecomplete date,
        defaultturn int,
        estcarout numeric(10,2),
        jobno varchar(30),
        numberofpackages int,
        overridedateinonjobitems bit,
        packagetype varchar(255),
        pickupdate datetime,
        rate numeric(10,2),
        receiptdate datetime,
        regdate date,
        storagearea varchar(255),
        typeid int,
        lastModifiedBy int,
        orgid int,
        bookedinaddrid int,
        bookedinlocid int,
        personid int,
        createdby int,
        currencyid int,
        defaultcaltypeid int,
        defaultpoid int,
        inoptionid int,
        statusid int,
        returnoptionid int,
        returnto int,
        returntoloc int,
        primary key (jobid)
    );

    create table dbo.jobbpolink (
       id int identity not null,
        lastModified datetime2,
        createdon datetime2,
        defaultforjob bit,
        lastModifiedBy int,
        bpoid int,
        jobid int,
        primary key (id)
    );

    create table dbo.jobcosting (
       id int identity not null,
        lastModified datetime2,
        clientref varchar(100),
        duration int,
        expirydate date,
        issued bit,
        issuedate date,
        rate numeric(10,2),
        regdate date,
        reqdate date,
        finalcost numeric(10,2),
        totalcost numeric(10,2),
        vatrate numeric(10,2),
        vatvalue numeric(10,2),
        costingtypeid int,
        issuemethod varchar(255),
        lastupdateon datetime2,
        sitecostingnote varchar(1000),
        clientcosting varchar(255),
        ver int,
        lastModifiedBy int,
        orgid int,
        createdby int,
        currencyid int,
        issueby int,
        personid int,
        jobid int,
        lastupdateby int,
        statusid int,
        primary key (id)
    );

    create table dbo.jobcostingclientapproval (
       id int identity not null,
        clientapproval bit,
        clientapprovalcomment varchar(255),
        clientapprovalon datetime2,
        jobcostingid int,
        primary key (id)
    );

    create table dbo.jobcostingexpenseitem (
       id int identity not null,
        lastModified datetime2,
        finalcost numeric(10,2),
        discountrate numeric(5,2),
        discountvalue numeric(10,2),
        itemno int,
        partofbaseunit bit,
        quantity int,
        totalcost numeric(10,2),
        lastModifiedBy int,
        jobCosting int,
        jobExpenseItem int,
        serviceCost int,
        primary key (id)
    );

    create table dbo.jobcostingitem (
       id int identity not null,
        lastModified datetime2,
        finalcost numeric(10,2),
        discountrate numeric(5,2),
        discountvalue numeric(10,2),
        itemno int,
        partofbaseunit bit,
        quantity int,
        totalcost numeric(10,2),
        tpcarin numeric(10,2),
        tpcarinmarkupvalue numeric(10,2),
        tpcarmarkuprate numeric(10,2),
        tpcarmarkupsrc varchar(255),
        tpcarout numeric(10,2),
        tpcaroutmarkupvalue numeric(10,2),
        tpcartotal numeric(10,2),
        calibrated bit,
        lastModifiedBy int,
        adjustmentcost_id int,
        calcost_id int,
        inspectioncost_id int,
        costingid int,
        jobitemid int,
        purchasecost_id int,
        repaircost_id int,
        primary key (id)
    );

    create table dbo.jobcostingitemnote (
       noteid int identity not null,
        lastModified datetime2,
        active tinyint,
        deactivatedon datetime2,
        label varchar(50),
        note nvarchar(2000),
        publish tinyint,
        seton datetime2,
        lastModifiedBy int,
        deactivatedby int,
        setby int,
        jobcostingitemid int,
        primary key (noteid)
    );

    create table dbo.jobcostingitemremark (
       id int identity not null,
        costType int,
        remark nvarchar(255),
        remarktype int,
        pricingitemid int,
        primary key (id)
    );

    create table dbo.jobcostinglinkedadjustmentcost (
       id int identity not null,
        costid int,
        contractrevcostid int,
        jobcostadjcostid int,
        primary key (id)
    );

    create table dbo.jobcostinglinkedcalibrationcost (
       id int identity not null,
        calcostid int,
        contractrevcalcostid int,
        jobcostcalcostid int,
        quotecalcostid int,
        primary key (id)
    );

    create table dbo.jobcostinglinkedpurchasecost (
       id int identity not null,
        contractrevpurcostid int,
        jobcostpurcostid int,
        costid int,
        quotepurcostid int,
        primary key (id)
    );

    create table dbo.jobcostinglinkedrepaircost (
       id int identity not null,
        contractrevrepcostid int,
        jobcostrepcostid int,
        costid int,
        primary key (id)
    );

    create table dbo.jobcostingnote (
       noteid int identity not null,
        lastModified datetime2,
        active tinyint,
        deactivatedon datetime2,
        label varchar(50),
        note nvarchar(2000),
        publish tinyint,
        seton datetime2,
        lastModifiedBy int,
        deactivatedby int,
        setby int,
        jobcostingid int,
        primary key (noteid)
    );

    create table dbo.jobcostingviewed (
       id int identity not null,
        lastviewedon datetime2,
        jobcostingid int,
        lastviewedby int,
        primary key (id)
    );

    create table dbo.jobcourierlink (
       id int identity not null,
        lastModified datetime2,
        trackingnumber varchar(255),
        lastModifiedBy int,
        courierid int,
        jobid int,
        primary key (id)
    );

    create table dbo.jobexpenseitem (
       id int identity not null,
        lastModified datetime2,
        clientref varchar(30),
        comment varchar(255),
        date date,
        invoiceable bit,
        itemNo int,
        quantity int,
        cost numeric(10,2),
        lastModifiedBy int,
        job int,
        stateid int,
        model int,
        serviceType int,
        primary key (id)
    );

    create table dbo.jobexpenseitemnotinvoiced (
       expenseitemid int,
        comments nvarchar(200),
        date date,
        reason int,
        contactid int,
        periodicinvoiceid int,
        primary key (expenseitemid)
    );

    create table dbo.jobexpenseitempo (
       id int identity not null,
        lastModified datetime2,
        lastModifiedBy int,
        bpoid int,
        expenseitemid int,
        poid int,
        primary key (id)
    );

    create table dbo.jobinstructionlink (
       id int identity not null,
        lastModified datetime2,
        lastModifiedBy int,
        instructionid int,
        jobid int,
        primary key (id)
    );

    create table dbo.jobitem (
       jobitemid int identity not null,
        lastModified datetime2,
        clientref varchar(30),
        accessoryFreeText varchar(500),
        agreeddeldate date,
        bypasscosting tinyint,
        chasingactive tinyint,
        clientreceiptdate datetime2,
        datecomplete datetime,
        datein datetime,
        duedate date,
        itemno int,
        neverchase tinyint,
        paymentreceived tinyint,
        reqcleaning tinyint,
        transitdate date,
        turn int,
        lastModifiedBy int,
        jobid int,
        stateid int,
        adjust_id int,
        baseaddrid int,
        baselocid int,
        bookedinaddrid int,
        bookedinlocid int,
        caladdrid int,
        callocid int,
        caltypeid int,
        calcost_id int,
        capabilityId int,
        conrevoutcomeid int,
        contractid int,
        addrid int,
        locid int,
        defectid int,
        groupid int,
        inoptionid int,
        plantid int,
        lastContractReviewBy_personid int,
        nextworkreqid int,
        salescost_id int,
        repcost_id int,
        returnoptionid int,
        returnToAddress int,
        returnToContact int,
        servicetypeid int,
        wiid int,
        primary key (jobitemid)
    );

    create table dbo.jobitemaction (
       type varchar(31),
        id int identity not null,
        lastModified datetime2,
        activitydesc varchar(1000),
        createdon datetime2,
        deletestamp datetime,
        deleted tinyint,
        endstamp datetime,
        remark varchar(1000),
        startstamp datetime,
        timespent int,
        transitdate datetime,
        complete tinyint,
        lastModifiedBy int,
        activityid int,
        deletedbyid int,
        endstatusid int,
        jobitemid int,
        startstatusid int,
        startedbyid int,
        fromaddrid int,
        fromlocid int,
        toaddrid int,
        tolocid int,
        completedbyid int,
        actionoutcomeid int,
        primary key (id)
    );

    create table dbo.jobitemdemand (
       id int identity not null,
        additionalDemand varchar(255),
        demandStatus varchar(255),
        comment varchar(255),
        jobItemId int,
        primary key (id)
    );

    create table dbo.jobitemgroup (
       id int identity not null,
        lastModified datetime2,
        accessoryFreeText varchar(500),
        calibration tinyint,
        delivery tinyint,
        lastModifiedBy int,
        jobid int,
        primary key (id)
    );

    create table dbo.jobitemhistory (
       id int identity not null,
        changedate datetime2,
        comment nvarchar(1000),
        fieldname varchar(32),
        newvalue date,
        oldvalue date,
        valueupdated tinyint,
        changeby int,
        jobitemid int,
        primary key (id)
    );

    create table dbo.jobitemnote (
       noteid int identity not null,
        lastModified datetime2,
        active tinyint,
        deactivatedon datetime2,
        label varchar(50),
        note nvarchar(2000),
        publish tinyint,
        seton datetime2,
        lastModifiedBy int,
        deactivatedby int,
        setby int,
        jobitemid int,
        primary key (noteid)
    );

    create table dbo.jobitemnotinvoiced (
       id int identity not null,
        comments nvarchar(200),
        date date,
        reason int,
        contactid int,
        periodicinvoiceid int,
        jobitemid int,
        primary key (id)
    );

    create table dbo.jobitempo (
       id int identity not null,
        lastModified datetime2,
        lastModifiedBy int,
        bpoid int,
        jobitemid int,
        poid int,
        primary key (id)
    );

    create table dbo.jobitemworkrequirement (
       id int identity not null,
        lastModified datetime2,
        lastModifiedBy int,
        jobitemid int,
        reqid int,
        primary key (id)
    );

    create table dbo.jobnote (
       noteid int identity not null,
        lastModified datetime2,
        active tinyint,
        deactivatedon datetime2,
        label varchar(50),
        note nvarchar(2000),
        publish tinyint,
        seton datetime2,
        lastModifiedBy int,
        deactivatedby int,
        setby int,
        jobid int,
        primary key (noteid)
    );

    create table dbo.jobquotelink (
       id int identity not null,
        lastModified datetime2,
        linkedon datetime2,
        lastModifiedBy int,
        jobid int,
        linkedby int,
        quoteid int,
        primary key (id)
    );

    create table dbo.jobstatus (
       statusid int identity not null,
        defaultstatus bit,
        description varchar(200),
        name varchar(100),
        complete bit,
        readytoinvoice bit,
        primary key (statusid)
    );

    create table dbo.jobstatusdescriptiontranslation (
       statusid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (statusid, locale, translation)
    );

    create table dbo.jobstatusnametranslation (
       statusid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (statusid, locale, translation)
    );

    create table dbo.jobtypecaltype (
       id int identity not null,
        defaultvalue bit default 0,
        jobtypeid int,
        caltypeid int,
        primary key (id)
    );

    create table dbo.jobtypeservicetype (
       id int identity not null,
        defaultvalue bit default 0,
        jobtypeid int,
        servicetypeid int,
        primary key (id)
    );

    create table dbo.labelprinter (
       id int identity not null,
        description varchar(200),
        path varchar(100),
        darknesslevel int,
        ipv4 varchar(15),
        language varchar(255),
        resolution int,
        addrid int,
        locid int,
        media int,
        primary key (id)
    );

    create table dbo.labeltemplate (
       id int identity not null,
        labeltemplatetype varchar(255),
        name nvarchar(50),
        propertyName varchar(255),
        primary key (id)
    );

    create table dbo.labeltemplateadditionalfield (
       id int identity not null,
        fieldname nvarchar(50),
        name nvarchar(50),
        labeltemplateid int,
        primary key (id)
    );

    create table dbo.labeltemplateadditionalfieldtranslation (
       id int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (id, locale, translation)
    );

    create table dbo.limitCompany (
       id int identity not null,
        lastModified datetime2,
        maximum numeric(19,2),
        lastModifiedBy int,
        orgid int,
        permLimitId int,
        primary key (id)
    );

    create table dbo.location (
       locationid int identity not null,
        lastModified datetime2,
        active tinyint,
        location varchar(100),
        lastModifiedBy int,
        addressid int,
        primary key (locationid)
    );

    create table dbo.login (
       id int identity not null,
        logintime datetime,
        weblogcreated tinyint,
        weblogpath varchar(200),
        personid int,
        primary key (id)
    );

    create table dbo.lookupinstance (
       id int identity not null,
        lookup varchar(100),
        lookupfunction int,
        primary key (id)
    );

    create table dbo.lookupresult (
       id int identity not null,
        resultmessage int,
        lookupid int,
        outcomestatusid int,
        primary key (id)
    );

    create table dbo.mailgroupmember (
       memberid int identity not null,
        lastModified datetime2,
        mailgroupid int,
        lastModifiedBy int,
        personid int,
        primary key (memberid)
    );

    create table dbo.markuprange (
       id int identity not null,
        action int,
        actionvalue double precision,
        rangestart double precision,
        typeid int,
        primary key (id)
    );

    create table dbo.measurementpossibility (
       measurementpossibilityid int identity not null,
        lmax nvarchar(2000),
        maximum double precision,
        minimum double precision,
        name nvarchar(2000),
        quality nvarchar(2000),
        scalederiva nvarchar(2000),
        plantid int,
        primary key (measurementpossibilityid)
    );

    create table dbo.mfr (
       mfrid int identity not null,
        lastModified datetime2,
        active tinyint,
        genericmfr tinyint,
        name varchar(100),
        tmlid int,
        lastModifiedBy int,
        primary key (mfrid)
    );

    create table dbo.modelcomponent (
       id int identity not null,
        includebydefault tinyint,
        componentid int,
        modelid int,
        primary key (id)
    );

    create table dbo.modelnote (
       noteid int identity not null,
        lastModified datetime2,
        active tinyint,
        deactivatedon datetime2,
        label varchar(50),
        note nvarchar(2000),
        publish tinyint,
        seton datetime2,
        lastModifiedBy int,
        deactivatedby int,
        setby int,
        modelid int,
        primary key (noteid)
    );

    create table dbo.modeloption (
       optionid int identity not null,
        lastModified datetime2,
        active bit default 0,
        code varchar(255),
        log_createdon datetime2,
        modeltmlid int,
        name nvarchar(4000),
        tmlid int,
        lastModifiedBy int,
        primary key (optionid)
    );

    create table dbo.modelpartof (
       id int identity not null,
        lastModified datetime2,
        includedbydefault tinyint,
        lastModifiedBy int,
        baseid int,
        moduleid int,
        primary key (id)
    );

    create table dbo.modelrange (
       id int identity not null,
        lastModified datetime2,
        endd double precision,
        maxIsInfinite bit default 0,
        maxvalue nvarchar(2000),
        minvalue nvarchar(2000),
        start double precision,
        characteristictype nvarchar(2000),
        lastModifiedBy int,
        maxuomid int,
        uomid int,
        characteristicdescriptionid int,
        modelid int,
        modeloptionid int,
        primary key (id)
    );

    create table dbo.modelrangecharacteristiclibrary (
       modelrangecharacteristiclibraryid int identity not null,
        characteristiclibraryid int,
        modelid int,
        primary key (modelrangecharacteristiclibraryid)
    );

    create table dbo.modeltype (
       instmodeltypeid int identity not null,
        active tinyint,
        baseunits tinyint,
        canselect bit default 0,
        capability bit default 0,
        defaulttype tinyint,
        modeltypedesc varchar(200),
        modeltypename varchar(20),
        modules tinyint,
        salecategory bit default 0,
        undefined bit default 0,
        primary key (instmodeltypeid)
    );

    create table dbo.modeltypedesctranslation (
       instmodeltypeid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (instmodeltypeid, locale, translation)
    );

    create table dbo.modeltypenametranslation (
       instmodeltypeid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (instmodeltypeid, locale, translation)
    );

    create table dbo.nextactivity (
       id int identity not null,
        manualentryallowed tinyint,
        activityid int,
        statusid int,
        primary key (id)
    );

    create table dbo.nominalcode (
       id int identity not null,
        lastModified datetime2,
        avalarataxcode varchar(255),
        code varchar(100),
        costtypeid int,
        ledger varchar(255),
        typenominalcode varchar(255),
        title varchar(100),
        lastModifiedBy int,
        caltypeid int,
        deptid int,
        primary key (id)
    );

    create table dbo.nominalcodeapplication (
       id int identity not null,
        costtypeid int,
        domainid int,
        familyid int,
        nominalcodeid int,
        subfamilyid int,
        primary key (id)
    );

    create table dbo.nominalcodetitletranslation (
       id int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (id, locale, translation)
    );

    create table dbo.nominalinvoicelookup (
       id int identity not null,
        nominaltype varchar(10),
        nominalid int,
        primary key (id)
    );

    create table dbo.notificationsystemfieldchange (
       id int identity not null,
        changedate datetime2,
        fieldname varchar(255),
        fieldtype varchar(255),
        newvalue varchar(255),
        oldvalue varchar(255),
        notificationid int,
        primary key (id)
    );

    create table dbo.notificationsystemqueue (
       id int identity not null,
        lastModified datetime2,
        createdon datetime2,
        entityclass varchar(255),
        fieldname varchar(255),
        fieldtype varchar(255),
        fieldvalue varchar(255),
        isreceived bit,
        issent bit,
        lasterrormessage varchar(255),
        lasterrormessageon datetime2,
        operationtype varchar(255),
        receivedon datetime2,
        senton datetime2,
        lastModifiedBy int,
        plantid int,
        jobcontactid int,
        jobitemid int,
        primary key (id)
    );

    create table dbo.numberformat (
       id int identity not null,
        lastModified datetime2,
        format varchar(255),
        global bit,
        numerationType varchar(255),
        universal bit,
        lastModifiedBy int,
        orgid int,
        primary key (id)
    );

    create table dbo.numeration (
       id int identity not null,
        lastModified datetime2,
        count int,
        numerationType varchar(255),
        year int,
        lastModifiedBy int,
        orgid int,
        primary key (id)
    );

    create table dbo.oauth2accesstoken (
       id int identity not null,
        serializedauthentication varchar(max),
        serializedtoken varchar(8000),
        tokenid varchar(255),
        authenticationid varchar(255),
        clientid varchar(255),
        refreshtoken varchar(255),
        username varchar(255),
        primary key (id)
    );

    create table dbo.oauth2clientdetails (
       clientid varchar(256),
        accesstokenvalidity int,
        clientSecret varchar(256),
        description nvarchar(2000),
        refreshtokenvalidity int,
        resourceIds varchar(256),
        scope varchar(256),
        role int,
        primary key (clientid)
    );

    create table dbo.oauth2refreshtoken (
       id int identity not null,
        serializedauthentication varchar(max),
        serializedtoken varchar(8000),
        tokenid varchar(255),
        primary key (id)
    );

    create table dbo.onbehalfitem (
       id int identity not null,
        lastModified datetime2,
        seton datetime2,
        lastModifiedBy int,
        addressid int,
        coid int,
        jobitemid int,
        setby int,
        primary key (id)
    );

    create table dbo.optiontranslation (
       optionid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (optionid, locale, translation)
    );

    create table dbo.outcomestatus (
       id int identity not null,
        defaultoutcome tinyint,
        activityid int,
        lookupid int,
        statusid int,
        primary key (id)
    );

    create table dbo.pat (
       id int identity not null,
        lastModified datetime2,
        bondingok tinyint,
        bondingreading numeric(10,2),
        cableok tinyint,
        caseok tinyint,
        comment varchar(200),
        fuseok tinyint,
        insulationok tinyint,
        insulationreading numeric(10,2),
        plugok tinyint,
        safetouse tinyint,
        testdate datetime,
        testeq varchar(20),
        lastModifiedBy int,
        jobitemid int,
        testedbyid int,
        primary key (id)
    );

    create table dbo.paymentmode (
       id int identity not null,
        code varchar(255),
        description varchar(255),
        primary key (id)
    );

    create table dbo.paymentmodetranslation (
       paymentmodeid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (paymentmodeid, locale, translation)
    );

    create table dbo.permission (
       groupId int,
        permission varchar(255)
    );

    create table dbo.permissiongroup (
       id int identity not null,
        name varchar(255),
        primary key (id)
    );

    create table dbo.permissiongrouptranslation (
       permissiongroup int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (permissiongroup, locale, translation)
    );

    create table dbo.permissionlimit (
       id int identity not null,
        levelPermission int,
        permissionName varchar(255),
        typePermission varchar(255),
        primary key (id)
    );

    create table dbo.permitedcalibrationextension (
       extensionid int identity not null,
        extensionEndDate date,
        timeunit varchar(255),
        type varchar(255),
        value int,
        plantid int,
        primary key (extensionid)
    );

    create table dbo.perrole (
       id int identity not null,
        name varchar(255),
        primary key (id)
    );

    create table dbo.perroletranslation (
       perrole int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (perrole, locale, translation)
    );

    create table dbo.po (
       poid int identity not null,
        lastModified datetime2,
        comment varchar(1000),
        ponumber varchar(60),
        active tinyint,
        deactivatedtime datetime2,
        lastModifiedBy int,
        orgid int,
        deactivatedby int,
        jobid int,
        prebookingdetailsid int,
        primary key (poid)
    );

    create table dbo.po_origin (
       type varchar(31),
        id bigint identity not null,
        lastModified datetime2,
        lastModifiedBy int,
        jobid int,
        poId int,
        quoteId int,
        primary key (id)
    );

    create table dbo.pointsettemplate (
       id int identity not null,
        lastModified datetime2,
        description varchar(500),
        seton datetime2,
        title varchar(100),
        lastModifiedBy int,
        personid int,
        primary key (id)
    );

    create table dbo.portal (
       portalId int identity not null,
        portalName varchar(255),
        primary key (portalId)
    );

    create table dbo.prebookingjobdetails (
       id int identity not null,
        clientref varchar(30),
        estcarout numeric(10,2),
        pickupDate datetime2,
        asnid int,
        bookedinaddrid int,
        bookedinlocid int,
        bpoid int,
        personid int,
        currencyid int,
        defaultpoid int,
        inoptionid int,
        returnoptionid int,
        returnto int,
        returntoloc int,
        primary key (id)
    );

    create table dbo.presetcomment (
       id int identity not null,
        lastModified datetime2,
        comment nvarchar(2000),
        label varchar(50),
        seton datetime2,
        type varchar(255),
        lastModifiedBy int,
        orgid int,
        categoryid int,
        setbyid int,
        primary key (id)
    );

    create table dbo.presetcommentcategory (
       id int identity not null,
        category varchar(50),
        seton datetime2,
        setbyid int,
        primary key (id)
    );

    create table dbo.presetcommentcategorytranslation (
       id int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (id, locale, translation)
    );

    create table dbo.printer (
       id int identity not null,
        description varchar(200),
        path varchar(100),
        addrid int,
        locid int,
        primary key (id)
    );

    create table dbo.printertray (
       id int identity not null,
        mediatrayref varchar(255),
        papersource int,
        papertype varchar(255),
        trayno int,
        printerid int,
        primary key (id)
    );

    create table dbo.printfilerule (
       id int identity not null,
        copies int,
        filenamelike varchar(100),
        firstpagepapertype varchar(255),
        restpagepapertype varchar(255),
        printerid int,
        primary key (id)
    );

    create table dbo.priorquotestatus (
       pqsid int identity not null,
        revokedon datetime2,
        seton datetime2,
        quoteid int,
        revokedby int,
        revokedto int,
        setby int,
        statusid int,
        primary key (pqsid)
    );

    create table dbo.procedureheading (
       id int identity not null,
        lastModified datetime2,
        heading varchar(100),
        lastModifiedBy int,
        primary key (id)
    );

    create table dbo.processed_changeentity (
       changeentity int,
        lastprocessedlsn varbinary(255),
        primary key (changeentity)
    );

    create table dbo.purchaseorder (
       id int identity not null,
        lastModified datetime2,
        clientref varchar(100),
        duration int,
        expirydate date,
        issued bit,
        issuedate date,
        rate numeric(10,2),
        regdate date,
        reqdate date,
        finalcost numeric(10,2),
        totalcost numeric(10,2),
        vatrate numeric(10,2),
        vatvalue numeric(10,2),
        accountstatus varchar(1),
        jobno varchar(30),
        pono varchar(30),
        taxable int,
        lastModifiedBy int,
        orgid int,
        createdby int,
        currencyid int,
        issueby int,
        personid int,
        addressid int,
        buspersonid int,
        jobId int,
        poOriginId bigint,
        returnaddressid int,
        statusid int,
        primary key (id)
    );

    create table dbo.purchaseorderactionlist (
       id int identity not null,
        date datetime2,
        personid int,
        activityid int,
        orderid int,
        statusid int,
        primary key (id)
    );

    create table dbo.purchaseorderitem (
       id int identity not null,
        lastModified datetime2,
        finalcost numeric(10,2),
        discountrate numeric(5,2),
        discountvalue numeric(10,2),
        itemno int,
        partofbaseunit bit,
        quantity int,
        totalcost numeric(10,2),
        accountsapproved tinyint,
        cancelled tinyint,
        costtypeid int,
        deliverydate date,
        description varchar(1000),
        goodsapproved tinyint,
        receiptstatus varchar(255),
        received tinyint,
        lastModifiedBy int,
        orgid int,
        internalinvoiceid int,
        nominalid int,
        orderid int,
        supplierinvoiceid int,
        primary key (id)
    );

    create table dbo.purchaseorderitemnote (
       noteid int identity not null,
        lastModified datetime2,
        active tinyint,
        deactivatedon datetime2,
        label varchar(50),
        note nvarchar(2000),
        publish tinyint,
        seton datetime2,
        lastModifiedBy int,
        deactivatedby int,
        setby int,
        id int,
        primary key (noteid)
    );

    create table dbo.purchaseorderitemprogressaction (
       id int identity not null,
        accountsapproved tinyint,
        cancelled tinyint,
        date datetime,
        description varchar(200),
        goodsapproved tinyint,
        receiptstatus int,
        personid int,
        itemid int,
        primary key (id)
    );

    create table dbo.purchaseorderjobitem (
       id int identity not null,
        lastModified datetime2,
        lastModifiedBy int,
        jobitemid int,
        poitemid int,
        primary key (id)
    );

    create table dbo.purchaseordernote (
       noteid int identity not null,
        lastModified datetime2,
        active tinyint,
        deactivatedon datetime2,
        label varchar(50),
        note nvarchar(2000),
        publish tinyint,
        seton datetime2,
        lastModifiedBy int,
        deactivatedby int,
        setby int,
        id int,
        primary key (noteid)
    );

    create table dbo.purchaseorderstatus (
       statusid int identity not null,
        defaultstatus bit,
        description varchar(200),
        name varchar(100),
        approved bit,
        depttype int,
        issued bit,
        oncancel bit,
        oncomplete bit,
        oninsert bit,
        onissue bit,
        requiresattention bit,
        statusorder int,
        potype varchar(255),
        nextid int,
        primary key (statusid)
    );

    create table dbo.purchaseorderstatusdescriptiontranslation (
       statusid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (statusid, locale, translation)
    );

    create table dbo.purchaseorderstatusnametranslation (
       statusid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (statusid, locale, translation)
    );

    create table dbo.queuedcalibration (
       id int identity not null,
        active tinyint,
        certid int,
        printnow tinyint,
        reprint tinyint,
        timecompleted datetime,
        timequeued datetime,
        printedbyid int,
        queuedcalid int,
        primary key (id)
    );

    create table dbo.quickpurchaseorder (
       id int identity not null,
        lastModified datetime2,
        consolidated tinyint,
        date datetime2,
        description varchar(1000),
        jobno varchar(30),
        pono varchar(30),
        lastModifiedBy int,
        orgid int,
        personid int,
        primary key (id)
    );

    create table dbo.quotation (
       id int identity not null,
        lastModified datetime2,
        clientref varchar(100),
        duration int,
        expirydate date,
        issued bit,
        issuedate date,
        rate numeric(10,2),
        regdate date,
        reqdate date,
        finalcost numeric(10,2),
        totalcost numeric(10,2),
        vatrate numeric(10,2),
        vatvalue numeric(10,2),
        accepted tinyint,
        acceptedon date,
        clientacceptedon date,
        contractinvoicethreshold numeric(19,2),
        contractstartdate date,
        defaultaddmodules tinyint,
        defaultqty int,
        defaultseton datetime2,
        duedate date,
        total_headingcaltype tinyint,
        headingsorttype varchar(255),
        total_heading tinyint,
        qno varchar(30),
        total_quotecaltype tinyint,
        doc_discounts tinyint default 0,
        doc_total_headingcaltype tinyint,
        doc_total_heading tinyint,
        doc_total_quotecaltype tinyint,
        showtotalquotevalue tinyint,
        sorttype int,
        usingdefaulttermsandconditions tinyint,
        ver int,
        lastModifiedBy int,
        orgid int,
        createdby int,
        currencyid int,
        issueby int,
        personid int,
        acceptedby int,
        defaultsetby int,
        defaulttermsid int,
        statusid int,
        sourceaddressid int,
        sourcedby int,
        primary key (id)
    );

    create table dbo.quotationdoccustomtext (
       lastModified datetime2,
        body nvarchar(2000),
        subject nvarchar(100),
        quotation int,
        lastModifiedBy int,
        primary key (quotation)
    );

    create table dbo.quotationdocdefaulttext (
       id int identity not null,
        lastModified datetime2,
        body nvarchar(2000),
        locale varchar(255),
        subject nvarchar(100),
        lastModifiedBy int,
        primary key (id)
    );

    create table dbo.quotationitem (
       id int,
        lastModified datetime2,
        finalcost numeric(10,2),
        discountrate numeric(5,2),
        discountvalue numeric(10,2),
        itemno int,
        partofbaseunit bit,
        quantity int,
        totalcost numeric(10,2),
        tpcarin numeric(10,2),
        tpcarinmarkupvalue numeric(10,2),
        tpcarmarkuprate numeric(10,2),
        tpcarmarkupsrc varchar(255),
        tpcarout numeric(10,2),
        tpcaroutmarkupvalue numeric(10,2),
        tpcartotal numeric(10,2),
        inspection numeric(10,2),
        plantno varchar(50),
        lastModifiedBy int,
        baseid int,
        calcost_id int,
        caltype_caltypeid int,
        heading_headingid int,
        plantid int,
        modelid int,
        purchasecost_id int,
        quoteid int,
        servicetypeid int,
        primary key (id)
    );

    create table dbo.quotationrequest (
       id int identity not null,
        lastModified datetime2,
        addr1 varchar(50),
        addr2 varchar(50),
        calibrationtype varchar(30),
        clientorderno varchar(100),
        clientref varchar(100),
        company varchar(100),
        contact varchar(100),
        country varchar(50),
        county varchar(30),
        detailssource varchar(255),
        duedate date,
        email varchar(200),
        fax varchar(50),
        logsource varchar(255),
        loggedon datetime2,
        phone varchar(50),
        postcode varchar(10),
        quotetype varchar(255),
        reqdate date,
        requestinfo varchar(1000),
        requestno varchar(20),
        status varchar(255),
        town varchar(255),
        lastModifiedBy int,
        orgid int,
        coid int,
        personid int,
        loggedby int,
        quoteid int,
        primary key (id)
    );

    create table dbo.quotecaldefaults (
       id int identity not null,
        conditionid int,
        quotationid int,
        primary key (id)
    );

    create table dbo.quotecaltypedefault (
       id int identity not null,
        caltype_caltypeid int,
        quotation_id int,
        primary key (id)
    );

    create table dbo.quoteheading (
       headingid int identity not null,
        lastModified datetime2,
        headingdescription varchar(200),
        headingname varchar(50),
        headingno int,
        systemdefault tinyint,
        lastModifiedBy int,
        quoteid int,
        primary key (headingid)
    );

    create table dbo.quoteitemnote (
       noteid int identity not null,
        lastModified datetime2,
        active tinyint,
        deactivatedon datetime2,
        label varchar(50),
        note nvarchar(2000),
        publish tinyint,
        seton datetime2,
        lastModifiedBy int,
        deactivatedby int,
        setby int,
        id int,
        primary key (noteid)
    );

    create table dbo.quotenote (
       noteid int identity not null,
        lastModified datetime2,
        active tinyint,
        deactivatedon datetime2,
        label varchar(50),
        note nvarchar(2000),
        publish tinyint,
        seton datetime2,
        lastModifiedBy int,
        deactivatedby int,
        setby int,
        quoteid int,
        primary key (noteid)
    );

    create table dbo.quotestatusgroupmember (
       id int identity not null,
        groupid int,
        statusid int,
        primary key (id)
    );

    create table dbo.rate (
       id int identity not null,
        active tinyint,
        description varchar(500),
        name varchar(100),
        rate numeric(10,2),
        seton datetime,
        baseid int,
        personid int,
        targetid int,
        primary key (id)
    );

    create table dbo.recall (
       id int identity not null,
        lastModified datetime2,
        date date,
        datefrom date,
        dateto date,
        recallno varchar(10),
        lastModifiedBy int,
        orgid int,
        coid int,
        primary key (id)
    );

    create table dbo.recallcompanyconfiguration (
       company_coid int,
        active bit default 0,
        attachmenttype varchar(255),
        customtext nvarchar(2000),
        futuremonths int,
        pastdue bit default 1,
        periodstart varchar(255),
        subjectkey varchar(255),
        templatetype varchar(255),
        primary key (company_coid)
    );

    create table dbo.recalldetail (
       id int identity not null,
        lastModified datetime2,
        error tinyint,
        ignoredallonjob tinyint,
        ignoredonsetting tinyint,
        numberofitems int,
        pathtofile varchar(200),
        recallerror varchar(255),
        recallsendstatus varchar(255),
        recallsendtype varchar(255),
        recalltype varchar(255),
        lastModifiedBy int,
        addrid int,
        personid int,
        recallid int,
        primary key (id)
    );

    create table dbo.recallitem (
       id int identity not null,
        lastModified datetime2,
        excludefromnotification tinyint,
        lastModifiedBy int,
        personid int,
        plantid int,
        recallid int,
        primary key (id)
    );

    create table dbo.recallnote (
       noteid int identity not null,
        lastModified datetime2,
        active tinyint,
        deactivatedon datetime2,
        label varchar(50),
        note nvarchar(2000),
        publish tinyint,
        seton datetime2,
        lastModifiedBy int,
        deactivatedby int,
        setby int,
        recallid int,
        primary key (noteid)
    );

    create table dbo.recallresponsenote (
       noteid int identity not null,
        lastModified datetime2,
        comment varchar(1000),
        seton datetime2,
        lastModifiedBy int,
        statusid int,
        recallitemid int,
        setbyid int,
        primary key (noteid)
    );

    create table dbo.recallresponsestatus (
       statusid int identity not null,
        statusdescription varchar(1000),
        primary key (statusid)
    );

    create table dbo.recallresponsestatustranslation (
       statusid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (statusid, locale, translation)
    );

    create table dbo.recallrule (
       id int identity not null,
        lastModified datetime2,
        active tinyint,
        calculateFromCert bit,
        calibrateOn date,
        deactivatedon date,
        period int,
        intervalunit varchar(20),
        recallrequirementtype varchar(20),
        recallruletype varchar(20),
        seton date,
        updateInstrument bit,
        lastModifiedBy int,
        coid int,
        personid int,
        deactivatedby int,
        plantid int,
        modelid int,
        servicetypeid int,
        setby int,
        subdivid int,
        primary key (id),
        check (([period]>=(1)))
    );

    create table dbo.regularexpression (
       id int identity not null,
        regex varchar(100),
        sc int,
        primary key (id)
    );

    create table dbo.repaircompletionreport (
       id int identity not null,
        lastModified datetime2,
        adjustmentperformed varchar(255),
        externalCompletionComments varchar(255),
        internalCompletionComments varchar(255),
        rcrnumber varchar(30),
        repaircompletiondate datetime2,
        validated bit,
        validatedon datetime,
        lastModifiedBy int,
        validatedby int,
        primary key (id)
    );

    create table dbo.repairinspectionreport (
       id int identity not null,
        lastModified datetime2,
        clientdescription varchar(255),
        clientinstructions varchar(255),
        completedbytechnician bit,
        completedbytechnicianon datetime,
        creationdate datetime2,
        estimatedreturndate datetime2,
        externalinstructions varchar(255),
        internaldescription varchar(255),
        internalinspection varchar(255),
        internalinstructions varchar(255),
        internaloperations varchar(255),
        leadtime int,
        manufacturerwarranty bit,
        misuse bit,
        misusecomment varchar(255),
        reviewedbycsr bit,
        reviewedbycsron datetime,
        rirnumber varchar(30),
        trescalwarranty bit,
        validatedbymanager bit,
        validatedbymanageron datetime,
        lastModifiedBy int,
        orgid int,
        createdby int,
        csr int,
        jobitemid int,
        manager int,
        repaircompletionreportid int,
        technician int,
        primary key (id)
    );

    create table dbo.repeatschedule (
       id int identity not null,
        lastModified datetime2,
        active tinyint,
        createdon datetime2,
        deactivatedon datetime2,
        notes varchar(1000),
        scheduletypeid int,
        lastModifiedBy int,
        orgid int,
        addressid int,
        personid int,
        createdbyid int,
        deactivatedbyid int,
        transoptid int,
        primary key (id)
    );

    create table dbo.requirement (
       reqid int identity not null,
        lastModified datetime2,
        created datetime2,
        deleted tinyint,
        deletedon datetime2,
        label varchar(50),
        requirement nvarchar(2000),
        lastModifiedBy int,
        personid int,
        deletebyid int,
        jobitemid int,
        primary key (reqid)
    );

    create table dbo.reversetraceabilitysettings (
       id int identity not null,
        reversetraceability tinyint,
        startdate date,
        subdivid int,
        primary key (id)
    );

    create table dbo.rolegroup (
       roleId int,
        groupId int
    );

    create table dbo.saledisc (
       code varchar(4),
        percentage numeric(2,0),
        userid varchar(30),
        primary key (code)
    );

    create table dbo.salescategory (
       id int identity not null,
        lastModified datetime2,
        active bit,
        lastModifiedBy int,
        primary key (id)
    );

    create table dbo.salescategorytranslation (
       id int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (id, locale, translation)
    );

    create table dbo.schedule (
       scheduleid int identity not null,
        lastModified datetime2,
        createdon date,
        repeatschedule tinyint,
        scheduledate date,
        schedulesourceid int,
        schedulestatusid int,
        scheduletypeid int,
        lastModifiedBy int,
        orgid int,
        addressid int,
        contactid int,
        createdby int,
        transportoptionid int,
        primary key (scheduleid)
    );

    create table dbo.scheduleddelivery (
       id int identity not null,
        scheduleddeliverystatusid int,
        deliveryid int,
        scheduleid int,
        primary key (id)
    );

    create table dbo.scheduledquotationrequest (
       id int identity not null,
        lastModified datetime2,
        cancelled tinyint,
        locale varchar(255),
        processed tinyint,
        processedon datetime,
        requestedon datetime2,
        requesttype varchar(255),
        requestscope varchar(255),
        strategy varchar(100),
        lastModifiedBy int,
        orgid int,
        caltypeid int,
        quoteid int,
        quotepersonid int,
        requestby int,
        primary key (id)
    );

    create table dbo.scheduledquotationrequestdaterange (
       requestid int,
        finishdate date,
        includeempty bit default 0,
        startdate date,
        primary key (requestid)
    );

    create table dbo.scheduledtask (
       id int identity not null,
        active tinyint,
        classname varchar(250),
        description varchar(250),
        lastrunmessage varchar(300),
        lastrunsuccess tinyint,
        lastruntime datetime2,
        methodname varchar(100),
        quartzjobname varchar(100),
        taskname varchar(50),
        primary key (id)
    );

    create table dbo.scheduleequipment (
       type varchar(31),
        id int identity not null,
        lastModified datetime2,
        commentdesc varchar(250),
        faultdesc varchar(250),
        faulty tinyint,
        ponumber varchar(60),
        supportdesc varchar(250),
        supported tinyint,
        turn varchar(255),
        qty int,
        lastModifiedBy int,
        caltypeid int,
        scheduleid int,
        modelid int,
        plantid int,
        primary key (id)
    );

    create table dbo.schedulenote (
       noteid int identity not null,
        lastModified datetime2,
        active tinyint,
        deactivatedon datetime2,
        label varchar(50),
        note nvarchar(2000),
        publish tinyint,
        seton datetime2,
        lastModifiedBy int,
        deactivatedby int,
        setby int,
        scheduleid int,
        primary key (noteid)
    );

    create table dbo.secretkey (
       keyvalue varbinary(255),
        primary key (keyvalue)
    );

    create table dbo.servicecapability (
       id int identity not null,
        lastModified datetime2,
        checksheet varchar(255),
        costtypeid int,
        lastModifiedBy int,
        orgid int,
        calibrationprocessid int,
        calibrationtypeid int,
        capabilityId int,
        modelid int,
        workinstructionid int,
        primary key (id)
    );

    create table dbo.servicetype (
       servicetypeid int identity not null,
        canbeperformedbyclient bit,
        displaycolour varchar(10),
        displaycolourfasttrack varchar(10),
        domaintype int,
        longname varchar(100),
        orderby int,
        repair bit,
        shortname varchar(10),
        primary key (servicetypeid)
    );

    create table dbo.servicetypelongnametranslation (
       servicetypeid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (servicetypeid, locale, translation)
    );

    create table dbo.servicetypeshortnametranslation (
       servicetypeid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (servicetypeid, locale, translation)
    );

    create table dbo.standardsusageanalysis (
       id int identity not null,
        finishdate date,
        startdate date,
        statusid int,
        certificateid int,
        instid int,
        primary key (id)
    );

    create table dbo.standardused (
       id int identity not null,
        lastModified datetime2,
        calpercentage int,
        recalldate date,
        lastModifiedBy int,
        calibration int,
        inst int,
        primary key (id)
    );

    create table dbo.stategrouplink (
       id int identity not null,
        groupid int,
        type varchar(255),
        stateid int,
        primary key (id)
    );

    create table dbo.subdiv (
       subdivid int identity not null,
        lastModified datetime2,
        acccontact int,
        active tinyint,
        analyticalCenter varchar(255),
        createdate date,
        deactreason varchar(255),
        deacttime datetime2,
        fiscalidentifier varchar(255),
        formerid varchar(50),
        siretNumber varchar(14),
        subdivcode varchar(3),
        subname varchar(75),
        lastModifiedBy int,
        coid int,
        defaddrid int,
        defpersonid int,
        primary key (subdivid)
    );

    create table dbo.subdiv_contract (
       contractid int,
        subdivid int,
        primary key (contractid, subdivid)
    );

    create table dbo.subdivinstructionlink (
       id int identity not null,
        lastModified datetime2,
        issubdivinstruction bit,
        lastModifiedBy int,
        orgid int,
        businesssubdivid int,
        instructionid int,
        subdivid int,
        primary key (id)
    );

    create table dbo.subdivsettingsforallocatedsubdiv (
       id int identity not null,
        lastModified datetime2,
        lastModifiedBy int,
        orgid int,
        businesscontactid int,
        subdivid int,
        primary key (id)
    );

    create table dbo.subscriber_queue (
       id int identity not null,
        processed bit,
        changeid int,
        subscriberid int,
        primary key (id)
    );

    create table dbo.supplier (
       supplierid int identity not null,
        active tinyint,
        allowuse tinyint,
        approvaltype int,
        chaseafterdays int,
        chaseclientonexpire tinyint,
        description varchar(150),
        doesnotexpire tinyint,
        name varchar(150),
        primary key (supplierid)
    );

    create table dbo.supplierinvoice (
       id int identity not null,
        lastModified datetime2,
        invoicedate date,
        invoicenumber varchar(100),
        paymentdate date,
        paymentterms varchar(10),
        totalgross numeric(10,2),
        totalnet numeric(10,2),
        lastModifiedBy int,
        orgid int,
        currencyid int,
        paymentmodeid int,
        purchaseorderid int,
        primary key (id)
    );

    create table dbo.supportedcurrency (
       currencyid int identity not null,
        accountscode varchar(5),
        currencycode varchar(3),
        currencyersymbol varchar(10),
        currencyname varchar(100),
        currencysymbol varchar(3),
        exchangerate numeric(20,2),
        majorpluralname nvarchar(50),
        majorsingularname nvarchar(50),
        minorexponent int,
        minorpluralname nvarchar(50),
        minorsingularname nvarchar(50),
        minorunit bit,
        orderby int,
        ratelastset datetime,
        rateset tinyint,
        symbolposition varchar(255),
        ratelastsetby int,
        primary key (currencyid)
    );

    create table dbo.supportedcurrencyratehistory (
       id int identity not null,
        datefrom datetime2,
        dateto datetime2,
        rate numeric(10,2),
        currencyid int,
        personid int,
        primary key (id)
    );

    create table dbo.supporteddoctype (
       doctypeId int identity not null,
        extension varchar(10),
        imagepath varchar(200),
        name varchar(50),
        primary key (doctypeId)
    );

    create table dbo.syncdeliveryportal (
       syncDeliveryId int identity not null,
        lastPull datetime,
        deliveryId int,
        portalId int,
        primary key (syncDeliveryId)
    );

    create table dbo.systemcomponent (
       componentid int identity not null,
        appendtoparentpath varchar(200),
        component int,
        componentname varchar(30),
        defaultemailbodyreference varchar(100),
        defaultemailsubject varchar(200),
        emailtemplates bit default 0,
        foldersdropboundaries tinyint,
        foldersperitem tinyint,
        folderssplit tinyint,
        folderssplitrange int,
        foldersyear tinyint,
        numberedsubfolderdivider varchar(50),
        numberedsubfolders tinyint,
        prependtoid char(1),
        rootdir varchar(30),
        usingparentsettings tinyint,
        parentid int,
        primary key (componentid)
    );

    create table dbo.systemdefault (
       defaultid int,
        defaultdatatype varchar(20),
        defaultdescription varchar(200),
        defaultname varchar(50),
        defaultvalue varchar(100),
        endd int,
        groupwide bit,
        start int,
        groupid int,
        primary key (defaultid)
    );

    create table dbo.systemdefaultapplication (
       id int identity not null,
        lastModified datetime2,
        defaultvalue varchar(100),
        lastModifiedBy int,
        orgid int,
        coid int,
        personid int,
        subdivid int,
        defaultid int,
        primary key (id)
    );

    create table dbo.systemdefaultcorole (
       defaultid int,
        coroleid int
    );

    create table dbo.systemdefaultdescriptiontranslation (
       defaultid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (defaultid, locale, translation)
    );

    create table dbo.systemdefaultgroup (
       id int identity not null,
        description varchar(200),
        dname varchar(50),
        primary key (id)
    );

    create table dbo.systemdefaultgroupdescriptiontranslation (
       id int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (id, locale, translation)
    );

    create table dbo.systemdefaultgroupnametranslation (
       id int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (id, locale, translation)
    );

    create table dbo.systemdefaultnametranslation (
       defaultid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (defaultid, locale, translation)
    );

    create table dbo.systemdefaultscope (
       defaultid int,
        scope int
    );

    create table dbo.systemimage (
       id int identity not null,
        description varchar(255),
        filedata varbinary(max),
        fileName varchar(255),
        type varchar(255),
        coid int,
        primary key (id)
    );

    create table dbo.systemsubdirectory (
       dirid int identity not null,
        dirname varchar(50),
        componentid int,
        primary key (dirid)
    );

    create table dbo.taggedimage (
       id int identity not null,
        lastModified datetime2,
        tag varchar(100),
        lastModifiedBy int,
        imageid int,
        primary key (id)
    );

    create table dbo.template (
       templateid int identity not null,
        description varchar(200),
        name varchar(50),
        templatepath varchar(100),
        primary key (templateid)
    );

    create table dbo.templatepoint (
       id int identity not null,
        lastModified datetime2,
        point numeric(19,4),
        relatedpoint numeric(19,4),
        lastModifiedBy int,
        relateduomid int,
        uomid int,
        tempid int,
        primary key (id)
    );

    create table dbo.timesheetentry (
       id int identity not null,
        comment varchar(255),
        duration bigint,
        expensevalue numeric(19,2),
        fulltime bit,
        start datetime2,
        timeactivity varchar(255),
        employeeid int,
        expensecurrencyid int,
        jobid int,
        subdivid int,
        primary key (id)
    );

    create table dbo.timesheetvalidation (
       id int identity not null,
        lastModified datetime2,
        carryover bigint,
        registeredtime bigint,
        rejectionreason varchar(255),
        status int,
        validatedon datetime2,
        week int,
        workingtime bigint,
        year int,
        lastModifiedBy int,
        employeeid int,
        validatedby int,
        primary key (id)
    );

    create table dbo.title (
       id int identity not null,
        description varchar(255),
        title varchar(10),
        countryid int,
        primary key (id)
    );

    create table dbo.tpinstruction (
       id int identity not null,
        lastModified datetime2,
        active tinyint,
        instruction varchar(1000),
        showontpdelnote tinyint,
        lastModifiedBy int,
        jobitemid int,
        recordedby int,
        primary key (id)
    );

    create table dbo.tpquotation (
       id int identity not null,
        lastModified datetime2,
        clientref varchar(100),
        duration int,
        expirydate date,
        issued bit,
        issuedate date,
        rate numeric(10,2),
        regdate date,
        reqdate date,
        finalcost numeric(10,2),
        totalcost numeric(10,2),
        vatrate numeric(10,2),
        vatvalue numeric(10,2),
        defqty int,
        qno varchar(30),
        tqno varchar(30),
        lastModifiedBy int,
        orgid int,
        createdby int,
        currencyid int,
        issueby int,
        personid int,
        tpqrid int,
        status int,
        primary key (id)
    );

    create table dbo.tpquotationcaltypedefault (
       id int identity not null,
        caltypeid int,
        tpquoteid int,
        primary key (id)
    );

    create table dbo.tpquotationitem (
       id int identity not null,
        lastModified datetime2,
        finalcost numeric(10,2),
        discountrate numeric(5,2),
        discountvalue numeric(10,2),
        itemno int,
        partofbaseunit bit,
        quantity int,
        totalcost numeric(10,2),
        carriagein numeric(10,2),
        carriageout numeric(10,2),
        inspection numeric(10,2),
        lastModifiedBy int,
        adjcost_id int,
        baseid int,
        calcost_id int,
        caltype_caltypeid int,
        modelid int,
        purchasecost_id int,
        repcost_id int,
        tpquoteid int,
        primary key (id)
    );

    create table dbo.tpquoteitemnote (
       noteid int identity not null,
        lastModified datetime2,
        active tinyint,
        deactivatedon datetime2,
        label varchar(50),
        note nvarchar(2000),
        publish tinyint,
        seton datetime2,
        lastModifiedBy int,
        deactivatedby int,
        setby int,
        id int,
        primary key (noteid)
    );

    create table dbo.tpquotelink (
       id int identity not null,
        lastModified datetime2,
        comment varchar(200),
        date date,
        lastModifiedBy int,
        personid int,
        jobitemid int,
        quoteitemid int,
        primary key (id)
    );

    create table dbo.tpquotenote (
       noteid int identity not null,
        lastModified datetime2,
        active tinyint,
        deactivatedon datetime2,
        label varchar(50),
        note nvarchar(2000),
        publish tinyint,
        seton datetime2,
        lastModifiedBy int,
        deactivatedby int,
        setby int,
        id int,
        primary key (noteid)
    );

    create table dbo.tpquoterequest (
       id int identity not null,
        lastModified datetime2,
        clientref varchar(100),
        duration int,
        expirydate date,
        issued bit,
        issuedate date,
        rate numeric(10,2),
        regdate date,
        reqdate date,
        duedate date,
        receiptdate date,
        requestno varchar(30),
        lastModifiedBy int,
        orgid int,
        createdby int,
        currencyid int,
        issueby int,
        personid int,
        jobid int,
        quoteid int,
        statusid int,
        primary key (id)
    );

    create table dbo.tpquoterequestdefaultcosttype (
       tpquotereqid int,
        costtypeid int
    );

    create table dbo.tpquoterequestedcosttype (
       itemid int,
        costtypeid int
    );

    create table dbo.tpquoterequestitem (
       id int identity not null,
        lastModified datetime2,
        partofbaseunit tinyint,
        qty int,
        referenceno varchar(50),
        lastModifiedBy int,
        baseid int,
        caltypeid int,
        modelid int,
        jobitemid int,
        quoteitemid int,
        tpquotereqid int,
        primary key (id)
    );

    create table dbo.tpquoterequestitemnote (
       noteid int identity not null,
        lastModified datetime2,
        active tinyint,
        deactivatedon datetime2,
        label varchar(50),
        note nvarchar(2000),
        publish tinyint,
        seton datetime2,
        lastModifiedBy int,
        deactivatedby int,
        setby int,
        itemid int,
        primary key (noteid)
    );

    create table dbo.tpquoterequestnote (
       noteid int identity not null,
        lastModified datetime2,
        active tinyint,
        deactivatedon datetime2,
        label varchar(50),
        note nvarchar(2000),
        publish tinyint,
        seton datetime2,
        lastModifiedBy int,
        deactivatedby int,
        setby int,
        tpquotereqid int,
        primary key (noteid)
    );

    create table dbo.tprequirement (
       id int identity not null,
        lastModified datetime2,
        adjustment tinyint,
        calibration tinyint,
        created datetime,
        investigation tinyint,
        lastupdated datetime,
        repair tinyint,
        lastModifiedBy int,
        jobitemid int,
        actionoutcomeid int,
        tpquoteid int,
        recordedbyid int,
        updatedbyid int,
        primary key (id)
    );

    create table dbo.transportmethod (
       id int identity not null,
        lastModified datetime2,
        active tinyint,
        method varchar(50),
        optionperday tinyint,
        lastModifiedBy int,
        primary key (id)
    );

    create table dbo.transportmethodtranslation (
       id int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (id, locale, translation)
    );

    create table dbo.transportoption (
       id int identity not null,
        lastModified datetime2,
        active tinyint,
        dayofweek int,
        externalref varchar(10),
        localizedname varchar(255),
        lastModifiedBy int,
        transportmethodid int,
        subdivid int,
        primary key (id)
    );

    create table dbo.transportoptionschedulerule (
       id int identity not null,
        cutofftime time,
        dayofweek varchar(255),
        transportoptionid int,
        primary key (id)
    );

    create table dbo.uom (
       id int identity not null,
        lastModified datetime2,
        active bit,
        description varchar(200),
        name varchar(50),
        ShortName nvarchar(25),
        symbol varchar(25),
        tmlid int,
        lastModifiedBy int,
        primary key (id)
    );

    create table dbo.uomnametranslation (
       id int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (id, locale, translation)
    );

    create table dbo.upcomingwork (
       id int identity not null,
        lastModified datetime2,
        active tinyint,
        description varchar(500),
        duedate date,
        startdate date,
        title varchar(100),
        lastModifiedBy int,
        orgid int,
        addedbyid int,
        coid int,
        personid int,
        deptid int,
        respuserid int,
        primary key (id)
    );

    create table dbo.upcomingworkjoblink (
       id int identity not null,
        active bit,
        jobid int,
        upcomingworkid int,
        primary key (id)
    );

    create table dbo.usergroup (
       groupid int identity not null,
        compdefault tinyint,
        description varchar(255),
        name varchar(30),
        systemdefault tinyint,
        coid int,
        primary key (groupid)
    );

    create table dbo.usergroupdescriptiontranslation (
       groupid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (groupid, locale, translation)
    );

    create table dbo.usergroupnametranslation (
       groupid int,
        locale varchar(255),
        translation nvarchar(1000),
        primary key (groupid, locale, translation)
    );

    create table dbo.usergrouppropertyvalue (
       id int identity not null,
        value varchar(255),
        property varchar(255),
        groupid int,
        primary key (id)
    );

    create table dbo.userinstructiontype (
       id int identity not null,
        instructiontypeid int,
        userprefid int,
        primary key (id)
    );

    create table dbo.usermanual (
       id int identity not null,
        filebinary varbinary(max),
        filename varchar(255),
        locale varchar(255),
        primary key (id)
    );

    create table dbo.userpreferences (
       id int identity not null,
        lastModified datetime2,
        autoprintlabel tinyint,
        defaultJobType int,
        defaultservicetypeusage tinyint,
        includeselfinreplies tinyint,
        jikeynavigation tinyint,
        mouseactive tinyint,
        mouseoverdelay int,
        navbarposition tinyint,
        scheme varchar(40),
        lastModifiedBy int,
        contactid int,
        labelprinterid int,
        primary key (id)
    );

    create table dbo.userpreferencesdefault (
       id int identity not null,
        globaldefault tinyint,
        addressid int,
        coid int,
        subdivid int,
        userpreferencesid int,
        primary key (id)
    );

    create table dbo.userrole (
       id int identity not null,
        lastModified datetime2,
        lastModifiedBy int,
        orgid int,
        role int,
        userName varchar(20),
        primary key (id)
    );

    create table dbo.users (
       username varchar(20),
        lastModified datetime2,
        last_login date,
        logincount int,
        password nvarchar(256),
        passwordInvalidated bit,
        passwordInvalidatedAt datetime2,
        registered_date date,
        securea varchar(20),
        secureq varchar(200),
        webaware tinyint,
        webtermsaccepted tinyint,
        webuser tinyint,
        lastModifiedBy int,
        personid int,
        primary key (username)
    );

    create table dbo.validatedfreerepaircomponent (
       id int identity not null,
        lastModified datetime2,
        cost numeric(19,2),
        quantity int,
        validatedstatus varchar(255),
        lastModifiedBy int,
        freerepaircomponentid int,
        validatedfreerepairoperationid int,
        primary key (id)
    );

    create table dbo.validatedfreerepairoperation (
       id int identity not null,
        lastModified datetime2,
        cost numeric(10,2),
        labourtime int,
        validatedstatus varchar(255),
        lastModifiedBy int,
        freerepairoperationid int,
        repaircompletionreportid int,
        primary key (id)
    );

    create table dbo.validfiletype (
       filetypeid int identity not null,
        extension varchar(6),
        usagetype varchar(255),
        primary key (filetypeid)
    );

    create table dbo.vatrate (
       vatcode varchar(1),
        lastModified datetime2,
        description varchar(50),
        rate numeric(4,2),
        type int,
        lastModifiedBy int,
        countryid int,
        primary key (vatcode)
    );

    create table dbo.webresource (
       type varchar(31),
        urlid int identity not null,
        lastModified datetime2,
        description varchar(200),
        url varchar(1000),
        lastModifiedBy int,
        modelid int,
        mfrid int,
        primary key (urlid)
    );

    create table dbo.workassignment (
       id int identity not null,
        depttypeid int,
        stategroupid int,
        primary key (id)
    );

    create table dbo.workinstruction (
       id int identity not null,
        lastModified datetime2,
        approvaldate datetime2,
        name varchar(100),
        title varchar(100),
        lastModifiedBy int,
        orgid int,
        approverid int,
        descid int,
        primary key (id)
    );

    create table dbo.workrequirement (
       id int identity not null,
        lastModified datetime2,
        requirement varchar(500),
        status int,
        type varchar(255),
        lastModifiedBy int,
        addressid int,
        capabilityId int,
        deptid int,
        servicetypeid int,
        wiid int,
        primary key (id)
    );


COMMIT TRAN
