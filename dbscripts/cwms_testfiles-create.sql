BEGIN TRAN
EXEC sp_msforeachtable 'ALTER TABLE ? NOCHECK CONSTRAINT all'
EXEC sp_msforeachtable 'DROP TABLE ?'
DROP SEQUENCE IF EXISTS dbo.invoice_po_item_id_sequence
DROP SEQUENCE IF EXISTS dbo.invoiceitem_id_sequence
DROP SEQUENCE IF EXISTS dbo.quotation_item_id_sequence

    create table dbo.ContractFile (
       contractId int,
        filedata varbinary(max),
        primary key (contractId)
    );

    create table dbo.ExchangeFormatFileData (
       entityId int,
        filedata varbinary(max),
        primary key (entityId)
    );

    create table dbo.ImageFileData (
       imageId int,
        filedata varbinary(max),
        primary key (imageId)
    );

    create table dbo.ImageThumbData (
       imageId int,
        thumbdata varbinary(max),
        primary key (imageId)
    );

COMMIT TRAN
