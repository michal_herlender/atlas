package org.trescal.cwms.core.po.entity.po.db;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.jobs.job.entity.po.db.PODao;
import org.trescal.cwms.core.jobs.job.projection.POProjectionDTO;
import org.trescal.cwms.tests.MvcCwmsTestClass;
import org.trescal.cwms.tests.generic.DataSetJob;
import org.trescal.cwms.tests.generic.job.TestDataJob;
import org.trescal.cwms.tests.generic.job.TestDataPo;

public class PODaoImplTest_getPOsByJobIds extends MvcCwmsTestClass {
	
	@Autowired
    private  PODao pODao;

	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetJob.getJobClientCompletedTestData();
	}
@Test
public void testMultiIjobIds() {
	
	Collection<Integer> jobIds = new ArrayList<>();
	jobIds.add(TestDataJob.ID_CLIENT_FR);
	List<POProjectionDTO> dto = this.pODao.getPOsByJobIds(jobIds);
	POProjectionDTO dto1 = dto.get(0);
	
	Assert.assertEquals(TestDataJob.ID_CLIENT_FR,dto1.getJobId());
	Assert.assertEquals(TestDataPo.PO_ID_CLIENT_FR,dto1.getPoId());
	Assert.assertEquals(TestDataPo.PO_NUMBER_CLIENT_FR,dto1.getPoNumber());
	
	Assert.assertEquals(TestDataPo.ACTIVE_CLIENT_FR,dto1.getActive());
	Assert.assertEquals(TestDataPo.JOB_DEFAULT_CLIENT_FR,dto1.getJobDefault());
	Assert.assertEquals(TestDataPo.COMMENT,dto1.getComment());
	}
}

