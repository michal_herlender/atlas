package org.trescal.cwms.core.tools.supportedlocale;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class SingleLocaleSupportedLocaleServiceImpl implements SupportedLocaleService {

    public final Locale locale;

    public SingleLocaleSupportedLocaleServiceImpl(Locale locale) {
        this.locale = locale;
    }

    @Override
    public Locale getPrimaryLocale() {
        return locale;
    }

    @Override
    public List<Locale> getAdditionalLocales() {
        return Collections.emptyList();
    }

    @Override
    public List<Locale> getSupportedLocales() {
        return Collections.singletonList(locale);
    }

    @Override
    public Locale getBestLocale(Contact contact) {
        return locale;
    }

    @Override
    public List<KeyValue<String, String>> getDTOList(List<Locale> locales, Locale displayLocale) {
        return locales.stream().map(l -> new KeyValue<>(l.toLanguageTag(), l.getDisplayName(displayLocale))).collect(Collectors.toList());
    }

    @Override
    public List<KeyValue<String, String>> getSupportedDTOList(Locale displayLocale) {
        return getDTOList(getSupportedLocales(), displayLocale);
    }

    @Override
    public Locale getBestSupportedLocale(Locale locale) {
        return this.locale;
    }
}
