package org.trescal.cwms.core.tools;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.context.support.StaticMessageSource;
import org.trescal.cwms.core.AtlasUnitTest;
import org.junit.Assert;

/*
 * Standalone JUnit test for TimeTool, doesn't require a spring context
 */
@Category(AtlasUnitTest.class)
public class TimeToolTest {
	
	public static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");  

	@BeforeClass
	public static void setup() {
		Locale locale = Locale.getDefault();
		StaticMessageSource messageSource = new StaticMessageSource();
		messageSource.addMessage("timephrase.pluralyear", locale, "{0} years ago");
		messageSource.addMessage("timephrase.pluralday", locale, "{0} days ago");
		messageSource.addMessage("timephrase.pluralhour", locale, "{0} hours ago");
		messageSource.addMessage("timephrase.pluralminute", locale, "{0} minutes ago");
		messageSource.addMessage("timephrase.pluralsec", locale, "{0} seconds ago");
		messageSource.addMessage("timephrase.singleyear", locale, "1 year ago");
		messageSource.addMessage("timephrase.singleday", locale, "1 day ago");
		messageSource.addMessage("timephrase.singlehour", locale, "1 hour ago");
		messageSource.addMessage("timephrase.singleminute", locale, "1 minute ago");
		messageSource.addMessage("timephrase.singlesec", locale, "1 second ago");
		
		TimeTool.setMessageSource(messageSource);
	}
	
	@Test
	public void testPluralYear() {
		performTest(GregorianCalendar.YEAR, 3, "3 years ago");
	}
	
	@Test
	public void testPluralDay() {
		performTest(GregorianCalendar.DATE, 3, "3 days ago");
	}
	
	@Test
	public void testPluralHour() {
		performTest(GregorianCalendar.HOUR, 3, "3 hours ago");
	}
	
	@Test
	public void testPluralMinute() {
		performTest(GregorianCalendar.MINUTE, 3, "3 minutes ago");
	}
	
	@Test
	public void testPluralSecond() {
		performTest(GregorianCalendar.SECOND, 3, "3 seconds ago");
	}
	
	@Test
	public void testSingleYear() {
		performTest(GregorianCalendar.YEAR, 1, "1 year ago");
	}
	
	@Test
	public void testSingleDay() {
		performTest(GregorianCalendar.DATE, 1, "1 day ago");
	}
	
	@Test
	public void testSingleHour() {
		performTest(GregorianCalendar.HOUR, 1, "1 hour ago");
	}
	
	@Test
	public void testSingleMinute() {
		performTest(GregorianCalendar.MINUTE, 1, "1 minute ago");
	}
	
	@Test
	public void testSingleSecond() {
		performTest(GregorianCalendar.SECOND, 1, "1 second ago");
	}
	
	@Test
	public void testNullFinishDate() {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.add(GregorianCalendar.MINUTE, -1);
		Date startDate = calendar.getTime();
		String actualText = TimeTool.calculateDateDifference(startDate, null);
		String expectedText = "1 minute ago"; 
		if (!expectedText.equals(actualText)) {
			assertFail(startDate, null, actualText, expectedText);
		}
			
	}
	
	/*
	 * Performs test between two dates differing by the specified amount, 
	 * and asserts that the formatted text is as supplied. 
	 */
	private void performTest(int gregorianCalendarField, int timeAmount, String expectedText) {
		GregorianCalendar calendar = new GregorianCalendar();
		Date startDate = calendar.getTime();
		calendar.add(gregorianCalendarField, timeAmount);
		Date finishDate = calendar.getTime();
		String actualText = TimeTool.calculateDateDifference(startDate, finishDate);
		if (!expectedText.equals(actualText)) {
			assertFail(startDate, finishDate, actualText, expectedText);
		}
	}
	
	private void assertFail(Date startDate, Date finishDate, String actualText, String expectedText) {
		StringBuffer message = new StringBuffer();
		message.append("Start Date : ");
		message.append(sdf.format(startDate));
		message.append(" Finish Date : ");
		message.append(finishDate == null ? "null" : sdf.format(finishDate));
		message.append(" Actual Text : ");
		message.append(actualText == null ? "null" : actualText);
		message.append(" Expected Text : ");
		message.append(expectedText);
		
		Assert.fail(message.toString());
	}
}
