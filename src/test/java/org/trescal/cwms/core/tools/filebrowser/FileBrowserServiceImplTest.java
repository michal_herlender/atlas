package org.trescal.cwms.core.tools.filebrowser;

import java.io.File;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;
import org.trescal.cwms.tests.MvcComponentTestClass;
import org.trescal.cwms.tests.generic.DataSetJob;
import org.trescal.cwms.tests.generic.company.TestDataCompany;
import org.trescal.cwms.tests.generic.job.TestDataJob;

public class FileBrowserServiceImplTest extends MvcComponentTestClass {

	@Autowired
	private ComponentDirectoryService componentDirectoryService;
	@Autowired
	private FileBrowserService fileBrowserService;
	@Autowired
	private SystemComponentService systemComponentService;
	
	@Override
	protected List<String> getDataSetFileNames() {
		// We want to test various scenarios of file browser handling, so using job data as includes companies too
		return DataSetJob.getJobClientCompletedTestData();
	}

	@Test
	@Transactional	// needed so that entities are in same session
	public void shouldGetFilesForRootDirectoryOfJob() {
		SystemComponent scJob = this.systemComponentService.findComponent(Component.JOB);
		
		File file = componentDirectoryService.getDirectory(scJob, TestDataJob.ID_CLIENT_FR.toString());
		// Directory should now exist and contain initialized subdirectories for job
		Assert.assertNotNull(file);
		Assert.assertTrue(file.exists());
		Assert.assertTrue(file.isDirectory());
		File[] directoryFiles = file.listFiles();
		
		String subdirName = null;
		List<String> newFiles = null;
		ResultWrapper result = this.fileBrowserService.getFilesForDirectory(subdirName, scJob.getComponentId(), TestDataJob.ID_CLIENT_FR, newFiles);
		
		Assert.assertNotNull(result);
		Assert.assertTrue(result.isSuccess());
		@SuppressWarnings("unchecked")
		List<FileWrapper> listFw = (List<FileWrapper>) result.getResults();
		Assert.assertNotNull(listFw);
		
		Assert.assertEquals(directoryFiles.length, listFw.size());
		// May need cleanup in a @BeforeClass method if we start placing files in test directories
		// Expect 6 directories
		Assert.assertEquals(6, listFw.size());
		
		Assert.assertTrue(listFw.get(0).isDirectory());
		Assert.assertEquals("Certificates", listFw.get(0).getFileName());
		
		Assert.assertTrue(listFw.get(1).isDirectory());
		Assert.assertEquals("Contract Review", listFw.get(1).getFileName());
		
		Assert.assertTrue(listFw.get(2).isDirectory());
		Assert.assertEquals("Deliveries", listFw.get(2).getFileName());
		
		Assert.assertTrue(listFw.get(3).isDirectory());
		Assert.assertEquals("Failure Reports", listFw.get(3).getFileName());
		
		Assert.assertTrue(listFw.get(4).isDirectory());
		Assert.assertEquals("Pricing", listFw.get(4).getFileName());
		
		Assert.assertTrue(listFw.get(5).isDirectory());
		Assert.assertEquals("Supplier PO", listFw.get(5).getFileName());
		
	}

	@Test
	@Transactional	// needed so that entities are in same session
	public void shouldGetFilesForRootDirectoryOfCompany() {
		SystemComponent scCompany = this.systemComponentService.findComponent(Component.COMPANY);

		File file = componentDirectoryService.getDirectory(scCompany, TestDataCompany.ID_BUSINESS_FRANCE.toString());
		// Directory should exist and contain initialized subdirectories for company
		Assert.assertNotNull(file);
		Assert.assertTrue(file.exists());
		Assert.assertTrue(file.isDirectory());
		File[] directoryFiles = file.listFiles();
		
		String subdirName = null;
		int entityId = TestDataCompany.ID_CLIENT_FRANCE;
		List<String> newFiles = null;
		ResultWrapper result = this.fileBrowserService.getFilesForDirectory(subdirName, scCompany.getComponentId(), entityId, newFiles);
		
		Assert.assertNotNull(result);
		Assert.assertTrue(result.isSuccess());
		@SuppressWarnings("unchecked")
		List<FileWrapper> listFileWrappers = (List<FileWrapper>) result.getResults();
		Assert.assertNotNull(listFileWrappers);
		
		Assert.assertEquals(directoryFiles.length, listFileWrappers.size());
		// May need cleanup in a @BeforeClass method if we start placing files in test directories
		// Currently expected - one directory with "Portal Downloads"
		Assert.assertEquals(1, listFileWrappers.size());
		FileWrapper fw = listFileWrappers.get(0);
		
		Assert.assertTrue(fw.isDirectory());
		Assert.assertEquals("Portal Downloads", fw.getFileName());
	}
}
