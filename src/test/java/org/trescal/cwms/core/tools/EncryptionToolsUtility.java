package org.trescal.cwms.core.tools;

import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * Sample class for encrypting + encoding / decoding and decrypting data as used in sharing file paths for download
 * 
 * Feel free to change / modify anything in this test class as it's a standalone main() class unused in Atlas or the test framework.
 */
public class EncryptionToolsUtility {

	// The following define strings to decode (URL decode) and unencrypt (using our encryption code)
	private static String DECODE_INPUT_1 = "cyF5RnvCz27xhuywtGXY3gxOYVX5Q0UoF9OsE4%2BWjwQm%2ByKNi7RhX%2BnSe1LZ9fbPDI0CBLW3JfoEgodphHQ9tZlFu79hP9jtu%2BQ5ceXPwFEV8JXS%2BIkFaeAZ5vDvBeRQOJWp%2FtzfOmQxwHwk11C%2Fl1kOvERUpRDUUiXZsrZulCc%3D";
	
	private static String ENCODE_INPUT_1 = "C:\\Users\\galen\\fileserver\\atlas\\Jobs\\FR005\\VND\\JI\\20038931\\Pricing\\Costing FR005-VND-JI-20038931 vers 1 rev 1.pdf";
	
	public static void main(String[] args) {
		decodeAndDecrypt(DECODE_INPUT_1);
		
		String result = encryptAndEncode(ENCODE_INPUT_1);
		decodeAndDecrypt(result);
	}
	
	public static String encryptAndEncode(String input) {
		try {
			System.out.println("encryptAndEncode:");
			System.out.println(input);
			System.out.println("length = "+input.length());
			
			String encrypted = EncryptionTools.encrypt(input);
			System.out.println(encrypted);
			System.out.println("length = "+encrypted.length());
			
			String encoded = URLEncoder.encode(encrypted, "UTF-8");
			System.out.println(encoded);
			System.out.println("length = "+encoded.length());
			System.out.println();
			return encoded;
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			return "";
		}
	}

	public static String decodeAndDecrypt(String input) {
		try {
			// Decode from UTF-8 and unencrypt:
			System.out.println("decodeAndUnencrypt:");
			System.out.println(input);
			System.out.println("length = "+input.length());
			String decoded = URLDecoder.decode(input, "UTF-8");
			System.out.println(decoded);
			System.out.println("decoded = "+decoded.length());
			String unencrypted = EncryptionTools.decrypt(decoded);
			System.out.println(unencrypted);
			System.out.println("length = "+unencrypted.length());
			System.out.println();
			return unencrypted;
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			return "";
		}
	}


	public static void decrypt(String input) throws Exception {
		String unencrypted = EncryptionTools.decrypt(input);
		System.out.println(unencrypted);
	}

}
