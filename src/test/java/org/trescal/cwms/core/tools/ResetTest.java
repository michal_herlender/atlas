package org.trescal.cwms.core.tools;

import org.junit.Ignore;
import org.junit.jupiter.api.Test;
import org.trescal.cwms.tests.MvcRestTestClass;

import java.util.Collections;
import java.util.List;

/**
 * Single test method that will inherently just reset the database (via Reset.xml)
 * for development / evaluation purposes
 */
@Ignore
public class ResetTest extends MvcRestTestClass {

	@Override
	protected List<String> getDataSetFileNames() {
		return Collections.emptyList();
	}

	@Test
	public void performResetTest() {
		// Intentionally blank
	}
	
}
