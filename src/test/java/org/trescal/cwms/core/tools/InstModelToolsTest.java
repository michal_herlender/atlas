package org.trescal.cwms.core.tools;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocales;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetInstrument;
import org.trescal.cwms.tests.generic.instrument.TestDataInstrumentModel;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Locale;

public class InstModelToolsTest extends MvcRestTestClass {
	
	public static final String PATH_TOOLS = "cwms/tools/"; 

	public static final String FILE_INST_MODEL_TOOLS = PATH_TOOLS + "InstModelToolsTest.xml";
	
	public static final int ID_SALESCATEGORY_CALIPER_300_TO_500MM = 1104;
	public static final int ID_SALESCATEGORY_CALIPER_2000MM_UP = 1107;
	
	public static final Logger logger = LoggerFactory.getLogger(InstModelToolsTest.class);
	
	@Autowired
	private InstrumentModelService instModelService;

	@Override
	protected List<String> getDataSetFileNames() {
		List<String> result = DataSetInstrument.getInstrumentModelTestData();
		result.add(FILE_INST_MODEL_TOOLS);
		return result;
	}
	
	@Test
	@Transactional
	public void testModelNameCaliperMin() {
		InstrumentModel instModel = this.instModelService.get(TestDataInstrumentModel.ID_SALESCATEGORY_CALIPER_TO_300MM);
		Locale locale = SupportedLocales.ENGLISH_GB;
		// Set sales category to null, to enable translation generation on model
		instModel.setSalesCategory(null);
		String actualTranslation = InstModelTools.modelName(instModel, locale, locale);
		logger.info(instModel.getModelid()+" : "+actualTranslation);
		String expected = "CALIPER Length max < 300 mm";
		Assertions.assertEquals(expected, actualTranslation);
		
	}
	
	@Test
	@Transactional
	public void testModelNameCaliperMid() {
		InstrumentModel instModel = this.instModelService.get(ID_SALESCATEGORY_CALIPER_300_TO_500MM);
		Locale locale = SupportedLocales.ENGLISH_GB;
		// Set sales category to null, to enable translation generation on model
		instModel.setSalesCategory(null);
		String actualTranslation = InstModelTools.modelName(instModel, locale, locale);
		logger.info(instModel.getModelid()+" : "+actualTranslation);
		String expected = "CALIPER 300 mm <= Length max < 500 mm";
		Assertions.assertEquals(expected, actualTranslation);
	}
	
	@Test
	@Transactional
	public void testModelNameCaliperMax() {
		InstrumentModel instModel = this.instModelService.get(ID_SALESCATEGORY_CALIPER_2000MM_UP);
		Locale locale = SupportedLocales.ENGLISH_GB;
		// Set sales category to null, to enable translation generation on model
		instModel.setSalesCategory(null);
		String actualTranslation = InstModelTools.modelName(instModel, locale, locale);
		logger.info(instModel.getModelid()+" : "+actualTranslation);
		String expected = "CALIPER 2000 mm <= Length max";
		Assertions.assertEquals(expected, actualTranslation);
	}

}
