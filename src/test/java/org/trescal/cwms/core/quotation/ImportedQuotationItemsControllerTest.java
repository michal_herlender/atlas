package org.trescal.cwms.core.quotation;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.trescal.cwms.core.quotation.controller.ImportedQuotationItemsSynthesisController;
import org.trescal.cwms.core.quotation.form.ImportedQuotationItemsSynthesisForm;
import org.trescal.cwms.core.quotation.form.ImportedQuotationItemsSynthesisForm.ImportedQuotationItemsSynthesisRowDTO;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.tests.MvcCwmsTestClass;
import org.trescal.cwms.tests.generic.DataSetCompany;
import org.trescal.cwms.tests.generic.DataSetExchangeFormat;
import org.trescal.cwms.tests.generic.DataSetInstrument;
import org.trescal.cwms.tests.generic.address.TestDataAddress;
import org.trescal.cwms.tests.generic.company.TestDataCompany;
import org.trescal.cwms.tests.generic.company.TestDataContact;
import org.trescal.cwms.tests.generic.company.TestDataSubdiv;
import org.trescal.cwms.tests.generic.exchangeformat.TestDataExchangeFormat;
import org.trescal.cwms.tests.generic.instrument.TestDataInstrument;
import org.trescal.cwms.tests.generic.pricing.TestDataQuotation;

public class ImportedQuotationItemsControllerTest extends MvcCwmsTestClass {
	public static final String EXCEL_FILE_VALID = "cwms/quotation/importQuotationItemsTest.xlsx";
	public static final String USERNAME = "technician";

	protected List<String> getDataSetFileNames() {
		List<String> result = DataSetCompany.getCompanyClientTestData();
		result.add(DataSetExchangeFormat.FILE_QUOTATION_ITEMS_IMPORTATION);
		result.add(DataSetInstrument.FILE_IMPORTED_INSTRUMENT);
		return result;
	}

	@Test
	public void testSuccess_ValidData() throws Exception {

		MultiValueMap<String, String> inputPayload = new LinkedMultiValueMap<>();
		inputPayload.add("subdivid", Integer.toString(TestDataSubdiv.ID_BUSINESS_FRANCE));
		inputPayload.add("personid", Integer.toString(TestDataContact.ID_BUSINESS_FRANCE));
		inputPayload.add("addrid", Integer.toString(TestDataAddress.ID_ADDRESS_RUNGIS));
		inputPayload.add("exchangeFormatId", Integer.toString(TestDataExchangeFormat.ID_QUOTATION_ITEMS_IMPORTATION));
		inputPayload.add("quotationId", Integer.toString(TestDataQuotation.ID_ACTIVE_FRANCE));
		inputPayload.add("coid", Integer.toString(TestDataCompany.ID_BUSINESS_FRANCE));

		InputStream is = getClass().getResourceAsStream("classpath:test-data/" + EXCEL_FILE_VALID);
		MockMultipartFile file = new MockMultipartFile("uploadFile", "imoprtQuotationItemsTest.xlsx", "text/plain",
				IOUtils.toByteArray(is));

		this.mockMvc.perform(fileUpload("/importquotationitems.htm").file(file).params(inputPayload))
				.andExpect(model().hasNoErrors()).andExpect(model().errorCount(0)).andDo(print());
	}

	/**
	 * The instrument is not available in the company
	 */
	@Test
	public void test_InstrumentNotAvailable() throws Exception {

		ImportedQuotationItemsSynthesisRowDTO i = new ImportedQuotationItemsSynthesisRowDTO();
		i.setPlantNo(TestDataInstrument.PLANT_NO1);
		i.setFinalPrice(99.99);
		i.setServiceType("REP");

		List<ImportedQuotationItemsSynthesisRowDTO> rows = new ArrayList<>();
		rows.add(i);

		ImportedQuotationItemsSynthesisForm a = new ImportedQuotationItemsSynthesisForm();
		a.setCoid(TestDataCompany.ID_CLIENT_FRANCE);
		a.setAddressId(TestDataAddress.ID_ADDRESS_RUNGIS);
		a.setBusinessCompanyId(1);
		a.setContactId(TestDataContact.ID_BUSINESS_FRANCE);
		a.setExchangeFormatId(TestDataExchangeFormat.ID_QUOTATION_ITEMS_IMPORTATION);
		a.setQuotationId(TestDataQuotation.ID_ACTIVE_FRANCE);
		a.setSubdivId(TestDataSubdiv.ID_BUSINESS_FRANCE);
		a.setRows(rows);

		Map<String, Object> sessionAttributes = new HashMap<>();
		sessionAttributes.put(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME);
		KeyValue<Integer, String> subdivDto = new KeyValue<>(1, "Madrid");
		sessionAttributes.put(Constants.SESSION_ATTRIBUTE_SUBDIV, subdivDto);

		MvcResult result = this.mockMvc
				.perform(post("/importedquotationitemssynthesis.htm?save=Submit").sessionAttrs(sessionAttributes)
						.flashAttr("form", a).param("id", Integer.toString(TestDataQuotation.ID_ACTIVE_FRANCE)))
				.andDo(print()).andReturn();

		BindingResult formBindingResult = (BindingResult) result.getModelAndView().getModel()
				.get(BindingResult.MODEL_KEY_PREFIX + ImportedQuotationItemsSynthesisController.FORM);

		assertThat(formBindingResult.getFieldError().getCode(), containsString("error.instrument.notavailable"));
	}

	/**
	 * The instrument is not found in database
	 */
	@Test
	public void test_InstrumentNotFound() throws Exception {

		ImportedQuotationItemsSynthesisRowDTO i = new ImportedQuotationItemsSynthesisRowDTO();
		i.setPlantId(0);
		i.setPlantNo(TestDataInstrument.PLANT_NO1);
		i.setFinalPrice(99.99);
		i.setServiceType("REP");

		List<ImportedQuotationItemsSynthesisRowDTO> rows = new ArrayList<>();
		rows.add(i);

		ImportedQuotationItemsSynthesisForm a = new ImportedQuotationItemsSynthesisForm();
		a.setCoid(TestDataCompany.ID_CLIENT_FRANCE);
		a.setAddressId(TestDataAddress.ID_ADDRESS_RUNGIS);
		a.setBusinessCompanyId(1);
		a.setContactId(TestDataContact.ID_BUSINESS_FRANCE);
		a.setExchangeFormatId(TestDataExchangeFormat.ID_QUOTATION_ITEMS_IMPORTATION);
		a.setQuotationId(TestDataQuotation.ID_ACTIVE_FRANCE);
		a.setSubdivId(TestDataSubdiv.ID_BUSINESS_FRANCE);
		a.setRows(rows);

		Map<String, Object> sessionAttributes = new HashMap<>();
		sessionAttributes.put(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME);
		KeyValue<Integer, String> subdivDto = new KeyValue<>(1, "Madrid");
		sessionAttributes.put(Constants.SESSION_ATTRIBUTE_SUBDIV, subdivDto);

		MvcResult result = this.mockMvc
				.perform(post("/importedquotationitemssynthesis.htm?save=Submit").sessionAttrs(sessionAttributes)
						.flashAttr("form", a).param("id", Integer.toString(TestDataQuotation.ID_ACTIVE_FRANCE)))
				.andDo(print()).andReturn();

		BindingResult formBindingResult = (BindingResult) result.getModelAndView().getModel()
				.get(BindingResult.MODEL_KEY_PREFIX + ImportedQuotationItemsSynthesisController.FORM);

		assertThat(formBindingResult.getFieldError().getCode(), containsString("error.instrument.plantid.notfound"));

	}

	/**
	 * The instrument is duplicated in the file
	 */
	@Test
	public void test_InstrumentIsDuplicateInFile() throws Exception {

		ImportedQuotationItemsSynthesisRowDTO i = new ImportedQuotationItemsSynthesisRowDTO();
		i.setPlantNo(TestDataInstrument.PLANT_NO1);
		i.setFinalPrice(99.99);
		i.setServiceType("REP");
		i.setServiceTypeId(6);

		ImportedQuotationItemsSynthesisRowDTO j = new ImportedQuotationItemsSynthesisRowDTO();
		j.setPlantNo(TestDataInstrument.PLANT_NO1);
		j.setFinalPrice(99.99);
		j.setServiceType("REP");
		j.setServiceTypeId(6);

		List<ImportedQuotationItemsSynthesisRowDTO> rows = new ArrayList<>();
		rows.add(i);
		rows.add(j);

		ImportedQuotationItemsSynthesisForm a = new ImportedQuotationItemsSynthesisForm();
		a.setCoid(2);
		a.setAddressId(2);
		a.setBusinessCompanyId(2);
		a.setContactId(2);
		a.setExchangeFormatId(TestDataExchangeFormat.ID_QUOTATION_ITEMS_IMPORTATION);
		a.setQuotationId(2);
		a.setSubdivId(2);
		a.setRows(rows);

		Map<String, Object> sessionAttributes = new HashMap<>();
		sessionAttributes.put(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME);
		KeyValue<Integer, String> subdivDto = new KeyValue<>(1, "Madrid");
		sessionAttributes.put(Constants.SESSION_ATTRIBUTE_SUBDIV, subdivDto);

		MvcResult result = this.mockMvc
				.perform(post("/importedquotationitemssynthesis.htm?save=Submit").sessionAttrs(sessionAttributes)
						.flashAttr("form", a).param("id", Integer.toString(TestDataQuotation.ID_ACTIVE_FRANCE)))
				.andDo(print()).andReturn();

		BindingResult formBindingResult = (BindingResult) result.getModelAndView().getModel()
				.get(BindingResult.MODEL_KEY_PREFIX + ImportedQuotationItemsSynthesisController.FORM);

		assertThat(formBindingResult.getFieldError().getCode(),
				containsString("exchangeformat.import.itemstatus.duplicatedintable"));

	}
	
	/**
	 * The instrument is already on quote
	 */
	@Test
	public void test_InstrumentIsOnQuote() throws Exception {

		ImportedQuotationItemsSynthesisRowDTO i = new ImportedQuotationItemsSynthesisRowDTO();
		i.setPlantId(2);
		i.setPlantNo(TestDataInstrument.PLANT_NO2);
		i.setFinalPrice(99.99);
		i.setServiceType("ET-AC-LA");
		i.setServiceTypeId(1);

		List<ImportedQuotationItemsSynthesisRowDTO> rows = new ArrayList<>();
		rows.add(i);
		
		ImportedQuotationItemsSynthesisForm a = new ImportedQuotationItemsSynthesisForm();
		a.setCoid(2);
		a.setAddressId(2);
		a.setBusinessCompanyId(2);
		a.setContactId(2);
		a.setExchangeFormatId(TestDataExchangeFormat.ID_QUOTATION_ITEMS_IMPORTATION);
		a.setQuotationId(2);
		a.setSubdivId(2);
		a.setRows(rows);

		Map<String, Object> sessionAttributes = new HashMap<>();
		sessionAttributes.put(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME);
		KeyValue<Integer, String> subdivDto = new KeyValue<>(1, "Madrid");
		sessionAttributes.put(Constants.SESSION_ATTRIBUTE_SUBDIV, subdivDto);

		MvcResult result = this.mockMvc
				.perform(post("/importedquotationitemssynthesis.htm?save=Submit").sessionAttrs(sessionAttributes)
						.flashAttr("form", a).param("id", Integer.toString(TestDataQuotation.ID_ACTIVE_FRANCE)))
				.andDo(print()).andReturn();

		BindingResult formBindingResult = (BindingResult) result.getModelAndView().getModel()
				.get(BindingResult.MODEL_KEY_PREFIX + ImportedQuotationItemsSynthesisController.FORM);

		assertThat(formBindingResult.getFieldError().getCode(),
				containsString("error.instrument.onquote"));

	}
}
