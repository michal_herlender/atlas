package org.trescal.cwms.core.misc.controller;

import java.util.Locale;

/**
 * Interface to be able to globally set locale for testing purposes
 * @author galen
 *
 */
public interface SettableLocaleResolver {
	void setLocale(Locale locale);
}
