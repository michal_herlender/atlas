package org.trescal.cwms.core.misc.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.i18n.LocaleContext;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.i18n.TimeZoneAwareLocaleContext;
import org.springframework.lang.Nullable;
import org.springframework.web.servlet.LocaleContextResolver;
import org.springframework.web.util.WebUtils;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocales;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.util.Locale;
import java.util.TimeZone;

/*
 * @author Galen
 * 2015-1-16
 * Sets a single controllable locale for unit testing purposes
 */
public class MockLocaleResolver implements LocaleContextResolver, SettableLocaleResolver {

    private Locale locale;

    private static Logger logger = LoggerFactory.getLogger(MockLocaleResolver.class);

    public MockLocaleResolver(Locale locale) {
        this.locale = locale;
        LocaleContextHolder.setLocale(locale);
        logger.info("Initialized MockLocaleResolver with specified locale " + locale.toString());
    }
	public MockLocaleResolver() {
		// For XML / autowiring purposes - assume en_GB locale
		this.locale = SupportedLocales.ENGLISH_GB;
		LocaleContextHolder.setLocale(locale);
		logger.info("Initialized MockLocaleResolver with default locale "+locale.toString());
	}

	@Override
	public Locale resolveLocale(HttpServletRequest arg0) {
		return locale;
	}

	@Override
	public void setLocale(HttpServletRequest arg0, HttpServletResponse arg1,
			Locale arg2) {
        setLocale(arg2);
    }

    @Override
    public void setLocale(Locale locale) {
        this.locale = locale;
        LocaleContextHolder.setLocale(locale);
        logger.info("Updated MockLocaleResolver with specified locale " + locale.toString());
    }

    public Locale getMainLocale() {
        return locale;
    }

    @Override
    public @NotNull LocaleContext resolveLocaleContext(final @NotNull HttpServletRequest request) {
        return new TimeZoneAwareLocaleContext() {
            @Override
            public Locale getLocale() {
                Locale locale = (Locale) WebUtils.getSessionAttribute(request, Constants.SESSION_ATTRIBUTE_LOCALE);
                if (locale == null) {
                    locale = getMainLocale();
                }
                return locale;
            }

            @Override
            @Nullable
            public TimeZone getTimeZone() {
                TimeZone timeZone = (TimeZone) WebUtils.getSessionAttribute(request, Constants.SESSION_ATTRIBUTE_TIME_ZONE);
                if (timeZone == null) {
                    timeZone = TimeZone.getTimeZone("Pacific/Pago_Pago");
                }
                return timeZone;
            }
        };
    }

    @Override
    public void setLocaleContext(@NotNull HttpServletRequest request, @Nullable HttpServletResponse response,
                                 @Nullable LocaleContext localeContext) {

        Locale locale = null;
        TimeZone timeZone = null;
        if (localeContext != null) {
            locale = localeContext.getLocale();
            if (localeContext instanceof TimeZoneAwareLocaleContext) {
                timeZone = ((TimeZoneAwareLocaleContext) localeContext).getTimeZone();
            }
        }
        WebUtils.setSessionAttribute(request, Constants.SESSION_ATTRIBUTE_LOCALE, locale);
        WebUtils.setSessionAttribute(request, Constants.SESSION_ATTRIBUTE_TIME_ZONE, timeZone);
    }
}
