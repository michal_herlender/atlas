package org.trescal.cwms.core.certificate.entity.certificate.db;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.jobs.certificate.dto.CertificateProjectionDTO;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateDao;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.tests.MvcCwmsTestClass;
import org.trescal.cwms.tests.generic.DataSetDelivery;
import org.trescal.cwms.tests.generic.certificate.TestDataCertificate;
import org.trescal.cwms.tests.generic.job.TestDataJobItem;

public class CertificateDaoImplTest_getCertificatesForJobItemIds extends MvcCwmsTestClass {

	@Autowired
    private  CertificateDao certificateDao;
	
	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetDelivery.getDeliveryNoteClientTestData();
	}
	@Test
	public void testjobItemIds() {
	
		Collection<Integer> jobItemIds = new ArrayList<>();
		jobItemIds.add(TestDataJobItem.ID_CLIENT_USA);
	 
		List<CertificateProjectionDTO> dto = this.certificateDao.getCertificatesForJobItemIds(jobItemIds);
		CertificateProjectionDTO dto1 = dto.get(0);
	
		Assert.assertEquals(TestDataJobItem.ID_CLIENT_USA,dto1.getJobItemId());
		Assert.assertEquals(TestDataCertificate.CERT_ID,dto1.getCertId());
		Assert.assertEquals(TestDataCertificate.CERT_NO,dto1.getCertno());
		Assert.assertEquals(TestDataCertificate.CERT_TYPE,dto1.getType().CERT_CLIENT.name());
		Assert.assertNotEquals(dto1.getCertDate(), TestDataCertificate.CERT_DATE);
	
		SimpleDateFormat df = DateTools.df_ISO8601;
	
		Assert.assertNotNull(dto1.getCalDate());
		Assert.assertEquals(df.format(dto1.getCalDate()), TestDataCertificate.CAL_DATE);
	
}
}
