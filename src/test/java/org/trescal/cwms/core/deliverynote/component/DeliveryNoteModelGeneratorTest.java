package org.trescal.cwms.core.deliverynote.component;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryDTO;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItemDTO;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocales;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetDelivery;
import org.trescal.cwms.tests.generic.company.TestDataCompany;
import org.trescal.cwms.tests.generic.company.TestDataSubdiv;
import org.trescal.cwms.tests.generic.delivery.TestDataDelivery;
import org.trescal.cwms.tests.generic.delivery.TestDataDeliveryItem;
import org.trescal.cwms.tests.generic.delivery.TestDataDeliveryItemAccessory;

import java.util.List;

public class DeliveryNoteModelGeneratorTest extends MvcRestTestClass {

	@Autowired
	private DeliveryNoteModelGenerator generator;
	
	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetDelivery.getDeliveryNoteClientTestData();
	}
	
	@Test
	public void testDeliveryNoteModel() throws Exception {
		
		// Here we test the delivery 150 (US client delivery) as it has an attached certificate
		DeliveryNoteModel model = generator.getModel(150, SupportedLocales.ENGLISH_GB);
		
		Assertions.assertNotNull(model);
		Assertions.assertNotNull(model.getAccessoryDtos());
		Assertions.assertNotNull(model.getCertificateDtos());
		Assertions.assertNotNull(model.getDeliveryDto());
		Assertions.assertNotNull(model.getDeliveryItemDtos());
		Assertions.assertNotNull(model.getInstructionDtos());
		
		DeliveryDTO delDto = model.getDeliveryDto();
		Assertions.assertEquals(TestDataCompany.ID_BUSINESS_USA, delDto.getSourceCompanyId());
		Assertions.assertEquals(TestDataSubdiv.ID_BUSINESS_USA, delDto.getSourceSubdivId());

		Assertions.assertEquals(TestDataDeliveryItem.COUNT_ITEMS_PER_DELIVERY, model.getDeliveryItemDtos().size());
				int index = 0;
		for (DeliveryItemDTO diDto : model.getDeliveryItemDtos()) {
			Assertions.assertNotNull( diDto,"Null diDto at index "+index);
			Assertions.assertNotNull( diDto.getJobItem(),"Null diDto::jobItem at index "+index);
			Assertions.assertNotNull( diDto.getJobItem().getInstrument(),"Null diDto::jobItem::inst at index "+index);
			
			index++;
		}

		Assertions.assertEquals(TestDataDeliveryItemAccessory.COUNT_ACCESSORIES_PER_DELIVERY, model.getAccessoryDtos().size());
		Assertions.assertEquals(TestDataDelivery.COUNT_CERTIFICATES_PER_DELIVERY, model.getCertificateDtos().size());
		Assertions.assertEquals(TestDataDelivery.COUNT_INSTRUCTIONS_PER_DELIVERY, model.getInstructionDtos().size());
		
	}

}
