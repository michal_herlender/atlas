package org.trescal.cwms.core.deliverynote.controller;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.hamcrest.core.IsNull;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.tests.MvcCwmsTestClass;
import org.trescal.cwms.tests.generic.DataSetJob;

import java.time.LocalDateTime;
import java.util.List;
import java.util.TimeZone;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
public class ViewDeliveryNoteControllerTest extends MvcCwmsTestClass {

    @Test
    public void delivery_wiht_no_items_but_with_receipt_date() throws Exception {
        mockMvc.perform(
                        get("/viewdelnote.htm").param("delid", "110")
                                .accept(MediaType.TEXT_HTML)
                                .sessionAttr(Constants.SESSION_ATTRIBUTE_SUBDIV, new KeyValue<>(510, "SUB"))
                                .sessionAttr(Constants.SESSION_ATTRIBUTE_COMPANY, new KeyValue<>(510, "SUB"))
                                .sessionAttr(Constants.SESSION_ATTRIBUTE_TIME_ZONE, TimeZone.getTimeZone("Pacific/Pago_Pago"))
                ).andExpect(status().isOk())
                .andExpect(model().attribute("minDestinationReceiptDate", IsNull.nullValue()))
                .andExpect(model().attribute("maxDestinationReceiptDate", LocalDateTime.parse("2015-10-16T04:23")))
                .andExpect(model().attribute("form", hasProperty("destinationReceiptDate", equalTo(LocalDateTime.parse("2015-09-24T21:12")))));
    }

    @Test
    public void delivery_with_items_destinationReceiptDate_lower_bound_should_be_taken_from_first_item() throws Exception {
        mockMvc.perform(
                        get("/viewdelnote.htm").param("delid", "111")
                                .accept(MediaType.TEXT_HTML)
                                .sessionAttr(Constants.SESSION_ATTRIBUTE_SUBDIV, new KeyValue<>(510, "SUB"))
                                .sessionAttr(Constants.SESSION_ATTRIBUTE_COMPANY, new KeyValue<>(510, "SUB"))
                                .sessionAttr(Constants.SESSION_ATTRIBUTE_TIME_ZONE, TimeZone.getTimeZone("Pacific/Pago_Pago"))
                ).andExpect(status().isOk())
                .andExpect(model().attribute("minDestinationReceiptDate", LocalDateTime.parse("2015-10-10T15:44")))
                .andExpect(model().attribute("maxDestinationReceiptDate", LocalDateTime.parse("2015-10-16T04:23")))
                .andExpect(model().attribute("form", hasProperty("destinationReceiptDate", equalTo(LocalDateTime.parse("2015-10-16T04:23")))))
        ;
    }

    @Override
    protected List<String> getDataSetFileNames() {
        val l = DataSetJob.getJobClientCompletedTestData();
        l.add("cwms/deliverynote/controller/ViewDeliveryNoteController.xml");
        return l;
    }
}