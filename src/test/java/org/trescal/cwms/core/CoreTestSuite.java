package org.trescal.cwms.core;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.trescal.cwms.core.certificate.entity.certificate.db.CertificateDaoImplTest_getCertificatesForJobItemIds;
import org.trescal.cwms.core.company.entity.address.controller.AddressLookupControllerTest;
import org.trescal.cwms.core.company.entity.address.db.AddressDaoImplTest_getAddressProjectionDTOs;
import org.trescal.cwms.core.company.entity.company.db.CompanyDaoImplTest_getDTOList;
import org.trescal.cwms.core.company.entity.contact.controller.ContactLookupControllerTest;
import org.trescal.cwms.core.company.entity.contact.db.ContactDaoImplTest;
import org.trescal.cwms.core.company.entity.contact.db.ContactDaoImplTest_getContactProjectionDTOs;
import org.trescal.cwms.core.company.entity.location.contoller.LocationLookupControllerTest;
import org.trescal.cwms.core.company.entity.status.db.StatusDaoImplTest_getAllProjectionStatuss;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivDaoImplTest_getSubdivProjectionDTOs;
import org.trescal.cwms.core.company.entity.vatrate.VatRateFormatterTest;
import org.trescal.cwms.core.company.entity.vatrate.db.VatRateDaoImplTest;
import org.trescal.cwms.core.contract.entity.contract.db.ContractServiceTests;
import org.trescal.cwms.core.deliverynote.component.DeliveryNoteModelGeneratorTest;
import org.trescal.cwms.core.instrument.controller.ImportInstrumentControllerTest;
import org.trescal.cwms.core.instrument.controller.ImportedInstrumentsSynthesisControllerTest;
import org.trescal.cwms.core.instrument.controller.PermitedCalibrationExtensionControllerTest;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumDaoImplTest_getInstrumentProjectionDTOs;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumentPercentageServiceTest;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumentUtilsTest;
import org.trescal.cwms.core.instrument.entity.permitedcalibrationextension.PermitedCalibrationExtensionVerificationTests;
import org.trescal.cwms.core.instrumentmodel.controller.description.SearchDescriptionTagsTest;
import org.trescal.cwms.core.instrumentmodel.controller.domain.AllDomainJsonTest;
import org.trescal.cwms.core.instrumentmodel.controller.domain.EditDomainTest;
import org.trescal.cwms.core.instrumentmodel.controller.family.AllFamilyJsonTest;
import org.trescal.cwms.core.instrumentmodel.controller.family.EditFamilyTest;
import org.trescal.cwms.core.instrumentmodel.controller.salescategory.AllSalesCategoryJsonTest;
import org.trescal.cwms.core.instrumentmodel.controller.salescategory.EditSalesCategoryTest;
import org.trescal.cwms.core.invoice.entity.PaymentTermTests;
import org.trescal.cwms.core.jobs.calibration.controller.ImportCalibrationsControllerTest;
import org.trescal.cwms.core.jobs.calibration.controller.ImportedOperationsSynthesisControllerTest;
import org.trescal.cwms.core.jobs.calibration.controller.StandardsUsedForJobItemControllerTest;
import org.trescal.cwms.core.jobs.job.controller.AddJobFromFileControllerTest;
import org.trescal.cwms.core.jobs.job.controller.BpoAjaxControllerTest;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobDaoImplTest_queryJobJPA;
import org.trescal.cwms.core.jobs.jobitem.controller.JobItemActivityControllerTest;
import org.trescal.cwms.core.jobs.jobitem.db.JobItemDaoImplTest_findMostRecentJobItemDTO;
import org.trescal.cwms.core.jobs.jobitem.db.JobItemDaoImplTest_getJobItemProjectionDTOs;
import org.trescal.cwms.core.jobs.jobitem.db.JobItemDaoImplTest_getJobItemProjectionDTOsByJobIds;
import org.trescal.cwms.core.jobs.jobitem.db.JobItemDaoImplTest_getJobItemsFromJobs;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.db.JobItemActivityServiceTest;
import org.trescal.cwms.core.jobs.prebooking.AddPrebookingJobControllerTest;
import org.trescal.cwms.core.jobs.prebooking.EFFileSynthesisControllerTest;
import org.trescal.cwms.core.jobs.quotation.controller.JobToQuotationControllerTest;
import org.trescal.cwms.core.login.entity.userpreferencesdefault.UserPreferencesDefaultTest;
import org.trescal.cwms.core.note.entity.deliveryitemotes.db.NoteProjectionDaoImplTest_getDeliveryItemNotes;
import org.trescal.cwms.core.note.entity.deliveryitemotes.db.NoteProjectionDaoImplTest_getDeliveryNotes;
import org.trescal.cwms.core.po.entity.po.db.PODaoImplTest_getPOsByJobIds;
import org.trescal.cwms.core.pricing.entity.purchaseorderstatus.db.PurchaseOrderDaoImplTest_findAllPurchaseOrderStatusRequiringAttentionProjection;
import org.trescal.cwms.core.pricing.entity.purchaseorderstatus.db.PurchaseOrderDaoImplTest_getAllPurchaseOrdersAtStatusUsingProjection;
import org.trescal.cwms.core.pricing.entity.purchaseorderstatus.db.PurchaseOrderDaoImplTest_getByAccountStatusUsingProjection;
import org.trescal.cwms.core.pricing.entity.quote.db.QuotationDaoImplTest_getAllQuotesInStatusGroupNew;
import org.trescal.cwms.core.pricing.entity.quote.db.QuotationDaoImplTest_getMostRecentIssuedQuotationsNew;
import org.trescal.cwms.core.pricing.lookup.PriceLookupServiceImplTest;
import org.trescal.cwms.core.procedure.controller.ViewWorkInstructionControllerTest;
import org.trescal.cwms.core.procedure.entity.capability.db.CapabilityDaoImplTest_findAllForSubdiv;
import org.trescal.cwms.core.procedure.entity.capability.db.CapabilityDaoImplTest_getCapabilityProjectionDtos;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.db.CapabilityAuthorizationServiceImplTest_authorizedFor;
import org.trescal.cwms.core.quotation.ImportedQuotationItemsControllerTest;
import org.trescal.cwms.core.system.controller.CreateEmailFormBackerTest;
import org.trescal.cwms.core.system.entity.instruction.db.InstructionServiceTest;
import org.trescal.cwms.core.system.entity.supportedcurrency.CurrencyValueFormatterTest;
import org.trescal.cwms.core.tools.InstModelToolsTest;
import org.trescal.cwms.core.tools.ResetTest;
import org.trescal.cwms.core.tools.TimeToolTest;
import org.trescal.cwms.core.user.PasswordValidationTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({

	// not included (separate test suites) : Batch, BIRT, CDC, RESTful tests
	// tests are otherwise sorted by package
	
	// core.certificate
	CertificateDaoImplTest_getCertificatesForJobItemIds.class,
	
	// core.company.entity.*
	AddressLookupControllerTest.class,
	AddressDaoImplTest_getAddressProjectionDTOs.class,
	CompanyDaoImplTest_getDTOList.class,
	ContactLookupControllerTest.class,
	ContactDaoImplTest_getContactProjectionDTOs.class,
	ContactDaoImplTest.class,
	LocationLookupControllerTest.class,
	StatusDaoImplTest_getAllProjectionStatuss.class,
	SubdivDaoImplTest_getSubdivProjectionDTOs.class,
	VatRateDaoImplTest.class,
	VatRateFormatterTest.class,
	
	// core.contract.* tests
	ContractServiceTests.class,

    // core.deliverynote.* tests
    DeliveryNoteModelGeneratorTest.class,

    // core.instrument.* tests
    ImportInstrumentControllerTest.class,
    ImportedInstrumentsSynthesisControllerTest.class,
    PermitedCalibrationExtensionControllerTest.class,
    InstrumDaoImplTest_getInstrumentProjectionDTOs.class,
    InstrumentUtilsTest.class,
    PermitedCalibrationExtensionVerificationTests.class,
    InstrumentPercentageServiceTest.class,

    // core.instrumentmodel.* tests
    SearchDescriptionTagsTest.class,
    AllDomainJsonTest.class,
    EditDomainTest.class,
    AllFamilyJsonTest.class,
    EditFamilyTest.class,
    AllSalesCategoryJsonTest.class,
    EditSalesCategoryTest.class,
	
	// core.invoice.*
	PaymentTermTests.class,

	// core.jobs.calibration.*
	ImportCalibrationsControllerTest.class,
	ImportedOperationsSynthesisControllerTest.class,
	StandardsUsedForJobItemControllerTest.class,

	// core.jobs.job.*
	AddJobFromFileControllerTest.class,
	JobDaoImplTest_queryJobJPA.class,
	BpoAjaxControllerTest.class,

	// core.jobs.jobitem.*
	JobItemDaoImplTest_findMostRecentJobItemDTO.class,
	JobItemDaoImplTest_getJobItemProjectionDTOs.class,
	JobItemDaoImplTest_getJobItemProjectionDTOsByJobIds.class,
	JobItemDaoImplTest_getJobItemsFromJobs.class,

	// core.jobs.prebooking.*
	AddPrebookingJobControllerTest.class,
	EFFileSynthesisControllerTest.class,
	
	// core.jobs.quotation.*
	JobToQuotationControllerTest.class,

    // core.login.*
    UserPreferencesDefaultTest.class,

    // core.note.*
    NoteProjectionDaoImplTest_getDeliveryItemNotes.class,
    NoteProjectionDaoImplTest_getDeliveryNotes.class,

    // core.po.*
    PODaoImplTest_getPOsByJobIds.class,

    // core.pricing.*
    PurchaseOrderDaoImplTest_findAllPurchaseOrderStatusRequiringAttentionProjection.class,
    PurchaseOrderDaoImplTest_getAllPurchaseOrdersAtStatusUsingProjection.class,
    PurchaseOrderDaoImplTest_getByAccountStatusUsingProjection.class,
    QuotationDaoImplTest_getAllQuotesInStatusGroupNew.class,
    QuotationDaoImplTest_getMostRecentIssuedQuotationsNew.class,
    PriceLookupServiceImplTest.class,

    // core.procedure.*
    ViewWorkInstructionControllerTest.class,
    CapabilityDaoImplTest_findAllForSubdiv.class,
    CapabilityDaoImplTest_getCapabilityProjectionDtos.class,
    CapabilityAuthorizationServiceImplTest_authorizedFor.class,

    // core.quotation.*
    ImportedQuotationItemsControllerTest.class,

    // core.system.*
    CreateEmailFormBackerTest.class,
    CurrencyValueFormatterTest.class,
    InstructionServiceTest.class,

    // core.tools
    InstModelToolsTest.class,
    ResetTest.class,
    TimeToolTest.class,

    // core.password
    PasswordValidationTest.class,

	// core.jobs.jobitem
	JobItemActivityServiceTest.class,
	
})
public class CoreTestSuite {

}
