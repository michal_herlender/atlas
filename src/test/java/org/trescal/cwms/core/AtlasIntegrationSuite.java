package org.trescal.cwms.core;

import java.net.URISyntaxException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.platform.suite.api.IncludeTags;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.platform.suite.api.Suite;
import org.trescal.cwms.tests.tools.ExecuteSqlSpringContext;

/**
 * This suite allows us to run just unit tests
 * where the test class contains the "org.trescal.cwms.core.AtlasIntegrationRestTest" tag
 */

@Suite
@SelectPackages("org.trescal.cwms")
@IncludeTags("org.trescal.cwms.core.AtlasIntegrationRestTest")

public class AtlasIntegrationSuite {

	@BeforeAll
	public static void setup() throws URISyntaxException{
		new ExecuteSqlSpringContext().execute();
	}
}
