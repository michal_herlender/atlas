package org.trescal.cwms.core.login.entity.userpreferencesdefault;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.login.entity.userpreferencesdefault.db.UserPreferencesDefaultService;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetCompany;
import org.trescal.cwms.tests.generic.address.TestDataAddress;

import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class UserPreferencesDefaultTest extends MvcRestTestClass {
	
	public static int ID_UPD_GLOBAL = 1;
	public static int ID_UPD_FR_COMPANY = 5100;
	public static int ID_UPD_FR_SUBDIV = 5101;
	public static int ID_UPD_FR_ADDRESS = 5102;
	public static int ID_UPD_DE_COMPANY = 5200;
	public static int ID_UPD_DE_SUBDIV = 5201;
	public static int ID_UPD_DE_ADDRESS = 5202;
	
	@Autowired
	public UserPreferencesDefaultService updService;

	@Override
	protected List<String> getDataSetFileNames() {
		List<String> result = DataSetCompany.getCompanyBusinessTestData();
		result.add("cwms/login/UserPreferencesDefaultTestData.xml");
		return result;
	}
	
	@Test
	public void testRetrievalAddress() {
		UserPreferencesDefault upd = this.updService.getBestDefaultForAddress(TestDataAddress.ID_ADDRESS_DARMSTADT);
		Assertions.assertEquals(ID_UPD_DE_ADDRESS, upd.getId());
	}
	
	@Test
	public void testRetrievalGlobal() {
		UserPreferencesDefault upd = this.updService.getBestDefaultForAddress(TestDataAddress.ID_ADDRESS_CASABLANCA);
		Assertions.assertEquals(ID_UPD_GLOBAL, upd.getId());
	}
	
	@Test
	public void testComparator() {
		TreeSet<UserPreferencesDefault> sortedSet = new TreeSet<>(new UserPreferencesDefaultComparator());
		sortedSet.add(this.updService.get(ID_UPD_FR_SUBDIV));
		sortedSet.add(this.updService.get(ID_UPD_GLOBAL));
		sortedSet.add(this.updService.get(ID_UPD_FR_ADDRESS));
		sortedSet.add(this.updService.get(ID_UPD_FR_COMPANY));
		
		Integer[] actualIds = sortedSet.stream().map(upd -> upd.getId()).collect(Collectors.toList()).toArray(new Integer[] {});
		Integer[] expectedIds = new Integer[] {ID_UPD_GLOBAL, ID_UPD_FR_COMPANY, ID_UPD_FR_SUBDIV, ID_UPD_FR_ADDRESS};

		Assertions.assertArrayEquals(expectedIds, actualIds);
	}

	@Test
	public void testQuery() {
		List<UserPreferencesDefault> results = this.updService.getAllForAddress(TestDataAddress.ID_ADDRESS_RUNGIS);
		
		Integer[] actualIds = results.stream().map(upd -> upd.getId()).sorted().collect(Collectors.toList()).toArray(new Integer[] {});
		Integer[] expectedIds = new Integer[] {ID_UPD_GLOBAL, ID_UPD_FR_COMPANY, ID_UPD_FR_SUBDIV, ID_UPD_FR_ADDRESS};

		Assertions.assertArrayEquals(expectedIds, actualIds);
	}
	
}
