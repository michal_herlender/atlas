package org.trescal.cwms.core.user;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.tests.MvcRestTestClass;

import java.util.List;

/**
 * Runs different test cases to test passwordValid function used to valid a
 * password
 */
public class PasswordValidationTest extends MvcRestTestClass {
	@Autowired
	private UserService userService;
	@Autowired
	private MessageSource messages;

	@Override
	protected List<String> getDataSetFileNames() {
		return null;
	}

	/**
	 * The size of password is less than 8
	 */
	@Test
	public void testPasswordSize() {
		ResultWrapper result = userService.passwordValid("a");
		String errorMessage = result.getMessage();
		Assertions.assertEquals(result.isSuccess(), false);
		Assertions.assertEquals(errorMessage, this.messages.getMessage("error.password.characters.size", null,
				"Your password should be 8 to 20 characters long!", null));
	}

	/**
	 * The password doesn't have any uppercase letter
	 */
	@Test
	public void testPasswordUpperCaseLetter() {
		ResultWrapper result = userService.passwordValid("testtest");
		String errorMessage = result.getMessage();
		Assertions.assertEquals(result.isSuccess(), false);
		Assertions.assertEquals(errorMessage, this.messages.getMessage("error.password.characters.uppercase", null,
				"Your password must contain at least one upperCase letter!", null));
	}

	/**
	 * The password doesn't have any lowercase letter
	 */
	@Test
	public void testPasswordLowerCaseLetter() {
		ResultWrapper result = userService.passwordValid("TESTTEST");
		String errorMessage = result.getMessage();
		Assertions.assertEquals(result.isSuccess(), false);
		Assertions.assertEquals(errorMessage, this.messages.getMessage("error.password.characters.lowercase", null,
				"Your password must contain at least one lowerCase letter!", null));
	}

	/**
	 * The password doesn't have any digit
	 */
	@Test
	public void testPasswordDigit() {
		ResultWrapper result = userService.passwordValid("TestTest");
		String errorMessage = result.getMessage();
		Assertions.assertEquals(result.isSuccess(), false);
		Assertions.assertEquals(errorMessage, this.messages.getMessage("error.password.characters.digit", null,
				"Your password must contain at least one digit!", null));
	}

	/**
	 * The password doesn't have any special character (#?!@$%^&*-)
	 */
	@Test
	public void testPasswordSpecialCharacter() {
		ResultWrapper result = userService.passwordValid("TestTest1");
		String errorMessage = result.getMessage();
		Assertions.assertEquals(result.isSuccess(), false);
		Assertions.assertEquals(errorMessage, this.messages.getMessage("error.password.characters.special", null,
				"Your password must contain at least one special character!", null));
	}

	/**
	 * The password is valid (strong)
	 */
	@Test
	public void testValidPassword() {
		ResultWrapper result = userService.passwordValid("TestTest1!");
		Assertions.assertEquals(result.isSuccess(), true);
	}
}
