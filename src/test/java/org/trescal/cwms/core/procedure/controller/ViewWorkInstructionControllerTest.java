package org.trescal.cwms.core.procedure.controller;

import static org.hamcrest.Matchers.hasProperty;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.trescal.cwms.tests.MvcCwmsTestClass;

public class ViewWorkInstructionControllerTest extends MvcCwmsTestClass {
	
	public static final String DATA_SET = "cwms/ViewWorkInstructionControllerTest.xml"; 
	
	@Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }
	
	@Test
	public void testGet() throws Exception {
		
		this.mockMvc.perform(get("/viewworkinstruction/versions").param("wid", "1"))
			.andExpect(status().isOk())
			.andExpect(forwardedUrl("/WEB-INF/jsp/trescal/core/procedure/viewworkinstruction/versions.jsp"))
			.andExpect(model().size(1))
		    .andExpect(model().attributeExists("form"))
		    .andExpect(model().attribute("form", hasProperty("currentVersion")));
		
	}


}
