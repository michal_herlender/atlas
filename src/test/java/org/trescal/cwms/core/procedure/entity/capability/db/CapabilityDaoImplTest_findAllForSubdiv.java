package org.trescal.cwms.core.procedure.entity.capability.db;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.procedure.dto.CapabilityProjectionDTO;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityDao;
import org.trescal.cwms.tests.MvcCwmsTestClass;
import org.trescal.cwms.tests.generic.DataSetCapability;
import org.trescal.cwms.tests.generic.company.TestDataSubdiv;
import org.trescal.cwms.tests.generic.instrument.TestDataProcs;

import java.util.List;

public class CapabilityDaoImplTest_findAllForSubdiv extends MvcCwmsTestClass {
    @Autowired
    private CapabilityDao capabilityDao;

    @Override
    protected List<String> getDataSetFileNames() {
        return DataSetCapability.getCapabilityData();
    }

    @Test
    public void testsubdivIds() {

        Integer subdivId = TestDataSubdiv.ID_BUSINESS_MOROCCO;
        List<CapabilityProjectionDTO> dtos = this.capabilityDao.findAllForSubdiv(subdivId);
        CapabilityProjectionDTO dto1 = dtos.get(0);

        Assert.assertEquals(dto1.getProcId(), TestDataProcs.PROCS_ID_5301);
        Assert.assertEquals(dto1.getName(), TestDataProcs.NAME_5301);
        Assert.assertEquals(dto1.getReference(), TestDataProcs.REFERENCE_5301);

    }

}
