package org.trescal.cwms.core.procedure.entity.capability.db;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.procedure.dto.CapabilityProjectionDTO;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityDao;
import org.trescal.cwms.tests.MvcCwmsTestClass;
import org.trescal.cwms.tests.generic.DataSetCapability;
import org.trescal.cwms.tests.generic.instrument.TestDataProcs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CapabilityDaoImplTest_getCapabilityProjectionDtos extends MvcCwmsTestClass {

    @Autowired
    private CapabilityDao capabilityDao;

    @Override
    protected List<String> getDataSetFileNames() {
        return DataSetCapability.getCapabilityData();
    }

    @Test
    public void testCapabilityIds() {

        Collection<Integer> capabilityIds = new ArrayList<>();
        capabilityIds.add(TestDataProcs.PROCS_ID_5101);
        List<CapabilityProjectionDTO> dtos = this.capabilityDao.getCapabilityProjectionDtos(capabilityIds);

        Assert.assertEquals(dtos.size(), 1);
        CapabilityProjectionDTO dto1 = dtos.get(0);
		
		Assert.assertEquals(dto1.getProcId(),TestDataProcs.PROCS_ID_5101);
		Assert.assertEquals(dto1.getName(),TestDataProcs.NAME_5101);
		Assert.assertEquals(dto1.getReference(),TestDataProcs.REFERENCE_5101);
		
	}
}
