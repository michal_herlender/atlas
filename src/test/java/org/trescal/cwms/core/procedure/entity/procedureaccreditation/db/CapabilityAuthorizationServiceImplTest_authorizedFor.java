package org.trescal.cwms.core.procedure.entity.procedureaccreditation.db;

import io.vavr.control.Either;
import lombok.val;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.AccreditationLevel;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.CapabilityAuthorization;

import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@Tag("org.trescal.cwms.core.AtlasUnitTest")
public class CapabilityAuthorizationServiceImplTest_authorizedFor {

    @Mock
    CapabilityAuthorizationDao capabilityAuthorizationDao;

    @InjectMocks
    CapabilityAuthorizationService capabilityAuthorizationService = new CapabilityAuthorizationServiceImpl();

    @Test
    void authorizedFor_noAuthorizations() {
        when(capabilityAuthorizationDao.getCapabilityAuthorizations(anyInt(), anyInt(), anyInt())).thenReturn(Collections.emptyList());
        assertEquals(Either.left("empty"), capabilityAuthorizationService.authorizedFor(1, 1, 2));
    }

    @Test
    void authorizedFor_removedLevel() {
        when(capabilityAuthorizationDao.getCapabilityAuthorizations(anyInt(), anyInt(), anyInt())).thenReturn(Stream.of(authorizationForLevel(AccreditationLevel.APPROVE), authorizationForLevel(AccreditationLevel.REMOVED), authorizationForLevel(AccreditationLevel.SIGN)).collect(Collectors.toList()));
        assertEquals(Either.left("removed"), capabilityAuthorizationService.authorizedFor(1, 2, 3));
    }

    @Test
    void authorizedFor_signOverPerform() {
        when(capabilityAuthorizationDao.getCapabilityAuthorizations(anyInt(), anyInt(), anyInt())).thenReturn(Stream.of(authorizationForLevel(AccreditationLevel.SUPERVISED), authorizationForLevel(AccreditationLevel.PERFORM), authorizationForLevel(AccreditationLevel.SIGN)).collect(Collectors.toList()));
        assertEquals(Either.right(AccreditationLevel.SIGN), capabilityAuthorizationService.authorizedFor(1, 2, 3));
    }

    @Test
    void authorizedFor_approveOverSign() {
        when(capabilityAuthorizationDao.getCapabilityAuthorizations(anyInt(), anyInt(), anyInt())).thenReturn(Stream.of(authorizationForLevel(AccreditationLevel.APPROVE), authorizationForLevel(AccreditationLevel.PERFORM), authorizationForLevel(AccreditationLevel.SIGN)).collect(Collectors.toList()));
        assertEquals(Either.right(AccreditationLevel.APPROVE), capabilityAuthorizationService.authorizedFor(1, 2, 3));
    }

    private CapabilityAuthorization authorizationForLevel(AccreditationLevel level) {
        val auth = new CapabilityAuthorization();
        auth.setAccredLevel(level);
        return auth;
    }
}