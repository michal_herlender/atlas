package org.trescal.cwms.core.external.adveso.dto;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.trescal.cwms.core.AtlasUnitTest;
import org.trescal.cwms.rest.adveso.dto.AdvesoJobCostingItemDTO;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Unit test for serialization of AdvesoActivityEventDTO
 * <p>
 * Ensures that date formats are as expected by Adveso interface
 */
@Slf4j
@Category(AtlasUnitTest.class)
public class AdvesoActivityEventDTOTest {

	private final ObjectMapper mapper;

	// Using format expected by Adveso for simplicity here
//	private static SimpleDateFormat dtf = new SimpleDateFormat("dd.MM.yyyy - HH.mm.ss"); 

	private LocalDateTime testUTCLocalDateTime;
	private LocalDateTime testEurParisLocalDateTime;
	private long testUTCDateTimeAsUnixTimestamp;
//	private long testEurParisDateTimeAsUnixTimestamp;
	private LocalDate testLocalDate;

	public AdvesoActivityEventDTOTest() {
		mapper = new ObjectMapper();
		// Required for LocalDate serialization
		mapper.registerModule(new JavaTimeModule());

		// Expect as "25.12.2020 - 01.23.45"
		LocalDateTime ldt = LocalDateTime.of(2020, 12, 25, 1, 23, 45);
		ZonedDateTime utcZdt = ldt.atZone(ZoneId.of("Z"));
		ZonedDateTime eurParisZdt = ldt.atZone(ZoneId.of("Europe/Paris"));

		testUTCLocalDateTime = ldt;
		testUTCDateTimeAsUnixTimestamp = utcZdt.toInstant().toEpochMilli();
		testEurParisLocalDateTime = eurParisZdt.toLocalDateTime();
//		testEurParisDateTimeAsUnixTimestamp = Date.from(testEurParisLocalDateTime.atZone(ZoneId.of("Europe/Paris")).toInstant()).getTime();
		testLocalDate = LocalDate.of(2020, 2, 29);
	}
	
	@Test
	public void testSerializationForUtc() throws Exception {
		
		AdvesoActivityEventDTO dto = createTestDtoForUtc();
		String jsonText = mapper.writeValueAsString(dto);
		log.info(jsonText);
		
		DocumentContext jsonContext = JsonPath.parse(jsonText);
		Assert.assertEquals("29.02.2020 - 00.00.00", jsonContext.read("$.approvedDeliveryDate"));
		Assert.assertEquals("29.02.2020 - 00.00.00", jsonContext.read("$.plannedDeliveryDate"));
		Assert.assertEquals("25.12.2020 - 01.23.45", jsonContext.read("$.createdOn"));
		Assert.assertEquals(testUTCDateTimeAsUnixTimestamp, ((Long) jsonContext.read("$.createdOnNonFormatted")).longValue());
		Assert.assertEquals("25.12.2020 - 01.23.45", jsonContext.read("$.realDeliveryDate"));
		Assert.assertEquals("25.12.2020 - 01.23.45", jsonContext.read("$.receiptDate"));
		Assert.assertEquals("25.12.2020 - 01.23.45", jsonContext.read("$.startDate"));

	}

	private AdvesoActivityEventDTO createTestDtoForUtc() {
		AdvesoActivityEventDTO dto = createCommonTestDto();

		dto.setApprovedDeliveryDate(testLocalDate);
		dto.setCreatedOn(testUTCLocalDateTime);
		dto.setCreatedOnNonFormatted(Date.from(testUTCLocalDateTime.atZone(ZoneId.of("Z")).toInstant()));
		dto.setPlannedDeliveryDate(testLocalDate);
		dto.setRealDeliveryDate(testUTCLocalDateTime);
		dto.setReceiptDate(testUTCLocalDateTime);
		dto.setStartDate(testUTCLocalDateTime);
		
		return dto;
	}
	
	private AdvesoActivityEventDTO createCommonTestDto() {
		AdvesoActivityEventDTO dto = new AdvesoActivityEventDTO();
		
		List<AdvesoJobCostingItemDTO> jobCostingItems = new ArrayList<>();
		AdvesoJobCostingItemDTO jciDto = new AdvesoJobCostingItemDTO();
		jciDto.setAdjustmentcost(new BigDecimal("100.00"));
		jciDto.setCalcost(new BigDecimal("200.00"));
		jciDto.setId(8);
		jciDto.setInspectioncost(new BigDecimal("300.00"));
		jciDto.setPurchasecost(new BigDecimal("400.00"));
		jciDto.setRepaircost(new BigDecimal("500.00"));
		
		jobCostingItems.add(jciDto);
		
		dto.setActionOutComeName("aoname");
		dto.setActivityID(1);
		dto.setActivityName("aname");		
		dto.setCalVerificationStatus("cvs");
		dto.setEndStatusDescription("esd");
		dto.setEndStatusId(2);
		dto.setFormerID("fid");
		dto.setIsActionDeleted(false);
		dto.setIsDeleted(false);
		dto.setIsOnSite(false);
		dto.setItemNo("itemno");
		dto.setJobCostingFileId("jcfileid");
		dto.setJobCostingItems(jobCostingItems);
		dto.setJobCostingType("jct");
		dto.setJobItemActionId(3);
		dto.setJobItemNumber("jin");
		dto.setOldJobItemReference("ojir");
		dto.setPlantId(4);
		dto.setPlantNo("plantno");
		dto.setQueueId(5);		
		dto.setRemarks("remarks");
		dto.setServiceType("servicetype");
		dto.setServicetypeId(6);
		dto.setSubdivID(7);
		dto.setTechnicienName("technicienNotTechnician");
		
		return dto;
	}
	
	@Test
	public void testSerializationForEurParis() throws Exception {
		
		AdvesoActivityEventDTO dto = createTestDtoForEurParis();
		String jsonText = mapper.writeValueAsString(dto);
		log.info(jsonText);
		
		DocumentContext jsonContext = JsonPath.parse(jsonText);
		Assert.assertEquals("29.02.2020 - 00.00.00", jsonContext.read("$.approvedDeliveryDate"));
		Assert.assertEquals("29.02.2020 - 00.00.00", jsonContext.read("$.plannedDeliveryDate"));
		Assert.assertEquals("25.12.2020 - 01.23.45", jsonContext.read("$.createdOn"));
//		Assert.assertEquals(testEurParisDateTimeAsUnixTimestamp, ((Long) jsonContext.read("$.createdOnNonFormatted")).longValue());
		Assert.assertEquals("25.12.2020 - 01.23.45", jsonContext.read("$.realDeliveryDate"));
		Assert.assertEquals("25.12.2020 - 01.23.45", jsonContext.read("$.receiptDate"));
		Assert.assertEquals("25.12.2020 - 01.23.45", jsonContext.read("$.startDate"));

	}

	private AdvesoActivityEventDTO createTestDtoForEurParis() {
		AdvesoActivityEventDTO dto = createCommonTestDto();

		dto.setApprovedDeliveryDate(testLocalDate);
		dto.setCreatedOn(testEurParisLocalDateTime);
		dto.setCreatedOnNonFormatted(Date.from(testEurParisLocalDateTime.atZone(ZoneId.of("Europe/Paris")).toInstant()));
		dto.setPlannedDeliveryDate(testLocalDate);
		dto.setRealDeliveryDate(testEurParisLocalDateTime);
		dto.setReceiptDate(testEurParisLocalDateTime);
		dto.setStartDate(testEurParisLocalDateTime);

		return dto;
	}
}
