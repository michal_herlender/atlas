package org.trescal.cwms.core.instrumentmodel.controller.description;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.trescal.cwms.spring.model.KeyValueIntegerString;
import org.trescal.cwms.tests.MvcCwmsTestClass;

/*
 * Originally in attempting to properly order the delete operations we created a filtered data set,
 * if needed this could also be done by reading in the XSD schema.
 * This is no longer needed as we are not committing any of the data that DbUnit loads.   
 *   	String[] tableNames = new String[] {"description", "descriptiontranslation"};
 *  	IDataSet filteredDataSet = new FilteredDataSet(tableNames, fullDataSet);
 *      return filteredDataSet;
 */
public class SearchDescriptionTagsTest extends MvcCwmsTestClass {
	@Autowired
	private SearchDescriptionTags controller;

	public final static String DATA_SET = "cwms/instrumentmodel/DescriptionTags.xml";

	@Override
	protected List<String> getDataSetFileNames() {
		return Collections.singletonList(DATA_SET);
	}

	/*
	 * As this is the first MvcTestClass, checking that data is populated correctly.
	 */
	@Test
	public void testGetTagListViaController() {
		List<KeyValueIntegerString> result = controller.getTagList(null, "Probe", Locale.UK);
		Assert.assertNotNull(result);
		Assert.assertEquals(2, result.size());
	}

	/*
	 * As this is the first MvcTestClass, checking that data is populated correctly.
	 */
	@Test
	public void testGetTagListFullViaController() {
		List<KeyValueIntegerString> result = controller.getTagList(null, "", Locale.UK);
		Assert.assertNotNull(result);
		Assert.assertEquals(5, result.size());
	}

	/*
	 * See http://goessner.net/articles/JsonPath/ regarding JSONPath expressions
	 * This contains more than we normally would use just to try out matchers
	 */
	@Test
	public void testJSONSearchViaMockMvc() throws Exception {
		this.mockMvc
				.perform(get("/searchdescriptiontags.json?term=Probe")
						.accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
				.andDo(print()).andExpect(status().isOk())
				.andExpect(content().contentType("application/json;charset=UTF-8")).andExpect(jsonPath("$").exists())
				.andExpect(jsonPath("$").isArray()).andExpect(jsonPath("$[0]").exists())
				.andExpect(jsonPath("$[1]").exists()).andExpect(jsonPath("$[2]").doesNotExist())
				.andExpect(jsonPath("$[0].label").value("Probe")).andExpect(jsonPath("$[0].id").value("2"))
				.andExpect(jsonPath("$[1].label").value("Temperature Probe")).andExpect(jsonPath("$[1].id").value("6"));
	}
}