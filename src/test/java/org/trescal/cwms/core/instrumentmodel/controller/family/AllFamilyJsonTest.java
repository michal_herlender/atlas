package org.trescal.cwms.core.instrumentmodel.controller.family;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.db.InstrumentModelFamilyDao;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.db.InstrumentModelFamilyService;
import org.trescal.cwms.spring.model.InstrumentModelFamilyJsonDTO;
import org.trescal.cwms.tests.MvcCwmsTestClass;

public class AllFamilyJsonTest extends MvcCwmsTestClass {
	
	public final static String DATA_SET = "cwms/instrumentmodel/Families.xml";
	
	@Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }
	
	@Autowired
	private InstrumentModelFamilyDao instModFamDao;
	
	@Autowired
	private InstrumentModelFamilyService instModFamService;
	
	@Autowired
	private AllFamilyJson allFamilyJson;
	
	@Test
	public void instrumentModelFamilyDaoTest() {
		List<InstrumentModelFamilyJsonDTO> families = instModFamDao.getAllFamilies(Locale.UK);
		Assert.assertNotNull(families);
		Assert.assertEquals(5,families.size());	
	}
	
	@Test
	public void instrumentModelFamilyServiceTest() {
		List<InstrumentModelFamilyJsonDTO> families = instModFamService.getAllFamilies(Locale.UK);
		Assert.assertNotNull(families);
		Assert.assertEquals(5,families.size());	
	}
	
	@Test
	public void allFamilyJsonControllerTest() {
		List<InstrumentModelFamilyJsonDTO> jsonOutput = allFamilyJson.getAllFamilies(Locale.UK);
		Assert.assertNotNull(jsonOutput);
		Assert.assertEquals(5,jsonOutput.size());
	}
	
	@Test
	public void allFamilyJsonTest() throws Exception {
		this.mockMvc.perform(get("/allfamilies.json")).
                andExpect(status().isOk()).
				andDo(print()).
				andExpect(jsonPath("$").exists()).
				andExpect(jsonPath("$[0].familyid").value(4)).
				andExpect(jsonPath("$[2].name").value("Power Supply")).
				andExpect(jsonPath("$[4].active").value("Yes")).
				andExpect(jsonPath("$[5]").doesNotExist());
	}

}
