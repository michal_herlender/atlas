package org.trescal.cwms.core.instrumentmodel.controller.salescategory;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.SalesCategory;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.db.SalesCategoryDao;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.db.SalesCategoryService;
import org.trescal.cwms.spring.model.SalesCategoryJsonDTO;
import org.trescal.cwms.tests.MvcCwmsTestClass;

public class AllSalesCategoryJsonTest extends MvcCwmsTestClass{

	public final static String DATA_SET = "cwms/instrumentmodel/SalesCategories.xml";
	
	@Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }
	
	@Autowired
	private SalesCategoryDao salesCategorydao;
	@Autowired
	private SalesCategoryService salesCategoryService;
	@Autowired
	private AllSalesCategoryJson allSalesCategoryJson;
	
	@Test
	public void salesCategoryServiceDaoTest() {
		List<SalesCategory> salesCategories = salesCategorydao.getAllSalesCategories();
		Assert.assertNotNull(salesCategories);
		Assert.assertEquals(5,salesCategories.size());	
	}
	
	@Test
	public void salesCategoryServiceTest() {
		List<SalesCategoryJsonDTO> salesCategories = salesCategoryService.getAllSalesCategories(Locale.UK);
		Assert.assertNotNull(salesCategories);
		Assert.assertEquals(5,salesCategories.size());	
	}
	
	@Test
	public void allSalesCategoryJsonControllerTest() {
		List<SalesCategoryJsonDTO> jsonOutput = allSalesCategoryJson.getAllSalesCategories(Locale.UK);
		Assert.assertNotNull(jsonOutput);
		Assert.assertEquals(5,jsonOutput.size());
	}
	
	@Test
	public void allSalesCategoryJsonTest() throws Exception {
		this.mockMvc.perform(get("/allsalescategories.json")
				.accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
				.andDo(print())
				.andExpect(jsonPath("$").exists())
				.andExpect(jsonPath("$[0].id").value("1"))
				.andExpect(jsonPath("$[0].name").value("GB Test 1"));
	}
	
}
