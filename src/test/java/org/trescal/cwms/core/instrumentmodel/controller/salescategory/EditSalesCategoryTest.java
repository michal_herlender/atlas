package org.trescal.cwms.core.instrumentmodel.controller.salescategory;

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.SortedSet;
import java.util.TreeSet;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.SalesCategory;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.db.SalesCategoryDao;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.db.SalesCategoryService;
import org.trescal.cwms.core.instrumentmodel.form.EditSalesCategoryForm;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.spring.model.SalesCategoryJsonDTO;
import org.trescal.cwms.tests.MvcCwmsTestClass;

public class EditSalesCategoryTest extends MvcCwmsTestClass {

	public final static String DATA_SET = "cwms/instrumentmodel/SalesCategories.xml";
	
	@Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }
	@Autowired
	private SalesCategoryDao salesCategoryDao;

	@Autowired
	private SalesCategoryService salesCategoryService;

	@Autowired
	private EditSalesCategoryController editSalesCategoryController;

	@Test
	public void salesCategoryDaoWriteTest() {
		SalesCategory salesCategory = new SalesCategory();		
		salesCategory.setActive(true);		
		salesCategoryDao.merge(salesCategory);
		List<SalesCategory> salesCategories = (List<SalesCategory>) salesCategoryDao.getAllSalesCategories();
		Assert.assertNotNull(salesCategories);
		Assert.assertEquals(6, salesCategories.size());
	}	
	@Test
	public void salesCategoryDaoDeleteTest(){
		SalesCategory salesCategory = salesCategoryDao.find(1);
		salesCategoryDao.remove(salesCategory);	
		SalesCategory category = salesCategoryDao.find(1);
		Assert.assertNull(category);
	}
	@Test
	public void salesCategoryDaoUpdateTest() {
		SortedSet<Translation> translations = new TreeSet<Translation>();
		translations.add(new Translation(Locale.UK, "GB Test 1 Updated"));	
		SalesCategory salesCategory = salesCategoryDao.find(1);
		salesCategory.setTranslations(translations);
		salesCategoryDao.merge(salesCategory);
		
		SalesCategory updatedSalesCategory = salesCategoryDao.find(1);
		for (Translation t: updatedSalesCategory.getTranslations()) {
			if (t.getLocale().toString().equals("en_GB")) {
				Assert.assertEquals("GB Test 1 Updated", t.getTranslation());
				break;
			}
		}
	}
	@Test
	public void salesCategoryServiceWriteTest() {
		SalesCategory salesCategory = new SalesCategory();
		SortedSet<Translation> translations = new TreeSet<Translation>();
		translations.add(new Translation(Locale.UK, "GB Test 6"));
		salesCategory.setTranslations(translations);
		salesCategory.setActive(true);		
		salesCategoryService.merge(salesCategory);
		List<SalesCategoryJsonDTO> salesCategories = (List<SalesCategoryJsonDTO>) salesCategoryService.getAllSalesCategories(Locale.UK);
		Assert.assertNotNull(salesCategories);
		Assert.assertEquals(6, salesCategories.size());
	}
	
	@Test
	public void salesCategoryServiceDeleteTest(){
		SalesCategory salesCategory = salesCategoryService.get(2);
		salesCategoryService.delete(salesCategory);	
		SalesCategory category = salesCategoryService.get(2);
		Assert.assertNull(category);
	}
	@Test
	public void salesCategoryServiceUpdateTest() {
		SortedSet<Translation> translations = new TreeSet<Translation>();
		translations.add(new Translation(Locale.UK, "GB Test 2 Updated"));	
		SalesCategory salesCategory = salesCategoryService.get(2);
		salesCategory.setTranslations(translations);
		salesCategoryService.merge(salesCategory);
		
		SalesCategory updatedSalesCategory = salesCategoryService.get(2);
		for (Translation t: updatedSalesCategory.getTranslations()) {
			if (t.getLocale().toString().equals("en_GB")) {
				Assert.assertEquals("GB Test 2 Updated", t.getTranslation());
				break;
			}
		}
	}	
	@Test
	public void editSalesCategoryAddControllerTest() {
		EditSalesCategoryForm editSalesCategoryForm = editSalesCategoryController.createEditSalesCategoryForm();
		for (Translation t: editSalesCategoryForm.getTranslations()) {
			if (t.getLocale().toString().equals("en_GB")) {
				t.setTranslation("GB Test 6");
				break;
			}
		}
		SalesCategory salesCategory = new SalesCategory();
		salesCategory.setActive(true);
		editSalesCategoryForm.setSalesCategory(salesCategory);				
		BindingResult result = (new WebDataBinder(editSalesCategoryForm)).getBindingResult();
		editSalesCategoryController.processFormSave(editSalesCategoryForm, result);
		List<SalesCategory> salesCategories = salesCategoryDao.getAllSalesCategories();
		Assert.assertNotNull(salesCategories);
		Assert.assertEquals(6, salesCategories.size());
	}
	@Test
	public void editSalesCategoryDeleteControllerTest() {
		EditSalesCategoryForm editSalesCategoryForm = editSalesCategoryController.createEditSalesCategoryForm();
		SalesCategory salesCategory = salesCategoryDao.find(3);
		editSalesCategoryForm.setSalesCategory(salesCategory);				
		BindingResult result = (new WebDataBinder(editSalesCategoryForm)).getBindingResult();
		editSalesCategoryController.processFormDelete(editSalesCategoryForm, result);
		SalesCategory category = salesCategoryDao.find(3);
		Assert.assertNull(category);
	}
	@Test
	public void editSalesCategoryUpdateControllerTest() {
		EditSalesCategoryForm editSalesCategoryForm = editSalesCategoryController.createEditSalesCategoryForm();
		for (Translation t: editSalesCategoryForm.getTranslations()) {
			if (t.getLocale().toString().equals("en_GB")) {
				t.setTranslation("GB Test 2 Updated");
				break;
			}
		}
		SalesCategory salesCategory = salesCategoryDao.find(2);
		editSalesCategoryForm.setSalesCategory(salesCategory);				
		BindingResult result = (new WebDataBinder(editSalesCategoryForm)).getBindingResult();
		editSalesCategoryController.processFormSave(editSalesCategoryForm, result);
		SalesCategory updatedSalesCategory = salesCategoryDao.find(2);
		for (Translation t: updatedSalesCategory.getTranslations()) {
			if (t.getLocale().toString().equals("en_GB")) {
				Assert.assertEquals("GB Test 2 Updated", t.getTranslation());
				break;
			}
		}
	}
}
