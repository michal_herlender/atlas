package org.trescal.cwms.core.instrumentmodel.controller.family;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.db.InstrumentModelFamilyDao;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.db.InstrumentModelFamilyService;
import org.trescal.cwms.core.instrumentmodel.form.EditFamilyForm;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.spring.model.InstrumentModelFamilyJsonDTO;
import org.trescal.cwms.tests.MvcCwmsTestClass;

public class EditFamilyTest extends MvcCwmsTestClass {

	public final static String DATA_SET = "cwms/instrumentmodel/Families.xml";
	
	@Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }

	@Autowired
	private InstrumentModelFamilyDao instModFamDao;

	@Autowired
	private InstrumentModelFamilyService instModFamService;

	@Autowired
	private EditFamilyController editFamilyController;

	@Test
	public void instrumentModelFamilyDaoWriteTest() {
		InstrumentModelFamily instrumentModelFamily = new InstrumentModelFamily();
		instrumentModelFamily.setName("Thermometer");
		instrumentModelFamily.setActive(true);
		instrumentModelFamily.setTmlid(0);
		instModFamDao.persist(instrumentModelFamily);
		List<InstrumentModelFamilyJsonDTO> families = instModFamDao.getAllFamilies(new Locale("en", "GB"));
		Assert.assertNotNull(families);
		Assert.assertEquals(6, families.size());
	}

	@Test
	public void instrumentModelFamilyServiceWriteTest() {
		InstrumentModelFamily instrumentModelFamily = new InstrumentModelFamily();
		instrumentModelFamily.setName("Thermometer");
		instrumentModelFamily.setActive(true);
		instrumentModelFamily.setTmlid(0);
		instModFamService.createInstrumentModelFamily(instrumentModelFamily);
		List<InstrumentModelFamilyJsonDTO> families = instModFamDao
				.getAllFamilies(new Locale("en", "GB"));
		Assert.assertNotNull(families);
		Assert.assertEquals(6, families.size());
	}

	@Test
	public void editInstrumentModelFamilyAddFamilyControllerTest() {
		EditFamilyForm editFamilyForm = editFamilyController.createEditFamilyForm();
		for (Translation t: editFamilyForm.getTranslations()) {
			if (t.getLocale().toString().equals("en_GB")) {
				t.setTranslation("Thermometer");
				break;
			}
		}
		InstrumentModelFamily instrumentModelFamily = new InstrumentModelFamily();
		instrumentModelFamily.setActive(true);
		instrumentModelFamily.setTmlid(0);
		editFamilyForm.setFamily(instrumentModelFamily);
		BindingResult result = (new WebDataBinder(editFamilyForm))
				.getBindingResult();
		editFamilyController.processFormSave(editFamilyForm, result);
		List<InstrumentModelFamilyJsonDTO> families = instModFamDao
				.getAllFamilies(new Locale("en", "GB"));
		Assert.assertNotNull(families);
		Assert.assertEquals(6, families.size());
	}

	
	
	@Test
	public void instrumentModelFamilyDaoDeleteTest() {
		InstrumentModelFamily instrumentModelFamily = instModFamDao.find(5);

		instModFamDao.remove(instrumentModelFamily);
		
		InstrumentModelFamily family = instModFamDao.find(5);
		Assert.assertNull(family);
	}
	
	@Test
	public void instrumentModelFamilyServiceDeleteTest() {
		InstrumentModelFamily instrumentModelFamily = instModFamDao.find(5);

		instModFamService.deleteInstrumentModelFamily(instrumentModelFamily);
		
		InstrumentModelFamily family = instModFamDao.find(5);
		Assert.assertNull(family);
	}
	
	@Test
	public void editInstrumentModelFamilyDeleteFamilyControllerTest() {
		EditFamilyForm editFamilyForm = editFamilyController.createEditFamilyForm();
		for (Translation t: editFamilyForm.getTranslations()) {
			if (t.getLocale().toString().equals("en_GB")) {
				t.setTranslation("Power Supply");
				break;
			}
		}
		InstrumentModelFamily instrumentModelFamily = instModFamDao.find(5);

		editFamilyForm.setFamily(instrumentModelFamily);
		BindingResult result = (new WebDataBinder(editFamilyForm))
				.getBindingResult();
		editFamilyController.processFormDelete(editFamilyForm, result);
		
		InstrumentModelFamily family = instModFamDao.find(5);
		Assert.assertNull(family);
	}
	
	
	
	@Test
	public void instrumentModelFamilyDaoUpdateTest() {
		InstrumentModelFamily instrumentModelFamily = new InstrumentModelFamily();
		instrumentModelFamily.setFamilyid(5);
		instrumentModelFamily.setName("Power Supply");
		instrumentModelFamily.setActive(true);
		instrumentModelFamily.setTmlid(69);
		instModFamDao.merge(instrumentModelFamily);
		
		InstrumentModelFamily family = instModFamDao.find(5);
		Assert.assertEquals(69, family.getTmlid().intValue());
	}
	
	@Test
	public void instrumentModelFamilyServiceUpdateTest() {
		InstrumentModelFamily instrumentModelFamily = new InstrumentModelFamily();
		instrumentModelFamily.setFamilyid(5);
		instrumentModelFamily.setName("Power Supply");
		instrumentModelFamily.setActive(true);
		instrumentModelFamily.setTmlid(69);
		instModFamService.updateInstrumentModelFamily(instrumentModelFamily);
		
		InstrumentModelFamily family = instModFamDao.find(5);
		Assert.assertEquals(69, family.getTmlid().intValue());
	}
	
	

}
