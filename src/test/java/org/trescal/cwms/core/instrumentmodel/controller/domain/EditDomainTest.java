package org.trescal.cwms.core.instrumentmodel.controller.domain;

import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.trescal.cwms.core.instrumentmodel.entity.domain.InstrumentModelDomain;
import org.trescal.cwms.core.instrumentmodel.entity.domain.db.InstrumentModelDomainDao;
import org.trescal.cwms.core.instrumentmodel.entity.domain.db.InstrumentModelDomainService;
import org.trescal.cwms.core.instrumentmodel.form.EditDomainForm;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.spring.model.InstrumentModelDomainJsonDTO;
import org.trescal.cwms.tests.MvcCwmsTestClass;

public class EditDomainTest extends MvcCwmsTestClass {

	public final static String DATA_SET = "cwms/instrumentmodel/Domains.xml";
	
	@Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }

	@Autowired
	private InstrumentModelDomainDao instModDomDao;

	@Autowired
	private InstrumentModelDomainService instModDomService;

	@Autowired
	private EditDomainController editDomainController;

	@Test
	public void instrumentModelDomainDaoWriteTest() {
		InstrumentModelDomain instrumentModelDomain = new InstrumentModelDomain();
		instrumentModelDomain.setName("Temperature");
		instrumentModelDomain.setActive(true);
		instrumentModelDomain.setTmlid(0);
		instModDomDao.persist(instrumentModelDomain);
		List<InstrumentModelDomainJsonDTO> domains = (List<InstrumentModelDomainJsonDTO>) instModDomDao
				.getAllDomains();
		Assert.assertNotNull(domains);
		Assert.assertEquals(4, domains.size());
	}

	@Test
	public void instrumentModelDomainServiceWriteTest() {
		InstrumentModelDomain instrumentModelDomain = new InstrumentModelDomain();
		instrumentModelDomain.setName("Temperature");
		instrumentModelDomain.setActive(true);
		instrumentModelDomain.setTmlid(0);
		instModDomService.createInstrumentModelDomain(instrumentModelDomain);
		List<InstrumentModelDomainJsonDTO> domains = instModDomDao
				.getAllDomains();
		Assert.assertNotNull(domains);
		Assert.assertEquals(4, domains.size());
	}

	@Test
	public void editInstrumentModelDomainAddDomainControllerTest() {
		EditDomainForm editDomainForm = editDomainController.createEditDomainForm();
		for (Translation t: editDomainForm.getTranslations()) {
			if (t.getLocale().toString().equals("en_GB")) {
				t.setTranslation("Temperature");
				break;
			}
		}
		InstrumentModelDomain domain = new InstrumentModelDomain();
		domain.setActive(true);
		domain.setTmlid(0);
		editDomainForm.setDomain(domain);
		BindingResult result = (new WebDataBinder(editDomainForm))
				.getBindingResult();
		editDomainController.processFormSave(editDomainForm, result);
		List<InstrumentModelDomainJsonDTO> domains = instModDomDao
				.getAllDomains();
		Assert.assertNotNull(domains);
		Assert.assertEquals(4, domains.size());
	}


	
	@Test
	public void instrumentModelDomainDaoDeleteTest() {
		InstrumentModelDomain instrumentModelDomain = instModDomDao.find(2);

		instModDomDao.remove(instrumentModelDomain);
		
		InstrumentModelDomain domain = instModDomDao.find(2);
		Assert.assertNull(domain);
	}
	
	@Test
	public void instrumentModelDomainServiceDeleteTest() {
		InstrumentModelDomain instrumentModelDomain = instModDomDao.find(2);

		instModDomService.deleteInstrumentModelDomain(instrumentModelDomain);
		
		InstrumentModelDomain domain = instModDomDao.find(2);
		Assert.assertNull(domain);
	}
	
	@Test
	public void editInstrumentModelDomainDeleteDomainControllerTest() {
		EditDomainForm editDomainForm = editDomainController.createEditDomainForm();
		for (Translation t: editDomainForm.getTranslations()) {
			if (t.getLocale().toString().equals("en_GB")) {
				t.setTranslation("Dimensional");
				break;
			}
		}
		InstrumentModelDomain domain = instModDomDao.find(2);

		editDomainForm.setDomain(domain);
		BindingResult result = (new WebDataBinder(editDomainForm))
				.getBindingResult();
		editDomainController.processFormDelete(editDomainForm, result);
		
		InstrumentModelDomain domainout = instModDomDao.find(2);
		Assert.assertNull(domainout);
	}

	
	@Test
	public void instrumentModelDomainDaoUpdateTest() {
		InstrumentModelDomain instrumentModelDomain = new InstrumentModelDomain();
		instrumentModelDomain.setDomainid(2);
		instrumentModelDomain.setName("Dimensional");
		instrumentModelDomain.setActive(true);
		instrumentModelDomain.setTmlid(69);
		instModDomDao.merge(instrumentModelDomain);
		
		InstrumentModelDomain domain = instModDomDao.find(2);
		Assert.assertEquals(69, domain.getTmlid().intValue());
	}
	
	@Test
	public void instrumentModelDomainServiceUpdateTest() {
		InstrumentModelDomain instrumentModelDomain = new InstrumentModelDomain();
		instrumentModelDomain.setDomainid(2);
		instrumentModelDomain.setName("Dimensional");
		instrumentModelDomain.setActive(true);
		instrumentModelDomain.setTmlid(69);
		instModDomService.updateInstrumentModelDomain(instrumentModelDomain);
		
		InstrumentModelDomain domain = instModDomDao.find(2);
		Assert.assertEquals(69, domain.getTmlid().intValue());
	}
	
	@Test
	public void editInstrumentModelDomainUpdateDomainControllerTest() {
		EditDomainForm editDomainForm = editDomainController.createEditDomainForm();
		for (Translation t: editDomainForm.getTranslations()) {
			if (t.getLocale().toString().equals("en_GB")) {
				t.setTranslation("Dimensional");
				break;
			}
		}
		InstrumentModelDomain domain = new InstrumentModelDomain();
		domain.setDomainid(2);
		domain.setActive(true);
		domain.setTmlid(69);
		editDomainForm.setDomain(domain);
		BindingResult result = (new WebDataBinder(editDomainForm))
				.getBindingResult();
		editDomainController.processFormSave(editDomainForm, result);
		
		InstrumentModelDomain domainout = instModDomDao.find(2);
		Assert.assertEquals(69, domainout.getTmlid().intValue());
	}
	



}
