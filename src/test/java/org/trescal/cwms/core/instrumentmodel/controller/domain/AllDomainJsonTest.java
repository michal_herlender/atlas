package org.trescal.cwms.core.instrumentmodel.controller.domain;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.trescal.cwms.core.instrumentmodel.entity.domain.db.InstrumentModelDomainDao;
import org.trescal.cwms.core.instrumentmodel.entity.domain.db.InstrumentModelDomainService;
import org.trescal.cwms.spring.model.InstrumentModelDomainJsonDTO;
import org.trescal.cwms.tests.MvcCwmsTestClass;

public class AllDomainJsonTest extends MvcCwmsTestClass {
	
	public final static String DATA_SET = "cwms/instrumentmodel/Domains.xml";
	
	@Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }
	
	@Autowired
	private InstrumentModelDomainDao instModDomDao;
	
	@Autowired
	private InstrumentModelDomainService instModDomService;
	
	@Autowired
	private AllDomainJson allDomainJson;
	
	@Test
	public void instrumentModelDomainDaoTest() {
		List<InstrumentModelDomainJsonDTO> domains = instModDomDao.getAllDomains(Locale.UK);
		Assert.assertNotNull(domains);
		Assert.assertEquals(3,domains.size());	
	}
	
	@Test
	public void instrumentModelDomainServiceTest() {
		List<InstrumentModelDomainJsonDTO> domains = instModDomService.getAllDomains(Locale.UK);
		Assert.assertNotNull(domains);
		Assert.assertEquals(3,domains.size());	
	}
	
	@Test
	public void allDomainJsonControllerTest() {
		List<InstrumentModelDomainJsonDTO> jsonOutput = allDomainJson.getAllDomains(Locale.UK);
		Assert.assertNotNull(jsonOutput);
		Assert.assertEquals(3,jsonOutput.size());
	}
	
	@Test
	public void allDomainJsonTest() throws Exception {
		this.mockMvc.perform(get("/alldomains.json").
				accept(MediaType.parseMediaType("application/json;charset=UTF-8"))).
				andDo(print()).
				andExpect(jsonPath("$").exists()).
				andExpect(jsonPath("$[0].domainid").value(2)).
				andExpect(jsonPath("$[1].name").value("Electrical")).
				andExpect(jsonPath("$[2].active").value("Yes")).
				andExpect(jsonPath("$[3]").doesNotExist());
	}

}
