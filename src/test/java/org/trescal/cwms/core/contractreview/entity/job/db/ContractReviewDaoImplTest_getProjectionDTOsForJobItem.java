package org.trescal.cwms.core.contractreview.entity.job.db;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreview.db.ContractReviewDao;
import org.trescal.cwms.core.jobs.contractreview.projection.ContractReviewProjectionDTO;
import org.trescal.cwms.tests.MvcCwmsTestClass;
import org.trescal.cwms.tests.generic.DataSetJob;
import org.trescal.cwms.tests.generic.contractreview.TestDataContractReview;
import org.trescal.cwms.tests.generic.job.TestDataJobItem;

public class ContractReviewDaoImplTest_getProjectionDTOsForJobItem extends MvcCwmsTestClass {
	
	@Autowired
	private ContractReviewDao contractReviewDao;
	
	@Override
	protected List<String> getDataSetFileNames() {
		List<String> result = DataSetJob.getJobClientCompletedTestData();
		result.add(DataSetJob.FILE_CONTRACT_REVIEW);
		return result;
	}
	
	@Test
	public void testJobItemId() {
		
		 Integer jobItemId = TestDataJobItem.ID_CLIENT_FR_1;
		 List<ContractReviewProjectionDTO> result = this.contractReviewDao.getProjectionDTOsForJobItem(jobItemId);
		 ContractReviewProjectionDTO dto0 = result.get(0);
		 
		 Assert.assertEquals(dto0.getId(),TestDataContractReview.CONTRACT_REVIEW_ID);
		 Assert.assertEquals(dto0.getComments(),TestDataContractReview.COMMENT);
		 Assert.assertEquals(dto0.getReviewById(),TestDataContractReview.REVIEW_BY_ID);
	}

}							


