package org.trescal.cwms.core.jobs.jobitem.db;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemDao;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;
import org.trescal.cwms.tests.MvcCwmsTestClass;
import org.trescal.cwms.tests.generic.DataSetJob;
import org.trescal.cwms.tests.generic.job.TestDataJobItem;

public class JobItemDaoImplTest_findMostRecentJobItemDTO extends MvcCwmsTestClass {
	@Autowired
    private JobItemDao jobitemDao;

	@Override
	protected List<String> getDataSetFileNames() {
		 return DataSetJob.getJobClientCompletedTestData();
	}
	@Test
	public void testplantids() {
	
		List<Integer> plantids = new ArrayList<>();
		plantids.add(TestDataJobItem.PLANT_ID_FR_1);
	
		Map<Integer, JobItemProjectionDTO> dtos = this.jobitemDao.findMostRecentJobItemDTO(plantids);
	
		Assert.assertEquals(plantids.size(), dtos.size());
		JobItemProjectionDTO dtoa = dtos.values().stream().findFirst().orElse(null);
	
		Assert.assertNotNull(dtoa);
		Assert.assertEquals(dtoa.getPlantid(), TestDataJobItem.PLANT_ID_FR_1);
	
	}
}
