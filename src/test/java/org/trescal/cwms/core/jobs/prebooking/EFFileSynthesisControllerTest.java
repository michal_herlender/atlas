package org.trescal.cwms.core.jobs.prebooking;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.trescal.cwms.core.logistics.controller.EFFileSynthesisController;
import org.trescal.cwms.core.logistics.form.AddPrebookingJobForm;
import org.trescal.cwms.core.logistics.utils.AsnItemStatusEnum;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.tests.MvcCwmsTestClass;

public class EFFileSynthesisControllerTest extends MvcCwmsTestClass {

	public static final String DATA_SET = "cwms/jobs/job/CheckStatus.xml";
	public static final String USERNAME = "technician";

	@Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }

	/**
	 * test checkCurrentItem method NOT_IDENTIFIED case & ALREADY_ONJOB case &
	 * ALREADY_ONPREBOOKING case
	 *
	 */
	@Test
	public void testValidationStatus() throws Exception {

		Map<String, Object> sessionAttributes = new HashMap<>();
		sessionAttributes.put(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME);
		KeyValue<Integer, String> subdivDto = new KeyValue<>(1, "Madrid");
		sessionAttributes.put(Constants.SESSION_ATTRIBUTE_SUBDIV, subdivDto);

		MultiValueMap<String, String> inputPayload = new LinkedMultiValueMap<>();
		inputPayload.add("addrid", "1");
		inputPayload.add("exchangeFormat", "1");
		inputPayload.add("jobType", "STANDARD");
		inputPayload.add("coid", "1");
		inputPayload.add("coname", "Trescal ITM Test");
		inputPayload.add("subdivid", "1");
		inputPayload.add("personid", "1");
		inputPayload.add("addrid", "1");
		inputPayload.add("recordType", "FILE");
		inputPayload.add("currencyCode", "GBP");
		inputPayload.add("bookedInAddrId", "2");
		inputPayload.add("bookedInLocId", "0");
		inputPayload.add("asnId", "1");

		LinkedCaseInsensitiveMap<String> notIdentified = new LinkedCaseInsensitiveMap<String>();
		notIdentified.put("Inst_Client_ID", "11");
		notIdentified.put("Marque", "Test");

		LinkedCaseInsensitiveMap<String> onJob = new LinkedCaseInsensitiveMap<String>();
		onJob.put("Inst_Client_ID", "100005");
		onJob.put("Marque", "Test");
		onJob.put("plantno", "plant");

		LinkedCaseInsensitiveMap<String> onPrebooking = new LinkedCaseInsensitiveMap<String>();
		onPrebooking.put("Inst_Client_ID", "100006");
		onPrebooking.put("Marque", "Test");
		onPrebooking.put("plantno", "plant");

		ArrayList<LinkedCaseInsensitiveMap<String>> file = new ArrayList<>();
		file.add(notIdentified);
		file.add(onJob);
		file.add(onPrebooking);

		MvcResult result = this.mockMvc.perform(get("/effilesynthesis.htm").params(inputPayload)
				.flashAttr("fileContent", file).sessionAttrs(sessionAttributes)).andReturn();

		BindingResult bindingResult = (BindingResult) result.getModelAndView().getModel()
				.get(BindingResult.MODEL_KEY_PREFIX + EFFileSynthesisController.FORM);

		// check that attribute has no errors
		assertEquals("apbjf", bindingResult.getObjectName());
		assertEquals(0, bindingResult.getErrorCount());
		// check the status
		AddPrebookingJobForm Form = (AddPrebookingJobForm) result.getModelAndView().getModel().get("apbjf");
		assertEquals(AsnItemStatusEnum.NOT_IDENTIFIED, Form.getAsnItemsDTOList().get(0).getStatus());
		assertEquals(AsnItemStatusEnum.ALREADY_ONJOB, Form.getAsnItemsDTOList().get(1).getStatus());
		assertEquals(AsnItemStatusEnum.ALREADY_ONPREBOOKING, Form.getAsnItemsDTOList().get(2).getStatus());
	}

	/**
	 * test checkCurrentItem method IDENTIFIED case
	 */
	@Test
	public void testValidationStatusIdentified() throws Exception {

		Map<String, Object> sessionAttributes = new HashMap<>();
		sessionAttributes.put(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME);
		KeyValue<Integer, String> subdivDto = new KeyValue<>(1, "Madrid");
		sessionAttributes.put(Constants.SESSION_ATTRIBUTE_SUBDIV, subdivDto);

		MultiValueMap<String, String> inputPayload = new LinkedMultiValueMap<>();
		inputPayload.add("addrid", "1");
		inputPayload.add("exchangeFormat", "1");
		inputPayload.add("jobType", "STANDARD");
		inputPayload.add("coid", "1");
		inputPayload.add("coname", "Trescal ITM Test");
		inputPayload.add("subdivid", "1");
		inputPayload.add("personid", "1");
		inputPayload.add("addrid", "1");
		inputPayload.add("recordType", "FILE");
		inputPayload.add("currencyCode", "GBP");
		inputPayload.add("bookedInAddrId", "2");
		inputPayload.add("bookedInLocId", "0");
		inputPayload.add("asnId", "1");

		LinkedCaseInsensitiveMap<String> identifiedOnOtherCompany = new LinkedCaseInsensitiveMap<String>();
		identifiedOnOtherCompany.put("Inst_Client_ID", "100007");
		identifiedOnOtherCompany.put("Marque", "Test");
		identifiedOnOtherCompany.put("plantno", "plantTest");

		ArrayList<LinkedCaseInsensitiveMap<String>> file = new ArrayList<>();

		file.add(identifiedOnOtherCompany);

		MvcResult result = this.mockMvc.perform(get("/effilesynthesis.htm").params(inputPayload)
				.flashAttr("fileContent", file).sessionAttrs(sessionAttributes)).andReturn();

		BindingResult bindingResult = (BindingResult) result.getModelAndView().getModel()
				.get(BindingResult.MODEL_KEY_PREFIX + EFFileSynthesisController.FORM);

		// check that attribute has no errors
		assertEquals("apbjf", bindingResult.getObjectName());
		assertEquals(0, bindingResult.getErrorCount());
		// check the status
		AddPrebookingJobForm Form = (AddPrebookingJobForm) result.getModelAndView().getModel().get("apbjf");
		assertEquals(AsnItemStatusEnum.IDENTIFIED, Form.getAsnItemsDTOList().get(0).getStatus());
	}
}
