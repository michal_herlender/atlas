package org.trescal.cwms.core.jobs.jobitem.db;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemDao;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocales;
import org.trescal.cwms.tests.MvcCwmsTestClass;
import org.trescal.cwms.tests.generic.DataSetJob;
import org.trescal.cwms.tests.generic.instrument.TestDataInstrument;
import org.trescal.cwms.tests.generic.instrument.TestDataInstrumentModel;
import org.trescal.cwms.tests.generic.instrument.TestDataInstrumentModelTranslation;
import org.trescal.cwms.tests.generic.instrument.TestDataMfr;
import org.trescal.cwms.tests.generic.job.TestDataJob;
import org.trescal.cwms.tests.generic.job.TestDataJobItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class JobItemDaoImplTest_getJobItemsFromJobs extends MvcCwmsTestClass {

	@Autowired
	private JobItemDao jobitemDao;

	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetJob.getJobClientCompletedTestData();
	}
	
	@Test
	public void testMultiIjobnos(){
		
		Locale locale = SupportedLocales.FRENCH_FR;
		
		
		List<String> jobnos = new ArrayList<>();
		jobnos.add(TestDataJob.JOB_NO_CLIENT_FR);
		List<JobItemProjectionDTO> dtos = this.jobitemDao.getJobItemsFromJobs(jobnos, locale);
		JobItemProjectionDTO dto = dtos.get(0);
		
		Assert.assertEquals(dto.getJobno(),TestDataJob.JOB_NO_CLIENT_FR);
		Assert.assertEquals(dto.getPlantid(), TestDataJobItem.PLANT_ID_FR_1 );
		Assert.assertEquals(dto.getJobItemId(),TestDataJobItem.ID_CLIENT_FR_1);
		Assert.assertEquals(dto.getInstrument().getPlantno(), TestDataInstrument.PLANT_NO_1101);
		Assert.assertEquals(dto.getInstrument().getSerialno(), TestDataInstrument.SERIAL_NO_1101);
		Assert.assertEquals(dto.getInstrument().getCustomerDescription(), TestDataInstrument.CUSTOMER_DESCRIPTION_1101);
		Assert.assertEquals(dto.getInstrument().getCalibrationStandard(), TestDataInstrument.CALIBRATION_STANDARD_1101);
		Assert.assertEquals(dto.getInstrument().getScrapped(), TestDataInstrument.SCRAPPED_1101);
		Assert.assertEquals(dto.getInstrument().getInstrumentModelName(), TestDataInstrument.MODEL_NAME_1101);
		Assert.assertEquals(dto.getInstrument().getModelid(), TestDataInstrumentModel.ID_STANDALONE_CALIPER_300MM);
		Assert.assertEquals(dto.getInstrument().getInstrumentModelTranslation(), TestDataInstrumentModelTranslation.TRANSLATION_FR_FR_CALIPER_300MM);
		Assert.assertEquals(dto.getInstrument().getModelMfrType(), TestDataInstrumentModel.MODEL_MFR_CALIPER_300MM);
		Assert.assertEquals(dto.getInstrument().getInstrumentMfrName(), TestDataMfr.MFR_NAME);
		Assert.assertEquals(dto.getInstrument().getSubfamilyTypology(), TestDataInstrumentModel.SUBFAMILYTYPOLOGY_CALIPER_300MM);
		
	}
} 
