package org.trescal.cwms.core.jobs.jobitemnotinvoiced.db;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.db.ExpenseItemNotInvoicedDao;
import org.trescal.cwms.core.jobs.jobitem.projection.JobExpenseItemNotInvoicedDTO;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocales;
import org.trescal.cwms.tests.MvcCwmsTestClass;
import org.trescal.cwms.tests.generic.DataSetPricing;
import org.trescal.cwms.tests.generic.pricing.TestDataInvoice;
import org.trescal.cwms.tests.generic.pricing.TestDataJobItemNotInvoiced;

public class ExpenseItemNotInvoicedDaoImplTest_getProjectionDTOs extends MvcCwmsTestClass {

	@Autowired
	private ExpenseItemNotInvoicedDao expenseItemNotInvoicedDao;
	
	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetPricing.getInvoicePeriodicTestData();
	}

	@Test
	public void testinvoiceIds() {
		Collection<Integer> invoiceIds  = new ArrayList<>();
		invoiceIds.add(TestDataInvoice.INVOICE_ID_FR_111_PERIODIC);
		Locale locale = SupportedLocales.FRENCH_FR;
		
		List<JobExpenseItemNotInvoicedDTO> results = this.expenseItemNotInvoicedDao.getProjectionDTOs(invoiceIds, locale);
		Integer resultSize = results.size();
		Assert.assertEquals(TestDataJobItemNotInvoiced.RECORD_COUNT, resultSize);
		
		JobExpenseItemNotInvoicedDTO dto1 = results.get(0);
		
		Assert.assertEquals(TestDataJobItemNotInvoiced.ID_CLIENT_FR_1101, dto1.getNotInvoicedId());
	}
}
