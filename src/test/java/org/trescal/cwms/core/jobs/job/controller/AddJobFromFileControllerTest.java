package org.trescal.cwms.core.jobs.job.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;

import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.tests.MvcCwmsTestClass;

public class AddJobFromFileControllerTest extends MvcCwmsTestClass {

	public static final String DATA_SET = "cwms/jobs/job/CreateJobFromFile.xml";
	public static final String EXCEL_FILE = "cwms/jobs/job/instrument_export_test.xlsx";
	public static final String USERNAME = "technician";

	@Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }

	@Test
	public void testMinim() throws Exception {
		// should cause a validation failure
		MultiValueMap<String, String> inputPayload = new LinkedMultiValueMap<>();
		inputPayload.add("addrid", "1");
		inputPayload.add("exchangeFormat", "1");
		inputPayload.add("receiptDate", "06.02.2019");
		inputPayload.add("jobType", "STANDARD");
		inputPayload.add("coid", "2");
		inputPayload.add("coname", "saf");
		inputPayload.add("subdivid", "2");
		inputPayload.add("personid", "2");
		inputPayload.add("addrid", "2");
		inputPayload.add("recordType", "FILE");
		inputPayload.add("currencyCode", "MAD");
		inputPayload.add("bookedInAddrId", "2");
		inputPayload.add("bookedInLocId", "0");
		inputPayload.add("receiptDate", "06.02.2019");
		inputPayload.add("transportIn", "56");
		inputPayload.add("carrier", "0");
		inputPayload.add("transportOut", "56");

		InputStream is = getClass().getResourceAsStream("classpath:test-data/" + EXCEL_FILE);
		MockMultipartFile file = new MockMultipartFile("uploadFile", "instrument_export_test.xlsx", "text/plain",
				IOUtils.toByteArray(is));

		testSuccess(inputPayload, file).andReturn();
	}

	private ResultActions testSuccess(MultiValueMap<String, String> inputPayload, MockMultipartFile file)
			throws Exception {
		// Return for further chaining
		return this.test(inputPayload, file).andExpect(redirectedUrl("jobfinalsynthesis.htm"));
	}

	private ResultActions test(MultiValueMap<String, String> inputPayload, MockMultipartFile file) throws Exception {

		Map<String, Object> sessionAttributes = new HashMap<>();
		sessionAttributes.put(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME);
		KeyValue<Integer, String> subdivDto = new KeyValue<>(1, "Madrid");
		sessionAttributes.put(Constants.SESSION_ATTRIBUTE_SUBDIV, subdivDto);

		return this.mockMvc
				.perform(fileUpload("/addjobff.htm").file(file).params(inputPayload).sessionAttrs(sessionAttributes))
				.andDo(print());
	}

}
