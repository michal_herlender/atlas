package org.trescal.cwms.core.jobs.jobitemnotinvoiced.db;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.db.JobItemNotInvoicedDao;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemNotInvoicedDTO;
import org.trescal.cwms.tests.MvcCwmsTestClass;
import org.trescal.cwms.tests.generic.DataSetPricing;
import org.trescal.cwms.tests.generic.job.TestDataJobItem;
import org.trescal.cwms.tests.generic.pricing.TestDataInvoice;
import org.trescal.cwms.tests.generic.pricing.TestDataJobItemNotInvoiced;

public class JobItemNotInvoicedDaoImplTest_getProjectionDTOs extends MvcCwmsTestClass {
	
	@Autowired
	private JobItemNotInvoicedDao jobItemNotInvoicedDao;
	
	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetPricing.getInvoicePeriodicTestData();
	}
	
	@Test
	public void testinvoiceIds() {
		Collection<Integer> invoiceIds  = new ArrayList<>();
		invoiceIds.add(TestDataInvoice.INVOICE_ID_FR_111_PERIODIC);
		
		List<JobItemNotInvoicedDTO> results = this.jobItemNotInvoicedDao.getProjectionDTOs(invoiceIds);
		Integer resultSize = results.size();
		Assert.assertEquals(TestDataJobItemNotInvoiced.RECORD_COUNT, resultSize);
		
		JobItemNotInvoicedDTO dto1 = results.get(0);
		
		Assert.assertEquals(TestDataJobItemNotInvoiced.ID_CLIENT_FR_1101, dto1.getNotInvoicedId());
		Assert.assertEquals(TestDataJobItem.ID_CLIENT_FR_1, dto1.getJobItemId());
	}
}
