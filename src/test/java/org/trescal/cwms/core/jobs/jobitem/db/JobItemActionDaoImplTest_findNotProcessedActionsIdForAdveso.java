package org.trescal.cwms.core.jobs.jobitem.db;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.db.JobItemActionDao;
import org.trescal.cwms.tests.MvcCwmsTestClass;
import org.trescal.cwms.tests.generic.DataSetAdveso;
import org.trescal.cwms.tests.generic.DataSetJob;
import org.trescal.cwms.tests.generic.job.TestDataJobItemAction;

public class JobItemActionDaoImplTest_findNotProcessedActionsIdForAdveso extends MvcCwmsTestClass {

	@Autowired
	private JobItemActionDao jobItemActionDao;
	
	@Override
	protected List<String> getDataSetFileNames() {
		List<String> result = DataSetJob.getJobClientCompletedTestData();
		result.add(DataSetJob.FILE_JOB_CLIENT_COMPLETED_ACTIONS);
		result.add(DataSetAdveso.FILE_ADVESO_SYSTEM_DEFAULT);
		result.add(DataSetAdveso.FILE_ADVESO_JOB_ITEM_ACTIVITY_QUEUE);
		return result;
	}
	
	@Test
	public void shouldReturnAdditionalJobItemActions() {
		List<Integer> actionIdList = this.jobItemActionDao.findNotProcessedActionsIdForAdveso();
		List<Integer> expectedIdList = new ArrayList<>();
		expectedIdList.add(TestDataJobItemAction.ID_JOB_ITEM_1301_RECEIVED);
		expectedIdList.add(TestDataJobItemAction.ID_JOB_ITEM_1301_REVIEWED);
		expectedIdList.add(TestDataJobItemAction.ID_JOB_ITEM_1302_RECEIVED);
		expectedIdList.add(TestDataJobItemAction.ID_JOB_ITEM_1302_REVIEWED);
		Assert.assertEquals(4, actionIdList.size());
		Assert.assertEquals(expectedIdList, actionIdList);
	}
	
}
