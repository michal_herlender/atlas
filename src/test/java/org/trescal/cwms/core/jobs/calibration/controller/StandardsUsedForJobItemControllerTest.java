package org.trescal.cwms.core.jobs.calibration.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.trescal.cwms.tests.MvcWebTestClass;

public class StandardsUsedForJobItemControllerTest extends MvcWebTestClass {
	
	private static final String DATA_SET = "web/StandardsUsedForJobItemControllerTest.xml";

	@Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }
	
	
	@Test
    public void testGetJSON() throws Exception {
    	this.mockMvc.perform(get("/getstandardsused.json?jobitemid=105")
    	.accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
    	.andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType("application/json;charset=UTF-8"))
    	.andExpect(jsonPath("$").exists())
    	.andExpect(jsonPath("$").isArray())
    	.andExpect(jsonPath("$[0]").exists())
    	.andExpect(jsonPath("$[1]").exists())
    	.andExpect(jsonPath("$[2]").exists())
    	.andExpect(jsonPath("$[3]").doesNotExist())
    	.andExpect(jsonPath("$[0].plantId").value(1001))
    	.andExpect(jsonPath("$[0].translatedModelName").value("aninstrumentmodel"))
    	.andExpect(jsonPath("$[0].calDueDateAtTimeOfUse").value("01/05/17"))
    	.andExpect(jsonPath("$[1].inCalAtTimeOfUse").value(true));
    }

}
