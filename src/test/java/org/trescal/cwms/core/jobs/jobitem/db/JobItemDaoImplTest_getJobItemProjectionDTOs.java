package org.trescal.cwms.core.jobs.jobitem.db;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemDao;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.tests.MvcCwmsTestClass;
import org.trescal.cwms.tests.generic.DataSetJob;
import org.trescal.cwms.tests.generic.core.TestDataServiceType;
import org.trescal.cwms.tests.generic.job.TestDataJobItem;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class JobItemDaoImplTest_getJobItemProjectionDTOs extends MvcCwmsTestClass {

	@Autowired
    private  JobItemDao jobitemDao;
	
	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetJob.getJobClientCompletedTestData();
	}
	
@Test
public void testjobMultiIds() {
	Collection<Integer> jobIds = new ArrayList<>();
	Collection<Integer> jobItemIds = new ArrayList<>(); 
	
	jobItemIds.add(TestDataJobItem.ID_CLIENT_FR_1);
	List<JobItemProjectionDTO> dtos = this.jobitemDao.getJobItemProjectionDTOs(jobIds, jobItemIds);
	
	Assert.assertEquals(jobItemIds.size(), dtos.size());
	JobItemProjectionDTO dtoa = dtos.get(0);

	Assert.assertEquals(TestDataJobItem.ID_CLIENT_FR_1, dtoa.getJobItemId());
	Assert.assertEquals(TestDataJobItem.JOB_ID_CLIENT_FR, dtoa.getJobid());
	// GB 2021-01-27 : Job number intentionally not tested, it's not populated by the Dao call tested here.
//	Assert.assertEquals(TestDataJob.JOB_NO_CLIENT_FR, dtoa.getJobno());
	Assert.assertEquals(TestDataJobItem.ITEM_NO_FR_1, dtoa.getItemno());
	Assert.assertEquals(TestDataJobItem.PLANT_ID_FR_1, dtoa.getPlantid());
	Assert.assertEquals(TestDataServiceType.ID_AC_M_IL, dtoa.getServiceTypeId());
	Assert.assertEquals(TestDataJobItem.CAPABILITY_ID_FR_1, dtoa.getCapabilityId());
	Assert.assertEquals(TestDataJobItem.STATE_ID_FR_1, dtoa.getStateId());
	Assert.assertEquals(TestDataJobItem.ONBEHALF_COMPANY_ID_FR_1, dtoa.getOnBehalfCompanyId());
	Assert.assertNull(dtoa.getContractReviewById());
	Assert.assertEquals(TestDataJobItem.TURN_FR_1, dtoa.getTurn());

	// For compatibility with RESTful web service tests (serialized JSON) we use the same date format here
	SimpleDateFormat dtf = DateTools.rest_dtf;

	Assert.assertNotNull(dtoa.getDateComplete());
	Assert.assertEquals(dtf.format(dtoa.getDateComplete()), TestDataJobItem.DATE_COMPLETE_TEXT_FR_1);
	Assert.assertNotNull(dtoa.getDateIn());
	Assert.assertEquals(dtf.format(dtoa.getDateIn()), TestDataJobItem.DATE_IN_TEXT_FR_1);
	Assert.assertNotNull(dtoa.getDueDate());
	Assert.assertEquals(dtoa.getDueDate().format(DateTimeFormatter.ISO_DATE), TestDataJobItem.DUE_DATE_TEXT_FR_1);

	Assert.assertEquals(TestDataJobItem.CLIENT_REF_FR_1, dtoa.getClientRef());
	// GB 2021-01-27 : These two fields are intentionally not tested, they're not populated by the Dao call being tested here.
//	Assert.assertEquals(TestDataJobItem.DEFAULT_PAGE_FR_1, dtoa.getDefaultPage());
//	Assert.assertEquals(TestDataJobItem.PROC_RFFERNCE_FR_1, dtoa.getProcReference());

}
}

