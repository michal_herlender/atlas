package org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.db;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.core.workflow.entity.workactivity.WorkActivity;
import org.trescal.cwms.core.workflow.entity.workstatus.WorkStatus;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class JobItemActivityServiceTest {

	@Mock
	private JobItemActivityDao jobItemActivityDao;

	@Mock
	private ItemStateService itemStateService;

	@Mock
	private JobItemService jobItemService;

	@InjectMocks
	private JobItemActivityServiceImpl jobItemActivityServiceImpl;

	@Test
	@DisplayName("We would like to create an incomplete action. ")
	public void createIncompleteAction(){
		
		Integer activityId = 111;
		Integer jobItemId =222;
		Contact contact = new Contact();
		ItemState itemStatus = new WorkStatus();
		String description = "Calibration";

		WorkActivity workActivity = new WorkActivity();
		workActivity.setStateid(activityId);
		workActivity.setDescription(description);

		JobItem jobItem = new JobItem();
		jobItem.setJobItemId(jobItemId);
		jobItem.setState(itemStatus);

		Mockito.when(itemStateService.findItemActivity(Mockito.eq(activityId))).thenReturn(workActivity);
		Mockito.when(jobItemService.findJobItem(Mockito.eq(jobItemId))).thenReturn(jobItem);

		JobItemActivity activity = jobItemActivityServiceImpl.createIncompleteAction(activityId, jobItemId, contact);

		ArgumentCaptor<JobItemActivity> argumentCaptor = ArgumentCaptor.forClass(JobItemActivity.class);
		Mockito.verify(jobItemActivityDao).persist( argumentCaptor.capture());
		JobItemActivity jobItemActivity = argumentCaptor.getValue();

		assertEquals(activity,jobItemActivity);

		Mockito.verify(itemStateService).findItemActivity(Mockito.eq(activityId));
		Mockito.verify(jobItemService).findJobItem(Mockito.eq(jobItemId));

		assertEquals(activityId, activity.getActivity().getStateid());
		assertEquals(jobItemId, activity.getJobItem().getJobItemId());
		Assertions.assertNotNull(activity.getCreatedOn());
		assertEquals(contact, activity.getStartedBy());
		Assertions.assertNotNull(activity.getStartStamp());
		assertEquals(itemStatus,activity.getStartStatus());
		assertEquals(description,activity.getActivityDesc());
		assertFalse(activity.isComplete());
	}

	@Test
	@DisplayName("The method will throw IllegalArgumentException if Job item state is not an instance of ItemStatus")
	public void jobItemStateInstance(){
		Integer activityId = 111;
		Integer jobItemId =222;
		Contact contact = new Contact();

		WorkActivity workActivity = new WorkActivity();
		JobItem jobItem = new JobItem();
		jobItem.setJobItemId(jobItemId);
		jobItem.setState(workActivity);

		Mockito.when(jobItemService.findJobItem(Mockito.eq(jobItemId))).thenReturn(jobItem);

		JobItemActivityServiceImpl jobItemActivityServiceImpl = new JobItemActivityServiceImpl(null,
				null,null,itemStateService,null,
				jobItemService,jobItemActivityDao,null,null,null,
				null,null,null);

		IllegalArgumentException illegalArgumentException = Assertions.assertThrows(IllegalArgumentException.class, () -> jobItemActivityServiceImpl.createIncompleteAction(activityId, jobItemId, contact));
		assertEquals("Cannot create a new activity whilst the item state is an activity.",illegalArgumentException.getMessage());
	}
}
