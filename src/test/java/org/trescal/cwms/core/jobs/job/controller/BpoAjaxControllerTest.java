package org.trescal.cwms.core.jobs.job.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.tests.MvcCwmsTestClass;
import org.trescal.cwms.tests.generic.DataSetCompany;

import lombok.val;

public class BpoAjaxControllerTest extends MvcCwmsTestClass {

    @Autowired
    MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter;

    @Override
    protected List<String> getDataSetFileNames() {
        return DataSetCompany.getCompanyBusinessTestData();
    }

    @Test
    public void createBPO() throws Exception {
        val objectMapper = mappingJackson2HttpMessageConverter.getObjectMapper();
        val from = DateTools.dateFromLocalDate(LocalDate.of(2021, 2, 2));
        val to = DateTools.dateFromLocalDate(LocalDate.of(2021, 6, 2));

        val in = new HashMap<String, Object>();
        in.put("poNumber", "12");
        in.put("comment", "TEST Comment");
        in.put("scope", Scope.COMPANY.name());
        in.put("unitId", 540);
        in.put("durationFrom", from);
        in.put("durationTo", to);
        in.put("active", true);
        this.mockMvc.perform(post("/bpo/save.json")
            .accept(MediaType.parseMediaType("application/json;charset=UTF-8"))
            .sessionAttr(Constants.SESSION_ATTRIBUTE_COMPANY, new KeyValue<>(510, "c"))
            .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
            .content(objectMapper.writeValueAsString(in))
        ).andDo(print())
            .andExpect(jsonPath("$.poId").isNotEmpty())
            .andExpect(jsonPath("$.poNumber").value("12"))
        ;

    }
}