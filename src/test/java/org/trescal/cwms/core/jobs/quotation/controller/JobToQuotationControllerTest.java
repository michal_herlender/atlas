package org.trescal.cwms.core.jobs.quotation.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.ResultActions;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.form.JobToQuotationForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcCwmsTestClass;

public class JobToQuotationControllerTest extends MvcCwmsTestClass {

	public static final String DATA_SET = "cwms/quotation/JobToQuotationWithSortingTest.xml";
	public static final String USERNAME = "technician";

	@Autowired
	private QuotationService quotationService;
	@Autowired
	private JobService jobService;
	
	@Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }

	@Test
	public void testEmptyForm() throws Exception {
		JobToQuotationForm jqf = new JobToQuotationForm();
		Job j = jobService.getReference(1);
		jqf.setJob(j);
		
		
		this.mockMvc.perform(
				post("/jobtoquotation.htm")
				.param("jobid", "1")
				.requestAttr("form", jqf)
				.sessionAttr(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME)
			)
		.andDo(print())
        .andExpect(status().isOk());
	}

	@Transactional
	@Test
	public void testSortQuotation() throws Exception {
		
		ResultActions ra = this.mockMvc.perform(
				post("/quotesort.htm")
				.param("quoteid", "1")
				.param("sortType", "ID")
				.param("submit", "Update")
			)
		.andDo(print());
        
		boolean success = true;
		Quotation quote = quotationService.get(1);
		for(int count = 1; count <= quote.getQuotationitems().size(); count++){
			if(!success) break;
			final int c = count;
			success = quote.getQuotationitems().stream().anyMatch(qit -> qit.getItemno().equals(c));
		}
		ra.andExpect(success ? status().is3xxRedirection() : status().is2xxSuccessful());
	}
	
}
