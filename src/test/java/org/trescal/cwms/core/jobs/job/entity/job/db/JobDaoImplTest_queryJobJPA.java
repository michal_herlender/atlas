package org.trescal.cwms.core.jobs.job.entity.job.db;

import static org.junit.Assert.assertEquals;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.job.form.JobSearchForm;
import org.trescal.cwms.core.jobs.job.projection.JobProjectionDTO;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.tests.MvcCwmsTestClass;

public class JobDaoImplTest_queryJobJPA extends MvcCwmsTestClass {
	public static final String DATA_SET = "cwms/jobs/job/JobDaoImplTest.xml";
	private static final SimpleDateFormat df = DateTools.sqlserverformat;
	private static final Logger logger = LoggerFactory.getLogger(JobDaoImplTest_queryJobJPA.class);

	@Autowired
	private JobDao jobDao;
	
	@Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }
	
	private void compareJobIds(int[] expected, PagedResultSet<JobProjectionDTO> rs) {
		int[] actual = getJobIds(rs);
		
		if (!Arrays.equals(expected, actual)) {
			logger.error("Actual results:" + rs.getResults().toString());

			String message = "expected " + Arrays.toString(expected) +
				" but was " +
				Arrays.toString(actual);
			Assert.fail(message);
		}
	}
	
	private int[] getJobIds(PagedResultSet<JobProjectionDTO> rs) {
		return rs.getResults().stream().
			mapToInt(JobProjectionDTO::getJobid).toArray();
	}
	
	@Test
	public void testEmptyForm() {
		JobSearchForm jsf = new JobSearchForm();
		PagedResultSet<JobProjectionDTO> rs = new PagedResultSet<>(0, 0);
		
		// Test with no parameters set on form
		jobDao.queryJobJPANew(jsf, rs);
		
		// We expect that all results should be returned
		assertEquals(4, rs.getResultsCount());
		
		// primary order is by regdate (newest first)
		compareJobIds(new int[] {4, 3, 2, 1}, rs);
	}

	@Test
	public void testSingle_Active() {
		JobSearchForm jsf = new JobSearchForm();
		PagedResultSet<JobProjectionDTO> rs = new PagedResultSet<>(0, 0);
		jsf.setActive(true);
		jobDao.queryJobJPANew(jsf, rs);

		compareJobIds(new int[]{2, 1}, rs);
	}

	@Test
	public void testSingle_BookedInDateOn() {
		JobSearchForm jsf = new JobSearchForm();
		PagedResultSet<JobProjectionDTO> rs = new PagedResultSet<>(0, 0);
		jsf.setBookedInDate1(LocalDate.parse("2019-03-02"));
		jobDao.queryJobJPANew(jsf, rs);

		compareJobIds(new int[]{3}, rs);
	}

	@Test
	public void testSingle_BookedInDateBetween() {
		JobSearchForm jsf = new JobSearchForm();
		PagedResultSet<JobProjectionDTO> rs = new PagedResultSet<>(0, 0);
		jsf.setBookedInDate1(LocalDate.parse("2019-03-01"));
		jsf.setBookedInDate2(LocalDate.parse("2019-04-30"));
		jsf.setBookedInDateBetween(true);

		jobDao.queryJobJPANew(jsf, rs);

		compareJobIds(new int[]{4, 3}, rs);
	}

	@Test
	public void testSingle_ClientRef() {
		JobSearchForm jsf = new JobSearchForm();
		PagedResultSet<JobProjectionDTO> rs = new PagedResultSet<>(0, 0);
		jsf.setClientRef("CR4");
		jobDao.queryJobJPANew(jsf, rs);

		compareJobIds(new int[]{4}, rs);
	}

	@Test
	public void testSingle_Coid() {
		JobSearchForm jsf = new JobSearchForm();
		PagedResultSet<JobProjectionDTO> rs = new PagedResultSet<>(0, 0);
		jsf.setCoid("2");
		jobDao.queryJobJPANew(jsf, rs);

		compareJobIds(new int[]{4, 3}, rs);
	}

	@Test
	public void testSingle_Coid_OnBehalf() {
		JobSearchForm jsf = new JobSearchForm();
		PagedResultSet<JobProjectionDTO> rs = new PagedResultSet<>(0, 0);
		jsf.setCoid("5");
		jobDao.queryJobJPANew(jsf, rs);

		compareJobIds(new int[]{4, 2}, rs);
	}

	@Test
	public void testSingle_CompletedDateOn() {
		JobSearchForm jsf = new JobSearchForm();
		PagedResultSet<JobProjectionDTO> rs = new PagedResultSet<>(0, 0);
		jsf.setCompletedDate1(LocalDate.parse("2019-03-15"));
		jobDao.queryJobJPANew(jsf, rs);

		compareJobIds(new int[]{3}, rs);
	}

	@Test
	public void testSingle_CompletedDateBetween() {
		JobSearchForm jsf = new JobSearchForm();
		PagedResultSet<JobProjectionDTO> rs = new PagedResultSet<>(0, 0);
		jsf.setCompletedDate1(LocalDate.parse("2019-04-01"));
		jsf.setCompletedDate2(LocalDate.parse("2019-04-30"));
		jsf.setCompletedDateBetween(true);
		jobDao.queryJobJPANew(jsf, rs);

		compareJobIds(new int[]{4}, rs);
	}

	@Test
	public void testSingle_Desc() {
		JobSearchForm jsf = new JobSearchForm();
		PagedResultSet<JobProjectionDTO> rs = new PagedResultSet<>(0, 0);
		jsf.setDesc("family 1");
		jobDao.queryJobJPANew(jsf, rs);

		compareJobIds(new int[]{4, 2}, rs);
	}

	@Test
	public void testSingle_Descid() {
		JobSearchForm jsf = new JobSearchForm();
		PagedResultSet<JobProjectionDTO> rs = new PagedResultSet<>(0, 0);
		jsf.setDescid(2);
		jobDao.queryJobJPANew(jsf, rs);

		compareJobIds(new int[]{4, 2}, rs);
	}

	@Test
	public void testSingle_JobNo() {
		JobSearchForm jsf = new JobSearchForm();
		PagedResultSet<JobProjectionDTO> rs = new PagedResultSet<>(0, 0);
		jsf.setJobno("SD3");
		jobDao.queryJobJPANew(jsf, rs);

		compareJobIds(new int[]{2, 1}, rs);
	}

	@Test
	public void testSingle_JobType() {
		JobSearchForm jsf = new JobSearchForm();
		PagedResultSet<JobProjectionDTO> rs = new PagedResultSet<>(0, 0);
		jsf.setJobType(JobType.STANDARD);
		jobDao.queryJobJPANew(jsf, rs);

		compareJobIds(new int[]{2, 1}, rs);
	}

	@Test
	public void testSingle_Mfr() {
		JobSearchForm jsf = new JobSearchForm();
		PagedResultSet<JobProjectionDTO> rs = new PagedResultSet<>(0, 0);
		jsf.setMfr("Mfr 1");
		jobDao.queryJobJPANew(jsf, rs);

		compareJobIds(new int[]{4, 2}, rs);
	}

	@Test
	public void testSingle_Mfrid() {
		JobSearchForm jsf = new JobSearchForm();
		PagedResultSet<JobProjectionDTO> rs = new PagedResultSet<>(0, 0);
		jsf.setMfrid(1);
		jobDao.queryJobJPANew(jsf, rs);

		compareJobIds(new int[]{4, 2}, rs);
	}

	@Test
	public void testSingle_Modelname() {
		JobSearchForm jsf = new JobSearchForm();
		PagedResultSet<JobProjectionDTO> rs = new PagedResultSet<>(0, 0);
		jsf.setModel("Model 2");
		jobDao.queryJobJPANew(jsf, rs);

		compareJobIds(new int[]{2}, rs);
	}

	@Test
	public void testSingle_Modelid() {
		JobSearchForm jsf = new JobSearchForm();
		PagedResultSet<JobProjectionDTO> rs = new PagedResultSet<>(0, 0);
		jsf.setModelid(2);
		jobDao.queryJobJPANew(jsf, rs);

		compareJobIds(new int[]{2}, rs);
	}

	@Test
	public void testSingle_Orgid() {
		JobSearchForm jsf = new JobSearchForm();
		PagedResultSet<JobProjectionDTO> rs = new PagedResultSet<>(0, 0);
		jsf.setOrgId("3");
		jobDao.queryJobJPANew(jsf, rs);

		compareJobIds(new int[]{2, 1}, rs);
	}

	@Test
	public void testSingle_Personid() {
		JobSearchForm jsf = new JobSearchForm();
		PagedResultSet<JobProjectionDTO> rs = new PagedResultSet<>(0, 0);
		jsf.setPersonid("1");
		jobDao.queryJobJPANew(jsf, rs);

		compareJobIds(new int[]{2, 1}, rs);
	}

	@Test
	public void testSingle_Plantid() {
		JobSearchForm jsf = new JobSearchForm();
		PagedResultSet<JobProjectionDTO> rs = new PagedResultSet<>(0, 0);
		jsf.setPlantid("2");
		jobDao.queryJobJPANew(jsf, rs);

		compareJobIds(new int[]{2}, rs);
	}

	@Test
	public void testSingle_Plantno() {
		JobSearchForm jsf = new JobSearchForm();
		PagedResultSet<JobProjectionDTO> rs = new PagedResultSet<>(0, 0);
		jsf.setPlantno("45");
		jobDao.queryJobJPANew(jsf, rs);

		compareJobIds(new int[]{4}, rs);
	}

	@Test
	public void testSingle_Procid() {
		JobSearchForm jsf = new JobSearchForm();
		PagedResultSet<JobProjectionDTO> rs = new PagedResultSet<>(0, 0);
		jsf.setProcId(2);
		jobDao.queryJobJPANew(jsf, rs);

		compareJobIds(new int[]{2}, rs);
	}

	@Test
	public void testSingle_PurOrder() {
		JobSearchForm jsf = new JobSearchForm();
		PagedResultSet<JobProjectionDTO> rs = new PagedResultSet<>(0, 0);
		jsf.setPurOrder("PO 2");
		jobDao.queryJobJPANew(jsf, rs);

		compareJobIds(new int[]{2}, rs);
	}

	@Test
	public void testSingle_ReturnTo() {
		JobSearchForm jsf = new JobSearchForm();
		PagedResultSet<JobProjectionDTO> rs = new PagedResultSet<>(0, 0);
		jsf.setReturnTo("Two");
		jobDao.queryJobJPANew(jsf, rs);

		compareJobIds(new int[]{4, 3}, rs);
	}

	@Test
	public void testSingle_Serialno() {
		JobSearchForm jsf = new JobSearchForm();
		PagedResultSet<JobProjectionDTO> rs = new PagedResultSet<>(0, 0);
		jsf.setSerialno("45");
		jobDao.queryJobJPANew(jsf, rs);

		compareJobIds(new int[]{4}, rs);
	}

	@Test
	public void testSingle_Subdivid() {
		JobSearchForm jsf = new JobSearchForm();
		PagedResultSet<JobProjectionDTO> rs = new PagedResultSet<>(0, 0);
		jsf.setSubdivid(1);
		jobDao.queryJobJPANew(jsf, rs);

		compareJobIds(new int[]{2, 1}, rs);
	}

	@Test
	public void testFullFormWithIds() {
		JobSearchForm jsf = new JobSearchForm();
		PagedResultSet<JobProjectionDTO> rs = new PagedResultSet<>(0, 0);

		// Test with all parameters set on form (using IDs instead of names where both options exist)
		jsf.setActive(false);
		// TODO issue with date vs datetime (change field to date?)
		jsf.setBookedInDate1(LocalDate.parse("2019-03-31"));
		jsf.setBookedInDate2(LocalDate.parse("2019-04-02"));
		jsf.setBookedInDateBetween(true);
		jsf.setClientRef("CR4");
		jsf.setCoid("2");
		jsf.setCompletedDate1(LocalDate.parse("2019-04-14"));
		jsf.setCompletedDate2(LocalDate.parse("2019-04-16"));
		jsf.setCompletedDateBetween(true);
		jsf.setDesc("");
		jsf.setDescid(2);
		jsf.setForced(null);
		jsf.setHighlight(true);
		jsf.setJobno("SD4");
		jsf.setJobType(JobType.SITE);
		jsf.setMfr("");
		jsf.setMfrid(3);    // On inst
		jsf.setModel("");
		jsf.setModelid(4);
		jsf.setOrgId("4");
		jsf.setPersonid("2");
		jsf.setPlantid("4");
		jsf.setPlantno("A456");
		jsf.setProcId(4);
		jsf.setPurOrder("PO 4");
		jsf.setReturnTo("Two");
		jsf.setSerialno("SN456");
		jsf.setSubdivid(2);
		
		jobDao.queryJobJPANew(jsf, rs);
		
		// We expect that only job id 4 should be returned (for job item 4)
		assertEquals(1, rs.getResultsCount());
		JobProjectionDTO dto = rs.getResults().iterator().next();

		// Test the projection values, only in this test
		assertEquals(4, dto.getJobid().intValue());
		assertEquals(4, dto.getBpoId().intValue());
		assertEquals("CR4", dto.getClientRef());
		assertEquals(2, dto.getContactid().intValue());
		assertEquals(4, dto.getDefaultPoId().intValue());
		assertEquals(4, dto.getJobid().intValue());
		assertEquals("JOB-SD4-004", dto.getJobno());
		assertEquals("2019-04-01", df.format(dto.getReceiptDate()));
		assertEquals("2019-04-02", dto.getRegDate().format(DateTimeFormatter.ISO_DATE));
	}	
}