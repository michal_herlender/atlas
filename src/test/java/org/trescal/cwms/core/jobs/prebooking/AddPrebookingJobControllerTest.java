package org.trescal.cwms.core.jobs.prebooking;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;

import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.trescal.cwms.core.logistics.controller.AddPrebookingJobController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.tests.MvcCwmsTestClass;

public class AddPrebookingJobControllerTest extends MvcCwmsTestClass {
	public static final String DATA_SET = "cwms/jobs/job/CreatePrebookingJobFromFile.xml";
	public static final String EXCEL_FILE = "cwms/jobs/job/addPrebookingFromFile.xlsx";
	public static final String EXCEL_FILE_EMPTY = "cwms/jobs/job/addPrebookingFromFileEmpty.xlsx";
	public static final String EXCEL_FILE_MANDATORY = "cwms/jobs/job/addPrebookingFromFileMandatory.xlsx";
	public static final String EXCEL_FILE_EXCHANGEFORMAT = "cwms/jobs/job/addPrebookingFromFileExchangeFormat.xlsx";
	public static final String EXCEL_FILE_CHECKDATA = "cwms/jobs/job/addPrebookingFromFileCheckData.xlsx";
	public static final String USERNAME = "technician";

	@Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }
	
	private Map<String, Object> getSessionAttributes() {
		Map<String, Object> sessionAttributes = new HashMap<>();
		sessionAttributes.put(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME);
		KeyValue<Integer, String> subdivDto = new KeyValue<>(1, "Madrid");
		sessionAttributes.put(Constants.SESSION_ATTRIBUTE_SUBDIV, subdivDto);
		return sessionAttributes;
	}
	
	/**
	 * Expectation : Controller should return correct reference data from a GET
	 * - correct view name
	 * - objects in reference data map 
	 */
	@Test
	public void testReferenceData() throws Exception {
		
		this.mockMvc
			.perform(MockMvcRequestBuilders.get("/addprebookingjob.htm").sessionAttrs(getSessionAttributes()))
			.andExpect(MockMvcResultMatchers.view().name("trescal/core/jobs/job/addprebookingjob"));
	}

	@Test
	public void testprebooking() throws Exception {

		MultiValueMap<String, String> inputPayload = new LinkedMultiValueMap<>();
		inputPayload.add("addrid", "1");
		inputPayload.add("exchangeFormat", "1");
		inputPayload.add("jobType", "STANDARD");
		inputPayload.add("coid", "2");
		inputPayload.add("coname", "saf");
		inputPayload.add("subdivid", "2");
		inputPayload.add("personid", "2");
		inputPayload.add("addrid", "2");
		inputPayload.add("recordType", "FILE");
		inputPayload.add("currencyCode", "GBP");
		inputPayload.add("bookedInAddrId", "2");
		inputPayload.add("bookedInLocId", "0");

		InputStream is = getClass().getResourceAsStream("classpath:test-data/" + EXCEL_FILE);
		MockMultipartFile file = new MockMultipartFile("uploadFile", "addPrebookingFromFile.xlsx", "text/plain",
				IOUtils.toByteArray(is));

		this.mockMvc
				.perform(fileUpload("/addprebookingjob.htm").file(file).params(inputPayload)
						.sessionAttrs(getSessionAttributes()))
				.andExpect(redirectedUrl("effilesynthesis.htm")).andExpect(model().hasNoErrors())
				.andExpect(model().errorCount(0)).andDo(print());
	}

	/**
	 * Excel file is not exist
	 */
	@Test
	public void testFileExelIsNotExist() throws Exception {

		MultiValueMap<String, String> inputPayload = new LinkedMultiValueMap<>();
		inputPayload.add("addrid", "1");
		inputPayload.add("exchangeFormat", "1");
		inputPayload.add("jobType", "STANDARD");
		inputPayload.add("coid", "2");
		inputPayload.add("coname", "saf");
		inputPayload.add("subdivid", "2");
		inputPayload.add("personid", "2");
		inputPayload.add("addrid", "2");
		inputPayload.add("recordType", "FILE");
		inputPayload.add("currencyCode", "GBP");
		inputPayload.add("bookedInAddrId", "2");
		inputPayload.add("bookedInLocId", "0");

		InputStream is = getClass().getResourceAsStream("classpath:test-data/" + EXCEL_FILE_MANDATORY);
		MockMultipartFile file = new MockMultipartFile("uploadFile", "NotExistFile.xlsx", "text/plain",
				IOUtils.toByteArray(is));

		MvcResult result = this.mockMvc.perform(
				fileUpload("/addprebookingjob.htm").file(file).params(inputPayload).sessionAttrs(getSessionAttributes()))
				.andReturn();
		BindingResult bindingResult = (BindingResult) result.getModelAndView().getModel()
				.get(BindingResult.MODEL_KEY_PREFIX + AddPrebookingJobController.FORM);

		assertEquals("pbjf", bindingResult.getObjectName());
		assertEquals(1, bindingResult.getErrorCount());
		assertEquals("exchangeformat.import.validation.mandatorycolumns.missingdata",
				bindingResult.getGlobalError().getCode());
	}

	/**
	 * Excel file is empty
	 */
	@Test
	public void testFileIsEmpty() throws Exception {
		MultiValueMap<String, String> inputPayload = new LinkedMultiValueMap<>();
		inputPayload.add("addrid", "1");
		inputPayload.add("exchangeFormat", "1");
		inputPayload.add("jobType", "STANDARD");
		inputPayload.add("coid", "2");
		inputPayload.add("coname", "saf");
		inputPayload.add("subdivid", "2");
		inputPayload.add("personid", "2");
		inputPayload.add("addrid", "2");
		inputPayload.add("recordType", "FILE");
		inputPayload.add("currencyCode", "GBP");
		inputPayload.add("bookedInAddrId", "2");
		inputPayload.add("bookedInLocId", "0");

		InputStream is = getClass().getResourceAsStream("classpath:test-data/" + EXCEL_FILE_EMPTY);
		MockMultipartFile file = new MockMultipartFile("uploadFile", "addPrebookingFromFileEmpty.xlsx", "text/plain",
				IOUtils.toByteArray(is));

		MvcResult result = this.mockMvc.perform(
				fileUpload("/addprebookingjob.htm").file(file).params(inputPayload).sessionAttrs(getSessionAttributes()))
				.andReturn();
		BindingResult bindingResult = (BindingResult) result.getModelAndView().getModel()
				.get(BindingResult.MODEL_KEY_PREFIX + AddPrebookingJobController.FORM);

		assertEquals("pbjf", bindingResult.getObjectName());
		assertEquals(1, bindingResult.getErrorCount());
		assertEquals("exchangeformat.emptyfile", bindingResult.getGlobalError().getCode());
	}

	/**
	 * Test case : The structure of the exchange format is different from the excel file
	 * structure
	 * Expectation : That the validation fails and there is missing data
	 */
	@Test
	public void testExchangeFormat() throws Exception {

		MultiValueMap<String, String> inputPayload = new LinkedMultiValueMap<>();
		inputPayload.add("addrid", "1");
		inputPayload.add("exchangeFormat", "1");
		inputPayload.add("jobType", "STANDARD");
		inputPayload.add("coid", "2");
		inputPayload.add("coname", "saf");
		inputPayload.add("subdivid", "2");
		inputPayload.add("personid", "2");
		inputPayload.add("addrid", "2");
		inputPayload.add("recordType", "FILE");
		inputPayload.add("currencyCode", "GBP");
		inputPayload.add("bookedInAddrId", "2");
		inputPayload.add("bookedInLocId", "0");

		InputStream is = getClass().getResourceAsStream("classpath:test-data/" + EXCEL_FILE_EXCHANGEFORMAT);
		MockMultipartFile file = new MockMultipartFile("uploadFile", "addPrebookingFromFileExchangeFormat.xlsx",
				"text/plain", IOUtils.toByteArray(is));

		MvcResult result = this.mockMvc.perform(
				fileUpload("/addprebookingjob.htm").file(file).params(inputPayload).sessionAttrs(getSessionAttributes()))
				.andReturn();
//		result.getModelAndView().getModel().get
//		FlashMap flashMap = result.getFlashMap();
//		assertTrue(flashMap.containsKey("pbjf"));
		
		
		BindingResult bindingResult = (BindingResult) result.getModelAndView().getModel()
				.get(BindingResult.MODEL_KEY_PREFIX + AddPrebookingJobController.FORM);

		assertEquals("pbjf", bindingResult.getObjectName());
		assertEquals(1, bindingResult.getErrorCount());
		assertEquals("exchangeformat.import.validation.mandatorycolumns.missingdata",
				bindingResult.getGlobalError().getCode());
	}

	/**
	 * Mandatory column IDTrescal is empty missing mandatory field
	 */
	@Test
	public void testMandatoryColumns() throws Exception {

		MultiValueMap<String, String> inputPayload = new LinkedMultiValueMap<>();
		inputPayload.add("addrid", "1");
		inputPayload.add("exchangeFormat", "1");
		inputPayload.add("jobType", "STANDARD");
		inputPayload.add("coid", "2");
		inputPayload.add("coname", "saf");
		inputPayload.add("subdivid", "2");
		inputPayload.add("personid", "2");
		inputPayload.add("addrid", "2");
		inputPayload.add("recordType", "FILE");
		inputPayload.add("currencyCode", "GBP");
		inputPayload.add("bookedInAddrId", "2");
		inputPayload.add("bookedInLocId", "0");

		InputStream is = getClass().getResourceAsStream("classpath:test-data/" + EXCEL_FILE_MANDATORY);
		MockMultipartFile file = new MockMultipartFile("uploadFile", "addPrebookingFromFileMondatory.xlsx",
				"text/plain", IOUtils.toByteArray(is));

		MvcResult result = this.mockMvc.perform(
				fileUpload("/addprebookingjob.htm").file(file).params(inputPayload).sessionAttrs(getSessionAttributes()))
				.andReturn();
		BindingResult bindingResult = (BindingResult) result.getModelAndView().getModel()
				.get(BindingResult.MODEL_KEY_PREFIX + AddPrebookingJobController.FORM);
		assertEquals("pbjf", bindingResult.getObjectName());
		assertEquals(1, bindingResult.getErrorCount());
		assertEquals("exchangeformat.import.validation.mandatorycolumns.missingdata",
				bindingResult.getGlobalError().getCode());
	}

	/**
	 * Test case:
	 * Mandatory Columns With Invalid data 
	 * 
	 * Expectation : 
	 * Reference data view is returned (not a redirect)
	 * Error message "column with invalid data: IDTrescal is not an integer"
	 */
	@Test
	public void testCheckDataMandatoryColumns() throws Exception {

		MultiValueMap<String, String> inputPayload = new LinkedMultiValueMap<>();
		inputPayload.add("addrid", "1");
		inputPayload.add("exchangeFormat", "1");
		inputPayload.add("jobType", "STANDARD");
		inputPayload.add("coid", "2");
		inputPayload.add("coname", "saf");
		inputPayload.add("subdivid", "2");
		inputPayload.add("personid", "2");
		inputPayload.add("addrid", "2");
		inputPayload.add("recordType", "FILE");
		inputPayload.add("currencyCode", "GBP");
		inputPayload.add("bookedInAddrId", "2");
		inputPayload.add("bookedInLocId", "0");

		InputStream is = getClass().getResourceAsStream("classpath:test-data/" + EXCEL_FILE_CHECKDATA);
		MockMultipartFile file = new MockMultipartFile("uploadFile", "addPrebookingFromFileCheckData.xlsx",
				"text/plain", IOUtils.toByteArray(is));

		MvcResult result = this.mockMvc.perform(
				fileUpload("/addprebookingjob.htm").file(file).params(inputPayload).sessionAttrs(getSessionAttributes()))
				
				.andExpect(MockMvcResultMatchers.redirectedUrl("effilesynthesis.htm"))
//				.andExpect(MockMvcResultMatchers.view().name("trescal/core/jobs/job/addprebookingjob"))
				.andReturn();
		BindingResult bindingResult = (BindingResult) result.getModelAndView().getModel()
				.get(BindingResult.MODEL_KEY_PREFIX + AddPrebookingJobController.FORM);

		assertEquals("pbjf", bindingResult.getObjectName());
		assertEquals(1, bindingResult.getErrorCount());
		assertEquals("exchangeformat.import.validation.columnswithinvaliddata",
				bindingResult.getGlobalError().getCode());
	}

	/**
	 * File is not excel (not xlsx or xls)
	 * 
	 */
	@Test
	public void testFileNotExcel() throws Exception {

		MultiValueMap<String, String> inputPayload = new LinkedMultiValueMap<>();
		inputPayload.add("addrid", "1");
		inputPayload.add("exchangeFormat", "1");
		inputPayload.add("jobType", "STANDARD");
		inputPayload.add("coid", "1");
		inputPayload.add("coname", "saf");
		inputPayload.add("subdivid", "1");
		inputPayload.add("personid", "1");
		inputPayload.add("addrid", "2");
		inputPayload.add("recordType", "FILE");
		inputPayload.add("currencyCode", "GBP");
		inputPayload.add("bookedInAddrId", "2");
		inputPayload.add("bookedInLocId", "0");

		InputStream is = getClass().getResourceAsStream("classpath:test-data/" + EXCEL_FILE_MANDATORY);
		MockMultipartFile file = new MockMultipartFile("uploadFile", "CheckStatus.xml", "text/plain",
				IOUtils.toByteArray(is));

		MvcResult result = this.mockMvc.perform(
				fileUpload("/addprebookingjob.htm").file(file).params(inputPayload).sessionAttrs(getSessionAttributes()))
				.andReturn();
		BindingResult bindingResult = (BindingResult) result.getModelAndView().getModel()
				.get(BindingResult.MODEL_KEY_PREFIX + AddPrebookingJobController.FORM);

		assertEquals("pbjf", bindingResult.getObjectName());
		assertEquals(1, bindingResult.getErrorCount());
		assertEquals(1, bindingResult.getFieldErrorCount());
		assertEquals("uploadFile", bindingResult.getFieldError().getField());
	}
}
