package org.trescal.cwms.core.jobs.repair.repairinspectionreport.db;

import io.vavr.Tuple;
import io.vavr.Tuple3;
import io.vavr.collection.List;
import io.vavr.control.Either;
import lombok.val;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.trescal.cwms.core.AtlasUnitTest;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.WorkRequirement;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.AccreditationLevel;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.db.CapabilityAuthorizationService;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;

import javax.validation.constraints.NotNull;
import java.util.function.BiPredicate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@Category(AtlasUnitTest.class)
class RepairInspectionReportServiceImplTest {

    @Mock
    CapabilityAuthorizationService capabilityAuthorizationService;

    @InjectMocks
    RepairInspectionReportService service = new RepairInspectionReportServiceImpl();


    @Test
    void isAccreditedToPerformRirInitialization() {
        val cases = List.of(
            Tuple.of(1, 1, true),
            Tuple.of(2, 1, true),
            Tuple.of(3, 1, true),
            Tuple.of(4, 1, true),
            Tuple.of(5, 1, false),
            Tuple.of(6, 1, false),
            Tuple.of(7, 9, false)
        );
        runTestCases(cases, service::isAccreditedToPerformRirInitialization);
    }

    private void runTestCases(@NotNull List<Tuple3<Integer, Integer, Boolean>> cases, BiPredicate<JobItemWorkRequirement, Contact> predicate) {
        initMock();
        cases.forEach(t -> assertEquals(t._3, predicate.test(createJobItemWorkRequerementForId(t._1, t._2), createContact())));
    }

    @Test
    void isAccreditedToPerformRirTechCompletion() {
        val cases = List.of(
            Tuple.of(1, 1, true),
            Tuple.of(2, 1, true),
            Tuple.of(3, 1, false),
            Tuple.of(4, 1, false),
            Tuple.of(5, 1, false),
            Tuple.of(6, 1, false),
            Tuple.of(7, 9, false)
        );
        runTestCases(cases, service::isAccreditedToPerformRirTechCompletion);
    }

    @Test
    void isAccreditedToPerformRirManagerValidation() {
        val cases = List.of(
            Tuple.of(1, 1, false),
            Tuple.of(2, 1, true),
            Tuple.of(3, 1, false),
            Tuple.of(4, 1, false),
            Tuple.of(5, 1, false),
            Tuple.of(6, 1, false),
            Tuple.of(7, 9, false)
        );
        runTestCases(cases, service::isAccreditedToPerformRirManagerValidation);
    }

    private @NotNull JobItemWorkRequirement createJobItemWorkRequerementForId(Integer id, Integer calibrationTypeId) {
        val jiw = new JobItemWorkRequirement();
        val wr = new WorkRequirement();
        val serviceType = new ServiceType();
        val calibrationType = new CalibrationType();
        calibrationType.setCalTypeId(calibrationTypeId);
        serviceType.setCalibrationType(calibrationType);
        val capab = new Capability();
        capab.setId(id);
        wr.setServiceType(serviceType);
        wr.setCapability(capab);
        jiw.setWorkRequirement(wr);
        return jiw;
    }

    private @NotNull Contact createContact() {
        val contact = new Contact();
        contact.setPersonid(1);
        return contact;
    }

    private void initMock() {
        when(capabilityAuthorizationService.authorizedFor(eq(1), anyInt(), anyInt())).thenReturn(Either.right(AccreditationLevel.SIGN));
        when(capabilityAuthorizationService.authorizedFor(eq(2), anyInt(), anyInt())).thenReturn(Either.right(AccreditationLevel.APPROVE));
        when(capabilityAuthorizationService.authorizedFor(eq(3), anyInt(), anyInt())).thenReturn(Either.right(AccreditationLevel.PERFORM));
        when(capabilityAuthorizationService.authorizedFor(eq(4), anyInt(), anyInt())).thenReturn(Either.right(AccreditationLevel.SUPERVISED));
        when(capabilityAuthorizationService.authorizedFor(eq(5), anyInt(), anyInt())).thenReturn(Either.right(AccreditationLevel.NONE));
        when(capabilityAuthorizationService.authorizedFor(eq(6), anyInt(), anyInt())).thenReturn(Either.right(AccreditationLevel.REMOVED));
        when(capabilityAuthorizationService.authorizedFor(anyInt(), eq(9), anyInt())).thenReturn(Either.left("Not Authorized"));
    }


}