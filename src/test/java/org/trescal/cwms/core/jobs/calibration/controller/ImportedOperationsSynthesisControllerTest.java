package org.trescal.cwms.core.jobs.calibration.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.validation.BindingResult;
import org.trescal.cwms.batch.jobs.operationsimport.ImportedOperationItem;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.documents.excel.utils.ExcelFileReaderUtil;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.form.ImportCalibrationsForm;
import org.trescal.cwms.core.jobs.calibration.form.ImportedOperationSynthesisForm;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.tests.MvcCwmsTestClass;
import org.trescal.cwms.tests.generic.DataSetCore;

import io.florianlopes.spring.test.web.servlet.request.MockMvcRequestBuilderUtils;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ImportedOperationsSynthesisControllerTest extends MvcCwmsTestClass {

	private static final int ALLOCATED_SUBDIV_ID = 1;
	private static final String DATA_SET = "cwms/jobs/Calibration/ImportedCalibrationSynthesis.xml";
	private static final String WORKFLOW_DATA_SET = "GeneratedWorkflowDataSet.xml";
	public static final String EXCEL_FILE = "cwms/jobs/Calibration/importedCalibrationSynthesis.xlsx";
	private static final String CSR = "csr";

	@Autowired
	private JobService jobService;
	@Autowired
	private ExcelFileReaderUtil excelFileReaderUtil;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private CertificateService certificateService;

	@Override
	protected List<String> getDataSetFileNames() {
		List<String> res = new ArrayList<>(Arrays.asList(DATA_SET, WORKFLOW_DATA_SET));
		res.addAll(DataSetCore.getAllBaseData());
		return res;
	}

	private MvcResult loadFile() throws Exception {

		InputStream is = getClass().getResourceAsStream("classpath:test-data/" + EXCEL_FILE);
		MockMultipartFile file = new MockMultipartFile("file", "importcalibration.xlsx", "text/plain",
			IOUtils.toByteArray(is));
		List<LinkedCaseInsensitiveMap<String>> fileContent = excelFileReaderUtil.readExcelFile(file.getInputStream(),
			null, null);

		ImportCalibrationsForm form = new ImportCalibrationsForm();
		form.setExchangeFormatId(1);
		form.setFile(file);
		form.setSubdivid(2);

		return this.mockMvc
			.perform(get("/importedoperationssynthesis.htm").flashAttr("fileContent", fileContent)
				.sessionAttrs(getSessionAttributes()).flashAttr("importCalibrationsForm", form))
			.andDo(print()).andReturn();
	}

	private Map<String, Object> getSessionAttributes() {
		Map<String, Object> sessionAttributes = new HashMap<>();
		sessionAttributes.put(Constants.SESSION_ATTRIBUTE_USERNAME, CSR);
		KeyValue<Integer, String> subdivDto = new KeyValue<>(ALLOCATED_SUBDIV_ID, "Vendome");
		sessionAttributes.put(Constants.SESSION_ATTRIBUTE_SUBDIV, subdivDto);
		return sessionAttributes;
	}

	@Test
	@Transactional
	public void a_loadTest() throws Exception {

		MvcResult result = loadFile();

		assertEquals(200, result.getResponse().getStatus());
	}

	@Test
	@Transactional
	public void b_analysisTest() throws Exception {

		MvcResult loadResult = loadFile();
		ImportedOperationSynthesisForm form = (ImportedOperationSynthesisForm) Objects.requireNonNull(loadResult.getModelAndView()).getModel()
			.get(ImportedOperationsSynthesisController.FORM_NAME);

		assertNotNull(form);

		simulateJsAutomatching(form);

		MockMvcRequestBuilderUtils.registerPropertyEditor(Date.class,
			new CustomDateEditor(new SimpleDateFormat("dd/MM/yy HH:mm"), true));

		MvcResult result = this.mockMvc
			.perform(MockMvcRequestBuilderUtils.postForm("/importedoperationssynthesis.htm", form)
				.param("newanalysis", "newanalysis").sessionAttrs(getSessionAttributes()))
			.andDo(print()).andReturn();

		BindingResult resultFormBindingResult = (BindingResult) Objects.requireNonNull(result.getModelAndView()).getModel()
			.get(BindingResult.MODEL_KEY_PREFIX + ImportedOperationsSynthesisController.FORM_NAME);


		assertEquals(200, result.getResponse().getStatus());
		assertEquals(0, resultFormBindingResult.getGlobalErrorCount());

		assertForAnalysisRow1(resultFormBindingResult);

	}

	@Test
	@Transactional
	public void c_submitTest() throws Exception {

		// import file (importcalibration.htm)
		MvcResult loadResult = loadFile();
		ImportedOperationSynthesisForm form = (ImportedOperationSynthesisForm) Objects.requireNonNull(loadResult.getModelAndView()).getModel()
			.get(ImportedOperationsSynthesisController.FORM_NAME);

		assertNotNull(form);

		simulateJsAutomatching(form);

		checkCalibrationsAndAutoContractReview(form);

		MockMvcRequestBuilderUtils.registerPropertyEditor(Date.class,
			new CustomDateEditor(new SimpleDateFormat("dd/MM/yy HH:mm"), true));

		Job job = this.jobService.get(1);
		MvcResult result = this.mockMvc
				.perform(MockMvcRequestBuilderUtils.postForm("/importedoperationssynthesis.htm", form)
						.param("save", "save").sessionAttrs(getSessionAttributes()).flashAttr("job", job))
				.andDo(print()).andReturn();

		assertEquals(302, result.getResponse().getStatus());

		assertCase1(form, job);

	}

	private void assertCase1(ImportedOperationSynthesisForm form, Job job) {
		List<Certificate> certs = certificateService.getByThirdCertNoAndJobId(form.getRows().get(0).getDocumentNumber(),
				job.getJobid());
		Certificate cert = certs.stream().findFirst().orElse(null);
		assertNotNull(cert);
		assertTrue(cert.getOptimization());
		Calibration cal = cert.getCal();
		assertNotNull(cal);
	}

	private void checkCalibrationsAndAutoContractReview(ImportedOperationSynthesisForm form) {
		form.getRows().forEach(r -> r.setChecked(true));
		form.getRows().stream().filter(r -> !r.getContractReviewed()).forEach(r -> r.setAutomaticContractReview(true));
	}

	/**
	 * A successful row with no errors
	 */
	private void assertForAnalysisRow1(BindingResult br) {
		assertEquals(0, br.getFieldErrors("rows[0].*").size());
	}

	private void simulateJsAutomatching(ImportedOperationSynthesisForm form) {

		List<Job> onsiteJos = jobService.getActiveJobsBySubdivAndAllocatedSubdiv(subdivService.get(form.getSubdivid()),
			subdivService.get(ALLOCATED_SUBDIV_ID)).stream().filter(j -> j.getType().equals(JobType.SITE)).collect(Collectors.toList());

		for (ImportedOperationItem row : form.getRows()) {

			// find jobitem based on instrument plantid
			if (row.getPlantid() != null) {
				JobItem jobitem = onsiteJos.stream().map(Job::getItems).flatMap(Collection::stream)
					.filter(j -> j.getInst().getPlantid() == row.getPlantid()
						|| j.getInst().getPlantno().equals(row.getPlantno()))
					.findFirst().orElseGet(null);
				if (jobitem != null) {
					row.setJobitemId(jobitem.getJobItemId());
					if (form.getJobId() == null)
						form.setJobId(jobitem.getJobItemId());
				}

				List<JobItemWorkRequirement> jiwrs = Objects.requireNonNull(jobitem).getWorkRequirements();

				// try to find WR
				JobItemWorkRequirement jiwr;
				if (row.getServiceTypeId() == null) {
					jiwr = jiwrs.stream().findFirst().orElse(null);
				} else {
					jiwr = jiwrs.stream().filter(j -> j.getWorkRequirement().getServiceType().getServiceTypeId() == row
						.getServiceTypeId()).findFirst().orElse(null);
				}
				if (jiwr != null)
					row.setJiWrId(jiwr.getId());

			}
		}

		// so they cannot be encoded
		form.getRows().forEach(r -> {
			r.setInstrument(null);
			r.setJobitem(null);
			r.setOperationBy(null);
			r.setCertificate(null);
			r.setOperationValidatedBy(null);
		});
	}

}
