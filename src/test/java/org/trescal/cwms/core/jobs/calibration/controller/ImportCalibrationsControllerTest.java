package org.trescal.cwms.core.jobs.calibration.controller;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;

import java.io.InputStream;
import java.util.Collections;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.trescal.cwms.core.documents.excel.utils.ExcelFileReaderUtil;
import org.trescal.cwms.tests.MvcCwmsTestClass;

public class ImportCalibrationsControllerTest extends MvcCwmsTestClass {

	public static final String DATA_SET = "cwms/jobs/Calibration/ImportCalibrations.xml";
	public static final String EXCEL_FILE = "cwms/jobs/Calibration/importcalibration.xlsx";
	public static final String Second_EXCEL_FILE = "cwms/jobs/Calibration/importcalibrationEmpty.xlsx";
	public static final String Missing_mandatorycolumns_EXCEL_FILE = "cwms/jobs/Calibration/importcalibration_mandatorycolumns.xlsx";
	public static final String Missing_mandatorydata_EXCEL_FILE = "cwms/jobs/Calibration/importcalibration_mandatorydata.xlsx";

	@Autowired
	private ExcelFileReaderUtil excelFileReaderUtil;

	@Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }

	@Test
	public void testMinimsuccessful() throws Exception {

		MultiValueMap<String, String> inputPayload = new LinkedMultiValueMap<>();
		inputPayload.add("exchangeFormatId", "1");
		inputPayload.add("subdivid", "2");

		InputStream is = getClass().getResourceAsStream("classpath:test-data/" + EXCEL_FILE);
		MockMultipartFile file = new MockMultipartFile("file", "importcalibration.xlsx", "text/plain",
				IOUtils.toByteArray(is));

		MvcResult result = this.mockMvc.perform(fileUpload("/importoperations.htm").file(file).params(inputPayload))
				.andExpect(redirectedUrl("/importedoperationsssynthesis.htm")).andDo(print()).andReturn();

		List<LinkedCaseInsensitiveMap<String>> fileContent = excelFileReaderUtil.readExcelFile(file.getInputStream(),
				null, null);

		assertEquals(result.getFlashMap().get("fileContent").toString(), fileContent.toString());
	}

	@Test
	public void testOnEmptyExcelFile() throws Exception {

		MultiValueMap<String, String> inputPayload = new LinkedMultiValueMap<>();
		inputPayload.add("exchangeFormatId", "1");
		inputPayload.add("subdivid", "2");

		InputStream is = getClass().getResourceAsStream("classpath:test-data/" + Second_EXCEL_FILE);
		MockMultipartFile file = new MockMultipartFile("file", "importcalibrationEmpty.xlsx", "text/plain",
				IOUtils.toByteArray(is));

		MvcResult result = this.mockMvc.perform(fileUpload("/importoperations.htm").file(file).params(inputPayload))
				.andDo(print()).andExpect(model().hasErrors()).andReturn();
		BindingResult formBindingResult = (BindingResult) result.getModelAndView().getModel()
				.get(BindingResult.MODEL_KEY_PREFIX + ImportOperationsController.FORM);

		assertEquals(formBindingResult.getGlobalError().getCode(), "exchangeformat.emptyfile");
	}

	@Test
	public void testOnEmptysubdivid() throws Exception {

		MultiValueMap<String, String> inputPayload = new LinkedMultiValueMap<>();
		inputPayload.add("exchangeFormatId", "1");
		InputStream is = getClass().getResourceAsStream("classpath:test-data/" + EXCEL_FILE);
		MockMultipartFile file = new MockMultipartFile("file", "importcalibration.xlsx", "text/plain",
				IOUtils.toByteArray(is));

		MvcResult result = this.mockMvc.perform(fileUpload("/importoperations.htm").file(file).params(inputPayload))
				.andExpect(model().attributeHasFieldErrors("form", "subdivid")).andDo(print()).andReturn();

		BindingResult formBindingResult = (BindingResult) result.getModelAndView().getModel()
				.get(BindingResult.MODEL_KEY_PREFIX + ImportOperationsController.FORM);

		assertThat(formBindingResult.getFieldError().getCode(),
				containsString("javax.validation.constraints.NotNull.message"));

	}

	@Test
	public void testOnMissingMandatoryColumns() throws Exception {

		MultiValueMap<String, String> inputPayload = new LinkedMultiValueMap<>();
		inputPayload.add("exchangeFormatId", "1");
		inputPayload.add("subdivid", "2");
		InputStream is = getClass().getResourceAsStream("classpath:test-data/" + Missing_mandatorycolumns_EXCEL_FILE);
		MockMultipartFile file = new MockMultipartFile("file", "importcalibration_mandatorycolumns.xlsx", "text/plain",
				IOUtils.toByteArray(is));

		MvcResult result = this.mockMvc.perform(fileUpload("/importoperations.htm").file(file).params(inputPayload))
				.andDo(print()).andReturn();

		BindingResult formBindingResult = (BindingResult) result.getModelAndView().getModel()
				.get(BindingResult.MODEL_KEY_PREFIX + ImportOperationsController.FORM);

		assertEquals(formBindingResult.getGlobalError().getCode(),
				"exchangeformat.import.validation.mandatorycolumns.missing");

	}

	@Test
	public void testOnMissingMandatoryData() throws Exception {

		MultiValueMap<String, String> inputPayload = new LinkedMultiValueMap<>();
		inputPayload.add("exchangeFormatId", "1");
		inputPayload.add("subdivid", "2");
		InputStream is = getClass().getResourceAsStream("classpath:test-data/" + Missing_mandatorydata_EXCEL_FILE);
		MockMultipartFile file = new MockMultipartFile("file", "importcalibration_mandatorydata.xlsx", "text/plain",
				IOUtils.toByteArray(is));

		MvcResult result = this.mockMvc.perform(fileUpload("/importoperations.htm").file(file).params(inputPayload))
				.andDo(print()).andReturn();

		BindingResult formBindingResult = (BindingResult) result.getModelAndView().getModel()
				.get(BindingResult.MODEL_KEY_PREFIX + ImportOperationsController.FORM);

		assertEquals(formBindingResult.getGlobalError().getCode(),
				"exchangeformat.import.validation.mandatorycolumns.missingdata");

	}

}
