package org.trescal.cwms.core.jobs.jobitem.db;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemDao;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocales;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetJob;
import org.trescal.cwms.tests.generic.core.TestDataServiceType;
import org.trescal.cwms.tests.generic.job.TestDataJob;
import org.trescal.cwms.tests.generic.job.TestDataJobItem;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class JobItemDaoImplTest_getJobItemProjectionDTOsByJobIds extends MvcRestTestClass {
	@Autowired
    private  JobItemDao jobitemDao;
	
	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetJob.getJobClientCompletedTestData();
	}

	@Test
	public void testjobIds() {
        Locale locale = SupportedLocales.FRENCH_FR;
        Collection<Integer> jobIds = new ArrayList<>();

        jobIds.add(TestDataJob.ID_CLIENT_FR);
        List<JobItemProjectionDTO> dtos = this.jobitemDao.getJobItemProjectionDTOsByJobIds(jobIds, locale);
        JobItemProjectionDTO dto1 = dtos.get(0);

        assertEquals(dto1.getJobid(), TestDataJob.ID_CLIENT_FR);
        assertEquals(dto1.getJobItemId(), TestDataJobItem.ID_CLIENT_FR_1);


        assertEquals(dto1.getJobid(), TestDataJobItem.JOB_ID_CLIENT_FR);
        // GB 2021-01-27 : Job number intentionally not tested, it's not populated by the Dao call tested here.
//		Assert.assertEquals(dtoa.getJobno(),TestDataJob.JOB_NO_CLIENT_FR );
        assertEquals(dto1.getItemno(), TestDataJobItem.ITEM_NO_FR_1);
        assertEquals(dto1.getPlantid(), TestDataJobItem.PLANT_ID_FR_1);
        assertEquals(dto1.getServiceTypeId(), TestDataServiceType.ID_AC_M_IL);
        assertEquals(dto1.getCapabilityId(), TestDataJobItem.CAPABILITY_ID_FR_1);
        assertEquals(dto1.getStateId(), TestDataJobItem.STATE_ID_FR_1);
        assertEquals(dto1.getOnBehalfCompanyId(), TestDataJobItem.ONBEHALF_COMPANY_ID_FR_1);
        Assertions.assertNull(dto1.getContractReviewById());
        assertEquals(dto1.getTurn(), TestDataJobItem.TURN_FR_1);

        // For compatibility with RESTful web service tests (serialized JSON) we use the same date format here
        SimpleDateFormat dtf = DateTools.rest_dtf;

        assertNotNull(dto1.getDateComplete());
        assertEquals(dtf.format(dto1.getDateComplete()), TestDataJobItem.DATE_COMPLETE_TEXT_FR_1);
        assertNotNull(dto1.getDateIn());
        assertEquals(dto1.getDateIn().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME), TestDataJobItem.DATE_IN_TEXT_FR_2);
        assertNotNull(dto1.getDueDate());
        assertEquals(dto1.getDueDate().format(DateTimeFormatter.ISO_DATE), TestDataJobItem.DUE_DATE_TEXT_FR_1);

        assertEquals(dto1.getClientRef(), TestDataJobItem.CLIENT_REF_FR_1);
        // GB 2021-01-27 : These two fields are intentionally not tested, they'res not populated by the Dao call being tested here.
//		Assert.assertEquals(dto1.getDefaultPage(),TestDataJobItem.DEFAULT_PAGE_FR_1);
//		Assert.assertEquals(dto1.getProcReference(),TestDataJobItem.PROC_RFFERNCE_FR_1);	

    }
	
}
