package org.trescal.cwms.core.jobs.jobitem.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.jobitem.dto.JobItemActivityRequestDTO;
import org.trescal.cwms.core.jobs.jobitem.dto.JobItemActivityResponseDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.db.JobItemActivityService;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;
import org.trescal.cwms.core.workflow.entity.workactivity.WorkActivity;
import org.trescal.cwms.core.workflow.entity.workstatus.WorkStatus;

import java.util.Date;
import java.util.HashSet;
import java.util.Locale;

@ExtendWith(MockitoExtension.class)
public class JobItemActivityControllerTest {

    @Mock
    private JobItemActivityService jobItemActivityService;
    @Mock
    private UserService userService;

    @Test
    @DisplayName("The method should return new job item activity")
    public void createIncompleteActivity ()
    {
        Integer activityId = 111;
        Integer statusId = 222;
        Integer jobItemId = 333;
        String userName = "John.Smith";
        String firstName = "John";
        String lastName = "Smith";
        String startStatus = "Awaiting calibration";
        String startStatusFrench = "En attente d'etalonnage";
        String description = "Calibration";
        String descriptionFrench = "etalonnage";
        Locale locale = Locale.FRANCE;

        Date startStamp = new Date();

        Contact contact = new Contact();
        contact.setFirstName(firstName);
        contact.setLastName(lastName);

        User user = new User();
        user.setUsername(userName);
        user.setCon(contact);

        Translation startStatusFrenchTranslation = new Translation(locale, startStatusFrench);
        Translation startStatusEnglishTranslation = new Translation(Locale.UK, startStatus);
        HashSet<Translation> startStatusTranslations = new HashSet<Translation>();
        startStatusTranslations.add(startStatusEnglishTranslation);
        startStatusTranslations.add(startStatusFrenchTranslation);

        Translation activityFrenchTranslation = new Translation(locale, descriptionFrench);
        HashSet<Translation> activityTranslations = new HashSet<Translation>();
        activityTranslations.add(activityFrenchTranslation);

        ItemActivity itemActivity = new WorkActivity();
        itemActivity.setStateid(activityId);
        itemActivity.setTranslations(activityTranslations);

        ItemStatus itemStatus = new WorkStatus();
        itemStatus.setStateid(statusId);
        itemStatus.setDescription(startStatus);
        itemStatus.setTranslations(startStatusTranslations);

        JobItem jobItem = new JobItem();
        jobItem.setJobItemId(jobItemId);

        JobItemActivityRequestDTO jobItemActivityRequestDTO = JobItemActivityRequestDTO.builder()
                .activityId(activityId)
                .jobItemId(jobItemId)
                .build();

        JobItemActivity jobItemActivity = new JobItemActivity();
        jobItemActivity.setActivity(itemActivity);
        jobItemActivity.setJobItem(jobItem);
        jobItemActivity.setActivityDesc(description);
        jobItemActivity.setStartStatus(itemStatus);
        jobItemActivity.setStartedBy(contact);
        jobItemActivity.setStartStamp(startStamp);

        Mockito.when(jobItemActivityService.createIncompleteAction(Mockito.eq(activityId),Mockito.eq(jobItemId),Mockito.eq(contact)))
                .thenReturn(jobItemActivity);

        Mockito.when(userService.get(Mockito.eq(userName))).thenReturn(user);

        JobItemActivityController jobItemActivityController =  new JobItemActivityController(jobItemActivityService,userService);

        JobItemActivityResponseDTO incompleteActivity = jobItemActivityController.createIncompleteActivity(jobItemActivityRequestDTO, userName, locale);

        Assertions.assertNotNull(incompleteActivity);
        Assertions.assertNotNull(incompleteActivity.getActivityId());

        Assertions.assertEquals(activityId,incompleteActivity.getActivityId());
        Assertions.assertEquals(jobItemId,incompleteActivity.getJobItemId());
        Assertions.assertEquals(descriptionFrench, incompleteActivity.getDescription());
        Assertions.assertEquals(startStatusFrench, incompleteActivity.getStartStatus());
        Assertions.assertEquals(contact.getName(), incompleteActivity.getStartedBy());
        Assertions.assertNotNull(incompleteActivity.getStartStamp());


    }

}