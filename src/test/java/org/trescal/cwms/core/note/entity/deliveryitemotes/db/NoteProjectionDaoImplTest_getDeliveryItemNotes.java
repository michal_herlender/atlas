package org.trescal.cwms.core.note.entity.deliveryitemotes.db;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.system.projection.note.NoteProjectionDTO;
import org.trescal.cwms.core.system.projection.note.service.NoteProjectionDao;
import org.trescal.cwms.tests.MvcCwmsTestClass;
import org.trescal.cwms.tests.generic.DataSetDelivery;
import org.trescal.cwms.tests.generic.job.TestDataDeliveryItemNote;

public class NoteProjectionDaoImplTest_getDeliveryItemNotes extends MvcCwmsTestClass {
	
	@Autowired
    private  NoteProjectionDao noteprojectionDao;

	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetDelivery.getDeliveryNoteClientTestData();
	}
@Test
public void testdeliveryIds() {
	
	Integer deliveryId = TestDataDeliveryItemNote.DELIVERY_ID; 
	
	List<NoteProjectionDTO> notes = this.noteprojectionDao.getDeliveryItemNotes(deliveryId);
	NoteProjectionDTO dto1 = notes.get(0);
	
	Assert.assertEquals(dto1.getNoteid(),TestDataDeliveryItemNote.NOTE_ID);
	Assert.assertEquals(dto1.getEntityId(),TestDataDeliveryItemNote.ENTITY_ID);
	Assert.assertEquals(dto1.getActive(),TestDataDeliveryItemNote.ACTIVE);
	Assert.assertEquals(dto1.getLabel(),TestDataDeliveryItemNote.LABEL);
	Assert.assertEquals(dto1.getNote(),TestDataDeliveryItemNote.NOTE);
	Assert.assertEquals(dto1.getPublish(),TestDataDeliveryItemNote.PUBLISH);
		
	}
}
