package org.trescal.cwms.core.note.entity.deliveryitemotes.db;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.system.projection.note.NoteProjectionDTO;
import org.trescal.cwms.core.system.projection.note.service.NoteProjectionDao;
import org.trescal.cwms.tests.MvcCwmsTestClass;
import org.trescal.cwms.tests.generic.DataSetDelivery;
import org.trescal.cwms.tests.generic.job.TestDataDeliveryItemNote;
import org.trescal.cwms.tests.generic.job.TestDataDeliveryNote;

public class NoteProjectionDaoImplTest_getDeliveryNotes extends MvcCwmsTestClass {
	
	@Autowired
    private  NoteProjectionDao noteprojectionDao;

	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetDelivery.getDeliveryNoteClientTestData();
	}
@Test
public void testdeliveryIds() {
	
	Collection<Integer> deliveryIds = new ArrayList<>();
	
	deliveryIds.add(TestDataDeliveryItemNote.DELIVERY_ID);
	List<NoteProjectionDTO> dtos = this.noteprojectionDao.getDeliveryNotes(deliveryIds);
	NoteProjectionDTO dto1 = dtos.get(0);
	
	Assert.assertEquals(dto1.getNoteid(),TestDataDeliveryItemNote.ENTITY_ID);
	Assert.assertEquals(dto1.getEntityId(),TestDataDeliveryNote.DELIVERY_ID);
	Assert.assertEquals(dto1.getLabel(),TestDataDeliveryItemNote.LABEL);
	Assert.assertEquals(dto1.getNote(),TestDataDeliveryNote.NOTE);
	Assert.assertEquals(dto1.getPublish(),TestDataDeliveryItemNote.PUBLISH);
	Assert.assertEquals(dto1.getActive(),TestDataDeliveryItemNote.ACTIVE);

		
	}
}
