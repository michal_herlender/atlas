package org.trescal.cwms.core.invoice.entity;
import java.time.LocalDate;

import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.trescal.cwms.core.AtlasUnitTest;
import org.trescal.cwms.core.company.entity.paymentterms.PaymentTerm;

@Category(AtlasUnitTest.class)
public class PaymentTermTests {

	@Test
	public void testupdateInvoiceDueDateP0() throws Exception {
		LocalDate issueDate = LocalDate.of(2019, 1, 10);
		LocalDate expectedDueDate = LocalDate.of(2019, 1, 10);

		Assert.assertEquals(expectedDueDate, PaymentTerm.P0.computePaymentDate(issueDate));
	}

	@Test
	public void testupdateInvoiceDueDateP60() throws Exception {
		LocalDate issueDate = LocalDate.of(2019, 1, 10);
		LocalDate expectedDueDate = LocalDate.of(2019, 3, 11);

		Assert.assertEquals(expectedDueDate, PaymentTerm.P60.computePaymentDate(issueDate));
	}
	
	
	@Test
	public void testupdateInvoiceDueDateP30EOM15() throws Exception {
		LocalDate issueDate = LocalDate.of(2019, 1, 10);
		LocalDate expectedDueDate = LocalDate.of(2019, 3, 15);

		Assert.assertEquals(expectedDueDate, PaymentTerm.P30EOM15.computePaymentDate(issueDate));
	}
	

	@Test
	public void testupdateInvoiceDueDatePNM25() throws Exception {
		LocalDate issueDate = LocalDate.of(2019, 1, 10);
		LocalDate expectedDueDate = LocalDate.of(2019, 2, 25);

		Assert.assertEquals(expectedDueDate, PaymentTerm.PNM25.computePaymentDate(issueDate));
	}

	@Test
	public void testupdateInvoiceDueDateP180() throws Exception {
		LocalDate issueDate = LocalDate.of(2019, 1, 10);
		LocalDate expectedDueDate = LocalDate.of(2019, 7, 9);

		Assert.assertEquals(expectedDueDate, PaymentTerm.P180.computePaymentDate(issueDate));
	}
	
	
	@Test
	public void testupdateInvoiceDueDatePEOM30() throws Exception {
		LocalDate issueDate = LocalDate.of(2019, 1, 10);
		LocalDate expectedDueDate = LocalDate.of(2019, 3, 2);

		Assert.assertEquals(expectedDueDate, PaymentTerm.PEOM30.computePaymentDate(issueDate));
	}
	
	
	@Test
	public void testupdateInvoiceDueDateP30EOM10() throws Exception {
		LocalDate issueDate = LocalDate.of(2019, 1, 10);
		LocalDate expectedDueDate = LocalDate.of(2019, 3, 10);

		Assert.assertEquals(expectedDueDate, PaymentTerm.P30EOM10.computePaymentDate(issueDate));
	}
	
	
	@Test
	public void testupdateInvoiceDueDatePEOM45() throws Exception {
		LocalDate issueDate = LocalDate.of(2019, 1, 10);
		LocalDate expectedDueDate = LocalDate.of(2019, 3, 17);

		Assert.assertEquals(expectedDueDate, PaymentTerm.PEOM45.computePaymentDate(issueDate));
	}
	
	@Test
	public void testupdateInvoiceDueDateP45EOM() throws Exception {
		LocalDate issueDate = LocalDate.of(2019, 1, 10);
		LocalDate expectedDueDate = LocalDate.of(2019, 2, 28);

		Assert.assertEquals(expectedDueDate, PaymentTerm.P45EOM.computePaymentDate(issueDate));
	}

	@Test
	public void testupdateInvoiceDueDateP30EOM() throws Exception {
		LocalDate issueDate = LocalDate.of(2019, 1, 10);
		LocalDate expectedDueDate = LocalDate.of(2019, 2, 28);

		Assert.assertEquals(expectedDueDate, PaymentTerm.P30EOM.computePaymentDate(issueDate));
	}
	

	@Test
	public void testupdateInvoiceDueDateP45() throws Exception {
		LocalDate issueDate = LocalDate.of(2019, 1, 10);
		LocalDate expectedDueDate = LocalDate.of(2019, 2, 24);

		Assert.assertEquals(expectedDueDate, PaymentTerm.P45.computePaymentDate(issueDate));
	}
	

	@Test
	public void testupdateInvoiceDueDateP30() throws Exception {
		LocalDate issueDate = LocalDate.of(2019, 1, 10);
		LocalDate expectedDueDate = LocalDate.of(2019, 2, 9);

		Assert.assertEquals(expectedDueDate, PaymentTerm.P30.computePaymentDate(issueDate));
	}
	
	
	@Test
	public void testupdateInvoiceDueDateP120() throws Exception {
		LocalDate issueDate = LocalDate.of(2019, 1, 10);
		LocalDate expectedDueDate = LocalDate.of(2019, 5, 10);

		Assert.assertEquals(expectedDueDate, PaymentTerm.P120.computePaymentDate(issueDate));
	}
	
	
	@Test
	public void testupdateInvoiceDueDateP150() throws Exception {
		LocalDate issueDate = LocalDate.of(2019, 1, 10);
		LocalDate expectedDueDate = LocalDate.of(2019, 6, 9);

		Assert.assertEquals(expectedDueDate, PaymentTerm.P150.computePaymentDate(issueDate));
	}
	
	@Test
	public void testupdateInvoiceDueDateP90() throws Exception {
		LocalDate issueDate = LocalDate.of(2019, 1, 10);
		LocalDate expectedDueDate = LocalDate.of(2019, 4, 10);

		Assert.assertEquals(expectedDueDate, PaymentTerm.P90.computePaymentDate(issueDate));
	}
	

	@Test
	public void testupdateInvoiceDueDateP85() throws Exception {
		LocalDate issueDate = LocalDate.of(2019, 1, 10);
		LocalDate expectedDueDate = LocalDate.of(2019, 4, 5);

		Assert.assertEquals(expectedDueDate, PaymentTerm.P85.computePaymentDate(issueDate));
	}
	
	@Test
	public void testupdateInvoiceDueDateP50() throws Exception {
		LocalDate issueDate = LocalDate.of(2019, 11, 29);
		LocalDate expectedDueDate = LocalDate.of(2020, 1, 18);

		Assert.assertEquals(expectedDueDate, PaymentTerm.P50.computePaymentDate(issueDate));
	}
}