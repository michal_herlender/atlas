package org.trescal.cwms.core.contract.entity.contract.db;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.contract.dtos.MatchingContractDto;
import org.trescal.cwms.core.contract.entity.contract.Contract;
import org.trescal.cwms.tests.MvcRestTestClass;

public class ContractServiceTests extends MvcRestTestClass {

    @Autowired
    ContractService contractService;

    @Autowired
    SubdivService subdivService;

    @Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList("cwms/contract/ContractServiceTests.xml");
    }

    /************   Add/Remove Subdiv tests  ****************************/

    @Test
    @Transactional
    public void successfullyAddSubdivOfSameCompanyThatContractIsAttachedToo(){
        contractService.addSubdivs(1, Collections.singletonList(1));
        Contract contract = contractService.get(1);
        assertThat(contract.getSubdivs().size(),is(1));
    }

    @Test
    public void failToAddSubdivOfDifferentCompanyToTheContractCompany(){
    	Assertions.assertThrows(IllegalArgumentException.class, () -> {
        	// Contract 2 doesn't belong to subdiv 1
            contractService.addSubdivs(2,Collections.singletonList(1));
    	});
    }

    @Test
    @Transactional
    public void sucessfullyRemoveAnAttachedSubdiv(){
        contractService.addSubdivs(1, Collections.singletonList(1));
        Contract contract = contractService.get(1);
        assertThat(contract.getSubdivs().size(),is(1));
        contractService.removeSubdiv(1, 1);
        assertThat(contract.getSubdivs().size(),is(0));
    }

    @Test
    @Transactional
    public void silentlyDoNothingWhenAttemptingToRemoveASubdivThatIsNotAttached(){
        contractService.addSubdivs(1, Collections.singletonList(1));
        Contract contract = contractService.get(1);
        assertThat(contract.getSubdivs().size(),is(1));
        contractService.removeSubdiv(1, 2);
        assertThat(contract.getSubdivs().size(),is(1));
    }

    @Test
    @Transactional
    public void silentlyDoNothingWhenAttemptingToAddASubdivThatIsAlreadyAttached(){
        Contract contract = contractService.get(1);
        contractService.addSubdivs(1, Collections.singletonList(1));
        assertThat(contract.getSubdivs().size(),is(1));
        contractService.addSubdivs(1,Collections.singletonList(1));
        assertThat(contract.getSubdivs().size(),is(1));
    }


    /****************  Contract matching tests **************************/

    @Test
    public void findContractsAtCustomer(){
        MatchingContractDto contract1 = new MatchingContractDto(1,"ESTEM-CO-19000001",false);
        MatchingContractDto contract3 = new MatchingContractDto(3,"ESTEM-CO-19000003",true);
        List<MatchingContractDto> contracts = contractService.findMatchingContractsForJobItem(1, 1);
        assertThat(contracts.size(),is(2));
        assertThat(contracts, hasItems(contract1, contract3));
    }

    @Test
    public void findContractAtSubdiv() {
        MatchingContractDto contract2 = new MatchingContractDto(2, "ESTEM-CO-19000002", true);
        contractService.addSubdivs(2, Collections.singletonList(4));
        List<MatchingContractDto> contracts = contractService.findMatchingContractsForJobItem(2, 1);
        assertThat(contracts.size(), is(1));
        assertThat(contracts, hasItems(contract2));
    }

    @Test
    public void failToFindContractAtDifferentSubdiv() {
        contractService.addSubdivs(2, Collections.singletonList(5));
        List<MatchingContractDto> contracts = contractService.findMatchingContractsForJobItem(2, 1);
        assertThat(contracts.size(), is(0));
    }

    @Test
    public void findMultipleContractsAtSubdivAndCustomerWhereSubdivContractIsBestMatch() {
        MatchingContractDto contract1 = new MatchingContractDto(1,"ESTEM-CO-19000001",true);
        MatchingContractDto contract3 = new MatchingContractDto(3,"ESTEM-CO-19000003",false);
        contractService.addSubdivs(1,Collections.singletonList(1));
        List<MatchingContractDto> contracts = contractService.findMatchingContractsForJobItem(1, 1);
        assertThat(contracts.size(),is(2));
        assertThat(contracts,hasItems(contract1,contract3));
    }


    /*****************  Add/Remove Instrument Tests *********************/

    @Test
    @Transactional
    public void successfullyAddAnInstrumentThatIsNotAlreadyAttached(){
        contractService.addInstrument(1, 1);
        Contract contract = contractService.get(1);
        assertThat(contract.getInstruments().size(),is(1));
    }

    @Test
    @Transactional
    public void sucessfullyRemoveAnAttachedInstrument(){
        contractService.addInstrument(1, 1);
        Contract contract = contractService.get(1);
        assertThat(contract.getInstruments().size(),is(1));
        contractService.removeInstrument(1, 1);
        assertThat(contract.getInstruments().size(),is(0));
    }

    @Test
    @Transactional
    public void silentlyDoNothingWhenAttemptingToRemoveAnInstrumentThatIsNotAttached(){
        contractService.addInstrument(1, 1);
        Contract contract = contractService.get(1);
        assertThat(contract.getInstruments().size(),is(1));
        contractService.removeInstrument(1, 2);
        assertThat(contract.getInstruments().size(),is(1));
    }

    @Test
    @Transactional
    public void silentlyDoNothingWhenAttemptingToAddAnInstrumentThatIsAlreadyAttached(){
        Contract contract = contractService.get(1);
        contractService.addInstrument(1, 1);
        assertThat(contract.getInstruments().size(),is(1));
        contractService.addInstrument(1,1);
        assertThat(contract.getInstruments().size(),is(1));
    }



}

