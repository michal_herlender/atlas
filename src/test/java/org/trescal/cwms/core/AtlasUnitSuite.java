package org.trescal.cwms.core;

import org.junit.platform.suite.api.IncludeTags;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.platform.suite.api.Suite;

/**
 * This suite allows us to run just unit tests
 * where the test class contains the "org.trescal.cwms.core.AtlasUnitTest" tag
 */

@Suite
@SelectPackages("org.trescal.cwms")
@IncludeTags("org.trescal.cwms.core.AtlasUnitTest")
public class AtlasUnitSuite {

}