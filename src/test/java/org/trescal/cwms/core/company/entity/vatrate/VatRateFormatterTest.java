package org.trescal.cwms.core.company.entity.vatrate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.company.entity.vatrate.db.VatRateService;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocales;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetCore;
import org.trescal.cwms.tests.generic.core.TestDataVatRate;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.*;

public class VatRateFormatterTest extends MvcRestTestClass {

	@Autowired
	private VatRateFormatter formatter;
	
	@Autowired
	private VatRateService vatRateService;
	
	private void testDescriptionAndRate(String vatCode, Locale locale, String expected) {
		VatRate vatRate = this.vatRateService.get(vatCode);
		Assertions.assertNotNull(vatRate,"VatRate was null for code "+vatCode);
		String actual = this.formatter.formatDescriptionAndRate(vatRate, locale, vatRate.getRate());
		Assertions.assertEquals(expected, actual);
	}

	private void testVatRate(String vatCode, Locale locale, String expected) {
		VatRate vatRate = this.vatRateService.get(vatCode);
		Assertions.assertNotNull( vatRate,"VatRate was null for code "+vatCode);
		String actual = this.formatter.formatVatRate(vatRate, locale);
		Assertions.assertEquals(expected, actual);
	}

	private void testVatRateWithRate(String vatCode, Locale locale, BigDecimal specialRate, String expected) {
		VatRate vatRate = this.vatRateService.get(vatCode);
		Assertions.assertNotNull( vatRate,"VatRate was null for code "+vatCode);
		String actual = this.formatter.formatVatRate(vatRate, locale, specialRate);
		Assertions.assertEquals(expected, actual);
	}
	
	@Test
	@Transactional
	public void testFormatDescriptionAndRate() {
		testDescriptionAndRate(TestDataVatRate.ID_DEFAULT_SPAIN, SupportedLocales.ENGLISH_GB, "Spain - 21.00%");
		testDescriptionAndRate(TestDataVatRate.ID_DEFAULT_MOROCCO, SupportedLocales.FRENCH_FR, "Maroc - 20.00%");
		testDescriptionAndRate(TestDataVatRate.ID_SPECIAL_CANARIAS, SupportedLocales.SPANISH_ES, "Espa\u00F1a - Canarias - 0.00%");
		testDescriptionAndRate(TestDataVatRate.ID_SPECIAL_MOROCCO_FREE, SupportedLocales.FRENCH_FR, "Maroc - Morocco Free Zone - 0.00%");
		testDescriptionAndRate(TestDataVatRate.ID_EUROZONE_EXPORT, SupportedLocales.ENGLISH_GB, "Eurozone Export - 0.00%");
		testDescriptionAndRate(TestDataVatRate.ID_EUROZONE_INTERNAL, SupportedLocales.ENGLISH_GB, "Eurozone Internal - 0.00%");
	}

	@Test
	@Transactional
	public void testFormatVatRate() {
		testVatRate(TestDataVatRate.ID_DEFAULT_SPAIN, SupportedLocales.ENGLISH_GB, "Country Default - Spain - 21.00%");
		testVatRate(TestDataVatRate.ID_DEFAULT_MOROCCO, SupportedLocales.FRENCH_FR, "Pays par d\u00E9faut - Maroc - 20.00%");
		testVatRate(TestDataVatRate.ID_SPECIAL_CANARIAS, SupportedLocales.SPANISH_ES, "Pa\u00EDs especial - Espa\u00F1a - Canarias - 0.00%");
		testVatRate(TestDataVatRate.ID_SPECIAL_MOROCCO_FREE, SupportedLocales.FRENCH_FR, "Pays sp\u00E9cial - Maroc - Morocco Free Zone - 0.00%");
		testVatRate(TestDataVatRate.ID_EUROZONE_EXPORT, SupportedLocales.ENGLISH_GB, "Export From Eurozone - Eurozone Export - 0.00%");
		testVatRate(TestDataVatRate.ID_EUROZONE_INTERNAL, SupportedLocales.ENGLISH_GB, "Between Eurozone Countries - Eurozone Internal - 0.00%");
		// Test with override of rate (e.g. old entity with different rate)
		testVatRateWithRate(TestDataVatRate.ID_DEFAULT_SPAIN, SupportedLocales.ENGLISH_GB, new BigDecimal("15.00"), "Country Default - Spain - 15.00%");
	}
	
	@Test
	@Transactional
	public void testOptionGroupMap() {
		// We test just a small defined set of rates that the map is built as expected.  
		// No need to test all keys, as these are tested in testFormatDescription.
		List<VatRate> vatRates = new ArrayList<>();
		vatRates.add(this.vatRateService.get(TestDataVatRate.ID_DEFAULT_SPAIN));
		vatRates.add(this.vatRateService.get(TestDataVatRate.ID_DEFAULT_MOROCCO));
		vatRates.add(this.vatRateService.get(TestDataVatRate.ID_SPECIAL_CANARIAS));
		vatRates.add(this.vatRateService.get(TestDataVatRate.ID_SPECIAL_MOROCCO_FREE));
		Map<String, Set<KeyValue<String, String>>> map = this.formatter.getOptionGroupMap(vatRates, SupportedLocales.ENGLISH_GB);
		
		// Expect to have two groups, and two values per group
		Assertions.assertNotNull(map);
		Assertions.assertEquals(2, map.size());
		
		Object[] expectedGroups = new Object[] {"Country Default", "Country Special"};
		Assertions.assertArrayEquals(expectedGroups, map.keySet().toArray());
		
		Object[] expectedOptions1 = new Object[] {
			new KeyValue<String, String>(TestDataVatRate.ID_DEFAULT_MOROCCO, "Morocco - 20.00%"),
			new KeyValue<String, String>(TestDataVatRate.ID_DEFAULT_SPAIN, "Spain - 21.00%")
		};
		Object[] expectedOptions2 = new Object[] {
			new KeyValue<String, String>(TestDataVatRate.ID_SPECIAL_MOROCCO_FREE, "Morocco - Morocco Free Zone - 0.00%"),
			new KeyValue<String, String>(TestDataVatRate.ID_SPECIAL_CANARIAS, "Spain - Canarias - 0.00%")
		};

		Assertions.assertArrayEquals(expectedOptions1, map.get("Country Default").toArray());
		Assertions.assertArrayEquals(expectedOptions2, map.get("Country Special").toArray());
		
	}
	
	@Override
	protected List<String> getDataSetFileNames() {
		return Collections.singletonList(DataSetCore.FILE_BASE_SYSTEM);
	}
}
