package org.trescal.cwms.core.company.entity.vatrate.db;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.company.entity.vatrate.VatRate;
import org.trescal.cwms.core.company.entity.vatrate.VatRateType;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetCore;
import org.trescal.cwms.tests.generic.core.TestDataSupportedCountry;
import org.trescal.cwms.tests.generic.core.TestDataVatRate;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

public class VatRateDaoImplTest extends MvcRestTestClass {

	@Autowired
	private VatRateDao vatRateDao;
	
	@Override
	protected List<String> getDataSetFileNames() {
		return Collections.singletonList(DataSetCore.FILE_BASE_SYSTEM);
	}
	
	@Test
	@Transactional
	public void testGetAllEagerWithCountry() {
		// TODO could assert that only one SQL query is run when accessing country results
		List<VatRate> results = this.vatRateDao.getAllEagerWithCountry();
		Assertions.assertEquals(TestDataVatRate.SIZE_DATA_SET, results.size());
	}
	
	private void testGetForType(VatRateType type) {
		VatRate vatRate = this.vatRateDao.getDefaultForType(type);
		Assertions.assertNotNull(vatRate);
		Assertions.assertEquals(type, vatRate.getType());
		Assertions.assertNull(vatRate.getCountry());
	}

	private void testGetForCountryAndType(int countryId, VatRateType type) {
		VatRate vatRate = this.vatRateDao.getForCountryAndType(countryId, type);
		Assertions.assertNotNull(vatRate);
		Assertions.assertEquals(type, vatRate.getType());
		Assertions.assertNotNull(vatRate.getCountry());
		Assertions.assertEquals(countryId, vatRate.getCountry().getCountryid());
	}

	@Test
	@Transactional
	public void testGetForType() {
		testGetForType(VatRateType.EUROZONE_EXTERNAL);
		testGetForType(VatRateType.EUROZONE_INTERNAL);
		testGetForType(VatRateType.NON_EUROZONE);
	}
	
	@Test
	@Transactional
	public void testGetForCountryAndType() {
		testGetForCountryAndType(TestDataSupportedCountry.ID_FRANCE, VatRateType.COUNTRY_DEFAULT);
		testGetForCountryAndType(TestDataSupportedCountry.ID_USA, VatRateType.COUNTRY_DEFAULT);
	}

}
