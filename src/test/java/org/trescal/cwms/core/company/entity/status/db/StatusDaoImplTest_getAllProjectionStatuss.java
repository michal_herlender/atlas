package org.trescal.cwms.core.company.entity.status.db;

import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.company.projection.StatusProjectionDTO;
import org.trescal.cwms.core.quotation.entity.quotestatus.QuoteStatus;
import org.trescal.cwms.core.system.entity.status.db.StatusDao;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocales;
import org.trescal.cwms.tests.MvcCwmsTestClass;
import org.trescal.cwms.tests.generic.DataSetBaseStatus;
import org.trescal.cwms.tests.generic.address.TestDataQuotationStatus;

public class StatusDaoImplTest_getAllProjectionStatuss extends MvcCwmsTestClass {
	
	@Autowired
	private StatusDao statusDao;

	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetBaseStatus.getQuoationStatusTestData();
	}
	
	@Test
	public void testMultiplesStatusIds() {
		
		Locale locale = SupportedLocales.FRENCH_FR;
		 
		 List<StatusProjectionDTO> result = this.statusDao.getAllProjectionStatuss(QuoteStatus.class, locale);
		 StatusProjectionDTO dto0 = result.get(0);
		 Assert.assertEquals(dto0.getId(),TestDataQuotationStatus.QUOTATION_STATUS_REQUESTED_ID);
		 Assert.assertEquals(dto0.getName(),TestDataQuotationStatus.QUOTATION_STATUS_REQUESTED_NAME_FR);
	
	}
}							
