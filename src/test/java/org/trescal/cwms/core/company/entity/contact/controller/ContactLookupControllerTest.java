package org.trescal.cwms.core.company.entity.contact.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.trescal.cwms.tests.MvcCwmsTestClass;

public class ContactLookupControllerTest extends MvcCwmsTestClass {

	@Override
	protected List<String> getDataSetFileNames() {
		return Collections.singletonList("web/ContactLookupControllerTest.xml");
	}
	
	@Test
    public void testGetJSON() throws Exception {
    	this.mockMvc.perform(get("/contactapi/subdivcontactlist/1")
    	.accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
    	.andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType("application/json;charset=UTF-8"))
    	.andExpect(jsonPath("$").exists())
    	.andExpect(jsonPath("$").isArray())
    	.andExpect(jsonPath("$[0]").exists())
    	.andExpect(jsonPath("$[2]").exists())
    	.andExpect(jsonPath("$[5]").doesNotExist())
    	.andExpect(jsonPath("$[0].key").value(1))
    	.andExpect(jsonPath("$[0].value").value("A Contact"));
    }
}
