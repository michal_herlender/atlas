package org.trescal.cwms.core.company.entity.address.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcCwmsTestClass;
public class AddressLookupControllerTest  extends MvcCwmsTestClass {

	public static final String USERNAME = "rdean";
	
	@Override
	protected List<String> getDataSetFileNames() {
		return Collections.singletonList("web/AddressLookupControllerTest.xml");
	}
	
	@Test
    public void testGetJSON() throws Exception {
    	this.mockMvc.perform(get("/addressapi/subdivdeliveryaddresslist/4")
    	.accept(MediaType.parseMediaType("application/json;charset=UTF-8"))
		.sessionAttr(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME))
    	.andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType("application/json;charset=UTF-8"))
    	.andExpect(jsonPath("$").exists())
    	.andExpect(jsonPath("$").isArray())
    	.andExpect(jsonPath("$[0]").exists())
    	.andExpect(jsonPath("$[1]").exists())
    	.andExpect(jsonPath("$[2]").doesNotExist())
    	.andExpect(jsonPath("$[0].key").value(5))
    	.andExpect(jsonPath("$[0].value").value("e"));
    }


}
