package org.trescal.cwms.core.company.entity.subdiv.db;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.company.projection.SubdivProjectionDTO;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetCompany;
import org.trescal.cwms.tests.generic.company.TestDataCompany;
import org.trescal.cwms.tests.generic.company.TestDataSubdiv;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SubdivDaoImplTest_getSubdivProjectionDTOs extends MvcRestTestClass {

	@Autowired
	private SubdivDao subdivDao ;
	
	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetCompany.getCompanyBusinessTestData();
	}
	
	@Test
	public void testMultiplesSubdivIds() {
		Collection<Integer> sourceSubdivIds = new ArrayList<>();
		sourceSubdivIds.add(TestDataSubdiv.ID_BUSINESS_FRANCE);
		sourceSubdivIds.add(TestDataSubdiv.ID_BUSINESS_MOROCCO);
		Integer allocatedCompanyId = TestDataCompany.ID_BUSINESS_MOROCCO;
	
		List<SubdivProjectionDTO> dtos = this.subdivDao.getSubdivProjectionDTOs(sourceSubdivIds, allocatedCompanyId);
		Assertions.assertEquals(sourceSubdivIds.size(), dtos.size());
		SubdivProjectionDTO dto1 = dtos.get(0);
		SubdivProjectionDTO dto2 = dtos.get(1);

		Assertions.assertEquals(TestDataSubdiv.ID_BUSINESS_FRANCE,dto1.getSubdivid());
		Assertions.assertEquals(TestDataSubdiv.ID_BUSINESS_MOROCCO,dto2.getSubdivid());
		Assertions.assertEquals(TestDataSubdiv.SUBDIV_CODE_BUSINESS_MOROCCO_CASA, dto2.getSubname());
		Assertions.assertEquals(TestDataSubdiv.ACTIVE_BUSINESS_MOROCCO_CASA, dto2.getActive());
	}
}
