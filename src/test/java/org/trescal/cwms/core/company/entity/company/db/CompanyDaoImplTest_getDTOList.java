package org.trescal.cwms.core.company.entity.company.db;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.company.projection.CompanyProjectionDTO;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetCompany;
import org.trescal.cwms.tests.generic.company.TestDataCompany;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CompanyDaoImplTest_getDTOList extends MvcRestTestClass {

	@Autowired
	private CompanyDao companyDao;
	
	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetCompany.getCompanyBusinessTestData();
	}
	
	@Test
	public void testMultiplecompanyIds() {
	Collection<Integer>	companyIds = new ArrayList<Integer>();
	companyIds.add(TestDataCompany.ID_BUSINESS_USA);
	companyIds.add(TestDataCompany.ID_BUSINESS_MOROCCO);
	
		Integer allocatedCompanyId = TestDataCompany.ID_BUSINESS_MOROCCO;
		List<CompanyProjectionDTO> dtos = this.companyDao.getDTOList(companyIds, allocatedCompanyId);
		Assertions.assertEquals(companyIds.size(), dtos.size());
		CompanyProjectionDTO dto1 = dtos.get(0);
		CompanyProjectionDTO dto2 = dtos.get(1);

		Assertions.assertEquals(TestDataCompany.ID_BUSINESS_MOROCCO, dto1.getCoid());
		Assertions.assertEquals(TestDataCompany.COMPANY_NAME_BUSINESS_USA, dto2.getConame());
		Assertions.assertEquals(TestDataCompany.ACTIVE_BUSINESS_USA, dto2.getActive());
		Assertions.assertEquals(TestDataCompany.ONSTOP_BUSINESS_USA, dto2.getOnstop());
	}

}
