package org.trescal.cwms.core.company.entity.address.db;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.company.projection.AddressProjectionDTO;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetCompany;
import org.trescal.cwms.tests.generic.address.TestDataAddress;
import org.trescal.cwms.tests.generic.company.TestDataCompany;
import org.trescal.cwms.tests.generic.company.TestDataSubdiv;
import org.trescal.cwms.tests.generic.core.TestDataSupportedCountry;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class AddressDaoImplTest_getAddressProjectionDTOs extends MvcRestTestClass {

	@Autowired
	private AddressDao addressDao;
	
	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetCompany.getCompanyBusinessTestData();
	}

	@Test
	public void testMultipleIds() {
		Collection<Integer> addressIds = new ArrayList<>();
		addressIds.add(TestDataAddress.ID_ADDRESS_RUNGIS);
		addressIds.add(TestDataAddress.ID_ADDRESS_HARTLAND);
		
		Integer allocatedCompanyId = TestDataCompany.ID_BUSINESS_USA;
		
		List<AddressProjectionDTO> dtos = this.addressDao.getAddressProjectionDTOs(addressIds, allocatedCompanyId);
		Assertions.assertEquals(addressIds.size(), dtos.size());
		AddressProjectionDTO dto1 = dtos.get(0);
		AddressProjectionDTO dto2 = dtos.get(1);

		Assertions.assertEquals(TestDataAddress.ID_ADDRESS_RUNGIS, dto1.getAddressId());
		// Full tests on 2nd DTO because already defined inside TestDataAddress
		Assertions.assertEquals(TestDataAddress.ID_ADDRESS_HARTLAND, dto2.getAddressId());
		Assertions.assertEquals(TestDataSubdiv.ID_BUSINESS_USA, dto2.getSubdivId());
		Assertions.assertEquals(TestDataAddress.ACTIVE_HARTLAND, dto2.getActive());
		Assertions.assertEquals(TestDataAddress.ADDR_1_HARTLAND, dto2.getAddr1());
		Assertions.assertEquals(TestDataAddress.ADDR_2_HARTLAND, dto2.getAddr2());
		Assertions.assertEquals(TestDataAddress.ADDR_3_HARTLAND, dto2.getAddr3());
		Assertions.assertEquals(TestDataSupportedCountry.ID_USA, dto2.getCountryId());
		Assertions.assertEquals(TestDataSupportedCountry.COUNTRY_CODE_USA, dto2.getCountryCode());
		Assertions.assertEquals(TestDataAddress.COUNTY_HARTLAND, dto2.getCounty());
		Assertions.assertEquals(TestDataAddress.POSTCODE_HARTLAND, dto2.getPostcode());
		Assertions.assertEquals(TestDataAddress.TOWN_HARTLAND, dto2.getTown());
		Assertions.assertEquals(TestDataAddress.TELEPHONE_HARTLAND, dto2.getTelephone());
		Assertions.assertEquals(TestDataAddress.FAX_HARTLAND, dto2.getFax());
	}	
}
