package org.trescal.cwms.core.company.entity.contact.db;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetCompany;
import org.trescal.cwms.tests.generic.company.TestDataContact;
import org.trescal.cwms.tests.generic.company.TestDataSubdiv;

import javax.transaction.Transactional;
import java.util.List;


public class ContactDaoImplTest extends MvcRestTestClass {

	@Autowired
	private ContactDaoImpl contactDaoImpl;
	
	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetCompany.getCompanyClientTestData();
	}
	
	@Test
	@Transactional
	public void testGetContactByFirstnameAndLastname() {
		// Expect no results - different subdiv
		{
			Contact contact = this.contactDaoImpl.getContactByFirstnameAndLastname(TestDataSubdiv.ID_BUSINESS_FRANCE, 
					TestDataContact.FIRST_NAME_CLIENT_FRANCE, TestDataContact.LAST_NAME_CLIENT_FRANCE);
			Assertions.assertNull(contact);
			/*Assert.assertNull(contact);*/
		}
		// Expect no results - different first name
		{
			Contact contact = this.contactDaoImpl.getContactByFirstnameAndLastname(TestDataSubdiv.ID_CLIENT_FRANCE, 
					TestDataContact.FIRST_NAME_BUSINESS_FRANCE, TestDataContact.LAST_NAME_CLIENT_FRANCE);
			Assertions.assertNull(contact);
		}
		// Expect no results - different last name
		{
			Contact contact = this.contactDaoImpl.getContactByFirstnameAndLastname(TestDataSubdiv.ID_CLIENT_FRANCE, 
					TestDataContact.FIRST_NAME_CLIENT_FRANCE, TestDataContact.LAST_NAME_CLIENT_GERMANY);
			Assertions.assertNull(contact);
		}
		
		// Expect one result(first result found) - duplicate contact
		{
			Contact contact = this.contactDaoImpl.getContactByFirstnameAndLastname(TestDataSubdiv.ID_CLIENT_FRANCE, 
					TestDataContact.FIRST_NAME_CLIENT_FRANCE_DUPLICATE, TestDataContact.LAST_NAME_CLIENT_FRANCE_DUPLICATE);
			Assertions.assertNotNull(contact);
		}
		
		// Expect 1 result
		{
			Contact contact = this.contactDaoImpl.getContactByFirstnameAndLastname(TestDataSubdiv.ID_CLIENT_FRANCE, 
					TestDataContact.FIRST_NAME_CLIENT_FRANCE, TestDataContact.LAST_NAME_CLIENT_FRANCE);
			Assertions.assertNotNull(contact);
			Assertions.assertEquals((long) TestDataSubdiv.ID_CLIENT_FRANCE, (long) contact.getSub().getId());
			Assertions.assertEquals(TestDataContact.FIRST_NAME_CLIENT_FRANCE, contact.getFirstName());
			Assertions.assertEquals(TestDataContact.LAST_NAME_CLIENT_FRANCE, contact.getLastName());
		}		
	}
}
