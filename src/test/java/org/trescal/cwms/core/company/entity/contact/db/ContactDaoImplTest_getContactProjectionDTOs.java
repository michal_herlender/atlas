package org.trescal.cwms.core.company.entity.contact.db;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.company.projection.ContactProjectionDTO;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetCompany;
import org.trescal.cwms.tests.generic.company.TestDataCompany;
import org.trescal.cwms.tests.generic.company.TestDataContact;
import org.trescal.cwms.tests.generic.company.TestDataSubdiv;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ContactDaoImplTest_getContactProjectionDTOs extends MvcRestTestClass {

	@Autowired
	private ContactDao contactDao;
	
	
	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetCompany.getCompanyBusinessTestData();
	}

	
	@Test
	public void testMultiplesContactIds() {
		
		Collection<Integer> contactIds = new ArrayList<>();
		contactIds.add(TestDataContact.ID_BUSINESS_FRANCE);
		contactIds.add(TestDataContact.ID_BUSINESS_MOROCCO);
		
		Integer allocatedCompanyId = TestDataCompany.ID_BUSINESS_MOROCCO;
		
		List<ContactProjectionDTO> dtos = this.contactDao.getContactProjectionDTOs(contactIds, allocatedCompanyId);
		Assertions.assertEquals(contactIds.size(), dtos.size());
		ContactProjectionDTO dtoA = dtos.get(0);
		ContactProjectionDTO dtoB = dtos.get(1);

		Assertions.assertEquals(TestDataContact.ID_BUSINESS_FRANCE, dtoA.getPersonid());
		Assertions.assertEquals(TestDataContact.ID_BUSINESS_MOROCCO, dtoB.getPersonid());
		Assertions.assertEquals(TestDataContact.FIRST_NAME_BUSINESS_MOROCCO, dtoB.getFirstName());
		Assertions.assertEquals(TestDataContact.LAST_NAME_BUSINESS_MOROCCO, dtoB.getLastName());
		Assertions.assertEquals(TestDataContact.EMAIL_BUSINESS_MOROCCO, dtoB.getEmail());
		Assertions.assertEquals(TestDataContact.TELEPHONE_BUSINESS_MOROCCO, dtoB.getTelephone());
		Assertions.assertEquals(TestDataSubdiv.ACTIVE_BUSINESS_MOROCCO_CASA, dtoB.getActive());
		
	}
}
