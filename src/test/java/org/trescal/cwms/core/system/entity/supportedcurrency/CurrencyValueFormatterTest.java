package org.trescal.cwms.core.system.entity.supportedcurrency;

import java.math.BigDecimal;
import java.util.Locale;

import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.trescal.cwms.core.AtlasUnitTest;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocales;

/**
 * Standalone test class, no spring framework support needed
 */
@Category(AtlasUnitTest.class)
public class CurrencyValueFormatterTest {

	@Test
	public void testFormats() {
		BigDecimal oneThousand = new BigDecimal("1000.00");
		
		// nonBreakingSpace
		char nbs = '\u00a0';
		
		// Actual symbols would be provided by SupportedCurrency so these are all "theoretical" formats as test cases
		
		performTest(oneThousand, "$", CurrencySymbolPosition.POSTFIX_SPACE, SupportedLocales.ENGLISH_GB, "1,000.00"+nbs+"$");
		performTest(oneThousand, "$", CurrencySymbolPosition.PREFIX_NO_SPACE, SupportedLocales.ENGLISH_GB, "$1,000.00");
		performTest(oneThousand, "$", CurrencySymbolPosition.PREFIX_SPACE, SupportedLocales.ENGLISH_GB, "$"+nbs+"1,000.00");

		// We expect that the "non breaking space" should be used in number formatting
		
		performTest(oneThousand, "$", CurrencySymbolPosition.POSTFIX_SPACE, SupportedLocales.FRENCH_FR, "1"+nbs+"000,00"+nbs+"$");
		performTest(oneThousand, "$", CurrencySymbolPosition.PREFIX_NO_SPACE, SupportedLocales.FRENCH_FR, "$1"+nbs+"000,00");
		performTest(oneThousand, "$", CurrencySymbolPosition.PREFIX_SPACE, SupportedLocales.FRENCH_FR, "$"+nbs+"1"+nbs+"000,00");
	}
	
	private void performTest(BigDecimal value, String currencySymbol, CurrencySymbolPosition symbolPosition, Locale locale, String expectedValue) {
		String formattedValue = CurrencyValueFormatter.format(value, currencySymbol, symbolPosition, locale);
		Assert.assertEquals(expectedValue, formattedValue);
	}
	
	@Test
	public void shouldReturnEmptyForNullValue() {
		BigDecimal nullValue = null;
		performTest(nullValue, "$", CurrencySymbolPosition.PREFIX_NO_SPACE, SupportedLocales.ENGLISH_GB, "");
	}
}
