package org.trescal.cwms.core.system.entity.instruction.db;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.system.dto.InstructionDTO;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetCompany;
import org.trescal.cwms.tests.generic.DataSetJob;
import org.trescal.cwms.tests.generic.company.TestDataSubdiv;
import org.trescal.cwms.tests.generic.job.TestDataJob;
import org.trescal.cwms.tests.generic.job.TestDataJobItem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class InstructionServiceTest extends MvcRestTestClass {

	@Autowired
	private InstructionService instructionService;
	
	@Test
	public void testFindAllByJobIds() {
		Collection<Integer> jobIds = new ArrayList<>();
		jobIds.add(TestDataJob.ID_CLIENT_US);
		
		Collection<Integer> jobItemIds = new ArrayList<>();
		jobItemIds.add(TestDataJobItem.ID_CLIENT_USA);

		List<InstructionDTO> results = this.instructionService.findAllByJob(jobIds, jobItemIds, TestDataSubdiv.ID_BUSINESS_USA, InstructionType.CARRIAGE, InstructionType.PACKAGING);
		// We expect 3 of each instruction type at job/company/subdiv/contact level (2 carriage and 1 packaging)
		Assertions.assertEquals(12, results.size());

		results = this.instructionService.findAllByJob(jobIds, jobItemIds, TestDataSubdiv.ID_BUSINESS_USA, InstructionType.PACKAGING);
		// We expect 1 of each instruction type at job/company/subdiv/contact level (1 packaging)
		Assertions.assertEquals(4, results.size());
	}
	
	@Test
	public void testByJobItemIds() {
		Collection<Integer> jobItemIds = new ArrayList<>();
		jobItemIds.add(TestDataJobItem.ID_CLIENT_USA);
		
		List<InstructionDTO> results = this.instructionService.findAllByJobItems(jobItemIds, TestDataSubdiv.ID_BUSINESS_USA, InstructionType.CARRIAGE, InstructionType.PACKAGING);
		// We expect 3 of each instruction type at job/company/subdiv/contact level
		Assertions.assertEquals(12, results.size());

		results = this.instructionService.findAllByJobItems(jobItemIds, TestDataSubdiv.ID_BUSINESS_USA, InstructionType.PACKAGING);
		// We expect 1 of each instruction type at job/company/subdiv/contact level (1 packaging)
		Assertions.assertEquals(4, results.size());
	}

	@Override
	protected List<String> getDataSetFileNames() {
		List<String> result = DataSetJob.getJobClientCompletedTestData();
		result.add(DataSetCompany.FILE_INSTRUCTION_CLIENT);
		return result;
	}
}
