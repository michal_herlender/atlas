package org.trescal.cwms.core.system.entity.basestatus.db;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.jobs.job.entity.jobstatus.JobStatus;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.tests.MvcCwmsTestClass;
import org.trescal.cwms.tests.generic.DataSetBaseStatus;
import org.trescal.cwms.tests.generic.DataSetCore;
import org.trescal.cwms.tests.generic.job.TestDataJobStatus;

class BaseStatusDaoImplTest extends MvcCwmsTestClass {

    @Autowired
    private BaseStatusDaoImpl baseStatusDao;

    @Override
    protected List<String> getDataSetFileNames() {
        final ArrayList<String> dataSetFiles = new ArrayList<>();
        dataSetFiles.add(DataSetBaseStatus.FILE_STATUS_INVOICE);
        dataSetFiles.add(DataSetCore.FILE_BASE_JOB_STATUS);
        return dataSetFiles;
    }

    @Test
    @Transactional
    @DisplayName("Should find default job status 'On-going'")
    void findDefaultJobStatus() {
        JobStatus jobStatus = baseStatusDao.findDefaultStatus(JobStatus.class);
        Assertions.assertTrue(jobStatus.getDefaultStatus());
        Assertions.assertEquals(TestDataJobStatus.ID_ONGOING_1, jobStatus.getStatusid());
        final Optional<Translation> englishTranslation = jobStatus.getNametranslations().stream().filter(js -> js.getLocale().equals(Locale.UK)).findFirst();
        Assertions.assertTrue(englishTranslation.isPresent());
        Assertions.assertEquals("On-going", englishTranslation.get().getTranslation());
    }
}