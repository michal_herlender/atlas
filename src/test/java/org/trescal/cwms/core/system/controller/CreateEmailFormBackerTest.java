package org.trescal.cwms.core.system.controller;

import static org.easymock.EasyMock.anyBoolean;
import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import static org.junit.Assert.assertTrue;

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.easymock.EasyMockRule;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.TestSubject;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.Assertions;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.WebContext;
import org.trescal.cwms.core.AtlasUnitTest;
import org.trescal.cwms.core.company.entity.addressprintformat.AddressPrintFormatter;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.pricing.jobcost.entity.addtionalcostcontact.AdditionalCostContact;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.dto.EmailResultWrapper;
import org.trescal.cwms.core.system.entity.businessdetails.BusinessDetails;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.emailrecipient.EmailRecipient;
import org.trescal.cwms.core.system.entity.emailrecipient.RecipientType;
import org.trescal.cwms.core.system.entity.emailtemplate.EmailTemplate;
import org.trescal.cwms.core.system.entity.emailtemplate.db.EmailTemplateService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.spring.model.KeyValue;

import io.vavr.Tuple;
import lombok.val;

@Category(AtlasUnitTest.class)
public class CreateEmailFormBackerTest extends EasyMockSupport {

	@Rule
	public EasyMockRule rule = new EasyMockRule(this);

	@TestSubject
	private final CreateEmailFormBacker backer = new CreateEmailFormBacker();

	@Mock
	private BusinessDetails bd;

	@Mock
	private ContactService conService;

	@Mock
	private SubdivService subdivService;

	@Mock
	private SystemComponentService systemComponentService;

	@Mock
	private EmailTemplateService emailTemplateService;

	@Mock
	private Contact currentContact;

	@Mock
	private EmailService emailService;

	@Mock
	private AddressPrintFormatter addressFormater;

	@Mock
	private ITemplateEngine templateEngine;

	private Contact configureContact(Contact con, Integer coid) {
		Subdiv sub = createMock(Subdiv.class);
		Company com = createMock(Company.class);
		expect(sub.getComp()).andStubReturn(com);
		expect(com.getCoid()).andStubReturn(coid);
		expect(con.getSub()).andStubReturn(sub);
		expect(con.getFirstName()).andStubReturn("FIRST_NAME");
		expect(con.getLastName()).andStubReturn("LAST_NAME");
		expect(con.getPosition()).andStubReturn(null);
		expect(con.getDefAddress()).andStubReturn(null);
		expect(con.getEmail()).andStubReturn("contact@1.de");
		expect(con.getTelephone()).andStubReturn("111");
		expect(con.getFax()).andStubReturn("111");
		expect(con.getMobile()).andStubReturn("111");
		expect(con.getTitle()).andStubReturn("Sir");

		return con;
	}

	@Test
	public void bakeWithPersonId() {
		baseConfiguration(true);
		val con = configureContact(createMock(Contact.class), 12);
		expect(conService.get(1)).andStubReturn(con);
		replayAll();
		val form = backer.bake(Locale.UK, new KeyValue<>(1, "a"), currentContact, 1, 0, 0, 0, "", bd, 14);
		assertNotNull(form);
		assertEquals(form.getFrom(), "test@email.com");
		assertEquals(form.getCoId().longValue(), 12);
		assertFalse(form.isComponentEmail());
	}

	@Test
	public void bakeWihUnsuitableParameters() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> backer.bake(Locale.UK, new KeyValue<>(1, "a"), currentContact, 0, 12, 0, 1, "", bd, 10),"Unsuitable parameters to create form backing object.");
		Assertions.assertThrows( IllegalArgumentException.class, () -> backer.bake(Locale.UK, new KeyValue<>(1, "a"), currentContact, 0, 13, 12, 0, "", bd, 10),"Unsuitable parameters to create form backing object.");
		
	}


	@Test
	public void bakeWithSysComIdAndObjIdWithIsAccountEmailAndDefaultContact() {
		baseConfiguration();
		expect(templateEngine.process(eq("email/fragments/email_footer.html"), anyObject(WebContext.class))).andReturn(" - signature");
		expect(systemComponentService.findComponent(1)).andStubReturn(prepareSystemComponent());
		expect(systemComponentService.findComponentEntity(anyObject(), eq(3))).andStubReturn(prepareJobCostingEntity(true));
		replayAll();
		val form = backer.bake(Locale.UK, new KeyValue<>(1, "a"), currentContact, 0, 0, 1, 3, "", bd, 10);
		assertEquals(form.getFrom(), "test-account@email.com");
		assertEquals(form.getCoId().longValue(), 13);
		assertTrue(form.isComponentEmail());
	}

	@Test
	public void bakeWithSysComIdAndObjIdWithNotIsAccountEmailAndNoDefaultContact() {
		baseConfiguration();
		expect(templateEngine.process(eq("email/fragments/email_footer.html"), anyObject(WebContext.class))).andReturn(" - signature");
		expect(systemComponentService.findComponent(1)).andStubReturn(prepareSystemComponent());
		expect(systemComponentService.findComponentEntity(anyObject(), eq(4))).andStubReturn(prepareJobCostingEntity(false));
		replayAll();
		val form = backer.bake(Locale.UK, new KeyValue<>(1, "a"), currentContact, 0, 0, 1, 4, "", bd, 10);
		assertEquals(form.getFrom(), "test@email.com");
		assertEquals(form.getCcCons().size(), 4);
		assertEquals(form.getEntityId().longValue(),4);
		assertTrue(form.isComponentEmail());
	}

	@Test
	public void bakeWithSysComIdAndObjIdWithNotIsAccountEmailAndNoDefaultContactAndEmailIdAndRecipients() {
		baseConfiguration();
		expect(systemComponentService.findComponent(1)).andStubReturn(prepareSystemComponent());
		expect(systemComponentService.findComponentEntity(anyObject(), eq(4))).andStubReturn(prepareJobCostingEntity(false));
		expect(emailService.getCompleteEmail(1)).andStubReturn(createEmailResultWrapper(true));
		replayAll();
		val form = backer.bake(Locale.UK, new KeyValue<>(1, "a"), currentContact, 0, 1, 1, 4, "", bd, 10);
		assertEquals(form.getFrom(), "from-email@email.com");
		assertEquals(form.getSubject(), "SUBJECT_OF_EMAIL");
		assertEquals(form.getBody(), "BODY_OF_EMAIL");
		assertEquals(form.getToEmails().size(), 2);
		assertEquals(form.getCcEmails().size(), 2);
		assertEquals(form.getToCons().size(), 2);
		assertEquals(form.getCcCons().size(), 2);
	}

	@Test
	public void bakeWithSysComIdAndObjIdWithNotIsAccountEmailAndNoDefaultContactAndEmailId() {
		baseConfiguration();
		expect(systemComponentService.findComponent(1)).andStubReturn(prepareSystemComponent());
		expect(systemComponentService.findComponentEntity(anyObject(), eq(4))).andStubReturn(prepareJobCostingEntity(false));
		expect(emailService.getCompleteEmail(1)).andStubReturn(createEmailResultWrapper(false));
		replayAll();
		val form = backer.bake(Locale.UK, new KeyValue<>(1, "a"), currentContact, 0, 1, 1, 4, "", bd, 10);
		assertEquals(form.getFrom(), "from-email@email.com");
		assertEquals(form.getSubject(), "SUBJECT_OF_EMAIL");
		assertEquals(form.getBody(), "BODY_OF_EMAIL");
		assertEquals(form.getToEmails().size(), 0);
		assertEquals(form.getCcEmails().size(), 0);
		assertEquals(form.getToCons().size(), 0);
		assertEquals(form.getCcCons().size(), 0);
	}

	@Test
	public void bakeWithSysComIdAndObjId() {
		baseConfiguration();
		expect(templateEngine.process(eq("email/fragments/email_footer.html"), anyObject(WebContext.class))).andReturn(" - signature");
		expect(systemComponentService.findComponent(1)).andStubReturn(prepareSystemComponent());
		expect(systemComponentService.findComponentEntity(anyObject(), eq(2))).andStubReturn(prepareJobCostingEntity(true));
		replayAll();
		val form = backer.bake(Locale.UK, new KeyValue<>(1, "a"), currentContact, 0, 0, 1, 2, "", bd, 10);
		assertEquals("test-account@email.com", form.getFrom());
		assertEquals("Email Subject: FIRST_NAME", form.getSubject());
		assertEquals("Email template: LAST_NAME - signature", form.getBody());
		assertEquals(13, form.getCoId().longValue());
	}

	private JobCosting prepareJobCostingEntity(Boolean isAccountEmail) {
		val entity = this.<JobCosting, JobCosting>createMock(JobCosting.class);
		expect(entity.isAccountsEmail()).andStubReturn(isAccountEmail);
		val contact = configureContact(createMock(Contact.class), 13);
		Set<AdditionalCostContact> additionalContacts = Stream.of(1, 2, 3, 4).map(ignored -> this.<Contact, Contact>createMock(Contact.class))
				.map(c -> {
					val ac = this.<AdditionalCostContact, AdditionalCostContact>createMock(AdditionalCostContact.class);
					expect(ac.getContact()).andStubReturn(c);
					return ac;
				}).collect(Collectors.toSet());
		expect(entity.getAdditionalContacts()).andStubReturn(additionalContacts);
		expect(entity.getDefaultContact()).andStubReturn(contact);
		expect(entity.getIdentifier()).andStubReturn("myId");
		return entity;
	}

	private SystemComponent prepareSystemComponent() {
		val sysComponent = this.<SystemComponent, SystemComponent>createMock(SystemComponent.class);
		val component = Component.JOB_COSTING;
		expect(sysComponent.getComponent()).andStubReturn(component);
		return sysComponent;
	}

	private void baseConfiguration() {
		baseConfiguration(false);
	}

	private void baseConfiguration(Boolean simplify) {
		configureContact(currentContact, 10);
		expect(bd.getSalesEmail()).andStubReturn("test@email.com");
		expect(currentContact.getUserPreferences()).andStubReturn(null);
		expect(addressFormater.getAddressText(anyObject(),anyObject(),anyBoolean()))
		.andStubReturn(Collections.singletonList("CONTACT_ADDRESS"));
		if (!simplify) {
			expect(bd.getAccountsEmail()).andStubReturn("test-account@email.com");
			expect(subdivService.get(1)).andStubReturn(createMock(Subdiv.class));
			expect(emailTemplateService.findBestTemplate(anyObject(), anyObject(), anyObject())).andStubReturn(createEmailTemplate());
		}

	}

	private EmailResultWrapper createEmailResultWrapper(Boolean withRecipients) {
		val wrapper = this.<EmailResultWrapper, EmailResultWrapper>createMock(EmailResultWrapper.class);
		expect(wrapper.getEmail()).andStubReturn(createEmail(withRecipients));
		expect(wrapper.getAttachments()).andStubReturn(new String[0][0]);
		expect(wrapper.getBody()).andStubReturn("BODY_OF_EMAIL");
		return wrapper;
	}

	private Email createEmail(Boolean withRecipients) {
		val email = this.<Email, Email>createMock(Email.class);
		expect(email.getFrom()).andStubReturn("from-email@email.com");
		List<EmailRecipient> recipients = withRecipients ? recipientsList() : Collections.emptyList();
		expect(email.getRecipients()).andStubReturn(recipients);
		expect(email.getSubject()).andStubReturn("SUBJECT_OF_EMAIL");
		return email;
	}

	private List<EmailRecipient> recipientsList() {
		return Stream.of(
				Tuple.of(true, RecipientType.EMAIL_TO),
				Tuple.of(true, RecipientType.EMAIL_CC),
				Tuple.of(false, RecipientType.EMAIL_TO),
				Tuple.of(false, RecipientType.EMAIL_CC),
				Tuple.of(true, RecipientType.EMAIL_TO),
				Tuple.of(true, RecipientType.EMAIL_CC),
				Tuple.of(false, RecipientType.EMAIL_TO),
				Tuple.of(false, RecipientType.EMAIL_CC)
				).map(t -> {
					val er = this.<EmailRecipient, EmailRecipient>createMock(EmailRecipient.class);
					if (t._1) {
						expect(er.getRecipientCon()).andStubReturn(createMock(Contact.class));
					} else {
						expect(er.getRecipientCon()).andStubReturn(null);
						expect(er.getEmailAddress()).andStubReturn("recipent@email.com");
					}
					expect(er.getType()).andStubReturn(t._2);
					return er;
				}).collect(Collectors.toList());
	}

	private EmailTemplate createEmailTemplate() {
		val temp = this.<EmailTemplate, EmailTemplate>createMock(EmailTemplate.class);
		expect(temp.getSubject()).andStubReturn("Email Subject: " + Constants.EMAIL_REPLACE_SENDER_FIRSTNAME);
		expect(temp.getTemplate()).andStubReturn("Email template: " + Constants.EMAIL_REPLACE_SENDER_LASTNAME);
		return temp;
	}

}

