package org.trescal.cwms.core.system.entity.defaultnote.db;

import lombok.val;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.pricing.invoice.entity.invoicenote.InvoiceNote;
import org.trescal.cwms.core.quotation.entity.quotenote.QuoteNote;
import org.trescal.cwms.core.system.entity.defaultnote.DefaultNote;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.tools.supportedlocale.SingleLocaleSupportedLocaleServiceImpl;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;

import java.util.Collections;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@Tag("org.trescal.cwms.core.AtlasUnitTest")
@ExtendWith(MockitoExtension.class)
class DefaultNoteServiceImplTest {

    @Mock
    DefaultNoteDao defaultNoteDao;

    SupportedLocaleService supportedLocaleService = new SingleLocaleSupportedLocaleServiceImpl(Locale.FRANCE);

    @Test
    @DisplayName("Should return empty stream if there is no default notes for component")
    void empty_createNotesFromDefaultNotesForComponent() {
        when(defaultNoteDao.getDefaultNotesByComponent(any(), anyInt())).thenReturn(Collections.emptyList());
        val defaultNoteService = new DefaultNoteServiceImpl(defaultNoteDao, null, supportedLocaleService, null);
        assertFalse(defaultNoteService.createNotesFromDefaultNotesForComponent(Component.QUOTATION,
                QuoteNote.class, 12, Locale.US, new Contact()).findFirst().isPresent());
    }


    @Test
    @DisplayName("Should copy default message fields into the proper note instance")
    void singleNote_createNotesFromDefaultNotesForComponent() {
        when(defaultNoteDao.getDefaultNotesByComponent(any(), anyInt()))
                .thenReturn(Collections.singletonList(createDefaultNote("defaultNote", "defaultLabel", "Note_fr", "Label_fr", Locale.FRANCE)));
        val defaultNoteService = new DefaultNoteServiceImpl(defaultNoteDao, null, supportedLocaleService, null);
        val notes = defaultNoteService.createNotesFromDefaultNotesForComponent(Component.QUOTATION, QuoteNote.class, 1, Locale.FRANCE, new Contact()).findFirst();
        assertEquals(Optional.of("Note_fr"), notes.map(QuoteNote::getNote));
        assertEquals(Optional.of("Label_fr"), notes.map(QuoteNote::getLabel));
    }

    @Test
    @DisplayName("Should falback to default if translation is empty")
    void singleNote_default_createNotesFromDefaultNotesForComponent() {
        when(defaultNoteDao.getDefaultNotesByComponent(any(), anyInt()))
                .thenReturn(Collections.singletonList(createDefaultNote("Note", "Label", "", "", Locale.FRANCE)));
        val defaultNoteService = new DefaultNoteServiceImpl(defaultNoteDao, null, supportedLocaleService, null);
        val notes = defaultNoteService.createNotesFromDefaultNotesForComponent(Component.QUOTATION, QuoteNote.class, 1, Locale.FRANCE, new Contact()).findFirst();
        assertEquals(Optional.of("Note"), notes.map(QuoteNote::getNote));
        assertEquals(Optional.of("Label"), notes.map(QuoteNote::getLabel));
    }

    @Test
    @DisplayName("Should map multiple notes")
    void multipleNotes_createNotesFromDefaultNotesForComponent() {
        when(defaultNoteDao.getDefaultNotesByComponent(any(), anyInt())).thenReturn(
                Stream.of(
                        createDefaultNote("Note2", "Label2", "Note_de", "Label_de", Locale.GERMAN),
                        createDefaultNote("Note1", "Label1", "", "", Locale.GERMAN)
                ).collect(Collectors.toList())
        );
        val defaultNoteService = new DefaultNoteServiceImpl(defaultNoteDao, null, supportedLocaleService, null);
        val results = defaultNoteService.createNotesFromDefaultNotesForComponent(Component.INVOICE, InvoiceNote.class, 12, Locale.GERMAN, new Contact()).collect(Collectors.toSet());
        assertEquals(Stream.of("Note1", "Note_de").collect(Collectors.toSet()), results.stream().map(InvoiceNote::getNote).collect(Collectors.toSet()));
        assertEquals(Stream.of("Label1", "Label_de").collect(Collectors.toSet()), results.stream().map(InvoiceNote::getLabel).collect(Collectors.toSet()));
    }

    private DefaultNote createDefaultNote(String defNote, String defLabel, String note, String label, Locale locale) {
        val dn = new DefaultNote();
        dn.setPublish(true);
        dn.setNote(defNote);
        dn.setLabel(defLabel);
        dn.setLabelTranslation(Collections.singleton(createTranslation(label, locale)));
        dn.setNoteTranslation(Collections.singleton(createTranslation(note, locale)));
        return dn;
    }

    private Translation createTranslation(String msg, Locale locale) {
        val translation = new Translation();
        translation.setLocale(locale);
        translation.setTranslation(msg);
        return translation;
    }
}

