package org.trescal.cwms.core.instrument.entity.instrument.db;

import lombok.val;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.trescal.cwms.core.system.enums.IntervalUnit;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;

import static org.junit.jupiter.api.Assertions.assertEquals;


@Tag("org.trescal.cwms.core.AtlasUnitTest")
public class InstrumentPercentageServiceTest {

    private InstrumentPercentageService instrumentPercentageService;

    private final String currentDate = "2015-10-16T15:23:30.0Z";
    private final LocalDate referentialDate = LocalDate.parse("2015-10-16");

    @BeforeEach
    void beforeEach() {
        val clock = Clock.fixed(Instant.parse(currentDate), ZoneId.of("Asia/Calcutta"));
        instrumentPercentageService = new InstrumentPercentageService(clock);
    }

    @Test
    @DisplayName("Total percentage should be 100% if due Date is in the past")
    void totalCalibrationPercentage_100() {
        assertEquals(100, instrumentPercentageService.totalCalibrationPercentage(referentialDate.minusMonths(6), IntervalUnit.MONTH, 12));
    }

    @Test
    @DisplayName("Total percentage should be 0 if interval is shorter than remaining days")
    void totalCalibrationPercentage_0() {
        assertEquals(0, instrumentPercentageService.totalCalibrationPercentage(referentialDate.plusMonths(2), IntervalUnit.MONTH, 1));
        assertEquals(0, instrumentPercentageService.totalCalibrationPercentage(referentialDate.plusMonths(1), IntervalUnit.MONTH, 1));
    }

    @Test
    @DisplayName("Total percentage should be 10 if 10% of time has passed")
    void totalCalibrationPercentage_10() {
        assertEquals(10, instrumentPercentageService.totalCalibrationPercentage(referentialDate.plusDays(9), IntervalUnit.DAY, 10));
    }

    @Test
    @DisplayName("Total percentage should be 90 if 90% of time has passed")
    void totalCalibrationPercentage_90() {
        assertEquals(90, instrumentPercentageService.totalCalibrationPercentage(referentialDate.plusDays(1), IntervalUnit.DAY, 10));
    }

    @Test
    @DisplayName("Final month percentage should be 100% if due Date is in the past")
    void finalMonthPercentage_100() {
        assertEquals(100, instrumentPercentageService.finalMonthPercentage(referentialDate.minusDays(5), IntervalUnit.MONTH, 12));
    }

    @Test
    @DisplayName("Final month percentage should be 0 if interval is shorter than remaining days")
    void finalMonthPercentage_0() {
        assertEquals(0, instrumentPercentageService.finalMonthPercentage(referentialDate.plusMonths(2), IntervalUnit.MONTH, 1));
        assertEquals(0, instrumentPercentageService.finalMonthPercentage(referentialDate.plusMonths(1), IntervalUnit.MONTH, 1));
    }

    @ParameterizedTest(name = "Final month percentage should be {0}% ")
    @ValueSource(ints = {0, 10, 20, 30, 50, 60, 70, 90})
    void finalMonthPercentage_10(int delta) {
        assertEquals(delta, instrumentPercentageService.finalMonthPercentage(referentialDate.plusDays(30).minusDays(Math.round(delta * 30.0 / 100)), IntervalUnit.MONTH, 10));
    }


    @Test
    @DisplayName("All but last month percentage should be 100% if due Date is in the past")
    void remainingPercentage_100() {
        assertEquals(100, instrumentPercentageService.remainingPercentage(referentialDate.minusDays(10), IntervalUnit.MONTH, 12));
    }

    @Test
    @DisplayName("All but last month percentage should be 0 if interval is shorter than remaining days")
    void remainingPercentage_0() {
        assertEquals(0, instrumentPercentageService.remainingPercentage(referentialDate.plusMonths(2), IntervalUnit.MONTH, 1));
        assertEquals(0, instrumentPercentageService.remainingPercentage(referentialDate.plusMonths(2), IntervalUnit.MONTH, 1));
    }

    @Test
    @DisplayName("Should be 0 if period is shorter than 30 days")
    void remainingPercentage_0_if_short() {
        assertEquals(0, instrumentPercentageService.remainingPercentage(referentialDate.plusDays(2), IntervalUnit.WEEK, 2));
        assertEquals(0, instrumentPercentageService.remainingPercentage(referentialDate.plusDays(2), IntervalUnit.WEEK, 4));
        assertEquals(0, instrumentPercentageService.remainingPercentage(referentialDate.plusDays(2), IntervalUnit.DAY, 1));
    }

    @ParameterizedTest(name = "All but last month percentage should be {0}")
    @ValueSource(ints = {10, 20, 30, 40, 50, 60, 70, 80, 90})
    void remainingPercentage_int(int delta) {
        //Currently, Trescal's IntervalUnit.Months always have 30 days, so we cannot test it with *.plusMonth(*) method
        assertEquals(delta, instrumentPercentageService.remainingPercentage(
            referentialDate.plusDays(11 * 30).minusDays(3L * delta)
            , IntervalUnit.MONTH, 11));
    }

}