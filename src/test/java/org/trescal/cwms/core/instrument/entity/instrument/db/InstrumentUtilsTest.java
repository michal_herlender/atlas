package org.trescal.cwms.core.instrument.entity.instrument.db;

import static org.junit.Assert.assertEquals;
import static org.trescal.cwms.core.instrument.entity.instrument.db.InstrumentUtils.simplifyId;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.trescal.cwms.core.AtlasUnitTest;

@Category(AtlasUnitTest.class)
public class InstrumentUtilsTest {

    @Test
    public void simplifyIdDashTest() {
        assertEquals("ab",simplifyId("a-b"));
    }

    @Test
    public void  simplifyIdSlashTest(){
        assertEquals("ab",simplifyId("a/b"));
        assertEquals("ab",simplifyId("a\\b"));
    }

    @Test
    public void simplifyIdPlusTest() {
        assertEquals("ab",simplifyId("a+b"));
    }

    @Test
    public void simplifyIdPlusUnderscore() {
        assertEquals("ab",simplifyId("a_b"));
        assertEquals("ab",simplifyId("a__b"));
    }

    @Test
    public void simplifyIdBrackets() {
        assertEquals("ab",simplifyId("a)b"));
        assertEquals("ab",simplifyId("a]b"));
        assertEquals("ab",simplifyId("a}b"));
        assertEquals("ab",simplifyId("a(b"));
        assertEquals("ab",simplifyId("a[b"));
        assertEquals("ab",simplifyId("a{b"));
    }

    @Test
    public void simplifyIdDot() {
        assertEquals("ab",simplifyId("a.b"));
    }

    @Test
    public void simplifyIdWhiteSpaces() {
        assertEquals("ab",simplifyId("a b"));
        assertEquals("ab",simplifyId("a\tb"));
    }
    @Test
    public void simplifyId0() {
        assertEquals("a0b",simplifyId("aob"));
        assertEquals("a0b",simplifyId("aOb"));
        assertEquals("a0b",simplifyId("aOb"));
        assertEquals("b",simplifyId("O0oO00b"));
    }
    @Test
    public void simplifyId1() {
        assertEquals("1b",simplifyId("ib"));
        assertEquals("1b",simplifyId("lb"));
        assertEquals("1b",simplifyId("Ib"));
        assertEquals("1b",simplifyId("Lb"));
    }
    @Test
    public void simplifyId2() {
        assertEquals("2b",simplifyId("zb"));
    }
    @Test
    public void simplifyId5() {
        assertEquals("a5",simplifyId("as"));
    }
}