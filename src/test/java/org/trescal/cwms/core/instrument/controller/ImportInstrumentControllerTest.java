package org.trescal.cwms.core.instrument.controller;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;

import java.io.InputStream;
import java.util.Collections;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.trescal.cwms.core.documents.excel.utils.ExcelFileReaderUtil;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.db.ExchangeFormatService;
import org.trescal.cwms.tests.MvcCwmsTestClass;

public class ImportInstrumentControllerTest extends MvcCwmsTestClass {

	public static final String DATA_SET = "cwms/instrument/ImportInstrumentFromFile.xml";
	public static final String EXCEL_FILE = "cwms/instrument/importinstrument.xlsx";
	public static final String Second_EXCEL_FILE = "cwms/instrument/importinstrumentEmpty.xlsx";
	public static final String Missing_mandatorycolumns_EXCEL_FILE = "cwms/instrument/importinstrument_mandatorycolumns.xlsx";
	public static final String Missing_mandatorydata_EXCEL_FILE = "cwms/instrument/importinstrument_mandatorydata.xlsx";
	
	@Autowired
	private ExcelFileReaderUtil excelFileReaderUtil;
	@Autowired
	private ExchangeFormatService efService;
	

	@Override
	protected List<String> getDataSetFileNames() {
		return Collections.singletonList(DATA_SET);
	}
	 	

	@Test
	public void testMinimsuccessful() throws Exception {

		MultiValueMap<String, String> inputPayload = new LinkedMultiValueMap<>();
		inputPayload.add("addrid", "1");
		inputPayload.add("defaultServiceTypeId", "1");
		inputPayload.add("exchangeFormatId", "1");
		inputPayload.add("personid", "1");
		inputPayload.add("subdivid", "2");

		InputStream is = getClass().getResourceAsStream("classpath:test-data/" + EXCEL_FILE);
		MockMultipartFile file = new MockMultipartFile("file", "importinstrument.xlsx", "text/plain",
				IOUtils.toByteArray(is));

		MvcResult result = this.mockMvc.perform(fileUpload("/importinstruments.htm").file(file).params(inputPayload))
				.andExpect(redirectedUrl("/importedinstrumentssynthesis.htm")).andDo(print()).andReturn();

		ExchangeFormat ef = efService.get(1);
		List<LinkedCaseInsensitiveMap<String>> fileContent = excelFileReaderUtil.readExcelFile(file.getInputStream(),
				ef.getSheetName(), ef.getLinesToSkip());

		assertEquals(result.getFlashMap().get("fileContent").toString(), fileContent.toString());
	}

	@Test
	public void testOnEmptyExcelFile() throws Exception {

		MultiValueMap<String, String> inputPayload = new LinkedMultiValueMap<>();
		inputPayload.add("addrid", "1");
		inputPayload.add("defaultServiceTypeId", "1");
		inputPayload.add("exchangeFormatId", "1");
		inputPayload.add("personid", "1");
		inputPayload.add("subdivid", "2");

		InputStream is = getClass().getResourceAsStream("classpath:test-data/" + Second_EXCEL_FILE);
		MockMultipartFile file = new MockMultipartFile("file", "importinstrumentEmpty.xlsx", "text/plain",
				IOUtils.toByteArray(is));

		MvcResult result = this.mockMvc.perform(fileUpload("/importinstruments.htm").file(file).params(inputPayload))
				.andDo(print()).andReturn();

		BindingResult formBindingResult = (BindingResult) result.getModelAndView().getModel()
				.get(BindingResult.MODEL_KEY_PREFIX + ImportInstrumentsController.FORM);

		assertEquals(formBindingResult.getGlobalError().getCode(), "exchangeformat.emptyfile");

	}

	@Test
	public void testOnEmptyExchangeFormate() throws Exception {

		MultiValueMap<String, String> inputPayload = new LinkedMultiValueMap<>();
		inputPayload.add("addrid", "1");
		inputPayload.add("defaultServiceTypeId", "1");
		inputPayload.add("personid", "1");
		inputPayload.add("subdivid", "2");

		InputStream is = getClass().getResourceAsStream("classpath:test-data/" + EXCEL_FILE);
		MockMultipartFile file = new MockMultipartFile("file", "importinstrument.xlsx", "text/plain",
				IOUtils.toByteArray(is));

		MvcResult result = this.mockMvc.perform(fileUpload("/importinstruments.htm").file(file).params(inputPayload))
				.andDo(print()).andReturn();

		BindingResult formBindingResult = (BindingResult) result.getModelAndView().getModel()
				.get(BindingResult.MODEL_KEY_PREFIX + ImportInstrumentsController.FORM);

		assertEquals(formBindingResult.getFieldError().getCode(), "error.instrument.exchangeformat.notnull");

	}

	@Test
	public void testOnEmptyaddrid() throws Exception {

		MultiValueMap<String, String> inputPayload = new LinkedMultiValueMap<>();

		inputPayload.add("defaultServiceTypeId", "1");
		inputPayload.add("exchangeFormatId", "1");
		inputPayload.add("personid", "1");
		inputPayload.add("subdivid", "2");

		InputStream is = getClass().getResourceAsStream("classpath:test-data/" + EXCEL_FILE);
		MockMultipartFile file = new MockMultipartFile("file", "importinstrument.xlsx", "text/plain",
				IOUtils.toByteArray(is));

		MvcResult result = this.mockMvc.perform(fileUpload("/importinstruments.htm").file(file).params(inputPayload))
				.andDo(print()).andReturn();

		BindingResult formBindingResult = (BindingResult) result.getModelAndView().getModel()
				.get(BindingResult.MODEL_KEY_PREFIX + ImportInstrumentsController.FORM);

		assertEquals(formBindingResult.getFieldError().getCode(), "error.instrument.address.notnull");

	}

	@Test
	public void testOnEmptypersonid() throws Exception {

		MultiValueMap<String, String> inputPayload = new LinkedMultiValueMap<>();

		inputPayload.add("addrid", "1");
		inputPayload.add("defaultServiceTypeId", "1");
		inputPayload.add("exchangeFormatId", "1");
		inputPayload.add("subdivid", "2");

		InputStream is = getClass().getResourceAsStream("classpath:test-data/" + EXCEL_FILE);
		MockMultipartFile file = new MockMultipartFile("file", "importinstrument.xlsx", "text/plain",
				IOUtils.toByteArray(is));

		MvcResult result = this.mockMvc.perform(fileUpload("/importinstruments.htm").file(file).params(inputPayload))
				.andDo(print()).andReturn();

		BindingResult formBindingResult = (BindingResult) result.getModelAndView().getModel()
				.get(BindingResult.MODEL_KEY_PREFIX + ImportInstrumentsController.FORM);

		assertEquals(formBindingResult.getFieldError().getCode(), "error.instrument.contact.notnull");

	}

	@Test
	public void testOnMissingMandatoryColumns() throws Exception {

		MultiValueMap<String, String> inputPayload = new LinkedMultiValueMap<>();
		inputPayload.add("addrid", "1");
		inputPayload.add("defaultServiceTypeId", "1");
		inputPayload.add("exchangeFormatId", "1");
		inputPayload.add("personid", "1");
		inputPayload.add("subdivid", "2");

		InputStream is = getClass().getResourceAsStream("classpath:test-data/" + Missing_mandatorycolumns_EXCEL_FILE);
		MockMultipartFile file = new MockMultipartFile("file", "importinstrument_mandatorycolumns.xlsx", "text/plain",
				IOUtils.toByteArray(is));

		MvcResult result = this.mockMvc.perform(fileUpload("/importinstruments.htm").file(file).params(inputPayload))
				.andDo(print()).andReturn();

		BindingResult formBindingResult = (BindingResult) result.getModelAndView().getModel()
				.get(BindingResult.MODEL_KEY_PREFIX + ImportInstrumentsController.FORM);

		assertEquals(formBindingResult.getGlobalError().getCode(),
				"exchangeformat.import.validation.mandatorycolumns.missing");

	}

	@Test
	public void testOnMissingMandatoryData() throws Exception {

		MultiValueMap<String, String> inputPayload = new LinkedMultiValueMap<>();
		inputPayload.add("addrid", "1");
		inputPayload.add("defaultServiceTypeId", "1");
		inputPayload.add("exchangeFormatId", "1");
		inputPayload.add("personid", "1");
		inputPayload.add("subdivid", "2");

		InputStream is = getClass().getResourceAsStream("classpath:test-data/" + Missing_mandatorydata_EXCEL_FILE);
		MockMultipartFile file = new MockMultipartFile("file", "importinstrument_mandatorydata.xlsx", "text/plain",
				IOUtils.toByteArray(is));

		MvcResult result = this.mockMvc.perform(fileUpload("/importinstruments.htm").file(file).params(inputPayload))
				.andDo(print()).andReturn();

		BindingResult formBindingResult = (BindingResult) result.getModelAndView().getModel()
				.get(BindingResult.MODEL_KEY_PREFIX + ImportInstrumentsController.FORM);

		assertEquals(formBindingResult.getGlobalError().getCode(),
				"exchangeformat.import.validation.mandatorycolumns.missingdata");

	}

	
}
