package org.trescal.cwms.core.instrument.entity.instrument.db;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.instrument.dto.InstrumentProjectionDTO;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocales;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetInstrument;
import org.trescal.cwms.tests.generic.instrument.TestDataInstrument;
import org.trescal.cwms.tests.generic.instrument.TestDataInstrumentModel;
import org.trescal.cwms.tests.generic.instrument.TestDataInstrumentModelTranslation;
import org.trescal.cwms.tests.generic.instrument.TestDataMfr;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

public class InstrumDaoImplTest_getInstrumentProjectionDTOs extends MvcRestTestClass {

	@Autowired
    private InstrumDao instrumDao;
	
	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetInstrument.getInstrumentClientTestData();
	}
	
	@Test
	public void testMultipleplantIds() {
		
		Locale locale = SupportedLocales.FRENCH_FR;
		Collection<Integer> plantIds = new ArrayList<>();
		plantIds.add(TestDataInstrument.ID_FRANCE_CALIPER_1101);
		
		List<InstrumentProjectionDTO> dtos = this.instrumDao.getInstrumentProjectionDTOs(locale, plantIds);
		Assertions.assertEquals(plantIds.size(), dtos.size());
		InstrumentProjectionDTO dto1 = dtos.get(0);
		
		Assertions.assertEquals(TestDataInstrument.ID_FRANCE_CALIPER_1101,dto1.getPlantid());
		Assertions.assertEquals(TestDataInstrument.PLANT_NO_1101,dto1.getPlantno());
		Assertions.assertEquals(TestDataInstrument.FORMER_BARCODE_1101,dto1.getFormerBarcode());
		Assertions.assertEquals(TestDataInstrument.SERIAL_NO_1101,dto1.getSerialno());
		Assertions.assertEquals(TestDataInstrument.SCRAPPED_1101,dto1.getScrapped());
		Assertions.assertEquals(TestDataInstrument.CALIBRATION_STANDARD_1101,dto1.getCalibrationStandard());
		Assertions.assertEquals(TestDataInstrument.MODEL_ID_1101,dto1.getModelid());
		Assertions.assertEquals(TestDataInstrument.MODEL_NAME_1101,dto1.getInstrumentModelName());

		Assertions.assertEquals(TestDataInstrumentModelTranslation.TRANSLATION_FR_FR_CALIPER_300MM,dto1.getInstrumentModelTranslation());
		Assertions.assertEquals(TestDataInstrumentModel.MODEL_MFR_CALIPER_300MM,dto1.getModelMfrType());
		Assertions.assertEquals(TestDataInstrumentModel.SUBFAMILYTYPOLOGY_CALIPER_300MM,dto1.getSubfamilyTypology());
		Assertions.assertEquals(TestDataInstrumentModel.MFR_ID_CALIPER_300MM,dto1.getInstrumentMfrId());
		Assertions.assertEquals(TestDataInstrumentModel.MODEL_TYPE_CALIPER_300MM,dto1.getInstrumentMfrGeneric());
		Assertions.assertEquals(TestDataMfr.MFR_NAME,dto1.getInstrumentMfrName());
		Assertions.assertEquals(TestDataInstrumentModel.MODEL_NAME_CALIPER_300MM,dto1.getModelname());
		Assertions.assertEquals(TestDataInstrument.CUSTOMER_DESCRIPTION_1101,dto1.getCustomerDescription());
		Assertions.assertEquals(TestDataInstrument.ADDRESS_ID_1101,dto1.getAddressId());
		Assertions.assertEquals(TestDataInstrument.CONTACT_ID_1101,dto1.getContactId());
		Assertions.assertEquals(TestDataInstrument.LOCATION_ID_1101,dto1.getLocationId());
		
	}
}
