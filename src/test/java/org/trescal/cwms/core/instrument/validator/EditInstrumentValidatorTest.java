package org.trescal.cwms.core.instrument.validator;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.company.entity.location.db.LocationService;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.instrument.form.EditInstrumentForm;
import org.trescal.cwms.core.instrument.form.EditInstrumentValidator;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.instrument.TestDataInstrument;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;


public class EditInstrumentValidatorTest extends MvcRestTestClass {

    /*public static final String DATA_SET = "cwms/instrument/ImportedInstrumentFile.xml";*/
    public static final String DATA_SET = "web/WebEditInstrumentControllerTest.xml";

    @Autowired
    private EditInstrumentValidator validator;

	@Autowired
	private ContactService contactService;
	
	@Autowired
	CompanyService companyService;

	@Autowired
	private AddressService addressService;

	@Autowired
	private SubdivService subDivService;

	@Autowired
	private LocationService locationService;

	@Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }
	
	

	@Test
	@Transactional
	public void editInstrumentConsistencyTest_Success(){
	
		EditInstrumentForm aif = new EditInstrumentForm();
		aif.setPersonid(TestDataInstrument.SUCCESS_PERSONID);
		aif.setAddrid(TestDataInstrument.SUCCESS_ADDRESSID);
		aif.setLocid(TestDataInstrument.SUCCESS_LOCATIONID);
		aif.setCoid(TestDataInstrument.COID);
		
		Contact contactId =  contactService.get(aif.getPersonid());
		Address addressId = addressService.get(aif.getAddrid());
		Location locationId = locationService.get(aif.getLocid()); //null
		Company companyId = companyService.get(aif.getCoid());

		int contId = contactId.getSub().getComp().getCoid();  	 //1
		int addId = addressId.getSub().getComp().getCoid();
		int locId = locationId.getAdd().getSub().getComp().getCoid();
		int compId = companyId.getCoid();
		
		Assert.assertEquals(contId,compId);
		Assert.assertEquals(addId,compId);
		Assert.assertEquals(locId,compId);
		
	}
	
	/*
	 * contactId is not consistent with CompanyId
	 * */
	@Test
	@Transactional
	public void editInstrumentContactIdConsistencyTest_Fail(){
	
		EditInstrumentForm aif = new EditInstrumentForm();
		aif.setPersonid(TestDataInstrument.FAIL_PERSONID_1);
		aif.setAddrid(TestDataInstrument.FAIL_ADDRESSID_1);
		aif.setLocid(TestDataInstrument.FAIL_LOCATIONID_1);
		aif.setCoid(TestDataInstrument.COID);
		
		Contact contactId =  contactService.get(aif.getPersonid());
		Address addressId = addressService.get(aif.getAddrid());
		Location locationId = locationService.get(aif.getLocid()); 
		Company companyId = companyService.get(aif.getCoid());

		int contId = contactId.getSub().getComp().getCoid();  	 
		int addId = addressId.getSub().getComp().getCoid();
		int locId = locationId.getAdd().getSub().getComp().getCoid();
		int compId = companyId.getCoid();
		
		Assert.assertNotEquals(contId, compId);
		Assert.assertEquals(addId,compId);
		Assert.assertEquals(locId,compId);
	}
	
	/*
	 * addressId is not consistent with CompanyId
	 * */
	@Test
	@Transactional
	public void editInstrumentAddressIdConsistencyTest_Fail(){
	
		EditInstrumentForm aif = new EditInstrumentForm();
		aif.setPersonid(TestDataInstrument.FAIL_PERSONID_2);
		aif.setAddrid(TestDataInstrument.FAIL_ADDRESSID_2);
		aif.setLocid(TestDataInstrument.FAIL_LOCATIONID_2);
		aif.setCoid(TestDataInstrument.COID);
		
		Contact contactId =  contactService.get(aif.getPersonid());
		Address addressId = addressService.get(aif.getAddrid());
		Location locationId = locationService.get(aif.getLocid()); 
		Company companyId = companyService.get(aif.getCoid());

		int contId = contactId.getSub().getComp().getCoid();  	 
		int addId = addressId.getSub().getComp().getCoid();
		int locId = locationId.getAdd().getSub().getComp().getCoid();
		int compId = companyId.getCoid();
		
		Assert.assertEquals(contId, compId);
		Assert.assertNotEquals(addId,compId);
		Assert.assertEquals(locId,compId);
	}
	
	/*
	 * locationId is not consistent with CompanyId
	 * */
	@Test
	@Transactional
	public void editInstrumentLocationIdConsistencyTest_Fail(){
	
		EditInstrumentForm aif = new EditInstrumentForm();
		aif.setPersonid(TestDataInstrument.FAIL_PERSONID_3);
		aif.setAddrid(TestDataInstrument.FAIL_ADDRESSID_3);
		aif.setLocid(TestDataInstrument.FAIL_LOCATIONID_3);
		aif.setCoid(TestDataInstrument.COID);
		
		Contact contactId =  contactService.get(aif.getPersonid());
		Address addressId = addressService.get(aif.getAddrid());
		Location locationId = locationService.get(aif.getLocid()); 
		Company companyId = companyService.get(aif.getCoid());

		int contId = contactId.getSub().getComp().getCoid();  	 
		int addId = addressId.getSub().getComp().getCoid();
		int locId = locationId.getAdd().getSub().getComp().getCoid();
		int compId = companyId.getCoid();
		
		Assert.assertEquals(contId, compId);
		Assert.assertEquals(addId,compId);
		Assert.assertNotEquals(locId,compId);
	}


}
