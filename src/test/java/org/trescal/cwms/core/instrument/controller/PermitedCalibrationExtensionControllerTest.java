package org.trescal.cwms.core.instrument.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.trescal.cwms.tests.MvcCwmsTestClass;

public class PermitedCalibrationExtensionControllerTest extends MvcCwmsTestClass {

	public final static String DATA_SET = "cwms/instrument/PermitedCalibrationExtensionTests.xml";
	
	@Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }
    
    @Test
    public void testGetExtensionOK() throws Exception {
    	this.mockMvc.perform(get("/calextension/get.json?plantid=1002")
    			.accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
    			.andDo(print())
    			.andExpect(status().isOk())
    			.andExpect(content().contentType("application/json;charset=UTF-8"))
    			.andExpect(jsonPath("$").exists())
    	        .andExpect(jsonPath("$.success").value(true))
                .andExpect(jsonPath("$.extensionId").isNotEmpty())
                .andExpect(jsonPath("$.extensionId").value(101));
    }

    @Test
    public void testGetExtensionFail() throws Exception {
        this.mockMvc.perform(get("/calextension/get.json?plantid=1001")
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.success").value(false))
                .andExpect(jsonPath("$.message").value("This instrument has no calibration extension set"))
                .andExpect(jsonPath("$.extensionId").isEmpty());
    }

    @Test
    public void testAddExtensionOK() throws Exception {
       this.mockMvc.perform(post("/calextension/addorupdate.json")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"plantid\" : \"1001\",\"type\" : \"PERCENTAGE\",\"value\" : \"10\"}")
                .accept(MediaType.APPLICATION_JSON))
       			.andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.extensionId").isNotEmpty())
                .andExpect(jsonPath("$.success").value(true));
    }

    @Test
    public void testAddExtensionValidationFailure() throws Exception {
        this.mockMvc.perform(post("/calextension/addorupdate.json")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"plantid\" : \"1001\",\"type\" : \"TIME\",\"value\" : \"10\"}")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.success").value(false))
                .andExpect(jsonPath("$.message").value("Calibration extension has incorrect format possibly time type with no time units"));
    }

    @Test
    public void testEditExtensionOk() throws Exception {
        this.mockMvc.perform(post("/calextension/addorupdate.json")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"plantid\" : \"1002\",\"type\" : \"TIME\",\"value\" : \"15\",\"timeUnit\" : \"MONTH\"}")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.success").value(true))
                .andExpect(jsonPath("$.type").value("TIME"));
    }

    @Test
    public void testDeleteExtensionOk() throws Exception {
        this.mockMvc.perform(get("/calextension/delete.json?plantid=1002")
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success").value(true));
    }

    @Test
    public void testDeleteExtensionFail() throws Exception {
        this.mockMvc.perform(get("/calextension/delete.json?plantid=1001")
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success").value(false));
    }


}
