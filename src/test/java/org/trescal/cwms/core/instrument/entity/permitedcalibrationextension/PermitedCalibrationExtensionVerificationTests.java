package org.trescal.cwms.core.instrument.entity.permitedcalibrationextension;

import java.util.Collections;
import java.util.List;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.entity.permitedcalibrationextension.db.PermitedCalibrationExtensionService;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.tests.MvcCwmsTestClass;

public class PermitedCalibrationExtensionVerificationTests extends MvcCwmsTestClass {

	public final static String DATA_SET = "cwms/instrument/PermitedCalibrationExtensionTests.xml";
	
	@Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }

    @Autowired
    PermitedCalibrationExtensionService extensionService;

    @Autowired
    InstrumService instrumentService;

    @Test
    public void savePercentageExtensionWithNoTimeUnitOK(){
        PermitedCalibrationExtension extension = new PermitedCalibrationExtension();
        extension.setType(PermitedCalibrationExtensionType.PERCENTAGE);
        extension.setInstrument(instrumentService.get(1001));
        extension.setValue(15);
        extensionService.save(extension);
    }

    @Test(expected = ConstraintViolationException.class)
    public void saveTimeExtensionWithNoTimeUnitFails(){
        PermitedCalibrationExtension extension = new PermitedCalibrationExtension();
        extension.setType(PermitedCalibrationExtensionType.TIME);
        extension.setInstrument(instrumentService.get(1001));
        extension.setValue(15);
        extensionService.save(extension);
    }

    @Test
    public void saveTimeExtensionWithTimeUnitOK(){
        PermitedCalibrationExtension extension = new PermitedCalibrationExtension();
        extension.setType(PermitedCalibrationExtensionType.TIME);
        extension.setInstrument(instrumentService.get(1001));
        extension.setValue(15);
        extension.setTimeUnit(IntervalUnit.DAY);
        extensionService.save(extension);
    }


}
