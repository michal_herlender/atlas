package org.trescal.cwms.core.instrument.controller;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.validation.BindingResult;
import org.trescal.cwms.core.instrument.form.ImportedInstrumentSynthesisForm;
import org.trescal.cwms.core.instrument.form.ImportedInstrumentSynthesisForm.ImportedInstrumentsSynthesisRowDTO;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.tests.MvcCwmsTestClass;

import lombok.val;

public class ImportedInstrumentsSynthesisControllerTest extends MvcCwmsTestClass {

	public static final String DATA_SET = "cwms/instrument/ImportedInstrumentFile.xml";
	public static final String USERNAME = "technician";

	@Override
	protected List<String> getDataSetFileNames() {
		return Arrays.asList("GeneratedDataSet.xml", DATA_SET);
	}

	@Test
	public void testMinimsuccessful() throws Exception {

		ImportedInstrumentsSynthesisRowDTO i = new ImportedInstrumentsSynthesisRowDTO();
		i.setSerialno("11223");
		i.setPlantno("PAR");
		i.setModel("ANALOG & DIGITAL OSCILLOSCOPE FLUKE PM3365A (BM)");
		String sDate1 = "31/12/2020";
		val date = LocalDate.parse(sDate1, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		i.setNextCalDueDate(date);
		i.setInstrumentModelId(1);
		i.setCalFrequency(12);
		i.setIntervalUnit(IntervalUnit.MONTH);
		i.setDescription("CD3");

		List<ImportedInstrumentsSynthesisRowDTO> rows = new ArrayList<>();
		rows.add(i);

		ImportedInstrumentSynthesisForm a = new ImportedInstrumentSynthesisForm();
		a.setSubdivId(1);
		a.setBusinessCompanyId(1);
		a.setDefaultServiceTypeId(1);
		a.setExchangeFormatId(1);
		a.setDefaultContactId(1);
		a.setDefaultAddressId(1);
		a.setRows(rows);

		Map<String, Object> sessionAttributes = new HashMap<>();
		sessionAttributes.put(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME);

		MvcResult result = this.mockMvc
			.perform(post("/importedinstrumentssynthesis.htm?save=Submit").sessionAttrs(sessionAttributes)
				.flashAttr("form", a))
			.andExpect(redirectedUrl("importinstruments.htm?username=technician")).andDo(print()).andReturn();

		BindingResult formBindingResult = (BindingResult) result.getModelAndView().getModel()
			.get(BindingResult.MODEL_KEY_PREFIX + ImportedInstrumentsSynthesisController.FORM);

		assertFalse(formBindingResult.hasErrors());

	}

	@Test
	public void testOnduedateinPast() throws Exception {

		ImportedInstrumentsSynthesisRowDTO i = new ImportedInstrumentsSynthesisRowDTO();
		i.setSerialno("11223");
		i.setPlantno("PAR");
		i.setModel("ANALOG & DIGITAL OSCILLOSCOPE FLUKE PM3365A (BM)");
		i.setNextCalDueDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()).minusDays(1));
		i.setInstrumentModelId(1);
		i.setCalFrequency(12);
		i.setIntervalUnit(IntervalUnit.MONTH);
		i.setDescription("CD3");

		List<ImportedInstrumentsSynthesisRowDTO> rows = new ArrayList<>();
		rows.add(i);

		ImportedInstrumentSynthesisForm a = new ImportedInstrumentSynthesisForm();
		a.setSubdivId(1);
		a.setBusinessCompanyId(1);
		a.setDefaultServiceTypeId(1);
		a.setExchangeFormatId(1);
		a.setDefaultContactId(1);
		a.setDefaultAddressId(1);
		a.setRows(rows);

		Map<String, Object> sessionAttributes = new HashMap<>();
		sessionAttributes.put(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME);

		MvcResult result = this.mockMvc.perform(post("/importedinstrumentssynthesis.htm?save=Submit")
			.sessionAttrs(sessionAttributes).flashAttr("form", a)).andDo(print()).andReturn();

		BindingResult formBindingResult = (BindingResult) result.getModelAndView().getModel()
			.get(BindingResult.MODEL_KEY_PREFIX + ImportedInstrumentsSynthesisController.FORM);

		assertNull(formBindingResult.getFieldError());
	}

	@Test
	public void testOnModelIDNonExists() throws Exception {

		ImportedInstrumentsSynthesisRowDTO i = new ImportedInstrumentsSynthesisRowDTO();
		i.setSerialno("11223");
		i.setPlantno("PAR");
		i.setModel("ANALOG & DIGITAL OSCILLOSCOPE FLUKE PM3365A (BM)");
		String sDate1 = "31/12/2020";
		LocalDate date = LocalDate.parse(sDate1, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		i.setNextCalDueDate(date);
		i.setInstrumentModelId(11);
		i.setCalFrequency(12);
		i.setIntervalUnit(IntervalUnit.MONTH);
		i.setDescription("CD3");

		List<ImportedInstrumentsSynthesisRowDTO> rows = new ArrayList<>();
		rows.add(i);

		ImportedInstrumentSynthesisForm a = new ImportedInstrumentSynthesisForm();
		a.setSubdivId(1);
		a.setBusinessCompanyId(1);
		a.setDefaultServiceTypeId(1);
		a.setExchangeFormatId(1);
		a.setDefaultContactId(1);
		a.setDefaultAddressId(1);
		a.setRows(rows);

		Map<String, Object> sessionAttributes = new HashMap<>();
		sessionAttributes.put(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME);

		MvcResult result = this.mockMvc.perform(post("/importedinstrumentssynthesis.htm?save=Submit")
				.sessionAttrs(sessionAttributes).flashAttr("form", a)).andDo(print()).andReturn();

		BindingResult formBindingResult = (BindingResult) result.getModelAndView().getModel()
				.get(BindingResult.MODEL_KEY_PREFIX + ImportedInstrumentsSynthesisController.FORM);

		assertEquals(formBindingResult.getFieldError().getCode(), "error.rest.data.notfound");

	}

	@Test
	public void testOnSerialnoAndPlantno() throws Exception {

		ImportedInstrumentsSynthesisRowDTO i = new ImportedInstrumentsSynthesisRowDTO();
		i.setSerialno("SN3");
		i.setPlantno("PN3");
		i.setModel("ANALOG & DIGITAL OSCILLOSCOPE FLUKE PM3365A (BM)");
		String sDate1 = "31/12/2020";
		LocalDate date = LocalDate.parse(sDate1, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		i.setNextCalDueDate(date);
		i.setInstrumentModelId(2);
		i.setCalFrequency(12);
		i.setIntervalUnit(IntervalUnit.MONTH);
		i.setDescription("CD3");

		List<ImportedInstrumentsSynthesisRowDTO> rows = new ArrayList<>();
		rows.add(i);

		ImportedInstrumentSynthesisForm a = new ImportedInstrumentSynthesisForm();
		a.setSubdivId(1);
		a.setBusinessCompanyId(1);
		a.setDefaultServiceTypeId(1);
		a.setExchangeFormatId(1);
		a.setDefaultContactId(1);
		a.setDefaultAddressId(1);
		a.setRows(rows);

		Map<String, Object> sessionAttributes = new HashMap<>();
		sessionAttributes.put(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME);

		MvcResult result = this.mockMvc.perform(post("/importedinstrumentssynthesis.htm?save=Submit")
				.sessionAttrs(sessionAttributes).flashAttr("form", a)).andDo(print()).andReturn();

		BindingResult formBindingResult = (BindingResult) result.getModelAndView().getModel()
				.get(BindingResult.MODEL_KEY_PREFIX + ImportedInstrumentsSynthesisController.FORM);

		assertEquals(formBindingResult.getFieldError().getCode(),
				"error.instrument.validate.serialnoandplantno.exists");

	}

	@Test
	public void testOnNullPlantno() throws Exception {

		ImportedInstrumentsSynthesisRowDTO i = new ImportedInstrumentsSynthesisRowDTO();
		i.setSerialno("11223");
		i.setModel("ANALOG & DIGITAL OSCILLOSCOPE FLUKE PM3365A (BM)");
		String sDate1 = "31/12/2020";
		LocalDate date = LocalDate.parse(sDate1, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		i.setNextCalDueDate(date);
		i.setInstrumentModelId(1);
		i.setCalFrequency(12);
		i.setIntervalUnit(IntervalUnit.MONTH);
		i.setDescription("CD3");

		List<ImportedInstrumentsSynthesisRowDTO> rows = new ArrayList<>();
		rows.add(i);

		ImportedInstrumentSynthesisForm a = new ImportedInstrumentSynthesisForm();
		a.setSubdivId(1);
		a.setBusinessCompanyId(1);
		a.setDefaultServiceTypeId(1);
		a.setExchangeFormatId(1);
		a.setDefaultContactId(1);
		a.setDefaultAddressId(1);
		a.setRows(rows);

		Map<String, Object> sessionAttributes = new HashMap<>();
		sessionAttributes.put(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME);

		MvcResult result = this.mockMvc.perform(post("/importedinstrumentssynthesis.htm?save=Submit")
				.sessionAttrs(sessionAttributes).flashAttr("form", a)).andDo(print()).andReturn();

		BindingResult formBindingResult = (BindingResult) result.getModelAndView().getModel()
				.get(BindingResult.MODEL_KEY_PREFIX + ImportedInstrumentsSynthesisController.FORM);

		assertThat(formBindingResult.getFieldError().getCode(),
				containsString("javax.validation.constraints.NotNull.message"));

	}

	

}
