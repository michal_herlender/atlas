package org.trescal.cwms.core.cdc.entity.referential;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.rest.export.dto.referential.ExportCountryDTO;

public class TestCountryCT_Json extends TestEntityCT_Json {
	
	public static Integer COUNTRYID = 123;
	public static String COUNTRY = "Country 123";
	public static String COUNTRYCODE = "CO";
	
	@Test
	public void compareFull() {
		ExportCountryDTO exportDto = new ExportCountryDTO(COUNTRYID, COUNTRY, COUNTRYCODE);

		CountryCT eventDto = new CountryCT(); 
		eventDto.setCountry(COUNTRY);
		eventDto.setCountrycode(COUNTRYCODE);
		eventDto.setCountryid(COUNTRYID);
		
		super.compareJson(exportDto, eventDto);
	}
}
