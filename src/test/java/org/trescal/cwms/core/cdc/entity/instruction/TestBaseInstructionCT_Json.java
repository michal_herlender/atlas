package org.trescal.cwms.core.cdc.entity.instruction;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;
import org.trescal.cwms.rest.export.dto.instruction.ExportBaseInstructionDTO;

public class TestBaseInstructionCT_Json extends TestEntityCT_Json {

	public static Integer INSTRUCTIONID = 123;
	public static String INSTRUCTION = "Instruction Text";
	public static InstructionType INSTRUCTIONTYPE = InstructionType.CALIBRATION;

	@Test
	public void compareFull() {
		ExportBaseInstructionDTO exportDto = new ExportBaseInstructionDTO(INSTRUCTIONID, INSTRUCTION, INSTRUCTIONTYPE); 
		
		BaseInstructionCT eventDto = new BaseInstructionCT(); 
		eventDto.setInstruction(INSTRUCTION);
		eventDto.setInstructionid(INSTRUCTIONID);
		eventDto.setInstructiontype(INSTRUCTIONTYPE);
		
		super.compareJson(exportDto, eventDto);
	}
}
