package org.trescal.cwms.core.cdc.entity.job;

import java.time.LocalDate;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.rest.export.dto.job.ExportUpcomingWorkDTO;

public class TestUpcomingWorkCT_Json extends TestEntityCT_Json {

	public static Integer ID = 123;
	public static Boolean ACTIVE = true;
	public static String DESCRIPTION = "Desc 123";
//	public static Date DUEDATE = Date.from(LocalDate.of(2021, 03, 20).atStartOfDay(ZoneId.systemDefault()).toInstant());
//	public static Date STARTDATE = Date.from(LocalDate.of(2021, 03, 15).atStartOfDay(ZoneId.systemDefault()).toInstant());
	public static LocalDate DUEDATE = LocalDate.of(2021, 03, 20);
	public static LocalDate STARTDATE = LocalDate.of(2021, 03, 15);
	public static String TITLE = "Title 123";
	public static Integer COID = 234;
	public static Integer PERSONID = 345;
	public static Integer DEPTID = 456;
	public static Integer RESPUSERID = 567;
	public static Integer ADDEDBYID = 678;
	public static Integer ORGID = 789;
	
	
	
	@Test
	public void compareFull() {
		ExportUpcomingWorkDTO exportDto = new ExportUpcomingWorkDTO(
				ID, ACTIVE, DESCRIPTION, DUEDATE, STARTDATE,
				TITLE, COID, PERSONID, DEPTID, RESPUSERID,
				ADDEDBYID, ORGID); 
		
		UpcomingWorkCT eventDto = new UpcomingWorkCT();
		eventDto.setActive(ACTIVE);
		eventDto.setAddedbyid(ADDEDBYID);
		eventDto.setCoid(COID);
		eventDto.setDeptid(DEPTID);
		eventDto.setDescription(DESCRIPTION);
		eventDto.setDueDate(DUEDATE);
		eventDto.setId(ID);
		eventDto.setOrgid(ORGID);
		eventDto.setPersonid(PERSONID);
		eventDto.setRespuserid(RESPUSERID);
		eventDto.setStartDate(STARTDATE);
		eventDto.setTitle(TITLE);
		
		super.compareJson(exportDto, eventDto);
	}
}
