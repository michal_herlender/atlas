package org.trescal.cwms.core.cdc.entity.referential;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.rest.export.dto.referential.ExportInstrumentModelDTO;

public class TestInstrumentModelCT_Json extends TestEntityCT_Json {
	
	public static Integer MODELID = 123;
	public static Integer DESCRIPTIONID = 234;
	public static Integer MFRID = 345;
	public static String MODEL = "Model 123";
	public static ModelMfrType MODELMFRTYPE = ModelMfrType.MFR_SPECIFIC;
	public static Integer MODELTYPEID = 456;
	public static Boolean QUARANTINED = true;
	public static Integer TMLID = 567;
	
	@Test
	public void compareFull() {
		ExportInstrumentModelDTO exportDto = new ExportInstrumentModelDTO(MODELID, DESCRIPTIONID, MFRID, MODEL, MODELMFRTYPE, MODELTYPEID, QUARANTINED, TMLID); 
		
		InstrumentModelCT eventDto = new InstrumentModelCT(); 
		eventDto.setDescriptionid(DESCRIPTIONID);
		eventDto.setMfrid(MFRID);
		eventDto.setModel(MODEL);
		eventDto.setModelid(MODELID);
		eventDto.setModelmfrtype(MODELMFRTYPE);
		eventDto.setModeltypeid(MODELTYPEID);
		eventDto.setQuarantined(QUARANTINED);
		eventDto.setTmlid(TMLID);
		
		super.compareJson(exportDto, eventDto);
	}
}
