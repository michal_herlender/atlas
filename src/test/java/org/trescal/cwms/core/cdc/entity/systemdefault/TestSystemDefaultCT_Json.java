package org.trescal.cwms.core.cdc.entity.systemdefault;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.core.cdc.entity.defaultdata.SystemDefaultCT;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultNames;
import org.trescal.cwms.rest.export.dto.systemdefault.ExportSystemDefaultDTO;

public class TestSystemDefaultCT_Json extends TestEntityCT_Json {

	// Corresponds to ID of system default
	public static SystemDefaultNames DEFAULT = SystemDefaultNames.ACCREDITED_LABEL_CERTIFICATE_NUMBER;
	public static String DEFAULTDATATYPE = "boolean";
	public static String DEFAULTDESCRIPTION = "Desc";
	public static String DEFAULTNAME = "Name";
	public static String DEFAULTVALUE = "true";
	public static Boolean GROUPWIDE = true;
	
	@Test
	public void compareFull() {
		ExportSystemDefaultDTO exportDto = new ExportSystemDefaultDTO(
				DEFAULT, DEFAULTDATATYPE, DEFAULTDESCRIPTION, DEFAULTNAME, DEFAULTVALUE, 
				GROUPWIDE);
		
		SystemDefaultCT eventDto = new SystemDefaultCT();
		eventDto.setDefaultdatatype(DEFAULTDATATYPE);
		eventDto.setDefaultdescription(DEFAULTDESCRIPTION);
		eventDto.setDefaultname(DEFAULTNAME);
		eventDto.setDefaultvalue(DEFAULTVALUE);
		eventDto.setGroupwide(GROUPWIDE);
		eventDto.setId(DEFAULT.ordinal());
		
		super.compareJson(exportDto, eventDto);
	}
}
