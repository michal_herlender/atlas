package org.trescal.cwms.core.cdc.entity.instrument;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.rest.export.dto.instrument.ExportCalReqDTO;

public class TestCalReqCT_Json extends TestEntityCT_Json {

	public static String TYPE = "instrument";
	public static Integer ID = 123;
	public static Boolean ACTIVE = true;
	public static String PUBLICINSTRUCTIONS = "Public";
	public static String PRIVATEINSTRUCTIONS = "Private";
	public static Integer JOBITEMID = 234;
	public static Integer PLANTID = 345;
	public static Integer COMPID = 456;
	public static Integer MODELID = 567;
	public static Integer DESCID = 678;
	
	@Test
	public void compareFull() {
		ExportCalReqDTO exportDto = new ExportCalReqDTO();
		exportDto.setActive(ACTIVE);
		exportDto.setCompid(COMPID);
		exportDto.setDescid(DESCID);
		exportDto.setId(ID);
		exportDto.setJobitemid(JOBITEMID);
		exportDto.setModelid(MODELID);
		exportDto.setPlantid(PLANTID);
		exportDto.setPrivateInstructions(PRIVATEINSTRUCTIONS);
		exportDto.setPublicInstructions(PUBLICINSTRUCTIONS);
		exportDto.setType(TYPE);
		
		CalReqCT eventDto = new CalReqCT();
		eventDto.setActive(ACTIVE);
		eventDto.setCompid(COMPID);
		eventDto.setDescid(DESCID);
		eventDto.setId(ID);
		eventDto.setJobitemid(JOBITEMID);
		eventDto.setModelid(MODELID);
		eventDto.setPlantid(PLANTID);
		eventDto.setPrivateInstructions(PRIVATEINSTRUCTIONS);
		eventDto.setPublicInstructions(PUBLICINSTRUCTIONS);
		eventDto.setType(TYPE);
		
		super.compareJson(exportDto, eventDto);
	}

}
