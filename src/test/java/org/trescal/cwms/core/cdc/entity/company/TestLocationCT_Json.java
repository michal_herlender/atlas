package org.trescal.cwms.core.cdc.entity.company;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.rest.export.dto.company.ExportLocationDTO;

public class TestLocationCT_Json extends TestEntityCT_Json {
	
	public static Integer LOCATIONID = 123;
	public static Integer ADDRESSID = 234;
	public static Boolean ACTIVE = false;
	public static String LOCATION = "Location 123";
	
	@Test
	public void compareFull() {
		ExportLocationDTO exportDto = new ExportLocationDTO(LOCATIONID, ADDRESSID, ACTIVE, LOCATION);
		
		LocationCT eventDto = new LocationCT();
		eventDto.setActive(ACTIVE);
		eventDto.setAddressid(ADDRESSID);
		eventDto.setLocation(LOCATION);
		eventDto.setLocationid(LOCATIONID);
		
		super.compareJson(exportDto, eventDto);
	}

}
