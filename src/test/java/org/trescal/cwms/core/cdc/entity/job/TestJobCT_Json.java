package org.trescal.cwms.core.cdc.entity.job;

import java.time.LocalDate;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.rest.export.dto.job.ExportJobDTO;

public class TestJobCT_Json extends TestEntityCT_Json {

	public static Integer JOBID = 123;
	public static String CLIENTREF = "CR 123";
	public static String JOBNO = "JN 123";
	public static LocalDate REGDATE = LocalDate.of(2021, 03, 15);
	public static LocalDate DATECOMPLETE = LocalDate.of(2021, 03, 25);
	public static Integer PERSONID = 234;
	public static Integer STATUSID = 345;
	public static Integer RETURNTO = 456;
	public static JobType JOBTYPE = JobType.STANDARD;
	public static Integer ORGID = 567;
	
	@Test
	public void compareFull() {
		ExportJobDTO exportDto = new ExportJobDTO(JOBID, CLIENTREF, JOBNO, REGDATE, DATECOMPLETE,
				PERSONID, STATUSID, RETURNTO, JOBTYPE, ORGID);
		
		JobCT eventDto = new JobCT(); 
		eventDto.setClientref(CLIENTREF);
		//eventDto.setDatecomplete(Date.from(DATECOMPLETE.atStartOfDay(ZoneId.systemDefault()).toInstant()));
		eventDto.setDatecomplete(DATECOMPLETE);
		eventDto.setJobid(JOBID);
		eventDto.setJobno(JOBNO);
		eventDto.setJobType(JOBTYPE);
		eventDto.setOrgid(ORGID);
		eventDto.setPersonid(PERSONID);
		//eventDto.setRegdate(Date.from(REGDATE.atStartOfDay(ZoneId.systemDefault()).toInstant()));
		eventDto.setRegdate(REGDATE);
		eventDto.setReturnto(RETURNTO);
		eventDto.setStatusid(STATUSID);
		
		super.compareJson(exportDto, eventDto);
	}
}
