package org.trescal.cwms.core.cdc.entity.company;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.core.company.entity.companystatus.CompanyStatus;
import org.trescal.cwms.rest.export.dto.company.ExportCompanySettingsDTO;

public class TestCompanySettingsCT_Json extends TestEntityCT_Json {

	public static Integer ID = 123;
	public static Integer ORGID = 456;
	public static Integer COMPANYID = 789;
	public static Boolean ACTIVE = true;
	public static Boolean ONSTOP = true;
	public static CompanyStatus STATUS = CompanyStatus.VERIFIED; 
	
	@Test
	public void compareFull() {
		ExportCompanySettingsDTO exportDto = new ExportCompanySettingsDTO(ID, ORGID, COMPANYID, ACTIVE, ONSTOP, STATUS);
		
		CompanySettingsCT eventDto = new CompanySettingsCT(); 
		eventDto.setActive(ACTIVE);
		eventDto.setCompanyid(COMPANYID);
		eventDto.setId(ID);
		eventDto.setOnstop(ONSTOP);
		eventDto.setOrgid(ORGID);
		eventDto.setStatus(STATUS);
		
		super.compareJson(exportDto, eventDto);
	}

}
