package org.trescal.cwms.core.cdc.entity.referential;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.rest.export.dto.referential.ExportSubFamilyDTO;

public class TestSubFamilyCT_Json extends TestEntityCT_Json {
	
	public static Integer DESCRIPTIONID = 123;
	public static String DESCRIPTION = "Subfamily 123";
	public static Integer TMLID = 456;
	public static Integer FAMILYID = 567;
	public static Boolean ACTIVE = true;
	
	@Test
	public void compareFull() {
		ExportSubFamilyDTO exportDto = new ExportSubFamilyDTO(DESCRIPTIONID, DESCRIPTION, TMLID, FAMILYID, ACTIVE); 
		
		SubFamilyCT eventDto = new SubFamilyCT(); 
		eventDto.setActive(ACTIVE);
		eventDto.setDescription(DESCRIPTION);
		eventDto.setDescriptionid(DESCRIPTIONID);
		eventDto.setFamilyid(FAMILYID);
		eventDto.setTmlid(TMLID);
		
		super.compareJson(exportDto, eventDto);
	}
}
