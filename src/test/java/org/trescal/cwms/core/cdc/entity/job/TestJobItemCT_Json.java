package org.trescal.cwms.core.cdc.entity.job;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Date;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.rest.export.dto.job.ExportJobItemDTO;

public class TestJobItemCT_Json extends TestEntityCT_Json {

	public static Integer JOBITEMID = 123;
	public static ZonedDateTime DATEIN = LocalDate.of(2021, 3, 16).atStartOfDay(ZoneId.systemDefault());
	public static LocalDate DUEDATE = LocalDate.of(2021, 3, 17);
	public static Date DATECOMPLETE = Date.from(LocalDate.of(2021, 3, 18).atStartOfDay(ZoneId.systemDefault()).toInstant());
	public static ZonedDateTime CLIENTRECEIPTDATE = OffsetDateTime.of(2021, 3, 19, 12, 34, 56, 789, ZoneOffset.UTC).toZonedDateTime();
	public static Integer ITEMNO = 1;
	public static Integer ADDRID = 234;
	public static Integer PLANTID = 345;
	public static Integer JOBID = 456;
	public static Integer STATEID = 567;
	public static String CLIENTREF = "CR 123";
	public static Integer SERVICETYPEID = 678;
	public static Integer GROUPID = 789;

	@Test
	public void compareFull() {
		ExportJobItemDTO exportDto = new ExportJobItemDTO(
			JOBITEMID, DATEIN, DUEDATE, DATECOMPLETE, CLIENTRECEIPTDATE,
			ITEMNO, ADDRID, PLANTID, JOBID, STATEID,
			CLIENTREF, SERVICETYPEID, GROUPID);

		JobItemCT eventDto = new JobItemCT();
		eventDto.setClientreceiptdate(CLIENTRECEIPTDATE.toOffsetDateTime());
		eventDto.setClientRef(CLIENTREF);
		eventDto.setDatecomplete(DATECOMPLETE);
		eventDto.setDatein(DATEIN);
		eventDto.setDuedate(DUEDATE);
		eventDto.setItemno(ITEMNO);
		eventDto.setAddrid(ADDRID);
		eventDto.setJobid(JOBID);
		eventDto.setJobitemid(JOBITEMID);
		eventDto.setPlantid(PLANTID);
		eventDto.setStateid(STATEID);
		eventDto.setServicetypeid(SERVICETYPEID);
		eventDto.setGroupid(GROUPID);
		
		super.compareJson(exportDto, eventDto);
	}
}
