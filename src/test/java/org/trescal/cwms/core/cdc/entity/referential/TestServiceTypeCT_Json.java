package org.trescal.cwms.core.cdc.entity.referential;

import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.rest.export.dto.referential.ExportServiceTypeDTO;

public class TestServiceTypeCT_Json extends TestEntityCT_Json {
	
	public static Integer SERVICETYPEID = 123;
	public static String LONGNAME = "ST Long";
	public static String SHORTNAME = "ST Short";
	public static Boolean CANBEPERFORMEDBYCLIENT = true;
	public static DomainType DOMAINTYPE = org.trescal.cwms.core.instrumentmodel.DomainType.INSTRUMENTMODEL;
	public static Integer ORDERBY = 1;
	public static Boolean REPAIR = true;
	
	@Test
	public void compareFull() {
		ExportServiceTypeDTO exportDto = new ExportServiceTypeDTO(SERVICETYPEID, LONGNAME, SHORTNAME, CANBEPERFORMEDBYCLIENT, DOMAINTYPE, ORDERBY, REPAIR); 
		
		ServiceTypeCT eventDto = new ServiceTypeCT(); 
		eventDto.setCanbeperformedbyclient(CANBEPERFORMEDBYCLIENT);
		eventDto.setDomaintype(DOMAINTYPE);
		eventDto.setLongname(LONGNAME);
		eventDto.setOrderby(ORDERBY);
		eventDto.setServicetypeid(SERVICETYPEID);
		eventDto.setShortname(SHORTNAME);
		eventDto.setRepair(REPAIR);
		
		super.compareJson(exportDto, eventDto);
	}
}
