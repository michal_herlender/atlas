package org.trescal.cwms.core.cdc.entity.company;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.rest.export.dto.company.ExportDepartmentMemberDTO;

public class TestDepartmentMemberCT_Json extends TestEntityCT_Json {

	public static Integer ID = 123;
	public static Integer PERSONID = 456;
	public static Integer DEPTID = 789;
	
	@Test
	public void compareFull() {
		ExportDepartmentMemberDTO exportDto = new ExportDepartmentMemberDTO(ID, PERSONID, DEPTID);
		
		DepartmentMemberCT eventDto = new DepartmentMemberCT();
		eventDto.setDeptid(DEPTID);
		eventDto.setId(ID);
		eventDto.setPersonid(PERSONID);
		
		super.compareJson(exportDto, eventDto);
	}
	
}
