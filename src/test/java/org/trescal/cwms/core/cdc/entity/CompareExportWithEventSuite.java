package org.trescal.cwms.core.cdc.entity;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.trescal.cwms.core.cdc.entity.certificate.TestCertLinkCT_Json;
import org.trescal.cwms.core.cdc.entity.certificate.TestCertificateCT_Json;
import org.trescal.cwms.core.cdc.entity.certificate.TestInstCertLinkCT_JSON;
import org.trescal.cwms.core.cdc.entity.company.TestAddressCT_Json;
import org.trescal.cwms.core.cdc.entity.company.TestCompanyCT_Json;
import org.trescal.cwms.core.cdc.entity.company.TestCompanySettingsCT_Json;
import org.trescal.cwms.core.cdc.entity.company.TestContactCT_Json;
import org.trescal.cwms.core.cdc.entity.company.TestDepartmentCT_Json;
import org.trescal.cwms.core.cdc.entity.company.TestDepartmentMemberCT_Json;
import org.trescal.cwms.core.cdc.entity.company.TestLocationCT_Json;
import org.trescal.cwms.core.cdc.entity.company.TestSubdivCT_Json;
import org.trescal.cwms.core.cdc.entity.company.TestSubdivSettingsCT_Json;
import org.trescal.cwms.core.cdc.entity.instruction.TestBaseInstructionCT_Json;
import org.trescal.cwms.core.cdc.entity.instruction.TestCompanyInstructionLinkCT_Json;
import org.trescal.cwms.core.cdc.entity.instruction.TestContactInstructionLinkCT_Json;
import org.trescal.cwms.core.cdc.entity.instruction.TestJobInstructionLinkCT_Json;
import org.trescal.cwms.core.cdc.entity.instruction.TestSubdivInstructionLinkCT_Json;
import org.trescal.cwms.core.cdc.entity.instrument.TestCalReqCT_Json;
import org.trescal.cwms.core.cdc.entity.instrument.TestInstrumentCT_Json;
import org.trescal.cwms.core.cdc.entity.instrument.TestInstrumentRangeCT_Json;
import org.trescal.cwms.core.cdc.entity.instrument.TestInstrumentUsageTypeCT_Json;
import org.trescal.cwms.core.cdc.entity.invoice.TestCreditNoteCT_Json;
import org.trescal.cwms.core.cdc.entity.invoice.TestInvoiceCT_Json;
import org.trescal.cwms.core.cdc.entity.job.TestEngineerAllocationCT_Json;
import org.trescal.cwms.core.cdc.entity.job.TestItemStateCT_Json;
import org.trescal.cwms.core.cdc.entity.job.TestJobCT_Json;
import org.trescal.cwms.core.cdc.entity.job.TestJobItemCT_Json;
import org.trescal.cwms.core.cdc.entity.job.TestJobItemGroupCT_Json;
import org.trescal.cwms.core.cdc.entity.job.TestOnBehalfOfItemCT_Json;
import org.trescal.cwms.core.cdc.entity.job.TestUpcomingWorkCT_Json;
import org.trescal.cwms.core.cdc.entity.job.TestUpcomingWorkJobLinkCT_Json;
import org.trescal.cwms.core.cdc.entity.referential.TestBrandCT_Json;
import org.trescal.cwms.core.cdc.entity.referential.TestCountryCT_Json;
import org.trescal.cwms.core.cdc.entity.referential.TestInstrumentModelCT_Json;
import org.trescal.cwms.core.cdc.entity.referential.TestInstrumentModelTranslationCT_Json;
import org.trescal.cwms.core.cdc.entity.referential.TestServiceTypeCT_Json;
import org.trescal.cwms.core.cdc.entity.referential.TestSubFamilyCT_Json;
import org.trescal.cwms.core.cdc.entity.referential.TestUomCT_JSON;
import org.trescal.cwms.core.cdc.entity.systemdefault.TestSystemDefaultApplicationCT_Json;
import org.trescal.cwms.core.cdc.entity.systemdefault.TestSystemDefaultCT_Json;
import org.trescal.cwms.core.cdc.entity.workinstruction.TestInstrumentWorkInstructionCT_Json;
import org.trescal.cwms.core.cdc.entity.workinstruction.TestWorkInstructionCT_Json;

/**
 * Contains all concrete Test*CT_Json test classes 
 * These test classes compare ExportDTO with their corresponding event change table (CT) class 
 * 
 * This doesn't need a Spring test harness to run, so it's quite fast to run 
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
	// Group 1 - Referential
	TestBrandCT_Json.class,
	TestCountryCT_Json.class,
	TestInstrumentModelCT_Json.class,
	TestInstrumentModelTranslationCT_Json.class,
	TestServiceTypeCT_Json.class,
	TestSubFamilyCT_Json.class,
	TestUomCT_JSON.class,
	
	// Group 2 - Company
	TestAddressCT_Json.class,
	TestCompanyCT_Json.class,
	TestCompanySettingsCT_Json.class,
	TestContactCT_Json.class,
	TestDepartmentCT_Json.class,
	TestDepartmentMemberCT_Json.class,
	TestLocationCT_Json.class,
	TestSubdivCT_Json.class,
	TestSubdivSettingsCT_Json.class,
	
	// Group 3 - Instrument
	TestCalReqCT_Json.class,
	TestInstrumentCT_Json.class,
	TestInstrumentRangeCT_Json.class,
	TestInstrumentUsageTypeCT_Json.class,
	
	// Group 4 - Job
	TestEngineerAllocationCT_Json.class,
	TestItemStateCT_Json.class,
	TestJobCT_Json.class,
	TestJobItemCT_Json.class,
	TestJobItemGroupCT_Json.class,
	TestUpcomingWorkCT_Json.class,
	TestUpcomingWorkJobLinkCT_Json.class,
	TestOnBehalfOfItemCT_Json.class,
	
	// Group 5 - Instruction
	TestBaseInstructionCT_Json.class,
	TestCompanyInstructionLinkCT_Json.class,
	TestContactInstructionLinkCT_Json.class,
	TestJobInstructionLinkCT_Json.class,
	TestSubdivInstructionLinkCT_Json.class,
	
	// Group 6 - System Default
	TestSystemDefaultApplicationCT_Json.class,
	TestSystemDefaultCT_Json.class,
	
	// Group 7 - Work Instruction
	TestInstrumentWorkInstructionCT_Json.class,
	TestWorkInstructionCT_Json.class,
	
	// Group 8 - Certificate
	TestCertificateCT_Json.class,
	TestInstCertLinkCT_JSON.class,
	TestCertLinkCT_Json.class,
	
	// Group 9 - Invoice
	TestInvoiceCT_Json.class,
	TestCreditNoteCT_Json.class
})
public class CompareExportWithEventSuite {
}
