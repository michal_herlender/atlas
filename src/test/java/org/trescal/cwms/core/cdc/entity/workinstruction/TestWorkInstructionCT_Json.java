package org.trescal.cwms.core.cdc.entity.workinstruction;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.rest.export.dto.workinstruction.ExportWorkInstructionDTO;

public class TestWorkInstructionCT_Json extends TestEntityCT_Json {

	public static Integer ID = 123;
	public static String NAME = "Name 123";
	public static String TITLE = "Title 123";
	public static Integer DESCID = 234;
	public static Integer ORGID = 345;	
	
	@Test
	public void compareFull() {
		ExportWorkInstructionDTO exportDto = new ExportWorkInstructionDTO(ID, NAME, TITLE, DESCID, ORGID); 
		
		WorkInstructionCT eventDto = new WorkInstructionCT(); 
		eventDto.setDescid(DESCID);
		eventDto.setId(ID);
		eventDto.setName(NAME);
		eventDto.setOrgid(ORGID);
		eventDto.setTitle(TITLE);
		
		super.compareJson(exportDto, eventDto);
	}
	
}
