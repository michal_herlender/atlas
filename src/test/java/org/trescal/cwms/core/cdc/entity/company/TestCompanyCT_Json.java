package org.trescal.cwms.core.cdc.entity.company;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.rest.export.dto.company.ExportCompanyDTO;

public class TestCompanyCT_Json extends TestEntityCT_Json {

	public static Integer COID = 123;
	public static String CONAME = "Test Company";
	public static CompanyRole COMPANY_ROLE = CompanyRole.BUSINESS;
	public static Integer COUNTRY_ID = 234;
	public static String COMPANY_CODE = "345";
	public static String LEGAL_IDENTIFIER = "LI-123";
	public static String FISCAL_IDENTIFIER = "FI-123";
	
	@Test
	public void compareFull() {
		ExportCompanyDTO exportDto = new ExportCompanyDTO(COID, CONAME, COMPANY_ROLE, COUNTRY_ID, COMPANY_CODE, LEGAL_IDENTIFIER, FISCAL_IDENTIFIER);

		CompanyCT eventDto = new CompanyCT();
		eventDto.setCoid(COID);
		eventDto.setCompanyCode(COMPANY_CODE);
		eventDto.setCompanyRole(COMPANY_ROLE);
		eventDto.setConame(CONAME);
		eventDto.setCountryid(COUNTRY_ID);
		eventDto.setFiscalIdentifier(FISCAL_IDENTIFIER);
		eventDto.setLegalIdentifier(LEGAL_IDENTIFIER);
		
		super.compareJson(exportDto, eventDto);
	}

}
