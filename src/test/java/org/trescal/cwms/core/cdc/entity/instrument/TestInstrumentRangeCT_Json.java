/**
 * 
 */
package org.trescal.cwms.core.cdc.entity.instrument;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.rest.export.dto.instrument.ExportInstrumentRangeDTO;

/**
 * @Author shazil.khan
 *
 */
public class TestInstrumentRangeCT_Json extends TestEntityCT_Json {
	
	private static Integer ID = 123;
	private static Double END = 123.20;
	private static Double START = 123.2 ;
	private static Integer UOM = 123;  
	private static Integer MAXUOM = 123;
	private static Integer PLANTID = 123;


	@Test
	public void compareFull() {
	ExportInstrumentRangeDTO exportDto = new ExportInstrumentRangeDTO(ID, END, START, UOM, MAXUOM, PLANTID);
	
	InstrumentRangeCT eventDto = new InstrumentRangeCT();
	
	eventDto.setId(ID);
	eventDto.setEnd(END);
	eventDto.setStart(START);
	eventDto.setUom(UOM);
	eventDto.setMaxUom(MAXUOM);
	eventDto.setPlantid(PLANTID);
	
	super.compareJson(exportDto, eventDto);
	}
	
}
