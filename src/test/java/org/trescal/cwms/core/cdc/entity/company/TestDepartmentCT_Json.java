package org.trescal.cwms.core.cdc.entity.company;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.rest.export.dto.company.ExportDepartmentDTO;

public class TestDepartmentCT_Json extends TestEntityCT_Json {
	public static Integer DEPTID = 123;
	public static String NAME = "Dept 234";
	public static Integer ADDRESSID = 345;
	public static Integer SUBDIVID = 456;
	public static DepartmentType TYPE = DepartmentType.LABORATORY;
	public static String SHORTNAME ="234";
	
	@Test
	public void compareFull() {
		ExportDepartmentDTO exportDto = new ExportDepartmentDTO(DEPTID, NAME, ADDRESSID, SUBDIVID, TYPE, SHORTNAME);
		
		DepartmentCT eventDto = new DepartmentCT();
		eventDto.setAddressid(ADDRESSID);
		eventDto.setDeptid(DEPTID);
		eventDto.setName(NAME);
		eventDto.setShortName(SHORTNAME);
		eventDto.setSubdivid(SUBDIVID);
		eventDto.setType(TYPE);
		
		super.compareJson(exportDto, eventDto);
	}

}
