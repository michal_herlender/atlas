package org.trescal.cwms.core.cdc.entity.job;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.rest.export.dto.job.ExportOnBehalfItemDTO;

public class TestOnBehalfOfItemCT_Json extends TestEntityCT_Json {

	public Integer ON_BEHALF_ITEM_ID = 1;
	public Integer ON_BEHALF_ITEM_ADDRESS_ID = 1;
	public Integer ON_BEHALF_ITEM_JOBITEM_ID = 1;
	public Integer ON_BEHALF_ITEM_COMPANY_ID = 1;

	@Test
	public void compareFull() {

		ExportOnBehalfItemDTO exportDto = new ExportOnBehalfItemDTO(ON_BEHALF_ITEM_ID, ON_BEHALF_ITEM_ADDRESS_ID,
				ON_BEHALF_ITEM_COMPANY_ID, ON_BEHALF_ITEM_JOBITEM_ID);
		
		OnBehalfItemCT eventDto = new OnBehalfItemCT();
		eventDto.setId(ON_BEHALF_ITEM_ID);
		eventDto.setCoid(ON_BEHALF_ITEM_COMPANY_ID);
		eventDto.setJobitemid(ON_BEHALF_ITEM_JOBITEM_ID);
		eventDto.setAddressid(ON_BEHALF_ITEM_ADDRESS_ID);
		
		super.compareJson(exportDto, eventDto);
	}

}
