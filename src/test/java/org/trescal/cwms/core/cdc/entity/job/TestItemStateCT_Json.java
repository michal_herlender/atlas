package org.trescal.cwms.core.cdc.entity.job;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.rest.export.dto.job.ExportItemStateDTO;

public class TestItemStateCT_Json extends TestEntityCT_Json {

	public static String TYPE = "status";
	public static Integer STATEID = 123;
	public static Boolean ACTIVE = true;
	public static String DESCRIPTION = "Desc 123";
	public static Boolean RETIRED = true;
	
	@Test
	public void compareFull() {
		ExportItemStateDTO exportDto = new ExportItemStateDTO();
		
		exportDto.setDescription(DESCRIPTION);
		exportDto.setRetired(RETIRED);
		exportDto.setStateid(STATEID);
		exportDto.setType(TYPE);
		exportDto.setActive(ACTIVE);
		
		ItemStateCT eventDto = new ItemStateCT();
		eventDto.setActive(ACTIVE);
		eventDto.setDescription(DESCRIPTION);
		eventDto.setRetired(RETIRED);
		eventDto.setStateid(STATEID);
		eventDto.setType(TYPE);
		
		super.compareJson(exportDto, eventDto);
	}
}
