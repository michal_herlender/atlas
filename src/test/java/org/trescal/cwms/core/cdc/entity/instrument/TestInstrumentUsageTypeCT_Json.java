package org.trescal.cwms.core.cdc.entity.instrument;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;
import org.trescal.cwms.rest.export.dto.instrument.ExportInstrumentUsageTypeDTO;

public class TestInstrumentUsageTypeCT_Json extends TestEntityCT_Json {

	public Integer TYPE_ID_1 = 1;
	public String FULL_DESC = "test_1";
	public boolean DEFAULT_TYPE = false;
	public String NAME = "name_test_1";
	public boolean recall = false;
	public InstrumentStatus instrumentStatus = InstrumentStatus.BER;

	@Test
	public void compareFull() {

		ExportInstrumentUsageTypeDTO exportDto = new ExportInstrumentUsageTypeDTO(TYPE_ID_1, DEFAULT_TYPE, FULL_DESC,
				NAME, recall, instrumentStatus);
		
		InstrumentUsageTypeCT eventDto = new InstrumentUsageTypeCT();
		eventDto.setTypeid(TYPE_ID_1);
		eventDto.setDefaultType(DEFAULT_TYPE);
		eventDto.setName(NAME);
		eventDto.setRecall(recall);
		eventDto.setInstrumentStatus(instrumentStatus);
		eventDto.setFullDescription(FULL_DESC);

		super.compareJson(exportDto, eventDto);
	}

}
