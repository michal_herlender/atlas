/**
 * 
 */
package org.trescal.cwms.core.cdc.entity.certificate;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateType;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.rest.export.dto.certificate.ExportCertificateDTO;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @Author shazil.khan
 *
 */
public class TestCertificateCT_Json extends TestEntityCT_Json{
	
	private static Integer CERTID = 123;
	@JsonFormat(pattern="yyyy-MM-dd")
	private static Date CALDATE = Date.from(LocalDate.of(2021, 03, 18).atStartOfDay(ZoneId.systemDefault()).toInstant());
	@JsonFormat(pattern="yyyy-MM-dd")
	private static Date CERTDATE = Date.from(LocalDate.of(2021, 03, 18).atStartOfDay(ZoneId.systemDefault()).toInstant());
	private static String CERTNO = "12345";
	private static Integer DURATION = 123;
	private static String THIRDCERTNO = "12345";
	private static CertificateType TYPE = CertificateType.CERT_CLIENT;
	private static Integer SERVICETYPEID = 123;
	private static Integer SUPPLEMENTARYFORID = 123;
	private static Integer THIRDDIV = 123;
	private static IntervalUnit INTERVALUNIT = IntervalUnit.DAY ;
	private static CalibrationVerificationStatus CALIBRATIONVERIFICATIONSTATUS;
	private static Boolean OPTIMIZATION = true ;
	private static Boolean REPAIR = true;
	private static Boolean RESTRICTION = true;
	private static Boolean ADJUSTMENT = true;
	private static CertStatusEnum CERTSTATUS;

	@Test
	public void compareFull() {
		ExportCertificateDTO exportDto = new ExportCertificateDTO(CERTID, CALDATE, CERTDATE, CERTNO, DURATION, THIRDCERTNO, TYPE,
				SERVICETYPEID, SUPPLEMENTARYFORID, THIRDDIV, INTERVALUNIT, CALIBRATIONVERIFICATIONSTATUS, OPTIMIZATION, 
				REPAIR, RESTRICTION, ADJUSTMENT, CERTSTATUS);
		
		CertificateCT eventDto = new CertificateCT();
		eventDto.setCertid(CERTID);
		eventDto.setCaldate(CALDATE);
		eventDto.setCertdate(CERTDATE);
		eventDto.setCertno(CERTNO);
		eventDto.setDuration(DURATION);
		eventDto.setThirdcertno(THIRDCERTNO);
		eventDto.setType(TYPE);
		eventDto.setServicetypeid(SERVICETYPEID);
		eventDto.setSupplementaryforid(SUPPLEMENTARYFORID);
		eventDto.setThirddiv(THIRDDIV);
		eventDto.setIntervalunit(INTERVALUNIT);
		eventDto.setCalibrationverificationstatus(CALIBRATIONVERIFICATIONSTATUS);
		eventDto.setOptimization(OPTIMIZATION);
		eventDto.setRepair(REPAIR);
		eventDto.setRestriction(RESTRICTION);
		eventDto.setAdjustment(ADJUSTMENT);
		eventDto.setCertstatus(CERTSTATUS);
		
		compareJson(exportDto, eventDto);
		
		
		
		
		
	}
	
}
