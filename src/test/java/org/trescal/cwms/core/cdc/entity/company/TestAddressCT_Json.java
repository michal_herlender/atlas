package org.trescal.cwms.core.cdc.entity.company;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.rest.export.dto.company.ExportAddressDTO;

public class TestAddressCT_Json extends TestEntityCT_Json {
	
	public static int ADDRESS_ID = 123;
	public static boolean ACTIVE = true;
	public static String ADDR1 = "Line 1";
	public static String ADDR2 = "Line 2";
	public static String ADDR3 = "Line 3";
	public static String COUNTY = "OH";
	public static String POSTCODE = "12345";
	public static String TOWN = "Cleveland";
	public static int COUNTRY_ID = 234;
	public static int SUBDIV_ID = 345;
	
	@Test
	public void compareFull() {
		ExportAddressDTO exportDto = new ExportAddressDTO(ADDRESS_ID, ACTIVE, ADDR1, ADDR2, ADDR3, COUNTY, POSTCODE, TOWN, COUNTRY_ID, SUBDIV_ID);

		AddressCT eventDto = new AddressCT();
		eventDto.setActive(ACTIVE);
		eventDto.setAddr1(ADDR1);
		eventDto.setAddr2(ADDR2);
		eventDto.setAddr3(ADDR3);
		eventDto.setAddressid(ADDRESS_ID);
		eventDto.setCounty(COUNTY);
		eventDto.setPostcode(POSTCODE);
		eventDto.setTown(TOWN);
		eventDto.setCountryid(COUNTRY_ID);
		eventDto.setSubdivid(SUBDIV_ID);
		
		super.compareJson(exportDto, eventDto);
	}
		
}
