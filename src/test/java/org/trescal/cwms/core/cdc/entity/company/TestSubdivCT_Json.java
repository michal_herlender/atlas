package org.trescal.cwms.core.cdc.entity.company;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.rest.export.dto.company.ExportSubdivDTO;

public class TestSubdivCT_Json extends TestEntityCT_Json {

	public static Integer SUBDIVID = 234;
	public static Integer COID = 123;
	public static Boolean ACTIVE = true;
	public static String SUBNAME = "Subdiv 234";
	public static String SUBDIVCODE = "SUB";
	
	@Test
	public void compareFull() {
		ExportSubdivDTO exportDto = new ExportSubdivDTO(SUBDIVID, COID, ACTIVE, SUBNAME, SUBDIVCODE);
		
		SubdivCT eventDto = new SubdivCT(); 
		eventDto.setActive(ACTIVE);
		eventDto.setCoid(COID);
		eventDto.setSubdivcode(SUBDIVCODE);
		eventDto.setSubdivid(SUBDIVID);
		eventDto.setSubname(SUBNAME);
		
		super.compareJson(exportDto, eventDto);
	}
	
}
