package org.trescal.cwms.core.cdc;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.Assert;
import org.junit.experimental.categories.Category;
import org.trescal.cwms.core.AtlasUnitTest;
import org.trescal.cwms.core.cdc.entity.EntityCT;

/**
 * Abstract class intended to compare the export DTOs and event DTOs 
 * 
 * Export DTO : Jackson serialized data (via Spring content conversion)
 * Event DTO : Gson implementation in EntityCT - which we expect to be the same
 * 
 * Note that currently the event DTO excludes null values, the export DTO doesn't
 *  (maybe OK - to discuss) so we test with fully populated records only
 */
@Category(AtlasUnitTest.class)
public abstract class TestEntityCT_Json {
	
	private ObjectMapper mapper;
	
	public TestEntityCT_Json() {
		mapper = new ObjectMapper();
		// Required for LocalDate serialization
		mapper.registerModule(new JavaTimeModule());
	}
	
	public void compareJson(Object exportDto, EntityCT eventDto) {
		try {
			String eventJson = mapper.writeValueAsString(eventDto);
			System.out.println(eventJson);
			
			String exportJson = mapper.writeValueAsString(exportDto);
			System.out.println(exportJson);
			
			Assert.assertEquals(mapper.readTree(exportJson), mapper.readTree(eventJson));
			//Assert.assertEquals(exportJson, eventJson);
			
		}
		catch (JsonProcessingException e) {
			Assert.fail(e.getMessage());
		}
	}
}
