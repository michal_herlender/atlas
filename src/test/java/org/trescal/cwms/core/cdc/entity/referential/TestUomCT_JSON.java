/**
 * 
 */
package org.trescal.cwms.core.cdc.entity.referential;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.rest.export.dto.referential.ExportUoMDTO;

/**
 * @Author shazil.khan
 *
 */
public class TestUomCT_JSON extends TestEntityCT_Json{

	private static Integer ID = 123;
	private static String NAME = "Mfr 123" ;
	private static String SYMBOL = "Mfr 123";
	private static Integer TMLID = 234;
	private static Boolean ACTIVE = true;
	
	@Test
	public void compareFull() {
		ExportUoMDTO exportDto =  new ExportUoMDTO(ID, NAME, SYMBOL, TMLID, ACTIVE);
		
		UomCT eventDto = new UomCT();
		eventDto.setId(ID);
		eventDto.setName(NAME);
		eventDto.setSymbol(SYMBOL);
		eventDto.setTmlid(TMLID);
		eventDto.setActive(ACTIVE);
		
		super.compareJson(exportDto, eventDto);
		
		
	}
}
