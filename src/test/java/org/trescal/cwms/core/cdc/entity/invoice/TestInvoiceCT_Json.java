package org.trescal.cwms.core.cdc.entity.invoice;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.rest.export.dto.invoice.ExportInvoiceDTO;

public class TestInvoiceCT_Json extends TestEntityCT_Json {

	public static final Integer ID = 123;
	public static final Boolean ISSUED = true;
	public static final LocalDate ISSUEDATE = LocalDate.of(2021, 11, 29);
	public static final BigDecimal FINALCOST = new BigDecimal("100.12");
	public static final BigDecimal TOTALCOST = new BigDecimal("120.30");
	public static final BigDecimal VATVALUE = new BigDecimal("20.28");
	public static final String INVNO = "IN-234";
	public static final Integer ADDRESSID = 345;
	public static final Integer COID = 456;
	public static final Integer ORGID = 567;
	public static final LocalDate DUEDATE = LocalDate.of(2021, 12, 29);
	
	@Test
	public void compareFull() {
		ExportInvoiceDTO exportDto = new ExportInvoiceDTO(ID, ISSUED, ISSUEDATE, FINALCOST, TOTALCOST, VATVALUE, INVNO, ADDRESSID, COID, ORGID, DUEDATE);
		InvoiceCT eventDto = new InvoiceCT();
		eventDto.setAddressid(ADDRESSID);
		eventDto.setCoid(COID);
		eventDto.setDuedate(DUEDATE);
		eventDto.setFinalcost(FINALCOST);
		eventDto.setId(ID);
		eventDto.setInvno(INVNO);
		eventDto.setIssued(ISSUED);
		eventDto.setIssuedate(ISSUEDATE);
		eventDto.setOrgid(ORGID);
		eventDto.setTotalcost(TOTALCOST);
		eventDto.setVatvalue(VATVALUE);
		
		super.compareJson(exportDto, eventDto);
	}	
}

