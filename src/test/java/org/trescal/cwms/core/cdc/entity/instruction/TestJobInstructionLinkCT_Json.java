package org.trescal.cwms.core.cdc.entity.instruction;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.rest.export.dto.instruction.ExportJobInstructionLinkDTO;

public class TestJobInstructionLinkCT_Json extends TestEntityCT_Json {

	public static Integer ID = 123;
	public static Integer INSTRUCTIONID = 234;
	public static Integer JOBID = 345;
	
	@Test
	public void compareFull() {
		ExportJobInstructionLinkDTO exportDto = new ExportJobInstructionLinkDTO(ID, INSTRUCTIONID, JOBID); 
		
		// TODO set ID and instruction ID on JobInstructionLinkCT
		JobInstructionLinkCT eventDto = new JobInstructionLinkCT();
		eventDto.setId(ID);
		eventDto.setInstructionid(INSTRUCTIONID);
		eventDto.setJobid(JOBID);
		
		super.compareJson(exportDto, eventDto);
	}
}
