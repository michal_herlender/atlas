package org.trescal.cwms.core.cdc.schedule;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.DatatypeConverter;

import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.cdc.entity.EntityCT;
import org.trescal.cwms.core.cdc.entity.ProcessedChangeEntity;
import org.trescal.cwms.core.cdc.repository.base.BaseCDCService;
import org.trescal.cwms.core.cdc.repository.processedchangeentity.ProcessedChangeEntityService;
import org.trescal.cwms.core.events.change.enums.ChangeEntity;
import org.trescal.cwms.core.events.change.queue.ChangeQueue;
import org.trescal.cwms.core.events.service.ChangeQueueService;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetCapability;
import org.trescal.cwms.tests.generic.DataSetCertificate;
import org.trescal.cwms.tests.generic.DataSetCompany;
import org.trescal.cwms.tests.generic.DataSetInstruction;
import org.trescal.cwms.tests.generic.DataSetJob;
import org.trescal.cwms.tests.generic.DataSetPricing;
import org.trescal.cwms.tests.generic.DataSetSystem;
import org.trescal.cwms.tests.generic.address.TestDataAddress;
import org.trescal.cwms.tests.generic.capability.TestDataInstrumentWorkInstruction;
import org.trescal.cwms.tests.generic.capability.TestDataWorkInstruction;
import org.trescal.cwms.tests.generic.certificate.TestDataCertificate;
import org.trescal.cwms.tests.generic.company.TestDataCompany;
import org.trescal.cwms.tests.generic.company.TestDataCompanySettings;
import org.trescal.cwms.tests.generic.company.TestDataContact;
import org.trescal.cwms.tests.generic.company.TestDataDepartment;
import org.trescal.cwms.tests.generic.company.TestDataDepartmentMember;
import org.trescal.cwms.tests.generic.company.TestDataLocation;
import org.trescal.cwms.tests.generic.company.TestDataSubdiv;
import org.trescal.cwms.tests.generic.company.TestDataSubdivSettings;
import org.trescal.cwms.tests.generic.core.TestDataItemState;
import org.trescal.cwms.tests.generic.core.TestDataServiceType;
import org.trescal.cwms.tests.generic.core.TestDataSupportedCountry;
import org.trescal.cwms.tests.generic.core.TestDataSystemDefault;
import org.trescal.cwms.tests.generic.core.TestDataUnitsOfMeasure;
import org.trescal.cwms.tests.generic.instruction.TestDataInstruction;
import org.trescal.cwms.tests.generic.instrument.TestDataCalReq;
import org.trescal.cwms.tests.generic.instrument.TestDataDescription;
import org.trescal.cwms.tests.generic.instrument.TestDataInstrument;
import org.trescal.cwms.tests.generic.instrument.TestDataInstrumentModel;
import org.trescal.cwms.tests.generic.instrument.TestDataInstrumentModelTranslation;
import org.trescal.cwms.tests.generic.instrument.TestDataInstrumentRange;
import org.trescal.cwms.tests.generic.instrument.TestDataInstrumentUsageType;
import org.trescal.cwms.tests.generic.instrument.TestDataMfr;
import org.trescal.cwms.tests.generic.job.TestDataEngineerAllocation;
import org.trescal.cwms.tests.generic.job.TestDataJob;
import org.trescal.cwms.tests.generic.job.TestDataJobItem;
import org.trescal.cwms.tests.generic.job.TestDataJobItemGroup;
import org.trescal.cwms.tests.generic.job.TestDataOnBehalfItem;
import org.trescal.cwms.tests.generic.job.TestDataUpcomingWork;
import org.trescal.cwms.tests.generic.job.TestDataUpcomingWorkJobLink;
import org.trescal.cwms.tests.generic.pricing.TestDataCreditNote;
import org.trescal.cwms.tests.generic.pricing.TestDataInvoice;

import lombok.extern.slf4j.Slf4j;

/**
 * Setup for this test requires:
 * 
 * (a) Existing cwms_test and cwms_testfiles databases 
 *     - (run dbscripts/cwms_test-create.sql and cwms_testfiles-create.sql)
 *  
 * (b) Configured and running CDC (change data capture) against cwms_test
 *     - run against the cwms_test database: 
 *        src/main/resources/scripts/spain/utilities/ 
 *            U029 - Configure all CDC tables.sql
 *            (U031 is no longer required - no stored procedure usage)
 *
 * Note, this test should work well against both a "newly configured" CDC setup
 * or against an existing CDC setup.
 *
 */
@Slf4j
public class TestCdcDataTransformationScheduledJob extends MvcRestTestClass {

	@Autowired
	private BaseCDCService<EntityCT> entityCTService;
	@Autowired
	private CdcDataTransformationScheduledJob scheduledJob;
	@Autowired
	private ChangeQueueService changeQueueService;
	@Autowired
	private ProcessedChangeEntityService pceService;
	
	public TestCdcDataTransformationScheduledJob() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Initializes a Map of expected counts
	 * If new ChangeEntity enum values are defined they should be added / definied here
	 * with their expected count based on test data
	 * (enter 0 if not sure... upon needing to add a new ChangeEntity value)
	 */
	private Map<ChangeEntity, Integer> initExpectedCounts() {
		Map<ChangeEntity, Integer> expectedCounts = new HashMap<>();
		for (ChangeEntity entityType : ChangeEntity.values()) {
			expectedCounts.put(entityType, getExpectedCount(entityType));
		}
		return expectedCounts;
	}
	
	private Integer getExpectedCount(ChangeEntity entityType) {
		if (entityType == null)
			throw new IllegalArgumentException("entityType was null!");
		switch (entityType) {
			// GROUP 1 - REFERENTIAL DATA 
			case BRAND:
				return TestDataMfr.RECORD_COUNT;
			case COUNTRY:
				return TestDataSupportedCountry.RECORD_COUNT;
			case SERVICE_TYPE:
				return TestDataServiceType.RECORD_COUNT;
			case SUB_FAMILY:
				return TestDataDescription.RECORD_COUNT;
			case INSTRUMENT_MODEL:
				return TestDataInstrumentModel.RECORD_COUNT;
			case INSTRUMENT_MODEL_TRANSLATION:
				return TestDataInstrumentModelTranslation.RECORD_COUNT;
			case UOM:
				return TestDataUnitsOfMeasure.RECORD_COUNT;
				
			// GROUP 2 - COMPANY DATA
			case COMPANY:
				return TestDataCompany.RECORD_COUNT_TOTAL;
			case COMPANY_SETTINGS:
				return TestDataCompanySettings.RECORD_COUNT_TOTAL;
			case SUBDIV:
				return TestDataSubdiv.RECORD_COUNT_TOTAL;
			case SUBDIV_SETTINGS:
				return TestDataSubdivSettings.RECORD_COUNT_CLIENT;
			case ADDRESS:
				return TestDataAddress.RECORD_COUNT_TOTAL;
			case LOCATION:
				return TestDataLocation.RECORD_COUNT_TOTAL;
			case CONTACT:
				return TestDataContact.RECORD_COUNT_TOTAL;
			case DEPARTMENT:
				return TestDataDepartment.RECORD_COUNT_BUSINESS;
			case DEPARTMENT_MEMBER:
				return TestDataDepartmentMember.RECORD_COUNT_BUSINESS;
				
			// GROUP 3 - INSTRUMENT DATA
			case INSTRUMENT_USAGE_TYPE:
				return TestDataInstrumentUsageType.RECORD_COUNT_INSTRUMENT;
			case INSTRUMENT:
				return TestDataInstrument.RECORD_COUNT;
			case INSTRUMENT_RANGE:
				return TestDataInstrumentRange.RECORD_COUNT;
				
			// GROUP 4 - JOB DATA
			case ITEM_STATE:
				return TestDataItemState.RECORD_COUNT;
			case JOB:
				return TestDataJob.RECORD_COUNT_COMPLETED;
			case JOB_ITEM:
				return TestDataJobItem.RECORD_COUNT_COMPLETED;
			case JOB_ITEM_GROUP:
				return TestDataJobItemGroup.RECORD_COUNT_COMPLETED;
			case ENGINEER_ALLOCATION:
				return TestDataEngineerAllocation.RECORD_COUNT_COMPLETED;
			case CAL_REQ:
				return TestDataCalReq.RECORD_COUNT;
			case UPCOMING_WORK:
				return TestDataUpcomingWork.RECORD_COUNT;
			case UPCOMING_WORK_JOB_LINK:
				return TestDataUpcomingWorkJobLink.RECORD_COUNT;
			case ON_BEHALF_OF_ITEM:
				return TestDataOnBehalfItem.RECORD_COUNT_COMPLETED;
				
			//	GROUP 5 -INSTRUCTION DATA
			case BASE_INSTRUCTION:
				return TestDataInstruction.RECORD_COUNT_BASE_INSTRUCTION;
			case COMPANY_INSTRUCTION_LINK:
				return TestDataInstruction.RECORD_COUNT_COMPANY_INSTRUCTION;
			case SUBDIV_INSTRUCTION_LINK:
				return TestDataInstruction.RECORD_COUNT_SUBDIV_INSTRUCTION;
			case CONTACT_INSTRUCTION_LINK:
				return TestDataInstruction.RECORD_COUNT_CONTACT_INSTRUCTION;
			case JOB_INSTRUCTION_LINK:
				return TestDataInstruction.RECORD_COUNT_JOB_DELIVERY_INSTRUCTION;
				
			// GROUP 6 - SYSTEM DEFAULT DATA
			case SYSTEM_DEFAULT:
				return TestDataSystemDefault.RECORD_COUNT_SYSTEM_DEFAULT;
			case SYSTEM_DEFAULT_APPLICATION:
				return TestDataSystemDefault.RECORD_COUNT_SYSTEM_DEFAULT_APPLICATION;
				
			// GROUP 7 - WORK INSTRUCTION DATA
			case WORK_INSTRUCTION:
				return TestDataWorkInstruction.RECORD_COUNT;
			case INSTRUMENT_WORK_INSTRUCTION:
				return TestDataInstrumentWorkInstruction.RECORD_COUNT;
				
			// Group 8 - Certificate data	
			case CERTIFICATE:
				return TestDataCertificate.RECORD_COUNT_CERTIFICATE;
			case INST_CERT_LINK:
				return TestDataCertificate.RECORD_COUNT_INST_CERT_LINK;
			case CERT_LINK:
				return TestDataCertificate.RECORD_COUNT_CERT_LINK;

			// Group 9 - Invoice data	
			case INVOICE:
				return TestDataInvoice.RECORD_COUNT_STANDALONE;
			case CREDIT_NOTE:
				return TestDataCreditNote.RECORD_COUNT_STANDALONE;
				
			default:
				throw new UnsupportedOperationException(entityType.name()+" needs an expected count");
		}
	}
	
	@Override
	protected void afterResetBeforeLoad() throws Exception {
		// Wait > 5 seconds for CDC job to process reset changes
		log.info("Waiting 10000 ms for CDC processing of reset");
		Thread.sleep(10000);
		log.info("Finished waiting");
		populateProcessedChangeEntity();
		
	}
	
	protected void populateProcessedChangeEntity() throws Exception {
		// set the CDC for <processedchangequeue> based on the highest current existing value for the system
		// this method called after reset, before data load
		byte[] maxLsn = entityCTService.findMaxLsn();
		log.info("maxLsn was "+DatatypeConverter.printHexBinary(maxLsn));
		for (ChangeEntity changeEntity : ChangeEntity.values()) {
			ProcessedChangeEntity pce = new ProcessedChangeEntity();
			pce.setChangeEntity(changeEntity);
			pce.setLastProcessedLsn(maxLsn);
			this.pceService.save(pce);
		}
	}
	
	@Override
	protected List<String> getDataSetFileNames() {
		List<String> result = DataSetJob.getJobClientCompletedTestData();
		result.addAll(DataSetInstruction.getJobClientCompletedTestData());
		result.add(DataSetCapability.FILE_WORK_INSTRUCTION);
		result.add(DataSetCapability.FILE_INSTRUMENT_WORK_INSTRUCTION);
		result.add(DataSetCompany.FILE_SYSTEM_DEFAULT_APPLICATION);
		result.add(DataSetCertificate.FILE_CLIENT_CERTIFICATE);
		result.add(DataSetJob.FILE_CAL_REQ);
		result.add(DataSetJob.FILE_UPCOMING_WORK);
		result.add(DataSetSystem.FILE_SCHEDULED_TASK_CDC_TRANSFORM);
		result.add(DataSetPricing.FILE_INVOICE);
		result.add(DataSetPricing.FILE_CREDIT_NOTE);
		return result;
	}

	@Test
	public void testTransformation() throws Exception {
		// Wait > 5 seconds for CDC job to process loading changes (6 seconds doesn't seem enough)
		log.info("Waiting 10000 ms for CDC processing of new data load");
		Thread.sleep(10000);
		log.info("Finished waiting");
		// Run the synchronization
		log.info("Running synchronization");
		scheduledJob.sync();
		log.info("Analyzing change queue");
		// Verify that we have subscriber_queue entries created for each of the data types created during loading
		analyzeChangeQueue();
		// TODO - verify that ProcessedChangeEntity has truly been updated with new LSNs?
	}
	
	/**
	 * Checks whether the change queue contains the expected number of records from data loading
	 */
	private void analyzeChangeQueue() {
		List<ChangeQueue> queue = this.changeQueueService.getAll();
		log.info("queue.size() : "+queue.size());
		
		// Test is limited to checking that the *correct* subscriber queue elements exist
		// TODO check whether content matches expected data (at least correct ids of records)
		MultiValuedMap<ChangeEntity, ChangeQueue> mapByType = new HashSetValuedHashMap<>();
		queue.stream().forEach(change -> mapByType.put(change.getEntityType(), change));
		
		Map<ChangeEntity, Integer> expectedCounts = initExpectedCounts();
		
		// All supported entity types are checked
		for (ChangeEntity entityType : ChangeEntity.values()) {
			Integer expectedCount = expectedCounts.get(entityType);
			Integer actualCount = mapByType.get(entityType).size();
			Assertions.assertEquals( expectedCount, actualCount,entityType.name());
		}
		
	}
}
