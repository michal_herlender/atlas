package org.trescal.cwms.core.cdc.entity.invoice;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.rest.export.dto.invoice.ExportCreditNoteDTO;

public class TestCreditNoteCT_Json extends TestEntityCT_Json {

	public static final Integer ID = 123;
	public static final Boolean ISSUED = true;
	public static final LocalDate ISSUEDATE = LocalDate.of(2021, 11, 29);
	public static final BigDecimal FINALCOST = new BigDecimal("100.12");
	public static final BigDecimal TOTALCOST = new BigDecimal("120.30");
	public static final BigDecimal VATVALUE = new BigDecimal("20.28");
	public static final String CREDITNOTENO = "CN-123";
	public static final Integer INVID = 234;
	public static final Integer ORGID = 345;
	
	@Test
	public void compareFull() {
		ExportCreditNoteDTO exportDto = new ExportCreditNoteDTO(ID, ISSUED, ISSUEDATE, FINALCOST, TOTALCOST, VATVALUE, CREDITNOTENO, INVID, ORGID);
		
		CreditNoteCT eventDto = new CreditNoteCT();
		eventDto.setCreditnoteno(CREDITNOTENO);
		eventDto.setFinalcost(FINALCOST);
		eventDto.setId(ID);
		eventDto.setInvid(INVID);
		eventDto.setIssued(ISSUED);
		eventDto.setIssuedate(ISSUEDATE);
		eventDto.setOrgid(ORGID);
		eventDto.setTotalcost(TOTALCOST);
		eventDto.setVatvalue(VATVALUE);
		
		super.compareJson(exportDto, eventDto);
	}	
}

