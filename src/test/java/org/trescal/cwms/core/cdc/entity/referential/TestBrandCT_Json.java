package org.trescal.cwms.core.cdc.entity.referential;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.rest.export.dto.referential.ExportBrandDTO;

public class TestBrandCT_Json extends TestEntityCT_Json {
	
	public static Integer MFRID = 123;
	public static Boolean ACTIVE = true;
	public static Boolean GENERICMFR = true;
	public static String NAME = "Mfr 123";
	public static Integer TMLID = 234;
	
	@Test
	public void compareFull() {
		ExportBrandDTO exportDto = new ExportBrandDTO(MFRID, ACTIVE, GENERICMFR, NAME, TMLID); 
		
		BrandCT eventDto = new BrandCT();
		eventDto.setActive(ACTIVE);
		eventDto.setGenericmfr(GENERICMFR);
		eventDto.setMfrid(MFRID);
		eventDto.setName(NAME);
		eventDto.setTmlid(TMLID);
		
		super.compareJson(exportDto, eventDto);
	}
}
