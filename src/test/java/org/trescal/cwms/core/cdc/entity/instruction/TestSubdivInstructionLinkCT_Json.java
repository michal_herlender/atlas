package org.trescal.cwms.core.cdc.entity.instruction;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.rest.export.dto.instruction.ExportSubdivInstructionLinkDTO;

public class TestSubdivInstructionLinkCT_Json extends TestEntityCT_Json {

	public static Integer ID = 123;
	public static Integer INSTRUCTIONID = 234;
	public static Integer SUBDIVID = 345;
	public static Integer ORGID = 456;
	public static Boolean ISSUBDIVINSTRUCTION = true;
	public static Integer BUSINESSSUBDIVID = 567;
	
	@Test
	public void compareFull() {
		ExportSubdivInstructionLinkDTO exportDto = new ExportSubdivInstructionLinkDTO(ID, INSTRUCTIONID, SUBDIVID, ORGID, ISSUBDIVINSTRUCTION, BUSINESSSUBDIVID); 
		
		// TODO set ID and instruction ID on CompanyInstructionLinkCT
		SubdivInstructionLinkCT eventDto = new SubdivInstructionLinkCT();
		eventDto.setId(ID);
		eventDto.setInstructionid(INSTRUCTIONID);
		eventDto.setBusinesssubdivid(BUSINESSSUBDIVID);
		eventDto.setIssubdivinstruction(ISSUBDIVINSTRUCTION);
		eventDto.setOrgid(ORGID);
		eventDto.setSubdivid(SUBDIVID);
		
		super.compareJson(exportDto, eventDto);
	}
}
