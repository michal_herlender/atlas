package org.trescal.cwms.core.cdc.entity.systemdefault;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.core.cdc.entity.defaultdata.SystemdDefaultApplicationCT;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultNames;
import org.trescal.cwms.rest.export.dto.systemdefault.ExportSystemDefaultApplicationDTO;

public class TestSystemDefaultApplicationCT_Json extends TestEntityCT_Json {

	public static Integer ID = 123;
	// Correlates to default id (stored in DB / exported as id due to use with SystemDefaultApplication)
	public static SystemDefaultNames DEFAULT = SystemDefaultNames.ACCREDITED_LABEL_CERTIFICATE_NUMBER;
	public static String DEFAULTVALUE = "true";
	public static Integer COMPANYID = 345;
	public static Integer SUBDIVID = 456;
	public static Integer PERSONID = 567;
	public static Integer ORGID = 678;
	
	@Test
	public void compareFull() {
		ExportSystemDefaultApplicationDTO exportDto = new ExportSystemDefaultApplicationDTO(
				ID, DEFAULT, DEFAULTVALUE, COMPANYID, SUBDIVID, 
				PERSONID, ORGID);
		
		SystemdDefaultApplicationCT eventDto = new SystemdDefaultApplicationCT();
		eventDto.setCompanyid(COMPANYID);
		eventDto.setDefaultid(DEFAULT.ordinal());
		eventDto.setDefaultvalue(DEFAULTVALUE);
		eventDto.setId(ID);
		eventDto.setOrgid(ORGID);
		eventDto.setPersonid(PERSONID);
		eventDto.setSubdivid(SUBDIVID);
		
		super.compareJson(exportDto, eventDto);
	}
}
