/**
 * 
 */
package org.trescal.cwms.core.cdc.entity.certificate;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.rest.export.dto.certificate.ExportCertLinkDTO;

/**
 * @Author shazil.khan
 *
 */
public class TestCertLinkCT_Json extends TestEntityCT_Json {
	
	private static Integer LINKID = 123;
	private static Integer CERTID = 123;
	private static Integer JOBITEMID = 123;
	
	
	@Test
	public void compareFull() {
		ExportCertLinkDTO exportDto = new ExportCertLinkDTO(LINKID, CERTID, JOBITEMID);
		
		CertLinkCT eventDto = new CertLinkCT();
		
		eventDto.setLinkid(LINKID);
		eventDto.setCertid(CERTID);
		eventDto.setJobitemid(JOBITEMID);
		
		compareJson(exportDto, eventDto);
	}

}
