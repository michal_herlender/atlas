package org.trescal.cwms.core.cdc.entity.instruction;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.rest.export.dto.instruction.ExportContactInstructionLinkDTO;

public class TestContactInstructionLinkCT_Json extends TestEntityCT_Json {

	public static Integer ID = 123;
	public static Integer INSTRUCTIONID = 234;
	public static Integer PERSONID = 345;
	public static Integer ORGID = 456;

	
	@Test
	public void compareFull() {
		ExportContactInstructionLinkDTO exportDto = new ExportContactInstructionLinkDTO(ID, INSTRUCTIONID, PERSONID, ORGID);
		
		// TODO set ID and instruction ID on ContactInstructionLinkCT
		ContactInstructionLinkCT eventDto = new ContactInstructionLinkCT();
		eventDto.setId(ID);
		eventDto.setInstructionid(INSTRUCTIONID);
		eventDto.setOrgid(ORGID);
		eventDto.setPersonid(PERSONID);
		
		super.compareJson(exportDto, eventDto);
	}
}
