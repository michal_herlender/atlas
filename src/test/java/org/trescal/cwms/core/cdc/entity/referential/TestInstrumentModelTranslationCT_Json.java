package org.trescal.cwms.core.cdc.entity.referential;

import java.util.Locale;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocales;
import org.trescal.cwms.rest.export.dto.referential.ExportTranslationDTO;

public class TestInstrumentModelTranslationCT_Json extends TestEntityCT_Json {
	
	public static Integer ID = 123;
	public static Locale LOCALE = SupportedLocales.ENGLISH_GB;
	public static String TRANSLATION = "Text 123";
	
	@Test
	public void compareFull() {
		
		ExportTranslationDTO exportDto = new ExportTranslationDTO(ID, LOCALE, TRANSLATION); 
		
		InstrumentModelTranslationCT eventDto = new InstrumentModelTranslationCT();
		eventDto.setId(ID);
		eventDto.setLocale(LOCALE);
		eventDto.setTranslation(TRANSLATION);
		
		super.compareJson(exportDto, eventDto);
	}
}
