package org.trescal.cwms.core.cdc.entity.workinstruction;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.rest.export.dto.workinstruction.ExportInstrumentWorkInstructionDTO;

public class TestInstrumentWorkInstructionCT_Json extends TestEntityCT_Json {

	public static Integer WORKINSTRUCTIONID = 123; 
	public static Integer PLANTID = 234;
	
	@Test
	public void compareFull() {
		ExportInstrumentWorkInstructionDTO exportDto = new ExportInstrumentWorkInstructionDTO();
		exportDto.setPlantId(PLANTID);
		exportDto.setWorkInstructionId(WORKINSTRUCTIONID);
		
		InstrumentWorkInstructionCT eventDto = new InstrumentWorkInstructionCT(); 
		eventDto.setPlantId(PLANTID);
		eventDto.setWorkInstructionId(WORKINSTRUCTIONID);
		
		super.compareJson(exportDto, eventDto);
	}
	
}
