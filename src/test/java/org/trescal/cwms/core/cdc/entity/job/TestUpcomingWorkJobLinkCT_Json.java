package org.trescal.cwms.core.cdc.entity.job;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.rest.export.dto.job.ExportUpcomingWorkJobLinkDTO;

public class TestUpcomingWorkJobLinkCT_Json extends TestEntityCT_Json {

	public static Integer ID = 123;
	public static Boolean ACTIVE = true;
	public static Integer JOBID = 234;
	public static Integer UPCOMINGWORKID = 345;
	
	@Test
	public void compareFull() {
		ExportUpcomingWorkJobLinkDTO exportDto = new ExportUpcomingWorkJobLinkDTO(ID, ACTIVE, JOBID, UPCOMINGWORKID);
		
		UpcomingWorkJobLinkCT eventDto = new UpcomingWorkJobLinkCT();
		eventDto.setActive(ACTIVE);
		eventDto.setId(ID);
		eventDto.setJobid(JOBID);
		eventDto.setUpcomingworkid(UPCOMINGWORKID);
		
		super.compareJson(exportDto, eventDto);
	}
}
