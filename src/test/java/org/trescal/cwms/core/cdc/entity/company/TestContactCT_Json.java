package org.trescal.cwms.core.cdc.entity.company;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.rest.export.dto.company.ExportContactDTO;

public class TestContactCT_Json extends TestEntityCT_Json {

	public static Integer PERSONID = 123;
	public static Boolean ACTIVE = true;
	public static String FIRSTNAME = "First";
	public static String LASTNAME = "Last";
	public static String EMAIL = "a@b.co";
	public static String TELEPHONE = "123-456-7890";
	public static String FAX = "234-567-8901";
	public static String MOBILE = "345-678-9012";
	public static Integer SUBDIVID = 234;
	public static Integer DEFADDRESS = 345;
		
	@Test
	public void compareFull() {
		ExportContactDTO exportDto = new ExportContactDTO(PERSONID, ACTIVE, FIRSTNAME, LASTNAME, EMAIL, TELEPHONE, FAX, MOBILE, SUBDIVID, DEFADDRESS);  
		
		ContactCT eventDto = new ContactCT(); 
		eventDto.setActive(ACTIVE);
		eventDto.setDefaddress(DEFADDRESS);
		eventDto.setEmail(EMAIL);
		eventDto.setFax(FAX);
		eventDto.setFirstname(FIRSTNAME);
		eventDto.setLastname(LASTNAME);
		eventDto.setMobile(MOBILE);
		eventDto.setPersonid(PERSONID);
		eventDto.setSubdivid(SUBDIVID);
		eventDto.setTelephone(TELEPHONE);
		
		super.compareJson(exportDto, eventDto);
	}
	
}
