package org.trescal.cwms.core.cdc.entity.instruction;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.rest.export.dto.instruction.ExportCompanyInstructionLinkDTO;

public class TestCompanyInstructionLinkCT_Json extends TestEntityCT_Json {

	public static Integer ID = 123;
	public static Integer INSTRUCTIONID = 234;
	public static Integer COID = 345;
	public static Integer ORGID = 456;
	public static Boolean ISSUBDIVINSTRUCTION = true;
	public static Integer BUSINESSSUBDIVID = 567;

	@Test
	public void compareFull() {
		ExportCompanyInstructionLinkDTO exportDto = new ExportCompanyInstructionLinkDTO(ID, INSTRUCTIONID, COID, ORGID, ISSUBDIVINSTRUCTION, BUSINESSSUBDIVID); 
		
		CompanyInstructionLinkCT eventDto = new CompanyInstructionLinkCT();
		
		// TODO set ID and instruction ID on CompanyInstructionLinkCT
		eventDto.setId(ID);
		eventDto.setInstructionid(INSTRUCTIONID);
		eventDto.setBusinesssubdivid(BUSINESSSUBDIVID);
		eventDto.setCoid(COID);
		eventDto.setIssubdivinstruction(ISSUBDIVINSTRUCTION);
		eventDto.setOrgid(ORGID);
		
		super.compareJson(exportDto, eventDto);
	}
}
