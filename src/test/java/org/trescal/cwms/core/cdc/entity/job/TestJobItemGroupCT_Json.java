package org.trescal.cwms.core.cdc.entity.job;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.rest.export.dto.job.ExportJobItemGroupDTO;

public class TestJobItemGroupCT_Json extends TestEntityCT_Json {
	
	private static Integer ID = 123;
	
	private static Boolean CALIBRATIONGROUP = true;
	
	private static Boolean DELIVERYGROUP = true;
	
	private static Integer JOBID = 110;
	
	@Test
	public void compareFull() {
		
		
		ExportJobItemGroupDTO exportDto = new ExportJobItemGroupDTO(ID,CALIBRATIONGROUP,DELIVERYGROUP,JOBID); 
		
		JobItemGroupCT eventDto = new JobItemGroupCT();
		eventDto.setId(ID);
		eventDto.setCalibrationGroup(CALIBRATIONGROUP);
		eventDto.setDeliveryGroup(DELIVERYGROUP);
		eventDto.setJobid(JOBID);
		
		super.compareJson(exportDto, eventDto);
	}

}
