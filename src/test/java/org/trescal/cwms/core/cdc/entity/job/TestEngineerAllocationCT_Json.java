package org.trescal.cwms.core.cdc.entity.job;

import java.time.LocalDate;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.core.workflow.entity.TimeOfDay;
import org.trescal.cwms.rest.export.dto.job.ExportEngineerAllocationDTO;

public class TestEngineerAllocationCT_Json extends TestEntityCT_Json {

	public static Integer ID = 123;
	public static Boolean ACTIVE = true;
	public static LocalDate ALLOCATEDFOR = LocalDate.of(2021, 3, 20);
	public static LocalDate ALLOCATEDON = LocalDate.of(2021, 3, 21);
	public static TimeOfDay TIMEOFDAY = TimeOfDay.AM;
	public static Integer ALLOCATEDBYID = 234;
	public static Integer ALLOCATEDTOID = 345;
	public static Integer JOBITEMID = 456;
	public static LocalDate ALLOCATEDUNTIL = LocalDate.of(2021, 3, 22);
	public static TimeOfDay TIMEOFDAYUNTIL = TimeOfDay.PM;
	public static Integer DEPTID = 567;

	@Test
	public void compareFull() {
		ExportEngineerAllocationDTO exportDto = new ExportEngineerAllocationDTO(
			ID, ACTIVE, ALLOCATEDFOR, ALLOCATEDON, TIMEOFDAY, ALLOCATEDBYID,
			ALLOCATEDTOID, JOBITEMID, ALLOCATEDUNTIL, TIMEOFDAYUNTIL, DEPTID);

		EngineerAllocationCT eventDto = new EngineerAllocationCT();
		eventDto.setActive(ACTIVE);
		eventDto.setAllocatedbyid(ALLOCATEDBYID);
		eventDto.setAllocatedFor((ALLOCATEDFOR));
		eventDto.setAllocatedOn((ALLOCATEDON));
		eventDto.setAllocatedtoid(ALLOCATEDTOID);
		eventDto.setAllocateduntil((ALLOCATEDUNTIL));
		eventDto.setDeptid(DEPTID);
		eventDto.setId(ID);
		eventDto.setJobitemid(JOBITEMID);
		eventDto.setTimeofday(TIMEOFDAY);
		eventDto.setTimeofdayuntil(TIMEOFDAYUNTIL);

		super.compareJson(exportDto, eventDto);
	}

}
