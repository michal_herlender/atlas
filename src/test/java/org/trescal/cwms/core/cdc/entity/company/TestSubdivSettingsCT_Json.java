package org.trescal.cwms.core.cdc.entity.company;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.rest.export.dto.company.ExportSubdivSettingsDTO;

public class TestSubdivSettingsCT_Json extends TestEntityCT_Json {

	public static Integer ID = 123;
	public static Integer ORGID = 234;
	public static Integer BUSINESSCONTACTID = 345;
	public static Integer SUBDIVID = 456;
	
	@Test
	public void compareFull() {
		ExportSubdivSettingsDTO exportDto = new ExportSubdivSettingsDTO(ID, ORGID, BUSINESSCONTACTID, SUBDIVID);
		SubdivSettingsCT eventDto = new SubdivSettingsCT();
		eventDto.setBusinesscontactid(BUSINESSCONTACTID);
		eventDto.setId(ID);
		eventDto.setOrgid(ORGID);
		eventDto.setSubdivid(SUBDIVID);
		
		super.compareJson(exportDto, eventDto);
	}	
}
