/**
 * 
 */
package org.trescal.cwms.core.cdc.entity.certificate;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.rest.export.dto.certificate.ExportInstCertLinkDTO;

/**
 * @Author shazil.khan
 *
 */
public class TestInstCertLinkCT_JSON extends TestEntityCT_Json{
	
	private static Integer LINKID = 123;
	private static Integer CERTID = 123;
	private static Integer PLANTID = 123;
	
	@Test
	public void compareFull() {
	ExportInstCertLinkDTO exportDto = new ExportInstCertLinkDTO(LINKID, CERTID, PLANTID);
	
	InstCertLinkCT eventDto = new InstCertLinkCT();
	eventDto.setId(LINKID);
	eventDto.setCertid(CERTID);
	eventDto.setPlantid(PLANTID);
	
	compareJson(exportDto, eventDto);
	
	
	}
	

}