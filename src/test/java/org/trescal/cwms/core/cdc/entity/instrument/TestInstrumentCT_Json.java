package org.trescal.cwms.core.cdc.entity.instrument;

import java.time.LocalDate;

import org.junit.Test;
import org.trescal.cwms.core.cdc.TestEntityCT_Json;
import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.rest.export.dto.instrument.ExportInstrumentDTO;

import com.fasterxml.jackson.annotation.JsonFormat;

public class TestInstrumentCT_Json extends TestEntityCT_Json {	 
	    
	public static Integer PLANTID = 123;
	public static Integer CALINTERVAL = 12;
	public static IntervalUnit CALINTERVALUNIT = IntervalUnit.MONTH;
	public static Boolean CALIBRATIONSTANDARD = true;
	
	@JsonFormat(pattern="yyyy-MM-dd")
	public static LocalDate RECALLDATE = LocalDate.of(2020, 02, 29);
	   
	public static String PLANTNO = "PN 123";
	public static String SERIALNO = "SN 123";
	public static InstrumentStatus STATUS = InstrumentStatus.IN_CIRCULATION;
	public static String CUSTOMERDESCRIPTION = "Cust Desc";
	public static String CUSTOMERSPECIFICATION = "Cust Spec";
	public static String FREETEXTLOCATION = "Free Text Location";
	public static String CLIENTBARCODE = "CBC 123";
	public static String FORMERBARCODE = "FBC 123";
	public static String MODELNAME = "MN 123";
	public static Integer ADDRESSID = 234;
	public static Integer COID = 345;
	public static Integer PERSONID = 456;
	public static Integer LOCATIONID = 567;
	public static Integer MFRID = 678;
	public static Integer MODELID = 789;
	public static Integer DEFAULTSERVICETYPEID = 890;
	public static Integer USAGEID = 1;
	public static boolean CUSTOMERMANAGED = false;
	
	@Test
	public void compareFull() {
		
		ExportInstrumentDTO exportDto = new ExportInstrumentDTO(PLANTID, CALINTERVAL, CALINTERVALUNIT, CALIBRATIONSTANDARD, RECALLDATE,
				 PLANTNO, SERIALNO, STATUS, CUSTOMERDESCRIPTION, CUSTOMERSPECIFICATION,
				 FREETEXTLOCATION, CLIENTBARCODE, FORMERBARCODE, MODELNAME, ADDRESSID,
				 COID, PERSONID, LOCATIONID, MFRID, MODELID,
				 DEFAULTSERVICETYPEID,USAGEID,CUSTOMERMANAGED);
		
		System.out.println("Value before " + RECALLDATE);
		
		InstrumentCT eventDto = new InstrumentCT();
		eventDto.setAddressid(ADDRESSID);
		eventDto.setCalibrationStandard(CALIBRATIONSTANDARD);
		eventDto.setCalInterval(CALINTERVAL);
		eventDto.setCalIntervalUnit(CALINTERVALUNIT);
		// Data type mismatch here; clientBarcode is nvarchar in database; String in Java, but Integer in CT
		eventDto.setClientBarcode(CLIENTBARCODE);
		eventDto.setCoid(COID);
		eventDto.setCustomerDescription(CUSTOMERDESCRIPTION);
		eventDto.setCustomerSpecification(CUSTOMERSPECIFICATION);
		eventDto.setDefaultservicetypeid(DEFAULTSERVICETYPEID);
		eventDto.setFormerBarcode(FORMERBARCODE);
		eventDto.setFreeTextLocation(FREETEXTLOCATION);
		eventDto.setLocationid(LOCATIONID);
		eventDto.setMfrid(MFRID);
		eventDto.setModelid(MODELID);
		eventDto.setModelname(MODELNAME);
		eventDto.setPersonid(PERSONID);
		eventDto.setPlantid(PLANTID);
		eventDto.setPlantno(PLANTNO);
		// Note, InstrumentCT currently uses Date, could move to LocalDate (note, we should format as yyyy-MM-dd)
		//eventDto.setRecallDate(LocalDate.from(RECALLDATE.atStartOfDay(ZoneId.systemDefault()).toInstant()));
		eventDto.setRecallDate(RECALLDATE);
		
		System.out.println("Value after " + RECALLDATE);
		
		
		eventDto.setSerialno(SERIALNO);
		eventDto.setStatus(STATUS);
		eventDto.setUsageid(USAGEID);
		eventDto.setCustomermanaged(CUSTOMERMANAGED);
		
		super.compareJson(exportDto, eventDto);
	}	
}
