package org.trescal.cwms.core.documents.birt.file;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.core.documents.birt.AbstractBirtTest;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentType;
import org.trescal.cwms.core.documents.birt.repaircompletionreport.RepairInspectionReportDataSet;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocales;
import org.trescal.cwms.tests.generic.DataSetJob;

public class RepairCompletionReportFileTest extends AbstractBirtTest {

	private static final String SUB_DIRECTORY = "Repair Completion Report";
	private static final String FILE_NAME_REGULAR = "Sample RCR";
	
	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetJob.getJobClientRepairTestData();
	}

	private void test(int entityId, Locale locale) throws Exception {
		Map<String,Object> additionalParameters = new HashMap<>();
		additionalParameters.put(RepairInspectionReportDataSet.PARAMETER_COMPLETION_REPORT, true);
		testDocument(entityId, BaseDocumentType.REPAIR_COMPLETION_REPORT, locale, SUB_DIRECTORY, FILE_NAME_REGULAR, additionalParameters);
	}
	
	@Test
	@Transactional
	public void testFrance() throws Exception {
		test(1101, SupportedLocales.FRENCH_FR);
	}
	
	@Test
	@Transactional
	public void testPortuguese() throws Exception {
		test(1101, SupportedLocales.PORTUGUESE_PT);
	}

}
