package org.trescal.cwms.core.documents.birt.file;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.core.documents.birt.AbstractBirtTest;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentType;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocales;
import org.trescal.cwms.tests.generic.DataSetDelivery;

public class ClientDeliveryProjectionFileTest extends AbstractBirtTest {

	private static final String SUB_DIRECTORY = "Client Delivery Projection";
	private static final String FILE_NAME_REGULAR = "Sample Client Delivery Projection";
	
	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetDelivery.getDeliveryNoteClientTestData();
	}

	private void test(int entityId, Locale locale) throws Exception {
		Map<String,Object> params = new HashMap<>();
		params.put("showPurchaseOrder", Boolean.TRUE);
		params.put("showTotalPrice", Boolean.TRUE);
		params.put("showDateReceived", Boolean.TRUE);
		params.put("showCertificates", Boolean.TRUE);
		testDocument(entityId, BaseDocumentType.DELIVERY_PROJECTION, locale, SUB_DIRECTORY, FILE_NAME_REGULAR, params);
	}
	
	@Test
	@Transactional
	public void testFrance() throws Exception {
		test(110, SupportedLocales.FRENCH_FR);
	}
	
	@Test
	@Transactional
	public void testGermany() throws Exception {
		test(120, SupportedLocales.GERMAN_DE);
	}
	
	@Test
	@Transactional
	public void testMorocco() throws Exception {
		test(130, SupportedLocales.FRENCH_FR);
	}
	
	@Test
	@Transactional
	public void testSpain() throws Exception {
		test(140, SupportedLocales.SPANISH_ES);
	}
	
	@Test
	@Transactional
	public void testUSA() throws Exception {
		test(150, SupportedLocales.ENGLISH_US);
	}
	
	@Test
	@Transactional
	public void testPortuguese() throws Exception {
		test(150, SupportedLocales.PORTUGUESE_PT);
	}

}
