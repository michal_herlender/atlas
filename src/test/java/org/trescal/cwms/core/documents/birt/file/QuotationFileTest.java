package org.trescal.cwms.core.documents.birt.file;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.documents.birt.AbstractBirtTest;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentType;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocales;
import org.trescal.cwms.tests.generic.DataSetPricing;

public class QuotationFileTest extends AbstractBirtTest {

	private static final String SUB_DIRECTORY = "Quotation";
	private static final String FILE_NAME_REGULAR = "Sample Quotation";
	
	@Autowired
	private QuotationService quotationService;
	
	@Override
	protected List<String> getDataSetFileNames() {
		List<String> result = DataSetPricing.getQuotationTestData();
		return result;
	}

	private void test(int entityId, Locale locale) throws Exception {
		Map<String,Object> additionalParameters = new HashMap<>();
		additionalParameters.put(BaseDocumentDefinitions.PARAMETER_FACSIMILE, false);
		testDocument(entityId, BaseDocumentType.QUOTATION, locale, SUB_DIRECTORY, FILE_NAME_REGULAR, additionalParameters);
	}
	
	@Test
	@Transactional
	public void testFrance() throws Exception {
		this.quotationService.setAvalaraAware(false);
		test(110, SupportedLocales.FRENCH_FR);
	}

	@Test
	@Transactional
	public void testPortuguese() throws Exception {
		this.quotationService.setAvalaraAware(false);
		test(110, SupportedLocales.PORTUGUESE_PT);
	}

	@Test
	@Transactional
	public void testEnglish() throws Exception {
		this.quotationService.setAvalaraAware(true);
		test(110, SupportedLocales.ENGLISH_US);
	}
	
}
