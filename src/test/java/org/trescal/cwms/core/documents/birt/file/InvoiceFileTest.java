package org.trescal.cwms.core.documents.birt.file;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.core.documents.birt.AbstractBirtTest;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentType;
import org.trescal.cwms.core.documents.birt.invoices.InvoiceDataSet;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocales;
import org.trescal.cwms.tests.generic.DataSetCore;
import org.trescal.cwms.tests.generic.DataSetPricing;

public class InvoiceFileTest extends AbstractBirtTest {

	private static final String SUB_DIRECTORY = "Invoice";
	private static final String FILE_NAME_REGULAR = "Sample Invoice";
	private static final String FILE_NAME_NO_DISCOUNT = "Sample Invoice (No Discount)";
	private static final String FILE_NAME_MINIMAL = "Sample Invoice (Minimal)";
	private static final String FILE_NAME_DRAFT = "Sample Invoice (Draft)";
	private static final String FILE_NAME_NO_APPENDIX = "Sample Invoice (No Appendix)";
	
	// Defines 3 maps of parameters for testing different cases of invoices
	private Map<String, Object> paramsFullDraft;
	private Map<String, Object> paramsFull;
	private Map<String, Object> paramsNoDiscount;
	private Map<String, Object> paramsMinimal;
	private Map<String, Object> paramsFullNoAppendix;
	
	public InvoiceFileTest() {
		
		// 1 - Full Draft - intended for testing that the DRAFT background appears on all pages
		paramsFullDraft = new HashMap<>();
		paramsFullDraft.put(BaseDocumentDefinitions.PARAMETER_FACSIMILE, true);
		paramsFullDraft.put(InvoiceDataSet.PARAMETER_PRINT_DISCOUNT, true);
		paramsFullDraft.put(InvoiceDataSet.PARAMETER_PRINT_QUANTITY, true);
		paramsFullDraft.put(InvoiceDataSet.PARAMETER_PRINT_UNITPRICE, true);
		paramsFullDraft.put(InvoiceDataSet.PARAMETER_SHOW_APPENDIX, true);
		paramsFullDraft.put(InvoiceDataSet.PARAMETER_SUMMARIZED, false);
		
		// 2 - Full - intended for testing with all configurable parts of document turned ON
		paramsFull = new HashMap<>();
		paramsFull.put(BaseDocumentDefinitions.PARAMETER_FACSIMILE, false);
		paramsFull.put(InvoiceDataSet.PARAMETER_PRINT_DISCOUNT, true);
		paramsFull.put(InvoiceDataSet.PARAMETER_PRINT_QUANTITY, true);
		paramsFull.put(InvoiceDataSet.PARAMETER_PRINT_UNITPRICE, true);
		paramsFull.put(InvoiceDataSet.PARAMETER_SHOW_APPENDIX, true);
		paramsFull.put(InvoiceDataSet.PARAMETER_SUMMARIZED, false);
		
		// 3 - Without Discount - intended for testing with all configurable parts of document turned ON except discount
		paramsNoDiscount= new HashMap<>();
		paramsNoDiscount.put(BaseDocumentDefinitions.PARAMETER_FACSIMILE, false);
		paramsNoDiscount.put(InvoiceDataSet.PARAMETER_PRINT_DISCOUNT, false);
		paramsNoDiscount.put(InvoiceDataSet.PARAMETER_PRINT_QUANTITY, true);
		paramsNoDiscount.put(InvoiceDataSet.PARAMETER_PRINT_UNITPRICE, true);
		paramsNoDiscount.put(InvoiceDataSet.PARAMETER_SHOW_APPENDIX, true);
		paramsNoDiscount.put(InvoiceDataSet.PARAMETER_SUMMARIZED, false);
		
		// 4 - Minimal - intended for testing with all configurable parts of document turned OFF
		paramsMinimal = new HashMap<>();
		paramsMinimal.put(BaseDocumentDefinitions.PARAMETER_FACSIMILE, false);
		paramsMinimal.put(InvoiceDataSet.PARAMETER_PRINT_DISCOUNT, false);
		paramsMinimal.put(InvoiceDataSet.PARAMETER_PRINT_QUANTITY, false);
		paramsMinimal.put(InvoiceDataSet.PARAMETER_PRINT_UNITPRICE, false);
		paramsMinimal.put(InvoiceDataSet.PARAMETER_SHOW_APPENDIX, false);
		paramsMinimal.put(InvoiceDataSet.PARAMETER_SUMMARIZED, true);
		
		// 5 - Full No Appendix- intended for testing with all configurable parts of document turned ON except for appendix
		paramsFullNoAppendix = new HashMap<>();
		paramsFullNoAppendix.put(BaseDocumentDefinitions.PARAMETER_FACSIMILE, false);
		paramsFullNoAppendix.put(InvoiceDataSet.PARAMETER_PRINT_DISCOUNT, true);
		paramsFullNoAppendix.put(InvoiceDataSet.PARAMETER_PRINT_QUANTITY, true);
		paramsFullNoAppendix.put(InvoiceDataSet.PARAMETER_PRINT_UNITPRICE, true);
		paramsFullNoAppendix.put(InvoiceDataSet.PARAMETER_SHOW_APPENDIX, false);
		paramsFullNoAppendix.put(InvoiceDataSet.PARAMETER_SUMMARIZED, false);
		
	}
	
	@Override
	protected List<String> getDataSetFileNames() {
		List<String> result = DataSetPricing.getInvoiceTestData();
		result.add(DataSetCore.FILE_BASE_SERVICE_TYPE_TRANSLATIONS);
		return result;
	}
	
	private void test(int entityId, BaseDocumentType documentType, Locale locale, String fileNamePrefix, Map<String,Object> additionalParameters) throws Exception {
		testDocument(entityId, documentType, locale, SUB_DIRECTORY, fileNamePrefix, additionalParameters);
	}

	@Test
	@Transactional
	public void testFrance() throws Exception {
		test(110, BaseDocumentType.INVOICE_CALIBRATION, SupportedLocales.FRENCH_FR, FILE_NAME_REGULAR, paramsFull);
	}

	@Test
	@Transactional
	public void testFranceDraft() throws Exception {
		test(110, BaseDocumentType.INVOICE_CALIBRATION, SupportedLocales.FRENCH_FR, FILE_NAME_DRAFT, paramsFullDraft);
	}

	@Test
	@Transactional
	public void testFranceNoDiscount() throws Exception {
		test(110, BaseDocumentType.INVOICE_CALIBRATION, SupportedLocales.FRENCH_FR, FILE_NAME_NO_DISCOUNT, paramsNoDiscount);
	}

	@Test
	@Transactional
	public void testFranceMinimal() throws Exception {
		test(110, BaseDocumentType.INVOICE_CALIBRATION, SupportedLocales.FRENCH_FR, FILE_NAME_MINIMAL, paramsMinimal);
	}

	@Test
	@Transactional
	public void testFranceIntercompany() throws Exception {
		// Tests intercompany document in English
		test(510, BaseDocumentType.INVOICE_CALIBRATION, SupportedLocales.ENGLISH_US, FILE_NAME_REGULAR, paramsFull);
	}
	
	@Test
	@Transactional
	public void testGermany() throws Exception {
		test(120, BaseDocumentType.INVOICE_CALIBRATION, SupportedLocales.GERMAN_DE, FILE_NAME_REGULAR, paramsFull);
	}
	
	@Test
	@Transactional
	public void testMorocco() throws Exception {
		test(130, BaseDocumentType.INVOICE_CALIBRATION, SupportedLocales.FRENCH_FR, FILE_NAME_REGULAR, paramsFull);
	}
	
	@Test
	@Transactional
	public void testSpain() throws Exception {
		test(140, BaseDocumentType.INVOICE_CALIBRATION, SupportedLocales.SPANISH_ES, FILE_NAME_REGULAR, paramsFull);
	}
	
	@Test
	@Transactional
	public void testUSA() throws Exception {
		test(150, BaseDocumentType.INVOICE_CALIBRATION, SupportedLocales.ENGLISH_US, FILE_NAME_REGULAR, paramsFull);
	}

	@Test
	@Transactional
	public void testUSANoDiscount() throws Exception {
		test(150, BaseDocumentType.INVOICE_CALIBRATION, SupportedLocales.ENGLISH_US, FILE_NAME_NO_DISCOUNT, paramsNoDiscount);
	}
	
	@Test
	@Transactional
	public void testUSAMinimal() throws Exception {
		test(150, BaseDocumentType.INVOICE_CALIBRATION, SupportedLocales.ENGLISH_US, FILE_NAME_MINIMAL, paramsMinimal);
	}
	
	@Test
	@Transactional
	public void testUSANoAppendix() throws Exception {
		test(150, BaseDocumentType.INVOICE_CALIBRATION, SupportedLocales.ENGLISH_US, FILE_NAME_NO_APPENDIX, paramsMinimal);
	}
	
	@Test
	@Transactional
	public void testPortuguese() throws Exception {
		test(150, BaseDocumentType.INVOICE_CALIBRATION, SupportedLocales.PORTUGUESE_PT, FILE_NAME_REGULAR, paramsFull);
	}

}
