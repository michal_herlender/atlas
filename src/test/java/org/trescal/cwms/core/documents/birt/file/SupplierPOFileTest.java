package org.trescal.cwms.core.documents.birt.file;

import java.util.List;
import java.util.Locale;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.core.documents.birt.AbstractBirtTest;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentType;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocales;
import org.trescal.cwms.tests.generic.DataSetPricing;

public class SupplierPOFileTest extends AbstractBirtTest {

	private static final String SUB_DIRECTORY = "SupplierPO";
	private static final String FILE_NAME_REGULAR = "Sample Supplier PO";
	
	@Override
	protected List<String> getDataSetFileNames() {
		List<String> result = DataSetPricing.getSupplierPOTestData();
		return result;
	}

	private void test(int entityId, Locale locale) throws Exception {
		testDocument(entityId, BaseDocumentType.PURCHASE_ORDER, locale, SUB_DIRECTORY, FILE_NAME_REGULAR);
	}
	
	@Test
	@Transactional
	public void testFrance() throws Exception {
		test(110, SupportedLocales.FRENCH_FR);
	}	
	
	@Test
	@Transactional
	public void testPortuguese() throws Exception {
		test(110, SupportedLocales.PORTUGUESE_PT);
	}
	
	@Test
	@Transactional
	public void testUSTaxable() throws Exception {
		// PO should show up as Taxable = Yes
		test(150, SupportedLocales.ENGLISH_US);
	}	
	
	@Test
	@Transactional
	public void testUSNonTaxable() throws Exception {
		// PO should show up as Taxable = No
		test(151, SupportedLocales.ENGLISH_US);
	}	
	
	@Test
	@Transactional
	public void testUSNotApplicable() throws Exception {
		// PO should show up with Taxable row hidden as Taxable = Not Applicable
		test(152, SupportedLocales.ENGLISH_US);
	}	
	
}
