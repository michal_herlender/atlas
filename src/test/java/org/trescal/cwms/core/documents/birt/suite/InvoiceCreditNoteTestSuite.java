package org.trescal.cwms.core.documents.birt.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.trescal.cwms.core.documents.birt.file.CreditNoteFileTest;
import org.trescal.cwms.core.documents.birt.file.InvoiceFileTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	CreditNoteFileTest.class,
	InvoiceFileTest.class,
})


public class InvoiceCreditNoteTestSuite {

}
