package org.trescal.cwms.core.documents.birt.file;

import org.junit.platform.suite.api.SelectPackages;
import org.junit.platform.suite.api.Suite;
//import org.junit.runner.RunWith;
//import org.junit.runners.Suite;

//@RunWith(Suite.class)
//@Suite.SuiteClasses({
//	ClientDeliveryFileTest.class,
//	ClientDeliveryProjectionFileTest.class,
//	CreditNoteFileTest.class,
//	InternalDeliveryFileTest.class,
//	InvoiceFileTest.class,
//	JobCostingFileTest.class,
//	QuotationFileTest.class,
//	RepairCompletionReportFileTest.class,
//	RepairInspectionReportFileTest.class,
//	SupplierDeliveryFileTest.class,
//	SupplierPOFileTest.class,
//	TpQuoteReqFileTest.class
//})
//
@Suite
@SelectPackages("org.trescal.cwms.core.documents.birt")

public class FileTestSuite {

}
