package org.trescal.cwms.core.documents.birt.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.trescal.cwms.core.misc.controller.SettableLocaleResolver;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocales;
import org.trescal.cwms.tests.MvcCwmsTestClass;
import org.trescal.cwms.tests.generic.DataSetPricing;

public class InvoiceDocumentControllerTest extends MvcCwmsTestClass {
	
	@Autowired
	@Qualifier(value="localeResolver")
	private SettableLocaleResolver settableLocaleResolver;
	
	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetPricing.getInvoiceTestData();
	}

	private void test(int invoiceId, Locale locale) throws Exception {
		this.settableLocaleResolver.setLocale(locale);
		
		this.mockMvc.perform(
				get("/birtinvoicedocument.htm")
				.param("id", String.valueOf(invoiceId))
				.sessionAttr(Constants.HTTPSESS_NEW_FILE_LIST, new ArrayList<String>()))
		.andExpect(redirectedUrl("viewinvoice.htm?id="+invoiceId+"&loadtab=files-tab"));
	}
	
	/**
	 * Just one document is tested here; as this is an MVC test of the controller
	 * see file tests for detailed tests of different formats / languages   
	 */
	@Test
	public void testFrance() throws Exception {
		test(110, SupportedLocales.FRENCH_FR);
	}		
}