package org.trescal.cwms.core.documents.birt;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentType;
import org.trescal.cwms.core.tools.BirtEngine;
import org.trescal.cwms.tests.MvcComponentTestClass;

/**
 * Base class for BIRT document testing
 * Subclasses should define their own @Test methods which :
 * (a) are also annotated with @Transactional for BirtEngine lazy loading
 * (b) define data sets needed
 * (c) normally call the base method testDocument(...) to generate document
 */
public abstract class AbstractBirtTest extends MvcComponentTestClass {
	// Note, designed for use in Windows, could set output directory in properties file
	public static final String TEST_OUTPUT_DIRECTORY = "C:\\GroupERP\\Testing\\Documents";
	
	@Autowired
	private BirtEngine birtEngine;
	
	private Logger logger = LoggerFactory.getLogger(AbstractBirtTest.class);

	/**
	 * For use when there are additionalParameters to provide, e.g. invoices
	 */
	public void testDocument(int entityId, BaseDocumentType type, Locale locale, String subDirectory, String fileNamePrefix, Map<String, Object> additionalParameters) throws Exception {
	    Map<String, Object> parameters = new HashMap<>();
	    parameters.put(BaseDocumentDefinitions.PARAMETER_ENTITY_ID, entityId);
	    parameters.put(BaseDocumentDefinitions.PARAMETER_DOCUMENT_TYPE, type);
	    parameters.put(BaseDocumentDefinitions.PARAMETER_DOCUMENT_LOCALE, locale);
	    if (additionalParameters != null) {
	    	parameters.putAll(additionalParameters);
	    }
	    
		//Create binary contents
	    String templatePath = type.getTemplatePath();
		byte[] pdf = birtEngine.generatePDFAsByteArray(templatePath, parameters, locale);
		
		StringBuffer buffer = new StringBuffer(TEST_OUTPUT_DIRECTORY);
		buffer.append(File.separator);
		buffer.append(subDirectory);
		String directoryName = buffer.toString(); 
		buffer.append(File.separator);
		buffer.append(fileNamePrefix);
		buffer.append(" - ");
		buffer.append(entityId);
		buffer.append(" - ");
		buffer.append(locale.toString());
		buffer.append(".pdf");
		String qualifiedFileName = buffer.toString();
		
		File existingFile = new File(qualifiedFileName);
		if (existingFile.exists()) {
			logger.info("Deleting existing file "+existingFile.getAbsolutePath());
			existingFile.delete();
		}
		File directory = new File(directoryName);
		if (!directory.exists()) {
			logger.info("Creating directory "+directoryName);
			directory.mkdirs();
		}
		
		try (FileOutputStream os = new FileOutputStream(qualifiedFileName)) {
			os.write(pdf);
			os.flush();
			os.close();
		}
		catch (Exception e) {
			logger.error("Error writing file", e);
			throw new RuntimeException(e);
		}
		logger.info("Successfully wrote file "+qualifiedFileName);
	}

	/**
	 * For use when there are no additional parameters to provide
	 */
	public void testDocument(int entityId, BaseDocumentType type, Locale locale, String subDirectory, String fileNamePrefix) throws Exception {
		testDocument(entityId, type, locale, subDirectory, fileNamePrefix, null);
	}
}
