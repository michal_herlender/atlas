package org.trescal.cwms.core.documents.birt.file;

import java.util.List;
import java.util.Locale;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.core.documents.birt.AbstractBirtTest;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentType;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocales;
import org.trescal.cwms.tests.generic.DataSetDelivery;

public class InternalDeliveryFileTest extends AbstractBirtTest {

	private static final String SUB_DIRECTORY = "Internal Delivery";
	private static final String FILE_NAME_REGULAR = "Sample Internal Delivery";
	
	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetDelivery.getDeliveryNoteInternalTestData();
	}

	private void test(int entityId, Locale locale) throws Exception {
		testDocument(entityId, BaseDocumentType.INTERNAL_DELIVERY, locale, SUB_DIRECTORY, FILE_NAME_REGULAR);
	}
	
	@Test
	@Transactional
	public void testFranceToMorocco() throws Exception {
		test(110, SupportedLocales.FRENCH_FR);
	}
	
	@Test
	@Transactional
	public void testGermanyToUSA() throws Exception {
		test(120, SupportedLocales.GERMAN_DE);
	}
	
	@Test
	@Transactional
	public void testMoroccoToFrance() throws Exception {
		test(130, SupportedLocales.FRENCH_FR);
	}
	
	@Test
	@Transactional
	public void testSpainToGermany() throws Exception {
		test(140, SupportedLocales.SPANISH_ES);
	}
	
	@Test
	@Transactional
	public void testUSAToSpain() throws Exception {
		test(150, SupportedLocales.ENGLISH_US);
	}
	
	@Test
	@Transactional
	public void testPortuguese() throws Exception {
		test(150, SupportedLocales.PORTUGUESE_PT);
	}
	

}
