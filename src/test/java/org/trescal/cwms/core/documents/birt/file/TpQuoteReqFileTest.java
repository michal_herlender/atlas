package org.trescal.cwms.core.documents.birt.file;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.core.documents.birt.AbstractBirtTest;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentType;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocales;
import org.trescal.cwms.tests.generic.DataSetPricing;
import org.trescal.cwms.tests.generic.pricing.TestDataTpQuoteRequest;

public class TpQuoteReqFileTest extends AbstractBirtTest {
	
	private static final String SUB_DIRECTORY = "Supplier Quotation Request";
	private static final String FILE_NAME_REGULAR = "Sample Supplier Quotation Request";
	private static final String FILE_NAME_DRAFT = "Sample Supplier Quotation Request (Draft)";
	
	@Override
	protected List<String> getDataSetFileNames() {
		List<String> result = DataSetPricing.getTPQuoteRequestTestData();
		return result;
	}
	
	private void test(int entityId, Locale locale,String fileNamePrefix, boolean facsimile) throws Exception {
		Map<String, Object> additionalParameters = new HashMap<>();
		additionalParameters.put(BaseDocumentDefinitions.PARAMETER_FACSIMILE, facsimile);
		testDocument(entityId, BaseDocumentType.TP_QUOTE_REQUEST, locale, SUB_DIRECTORY, fileNamePrefix, additionalParameters);
	}

	@Test
	@Transactional
	public void testFrance() throws Exception {
		test(TestDataTpQuoteRequest.ID_ACTIVE_FRANCE, SupportedLocales.FRENCH_FR, FILE_NAME_REGULAR, false);
	}

	@Test
	@Transactional
	public void testFranceDraft() throws Exception {
		test(110, SupportedLocales.FRENCH_FR, FILE_NAME_DRAFT, true);
	}
	
	@Test
	@Transactional
	public void testGermany() throws Exception {
		test(TestDataTpQuoteRequest.ID_ACTIVE_Germany, SupportedLocales.GERMAN_DE, FILE_NAME_REGULAR, false);
	}
	
	@Test
	@Transactional
	public void testMorocco() throws Exception {
		test(TestDataTpQuoteRequest.ID_ACTIVE_Morocco, SupportedLocales.FRENCH_FR, FILE_NAME_REGULAR, false);
	}
	
	@Test
	@Transactional
	public void testSpain() throws Exception {
		test(TestDataTpQuoteRequest.ID_ACTIVE_Spain, SupportedLocales.SPANISH_ES, FILE_NAME_REGULAR, false);
	}

	@Test
	@Transactional
	public void testUSA() throws Exception {
		test(TestDataTpQuoteRequest.ID_ACTIVE_USA, SupportedLocales.ENGLISH_US, FILE_NAME_REGULAR, false);
	}

	@Test
	@Transactional
	public void testPortuguese() throws Exception {
		test(TestDataTpQuoteRequest.ID_ACTIVE_USA, SupportedLocales.PORTUGUESE_PT, FILE_NAME_REGULAR, false);
	}
	
}
