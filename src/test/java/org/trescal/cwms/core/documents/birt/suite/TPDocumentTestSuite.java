package org.trescal.cwms.core.documents.birt.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.trescal.cwms.core.documents.birt.file.TPQuotationFileTest;
import org.trescal.cwms.core.documents.birt.file.TpQuoteReqFileTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	TPQuotationFileTest.class,
	TpQuoteReqFileTest.class,
})

public class TPDocumentTestSuite {

}
