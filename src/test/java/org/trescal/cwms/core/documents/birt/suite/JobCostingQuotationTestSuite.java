package org.trescal.cwms.core.documents.birt.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.trescal.cwms.core.documents.birt.file.JobCostingFileTest;
import org.trescal.cwms.core.documents.birt.file.QuotationFileTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	JobCostingFileTest.class,
	QuotationFileTest.class,
})


public class JobCostingQuotationTestSuite {

}
