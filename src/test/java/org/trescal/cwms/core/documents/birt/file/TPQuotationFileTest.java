package org.trescal.cwms.core.documents.birt.file;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.core.documents.birt.AbstractBirtTest;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentType;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocales;
import org.trescal.cwms.tests.generic.DataSetPricing;
import org.trescal.cwms.tests.generic.pricing.TestDataTPQuotation;

public class TPQuotationFileTest extends AbstractBirtTest {

	private static final String SUB_DIRECTORY = "Supplier Quotation";
	private static final String FILE_NAME_REGULAR = "Sample Supplier Quotation";
	private static final String FILE_NAME_DRAFT = "Sample Supplier Quotation (Draft)";
	
	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetPricing.getTPQuotationTestData();
	}
	
	private void test(int entityId, Locale locale,String fileNamePrefix, boolean facsimile) throws Exception {
		Map<String, Object> additionalParameters = new HashMap<>();
		additionalParameters.put(BaseDocumentDefinitions.PARAMETER_FACSIMILE, facsimile);
		testDocument(entityId, BaseDocumentType.TP_QUOTATION, locale, SUB_DIRECTORY, fileNamePrefix, additionalParameters);
	}

	@Test
	@Transactional
	public void testFrance() throws Exception {
		test(TestDataTPQuotation.ID_ACTIVE_FRANCE, SupportedLocales.FRENCH_FR, FILE_NAME_REGULAR, false);
	}

	@Test
	@Transactional
	public void testFranceDraft() throws Exception {
		test(TestDataTPQuotation.ID_ACTIVE_FRANCE, SupportedLocales.FRENCH_FR, FILE_NAME_DRAFT, true);
	}
	
}
