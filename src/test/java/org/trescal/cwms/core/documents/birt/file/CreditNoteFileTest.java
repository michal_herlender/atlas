package org.trescal.cwms.core.documents.birt.file;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.documents.birt.AbstractBirtTest;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentType;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocales;
import org.trescal.cwms.tests.generic.DataSetPricing;

public class CreditNoteFileTest extends AbstractBirtTest {
	
	private static final String SUB_DIRECTORY = "CreditNote";
	private static final String FILE_NAME_REGULAR = "Sample Credit Note";
	private static final String FILE_NAME_DRAFT = "Sample Credit Note (Draft)";
	private static final String FILE_NAME_ADDRESS = "Sample Credit Note (Address)";
	
	@Autowired
	private InvoiceService invoiceService;
	@Autowired
	private AddressService addressService;
	
	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetPricing.getCreditNoteTestData();
	}
	
	private void test(int entityId, Locale locale, String fileNamePrefix, boolean facsimile) throws Exception {
		Map<String, Object> additionalParameters = new HashMap<>();
		additionalParameters.put(BaseDocumentDefinitions.PARAMETER_FACSIMILE, facsimile);
		testDocument(entityId, BaseDocumentType.CREDIT_NOTE, locale, SUB_DIRECTORY, fileNamePrefix, additionalParameters);
	}

	@Test
	@Transactional
	public void testFrance() throws Exception {
		test(110, SupportedLocales.FRENCH_FR, FILE_NAME_REGULAR, false);
	}

	@Test
	@Transactional
	public void testFranceDraft() throws Exception {
		test(110, SupportedLocales.FRENCH_FR, FILE_NAME_DRAFT, true);
	}

	@Test
	@Transactional
	public void testFranceLegalAddress() throws Exception {
		// Test case where the legal address of the invoice is different than the mailing address
		Invoice invoice110 = this.invoiceService.findInvoice(110);
		Address address111 = this.addressService.get(111);
		invoice110.setAddress(address111);
		test(110, SupportedLocales.FRENCH_FR, FILE_NAME_ADDRESS, false);
	}
	
	@Test
	@Transactional
	public void testGermany() throws Exception {
		test(120, SupportedLocales.GERMAN_DE, FILE_NAME_REGULAR, false);
	}
	
	@Test
	@Transactional
	public void testMorocco() throws Exception {
		test(130, SupportedLocales.FRENCH_FR, FILE_NAME_REGULAR, false);
	}
	
	@Test
	@Transactional
	public void testSpain() throws Exception {
		test(140, SupportedLocales.SPANISH_ES, FILE_NAME_REGULAR, false);
	}
	
	@Test
	@Transactional
	public void testUSA() throws Exception {
		test(150, SupportedLocales.ENGLISH_US, FILE_NAME_REGULAR, false);
	}
	
	@Test
	@Transactional
	public void testPortuguese() throws Exception {
		test(150, SupportedLocales.PORTUGUESE_PT, FILE_NAME_REGULAR, false);
	}
	
}
