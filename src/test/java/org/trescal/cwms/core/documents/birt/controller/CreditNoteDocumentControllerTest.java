package org.trescal.cwms.core.documents.birt.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.trescal.cwms.core.misc.controller.SettableLocaleResolver;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocales;
import org.trescal.cwms.tests.MvcCwmsTestClass;
import org.trescal.cwms.tests.generic.DataSetPricing;

/**
 * TODO test file existence, delete files before generation?
 */
public class CreditNoteDocumentControllerTest extends MvcCwmsTestClass {
	
	@Autowired
	@Qualifier(value="localeResolver")
	private SettableLocaleResolver settableLocaleResolver;
	
	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetPricing.getCreditNoteTestData();
	}

	private void test(int creditNoteId, Locale locale) throws Exception {
		this.settableLocaleResolver.setLocale(locale);
		
		this.mockMvc.perform(
				get("/birtcreditnote.htm")
				.param("id", String.valueOf(creditNoteId))
				.sessionAttr(Constants.HTTPSESS_NEW_FILE_LIST, new ArrayList<String>()))
		.andExpect(redirectedUrl("viewcreditnote.htm?id="+creditNoteId+"&loadtab=cndocs-tab"));
	}

	/**
	 * Just one document is tested here; as this is an MVC test of the controller
	 * see file tests for detailed tests of different formats / languages   
	 */
	@Test
	public void testFrance() throws Exception {
		test(110, SupportedLocales.FRENCH_FR);
	}
}