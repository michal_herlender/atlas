package org.trescal.cwms.core.documents.birt.file;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.documents.birt.AbstractBirtTest;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentType;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.db.JobCostingService;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocales;
import org.trescal.cwms.tests.generic.DataSetPricing;

public class JobCostingFileTest extends AbstractBirtTest {
	
	@Autowired
	private JobCostingService jobCostingService;

	private static final String SUB_DIRECTORY = "Job Costing";
	private static final String FILE_NAME_REGULAR = "Sample Job Costing";
	
	@Override
	protected List<String> getDataSetFileNames() {
		List<String> result = DataSetPricing.getJobCostingTestData();
		return result;
	}

	private void test(int entityId, Locale locale) throws Exception {
		Map<String,Object> additionalParameters = new HashMap<>();
		additionalParameters.put(BaseDocumentDefinitions.PARAMETER_FACSIMILE, false);
		testDocument(entityId, BaseDocumentType.JOB_COSTING, locale, SUB_DIRECTORY, FILE_NAME_REGULAR, additionalParameters);
	}
	
	@Test
	@Transactional
	public void testFrance() throws Exception {
		jobCostingService.setAvalaraAware(false);
		test(110, SupportedLocales.FRENCH_FR);
	}
	
	@Test
	@Transactional
	public void testUnitedStates() throws Exception {
		jobCostingService.setAvalaraAware(true);
		test(150, SupportedLocales.ENGLISH_US);
	}
}
