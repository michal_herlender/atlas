package org.trescal.cwms.core.workflow.controller;

import io.vavr.Tuple;
import io.vavr.Tuple2;
import lombok.val;
import org.junit.jupiter.api.Test;
import org.trescal.cwms.core.procedure.dto.LaboratoryCategorySplit;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.workflow.entity.workassignment.dto.*;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.tests.MvcCwmsTestClass;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class ViewWorkForDepartmentTypeControllerTest extends MvcCwmsTestClass {

    @Override
    protected List<String> getDataSetFileNames() {
        return Stream.of(
                        "cwms/generic/company/CompanyClient.xml",
                        "cwms/generic/company/CompanyBusiness.xml",
                        "cwms/jobs/jobitem/WorkAssigmentCount.xml")
                .collect(Collectors.toList());
    }

    @Test
    void workAssigmentCountsTest() throws Exception {
        mockMvc.perform(get("/viewworkfordepttype.htm")
                        .sessionAttr(Constants.SESSION_ATTRIBUTE_SUBDIV, new KeyValue<>(510, "sub"))
                        .sessionAttr(Constants.SESSION_ATTRIBUTE_TIME_ZONE, TimeZone.getTimeZone("Pacific/Pago_Pago"))
                )
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("works"))
                .andExpect(model().attribute("works", getWorksExpectaions()))
        ;

    }

    private Map<Integer, WorkAssigmentCounters> getWorksExpectaions() {
        val uncategorised = Stream.of(WorkCountDay.values())
                .map(d -> {
                    switch (d) {
                        case DAY_BUSINESS:
                        case DAY_4:
                            return Tuple.of(d, 1);
                        case ALL:
                            return Tuple.of(d, 2);
                        default:
                            return Tuple.of(d, 0);
                    }
                }).collect(Collectors.toMap(Tuple2::_1, Tuple2::_2));
        val counter = Stream.of(WorkCountDay.values())
                .map(d -> {
                    switch (d) {
                        case DAY_OVERDUE:
                            return Tuple.of(d, 2);
                        case DAY_BUSINESS:
                            return Tuple.of(d, 3);
                        case DAY_5:
                            return Tuple.of(d, 4);
                        case ALL:
                            return Tuple.of(d, 13);
                        default:
                            return Tuple.of(d, 1);
                    }
                }).collect(Collectors.toMap(Tuple2::_1, Tuple2::_2, (u1, u2) -> u1, TreeMap::new));
        val workByDepatment = new TreeMap<>(Collections.singletonMap(510, WorkByDepartment.of(
                Collections.singletonMap(Tuple.of(1, LaboratoryCategorySplit.NONE), counter)
                , counter)));
        val labCounter =
                WorkAssigmentLaboratoryCounter.of(workByDepatment, uncategorised, counter);
        return Stream.of(
                Tuple.of(1, WorkAssigmentStateGroupsCounters.of(Collections.emptyMap())),
                Tuple.of(2, WorkAssigmentStateGroupsCounters.of(Collections.emptyMap())),
                Tuple.of(3, labCounter),
                Tuple.of(4, WorkAssigmentStateGroupsCounters.of(Collections.emptyMap())),
                Tuple.of(5, WorkAssigmentStateGroupsCounters.of(Collections.emptyMap())),
                Tuple.of(6, WorkAssigmentStateGroupsCounters.of(Collections.emptyMap())),
                Tuple.of(7, WorkAssigmentStateGroupsCounters.of(Collections.emptyMap()))
        ).collect(Collectors.toMap(Tuple2::_1, Tuple2::_2));
    }
}