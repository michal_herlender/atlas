package org.trescal.cwms.core.pricing.entity.purchaseorderstatus.db;

import java.text.SimpleDateFormat;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.pricing.purchaseorder.dto.SupplierPOProjectionDTO;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.db.PurchaseOrderDao;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.tests.MvcCwmsTestClass;
import org.trescal.cwms.tests.generic.DataSetPricing;
import org.trescal.cwms.tests.generic.company.TestDataCompany;
import org.trescal.cwms.tests.generic.company.TestDataSubdiv;
import org.trescal.cwms.tests.generic.job.TestDataPo;
import org.trescal.cwms.tests.generic.job.TestDataPoStatus;

public class PurchaseOrderDaoImplTest_getAllPurchaseOrdersAtStatusUsingProjection extends MvcCwmsTestClass {

	@Autowired
	private PurchaseOrderDao purchaseOrderDao;
	
	@Override
		protected List<String> getDataSetFileNames() {
		  return DataSetPricing.getSupplierPOTestData();
		}
	
	@Test
	public void testSupplierPO() {
		
		Integer statusId = TestDataPoStatus.STATUS_ID;
		Integer alloactedCompanyId = TestDataCompany.ID_BUSINESS_FRANCE;
		Integer subdivId = TestDataSubdiv.ID_BUSINESS_FRANCE;
		
		List<SupplierPOProjectionDTO> dtos = this.purchaseOrderDao.getAllPurchaseOrdersAtStatusUsingProjection(statusId, alloactedCompanyId, subdivId);
		Assert.assertEquals(TestDataPoStatus.PO_STATUS_SIZE,dtos.size());
		
		SupplierPOProjectionDTO dtoa = dtos.get(0);
		
		Assert.assertEquals(TestDataPo.PO_NO_FR,dtoa.getPono());
		Assert.assertEquals(TestDataPoStatus.COMP_NAME_FR,dtoa.getCompName());
		
		SimpleDateFormat df = DateTools.df_ISO8601;
		
		Assert.assertNotNull(dtoa.getRegdate());
		Assert.assertEquals(df.format(dtoa.getRegdate()), TestDataPoStatus.REG_DATE);
		
}
}
