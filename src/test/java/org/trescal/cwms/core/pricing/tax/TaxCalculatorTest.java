package org.trescal.cwms.core.pricing.tax;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.trescal.cwms.core.AtlasUnitTest;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnoteitem.CreditNoteItem;

@RunWith(MockitoJUnitRunner.class)
@Category(AtlasUnitTest.class)
public class TaxCalculatorTest {
	
	@Mock
	private MessageSource messageSource;
	
	@InjectMocks
	private TaxCalculator calculator = new TaxCalculator();
	
	@Test
	public void shouldCalculateCreditNoteTaxWithAtlasSettings() {
		CreditNote creditNote = new CreditNote();
		creditNote.setVatRate(new BigDecimal("8.50"));
		creditNote.setTotalCost(new BigDecimal("1000.00"));
		
		CreditNoteItem item = new CreditNoteItem();
		item.setFinalCost(new BigDecimal("1000.00"));
		
		calculator.calculateTaxWithAtlasSettings(creditNote);
		
		Assert.assertEquals(new BigDecimal("85.00"), creditNote.getVatValue());
	}
}
