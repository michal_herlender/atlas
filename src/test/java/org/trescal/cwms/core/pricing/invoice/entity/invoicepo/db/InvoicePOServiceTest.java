package org.trescal.cwms.core.pricing.invoice.entity.invoicepo.db;

import io.vavr.control.Either;
import lombok.val;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.trescal.cwms.core.pricing.invoice.dto.InvoicePoDto;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.db.InvoiceItemService;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoicePOItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.db.InvoiceItemPOService;
import org.trescal.cwms.core.pricing.invoice.entity.invoicepo.InvoicePO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@ExtendWith(MockitoExtension.class)
public class InvoicePOServiceTest {

    @Mock
    private InvoicePODao invoicePODao;

    @Mock
    private InvoiceItemPOService invoiceItemPOService;

    @Mock
    private InvoiceService invoiceService;

    @Mock
    private InvoiceItemService invoiceItemService;

    @InjectMocks
    private InvoicePOServiceImpl invoicePOServiceimpl;


    @Test
    @DisplayName("We would like to create Invoice PO")
    public void ajaxCreateInvoicePO() {

        Integer invoiceId = 111;
        String poNo = "111ABC";

        Invoice invoice = new Invoice();
        invoice.setId(invoiceId);
        invoice.setItems(Collections.emptySet());

        InvoicePO invoicePO = new InvoicePO();
        invoicePO.setInvoice(invoice);
        invoicePO.setPoNumber(poNo);

        Mockito.when(invoiceService.findInvoice(Mockito.eq(invoiceId))).thenReturn(invoice);

        Either<String, InvoicePoDto> invoicePoCreation = invoicePOServiceimpl.ajaxCreateInvoicePO(invoiceId, poNo);

        ArgumentCaptor<InvoicePO> argumentCaptor = ArgumentCaptor.forClass(InvoicePO.class);
        Mockito.verify(invoicePODao).persist(argumentCaptor.capture());
        InvoicePO invoicePOPersisted = argumentCaptor.getValue();

        assertEquals(
                invoicePoCreation,
                Either.right(
                        InvoicePoDto.of(invoicePOPersisted.getPoId()
                                , invoicePOPersisted.getPoNumber()))
        );

        assertEquals(invoiceId, invoicePOPersisted.getInvoice().getId());
        assertEquals(poNo, invoicePOPersisted.getPoNumber());
    }

    @Test
    @DisplayName("We would like to delete Invoice PO")
    public void deleteInvoicePO() {

        Integer poId = 111;

        InvoicePO invoicePO = new InvoicePO();
        invoicePO.setPoId(poId);
        invoicePO.setInvItemPOs(Collections.emptyList());

        Mockito.when(invoicePODao.find(Mockito.eq(poId))).thenReturn(invoicePO);

        Either<String, Integer> invoicePODeletion = invoicePOServiceimpl.deleteInvoicePO(poId);

        ArgumentCaptor<InvoicePO> argumentCaptor = ArgumentCaptor.forClass(InvoicePO.class);
        Mockito.verify(invoicePODao).remove(argumentCaptor.capture());
        InvoicePO invoicePODeleted = argumentCaptor.getValue();

        assertEquals(invoicePODeletion, Either.right(invoicePODeleted.getPoId()));

        val left = invoicePOServiceimpl.deleteInvoicePO(100);
        assertEquals(Either.left("The Invoice Po cannot be found for selected id."), left);

    }


    @Test
    @DisplayName("We would like to link invoiceitems to PO")
    public void linkItemsToPO() {

        Integer invoiceId = 111;
        Integer poId = 1;
        Integer invoiceItemId = 333;
        Integer invoiceItemId2 = 777;

        List<Integer> invoiceItemIds = new ArrayList<>(
                Arrays.asList(
                        333,
                        444,
                        555
                ));

        Invoice invoice = new Invoice();
        invoice.setId(invoiceId);

        InvoicePO invoicePO = new InvoicePO();
        invoicePO.setPoId(poId);
        invoicePO.setInvoice(invoice);
        invoicePO.setInvItemPOs(Collections.emptyList());

        val iipo = new InvoicePOItem();
        iipo.setInvPO(invoicePO);

        val ii = new InvoiceItem();
        ii.setId(invoiceItemId);
        ii.setInvItemPOs(Collections.emptySet());

        val ii2 = new InvoiceItem();
        ii2.setId(invoiceItemId2);
        ii2.setInvItemPOs(Collections.emptySet());

        List<InvoiceItem> invoiceItem = new ArrayList<>(
                Arrays.asList(
                        ii,
                        ii2
                )
        );

        Mockito.when(invoiceItemService.getAllByInvoiceAndJob(Mockito.eq(invoiceId), Mockito.eq(null)))
                .thenReturn(invoiceItem);

        Mockito.when(invoicePODao.find(Mockito.eq(poId))).thenReturn(invoicePO);

        Either<String, Boolean> linkItemsToPO = invoicePOServiceimpl.linkItemsToPO(invoiceId, poId, invoiceItemIds);

        ArgumentCaptor<InvoicePOItem> argumentCaptor = ArgumentCaptor.forClass(InvoicePOItem.class);
        Mockito.verify(invoiceItemPOService).updateInvoicePOItem(argumentCaptor.capture());
        InvoicePOItem linkedinvoiceItemPO = argumentCaptor.getValue();

        assertEquals(invoiceItemId, linkedinvoiceItemPO.getInvItem().getId());

        assertNotEquals(invoiceItemId2, linkedinvoiceItemPO.getInvItem().getId());

        assertEquals(linkItemsToPO, Either.right(true));

    }
}