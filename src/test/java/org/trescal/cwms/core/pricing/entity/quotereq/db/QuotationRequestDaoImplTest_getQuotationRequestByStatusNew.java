package org.trescal.cwms.core.pricing.entity.quotereq.db;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.quotation.dto.QuotationRequestProjectionDTO;
import org.trescal.cwms.core.quotation.entity.quotationrequest.QuotationRequestStatus;
import org.trescal.cwms.core.quotation.entity.quotationrequest.db.QuotationRequestDao;
import org.trescal.cwms.tests.MvcCwmsTestClass;
import org.trescal.cwms.tests.generic.DataSetPricing;
import org.trescal.cwms.tests.generic.company.TestDataSubdiv;
import org.trescal.cwms.tests.generic.pricing.TestDataQuotationReq;

public class QuotationRequestDaoImplTest_getQuotationRequestByStatusNew extends MvcCwmsTestClass {

	@Autowired
	private QuotationRequestDao quotationRequestDao;
	
	@Override
	protected List<String> getDataSetFileNames() {
		return  DataSetPricing.getQuotationTestData();
	}

	@Test
	public void testQuotateRequestByStatusNew() {
		
		Integer allocatedSubdivId = TestDataSubdiv.ID_CLIENT_FRANCE; 
		QuotationRequestStatus status = QuotationRequestStatus.REQUESTED;
		
		List<QuotationRequestProjectionDTO> dtos =this.quotationRequestDao.getQuotationRequestByStatusNew(status, allocatedSubdivId);
		Assert.assertEquals(TestDataQuotationReq.QUOTATIONS_REQ_SIZE, dtos.size());
		
		QuotationRequestProjectionDTO dtoa = dtos.get(0);
		
		Assert.assertEquals(dtoa.getId(), TestDataQuotationReq.ID_QUOTATIONS_REQ);
		Assert.assertEquals(dtoa.getRequestNo(), TestDataQuotationReq.REQUEST_NO_FR);
		Assert.assertEquals(dtoa.getQuotestatus(), TestDataQuotationReq.QUOTE_STATUS_);
		
		}
	}
