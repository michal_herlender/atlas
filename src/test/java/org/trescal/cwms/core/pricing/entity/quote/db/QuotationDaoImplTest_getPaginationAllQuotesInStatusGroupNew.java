package org.trescal.cwms.core.pricing.entity.quote.db;

import java.util.List;
import java.util.Locale;

import javax.transaction.Transactional;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.quotation.dto.QuotationProjectionDTO;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationDao;
import org.trescal.cwms.core.quotation.entity.quotestatusgroup.QuoteStatusGroup;
import org.trescal.cwms.core.quotation.form.NewQuotationSearchForm;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocales;
import org.trescal.cwms.tests.MvcCwmsTestClass;
import org.trescal.cwms.tests.generic.DataSetPricing;
import org.trescal.cwms.tests.generic.company.TestDataQuoteStatusGroupMember;
import org.trescal.cwms.tests.generic.company.TestDataSubdiv;
import org.trescal.cwms.tests.generic.pricing.TestDataQuotation;

public class QuotationDaoImplTest_getPaginationAllQuotesInStatusGroupNew extends MvcCwmsTestClass {

	@Autowired
    private QuotationDao quotationDao;
	
	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetPricing.getQuotationTestData();
	}

	
@Transactional
@Test
public void testQuoteStatusGroup() {
	
	QuoteStatusGroup group = QuoteStatusGroup.getQuoteStatusGroupById(TestDataQuoteStatusGroupMember.ID_GROUP);
	Integer allocatedSubdivId = TestDataSubdiv.ID_BUSINESS_FRANCE; 
	
	PagedResultSet<QuotationProjectionDTO> webQuotesRequiringAction = new PagedResultSet<QuotationProjectionDTO>(25, 1);
	NewQuotationSearchForm qsf = new NewQuotationSearchForm();
	
	qsf.setQuoteSubdivId(0);
	qsf.setQuoteContactId(0);
	
	Locale locale = SupportedLocales.FRENCH_FR;
	
	this.quotationDao.getPaginationAllQuotesInStatusGroupNew(group, allocatedSubdivId, webQuotesRequiringAction, qsf, locale);
	
	Assert.assertEquals(TestDataQuotation.QUOTATIONS_SIZE_FOR_GROUP_1, webQuotesRequiringAction.getResults().size());
	
	QuotationProjectionDTO dtoa = webQuotesRequiringAction.getResults().iterator().next();
	
	Assert.assertEquals(dtoa.getId(), TestDataQuoteStatusGroupMember.ID_QUOTE);
	Assert.assertEquals(dtoa.getQno(), TestDataQuotation.QNO1_FRANCE);
	Assert.assertEquals(dtoa.getQuotestatus(), TestDataQuoteStatusGroupMember.QUOTE_STATUS_TRANSLATION_2);
	Assert.assertEquals(dtoa.getFinalCost().intValue(), TestDataQuotation.FINAL_COST);

	}

}
