package org.trescal.cwms.core.pricing.entity.quote.db;

import java.util.List;
import java.util.Locale;

import javax.transaction.Transactional;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.quotation.dto.QuotationProjectionDTO;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationDao;
import org.trescal.cwms.core.quotation.form.NewQuotationSearchForm;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocales;
import org.trescal.cwms.tests.MvcCwmsTestClass;
import org.trescal.cwms.tests.generic.DataSetPricing;
import org.trescal.cwms.tests.generic.pricing.TestDataQuotation;

public class QuotationDaoImplTest_queryQuotationNew extends MvcCwmsTestClass {
	
	@Autowired
    private QuotationDao quotationDao;

	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetPricing.getQuotationTestData();
	}
	
	@Transactional
	@Test
	public void testQueryQuotationNew() {
		
		PagedResultSet<QuotationProjectionDTO> rs = new PagedResultSet<QuotationProjectionDTO>(25, 1);
		NewQuotationSearchForm qsf = new NewQuotationSearchForm();
		Locale locale = SupportedLocales.FRENCH_FR;
		
		qsf.setQuoteSubdivId(0);
		qsf.setQuoteContactId(0);
		
		
		this.quotationDao.queryQuotationNew(qsf, rs, locale);
		
		Assert.assertEquals(TestDataQuotation.QUOTATIONS_RESULT_SIZE, rs.getResults().size());
		
		QuotationProjectionDTO dtoa = rs.getResults().iterator().next();
		
		Assert.assertEquals(dtoa.getId(), TestDataQuotation.ID_QUOTATIONS_RESULT);
		Assert.assertEquals(dtoa.getQno(), TestDataQuotation.QNO3_FRANCE);
		Assert.assertEquals(dtoa.getClientref(), TestDataQuotation.CLIENT_REF_3);
		Assert.assertEquals(dtoa.getFinalCost().intValue(), TestDataQuotation.FINAL_COST);

		}
	}
