package org.trescal.cwms.core.pricing.lookup;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.pricing.lookup.dto.*;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetPricing;
import org.trescal.cwms.tests.generic.company.TestDataCompany;
import org.trescal.cwms.tests.generic.core.TestDataServiceType;
import org.trescal.cwms.tests.generic.core.TestDataSupportedCurrency;
import org.trescal.cwms.tests.generic.instrument.TestDataInstrument;
import org.trescal.cwms.tests.generic.pricing.TestDataQuotation;
import org.trescal.cwms.tests.generic.pricing.TestDataQuotationItem;
import org.trescal.cwms.tests.generic.pricing.TestDataQuotationItemCosts;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.samePropertyValuesAs;

/**
 * Tests various cases of price lookup service
 * Default case for price lookup service is for a job item search
 * By default, all search parameters for PriceLookupRequirements are for job item "goods in" when class is created
 */
public class PriceLookupServiceImplTest extends MvcRestTestClass {

	@Autowired
	private PriceLookupService priceLookupService;
	
	@Override
	protected List<String> getDataSetFileNames() {
		List<String> result = DataSetPricing.getQuotationTestData();
		// Want to include additional (expired, non-accepted, non-issued) quotations
		result.add(DataSetPricing.FILE_QUOTATION_ADDITIONAL);
		result.add(DataSetPricing.FILE_CATALOG_PRICE);
		return result;
	}
	
	@Test
	@Transactional
	public void testDefaultQuotationItemInstrumentSearch() throws Exception {
		// Test for French business company data
		int businessCompanyId = TestDataCompany.ID_BUSINESS_FRANCE; 
		int clientCompanyId = TestDataCompany.ID_CLIENT_FRANCE;
		
		PriceLookupRequirements requirements = new PriceLookupRequirements(businessCompanyId, clientCompanyId, true);

		PriceLookupInputFactory factory = new PriceLookupInputFactory(requirements);
		int serviceTypeId = TestDataServiceType.ID_AC_M_IL;
		int instrumentId = TestDataInstrument.ID_FRANCE_FLUKE_77_1102;

		PriceLookupInput input = factory.addInstrument(serviceTypeId, instrumentId);
		requirements.addInput(input);

		priceLookupService.findPrices(requirements);

		Assertions.assertEquals(1, input.getOutputs().size());
		PriceLookupOutput output = input.getOutputs().first();
		// instrumentModelId, salesCategoryId expected as null
		LocalDate issueDate = TestDataQuotation.DATE_ACTIVE_FRANCE_ISSUEDATE;
		Date expiryDate = DateTools.sqlserverformat.parse(TestDataQuotation.DATE_ACTIVE_FRANCE_EXPIRYDATE);
		boolean acceptedTrue = true;
		boolean issuedTrue = true;

		LocalDate expiryTimestamp = DateTools.dateToLocalDate(expiryDate);
		PriceLookupOutput expected = new PriceLookupOutputQuotationItem(
			TestDataServiceType.ID_AC_M_IL,
			TestDataInstrument.ID_FRANCE_FLUKE_77_1102, null, null,
			TestDataQuotationItem.ID_ACTIVE_FRANCE_FLUKE_77_1102,
			TestDataQuotationItemCosts.ID_CALCOST_QUOTATIONITEM_INSTRUMENT_1102,
			TestDataQuotationItem.VALUE_DISCOUNT_RATE_ACTIVE_INSTRUMENT_1102,
			TestDataQuotationItem.VALUE_TOTAL_COST_ACTIVE_INSTRUMENT_1102,
			TestDataQuotation.ID_ACTIVE_FRANCE,
			acceptedTrue,
			expiryTimestamp,
			issuedTrue,
			issueDate,
			TestDataSupportedCurrency.ID_EUR);
		// From post processing
		expected.setQueryType(PriceLookupQueryType.QUOTATION_ITEM_INSTRUMENT);
		assertThat(output, samePropertyValuesAs(expected));

	}

}
