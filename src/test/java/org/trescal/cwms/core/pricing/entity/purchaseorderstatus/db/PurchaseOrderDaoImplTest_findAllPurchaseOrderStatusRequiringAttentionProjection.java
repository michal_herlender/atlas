package org.trescal.cwms.core.pricing.entity.purchaseorderstatus.db;

import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderstatus.db.PurchaseOrderStatusDao;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderstatus.dto.PurchaseOrderStatusProjectionDTO;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocales;
import org.trescal.cwms.tests.MvcCwmsTestClass;
import org.trescal.cwms.tests.generic.DataSetPricing;
import org.trescal.cwms.tests.generic.job.TestDataPoStatus;

public class PurchaseOrderDaoImplTest_findAllPurchaseOrderStatusRequiringAttentionProjection extends MvcCwmsTestClass {
	
	@Autowired
	private PurchaseOrderStatusDao purchaseorderStatusDao;
	
	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetPricing.getSupplierPOTestData();
	}
	@Test
	public void testlocales() {
		Locale locale = SupportedLocales.FRENCH_FR;
		
		List<PurchaseOrderStatusProjectionDTO> dtos = this.purchaseorderStatusDao.findAllPurchaseOrderStatusRequiringAttentionProjection(locale);
		Assert.assertEquals(dtos.size(),TestDataPoStatus.PO_STATUS_SIZE);
		
		PurchaseOrderStatusProjectionDTO dtoa = dtos.get(0);
		
		Assert.assertEquals(dtoa.getName(),TestDataPoStatus.NAME_STATUS_2);
		if(dtoa.getNameTranslation() != null) {
			Assert.assertEquals(dtoa.getNameTranslation(),TestDataPoStatus.NAME_TRANSLATION_STATUS_2);
		}
		
		Assert.assertEquals(dtoa.getDescription(),TestDataPoStatus.DESCRIPTION_STATUS_2);
		if(dtoa.getDescTranslation() != null) {
			Assert.assertEquals(dtoa.getDescTranslation(),TestDataPoStatus.DESCRIPTION_TRANSLATION_STATUS_2);
		}
		Assert.assertEquals(dtoa.getRequireAttention(),TestDataPoStatus.REQUIRES_ATTETION);
		
}
}
