package org.trescal.cwms.core.pricing.entity.quote.db;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.quotation.dto.QuotationProjectionDTO;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationDao;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocales;
import org.trescal.cwms.tests.MvcCwmsTestClass;
import org.trescal.cwms.tests.generic.DataSetPricing;
import org.trescal.cwms.tests.generic.company.TestDataSubdiv;
import org.trescal.cwms.tests.generic.pricing.TestDataQuotation;

public class QuotationDaoImplTest_getMostRecentIssuedQuotationsNew extends MvcCwmsTestClass {
	
	@Autowired
    private QuotationDao quotationDao;

	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetPricing.getQuotationTestData();
	}
	@Test
	public void testQuoteStatusGroup() {
		
		Integer resultSize = TestDataQuotation.RESULT_SIZE;
		Integer allocatedSubdivId = TestDataSubdiv.ID_BUSINESS_FRANCE; 
		Locale locale = SupportedLocales.FRENCH_FR;
		
		List<QuotationProjectionDTO> dtos = this.quotationDao.getMostRecentIssuedQuotationsNew(resultSize, allocatedSubdivId, locale);
		Assert.assertEquals(TestDataQuotation.ISSUED_QUOTATION_SIZE, dtos.size());
		
		QuotationProjectionDTO dtoa = dtos.get(0);
		
		Assert.assertEquals(dtoa.getId(), TestDataQuotation.ID_QUOTATIONS_RESULT);
		Assert.assertEquals(dtoa.getFinalCost().intValue(),TestDataQuotation.FINAL_COST);
		
		SimpleDateFormat df = DateTools.df_ISO8601;
		
		Assert.assertNotNull(dtoa.getIssuedate());
		Assert.assertEquals(df.format(dtoa.getIssuedate()), TestDataQuotation.DATE_ISSUEDATE);
		}

	}