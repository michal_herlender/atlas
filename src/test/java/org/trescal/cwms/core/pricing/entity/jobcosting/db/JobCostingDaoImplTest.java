package org.trescal.cwms.core.pricing.entity.jobcosting.db;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.db.JobCostingDao;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetPricing;
import org.trescal.cwms.tests.generic.job.TestDataJobItem;
import org.trescal.cwms.tests.generic.pricing.TestDataJobCosting;

import java.util.List;

public class JobCostingDaoImplTest extends MvcRestTestClass {
	
	@Autowired
	private JobCostingDao jobCostingDao;

	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetPricing.getJobCostingTestData();
	}
	
	@Test
	public void shouldReturnJobCostingForJobItemIdAndVersion() {
		JobCosting jobCosting = this.jobCostingDao.getJobCosting(TestDataJobItem.ID_CLIENT_FR_1, TestDataJobCosting.VERSION_1);
		Assertions.assertNotNull(jobCosting);
		Assertions.assertEquals(TestDataJobCosting.ID_110_FR, jobCosting.getId());
		Assertions.assertEquals(TestDataJobCosting.VERSION_1, jobCosting.getVer());
	}
	
	@Test
	public void shouldReturnNullJobCostingWhenVersionNotFound() {
		JobCosting jobCosting = this.jobCostingDao.getJobCosting(TestDataJobItem.ID_CLIENT_FR_1, TestDataJobCosting.VERSION_2);
		Assertions.assertNull(jobCosting);
	}
	
	
}
