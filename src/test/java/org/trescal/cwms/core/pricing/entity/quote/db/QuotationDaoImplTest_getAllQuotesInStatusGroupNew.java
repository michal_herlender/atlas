package org.trescal.cwms.core.pricing.entity.quote.db;

import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.quotation.dto.QuotationProjectionDTO;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationDao;
import org.trescal.cwms.core.quotation.entity.quotestatusgroup.QuoteStatusGroup;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocales;
import org.trescal.cwms.tests.MvcCwmsTestClass;
import org.trescal.cwms.tests.generic.DataSetPricing;
import org.trescal.cwms.tests.generic.company.TestDataQuoteStatusGroupMember;
import org.trescal.cwms.tests.generic.company.TestDataSubdiv;
import org.trescal.cwms.tests.generic.pricing.TestDataQuotation;

public class QuotationDaoImplTest_getAllQuotesInStatusGroupNew extends MvcCwmsTestClass {
    
	@Autowired
    private QuotationDao quotationDao;
	
	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetPricing.getQuotationTestData();
	}
@Test
public void testQuoteStatusGroup() {
	
	QuoteStatusGroup group = QuoteStatusGroup.getQuoteStatusGroupById(TestDataQuoteStatusGroupMember.ID_GROUP);
	Integer allocatedSubdivId = TestDataSubdiv.ID_BUSINESS_FRANCE; 
	Locale locale = SupportedLocales.FRENCH_FR;
	
	List<QuotationProjectionDTO> dtos = this.quotationDao.getAllQuotesInStatusGroupNew(group, allocatedSubdivId, locale);
	
	Assert.assertEquals(TestDataQuotation.QUOTATIONS_SIZE_FOR_GROUP_1, dtos.size());
	
	QuotationProjectionDTO dtoa = dtos.get(0);
	
	Assert.assertEquals(dtoa.getId(), TestDataQuoteStatusGroupMember.ID_QUOTE);
	Assert.assertEquals(TestDataQuotation.QNO1_FRANCE,dtoa.getQno());
	Assert.assertEquals(dtoa.getQuotestatus(), TestDataQuoteStatusGroupMember.QUOTE_STATUS_TRANSLATION_2);
	Assert.assertEquals(TestDataQuotation.FINAL_COST,dtoa.getFinalCost().intValue());

	}

}
