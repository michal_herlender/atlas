package org.trescal.cwms.core.pricing.entity.invoice.db;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceDao;
import org.trescal.cwms.core.pricing.invoice.projection.InvoiceProjectionDTO;
import org.trescal.cwms.tests.MvcCwmsTestClass;
import org.trescal.cwms.tests.generic.DataSetPricing;
import org.trescal.cwms.tests.generic.company.TestDataCompany;
import org.trescal.cwms.tests.generic.pricing.TestDataInvoice;

public class InvoiceDaoImplTest_getInvoiceProjectionDTO extends MvcCwmsTestClass {
	
	@Autowired
	private InvoiceDao invoiceDao;

	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetPricing.getInvoiceTestData();
	}
	
	@Test
	public void testMultipleinvoiceIds() {
		Collection<Integer> invoiceIds = new ArrayList<>();
		invoiceIds.add(TestDataInvoice.INVOICE_ID_FR_110);
	
		List<InvoiceProjectionDTO> dtos = this.invoiceDao.getProjectionDtos(invoiceIds);
		Assert.assertEquals(invoiceIds.size(), dtos.size());
		
		InvoiceProjectionDTO dto1 = dtos.get(0);
		
		Assert.assertEquals(TestDataInvoice.INVOICE_ID_FR_110, dto1.getId());
		Assert.assertEquals(dto1.getInvoiceNumber(), TestDataInvoice.INVOICE_NUMBER_FR_110);
		Assert.assertEquals(dto1.getAllocatedCompanyId(), TestDataCompany.ID_BUSINESS_FRANCE);
		Assert.assertEquals(dto1.getCompanyId(), TestDataCompany.ID_CLIENT_FRANCE);
		Assert.assertEquals(dto1.getInvoiceDate(), TestDataInvoice.INVOICE_DATE_FR_110);
		
	}
}

