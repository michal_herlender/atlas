package org.trescal.cwms.core.pricing.entity.invoice.db;

import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.AtlasIntegrationTest;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.db.InvoiceItemDao;
import org.trescal.cwms.core.pricing.invoice.projection.invoiceitem.InvoiceItemProjectionDTO;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetPricing;
import org.trescal.cwms.tests.generic.pricing.TestDataExpenseItem;
import org.trescal.cwms.tests.generic.pricing.TestDataInvoice;
import org.trescal.cwms.tests.generic.pricing.TestDataInvoiceItem;

import java.util.List;

@Category(AtlasIntegrationTest.class)
public class InvoiceItemDaoImplTest extends MvcRestTestClass {
	
	@Autowired
	private InvoiceItemDao invoiceItemDao;
	
	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetPricing.getInvoiceTestData();
	}
	
	@Test
	public void shouldReturnInvoiceItemProjectionDTOs() throws Exception {
		List<InvoiceItemProjectionDTO> resultList = invoiceItemDao.getInvoiceItemProjectionDTOs(TestDataInvoice.INVOICE_ID_US_150);
		
		Assertions.assertEquals(TestDataInvoiceItem.RESULT_COUNT_INVOICE_US_150, resultList.size());
		
		InvoiceItemProjectionDTO dtoForExpenseItem = resultList.get(0);
		Assertions.assertEquals(TestDataInvoiceItem.ITEM_ID_US_1503, dtoForExpenseItem.getItemId());
		Assertions.assertEquals(TestDataInvoiceItem.ITEM_NUMBER_US_1503, dtoForExpenseItem.getItemNumber());
		Assertions.assertEquals(TestDataInvoiceItem.DESCRIPTION_US_1503, dtoForExpenseItem.getDescription());
		Assertions.assertEquals(TestDataInvoiceItem.QUANTITY_US_1503, dtoForExpenseItem.getQuantity());
		Assertions.assertEquals(TestDataInvoiceItem.FINAL_COST_US_1503, dtoForExpenseItem.getFinalCost());
		Assertions.assertEquals(TestDataInvoiceItem.TAXABLE_US_1503, dtoForExpenseItem.getTaxable());
		Assertions.assertEquals(TestDataInvoiceItem.TAX_AMOUNT_US_1503, dtoForExpenseItem.getTaxAmount());
		Assertions.assertEquals(TestDataInvoiceItem.NOMINAL_CODE_US_1503, dtoForExpenseItem.getNominalCodeId());
		Assertions.assertEquals(TestDataInvoiceItem.COST_TYPE_US_1503, dtoForExpenseItem.getNominalCostType());
		Assertions.assertNull(dtoForExpenseItem.getJobItemId());
		Assertions.assertEquals(TestDataExpenseItem.ITEM_ID_US_1503,dtoForExpenseItem.getExpenseItemId());
		Assertions.assertEquals(TestDataExpenseItem.ITEM_NUMBER_US_1503,dtoForExpenseItem.getExpenseItemNumber());
		Assertions.assertEquals(TestDataExpenseItem.JOB_ID_US_1503,dtoForExpenseItem.getExpenseItemJobId());
		Assertions.assertEquals(TestDataExpenseItem.MODEL_ID_US_1503,dtoForExpenseItem.getExpenseItemModelId());
		Assertions.assertEquals(TestDataExpenseItem.CLIENT_REF_US_1503,dtoForExpenseItem.getExpenseItemClientRef());

		InvoiceItemProjectionDTO dtoForJobItem = resultList.get(2);
		Assertions.assertEquals(TestDataInvoiceItem.ITEM_ID_US_15011, dtoForJobItem.getItemId());
		Assertions.assertEquals(TestDataInvoiceItem.ITEM_NUMBER_US_15011, dtoForJobItem.getItemNumber());
		Assertions.assertEquals(TestDataInvoiceItem.DESCRIPTION_US_15011, dtoForJobItem.getDescription());
		Assertions.assertEquals(TestDataInvoiceItem.QUANTITY_US_15011, dtoForJobItem.getQuantity());
		Assertions.assertEquals(TestDataInvoiceItem.FINAL_COST_US_15011, dtoForJobItem.getFinalCost());
		Assertions.assertEquals(TestDataInvoiceItem.TAXABLE_US_15011, dtoForExpenseItem.getTaxable());
		Assertions.assertEquals(TestDataInvoiceItem.TAX_AMOUNT_US_15011, dtoForExpenseItem.getTaxAmount());
		Assertions.assertEquals(TestDataInvoiceItem.NOMINAL_CODE_US_15011 , dtoForJobItem.getNominalCodeId());
		Assertions.assertEquals(TestDataInvoiceItem.COST_TYPE_US_15011 , dtoForJobItem.getNominalCostType());
		Assertions.assertEquals(TestDataInvoiceItem.JOB_ITEM_ID_US_15011 , dtoForJobItem.getJobItemId());
		Assertions.assertNull(dtoForJobItem.getExpenseItemId());
		Assertions.assertNull(dtoForJobItem.getExpenseItemNumber());
		Assertions.assertNull(dtoForJobItem.getExpenseItemJobId());
		Assertions.assertNull(dtoForJobItem.getExpenseItemModelId());
		Assertions.assertNull(dtoForJobItem.getExpenseItemClientRef());
	
	}
}
