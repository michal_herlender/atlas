package org.trescal.cwms.core.pricing.entity.creditnote.db;

import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.AtlasIntegrationTest;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnoteitem.db.CreditNoteItemDao;
import org.trescal.cwms.core.pricing.creditnote.projection.items.CreditNoteItemProjectionDTO;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetPricing;
import org.trescal.cwms.tests.generic.pricing.TestDataCreditNote;
import org.trescal.cwms.tests.generic.pricing.TestDataCreditNoteItem;

import java.util.List;

@Category(AtlasIntegrationTest.class)
public class CreditNoteItemDaoImplTest extends MvcRestTestClass {
	
	@Autowired
	private CreditNoteItemDao creditNoteItemDao;
	
	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetPricing.getCreditNoteTestData();
	}
	
	@Test
	public void shouldReturnCreditNoteItemProjectionDTOs() throws Exception {
		List<CreditNoteItemProjectionDTO> resultList = creditNoteItemDao.getCreditNoteItemProjectionDTOs(TestDataCreditNote.CREDIT_NOTE_ID_US_150);
		
		Assertions.assertEquals(TestDataCreditNoteItem.RESULT_COUNT_CREDIT_NOTE_US_150, resultList.size());
		
		CreditNoteItemProjectionDTO dtoForExpenseItem = resultList.get(0);
		Assertions.assertEquals(TestDataCreditNoteItem.ITEM_ID_US_1501, dtoForExpenseItem.getItemId());
		Assertions.assertEquals(TestDataCreditNoteItem.ITEM_NUMBER_US_1501, dtoForExpenseItem.getItemNumber());
		Assertions.assertEquals(TestDataCreditNoteItem.DESCRIPTION_US_1501, dtoForExpenseItem.getDescription());
		Assertions.assertEquals(TestDataCreditNoteItem.QUANTITY_US_1501, dtoForExpenseItem.getQuantity());
		Assertions.assertEquals(TestDataCreditNoteItem.FINAL_COST_US_1501, dtoForExpenseItem.getFinalCost());
		Assertions.assertEquals(TestDataCreditNoteItem.TAXABLE_US_1501, dtoForExpenseItem.getTaxable());
		Assertions.assertEquals(TestDataCreditNoteItem.TAX_AMOUNT_US_1501, dtoForExpenseItem.getTaxAmount());
		Assertions.assertEquals(TestDataCreditNoteItem.NOMINAL_CODE_US_1501, dtoForExpenseItem.getNominalCodeId());
		Assertions.assertEquals(TestDataCreditNoteItem.COST_TYPE_US_1501, dtoForExpenseItem.getNominalCostType());

		CreditNoteItemProjectionDTO dtoForJobItem = resultList.get(1);
		Assertions.assertEquals(TestDataCreditNoteItem.ITEM_ID_US_1502, dtoForJobItem.getItemId());
		Assertions.assertEquals(TestDataCreditNoteItem.ITEM_NUMBER_US_1502, dtoForJobItem.getItemNumber());
	}
}
