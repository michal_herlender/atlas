package org.trescal.cwms.web.instrument.controller;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.trescal.cwms.tests.MvcWebTestClass;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;
import java.util.List;

public class WebRecordUseControllerTest extends MvcWebTestClass {

    public final static String DATA_SET = "web/StandardsUsedForJobItemControllerTest.xml";
    
	@Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }
 
    @Test
    public void failureInstrumentNotFound() throws Exception {
        this.mockMvc.perform(put("/recordinstrmentuse.json/1000")
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.success").value(false))
                .andExpect(jsonPath("$.message").value("Instrument not found!"));
    }

    @Test
    public void failureInstrumentNotConfiguredForUses() throws Exception {
        this.mockMvc.perform(put("/recordinstrmentuse.json/1001")
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.success").value(false))
                .andExpect(jsonPath("$.message").value("Instrument does not support cal expiry based on number of uses!"));
    }

    @Test
    public void failureUsesUsedUp() throws Exception {
        this.mockMvc.perform(put("/recordinstrmentuse.json/100007")
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.success").value(false))
                .andExpect(jsonPath("$.message").value("Maximun number of permitted uses reached"));
    }

    @Test
    public void testIncrementUsedCountSuccess() throws Exception {
        this.mockMvc.perform(put("/recordinstrmentuse.json/100006")
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.success").value(true))
                .andExpect(jsonPath("$.permittedNumberOfUses").value(5))
                .andExpect(jsonPath("$.numberOfUsesSinceCal").value(3));
    }

}
