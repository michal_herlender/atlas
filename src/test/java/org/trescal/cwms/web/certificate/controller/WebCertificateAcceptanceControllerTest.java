package org.trescal.cwms.web.certificate.controller;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcWebTestClass;

import com.fasterxml.jackson.databind.ObjectMapper;

public class WebCertificateAcceptanceControllerTest extends MvcWebTestClass {

    public static final String DATA_SET = "web/CertificateAcceptanceControllerTest.xml";
    
	@Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }

    private static final String USERNAME = "eleonard";

    private ObjectMapper mapperObj = new ObjectMapper();

    @Test
    public void successfullyRetrieveValidationRecord() throws Exception {
        this.mockMvc.perform(get("/certificateacceptance/1001")
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.certificateNo").value("U1000-04"))
                .andExpect(jsonPath("$.status").value("Accepted"));
    }

    @Test
    public void validationRecordNotFound() throws Exception {
        this.mockMvc.perform(get("/certificateacceptance/101")
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.message").value("cert validation id-101"));
    }

    @Test
    public void successfullyCreateValidation() throws Exception {

        PostData postData = new PostData();
        postData.setCertificateId(102);
        postData.setDateValidated("04/03/2018");
        postData.setNote("test");
        postData.setStatus("rejected");

        String jsonString = mapperObj.writeValueAsString(postData);

        this.mockMvc.perform(post("/certificateacceptance/add")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(jsonString)
                .sessionAttr(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME)
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(header().string("location", containsString("/certificateacceptance/")))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.certificateNo").value("U1000-05"))
                .andExpect(jsonPath("$.status").value("Rejected"));;
    }

    @Test
    public void createFailsCertNotFound() throws Exception {

        PostData postData = new PostData();
        postData.setCertificateId(1002);
        postData.setDateValidated("04/03/2018");
        postData.setNote("test");
        postData.setStatus("ACCEPTED");

        String jsonString = mapperObj.writeValueAsString(postData);

        this.mockMvc.perform(post("/certificateacceptance/add")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(jsonString)
                .sessionAttr(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME)
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.message").value("cert id-1002"));

    }

    @Test
    public void createFailsStatusInvalid() throws Exception {

        PostData postData = new PostData();
        postData.setCertificateId(102);
        postData.setDateValidated("04/03/2018");
        postData.setNote("test");
        postData.setStatus("wibble");

        String jsonString = mapperObj.writeValueAsString(postData);

        this.mockMvc.perform(post("/certificateacceptance/add")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(jsonString)
                .sessionAttr(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME)
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.message").value(containsString("wibble is an invalid value. Supported values are ACCEPTED,REJECTED")));
    }

    @Test
    public void createFailsDateInvalid() throws Exception {

        PostData postData = new PostData();
        postData.setCertificateId(102);
        postData.setDateValidated("notadate");
        postData.setNote("test");
        postData.setStatus("accepted");

        String jsonString = mapperObj.writeValueAsString(postData);

        this.mockMvc.perform(post("/certificateacceptance/add")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(jsonString)
                .sessionAttr(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME)
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.message").value(containsString("Unparseable date")));
    }


    @Test
    public void successfullyCheckValidationOfCerts() throws Exception {
        int[] ids = new int[]{101,102,103,104,105};
        String jsonString = mapperObj.writeValueAsString(ids);

        this.mockMvc.perform(post("/certificateacceptance/bulkcheck")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(jsonString)
                .sessionAttr(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME)
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.notValidated").isArray())
                .andExpect(jsonPath("$.notValidated",hasSize(4)))
                .andExpect(jsonPath("$.validated").isArray())
                .andExpect(jsonPath("$.validated",hasSize(1)));
    }


    @Test
    public void bulkAcceptCertsSuccessful() throws Exception {
        int[] ids = new int[]{101,102,103,104,105};
        String jsonString = mapperObj.writeValueAsString(ids);

        this.mockMvc.perform(post("/certificateacceptance/bulkadd")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(jsonString)
                .sessionAttr(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME)
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$.[3]").value(105));
    }

    @Test
    public void testGetInstructions() throws Exception {

        this.mockMvc.perform(get("/certificateacceptance/instructions/105")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .sessionAttr(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME)
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray());
    }


    private class PostData {

        private Integer certificateId;
        private String dateValidated;
        private String note;
        private String status;

        public Integer getCertificateId() {
            return certificateId;
        }

        public void setCertificateId(Integer certificateId) {
            this.certificateId = certificateId;
        }

        public String getDateValidated() {
            return dateValidated;
        }

        public void setDateValidated(String dateValidated) {
            this.dateValidated = dateValidated;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }

}
