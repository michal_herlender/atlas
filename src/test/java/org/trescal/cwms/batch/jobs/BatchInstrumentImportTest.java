package org.trescal.cwms.batch.jobs;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobInstance;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.StepExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.trescal.cwms.batch.config.AutoSubmitPolicyEnum;
import org.trescal.cwms.batch.config.BatchConfig;
import org.trescal.cwms.batch.config.entities.itemanalysisresult.ItemAnalysisResult;
import org.trescal.cwms.batch.config.entities.itemanalysisresult.ItemAnalysisResultService;
import org.trescal.cwms.batch.jobs.importInstrument.InstrumentImportConfig;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatFieldNameEnum;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.tests.BatchBaseTestClass;

public class BatchInstrumentImportTest extends BatchBaseTestClass {

	@Autowired
	private ItemAnalysisResultService iarService;
	@Autowired
	private InstrumService instrumentService;
	@Autowired
	@Qualifier(InstrumentImportConfig.JOB_NAME)
	private Job instrumentImportjob;

	@Override
	protected List<String> getDataSetFileNames() {
		return Arrays.asList("GeneratedDataSet.xml", "GeneratedWorkflowDataSet.xml",
				"batch/instrumentImport/BatchInstrumentImportTestdataSet.xml");
	}

	@Override
	protected List<String> getFilesDataSetFileNames() {
		return Arrays.asList("batch/instrumentImport/BatchInstrumentImportTestFilesDataSet.xml");
	}

	@Override
	protected Job getJob() {
		return instrumentImportjob;
	}

	protected JobParameters defaultJobParameters() {
		JobParameters params = new JobParametersBuilder()
				.addLong(BatchConfig.BATCH_JOB_ID, System.currentTimeMillis(), true)
				.addLong(InstrumentImportConfig.CLIENT_SUBDIV_ID, 13729l)
				.addLong(InstrumentImportConfig.BUSINESS_SUBDIV_ID, 6951l)
				.addLong(InstrumentImportConfig.DEFAULT_SERVICETYPE_ID, 1l)
				.addLong(InstrumentImportConfig.DEFAULT_ADDRESS_ID, 21442l)
				.addLong(InstrumentImportConfig.DEFAULT_CONTACT_ID, 32615l).addLong(BatchConfig.EF_ID, 96l)
				.addLong(BatchConfig.EF_FILE_ID, 1282l).addString(BatchConfig.LOCALE, "en-GB")
				.addString(BatchConfig.AUTO_SUBMIT_POLICY, AutoSubmitPolicyEnum.SUBMIT_ONLY_ITEMS_WITHOUT_ERRORS.name())
				.addLong(InstrumentImportConfig.SUBMITED_BY, 38526l).toJobParameters();
		return params;
	}

	@Test
	public void testJob() throws Exception {

		JobExecution jobExecution = jobLauncherTestUtils.launchJob(defaultJobParameters());
		JobInstance actualJobInstance = jobExecution.getJobInstance();
		ExitStatus actualJobExitStatus = jobExecution.getExitStatus();

		assertEquals(actualJobInstance.getJobName(), InstrumentImportConfig.JOB_NAME);
		assertEquals(ExitStatus.COMPLETED.getExitCode(), actualJobExitStatus.getExitCode());

		assertFirstSuccessfulRow(actualJobExitStatus);

		assertSecondRowWithErrors(jobExecution);

	}

	private void assertFirstSuccessfulRow(ExitStatus actualJobExitStatus) {
		// test inserted instrument
		Instrument inst = instrumentService.getByOwningCompany(null, 11314, "2000", "123456").stream().findAny()
				.orElse(null);
		assertNotNull(inst);

		assertEquals("1 items inserted", actualJobExitStatus.getExitDescription());

	}

	private void assertSecondRowWithErrors(JobExecution jobExecution) {
		// get analysis step execution context
		StepExecution stepExec = jobExecution.getStepExecutions().stream()
				.filter(se -> se.getStepName().equals(BatchConfig.ANALYSIS_STEP)).findFirst().orElse(null);
		List<ItemAnalysisResult> analysisResults = iarService.getAllAnalysisResults(stepExec.getId().intValue(), 2, 2);

		// find TML instrument model id error
		ItemAnalysisResult tmlInstrumentModelIdError = analysisResults.stream()
				.filter(ar -> ar.getField().equals(ExchangeFormatFieldNameEnum.TML_INSTRUMENT_MODEL_ID)
						&& ar.getError().contains("Data not found"))
				.findAny().orElse(null);
		assertNotNull(tmlInstrumentModelIdError);

		// find instrument model id error
		ItemAnalysisResult InstrumentModelIdError = analysisResults.stream()
				.filter(ar -> ar.getField().equals(ExchangeFormatFieldNameEnum.INSTRUMENT_MODEL_ID)
						&& ar.getError().contains("Data not found"))
				.findAny().orElse(null);
		assertNotNull(InstrumentModelIdError);
	}

}
