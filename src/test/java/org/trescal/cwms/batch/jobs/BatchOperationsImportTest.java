package org.trescal.cwms.batch.jobs;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobInstance;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.StepExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.trescal.cwms.batch.config.AutoSubmitPolicyEnum;
import org.trescal.cwms.batch.config.BatchConfig;
import org.trescal.cwms.batch.config.entities.itemanalysisresult.ItemAnalysisResult;
import org.trescal.cwms.batch.config.entities.itemanalysisresult.ItemAnalysisResultService;
import org.trescal.cwms.batch.jobs.operationsimport.OperationsImportConfig;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatFieldNameEnum;
import org.trescal.cwms.tests.BatchBaseTestClass;
import org.trescal.cwms.tests.generic.DataSetBaseStatus;
import org.trescal.cwms.tests.generic.DataSetCapability;
import org.trescal.cwms.tests.generic.DataSetCore;
import org.trescal.cwms.tests.generic.DataSetInstrument;
import org.trescal.cwms.tests.generic.DataSetJob;

public class BatchOperationsImportTest extends BatchBaseTestClass {

	@Autowired
	@Qualifier(OperationsImportConfig.JOB_NAME)
	private Job operationsImportjob;
	@Autowired
	private ItemAnalysisResultService iarService;

	@Override
	protected Job getJob() {
		return this.operationsImportjob;
	}

	@Override
	protected List<String> getDataSetFileNames() {
		List<String> result = DataSetJob.getJobClientAwaitingCalibrationTestData();
		result.add(DataSetInstrument.FILE_INSTRUMENT_CLIENT_ADDITIONAL);	// Additional instruments not on open job
		result.add(DataSetCapability.FILE_PROCEDURE_SUBFAMILY);				// Additional subfamily capabilities and filters
		result.add(DataSetBaseStatus.FILE_STATUS_CALIBRATION);
		result.remove(DataSetCore.FILE_BASE_ITEM_STATE);
		result.add("GeneratedWorkflowDataSet.xml");
		result.add("batch/operationImport/BatchOperationImportTestdataSet.xml");
		return result;
	}

	@Override
	protected List<String> getFilesDataSetFileNames() {
		return Arrays.asList("batch/operationImport/BatchOperationImportTestFilesDataSet.xml");
	}

	private JobParameters defaultJobParameters_doNotSubmit_withErrorAndWarningMessages() {

		JobParameters params = new JobParametersBuilder()
				.addLong(BatchConfig.BATCH_JOB_ID, System.currentTimeMillis(), true)
				.addLong(OperationsImportConfig.JOB_ID, 96602l).addLong(BatchConfig.CLIENT_SUBDIV_ID, 13729l)
				.addLong(OperationsImportConfig.BUSINESS_SUBDIV_ID, 6951l).addLong(BatchConfig.EF_ID, 100l)
				.addLong(BatchConfig.EF_FILE_ID, 1283l).addString(BatchConfig.LOCALE, "en-GB")
				.addString(OperationsImportConfig.AUTO_CONTRACT_REVIEW, "true")
				.addString(OperationsImportConfig.AUTO_CREATE_JOBITEMS, "true")
				.addString(BatchConfig.AUTO_SUBMIT_POLICY, AutoSubmitPolicyEnum.DO_NOT_SUBMIT.name()).toJobParameters();

		return params;
	}
	
	private JobParameters defaultJobParameters_submit() {

		JobParameters params = new JobParametersBuilder()
				.addLong(BatchConfig.BATCH_JOB_ID, System.currentTimeMillis(), true)
				.addLong(OperationsImportConfig.JOB_ID, 152l).addLong(BatchConfig.CLIENT_SUBDIV_ID, 150l)
				.addLong(OperationsImportConfig.BUSINESS_SUBDIV_ID, 550l).addLong(BatchConfig.EF_ID, 100l)
				.addLong(BatchConfig.EF_FILE_ID, 1285l).addString(BatchConfig.LOCALE, "en-GB")
				.addString(OperationsImportConfig.AUTO_CONTRACT_REVIEW, "true")
				.addString(OperationsImportConfig.AUTO_CREATE_JOBITEMS, "true")
				.addString(BatchConfig.AUTO_SUBMIT_POLICY, AutoSubmitPolicyEnum.SUBMIT_IF_ALL_WITHOUT_ERRORS.name()).toJobParameters();

		return params;
	}
	
	@Test
	public void testJob_doNotSubmit_withErrorAndWarningMessages() throws Exception {

		JobExecution jobExecution = jobLauncherTestUtils
				.launchJob(defaultJobParameters_doNotSubmit_withErrorAndWarningMessages());
		JobInstance actualJobInstance = jobExecution.getJobInstance();
		ExitStatus actualJobExitStatus = jobExecution.getExitStatus();

		assertEquals(actualJobInstance.getJobName(), OperationsImportConfig.JOB_NAME);
		assertEquals(ExitStatus.COMPLETED.getExitCode(), actualJobExitStatus.getExitCode());

		assertRowWithErrors(jobExecution);

	}
	
	@Test
	public void testJob_submit() throws Exception {

		JobExecution jobExecution = jobLauncherTestUtils
				.launchJob(defaultJobParameters_submit());
		JobInstance actualJobInstance = jobExecution.getJobInstance();
		ExitStatus actualJobExitStatus = jobExecution.getExitStatus();

		assertEquals(actualJobInstance.getJobName(), OperationsImportConfig.JOB_NAME);
		assertEquals(ExitStatus.COMPLETED.getExitCode(), actualJobExitStatus.getExitCode());
		assertRowWithOnlyWarnings(jobExecution);
		assertSubmitStep(jobExecution);

	}
	
	private void assertRowWithErrors(JobExecution jobExecution) {
		// Task completed with 4 errors and 2 warnings for job
		// MA030CASJI19000703
		StepExecution stepExec = jobExecution.getStepExecutions().stream()
				.filter(se -> se.getStepName().equals(BatchConfig.ANALYSIS_STEP)).findFirst().orElse(null);

		List<ItemAnalysisResult> analysisResults = iarService.getAllAnalysisResults(stepExec.getId().intValue(), 1, 1);

		String instrumentNotFoundError = analysisResults.stream().filter(
				result -> result.getError() != null && result.getField().equals(ExchangeFormatFieldNameEnum.PLANT_NO))
				.findFirst().get().getError();

		String technicianNotFoundError = analysisResults.stream()
				.filter(result -> result.getError() != null
						&& result.getField().equals(ExchangeFormatFieldNameEnum.OPERATION_BY_HRID))
				.findFirst().get().getError();

		String instrumentNotFoundWarning = analysisResults.stream()
				.filter(result -> result.getWarning() != null
						&& result.getField().equals(ExchangeFormatFieldNameEnum.SERIAL_NUMBER))
				.findFirst().get().getWarning();

		assertEquals(stepExec.getStatus(), BatchStatus.COMPLETED);
		assertEquals(instrumentNotFoundError, "Instrument not found");
		assertEquals(technicianNotFoundError, "technician not found");
		assertEquals(instrumentNotFoundWarning, "Instrument not found");

	}

	private void assertRowWithOnlyWarnings(JobExecution jobExecution) {
		// Task completed with 0 errors and 1 warnings for job
		// MA030CASJI19000703
		StepExecution stepExec = jobExecution.getStepExecutions().stream()
				.filter(se -> se.getStepName().equals(BatchConfig.ANALYSIS_STEP)).findFirst().orElse(null);

		List<ItemAnalysisResult> analysisResults = iarService.getAllAnalysisResults(stepExec.getId().intValue(), 1, 1);

		List<ItemAnalysisResult> errorMessages = analysisResults.stream().filter(result -> result.getError() != null)
				.collect(Collectors.toList());

		String activeJobItemNotFoundWarning = analysisResults.stream()
				.filter(result -> result.getWarning() != null
						&& result.getField().equals(ExchangeFormatFieldNameEnum.ID_TRESCAL))
				.findFirst().get().getWarning();

		assertEquals(stepExec.getStatus(), BatchStatus.COMPLETED);
		assertTrue(errorMessages.isEmpty());
		assertEquals(activeJobItemNotFoundWarning, "Active job item not found for instrument barcode");

	}
	
	private void assertSubmitStep(JobExecution jobExecution) {
		// Task completed : 1 calibrations inserted into job USTUS-DTW-JI-19000152
		StepExecution stepExec = jobExecution.getStepExecutions().stream()
				.filter(se -> se.getStepName().equals(BatchConfig.SUBMIT_STEP)).findFirst().orElse(null);
		assertEquals(stepExec.getStatus(), BatchStatus.COMPLETED);
		assertEquals("1 calibrations inserted into job USTUS-DTW-JI-19000152", jobExecution.getExitStatus().getExitDescription());
	}
}
