package org.trescal.cwms.http;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Runs a single JUnit test, reading in Excel file and then testing every URL in each sheet of the file
 * For each sheet of the file, the column "PATH" is read and the columns e.g. "QUERYCOUNT" is the output
 * If a sheet doesn't have all columns, the sheet is skipped 
 *
 * To provide a username and password for running the test, edit the "Run Configurations" for the test
 * in Eclipse and in the VM arguments, enter "username" and "password" properties, for example:
 *
 *  -Dusername=GalenB -Dpassword=secretpassword
 *
 * @author galen
 *
 */
public class HtmlTestQueryCounts extends AbstractHtmlTest {

	public final static Logger log = LoggerFactory.getLogger(HtmlTestQueryCounts.class);

	private XSSFWorkbook wb;
	private int testCount;
	private int failureCount;
	private int successCount;

	@Test
	public void testQueryCounts() throws Exception {
		StopWatch sw = new StopWatch();
		sw.start();
		loadInputFile();
		processSheets();
		writeOutputFile();
		sw.stop();
		log.info("Total tests : " + testCount + ", successes : " + successCount + " failures : " + failureCount);
		log.info("Running time : " + sw.getTotalTimeSeconds()+ " seconds");
	}

	private void loadInputFile() throws Exception {
		File path = new File(QueryCountDefinitions.EXCEL_INPUT_PATH+File.separator+QueryCountDefinitions.EXCEL_INPUT_FILE);
		if (!path.exists()) Assert.fail("Input file doesn't exist : "+path.toString());
		FileInputStream inputStream = new FileInputStream(path);
		wb = new XSSFWorkbook(inputStream);
	}
	private void processSheets() {
		for (int sheetIndex=0; sheetIndex < wb.getNumberOfSheets(); sheetIndex++) {
			XSSFSheet sheet = wb.getSheetAt(sheetIndex);
			String sheetName = wb.getSheetName(sheetIndex);
			log.info("Processing sheet " + sheetIndex + " : " + sheetName);
			processSheet(sheet);
		}
	}
	private void processSheet(XSSFSheet sheet) {
		XSSFRow row = sheet.getRow(QueryCountDefinitions.ROW_INDEX_HEADER);
		if (row != null) {
			log.info("Processing row " + row.getRowNum());
			HtmlTestColumnIndex columnIndex = new HtmlTestColumnIndex();
			for (int colIndex = row.getFirstCellNum(); colIndex < row.getLastCellNum(); colIndex++) {
				XSSFCell cell = row.getCell(colIndex);
				if (cell != null && cell.getCellType() == Cell.CELL_TYPE_STRING) {
					switch (cell.getStringCellValue()) {
						case QueryCountDefinitions.COLUMN_HEADER_PATH:
							columnIndex.setColIndexPath(colIndex);
							break;
						case QueryCountDefinitions.COLUMN_HEADER_STATUS:
							columnIndex.setColIndexStatus(colIndex);
							break;
						case QueryCountDefinitions.COLUMN_HEADER_QUERYCOUNT:
							columnIndex.setColIndexQueryCount(colIndex);
							break;
						case QueryCountDefinitions.COLUMN_HEADER_QUERYTIME:
							columnIndex.setColIndexQueryTime(colIndex);
							break;
						case QueryCountDefinitions.COLUMN_HEADER_LOADTIME:
							columnIndex.setColIndexLoadTime(colIndex);
							break;
						case QueryCountDefinitions.COLUMN_HEADER_CONTENTLENGTH:
							columnIndex.setColIndexResponseLength(colIndex);
							break;
					}
				}
			}
			if (columnIndex.isAllColumnsDefined()) {
				// Headers found, process rows in sheet
				processRows(sheet, columnIndex);
			}
			else {
				log.info("Skipping sheet, header cells not found in row " + QueryCountDefinitions.ROW_INDEX_HEADER);
			}
		}
		else {
			log.info("Skipping sheet, no row " + QueryCountDefinitions.ROW_INDEX_HEADER);
		}
	}
	private void processRows(XSSFSheet sheet, HtmlTestColumnIndex columnIndex) {
		for (int rowIndex = sheet.getFirstRowNum(); rowIndex < sheet.getLastRowNum() + 1; rowIndex++) {
			if (rowIndex > QueryCountDefinitions.ROW_INDEX_HEADER) {
				log.info("Processing row " + rowIndex);
				XSSFRow row = sheet.getRow(rowIndex);
				XSSFCell cellPath = row.getCell(columnIndex.getColIndexPath(), Row.RETURN_BLANK_AS_NULL);
				if (cellPath != null && cellPath.getCellType() == Cell.CELL_TYPE_STRING) {
					String urlFragment = cellPath.getStringCellValue();
					HtmlTestStatistics statistics;
					try {
						statistics = testUrlWebRequest(urlFragment);
					} catch (Exception e) {
						log.error("ERROR");
						log.error(urlFragment);
						log.error(e.getMessage(), e);
						statistics = new HtmlTestStatistics();
						statistics.setSuccess(false);
						statistics.setErrorMessage(e.getMessage());
					}
					
					if (statistics.isSuccess()) {
						successCount++;
						setValue(row, columnIndex.getColIndexStatus(), QueryCountDefinitions.VALUE_OK);
					}
					else { 
						failureCount++;
						setValue(row, columnIndex.getColIndexStatus(), statistics.getErrorMessage());
					}
					setValue(row, columnIndex.getColIndexLoadTime(), statistics.getLoadTime());
					setValue(row, columnIndex.getColIndexQueryCount(), statistics.getQueryCount());
					setValue(row, columnIndex.getColIndexQueryTime(), statistics.getQueryTime());
					setValue(row, columnIndex.getColIndexResponseLength(), statistics.getContentLength());

					testCount++;
				}
			}			
		}
	}
	
	private void setValue(XSSFRow row, int columnIndex, Long value) {
		if (value != null) {
			XSSFCell cellResult = row.getCell(columnIndex, Row.CREATE_NULL_AS_BLANK);
			cellResult.setCellValue(value);
		}
		else {
			XSSFCell cell = row.getCell(columnIndex, Row.RETURN_NULL_AND_BLANK);
			if (cell != null) row.removeCell(cell);
		}
	}
	
	private void setValue(XSSFRow row, int columnIndex, String value) {
		if (value != null) {
			XSSFCell cellResult = row.getCell(columnIndex, Row.CREATE_NULL_AS_BLANK);
			cellResult.setCellValue(value);
		} else {
			XSSFCell cell = row.getCell(columnIndex, Row.RETURN_NULL_AND_BLANK);
			if (cell != null) row.removeCell(cell);
		}
	}

	private void writeOutputFile() {
		SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy - HH.mm.ss");

		StringBuilder outputFilePath = new StringBuilder(QueryCountDefinitions.EXCEL_OUTPUT_PATH);
		outputFilePath.append(File.separator);
		outputFilePath.append(QueryCountDefinitions.EXCEL_OUTPUT_PREFIX);
		outputFilePath.append(" ");
		outputFilePath.append(df.format(new Date()));
		outputFilePath.append(QueryCountDefinitions.EXCEL_OUTPUT_SUFFIX);

		try (FileOutputStream os = new FileOutputStream(outputFilePath.toString())) {
			wb.write(os);
			os.flush();
			os.close();
			log.info("Wrote output file to " + outputFilePath.toString());
		}
		catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}
}
