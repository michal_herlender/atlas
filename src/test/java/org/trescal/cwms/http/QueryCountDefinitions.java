package org.trescal.cwms.http;

/*
 * Definitions related to query testing
 */
public class QueryCountDefinitions {
	// Column header names searched in Excel file
	public static final String COLUMN_HEADER_PATH = "PATH";
	public static final String COLUMN_HEADER_STATUS = "STATUS";
	public static final String COLUMN_HEADER_QUERYCOUNT = "QUERYCOUNT";
	public static final String COLUMN_HEADER_QUERYTIME = "QUERYTIME";
	public static final String COLUMN_HEADER_LOADTIME = "LOADTIME";
	public static final String COLUMN_HEADER_CONTENTLENGTH = "SIZE";
	
	// Index of row with column headings
	public static final int ROW_INDEX_HEADER = 0;
	
	// Index of row with column headings
	public static final String VALUE_OK = "OK";
	
	// Paths for input and output files
	public static final String EXCEL_INPUT_PATH = "C:\\GroupERP\\Testing";
	public static final String EXCEL_OUTPUT_PATH = "C:\\GroupERP\\Testing";
	
	// File name for input file
	public static final String EXCEL_INPUT_FILE = "querycount.xlsx";
	public static final String EXCEL_OUTPUT_PREFIX = "querycount";
	public static final String EXCEL_OUTPUT_SUFFIX = ".xlsx";
}
