package org.trescal.cwms.http;

/**
 * Statistics holder for html testing
 * @author galen
 *
 */
public class HtmlTestStatistics {
	private boolean success;
	private Long queryCount;
	private Long queryTime;
	private Long loadTime;
	private Long contentLength;
	private String errorMessage;
	
	public boolean isSuccess() {
		return success;
	}
	public Long getQueryCount() {
		return queryCount;
	}
	public Long getQueryTime() {
		return queryTime;
	}
	public Long getLoadTime() {
		return loadTime;
	}
	public Long getContentLength() {
		return contentLength;
	}
	public String getErrorMessage() {
		return errorMessage;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}
	public void setQueryCount(Long queryCount) {
		this.queryCount = queryCount;
	}
	public void setQueryTime(Long queryTime) {
		this.queryTime = queryTime;
	}
	public void setLoadTime(Long loadTime) {
		this.loadTime = loadTime;
	}
	public void setContentLength(Long contentLength) {
		this.contentLength = contentLength;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
