package org.trescal.cwms.http;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.auth.UsernamePasswordCredentials;
import org.junit.Assert;
import org.junit.BeforeClass;

import com.gargoylesoftware.htmlunit.FormEncodingType;
import com.gargoylesoftware.htmlunit.HttpMethod;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.WebResponse;
import com.gargoylesoftware.htmlunit.util.NameValuePair;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import lombok.extern.slf4j.Slf4j;

/**
 * Base class which provides example how to perform OAuth2 authentication to Atlas 
 * 
 * The properties are supplied in VM arguments for each test / a test suite is run e.g.
 * 
 * -ea -Dusername=jane.smith -Dpassword=password1 -Dapp_username=myapp 
 * -Dapp_password=password2  -Dbaseurl=http://localhost:8080/cwms
 * 
 */

@Slf4j
public class AbstractOAuth2Test {

	// Configuration properties
	public static final String PROPERTY_BASE_URL = "baseurl";			// e.g. "http://localhost:8080/cwms" or address of external test system
	public static final String PROPERTY_USERNAME = "username";			// e.g. "jane.smith" your own Atlas account, or one for testing
	public static final String PROPERTY_PASSWORD = "password";			// e.g. "password1" your own Atlas password, or one for testing
	public static final String PROPERTY_APP_USERNAME = "app_username";	// e.g. "myapp" application username
	public static final String PROPERTY_APP_PASSWORD = "app_password";	// e.g. "password2" application password
	
	public static final int TIMEOUT_MS = 5000;							// Timeout in ms to use for all requests							
	
	public static final String ENDPOINT_OAUTH_TOKEN = "/oauth/token";	// Endpoint for OAuth authorization
	public static final String PARAM_ACCESS_TOKEN = "access_token";		// Request parameter name for access token (used in subclasses)
	public static final String PARAM_REFRESH_TOKEN = "refresh_token";	// Request parameter name for refresh token (used in subclasses)
	
	// Shared with subclasses for their own use after authorization 
	protected static WebClient webClient;
	protected static String accessToken;
	protected static String refreshToken;
	protected static String expires;
	
	@BeforeClass
	public static void login() throws Exception {
		// Performs authentication to get token before any test could be performed 
		
		// Obtain ERP information / credentials from VM configuration, including:
		String baseurl = System.getProperty(PROPERTY_BASE_URL);
		String username = System.getProperty(PROPERTY_USERNAME);
		String password = System.getProperty(PROPERTY_PASSWORD);
		String app_username = System.getProperty(PROPERTY_APP_USERNAME);
		String app_password = System.getProperty(PROPERTY_APP_PASSWORD);
		
		webClient = new WebClient();
		webClient.getOptions().setTimeout(TIMEOUT_MS);
		
		String authenticationUrl = baseurl+ENDPOINT_OAUTH_TOKEN;
		log.info("Authenticating to "+authenticationUrl);
		log.debug("username : "+username);
		log.debug("password : "+password);
		log.debug("app_username : "+app_username);
		log.debug("app_password : "+app_password);
		
		URL url = new URL(authenticationUrl);
		WebRequest request = new WebRequest(url);
		request.setHttpMethod(HttpMethod.POST);
		request.setEncodingType(FormEncodingType.URL_ENCODED);
		request.setCredentials(new UsernamePasswordCredentials(app_username, app_password));
		List<NameValuePair> requestParameters = new ArrayList<NameValuePair>(); 
		requestParameters.add(new NameValuePair("username", username));
		requestParameters.add(new NameValuePair("password", password));
		requestParameters.add(new NameValuePair("grant_type", "password"));
		requestParameters.add(new NameValuePair("scope", "all"));
		
		request.setRequestParameters(requestParameters);
		getPageAndStoreToken(request);
	}
	
	/**
	 * Used both here and in a subclass which tests the refresh token capability  
	 */
	protected static void getPageAndStoreToken(WebRequest request) throws Exception {
		Page page = webClient.getPage(request);
		WebResponse response = page.getWebResponse();
		if (response.getContentType().equals("application/json")) {
			String json = response.getContentAsString();
			 Map<String, String> map = new Gson().fromJson(json, new TypeToken<Map<String, String>>() {}.getType());
			 log.info(map.toString());
			 accessToken = map.get("access_token");
			 refreshToken = map.get("refresh_token");
			 expires = map.get("expires_in");
			 log.info("access_token : "+(accessToken == null ? "null" : accessToken));
			 log.info("refresh_token : "+(refreshToken == null ? "null" : refreshToken));
			 log.info("expires : "+(expires == null ? "null" : expires));
			 Assert.assertNotNull("access_token was null!", accessToken);
		}
		else {
			Assert.fail("Unexpected content type : "+response.getContentType());
		}	
	}
}
