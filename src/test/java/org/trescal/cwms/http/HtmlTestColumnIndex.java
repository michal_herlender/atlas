package org.trescal.cwms.http;

/*
 * Dynamic column header holder  
 */
public class HtmlTestColumnIndex {
	private Integer colIndexPath;
	private Integer colIndexStatus;
	private Integer colIndexQueryCount;
	private Integer colIndexQueryTime;
	private Integer colIndexLoadTime;
	private Integer colIndexResponseLength;
	
	
	public boolean isAllColumnsDefined() {
		return colIndexPath != null &&
				colIndexStatus != null &&
				colIndexQueryCount != null &&
				colIndexQueryTime != null &&
				colIndexLoadTime != null &&
				colIndexResponseLength != null;
	}
	
	public Integer getColIndexPath() {
		return colIndexPath;
	}
	public Integer getColIndexStatus() {
		return colIndexStatus;
	}
	public Integer getColIndexQueryCount() {
		return colIndexQueryCount;
	}
	public Integer getColIndexQueryTime() {
		return colIndexQueryTime;
	}
	public Integer getColIndexLoadTime() {
		return colIndexLoadTime;
	}
	public Integer getColIndexResponseLength() {
		return colIndexResponseLength;
	}
	public void setColIndexPath(Integer colIndexPath) {
		this.colIndexPath = colIndexPath;
	}
	public void setColIndexStatus(Integer colIndexStatus) {
		this.colIndexStatus = colIndexStatus;
	}
	public void setColIndexQueryCount(Integer colIndexQueryCount) {
		this.colIndexQueryCount = colIndexQueryCount;
	}
	public void setColIndexQueryTime(Integer colIndexQueryTime) {
		this.colIndexQueryTime = colIndexQueryTime;
	}
	public void setColIndexLoadTime(Integer colIndexLoadTime) {
		this.colIndexLoadTime = colIndexLoadTime;
	}
	public void setColIndexResponseLength(Integer colIndexResponseLength) {
		this.colIndexResponseLength = colIndexResponseLength;
	}

	
}
