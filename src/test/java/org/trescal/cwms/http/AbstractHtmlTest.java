package org.trescal.cwms.http;

import com.gargoylesoftware.htmlunit.*;
import com.gargoylesoftware.htmlunit.html.*;
import lombok.val;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import java.net.URL;
import java.util.List;

public abstract class AbstractHtmlTest {
	public static final String LOCALHOST_BASE_URL = "http://localhost:8080/cwms";
//	public static final String LOCALHOST_BASE_URL = "https://my.atlas.trescal.com";
//	public static final String LOCALHOST_BASE_URL = "https://esportal.trescal.com";
	public static final BrowserVersion BROWSER_VERSION = BrowserVersion.CHROME;
	public static final String LOGIN_URL = "/login.htm";
	public static final String LOGIN_USERNAME = "username";
	public static final String LOGIN_PASSWORD = "password";

	public static final String PROPERTY_USERNAME = "username";
	public static final String PROPERTY_PASSWORD = "password";

	public static final String SUBDIV_CHANGE_URL = "/changeallocatedsubdiv";
	public static final String SUBDIV_CHANGE_BODY = "allocatedSubdivid=6645";
	public static final Boolean TEST_QUERY_COUNT = true;

	public static final String ID_QUERY_COUNT = "querycounttotal";
	public static final String ID_QUERY_TIME = "querytime";

	protected static WebClient webClient;

	private static final Logger log = LoggerFactory.getLogger(AbstractHtmlTest.class);

	@BeforeClass
	public static void login() throws Exception {
		// ERP credentials should be in run configuration, including:
		String username = System.getProperty(PROPERTY_USERNAME);
		String password = System.getProperty(PROPERTY_PASSWORD);

		if (username == null)
			throw new IllegalArgumentException("System property " + PROPERTY_USERNAME + " was null, check VM config");
		if (password == null)
			throw new IllegalArgumentException("System property " + PROPERTY_PASSWORD + " was null, check VM config");

		webClient = new WebClient(BROWSER_VERSION);
		// For now, disabled as the ERP JS and CSS doens't all load / parse properly by HtmlUnit.
		// Also runs faster as we just fetch the main URLs this way
		webClient.getOptions().setJavaScriptEnabled(false);
		webClient.getOptions().setCssEnabled(false);

		final HtmlPage loginPage = webClient.getPage(LOCALHOST_BASE_URL + LOGIN_URL);
		final HtmlForm loginForm = loginPage.getForms().get(0);
		final HtmlTextInput inputName = loginForm.getInputByName(LOGIN_USERNAME);
		final HtmlPasswordInput inputPassword = loginForm.getInputByName(LOGIN_PASSWORD);

		inputName.setValueAttribute(username);
		inputPassword.setValueAttribute(password);

		final HtmlButton buttonSubmit = loginForm.getButtonByName("submit");
		final HtmlPage userHomePage = buttonSubmit.click();

		// After login we should be at the home page.
		log.info(userHomePage.getBaseURI());

		// Change logged in subdivision to Madrid for consistency		
		URL subdivUrl = new URL(LOCALHOST_BASE_URL + SUBDIV_CHANGE_URL);
		WebRequest webRequest = new WebRequest(subdivUrl, HttpMethod.POST);
		webRequest.setRequestBody(SUBDIV_CHANGE_BODY + csrfFormPart(userHomePage.getPage()));
		final HtmlPage madridHomePage = webClient.getPage(webRequest);

		// After subdiv change we should still be at the home page.
		log.info(madridHomePage.getBaseURI());
	}
	
	/*
	 * Full way of sending request, to get full WebResponse stats
	 * For now just using GET
	 */
	public HtmlTestStatistics testUrlWebRequest(String urlFragment) throws Exception {
		HtmlTestStatistics result = new HtmlTestStatistics();
		
		URL url = new URL(LOCALHOST_BASE_URL+urlFragment);
		WebRequest webRequest = new WebRequest(url, HttpMethod.GET);
		WebResponse webResponse = webClient.loadWebResponse(webRequest);
		
		result.setContentLength(webResponse.getContentLength());
		result.setLoadTime(webResponse.getLoadTime());
		
		if (webResponse.getStatusCode() == HttpStatus.OK.value()) {
			WebWindow webWindow = webClient.getCurrentWindow(); 
			
			Page page = webClient.getPageCreator().createPage(webResponse, webWindow);
			if (page.isHtmlPage()) {
				final HtmlPage htmlPage = (HtmlPage) page;
				if (TEST_QUERY_COUNT) {
					// normally used for development testing
					HtmlSpan spanQueryCount = htmlPage.getHtmlElementById(ID_QUERY_COUNT);
					HtmlSpan spanQueryTime = htmlPage.getHtmlElementById(ID_QUERY_TIME);
					
					Long queryCount = parseAsLong(spanQueryCount.getTextContent());
					Long queryTime = parseAsLong(spanQueryTime.getTextContent());
					
					result.setQueryCount(queryCount);
					result.setQueryTime(queryTime);
					result.setSuccess(queryCount != null && queryTime != null);
				}
				else {
					// e.g. production testing; just test for footer
					List<Object> footers = htmlPage.getByXPath("//div[@class=\"footer\"]");
					result.setSuccess(footers != null && footers.size() == 1);
				}
			}
			else {
				result.setSuccess(false);
				result.setErrorMessage("Non html page");
			}
		}
		else {
			result.setSuccess(false);
			result.setErrorMessage("Http status : "+webResponse.getStatusCode());
		}
		
		return result;
	}

	private Long parseAsLong(String text) {
		Long result = null;
		try {
			result = Long.valueOf(text);
		}
		catch (NumberFormatException e) {
			// returns null
		}
		return result;
	}

	/*
	 * Alternate basic way of sending request, just with query count stat as result
	 */
	public String testUrlWebClient(String urlFragment) throws Exception {
		final HtmlPage htmlPage = webClient.getPage(LOCALHOST_BASE_URL + urlFragment);
		HtmlSpan span = htmlPage.getHtmlElementById(ID_QUERY_COUNT);
		return span.getTextContent();
	}

	public static String csrfFormPart(HtmlPage page) {
		val csrfToken = page.querySelector("meta[name='_csrf']").getAttributes().getNamedItem("content").getNodeValue();
		return "&_csrf=" + csrfToken;
	}
}
