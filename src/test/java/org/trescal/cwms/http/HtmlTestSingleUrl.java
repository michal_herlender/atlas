package org.trescal.cwms.http;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simple demo class as proof of concept 
 * The ERP Tomcat VM should be up and running locally 
 *
 * To provide a username and password for running the test, edit the "Run Configurations" for the test
 * in Eclipse and in the VM arguments, enter "username" and "password" properties, for example:
 *
 *  -Dusername=GalenB -Dpassword=secretpassword
 *
 * @author galen
 *
 */
public class HtmlTestSingleUrl extends AbstractHtmlTest {

	public static final String TEST_URL = "/quoteform.htm?personid=24349";

	private static final Logger log = LoggerFactory.getLogger(HtmlTestSingleUrl.class);

	@Test
	public void testPageWebClient() throws Exception {
		String result = testUrlWebClient(TEST_URL);
		log.info(result);
	}

	@Test
	public void testPageViaWebRequest() throws Exception {
		HtmlTestStatistics result = testUrlWebRequest(TEST_URL);
		log.info("Content length : " + result.getContentLength());
		log.info("Load time : " + result.getLoadTime());
		log.info("Query count : " + result.getQueryCount());
		log.info("Query time : " + result.getQueryTime());
	}

}
