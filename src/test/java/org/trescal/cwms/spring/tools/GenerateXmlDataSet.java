package org.trescal.cwms.spring.tools;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

import org.dbunit.database.AmbiguousTableNameException;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.ext.mssql.MsSqlDataTypeFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class GenerateXmlDataSet {

	private static final Logger logger = LoggerFactory.getLogger(GenerateXmlDataSet.class);

	private static final String FILE_PATH = "./src/test/resources/test-data/";
	private static final String FILE_NAME = "GeneratedDataSet";
	public static final String PROPERTIES_FILE = "properties/atlas-develop.properties";
	private String dbDriver;
	private String dbName;
	private String dbUser;
	private String dbPassword;

	public GenerateXmlDataSet() {
		Properties properties = new Properties();
		try (InputStream is = this.getClass().getClassLoader().getResourceAsStream(PROPERTIES_FILE)) {
			if (is == null)
				throw new RuntimeException("Could not load resource : "+PROPERTIES_FILE);
			properties.load(is);
			this.dbDriver = properties.getProperty("db.driver");
			this.dbName = properties.getProperty("db.name");
			this.dbUser = properties.getProperty("db.user");
			this.dbPassword = properties.getProperty("db.password");
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			System.exit(-1);
		}
	}

	private QueryDataSet connectTodatabase() throws Exception {
		@SuppressWarnings("unused")
		Class<?> driverClass = Class.forName(dbDriver);
		Connection jdbcConnection = null;
		logger.info("connecting to " + dbName);
		jdbcConnection = DriverManager.getConnection(dbName, dbUser, dbPassword);
		logger.info("Connected database successfully...");
		IDatabaseConnection connection = new DatabaseConnection(jdbcConnection);
		DatabaseConfig config = connection.getConfig();
		config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MsSqlDataTypeFactory());
		return new QueryDataSet(connection);
	}

	abstract protected void defineTables(QueryDataSet dataSet) throws AmbiguousTableNameException;

	public void generate() throws Exception {
		QueryDataSet dataSet = connectTodatabase();
		defineTables(dataSet);
		FileOutputStream fos = new FileOutputStream(getPath() + getFilename() + ".xml");
		FlatXmlDataSet.write(dataSet, fos);
	}

	protected String getFilename() {
		return GenerateXmlDataSet.FILE_NAME;
	}

	protected String getPath() {
		return GenerateXmlDataSet.FILE_PATH;
	}

}
