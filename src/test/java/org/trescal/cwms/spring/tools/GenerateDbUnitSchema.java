package org.trescal.cwms.spring.tools;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatDtdWriter;
import org.dbunit.ext.mssql.MsSqlDataTypeFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Standalone utility to generate DBUnit DTD for the primary database schema
 * This can possibly replace the DbMaintain one.
 * 
 * @author Galen 2017-08-30
 * 
 *         Validation of XML test data then works with:
 * 
 *         <!DOCTYPE dataset SYSTEM "file:///C:/cwms/dbscripts/dbunit.dtd" >
 *         with all elements contained in a <dataset></dataset> definition
 */
public class GenerateDbUnitSchema {

	private String dbDriver;
	private String dbName;
	private String dbName2;
	private String dbUser;
	private String dbPassword;

	private static final Logger logger = LoggerFactory.getLogger(GenerateDbUnitSchema.class);
	public static final String PROPERTIES_FILE = "test.properties";
	public static final String OUTPUT_PATH = "C:\\cwms\\dbscripts";
	public static final String OUTPUT_FILE = "dbunit.dtd";
	public static final String OUTPUT_FILE2 = "dbunit2.dtd";

	/*
	 * Attempted to output schema DTD in src/test/java/org/trescal/cwms for relative
	 * reference by XML files e.g. with SYSTEM and relative doc location using ../..
	 * - validates OK in Eclipse but DTD not found when running tests. OUTPUT_PATH =
	 * "src/test/java/org/trescal/cwms"; OUTPUT_FILE = "cwms_test.dtd"
	 */

	public GenerateDbUnitSchema() {
		Properties properties = new Properties();
		try (InputStream is = this.getClass().getClassLoader().getResourceAsStream(PROPERTIES_FILE)) {
			if (is == null)
				throw new RuntimeException("Could not load resource : "+PROPERTIES_FILE);
			properties.load(is);
			this.dbDriver = properties.getProperty("db.driver");
			this.dbName = properties.getProperty("db.name");
			this.dbName2 = properties.getProperty("db2.name");
			this.dbUser = properties.getProperty("db.user");
			this.dbPassword = properties.getProperty("db.password");
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	public void execute() throws Exception {
		// Initializing driver class using 'old' backwards compatible way for
		// DriverManager
		@SuppressWarnings("unused")
		Class<?> driverClass = Class.forName(dbDriver);
		Connection jdbcConnection = DriverManager.getConnection(dbName, dbUser, dbPassword);
		IDatabaseConnection connection = new DatabaseConnection(jdbcConnection);
		DatabaseConfig config = connection.getConfig();
		config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MsSqlDataTypeFactory());
		IDataSet dataSet = connection.createDataSet();

		// write DTD file from db
		String pathname = OUTPUT_PATH.replace("/", File.separator) + File.separator + OUTPUT_FILE;
		File file = new File(pathname);
		logger.info("Writing DTD to " + file.getAbsolutePath());

		try (Writer out = new OutputStreamWriter(new FileOutputStream(file))) {
			FlatDtdWriter datasetWriter = new FlatDtdWriter(out);
			// The FlatDtdWriter.SEQUENCE model is the default
			// The CHOICE appears to allow us
			datasetWriter.setContentModel(FlatDtdWriter.CHOICE);
			datasetWriter.write(dataSet);
		}
		logger.info("Wrote DTD to " + file.getAbsolutePath());

		// files db connection
		Connection jdbcConnection2 = DriverManager.getConnection(dbName2, dbUser, dbPassword);
		IDatabaseConnection connection2 = new DatabaseConnection(jdbcConnection2);
		DatabaseConfig config2 = connection2.getConfig();
		config2.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MsSqlDataTypeFactory());
		IDataSet dataSet2 = connection2.createDataSet();

		// write DTD file from files db
		String pathname2 = OUTPUT_PATH.replace("/", File.separator) + File.separator + OUTPUT_FILE2;
		File file2 = new File(pathname2);
		logger.info("Writing DTD to " + file2.getAbsolutePath());

		try (Writer out = new OutputStreamWriter(new FileOutputStream(file2))) {
			FlatDtdWriter datasetWriter = new FlatDtdWriter(out);
			// The FlatDtdWriter.SEQUENCE model is the default
			// The CHOICE appears to allow us
			datasetWriter.setContentModel(FlatDtdWriter.CHOICE);
			datasetWriter.write(dataSet2);
		}
		logger.info("Wrote DTD to " + file2.getAbsolutePath());
	}

	public static void main(String[] args) {
		try {
			(new GenerateDbUnitSchema()).execute();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
}
