package org.trescal.cwms.spring.tools;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.util.EnumSet;

import org.hibernate.boot.Metadata;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.schema.TargetType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This utility creates SQL scripts to perform a full create of tables in the integration test databases:
 * 
 * cwms_test
 * cwmsfiles_test
 * 
 * Normally, these databases should exist/ be created first in SQL management studio, 
 * and this should run one time to set up the tables before running any integration tests. 
 * 
 * The utility uses Hibernate's SchemaUpdate tool to generate (but not run) the creation scripts, 
 * and then modifies the SQL statements to:
 * a) Remove not null constraints
 * 
 * This matches the strategy used up until Hibernate 4 with hbm2ddl.auto = update and Unitils.disableConstraints
 * and therefore allows Unitils to be removed from the codebase while still being able to configure the test
 * database in its original manner.  
 * 
 * @author Galen
 * 2017-08-29
 */
public class CreateTestTables extends SchemaTool {
	private static final String FILENAME_PRIMARY = "cwms_test-create.sql"; 
	private static final String FILENAME_SECONDARY = "cwms_testfiles-create.sql";
	private static final String FILENAME_TEMP = "temp.sql"; 
	private static final Logger logger = LoggerFactory.getLogger(CreateTestTables.class);

	private String tempFilePath;
	
	public CreateTestTables() {
		super(false, FILENAME_PRIMARY, FILENAME_SECONDARY);
		tempFilePath = SchemaConstants.SCRIPT_DIRECTORY + File.separatorChar + FILENAME_TEMP;
	}
	
	public static void main(String[] args) {
		(new CreateTestTables()).execute();
	}

	@Override
	protected void performTask(Metadata metadata, String database, String outputFilePath) {
		SchemaExport export = getSchemaExport(tempFilePath);
		// Not running on database TargetType.DATABASE
		EnumSet<TargetType> targetTypes = EnumSet.of(TargetType.SCRIPT, TargetType.STDOUT);
		SchemaExport.Action action = SchemaExport.Action.CREATE;
		export.execute(targetTypes, action, metadata);
		System.out.println("Create script written by Hibernate's Schema Export as temp file");
		createFinalScript(database, outputFilePath);
		System.out.println("Create script written without NOT NULL and FOREIGN constraints");
	}

	/**
	 * The file format starts with create table... statements and 
	 * finishes with alter table... statements.    
	 * For simplicity, we copy / process lines until the first alter table statement. 
	 */
	private void createFinalScript(String database, String outputFilePath) {
		super.deleteFileIfExists(outputFilePath);
		super.initialiseFile(database, outputFilePath);
		try {
			LineNumberReader reader = new LineNumberReader(new FileReader(tempFilePath));
			FileOutputStream os = new FileOutputStream(outputFilePath, true);
			PrintWriter pw = new PrintWriter(os, true);
			String currentLine = reader.readLine();
			boolean foundAlterTable = false;
			while ((currentLine != null) && !foundAlterTable) {
				foundAlterTable = currentLine.contains("alter table");
				if (!foundAlterTable) {
					if (currentLine.contains(" not null") && !currentLine.contains("identity not null")) {
						pw.println(currentLine.replace(" not null", ""));
					}
					else {
						pw.println(currentLine);
					}
				}
				currentLine = reader.readLine();
			}
			if (foundAlterTable) {
				logger.info("First 'alter table' statement found on line "+reader.getLineNumber());
			}
			else {
				logger.info("No 'alter table' statements found");
			}
			reader.close();
			os.flush();
			os.close();
			super.deleteFileIfExists(tempFilePath);
		}
		catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
		super.finaliseFile(outputFilePath);
	}

}