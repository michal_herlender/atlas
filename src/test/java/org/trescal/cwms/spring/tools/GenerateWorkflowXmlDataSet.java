package org.trescal.cwms.spring.tools;

import java.io.IOException;

import org.dbunit.database.AmbiguousTableNameException;
import org.dbunit.database.QueryDataSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GenerateWorkflowXmlDataSet extends GenerateXmlDataSet {

	private static final Logger logger = LoggerFactory.getLogger(GenerateWorkflowXmlDataSet.class);

	@Override
	protected void defineTables(QueryDataSet dataSet) throws AmbiguousTableNameException {
		dataSet.addTable("itemstate");
		dataSet.addTable("itemstatetranslation");
		dataSet.addTable("nextactivity");
		dataSet.addTable("lookupinstance");
		dataSet.addTable("actionoutcome");
		dataSet.addTable("actionoutcometranslation");
		dataSet.addTable("lookupresult", "select id, lookupid, outcomestatusid, resultmessage from lookupresult");
		dataSet.addTable("outcomestatus");
		dataSet.addTable("hook");
		dataSet.addTable("hookactivity");
		dataSet.addTable("stategrouplink");
	}

	@Override
	protected String getFilename() {
		return "GeneratedWorkflowDataSet";
	}

	public static void main(String[] args) throws IOException {
		try {
			new GenerateWorkflowXmlDataSet().generate();
			logger.info("DataSet Created successfully");
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

}
