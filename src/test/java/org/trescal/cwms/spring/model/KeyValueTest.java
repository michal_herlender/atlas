package org.trescal.cwms.spring.model;

import java.util.ArrayList;
import java.util.TreeSet;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class KeyValueTest {
	private KeyValue<Integer, String> key1US;
	private KeyValue<Integer, String> key1Duplicate;
	private KeyValue<Integer, String> key1USA;
	private KeyValue<Integer, String> key2US;
	private KeyValue<Integer, String> key3Spain;
	@Before
	public void setup() {
		key1US = new KeyValue<>(1, "Trescal US");
		key1Duplicate = new KeyValue<>(1, "Trescal US");
		key1USA = new KeyValue<>(1, "Trescal USA");
		key2US = new KeyValue<>(2, "Trescal US");
		key3Spain = new KeyValue<>(3, "Trescal Spain");
	}
	
	@Test
	public void testAccessors() {
		KeyValue<Integer, String> key = new KeyValue<>(3, "Trescal France"); 
		Assert.assertEquals(new Integer(3), key.getKey());
		Assert.assertEquals("Trescal France", key.getValue());
		key.setKey(4);
		key.setValue("Trescal Germany");
		Assert.assertEquals(new Integer(4), key.getKey());
		Assert.assertEquals("Trescal Germany", key.getValue());
	}

	@Test
	public void testEquals_true() {
		Assert.assertTrue(key1US.equals(key1US));
		Assert.assertTrue(key1US.equals(key1Duplicate));
	}
	
	@Test
	public void testEquals_false() {
		Assert.assertFalse(key1US.equals(key1USA));
		Assert.assertFalse(key1US.equals(key2US));
		Assert.assertFalse(key1US.equals(key3Spain));
	}

	@Test
	public void testCompareTo_zero() {
		Assert.assertEquals(0, key1US.compareTo(key1US));
		Assert.assertEquals(0, key1US.compareTo(key1Duplicate));
	}
	
	/*
	 * Expected ordering of keys - by value then by key so:
	 * Trescal Spain - 3
	 * Trescal US - 1
	 * Trescal US - 2
	 * Trescal USA - 1
	 */
	@Test
	public void testCompareTo_positive() {
		Assert.assertTrue(key1US.compareTo(key3Spain) > 0);
		Assert.assertTrue(key2US.compareTo(key3Spain) > 0);
		Assert.assertTrue(key1USA.compareTo(key3Spain) > 0);
		
		Assert.assertTrue(key2US.compareTo(key1US) > 0);
		Assert.assertTrue(key1USA.compareTo(key1US) > 0);
	}

	@Test
	public void testCompareTo_negative() {
		Assert.assertTrue(key3Spain.compareTo(key1US) < 0);
		Assert.assertTrue(key3Spain.compareTo(key2US) < 0);
		Assert.assertTrue(key3Spain.compareTo(key1USA) < 0);
		
		Assert.assertTrue(key1US.compareTo(key2US) < 0);
		Assert.assertTrue(key1US.compareTo(key1USA) < 0);
	}
	@Test
	public void testHashCode_same() {
		Assert.assertEquals(key1US.hashCode(), key1Duplicate.hashCode());
	}
	
	@Test
	public void testHashCode_different() {
		Assert.assertNotEquals(key1US.hashCode(), key1USA.hashCode());
		Assert.assertNotEquals(key1US.hashCode(), key2US.hashCode());
		Assert.assertNotEquals(key1US.hashCode(), key3Spain.hashCode());
	}

	@Test
	public void testToString() {
		Assert.assertEquals("1: Trescal US", key1US.toString());
	}
	
	@Test
	public void testSetOrdering() {
		TreeSet<KeyValue<Integer, String>> sortedSet = new TreeSet<>();
		sortedSet.add(key1US);
		sortedSet.add(key1US);
		sortedSet.add(key1Duplicate);
		Assert.assertEquals(1, sortedSet.size());
		
		sortedSet.add(key1USA);
		sortedSet.add(key2US);
		sortedSet.add(key3Spain);
		Assert.assertEquals(4, sortedSet.size());
		
		ArrayList<KeyValue<Integer, String>> expectedOrder = new ArrayList<>();
		expectedOrder.add(key3Spain);
		expectedOrder.add(key1US);
		expectedOrder.add(key2US);
		expectedOrder.add(key1USA);
		
		Assert.assertArrayEquals(expectedOrder.toArray(), sortedSet.toArray());
	}
}