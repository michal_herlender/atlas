package org.trescal.cwms.spring.matchers;

/**
 * Galen Beck - 2019-01-25
 * Taken from:
 * https://stackoverflow.com/questions/34479906/how-to-extract-explicit-content-from-org-springframework-test-web-servlet-result
 */
public class AssignmentResult {
    private Object value;

    /**
     * Set the result value
     * @param value result value
     */
    protected void setValue(Object value) {
        this.value = value;
    }

    /**
     * Returns the result value
     * @return the result value
     */
    public Object getValue() {
        return this.value;
    } 
}
