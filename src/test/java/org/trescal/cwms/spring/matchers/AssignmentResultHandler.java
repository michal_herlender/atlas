package org.trescal.cwms.spring.matchers;

import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultHandler;

import com.jayway.jsonpath.JsonPath;

/**
 * Spring ResultHandler for MVC testing, allows the assignment of a JSON path to a variable.
 * Galen Beck - 2019-01-25
 * Taken from:
 * https://stackoverflow.com/questions/34479906/how-to-extract-explicit-content-from-org-springframework-test-web-servlet-result
 */
public class AssignmentResultHandler implements ResultHandler {

    private final JsonPath jsonPath;
    private final AssignmentResult assignmentResult;

    public static ResultHandler assignTo(String jsonPath, AssignmentResult assignmentResult) {
        return new AssignmentResultHandler(JsonPath.compile(jsonPath), assignmentResult);
    }

    protected AssignmentResultHandler(JsonPath jsonPath, AssignmentResult assignmentResult) {
        this.jsonPath = jsonPath;
        this.assignmentResult = assignmentResult;
    }

    @Override
    public void handle(MvcResult result) throws Exception {
        String resultString = result.getResponse().getContentAsString();
        assignmentResult.setValue(jsonPath.read(resultString));
    }
}
