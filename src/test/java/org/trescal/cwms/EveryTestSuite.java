package org.trescal.cwms;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.trescal.cwms.core.CoreTestSuite;
import org.trescal.cwms.integration.IntegrationTestsSuite;
import org.trescal.cwms.rest.RestTestSuite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ 
	CoreTestSuite.class,
	RestTestSuite.class,
	IntegrationTestsSuite.class
})
public class EveryTestSuite {

}
