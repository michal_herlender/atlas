package org.trescal.cwms.rest.adveso;

import lombok.val;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetCompany;
import org.trescal.cwms.tests.generic.DataSetDelivery;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestAdvesoClientDeliveryNotesTest extends MvcRestTestClass {

	public static final String DATA_SET = "/cwms/generic/delivery/JobDeliveryClient.xml";
	public static final String USERNAME = "technician";

	@Override
	protected List<String> getDataSetFileNames() {
		return Stream.of(DataSetDelivery.FILE_JOB_DELIVERY_CLIENT,
			DataSetCompany.FILE_COMPANY_CLIENT).collect(Collectors.toList());
	}

	@Test
	public void getAcceptDateWithTime() throws Exception {
		val uri = ServletUriComponentsBuilder.fromCurrentServletMapping()
			.path("/customer/deliverynotes")
			.queryParam("subdivid", "110")
			.queryParam("deliveryType", "CLIENT")
			.queryParam("dtstart", "2019-10-11T00:00:00.000").build().toUriString();
		this.mockMvc
			.perform(get(uri)
				.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
				.sessionAttr(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME))
			.andDo(print()).andExpect(status().isOk())
			.andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
			.andExpect(jsonPath("$.success").value(true))
			.andExpect(jsonPath("$.results").isNotEmpty());
	}

	@Test
	public void getFailWithDateWithoutTime() throws Exception {
		val uri = ServletUriComponentsBuilder.fromCurrentServletMapping()
			.path("/customer/deliverynotes")
			.queryParam("subdivid", "110")
			.queryParam("deliveryType", "CLIENT")
			.queryParam("dtstart", "2019-10-01").build().toUriString();
		this.mockMvc
			.perform(get(uri)
				.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
				.sessionAttr(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME))
			.andDo(print()).andExpect(status().isOk())
			.andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
			.andExpect(jsonPath("$.success").value(false));
	}

}
