package org.trescal.cwms.rest.adveso;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcRestTestClass;

import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestAdvesoConfirmReceiptControllerTest extends MvcRestTestClass {
    public static final String DATA_SET = "rest/adveso/Adveso.xml";
    public static final String USERNAME = "technician";

    @Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }

	@Test
	public void confirmReceipt() throws Exception {
		String actionId = "1";
		this.mockMvc
				.perform(get("/adveso/confirmReceipt/" + actionId+"?isSuccess=true")
						.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
						.contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
						.sessionAttr(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME))
				.andDo(print()).andExpect(status().isOk())
				.andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.message").value("")).andExpect(jsonPath("$.success").value(true));
	}
	
	@Test
	public void confirmReceiptWithError() throws Exception {
		String actionId = "1";
		this.mockMvc
				.perform(get("/adveso/confirmReceipt/" + actionId+"?isSuccess=false&errorMessage=le error message")
						.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
						.contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
						.sessionAttr(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME))
				.andDo(print()).andExpect(status().isOk())
				.andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.message").value("")).andExpect(jsonPath("$.success").value(true));
	}
	
	@Test
	public void confirmInexistingActivity() throws Exception {
		String actionId = "1000";
		this.mockMvc
				.perform(get("/adveso/confirmReceipt/" + actionId+"?isSuccess=false&errorMessage=le error message")
						.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
						.contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
						.sessionAttr(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME))
				.andDo(print()).andExpect(status().isOk())
				.andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.success").value(false));
	}

}
