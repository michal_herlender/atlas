package org.trescal.cwms.rest.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetCompany;
import org.trescal.cwms.tests.generic.DataSetPricing;
import org.trescal.cwms.tests.generic.pricing.TestDataCreditNote;
import org.trescal.cwms.tests.generic.pricing.TestDataInvoice;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestInvoiceCreditNoteValidatorTest extends MvcRestTestClass {

	public static final String USER_CASA = "casablanca_user";
	public static final String PARAM_USER = "username";
	public static final String PARAM_INVOICE = "invoiceNo";
	public static final String PARAM_CREDIT_NOTE= "creditNoteNo";
	@Override
	protected List<String> getDataSetFileNames() {
		List<String> result = DataSetCompany.getCompanyClientTestData();
		result.add(DataSetPricing.FILE_INVOICE);
		result.add(DataSetPricing.FILE_CREDIT_NOTE);
		return result;
	}

	@Transactional
	@Test
	public void testFailure_NullData() throws Exception {
		String errorMessage = "At least one of invoice number or credit note number must be specified";
		String errorFieldName = "invoiceNo";
		Map<String, Object> inputPayload = new HashMap<>();
		inputPayload.put(PARAM_USER, USER_CASA);
		inputPayload.put(PARAM_INVOICE, null);
		inputPayload.put(PARAM_CREDIT_NOTE, null);
		testFailure(inputPayload, errorFieldName, errorMessage);
	}
	
	@Transactional
	@Test
	public void testFailure_InvoiceNotFoundData() throws Exception {
		String errorMessage = "Invoice not found";
		String errorFieldName = "invoiceNo";
		Map<String, Object> inputPayload = new HashMap<>();
		inputPayload.put(PARAM_USER, USER_CASA);
		inputPayload.put(PARAM_INVOICE, TestDataInvoice.INVOICE_NUMBER_FR_110);
		inputPayload.put(PARAM_CREDIT_NOTE, null);
		testFailure(inputPayload, errorFieldName, errorMessage);
	}
	
	@Transactional
	@Test
	public void testFailure_CreditNoteNotFoundData() throws Exception {
		String errorMessage = "Credit note not found";
		String errorFieldName = "creditNoteNo";
		Map<String, Object> inputPayload = new HashMap<>();
		inputPayload.put(PARAM_USER, USER_CASA);
		inputPayload.put(PARAM_INVOICE, null);
		inputPayload.put(PARAM_CREDIT_NOTE, TestDataCreditNote.CREDIT_NOTE_NUMBER_FR_110);
		testFailure(inputPayload, errorFieldName, errorMessage);
	}
	
	@Transactional
    @Test
    @Disabled
	public void testFailure_UserNotAuthorized() throws Exception {
		String errorMessage = "The user is not authorized to receive the invoice";
		String errorFieldName = "invoiceNo";
		Map<String, Object> inputPayload = new HashMap<>();
		inputPayload.put(PARAM_USER, USER_CASA);
		inputPayload.put(PARAM_INVOICE, TestDataInvoice.INVOICE_NUMBER_FR_111);
		inputPayload.put(PARAM_CREDIT_NOTE, null);
		testFailure(inputPayload, errorFieldName, errorMessage);
	}

	private void testFailure(Map<String, Object> inputPayload, String errorFieldName, String errorMessage)
			throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		String jsonInput = mapper.writeValueAsString(inputPayload);
		this.mockMvc
				.perform(post("/api/data/invoice")
						.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
						.contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8).content(jsonInput))
				.andDo(print()).andExpect(status().isOk())
				.andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.errors").exists())
				.andExpect(jsonPath("errors[0].fieldName", Matchers.equalTo(errorFieldName)))
				.andExpect(jsonPath("errors[0].message", Matchers.equalTo(errorMessage)))
				.andExpect(jsonPath("$.success").value(false));
	}

}
