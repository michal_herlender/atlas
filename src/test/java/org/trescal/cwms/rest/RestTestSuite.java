package org.trescal.cwms.rest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.trescal.cwms.rest.logistics.service.RestPrebookingServiceImplTest;
import org.trescal.cwms.rest.logistics.validator.RestPrebookingDTOValidatorTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	// logistics.controller
	RestPrebookingDTOValidatorTest.class,
	RestPrebookingServiceImplTest.class,
})

public class RestTestSuite {

}
