package org.trescal.cwms.rest.security;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcRestTestClass;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class RestPingControllerTest extends MvcRestTestClass {

    // No test data needed
    @Override
    protected List<String> getDataSetFileNames() {
        return null;
    }

    @Test
	public void testPing() throws Exception {
		// Used for session authenticated access
		this.mockMvc.perform(get("/api/ping")
			.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)))
			.andDo(print())
		    .andExpect(status().isOk());
	}

	@Test
	public void testPingApi() throws Exception {
		// Used for session authenticated access
		this.mockMvc.perform(get("/ping")
			.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)))
			.andDo(print())
		    .andExpect(status().isOk());
	}
}
