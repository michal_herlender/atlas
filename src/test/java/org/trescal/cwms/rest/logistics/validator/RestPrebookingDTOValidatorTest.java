package org.trescal.cwms.rest.logistics.validator;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.WebDataBinder;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.logistics.dto.RestPrebookingDTO;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetCompany;
import org.trescal.cwms.tests.generic.DataSetExchangeFormat;
import org.trescal.cwms.tests.generic.company.TestDataContact;
import org.trescal.cwms.tests.generic.company.TestDataSubdiv;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

/**
 * Runs different test cases of validator
 * <p>
 * Note that full test DTOs are not created here currently, just enough for
 * "white box" test cases to start.
 * <p>
 * We could expand the DTO to be a full representative example (to check
 * instrument fields etc...)
 */
public class RestPrebookingDTOValidatorTest extends MvcRestTestClass {


	@Autowired
	private RestPrebookingDTOValidator validator;
	@Autowired
	private SubdivService subdivService;

	@Override
	protected List<String> getDataSetFileNames() {
		List<String> result = DataSetCompany.getCompanyClientTestData();
		result.add(DataSetExchangeFormat.FILE_EXTERNAL_SYSTEM);
		return result;
	}

	@Test
	@Transactional
	public void testSuccess() {
		RestPrebookingDTO dto = new RestPrebookingDTO();
		dto.setFirstName(TestDataContact.FIRST_NAME_CLIENT_FRANCE);
		dto.setLastName(TestDataContact.FIRST_NAME_CLIENT_FRANCE);
		dto.setSubdivid(TestDataSubdiv.ID_CLIENT_FRANCE);
		dto.setLogisticianContactId(TestDataContact.ID_BUSINESS_FRANCE);
		dto.setItems(Collections.emptyList());

		BindingResult bindingResult = (new WebDataBinder(dto)).getBindingResult();
		validator.validate(dto, bindingResult);
		String errorMessages = bindingResult.getAllErrors().toString();

		Assertions.assertFalse( bindingResult.hasErrors(),errorMessages);
	}

	@Test
	@Transactional
	public void testErrorNoDefaultContactOnSubdiv() {
		// Clear the default contact on our test subdivision
		Subdiv subdiv = this.subdivService.get(TestDataSubdiv.ID_BUSINESS_FRANCE);
		subdiv.setDefaultContact(null);

		// DTO with client subdiv but no name selected
		RestPrebookingDTO dto = new RestPrebookingDTO();
		dto.setSubdivid(TestDataSubdiv.ID_BUSINESS_FRANCE);
		dto.setLogisticianContactId(TestDataContact.ID_BUSINESS_FRANCE);
		dto.setItems(Collections.emptyList());

        BindingResult bindingResult = (new WebDataBinder(dto)).getBindingResult();
        validator.validate(dto, bindingResult);

        Assertions.assertTrue(bindingResult.hasErrors());
        String errorMessages = bindingResult.getAllErrors().toString();
        Assertions.assertEquals(1l, bindingResult.getErrorCount(), errorMessages);
        Assertions.assertEquals(1l, bindingResult.getFieldErrorCount(), errorMessages);

        FieldError fieldError = bindingResult.getFieldError();
        Assertions.assertEquals(fieldError.getField(), "subdivid");
        Assertions.assertEquals(fieldError.getCode(), RestErrors.CODE_SUBDIV_DEFAULT_CONTACT, fieldError.toString());
    }

	@Test
	@Transactional
	public void testErrorNullFields() {
		RestPrebookingDTO dto = new RestPrebookingDTO();
		BindingResult bindingResult = (new WebDataBinder(dto)).getBindingResult();
		validator.validate(dto, bindingResult);

		Assertions.assertTrue(bindingResult.hasErrors());
		// 3 errors - items, subdivId, logisticianContactId -
		String errorMessages = bindingResult.getAllErrors().toString();
		Assertions.assertEquals( 3l, bindingResult.getErrorCount(),errorMessages);
		Assertions.assertEquals( 3l, bindingResult.getFieldErrorCount(),errorMessages);
		
	}

}
