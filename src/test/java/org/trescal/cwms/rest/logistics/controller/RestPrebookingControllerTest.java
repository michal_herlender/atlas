package org.trescal.cwms.rest.logistics.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetCompany;
import org.trescal.cwms.tests.generic.DataSetExchangeFormat;
import org.trescal.cwms.tests.generic.address.TestDataAddress;
import org.trescal.cwms.tests.generic.company.TestDataContact;
import org.trescal.cwms.tests.generic.company.TestDataSubdiv;
import org.trescal.cwms.tests.generic.instrument.TestDataInstrument;

import javax.transaction.Transactional;
import java.util.*;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestPrebookingControllerTest extends MvcRestTestClass {

	@Override
	protected List<String> getDataSetFileNames() {
		List<String> result = DataSetCompany.getCompanyClientTestData();
		result.add(DataSetExchangeFormat.FILE_EXTERNAL_SYSTEM);
		return result;
	}

	/**
	 * Create a "minimal" prebooking with the minimum information needed
	 */
	@Test
	@Transactional
	public void testSuccess_MinimumReferences() throws Exception {
		Map<String, Object> inputPayload = new HashMap<>();
		inputPayload.put("subdivid", TestDataSubdiv.ID_BUSINESS_MOROCCO);
		inputPayload.put("logisticianContactId", TestDataContact.ID_BUSINESS_MOROCCO);
		inputPayload.put("items", Collections.emptyList());
		testSuccess(inputPayload);
	}

	/**
	 * Create a "maximal" prebooking with examples of all fields populated that
	 * are used
	 */
	@Test
	@Transactional
	public void testSuccess_MaximumReferences() throws Exception {
		Map<String, Object> inputPayload = new HashMap<>();
		List<Map<String, String>> items = new ArrayList<>();
		// add items (instruments)
		Map<String, String> item1 = new HashMap<String, String>();
		item1.put("idTrescal", String.valueOf(TestDataInstrument.ID_FRANCE_CALIPER_1101));
		item1.put("domain", "");
		item1.put("accessories", "3");
		Map<String, String> item2 = new HashMap<String, String>();
		item2.put("idTrescal", null);
		item2.put("customerDescription", "test application mobile");
		item2.put("serialNo", "3811140");
		item2.put("accessories", "2");
		items.add(item1);
		items.add(item2);

		inputPayload.put("subdivid", TestDataSubdiv.ID_CLIENT_FRANCE);
		inputPayload.put("logisticianContactId", TestDataContact.ID_BUSINESS_FRANCE);
		inputPayload.put("items", items);
		inputPayload.put("pickupDate", "11-11-2019T11:30:00.000+0100");
		inputPayload.put("clientRef", "PO11");
		inputPayload.put("firstName", TestDataContact.FIRST_NAME_CLIENT_FRANCE);
		inputPayload.put("lastName", TestDataContact.LAST_NAME_CLIENT_FRANCE);
		inputPayload.put("estCarriageOut", 0.0);
		inputPayload.put("bookedInAddrId", TestDataAddress.ID_ADDRESS_RUNGIS);
		inputPayload.put("bookedInLocId", TestDataAddress.ID_ADDRESS_RUNGIS);
		inputPayload.put("defaultPO", "");
		inputPayload.put("bpo", 11);
		inputPayload.put("poCommentList", new ArrayList<String>());
		inputPayload.put("poNumberList", new ArrayList<String>());
		testSuccess(inputPayload);

	}

	@Transactional
	@Test
	public void testFailure_PoorData() throws Exception {
		// if idTrescal is null we should at least have customerDescription
		String errorMessage = "Please select Trescal ID and if it is null, select at least model and customer description";
		String errorFieldName = "items";
		Map<String, Object> inputPayload = new HashMap<>();
		List<Map<String, String>> items = new ArrayList<>();
		Map<String, String> item1 = new HashMap<String, String>();
		item1.put("idTrescal", null);
		item1.put("domain", "");
		item1.put("customerDescription", "");
		item1.put("accessories", "3");
		items.add(item1);

		inputPayload.put("subdivid", TestDataSubdiv.ID_BUSINESS_FRANCE);
		inputPayload.put("logisticianContactId", TestDataContact.ID_BUSINESS_MOROCCO);
		inputPayload.put("items", items);
		testFailure(inputPayload, errorFieldName, errorMessage);

	}

	private void testSuccess(Map<String, Object> inputPayload) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		String jsonInput = mapper.writeValueAsString(inputPayload);

		this.mockMvc
				.perform(post("/api/logistics/addprebooking")
						.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
						.contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8).content(jsonInput))
				.andDo(print()).andExpect(status().isOk())
				.andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.errors").isEmpty()).andExpect(jsonPath("$.success").value(true));
	}

	private void testFailure(Map<String, Object> inputPayload, String errorFieldName, String errorMessage)
			throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		String jsonInput = mapper.writeValueAsString(inputPayload);
		this.mockMvc
				.perform(post("/api/logistics/addprebooking")
						.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
						.contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8).content(jsonInput))
				.andDo(print()).andExpect(status().isOk())
				.andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.errors").exists())
				.andExpect(jsonPath("errors[0].fieldName", Matchers.equalTo(errorFieldName)))
				.andExpect(jsonPath("errors[0].message", Matchers.equalTo(errorMessage)))
				.andExpect(jsonPath("$.success").value(false));
	}
}
