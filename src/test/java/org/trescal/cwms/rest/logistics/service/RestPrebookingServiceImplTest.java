package org.trescal.cwms.rest.logistics.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.logistics.entity.asn.Asn;
import org.trescal.cwms.core.logistics.form.AsnItemFinalSynthesisDTO;
import org.trescal.cwms.core.logistics.form.PrebookingJobForm;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetCompany;
import org.trescal.cwms.tests.generic.DataSetExchangeFormat;
import org.trescal.cwms.tests.generic.address.TestDataAddress;
import org.trescal.cwms.tests.generic.company.TestDataCompany;
import org.trescal.cwms.tests.generic.company.TestDataContact;
import org.trescal.cwms.tests.generic.company.TestDataSubdiv;
import org.trescal.cwms.tests.generic.core.TestDataSupportedCurrency;
import org.trescal.cwms.tests.generic.instrument.TestDataInstrument;
import org.trescal.cwms.tests.generic.pricing.TestDataQuotationItem;

import javax.transaction.Transactional;
import java.util.*;

/**
 * Test the functionality of the RestPrebookingServiceImpl class:
 * convertDataToAsnItemDTO, analyseData and savePrebooking
 * 
 */
public class RestPrebookingServiceImplTest extends MvcRestTestClass {

	@Autowired
	private RestPrebookingService restPrepookingService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private SupportedCurrencyService supportedCurrencyService;

	@Override
	protected List<String> getDataSetFileNames() {
		List<String> result = DataSetCompany.getCompanyClientTestData();
		result.add(DataSetExchangeFormat.FILE_EXTERNAL_SYSTEM);
		return result;
	}

	@Test
	@Transactional
	public void testSavePrebooking() throws Exception {

		Locale locale = Locale.forLanguageTag("en-GB");
		ObjectMapper mapper = new ObjectMapper();
		PrebookingJobForm dto = new PrebookingJobForm();
		dto.setSubdivid(TestDataSubdiv.ID_CLIENT_FRANCE);
		dto.setAddrid(TestDataAddress.ID_ADDRESS_CASABLANCA);
		dto.setPersonid(TestDataContact.ID_CLIENT_FRANCE);
		dto.setJobType(JobType.STANDARD);
		dto.setEstCarriageOut(TestDataQuotationItem.VALUE_DISCOUNT_RATE_ACTIVE_INSTRUMENT_1102);
		dto.setCurrencyCode(
				supportedCurrencyService.findSupportedCurrency(TestDataSupportedCurrency.ID_EUR).getCurrencyCode());

		List<Map<String, String>> items = new ArrayList<>();
		Map<String, String> item1 = new HashMap<String, String>();
		item1.put("idTrescal", String.valueOf(TestDataInstrument.ID_FRANCE_CALIPER_1101));
		item1.put("domain", "");
		item1.put("accessories", "3");
		Map<String, String> item2 = new HashMap<String, String>();
		item2.put("idTrescal", null);
		item2.put("customerDescription", "test application mobile");
		item2.put("serialNo", "3811140");
		item2.put("accessories", "2");
		items.add(item1);
		items.add(item2);

		String inputItem1 = mapper.writeValueAsString(item1);
		String inputItem2 = mapper.writeValueAsString(item2);

		List<AsnItemFinalSynthesisDTO> aiFinalSynthDtoListe = restPrepookingService.convertDataToAsnItemDTO(items);

		List<AsnItemFinalSynthesisDTO> asnItemsDTOListe = restPrepookingService.analyseData(aiFinalSynthDtoListe,
				TestDataCompany.ID_CLIENT_FRANCE, TestDataSubdiv.ID_CLIENT_FRANCE, locale);

		Asn asn = restPrepookingService.savePrebooking(dto, asnItemsDTOListe,
				contactService.get(TestDataContact.ID_CLIENT_FRANCE),
				contactService.get(TestDataContact.ID_BUSINESS_MOROCCO), inputItem1 + inputItem2);

		RestResultDTO<AsnItemFinalSynthesisDTO> result = new RestResultDTO<>(true, String.valueOf(asn.getId()));
		Assertions.assertEquals(result.isSuccess(), true);
		Assertions.assertEquals(result.getErrors().isEmpty(), true);
		Assertions.assertTrue(!asnItemsDTOListe.isEmpty());
	}
}
