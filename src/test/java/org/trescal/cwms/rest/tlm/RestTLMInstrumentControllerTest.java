package org.trescal.cwms.rest.tlm;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcRestTestClass;

import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestTLMInstrumentControllerTest extends MvcRestTestClass {

    public static final String DATA_SET = "rest/tlm/Instrument.xml";
    public static final String USERNAME = "technician";

    @Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }

	@Test
	public void testSuccess() throws Exception {
		testSuccess(1).andExpect(jsonPath("$.results[0]").exists())
			.andExpect(jsonPath("$.results[0].plantId").value(1));
	}

	private ResultActions testSuccess(int plantid) throws Exception {

		return this.mockMvc
				.perform(get("/tlm/instrument/" + plantid)
						.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
						.contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
						.sessionAttr(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME))
				.andDo(print()).andExpect(status().isOk())
				.andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.message").value("")).andExpect(jsonPath("$.success").value(true));
	}

}
