package org.trescal.cwms.rest.alligator.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.val;
import org.apache.commons.io.FileUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateAction;
import org.trescal.cwms.core.jobs.certificate.entity.certaction.CertAction;
import org.trescal.cwms.core.jobs.certificate.entity.certaction.db.CertActionService;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.tests.MvcRestTestClass;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WithMockUser("technician")
public class RestUploadCertificateControllerTest extends MvcRestTestClass {
	public static final String DATA_SET = "rest/alligator/AlligatorCertificateUpload.xml";
	// Taken from
	// http://stackoverflow.com/questions/17279712/what-is-the-smallest-possible-valid-pdf
	public static final String SMALL_PDF = "%PDF-1.\r\ntrailer<</Root<</Pages<</Kids[<</MediaBox[0 0 3 3]>>]>>>>>>";

	public static final Logger logger = LoggerFactory.getLogger(RestUploadCertificateControllerTest.class);

	public static final String URL_TEMPLATE_LOCALE = "/uploadcertificate?locale=es_ES";
	public static final String URL_TEMPLATE = "/uploadcertificate";
	
	public static final String CERT_NUMBER_1 = "ES-A-MAD-C-16-000001";
	public static final String CERT_NUMBER_2 = "ES-A-MAD-C-16-000002";
	
	public static final Integer CERT_ID_1 = 1;
	public static final Integer CERT_ID_2 = 2;

	private final ObjectMapper objectMapper = new ObjectMapper();

	@Autowired
	private CertificateService certificateService;
	@Autowired
	private CertActionService certActionService;

	@Override
	protected List<String> getDataSetFileNames() {
		return Collections.singletonList(DATA_SET);
	}

	@Test
	public void testSuccess_WithLocale() throws Exception {
		String encodedContent = Base64.getEncoder().encodeToString(SMALL_PDF.getBytes());

		val params = getCommonsParamsMap(CERT_NUMBER_1, encodedContent);
		val request = objectMapper.writeValueAsString(params);
		logger.info(request);
		assertSuccess(URL_TEMPLATE_LOCALE, request, CERT_ID_1);
	}

	@Test
	public void testSuccess_NoExtraFields() throws Exception {
		String encodedContent = Base64.getEncoder().encodeToString(SMALL_PDF.getBytes());

		val params = getCommonsParamsMap(CERT_NUMBER_1, encodedContent);
		val request = objectMapper.writeValueAsString(params);
		logger.info(request);
		assertSuccess(URL_TEMPLATE, request, CERT_ID_1);
	}

	@Test
	public void testFail_WrongStartDate() throws Exception {
		String encodedContent = Base64.getEncoder().encodeToString(SMALL_PDF.getBytes());
		val params = getCommonsParamsMap(CERT_NUMBER_1, encodedContent);
		params.put("startDate", "2016-06-07");
		this.mockMvc
			.perform(post(URL_TEMPLATE)
				.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8).content(objectMapper.writeValueAsString(params)))
			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.success").value(false))
			.andExpect(jsonPath("$.message", Matchers.containsString("JSON parse error: Cannot deserialize value of type `java.time.LocalDateTime`")));
	}

	@Test
	public void testFail_WrongStartDate2() throws Exception {
		String encodedContent = Base64.getEncoder().encodeToString(SMALL_PDF.getBytes());
		val params = getCommonsParamsMap(CERT_NUMBER_1, encodedContent);
		params.put("startDate", "2016-06-07T12:17:18.965Z");
		this.mockMvc
			.perform(post(URL_TEMPLATE)
				.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8).content(objectMapper.writeValueAsString(params)))
			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.success").value(false))
			.andExpect(jsonPath("$.message", Matchers.containsString("JSON parse error: Cannot deserialize value of type `java.time.LocalDateTime`")));
	}

	@Test
	public void testSuccess_AllExtraFields() throws Exception {
		String encodedContent = Base64.getEncoder().encodeToString(SMALL_PDF.getBytes());
		val params = getCommonsParamsMap(CERT_NUMBER_1, encodedContent);
		params.put("issueDate", "2016-04-04T00:00:00.000");
		params.put("startDate", "2016-06-07T12:17:18.965");
		params.put("thirdCertificateNumber", "Sample TLM Cert Number");
		val request = objectMapper.writeValueAsString(params);
		logger.info(request);
		assertSuccess(URL_TEMPLATE, request, CERT_ID_1);
		assertThirdCertNumber(CERT_ID_1);
		assertCertDate(LocalDate.of(2016, 4, 4), CERT_ID_1);
	}

	private HashMap<String, String> getCommonsParamsMap(String certNumber, String encodedContent) {
		val params = new HashMap<String, String>();
		params.put("certificateNumber", certNumber);
		params.put("content", encodedContent);
		params.put("fileName", "test.pdf");
		return params;
	}

	@Test
	public void testSuccess_thirdCertificateNumber_ExtraFields() throws Exception {
		String encodedContent = Base64.getEncoder().encodeToString(SMALL_PDF.getBytes());

		val params = getCommonsParamsMap(CERT_NUMBER_1, encodedContent);
		params.put("issueDate", "2016-04-04T00:00:00.000");
		params.put("thirdCertificateNumber", "Sample TLM Cert Number");
		String request = objectMapper.writeValueAsString(params);
		logger.info(request);
		assertSuccess(URL_TEMPLATE, request, CERT_ID_1);
		assertThirdCertNumber(CERT_ID_1);
		assertCertDate(LocalDate.of(2016, 4, 4), CERT_ID_1);
	}

	@Test
	public void testSuccess_ShortIssueDate() throws Exception {
		String encodedContent = Base64.getEncoder().encodeToString(SMALL_PDF.getBytes());

		val params = getCommonsParamsMap(CERT_NUMBER_1, encodedContent);
        params.put("issueDate", "2016-04-04");
        params.put("startDate", "2016-06-07T12:15:15.123");
        params.put("thirdCertificateNumber", "Sample TLM Cert Number");
        val request = objectMapper.writeValueAsString(params);
        logger.info(request);
        assertSuccess(URL_TEMPLATE, request, CERT_ID_1);
        assertThirdCertNumber(CERT_ID_1);
        assertCertDate(LocalDate.of(2016, 4, 4), CERT_ID_1);
    }

    @Test
    public void testSuccess_IssueDateWithTimeZone() throws Exception {
        String encodedContent = Base64.getEncoder().encodeToString(SMALL_PDF.getBytes());

        val params = getCommonsParamsMap(CERT_NUMBER_1, encodedContent);
        params.put("issueDate", "2021-06-24T08:20:00.439+02:00");
        params.put("startDate", "2021-06-07T12:15:15.123");
        params.put("thirdCertificateNumber", "Sample TLM Cert Number");
        val request = objectMapper.writeValueAsString(params);
        logger.info(request);
        assertSuccess(URL_TEMPLATE, request, CERT_ID_1);
        assertThirdCertNumber(CERT_ID_1);
        assertCertDate(LocalDate.of(2021, 6, 24), CERT_ID_1);
    }

    /**
     * This is the only test using cert id 2 - already signed
     */
    @Test
    public void testSuccess_IssueDate_SignedCertificate() throws Exception {
        String encodedContent = Base64.getEncoder().encodeToString(SMALL_PDF.getBytes());

        val params = getCommonsParamsMap(CERT_NUMBER_2, encodedContent);
		
		params.put("issueDate", "2016-04-04");
		val request = objectMapper.writeValueAsString(params);
		logger.info(request);
		assertSuccess(URL_TEMPLATE, request, CERT_ID_2);
		assertCertDate(LocalDate.of(2016, 4, 4), CERT_ID_2);
	}


	@Test
	public void testSuccess_NullExtraFields() throws Exception {
		String encodedContent = Base64.getEncoder().encodeToString(SMALL_PDF.getBytes());

		val params = getCommonsParamsMap(CERT_NUMBER_1, encodedContent);
		params.put("issueDate", null);
		params.put("startDate", null);
		params.put("thirdCertificateNumber", null);
		val request = objectMapper.writeValueAsString(params);
		logger.info(request);
		assertSuccess(URL_TEMPLATE, request, CERT_ID_1);
	}

	private JSONObject readJson() throws JSONException {
		String content = null;
		File file = new File("./src/test/resources/test-data/cwms/generic/certificate/jsonFile.json");
		try {
			content = FileUtils.readFileToString(file, "utf-8");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new JSONObject(content);
	}

	private void assertSuccess(String urlTemplate, String content, Integer certId) throws Exception {
		this.mockMvc
			.perform(post(urlTemplate).accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8).content(content))
			.andDo(print()).andExpect(status().isOk())
			.andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
			.andExpect(jsonPath("$.success").value(true)).andExpect(jsonPath("$.message").value(""));

		assertCertificateValues(certId);
	}

	private void assertFailure(String jsonInput) throws Exception {
		this.mockMvc
			.perform(post(URL_TEMPLATE)
				.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8).content(jsonInput))
			.andDo(print()).andExpect(status().isOk())
			.andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
			.andExpect(jsonPath("$.errors").exists())
			.andExpect(jsonPath("$.success").value(false))
			.andExpect(jsonPath("errors[0].message", Matchers.equalTo("File Size is larger than 20MB")));
	}

	// Runs outside of transaction
	private void assertCertificateValues(Integer certId) {
        // Certificate should now be in signed status
        Certificate certificate = this.certificateService.findCertificate(certId);
        assertNotNull(certificate.getStatus());
        assertEquals(CertStatusEnum.SIGNED, certificate.getStatus()); // Signed

        if (certId == CERT_ID_1) {
            // There should also be a CertAction logged against the certificate
            assertNotNull(certificate.getActions());
            assertEquals(1, this.certActionService.getAllCertActions().size());
            CertAction certAction = this.certActionService.getAllCertActions().get(0);
            assertEquals(certAction.getCert().getCertid(), certificate.getCertid());
            assertEquals(CertificateAction.SIGNEDANDAPPROVED, certAction.getAction());
            assertNotEquals(certAction.getActionBy().getPersonid(), 0);
            assertEquals(1, certAction.getActionBy().getPersonid());
        }
		else if (certId == CERT_ID_1) {
            // There should be no CertAction logged against the certificate (as it was already signed)
            assertNotNull(certificate.getActions());
            assertEquals(0, this.certActionService.getAllCertActions().size());
        }
	}


    private void assertThirdCertNumber(Integer certId) {
        Certificate certificate = this.certificateService.findCertificate(certId);
        assertEquals("Sample TLM Cert Number", certificate.getThirdCertNo());
    }

	private void assertCertDate(LocalDate expectedDate, Integer certId) {
		Certificate certificate = this.certificateService.findCertificate(certId);
		LocalDate certDate = DateTools.dateToLocalDate(certificate.getCertDate());
        logger.info(certDate.toString());
        assertEquals(expectedDate, certDate);
	}
}
