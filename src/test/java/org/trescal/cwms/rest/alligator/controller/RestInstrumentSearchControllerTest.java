package org.trescal.cwms.rest.alligator.controller;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcRestTestClass;

import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestInstrumentSearchControllerTest extends MvcRestTestClass {
    @Autowired
    private InstrumService instrumentService;

    public static final String DATA_SET = "rest/alligator/AlligatorCalibration.xml";
    public static final String USERNAME = "technician";

    @Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }

	private void testError(String url, String expectedErrorMessage) throws Exception {
		this.mockMvc.perform(get(url)
		  .accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
		  .sessionAttr(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME))
		.andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.message").value(expectedErrorMessage))
        .andExpect(jsonPath("$.success").value(false))
        .andExpect(jsonPath("$.results").isArray())
        .andExpect(jsonPath("$.results[0]").doesNotExist());
	}
	private ResultActions testSuccess(String url) throws Exception {
		return this.mockMvc.perform(get(url)
		  .accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
		  .sessionAttr(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME))
		.andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.message").value(""))
        .andExpect(jsonPath("$.success").value(true))
        .andExpect(jsonPath("$.results").isArray());
	}
	
	@Test
	public void testError_BlankBarcode_EN() throws Exception {
		testError("/instruments?barcode=", "You must enter a non-blank barcode");
	}
	@Test
	public void testError_BlankBarcode_ES() throws Exception {
		testError("/instruments?locale=es_ES&barcode=", "Debe introducir un código de barras no esté en blanco");
	}
	@Test
	public void testError_BlankPlantNumber_EN() throws Exception {
		testError("/instruments?workingStandard_plantNumber=", "You must enter a non-blank plant number");
	}
	@Test
	public void testError_BlankPlantNumber_ES() throws Exception {
		testError("/instruments?locale=es_ES&workingStandard_plantNumber=", "Debe introducir un n\u00FAmero de equipo no est\u00E9 en blanco");
	}
	/*
	 * We expect 1 result
	 */
	@Test
	public void testSuccess_Barcode_1() throws Exception {
		testSuccess("/instruments?locale=es_ES&barcode=1")
        .andExpect(jsonPath("$.results[0]").exists())
        .andExpect(jsonPath("$.results[1]").doesNotExist())
        .andExpect(jsonPath("$.results[0].customerDescription").value("CD1"))
        .andExpect(jsonPath("$.results[0].formerBarcode").value("FBC1"))
        .andExpect(jsonPath("$.results[0].jobItemId").value(1))
        .andExpect(jsonPath("$.results[0].manufacturer").value("AGILENT"))
        .andExpect(jsonPath("$.results[0].model").value("34901A"))
        .andExpect(jsonPath("$.results[0].plantId").value(1))
        .andExpect(jsonPath("$.results[0].plantNumber").value("PN01"))
        .andExpect(jsonPath("$.results[0].serialNumber").value("SN1"))
        .andExpect(jsonPath("$.results[0].subFamily").value("tarjeta de adquisición"));
	}
	/*
	 * We expect 2 results, one with formerBarcode of 2 and one with plantId of 2
	 * Neither should have an active job (plantid=2 has inactive, plantid=3 has none)
	 */
	@Test
	public void testSuccess_Combination_2() throws Exception {	
		testSuccess("/instruments?locale=es_ES&barcode=2")
        .andExpect(jsonPath("$.results[0]").exists())
        .andExpect(jsonPath("$.results[0].jobItemId").value(0))
        .andExpect(jsonPath("$.results[1]").exists())
        .andExpect(jsonPath("$.results[1].jobItemId").value(0))
        .andExpect(jsonPath("$.results[2]").doesNotExist());
	}
	/*
	 * We expect 1 result
	 */
	@Test
	public void testSuccess_FormerBarcode_FBC1() throws Exception {	
		testSuccess("/instruments?locale=es_ES&barcode=FBC1")
        .andExpect(jsonPath("$.results[0]").exists())
        .andExpect(jsonPath("$.results[0].formerBarcode").value("FBC1"))
        .andExpect(jsonPath("$.results[1]").doesNotExist());
	}
	/*
	 * Should return 2 results (not 3 - based on user rights) 
	 */
	@Test
	public void testSuccess_PlantNumber_PN01() throws Exception {
		testSuccess("/instruments?workingStandard_plantNumber=PN01")
        .andExpect(jsonPath("$.results[0]").exists())
        .andExpect(jsonPath("$.results[0].plantNumber").value("PN01"))
        .andExpect(jsonPath("$.results[1]").exists())
        .andExpect(jsonPath("$.results[1].plantNumber").value("PN01"))
        .andExpect(jsonPath("$.results[2]").doesNotExist());
	}
	/*
	 * Should return 9 results (not more - based on business company user rights) 
	 */
	@Test
	public void testSuccess_PlantNumber_PN() throws Exception {
		testSuccess("/instruments?workingStandard_plantNumber=PN")
        .andExpect(jsonPath("$.results[7]").exists())
        .andExpect(jsonPath("$.results[8]").doesNotExist());
	}
	
	/*
	 * Written to diagnose issues with data loading / searching 2016-03-18 GB
	 */
	@Test
	public void testDataAvailable() throws Exception {
		
		Assert.assertEquals(11, this.instrumentService.getAll().size());
		
		Assert.assertEquals(1, this.instrumentService.get(1).getPlantid());
		Assert.assertEquals("FBC1", this.instrumentService.get(1).getFormerBarCode());
		Assert.assertEquals(2, this.instrumentService.get(2).getPlantid());
		Assert.assertEquals("2", this.instrumentService.get(3).getFormerBarCode());
		
		Assert.assertEquals(1, this.instrumentService.findInstrumByBarCode("1").size());
		Assert.assertEquals(1, this.instrumentService.findInstrumByBarCode("FBC1").size());
		Assert.assertEquals(2, this.instrumentService.findInstrumByBarCode("2").size());
	}
}
