package org.trescal.cwms.rest.alligator.component;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.db.JobItemWorkRequirementService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocales;
import org.trescal.cwms.tests.MvcRestTestClass;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

/*
 * Not strictly MVC tests, so we run in transactional context
 */

public class RestStartCalibrationValidatorTest extends MvcRestTestClass {
    public static final String DATA_SET = "rest/alligator/AlligatorCalibration.xml";
    public static final String USER_NAME = "technician";

    @Autowired
	private RestStartCalibrationValidator validator;
	
	@Autowired
	private JobItemWorkRequirementService jiwrService;
	
	@Autowired
	private UserService userService;
	
	@Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }

	@Test
	@Transactional	
	public void shouldFailNotStartable() {
		Locale locale = SupportedLocales.ENGLISH_GB;
		Contact contact = this.userService.get(USER_NAME).getCon();
		JobItemWorkRequirement jiwr4 = jiwrService.findJobItemWorkRequirement(4);
		String expectedMessage = "Work Requirement 4 not awaiting calibration; Work Requirement 4 already complete; Jobitem JOB-1.2 has no current address"; 
		Assert.assertEquals(expectedMessage, validator.getNotStartableReasons(jiwr4, contact, false, locale));
		Assert.assertFalse(validator.isCalibrationStartable(jiwr4, contact, false));
	}

	@Test
	@Transactional	
	public void shouldFailNotStartableAsGroup() {
		Locale locale = SupportedLocales.ENGLISH_GB;
		Contact contact = this.userService.get(USER_NAME).getCon();
		Collection<JobItemWorkRequirement> workRequirements = new HashSet<>();
		workRequirements.add(jiwrService.findJobItemWorkRequirement(10));
		workRequirements.add(jiwrService.findJobItemWorkRequirement(13));
		
		String expectedMessage = "Hector Rigas is not authorised for capability 10004 and calibration type AC-IH; Hector Rigas is not authorised for capability 10004 and calibration type AC-IH"; 
		Assert.assertEquals(expectedMessage, validator.getNotStartableReasons(workRequirements, contact, false, locale));
		Assert.assertFalse(validator.isCalibrationStartableAsGroup(workRequirements, contact, false));
	}

	@Test
	@Transactional	
	public void testFailNotAuthorized() {
		Locale locale = SupportedLocales.ENGLISH_GB;
		Contact contact = this.userService.get(USER_NAME).getCon();
		JobItemWorkRequirement jiwr10 = jiwrService.findJobItemWorkRequirement(10);
		String expectedMessage = "Hector Rigas is not authorised for capability 10004 and calibration type AC-IH"; 
		Assert.assertEquals(expectedMessage, validator.getNotStartableReasons(jiwr10, contact, false, locale));
		Assert.assertFalse(validator.isCalibrationStartable(jiwr10, contact, false));
	}

	@Test
	@Transactional	
	public void testFailNotFullCalibration() {
		Locale locale = SupportedLocales.ENGLISH_GB;
		Contact contact = this.userService.get(USER_NAME).getCon();
		JobItemWorkRequirement jiwr11 = jiwrService.findJobItemWorkRequirement(11);
		String expectedMessage = "Calibration is not a main calibration"; 
		Assert.assertEquals(expectedMessage, validator.getNotStartableReasons(jiwr11, contact, false, locale));
		Assert.assertFalse(validator.isCalibrationStartable(jiwr11, contact, false));
	}

	@Test
	@Transactional	
	public void testSuccessByRequirement() {
		Locale locale = SupportedLocales.ENGLISH_GB;
		Contact contact = this.userService.get(USER_NAME).getCon();
		JobItemWorkRequirement jiwr12 = jiwrService.findJobItemWorkRequirement(12);
		String expectedMessage = ""; 
		Assert.assertEquals(expectedMessage, validator.getNotStartableReasons(jiwr12, contact, false, locale));
		Assert.assertTrue(validator.isCalibrationStartable(jiwr12, contact, false));
	}
}
