package org.trescal.cwms.rest.alligator.controller;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcRestTestClass;

import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestCertificateControllerTest extends MvcRestTestClass {
    public static final String DATA_SET = "rest/alligator/AlligatorCertificate.xml";
    public static final String TEST_DOCUMENT_URL = "/certificate?locale=es_ES&documentnumber=";
    public static final String TEST_JOBITEM_URL = "/certificate?locale=es_ES&jobitemid=";

    public static final String USERNAME_TECHNICIAN = "technician";
    public static final String USERNAME_MANAGER = "manager";

    public static final String CAPABILITY_REFERENCE = "10001";                // Expected when using Procedures (default setting)
	public static final String WORK_INSTRUCTION_REFERENCE = "WI 3 Title";	// Expected when using WIs (special Subdiv setting)

	@Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }

	private void assertFail(String urlFragment, String expectedMessage) throws Exception {
		this.mockMvc.perform(
				get(urlFragment)
				.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.sessionAttr(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME_TECHNICIAN)
			)
		.andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.message").value(expectedMessage))
        .andExpect(jsonPath("$.success").value(false))
        .andExpect(jsonPath("$.results").isArray())
        .andExpect(jsonPath("$.results[0]").doesNotExist());
	}
	private ResultActions assertSuccess(String urlFragment) throws Exception {
		return this.mockMvc.perform(
				get(urlFragment)
				.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
			)
		.andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.message").value(""))
        .andExpect(jsonPath("$.success").value(true))
        .andExpect(jsonPath("$.results").isArray())
        .andExpect(jsonPath("$.results[0]").exists())
        .andExpect(jsonPath("$.results[1]").doesNotExist());
	}
	@Test
	public void testError_CertificateNotFound() throws Exception {
		assertFail("/certificate?documentnumber=BadNumber", "Certificate BadNumber not found");
	}
	@Test
	public void testError_CertificateNotFound_Blank() throws Exception {
		assertFail("/certificate?documentnumber=", "Certificate  not found");
	}
	@Test
	public void testError_NoJobItemLink() throws Exception {
		assertFail("/certificate?documentnumber=ES-A-MAD-C-16-000002", "Certificate has no Job Item link(s)");
	}
	@Test
	public void testSuccess_FullCertificate() throws Exception {
		
		String urlFragment = TEST_DOCUMENT_URL+"ES-A-MAD-C-16-000001";
		assertSuccess(urlFragment)
		.andExpect(jsonPath("$.results[0].defaultLanguage").value("es_ES"))
        .andExpect(jsonPath("$.results[0].secondLanguage").value("en_GB"))
        .andExpect(jsonPath("$.results[0].language").value("es_ES"))
        .andExpect(jsonPath("$.results[0].jobID").value(1))
        .andExpect(jsonPath("$.results[0].jobGroupID").value(0))
        .andExpect(jsonPath("$.results[0].clientCompanyID").value(2))
        .andExpect(jsonPath("$.results[0].clientSubdivID").value(2))
        .andExpect(jsonPath("$.results[0].jobNumber").value("ESTEM-MAD-JI-16000004"))
        .andExpect(jsonPath("$.results[0].certificateTitle").value("Certificado de Calibración"))
        .andExpect(jsonPath("$.results[0].certificateNumber").value("ES-A-MAD-C-16-000001"))
        .andExpect(jsonPath("$.results[0].certificateStatus").value("UNSIGNED_AWAITING_SIGNATURE"))
        .andExpect(jsonPath("$.results[0].certificateStatusTranslation").value("Sin firmar - esperando la firma"))
        .andExpect(jsonPath("$.results[0].accreditationNumber").value("1791.01 : A2LA Calibration Accreditation"))
        .andExpect(jsonPath("$.results[0].laboratoryName").value("Trescal ITM"))
        .andExpect(jsonPath("$.results[0].laboratoryDepartments[0]").value("Madrid"))
        .andExpect(jsonPath("$.results[0].laboratoryAddress[0]").value("Linea 1"))
        .andExpect(jsonPath("$.results[0].laboratoryAddress[1]").value("Linea 2"))
        .andExpect(jsonPath("$.results[0].laboratoryAddress[2]").value("Linea 3"))
        .andExpect(jsonPath("$.results[0].laboratoryPostalCode").value("12345"))
        .andExpect(jsonPath("$.results[0].laboratoryCity").value("Madrid"))
        .andExpect(jsonPath("$.results[0].laboratoryProvinceState").value("Comunidad de Madrid"))
        .andExpect(jsonPath("$.results[0].laboratoryCountry").value("España"))
        .andExpect(jsonPath("$.results[0].laboratoryFax").value(""))
        .andExpect(jsonPath("$.results[0].laboratoryPhone").value(""))
        .andExpect(jsonPath("$.results[0].laboratoryEmail").value(""))
        .andExpect(jsonPath("$.results[0].laboratoryWebsite").value(""))
        .andExpect(jsonPath("$.results[0].trescalWebsite").value("www.trescal.com"))
        .andExpect(jsonPath("$.results[0].instruments[0].customerDescription").value("CD1"))
        .andExpect(jsonPath("$.results[0].instruments[0].formerBarcode").value("FB1"))
        .andExpect(jsonPath("$.results[0].instruments[0].jobItemClientReference").value("JICR1"))
        .andExpect(jsonPath("$.results[0].instruments[0].jobItemID").value(1))
        .andExpect(jsonPath("$.results[0].instruments[0].jobItemNumber").value(1))
        .andExpect(jsonPath("$.results[0].instruments[0].manufacturer").value("Agilent"))
        .andExpect(jsonPath("$.results[0].instruments[0].model").value("34401A"))
        .andExpect(jsonPath("$.results[0].instruments[0].plantID").value(1))
        .andExpect(jsonPath("$.results[0].instruments[0].plantNumber").value("PN1"))
        .andExpect(jsonPath("$.results[0].instruments[0].serialNumber").value("SN1"))
        .andExpect(jsonPath("$.results[0].instruments[0].subFamily").value("Multimetro"))
        .andExpect(jsonPath("$.results[0].applicantName").value("Aldi Supermercado"))
        .andExpect(jsonPath("$.results[0].applicantAddress[0]").value("Linea 4"))
        .andExpect(jsonPath("$.results[0].applicantAddress[1]").value("Linea 5"))
        .andExpect(jsonPath("$.results[0].applicantAddress[2]").value("Linea 6"))
        .andExpect(jsonPath("$.results[0].applicantPostalCode").value("67890"))
        .andExpect(jsonPath("$.results[0].applicantCity").value("Zaragoza"))
        .andExpect(jsonPath("$.results[0].applicantProvinceState").value("Aragon"))
        .andExpect(jsonPath("$.results[0].applicantCountry").value("España"))
        .andExpect(jsonPath("$.results[0].calibrationStartDate").value("2016-01-01T00:00:00.000"))
        .andExpect(jsonPath("$.results[0].calibrationEndDate").value("2016-01-02T00:00:00.000"))
        .andExpect(jsonPath("$.results[0].calibrationDate").value("2016-01-03T00:00:00.000"))
        .andExpect(jsonPath("$.results[0].calibrationFrequency").value(12))
        .andExpect(jsonPath("$.results[0].calibrationFrequencyUnits").value("MONTHS"))
        .andExpect(jsonPath("$.results[0].calibrationDescription").isEmpty())
        .andExpect(jsonPath("$.results[0].technicians").exists())
        .andExpect(jsonPath("$.results[0].technicians").isArray())
        .andExpect(jsonPath("$.results[0].technicians[0].technicianName").value("Hector Rigas"))
        .andExpect(jsonPath("$.results[0].technicians[0].technicianJobTitle").value("Technician"))
        .andExpect(jsonPath("$.results[0].signatureName").isEmpty())
        .andExpect(jsonPath("$.results[0].signatureDateTime").isEmpty())
        .andExpect(jsonPath("$.results[0].signatureTitle").isEmpty())
        .andExpect(jsonPath("$.results[0].signatureImage").isEmpty())
        .andExpect(jsonPath("$.results[0].signatureStamp").isEmpty())
        .andExpect(jsonPath("$.results[0].certificateRemark1").isEmpty())
        .andExpect(jsonPath("$.results[0].certificateRemark2").isEmpty())
        .andExpect(jsonPath("$.results[0].certificateRemark3").isEmpty())
        .andExpect(jsonPath("$.results[0].certificateRemark4").isEmpty())
        .andExpect(jsonPath("$.results[0].locations").isEmpty())
        .andExpect(jsonPath("$.results[0].calibrationProcedures").isArray())
        .andExpect(jsonPath("$.results[0].calibrationProcedures[0]").value(CAPABILITY_REFERENCE))
        .andExpect(jsonPath("$.results[0].workingStandards").isArray())
        .andExpect(jsonPath("$.results[0].workingStandards[0]").exists())
        .andExpect(jsonPath("$.results[0].workingStandards[0]").value("2"))
        .andExpect(jsonPath("$.results[0].workingStandards[1]").doesNotExist())
        .andExpect(jsonPath("$.results[0].certificateUncertaintyStatement").isEmpty())
        .andExpect(jsonPath("$.results[0].templateCode").isEmpty())
        .andExpect(jsonPath("$.results[0].templateExp").isEmpty())
        .andExpect(jsonPath("$.results[0].templateVersion").isEmpty())
        .andExpect(jsonPath("$.results[0].certificateContent").value("Como se ha encontrado y ha dejado"))
        .andExpect(jsonPath("$.results[0].isAdjusted").isEmpty())
        .andExpect(jsonPath("$.results[0].isAccredited").value(true))
        .andExpect(jsonPath("$.results[0].isAsFound").value(true))
        .andExpect(jsonPath("$.results[0].isAsLeft").value(true))
        .andExpect(jsonPath("$.results[0].specifications").isEmpty())
        .andExpect(jsonPath("$.results[0].complianceStatement").isEmpty());
	}
	@Test
	public void testSuccess_OnBehalfOf() throws Exception {
		
		String urlFragment = TEST_DOCUMENT_URL+"ES-A-MAD-C-16-000003";
		assertSuccess(urlFragment)
        .andExpect(jsonPath("$.results[0].instruments[0].customerDescription").value("CD3"))
        .andExpect(jsonPath("$.results[0].instruments[0].formerBarcode").value("FB3"))
        .andExpect(jsonPath("$.results[0].instruments[0].jobItemID").value(3))
        .andExpect(jsonPath("$.results[0].instruments[0].jobItemClientReference").value(""))
        .andExpect(jsonPath("$.results[0].instruments[0].manufacturer").value("Young"))
        .andExpect(jsonPath("$.results[0].instruments[0].model").value("41382LC"))
        .andExpect(jsonPath("$.results[0].instruments[0].plantID").value(3))
        .andExpect(jsonPath("$.results[0].instruments[0].plantNumber").value("PN3"))
        .andExpect(jsonPath("$.results[0].instruments[0].serialNumber").value("SN3"))
        .andExpect(jsonPath("$.results[0].instruments[0].subFamily").value("EQUIPOS VARIOS"))
        .andExpect(jsonPath("$.results[0].applicantName").value("Naranjas Mauricio"))
        .andExpect(jsonPath("$.results[0].applicantAddress[0]").value("Linea 7"))
        .andExpect(jsonPath("$.results[0].applicantAddress[1]").value("Linea 8"))
        .andExpect(jsonPath("$.results[0].applicantAddress[2]").value("Linea 9"))
        .andExpect(jsonPath("$.results[0].applicantPostalCode").value("ABCDE"))
        .andExpect(jsonPath("$.results[0].applicantCity").value("Casablanca"))
        .andExpect(jsonPath("$.results[0].applicantProvinceState").value("Casablanca-Settat"))
        .andExpect(jsonPath("$.results[0].applicantCountry").value("Marruecos"));
	}
	@Test
	public void testSuccess_WorkIntruction() throws Exception {
		
		String urlFragment = TEST_DOCUMENT_URL+"ES-A-BIL-C-16-000005";
		assertSuccess(urlFragment)
        .andExpect(jsonPath("$.results[0].calibrationProcedures").isArray())
        .andExpect(jsonPath("$.results[0].calibrationProcedures[0]").value(WORK_INSTRUCTION_REFERENCE));
	}
	@Test
	public void testSuccess_Jobitem() throws Exception {
		String urlFragment = TEST_JOBITEM_URL+"1";
		assertSuccess(urlFragment)
        .andExpect(jsonPath("$.results[0].certificateNumber").value("ES-A-MAD-C-16-000001"));
		// Remaining data from same source as Success_Certificate
	}
}
