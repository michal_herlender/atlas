package org.trescal.cwms.rest.alligator.controller;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcRestTestClass;

import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestCalibrationDataContentControllerTest extends MvcRestTestClass {
    @Override
    protected List<String> getDataSetFileNames() {
        return Collections.emptyList();
    }

    @Test
    public void testReferenceData() throws Exception {
        this.mockMvc.perform(get("/calibrationdatacontent")
		.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)))
		.andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.0").value("As Found"))
        .andExpect(jsonPath("$.1").value("As Left"))
        .andExpect(jsonPath("$.2").value("As Found and As Left"))
        .andExpect(jsonPath("$.3").doesNotExist())
        ;
	}
}
