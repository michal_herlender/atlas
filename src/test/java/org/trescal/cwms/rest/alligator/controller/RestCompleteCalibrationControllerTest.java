package org.trescal.cwms.rest.alligator.controller;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcRestTestClass;

import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestCompleteCalibrationControllerTest extends MvcRestTestClass {
    public static final String DATA_SET = "rest/alligator/AlligatorCertificate.xml";
    public static final String USERNAME = "technician";

    @Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }
	
	@Test
	public void testSuccess_NoCalDateOrVerificationStatus() throws Exception {
		StringBuffer request = new StringBuffer();
		request.append("{");
        request.append("  \"certificateNumber\":\"ES-A-MAD-C-16-000001\",");
        request.append("  \"calibrationOutcome\":20,");
        request.append("  \"calibrationDataContent\":2,");
        request.append("  \"calibrationStartDate\":\"2016-04-01T00:00:00.000\",");
        request.append("  \"calibrationEndDate\":\"2016-04-03T00:00:00.000\",");
        request.append("  \"calibrationTimes\":[{\"workRequirementId\":1, \"calibrationTime\":5}]");
        request.append("}");
        assertSuccess(request.toString());
    }

    /**
     * The calibrationDate field is a calendar date in its most minimal form
     *
     * @throws Exception
     */
    @Test
    public void testSuccess_WithCalDateOnlyAndVerificationStatus() throws Exception {
        StringBuffer request = new StringBuffer();
        request.append("{");
        request.append("  \"certificateNumber\":\"ES-A-MAD-C-16-000001\",");
        request.append("  \"calibrationOutcome\":20,");
        request.append("  \"calibrationDataContent\":2,");
        request.append("  \"calibrationStartDate\":\"2016-04-01T00:00:00.000\",");
        request.append("  \"calibrationEndDate\":\"2016-04-03T00:00:00.000\",");
        request.append("  \"calibrationDate\":\"2016-04-02\",");
        request.append("  \"calibrationVerificationStatus\":\"ACCEPTABLE\",");
        request.append("  \"calibrationTimes\":[{\"workRequirementId\":1, \"calibrationTime\":5}]");
        request.append("}");
        assertSuccess(request.toString());
    }

    /**
     * The calibrationDate field should optionally support time information (ignored)
     *
     * @throws Exception
     */
    @Test
    public void testSuccess_WithCalDateTimeAndVerificationStatus() throws Exception {
        StringBuffer request = new StringBuffer();
        request.append("{");
        request.append("  \"certificateNumber\":\"ES-A-MAD-C-16-000001\",");
        request.append("  \"calibrationOutcome\":20,");
        request.append("  \"calibrationDataContent\":2,");
        request.append("  \"calibrationStartDate\":\"2016-04-01T00:00:00.000\",");
        request.append("  \"calibrationEndDate\":\"2016-04-03T00:00:00.000\",");
        request.append("  \"calibrationDate\":\"2016-04-02T00:00:00.000\",");
        request.append("  \"calibrationVerificationStatus\":\"ACCEPTABLE\",");
        request.append("  \"calibrationTimes\":[{\"workRequirementId\":1, \"calibrationTime\":5}]");
        request.append("}");
        assertSuccess(request.toString());
    }

    /**
     * The calibrationDate field should optionally support time + time zone information (ignored)
     *
     * @throws Exception
     */
    @Test
    public void testSuccess_WithCalDateTimeZoneAndVerificationStatus() throws Exception {
        StringBuffer request = new StringBuffer();
        request.append("{");
        request.append("  \"certificateNumber\":\"ES-A-MAD-C-16-000001\",");
        request.append("  \"calibrationOutcome\":20,");
        request.append("  \"calibrationDataContent\":2,");
        request.append("  \"calibrationStartDate\":\"2016-04-01T00:00:00.000\",");
        request.append("  \"calibrationEndDate\":\"2016-04-03T00:00:00.000\",");
        request.append("  \"calibrationDate\":\"2016-04-02T00:00:00.000+02:00\",");
        request.append("  \"calibrationVerificationStatus\":\"ACCEPTABLE\",");
        request.append("  \"calibrationTimes\":[{\"workRequirementId\":1, \"calibrationTime\":5}]");
        request.append("}");
        assertSuccess(request.toString());
    }

    @Test
    public void testSuccess_NullCalDateNullVerificationStatus() throws Exception {
        StringBuffer request = new StringBuffer();
        request.append("{");
        request.append("  \"certificateNumber\":\"ES-A-MAD-C-16-000001\",");
        request.append("  \"calibrationOutcome\":20,");
        request.append("  \"calibrationDataContent\":2,");
        request.append("  \"calibrationStartDate\":\"2016-04-01T00:00:00.000\",");
        request.append("  \"calibrationEndDate\":\"2016-04-03T00:00:00.000\",");
		request.append("  \"calibrationDate\":null,");
		request.append("  \"calibrationVerificationStatus\":null,");
		request.append("  \"calibrationTimes\":[{\"workRequirementId\":1, \"calibrationTime\":5}]");
		request.append("}");
		assertSuccess(request.toString());
	}
	
	@Test
	public void assertFail_EmptyCalibrationTimes() throws Exception {
		StringBuffer request = new StringBuffer();
		request.append("{");
		request.append("  \"certificateNumber\":\"ES-A-MAD-C-16-000001\",");
		request.append("  \"calibrationOutcome\":20,");
		request.append("  \"calibrationDataContent\":2,");
		request.append("  \"calibrationStartDate\":\"2016-04-01T00:00:00.000\",");
		request.append("  \"calibrationEndDate\":\"2016-04-03T00:00:00.000\",");
		request.append("  \"calibrationEndDate\":\"2016-04-03T00:00:00.000\",");
		request.append("  \"calibrationDate\":\"2016-04-02T00:00:00.000\",");
		request.append("  \"calibrationVerificationStatus\":\"ACCEPTABLE\",");
		// Here we provide an empty calibrationTimes array;
		request.append("  \"calibrationTimes\":[]");
		request.append("}");
		assertFail(request.toString(), "No Work Requirements Specified");
	}
	
	@Test
	public void assertFail_NoCalibrationTimes() throws Exception {
		StringBuffer request = new StringBuffer();
		request.append("{");
		request.append("  \"certificateNumber\":\"ES-A-MAD-C-16-000001\",");
		request.append("  \"calibrationOutcome\":20,");
		request.append("  \"calibrationDataContent\":2,");
		request.append("  \"calibrationStartDate\":\"2016-04-01T00:00:00.000\",");
		request.append("  \"calibrationEndDate\":\"2016-04-03T00:00:00.000\",");
		request.append("  \"calibrationEndDate\":\"2016-04-03T00:00:00.000\",");
		request.append("  \"calibrationDate\":\"2016-04-02T00:00:00.000\",");
		request.append("  \"calibrationVerificationStatus\":\"ACCEPTABLE\",");
		// Here we provide no calibrationTimes array so that the input is null;
		request.append("  \"calibrationTimes\":[]");
		request.append("}");
		assertFail(request.toString(), "No Work Requirements Specified");
	}
	
	@Test
	public void assertFail_BadCalibrationTimes() throws Exception {
		StringBuffer request = new StringBuffer();
		request.append("{");
		request.append("  \"certificateNumber\":\"ES-A-MAD-C-16-000001\",");
		request.append("  \"calibrationOutcome\":20,");
		request.append("  \"calibrationDataContent\":2,");
		request.append("  \"calibrationStartDate\":\"2016-04-01T00:00:00.000\",");
		request.append("  \"calibrationEndDate\":\"2016-04-03T00:00:00.000\",");
		request.append("  \"calibrationEndDate\":\"2016-04-03T00:00:00.000\",");
		request.append("  \"calibrationDate\":\"2016-04-02T00:00:00.000\",");
		request.append("  \"calibrationVerificationStatus\":\"ACCEPTABLE\",");
		// Here we provide a calibrationTimes array with a bad workRequirementId;
		request.append("  \"calibrationTimes\":[{\"workRequirementId\":9999, \"calibrationTime\":5}]");
		request.append("}");
		assertFail(request.toString(), "Work Requirement IDs Not Found: 9999");
	}
	
	@Test
	public void assertFail_NotAuthorized() throws Exception {
		StringBuffer request = new StringBuffer();
		request.append("{");
		request.append("  \"certificateNumber\":\"ES-A-MAD-C-16-000004\",");
		request.append("  \"calibrationOutcome\":20,");
		request.append("  \"calibrationDataContent\":2,");
		request.append("  \"calibrationStartDate\":\"2016-04-01T00:00:00.000\",");
		request.append("  \"calibrationEndDate\":\"2016-04-03T00:00:00.000\",");
		request.append("  \"calibrationTimes\":[{\"workRequirementId\":4, \"calibrationTime\":5}]");
		request.append("}");
		String expectedMessage = "Hector Rigas is not authorised for capability 10002 and calibration type AC-IH";
		assertFail(request.toString(), expectedMessage);
	}
	
	private void assertSuccess(String content) throws Exception {
		this.mockMvc.perform(
				post("/completecalibration")
				.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
				.content(content)
				.sessionAttr(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME)
			)
		.andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.message").value(""))
        .andExpect(jsonPath("$.success").value(true));
	}
	
	private void assertFail(String content, String expectedMessage) throws Exception {
		this.mockMvc.perform(
				post("/completecalibration")
				.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
				.content(content)
				.sessionAttr(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME)
			)
		.andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.message").value(expectedMessage))
        .andExpect(jsonPath("$.success").value(false))
        .andExpect(jsonPath("$.results").isArray())
        .andExpect(jsonPath("$.results[0]").doesNotExist());
	}
}
