package org.trescal.cwms.rest.alligator.controller;

import com.ibm.icu.text.SimpleDateFormat;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcRestTestClass;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestSupplementaryCertificateControllerTest extends MvcRestTestClass {
	public static final String DATA_SET = "rest/alligator/AlligatorCertificate.xml";
	public static final String USERNAME = "technician";
	public static final String TEST_DOCUMENT_URL = "/alligator/certificate/supplementary?locale=es_ES&documentnumber=";

	@Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }

	private ResultActions assertSuccess(String urlFragment) throws Exception {
		return this.mockMvc.perform(
				post(urlFragment)
				.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.sessionAttr(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME)
			)
		.andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.message").value(""))
        .andExpect(jsonPath("$.success").value(true))
        .andExpect(jsonPath("$.results").isArray())
        .andExpect(jsonPath("$.results[0]").exists())
        .andExpect(jsonPath("$.results[1]").doesNotExist());
	}

	@Test
	public void testSuccess_NoOptionalParams() throws Exception {
		
		String year = (new SimpleDateFormat("yy")).format(new Date());
		String expectedCertificateNumber = "ESA-MAD-CI-"+year+"000001";	// First for current year
		
		String urlFragment = TEST_DOCUMENT_URL+"ES-A-MAD-C-16-000001";
		assertSuccess(urlFragment)
		.andExpect(jsonPath("$.results[0].defaultLanguage").value("es_ES"))
        .andExpect(jsonPath("$.results[0].secondLanguage").value("en_GB"))
        .andExpect(jsonPath("$.results[0].language").value("es_ES"))
        .andExpect(jsonPath("$.results[0].jobID").value(1))
        .andExpect(jsonPath("$.results[0].jobGroupID").value(0))
        .andExpect(jsonPath("$.results[0].jobNumber").value("ESTEM-MAD-JI-16000004"))
        .andExpect(jsonPath("$.results[0].certificateTitle").value("Certificado de Calibraci\u00F3n"))
        .andExpect(jsonPath("$.results[0].certificateNumber").value(expectedCertificateNumber));
	}

	@Test
	public void testSuccess_Hrid() throws Exception {
		
		String year = (new SimpleDateFormat("yy")).format(new Date());
		String expectedCertificateNumber = "ESA-MAD-CI-"+year+"000001";	// First for current year
		
		String urlFragment = TEST_DOCUMENT_URL+"ES-A-MAD-C-16-000001"+"&hrid=FR-1234";
		assertSuccess(urlFragment)
		.andExpect(jsonPath("$.results[0].defaultLanguage").value("es_ES"))
        .andExpect(jsonPath("$.results[0].secondLanguage").value("en_GB"))
        .andExpect(jsonPath("$.results[0].language").value("es_ES"))
        .andExpect(jsonPath("$.results[0].jobID").value(1))
        .andExpect(jsonPath("$.results[0].jobGroupID").value(0))
        .andExpect(jsonPath("$.results[0].jobNumber").value("ESTEM-MAD-JI-16000004"))
        .andExpect(jsonPath("$.results[0].certificateTitle").value("Certificado de Calibraci\u00F3n"))
        .andExpect(jsonPath("$.results[0].certificateNumber").value(expectedCertificateNumber));
	}

	@Test
	public void testSuccess_StartDate() throws Exception {
		
		String year = (new SimpleDateFormat("yy")).format(new Date());
		String expectedCertificateNumber = "ESA-MAD-CI-"+year+"000001";	// First for current year
		
		String urlFragment = TEST_DOCUMENT_URL+"ES-A-MAD-C-16-000001"+"&startDate=2018-09-24";
		assertSuccess(urlFragment)
		.andExpect(jsonPath("$.results[0].defaultLanguage").value("es_ES"))
        .andExpect(jsonPath("$.results[0].secondLanguage").value("en_GB"))
        .andExpect(jsonPath("$.results[0].language").value("es_ES"))
        .andExpect(jsonPath("$.results[0].jobID").value(1))
        .andExpect(jsonPath("$.results[0].jobGroupID").value(0))
        .andExpect(jsonPath("$.results[0].jobNumber").value("ESTEM-MAD-JI-16000004"))
        .andExpect(jsonPath("$.results[0].certificateTitle").value("Certificado de Calibraci\u00F3n"))
        .andExpect(jsonPath("$.results[0].certificateNumber").value(expectedCertificateNumber));
	}

}
