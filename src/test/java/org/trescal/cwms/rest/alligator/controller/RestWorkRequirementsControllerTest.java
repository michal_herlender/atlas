package org.trescal.cwms.rest.alligator.controller;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcRestTestClass;

import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestWorkRequirementsControllerTest extends MvcRestTestClass {
    public static final String DATA_SET = "rest/alligator/AlligatorCalibration.xml";
    public static final String USERNAME = "technician";

    @Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }

	@Test
	public void test_JobItemNotFound() throws Exception {
		this.mockMvc.perform(get("/workrequirements?jobitemid=8")
					.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
					.sessionAttr(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME))
		.andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.success").value(false))
        .andExpect(jsonPath("$.message").value("Job item not found"))
        .andExpect(jsonPath("$.results").isArray())
        .andExpect(jsonPath("$.results[0]").doesNotExist());
	}
	@Test
	public void test_JobItemInactive() throws Exception {
		this.mockMvc.perform(get("/workrequirements?jobitemid=2")
					.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
					.sessionAttr(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME))
		.andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.message").value(""))
        .andExpect(jsonPath("$.success").value(true))
        .andExpect(jsonPath("$.results").isArray())
        .andExpect(jsonPath("$.results[0]").exists())
        .andExpect(jsonPath("$.results[1]").doesNotExist())
        .andExpect(jsonPath("$.results[0].workRequirementId").value(4))
        .andExpect(jsonPath("$.results[0].canStart").value(false))
        //	* Barcode + S/N + Manufacturer Name + Model Number
        .andExpect(jsonPath("$.results[0].description").value("2 SN2 AGILENT 34901A"))
        //	* Procedure Number (Reference) + Procedure Name
        .andExpect(jsonPath("$.results[0].procedures").value("10001 Procedure One"));
	}
	@Test
	public void test_JobItemActive() throws Exception {
		this.mockMvc.perform(get("/workrequirements?jobitemid=1")
					.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
					.sessionAttr(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME))
		.andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.message").value(""))
        .andExpect(jsonPath("$.success").value(true))
        .andExpect(jsonPath("$.results").isArray())
        .andExpect(jsonPath("$.results[2]").exists())
        .andExpect(jsonPath("$.results[3]").doesNotExist())
        //	description = Barcode + S/N + Manufacturer Name + Model Number
        //	procedures = Procedure Number (Reference) + Procedure Name
        .andExpect(jsonPath("$.results[0].workRequirementId").value(1))
        .andExpect(jsonPath("$.results[0].canStart").value(false))
        .andExpect(jsonPath("$.results[0].description").value("1 SN1 AGILENT 34901A"))
        .andExpect(jsonPath("$.results[0].procedures").value("10001 Procedure One"))

		.andExpect(jsonPath("$.results[1].workRequirementId").value(2))
        .andExpect(jsonPath("$.results[1].canStart").value(true))
        .andExpect(jsonPath("$.results[1].description").value("1 SN1 AGILENT 34901A"))
        .andExpect(jsonPath("$.results[1].procedures").value("10002 Procedure Two"))
		
        .andExpect(jsonPath("$.results[2].workRequirementId").value(3))
        .andExpect(jsonPath("$.results[2].canStart").value(true))
        .andExpect(jsonPath("$.results[2].description").value("1 SN1 AGILENT 34901A"))
        .andExpect(jsonPath("$.results[2].procedures").value("10003 Procedure Three"));
	}
}
