package org.trescal.cwms.rest.alligator.controller;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcRestTestClass;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestStartCalibrationControllerTest extends MvcRestTestClass {
	public static final String DATA_SET = "rest/alligator/AlligatorCalibration.xml";
	public static final String USERNAME = "technician";
	
	@Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }

	private void assertFail(String content, String expectedMessage) throws Exception {
		this.mockMvc.perform(
				post("/startcalibration")
				.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
				.content(content)
				.sessionAttr(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME)
			)
		.andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.message").value(expectedMessage))
        .andExpect(jsonPath("$.success").value(false))
        .andExpect(jsonPath("$.results").isArray())
        .andExpect(jsonPath("$.results[0]").doesNotExist());
	}
	private void assertSuccess(String content) throws Exception {
		StringBuffer documentNumber = new StringBuffer("ESA-MAD-CI-");
		documentNumber.append((new SimpleDateFormat("yy").format(new Date())));
		documentNumber.append("000001");
		
		this.mockMvc.perform(
				post("/startcalibration")
				.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
				.content(content)
				.sessionAttr(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME)
			)
		.andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.message").value(""))
        .andExpect(jsonPath("$.success").value(true))
        .andExpect(jsonPath("$.results").isArray())
        .andExpect(jsonPath("$.results[0]").exists())
        .andExpect(jsonPath("$.results[1]").doesNotExist())
        .andExpect(jsonPath("$.results[0].calibrationId").exists())
        .andExpect(jsonPath("$.results[0].documentNumber").exists())
        .andExpect(jsonPath("$.results[0].documentNumber").value(documentNumber.toString()));
	}
	@Test
	public void test_ErrorNoWorkRequirements() throws Exception {
		String content = "{\"workRequirementIds\":[]}";
		String expectedMessage = "No Work Requirements Specified";
		assertFail(content, expectedMessage);
	}
	@Test
	public void test_ErrorWorkRequirementsNotFound() throws Exception {
		String content = "{\"workRequirementIds\":[101, 102, 103]}";
		String expectedMessage = "Work Requirement IDs Not Found: 101, 102, 103";
		assertFail(content, expectedMessage);
	}
	@Test
	public void test_ErrorWorkRequirementNotStartable_NoProcedure() throws Exception {
		String content = "{\"workRequirementIds\":[6]}";
		String expectedMessage = "Work Requirement 6 has no capability";
		assertFail(content, expectedMessage);
	}
	@Test
	public void test_ErrorWorkRequirementNotStartable_NotAwaitingCalibration() throws Exception {
		String content = "{\"workRequirementIds\":[7]}";
		String expectedMessage = "Work Requirement 7 not awaiting calibration";
		assertFail(content, expectedMessage);
	}
	@Test
	public void test_ErrorWorkRequirementNotStartable_AlreadyComplete() throws Exception {
		String content = "{\"workRequirementIds\":[1]}";
		String expectedMessage = "Work Requirement 1 already complete";
		assertFail(content, expectedMessage);
	}
	@Test
	public void test_ErrorWorkRequirementNotStartable_DifferentJobs() throws Exception {
		String content = "{\"workRequirementIds\":[2, 5]}";
		String expectedMessage = "Work Requirements are on different jobs";
		assertFail(content, expectedMessage);
	}
	@Test
	public void test_ErrorWorkRequirementNotStartable_DifferentCalFrequency() throws Exception {
		String content = "{\"workRequirementIds\":[2, 8]}";
		String expectedMessage = "Work Requirements are for instruments with different cal frequencies";
		assertFail(content, expectedMessage);
	}
	@Test
	public void test_ErrorWorkRequirementNotStartable_NotAuthorized() throws Exception {
		String content = "{\"workRequirementIds\":[10]}";
		String expectedMessage = "Hector Rigas is not authorised for capability 10004 and calibration type AC-IH";
		assertFail(content, expectedMessage);
	}
	@Test
	public void testSuccessStart_Multiple() throws Exception {
		String content = "{\"workRequirementIds\":[2, 9]}";
		assertSuccess(content);
		// TODO look up calibration, certificate, validate fields
	}
	@Test
	public void testSuccessStart_Single() throws Exception {
		String content = "{\"workRequirementIds\":[2]}";
		assertSuccess(content);
		// TODO look up calibration, certificate, validate fields
	}
}