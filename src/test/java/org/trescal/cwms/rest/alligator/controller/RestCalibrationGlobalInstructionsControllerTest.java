package org.trescal.cwms.rest.alligator.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcRestTestClass;

public class RestCalibrationGlobalInstructionsControllerTest extends MvcRestTestClass {

    public static final String DATA_SET = "rest/alligator/AlligatorCalibrationInstructions.xml";

    @Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }

    private void assertFail(String content, String expectedMessage) throws Exception {
        this.mockMvc.perform(
                        post("/calibrationinstructions")
                                .accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
                                .contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
                                .content(content)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.success").value(false))
                .andExpect(jsonPath("$.message").value(expectedMessage))
                .andExpect(jsonPath("$.results").isArray())
                .andExpect(jsonPath("$.results[0]").doesNotExist());
    }
    private ResultActions assertSuccess(String content) throws Exception {
        return this.mockMvc.perform(
                        post("/calibrationinstructions")
                                .accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
                                .contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
                                .content(content)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.success").value(true))
                .andExpect(jsonPath("$.message").value(""))
                .andExpect(jsonPath("$.results").isArray());
    }

    /*
    * */
    @Test
    public void testError_NoWorkRequirementsEntered() throws Exception {
        String content = "{\"workRequirementIds\":[]}";
        String expectedMessage = "No Work Requirements Specified";
        assertFail(content, expectedMessage);
    }

    @Test
    public void test_WorkRequirementsEntered() throws Exception {
        String content = "{\"workRequirementIds\":[7]}";
        assertSuccess(content);
    }

    @Test
    public void testError_MultipleJobs() throws Exception {
        String content = "{\"workRequirementIds\":[5,6]}";
        String expectedMessage = "Work Requirements are on different jobs";
        assertFail(content, expectedMessage);
    }

    //Test for Global Instructions.
    @Test
    public void testSuccess_forOneWkReqId() throws Exception {
        // TODO test remaining results
        String content = "{\"workRequirementIds\":[7]}";
        assertSuccess(content)
                .andExpect(jsonPath("$.results[2].globalInstruction").value(true))
                .andExpect(jsonPath("$.results[2].description").value(""))
                .andExpect(jsonPath("$.results[2].procedures").value(""))
                .andExpect(jsonPath("$.results[2].text").value("Test Company: Company Instruction 1 - Calibration"));
    }

    @Test
    public void testSuccess_forMultipleWkReqId() throws Exception {
        // TODO test remaining results
        String content = "{\"workRequirementIds\":[7,8]}";
        assertSuccess(content)

                .andExpect(jsonPath("$.results[3].globalInstruction").value(true))
                .andExpect(jsonPath("$.results[3].description").value(""))
                .andExpect(jsonPath("$.results[3].procedures").value(""))
                .andExpect(jsonPath("$.results[3].text").value("Test Company: Company Instruction 1 - Calibration"))

                .andExpect(jsonPath("$.results[4].globalInstruction").value(true))
                .andExpect(jsonPath("$.results[4].description").value(""))
                .andExpect(jsonPath("$.results[4].procedures").value(""))
                .andExpect(jsonPath("$.results[4].text").value("Test Contact: Contact Instruction 2  - Calibration"));
    }

	/* We expect: (description / procedures not shown)
		{"globalInstruction":false,"workRequirementId":4,"text":"Calibration instructions: Model Public Instructions. Model Private Instructions."},
		{"globalInstruction":false,"workRequirementId":5,"text":"Calibration instructions: Company Model Public Instructions"},
		{"globalInstruction":true,"workRequirementId":null,"text":"Test Company: Company Instruction - Calibration"},
		{"globalInstruction":true,"workRequirementId":null,"text":"Test Contact: Contact Instruction - Calibration"},
		{"globalInstruction":true,"workRequirementId":null,"text":"Job Instruction - Calibration"},
		{"globalInstruction":true,"workRequirementId":null,"text":"Test Subdiv: Subdiv Instruction - Calibration"}
	*/
	@Test
	public void testSuccess_SingleCalReq() throws Exception {
		// TODO test remaining results
		String content = "{\"workRequirementIds\":[4, 5]}";
		assertSuccess(content)
	    .andExpect(jsonPath("$.results[6]").exists())
	    .andExpect(jsonPath("$.results[7]").doesNotExist())
		.andExpect(jsonPath("$.results[0].globalInstruction").value(false))
		.andExpect(jsonPath("$.results[0].description").value("4 SN4 AGILENT 34902A"))
		.andExpect(jsonPath("$.results[0].procedures").value("10001 Procedure One"))
		.andExpect(jsonPath("$.results[0].text").value("Text Requirement for WR 4"));
	}
	
	/* We Expect: (description / procedures not shown)
	 * 1 - "Calibration instructions: Model Public Instructions. Model Private Instructions." 
		{"globalInstruction":false,"workRequirementId":1,"text":"Requirement Instruction"},
		{"globalInstruction":false,"workRequirementId":1,"text":"Calibration instructions: Job Item Public Instructions"},
		{"globalInstruction":false,"workRequirementId":2,"text":"Calibration instructions: Instrument Public Instructions"},
		{"globalInstruction":false,"workRequirementId":3,"text":"Calibration instructions: Company Model Public Instructions"},
		{"globalInstruction":false,"workRequirementId":4,"text":"Calibration instructions: Model Public Instructions"},
		{"globalInstruction":true,"workRequirementId":null,"text":"Test Company: Company Instruction - Calibration"},
		{"globalInstruction":true,"workRequirementId":null,"text":"Test Contact: Contact Instruction - Calibration"},
		{"globalInstruction":true,"workRequirementId":null,"text":"Job Instruction - Calibration"},
		{"globalInstruction":true,"workRequirementId":null,"text":"Test Subdiv: Subdiv Instruction - Calibration"} 
	 */
	@Test
	public void testSuccess_MultipleCalReq() throws Exception {
		// TODO test remaining results
		String content = "{\"workRequirementIds\":[1, 2, 3, 4]}";
		assertSuccess(content)
	    .andExpect(jsonPath("$.results[9]").exists())
	    .andExpect(jsonPath("$.results[10]").doesNotExist())
		.andExpect(jsonPath("$.results[0].globalInstruction").value(false))
		.andExpect(jsonPath("$.results[0].workRequirementId").value(1))
		.andExpect(jsonPath("$.results[0].description").value("1 SN1 AGILENT 34901A"))
		.andExpect(jsonPath("$.results[0].procedures").value("10001 Procedure One"))
		.andExpect(jsonPath("$.results[0].text").value("Requirement Instruction"));
	}
}
