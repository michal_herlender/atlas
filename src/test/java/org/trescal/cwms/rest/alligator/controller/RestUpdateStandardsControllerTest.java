package org.trescal.cwms.rest.alligator.controller;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcRestTestClass;

import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestUpdateStandardsControllerTest extends MvcRestTestClass {
    public static final String DATA_SET = "rest/alligator/AlligatorCertificate.xml";
    public static final String CERTIFICATE_NUMBER_REGULAR = "ES-A-MAD-C-16-000001";
    public static final String CERTIFICATE_NUMBER_ONSITE = "ES-A-MAD-C-16-000006";

    @Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }

	private void assertFail(String content, String expectedMessage, boolean expectStandards) throws Exception {
		ResultActions resultActions = this.mockMvc.perform(
				post("/updatestandards")
				.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
				.content(content)
			)
		.andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.success").value(false))
        .andExpect(jsonPath("$.message").value(expectedMessage))
        .andExpect(jsonPath("$.results").isArray());
        if (expectStandards) {
			resultActions
				.andExpect(jsonPath("$.results[0]").exists())
				.andExpect(jsonPath("$.results[0]").value(2))
				.andExpect(jsonPath("$.results[1]").doesNotExist());
		} else {
			resultActions
				.andExpect(jsonPath("$.results[0]").doesNotExist());
		}
	}

	private <T> ResultActions assertSuccess(String content, Matcher<T> matcher) throws Exception {
		return this.mockMvc.perform(
			post("/updatestandards")
				.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
				.content(content)
		)
			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
			.andExpect(jsonPath("$.message").value(""))
			.andExpect(jsonPath("$.success").value(true))
			.andExpect(jsonPath("$.results").value(matcher));
	}
	@Test
	public void test_ErrorNoInstrumentsFound() throws Exception {
		String content = "{\"certificateNumber\":\""+CERTIFICATE_NUMBER_REGULAR+"\",\"barcodes\":[\"9999\"]}";
		assertFail(content, "0 instruments found for barcode 9999", true);
	}
	@Test
	public void test_ErrorTwoInstrumentsFound() throws Exception {
		String content = "{\"certificateNumber\":\""+CERTIFICATE_NUMBER_REGULAR+"\",\"barcodes\":[\"3\"]}";
		assertFail(content, "2 instruments found for barcode 3", true);
	}
	@Test
	public void test_ErrorStandardPastDue() throws Exception {
		String content = "{\"certificateNumber\":\""+CERTIFICATE_NUMBER_REGULAR+"\",\"barcodes\":[\"FB5\"]}";
		assertFail(content, "Instrument 5 with barcode FB5 is past due", true);
	}
	// Search now restricted to instruments from same company
/*	@Test
	public void test_ErrorOtherCompany() throws Exception {
		String content = "{\"certificateNumber\":\""+CERTIFICATE_NUMBER+"\",\"barcodes\":[\"FB8\"]}";
		assertFail(content, "Instrument 8 with barcode FB8 belongs to another company", true);
	} */
	@Test
	public void test_ErrorNotFoundOtherCompany() throws Exception {
		String content = "{\"certificateNumber\":\""+CERTIFICATE_NUMBER_REGULAR+"\",\"barcodes\":[\"FB8\"]}";
		assertFail(content, "0 instruments found for barcode FB8", true);
	}
	@Test
	public void test_ErrorCertificateNotFound() throws Exception {
		String content = "{\"certificateNumber\":\"BAD1234\",\"barcodes\":[\"6\",\"7\"]}";
		assertFail(content, "Certificate BAD1234 not found", false);
	}
	@Test
	public void test_ErrorBER() throws Exception {
		String content = "{\"certificateNumber\":\""+CERTIFICATE_NUMBER_REGULAR+"\",\"barcodes\":[\"FB9\"]}";
		assertFail(content, "Instrument 9 with barcode FB9 has B.E.R. status", true);
	}
	@Test
	public void test_ErrorNotStandard() throws Exception {
		String content = "{\"certificateNumber\":\""+CERTIFICATE_NUMBER_REGULAR+"\",\"barcodes\":[\"FB10\"]}";
		assertFail(content, "Instrument 10 with barcode FB10 is not a calibration standard", true);
	}
	@Test
	public void test_ErrorScrapped() throws Exception {
		String content = "{\"certificateNumber\":\""+CERTIFICATE_NUMBER_REGULAR+"\",\"barcodes\":[\"FB11\"]}";
		assertFail(content, "Instrument 11 with barcode FB11 is scrapped", true);
	}
	@Test
	public void test_SuccessAddStandard() throws Exception {
		String content = "{\"certificateNumber\":\""+CERTIFICATE_NUMBER_REGULAR+"\",\"barcodes\":[\"2\",\"6\"]}";
		assertSuccess(content, Matchers.containsInAnyOrder(2,6));
	}
	@Test
	public void test_SuccessDeleteStandard() throws Exception {
		String content = "{\"certificateNumber\":\"" + CERTIFICATE_NUMBER_REGULAR + "\",\"barcodes\":[]}";
		assertSuccess(content, Matchers.emptyIterable());
	}
	@Test
	public void test_SuccessReplaceStandards() throws Exception {
		String content = "{\"certificateNumber\":\""+CERTIFICATE_NUMBER_REGULAR+"\",\"barcodes\":[\"6\",\"7\"]}";
		assertSuccess(content, Matchers.containsInAnyOrder(6,7));
	}
	@Test
	public void test_SuccessAddStandardOnsite() throws Exception {
		String content = "{\"certificateNumber\":\""+CERTIFICATE_NUMBER_ONSITE+"\",\"barcodes\":[\"2\",\"6\"]}";
		assertSuccess(content, Matchers.containsInAnyOrder(2,6));
	}
	@Test
	public void test_SuccessDeleteStandardOnsite() throws Exception {
		String content = "{\"certificateNumber\":\"" + CERTIFICATE_NUMBER_ONSITE + "\",\"barcodes\":[]}";
		assertSuccess(content, Matchers.emptyIterable());
	}
	@Test
	public void test_SuccessReplaceStandardsOnsite() throws Exception {
		String content = "{\"certificateNumber\":\""+CERTIFICATE_NUMBER_ONSITE+"\",\"barcodes\":[\"6\",\"7\"]}";
		assertSuccess(content, Matchers.containsInAnyOrder(6,7));
	}
}