package org.trescal.cwms.rest.alligator.controller;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcRestTestClass;

import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestCalibrationOutcomeControllerTest extends MvcRestTestClass {

    public String DATA_SET = "rest/alligator/AlligatorCalibration.xml";

    @Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }
	
	@Test
	public void testReferenceData_English() throws Exception {
		this.mockMvc.perform(get("/calibrationoutcome")
		.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)))
		.andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.20").value("Calibrated successfully"))
        .andExpect(jsonPath("$.83").value("Calibration placed on hold"))
        .andExpect(jsonPath("$.10").doesNotExist());
	}
	@Test
	public void testReferenceData_EnglishFallback() throws Exception {
		this.mockMvc.perform(get("/calibrationoutcome?locale=zh_CN")
		.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)))
		.andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.20").value("Calibrated successfully"))
        .andExpect(jsonPath("$.83").value("Calibration placed on hold"))
        .andExpect(jsonPath("$.10").doesNotExist());
	}
	@Test
	public void testReferenceData_Spanish() throws Exception {
		this.mockMvc.perform(get("/calibrationoutcome?locale=es_ES")
		.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)))
		.andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.20").value("Calibrado con \u00E9xito"))
        .andExpect(jsonPath("$.83").value("Calibraci\u00F3n en suspenso"))
        .andExpect(jsonPath("$.10").doesNotExist());
	}
}
