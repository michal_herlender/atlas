package org.trescal.cwms.rest.api.document;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.FileOutputStream;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.trescal.cwms.core.documents.filenamingservice.InvoiceNamingService;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.FileTools;
import org.trescal.cwms.rest.api.document.dto.RestApiDocumentInputDTO;
import org.trescal.cwms.rest.api.document.dto.RestApiDocumentOutputDTO;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetPricing;
import org.trescal.cwms.tests.generic.pricing.TestDataInvoice;

import com.fasterxml.jackson.databind.ObjectMapper;

public class RestApiDocumentControllerTest extends MvcRestTestClass {

	// Taken from
	// http://stackoverflow.com/questions/17279712/what-is-the-smallest-possible-valid-pdf
	public static final String SMALL_PDF = "%PDF-1.\r\ntrailer<</Root<</Pages<</Kids[<</MediaBox[0 0 3 3]>>]>>>>>>";
	
	private ObjectMapper mapper = new ObjectMapper();
	
	@Autowired
	private InvoiceService invoiceService;
	
	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetPricing.getCreditNoteTestData();
	}
	
	/**
	 * Transactional due to invoice naming
	 */
	@Test
	@Transactional
	public void shouldReturnInvoiceDocumentById() throws Exception {
		Integer invoiceid = TestDataInvoice.INVOICE_ID_US_150;
		
		Invoice invoice = this.invoiceService.findInvoice(invoiceid);
		// Ensures that new invoice file is generated as rev 1 in file system for each test
		FileTools.deleteFiles(invoice.getDirectory());
		
		InvoiceNamingService fnService = new InvoiceNamingService(invoice);
		String documentFileName = fnService.getFileName()+".pdf";
		try (FileOutputStream fos = new FileOutputStream(documentFileName)) {
			fos.write(SMALL_PDF.getBytes());
		}
		
		RestApiDocumentInputDTO inputDto = RestApiDocumentInputDTO.builder()
				.id(invoiceid)
				.build();
		
		String content = mapper.writeValueAsString(inputDto);
		
		MvcResult result = this.mockMvc.perform(
				post("/api/document/invoice")
				.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.contentType(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.content(content)
			)
		.andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
        .andReturn();
		
		String filename = "Invoice "+TestDataInvoice.INVOICE_NUMBER_US_150+" rev 1"+".pdf";
		
		RestApiDocumentOutputDTO expectedOutputDto = RestApiDocumentOutputDTO.builder()
				.data(SMALL_PDF.getBytes())
				.filename(filename)
				.build();
		RestResultDTO<RestApiDocumentOutputDTO> expectedResult = new RestResultDTO<RestApiDocumentOutputDTO>(true,"");
		expectedResult.addResult(expectedOutputDto);
		
		String output = result.getResponse().getContentAsString();
		String expectedOutput = mapper.writeValueAsString(expectedResult);
		
		Assert.assertEquals(mapper.readTree(expectedOutput), mapper.readTree(output));
	}

}
