package org.trescal.cwms.rest.api.invoice;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetPricing;
import org.trescal.cwms.tests.generic.pricing.TestDataExpenseItem;
import org.trescal.cwms.tests.generic.pricing.TestDataInvoice;
import org.trescal.cwms.tests.generic.pricing.TestDataInvoiceItem;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestApiInvoiceViewControllerTest extends MvcRestTestClass {

    @Override
    protected List<String> getDataSetFileNames() {
        return DataSetPricing.getInvoiceTestData();
    }

    @Test
    public void shouldReturnInvoiceView() throws Exception {
		Integer invoiceid = TestDataInvoice.INVOICE_ID_US_150;
		
		this.mockMvc.perform(
				get("/api/invoice/details?id="+invoiceid)
				.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
			)
		.andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.message").value(""))
        .andExpect(jsonPath("$.success").value(true))
        .andExpect(jsonPath("$.results").isArray())
        .andExpect(jsonPath("$.results[0]").exists())
        .andExpect(jsonPath("$.results[1]").doesNotExist())
        .andExpect(jsonPath("$.results[0].invoiceItems").exists())
        .andExpect(jsonPath("$.results[0].invoiceItems").isArray())
        .andExpect(jsonPath("$.results[0].invoiceItems[0]").exists())
        .andExpect(jsonPath("$.results[0].invoiceItems[7]").doesNotExist())
        .andExpect(jsonPath("$.results[0].invoiceItems[0].itemId").value(TestDataInvoiceItem.ITEM_ID_US_1503))
        .andExpect(jsonPath("$.results[0].invoiceItems[0].itemNumber").value(TestDataInvoiceItem.ITEM_NUMBER_US_1503))
        .andExpect(jsonPath("$.results[0].invoiceItems[0].description").value(TestDataInvoiceItem.DESCRIPTION_US_1503))
        .andExpect(jsonPath("$.results[0].invoiceItems[0].quantity").value(TestDataInvoiceItem.QUANTITY_US_1503))
        .andExpect(jsonPath("$.results[0].invoiceItems[0].finalCost").value(TestDataInvoiceItem.FINAL_COST_US_1503.doubleValue()))
        .andExpect(jsonPath("$.results[0].invoiceItems[0].taxable").value(TestDataInvoiceItem.TAXABLE_US_1503))
        .andExpect(jsonPath("$.results[0].invoiceItems[0].taxAmount").value(TestDataInvoiceItem.TAX_AMOUNT_US_1503.doubleValue()))
        .andExpect(jsonPath("$.results[0].invoiceItems[0].costType").isEmpty())
        .andExpect(jsonPath("$.results[0].invoiceItems[0].jobItemId").isEmpty())
        .andExpect(jsonPath("$.results[0].invoiceItems[0].expenseItemNumber").value(TestDataExpenseItem.ITEM_NUMBER_US_1503))
        .andExpect(jsonPath("$.results[0].invoiceItems[0].expenseItemJobId").value(TestDataExpenseItem.JOB_ID_US_1503))
        .andExpect(jsonPath("$.results[0].invoiceItems[0].expenseItemModelId").value(TestDataExpenseItem.MODEL_ID_US_1503))
        .andExpect(jsonPath("$.results[0].invoiceItems[0].expenseItemClientRef").value(TestDataExpenseItem.CLIENT_REF_US_1503))
        
        .andExpect(jsonPath("$.results[0].invoiceItems[2].itemId").value(TestDataInvoiceItem.ITEM_ID_US_15011))
        .andExpect(jsonPath("$.results[0].invoiceItems[2].jobItemId").value(TestDataInvoiceItem.JOB_ITEM_ID_US_15011))
        .andExpect(jsonPath("$.results[0].invoiceItems[2].costType").value(TestDataInvoiceItem.COST_TYPE_US_15011.getName()))
        .andExpect(jsonPath("$.results[0].invoiceItems[2].expenseItemNumber").isEmpty())
        .andExpect(jsonPath("$.results[0].invoiceItems[2].expenseItemJobId").isEmpty())
        .andExpect(jsonPath("$.results[0].invoiceItems[2].expenseItemModelId").isEmpty())
        .andExpect(jsonPath("$.results[0].invoiceItems[2].expenseItemClientRef").isEmpty())
        ;
	}

}
