package org.trescal.cwms.rest.api;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.trescal.cwms.http.AbstractOAuth2Test;

import com.gargoylesoftware.htmlunit.HttpMethod;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.WebResponse;
import com.gargoylesoftware.htmlunit.util.NameValuePair;

import lombok.extern.slf4j.Slf4j;

/**
 * Tests that the basic "ping" api can be accessed with in two different ways:
 * (a) via the access token provided as a bearer token in the request header
 * (b) via the access token provided as a request parameter 
 *
 */
@Slf4j
public class RestApiPingTest extends AbstractOAuth2Test {

	public static final String ENDPOINT_API_PING = "/rest/api/ping";
	
	@Test
	public void pingTestWithAccessTokenAsBearerToken() throws Exception {
		String baseurl = System.getProperty(PROPERTY_BASE_URL);
		
		String pingUrl = baseurl+ENDPOINT_API_PING;
		log.info("Ping test to : "+pingUrl);
		URL url = new URL(pingUrl);

		WebRequest request = new WebRequest(url);
		request.setHttpMethod(HttpMethod.GET);
		request.setAdditionalHeader("Accept", "application/json");
		request.setAdditionalHeader("Authorization", "Bearer "+accessToken);
		
		getPageAndAssertSucccess(request);
	}
	
	@Test
	public void pingTestWithAccessTokenAsRequestParam() throws Exception {
		String baseurl = System.getProperty(PROPERTY_BASE_URL);
		
		String pingUrl = baseurl+ENDPOINT_API_PING;
		log.info("Ping test to : "+pingUrl);
		URL url = new URL(pingUrl);

		WebRequest request = new WebRequest(url);
		request.setHttpMethod(HttpMethod.GET);
		request.setAdditionalHeader("Accept", "application/json");

		List<NameValuePair> requestParameters = new ArrayList<NameValuePair>(); 
		requestParameters.add(new NameValuePair(PARAM_ACCESS_TOKEN, accessToken));
		request.setRequestParameters(requestParameters);
		
		getPageAndAssertSucccess(request);
	}
	
	private void getPageAndAssertSucccess(WebRequest request) throws Exception {
		log.info("request : "+request.toString());

		Page page = webClient.getPage(request);
		WebResponse response = page.getWebResponse();
		if (response.getContentType().equals("application/json")) {
			String json = response.getContentAsString();
			log.info("response : "+json);
			String expected = "{\"success\":true,\"message\":\"\",\"results\":[],\"errors\":[]}";
			Assert.assertEquals(expected, json);
		}
		else {
			Assert.fail("Unexpected content type : "+response.getContentType());
		}
	}
}
