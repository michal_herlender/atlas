package org.trescal.cwms.rest.api.jobitem;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetCapability;
import org.trescal.cwms.tests.generic.DataSetInstrument;
import org.trescal.cwms.tests.generic.DataSetJob;
import org.trescal.cwms.tests.generic.address.TestDataAddress;
import org.trescal.cwms.tests.generic.company.TestDataContact;
import org.trescal.cwms.tests.generic.core.TestDataItemState;
import org.trescal.cwms.tests.generic.core.TestDataServiceType;
import org.trescal.cwms.tests.generic.instrument.TestDataInstrument;
import org.trescal.cwms.tests.generic.instrument.TestDataProcs;
import org.trescal.cwms.tests.generic.job.TestDataJob;

import javax.transaction.Transactional;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Transactional
@Disabled
public class RestApiJobItemCreateControllerTest extends MvcRestTestClass {

    @Autowired
    private InstrumService instService;

    @Override
    protected List<String> getDataSetFileNames() {
        List<String> result = DataSetJob.getJobClientEmpty();
        result.add(DataSetInstrument.FILE_INSTRUMENT_CLIENT_ADDITIONAL);
        result.add(DataSetCapability.FILE_PROCEDURE_SUBFAMILY);                // Additional subfamily capabilities and filters
        return result;
    }

    private void assertSuccess(String content) throws Exception {
        this.mockMvc.perform(
            post("/api/jobitem/create")
                .accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
                .contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
                .content(content)
        )
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.message").value(""))
            .andExpect(jsonPath("$.success").value(true))
            .andExpect(jsonPath("$.results").isArray())
            .andExpect(jsonPath("$.results[0]").exists())
            .andExpect(jsonPath("$.results[0].jobItemId").exists())
            .andExpect(jsonPath("$.results[0].jobItemId").isNumber())
            .andExpect(jsonPath("$.results[1]").doesNotExist());

    }

    private void assertFail(String content, String expectedMessage) throws Exception {
        this.mockMvc.perform(
            post("/api/jobitem/create")
                .accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
                .contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
                .content(content)
        )
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.message").value(expectedMessage))
            .andExpect(jsonPath("$.success").value(false))
            .andExpect(jsonPath("$.results").isArray())
            .andExpect(jsonPath("$.results[0]").doesNotExist());
    }

    /**
     * With optional contactId field AND
     * automaticContractReview = true AND
     * automaticCapability = true
     * (but for an instrument / capability which should look up OK)
     */
    @Test
    @WithMockUser("hartland_user")
    public void testCreateSuccessMaximal() throws Exception {
        assertSuccess("{"
            + "\"contactId\" : " + TestDataContact.ID_BUSINESS_USA + ","
            + "\"plantId\" : " + TestDataInstrument.ID_USA_ADDITIONAL_CALIPER_1503 + ","
            + "\"jobId\" : " + TestDataJob.ID_CLIENT_US_ADDITIONAL_151 + ","
				+ "\"serviceTypeId\" : "+TestDataServiceType.ID_AC_M_IL+","
            + "\"addressId\" : " + TestDataAddress.ID_ADDRESS_HARTLAND + ","
            + "\"automaticContractReview\" : true,"
            + "\"automaticCapability\" : true"
            + "}");

        // We expect the default capability lookup to use the CALIPER capability
        // We expect the "automatic" contract review to have progressed the job item to "Awaiting Calibration" status
        testInstrument(TestDataInstrument.ID_USA_ADDITIONAL_CALIPER_1503,
            TestDataProcs.PROCS_ADDITIONAL_US_CALIPER_5504,
            TestDataItemState.ID_AWAITING_CALIBRATION_19);
    }

    /**
     * With optional contactId field AND
     * automaticContractReview = true AND
     * automaticCapability = true
     * (but for an instrument / capability / service type which should only match at fallback EQUIPMENT MISC level)
     * i.e. traceable
     */
    @Test
    @WithMockUser("hartland_user")
    public void testCreateSuccessAutomaticCapability() throws Exception {
        assertSuccess("{"
            + "\"contactId\" : " + TestDataContact.ID_BUSINESS_USA + ","
            + "\"plantId\" : " + TestDataInstrument.ID_USA_ADDITIONAL_CALIPER_1503 + ","
            + "\"jobId\" : " + TestDataJob.ID_CLIENT_US_ADDITIONAL_151 + ","
				+ "\"serviceTypeId\" : "+TestDataServiceType.ID_TC_M_IL+","
            + "\"addressId\" : " + TestDataAddress.ID_ADDRESS_HARTLAND + ","
            + "\"automaticContractReview\" : true,"
            + "\"automaticCapability\" : true"
            + "}");

        // We expect the "automatic" capability lookup to use the EQUIPMENT_MISC capability
        // We expect the "automatic" contract review to have progressed the job item to "Awaiting Calibration" status
        testInstrument(TestDataInstrument.ID_USA_ADDITIONAL_CALIPER_1503,
            TestDataProcs.PROCS_ADDITIONAL_US_MISC_5502,
            TestDataItemState.ID_AWAITING_CALIBRATION_19);
    }

    private void testInstrument(int plantid, int expectedProcId, Integer expectedStateId) {
        Instrument inst = this.instService.get(plantid);
        assertNotNull(inst.getJobItems());
        assertEquals(1, inst.getJobItems().size());
        JobItem ji = inst.getJobItems().iterator().next();
        assertEquals(1, ji.getWorkRequirements().size());

        JobItemWorkRequirement jiwr = ji.getWorkRequirements().iterator().next();
        assertNotNull(jiwr.getWorkRequirement().getCapability());
        assertEquals(expectedProcId, jiwr.getWorkRequirement().getCapability().getId());

        assertNotNull(ji.getState());
        assertEquals(expectedStateId, ji.getState().getStateid());
    }

    /**
     * With optional contactId field
     */
    @Test
    @WithMockUser("hartland_user")
    public void testCreateSuccessOptionalParams() throws Exception {
        assertSuccess("{"
            + "\"contactId\" : " + TestDataContact.ID_BUSINESS_USA + ","
            + "\"plantId\" : " + TestDataInstrument.ID_USA_ADDITIONAL_CALIPER_1503 + ","
            + "\"jobId\" : " + TestDataJob.ID_CLIENT_US_ADDITIONAL_151 + ","
				+ "\"serviceTypeId\" : "+TestDataServiceType.ID_AC_M_IL+","
            + "\"addressId\" : " + TestDataAddress.ID_ADDRESS_HARTLAND
            + "}");

        // We expect the default capability lookup to use the CALIPER capability
        // We expect the default status to be "Awaiting Contract Review"
        testInstrument(TestDataInstrument.ID_USA_ADDITIONAL_CALIPER_1503,
            TestDataProcs.PROCS_ADDITIONAL_US_CALIPER_5504,
            TestDataItemState.ID_AWAITING_CONTRACT_REVIEW_7);
    }

    /**
     * Without optional contactId field
     */
    @Test
    @WithMockUser("hartland_user")
    public void testCreateSuccessMinimal() throws Exception {
        assertSuccess("{"
            + "\"plantId\" : " + TestDataInstrument.ID_USA_ADDITIONAL_CALIPER_1503 + ","
            + "\"jobId\" : " + TestDataJob.ID_CLIENT_US_ADDITIONAL_151 + ","
				+ "\"serviceTypeId\" : "+TestDataServiceType.ID_AC_M_IL+","
            + "\"addressId\" : " + TestDataAddress.ID_ADDRESS_HARTLAND
            + "}");

        // We expect the default capability lookup to use the CALIPER capability
        // We expect the default status to be "Awaiting Contract Review"
        testInstrument(TestDataInstrument.ID_USA_ADDITIONAL_CALIPER_1503,
            TestDataProcs.PROCS_ADDITIONAL_US_CALIPER_5504,
            TestDataItemState.ID_AWAITING_CONTRACT_REVIEW_7);
    }

    /**
     * Test all validation failures at once
     * Remove all fields (should have validation failures for all optional fields)
     *
     * TODO check for specific validation messages (maybe better handled via validator unit / integration testing)
     */
    @Test
    @WithMockUser("hartland_user")
    public void testCreateFailureValidation() throws Exception {
        assertFail("{"
                + "\"plantId\" : null,"
                + "\"jobId\" : null,"
                + "\"serviceTypeId\" : null,"
                + "\"addressId\" : null"
                + "}",
            "One or more errors are present, please see below:");
    }
}
