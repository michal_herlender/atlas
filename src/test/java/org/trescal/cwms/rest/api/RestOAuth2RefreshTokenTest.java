package org.trescal.cwms.rest.api;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.auth.UsernamePasswordCredentials;
import org.junit.Test;
import org.trescal.cwms.http.AbstractOAuth2Test;

import com.gargoylesoftware.htmlunit.HttpMethod;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.util.NameValuePair;

import lombok.extern.slf4j.Slf4j;

/*
 * This tests that we can successfully use the "refresh token" obtained in initial authorization 
 * (see @BeforeClass in AbstractOAuth2Test) to obtain a new OAuth token. 
 */

@Slf4j
public class RestOAuth2RefreshTokenTest extends AbstractOAuth2Test {

	@Test
	public void refreshTokenTest() throws Exception {
		String baseurl = System.getProperty(PROPERTY_BASE_URL);
		String app_username = System.getProperty(PROPERTY_APP_USERNAME);
		String app_password = System.getProperty(PROPERTY_APP_PASSWORD);
		
		String authenticationUrl = baseurl+ENDPOINT_OAUTH_TOKEN;
		log.info("Refresh test to : "+authenticationUrl);
		URL url = new URL(authenticationUrl);
		
		WebRequest request = new WebRequest(url);
		// Must use POST for refresh token 
		request.setHttpMethod(HttpMethod.POST);
		request.setAdditionalHeader("Accept", "application/json");
		request.setCredentials(new UsernamePasswordCredentials(app_username, app_password));

		List<NameValuePair> requestParameters = new ArrayList<NameValuePair>(); 
		requestParameters.add(new NameValuePair("grant_type", "refresh_token"));
		requestParameters.add(new NameValuePair("scope", "all"));
		requestParameters.add(new NameValuePair(PARAM_REFRESH_TOKEN, refreshToken));
		request.setRequestParameters(requestParameters);
		
		log.info("request : "+request.toString());
		
		getPageAndStoreToken(request);
	}
}
