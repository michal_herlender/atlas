package org.trescal.cwms.rest.api.calibration;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetJob;
import org.trescal.cwms.tests.generic.core.TestDataItemState;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestApiCalibrationStartControllerTest extends MvcRestTestClass {

    @Autowired
    private JobItemService jobItemService;

    @Override
    protected List<String> getDataSetFileNames() {
        return DataSetJob.getJobClientAwaitingCalibrationTestData();
    }
	
	private void assertFail(String content, String expectedMessage) throws Exception {
		this.mockMvc.perform(
				post("/api/calibration/start")
				.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
				.content(content)
			)
		.andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.message").value(expectedMessage))
        .andExpect(jsonPath("$.success").value(false))
        .andExpect(jsonPath("$.results").isArray())
        .andExpect(jsonPath("$.results[0]").doesNotExist());
	}
	
	private void assertSuccess(String content) throws Exception {
		StringBuffer documentNumber = new StringBuffer("USTUS-DTW-CI-");
		documentNumber.append((new SimpleDateFormat("yy").format(new Date())));
		documentNumber.append("000001");
		
		this.mockMvc.perform(
				post("/api/calibration/start")
				.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
				.content(content)
			)
		.andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.message").value(""))
        .andExpect(jsonPath("$.success").value(true))
        .andExpect(jsonPath("$.results").isArray())
        .andExpect(jsonPath("$.results[0]").exists())
        .andExpect(jsonPath("$.results[1]").doesNotExist())
        .andExpect(jsonPath("$.results[0].calibrationId").exists())
                .andExpect(jsonPath("$.results[0].certificateId").exists())
                .andExpect(jsonPath("$.results[0].documentNumber").exists())
                .andExpect(jsonPath("$.results[0].documentNumber").value(documentNumber.toString()));
    }

    private void assertJobItemInState(Integer jobItemId, Integer expectedStateId) {
        JobItem jobItem = this.jobItemService.get(jobItemId);
        assertEquals(expectedStateId, jobItem.getState().getStateid());

    }


    @Test
    @WithMockUser("rungis_user")
    public void testBypassAuthorizationStartCalibration() throws Exception {
        String content = "{"
                + "\"jobItemIds\" : [1511],"
                + "\"bypassAuthorisation\" : true"
                + "}";
        assertSuccess(content);
    }

    @Test
	@WithMockUser("rungis_user")
	public void testNotAuthorizedToStartCalibration() throws Exception {
		String content = "{\"jobItemIds\" : [1511]}";
		assertFail(content, "One or more errors are present, please see below:");
	}
	
	@Test
	@WithMockUser("hartland_user")
	public void testMinimumStartCalibration() throws Exception {
		String content = "{\"jobItemIds\" : [1511]}";
		assertSuccess(content);

		assertJobItemInState(1511, TestDataItemState.ID_PRE_CALIBRATION_26);
	}
	
	@Test
	@WithMockUser("hartland_user")
	public void testMaximumStartCalibration() throws Exception {
		String content = "{"
				+ "\"jobItemIds\" : [1511],"
				+ "\"contactId\" : 550,"
				+ "\"startTime\" : \"2021-04-01T01:23:45.678+02:00\","
				+ "\"dataContent\" : \"AS_LEFT\","
				+ "\"processName\" : \"Calypso\","
				+ "\"bypassAuthorisation\" : false"
				+ "}";
		assertSuccess(content);
	}
	
	@Test
	@WithMockUser("hartland_user")
	public void testMinimumStartMultipleJobItems() throws Exception {
		String content = "{\"jobItemIds\" : [1511, 1513]}";
		assertSuccess(content);
		
		assertJobItemInState(1511, TestDataItemState.ID_PRE_CALIBRATION_26);
		assertJobItemInState(1513, TestDataItemState.ID_PRE_CALIBRATION_26);
	}
}