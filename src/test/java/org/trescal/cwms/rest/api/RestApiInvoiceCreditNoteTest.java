package org.trescal.cwms.rest.api;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.trescal.cwms.http.AbstractOAuth2Test;

import com.gargoylesoftware.htmlunit.HttpMethod;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.WebResponse;
import com.gargoylesoftware.htmlunit.util.NameValuePair;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RestApiInvoiceCreditNoteTest extends AbstractOAuth2Test {

public static final String ENDPOINT_API_PING = "/rest/api/data/invoice";
	
	@Test
	public void pingTest() throws Exception {
		String baseurl = System.getProperty(PROPERTY_BASE_URL);
		
		String pingUrl = baseurl+ENDPOINT_API_PING;
		log.info("Ping test to : "+pingUrl);
		URL url = new URL(pingUrl);

		WebRequest request = new WebRequest(url);
		request.setHttpMethod(HttpMethod.POST);
		request.setAdditionalHeader("Accept", "application/json;charset=UTF-8");
		request.setAdditionalHeader("Content-Type", "application/json");

		List<NameValuePair> requestParameters = new ArrayList<NameValuePair>(); 
		requestParameters.add(new NameValuePair(PARAM_ACCESS_TOKEN, accessToken));
		request.setRequestParameters(requestParameters);
		
		log.info("request : "+request.toString());
		webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
		Page page = webClient.getPage(request);
		WebResponse response = page.getWebResponse();
		if (response.getContentType().equals("application/json")) {
			String json = response.getContentAsString();
			log.info("response : "+json);
		}
		else {
			Assert.fail("Unexpected content type : "+response.getContentType());
		}
	}
}
