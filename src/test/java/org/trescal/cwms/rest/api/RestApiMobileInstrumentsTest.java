package org.trescal.cwms.rest.api;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.trescal.cwms.http.AbstractOAuth2Test;

import com.gargoylesoftware.htmlunit.HttpMethod;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.WebResponse;
import com.gargoylesoftware.htmlunit.util.NameValuePair;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RestApiMobileInstrumentsTest extends AbstractOAuth2Test {

	public static final String ENDPOINT_API_MOBILE_INSTRUMENTS = "/rest/api/mobile/instruments";
	public static final String PARAM_BARCODE = "barcode";
	public static final String BARCODE_18383 = "18383"; 
	
	@Test
	public void pingTest() throws Exception {
		String baseurl = System.getProperty(PROPERTY_BASE_URL);
		
		String endpointUrl = baseurl+ENDPOINT_API_MOBILE_INSTRUMENTS;
		log.info("Ping test to : "+endpointUrl);
		URL url = new URL(endpointUrl);

		WebRequest request = new WebRequest(url);
		request.setHttpMethod(HttpMethod.GET);
		request.setAdditionalHeader("Accept", "application/json");

		List<NameValuePair> requestParameters = new ArrayList<NameValuePair>(); 
		requestParameters.add(new NameValuePair(PARAM_ACCESS_TOKEN, accessToken));
		requestParameters.add(new NameValuePair(PARAM_BARCODE, BARCODE_18383));
		request.setRequestParameters(requestParameters);
		
		log.info("request : "+request.toString());
		
		Page page = webClient.getPage(request);
		WebResponse response = page.getWebResponse();
		if (response.getContentType().equals("application/json")) {
			String json = response.getContentAsString();
			log.info("response : "+json);
		}
		else {
			Assert.fail("Unexpected content type : "+response.getContentType());
		}
	}

}
