package org.trescal.cwms.rest.api.calibration;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetJob;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestApiCalibrationCompleteControllerTest extends MvcRestTestClass {

    @Override
    protected List<String> getDataSetFileNames() {
        return DataSetJob.getJobClientPreCalibrationTestData();
    }

    private void assertFail(String content, String expectedMessage) throws Exception {
        this.mockMvc.perform(
                        post("/api/calibration/complete")
				.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
				.content(content)
			)
		.andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.message").value(expectedMessage))
        .andExpect(jsonPath("$.success").value(false))
        .andExpect(jsonPath("$.results").isArray())
        .andExpect(jsonPath("$.results[0]").doesNotExist());
	}

	private void assertSuccess(String content) throws Exception {
		this.mockMvc.perform(
				post("/api/calibration/complete")
				.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
				.content(content)
			)
		.andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.message").value(""))
        .andExpect(jsonPath("$.success").value(true));
	}
	
	@Test
	@WithMockUser("hartland_user")
	public void testMinimumCompleteCalibration() throws Exception {
		String content = "{"
				+ "\"jobItemIds\" : [1511],"
				+ "\"actionOutcome\" : \"SUCCESSFUL\""
				+ "}";
		assertSuccess(content);
	}

	@Test
	@WithMockUser("rungis_user")
	public void testFailNotAuthorised() throws Exception {
		String content = "{"
				+ "\"jobItemIds\" : [1511],"
				+ "\"actionOutcome\" : \"SUCCESSFUL\""
				+ "}";
		assertFail(content, "One or more errors are present, please see below:");
	}

	@Test
	@WithMockUser("rungis_user")
	public void testBypassAuthorisation() throws Exception {
		String content = "{"
				+ "\"jobItemIds\" : [1511],"
				+ "\"actionOutcome\" : \"SUCCESSFUL\","
				+ "\"bypassAuthorisation\" : true"
				+ "}";
		assertSuccess(content);
	}
	
	@Test
	@WithMockUser("hartland_user")
	public void testMaximumCompleteCalibration() throws Exception {
		String content = "{"
				+ "\"jobItemIds\" : [1511],"
				+ "\"actionOutcome\" : \"SUCCESSFUL\","
				+ "\"dataContent\" : \"AS_FOUND\","
				+ "\"contactId\" : 550,"
				+ "\"startTime\" : \"2021-03-17T13:00:00.000+02:00\","
				+ "\"completeTime\" : \"2021-03-17T15:00:00.000+02:00\","
				+ "\"calibrationDate\" : \"2021-03-17\","
				+ "\"calibrationVerificationStatus\" : \"ACCEPTABLE\","
				+ "\"remark\" : \"Sample remark\","
				+ "\"bypassAuthorisation\" : true,"
				+ "\"workingTimes\" : {\"1511\" : 15}"
				+ "}";
		assertSuccess(content);
	}
	
}
