package org.trescal.cwms.rest.api.instrument;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	RestApiInstrumentCreateControllerTest.class,
	RestApiInstrumentUpdateControllerTest.class
})

public class RestApiInstrumentTestSuite {
	
}
