package org.trescal.cwms.rest.api.certificate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.rest.api.certificate.dto.RestApiUploadClientCertificateInputDto;
import org.trescal.cwms.rest.api.certificate.dto.RestApiUploadClientCertificateOutputDto;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;
import org.trescal.cwms.spring.matchers.AssignmentResult;
import org.trescal.cwms.spring.matchers.AssignmentResultHandler;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetInstrument;
import org.trescal.cwms.tests.generic.company.TestDataCompany;
import org.trescal.cwms.tests.generic.company.TestDataContact;
import org.trescal.cwms.tests.generic.core.TestDataServiceType;
import org.trescal.cwms.tests.generic.instrument.TestDataInstrument;

import java.time.LocalDate;
import java.util.Base64;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class RestApiUploadClientCertificateControllerTest extends MvcRestTestClass {
	
	private static final Integer CONTACT_ID = TestDataContact.ID_CLIENT_FRANCE;
	private static final Integer PLANT_ID = TestDataInstrument.ID_FRANCE_CALIPER_1101; 
	private static final String EXTERNAL_CERT_NUMBER = "TP-12345";
	private static final LocalDate CAL_DATE = LocalDate.of(2021,  12,  8);
	private static final LocalDate CERT_DATE = LocalDate.of(2021,  12,  9);
	private static final Integer DURATION = 12;
	private static final IntervalUnit INTERVAL_UNIT = IntervalUnit.MONTH;
	private static final Boolean UPDATE_DUE_DATE = true;
	private static final Integer EXTERNAL_SUBDIV_ID = TestDataCompany.ID_CLIENT_FRANCE;
	private static final Integer SERVICE_TYPE_ID = TestDataServiceType.ID_SERVICE;
	private static final String REMARKS = "Certificate Remarks";
	private static final String PDF_CONTENT = "%PDF-1.\r\ntrailer<</Root<</Pages<</Kids[<</MediaBox[0 0 3 3]>>]>>>>>>";
	private static final Boolean OPTIMIZATION = true;
	private static final Boolean RESTRICTION = true;
	private static final Boolean ADJUSTMENT = true;
	private static final Boolean REPAIR = true;
	
	@Autowired
	private CertificateService certificateService;
	
	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetInstrument.getInstrumentClientTestData();
	}
	
	@Test
	public void shouldUploadClientCertificate() throws Exception {
		String encodedContent = Base64.getEncoder().encodeToString(PDF_CONTENT.getBytes()); 
		
		RestApiUploadClientCertificateInputDto inputDto = RestApiUploadClientCertificateInputDto.builder()
				.contactId(CONTACT_ID)
				.plantId(PLANT_ID)
				.externalCertificateNumber(EXTERNAL_CERT_NUMBER)
				.calibrationDate(CAL_DATE)
				.certificateDate(CERT_DATE)
				.duration(DURATION)
				.intervalUnit(INTERVAL_UNIT)
				.updateCalibrationDueDate(UPDATE_DUE_DATE)
				.externalSubdivId(EXTERNAL_SUBDIV_ID)
				.serviceTypeId(SERVICE_TYPE_ID)
				.remarks(REMARKS)
				.content(encodedContent)
				.optimization(OPTIMIZATION)
				.restriction(RESTRICTION)
				.adjustment(ADJUSTMENT)
				.repair(REPAIR)
				.build();
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		String inputBody = mapper.writeValueAsString(inputDto);

		AssignmentResult resultId = new AssignmentResult();
		AssignmentResult resultNumber = new AssignmentResult();
		
		MvcResult mvcResult = this.mockMvc
		.perform(post("/api/certificate/createclient").accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
			.contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8).content(inputBody))
		.andDo(print()).andExpect(status().isOk())
		.andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
		.andDo(AssignmentResultHandler.assignTo("$.results[0].certificateId", resultId))
		.andDo(AssignmentResultHandler.assignTo("$.results[0].certificateNumber", resultNumber))
		.andReturn();
		
		int certId = (Integer) resultId.getValue();
		String certNumber = (String) resultNumber.getValue();
		
		String output = mvcResult.getResponse().getContentAsString();
		
		RestApiUploadClientCertificateOutputDto expectedDto = RestApiUploadClientCertificateOutputDto.builder()
				.certificateId(certId)
				.certificateNumber(certNumber)
				.build();
		
		RestResultDTO<RestApiUploadClientCertificateOutputDto> expectedResult = new RestResultDTO<>();
		expectedResult.setMessage("");
		expectedResult.setSuccess(true);
		expectedResult.addResult(expectedDto);
		String expectedOutput = mapper.writeValueAsString(expectedResult);

        assertEquals(mapper.readTree(expectedOutput), mapper.readTree(output));

        Certificate cert = this.certificateService.findCertificate(certId);
        Assert.assertTrue("Certificate file should exist but did not", this.certificateService.getCertificateFileExists(cert));
	}
}
