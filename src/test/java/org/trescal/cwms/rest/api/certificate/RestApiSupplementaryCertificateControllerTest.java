package org.trescal.cwms.rest.api.certificate;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetJob;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestApiSupplementaryCertificateControllerTest extends MvcRestTestClass {

    @Override
    protected List<String> getDataSetFileNames() {
        return DataSetJob.getJobClientPostCalibrationTestData();
    }

    private void assertSuccess(String content) throws Exception {
        StringBuffer documentNumber = new StringBuffer("USTUS-DTW-CI-");
		documentNumber.append((new SimpleDateFormat("yy").format(new Date())));
		documentNumber.append("000001");
		
		this.mockMvc.perform(
				post("/api/certificate/supplementary")
				.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
				.content(content)
			)
		.andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.message").value(""))
        .andExpect(jsonPath("$.success").value(true))
        .andExpect(jsonPath("$.results").isArray())
        .andExpect(jsonPath("$.results[0]").exists())
        .andExpect(jsonPath("$.results[1]").doesNotExist())
        .andExpect(jsonPath("$.results[0].certificateId").exists())
        .andExpect(jsonPath("$.results[0].documentNumber").exists())
        .andExpect(jsonPath("$.results[0].documentNumber").value(documentNumber.toString()));
	}	
	
	@Test
	@WithMockUser("hartland_user")
	public void testMinimumReissueCertificate() throws Exception {
		String content = "{\"certificateId\" : 1502}";
		assertSuccess(content);
	}
	
	@Test
	@WithMockUser("hartland_user")
	public void testMaximumReissueCertificate() throws Exception {
		String content = "{"
				+ "\"certificateId\" : 1502,"
				+ "\"contactId\" : 550"
				+ "}";
		assertSuccess(content);
	}
}
