package org.trescal.cwms.rest.api.creditnote;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetPricing;
import org.trescal.cwms.tests.generic.pricing.TestDataCreditNote;
import org.trescal.cwms.tests.generic.pricing.TestDataCreditNoteItem;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestApiCreditNoteViewControllerTest extends MvcRestTestClass {

    @Override
    protected List<String> getDataSetFileNames() {
        return DataSetPricing.getCreditNoteTestData();
    }

    @Test
    public void shouldReturnCreditNoteView() throws Exception {
		Integer creditNoteId = TestDataCreditNote.CREDIT_NOTE_ID_US_150;
		
		this.mockMvc.perform(
				get("/api/creditnote/details?id="+creditNoteId)
				.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
			)
		.andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.message").value(""))
        .andExpect(jsonPath("$.success").value(true))
        .andExpect(jsonPath("$.results").isArray())
        .andExpect(jsonPath("$.results[0]").exists())
        .andExpect(jsonPath("$.results[1]").doesNotExist())
        .andExpect(jsonPath("$.results[0].creditNoteItems").exists())
        .andExpect(jsonPath("$.results[0].creditNoteItems").isArray())
        .andExpect(jsonPath("$.results[0].creditNoteItems[0]").exists())
        .andExpect(jsonPath("$.results[0].creditNoteItems[2]").doesNotExist())
        .andExpect(jsonPath("$.results[0].creditNoteItems[0].itemId").value(TestDataCreditNoteItem.ITEM_ID_US_1501))
        .andExpect(jsonPath("$.results[0].creditNoteItems[0].itemNumber").value(TestDataCreditNoteItem.ITEM_NUMBER_US_1501))
        .andExpect(jsonPath("$.results[0].creditNoteItems[0].description").value(TestDataCreditNoteItem.DESCRIPTION_US_1501))
        .andExpect(jsonPath("$.results[0].creditNoteItems[0].quantity").value(TestDataCreditNoteItem.QUANTITY_US_1501))
        .andExpect(jsonPath("$.results[0].creditNoteItems[0].finalCost").value(TestDataCreditNoteItem.FINAL_COST_US_1501.doubleValue()))
        .andExpect(jsonPath("$.results[0].creditNoteItems[0].taxable").value(TestDataCreditNoteItem.TAXABLE_US_1501))
        .andExpect(jsonPath("$.results[0].creditNoteItems[0].taxAmount").value(TestDataCreditNoteItem.TAX_AMOUNT_US_1501.doubleValue()))
        .andExpect(jsonPath("$.results[0].creditNoteItems[0].costType").value(TestDataCreditNoteItem.COST_TYPE_US_1501.getName()))
        
        .andExpect(jsonPath("$.results[0].creditNoteItems[1].itemId").value(TestDataCreditNoteItem.ITEM_ID_US_1502))
        .andExpect(jsonPath("$.results[0].creditNoteItems[1].itemNumber").value(TestDataCreditNoteItem.ITEM_NUMBER_US_1502))
        ;
	}

}
