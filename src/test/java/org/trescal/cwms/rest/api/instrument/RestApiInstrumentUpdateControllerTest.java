package org.trescal.cwms.rest.api.instrument;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetInstrument;
import org.trescal.cwms.tests.generic.address.TestDataAddress;
import org.trescal.cwms.tests.generic.company.TestDataCompany;
import org.trescal.cwms.tests.generic.company.TestDataContact;
import org.trescal.cwms.tests.generic.company.TestDataLocation;
import org.trescal.cwms.tests.generic.core.TestDataServiceType;
import org.trescal.cwms.tests.generic.instrument.TestDataInstrument;
import org.trescal.cwms.tests.generic.instrument.TestDataInstrumentModel;
import org.trescal.cwms.tests.generic.instrument.TestDataInstrumentUsageType;
import org.trescal.cwms.tests.generic.instrument.TestDataMfr;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestApiInstrumentUpdateControllerTest extends MvcRestTestClass {

    @Override
    protected List<String> getDataSetFileNames() {
        // Test instrument to update
        List<String> result = DataSetInstrument.getInstrumentClientTestData();
        return result;
    }

    // Using string definitions instead of enum names here intentionally...
	private static final int CAL_INTERVAL = 12;
	private static final String CAL_INTERVAL_UNIT = "MONTH";
	private static final String RECALL_DATE = "2021-05-27";
	private static final boolean CAL_STANDARD = false;
	private static final String PLANT_NO = "PN 1234";
	private static final String SERIAL_NO = "SN 1234";
	private static final String STATUS = "IN_CIRCULATION";
	private static final String CUSTOMER_DESC = "Cust Desc 1234";
	private static final String CUSTOMER_SPEC = "Cust Spec 1234";
	private static final String FORMER_BARCODE = "FBC 1234";
	private static final String FREE_TEXT_LOCATION = "FTL 1234";
	private static final String CLIENT_BARCODE = "CBC 1234";
	private static final String MODEL_NAME = "MN 1234";
	private static final boolean CUSTOMER_MANAGED = true;
	
	@Test
	public void testUpdateSuccessMaximal() throws Exception {
		assertSuccess("{"
				+ "\"contactId\" : "+TestDataContact.ID_CLIENT_FRANCE+","
				+ "\"plantid\" : "+TestDataInstrument.ID_FRANCE_CALIPER_1101+","
				+ "\"calInterval\" : "+CAL_INTERVAL+","
				+ "\"calIntervalUnit\" : \""+CAL_INTERVAL_UNIT+"\","
				+ "\"recallDate\" : \""+RECALL_DATE+"\","
				+ "\"calibrationStandard\" : "+CAL_STANDARD+","
				+ "\"plantno\" : \""+PLANT_NO+"\","
				+ "\"serialno\" : \""+SERIAL_NO+"\","
				+ "\"status\" : \""+STATUS+"\","
				+ "\"customerDescription\" : \""+CUSTOMER_DESC+"\","
				+ "\"customerSpecification\" : \""+CUSTOMER_SPEC+"\","
				+ "\"formerBarcode\" : \""+FORMER_BARCODE+"\","
				+ "\"freeTextLocation\" : \""+FREE_TEXT_LOCATION+"\","
				+ "\"clientBarcode\" : \""+CLIENT_BARCODE+"\","
				+ "\"modelname\" : \""+MODEL_NAME+"\","
				+ "\"addressid\" : "+TestDataAddress.ID_ADDRESS_CLIENT_FRANCE+","
				+ "\"coid\" : "+TestDataCompany.ID_BUSINESS_FRANCE+","
				+ "\"personid\" : "+TestDataContact.ID_CLIENT_FRANCE+","
				+ "\"locationid\" :"+TestDataLocation.ID_CLIENT_FRANCE_1101+" ,"
				+ "\"mfrid\" : "+TestDataMfr.ID_FLUKE+","
				+ "\"modelid\" : "+TestDataInstrumentModel.ID_STANDALONE_FLUKE_77+","
				+ "\"defaultservicetypeid\" : "+TestDataServiceType.ID_AC_M_IL+","
				+ "\"usageid\" : "+TestDataInstrumentUsageType.TYPE_ID_1+","
				+ "\"customermanaged\" : "+CUSTOMER_MANAGED
				+ "}");
	}

	/**
	 * Remove all optional fields: (contactId necessary for test framework - no Spring security)
	 * recallDate, customerDescription, customerSpecification,  
	 * formerBarcode, freeTextLocation, clientBarcode, modelname, locationid, mfrid, defaultservicetypeid,
	 * usageid, customermanaged
	 */
	@Test
	public void testUpdateSuccessMinimal() throws Exception {
		assertSuccess("{"
				+ "\"contactId\" : "+TestDataContact.ID_CLIENT_FRANCE+","
				+ "\"plantid\" : "+TestDataInstrument.ID_FRANCE_CALIPER_1101+","
				+ "\"calInterval\" : "+CAL_INTERVAL+","
				+ "\"calIntervalUnit\" : \""+CAL_INTERVAL_UNIT+"\","
				+ "\"calibrationStandard\" : "+CAL_STANDARD+","
				+ "\"plantno\" : \""+PLANT_NO+"\","
				+ "\"serialno\" : \""+SERIAL_NO+"\","
				+ "\"status\" : \""+STATUS+"\","
				+ "\"addressid\" : "+TestDataAddress.ID_ADDRESS_CLIENT_FRANCE+","
				+ "\"coid\" : "+TestDataCompany.ID_BUSINESS_FRANCE+","
				+ "\"personid\" : "+TestDataContact.ID_CLIENT_FRANCE+","
				+ "\"modelid\" : "+TestDataInstrumentModel.ID_STANDALONE_FLUKE_77
				+ "}");
	}
	
	
	private void assertSuccess(String content) throws Exception {
		this.mockMvc.perform(
				post("/api/instrument/update")
				.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
				.content(content)
			)
		.andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.message").value(""))
        .andExpect(jsonPath("$.success").value(true));
		
		// TODO lookup instrument and check values
	}
	
	private void assertFail(String content, String expectedMessage) throws Exception {
		this.mockMvc.perform(
				post("/api/instrument/update")
				.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
				.content(content)
			)
		.andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.message").value(expectedMessage))
        .andExpect(jsonPath("$.success").value(false))
        .andExpect(jsonPath("$.results").isArray())
        .andExpect(jsonPath("$.results[0]").doesNotExist());
	}
	
	/**
	 * Test all validation failures at once
	 * Remove all fields (should have validation failures for all optional fields)
	 * 
	 * TODO check for specific validation messages (maybe better handled via validator unit / integration testing)
	 */
	@Test
	public void testUpdateFailureValidation() throws Exception {
		assertFail("{"
				+ "\"plantid\" : null"
				+ "}", "One or more errors are present, please see below:");
	}

}
