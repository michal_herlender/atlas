package org.trescal.cwms.rest.common.controller.certificate;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcRestTestClass;

import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestCommonCertificateControllerTest extends MvcRestTestClass {
    public static final String DATA_SET = "rest/common/CommonCertificate.xml";
    public static final String TEST_URL = "/common/certificate";

    @Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }

	@Test
	public void testSuccess_getAllCertificatesForInstrument() throws Exception {
		assertSuccess(TEST_URL + "?plantId=1")
		.andExpect(jsonPath("$.results[0]").exists())
		.andExpect(jsonPath("$.results[0].certId").value(1))
		.andExpect(jsonPath("$.results[0].duration").value(12))
		.andExpect(jsonPath("$.results[0].certificateDate").value("2016-01-06T00:00:00.000"))
		.andExpect(jsonPath("$.results[0].calibrationDate").value("2016-01-05T00:00:00.000"))
		.andExpect(jsonPath("$.results[0].certificateNumber").value("ES-A-MAD-C-16000001"))
		.andExpect(jsonPath("$.results[0]").exists())
		.andExpect(jsonPath("$.results[1].certId").value(2))
		.andExpect(jsonPath("$.results[1].duration").value(12))
		.andExpect(jsonPath("$.results[1].certificateDate").value("2016-01-04T00:00:00.000"))
		.andExpect(jsonPath("$.results[1].calibrationDate").value("2016-01-03T00:00:00.000"))
		.andExpect(jsonPath("$.results[1].certificateNumber").value("ES-A-MAD-C-16000003"))
		.andExpect(jsonPath("$.results[2]").exists())
		.andExpect(jsonPath("$.results[2].certId").value(3))
		.andExpect(jsonPath("$.results[2].duration").value(12))
		.andExpect(jsonPath("$.results[2].certificateDate").value("2016-01-02T00:00:00.000"))
		.andExpect(jsonPath("$.results[2].calibrationDate").value("2016-01-01T00:00:00.000"))
		.andExpect(jsonPath("$.results[2].certificateNumber").value("ES-A-MAD-C-16000004"));
	}

	private ResultActions assertSuccess(String urlFragment) throws Exception {
		return this.mockMvc.perform(
				get(urlFragment)
				.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
			)
		.andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.message").value(""))
        .andExpect(jsonPath("$.success").value(true))
        .andExpect(jsonPath("$.results").isArray());
        
	}

}
