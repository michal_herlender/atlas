package org.trescal.cwms.rest.common.controller.certificate;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.jobs.certificate.entity.instcertlink.InstCertLink;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.matchers.AssignmentResult;
import org.trescal.cwms.spring.matchers.AssignmentResultHandler;
import org.trescal.cwms.tests.MvcRestTestClass;

import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestCommonUploadCertificateControllerTest extends MvcRestTestClass {
	public static final String DATA_SET = "rest/common/CommonCertificate.xml";
	public static final String USERNAME = "technician";
	public static final String YEAR = (new SimpleDateFormat("yy")).format(new Date());

	@Autowired
	private CertificateService certificateService;
	
	@Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }

	@Test
	@Transactional
	public void testSuccess_MinimumReferences_Client() throws Exception {
		// wrapper to extract result from the response
		AssignmentResult result = new AssignmentResult();
		
		Map<String, Object> inputPayload = new HashMap<>();
		inputPayload.put("plantId", 1);
		inputPayload.put("calibrationDate", "2017-12-31T00:00:00.000");
		inputPayload.put("certificateDate", "2017-12-31T00:00:00.000");
		inputPayload.put("certificateType", "CERT_CLIENT");
		inputPayload.put("thirdCertificateNumber", "XE125J");
		inputPayload.put("filename", "Certificate_XE125J.PDF");
		inputPayload.put("file", "JVBERi0xLjYNJeLjz9MNCjcgMCBvYmoNPDwvTGluZWFyaXplZCAxL0wgMjU1NDYvTyA5L0UgMjE2");
		testSuccess(inputPayload)
			.andExpect(jsonPath("$.results[0]").exists())
			.andExpect(jsonPath("$.results[0].calibrationDate").value("2017-12-31T00:00:00.000"))
			.andExpect(jsonPath("$.results[0].certificateDate").value("2017-12-31T00:00:00.000"))
			.andExpect(jsonPath("$.results[0].certId").isNumber())
			.andExpect(jsonPath("$.results[0].certificateNumber").value("ESTEM-MAD-CC-"+YEAR+"000001"))
			.andExpect(jsonPath("$.results[0].duration").value(12))
			.andExpect(jsonPath("$.results[0].intervalUnit").value("MONTH"))
			.andExpect(jsonPath("$.results[0].certificateStatus").value("SIGNED"))
                .andExpect(jsonPath("$.results[0].certificateType").value("CERT_CLIENT"))
                .andExpect(jsonPath("$.results[0].plantId").value(1))
                .andExpect(jsonPath("$.results[0].thirdCertificateNumber").value("XE125J"))
                .andExpect(jsonPath("$.results[0].thirdDivId").isEmpty())
                .andDo(AssignmentResultHandler.assignTo("$.results[0].certId", result));

        int certId = (Integer) result.getValue();
        Certificate certificate = this.certificateService.findCertificate(certId);

        // For a client certificate, there should be 1 link to instrument, 0 links via job item
        assertNotNull(certificate.getInstCertLinks());
        assertEquals(1, certificate.getInstCertLinks().size());
        assertTrue(certificate.getLinks() == null || certificate.getLinks().isEmpty());
        InstCertLink instCertLink = certificate.getInstCertLinks().iterator().next();
        assertEquals(1, instCertLink.getInst().getPlantid());
    }

	@Test
	@Transactional
	public void testSuccess_MaximumReferences_Client() throws Exception {
		// wrapper to extract result from the response
		AssignmentResult result = new AssignmentResult();
		
		Map<String, Object> inputPayload = new HashMap<>();
		inputPayload.put("plantId", 1);		// No jobitemId must be specified for a CERT_CLIENT 
		inputPayload.put("calibrationDate", "2017-12-31T00:00:00.000");
		inputPayload.put("calVerificationStatus", "ACCEPTABLE");
		inputPayload.put("certificateDate", "2017-12-31T00:00:00.000");
		inputPayload.put("certificateType", "CERT_CLIENT");
		inputPayload.put("thirdCertificateNumber", "XE125J");
		inputPayload.put("calTypeId", 1);
		inputPayload.put("thirdDivId", 1);
		inputPayload.put("filename", "Certificate_XE125J.PDF");
		inputPayload.put("remarks", "Description about the certificate");
		inputPayload.put("file", "JVBERi0xLjYNJeLjz9MNCjcgMCBvYmoNPDwvTGluZWFyaXplZCAxL0wgMjU1NDYvTyA5L0UgMjE2");
		inputPayload.put("intervalUnit", "MONTH");
		inputPayload.put("duration", "12");
		testSuccess(inputPayload)
		.andExpect(jsonPath("$.results[0]").exists())
		.andExpect(jsonPath("$.results[0].calibrationDate").value("2017-12-31T00:00:00.000"))
		.andExpect(jsonPath("$.results[0].certificateDate").value("2017-12-31T00:00:00.000"))
		.andExpect(jsonPath("$.results[0].certId").isNumber())
		.andExpect(jsonPath("$.results[0].certificateNumber").value("ESTEM-MAD-CC-"+YEAR+"000001"))
		.andExpect(jsonPath("$.results[0].duration").value(12))
		.andExpect(jsonPath("$.results[0].intervalUnit").value("MONTH"))
		.andExpect(jsonPath("$.results[0].calVerificationStatus").value("ACCEPTABLE"))
		.andExpect(jsonPath("$.results[0].certificateStatus").value("SIGNED"))
		.andExpect(jsonPath("$.results[0].certificateType").value("CERT_CLIENT"))
		.andExpect(jsonPath("$.results[0].plantId").value(1))
                .andExpect(jsonPath("$.results[0].remarks").value("Description about the certificate"))
                .andExpect(jsonPath("$.results[0].thirdCertificateNumber").value("XE125J"))
                .andExpect(jsonPath("$.results[0].thirdDivId").value(1))
                .andExpect(jsonPath("$.results[0].calTypeId").value(1))
                .andDo(AssignmentResultHandler.assignTo("$.results[0].certId", result));

        int certId = (Integer) result.getValue();
        Certificate certificate = this.certificateService.findCertificate(certId);

        // For a client certificate, there should be 1 link to instrument, 0 links via job item
        assertNotNull(certificate.getInstCertLinks());
        assertEquals(1, certificate.getInstCertLinks().size());
        assertTrue(certificate.getLinks() == null || certificate.getLinks().isEmpty());
        InstCertLink instCertLink = certificate.getInstCertLinks().iterator().next();
        assertEquals(1, instCertLink.getInst().getPlantid());
    }

	@Test
	@Transactional
	public void testSuccess_MaximumReferences_ThirdParty() throws Exception {
		// wrapper to extract result from the response
		AssignmentResult result = new AssignmentResult();
		
		Map<String, Object> inputPayload = new HashMap<>();
		inputPayload.put("plantId", 1);
		inputPayload.put("jobItemId", 1);
		inputPayload.put("calibrationDate", "2017-12-31T00:00:00.000");
		inputPayload.put("certificateDate", "2017-12-31T00:00:00.000");
		inputPayload.put("certificateType", "CERT_THIRDPARTY");
		inputPayload.put("thirdCertificateNumber", "XE125J");
		inputPayload.put("calTypeId", 1);
		inputPayload.put("thirdDivId", 1);
		inputPayload.put("filename", "Certificate_XE125J.PDF");
		inputPayload.put("remarks", "Description about the certificate");
		inputPayload.put("file", "JVBERi0xLjYNJeLjz9MNCjcgMCBvYmoNPDwvTGluZWFyaXplZCAxL0wgMjU1NDYvTyA5L0UgMjE2");
		inputPayload.put("intervalUnit", "MONTH");
		inputPayload.put("duration", "12");
		inputPayload.put("thirdPartyWorkOutcome", "NO_FURTHER_WORK_NECESSARY");
		
		testSuccess(inputPayload)
		.andExpect(jsonPath("$.results[0]").exists())
		.andExpect(jsonPath("$.results[0].calibrationDate").value("2017-12-31T00:00:00.000"))
		.andExpect(jsonPath("$.results[0].certificateDate").value("2017-12-31T00:00:00.000"))
		.andExpect(jsonPath("$.results[0].certId").isNumber())
		.andExpect(jsonPath("$.results[0].certificateNumber").value("ESTEM-MAD-CS-"+YEAR+"000001"))
		.andExpect(jsonPath("$.results[0].duration").value(12))
		.andExpect(jsonPath("$.results[0].intervalUnit").value("MONTH"))
		.andExpect(jsonPath("$.results[0].certificateStatus").value("SIGNED"))
		.andExpect(jsonPath("$.results[0].certificateType").value("CERT_THIRDPARTY"))
		.andExpect(jsonPath("$.results[0].plantId").value(1))
		.andExpect(jsonPath("$.results[0].thirdCertificateNumber").value("XE125J"))
                .andExpect(jsonPath("$.results[0].thirdDivId").value(1))
                .andExpect(jsonPath("$.results[0].calTypeId").value(1))
                .andExpect(jsonPath("$.results[0].thirdPartyWorkOutcome").value("NO_FURTHER_WORK_NECESSARY"))
                .andExpect(jsonPath("$.results[0].jobItemId").value(1))
                .andDo(AssignmentResultHandler.assignTo("$.results[0].certId", result));

        int certId = (Integer) result.getValue();
        Certificate certificate = this.certificateService.findCertificate(certId);

        // For a third party certificate, there should be 1 link to job item, 0 links via instrument
        assertTrue(certificate.getInstCertLinks() == null || certificate.getInstCertLinks().isEmpty());
        assertNotNull(certificate.getLinks());
        assertEquals(1, certificate.getLinks().size());
        CertLink certLink = certificate.getLinks().iterator().next();
        assertEquals(1, certLink.getJobItem().getJobItemId());
    }

	@Test
	@Transactional
	public void testSuccess_MinimumReferences_ThirdParty() throws Exception {
		// Same as max references, but with no calTypeId, duration, intervalUnit, remarks 
		// wrapper to extract result from the response
		AssignmentResult result = new AssignmentResult();
		
		Map<String, Object> inputPayload = new HashMap<>();
		inputPayload.put("plantId", 1);
		inputPayload.put("jobItemId", 1);
		inputPayload.put("calibrationDate", "2017-12-31T00:00:00.000");
		inputPayload.put("certificateDate", "2017-12-31T00:00:00.000");
		inputPayload.put("certificateType", "CERT_THIRDPARTY");
		inputPayload.put("thirdCertificateNumber", "XE125J");
		inputPayload.put("thirdDivId", 1);
		inputPayload.put("filename", "Certificate_XE125J.PDF");
		inputPayload.put("file", "JVBERi0xLjYNJeLjz9MNCjcgMCBvYmoNPDwvTGluZWFyaXplZCAxL0wgMjU1NDYvTyA5L0UgMjE2");
		inputPayload.put("thirdPartyWorkOutcome", "REQUIRES_FURTHER_TP_WORK");
		
		testSuccess(inputPayload)
		.andExpect(jsonPath("$.results[0]").exists())
		.andExpect(jsonPath("$.results[0].calibrationDate").value("2017-12-31T00:00:00.000"))
		.andExpect(jsonPath("$.results[0].certificateDate").value("2017-12-31T00:00:00.000"))
		.andExpect(jsonPath("$.results[0].certId").isNumber())
		.andExpect(jsonPath("$.results[0].certificateNumber").value("ESTEM-MAD-CS-"+YEAR+"000001"))
		.andExpect(jsonPath("$.results[0].duration").value(12))
		.andExpect(jsonPath("$.results[0].intervalUnit").value("MONTH"))
		.andExpect(jsonPath("$.results[0].certificateStatus").value("SIGNED"))
		.andExpect(jsonPath("$.results[0].certificateType").value("CERT_THIRDPARTY"))
		.andExpect(jsonPath("$.results[0].plantId").value(1))
		.andExpect(jsonPath("$.results[0].thirdCertificateNumber").value("XE125J"))
                .andExpect(jsonPath("$.results[0].thirdDivId").value(1))
                .andExpect(jsonPath("$.results[0].calTypeId").value(1))
                .andExpect(jsonPath("$.results[0].thirdPartyWorkOutcome").value("REQUIRES_FURTHER_TP_WORK"))
                .andExpect(jsonPath("$.results[0].jobItemId").value(1))
                .andDo(AssignmentResultHandler.assignTo("$.results[0].certId", result));

        int certId = (Integer) result.getValue();
        Certificate certificate = this.certificateService.findCertificate(certId);

        // For a third party certificate, there should be 1 link to job item, 0 links via instrument
        assertTrue(certificate.getInstCertLinks() == null || certificate.getInstCertLinks().isEmpty());
        assertNotNull(certificate.getLinks());
        assertEquals(1, certificate.getLinks().size());
        CertLink certLink = certificate.getLinks().iterator().next();
        assertEquals(1, certLink.getJobItem().getJobItemId());
    }

	@Test
	public void testValidationFailure_ClientCert_JobItem() throws Exception {
		// Providing both jobItemId and plantId on a CERT_CLIENT (no job item) should cause a validation failure 
		Map<String, Object> inputPayload = new HashMap<>();
		inputPayload.put("plantId", 1);
		inputPayload.put("jobItemId", 1);
		inputPayload.put("certificateType","CERT_CLIENT");
		
		String[] expectedFieldNames = new String[] {
			"certificateDate",
			"thirdCertificateNumber",
			"file",
			"calibrationDate",
			"jobItemId"
		};
		
		String[] expectedMessages = new String[] {
			"may not be null",
			"may not be null",
			"may not be null",
			"may not be null",
			"Data invalid: 1"
		};
		
		testErrors(inputPayload)
		.andExpect(jsonPath("$.errors").isArray())
		.andExpect(jsonPath("$.errors", Matchers.hasSize(5)))
		.andExpect(jsonPath("$.errors[*].fieldName", Matchers.containsInAnyOrder(expectedFieldNames)))
		.andExpect(jsonPath("$.errors[*].message", Matchers.containsInAnyOrder(expectedMessages)));
	}
	

	private ResultActions test(Map<String, Object> inputPayload) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		String jsonInput = mapper.writeValueAsString(inputPayload);

		// Return for further chaining
		return this.mockMvc
				.perform(post("/common/certificate/client/create")
						.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
						.contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8).content(jsonInput)
						.sessionAttr(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME))
				.andDo(print()).andExpect(status().isOk())
				.andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8));
	}
	
	private ResultActions testSuccess(Map<String, Object> inputPayload) throws Exception {
		// Return for further chaining
		return this.test(inputPayload)
				.andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.errors").isEmpty())
				.andExpect(jsonPath("$.message").value(""))
				.andExpect(jsonPath("$.success").value(true));
	}
	
	private ResultActions testErrors(Map<String, Object> inputPayload) throws Exception {
		// Return for further chaining
		return this.test(inputPayload)
				.andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.errors").isNotEmpty())
				.andExpect(jsonPath("$.message").value("One or more errors are present, please see below:"))
				.andExpect(jsonPath("$.success").value(false));
	}

}
