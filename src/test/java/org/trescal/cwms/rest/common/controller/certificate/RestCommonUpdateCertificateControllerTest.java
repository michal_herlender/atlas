package org.trescal.cwms.rest.common.controller.certificate;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcRestTestClass;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestCommonUpdateCertificateControllerTest extends MvcRestTestClass {

	public static final String DATA_SET = "rest/common/CommonCertificate.xml";
	public static final String USERNAME = "technician";

	@Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }

	@Test
	public void testSuccess() throws Exception {
		Map<String, Object> inputPayload = new HashMap<>();
		inputPayload.put("certId", 1);
		inputPayload.put("remarks", "it's working");
		testSuccess(inputPayload)
		.andExpect(jsonPath("$.results[0]").exists())
		.andExpect(jsonPath("$.results[0].certId").value(1))
		.andExpect(jsonPath("$.results[0].remarks").value("it's working"));
	}

	private ResultActions testSuccess(Map<String, Object> inputPayload) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		String jsonInput = mapper.writeValueAsString(inputPayload);

		return this.mockMvc
				.perform(post("/common/certificate/client/update")
						.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
						.contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8).content(jsonInput)
						.sessionAttr(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME))
				.andDo(print()).andExpect(status().isOk())
				.andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.message").value("")).andExpect(jsonPath("$.success").value(true));
	}

}
