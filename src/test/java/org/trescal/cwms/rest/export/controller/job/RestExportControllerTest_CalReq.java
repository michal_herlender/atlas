package org.trescal.cwms.rest.export.controller.job;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.rest.export.controller.RestExportControllerTest;
import org.trescal.cwms.tests.generic.DataSetJob;
import org.trescal.cwms.tests.generic.instrument.TestDataCalReq;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class RestExportControllerTest_CalReq extends RestExportControllerTest {

    @Override
    protected List<String> getDataSetFileNames() {
        List<String> result = DataSetJob.getJobClientCompletedTestData();
        result.add(DataSetJob.FILE_CAL_REQ);
        return result;
    }

	@Test
	public void testSuccessEmpty_CalReq_Id() throws Exception {
		assertSuccessEmpty("calreq", "id=987654");
	}
	
	@Test
	public void testSuccess_CalReq_Paged() throws Exception {
		assertSuccess("calreq", null, TestDataCalReq.RECORD_COUNT)
	    .andExpect(jsonPath("$.results[0].id").value(TestDataCalReq.ID_1))
	    .andExpect(jsonPath("$.results[1].id").value(TestDataCalReq.ID_2));
	}
	
	@Test
	public void testSuccess_CalReq_Id() throws Exception {
		assertSuccess("calreq", "id="+TestDataCalReq.ID_1, 1)
	    .andExpect(jsonPath("$.results[0].type").value(TestDataCalReq.TYPE_1))
	    .andExpect(jsonPath("$.results[0].id").value(TestDataCalReq.ID_1))
	    .andExpect(jsonPath("$.results[0].active").value(TestDataCalReq.ACTIVE_1))
	    .andExpect(jsonPath("$.results[0].publicInstructions").value(TestDataCalReq.PUBLIC_TEXT_1))
	    .andExpect(jsonPath("$.results[0].privateInstructions").value(TestDataCalReq.PRIVATE_TEXT_1))
	    .andExpect(jsonPath("$.results[0].jobitemid").isEmpty())
	    .andExpect(jsonPath("$.results[0].plantid").value(TestDataCalReq.PLANT_ID_1))
	    .andExpect(jsonPath("$.results[0].compid").isEmpty())
	    .andExpect(jsonPath("$.results[0].modelid").isEmpty())
	    .andExpect(jsonPath("$.results[0].descid").isEmpty());
	}

}
