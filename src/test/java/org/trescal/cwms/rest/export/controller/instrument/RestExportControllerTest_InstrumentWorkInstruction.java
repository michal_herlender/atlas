package org.trescal.cwms.rest.export.controller.instrument;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.rest.export.controller.RestExportControllerTest;
import org.trescal.cwms.tests.generic.DataSetCapability;
import org.trescal.cwms.tests.generic.DataSetInstrument;
import org.trescal.cwms.tests.generic.capability.TestDataInstrumentWorkInstruction;
import org.trescal.cwms.tests.generic.capability.TestDataWorkInstruction;
import org.trescal.cwms.tests.generic.instrument.TestDataInstrument;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class RestExportControllerTest_InstrumentWorkInstruction extends RestExportControllerTest {

    @Override
    protected List<String> getDataSetFileNames() {
        List<String> result = DataSetInstrument.getInstrumentClientTestData();
        result.add(DataSetCapability.FILE_INSTRUMENT_WORK_INSTRUCTION);
        return result;
    }

	@Test
	public void testSuccessEmpty_IWI_Id() throws Exception {
		assertSuccessEmpty("instrumentworkinstruction", "id=987654");
	}

	/**
	 * Returns results based on plantId
	 * @throws Exception
	 */
	@Test
	public void testSuccess_IWI_Id() throws Exception {
		assertSuccess("instrumentworkinstruction", "id=1101", 1)
	    .andExpect(jsonPath("$.results[0].plantId").value(TestDataInstrument.ID_FRANCE_CALIPER_1101))
	    .andExpect(jsonPath("$.results[0].workInstructionId").value(TestDataWorkInstruction.ID_110_FR_CALIPER));
	}
	
	
	@Test
	public void testSuccess_IWI_Paged() throws Exception {
		assertSuccess("instrumentworkinstruction", null, TestDataInstrumentWorkInstruction.RECORD_COUNT)
	    .andExpect(jsonPath("$.results[0].plantId").value(TestDataInstrument.ID_FRANCE_CALIPER_1101))
	    .andExpect(jsonPath("$.results[1].plantId").value(TestDataInstrument.ID_GERMANY_CALIPER_1201))
	    .andExpect(jsonPath("$.results[0].workInstructionId").value(TestDataWorkInstruction.ID_110_FR_CALIPER))
	    .andExpect(jsonPath("$.results[1].workInstructionId").value(TestDataWorkInstruction.ID_120_DE_CALIPER));
	}
	
}
