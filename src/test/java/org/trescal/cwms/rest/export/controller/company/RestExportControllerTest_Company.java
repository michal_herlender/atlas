package org.trescal.cwms.rest.export.controller.company;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.rest.export.controller.RestExportControllerTest;
import org.trescal.cwms.tests.generic.DataSetCompany;
import org.trescal.cwms.tests.generic.company.TestDataCompany;
import org.trescal.cwms.tests.generic.core.TestDataSupportedCountry;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class RestExportControllerTest_Company extends RestExportControllerTest {

    @Override
    protected List<String> getDataSetFileNames() {
        return DataSetCompany.getCompanyBusinessTestData();
    }

    @Test
    public void testSuccessEmpty_Company_Id() throws Exception {
		assertSuccessEmpty("company", "id=987654");
	}
	
	@Test
	public void testSuccess_Company_Paged() throws Exception {
		assertSuccess("company", null, TestDataCompany.RECORD_COUNT_BUSINESS)
	    .andExpect(jsonPath("$.results[0].coid").value(TestDataCompany.ID_BUSINESS_FRANCE))
	    .andExpect(jsonPath("$.results[1].coid").value(TestDataCompany.ID_BUSINESS_GERMANY))
	    .andExpect(jsonPath("$.results[2].coid").value(TestDataCompany.ID_BUSINESS_MOROCCO));
	}
	
	@Test
	public void testSuccess_Company_Id() throws Exception {
		assertSuccess("company", "id="+TestDataCompany.ID_BUSINESS_USA, 1)
	    .andExpect(jsonPath("$.results[0].coid").value(TestDataCompany.ID_BUSINESS_USA))
	    
	    .andExpect(jsonPath("$.results[0].coname").value(TestDataCompany.COMPANY_NAME_BUSINESS_USA))
	    .andExpect(jsonPath("$.results[0].companyRole").value(CompanyRole.BUSINESS.name()))
		.andExpect(jsonPath("$.results[0].countryid").value(TestDataSupportedCountry.ID_USA))
		.andExpect(jsonPath("$.results[0].companyCode").value(TestDataCompany.COMPANY_CODE_BUSINESS_USA))
		.andExpect(jsonPath("$.results[0].legalIdentifier").value(TestDataCompany.LEGAL_ID_BUSINESS_USA))
		.andExpect(jsonPath("$.results[0].fiscalIdentifier").value(TestDataCompany.FISCAL_ID_BUSINESS_USA))
		;
	}
	
}
