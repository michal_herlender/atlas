package org.trescal.cwms.rest.export.controller.company;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.rest.export.controller.RestExportControllerTest;
import org.trescal.cwms.tests.generic.DataSetCompany;
import org.trescal.cwms.tests.generic.address.TestDataAddress;
import org.trescal.cwms.tests.generic.company.TestDataContact;
import org.trescal.cwms.tests.generic.company.TestDataSubdiv;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class RestExportControllerTest_Contact extends RestExportControllerTest {

    @Override
    protected List<String> getDataSetFileNames() {
        return DataSetCompany.getCompanyBusinessTestData();
    }

    @Test
    public void testSuccessEmpty_Contact_Id() throws Exception {
		assertSuccessEmpty("contact", "id=987654");
	}
	
	@Test
	public void testSuccess_Contact_Paged() throws Exception {
		assertSuccess("contact", null, TestDataContact.RECORD_COUNT_BUSINESS)
	    .andExpect(jsonPath("$.results[0].personid").value(TestDataContact.ID_BUSINESS_FRANCE))
	    .andExpect(jsonPath("$.results[1].personid").value(TestDataContact.ID_BUSINESS_GERMANY))
	    .andExpect(jsonPath("$.results[2].personid").value(TestDataContact.ID_BUSINESS_MOROCCO))
	    ;
	}
	
	@Test
	public void testSuccess_Contact_Id() throws Exception {
		assertSuccess("contact", "id="+TestDataContact.ID_BUSINESS_FRANCE, 1)
	    .andExpect(jsonPath("$.results[0].personid").value(TestDataContact.ID_BUSINESS_FRANCE))
	    .andExpect(jsonPath("$.results[0].active").value(true))
	    .andExpect(jsonPath("$.results[0].firstname").value(TestDataContact.FIRST_NAME_BUSINESS_FRANCE))
	    .andExpect(jsonPath("$.results[0].lastname").value(TestDataContact.LAST_NAME_BUSINESS_FRANCE))
	    .andExpect(jsonPath("$.results[0].email").value(TestDataContact.EMAIL_BUSINESS_FRANCE))
	    .andExpect(jsonPath("$.results[0].telephone").value(TestDataContact.TELEPHONE_BUSINESS_FRANCE))
	    .andExpect(jsonPath("$.results[0].fax").value(TestDataContact.FAX_BUSINESS_FRANCE))
	    .andExpect(jsonPath("$.results[0].mobile").value(TestDataContact.MOBILE_BUSINESS_FRANCE))
	    .andExpect(jsonPath("$.results[0].subdivid").value(TestDataSubdiv.ID_BUSINESS_FRANCE))
	    .andExpect(jsonPath("$.results[0].defaddress").value(TestDataAddress.ID_ADDRESS_RUNGIS))
	    ;
	}
}
