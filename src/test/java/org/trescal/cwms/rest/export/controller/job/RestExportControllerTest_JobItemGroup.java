/**
 *
 */
package org.trescal.cwms.rest.export.controller.job;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.rest.export.controller.RestExportControllerTest;
import org.trescal.cwms.tests.generic.DataSetJob;
import org.trescal.cwms.tests.generic.job.TestDataJobItemGroup;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

/**
 * @author shazil.khan
 *
 */
public class RestExportControllerTest_JobItemGroup extends RestExportControllerTest {

    @Override
    protected List<String> getDataSetFileNames() {
        List<String> result = DataSetJob.getJobClientCompletedTestData();
        return result;
    }
	
	/*
	 * We do the test here for id that doesnot exist*/
	@Test
	public void testSuccessEmpty_JobitemGroup_Id() throws Exception {
		assertSuccessEmpty("jobitemgroup", "id=39");
	}
	
	
	@Test
	public void testSuccess_JobItemGroup_Paged() throws Exception {
		assertSuccess("jobitemgroup", null, TestDataJobItemGroup.RECORD_COUNT_COMPLETED)
	    .andExpect(jsonPath("$.results[0].jobid").value(TestDataJobItemGroup.ID_CLIENT_1)) 
	    .andExpect(jsonPath("$.results[1].jobid").value(TestDataJobItemGroup.ID_CLIENT_2))
	    .andExpect(jsonPath("$.results[2].jobid").value(TestDataJobItemGroup.ID_CLIENT_3))
	    .andExpect(jsonPath("$.results[3].jobid").value(TestDataJobItemGroup.ID_CLIENT_4))
	    .andExpect(jsonPath("$.results[4].jobid").value(TestDataJobItemGroup.ID_CLIENT_5));
	}
	
	@Test
	public void testSuccess_JobItemGroup_Id() throws Exception {
		assertSuccess("jobitemgroup", "id="+TestDataJobItemGroup.ID_1, 1)
	    .andExpect(jsonPath("$.results[0].id").value(TestDataJobItemGroup.ID_1))
	    .andExpect(jsonPath("$.results[0].calibrationGroup").value(TestDataJobItemGroup.CALIBRATION_1))
	    .andExpect(jsonPath("$.results[0].deliveryGroup").value(TestDataJobItemGroup.DELIVERY_1))
	    .andExpect(jsonPath("$.results[0].jobid").value(TestDataJobItemGroup.JOB_ID_1));
	}
	

}
