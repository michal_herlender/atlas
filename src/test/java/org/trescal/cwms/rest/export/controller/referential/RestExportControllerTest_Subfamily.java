package org.trescal.cwms.rest.export.controller.referential;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.rest.export.controller.RestExportControllerTest;
import org.trescal.cwms.tests.generic.DataSetInstrument;
import org.trescal.cwms.tests.generic.instrument.TestDataDescription;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class RestExportControllerTest_Subfamily extends RestExportControllerTest {

    @Override
    protected List<String> getDataSetFileNames() {
        List<String> result = new ArrayList<>();
        result.add(DataSetInstrument.FILE_INSTRUMENT_MODEL);

        return result;
    }

	@Test
	public void testSuccessEmpty_Subfamily_Id() throws Exception {
		assertSuccessEmpty("subfamily", "id=987654");
	}
	
	@Test
	public void testSuccess_Subfamily_Paged() throws Exception {
		assertSuccess("subfamily", null, TestDataDescription.RECORD_COUNT)

            .andExpect(jsonPath("$.results[0].descriptionid").value(TestDataDescription.ID_EQUIPMENT_MISC))
            .andExpect(jsonPath("$.results[1].descriptionid").value(TestDataDescription.ID_DIGITAL_MULTIMETER))
            .andExpect(jsonPath("$.results[2].descriptionid").value(TestDataDescription.ID_CALIPER))
            .andExpect(jsonPath("$.results[3].descriptionid").value(TestDataDescription.ID_SERVICES))
            .andExpect(jsonPath("$.results[4].descriptionid").value(TestDataDescription.ID_SERVICES_PACKAGING));
	}
	
	@Test
	public void testSuccess_Subfamily_Id() throws Exception {
		assertSuccess("subfamily", "id="+TestDataDescription.ID_DIGITAL_MULTIMETER, 1)

	    .andExpect(jsonPath("$.results[0].descriptionid").value(TestDataDescription.ID_DIGITAL_MULTIMETER))
	    .andExpect(jsonPath("$.results[0].description").value(TestDataDescription.DESCRIPTION_DIGITAL_MULTIMETER))
	    .andExpect(jsonPath("$.results[0].tmlid").value(TestDataDescription.TML_ID_DIGITAL_MULTIMETER))
	    .andExpect(jsonPath("$.results[0].familyid").value(TestDataDescription.FAMILY_ID_DIGITAL_MULTIMETER))
	    .andExpect(jsonPath("$.results[0].active").value(TestDataDescription.ACTIVE_DIGITAL_MULTIMETER));
	}
	
}
