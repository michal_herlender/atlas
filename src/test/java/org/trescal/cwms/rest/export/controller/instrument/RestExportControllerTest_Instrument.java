package org.trescal.cwms.rest.export.controller.instrument;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.rest.export.controller.RestExportControllerTest;
import org.trescal.cwms.tests.generic.DataSetInstrument;
import org.trescal.cwms.tests.generic.address.TestDataAddress;
import org.trescal.cwms.tests.generic.company.TestDataCompany;
import org.trescal.cwms.tests.generic.company.TestDataContact;
import org.trescal.cwms.tests.generic.company.TestDataLocation;
import org.trescal.cwms.tests.generic.core.TestDataServiceType;
import org.trescal.cwms.tests.generic.instrument.TestDataInstrument;
import org.trescal.cwms.tests.generic.instrument.TestDataInstrumentModel;
import org.trescal.cwms.tests.generic.instrument.TestDataMfr;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class RestExportControllerTest_Instrument extends RestExportControllerTest {

    @Override
    protected List<String> getDataSetFileNames() {
        return DataSetInstrument.getInstrumentClientTestData();
    }

    @Test
    public void testSuccessEmpty_Instrument_Id() throws Exception {
		assertSuccessEmpty("instrument", "id=987654");
	}
	
	@Test
	public void testSuccess_Instrument_Paged() throws Exception {
		assertSuccess("instrument", null, TestDataInstrument.RECORD_COUNT)
	    .andExpect(jsonPath("$.results[0].plantid").value(TestDataInstrument.ID_FRANCE_CALIPER_1101))
	    .andExpect(jsonPath("$.results[1].plantid").value(TestDataInstrument.ID_FRANCE_FLUKE_77_1102));
	}
	
	/*
		Integer plantid, Integer calInterval, IntervalUnit calIntervalUnit,
		Boolean calibrationStandard, Date recallDate, String plantno, String serialno, InstrumentStatus status,
		String customerDescription, String formerBarcode, String modelname, Integer addressid, Integer coid,
		Integer personid, Integer locationid, Integer mfrid, Integer modelid, Integer defaultservicetypeid
	*/
	
	@Test
	public void testSuccess_Instrument_Id() throws Exception {
		assertSuccess("instrument", "id="+TestDataInstrument.ID_FRANCE_CALIPER_1101, 1)
	    .andExpect(jsonPath("$.results[0].plantid").value(TestDataInstrument.ID_FRANCE_CALIPER_1101))
	    .andExpect(jsonPath("$.results[0].calInterval").value(TestDataInstrument.CAL_FREQUENCY_1101))
	    .andExpect(jsonPath("$.results[0].calIntervalUnit").value(IntervalUnit.MONTH.name()))
	    .andExpect(jsonPath("$.results[0].calibrationStandard").value(TestDataInstrument.CALIBRATION_STANDARD_1101))
	    .andExpect(jsonPath("$.results[0].recallDate").value(TestDataInstrument.NEXT_CAL_DATE_1101))
	    .andExpect(jsonPath("$.results[0].plantno").value(TestDataInstrument.PLANT_NO_1101))
	    .andExpect(jsonPath("$.results[0].serialno").value(TestDataInstrument.SERIAL_NO_1101))
	    .andExpect(jsonPath("$.results[0].status").value(InstrumentStatus.IN_CIRCULATION.name()))
	    .andExpect(jsonPath("$.results[0].customerDescription").value(TestDataInstrument.CUSTOMER_DESCRIPTION_1101))
	    .andExpect(jsonPath("$.results[0].customerSpecification").value(TestDataInstrument.CUSTOMER_SPECIFICATION_1101))
	    .andExpect(jsonPath("$.results[0].freeTextLocation").value(TestDataInstrument.FREE_TEXT_LOCATION_1101))
	    .andExpect(jsonPath("$.results[0].clientBarcode").value(TestDataInstrument.CLIENT_BARCODE_1101))
	    .andExpect(jsonPath("$.results[0].formerBarcode").value(TestDataInstrument.FORMER_BARCODE_1101))
	    .andExpect(jsonPath("$.results[0].modelname").value(TestDataInstrument.MODEL_NAME_1101))
	    .andExpect(jsonPath("$.results[0].addressid").value(TestDataAddress.ID_ADDRESS_CLIENT_FRANCE))
	    .andExpect(jsonPath("$.results[0].coid").value(TestDataCompany.ID_CLIENT_FRANCE))
	    .andExpect(jsonPath("$.results[0].personid").value(TestDataContact.ID_CLIENT_FRANCE))
	    .andExpect(jsonPath("$.results[0].locationid").value(TestDataLocation.ID_CLIENT_FRANCE_1101))
	    .andExpect(jsonPath("$.results[0].mfrid").value(TestDataMfr.ID_MITUTOYO))
	    .andExpect(jsonPath("$.results[0].modelid").value(TestDataInstrumentModel.ID_STANDALONE_CALIPER_300MM))
	    .andExpect(jsonPath("$.results[0].defaultservicetypeid").value(TestDataServiceType.ID_AC_M_IL))
	    .andExpect(jsonPath("$.results[0].usageid").value(TestDataInstrument.USAGEID))
	    .andExpect(jsonPath("$.results[0].customermanaged").value(TestDataInstrument.customermanaged));
	    ;
	}
	
}
