package org.trescal.cwms.rest.export.controller.company;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.rest.export.controller.RestExportControllerTest;
import org.trescal.cwms.tests.generic.DataSetCompany;
import org.trescal.cwms.tests.generic.company.TestDataCompany;
import org.trescal.cwms.tests.generic.company.TestDataSubdiv;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class RestExportControllerTest_Subdiv extends RestExportControllerTest {

    @Override
    protected List<String> getDataSetFileNames() {
        return DataSetCompany.getCompanyBusinessTestData();
    }

    @Test
    public void testSuccessEmpty_Subdiv_Id() throws Exception {
		assertSuccessEmpty("subdiv", "id=987654");
	}
	
	@Test
	public void testSuccess_Subdiv_Paged() throws Exception {
		assertSuccess("subdiv", null, TestDataSubdiv.RECORD_COUNT_BUSINESS)
	    .andExpect(jsonPath("$.results[0].subdivid").value(TestDataSubdiv.ID_BUSINESS_FRANCE))
	    .andExpect(jsonPath("$.results[1].subdivid").value(TestDataSubdiv.ID_BUSINESS_GERMANY))
	    .andExpect(jsonPath("$.results[2].subdivid").value(TestDataSubdiv.ID_BUSINESS_MOROCCO));
	}
	
	@Test
	public void testSuccess_Subdiv_Id() throws Exception {
		assertSuccess("subdiv", "id="+TestDataSubdiv.ID_BUSINESS_USA, 1)
	    .andExpect(jsonPath("$.results[0].subdivid").value(TestDataSubdiv.ID_BUSINESS_USA))
	    .andExpect(jsonPath("$.results[0].coid").value(TestDataCompany.ID_BUSINESS_USA))
	    .andExpect(jsonPath("$.results[0].active").value(TestDataSubdiv.ACTIVE_BUSINESS_USA_DTW))
	    .andExpect(jsonPath("$.results[0].subdivcode").value(TestDataSubdiv.SUBDIV_CODE_BUSINESS_USA_DTW))
	    .andExpect(jsonPath("$.results[0].subname").value(TestDataSubdiv.SUBNAME_BUSINESS_USA_DTW));
	}
	
}
