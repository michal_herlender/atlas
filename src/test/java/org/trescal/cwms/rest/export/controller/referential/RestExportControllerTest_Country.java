package org.trescal.cwms.rest.export.controller.referential;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.rest.export.controller.RestExportControllerTest;
import org.trescal.cwms.tests.generic.DataSetCore;
import org.trescal.cwms.tests.generic.core.TestDataSupportedCountry;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class RestExportControllerTest_Country extends RestExportControllerTest {

    @Override
    protected List<String> getDataSetFileNames() {
        List<String> result = new ArrayList<>();
        result.add(DataSetCore.FILE_BASE_SYSTEM);

        return result;
    }
	@Test
	public void testSuccessEmpty_Country_Id() throws Exception {
		assertSuccessEmpty("country", "id=987654");
	}
	
	@Test
	public void testSuccess_Country_Paged() throws Exception {
		assertSuccess("country", null, TestDataSupportedCountry.RECORD_COUNT)
	    .andExpect(jsonPath("$.results[0].countryid").value(TestDataSupportedCountry.ID_FRANCE))
	    .andExpect(jsonPath("$.results[1].countryid").value(TestDataSupportedCountry.ID_GERMANY))
	    .andExpect(jsonPath("$.results[2].countryid").value(TestDataSupportedCountry.ID_MOROCCO))
	    .andExpect(jsonPath("$.results[3].countryid").value(TestDataSupportedCountry.ID_SPAIN))
	    .andExpect(jsonPath("$.results[4].countryid").value(TestDataSupportedCountry.ID_USA));
	}
	
	@Test
	public void testSuccess_Country_Id() throws Exception {
		assertSuccess("country", "id="+TestDataSupportedCountry.ID_FRANCE, 1)
	    .andExpect(jsonPath("$.results[0].countryid").value(TestDataSupportedCountry.ID_FRANCE))
	    .andExpect(jsonPath("$.results[0].country").value(TestDataSupportedCountry.COUNTRY_FRANCE))
	    .andExpect(jsonPath("$.results[0].countrycode").value(TestDataSupportedCountry.COUNTRY_CODE_FRANCE));
	}

}
