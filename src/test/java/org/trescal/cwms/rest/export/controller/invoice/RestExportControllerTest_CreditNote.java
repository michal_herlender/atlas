package org.trescal.cwms.rest.export.controller.invoice;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.rest.export.controller.RestExportControllerTest;
import org.trescal.cwms.tests.generic.DataSetPricing;
import org.trescal.cwms.tests.generic.pricing.TestDataCreditNote;
import org.trescal.cwms.tests.generic.pricing.TestDataInvoice;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class RestExportControllerTest_CreditNote extends RestExportControllerTest {

    @Override
    protected List<String> getDataSetFileNames() {
        return DataSetPricing.getCreditNoteTestData();
    }

    @Test
    public void testSuccessEmpty_CreditNote_Id() throws Exception {
		assertSuccessEmpty("creditnote", "id=987");
	}
	
	@Test
	public void testSuccess_CreditNote_Paged() throws Exception {
		assertSuccess("creditnote", null, TestDataCreditNote.RECORD_COUNT_STANDALONE)
	    .andExpect(jsonPath("$.results[0].id").value(TestDataCreditNote.CREDIT_NOTE_ID_FR_110))
	    .andExpect(jsonPath("$.results[1].id").value(TestDataCreditNote.CREDIT_NOTE_ID_DE_120));
	}
	
	@Test
	public void testSuccess_CreditNote_Id() throws Exception {
		assertSuccess("creditnote", "id="+TestDataCreditNote.CREDIT_NOTE_ID_FR_110, 1)
		.andExpect(jsonPath("$.results[0].id").value(TestDataCreditNote.CREDIT_NOTE_ID_FR_110))
		.andExpect(jsonPath("$.results[0].issued").value(TestDataCreditNote.ISSUED_FR_110))
		.andExpect(jsonPath("$.results[0].issuedate").value("2019-09-16"))
		.andExpect(jsonPath("$.results[0].finalcost").value(240.00d))
		.andExpect(jsonPath("$.results[0].totalcost").value(200.00d))
		.andExpect(jsonPath("$.results[0].vatvalue").value(40.00d))
	    .andExpect(jsonPath("$.results[0].creditnoteno").value(TestDataCreditNote.CREDIT_NOTE_NUMBER_FR_110))
		.andExpect(jsonPath("$.results[0].invid").value(TestDataInvoice.INVOICE_ID_FR_110))
		.andExpect(jsonPath("$.results[0].orgid").value(TestDataCreditNote.ORG_ID_FR_110));
	}

}
