package org.trescal.cwms.rest.export.controller.job;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.rest.export.controller.RestExportControllerTest;
import org.trescal.cwms.tests.generic.DataSetJob;
import org.trescal.cwms.tests.generic.job.TestDataOnBehalfItem;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class RestExportControllerTest_OnBehalfItem extends RestExportControllerTest {

    @Override
    protected List<String> getDataSetFileNames() {
        return DataSetJob.getOnBehalfItemTestData();
    }

    @Test
    public void testSuccessEmpty_Onbehalf_Item_Id() throws Exception {
		assertSuccessEmpty("onbehalfitem", "id=987");
	}
	
	@Test
	public void testSuccess_On_Behalf_Item_Paged() throws Exception {
		assertSuccess("onbehalfitem", null, TestDataOnBehalfItem.RECORD_COUNT_STANDALONE)
	    .andExpect(jsonPath("$.results[0].id").value(TestDataOnBehalfItem.ON_BEHALF_ITEM_ID_1))
	    .andExpect(jsonPath("$.results[1].id").value(TestDataOnBehalfItem.ON_BEHALF_ITEM_ID_2));
	}
	
	
	
	@Test
	public void testSuccess_On_Behalf_Item_Id() throws Exception {
		assertSuccess("onbehalfitem", "id="+TestDataOnBehalfItem.ON_BEHALF_ITEM_ID_1, 1)
		.andExpect(jsonPath("$.results[0].id").value(TestDataOnBehalfItem.ON_BEHALF_ITEM_ID_1))
	    .andExpect(jsonPath("$.results[0].coid").value(TestDataOnBehalfItem.ON_BEHALF_ITEM_COMPANY_ID))
	    .andExpect(jsonPath("$.results[0].jobitemid").value(TestDataOnBehalfItem.ON_BEHALF_ITEM_JOBITEM_ID))
	    .andExpect(jsonPath("$.results[0].addressid").value(TestDataOnBehalfItem.ON_BEHALF_ITEM_ADDRESS_ID));
	}

}
