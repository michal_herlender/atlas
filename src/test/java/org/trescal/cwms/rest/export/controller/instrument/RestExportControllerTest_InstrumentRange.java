package org.trescal.cwms.rest.export.controller.instrument;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.rest.export.controller.RestExportControllerTest;
import org.trescal.cwms.tests.generic.DataSetInstrument;
import org.trescal.cwms.tests.generic.instrument.TestDataInstrument;
import org.trescal.cwms.tests.generic.instrument.TestDataInstrumentRange;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class RestExportControllerTest_InstrumentRange extends RestExportControllerTest {
    @Override
    protected List<String> getDataSetFileNames() {
        return DataSetInstrument.getInstrumentClientTestData();
    }

    @Test
    public void testSuccessEmpty_InstrumentRange_Id() throws Exception {
        assertSuccessEmpty("instrumentrange", "id=987654");
	}
	
	@Test
	public void testSuccess_InstrumentRange_Paged() throws Exception {
		assertSuccess("instrumentrange", null, TestDataInstrumentRange.RECORD_COUNT)
	    .andExpect(jsonPath("$.results[0].id").value(TestDataInstrumentRange.ID_FRANCE_CALIPER_1101))
	    .andExpect(jsonPath("$.results[1].id").value(TestDataInstrumentRange.ID_FRANCE_FLUKE_77_1102));
	}

	@Test
	public void testSuccess_InstrumentRange_Id_MaxUomNull() throws Exception {
		assertSuccess("instrumentrange", "id="+TestDataInstrumentRange.ID_FRANCE_CALIPER_1101, 1)
		.andExpect(jsonPath("$.results[0].start").value(TestDataInstrumentRange.START_1101))
		.andExpect(jsonPath("$.results[0].end").value(TestDataInstrumentRange.ENDD_1101))
		.andExpect(jsonPath("$.results[0].uom").value(TestDataInstrumentRange.UOM_ID_1101))
		.andExpect(jsonPath("$.results[0].maxUom").doesNotExist())
	    .andExpect(jsonPath("$.results[0].plantid").value(TestDataInstrument.ID_FRANCE_CALIPER_1101));
	}
	
	@Test
	public void testSuccess_InstrumentRange_Id_MaxUom() throws Exception {
		assertSuccess("instrumentrange", "id="+TestDataInstrumentRange.ID_FRANCE_FLUKE_77_1102, 1)
		.andExpect(jsonPath("$.results[0].start").value(TestDataInstrumentRange.START_1102))
		.andExpect(jsonPath("$.results[0].end").value(TestDataInstrumentRange.ENDD_1102))
		.andExpect(jsonPath("$.results[0].uom").value(TestDataInstrumentRange.UOM_ID_1102))
		.andExpect(jsonPath("$.results[0].maxUom").value(TestDataInstrumentRange.MAX_UOM_ID_1102))
	    .andExpect(jsonPath("$.results[0].plantid").value(TestDataInstrument.ID_FRANCE_FLUKE_77_1102));
	}
	
}
