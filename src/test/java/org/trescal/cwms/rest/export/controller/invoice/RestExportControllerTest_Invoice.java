package org.trescal.cwms.rest.export.controller.invoice;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.rest.export.controller.RestExportControllerTest;
import org.trescal.cwms.tests.generic.DataSetPricing;
import org.trescal.cwms.tests.generic.pricing.TestDataInvoice;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class RestExportControllerTest_Invoice extends RestExportControllerTest {

    @Override
    protected List<String> getDataSetFileNames() {
        return DataSetPricing.getInvoiceTestData();
    }

    @Test
    public void testSuccessEmpty_Invoice_Id() throws Exception {
		assertSuccessEmpty("invoice", "id=987");
	}
	
	@Test
	public void testSuccess_Invoice_Paged() throws Exception {
		assertSuccess("invoice", null, TestDataInvoice.RECORD_COUNT_STANDALONE)
	    .andExpect(jsonPath("$.results[0].id").value(TestDataInvoice.INVOICE_ID_FR_110))
	    .andExpect(jsonPath("$.results[1].id").value(TestDataInvoice.INVOICE_ID_FR_111_PERIODIC));
	}
	
	@Test
	public void testSuccess_Invoice_Id() throws Exception {
		assertSuccess("invoice", "id="+TestDataInvoice.INVOICE_ID_FR_110, 1)
		.andExpect(jsonPath("$.results[0].id").value(TestDataInvoice.INVOICE_ID_FR_110))
		.andExpect(jsonPath("$.results[0].issued").value(TestDataInvoice.ISSUED_FR_110))
		.andExpect(jsonPath("$.results[0].issuedate").value("2019-09-09"))
		.andExpect(jsonPath("$.results[0].finalcost").value(1176.00d))
		.andExpect(jsonPath("$.results[0].totalcost").value(980.00d))
		.andExpect(jsonPath("$.results[0].vatvalue").value(196.00d))
	    .andExpect(jsonPath("$.results[0].invno").value(TestDataInvoice.INVOICE_NUMBER_FR_110))
		.andExpect(jsonPath("$.results[0].addressid").value(TestDataInvoice.ADDRESS_ID_FR_110))
		.andExpect(jsonPath("$.results[0].coid").value(TestDataInvoice.COMPANY_ID_FR_110))
		.andExpect(jsonPath("$.results[0].orgid").value(TestDataInvoice.ORG_ID_FR_110))
	    .andExpect(jsonPath("$.results[0].duedate").value("2019-10-09"));
	}
	
}
