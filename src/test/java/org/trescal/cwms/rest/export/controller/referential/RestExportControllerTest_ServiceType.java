package org.trescal.cwms.rest.export.controller.referential;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.rest.export.controller.RestExportControllerTest;
import org.trescal.cwms.tests.generic.DataSetCore;
import org.trescal.cwms.tests.generic.core.TestDataServiceType;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class RestExportControllerTest_ServiceType extends RestExportControllerTest {

    @Override
    protected List<String> getDataSetFileNames() {
        List<String> result = new ArrayList<>();
        result.add(DataSetCore.FILE_BASE_SERVICE_TYPE);

        return result;
    }
	@Test
	public void testSuccessEmpty_ServiceType_Id() throws Exception {
		assertSuccessEmpty("servicetype", "id=987654");
	}
	
	@Test
	public void testSuccess_ServiceType_Paged() throws Exception {
		assertSuccess("servicetype", null, TestDataServiceType.RECORD_COUNT)

		.andExpect(jsonPath("$.results[0].servicetypeid").value(TestDataServiceType.ID_AC_M_IL))
	    .andExpect(jsonPath("$.results[1].servicetypeid").value(TestDataServiceType.ID_TC_M_IL))
	    .andExpect(jsonPath("$.results[2].servicetypeid").value(TestDataServiceType.ID_REPAIR))
	    .andExpect(jsonPath("$.results[3].servicetypeid").value(TestDataServiceType.ID_TC_MC_OS))
	    .andExpect(jsonPath("$.results[4].servicetypeid").value(TestDataServiceType.ID_SERVICE));
	}
	
	@Test
	public void testSuccess_ServiceType_Id() throws Exception {
		assertSuccess("servicetype", "id="+TestDataServiceType.ID_AC_M_IL, 1)

	    .andExpect(jsonPath("$.results[0].servicetypeid").value(TestDataServiceType.ID_AC_M_IL))
	    .andExpect(jsonPath("$.results[0].longname").value(TestDataServiceType.LONG_NAME_AC_M_IL))
	    .andExpect(jsonPath("$.results[0].shortname").value(TestDataServiceType.SHORT_NAME_AC_M_IL))
	    .andExpect(jsonPath("$.results[0].canbeperformedbyclient").value(TestDataServiceType.CLIENT_AC_M_IL))
	    .andExpect(jsonPath("$.results[0].domaintype").value(TestDataServiceType.DOMAIN_TYPE_AC_M_IL.name()))
	    .andExpect(jsonPath("$.results[0].orderby").value(TestDataServiceType.ORDER_BY_AC_M_IL))
	    .andExpect(jsonPath("$.results[0].repair").value(TestDataServiceType.REPAIR_AC_M_IL));
	}
	
}
