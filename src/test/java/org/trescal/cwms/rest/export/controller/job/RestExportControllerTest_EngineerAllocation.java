package org.trescal.cwms.rest.export.controller.job;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.core.workflow.entity.TimeOfDay;
import org.trescal.cwms.rest.export.controller.RestExportControllerTest;
import org.trescal.cwms.tests.generic.DataSetJob;
import org.trescal.cwms.tests.generic.company.TestDataContact;
import org.trescal.cwms.tests.generic.company.TestDataDepartment;
import org.trescal.cwms.tests.generic.job.TestDataEngineerAllocation;
import org.trescal.cwms.tests.generic.job.TestDataJobItem;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class RestExportControllerTest_EngineerAllocation extends RestExportControllerTest {

    @Override
    protected List<String> getDataSetFileNames() {
        return DataSetJob.getJobClientCompletedTestData();
    }

    @Test
    public void testSuccessEmpty_EngineerAllocation_Id() throws Exception {
		assertSuccessEmpty("engineerallocation", "id=987654");
	}
	
	@Test
	public void testSuccess_EngineerAllocation_Paged() throws Exception {
		assertSuccess("engineerallocation", null, TestDataEngineerAllocation.RECORD_COUNT_COMPLETED)
	    .andExpect(jsonPath("$.results[0].id").value(TestDataEngineerAllocation.ID_CLIENT_FRANCE_1101))
	    .andExpect(jsonPath("$.results[1].id").value(TestDataEngineerAllocation.ID_CLIENT_GERMANY_1201));
	}
	
	@Test
	public void testSuccess_EngineerAllocation_Id() throws Exception {
		assertSuccess("engineerallocation", "id="+TestDataEngineerAllocation.ID_CLIENT_FRANCE_1101, 1)
	    .andExpect(jsonPath("$.results[0].id").value(TestDataEngineerAllocation.ID_CLIENT_FRANCE_1101))
	    .andExpect(jsonPath("$.results[0].active").value(TestDataEngineerAllocation.ACTIVE_CLIENT_FRANCE_1101))
	    .andExpect(jsonPath("$.results[0].allocatedFor").value(TestDataEngineerAllocation.ALLOCATED_FOR_TEXT_FRANCE_1101))
	    .andExpect(jsonPath("$.results[0].allocatedOn").value(TestDataEngineerAllocation.ALLOCATED_ON_TEXT_FRANCE_1101))
	    .andExpect(jsonPath("$.results[0].timeofday").value(TimeOfDay.AM.name()))
	    .andExpect(jsonPath("$.results[0].allocatedbyid").value(TestDataContact.ID_BUSINESS_FRANCE))
	    .andExpect(jsonPath("$.results[0].allocatedtoid").value(TestDataContact.ID_BUSINESS_FRANCE))
	    .andExpect(jsonPath("$.results[0].jobitemid").value(TestDataJobItem.ID_CLIENT_FR_1))
	    .andExpect(jsonPath("$.results[0].allocateduntil").value(TestDataEngineerAllocation.ALLOCATED_UNTIL_TEXT_FRANCE_1101))
	    .andExpect(jsonPath("$.results[0].timeofdayuntil").value(TimeOfDay.PM.name()))
	    .andExpect(jsonPath("$.results[0].deptid").value(TestDataDepartment.ID_BUSINESS_FRANCE))
	    ;
	}

}
