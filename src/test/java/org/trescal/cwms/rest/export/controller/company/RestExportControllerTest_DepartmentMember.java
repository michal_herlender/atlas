package org.trescal.cwms.rest.export.controller.company;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.rest.export.controller.RestExportControllerTest;
import org.trescal.cwms.tests.generic.DataSetCompany;
import org.trescal.cwms.tests.generic.company.TestDataContact;
import org.trescal.cwms.tests.generic.company.TestDataDepartment;
import org.trescal.cwms.tests.generic.company.TestDataDepartmentMember;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class RestExportControllerTest_DepartmentMember extends RestExportControllerTest {

    @Override
    protected List<String> getDataSetFileNames() {
        return DataSetCompany.getCompanyBusinessTestData();
    }

    @Test
    public void testSuccessEmpty_DepartmentMember_Id() throws Exception {
		assertSuccessEmpty("departmentmember", "id=987654");
	}
	
	@Test
	public void testSuccess_DepartmentMember_Paged() throws Exception {
		assertSuccess("departmentmember", null, TestDataDepartmentMember.RECORD_COUNT_BUSINESS)
	    .andExpect(jsonPath("$.results[0].id").value(TestDataDepartmentMember.ID_BUSINESS_FRANCE))
	    .andExpect(jsonPath("$.results[1].id").value(TestDataDepartmentMember.ID_BUSINESS_GERMANY))
	    .andExpect(jsonPath("$.results[2].id").value(TestDataDepartmentMember.ID_BUSINESS_MOROCCO))
	    ;
	}
	
	@Test
	public void testSuccess_DepartmentMember_Id() throws Exception {
		assertSuccess("departmentmember", "id="+TestDataDepartmentMember.ID_BUSINESS_FRANCE, 1)
	    .andExpect(jsonPath("$.results[0].id").value(TestDataDepartment.ID_BUSINESS_FRANCE))
	    .andExpect(jsonPath("$.results[0].personid").value(TestDataContact.ID_BUSINESS_FRANCE))
	    .andExpect(jsonPath("$.results[0].deptid").value(TestDataDepartment.ID_BUSINESS_FRANCE))
	    ;
	}
	
}
