package org.trescal.cwms.rest.export.controller.referential;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.rest.export.controller.RestExportControllerTest;
import org.trescal.cwms.tests.generic.DataSetCapability;
import org.trescal.cwms.tests.generic.capability.TestDataWorkInstruction;
import org.trescal.cwms.tests.generic.instrument.TestDataInstrumentModel;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class RestExportControllerTest_WorkInstruction extends RestExportControllerTest {

    @Override
    protected List<String> getDataSetFileNames() {
        return DataSetCapability.getWorkInstructionData();
    }

    @Test
    public void testSuccessEmpty_WorkInstruction_Id() throws Exception {
		assertSuccessEmpty("workinstruction", "id=987654");
	}
	
	@Test
	public void testSuccess_WorkInstruction_ID() throws Exception {
		assertSuccess("workinstruction", "id=150", 1)
	    .andExpect(jsonPath("$.results[0].id").value(TestDataWorkInstruction.ID_150_US_CALIPER))
	    .andExpect(jsonPath("$.results[0].descid").value(TestDataInstrumentModel.DESCRIPTION_CALIPER_300MM))
	    .andExpect(jsonPath("$.results[0].name").value(TestDataWorkInstruction.NAME_150_US_CALIPER))
	    .andExpect(jsonPath("$.results[0].title").value(TestDataWorkInstruction.TITLE_150_US_CALIPER));
	}
	
	@Test
	public void testSuccess_WorkInstruction_Paged() throws Exception {
		assertSuccess("workinstruction", null, TestDataWorkInstruction.RECORD_COUNT)
	    .andExpect(jsonPath("$.results[4].id").value(TestDataWorkInstruction.ID_150_US_CALIPER))
	    .andExpect(jsonPath("$.results[4].descid").value(TestDataInstrumentModel.DESCRIPTION_CALIPER_300MM))
	    .andExpect(jsonPath("$.results[4].name").value(TestDataWorkInstruction.NAME_150_US_CALIPER))
	    .andExpect(jsonPath("$.results[4].title").value(TestDataWorkInstruction.TITLE_150_US_CALIPER));
	}
	
}
