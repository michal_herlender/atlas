package org.trescal.cwms.rest.export.controller.instrument;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.rest.export.controller.RestExportControllerTest;
import org.trescal.cwms.tests.generic.DataSetInstrument;
import org.trescal.cwms.tests.generic.instrument.TestDataInstrumentUsageType;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class RestExportControllerTest_InstrumentUsageType extends RestExportControllerTest {

    @Override
    protected List<String> getDataSetFileNames() {
        return DataSetInstrument.getInstrumentUsageTypeTestData();
    }

    @Test
    public void testSuccessEmpty_Instrument_Usage_Type_Id() throws Exception {
		assertSuccessEmpty("instrumentusagetype", "id=9");
	}
	
	@Test
	public void testSuccess_Instrument_Usage_Type_Paged() throws Exception {
		assertSuccess("instrumentusagetype", null, TestDataInstrumentUsageType.RECORD_COUNT_STANDALONE)
	    .andExpect(jsonPath("$.results[0].typeid").value(TestDataInstrumentUsageType.TYPE_ID_1))
	    .andExpect(jsonPath("$.results[1].typeid").value(TestDataInstrumentUsageType.TYPE_ID_2));
	}
	
	
	
	@Test
	public void testSuccess_Instrument_Usage_Type_Id() throws Exception {
		assertSuccess("instrumentusagetype", "typeid="+TestDataInstrumentUsageType.TYPE_ID_1, 4)
		.andExpect(jsonPath("$.results[0].fullDescription").value(TestDataInstrumentUsageType.FULL_DESC))
	    .andExpect(jsonPath("$.results[0].defaultType").value(TestDataInstrumentUsageType.DEFAULT_TYPE))
	    .andExpect(jsonPath("$.results[0].name").value(TestDataInstrumentUsageType.NAME))
	    .andExpect(jsonPath("$.results[0].recall").value(TestDataInstrumentUsageType.recall));
	}

}
