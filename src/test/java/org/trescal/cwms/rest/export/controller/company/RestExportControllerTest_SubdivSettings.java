package org.trescal.cwms.rest.export.controller.company;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.rest.export.controller.RestExportControllerTest;
import org.trescal.cwms.tests.generic.DataSetCompany;
import org.trescal.cwms.tests.generic.company.TestDataContact;
import org.trescal.cwms.tests.generic.company.TestDataSubdiv;
import org.trescal.cwms.tests.generic.company.TestDataSubdivSettings;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class RestExportControllerTest_SubdivSettings extends RestExportControllerTest {

    @Override
    protected List<String> getDataSetFileNames() {
        return DataSetCompany.getCompanyClientTestData();
    }

    @Test
    public void testSuccessEmpty_SubdivSettings_Id() throws Exception {
		assertSuccessEmpty("subdivsettings", "id=987654");
	}
	
	@Test
	public void testSuccess_SubdivSettings_Paged() throws Exception {
		assertSuccess("subdivsettings", null, TestDataSubdivSettings.RECORD_COUNT_CLIENT)
	    .andExpect(jsonPath("$.results[0].id").value(TestDataSubdivSettings.ID_510_FR_FOR_110_FR))
	    .andExpect(jsonPath("$.results[1].id").value(TestDataSubdivSettings.ID_520_DE_FOR_120_DE))
	    .andExpect(jsonPath("$.results[2].id").value(TestDataSubdivSettings.ID_530_MA_FOR_130_MA));
	}
	
	@Test
	public void testSuccess_SubdivSettings_Id() throws Exception {
		assertSuccess("subdivsettings", "id="+TestDataSubdivSettings.ID_510_FR_FOR_110_FR, 1)
		.andExpect(jsonPath("$.results[0].id").value(TestDataSubdivSettings.ID_510_FR_FOR_110_FR))
		.andExpect(jsonPath("$.results[0].orgid").value(TestDataSubdiv.ID_BUSINESS_FRANCE))
	    .andExpect(jsonPath("$.results[0].businesscontactid").value(TestDataContact.ID_BUSINESS_FRANCE))
	    .andExpect(jsonPath("$.results[0].subdivid").value(TestDataSubdiv.ID_CLIENT_FRANCE));
		;
	}

}
