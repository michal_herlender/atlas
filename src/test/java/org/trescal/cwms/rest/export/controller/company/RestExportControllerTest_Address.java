package org.trescal.cwms.rest.export.controller.company;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.trescal.cwms.rest.export.controller.RestExportControllerTest;
import org.trescal.cwms.tests.generic.DataSetCompany;
import org.trescal.cwms.tests.generic.address.TestDataAddress;
import org.trescal.cwms.tests.generic.company.TestDataSubdiv;
import org.trescal.cwms.tests.generic.core.TestDataSupportedCountry;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@Slf4j
public class RestExportControllerTest_Address extends RestExportControllerTest {

	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetCompany.getCompanyBusinessTestData();
	}

	@Test
	public void testSuccessEmpty_Address_Id() throws Exception {
		assertSuccessEmpty("address", "id=987654");
	}
	
	@Test
	public void testSuccess_Address_Paged() throws Exception {
		long startTime = System.currentTimeMillis();
		
		assertSuccess("address", null, TestDataAddress.RECORD_COUNT_BUSINESS)
	    .andExpect(jsonPath("$.results[0].addressid").value(TestDataAddress.ID_ADDRESS_RUNGIS))
	    .andExpect(jsonPath("$.results[1].addressid").value(TestDataAddress.ID_ADDRESS_DARMSTADT))
	    .andExpect(jsonPath("$.results[2].addressid").value(TestDataAddress.ID_ADDRESS_CASABLANCA));

		long finishTime = System.currentTimeMillis();
		log.info("Internal test execution time : "+(finishTime-startTime)+" ms");		
	}
	
	@Test
	public void testSuccess_Address_Id() throws Exception {
		assertSuccess("address", "id="+TestDataAddress.ID_ADDRESS_HARTLAND, 1)
	    .andExpect(jsonPath("$.results[0].addressid").value(TestDataAddress.ID_ADDRESS_HARTLAND))
	    .andExpect(jsonPath("$.results[0].active").value(TestDataAddress.ACTIVE_HARTLAND))
	    .andExpect(jsonPath("$.results[0].addr1").value(TestDataAddress.ADDR_1_HARTLAND))
	    .andExpect(jsonPath("$.results[0].addr2").value(TestDataAddress.ADDR_2_HARTLAND))
	    .andExpect(jsonPath("$.results[0].addr3").value(TestDataAddress.ADDR_3_HARTLAND))
	    .andExpect(jsonPath("$.results[0].town").value(TestDataAddress.TOWN_HARTLAND))
	    .andExpect(jsonPath("$.results[0].county").value(TestDataAddress.COUNTY_HARTLAND))
	    .andExpect(jsonPath("$.results[0].postcode").value(TestDataAddress.POSTCODE_HARTLAND))
	    .andExpect(jsonPath("$.results[0].countryid").value(TestDataSupportedCountry.ID_USA))
	    .andExpect(jsonPath("$.results[0].subdivid").value(TestDataSubdiv.ID_BUSINESS_USA));
	}
	
}
