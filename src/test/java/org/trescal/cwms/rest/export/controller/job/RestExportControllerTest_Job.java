package org.trescal.cwms.rest.export.controller.job;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.rest.export.controller.RestExportControllerTest;
import org.trescal.cwms.tests.generic.DataSetJob;
import org.trescal.cwms.tests.generic.address.TestDataAddress;
import org.trescal.cwms.tests.generic.company.TestDataCompany;
import org.trescal.cwms.tests.generic.company.TestDataContact;
import org.trescal.cwms.tests.generic.job.TestDataJob;
import org.trescal.cwms.tests.generic.job.TestDataJobStatus;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class RestExportControllerTest_Job extends RestExportControllerTest {

    @Override
    protected List<String> getDataSetFileNames() {
        return DataSetJob.getJobClientCompletedTestData();
    }

    @Test
    public void testSuccessEmpty_Job_Id() throws Exception {
		assertSuccessEmpty("job", "id=987654");
	}
	
	@Test
	public void testSuccess_Job_Paged() throws Exception {
		assertSuccess("job", null, TestDataJob.RECORD_COUNT_COMPLETED)
	    .andExpect(jsonPath("$.results[0].jobid").value(TestDataJob.ID_CLIENT_FR))
	    .andExpect(jsonPath("$.results[1].jobid").value(TestDataJob.ID_CLIENT_DE));
	}
	
	@Test
	public void testSuccess_Job_Id() throws Exception {
		assertSuccess("job", "id="+TestDataJob.ID_CLIENT_FR, 1)
		.andExpect(jsonPath("$.results[0].jobid").value(TestDataJob.ID_CLIENT_FR))
	    .andExpect(jsonPath("$.results[0].clientref").value(TestDataJob.CLIENT_REF_CLIENT_FR))
	    .andExpect(jsonPath("$.results[0].jobno").value(TestDataJob.JOB_NO_CLIENT_FR))
	    .andExpect(jsonPath("$.results[0].regdate").value(TestDataJob.REG_DATE_TEXT_CLIENT_FR))
	    .andExpect(jsonPath("$.results[0].datecomplete").value(TestDataJob.DATE_COMPLETE_TEXT_CLIENT_FR))
	    .andExpect(jsonPath("$.results[0].personid").value(TestDataContact.ID_CLIENT_FRANCE))
	    .andExpect(jsonPath("$.results[0].statusid").value(TestDataJobStatus.ID_COMPLETE_3))
	    .andExpect(jsonPath("$.results[0].returnto").value(TestDataAddress.ID_ADDRESS_CLIENT_FRANCE))
	    .andExpect(jsonPath("$.results[0].jobType").value(JobType.STANDARD.name()))
	    .andExpect(jsonPath("$.results[0].orgid").value(TestDataCompany.ID_BUSINESS_FRANCE))
	    ;
	}

}
