package org.trescal.cwms.rest.export.controller.referential;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.rest.export.controller.RestExportControllerTest;
import org.trescal.cwms.tests.generic.DataSetInstrument;
import org.trescal.cwms.tests.generic.instrument.TestDataInstrumentModel;
import org.trescal.cwms.tests.generic.instrument.TestDataInstrumentModelTranslation;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class RestExportControllerTest_InstrumentModelTranslation extends RestExportControllerTest {

    @Override
    protected List<String> getDataSetFileNames() {
        List<String> result = new ArrayList<>();
        result.add(DataSetInstrument.FILE_INSTRUMENT_MODEL);

        return result;
    }

	@Test
	public void testSuccessEmpty_Id() throws Exception {
		assertSuccessEmpty("instrumentmodeltranslation", "id=987654");
	}

	@Test
	public void testSuccess_Paged() throws Exception {
		assertSuccess("instrumentmodeltranslation", null, TestDataInstrumentModelTranslation.RECORD_COUNT)
	    .andExpect(jsonPath("$.results[0].id").value(TestDataInstrumentModel.ID_SALESCATEGORY_CALIPER_TO_300MM))
	    .andExpect(jsonPath("$.results[5].id").value(TestDataInstrumentModel.ID_SALESCATEGORY_CALIPER_QUARANTINED))
	    .andExpect(jsonPath("$.results[10].id").value(TestDataInstrumentModel.ID_STANDALONE_CALIPER_300MM));
		// Remaining model ids (in order of id and then locale) not listed
	}

	@Test
	public void testSuccess_Locale() throws Exception {
		assertSuccess("instrumentmodeltranslation", "locale=en_GB", TestDataInstrumentModel.RECORD_COUNT)
	    .andExpect(jsonPath("$.results[0].id").value(TestDataInstrumentModel.ID_SALESCATEGORY_CALIPER_TO_300MM))
	    .andExpect(jsonPath("$.results[1].id").value(TestDataInstrumentModel.ID_SALESCATEGORY_CALIPER_QUARANTINED))
	    .andExpect(jsonPath("$.results[2].id").value(TestDataInstrumentModel.ID_STANDALONE_CALIPER_300MM));
		// Remaining model ids (in order of id and then locale) not listed
	}
	
	
	@Test
	public void testSuccess_Id() throws Exception {
		assertSuccess("instrumentmodeltranslation", "id="+TestDataInstrumentModel.ID_STANDALONE_FLUKE_77, 
				TestDataInstrumentModelTranslation.RECORD_COUNT_FLUKE_77)

	    .andExpect(jsonPath("$.results[0].id").value(TestDataInstrumentModel.ID_STANDALONE_FLUKE_77))
	    .andExpect(jsonPath("$.results[0].locale").value(TestDataInstrumentModelTranslation.LOCALE_DE_DE))
	    .andExpect(jsonPath("$.results[0].translation").value(TestDataInstrumentModelTranslation.TRANSLATION_DE_DE_FLUKE_77))

	    .andExpect(jsonPath("$.results[1].id").value(TestDataInstrumentModel.ID_STANDALONE_FLUKE_77))
	    .andExpect(jsonPath("$.results[1].locale").value(TestDataInstrumentModelTranslation.LOCALE_EN_GB))
	    .andExpect(jsonPath("$.results[1].translation").value(TestDataInstrumentModelTranslation.TRANSLATION_EN_GB_FLUKE_77))

	    .andExpect(jsonPath("$.results[2].id").value(TestDataInstrumentModel.ID_STANDALONE_FLUKE_77))
	    .andExpect(jsonPath("$.results[2].locale").value(TestDataInstrumentModelTranslation.LOCALE_EN_US))
	    .andExpect(jsonPath("$.results[2].translation").value(TestDataInstrumentModelTranslation.TRANSLATION_EN_US_FLUKE_77))

	    .andExpect(jsonPath("$.results[3].id").value(TestDataInstrumentModel.ID_STANDALONE_FLUKE_77))
	    .andExpect(jsonPath("$.results[3].locale").value(TestDataInstrumentModelTranslation.LOCALE_ES_ES))
	    .andExpect(jsonPath("$.results[3].translation").value(TestDataInstrumentModelTranslation.TRANSLATION_ES_ES_FLUKE_77))

	    .andExpect(jsonPath("$.results[4].id").value(TestDataInstrumentModel.ID_STANDALONE_FLUKE_77))
	    .andExpect(jsonPath("$.results[4].locale").value(TestDataInstrumentModelTranslation.LOCALE_FR_FR))
	    .andExpect(jsonPath("$.results[4].translation").value(TestDataInstrumentModelTranslation.TRANSLATION_FR_FR_FLUKE_77));
	}
	
	@Test
	public void testSuccess_Id_Locale() throws Exception {
		assertSuccess("instrumentmodeltranslation", "id="+TestDataInstrumentModel.ID_STANDALONE_FLUKE_77+"&locale=en_GB", 1)

	    .andExpect(jsonPath("$.results[0].id").value(TestDataInstrumentModel.ID_STANDALONE_FLUKE_77))
	    .andExpect(jsonPath("$.results[0].locale").value(TestDataInstrumentModelTranslation.LOCALE_EN_GB))
	    .andExpect(jsonPath("$.results[0].translation").value(TestDataInstrumentModelTranslation.TRANSLATION_EN_GB_FLUKE_77));
	}
	
/*
<instmodeltranslation locale="de_DE" modelid="21441" translation="DIGITAL-MULTIMETER FLUKE 77" />
<instmodeltranslation locale="en_GB" modelid="21441" translation="DIGITAL MULTIMETER FLUKE 77" />
<instmodeltranslation locale="es_ES" modelid="21441" translation="MULTIMETRO DIGITAL FLUKE 77" />
<instmodeltranslation locale="fr_FR" modelid="21441" translation="MULTIMETRE NUMERIQUE FLUKE 77" />
 */
}
