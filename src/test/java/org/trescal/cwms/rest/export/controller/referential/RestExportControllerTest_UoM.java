package org.trescal.cwms.rest.export.controller.referential;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.rest.export.controller.RestExportControllerTest;
import org.trescal.cwms.tests.generic.DataSetInstrument;
import org.trescal.cwms.tests.generic.core.TestDataUnitsOfMeasure;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class RestExportControllerTest_UoM extends RestExportControllerTest {

    @Override
    protected List<String> getDataSetFileNames() {
        return DataSetInstrument.getInstrumentModelTestData();
    }

    @Test
    public void testSuccessEmpty_UoM_Id() throws Exception {
		assertSuccessEmpty("uom", "id=987654");
	}
	
	@Test
	public void testSuccess_UoM_Paged() throws Exception {
		assertSuccess("uom", null, TestDataUnitsOfMeasure.RECORD_COUNT)
	    .andExpect(jsonPath("$.results[0].id").value(TestDataUnitsOfMeasure.ID_VOLTS_49))
	    .andExpect(jsonPath("$.results[1].id").value(TestDataUnitsOfMeasure.ID_INCH_186));
	}

	@Test
	public void testSuccess_UoM_Id() throws Exception {
		assertSuccess("uom", "id="+TestDataUnitsOfMeasure.ID_VOLTS_49, 1)
		.andExpect(jsonPath("$.results[0].id").value(TestDataUnitsOfMeasure.ID_VOLTS_49))
		.andExpect(jsonPath("$.results[0].name").value(TestDataUnitsOfMeasure.NAME_VOLTS_49))
		.andExpect(jsonPath("$.results[0].symbol").value(TestDataUnitsOfMeasure.SYMBOL_VOLTS_49))
		.andExpect(jsonPath("$.results[0].tmlid").value(TestDataUnitsOfMeasure.TMLID_VOLTS_49))
	    .andExpect(jsonPath("$.results[0].active").value(TestDataUnitsOfMeasure.ACTIVE_VOLTS_49));
	}	
}
