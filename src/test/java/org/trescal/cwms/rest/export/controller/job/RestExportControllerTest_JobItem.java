package org.trescal.cwms.rest.export.controller.job;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.rest.export.controller.RestExportControllerTest;
import org.trescal.cwms.tests.generic.DataSetJob;
import org.trescal.cwms.tests.generic.address.TestDataAddress;
import org.trescal.cwms.tests.generic.core.TestDataItemState;
import org.trescal.cwms.tests.generic.core.TestDataServiceType;
import org.trescal.cwms.tests.generic.instrument.TestDataInstrument;
import org.trescal.cwms.tests.generic.job.TestDataJob;
import org.trescal.cwms.tests.generic.job.TestDataJobItem;
import org.trescal.cwms.tests.generic.job.TestDataJobItemGroup;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class RestExportControllerTest_JobItem extends RestExportControllerTest {

	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetJob.getJobClientCompletedTestData();
	}

	@Test
	public void testSuccessEmpty_JobItem_Id() throws Exception {
		assertSuccessEmpty("jobitem", "id=987654");
	}
	
	@Test
	public void testSuccess_JobItem_Paged() throws Exception {
		assertSuccess("jobitem", null, TestDataJobItem.RECORD_COUNT_COMPLETED)
	    .andExpect(jsonPath("$.results[0].jobitemid").value(TestDataJobItem.ID_CLIENT_FR_1))
	    .andExpect(jsonPath("$.results[1].jobitemid").value(TestDataJobItem.ID_CLIENT_FR_2));
	}
	
	@Test
	public void testSuccess_JobItem_Id() throws Exception {
		assertSuccess("jobitem", "id="+TestDataJobItem.ID_CLIENT_FR_1, 1)
	    .andExpect(jsonPath("$.results[0].jobitemid").value(TestDataJobItem.ID_CLIENT_FR_1))
	    .andExpect(jsonPath("$.results[0].datein").value(TestDataJobItem.DATE_IN_TEXT_FR_1))
	    .andExpect(jsonPath("$.results[0].duedate").value(TestDataJobItem.DUE_DATE_TEXT_FR_1))
	    .andExpect(jsonPath("$.results[0].clientreceiptdate").value(TestDataJobItem.CLIENT_RECEIPT_DATE_TEXT_FR_1))
	    .andExpect(jsonPath("$.results[0].itemno").value(TestDataJobItem.ITEM_NO_FR_1))
	    .andExpect(jsonPath("$.results[0].addrid").value(TestDataAddress.ID_ADDRESS_RUNGIS))
	    .andExpect(jsonPath("$.results[0].plantid").value(TestDataInstrument.ID_FRANCE_CALIPER_1101))
	    .andExpect(jsonPath("$.results[0].jobid").value(TestDataJob.ID_CLIENT_FR))
	    .andExpect(jsonPath("$.results[0].stateid").value(TestDataItemState.ID_WORK_COMPLETED_54))
	    .andExpect(jsonPath("$.results[0].clientRef").value(TestDataJobItem.CLIENT_REF_FR_1))
	    .andExpect(jsonPath("$.results[0].servicetypeid").value(TestDataServiceType.ID_AC_M_IL))
	    .andExpect(jsonPath("$.results[0].groupid").value(TestDataJobItemGroup.ID_CLIENT_1))
		;
	}

}
