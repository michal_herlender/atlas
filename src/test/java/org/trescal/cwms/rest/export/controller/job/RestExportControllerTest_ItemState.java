package org.trescal.cwms.rest.export.controller.job;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.rest.export.controller.RestExportControllerTest;
import org.trescal.cwms.tests.generic.DataSetCore;
import org.trescal.cwms.tests.generic.core.TestDataItemState;

import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class RestExportControllerTest_ItemState extends RestExportControllerTest {

    @Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DataSetCore.FILE_BASE_ITEM_STATE);
    }

    @Test
    public void testSuccessEmpty_ItemState_Id() throws Exception {
		assertSuccessEmpty("itemstate", "id=987654");
	}
	
	@Test
	public void testSuccess_ItemState_Paged() throws Exception {
		assertSuccess("itemstate", null, TestDataItemState.RECORD_COUNT)
	    .andExpect(jsonPath("$.results[0].stateid").value(TestDataItemState.ID_UNIT_RECEIVED_3))
	    .andExpect(jsonPath("$.results[1].stateid").value(TestDataItemState.ID_AWAITING_CONTRACT_REVIEW_7));
	}
	
	@Test
	public void testSuccess_ItemState_Id() throws Exception {
		assertSuccess("itemstate", "id="+TestDataItemState.ID_UNIT_RECEIVED_3, 1)
	    .andExpect(jsonPath("$.results[0].stateid").value(TestDataItemState.ID_UNIT_RECEIVED_3))
	    .andExpect(jsonPath("$.results[0].active").value(TestDataItemState.ACTIVE_UNIT_RECEIVED_3))
	    .andExpect(jsonPath("$.results[0].description").value(TestDataItemState.DESCRIPTION_UNIT_RECEIVED_3))
	    .andExpect(jsonPath("$.results[0].retired").value(TestDataItemState.RETIRED_UNIT_RECEIVED_3))
	    .andExpect(jsonPath("$.results[0].type").value(TestDataItemState.TYPE_UNIT_RECEIVED_3));
	}
}
