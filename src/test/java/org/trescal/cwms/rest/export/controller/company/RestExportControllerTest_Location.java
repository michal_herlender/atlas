package org.trescal.cwms.rest.export.controller.company;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.rest.export.controller.RestExportControllerTest;
import org.trescal.cwms.tests.generic.DataSetCompany;
import org.trescal.cwms.tests.generic.address.TestDataAddress;
import org.trescal.cwms.tests.generic.company.TestDataLocation;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class RestExportControllerTest_Location extends RestExportControllerTest {

    @Override
    protected List<String> getDataSetFileNames() {
        return DataSetCompany.getCompanyBusinessTestData();
    }

    @Test
    public void testSuccessEmpty_Location_Id() throws Exception {
		assertSuccessEmpty("location", "id=987654");
	}
	
	@Test
	public void testSuccess_Location_Paged() throws Exception {
		assertSuccess("location", null, TestDataLocation.RECORD_COUNT_BUSINESS)
	    .andExpect(jsonPath("$.results[0].locationid").value(TestDataLocation.ID_RUNGIS_5101))
	    .andExpect(jsonPath("$.results[1].locationid").value(TestDataLocation.ID_RUNGIS_5102))
	    .andExpect(jsonPath("$.results[2].locationid").value(TestDataLocation.ID_RUNGIS_5103))
	    .andExpect(jsonPath("$.results[0].active").value(false))
	    .andExpect(jsonPath("$.results[1].active").value(true))
	    .andExpect(jsonPath("$.results[2].active").value(true))
	    ;
	}
	
	@Test
	public void testSuccess_Location_Id() throws Exception {
		assertSuccess("location", "id="+TestDataLocation.ID_RUNGIS_5101, 1)
	    .andExpect(jsonPath("$.results[0].locationid").value(TestDataLocation.ID_RUNGIS_5101))
	    .andExpect(jsonPath("$.results[0].active").value(false))
	    .andExpect(jsonPath("$.results[0].addressid").value(TestDataAddress.ID_ADDRESS_RUNGIS))
	    .andExpect(jsonPath("$.results[0].location").value(TestDataLocation.LOCATION_RUNGIS_5101))
	    ;
	}

}
