package org.trescal.cwms.rest.export.controller.company;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.core.company.entity.companystatus.CompanyStatus;
import org.trescal.cwms.rest.export.controller.RestExportControllerTest;
import org.trescal.cwms.tests.generic.DataSetCompany;
import org.trescal.cwms.tests.generic.company.TestDataCompany;
import org.trescal.cwms.tests.generic.company.TestDataCompanySettings;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class RestExportControllerTest_CompanySettings extends RestExportControllerTest {

    @Override
    protected List<String> getDataSetFileNames() {
        return DataSetCompany.getCompanyBusinessTestData();
    }

    @Test
    public void testSuccessEmpty_CompanySettings_Id() throws Exception {
		assertSuccessEmpty("companysettings", "id=987654");
	}
	
	@Test
	public void testSuccess_CompanySettings_Paged() throws Exception {
		assertSuccess("companysettings", null, TestDataCompanySettings.RECORD_COUNT_BUSINESS)
	    .andExpect(jsonPath("$.results[0].id").value(TestDataCompanySettings.ID_BUSINESS_FRANCE_FOR_SELF))
	    .andExpect(jsonPath("$.results[1].id").value(TestDataCompanySettings.ID_BUSINESS_FRANCE_FOR_SPAIN))
	    .andExpect(jsonPath("$.results[2].id").value(TestDataCompanySettings.ID_BUSINESS_GERMANY_FOR_SELF));
	}
	
	@Test
	public void testSuccess_CompanySettings_Id() throws Exception {
		assertSuccess("companysettings", "id="+TestDataCompanySettings.ID_BUSINESS_FRANCE_FOR_SPAIN, 1)
		.andExpect(jsonPath("$.results[0].id").value(TestDataCompanySettings.ID_BUSINESS_FRANCE_FOR_SPAIN))
		.andExpect(jsonPath("$.results[0].orgid").value(TestDataCompany.ID_BUSINESS_FRANCE))
	    .andExpect(jsonPath("$.results[0].companyid").value(TestDataCompany.ID_BUSINESS_SPAIN))
	    
	    .andExpect(jsonPath("$.results[0].active").value(true))
	    .andExpect(jsonPath("$.results[0].onstop").value(false))
		.andExpect(jsonPath("$.results[0].status").value(CompanyStatus.VERIFIED.name()));
		;
	}

}
