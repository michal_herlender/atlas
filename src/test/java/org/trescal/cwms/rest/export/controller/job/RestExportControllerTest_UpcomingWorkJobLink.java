package org.trescal.cwms.rest.export.controller.job;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.rest.export.controller.RestExportControllerTest;
import org.trescal.cwms.tests.generic.DataSetJob;
import org.trescal.cwms.tests.generic.job.TestDataUpcomingWorkJobLink;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class RestExportControllerTest_UpcomingWorkJobLink extends RestExportControllerTest {

    @Override
    protected List<String> getDataSetFileNames() {
        List<String> result = DataSetJob.getJobClientCompletedTestData();
        result.add(DataSetJob.FILE_UPCOMING_WORK);
        return result;
    }
	
	@Test
	public void testSuccessEmpty_UpcomingWorkJobLink_Id() throws Exception {
		assertSuccessEmpty("upcomingworkjoblink", "id=987654");
	}
	
	@Test
	public void testSuccess_UpcomingWorkJobLink_Paged() throws Exception {
		assertSuccess("upcomingworkjoblink", null, TestDataUpcomingWorkJobLink.RECORD_COUNT)
	    .andExpect(jsonPath("$.results[0].id").value(TestDataUpcomingWorkJobLink.ID_JOB_110))
	    .andExpect(jsonPath("$.results[1].id").value(TestDataUpcomingWorkJobLink.ID_JOB_120));
	}
	
	@Test
	public void testSuccess_JobUpcomingWorkJobLink_Id() throws Exception {
		assertSuccess("upcomingworkjoblink", "id="+TestDataUpcomingWorkJobLink.ID_JOB_110, 1)
		// TODO verify fields
//	    .andExpect(jsonPath("$.results[0].jobitemid").value(TestDataJobItem.ID_CLIENT_FR_1))
		;
	}

}
