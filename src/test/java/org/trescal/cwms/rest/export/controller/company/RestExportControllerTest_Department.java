package org.trescal.cwms.rest.export.controller.company;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.rest.export.controller.RestExportControllerTest;
import org.trescal.cwms.tests.generic.DataSetCompany;
import org.trescal.cwms.tests.generic.address.TestDataAddress;
import org.trescal.cwms.tests.generic.company.TestDataDepartment;
import org.trescal.cwms.tests.generic.company.TestDataSubdiv;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class RestExportControllerTest_Department extends RestExportControllerTest {

    @Override
    protected List<String> getDataSetFileNames() {
        return DataSetCompany.getCompanyBusinessTestData();
    }

    @Test
    public void testSuccessEmpty_Department_Id() throws Exception {
		assertSuccessEmpty("department", "id=987654");
	}
	
	@Test
	public void testSuccess_Department_Paged() throws Exception {
		assertSuccess("department", null, TestDataDepartment.RECORD_COUNT_BUSINESS)
	    .andExpect(jsonPath("$.results[0].deptid").value(TestDataDepartment.ID_BUSINESS_FRANCE))
	    .andExpect(jsonPath("$.results[1].deptid").value(TestDataDepartment.ID_BUSINESS_GERMANY))
	    .andExpect(jsonPath("$.results[2].deptid").value(TestDataDepartment.ID_BUSINESS_MOROCCO))
	    ;
	}
	
	@Test
	public void testSuccess_Department_Id() throws Exception {
		assertSuccess("department", "id="+TestDataDepartment.ID_BUSINESS_FRANCE, 1)
	    .andExpect(jsonPath("$.results[0].deptid").value(TestDataDepartment.ID_BUSINESS_FRANCE))
	    .andExpect(jsonPath("$.results[0].name").value(TestDataDepartment.NAME_BUSINESS_FRANCE))
	    .andExpect(jsonPath("$.results[0].addressid").value(TestDataAddress.ID_ADDRESS_RUNGIS))
	    .andExpect(jsonPath("$.results[0].subdivid").value(TestDataSubdiv.ID_BUSINESS_FRANCE))
	    .andExpect(jsonPath("$.results[0].type").value(DepartmentType.LABORATORY.name()))
	    .andExpect(jsonPath("$.results[0].shortName").value(TestDataDepartment.SHORT_NAME_BUSINESS_FRANCE))
	    ;
	}
	
}
