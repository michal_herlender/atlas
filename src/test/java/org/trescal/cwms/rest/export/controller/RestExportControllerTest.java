package org.trescal.cwms.rest.export.controller;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcRestTestClass;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public abstract class RestExportControllerTest extends MvcRestTestClass {

	private String createUrl(String entityType, String additionalParameters) {
		StringBuilder url = new StringBuilder("/api/export/");
		url.append(entityType);
		if (additionalParameters != null && !additionalParameters.isEmpty()) {
			url.append("?");
			url.append(additionalParameters);
		}
		return url.toString();
	}

	public void assertFail(String entityType, String additionalParameters, String expectedMessage) throws Exception {
		this.mockMvc.perform(
			get(createUrl(entityType, additionalParameters))
				.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
		)
			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
			.andExpect(jsonPath("$.success").value(false))
        .andExpect(jsonPath("$.message").value(expectedMessage))
        .andExpect(jsonPath("$.results").isArray())
    	.andExpect(jsonPath("$.results[0]").doesNotExist());
	}
	
	private ResultActions assertSuccess(String entityType, String additionalParameters) throws Exception {
		return this.mockMvc.perform(
				get(createUrl(entityType, additionalParameters))
				.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
			)
		.andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.message").value(""))
        .andExpect(jsonPath("$.success").value(true))
        .andExpect(jsonPath("$.results").isArray())
		.andExpect(jsonPath("$.currentPage").value("1"))
		.andExpect(jsonPath("$.pageCount").value("1"))
		.andExpect(jsonPath("$.resultsPerPage").value("100"))
		.andExpect(jsonPath("$.startResultsFrom").value("0"));
	}
	
	public ResultActions assertSuccess(String entityType, String additionalParameters, int expectedResultCount) throws Exception {
		int expectedLastIndex = expectedResultCount - 1;

		return assertSuccess(entityType, additionalParameters)
			.andExpect(jsonPath("$.resultsCount").value(expectedResultCount))
			.andExpect(jsonPath("$.results[" + expectedLastIndex + "]").exists())
			.andExpect(jsonPath("$.results[" + expectedResultCount + "]").doesNotExist());
	}
		
	public void assertSuccessEmpty(String entityType, String additionalParameters) throws Exception {
		this.assertSuccess(entityType, additionalParameters)
		.andExpect(jsonPath("$.resultsCount").value("0"))
    	.andExpect(jsonPath("$.results[0]").doesNotExist());
	}
	
}
