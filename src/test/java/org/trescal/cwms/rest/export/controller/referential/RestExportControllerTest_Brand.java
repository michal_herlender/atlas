package org.trescal.cwms.rest.export.controller.referential;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.rest.export.controller.RestExportControllerTest;
import org.trescal.cwms.tests.generic.DataSetCore;
import org.trescal.cwms.tests.generic.DataSetInstrument;
import org.trescal.cwms.tests.generic.instrument.TestDataMfr;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

/**
 * This class was created first, so:
 * - has an additional test for undefined entity type
 */
public class RestExportControllerTest_Brand extends RestExportControllerTest {

    @Override
    protected List<String> getDataSetFileNames() {
		List<String> result = new ArrayList<>();
		result.add(DataSetInstrument.FILE_INSTRUMENT_MODEL);
		result.add(DataSetCore.FILE_BASE_SERVICE_TYPE);
		result.add(DataSetCore.FILE_BASE_SYSTEM);
		
		return result;
	}
	
	@Test
	public void testFail_BadEntityType() throws Exception {
		assertFail("undefined", null, "Invalid path : undefined"); 
	}
	
	@Test
	public void testSuccessEmpty_Brand_Id() throws Exception {
		assertSuccessEmpty("brand", "id=987654");
	}
	
	@Test
	public void testSuccess_Brand_Paged() throws Exception {
		assertSuccess("brand", null, TestDataMfr.RECORD_COUNT)
	    .andExpect(jsonPath("$.results[0].mfrid").value(TestDataMfr.ID_GENERIC))
	    .andExpect(jsonPath("$.results[1].mfrid").value(TestDataMfr.ID_FLUKE))
	    .andExpect(jsonPath("$.results[2].mfrid").value(TestDataMfr.ID_MITUTOYO));
	}
	
	@Test
	public void testSuccess_Brand_Id() throws Exception {
		assertSuccess("brand", "id="+TestDataMfr.ID_FLUKE, 1)
	    .andExpect(jsonPath("$.results[0].mfrid").value(TestDataMfr.ID_FLUKE))
	    .andExpect(jsonPath("$.results[0].active").value(TestDataMfr.ACTIVE_FLUKE))
	    .andExpect(jsonPath("$.results[0].genericmfr").value(TestDataMfr.GENERIC_FLUKE))
	    .andExpect(jsonPath("$.results[0].name").value(TestDataMfr.NAME_FLUKE))
	    .andExpect(jsonPath("$.results[0].tmlid").value(TestDataMfr.TML_ID_FLUKE));
	}

}
