package org.trescal.cwms.rest.export.controller.referential;

import org.junit.jupiter.api.Test;
import org.trescal.cwms.rest.export.controller.RestExportControllerTest;
import org.trescal.cwms.tests.generic.DataSetInstrument;
import org.trescal.cwms.tests.generic.instrument.TestDataInstrumentModel;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class RestExportControllerTest_InstrumentModel extends RestExportControllerTest {

    @Override
    protected List<String> getDataSetFileNames() {
        List<String> result = new ArrayList<>();
        result.add(DataSetInstrument.FILE_INSTRUMENT_MODEL);

        return result;
    }

	@Test
	public void testSuccessEmpty_InstrumentModel_Id() throws Exception {
		assertSuccessEmpty("instrumentmodel", "id=987654");
	}

	@Test
	public void testSuccess_InstrumentModel_Paged() throws Exception {
		assertSuccess("instrumentmodel", null, TestDataInstrumentModel.RECORD_COUNT)
	    .andExpect(jsonPath("$.results[0].modelid").value(TestDataInstrumentModel.ID_SALESCATEGORY_CALIPER_TO_300MM))
	    .andExpect(jsonPath("$.results[1].modelid").value(TestDataInstrumentModel.ID_SALESCATEGORY_CALIPER_QUARANTINED))
	    .andExpect(jsonPath("$.results[2].modelid").value(TestDataInstrumentModel.ID_STANDALONE_CALIPER_300MM));
		// Remaining model ids (in irder of id) not listed
	}
	
	@Test
	public void testSuccess_InstrumentModel_Id() throws Exception {
		assertSuccess("instrumentmodel", "id="+TestDataInstrumentModel.ID_STANDALONE_FLUKE_77, 1)
	    .andExpect(jsonPath("$.results[0].modelid").value(TestDataInstrumentModel.ID_STANDALONE_FLUKE_77))
	    .andExpect(jsonPath("$.results[0].descriptionid").value(TestDataInstrumentModel.DESCRIPTION_ID_FLUKE_77))
	    .andExpect(jsonPath("$.results[0].mfrid").value(TestDataInstrumentModel.MFR_ID_FLUKE_77))
	    .andExpect(jsonPath("$.results[0].model").value(TestDataInstrumentModel.MODEL_FLUKE_77))
	    .andExpect(jsonPath("$.results[0].modelmfrtype").value(TestDataInstrumentModel.MODEL_MFR_TYPE_FLUKE_77.name()))
	    .andExpect(jsonPath("$.results[0].modeltypeid").value(TestDataInstrumentModel.MODEL_TYPE_ID_FLUKE_77))
	    .andExpect(jsonPath("$.results[0].quarantined").value(TestDataInstrumentModel.QUARANTINED_FLUKE_77))
	    .andExpect(jsonPath("$.results[0].tmlid").value(TestDataInstrumentModel.TML_ID_FLUKE_77));
	}
/*
<instmodel modelid="21441" descriptionid="207" lastModified="2019-10-07" mfrid="3558" 
	model="77" modelmfrtype="MFR_SPECIFIC" modeltypeid="1" quarantined="0" tmlid="10656" />
 */
	
}
