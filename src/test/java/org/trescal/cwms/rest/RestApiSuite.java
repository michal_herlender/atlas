package org.trescal.cwms.rest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.trescal.cwms.rest.api.RestApiInvoiceCreditNoteTest;
import org.trescal.cwms.rest.api.RestApiMobileInstrumentsTest;
import org.trescal.cwms.rest.api.RestApiPingTest;
import org.trescal.cwms.rest.api.RestOAuth2RefreshTokenTest;

/**
 * This test suite is meant to be run against a live Tomcat server with one's own credentials
 * (not against a test context, as these test Spring Security including OAuth2 authentication)
 * Perhaps in the future it could run against a sample database.
 * 
 * See the comments in AbstractOAuth2Test for the necessary VM parameters to run this test
 * (including server, credentials, etc...)
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
	RestApiInvoiceCreditNoteTest.class,
	RestApiMobileInstrumentsTest.class,
	RestApiPingTest.class,
	RestOAuth2RefreshTokenTest.class
})
public class RestApiSuite {
}
