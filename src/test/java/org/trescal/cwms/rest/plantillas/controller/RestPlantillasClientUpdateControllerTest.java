package org.trescal.cwms.rest.plantillas.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcRestTestClass;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestPlantillasClientUpdateControllerTest extends MvcRestTestClass {
	public static final String DATA_SET = "rest/plantillas/PlantillasClientUpdate.xml";
	public static final String USERNAME = "technician";

	@Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }
	
	/**
	 * Tests update in situation where the client company already has Plantillas settings 
	 * @throws Exception
	 */
	@Test
	public void testSuccess_ExistingClientCompany() throws Exception {
		Map<String,Object> inputPayload = new HashMap<>();
		inputPayload.put("addrid",200);
		inputPayload.put("formerCustomerId",54321);
		inputPayload.put("syncToPlantillas",true);
		testSuccess(inputPayload);
	}

	/**
	 * Tests update in situation where the company does not yet have any Plantillas settings
	 */
	@Test
	public void testSuccess_NewClientCompany() throws Exception {
		Map<String,Object> inputPayload = new HashMap<>();
		inputPayload.put("addrid",300);
		inputPayload.put("formerCustomerId",98765);
		inputPayload.put("syncToPlantillas",true);
		testSuccess(inputPayload);
	}
	
	private void testSuccess(Map<String,Object> inputPayload) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		String jsonInput = mapper.writeValueAsString(inputPayload);

		this.mockMvc.perform(
				post("/plantillas/client/update")
				.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
				.content(jsonInput)
				.sessionAttr(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME)
			)
		.andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.errors").isEmpty())
        .andExpect(jsonPath("$.message").value(""))
        .andExpect(jsonPath("$.success").value(true));
	}	
	
}
