package org.trescal.cwms.rest.plantillas.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcHsqlRestTestClass;

/**
 * This is a sample test for HSQLDB in memory database - not included in integration test suite.
 * Not working yet (current issue is field type differences with MSSQL definitions).
 * @author Galen - 2018-01-18
 */
public class RestPlantillasSubFamilyControllerTest extends MvcHsqlRestTestClass {
	public static final String DATA_SET = "rest/plantillas/PlantillasSubfamily.xml";

	@Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }
	
	@Test
	public void testSuccess() throws Exception {
		this.mockMvc.perform(get("/rest/plantillas/subfamily")
				.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)))
	.andDo(print())
    .andExpect(status().isOk())
    .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
    .andExpect(jsonPath("$.message").value(""))
    .andExpect(jsonPath("$.success").value(true))
    .andExpect(jsonPath("$.results").isArray())
    .andExpect(jsonPath("$.results[0]").exists())
    .andExpect(jsonPath("$.results[1]").exists())
    .andExpect(jsonPath("$.results[2]").exists())
    .andExpect(jsonPath("$.results[3]").doesNotExist());

//    .andExpect(jsonPath("$.results[0].").value(4))
		
	}
	
}
