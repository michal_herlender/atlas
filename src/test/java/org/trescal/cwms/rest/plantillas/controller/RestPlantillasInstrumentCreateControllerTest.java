package org.trescal.cwms.rest.plantillas.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcRestTestClass;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestPlantillasInstrumentCreateControllerTest extends MvcRestTestClass {
	public static final String DATA_SET = "rest/plantillas/PlantillasInstrument.xml";
	public static final String USERNAME = "technician";

	@Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }
	
	@Test
	public void testSuccess_MinimumReferences() throws Exception {
		Map<String,Object> inputPayload = new HashMap<>();
		inputPayload.put("addrid",1);
		inputPayload.put("modelid",1);
		inputPayload.put("calFrequency",12);
		inputPayload.put("calFrequencyUnits","MONTH");
		testSuccess(inputPayload);
	}

	@Test
	public void testSuccess_MaximumReferences() throws Exception {
		Map<String,Object> inputPayload = new HashMap<>();
		inputPayload.put("addrid",1);
		inputPayload.put("modelid",2);
		inputPayload.put("manufacturer","Agilent");
		inputPayload.put("modelName","12345A");
		inputPayload.put("serialno","SN1");
		inputPayload.put("plantno","PN1");
		inputPayload.put("location","Location 1");
		inputPayload.put("formerBarCode","FBC1");
		inputPayload.put("customerDescription","CD1");
		inputPayload.put("receivesCalibration",true);
		inputPayload.put("customermanaged",false);
		inputPayload.put("outOfService",false);
		inputPayload.put("calFrequency",12);
		inputPayload.put("calFrequencyUnits","MONTH");
		inputPayload.put("nextCalDueDate","2017-12-31T00:00:00.000");
		testSuccess(inputPayload);
	}
	
	
	private void testSuccess(Map<String,Object> inputPayload) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		String jsonInput = mapper.writeValueAsString(inputPayload);

		this.mockMvc.perform(
				post("/plantillas/instrument/create")
				.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
				.content(jsonInput)
				.sessionAttr(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME)
			)
		.andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.message").value(""))
        .andExpect(jsonPath("$.success").value(true));
	}	
}
