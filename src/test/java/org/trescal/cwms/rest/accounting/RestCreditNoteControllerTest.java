package org.trescal.cwms.rest.accounting;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetCompany;
import org.trescal.cwms.tests.generic.DataSetPricing;
import org.trescal.cwms.tests.generic.company.TestDataCompany;
import org.trescal.cwms.tests.generic.company.TestDataSubdiv;
import org.trescal.cwms.tests.generic.pricing.TestDataCreditNote;
import org.trescal.cwms.tests.generic.pricing.TestDataInvoice;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestCreditNoteControllerTest extends MvcRestTestClass {

    @Override
    protected List<String> getDataSetFileNames() {
        List<String> result = DataSetCompany.getCompanyClientTestData();
        result.add(DataSetPricing.FILE_INVOICE);
        result.add(DataSetPricing.FILE_CREDIT_NOTE);
        return result;
    }

	@Test
	public void test_successGetCreditNoteDetailsAPI_France() throws Exception {
		test_successViaURL_France("/api/accounting/creditnotes");
	}
	
	@Test
	public void test_successGetCreditNoteDetails_France() throws Exception {
		test_successViaURL_France("/accounting/creditnotes");
	}

	@Test
	public void test_successGetCreditNoteDetailsAPI_USA() throws Exception {
		test_successViaURL_USA("/api/accounting/creditnotes");
	}
	
	@Test
	public void test_successGetCreditNoteDetails_USA() throws Exception {
		test_successViaURL_USA("/accounting/creditnotes");
	}
	
	private void test_successViaURL_France(String endpointURL) throws Exception {
		this.mockMvc
		.perform(get(endpointURL+"?busId="+TestDataCompany.ID_BUSINESS_FRANCE)
				.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)))
		.andDo(print()).andExpect(status().isOk())
		.andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
		.andExpect(jsonPath("$").exists())
		.andExpect(jsonPath("$").isNotEmpty())
		.andExpect(jsonPath("$[0].creditNoteId").value(TestDataCreditNote.CREDIT_NOTE_ID_FR_110))
		.andExpect(jsonPath("$[0].creditNoteNo").value(TestDataCreditNote.CREDIT_NOTE_NUMBER_FR_110))
		.andExpect(jsonPath("$[0].invoiceId").value(TestDataInvoice.INVOICE_ID_FR_110))
		.andExpect(jsonPath("$[0].invoiceNo").value(TestDataInvoice.INVOICE_NUMBER_FR_110))
		.andExpect(jsonPath("$[0].invoiceDate").value("2019-09-08"))
		.andExpect(jsonPath("$[0].totalAmountExclTax").value("200.0"))
		.andExpect(jsonPath("$[0].totalAmountInclTax").value("240.0"))
		.andExpect(jsonPath("$[0].accountStatus").value("P"))
		.andExpect(jsonPath("$[0].billTo_id").value(TestDataCompany.ID_CLIENT_FRANCE))
		.andExpect(jsonPath("$[0].billTo_name").value(TestDataCompany.COMPANY_NAME_CLIENT_FRANCE))
		.andExpect(jsonPath("$[0].billTo_fiscalId").value(TestDataCompany.FISCAL_ID_CLIENT_FRANCE))
		.andExpect(jsonPath("$[0].billTo_subdivId").value(TestDataSubdiv.ID_CLIENT_FRANCE))		
		.andExpect(jsonPath("$[0].billTo_subdivFiscalId").value(TestDataSubdiv.FISCAL_ID_CLIENT_FRANCE))	
		.andExpect(jsonPath("$[0].factoring").value(true))
		.andExpect(jsonPath("$[0].items").isNotEmpty())
		.andExpect(jsonPath("$[0].items").isArray())
		.andExpect(jsonPath("$[0].items[0]").exists())
		.andExpect(jsonPath("$[0].items[1]").exists())
		.andExpect(jsonPath("$[0].items[2]").doesNotExist());
	}

	
	private void test_successViaURL_USA(String endpointURL) throws Exception {
		this.mockMvc
		.perform(get(endpointURL+"?busId="+TestDataCompany.ID_BUSINESS_USA)
				.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)))
		.andDo(print()).andExpect(status().isOk())
		.andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
		.andExpect(jsonPath("$").exists())
		.andExpect(jsonPath("$").isNotEmpty())
		.andExpect(jsonPath("$[0].creditNoteId").value(TestDataCreditNote.CREDIT_NOTE_ID_US_150))
		.andExpect(jsonPath("$[0].creditNoteNo").value(TestDataCreditNote.CREDIT_NOTE_NUMBER_US_150))
		.andExpect(jsonPath("$[0].invoiceId").value(TestDataInvoice.INVOICE_ID_US_150))
		.andExpect(jsonPath("$[0].invoiceNo").value(TestDataInvoice.INVOICE_NUMBER_US_150))
		.andExpect(jsonPath("$[0].invoiceDate").value("2019-09-08"))
		.andExpect(jsonPath("$[0].totalAmountExclTax").value("200.0"))
		.andExpect(jsonPath("$[0].totalAmountInclTax").value("214.5"))
		.andExpect(jsonPath("$[0].accountStatus").value("P"))
		.andExpect(jsonPath("$[0].billTo_id").value(TestDataCompany.ID_CLIENT_USA))
		.andExpect(jsonPath("$[0].billTo_name").value(TestDataCompany.COMPANY_NAME_CLIENT_USA))
		.andExpect(jsonPath("$[0].billTo_fiscalId").value(TestDataCompany.FISCAL_ID_CLIENT_USA))
		.andExpect(jsonPath("$[0].billTo_subdivId").value(TestDataSubdiv.ID_CLIENT_USA))		
		.andExpect(jsonPath("$[0].billTo_subdivFiscalId").value(TestDataSubdiv.FISCAL_ID_CLIENT_USA))		
		.andExpect(jsonPath("$[0].factoring").value(false))
		.andExpect(jsonPath("$[0].items").isNotEmpty())
		.andExpect(jsonPath("$[0].items").isArray())
		.andExpect(jsonPath("$[0].items[0]").exists())
		.andExpect(jsonPath("$[0].items[1]").exists())
		.andExpect(jsonPath("$[0].items[2]").doesNotExist());
	}
	
}
