package org.trescal.cwms.rest.accounting;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetCompany;
import org.trescal.cwms.tests.generic.company.TestDataCompany;
import org.trescal.cwms.tests.generic.company.TestDataSubdiv;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestSubdivisionControllerTest extends MvcRestTestClass {

    @Override
    protected List<String> getDataSetFileNames() {
        List<String> result = DataSetCompany.getCompanyClientTestData();
        return result;
    }

    @Test
	public void test_successGetSubdivisionDetails() throws Exception {
		test_successViaURL("/accounting/subdivisions");
	}
	
	@Test
	public void test_successGetSubdivisionDetailsAPI() throws Exception {
		test_successViaURL("/api/accounting/subdivisions");
	}

	private void test_successViaURL(String endpointURL) throws Exception {
		this.mockMvc
		.perform(get(endpointURL+"?busId=" + TestDataCompany.ID_BUSINESS_FRANCE
				+ "&role=BUSINESS&subdivId=" + TestDataSubdiv.ID_BUSINESS_FRANCE)
						.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)))
		.andDo(print()).andExpect(status().isOk())
		.andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
		.andExpect(jsonPath("$.message").value("")).andExpect(jsonPath("$.success").value(true))
		.andExpect(jsonPath("$.results").isArray()).andExpect(jsonPath("$.results[0]").exists())
		.andExpect(jsonPath("$.results[0].subdivId").value(TestDataSubdiv.ID_BUSINESS_FRANCE))
		.andExpect(jsonPath("$.results[0].subdivFiscalId").value(TestDataSubdiv.FISCAL_ID_BUSINESS_FRANCE))
		.andExpect(jsonPath("$.results[0].subdivName").value(TestDataSubdiv.SUBNAME_BUSINESS_FRANCE_RUNGIS))
		.andExpect(jsonPath("$.results[0].busId").value(TestDataCompany.ID_BUSINESS_FRANCE))
		.andExpect(jsonPath("$.results[0].companyId").value(TestDataCompany.ID_BUSINESS_FRANCE))
		.andExpect(jsonPath("$.results[0].companyFiscalId").value(TestDataCompany.FISCAL_ID_BUSINESS_FRANCE))
		.andExpect(jsonPath("$.results[0].companyName").value(TestDataCompany.COMPANY_NAME_BUSINESS_FRANCE));
	}
	
	@Test
	public void test_failSubdivIdAndsubdivFiscalIdNull() throws Exception {
		this.mockMvc
				.perform(get("/accounting/subdivisions?busId=" + TestDataCompany.ID_BUSINESS_FRANCE + "&role=CLIENT")
						.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)))
				.andDo(print()).andExpect(status().isOk())
				.andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.message").value("At least one of subdivId or subdivFiscalId must be specified"))
				.andExpect(jsonPath("$.success").value(false));
	}

}
