package org.trescal.cwms.rest.accounting;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	RestInvoicesControllerTest.class,
	RestCreditNoteControllerTest.class,
	RestSubdivisionControllerTest.class,
	RestPurchaseOrderControllerTest.class
})
public class RestAccountingSuite {

}
