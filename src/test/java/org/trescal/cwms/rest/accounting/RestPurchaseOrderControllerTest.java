package org.trescal.cwms.rest.accounting;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetCompany;
import org.trescal.cwms.tests.generic.DataSetPricing;
import org.trescal.cwms.tests.generic.company.TestDataCompany;
import org.trescal.cwms.tests.generic.company.TestDataSubdiv;
import org.trescal.cwms.tests.generic.job.TestDataPo;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestPurchaseOrderControllerTest extends MvcRestTestClass {

    @Override
    protected List<String> getDataSetFileNames() {
        List<String> result = DataSetCompany.getCompanyClientTestData();
        result.add(DataSetPricing.FILE_SUPPLIER_PO);
        result.add(DataSetCompany.FILE_COMPANY_SUPPLIER);
        return result;
    }
	
	@Test
	public void test_successGetPurchaseOrders() throws Exception {
		test_successViaURL("/accounting/purchaseorder");
	}

	@Test
	public void test_successGetPurchaseOrdersApi() throws Exception {
		test_successViaURL("/api/accounting/purchaseorder");
	}
	
	private void test_successViaURL(String endpointURL) throws Exception {
		this.mockMvc
				.perform(get(endpointURL+"?busId=" + TestDataCompany.ID_BUSINESS_FRANCE)
						.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)))
				.andDo(print()).andExpect(status().isOk())
				.andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$").exists()).andExpect(jsonPath("$").isNotEmpty())
				.andExpect(jsonPath("$[0].payTo_id").value(TestDataCompany.ID_SUPPLIER_FRANCE))
				.andExpect(jsonPath("$[0].poNo").value(TestDataPo.PO_NO_FR))
				.andExpect(jsonPath("$[0].payTo_fiscalId").value(TestDataCompany.FISCAL_ID_SUPPLIER_FRANCE))
				.andExpect(jsonPath("$[0].payTo_subdivId").value(TestDataSubdiv.ID_SUPPLIER_FRANCE))
				.andExpect(jsonPath("$[0].payTo_subdivFiscalId").value(TestDataSubdiv.FISCAL_ID_SUPPLIER_FRANCE))
				.andExpect(jsonPath("$[0].invoiceDate").value("2019-11-13"))
				.andExpect(jsonPath("$[0].invoiceNo").value("Test110"))
				.andExpect(jsonPath("$[0].busId").value(TestDataCompany.ID_BUSINESS_FRANCE));
	}
}
