package org.trescal.cwms.rest.generalserviceoperation;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.tests.MvcRestTestClass;

import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Disabled
public class RestStartGeneralServiceOperationControllerTest extends MvcRestTestClass {
    public static final String DATA_SET = "rest/generalserviceoperation/GeneralServiceOperation.xml";
    public static final String USERNAME = "technician";

    @Override
    protected List<String> getDataSetFileNames() {
        return Collections.singletonList(DATA_SET);
    }

    private void assertFail(String content, String expectedMessage, String expectedErrorMessage) throws Exception {
        this.mockMvc.perform(
                        post("/gso/start")
                                .accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
                                .contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
                                .content(content)
                                .sessionAttr(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.message").value(expectedMessage))
                .andExpect(jsonPath("$.success").value(false))
                .andExpect(jsonPath("$.results").isArray())
                .andExpect(jsonPath("$.errors[0].message").value(expectedErrorMessage));
    }

    private void assertSuccess(String content) throws Exception {
        this.mockMvc.perform(
                        post("/gso/start")
                                .accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
                                .contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
                                .content(content)
                                .sessionAttr(Constants.SESSION_ATTRIBUTE_USERNAME, USERNAME)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.message").value(""))
                .andExpect(jsonPath("$.success").value(true))
                .andExpect(jsonPath("$.results").isArray())
                .andExpect(jsonPath("$.results[0]").exists())
                .andExpect(jsonPath("$.results[1]").doesNotExist())
                .andExpect(jsonPath("$.results[0].gsoId").exists())
                .andExpect(jsonPath("$.results[0].gsoStatus").value("On-going"));
    }

    @Test
    public void test_ErrorNoWorkRequirements() throws Exception {
        String content = "{\"workRequirementIds\":[],\"hrid\" : \"en100200\"," +
                "\"actionDate\": \"2020-06-06T08:15:00.310\"}";
        String expectedMessage = "One or more errors are present, please see below:";
        String expectedErrorMessage = "Please mention at least one Jobitem Work requirement ID";
        assertFail(content, expectedMessage, expectedErrorMessage);
    }

    @Test
    public void test_ErrorWorkRequirementNotFound() throws Exception {
        String content = "{\"workRequirementIds\":[4],\"hrid\" : \"en100200\"," +
                "\"actionDate\": \"2020-06-06T08:15:00.310\"}";
        String expectedMessage = "One or more errors are present, please see below:";
        String expectedErrorMessage = "The following Work requirement IDs [4] are not found";
        assertFail(content, expectedMessage, expectedErrorMessage);
    }

    @Test
    public void test_ErrorWorkRequirementNotReadyForGso() throws Exception {
        String content = "{\"workRequirementIds\":[1],\"hrid\" : \"en100200\"," +
                "\"actionDate\": \"2020-06-06T08:15:00.310\"}";
        String expectedMessage = "One or more errors are present, please see below:";
        String expectedErrorMessage = "The following Work requirements IDs [1] are not ready for General service operation";
        assertFail(content, expectedMessage, expectedErrorMessage);
    }

    @Test
    public void test_ErrorNotAccreditedUserForStartGso() throws Exception {
        String content = "{\"workRequirementIds\":[2],\"hrid\" : \"en100200\"," +
                "\"actionDate\": \"2020-06-06T08:15:00.310\"}";
        String expectedMessage = "One or more errors are present, please see below:";
        String expectedErrorMessage = "The user is not accredited to perform this action for [2].";
        assertFail(content, expectedMessage, expectedErrorMessage);
    }

    @Test
    public void test_SuccessStartGso() throws Exception {
        String content = "{\"workRequirementIds\":[3],\"hrid\" : \"en100200\"," +
                "\"actionDate\": \"2020-06-06T08:15:00.310\"}";
        assertSuccess(content);
    }
}