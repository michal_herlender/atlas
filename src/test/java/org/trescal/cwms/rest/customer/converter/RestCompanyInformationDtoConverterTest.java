package org.trescal.cwms.rest.customer.converter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

import org.junit.Assert;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.paymentterms.PaymentTerm;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.rest.customer.component.RestCompanyInformationDtoConverter;
import org.trescal.cwms.rest.customer.dto.RestCompanyInformation;
import org.trescal.cwms.rest.customer.dto.RestCompanyInformationDto;

@Tag("org.trescal.cwms.core.AtlasUnitTest")
public class RestCompanyInformationDtoConverterTest {

	@Test
	public void shouldConvertNullModifiedDates() {
		RestCompanyInformation companyInformation = RestCompanyInformation.builder()
				.build();
		
		RestCompanyInformationDtoConverter converter = new RestCompanyInformationDtoConverter();
		RestCompanyInformationDto result = converter.convert(companyInformation);
		
		Assert.assertNull(result.getLastModified());
	}

	@Test
	public void shouldExcludeNullModifiedDates() {
		Date lastModified_2022_01_20 = DateTools.dateFromLocalDate(LocalDate.of(2022,  1,  20));
		Date lastModified_2022_01_21 = DateTools.dateFromLocalDate(LocalDate.of(2022,  1,  21));
		Date lastModified_2022_01_22 = DateTools.dateFromLocalDate(LocalDate.of(2022,  1,  22));
		
		RestCompanyInformation companyInformation = RestCompanyInformation.builder()
				.lastModifiedBusinessArea(null)
				.lastModifiedCompany(lastModified_2022_01_20)
				.lastModifiedCompanyGroup(null)
				.lastModifiedCompanySettings(lastModified_2022_01_21)
				.lastModifiedLegalAddress(null)
				.lastModifiedVatRate(lastModified_2022_01_22)
				
				.build();
		
		RestCompanyInformationDtoConverter converter = new RestCompanyInformationDtoConverter();
		RestCompanyInformationDto result = converter.convert(companyInformation);
		
		Assert.assertEquals(result.getLastModified(), LocalDate.of(2022,  1, 22));
	}
	
	@Test
	public void shouldConvertSingleResult() {
		
		Date lastModified_2022_01_20 = DateTools.dateFromLocalDate(LocalDate.of(2022,  1,  20));
		Date lastModified_2022_01_21 = DateTools.dateFromLocalDate(LocalDate.of(2022,  1,  21));
		
		RestCompanyInformation companyInformation = RestCompanyInformation.builder()
				.active(true)
				.companyId(1)
				.companyName("Name")
				.companyRole(CompanyRole.CLIENT)
				.companySector("Sector")
				.createDate(LocalDate.of(2022,  1,  12))
				.currencyCode("USD")
				.defaultVATRate(new BigDecimal(20))
				.fiscalId("Fiscal ID")
				.groupName("Group")
				.invoiceFrequency("IF")
				.lastModifiedBusinessArea(lastModified_2022_01_20)
				.lastModifiedCompany(lastModified_2022_01_20)
				.lastModifiedCompanyGroup(lastModified_2022_01_20)
				.lastModifiedCompanySettings(lastModified_2022_01_20)
				.lastModifiedLegalAddress(lastModified_2022_01_20)
				.lastModifiedVatRate(lastModified_2022_01_21)
				.legalAddress1("Address 1")
				.legalAddress2("Address 2")
				.legalAddress3("Address 3")
				.legalAddressCounty("County")
				.legalAddressTown("Town")
				.legalAddressPostCode("12345")
				.legalAddressCountryCode("US")
				.legalId("Legal ID")
				.onStop(true)
				.paymentMode("CK")
				.paymentTerm(PaymentTerm.P30)
				.taxable(true)
				.build();
		
		
		RestCompanyInformationDtoConverter converter = new RestCompanyInformationDtoConverter();
		RestCompanyInformationDto result = converter.convert(companyInformation);
		
		Assert.assertEquals(true, result.getActive());
		Assert.assertEquals(1, result.getCompanyId().intValue());
		Assert.assertEquals("Name", result.getCompanyName());
		Assert.assertEquals(CompanyRole.CLIENT, result.getCompanyRole());
		Assert.assertEquals("Sector", result.getCompanySector());
		Assert.assertEquals("US", result.getLegalAddressCountryCode());
		Assert.assertEquals(LocalDate.of(2022, 1, 12), result.getCreateDate());
		Assert.assertEquals("USD", result.getCurrencyCode());
		Assert.assertEquals(new BigDecimal(20), result.getDefaultVATRate());
		Assert.assertEquals("Fiscal ID", result.getFiscalId());
		Assert.assertEquals("Group", result.getGroupName());
		Assert.assertEquals("IF", result.getInvoiceFrequency());
		Assert.assertEquals(LocalDate.of(2022,  1,  21), result.getLastModified());
		Assert.assertEquals("Legal ID", result.getLegalId());
		Assert.assertEquals(true, result.getOnStop());
		Assert.assertEquals("CK", result.getPaymentMode());
		Assert.assertEquals(PaymentTerm.P30, result.getPaymentTerm());
		Assert.assertEquals("12345", result.getLegalAddressPostCode());
		Assert.assertEquals("County", result.getLegalAddressRegion());
		Assert.assertEquals(true, result.getTaxable());
		Assert.assertEquals("Town", result.getLegalAddressTown());
		
	}

}
