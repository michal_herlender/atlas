package org.trescal.cwms.rest.customer.repository;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.beans.SamePropertyValuesAs.samePropertyValuesAs;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.paymentterms.PaymentTerm;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.customer.dto.RestCompanyInformation;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetCompany;
import org.trescal.cwms.tests.generic.company.TestDataCompany;

public class RestCompanyDaoImplTest extends MvcRestTestClass {

	@Autowired
	private RestCompanyDao restCompanyDao; 
	
	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetCompany.getCompanyClientTestData();
	}

	private RestCompanyInformation getExpectedFranceClientInfo() throws Exception {
		Timestamp lastModifiedAddress = new Timestamp((new SimpleDateFormat("yyyy-MM-dd")).parse("2019-09-08").getTime());
		Timestamp lastModifiedBusinessArea = new Timestamp((new SimpleDateFormat("yyyy-MM-dd")).parse("2022-01-12").getTime());
		Timestamp lastModifiedCompany = new Timestamp((new SimpleDateFormat("yyyy-MM-dd")).parse("2019-09-08").getTime());
		Timestamp lastModifiedCompanyGroup = new Timestamp((new SimpleDateFormat("yyyy-MM-dd")).parse("2022-01-12").getTime());
		Timestamp lastModifiedCompanySettings = new Timestamp((new SimpleDateFormat("yyyy-MM-dd")).parse("2022-01-13").getTime());
		Timestamp lastModifiedVatRate = new Timestamp((new SimpleDateFormat("yyyy-MM-dd")).parse("2019-09-08").getTime());
		
		RestCompanyInformation expectedInfo = RestCompanyInformation.builder()
				.active(true)
				.companyId(TestDataCompany.ID_CLIENT_FRANCE)
				.companyName(TestDataCompany.COMPANY_NAME_CLIENT_FRANCE)
				.companyRole(CompanyRole.CLIENT)
				.companySector("Area 1")
				.createDate(LocalDate.of(2019,9,1))
				.currencyCode("EUR")
				.defaultVATRate(new BigDecimal("20.00"))
				.fiscalId("FR110456789")
				.groupName("Client Group 1")
				.invoiceFrequency("OG")
				.invoiceFactoring(true)
				.lastModifiedBusinessArea(lastModifiedBusinessArea)
				.lastModifiedCompany(lastModifiedCompany)
				.lastModifiedCompanyGroup(lastModifiedCompanyGroup)
				.lastModifiedCompanySettings(lastModifiedCompanySettings)
				.lastModifiedLegalAddress(lastModifiedAddress)
				.lastModifiedVatRate(lastModifiedVatRate)
				.legalAddress1("Parc d'Affaires Example 2")
				.legalAddress2("456 rue de l'Example")
				.legalAddress3(null)
				.legalAddressCountryCode("FR")
				.legalAddressCounty("")
				.legalAddressPostCode("41000")
				.legalAddressTown("Blois")
				.legalId("110456789")
				.onStop(false)
				.paymentMode("WT")
				.paymentTerm(PaymentTerm.P30)
				.taxable(true)
				.build();
		
		return expectedInfo;
	}
	
	@Test
	public void shouldReturnSingleCompanyByCompanyId() throws Exception {
		Integer orgId = TestDataCompany.ID_BUSINESS_FRANCE;
		Integer companyId = TestDataCompany.ID_CLIENT_FRANCE;
		
		PagedResultSet<RestCompanyInformation> prs = new PagedResultSet<>(100, 0);  
		this.restCompanyDao.searchCompanyInformationByCompanyId(prs, companyId, orgId);
		
		Assert.assertEquals(1, prs.getResultsCount());		
		RestCompanyInformation actualInfo = prs.getResults().iterator().next();
		
		RestCompanyInformation expectedInfo = getExpectedFranceClientInfo();
		
		// does shallow compare - works well for projections / DTOs
		assertThat(actualInfo, samePropertyValuesAs(expectedInfo));
	}
	
	@Test
	public void shouldReturnSingleCompanyByLegalId() throws Exception {
		Integer orgId = TestDataCompany.ID_BUSINESS_FRANCE;
		String legalId = TestDataCompany.LEGAL_ID_CLIENT_FRANCE;
		
		PagedResultSet<RestCompanyInformation> prs = new PagedResultSet<>(100, 0);  
		this.restCompanyDao.searchCompanyInformationByLegalId(prs, legalId, orgId);
		
		Assert.assertEquals(1, prs.getResultsCount());		
		RestCompanyInformation actualInfo = prs.getResults().iterator().next();
		
		RestCompanyInformation expectedInfo = getExpectedFranceClientInfo();
		
		// does shallow compare - works well for projections / DTOs
		assertThat(actualInfo, samePropertyValuesAs(expectedInfo));
	}
	
	@Test
	public void shouldReturnTwoCompaniesByLastModifiedFrom() throws Exception {
		Integer orgId = TestDataCompany.ID_BUSINESS_FRANCE;
		LocalDate modifiedFrom = LocalDate.of(2019, 9, 8);
		
		PagedResultSet<RestCompanyInformation> prs = new PagedResultSet<>(100, 0);  
		this.restCompanyDao.searchCompanyInformationByModifiedFrom(prs, modifiedFrom, orgId);
		
		Assert.assertEquals(2, prs.getResultsCount());		
		RestCompanyInformation actualInfo = prs.getResults().iterator().next();
		// Sorted by ID so company 110 (French client) should be the first result...
		RestCompanyInformation expectedInfo = getExpectedFranceClientInfo();
		
		// does shallow compare - works well for projections / DTOs
		assertThat(actualInfo, samePropertyValuesAs(expectedInfo));
	}
	
	@Test
	public void shouldReturnNoCompaniesByLastModifiedFrom() throws Exception {
		Integer orgId = TestDataCompany.ID_BUSINESS_FRANCE;
		// No modifiedFrom values on the various entities should be equal to or greater than this...
		LocalDate modifiedFrom = LocalDate.of(2022, 1, 14);
		
		PagedResultSet<RestCompanyInformation> prs = new PagedResultSet<>(100, 0);  
		this.restCompanyDao.searchCompanyInformationByModifiedFrom(prs, modifiedFrom, orgId);
		
		Assert.assertEquals(0, prs.getResultsCount());		
	}

}
