package org.trescal.cwms.rest.customer.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.paymentterms.PaymentTerm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.common.dto.output.RestPagedResultDTO;
import org.trescal.cwms.rest.customer.dto.RestCompanyInformationDto;
import org.trescal.cwms.tests.MvcRestTestClass;
import org.trescal.cwms.tests.generic.DataSetCompany;
import org.trescal.cwms.tests.generic.company.TestDataCompany;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public class RestCompanyControllerTest extends MvcRestTestClass {

	private ObjectMapper mapper;
	
	public RestCompanyControllerTest() {
		mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
	}
	
	@Override
	protected List<String> getDataSetFileNames() {
		return DataSetCompany.getCompanyClientTestData();
	}
	
	private RestCompanyInformationDto getExpectedFranceClientDto() {
		// Last modified is "latest" last modified date
		RestCompanyInformationDto expectedDto = RestCompanyInformationDto.builder()
				.active(true)
				.companyId(TestDataCompany.ID_CLIENT_FRANCE)
				.companyName(TestDataCompany.COMPANY_NAME_CLIENT_FRANCE)
				.companyRole(CompanyRole.CLIENT)
				.companySector("Area 1")
				.createDate(LocalDate.of(2019,9,1))
				.currencyCode("EUR")
				.defaultVATRate(new BigDecimal("20.0"))
				.fiscalId(TestDataCompany.FISCAL_ID_CLIENT_FRANCE)
				.groupName("Client Group 1")
				.invoiceFactoring(true)
				.invoiceFrequency("OG")
				.lastModified(LocalDate.of(2022,1,13))
				.legalAddress1("Parc d'Affaires Example 2")
				.legalAddress2("456 rue de l'Example")
				.legalAddress3(null)
				.legalAddressCountryCode("FR")
				.legalAddressRegion("")
				.legalAddressPostCode("41000")
				.legalAddressTown("Blois")
				.legalId(TestDataCompany.LEGAL_ID_CLIENT_FRANCE)
				.onStop(false)
				.paymentMode("WT")
				.paymentTerm(PaymentTerm.P30)
				.taxable(true)
				.build();
		return expectedDto;
	}

	private String performGetAndReturnContent(String url, int expectedResultCount) throws Exception {
		MvcResult mvcResult = this.mockMvc
		.perform(get(url)
			.accept(MediaType.parseMediaType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8)))
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(content().contentType(Constants.MEDIATYPE_APPLICATION_JSON_UTF8))
		.andExpect(jsonPath("$.resultsCount").value(expectedResultCount))
		.andReturn();
		
		String output = mvcResult.getResponse().getContentAsString();
		return output;
	}
	
	@Test
	public void shouldReturnForCompanyId() throws Exception {
		String url = "/customer/company?companyId="+TestDataCompany.ID_CLIENT_FRANCE+"&orgId="+TestDataCompany.ID_BUSINESS_FRANCE+"&locale=fr_FR";
		
		String output = performGetAndReturnContent(url, 1);

		RestPagedResultDTO<RestCompanyInformationDto> expectedResult = new RestPagedResultDTO<>(true, "", 100, 1);
		expectedResult.addResult(getExpectedFranceClientDto());
		expectedResult.setResultsCount(1);
		String expectedOutput = mapper.writeValueAsString(expectedResult);

        assertEquals(mapper.readTree(expectedOutput), mapper.readTree(output));
	}
	
	@Test
	public void shouldReturnForLegalId() throws Exception {
		String url = "/customer/company?legalId="+TestDataCompany.LEGAL_ID_CLIENT_FRANCE+"&orgId="+TestDataCompany.ID_BUSINESS_FRANCE+"&locale=fr_FR";
		
		String output = performGetAndReturnContent(url, 1);

		RestPagedResultDTO<RestCompanyInformationDto> expectedResult = new RestPagedResultDTO<>(true, "", 100, 1);
		expectedResult.addResult(getExpectedFranceClientDto());
		expectedResult.setResultsCount(1);
		String expectedOutput = mapper.writeValueAsString(expectedResult);

        assertEquals(mapper.readTree(expectedOutput), mapper.readTree(output));
	}
	
	@Test
	public void shouldReturnForLastModifiedFrom() throws Exception {
		// Correctness of underlying query verified in RestCompanyDaoImplTest
		String url = "/customer/company?modifiedFrom=2019-09-08&orgId="+TestDataCompany.ID_BUSINESS_FRANCE+"&locale=fr_FR";
		
		performGetAndReturnContent(url, 2);
	}
	
	@Test
	public void shouldReturnAllCompanies() throws Exception {
		String url = "/customer/company?orgId="+TestDataCompany.ID_BUSINESS_FRANCE+"&locale=fr_FR";
		
		performGetAndReturnContent(url, 2);
	}
}
