package org.trescal.cwms.tests.config;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseDataSourceConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.ext.mysql.MySqlMetadataHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.repository.NoRepositoryBean;

import lombok.extern.slf4j.Slf4j;

@Slf4j
//Commenting out for now; spring-data picks up when starting app with Eclipse; uncomment to run integration tests 
//@Configuration
public class DbUnitConnection {
	
	@Autowired
	private DataSource dataSource;
	@Autowired
	@Qualifier("dataSource2")
	private DataSource dataSource2;
	
	@Bean(name="dbUnitConnection")
	public IDatabaseConnection getConnection() {
		try {
			IDatabaseConnection connection = new DatabaseDataSourceConnection(dataSource);
			connection.getConfig().setProperty("http://www.dbunit.org/properties/datatypeFactory",
					new org.dbunit.ext.mssql.MsSqlDataTypeFactory());
			connection.getConfig().setProperty("http://www.dbunit.org/features/allowEmptyFields", Boolean.TRUE);
			connection.getConfig().setProperty(DatabaseConfig.PROPERTY_METADATA_HANDLER, new MySqlMetadataHandler());
			connection.getConfig().setProperty("http://www.dbunit.org/features/batchedStatements", Boolean.TRUE);
			return connection;
		}
		catch (SQLException e) {
			log.error("Exception creating datasource", e);
			throw new RuntimeException(e);
		}
	}
	@Bean(name="dbUnitConnection2")
	public IDatabaseConnection getConnection2() {
		try {
			IDatabaseConnection connection2 = new DatabaseDataSourceConnection(dataSource2);
			connection2.getConfig().setProperty("http://www.dbunit.org/properties/datatypeFactory",
					new org.dbunit.ext.mssql.MsSqlDataTypeFactory());
			connection2.getConfig().setProperty("http://www.dbunit.org/features/allowEmptyFields", Boolean.TRUE);
			connection2.getConfig().setProperty(DatabaseConfig.PROPERTY_METADATA_HANDLER, new MySqlMetadataHandler());
			connection2.getConfig().setProperty("http://www.dbunit.org/features/batchedStatements", Boolean.TRUE);
			return connection2;
		}
		catch (SQLException e) {
			log.error("Exception creating datasource", e);
			throw new RuntimeException(e);
		}
	}
}
