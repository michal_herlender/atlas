package org.trescal.cwms.tests.config;


import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;

public class ClockTestConfig extends Clock {

    public static final String currentDate = "2015-10-16T15:23:30.0Z";
    public static final String currentLocalDateTime = "2015-10-16T15:23:30.0";
    public static final String currentLocalDate = "2015-10-16";


    @Override
    public ZoneId getZone() {
        return ZoneId.of("Asia/Calcutta");
    }

    @Override
    public Clock withZone(ZoneId zone) {
        return Clock.fixed(this.instant(), getZone()).withZone(zone);
    }

    @Override
    public Instant instant() {
        return Instant.parse(currentDate);
    }
}
