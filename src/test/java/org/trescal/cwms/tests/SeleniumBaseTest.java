package org.trescal.cwms.tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.client.ClientUtil;
import net.lightbody.bmp.core.har.Har;
import net.lightbody.bmp.core.har.HarEntry;
import net.sf.ehcache.CacheManager;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.*;
import org.junit.rules.Stopwatch;
import org.junit.runner.Description;
import org.junit.runner.RunWith;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxDriverLogLevel;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.GenericXmlWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;

import java.io.File;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertNotEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(loader = GenericXmlWebContextLoader.class, locations = { 
		"classpath:applicationContext-dao.xml",
		"classpath:applicationContext-service.xml",
		"classpath:cwmsweb-servlet.xml",
		"classpath:mocks.xml",
		"classpath:applicationContext-integration-test.xml",
		"classpath:applicationContext-schedule.xml" })
public abstract class SeleniumBaseTest {

	private static Log logger = LogFactory.getLog(SeleniumBaseTest.class);

	protected static WebDriver webDriver;
	protected static BrowserMobProxy proxy;
	protected static String baseUrl;

	@Value("${cwms.integration.usename}")
	public String username;
	@Value("${cwms.integration.password}")
	public String password;
	@Value("${cwms.integration.usename1}")
	public String wrongusername;
	@Value("${cwms.integration.password1}")
	public String wrongpassword;

	/**
	 * Sets the base url for all selenium requests. Also changes the domain from
	 * localhost to local ip address because all browsers bypass the proxy
	 * settings for localhost
	 * 
	 * @param baseUrl
	 */
	@Value("${cwms.deployed.url}")
	public void setBaseUrl(String baseUrl) {
		SeleniumBaseTest.baseUrl = baseUrl;
		try {
			if (StringUtils.containsIgnoreCase(SeleniumBaseTest.baseUrl, "localhost")) {
				SeleniumBaseTest.baseUrl = SeleniumBaseTest.baseUrl.toLowerCase().replace("localhost",
						Inet4Address.getLocalHost().getHostAddress());
			} else if (SeleniumBaseTest.baseUrl.contains("127.0.0.1")) {
				SeleniumBaseTest.baseUrl = SeleniumBaseTest.baseUrl.replace("127.0.0.1",
						Inet4Address.getLocalHost().getHostAddress());
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

	public static void navigateFirefox() {
		// using chrome in headless mode using the argument "-headless"
		// if we want to display the result in the browser, we need just to
		// delete this argument ("-headless")
		WebDriverManager.firefoxdriver().proxy("gateway.zscaler.net:9400").setup();
		// WebDriverManager.firefoxdriver().setup();
		FirefoxOptions firefoxOptions = new FirefoxOptions();
		firefoxOptions.addArguments("-headless");
		firefoxOptions.addPreference("network.captive-portal-service.enabled", false);
		firefoxOptions.addPreference("dom.push.enabled", false);
		firefoxOptions.addPreference("services.blocklist.update_enabled", false);
		firefoxOptions.addPreference("intl.accept_languages", "en-us");
		firefoxOptions.setLogLevel(FirefoxDriverLogLevel.TRACE);

		proxy = getProxyServer();
		firefoxOptions.setCapability(CapabilityType.PROXY, getSeleniumProxy(proxy));
		webDriver = new FirefoxDriver(firefoxOptions);
		webDriver.manage().window().maximize();
		webDriver.manage().timeouts().pageLoadTimeout(240, TimeUnit.SECONDS);
		webDriver.manage().timeouts().setScriptTimeout(240, TimeUnit.SECONDS);
		webDriver.manage().timeouts().implicitlyWait(240, TimeUnit.SECONDS);
	}

	public static void navigateChrome() {
		// using chrome in headless mode using the argument '--headless'
		WebDriverManager.chromedriver().setup();
		ChromeOptions chromeOptions = new ChromeOptions();
		chromeOptions
        .addArguments("--headless");
		proxy = getProxyServer();
		chromeOptions.setCapability(CapabilityType.PROXY, getSeleniumProxy(proxy));
		webDriver = new ChromeDriver(chromeOptions);
		webDriver.manage().window().maximize();
		webDriver.manage().timeouts().pageLoadTimeout(240, TimeUnit.SECONDS);
		webDriver.manage().timeouts().setScriptTimeout(240, TimeUnit.SECONDS);
		webDriver.manage().timeouts().implicitlyWait(240, TimeUnit.SECONDS);
	}

	@BeforeClass
	public static void initWebDriver() throws Exception {
		// we used firefox because we encountered GoogleChrome launch problems
		// in headless mode in Jenkins
		// so if we want to use GoogleChrome as our browser, it's enough to
		// call the method 'navigateChrome()'
		navigateFirefox();
		// delete old screens if they exist
		File folder = new File("./errorScreens");
		if (folder.exists()) {
			for (File f : folder.listFiles()) {
				if ((f.getName().startsWith("error_"))) {
					f.delete();
				}
			}
		}

	}

	@AfterClass
	public static void terminateWebDriverAndProxy() {
		proxy.stop();
		// webDriver.close();
		webDriver.quit();
		CacheManager.getInstance().shutdown();
	}

	public static Integer getHttpStatus(String Url) {
		Har har = proxy.getHar();
		List<HarEntry> entries = har.getLog().getEntries();
		assertNotEquals(entries.size(), 0);
		for (HarEntry harEntry : entries) {
			if (harEntry.getRequest().getUrl().equalsIgnoreCase(Url))
				return harEntry.getResponse().getStatus();
		}

		return null;
	}

	@Before
	public void newHar() {
		proxy.newHar();
	}

	private static Proxy getSeleniumProxy(BrowserMobProxy proxyServer) {
		Proxy seleniumProxy = ClientUtil.createSeleniumProxy(proxyServer);
		try {
			String hostIp = Inet4Address.getLocalHost().getHostAddress();
			seleniumProxy.setHttpProxy(hostIp + ":" + proxyServer.getPort());
		} catch (UnknownHostException e) {
			e.printStackTrace();
			Assert.fail("invalid Host Address");
		}
		return seleniumProxy;
	}

	private static BrowserMobProxy getProxyServer() {
		BrowserMobProxy proxy = new BrowserMobProxyServer();
		proxy.setTrustAllServers(true);
		proxy.start();
		return proxy;
	}

	@Rule
	public Stopwatch stopwatch = new Stopwatch() {
		@Override
		protected void succeeded(long nanos, Description description) {
			logInfo(description, "succeeded", nanos);
		}

		@Override
		protected void failed(long nanos, Throwable e, Description description) {
			logInfo(description, "failed", nanos);
			// Create a errorScreens file in 'cwms' if not exist!
			File file = new File("./errorScreens/");
			if (!file.exists()) {
				if (file.mkdir()) {
					System.out.println("the file is created");
				}}
				// Take screenshots if the test fails and save it in the file
				// "error Screens"
				try {
					File screenshotFile = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);
					FileUtils.copyFile(screenshotFile,
							new File("./errorScreens/error_" + description.getMethodName() + ".png"));
				} catch (Exception e2) {
					System.out.println(e2.getMessage());
				}
		}

		@Override
		protected void skipped(long nanos, AssumptionViolatedException e, Description description) {
			logInfo(description, "skipped", nanos);
		}

		@Override
		protected void finished(long nanos, Description description) {
			logInfo(description, "finished", nanos);
		}
	};

	private static void logInfo(Description description, String status, long nanos) {
		String testName = description.getMethodName();
		logger.info(String.format("Test %s %s, spent %d microseconds / %d milliseconds / %d seconds", testName, status,
				TimeUnit.NANOSECONDS.toMicros(nanos), TimeUnit.NANOSECONDS.toMillis(nanos),
				TimeUnit.NANOSECONDS.toSeconds(nanos)));
	}

	public static void hoverOver(WebDriver driver, String args, String args2) throws Exception {
		Actions action = new Actions(driver);
		WebElement element = driver.findElement(By.xpath(args));
		action.moveToElement(element).build().perform();
		action.pause(2000).build().perform();
		driver.findElement(By.linkText(args2)).click();
	}

	public static boolean isElementPresent(By by) {
		try {
			webDriver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		} catch (Exception e) {
			return false;
		}
	}

}
