package org.trescal.cwms.tests;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.dbunit.database.DatabaseDataSourceConnection;
import org.springframework.jdbc.datasource.DataSourceUtils;

/*
 * Proxy that prevents commit of dBUnit (seems to be the only effective way to do this!)
 * See http://mikael-amborn.blogspot.com/2011/02/dbunit-tests-with-spring-and-nested.html
 * GB 2015-06-05
 */
public class NoCommitDatabaseDataSourceConnection extends DatabaseDataSourceConnection {
    private final DataSource dataSource;
    public NoCommitDatabaseDataSourceConnection(DataSource dataSource) throws SQLException {
        super(dataSource);
        this.dataSource = dataSource;
    }
 
    public Connection getConnection() throws SQLException {
        Connection conn = DataSourceUtils.getConnection(dataSource);
        return new NoCommitConnection(dataSource, conn);
    }
}
