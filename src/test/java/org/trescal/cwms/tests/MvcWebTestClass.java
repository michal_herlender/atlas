package org.trescal.cwms.tests;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.GenericXmlWebContextLoader;


/*
 * Base MVC test class for cwmsweb-servlet configuration
 * 
 * GB 2019-06-13 : There's an issue with cwmsweb-servlet.xml using WebTermsInterceptor, 
 * which uses SessionUtilsService and relies on SpringSecurity.  Need mock SessionUtilsService?
 * 
 */
@ContextConfiguration(loader = GenericXmlWebContextLoader.class,
        locations = {
                "classpath:applicationContext-dao.xml",
                "classpath:applicationContext-service.xml",
                "classpath:cwmsweb-servlet.xml",
                "classpath:mocks.xml",
                "classpath:applicationContext-mvc-test.xml",
                "classpath:applicationContext-schedule.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public abstract class MvcWebTestClass extends MvcBaseTestClass {

}
