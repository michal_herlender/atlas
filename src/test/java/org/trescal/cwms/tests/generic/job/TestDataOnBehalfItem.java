package org.trescal.cwms.tests.generic.job;

public class TestDataOnBehalfItem {
	
	public final static Integer RECORD_COUNT_STANDALONE = 3;
	
	public final static Integer ON_BEHALF_ITEM_ID_1 = 1;
	public final static Integer ON_BEHALF_ITEM_ID_2 = 2;
	public final static Integer ON_BEHALF_ITEM_ID_3 = 3;
	
	public final static Integer ON_BEHALF_ITEM_ADDRESS_ID = 1;
	public final static Integer ON_BEHALF_ITEM_JOBITEM_ID = 1;
	public final static Integer ON_BEHALF_ITEM_COMPANY_ID = 1;
	
	// As part of JobClientCompleted data
	public final static Integer RECORD_COUNT_COMPLETED = 5;

}
