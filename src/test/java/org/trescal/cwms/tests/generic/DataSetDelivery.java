package org.trescal.cwms.tests.generic;

import java.util.List;

public class DataSetDelivery {
	public static final String PATH_DELIVERY = "cwms/generic/delivery/"; 

	public static final String FILE_COURIER_DESPATCH = PATH_DELIVERY + "CourierDespatch.xml";
	public static final String FILE_JOB_DELIVERY_CLIENT = PATH_DELIVERY + "JobDeliveryClient.xml";
	public static final String FILE_JOB_DELIVERY_INTERNAL = PATH_DELIVERY + "JobDeliveryInternal.xml";
	public static final String FILE_JOB_DELIVERY_ITEMS = PATH_DELIVERY + "JobDeliveryItems.xml";
	public static final String FILE_JOB_DELIVERY_SUPPLIER = PATH_DELIVERY + "JobDeliverySupplier.xml";

	public static List<String> getDeliveryNoteClientTestData() {
		List<String> result = DataSetJob.getJobClientCompletedTestData();
		result.add(DataSetJob.FILE_CONTRACT_REVIEW);
		result.add(DataSetCompany.FILE_INSTRUCTION_CLIENT);
		result.add(DataSetCore.FILE_BASE_SERVICE_TYPE_TRANSLATIONS);
		result.add(FILE_JOB_DELIVERY_CLIENT);
		result.add(FILE_JOB_DELIVERY_ITEMS);
		result.add(FILE_COURIER_DESPATCH);
		result.add(DataSetCertificate.FILE_CLIENT_CERTIFICATE);
		return result;
	}
	
	
	public static List<String> getDeliveryNoteInternalTestData() {
		List<String> result = DataSetJob.getJobClientCompletedTestData();
		result.add(DataSetCompany.FILE_INSTRUCTION_CLIENT);
		result.add(DataSetCore.FILE_BASE_SERVICE_TYPE_TRANSLATIONS);
		result.add(FILE_JOB_DELIVERY_INTERNAL);
		result.add(FILE_JOB_DELIVERY_ITEMS);
		return result;
	}
	
	public static List<String> getDeliveryNoteSupplierTestData() {
		List<String> result = DataSetJob.getJobClientCompletedTestData();
		result.add(DataSetCompany.FILE_INSTRUCTION_CLIENT);
		result.add(DataSetCompany.FILE_COMPANY_SUPPLIER);
		result.add(DataSetCore.FILE_BASE_SERVICE_TYPE_TRANSLATIONS);
		result.add(FILE_JOB_DELIVERY_SUPPLIER);
		result.add(FILE_JOB_DELIVERY_ITEMS);
		result.add(FILE_COURIER_DESPATCH);
		return result;
	}
	
}
