package org.trescal.cwms.tests.generic.instrument;

public class TestDataInstrument {

	// Define IDs here for reference in test classes (adding when put to use in Java tests)

	public static final Integer ID_FRANCE_CALIPER_1101 = 1101;
	public static final Integer ID_FRANCE_FLUKE_77_1102 = 1102;
	
	public static final Integer ID_GERMANY_CALIPER_1201 = 1201;

	public static final Integer ID_USA_ADDITIONAL_CALIPER_1503 = 1503;
	
	public static final String PLANT_NO1 = "PN1";
	public static final String PLANT_NO2 = "PN2";

	public static final Integer RECORD_COUNT = 10;
	
	// Sample data from first record (plantid 1101)
	
	public static final Integer CAL_FREQUENCY_1101 = 12;
	public static final String FORMER_BARCODE_1101 = "IFBC1101";
	public static final String MODEL_NAME_1101 = "500-173";
	public static final Integer MODEL_ID_1101 = 4111;
	public static final String PLANT_NO_1101 = "IPN1101";
	public static final String NEXT_CAL_DATE_1101 = "2019-10-05";
	public static final String SERIAL_NO_1101 = "SN1101";
	public static final Integer ADDRESS_ID_1101 = 110;
	public static final Integer CONTACT_ID_1101 = 110;
	public static final Integer LOCATION_ID_1101 = 1101;
	public static final String CUSTOMER_DESCRIPTION_1101 = "CDESC1101";
	public static final String CUSTOMER_SPECIFICATION_1101 = "CSPEC1101";
	public static final String FREE_TEXT_LOCATION_1101 = "FTL1101";
	public static final String CLIENT_BARCODE_1101 = "CBC1101";
	public static final Boolean CALIBRATION_STANDARD_1101 = false;
	public static final Boolean SCRAPPED_1101 = null;
	public static final Integer USAGEID = 1;
	public static final boolean customermanaged = false;
	
	
	public static final Integer COID = 1;

	//SUCCESS TEST VALUES
	public static final Integer SUCCESS_PERSONID = 2;
	public static final Integer SUCCESS_ADDRESSID = 2;
	public static final Integer SUCCESS_LOCATIONID =  1;
	
	
	//FAIL TEST VALUES AS PERSON ID IS NOT CONSISTENT
	public static final Integer FAIL_PERSONID_1 = 4;
	public static final Integer FAIL_ADDRESSID_1 = 2;
	public static final Integer FAIL_LOCATIONID_1 =  1;
	
	
	//FAIL TEST VALUES AS ADDRESS ID ID IS NOT CONSISTENT
	public static final Integer FAIL_PERSONID_2 = 2;
	public static final Integer FAIL_ADDRESSID_2 = 4;
	public static final Integer FAIL_LOCATIONID_2 =  1;
	
	
	//FAIL TEST VALUES AS LOCATION ID IS NOT CONSISTENT
	public static final Integer FAIL_PERSONID_3 = 2;
	public static final Integer FAIL_ADDRESSID_3 = 2;
	public static final Integer FAIL_LOCATIONID_3 =  2;
	
	
}
