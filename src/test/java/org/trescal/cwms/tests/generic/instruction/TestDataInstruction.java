package org.trescal.cwms.tests.generic.instruction;

public class TestDataInstruction {
	public static final int RECORD_COUNT_COMPANY_INSTRUCTION = 5; 
	public static final int RECORD_COUNT_CONTACT_INSTRUCTION = 5; 
	public static final int RECORD_COUNT_JOB_DELIVERY_INSTRUCTION = 15; 
	public static final int RECORD_COUNT_SUBDIV_INSTRUCTION = 5;
	
	// One for each of the above
	public static final int RECORD_COUNT_BASE_INSTRUCTION = 
			RECORD_COUNT_COMPANY_INSTRUCTION + 
			RECORD_COUNT_CONTACT_INSTRUCTION + 
			RECORD_COUNT_JOB_DELIVERY_INSTRUCTION +
			RECORD_COUNT_SUBDIV_INSTRUCTION;  
}
