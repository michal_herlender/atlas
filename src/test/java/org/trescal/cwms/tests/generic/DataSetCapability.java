package org.trescal.cwms.tests.generic;

import java.util.List;

/**
 * Defines a set of generic capabilities that can be reused throughout Atlas for testing
 * 2021-03-01
 *
 */
public class DataSetCapability {
    public static final String PATH_PROCEDURE = "cwms/generic/procedure/";
    public static final String PATH_WORKINSTRUCTION = "cwms/generic/workinstruction/";

    public static final String FILE_PROCEDURE = PATH_PROCEDURE + "Procedure.xml";
    public static final String FILE_PROCEDURE_AUTHORIZATION = PATH_PROCEDURE + "ProcedureAuthorization.xml";
    public static final String FILE_PROCEDURE_SUBFAMILY = PATH_PROCEDURE + "ProcedureSubfamily.xml";

    public static final String FILE_WORK_INSTRUCTION = PATH_WORKINSTRUCTION + "WorkInstruction.xml";
    public static final String FILE_INSTRUMENT_WORK_INSTRUCTION = PATH_WORKINSTRUCTION + "InstrumentWorkInstruction.xml";

    public static List<String> getCapabilityData() {
        List<String> result = DataSetCompany.getCompanyBusinessTestData();
        result.add(DataSetCore.FILE_BASE_CALIBRATION);
        result.add(FILE_PROCEDURE);
        return result;
    }
	
	public static List<String> getWorkInstructionData() {
		List<String> result = DataSetCompany.getCompanyBusinessTestData();
		result.add(DataSetInstrument.FILE_INSTRUMENT_MODEL);
		result.add(FILE_WORK_INSTRUCTION);
		return result;
	}
}
