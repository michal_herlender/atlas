package org.trescal.cwms.tests.generic.pricing;

import java.math.BigDecimal;

import org.trescal.cwms.core.pricing.entity.costtype.CostType;

public class TestDataCreditNoteItem {
	// 2 credit note items
	public static final int RESULT_COUNT_CREDIT_NOTE_US_150 = 2;
	
	// For first credit note item
	public static final Integer ITEM_ID_US_1501 = 1501;
	public static final Integer ITEM_NUMBER_US_1501 = 1;
	public static final String DESCRIPTION_US_1501 = "Sample description of credit note 150 item 1";
	public static final Integer QUANTITY_US_1501 = 1;
	public static final BigDecimal FINAL_COST_US_1501 = new BigDecimal("150.00");
	public static final Boolean TAXABLE_US_1501 = true;
	public static final BigDecimal TAX_AMOUNT_US_1501 = new BigDecimal("15.00");
	public static final Integer NOMINAL_CODE_US_1501 = 159;
	public static final CostType COST_TYPE_US_1501 = CostType.CALIBRATION;

	// For second credit note item
	public static final Integer ITEM_ID_US_1502 = 1502;
	public static final Integer ITEM_NUMBER_US_1502 = 2;
}
