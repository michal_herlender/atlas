package org.trescal.cwms.tests.generic.job;

public class TestDataPo {
	
	public final static Integer PO_ID_CLIENT_FR = 110;
	public final static String PO_NO_FR = "FR005-PO-19000110";
	public final static Boolean ACTIVE_CLIENT_FR = null;
	public final static Integer JOB_ID_CLIENT_FR = 1;  
	public final static String PO_NUMBER_CLIENT_FR ="Job PO 110";
	public final static Boolean JOB_DEFAULT_CLIENT_FR = null;
	public final static String COMMENT = null;
			  
}
