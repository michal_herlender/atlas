package org.trescal.cwms.tests.generic.company;

public class TestDataSubdiv {
	// Define IDs here for reference in test classes (adding when put into use in Java tests)
	
	public static final Integer ID_BUSINESS_FRANCE = 510;
	public static final Integer ID_BUSINESS_GERMANY = 520;
	public static final Integer ID_BUSINESS_MOROCCO = 530;
	public static final Integer ID_BUSINESS_SPAIN = 540;
	public static final Integer ID_BUSINESS_USA = 550;
	
	public static final Integer ID_CLIENT_FRANCE = 110;
	public static final Integer ID_CLIENT_GERMANY = 120;
	public static final Integer ID_CLIENT_MOROCCO = 130;
	public static final Integer ID_CLIENT_SPAIN = 140;
	public static final Integer ID_CLIENT_USA = 150;
	
	public static final Integer ID_SUPPLIER_FRANCE = 310;
	
	public static final String FISCAL_ID_BUSINESS_FRANCE = "TEST_510";
	public static final String FISCAL_ID_CLIENT_FRANCE = "TEST_110";
	public static final String FISCAL_ID_SUPPLIER_FRANCE = "TEST_310";
	
	public static final String FISCAL_ID_CLIENT_USA = "123456789";
	
	public static final Integer RECORD_COUNT_BUSINESS = 5;
	public static final Integer RECORD_COUNT_CLIENT = 5;
	public static final Integer RECORD_COUNT_TOTAL = 10;
	
	public static final Boolean ACTIVE_BUSINESS_USA_DTW = true;
	public static final String SUBNAME_BUSINESS_USA_DTW = "Detroit";
	public static final String SUBDIV_CODE_BUSINESS_USA_DTW = "DTW";
	
	public static final Boolean ACTIVE_BUSINESS_MOROCCO_CASA = true;
	public static final String SUBNAME_BUSINESS_MOROCCO_CASA = "Casablanca";
	public static final String SUBDIV_CODE_BUSINESS_MOROCCO_CASA = "Casablanca";
	public static final String SUBNAME_BUSINESS_FRANCE_RUNGIS = "Rungis";
	
}
