package org.trescal.cwms.tests.generic.company;

public class TestDataLocation {
	public static final Integer ID_RUNGIS_5101 = 5101;
	public static final Integer ID_RUNGIS_5102 = 5102;
	public static final Integer ID_RUNGIS_5103 = 5103;
	public static final Integer ID_HARTLAND_5101 = 5501;
	public static final Integer ID_HARTLAND_5102 = 5502;
	public static final Integer ID_HARTLAND_5103 = 5503;
	
	public static final Integer ID_CLIENT_FRANCE_1101 = 1101;
			
	public static final String LOCATION_RUNGIS_5101 = "Rungis 5101 - Inactive";
	public static final String LOCATION_RUNGIS_5102 = "Rungis 5102";
	
	public static final Integer RECORD_COUNT_BUSINESS = 6;
	public static final Integer RECORD_COUNT_CLIENT = 10;
	public static final Integer RECORD_COUNT_TOTAL = RECORD_COUNT_BUSINESS + RECORD_COUNT_CLIENT;
	
}
