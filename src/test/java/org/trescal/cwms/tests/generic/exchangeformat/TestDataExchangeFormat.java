package org.trescal.cwms.tests.generic.exchangeformat;

public class TestDataExchangeFormat {
	
	// IDs of exchange formats spaced by 100 so that fields could be 101, 102, etc...
	public static final int ID_EXTERNAL_SYSTEM = 100;
	public static final int ID_QUOTATION_ITEMS_IMPORTATION = 101;
}
