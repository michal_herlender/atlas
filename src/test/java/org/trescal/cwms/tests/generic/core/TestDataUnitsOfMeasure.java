package org.trescal.cwms.tests.generic.core;

public class TestDataUnitsOfMeasure {
	public static final int RECORD_COUNT = 2;
	
	public static final int ID_VOLTS_49 = 49;
	public static final int ID_INCH_186 = 186;
	
	public static final String NAME_VOLTS_49 = "Volts";
	public static final String SYMBOL_VOLTS_49 = "V";
	public static final int TMLID_VOLTS_49 = 386;
	public static final boolean ACTIVE_VOLTS_49 = true;
}
