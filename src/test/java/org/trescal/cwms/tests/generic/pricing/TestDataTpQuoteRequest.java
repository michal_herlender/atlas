package org.trescal.cwms.tests.generic.pricing;

public class TestDataTpQuoteRequest {
	public static final Integer ID_ACTIVE_FRANCE = 110;
	public static final Integer ID_ACTIVE_Germany = 120;
	public static final Integer ID_ACTIVE_Morocco = 130;
	public static final Integer ID_ACTIVE_Spain = 140;
	public static final Integer ID_ACTIVE_USA = 150;
}
