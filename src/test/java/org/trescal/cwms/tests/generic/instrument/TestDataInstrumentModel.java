package org.trescal.cwms.tests.generic.instrument;

import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.instrumentmodel.entity.description.SubfamilyTypology;

public class TestDataInstrumentModel {

	// Quantity of records defined in test data set
	public static final Integer RECORD_COUNT = 8;

	// Define IDs here for reference in test classes (adding when put Integero use in Java tests)

	public static final Integer ID_STANDALONE_CALIPER_300MM = 4111;
	public static final Integer ID_STANDALONE_FLUKE_77 = 21441;
	
	public static final Integer ID_SALESCATEGORY_CALIPER_TO_300MM = 1103;
	public static final Integer ID_SALESCATEGORY_CALIPER_QUARANTINED = 3802;
	public static final Integer ID_CAPABILITY_CALIPER_QUARANTINED = 33735;
	public static final Integer ID_SERVICE_QUARANTINED = 39148;
	public static final Integer ID_SERVICE_LOGISTICS_PACKAGING = 43571;
	public static final Integer ID_STANDALONE_CALIPER_QUARANTINED = 5400;
	

/*
<instmodel modelid="21441" descriptionid="207" lastModified="2019-10-07" mfrid="3558" 
	model="77" modelmfrtype="MFR_SPECIFIC" modeltypeid="1" quarantined="0" tmlid="10656" />
 */
	// Define data here for use in testing model related data (e.g. specific projections), documenting Fluke 77 data only here
	public static final Integer DESCRIPTION_ID_FLUKE_77 = 207;
	public static final Integer MFR_ID_FLUKE_77 = 3558;
	public static final String MODEL_FLUKE_77 = "77";
	public static final ModelMfrType MODEL_MFR_TYPE_FLUKE_77 = ModelMfrType.MFR_SPECIFIC;
	public static final Integer MODEL_TYPE_ID_FLUKE_77 = 1;
	public static final Boolean QUARANTINED_FLUKE_77 = false;
	public static final Integer TML_ID_FLUKE_77 = 10656;
	
	public static final Integer DESCRIPTION_CALIPER_300MM = 460;
	public static final Integer MFR_ID_CALIPER_300MM = 6427;
	public static final ModelMfrType MODEL_MFR_CALIPER_300MM = ModelMfrType.MFR_GENERIC;
	
	public static final String MODEL_NAME_CALIPER_300MM = "/";
	public static final Boolean MODEL_TYPE_CALIPER_300MM = false;
	public static final Boolean QUARANTINED_CALIPER_300MM = false;
	public static final SubfamilyTypology SUBFAMILYTYPOLOGY_CALIPER_300MM = SubfamilyTypology.SFC;
}
