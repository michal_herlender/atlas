package org.trescal.cwms.tests.generic.job;

public class TestDataJobStatus {
	
	public final static Integer ID_ONGOING_1 = 1;
	public final static Integer ID_AWAITING_INVOICE_2 = 2;
	public final static Integer ID_COMPLETE_3 = 3;
}
