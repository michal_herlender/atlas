package org.trescal.cwms.tests.generic.job;

public class TestDataJobItemAction {
	public static final Integer ID_JOB_ITEM_1101_RECEIVED = 11010;
	public static final Integer ID_JOB_ITEM_1101_REVIEWED = 11011;
	
	public static final Integer ID_JOB_ITEM_1102_RECEIVED = 11020;
	public static final Integer ID_JOB_ITEM_1102_REVIEWED = 11021;
	
	public static final Integer ID_JOB_ITEM_1301_RECEIVED = 13010;
	public static final Integer ID_JOB_ITEM_1301_REVIEWED = 13011;

	public static final Integer ID_JOB_ITEM_1302_RECEIVED = 13020;
	public static final Integer ID_JOB_ITEM_1302_REVIEWED = 13021;
}
