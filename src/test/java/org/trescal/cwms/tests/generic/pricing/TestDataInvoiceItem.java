package org.trescal.cwms.tests.generic.pricing;

import java.math.BigDecimal;

import org.trescal.cwms.core.pricing.entity.costtype.CostType;

public class TestDataInvoiceItem {
	// 5 from job items, and 2 additional items
	public static final int RESULT_COUNT_INVOICE_US_150 = 7;
	
	// For first job item
	public static final Integer ITEM_ID_US_15011 = 15011;
	public static final Integer ITEM_NUMBER_US_15011 = 1;
	public static final String DESCRIPTION_US_15011 = "Calibration Description for Invoice Item 1A";
	public static final Integer QUANTITY_US_15011 = 1;
	public static final BigDecimal FINAL_COST_US_15011 = new BigDecimal("140.00");
	public static final Boolean TAXABLE_US_15011 = true;
	public static final BigDecimal TAX_AMOUNT_US_15011 = new BigDecimal("14.00");
	public static final Integer NOMINAL_CODE_US_15011 = 159;
	public static final CostType COST_TYPE_US_15011 = CostType.CALIBRATION;
	public static final Integer JOB_ITEM_ID_US_15011 = 1501;
	
	// For first expense item
	public static final Integer ITEM_ID_US_1503 = 1503;
	public static final Integer ITEM_NUMBER_US_1503 = 6;
	public static final String DESCRIPTION_US_1503 = "Service Description for Invoice Item 3";
	public static final Integer QUANTITY_US_1503 = 1;
	public static final BigDecimal FINAL_COST_US_1503 = new BigDecimal("140.00");
	public static final Boolean TAXABLE_US_1503 = true;
	public static final BigDecimal TAX_AMOUNT_US_1503 = new BigDecimal("14.00");
	public static final Integer NOMINAL_CODE_US_1503 = 162;
	public static final CostType COST_TYPE_US_1503 = null;
}
