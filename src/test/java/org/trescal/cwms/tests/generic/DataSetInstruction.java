package org.trescal.cwms.tests.generic;

import java.util.ArrayList;
import java.util.List;

public class DataSetInstruction {
	public static final String PATH_INSTRUCTION = "cwms/generic/instruction/"; 
	
	public static final String FILE_INSTRUCTION_COMPANY = PATH_INSTRUCTION + "CompanyInstruction.xml";
	public static final String FILE_INSTRUCTION_CONTACT = PATH_INSTRUCTION + "ContactInstruction.xml";
	public static final String FILE_INSTRUCTION_JOB_DELIVERY = PATH_INSTRUCTION + "JobDeliveryInstruction.xml";
	public static final String FILE_INSTRUCTION_SUBDIV = PATH_INSTRUCTION + "SubdivInstruction.xml";
	
	/**
	 * Must be used in addition to other test data files (e.g. client job data)
	 * @return
	 */
	public static List<String> getJobClientCompletedTestData() {
		List<String> result = new ArrayList<>();
		result.add(FILE_INSTRUCTION_COMPANY);
		result.add(FILE_INSTRUCTION_CONTACT);
		result.add(FILE_INSTRUCTION_JOB_DELIVERY);
		result.add(FILE_INSTRUCTION_SUBDIV);
		return result;
	}
	
}
