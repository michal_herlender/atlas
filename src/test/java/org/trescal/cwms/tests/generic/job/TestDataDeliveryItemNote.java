package org.trescal.cwms.tests.generic.job;

public class TestDataDeliveryItemNote {
	
	public final static Integer NOTE_ID = 12010;
	public final static Integer ENTITY_ID = 1201;
	public final static Integer DELIVERY_ID = 120;
	public final static String LABEL = "Label 1";
	public final static Boolean ACTIVE = true; 
	public final static Boolean PUBLISH = true; 
	public final static Integer DELIVERY_ITEM_ID = 1101; 
	public final static String NOTE = "Delivery Item Note remark 1 - sample text here";

}
