package org.trescal.cwms.tests.generic.core;

public class TestDataSupportedCurrency {

	// Define IDs here for reference in test classes (adding when put Integero use in Java tests)
	
	public static final Integer ID_GBP = 1;
	public static final Integer ID_USD = 2;
	public static final Integer ID_EUR = 3;
	public static final Integer ID_SGD = 4;
	public static final Integer ID_MAD = 8;

}
