package org.trescal.cwms.tests.generic;

import java.util.ArrayList;
import java.util.List;

public class DataSetInstrument {
	public static final String PATH_INSTRUMENT = "cwms/generic/instrument/";
	public static final String PATH_INSTRUMENT2 = "cwms/instrument/";

	public static final String FILE_INSTRUMENT_CLIENT = PATH_INSTRUMENT + "InstrumentClient.xml";
	public static final String FILE_INSTRUMENT_CLIENT_ADDITIONAL = PATH_INSTRUMENT + "InstrumentClientAdditional.xml";
	public static final String FILE_INSTRUMENT_MODEL = PATH_INSTRUMENT + "InstrumentModel.xml";
	public static final String FILE_IMPORTED_INSTRUMENT = PATH_INSTRUMENT2 + "ImportedInstrumentFile.xml";
	public static final String FILE_INSTRUMENT_USGAE_TYPE = PATH_INSTRUMENT + "InstrumentUsageType.xml";
	
	public static List<String> getInstrumentClientTestData() {
		List<String> result = DataSetCore.getAllBaseData();
		result.add(DataSetCompany.FILE_COMPANY_BUSINESS);
		result.add(DataSetCompany.FILE_COMPANY_CLIENT);
		result.add(DataSetInstrument.FILE_INSTRUMENT_CLIENT);
		result.add(DataSetInstrument.FILE_INSTRUMENT_MODEL);
		return result;
	}

	public static List<String> getInstrumentModelTestData() {
		List<String> result = new ArrayList<>();
		result.add(DataSetInstrument.FILE_INSTRUMENT_MODEL);
		return result;
	}
	
	public static List<String> getInstrumentUsageTypeTestData() {
		List<String> result = new ArrayList<>();
		result.add(DataSetInstrument.FILE_INSTRUMENT_USGAE_TYPE);
		return result;
	}

}
