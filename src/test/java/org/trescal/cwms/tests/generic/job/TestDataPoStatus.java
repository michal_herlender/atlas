package org.trescal.cwms.tests.generic.job;

public class TestDataPoStatus {
	public final static int PO_STATUS_SIZE = 1;
	public final static Integer STATUS_ID = 2;
	
	public final static String COMP_NAME_FR = "Test Supplier Company - France";
	public final static String REG_DATE = "2019-10-02"; 

	public final static String NAME_STATUS_2 = "awaiting construction";
	public final static String NAME_TRANSLATION_STATUS_2 = "En attente de construction";

	public final static String DESCRIPTION_STATUS_2 ="Awaiting construction of purchase order";
	public final static String DESCRIPTION_TRANSLATION_STATUS_2 = "En attente de construction du bon de commande";
	public final static Boolean REQUIRES_ATTETION = true;

}