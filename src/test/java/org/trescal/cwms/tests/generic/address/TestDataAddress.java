package org.trescal.cwms.tests.generic.address;

public class TestDataAddress {
	// IDs of addresses
	public static final Integer ID_ADDRESS_CLIENT_FRANCE = 110;
	
	public static final Integer ID_ADDRESS_RUNGIS = 510;
	public static final Integer ID_ADDRESS_DARMSTADT = 520;
	public static final Integer ID_ADDRESS_CASABLANCA = 530;
	public static final Integer ID_ADDRESS_ZARAGOZA = 540;
	public static final Integer ID_ADDRESS_HARTLAND = 550;
	
	public static final Integer RECORD_COUNT_BUSINESS = 5;
	public static final Integer RECORD_COUNT_CLIENT = 6;	// test 2 addresses for 1 client
	public static final Integer RECORD_COUNT_TOTAL = RECORD_COUNT_BUSINESS + RECORD_COUNT_CLIENT;
	
	public static final Boolean ACTIVE_HARTLAND = true;
	public static final String ADDR_1_HARTLAND = "1200 North Old US 23";
	public static final String ADDR_2_HARTLAND = "PO Box 559";
	public static final String ADDR_3_HARTLAND = "";
	public static final String TOWN_HARTLAND = "Hartland";
	public static final String COUNTY_HARTLAND = "MI";
	public static final String POSTCODE_HARTLAND = "48353";
	public static final String TELEPHONE_HARTLAND = "1-345-678-9012";
	public static final String FAX_HARTLAND = "1-876-653-2109";
}
