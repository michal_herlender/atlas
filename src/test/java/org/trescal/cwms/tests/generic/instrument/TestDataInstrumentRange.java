package org.trescal.cwms.tests.generic.instrument;

public class TestDataInstrumentRange {

	// Define IDs here for reference in test classes

	public static final Integer ID_FRANCE_CALIPER_1101 = 1101;
	public static final Integer ID_FRANCE_FLUKE_77_1102 = 1102;

	// Just defining two ranges, one for each French record
	public static final Integer RECORD_COUNT = 2;
	
	public static final Double START_1101 = 0d;
	public static final Double ENDD_1101 = 6d;
	public static final Integer UOM_ID_1101 = 186;
	
	public static final Double START_1102 = 0d;
	public static final Double ENDD_1102 = 1000d;
	public static final Integer UOM_ID_1102 = 49;
	public static final Integer MAX_UOM_ID_1102 = 49;
}
