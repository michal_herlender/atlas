package org.trescal.cwms.tests.generic.pricing;

import java.math.BigDecimal;

public class TestDataQuotationItem {

	public static final Integer ID_ACTIVE_FRANCE_FLUKE_77_1102 = 1102;
	public static final BigDecimal VALUE_DISCOUNT_RATE_ACTIVE_INSTRUMENT_1102 = new BigDecimal("0.00");
	public static final BigDecimal VALUE_TOTAL_COST_ACTIVE_INSTRUMENT_1102 = new BigDecimal("45.00");

}
