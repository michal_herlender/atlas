package org.trescal.cwms.tests.generic;

public class DataSetSystem {
	public static final String PATH_BASE = "cwms/generic/system/"; 

	// Individual scheduled tasks defined to they could be enabled / tested individually
	public static final String FILE_SCHEDULED_TASK_CDC_TRANSFORM = PATH_BASE + "ScheduledTaskCdcTransform.xml";

}
