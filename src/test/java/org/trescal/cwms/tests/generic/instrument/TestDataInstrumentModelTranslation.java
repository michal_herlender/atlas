package org.trescal.cwms.tests.generic.instrument;

public class TestDataInstrumentModelTranslation {

	// Quantity of records defined in test data set (models * locales)
	public static final Integer RECORD_COUNT = 8 * 5;
	public static final Integer RECORD_COUNT_FLUKE_77 = 5;

	// Defining data here for FLUKE_77 instrument model only

	public static final String TRANSLATION_DE_DE_CALIPER_300MM = "MESSSCHIEBER Length max < 300 mm";
	public static final String TRANSLATION_EN_GB_CALIPER_300MM = "CALIPER Length max < 300 mm";
	public static final String TRANSLATION_ES_ES_CALIPER_300MM = "PIE DE REY Length max < 300 mm";
	public static final String TRANSLATION_FR_FR_CALIPER_300MM = "PIED A COULISSE Length max < 300 mm";
	
	public static final String TRANSLATION_DE_DE_FLUKE_77 = "DIGITAL-MULTIMETER FLUKE 77";
	public static final String TRANSLATION_EN_GB_FLUKE_77 = "DIGITAL MULTIMETER FLUKE 77";
	public static final String TRANSLATION_EN_US_FLUKE_77 = "DIGITAL MULTIMETER FLUKE 77";
	public static final String TRANSLATION_ES_ES_FLUKE_77 = "MULTIMETRO DIGITAL FLUKE 77";
	public static final String TRANSLATION_FR_FR_FLUKE_77 = "MULTIMETRE NUMERIQUE FLUKE 77";
	
	public static final String LOCALE_DE_DE = "de_DE";
	public static final String LOCALE_EN_GB = "en_GB";
	public static final String LOCALE_EN_US = "en_US";
	public static final String LOCALE_ES_ES = "es_ES";
	public static final String LOCALE_FR_FR = "fr_FR";
}
