package org.trescal.cwms.tests.generic.core;

public class TestDataItemState {
    public static final Integer RECORD_COUNT = 7;

    public static final Integer ID_UNIT_RECEIVED_3 = 3;
    public static final Integer ID_AWAITING_CONTRACT_REVIEW_7 = 7;
    public static final Integer ID_AWAITING_CALIBRATION_19 = 19;
    public static final Integer ID_PRE_CALIBRATION_26 = 26;
    public static final Integer ID_WORK_COMPLETED_54 = 54;

    public static final String TYPE_UNIT_RECEIVED_3 = "transitactivity";
    public static final Boolean ACTIVE_UNIT_RECEIVED_3 = true;
    public static final Boolean RETIRED_UNIT_RECEIVED_3 = false;
    public static final String DESCRIPTION_UNIT_RECEIVED_3 = "Unit received at goods in";
}
