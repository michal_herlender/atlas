package org.trescal.cwms.tests.generic;

public class DataSetCertificate {
	public static final String PATH_CERTIFICATE = "cwms/generic/certificate/"; 
	
	public static final String FILE_CLIENT_CERTIFICATE = PATH_CERTIFICATE + "CertificateClient.xml";

}
