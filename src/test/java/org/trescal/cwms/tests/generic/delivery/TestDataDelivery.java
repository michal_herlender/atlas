package org.trescal.cwms.tests.generic.delivery;

public class TestDataDelivery {
	public static int ID_USA_150 = 150;
	
	// Expect 3 Company, 3 Subdiv, 3 Contact, 3 Job (all CARRIAGE/PACKAGING)
	public static int COUNT_INSTRUCTIONS_PER_DELIVERY = 12;
	// Expect 1 certificate for delivery 150 only
	public static int COUNT_CERTIFICATES_PER_DELIVERY = 1;
}
