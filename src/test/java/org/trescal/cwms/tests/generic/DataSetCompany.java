package org.trescal.cwms.tests.generic;

import java.util.List;

public class DataSetCompany {
	public static final String PATH_COMPANY = "cwms/generic/company/"; 

	public static final String FILE_COMPANY_BUSINESS = PATH_COMPANY + "CompanyBusiness.xml";
	public static final String FILE_COMPANY_CLIENT = PATH_COMPANY + "CompanyClient.xml";
	public static final String FILE_COMPANY_SUPPLIER = PATH_COMPANY + "CompanySupplier.xml";
	public static final String FILE_INSTRUCTION_CLIENT = PATH_COMPANY + "InstructionClient.xml";
	public static final String FILE_SYSTEM_DEFAULT_APPLICATION = PATH_COMPANY + "SystemDefaultApplication.xml";

	public static List<String> getCompanyClientTestData() {
		List<String> result = DataSetCore.getAllBaseData();
		result.add(DataSetCompany.FILE_COMPANY_BUSINESS);
		result.add(DataSetCompany.FILE_COMPANY_CLIENT);
		return result;
	}
	
	public static List<String> getCompanyBusinessTestData() {
		List<String> result = DataSetCore.getAllBaseData();
		result.add(DataSetCompany.FILE_COMPANY_BUSINESS);
		return result;
	}	
}
