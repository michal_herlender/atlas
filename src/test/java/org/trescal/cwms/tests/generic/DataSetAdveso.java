package org.trescal.cwms.tests.generic;

public class DataSetAdveso {
	public static final String PATH_ADVESO = "cwms/generic/adveso/"; 

	public static final String FILE_ADVESO_SYSTEM_DEFAULT = PATH_ADVESO + "SystemDefaultApplicationAdveso.xml";
	public static final String FILE_ADVESO_JOB_ITEM_ACTIVITY_QUEUE = PATH_ADVESO + "AdvesoJobItemActivityQueue.xml";
}
