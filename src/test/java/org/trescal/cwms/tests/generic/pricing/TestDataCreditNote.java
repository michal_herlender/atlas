package org.trescal.cwms.tests.generic.pricing;

public class TestDataCreditNote {
	public static final Integer RECORD_COUNT_STANDALONE = 5;

	// Credit note numbers
	public static final String CREDIT_NOTE_NUMBER_FR_110 = "FR005-CN-19000110";
	public static final String CREDIT_NOTE_NUMBER_US_150 = "USTUS-CN-19000150";

	// Credit note IDs
	public static final Integer CREDIT_NOTE_ID_FR_110 = 110;
	public static final Integer CREDIT_NOTE_ID_DE_120 = 120;
	public static final Integer CREDIT_NOTE_ID_US_150 = 150;
	
	// Other data
	public static final Boolean ISSUED_FR_110 = true;
	public static final Integer ORG_ID_FR_110 = 510;	
}
