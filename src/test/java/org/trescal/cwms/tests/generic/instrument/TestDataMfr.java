package org.trescal.cwms.tests.generic.instrument;

public class TestDataMfr {

	// Quantity of records defined in test data set
	public static final Integer RECORD_COUNT = 3;
	
	// Define IDs here for reference in test classes (adding when put into use in Java tests)

	public static final Integer ID_GENERIC = 1;
	public static final Integer ID_FLUKE = 3558;
	public static final Integer ID_MITUTOYO = 6427;
	
	public static final String NAME_FLUKE = "FLUKE";
	public static final Boolean ACTIVE_FLUKE = true;
	public static final Boolean GENERIC_FLUKE = false;
	public static final Integer TML_ID_FLUKE = 3615;
	
	public static final String MFR_NAME = "MITUTOYO";
	public static final Boolean ACTIVE_GENERIC = true;
	public static final Boolean GENERIC_GENERIC = false;
	public static final Integer TML_ID_GENERIC = 1;
}
