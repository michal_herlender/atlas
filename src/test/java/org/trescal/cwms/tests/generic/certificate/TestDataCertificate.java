package org.trescal.cwms.tests.generic.certificate;

public class TestDataCertificate {
	public final static Integer CERT_ID = 1502;
	public final static String CERT_NO = "US-006-CI-16001502";
	public final static String CAL_DATE = "2016-01-03";
	public final static String CERT_DATE = "2016-01-03T00:00:00.000"; 
	public final static String CERT_TYPE = "CERT_CLIENT";
	
	public final static Integer RECORD_COUNT_CERTIFICATE = 2;
	public final static Integer RECORD_COUNT_INST_CERT_LINK = 1;
	public final static Integer RECORD_COUNT_CERT_LINK = 1;
}
