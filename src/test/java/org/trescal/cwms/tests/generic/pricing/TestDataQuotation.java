package org.trescal.cwms.tests.generic.pricing;

import java.time.LocalDate;

public class TestDataQuotation {

    public static final int ID_QUOTATIONS_RESULT = 130;
    public static final int QUOTATIONS_SIZE_FOR_GROUP_1 = 1;
    public static final int ISSUED_QUOTATION_SIZE = 1;
    public static final int RESULT_SIZE = 1;
    public static final int QUOTATIONS_RESULT_SIZE = 3;
    public static final int ID_ACTIVE_FRANCE = 110;
    public static final LocalDate DATE_ACTIVE_FRANCE_ISSUEDATE = LocalDate.of(2019, 9, 1);
    public static final String DATE_ACTIVE_FRANCE_EXPIRYDATE = "2039-10-01";
    public static final String DATE_ISSUEDATE = "2019-10-01";
    public static final String QNO1_FRANCE = "FR005-QN-19000110";
    public static final String QNO3_FRANCE = "FR005-QN-19000130";
    public static final String CLIENT_REF_3 = "CRQUO1230";
    public static final int FINAL_COST = 190;
}
