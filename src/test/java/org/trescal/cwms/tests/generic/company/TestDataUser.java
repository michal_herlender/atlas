package org.trescal.cwms.tests.generic.company;

public class TestDataUser {
	public static final String USER_NAME_RUNGIS = "rungis_user";
	public static final String USER_NAME_DARMSTADT = "darmstadt_user";
	public static final String USER_NAME_CASABLANCA = "casablanca_user";
	public static final String USER_NAME_ZARAGOZA = "zaragoza_user";
	public static final String USER_NAME_HARTLAND = "hartland_user";
}
