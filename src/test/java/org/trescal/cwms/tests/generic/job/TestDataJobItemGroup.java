package org.trescal.cwms.tests.generic.job;

public class TestDataJobItemGroup {
	
	public final static Integer RECORD_COUNT_COMPLETED = 5;
	
	
	//jobid value in table
	public final static Integer ID_CLIENT_1 = 110;
	public final static Integer ID_CLIENT_2 = 120;
	public final static Integer ID_CLIENT_3 = 130;
	public final static Integer ID_CLIENT_4 = 140;
	public final static Integer ID_CLIENT_5 = 150;
	
	public final static Integer ID_1 = 110;
	public final static boolean CALIBRATION_1 = true;
	public final static boolean DELIVERY_1 = true;
	public final static Integer JOB_ID_1 = 110;
	

}
