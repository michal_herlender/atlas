package org.trescal.cwms.tests.generic.pricing;

public class TestDataExpenseItem {
	public static final Integer ITEM_ID_US_1503 = 1503;
	public static final Integer ITEM_NUMBER_US_1503 = 1;
	public static final Integer JOB_ID_US_1503 = 150;
	public static final Integer MODEL_ID_US_1503 = 43571;
	public static final String CLIENT_REF_US_1503 = "Client Ref 1503";
}
