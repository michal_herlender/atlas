package org.trescal.cwms.tests.generic;

import java.util.ArrayList;
import java.util.List;

/**
 * Defines a set of generic test data XML files that can be re-used throughout Atlas for integration tests.  
 * 
 * Created 2019-09-16
 */
public class DataSetJob {
	public static final String PATH_JOB = "cwms/generic/job/"; 
		
	public static final String FILE_JOB_CLIENT_COMPLETED = PATH_JOB + "JobClientCompleted.xml";
	public static final String FILE_JOB_CLIENT_REPAIR = PATH_JOB + "JobClientRepair.xml";
	public static final String FILE_JOB_CLIENT_AWAITING_CALIBRATION = PATH_JOB + "JobClientAwaitingCalibration.xml";
	public static final String FILE_JOB_CLIENT_PRE_CALIBRATION = PATH_JOB + "JobClientPreCalibration.xml";
	public static final String FILE_JOB_CLIENT_EMPTY = PATH_JOB + "JobClientEmpty.xml";
	public static final String FILE_JOB_CLIENT_COMPLETED_ACTIONS = PATH_JOB + "JobClientCompletedActions.xml";
	
	public static final String FILE_CAL_REQ = PATH_JOB + "CalReq.xml";
	public static final String FILE_CONTRACT_REVIEW = PATH_JOB + "ContractReview.xml";
	public static final String FILE_UPCOMING_WORK = PATH_JOB + "UpcomingWork.xml";
	
	
	public static final String FILE_ON_BEHALF_ITEM = PATH_JOB + "OnBehalfItem.xml";
	
	public static List<String> getJobClientEmpty() {
		List<String> result = DataSetInstrument.getInstrumentClientTestData();
		result.add(DataSetCore.FILE_BASE_ITEM_STATE);
		result.add(FILE_JOB_CLIENT_EMPTY);
		return result;
	}
	
	public static List<String> getJobClientCompletedTestData() {
		List<String> result = DataSetInstrument.getInstrumentClientTestData();
		result.add(DataSetCore.FILE_BASE_ITEM_STATE);
		result.add(FILE_JOB_CLIENT_COMPLETED);
		return result;
	}
	
	public static List<String> getJobClientRepairTestData() {
		List<String> result = getJobClientCompletedTestData();
		result.add(FILE_JOB_CLIENT_REPAIR);
		return result;
	}
	
	/**
	 * Used in multiple methods below
	 */
	private static List<String> getCalibrationTestData() {
		List<String> result = DataSetInstrument.getInstrumentClientTestData();
		result.add(DataSetInstrument.FILE_INSTRUMENT_CLIENT_ADDITIONAL);
		result.add(DataSetCore.FILE_BASE_ITEM_STATE);
		result.add(DataSetCore.FILE_BASE_CALIBRATION);
		result.add(DataSetCapability.FILE_PROCEDURE);
		result.add(DataSetCapability.FILE_PROCEDURE_AUTHORIZATION);
		return result;
	}
	
	/**
	 * Contains all data required to start a calibration
	 * @return
	 */
	public static List<String> getJobClientAwaitingCalibrationTestData() {
		List<String> result = getCalibrationTestData();
		result.add(FILE_JOB_CLIENT_AWAITING_CALIBRATION);
		return result;
	}
	
	/**
	 * Contains all data required to complete a calibration
	 * @return
	 */
	public static List<String> getJobClientPreCalibrationTestData() {
		List<String> result = getCalibrationTestData();
		result.add(DataSetBaseStatus.FILE_STATUS_CALIBRATION);
		result.add(FILE_JOB_CLIENT_PRE_CALIBRATION);
		return result;
	}
	
	/**
	 * Contains all data required after calibration, e.g. for supplementary cert tests
	 * @return
	 */
	public static List<String> getJobClientPostCalibrationTestData() {
		List<String> result = getCalibrationTestData();
		result.add(DataSetBaseStatus.FILE_STATUS_CALIBRATION);
		result.add(FILE_JOB_CLIENT_COMPLETED);
		result.add(DataSetCertificate.FILE_CLIENT_CERTIFICATE);
		return result;
	}
	
	/**
	 * This has a list of onbehalf items which are disconnected from all other generic test data...
	 * @return
	 */
	public static List<String> getOnBehalfItemTestData() {
		List<String> result = new ArrayList<String>();
		result.add(FILE_ON_BEHALF_ITEM);
		return result;
	}
	
}