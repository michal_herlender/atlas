package org.trescal.cwms.tests.generic.company;

public class TestDataSubdivSettings {
	public static final Integer RECORD_COUNT_CLIENT = 5;
	
	// IDs are assigned by orgid then coid
	public static final Integer ID_510_FR_FOR_110_FR = 510110;
	public static final Integer ID_520_DE_FOR_120_DE = 520120;
	public static final Integer ID_530_MA_FOR_130_MA = 530130;
	public static final Integer ID_540_ES_FOR_140_ES = 540140;
	public static final Integer ID_550_US_FOR_150_US = 550150;
	
}
