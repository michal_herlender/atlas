package org.trescal.cwms.tests.generic.company;

public class TestDataContact {
	// Define IDs here for reference in test classes (adding when put Integero use in Java tests)
	
	public static final Integer ID_BUSINESS_FRANCE = 510;
	public static final Integer ID_BUSINESS_GERMANY = 520;
	public static final Integer ID_BUSINESS_MOROCCO = 530;
	public static final Integer ID_BUSINESS_SPAIN = 540;
	public static final Integer ID_BUSINESS_USA = 550;
	
	public static final Integer ID_CLIENT_FRANCE = 110;
	public static final Integer ID_CLIENT_GERMANY = 120;
	public static final Integer ID_CLIENT_MOROCCO = 130;
	public static final Integer ID_CLIENT_SPAIN = 140;
	public static final Integer ID_CLIENT_USA = 150;
	
	public static final Integer RECORD_COUNT_BUSINESS = 5;
	public static final Integer RECORD_COUNT_CLIENT = 6;	// Two clients for French client company
	public static final Integer RECORD_COUNT_TOTAL = RECORD_COUNT_BUSINESS + RECORD_COUNT_CLIENT;
	
	public static final String FIRST_NAME_CLIENT_FRANCE = "Client";
	public static final String LAST_NAME_CLIENT_FRANCE = "FR Contact";
	
	public static final String FIRST_NAME_CLIENT_FRANCE_DUPLICATE = "Client";
	public static final String LAST_NAME_CLIENT_FRANCE_DUPLICATE = "FR Contact";
	
	public static final String FIRST_NAME_BUSINESS_FRANCE = "Rungis";
	public static final String LAST_NAME_BUSINESS_FRANCE = "Default Contact";
	public static final String EMAIL_BUSINESS_FRANCE = "rungis_contact@trescal.com"; 
	public static final String TELEPHONE_BUSINESS_FRANCE = "+33 (0)2 34 56 78 90";
	public static final String FAX_BUSINESS_FRANCE = "+33 (0)8 76 54 32 10";
	public static final String MOBILE_BUSINESS_FRANCE = "+33 (0)4 32 10 98 76";
	
	public static final String FIRST_NAME_BUSINESS_MOROCCO = "Casablanca";
	public static final String LAST_NAME_BUSINESS_MOROCCO = "Default Contact";
	public static final String EMAIL_BUSINESS_MOROCCO = "casablanca_contact@trescal.com"; 
	public static final String TELEPHONE_BUSINESS_MOROCCO = "+212(0)2 3456 7890";
	public static final String FAX_BUSINESS_MOROCCO = "+212(0)8 7654 3210";
	public static final String MOBILE_BUSINESS_MOROCCO = "212(0)7 33 24 98 95";
		
	public static final String FIRST_NAME_CLIENT_GERMANY = "Client";
	public static final String LAST_NAME_CLIENT_GERMANY = "DE Contact";
}
