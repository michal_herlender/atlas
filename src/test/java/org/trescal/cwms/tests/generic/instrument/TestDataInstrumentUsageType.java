package org.trescal.cwms.tests.generic.instrument;

public class TestDataInstrumentUsageType {
	
	public static final Integer RECORD_COUNT_STANDALONE = 4;	// In standalone file for export test
	public static final Integer RECORD_COUNT_INSTRUMENT = 1;	// In InstrumentClient.xml
	
	public static final Integer TYPE_ID_1 = 1;
	public static final Integer TYPE_ID_2 = 2;
	public static final Integer TYPE_ID_3 = 3;
	
	public static final String FULL_DESC= "test_1";
	public static final boolean DEFAULT_TYPE=false; 
	public static final String	NAME="name_test_1";
	public static final boolean recall=false;
	

}
