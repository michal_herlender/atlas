package org.trescal.cwms.tests.generic.instrument;

public class TestDataProcs {
    public static final Integer PROCS_ID_5101 = 5101;
    public static final Integer PROCS_ID_5301 = 5301;

    public static final int PROCS_ADDITIONAL_US_MISC_5502 = 5502;
    public static final int PROCS_ADDITIONAL_US_DMM_5503 = 5503;
    public static final int PROCS_ADDITIONAL_US_CALIPER_5504 = 5504;

    public static final String REFERENCE_5101 = "FR-005-RUN-5101";
    public static final String REFERENCE_5301 = "MA-030-CAS-5301";
    public static final String NAME_5101 = "Generic - France - Rungis";
    public static final String NAME_5301 = "Generic - Morocco - Casablanca";
    public static final Integer CAL_PROCESS_ID_5101 = 1;

}
