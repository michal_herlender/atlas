package org.trescal.cwms.tests.generic;

public class DataSetExchangeFormat {
	public static final String PATH_EXCHANGE_FORMAT = "cwms/generic/exchangeformat/"; 

	public static final String FILE_EXTERNAL_SYSTEM = PATH_EXCHANGE_FORMAT + "ExchangeFormatExternalSystem.xml";
	
	public static final String FILE_QUOTATION_ITEMS_IMPORTATION = PATH_EXCHANGE_FORMAT + "ExchangeFormatQuotationItemsImportation.xml";
	
}
