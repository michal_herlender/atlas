package org.trescal.cwms.tests.generic.core;

import java.math.BigDecimal;

public class TestDataVatRate {
	
	// Count of all records in test data set 
	
	public final static int SIZE_DATA_SET = 12;
	
	// Define IDs here for reference in test classes (adding when put into use in Java tests)
	
	public final static String ID_SPECIAL_CANARIAS = "0";
	public final static String ID_DEFAULT_SPAIN = "1";
	public final static String ID_EUROZONE_INTERNAL = "2";
	public final static String ID_EUROZONE_EXPORT = "3";
	public final static String ID_NON_EUROZONE = "4";
	public final static String ID_DEFAULT_MOROCCO = "5";
	public final static String ID_SPECIAL_MOROCCO_FREE = "8";

	public final static BigDecimal RATE_SPECIAL_CANARIAS = new BigDecimal("0.00");
	public final static BigDecimal RATE_DEFAULT_SPAIN = new BigDecimal("21.00");
	public final static BigDecimal RATE_DEFAULT_MOROCCO = new BigDecimal("20.00");
	public final static BigDecimal RATE_SPECIAL_MOROCCO_FREE = new BigDecimal("0.00");
}