package org.trescal.cwms.tests.generic.company;

public class TestDataDepartmentMember {
	// Define IDs here for reference in test classes (adding when put Integero use in Java tests)
	
	public static final Integer ID_BUSINESS_FRANCE = 510;
	public static final Integer ID_BUSINESS_GERMANY = 520;
	public static final Integer ID_BUSINESS_MOROCCO = 530;
	public static final Integer ID_BUSINESS_SPAIN = 540;
	public static final Integer ID_BUSINESS_USA = 550;
	
	public static final Integer RECORD_COUNT_BUSINESS = 5;
}
