package org.trescal.cwms.tests.generic;

import java.util.ArrayList;
import java.util.List;

public class DataSetCore {

	public static final String PATH_BASE = "cwms/generic/core/"; 

	public static final String FILE_BASE_CALIBRATION = PATH_BASE + "CoreCalibration.xml";
	public static final String FILE_BASE_FINANCE = PATH_BASE + "CoreFinance.xml";
	public static final String FILE_BASE_SERVICE_TYPE = PATH_BASE + "CoreServiceType.xml";
	public static final String FILE_BASE_SERVICE_TYPE_TRANSLATIONS = PATH_BASE + "CoreServiceTypeTranslations.xml";
	public static final String FILE_BASE_JOB_STATUS = PATH_BASE + "CoreJobStatus.xml";
	public static final String FILE_BASE_ITEM_STATE = PATH_BASE + "CoreItemState.xml";
	public static final String FILE_BASE_SYSTEM = PATH_BASE + "CoreSystem.xml";

	public static List<String> getAllBaseData() {
		List<String> result = new ArrayList<String>();

		// Service type translations and item states (due to variations in states used) are intentionally not included here
		result.add(FILE_BASE_FINANCE);
		result.add(FILE_BASE_SERVICE_TYPE);
		result.add(FILE_BASE_JOB_STATUS);
		result.add(FILE_BASE_SYSTEM);
		return result;
	}

}
