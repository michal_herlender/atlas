package org.trescal.cwms.tests.generic;

import java.util.List;

/**
 * Definitions and convenience methods for getting test data for pricing related entities
 */
public class DataSetPricing {
	public static final String PATH = "cwms/generic/pricing/"; 
	
	public static final String FILE_CATALOG_PRICE = PATH + "CatalogPrice.xml";
	public static final String FILE_CREDIT_NOTE = PATH + "CreditNote.xml";
	public static final String FILE_SUPPLIER_QUOTATION_REQUEST = PATH + "TPQuoteRequest.xml";
	public static final String FILE_SUPPLIER_QUOTATION = PATH + "TPQuotation.xml";
	public static final String FILE_INVOICE = PATH + "Invoice.xml";
	public static final String FILE_INVOICE_PERIODIC = PATH + "InvoicePeriodic.xml";
	public static final String FILE_QUOTATION = PATH + "Quotation.xml";
	public static final String FILE_SUPPLIER_PO = PATH + "SupplierPO.xml";
	public static final String FILE_QUOTATION_ADDITIONAL = PATH + "QuotationAdditional.xml";
	public static final String FILE_JOB_COSTING = PATH + "JobCosting.xml";
	
	public static List<String> getCreditNoteTestData() {
		List<String> result = getInvoiceTestData();
		result.add(FILE_CREDIT_NOTE);
		return result;
	}
	
	public static List<String> getTPQuoteRequestTestData() {
		List<String> result = DataSetJob.getJobClientCompletedTestData(); 
		result.add(DataSetCompany.FILE_COMPANY_SUPPLIER);
		result.add(FILE_SUPPLIER_QUOTATION_REQUEST);
		result.add(DataSetBaseStatus.FILE_STATUS_TP_QUOTE_REQUEST);
//		result.add(DataSetBaseStatus.FILE_STATUS_QUOTATION);
//		result.add(FILE_QUOTATION);		// Confirm unnecessary and delete
		return result;
	}
	
	public static List<String> getTPQuotationTestData() {
		// Contains reference to TP quote request so both included
		
		List<String> result = getTPQuoteRequestTestData();
		result.add(FILE_SUPPLIER_QUOTATION);
		result.add(DataSetBaseStatus.FILE_STATUS_TP_QUOTATION);
//		result.add(DataSetBaseStatus.FILE_STATUS_QUOTATION);
//		result.add(FILE_QUOTATION);		// Confirm unnecessary and delete
		return result;
	}
	
	public static List<String> getInvoiceTestData() {
		List<String> result = DataSetJob.getJobClientCompletedTestData();
		result.add(DataSetDelivery.FILE_JOB_DELIVERY_CLIENT);
		result.add(DataSetDelivery.FILE_JOB_DELIVERY_ITEMS);
		result.add(DataSetBaseStatus.FILE_STATUS_INVOICE);
		result.add(FILE_INVOICE);
		return result;
	}

	public static List<String> getInvoicePeriodicTestData() {
		List<String> result = DataSetJob.getJobClientCompletedTestData();
		result.add(FILE_INVOICE_PERIODIC);
		return result;
	}
	
	public static List<String> getJobCostingTestData() {
		List<String> result = DataSetJob.getJobClientCompletedTestData();
		result.add(DataSetBaseStatus.FILE_STATUS_JOB_COSTING);
		result.add(FILE_JOB_COSTING);
		return result;
	}

	public static List<String> getQuotationTestData() {
		List<String> result = DataSetInstrument.getInstrumentClientTestData();
		result.add(DataSetBaseStatus.FILE_STATUS_QUOTATION);
		result.add(FILE_QUOTATION);
		return result;
	}
	
	public static List<String> getSupplierPOTestData() {
		List<String> result = DataSetJob.getJobClientCompletedTestData();
		result.add(DataSetCompany.FILE_COMPANY_SUPPLIER);
		result.add(DataSetBaseStatus.FILE_STATUS_SUPPLIER_PO);
		result.add(FILE_SUPPLIER_PO);
		return result;
	}
	
}
