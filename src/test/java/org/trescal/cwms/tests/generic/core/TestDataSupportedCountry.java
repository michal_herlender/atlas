package org.trescal.cwms.tests.generic.core;

public class TestDataSupportedCountry {
	
	// Quantity of records defined in test data set
	public static final Integer RECORD_COUNT = 5;
	
	// Define IDs here for reference in test classes (adding when put Integero use in Java tests)
	
	public static final Integer ID_FRANCE = 1;
	public static final Integer ID_GERMANY = 2;
	public static final Integer ID_MOROCCO = 3;
	public static final Integer ID_SPAIN = 4;
	public static final Integer ID_USA = 5;
	
	public static final String COUNTRY_FRANCE = "France";
	public static final String COUNTRY_CODE_FRANCE = "FR";
	
	public static final String COUNTRY_USA = "United States";
	public static final String COUNTRY_CODE_USA = "US";
}
