package org.trescal.cwms.tests.generic.instrument;

public class TestDataCalReq {
	public static Integer RECORD_COUNT = 5;
	
	public static Integer ID_1 = 1;
	public static Integer ID_2 = 2;
	
	public static String TYPE_1 = "instrument";
	public static boolean ACTIVE_1 = true;
	public static String PUBLIC_TEXT_1 = "Public text 1";
	public static String PRIVATE_TEXT_1 = "Private text 1"; 
	public static Integer PLANT_ID_1 = 1101;
	
	public static String TYPE_2 = "jobitem";
	public static boolean ACTIVE_2 = true;
	public static String PUBLIC_TEXT_2 = "Public text 2";
	public static String PRIVATE_TEXT_2 = "Private text 2"; 
	public static Integer JOBITEM_ID_2 = 1101;
}
