package org.trescal.cwms.tests.generic.job;


public class TestDataJobItem {
	public final static Integer RECORD_COUNT_COMPLETED = 10;
	
	public final static Integer ID_CLIENT_FR_1 = 1101;
	public final static Integer ID_CLIENT_FR_2 = 1102;
	public final static Integer ID_CLIENT_USA = 1501;
	
	
	public final static Integer JOB_ID_CLIENT_FR = 110;
	public final static String CLIENT_REF_FR_1 = "JICR1101";
	public final static Integer ITEM_NO_FR_1 = 1;
	public final static Integer PLANT_ID_FR_1 = 1101;
    public final static Integer STATE_ID_FR_1 = 54;
    public final static Integer CAPABILITY_ID_FR_1 = null;
    public final static Integer ONBEHALF_COMPANY_ID_FR_1 = 120;
    public final static String CONTRACT_REVIEWBY_ID_FR_1 = null;
    public final static Integer TURN_FR_1 = 7;
    public final static String DEFAULT_PAGE_FR_1 = null;
    public final static String PROC_RFFERNCE_FR_1 = null;

    // We use the format as serialized in RESTful JSON (see DateTools.rest_dtf),
    // as many integration tests are for REST web services
    public final static String DATE_IN_TEXT_FR_1 = "2019-10-17T00:00:00.000";
    public final static String DATE_IN_TEXT_FR_2 = "2019-10-17T00:00:00";
    public final static String DATE_COMPLETE_TEXT_FR_1 = "2019-10-23T00:00:00.000";
    public final static String DUE_DATE_TEXT_FR_1 = "2019-10-24";
    public final static String CLIENT_RECEIPT_DATE_TEXT_FR_1 = "2019-10-25T00:00:00.000";
}
