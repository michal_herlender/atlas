package org.trescal.cwms.tests.generic.company;

public class TestDataCompanySettings {
	// Test data for CompanySettingsForAllocatedCompany entity
	
	// 5 settings - 1 per per business company with self, then 2 bidirectional between 510 (France) and 540 (Spain) 
	public static final Integer RECORD_COUNT_BUSINESS = 7;
	// Additional 5 settings - 1 per client company
	public static final Integer RECORD_COUNT_CLIENT = 5;
	public static final Integer RECORD_COUNT_TOTAL = RECORD_COUNT_CLIENT + RECORD_COUNT_BUSINESS;
	
	// IDs are assigned by orgid then coid
	public static final Integer ID_BUSINESS_FRANCE_FOR_SELF = 510510;
	public static final Integer ID_BUSINESS_FRANCE_FOR_SPAIN = 510540;
	public static final Integer ID_BUSINESS_GERMANY_FOR_SELF = 520520;
	public static final Integer ID_BUSINESS_MOROCCO_FOR_SELF = 530530;
	public static final Integer ID_BUSINESS_SPAIN_FOR_SELF = 540540;
	public static final Integer ID_BUSINESS_SPAIN_FOR_FRANCE = 540510;
	public static final Integer ID_BUSINESS_USA_FOR_SELF = 550550;
	
}
