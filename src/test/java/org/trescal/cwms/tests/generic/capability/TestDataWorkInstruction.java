package org.trescal.cwms.tests.generic.capability;

public class TestDataWorkInstruction {
	public static final int RECORD_COUNT = 5;
	
	public static final int ID_110_FR_CALIPER = 110;
	public static final int ID_120_DE_CALIPER = 120;
	public static final int ID_130_MO_CALIPER = 130;
	public static final int ID_140_ES_CALIPER = 140;
	public static final int ID_150_US_CALIPER = 150;
	
	public static final String NAME_150_US_CALIPER = "US Digital Caliper 0-12 inch";
	public static final String TITLE_150_US_CALIPER = "CD-12in-US";
}
