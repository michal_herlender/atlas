package org.trescal.cwms.tests.generic.company;

public class TestDataCompany {

	// Define IDs here for reference in test classes (adding when put into use in Java tests)
	
	public static final Integer ID_BUSINESS_FRANCE = 510;
	public static final Integer ID_BUSINESS_GERMANY = 520;
	public static final Integer ID_BUSINESS_MOROCCO = 530;
	public static final Integer ID_BUSINESS_SPAIN = 540;
	public static final Integer ID_BUSINESS_USA = 550;
	
	public static final Integer ID_CLIENT_FRANCE = 110;
	public static final Integer ID_CLIENT_GERMANY = 120;
	public static final Integer ID_CLIENT_MOROCCO = 130;
	public static final Integer ID_CLIENT_SPAIN = 140;
	public static final Integer ID_CLIENT_USA = 150;
	
	public static final Integer ID_SUPPLIER_FRANCE = 310;
	public final static String FISCAL_ID_SUPPLIER_FRANCE = "FR310456789";
	
	public static final int RECORD_COUNT_BUSINESS = 5;
	public static final int RECORD_COUNT_TOTAL = 10;	// Business and client

	public static final Boolean ACTIVE_BUSINESS_USA = null;
	public static final Boolean ONSTOP_BUSINESS_USA = null;
	public final static String COMPANY_CODE_BUSINESS_USA = "TUS";
	public final static String COMPANY_NAME_BUSINESS_USA = "TRESCAL INC.";
	public final static String LEGAL_ID_BUSINESS_USA = "40437";
	public final static String FISCAL_ID_BUSINESS_USA = "38-2716012";
	
	public final static String COMPANY_NAME_CLIENT_FRANCE = "Test Client Company - France";
	public final static String FISCAL_ID_CLIENT_FRANCE = "FR110456789";
	public final static String LEGAL_ID_CLIENT_FRANCE = "110456789";
	
	public final static String COMPANY_NAME_BUSINESS_FRANCE = "TRESCAL SAS";
	public final static String FISCAL_ID_BUSINESS_FRANCE = "FR562047050";

	public final static String COMPANY_NAME_CLIENT_USA = "Test Client Company - USA";
	public final static String FISCAL_ID_CLIENT_USA = "US150456789";

}
