package org.trescal.cwms.tests.generic.instrument;

public class TestDataDescription {

	// Quantity of records defined in test data set
    public static final Integer RECORD_COUNT = 5;

    // IDs matching actual production data
    public static final Integer ID_EQUIPMENT_MISC = 43;
    public static final Integer ID_DIGITAL_MULTIMETER = 207;
	public static final Integer ID_CALIPER = 460;
	public static final Integer ID_SERVICES = 658;
	public static final Integer ID_SERVICES_PACKAGING = 750;
	
	public static final Integer TML_ID_DIGITAL_MULTIMETER = 231;
	public static final String DESCRIPTION_DIGITAL_MULTIMETER = "DIGITAL MULTIMETER";
	public static final Boolean ACTIVE_DIGITAL_MULTIMETER = true;
	public static final Integer FAMILY_ID_DIGITAL_MULTIMETER = 53;
	
}
