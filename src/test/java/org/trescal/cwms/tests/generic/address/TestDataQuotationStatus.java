package org.trescal.cwms.tests.generic.address;

public class TestDataQuotationStatus {
	// IDs of QUOTATION Status 
	public static final Integer QUOTATION_STATUS_REQUESTED_ID = 1;
	public static final Integer QUOTATION_STATUS_ACCEPTED_ID = 2;
	public static final Integer QUOTATION_STATUS_AWAITING_INFORMATION_ID = 3;
	public static final Integer QUOTATION_STATUS_LOST_ID = 4;
	public static final Integer QUOTATION_STATUS_AWAITING_INFORMATION_FORM_CUSTOMER_ID = 5;
	public static final Integer QUOTATION_STATUS_AWAITING_THIRD_PARTY_QUOTATION_ID = 6;
	public static final Integer QUOTATION_STATUS_QUOTE_ISSUED_TO_CLIENT_ID = 7;
	
	public static final String  QUOTATION_STATUS_REQUESTED_NAME_FR = "Demandé";

}
