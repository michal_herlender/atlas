package org.trescal.cwms.tests.generic.core;

import org.trescal.cwms.core.instrumentmodel.DomainType;

public class TestDataServiceType {

	// Quantity of records defined in test data set
	public static final Integer RECORD_COUNT = 5;

	// Define IDs here for reference in test classes (adding when used in Java tests)

	public static final Integer ID_AC_M_IL = 1;	// AC-M-IL = Accredited calibration with measurement results in laboratory 
	public static final Integer ID_TC_M_IL = 2;	// TC-M-IL = Traceable calibration with measurement results in laboratory
	public static final Integer ID_REPAIR = 6;
	public static final Integer ID_TC_MC_OS = 18;
	public static final Integer ID_SERVICE = 31;

	public static final String LONG_NAME_AC_M_IL = "Accredited calibration with measurement results in laboratory"; 
	public static final String SHORT_NAME_AC_M_IL = "AC-M-IL";
	public static final boolean CLIENT_AC_M_IL = false;
	public static final Integer ORDER_BY_AC_M_IL = 6;
	public static final boolean REPAIR_AC_M_IL = false;
	public static final DomainType DOMAIN_TYPE_AC_M_IL = DomainType.INSTRUMENTMODEL;
}
