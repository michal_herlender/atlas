package org.trescal.cwms.tests.generic;

import java.util.List;

public class DataSetBaseStatus {
	public static final String PATH_BASE_STATUS = "cwms/generic/basestatus/"; 

	public static final String FILE_STATUS_CALIBRATION = PATH_BASE_STATUS + "CalibrationStatus.xml";
	public static final String FILE_STATUS_INVOICE = PATH_BASE_STATUS + "InvoiceStatus.xml";
	public static final String FILE_STATUS_QUOTATION = PATH_BASE_STATUS + "BaseStatusQuotation.xml";
	public static final String FILE_STATUS_TP_QUOTATION = PATH_BASE_STATUS + "BaseStatusTPQuotation.xml";
	public static final String FILE_STATUS_TP_QUOTE_REQUEST = PATH_BASE_STATUS + "BaseStatusTPQuoteRequest.xml";
	public static final String FILE_STATUS_SUPPLIER_PO = PATH_BASE_STATUS + "PurchaseOrderStatus.xml";
	public static final String FILE_STATUS_JOB_COSTING = PATH_BASE_STATUS + "BaseStatusJobCosting.xml";
	
	public static List<String> getQuoationStatusTestData() {
		List<String> result = DataSetCore.getAllBaseData();
		result.add(DataSetBaseStatus.FILE_STATUS_QUOTATION);
		return result;
	}
}

