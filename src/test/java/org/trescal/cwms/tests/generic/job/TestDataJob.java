package org.trescal.cwms.tests.generic.job;

public class TestDataJob {
	public final static Integer RECORD_COUNT_COMPLETED = 5;
	
	public final static Integer ID_CLIENT_FR = 110;
	public final static Integer ID_CLIENT_DE = 120;
	public final static Integer ID_CLIENT_ES = 130;
	public final static Integer ID_CLIENT_MA = 140;
	public final static Integer ID_CLIENT_US = 150;
	public final static Integer ID_CLIENT_US_ADDITIONAL_151 = 151;
	
	public final static String JOB_NO_CLIENT_FR = "FR005-RUN-JI-19000110";
	public final static String CLIENT_REF_CLIENT_FR = "CRJ110";
	
	public final static String REG_DATE_TEXT_CLIENT_FR = "2019-10-16";
	public final static String DATE_COMPLETE_TEXT_CLIENT_FR = "2019-10-23";

}
