package org.trescal.cwms.tests.generic.pricing;

import java.time.LocalDate;

public class TestDataInvoice {
	public static final Integer RECORD_COUNT_STANDALONE = 7;

	// Invoice numbers
	public static final String INVOICE_NUMBER_FR_110 = "FR005-IN-19000110";
	public static final String INVOICE_NUMBER_FR_111 = "FR005-IN-19000111";
	public static final String INVOICE_NUMBER_US_150 = "USTUS-IN-19000150";
	
	// Invoice IDs
	public static final Integer INVOICE_ID_FR_110 = 110;
	public static final Integer INVOICE_ID_FR_111_PERIODIC = 111;
	public static final Integer INVOICE_ID_US_150 = 150;
	
	// Other data
	public static final LocalDate INVOICE_DATE_FR_110 = LocalDate.of(2019, 9, 8);	// 2019-09-08
	public static final Boolean ISSUED_FR_110 = true;
	public static final Integer ADDRESS_ID_FR_110 = 110;
	public static final Integer COMPANY_ID_FR_110 = 110;
	public static final Integer ORG_ID_FR_110 = 510;
}
