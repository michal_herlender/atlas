package org.trescal.cwms.tests.generic.job;

public class TestDataEngineerAllocation {
	public final static Integer RECORD_COUNT_COMPLETED = 5;
	
	public final static Integer ID_CLIENT_FRANCE_1101 = 1101;
	public final static Integer ID_CLIENT_GERMANY_1201 = 1201;
	public final static Integer ID_CLIENT_SPAIN_1301 = 1301;
	public final static Integer ID_CLIENT_MOROCCO_1401 = 1401;
	public final static Integer ID_CLIENT_USA_1501 = 1501;
	
	public final static Boolean ACTIVE_CLIENT_FRANCE_1101 = false;
	public final static String ALLOCATED_FOR_TEXT_FRANCE_1101 = "2019-10-19";
	public final static String ALLOCATED_ON_TEXT_FRANCE_1101 = "2019-10-18";
	public final static String ALLOCATED_UNTIL_TEXT_FRANCE_1101 = "2019-10-20";
}