package org.trescal.cwms.tests;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.GenericXmlWebContextLoader;


/*
 * Base MVC test class for cwms-servlet configuration
 */
@ContextConfiguration(loader = GenericXmlWebContextLoader.class,
        locations = {
                "classpath:applicationContext-dao.xml",
                "classpath:applicationContext-service.xml",
                "classpath:cwms-servlet.xml",
                "classpath:mocks.xml",
                "classpath:mocks-cwms.xml",
                "classpath:applicationContext-mvc-test.xml",
                "classpath:applicationContext-schedule.xml"})
//@RunWith(SpringJUnit4ClassRunner.class)
@ExtendWith(SpringExtension.class)
@Tag("org.trescal.cwms.core.AtlasIntegrationRestTest")
public abstract class MvcCwmsTestClass extends MvcBaseTestClass {
}