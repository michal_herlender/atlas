package org.trescal.cwms.tests;

import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mssql.InsertIdentityOperation;
import org.dbunit.operation.DatabaseOperation;
import org.junit.AfterClass;
import org.junit.AssumptionViolatedException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.rules.Stopwatch;
import org.junit.runner.Description;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.trescal.cwms.core.AtlasIntegrationTest;

import net.sf.ehcache.CacheManager;

/*
 * This is a base test class for use with the Spring Test framework that is decoupled from
 * Unitils.  After trying several approaches, it seems that Spring Test requires use of 
 * SpringJunit4ClassRunner to recognize the annotations to create a web app context, whereas
 * Unitils requires UnitilsJUnit4TestClassRunner.
 * This is a DBUnit aware version that loads XML data for integration testing, at the present
 * time we will need to inject data with mock services.
 * 
 * Individual subclasses do their own @ContextConfiguration 
 * e.g. for cwms, cwms-web, rest servlet configurations
 * but everything else is configured here
 * 
 * @author Galen Beck 2015-06-01 
 */
@Category(AtlasIntegrationTest.class)
@WebAppConfiguration
public abstract class MvcBaseTestClass {
	@Autowired
	private WebApplicationContext wac;
	@Autowired
	private ServletContext sc;
	@Autowired
	@Qualifier("dbUnitConnection")
	private IDatabaseConnection connection;
	@Autowired
	@Qualifier("dbUnitConnection2")
	private IDatabaseConnection connection2;

	private static Log logger = LogFactory.getLog(MvcBaseTestClass.class);
	private static final String RESET_SCRIPT = "Reset.xml";
	private static final String RESET_SCRIPT2 = "Reset2.xml";

	protected MockMvc mockMvc;

	/**
	 * Must be defined by subclass; if subclasses want to provide no data set,
	 * override the method and provide an empty list.
	 */
	protected abstract List<String> getDataSetFileNames();

	/**
	 * Defines the datasets for the files db
	 */
	protected List<String> getFilesDataSetFileNames() {
		return Collections.emptyList();
	}

	/**
	 * Takes a local file name relative to the instance class directory (i.e. the
	 * actual test class) and loads the data, providing it back in a DbUnit IDataSet
	 */
	private IDataSet getDataSet(String fileName) throws Exception {
		String resourceLocation = "test-data/" + fileName;
		try(InputStream inputStream = getClass().getClassLoader().getResourceAsStream(resourceLocation)) {
			FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
			builder.setColumnSensing(true);
			builder.setDtdMetadata(false);
			IDataSet fullDataSet = builder.build(inputStream);
			return fullDataSet;
		}
		catch (Exception e) {
			throw new RuntimeException("Error loading resource : "+resourceLocation, e); 
		}
	}
	
	@BeforeClass
	public static void setupBeforeEachClass() throws Exception {
		
	}

	/*
	 * Runs before each subclass test method, loading data set if provided and then
	 * building mockMvc for use by subclass test methods. Ordered insert won't work
	 * due to cyclic tables dependency (quotation header, maybe others) e.g. //
	 * IDataSet filteredDataSet = new FilteredDataSet(new
	 * DatabaseSequenceFilter(connection), dataSet);
	 */
    @Before
    @BeforeEach
	public void setupBeforeEachTest() throws Exception {
		loadRegularDatabase();
		loadFilesDatabase();

		OpenEntityManagerInViewFilter filter = new OpenEntityManagerInViewFilter();
		filter.setServletContext(this.sc);

		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).addFilters(filter).build();
	}
	
	private void loadRegularDatabase() throws Exception {
		// load into database
		List<String> dataSetFileNames = getDataSetFileNames();
		if (dataSetFileNames != null && !dataSetFileNames.isEmpty()) {
			// reset database if there are data to load
			performOperation(RESET_SCRIPT, DatabaseOperation.DELETE_ALL, connection);
			// method which can be overriden by subclasses for any initialization before load
			afterResetBeforeLoad();
			for (String fileName : dataSetFileNames) {
				performOperation(fileName, new InsertIdentityOperation(DatabaseOperation.INSERT), connection);
			}
		} else {
			logger.info("No data set file names provided, skipping data load");
		}
	}

	private void loadFilesDatabase() throws Exception {
		// load into files database
		List<String> filesDataSetFileNames = getFilesDataSetFileNames();
		if (filesDataSetFileNames != null && !filesDataSetFileNames.isEmpty()) {
			// reset files database if there are data to load
			performOperation(RESET_SCRIPT2, DatabaseOperation.DELETE_ALL, connection2);
			for (String fileName : filesDataSetFileNames) {
				performOperation(fileName, new InsertIdentityOperation(DatabaseOperation.INSERT), connection2);
			}
		} else {
			logger.info("No data set file names provided, skipping data load for files db");
		}
	}
	
	/**
	 * method which can be overriden by subclasses for any special initialization before data loading
	 */
	protected void afterResetBeforeLoad() throws Exception {
		// intentionally empty
	}

	private void performOperation(String fileName, DatabaseOperation operation, IDatabaseConnection connection)
			throws Exception {
		
		long startTime = System.currentTimeMillis();

		logger.info("Performing " + operation.toString() + " from file " + fileName);
		IDataSet dataSet = getDataSet(fileName);
		operation.execute(connection, dataSet);
		
		long finishTime = System.currentTimeMillis();
		logger.info("Performed operation from file "+fileName+" in "+(finishTime-startTime)+" ms");
	}

	private static void logInfo(Description description, String status, long nanos) {
		String testName = description.getMethodName();
		logger.info(String.format("Test %s %s, spent %d microseconds / %d milliseconds / %d seconds", testName, status,
				TimeUnit.NANOSECONDS.toMicros(nanos), TimeUnit.NANOSECONDS.toMillis(nanos),
				TimeUnit.NANOSECONDS.toSeconds(nanos)));
	}

	@Rule
	public Stopwatch stopwatch = new Stopwatch() {
		@Override
		protected void succeeded(long nanos, Description description) {
			logInfo(description, "succeeded", nanos);
		}

		@Override
		protected void failed(long nanos, Throwable e, Description description) {
			logInfo(description, "failed", nanos);
		}

		@Override
		protected void skipped(long nanos, AssumptionViolatedException e, Description description) {
			logInfo(description, "skipped", nanos);
		}

		@Override
		protected void finished(long nanos, Description description) {
			logInfo(description, "finished", nanos);
		}
	};

    @AfterClass
    @AfterAll
	public static void shutdownEhCache() {
		CacheManager.getInstance().shutdown();
	}
}
