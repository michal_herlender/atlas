package org.trescal.cwms.tests;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.GenericXmlWebContextLoader;

/*
 * Base MVC test class for rest-servlet configuration
 */
@ContextConfiguration(loader = GenericXmlWebContextLoader.class,
        locations = {
                "classpath:applicationContext-dao.xml",
                "classpath:applicationContext-service.xml",
                "classpath:rest-servlet.xml",
                "classpath:mocks.xml",
                "classpath:applicationContext-mvc-test.xml"})
@Tag("org.trescal.cwms.core.AtlasIntegrationRestTest")
@ExtendWith(SpringExtension.class)
public abstract class MvcRestTestClass extends MvcBaseTestClass {
}
