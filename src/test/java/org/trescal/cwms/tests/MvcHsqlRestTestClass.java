package org.trescal.cwms.tests;

import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.GenericXmlWebContextLoader;

/*
 * Base MVC test class for rest-servlet configuration using EMBEDDED HSQL databases
 * 
 * Not working yet due to field type differences with MSSQL - Galen Beck - 2018-01-18
 */
@Ignore
@ContextConfiguration(loader = GenericXmlWebContextLoader.class,
        locations = {
                "classpath:applicationContext-dao.xml",
                "classpath:applicationContext-service.xml",
                "classpath:rest-servlet.xml",
                "classpath:applicationContext-hql-test.xml",
                "classpath:applicationContext-schedule.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public abstract class MvcHsqlRestTestClass extends MvcBaseTestClass {

}
