package org.trescal.cwms.tests.tools;

import javax.sql.DataSource;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExecuteSqlSpringContext {
	
	private static final String SQL_TEST_SCHEMA = "test-data/setup/cwms_test-create.sql";
	private static final String SQL_TEST_SCHEMA_FILES = "test-data/setup/cwms_testfiles-create.sql"; 
	
	public void execute() {
		try(ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext(
				"applicationContext-test-properties.xml", "applicationContext-mvc-test.xml")) {
					
			DataSource dataSource = (DataSource) applicationContext.getBean("pooledDataSource");
			DataSource dataSource2 = (DataSource) applicationContext.getBean("pooledDataSource2");

			executeScript(dataSource, SQL_TEST_SCHEMA);
			executeScript(dataSource2, SQL_TEST_SCHEMA_FILES);
		}
        catch(Exception e) {
        	log.error("Error executing SQL scripts", e);
        }
	}
	private void executeScript(DataSource dataSource, String filename) {
		log.info("executing script : " + filename);
		long start = System.currentTimeMillis();
		
		ResourceDatabasePopulator resourceDatabasePopulator = new ResourceDatabasePopulator();
	    resourceDatabasePopulator.addScript(new ClassPathResource(filename));
	    resourceDatabasePopulator.execute(dataSource);
	    
	    long finish = System.currentTimeMillis();
	    log.info("executed script in : " + (finish-start)+" ms");
	}
}
