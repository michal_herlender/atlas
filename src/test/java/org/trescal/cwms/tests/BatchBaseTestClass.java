package org.trescal.cwms.tests;

import org.junit.runner.RunWith;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.batch.test.JobRepositoryTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.SyncTaskExecutor;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.GenericXmlWebContextLoader;
import org.trescal.cwms.batch.config.entities.backgroundtask.BackgroundTaskService;

import javax.annotation.PostConstruct;

@EnableBatchProcessing
@ContextConfiguration(loader = GenericXmlWebContextLoader.class, locations = {"classpath:applicationContext-dao.xml",
        "classpath:applicationContext-service.xml", "classpath:cwms-servlet.xml", "classpath:mocks.xml",
        "classpath:mocks-cwms.xml", "classpath:applicationContext-mvc-test.xml",
        "classpath:applicationContext-schedule.xml", "classpath:applicationContext-batch-test.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public abstract class BatchBaseTestClass extends MvcBaseTestClass {

    @Autowired
	protected BackgroundTaskService backgroundTaskService;
	@Autowired
	private JobRepository jobRepository;
	@Autowired
	protected JobLauncher jobLauncher;

	protected JobLauncherTestUtils jobLauncherTestUtils;
	protected JobRepositoryTestUtils jobRepositoryTestUtils;

	@PostConstruct
	public void init() {

		((SimpleJobLauncher) jobLauncher).setTaskExecutor(new SyncTaskExecutor());
		this.jobLauncherTestUtils = new JobLauncherTestUtils();
		this.jobLauncherTestUtils.setJob(getJob());
		this.jobLauncherTestUtils.setJobLauncher(jobLauncher);

		this.jobRepositoryTestUtils = new JobRepositoryTestUtils();
		this.jobRepositoryTestUtils.setJobRepository(jobRepository);

	}

	protected abstract Job getJob();

}
