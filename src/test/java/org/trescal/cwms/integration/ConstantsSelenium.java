package org.trescal.cwms.integration;

public class ConstantsSelenium {

	public static final String ID_PASSWORD = "j_password";
	public static final String ID_USERNAME = "j_username";
	public static final String ID_SUBMIT_BUTTON = "submit";
	public static final String XPATH_VIEWPERSON_PERSONID = "//a[contains(@href, 'viewperson.htm?personid=')]";
	public static final String XPATH_LANGUAGE = "//*[@id='maincontent']/div[1]/span[2]/a[1]";
	public static final String XPATH_CONTINUE_BUTTON = "//*[@id='submit']";
	public static final String ID_ALLOCATED_SUBDIVID = "allocatedSubdivid";
	public static final String HEAD_TEXT_SMALL = "headtextsmall";
	public static final String HEAD_TEXT = "headtext";
	public static final String CONAME = "coname";
	public static final String ID_RECEIPT_DATE = "receiptdate";
	public static final String ID_STANDARD_RADIO = "standardradio";
	public static final String XPATH_ADD_STRAIGHT_TO_JOB = "//*[@id='addStraightToJob']";
	public static final String XPATH_ADD_IMG = "//*[@id='add']/img";
	public static final String ID_CLOSE_BUTTON = "jiwr_close";
	public static final String ID_SAVE_BUTTON = "jiwr_submit";
	public static final String XPATH_CAPABILITY = "//*[@id='ui-id-2']";
	public static final String ID_ACTION_OUTCOME = "actionOutcomeId";
	public static final String CLIENT_CAS = "Client - CAS";
	public static final String XPATH_JOBITEMFIRST = "//*[@id='subnav']/dl/dt[1]";
	public static final String XPATH_JOBITEMLIST = "//*[@id='jobitemlist']/tbody/tr[1]/td[1]/a";
	public static final String JOBS_LINK = "//*[@id=\"navigation\"]/div[2]/ul/li[8]/a";
	public static final String XPATH_NEWITEM_LINK = "//*[@id='newitem-link']";
	public static final String CERTIFICATE_OPTION = "img_inl_margleft";
	public static final String XPATH_LINKCERTIFICATES = "//*[@id='linkcertificates']";
	public static final String XPATH_CALIBRATION_LINK = "//*[@id='linkcalibration']";
	public static final String XPATH_COMPLETE_BUTTON = "//*[@id='completeButton']";
	public static final String XPATH_COMPLETECALIBRATION_SELECT = "//*[@id='completecalibration']/ol/li[14]/div[1]/select";
	public static final String XPATH_NEWACTIVITYDIV = "//*[@id='newactivitydiv']/div/fieldset/ol/li[2]/input[2]";
	public static final String XPATH_CURRENT_STATE = "//*[@id='currentState']/a";
	public static final String XPATH_CREATE_PRICE = "//*[@id='jcgeneral-tab']/table[1]/tfoot/tr/td/a";
	public static final String XPATH_COSTS_LINK = "//*[@id='costs-link']";
	public static final String XPATH_JOB_LINK = "//*[@id='jiHeader']/fieldset/ol/li[1]/span[1]";	
	public static final String fullname = "intergration Selenium";
	public static final String xpath_user_logged = "//a[contains(@href, 'viewperson.htm?personid=')]";
	public static final String lang_to_select = "en_GB";
	public static final String xpath_change_language_link = "//*[@id='maincontent']/div[1]/span[2]/a[1]";
	public static final String first_departement = "//*[@id=\"dept1-tab\"]/table[2]/tbody/tr[1]/td[2]/a";
	public static final String xpathOfJob = "//*[@id='workflowresults']/table[2]/tbody/tr[1]/td[1]/a";
	public static final String xpathofFirstInstrument = "//*[@id='jobitemlist']/tbody/tr[1]/td[2]/a";
	public static final String xpath_LANGUAGE = "//*[@id='maincontent']/div[1]/span[2]/a[1]";
}
