package org.trescal.cwms.integration.Job;

import org.junit.Assume;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.osgi.service.component.annotations.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryServiceImpl;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.db.DeliveryItemServiceImpl;
import org.trescal.cwms.core.deliverynote.entity.deliveryitemnote.db.DeliveryItemNoteServiceImpl;
import org.trescal.cwms.core.deliverynote.entity.deliverynote.db.DeliveryNoteServiceImpl;
import org.trescal.cwms.core.deliverynote.entity.jobdelivery.JobDelivery;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.db.CalibrationService;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateServiceImpl;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreviewitem.ContractReviewItem;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobServiceImpl;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemServiceImpl;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.db.JobCostingServiceImpl;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.db.JobCostingItemServiceImpl;
import org.trescal.cwms.integration.ConstantsSelenium;
import org.trescal.cwms.tests.SeleniumBaseTest;

import java.util.Iterator;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@Component
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Transactional(noRollbackFor = Exception.class, propagation = Propagation.REQUIRED)
public class JobItemCreationSeleniumTest extends SeleniumBaseTest {

	@Autowired
	MessageSource messages;
	@Autowired
	JobItemServiceImpl jobItemService;
	@Autowired
	CalibrationService calServ;
	@Autowired
	JobServiceImpl jobService;
	@Autowired
	DeliveryItemNoteServiceImpl deliveryItemNoteService;
	@Autowired
	DeliveryItemServiceImpl deliveryItemService;
	@Autowired
	private JobCostingServiceImpl jobCostServ;
	@Autowired
	CertificateServiceImpl certificateService;
	@Autowired
	DeliveryNoteServiceImpl deliveryNoteService;
	@Autowired
	JobCostingItemServiceImpl jobCostingItemService;
	@Autowired
	DeliveryServiceImpl deliveryService;
    @Autowired
    CertificateServiceImpl cerificateService;
    @Autowired
    InstrumService instrumService;

	private static String jobCostingId;
	private static String barecode;
	private static String jobItemId;
	private static int jobId;

	/**
	 * the classes test the job item creation : t01: Connection t02: Change language
	 * t03: Change Subdivision t04: Create Standard Job t05: Add New Instrument and
	 * Link it to Job t06: Perform Contract Review t07: Add WorkRequirements t08:
	 * Start calibration t09: Sign certificate t10: Create job costing t11: Create
	 * new delivery t12: Goods out t13: delete actions, deliveryNote, jobCosting,
	 * certificate, calibration t14: delete jobItem and Job added by the user
	 */

	@Test
	public void t01_login() throws Exception {
		webDriver.get(baseUrl);
		webDriver.findElement(By.id(ConstantsSelenium.ID_USERNAME)).sendKeys(this.username);
		webDriver.findElement(By.id(ConstantsSelenium.ID_PASSWORD)).sendKeys(this.password);

		WebElement loginButton = webDriver.findElement(By.name(ConstantsSelenium.ID_SUBMIT_BUTTON));
		loginButton.click();

		String userProfileName = webDriver.findElement(By.xpath(ConstantsSelenium.XPATH_VIEWPERSON_PERSONID)).getText();
		assertEquals(userProfileName, "intergration Selenium");

	}

	@Test
	public void t02_changeLocalLang() {
		Assume.assumeThat(isElementPresent(By.className("headscan")), is(true));

		WebElement selectLanguage = webDriver.findElement(By.xpath(ConstantsSelenium.XPATH_LANGUAGE));
		selectLanguage.click();

		WebElement desiredLanguage = webDriver.findElement(By.name("desiredLocale"));
		Select LabSelect = new Select(desiredLanguage);
		LabSelect.selectByValue("en_GB");

		WebElement changeLanguageButton = webDriver.findElement(By.xpath(ConstantsSelenium.XPATH_CONTINUE_BUTTON));
		changeLanguageButton.click();

		String language = webDriver.findElement(By.xpath(ConstantsSelenium.XPATH_LANGUAGE)).getText();
		assertEquals("English (United Kingdom)", language);
	}

	@Test
	public void t03_testselectSubdiv() {
		Assume.assumeThat(isElementPresent(By.id("subdivBox")), is(true));
		WebElement allocatedSubdiv = webDriver.findElement(By.id(ConstantsSelenium.ID_ALLOCATED_SUBDIVID));
		Select LabSelect = new Select(allocatedSubdiv);
		LabSelect.selectByVisibleText("Casablanca");
		String subdivName = webDriver.findElement(By.className(ConstantsSelenium.HEAD_TEXT_SMALL)).getText();
		assertEquals(subdivName, "TRESCAL MAROC SARL");
	}

	@Test
	public void t04_createStandardjob() throws Exception {
		Assume.assumeThat(isElementPresent(By.xpath(ConstantsSelenium.JOBS_LINK)), is(true));

		((JavascriptExecutor) webDriver).executeScript("document.getElementById('Goods_In').click();");

		Thread.sleep(3000);
		String pageTitle = webDriver.findElement(By.className(ConstantsSelenium.HEAD_TEXT)).getText();

		assertEquals(pageTitle, "Create Job");

		WebElement contactAddress = webDriver.findElement(By.name(ConstantsSelenium.CONAME));
		contactAddress.sendKeys("saf");

		wait(By.linkText("SAFRAN NACELLES MAROC"));
		webDriver.findElement(By.linkText("SAFRAN NACELLES MAROC")).click();

		WebElement standardButton = webDriver.findElement(By.id(ConstantsSelenium.ID_STANDARD_RADIO));
		standardButton.click();

		WebElement receiptDate = webDriver.findElement(By.id(ConstantsSelenium.ID_RECEIPT_DATE));
		receiptDate.click();

		((JavascriptExecutor) webDriver).executeScript(
				"document.querySelector('#ui-datepicker-div > table > tbody > tr:nth-child(3) > td.ui-datepicker-days-cell-over.ui-datepicker-today > a').click();");

		WebElement submit = webDriver.findElement(By.id(ConstantsSelenium.ID_SUBMIT_BUTTON));
		submit.click();

	}

	@Test
	public void t05_addNewInstrumentandLinkittoJob() throws Exception {
		Assume.assumeThat(isElementPresent(By.xpath(ConstantsSelenium.XPATH_NEWITEM_LINK)), is(true));

		((JavascriptExecutor) webDriver).executeScript("document.getElementById('newitem-link').click();");

		Thread.sleep(3000);
		assertEquals(webDriver.findElement(By.className(ConstantsSelenium.HEAD_TEXT)).getText(), "New Job Item Search");

		WebElement Brand = webDriver.findElement(By.id("mfrNm"));
		Brand.sendKeys("/");

		WebElement codebare = webDriver.findElement(By.id("searchForm.barcode"));
		codebare.sendKeys("100245");

		WebElement serachEveryWhere = webDriver.findElement(
				By.cssSelector("#searchfields > div:nth-child(2) > ol > li:nth-child(9) > button:nth-child(4)"));
		serachEveryWhere.click();

		Assume.assumeTrue(isElementPresent(By.xpath(ConstantsSelenium.XPATH_ADD_IMG)));
		webDriver.findElement(By.xpath(ConstantsSelenium.XPATH_ADD_IMG)).click();

		wait(By.xpath(ConstantsSelenium.XPATH_ADD_STRAIGHT_TO_JOB));
		webDriver.findElement(By.xpath(ConstantsSelenium.XPATH_ADD_STRAIGHT_TO_JOB)).click();

		assertTrue(webDriver.findElement(By.className(ConstantsSelenium.HEAD_TEXT)).getText()
				.contains(messages.getMessage("viewjob.viewjob", null, "View Job", LocaleContextHolder.getLocale())));
	}

	@Test
	public void t07_performContractReview() throws Exception {

		Select selectTransportIn = new Select(webDriver.findElement(By.id("inMethodId")));
		selectTransportIn.selectByVisibleText(ConstantsSelenium.CLIENT_CAS);

		Select selectTransportOut = new Select(webDriver.findElement(By.id("returnMethodId")));
		selectTransportOut.selectByVisibleText(ConstantsSelenium.CLIENT_CAS);

		WebElement requiresCleaning = webDriver.findElement(By.id("ji.reqCleaning2"));
		requiresCleaning.click();

		Select actionOutcome = new Select(webDriver.findElement(By.id(ConstantsSelenium.ID_ACTION_OUTCOME)));
		actionOutcome.selectByVisibleText("Requires in-house calibration");

		WebElement contractReviewButton = webDriver.findElement(By.className("contract_review"));
		contractReviewButton.click();

		String pageTitle = webDriver.findElement(By.className(ConstantsSelenium.HEAD_TEXT)).getText();
		assertTrue(pageTitle.contains("Viewing job"));

	}

	@Test
	public void t06_addWorkRequirements() throws Exception {

		WebElement itemsJob = webDriver.findElement(By.xpath(ConstantsSelenium.XPATH_JOBITEMLIST));
		itemsJob.click();

		WebElement itemJob = webDriver.findElement(By.xpath(ConstantsSelenium.XPATH_JOBITEMFIRST));
		itemJob.click();

		Assume.assumeThat(isElementPresent(By.className("workReqEdit")), is(true));

		WebElement editworkRequirement = webDriver.findElement(By.className("workReqEdit"));
		editworkRequirement.click();

		webDriver.switchTo().frame(webDriver.findElement(By.tagName("iframe")));

		webDriver.findElement(By.className("ui-autocomplete-input")).sendKeys("MA-");
		WebElement capability = webDriver.findElement(By.xpath(ConstantsSelenium.XPATH_CAPABILITY));
		WebDriverWait wait = new WebDriverWait(webDriver, 20);
		wait.until(ExpectedConditions.visibilityOf(capability));
		capability.click();

		WebElement saveRequirement = webDriver.findElement(By.id(ConstantsSelenium.ID_SAVE_BUTTON));
		saveRequirement.click();

		WebElement closeButton = webDriver.findElement(By.id(ConstantsSelenium.ID_CLOSE_BUTTON));
		closeButton.click();

		webDriver.switchTo().defaultContent();
	}

	@Test
	public void t08_startCalibration() throws Exception {

		webDriver.findElement(By.xpath("//*[@id='linkcontractrev']")).click();
		WebElement contractReviewButton2 = webDriver.findElement(By.className("contract_review"));
		contractReviewButton2.click();

		Assume.assumeThat(isElementPresent(By.id("linkcalibration")), is(true));

		webDriver.findElement(By.id("linkcalibration")).click();

		webDriver.findElement(By.linkText("Create New Calibration")).click();

		Thread.sleep(2000);

		WebElement calibrationDate = webDriver.findElement(By.id("calDate"));
		Assume.assumeNotNull(calibrationDate.getAttribute("value"));

		Select asFound = new Select(webDriver.findElement(By.id("cal.calClass")));
		Assume.assumeNotNull(asFound.getFirstSelectedOption());

		Select calibrationProcess = new Select(webDriver.findElement(By.id("processSelect")));
		Assume.assumeNotNull(calibrationProcess.getFirstSelectedOption());

		WebElement continueButton = webDriver.findElement(By.xpath(ConstantsSelenium.XPATH_CONTINUE_BUTTON));
		continueButton.click();

		WebElement remarks = webDriver.findElement(By.id("remark"));
		remarks.sendKeys("test");

		Select selectCompleteCalibration = new Select(
				webDriver.findElement(By.xpath(ConstantsSelenium.XPATH_COMPLETECALIBRATION_SELECT)));
		WebElement option = selectCompleteCalibration.getFirstSelectedOption();
		String defaultItem = option.getText();
		assertEquals("Calibrated successfully", defaultItem);

		WebElement timeTaken = webDriver.findElement(By.xpath("//*[@id='completecalibration']/ol/li[14]/div[2]/input"));
		timeTaken.clear();
		timeTaken.sendKeys("5");

		WebElement newCertificateNumber = webDriver.findElement(By.id("issueNewCertCheckbox"));
		newCertificateNumber.click();
		Thread.sleep(2000);
		newCertificateNumber.click();

		WebElement completeCalibration = webDriver.findElement(By.xpath(ConstantsSelenium.XPATH_COMPLETE_BUTTON));
		completeCalibration.click();

		WebElement calibrationLink = webDriver.findElement(By.xpath(ConstantsSelenium.XPATH_CALIBRATION_LINK));
		calibrationLink.click();

		String itemStatus = webDriver.findElement(By.id("currentState")).getText();
		assertTrue(itemStatus.contains("Calibrated, certificate awaiting signature approval"));
	}

	@Test
	public void t09_certificateSign() throws Exception {
		Assume.assumeThat(isElementPresent(By.xpath(ConstantsSelenium.XPATH_LINKCERTIFICATES)), is(true));

		WebElement certificateLink = webDriver.findElement(By.xpath(ConstantsSelenium.XPATH_LINKCERTIFICATES));
		certificateLink.click();

		WebElement certificateOption = webDriver.findElement(By.className(ConstantsSelenium.CERTIFICATE_OPTION));
		certificateOption.click();

		((JavascriptExecutor) webDriver).executeScript(
				"document.querySelector('#visibleBox > fieldset > ol > li:nth-child(12) > div.float-left.padtop > form > a').click()");

		Thread.sleep(3000);
	}

	@Test
	public void t10_createJboCosting() throws Exception {
		Assume.assumeThat(isElementPresent(By.xpath(ConstantsSelenium.XPATH_JOB_LINK)), is(true));

		WebElement job = webDriver.findElement(By.xpath(ConstantsSelenium.XPATH_JOB_LINK));
		job.click();

		WebElement costLink = webDriver.findElement(By.xpath(ConstantsSelenium.XPATH_COSTS_LINK));
		costLink.click();

		WebElement createPrice = webDriver.findElement(By.xpath(ConstantsSelenium.XPATH_CREATE_PRICE));
		createPrice.click();

		Select primaryContact = new Select(webDriver.findElement(By.id("personid")));
		Assume.assumeNotNull(primaryContact.getFirstSelectedOption());

		Select pricingType = new Select(webDriver.findElement(By.id("clientCosting")));
		Assume.assumeNotNull(pricingType.getFirstSelectedOption());

		WebElement pricingStatus = webDriver.findElement(By.id("jobCostingTypeId"));
		Select dropdown = new Select(pricingStatus);
		dropdown.selectByVisibleText("Final");

		WebElement clientRef = webDriver.findElement(By.id("clientref"));
		clientRef.sendKeys("test11");

		WebElement selectItem = webDriver.findElement(By.cssSelector("#jobitems > tbody > tr > td:nth-child(1)"));
		selectItem.click();

		WebElement createButton = webDriver.findElement(By.xpath(ConstantsSelenium.XPATH_CONTINUE_BUTTON));
		createButton.click();

		jobCostingId = (String) ((JavascriptExecutor) webDriver)
				.executeScript(" return(window.location.href.substring(window.location.href.lastIndexOf('=')+1))");

		WebElement setPricingAsIssued = webDriver.findElement(By.xpath("//*[@id='issueCostingLink']"));
		setPricingAsIssued.click();

		webDriver.findElement(By.xpath("//*[@id='costingitems-link']")).click();

		webDriver.findElement(By.linkText("1")).click();

		WebElement newActivity = webDriver.findElement(By.xpath("//*[@id='newActivity']/a"));
		newActivity.click();

		WebElement completeActivity = webDriver.findElement(By.xpath(ConstantsSelenium.XPATH_NEWACTIVITYDIV));
		completeActivity.click();

		WebElement completeActivity2 = webDriver.findElement(By.xpath("//*[@id='noIncompleteButton']"));
		completeActivity2.click();

		Thread.sleep(3000);
		String refresh = webDriver.getCurrentUrl();
		webDriver.get(refresh);

		String itemStatus = webDriver.findElement(By.id("currentState")).getText();
		assertTrue(itemStatus.contains("Awaiting delivery note to client"));
	}

	@Test
	public void t11_createNewDelivery() {
		Assume.assumeThat(isElementPresent(By.xpath(ConstantsSelenium.XPATH_CURRENT_STATE)), is(true));

		webDriver.findElement(By.xpath(ConstantsSelenium.XPATH_CURRENT_STATE)).click();

		Select contact = new Select(webDriver.findElement(By.id("contactid")));
		Assume.assumeNotNull(contact.getFirstSelectedOption());

		Assume.assumeNotNull(webDriver.findElement(By.id("deliverydate")));

		WebElement deliveryInstruction = webDriver.findElement(By.id("delInstruction"));
		deliveryInstruction.sendKeys("test");

		WebElement addToDeliveryNote = webDriver.findElement(By.id("selectAll"));
		addToDeliveryNote.click();

		WebElement submitButton = webDriver.findElement(By.xpath(ConstantsSelenium.XPATH_CONTINUE_BUTTON));
		submitButton.click();

		webDriver.findElement(By.linkText("1")).click();

		String itemStatus = webDriver.findElement(By.id("currentState")).getText();

		assertTrue(itemStatus.contains("Awaiting despatch to client"));
	}

	@Test
	public void t12_goodsOut() throws Exception {

		Assume.assumeThat(isElementPresent(By.cssSelector("#jiHeader > fieldset > ol > li:nth-child(3)")), is(true));
		barecode = webDriver.findElement(By.cssSelector("#jiHeader > fieldset > ol > li:nth-child(3) > span > a"))
				.getText();

		((JavascriptExecutor) webDriver).executeScript("document.getElementById('Scan_Out').click();");

		Thread.sleep(2000);

		WebElement scanOut = webDriver.findElement(By.id("scanout"));
		scanOut.sendKeys(barecode);
		scanOut.sendKeys(Keys.ENTER);

		assertTrue(isElementPresent(By.xpath("//*[@id='scanOutHistory']/div/img")));

		webDriver.navigate().back();

		String itemStatus = webDriver.findElement(By.id("currentState")).getText();
		assertTrue(itemStatus.contains("Work completed, despatched to client"));
	}

	@Test
	@Rollback(false)
	public void t13_deleteWhatUserAddedPart1() throws Exception {
		Assume.assumeNotNull(jobCostingId, barecode);

		jobItemId = webDriver.findElement(By.id("currentJobItemId")).getAttribute("value");
		JobItem jobItem = this.jobItemService.findJobItem(Integer.parseInt(jobItemId));
		jobId = jobItem.getJob().getJobid();

		// delete Actions
		String actionString = webDriver.findElement(By.xpath("//*[@id='tableofactions']/thead/tr[2]/td/span"))
				.getText();
		int nbAction = Integer.parseInt(actionString) - 1;
		while (nbAction > 0) {
			webDriver.findElement(By.xpath("//*[@id='form2']/a")).click();
			nbAction--;
		}

		// delete Delivery Note
		JobDelivery delivery = jobItem.getJob().getDeliveries().stream().filter(ii -> ii.getJob().getJobid() == jobId)
				.findFirst().orElse(null);
		deliveryService.removeDeliveryFromSchedule(delivery.getDeliveryid());
		deliveryService.deleteDelivery(delivery);

		// delete jobCosting
		JobCosting cost = jobCostServ.findJobCosting(Integer.parseInt(jobCostingId));
		jobCostServ.delete(cost);

		ContractReviewItem user = jobItem.getContactReviewItems().first();
		Contact userSelenium = user.getLastModifiedBy();

		// delete Certificate
		Calibration calibration = calServ.findLastCompletedCalibrationForJobitem(jobItem.getJobItemId());
		Certificate Certif = calibration.getCerts().iterator().next();
		certificateService.deleteCertificate(Certif, userSelenium);

		// Set lastCali to null in Instrument Table
		Iterator<org.trescal.cwms.core.instrument.entity.instrument.Instrument> ins = instrumService
			.findInstrumByBarCode(barecode).iterator();
		org.trescal.cwms.core.instrument.entity.instrument.Instrument newInst = ins.next();
		newInst.setLastCal(null);
		instrumService.merge(newInst);

		// Delete Calibration
		calServ.deleteCalibration(calibration, userSelenium);
	}

	@Test
	@Rollback(false)
	public void t14_deleteWhatUserAddedPart2() throws Exception {
		Assume.assumeNotNull(jobItemId, jobId);
		// Delete the job item
		this.jobItemService.deleteJobItemWithChecks(Integer.parseInt(jobItemId));
		// Delete the job
		this.jobService.deleteJobById(jobId);
	}

	private void wait(By by) {
		WebDriverWait wait = new WebDriverWait(webDriver, 200);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
	}
}
