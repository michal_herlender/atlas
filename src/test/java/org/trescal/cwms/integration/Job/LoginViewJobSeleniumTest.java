package org.trescal.cwms.integration.Job;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Assume;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.integration.ConstantsSelenium;
import org.trescal.cwms.tests.SeleniumBaseTest;

/**
 * classes test : login and view a job 
 * t1 : Login to the application by a user we created (See script 431) 
 * t2 : Change Local Language to english 
 * t3 : Change selected subdiv to Vendôme 
 * t4 : Navigate to the "job item dashboard" and click through the various tabs 
 * t5 : Navigate "job item results page" 
 * t6 : Nagivate to view a particular job 
 * t7 : Nagivate to view the first instrument of the job
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class LoginViewJobSeleniumTest extends SeleniumBaseTest {

	@Autowired
	MessageSource messages;



	@Test
	public void t0_testLoginUnsuccessful() throws Exception {
		this.webDriver.get(this.baseUrl);
		assertEquals(webDriver.getTitle(), "Trescal Group ERP");
		webDriver.findElement(By.id("j_username")).sendKeys(this.wrongusername);
		webDriver.findElement(By.id("j_password")).sendKeys(this.wrongpassword);
		webDriver.findElement(By.name("submit")).click();

		assertEquals(webDriver.findElement(By.xpath("/html/body/div/div[3]/fieldset/p/font")).getText(),
				messages.getMessage("login.error1", null, null, LocaleContextHolder.getLocale()));
	}

	@Test
	public void t1_testLoginSuccessful() {
		assertEquals(webDriver.getTitle(), "Trescal Group ERP");
		webDriver.findElement(By.id("j_username")).sendKeys(this.username);
		webDriver.findElement(By.id("j_password")).sendKeys(this.password);
		webDriver.findElement(By.name("submit")).click();
		assertEquals(webDriver.findElement(By.xpath(ConstantsSelenium.xpath_user_logged)).getText(), ConstantsSelenium.fullname);
	}

	@Test
	public void t2_changeLocalLang() {
		WebElement change_language = webDriver.findElement(By.xpath(ConstantsSelenium.xpath_change_language_link));
		change_language.click();
		WebElement languageDropDown = webDriver.findElement(By.name("desiredLocale"));
		Select labSelect = new Select(languageDropDown);
		labSelect.selectByValue(ConstantsSelenium.lang_to_select);
		WebElement Change_Language_button = webDriver.findElement(By.xpath("//*[@id='submit']"));
		Change_Language_button.click();
		String language = webDriver.findElement(By.xpath(ConstantsSelenium.xpath_LANGUAGE)).getText();
		assertEquals("English (United Kingdom)", language);
	}

	@Test
	public void t3_testselectSubdiv() {
		WebElement element = webDriver.findElement(By.id("allocatedSubdivid"));
		Select LabSelect = new Select(element);
		LabSelect.selectByVisibleText("Casablanca");
		assertEquals(webDriver.findElement(By.className("headtextsmall")).getText(), "TRESCAL MAROC SARL");
	}

	@Test
	public void t4_jobMenuActions() throws Exception {
		((JavascriptExecutor) webDriver).executeScript("document.getElementById('Job_item_dashboard').click();");
		
		for (int i = 1; i <= 7; i++) {
			webDriver.findElement(By.id("dept" + i + "-link")).click();
			assertTrue(webDriver.findElement(By.className("tab-box")).isDisplayed());
			if (i == 7) {
				webDriver.findElement(By.id("dept1-link")).click();
				assertTrue(webDriver.findElement(By.className("tab-box")).isDisplayed());
			}
		}
		assertEquals(webDriver.findElement(By.className("headtext")).getText(),
				messages.getMessage("workflow.viewworkfordepartmenttypes", null, "View Work For Department Types",
						LocaleContextHolder.getLocale()));
	}

	@Test
	public void t5_resultsPage() throws Exception {
		WebElement first_depart = webDriver.findElement(By.xpath(ConstantsSelenium.first_departement));
		first_depart.click();
		assertEquals(messages.getMessage("workflow.viewselectedjobitemsprogress", null, "View Selected Job Items",
				LocaleContextHolder.getLocale()), webDriver.findElement(By.className("headtext")).getText());
	}

	@Test
	public void t6_clickingParticularJob() throws Exception {
		Assume.assumeThat(webDriver.findElements(By.xpath(ConstantsSelenium.xpathOfJob)).size(), is(1));
		WebElement job = webDriver.findElement(By.xpath(ConstantsSelenium.xpathOfJob));
		job.click();
		assertTrue(webDriver.findElement(By.className("headtext")).getText()
				.contains(messages.getMessage("viewjob.viewjob", null, "View Job", LocaleContextHolder.getLocale())));
	}

	@Test
	public void t7_clickingFirstInstrument() throws Exception {
		Assume.assumeThat(webDriver.findElements(By.xpath(ConstantsSelenium.xpathofFirstInstrument)).size(), is(1));
		WebElement first_instrument = webDriver.findElement(By.xpath(ConstantsSelenium.xpathofFirstInstrument));
		first_instrument.click();
		assertTrue(webDriver.findElement(By.className("headtext")).getText().contains(
				messages.getMessage("viewinstrument.title", null, "View Instrument", LocaleContextHolder.getLocale())));
	}

}
