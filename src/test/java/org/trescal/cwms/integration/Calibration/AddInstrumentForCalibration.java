package org.trescal.cwms.integration.Calibration;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Assume;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.osgi.service.component.annotations.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobServiceImpl;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemServiceImpl;
import org.trescal.cwms.integration.ConstantsSelenium;
import org.trescal.cwms.tests.SeleniumBaseTest;

@Component
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Transactional(noRollbackFor = Exception.class, propagation = Propagation.REQUIRED)
public class AddInstrumentForCalibration extends SeleniumBaseTest {
	
	/**Add Instrument for different types of Calibration : 
	 * t01: Login with Credentials
	 * t02: Change language to English
	 * t03: Change the Subdivision to Madrid
	 * t04: Create a job
	 * t05: Add new items
	 * t06: Delete jobItems and job Added
	 */
	
	@Autowired
	MessageSource messages;
	@Autowired
	JobItemServiceImpl jobItemService;
	@Autowired
	JobServiceImpl jobService;
	
	@Test
	public void t01_login() throws Exception {
		this.webDriver.get(this.baseUrl);
		webDriver.findElement(By.id(ConstantsSelenium.ID_USERNAME)).sendKeys(this.username);
		webDriver.findElement(By.id(ConstantsSelenium.ID_PASSWORD)).sendKeys(this.password);

		WebElement loginButton = webDriver.findElement(By.name(ConstantsSelenium.ID_SUBMIT_BUTTON));
		loginButton.click();

		String userProfileName = webDriver.findElement(By.xpath(ConstantsSelenium.XPATH_VIEWPERSON_PERSONID)).getText();
		assertEquals(userProfileName, "intergration Selenium");
	}

	@Test
	public void t02_changeLocalLang() {
		Assume.assumeThat(isElementPresent(By.className("headscan")), is(true));

		WebElement selectLanguage = webDriver.findElement(By.xpath(ConstantsSelenium.XPATH_LANGUAGE));
		selectLanguage.click();

		WebElement desiredLanguage = webDriver.findElement(By.name("desiredLocale"));
		Select LabSelect = new Select(desiredLanguage);
		LabSelect.selectByValue("en_GB");

		WebElement changeLanguageButton = webDriver.findElement(By.xpath(ConstantsSelenium.XPATH_CONTINUE_BUTTON));
		changeLanguageButton.click();

		String language = webDriver.findElement(By.xpath(ConstantsSelenium.XPATH_LANGUAGE)).getText();
		assertEquals("English (United Kingdom)", language);
	}

	@Test
	public void t03_selectSubdiv() {
		Assume.assumeThat(isElementPresent(By.id("subdivBox")), is(true));
		WebElement allocatedSubdiv = webDriver.findElement(By.id(ConstantsSelenium.ID_ALLOCATED_SUBDIVID));
		Select LabSelect = new Select(allocatedSubdiv);
		LabSelect.selectByVisibleText("Madrid");
		String subdivName = webDriver.findElement(By.className(ConstantsSelenium.HEAD_TEXT_SMALL)).getText();
		assertEquals(subdivName, "TRESCAL ESPAÑA DE METROLOGIA SL");
	}
	
	@Test
	public void t04_goodsIn() throws Exception{
		((JavascriptExecutor) webDriver).executeScript("document.getElementById('Goods_In').click();");
		Thread.sleep(3000);
		String pageTitle = webDriver.findElement(By.className(ConstantsSelenium.HEAD_TEXT)).getText();

		assertEquals(pageTitle, "Create Job");

		webDriver.findElement(By.xpath("//*[@id='coname']")).sendKeys("saf");

		wait(By.linkText("SAFER INSTRUMENTACION S.L."));
		webDriver.findElement(By.linkText("SAFER INSTRUMENTACION S.L.")).click();

		WebElement standardButton = webDriver.findElement(By.id(ConstantsSelenium.ID_STANDARD_RADIO));
		standardButton.click();

		WebElement receiptDate = webDriver.findElement(By.id(ConstantsSelenium.ID_RECEIPT_DATE));
		receiptDate.click();
		
		webDriver.findElement(By.xpath("/html/body/div[6]/table/tbody/tr[2]/td[4]/a")).click();
	
		new Select(webDriver.findElement(By.id("transportIn"))).selectByVisibleText("Client");
	    webDriver.findElement(By.id("transportIn")).click();
		
	    new Select(webDriver.findElement(By.id("transportOut"))).selectByVisibleText("Client");
	    webDriver.findElement(By.id("transportOut")).click();
		
		WebElement submit = webDriver.findElement(By.id(ConstantsSelenium.ID_SUBMIT_BUTTON));
		submit.click();
		
		assertTrue(webDriver.findElement(By.className(ConstantsSelenium.HEAD_TEXT)).getText()
				.contains(messages.getMessage("viewjob.viewjob", null, "View Job", LocaleContextHolder.getLocale())));
	}
	
	@Test
	public void t05_addNewItem() throws Exception {
		Assume.assumeThat(webDriver.findElements(By.id("newitem-link")).size(), is(1));
		
		webDriver.findElement(By.id("newitem-link")).click();
		
		WebElement brand=webDriver.findElement(By.xpath("//*[@id='mfrNm']"));
		brand.sendKeys("FLUKE");
		
		WebElement model=webDriver.findElement(By.xpath("//*[@id='searchForm.model']"));
		model.sendKeys("110");
		
		WebElement searchEverywhere=webDriver.findElement(By.name("action"));
		searchEverywhere.click();
		
		webDriver.findElement(By.id("models-link")).click();
		//add the first item
		webDriver.findElement(By.xpath("//*[@id='model-table']/img")).click();
		//add the second item
		webDriver.findElement(By.xpath("//*[@id='model-table']/img")).click();
		
		wait(By.id("addItems"));
		webDriver.findElement(By.id("addItems")).click();
		
		assertTrue(webDriver.findElement(By.className(ConstantsSelenium.HEAD_TEXT)).getText().contains("Add New Items To Job"));
		
		WebElement serialNo1=webDriver.findElement(By.name("instSerialNos[0]"));
		serialNo1.sendKeys("SN4A4A4A22223");
		
		WebElement plantNo1=webDriver.findElement(By.name("instPlantNos[0]"));
		plantNo1.sendKeys("PN434243");
		
		new Select(webDriver.findElement(By.id("serviceTypeIds0"))).selectByVisibleText("AC-MVO-IL - Accredited calibration without statement of compliance in laboratory");
	    webDriver.findElement(By.id("serviceTypeIds0")).click();
		
		WebElement serialNo2=webDriver.findElement(By.name("instSerialNos[1]"));
		serialNo2.sendKeys("SN4A4A4A222245");
		
		WebElement plantNo2=webDriver.findElement(By.name("instPlantNos[1]"));
		plantNo2.sendKeys("PN434244655");
		
		new Select(webDriver.findElement(By.id("serviceTypeIds1"))).selectByVisibleText("AC-MVO-IL - Accredited calibration without statement of compliance in laboratory");
	    webDriver.findElement(By.id("serviceTypeIds1")).click();
	    
	    webDriver.findElement(By.xpath("//*[@id='addnewitemstojobform']/div[2]/input[2]")).click();
	    
	    Thread.sleep(4000);
	    
	    if(isElementPresent(By.xpath("/html/body/table/tbody/tr[2]/td[2]/div[2]/div/div[2]/input[1]"))){
	    	webDriver.findElement(By.xpath("/html/body/table/tbody/tr[2]/td[2]/div[2]/div/div[2]/input[1]")).click();
	    }
	    
	    Thread.sleep(3000);
	    Assume.assumeThat(isElementPresent(By.linkText("1")), is(true));

	}
	
	@Test
	@Rollback(false)
	public void t06_deleteItemsAdded() {
		//delete the jobItems added
		String jobitem1=webDriver.findElement(By.xpath("//*[@id='jobitemlist']/tbody/tr[1]")).getAttribute("id");
		String jobitem1Id=jobitem1.substring(2);
		String jobitem2=webDriver.findElement(By.xpath("//*[@id='jobitemlist']/tbody/tr[2]")).getAttribute("id");
		String jobitem2Id=jobitem2.substring(2);
		JobItem jobItem=jobItemService.findJobItem(Integer.parseInt(jobitem2Id));
		this.jobItemService.deleteJobItemWithChecks(Integer.parseInt(jobitem1Id));
		this.jobItemService.deleteJobItemWithChecks(Integer.parseInt(jobitem2Id));
		//delete the job 
		this.jobService.deleteJobById(jobItem.getJob().getJobid());
	}
	
	private void wait(By by) {
		WebDriverWait wait = new WebDriverWait(webDriver, 200);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
	}
	
}
