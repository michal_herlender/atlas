package org.trescal.cwms.integration;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.trescal.cwms.integration.Job.JobItemCreationSeleniumTest;
import org.trescal.cwms.integration.Job.LoginViewJobSeleniumTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	LoginViewJobSeleniumTest.class,
	JobItemCreationSeleniumTest.class
})
public class IntegrationTestsSuite {
	
}
