package org.trescal.cwms.integration.Login;

import static org.junit.Assert.assertThat;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.trescal.cwms.tests.SeleniumBaseTest;

public class MainLoginPageSeleniumTest extends SeleniumBaseTest {

	@Test
	public void testLogin() throws Exception {
		this.webDriver.get(this.baseUrl);
		assertThat(getHttpStatus(this.baseUrl),
				Matchers.either(Matchers.is(HttpStatus.OK.value())).or(Matchers.is(HttpStatus.FOUND.value())));
	}
	

}
