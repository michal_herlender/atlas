package org.trescal.cwms.files.exchangeformat;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ExchangeFormatFileData.class)
public abstract class ExchangeFormatFileData_ {

	public static volatile SingularAttribute<ExchangeFormatFileData, byte[]> fileData;
	public static volatile SingularAttribute<ExchangeFormatFileData, Integer> entityId;

	public static final String FILE_DATA = "fileData";
	public static final String ENTITY_ID = "entityId";

}

