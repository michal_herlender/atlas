package org.trescal.cwms.files.images.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ImageFileData.class)
public abstract class ImageFileData_ {

	public static volatile SingularAttribute<ImageFileData, Integer> imageId;
	public static volatile SingularAttribute<ImageFileData, byte[]> fileData;

	public static final String IMAGE_ID = "imageId";
	public static final String FILE_DATA = "fileData";

}

