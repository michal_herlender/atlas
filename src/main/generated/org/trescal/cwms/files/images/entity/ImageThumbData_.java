package org.trescal.cwms.files.images.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ImageThumbData.class)
public abstract class ImageThumbData_ {

	public static volatile SingularAttribute<ImageThumbData, Integer> imageId;
	public static volatile SingularAttribute<ImageThumbData, byte[]> thumbData;

	public static final String IMAGE_ID = "imageId";
	public static final String THUMB_DATA = "thumbData";

}

