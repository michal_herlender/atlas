package org.trescal.cwms.files.contract.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ContractFile.class)
public abstract class ContractFile_ {

	public static volatile SingularAttribute<ContractFile, byte[]> fileData;
	public static volatile SingularAttribute<ContractFile, Integer> contractId;

	public static final String FILE_DATA = "fileData";
	public static final String CONTRACT_ID = "contractId";

}

