package org.trescal.cwms.core.logistics.entity.prebookingdetails;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.po.PO;
import org.trescal.cwms.core.logistics.entity.asn.Asn;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PrebookingJobDetails.class)
public abstract class PrebookingJobDetails_ {

	public static volatile SingularAttribute<PrebookingJobDetails, BPO> bpo;
	public static volatile ListAttribute<PrebookingJobDetails, PO> poList;
	public static volatile SingularAttribute<PrebookingJobDetails, TransportOption> inOption;
	public static volatile SingularAttribute<PrebookingJobDetails, TransportOption> returnOption;
	public static volatile SingularAttribute<PrebookingJobDetails, Contact> con;
	public static volatile SingularAttribute<PrebookingJobDetails, String> clientRef;
	public static volatile SingularAttribute<PrebookingJobDetails, BigDecimal> estCarriageOut;
	public static volatile SingularAttribute<PrebookingJobDetails, Location> bookedInLoc;
	public static volatile SingularAttribute<PrebookingJobDetails, PO> defaultPO;
	public static volatile SingularAttribute<PrebookingJobDetails, Address> returnTo;
	public static volatile SingularAttribute<PrebookingJobDetails, Date> pickupDate;
	public static volatile SingularAttribute<PrebookingJobDetails, SupportedCurrency> currency;
	public static volatile SingularAttribute<PrebookingJobDetails, Integer> id;
	public static volatile SingularAttribute<PrebookingJobDetails, Address> bookedInAddr;
	public static volatile SingularAttribute<PrebookingJobDetails, Asn> asn;
	public static volatile SingularAttribute<PrebookingJobDetails, Location> returnToLoc;

	public static final String BPO = "bpo";
	public static final String PO_LIST = "poList";
	public static final String IN_OPTION = "inOption";
	public static final String RETURN_OPTION = "returnOption";
	public static final String CON = "con";
	public static final String CLIENT_REF = "clientRef";
	public static final String EST_CARRIAGE_OUT = "estCarriageOut";
	public static final String BOOKED_IN_LOC = "bookedInLoc";
	public static final String DEFAULT_PO = "defaultPO";
	public static final String RETURN_TO = "returnTo";
	public static final String PICKUP_DATE = "pickupDate";
	public static final String CURRENCY = "currency";
	public static final String ID = "id";
	public static final String BOOKED_IN_ADDR = "bookedInAddr";
	public static final String ASN = "asn";
	public static final String RETURN_TO_LOC = "returnToLoc";

}

