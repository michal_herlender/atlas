package org.trescal.cwms.core.logistics.entity.asn;

import java.sql.Timestamp;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.logistics.entity.asnitems.AsnItem;
import org.trescal.cwms.core.logistics.entity.prebookingdetails.PrebookingJobDetails;
import org.trescal.cwms.core.logistics.entity.prebookingfile.PrebookingFile;
import org.trescal.cwms.core.logistics.utils.SourceAPIEnum;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Asn.class)
public abstract class Asn_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SingularAttribute<Asn, PrebookingJobDetails> prebookingJobDetails;
	public static volatile SingularAttribute<Asn, String> itemsFromJson;
	public static volatile SingularAttribute<Asn, Contact> createdBy;
	public static volatile ListAttribute<Asn, AsnItem> asnItems;
	public static volatile SingularAttribute<Asn, PrebookingFile> exchangeFormatFile;
	public static volatile SingularAttribute<Asn, Integer> id;
	public static volatile SingularAttribute<Asn, JobType> jobType;
	public static volatile SingularAttribute<Asn, SourceAPIEnum> sourceApi;
	public static volatile SingularAttribute<Asn, ExchangeFormat> exchangeFormat;
	public static volatile SingularAttribute<Asn, Job> job;
	public static volatile SingularAttribute<Asn, Timestamp> createdOn;

	public static final String PREBOOKING_JOB_DETAILS = "prebookingJobDetails";
	public static final String ITEMS_FROM_JSON = "itemsFromJson";
	public static final String CREATED_BY = "createdBy";
	public static final String ASN_ITEMS = "asnItems";
	public static final String EXCHANGE_FORMAT_FILE = "exchangeFormatFile";
	public static final String ID = "id";
	public static final String JOB_TYPE = "jobType";
	public static final String SOURCE_API = "sourceApi";
	public static final String EXCHANGE_FORMAT = "exchangeFormat";
	public static final String JOB = "job";
	public static final String CREATED_ON = "createdOn";

}

