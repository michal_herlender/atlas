package org.trescal.cwms.core.logistics.entity.jobcourier;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.deliverynote.entity.courier.Courier;
import org.trescal.cwms.core.jobs.job.entity.job.Job;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobCourierLink.class)
public abstract class JobCourierLink_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<JobCourierLink, Courier> courier;
	public static volatile SingularAttribute<JobCourierLink, Integer> id;
	public static volatile SingularAttribute<JobCourierLink, Job> job;
	public static volatile SingularAttribute<JobCourierLink, String> trackingNumber;

	public static final String COURIER = "courier";
	public static final String ID = "id";
	public static final String JOB = "job";
	public static final String TRACKING_NUMBER = "trackingNumber";

}

