package org.trescal.cwms.core.logistics.entity.asnitems;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.logistics.entity.analysisresult.AsnItemAnalysisResult;
import org.trescal.cwms.core.logistics.entity.asn.Asn;
import org.trescal.cwms.core.logistics.utils.AsnItemStatusEnum;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AsnItem.class)
public abstract class AsnItem_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SingularAttribute<AsnItem, ServiceType> servicetype;
	public static volatile SingularAttribute<AsnItem, Integer> index;
	public static volatile SingularAttribute<AsnItem, AsnItemAnalysisResult> analysisResult;
	public static volatile SingularAttribute<AsnItem, String> clientRef;
	public static volatile SingularAttribute<AsnItem, String> comment;
	public static volatile SingularAttribute<AsnItem, Instrument> instrument;
	public static volatile SingularAttribute<AsnItem, Integer> id;
	public static volatile SingularAttribute<AsnItem, Asn> asn;
	public static volatile SingularAttribute<AsnItem, String> poNumber;
	public static volatile SingularAttribute<AsnItem, AsnItemStatusEnum> status;

	public static final String SERVICETYPE = "servicetype";
	public static final String INDEX = "index";
	public static final String ANALYSIS_RESULT = "analysisResult";
	public static final String CLIENT_REF = "clientRef";
	public static final String COMMENT = "comment";
	public static final String INSTRUMENT = "instrument";
	public static final String ID = "id";
	public static final String ASN = "asn";
	public static final String PO_NUMBER = "poNumber";
	public static final String STATUS = "status";

}

