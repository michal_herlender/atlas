package org.trescal.cwms.core.logistics.entity.analysisresult;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.logistics.utils.NextActionEnum;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AsnItemAnalysisResult.class)
public abstract class AsnItemAnalysisResult_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SingularAttribute<AsnItemAnalysisResult, JobItem> jobItem;
	public static volatile SingularAttribute<AsnItemAnalysisResult, String> comment;
	public static volatile SingularAttribute<AsnItemAnalysisResult, Integer> id;
	public static volatile SingularAttribute<AsnItemAnalysisResult, NextActionEnum> nextAction;

	public static final String JOB_ITEM = "jobItem";
	public static final String COMMENT = "comment";
	public static final String ID = "id";
	public static final String NEXT_ACTION = "nextAction";

}

