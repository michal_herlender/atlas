package org.trescal.cwms.core.workflow.entity.actionoutcome;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_ClientDecisionCosting;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ActionOutcome_ClientDecisionTPInspectionCosts.class)
public abstract class ActionOutcome_ClientDecisionTPInspectionCosts_ extends org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome_ {

	public static volatile SingularAttribute<ActionOutcome_ClientDecisionTPInspectionCosts, ActionOutcomeValue_ClientDecisionCosting> value;

	public static final String VALUE = "value";

}

