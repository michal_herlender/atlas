package org.trescal.cwms.core.workflow.entity.nextactivity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(NextActivity.class)
public abstract class NextActivity_ {

	public static volatile SingularAttribute<NextActivity, ItemActivity> activity;
	public static volatile SingularAttribute<NextActivity, Boolean> manualEntryAllowed;
	public static volatile SingularAttribute<NextActivity, Integer> id;
	public static volatile SingularAttribute<NextActivity, ItemStatus> status;

	public static final String ACTIVITY = "activity";
	public static final String MANUAL_ENTRY_ALLOWED = "manualEntryAllowed";
	public static final String ID = "id";
	public static final String STATUS = "status";

}

