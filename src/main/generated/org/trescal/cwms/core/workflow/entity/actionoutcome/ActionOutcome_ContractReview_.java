package org.trescal.cwms.core.workflow.entity.actionoutcome;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_ContractReview;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ActionOutcome_ContractReview.class)
public abstract class ActionOutcome_ContractReview_ extends org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome_ {

	public static volatile SingularAttribute<ActionOutcome_ContractReview, ActionOutcomeValue_ContractReview> value;

	public static final String VALUE = "value";

}

