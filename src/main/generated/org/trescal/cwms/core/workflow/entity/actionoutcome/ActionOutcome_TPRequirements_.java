package org.trescal.cwms.core.workflow.entity.actionoutcome;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_TPRequirements;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ActionOutcome_TPRequirements.class)
public abstract class ActionOutcome_TPRequirements_ extends org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome_ {

	public static volatile SingularAttribute<ActionOutcome_TPRequirements, ActionOutcomeValue_TPRequirements> value;

	public static final String VALUE = "value";

}

