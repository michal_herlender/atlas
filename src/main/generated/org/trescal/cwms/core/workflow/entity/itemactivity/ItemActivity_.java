package org.trescal.cwms.core.workflow.entity.itemactivity;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.external.adveso.entity.advesotransferableactivity.AdvesoTransferableActivity;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeType;
import org.trescal.cwms.core.workflow.entity.lookupinstance.LookupInstance;
import org.trescal.cwms.core.workflow.entity.outcomestatus.OutcomeStatus;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ItemActivity.class)
public abstract class ItemActivity_ extends org.trescal.cwms.core.workflow.entity.itemstate.ItemState_ {

	public static volatile SingularAttribute<ItemActivity, LookupInstance> lookup;
	public static volatile SingularAttribute<ItemActivity, ActionOutcomeType> actionOutcomeType;
	public static volatile SetAttribute<ItemActivity, OutcomeStatus> outcomeStatuses;
	public static volatile SetAttribute<ItemActivity, ActionOutcome> actionOutcomes;
	public static volatile ListAttribute<ItemActivity, AdvesoTransferableActivity> advesoTransferableActivities;

	public static final String LOOKUP = "lookup";
	public static final String ACTION_OUTCOME_TYPE = "actionOutcomeType";
	public static final String OUTCOME_STATUSES = "outcomeStatuses";
	public static final String ACTION_OUTCOMES = "actionOutcomes";
	public static final String ADVESO_TRANSFERABLE_ACTIVITIES = "advesoTransferableActivities";

}

