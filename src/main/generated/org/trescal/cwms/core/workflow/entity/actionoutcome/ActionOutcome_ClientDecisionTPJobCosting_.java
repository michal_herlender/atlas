package org.trescal.cwms.core.workflow.entity.actionoutcome;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_ClientDecisionCosting;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ActionOutcome_ClientDecisionTPJobCosting.class)
public abstract class ActionOutcome_ClientDecisionTPJobCosting_ extends org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome_ {

	public static volatile SingularAttribute<ActionOutcome_ClientDecisionTPJobCosting, ActionOutcomeValue_ClientDecisionCosting> value;

	public static final String VALUE = "value";

}

