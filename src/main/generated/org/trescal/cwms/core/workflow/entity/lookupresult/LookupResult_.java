package org.trescal.cwms.core.workflow.entity.lookupresult;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.workflow.entity.lookupinstance.LookupInstance;
import org.trescal.cwms.core.workflow.entity.outcomestatus.OutcomeStatus;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(LookupResult.class)
public abstract class LookupResult_ {

	public static volatile SingularAttribute<LookupResult, LookupInstance> lookup;
	public static volatile SingularAttribute<LookupResult, Integer> resultMessageId;
	public static volatile SingularAttribute<LookupResult, OutcomeStatus> outcomeStatus;
	public static volatile SingularAttribute<LookupResult, Integer> id;

	public static final String LOOKUP = "lookup";
	public static final String RESULT_MESSAGE_ID = "resultMessageId";
	public static final String OUTCOME_STATUS = "outcomeStatus";
	public static final String ID = "id";

}

