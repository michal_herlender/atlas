package org.trescal.cwms.core.workflow.entity.lookupinstance;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupFunction;
import org.trescal.cwms.core.workflow.entity.lookupresult.LookupResult;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(LookupInstance.class)
public abstract class LookupInstance_ {

	public static volatile SingularAttribute<LookupInstance, String> description;
	public static volatile SingularAttribute<LookupInstance, Integer> id;
	public static volatile SingularAttribute<LookupInstance, LookupFunction> lookupFunction;
	public static volatile SetAttribute<LookupInstance, LookupResult> lookupResults;

	public static final String DESCRIPTION = "description";
	public static final String ID = "id";
	public static final String LOOKUP_FUNCTION = "lookupFunction";
	public static final String LOOKUP_RESULTS = "lookupResults";

}

