package org.trescal.cwms.core.workflow.entity.itemstatus;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.workflow.entity.nextactivity.NextActivity;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ItemStatus.class)
public abstract class ItemStatus_ extends org.trescal.cwms.core.workflow.entity.itemstate.ItemState_ {

	public static volatile SetAttribute<ItemStatus, NextActivity> nextActivities;

	public static final String NEXT_ACTIVITIES = "nextActivities";

}

