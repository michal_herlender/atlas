package org.trescal.cwms.core.workflow.entity.itemstate;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ItemState.class)
public abstract class ItemState_ {

	public static volatile SetAttribute<ItemState, Translation> translations;
	public static volatile SingularAttribute<ItemState, Integer> stateid;
	public static volatile SingularAttribute<ItemState, String> description;
	public static volatile SingularAttribute<ItemState, Boolean> active;
	public static volatile SetAttribute<ItemState, StateGroupLink> groupLinks;
	public static volatile SingularAttribute<ItemState, Boolean> retired;
	public static volatile SetAttribute<ItemState, JobItem> items;

	public static final String TRANSLATIONS = "translations";
	public static final String STATEID = "stateid";
	public static final String DESCRIPTION = "description";
	public static final String ACTIVE = "active";
	public static final String GROUP_LINKS = "groupLinks";
	public static final String RETIRED = "retired";
	public static final String ITEMS = "items";

}

