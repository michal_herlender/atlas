package org.trescal.cwms.core.workflow.entity.outcomestatus;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;
import org.trescal.cwms.core.workflow.entity.lookupinstance.LookupInstance;
import org.trescal.cwms.core.workflow.entity.lookupresult.LookupResult;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(OutcomeStatus.class)
public abstract class OutcomeStatus_ {

	public static volatile SingularAttribute<OutcomeStatus, LookupInstance> lookup;
	public static volatile SingularAttribute<OutcomeStatus, Boolean> defaultOutcome;
	public static volatile SingularAttribute<OutcomeStatus, ItemActivity> activity;
	public static volatile SingularAttribute<OutcomeStatus, Integer> id;
	public static volatile SetAttribute<OutcomeStatus, LookupResult> lookupResults;
	public static volatile SingularAttribute<OutcomeStatus, ItemStatus> status;

	public static final String LOOKUP = "lookup";
	public static final String DEFAULT_OUTCOME = "defaultOutcome";
	public static final String ACTIVITY = "activity";
	public static final String ID = "id";
	public static final String LOOKUP_RESULTS = "lookupResults";
	public static final String STATUS = "status";

}

