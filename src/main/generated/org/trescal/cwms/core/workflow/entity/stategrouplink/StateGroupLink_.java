package org.trescal.cwms.core.workflow.entity.stategrouplink;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(StateGroupLink.class)
public abstract class StateGroupLink_ {

	public static volatile SingularAttribute<StateGroupLink, Integer> groupId;
	public static volatile SingularAttribute<StateGroupLink, Integer> id;
	public static volatile SingularAttribute<StateGroupLink, ItemState> state;
	public static volatile SingularAttribute<StateGroupLink, StateGroupLinkType> type;

	public static final String GROUP_ID = "groupId";
	public static final String ID = "id";
	public static final String STATE = "state";
	public static final String TYPE = "type";

}

