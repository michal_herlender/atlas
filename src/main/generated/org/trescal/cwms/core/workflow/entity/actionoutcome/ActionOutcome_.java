package org.trescal.cwms.core.workflow.entity.actionoutcome;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ActionOutcome.class)
public abstract class ActionOutcome_ {

	public static volatile SingularAttribute<ActionOutcome, Boolean> defaultOutcome;
	public static volatile SingularAttribute<ActionOutcome, Boolean> positiveOutcome;
	public static volatile SingularAttribute<ActionOutcome, ItemActivity> activity;
	public static volatile SetAttribute<ActionOutcome, Translation> translations;
	public static volatile SingularAttribute<ActionOutcome, String> description;
	public static volatile SingularAttribute<ActionOutcome, Boolean> active;
	public static volatile SingularAttribute<ActionOutcome, Integer> id;

	public static final String DEFAULT_OUTCOME = "defaultOutcome";
	public static final String POSITIVE_OUTCOME = "positiveOutcome";
	public static final String ACTIVITY = "activity";
	public static final String TRANSLATIONS = "translations";
	public static final String DESCRIPTION = "description";
	public static final String ACTIVE = "active";
	public static final String ID = "id";

}

