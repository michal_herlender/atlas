package org.trescal.cwms.core.workflow.entity.actionoutcome;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_Adjustment;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ActionOutcome_Adjustment.class)
public abstract class ActionOutcome_Adjustment_ extends org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome_ {

	public static volatile SingularAttribute<ActionOutcome_Adjustment, ActionOutcomeValue_Adjustment> value;

	public static final String VALUE = "value";

}

