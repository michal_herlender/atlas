package org.trescal.cwms.core.workflow.entity.workassignment;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(WorkAssignment.class)
public abstract class WorkAssignment_ {

	public static volatile SingularAttribute<WorkAssignment, Integer> stateGroupId;
	public static volatile SingularAttribute<WorkAssignment, DepartmentType> deptType;
	public static volatile SingularAttribute<WorkAssignment, Integer> id;

	public static final String STATE_GROUP_ID = "stateGroupId";
	public static final String DEPT_TYPE = "deptType";
	public static final String ID = "id";

}

