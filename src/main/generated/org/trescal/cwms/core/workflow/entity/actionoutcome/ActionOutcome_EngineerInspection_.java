package org.trescal.cwms.core.workflow.entity.actionoutcome;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_EngineerInspection;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ActionOutcome_EngineerInspection.class)
public abstract class ActionOutcome_EngineerInspection_ extends org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome_ {

	public static volatile SingularAttribute<ActionOutcome_EngineerInspection, ActionOutcomeValue_EngineerInspection> value;

	public static final String VALUE = "value";

}

