package org.trescal.cwms.core.instrumentmodel.dimensional.entity.wire;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.thread.Thread;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Wire.class)
public abstract class Wire_ {

	public static volatile SingularAttribute<Wire, BigDecimal> baPitch;
	public static volatile SingularAttribute<Wire, BigDecimal> unifiedTPI;
	public static volatile SingularAttribute<Wire, BigDecimal> whitworthTPI;
	public static volatile SingularAttribute<Wire, BigDecimal> ballNo;
	public static volatile SingularAttribute<Wire, BigDecimal> eoWidth;
	public static volatile SingularAttribute<Wire, BigDecimal> eoUnified;
	public static volatile SingularAttribute<Wire, BigDecimal> metricPitch;
	public static volatile SetAttribute<Wire, Thread> threads;
	public static volatile SingularAttribute<Wire, BigDecimal> wire1;
	public static volatile SingularAttribute<Wire, BigDecimal> wire2;
	public static volatile SingularAttribute<Wire, BigDecimal> eoISO;
	public static volatile SingularAttribute<Wire, String> designation;
	public static volatile SingularAttribute<Wire, Integer> id;
	public static volatile SingularAttribute<Wire, BigDecimal> rubyBall;
	public static volatile SingularAttribute<Wire, BigDecimal> wireMean;
	public static volatile SingularAttribute<Wire, BigDecimal> ba;

	public static final String BA_PITCH = "baPitch";
	public static final String UNIFIED_TP_I = "unifiedTPI";
	public static final String WHITWORTH_TP_I = "whitworthTPI";
	public static final String BALL_NO = "ballNo";
	public static final String EO_WIDTH = "eoWidth";
	public static final String EO_UNIFIED = "eoUnified";
	public static final String METRIC_PITCH = "metricPitch";
	public static final String THREADS = "threads";
	public static final String WIRE1 = "wire1";
	public static final String WIRE2 = "wire2";
	public static final String EO_IS_O = "eoISO";
	public static final String DESIGNATION = "designation";
	public static final String ID = "id";
	public static final String RUBY_BALL = "rubyBall";
	public static final String WIRE_MEAN = "wireMean";
	public static final String BA = "ba";

}

