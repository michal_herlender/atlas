package org.trescal.cwms.core.instrumentmodel.entity.description;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.CharacteristicDescription;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Description.class)
public abstract class Description_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SetAttribute<Description, InstrumentModel> models;
	public static volatile SingularAttribute<Description, Integer> typology;
	public static volatile SetAttribute<Description, CharacteristicDescription> characteristicDescriptions;
	public static volatile SetAttribute<Description, Translation> translations;
	public static volatile SingularAttribute<Description, String> description;
	public static volatile SingularAttribute<Description, Boolean> active;
	public static volatile SingularAttribute<Description, Integer> id;
	public static volatile SingularAttribute<Description, Integer> tmlid;
	public static volatile SingularAttribute<Description, InstrumentModelFamily> family;

	public static final String MODELS = "models";
	public static final String TYPOLOGY = "typology";
	public static final String CHARACTERISTIC_DESCRIPTIONS = "characteristicDescriptions";
	public static final String TRANSLATIONS = "translations";
	public static final String DESCRIPTION = "description";
	public static final String ACTIVE = "active";
	public static final String ID = "id";
	public static final String TMLID = "tmlid";
	public static final String FAMILY = "family";

}

