package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstrumentModelType.class)
public abstract class InstrumentModelType_ {

	public static volatile SetAttribute<InstrumentModelType, InstrumentModel> models;
	public static volatile SingularAttribute<InstrumentModelType, String> modelTypeName;
	public static volatile SingularAttribute<InstrumentModelType, Boolean> active;
	public static volatile SingularAttribute<InstrumentModelType, Boolean> salescategory;
	public static volatile SingularAttribute<InstrumentModelType, Boolean> modules;
	public static volatile SingularAttribute<InstrumentModelType, Boolean> undefined;
	public static volatile SetAttribute<InstrumentModelType, Translation> modelTypeDescTranslation;
	public static volatile SingularAttribute<InstrumentModelType, Boolean> capability;
	public static volatile SingularAttribute<InstrumentModelType, String> modelTypeDesc;
	public static volatile SingularAttribute<InstrumentModelType, Boolean> baseUnits;
	public static volatile SingularAttribute<InstrumentModelType, Boolean> canSelect;
	public static volatile SingularAttribute<InstrumentModelType, Integer> instModelTypeId;
	public static volatile SetAttribute<InstrumentModelType, Translation> modelTypeNameTranslation;
	public static volatile SingularAttribute<InstrumentModelType, Boolean> defaultModelType;

	public static final String MODELS = "models";
	public static final String MODEL_TYPE_NAME = "modelTypeName";
	public static final String ACTIVE = "active";
	public static final String SALESCATEGORY = "salescategory";
	public static final String MODULES = "modules";
	public static final String UNDEFINED = "undefined";
	public static final String MODEL_TYPE_DESC_TRANSLATION = "modelTypeDescTranslation";
	public static final String CAPABILITY = "capability";
	public static final String MODEL_TYPE_DESC = "modelTypeDesc";
	public static final String BASE_UNITS = "baseUnits";
	public static final String CAN_SELECT = "canSelect";
	public static final String INST_MODEL_TYPE_ID = "instModelTypeId";
	public static final String MODEL_TYPE_NAME_TRANSLATION = "modelTypeNameTranslation";
	public static final String DEFAULT_MODEL_TYPE = "defaultModelType";

}

