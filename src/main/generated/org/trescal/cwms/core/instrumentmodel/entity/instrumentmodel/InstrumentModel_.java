package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.documents.images.instrumentmodelimage.InstrumentModelImage;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.hiremodel.HireModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelcomponent.InstrumentModelComponent;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelpartof.InstrumentModelPartOf;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype.InstrumentModelType;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.modelnote.ModelNote;
import org.trescal.cwms.core.instrumentmodel.entity.modelrange.ModelRange;
import org.trescal.cwms.core.instrumentmodel.entity.modelrangecharacteristiclibrary.ModelRangeCharacteristicLibrary;
import org.trescal.cwms.core.instrumentmodel.entity.modelwebresource.ModelWebResource;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.SalesCategory;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.ServiceCapability;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CompanyModelCalReq;
import org.trescal.cwms.core.pricing.catalogprice.entity.CatalogPrice;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstrumentModel.class)
public abstract class InstrumentModel_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SetAttribute<InstrumentModel, InstrumentModelComponent> components;
	public static volatile SetAttribute<InstrumentModel, ModelNote> notes;
	public static volatile ListAttribute<InstrumentModel, ModelRange> ranges;
	public static volatile SetAttribute<InstrumentModel, ServiceCapability> serviceCapabilities;
	public static volatile SingularAttribute<InstrumentModel, Description> description;
	public static volatile ListAttribute<InstrumentModel, ModelRangeCharacteristicLibrary> libraryValues;
	public static volatile SingularAttribute<InstrumentModel, Boolean> quarantined;
	public static volatile SetAttribute<InstrumentModel, HireModel> hireModels;
	public static volatile SetAttribute<InstrumentModel, CatalogPrice> catalogPrices;
	public static volatile SingularAttribute<InstrumentModel, SalesCategory> salesCategory;
	public static volatile SingularAttribute<InstrumentModel, ModelMfrType> modelMfrType;
	public static volatile SetAttribute<InstrumentModel, InstrumentModelPartOf> thisModulesBaseUnits;
	public static volatile ListAttribute<InstrumentModel, CompanyModelCalReq> companyModelCalReq;
	public static volatile SingularAttribute<InstrumentModel, String> model;
	public static volatile SetAttribute<InstrumentModel, ModelWebResource> webResources;
	public static volatile SetAttribute<InstrumentModel, Quotationitem> quoteItems;
	public static volatile SetAttribute<InstrumentModel, InstrumentModelImage> images;
	public static volatile SingularAttribute<InstrumentModel, Integer> modelid;
	public static volatile SingularAttribute<InstrumentModel, Contact> addedBy;
	public static volatile SetAttribute<InstrumentModel, InstrumentModelPartOf> thisBaseUnitsModules;
	public static volatile SingularAttribute<InstrumentModel, InstrumentModelType> modelType;
	public static volatile SingularAttribute<InstrumentModel, LocalDate> addedOn;
	public static volatile SingularAttribute<InstrumentModel, Mfr> mfr;
	public static volatile ListAttribute<InstrumentModel, CompanyModelCalReq> companyModelCalReqs;
	public static volatile SingularAttribute<InstrumentModel, Integer> tmlid;
	public static volatile SetAttribute<InstrumentModel, Translation> nameTranslations;

	public static final String COMPONENTS = "components";
	public static final String NOTES = "notes";
	public static final String RANGES = "ranges";
	public static final String SERVICE_CAPABILITIES = "serviceCapabilities";
	public static final String DESCRIPTION = "description";
	public static final String LIBRARY_VALUES = "libraryValues";
	public static final String QUARANTINED = "quarantined";
	public static final String HIRE_MODELS = "hireModels";
	public static final String CATALOG_PRICES = "catalogPrices";
	public static final String SALES_CATEGORY = "salesCategory";
	public static final String MODEL_MFR_TYPE = "modelMfrType";
	public static final String THIS_MODULES_BASE_UNITS = "thisModulesBaseUnits";
	public static final String COMPANY_MODEL_CAL_REQ = "companyModelCalReq";
	public static final String MODEL = "model";
	public static final String WEB_RESOURCES = "webResources";
	public static final String QUOTE_ITEMS = "quoteItems";
	public static final String IMAGES = "images";
	public static final String MODELID = "modelid";
	public static final String ADDED_BY = "addedBy";
	public static final String THIS_BASE_UNITS_MODULES = "thisBaseUnitsModules";
	public static final String MODEL_TYPE = "modelType";
	public static final String ADDED_ON = "addedOn";
	public static final String MFR = "mfr";
	public static final String COMPANY_MODEL_CAL_REQS = "companyModelCalReqs";
	public static final String TMLID = "tmlid";
	public static final String NAME_TRANSLATIONS = "nameTranslations";

}

