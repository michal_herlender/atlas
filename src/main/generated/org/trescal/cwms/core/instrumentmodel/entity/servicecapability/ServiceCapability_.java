package org.trescal.cwms.core.instrumentmodel.entity.servicecapability;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.CalibrationProcess;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ServiceCapability.class)
public abstract class ServiceCapability_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<ServiceCapability, Capability> capability;
	public static volatile SingularAttribute<ServiceCapability, CalibrationProcess> calibrationProcess;
	public static volatile SingularAttribute<ServiceCapability, CostType> costType;
	public static volatile SingularAttribute<ServiceCapability, CalibrationType> calibrationType;
	public static volatile SingularAttribute<ServiceCapability, String> checksheet;
	public static volatile SingularAttribute<ServiceCapability, WorkInstruction> workInstruction;
	public static volatile SingularAttribute<ServiceCapability, InstrumentModel> instrumentModel;
	public static volatile SingularAttribute<ServiceCapability, Integer> id;

	public static final String CAPABILITY = "capability";
	public static final String CALIBRATION_PROCESS = "calibrationProcess";
	public static final String COST_TYPE = "costType";
	public static final String CALIBRATION_TYPE = "calibrationType";
	public static final String CHECKSHEET = "checksheet";
	public static final String WORK_INSTRUCTION = "workInstruction";
	public static final String INSTRUMENT_MODEL = "instrumentModel";
	public static final String ID = "id";

}

