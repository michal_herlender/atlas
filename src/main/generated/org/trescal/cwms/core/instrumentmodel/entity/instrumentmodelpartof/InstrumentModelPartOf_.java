package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelpartof;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstrumentModelPartOf.class)
public abstract class InstrumentModelPartOf_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<InstrumentModelPartOf, InstrumentModel> module;
	public static volatile SingularAttribute<InstrumentModelPartOf, Integer> id;
	public static volatile SingularAttribute<InstrumentModelPartOf, Boolean> includedByDefault;
	public static volatile SingularAttribute<InstrumentModelPartOf, InstrumentModel> base;

	public static final String MODULE = "module";
	public static final String ID = "id";
	public static final String INCLUDED_BY_DEFAULT = "includedByDefault";
	public static final String BASE = "base";

}

