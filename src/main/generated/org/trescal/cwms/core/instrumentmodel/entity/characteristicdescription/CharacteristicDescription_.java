package org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrument.entity.instrumentcharacteristic.InstrumentCharacteristic;
import org.trescal.cwms.core.instrumentmodel.entity.characteristiclibrary.CharacteristicLibrary;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.uom.UoM;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CharacteristicDescription.class)
public abstract class CharacteristicDescription_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<CharacteristicDescription, String> metadata;
	public static volatile SingularAttribute<CharacteristicDescription, Integer> orderno;
	public static volatile SingularAttribute<CharacteristicDescription, Integer> characteristicTypeInt;
	public static volatile SingularAttribute<CharacteristicDescription, Description> description;
	public static volatile SingularAttribute<CharacteristicDescription, Boolean> active;
	public static volatile SingularAttribute<CharacteristicDescription, Integer> characteristicDescriptionId;
	public static volatile SingularAttribute<CharacteristicDescription, Boolean> required;
	public static volatile SingularAttribute<CharacteristicDescription, Date> log_createdon;
	public static volatile SingularAttribute<CharacteristicDescription, String> internalName;
	public static volatile SingularAttribute<CharacteristicDescription, UoM> uom;
	public static volatile SetAttribute<CharacteristicDescription, Translation> shortNameTranslations;
	public static volatile ListAttribute<CharacteristicDescription, CharacteristicLibrary> library;
	public static volatile SetAttribute<CharacteristicDescription, Translation> translations;
	public static volatile SingularAttribute<CharacteristicDescription, Boolean> partOfKey;
	public static volatile SingularAttribute<CharacteristicDescription, String> name;
	public static volatile SingularAttribute<CharacteristicDescription, Integer> tmlid;
	public static volatile SingularAttribute<CharacteristicDescription, String> shortName;
	public static volatile ListAttribute<CharacteristicDescription, InstrumentCharacteristic> instrumentCharacteristics;

	public static final String METADATA = "metadata";
	public static final String ORDERNO = "orderno";
	public static final String CHARACTERISTIC_TYPE_INT = "characteristicTypeInt";
	public static final String DESCRIPTION = "description";
	public static final String ACTIVE = "active";
	public static final String CHARACTERISTIC_DESCRIPTION_ID = "characteristicDescriptionId";
	public static final String REQUIRED = "required";
	public static final String LOG_CREATEDON = "log_createdon";
	public static final String INTERNAL_NAME = "internalName";
	public static final String UOM = "uom";
	public static final String SHORT_NAME_TRANSLATIONS = "shortNameTranslations";
	public static final String LIBRARY = "library";
	public static final String TRANSLATIONS = "translations";
	public static final String PART_OF_KEY = "partOfKey";
	public static final String NAME = "name";
	public static final String TMLID = "tmlid";
	public static final String SHORT_NAME = "shortName";
	public static final String INSTRUMENT_CHARACTERISTICS = "instrumentCharacteristics";

}

