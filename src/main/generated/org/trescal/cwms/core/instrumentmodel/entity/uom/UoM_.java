package org.trescal.cwms.core.instrumentmodel.entity.uom;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationpoint.CalibrationPoint;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UoM.class)
public abstract class UoM_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<UoM, String> symbol;
	public static volatile SetAttribute<UoM, Translation> nameTranslation;
	public static volatile SingularAttribute<UoM, String> name;
	public static volatile SingularAttribute<UoM, String> description;
	public static volatile SingularAttribute<UoM, Boolean> active;
	public static volatile SingularAttribute<UoM, Integer> id;
	public static volatile SingularAttribute<UoM, Integer> tmlid;
	public static volatile SingularAttribute<UoM, String> shortName;
	public static volatile SetAttribute<UoM, CalibrationPoint> points;

	public static final String SYMBOL = "symbol";
	public static final String NAME_TRANSLATION = "nameTranslation";
	public static final String NAME = "name";
	public static final String DESCRIPTION = "description";
	public static final String ACTIVE = "active";
	public static final String ID = "id";
	public static final String TMLID = "tmlid";
	public static final String SHORT_NAME = "shortName";
	public static final String POINTS = "points";

}

