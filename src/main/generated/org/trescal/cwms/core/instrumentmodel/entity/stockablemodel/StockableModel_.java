package org.trescal.cwms.core.instrumentmodel.entity.stockablemodel;

import java.math.BigDecimal;
import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(StockableModel.class)
public abstract class StockableModel_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SingularAttribute<StockableModel, Contact> stockLastCheckedBy;
	public static volatile SingularAttribute<StockableModel, BigDecimal> salesCost;
	public static volatile SingularAttribute<StockableModel, LocalDate> stockLastCheckedOn;
	public static volatile SingularAttribute<StockableModel, Integer> inStock;
	public static volatile SingularAttribute<StockableModel, Integer> minimumStockLevel;

	public static final String STOCK_LAST_CHECKED_BY = "stockLastCheckedBy";
	public static final String SALES_COST = "salesCost";
	public static final String STOCK_LAST_CHECKED_ON = "stockLastCheckedOn";
	public static final String IN_STOCK = "inStock";
	public static final String MINIMUM_STOCK_LEVEL = "minimumStockLevel";

}

