package org.trescal.cwms.core.instrumentmodel.entity.modelrange;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.RangeType;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.CharacteristicDescription;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.option.Option;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ModelRange.class)
public abstract class ModelRange_ extends org.trescal.cwms.core.instrumentmodel.entity.range.Range_ {

	public static volatile SingularAttribute<ModelRange, Option> modelOption;
	public static volatile SingularAttribute<ModelRange, RangeType> characteristicType;
	public static volatile SingularAttribute<ModelRange, InstrumentModel> model;
	public static volatile SingularAttribute<ModelRange, CharacteristicDescription> characteristicDescription;

	public static final String MODEL_OPTION = "modelOption";
	public static final String CHARACTERISTIC_TYPE = "characteristicType";
	public static final String MODEL = "model";
	public static final String CHARACTERISTIC_DESCRIPTION = "characteristicDescription";

}

