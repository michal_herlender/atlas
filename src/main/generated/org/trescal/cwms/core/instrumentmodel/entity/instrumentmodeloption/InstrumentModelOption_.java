package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeloption;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.option.Option;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstrumentModelOption.class)
public abstract class InstrumentModelOption_ {

	public static volatile SingularAttribute<InstrumentModelOption, Integer> instrumentmodeloptionid;
	public static volatile SingularAttribute<InstrumentModelOption, InstrumentModel> instrumentModel;
	public static volatile SingularAttribute<InstrumentModelOption, Option> option;

	public static final String INSTRUMENTMODELOPTIONID = "instrumentmodeloptionid";
	public static final String INSTRUMENT_MODEL = "instrumentModel";
	public static final String OPTION = "option";

}

