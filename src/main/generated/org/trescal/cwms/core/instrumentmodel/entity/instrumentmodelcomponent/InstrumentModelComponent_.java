package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelcomponent;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.entity.component.Component;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstrumentModelComponent.class)
public abstract class InstrumentModelComponent_ {

	public static volatile SingularAttribute<InstrumentModelComponent, Component> component;
	public static volatile SingularAttribute<InstrumentModelComponent, Boolean> includeByDefault;
	public static volatile SingularAttribute<InstrumentModelComponent, InstrumentModel> model;
	public static volatile SingularAttribute<InstrumentModelComponent, Integer> id;

	public static final String COMPONENT = "component";
	public static final String INCLUDE_BY_DEFAULT = "includeByDefault";
	public static final String MODEL = "model";
	public static final String ID = "id";

}

