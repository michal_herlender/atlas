package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelsubfamily;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstrumentModelSubfamily.class)
public abstract class InstrumentModelSubfamily_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<InstrumentModelSubfamily, Integer> subfamilyid;
	public static volatile SetAttribute<InstrumentModelSubfamily, Translation> translation;
	public static volatile SingularAttribute<InstrumentModelSubfamily, String> name;
	public static volatile SingularAttribute<InstrumentModelSubfamily, Boolean> active;
	public static volatile SingularAttribute<InstrumentModelSubfamily, Integer> tmlid;
	public static volatile SingularAttribute<InstrumentModelSubfamily, InstrumentModelFamily> family;

	public static final String SUBFAMILYID = "subfamilyid";
	public static final String TRANSLATION = "translation";
	public static final String NAME = "name";
	public static final String ACTIVE = "active";
	public static final String TMLID = "tmlid";
	public static final String FAMILY = "family";

}

