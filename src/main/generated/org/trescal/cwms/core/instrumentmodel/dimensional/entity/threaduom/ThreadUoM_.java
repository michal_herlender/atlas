package org.trescal.cwms.core.instrumentmodel.dimensional.entity.threaduom;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.thread.Thread;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ThreadUoM.class)
public abstract class ThreadUoM_ {

	public static volatile SingularAttribute<ThreadUoM, String> description;
	public static volatile SetAttribute<ThreadUoM, Thread> threads;
	public static volatile SingularAttribute<ThreadUoM, Integer> id;

	public static final String DESCRIPTION = "description";
	public static final String THREADS = "threads";
	public static final String ID = "id";

}

