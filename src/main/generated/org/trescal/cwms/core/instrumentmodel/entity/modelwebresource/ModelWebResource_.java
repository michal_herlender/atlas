package org.trescal.cwms.core.instrumentmodel.entity.modelwebresource;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ModelWebResource.class)
public abstract class ModelWebResource_ extends org.trescal.cwms.core.system.entity.webresource.WebResource_ {

	public static volatile SingularAttribute<ModelWebResource, InstrumentModel> model;

	public static final String MODEL = "model";

}

