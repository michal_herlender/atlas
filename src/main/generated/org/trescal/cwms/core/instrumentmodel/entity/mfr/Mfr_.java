package org.trescal.cwms.core.instrumentmodel.entity.mfr;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.entity.component.Component;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.mfrwebresource.MfrWebResource;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Mfr.class)
public abstract class Mfr_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SetAttribute<Mfr, InstrumentModel> models;
	public static volatile SetAttribute<Mfr, Component> components;
	public static volatile SingularAttribute<Mfr, Boolean> genericMfr;
	public static volatile SingularAttribute<Mfr, Integer> mfrid;
	public static volatile SingularAttribute<Mfr, String> name;
	public static volatile SingularAttribute<Mfr, Boolean> active;
	public static volatile SetAttribute<Mfr, MfrWebResource> webResources;
	public static volatile SingularAttribute<Mfr, Integer> tmlid;

	public static final String MODELS = "models";
	public static final String COMPONENTS = "components";
	public static final String GENERIC_MFR = "genericMfr";
	public static final String MFRID = "mfrid";
	public static final String NAME = "name";
	public static final String ACTIVE = "active";
	public static final String WEB_RESOURCES = "webResources";
	public static final String TMLID = "tmlid";

}

