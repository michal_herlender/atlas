package org.trescal.cwms.core.instrumentmodel.entity.mfrwebresource;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(MfrWebResource.class)
public abstract class MfrWebResource_ extends org.trescal.cwms.core.system.entity.webresource.WebResource_ {

	public static volatile SingularAttribute<MfrWebResource, Mfr> mfr;

	public static final String MFR = "mfr";

}

