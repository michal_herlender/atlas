package org.trescal.cwms.core.instrumentmodel.entity.characteristiclibrary;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.CharacteristicDescription;
import org.trescal.cwms.core.instrumentmodel.entity.modelrangecharacteristiclibrary.ModelRangeCharacteristicLibrary;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CharacteristicLibrary.class)
public abstract class CharacteristicLibrary_ {

	public static volatile SingularAttribute<CharacteristicLibrary, Date> log_createdon;
	public static volatile SingularAttribute<CharacteristicLibrary, String> name;
	public static volatile SetAttribute<CharacteristicLibrary, Translation> translation;
	public static volatile SingularAttribute<CharacteristicLibrary, Boolean> active;
	public static volatile SingularAttribute<CharacteristicLibrary, Integer> tmlid;
	public static volatile SetAttribute<CharacteristicLibrary, ModelRangeCharacteristicLibrary> modelRangeCharacteristicLibraries;
	public static volatile SingularAttribute<CharacteristicLibrary, Integer> characteristiclibraryid;
	public static volatile SingularAttribute<CharacteristicLibrary, CharacteristicDescription> characteristicDescription;

	public static final String LOG_CREATEDON = "log_createdon";
	public static final String NAME = "name";
	public static final String TRANSLATION = "translation";
	public static final String ACTIVE = "active";
	public static final String TMLID = "tmlid";
	public static final String MODEL_RANGE_CHARACTERISTIC_LIBRARIES = "modelRangeCharacteristicLibraries";
	public static final String CHARACTERISTICLIBRARYID = "characteristiclibraryid";
	public static final String CHARACTERISTIC_DESCRIPTION = "characteristicDescription";

}

