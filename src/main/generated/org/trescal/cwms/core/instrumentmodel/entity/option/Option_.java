package org.trescal.cwms.core.instrumentmodel.entity.option;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.entity.modelrange.ModelRange;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Option.class)
public abstract class Option_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<Option, Date> log_createdon;
	public static volatile SingularAttribute<Option, String> code;
	public static volatile SetAttribute<Option, Translation> translation;
	public static volatile SingularAttribute<Option, String> name;
	public static volatile SingularAttribute<Option, Integer> modeltmlid;
	public static volatile SingularAttribute<Option, Boolean> active;
	public static volatile SingularAttribute<Option, Integer> optionid;
	public static volatile SingularAttribute<Option, Integer> tmlid;
	public static volatile SetAttribute<Option, ModelRange> modelRanges;

	public static final String LOG_CREATEDON = "log_createdon";
	public static final String CODE = "code";
	public static final String TRANSLATION = "translation";
	public static final String NAME = "name";
	public static final String MODELTMLID = "modeltmlid";
	public static final String ACTIVE = "active";
	public static final String OPTIONID = "optionid";
	public static final String TMLID = "tmlid";
	public static final String MODEL_RANGES = "modelRanges";

}

