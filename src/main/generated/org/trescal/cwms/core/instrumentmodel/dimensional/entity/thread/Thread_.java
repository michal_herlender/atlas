package org.trescal.cwms.core.instrumentmodel.dimensional.entity.thread;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.cylindricalstandard.CylindricalStandard;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.threadtype.ThreadType;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.threaduom.ThreadUoM;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.wire.Wire;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Thread.class)
public abstract class Thread_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<Thread, BigDecimal> depthStepLower;
	public static volatile SingularAttribute<Thread, BigDecimal> goLowerLimit;
	public static volatile SingularAttribute<Thread, BigDecimal> notGoEffective;
	public static volatile SingularAttribute<Thread, BigDecimal> wornLimit;
	public static volatile SingularAttribute<Thread, ThreadType> type;
	public static volatile SingularAttribute<Thread, BigDecimal> notGoWornLimit;
	public static volatile SingularAttribute<Thread, BigDecimal> ball;
	public static volatile SingularAttribute<Thread, BigDecimal> overallLengthUpper;
	public static volatile SingularAttribute<Thread, BigDecimal> SDatum;
	public static volatile SingularAttribute<Thread, ThreadUoM> uom;
	public static volatile SetAttribute<Thread, Instrument> instruments;
	public static volatile SingularAttribute<Thread, BigDecimal> notGoLowerLimit;
	public static volatile SingularAttribute<Thread, BigDecimal> minStep;
	public static volatile SingularAttribute<Thread, Integer> id;
	public static volatile SingularAttribute<Thread, BigDecimal> pitch;
	public static volatile SingularAttribute<Thread, BigDecimal> lengthToGaugePlaneLower;
	public static volatile SingularAttribute<Thread, BigDecimal> maxStep;
	public static volatile SingularAttribute<Thread, BigDecimal> notGoUpperLimit;
	public static volatile SingularAttribute<Thread, BigDecimal> depthStepUpper;
	public static volatile SingularAttribute<Thread, BigDecimal> goEffectiveDiameter;
	public static volatile SingularAttribute<Thread, BigDecimal> goWornLimit;
	public static volatile SingularAttribute<Thread, BigDecimal> lead;
	public static volatile SingularAttribute<Thread, BigDecimal> overallLengthB;
	public static volatile SingularAttribute<Thread, BigDecimal> depthStep;
	public static volatile SingularAttribute<Thread, BigDecimal> overallLengthLower;
	public static volatile SingularAttribute<Thread, BigDecimal> overallLength;
	public static volatile SingularAttribute<Thread, Wire> wire;
	public static volatile SingularAttribute<Thread, String> size;
	public static volatile SingularAttribute<Thread, Integer> noStarts;
	public static volatile SingularAttribute<Thread, BigDecimal> goUpperLimit;
	public static volatile SingularAttribute<Thread, BigDecimal> lengthToGaugePlane;
	public static volatile SingularAttribute<Thread, CylindricalStandard> cylindricalStandard;
	public static volatile SingularAttribute<Thread, BigDecimal> taperLimit;
	public static volatile SingularAttribute<Thread, BigDecimal> lengthToGaugePlaneUpper;

	public static final String DEPTH_STEP_LOWER = "depthStepLower";
	public static final String GO_LOWER_LIMIT = "goLowerLimit";
	public static final String NOT_GO_EFFECTIVE = "notGoEffective";
	public static final String WORN_LIMIT = "wornLimit";
	public static final String TYPE = "type";
	public static final String NOT_GO_WORN_LIMIT = "notGoWornLimit";
	public static final String BALL = "ball";
	public static final String OVERALL_LENGTH_UPPER = "overallLengthUpper";
	public static final String S_DATUM = "SDatum";
	public static final String UOM = "uom";
	public static final String INSTRUMENTS = "instruments";
	public static final String NOT_GO_LOWER_LIMIT = "notGoLowerLimit";
	public static final String MIN_STEP = "minStep";
	public static final String ID = "id";
	public static final String PITCH = "pitch";
	public static final String LENGTH_TO_GAUGE_PLANE_LOWER = "lengthToGaugePlaneLower";
	public static final String MAX_STEP = "maxStep";
	public static final String NOT_GO_UPPER_LIMIT = "notGoUpperLimit";
	public static final String DEPTH_STEP_UPPER = "depthStepUpper";
	public static final String GO_EFFECTIVE_DIAMETER = "goEffectiveDiameter";
	public static final String GO_WORN_LIMIT = "goWornLimit";
	public static final String LEAD = "lead";
	public static final String OVERALL_LENGTH_B = "overallLengthB";
	public static final String DEPTH_STEP = "depthStep";
	public static final String OVERALL_LENGTH_LOWER = "overallLengthLower";
	public static final String OVERALL_LENGTH = "overallLength";
	public static final String WIRE = "wire";
	public static final String SIZE = "size";
	public static final String NO_STARTS = "noStarts";
	public static final String GO_UPPER_LIMIT = "goUpperLimit";
	public static final String LENGTH_TO_GAUGE_PLANE = "lengthToGaugePlane";
	public static final String CYLINDRICAL_STANDARD = "cylindricalStandard";
	public static final String TAPER_LIMIT = "taperLimit";
	public static final String LENGTH_TO_GAUGE_PLANE_UPPER = "lengthToGaugePlaneUpper";

}

