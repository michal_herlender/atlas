package org.trescal.cwms.core.instrumentmodel.dimensional.entity.threadtype;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.thread.Thread;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ThreadType.class)
public abstract class ThreadType_ {

	public static volatile SetAttribute<ThreadType, Thread> threads;
	public static volatile SingularAttribute<ThreadType, Integer> id;
	public static volatile SingularAttribute<ThreadType, String> type;

	public static final String THREADS = "threads";
	public static final String ID = "id";
	public static final String TYPE = "type";

}

