package org.trescal.cwms.core.instrumentmodel.entity.component;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.entity.componentdescription.ComponentDescription;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelcomponent.InstrumentModelComponent;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Component.class)
public abstract class Component_ extends org.trescal.cwms.core.instrumentmodel.entity.stockablemodel.StockableModel_ {

	public static volatile SetAttribute<Component, InstrumentModelComponent> modelComponents;
	public static volatile SingularAttribute<Component, Mfr> mfr;
	public static volatile SingularAttribute<Component, ComponentDescription> description;
	public static volatile SingularAttribute<Component, Integer> id;

	public static final String MODEL_COMPONENTS = "modelComponents";
	public static final String MFR = "mfr";
	public static final String DESCRIPTION = "description";
	public static final String ID = "id";

}

