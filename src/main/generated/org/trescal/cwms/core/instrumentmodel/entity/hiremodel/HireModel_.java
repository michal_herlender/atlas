package org.trescal.cwms.core.instrumentmodel.entity.hiremodel;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.hire.entity.hiremodelincategory.HireModelInCategory;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(HireModel.class)
public abstract class HireModel_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<HireModel, BigDecimal> hireCost;
	public static volatile SingularAttribute<HireModel, BigDecimal> purchaseCost;
	public static volatile SingularAttribute<HireModel, InstrumentModel> instmodel;
	public static volatile SetAttribute<HireModel, HireModelInCategory> hireModelInCategories;
	public static volatile SingularAttribute<HireModel, String> hireModelDetail;
	public static volatile SingularAttribute<HireModel, Integer> id;

	public static final String HIRE_COST = "hireCost";
	public static final String PURCHASE_COST = "purchaseCost";
	public static final String INSTMODEL = "instmodel";
	public static final String HIRE_MODEL_IN_CATEGORIES = "hireModelInCategories";
	public static final String HIRE_MODEL_DETAIL = "hireModelDetail";
	public static final String ID = "id";

}

