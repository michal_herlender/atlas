package org.trescal.cwms.core.instrumentmodel.entity.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstrumentModelDomain.class)
public abstract class InstrumentModelDomain_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SingularAttribute<InstrumentModelDomain, DomainType> domainType;
	public static volatile SetAttribute<InstrumentModelDomain, Translation> translation;
	public static volatile SingularAttribute<InstrumentModelDomain, String> name;
	public static volatile SingularAttribute<InstrumentModelDomain, Boolean> active;
	public static volatile SingularAttribute<InstrumentModelDomain, Integer> tmlid;
	public static volatile SingularAttribute<InstrumentModelDomain, Integer> domainid;

	public static final String DOMAIN_TYPE = "domainType";
	public static final String TRANSLATION = "translation";
	public static final String NAME = "name";
	public static final String ACTIVE = "active";
	public static final String TMLID = "tmlid";
	public static final String DOMAINID = "domainid";

}

