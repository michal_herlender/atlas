package org.trescal.cwms.core.instrumentmodel.dimensional.entity.cylindricalstandard;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.thread.Thread;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CylindricalStandard.class)
public abstract class CylindricalStandard_ {

	public static volatile SingularAttribute<CylindricalStandard, BigDecimal> sizeImperial;
	public static volatile SetAttribute<CylindricalStandard, Thread> threads;
	public static volatile SingularAttribute<CylindricalStandard, Double> id;
	public static volatile SingularAttribute<CylindricalStandard, BigDecimal> sizeMetric;

	public static final String SIZE_IMPERIAL = "sizeImperial";
	public static final String THREADS = "threads";
	public static final String ID = "id";
	public static final String SIZE_METRIC = "sizeMetric";

}

