package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.entity.domain.InstrumentModelDomain;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstrumentModelFamily.class)
public abstract class InstrumentModelFamily_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SingularAttribute<InstrumentModelFamily, Integer> familyid;
	public static volatile SingularAttribute<InstrumentModelFamily, InstrumentModelDomain> domain;
	public static volatile SetAttribute<InstrumentModelFamily, Translation> translation;
	public static volatile SingularAttribute<InstrumentModelFamily, String> name;
	public static volatile SingularAttribute<InstrumentModelFamily, Boolean> active;
	public static volatile SingularAttribute<InstrumentModelFamily, Integer> tmlid;

	public static final String FAMILYID = "familyid";
	public static final String DOMAIN = "domain";
	public static final String TRANSLATION = "translation";
	public static final String NAME = "name";
	public static final String ACTIVE = "active";
	public static final String TMLID = "tmlid";

}

