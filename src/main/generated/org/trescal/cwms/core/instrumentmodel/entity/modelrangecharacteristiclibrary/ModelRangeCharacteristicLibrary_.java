package org.trescal.cwms.core.instrumentmodel.entity.modelrangecharacteristiclibrary;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.entity.characteristiclibrary.CharacteristicLibrary;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ModelRangeCharacteristicLibrary.class)
public abstract class ModelRangeCharacteristicLibrary_ {

	public static volatile SingularAttribute<ModelRangeCharacteristicLibrary, InstrumentModel> model;
	public static volatile SingularAttribute<ModelRangeCharacteristicLibrary, CharacteristicLibrary> characteristicLibrary;
	public static volatile SingularAttribute<ModelRangeCharacteristicLibrary, Integer> modelrangecharacteristiclibraryid;

	public static final String MODEL = "model";
	public static final String CHARACTERISTIC_LIBRARY = "characteristicLibrary";
	public static final String MODELRANGECHARACTERISTICLIBRARYID = "modelrangecharacteristiclibraryid";

}

