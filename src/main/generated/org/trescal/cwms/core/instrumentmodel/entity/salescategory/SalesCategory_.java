package org.trescal.cwms.core.instrumentmodel.entity.salescategory;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SalesCategory.class)
public abstract class SalesCategory_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SetAttribute<SalesCategory, Translation> translations;
	public static volatile SingularAttribute<SalesCategory, Boolean> active;
	public static volatile SingularAttribute<SalesCategory, Integer> id;

	public static final String TRANSLATIONS = "translations";
	public static final String ACTIVE = "active";
	public static final String ID = "id";

}

