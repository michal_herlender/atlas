package org.trescal.cwms.core.instrumentmodel.entity.componentdescription;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.entity.component.Component;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ComponentDescription.class)
public abstract class ComponentDescription_ {

	public static volatile SetAttribute<ComponentDescription, Component> components;
	public static volatile SingularAttribute<ComponentDescription, String> description;
	public static volatile SingularAttribute<ComponentDescription, Integer> id;

	public static final String COMPONENTS = "components";
	public static final String DESCRIPTION = "description";
	public static final String ID = "id";

}

