package org.trescal.cwms.core.instrumentmodel.entity.range;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.entity.uom.UoM;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Range.class)
public abstract class Range_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<Range, UoM> maxUom;
	public static volatile SingularAttribute<Range, UoM> uom;
	public static volatile SingularAttribute<Range, String> maxvalue;
	public static volatile SingularAttribute<Range, Double> start;
	public static volatile SingularAttribute<Range, Double> end;
	public static volatile SingularAttribute<Range, Integer> id;
	public static volatile SingularAttribute<Range, Boolean> maxIsInfinite;
	public static volatile SingularAttribute<Range, String> minvalue;

	public static final String MAX_UOM = "maxUom";
	public static final String UOM = "uom";
	public static final String MAXVALUE = "maxvalue";
	public static final String START = "start";
	public static final String END = "end";
	public static final String ID = "id";
	public static final String MAX_IS_INFINITE = "maxIsInfinite";
	public static final String MINVALUE = "minvalue";

}

