package org.trescal.cwms.core.jobs.job.entity.po;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitempo.JobExpenseItemPO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.JobItemPO;
import org.trescal.cwms.core.logistics.entity.prebookingdetails.PrebookingJobDetails;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoiceItemPO;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PO.class)
public abstract class PO_ extends org.trescal.cwms.core.jobs.job.entity.abstractpo.AbstractPO_ {

	public static volatile SingularAttribute<PO, Job> defaultForJob;
	public static volatile ListAttribute<PO, JobExpenseItemPO> expenseItemPOs;
	public static volatile SingularAttribute<PO, PrebookingJobDetails> defaultForPrebooking;
	public static volatile ListAttribute<PO, JobItemPO> jobItemPOs;
	public static volatile SingularAttribute<PO, Boolean> active;
	public static volatile SingularAttribute<PO, PrebookingJobDetails> prebooking;
	public static volatile SingularAttribute<PO, Date> deactivatedTime;
	public static volatile SetAttribute<PO, InvoiceItemPO> invItemPOs;
	public static volatile SingularAttribute<PO, Job> job;
	public static volatile SingularAttribute<PO, Contact> deactivatedBy;

	public static final String DEFAULT_FOR_JOB = "defaultForJob";
	public static final String EXPENSE_ITEM_POS = "expenseItemPOs";
	public static final String DEFAULT_FOR_PREBOOKING = "defaultForPrebooking";
	public static final String JOB_ITEM_POS = "jobItemPOs";
	public static final String ACTIVE = "active";
	public static final String PREBOOKING = "prebooking";
	public static final String DEACTIVATED_TIME = "deactivatedTime";
	public static final String INV_ITEM_POS = "invItemPOs";
	public static final String JOB = "job";
	public static final String DEACTIVATED_BY = "deactivatedBy";

}

