package org.trescal.cwms.core.jobs.contractreview.entity.contractreview;

import java.util.BitSet;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreviewitem.ContractReviewItem;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.logistics.entity.prebookingdetails.PrebookingJobDetails;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ContractReview.class)
public abstract class ContractReview_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<ContractReview, Contact> reviewBy;
	public static volatile SingularAttribute<ContractReview, PrebookingJobDetails> prebookingJobDetails;
	public static volatile SingularAttribute<ContractReview, String> comments;
	public static volatile SingularAttribute<ContractReview, Date> reviewDate;
	public static volatile SingularAttribute<ContractReview, Integer> id;
	public static volatile SingularAttribute<ContractReview, BitSet> demandSet;
	public static volatile SingularAttribute<ContractReview, Job> job;
	public static volatile SetAttribute<ContractReview, ContractReviewItem> items;

	public static final String REVIEW_BY = "reviewBy";
	public static final String PREBOOKING_JOB_DETAILS = "prebookingJobDetails";
	public static final String COMMENTS = "comments";
	public static final String REVIEW_DATE = "reviewDate";
	public static final String ID = "id";
	public static final String DEMAND_SET = "demandSet";
	public static final String JOB = "job";
	public static final String ITEMS = "items";

}

