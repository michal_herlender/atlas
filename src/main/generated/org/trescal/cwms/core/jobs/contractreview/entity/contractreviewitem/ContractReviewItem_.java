package org.trescal.cwms.core.jobs.contractreview.entity.contractreviewitem;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreview.ContractReview;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ContractReviewItem.class)
public abstract class ContractReviewItem_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<ContractReviewItem, JobItem> jobitem;
	public static volatile SingularAttribute<ContractReviewItem, ContractReview> review;
	public static volatile SingularAttribute<ContractReviewItem, Integer> id;

	public static final String JOBITEM = "jobitem";
	public static final String REVIEW = "review";
	public static final String ID = "id";

}

