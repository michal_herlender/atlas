package org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitempo;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.po.PO;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AbstractJobItemPO.class)
public abstract class AbstractJobItemPO_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SingularAttribute<AbstractJobItemPO, BPO> bpo;
	public static volatile SingularAttribute<AbstractJobItemPO, Object> item;
	public static volatile SingularAttribute<AbstractJobItemPO, Integer> id;
	public static volatile SingularAttribute<AbstractJobItemPO, PO> po;

	public static final String BPO = "bpo";
	public static final String ITEM = "item";
	public static final String ID = "id";
	public static final String PO = "po";

}

