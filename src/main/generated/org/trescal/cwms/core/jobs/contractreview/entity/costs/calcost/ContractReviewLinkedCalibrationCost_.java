package org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingCalibrationCost;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ContractReviewLinkedCalibrationCost.class)
public abstract class ContractReviewLinkedCalibrationCost_ extends org.trescal.cwms.core.jobs.contractreview.entity.costs.ContractReviewLinkedCost_ {

	public static volatile SingularAttribute<ContractReviewLinkedCalibrationCost, JobCostingCalibrationCost> jobCostingCalibrationCost;
	public static volatile SingularAttribute<ContractReviewLinkedCalibrationCost, ContractReviewCalibrationCost> calibrationCost;
	public static volatile SingularAttribute<ContractReviewLinkedCalibrationCost, QuotationCalibrationCost> quotationCalibrationCost;

	public static final String JOB_COSTING_CALIBRATION_COST = "jobCostingCalibrationCost";
	public static final String CALIBRATION_COST = "calibrationCost";
	public static final String QUOTATION_CALIBRATION_COST = "quotationCalibrationCost";

}

