package org.trescal.cwms.core.jobs.calibration.entity.queuedcalibration;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(QueuedCalibration.class)
public abstract class QueuedCalibration_ {

	public static volatile SingularAttribute<QueuedCalibration, Calibration> queuedCal;
	public static volatile SingularAttribute<QueuedCalibration, Boolean> printNow;
	public static volatile SingularAttribute<QueuedCalibration, Boolean> reprint;
	public static volatile SingularAttribute<QueuedCalibration, Date> timeCompleted;
	public static volatile SingularAttribute<QueuedCalibration, Date> timeQueued;
	public static volatile SingularAttribute<QueuedCalibration, Contact> printedBy;
	public static volatile SingularAttribute<QueuedCalibration, Boolean> active;
	public static volatile SingularAttribute<QueuedCalibration, Integer> certId;
	public static volatile SingularAttribute<QueuedCalibration, Integer> id;

	public static final String QUEUED_CAL = "queuedCal";
	public static final String PRINT_NOW = "printNow";
	public static final String REPRINT = "reprint";
	public static final String TIME_COMPLETED = "timeCompleted";
	public static final String TIME_QUEUED = "timeQueued";
	public static final String PRINTED_BY = "printedBy";
	public static final String ACTIVE = "active";
	public static final String CERT_ID = "certId";
	public static final String ID = "id";

}

