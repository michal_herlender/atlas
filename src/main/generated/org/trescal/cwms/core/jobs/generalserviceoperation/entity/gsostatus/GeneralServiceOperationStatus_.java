package org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsostatus;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.GeneralServiceOperation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(GeneralServiceOperationStatus.class)
public abstract class GeneralServiceOperationStatus_ extends org.trescal.cwms.core.system.entity.status.Status_ {

	public static volatile ListAttribute<GeneralServiceOperationStatus, GeneralServiceOperation> generalServiceOperations;

	public static final String GENERAL_SERVICE_OPERATIONS = "generalServiceOperations";

}

