package org.trescal.cwms.core.jobs.calibration.entity.pointsettemplate;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.calibration.entity.templatepoint.TemplatePoint;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PointSetTemplate.class)
public abstract class PointSetTemplate_ extends org.trescal.cwms.core.jobs.calibration.entity.pointset.PointSet_ {

	public static volatile SingularAttribute<PointSetTemplate, Date> setOn;
	public static volatile SingularAttribute<PointSetTemplate, String> description;
	public static volatile SingularAttribute<PointSetTemplate, String> title;
	public static volatile SingularAttribute<PointSetTemplate, Contact> setBy;
	public static volatile SetAttribute<PointSetTemplate, TemplatePoint> points;

	public static final String SET_ON = "setOn";
	public static final String DESCRIPTION = "description";
	public static final String TITLE = "title";
	public static final String SET_BY = "setBy";
	public static final String POINTS = "points";

}

