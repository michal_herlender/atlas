package org.trescal.cwms.core.jobs.job.entity.jobtypeservicetype;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobTypeServiceType.class)
public abstract class JobTypeServiceType_ {

	public static volatile SingularAttribute<JobTypeServiceType, ServiceType> serviceType;
	public static volatile SingularAttribute<JobTypeServiceType, Boolean> defaultValue;
	public static volatile SingularAttribute<JobTypeServiceType, Integer> id;
	public static volatile SingularAttribute<JobTypeServiceType, JobType> jobType;

	public static final String SERVICE_TYPE = "serviceType";
	public static final String DEFAULT_VALUE = "defaultValue";
	public static final String ID = "id";
	public static final String JOB_TYPE = "jobType";

}

