package org.trescal.cwms.core.jobs.calibration.entity.calibrationrange;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.entity.uom.UoM;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CalibrationRange.class)
public abstract class CalibrationRange_ extends org.trescal.cwms.core.instrumentmodel.entity.range.Range_ {

	public static volatile SingularAttribute<CalibrationRange, UoM> relatedUom;
	public static volatile SingularAttribute<CalibrationRange, BigDecimal> relatedPoint;

	public static final String RELATED_UOM = "relatedUom";
	public static final String RELATED_POINT = "relatedPoint";

}

