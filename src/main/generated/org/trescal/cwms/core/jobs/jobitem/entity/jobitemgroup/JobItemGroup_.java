package org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.GroupAccessory;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobItemGroup.class)
public abstract class JobItemGroup_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<JobItemGroup, String> accessoryFreeText;
	public static volatile SetAttribute<JobItemGroup, GroupAccessory> accessories;
	public static volatile SingularAttribute<JobItemGroup, Boolean> calibrationGroup;
	public static volatile SingularAttribute<JobItemGroup, Integer> id;
	public static volatile SingularAttribute<JobItemGroup, Boolean> deliveryGroup;
	public static volatile SingularAttribute<JobItemGroup, Job> job;
	public static volatile SetAttribute<JobItemGroup, JobItem> items;

	public static final String ACCESSORY_FREE_TEXT = "accessoryFreeText";
	public static final String ACCESSORIES = "accessories";
	public static final String CALIBRATION_GROUP = "calibrationGroup";
	public static final String ID = "id";
	public static final String DELIVERY_GROUP = "deliveryGroup";
	public static final String JOB = "job";
	public static final String ITEMS = "items";

}

