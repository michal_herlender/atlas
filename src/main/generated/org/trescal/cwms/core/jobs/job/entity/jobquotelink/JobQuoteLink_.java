package org.trescal.cwms.core.jobs.job.entity.jobquotelink;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobQuoteLink.class)
public abstract class JobQuoteLink_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<JobQuoteLink, Contact> linkedBy;
	public static volatile SingularAttribute<JobQuoteLink, Integer> id;
	public static volatile SingularAttribute<JobQuoteLink, Job> job;
	public static volatile SingularAttribute<JobQuoteLink, Date> linkedOn;
	public static volatile SingularAttribute<JobQuoteLink, Quotation> quotation;

	public static final String LINKED_BY = "linkedBy";
	public static final String ID = "id";
	public static final String JOB = "job";
	public static final String LINKED_ON = "linkedOn";
	public static final String QUOTATION = "quotation";

}

