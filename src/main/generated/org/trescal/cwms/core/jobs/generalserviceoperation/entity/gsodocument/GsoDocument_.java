package org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocument;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.GeneralServiceOperation;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocumentlink.GsoDocumentLink;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(GsoDocument.class)
public abstract class GsoDocument_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<GsoDocument, Date> documentDate;
	public static volatile SingularAttribute<GsoDocument, GeneralServiceOperation> gso;
	public static volatile SingularAttribute<GsoDocument, String> documentNumber;
	public static volatile SingularAttribute<GsoDocument, GsoDocument> supplementaryFor;
	public static volatile SetAttribute<GsoDocument, GsoDocumentLink> links;
	public static volatile SingularAttribute<GsoDocument, Integer> id;

	public static final String DOCUMENT_DATE = "documentDate";
	public static final String GSO = "gso";
	public static final String DOCUMENT_NUMBER = "documentNumber";
	public static final String SUPPLEMENTARY_FOR = "supplementaryFor";
	public static final String LINKS = "links";
	public static final String ID = "id";

}

