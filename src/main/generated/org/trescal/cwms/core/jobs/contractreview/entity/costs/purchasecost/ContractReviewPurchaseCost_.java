package org.trescal.cwms.core.jobs.contractreview.entity.costs.purchasecost;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ContractReviewPurchaseCost.class)
public abstract class ContractReviewPurchaseCost_ extends org.trescal.cwms.core.pricing.entity.costs.base.PurchaseCost_ {

	public static volatile SingularAttribute<ContractReviewPurchaseCost, ContractReviewLinkedPurchaseCost> linkedCost;
	public static volatile SingularAttribute<ContractReviewPurchaseCost, JobItem> jobitem;
	public static volatile SingularAttribute<ContractReviewPurchaseCost, CostSource> costSrc;

	public static final String LINKED_COST = "linkedCost";
	public static final String JOBITEM = "jobitem";
	public static final String COST_SRC = "costSrc";

}

