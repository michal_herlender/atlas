package org.trescal.cwms.core.jobs.calibration.entity.calibration;

import java.time.LocalDate;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.calibration.entity.batchcalibration.BatchCalibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.CalibrationProcess;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationstatus.CalibrationStatus;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalibrationActivityLink;
import org.trescal.cwms.core.jobs.calibration.entity.queuedcalibration.QueuedCalibration;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.StandardUsed;
import org.trescal.cwms.core.jobs.calibration.enums.CalibrationClass;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Calibration.class)
public abstract class Calibration_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<Calibration, CalibrationType> calType;
	public static volatile SingularAttribute<Calibration, Address> calAddress;
	public static volatile SingularAttribute<Calibration, Contact> startedBy;
	public static volatile SingularAttribute<Calibration, String> xindiceKey;
	public static volatile SetAttribute<Calibration, QueuedCalibration> queued;
	public static volatile SingularAttribute<Calibration, String> certTemplate;
	public static volatile SingularAttribute<Calibration, BatchCalibration> batch;
	public static volatile SingularAttribute<Calibration, Date> completeTime;
	public static volatile ListAttribute<Calibration, Certificate> certs;
	public static volatile SingularAttribute<Calibration, Capability> capability;
	public static volatile SingularAttribute<Calibration, CalibrationClass> calClass;
	public static volatile SetAttribute<Calibration, StandardUsed> standardsUsed;
	public static volatile ListAttribute<Calibration, CalibrationActivityLink> jobitemActivityLinks;
	public static volatile SingularAttribute<Calibration, CalibrationProcess> calProcess;
	public static volatile SingularAttribute<Calibration, WorkInstruction> workInstruction;
	public static volatile SingularAttribute<Calibration, Integer> batchPosition;
	public static volatile SetAttribute<Calibration, CalLink> links;
	public static volatile SingularAttribute<Calibration, Date> startTime;
	public static volatile SingularAttribute<Calibration, Integer> id;
	public static volatile SingularAttribute<Calibration, LocalDate> calDate;
	public static volatile SingularAttribute<Calibration, Contact> completedBy;
	public static volatile SingularAttribute<Calibration, CalibrationStatus> status;

	public static final String CAL_TYPE = "calType";
	public static final String CAL_ADDRESS = "calAddress";
	public static final String STARTED_BY = "startedBy";
	public static final String XINDICE_KEY = "xindiceKey";
	public static final String QUEUED = "queued";
	public static final String CERT_TEMPLATE = "certTemplate";
	public static final String BATCH = "batch";
	public static final String COMPLETE_TIME = "completeTime";
	public static final String CERTS = "certs";
	public static final String CAPABILITY = "capability";
	public static final String CAL_CLASS = "calClass";
	public static final String STANDARDS_USED = "standardsUsed";
	public static final String JOBITEM_ACTIVITY_LINKS = "jobitemActivityLinks";
	public static final String CAL_PROCESS = "calProcess";
	public static final String WORK_INSTRUCTION = "workInstruction";
	public static final String BATCH_POSITION = "batchPosition";
	public static final String LINKS = "links";
	public static final String START_TIME = "startTime";
	public static final String ID = "id";
	public static final String CAL_DATE = "calDate";
	public static final String COMPLETED_BY = "completedBy";
	public static final String STATUS = "status";

}

