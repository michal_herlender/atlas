package org.trescal.cwms.core.jobs.jobitem.entity.accessory;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ItemAccessory.class)
public abstract class ItemAccessory_ {

	public static volatile SingularAttribute<ItemAccessory, Integer> quantity;
	public static volatile SingularAttribute<ItemAccessory, FaultReport> faultReport;
	public static volatile SingularAttribute<ItemAccessory, Integer> id;
	public static volatile SingularAttribute<ItemAccessory, Accessory> accessory;
	public static volatile SingularAttribute<ItemAccessory, JobItem> ji;

	public static final String QUANTITY = "quantity";
	public static final String FAULT_REPORT = "faultReport";
	public static final String ID = "id";
	public static final String ACCESSORY = "accessory";
	public static final String JI = "ji";

}

