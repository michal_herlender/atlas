package org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.external.adveso.entity.advesojobitemactivity.AdvesoJobItemActivity;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobItemAction.class)
public abstract class JobItemAction_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<JobItemAction, Date> deleteStamp;
	public static volatile SingularAttribute<JobItemAction, ItemActivity> activity;
	public static volatile SingularAttribute<JobItemAction, Contact> startedBy;
	public static volatile SingularAttribute<JobItemAction, Integer> timeSpent;
	public static volatile SingularAttribute<JobItemAction, ItemStatus> startStatus;
	public static volatile ListAttribute<JobItemAction, AdvesoJobItemActivity> advesoJobItemActivities;
	public static volatile SingularAttribute<JobItemAction, String> remark;
	public static volatile SingularAttribute<JobItemAction, Date> createdOn;
	public static volatile SingularAttribute<JobItemAction, String> activityDesc;
	public static volatile SingularAttribute<JobItemAction, Contact> deletedBy;
	public static volatile SingularAttribute<JobItemAction, ItemStatus> endStatus;
	public static volatile SingularAttribute<JobItemAction, Date> startStamp;
	public static volatile SingularAttribute<JobItemAction, Boolean> deleted;
	public static volatile SingularAttribute<JobItemAction, JobItem> jobItem;
	public static volatile SingularAttribute<JobItemAction, Date> endStamp;
	public static volatile SingularAttribute<JobItemAction, Integer> id;

	public static final String DELETE_STAMP = "deleteStamp";
	public static final String ACTIVITY = "activity";
	public static final String STARTED_BY = "startedBy";
	public static final String TIME_SPENT = "timeSpent";
	public static final String START_STATUS = "startStatus";
	public static final String ADVESO_JOB_ITEM_ACTIVITIES = "advesoJobItemActivities";
	public static final String REMARK = "remark";
	public static final String CREATED_ON = "createdOn";
	public static final String ACTIVITY_DESC = "activityDesc";
	public static final String DELETED_BY = "deletedBy";
	public static final String END_STATUS = "endStatus";
	public static final String START_STAMP = "startStamp";
	public static final String DELETED = "deleted";
	public static final String JOB_ITEM = "jobItem";
	public static final String END_STAMP = "endStamp";
	public static final String ID = "id";

}

