package org.trescal.cwms.core.jobs.calibration.entity.reversetraceability;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ReverseTraceabilitySettings.class)
public abstract class ReverseTraceabilitySettings_ {

	public static volatile SingularAttribute<ReverseTraceabilitySettings, Boolean> reverseTraceability;
	public static volatile SingularAttribute<ReverseTraceabilitySettings, Subdiv> businessSubdiv;
	public static volatile SingularAttribute<ReverseTraceabilitySettings, Integer> id;
	public static volatile SingularAttribute<ReverseTraceabilitySettings, LocalDate> startDate;

	public static final String REVERSE_TRACEABILITY = "reverseTraceability";
	public static final String BUSINESS_SUBDIV = "businessSubdiv";
	public static final String ID = "id";
	public static final String START_DATE = "startDate";

}

