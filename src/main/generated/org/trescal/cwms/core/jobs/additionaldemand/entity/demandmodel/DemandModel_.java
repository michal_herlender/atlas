package org.trescal.cwms.core.jobs.additionaldemand.entity.demandmodel;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.jobs.additionaldemand.enums.AdditionalDemand;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(DemandModel.class)
public abstract class DemandModel_ {

	public static volatile SingularAttribute<DemandModel, InstrumentModel> model;
	public static volatile SingularAttribute<DemandModel, Integer> id;
	public static volatile SingularAttribute<DemandModel, AdditionalDemand> demand;

	public static final String MODEL = "model";
	public static final String ID = "id";
	public static final String DEMAND = "demand";

}

