package org.trescal.cwms.core.jobs.calibration.entity.pointset;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PointSet.class)
public abstract class PointSet_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<PointSet, Integer> id;

	public static final String ID = "id";

}

