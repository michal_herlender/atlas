package org.trescal.cwms.core.jobs.contractreview.entity.costs.repcost;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ContractReviewLinkedRepairCost.class)
public abstract class ContractReviewLinkedRepairCost_ extends org.trescal.cwms.core.jobs.contractreview.entity.costs.ContractReviewLinkedCost_ {

	public static volatile SingularAttribute<ContractReviewLinkedRepairCost, ContractReviewRepairCost> repairCost;

	public static final String REPAIR_COST = "repairCost";

}

