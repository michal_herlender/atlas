package org.trescal.cwms.core.jobs.jobitem.entity.defect;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ItemDefect.class)
public abstract class ItemDefect_ extends org.trescal.cwms.core.jobs.jobitem.entity.defect.Defect_ {

	public static volatile SingularAttribute<ItemDefect, Boolean> calSealBroken;
	public static volatile SingularAttribute<ItemDefect, JobItem> ji;

	public static final String CAL_SEAL_BROKEN = "calSealBroken";
	public static final String JI = "ji";

}

