package org.trescal.cwms.core.jobs.repair.componentinventory;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.instrumentmodel.entity.component.Component;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ComponentInventory.class)
public abstract class ComponentInventory_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<ComponentInventory, Company> supplierCompanyId;
	public static volatile SingularAttribute<ComponentInventory, Component> component;
	public static volatile SingularAttribute<ComponentInventory, Integer> componentInventoryId;
	public static volatile SingularAttribute<ComponentInventory, Subdiv> businessSubdivId;

	public static final String SUPPLIER_COMPANY_ID = "supplierCompanyId";
	public static final String COMPONENT = "component";
	public static final String COMPONENT_INVENTORY_ID = "componentInventoryId";
	public static final String BUSINESS_SUBDIV_ID = "businessSubdivId";

}

