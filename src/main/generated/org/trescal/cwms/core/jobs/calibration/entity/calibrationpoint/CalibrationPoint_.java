package org.trescal.cwms.core.jobs.calibration.entity.calibrationpoint;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationpointset.CalibrationPointSet;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CalibrationPoint.class)
public abstract class CalibrationPoint_ extends org.trescal.cwms.core.jobs.calibration.entity.point.Point_ {

	public static volatile SingularAttribute<CalibrationPoint, CalibrationPointSet> pointSet;

	public static final String POINT_SET = "pointSet";

}

