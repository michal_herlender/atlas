package org.trescal.cwms.core.jobs.calibration.entity.calreq;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstrumentCalReq.class)
public abstract class InstrumentCalReq_ extends org.trescal.cwms.core.jobs.calibration.entity.calreq.CalReq_ {

	public static volatile SingularAttribute<InstrumentCalReq, Instrument> inst;

	public static final String INST = "inst";

}

