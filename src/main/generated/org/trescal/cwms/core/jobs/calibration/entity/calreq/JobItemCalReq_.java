package org.trescal.cwms.core.jobs.calibration.entity.calreq;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobItemCalReq.class)
public abstract class JobItemCalReq_ extends org.trescal.cwms.core.jobs.calibration.entity.calreq.CalReq_ {

	public static volatile SingularAttribute<JobItemCalReq, JobItem> jobItem;

	public static final String JOB_ITEM = "jobItem";

}

