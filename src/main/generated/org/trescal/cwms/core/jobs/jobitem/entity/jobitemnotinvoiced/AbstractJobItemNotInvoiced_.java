package org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AbstractJobItemNotInvoiced.class)
public abstract class AbstractJobItemNotInvoiced_ {

	public static volatile SingularAttribute<AbstractJobItemNotInvoiced, LocalDate> date;
	public static volatile SingularAttribute<AbstractJobItemNotInvoiced, NotInvoicedReason> reason;
	public static volatile SingularAttribute<AbstractJobItemNotInvoiced, Invoice> periodicInvoice;
	public static volatile SingularAttribute<AbstractJobItemNotInvoiced, String> comments;
	public static volatile SingularAttribute<AbstractJobItemNotInvoiced, Contact> contact;

	public static final String DATE = "date";
	public static final String REASON = "reason";
	public static final String PERIODIC_INVOICE = "periodicInvoice";
	public static final String COMMENTS = "comments";
	public static final String CONTACT = "contact";

}

