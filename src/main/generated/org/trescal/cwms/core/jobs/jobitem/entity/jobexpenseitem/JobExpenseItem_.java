package org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem;

import java.math.BigDecimal;
import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.JobExpenseItemNotInvoiced;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingexpenseitem.JobCostingExpenseItem;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobExpenseItem.class)
public abstract class JobExpenseItem_ extends org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitem.AbstractJobItem_ {

	public static volatile SingularAttribute<JobExpenseItem, ServiceType> serviceType;
	public static volatile SingularAttribute<JobExpenseItem, LocalDate> date;
	public static volatile SingularAttribute<JobExpenseItem, JobExpenseItemNotInvoiced> notInvoiced;
	public static volatile SingularAttribute<JobExpenseItem, BigDecimal> singlePrice;
	public static volatile SingularAttribute<JobExpenseItem, Integer> quantity;
	public static volatile SingularAttribute<JobExpenseItem, Boolean> invoiceable;
	public static volatile SetAttribute<JobExpenseItem, InvoiceItem> invoiceItems;
	public static volatile ListAttribute<JobExpenseItem, JobCostingExpenseItem> costingItems;
	public static volatile SingularAttribute<JobExpenseItem, InstrumentModel> model;
	public static volatile SingularAttribute<JobExpenseItem, String> comment;
	public static volatile SingularAttribute<JobExpenseItem, Integer> id;
	public static volatile SingularAttribute<JobExpenseItem, Integer> itemNo;

	public static final String SERVICE_TYPE = "serviceType";
	public static final String DATE = "date";
	public static final String NOT_INVOICED = "notInvoiced";
	public static final String SINGLE_PRICE = "singlePrice";
	public static final String QUANTITY = "quantity";
	public static final String INVOICEABLE = "invoiceable";
	public static final String INVOICE_ITEMS = "invoiceItems";
	public static final String COSTING_ITEMS = "costingItems";
	public static final String MODEL = "model";
	public static final String COMMENT = "comment";
	public static final String ID = "id";
	public static final String ITEM_NO = "itemNo";

}

