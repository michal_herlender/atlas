package org.trescal.cwms.core.jobs.certificate.entity.certaction;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateAction;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CertAction.class)
public abstract class CertAction_ {

	public static volatile SingularAttribute<CertAction, Contact> actionBy;
	public static volatile SingularAttribute<CertAction, CertificateAction> action;
	public static volatile SingularAttribute<CertAction, Certificate> cert;
	public static volatile SingularAttribute<CertAction, Integer> id;
	public static volatile SingularAttribute<CertAction, Date> actionOn;

	public static final String ACTION_BY = "actionBy";
	public static final String ACTION = "action";
	public static final String CERT = "cert";
	public static final String ID = "id";
	public static final String ACTION_ON = "actionOn";

}

