package org.trescal.cwms.core.jobs.repair.component;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.entity.component.Component;
import org.trescal.cwms.core.jobs.repair.component.enums.RepairComponentSourceEnum;
import org.trescal.cwms.core.jobs.repair.component.enums.RepairComponentStateEnum;
import org.trescal.cwms.core.jobs.repair.operation.FreeRepairOperation;
import org.trescal.cwms.core.jobs.repair.validatedcomponent.ValidatedFreeRepairComponent;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(FreeRepairComponent.class)
public abstract class FreeRepairComponent_ {

	public static volatile SingularAttribute<FreeRepairComponent, Component> component;
	public static volatile SingularAttribute<FreeRepairComponent, Integer> componentId;
	public static volatile SingularAttribute<FreeRepairComponent, Integer> quantity;
	public static volatile SingularAttribute<FreeRepairComponent, BigDecimal> cost;
	public static volatile SingularAttribute<FreeRepairComponent, String> name;
	public static volatile SingularAttribute<FreeRepairComponent, FreeRepairOperation> freeRepairOperation;
	public static volatile SingularAttribute<FreeRepairComponent, RepairComponentSourceEnum> source;
	public static volatile SingularAttribute<FreeRepairComponent, Boolean> addedAfterRiRValidation;
	public static volatile SingularAttribute<FreeRepairComponent, ValidatedFreeRepairComponent> validatedFreeRepairComponent;
	public static volatile SingularAttribute<FreeRepairComponent, RepairComponentStateEnum> status;

	public static final String COMPONENT = "component";
	public static final String COMPONENT_ID = "componentId";
	public static final String QUANTITY = "quantity";
	public static final String COST = "cost";
	public static final String NAME = "name";
	public static final String FREE_REPAIR_OPERATION = "freeRepairOperation";
	public static final String SOURCE = "source";
	public static final String ADDED_AFTER_RI_RVALIDATION = "addedAfterRiRValidation";
	public static final String VALIDATED_FREE_REPAIR_COMPONENT = "validatedFreeRepairComponent";
	public static final String STATUS = "status";

}

