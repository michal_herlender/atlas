package org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(OnBehalfItem.class)
public abstract class OnBehalfItem_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<OnBehalfItem, Address> address;
	public static volatile SingularAttribute<OnBehalfItem, JobItem> jobitem;
	public static volatile SingularAttribute<OnBehalfItem, Date> setOn;
	public static volatile SingularAttribute<OnBehalfItem, Company> company;
	public static volatile SingularAttribute<OnBehalfItem, Integer> id;
	public static volatile SingularAttribute<OnBehalfItem, Contact> setBy;

	public static final String ADDRESS = "address";
	public static final String JOBITEM = "jobitem";
	public static final String SET_ON = "setOn";
	public static final String COMPANY = "company";
	public static final String ID = "id";
	public static final String SET_BY = "setBy";

}

