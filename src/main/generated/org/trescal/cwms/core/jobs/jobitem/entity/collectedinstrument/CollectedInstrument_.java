package org.trescal.cwms.core.jobs.jobitem.entity.collectedinstrument;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CollectedInstrument.class)
public abstract class CollectedInstrument_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<CollectedInstrument, Schedule> schedule;
	public static volatile SingularAttribute<CollectedInstrument, Instrument> inst;
	public static volatile SingularAttribute<CollectedInstrument, Date> collectStamp;
	public static volatile SingularAttribute<CollectedInstrument, InstrumentModel> model;
	public static volatile SingularAttribute<CollectedInstrument, Integer> id;
	public static volatile SingularAttribute<CollectedInstrument, Contact> collectedBy;
	public static volatile SingularAttribute<CollectedInstrument, Integer> numItems;

	public static final String SCHEDULE = "schedule";
	public static final String INST = "inst";
	public static final String COLLECT_STAMP = "collectStamp";
	public static final String MODEL = "model";
	public static final String ID = "id";
	public static final String COLLECTED_BY = "collectedBy";
	public static final String NUM_ITEMS = "numItems";

}

