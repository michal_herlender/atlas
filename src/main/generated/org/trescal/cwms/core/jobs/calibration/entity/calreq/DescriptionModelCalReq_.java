package org.trescal.cwms.core.jobs.calibration.entity.calreq;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(DescriptionModelCalReq.class)
public abstract class DescriptionModelCalReq_ extends org.trescal.cwms.core.jobs.calibration.entity.calreq.CalReq_ {

	public static volatile SingularAttribute<DescriptionModelCalReq, Description> description;
	public static volatile SingularAttribute<DescriptionModelCalReq, Company> company;

	public static final String DESCRIPTION = "description";
	public static final String COMPANY = "company";

}

