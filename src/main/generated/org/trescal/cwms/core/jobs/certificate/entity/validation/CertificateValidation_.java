package org.trescal.cwms.core.jobs.certificate.entity.validation;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CertificateValidation.class)
public abstract class CertificateValidation_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<CertificateValidation, String> note;
	public static volatile SingularAttribute<CertificateValidation, LocalDate> datevalidated;
	public static volatile SingularAttribute<CertificateValidation, String> responsible;
	public static volatile SingularAttribute<CertificateValidation, String> document;
	public static volatile SingularAttribute<CertificateValidation, Certificate> cert;
	public static volatile SingularAttribute<CertificateValidation, Contact> validatedBy;
	public static volatile SingularAttribute<CertificateValidation, Integer> id;
	public static volatile SingularAttribute<CertificateValidation, CertificateValidationStatus> status;

	public static final String NOTE = "note";
	public static final String DATEVALIDATED = "datevalidated";
	public static final String RESPONSIBLE = "responsible";
	public static final String DOCUMENT = "document";
	public static final String CERT = "cert";
	public static final String VALIDATED_BY = "validatedBy";
	public static final String ID = "id";
	public static final String STATUS = "status";

}

