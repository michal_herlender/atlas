package org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobItemActivity.class)
public abstract class JobItemActivity_ extends org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction_ {

	public static volatile SingularAttribute<JobItemActivity, Boolean> complete;
	public static volatile SingularAttribute<JobItemActivity, Contact> completedBy;
	public static volatile SingularAttribute<JobItemActivity, ActionOutcome> outcome;

	public static final String COMPLETE = "complete";
	public static final String COMPLETED_BY = "completedBy";
	public static final String OUTCOME = "outcome";

}

