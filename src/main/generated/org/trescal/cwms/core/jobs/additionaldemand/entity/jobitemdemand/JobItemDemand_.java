package org.trescal.cwms.core.jobs.additionaldemand.entity.jobitemdemand;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.additionaldemand.enums.AdditionalDemand;
import org.trescal.cwms.core.jobs.additionaldemand.enums.AdditionalDemandStatus;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobItemDemand.class)
public abstract class JobItemDemand_ {

	public static volatile SingularAttribute<JobItemDemand, JobItem> jobItem;
	public static volatile SingularAttribute<JobItemDemand, AdditionalDemand> additionalDemand;
	public static volatile SingularAttribute<JobItemDemand, AdditionalDemandStatus> additionalDemandStatus;
	public static volatile SingularAttribute<JobItemDemand, String> comment;
	public static volatile SingularAttribute<JobItemDemand, Integer> id;

	public static final String JOB_ITEM = "jobItem";
	public static final String ADDITIONAL_DEMAND = "additionalDemand";
	public static final String ADDITIONAL_DEMAND_STATUS = "additionalDemandStatus";
	public static final String COMMENT = "comment";
	public static final String ID = "id";

}

