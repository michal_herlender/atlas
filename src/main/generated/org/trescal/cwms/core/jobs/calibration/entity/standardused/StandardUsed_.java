package org.trescal.cwms.core.jobs.calibration.entity.standardused;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(StandardUsed.class)
public abstract class StandardUsed_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<StandardUsed, Instrument> inst;
	public static volatile SingularAttribute<StandardUsed, Integer> id;
	public static volatile SingularAttribute<StandardUsed, Calibration> calibration;
	public static volatile SingularAttribute<StandardUsed, Integer> calPercentage;
	public static volatile SingularAttribute<StandardUsed, LocalDate> nextCalDueDate;

	public static final String INST = "inst";
	public static final String ID = "id";
	public static final String CALIBRATION = "calibration";
	public static final String CAL_PERCENTAGE = "calPercentage";
	public static final String NEXT_CAL_DUE_DATE = "nextCalDueDate";

}

