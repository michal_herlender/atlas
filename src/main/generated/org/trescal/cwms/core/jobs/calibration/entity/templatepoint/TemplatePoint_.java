package org.trescal.cwms.core.jobs.calibration.entity.templatepoint;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.calibration.entity.pointsettemplate.PointSetTemplate;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TemplatePoint.class)
public abstract class TemplatePoint_ extends org.trescal.cwms.core.jobs.calibration.entity.point.Point_ {

	public static volatile SingularAttribute<TemplatePoint, PointSetTemplate> template;

	public static final String TEMPLATE = "template";

}

