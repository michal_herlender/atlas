package org.trescal.cwms.core.jobs.job.entity.upcomingworkjoblink;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.system.entity.upcomingwork.UpcomingWork;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UpcomingWorkJobLink.class)
public abstract class UpcomingWorkJobLink_ {

	public static volatile SingularAttribute<UpcomingWorkJobLink, UpcomingWork> upcomingWork;
	public static volatile SingularAttribute<UpcomingWorkJobLink, Boolean> active;
	public static volatile SingularAttribute<UpcomingWorkJobLink, Integer> id;
	public static volatile SingularAttribute<UpcomingWorkJobLink, Job> job;

	public static final String UPCOMING_WORK = "upcomingWork";
	public static final String ACTIVE = "active";
	public static final String ID = "id";
	public static final String JOB = "job";

}

