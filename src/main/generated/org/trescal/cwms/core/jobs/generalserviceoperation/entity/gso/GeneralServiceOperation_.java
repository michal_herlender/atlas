package org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocument.GsoDocument;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsolink.GsoLink;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsostatus.GeneralServiceOperationStatus;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(GeneralServiceOperation.class)
public abstract class GeneralServiceOperation_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<GeneralServiceOperation, CalibrationType> calType;
	public static volatile SingularAttribute<GeneralServiceOperation, Capability> capability;
	public static volatile SingularAttribute<GeneralServiceOperation, Boolean> clientDecisionNeeded;
	public static volatile SingularAttribute<GeneralServiceOperation, Address> address;
	public static volatile SingularAttribute<GeneralServiceOperation, Contact> startedBy;
	public static volatile SingularAttribute<GeneralServiceOperation, Date> completedOn;
	public static volatile SetAttribute<GeneralServiceOperation, GsoDocument> gsoDocuments;
	public static volatile SetAttribute<GeneralServiceOperation, GsoLink> links;
	public static volatile SingularAttribute<GeneralServiceOperation, Integer> id;
	public static volatile SingularAttribute<GeneralServiceOperation, Date> startedOn;
	public static volatile SingularAttribute<GeneralServiceOperation, Contact> completedBy;
	public static volatile SingularAttribute<GeneralServiceOperation, GeneralServiceOperationStatus> status;

	public static final String CAL_TYPE = "calType";
	public static final String CAPABILITY = "capability";
	public static final String CLIENT_DECISION_NEEDED = "clientDecisionNeeded";
	public static final String ADDRESS = "address";
	public static final String STARTED_BY = "startedBy";
	public static final String COMPLETED_ON = "completedOn";
	public static final String GSO_DOCUMENTS = "gsoDocuments";
	public static final String LINKS = "links";
	public static final String ID = "id";
	public static final String STARTED_ON = "startedOn";
	public static final String COMPLETED_BY = "completedBy";
	public static final String STATUS = "status";

}

