package org.trescal.cwms.core.jobs.repair.repaircompletionreport;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.repair.operation.FreeRepairOperation;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.enums.AdjustmentPerformedEnum;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.RepairInspectionReport;
import org.trescal.cwms.core.jobs.repair.validatedoperation.ValidatedFreeRepairOperation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RepairCompletionReport.class)
public abstract class RepairCompletionReport_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SetAttribute<RepairCompletionReport, RepairInspectionReport> repairInspectionReports;
	public static volatile SingularAttribute<RepairCompletionReport, Date> validatedOn;
	public static volatile SingularAttribute<RepairCompletionReport, Boolean> validated;
	public static volatile SingularAttribute<RepairCompletionReport, AdjustmentPerformedEnum> adjustmentPerformed;
	public static volatile SingularAttribute<RepairCompletionReport, Integer> rcrId;
	public static volatile SingularAttribute<RepairCompletionReport, String> internalCompletionComments;
	public static volatile SingularAttribute<RepairCompletionReport, String> externalCompletionComments;
	public static volatile SingularAttribute<RepairCompletionReport, Contact> validatedBy;
	public static volatile SingularAttribute<RepairCompletionReport, String> rcrNumber;
	public static volatile SetAttribute<RepairCompletionReport, ValidatedFreeRepairOperation> validatedFreeRepairOperations;
	public static volatile SingularAttribute<RepairCompletionReport, Date> repairCompletionDate;
	public static volatile SetAttribute<RepairCompletionReport, FreeRepairOperation> freeRepairOperations;

	public static final String REPAIR_INSPECTION_REPORTS = "repairInspectionReports";
	public static final String VALIDATED_ON = "validatedOn";
	public static final String VALIDATED = "validated";
	public static final String ADJUSTMENT_PERFORMED = "adjustmentPerformed";
	public static final String RCR_ID = "rcrId";
	public static final String INTERNAL_COMPLETION_COMMENTS = "internalCompletionComments";
	public static final String EXTERNAL_COMPLETION_COMMENTS = "externalCompletionComments";
	public static final String VALIDATED_BY = "validatedBy";
	public static final String RCR_NUMBER = "rcrNumber";
	public static final String VALIDATED_FREE_REPAIR_OPERATIONS = "validatedFreeRepairOperations";
	public static final String REPAIR_COMPLETION_DATE = "repairCompletionDate";
	public static final String FREE_REPAIR_OPERATIONS = "freeRepairOperations";

}

