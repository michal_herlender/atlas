package org.trescal.cwms.core.jobs.jobitem.entity.requirement;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Requirement.class)
public abstract class Requirement_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<Requirement, JobItem> item;
	public static volatile SingularAttribute<Requirement, Boolean> deleted;
	public static volatile SingularAttribute<Requirement, Contact> createdBy;
	public static volatile SingularAttribute<Requirement, Date> created;
	public static volatile SingularAttribute<Requirement, Date> deletedOn;
	public static volatile SingularAttribute<Requirement, Integer> id;
	public static volatile SingularAttribute<Requirement, String> label;
	public static volatile SingularAttribute<Requirement, String> requirement;
	public static volatile SingularAttribute<Requirement, Contact> deletedBy;

	public static final String ITEM = "item";
	public static final String DELETED = "deleted";
	public static final String CREATED_BY = "createdBy";
	public static final String CREATED = "created";
	public static final String DELETED_ON = "deletedOn";
	public static final String ID = "id";
	public static final String LABEL = "label";
	public static final String REQUIREMENT = "requirement";
	public static final String DELETED_BY = "deletedBy";

}

