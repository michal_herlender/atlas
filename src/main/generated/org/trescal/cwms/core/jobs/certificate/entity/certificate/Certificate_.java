package org.trescal.cwms.core.jobs.certificate.entity.certificate;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateClass;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateType;
import org.trescal.cwms.core.jobs.certificate.entity.certaction.CertAction;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.jobs.certificate.entity.certnote.CertNote;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.jobs.certificate.entity.instcertlink.InstCertLink;
import org.trescal.cwms.core.jobs.certificate.entity.validation.CertificateValidation;
import org.trescal.cwms.core.procedure.entity.proceduretrainingrecord.CapabilityTrainingRecord;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Certificate.class)
public abstract class Certificate_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<Certificate, CalibrationType> calType;
	public static volatile SingularAttribute<Certificate, ServiceType> serviceType;
	public static volatile SingularAttribute<Certificate, Boolean> repair;
	public static volatile SetAttribute<Certificate, CertNote> notes;
	public static volatile SingularAttribute<Certificate, Contact> registeredBy;
	public static volatile SingularAttribute<Certificate, Contact> signedBy;
	public static volatile SingularAttribute<Certificate, CapabilityTrainingRecord> procTrainingRec;
	public static volatile SingularAttribute<Certificate, Date> certDate;
	public static volatile SingularAttribute<Certificate, Contact> approvedBy;
	public static volatile SingularAttribute<Certificate, Certificate> supplementaryFor;
	public static volatile SingularAttribute<Certificate, Integer> certid;
	public static volatile SingularAttribute<Certificate, CertificateType> type;
	public static volatile SingularAttribute<Certificate, Calibration> cal;
	public static volatile SingularAttribute<Certificate, Integer> duration;
	public static volatile SingularAttribute<Certificate, String> certno;
	public static volatile SingularAttribute<Certificate, ActionOutcome> thirdOutcome;
	public static volatile SingularAttribute<Certificate, Boolean> certPreviewed;
	public static volatile SingularAttribute<Certificate, String> supplementaryCertificateComments;
	public static volatile SingularAttribute<Certificate, Boolean> optimization;
	public static volatile SingularAttribute<Certificate, Subdiv> thirdDiv;
	public static volatile SetAttribute<Certificate, CertLink> links;
	public static volatile SingularAttribute<Certificate, String> thirdCertNo;
	public static volatile SetAttribute<Certificate, CertificateValidation> validations;
	public static volatile SingularAttribute<Certificate, CalibrationVerificationStatus> calibrationVerificationStatus;
	public static volatile SingularAttribute<Certificate, CertificateClass> certClass;
	public static volatile SetAttribute<Certificate, InstCertLink> instCertLinks;
	public static volatile SingularAttribute<Certificate, String> docTemplateKey;
	public static volatile SingularAttribute<Certificate, String> procTemplateKey;
	public static volatile SingularAttribute<Certificate, BigDecimal> deviation;
	public static volatile SingularAttribute<Certificate, IntervalUnit> unit;
	public static volatile SingularAttribute<Certificate, Boolean> restriction;
	public static volatile SingularAttribute<Certificate, Boolean> adjustment;
	public static volatile SetAttribute<Certificate, CertAction> actions;
	public static volatile SingularAttribute<Certificate, Date> calDate;
	public static volatile SingularAttribute<Certificate, String> remarks;
	public static volatile SingularAttribute<Certificate, CertStatusEnum> status;

	public static final String CAL_TYPE = "calType";
	public static final String SERVICE_TYPE = "serviceType";
	public static final String REPAIR = "repair";
	public static final String NOTES = "notes";
	public static final String REGISTERED_BY = "registeredBy";
	public static final String SIGNED_BY = "signedBy";
	public static final String PROC_TRAINING_REC = "procTrainingRec";
	public static final String CERT_DATE = "certDate";
	public static final String APPROVED_BY = "approvedBy";
	public static final String SUPPLEMENTARY_FOR = "supplementaryFor";
	public static final String CERTID = "certid";
	public static final String TYPE = "type";
	public static final String CAL = "cal";
	public static final String DURATION = "duration";
	public static final String CERTNO = "certno";
	public static final String THIRD_OUTCOME = "thirdOutcome";
	public static final String CERT_PREVIEWED = "certPreviewed";
	public static final String SUPPLEMENTARY_CERTIFICATE_COMMENTS = "supplementaryCertificateComments";
	public static final String OPTIMIZATION = "optimization";
	public static final String THIRD_DIV = "thirdDiv";
	public static final String LINKS = "links";
	public static final String THIRD_CERT_NO = "thirdCertNo";
	public static final String VALIDATIONS = "validations";
	public static final String CALIBRATION_VERIFICATION_STATUS = "calibrationVerificationStatus";
	public static final String CERT_CLASS = "certClass";
	public static final String INST_CERT_LINKS = "instCertLinks";
	public static final String DOC_TEMPLATE_KEY = "docTemplateKey";
	public static final String PROC_TEMPLATE_KEY = "procTemplateKey";
	public static final String DEVIATION = "deviation";
	public static final String UNIT = "unit";
	public static final String RESTRICTION = "restriction";
	public static final String ADJUSTMENT = "adjustment";
	public static final String ACTIONS = "actions";
	public static final String CAL_DATE = "calDate";
	public static final String REMARKS = "remarks";
	public static final String STATUS = "status";

}

