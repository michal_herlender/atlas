package org.trescal.cwms.core.jobs.calibration.entity.point;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.entity.uom.UoM;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Point.class)
public abstract class Point_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<Point, UoM> uom;
	public static volatile SingularAttribute<Point, UoM> relatedUom;
	public static volatile SingularAttribute<Point, BigDecimal> relatedPoint;
	public static volatile SingularAttribute<Point, Integer> id;
	public static volatile SingularAttribute<Point, BigDecimal> point;

	public static final String UOM = "uom";
	public static final String RELATED_UOM = "relatedUom";
	public static final String RELATED_POINT = "relatedPoint";
	public static final String ID = "id";
	public static final String POINT = "point";

}

