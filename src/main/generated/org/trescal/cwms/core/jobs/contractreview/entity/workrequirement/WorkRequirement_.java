package org.trescal.cwms.core.jobs.contractreview.entity.workrequirement;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.jobs.contractreview.entity.WorkRequirementType;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirementstatus.WorkrequirementStatus;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(WorkRequirement.class)
public abstract class WorkRequirement_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<WorkRequirement, ServiceType> serviceType;
	public static volatile SingularAttribute<WorkRequirement, Capability> capability;
	public static volatile SingularAttribute<WorkRequirement, Address> address;
	public static volatile SingularAttribute<WorkRequirement, WorkInstruction> workInstruction;
	public static volatile SingularAttribute<WorkRequirement, Integer> id;
	public static volatile SingularAttribute<WorkRequirement, String> requirement;
	public static volatile SetAttribute<WorkRequirement, JobItemWorkRequirement> jobItemWorkRequirements;
	public static volatile SingularAttribute<WorkRequirement, Department> department;
	public static volatile SingularAttribute<WorkRequirement, WorkRequirementType> type;
	public static volatile SingularAttribute<WorkRequirement, WorkrequirementStatus> status;

	public static final String SERVICE_TYPE = "serviceType";
	public static final String CAPABILITY = "capability";
	public static final String ADDRESS = "address";
	public static final String WORK_INSTRUCTION = "workInstruction";
	public static final String ID = "id";
	public static final String REQUIREMENT = "requirement";
	public static final String JOB_ITEM_WORK_REQUIREMENTS = "jobItemWorkRequirements";
	public static final String DEPARTMENT = "department";
	public static final String TYPE = "type";
	public static final String STATUS = "status";

}

