package org.trescal.cwms.core.jobs.calibration.entity.calreq;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationpointset.CalibrationPointSet;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationrange.CalibrationRange;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CalReq.class)
public abstract class CalReq_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SingularAttribute<CalReq, String> privateInstructions;
	public static volatile SingularAttribute<CalReq, Date> deactivatedOn;
	public static volatile SingularAttribute<CalReq, Boolean> publish;
	public static volatile SingularAttribute<CalReq, CalibrationRange> range;
	public static volatile SingularAttribute<CalReq, Boolean> active;
	public static volatile SingularAttribute<CalReq, CalibrationPointSet> pointSet;
	public static volatile SingularAttribute<CalReq, Integer> id;
	public static volatile SingularAttribute<CalReq, String> publicInstructions;
	public static volatile SingularAttribute<CalReq, Contact> deactivatedBy;

	public static final String PRIVATE_INSTRUCTIONS = "privateInstructions";
	public static final String DEACTIVATED_ON = "deactivatedOn";
	public static final String PUBLISH = "publish";
	public static final String RANGE = "range";
	public static final String ACTIVE = "active";
	public static final String POINT_SET = "pointSet";
	public static final String ID = "id";
	public static final String PUBLIC_INSTRUCTIONS = "publicInstructions";
	public static final String DEACTIVATED_BY = "deactivatedBy";

}

