package org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobExpenseItemNotInvoiced.class)
public abstract class JobExpenseItemNotInvoiced_ extends org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.AbstractJobItemNotInvoiced_ {

	public static volatile SingularAttribute<JobExpenseItemNotInvoiced, JobExpenseItem> expenseItem;
	public static volatile SingularAttribute<JobExpenseItemNotInvoiced, Integer> id;

	public static final String EXPENSE_ITEM = "expenseItem";
	public static final String ID = "id";

}

