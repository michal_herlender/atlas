package org.trescal.cwms.core.jobs.jobitemhistory;

import java.time.LocalDate;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobItemHistory.class)
public abstract class JobItemHistory_ {

	public static volatile SingularAttribute<JobItemHistory, String> fieldname;
	public static volatile SingularAttribute<JobItemHistory, LocalDate> newValue;
	public static volatile SingularAttribute<JobItemHistory, JobItem> jobItem;
	public static volatile SingularAttribute<JobItemHistory, Contact> changeBy;
	public static volatile SingularAttribute<JobItemHistory, Date> changeDate;
	public static volatile SingularAttribute<JobItemHistory, Boolean> valueUpdated;
	public static volatile SingularAttribute<JobItemHistory, String> comment;
	public static volatile SingularAttribute<JobItemHistory, Integer> id;
	public static volatile SingularAttribute<JobItemHistory, LocalDate> oldValue;

	public static final String FIELDNAME = "fieldname";
	public static final String NEW_VALUE = "newValue";
	public static final String JOB_ITEM = "jobItem";
	public static final String CHANGE_BY = "changeBy";
	public static final String CHANGE_DATE = "changeDate";
	public static final String VALUE_UPDATED = "valueUpdated";
	public static final String COMMENT = "comment";
	public static final String ID = "id";
	public static final String OLD_VALUE = "oldValue";

}

