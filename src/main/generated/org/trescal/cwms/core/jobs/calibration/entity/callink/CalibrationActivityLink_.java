package org.trescal.cwms.core.jobs.calibration.entity.callink;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CalibrationActivityLink.class)
public abstract class CalibrationActivityLink_ {

	public static volatile SingularAttribute<CalibrationActivityLink, JobItemActivity> jobItemActivity;
	public static volatile SingularAttribute<CalibrationActivityLink, Integer> id;
	public static volatile SingularAttribute<CalibrationActivityLink, Calibration> calibration;

	public static final String JOB_ITEM_ACTIVITY = "jobItemActivity";
	public static final String ID = "id";
	public static final String CALIBRATION = "calibration";

}

