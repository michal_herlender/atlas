package org.trescal.cwms.core.jobs.repair.validatedoperation;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.repair.operation.FreeRepairOperation;
import org.trescal.cwms.core.jobs.repair.operation.enums.RepairOperationStateEnum;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.RepairCompletionReport;
import org.trescal.cwms.core.jobs.repair.validatedcomponent.ValidatedFreeRepairComponent;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ValidatedFreeRepairOperation.class)
public abstract class ValidatedFreeRepairOperation_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<ValidatedFreeRepairOperation, Integer> validatedOperationId;
	public static volatile SingularAttribute<ValidatedFreeRepairOperation, BigDecimal> cost;
	public static volatile SingularAttribute<ValidatedFreeRepairOperation, RepairOperationStateEnum> validatedStatus;
	public static volatile SingularAttribute<ValidatedFreeRepairOperation, RepairCompletionReport> repairCompletionReport;
	public static volatile SingularAttribute<ValidatedFreeRepairOperation, Integer> labourTime;
	public static volatile SingularAttribute<ValidatedFreeRepairOperation, FreeRepairOperation> freeRepairOperation;
	public static volatile SetAttribute<ValidatedFreeRepairOperation, ValidatedFreeRepairComponent> validatedFreeRepairComponents;

	public static final String VALIDATED_OPERATION_ID = "validatedOperationId";
	public static final String COST = "cost";
	public static final String VALIDATED_STATUS = "validatedStatus";
	public static final String REPAIR_COMPLETION_REPORT = "repairCompletionReport";
	public static final String LABOUR_TIME = "labourTime";
	public static final String FREE_REPAIR_OPERATION = "freeRepairOperation";
	public static final String VALIDATED_FREE_REPAIR_COMPONENTS = "validatedFreeRepairComponents";

}

