package org.trescal.cwms.core.jobs.certificate.entity.certlink;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CertLink.class)
public abstract class CertLink_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<CertLink, Integer> linkId;
	public static volatile SingularAttribute<CertLink, JobItem> jobItem;
	public static volatile SingularAttribute<CertLink, Certificate> cert;

	public static final String LINK_ID = "linkId";
	public static final String JOB_ITEM = "jobItem";
	public static final String CERT = "cert";

}

