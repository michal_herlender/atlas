package org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitem;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitempo.AbstractJobItemPO;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AbstractJobItem.class)
public abstract class AbstractJobItem_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SetAttribute<AbstractJobItem, AbstractJobItemPO> itemPOs;
	public static volatile SingularAttribute<AbstractJobItem, String> clientRef;
	public static volatile SingularAttribute<AbstractJobItem, ItemState> state;
	public static volatile SingularAttribute<AbstractJobItem, Job> job;

	public static final String ITEM_POS = "itemPOs";
	public static final String CLIENT_REF = "clientRef";
	public static final String STATE = "state";
	public static final String JOB = "job";

}

