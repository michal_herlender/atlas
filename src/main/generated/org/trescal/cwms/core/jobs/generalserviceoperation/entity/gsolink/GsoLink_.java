package org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsolink;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.GeneralServiceOperation;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(GsoLink.class)
public abstract class GsoLink_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<GsoLink, GeneralServiceOperation> gso;
	public static volatile SingularAttribute<GsoLink, Integer> timeSpent;
	public static volatile SingularAttribute<GsoLink, Integer> id;
	public static volatile SingularAttribute<GsoLink, ActionOutcome> outcome;
	public static volatile SingularAttribute<GsoLink, JobItem> ji;

	public static final String GSO = "gso";
	public static final String TIME_SPENT = "timeSpent";
	public static final String ID = "id";
	public static final String OUTCOME = "outcome";
	public static final String JI = "ji";

}

