package org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobItemNotInvoiced.class)
public abstract class JobItemNotInvoiced_ extends org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.AbstractJobItemNotInvoiced_ {

	public static volatile SingularAttribute<JobItemNotInvoiced, JobItem> jobItem;
	public static volatile SingularAttribute<JobItemNotInvoiced, Integer> id;

	public static final String JOB_ITEM = "jobItem";
	public static final String ID = "id";

}

