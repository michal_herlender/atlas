package org.trescal.cwms.core.jobs.jobitem.entity.accessory;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Accessory.class)
public abstract class Accessory_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SetAttribute<Accessory, Translation> translations;
	public static volatile SingularAttribute<Accessory, Integer> id;
	public static volatile SingularAttribute<Accessory, String> desc;

	public static final String TRANSLATIONS = "translations";
	public static final String ID = "id";
	public static final String DESC = "desc";

}

