package org.trescal.cwms.core.jobs.jobitem.entity.jobitemtransit;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.location.Location;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobItemTransit.class)
public abstract class JobItemTransit_ extends org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction_ {

	public static volatile SingularAttribute<JobItemTransit, Date> transitDate;
	public static volatile SingularAttribute<JobItemTransit, Address> toAddr;
	public static volatile SingularAttribute<JobItemTransit, Location> toLoc;
	public static volatile SingularAttribute<JobItemTransit, Location> fromLoc;
	public static volatile SingularAttribute<JobItemTransit, Address> fromAddr;

	public static final String TRANSIT_DATE = "transitDate";
	public static final String TO_ADDR = "toAddr";
	public static final String TO_LOC = "toLoc";
	public static final String FROM_LOC = "fromLoc";
	public static final String FROM_ADDR = "fromAddr";

}

