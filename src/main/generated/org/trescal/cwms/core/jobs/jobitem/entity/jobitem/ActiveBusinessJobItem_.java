package org.trescal.cwms.core.jobs.jobitem.entity.jobitem;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ActiveBusinessJobItem.class)
public abstract class ActiveBusinessJobItem_ {

	public static volatile SingularAttribute<ActiveBusinessJobItem, Integer> jobItemId;
	public static volatile SingularAttribute<ActiveBusinessJobItem, JobItem> jobItem;
	public static volatile SingularAttribute<ActiveBusinessJobItem, LocalDate> dueDate;

	public static final String JOB_ITEM_ID = "jobItemId";
	public static final String JOB_ITEM = "jobItem";
	public static final String DUE_DATE = "dueDate";

}

