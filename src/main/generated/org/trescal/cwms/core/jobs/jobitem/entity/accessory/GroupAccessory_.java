package org.trescal.cwms.core.jobs.jobitem.entity.accessory;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.JobItemGroup;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(GroupAccessory.class)
public abstract class GroupAccessory_ {

	public static volatile SingularAttribute<GroupAccessory, Integer> quantity;
	public static volatile SingularAttribute<GroupAccessory, Integer> id;
	public static volatile SingularAttribute<GroupAccessory, Accessory> accessory;
	public static volatile SingularAttribute<GroupAccessory, JobItemGroup> group;

	public static final String QUANTITY = "quantity";
	public static final String ID = "id";
	public static final String ACCESSORY = "accessory";
	public static final String GROUP = "group";

}

