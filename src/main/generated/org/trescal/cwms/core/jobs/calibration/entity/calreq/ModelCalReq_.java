package org.trescal.cwms.core.jobs.calibration.entity.calreq;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ModelCalReq.class)
public abstract class ModelCalReq_ extends org.trescal.cwms.core.jobs.calibration.entity.calreq.CalReq_ {

	public static volatile SingularAttribute<ModelCalReq, InstrumentModel> model;

	public static final String MODEL = "model";

}

