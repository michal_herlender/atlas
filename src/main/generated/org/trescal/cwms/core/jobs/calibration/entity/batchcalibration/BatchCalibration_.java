package org.trescal.cwms.core.jobs.calibration.entity.batchcalibration;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.CalibrationProcess;
import org.trescal.cwms.core.jobs.job.entity.job.Job;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BatchCalibration.class)
public abstract class BatchCalibration_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<BatchCalibration, Contact> owner;
	public static volatile SingularAttribute<BatchCalibration, CalibrationProcess> process;
	public static volatile SingularAttribute<BatchCalibration, Boolean> passwordAccepted;
	public static volatile ListAttribute<BatchCalibration, Calibration> cals;
	public static volatile SingularAttribute<BatchCalibration, Date> lastLabels;
	public static volatile SingularAttribute<BatchCalibration, Date> lastCertify;
	public static volatile SingularAttribute<BatchCalibration, Boolean> chooseNext;
	public static volatile SingularAttribute<BatchCalibration, Date> lastInvoice;
	public static volatile SingularAttribute<BatchCalibration, Date> lastDespatch;
	public static volatile SingularAttribute<BatchCalibration, Date> beginTime;
	public static volatile SingularAttribute<BatchCalibration, Integer> id;
	public static volatile SingularAttribute<BatchCalibration, Job> job;
	public static volatile SingularAttribute<BatchCalibration, Date> calDate;

	public static final String OWNER = "owner";
	public static final String PROCESS = "process";
	public static final String PASSWORD_ACCEPTED = "passwordAccepted";
	public static final String CALS = "cals";
	public static final String LAST_LABELS = "lastLabels";
	public static final String LAST_CERTIFY = "lastCertify";
	public static final String CHOOSE_NEXT = "chooseNext";
	public static final String LAST_INVOICE = "lastInvoice";
	public static final String LAST_DESPATCH = "lastDespatch";
	public static final String BEGIN_TIME = "beginTime";
	public static final String ID = "id";
	public static final String JOB = "job";
	public static final String CAL_DATE = "calDate";

}

