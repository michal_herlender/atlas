package org.trescal.cwms.core.jobs.contractreview.entity.costs;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ContractReviewLinkedCost.class)
public abstract class ContractReviewLinkedCost_ {

	public static volatile SingularAttribute<ContractReviewLinkedCost, Integer> id;

	public static final String ID = "id";

}

