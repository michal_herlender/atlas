package org.trescal.cwms.core.jobs.job.entity.abstractpo;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AbstractPO.class)
public abstract class AbstractPO_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<AbstractPO, String> comment;
	public static volatile SingularAttribute<AbstractPO, String> poNumber;
	public static volatile SingularAttribute<AbstractPO, Integer> poId;

	public static final String COMMENT = "comment";
	public static final String PO_NUMBER = "poNumber";
	public static final String PO_ID = "poId";

}

