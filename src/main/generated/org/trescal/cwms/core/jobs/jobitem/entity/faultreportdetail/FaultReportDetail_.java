package org.trescal.cwms.core.jobs.jobitem.entity.faultreportdetail;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(FaultReportDetail.class)
public abstract class FaultReportDetail_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<FaultReportDetail, Contact> recordedBy;
	public static volatile SingularAttribute<FaultReportDetail, FaultReport> faultRep;
	public static volatile SingularAttribute<FaultReportDetail, String> description;
	public static volatile SingularAttribute<FaultReportDetail, Boolean> active;
	public static volatile SingularAttribute<FaultReportDetail, Integer> id;

	public static final String RECORDED_BY = "recordedBy";
	public static final String FAULT_REP = "faultRep";
	public static final String DESCRIPTION = "description";
	public static final String ACTIVE = "active";
	public static final String ID = "id";

}

