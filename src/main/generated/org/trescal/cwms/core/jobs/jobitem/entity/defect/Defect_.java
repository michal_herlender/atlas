package org.trescal.cwms.core.jobs.jobitem.entity.defect;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Defect.class)
public abstract class Defect_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<Defect, Integer> id;
	public static volatile SingularAttribute<Defect, String> desc;

	public static final String ID = "id";
	public static final String DESC = "desc";

}

