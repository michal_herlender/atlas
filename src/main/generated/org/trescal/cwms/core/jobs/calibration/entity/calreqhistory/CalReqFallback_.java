package org.trescal.cwms.core.jobs.calibration.entity.calreqhistory;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CalReqFallback.class)
public abstract class CalReqFallback_ extends org.trescal.cwms.core.jobs.calibration.entity.calreqhistory.CalReqHistory_ {

	public static volatile SingularAttribute<CalReqFallback, Instrument> fallbackFromInst;
	public static volatile SingularAttribute<CalReqFallback, JobItem> fallbackFromJobItem;

	public static final String FALLBACK_FROM_INST = "fallbackFromInst";
	public static final String FALLBACK_FROM_JOB_ITEM = "fallbackFromJobItem";

}

