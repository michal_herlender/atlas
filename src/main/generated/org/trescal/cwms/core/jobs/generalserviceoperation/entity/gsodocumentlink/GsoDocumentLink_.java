package org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocumentlink;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocument.GsoDocument;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(GsoDocumentLink.class)
public abstract class GsoDocumentLink_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<GsoDocumentLink, Integer> id;
	public static volatile SingularAttribute<GsoDocumentLink, GsoDocument> gsoDocument;
	public static volatile SingularAttribute<GsoDocumentLink, JobItem> ji;

	public static final String ID = "id";
	public static final String GSO_DOCUMENT = "gsoDocument";
	public static final String JI = "ji";

}

