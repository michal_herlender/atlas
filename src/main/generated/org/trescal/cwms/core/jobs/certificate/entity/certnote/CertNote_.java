package org.trescal.cwms.core.jobs.certificate.entity.certnote;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CertNote.class)
public abstract class CertNote_ extends org.trescal.cwms.core.system.entity.note.Note_ {

	public static volatile SingularAttribute<CertNote, Certificate> cert;

	public static final String CERT = "cert";

}

