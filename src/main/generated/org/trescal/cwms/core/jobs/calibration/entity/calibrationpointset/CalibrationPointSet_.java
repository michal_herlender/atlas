package org.trescal.cwms.core.jobs.calibration.entity.calibrationpointset;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationpoint.CalibrationPoint;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CalibrationPointSet.class)
public abstract class CalibrationPointSet_ extends org.trescal.cwms.core.jobs.calibration.entity.pointset.PointSet_ {

	public static volatile SetAttribute<CalibrationPointSet, CalibrationPoint> points;

	public static final String POINTS = "points";

}

