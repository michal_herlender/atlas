package org.trescal.cwms.core.jobs.contractreview.entity.costs.purchasecost;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.quotation.entity.costs.purchasecost.QuotationPurchaseCost;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ContractReviewLinkedPurchaseCost.class)
public abstract class ContractReviewLinkedPurchaseCost_ extends org.trescal.cwms.core.jobs.contractreview.entity.costs.ContractReviewLinkedCost_ {

	public static volatile SingularAttribute<ContractReviewLinkedPurchaseCost, ContractReviewPurchaseCost> purchaseCost;
	public static volatile SingularAttribute<ContractReviewLinkedPurchaseCost, QuotationPurchaseCost> quotationPurchaseCost;

	public static final String PURCHASE_COST = "purchaseCost";
	public static final String QUOTATION_PURCHASE_COST = "quotationPurchaseCost";

}

