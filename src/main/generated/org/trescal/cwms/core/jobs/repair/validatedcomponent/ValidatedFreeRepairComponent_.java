package org.trescal.cwms.core.jobs.repair.validatedcomponent;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.repair.component.FreeRepairComponent;
import org.trescal.cwms.core.jobs.repair.component.enums.RepairComponentStateEnum;
import org.trescal.cwms.core.jobs.repair.validatedoperation.ValidatedFreeRepairOperation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ValidatedFreeRepairComponent.class)
public abstract class ValidatedFreeRepairComponent_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<ValidatedFreeRepairComponent, Integer> validatedComponentId;
	public static volatile SingularAttribute<ValidatedFreeRepairComponent, Integer> quantity;
	public static volatile SingularAttribute<ValidatedFreeRepairComponent, BigDecimal> cost;
	public static volatile SingularAttribute<ValidatedFreeRepairComponent, RepairComponentStateEnum> validatedStatus;
	public static volatile SingularAttribute<ValidatedFreeRepairComponent, ValidatedFreeRepairOperation> validatedFreeRepairOperation;
	public static volatile SingularAttribute<ValidatedFreeRepairComponent, FreeRepairComponent> freeRepairComponent;

	public static final String VALIDATED_COMPONENT_ID = "validatedComponentId";
	public static final String QUANTITY = "quantity";
	public static final String COST = "cost";
	public static final String VALIDATED_STATUS = "validatedStatus";
	public static final String VALIDATED_FREE_REPAIR_OPERATION = "validatedFreeRepairOperation";
	public static final String FREE_REPAIR_COMPONENT = "freeRepairComponent";

}

