package org.trescal.cwms.core.jobs.job.entity.jobstatus;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobStatus.class)
public abstract class JobStatus_ extends org.trescal.cwms.core.system.entity.basestatus.BaseStatus_ {

	public static volatile ListAttribute<JobStatus, Job> jobs;
	public static volatile SingularAttribute<JobStatus, Boolean> readyToInvoice;
	public static volatile SetAttribute<JobStatus, Translation> nametranslations;
	public static volatile SetAttribute<JobStatus, Translation> descriptiontranslations;
	public static volatile SingularAttribute<JobStatus, Boolean> complete;

	public static final String JOBS = "jobs";
	public static final String READY_TO_INVOICE = "readyToInvoice";
	public static final String NAMETRANSLATIONS = "nametranslations";
	public static final String DESCRIPTIONTRANSLATIONS = "descriptiontranslations";
	public static final String COMPLETE = "complete";

}

