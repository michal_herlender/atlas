package org.trescal.cwms.core.jobs.repair.repairinspectionreport;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.repair.operation.FreeRepairOperation;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.RepairCompletionReport;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RepairInspectionReport.class)
public abstract class RepairInspectionReport_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<RepairInspectionReport, Boolean> validatedByManager;
	public static volatile SingularAttribute<RepairInspectionReport, Contact> csr;
	public static volatile SingularAttribute<RepairInspectionReport, String> externalInstructions;
	public static volatile SingularAttribute<RepairInspectionReport, String> clientInstructions;
	public static volatile SingularAttribute<RepairInspectionReport, String> internalInspection;
	public static volatile SingularAttribute<RepairInspectionReport, Boolean> manufacturerWarranty;
	public static volatile SingularAttribute<RepairInspectionReport, RepairCompletionReport> repairCompletionReport;
	public static volatile SingularAttribute<RepairInspectionReport, String> misuseComment;
	public static volatile SingularAttribute<RepairInspectionReport, Integer> leadTime;
	public static volatile SingularAttribute<RepairInspectionReport, Boolean> reviewedByCsr;
	public static volatile SingularAttribute<RepairInspectionReport, Boolean> completedByTechnician;
	public static volatile SetAttribute<RepairInspectionReport, FreeRepairOperation> freeRepairOperations;
	public static volatile SingularAttribute<RepairInspectionReport, Date> validatedByManagerOn;
	public static volatile SingularAttribute<RepairInspectionReport, String> clientDescription;
	public static volatile SingularAttribute<RepairInspectionReport, Date> estimatedReturnDate;
	public static volatile SingularAttribute<RepairInspectionReport, String> rirNumber;
	public static volatile SingularAttribute<RepairInspectionReport, String> internalOperations;
	public static volatile SingularAttribute<RepairInspectionReport, Integer> rirId;
	public static volatile SingularAttribute<RepairInspectionReport, Contact> manager;
	public static volatile SingularAttribute<RepairInspectionReport, Date> completedByTechnicianOn;
	public static volatile SingularAttribute<RepairInspectionReport, Contact> technician;
	public static volatile SingularAttribute<RepairInspectionReport, Date> creationDate;
	public static volatile SingularAttribute<RepairInspectionReport, Boolean> misuse;
	public static volatile SingularAttribute<RepairInspectionReport, Boolean> trescalWarranty;
	public static volatile SingularAttribute<RepairInspectionReport, String> internalInstructions;
	public static volatile SingularAttribute<RepairInspectionReport, String> internalDescription;
	public static volatile SingularAttribute<RepairInspectionReport, Date> reviewedByCsrOn;
	public static volatile SingularAttribute<RepairInspectionReport, Contact> createdBy;
	public static volatile SingularAttribute<RepairInspectionReport, JobItem> ji;

	public static final String VALIDATED_BY_MANAGER = "validatedByManager";
	public static final String CSR = "csr";
	public static final String EXTERNAL_INSTRUCTIONS = "externalInstructions";
	public static final String CLIENT_INSTRUCTIONS = "clientInstructions";
	public static final String INTERNAL_INSPECTION = "internalInspection";
	public static final String MANUFACTURER_WARRANTY = "manufacturerWarranty";
	public static final String REPAIR_COMPLETION_REPORT = "repairCompletionReport";
	public static final String MISUSE_COMMENT = "misuseComment";
	public static final String LEAD_TIME = "leadTime";
	public static final String REVIEWED_BY_CSR = "reviewedByCsr";
	public static final String COMPLETED_BY_TECHNICIAN = "completedByTechnician";
	public static final String FREE_REPAIR_OPERATIONS = "freeRepairOperations";
	public static final String VALIDATED_BY_MANAGER_ON = "validatedByManagerOn";
	public static final String CLIENT_DESCRIPTION = "clientDescription";
	public static final String ESTIMATED_RETURN_DATE = "estimatedReturnDate";
	public static final String RIR_NUMBER = "rirNumber";
	public static final String INTERNAL_OPERATIONS = "internalOperations";
	public static final String RIR_ID = "rirId";
	public static final String MANAGER = "manager";
	public static final String COMPLETED_BY_TECHNICIAN_ON = "completedByTechnicianOn";
	public static final String TECHNICIAN = "technician";
	public static final String CREATION_DATE = "creationDate";
	public static final String MISUSE = "misuse";
	public static final String TRESCAL_WARRANTY = "trescalWarranty";
	public static final String INTERNAL_INSTRUCTIONS = "internalInstructions";
	public static final String INTERNAL_DESCRIPTION = "internalDescription";
	public static final String REVIEWED_BY_CSR_ON = "reviewedByCsrOn";
	public static final String CREATED_BY = "createdBy";
	public static final String JI = "ji";

}

