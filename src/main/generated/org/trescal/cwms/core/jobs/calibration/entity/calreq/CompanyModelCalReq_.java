package org.trescal.cwms.core.jobs.calibration.entity.calreq;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CompanyModelCalReq.class)
public abstract class CompanyModelCalReq_ extends org.trescal.cwms.core.jobs.calibration.entity.calreq.CalReq_ {

	public static volatile SingularAttribute<CompanyModelCalReq, Company> comp;
	public static volatile SingularAttribute<CompanyModelCalReq, InstrumentModel> model;

	public static final String COMP = "comp";
	public static final String MODEL = "model";

}

