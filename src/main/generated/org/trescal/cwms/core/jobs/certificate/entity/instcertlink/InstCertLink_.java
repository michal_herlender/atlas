package org.trescal.cwms.core.jobs.certificate.entity.instcertlink;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstCertLink.class)
public abstract class InstCertLink_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<InstCertLink, Instrument> inst;
	public static volatile SingularAttribute<InstCertLink, Certificate> cert;
	public static volatile SingularAttribute<InstCertLink, Integer> id;

	public static final String INST = "inst";
	public static final String CERT = "cert";
	public static final String ID = "id";

}

