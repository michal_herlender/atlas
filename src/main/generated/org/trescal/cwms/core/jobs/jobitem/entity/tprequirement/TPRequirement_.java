package org.trescal.cwms.core.jobs.jobitem.entity.tprequirement;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TPRequirement.class)
public abstract class TPRequirement_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<TPRequirement, Boolean> repair;
	public static volatile SingularAttribute<TPRequirement, Contact> updatedBy;
	public static volatile SingularAttribute<TPRequirement, Date> created;
	public static volatile SingularAttribute<TPRequirement, JobDeliveryItem> deliveryItem;
	public static volatile SingularAttribute<TPRequirement, Date> lastUpdated;
	public static volatile SingularAttribute<TPRequirement, Contact> recordedBy;
	public static volatile SingularAttribute<TPRequirement, TPQuotation> quote;
	public static volatile SingularAttribute<TPRequirement, Boolean> investigation;
	public static volatile SingularAttribute<TPRequirement, Boolean> adjustment;
	public static volatile SingularAttribute<TPRequirement, Integer> id;
	public static volatile SingularAttribute<TPRequirement, Boolean> calibration;
	public static volatile SingularAttribute<TPRequirement, ActionOutcome> outcome;
	public static volatile SingularAttribute<TPRequirement, JobItem> ji;

	public static final String REPAIR = "repair";
	public static final String UPDATED_BY = "updatedBy";
	public static final String CREATED = "created";
	public static final String DELIVERY_ITEM = "deliveryItem";
	public static final String LAST_UPDATED = "lastUpdated";
	public static final String RECORDED_BY = "recordedBy";
	public static final String QUOTE = "quote";
	public static final String INVESTIGATION = "investigation";
	public static final String ADJUSTMENT = "adjustment";
	public static final String ID = "id";
	public static final String CALIBRATION = "calibration";
	public static final String OUTCOME = "outcome";
	public static final String JI = "ji";

}

