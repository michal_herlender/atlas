package org.trescal.cwms.core.jobs.calibration.entity.calibrationstatus;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CalibrationStatus.class)
public abstract class CalibrationStatus_ extends org.trescal.cwms.core.system.entity.status.Status_ {

	public static volatile ListAttribute<CalibrationStatus, Calibration> calibrations;

	public static final String CALIBRATIONS = "calibrations";

}

