package org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CalibrationProcess.class)
public abstract class CalibrationProcess_ {

	public static volatile SingularAttribute<CalibrationProcess, Boolean> actionAfterIssue;
	public static volatile SingularAttribute<CalibrationProcess, String> process;
	public static volatile SingularAttribute<CalibrationProcess, Boolean> uploadAfterIssue;
	public static volatile SingularAttribute<CalibrationProcess, String> description;
	public static volatile SingularAttribute<CalibrationProcess, Boolean> signingSupported;
	public static volatile SingularAttribute<CalibrationProcess, Boolean> applicationComponent;
	public static volatile SingularAttribute<CalibrationProcess, Boolean> quickSign;
	public static volatile SingularAttribute<CalibrationProcess, String> toolLink;
	public static volatile SingularAttribute<CalibrationProcess, Boolean> uploadBeforeIssue;
	public static volatile SingularAttribute<CalibrationProcess, String> toolContinueLink;
	public static volatile ListAttribute<CalibrationProcess, Calibration> calibrations;
	public static volatile SingularAttribute<CalibrationProcess, String> fileExtension;
	public static volatile SingularAttribute<CalibrationProcess, Boolean> issueImmediately;
	public static volatile SingularAttribute<CalibrationProcess, Integer> id;
	public static volatile SingularAttribute<CalibrationProcess, Boolean> signOnIssue;

	public static final String ACTION_AFTER_ISSUE = "actionAfterIssue";
	public static final String PROCESS = "process";
	public static final String UPLOAD_AFTER_ISSUE = "uploadAfterIssue";
	public static final String DESCRIPTION = "description";
	public static final String SIGNING_SUPPORTED = "signingSupported";
	public static final String APPLICATION_COMPONENT = "applicationComponent";
	public static final String QUICK_SIGN = "quickSign";
	public static final String TOOL_LINK = "toolLink";
	public static final String UPLOAD_BEFORE_ISSUE = "uploadBeforeIssue";
	public static final String TOOL_CONTINUE_LINK = "toolContinueLink";
	public static final String CALIBRATIONS = "calibrations";
	public static final String FILE_EXTENSION = "fileExtension";
	public static final String ISSUE_IMMEDIATELY = "issueImmediately";
	public static final String ID = "id";
	public static final String SIGN_ON_ISSUE = "signOnIssue";

}

