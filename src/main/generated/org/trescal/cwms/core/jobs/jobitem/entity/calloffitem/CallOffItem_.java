package org.trescal.cwms.core.jobs.jobitem.entity.calloffitem;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CallOffItem.class)
public abstract class CallOffItem_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<CallOffItem, JobItem> offItem;
	public static volatile SingularAttribute<CallOffItem, String> reason;
	public static volatile SingularAttribute<CallOffItem, Date> offDate;
	public static volatile SingularAttribute<CallOffItem, Date> onDate;
	public static volatile SingularAttribute<CallOffItem, JobItem> onItem;
	public static volatile SingularAttribute<CallOffItem, Boolean> active;
	public static volatile SingularAttribute<CallOffItem, Integer> id;
	public static volatile SingularAttribute<CallOffItem, Contact> calledOffBy;

	public static final String OFF_ITEM = "offItem";
	public static final String REASON = "reason";
	public static final String OFF_DATE = "offDate";
	public static final String ON_DATE = "onDate";
	public static final String ON_ITEM = "onItem";
	public static final String ACTIVE = "active";
	public static final String ID = "id";
	public static final String CALLED_OFF_BY = "calledOffBy";

}

