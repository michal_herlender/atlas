package org.trescal.cwms.core.jobs.calibration.entity.calreqhistory;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CalReq;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CalReqHistory.class)
public abstract class CalReqHistory_ {

	public static volatile SingularAttribute<CalReqHistory, CalReq> newCalReq;
	public static volatile SingularAttribute<CalReqHistory, Contact> changeBy;
	public static volatile SingularAttribute<CalReqHistory, Integer> id;
	public static volatile SingularAttribute<CalReqHistory, Date> changeOn;
	public static volatile SingularAttribute<CalReqHistory, CalReq> oldCalReq;

	public static final String NEW_CAL_REQ = "newCalReq";
	public static final String CHANGE_BY = "changeBy";
	public static final String ID = "id";
	public static final String CHANGE_ON = "changeOn";
	public static final String OLD_CAL_REQ = "oldCalReq";

}

