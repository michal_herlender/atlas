package org.trescal.cwms.core.jobs.job.entity.bpo;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.jobs.job.entity.jobbpolink.JobBPOLink;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitempo.JobExpenseItemPO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.JobItemPO;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BPO.class)
public abstract class BPO_ extends org.trescal.cwms.core.jobs.job.entity.abstractpo.AbstractPO_ {

	public static volatile SingularAttribute<BPO, Boolean> expirationWarningSent;
	public static volatile SingularAttribute<BPO, Date> durationFrom;
	public static volatile SingularAttribute<BPO, Date> expirationWarningDate;
	public static volatile SetAttribute<BPO, JobExpenseItemPO> expenseItemPOs;
	public static volatile SingularAttribute<BPO, Contact> contact;
	public static volatile SetAttribute<BPO, JobItemPO> jobItemPOs;
	public static volatile SetAttribute<BPO, JobBPOLink> jobLinks;
	public static volatile SingularAttribute<BPO, Date> durationTo;
	public static volatile SingularAttribute<BPO, BigDecimal> limitAmount;
	public static volatile SingularAttribute<BPO, Boolean> active;
	public static volatile SingularAttribute<BPO, Company> company;
	public static volatile SingularAttribute<BPO, Subdiv> subdiv;

	public static final String EXPIRATION_WARNING_SENT = "expirationWarningSent";
	public static final String DURATION_FROM = "durationFrom";
	public static final String EXPIRATION_WARNING_DATE = "expirationWarningDate";
	public static final String EXPENSE_ITEM_POS = "expenseItemPOs";
	public static final String CONTACT = "contact";
	public static final String JOB_ITEM_POS = "jobItemPOs";
	public static final String JOB_LINKS = "jobLinks";
	public static final String DURATION_TO = "durationTo";
	public static final String LIMIT_AMOUNT = "limitAmount";
	public static final String ACTIVE = "active";
	public static final String COMPANY = "company";
	public static final String SUBDIV = "subdiv";

}

