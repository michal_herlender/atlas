package org.trescal.cwms.core.jobs.calibration.entity.callink;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CalLink.class)
public abstract class CalLink_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<CalLink, Boolean> certIssued;
	public static volatile SingularAttribute<CalLink, Integer> timeSpent;
	public static volatile SingularAttribute<CalLink, Boolean> mainItem;
	public static volatile SingularAttribute<CalLink, Integer> id;
	public static volatile SingularAttribute<CalLink, ActionOutcome> outcome;
	public static volatile SingularAttribute<CalLink, Calibration> cal;
	public static volatile SingularAttribute<CalLink, JobItem> ji;

	public static final String CERT_ISSUED = "certIssued";
	public static final String TIME_SPENT = "timeSpent";
	public static final String MAIN_ITEM = "mainItem";
	public static final String ID = "id";
	public static final String OUTCOME = "outcome";
	public static final String CAL = "cal";
	public static final String JI = "ji";

}

