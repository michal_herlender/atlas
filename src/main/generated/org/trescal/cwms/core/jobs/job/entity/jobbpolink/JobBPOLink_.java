package org.trescal.cwms.core.jobs.job.entity.jobbpolink;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.job.Job;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobBPOLink.class)
public abstract class JobBPOLink_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<JobBPOLink, BPO> bpo;
	public static volatile SingularAttribute<JobBPOLink, Boolean> defaultForJob;
	public static volatile SingularAttribute<JobBPOLink, Integer> id;
	public static volatile SingularAttribute<JobBPOLink, Job> job;
	public static volatile SingularAttribute<JobBPOLink, Date> createdOn;

	public static final String BPO = "bpo";
	public static final String DEFAULT_FOR_JOB = "defaultForJob";
	public static final String ID = "id";
	public static final String JOB = "job";
	public static final String CREATED_ON = "createdOn";

}

