package org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ContractReviewCalibrationCost.class)
public abstract class ContractReviewCalibrationCost_ extends org.trescal.cwms.core.pricing.entity.costs.base.CalCost_ {

	public static volatile SingularAttribute<ContractReviewCalibrationCost, ContractReviewLinkedCalibrationCost> linkedCost;
	public static volatile SingularAttribute<ContractReviewCalibrationCost, JobItem> jobitem;
	public static volatile SingularAttribute<ContractReviewCalibrationCost, SupportedCurrency> currency;

	public static final String LINKED_COST = "linkedCost";
	public static final String JOBITEM = "jobitem";
	public static final String CURRENCY = "currency";

}

