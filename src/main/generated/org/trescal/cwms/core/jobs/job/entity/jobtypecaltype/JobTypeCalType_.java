package org.trescal.cwms.core.jobs.job.entity.jobtypecaltype;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobTypeCalType.class)
public abstract class JobTypeCalType_ {

	public static volatile SingularAttribute<JobTypeCalType, CalibrationType> calType;
	public static volatile SingularAttribute<JobTypeCalType, Boolean> defaultValue;
	public static volatile SingularAttribute<JobTypeCalType, Integer> id;
	public static volatile SingularAttribute<JobTypeCalType, JobType> jobType;

	public static final String CAL_TYPE = "calType";
	public static final String DEFAULT_VALUE = "defaultValue";
	public static final String ID = "id";
	public static final String JOB_TYPE = "jobType";

}

