package org.trescal.cwms.core.jobs.contractreview.entity.costs.adjcost;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ContractReviewLinkedAdjustmentCost.class)
public abstract class ContractReviewLinkedAdjustmentCost_ extends org.trescal.cwms.core.jobs.contractreview.entity.costs.ContractReviewLinkedCost_ {

	public static volatile SingularAttribute<ContractReviewLinkedAdjustmentCost, ContractReviewAdjustmentCost> adjustmentCost;

	public static final String ADJUSTMENT_COST = "adjustmentCost";

}

