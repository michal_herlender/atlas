package org.trescal.cwms.core.jobs.contractreview.entity.costs.repcost;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ContractReviewRepairCost.class)
public abstract class ContractReviewRepairCost_ extends org.trescal.cwms.core.pricing.entity.costs.base.RepCost_ {

	public static volatile SingularAttribute<ContractReviewRepairCost, ContractReviewLinkedRepairCost> linkedCost;
	public static volatile SingularAttribute<ContractReviewRepairCost, JobItem> jobitem;
	public static volatile SingularAttribute<ContractReviewRepairCost, CostSource> costSrc;

	public static final String LINKED_COST = "linkedCost";
	public static final String JOBITEM = "jobitem";
	public static final String COST_SRC = "costSrc";

}

