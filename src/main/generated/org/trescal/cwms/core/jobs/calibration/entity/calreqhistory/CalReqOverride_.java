package org.trescal.cwms.core.jobs.calibration.entity.calreqhistory;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CalReqOverride.class)
public abstract class CalReqOverride_ extends org.trescal.cwms.core.jobs.calibration.entity.calreqhistory.CalReqHistory_ {

	public static volatile SingularAttribute<CalReqOverride, Instrument> overrideOnInst;
	public static volatile SingularAttribute<CalReqOverride, JobItem> overrideOnJobItem;

	public static final String OVERRIDE_ON_INST = "overrideOnInst";
	public static final String OVERRIDE_ON_JOB_ITEM = "overrideOnJobItem";

}

