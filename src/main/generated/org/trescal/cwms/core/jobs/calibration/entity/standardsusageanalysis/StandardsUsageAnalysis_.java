package org.trescal.cwms.core.jobs.calibration.entity.standardsusageanalysis;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.calibration.entity.standardsusageanalysisstatus.StandardsUsageAnalysisStatus;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(StandardsUsageAnalysis.class)
public abstract class StandardsUsageAnalysis_ {

	public static volatile SingularAttribute<StandardsUsageAnalysis, Certificate> certificate;
	public static volatile SingularAttribute<StandardsUsageAnalysis, Date> finishDate;
	public static volatile SingularAttribute<StandardsUsageAnalysis, Instrument> instrument;
	public static volatile SingularAttribute<StandardsUsageAnalysis, Integer> id;
	public static volatile SingularAttribute<StandardsUsageAnalysis, Date> startDate;
	public static volatile SingularAttribute<StandardsUsageAnalysis, StandardsUsageAnalysisStatus> status;

	public static final String CERTIFICATE = "certificate";
	public static final String FINISH_DATE = "finishDate";
	public static final String INSTRUMENT = "instrument";
	public static final String ID = "id";
	public static final String START_DATE = "startDate";
	public static final String STATUS = "status";

}

