package org.trescal.cwms.core.jobs.repair.operation;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.jobs.repair.component.FreeRepairComponent;
import org.trescal.cwms.core.jobs.repair.operation.enums.RepairOperationStateEnum;
import org.trescal.cwms.core.jobs.repair.operation.enums.RepairOperationTypeEnum;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.RepairCompletionReport;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.RepairInspectionReport;
import org.trescal.cwms.core.jobs.repair.validatedoperation.ValidatedFreeRepairOperation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(FreeRepairOperation.class)
public abstract class FreeRepairOperation_ {

	public static volatile SingularAttribute<FreeRepairOperation, RepairOperationStateEnum> operationStatus;
	public static volatile SingularAttribute<FreeRepairOperation, BigDecimal> cost;
	public static volatile SingularAttribute<FreeRepairOperation, Company> tpCompany;
	public static volatile SingularAttribute<FreeRepairOperation, RepairInspectionReport> repairInspectionReport;
	public static volatile SingularAttribute<FreeRepairOperation, RepairCompletionReport> repairCompletionReport;
	public static volatile SingularAttribute<FreeRepairOperation, Integer> labourTime;
	public static volatile SetAttribute<FreeRepairOperation, FreeRepairComponent> freeRepairComponents;
	public static volatile SingularAttribute<FreeRepairOperation, ValidatedFreeRepairOperation> validatedFreeRepairOperation;
	public static volatile SingularAttribute<FreeRepairOperation, String> name;
	public static volatile SingularAttribute<FreeRepairOperation, Integer> operationId;
	public static volatile SingularAttribute<FreeRepairOperation, RepairOperationTypeEnum> operationType;
	public static volatile SingularAttribute<FreeRepairOperation, Integer> position;
	public static volatile SingularAttribute<FreeRepairOperation, Boolean> addedAfterRiRValidation;

	public static final String OPERATION_STATUS = "operationStatus";
	public static final String COST = "cost";
	public static final String TP_COMPANY = "tpCompany";
	public static final String REPAIR_INSPECTION_REPORT = "repairInspectionReport";
	public static final String REPAIR_COMPLETION_REPORT = "repairCompletionReport";
	public static final String LABOUR_TIME = "labourTime";
	public static final String FREE_REPAIR_COMPONENTS = "freeRepairComponents";
	public static final String VALIDATED_FREE_REPAIR_OPERATION = "validatedFreeRepairOperation";
	public static final String NAME = "name";
	public static final String OPERATION_ID = "operationId";
	public static final String OPERATION_TYPE = "operationType";
	public static final String POSITION = "position";
	public static final String ADDED_AFTER_RI_RVALIDATION = "addedAfterRiRValidation";

}

