package org.trescal.cwms.core.jobs.job.entity.jobinstructionlink;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.system.entity.instruction.Instruction;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobInstructionLink.class)
public abstract class JobInstructionLink_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SingularAttribute<JobInstructionLink, Instruction> instruction;
	public static volatile SingularAttribute<JobInstructionLink, Integer> id;
	public static volatile SingularAttribute<JobInstructionLink, Job> job;

	public static final String INSTRUCTION = "instruction";
	public static final String ID = "id";
	public static final String JOB = "job";

}

