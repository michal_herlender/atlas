package org.trescal.cwms.core.jobs.contractreview.entity.costs.adjcost;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ContractReviewAdjustmentCost.class)
public abstract class ContractReviewAdjustmentCost_ extends org.trescal.cwms.core.pricing.entity.costs.base.AdjCost_ {

	public static volatile SingularAttribute<ContractReviewAdjustmentCost, ContractReviewLinkedAdjustmentCost> linkedCost;
	public static volatile SingularAttribute<ContractReviewAdjustmentCost, JobItem> jobitem;
	public static volatile SingularAttribute<ContractReviewAdjustmentCost, CostSource> costSrc;

	public static final String LINKED_COST = "linkedCost";
	public static final String JOBITEM = "jobitem";
	public static final String COST_SRC = "costSrc";

}

