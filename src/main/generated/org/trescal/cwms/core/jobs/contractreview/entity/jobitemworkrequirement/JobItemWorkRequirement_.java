package org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.WorkRequirement;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobItemWorkRequirement.class)
public abstract class JobItemWorkRequirement_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<JobItemWorkRequirement, JobItem> jobitem;
	public static volatile SingularAttribute<JobItemWorkRequirement, WorkRequirement> workRequirement;
	public static volatile SingularAttribute<JobItemWorkRequirement, Integer> id;

	public static final String JOBITEM = "jobitem";
	public static final String WORK_REQUIREMENT = "workRequirement";
	public static final String ID = "id";

}

