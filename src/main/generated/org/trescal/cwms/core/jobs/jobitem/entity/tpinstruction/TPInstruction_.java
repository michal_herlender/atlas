package org.trescal.cwms.core.jobs.jobitem.entity.tpinstruction;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TPInstruction.class)
public abstract class TPInstruction_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<TPInstruction, Contact> recordedBy;
	public static volatile SingularAttribute<TPInstruction, String> instruction;
	public static volatile SingularAttribute<TPInstruction, Boolean> active;
	public static volatile SingularAttribute<TPInstruction, Integer> id;
	public static volatile SingularAttribute<TPInstruction, JobItem> ji;
	public static volatile SingularAttribute<TPInstruction, Boolean> showOnTPDelNote;

	public static final String RECORDED_BY = "recordedBy";
	public static final String INSTRUCTION = "instruction";
	public static final String ACTIVE = "active";
	public static final String ID = "id";
	public static final String JI = "ji";
	public static final String SHOW_ON_TP_DEL_NOTE = "showOnTPDelNote";

}

