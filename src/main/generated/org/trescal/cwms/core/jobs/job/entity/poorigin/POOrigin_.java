package org.trescal.cwms.core.jobs.job.entity.poorigin;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(POOrigin.class)
public abstract class POOrigin_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SingularAttribute<POOrigin, TPQuotation> tpQuotation;
	public static volatile SingularAttribute<POOrigin, PurchaseOrder> purchaseOrder;
	public static volatile SingularAttribute<POOrigin, Long> id;
	public static volatile SingularAttribute<POOrigin, Job> job;

	public static final String TP_QUOTATION = "tpQuotation";
	public static final String PURCHASE_ORDER = "purchaseOrder";
	public static final String ID = "id";
	public static final String JOB = "job";

}

