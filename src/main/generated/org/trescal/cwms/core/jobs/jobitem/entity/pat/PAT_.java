package org.trescal.cwms.core.jobs.jobitem.entity.pat;

import java.math.BigDecimal;
import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PAT.class)
public abstract class PAT_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<PAT, Boolean> insulationOk;
	public static volatile SingularAttribute<PAT, Contact> testedBy;
	public static volatile SingularAttribute<PAT, Boolean> fuseOk;
	public static volatile SingularAttribute<PAT, BigDecimal> bondingReading;
	public static volatile SingularAttribute<PAT, Boolean> plugOk;
	public static volatile SingularAttribute<PAT, String> testEquipment;
	public static volatile SingularAttribute<PAT, Boolean> caseOk;
	public static volatile SingularAttribute<PAT, BigDecimal> insulationReading;
	public static volatile SingularAttribute<PAT, Boolean> safe;
	public static volatile SingularAttribute<PAT, Boolean> bondingOk;
	public static volatile SingularAttribute<PAT, String> comment;
	public static volatile SingularAttribute<PAT, Integer> id;
	public static volatile SingularAttribute<PAT, LocalDate> testDate;
	public static volatile SingularAttribute<PAT, Boolean> cableOk;
	public static volatile SingularAttribute<PAT, JobItem> ji;

	public static final String INSULATION_OK = "insulationOk";
	public static final String TESTED_BY = "testedBy";
	public static final String FUSE_OK = "fuseOk";
	public static final String BONDING_READING = "bondingReading";
	public static final String PLUG_OK = "plugOk";
	public static final String TEST_EQUIPMENT = "testEquipment";
	public static final String CASE_OK = "caseOk";
	public static final String INSULATION_READING = "insulationReading";
	public static final String SAFE = "safe";
	public static final String BONDING_OK = "bondingOk";
	public static final String COMMENT = "comment";
	public static final String ID = "id";
	public static final String TEST_DATE = "testDate";
	public static final String CABLE_OK = "cableOk";
	public static final String JI = "ji";

}

