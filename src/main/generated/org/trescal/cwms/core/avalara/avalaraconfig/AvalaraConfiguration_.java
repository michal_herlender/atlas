package org.trescal.cwms.core.avalara.avalaraconfig;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import net.avalara.avatax.rest.client.enums.AvaTaxEnvironment;
import org.trescal.cwms.core.company.entity.company.Company;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AvalaraConfiguration.class)
public abstract class AvalaraConfiguration_ {

	public static volatile SingularAttribute<AvalaraConfiguration, Integer> accountId;
	public static volatile SingularAttribute<AvalaraConfiguration, String> licenseKey;
	public static volatile SingularAttribute<AvalaraConfiguration, AvaTaxEnvironment> environment;
	public static volatile SingularAttribute<AvalaraConfiguration, Integer> avalaraId;
	public static volatile SingularAttribute<AvalaraConfiguration, String> avalaraCode;
	public static volatile SingularAttribute<AvalaraConfiguration, Company> businessCompany;
	public static volatile SingularAttribute<AvalaraConfiguration, Integer> id;

	public static final String ACCOUNT_ID = "accountId";
	public static final String LICENSE_KEY = "licenseKey";
	public static final String ENVIRONMENT = "environment";
	public static final String AVALARA_ID = "avalaraId";
	public static final String AVALARA_CODE = "avalaraCode";
	public static final String BUSINESS_COMPANY = "businessCompany";
	public static final String ID = "id";

}

