package org.trescal.cwms.core.external.adveso.entity.advesojobitemactivity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AdvesoJobItemActivity.class)
public abstract class AdvesoJobItemActivity_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<AdvesoJobItemActivity, String> jobCostingFileName;
	public static volatile SingularAttribute<AdvesoJobItemActivity, String> lastErrorMessage;
	public static volatile SingularAttribute<AdvesoJobItemActivity, Boolean> isTreated;
	public static volatile SingularAttribute<AdvesoJobItemActivity, Date> lastErrorMessageOn;
	public static volatile SingularAttribute<AdvesoJobItemActivity, String> linkedEntityType;
	public static volatile SingularAttribute<AdvesoJobItemActivity, Boolean> isReceived;
	public static volatile SingularAttribute<AdvesoJobItemActivity, Integer> linkedEntityId;
	public static volatile SingularAttribute<AdvesoJobItemActivity, Boolean> isSent;
	public static volatile SingularAttribute<AdvesoJobItemActivity, Date> treatedOn;
	public static volatile SingularAttribute<AdvesoJobItemActivity, Date> createdOn;
	public static volatile SingularAttribute<AdvesoJobItemActivity, Date> modifiedOn;
	public static volatile SingularAttribute<AdvesoJobItemActivity, Date> sentOn;
	public static volatile SingularAttribute<AdvesoJobItemActivity, Date> receivedOn;
	public static volatile SingularAttribute<AdvesoJobItemActivity, Integer> id;
	public static volatile SingularAttribute<AdvesoJobItemActivity, JobItemAction> jobItemAction;

	public static final String JOB_COSTING_FILE_NAME = "jobCostingFileName";
	public static final String LAST_ERROR_MESSAGE = "lastErrorMessage";
	public static final String IS_TREATED = "isTreated";
	public static final String LAST_ERROR_MESSAGE_ON = "lastErrorMessageOn";
	public static final String LINKED_ENTITY_TYPE = "linkedEntityType";
	public static final String IS_RECEIVED = "isReceived";
	public static final String LINKED_ENTITY_ID = "linkedEntityId";
	public static final String IS_SENT = "isSent";
	public static final String TREATED_ON = "treatedOn";
	public static final String CREATED_ON = "createdOn";
	public static final String MODIFIED_ON = "modifiedOn";
	public static final String SENT_ON = "sentOn";
	public static final String RECEIVED_ON = "receivedOn";
	public static final String ID = "id";
	public static final String JOB_ITEM_ACTION = "jobItemAction";

}

