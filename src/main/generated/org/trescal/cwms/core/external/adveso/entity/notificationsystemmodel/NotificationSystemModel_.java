package org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemfieldchangemodel.NotificationSystemFieldChangeModel;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemEntityClassEnum;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemFieldTypeEnum;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemOperationTypeEnum;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(NotificationSystemModel.class)
public abstract class NotificationSystemModel_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<NotificationSystemModel, NotificationSystemFieldChangeModel> fieldChange;
	public static volatile SingularAttribute<NotificationSystemModel, NotificationSystemEntityClassEnum> entityClass;
	public static volatile SingularAttribute<NotificationSystemModel, String> lastErrorMessage;
	public static volatile SingularAttribute<NotificationSystemModel, Date> lastErrorMessageOn;
	public static volatile SingularAttribute<NotificationSystemModel, Boolean> isReceived;
	public static volatile SingularAttribute<NotificationSystemModel, Boolean> isSent;
	public static volatile SingularAttribute<NotificationSystemModel, Instrument> instrument;
	public static volatile SingularAttribute<NotificationSystemModel, String> entityIdFieldName;
	public static volatile SingularAttribute<NotificationSystemModel, Date> createdOn;
	public static volatile SingularAttribute<NotificationSystemModel, String> entityIdFieldValue;
	public static volatile SingularAttribute<NotificationSystemModel, Contact> jobContact;
	public static volatile SingularAttribute<NotificationSystemModel, JobItem> jobItem;
	public static volatile SingularAttribute<NotificationSystemModel, Date> sentOn;
	public static volatile SingularAttribute<NotificationSystemModel, Date> receivedOn;
	public static volatile SingularAttribute<NotificationSystemModel, NotificationSystemFieldTypeEnum> entityIdFieldType;
	public static volatile SingularAttribute<NotificationSystemModel, NotificationSystemOperationTypeEnum> operationType;
	public static volatile SingularAttribute<NotificationSystemModel, Integer> id;

	public static final String FIELD_CHANGE = "fieldChange";
	public static final String ENTITY_CLASS = "entityClass";
	public static final String LAST_ERROR_MESSAGE = "lastErrorMessage";
	public static final String LAST_ERROR_MESSAGE_ON = "lastErrorMessageOn";
	public static final String IS_RECEIVED = "isReceived";
	public static final String IS_SENT = "isSent";
	public static final String INSTRUMENT = "instrument";
	public static final String ENTITY_ID_FIELD_NAME = "entityIdFieldName";
	public static final String CREATED_ON = "createdOn";
	public static final String ENTITY_ID_FIELD_VALUE = "entityIdFieldValue";
	public static final String JOB_CONTACT = "jobContact";
	public static final String JOB_ITEM = "jobItem";
	public static final String SENT_ON = "sentOn";
	public static final String RECEIVED_ON = "receivedOn";
	public static final String ENTITY_ID_FIELD_TYPE = "entityIdFieldType";
	public static final String OPERATION_TYPE = "operationType";
	public static final String ID = "id";

}

