package org.trescal.cwms.core.external.adveso.entity.notificationsystemfieldchangemodel;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.NotificationSystemModel;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemFieldTypeEnum;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(NotificationSystemFieldChangeModel.class)
public abstract class NotificationSystemFieldChangeModel_ {

	public static volatile SingularAttribute<NotificationSystemFieldChangeModel, NotificationSystemModel> notification;
	public static volatile SingularAttribute<NotificationSystemFieldChangeModel, String> fieldName;
	public static volatile SingularAttribute<NotificationSystemFieldChangeModel, Date> changeDate;
	public static volatile SingularAttribute<NotificationSystemFieldChangeModel, String> newFieldValue;
	public static volatile SingularAttribute<NotificationSystemFieldChangeModel, Integer> id;
	public static volatile SingularAttribute<NotificationSystemFieldChangeModel, String> oldFieldValue;
	public static volatile SingularAttribute<NotificationSystemFieldChangeModel, NotificationSystemFieldTypeEnum> fieldType;

	public static final String NOTIFICATION = "notification";
	public static final String FIELD_NAME = "fieldName";
	public static final String CHANGE_DATE = "changeDate";
	public static final String NEW_FIELD_VALUE = "newFieldValue";
	public static final String ID = "id";
	public static final String OLD_FIELD_VALUE = "oldFieldValue";
	public static final String FIELD_TYPE = "fieldType";

}

