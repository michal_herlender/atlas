package org.trescal.cwms.core.external.adveso.entity.advesotransferableactivity;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.external.adveso.entity.advesooverriddenactivitysetting.AdvesoOverriddenActivitySetting;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AdvesoTransferableActivity.class)
public abstract class AdvesoTransferableActivity_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SingularAttribute<AdvesoTransferableActivity, ItemActivity> itemActivity;
	public static volatile ListAttribute<AdvesoTransferableActivity, AdvesoOverriddenActivitySetting> advesoOverriddenActivitiesSetting;
	public static volatile SingularAttribute<AdvesoTransferableActivity, Boolean> isDisplayedOnJobOrderDetails;
	public static volatile SingularAttribute<AdvesoTransferableActivity, Integer> id;
	public static volatile SingularAttribute<AdvesoTransferableActivity, Boolean> isTransferredToAdveso;

	public static final String ITEM_ACTIVITY = "itemActivity";
	public static final String ADVESO_OVERRIDDEN_ACTIVITIES_SETTING = "advesoOverriddenActivitiesSetting";
	public static final String IS_DISPLAYED_ON_JOB_ORDER_DETAILS = "isDisplayedOnJobOrderDetails";
	public static final String ID = "id";
	public static final String IS_TRANSFERRED_TO_ADVESO = "isTransferredToAdveso";

}

