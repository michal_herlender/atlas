package org.trescal.cwms.core.external.adveso.entity.advesooverriddenactivitysetting;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.external.adveso.entity.advesotransferableactivity.AdvesoTransferableActivity;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AdvesoOverriddenActivitySetting.class)
public abstract class AdvesoOverriddenActivitySetting_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SingularAttribute<AdvesoOverriddenActivitySetting, Boolean> isDisplayedOnJobOrderDetails;
	public static volatile SingularAttribute<AdvesoOverriddenActivitySetting, Integer> id;
	public static volatile SingularAttribute<AdvesoOverriddenActivitySetting, Subdiv> subdiv;
	public static volatile SingularAttribute<AdvesoOverriddenActivitySetting, Boolean> isTransferredToAdveso;
	public static volatile SingularAttribute<AdvesoOverriddenActivitySetting, AdvesoTransferableActivity> advesoTransferableActivity;

	public static final String IS_DISPLAYED_ON_JOB_ORDER_DETAILS = "isDisplayedOnJobOrderDetails";
	public static final String ID = "id";
	public static final String SUBDIV = "subdiv";
	public static final String IS_TRANSFERRED_TO_ADVESO = "isTransferredToAdveso";
	public static final String ADVESO_TRANSFERABLE_ACTIVITY = "advesoTransferableActivity";

}

