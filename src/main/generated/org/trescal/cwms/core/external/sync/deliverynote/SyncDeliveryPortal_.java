package org.trescal.cwms.core.external.sync.deliverynote;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.deliverynote.entity.jobdelivery.JobDelivery;
import org.trescal.cwms.core.external.portal.Portal;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SyncDeliveryPortal.class)
public abstract class SyncDeliveryPortal_ {

	public static volatile SingularAttribute<SyncDeliveryPortal, JobDelivery> delivery;
	public static volatile SingularAttribute<SyncDeliveryPortal, Date> lastPull;
	public static volatile SingularAttribute<SyncDeliveryPortal, Portal> portal;
	public static volatile SingularAttribute<SyncDeliveryPortal, Integer> syncDeliveryId;

	public static final String DELIVERY = "delivery";
	public static final String LAST_PULL = "lastPull";
	public static final String PORTAL = "portal";
	public static final String SYNC_DELIVERY_ID = "syncDeliveryId";

}

