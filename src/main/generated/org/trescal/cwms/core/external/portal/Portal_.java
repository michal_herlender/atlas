package org.trescal.cwms.core.external.portal;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Portal.class)
public abstract class Portal_ {

	public static volatile SingularAttribute<Portal, Portals> portalName;
	public static volatile SingularAttribute<Portal, Integer> portalId;

	public static final String PORTAL_NAME = "portalName";
	public static final String PORTAL_ID = "portalId";

}

