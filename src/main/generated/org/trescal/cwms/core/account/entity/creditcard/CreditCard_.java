package org.trescal.cwms.core.account.entity.creditcard;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.account.entity.creditcardtype.CreditCardType;
import org.trescal.cwms.core.company.entity.contact.Contact;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CreditCard.class)
public abstract class CreditCard_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<CreditCard, String> expiryDate;
	public static volatile SingularAttribute<CreditCard, String> encryptedCardNo;
	public static volatile SingularAttribute<CreditCard, Contact> con;
	public static volatile SingularAttribute<CreditCard, Integer> ccid;
	public static volatile SingularAttribute<CreditCard, String> cardname;
	public static volatile SingularAttribute<CreditCard, String> postcode;
	public static volatile SingularAttribute<CreditCard, String> houseno;
	public static volatile SingularAttribute<CreditCard, String> issueno;
	public static volatile SingularAttribute<CreditCard, CreditCardType> type;

	public static final String EXPIRY_DATE = "expiryDate";
	public static final String ENCRYPTED_CARD_NO = "encryptedCardNo";
	public static final String CON = "con";
	public static final String CCID = "ccid";
	public static final String CARDNAME = "cardname";
	public static final String POSTCODE = "postcode";
	public static final String HOUSENO = "houseno";
	public static final String ISSUENO = "issueno";
	public static final String TYPE = "type";

}

