package org.trescal.cwms.core.account.entity.creditcardtype;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.account.entity.creditcard.CreditCard;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CreditCardType.class)
public abstract class CreditCardType_ {

	public static volatile SetAttribute<CreditCardType, CreditCard> creditCards;
	public static volatile SingularAttribute<CreditCardType, String> name;
	public static volatile SingularAttribute<CreditCardType, String> description;
	public static volatile SingularAttribute<CreditCardType, Integer> typeid;

	public static final String CREDIT_CARDS = "creditCards";
	public static final String NAME = "name";
	public static final String DESCRIPTION = "description";
	public static final String TYPEID = "typeid";

}

