package org.trescal.cwms.core.account.entity.nominalcode;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.account.NominalCodeType;
import org.trescal.cwms.core.account.entity.Ledgers;
import org.trescal.cwms.core.account.entity.nominalcodeapplication.NominalCodeApplication;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(NominalCode.class)
public abstract class NominalCode_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<NominalCode, CalibrationType> calType;
	public static volatile SingularAttribute<NominalCode, Ledgers> ledger;
	public static volatile SetAttribute<NominalCode, Translation> titleTranslations;
	public static volatile SingularAttribute<NominalCode, String> code;
	public static volatile SingularAttribute<NominalCode, CostType> costType;
	public static volatile SingularAttribute<NominalCode, Integer> id;
	public static volatile SingularAttribute<NominalCode, Department> department;
	public static volatile SingularAttribute<NominalCode, String> title;
	public static volatile SetAttribute<NominalCode, NominalCodeApplication> nominalCodeApplications;
	public static volatile SingularAttribute<NominalCode, NominalCodeType> nominalCodeType;
	public static volatile SingularAttribute<NominalCode, String> avalaraTaxCode;
	public static volatile SetAttribute<NominalCode, PurchaseOrderItem> poItems;

	public static final String CAL_TYPE = "calType";
	public static final String LEDGER = "ledger";
	public static final String TITLE_TRANSLATIONS = "titleTranslations";
	public static final String CODE = "code";
	public static final String COST_TYPE = "costType";
	public static final String ID = "id";
	public static final String DEPARTMENT = "department";
	public static final String TITLE = "title";
	public static final String NOMINAL_CODE_APPLICATIONS = "nominalCodeApplications";
	public static final String NOMINAL_CODE_TYPE = "nominalCodeType";
	public static final String AVALARA_TAX_CODE = "avalaraTaxCode";
	public static final String PO_ITEMS = "poItems";

}

