package org.trescal.cwms.core.account.entity.bankaccount;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BankAccount.class)
public abstract class BankAccount_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SingularAttribute<BankAccount, Boolean> includeOnFactoredInvoices;
	public static volatile SingularAttribute<BankAccount, String> swiftBic;
	public static volatile SingularAttribute<BankAccount, String> iban;
	public static volatile SingularAttribute<BankAccount, String> accountNo;
	public static volatile SingularAttribute<BankAccount, Company> company;
	public static volatile SingularAttribute<BankAccount, String> bankName;
	public static volatile SingularAttribute<BankAccount, SupportedCurrency> currency;
	public static volatile SingularAttribute<BankAccount, Integer> id;
	public static volatile SingularAttribute<BankAccount, Boolean> includeOnNonFactoredInvoices;

	public static final String INCLUDE_ON_FACTORED_INVOICES = "includeOnFactoredInvoices";
	public static final String SWIFT_BIC = "swiftBic";
	public static final String IBAN = "iban";
	public static final String ACCOUNT_NO = "accountNo";
	public static final String COMPANY = "company";
	public static final String BANK_NAME = "bankName";
	public static final String CURRENCY = "currency";
	public static final String ID = "id";
	public static final String INCLUDE_ON_NON_FACTORED_INVOICES = "includeOnNonFactoredInvoices";

}

