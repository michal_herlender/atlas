package org.trescal.cwms.core.account.entity.nominalcodeapplication;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.domain.InstrumentModelDomain;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(NominalCodeApplication.class)
public abstract class NominalCodeApplication_ {

	public static volatile SingularAttribute<NominalCodeApplication, Description> subfamily;
	public static volatile SingularAttribute<NominalCodeApplication, CostType> costType;
	public static volatile SingularAttribute<NominalCodeApplication, InstrumentModelDomain> domain;
	public static volatile SingularAttribute<NominalCodeApplication, NominalCode> nominalCode;
	public static volatile SingularAttribute<NominalCodeApplication, Integer> id;
	public static volatile SingularAttribute<NominalCodeApplication, InstrumentModelFamily> family;

	public static final String SUBFAMILY = "subfamily";
	public static final String COST_TYPE = "costType";
	public static final String DOMAIN = "domain";
	public static final String NOMINAL_CODE = "nominalCode";
	public static final String ID = "id";
	public static final String FAMILY = "family";

}

