package org.trescal.cwms.core.events.change.queue;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.events.change.enums.ChangeEntity;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ChangeQueue.class)
public abstract class ChangeQueue_ {

	public static volatile SingularAttribute<ChangeQueue, Boolean> processed;
	public static volatile SingularAttribute<ChangeQueue, String> payload;
	public static volatile SingularAttribute<ChangeQueue, ChangeEntity> entityType;
	public static volatile SingularAttribute<ChangeQueue, Integer> changeType;
	public static volatile SingularAttribute<ChangeQueue, Integer> id;

	public static final String PROCESSED = "processed";
	public static final String PAYLOAD = "payload";
	public static final String ENTITY_TYPE = "entityType";
	public static final String CHANGE_TYPE = "changeType";
	public static final String ID = "id";

}

