package org.trescal.cwms.core.events.subscriber.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.events.change.enums.ChangeEntity;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(EventSubscription.class)
public abstract class EventSubscription_ {

	public static volatile SingularAttribute<EventSubscription, EventSubscriber> subscriber;
	public static volatile SingularAttribute<EventSubscription, ChangeEntity> entityType;
	public static volatile SingularAttribute<EventSubscription, Integer> id;

	public static final String SUBSCRIBER = "subscriber";
	public static final String ENTITY_TYPE = "entityType";
	public static final String ID = "id";

}

