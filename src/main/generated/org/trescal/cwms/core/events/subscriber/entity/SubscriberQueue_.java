package org.trescal.cwms.core.events.subscriber.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.events.change.queue.ChangeQueue;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SubscriberQueue.class)
public abstract class SubscriberQueue_ {

	public static volatile SingularAttribute<SubscriberQueue, Boolean> processed;
	public static volatile SingularAttribute<SubscriberQueue, EventSubscriber> subscriber;
	public static volatile SingularAttribute<SubscriberQueue, ChangeQueue> queueEvent;
	public static volatile SingularAttribute<SubscriberQueue, Integer> id;

	public static final String PROCESSED = "processed";
	public static final String SUBSCRIBER = "subscriber";
	public static final String QUEUE_EVENT = "queueEvent";
	public static final String ID = "id";

}

