package org.trescal.cwms.core.events.subscriber.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(EventSubscriber.class)
public abstract class EventSubscriber_ {

	public static volatile SingularAttribute<EventSubscriber, String> description;
	public static volatile SingularAttribute<EventSubscriber, Integer> id;
	public static volatile ListAttribute<EventSubscriber, EventSubscription> eventSubscriptions;

	public static final String DESCRIPTION = "description";
	public static final String ID = "id";
	public static final String EVENT_SUBSCRIPTIONS = "eventSubscriptions";

}

