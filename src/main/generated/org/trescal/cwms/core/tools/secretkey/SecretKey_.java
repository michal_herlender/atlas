package org.trescal.cwms.core.tools.secretkey;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SecretKey.class)
public abstract class SecretKey_ {

	public static volatile SingularAttribute<SecretKey, byte[]> keyValue;

	public static final String KEY_VALUE = "keyValue";

}

