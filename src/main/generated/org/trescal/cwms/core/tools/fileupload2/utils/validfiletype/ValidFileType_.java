package org.trescal.cwms.core.tools.fileupload2.utils.validfiletype;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.tools.fileupload2.utils.validfiletype.ValidFileType.FileTypeUsage;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ValidFileType.class)
public abstract class ValidFileType_ {

	public static volatile SingularAttribute<ValidFileType, String> extension;
	public static volatile SingularAttribute<ValidFileType, FileTypeUsage> usage;
	public static volatile SingularAttribute<ValidFileType, Integer> fileTypeId;

	public static final String EXTENSION = "extension";
	public static final String USAGE = "usage";
	public static final String FILE_TYPE_ID = "fileTypeId";

}

