package org.trescal.cwms.core.tools.filebrowser;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RegularExpression.class)
public abstract class RegularExpression_ {

	public static volatile SingularAttribute<RegularExpression, SystemComponent> sc;
	public static volatile SingularAttribute<RegularExpression, String> regEx;
	public static volatile SingularAttribute<RegularExpression, Integer> id;

	public static final String SC = "sc";
	public static final String REG_EX = "regEx";
	public static final String ID = "id";

}

