package org.trescal.cwms.core.recall.entity.recallcompanyconfiguration;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.company.Company;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RecallCompanyConfiguration.class)
public abstract class RecallCompanyConfiguration_ {

	public static volatile SingularAttribute<RecallCompanyConfiguration, RecallTemplateType> templateType;
	public static volatile SingularAttribute<RecallCompanyConfiguration, RecallAttachmentType> attachmentType;
	public static volatile SingularAttribute<RecallCompanyConfiguration, String> customText;
	public static volatile SingularAttribute<RecallCompanyConfiguration, Boolean> pastDue;
	public static volatile SingularAttribute<RecallCompanyConfiguration, Boolean> active;
	public static volatile SingularAttribute<RecallCompanyConfiguration, Company> company;
	public static volatile SingularAttribute<RecallCompanyConfiguration, Integer> futureMonths;
	public static volatile SingularAttribute<RecallCompanyConfiguration, Integer> id;
	public static volatile SingularAttribute<RecallCompanyConfiguration, RecallPeriodStart> periodStart;
	public static volatile SingularAttribute<RecallCompanyConfiguration, RecallSubjectKey> subjectKey;

	public static final String TEMPLATE_TYPE = "templateType";
	public static final String ATTACHMENT_TYPE = "attachmentType";
	public static final String CUSTOM_TEXT = "customText";
	public static final String PAST_DUE = "pastDue";
	public static final String ACTIVE = "active";
	public static final String COMPANY = "company";
	public static final String FUTURE_MONTHS = "futureMonths";
	public static final String ID = "id";
	public static final String PERIOD_START = "periodStart";
	public static final String SUBJECT_KEY = "subjectKey";

}

