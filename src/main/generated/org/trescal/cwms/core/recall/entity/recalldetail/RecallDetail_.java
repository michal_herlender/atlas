package org.trescal.cwms.core.recall.entity.recalldetail;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.recall.entity.recall.Recall;
import org.trescal.cwms.core.recall.entity.recall.RecallError;
import org.trescal.cwms.core.recall.entity.recall.RecallSendStatus;
import org.trescal.cwms.core.recall.entity.recall.RecallSendType;
import org.trescal.cwms.core.recall.entity.recall.RecallType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RecallDetail.class)
public abstract class RecallDetail_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<RecallDetail, RecallType> recallType;
	public static volatile SingularAttribute<RecallDetail, Address> address;
	public static volatile SingularAttribute<RecallDetail, RecallSendStatus> recallSendStatus;
	public static volatile SingularAttribute<RecallDetail, String> pathToFile;
	public static volatile SingularAttribute<RecallDetail, Boolean> ignoredOnSetting;
	public static volatile SingularAttribute<RecallDetail, Boolean> error;
	public static volatile SingularAttribute<RecallDetail, RecallSendType> recallSendType;
	public static volatile SingularAttribute<RecallDetail, Boolean> ignoredAllOnJob;
	public static volatile SingularAttribute<RecallDetail, Integer> numberOfItems;
	public static volatile SingularAttribute<RecallDetail, Contact> contact;
	public static volatile SingularAttribute<RecallDetail, Recall> recall;
	public static volatile SingularAttribute<RecallDetail, Integer> id;
	public static volatile SingularAttribute<RecallDetail, RecallError> recallError;

	public static final String RECALL_TYPE = "recallType";
	public static final String ADDRESS = "address";
	public static final String RECALL_SEND_STATUS = "recallSendStatus";
	public static final String PATH_TO_FILE = "pathToFile";
	public static final String IGNORED_ON_SETTING = "ignoredOnSetting";
	public static final String ERROR = "error";
	public static final String RECALL_SEND_TYPE = "recallSendType";
	public static final String IGNORED_ALL_ON_JOB = "ignoredAllOnJob";
	public static final String NUMBER_OF_ITEMS = "numberOfItems";
	public static final String CONTACT = "contact";
	public static final String RECALL = "recall";
	public static final String ID = "id";
	public static final String RECALL_ERROR = "recallError";

}

