package org.trescal.cwms.core.recall.entity.recallitem;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.recall.entity.recall.Recall;
import org.trescal.cwms.core.recall.entity.recallresponsenote.RecallResponseNote;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RecallItem.class)
public abstract class RecallItem_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<RecallItem, Boolean> excludeFromNotification;
	public static volatile SingularAttribute<RecallItem, Contact> contact;
	public static volatile SingularAttribute<RecallItem, Recall> recall;
	public static volatile SingularAttribute<RecallItem, Instrument> instrument;
	public static volatile SingularAttribute<RecallItem, Integer> id;
	public static volatile SetAttribute<RecallItem, RecallResponseNote> recallResponseNotes;

	public static final String EXCLUDE_FROM_NOTIFICATION = "excludeFromNotification";
	public static final String CONTACT = "contact";
	public static final String RECALL = "recall";
	public static final String INSTRUMENT = "instrument";
	public static final String ID = "id";
	public static final String RECALL_RESPONSE_NOTES = "recallResponseNotes";

}

