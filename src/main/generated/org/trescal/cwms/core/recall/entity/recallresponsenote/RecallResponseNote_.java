package org.trescal.cwms.core.recall.entity.recallresponsenote;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.recall.entity.recallitem.RecallItem;
import org.trescal.cwms.core.recall.entity.recallresponsestatus.RecallResponseStatus;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RecallResponseNote.class)
public abstract class RecallResponseNote_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<RecallResponseNote, Date> setOn;
	public static volatile SingularAttribute<RecallResponseNote, RecallResponseStatus> recallResponseStatus;
	public static volatile SingularAttribute<RecallResponseNote, RecallItem> recallitem;
	public static volatile SingularAttribute<RecallResponseNote, String> comment;
	public static volatile SingularAttribute<RecallResponseNote, Integer> noteid;
	public static volatile SingularAttribute<RecallResponseNote, Contact> setBy;

	public static final String SET_ON = "setOn";
	public static final String RECALL_RESPONSE_STATUS = "recallResponseStatus";
	public static final String RECALLITEM = "recallitem";
	public static final String COMMENT = "comment";
	public static final String NOTEID = "noteid";
	public static final String SET_BY = "setBy";

}

