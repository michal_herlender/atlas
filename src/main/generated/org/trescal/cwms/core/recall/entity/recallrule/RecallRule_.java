package org.trescal.cwms.core.recall.entity.recallrule;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.recall.enums.RecallRequirementType;
import org.trescal.cwms.core.recall.enums.RecallRuleType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.enums.IntervalUnit;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RecallRule.class)
public abstract class RecallRule_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<RecallRule, ServiceType> serviceType;
	public static volatile SingularAttribute<RecallRule, Company> comp;
	public static volatile SingularAttribute<RecallRule, RecallRuleType> recallRuleType;
	public static volatile SingularAttribute<RecallRule, Subdiv> sub;
	public static volatile SingularAttribute<RecallRule, Contact> con;
	public static volatile SingularAttribute<RecallRule, Boolean> calculateFromCert;
	public static volatile SingularAttribute<RecallRule, LocalDate> setOn;
	public static volatile SingularAttribute<RecallRule, LocalDate> deactivatedOn;
	public static volatile SingularAttribute<RecallRule, IntervalUnit> intervalUnit;
	public static volatile SingularAttribute<RecallRule, Boolean> active;
	public static volatile SingularAttribute<RecallRule, Instrument> instrument;
	public static volatile SingularAttribute<RecallRule, Contact> deactivatedBy;
	public static volatile SingularAttribute<RecallRule, LocalDate> calibrateOn;
	public static volatile SingularAttribute<RecallRule, RecallRequirementType> recallRequirementType;
	public static volatile SingularAttribute<RecallRule, Boolean> updateInstrument;
	public static volatile SingularAttribute<RecallRule, Integer> interval;
	public static volatile SingularAttribute<RecallRule, InstrumentModel> model;
	public static volatile SingularAttribute<RecallRule, Integer> id;
	public static volatile SingularAttribute<RecallRule, Contact> setBy;

	public static final String SERVICE_TYPE = "serviceType";
	public static final String COMP = "comp";
	public static final String RECALL_RULE_TYPE = "recallRuleType";
	public static final String SUB = "sub";
	public static final String CON = "con";
	public static final String CALCULATE_FROM_CERT = "calculateFromCert";
	public static final String SET_ON = "setOn";
	public static final String DEACTIVATED_ON = "deactivatedOn";
	public static final String INTERVAL_UNIT = "intervalUnit";
	public static final String ACTIVE = "active";
	public static final String INSTRUMENT = "instrument";
	public static final String DEACTIVATED_BY = "deactivatedBy";
	public static final String CALIBRATE_ON = "calibrateOn";
	public static final String RECALL_REQUIREMENT_TYPE = "recallRequirementType";
	public static final String UPDATE_INSTRUMENT = "updateInstrument";
	public static final String INTERVAL = "interval";
	public static final String MODEL = "model";
	public static final String ID = "id";
	public static final String SET_BY = "setBy";

}

