package org.trescal.cwms.core.recall.entity.recallresponsestatus;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.recall.entity.recallresponsenote.RecallResponseNote;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RecallResponseStatus.class)
public abstract class RecallResponseStatus_ {

	public static volatile SingularAttribute<RecallResponseStatus, Integer> statusid;
	public static volatile SetAttribute<RecallResponseStatus, Translation> descriptiontranslation;
	public static volatile SingularAttribute<RecallResponseStatus, String> description;
	public static volatile SetAttribute<RecallResponseStatus, RecallResponseNote> recallResponseNotes;

	public static final String STATUSID = "statusid";
	public static final String DESCRIPTIONTRANSLATION = "descriptiontranslation";
	public static final String DESCRIPTION = "description";
	public static final String RECALL_RESPONSE_NOTES = "recallResponseNotes";

}

