package org.trescal.cwms.core.recall.entity.recall;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.recall.entity.recalldetail.RecallDetail;
import org.trescal.cwms.core.recall.entity.recallitem.RecallItem;
import org.trescal.cwms.core.recall.entity.recallnote.RecallNote;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Recall.class)
public abstract class Recall_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<Recall, LocalDate> date;
	public static volatile SetAttribute<Recall, RecallNote> notes;
	public static volatile SingularAttribute<Recall, String> recallNo;
	public static volatile SingularAttribute<Recall, LocalDate> dateTo;
	public static volatile SingularAttribute<Recall, Company> company;
	public static volatile SingularAttribute<Recall, Integer> id;
	public static volatile SingularAttribute<Recall, LocalDate> dateFrom;
	public static volatile SetAttribute<Recall, RecallItem> items;
	public static volatile SetAttribute<Recall, RecallDetail> rdetails;

	public static final String DATE = "date";
	public static final String NOTES = "notes";
	public static final String RECALL_NO = "recallNo";
	public static final String DATE_TO = "dateTo";
	public static final String COMPANY = "company";
	public static final String ID = "id";
	public static final String DATE_FROM = "dateFrom";
	public static final String ITEMS = "items";
	public static final String RDETAILS = "rdetails";

}

