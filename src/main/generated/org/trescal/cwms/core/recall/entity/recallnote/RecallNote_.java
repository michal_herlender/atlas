package org.trescal.cwms.core.recall.entity.recallnote;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.recall.entity.recall.Recall;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RecallNote.class)
public abstract class RecallNote_ extends org.trescal.cwms.core.system.entity.note.Note_ {

	public static volatile SingularAttribute<RecallNote, Recall> recall;

	public static final String RECALL = "recall";

}

