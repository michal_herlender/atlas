package org.trescal.cwms.core.system.entity.translation;

import java.util.Locale;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Translation.class)
public abstract class Translation_ {

	public static volatile SingularAttribute<Translation, String> translation;
	public static volatile SingularAttribute<Translation, Locale> locale;

	public static final String TRANSLATION = "translation";
	public static final String LOCALE = "locale";

}

