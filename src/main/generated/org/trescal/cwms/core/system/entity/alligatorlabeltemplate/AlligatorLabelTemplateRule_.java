package org.trescal.cwms.core.system.entity.alligatorlabeltemplate;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.company.Company;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AlligatorLabelTemplateRule.class)
public abstract class AlligatorLabelTemplateRule_ {

	public static volatile SingularAttribute<AlligatorLabelTemplateRule, AlligatorLabelTemplate> template;
	public static volatile SingularAttribute<AlligatorLabelTemplateRule, Company> businessCompany;
	public static volatile SingularAttribute<AlligatorLabelTemplateRule, Integer> id;

	public static final String TEMPLATE = "template";
	public static final String BUSINESS_COMPANY = "businessCompany";
	public static final String ID = "id";

}

