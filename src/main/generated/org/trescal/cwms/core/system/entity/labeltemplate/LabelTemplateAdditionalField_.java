package org.trescal.cwms.core.system.entity.labeltemplate;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(LabelTemplateAdditionalField.class)
public abstract class LabelTemplateAdditionalField_ {

	public static volatile SingularAttribute<LabelTemplateAdditionalField, String> fieldName;
	public static volatile SetAttribute<LabelTemplateAdditionalField, Translation> translations;
	public static volatile SingularAttribute<LabelTemplateAdditionalField, String> name;
	public static volatile SingularAttribute<LabelTemplateAdditionalField, Integer> id;
	public static volatile SingularAttribute<LabelTemplateAdditionalField, LabelTemplate> labelTemplate;

	public static final String FIELD_NAME = "fieldName";
	public static final String TRANSLATIONS = "translations";
	public static final String NAME = "name";
	public static final String ID = "id";
	public static final String LABEL_TEMPLATE = "labelTemplate";

}

