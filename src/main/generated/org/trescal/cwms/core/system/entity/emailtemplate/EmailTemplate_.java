package org.trescal.cwms.core.system.entity.emailtemplate;

import java.util.Locale;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(EmailTemplate.class)
public abstract class EmailTemplate_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<EmailTemplate, String> template;
	public static volatile SingularAttribute<EmailTemplate, Component> component;
	public static volatile SingularAttribute<EmailTemplate, String> subject;
	public static volatile SingularAttribute<EmailTemplate, Integer> id;
	public static volatile SingularAttribute<EmailTemplate, Locale> locale;
	public static volatile SingularAttribute<EmailTemplate, Subdiv> subdiv;

	public static final String TEMPLATE = "template";
	public static final String COMPONENT = "component";
	public static final String SUBJECT = "subject";
	public static final String ID = "id";
	public static final String LOCALE = "locale";
	public static final String SUBDIV = "subdiv";

}

