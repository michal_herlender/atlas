package org.trescal.cwms.core.system.entity.alligatorlabeltemplate;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.system.entity.alligatorlabeltemplate.enums.AlligatorParameterStyle;
import org.trescal.cwms.core.system.entity.alligatorlabeltemplate.enums.AlligatorTemplateType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AlligatorLabelTemplate.class)
public abstract class AlligatorLabelTemplate_ {

	public static volatile SingularAttribute<AlligatorLabelTemplate, AlligatorTemplateType> templateType;
	public static volatile SingularAttribute<AlligatorLabelTemplate, String> templateXml;
	public static volatile SingularAttribute<AlligatorLabelTemplate, String> description;
	public static volatile ListAttribute<AlligatorLabelTemplate, AlligatorLabelTemplateRule> rules;
	public static volatile SingularAttribute<AlligatorLabelTemplate, Integer> id;
	public static volatile SingularAttribute<AlligatorLabelTemplate, AlligatorParameterStyle> parameterStyle;
	public static volatile ListAttribute<AlligatorLabelTemplate, AlligatorLabelParameter> parameters;

	public static final String TEMPLATE_TYPE = "templateType";
	public static final String TEMPLATE_XML = "templateXml";
	public static final String DESCRIPTION = "description";
	public static final String RULES = "rules";
	public static final String ID = "id";
	public static final String PARAMETER_STYLE = "parameterStyle";
	public static final String PARAMETERS = "parameters";

}

