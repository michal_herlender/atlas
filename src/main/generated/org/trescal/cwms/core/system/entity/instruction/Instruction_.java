package org.trescal.cwms.core.system.entity.instruction;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Instruction.class)
public abstract class Instruction_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SingularAttribute<Instruction, InstructionType> instructiontype;
	public static volatile SingularAttribute<Instruction, String> instruction;
	public static volatile SingularAttribute<Instruction, Boolean> includeOnDelNote;
	public static volatile SingularAttribute<Instruction, Boolean> includeOnSupplierDelNote;
	public static volatile SingularAttribute<Instruction, Integer> instructionid;

	public static final String INSTRUCTIONTYPE = "instructiontype";
	public static final String INSTRUCTION = "instruction";
	public static final String INCLUDE_ON_DEL_NOTE = "includeOnDelNote";
	public static final String INCLUDE_ON_SUPPLIER_DEL_NOTE = "includeOnSupplierDelNote";
	public static final String INSTRUCTIONID = "instructionid";

}

