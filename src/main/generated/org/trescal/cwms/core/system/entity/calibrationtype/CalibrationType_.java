package org.trescal.cwms.core.system.entity.calibrationtype;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.job.entity.jobtypecaltype.JobTypeCalType;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.CapabilityAuthorization;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotecaltypedefault.QuoteCaltypeDefault;
import org.trescal.cwms.core.recall.enums.RecallRequirementType;
import org.trescal.cwms.core.system.entity.calibrationaccreditationlevel.CalibrationAccreditationLevel;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationcaltypedefault.TPQuotationCaltypeDefault;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CalibrationType.class)
public abstract class CalibrationType_ {

	public static volatile SingularAttribute<CalibrationType, ServiceType> serviceType;
	public static volatile SingularAttribute<CalibrationType, Boolean> accreditationSpecific;
	public static volatile SingularAttribute<CalibrationType, Boolean> calibrationWithJudgement;
	public static volatile SetAttribute<CalibrationType, JobItem> jobItems;
	public static volatile SetAttribute<CalibrationType, CapabilityAuthorization> authorizations;
	public static volatile SingularAttribute<CalibrationType, Integer> orderBy;
	public static volatile SingularAttribute<CalibrationType, Boolean> active;
	public static volatile SetAttribute<CalibrationType, TPQuotationCaltypeDefault> tpQuoteCaltypeDefaults;
	public static volatile SetAttribute<CalibrationType, QuoteCaltypeDefault> quoteCaltypeDefaults;
	public static volatile SingularAttribute<CalibrationType, RecallRequirementType> recallRequirementType;
	public static volatile SetAttribute<CalibrationType, TPQuotationItem> tpQuoteItems;
	public static volatile SingularAttribute<CalibrationType, Integer> calTypeId;
	public static volatile SetAttribute<CalibrationType, Quotationitem> quoteItems;
	public static volatile SetAttribute<CalibrationType, CalibrationAccreditationLevel> accreditationLevels;
	public static volatile SetAttribute<CalibrationType, JobTypeCalType> jobTypes;

	public static final String SERVICE_TYPE = "serviceType";
	public static final String ACCREDITATION_SPECIFIC = "accreditationSpecific";
	public static final String CALIBRATION_WITH_JUDGEMENT = "calibrationWithJudgement";
	public static final String JOB_ITEMS = "jobItems";
	public static final String AUTHORIZATIONS = "authorizations";
	public static final String ORDER_BY = "orderBy";
	public static final String ACTIVE = "active";
	public static final String TP_QUOTE_CALTYPE_DEFAULTS = "tpQuoteCaltypeDefaults";
	public static final String QUOTE_CALTYPE_DEFAULTS = "quoteCaltypeDefaults";
	public static final String RECALL_REQUIREMENT_TYPE = "recallRequirementType";
	public static final String TP_QUOTE_ITEMS = "tpQuoteItems";
	public static final String CAL_TYPE_ID = "calTypeId";
	public static final String QUOTE_ITEMS = "quoteItems";
	public static final String ACCREDITATION_LEVELS = "accreditationLevels";
	public static final String JOB_TYPES = "jobTypes";

}

