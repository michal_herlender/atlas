package org.trescal.cwms.core.system.entity.calibrationaccreditationlevel;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.AccreditationLevel;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CalibrationAccreditationLevel.class)
public abstract class CalibrationAccreditationLevel_ {

	public static volatile SingularAttribute<CalibrationAccreditationLevel, AccreditationLevel> level;
	public static volatile SingularAttribute<CalibrationAccreditationLevel, Integer> id;
	public static volatile SingularAttribute<CalibrationAccreditationLevel, CalibrationType> caltype;

	public static final String LEVEL = "level";
	public static final String ID = "id";
	public static final String CALTYPE = "caltype";

}

