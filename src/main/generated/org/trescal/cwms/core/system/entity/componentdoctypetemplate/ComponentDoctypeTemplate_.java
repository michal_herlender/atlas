package org.trescal.cwms.core.system.entity.componentdoctypetemplate;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.system.entity.componentdoctype.ComponentDoctype;
import org.trescal.cwms.core.system.entity.template.Template;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ComponentDoctypeTemplate.class)
public abstract class ComponentDoctypeTemplate_ {

	public static volatile SingularAttribute<ComponentDoctypeTemplate, Template> template;
	public static volatile SingularAttribute<ComponentDoctypeTemplate, Boolean> defaultTemplate;
	public static volatile SingularAttribute<ComponentDoctypeTemplate, ComponentDoctype> componentDoctype;
	public static volatile SingularAttribute<ComponentDoctypeTemplate, Integer> id;

	public static final String TEMPLATE = "template";
	public static final String DEFAULT_TEMPLATE = "defaultTemplate";
	public static final String COMPONENT_DOCTYPE = "componentDoctype";
	public static final String ID = "id";

}

