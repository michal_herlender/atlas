package org.trescal.cwms.core.system.entity.alligatorsettings;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AlligatorSettingsContact.class)
public abstract class AlligatorSettingsContact_ extends org.trescal.cwms.core.system.entity.alligatorsettings.AlligatorSettingsUsage_ {

	public static volatile SingularAttribute<AlligatorSettingsContact, Contact> contact;

	public static final String CONTACT = "contact";

}

