package org.trescal.cwms.core.system.entity.labelprinter;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.login.entity.userpreferences.UserPreferences;
import org.trescal.cwms.core.system.entity.alligatorlabelmedia.AlligatorLabelMedia;
import org.trescal.cwms.core.system.entity.labelprinter.enums.AlligatorPrinterLanguage;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(LabelPrinter.class)
public abstract class LabelPrinter_ extends org.trescal.cwms.core.system.entity.printer.AbstractPrinter_ {

	public static volatile SetAttribute<LabelPrinter, Address> usedByAddresses;
	public static volatile SingularAttribute<LabelPrinter, String> ipv4;
	public static volatile SetAttribute<LabelPrinter, UserPreferences> usedByUsers;
	public static volatile SingularAttribute<LabelPrinter, Integer> darknessLevel;
	public static volatile SingularAttribute<LabelPrinter, AlligatorPrinterLanguage> language;
	public static volatile SingularAttribute<LabelPrinter, AlligatorLabelMedia> media;
	public static volatile SingularAttribute<LabelPrinter, Integer> resolution;

	public static final String USED_BY_ADDRESSES = "usedByAddresses";
	public static final String IPV4 = "ipv4";
	public static final String USED_BY_USERS = "usedByUsers";
	public static final String DARKNESS_LEVEL = "darknessLevel";
	public static final String LANGUAGE = "language";
	public static final String MEDIA = "media";
	public static final String RESOLUTION = "resolution";

}

