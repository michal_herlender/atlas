package org.trescal.cwms.core.system.entity.rate;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Rate.class)
public abstract class Rate_ {

	public static volatile SingularAttribute<Rate, SupportedCurrency> targetCurrency;
	public static volatile SingularAttribute<Rate, BigDecimal> rate;
	public static volatile SingularAttribute<Rate, Date> setOn;
	public static volatile SingularAttribute<Rate, String> name;
	public static volatile SingularAttribute<Rate, String> description;
	public static volatile SingularAttribute<Rate, Boolean> active;
	public static volatile SingularAttribute<Rate, Integer> id;
	public static volatile SingularAttribute<Rate, Contact> setBy;
	public static volatile SingularAttribute<Rate, SupportedCurrency> baseCurrency;

	public static final String TARGET_CURRENCY = "targetCurrency";
	public static final String RATE = "rate";
	public static final String SET_ON = "setOn";
	public static final String NAME = "name";
	public static final String DESCRIPTION = "description";
	public static final String ACTIVE = "active";
	public static final String ID = "id";
	public static final String SET_BY = "setBy";
	public static final String BASE_CURRENCY = "baseCurrency";

}

