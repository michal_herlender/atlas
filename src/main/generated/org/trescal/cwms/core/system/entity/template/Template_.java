package org.trescal.cwms.core.system.entity.template;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.system.entity.componentdoctypetemplate.ComponentDoctypeTemplate;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Template.class)
public abstract class Template_ {

	public static volatile SingularAttribute<Template, String> name;
	public static volatile SetAttribute<Template, ComponentDoctypeTemplate> componentDoctypeTemplates;
	public static volatile SingularAttribute<Template, String> description;
	public static volatile SingularAttribute<Template, Integer> templateId;
	public static volatile SingularAttribute<Template, String> templatePath;

	public static final String NAME = "name";
	public static final String COMPONENT_DOCTYPE_TEMPLATES = "componentDoctypeTemplates";
	public static final String DESCRIPTION = "description";
	public static final String TEMPLATE_ID = "templateId";
	public static final String TEMPLATE_PATH = "templatePath";

}

