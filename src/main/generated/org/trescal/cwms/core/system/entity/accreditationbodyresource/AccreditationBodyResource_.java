package org.trescal.cwms.core.system.entity.accreditationbodyresource;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.system.entity.accreditationbody.AccreditationBody;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AccreditationBodyResource.class)
public abstract class AccreditationBodyResource_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SingularAttribute<AccreditationBodyResource, AccreditationBody> accreditationBody;
	public static volatile SingularAttribute<AccreditationBodyResource, String> resourcePath;
	public static volatile SingularAttribute<AccreditationBodyResource, Integer> id;
	public static volatile SingularAttribute<AccreditationBodyResource, AccreditationBodyResourceType> type;

	public static final String ACCREDITATION_BODY = "accreditationBody";
	public static final String RESOURCE_PATH = "resourcePath";
	public static final String ID = "id";
	public static final String TYPE = "type";

}

