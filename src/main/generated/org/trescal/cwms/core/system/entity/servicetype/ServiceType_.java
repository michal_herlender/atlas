package org.trescal.cwms.core.system.entity.servicetype;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.jobs.job.entity.jobtypeservicetype.JobTypeServiceType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ServiceType.class)
public abstract class ServiceType_ {

	public static volatile SingularAttribute<ServiceType, String> displayColour;
	public static volatile SingularAttribute<ServiceType, Boolean> repair;
	public static volatile SetAttribute<ServiceType, Translation> shortnameTranslation;
	public static volatile SingularAttribute<ServiceType, DomainType> domainType;
	public static volatile SingularAttribute<ServiceType, CalibrationType> calibrationType;
	public static volatile SingularAttribute<ServiceType, String> displayColourFastTrack;
	public static volatile SingularAttribute<ServiceType, Boolean> canBePerformedByClient;
	public static volatile SingularAttribute<ServiceType, Integer> serviceTypeId;
	public static volatile SingularAttribute<ServiceType, String> shortName;
	public static volatile SetAttribute<ServiceType, Translation> longnameTranslation;
	public static volatile SingularAttribute<ServiceType, String> longName;
	public static volatile SingularAttribute<ServiceType, Integer> order;
	public static volatile SetAttribute<ServiceType, JobTypeServiceType> jobTypes;

	public static final String DISPLAY_COLOUR = "displayColour";
	public static final String REPAIR = "repair";
	public static final String SHORTNAME_TRANSLATION = "shortnameTranslation";
	public static final String DOMAIN_TYPE = "domainType";
	public static final String CALIBRATION_TYPE = "calibrationType";
	public static final String DISPLAY_COLOUR_FAST_TRACK = "displayColourFastTrack";
	public static final String CAN_BE_PERFORMED_BY_CLIENT = "canBePerformedByClient";
	public static final String SERVICE_TYPE_ID = "serviceTypeId";
	public static final String SHORT_NAME = "shortName";
	public static final String LONGNAME_TRANSLATION = "longnameTranslation";
	public static final String LONG_NAME = "longName";
	public static final String ORDER = "order";
	public static final String JOB_TYPES = "jobTypes";

}

