package org.trescal.cwms.core.system.entity.userinstructiontype;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.login.entity.userpreferences.UserPreferences;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserInstructionType.class)
public abstract class UserInstructionType_ {

	public static volatile SingularAttribute<UserInstructionType, InstructionType> instructionType;
	public static volatile SingularAttribute<UserInstructionType, UserPreferences> userPreference;
	public static volatile SingularAttribute<UserInstructionType, Integer> id;

	public static final String INSTRUCTION_TYPE = "instructionType";
	public static final String USER_PREFERENCE = "userPreference";
	public static final String ID = "id";

}

