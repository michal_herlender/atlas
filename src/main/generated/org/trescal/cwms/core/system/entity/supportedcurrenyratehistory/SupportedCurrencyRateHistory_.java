package org.trescal.cwms.core.system.entity.supportedcurrenyratehistory;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SupportedCurrencyRateHistory.class)
public abstract class SupportedCurrencyRateHistory_ {

	public static volatile SingularAttribute<SupportedCurrencyRateHistory, BigDecimal> rate;
	public static volatile SingularAttribute<SupportedCurrencyRateHistory, Date> dateTo;
	public static volatile SingularAttribute<SupportedCurrencyRateHistory, SupportedCurrency> currency;
	public static volatile SingularAttribute<SupportedCurrencyRateHistory, Integer> id;
	public static volatile SingularAttribute<SupportedCurrencyRateHistory, Date> dateFrom;
	public static volatile SingularAttribute<SupportedCurrencyRateHistory, Contact> setBy;

	public static final String RATE = "rate";
	public static final String DATE_TO = "dateTo";
	public static final String CURRENCY = "currency";
	public static final String ID = "id";
	public static final String DATE_FROM = "dateFrom";
	public static final String SET_BY = "setBy";

}

