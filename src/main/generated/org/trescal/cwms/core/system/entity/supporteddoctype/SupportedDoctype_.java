package org.trescal.cwms.core.system.entity.supporteddoctype;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.system.entity.componentdoctype.ComponentDoctype;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SupportedDoctype.class)
public abstract class SupportedDoctype_ {

	public static volatile SingularAttribute<SupportedDoctype, String> extension;
	public static volatile SingularAttribute<SupportedDoctype, String> imagePath;
	public static volatile SingularAttribute<SupportedDoctype, String> name;
	public static volatile SingularAttribute<SupportedDoctype, Integer> id;
	public static volatile SetAttribute<SupportedDoctype, ComponentDoctype> componentDoctypes;

	public static final String EXTENSION = "extension";
	public static final String IMAGE_PATH = "imagePath";
	public static final String NAME = "name";
	public static final String ID = "id";
	public static final String COMPONENT_DOCTYPES = "componentDoctypes";

}

