package org.trescal.cwms.core.system.entity.accreditationbody;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.country.Country;
import org.trescal.cwms.core.system.entity.accreditationbodyresource.AccreditationBodyResource;
import org.trescal.cwms.core.system.entity.accreditedlab.AccreditedLab;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AccreditationBody.class)
public abstract class AccreditationBody_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SingularAttribute<AccreditationBody, Country> country;
	public static volatile SetAttribute<AccreditationBody, AccreditedLab> accreditedLabs;
	public static volatile SetAttribute<AccreditationBody, AccreditationBodyResource> accreditationBodyResources;
	public static volatile SingularAttribute<AccreditationBody, Integer> id;
	public static volatile SingularAttribute<AccreditationBody, String> shortName;

	public static final String COUNTRY = "country";
	public static final String ACCREDITED_LABS = "accreditedLabs";
	public static final String ACCREDITATION_BODY_RESOURCES = "accreditationBodyResources";
	public static final String ID = "id";
	public static final String SHORT_NAME = "shortName";

}

