package org.trescal.cwms.core.system.entity.systemdefaultgroup;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.system.entity.systemdefault.SystemDefault;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SystemDefaultGroup.class)
public abstract class SystemDefaultGroup_ {

	public static volatile SetAttribute<SystemDefaultGroup, Translation> nameTranslation;
	public static volatile SetAttribute<SystemDefaultGroup, SystemDefault> defaults;
	public static volatile SingularAttribute<SystemDefaultGroup, String> name;
	public static volatile SetAttribute<SystemDefaultGroup, Translation> descriptionTranslation;
	public static volatile SingularAttribute<SystemDefaultGroup, String> description;
	public static volatile SingularAttribute<SystemDefaultGroup, Integer> id;

	public static final String NAME_TRANSLATION = "nameTranslation";
	public static final String DEFAULTS = "defaults";
	public static final String NAME = "name";
	public static final String DESCRIPTION_TRANSLATION = "descriptionTranslation";
	public static final String DESCRIPTION = "description";
	public static final String ID = "id";

}

