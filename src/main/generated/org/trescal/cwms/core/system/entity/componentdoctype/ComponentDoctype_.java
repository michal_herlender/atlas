package org.trescal.cwms.core.system.entity.componentdoctype;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.system.entity.componentdoctypetemplate.ComponentDoctypeTemplate;
import org.trescal.cwms.core.system.entity.supporteddoctype.SupportedDoctype;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ComponentDoctype.class)
public abstract class ComponentDoctype_ {

	public static volatile SingularAttribute<ComponentDoctype, SupportedDoctype> doctype;
	public static volatile SingularAttribute<ComponentDoctype, SystemComponent> component;
	public static volatile SetAttribute<ComponentDoctype, ComponentDoctypeTemplate> templates;
	public static volatile SingularAttribute<ComponentDoctype, Integer> id;

	public static final String DOCTYPE = "doctype";
	public static final String COMPONENT = "component";
	public static final String TEMPLATES = "templates";
	public static final String ID = "id";

}

