package org.trescal.cwms.core.system.entity.baseaction;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BaseAction.class)
public abstract class BaseAction_ {

	public static volatile SingularAttribute<BaseAction, Date> date;
	public static volatile SingularAttribute<BaseAction, Contact> contact;
	public static volatile SingularAttribute<BaseAction, Integer> id;

	public static final String DATE = "date";
	public static final String CONTACT = "contact";
	public static final String ID = "id";

}

