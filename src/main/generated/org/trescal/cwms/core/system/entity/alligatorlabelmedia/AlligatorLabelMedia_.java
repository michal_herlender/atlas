package org.trescal.cwms.core.system.entity.alligatorlabelmedia;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AlligatorLabelMedia.class)
public abstract class AlligatorLabelMedia_ {

	public static volatile SingularAttribute<AlligatorLabelMedia, BigDecimal> paddingBottom;
	public static volatile SingularAttribute<AlligatorLabelMedia, BigDecimal> rotationAngle;
	public static volatile SingularAttribute<AlligatorLabelMedia, BigDecimal> gap;
	public static volatile SingularAttribute<AlligatorLabelMedia, BigDecimal> paddingRight;
	public static volatile SingularAttribute<AlligatorLabelMedia, String> name;
	public static volatile SingularAttribute<AlligatorLabelMedia, BigDecimal> width;
	public static volatile SingularAttribute<AlligatorLabelMedia, Integer> id;
	public static volatile SingularAttribute<AlligatorLabelMedia, BigDecimal> paddingTop;
	public static volatile SingularAttribute<AlligatorLabelMedia, BigDecimal> paddingLeft;
	public static volatile SingularAttribute<AlligatorLabelMedia, BigDecimal> height;

	public static final String PADDING_BOTTOM = "paddingBottom";
	public static final String ROTATION_ANGLE = "rotationAngle";
	public static final String GAP = "gap";
	public static final String PADDING_RIGHT = "paddingRight";
	public static final String NAME = "name";
	public static final String WIDTH = "width";
	public static final String ID = "id";
	public static final String PADDING_TOP = "paddingTop";
	public static final String PADDING_LEFT = "paddingLeft";
	public static final String HEIGHT = "height";

}

