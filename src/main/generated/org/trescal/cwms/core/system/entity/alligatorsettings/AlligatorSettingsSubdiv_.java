package org.trescal.cwms.core.system.entity.alligatorsettings;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AlligatorSettingsSubdiv.class)
public abstract class AlligatorSettingsSubdiv_ extends org.trescal.cwms.core.system.entity.alligatorsettings.AlligatorSettingsUsage_ {

	public static volatile SingularAttribute<AlligatorSettingsSubdiv, Subdiv> businessSubdiv;

	public static final String BUSINESS_SUBDIV = "businessSubdiv";

}

