package org.trescal.cwms.core.system.entity.supportedcurrency;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.entity.rate.Rate;
import org.trescal.cwms.core.system.entity.supportedcurrenyratehistory.SupportedCurrencyRateHistory;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SupportedCurrency.class)
public abstract class SupportedCurrency_ {

	public static volatile SingularAttribute<SupportedCurrency, String> accountsCode;
	public static volatile SingularAttribute<SupportedCurrency, Date> rateLastSet;
	public static volatile SingularAttribute<SupportedCurrency, CurrencySymbolPosition> symbolPosition;
	public static volatile SingularAttribute<SupportedCurrency, String> currencyERSymbol;
	public static volatile SingularAttribute<SupportedCurrency, Contact> rateLastSetBy;
	public static volatile SetAttribute<SupportedCurrency, SupportedCurrencyRateHistory> rateHistory;
	public static volatile SingularAttribute<SupportedCurrency, String> currencySymbol;
	public static volatile SingularAttribute<SupportedCurrency, Integer> orderBy;
	public static volatile SingularAttribute<SupportedCurrency, String> minorPluralName;
	public static volatile SetAttribute<SupportedCurrency, Rate> exRates;
	public static volatile SingularAttribute<SupportedCurrency, Integer> minorExponent;
	public static volatile SingularAttribute<SupportedCurrency, BigDecimal> defaultRate;
	public static volatile SingularAttribute<SupportedCurrency, String> currencyName;
	public static volatile SingularAttribute<SupportedCurrency, String> majorSingularName;
	public static volatile SetAttribute<SupportedCurrency, Rate> baseRates;
	public static volatile SingularAttribute<SupportedCurrency, String> majorPluralName;
	public static volatile SingularAttribute<SupportedCurrency, Integer> currencyId;
	public static volatile SingularAttribute<SupportedCurrency, Boolean> rateSet;
	public static volatile SingularAttribute<SupportedCurrency, String> minorSingularName;
	public static volatile SingularAttribute<SupportedCurrency, String> currencyCode;
	public static volatile SingularAttribute<SupportedCurrency, Boolean> minorUnit;

	public static final String ACCOUNTS_CODE = "accountsCode";
	public static final String RATE_LAST_SET = "rateLastSet";
	public static final String SYMBOL_POSITION = "symbolPosition";
	public static final String CURRENCY_ER_SYMBOL = "currencyERSymbol";
	public static final String RATE_LAST_SET_BY = "rateLastSetBy";
	public static final String RATE_HISTORY = "rateHistory";
	public static final String CURRENCY_SYMBOL = "currencySymbol";
	public static final String ORDER_BY = "orderBy";
	public static final String MINOR_PLURAL_NAME = "minorPluralName";
	public static final String EX_RATES = "exRates";
	public static final String MINOR_EXPONENT = "minorExponent";
	public static final String DEFAULT_RATE = "defaultRate";
	public static final String CURRENCY_NAME = "currencyName";
	public static final String MAJOR_SINGULAR_NAME = "majorSingularName";
	public static final String BASE_RATES = "baseRates";
	public static final String MAJOR_PLURAL_NAME = "majorPluralName";
	public static final String CURRENCY_ID = "currencyId";
	public static final String RATE_SET = "rateSet";
	public static final String MINOR_SINGULAR_NAME = "minorSingularName";
	public static final String CURRENCY_CODE = "currencyCode";
	public static final String MINOR_UNIT = "minorUnit";

}

