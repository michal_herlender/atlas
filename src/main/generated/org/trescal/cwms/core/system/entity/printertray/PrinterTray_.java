package org.trescal.cwms.core.system.entity.printertray;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.system.entity.papertype.PaperType;
import org.trescal.cwms.core.system.entity.printer.Printer;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PrinterTray.class)
public abstract class PrinterTray_ {

	public static volatile SingularAttribute<PrinterTray, Integer> trayNo;
	public static volatile SingularAttribute<PrinterTray, Integer> paperSource;
	public static volatile SingularAttribute<PrinterTray, PaperType> paperType;
	public static volatile SingularAttribute<PrinterTray, Printer> printer;
	public static volatile SingularAttribute<PrinterTray, Integer> id;
	public static volatile SingularAttribute<PrinterTray, MediaTrayRef> mediaTrayRef;

	public static final String TRAY_NO = "trayNo";
	public static final String PAPER_SOURCE = "paperSource";
	public static final String PAPER_TYPE = "paperType";
	public static final String PRINTER = "printer";
	public static final String ID = "id";
	public static final String MEDIA_TRAY_REF = "mediaTrayRef";

}

