package org.trescal.cwms.core.system.entity.deletedcomponent;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(DeletedComponent.class)
public abstract class DeletedComponent_ {

	public static volatile SingularAttribute<DeletedComponent, Date> date;
	public static volatile SingularAttribute<DeletedComponent, String> reason;
	public static volatile SingularAttribute<DeletedComponent, String> refNo;
	public static volatile SingularAttribute<DeletedComponent, Component> component;
	public static volatile SingularAttribute<DeletedComponent, Contact> contact;
	public static volatile SingularAttribute<DeletedComponent, Integer> id;

	public static final String DATE = "date";
	public static final String REASON = "reason";
	public static final String REF_NO = "refNo";
	public static final String COMPONENT = "component";
	public static final String CONTACT = "contact";
	public static final String ID = "id";

}

