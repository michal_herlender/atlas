package org.trescal.cwms.core.system.entity.note;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Note.class)
public abstract class Note_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<Note, String> note;
	public static volatile SingularAttribute<Note, Date> setOn;
	public static volatile SingularAttribute<Note, Date> deactivatedOn;
	public static volatile SingularAttribute<Note, Boolean> publish;
	public static volatile SingularAttribute<Note, Boolean> active;
	public static volatile SingularAttribute<Note, Integer> noteid;
	public static volatile SingularAttribute<Note, String> label;
	public static volatile SingularAttribute<Note, Contact> setBy;
	public static volatile SingularAttribute<Note, Contact> deactivatedBy;

	public static final String NOTE = "note";
	public static final String SET_ON = "setOn";
	public static final String DEACTIVATED_ON = "deactivatedOn";
	public static final String PUBLISH = "publish";
	public static final String ACTIVE = "active";
	public static final String NOTEID = "noteid";
	public static final String LABEL = "label";
	public static final String SET_BY = "setBy";
	public static final String DEACTIVATED_BY = "deactivatedBy";

}

