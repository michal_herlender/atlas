package org.trescal.cwms.core.system.entity.systemdefault;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.system.entity.systemdefaultapplication.SystemDefaultApplication;
import org.trescal.cwms.core.system.entity.systemdefaultgroup.SystemDefaultGroup;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultNames;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.enums.Scope;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SystemDefault.class)
public abstract class SystemDefault_ {

	public static volatile SetAttribute<SystemDefault, CompanyRole> companyRoles;
	public static volatile SetAttribute<SystemDefault, Translation> defaultNameTranslation;
	public static volatile SingularAttribute<SystemDefault, String> defaultValue;
	public static volatile SingularAttribute<SystemDefault, Integer> start;
	public static volatile SingularAttribute<SystemDefault, Boolean> groupwide;
	public static volatile SingularAttribute<SystemDefault, Integer> end;
	public static volatile SetAttribute<SystemDefault, Translation> defaultDescriptionTranslation;
	public static volatile SingularAttribute<SystemDefault, SystemDefaultNames> id;
	public static volatile SetAttribute<SystemDefault, Scope> scopes;
	public static volatile SingularAttribute<SystemDefault, String> defaultName;
	public static volatile SingularAttribute<SystemDefault, String> defaultDataType;
	public static volatile SetAttribute<SystemDefault, SystemDefaultApplication> applications;
	public static volatile SingularAttribute<SystemDefault, String> defaultDescription;
	public static volatile SingularAttribute<SystemDefault, SystemDefaultGroup> group;

	public static final String COMPANY_ROLES = "companyRoles";
	public static final String DEFAULT_NAME_TRANSLATION = "defaultNameTranslation";
	public static final String DEFAULT_VALUE = "defaultValue";
	public static final String START = "start";
	public static final String GROUPWIDE = "groupwide";
	public static final String END = "end";
	public static final String DEFAULT_DESCRIPTION_TRANSLATION = "defaultDescriptionTranslation";
	public static final String ID = "id";
	public static final String SCOPES = "scopes";
	public static final String DEFAULT_NAME = "defaultName";
	public static final String DEFAULT_DATA_TYPE = "defaultDataType";
	public static final String APPLICATIONS = "applications";
	public static final String DEFAULT_DESCRIPTION = "defaultDescription";
	public static final String GROUP = "group";

}

