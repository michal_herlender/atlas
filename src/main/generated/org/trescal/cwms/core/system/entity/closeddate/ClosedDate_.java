package org.trescal.cwms.core.system.entity.closeddate;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.company.Company;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ClosedDate.class)
public abstract class ClosedDate_ {

	public static volatile SingularAttribute<ClosedDate, Company> comp;
	public static volatile SingularAttribute<ClosedDate, String> reason;
	public static volatile SingularAttribute<ClosedDate, LocalDate> closedDate;
	public static volatile SingularAttribute<ClosedDate, Integer> id;

	public static final String COMP = "comp";
	public static final String REASON = "reason";
	public static final String CLOSED_DATE = "closedDate";
	public static final String ID = "id";

}

