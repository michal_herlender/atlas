package org.trescal.cwms.core.system.entity.webresource;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(WebResource.class)
public abstract class WebResource_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<WebResource, String> description;
	public static volatile SingularAttribute<WebResource, Integer> id;
	public static volatile SingularAttribute<WebResource, String> url;

	public static final String DESCRIPTION = "description";
	public static final String ID = "id";
	public static final String URL = "url";

}

