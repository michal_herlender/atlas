package org.trescal.cwms.core.system.entity.alligatorsettings;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AlligatorSettingsUsage.class)
public abstract class AlligatorSettingsUsage_ {

	public static volatile SingularAttribute<AlligatorSettingsUsage, AlligatorSettings> settingsProduction;
	public static volatile SingularAttribute<AlligatorSettingsUsage, Integer> id;
	public static volatile SingularAttribute<AlligatorSettingsUsage, AlligatorSettings> settingsTest;

	public static final String SETTINGS_PRODUCTION = "settingsProduction";
	public static final String ID = "id";
	public static final String SETTINGS_TEST = "settingsTest";

}

