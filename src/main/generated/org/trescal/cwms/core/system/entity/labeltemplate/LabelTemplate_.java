package org.trescal.cwms.core.system.entity.labeltemplate;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(LabelTemplate.class)
public abstract class LabelTemplate_ {

	public static volatile SetAttribute<LabelTemplate, LabelTemplateAdditionalField> additionalFields;
	public static volatile SingularAttribute<LabelTemplate, String> propertyName;
	public static volatile SingularAttribute<LabelTemplate, String> name;
	public static volatile SingularAttribute<LabelTemplate, Integer> id;
	public static volatile SingularAttribute<LabelTemplate, LabelTemplateType> labelTemplateType;

	public static final String ADDITIONAL_FIELDS = "additionalFields";
	public static final String PROPERTY_NAME = "propertyName";
	public static final String NAME = "name";
	public static final String ID = "id";
	public static final String LABEL_TEMPLATE_TYPE = "labelTemplateType";

}

