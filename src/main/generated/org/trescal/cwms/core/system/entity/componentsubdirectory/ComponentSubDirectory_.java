package org.trescal.cwms.core.system.entity.componentsubdirectory;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ComponentSubDirectory.class)
public abstract class ComponentSubDirectory_ {

	public static volatile SingularAttribute<ComponentSubDirectory, SystemComponent> component;
	public static volatile SingularAttribute<ComponentSubDirectory, Integer> dirId;
	public static volatile SingularAttribute<ComponentSubDirectory, String> dirName;

	public static final String COMPONENT = "component";
	public static final String DIR_ID = "dirId";
	public static final String DIR_NAME = "dirName";

}

