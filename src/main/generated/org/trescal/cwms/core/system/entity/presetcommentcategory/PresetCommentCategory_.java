package org.trescal.cwms.core.system.entity.presetcommentcategory;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.entity.presetcomment.PresetComment;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PresetCommentCategory.class)
public abstract class PresetCommentCategory_ {

	public static volatile SetAttribute<PresetCommentCategory, PresetComment> comments;
	public static volatile SingularAttribute<PresetCommentCategory, Date> setOn;
	public static volatile SetAttribute<PresetCommentCategory, Translation> translations;
	public static volatile SingularAttribute<PresetCommentCategory, Integer> id;
	public static volatile SingularAttribute<PresetCommentCategory, String> category;
	public static volatile SingularAttribute<PresetCommentCategory, Contact> setBy;

	public static final String COMMENTS = "comments";
	public static final String SET_ON = "setOn";
	public static final String TRANSLATIONS = "translations";
	public static final String ID = "id";
	public static final String CATEGORY = "category";
	public static final String SET_BY = "setBy";

}

