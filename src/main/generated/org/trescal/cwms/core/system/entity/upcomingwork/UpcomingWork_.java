package org.trescal.cwms.core.system.entity.upcomingwork;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.department.Department;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UpcomingWork.class)
public abstract class UpcomingWork_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<UpcomingWork, Company> comp;
	public static volatile SingularAttribute<UpcomingWork, Contact> addedBy;
	public static volatile SingularAttribute<UpcomingWork, Contact> contact;
	public static volatile SingularAttribute<UpcomingWork, LocalDate> dueDate;
	public static volatile SingularAttribute<UpcomingWork, String> description;
	public static volatile SingularAttribute<UpcomingWork, Boolean> active;
	public static volatile SingularAttribute<UpcomingWork, Contact> userResponsible;
	public static volatile SingularAttribute<UpcomingWork, Integer> id;
	public static volatile SingularAttribute<UpcomingWork, Department> department;
	public static volatile SingularAttribute<UpcomingWork, String> title;
	public static volatile SingularAttribute<UpcomingWork, LocalDate> startDate;

	public static final String COMP = "comp";
	public static final String ADDED_BY = "addedBy";
	public static final String CONTACT = "contact";
	public static final String DUE_DATE = "dueDate";
	public static final String DESCRIPTION = "description";
	public static final String ACTIVE = "active";
	public static final String USER_RESPONSIBLE = "userResponsible";
	public static final String ID = "id";
	public static final String DEPARTMENT = "department";
	public static final String TITLE = "title";
	public static final String START_DATE = "startDate";

}

