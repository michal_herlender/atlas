package org.trescal.cwms.core.system.entity.accreditedlabcaltype;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.system.entity.accreditedlab.AccreditedLab;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AccreditedLabCalType.class)
public abstract class AccreditedLabCalType_ {

	public static volatile SingularAttribute<AccreditedLabCalType, CalibrationType> calType;
	public static volatile SingularAttribute<AccreditedLabCalType, AccreditedLab> accreditedLab;
	public static volatile SingularAttribute<AccreditedLabCalType, String> labelTemplatePath;
	public static volatile SingularAttribute<AccreditedLabCalType, Integer> id;

	public static final String CAL_TYPE = "calType";
	public static final String ACCREDITED_LAB = "accreditedLab";
	public static final String LABEL_TEMPLATE_PATH = "labelTemplatePath";
	public static final String ID = "id";

}

