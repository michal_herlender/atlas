package org.trescal.cwms.core.system.entity.printer;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.entity.printertray.PrinterTray;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Printer.class)
public abstract class Printer_ extends org.trescal.cwms.core.system.entity.printer.AbstractPrinter_ {

	public static volatile SetAttribute<Printer, Address> usedByAddresses;
	public static volatile SetAttribute<Printer, Contact> usedByContacts;
	public static volatile SetAttribute<Printer, PrinterTray> trays;

	public static final String USED_BY_ADDRESSES = "usedByAddresses";
	public static final String USED_BY_CONTACTS = "usedByContacts";
	public static final String TRAYS = "trays";

}

