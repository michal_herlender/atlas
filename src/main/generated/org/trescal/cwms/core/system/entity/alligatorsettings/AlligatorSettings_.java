package org.trescal.cwms.core.system.entity.alligatorsettings;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AlligatorSettings.class)
public abstract class AlligatorSettings_ {

	public static volatile SingularAttribute<AlligatorSettings, String> uploadWindowsUsername;
	public static volatile SingularAttribute<AlligatorSettings, String> uploadTempDataFolder;
	public static volatile SingularAttribute<AlligatorSettings, String> uploadWindowsPassword;
	public static volatile SingularAttribute<AlligatorSettings, String> description;
	public static volatile SetAttribute<AlligatorSettings, AlligatorSettingsSubdiv> usageSubdivTest;
	public static volatile SingularAttribute<AlligatorSettings, Boolean> uploadUseWindowsCredentials;
	public static volatile SingularAttribute<AlligatorSettings, Boolean> uploadMoveUponUpload;
	public static volatile SingularAttribute<AlligatorSettings, String> uploadWindowsDomain;
	public static volatile SingularAttribute<AlligatorSettings, String> uploadRawDataFolder;
	public static volatile SetAttribute<AlligatorSettings, AlligatorSettingsSubdiv> usageSubdivProduction;
	public static volatile SingularAttribute<AlligatorSettings, Boolean> globalDefault;
	public static volatile SingularAttribute<AlligatorSettings, Boolean> excelRelativeTempFolder;
	public static volatile SingularAttribute<AlligatorSettings, Boolean> uploadEnabled;
	public static volatile SetAttribute<AlligatorSettings, AlligatorSettingsContact> usageContactTest;
	public static volatile SingularAttribute<AlligatorSettings, Integer> id;
	public static volatile SetAttribute<AlligatorSettings, AlligatorSettingsContact> usageContactProduction;
	public static volatile SingularAttribute<AlligatorSettings, Boolean> excelSaveTempCopy;
	public static volatile SingularAttribute<AlligatorSettings, String> excelLocalTempFolder;

	public static final String UPLOAD_WINDOWS_USERNAME = "uploadWindowsUsername";
	public static final String UPLOAD_TEMP_DATA_FOLDER = "uploadTempDataFolder";
	public static final String UPLOAD_WINDOWS_PASSWORD = "uploadWindowsPassword";
	public static final String DESCRIPTION = "description";
	public static final String USAGE_SUBDIV_TEST = "usageSubdivTest";
	public static final String UPLOAD_USE_WINDOWS_CREDENTIALS = "uploadUseWindowsCredentials";
	public static final String UPLOAD_MOVE_UPON_UPLOAD = "uploadMoveUponUpload";
	public static final String UPLOAD_WINDOWS_DOMAIN = "uploadWindowsDomain";
	public static final String UPLOAD_RAW_DATA_FOLDER = "uploadRawDataFolder";
	public static final String USAGE_SUBDIV_PRODUCTION = "usageSubdivProduction";
	public static final String GLOBAL_DEFAULT = "globalDefault";
	public static final String EXCEL_RELATIVE_TEMP_FOLDER = "excelRelativeTempFolder";
	public static final String UPLOAD_ENABLED = "uploadEnabled";
	public static final String USAGE_CONTACT_TEST = "usageContactTest";
	public static final String ID = "id";
	public static final String USAGE_CONTACT_PRODUCTION = "usageContactProduction";
	public static final String EXCEL_SAVE_TEMP_COPY = "excelSaveTempCopy";
	public static final String EXCEL_LOCAL_TEMP_FOLDER = "excelLocalTempFolder";

}

