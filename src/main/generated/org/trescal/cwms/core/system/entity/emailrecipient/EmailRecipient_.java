package org.trescal.cwms.core.system.entity.emailrecipient;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.entity.email.Email;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(EmailRecipient.class)
public abstract class EmailRecipient_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<EmailRecipient, String> emailAddress;
	public static volatile SingularAttribute<EmailRecipient, Contact> recipientCon;
	public static volatile SingularAttribute<EmailRecipient, Integer> id;
	public static volatile SingularAttribute<EmailRecipient, RecipientType> type;
	public static volatile SingularAttribute<EmailRecipient, Email> email;

	public static final String EMAIL_ADDRESS = "emailAddress";
	public static final String RECIPIENT_CON = "recipientCon";
	public static final String ID = "id";
	public static final String TYPE = "type";
	public static final String EMAIL = "email";

}

