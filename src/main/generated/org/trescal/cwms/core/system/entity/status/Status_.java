package org.trescal.cwms.core.system.entity.status;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Status.class)
public abstract class Status_ extends org.trescal.cwms.core.system.entity.basestatus.BaseStatus_ {

	public static volatile SingularAttribute<Status, Boolean> followingIssue;
	public static volatile SingularAttribute<Status, Boolean> requiringAttention;
	public static volatile SingularAttribute<Status, Boolean> rejected;
	public static volatile SetAttribute<Status, Translation> nametranslations;
	public static volatile SingularAttribute<Status, Boolean> accepted;
	public static volatile SetAttribute<Status, Translation> descriptiontranslations;
	public static volatile SingularAttribute<Status, Boolean> issued;
	public static volatile SingularAttribute<Status, Boolean> followingReceipt;

	public static final String FOLLOWING_ISSUE = "followingIssue";
	public static final String REQUIRING_ATTENTION = "requiringAttention";
	public static final String REJECTED = "rejected";
	public static final String NAMETRANSLATIONS = "nametranslations";
	public static final String ACCEPTED = "accepted";
	public static final String DESCRIPTIONTRANSLATIONS = "descriptiontranslations";
	public static final String ISSUED = "issued";
	public static final String FOLLOWING_RECEIPT = "followingReceipt";

}

