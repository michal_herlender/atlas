package org.trescal.cwms.core.system.entity.markups.markuprange;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(MarkupRange.class)
public abstract class MarkupRange_ {

	public static volatile SingularAttribute<MarkupRange, Double> rangeStart;
	public static volatile SingularAttribute<MarkupRange, Double> actionValue;
	public static volatile SingularAttribute<MarkupRange, MarkupRangeAction> action;
	public static volatile SingularAttribute<MarkupRange, Integer> typeId;
	public static volatile SingularAttribute<MarkupRange, Integer> id;

	public static final String RANGE_START = "rangeStart";
	public static final String ACTION_VALUE = "actionValue";
	public static final String ACTION = "action";
	public static final String TYPE_ID = "typeId";
	public static final String ID = "id";

}

