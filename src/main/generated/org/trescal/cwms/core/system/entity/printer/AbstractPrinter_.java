package org.trescal.cwms.core.system.entity.printer;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.location.Location;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AbstractPrinter.class)
public abstract class AbstractPrinter_ {

	public static volatile SingularAttribute<AbstractPrinter, Location> loc;
	public static volatile SingularAttribute<AbstractPrinter, String> path;
	public static volatile SingularAttribute<AbstractPrinter, String> description;
	public static volatile SingularAttribute<AbstractPrinter, Integer> id;
	public static volatile SingularAttribute<AbstractPrinter, Address> addr;

	public static final String LOC = "loc";
	public static final String PATH = "path";
	public static final String DESCRIPTION = "description";
	public static final String ID = "id";
	public static final String ADDR = "addr";

}

