package org.trescal.cwms.core.system.entity.scheduledtask;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ScheduledTask.class)
public abstract class ScheduledTask_ {

	public static volatile SingularAttribute<ScheduledTask, Date> lastRunTime;
	public static volatile SingularAttribute<ScheduledTask, String> lastRunMessage;
	public static volatile SingularAttribute<ScheduledTask, String> quartzJobName;
	public static volatile SingularAttribute<ScheduledTask, String> description;
	public static volatile SingularAttribute<ScheduledTask, String> methodName;
	public static volatile SingularAttribute<ScheduledTask, Boolean> active;
	public static volatile SingularAttribute<ScheduledTask, String> className;
	public static volatile SingularAttribute<ScheduledTask, String> taskName;
	public static volatile SingularAttribute<ScheduledTask, Integer> id;
	public static volatile SingularAttribute<ScheduledTask, Boolean> lastRunSuccess;

	public static final String LAST_RUN_TIME = "lastRunTime";
	public static final String LAST_RUN_MESSAGE = "lastRunMessage";
	public static final String QUARTZ_JOB_NAME = "quartzJobName";
	public static final String DESCRIPTION = "description";
	public static final String METHOD_NAME = "methodName";
	public static final String ACTIVE = "active";
	public static final String CLASS_NAME = "className";
	public static final String TASK_NAME = "taskName";
	public static final String ID = "id";
	public static final String LAST_RUN_SUCCESS = "lastRunSuccess";

}

