package org.trescal.cwms.core.system.entity.accreditedlab;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.system.entity.accreditationbody.AccreditationBody;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AccreditedLab.class)
public abstract class AccreditedLab_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<AccreditedLab, String> labNo;
	public static volatile SingularAttribute<AccreditedLab, Boolean> watermarkForLab;
	public static volatile SingularAttribute<AccreditedLab, AccreditationBody> accreditationBody;
	public static volatile SingularAttribute<AccreditedLab, String> description;
	public static volatile SingularAttribute<AccreditedLab, Integer> id;

	public static final String LAB_NO = "labNo";
	public static final String WATERMARK_FOR_LAB = "watermarkForLab";
	public static final String ACCREDITATION_BODY = "accreditationBody";
	public static final String DESCRIPTION = "description";
	public static final String ID = "id";

}

