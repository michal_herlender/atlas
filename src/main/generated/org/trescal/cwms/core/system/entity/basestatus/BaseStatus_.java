package org.trescal.cwms.core.system.entity.basestatus;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BaseStatus.class)
public abstract class BaseStatus_ {

	public static volatile SingularAttribute<BaseStatus, Boolean> defaultStatus;
	public static volatile SingularAttribute<BaseStatus, Integer> statusid;
	public static volatile SingularAttribute<BaseStatus, String> name;
	public static volatile SingularAttribute<BaseStatus, String> description;

	public static final String DEFAULT_STATUS = "defaultStatus";
	public static final String STATUSID = "statusid";
	public static final String NAME = "name";
	public static final String DESCRIPTION = "description";

}

