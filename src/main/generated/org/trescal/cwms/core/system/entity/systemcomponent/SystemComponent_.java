package org.trescal.cwms.core.system.entity.systemcomponent;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.system.entity.componentdoctype.ComponentDoctype;
import org.trescal.cwms.core.system.entity.componentsubdirectory.ComponentSubDirectory;
import org.trescal.cwms.core.system.entity.defaultnote.DefaultNote;
import org.trescal.cwms.core.tools.filebrowser.RegularExpression;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SystemComponent.class)
public abstract class SystemComponent_ {

	public static volatile SingularAttribute<SystemComponent, String> defaultEmailBodyReference;
	public static volatile SingularAttribute<SystemComponent, String> defaultEmailSubject;
	public static volatile SingularAttribute<SystemComponent, Integer> componentId;
	public static volatile SingularAttribute<SystemComponent, String> appendToParentPath;
	public static volatile SingularAttribute<SystemComponent, String> rootDir;
	public static volatile ListAttribute<SystemComponent, RegularExpression> fileNameRegexs;
	public static volatile SingularAttribute<SystemComponent, Boolean> foldersYear;
	public static volatile SetAttribute<SystemComponent, DefaultNote> defaultNotes;
	public static volatile SingularAttribute<SystemComponent, Boolean> foldersPerItem;
	public static volatile SingularAttribute<SystemComponent, Boolean> emailTemplates;
	public static volatile SetAttribute<SystemComponent, ComponentDoctype> docTypes;
	public static volatile SingularAttribute<SystemComponent, Boolean> usingParentSettings;
	public static volatile SingularAttribute<SystemComponent, Component> component;
	public static volatile SingularAttribute<SystemComponent, String> numberedSubFolderDivider;
	public static volatile SingularAttribute<SystemComponent, Integer> foldersSplitRange;
	public static volatile SingularAttribute<SystemComponent, Boolean> numberedSubFolders;
	public static volatile SingularAttribute<SystemComponent, SystemComponent> parentComponent;
	public static volatile SetAttribute<SystemComponent, SystemComponent> childrenComponents;
	public static volatile SingularAttribute<SystemComponent, String> componentName;
	public static volatile SingularAttribute<SystemComponent, Boolean> foldersSplit;
	public static volatile SingularAttribute<SystemComponent, Boolean> foldersDropBoundaries;
	public static volatile SingularAttribute<SystemComponent, Character> prependToId;
	public static volatile SetAttribute<SystemComponent, ComponentSubDirectory> subDirs;

	public static final String DEFAULT_EMAIL_BODY_REFERENCE = "defaultEmailBodyReference";
	public static final String DEFAULT_EMAIL_SUBJECT = "defaultEmailSubject";
	public static final String COMPONENT_ID = "componentId";
	public static final String APPEND_TO_PARENT_PATH = "appendToParentPath";
	public static final String ROOT_DIR = "rootDir";
	public static final String FILE_NAME_REGEXS = "fileNameRegexs";
	public static final String FOLDERS_YEAR = "foldersYear";
	public static final String DEFAULT_NOTES = "defaultNotes";
	public static final String FOLDERS_PER_ITEM = "foldersPerItem";
	public static final String EMAIL_TEMPLATES = "emailTemplates";
	public static final String DOC_TYPES = "docTypes";
	public static final String USING_PARENT_SETTINGS = "usingParentSettings";
	public static final String COMPONENT = "component";
	public static final String NUMBERED_SUB_FOLDER_DIVIDER = "numberedSubFolderDivider";
	public static final String FOLDERS_SPLIT_RANGE = "foldersSplitRange";
	public static final String NUMBERED_SUB_FOLDERS = "numberedSubFolders";
	public static final String PARENT_COMPONENT = "parentComponent";
	public static final String CHILDREN_COMPONENTS = "childrenComponents";
	public static final String COMPONENT_NAME = "componentName";
	public static final String FOLDERS_SPLIT = "foldersSplit";
	public static final String FOLDERS_DROP_BOUNDARIES = "foldersDropBoundaries";
	public static final String PREPEND_TO_ID = "prependToId";
	public static final String SUB_DIRS = "subDirs";

}

