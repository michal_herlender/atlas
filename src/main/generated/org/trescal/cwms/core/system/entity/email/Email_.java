package org.trescal.cwms.core.system.entity.email;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.entity.emailrecipient.EmailRecipient;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Email.class)
public abstract class Email_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<Email, Component> component;
	public static volatile ListAttribute<Email, EmailRecipient> recipients;
	public static volatile SingularAttribute<Email, String> subject;
	public static volatile SingularAttribute<Email, Date> sentOn;
	public static volatile SingularAttribute<Email, Boolean> publish;
	public static volatile SingularAttribute<Email, Integer> entityId;
	public static volatile SingularAttribute<Email, String> from;
	public static volatile SingularAttribute<Email, Integer> id;
	public static volatile SingularAttribute<Email, String> body;
	public static volatile SingularAttribute<Email, String> bodyXindiceKey;
	public static volatile SingularAttribute<Email, Contact> sentBy;

	public static final String COMPONENT = "component";
	public static final String RECIPIENTS = "recipients";
	public static final String SUBJECT = "subject";
	public static final String SENT_ON = "sentOn";
	public static final String PUBLISH = "publish";
	public static final String ENTITY_ID = "entityId";
	public static final String FROM = "from";
	public static final String ID = "id";
	public static final String BODY = "body";
	public static final String BODY_XINDICE_KEY = "bodyXindiceKey";
	public static final String SENT_BY = "sentBy";

}

