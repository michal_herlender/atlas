package org.trescal.cwms.core.system.entity.systemdefaultapplication;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.systemdefault.SystemDefault;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SystemDefaultApplication.class)
public abstract class SystemDefaultApplication_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<SystemDefaultApplication, SystemDefault> systemDefault;
	public static volatile SingularAttribute<SystemDefaultApplication, Contact> contact;
	public static volatile SingularAttribute<SystemDefaultApplication, Company> company;
	public static volatile SingularAttribute<SystemDefaultApplication, Integer> id;
	public static volatile SingularAttribute<SystemDefaultApplication, Subdiv> subdiv;
	public static volatile SingularAttribute<SystemDefaultApplication, String> value;

	public static final String SYSTEM_DEFAULT = "systemDefault";
	public static final String CONTACT = "contact";
	public static final String COMPANY = "company";
	public static final String ID = "id";
	public static final String SUBDIV = "subdiv";
	public static final String VALUE = "value";

}

