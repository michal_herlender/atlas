package org.trescal.cwms.core.system.entity.presetcomment;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.entity.presetcommentcategory.PresetCommentCategory;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PresetComment.class)
public abstract class PresetComment_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<PresetComment, Date> seton;
	public static volatile SingularAttribute<PresetComment, String> comment;
	public static volatile SingularAttribute<PresetComment, Integer> id;
	public static volatile SingularAttribute<PresetComment, String> label;
	public static volatile SingularAttribute<PresetComment, PresetCommentType> type;
	public static volatile SingularAttribute<PresetComment, PresetCommentCategory> category;
	public static volatile SingularAttribute<PresetComment, Contact> setby;

	public static final String SETON = "seton";
	public static final String COMMENT = "comment";
	public static final String ID = "id";
	public static final String LABEL = "label";
	public static final String TYPE = "type";
	public static final String CATEGORY = "category";
	public static final String SETBY = "setby";

}

