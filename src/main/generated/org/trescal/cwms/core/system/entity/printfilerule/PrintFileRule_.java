package org.trescal.cwms.core.system.entity.printfilerule;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.system.entity.papertype.PaperType;
import org.trescal.cwms.core.system.entity.printer.Printer;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PrintFileRule.class)
public abstract class PrintFileRule_ {

	public static volatile SingularAttribute<PrintFileRule, PaperType> firstPagePaperType;
	public static volatile SingularAttribute<PrintFileRule, String> fileNameLike;
	public static volatile SingularAttribute<PrintFileRule, Integer> copies;
	public static volatile SingularAttribute<PrintFileRule, PaperType> restPagePaperType;
	public static volatile SingularAttribute<PrintFileRule, Printer> printer;
	public static volatile SingularAttribute<PrintFileRule, Integer> id;

	public static final String FIRST_PAGE_PAPER_TYPE = "firstPagePaperType";
	public static final String FILE_NAME_LIKE = "fileNameLike";
	public static final String COPIES = "copies";
	public static final String REST_PAGE_PAPER_TYPE = "restPagePaperType";
	public static final String PRINTER = "printer";
	public static final String ID = "id";

}

