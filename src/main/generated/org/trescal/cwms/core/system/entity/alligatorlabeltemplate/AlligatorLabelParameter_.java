package org.trescal.cwms.core.system.entity.alligatorlabeltemplate;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.system.entity.alligatorlabeltemplate.enums.AlligatorParameterValue;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AlligatorLabelParameter.class)
public abstract class AlligatorLabelParameter_ {

	public static volatile SingularAttribute<AlligatorLabelParameter, AlligatorLabelTemplate> template;
	public static volatile SingularAttribute<AlligatorLabelParameter, String> name;
	public static volatile SingularAttribute<AlligatorLabelParameter, String> format;
	public static volatile SingularAttribute<AlligatorLabelParameter, Integer> id;
	public static volatile SingularAttribute<AlligatorLabelParameter, String> text;
	public static volatile SingularAttribute<AlligatorLabelParameter, AlligatorParameterValue> value;

	public static final String TEMPLATE = "template";
	public static final String NAME = "name";
	public static final String FORMAT = "format";
	public static final String ID = "id";
	public static final String TEXT = "text";
	public static final String VALUE = "value";

}

