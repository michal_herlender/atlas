package org.trescal.cwms.core.system.entity.defaultnote;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(DefaultNote.class)
public abstract class DefaultNote_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<DefaultNote, String> note;
	public static volatile SingularAttribute<DefaultNote, Integer> defaultNoteId;
	public static volatile SingularAttribute<DefaultNote, SystemComponent> component;
	public static volatile SetAttribute<DefaultNote, Translation> noteTranslation;
	public static volatile SingularAttribute<DefaultNote, Boolean> publish;
	public static volatile SetAttribute<DefaultNote, Translation> labelTranslation;
	public static volatile SingularAttribute<DefaultNote, String> label;

	public static final String NOTE = "note";
	public static final String DEFAULT_NOTE_ID = "defaultNoteId";
	public static final String COMPONENT = "component";
	public static final String NOTE_TRANSLATION = "noteTranslation";
	public static final String PUBLISH = "publish";
	public static final String LABEL_TRANSLATION = "labelTranslation";
	public static final String LABEL = "label";

}

