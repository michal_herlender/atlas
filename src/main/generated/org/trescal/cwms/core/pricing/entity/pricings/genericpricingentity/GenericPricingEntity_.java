package org.trescal.cwms.core.pricing.entity.pricings.genericpricingentity;

import java.math.BigDecimal;
import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(GenericPricingEntity.class)
public abstract class GenericPricingEntity_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<GenericPricingEntity, Integer> duration;
	public static volatile SingularAttribute<GenericPricingEntity, LocalDate> expiryDate;
	public static volatile SingularAttribute<GenericPricingEntity, Contact> createdBy;
	public static volatile SingularAttribute<GenericPricingEntity, BigDecimal> rate;
	public static volatile SingularAttribute<GenericPricingEntity, Contact> issueby;
	public static volatile SingularAttribute<GenericPricingEntity, LocalDate> regdate;
	public static volatile SingularAttribute<GenericPricingEntity, String> clientref;
	public static volatile SingularAttribute<GenericPricingEntity, SupportedCurrency> currency;
	public static volatile SingularAttribute<GenericPricingEntity, Integer> id;
	public static volatile SingularAttribute<GenericPricingEntity, LocalDate> reqdate;
	public static volatile SingularAttribute<GenericPricingEntity, Boolean> issued;
	public static volatile SingularAttribute<GenericPricingEntity, LocalDate> issuedate;

	public static final String DURATION = "duration";
	public static final String EXPIRY_DATE = "expiryDate";
	public static final String CREATED_BY = "createdBy";
	public static final String RATE = "rate";
	public static final String ISSUEBY = "issueby";
	public static final String REGDATE = "regdate";
	public static final String CLIENTREF = "clientref";
	public static final String CURRENCY = "currency";
	public static final String ID = "id";
	public static final String REQDATE = "reqdate";
	public static final String ISSUED = "issued";
	public static final String ISSUEDATE = "issuedate";

}

