package org.trescal.cwms.core.pricing.entity.costs.base;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Cost.class)
public abstract class Cost_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<Cost, BigDecimal> discountRate;
	public static volatile SingularAttribute<Cost, Integer> customOrderBy;
	public static volatile SingularAttribute<Cost, BigDecimal> finalCost;
	public static volatile SingularAttribute<Cost, CostType> costType;
	public static volatile SingularAttribute<Cost, Integer> costid;
	public static volatile SingularAttribute<Cost, BigDecimal> labourCostTotal;
	public static volatile SingularAttribute<Cost, Integer> labourTime;
	public static volatile SingularAttribute<Cost, Boolean> active;
	public static volatile SingularAttribute<Cost, Boolean> autoSet;
	public static volatile SingularAttribute<Cost, BigDecimal> hourlyRate;
	public static volatile SingularAttribute<Cost, BigDecimal> discountValue;
	public static volatile SingularAttribute<Cost, BigDecimal> totalCost;

	public static final String DISCOUNT_RATE = "discountRate";
	public static final String CUSTOM_ORDER_BY = "customOrderBy";
	public static final String FINAL_COST = "finalCost";
	public static final String COST_TYPE = "costType";
	public static final String COSTID = "costid";
	public static final String LABOUR_COST_TOTAL = "labourCostTotal";
	public static final String LABOUR_TIME = "labourTime";
	public static final String ACTIVE = "active";
	public static final String AUTO_SET = "autoSet";
	public static final String HOURLY_RATE = "hourlyRate";
	public static final String DISCOUNT_VALUE = "discountValue";
	public static final String TOTAL_COST = "totalCost";

}

