package org.trescal.cwms.core.pricing.entity.pricingremark;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem.PricingItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PricingItemRemark.class)
public abstract class PricingItemRemark_ extends org.trescal.cwms.core.pricing.entity.pricingremark.PricingRemark_ {

	public static volatile SingularAttribute<PricingItemRemark, PricingItem> pricingItem;

	public static final String PRICING_ITEM = "pricingItem";

}

