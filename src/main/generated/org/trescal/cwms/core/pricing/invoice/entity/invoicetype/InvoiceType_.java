package org.trescal.cwms.core.pricing.invoice.entity.invoicetype;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InvoiceType.class)
public abstract class InvoiceType_ {

	public static volatile SetAttribute<InvoiceType, Invoice> invoices;
	public static volatile SingularAttribute<InvoiceType, Boolean> includeOnJobSheet;
	public static volatile SingularAttribute<InvoiceType, String> name;
	public static volatile SingularAttribute<InvoiceType, Boolean> defToSingleCost;
	public static volatile SingularAttribute<InvoiceType, Boolean> addToAccount;
	public static volatile SingularAttribute<InvoiceType, Integer> id;
	public static volatile SetAttribute<InvoiceType, Translation> nametranslation;

	public static final String INVOICES = "invoices";
	public static final String INCLUDE_ON_JOB_SHEET = "includeOnJobSheet";
	public static final String NAME = "name";
	public static final String DEF_TO_SINGLE_COST = "defToSingleCost";
	public static final String ADD_TO_ACCOUNT = "addToAccount";
	public static final String ID = "id";
	public static final String NAMETRANSLATION = "nametranslation";

}

