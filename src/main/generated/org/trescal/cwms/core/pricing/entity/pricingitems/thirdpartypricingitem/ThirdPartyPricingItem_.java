package org.trescal.cwms.core.pricing.entity.pricingitems.thirdpartypricingitem;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.entity.enums.ThirdCostMarkupSource;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ThirdPartyPricingItem.class)
public abstract class ThirdPartyPricingItem_ extends org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem.PricingItem_ {

	public static volatile SingularAttribute<ThirdPartyPricingItem, BigDecimal> tpCarriageIn;
	public static volatile SingularAttribute<ThirdPartyPricingItem, BigDecimal> tpCarriageOutMarkupValue;
	public static volatile SingularAttribute<ThirdPartyPricingItem, ThirdCostMarkupSource> tpCarriageMarkupSrc;
	public static volatile SingularAttribute<ThirdPartyPricingItem, BigDecimal> tpCarriageInMarkupValue;
	public static volatile SingularAttribute<ThirdPartyPricingItem, BigDecimal> tpCarriageTotal;
	public static volatile SingularAttribute<ThirdPartyPricingItem, BigDecimal> tpCarriageOut;
	public static volatile SingularAttribute<ThirdPartyPricingItem, BigDecimal> tpCarriageMarkupRate;

	public static final String TP_CARRIAGE_IN = "tpCarriageIn";
	public static final String TP_CARRIAGE_OUT_MARKUP_VALUE = "tpCarriageOutMarkupValue";
	public static final String TP_CARRIAGE_MARKUP_SRC = "tpCarriageMarkupSrc";
	public static final String TP_CARRIAGE_IN_MARKUP_VALUE = "tpCarriageInMarkupValue";
	public static final String TP_CARRIAGE_TOTAL = "tpCarriageTotal";
	public static final String TP_CARRIAGE_OUT = "tpCarriageOut";
	public static final String TP_CARRIAGE_MARKUP_RATE = "tpCarriageMarkupRate";

}

