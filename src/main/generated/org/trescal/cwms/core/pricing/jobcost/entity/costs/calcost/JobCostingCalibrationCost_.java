package org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobCostingCalibrationCost.class)
public abstract class JobCostingCalibrationCost_ extends org.trescal.cwms.core.pricing.entity.costs.thirdparty.TPSupportedCalCost_ {

	public static volatile SingularAttribute<JobCostingCalibrationCost, JobCostingLinkedCalibrationCost> linkedCost;
	public static volatile SingularAttribute<JobCostingCalibrationCost, JobCostingItem> jobCostingItem;

	public static final String LINKED_COST = "linkedCost";
	public static final String JOB_COSTING_ITEM = "jobCostingItem";

}

