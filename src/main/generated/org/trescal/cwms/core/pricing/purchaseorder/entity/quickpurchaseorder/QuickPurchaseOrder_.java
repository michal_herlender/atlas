package org.trescal.cwms.core.pricing.purchaseorder.entity.quickpurchaseorder;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(QuickPurchaseOrder.class)
public abstract class QuickPurchaseOrder_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<QuickPurchaseOrder, Boolean> consolidated;
	public static volatile SingularAttribute<QuickPurchaseOrder, String> pono;
	public static volatile SingularAttribute<QuickPurchaseOrder, Contact> createdBy;
	public static volatile SingularAttribute<QuickPurchaseOrder, String> jobno;
	public static volatile SingularAttribute<QuickPurchaseOrder, String> description;
	public static volatile SingularAttribute<QuickPurchaseOrder, Integer> id;
	public static volatile SingularAttribute<QuickPurchaseOrder, Date> createdOn;

	public static final String CONSOLIDATED = "consolidated";
	public static final String PONO = "pono";
	public static final String CREATED_BY = "createdBy";
	public static final String JOBNO = "jobno";
	public static final String DESCRIPTION = "description";
	public static final String ID = "id";
	public static final String CREATED_ON = "createdOn";

}

