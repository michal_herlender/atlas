package org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.address.Address;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TaxablePricingItem.class)
public abstract class TaxablePricingItem_ extends org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem.PricingItem_ {

	public static volatile SingularAttribute<TaxablePricingItem, Address> shipToAddress;
	public static volatile SingularAttribute<TaxablePricingItem, Boolean> taxable;
	public static volatile SingularAttribute<TaxablePricingItem, BigDecimal> taxAmount;

	public static final String SHIP_TO_ADDRESS = "shipToAddress";
	public static final String TAXABLE = "taxable";
	public static final String TAX_AMOUNT = "taxAmount";

}

