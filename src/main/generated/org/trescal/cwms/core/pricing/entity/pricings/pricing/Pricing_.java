package org.trescal.cwms.core.pricing.entity.pricings.pricing;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Pricing.class)
public abstract class Pricing_ extends org.trescal.cwms.core.pricing.entity.pricings.genericpricingentity.GenericPricingEntity_ {

	public static volatile SingularAttribute<Pricing, BigDecimal> finalCost;
	public static volatile SingularAttribute<Pricing, Contact> contact;
	public static volatile SingularAttribute<Pricing, BigDecimal> vatRate;
	public static volatile SingularAttribute<Pricing, BigDecimal> vatValue;
	public static volatile SingularAttribute<Pricing, BigDecimal> totalCost;

	public static final String FINAL_COST = "finalCost";
	public static final String CONTACT = "contact";
	public static final String VAT_RATE = "vatRate";
	public static final String VAT_VALUE = "vatValue";
	public static final String TOTAL_COST = "totalCost";

}

