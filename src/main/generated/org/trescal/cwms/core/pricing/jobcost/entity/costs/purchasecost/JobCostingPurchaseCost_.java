package org.trescal.cwms.core.pricing.jobcost.entity.costs.purchasecost;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobCostingPurchaseCost.class)
public abstract class JobCostingPurchaseCost_ extends org.trescal.cwms.core.pricing.entity.costs.thirdparty.TPSupportedPurchaseCost_ {

	public static volatile SingularAttribute<JobCostingPurchaseCost, JobCostingLinkedPurchaseCost> linkedCost;
	public static volatile SingularAttribute<JobCostingPurchaseCost, CostSource> costSrc;
	public static volatile SingularAttribute<JobCostingPurchaseCost, JobCostingItem> jobCostingItem;

	public static final String LINKED_COST = "linkedCost";
	public static final String COST_SRC = "costSrc";
	public static final String JOB_COSTING_ITEM = "jobCostingItem";

}

