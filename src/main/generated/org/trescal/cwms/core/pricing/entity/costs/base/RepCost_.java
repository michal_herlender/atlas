package org.trescal.cwms.core.pricing.entity.costs.base;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.entity.enums.PartsMarkupSource;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RepCost.class)
public abstract class RepCost_ extends org.trescal.cwms.core.pricing.entity.costs.base.Cost_ {

	public static volatile SingularAttribute<RepCost, BigDecimal> partsMarkupRate;
	public static volatile SingularAttribute<RepCost, BigDecimal> partsCost;
	public static volatile SingularAttribute<RepCost, BigDecimal> partsMarkupValue;
	public static volatile SingularAttribute<RepCost, PartsMarkupSource> partsMarkupSource;
	public static volatile SingularAttribute<RepCost, BigDecimal> partsTotal;

	public static final String PARTS_MARKUP_RATE = "partsMarkupRate";
	public static final String PARTS_COST = "partsCost";
	public static final String PARTS_MARKUP_VALUE = "partsMarkupValue";
	public static final String PARTS_MARKUP_SOURCE = "partsMarkupSource";
	public static final String PARTS_TOTAL = "partsTotal";

}

