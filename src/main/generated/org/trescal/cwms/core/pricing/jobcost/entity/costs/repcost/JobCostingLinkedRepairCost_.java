package org.trescal.cwms.core.pricing.jobcost.entity.costs.repcost;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.repcost.ContractReviewRepairCost;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobCostingLinkedRepairCost.class)
public abstract class JobCostingLinkedRepairCost_ extends org.trescal.cwms.core.pricing.jobcost.entity.costs.JobCostingLinkedCost_ {

	public static volatile SingularAttribute<JobCostingLinkedRepairCost, ContractReviewRepairCost> contractReviewRepairCost;
	public static volatile SingularAttribute<JobCostingLinkedRepairCost, JobCostingRepairCost> jobCostingRepairCost;
	public static volatile SingularAttribute<JobCostingLinkedRepairCost, JobCostingRepairCost> repairCost;

	public static final String CONTRACT_REVIEW_REPAIR_COST = "contractReviewRepairCost";
	public static final String JOB_COSTING_REPAIR_COST = "jobCostingRepairCost";
	public static final String REPAIR_COST = "repairCost";

}

