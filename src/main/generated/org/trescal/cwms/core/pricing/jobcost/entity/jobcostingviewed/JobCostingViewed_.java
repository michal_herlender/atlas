package org.trescal.cwms.core.pricing.jobcost.entity.jobcostingviewed;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobCostingViewed.class)
public abstract class JobCostingViewed_ {

	public static volatile SingularAttribute<JobCostingViewed, Contact> lastViewedBy;
	public static volatile SingularAttribute<JobCostingViewed, JobCosting> jobCosting;
	public static volatile SingularAttribute<JobCostingViewed, Integer> id;
	public static volatile SingularAttribute<JobCostingViewed, Date> lastViewedOn;

	public static final String LAST_VIEWED_BY = "lastViewedBy";
	public static final String JOB_COSTING = "jobCosting";
	public static final String ID = "id";
	public static final String LAST_VIEWED_ON = "lastViewedOn";

}

