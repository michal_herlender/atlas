package org.trescal.cwms.core.pricing.invoice.entity.invoicedoctypetemplate;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.InvoiceType;
import org.trescal.cwms.core.system.entity.template.Template;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InvoiceDoctypeTemplate.class)
public abstract class InvoiceDoctypeTemplate_ extends org.trescal.cwms.core.system.entity.componentdoctypetemplate.ComponentDoctypeTemplate_ {

	public static volatile SingularAttribute<InvoiceDoctypeTemplate, String> description;
	public static volatile SingularAttribute<InvoiceDoctypeTemplate, Company> company;
	public static volatile SingularAttribute<InvoiceDoctypeTemplate, Template> supplementaryTemplate;
	public static volatile SingularAttribute<InvoiceDoctypeTemplate, InvoiceType> type;

	public static final String DESCRIPTION = "description";
	public static final String COMPANY = "company";
	public static final String SUPPLEMENTARY_TEMPLATE = "supplementaryTemplate";
	public static final String TYPE = "type";

}

