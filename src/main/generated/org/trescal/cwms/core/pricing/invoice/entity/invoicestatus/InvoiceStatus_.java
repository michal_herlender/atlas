package org.trescal.cwms.core.pricing.invoice.entity.invoicestatus;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.system.entity.basestatus.ActionType;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InvoiceStatus.class)
public abstract class InvoiceStatus_ extends org.trescal.cwms.core.system.entity.basestatus.BaseStatus_ {

	public static volatile SingularAttribute<InvoiceStatus, InvoiceStatus> next;
	public static volatile SingularAttribute<InvoiceStatus, Boolean> approved;
	public static volatile SingularAttribute<InvoiceStatus, Boolean> requiresAttention;
	public static volatile ListAttribute<InvoiceStatus, Invoice> invoices;
	public static volatile SetAttribute<InvoiceStatus, Translation> nametranslations;
	public static volatile SingularAttribute<InvoiceStatus, Boolean> onIssue;
	public static volatile SetAttribute<InvoiceStatus, Translation> descriptiontranslations;
	public static volatile SingularAttribute<InvoiceStatus, ActionType> type;
	public static volatile SingularAttribute<InvoiceStatus, Boolean> issued;
	public static volatile SingularAttribute<InvoiceStatus, Boolean> onInsert;

	public static final String NEXT = "next";
	public static final String APPROVED = "approved";
	public static final String REQUIRES_ATTENTION = "requiresAttention";
	public static final String INVOICES = "invoices";
	public static final String NAMETRANSLATIONS = "nametranslations";
	public static final String ON_ISSUE = "onIssue";
	public static final String DESCRIPTIONTRANSLATIONS = "descriptiontranslations";
	public static final String TYPE = "type";
	public static final String ISSUED = "issued";
	public static final String ON_INSERT = "onInsert";

}

