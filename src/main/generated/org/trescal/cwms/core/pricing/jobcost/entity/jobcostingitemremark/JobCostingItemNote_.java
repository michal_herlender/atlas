package org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitemremark;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobCostingItemNote.class)
public abstract class JobCostingItemNote_ extends org.trescal.cwms.core.system.entity.note.Note_ {

	public static volatile SingularAttribute<JobCostingItemNote, JobCostingItem> jobCostingItem;

	public static final String JOB_COSTING_ITEM = "jobCostingItem";

}

