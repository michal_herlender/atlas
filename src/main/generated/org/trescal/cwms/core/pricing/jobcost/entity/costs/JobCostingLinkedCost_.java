package org.trescal.cwms.core.pricing.jobcost.entity.costs;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobCostingLinkedCost.class)
public abstract class JobCostingLinkedCost_ {

	public static volatile SingularAttribute<JobCostingLinkedCost, Integer> id;

	public static final String ID = "id";

}

