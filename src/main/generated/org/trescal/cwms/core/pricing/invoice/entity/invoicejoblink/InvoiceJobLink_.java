package org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InvoiceJobLink.class)
public abstract class InvoiceJobLink_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<InvoiceJobLink, Boolean> systemJob;
	public static volatile SingularAttribute<InvoiceJobLink, String> jobno;
	public static volatile SingularAttribute<InvoiceJobLink, Integer> id;
	public static volatile SingularAttribute<InvoiceJobLink, Invoice> invoice;
	public static volatile SingularAttribute<InvoiceJobLink, Job> job;

	public static final String SYSTEM_JOB = "systemJob";
	public static final String JOBNO = "jobno";
	public static final String ID = "id";
	public static final String INVOICE = "invoice";
	public static final String JOB = "job";

}

