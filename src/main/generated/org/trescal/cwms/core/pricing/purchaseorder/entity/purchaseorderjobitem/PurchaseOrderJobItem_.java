package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PurchaseOrderJobItem.class)
public abstract class PurchaseOrderJobItem_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<PurchaseOrderJobItem, Integer> id;
	public static volatile SingularAttribute<PurchaseOrderJobItem, PurchaseOrderItem> poitem;
	public static volatile SingularAttribute<PurchaseOrderJobItem, JobItem> ji;

	public static final String ID = "id";
	public static final String POITEM = "poitem";
	public static final String JI = "ji";

}

