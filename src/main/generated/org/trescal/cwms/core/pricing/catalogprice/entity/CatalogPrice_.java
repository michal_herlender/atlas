package org.trescal.cwms.core.pricing.catalogprice.entity;

import java.math.BigDecimal;
import java.time.Duration;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.variableprice.entity.VariablePrice;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CatalogPrice.class)
public abstract class CatalogPrice_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<CatalogPrice, ServiceType> serviceType;
	public static volatile SingularAttribute<CatalogPrice, String> comments;
	public static volatile SingularAttribute<CatalogPrice, CostType> costType;
	public static volatile SingularAttribute<CatalogPrice, Duration> standardTime;
	public static volatile SingularAttribute<CatalogPrice, VariablePrice> variablePrice;
	public static volatile SingularAttribute<CatalogPrice, InstrumentModel> instrumentModel;
	public static volatile SingularAttribute<CatalogPrice, Integer> id;
	public static volatile SingularAttribute<CatalogPrice, BigDecimal> fixedPrice;

	public static final String SERVICE_TYPE = "serviceType";
	public static final String COMMENTS = "comments";
	public static final String COST_TYPE = "costType";
	public static final String STANDARD_TIME = "standardTime";
	public static final String VARIABLE_PRICE = "variablePrice";
	public static final String INSTRUMENT_MODEL = "instrumentModel";
	public static final String ID = "id";
	public static final String FIXED_PRICE = "fixedPrice";

}

