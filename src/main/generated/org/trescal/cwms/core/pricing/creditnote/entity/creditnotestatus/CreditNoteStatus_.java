package org.trescal.cwms.core.pricing.creditnote.entity.creditnotestatus;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.system.entity.basestatus.ActionType;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CreditNoteStatus.class)
public abstract class CreditNoteStatus_ extends org.trescal.cwms.core.system.entity.basestatus.BaseStatus_ {

	public static volatile SingularAttribute<CreditNoteStatus, CreditNoteStatus> next;
	public static volatile SingularAttribute<CreditNoteStatus, Boolean> requiresAttention;
	public static volatile SetAttribute<CreditNoteStatus, Translation> nametranslations;
	public static volatile SingularAttribute<CreditNoteStatus, Boolean> onIssue;
	public static volatile SetAttribute<CreditNoteStatus, Translation> descriptiontranslations;
	public static volatile SingularAttribute<CreditNoteStatus, ActionType> type;
	public static volatile SingularAttribute<CreditNoteStatus, Boolean> onInsert;

	public static final String NEXT = "next";
	public static final String REQUIRES_ATTENTION = "requiresAttention";
	public static final String NAMETRANSLATIONS = "nametranslations";
	public static final String ON_ISSUE = "onIssue";
	public static final String DESCRIPTIONTRANSLATIONS = "descriptiontranslations";
	public static final String TYPE = "type";
	public static final String ON_INSERT = "onInsert";

}

