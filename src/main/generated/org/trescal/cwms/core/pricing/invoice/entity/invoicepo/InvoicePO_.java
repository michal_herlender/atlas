package org.trescal.cwms.core.pricing.invoice.entity.invoicepo;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoiceItemPO;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoicePOItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InvoicePO.class)
public abstract class InvoicePO_ extends org.trescal.cwms.core.jobs.job.entity.abstractpo.AbstractPO_ {

	public static volatile ListAttribute<InvoicePO, InvoicePOItem> newInvItemPOs;
	public static volatile SingularAttribute<InvoicePO, Invoice> invoice;
	public static volatile ListAttribute<InvoicePO, InvoiceItemPO> invItemPOs;

	public static final String NEW_INV_ITEM_POS = "newInvItemPOs";
	public static final String INVOICE = "invoice";
	public static final String INV_ITEM_POS = "invItemPOs";

}

