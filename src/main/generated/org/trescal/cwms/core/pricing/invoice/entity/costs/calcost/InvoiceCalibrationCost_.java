package org.trescal.cwms.core.pricing.invoice.entity.costs.calcost;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InvoiceCalibrationCost.class)
public abstract class InvoiceCalibrationCost_ extends org.trescal.cwms.core.pricing.entity.costs.base.CalCost_ {

	public static volatile SingularAttribute<InvoiceCalibrationCost, NominalCode> nominal;
	public static volatile SingularAttribute<InvoiceCalibrationCost, InvoiceItem> invoiceItem;

	public static final String NOMINAL = "nominal";
	public static final String INVOICE_ITEM = "invoiceItem";

}

