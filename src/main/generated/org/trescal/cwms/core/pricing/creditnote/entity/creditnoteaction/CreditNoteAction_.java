package org.trescal.cwms.core.pricing.creditnote.entity.creditnoteaction;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnotestatus.CreditNoteStatus;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CreditNoteAction.class)
public abstract class CreditNoteAction_ extends org.trescal.cwms.core.system.entity.baseaction.BaseAction_ {

	public static volatile SingularAttribute<CreditNoteAction, CreditNote> creditNote;
	public static volatile SingularAttribute<CreditNoteAction, CreditNoteStatus> activity;
	public static volatile SingularAttribute<CreditNoteAction, CreditNoteStatus> outcomeStatus;

	public static final String CREDIT_NOTE = "creditNote";
	public static final String ACTIVITY = "activity";
	public static final String OUTCOME_STATUS = "outcomeStatus";

}

