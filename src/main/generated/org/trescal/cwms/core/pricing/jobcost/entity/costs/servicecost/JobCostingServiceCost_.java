package org.trescal.cwms.core.pricing.jobcost.entity.costs.servicecost;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingexpenseitem.JobCostingExpenseItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobCostingServiceCost.class)
public abstract class JobCostingServiceCost_ extends org.trescal.cwms.core.pricing.entity.costs.base.ServiceCost_ {

	public static volatile SingularAttribute<JobCostingServiceCost, JobCostingExpenseItem> expenseItem;

	public static final String EXPENSE_ITEM = "expenseItem";

}

