package org.trescal.cwms.core.pricing.invoice.entity.costs.purchasecost;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InvoicePurchaseCost.class)
public abstract class InvoicePurchaseCost_ extends org.trescal.cwms.core.pricing.entity.costs.base.PurchaseCost_ {

	public static volatile SingularAttribute<InvoicePurchaseCost, NominalCode> nominal;
	public static volatile SingularAttribute<InvoicePurchaseCost, InvoiceItem> invoiceItem;

	public static final String NOMINAL = "nominal";
	public static final String INVOICE_ITEM = "invoiceItem";

}

