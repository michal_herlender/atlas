package org.trescal.cwms.core.pricing.jobcost.entity.addtionalcostcontact;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AdditionalCostContact.class)
public abstract class AdditionalCostContact_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<AdditionalCostContact, Contact> contact;
	public static volatile SingularAttribute<AdditionalCostContact, JobCosting> costing;
	public static volatile SingularAttribute<AdditionalCostContact, Integer> id;

	public static final String CONTACT = "contact";
	public static final String COSTING = "costing";
	public static final String ID = "id";

}

