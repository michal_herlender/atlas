package org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.adjcost.JobCostingAdjustmentCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingCalibrationCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.inspectioncost.JobCostingInspectionCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.purchasecost.JobCostingPurchaseCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.repcost.JobCostingRepairCost;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitemremark.JobCostingItemNote;
import org.trescal.cwms.core.pricing.jobcost.entity.pricingremark.JobCostingItemRemark;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobCostingItem.class)
public abstract class JobCostingItem_ extends org.trescal.cwms.core.pricing.entity.pricingitems.thirdpartypricingitem.ThirdPartyPricingItem_ {

	public static volatile SingularAttribute<JobCostingItem, JobCostingPurchaseCost> purchaseCost;
	public static volatile SingularAttribute<JobCostingItem, JobCostingAdjustmentCost> adjustmentCost;
	public static volatile SingularAttribute<JobCostingItem, JobCostingCalibrationCost> calibrationCost;
	public static volatile SetAttribute<JobCostingItem, JobCostingItemNote> notes;
	public static volatile SingularAttribute<JobCostingItem, JobCosting> jobCosting;
	public static volatile SingularAttribute<JobCostingItem, JobItem> jobItem;
	public static volatile SingularAttribute<JobCostingItem, Integer> id;
	public static volatile SingularAttribute<JobCostingItem, Boolean> calibrated;
	public static volatile SingularAttribute<JobCostingItem, JobCostingRepairCost> repairCost;
	public static volatile SingularAttribute<JobCostingItem, JobCostingInspectionCost> inspectionCost;
	public static volatile SetAttribute<JobCostingItem, JobCostingItemRemark> remarks;

	public static final String PURCHASE_COST = "purchaseCost";
	public static final String ADJUSTMENT_COST = "adjustmentCost";
	public static final String CALIBRATION_COST = "calibrationCost";
	public static final String NOTES = "notes";
	public static final String JOB_COSTING = "jobCosting";
	public static final String JOB_ITEM = "jobItem";
	public static final String ID = "id";
	public static final String CALIBRATED = "calibrated";
	public static final String REPAIR_COST = "repairCost";
	public static final String INSPECTION_COST = "inspectionCost";
	public static final String REMARKS = "remarks";

}

