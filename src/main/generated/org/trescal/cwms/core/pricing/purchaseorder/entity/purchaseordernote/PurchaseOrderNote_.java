package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseordernote;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PurchaseOrderNote.class)
public abstract class PurchaseOrderNote_ extends org.trescal.cwms.core.system.entity.note.Note_ {

	public static volatile SingularAttribute<PurchaseOrderNote, PurchaseOrder> order;

	public static final String ORDER = "order";

}

