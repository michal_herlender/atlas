package org.trescal.cwms.core.pricing.jobcost.entity.costs.adjcost;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobCostingAdjustmentCost.class)
public abstract class JobCostingAdjustmentCost_ extends org.trescal.cwms.core.pricing.entity.costs.thirdparty.TPSupportedAdjCost_ {

	public static volatile SingularAttribute<JobCostingAdjustmentCost, JobCostingLinkedAdjustmentCost> linkedCost;
	public static volatile SingularAttribute<JobCostingAdjustmentCost, CostSource> costSrc;
	public static volatile SingularAttribute<JobCostingAdjustmentCost, JobCostingItem> jobCostingItem;

	public static final String LINKED_COST = "linkedCost";
	public static final String COST_SRC = "costSrc";
	public static final String JOB_COSTING_ITEM = "jobCostingItem";

}

