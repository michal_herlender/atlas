package org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PricingItem.class)
public abstract class PricingItem_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<PricingItem, Integer> quantity;
	public static volatile SingularAttribute<PricingItem, BigDecimal> finalCost;
	public static volatile SingularAttribute<PricingItem, BigDecimal> generalDiscountValue;
	public static volatile SingularAttribute<PricingItem, Boolean> partOfBaseUnit;
	public static volatile SingularAttribute<PricingItem, Integer> itemno;
	public static volatile SingularAttribute<PricingItem, BigDecimal> totalCost;
	public static volatile SingularAttribute<PricingItem, BigDecimal> generalDiscountRate;

	public static final String QUANTITY = "quantity";
	public static final String FINAL_COST = "finalCost";
	public static final String GENERAL_DISCOUNT_VALUE = "generalDiscountValue";
	public static final String PART_OF_BASE_UNIT = "partOfBaseUnit";
	public static final String ITEMNO = "itemno";
	public static final String TOTAL_COST = "totalCost";
	public static final String GENERAL_DISCOUNT_RATE = "generalDiscountRate";

}

