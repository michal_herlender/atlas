package org.trescal.cwms.core.pricing.jobcost.entity.costs.adjcost;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.adjcost.ContractReviewAdjustmentCost;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobCostingLinkedAdjustmentCost.class)
public abstract class JobCostingLinkedAdjustmentCost_ extends org.trescal.cwms.core.pricing.jobcost.entity.costs.JobCostingLinkedCost_ {

	public static volatile SingularAttribute<JobCostingLinkedAdjustmentCost, JobCostingAdjustmentCost> jobCostingAdjustmentCost;
	public static volatile SingularAttribute<JobCostingLinkedAdjustmentCost, JobCostingAdjustmentCost> adjustmentCost;
	public static volatile SingularAttribute<JobCostingLinkedAdjustmentCost, ContractReviewAdjustmentCost> contractReviewAdjustmentCost;

	public static final String JOB_COSTING_ADJUSTMENT_COST = "jobCostingAdjustmentCost";
	public static final String ADJUSTMENT_COST = "adjustmentCost";
	public static final String CONTRACT_REVIEW_ADJUSTMENT_COST = "contractReviewAdjustmentCost";

}

