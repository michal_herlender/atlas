package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderaction;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderstatus.PurchaseOrderStatus;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PurchaseOrderAction.class)
public abstract class PurchaseOrderAction_ extends org.trescal.cwms.core.system.entity.baseaction.BaseAction_ {

	public static volatile SingularAttribute<PurchaseOrderAction, PurchaseOrderStatus> activity;
	public static volatile SingularAttribute<PurchaseOrderAction, PurchaseOrderStatus> outcomeStatus;
	public static volatile SingularAttribute<PurchaseOrderAction, PurchaseOrder> order;

	public static final String ACTIVITY = "activity";
	public static final String OUTCOME_STATUS = "outcomeStatus";
	public static final String ORDER = "order";

}

