package org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoicepo.InvoicePO;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InvoicePOItem.class)
public abstract class InvoicePOItem_ {

	public static volatile SingularAttribute<InvoicePOItem, InvoiceItem> invItem;
	public static volatile SingularAttribute<InvoicePOItem, Integer> id;
	public static volatile SingularAttribute<InvoicePOItem, InvoicePO> invPO;

	public static final String INV_ITEM = "invItem";
	public static final String ID = "id";
	public static final String INV_PO = "invPO";

}

