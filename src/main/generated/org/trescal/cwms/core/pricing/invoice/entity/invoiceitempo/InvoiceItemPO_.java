package org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.po.PO;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoicepo.InvoicePO;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InvoiceItemPO.class)
public abstract class InvoiceItemPO_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<InvoiceItemPO, BPO> bpo;
	public static volatile SingularAttribute<InvoiceItemPO, InvoiceItem> invItem;
	public static volatile SingularAttribute<InvoiceItemPO, PO> jobPO;
	public static volatile SingularAttribute<InvoiceItemPO, Integer> id;
	public static volatile SingularAttribute<InvoiceItemPO, InvoicePO> invPO;

	public static final String BPO = "bpo";
	public static final String INV_ITEM = "invItem";
	public static final String JOB_PO = "jobPO";
	public static final String ID = "id";
	public static final String INV_PO = "invPO";

}

