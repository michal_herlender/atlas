package org.trescal.cwms.core.pricing.invoice.entity.costs.servicecost;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InvoiceServiceCost.class)
public abstract class InvoiceServiceCost_ extends org.trescal.cwms.core.pricing.entity.costs.base.ServiceCost_ {

	public static volatile SingularAttribute<InvoiceServiceCost, NominalCode> nominal;
	public static volatile SingularAttribute<InvoiceServiceCost, InvoiceItem> invoiceItem;

	public static final String NOMINAL = "nominal";
	public static final String INVOICE_ITEM = "invoiceItem";

}

