package org.trescal.cwms.core.pricing.jobcost.entity.costs.purchasecost;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.purchasecost.ContractReviewPurchaseCost;
import org.trescal.cwms.core.quotation.entity.costs.purchasecost.QuotationPurchaseCost;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobCostingLinkedPurchaseCost.class)
public abstract class JobCostingLinkedPurchaseCost_ extends org.trescal.cwms.core.pricing.jobcost.entity.costs.JobCostingLinkedCost_ {

	public static volatile SingularAttribute<JobCostingLinkedPurchaseCost, ContractReviewPurchaseCost> contractReviewPurchaseCost;
	public static volatile SingularAttribute<JobCostingLinkedPurchaseCost, JobCostingPurchaseCost> purchaseCost;
	public static volatile SingularAttribute<JobCostingLinkedPurchaseCost, QuotationPurchaseCost> quotationPurchaseCost;
	public static volatile SingularAttribute<JobCostingLinkedPurchaseCost, JobCostingPurchaseCost> jobCostingPurchaseCost;

	public static final String CONTRACT_REVIEW_PURCHASE_COST = "contractReviewPurchaseCost";
	public static final String PURCHASE_COST = "purchaseCost";
	public static final String QUOTATION_PURCHASE_COST = "quotationPurchaseCost";
	public static final String JOB_COSTING_PURCHASE_COST = "jobCostingPurchaseCost";

}

