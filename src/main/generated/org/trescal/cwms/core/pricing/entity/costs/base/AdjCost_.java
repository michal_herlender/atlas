package org.trescal.cwms.core.pricing.entity.costs.base;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AdjCost.class)
public abstract class AdjCost_ extends org.trescal.cwms.core.pricing.entity.costs.base.Cost_ {

	public static volatile SingularAttribute<AdjCost, BigDecimal> fixedPrice;

	public static final String FIXED_PRICE = "fixedPrice";

}

