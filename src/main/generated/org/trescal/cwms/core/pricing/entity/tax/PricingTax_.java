package org.trescal.cwms.core.pricing.entity.tax;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.vatrate.VatRate;
import org.trescal.cwms.core.pricing.entity.pricings.pricing.Pricing;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PricingTax.class)
public abstract class PricingTax_ {

	public static volatile SingularAttribute<PricingTax, BigDecimal> amount;
	public static volatile SingularAttribute<PricingTax, BigDecimal> rate;
	public static volatile SingularAttribute<PricingTax, VatRate> vatRate;
	public static volatile SingularAttribute<PricingTax, String> description;
	public static volatile SingularAttribute<PricingTax, BigDecimal> tax;
	public static volatile SingularAttribute<PricingTax, Integer> id;
	public static volatile SingularAttribute<PricingTax, Pricing> pricing;

	public static final String AMOUNT = "amount";
	public static final String RATE = "rate";
	public static final String VAT_RATE = "vatRate";
	public static final String DESCRIPTION = "description";
	public static final String TAX = "tax";
	public static final String ID = "id";
	public static final String PRICING = "pricing";

}

