package org.trescal.cwms.core.pricing.invoice.entity.costs.adjcost;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InvoiceAdjustmentCost.class)
public abstract class InvoiceAdjustmentCost_ extends org.trescal.cwms.core.pricing.entity.costs.base.AdjCost_ {

	public static volatile SingularAttribute<InvoiceAdjustmentCost, NominalCode> nominal;
	public static volatile SingularAttribute<InvoiceAdjustmentCost, InvoiceItem> invoiceItem;

	public static final String NOMINAL = "nominal";
	public static final String INVOICE_ITEM = "invoiceItem";

}

