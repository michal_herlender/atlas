package org.trescal.cwms.core.pricing.invoice.entity.invoiceitem;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.invoice.entity.costs.adjcost.InvoiceAdjustmentCost;
import org.trescal.cwms.core.pricing.invoice.entity.costs.calcost.InvoiceCalibrationCost;
import org.trescal.cwms.core.pricing.invoice.entity.costs.purchasecost.InvoicePurchaseCost;
import org.trescal.cwms.core.pricing.invoice.entity.costs.repcost.InvoiceRepairCost;
import org.trescal.cwms.core.pricing.invoice.entity.costs.servicecost.InvoiceServiceCost;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoiceItemPO;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoicePOItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InvoiceItem.class)
public abstract class InvoiceItem_ extends org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem.TaxablePricingItem_ {

	public static volatile SingularAttribute<InvoiceItem, JobExpenseItem> expenseItem;
	public static volatile SingularAttribute<InvoiceItem, InvoicePurchaseCost> purchaseCost;
	public static volatile SingularAttribute<InvoiceItem, InvoiceAdjustmentCost> adjustmentCost;
	public static volatile SingularAttribute<InvoiceItem, InvoiceServiceCost> serviceCost;
	public static volatile SingularAttribute<InvoiceItem, String> description;
	public static volatile SingularAttribute<InvoiceItem, String> serialno;
	public static volatile SingularAttribute<InvoiceItem, InvoiceCalibrationCost> calibrationCost;
	public static volatile SingularAttribute<InvoiceItem, NominalCode> nominal;
	public static volatile SingularAttribute<InvoiceItem, JobItem> jobItem;
	public static volatile SingularAttribute<InvoiceItem, Subdiv> businessSubdiv;
	public static volatile SingularAttribute<InvoiceItem, Integer> id;
	public static volatile SingularAttribute<InvoiceItem, Invoice> invoice;
	public static volatile SetAttribute<InvoiceItem, InvoiceItemPO> invItemPOs;
	public static volatile SetAttribute<InvoiceItem, InvoicePOItem> invPOsItem;
	public static volatile SingularAttribute<InvoiceItem, InvoiceRepairCost> repairCost;
	public static volatile SingularAttribute<InvoiceItem, Boolean> breakUpCosts;

	public static final String EXPENSE_ITEM = "expenseItem";
	public static final String PURCHASE_COST = "purchaseCost";
	public static final String ADJUSTMENT_COST = "adjustmentCost";
	public static final String SERVICE_COST = "serviceCost";
	public static final String DESCRIPTION = "description";
	public static final String SERIALNO = "serialno";
	public static final String CALIBRATION_COST = "calibrationCost";
	public static final String NOMINAL = "nominal";
	public static final String JOB_ITEM = "jobItem";
	public static final String BUSINESS_SUBDIV = "businessSubdiv";
	public static final String ID = "id";
	public static final String INVOICE = "invoice";
	public static final String INV_ITEM_POS = "invItemPOs";
	public static final String INV_POS_ITEM = "invPOsItem";
	public static final String REPAIR_COST = "repairCost";
	public static final String BREAK_UP_COSTS = "breakUpCosts";

}

