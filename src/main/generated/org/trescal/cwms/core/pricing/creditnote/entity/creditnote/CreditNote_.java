package org.trescal.cwms.core.pricing.creditnote.entity.creditnote;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.account.entity.AccountStatus;
import org.trescal.cwms.core.company.entity.vatrate.VatRate;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnoteaction.CreditNoteAction;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnoteitem.CreditNoteItem;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnotenote.CreditNoteNote;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnotestatus.CreditNoteStatus;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CreditNote.class)
public abstract class CreditNote_ extends org.trescal.cwms.core.pricing.entity.pricings.pricing.TaxablePricing_ {

	public static volatile SetAttribute<CreditNote, CreditNoteNote> notes;
	public static volatile SingularAttribute<CreditNote, String> creditNoteNo;
	public static volatile SingularAttribute<CreditNote, VatRate> vatRateEntity;
	public static volatile SingularAttribute<CreditNote, AccountStatus> accountsStatus;
	public static volatile SingularAttribute<CreditNote, Invoice> invoice;
	public static volatile SetAttribute<CreditNote, CreditNoteAction> actions;
	public static volatile SetAttribute<CreditNote, CreditNoteItem> items;
	public static volatile SingularAttribute<CreditNote, CreditNoteStatus> status;

	public static final String NOTES = "notes";
	public static final String CREDIT_NOTE_NO = "creditNoteNo";
	public static final String VAT_RATE_ENTITY = "vatRateEntity";
	public static final String ACCOUNTS_STATUS = "accountsStatus";
	public static final String INVOICE = "invoice";
	public static final String ACTIONS = "actions";
	public static final String ITEMS = "items";
	public static final String STATUS = "status";

}

