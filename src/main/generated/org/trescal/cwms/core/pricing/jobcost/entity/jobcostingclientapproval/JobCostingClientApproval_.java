package org.trescal.cwms.core.pricing.jobcost.entity.jobcostingclientapproval;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobCostingClientApproval.class)
public abstract class JobCostingClientApproval_ {

	public static volatile SingularAttribute<JobCostingClientApproval, Date> clientApprovalOn;
	public static volatile SingularAttribute<JobCostingClientApproval, JobCosting> jobCosting;
	public static volatile SingularAttribute<JobCostingClientApproval, Integer> id;
	public static volatile SingularAttribute<JobCostingClientApproval, Boolean> clientApproval;
	public static volatile SingularAttribute<JobCostingClientApproval, String> clientApprovalComment;

	public static final String CLIENT_APPROVAL_ON = "clientApprovalOn";
	public static final String JOB_COSTING = "jobCosting";
	public static final String ID = "id";
	public static final String CLIENT_APPROVAL = "clientApproval";
	public static final String CLIENT_APPROVAL_COMMENT = "clientApprovalComment";

}

