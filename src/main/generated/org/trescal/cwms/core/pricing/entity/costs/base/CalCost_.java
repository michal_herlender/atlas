package org.trescal.cwms.core.pricing.entity.costs.base;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CalCost.class)
public abstract class CalCost_ extends org.trescal.cwms.core.pricing.entity.costs.base.Cost_ {

	public static volatile SingularAttribute<CalCost, CostSource> costSrc;

	public static final String COST_SRC = "costSrc";

}

