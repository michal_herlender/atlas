package org.trescal.cwms.core.pricing.creditnote.entity.creditnotenote;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CreditNoteNote.class)
public abstract class CreditNoteNote_ extends org.trescal.cwms.core.system.entity.note.Note_ {

	public static volatile SingularAttribute<CreditNoteNote, CreditNote> creditNote;

	public static final String CREDIT_NOTE = "creditNote";

}

