package org.trescal.cwms.core.pricing.entity.pricingremark;

import java.util.Locale;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.company.Company;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(DefaultPricingRemark.class)
public abstract class DefaultPricingRemark_ extends org.trescal.cwms.core.pricing.entity.pricingremark.PricingRemark_ {

	public static volatile SingularAttribute<DefaultPricingRemark, Company> organisation;
	public static volatile SingularAttribute<DefaultPricingRemark, Locale> locale;

	public static final String ORGANISATION = "organisation";
	public static final String LOCALE = "locale";

}

