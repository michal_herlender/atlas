package org.trescal.cwms.core.pricing.supplierinvoice.entity;

import java.math.BigDecimal;
import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.paymentmode.PaymentMode;
import org.trescal.cwms.core.company.entity.paymentterms.PaymentTerm;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SupplierInvoice.class)
public abstract class SupplierInvoice_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<SupplierInvoice, PaymentMode> paymentMode;
	public static volatile SingularAttribute<SupplierInvoice, String> invoiceNumber;
	public static volatile SingularAttribute<SupplierInvoice, PurchaseOrder> purchaseOrder;
	public static volatile SingularAttribute<SupplierInvoice, SupportedCurrency> currency;
	public static volatile SingularAttribute<SupplierInvoice, Integer> id;
	public static volatile SingularAttribute<SupplierInvoice, BigDecimal> totalWithTax;
	public static volatile SingularAttribute<SupplierInvoice, LocalDate> invoiceDate;
	public static volatile SingularAttribute<SupplierInvoice, LocalDate> paymentDate;
	public static volatile SingularAttribute<SupplierInvoice, PaymentTerm> paymentTerm;
	public static volatile SingularAttribute<SupplierInvoice, BigDecimal> totalWithoutTax;

	public static final String PAYMENT_MODE = "paymentMode";
	public static final String INVOICE_NUMBER = "invoiceNumber";
	public static final String PURCHASE_ORDER = "purchaseOrder";
	public static final String CURRENCY = "currency";
	public static final String ID = "id";
	public static final String TOTAL_WITH_TAX = "totalWithTax";
	public static final String INVOICE_DATE = "invoiceDate";
	public static final String PAYMENT_DATE = "paymentDate";
	public static final String PAYMENT_TERM = "paymentTerm";
	public static final String TOTAL_WITHOUT_TAX = "totalWithoutTax";

}

