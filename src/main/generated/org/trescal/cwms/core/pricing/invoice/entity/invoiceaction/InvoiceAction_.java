package org.trescal.cwms.core.pricing.invoice.entity.invoiceaction;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoicestatus.InvoiceStatus;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InvoiceAction.class)
public abstract class InvoiceAction_ extends org.trescal.cwms.core.system.entity.baseaction.BaseAction_ {

	public static volatile SingularAttribute<InvoiceAction, InvoiceStatus> activity;
	public static volatile SingularAttribute<InvoiceAction, InvoiceStatus> outcomeStatus;
	public static volatile SingularAttribute<InvoiceAction, Invoice> invoice;

	public static final String ACTIVITY = "activity";
	public static final String OUTCOME_STATUS = "outcomeStatus";
	public static final String INVOICE = "invoice";

}

