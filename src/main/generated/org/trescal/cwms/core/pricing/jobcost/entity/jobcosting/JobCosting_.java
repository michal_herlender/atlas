package org.trescal.cwms.core.pricing.jobcost.entity.jobcosting;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.pricing.PricingIssueMethod;
import org.trescal.cwms.core.pricing.PricingType;
import org.trescal.cwms.core.pricing.jobcost.entity.addtionalcostcontact.AdditionalCostContact;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingclientapproval.JobCostingClientApproval;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingexpenseitem.JobCostingExpenseItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingnote.JobCostingNote;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingstatus.JobCostingStatus;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingviewed.JobCostingViewed;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobCosting.class)
public abstract class JobCosting_ extends org.trescal.cwms.core.pricing.entity.pricings.pricing.Pricing_ {

	public static volatile SingularAttribute<JobCosting, JobCostingViewed> jcViewed;
	public static volatile SetAttribute<JobCosting, JobCostingClientApproval> clientApprovals;
	public static volatile SingularAttribute<JobCosting, Integer> ver;
	public static volatile SetAttribute<JobCosting, JobCostingNote> notes;
	public static volatile ListAttribute<JobCosting, JobCostingExpenseItem> expenseItems;
	public static volatile SetAttribute<JobCosting, AdditionalCostContact> additionalContacts;
	public static volatile SingularAttribute<JobCosting, PricingIssueMethod> issueMethod;
	public static volatile SingularAttribute<JobCosting, String> siteCostingNote;
	public static volatile SingularAttribute<JobCosting, PricingType> type;
	public static volatile SingularAttribute<JobCosting, Integer> costingTypeId;
	public static volatile SingularAttribute<JobCosting, Date> lastUpdateOn;
	public static volatile SingularAttribute<JobCosting, Job> job;
	public static volatile SetAttribute<JobCosting, JobCostingItem> items;
	public static volatile SingularAttribute<JobCosting, Contact> lastUpdateBy;
	public static volatile SingularAttribute<JobCosting, JobCostingStatus> status;

	public static final String JC_VIEWED = "jcViewed";
	public static final String CLIENT_APPROVALS = "clientApprovals";
	public static final String VER = "ver";
	public static final String NOTES = "notes";
	public static final String EXPENSE_ITEMS = "expenseItems";
	public static final String ADDITIONAL_CONTACTS = "additionalContacts";
	public static final String ISSUE_METHOD = "issueMethod";
	public static final String SITE_COSTING_NOTE = "siteCostingNote";
	public static final String TYPE = "type";
	public static final String COSTING_TYPE_ID = "costingTypeId";
	public static final String LAST_UPDATE_ON = "lastUpdateOn";
	public static final String JOB = "job";
	public static final String ITEMS = "items";
	public static final String LAST_UPDATE_BY = "lastUpdateBy";
	public static final String STATUS = "status";

}

