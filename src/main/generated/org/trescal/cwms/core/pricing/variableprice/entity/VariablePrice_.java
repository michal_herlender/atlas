package org.trescal.cwms.core.pricing.variableprice.entity;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VariablePrice.class)
public abstract class VariablePrice_ {

	public static volatile SingularAttribute<VariablePrice, BigDecimal> unitPrice;
	public static volatile SingularAttribute<VariablePrice, VariablePriceUnit> unit;

	public static final String UNIT_PRICE = "unitPrice";
	public static final String UNIT = "unit";

}

