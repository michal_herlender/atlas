package org.trescal.cwms.core.pricing.jobcost.entity.jobcostingstatus;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobCostingStatus.class)
public abstract class JobCostingStatus_ extends org.trescal.cwms.core.system.entity.status.Status_ {

	public static volatile ListAttribute<JobCostingStatus, JobCosting> jobCostings;

	public static final String JOB_COSTINGS = "jobCostings";

}

