package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitemnote;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PurchaseOrderItemNote.class)
public abstract class PurchaseOrderItemNote_ extends org.trescal.cwms.core.system.entity.note.Note_ {

	public static volatile SingularAttribute<PurchaseOrderItemNote, PurchaseOrderItem> item;

	public static final String ITEM = "item";

}

