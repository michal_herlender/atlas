package org.trescal.cwms.core.pricing.entity.pricingremark;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PricingRemark.class)
public abstract class PricingRemark_ {

	public static volatile SingularAttribute<PricingRemark, CostType> costType;
	public static volatile SingularAttribute<PricingRemark, String> remark;
	public static volatile SingularAttribute<PricingRemark, Integer> id;
	public static volatile SingularAttribute<PricingRemark, PricingRemarkType> remarkType;

	public static final String COST_TYPE = "costType";
	public static final String REMARK = "remark";
	public static final String ID = "id";
	public static final String REMARK_TYPE = "remarkType";

}

