package org.trescal.cwms.core.pricing.entity.costs.thirdparty;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.entity.enums.ThirdCostMarkupSource;
import org.trescal.cwms.core.pricing.entity.enums.ThirdCostSource;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.repcost.TPQuotationRepairCost;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TPSupportedRepCost.class)
public abstract class TPSupportedRepCost_ extends org.trescal.cwms.core.pricing.entity.costs.base.RepCost_ {

	public static volatile SingularAttribute<TPSupportedRepCost, BigDecimal> tpCarriageIn;
	public static volatile SingularAttribute<TPSupportedRepCost, BigDecimal> tpCarriageOutMarkupValue;
	public static volatile SingularAttribute<TPSupportedRepCost, ThirdCostMarkupSource> tpCarriageMarkupSrc;
	public static volatile SingularAttribute<TPSupportedRepCost, BigDecimal> thirdMarkupValue;
	public static volatile SingularAttribute<TPSupportedRepCost, BigDecimal> tpCarriageOut;
	public static volatile SingularAttribute<TPSupportedRepCost, BigDecimal> thirdCostTotal;
	public static volatile SingularAttribute<TPSupportedRepCost, ThirdCostMarkupSource> thirdMarkupSrc;
	public static volatile SingularAttribute<TPSupportedRepCost, BigDecimal> houseCost;
	public static volatile SingularAttribute<TPSupportedRepCost, BigDecimal> thirdManualPrice;
	public static volatile SingularAttribute<TPSupportedRepCost, BigDecimal> tpCarriageInMarkupValue;
	public static volatile SingularAttribute<TPSupportedRepCost, BigDecimal> tpCarriageTotal;
	public static volatile SingularAttribute<TPSupportedRepCost, TPQuotationRepairCost> linkedCostSrc;
	public static volatile SingularAttribute<TPSupportedRepCost, ThirdCostSource> thirdCostSrc;
	public static volatile SingularAttribute<TPSupportedRepCost, BigDecimal> thirdMarkupRate;
	public static volatile SingularAttribute<TPSupportedRepCost, BigDecimal> tpCarriageMarkupRate;

	public static final String TP_CARRIAGE_IN = "tpCarriageIn";
	public static final String TP_CARRIAGE_OUT_MARKUP_VALUE = "tpCarriageOutMarkupValue";
	public static final String TP_CARRIAGE_MARKUP_SRC = "tpCarriageMarkupSrc";
	public static final String THIRD_MARKUP_VALUE = "thirdMarkupValue";
	public static final String TP_CARRIAGE_OUT = "tpCarriageOut";
	public static final String THIRD_COST_TOTAL = "thirdCostTotal";
	public static final String THIRD_MARKUP_SRC = "thirdMarkupSrc";
	public static final String HOUSE_COST = "houseCost";
	public static final String THIRD_MANUAL_PRICE = "thirdManualPrice";
	public static final String TP_CARRIAGE_IN_MARKUP_VALUE = "tpCarriageInMarkupValue";
	public static final String TP_CARRIAGE_TOTAL = "tpCarriageTotal";
	public static final String LINKED_COST_SRC = "linkedCostSrc";
	public static final String THIRD_COST_SRC = "thirdCostSrc";
	public static final String THIRD_MARKUP_RATE = "thirdMarkupRate";
	public static final String TP_CARRIAGE_MARKUP_RATE = "tpCarriageMarkupRate";

}

