package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.account.entity.AccountStatus;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.poorigin.POOrigin;
import org.trescal.cwms.core.pricing.purchaseorder.entity.enums.PurchaseOrderTaxableOption;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderaction.PurchaseOrderAction;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseordernote.PurchaseOrderNote;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderstatus.PurchaseOrderStatus;
import org.trescal.cwms.core.pricing.supplierinvoice.entity.SupplierInvoice;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PurchaseOrder.class)
public abstract class PurchaseOrder_ extends org.trescal.cwms.core.pricing.entity.pricings.pricing.Pricing_ {

	public static volatile SingularAttribute<PurchaseOrder, Address> address;
	public static volatile SetAttribute<PurchaseOrder, PurchaseOrderNote> notes;
	public static volatile SingularAttribute<PurchaseOrder, PurchaseOrderTaxableOption> taxable;
	public static volatile SingularAttribute<PurchaseOrder, Contact> businessContact;
	public static volatile SingularAttribute<PurchaseOrder, AccountStatus> accountsStatus;
	public static volatile SingularAttribute<PurchaseOrder, String> pono;
	public static volatile SetAttribute<PurchaseOrder, SupplierInvoice> supplierInvoices;
	public static volatile SingularAttribute<PurchaseOrder, String> jobno;
	public static volatile SingularAttribute<PurchaseOrder, POOrigin> poOrigin;
	public static volatile SingularAttribute<PurchaseOrder, Address> returnToAddress;
	public static volatile SingularAttribute<PurchaseOrder, Job> job;
	public static volatile SetAttribute<PurchaseOrder, PurchaseOrderAction> actions;
	public static volatile SetAttribute<PurchaseOrder, PurchaseOrderItem> items;
	public static volatile SingularAttribute<PurchaseOrder, PurchaseOrderStatus> status;

	public static final String ADDRESS = "address";
	public static final String NOTES = "notes";
	public static final String TAXABLE = "taxable";
	public static final String BUSINESS_CONTACT = "businessContact";
	public static final String ACCOUNTS_STATUS = "accountsStatus";
	public static final String PONO = "pono";
	public static final String SUPPLIER_INVOICES = "supplierInvoices";
	public static final String JOBNO = "jobno";
	public static final String PO_ORIGIN = "poOrigin";
	public static final String RETURN_TO_ADDRESS = "returnToAddress";
	public static final String JOB = "job";
	public static final String ACTIONS = "actions";
	public static final String ITEMS = "items";
	public static final String STATUS = "status";

}

