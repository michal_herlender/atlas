package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitemprogressaction;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItemReceiptStatus;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PurchaseOrderItemProgressAction.class)
public abstract class PurchaseOrderItemProgressAction_ {

	public static volatile SingularAttribute<PurchaseOrderItemProgressAction, Date> date;
	public static volatile SingularAttribute<PurchaseOrderItemProgressAction, PurchaseOrderItem> item;
	public static volatile SingularAttribute<PurchaseOrderItemProgressAction, Boolean> accountsApproved;
	public static volatile SingularAttribute<PurchaseOrderItemProgressAction, Contact> contact;
	public static volatile SingularAttribute<PurchaseOrderItemProgressAction, Boolean> goodsApproved;
	public static volatile SingularAttribute<PurchaseOrderItemProgressAction, Boolean> cancelled;
	public static volatile SingularAttribute<PurchaseOrderItemProgressAction, String> description;
	public static volatile SingularAttribute<PurchaseOrderItemProgressAction, Integer> id;
	public static volatile SingularAttribute<PurchaseOrderItemProgressAction, PurchaseOrderItemReceiptStatus> receiptStatus;

	public static final String DATE = "date";
	public static final String ITEM = "item";
	public static final String ACCOUNTS_APPROVED = "accountsApproved";
	public static final String CONTACT = "contact";
	public static final String GOODS_APPROVED = "goodsApproved";
	public static final String CANCELLED = "cancelled";
	public static final String DESCRIPTION = "description";
	public static final String ID = "id";
	public static final String RECEIPT_STATUS = "receiptStatus";

}

