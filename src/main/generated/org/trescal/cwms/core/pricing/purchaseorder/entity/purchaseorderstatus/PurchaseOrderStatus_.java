package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderstatus;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.system.entity.basestatus.ActionType;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PurchaseOrderStatus.class)
public abstract class PurchaseOrderStatus_ extends org.trescal.cwms.core.system.entity.basestatus.BaseStatus_ {

	public static volatile SingularAttribute<PurchaseOrderStatus, PurchaseOrderStatus> next;
	public static volatile SingularAttribute<PurchaseOrderStatus, Boolean> requiresAttention;
	public static volatile SingularAttribute<PurchaseOrderStatus, Boolean> onComplete;
	public static volatile SetAttribute<PurchaseOrderStatus, Translation> nametranslations;
	public static volatile SetAttribute<PurchaseOrderStatus, Translation> descriptiontranslations;
	public static volatile SingularAttribute<PurchaseOrderStatus, ActionType> type;
	public static volatile SingularAttribute<PurchaseOrderStatus, Boolean> approved;
	public static volatile SingularAttribute<PurchaseOrderStatus, Boolean> onCancel;
	public static volatile SingularAttribute<PurchaseOrderStatus, DepartmentType> deptMembershipRequired;
	public static volatile SingularAttribute<PurchaseOrderStatus, Boolean> onIssue;
	public static volatile ListAttribute<PurchaseOrderStatus, PurchaseOrder> orders;
	public static volatile SingularAttribute<PurchaseOrderStatus, Boolean> issued;
	public static volatile SingularAttribute<PurchaseOrderStatus, Boolean> onInsert;
	public static volatile SingularAttribute<PurchaseOrderStatus, Integer> statusorder;

	public static final String NEXT = "next";
	public static final String REQUIRES_ATTENTION = "requiresAttention";
	public static final String ON_COMPLETE = "onComplete";
	public static final String NAMETRANSLATIONS = "nametranslations";
	public static final String DESCRIPTIONTRANSLATIONS = "descriptiontranslations";
	public static final String TYPE = "type";
	public static final String APPROVED = "approved";
	public static final String ON_CANCEL = "onCancel";
	public static final String DEPT_MEMBERSHIP_REQUIRED = "deptMembershipRequired";
	public static final String ON_ISSUE = "onIssue";
	public static final String ORDERS = "orders";
	public static final String ISSUED = "issued";
	public static final String ON_INSERT = "onInsert";
	public static final String STATUSORDER = "statusorder";

}

