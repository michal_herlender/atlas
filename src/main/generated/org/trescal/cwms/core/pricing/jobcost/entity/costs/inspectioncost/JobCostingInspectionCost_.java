package org.trescal.cwms.core.pricing.jobcost.entity.costs.inspectioncost;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobCostingInspectionCost.class)
public abstract class JobCostingInspectionCost_ extends org.trescal.cwms.core.pricing.entity.costs.base.InspectionCost_ {

	public static volatile SingularAttribute<JobCostingInspectionCost, JobCostingItem> jobCostingItem;

	public static final String JOB_COSTING_ITEM = "jobCostingItem";

}

