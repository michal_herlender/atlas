package org.trescal.cwms.core.pricing.invoice.entity.nominalinvoicelookup;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(NominalInvoiceLookup.class)
public abstract class NominalInvoiceLookup_ {

	public static volatile SingularAttribute<NominalInvoiceLookup, NominalCode> nominalCode;
	public static volatile SingularAttribute<NominalInvoiceLookup, Integer> id;
	public static volatile SingularAttribute<NominalInvoiceLookup, String> nominalType;

	public static final String NOMINAL_CODE = "nominalCode";
	public static final String ID = "id";
	public static final String NOMINAL_TYPE = "nominalType";

}

