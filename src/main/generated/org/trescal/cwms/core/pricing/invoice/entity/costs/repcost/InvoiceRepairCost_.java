package org.trescal.cwms.core.pricing.invoice.entity.costs.repcost;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InvoiceRepairCost.class)
public abstract class InvoiceRepairCost_ extends org.trescal.cwms.core.pricing.entity.costs.base.RepCost_ {

	public static volatile SingularAttribute<InvoiceRepairCost, NominalCode> nominal;
	public static volatile SingularAttribute<InvoiceRepairCost, InvoiceItem> invoiceItem;

	public static final String NOMINAL = "nominal";
	public static final String INVOICE_ITEM = "invoiceItem";

}

