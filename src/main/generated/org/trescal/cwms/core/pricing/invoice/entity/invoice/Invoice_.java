package org.trescal.cwms.core.pricing.invoice.entity.invoice;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.account.entity.AccountStatus;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.paymentmode.PaymentMode;
import org.trescal.cwms.core.company.entity.paymentterms.PaymentTerm;
import org.trescal.cwms.core.company.entity.vatrate.VatRate;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.JobItemNotInvoiced;
import org.trescal.cwms.core.pricing.PricingType;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceaction.InvoiceAction;
import org.trescal.cwms.core.pricing.invoice.entity.invoicedelivery.InvoiceDelivery;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink.InvoiceJobLink;
import org.trescal.cwms.core.pricing.invoice.entity.invoicenote.InvoiceNote;
import org.trescal.cwms.core.pricing.invoice.entity.invoicepo.InvoicePO;
import org.trescal.cwms.core.pricing.invoice.entity.invoicestatus.InvoiceStatus;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.InvoiceType;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Invoice.class)
public abstract class Invoice_ extends org.trescal.cwms.core.pricing.entity.pricings.pricing.TaxablePricing_ {

	public static volatile SingularAttribute<Invoice, Company> comp;
	public static volatile SingularAttribute<Invoice, Address> address;
	public static volatile SetAttribute<Invoice, InvoiceNote> notes;
	public static volatile SingularAttribute<Invoice, PaymentMode> paymentMode;
	public static volatile SingularAttribute<Invoice, VatRate> vatRateEntity;
	public static volatile SingularAttribute<Invoice, Contact> businessContact;
	public static volatile ListAttribute<Invoice, JobItemNotInvoiced> periodicJobItemLinks;
	public static volatile SingularAttribute<Invoice, AccountStatus> accountsStatus;
	public static volatile SingularAttribute<Invoice, String> invno;
	public static volatile SingularAttribute<Invoice, String> siteCostingNote;
	public static volatile ListAttribute<Invoice, PurchaseOrderItem> internalPOItems;
	public static volatile SingularAttribute<Invoice, LocalDate> invoiceDate;
	public static volatile SingularAttribute<Invoice, InvoiceType> type;
	public static volatile SetAttribute<Invoice, InvoiceDelivery> deliveries;
	public static volatile SetAttribute<Invoice, CreditNote> creditNotes;
	public static volatile SingularAttribute<Invoice, Integer> financialPeriod;
	public static volatile SetAttribute<Invoice, InvoicePO> pos;
	public static volatile SingularAttribute<Invoice, LocalDate> duedate;
	public static volatile SetAttribute<Invoice, InvoiceJobLink> jobLinks;
	public static volatile SingularAttribute<Invoice, PaymentTerm> paymentTerm;
	public static volatile SetAttribute<Invoice, InvoiceAction> actions;
	public static volatile SetAttribute<Invoice, InvoiceItem> items;
	public static volatile SingularAttribute<Invoice, PricingType> pricingType;
	public static volatile SingularAttribute<Invoice, InvoiceStatus> status;

	public static final String COMP = "comp";
	public static final String ADDRESS = "address";
	public static final String NOTES = "notes";
	public static final String PAYMENT_MODE = "paymentMode";
	public static final String VAT_RATE_ENTITY = "vatRateEntity";
	public static final String BUSINESS_CONTACT = "businessContact";
	public static final String PERIODIC_JOB_ITEM_LINKS = "periodicJobItemLinks";
	public static final String ACCOUNTS_STATUS = "accountsStatus";
	public static final String INVNO = "invno";
	public static final String SITE_COSTING_NOTE = "siteCostingNote";
	public static final String INTERNAL_PO_ITEMS = "internalPOItems";
	public static final String INVOICE_DATE = "invoiceDate";
	public static final String TYPE = "type";
	public static final String DELIVERIES = "deliveries";
	public static final String CREDIT_NOTES = "creditNotes";
	public static final String FINANCIAL_PERIOD = "financialPeriod";
	public static final String POS = "pos";
	public static final String DUEDATE = "duedate";
	public static final String JOB_LINKS = "jobLinks";
	public static final String PAYMENT_TERM = "paymentTerm";
	public static final String ACTIONS = "actions";
	public static final String ITEMS = "items";
	public static final String PRICING_TYPE = "pricingType";
	public static final String STATUS = "status";

}

