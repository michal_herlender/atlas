package org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.ContractReviewCalibrationCost;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobCostingLinkedCalibrationCost.class)
public abstract class JobCostingLinkedCalibrationCost_ extends org.trescal.cwms.core.pricing.jobcost.entity.costs.JobCostingLinkedCost_ {

	public static volatile SingularAttribute<JobCostingLinkedCalibrationCost, JobCostingCalibrationCost> jobCostingCalibrationCost;
	public static volatile SingularAttribute<JobCostingLinkedCalibrationCost, JobCostingCalibrationCost> calibrationCost;
	public static volatile SingularAttribute<JobCostingLinkedCalibrationCost, QuotationCalibrationCost> quotationCalibrationCost;
	public static volatile SingularAttribute<JobCostingLinkedCalibrationCost, ContractReviewCalibrationCost> contractReviewCalibrationCost;

	public static final String JOB_COSTING_CALIBRATION_COST = "jobCostingCalibrationCost";
	public static final String CALIBRATION_COST = "calibrationCost";
	public static final String QUOTATION_CALIBRATION_COST = "quotationCalibrationCost";
	public static final String CONTRACT_REVIEW_CALIBRATION_COST = "contractReviewCalibrationCost";

}

