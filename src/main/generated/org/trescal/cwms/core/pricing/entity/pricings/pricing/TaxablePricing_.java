package org.trescal.cwms.core.pricing.entity.pricings.pricing;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.entity.tax.PricingTax;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TaxablePricing.class)
public abstract class TaxablePricing_ extends org.trescal.cwms.core.pricing.entity.pricings.pricing.Pricing_ {

	public static volatile ListAttribute<TaxablePricing, PricingTax> taxes;

	public static final String TAXES = "taxes";

}

