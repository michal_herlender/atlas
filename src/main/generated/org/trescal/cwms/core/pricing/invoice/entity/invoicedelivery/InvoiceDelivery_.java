package org.trescal.cwms.core.pricing.invoice.entity.invoicedelivery;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InvoiceDelivery.class)
public abstract class InvoiceDelivery_ {

	public static volatile SingularAttribute<InvoiceDelivery, String> deliveryNo;
	public static volatile SingularAttribute<InvoiceDelivery, Integer> id;
	public static volatile SingularAttribute<InvoiceDelivery, Invoice> invoice;

	public static final String DELIVERY_NO = "deliveryNo";
	public static final String ID = "id";
	public static final String INVOICE = "invoice";

}

