package org.trescal.cwms.core.pricing.jobcost.entity.jobcostingexpenseitem;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.servicecost.JobCostingServiceCost;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobCostingExpenseItem.class)
public abstract class JobCostingExpenseItem_ extends org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem.PricingItem_ {

	public static volatile SingularAttribute<JobCostingExpenseItem, JobExpenseItem> jobExpenseItem;
	public static volatile SingularAttribute<JobCostingExpenseItem, JobCostingServiceCost> serviceCost;
	public static volatile SingularAttribute<JobCostingExpenseItem, JobCosting> jobCosting;
	public static volatile SingularAttribute<JobCostingExpenseItem, Integer> id;

	public static final String JOB_EXPENSE_ITEM = "jobExpenseItem";
	public static final String SERVICE_COST = "serviceCost";
	public static final String JOB_COSTING = "jobCosting";
	public static final String ID = "id";

}

