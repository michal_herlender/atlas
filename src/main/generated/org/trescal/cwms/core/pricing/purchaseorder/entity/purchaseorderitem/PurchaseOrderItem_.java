package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitemnote.PurchaseOrderItemNote;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitemprogressaction.PurchaseOrderItemProgressAction;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.PurchaseOrderJobItem;
import org.trescal.cwms.core.pricing.supplierinvoice.entity.SupplierInvoice;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PurchaseOrderItem.class)
public abstract class PurchaseOrderItem_ extends org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem.PricingItem_ {

	public static volatile SetAttribute<PurchaseOrderItem, PurchaseOrderItemNote> notes;
	public static volatile SingularAttribute<PurchaseOrderItem, String> description;
	public static volatile SingularAttribute<PurchaseOrderItem, SupplierInvoice> supplierInvoice;
	public static volatile SetAttribute<PurchaseOrderItem, PurchaseOrderItemProgressAction> progressActions;
	public static volatile SingularAttribute<PurchaseOrderItem, Boolean> received;
	public static volatile SetAttribute<PurchaseOrderItem, PurchaseOrderJobItem> linkedJobItems;
	public static volatile SingularAttribute<PurchaseOrderItem, Invoice> internalInvoice;
	public static volatile SingularAttribute<PurchaseOrderItem, Boolean> accountsApproved;
	public static volatile SingularAttribute<PurchaseOrderItem, NominalCode> nominal;
	public static volatile SingularAttribute<PurchaseOrderItem, CostType> costType;
	public static volatile SingularAttribute<PurchaseOrderItem, Subdiv> businessSubdiv;
	public static volatile SingularAttribute<PurchaseOrderItem, Boolean> goodsApproved;
	public static volatile SingularAttribute<PurchaseOrderItem, Boolean> cancelled;
	public static volatile SingularAttribute<PurchaseOrderItem, Integer> id;
	public static volatile SingularAttribute<PurchaseOrderItem, LocalDate> deliveryDate;
	public static volatile SingularAttribute<PurchaseOrderItem, PurchaseOrderItemReceiptStatus> receiptStatus;
	public static volatile SingularAttribute<PurchaseOrderItem, PurchaseOrder> order;

	public static final String NOTES = "notes";
	public static final String DESCRIPTION = "description";
	public static final String SUPPLIER_INVOICE = "supplierInvoice";
	public static final String PROGRESS_ACTIONS = "progressActions";
	public static final String RECEIVED = "received";
	public static final String LINKED_JOB_ITEMS = "linkedJobItems";
	public static final String INTERNAL_INVOICE = "internalInvoice";
	public static final String ACCOUNTS_APPROVED = "accountsApproved";
	public static final String NOMINAL = "nominal";
	public static final String COST_TYPE = "costType";
	public static final String BUSINESS_SUBDIV = "businessSubdiv";
	public static final String GOODS_APPROVED = "goodsApproved";
	public static final String CANCELLED = "cancelled";
	public static final String ID = "id";
	public static final String DELIVERY_DATE = "deliveryDate";
	public static final String RECEIPT_STATUS = "receiptStatus";
	public static final String ORDER = "order";

}

