package org.trescal.cwms.core.pricing.jobcost.entity.jobcostingnote;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobCostingNote.class)
public abstract class JobCostingNote_ extends org.trescal.cwms.core.system.entity.note.Note_ {

	public static volatile SingularAttribute<JobCostingNote, JobCosting> jobcosting;

	public static final String JOBCOSTING = "jobcosting";

}

