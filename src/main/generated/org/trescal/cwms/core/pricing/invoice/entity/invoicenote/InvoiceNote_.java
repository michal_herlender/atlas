package org.trescal.cwms.core.pricing.invoice.entity.invoicenote;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InvoiceNote.class)
public abstract class InvoiceNote_ extends org.trescal.cwms.core.system.entity.note.Note_ {

	public static volatile SingularAttribute<InvoiceNote, Invoice> invoice;

	public static final String INVOICE = "invoice";

}

