package org.trescal.cwms.core.pricing.jobcost.entity.costs.repcost;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobCostingRepairCost.class)
public abstract class JobCostingRepairCost_ extends org.trescal.cwms.core.pricing.entity.costs.thirdparty.TPSupportedRepCost_ {

	public static volatile SingularAttribute<JobCostingRepairCost, JobCostingLinkedRepairCost> linkedCost;
	public static volatile SingularAttribute<JobCostingRepairCost, CostSource> costSrc;
	public static volatile SingularAttribute<JobCostingRepairCost, JobCostingItem> jobCostingItem;

	public static final String LINKED_COST = "linkedCost";
	public static final String COST_SRC = "costSrc";
	public static final String JOB_COSTING_ITEM = "jobCostingItem";

}

