package org.trescal.cwms.core.pricing.creditnote.entity.creditnoteitem;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CreditNoteItem.class)
public abstract class CreditNoteItem_ extends org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem.TaxablePricingItem_ {

	public static volatile SingularAttribute<CreditNoteItem, CreditNote> creditNote;
	public static volatile SingularAttribute<CreditNoteItem, NominalCode> nominal;
	public static volatile SingularAttribute<CreditNoteItem, Subdiv> businessSubdiv;
	public static volatile SingularAttribute<CreditNoteItem, String> description;
	public static volatile SingularAttribute<CreditNoteItem, Integer> id;

	public static final String CREDIT_NOTE = "creditNote";
	public static final String NOMINAL = "nominal";
	public static final String BUSINESS_SUBDIV = "businessSubdiv";
	public static final String DESCRIPTION = "description";
	public static final String ID = "id";

}

