package org.trescal.cwms.core.userright.entity.permissionlimit;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.userright.enums.Permission;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PermissionLimit.class)
public abstract class PermissionLimit_ {

	public static volatile SingularAttribute<PermissionLimit, Integer> level;
	public static volatile SingularAttribute<PermissionLimit, Permission> permission;
	public static volatile SingularAttribute<PermissionLimit, Integer> id;
	public static volatile SingularAttribute<PermissionLimit, TypeLimit> type;

	public static final String LEVEL = "level";
	public static final String PERMISSION = "permission";
	public static final String ID = "id";
	public static final String TYPE = "type";

}

