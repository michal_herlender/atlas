package org.trescal.cwms.core.userright.entity.userrole;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.userright.entity.role.UserRightRole;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserRole.class)
public abstract class UserRole_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<UserRole, UserRightRole> role;
	public static volatile SingularAttribute<UserRole, Integer> id;
	public static volatile SingularAttribute<UserRole, User> user;

	public static final String ROLE = "role";
	public static final String ID = "id";
	public static final String USER = "user";

}

