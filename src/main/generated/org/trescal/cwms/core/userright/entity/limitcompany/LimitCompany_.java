package org.trescal.cwms.core.userright.entity.limitcompany;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.userright.entity.permissionlimit.PermissionLimit;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(LimitCompany.class)
public abstract class LimitCompany_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<LimitCompany, PermissionLimit> permissionLimit;
	public static volatile SingularAttribute<LimitCompany, BigDecimal> maximum;
	public static volatile SingularAttribute<LimitCompany, Integer> id;

	public static final String PERMISSION_LIMIT = "permissionLimit";
	public static final String MAXIMUM = "maximum";
	public static final String ID = "id";

}

