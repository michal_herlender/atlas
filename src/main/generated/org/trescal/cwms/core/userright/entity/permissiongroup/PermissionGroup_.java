package org.trescal.cwms.core.userright.entity.permissiongroup;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.userright.enums.Permission;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PermissionGroup.class)
public abstract class PermissionGroup_ {

	public static volatile SetAttribute<PermissionGroup, Permission> permissions;
	public static volatile SingularAttribute<PermissionGroup, String> name;
	public static volatile SingularAttribute<PermissionGroup, Integer> id;
	public static volatile SetAttribute<PermissionGroup, Translation> descriptionTranslations;

	public static final String PERMISSIONS = "permissions";
	public static final String NAME = "name";
	public static final String ID = "id";
	public static final String DESCRIPTION_TRANSLATIONS = "descriptionTranslations";

}

