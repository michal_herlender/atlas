package org.trescal.cwms.core.userright.entity.role;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.userright.entity.permissiongroup.PermissionGroup;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserRightRole.class)
public abstract class UserRightRole_ {

	public static volatile SingularAttribute<UserRightRole, String> name;
	public static volatile ListAttribute<UserRightRole, PermissionGroup> groups;
	public static volatile SingularAttribute<UserRightRole, Integer> id;
	public static volatile SetAttribute<UserRightRole, Translation> descriptionTranslations;

	public static final String NAME = "name";
	public static final String GROUPS = "groups";
	public static final String ID = "id";
	public static final String DESCRIPTION_TRANSLATIONS = "descriptionTranslations";

}

