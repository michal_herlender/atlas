package org.trescal.cwms.core.cdc.entity.referential;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CountryCT.class)
public abstract class CountryCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<CountryCT, String> country;
	public static volatile SingularAttribute<CountryCT, String> countrycode;
	public static volatile SingularAttribute<CountryCT, Integer> countryid;

	public static final String COUNTRY = "country";
	public static final String COUNTRYCODE = "countrycode";
	public static final String COUNTRYID = "countryid";

}

