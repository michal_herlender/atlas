package org.trescal.cwms.core.cdc.entity.company;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SubdivCT.class)
public abstract class SubdivCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<SubdivCT, Integer> subdivid;
	public static volatile SingularAttribute<SubdivCT, String> subname;
	public static volatile SingularAttribute<SubdivCT, Integer> coid;
	public static volatile SingularAttribute<SubdivCT, Boolean> active;
	public static volatile SingularAttribute<SubdivCT, String> subdivcode;

	public static final String SUBDIVID = "subdivid";
	public static final String SUBNAME = "subname";
	public static final String COID = "coid";
	public static final String ACTIVE = "active";
	public static final String SUBDIVCODE = "subdivcode";

}

