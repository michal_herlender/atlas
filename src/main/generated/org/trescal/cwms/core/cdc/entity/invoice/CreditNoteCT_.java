package org.trescal.cwms.core.cdc.entity.invoice;

import java.math.BigDecimal;
import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CreditNoteCT.class)
public abstract class CreditNoteCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<CreditNoteCT, BigDecimal> finalcost;
	public static volatile SingularAttribute<CreditNoteCT, Integer> invid;
	public static volatile SingularAttribute<CreditNoteCT, String> creditnoteno;
	public static volatile SingularAttribute<CreditNoteCT, Integer> id;
	public static volatile SingularAttribute<CreditNoteCT, Boolean> issued;
	public static volatile SingularAttribute<CreditNoteCT, LocalDate> issuedate;
	public static volatile SingularAttribute<CreditNoteCT, BigDecimal> vatvalue;
	public static volatile SingularAttribute<CreditNoteCT, Integer> orgid;
	public static volatile SingularAttribute<CreditNoteCT, BigDecimal> totalcost;

	public static final String FINALCOST = "finalcost";
	public static final String INVID = "invid";
	public static final String CREDITNOTENO = "creditnoteno";
	public static final String ID = "id";
	public static final String ISSUED = "issued";
	public static final String ISSUEDATE = "issuedate";
	public static final String VATVALUE = "vatvalue";
	public static final String ORGID = "orgid";
	public static final String TOTALCOST = "totalcost";

}

