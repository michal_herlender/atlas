package org.trescal.cwms.core.cdc.entity.instrument;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstrumentUsageTypeCT.class)
public abstract class InstrumentUsageTypeCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<InstrumentUsageTypeCT, InstrumentStatus> instrumentStatus;
	public static volatile SingularAttribute<InstrumentUsageTypeCT, Boolean> recall;
	public static volatile SingularAttribute<InstrumentUsageTypeCT, String> name;
	public static volatile SingularAttribute<InstrumentUsageTypeCT, Integer> typeid;
	public static volatile SingularAttribute<InstrumentUsageTypeCT, String> fullDescription;
	public static volatile SingularAttribute<InstrumentUsageTypeCT, Boolean> defaultType;

	public static final String INSTRUMENT_STATUS = "instrumentStatus";
	public static final String RECALL = "recall";
	public static final String NAME = "name";
	public static final String TYPEID = "typeid";
	public static final String FULL_DESCRIPTION = "fullDescription";
	public static final String DEFAULT_TYPE = "defaultType";

}

