package org.trescal.cwms.core.cdc.entity.workinstruction;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(WorkInstructionCT.class)
public abstract class WorkInstructionCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<WorkInstructionCT, Integer> descid;
	public static volatile SingularAttribute<WorkInstructionCT, String> name;
	public static volatile SingularAttribute<WorkInstructionCT, Integer> id;
	public static volatile SingularAttribute<WorkInstructionCT, String> title;
	public static volatile SingularAttribute<WorkInstructionCT, Integer> orgid;

	public static final String DESCID = "descid";
	public static final String NAME = "name";
	public static final String ID = "id";
	public static final String TITLE = "title";
	public static final String ORGID = "orgid";

}

