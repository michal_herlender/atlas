package org.trescal.cwms.core.cdc.entity.certificate;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CertLinkCT.class)
public abstract class CertLinkCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<CertLinkCT, Integer> jobitemid;
	public static volatile SingularAttribute<CertLinkCT, Integer> linkid;
	public static volatile SingularAttribute<CertLinkCT, Integer> certid;

	public static final String JOBITEMID = "jobitemid";
	public static final String LINKID = "linkid";
	public static final String CERTID = "certid";

}

