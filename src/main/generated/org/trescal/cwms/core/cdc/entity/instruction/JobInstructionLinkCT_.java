package org.trescal.cwms.core.cdc.entity.instruction;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobInstructionLinkCT.class)
public abstract class JobInstructionLinkCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<JobInstructionLinkCT, Integer> jobid;
	public static volatile SingularAttribute<JobInstructionLinkCT, Integer> instructionid;
	public static volatile SingularAttribute<JobInstructionLinkCT, Integer> id;

	public static final String JOBID = "jobid";
	public static final String INSTRUCTIONID = "instructionid";
	public static final String ID = "id";

}

