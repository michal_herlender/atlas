package org.trescal.cwms.core.cdc.entity.workinstruction;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstrumentWorkInstructionCT.class)
public abstract class InstrumentWorkInstructionCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<InstrumentWorkInstructionCT, Integer> workInstructionId;
	public static volatile SingularAttribute<InstrumentWorkInstructionCT, Integer> plantId;

	public static final String WORK_INSTRUCTION_ID = "workInstructionId";
	public static final String PLANT_ID = "plantId";

}

