package org.trescal.cwms.core.cdc.entity.job;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobItemCT.class)
public abstract class JobItemCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<JobItemCT, ZonedDateTime> datein;
	public static volatile SingularAttribute<JobItemCT, Integer> servicetypeid;
	public static volatile SingularAttribute<JobItemCT, Integer> stateid;
	public static volatile SingularAttribute<JobItemCT, Integer> groupid;
	public static volatile SingularAttribute<JobItemCT, Integer> plantid;
	public static volatile SingularAttribute<JobItemCT, String> clientRef;
	public static volatile SingularAttribute<JobItemCT, Integer> itemno;
	public static volatile SingularAttribute<JobItemCT, Integer> jobitemid;
	public static volatile SingularAttribute<JobItemCT, Integer> jobid;
	public static volatile SingularAttribute<JobItemCT, LocalDate> duedate;
	public static volatile SingularAttribute<JobItemCT, OffsetDateTime> clientreceiptdate;
	public static volatile SingularAttribute<JobItemCT, Integer> addrid;
	public static volatile SingularAttribute<JobItemCT, Date> datecomplete;

	public static final String DATEIN = "datein";
	public static final String SERVICETYPEID = "servicetypeid";
	public static final String STATEID = "stateid";
	public static final String GROUPID = "groupid";
	public static final String PLANTID = "plantid";
	public static final String CLIENT_REF = "clientRef";
	public static final String ITEMNO = "itemno";
	public static final String JOBITEMID = "jobitemid";
	public static final String JOBID = "jobid";
	public static final String DUEDATE = "duedate";
	public static final String CLIENTRECEIPTDATE = "clientreceiptdate";
	public static final String ADDRID = "addrid";
	public static final String DATECOMPLETE = "datecomplete";

}

