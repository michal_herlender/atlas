package org.trescal.cwms.core.cdc.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(EntityCT.class)
public abstract class EntityCT_ {

	public static volatile SingularAttribute<EntityCT, Integer> operationCode;
	public static volatile SingularAttribute<EntityCT, CdcCompositeId> compositeId;

	public static final String OPERATION_CODE = "operationCode";
	public static final String COMPOSITE_ID = "compositeId";

}

