package org.trescal.cwms.core.cdc.entity.referential;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstrumentModelCT.class)
public abstract class InstrumentModelCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<InstrumentModelCT, Integer> descriptionid;
	public static volatile SingularAttribute<InstrumentModelCT, Integer> mfrid;
	public static volatile SingularAttribute<InstrumentModelCT, ModelMfrType> modelmfrtype;
	public static volatile SingularAttribute<InstrumentModelCT, Integer> modeltypeid;
	public static volatile SingularAttribute<InstrumentModelCT, Integer> modelid;
	public static volatile SingularAttribute<InstrumentModelCT, String> model;
	public static volatile SingularAttribute<InstrumentModelCT, Boolean> quarantined;
	public static volatile SingularAttribute<InstrumentModelCT, Integer> tmlid;

	public static final String DESCRIPTIONID = "descriptionid";
	public static final String MFRID = "mfrid";
	public static final String MODELMFRTYPE = "modelmfrtype";
	public static final String MODELTYPEID = "modeltypeid";
	public static final String MODELID = "modelid";
	public static final String MODEL = "model";
	public static final String QUARANTINED = "quarantined";
	public static final String TMLID = "tmlid";

}

