package org.trescal.cwms.core.cdc.entity.job;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UpcomingWorkJobLinkCT.class)
public abstract class UpcomingWorkJobLinkCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<UpcomingWorkJobLinkCT, Integer> upcomingworkid;
	public static volatile SingularAttribute<UpcomingWorkJobLinkCT, Integer> jobid;
	public static volatile SingularAttribute<UpcomingWorkJobLinkCT, Boolean> active;
	public static volatile SingularAttribute<UpcomingWorkJobLinkCT, Integer> id;

	public static final String UPCOMINGWORKID = "upcomingworkid";
	public static final String JOBID = "jobid";
	public static final String ACTIVE = "active";
	public static final String ID = "id";

}

