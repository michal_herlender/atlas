package org.trescal.cwms.core.cdc.entity.company;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(LocationCT.class)
public abstract class LocationCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<LocationCT, Integer> locationid;
	public static volatile SingularAttribute<LocationCT, Boolean> active;
	public static volatile SingularAttribute<LocationCT, String> location;
	public static volatile SingularAttribute<LocationCT, Integer> addressid;

	public static final String LOCATIONID = "locationid";
	public static final String ACTIVE = "active";
	public static final String LOCATION = "location";
	public static final String ADDRESSID = "addressid";

}

