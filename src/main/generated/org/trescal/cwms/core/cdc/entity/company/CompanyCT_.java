package org.trescal.cwms.core.cdc.entity.company;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CompanyCT.class)
public abstract class CompanyCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<CompanyCT, String> companyCode;
	public static volatile SingularAttribute<CompanyCT, CompanyRole> companyRole;
	public static volatile SingularAttribute<CompanyCT, String> coname;
	public static volatile SingularAttribute<CompanyCT, String> fiscalIdentifier;
	public static volatile SingularAttribute<CompanyCT, Integer> coid;
	public static volatile SingularAttribute<CompanyCT, String> legalIdentifier;
	public static volatile SingularAttribute<CompanyCT, Integer> countryid;

	public static final String COMPANY_CODE = "companyCode";
	public static final String COMPANY_ROLE = "companyRole";
	public static final String CONAME = "coname";
	public static final String FISCAL_IDENTIFIER = "fiscalIdentifier";
	public static final String COID = "coid";
	public static final String LEGAL_IDENTIFIER = "legalIdentifier";
	public static final String COUNTRYID = "countryid";

}

