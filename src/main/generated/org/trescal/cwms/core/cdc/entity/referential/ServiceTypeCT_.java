package org.trescal.cwms.core.cdc.entity.referential;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.DomainType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ServiceTypeCT.class)
public abstract class ServiceTypeCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<ServiceTypeCT, String> longname;
	public static volatile SingularAttribute<ServiceTypeCT, Boolean> repair;
	public static volatile SingularAttribute<ServiceTypeCT, Integer> servicetypeid;
	public static volatile SingularAttribute<ServiceTypeCT, DomainType> domaintype;
	public static volatile SingularAttribute<ServiceTypeCT, Boolean> canbeperformedbyclient;
	public static volatile SingularAttribute<ServiceTypeCT, Integer> orderby;
	public static volatile SingularAttribute<ServiceTypeCT, String> shortname;

	public static final String LONGNAME = "longname";
	public static final String REPAIR = "repair";
	public static final String SERVICETYPEID = "servicetypeid";
	public static final String DOMAINTYPE = "domaintype";
	public static final String CANBEPERFORMEDBYCLIENT = "canbeperformedbyclient";
	public static final String ORDERBY = "orderby";
	public static final String SHORTNAME = "shortname";

}

