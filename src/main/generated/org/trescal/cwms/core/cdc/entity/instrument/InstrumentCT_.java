package org.trescal.cwms.core.cdc.entity.instrument;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;
import org.trescal.cwms.core.system.enums.IntervalUnit;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstrumentCT.class)
public abstract class InstrumentCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<InstrumentCT, Integer> usageid;
	public static volatile SingularAttribute<InstrumentCT, String> plantno;
	public static volatile SingularAttribute<InstrumentCT, Integer> mfrid;
	public static volatile SingularAttribute<InstrumentCT, Integer> modelid;
	public static volatile SingularAttribute<InstrumentCT, String> customerSpecification;
	public static volatile SingularAttribute<InstrumentCT, String> modelname;
	public static volatile SingularAttribute<InstrumentCT, Integer> plantid;
	public static volatile SingularAttribute<InstrumentCT, Integer> coid;
	public static volatile SingularAttribute<InstrumentCT, String> customerDescription;
	public static volatile SingularAttribute<InstrumentCT, Integer> calInterval;
	public static volatile SingularAttribute<InstrumentCT, String> freeTextLocation;
	public static volatile SingularAttribute<InstrumentCT, Boolean> customermanaged;
	public static volatile SingularAttribute<InstrumentCT, String> serialno;
	public static volatile SingularAttribute<InstrumentCT, Integer> addressid;
	public static volatile SingularAttribute<InstrumentCT, IntervalUnit> calIntervalUnit;
	public static volatile SingularAttribute<InstrumentCT, Boolean> calibrationStandard;
	public static volatile SingularAttribute<InstrumentCT, Integer> locationid;
	public static volatile SingularAttribute<InstrumentCT, Integer> personid;
	public static volatile SingularAttribute<InstrumentCT, String> formerBarcode;
	public static volatile SingularAttribute<InstrumentCT, LocalDate> recallDate;
	public static volatile SingularAttribute<InstrumentCT, InstrumentStatus> status;
	public static volatile SingularAttribute<InstrumentCT, Integer> defaultservicetypeid;
	public static volatile SingularAttribute<InstrumentCT, String> clientBarcode;

	public static final String USAGEID = "usageid";
	public static final String PLANTNO = "plantno";
	public static final String MFRID = "mfrid";
	public static final String MODELID = "modelid";
	public static final String CUSTOMER_SPECIFICATION = "customerSpecification";
	public static final String MODELNAME = "modelname";
	public static final String PLANTID = "plantid";
	public static final String COID = "coid";
	public static final String CUSTOMER_DESCRIPTION = "customerDescription";
	public static final String CAL_INTERVAL = "calInterval";
	public static final String FREE_TEXT_LOCATION = "freeTextLocation";
	public static final String CUSTOMERMANAGED = "customermanaged";
	public static final String SERIALNO = "serialno";
	public static final String ADDRESSID = "addressid";
	public static final String CAL_INTERVAL_UNIT = "calIntervalUnit";
	public static final String CALIBRATION_STANDARD = "calibrationStandard";
	public static final String LOCATIONID = "locationid";
	public static final String PERSONID = "personid";
	public static final String FORMER_BARCODE = "formerBarcode";
	public static final String RECALL_DATE = "recallDate";
	public static final String STATUS = "status";
	public static final String DEFAULTSERVICETYPEID = "defaultservicetypeid";
	public static final String CLIENT_BARCODE = "clientBarcode";

}

