package org.trescal.cwms.core.cdc.entity.company;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(DepartmentMemberCT.class)
public abstract class DepartmentMemberCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<DepartmentMemberCT, Integer> deptid;
	public static volatile SingularAttribute<DepartmentMemberCT, Integer> personid;
	public static volatile SingularAttribute<DepartmentMemberCT, Integer> id;

	public static final String DEPTID = "deptid";
	public static final String PERSONID = "personid";
	public static final String ID = "id";

}

