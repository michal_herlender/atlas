package org.trescal.cwms.core.cdc.entity.job;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobItemGroupCT.class)
public abstract class JobItemGroupCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<JobItemGroupCT, Integer> jobid;
	public static volatile SingularAttribute<JobItemGroupCT, Boolean> calibrationGroup;
	public static volatile SingularAttribute<JobItemGroupCT, Integer> id;
	public static volatile SingularAttribute<JobItemGroupCT, Boolean> deliveryGroup;

	public static final String JOBID = "jobid";
	public static final String CALIBRATION_GROUP = "calibrationGroup";
	public static final String ID = "id";
	public static final String DELIVERY_GROUP = "deliveryGroup";

}

