package org.trescal.cwms.core.cdc.entity.company;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ContactCT.class)
public abstract class ContactCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<ContactCT, Integer> defaddress;
	public static volatile SingularAttribute<ContactCT, Integer> subdivid;
	public static volatile SingularAttribute<ContactCT, String> firstname;
	public static volatile SingularAttribute<ContactCT, String> mobile;
	public static volatile SingularAttribute<ContactCT, Boolean> active;
	public static volatile SingularAttribute<ContactCT, Integer> personid;
	public static volatile SingularAttribute<ContactCT, String> telephone;
	public static volatile SingularAttribute<ContactCT, String> fax;
	public static volatile SingularAttribute<ContactCT, String> email;
	public static volatile SingularAttribute<ContactCT, String> lastname;

	public static final String DEFADDRESS = "defaddress";
	public static final String SUBDIVID = "subdivid";
	public static final String FIRSTNAME = "firstname";
	public static final String MOBILE = "mobile";
	public static final String ACTIVE = "active";
	public static final String PERSONID = "personid";
	public static final String TELEPHONE = "telephone";
	public static final String FAX = "fax";
	public static final String EMAIL = "email";
	public static final String LASTNAME = "lastname";

}

