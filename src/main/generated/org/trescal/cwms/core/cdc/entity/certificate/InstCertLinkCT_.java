package org.trescal.cwms.core.cdc.entity.certificate;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstCertLinkCT.class)
public abstract class InstCertLinkCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<InstCertLinkCT, Integer> plantid;
	public static volatile SingularAttribute<InstCertLinkCT, Integer> id;
	public static volatile SingularAttribute<InstCertLinkCT, Integer> certid;

	public static final String PLANTID = "plantid";
	public static final String ID = "id";
	public static final String CERTID = "certid";

}

