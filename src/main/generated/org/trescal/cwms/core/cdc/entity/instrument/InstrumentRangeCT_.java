package org.trescal.cwms.core.cdc.entity.instrument;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstrumentRangeCT.class)
public abstract class InstrumentRangeCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<InstrumentRangeCT, Integer> maxUom;
	public static volatile SingularAttribute<InstrumentRangeCT, Integer> uom;
	public static volatile SingularAttribute<InstrumentRangeCT, Double> start;
	public static volatile SingularAttribute<InstrumentRangeCT, Integer> plantid;
	public static volatile SingularAttribute<InstrumentRangeCT, Double> end;
	public static volatile SingularAttribute<InstrumentRangeCT, Integer> id;

	public static final String MAX_UOM = "maxUom";
	public static final String UOM = "uom";
	public static final String START = "start";
	public static final String PLANTID = "plantid";
	public static final String END = "end";
	public static final String ID = "id";

}

