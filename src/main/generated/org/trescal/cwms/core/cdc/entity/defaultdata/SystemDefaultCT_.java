package org.trescal.cwms.core.cdc.entity.defaultdata;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SystemDefaultCT.class)
public abstract class SystemDefaultCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<SystemDefaultCT, String> defaultdescription;
	public static volatile SingularAttribute<SystemDefaultCT, String> defaultvalue;
	public static volatile SingularAttribute<SystemDefaultCT, Boolean> groupwide;
	public static volatile SingularAttribute<SystemDefaultCT, String> defaultdatatype;
	public static volatile SingularAttribute<SystemDefaultCT, Integer> id;
	public static volatile SingularAttribute<SystemDefaultCT, String> defaultname;

	public static final String DEFAULTDESCRIPTION = "defaultdescription";
	public static final String DEFAULTVALUE = "defaultvalue";
	public static final String GROUPWIDE = "groupwide";
	public static final String DEFAULTDATATYPE = "defaultdatatype";
	public static final String ID = "id";
	public static final String DEFAULTNAME = "defaultname";

}

