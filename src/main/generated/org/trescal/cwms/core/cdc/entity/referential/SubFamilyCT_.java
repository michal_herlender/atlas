package org.trescal.cwms.core.cdc.entity.referential;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SubFamilyCT.class)
public abstract class SubFamilyCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<SubFamilyCT, Integer> familyid;
	public static volatile SingularAttribute<SubFamilyCT, Integer> descriptionid;
	public static volatile SingularAttribute<SubFamilyCT, String> description;
	public static volatile SingularAttribute<SubFamilyCT, Boolean> active;
	public static volatile SingularAttribute<SubFamilyCT, Integer> tmlid;

	public static final String FAMILYID = "familyid";
	public static final String DESCRIPTIONID = "descriptionid";
	public static final String DESCRIPTION = "description";
	public static final String ACTIVE = "active";
	public static final String TMLID = "tmlid";

}

