package org.trescal.cwms.core.cdc.entity.referential;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UomCT.class)
public abstract class UomCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<UomCT, String> symbol;
	public static volatile SingularAttribute<UomCT, String> name;
	public static volatile SingularAttribute<UomCT, Boolean> active;
	public static volatile SingularAttribute<UomCT, Integer> id;
	public static volatile SingularAttribute<UomCT, Integer> tmlid;

	public static final String SYMBOL = "symbol";
	public static final String NAME = "name";
	public static final String ACTIVE = "active";
	public static final String ID = "id";
	public static final String TMLID = "tmlid";

}

