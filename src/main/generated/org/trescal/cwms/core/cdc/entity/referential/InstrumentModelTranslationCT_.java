package org.trescal.cwms.core.cdc.entity.referential;

import java.util.Locale;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstrumentModelTranslationCT.class)
public abstract class InstrumentModelTranslationCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<InstrumentModelTranslationCT, String> translation;
	public static volatile SingularAttribute<InstrumentModelTranslationCT, Integer> id;
	public static volatile SingularAttribute<InstrumentModelTranslationCT, Locale> locale;

	public static final String TRANSLATION = "translation";
	public static final String ID = "id";
	public static final String LOCALE = "locale";

}

