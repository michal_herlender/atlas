package org.trescal.cwms.core.cdc.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.events.change.enums.ChangeEntity;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ProcessedChangeEntity.class)
public abstract class ProcessedChangeEntity_ {

	public static volatile SingularAttribute<ProcessedChangeEntity, byte[]> lastProcessedLsn;
	public static volatile SingularAttribute<ProcessedChangeEntity, ChangeEntity> changeEntity;

	public static final String LAST_PROCESSED_LSN = "lastProcessedLsn";
	public static final String CHANGE_ENTITY = "changeEntity";

}

