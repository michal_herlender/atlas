package org.trescal.cwms.core.cdc.entity.defaultdata;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SystemdDefaultApplicationCT.class)
public abstract class SystemdDefaultApplicationCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<SystemdDefaultApplicationCT, Integer> subdivid;
	public static volatile SingularAttribute<SystemdDefaultApplicationCT, Integer> companyid;
	public static volatile SingularAttribute<SystemdDefaultApplicationCT, String> defaultvalue;
	public static volatile SingularAttribute<SystemdDefaultApplicationCT, Integer> personid;
	public static volatile SingularAttribute<SystemdDefaultApplicationCT, Integer> id;
	public static volatile SingularAttribute<SystemdDefaultApplicationCT, Integer> defaultid;
	public static volatile SingularAttribute<SystemdDefaultApplicationCT, Integer> orgid;

	public static final String SUBDIVID = "subdivid";
	public static final String COMPANYID = "companyid";
	public static final String DEFAULTVALUE = "defaultvalue";
	public static final String PERSONID = "personid";
	public static final String ID = "id";
	public static final String DEFAULTID = "defaultid";
	public static final String ORGID = "orgid";

}

