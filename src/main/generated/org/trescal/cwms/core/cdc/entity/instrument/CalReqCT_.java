package org.trescal.cwms.core.cdc.entity.instrument;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CalReqCT.class)
public abstract class CalReqCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<CalReqCT, Integer> jobitemid;
	public static volatile SingularAttribute<CalReqCT, String> privateInstructions;
	public static volatile SingularAttribute<CalReqCT, Integer> compid;
	public static volatile SingularAttribute<CalReqCT, Integer> modelid;
	public static volatile SingularAttribute<CalReqCT, Integer> descid;
	public static volatile SingularAttribute<CalReqCT, Boolean> active;
	public static volatile SingularAttribute<CalReqCT, Integer> plantid;
	public static volatile SingularAttribute<CalReqCT, Integer> id;
	public static volatile SingularAttribute<CalReqCT, String> type;
	public static volatile SingularAttribute<CalReqCT, String> publicInstructions;

	public static final String JOBITEMID = "jobitemid";
	public static final String PRIVATE_INSTRUCTIONS = "privateInstructions";
	public static final String COMPID = "compid";
	public static final String MODELID = "modelid";
	public static final String DESCID = "descid";
	public static final String ACTIVE = "active";
	public static final String PLANTID = "plantid";
	public static final String ID = "id";
	public static final String TYPE = "type";
	public static final String PUBLIC_INSTRUCTIONS = "publicInstructions";

}

