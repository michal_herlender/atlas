package org.trescal.cwms.core.cdc.entity.invoice;

import java.math.BigDecimal;
import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InvoiceCT.class)
public abstract class InvoiceCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<InvoiceCT, BigDecimal> finalcost;
	public static volatile SingularAttribute<InvoiceCT, LocalDate> duedate;
	public static volatile SingularAttribute<InvoiceCT, String> invno;
	public static volatile SingularAttribute<InvoiceCT, Integer> coid;
	public static volatile SingularAttribute<InvoiceCT, Integer> id;
	public static volatile SingularAttribute<InvoiceCT, Boolean> issued;
	public static volatile SingularAttribute<InvoiceCT, LocalDate> issuedate;
	public static volatile SingularAttribute<InvoiceCT, BigDecimal> vatvalue;
	public static volatile SingularAttribute<InvoiceCT, Integer> orgid;
	public static volatile SingularAttribute<InvoiceCT, BigDecimal> totalcost;
	public static volatile SingularAttribute<InvoiceCT, Integer> addressid;

	public static final String FINALCOST = "finalcost";
	public static final String DUEDATE = "duedate";
	public static final String INVNO = "invno";
	public static final String COID = "coid";
	public static final String ID = "id";
	public static final String ISSUED = "issued";
	public static final String ISSUEDATE = "issuedate";
	public static final String VATVALUE = "vatvalue";
	public static final String ORGID = "orgid";
	public static final String TOTALCOST = "totalcost";
	public static final String ADDRESSID = "addressid";

}

