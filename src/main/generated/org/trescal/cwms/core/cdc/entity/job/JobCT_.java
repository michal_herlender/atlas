package org.trescal.cwms.core.cdc.entity.job;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobCT.class)
public abstract class JobCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<JobCT, Integer> jobid;
	public static volatile SingularAttribute<JobCT, Integer> statusid;
	public static volatile SingularAttribute<JobCT, String> jobno;
	public static volatile SingularAttribute<JobCT, LocalDate> regdate;
	public static volatile SingularAttribute<JobCT, Integer> returnto;
	public static volatile SingularAttribute<JobCT, String> clientref;
	public static volatile SingularAttribute<JobCT, Integer> personid;
	public static volatile SingularAttribute<JobCT, JobType> jobType;
	public static volatile SingularAttribute<JobCT, Integer> orgid;
	public static volatile SingularAttribute<JobCT, LocalDate> datecomplete;

	public static final String JOBID = "jobid";
	public static final String STATUSID = "statusid";
	public static final String JOBNO = "jobno";
	public static final String REGDATE = "regdate";
	public static final String RETURNTO = "returnto";
	public static final String CLIENTREF = "clientref";
	public static final String PERSONID = "personid";
	public static final String JOB_TYPE = "jobType";
	public static final String ORGID = "orgid";
	public static final String DATECOMPLETE = "datecomplete";

}

