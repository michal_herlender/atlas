package org.trescal.cwms.core.cdc.entity.instruction;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CompanyInstructionLinkCT.class)
public abstract class CompanyInstructionLinkCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<CompanyInstructionLinkCT, Integer> instructionid;
	public static volatile SingularAttribute<CompanyInstructionLinkCT, Integer> coid;
	public static volatile SingularAttribute<CompanyInstructionLinkCT, Integer> businesssubdivid;
	public static volatile SingularAttribute<CompanyInstructionLinkCT, Integer> id;
	public static volatile SingularAttribute<CompanyInstructionLinkCT, Boolean> issubdivinstruction;
	public static volatile SingularAttribute<CompanyInstructionLinkCT, Integer> orgid;

	public static final String INSTRUCTIONID = "instructionid";
	public static final String COID = "coid";
	public static final String BUSINESSSUBDIVID = "businesssubdivid";
	public static final String ID = "id";
	public static final String ISSUBDIVINSTRUCTION = "issubdivinstruction";
	public static final String ORGID = "orgid";

}

