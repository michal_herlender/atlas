package org.trescal.cwms.core.cdc.entity.job;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UpcomingWorkCT.class)
public abstract class UpcomingWorkCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<UpcomingWorkCT, Integer> respuserid;
	public static volatile SingularAttribute<UpcomingWorkCT, LocalDate> dueDate;
	public static volatile SingularAttribute<UpcomingWorkCT, Integer> deptid;
	public static volatile SingularAttribute<UpcomingWorkCT, Boolean> active;
	public static volatile SingularAttribute<UpcomingWorkCT, String> description;
	public static volatile SingularAttribute<UpcomingWorkCT, Integer> coid;
	public static volatile SingularAttribute<UpcomingWorkCT, Integer> addedbyid;
	public static volatile SingularAttribute<UpcomingWorkCT, Integer> personid;
	public static volatile SingularAttribute<UpcomingWorkCT, Integer> id;
	public static volatile SingularAttribute<UpcomingWorkCT, String> title;
	public static volatile SingularAttribute<UpcomingWorkCT, LocalDate> startDate;
	public static volatile SingularAttribute<UpcomingWorkCT, Integer> orgid;

	public static final String RESPUSERID = "respuserid";
	public static final String DUE_DATE = "dueDate";
	public static final String DEPTID = "deptid";
	public static final String ACTIVE = "active";
	public static final String DESCRIPTION = "description";
	public static final String COID = "coid";
	public static final String ADDEDBYID = "addedbyid";
	public static final String PERSONID = "personid";
	public static final String ID = "id";
	public static final String TITLE = "title";
	public static final String START_DATE = "startDate";
	public static final String ORGID = "orgid";

}

