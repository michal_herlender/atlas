package org.trescal.cwms.core.cdc.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CdcCompositeId.class)
public abstract class CdcCompositeId_ {

	public static volatile SingularAttribute<CdcCompositeId, byte[]> seqVal;
	public static volatile SingularAttribute<CdcCompositeId, byte[]> startLsn;

	public static final String SEQ_VAL = "seqVal";
	public static final String START_LSN = "startLsn";

}

