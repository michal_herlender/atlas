package org.trescal.cwms.core.cdc.entity.instruction;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SubdivInstructionLinkCT.class)
public abstract class SubdivInstructionLinkCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<SubdivInstructionLinkCT, Integer> subdivid;
	public static volatile SingularAttribute<SubdivInstructionLinkCT, Integer> instructionid;
	public static volatile SingularAttribute<SubdivInstructionLinkCT, Integer> businesssubdivid;
	public static volatile SingularAttribute<SubdivInstructionLinkCT, Integer> id;
	public static volatile SingularAttribute<SubdivInstructionLinkCT, Boolean> issubdivinstruction;
	public static volatile SingularAttribute<SubdivInstructionLinkCT, Integer> orgid;

	public static final String SUBDIVID = "subdivid";
	public static final String INSTRUCTIONID = "instructionid";
	public static final String BUSINESSSUBDIVID = "businesssubdivid";
	public static final String ID = "id";
	public static final String ISSUBDIVINSTRUCTION = "issubdivinstruction";
	public static final String ORGID = "orgid";

}

