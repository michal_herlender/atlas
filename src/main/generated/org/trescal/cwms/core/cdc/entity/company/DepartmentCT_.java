package org.trescal.cwms.core.cdc.entity.company;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(DepartmentCT.class)
public abstract class DepartmentCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<DepartmentCT, Integer> subdivid;
	public static volatile SingularAttribute<DepartmentCT, Integer> deptid;
	public static volatile SingularAttribute<DepartmentCT, String> name;
	public static volatile SingularAttribute<DepartmentCT, DepartmentType> type;
	public static volatile SingularAttribute<DepartmentCT, String> shortName;
	public static volatile SingularAttribute<DepartmentCT, Integer> addressid;

	public static final String SUBDIVID = "subdivid";
	public static final String DEPTID = "deptid";
	public static final String NAME = "name";
	public static final String TYPE = "type";
	public static final String SHORT_NAME = "shortName";
	public static final String ADDRESSID = "addressid";

}

