package org.trescal.cwms.core.cdc.entity.company;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.companystatus.CompanyStatus;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CompanySettingsCT.class)
public abstract class CompanySettingsCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<CompanySettingsCT, Integer> companyid;
	public static volatile SingularAttribute<CompanySettingsCT, Boolean> active;
	public static volatile SingularAttribute<CompanySettingsCT, Integer> id;
	public static volatile SingularAttribute<CompanySettingsCT, Integer> orgid;
	public static volatile SingularAttribute<CompanySettingsCT, Boolean> onstop;
	public static volatile SingularAttribute<CompanySettingsCT, CompanyStatus> status;

	public static final String COMPANYID = "companyid";
	public static final String ACTIVE = "active";
	public static final String ID = "id";
	public static final String ORGID = "orgid";
	public static final String ONSTOP = "onstop";
	public static final String STATUS = "status";

}

