package org.trescal.cwms.core.cdc.entity.job;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.workflow.entity.TimeOfDay;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(EngineerAllocationCT.class)
public abstract class EngineerAllocationCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<EngineerAllocationCT, LocalDate> allocateduntil;
	public static volatile SingularAttribute<EngineerAllocationCT, LocalDate> allocatedFor;
	public static volatile SingularAttribute<EngineerAllocationCT, Integer> jobitemid;
	public static volatile SingularAttribute<EngineerAllocationCT, TimeOfDay> timeofday;
	public static volatile SingularAttribute<EngineerAllocationCT, LocalDate> allocatedOn;
	public static volatile SingularAttribute<EngineerAllocationCT, Integer> allocatedbyid;
	public static volatile SingularAttribute<EngineerAllocationCT, Integer> deptid;
	public static volatile SingularAttribute<EngineerAllocationCT, Boolean> active;
	public static volatile SingularAttribute<EngineerAllocationCT, Integer> allocatedtoid;
	public static volatile SingularAttribute<EngineerAllocationCT, Integer> id;
	public static volatile SingularAttribute<EngineerAllocationCT, TimeOfDay> timeofdayuntil;

	public static final String ALLOCATEDUNTIL = "allocateduntil";
	public static final String ALLOCATED_FOR = "allocatedFor";
	public static final String JOBITEMID = "jobitemid";
	public static final String TIMEOFDAY = "timeofday";
	public static final String ALLOCATED_ON = "allocatedOn";
	public static final String ALLOCATEDBYID = "allocatedbyid";
	public static final String DEPTID = "deptid";
	public static final String ACTIVE = "active";
	public static final String ALLOCATEDTOID = "allocatedtoid";
	public static final String ID = "id";
	public static final String TIMEOFDAYUNTIL = "timeofdayuntil";

}

