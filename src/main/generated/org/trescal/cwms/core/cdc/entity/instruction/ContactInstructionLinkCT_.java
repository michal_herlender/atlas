package org.trescal.cwms.core.cdc.entity.instruction;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ContactInstructionLinkCT.class)
public abstract class ContactInstructionLinkCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<ContactInstructionLinkCT, Integer> instructionid;
	public static volatile SingularAttribute<ContactInstructionLinkCT, Integer> personid;
	public static volatile SingularAttribute<ContactInstructionLinkCT, Integer> id;
	public static volatile SingularAttribute<ContactInstructionLinkCT, Integer> orgid;

	public static final String INSTRUCTIONID = "instructionid";
	public static final String PERSONID = "personid";
	public static final String ID = "id";
	public static final String ORGID = "orgid";

}

