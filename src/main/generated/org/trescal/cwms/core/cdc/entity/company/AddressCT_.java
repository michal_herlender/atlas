package org.trescal.cwms.core.cdc.entity.company;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AddressCT.class)
public abstract class AddressCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<AddressCT, Integer> subdivid;
	public static volatile SingularAttribute<AddressCT, String> addr2;
	public static volatile SingularAttribute<AddressCT, String> town;
	public static volatile SingularAttribute<AddressCT, String> addr1;
	public static volatile SingularAttribute<AddressCT, String> addr3;
	public static volatile SingularAttribute<AddressCT, String> county;
	public static volatile SingularAttribute<AddressCT, String> postcode;
	public static volatile SingularAttribute<AddressCT, Boolean> active;
	public static volatile SingularAttribute<AddressCT, Integer> countryid;
	public static volatile SingularAttribute<AddressCT, Integer> addressid;

	public static final String SUBDIVID = "subdivid";
	public static final String ADDR2 = "addr2";
	public static final String TOWN = "town";
	public static final String ADDR1 = "addr1";
	public static final String ADDR3 = "addr3";
	public static final String COUNTY = "county";
	public static final String POSTCODE = "postcode";
	public static final String ACTIVE = "active";
	public static final String COUNTRYID = "countryid";
	public static final String ADDRESSID = "addressid";

}

