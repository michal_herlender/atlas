package org.trescal.cwms.core.cdc.entity.instruction;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BaseInstructionCT.class)
public abstract class BaseInstructionCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<BaseInstructionCT, InstructionType> instructiontype;
	public static volatile SingularAttribute<BaseInstructionCT, String> instruction;
	public static volatile SingularAttribute<BaseInstructionCT, Integer> instructionid;

	public static final String INSTRUCTIONTYPE = "instructiontype";
	public static final String INSTRUCTION = "instruction";
	public static final String INSTRUCTIONID = "instructionid";

}

