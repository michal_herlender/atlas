package org.trescal.cwms.core.cdc.entity.company;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SubdivSettingsCT.class)
public abstract class SubdivSettingsCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<SubdivSettingsCT, Integer> subdivid;
	public static volatile SingularAttribute<SubdivSettingsCT, Integer> id;
	public static volatile SingularAttribute<SubdivSettingsCT, Integer> orgid;
	public static volatile SingularAttribute<SubdivSettingsCT, Integer> businesscontactid;

	public static final String SUBDIVID = "subdivid";
	public static final String ID = "id";
	public static final String ORGID = "orgid";
	public static final String BUSINESSCONTACTID = "businesscontactid";

}

