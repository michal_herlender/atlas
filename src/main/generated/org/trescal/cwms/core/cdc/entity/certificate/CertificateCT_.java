package org.trescal.cwms.core.cdc.entity.certificate;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateType;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.system.enums.IntervalUnit;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CertificateCT.class)
public abstract class CertificateCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<CertificateCT, Boolean> repair;
	public static volatile SingularAttribute<CertificateCT, Integer> servicetypeid;
	public static volatile SingularAttribute<CertificateCT, Integer> supplementaryforid;
	public static volatile SingularAttribute<CertificateCT, Integer> certid;
	public static volatile SingularAttribute<CertificateCT, CertificateType> type;
	public static volatile SingularAttribute<CertificateCT, Integer> duration;
	public static volatile SingularAttribute<CertificateCT, Date> certdate;
	public static volatile SingularAttribute<CertificateCT, String> certno;
	public static volatile SingularAttribute<CertificateCT, Date> caldate;
	public static volatile SingularAttribute<CertificateCT, CalibrationVerificationStatus> calibrationverificationstatus;
	public static volatile SingularAttribute<CertificateCT, IntervalUnit> intervalunit;
	public static volatile SingularAttribute<CertificateCT, Boolean> optimization;
	public static volatile SingularAttribute<CertificateCT, Integer> thirddiv;
	public static volatile SingularAttribute<CertificateCT, Boolean> restriction;
	public static volatile SingularAttribute<CertificateCT, Boolean> adjustment;
	public static volatile SingularAttribute<CertificateCT, CertStatusEnum> certstatus;
	public static volatile SingularAttribute<CertificateCT, String> thirdcertno;

	public static final String REPAIR = "repair";
	public static final String SERVICETYPEID = "servicetypeid";
	public static final String SUPPLEMENTARYFORID = "supplementaryforid";
	public static final String CERTID = "certid";
	public static final String TYPE = "type";
	public static final String DURATION = "duration";
	public static final String CERTDATE = "certdate";
	public static final String CERTNO = "certno";
	public static final String CALDATE = "caldate";
	public static final String CALIBRATIONVERIFICATIONSTATUS = "calibrationverificationstatus";
	public static final String INTERVALUNIT = "intervalunit";
	public static final String OPTIMIZATION = "optimization";
	public static final String THIRDDIV = "thirddiv";
	public static final String RESTRICTION = "restriction";
	public static final String ADJUSTMENT = "adjustment";
	public static final String CERTSTATUS = "certstatus";
	public static final String THIRDCERTNO = "thirdcertno";

}

