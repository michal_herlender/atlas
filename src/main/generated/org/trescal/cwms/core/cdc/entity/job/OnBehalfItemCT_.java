package org.trescal.cwms.core.cdc.entity.job;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(OnBehalfItemCT.class)
public abstract class OnBehalfItemCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<OnBehalfItemCT, Integer> jobitemid;
	public static volatile SingularAttribute<OnBehalfItemCT, Integer> coid;
	public static volatile SingularAttribute<OnBehalfItemCT, Integer> id;
	public static volatile SingularAttribute<OnBehalfItemCT, Integer> addressid;

	public static final String JOBITEMID = "jobitemid";
	public static final String COID = "coid";
	public static final String ID = "id";
	public static final String ADDRESSID = "addressid";

}

