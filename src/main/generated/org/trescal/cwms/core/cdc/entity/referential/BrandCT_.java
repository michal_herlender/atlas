package org.trescal.cwms.core.cdc.entity.referential;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BrandCT.class)
public abstract class BrandCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<BrandCT, Boolean> genericmfr;
	public static volatile SingularAttribute<BrandCT, Integer> mfrid;
	public static volatile SingularAttribute<BrandCT, String> name;
	public static volatile SingularAttribute<BrandCT, Boolean> active;
	public static volatile SingularAttribute<BrandCT, Integer> tmlid;

	public static final String GENERICMFR = "genericmfr";
	public static final String MFRID = "mfrid";
	public static final String NAME = "name";
	public static final String ACTIVE = "active";
	public static final String TMLID = "tmlid";

}

