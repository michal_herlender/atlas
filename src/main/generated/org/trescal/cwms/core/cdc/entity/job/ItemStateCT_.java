package org.trescal.cwms.core.cdc.entity.job;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ItemStateCT.class)
public abstract class ItemStateCT_ extends org.trescal.cwms.core.cdc.entity.EntityCT_ {

	public static volatile SingularAttribute<ItemStateCT, Integer> stateid;
	public static volatile SingularAttribute<ItemStateCT, Boolean> active;
	public static volatile SingularAttribute<ItemStateCT, String> description;
	public static volatile SingularAttribute<ItemStateCT, Boolean> retired;
	public static volatile SingularAttribute<ItemStateCT, String> type;

	public static final String STATEID = "stateid";
	public static final String ACTIVE = "active";
	public static final String DESCRIPTION = "description";
	public static final String RETIRED = "retired";
	public static final String TYPE = "type";

}

