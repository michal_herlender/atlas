package org.trescal.cwms.core.audit;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.organisation.OrganisationLevel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Allocated.class)
public abstract class Allocated_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SingularAttribute<Allocated, OrganisationLevel> organisation;

	public static final String ORGANISATION = "organisation";

}

