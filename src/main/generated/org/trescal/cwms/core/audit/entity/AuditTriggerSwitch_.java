package org.trescal.cwms.core.audit.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AuditTriggerSwitch.class)
public abstract class AuditTriggerSwitch_ {

	public static volatile SingularAttribute<AuditTriggerSwitch, Integer> id;
	public static volatile SingularAttribute<AuditTriggerSwitch, Boolean> enabled;

	public static final String ID = "id";
	public static final String ENABLED = "enabled";

}

