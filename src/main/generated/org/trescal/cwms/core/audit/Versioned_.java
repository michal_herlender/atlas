package org.trescal.cwms.core.audit;

import java.sql.Timestamp;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Versioned.class)
public abstract class Versioned_ {

	public static volatile SingularAttribute<Versioned, Contact> lastModifiedBy;
	public static volatile SingularAttribute<Versioned, Timestamp> lastModified;

	public static final String LAST_MODIFIED_BY = "lastModifiedBy";
	public static final String LAST_MODIFIED = "lastModified";

}

