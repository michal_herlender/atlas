package org.trescal.cwms.core.audit;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Auditable.class)
public abstract class Auditable_ {

	public static volatile SingularAttribute<Auditable, Contact> lastModifiedBy;
	public static volatile SingularAttribute<Auditable, Date> lastModified;

	public static final String LAST_MODIFIED_BY = "lastModifiedBy";
	public static final String LAST_MODIFIED = "lastModified";

}

