package org.trescal.cwms.core.timekeeping.entity;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.timekeeping.enums.TimeActivity;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TimeSheetEntry.class)
public abstract class TimeSheetEntry_ {

	public static volatile SingularAttribute<TimeSheetEntry, Duration> duration;
	public static volatile SingularAttribute<TimeSheetEntry, SupportedCurrency> expenseCurrency;
	public static volatile SingularAttribute<TimeSheetEntry, BigDecimal> expenseValue;
	public static volatile SingularAttribute<TimeSheetEntry, LocalDateTime> start;
	public static volatile SingularAttribute<TimeSheetEntry, Boolean> fullTime;
	public static volatile SingularAttribute<TimeSheetEntry, String> comment;
	public static volatile SingularAttribute<TimeSheetEntry, Integer> id;
	public static volatile SingularAttribute<TimeSheetEntry, TimeActivity> timeActivity;
	public static volatile SingularAttribute<TimeSheetEntry, Contact> employee;
	public static volatile SingularAttribute<TimeSheetEntry, Job> job;
	public static volatile SingularAttribute<TimeSheetEntry, Subdiv> subdiv;

	public static final String DURATION = "duration";
	public static final String EXPENSE_CURRENCY = "expenseCurrency";
	public static final String EXPENSE_VALUE = "expenseValue";
	public static final String START = "start";
	public static final String FULL_TIME = "fullTime";
	public static final String COMMENT = "comment";
	public static final String ID = "id";
	public static final String TIME_ACTIVITY = "timeActivity";
	public static final String EMPLOYEE = "employee";
	public static final String JOB = "job";
	public static final String SUBDIV = "subdiv";

}

