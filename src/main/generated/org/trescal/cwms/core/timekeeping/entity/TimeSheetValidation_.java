package org.trescal.cwms.core.timekeeping.entity;

import java.time.Duration;
import java.time.LocalDateTime;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.timekeeping.enums.TimeSheetValidationStatus;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TimeSheetValidation.class)
public abstract class TimeSheetValidation_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SingularAttribute<TimeSheetValidation, LocalDateTime> validatedOn;
	public static volatile SingularAttribute<TimeSheetValidation, Integer> week;
	public static volatile SingularAttribute<TimeSheetValidation, Duration> registeredTime;
	public static volatile SingularAttribute<TimeSheetValidation, Integer> year;
	public static volatile SingularAttribute<TimeSheetValidation, Duration> carryOver;
	public static volatile SingularAttribute<TimeSheetValidation, Duration> workingTime;
	public static volatile SingularAttribute<TimeSheetValidation, Contact> validatedBy;
	public static volatile SingularAttribute<TimeSheetValidation, Integer> id;
	public static volatile SingularAttribute<TimeSheetValidation, Contact> employee;
	public static volatile SingularAttribute<TimeSheetValidation, String> rejectionReason;
	public static volatile SingularAttribute<TimeSheetValidation, TimeSheetValidationStatus> status;

	public static final String VALIDATED_ON = "validatedOn";
	public static final String WEEK = "week";
	public static final String REGISTERED_TIME = "registeredTime";
	public static final String YEAR = "year";
	public static final String CARRY_OVER = "carryOver";
	public static final String WORKING_TIME = "workingTime";
	public static final String VALIDATED_BY = "validatedBy";
	public static final String ID = "id";
	public static final String EMPLOYEE = "employee";
	public static final String REJECTION_REASON = "rejectionReason";
	public static final String STATUS = "status";

}

