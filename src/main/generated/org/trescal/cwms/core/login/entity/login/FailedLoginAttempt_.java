package org.trescal.cwms.core.login.entity.login;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(FailedLoginAttempt.class)
public abstract class FailedLoginAttempt_ {

	public static volatile SingularAttribute<FailedLoginAttempt, Date> createdAt;
	public static volatile SingularAttribute<FailedLoginAttempt, Integer> id;
	public static volatile SingularAttribute<FailedLoginAttempt, String> userName;

	public static final String CREATED_AT = "createdAt";
	public static final String ID = "id";
	public static final String USER_NAME = "userName";

}

