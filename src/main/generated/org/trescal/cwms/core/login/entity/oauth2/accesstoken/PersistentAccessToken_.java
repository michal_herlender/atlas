package org.trescal.cwms.core.login.entity.oauth2.accesstoken;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PersistentAccessToken.class)
public abstract class PersistentAccessToken_ extends org.trescal.cwms.core.login.entity.oauth2.PersistentToken_ {

	public static volatile SingularAttribute<PersistentAccessToken, String> clientId;
	public static volatile SingularAttribute<PersistentAccessToken, String> authenticationId;
	public static volatile SingularAttribute<PersistentAccessToken, Integer> id;
	public static volatile SingularAttribute<PersistentAccessToken, String> username;
	public static volatile SingularAttribute<PersistentAccessToken, String> refreshToken;

	public static final String CLIENT_ID = "clientId";
	public static final String AUTHENTICATION_ID = "authenticationId";
	public static final String ID = "id";
	public static final String USERNAME = "username";
	public static final String REFRESH_TOKEN = "refreshToken";

}

