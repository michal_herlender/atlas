package org.trescal.cwms.core.login.entity.portal.usergroup;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.login.entity.portal.usergrouppropertyvalue.UserGroupPropertyValue;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserGroup.class)
public abstract class UserGroup_ {

	public static volatile SingularAttribute<UserGroup, Boolean> systemDefault;
	public static volatile SetAttribute<UserGroup, Translation> nameTranslation;
	public static volatile SingularAttribute<UserGroup, Integer> groupid;
	public static volatile SingularAttribute<UserGroup, String> name;
	public static volatile SetAttribute<UserGroup, Translation> descriptionTranslation;
	public static volatile SingularAttribute<UserGroup, String> description;
	public static volatile SingularAttribute<UserGroup, Company> company;
	public static volatile SetAttribute<UserGroup, Contact> contacts;
	public static volatile SingularAttribute<UserGroup, Boolean> compDefault;
	public static volatile SetAttribute<UserGroup, UserGroupPropertyValue> properties;

	public static final String SYSTEM_DEFAULT = "systemDefault";
	public static final String NAME_TRANSLATION = "nameTranslation";
	public static final String GROUPID = "groupid";
	public static final String NAME = "name";
	public static final String DESCRIPTION_TRANSLATION = "descriptionTranslation";
	public static final String DESCRIPTION = "description";
	public static final String COMPANY = "company";
	public static final String CONTACTS = "contacts";
	public static final String COMP_DEFAULT = "compDefault";
	public static final String PROPERTIES = "properties";

}

