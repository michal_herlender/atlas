package org.trescal.cwms.core.login.entity.userpreferences;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.system.entity.labelprinter.LabelPrinter;
import org.trescal.cwms.core.system.entity.userinstructiontype.UserInstructionType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserPreferences.class)
public abstract class UserPreferences_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<UserPreferences, String> scheme;
	public static volatile SingularAttribute<UserPreferences, JobType> defaultJobType;
	public static volatile SingularAttribute<UserPreferences, Boolean> mouseoverActive;
	public static volatile SingularAttribute<UserPreferences, LabelPrinter> labelPrinter;
	public static volatile SingularAttribute<UserPreferences, Integer> mouseoverDelay;
	public static volatile SetAttribute<UserPreferences, UserInstructionType> userInstructionTypes;
	public static volatile SingularAttribute<UserPreferences, Boolean> autoPrintLabel;
	public static volatile SingularAttribute<UserPreferences, Boolean> jiKeyNavigation;
	public static volatile SingularAttribute<UserPreferences, Contact> contact;
	public static volatile SingularAttribute<UserPreferences, Integer> id;
	public static volatile SingularAttribute<UserPreferences, Boolean> navBarPosition;
	public static volatile SingularAttribute<UserPreferences, Boolean> defaultServiceTypeUsage;
	public static volatile SingularAttribute<UserPreferences, Boolean> includeSelfInEmailReplies;

	public static final String SCHEME = "scheme";
	public static final String DEFAULT_JOB_TYPE = "defaultJobType";
	public static final String MOUSEOVER_ACTIVE = "mouseoverActive";
	public static final String LABEL_PRINTER = "labelPrinter";
	public static final String MOUSEOVER_DELAY = "mouseoverDelay";
	public static final String USER_INSTRUCTION_TYPES = "userInstructionTypes";
	public static final String AUTO_PRINT_LABEL = "autoPrintLabel";
	public static final String JI_KEY_NAVIGATION = "jiKeyNavigation";
	public static final String CONTACT = "contact";
	public static final String ID = "id";
	public static final String NAV_BAR_POSITION = "navBarPosition";
	public static final String DEFAULT_SERVICE_TYPE_USAGE = "defaultServiceTypeUsage";
	public static final String INCLUDE_SELF_IN_EMAIL_REPLIES = "includeSelfInEmailReplies";

}

