package org.trescal.cwms.core.login.entity.userpreferencesdefault;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.login.entity.userpreferences.UserPreferences;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserPreferencesDefault.class)
public abstract class UserPreferencesDefault_ {

	public static volatile SingularAttribute<UserPreferencesDefault, Address> address;
	public static volatile SingularAttribute<UserPreferencesDefault, UserPreferences> userPreferences;
	public static volatile SingularAttribute<UserPreferencesDefault, Boolean> globalDefault;
	public static volatile SingularAttribute<UserPreferencesDefault, Company> company;
	public static volatile SingularAttribute<UserPreferencesDefault, Integer> id;
	public static volatile SingularAttribute<UserPreferencesDefault, Subdiv> subdiv;

	public static final String ADDRESS = "address";
	public static final String USER_PREFERENCES = "userPreferences";
	public static final String GLOBAL_DEFAULT = "globalDefault";
	public static final String COMPANY = "company";
	public static final String ID = "id";
	public static final String SUBDIV = "subdiv";

}

