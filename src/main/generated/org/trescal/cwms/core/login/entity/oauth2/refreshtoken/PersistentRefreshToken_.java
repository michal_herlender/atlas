package org.trescal.cwms.core.login.entity.oauth2.refreshtoken;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PersistentRefreshToken.class)
public abstract class PersistentRefreshToken_ extends org.trescal.cwms.core.login.entity.oauth2.PersistentToken_ {

	public static volatile SingularAttribute<PersistentRefreshToken, Integer> id;

	public static final String ID = "id";

}

