package org.trescal.cwms.core.login.entity.login;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Login.class)
public abstract class Login_ {

	public static volatile SingularAttribute<Login, Contact> con;
	public static volatile SingularAttribute<Login, Date> loginTime;
	public static volatile SingularAttribute<Login, Integer> id;
	public static volatile SingularAttribute<Login, Boolean> webLogCreated;
	public static volatile SingularAttribute<Login, String> webLogPath;

	public static final String CON = "con";
	public static final String LOGIN_TIME = "loginTime";
	public static final String ID = "id";
	public static final String WEB_LOG_CREATED = "webLogCreated";
	public static final String WEB_LOG_PATH = "webLogPath";

}

