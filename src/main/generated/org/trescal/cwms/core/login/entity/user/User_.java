package org.trescal.cwms.core.login.entity.user;

import java.time.LocalDate;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.userright.entity.userrole.UserRole;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(User.class)
public abstract class User_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SingularAttribute<User, LocalDate> lastLogin;
	public static volatile SingularAttribute<User, Boolean> passwordInvalidated;
	public static volatile SingularAttribute<User, Contact> con;
	public static volatile SingularAttribute<User, Boolean> webTermsAccepted;
	public static volatile SingularAttribute<User, LocalDate> registeredDate;
	public static volatile SingularAttribute<User, String> securityAnswer;
	public static volatile SingularAttribute<User, String> securityQuestion;
	public static volatile SingularAttribute<User, Boolean> webAware;
	public static volatile SingularAttribute<User, Integer> loginCount;
	public static volatile SingularAttribute<User, Date> passwordInvalidatedAt;
	public static volatile ListAttribute<User, UserRole> userRoles;
	public static volatile SingularAttribute<User, String> password;
	public static volatile SingularAttribute<User, Boolean> webUser;
	public static volatile SingularAttribute<User, String> username;

	public static final String LAST_LOGIN = "lastLogin";
	public static final String PASSWORD_INVALIDATED = "passwordInvalidated";
	public static final String CON = "con";
	public static final String WEB_TERMS_ACCEPTED = "webTermsAccepted";
	public static final String REGISTERED_DATE = "registeredDate";
	public static final String SECURITY_ANSWER = "securityAnswer";
	public static final String SECURITY_QUESTION = "securityQuestion";
	public static final String WEB_AWARE = "webAware";
	public static final String LOGIN_COUNT = "loginCount";
	public static final String PASSWORD_INVALIDATED_AT = "passwordInvalidatedAt";
	public static final String USER_ROLES = "userRoles";
	public static final String PASSWORD = "password";
	public static final String WEB_USER = "webUser";
	public static final String USERNAME = "username";

}

