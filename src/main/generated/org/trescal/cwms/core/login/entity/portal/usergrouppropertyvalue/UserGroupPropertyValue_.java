package org.trescal.cwms.core.login.entity.portal.usergrouppropertyvalue;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.login.entity.portal.UserGroupProperty;
import org.trescal.cwms.core.login.entity.portal.UserScope;
import org.trescal.cwms.core.login.entity.portal.usergroup.UserGroup;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserGroupPropertyValue.class)
public abstract class UserGroupPropertyValue_ {

	public static volatile SingularAttribute<UserGroupPropertyValue, UserScope> scope;
	public static volatile SingularAttribute<UserGroupPropertyValue, UserGroupProperty> userGroupProperty;
	public static volatile SingularAttribute<UserGroupPropertyValue, Integer> id;
	public static volatile SingularAttribute<UserGroupPropertyValue, UserGroup> userGroup;

	public static final String SCOPE = "scope";
	public static final String USER_GROUP_PROPERTY = "userGroupProperty";
	public static final String ID = "id";
	public static final String USER_GROUP = "userGroup";

}

