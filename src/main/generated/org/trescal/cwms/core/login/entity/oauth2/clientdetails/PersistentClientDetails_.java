package org.trescal.cwms.core.login.entity.oauth2.clientdetails;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.userright.entity.role.UserRightRole;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PersistentClientDetails.class)
public abstract class PersistentClientDetails_ {

	public static volatile SingularAttribute<PersistentClientDetails, String> clientId;
	public static volatile SingularAttribute<PersistentClientDetails, UserRightRole> role;
	public static volatile SingularAttribute<PersistentClientDetails, String> scope;
	public static volatile SingularAttribute<PersistentClientDetails, Integer> accessTokenValiditySeconds;
	public static volatile SingularAttribute<PersistentClientDetails, String> description;
	public static volatile SingularAttribute<PersistentClientDetails, String> clientSecret;
	public static volatile SingularAttribute<PersistentClientDetails, Integer> refreshTokenValiditySeconds;
	public static volatile SingularAttribute<PersistentClientDetails, String> resourceIds;

	public static final String CLIENT_ID = "clientId";
	public static final String ROLE = "role";
	public static final String SCOPE = "scope";
	public static final String ACCESS_TOKEN_VALIDITY_SECONDS = "accessTokenValiditySeconds";
	public static final String DESCRIPTION = "description";
	public static final String CLIENT_SECRET = "clientSecret";
	public static final String REFRESH_TOKEN_VALIDITY_SECONDS = "refreshTokenValiditySeconds";
	public static final String RESOURCE_IDS = "resourceIds";

}

