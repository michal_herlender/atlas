package org.trescal.cwms.core.login.entity.oauth2;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PersistentToken.class)
public abstract class PersistentToken_ {

	public static volatile SingularAttribute<PersistentToken, String> serializedAuthentication;
	public static volatile SingularAttribute<PersistentToken, String> tokenId;
	public static volatile SingularAttribute<PersistentToken, String> serializedToken;

	public static final String SERIALIZED_AUTHENTICATION = "serializedAuthentication";
	public static final String TOKEN_ID = "tokenId";
	public static final String SERIALIZED_TOKEN = "serializedToken";

}

