package org.trescal.cwms.core.contract.entity.contractdemand;

import java.util.BitSet;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.contract.entity.contract.Contract;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.domain.InstrumentModelDomain;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ContractDemand.class)
public abstract class ContractDemand_ {

	public static volatile SingularAttribute<ContractDemand, Description> subfamily;
	public static volatile SingularAttribute<ContractDemand, Contract> contract;
	public static volatile SingularAttribute<ContractDemand, InstrumentModelDomain> domain;
	public static volatile SingularAttribute<ContractDemand, Integer> id;
	public static volatile SingularAttribute<ContractDemand, BitSet> demandSet;

	public static final String SUBFAMILY = "subfamily";
	public static final String CONTRACT = "contract";
	public static final String DOMAIN = "domain";
	public static final String ID = "id";
	public static final String DEMAND_SET = "demandSet";

}

