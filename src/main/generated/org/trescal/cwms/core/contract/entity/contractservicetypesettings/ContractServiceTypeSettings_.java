package org.trescal.cwms.core.contract.entity.contractservicetypesettings;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.contract.entity.contract.Contract;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ContractServiceTypeSettings.class)
public abstract class ContractServiceTypeSettings_ {

	public static volatile SingularAttribute<ContractServiceTypeSettings, ServiceType> serviceType;
	public static volatile SingularAttribute<ContractServiceTypeSettings, Contract> contract;
	public static volatile SingularAttribute<ContractServiceTypeSettings, Integer> turnaround;
	public static volatile SingularAttribute<ContractServiceTypeSettings, Integer> id;

	public static final String SERVICE_TYPE = "serviceType";
	public static final String CONTRACT = "contract";
	public static final String TURNAROUND = "turnaround";
	public static final String ID = "id";

}

