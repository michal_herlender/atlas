package org.trescal.cwms.core.contract.entity.contract;

import java.math.BigDecimal;
import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.paymentmode.PaymentMode;
import org.trescal.cwms.core.company.entity.paymentterms.PaymentTerm;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.contract.entity.contractdemand.ContractDemand;
import org.trescal.cwms.core.contract.entity.contractservicetypesettings.ContractServiceTypeSettings;
import org.trescal.cwms.core.contract.enums.ContractInstrumentListType;
import org.trescal.cwms.core.contract.enums.InvoicingMethod;
import org.trescal.cwms.core.contract.enums.Software;
import org.trescal.cwms.core.contract.enums.TransferType;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.system.entity.instruction.Instruction;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.enums.IntervalUnit;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Contract.class)
public abstract class Contract_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SetAttribute<Contract, Instruction> instructions;
	public static volatile SingularAttribute<Contract, Software> software;
	public static volatile SingularAttribute<Contract, LocalDate> endDate;
	public static volatile SingularAttribute<Contract, Company> clientCompany;
	public static volatile SingularAttribute<Contract, String> description;
	public static volatile SingularAttribute<Contract, Integer> duration;
	public static volatile SingularAttribute<Contract, Integer> warrantyRepair;
	public static volatile SingularAttribute<Contract, BigDecimal> premium;
	public static volatile SetAttribute<Contract, Instrument> instruments;
	public static volatile SingularAttribute<Contract, ContractInstrumentListType> instrumentListType;
	public static volatile SingularAttribute<Contract, Contact> clientContact;
	public static volatile SingularAttribute<Contract, String> legalContractNo;
	public static volatile SingularAttribute<Contract, Boolean> dbManagement;
	public static volatile SingularAttribute<Contract, SupportedCurrency> currency;
	public static volatile SingularAttribute<Contract, Integer> id;
	public static volatile SingularAttribute<Contract, IntervalUnit> durationUnit;
	public static volatile SingularAttribute<Contract, Integer> invoiceFrequency;
	public static volatile ListAttribute<Contract, ContractServiceTypeSettings> serviceTypeSettings;
	public static volatile SingularAttribute<Contract, BPO> bpo;
	public static volatile SingularAttribute<Contract, InvoicingMethod> invoicingMethod;
	public static volatile SetAttribute<Contract, Subdiv> subdivs;
	public static volatile SingularAttribute<Contract, PaymentMode> paymentMode;
	public static volatile SingularAttribute<Contract, BigDecimal> outstandingPremium;
	public static volatile SingularAttribute<Contract, String> contractNumber;
	public static volatile SingularAttribute<Contract, Contact> trescalManager;
	public static volatile SingularAttribute<Contract, Contact> invoicingManager;
	public static volatile SingularAttribute<Contract, Integer> warrantyCalibration;
	public static volatile SingularAttribute<Contract, TransferType> transferType;
	public static volatile SingularAttribute<Contract, Quotation> quotation;
	public static volatile SingularAttribute<Contract, PaymentTerm> paymentTerm;
	public static volatile SingularAttribute<Contract, LocalDate> startDate;
	public static volatile ListAttribute<Contract, ContractDemand> demands;

	public static final String INSTRUCTIONS = "instructions";
	public static final String SOFTWARE = "software";
	public static final String END_DATE = "endDate";
	public static final String CLIENT_COMPANY = "clientCompany";
	public static final String DESCRIPTION = "description";
	public static final String DURATION = "duration";
	public static final String WARRANTY_REPAIR = "warrantyRepair";
	public static final String PREMIUM = "premium";
	public static final String INSTRUMENTS = "instruments";
	public static final String INSTRUMENT_LIST_TYPE = "instrumentListType";
	public static final String CLIENT_CONTACT = "clientContact";
	public static final String LEGAL_CONTRACT_NO = "legalContractNo";
	public static final String DB_MANAGEMENT = "dbManagement";
	public static final String CURRENCY = "currency";
	public static final String ID = "id";
	public static final String DURATION_UNIT = "durationUnit";
	public static final String INVOICE_FREQUENCY = "invoiceFrequency";
	public static final String SERVICE_TYPE_SETTINGS = "serviceTypeSettings";
	public static final String BPO = "bpo";
	public static final String INVOICING_METHOD = "invoicingMethod";
	public static final String SUBDIVS = "subdivs";
	public static final String PAYMENT_MODE = "paymentMode";
	public static final String OUTSTANDING_PREMIUM = "outstandingPremium";
	public static final String CONTRACT_NUMBER = "contractNumber";
	public static final String TRESCAL_MANAGER = "trescalManager";
	public static final String INVOICING_MANAGER = "invoicingManager";
	public static final String WARRANTY_CALIBRATION = "warrantyCalibration";
	public static final String TRANSFER_TYPE = "transferType";
	public static final String QUOTATION = "quotation";
	public static final String PAYMENT_TERM = "paymentTerm";
	public static final String START_DATE = "startDate";
	public static final String DEMANDS = "demands";

}

