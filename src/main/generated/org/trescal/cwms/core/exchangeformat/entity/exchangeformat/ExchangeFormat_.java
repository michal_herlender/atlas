package org.trescal.cwms.core.exchangeformat.entity.exchangeformat;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.AliasGroup;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfieldnamedetails.ExchangeFormatFieldNameDetails;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatDateFormatEnum;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatLevelEnum;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatTypeEnum;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ExchangeFormat.class)
public abstract class ExchangeFormat_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SingularAttribute<ExchangeFormat, String> sheetName;
	public static volatile SingularAttribute<ExchangeFormat, ExchangeFormatDateFormatEnum> dateFormat;
	public static volatile SingularAttribute<ExchangeFormat, Company> clientCompany;
	public static volatile SingularAttribute<ExchangeFormat, String> description;
	public static volatile ListAttribute<ExchangeFormat, ExchangeFormatFieldNameDetails> exchangeFormatFieldNameDetails;
	public static volatile SingularAttribute<ExchangeFormat, Company> businessCompany;
	public static volatile SingularAttribute<ExchangeFormat, AliasGroup> aliasGroup;
	public static volatile SingularAttribute<ExchangeFormat, ExchangeFormatTypeEnum> type;
	public static volatile SingularAttribute<ExchangeFormat, Boolean> deleted;
	public static volatile SingularAttribute<ExchangeFormat, Subdiv> businessSubdiv;
	public static volatile SingularAttribute<ExchangeFormat, String> name;
	public static volatile SingularAttribute<ExchangeFormat, ExchangeFormatLevelEnum> exchangeFormatLevel;
	public static volatile SingularAttribute<ExchangeFormat, Integer> linesToSkip;
	public static volatile SingularAttribute<ExchangeFormat, Integer> id;

	public static final String SHEET_NAME = "sheetName";
	public static final String DATE_FORMAT = "dateFormat";
	public static final String CLIENT_COMPANY = "clientCompany";
	public static final String DESCRIPTION = "description";
	public static final String EXCHANGE_FORMAT_FIELD_NAME_DETAILS = "exchangeFormatFieldNameDetails";
	public static final String BUSINESS_COMPANY = "businessCompany";
	public static final String ALIAS_GROUP = "aliasGroup";
	public static final String TYPE = "type";
	public static final String DELETED = "deleted";
	public static final String BUSINESS_SUBDIV = "businessSubdiv";
	public static final String NAME = "name";
	public static final String EXCHANGE_FORMAT_LEVEL = "exchangeFormatLevel";
	public static final String LINES_TO_SKIP = "linesToSkip";
	public static final String ID = "id";

}

