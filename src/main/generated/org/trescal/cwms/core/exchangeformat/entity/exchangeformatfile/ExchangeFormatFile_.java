package org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ExchangeFormatFile.class)
public abstract class ExchangeFormatFile_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<ExchangeFormatFile, String> fileName;
	public static volatile SingularAttribute<ExchangeFormatFile, Contact> addedBy;
	public static volatile SingularAttribute<ExchangeFormatFile, Integer> id;
	public static volatile SingularAttribute<ExchangeFormatFile, String> type;
	public static volatile SingularAttribute<ExchangeFormatFile, Date> addedOn;

	public static final String FILE_NAME = "fileName";
	public static final String ADDED_BY = "addedBy";
	public static final String ID = "id";
	public static final String TYPE = "type";
	public static final String ADDED_ON = "addedOn";

}

