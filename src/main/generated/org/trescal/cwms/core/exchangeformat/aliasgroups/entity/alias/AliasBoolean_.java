package org.trescal.cwms.core.exchangeformat.aliasgroups.entity.alias;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AliasBoolean.class)
public abstract class AliasBoolean_ extends org.trescal.cwms.core.exchangeformat.aliasgroups.entity.alias.Alias_ {

	public static volatile SingularAttribute<AliasBoolean, Boolean> value;

	public static final String VALUE = "value";

}

