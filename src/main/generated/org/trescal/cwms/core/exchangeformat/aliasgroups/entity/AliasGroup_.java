package org.trescal.cwms.core.exchangeformat.aliasgroups.entity;

import java.time.LocalDateTime;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.alias.AliasBoolean;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.alias.AliasIntervalUnit;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.alias.AliasServiceType;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AliasGroup.class)
public abstract class AliasGroup_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile ListAttribute<AliasGroup, AliasBoolean> booleanAliases;
	public static volatile ListAttribute<AliasGroup, ExchangeFormat> exchangeFormats;
	public static volatile ListAttribute<AliasGroup, AliasServiceType> serviceTypeAliases;
	public static volatile SingularAttribute<AliasGroup, Contact> createdBy;
	public static volatile SingularAttribute<AliasGroup, Boolean> defaultBusinessCompany;
	public static volatile SingularAttribute<AliasGroup, String> name;
	public static volatile SingularAttribute<AliasGroup, String> description;
	public static volatile SingularAttribute<AliasGroup, Integer> id;
	public static volatile SingularAttribute<AliasGroup, LocalDateTime> createdOn;
	public static volatile ListAttribute<AliasGroup, AliasIntervalUnit> intervalUnitAliases;

	public static final String BOOLEAN_ALIASES = "booleanAliases";
	public static final String EXCHANGE_FORMATS = "exchangeFormats";
	public static final String SERVICE_TYPE_ALIASES = "serviceTypeAliases";
	public static final String CREATED_BY = "createdBy";
	public static final String DEFAULT_BUSINESS_COMPANY = "defaultBusinessCompany";
	public static final String NAME = "name";
	public static final String DESCRIPTION = "description";
	public static final String ID = "id";
	public static final String CREATED_ON = "createdOn";
	public static final String INTERVAL_UNIT_ALIASES = "intervalUnitAliases";

}

