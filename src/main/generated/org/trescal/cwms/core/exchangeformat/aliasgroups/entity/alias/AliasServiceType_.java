package org.trescal.cwms.core.exchangeformat.aliasgroups.entity.alias;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AliasServiceType.class)
public abstract class AliasServiceType_ extends org.trescal.cwms.core.exchangeformat.aliasgroups.entity.alias.Alias_ {

	public static volatile SingularAttribute<AliasServiceType, ServiceType> value;

	public static final String VALUE = "value";

}

