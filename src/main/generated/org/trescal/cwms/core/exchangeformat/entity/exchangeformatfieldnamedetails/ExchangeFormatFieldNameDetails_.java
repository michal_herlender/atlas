package org.trescal.cwms.core.exchangeformat.entity.exchangeformatfieldnamedetails;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatFieldNameEnum;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ExchangeFormatFieldNameDetails.class)
public abstract class ExchangeFormatFieldNameDetails_ {

	public static volatile SingularAttribute<ExchangeFormatFieldNameDetails, ExchangeFormatFieldNameEnum> fieldName;
	public static volatile SingularAttribute<ExchangeFormatFieldNameDetails, String> templateName;
	public static volatile SingularAttribute<ExchangeFormatFieldNameDetails, Integer> id;
	public static volatile SingularAttribute<ExchangeFormatFieldNameDetails, Integer> position;
	public static volatile SingularAttribute<ExchangeFormatFieldNameDetails, ExchangeFormat> exchangeFormat;
	public static volatile SingularAttribute<ExchangeFormatFieldNameDetails, Boolean> mandatory;

	public static final String FIELD_NAME = "fieldName";
	public static final String TEMPLATE_NAME = "templateName";
	public static final String ID = "id";
	public static final String POSITION = "position";
	public static final String EXCHANGE_FORMAT = "exchangeFormat";
	public static final String MANDATORY = "mandatory";

}

