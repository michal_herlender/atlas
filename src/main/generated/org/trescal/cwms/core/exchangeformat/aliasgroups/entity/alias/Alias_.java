package org.trescal.cwms.core.exchangeformat.aliasgroups.entity.alias;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.AliasGroup;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Alias.class)
public abstract class Alias_ {

	public static volatile SingularAttribute<Alias, String> alias;
	public static volatile SingularAttribute<Alias, AliasGroup> aliasGroup;
	public static volatile SingularAttribute<Alias, Integer> id;

	public static final String ALIAS = "alias";
	public static final String ALIAS_GROUP = "aliasGroup";
	public static final String ID = "id";

}

