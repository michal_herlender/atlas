package org.trescal.cwms.core.exchangeformat.aliasgroups.entity.alias;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.system.enums.IntervalUnit;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AliasIntervalUnit.class)
public abstract class AliasIntervalUnit_ extends org.trescal.cwms.core.exchangeformat.aliasgroups.entity.alias.Alias_ {

	public static volatile SingularAttribute<AliasIntervalUnit, IntervalUnit> value;

	public static final String VALUE = "value";

}

