package org.trescal.cwms.core.company.entity.supplier;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.companyaccreditation.CompanyAccreditation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Supplier.class)
public abstract class Supplier_ {

	public static volatile SetAttribute<Supplier, CompanyAccreditation> companyAccreditations;
	public static volatile SingularAttribute<Supplier, Integer> supplierid;
	public static volatile SingularAttribute<Supplier, Boolean> doesNotExpire;
	public static volatile SingularAttribute<Supplier, Integer> chaseAfterDays;
	public static volatile SingularAttribute<Supplier, SupplierApprovalType> approvalType;
	public static volatile SingularAttribute<Supplier, String> name;
	public static volatile SingularAttribute<Supplier, Boolean> chaseClientOnExpire;
	public static volatile SingularAttribute<Supplier, String> description;
	public static volatile SingularAttribute<Supplier, Boolean> active;
	public static volatile SingularAttribute<Supplier, Boolean> allowUse;

	public static final String COMPANY_ACCREDITATIONS = "companyAccreditations";
	public static final String SUPPLIERID = "supplierid";
	public static final String DOES_NOT_EXPIRE = "doesNotExpire";
	public static final String CHASE_AFTER_DAYS = "chaseAfterDays";
	public static final String APPROVAL_TYPE = "approvalType";
	public static final String NAME = "name";
	public static final String CHASE_CLIENT_ON_EXPIRE = "chaseClientOnExpire";
	public static final String DESCRIPTION = "description";
	public static final String ACTIVE = "active";
	public static final String ALLOW_USE = "allowUse";

}

