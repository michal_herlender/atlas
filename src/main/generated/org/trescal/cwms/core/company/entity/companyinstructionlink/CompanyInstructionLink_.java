package org.trescal.cwms.core.company.entity.companyinstructionlink;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.instruction.Instruction;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CompanyInstructionLink.class)
public abstract class CompanyInstructionLink_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<CompanyInstructionLink, Boolean> subdivInstruction;
	public static volatile SingularAttribute<CompanyInstructionLink, Instruction> instruction;
	public static volatile SingularAttribute<CompanyInstructionLink, Subdiv> businessSubdiv;
	public static volatile SingularAttribute<CompanyInstructionLink, Company> company;
	public static volatile SingularAttribute<CompanyInstructionLink, Integer> id;

	public static final String SUBDIV_INSTRUCTION = "subdivInstruction";
	public static final String INSTRUCTION = "instruction";
	public static final String BUSINESS_SUBDIV = "businessSubdiv";
	public static final String COMPANY = "company";
	public static final String ID = "id";

}

