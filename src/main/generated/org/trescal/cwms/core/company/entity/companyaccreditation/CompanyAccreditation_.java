package org.trescal.cwms.core.company.entity.companyaccreditation;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.supplier.Supplier;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CompanyAccreditation.class)
public abstract class CompanyAccreditation_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<CompanyAccreditation, LocalDate> expiryDate;
	public static volatile SingularAttribute<CompanyAccreditation, String> reason;
	public static volatile SingularAttribute<CompanyAccreditation, LocalDate> lastEmailAlert;
	public static volatile SingularAttribute<CompanyAccreditation, Supplier> supplier;
	public static volatile SingularAttribute<CompanyAccreditation, Company> company;
	public static volatile SingularAttribute<CompanyAccreditation, Integer> id;
	public static volatile SingularAttribute<CompanyAccreditation, Boolean> emailAlert;

	public static final String EXPIRY_DATE = "expiryDate";
	public static final String REASON = "reason";
	public static final String LAST_EMAIL_ALERT = "lastEmailAlert";
	public static final String SUPPLIER = "supplier";
	public static final String COMPANY = "company";
	public static final String ID = "id";
	public static final String EMAIL_ALERT = "emailAlert";

}

