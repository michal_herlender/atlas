package org.trescal.cwms.core.company.entity.companylistmember;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companylist.CompanyList;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CompanyListMember.class)
public abstract class CompanyListMember_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SingularAttribute<CompanyListMember, CompanyList> companyList;
	public static volatile SingularAttribute<CompanyListMember, Boolean> active;
	public static volatile SingularAttribute<CompanyListMember, Company> company;
	public static volatile SingularAttribute<CompanyListMember, Integer> id;

	public static final String COMPANY_LIST = "companyList";
	public static final String ACTIVE = "active";
	public static final String COMPANY = "company";
	public static final String ID = "id";

}

