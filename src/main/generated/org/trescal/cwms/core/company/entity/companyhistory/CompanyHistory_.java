package org.trescal.cwms.core.company.entity.companyhistory;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CompanyHistory.class)
public abstract class CompanyHistory_ {

	public static volatile SingularAttribute<CompanyHistory, Company> comp;
	public static volatile SingularAttribute<CompanyHistory, Contact> changedBy;
	public static volatile SingularAttribute<CompanyHistory, String> coname;
	public static volatile SingularAttribute<CompanyHistory, Integer> id;
	public static volatile SingularAttribute<CompanyHistory, Date> validTo;

	public static final String COMP = "comp";
	public static final String CHANGED_BY = "changedBy";
	public static final String CONAME = "coname";
	public static final String ID = "id";
	public static final String VALID_TO = "validTo";

}

