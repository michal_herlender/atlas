package org.trescal.cwms.core.company.entity.mailgroupmember;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.mailgrouptype.MailGroupType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(MailGroupMember.class)
public abstract class MailGroupMember_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<MailGroupMember, MailGroupType> mailGroupType;
	public static volatile SingularAttribute<MailGroupMember, Contact> contact;
	public static volatile SingularAttribute<MailGroupMember, Integer> id;

	public static final String MAIL_GROUP_TYPE = "mailGroupType";
	public static final String CONTACT = "contact";
	public static final String ID = "id";

}

