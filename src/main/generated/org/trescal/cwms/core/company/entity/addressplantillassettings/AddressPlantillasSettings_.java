package org.trescal.cwms.core.company.entity.addressplantillassettings;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.address.Address;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AddressPlantillasSettings.class)
public abstract class AddressPlantillasSettings_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<AddressPlantillasSettings, Boolean> metraClient;
	public static volatile SingularAttribute<AddressPlantillasSettings, Address> address;
	public static volatile SingularAttribute<AddressPlantillasSettings, Boolean> sendByFtp;
	public static volatile SingularAttribute<AddressPlantillasSettings, Integer> formerCustomerId;
	public static volatile SingularAttribute<AddressPlantillasSettings, Address> managedByAddress;
	public static volatile SingularAttribute<AddressPlantillasSettings, String> ftpPassword;
	public static volatile SingularAttribute<AddressPlantillasSettings, Boolean> contract;
	public static volatile SingularAttribute<AddressPlantillasSettings, String> ftpServer;
	public static volatile SingularAttribute<AddressPlantillasSettings, String> ftpUser;
	public static volatile SingularAttribute<AddressPlantillasSettings, String> textUncertaintyFailure;
	public static volatile SingularAttribute<AddressPlantillasSettings, String> textToleranceFailure;
	public static volatile SingularAttribute<AddressPlantillasSettings, Address> certificateAddress;
	public static volatile SingularAttribute<AddressPlantillasSettings, Integer> id;
	public static volatile SingularAttribute<AddressPlantillasSettings, Boolean> nextDateOnCertificate;

	public static final String METRA_CLIENT = "metraClient";
	public static final String ADDRESS = "address";
	public static final String SEND_BY_FTP = "sendByFtp";
	public static final String FORMER_CUSTOMER_ID = "formerCustomerId";
	public static final String MANAGED_BY_ADDRESS = "managedByAddress";
	public static final String FTP_PASSWORD = "ftpPassword";
	public static final String CONTRACT = "contract";
	public static final String FTP_SERVER = "ftpServer";
	public static final String FTP_USER = "ftpUser";
	public static final String TEXT_UNCERTAINTY_FAILURE = "textUncertaintyFailure";
	public static final String TEXT_TOLERANCE_FAILURE = "textToleranceFailure";
	public static final String CERTIFICATE_ADDRESS = "certificateAddress";
	public static final String ID = "id";
	public static final String NEXT_DATE_ON_CERTIFICATE = "nextDateOnCertificate";

}

