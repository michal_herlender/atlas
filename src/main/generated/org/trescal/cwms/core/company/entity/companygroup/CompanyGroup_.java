package org.trescal.cwms.core.company.entity.companygroup;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.company.Company;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CompanyGroup.class)
public abstract class CompanyGroup_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SingularAttribute<CompanyGroup, String> groupName;
	public static volatile SetAttribute<CompanyGroup, Company> companies;
	public static volatile SingularAttribute<CompanyGroup, Boolean> active;
	public static volatile SingularAttribute<CompanyGroup, Integer> id;

	public static final String GROUP_NAME = "groupName";
	public static final String COMPANIES = "companies";
	public static final String ACTIVE = "active";
	public static final String ID = "id";

}

