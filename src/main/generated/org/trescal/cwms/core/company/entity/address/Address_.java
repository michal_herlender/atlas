package org.trescal.cwms.core.company.entity.address;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.addressplantillassettings.AddressPlantillasSettings;
import org.trescal.cwms.core.company.entity.addresssettings.AddressSettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.addresssettings.AddressSettingsForAllocatedSubdiv;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.country.Country;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.OnBehalfItem;
import org.trescal.cwms.core.system.entity.labelprinter.LabelPrinter;
import org.trescal.cwms.core.system.entity.printer.Printer;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Address.class)
public abstract class Address_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SingularAttribute<Address, Country> country;
	public static volatile SetAttribute<Address, OnBehalfItem> onBehalfJobItems;
	public static volatile SingularAttribute<Address, Subdiv> sub;
	public static volatile SingularAttribute<Address, BigDecimal> latitude;
	public static volatile SingularAttribute<Address, String> county;
	public static volatile SingularAttribute<Address, AddressValidator> validator;
	public static volatile SetAttribute<Address, Job> returnJobs;
	public static volatile SingularAttribute<Address, Date> deactTime;
	public static volatile SetAttribute<Address, Instrument> insts;
	public static volatile SingularAttribute<Address, LabelPrinter> defaultLabelPrinter;
	public static volatile SingularAttribute<Address, String> deactReason;
	public static volatile SetAttribute<Address, Department> departments;
	public static volatile SingularAttribute<Address, String> fax;
	public static volatile SingularAttribute<Address, BigDecimal> longitude;
	public static volatile SingularAttribute<Address, String> addr2;
	public static volatile SingularAttribute<Address, String> town;
	public static volatile SingularAttribute<Address, String> addr1;
	public static volatile SetAttribute<Address, AddressType> addressType;
	public static volatile SingularAttribute<Address, String> addr3;
	public static volatile SingularAttribute<Address, String> postcode;
	public static volatile SingularAttribute<Address, Boolean> active;
	public static volatile SingularAttribute<Address, String> telephone;
	public static volatile SingularAttribute<Address, Printer> defaultPrinter;
	public static volatile SingularAttribute<Address, String> costCentre;
	public static volatile SingularAttribute<Address, LocalDateTime> validatedOn;
	public static volatile SetAttribute<Address, AddressPlantillasSettings> addressPlantillasSettings;
	public static volatile SetAttribute<Address, AddressSettingsForAllocatedCompany> settingsForAllocatedCompanies;
	public static volatile SingularAttribute<Address, Integer> addrid;
	public static volatile SetAttribute<Address, Location> locations;
	public static volatile SetAttribute<Address, AddressSettingsForAllocatedSubdiv> settingsForAllocatedSubdivs;
	public static volatile SingularAttribute<Address, Contact> validatedBy;

	public static final String COUNTRY = "country";
	public static final String ON_BEHALF_JOB_ITEMS = "onBehalfJobItems";
	public static final String SUB = "sub";
	public static final String LATITUDE = "latitude";
	public static final String COUNTY = "county";
	public static final String VALIDATOR = "validator";
	public static final String RETURN_JOBS = "returnJobs";
	public static final String DEACT_TIME = "deactTime";
	public static final String INSTS = "insts";
	public static final String DEFAULT_LABEL_PRINTER = "defaultLabelPrinter";
	public static final String DEACT_REASON = "deactReason";
	public static final String DEPARTMENTS = "departments";
	public static final String FAX = "fax";
	public static final String LONGITUDE = "longitude";
	public static final String ADDR2 = "addr2";
	public static final String TOWN = "town";
	public static final String ADDR1 = "addr1";
	public static final String ADDRESS_TYPE = "addressType";
	public static final String ADDR3 = "addr3";
	public static final String POSTCODE = "postcode";
	public static final String ACTIVE = "active";
	public static final String TELEPHONE = "telephone";
	public static final String DEFAULT_PRINTER = "defaultPrinter";
	public static final String COST_CENTRE = "costCentre";
	public static final String VALIDATED_ON = "validatedOn";
	public static final String ADDRESS_PLANTILLAS_SETTINGS = "addressPlantillasSettings";
	public static final String SETTINGS_FOR_ALLOCATED_COMPANIES = "settingsForAllocatedCompanies";
	public static final String ADDRID = "addrid";
	public static final String LOCATIONS = "locations";
	public static final String SETTINGS_FOR_ALLOCATED_SUBDIVS = "settingsForAllocatedSubdivs";
	public static final String VALIDATED_BY = "validatedBy";

}

