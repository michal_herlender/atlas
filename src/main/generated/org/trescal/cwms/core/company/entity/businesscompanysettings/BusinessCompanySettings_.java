package org.trescal.cwms.core.company.entity.businesscompanysettings;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.businesscompanylogo.BusinessCompanyLogo;
import org.trescal.cwms.core.company.entity.company.Company;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BusinessCompanySettings.class)
public abstract class BusinessCompanySettings_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SingularAttribute<BusinessCompanySettings, String> capital;
	public static volatile SingularAttribute<BusinessCompanySettings, Integer> defaultQuotationDuration;
	public static volatile SingularAttribute<BusinessCompanySettings, BigDecimal> interbranchMarkupRate;
	public static volatile SingularAttribute<BusinessCompanySettings, String> legalRegistration2;
	public static volatile SingularAttribute<BusinessCompanySettings, Boolean> usePriceCatalog;
	public static volatile SingularAttribute<BusinessCompanySettings, Integer> warrantyRepaire;
	public static volatile SingularAttribute<BusinessCompanySettings, BigDecimal> interbranchDiscountRate;
	public static volatile SingularAttribute<BusinessCompanySettings, String> legalRegistration1;
	public static volatile SingularAttribute<BusinessCompanySettings, Boolean> useTaxableOption;
	public static volatile SingularAttribute<BusinessCompanySettings, InterbranchSalesMode> interbranchSalesMode;
	public static volatile SingularAttribute<BusinessCompanySettings, Boolean> usesPlantillas;
	public static volatile SingularAttribute<BusinessCompanySettings, Integer> warrantyCalibration;
	public static volatile SingularAttribute<BusinessCompanySettings, BusinessCompanyLogo> logo;
	public static volatile SingularAttribute<BusinessCompanySettings, Company> company;
	public static volatile SingularAttribute<BusinessCompanySettings, Integer> id;
	public static volatile SingularAttribute<BusinessCompanySettings, BigDecimal> hourlyRate;

	public static final String CAPITAL = "capital";
	public static final String DEFAULT_QUOTATION_DURATION = "defaultQuotationDuration";
	public static final String INTERBRANCH_MARKUP_RATE = "interbranchMarkupRate";
	public static final String LEGAL_REGISTRATION2 = "legalRegistration2";
	public static final String USE_PRICE_CATALOG = "usePriceCatalog";
	public static final String WARRANTY_REPAIRE = "warrantyRepaire";
	public static final String INTERBRANCH_DISCOUNT_RATE = "interbranchDiscountRate";
	public static final String LEGAL_REGISTRATION1 = "legalRegistration1";
	public static final String USE_TAXABLE_OPTION = "useTaxableOption";
	public static final String INTERBRANCH_SALES_MODE = "interbranchSalesMode";
	public static final String USES_PLANTILLAS = "usesPlantillas";
	public static final String WARRANTY_CALIBRATION = "warrantyCalibration";
	public static final String LOGO = "logo";
	public static final String COMPANY = "company";
	public static final String ID = "id";
	public static final String HOURLY_RATE = "hourlyRate";

}

