package org.trescal.cwms.core.company.entity.businesssubdivisionsettings;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BusinessSubdivisionSettings.class)
public abstract class BusinessSubdivisionSettings_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SingularAttribute<BusinessSubdivisionSettings, BigDecimal> ebitdaHourlyCostRate;
	public static volatile SingularAttribute<BusinessSubdivisionSettings, Contact> defaultBusinessContact;
	public static volatile SingularAttribute<BusinessSubdivisionSettings, Boolean> workInstructionReference;
	public static volatile SingularAttribute<BusinessSubdivisionSettings, Integer> id;
	public static volatile SingularAttribute<BusinessSubdivisionSettings, Subdiv> subdiv;
	public static volatile SingularAttribute<BusinessSubdivisionSettings, Integer> defaultTurnaround;

	public static final String EBITDA_HOURLY_COST_RATE = "ebitdaHourlyCostRate";
	public static final String DEFAULT_BUSINESS_CONTACT = "defaultBusinessContact";
	public static final String WORK_INSTRUCTION_REFERENCE = "workInstructionReference";
	public static final String ID = "id";
	public static final String SUBDIV = "subdiv";
	public static final String DEFAULT_TURNAROUND = "defaultTurnaround";

}

