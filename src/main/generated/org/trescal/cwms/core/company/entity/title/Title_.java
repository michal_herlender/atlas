package org.trescal.cwms.core.company.entity.title;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.country.Country;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Title.class)
public abstract class Title_ {

	public static volatile SingularAttribute<Title, Country> country;
	public static volatile SingularAttribute<Title, String> description;
	public static volatile SingularAttribute<Title, Integer> id;
	public static volatile SingularAttribute<Title, String> title;

	public static final String COUNTRY = "country";
	public static final String DESCRIPTION = "description";
	public static final String ID = "id";
	public static final String TITLE = "title";

}

