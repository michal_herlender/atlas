package org.trescal.cwms.core.company.entity.paymentmode;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PaymentMode.class)
public abstract class PaymentMode_ {

	public static volatile SingularAttribute<PaymentMode, String> code;
	public static volatile SetAttribute<PaymentMode, Translation> translations;
	public static volatile SingularAttribute<PaymentMode, String> description;
	public static volatile SingularAttribute<PaymentMode, Integer> id;

	public static final String CODE = "code";
	public static final String TRANSLATIONS = "translations";
	public static final String DESCRIPTION = "description";
	public static final String ID = "id";

}

