package org.trescal.cwms.core.company.entity.addresssettings;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AddressSettingsForAllocatedSubdiv.class)
public abstract class AddressSettingsForAllocatedSubdiv_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<AddressSettingsForAllocatedSubdiv, Address> address;
	public static volatile SingularAttribute<AddressSettingsForAllocatedSubdiv, Integer> id;
	public static volatile SingularAttribute<AddressSettingsForAllocatedSubdiv, TransportOption> transportOut;
	public static volatile SingularAttribute<AddressSettingsForAllocatedSubdiv, TransportOption> transportIn;

	public static final String ADDRESS = "address";
	public static final String ID = "id";
	public static final String TRANSPORT_OUT = "transportOut";
	public static final String TRANSPORT_IN = "transportIn";

}

