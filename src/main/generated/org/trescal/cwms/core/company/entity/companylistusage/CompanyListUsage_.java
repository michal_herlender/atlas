package org.trescal.cwms.core.company.entity.companylistusage;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companylist.CompanyList;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CompanyListUsage.class)
public abstract class CompanyListUsage_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SingularAttribute<CompanyListUsage, CompanyList> companyList;
	public static volatile SingularAttribute<CompanyListUsage, Company> company;
	public static volatile SingularAttribute<CompanyListUsage, Integer> id;

	public static final String COMPANY_LIST = "companyList";
	public static final String COMPANY = "company";
	public static final String ID = "id";

}

