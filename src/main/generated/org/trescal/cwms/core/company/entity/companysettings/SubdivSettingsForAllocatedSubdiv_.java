package org.trescal.cwms.core.company.entity.companysettings;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SubdivSettingsForAllocatedSubdiv.class)
public abstract class SubdivSettingsForAllocatedSubdiv_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<SubdivSettingsForAllocatedSubdiv, Contact> defaultBusinessContact;
	public static volatile SingularAttribute<SubdivSettingsForAllocatedSubdiv, Integer> id;
	public static volatile SingularAttribute<SubdivSettingsForAllocatedSubdiv, Subdiv> subdiv;

	public static final String DEFAULT_BUSINESS_CONTACT = "defaultBusinessContact";
	public static final String ID = "id";
	public static final String SUBDIV = "subdiv";

}

