package org.trescal.cwms.core.company.entity.businessdocumentsettings;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.company.Company;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BusinessDocumentSettings.class)
public abstract class BusinessDocumentSettings_ {

	public static volatile SingularAttribute<BusinessDocumentSettings, Boolean> subdivisionFiscalIdentifier;
	public static volatile SingularAttribute<BusinessDocumentSettings, Boolean> globalDefault;
	public static volatile SingularAttribute<BusinessDocumentSettings, Boolean> invoiceAmountText;
	public static volatile SingularAttribute<BusinessDocumentSettings, Boolean> recipientAddressOnLeft;
	public static volatile SingularAttribute<BusinessDocumentSettings, Boolean> deliveryNoteSignedCertificates;
	public static volatile SingularAttribute<BusinessDocumentSettings, Company> businessCompany;
	public static volatile SingularAttribute<BusinessDocumentSettings, Integer> id;

	public static final String SUBDIVISION_FISCAL_IDENTIFIER = "subdivisionFiscalIdentifier";
	public static final String GLOBAL_DEFAULT = "globalDefault";
	public static final String INVOICE_AMOUNT_TEXT = "invoiceAmountText";
	public static final String RECIPIENT_ADDRESS_ON_LEFT = "recipientAddressOnLeft";
	public static final String DELIVERY_NOTE_SIGNED_CERTIFICATES = "deliveryNoteSignedCertificates";
	public static final String BUSINESS_COMPANY = "businessCompany";
	public static final String ID = "id";

}

