package org.trescal.cwms.core.company.entity.salesdiscount;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SalesDiscount.class)
public abstract class SalesDiscount_ {

	public static volatile SingularAttribute<SalesDiscount, String> code;
	public static volatile SingularAttribute<SalesDiscount, BigDecimal> percent;
	public static volatile SingularAttribute<SalesDiscount, String> userid;
	public static volatile SetAttribute<SalesDiscount, Contact> contacts;

	public static final String CODE = "code";
	public static final String PERCENT = "percent";
	public static final String USERID = "userid";
	public static final String CONTACTS = "contacts";

}

