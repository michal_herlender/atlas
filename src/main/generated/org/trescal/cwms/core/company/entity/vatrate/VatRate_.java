package org.trescal.cwms.core.company.entity.vatrate;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.country.Country;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VatRate.class)
public abstract class VatRate_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<VatRate, Country> country;
	public static volatile SingularAttribute<VatRate, BigDecimal> rate;
	public static volatile SetAttribute<VatRate, CompanySettingsForAllocatedCompany> settingsForAllocatedCompanies;
	public static volatile SingularAttribute<VatRate, String> description;
	public static volatile SingularAttribute<VatRate, VatRateType> type;
	public static volatile SingularAttribute<VatRate, String> vatCode;

	public static final String COUNTRY = "country";
	public static final String RATE = "rate";
	public static final String SETTINGS_FOR_ALLOCATED_COMPANIES = "settingsForAllocatedCompanies";
	public static final String DESCRIPTION = "description";
	public static final String TYPE = "type";
	public static final String VAT_CODE = "vatCode";

}

