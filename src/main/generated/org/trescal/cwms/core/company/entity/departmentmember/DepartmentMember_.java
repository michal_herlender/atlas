package org.trescal.cwms.core.company.entity.departmentmember;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.departmentrole.DepartmentRole;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(DepartmentMember.class)
public abstract class DepartmentMember_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<DepartmentMember, Contact> contact;
	public static volatile SingularAttribute<DepartmentMember, DepartmentRole> departmentRole;
	public static volatile SingularAttribute<DepartmentMember, Integer> id;
	public static volatile SingularAttribute<DepartmentMember, Department> department;

	public static final String CONTACT = "contact";
	public static final String DEPARTMENT_ROLE = "departmentRole";
	public static final String ID = "id";
	public static final String DEPARTMENT = "department";

}

