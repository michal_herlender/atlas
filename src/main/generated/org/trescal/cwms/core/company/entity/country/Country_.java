package org.trescal.cwms.core.company.entity.country;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.addressprintformat.AddressPrintFormat;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.title.Title;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Country.class)
public abstract class Country_ {

	public static volatile SingularAttribute<Country, String> country;
	public static volatile SingularAttribute<Country, AddressPrintFormat> addressPrintFormat;
	public static volatile SetAttribute<Country, Company> companies;
	public static volatile SingularAttribute<Country, String> countryCode;
	public static volatile SetAttribute<Country, Title> titles;
	public static volatile SingularAttribute<Country, Boolean> memberOfUE;
	public static volatile SingularAttribute<Country, Integer> countryid;

	public static final String COUNTRY = "country";
	public static final String ADDRESS_PRINT_FORMAT = "addressPrintFormat";
	public static final String COMPANIES = "companies";
	public static final String COUNTRY_CODE = "countryCode";
	public static final String TITLES = "titles";
	public static final String MEMBER_OF_UE = "memberOfUE";
	public static final String COUNTRYID = "countryid";

}

