package org.trescal.cwms.core.company.entity.addresssettings;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.vatrate.VatRate;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AddressSettingsForAllocatedCompany.class)
public abstract class AddressSettingsForAllocatedCompany_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<AddressSettingsForAllocatedCompany, Address> address;
	public static volatile SingularAttribute<AddressSettingsForAllocatedCompany, VatRate> vatRate;
	public static volatile SingularAttribute<AddressSettingsForAllocatedCompany, Integer> id;

	public static final String ADDRESS = "address";
	public static final String VAT_RATE = "vatRate";
	public static final String ID = "id";

}

