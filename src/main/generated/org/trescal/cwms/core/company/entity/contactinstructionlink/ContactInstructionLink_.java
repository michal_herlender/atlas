package org.trescal.cwms.core.company.entity.contactinstructionlink;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.entity.instruction.Instruction;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ContactInstructionLink.class)
public abstract class ContactInstructionLink_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<ContactInstructionLink, Instruction> instruction;
	public static volatile SingularAttribute<ContactInstructionLink, Contact> contact;
	public static volatile SingularAttribute<ContactInstructionLink, Integer> id;

	public static final String INSTRUCTION = "instruction";
	public static final String CONTACT = "contact";
	public static final String ID = "id";

}

