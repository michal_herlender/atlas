package org.trescal.cwms.core.company.entity.subdivinstructionlink;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.instruction.Instruction;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SubdivInstructionLink.class)
public abstract class SubdivInstructionLink_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<SubdivInstructionLink, Boolean> subdivInstruction;
	public static volatile SingularAttribute<SubdivInstructionLink, Instruction> instruction;
	public static volatile SingularAttribute<SubdivInstructionLink, Subdiv> businessSubdiv;
	public static volatile SingularAttribute<SubdivInstructionLink, Integer> id;
	public static volatile SingularAttribute<SubdivInstructionLink, Subdiv> subdiv;

	public static final String SUBDIV_INSTRUCTION = "subdivInstruction";
	public static final String INSTRUCTION = "instruction";
	public static final String BUSINESS_SUBDIV = "businessSubdiv";
	public static final String ID = "id";
	public static final String SUBDIV = "subdiv";

}

