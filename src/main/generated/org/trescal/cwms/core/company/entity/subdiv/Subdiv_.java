package org.trescal.cwms.core.company.entity.subdiv;

import java.time.LocalDate;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.businesssubdivisionsettings.BusinessSubdivisionSettings;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companysettings.SubdivSettingsForAllocatedSubdiv;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.subdivinstructionlink.SubdivInstructionLink;
import org.trescal.cwms.core.contract.entity.contract.Contract;
import org.trescal.cwms.core.external.adveso.entity.advesooverriddenactivitysetting.AdvesoOverriddenActivitySetting;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.system.entity.systemdefaultapplication.SystemDefaultApplication;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Subdiv.class)
public abstract class Subdiv_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SetAttribute<Subdiv, SubdivInstructionLink> instructions;
	public static volatile SetAttribute<Subdiv, Address> addresses;
	public static volatile SingularAttribute<Subdiv, String> siretNumber;
	public static volatile SingularAttribute<Subdiv, String> fiscalIdentifier;
	public static volatile ListAttribute<Subdiv, Contract> contracts;
	public static volatile ListAttribute<Subdiv, BPO> BPOs;
	public static volatile SingularAttribute<Subdiv, Integer> accContact;
	public static volatile SingularAttribute<Subdiv, Date> deactTime;
	public static volatile SingularAttribute<Subdiv, String> deactReason;
	public static volatile SetAttribute<Subdiv, Department> departments;
	public static volatile SingularAttribute<Subdiv, LocalDate> createDate;
	public static volatile SingularAttribute<Subdiv, Integer> subdivid;
	public static volatile SingularAttribute<Subdiv, Company> comp;
	public static volatile SingularAttribute<Subdiv, BusinessSubdivisionSettings> businessSettings;
	public static volatile SingularAttribute<Subdiv, String> subname;
	public static volatile SingularAttribute<Subdiv, Boolean> active;
	public static volatile SingularAttribute<Subdiv, String> subdivCode;
	public static volatile SingularAttribute<Subdiv, String> formerId;
	public static volatile SingularAttribute<Subdiv, Contact> defaultContact;
	public static volatile ListAttribute<Subdiv, AdvesoOverriddenActivitySetting> advesoOverriddenActivitiesSetting;
	public static volatile ListAttribute<Subdiv, SubdivSettingsForAllocatedSubdiv> subdivSettings;
	public static volatile SingularAttribute<Subdiv, String> analyticalCenter;
	public static volatile SetAttribute<Subdiv, SystemDefaultApplication> systemDefaults;
	public static volatile SetAttribute<Subdiv, Contact> contacts;
	public static volatile SingularAttribute<Subdiv, Address> defaultAddress;

	public static final String INSTRUCTIONS = "instructions";
	public static final String ADDRESSES = "addresses";
	public static final String SIRET_NUMBER = "siretNumber";
	public static final String FISCAL_IDENTIFIER = "fiscalIdentifier";
	public static final String CONTRACTS = "contracts";
	public static final String B_POS = "BPOs";
	public static final String ACC_CONTACT = "accContact";
	public static final String DEACT_TIME = "deactTime";
	public static final String DEACT_REASON = "deactReason";
	public static final String DEPARTMENTS = "departments";
	public static final String CREATE_DATE = "createDate";
	public static final String SUBDIVID = "subdivid";
	public static final String COMP = "comp";
	public static final String BUSINESS_SETTINGS = "businessSettings";
	public static final String SUBNAME = "subname";
	public static final String ACTIVE = "active";
	public static final String SUBDIV_CODE = "subdivCode";
	public static final String FORMER_ID = "formerId";
	public static final String DEFAULT_CONTACT = "defaultContact";
	public static final String ADVESO_OVERRIDDEN_ACTIVITIES_SETTING = "advesoOverriddenActivitiesSetting";
	public static final String SUBDIV_SETTINGS = "subdivSettings";
	public static final String ANALYTICAL_CENTER = "analyticalCenter";
	public static final String SYSTEM_DEFAULTS = "systemDefaults";
	public static final String CONTACTS = "contacts";
	public static final String DEFAULT_ADDRESS = "defaultAddress";

}

