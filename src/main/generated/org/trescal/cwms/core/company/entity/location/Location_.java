package org.trescal.cwms.core.company.entity.location;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Location.class)
public abstract class Location_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<Location, Address> add;
	public static volatile SetAttribute<Location, Instrument> instrums;
	public static volatile SingularAttribute<Location, Integer> locationid;
	public static volatile SingularAttribute<Location, Boolean> active;
	public static volatile SingularAttribute<Location, String> location;
	public static volatile SetAttribute<Location, Contact> contacts;

	public static final String ADD = "add";
	public static final String INSTRUMS = "instrums";
	public static final String LOCATIONID = "locationid";
	public static final String ACTIVE = "active";
	public static final String LOCATION = "location";
	public static final String CONTACTS = "contacts";

}

