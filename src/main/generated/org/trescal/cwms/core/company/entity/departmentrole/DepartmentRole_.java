package org.trescal.cwms.core.company.entity.departmentrole;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.departmentmember.DepartmentMember;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(DepartmentRole.class)
public abstract class DepartmentRole_ {

	public static volatile SingularAttribute<DepartmentRole, String> role;
	public static volatile SingularAttribute<DepartmentRole, String> description;
	public static volatile SingularAttribute<DepartmentRole, Boolean> active;
	public static volatile SingularAttribute<DepartmentRole, Integer> id;
	public static volatile SetAttribute<DepartmentRole, DepartmentMember> departmentMembers;
	public static volatile SingularAttribute<DepartmentRole, Boolean> manage;

	public static final String ROLE = "role";
	public static final String DESCRIPTION = "description";
	public static final String ACTIVE = "active";
	public static final String ID = "id";
	public static final String DEPARTMENT_MEMBERS = "departmentMembers";
	public static final String MANAGE = "manage";

}

