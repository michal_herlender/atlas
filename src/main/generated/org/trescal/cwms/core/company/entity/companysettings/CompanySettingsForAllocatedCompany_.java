package org.trescal.cwms.core.company.entity.companysettings;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.account.entity.bankaccount.BankAccount;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companystatus.CompanyStatus;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.invoicefrequency.InvoiceFrequency;
import org.trescal.cwms.core.company.entity.paymentmode.PaymentMode;
import org.trescal.cwms.core.company.entity.paymentterms.PaymentTerm;
import org.trescal.cwms.core.company.entity.vatrate.VatRate;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CompanySettingsForAllocatedCompany.class)
public abstract class CompanySettingsForAllocatedCompany_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<CompanySettingsForAllocatedCompany, BankAccount> bankAccount;
	public static volatile SingularAttribute<CompanySettingsForAllocatedCompany, String> onStopReason;
	public static volatile SingularAttribute<CompanySettingsForAllocatedCompany, Boolean> syncToPlantillas;
	public static volatile SingularAttribute<CompanySettingsForAllocatedCompany, Boolean> taxable;
	public static volatile SingularAttribute<CompanySettingsForAllocatedCompany, String> softwareAccountNumber;
	public static volatile SingularAttribute<CompanySettingsForAllocatedCompany, PaymentMode> paymentMode;
	public static volatile SingularAttribute<CompanySettingsForAllocatedCompany, Boolean> invoiceFactoring;
	public static volatile SingularAttribute<CompanySettingsForAllocatedCompany, Boolean> poRequired;
	public static volatile SingularAttribute<CompanySettingsForAllocatedCompany, Boolean> contract;
	public static volatile SingularAttribute<CompanySettingsForAllocatedCompany, Boolean> active;
	public static volatile SingularAttribute<CompanySettingsForAllocatedCompany, String> companyReference;
	public static volatile SingularAttribute<CompanySettingsForAllocatedCompany, PaymentTerm> paymentterm;
	public static volatile SingularAttribute<CompanySettingsForAllocatedCompany, Contact> onStopBy;
	public static volatile SingularAttribute<CompanySettingsForAllocatedCompany, Boolean> requireSupplierCompanyList;
	public static volatile SingularAttribute<CompanySettingsForAllocatedCompany, VatRate> vatrate;
	public static volatile SingularAttribute<CompanySettingsForAllocatedCompany, BigDecimal> creditLimit;
	public static volatile SingularAttribute<CompanySettingsForAllocatedCompany, Company> company;
	public static volatile SingularAttribute<CompanySettingsForAllocatedCompany, String> deactReason;
	public static volatile SingularAttribute<CompanySettingsForAllocatedCompany, Integer> id;
	public static volatile SingularAttribute<CompanySettingsForAllocatedCompany, LocalDate> onStopSince;
	public static volatile SingularAttribute<CompanySettingsForAllocatedCompany, InvoiceFrequency> invoiceFrequency;
	public static volatile SingularAttribute<CompanySettingsForAllocatedCompany, Boolean> onStop;
	public static volatile SingularAttribute<CompanySettingsForAllocatedCompany, Date> deactDate;
	public static volatile SingularAttribute<CompanySettingsForAllocatedCompany, CompanyStatus> status;

	public static final String BANK_ACCOUNT = "bankAccount";
	public static final String ON_STOP_REASON = "onStopReason";
	public static final String SYNC_TO_PLANTILLAS = "syncToPlantillas";
	public static final String TAXABLE = "taxable";
	public static final String SOFTWARE_ACCOUNT_NUMBER = "softwareAccountNumber";
	public static final String PAYMENT_MODE = "paymentMode";
	public static final String INVOICE_FACTORING = "invoiceFactoring";
	public static final String PO_REQUIRED = "poRequired";
	public static final String CONTRACT = "contract";
	public static final String ACTIVE = "active";
	public static final String COMPANY_REFERENCE = "companyReference";
	public static final String PAYMENTTERM = "paymentterm";
	public static final String ON_STOP_BY = "onStopBy";
	public static final String REQUIRE_SUPPLIER_COMPANY_LIST = "requireSupplierCompanyList";
	public static final String VATRATE = "vatrate";
	public static final String CREDIT_LIMIT = "creditLimit";
	public static final String COMPANY = "company";
	public static final String DEACT_REASON = "deactReason";
	public static final String ID = "id";
	public static final String ON_STOP_SINCE = "onStopSince";
	public static final String INVOICE_FREQUENCY = "invoiceFrequency";
	public static final String ON_STOP = "onStop";
	public static final String DEACT_DATE = "deactDate";
	public static final String STATUS = "status";

}

