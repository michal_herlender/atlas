package org.trescal.cwms.core.company.entity.businessarea;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BusinessArea.class)
public abstract class BusinessArea_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SetAttribute<BusinessArea, Company> companies;
	public static volatile SetAttribute<BusinessArea, Translation> descriptiontranslation;
	public static volatile SingularAttribute<BusinessArea, String> name;
	public static volatile SingularAttribute<BusinessArea, Integer> businessareaid;
	public static volatile SingularAttribute<BusinessArea, String> description;
	public static volatile SetAttribute<BusinessArea, Translation> nametranslation;

	public static final String COMPANIES = "companies";
	public static final String DESCRIPTIONTRANSLATION = "descriptiontranslation";
	public static final String NAME = "name";
	public static final String BUSINESSAREAID = "businessareaid";
	public static final String DESCRIPTION = "description";
	public static final String NAMETRANSLATION = "nametranslation";

}

