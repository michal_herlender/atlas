package org.trescal.cwms.core.company.entity.department;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.departmentmember.DepartmentMember;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.WorkRequirement;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Department.class)
public abstract class Department_ {

	public static volatile SingularAttribute<Department, Address> address;
	public static volatile SetAttribute<Department, DepartmentMember> members;
	public static volatile SetAttribute<Department, WorkRequirement> workRequirements;
	public static volatile SingularAttribute<Department, Integer> deptid;
	public static volatile SingularAttribute<Department, String> name;
	public static volatile SetAttribute<Department, CapabilityCategory> categories;
	public static volatile SingularAttribute<Department, String> shortName;
	public static volatile SingularAttribute<Department, DepartmentType> type;
	public static volatile SingularAttribute<Department, Subdiv> subdiv;

	public static final String ADDRESS = "address";
	public static final String MEMBERS = "members";
	public static final String WORK_REQUIREMENTS = "workRequirements";
	public static final String DEPTID = "deptid";
	public static final String NAME = "name";
	public static final String CATEGORIES = "categories";
	public static final String SHORT_NAME = "shortName";
	public static final String TYPE = "type";
	public static final String SUBDIV = "subdiv";

}

