package org.trescal.cwms.core.company.entity.company;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Locale;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.account.entity.bankaccount.BankAccount;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.businessarea.BusinessArea;
import org.trescal.cwms.core.company.entity.businesscompanysettings.BusinessCompanySettings;
import org.trescal.cwms.core.company.entity.companyaccreditation.CompanyAccreditation;
import org.trescal.cwms.core.company.entity.companygroup.CompanyGroup;
import org.trescal.cwms.core.company.entity.companyhistory.CompanyHistory;
import org.trescal.cwms.core.company.entity.companyinstructionlink.CompanyInstructionLink;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.country.Country;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.documents.images.systemimage.SystemImage;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentFieldDefinition;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.OnBehalfItem;
import org.trescal.cwms.core.login.entity.portal.usergroup.UserGroup;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.systemdefaultapplication.SystemDefaultApplication;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Company.class)
public abstract class Company_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SingularAttribute<Company, Country> country;
	public static volatile SetAttribute<Company, CompanyInstructionLink> instructions;
	public static volatile SetAttribute<Company, OnBehalfItem> onBehalfJobItems;
	public static volatile SingularAttribute<Company, String> coname;
	public static volatile SingularAttribute<Company, String> fiscalIdentifier;
	public static volatile ListAttribute<Company, BankAccount> bankAccounts;
	public static volatile ListAttribute<Company, BPO> BPOs;
	public static volatile SingularAttribute<Company, Address> legalAddress;
	public static volatile SingularAttribute<Company, String> formerID;
	public static volatile SetAttribute<Company, Invoice> invoices;
	public static volatile SingularAttribute<Company, BigDecimal> rate;
	public static volatile SingularAttribute<Company, SystemImage> logo;
	public static volatile SingularAttribute<Company, SupportedCurrency> currency;
	public static volatile SingularAttribute<Company, LocalDate> createDate;
	public static volatile SingularAttribute<Company, String> companyCode;
	public static volatile SingularAttribute<Company, BusinessArea> businessarea;
	public static volatile SetAttribute<Company, CompanyAccreditation> companyAccreditations;
	public static volatile SingularAttribute<Company, CompanyRole> companyRole;
	public static volatile SingularAttribute<Company, Subdiv> defaultSubdiv;
	public static volatile SingularAttribute<Company, BusinessCompanySettings> businessSettings;
	public static volatile SingularAttribute<Company, Boolean> partnerComp;
	public static volatile SingularAttribute<Company, CompanyGroup> companyGroup;
	public static volatile SingularAttribute<Company, Integer> coid;
	public static volatile SetAttribute<Company, CompanyHistory> histories;
	public static volatile SingularAttribute<Company, String> legalIdentifier;
	public static volatile SetAttribute<Company, InstrumentFieldDefinition> instrumentFieldDefinition;
	public static volatile SetAttribute<Company, Instrument> instrums;
	public static volatile SingularAttribute<Company, String> groupName;
	public static volatile SingularAttribute<Company, Locale> documentLanguage;
	public static volatile SetAttribute<Company, UserGroup> usergroups;
	public static volatile SingularAttribute<Company, Contact> defaultBusinessContact;
	public static volatile SetAttribute<Company, CompanySettingsForAllocatedCompany> settingsForAllocatedCompanies;
	public static volatile SetAttribute<Company, SystemDefaultApplication> systemDefaults;
	public static volatile SetAttribute<Company, Subdiv> subdivisions;

	public static final String COUNTRY = "country";
	public static final String INSTRUCTIONS = "instructions";
	public static final String ON_BEHALF_JOB_ITEMS = "onBehalfJobItems";
	public static final String CONAME = "coname";
	public static final String FISCAL_IDENTIFIER = "fiscalIdentifier";
	public static final String BANK_ACCOUNTS = "bankAccounts";
	public static final String B_POS = "BPOs";
	public static final String LEGAL_ADDRESS = "legalAddress";
	public static final String FORMER_ID = "formerID";
	public static final String INVOICES = "invoices";
	public static final String RATE = "rate";
	public static final String LOGO = "logo";
	public static final String CURRENCY = "currency";
	public static final String CREATE_DATE = "createDate";
	public static final String COMPANY_CODE = "companyCode";
	public static final String BUSINESSAREA = "businessarea";
	public static final String COMPANY_ACCREDITATIONS = "companyAccreditations";
	public static final String COMPANY_ROLE = "companyRole";
	public static final String DEFAULT_SUBDIV = "defaultSubdiv";
	public static final String BUSINESS_SETTINGS = "businessSettings";
	public static final String PARTNER_COMP = "partnerComp";
	public static final String COMPANY_GROUP = "companyGroup";
	public static final String COID = "coid";
	public static final String HISTORIES = "histories";
	public static final String LEGAL_IDENTIFIER = "legalIdentifier";
	public static final String INSTRUMENT_FIELD_DEFINITION = "instrumentFieldDefinition";
	public static final String INSTRUMS = "instrums";
	public static final String GROUP_NAME = "groupName";
	public static final String DOCUMENT_LANGUAGE = "documentLanguage";
	public static final String USERGROUPS = "usergroups";
	public static final String DEFAULT_BUSINESS_CONTACT = "defaultBusinessContact";
	public static final String SETTINGS_FOR_ALLOCATED_COMPANIES = "settingsForAllocatedCompanies";
	public static final String SYSTEM_DEFAULTS = "systemDefaults";
	public static final String SUBDIVISIONS = "subdivisions";

}

