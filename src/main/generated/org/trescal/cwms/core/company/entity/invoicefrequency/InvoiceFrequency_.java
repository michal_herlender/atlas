package org.trescal.cwms.core.company.entity.invoicefrequency;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InvoiceFrequency.class)
public abstract class InvoiceFrequency_ {

	public static volatile SingularAttribute<InvoiceFrequency, String> code;
	public static volatile SetAttribute<InvoiceFrequency, Translation> translations;
	public static volatile SingularAttribute<InvoiceFrequency, String> description;
	public static volatile SingularAttribute<InvoiceFrequency, Integer> id;

	public static final String CODE = "code";
	public static final String TRANSLATIONS = "translations";
	public static final String DESCRIPTION = "description";
	public static final String ID = "id";

}

