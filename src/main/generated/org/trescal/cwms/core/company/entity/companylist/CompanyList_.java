package org.trescal.cwms.core.company.entity.companylist;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CompanyList.class)
public abstract class CompanyList_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<CompanyList, String> name;
	public static volatile SingularAttribute<CompanyList, Boolean> active;
	public static volatile SingularAttribute<CompanyList, Integer> id;

	public static final String NAME = "name";
	public static final String ACTIVE = "active";
	public static final String ID = "id";

}

