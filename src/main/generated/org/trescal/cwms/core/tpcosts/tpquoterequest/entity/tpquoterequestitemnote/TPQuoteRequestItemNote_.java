package org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitemnote;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.TPQuoteRequestItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TPQuoteRequestItemNote.class)
public abstract class TPQuoteRequestItemNote_ extends org.trescal.cwms.core.system.entity.note.Note_ {

	public static volatile SingularAttribute<TPQuoteRequestItemNote, TPQuoteRequestItem> tpQuoteRequestItem;

	public static final String TP_QUOTE_REQUEST_ITEM = "tpQuoteRequestItem";

}

