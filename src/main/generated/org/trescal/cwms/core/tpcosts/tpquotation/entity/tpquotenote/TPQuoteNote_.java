package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotenote;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TPQuoteNote.class)
public abstract class TPQuoteNote_ extends org.trescal.cwms.core.system.entity.note.Note_ {

	public static volatile SingularAttribute<TPQuoteNote, TPQuotation> tpquotation;

	public static final String TPQUOTATION = "tpquotation";

}

