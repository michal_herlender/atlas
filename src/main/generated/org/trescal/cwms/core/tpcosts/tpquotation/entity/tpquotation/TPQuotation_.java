package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationcaltypedefault.TPQuotationCaltypeDefault;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotenote.TPQuoteNote;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotestatus.TPQuoteStatus;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TPQuotation.class)
public abstract class TPQuotation_ extends org.trescal.cwms.core.pricing.entity.pricings.pricing.Pricing_ {

	public static volatile SetAttribute<TPQuotation, TPQuotationCaltypeDefault> defaultCalTypes;
	public static volatile SingularAttribute<TPQuotation, String> tpqno;
	public static volatile SingularAttribute<TPQuotation, TPQuoteRequest> fromRequest;
	public static volatile SetAttribute<TPQuotation, TPQuoteNote> notes;
	public static volatile SingularAttribute<TPQuotation, String> qno;
	public static volatile SingularAttribute<TPQuotation, Integer> defaultQty;
	public static volatile SetAttribute<TPQuotation, TPQuotationItem> items;
	public static volatile SingularAttribute<TPQuotation, TPQuoteStatus> status;

	public static final String DEFAULT_CAL_TYPES = "defaultCalTypes";
	public static final String TPQNO = "tpqno";
	public static final String FROM_REQUEST = "fromRequest";
	public static final String NOTES = "notes";
	public static final String QNO = "qno";
	public static final String DEFAULT_QTY = "defaultQty";
	public static final String ITEMS = "items";
	public static final String STATUS = "status";

}

