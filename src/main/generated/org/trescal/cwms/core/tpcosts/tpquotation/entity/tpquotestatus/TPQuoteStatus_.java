package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotestatus;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TPQuoteStatus.class)
public abstract class TPQuoteStatus_ extends org.trescal.cwms.core.system.entity.status.Status_ {

	public static volatile SetAttribute<TPQuoteStatus, TPQuotation> tpquotations;

	public static final String TPQUOTATIONS = "tpquotations";

}

