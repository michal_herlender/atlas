package org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestnote;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TPQuoteRequestNote.class)
public abstract class TPQuoteRequestNote_ extends org.trescal.cwms.core.system.entity.note.Note_ {

	public static volatile SingularAttribute<TPQuoteRequestNote, TPQuoteRequest> tpQuoteRequest;

	public static final String TP_QUOTE_REQUEST = "tpQuoteRequest";

}

