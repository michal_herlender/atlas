package org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.repcost;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.repcost.JobCostingRepairCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TPQuotationRepairCost.class)
public abstract class TPQuotationRepairCost_ extends org.trescal.cwms.core.pricing.entity.costs.base.RepCost_ {

	public static volatile SetAttribute<TPQuotationRepairCost, JobCostingRepairCost> linkedJobRepCosts;
	public static volatile SingularAttribute<TPQuotationRepairCost, TPQuotationItem> tpQuoteItem;

	public static final String LINKED_JOB_REP_COSTS = "linkedJobRepCosts";
	public static final String TP_QUOTE_ITEM = "tpQuoteItem";

}

