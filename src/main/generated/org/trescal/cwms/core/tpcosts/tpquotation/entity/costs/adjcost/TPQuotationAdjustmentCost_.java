package org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.adjcost;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.adjcost.JobCostingAdjustmentCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TPQuotationAdjustmentCost.class)
public abstract class TPQuotationAdjustmentCost_ extends org.trescal.cwms.core.pricing.entity.costs.base.AdjCost_ {

	public static volatile SetAttribute<TPQuotationAdjustmentCost, JobCostingAdjustmentCost> linkedJobAdjCosts;
	public static volatile SingularAttribute<TPQuotationAdjustmentCost, TPQuotationItem> tpQuoteItem;

	public static final String LINKED_JOB_ADJ_COSTS = "linkedJobAdjCosts";
	public static final String TP_QUOTE_ITEM = "tpQuoteItem";

}

