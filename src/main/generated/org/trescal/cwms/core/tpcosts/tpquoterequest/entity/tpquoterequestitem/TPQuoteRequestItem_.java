package org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitemnote.TPQuoteRequestItemNote;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TPQuoteRequestItem.class)
public abstract class TPQuoteRequestItem_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<TPQuoteRequestItem, Quotationitem> requestFromQuoteItem;
	public static volatile SetAttribute<TPQuoteRequestItem, TPQuoteRequestItemNote> notes;
	public static volatile SingularAttribute<TPQuoteRequestItem, String> referenceNo;
	public static volatile SingularAttribute<TPQuoteRequestItem, Boolean> partOfBaseUnit;
	public static volatile SingularAttribute<TPQuoteRequestItem, TPQuoteRequest> tpQuoteRequest;
	public static volatile SetAttribute<TPQuoteRequestItem, TPQuoteRequestItem> modules;
	public static volatile SingularAttribute<TPQuoteRequestItem, TPQuoteRequestItem> baseUnit;
	public static volatile SingularAttribute<TPQuoteRequestItem, Integer> qty;
	public static volatile SingularAttribute<TPQuoteRequestItem, InstrumentModel> model;
	public static volatile SingularAttribute<TPQuoteRequestItem, Integer> id;
	public static volatile SetAttribute<TPQuoteRequestItem, CostType> costTypes;
	public static volatile SingularAttribute<TPQuoteRequestItem, JobItem> requestFromJobItem;
	public static volatile SingularAttribute<TPQuoteRequestItem, CalibrationType> caltype;

	public static final String REQUEST_FROM_QUOTE_ITEM = "requestFromQuoteItem";
	public static final String NOTES = "notes";
	public static final String REFERENCE_NO = "referenceNo";
	public static final String PART_OF_BASE_UNIT = "partOfBaseUnit";
	public static final String TP_QUOTE_REQUEST = "tpQuoteRequest";
	public static final String MODULES = "modules";
	public static final String BASE_UNIT = "baseUnit";
	public static final String QTY = "qty";
	public static final String MODEL = "model";
	public static final String ID = "id";
	public static final String COST_TYPES = "costTypes";
	public static final String REQUEST_FROM_JOB_ITEM = "requestFromJobItem";
	public static final String CALTYPE = "caltype";

}

