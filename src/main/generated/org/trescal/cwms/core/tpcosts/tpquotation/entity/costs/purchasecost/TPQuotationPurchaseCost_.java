package org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.purchasecost;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.purchasecost.JobCostingPurchaseCost;
import org.trescal.cwms.core.quotation.entity.costs.purchasecost.QuotationPurchaseCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TPQuotationPurchaseCost.class)
public abstract class TPQuotationPurchaseCost_ extends org.trescal.cwms.core.pricing.entity.costs.base.PurchaseCost_ {

	public static volatile SetAttribute<TPQuotationPurchaseCost, JobCostingPurchaseCost> linkedJobCalCosts;
	public static volatile SingularAttribute<TPQuotationPurchaseCost, TPQuotationItem> tpQuoteItem;
	public static volatile SetAttribute<TPQuotationPurchaseCost, QuotationPurchaseCost> linkedQuoteCalCosts;

	public static final String LINKED_JOB_CAL_COSTS = "linkedJobCalCosts";
	public static final String TP_QUOTE_ITEM = "tpQuoteItem";
	public static final String LINKED_QUOTE_CAL_COSTS = "linkedQuoteCalCosts";

}

