package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationcaltypedefault;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TPQuotationCaltypeDefault.class)
public abstract class TPQuotationCaltypeDefault_ {

	public static volatile SingularAttribute<TPQuotationCaltypeDefault, TPQuotation> tpquotation;
	public static volatile SingularAttribute<TPQuotationCaltypeDefault, Integer> id;
	public static volatile SingularAttribute<TPQuotationCaltypeDefault, CalibrationType> caltype;

	public static final String TPQUOTATION = "tpquotation";
	public static final String ID = "id";
	public static final String CALTYPE = "caltype";

}

