package org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequeststatus;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TPQuoteRequestStatus.class)
public abstract class TPQuoteRequestStatus_ extends org.trescal.cwms.core.system.entity.status.Status_ {

	public static volatile SetAttribute<TPQuoteRequestStatus, TPQuoteRequest> tpQuoteRequests;

	public static final String TP_QUOTE_REQUESTS = "tpQuoteRequests";

}

