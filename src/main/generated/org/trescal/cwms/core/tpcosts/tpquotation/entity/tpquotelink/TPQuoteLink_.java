package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotelink;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TPQuoteLink.class)
public abstract class TPQuoteLink_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<TPQuoteLink, LocalDate> date;
	public static volatile SingularAttribute<TPQuoteLink, JobItem> jobItem;
	public static volatile SingularAttribute<TPQuoteLink, Contact> contact;
	public static volatile SingularAttribute<TPQuoteLink, TPQuotationItem> tpQuoteItem;
	public static volatile SingularAttribute<TPQuoteLink, String> comment;
	public static volatile SingularAttribute<TPQuoteLink, Integer> id;

	public static final String DATE = "date";
	public static final String JOB_ITEM = "jobItem";
	public static final String CONTACT = "contact";
	public static final String TP_QUOTE_ITEM = "tpQuoteItem";
	public static final String COMMENT = "comment";
	public static final String ID = "id";

}

