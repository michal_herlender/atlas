package org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.TPQuoteRequestItem;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestnote.TPQuoteRequestNote;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequeststatus.TPQuoteRequestStatus;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TPQuoteRequest.class)
public abstract class TPQuoteRequest_ extends org.trescal.cwms.core.pricing.entity.pricings.genericpricingentity.GenericPricingEntity_ {

	public static volatile SetAttribute<TPQuoteRequest, TPQuoteRequestNote> notes;
	public static volatile SingularAttribute<TPQuoteRequest, Job> requestFromJob;
	public static volatile SingularAttribute<TPQuoteRequest, Contact> contact;
	public static volatile SingularAttribute<TPQuoteRequest, LocalDate> dueDate;
	public static volatile SingularAttribute<TPQuoteRequest, Quotation> requestFromQuote;
	public static volatile SingularAttribute<TPQuoteRequest, LocalDate> receiptDate;
	public static volatile SingularAttribute<TPQuoteRequest, String> requestNo;
	public static volatile SetAttribute<TPQuoteRequest, TPQuoteRequestItem> items;
	public static volatile SetAttribute<TPQuoteRequest, CostType> defaultCostTypes;
	public static volatile SingularAttribute<TPQuoteRequest, TPQuoteRequestStatus> status;

	public static final String NOTES = "notes";
	public static final String REQUEST_FROM_JOB = "requestFromJob";
	public static final String CONTACT = "contact";
	public static final String DUE_DATE = "dueDate";
	public static final String REQUEST_FROM_QUOTE = "requestFromQuote";
	public static final String RECEIPT_DATE = "receiptDate";
	public static final String REQUEST_NO = "requestNo";
	public static final String ITEMS = "items";
	public static final String DEFAULT_COST_TYPES = "defaultCostTypes";
	public static final String STATUS = "status";

}

