package org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.calcost;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingCalibrationCost;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TPQuotationCalibrationCost.class)
public abstract class TPQuotationCalibrationCost_ extends org.trescal.cwms.core.pricing.entity.costs.base.CalCost_ {

	public static volatile SetAttribute<TPQuotationCalibrationCost, JobCostingCalibrationCost> linkedJobCalCosts;
	public static volatile SingularAttribute<TPQuotationCalibrationCost, TPQuotationItem> tpQuoteItem;
	public static volatile SetAttribute<TPQuotationCalibrationCost, QuotationCalibrationCost> linkedQuoteCalCosts;

	public static final String LINKED_JOB_CAL_COSTS = "linkedJobCalCosts";
	public static final String TP_QUOTE_ITEM = "tpQuoteItem";
	public static final String LINKED_QUOTE_CAL_COSTS = "linkedQuoteCalCosts";

}

