package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquoteitemnote;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TPQuoteItemNote.class)
public abstract class TPQuoteItemNote_ extends org.trescal.cwms.core.system.entity.note.Note_ {

	public static volatile SingularAttribute<TPQuoteItemNote, TPQuotationItem> tpquotationitem;

	public static final String TPQUOTATIONITEM = "tpquotationitem";

}

