package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.adjcost.TPQuotationAdjustmentCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.calcost.TPQuotationCalibrationCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.purchasecost.TPQuotationPurchaseCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.repcost.TPQuotationRepairCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquoteitemnote.TPQuoteItemNote;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotelink.TPQuoteLink;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TPQuotationItem.class)
public abstract class TPQuotationItem_ extends org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem.PricingItem_ {

	public static volatile SetAttribute<TPQuotationItem, TPQuoteLink> linkedTo;
	public static volatile SingularAttribute<TPQuotationItem, TPQuotationPurchaseCost> purchaseCost;
	public static volatile SingularAttribute<TPQuotationItem, TPQuotationAdjustmentCost> adjustmentCost;
	public static volatile SetAttribute<TPQuotationItem, TPQuoteItemNote> notes;
	public static volatile SingularAttribute<TPQuotationItem, TPQuotation> tpquotation;
	public static volatile SingularAttribute<TPQuotationItem, BigDecimal> carriageOut;
	public static volatile SingularAttribute<TPQuotationItem, BigDecimal> carriageIn;
	public static volatile SetAttribute<TPQuotationItem, TPQuotationItem> modules;
	public static volatile SingularAttribute<TPQuotationItem, TPQuotationItem> baseUnit;
	public static volatile SingularAttribute<TPQuotationItem, TPQuotationCalibrationCost> calibrationCost;
	public static volatile SingularAttribute<TPQuotationItem, InstrumentModel> model;
	public static volatile SingularAttribute<TPQuotationItem, Integer> id;
	public static volatile SingularAttribute<TPQuotationItem, TPQuotationRepairCost> repairCost;
	public static volatile SingularAttribute<TPQuotationItem, CalibrationType> caltype;
	public static volatile SingularAttribute<TPQuotationItem, BigDecimal> inspection;

	public static final String LINKED_TO = "linkedTo";
	public static final String PURCHASE_COST = "purchaseCost";
	public static final String ADJUSTMENT_COST = "adjustmentCost";
	public static final String NOTES = "notes";
	public static final String TPQUOTATION = "tpquotation";
	public static final String CARRIAGE_OUT = "carriageOut";
	public static final String CARRIAGE_IN = "carriageIn";
	public static final String MODULES = "modules";
	public static final String BASE_UNIT = "baseUnit";
	public static final String CALIBRATION_COST = "calibrationCost";
	public static final String MODEL = "model";
	public static final String ID = "id";
	public static final String REPAIR_COST = "repairCost";
	public static final String CALTYPE = "caltype";
	public static final String INSPECTION = "inspection";

}

