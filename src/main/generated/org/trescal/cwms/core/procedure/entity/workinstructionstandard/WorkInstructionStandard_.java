package org.trescal.cwms.core.procedure.entity.workinstructionstandard;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(WorkInstructionStandard.class)
public abstract class WorkInstructionStandard_ extends org.trescal.cwms.core.procedure.entity.defaultstandard.DefaultStandard_ {

	public static volatile SingularAttribute<WorkInstructionStandard, WorkInstruction> workInstruction;

	public static final String WORK_INSTRUCTION = "workInstruction";

}

