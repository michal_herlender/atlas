package org.trescal.cwms.core.procedure.entity.proceduretrainingrecord;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CapabilityTrainingRecord.class)
public abstract class CapabilityTrainingRecord_ {

	public static volatile SingularAttribute<CapabilityTrainingRecord, Capability> capability;
	public static volatile SingularAttribute<CapabilityTrainingRecord, Certificate> cert;
	public static volatile SingularAttribute<CapabilityTrainingRecord, Integer> id;
	public static volatile SingularAttribute<CapabilityTrainingRecord, Contact> trainee;
	public static volatile SingularAttribute<CapabilityTrainingRecord, Contact> labManager;

	public static final String CAPABILITY = "capability";
	public static final String CERT = "cert";
	public static final String ID = "id";
	public static final String TRAINEE = "trainee";
	public static final String LAB_MANAGER = "labManager";

}

