package org.trescal.cwms.core.procedure.entity.procedure;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.ServiceCapability;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.CalibrationProcess;
import org.trescal.cwms.core.jobs.contractreview.entity.WorkRequirementType;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.procedure.entity.capabilityfilter.CapabilityFilter;
import org.trescal.cwms.core.procedure.entity.categorisedprocedure.CategorisedCapability;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.CapabilityAuthorization;
import org.trescal.cwms.core.procedure.entity.procedureheading.CapabilityHeading;
import org.trescal.cwms.core.procedure.entity.procedurestandard.CapabilityStandard;
import org.trescal.cwms.core.procedure.entity.proceduretrainingrecord.CapabilityTrainingRecord;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;
import org.trescal.cwms.core.system.entity.accreditedlab.AccreditedLab;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Capability.class)
public abstract class Capability_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<Capability, Integer> estimatedTime;
	public static volatile SingularAttribute<Capability, CapabilityHeading> heading;
	public static volatile ListAttribute<Capability, ServiceCapability> serviceCapabilities;
	public static volatile SetAttribute<Capability, JobItem> jobItems;
	public static volatile SingularAttribute<Capability, Description> subfamily;
	public static volatile SingularAttribute<Capability, LocalDate> deactivatedOn;
	public static volatile SingularAttribute<Capability, Location> defLocation;
	public static volatile SetAttribute<Capability, CapabilityAuthorization> authorizations;
	public static volatile SingularAttribute<Capability, String> description;
	public static volatile SingularAttribute<Capability, Boolean> active;
	public static volatile SetAttribute<Capability, CapabilityTrainingRecord> capabilityTrainingRecords;
	public static volatile SingularAttribute<Capability, WorkInstruction> defWorkInstruction;
	public static volatile SingularAttribute<Capability, Contact> deactivatedBy;
	public static volatile SingularAttribute<Capability, String> reference;
	public static volatile SetAttribute<Capability, CapabilityStandard> standards;
	public static volatile SetAttribute<Capability, CapabilityFilter> capabilityFilters;
	public static volatile SingularAttribute<Capability, CalibrationProcess> calibrationProcess;
	public static volatile SingularAttribute<Capability, Address> defAddress;
	public static volatile SingularAttribute<Capability, String> name;
	public static volatile SingularAttribute<Capability, WorkRequirementType> reqType;
	public static volatile SingularAttribute<Capability, AccreditedLab> labAccreditation;
	public static volatile ListAttribute<Capability, CategorisedCapability> categories;
	public static volatile SingularAttribute<Capability, Integer> id;

	public static final String ESTIMATED_TIME = "estimatedTime";
	public static final String HEADING = "heading";
	public static final String SERVICE_CAPABILITIES = "serviceCapabilities";
	public static final String JOB_ITEMS = "jobItems";
	public static final String SUBFAMILY = "subfamily";
	public static final String DEACTIVATED_ON = "deactivatedOn";
	public static final String DEF_LOCATION = "defLocation";
	public static final String AUTHORIZATIONS = "authorizations";
	public static final String DESCRIPTION = "description";
	public static final String ACTIVE = "active";
	public static final String CAPABILITY_TRAINING_RECORDS = "capabilityTrainingRecords";
	public static final String DEF_WORK_INSTRUCTION = "defWorkInstruction";
	public static final String DEACTIVATED_BY = "deactivatedBy";
	public static final String REFERENCE = "reference";
	public static final String STANDARDS = "standards";
	public static final String CAPABILITY_FILTERS = "capabilityFilters";
	public static final String CALIBRATION_PROCESS = "calibrationProcess";
	public static final String DEF_ADDRESS = "defAddress";
	public static final String NAME = "name";
	public static final String REQ_TYPE = "reqType";
	public static final String LAB_ACCREDITATION = "labAccreditation";
	public static final String CATEGORIES = "categories";
	public static final String ID = "id";

}

