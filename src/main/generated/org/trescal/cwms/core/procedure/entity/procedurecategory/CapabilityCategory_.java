package org.trescal.cwms.core.procedure.entity.procedurecategory;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.procedure.entity.categorisedprocedure.CategorisedCapability;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CapabilityCategory.class)
public abstract class CapabilityCategory_ {

	public static volatile SingularAttribute<CapabilityCategory, Boolean> departmentDefault;
	public static volatile SetAttribute<CapabilityCategory, CategorisedCapability> procedures;
	public static volatile SingularAttribute<CapabilityCategory, String> name;
	public static volatile SingularAttribute<CapabilityCategory, String> description;
	public static volatile SingularAttribute<CapabilityCategory, Integer> id;
	public static volatile SingularAttribute<CapabilityCategory, Boolean> splitTraceable;
	public static volatile SingularAttribute<CapabilityCategory, Department> department;

	public static final String DEPARTMENT_DEFAULT = "departmentDefault";
	public static final String PROCEDURES = "procedures";
	public static final String NAME = "name";
	public static final String DESCRIPTION = "description";
	public static final String ID = "id";
	public static final String SPLIT_TRACEABLE = "splitTraceable";
	public static final String DEPARTMENT = "department";

}

