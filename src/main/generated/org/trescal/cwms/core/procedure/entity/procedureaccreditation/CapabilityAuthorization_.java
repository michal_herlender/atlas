package org.trescal.cwms.core.procedure.entity.procedureaccreditation;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CapabilityAuthorization.class)
public abstract class CapabilityAuthorization_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<CapabilityAuthorization, AccreditationLevel> accredLevel;
	public static volatile SingularAttribute<CapabilityAuthorization, Contact> authorizationFor;
	public static volatile SingularAttribute<CapabilityAuthorization, Capability> capability;
	public static volatile SingularAttribute<CapabilityAuthorization, LocalDate> awardedOn;
	public static volatile SingularAttribute<CapabilityAuthorization, Integer> id;
	public static volatile SingularAttribute<CapabilityAuthorization, Contact> awardedBy;
	public static volatile SingularAttribute<CapabilityAuthorization, CalibrationType> caltype;

	public static final String ACCRED_LEVEL = "accredLevel";
	public static final String AUTHORIZATION_FOR = "authorizationFor";
	public static final String CAPABILITY = "capability";
	public static final String AWARDED_ON = "awardedOn";
	public static final String ID = "id";
	public static final String AWARDED_BY = "awardedBy";
	public static final String CALTYPE = "caltype";

}

