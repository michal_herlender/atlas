package org.trescal.cwms.core.procedure.entity.workinstruction;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.procedure.entity.workinstructionstandard.WorkInstructionStandard;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(WorkInstruction.class)
public abstract class WorkInstruction_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<WorkInstruction, Contact> approver;
	public static volatile ListAttribute<WorkInstruction, WorkInstructionStandard> standards;
	public static volatile SingularAttribute<WorkInstruction, LocalDate> approvalDate;
	public static volatile ListAttribute<WorkInstruction, Instrument> instruments;
	public static volatile SingularAttribute<WorkInstruction, String> name;
	public static volatile SingularAttribute<WorkInstruction, Description> description;
	public static volatile SingularAttribute<WorkInstruction, Integer> id;
	public static volatile SingularAttribute<WorkInstruction, String> title;

	public static final String APPROVER = "approver";
	public static final String STANDARDS = "standards";
	public static final String APPROVAL_DATE = "approvalDate";
	public static final String INSTRUMENTS = "instruments";
	public static final String NAME = "name";
	public static final String DESCRIPTION = "description";
	public static final String ID = "id";
	public static final String TITLE = "title";

}

