package org.trescal.cwms.core.procedure.entity.procedurestandard;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CapabilityStandard.class)
public abstract class CapabilityStandard_ extends org.trescal.cwms.core.procedure.entity.defaultstandard.DefaultStandard_ {

	public static volatile SingularAttribute<CapabilityStandard, Capability> capability;

	public static final String CAPABILITY = "capability";

}

