package org.trescal.cwms.core.procedure.entity.procedureheading;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CapabilityHeading.class)
public abstract class CapabilityHeading_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<CapabilityHeading, String> heading;
	public static volatile SetAttribute<CapabilityHeading, Capability> procedures;
	public static volatile SingularAttribute<CapabilityHeading, Integer> id;

	public static final String HEADING = "heading";
	public static final String PROCEDURES = "procedures";
	public static final String ID = "id";

}

