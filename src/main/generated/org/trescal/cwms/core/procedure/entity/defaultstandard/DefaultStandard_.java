package org.trescal.cwms.core.procedure.entity.defaultstandard;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(DefaultStandard.class)
public abstract class DefaultStandard_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<DefaultStandard, Boolean> enforceUse;
	public static volatile SingularAttribute<DefaultStandard, LocalDate> setOn;
	public static volatile SingularAttribute<DefaultStandard, Instrument> instrument;
	public static volatile SingularAttribute<DefaultStandard, Integer> id;
	public static volatile SingularAttribute<DefaultStandard, Contact> setBy;

	public static final String ENFORCE_USE = "enforceUse";
	public static final String SET_ON = "setOn";
	public static final String INSTRUMENT = "instrument";
	public static final String ID = "id";
	public static final String SET_BY = "setBy";

}

