package org.trescal.cwms.core.procedure.entity.capabilityfilter;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CapabilityFilter.class)
public abstract class CapabilityFilter_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SingularAttribute<CapabilityFilter, String> internalComments;
	public static volatile SingularAttribute<CapabilityFilter, Capability> capability;
	public static volatile SingularAttribute<CapabilityFilter, ServiceType> servicetype;
	public static volatile SingularAttribute<CapabilityFilter, String> externalComments;
	public static volatile SingularAttribute<CapabilityFilter, Integer> standardTime;
	public static volatile SingularAttribute<CapabilityFilter, WorkInstruction> defaultWorkInstruction;
	public static volatile SingularAttribute<CapabilityFilter, Boolean> active;
	public static volatile SingularAttribute<CapabilityFilter, Integer> id;
	public static volatile SingularAttribute<CapabilityFilter, CapabilityFilterType> filterType;
	public static volatile SingularAttribute<CapabilityFilter, Boolean> bestLab;

	public static final String INTERNAL_COMMENTS = "internalComments";
	public static final String CAPABILITY = "capability";
	public static final String SERVICETYPE = "servicetype";
	public static final String EXTERNAL_COMMENTS = "externalComments";
	public static final String STANDARD_TIME = "standardTime";
	public static final String DEFAULT_WORK_INSTRUCTION = "defaultWorkInstruction";
	public static final String ACTIVE = "active";
	public static final String ID = "id";
	public static final String FILTER_TYPE = "filterType";
	public static final String BEST_LAB = "bestLab";

}

