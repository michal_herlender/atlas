package org.trescal.cwms.core.procedure.entity.categorisedprocedure;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CategorisedCapability.class)
public abstract class CategorisedCapability_ {

	public static volatile SingularAttribute<CategorisedCapability, Capability> capability;
	public static volatile SingularAttribute<CategorisedCapability, Integer> id;
	public static volatile SingularAttribute<CategorisedCapability, CapabilityCategory> category;

	public static final String CAPABILITY = "capability";
	public static final String ID = "id";
	public static final String CATEGORY = "category";

}

