package org.trescal.cwms.core.deliverynote.entity.transportoptionschedulerule;

import java.time.LocalTime;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TransportOptionScheduleRule.class)
public abstract class TransportOptionScheduleRule_ {

	public static volatile SingularAttribute<TransportOptionScheduleRule, TransportOption> transportOption;
	public static volatile SingularAttribute<TransportOptionScheduleRule, String> dayOfWeek;
	public static volatile SingularAttribute<TransportOptionScheduleRule, LocalTime> cutoffTime;
	public static volatile SingularAttribute<TransportOptionScheduleRule, Integer> id;

	public static final String TRANSPORT_OPTION = "transportOption";
	public static final String DAY_OF_WEEK = "dayOfWeek";
	public static final String CUTOFF_TIME = "cutoffTime";
	public static final String ID = "id";

}

