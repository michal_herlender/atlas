package org.trescal.cwms.core.deliverynote.entity.deliveryitem;

import java.time.ZonedDateTime;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.deliveryitemaccessory.DeliveryItemAccessory;
import org.trescal.cwms.core.deliverynote.entity.deliveryitemnote.DeliveryItemNote;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(DeliveryItem.class)
public abstract class DeliveryItem_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<DeliveryItem, Delivery> delivery;
	public static volatile SingularAttribute<DeliveryItem, String> accessoryFreeText;
	public static volatile SetAttribute<DeliveryItem, DeliveryItemNote> notes;
	public static volatile SetAttribute<DeliveryItem, DeliveryItemAccessory> accessories;
	public static volatile SingularAttribute<DeliveryItem, Integer> delitemid;
	public static volatile SingularAttribute<DeliveryItem, Boolean> despatched;
	public static volatile SingularAttribute<DeliveryItem, ZonedDateTime> despatchedon;

	public static final String DELIVERY = "delivery";
	public static final String ACCESSORY_FREE_TEXT = "accessoryFreeText";
	public static final String NOTES = "notes";
	public static final String ACCESSORIES = "accessories";
	public static final String DELITEMID = "delitemid";
	public static final String DESPATCHED = "despatched";
	public static final String DESPATCHEDON = "despatchedon";

}

