package org.trescal.cwms.core.deliverynote.entity.courier;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.CourierDespatchType;
import org.trescal.cwms.core.logistics.entity.jobcourier.JobCourierLink;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Courier.class)
public abstract class Courier_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<Courier, Boolean> defaultCourier;
	public static volatile SingularAttribute<Courier, String> webTrackURL;
	public static volatile SingularAttribute<Courier, String> name;
	public static volatile SingularAttribute<Courier, Integer> courierid;
	public static volatile SetAttribute<Courier, CourierDespatchType> cdts;
	public static volatile ListAttribute<Courier, JobCourierLink> jobCouriers;

	public static final String DEFAULT_COURIER = "defaultCourier";
	public static final String WEB_TRACK_UR_L = "webTrackURL";
	public static final String NAME = "name";
	public static final String COURIERID = "courierid";
	public static final String CDTS = "cdts";
	public static final String JOB_COURIERS = "jobCouriers";

}

