package org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.TPRequirement;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobDeliveryItem.class)
public abstract class JobDeliveryItem_ extends org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItem_ {

	public static volatile SingularAttribute<JobDeliveryItem, JobItem> jobitem;
	public static volatile SingularAttribute<JobDeliveryItem, TPRequirement> tpRequirement;

	public static final String JOBITEM = "jobitem";
	public static final String TP_REQUIREMENT = "tpRequirement";

}

