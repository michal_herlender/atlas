package org.trescal.cwms.core.deliverynote.entity.transportmethod;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TransportMethod.class)
public abstract class TransportMethod_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<TransportMethod, String> method;
	public static volatile SingularAttribute<TransportMethod, Boolean> optionPerDayOfWeek;
	public static volatile SingularAttribute<TransportMethod, Boolean> active;
	public static volatile SingularAttribute<TransportMethod, Integer> id;
	public static volatile SetAttribute<TransportMethod, Translation> methodTranslation;

	public static final String METHOD = "method";
	public static final String OPTION_PER_DAY_OF_WEEK = "optionPerDayOfWeek";
	public static final String ACTIVE = "active";
	public static final String ID = "id";
	public static final String METHOD_TRANSLATION = "methodTranslation";

}

