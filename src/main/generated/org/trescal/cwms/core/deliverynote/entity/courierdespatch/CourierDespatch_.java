package org.trescal.cwms.core.deliverynote.entity.courierdespatch;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.CourierDespatchType;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CourierDespatch.class)
public abstract class CourierDespatch_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<CourierDespatch, Contact> createdBy;
	public static volatile SingularAttribute<CourierDespatch, LocalDate> despatchDate;
	public static volatile SingularAttribute<CourierDespatch, CourierDespatchType> cdtype;
	public static volatile SingularAttribute<CourierDespatch, Integer> crdespid;
	public static volatile SingularAttribute<CourierDespatch, LocalDate> creationDate;
	public static volatile SingularAttribute<CourierDespatch, String> consignmentno;
	public static volatile ListAttribute<CourierDespatch, Delivery> deliveries;

	public static final String CREATED_BY = "createdBy";
	public static final String DESPATCH_DATE = "despatchDate";
	public static final String CDTYPE = "cdtype";
	public static final String CRDESPID = "crdespid";
	public static final String CREATION_DATE = "creationDate";
	public static final String CONSIGNMENTNO = "consignmentno";
	public static final String DELIVERIES = "deliveries";

}

