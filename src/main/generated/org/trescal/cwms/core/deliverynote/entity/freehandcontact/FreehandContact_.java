package org.trescal.cwms.core.deliverynote.entity.freehandcontact;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(FreehandContact.class)
public abstract class FreehandContact_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<FreehandContact, String> address;
	public static volatile SingularAttribute<FreehandContact, String> contact;
	public static volatile SingularAttribute<FreehandContact, String> company;
	public static volatile SingularAttribute<FreehandContact, String> telephone;
	public static volatile SingularAttribute<FreehandContact, Integer> id;
	public static volatile SingularAttribute<FreehandContact, String> subdiv;

	public static final String ADDRESS = "address";
	public static final String CONTACT = "contact";
	public static final String COMPANY = "company";
	public static final String TELEPHONE = "telephone";
	public static final String ID = "id";
	public static final String SUBDIV = "subdiv";

}

