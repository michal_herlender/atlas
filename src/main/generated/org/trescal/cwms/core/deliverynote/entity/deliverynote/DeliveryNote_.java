package org.trescal.cwms.core.deliverynote.entity.deliverynote;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(DeliveryNote.class)
public abstract class DeliveryNote_ extends org.trescal.cwms.core.system.entity.note.Note_ {

	public static volatile SingularAttribute<DeliveryNote, Delivery> delivery;

	public static final String DELIVERY = "delivery";

}

