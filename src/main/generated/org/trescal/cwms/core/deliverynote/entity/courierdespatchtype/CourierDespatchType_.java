package org.trescal.cwms.core.deliverynote.entity.courierdespatchtype;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.deliverynote.entity.courier.Courier;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.CourierDespatch;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CourierDespatchType.class)
public abstract class CourierDespatchType_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<CourierDespatchType, Courier> courier;
	public static volatile SingularAttribute<CourierDespatchType, String> description;
	public static volatile SingularAttribute<CourierDespatchType, Integer> cdtid;
	public static volatile SetAttribute<CourierDespatchType, CourierDespatch> despatches;
	public static volatile SingularAttribute<CourierDespatchType, Boolean> defaultForCourier;

	public static final String COURIER = "courier";
	public static final String DESCRIPTION = "description";
	public static final String CDTID = "cdtid";
	public static final String DESPATCHES = "despatches";
	public static final String DEFAULT_FOR_COURIER = "defaultForCourier";

}

