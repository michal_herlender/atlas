package org.trescal.cwms.core.deliverynote.entity.generaldelivery;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.deliverynote.entity.generaldeliveryitem.GeneralDeliveryItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(GeneralDelivery.class)
public abstract class GeneralDelivery_ extends org.trescal.cwms.core.deliverynote.entity.delivery.Delivery_ {

	public static volatile SingularAttribute<GeneralDelivery, String> ourref;
	public static volatile SingularAttribute<GeneralDelivery, String> clientref;
	public static volatile SetAttribute<GeneralDelivery, GeneralDeliveryItem> items;

	public static final String OURREF = "ourref";
	public static final String CLIENTREF = "clientref";
	public static final String ITEMS = "items";

}

