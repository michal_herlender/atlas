package org.trescal.cwms.core.deliverynote.entity.deliveryitemaccessory;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItem;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.Accessory;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(DeliveryItemAccessory.class)
public abstract class DeliveryItemAccessory_ {

	public static volatile SingularAttribute<DeliveryItemAccessory, DeliveryItem> deliveryItem;
	public static volatile SingularAttribute<DeliveryItemAccessory, Integer> qty;
	public static volatile SingularAttribute<DeliveryItemAccessory, Integer> id;
	public static volatile SingularAttribute<DeliveryItemAccessory, Accessory> accessory;

	public static final String DELIVERY_ITEM = "deliveryItem";
	public static final String QTY = "qty";
	public static final String ID = "id";
	public static final String ACCESSORY = "accessory";

}

