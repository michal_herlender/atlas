package org.trescal.cwms.core.deliverynote.entity.delivery;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.CourierDespatch;
import org.trescal.cwms.core.deliverynote.entity.deliverynote.DeliveryNote;
import org.trescal.cwms.core.deliverynote.entity.freehandcontact.FreehandContact;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Delivery.class)
public abstract class Delivery_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<Delivery, LocalDate> creationdate;
	public static volatile SingularAttribute<Delivery, Address> address;
	public static volatile SetAttribute<Delivery, DeliveryNote> notes;
	public static volatile SingularAttribute<Delivery, FreehandContact> freehandContact;
	public static volatile SingularAttribute<Delivery, String> deliveryno;
	public static volatile SingularAttribute<Delivery, CourierDespatch> crdes;
	public static volatile SingularAttribute<Delivery, String> signeeName;
	public static volatile SingularAttribute<Delivery, Date> signedOn;
	public static volatile SingularAttribute<Delivery, LocalDate> deliverydate;
	public static volatile SingularAttribute<Delivery, Boolean> signatureCaptured;
	public static volatile SingularAttribute<Delivery, ZonedDateTime> destinationReceiptDate;
	public static volatile SingularAttribute<Delivery, Schedule> schedule;
	public static volatile SingularAttribute<Delivery, Integer> deliveryid;
	public static volatile SingularAttribute<Delivery, Contact> createdby;
	public static volatile SingularAttribute<Delivery, Contact> contact;
	public static volatile SingularAttribute<Delivery, Location> location;

	public static final String CREATIONDATE = "creationdate";
	public static final String ADDRESS = "address";
	public static final String NOTES = "notes";
	public static final String FREEHAND_CONTACT = "freehandContact";
	public static final String DELIVERYNO = "deliveryno";
	public static final String CRDES = "crdes";
	public static final String SIGNEE_NAME = "signeeName";
	public static final String SIGNED_ON = "signedOn";
	public static final String DELIVERYDATE = "deliverydate";
	public static final String SIGNATURE_CAPTURED = "signatureCaptured";
	public static final String DESTINATION_RECEIPT_DATE = "destinationReceiptDate";
	public static final String SCHEDULE = "schedule";
	public static final String DELIVERYID = "deliveryid";
	public static final String CREATEDBY = "createdby";
	public static final String CONTACT = "contact";
	public static final String LOCATION = "location";

}

