package org.trescal.cwms.core.deliverynote.entity.transportoption;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.deliverynote.entity.transportmethod.TransportMethod;
import org.trescal.cwms.core.deliverynote.entity.transportoptionschedulerule.TransportOptionScheduleRule;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TransportOption.class)
public abstract class TransportOption_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<TransportOption, Subdiv> sub;
	public static volatile SingularAttribute<TransportOption, String> localizedName;
	public static volatile SingularAttribute<TransportOption, Integer> dayOfWeek;
	public static volatile SingularAttribute<TransportOption, TransportMethod> method;
	public static volatile SingularAttribute<TransportOption, Boolean> active;
	public static volatile ListAttribute<TransportOption, TransportOptionScheduleRule> scheduleRules;
	public static volatile SingularAttribute<TransportOption, Integer> id;
	public static volatile SingularAttribute<TransportOption, String> externalRef;

	public static final String SUB = "sub";
	public static final String LOCALIZED_NAME = "localizedName";
	public static final String DAY_OF_WEEK = "dayOfWeek";
	public static final String METHOD = "method";
	public static final String ACTIVE = "active";
	public static final String SCHEDULE_RULES = "scheduleRules";
	public static final String ID = "id";
	public static final String EXTERNAL_REF = "externalRef";

}

