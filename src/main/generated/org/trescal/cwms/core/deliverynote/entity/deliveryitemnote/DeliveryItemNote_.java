package org.trescal.cwms.core.deliverynote.entity.deliveryitemnote;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(DeliveryItemNote.class)
public abstract class DeliveryItemNote_ extends org.trescal.cwms.core.system.entity.note.Note_ {

	public static volatile SingularAttribute<DeliveryItemNote, DeliveryItem> deliveryitem;

	public static final String DELIVERYITEM = "deliveryitem";

}

