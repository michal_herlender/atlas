package org.trescal.cwms.core.deliverynote.entity.generaldeliveryitem;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(GeneralDeliveryItem.class)
public abstract class GeneralDeliveryItem_ extends org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItem_ {

	public static volatile SingularAttribute<GeneralDeliveryItem, String> itemDescription;

	public static final String ITEM_DESCRIPTION = "itemDescription";

}

