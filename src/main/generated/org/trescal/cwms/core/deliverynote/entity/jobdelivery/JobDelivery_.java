package org.trescal.cwms.core.deliverynote.entity.jobdelivery;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItem;
import org.trescal.cwms.core.jobs.job.entity.job.Job;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobDelivery.class)
public abstract class JobDelivery_ extends org.trescal.cwms.core.deliverynote.entity.delivery.Delivery_ {

	public static volatile SingularAttribute<JobDelivery, Job> job;
	public static volatile SetAttribute<JobDelivery, JobDeliveryItem> items;

	public static final String JOB = "job";
	public static final String ITEMS = "items";

}

