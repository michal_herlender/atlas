package org.trescal.cwms.core.quotation.entity.costs.purchasecost;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.purchasecost.ContractReviewLinkedPurchaseCost;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(QuotationPurchaseCost.class)
public abstract class QuotationPurchaseCost_ extends org.trescal.cwms.core.pricing.entity.costs.thirdparty.TPSupportedPurchaseCost_ {

	public static volatile SingularAttribute<QuotationPurchaseCost, Quotationitem> quoteItem;
	public static volatile SetAttribute<QuotationPurchaseCost, ContractReviewLinkedPurchaseCost> conRevLinkedPurchaseCosts;

	public static final String QUOTE_ITEM = "quoteItem";
	public static final String CON_REV_LINKED_PURCHASE_COSTS = "conRevLinkedPurchaseCosts";

}

