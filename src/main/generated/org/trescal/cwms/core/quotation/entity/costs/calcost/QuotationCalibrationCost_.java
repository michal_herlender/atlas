package org.trescal.cwms.core.quotation.entity.costs.calcost;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.ContractReviewLinkedCalibrationCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingLinkedCalibrationCost;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(QuotationCalibrationCost.class)
public abstract class QuotationCalibrationCost_ extends org.trescal.cwms.core.pricing.entity.costs.thirdparty.TPSupportedCalCost_ {

	public static volatile SetAttribute<QuotationCalibrationCost, JobCostingLinkedCalibrationCost> jobCostLinkedCalibrationCosts;
	public static volatile SetAttribute<QuotationCalibrationCost, ContractReviewLinkedCalibrationCost> conRevLinkedCalibrationCosts;
	public static volatile SingularAttribute<QuotationCalibrationCost, Quotationitem> quoteItem;

	public static final String JOB_COST_LINKED_CALIBRATION_COSTS = "jobCostLinkedCalibrationCosts";
	public static final String CON_REV_LINKED_CALIBRATION_COSTS = "conRevLinkedCalibrationCosts";
	public static final String QUOTE_ITEM = "quoteItem";

}

