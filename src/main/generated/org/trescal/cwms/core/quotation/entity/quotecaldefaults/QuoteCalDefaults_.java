package org.trescal.cwms.core.quotation.entity.quotecaldefaults;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.calibration.DefaultCalibrationCondition;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(QuoteCalDefaults.class)
public abstract class QuoteCalDefaults_ {

	public static volatile SingularAttribute<QuoteCalDefaults, DefaultCalibrationCondition> condition;
	public static volatile SingularAttribute<QuoteCalDefaults, Integer> id;
	public static volatile SingularAttribute<QuoteCalDefaults, Quotation> quotation;

	public static final String CONDITION = "condition";
	public static final String ID = "id";
	public static final String QUOTATION = "quotation";

}

