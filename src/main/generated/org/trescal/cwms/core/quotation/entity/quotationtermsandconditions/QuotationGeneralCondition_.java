package org.trescal.cwms.core.quotation.entity.quotationtermsandconditions;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(QuotationGeneralCondition.class)
public abstract class QuotationGeneralCondition_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<QuotationGeneralCondition, String> conditionText;
	public static volatile SingularAttribute<QuotationGeneralCondition, Integer> genConId;

	public static final String CONDITION_TEXT = "conditionText";
	public static final String GEN_CON_ID = "genConId";

}

