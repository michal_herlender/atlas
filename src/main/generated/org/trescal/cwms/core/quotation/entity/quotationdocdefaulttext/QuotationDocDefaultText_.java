package org.trescal.cwms.core.quotation.entity.quotationdocdefaulttext;

import java.util.Locale;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(QuotationDocDefaultText.class)
public abstract class QuotationDocDefaultText_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<QuotationDocDefaultText, String> subject;
	public static volatile SingularAttribute<QuotationDocDefaultText, Integer> id;
	public static volatile SingularAttribute<QuotationDocDefaultText, Locale> locale;
	public static volatile SingularAttribute<QuotationDocDefaultText, String> body;

	public static final String SUBJECT = "subject";
	public static final String ID = "id";
	public static final String LOCALE = "locale";
	public static final String BODY = "body";

}

