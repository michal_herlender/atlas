package org.trescal.cwms.core.quotation.entity.quotationrequest;

import java.time.LocalDate;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.quotation.entity.QuoteRequestType;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(QuotationRequest.class)
public abstract class QuotationRequest_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<QuotationRequest, String> country;
	public static volatile SingularAttribute<QuotationRequest, Contact> con;
	public static volatile SingularAttribute<QuotationRequest, String> calibrationType;
	public static volatile SingularAttribute<QuotationRequest, String> county;
	public static volatile SingularAttribute<QuotationRequest, Contact> loggedBy;
	public static volatile SingularAttribute<QuotationRequest, LocalDate> reqdate;
	public static volatile SingularAttribute<QuotationRequest, QuoteRequestType> quoteType;
	public static volatile SingularAttribute<QuotationRequest, QuotationRequestDetailsSource> detailsSource;
	public static volatile SingularAttribute<QuotationRequest, String> contact;
	public static volatile SingularAttribute<QuotationRequest, String> company;
	public static volatile SingularAttribute<QuotationRequest, String> requestInfo;
	public static volatile SingularAttribute<QuotationRequest, Integer> id;
	public static volatile SingularAttribute<QuotationRequest, String> fax;
	public static volatile SingularAttribute<QuotationRequest, String> email;
	public static volatile SingularAttribute<QuotationRequest, Company> comp;
	public static volatile SingularAttribute<QuotationRequest, String> addr2;
	public static volatile SingularAttribute<QuotationRequest, String> town;
	public static volatile SingularAttribute<QuotationRequest, String> addr1;
	public static volatile SingularAttribute<QuotationRequest, QuoteRequestLogSource> logSource;
	public static volatile SingularAttribute<QuotationRequest, String> postcode;
	public static volatile SingularAttribute<QuotationRequest, String> clientref;
	public static volatile SingularAttribute<QuotationRequest, String> requestNo;
	public static volatile SingularAttribute<QuotationRequest, Date> loggedOn;
	public static volatile SingularAttribute<QuotationRequest, String> phone;
	public static volatile SingularAttribute<QuotationRequest, LocalDate> duedate;
	public static volatile SingularAttribute<QuotationRequest, String> clientOrderNo;
	public static volatile SingularAttribute<QuotationRequest, Quotation> quotation;
	public static volatile SingularAttribute<QuotationRequest, QuotationRequestStatus> status;

	public static final String COUNTRY = "country";
	public static final String CON = "con";
	public static final String CALIBRATION_TYPE = "calibrationType";
	public static final String COUNTY = "county";
	public static final String LOGGED_BY = "loggedBy";
	public static final String REQDATE = "reqdate";
	public static final String QUOTE_TYPE = "quoteType";
	public static final String DETAILS_SOURCE = "detailsSource";
	public static final String CONTACT = "contact";
	public static final String COMPANY = "company";
	public static final String REQUEST_INFO = "requestInfo";
	public static final String ID = "id";
	public static final String FAX = "fax";
	public static final String EMAIL = "email";
	public static final String COMP = "comp";
	public static final String ADDR2 = "addr2";
	public static final String TOWN = "town";
	public static final String ADDR1 = "addr1";
	public static final String LOG_SOURCE = "logSource";
	public static final String POSTCODE = "postcode";
	public static final String CLIENTREF = "clientref";
	public static final String REQUEST_NO = "requestNo";
	public static final String LOGGED_ON = "loggedOn";
	public static final String PHONE = "phone";
	public static final String DUEDATE = "duedate";
	public static final String CLIENT_ORDER_NO = "clientOrderNo";
	public static final String QUOTATION = "quotation";
	public static final String STATUS = "status";

}

