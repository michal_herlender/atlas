package org.trescal.cwms.core.quotation.entity.quotenote;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(QuoteNote.class)
public abstract class QuoteNote_ extends org.trescal.cwms.core.system.entity.note.Note_ {

	public static volatile SingularAttribute<QuoteNote, Quotation> quotation;

	public static final String QUOTATION = "quotation";

}

