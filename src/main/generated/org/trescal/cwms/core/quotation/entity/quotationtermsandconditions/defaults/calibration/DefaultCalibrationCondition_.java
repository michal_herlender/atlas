package org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.calibration;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.quotation.entity.quotecaldefaults.QuoteCalDefaults;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(DefaultCalibrationCondition.class)
public abstract class DefaultCalibrationCondition_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SetAttribute<DefaultCalibrationCondition, Translation> conditiontextTranslation;
	public static volatile SetAttribute<DefaultCalibrationCondition, QuoteCalDefaults> defaultcalterms;
	public static volatile SingularAttribute<DefaultCalibrationCondition, String> conditionText;
	public static volatile SingularAttribute<DefaultCalibrationCondition, Integer> defCalConId;
	public static volatile SingularAttribute<DefaultCalibrationCondition, CalibrationType> caltype;

	public static final String CONDITIONTEXT_TRANSLATION = "conditiontextTranslation";
	public static final String DEFAULTCALTERMS = "defaultcalterms";
	public static final String CONDITION_TEXT = "conditionText";
	public static final String DEF_CAL_CON_ID = "defCalConId";
	public static final String CALTYPE = "caltype";

}

