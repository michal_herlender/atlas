package org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.general;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(DefaultGeneralCondition.class)
public abstract class DefaultGeneralCondition_ extends org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.QuotationGeneralCondition_ {

	public static volatile SetAttribute<DefaultGeneralCondition, Translation> conditiontextTranslation;
	public static volatile SetAttribute<DefaultGeneralCondition, Quotation> quotations;

	public static final String CONDITIONTEXT_TRANSLATION = "conditiontextTranslation";
	public static final String QUOTATIONS = "quotations";

}

