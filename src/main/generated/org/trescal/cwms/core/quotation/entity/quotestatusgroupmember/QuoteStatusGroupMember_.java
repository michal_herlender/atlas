package org.trescal.cwms.core.quotation.entity.quotestatusgroupmember;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.quotation.entity.quotestatus.QuoteStatus;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(QuoteStatusGroupMember.class)
public abstract class QuoteStatusGroupMember_ {

	public static volatile SingularAttribute<QuoteStatusGroupMember, Integer> groupId;
	public static volatile SingularAttribute<QuoteStatusGroupMember, Integer> id;
	public static volatile SingularAttribute<QuoteStatusGroupMember, QuoteStatus> status;

	public static final String GROUP_ID = "groupId";
	public static final String ID = "id";
	public static final String STATUS = "status";

}

