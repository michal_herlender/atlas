package org.trescal.cwms.core.quotation.entity.quotation;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(QuotationId.class)
public abstract class QuotationId_ {

	public static volatile SingularAttribute<QuotationId, Integer> ver;
	public static volatile SingularAttribute<QuotationId, String> qno;

	public static final String VER = "ver";
	public static final String QNO = "qno";

}

