package org.trescal.cwms.core.quotation.entity.scheduledquoterequest;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ScheduledQuotationRequestDateRange.class)
public abstract class ScheduledQuotationRequestDateRange_ {

	public static volatile SingularAttribute<ScheduledQuotationRequestDateRange, ScheduledQuotationRequest> request;
	public static volatile SingularAttribute<ScheduledQuotationRequestDateRange, Boolean> includeEmptyDates;
	public static volatile SingularAttribute<ScheduledQuotationRequestDateRange, Integer> requestid;
	public static volatile SingularAttribute<ScheduledQuotationRequestDateRange, LocalDate> finishDate;
	public static volatile SingularAttribute<ScheduledQuotationRequestDateRange, LocalDate> startDate;

	public static final String REQUEST = "request";
	public static final String INCLUDE_EMPTY_DATES = "includeEmptyDates";
	public static final String REQUESTID = "requestid";
	public static final String FINISH_DATE = "finishDate";
	public static final String START_DATE = "startDate";

}

