package org.trescal.cwms.core.quotation.entity.quotestatus;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotestatusgroupmember.QuoteStatusGroupMember;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(QuoteStatus.class)
public abstract class QuoteStatus_ extends org.trescal.cwms.core.system.entity.status.Status_ {

	public static volatile SetAttribute<QuoteStatus, Quotation> quotations;
	public static volatile SetAttribute<QuoteStatus, QuoteStatusGroupMember> statusGroups;

	public static final String QUOTATIONS = "quotations";
	public static final String STATUS_GROUPS = "statusGroups";

}

