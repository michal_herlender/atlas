package org.trescal.cwms.core.quotation.entity.quotationitem;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(QuotationitemId.class)
public abstract class QuotationitemId_ {

	public static volatile SingularAttribute<QuotationitemId, Integer> ver;
	public static volatile SingularAttribute<QuotationitemId, String> qno;
	public static volatile SingularAttribute<QuotationitemId, Integer> itemno;

	public static final String VER = "ver";
	public static final String QNO = "qno";
	public static final String ITEMNO = "itemno";

}

