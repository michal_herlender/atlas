package org.trescal.cwms.core.quotation.entity.quoteitemnote;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(QuoteItemNote.class)
public abstract class QuoteItemNote_ extends org.trescal.cwms.core.system.entity.note.Note_ {

	public static volatile SingularAttribute<QuoteItemNote, Quotationitem> quotationitem;

	public static final String QUOTATIONITEM = "quotationitem";

}

