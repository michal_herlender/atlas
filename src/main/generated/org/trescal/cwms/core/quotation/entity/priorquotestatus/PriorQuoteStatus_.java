package org.trescal.cwms.core.quotation.entity.priorquotestatus;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotestatus.QuoteStatus;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PriorQuoteStatus.class)
public abstract class PriorQuoteStatus_ {

	public static volatile SingularAttribute<PriorQuoteStatus, Date> revokedOn;
	public static volatile SingularAttribute<PriorQuoteStatus, Integer> pqsId;
	public static volatile SingularAttribute<PriorQuoteStatus, QuoteStatus> revokedTo;
	public static volatile SingularAttribute<PriorQuoteStatus, Date> setOn;
	public static volatile SingularAttribute<PriorQuoteStatus, Contact> revokedBy;
	public static volatile SingularAttribute<PriorQuoteStatus, Quotation> quotation;
	public static volatile SingularAttribute<PriorQuoteStatus, Contact> setBy;
	public static volatile SingularAttribute<PriorQuoteStatus, QuoteStatus> status;

	public static final String REVOKED_ON = "revokedOn";
	public static final String PQS_ID = "pqsId";
	public static final String REVOKED_TO = "revokedTo";
	public static final String SET_ON = "setOn";
	public static final String REVOKED_BY = "revokedBy";
	public static final String QUOTATION = "quotation";
	public static final String SET_BY = "setBy";
	public static final String STATUS = "status";

}

