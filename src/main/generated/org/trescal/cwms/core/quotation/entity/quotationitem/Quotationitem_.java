package org.trescal.cwms.core.quotation.entity.quotationitem;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost;
import org.trescal.cwms.core.quotation.entity.costs.purchasecost.QuotationPurchaseCost;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;
import org.trescal.cwms.core.quotation.entity.quoteitemnote.QuoteItemNote;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.TPQuoteRequestItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Quotationitem.class)
public abstract class Quotationitem_ extends org.trescal.cwms.core.pricing.entity.pricingitems.thirdpartypricingitem.ThirdPartyPricingItem_ {

	public static volatile SingularAttribute<Quotationitem, ServiceType> serviceType;
	public static volatile SingularAttribute<Quotationitem, QuotationPurchaseCost> purchaseCost;
	public static volatile SetAttribute<Quotationitem, QuoteItemNote> notes;
	public static volatile SingularAttribute<Quotationitem, String> plantno;
	public static volatile SetAttribute<Quotationitem, TPQuoteRequestItem> tpQuoteItemRequests;
	public static volatile SingularAttribute<Quotationitem, QuoteHeading> heading;
	public static volatile SetAttribute<Quotationitem, Quotationitem> modules;
	public static volatile SingularAttribute<Quotationitem, Quotationitem> baseUnit;
	public static volatile SingularAttribute<Quotationitem, QuotationCalibrationCost> calibrationCost;
	public static volatile SingularAttribute<Quotationitem, Instrument> inst;
	public static volatile SingularAttribute<Quotationitem, InstrumentModel> model;
	public static volatile SingularAttribute<Quotationitem, Integer> id;
	public static volatile SingularAttribute<Quotationitem, Quotation> quotation;
	public static volatile SingularAttribute<Quotationitem, CalibrationType> caltype;
	public static volatile SingularAttribute<Quotationitem, BigDecimal> inspection;

	public static final String SERVICE_TYPE = "serviceType";
	public static final String PURCHASE_COST = "purchaseCost";
	public static final String NOTES = "notes";
	public static final String PLANTNO = "plantno";
	public static final String TP_QUOTE_ITEM_REQUESTS = "tpQuoteItemRequests";
	public static final String HEADING = "heading";
	public static final String MODULES = "modules";
	public static final String BASE_UNIT = "baseUnit";
	public static final String CALIBRATION_COST = "calibrationCost";
	public static final String INST = "inst";
	public static final String MODEL = "model";
	public static final String ID = "id";
	public static final String QUOTATION = "quotation";
	public static final String CALTYPE = "caltype";
	public static final String INSPECTION = "inspection";

}

