package org.trescal.cwms.core.quotation.entity.quotationdoccustomtext;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(QuotationDocCustomText.class)
public abstract class QuotationDocCustomText_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<QuotationDocCustomText, String> subject;
	public static volatile SingularAttribute<QuotationDocCustomText, Integer> id;
	public static volatile SingularAttribute<QuotationDocCustomText, String> body;
	public static volatile SingularAttribute<QuotationDocCustomText, Quotation> quotation;

	public static final String SUBJECT = "subject";
	public static final String ID = "id";
	public static final String BODY = "body";
	public static final String QUOTATION = "quotation";

}

