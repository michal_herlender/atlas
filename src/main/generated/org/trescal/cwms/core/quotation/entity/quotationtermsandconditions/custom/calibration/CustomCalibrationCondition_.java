package org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.custom.calibration;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CustomCalibrationCondition.class)
public abstract class CustomCalibrationCondition_ extends org.trescal.cwms.core.audit.Versioned_ {

	public static volatile SingularAttribute<CustomCalibrationCondition, String> conditionText;
	public static volatile SingularAttribute<CustomCalibrationCondition, Integer> defCalConId;
	public static volatile SingularAttribute<CustomCalibrationCondition, Quotation> quotation;
	public static volatile SingularAttribute<CustomCalibrationCondition, CalibrationType> caltype;

	public static final String CONDITION_TEXT = "conditionText";
	public static final String DEF_CAL_CON_ID = "defCalConId";
	public static final String QUOTATION = "quotation";
	public static final String CALTYPE = "caltype";

}

