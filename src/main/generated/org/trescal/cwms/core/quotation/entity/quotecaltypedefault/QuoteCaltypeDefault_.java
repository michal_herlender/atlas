package org.trescal.cwms.core.quotation.entity.quotecaltypedefault;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(QuoteCaltypeDefault.class)
public abstract class QuoteCaltypeDefault_ {

	public static volatile SingularAttribute<QuoteCaltypeDefault, Integer> quotecaltypeid;
	public static volatile SingularAttribute<QuoteCaltypeDefault, Quotation> quotation;
	public static volatile SingularAttribute<QuoteCaltypeDefault, CalibrationType> caltype;

	public static final String QUOTECALTYPEID = "quotecaltypeid";
	public static final String QUOTATION = "quotation";
	public static final String CALTYPE = "caltype";

}

