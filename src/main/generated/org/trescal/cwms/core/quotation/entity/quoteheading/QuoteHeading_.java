package org.trescal.cwms.core.quotation.entity.quoteheading;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(QuoteHeading.class)
public abstract class QuoteHeading_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<QuoteHeading, Boolean> systemDefault;
	public static volatile SingularAttribute<QuoteHeading, Integer> headingNo;
	public static volatile SingularAttribute<QuoteHeading, String> headingName;
	public static volatile SingularAttribute<QuoteHeading, String> headingDescription;
	public static volatile SetAttribute<QuoteHeading, Quotationitem> quoteitems;
	public static volatile SingularAttribute<QuoteHeading, Integer> headingId;
	public static volatile SingularAttribute<QuoteHeading, Quotation> quotation;

	public static final String SYSTEM_DEFAULT = "systemDefault";
	public static final String HEADING_NO = "headingNo";
	public static final String HEADING_NAME = "headingName";
	public static final String HEADING_DESCRIPTION = "headingDescription";
	public static final String QUOTEITEMS = "quoteitems";
	public static final String HEADING_ID = "headingId";
	public static final String QUOTATION = "quotation";

}

