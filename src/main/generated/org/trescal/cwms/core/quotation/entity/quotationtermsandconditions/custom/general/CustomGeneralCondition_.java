package org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.custom.general;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CustomGeneralCondition.class)
public abstract class CustomGeneralCondition_ extends org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.QuotationGeneralCondition_ {

	public static volatile SingularAttribute<CustomGeneralCondition, Quotation> quotation;

	public static final String QUOTATION = "quotation";

}

