package org.trescal.cwms.core.quotation.entity.scheduledquoterequest;

import java.util.Date;
import java.util.Locale;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.enums.QuoteGenerationStrategy;
import org.trescal.cwms.core.quotation.enums.ScheduledQuoteRequestScope;
import org.trescal.cwms.core.quotation.enums.ScheduledQuoteRequestType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ScheduledQuotationRequest.class)
public abstract class ScheduledQuotationRequest_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<ScheduledQuotationRequest, ScheduledQuoteRequestType> requestType;
	public static volatile SingularAttribute<ScheduledQuotationRequest, ScheduledQuotationRequestDateRange> dateRange;
	public static volatile SingularAttribute<ScheduledQuotationRequest, Locale> locale;
	public static volatile SingularAttribute<ScheduledQuotationRequest, Boolean> processed;
	public static volatile SingularAttribute<ScheduledQuotationRequest, Date> requestOn;
	public static volatile SingularAttribute<ScheduledQuotationRequest, Date> processedOn;
	public static volatile SingularAttribute<ScheduledQuotationRequest, Contact> quoteContact;
	public static volatile SingularAttribute<ScheduledQuotationRequest, ScheduledQuoteRequestScope> scope;
	public static volatile SingularAttribute<ScheduledQuotationRequest, Boolean> cancelled;
	public static volatile SingularAttribute<ScheduledQuotationRequest, CalibrationType> defaultCalType;
	public static volatile SingularAttribute<ScheduledQuotationRequest, Contact> requestBy;
	public static volatile SingularAttribute<ScheduledQuotationRequest, Integer> id;
	public static volatile SingularAttribute<ScheduledQuotationRequest, QuoteGenerationStrategy> strategy;
	public static volatile SingularAttribute<ScheduledQuotationRequest, Quotation> quotation;

	public static final String REQUEST_TYPE = "requestType";
	public static final String DATE_RANGE = "dateRange";
	public static final String LOCALE = "locale";
	public static final String PROCESSED = "processed";
	public static final String REQUEST_ON = "requestOn";
	public static final String PROCESSED_ON = "processedOn";
	public static final String QUOTE_CONTACT = "quoteContact";
	public static final String SCOPE = "scope";
	public static final String CANCELLED = "cancelled";
	public static final String DEFAULT_CAL_TYPE = "defaultCalType";
	public static final String REQUEST_BY = "requestBy";
	public static final String ID = "id";
	public static final String STRATEGY = "strategy";
	public static final String QUOTATION = "quotation";

}

