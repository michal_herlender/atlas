package org.trescal.cwms.core.asset.entity.assetnote;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.asset.entity.asset.Asset;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AssetNote.class)
public abstract class AssetNote_ extends org.trescal.cwms.core.system.entity.note.Note_ {

	public static volatile SingularAttribute<AssetNote, Asset> asset;

	public static final String ASSET = "asset";

}

