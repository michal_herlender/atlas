package org.trescal.cwms.core.asset.entity.asset;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(NetworkAsset.class)
public abstract class NetworkAsset_ extends org.trescal.cwms.core.asset.entity.asset.Asset_ {

	public static volatile SingularAttribute<NetworkAsset, String> networkLocation;

	public static final String NETWORK_LOCATION = "networkLocation";

}

