package org.trescal.cwms.core.asset.entity.assetstatus;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AssetStatus.class)
public abstract class AssetStatus_ {

	public static volatile SetAttribute<AssetStatus, Translation> translations;
	public static volatile SingularAttribute<AssetStatus, String> name;
	public static volatile SingularAttribute<AssetStatus, Boolean> active;
	public static volatile SingularAttribute<AssetStatus, Integer> id;

	public static final String TRANSLATIONS = "translations";
	public static final String NAME = "name";
	public static final String ACTIVE = "active";
	public static final String ID = "id";

}

