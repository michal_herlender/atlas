package org.trescal.cwms.core.asset.entity.assettype;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AssetType.class)
public abstract class AssetType_ {

	public static volatile SingularAttribute<AssetType, Boolean> instType;
	public static volatile SetAttribute<AssetType, Translation> translations;
	public static volatile SingularAttribute<AssetType, String> name;
	public static volatile SingularAttribute<AssetType, String> description;
	public static volatile SingularAttribute<AssetType, Integer> id;

	public static final String INST_TYPE = "instType";
	public static final String TRANSLATIONS = "translations";
	public static final String NAME = "name";
	public static final String DESCRIPTION = "description";
	public static final String ID = "id";

}

