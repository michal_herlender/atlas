package org.trescal.cwms.core.asset.entity.asset;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PhysicalAsset.class)
public abstract class PhysicalAsset_ extends org.trescal.cwms.core.asset.entity.asset.Asset_ {

	public static volatile SingularAttribute<PhysicalAsset, Location> physicalLocation;
	public static volatile SingularAttribute<PhysicalAsset, String> plantNo;
	public static volatile SingularAttribute<PhysicalAsset, Instrument> inst;
	public static volatile SingularAttribute<PhysicalAsset, Company> supplier;
	public static volatile SingularAttribute<PhysicalAsset, String> ipAddress;
	public static volatile SingularAttribute<PhysicalAsset, String> serialNo;

	public static final String PHYSICAL_LOCATION = "physicalLocation";
	public static final String PLANT_NO = "plantNo";
	public static final String INST = "inst";
	public static final String SUPPLIER = "supplier";
	public static final String IP_ADDRESS = "ipAddress";
	public static final String SERIAL_NO = "serialNo";

}

