package org.trescal.cwms.core.asset.entity.asset;

import java.math.BigDecimal;
import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.asset.entity.assetnote.AssetNote;
import org.trescal.cwms.core.asset.entity.assetstatus.AssetStatus;
import org.trescal.cwms.core.asset.entity.assettype.AssetType;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.department.Department;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Asset.class)
public abstract class Asset_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<Asset, Contact> owner;
	public static volatile SetAttribute<Asset, AssetNote> notes;
	public static volatile SingularAttribute<Asset, Integer> quantity;
	public static volatile SingularAttribute<Asset, Contact> custodian;
	public static volatile SingularAttribute<Asset, Contact> primaryUser;
	public static volatile SingularAttribute<Asset, Contact> systemAddedBy;
	public static volatile SingularAttribute<Asset, Department> dept;
	public static volatile SingularAttribute<Asset, String> assetNo;
	public static volatile SingularAttribute<Asset, AssetType> type;
	public static volatile SingularAttribute<Asset, LocalDate> editableAddedOn;
	public static volatile SingularAttribute<Asset, LocalDate> systemAddedOn;
	public static volatile SingularAttribute<Asset, Integer> assetId;
	public static volatile SingularAttribute<Asset, BigDecimal> price;
	public static volatile SingularAttribute<Asset, String> name;
	public static volatile SingularAttribute<Asset, LocalDate> purchasedOn;
	public static volatile SingularAttribute<Asset, AssetStatus> status;

	public static final String OWNER = "owner";
	public static final String NOTES = "notes";
	public static final String QUANTITY = "quantity";
	public static final String CUSTODIAN = "custodian";
	public static final String PRIMARY_USER = "primaryUser";
	public static final String SYSTEM_ADDED_BY = "systemAddedBy";
	public static final String DEPT = "dept";
	public static final String ASSET_NO = "assetNo";
	public static final String TYPE = "type";
	public static final String EDITABLE_ADDED_ON = "editableAddedOn";
	public static final String SYSTEM_ADDED_ON = "systemAddedOn";
	public static final String ASSET_ID = "assetId";
	public static final String PRICE = "price";
	public static final String NAME = "name";
	public static final String PURCHASED_ON = "purchasedOn";
	public static final String STATUS = "status";

}

