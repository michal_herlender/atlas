package org.trescal.cwms.core.documents.images.instrumentmodelimage;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstrumentModelImage.class)
public abstract class InstrumentModelImage_ extends org.trescal.cwms.core.documents.images.image.Image_ {

	public static volatile SingularAttribute<InstrumentModelImage, InstrumentModel> model;

	public static final String MODEL = "model";

}

