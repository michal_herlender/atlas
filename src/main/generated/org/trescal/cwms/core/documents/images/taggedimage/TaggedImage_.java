package org.trescal.cwms.core.documents.images.taggedimage;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.documents.images.image.Image;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TaggedImage.class)
public abstract class TaggedImage_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<TaggedImage, Image> image;
	public static volatile SingularAttribute<TaggedImage, Integer> id;
	public static volatile SingularAttribute<TaggedImage, String> tag;

	public static final String IMAGE = "image";
	public static final String ID = "id";
	public static final String TAG = "tag";

}

