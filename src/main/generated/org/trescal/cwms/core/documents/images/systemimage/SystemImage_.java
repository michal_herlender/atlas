package org.trescal.cwms.core.documents.images.systemimage;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.documents.images.systemimage.enums.SystemImageType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SystemImage.class)
public abstract class SystemImage_ {

	public static volatile SingularAttribute<SystemImage, String> fileName;
	public static volatile SingularAttribute<SystemImage, byte[]> fileData;
	public static volatile SingularAttribute<SystemImage, String> description;
	public static volatile SingularAttribute<SystemImage, Company> businessCompany;
	public static volatile SingularAttribute<SystemImage, Integer> id;
	public static volatile SingularAttribute<SystemImage, SystemImageType> type;

	public static final String FILE_NAME = "fileName";
	public static final String FILE_DATA = "fileData";
	public static final String DESCRIPTION = "description";
	public static final String BUSINESS_COMPANY = "businessCompany";
	public static final String ID = "id";
	public static final String TYPE = "type";

}

