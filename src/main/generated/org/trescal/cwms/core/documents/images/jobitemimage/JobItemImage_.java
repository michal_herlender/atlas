package org.trescal.cwms.core.documents.images.jobitemimage;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(JobItemImage.class)
public abstract class JobItemImage_ extends org.trescal.cwms.core.documents.images.image.Image_ {

	public static volatile SingularAttribute<JobItemImage, JobItem> jobitem;

	public static final String JOBITEM = "jobitem";

}

