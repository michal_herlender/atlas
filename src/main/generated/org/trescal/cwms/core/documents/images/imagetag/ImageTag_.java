package org.trescal.cwms.core.documents.images.imagetag;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ImageTag.class)
public abstract class ImageTag_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<ImageTag, String> description;
	public static volatile SingularAttribute<ImageTag, Integer> id;
	public static volatile SingularAttribute<ImageTag, String> tag;

	public static final String DESCRIPTION = "description";
	public static final String ID = "id";
	public static final String TAG = "tag";

}

