package org.trescal.cwms.core.documents.usermanual.entity;

import java.util.Locale;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserManual.class)
public abstract class UserManual_ {

	public static volatile SingularAttribute<UserManual, String> filename;
	public static volatile SingularAttribute<UserManual, Integer> id;
	public static volatile SingularAttribute<UserManual, Locale> locale;
	public static volatile SingularAttribute<UserManual, byte[]> fileBinary;

	public static final String FILENAME = "filename";
	public static final String ID = "id";
	public static final String LOCALE = "locale";
	public static final String FILE_BINARY = "fileBinary";

}

