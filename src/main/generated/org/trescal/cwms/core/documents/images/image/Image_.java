package org.trescal.cwms.core.documents.images.image;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.documents.images.taggedimage.TaggedImage;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Image.class)
public abstract class Image_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<Image, String> fileName;
	public static volatile SingularAttribute<Image, Contact> addedBy;
	public static volatile SingularAttribute<Image, String> description;
	public static volatile SingularAttribute<Image, Integer> id;
	public static volatile SingularAttribute<Image, Date> addedOn;
	public static volatile SingularAttribute<Image, String> relativeFilePath;
	public static volatile SetAttribute<Image, TaggedImage> tags;

	public static final String FILE_NAME = "fileName";
	public static final String ADDED_BY = "addedBy";
	public static final String DESCRIPTION = "description";
	public static final String ID = "id";
	public static final String ADDED_ON = "addedOn";
	public static final String RELATIVE_FILE_PATH = "relativeFilePath";
	public static final String TAGS = "tags";

}

