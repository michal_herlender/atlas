package org.trescal.cwms.core.schedule.entity.scheduleddelivery;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule;
import org.trescal.cwms.core.schedule.entity.scheduleddeliverystatus.ScheduledDeliveryStatus;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ScheduledDelivery.class)
public abstract class ScheduledDelivery_ {

	public static volatile SingularAttribute<ScheduledDelivery, Delivery> delivery;
	public static volatile SingularAttribute<ScheduledDelivery, Schedule> schedule;
	public static volatile SingularAttribute<ScheduledDelivery, Integer> id;
	public static volatile SingularAttribute<ScheduledDelivery, ScheduledDeliveryStatus> status;

	public static final String DELIVERY = "delivery";
	public static final String SCHEDULE = "schedule";
	public static final String ID = "id";
	public static final String STATUS = "status";

}

