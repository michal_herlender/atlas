package org.trescal.cwms.core.schedule.entity.scheduleequipmentmodel;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ScheduleEquipmentModel.class)
public abstract class ScheduleEquipmentModel_ extends org.trescal.cwms.core.schedule.entity.scheduleequipment.ScheduleEquipment_ {

	public static volatile SingularAttribute<ScheduleEquipmentModel, Integer> qty;
	public static volatile SingularAttribute<ScheduleEquipmentModel, InstrumentModel> equipModel;

	public static final String QTY = "qty";
	public static final String EQUIP_MODEL = "equipModel";

}

