package org.trescal.cwms.core.schedule.entity.repeatschedule;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.schedule.entity.scheduletype.ScheduleType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RepeatSchedule.class)
public abstract class RepeatSchedule_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<RepeatSchedule, TransportOption> transportOption;
	public static volatile SingularAttribute<RepeatSchedule, Contact> con;
	public static volatile SingularAttribute<RepeatSchedule, String> notes;
	public static volatile SingularAttribute<RepeatSchedule, Contact> createdBy;
	public static volatile SingularAttribute<RepeatSchedule, Date> deactivatedOn;
	public static volatile SingularAttribute<RepeatSchedule, Boolean> active;
	public static volatile SingularAttribute<RepeatSchedule, Integer> id;
	public static volatile SingularAttribute<RepeatSchedule, Address> addr;
	public static volatile SingularAttribute<RepeatSchedule, ScheduleType> type;
	public static volatile SingularAttribute<RepeatSchedule, Date> createdOn;
	public static volatile SingularAttribute<RepeatSchedule, Contact> deactivatedBy;

	public static final String TRANSPORT_OPTION = "transportOption";
	public static final String CON = "con";
	public static final String NOTES = "notes";
	public static final String CREATED_BY = "createdBy";
	public static final String DEACTIVATED_ON = "deactivatedOn";
	public static final String ACTIVE = "active";
	public static final String ID = "id";
	public static final String ADDR = "addr";
	public static final String TYPE = "type";
	public static final String CREATED_ON = "createdOn";
	public static final String DEACTIVATED_BY = "deactivatedBy";

}

