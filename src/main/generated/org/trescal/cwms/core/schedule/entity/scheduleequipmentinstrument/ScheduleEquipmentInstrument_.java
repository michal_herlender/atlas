package org.trescal.cwms.core.schedule.entity.scheduleequipmentinstrument;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ScheduleEquipmentInstrument.class)
public abstract class ScheduleEquipmentInstrument_ extends org.trescal.cwms.core.schedule.entity.scheduleequipment.ScheduleEquipment_ {

	public static volatile SingularAttribute<ScheduleEquipmentInstrument, Instrument> equipInst;

	public static final String EQUIP_INST = "equipInst";

}

