package org.trescal.cwms.core.schedule.entity.scheduleequipment;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.schedule.entity.ScheduleEquipmentTurn;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ScheduleEquipment.class)
public abstract class ScheduleEquipment_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<ScheduleEquipment, String> supportDesc;
	public static volatile SingularAttribute<ScheduleEquipment, Schedule> schedule;
	public static volatile SingularAttribute<ScheduleEquipment, String> ponumber;
	public static volatile SingularAttribute<ScheduleEquipment, String> commentDesc;
	public static volatile SingularAttribute<ScheduleEquipment, Boolean> faulty;
	public static volatile SingularAttribute<ScheduleEquipment, Integer> id;
	public static volatile SingularAttribute<ScheduleEquipment, ScheduleEquipmentTurn> turn;
	public static volatile SingularAttribute<ScheduleEquipment, String> faultDesc;
	public static volatile SingularAttribute<ScheduleEquipment, CalibrationType> caltype;
	public static volatile SingularAttribute<ScheduleEquipment, Boolean> supported;

	public static final String SUPPORT_DESC = "supportDesc";
	public static final String SCHEDULE = "schedule";
	public static final String PONUMBER = "ponumber";
	public static final String COMMENT_DESC = "commentDesc";
	public static final String FAULTY = "faulty";
	public static final String ID = "id";
	public static final String TURN = "turn";
	public static final String FAULT_DESC = "faultDesc";
	public static final String CALTYPE = "caltype";
	public static final String SUPPORTED = "supported";

}

