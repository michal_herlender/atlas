package org.trescal.cwms.core.schedule.entity.schedule;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.schedule.entity.scheduleequipment.ScheduleEquipment;
import org.trescal.cwms.core.schedule.entity.schedulenote.ScheduleNote;
import org.trescal.cwms.core.schedule.entity.schedulesource.ScheduleSource;
import org.trescal.cwms.core.schedule.entity.schedulestatus.ScheduleStatus;
import org.trescal.cwms.core.schedule.entity.scheduletype.ScheduleType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Schedule.class)
public abstract class Schedule_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SetAttribute<Schedule, ScheduleEquipment> scheduleequipment;
	public static volatile SingularAttribute<Schedule, TransportOption> transportOption;
	public static volatile SingularAttribute<Schedule, Address> address;
	public static volatile SetAttribute<Schedule, ScheduleNote> notes;
	public static volatile SingularAttribute<Schedule, ScheduleSource> source;
	public static volatile SingularAttribute<Schedule, ScheduleType> type;
	public static volatile SingularAttribute<Schedule, LocalDate> createdOn;
	public static volatile SingularAttribute<Schedule, Boolean> repeatSchedule;
	public static volatile SingularAttribute<Schedule, Contact> createdBy;
	public static volatile SingularAttribute<Schedule, Contact> contact;
	public static volatile SingularAttribute<Schedule, LocalDate> scheduleDate;
	public static volatile SingularAttribute<Schedule, Integer> scheduleId;
	public static volatile SingularAttribute<Schedule, ScheduleStatus> status;

	public static final String SCHEDULEEQUIPMENT = "scheduleequipment";
	public static final String TRANSPORT_OPTION = "transportOption";
	public static final String ADDRESS = "address";
	public static final String NOTES = "notes";
	public static final String SOURCE = "source";
	public static final String TYPE = "type";
	public static final String CREATED_ON = "createdOn";
	public static final String REPEAT_SCHEDULE = "repeatSchedule";
	public static final String CREATED_BY = "createdBy";
	public static final String CONTACT = "contact";
	public static final String SCHEDULE_DATE = "scheduleDate";
	public static final String SCHEDULE_ID = "scheduleId";
	public static final String STATUS = "status";

}

