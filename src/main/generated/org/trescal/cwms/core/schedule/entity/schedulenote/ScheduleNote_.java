package org.trescal.cwms.core.schedule.entity.schedulenote;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ScheduleNote.class)
public abstract class ScheduleNote_ extends org.trescal.cwms.core.system.entity.note.Note_ {

	public static volatile SingularAttribute<ScheduleNote, Schedule> schedule;

	public static final String SCHEDULE = "schedule";

}

