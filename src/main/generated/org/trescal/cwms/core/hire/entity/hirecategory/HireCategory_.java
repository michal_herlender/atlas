package org.trescal.cwms.core.hire.entity.hirecategory;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.hire.entity.hiremodelcategory.HireModelCategory;
import org.trescal.cwms.core.hire.entity.hiresection.HireSection;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(HireCategory.class)
public abstract class HireCategory_ {

	public static volatile SetAttribute<HireCategory, HireModelCategory> hireModelCategories;
	public static volatile SingularAttribute<HireCategory, HireSection> hireSection;
	public static volatile SingularAttribute<HireCategory, Integer> categorycode;
	public static volatile SingularAttribute<HireCategory, String> categoryname;
	public static volatile SingularAttribute<HireCategory, Integer> id;

	public static final String HIRE_MODEL_CATEGORIES = "hireModelCategories";
	public static final String HIRE_SECTION = "hireSection";
	public static final String CATEGORYCODE = "categorycode";
	public static final String CATEGORYNAME = "categoryname";
	public static final String ID = "id";

}

