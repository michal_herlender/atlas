package org.trescal.cwms.core.hire.entity.hirestatus;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.hire.entity.hire.Hire;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(HireStatus.class)
public abstract class HireStatus_ extends org.trescal.cwms.core.system.entity.status.Status_ {

	public static volatile SetAttribute<HireStatus, Hire> hires;

	public static final String HIRES = "hires";

}

