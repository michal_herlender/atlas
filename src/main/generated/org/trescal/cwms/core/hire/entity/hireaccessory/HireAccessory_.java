package org.trescal.cwms.core.hire.entity.hireaccessory;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.hire.entity.hireaccessorystatus.HireAccessoryStatus;
import org.trescal.cwms.core.hire.entity.hireitemaccessory.HireItemAccessory;
import org.trescal.cwms.core.instrument.entity.hireinstrument.HireInstrument;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(HireAccessory.class)
public abstract class HireAccessory_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<HireAccessory, Integer> hireAccessoryId;
	public static volatile SingularAttribute<HireAccessory, String> item;
	public static volatile SetAttribute<HireAccessory, HireItemAccessory> hireItemAccessory;
	public static volatile SingularAttribute<HireAccessory, HireInstrument> hireInst;
	public static volatile SingularAttribute<HireAccessory, Boolean> active;
	public static volatile SingularAttribute<HireAccessory, HireAccessoryStatus> status;

	public static final String HIRE_ACCESSORY_ID = "hireAccessoryId";
	public static final String ITEM = "item";
	public static final String HIRE_ITEM_ACCESSORY = "hireItemAccessory";
	public static final String HIRE_INST = "hireInst";
	public static final String ACTIVE = "active";
	public static final String STATUS = "status";

}

