package org.trescal.cwms.core.hire.entity.hiresuspenditem;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.hire.entity.hireitem.HireItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(HireSuspendItem.class)
public abstract class HireSuspendItem_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<HireSuspendItem, HireItem> hireItem;
	public static volatile SingularAttribute<HireSuspendItem, LocalDate> endDate;
	public static volatile SingularAttribute<HireSuspendItem, Integer> id;
	public static volatile SingularAttribute<HireSuspendItem, LocalDate> startDate;

	public static final String HIRE_ITEM = "hireItem";
	public static final String END_DATE = "endDate";
	public static final String ID = "id";
	public static final String START_DATE = "startDate";

}

