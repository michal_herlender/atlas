package org.trescal.cwms.core.hire.entity.hireitem;

import java.math.BigDecimal;
import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.hire.entity.hirecrossitem.HireCrossItem;
import org.trescal.cwms.core.hire.entity.hireitemaccessory.HireItemAccessory;
import org.trescal.cwms.core.hire.entity.hirereplacedinstrument.HireReplacedInstrument;
import org.trescal.cwms.core.hire.entity.hiresuspenditem.HireSuspendItem;
import org.trescal.cwms.core.instrument.entity.hireinstrument.HireInstrument;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(HireItem.class)
public abstract class HireItem_ extends org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem.PricingItem_ {

	public static volatile SingularAttribute<HireItem, CalibrationType> calType;
	public static volatile SingularAttribute<HireItem, BigDecimal> hireCost;
	public static volatile SingularAttribute<HireItem, Boolean> offHire;
	public static volatile SingularAttribute<HireItem, Hire> hire;
	public static volatile SetAttribute<HireItem, HireItemAccessory> accessories;
	public static volatile SetAttribute<HireItem, HireSuspendItem> suspenditems;
	public static volatile SingularAttribute<HireItem, Boolean> suspended;
	public static volatile SetAttribute<HireItem, HireReplacedInstrument> replacedInsts;
	public static volatile SingularAttribute<HireItem, Integer> offHireDays;
	public static volatile SingularAttribute<HireItem, String> offHireCondition;
	public static volatile SingularAttribute<HireItem, BigDecimal> ukasCalCost;
	public static volatile SingularAttribute<HireItem, HireInstrument> hireInst;
	public static volatile SingularAttribute<HireItem, LocalDate> offHireDate;
	public static volatile SingularAttribute<HireItem, Integer> id;
	public static volatile SingularAttribute<HireItem, HireCrossItem> hireCrossItem;

	public static final String CAL_TYPE = "calType";
	public static final String HIRE_COST = "hireCost";
	public static final String OFF_HIRE = "offHire";
	public static final String HIRE = "hire";
	public static final String ACCESSORIES = "accessories";
	public static final String SUSPENDITEMS = "suspenditems";
	public static final String SUSPENDED = "suspended";
	public static final String REPLACED_INSTS = "replacedInsts";
	public static final String OFF_HIRE_DAYS = "offHireDays";
	public static final String OFF_HIRE_CONDITION = "offHireCondition";
	public static final String UKAS_CAL_COST = "ukasCalCost";
	public static final String HIRE_INST = "hireInst";
	public static final String OFF_HIRE_DATE = "offHireDate";
	public static final String ID = "id";
	public static final String HIRE_CROSS_ITEM = "hireCrossItem";

}

