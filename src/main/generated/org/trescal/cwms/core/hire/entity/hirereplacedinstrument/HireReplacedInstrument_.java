package org.trescal.cwms.core.hire.entity.hirereplacedinstrument;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.hire.entity.hireitem.HireItem;
import org.trescal.cwms.core.instrument.entity.hireinstrument.HireInstrument;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(HireReplacedInstrument.class)
public abstract class HireReplacedInstrument_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<HireReplacedInstrument, String> reason;
	public static volatile SingularAttribute<HireReplacedInstrument, HireItem> hireItem;
	public static volatile SingularAttribute<HireReplacedInstrument, Contact> replacedBy;
	public static volatile SingularAttribute<HireReplacedInstrument, HireInstrument> hireInst;
	public static volatile SingularAttribute<HireReplacedInstrument, Integer> id;
	public static volatile SingularAttribute<HireReplacedInstrument, LocalDate> replacedOn;

	public static final String REASON = "reason";
	public static final String HIRE_ITEM = "hireItem";
	public static final String REPLACED_BY = "replacedBy";
	public static final String HIRE_INST = "hireInst";
	public static final String ID = "id";
	public static final String REPLACED_ON = "replacedOn";

}

