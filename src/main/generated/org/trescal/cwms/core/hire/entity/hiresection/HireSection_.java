package org.trescal.cwms.core.hire.entity.hiresection;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.hire.entity.hirecategory.HireCategory;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(HireSection.class)
public abstract class HireSection_ {

	public static volatile SetAttribute<HireSection, HireCategory> hireCategories;
	public static volatile SingularAttribute<HireSection, Integer> sectioncode;
	public static volatile SingularAttribute<HireSection, Integer> id;
	public static volatile SingularAttribute<HireSection, String> sectionname;

	public static final String HIRE_CATEGORIES = "hireCategories";
	public static final String SECTIONCODE = "sectioncode";
	public static final String ID = "id";
	public static final String SECTIONNAME = "sectionname";

}

