package org.trescal.cwms.core.hire.entity.hiremodelincategory;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.hire.entity.hiremodelcategory.HireModelCategory;
import org.trescal.cwms.core.instrumentmodel.entity.hiremodel.HireModel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(HireModelInCategory.class)
public abstract class HireModelInCategory_ {

	public static volatile SingularAttribute<HireModelInCategory, HireModelCategory> hireModelCategory;
	public static volatile SingularAttribute<HireModelInCategory, Integer> id;
	public static volatile SingularAttribute<HireModelInCategory, HireModel> hireModel;

	public static final String HIRE_MODEL_CATEGORY = "hireModelCategory";
	public static final String ID = "id";
	public static final String HIRE_MODEL = "hireModel";

}

