package org.trescal.cwms.core.hire.entity.hirecrossitem;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(HireCrossItem.class)
public abstract class HireCrossItem_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<HireCrossItem, Integer> itemid;
	public static volatile SingularAttribute<HireCrossItem, String> plantno;
	public static volatile SingularAttribute<HireCrossItem, String> description;
	public static volatile SingularAttribute<HireCrossItem, String> serialno;

	public static final String ITEMID = "itemid";
	public static final String PLANTNO = "plantno";
	public static final String DESCRIPTION = "description";
	public static final String SERIALNO = "serialno";

}

