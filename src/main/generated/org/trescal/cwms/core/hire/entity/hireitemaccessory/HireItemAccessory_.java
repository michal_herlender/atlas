package org.trescal.cwms.core.hire.entity.hireitemaccessory;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.hire.entity.hireaccessory.HireAccessory;
import org.trescal.cwms.core.hire.entity.hireitem.HireItem;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(HireItemAccessory.class)
public abstract class HireItemAccessory_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<HireItemAccessory, HireAccessory> hireAccessory;
	public static volatile SingularAttribute<HireItemAccessory, HireItem> hireItem;
	public static volatile SingularAttribute<HireItemAccessory, Integer> id;
	public static volatile SingularAttribute<HireItemAccessory, Boolean> returned;

	public static final String HIRE_ACCESSORY = "hireAccessory";
	public static final String HIRE_ITEM = "hireItem";
	public static final String ID = "id";
	public static final String RETURNED = "returned";

}

