package org.trescal.cwms.core.hire.entity.hire;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.CourierDespatch;
import org.trescal.cwms.core.deliverynote.entity.freehandcontact.FreehandContact;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.hire.entity.hireitem.HireItem;
import org.trescal.cwms.core.hire.entity.hirenote.HireNote;
import org.trescal.cwms.core.hire.entity.hirestatus.HireStatus;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Hire.class)
public abstract class Hire_ extends org.trescal.cwms.core.pricing.entity.pricings.pricing.Pricing_ {

	public static volatile SingularAttribute<Hire, String> hireno;
	public static volatile SingularAttribute<Hire, LocalDate> hireDate;
	public static volatile SingularAttribute<Hire, TransportOption> transportOption;
	public static volatile SingularAttribute<Hire, Address> address;
	public static volatile SetAttribute<Hire, HireNote> notes;
	public static volatile SingularAttribute<Hire, FreehandContact> freehandContact;
	public static volatile SingularAttribute<Hire, String> specialInst;
	public static volatile SingularAttribute<Hire, CourierDespatch> crdes;
	public static volatile SingularAttribute<Hire, LocalDate> enquiryDate;
	public static volatile SingularAttribute<Hire, Integer> enquiryEstDuration;
	public static volatile SingularAttribute<Hire, Boolean> accountsVarified;
	public static volatile SingularAttribute<Hire, Boolean> enquiry;
	public static volatile SetAttribute<Hire, HireItem> items;
	public static volatile SingularAttribute<Hire, String> poNumber;
	public static volatile SingularAttribute<Hire, HireStatus> status;

	public static final String HIRENO = "hireno";
	public static final String HIRE_DATE = "hireDate";
	public static final String TRANSPORT_OPTION = "transportOption";
	public static final String ADDRESS = "address";
	public static final String NOTES = "notes";
	public static final String FREEHAND_CONTACT = "freehandContact";
	public static final String SPECIAL_INST = "specialInst";
	public static final String CRDES = "crdes";
	public static final String ENQUIRY_DATE = "enquiryDate";
	public static final String ENQUIRY_EST_DURATION = "enquiryEstDuration";
	public static final String ACCOUNTS_VARIFIED = "accountsVarified";
	public static final String ENQUIRY = "enquiry";
	public static final String ITEMS = "items";
	public static final String PO_NUMBER = "poNumber";
	public static final String STATUS = "status";

}

