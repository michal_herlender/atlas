package org.trescal.cwms.core.hire.entity.hirenote;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.hire.entity.hire.Hire;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(HireNote.class)
public abstract class HireNote_ extends org.trescal.cwms.core.system.entity.note.Note_ {

	public static volatile SingularAttribute<HireNote, Hire> hire;

	public static final String HIRE = "hire";

}

