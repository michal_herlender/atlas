package org.trescal.cwms.core.hire.entity.hiremodelcategory;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.hire.entity.hirecategory.HireCategory;
import org.trescal.cwms.core.hire.entity.hiremodelincategory.HireModelInCategory;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(HireModelCategory.class)
public abstract class HireModelCategory_ {

	public static volatile SingularAttribute<HireModelCategory, Integer> modelcode;
	public static volatile SetAttribute<HireModelCategory, HireModelInCategory> hireModelInCategories;
	public static volatile SingularAttribute<HireModelCategory, String> plantcode;
	public static volatile SingularAttribute<HireModelCategory, HireCategory> hireCategory;
	public static volatile SingularAttribute<HireModelCategory, Integer> id;

	public static final String MODELCODE = "modelcode";
	public static final String HIRE_MODEL_IN_CATEGORIES = "hireModelInCategories";
	public static final String PLANTCODE = "plantcode";
	public static final String HIRE_CATEGORY = "hireCategory";
	public static final String ID = "id";

}

