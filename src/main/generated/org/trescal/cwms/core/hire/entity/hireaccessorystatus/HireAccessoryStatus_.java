package org.trescal.cwms.core.hire.entity.hireaccessorystatus;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.hire.entity.hireaccessory.HireAccessory;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(HireAccessoryStatus.class)
public abstract class HireAccessoryStatus_ extends org.trescal.cwms.core.system.entity.status.Status_ {

	public static volatile SetAttribute<HireAccessoryStatus, HireAccessory> hireAccessories;

	public static final String HIRE_ACCESSORIES = "hireAccessories";

}

