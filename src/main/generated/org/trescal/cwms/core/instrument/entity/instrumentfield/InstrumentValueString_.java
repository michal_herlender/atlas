package org.trescal.cwms.core.instrument.entity.instrumentfield;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstrumentValueString.class)
public abstract class InstrumentValueString_ extends org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentValue_ {

	public static volatile SingularAttribute<InstrumentValueString, String> value;

	public static final String VALUE = "value";

}

