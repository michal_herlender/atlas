package org.trescal.cwms.core.instrument.entity.hireinstrument;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.hire.entity.hireaccessory.HireAccessory;
import org.trescal.cwms.core.hire.entity.hireitem.HireItem;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(HireInstrument.class)
public abstract class HireInstrument_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<HireInstrument, Instrument> inst;
	public static volatile SingularAttribute<HireInstrument, Integer> id;
	public static volatile SetAttribute<HireInstrument, HireAccessory> hireInstAccessories;
	public static volatile SetAttribute<HireInstrument, HireItem> hireItems;

	public static final String INST = "inst";
	public static final String ID = "id";
	public static final String HIRE_INST_ACCESSORIES = "hireInstAccessories";
	public static final String HIRE_ITEMS = "hireItems";

}

