package org.trescal.cwms.core.instrument.entity.instrumentusagetype;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstrumentUsageType.class)
public abstract class InstrumentUsageType_ extends org.trescal.cwms.core.instrument.entity.instrumenttype.InstrumentType_ {

	public static volatile SingularAttribute<InstrumentUsageType, InstrumentStatus> instrumentStatus;
	public static volatile SetAttribute<InstrumentUsageType, Instrument> instruments;
	public static volatile SetAttribute<InstrumentUsageType, Translation> descriptiontranslation;
	public static volatile SetAttribute<InstrumentUsageType, Translation> nametranslation;

	public static final String INSTRUMENT_STATUS = "instrumentStatus";
	public static final String INSTRUMENTS = "instruments";
	public static final String DESCRIPTIONTRANSLATION = "descriptiontranslation";
	public static final String NAMETRANSLATION = "nametranslation";

}

