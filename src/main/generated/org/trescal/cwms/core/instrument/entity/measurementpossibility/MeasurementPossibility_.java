package org.trescal.cwms.core.instrument.entity.measurementpossibility;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(MeasurementPossibility.class)
public abstract class MeasurementPossibility_ {

	public static volatile SingularAttribute<MeasurementPossibility, String> scalederiva;
	public static volatile SingularAttribute<MeasurementPossibility, String> name;
	public static volatile SingularAttribute<MeasurementPossibility, Double> maximum;
	public static volatile SingularAttribute<MeasurementPossibility, String> lmax;
	public static volatile SingularAttribute<MeasurementPossibility, Instrument> instrument;
	public static volatile SingularAttribute<MeasurementPossibility, Integer> measurementpossibilityid;
	public static volatile SingularAttribute<MeasurementPossibility, Double> minimum;
	public static volatile SingularAttribute<MeasurementPossibility, String> quality;

	public static final String SCALEDERIVA = "scalederiva";
	public static final String NAME = "name";
	public static final String MAXIMUM = "maximum";
	public static final String LMAX = "lmax";
	public static final String INSTRUMENT = "instrument";
	public static final String MEASUREMENTPOSSIBILITYID = "measurementpossibilityid";
	public static final String MINIMUM = "minimum";
	public static final String QUALITY = "quality";

}

