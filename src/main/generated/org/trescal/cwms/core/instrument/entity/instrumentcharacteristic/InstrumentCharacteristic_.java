package org.trescal.cwms.core.instrument.entity.instrumentcharacteristic;

import java.util.UUID;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.CharacteristicDescription;
import org.trescal.cwms.core.instrumentmodel.entity.characteristiclibrary.CharacteristicLibrary;
import org.trescal.cwms.core.instrumentmodel.entity.uom.UoM;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstrumentCharacteristic.class)
public abstract class InstrumentCharacteristic_ {

	public static volatile SingularAttribute<InstrumentCharacteristic, UoM> uom;
	public static volatile SingularAttribute<InstrumentCharacteristic, CharacteristicLibrary> libraryValue;
	public static volatile SingularAttribute<InstrumentCharacteristic, Instrument> instrument;
	public static volatile SingularAttribute<InstrumentCharacteristic, UUID> id;
	public static volatile SingularAttribute<InstrumentCharacteristic, Double> value;
	public static volatile SingularAttribute<InstrumentCharacteristic, CharacteristicDescription> characteristic;
	public static volatile SingularAttribute<InstrumentCharacteristic, String> freeTextValue;

	public static final String UOM = "uom";
	public static final String LIBRARY_VALUE = "libraryValue";
	public static final String INSTRUMENT = "instrument";
	public static final String ID = "id";
	public static final String VALUE = "value";
	public static final String CHARACTERISTIC = "characteristic";
	public static final String FREE_TEXT_VALUE = "freeTextValue";

}

