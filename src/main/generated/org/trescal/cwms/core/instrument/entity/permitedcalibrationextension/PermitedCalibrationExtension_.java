package org.trescal.cwms.core.instrument.entity.permitedcalibrationextension;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.system.enums.IntervalUnit;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PermitedCalibrationExtension.class)
public abstract class PermitedCalibrationExtension_ {

	public static volatile SingularAttribute<PermitedCalibrationExtension, LocalDate> extensionEndDate;
	public static volatile SingularAttribute<PermitedCalibrationExtension, Instrument> instrument;
	public static volatile SingularAttribute<PermitedCalibrationExtension, PermitedCalibrationExtensionType> type;
	public static volatile SingularAttribute<PermitedCalibrationExtension, Integer> extensionId;
	public static volatile SingularAttribute<PermitedCalibrationExtension, Integer> value;
	public static volatile SingularAttribute<PermitedCalibrationExtension, IntervalUnit> timeUnit;

	public static final String EXTENSION_END_DATE = "extensionEndDate";
	public static final String INSTRUMENT = "instrument";
	public static final String TYPE = "type";
	public static final String EXTENSION_ID = "extensionId";
	public static final String VALUE = "value";
	public static final String TIME_UNIT = "timeUnit";

}

