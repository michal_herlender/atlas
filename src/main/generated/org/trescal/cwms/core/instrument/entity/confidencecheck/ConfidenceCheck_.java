package org.trescal.cwms.core.instrument.entity.confidencecheck;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.checkout.CheckOut;
import org.trescal.cwms.core.instrument.entity.confidencecheck.confidencecheckresultenum.ConfidenceCheckResultEnum;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ConfidenceCheck.class)
public abstract class ConfidenceCheck_ {

	public static volatile SingularAttribute<ConfidenceCheck, ConfidenceCheckResultEnum> result;
	public static volatile ListAttribute<ConfidenceCheck, CheckOut> checkOuts;
	public static volatile SingularAttribute<ConfidenceCheck, Contact> performedBy;
	public static volatile SingularAttribute<ConfidenceCheck, String> description;
	public static volatile SingularAttribute<ConfidenceCheck, Integer> id;
	public static volatile SingularAttribute<ConfidenceCheck, Date> performedOn;

	public static final String RESULT = "result";
	public static final String CHECK_OUTS = "checkOuts";
	public static final String PERFORMED_BY = "performedBy";
	public static final String DESCRIPTION = "description";
	public static final String ID = "id";
	public static final String PERFORMED_ON = "performedOn";

}

