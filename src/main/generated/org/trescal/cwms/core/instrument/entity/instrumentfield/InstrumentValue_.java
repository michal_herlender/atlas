package org.trescal.cwms.core.instrument.entity.instrumentfield;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstrumentValue.class)
public abstract class InstrumentValue_ {

	public static volatile SingularAttribute<InstrumentValue, Integer> instrumentFieldValueid;
	public static volatile SingularAttribute<InstrumentValue, Instrument> instrument;
	public static volatile SingularAttribute<InstrumentValue, InstrumentFieldDefinition> instrumentFieldDefinition;

	public static final String INSTRUMENT_FIELD_VALUEID = "instrumentFieldValueid";
	public static final String INSTRUMENT = "instrument";
	public static final String INSTRUMENT_FIELD_DEFINITION = "instrumentFieldDefinition";

}

