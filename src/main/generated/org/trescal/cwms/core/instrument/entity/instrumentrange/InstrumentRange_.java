package org.trescal.cwms.core.instrument.entity.instrumentrange;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstrumentRange.class)
public abstract class InstrumentRange_ extends org.trescal.cwms.core.instrumentmodel.entity.range.Range_ {

	public static volatile SingularAttribute<InstrumentRange, Instrument> instrument;

	public static final String INSTRUMENT = "instrument";

}

