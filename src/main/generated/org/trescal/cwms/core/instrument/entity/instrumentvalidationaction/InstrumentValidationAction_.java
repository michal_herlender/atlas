package org.trescal.cwms.core.instrument.entity.instrumentvalidationaction;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationissue.InstrumentValidationIssue;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstrumentValidationAction.class)
public abstract class InstrumentValidationAction_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<InstrumentValidationAction, InstrumentValidationIssue> issue;
	public static volatile SingularAttribute<InstrumentValidationAction, Contact> underTakenBy;
	public static volatile SingularAttribute<InstrumentValidationAction, String> action;
	public static volatile SingularAttribute<InstrumentValidationAction, Integer> id;
	public static volatile SingularAttribute<InstrumentValidationAction, Date> underTakenOn;

	public static final String ISSUE = "issue";
	public static final String UNDER_TAKEN_BY = "underTakenBy";
	public static final String ACTION = "action";
	public static final String ID = "id";
	public static final String UNDER_TAKEN_ON = "underTakenOn";

}

