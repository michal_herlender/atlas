package org.trescal.cwms.core.instrument.entity.instrumentfield;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.tools.FieldType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstrumentFieldDefinition.class)
public abstract class InstrumentFieldDefinition_ {

	public static volatile SingularAttribute<InstrumentFieldDefinition, Boolean> isUpdatable;
	public static volatile SetAttribute<InstrumentFieldDefinition, InstrumentFieldLibraryValue> instrumentFieldLibraryValues;
	public static volatile SingularAttribute<InstrumentFieldDefinition, String> name;
	public static volatile SingularAttribute<InstrumentFieldDefinition, Company> client;
	public static volatile SingularAttribute<InstrumentFieldDefinition, Integer> instrumentFieldDefinitionid;
	public static volatile SingularAttribute<InstrumentFieldDefinition, FieldType> fieldType;

	public static final String IS_UPDATABLE = "isUpdatable";
	public static final String INSTRUMENT_FIELD_LIBRARY_VALUES = "instrumentFieldLibraryValues";
	public static final String NAME = "name";
	public static final String CLIENT = "client";
	public static final String INSTRUMENT_FIELD_DEFINITIONID = "instrumentFieldDefinitionid";
	public static final String FIELD_TYPE = "fieldType";

}

