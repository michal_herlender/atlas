package org.trescal.cwms.core.instrument.entity.instrumentfield;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstrumentValueDateTime.class)
public abstract class InstrumentValueDateTime_ extends org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentValue_ {

	public static volatile SingularAttribute<InstrumentValueDateTime, Date> value;

	public static final String VALUE = "value";

}

