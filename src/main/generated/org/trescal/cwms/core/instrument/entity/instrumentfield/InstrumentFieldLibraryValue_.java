package org.trescal.cwms.core.instrument.entity.instrumentfield;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstrumentFieldLibraryValue.class)
public abstract class InstrumentFieldLibraryValue_ {

	public static volatile SingularAttribute<InstrumentFieldLibraryValue, String> name;
	public static volatile SingularAttribute<InstrumentFieldLibraryValue, Integer> instrumentFieldLibraryid;
	public static volatile SingularAttribute<InstrumentFieldLibraryValue, InstrumentFieldDefinition> instrumentFieldDefinition;

	public static final String NAME = "name";
	public static final String INSTRUMENT_FIELD_LIBRARYID = "instrumentFieldLibraryid";
	public static final String INSTRUMENT_FIELD_DEFINITION = "instrumentFieldDefinition";

}

