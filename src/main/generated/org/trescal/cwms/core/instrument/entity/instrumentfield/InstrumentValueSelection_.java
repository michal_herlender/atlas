package org.trescal.cwms.core.instrument.entity.instrumentfield;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstrumentValueSelection.class)
public abstract class InstrumentValueSelection_ extends org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentValue_ {

	public static volatile SingularAttribute<InstrumentValueSelection, InstrumentFieldLibraryValue> instrumentFieldLibraryValue;

	public static final String INSTRUMENT_FIELD_LIBRARY_VALUE = "instrumentFieldLibraryValue";

}

