package org.trescal.cwms.core.instrument.entity.instrumentcomplementaryfield;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.entity.uom.UoM;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateClass;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstrumentComplementaryField.class)
public abstract class InstrumentComplementaryField_ {

	public static volatile SingularAttribute<InstrumentComplementaryField, Date> purchaseDate;
	public static volatile SingularAttribute<InstrumentComplementaryField, BigDecimal> purchaseCost;
	public static volatile SingularAttribute<InstrumentComplementaryField, Integer> instrumentComplementaryFieldId;
	public static volatile SingularAttribute<InstrumentComplementaryField, String> formerCharacteristics;
	public static volatile SingularAttribute<InstrumentComplementaryField, String> formerModel;
	public static volatile SingularAttribute<InstrumentComplementaryField, Date> nextInterimCalibrationDate;
	public static volatile SingularAttribute<InstrumentComplementaryField, String> formerManufacturer;
	public static volatile SingularAttribute<InstrumentComplementaryField, Instrument> instrument;
	public static volatile SingularAttribute<InstrumentComplementaryField, String> formerBarCode;
	public static volatile SingularAttribute<InstrumentComplementaryField, Date> firstUseDate;
	public static volatile SingularAttribute<InstrumentComplementaryField, CertificateClass> certificateClass;
	public static volatile SingularAttribute<InstrumentComplementaryField, UoM> deviationUnits;
	public static volatile SingularAttribute<InstrumentComplementaryField, String> clientRemarks;
	public static volatile SingularAttribute<InstrumentComplementaryField, String> formerLocalDescription;
	public static volatile SingularAttribute<InstrumentComplementaryField, String> provider;
	public static volatile SingularAttribute<InstrumentComplementaryField, String> formerFamily;
	public static volatile SingularAttribute<InstrumentComplementaryField, Date> nextMaintenanceDate;
	public static volatile SingularAttribute<InstrumentComplementaryField, CalibrationVerificationStatus> calibrationStatusOverride;
	public static volatile SingularAttribute<InstrumentComplementaryField, String> accreditation;
	public static volatile SingularAttribute<InstrumentComplementaryField, String> formerName;
	public static volatile SingularAttribute<InstrumentComplementaryField, BigDecimal> nominalValue;
	public static volatile SingularAttribute<InstrumentComplementaryField, String> deliveryStatus;
	public static volatile SingularAttribute<InstrumentComplementaryField, String> customerAcceptanceCriteria;

	public static final String PURCHASE_DATE = "purchaseDate";
	public static final String PURCHASE_COST = "purchaseCost";
	public static final String INSTRUMENT_COMPLEMENTARY_FIELD_ID = "instrumentComplementaryFieldId";
	public static final String FORMER_CHARACTERISTICS = "formerCharacteristics";
	public static final String FORMER_MODEL = "formerModel";
	public static final String NEXT_INTERIM_CALIBRATION_DATE = "nextInterimCalibrationDate";
	public static final String FORMER_MANUFACTURER = "formerManufacturer";
	public static final String INSTRUMENT = "instrument";
	public static final String FORMER_BAR_CODE = "formerBarCode";
	public static final String FIRST_USE_DATE = "firstUseDate";
	public static final String CERTIFICATE_CLASS = "certificateClass";
	public static final String DEVIATION_UNITS = "deviationUnits";
	public static final String CLIENT_REMARKS = "clientRemarks";
	public static final String FORMER_LOCAL_DESCRIPTION = "formerLocalDescription";
	public static final String PROVIDER = "provider";
	public static final String FORMER_FAMILY = "formerFamily";
	public static final String NEXT_MAINTENANCE_DATE = "nextMaintenanceDate";
	public static final String CALIBRATION_STATUS_OVERRIDE = "calibrationStatusOverride";
	public static final String ACCREDITATION = "accreditation";
	public static final String FORMER_NAME = "formerName";
	public static final String NOMINAL_VALUE = "nominalValue";
	public static final String DELIVERY_STATUS = "deliveryStatus";
	public static final String CUSTOMER_ACCEPTANCE_CRITERIA = "customerAcceptanceCriteria";

}

