package org.trescal.cwms.core.instrument.entity.instrumentstoragetype;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstrumentStorageType.class)
public abstract class InstrumentStorageType_ extends org.trescal.cwms.core.instrument.entity.instrumenttype.InstrumentType_ {

	public static volatile SetAttribute<InstrumentStorageType, Instrument> instruments;
	public static volatile SetAttribute<InstrumentStorageType, Translation> descriptiontranslation;
	public static volatile SetAttribute<InstrumentStorageType, Translation> nametranslation;

	public static final String INSTRUMENTS = "instruments";
	public static final String DESCRIPTIONTRANSLATION = "descriptiontranslation";
	public static final String NAMETRANSLATION = "nametranslation";

}

