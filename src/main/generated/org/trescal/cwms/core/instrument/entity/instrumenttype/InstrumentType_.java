package org.trescal.cwms.core.instrument.entity.instrumenttype;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstrumentType.class)
public abstract class InstrumentType_ {

	public static volatile SingularAttribute<InstrumentType, Boolean> recall;
	public static volatile SingularAttribute<InstrumentType, String> name;
	public static volatile SingularAttribute<InstrumentType, Integer> id;
	public static volatile SingularAttribute<InstrumentType, String> fullDescription;
	public static volatile SingularAttribute<InstrumentType, Boolean> defaultType;

	public static final String RECALL = "recall";
	public static final String NAME = "name";
	public static final String ID = "id";
	public static final String FULL_DESCRIPTION = "fullDescription";
	public static final String DEFAULT_TYPE = "defaultType";

}

