package org.trescal.cwms.core.instrument.entity.checkout;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.confidencecheck.ConfidenceCheck;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.job.entity.job.Job;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CheckOut.class)
public abstract class CheckOut_ {

	public static volatile SingularAttribute<CheckOut, Date> checkOutDate;
	public static volatile SingularAttribute<CheckOut, Instrument> instrument;
	public static volatile SingularAttribute<CheckOut, Integer> id;
	public static volatile SingularAttribute<CheckOut, Job> job;
	public static volatile SingularAttribute<CheckOut, Date> checkInDate;
	public static volatile ListAttribute<CheckOut, ConfidenceCheck> confidenceChecks;
	public static volatile SingularAttribute<CheckOut, Boolean> checkOutComplet;
	public static volatile SingularAttribute<CheckOut, Contact> technicien;

	public static final String CHECK_OUT_DATE = "checkOutDate";
	public static final String INSTRUMENT = "instrument";
	public static final String ID = "id";
	public static final String JOB = "job";
	public static final String CHECK_IN_DATE = "checkInDate";
	public static final String CONFIDENCE_CHECKS = "confidenceChecks";
	public static final String CHECK_OUT_COMPLET = "checkOutComplet";
	public static final String TECHNICIEN = "technicien";

}

