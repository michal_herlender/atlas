package org.trescal.cwms.core.instrument.entity.instrumentvalidationissue;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationaction.InstrumentValidationAction;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationstatus.InstrumentValidationStatus;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstrumentValidationIssue.class)
public abstract class InstrumentValidationIssue_ extends org.trescal.cwms.core.audit.Auditable_ {

	public static volatile SingularAttribute<InstrumentValidationIssue, String> issue;
	public static volatile SingularAttribute<InstrumentValidationIssue, LocalDate> raisedOn;
	public static volatile SingularAttribute<InstrumentValidationIssue, Contact> resolvedBy;
	public static volatile SingularAttribute<InstrumentValidationIssue, Instrument> instrument;
	public static volatile SingularAttribute<InstrumentValidationIssue, Integer> id;
	public static volatile SingularAttribute<InstrumentValidationIssue, Contact> raisedBy;
	public static volatile SetAttribute<InstrumentValidationIssue, InstrumentValidationAction> actions;
	public static volatile SingularAttribute<InstrumentValidationIssue, InstrumentValidationStatus> status;
	public static volatile SingularAttribute<InstrumentValidationIssue, LocalDate> resolvedOn;

	public static final String ISSUE = "issue";
	public static final String RAISED_ON = "raisedOn";
	public static final String RESOLVED_BY = "resolvedBy";
	public static final String INSTRUMENT = "instrument";
	public static final String ID = "id";
	public static final String RAISED_BY = "raisedBy";
	public static final String ACTIONS = "actions";
	public static final String STATUS = "status";
	public static final String RESOLVED_ON = "resolvedOn";

}

