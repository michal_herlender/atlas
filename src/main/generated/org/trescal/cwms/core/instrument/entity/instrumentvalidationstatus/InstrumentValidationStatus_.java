package org.trescal.cwms.core.instrument.entity.instrumentvalidationstatus;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationissue.InstrumentValidationIssue;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstrumentValidationStatus.class)
public abstract class InstrumentValidationStatus_ extends org.trescal.cwms.core.system.entity.status.Status_ {

	public static volatile SetAttribute<InstrumentValidationStatus, InstrumentValidationIssue> issues;

	public static final String ISSUES = "issues";

}

