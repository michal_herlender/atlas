package org.trescal.cwms.core.instrument.entity.instrumenthistory;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstrumentHistory.class)
public abstract class InstrumentHistory_ {

	public static volatile SingularAttribute<InstrumentHistory, Boolean> serialNoUpdated;
	public static volatile SingularAttribute<InstrumentHistory, InstrumentStatus> newStatus;
	public static volatile SingularAttribute<InstrumentHistory, Contact> oldContact;
	public static volatile SingularAttribute<InstrumentHistory, Boolean> contactUpdated;
	public static volatile SingularAttribute<InstrumentHistory, Contact> changeBy;
	public static volatile SingularAttribute<InstrumentHistory, Company> newCompany;
	public static volatile SingularAttribute<InstrumentHistory, Contact> newContact;
	public static volatile SingularAttribute<InstrumentHistory, String> oldSerialNo;
	public static volatile SingularAttribute<InstrumentHistory, Boolean> plantNoUpdated;
	public static volatile SingularAttribute<InstrumentHistory, Instrument> instrument;
	public static volatile SingularAttribute<InstrumentHistory, String> newPlantNo;
	public static volatile SingularAttribute<InstrumentHistory, Address> oldAddress;
	public static volatile SingularAttribute<InstrumentHistory, String> oldPlantNo;
	public static volatile SingularAttribute<InstrumentHistory, Boolean> companyUpdated;
	public static volatile SingularAttribute<InstrumentHistory, InstrumentStatus> oldStatus;
	public static volatile SingularAttribute<InstrumentHistory, Boolean> addressUpdated;
	public static volatile SingularAttribute<InstrumentHistory, String> newSerialNo;
	public static volatile SingularAttribute<InstrumentHistory, Date> changeDate;
	public static volatile SingularAttribute<InstrumentHistory, Company> oldCompany;
	public static volatile SingularAttribute<InstrumentHistory, Integer> id;
	public static volatile SingularAttribute<InstrumentHistory, Address> newAddress;
	public static volatile SingularAttribute<InstrumentHistory, Boolean> statusUpdated;

	public static final String SERIAL_NO_UPDATED = "serialNoUpdated";
	public static final String NEW_STATUS = "newStatus";
	public static final String OLD_CONTACT = "oldContact";
	public static final String CONTACT_UPDATED = "contactUpdated";
	public static final String CHANGE_BY = "changeBy";
	public static final String NEW_COMPANY = "newCompany";
	public static final String NEW_CONTACT = "newContact";
	public static final String OLD_SERIAL_NO = "oldSerialNo";
	public static final String PLANT_NO_UPDATED = "plantNoUpdated";
	public static final String INSTRUMENT = "instrument";
	public static final String NEW_PLANT_NO = "newPlantNo";
	public static final String OLD_ADDRESS = "oldAddress";
	public static final String OLD_PLANT_NO = "oldPlantNo";
	public static final String COMPANY_UPDATED = "companyUpdated";
	public static final String OLD_STATUS = "oldStatus";
	public static final String ADDRESS_UPDATED = "addressUpdated";
	public static final String NEW_SERIAL_NO = "newSerialNo";
	public static final String CHANGE_DATE = "changeDate";
	public static final String OLD_COMPANY = "oldCompany";
	public static final String ID = "id";
	public static final String NEW_ADDRESS = "newAddress";
	public static final String STATUS_UPDATED = "statusUpdated";

}

