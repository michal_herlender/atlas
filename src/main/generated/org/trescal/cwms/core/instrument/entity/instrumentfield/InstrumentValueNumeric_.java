package org.trescal.cwms.core.instrument.entity.instrumentfield;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InstrumentValueNumeric.class)
public abstract class InstrumentValueNumeric_ extends org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentValue_ {

	public static volatile SingularAttribute<InstrumentValueNumeric, Double> value;

	public static final String VALUE = "value";

}

