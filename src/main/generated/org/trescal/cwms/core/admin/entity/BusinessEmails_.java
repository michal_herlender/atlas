package org.trescal.cwms.core.admin.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BusinessEmails.class)
public abstract class BusinessEmails_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<BusinessEmails, String> collections;
	public static volatile SingularAttribute<BusinessEmails, String> web;
	public static volatile SingularAttribute<BusinessEmails, String> quotations;
	public static volatile SingularAttribute<BusinessEmails, String> recall;
	public static volatile SingularAttribute<BusinessEmails, Integer> id;
	public static volatile SingularAttribute<BusinessEmails, String> goodsIn;
	public static volatile SingularAttribute<BusinessEmails, String> account;
	public static volatile SingularAttribute<BusinessEmails, String> sales;
	public static volatile SingularAttribute<BusinessEmails, String> autoService;
	public static volatile SingularAttribute<BusinessEmails, String> quality;

	public static final String COLLECTIONS = "collections";
	public static final String WEB = "web";
	public static final String QUOTATIONS = "quotations";
	public static final String RECALL = "recall";
	public static final String ID = "id";
	public static final String GOODS_IN = "goodsIn";
	public static final String ACCOUNT = "account";
	public static final String SALES = "sales";
	public static final String AUTO_SERVICE = "autoService";
	public static final String QUALITY = "quality";

}

