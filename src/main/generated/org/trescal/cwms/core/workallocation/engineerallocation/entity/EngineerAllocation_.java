package org.trescal.cwms.core.workallocation.engineerallocation.entity;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.workallocation.engineerallocationmessage.entity.EngineerAllocationMessage;
import org.trescal.cwms.core.workflow.entity.TimeOfDay;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(EngineerAllocation.class)
public abstract class EngineerAllocation_ {

	public static volatile SingularAttribute<EngineerAllocation, LocalDate> allocatedOn;
	public static volatile SingularAttribute<EngineerAllocation, Contact> allocatedTo;
	public static volatile SingularAttribute<EngineerAllocation, Boolean> active;
	public static volatile SingularAttribute<EngineerAllocation, Department> dept;
	public static volatile SingularAttribute<EngineerAllocation, LocalDate> allocatedFor;
	public static volatile SingularAttribute<EngineerAllocation, JobItem> itemAllocated;
	public static volatile SingularAttribute<EngineerAllocation, LocalDate> allocatedUntil;
	public static volatile SingularAttribute<EngineerAllocation, TimeOfDay> timeOfDayUntil;
	public static volatile SingularAttribute<EngineerAllocation, Integer> estimatedAllocationTime;
	public static volatile SetAttribute<EngineerAllocation, EngineerAllocationMessage> messages;
	public static volatile SingularAttribute<EngineerAllocation, Contact> allocatedBy;
	public static volatile SingularAttribute<EngineerAllocation, Integer> id;
	public static volatile SingularAttribute<EngineerAllocation, TimeOfDay> timeOfDay;

	public static final String ALLOCATED_ON = "allocatedOn";
	public static final String ALLOCATED_TO = "allocatedTo";
	public static final String ACTIVE = "active";
	public static final String DEPT = "dept";
	public static final String ALLOCATED_FOR = "allocatedFor";
	public static final String ITEM_ALLOCATED = "itemAllocated";
	public static final String ALLOCATED_UNTIL = "allocatedUntil";
	public static final String TIME_OF_DAY_UNTIL = "timeOfDayUntil";
	public static final String ESTIMATED_ALLOCATION_TIME = "estimatedAllocationTime";
	public static final String MESSAGES = "messages";
	public static final String ALLOCATED_BY = "allocatedBy";
	public static final String ID = "id";
	public static final String TIME_OF_DAY = "timeOfDay";

}

