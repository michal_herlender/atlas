package org.trescal.cwms.core.workallocation.engineerallocationmessage.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.workallocation.engineerallocation.entity.EngineerAllocation;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(EngineerAllocationMessage.class)
public abstract class EngineerAllocationMessage_ {

	public static volatile SingularAttribute<EngineerAllocationMessage, EngineerAllocation> allocation;
	public static volatile SingularAttribute<EngineerAllocationMessage, Date> setOn;
	public static volatile SingularAttribute<EngineerAllocationMessage, Integer> id;
	public static volatile SingularAttribute<EngineerAllocationMessage, String> message;
	public static volatile SingularAttribute<EngineerAllocationMessage, Contact> setBy;

	public static final String ALLOCATION = "allocation";
	public static final String SET_ON = "setOn";
	public static final String ID = "id";
	public static final String MESSAGE = "message";
	public static final String SET_BY = "setBy";

}

