package org.trescal.cwms.core.misc.entity.exception;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Exception.class)
public abstract class Exception_ {

	public static volatile SingularAttribute<Exception, Date> date;
	public static volatile SingularAttribute<Exception, String> exception;
	public static volatile SingularAttribute<Exception, Boolean> reviewed;
	public static volatile SingularAttribute<Exception, String> comment;
	public static volatile SingularAttribute<Exception, Integer> id;
	public static volatile SingularAttribute<Exception, String> stackTrace;
	public static volatile SingularAttribute<Exception, String> user;
	public static volatile SingularAttribute<Exception, String> url;

	public static final String DATE = "date";
	public static final String EXCEPTION = "exception";
	public static final String REVIEWED = "reviewed";
	public static final String COMMENT = "comment";
	public static final String ID = "id";
	public static final String STACK_TRACE = "stackTrace";
	public static final String USER = "user";
	public static final String URL = "url";

}

