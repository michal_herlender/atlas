package org.trescal.cwms.core.misc.entity.hook;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.misc.entity.hookactivity.HookActivity;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Hook.class)
public abstract class Hook_ {

	public static volatile SingularAttribute<Hook, ItemActivity> activity;
	public static volatile SingularAttribute<Hook, Boolean> alwaysPossible;
	public static volatile SetAttribute<Hook, HookActivity> hookActivities;
	public static volatile SingularAttribute<Hook, String> name;
	public static volatile SingularAttribute<Hook, Boolean> beginActivity;
	public static volatile SingularAttribute<Hook, Boolean> active;
	public static volatile SingularAttribute<Hook, Integer> id;
	public static volatile SingularAttribute<Hook, Boolean> completeActivity;

	public static final String ACTIVITY = "activity";
	public static final String ALWAYS_POSSIBLE = "alwaysPossible";
	public static final String HOOK_ACTIVITIES = "hookActivities";
	public static final String NAME = "name";
	public static final String BEGIN_ACTIVITY = "beginActivity";
	public static final String ACTIVE = "active";
	public static final String ID = "id";
	public static final String COMPLETE_ACTIVITY = "completeActivity";

}

