package org.trescal.cwms.core.misc.entity.hookactivity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(HookActivity.class)
public abstract class HookActivity_ {

	public static volatile SingularAttribute<HookActivity, Hook> hook;
	public static volatile SingularAttribute<HookActivity, ItemActivity> activity;
	public static volatile SingularAttribute<HookActivity, Boolean> beginActivity;
	public static volatile SingularAttribute<HookActivity, Integer> id;
	public static volatile SingularAttribute<HookActivity, ItemState> state;
	public static volatile SingularAttribute<HookActivity, Boolean> completeActivity;

	public static final String HOOK = "hook";
	public static final String ACTIVITY = "activity";
	public static final String BEGIN_ACTIVITY = "beginActivity";
	public static final String ID = "id";
	public static final String STATE = "state";
	public static final String COMPLETE_ACTIVITY = "completeActivity";

}

