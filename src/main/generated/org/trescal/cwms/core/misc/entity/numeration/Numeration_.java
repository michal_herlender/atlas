package org.trescal.cwms.core.misc.entity.numeration;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Numeration.class)
public abstract class Numeration_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<Numeration, Integer> year;
	public static volatile SingularAttribute<Numeration, Integer> count;
	public static volatile SingularAttribute<Numeration, NumerationType> numerationType;
	public static volatile SingularAttribute<Numeration, Integer> id;

	public static final String YEAR = "year";
	public static final String COUNT = "count";
	public static final String NUMERATION_TYPE = "numerationType";
	public static final String ID = "id";

}

