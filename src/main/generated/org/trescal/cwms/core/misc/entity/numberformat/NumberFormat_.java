package org.trescal.cwms.core.misc.entity.numberformat;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(NumberFormat.class)
public abstract class NumberFormat_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<NumberFormat, String> format;
	public static volatile SingularAttribute<NumberFormat, NumerationType> numerationType;
	public static volatile SingularAttribute<NumberFormat, Boolean> global;
	public static volatile SingularAttribute<NumberFormat, Integer> id;
	public static volatile SingularAttribute<NumberFormat, Boolean> universal;

	public static final String FORMAT = "format";
	public static final String NUMERATION_TYPE = "numerationType";
	public static final String GLOBAL = "global";
	public static final String ID = "id";
	public static final String UNIVERSAL = "universal";

}

