package org.trescal.cwms.batch.config.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BatchJobExecutionContext.class)
public abstract class BatchJobExecutionContext_ {

	public static volatile SingularAttribute<BatchJobExecutionContext, String> serializedContext;
	public static volatile SingularAttribute<BatchJobExecutionContext, Long> id;
	public static volatile SingularAttribute<BatchJobExecutionContext, String> shortContext;

	public static final String SERIALIZED_CONTEXT = "serializedContext";
	public static final String ID = "id";
	public static final String SHORT_CONTEXT = "shortContext";

}

