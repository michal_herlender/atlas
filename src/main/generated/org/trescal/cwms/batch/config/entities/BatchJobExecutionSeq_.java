package org.trescal.cwms.batch.config.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BatchJobExecutionSeq.class)
public abstract class BatchJobExecutionSeq_ {

	public static volatile SingularAttribute<BatchJobExecutionSeq, Long> id;

	public static final String ID = "id";

}

