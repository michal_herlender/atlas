package org.trescal.cwms.batch.config.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BatchStepExecutionSeq.class)
public abstract class BatchStepExecutionSeq_ {

	public static volatile SingularAttribute<BatchStepExecutionSeq, Long> id;

	public static final String ID = "id";

}

