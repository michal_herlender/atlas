package org.trescal.cwms.batch.config.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BatchStepExecutionContext.class)
public abstract class BatchStepExecutionContext_ {

	public static volatile SingularAttribute<BatchStepExecutionContext, String> serializedContext;
	public static volatile SingularAttribute<BatchStepExecutionContext, Long> id;
	public static volatile SingularAttribute<BatchStepExecutionContext, String> shortContext;

	public static final String SERIALIZED_CONTEXT = "serializedContext";
	public static final String ID = "id";
	public static final String SHORT_CONTEXT = "shortContext";

}

