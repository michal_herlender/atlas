package org.trescal.cwms.batch.config.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BatchJobSeq.class)
public abstract class BatchJobSeq_ {

	public static volatile SingularAttribute<BatchJobSeq, Long> id;

	public static final String ID = "id";

}

