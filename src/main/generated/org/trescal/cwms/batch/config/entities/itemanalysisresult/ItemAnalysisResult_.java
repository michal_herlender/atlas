package org.trescal.cwms.batch.config.entities.itemanalysisresult;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.batch.config.entities.batchstepexecution.BatchStepExecution;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatFieldNameEnum;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ItemAnalysisResult.class)
public abstract class ItemAnalysisResult_ {

	public static volatile SingularAttribute<ItemAnalysisResult, ExchangeFormatFieldNameEnum> field;
	public static volatile SingularAttribute<ItemAnalysisResult, Integer> index;
	public static volatile SingularAttribute<ItemAnalysisResult, String> warning;
	public static volatile SingularAttribute<ItemAnalysisResult, Integer> id;
	public static volatile SingularAttribute<ItemAnalysisResult, String> error;
	public static volatile SingularAttribute<ItemAnalysisResult, BatchStepExecution> stepExecution;

	public static final String FIELD = "field";
	public static final String INDEX = "index";
	public static final String WARNING = "warning";
	public static final String ID = "id";
	public static final String ERROR = "error";
	public static final String STEP_EXECUTION = "stepExecution";

}

