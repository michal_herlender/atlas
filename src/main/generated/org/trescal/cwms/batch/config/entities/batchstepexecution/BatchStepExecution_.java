package org.trescal.cwms.batch.config.entities.batchstepexecution;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.batch.config.entities.batchjobexecution.BatchJobExecution;
import org.trescal.cwms.batch.config.entities.itemanalysisresult.ItemAnalysisResult;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BatchStepExecution.class)
public abstract class BatchStepExecution_ {

	public static volatile SingularAttribute<BatchStepExecution, Integer> commitCount;
	public static volatile SingularAttribute<BatchStepExecution, Integer> processSkipCount;
	public static volatile SingularAttribute<BatchStepExecution, String> exitMessage;
	public static volatile SingularAttribute<BatchStepExecution, Integer> readCount;
	public static volatile SingularAttribute<BatchStepExecution, Long> version;
	public static volatile ListAttribute<BatchStepExecution, ItemAnalysisResult> itemAnalysisResults;
	public static volatile SingularAttribute<BatchStepExecution, Date> lastUpdated;
	public static volatile SingularAttribute<BatchStepExecution, Integer> rollbackCount;
	public static volatile SingularAttribute<BatchStepExecution, String> stepName;
	public static volatile SingularAttribute<BatchStepExecution, String> exitCode;
	public static volatile SingularAttribute<BatchStepExecution, Integer> writeCount;
	public static volatile SingularAttribute<BatchStepExecution, Integer> writeSkipCount;
	public static volatile SingularAttribute<BatchStepExecution, Date> startTime;
	public static volatile SingularAttribute<BatchStepExecution, Integer> id;
	public static volatile SingularAttribute<BatchStepExecution, Date> endTime;
	public static volatile SingularAttribute<BatchStepExecution, BatchJobExecution> jobExecution;
	public static volatile SingularAttribute<BatchStepExecution, Integer> readSkipCount;
	public static volatile SingularAttribute<BatchStepExecution, String> status;
	public static volatile SingularAttribute<BatchStepExecution, Integer> filterCount;

	public static final String COMMIT_COUNT = "commitCount";
	public static final String PROCESS_SKIP_COUNT = "processSkipCount";
	public static final String EXIT_MESSAGE = "exitMessage";
	public static final String READ_COUNT = "readCount";
	public static final String VERSION = "version";
	public static final String ITEM_ANALYSIS_RESULTS = "itemAnalysisResults";
	public static final String LAST_UPDATED = "lastUpdated";
	public static final String ROLLBACK_COUNT = "rollbackCount";
	public static final String STEP_NAME = "stepName";
	public static final String EXIT_CODE = "exitCode";
	public static final String WRITE_COUNT = "writeCount";
	public static final String WRITE_SKIP_COUNT = "writeSkipCount";
	public static final String START_TIME = "startTime";
	public static final String ID = "id";
	public static final String END_TIME = "endTime";
	public static final String JOB_EXECUTION = "jobExecution";
	public static final String READ_SKIP_COUNT = "readSkipCount";
	public static final String STATUS = "status";
	public static final String FILTER_COUNT = "filterCount";

}

