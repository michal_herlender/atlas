package org.trescal.cwms.batch.config.entities.backgroundtask;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.batch.config.AutoSubmitPolicyEnum;
import org.trescal.cwms.batch.config.entities.BatchJobInstance;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.ExchangeFormatFile;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BackgroundTask.class)
public abstract class BackgroundTask_ extends org.trescal.cwms.core.audit.Allocated_ {

	public static volatile SingularAttribute<BackgroundTask, Contact> submittedBy;
	public static volatile SingularAttribute<BackgroundTask, ExchangeFormat> ef;
	public static volatile SingularAttribute<BackgroundTask, AutoSubmitPolicyEnum> autoSubmitPolicy;
	public static volatile SingularAttribute<BackgroundTask, ExchangeFormatFile> efFile;
	public static volatile SingularAttribute<BackgroundTask, Boolean> notified;
	public static volatile SingularAttribute<BackgroundTask, Contact> notifiee;
	public static volatile SingularAttribute<BackgroundTask, Integer> id;
	public static volatile SingularAttribute<BackgroundTask, Date> submitedOn;
	public static volatile SingularAttribute<BackgroundTask, Subdiv> clientSubdiv;
	public static volatile SingularAttribute<BackgroundTask, BatchJobInstance> jobInstance;

	public static final String SUBMITTED_BY = "submittedBy";
	public static final String EF = "ef";
	public static final String AUTO_SUBMIT_POLICY = "autoSubmitPolicy";
	public static final String EF_FILE = "efFile";
	public static final String NOTIFIED = "notified";
	public static final String NOTIFIEE = "notifiee";
	public static final String ID = "id";
	public static final String SUBMITED_ON = "submitedOn";
	public static final String CLIENT_SUBDIV = "clientSubdiv";
	public static final String JOB_INSTANCE = "jobInstance";

}

