package org.trescal.cwms.batch.config.entities.batchjobexecution;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.batch.config.entities.BatchJobExecutionParams;
import org.trescal.cwms.batch.config.entities.BatchJobInstance;
import org.trescal.cwms.batch.config.entities.batchstepexecution.BatchStepExecution;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BatchJobExecution.class)
public abstract class BatchJobExecution_ {

	public static volatile SingularAttribute<BatchJobExecution, String> jobConfigurationLocation;
	public static volatile SingularAttribute<BatchJobExecution, String> exitMessage;
	public static volatile ListAttribute<BatchJobExecution, BatchJobExecutionParams> params;
	public static volatile SingularAttribute<BatchJobExecution, Long> version;
	public static volatile SingularAttribute<BatchJobExecution, Date> lastUpdated;
	public static volatile SingularAttribute<BatchJobExecution, Date> createTime;
	public static volatile ListAttribute<BatchJobExecution, BatchStepExecution> stepExecutions;
	public static volatile SingularAttribute<BatchJobExecution, String> exitCode;
	public static volatile SingularAttribute<BatchJobExecution, Date> startTime;
	public static volatile SingularAttribute<BatchJobExecution, Integer> id;
	public static volatile SingularAttribute<BatchJobExecution, Date> endTime;
	public static volatile SingularAttribute<BatchJobExecution, String> status;
	public static volatile SingularAttribute<BatchJobExecution, BatchJobInstance> jobInstance;

	public static final String JOB_CONFIGURATION_LOCATION = "jobConfigurationLocation";
	public static final String EXIT_MESSAGE = "exitMessage";
	public static final String PARAMS = "params";
	public static final String VERSION = "version";
	public static final String LAST_UPDATED = "lastUpdated";
	public static final String CREATE_TIME = "createTime";
	public static final String STEP_EXECUTIONS = "stepExecutions";
	public static final String EXIT_CODE = "exitCode";
	public static final String START_TIME = "startTime";
	public static final String ID = "id";
	public static final String END_TIME = "endTime";
	public static final String STATUS = "status";
	public static final String JOB_INSTANCE = "jobInstance";

}

