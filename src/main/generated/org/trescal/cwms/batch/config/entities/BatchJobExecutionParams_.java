package org.trescal.cwms.batch.config.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.batch.config.entities.batchjobexecution.BatchJobExecution;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BatchJobExecutionParams.class)
public abstract class BatchJobExecutionParams_ {

	public static volatile SingularAttribute<BatchJobExecutionParams, Date> dateValue;
	public static volatile SingularAttribute<BatchJobExecutionParams, String> stringValue;
	public static volatile SingularAttribute<BatchJobExecutionParams, BatchJobExecution> batchJobExecution;
	public static volatile SingularAttribute<BatchJobExecutionParams, String> keyName;
	public static volatile SingularAttribute<BatchJobExecutionParams, Double> doubleValue;
	public static volatile SingularAttribute<BatchJobExecutionParams, String> type;
	public static volatile SingularAttribute<BatchJobExecutionParams, Character> identifiying;
	public static volatile SingularAttribute<BatchJobExecutionParams, Long> longValue;

	public static final String DATE_VALUE = "dateValue";
	public static final String STRING_VALUE = "stringValue";
	public static final String BATCH_JOB_EXECUTION = "batchJobExecution";
	public static final String KEY_NAME = "keyName";
	public static final String DOUBLE_VALUE = "doubleValue";
	public static final String TYPE = "type";
	public static final String IDENTIFIYING = "identifiying";
	public static final String LONG_VALUE = "longValue";

}

