package org.trescal.cwms.batch.config.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.trescal.cwms.batch.config.entities.backgroundtask.BackgroundTask;
import org.trescal.cwms.batch.config.entities.batchjobexecution.BatchJobExecution;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BatchJobInstance.class)
public abstract class BatchJobInstance_ {

	public static volatile SingularAttribute<BatchJobInstance, String> jobName;
	public static volatile SingularAttribute<BatchJobInstance, BackgroundTask> bgTask;
	public static volatile ListAttribute<BatchJobInstance, BatchJobExecution> jobExecutions;
	public static volatile SingularAttribute<BatchJobInstance, String> jobKey;
	public static volatile SingularAttribute<BatchJobInstance, Integer> id;
	public static volatile SingularAttribute<BatchJobInstance, Long> version;

	public static final String JOB_NAME = "jobName";
	public static final String BG_TASK = "bgTask";
	public static final String JOB_EXECUTIONS = "jobExecutions";
	public static final String JOB_KEY = "jobKey";
	public static final String ID = "id";
	public static final String VERSION = "version";

}

