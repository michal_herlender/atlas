import { css } from "../../thirdparty/lit-element/lit-element.js";

export let styling = css`

.upload-message {
	text-align: center;
}

#subFolders {
    visibility : visible;
}

progress {
	width : 70%;
}

h3 {
	margin: 0px;
}

.upload-message {
	text-align: center;
}

progress[value]::-webkit-progress-value::before {
	content: '80%';
 	position: absolute;
 	right: 0;
 	top: -125%;
}

progress {
	width : 70%;
}

#progressValue {
	display : block;
}

`;