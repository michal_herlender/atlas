import { html, directive } from '../../thirdparty/lit-html/lit-html.js';
import {repeat} from "../../thirdparty/lit-html/directives/repeat.js";
import {unsafeHTML} from '../../thirdparty/lit-html/directives/unsafe-html.js';
import '../../components/cwms-translate/cwms-translate.js';
export let htmlTemplate = (context) => html `
<cwms-overlay @cwms-overlay-closed="${context.cancel}">
 <cwms-translate slot="cwms-overlay-title" code="viewjob.uploadcertfile">Upload Files</cwms-translate>
  <div slot="cwms-overlay-body" >
    <form method="POST" enctype="multipart/form-data">
         <div class="marg" id="subFolders">
            Root Folder <input type="radio" name="directory" value="" 
             checked/>
            ${repeat(context.filesName, (fw)=>
                 html`
                 ${fw} <input type="radio" name="directory" value="${fw}"/>
                `
            )}
         </div><br>
        <center><div>
             <input type="file" name="file" size="36" id="fileInput" @change="${context.readFileName}"/>
            <button type="button" id="uploadButton" @click="${context.uploadFile}" >
			   <cwms-translate code="tpcosts.uploadfile">Upload</cwms-translate>
            </button>
            ${ context.web ? html `
                <button type="button" @click="${context.hide}" id="cancelButton">
					<cwms-translate code="cancel">Cancel</cwms-translate>
                </button>` : ''}
         </div></center><br>
    </form>
    ${context.uploadProgress == null || context.file == null ? `` :
            html`<center>
                    <!-- 110 % : 100% for file upload, extra 10% for server response -->
                    <span id="progressValue" > ${formatProgressPrcntg(context.uploadProgress - 10) } </span>
                    <progress value="${context.uploadProgress }" max="110"></progress>
                </center></br>`
    }
    ${context.uploadResult == undefined  || context.uploadResult.message == null ? `` :
            html`<div class="upload-message">
                <h3 style="color:${context.uploadResult.success ? `green` : `red`}" > ${ context.uploadResult.message } </h3>
            </div><br>`
    }
  </div>
</cwms-overlay>
`;

const formatProgressPrcntg = directive(num => (part) => {
    let v = 0;
    if(num > 0)
        v = Math.round( num * 100 + Number.EPSILON ) / 100
    part.setValue(unsafeHTML(`${v} %`));
});