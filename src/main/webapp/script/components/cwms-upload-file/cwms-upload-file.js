import { htmlTemplate } from "./cwms-upload-file.html.js";
import { styling } from "./cwms-upload-file.css.js";
import { LitElement } from "../../thirdparty/lit-element/lit-element.js";
import { MetaDataObtainer }  from "../../tools/metadataObtainer.js";
window.customElements.define('cwms-upload-file', class CwmsUploadFile extends LitElement {
	
	// watched properties
    static get properties() {
        return {
            systemComponentId : {attribute: 'system-component-id'},
            thisEntityId1 : {attribute: 'entity-id1'},
            thisEntityId2 : {attribute: 'entity-id2'},
            filesName : {attribute: 'files-name'},
            renameTo : {attribute: 'rename-to'},
			web : {attribute: 'web'},
			file : { type : Object },
			uploadResult : { type : Object },
			uploadProgress : { type : Number }
        }
    }
    
    static get styles() {
        return styling;
    }
    
    constructor() {
		super();
		this.reset();
    }

    /* Overridden : see lit-element docs */
    render() {  
        return htmlTemplate(this);
    }
	
	firstUpdated() {
		//if filesName is empty, hide the sub folders div
		if(this.filesName === "[]" || !this.filesName){
			this.shadowRoot.getElementById('subFolders').style.visibility = 'hidden';
		}
		// transform filesName attribute to Array
		this.filesName = this.filesName.replace('[', '').replace(']', '').split(', ');
	}

    show() {
		this.shadowRoot.querySelector('cwms-overlay').show();
    }
    
    hide() {
    	if(this.shadowRoot.querySelector('#fileInput')!= null)
    		this.shadowRoot.querySelector('#fileInput').value = "";
    	
    	if(this.shadowRoot.querySelector('cwms-overlay').open)
    		this.shadowRoot.querySelector('cwms-overlay').hide();
    }   
    
    cancel() {
    	if(this.ajaxCall != null)
    		this.ajaxCall.abort();
		this.hide();
		this.reset();
		// not refresh the parent page if we have an existing iframe (the case of "viewquotationrequest" iframe)
		if(!parent.document.getElementById("iframe-id")) {
			parent.location.reload(true);
		}
	}
	
	readFileName(e) {
    	if(this.shadowRoot!= null){
    		this.reset();
			this.file = this.shadowRoot.querySelector('#fileInput').file;
    	}
    }
	
	uploadFile() {
		// form data
    	var formdata = new FormData();
		formdata.append('compid', this.systemComponentId);
		formdata.append('compEntityId', this.thisEntityId1);
		formdata.append('compEntityVersion', this.thisEntityId2);
		formdata.append('renameTo', this.renameTo);
		formdata.append('subdirectory', this.shadowRoot.querySelector("input[name=directory]:checked").value);
		formdata.append('file', this.shadowRoot.querySelector('#fileInput').files[0]);
		// get the value of the file to use it before the launch of progress bar
		this.file = this.shadowRoot.querySelector('#fileInput').files[0];
		// include the CSRF token in the header
		var csrfToken = MetaDataObtainer.csrfToken;
		// send data using XMLHttpRequest
		this.ajaxCall = new XMLHttpRequest();
		this.ajaxCall.upload.onprogress = (e) => {
			if(e.lengthComputable){
				var max = e.total;
				var current = e.loaded;
				var percentage = (current * 100)/max;
				this.uploadProgress = percentage;
			}
		};
		this.ajaxCall.onload = () => {
			if (this.ajaxCall.readyState === this.ajaxCall.DONE) {
				if (this.ajaxCall.status === 200) {
					this.uploadProgress += 10;
					this.uploadResult = JSON.parse(this.ajaxCall.responseText);
				}
			}
		};
		this.ajaxCall.open("POST","uploadfile.json", true);   
		this.ajaxCall.setRequestHeader("X-CSRF-TOKEN", csrfToken);
        this.ajaxCall.send(formdata);			
	}

	reset() {
		this.uploadProgress = null;
		this.uploadResult = null;
		this.ajaxCall = null;

	}
    
});
