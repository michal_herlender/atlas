import { htmlTemplate } from "./cwms-ef.html.js";
import { styling } from "./cwms-ef.css.js";
import { LitElement, html } from '../../thirdparty/lit-element/lit-element.js';
import { translationConfig } from '../tools/datatables.translation.config.js';
import CwmsTranslate from '../cwms-translate/cwms-translate.js';

window.customElements.define('cwms-ef', class CwmsEF extends LitElement {

    static get dataURL(){ return "showExchangeFormats.json" };
    static get downloadURL(){ return "downloadExchangeFormatTemplate" };
    static get editURL(){ return "exchangeformat.htm?action=edit" };
    static get addURL(){ return "exchangeformat.htm?action=create" };
    static get deleteURL(){ return "deleteExchangeFormat.json" };
    datatable;

    // watched properties
    static get properties() {
        return {
            data: { type : Array },
            companyId : { attribute : 'company-id' },
            companyRole : { attribute : 'company-role' },
            subdivId : { attribute : 'subdiv-id' },
            permission : { type : Boolean, attribute : 'permission' }
            
        };
    }
    
    static get styles() {
        return styling;
    }
    
    deleteButtonTemplate(efId){
    	return html`
				<a class="mainlink" 
					@click="${ e => {e.preventDefault(); this.deleteExchangeFormat(efId)} }" href="">
				<cwms-translate code="delete">Delete</cwms-translate>
			</a>
		`;
    }

    constructor(){
        super();
    }

    /* Overridden : see lit-element docs */
    render() {
    	
    	// refresh in case of param loadtab=exchangeformat-tab exist (in case of
		// redirection)
    	var url = new URL(window.location.href);
        if(url.searchParams.get("loadtab")=='exchangeformat-tab' && this.datatable === undefined){
        	this.refresh();
        }
        // destroy dataTables if exist
        if(this.datatable !== undefined) {
        	this.datatable.destroy();
        }    
        return htmlTemplate(this);
    }
    
    /* Overridden : see lit-element docs */
    updated(){
        this.datatable = $(this.shadowRoot.querySelector('table')).DataTable({"language": translationConfig(),
        	columnDefs: [{ targets: "_all", type: 'diacritics-neutralise' }]
        });
    }
    
    /* public methods */
    refresh() {
    	// set default message
    	if( this.shadowRoot.querySelector('.dataTables_empty') !== null )
    			this.shadowRoot.querySelector('.dataTables_empty').innerHTML = CwmsTranslate.getMessage('newassetinst.loadingresults', 'Loading results...');
    	
    	// construct url
    	let url;
    	if(this._constructParams() !== null && this._constructParams() !== undefined)
    		url = this._constructParams();
    	else{
    		console.error(this.localName, 'Please provide either attibutes : company-id or subdiv-id')
    		return;
    	}
    	
    	// fetch data
        fetch(url).then(res => res.text().then(text => {
        	// changing a watched property will ask to execute render()
			// automatically afterwards
            this.data = JSON.parse(text);
        }));
    }
    
    deleteExchangeFormat(efId){
    	const yes = CwmsTranslate.getMessage('yes','Yes');
    	const no = CwmsTranslate.getMessage('no','No');
    	$j.prompt(CwmsTranslate.getMessage('delete','Delete')+" ?", {
    		buttons: { [no]: false, [yes]: true },
    		submit: function(e,v,m,f){
    			if(v){
    				fetch(CwmsEF.deleteURL+"?id="+efId).then(res=> res.text().then(text=>{
    					let json = JSON.parse(text);
    					if(json.success===true){
    						// refresh page
    						window.location.reload(true);
    					}else{
    						$j.prompt.close();
    						$j.prompt(json.message);
    						$j.prompt(res.message);
    						
    					}
    				}));
    			}
    		}
    	});
    }
    
    /* private methods */
    _constructParams(){
    	// construct params
    	if(this.subdivId !== undefined)
    		return CwmsEF.dataURL + `?subdivid=${this.subdivId}`;
    	else if(this.companyId !== undefined)
    		return CwmsEF.dataURL + `?coid=${this.companyId}`;
    	else 
    		return undefined;
    }
    
    convertTimeStampToDate(timeStamp){
    	return new Date(timeStamp).toLocaleString("en-GB");
    }
});
