import { css } from '../../thirdparty/lit-element/lit-element.js'

export let styling = css`

table {
    font-size: 11px;
    border-collapse: collapse !important;
}
.bold {
	font-weight: bold;
}
.dataTables_filter {
    margin-bottom: 5px;
}
.dataTables_length select {
	font-size: 11px;
}
.center-text {
	text-align: center;
}
img {
	vertical-align: bottom;
}

.float-right{
	float: right;
	text-decoration: underline;
	cursor: pointer;
	color: #0063be;
}

.float-right:hover{
	color:  #990033;
}

`;