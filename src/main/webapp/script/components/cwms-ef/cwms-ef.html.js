import {html, directive} from  '../../thirdparty/lit-html/lit-html.js';
import {repeat} from '../../thirdparty/lit-html/directives/repeat.js'
import {unsafeHTML} from '../../thirdparty/lit-html/directives/unsafe-html.js'

export let htmlTemplate = (context) => html `
<link rel="stylesheet" href="styles/structure/elements/table.css">
<link rel="stylesheet" href="styles/theme/theme.css">
<link rel="stylesheet" href="script/thirdparty/DataTables/datatables.min.css" />

<table class="default2">
    <thead>
    
    	${ context.permission === true ? 
	    	 html`<tr  style="height:20px;">
	    		<th colspan="8">
					${addButton(context)}
				</th>
	    	</tr>`
	    	 
	    	 : ''
    	 }
    	
    	<tr>
			<th><cwms-translate code="exchangeformat.organization" >Organization</cwms-translate></th>
			<th style="width:100px;" ><cwms-translate code="name" >Name</cwms-translate></th>
			<th style="width:80px;" ><cwms-translate code="type" >Type</cwms-translate></th>
			<th style="width:150px;" ><cwms-translate code="description" >Description</cwms-translate></th>
			<th><cwms-translate code="clientcompany" >Company</cwms-translate></th>
			<th><cwms-translate code="viewworkinstruction.modifiedon">Modified on</cwms-translate></th>
			<th><cwms-translate code="viewworkinstruction.modifiedby">Modified by</cwms-translate></th>
			<th><cwms-translate code="viewinstrument.actions">Actions</cwms-translate></th>
		</tr>
    </thead>
    <tbody>
       ${
           context.data===undefined || context.data.length==0 ?  `` :
           repeat(context.data,(i,index)=>html`
                <tr data-id="${i.id}">
                	<td class="bold" >
                		${showOrganisationColumn(i.businessCompanyName,i.businessSudivName)} 
            		</td>
                    <td class="bold" >${i.name}</td>
                    <td>${i.exchangeFormatType}</td>
                    <td>${i.description}</td>
                    <td class="bold" >${i.clientCompanyName}</td>
                    <td data-order="${i.lastModified}">${context.convertTimeStampToDate(i.lastModified)}</td>
                    <td>${i.lastModifiedBy}</td>
                    <td class="center-text">
                    	${downloadButton(i.exchangeFormatLevel , i.id, context)}
						${ context.permission === true ? 
							showEditButton(i.businessCompanyId, i.businessSubdivId , i.clientCompanyId, i.id, i.exchangeFormatLevel, context)
							: ''
						}
						${ context.permission === true ? 
							deleteButton(i.exchangeFormatLevel , i.id, context)
							: ''
						}
                    </td>
                </tr>
           `)
       }
    </tbody>
</table>

`;

/* Directives */

const showOrganisationColumn = directive((businessCompanyName, businessSudivName) => (part) => {
	if(businessCompanyName)
		part.setValue(unsafeHTML(`<img src="img/icons/building.png" width="24" height="16"/> ${businessCompanyName}`));
	else if(businessSudivName)
		part.setValue(unsafeHTML(`<img src="img/icons/house.png" width="24" height="16"/> ${businessSudivName}`));
	else
		part.setValue(unsafeHTML(`<img src="img/icons/global.png" width="24" height="16"/> [GLOBAL]`));
});

const downloadButton = (exchangeFormatLevel, efId, context) =>  
	exchangeFormatLevel !== 'GLOBAL'?html`
			<a href=${`${context.constructor.downloadURL}?id=${efId}`} target="_blank" class="mainlink">
				<cwms-translate code="exchangeformat.download">Download</cwms-translate>
			</a>	
	`:html``;


const showEditButton = directive((businessCompanyId, businessSubdivId, clientCompanyId, efId, exchangeFormatLevel, context) => (part) => {
	
	let url = context.constructor.editURL + `&id=${efId}`; 
	
	if ( context.companyRole == 'BUSINESS'){
		if( businessSubdivId != null && businessSubdivId !== undefined)
			url = url + `&businessSubdiv=${businessSubdivId}`;
		else if ( businessCompanyId != null && businessCompanyId !== undefined)
			url = url + `&businessCompany=${businessCompanyId}`;
	}else if (context.companyRole == 'CLIENT'){
		if( clientCompanyId!=null && clientCompanyId!==undefined )
			url = url + `&clientComp=${clientCompanyId}`;
	}
	if(exchangeFormatLevel != 'GLOBAL')
		part.setValue(unsafeHTML(`<a href="${url}" target="blank" class="mainlink"><cwms-translate code="edit">Edit</cwms-translate></a>`));
	else
		part.setValue(unsafeHTML(`<a href="${url}" target="blank" class="mainlink"><cwms-translate code="newcalibration.view">View</cwms-translate></a>`));
});

const deleteButton = directive((exchangeFormatLevel, efId, context) => (part) => {
	if(exchangeFormatLevel !== 'GLOBAL')
		part.setValue(context.deleteButtonTemplate(efId));
});


const addButton = directive((context) => (part) => {
	let url = context.constructor.addURL;
	if ( context.companyRole == 'BUSINESS'){
		if ( context.companyId!=null && context.companyId!==undefined && context.subdivId==null || context.subdivId==undefined){
			url = url + `&businessCompany=${context.companyId}`;
		}else if( context.subdivId!=null && context.subdivId!==undefined){
			url = url + `&businessSubdiv=${context.subdivId}`;
		}
	}else if (context.companyRole == 'CLIENT'){
		if( context.companyId!=null && context.companyId!==undefined){
			url = url + `&clientComp=${context.companyId}`;
			}
	}
	part.setValue(unsafeHTML(`<a href="${url}" class="float-right"><cwms-translate code="add">Create new exchange format</cwms-translate></a>`));
});



