import {htmlTemplate} from "./cwms-bg-tasks.html.js";
import {htmlRowDetailsTemplate} from "./cwms-bg-tasks-row-details.html.js";
import {styling} from "./cwms-bg-tasks.css.js";
import {LitElement} from '../../thirdparty/lit-element/lit-element.js';
import {render} from '../../thirdparty/lit-html/lit-html.js';
import '../../thirdparty/@vaadin/vaadin-grid/all-imports.js';
import '../../thirdparty/@vaadin/vaadin-progress-bar/vaadin-progress-bar.js';
import '../../thirdparty/@vaadin/vaadin-button/vaadin-button.js';
import '../../thirdparty/@vaadin/vaadin-icons/vaadin-icons.js';

export default class CwmsBgTasks extends LitElement {

    static get dataURL() {
        return "getAllBgTasks.json"
    };

    static get detailsURL() {
        return "getAllExecutionsByJobInstance.json"
    };
	
    static get properties() {
        return {
            items : { type : Array },
            currentPage : { type : Number },
            pageCount : { type : Number },
            personid : { attribute : 'person-id'},
            jobname : { attribute : 'job-name'},
            id : { attribute : 'id'}
        };
    }
    
    static get styles() {
        return styling(this);
    }
    
    constructor(){
        super();
        this.currentPage = 1;
        this.loading = false;
    }
    
    firstUpdated(){
    	this.refresh();
    }
    
    rowSelected (event){
    	let grid = event.target;
		let item = event.detail.value;
	    grid.selectedItems = item ? [item] : [];
	      	
	    if(grid._isDetailsOpened(item))
	    	grid.closeItemDetails(item);
	    else
	    	grid.openItemDetails(item);
    }
    
    refresh(){
    	this.showProgress(true);
        var url='';
        if(this.personid){
            url = url+'&personid=' + this.personid;
        }
        if(this.jobname){
            url = url + '&jobname=' + this.jobname;
        }
        if(this.id){
        	url = url + '&id=' + this.id;
        }
        fetch(CwmsBgTasks.dataURL + '?page=' + this.currentPage + '&maxResults=' + this.grid.pageSize + url)
            .then(res => res.ok ? res.json() : Promise.reject(res.statusText))
            .then(res => {
                this.items = res.results;
                this.currentPage = res.currentPage;
                this.pageCount = res.pageCount;
                this.showProgress(false);
            });
    }

    render() {  
        return htmlTemplate(this);
    }
    
    get grid() { return this.shadowRoot.querySelector('#jobsGrid'); }
    get progressBar() { return this.shadowRoot.querySelector('#jobsGridProgressBar'); }
    
    prevPage() {
    	if(this.currentPage > 1) {
    		this.currentPage--;
    		this.showProgress(true);
    		this.refresh();
    	}
    }
    
    nextPage() {
    	if(this.currentPage < this.pageCount) {
    		this.currentPage++;
    		this.showProgress(true);
    		this.refresh();
    	}
    }
    
    rowDetailsRenderer(root, column, rowData) {
    	
    	if(!this.isLoading && !root.firstElementChild){
    		
    		this.isLoading = true;
    		
    		render(htmlRowDetailsTemplate(column, rowData, null), root);
    		
    		fetch(CwmsBgTasks.detailsURL+'?batchJobId='+rowData.item.jobInstanceId).then(res => res.text().then(text => {
            	
            	let executionsAndSteps = JSON.parse(text);
            	
                render(htmlRowDetailsTemplate(column, rowData, executionsAndSteps), root);
                
                this.isLoading = false;
                
                setTimeout(function(){ 
            		document.querySelector('cwms-bg-tasks').shadowRoot.querySelector("vaadin-grid").notifyResize(); 
            	}, 100);
            	
            }));
    	}
    }
    
    showProgress(show){
    	this.progressBar.hidden=!show;
    }
    
};

window.customElements.define('cwms-bg-tasks', CwmsBgTasks);