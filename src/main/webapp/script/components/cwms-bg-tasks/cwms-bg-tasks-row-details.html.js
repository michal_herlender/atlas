import { html, render } from '../../thirdparty/lit-html/lit-html.js';
import '../../thirdparty/@vaadin/vaadin-progress-bar/vaadin-progress-bar.js';
import '../../thirdparty/@vaadin/vaadin-dialog/vaadin-dialog.js';

export let htmlRowDetailsTemplate = (column, rowData, executionsAndSteps) => html `

<center>
	<span class="label"><cwms-translate code='addjob.recordtype.file.exchangeformat'>Exchange format</cwms-translate></span>:&nbsp;<small>${rowData.item.exchangeFormatName}</small>
	<span class="label"><cwms-translate code='importbatch.file.name'>File name</cwms-translate></span>:&nbsp;</span><small>${rowData.item.fileName}</small>
	<span class="label"><cwms-translate code='importbatch.submit.policy'>Submit policy</cwms-translate></span>:&nbsp;</span><small>${rowData.item.submitPolicy}</small>
</center>
	
	${ executionsAndSteps == null ? 
		html`<vaadin-progress-bar indeterminate value="0"></vaadin-progress-bar>`
		: 
		html`
			<vaadin-grid .items="${executionsAndSteps}" height-by-rows theme="row-dividers compact column-borders row-stripes" >
				<vaadin-grid-column resizable auto-width .renderer="${treeToggleRenderer}" ></vaadin-grid-column>
				<vaadin-grid-tree-column resizable auto-width path="id"></vaadin-grid-tree-column>
				<vaadin-grid-column resizable auto-width header="Start Time" .renderer="${startTimeRenderer}"></vaadin-grid-column>
				<vaadin-grid-column resizable auto-width header="End Time" .renderer="${endTimeRenderer}"></vaadin-grid-column>
				<vaadin-grid-tree-column resizable auto-width path="status"></vaadin-grid-tree-column>
				<vaadin-grid-tree-column resizable auto-width path="exitCode"></vaadin-grid-tree-column>
				<vaadin-grid-column resizable width="100px" header="Exit message" .renderer="${exitMessageRenderer}"></vaadin-grid-column>
				<vaadin-grid-tree-column resizable auto-width path="readCount"></vaadin-grid-tree-column>
				<vaadin-grid-tree-column resizable auto-width path="readSkipCount"></vaadin-grid-tree-column>
				<vaadin-grid-tree-column resizable auto-width path="filterCount"></vaadin-grid-tree-column>
				<vaadin-grid-tree-column resizable auto-width path="processSkipCount"></vaadin-grid-tree-column>
				<vaadin-grid-tree-column resizable auto-width path="writeCount"></vaadin-grid-tree-column>
				<vaadin-grid-tree-column resizable auto-width path="writeSkipCount"></vaadin-grid-tree-column>
				<vaadin-grid-tree-column resizable auto-width path="commitCount"></vaadin-grid-tree-column>
				<vaadin-grid-tree-column resizable auto-width path="rollbackCount"></vaadin-grid-tree-column>
				
			</vaadin-grid>
		`
	}
`;

function treeToggleRenderer(root, column, rowData) {
	
	let grid = this.parentNode.parentNode;
	let toggler = root.firstElementChild;
	
    if (!toggler) {
    	toggler = window.document.createElement('vaadin-grid-tree-toggle');
    	/*toggler.addEventListener('expanded-changed', function(e) {
    		let grid = this.parentNode.parentNode;
    		if(toggler.expanded)
    			grid.collapseItem(toggler.item);
    		else
    			grid.expandItem(toggler.item);
    		//grid[toggler.expanded ? 'expandItem' : 'collapseItem'](toggler.item);
    	});*/
      root.appendChild(toggler);
    }

    toggler.item = rowData.item;
    toggler.leaf = rowData.item.level === 0 ? false : true;
    toggler.expanded = false;
    toggler.level = rowData.item.level;
    toggler.textContent = rowData.item.level === 0 ? "Execution" : "   Step : "+rowData.item.stepName;
    
}


function startTimeRenderer(root, column, rowData) {
    render(
      html`
        <div>${new Date(rowData.item.startTime).toLocaleString()}</div>
      `, 
      root
    );
}


function endTimeRenderer(root, column, rowData) {
    render(
      html`
        <div>${new Date(rowData.item.endTime).toLocaleString()}</div>
      `, 
      root
    );
}

function exitMessageRenderer(root, column, rowData) {
    render(
      html`
      	<div @click="${showMessage}" class="tooltip">
      		${rowData.item.exitMessage}
      		<vaadin-dialog></vaadin-dialog>
      	</div>
      `, 
      root
    );
    /*if (root.querySelector('div').getAttribute('listener') !== 'true') {
	    root.querySelector('div').addEventListener('click', function(e) {
	    	console.log(e.target.textContent);
	    	e.target.setAttribute('listener', 'true');
	    });
    }*/
}

function showMessage(e){
	let text = e.target.textContent;
	let dialog = e.target.querySelector('vaadin-dialog');
	dialog.renderer = function(root, dialog) {
      root.textContent = text;
    };
	dialog.opened = true;
	console.log(e.target.textContent);
}