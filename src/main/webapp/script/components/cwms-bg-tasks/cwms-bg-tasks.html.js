import { html, render } from '../../thirdparty/lit-html/lit-html.js';

export let htmlTemplate = (context) => html `

<vaadin-button theme="icon" class="small-text" @click="${context.refresh}">
	<iron-icon icon="vaadin:refresh"></iron-icon>
	<cwms-translate code="importbatch.refresh" >Refresh</cwms-translate> 
</vaadin-button>

<div class="progressBarWrapper">
	<vaadin-progress-bar id="jobsGridProgressBar" ?hidden=${false} indeterminate value="0"></vaadin-progress-bar>
</div>

<vaadin-grid id="jobsGrid" @active-item-changed="${context.rowSelected}" .items="${context.items}" 
		.rowDetailsRenderer="${context.rowDetailsRenderer}"
		height-by-rows page-size="5" theme="row-dividers wrap-cell-content compact column-borders row-stripes"
		column-reordering-allowed >
    <vaadin-grid-column resizable auto-width path="bgTaskId" header="Id"></vaadin-grid-column>
    <vaadin-grid-column resizable auto-width path="batchJobName">
    	<template class="header"><cwms-translate code='importbatch.task.type'>Task type</cwms-translate></template>
    </vaadin-grid-column>
	<vaadin-grid-column resizable auto-width path="submittedByFullName">
		<template class="header"><cwms-translate code='importbatch.submitter'>Submit policy</cwms-translate></template>
	</vaadin-grid-column>
	<vaadin-grid-column resizable auto-width .renderer="${submittedOnRenderer}">
		<template class="header"><cwms-translate code='importbatch.submitted.on'>Submitted on</cwms-translate></template>
	</vaadin-grid-column>
	<vaadin-grid-column resizable auto-width path="businessSubdivName">
		<template class="header"><cwms-translate code='exchangeformat.bussubdivlabel'>Business subdiv</cwms-translate></template>
	</vaadin-grid-column>
	<vaadin-grid-column resizable auto-width .renderer="${clientRenderer}">
		<template class="header"><cwms-translate code='client'>Client</cwms-translate></template>
	</vaadin-grid-column>
	<vaadin-grid-column resizable auto-width path="executions">
		<template class="header"><cwms-translate code='importbatch.executions'>Executions</cwms-translate></template>
	</vaadin-grid-column>
	<vaadin-grid-column resizable auto-width path="lastExecutionStatus">
		<template class="header"><cwms-translate code='importbatch.last.status'>Last Status</cwms-translate></template>
	</vaadin-grid-column>
	<vaadin-grid-column resizable .renderer="${lastExecutionMessageRenderer}" width="200px">
		<template class="header"><cwms-translate code='importbatch.last.execution.message'>Last Execution Message</cwms-translate></template>
	</vaadin-grid-column>
	<vaadin-grid-column resizable auto-width .renderer="${buttonsRenderer}" header=""></vaadin-grid-column>
</vaadin-grid>

${ context.items != undefined && context.items.length == 0 ? 
	html` <hr><center><cwms-translate code='nodata'>No data</cwms-translate></center>` 
	: html``
}
    
<br/>
    
<div style="float:right">
	<vaadin-button theme="icon" @click="${context.prevPage}" ?disabled="${context.currentPage==1}">
 		<iron-icon icon="vaadin:angle-left"></iron-icon>
	</vaadin-button>
	<span id="pageStats"> ${context.currentPage} / ${context.pageCount}</span> 
	<vaadin-button theme="icon" @click="${context.nextPage}" ?disabled="${context.currentPage==context.pageCount}">
 		<iron-icon icon="vaadin:angle-right"></iron-icon>
	</vaadin-button>
</div>
   
<br/>
<br/>
<br/>

`;

function buttonsRenderer(root, column, rowData) {
    render(
      html`
		${ rowData.item.lastExecutionStatus == 'COMPLETED' ? 
      		html`
      			<div class="inline" >
      				<cwms-translate code='analysis.result'>Last Analysis results</cwms-translate>
      			</div>
      			<a target="_blank"
      				href="analysisResults.htm?lastJobExecutionId=${rowData.item.lastJobExecutionId}">
      				
      				<iron-icon class="small-icon" icon="vaadin:external-link"></iron-icon>
      			</a>
      		` : ``
        }
      `, 
      root
    );
}


function submittedOnRenderer(root, column, rowData) {
    render(
      html`
        <div>${new Date(rowData.item.submittedOn).toLocaleString()}</div>
      `, 
      root
    );
}

function clientRenderer(root, column, rowData) {
    render(
      html`
        <div>${rowData.item.clientCompanyName}<br/>(${rowData.item.clientSubdivName})</div>
      `, 
      root
    );
}

function lastExecutionMessageRenderer(root, column, rowData) {
    render(
      html`
        ${rowData.item.lastExecutionMessage}
      `, 
      root
    );
}


