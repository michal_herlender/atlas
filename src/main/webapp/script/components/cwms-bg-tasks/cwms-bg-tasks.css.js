import { css } from '../../thirdparty/lit-element/lit-element.js'

export let styling = (context) => css`
vaadin-grid {
	--lumo-font-size-s : 11px;
	--lumo-font-size-m : 13px;
}
.progressBarWrapper {
	min-height : 16px;
	//padding-top: 8px;
}

.label {
	font-weight : 500;
}

.tooltip {
	display: inline;
	border-bottom: 1px dotted black; 
	white-space: nowrap; 
	overflow: hidden;
	text-overflow: ellipsis;
}

.inline {
	display : inline;
}

.small-icon {
	width : 15px;
    cursor: pointer;
    --iron-icon-fill-color : hsl(214, 90%, 52%) !important;
    margin-left:8px;
}

.small-text {
	font-size : 11px !important;
}

</style>


`;