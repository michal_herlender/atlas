import {html, LitElement} from "../../thirdparty/lit-element/lit-element.js";
import {textStyles} from "../tools/styles/text.css.js";
import "../cwms-translate/cwms-translate.js";

class CwmsWarningOnchange extends LitElement {

    static get propeties() {
        return {
            messageCode: {type: String},
            messageDefault: {type: String},
            obervableId: {type: String},
            showWarning: {type: Boolean, attributes: false}
        }
    }

    static get styles() {
        return [textStyles];
    }

    constructor() {
        super();
        this.showWarning = false;
        this.observableId = "serviceTypeId";
        this.messageCode = "editquot.servicetype.changed";
        this.messageDefault = "The service type has changed. Please click on “Save” to update the price source table accordingly and select a new price.";
    }

    #startValue;

    connectedCallback() {
        super.connectedCallback();
        const observable = document.getElementById(this.observableId);
        this.#startValue = observable?.value;
        observable?.addEventListener("input", e => {
            const oldShow = this.showWarning;
            this.showWarning = e.currentTarget.value !== this.#startValue;
            this.requestUpdate("showWarning", oldShow);
        })
    }

    render() {
        return this.showWarning ? html`
            <span class="error">
                <b><cwms-translate code="warning">Warning</cwms-translate>:</b> <cwms-translate
                    code=${this.messageCode}>${this.messageDefault}</cwms-translate></span>` : html``
    }
}

customElements.define("cwms-warning-onchange", CwmsWarningOnchange);