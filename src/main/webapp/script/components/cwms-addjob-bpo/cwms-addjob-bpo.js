import {LitElement} from "../../thirdparty/lit-element/lit-element.js";
import {tableStyles} from "../tools/styles/table.css.js";
import {textStyles} from "../tools/styles/text.css.js";
import {styling} from "./cwms-addjob-bpo.css.js";
import {template} from "./cwms-addjob-bpo.html.js";
import cascadeStore from "../cwms-cascade-search/store/store.js";
import {urlWithParamsGenerator} from "../../tools/UrlGenerator.js";

export const keyCompany = "company";
export const keySubdiv = "subdiv";
export const keyContact = "contact";

class CwmsAddJobBPO extends LitElement {

    static get properties() {
        return {
            name: {type: String},
            personId: {type: Number},
            resultSize: {type: Number},
            bpoResultsSet: {type: Object}
        }
    }

    static get formAssociated() {
        return true;
    }

    #internals;

    constructor() {
        super();
        this.#internals = this.attachInternals();
        this.bpoResultsSet = {};
        cascadeStore.subscribe(() => {
            const {personId} = cascadeStore.getState();
            this.contactUpdated(personId);
        })
    }

    updateItems(items, key) {
        this.bpoResultsSet = {...this.bpoResultsSet, [key]: items};
        const formData = [...this.bpoItems(keyContact + "BPOs"), ...this.bpoItems(keySubdiv + "BPOs"), ...this.bpoItems(keyCompany + "BPOs")]
            .filter(i => i.selected ?? false).reduce((fd, i) => {
                fd.append(this.name, i.poId);
                return fd;
            }, new FormData());
        this.#internals.setFormValue(formData);
    }

    contactUpdated(personId) {
        if (personId === this.personId) return;
        this.personId = personId;
        if (personId) {
            fetch(urlWithParamsGenerator("bpo/relatedToContact.json", {personId}))
            .then(res => res.ok ? res.json() : Promise.reject(res.statusText))
            .then(result => {
                this.bpoResultsSet = result;
                this.resultSize = result.size;
            });
        }
        else {
            this.bpoResultsSet = [];
            this.resultSize = 0;
        }
    }

    static get styles() {
        return [tableStyles, textStyles, styling()]
    }

    bpoItems(key) {
        return this.bpoResultsSet[key] ?? [];
    }

    get hasContact() {
        return (this.bpoResultsSet.contactBPOs ?? []).length > 0;
    }

    get contactName() {
        return this.bpoResultsSet.contactName;
    }

    get hasSubdiv() {
        return (this.bpoResultsSet.subdivBPOs ?? []).length > 0;
    }

    get subdivName() {
        return this.bpoResultsSet.subdivName;
    }

    get hasCompany(){
        return (this.bpoResultsSet.companyBPOs ?? []).length > 0;
    }

    get companyName() {
        return this.bpoResultsSet.companyName;
    }

    get hasAny() {
        return this.hasSubdiv || this.hasCompany || this.hasContact;
    }


    toggleSelectItem(item, key) {
        this.updateItems(this.bpoResultsSet[key].map(i => i.poId === item.poId ?
            {...i, selected: !(i.selected ?? false)} : i), key);
    }

    toggleSelectAllItems(e) {
        const itemsKey = key => key + "BPOs";
        this.updateItems(this.bpoItems(itemsKey(keyCompany)).map(i => ({
            ...i,
            selected: e.currentTarget.checked
        })), itemsKey(keyCompany));
        this.updateItems(this.bpoItems(itemsKey(keyContact)).map(i => ({
            ...i,
            selected: e.currentTarget.checked
        })), itemsKey(keyContact));
        this.updateItems(this.bpoItems(itemKey(keySubdiv)).map(i => ({
            ...i,
            selected: e.currentTarget.checked
        })), itemsKey(keySubdiv));
    }


    render(){
        return template(this);
    }
}

customElements.define("cwms-addjob-bpo", CwmsAddJobBPO);