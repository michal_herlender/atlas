import {html} from "../../thirdparty/lit-element/lit-element.js";
import {repeat} from "../../thirdparty/lit-html/directives/repeat.js";
import {keyCompany, keyContact, keySubdiv} from "./cwms-addjob-bpo.js";
import {cwmsTranslateDirective} from "../cwms-translate/cwms-translate-directive.js";
import {currencyFormatWithZeroDirective, dateFormatDirective} from "../tools/formatingDirectives.js";


export const template = (context) =>
    context.hasAny
        ? html` 
        <table class="default2" id="itemBPOResults" >
          <thead>
            <tr>
              <th class="BPOselect" scope="col">
                <span>
                  <cwms-translate code="additems.selectall">Select All</cwms-translate>
                  <input type="checkbox" class="checkbox" @change=${context.toggleSelectAllItems} />
                </span>
              </th>
              <th class="BPOnumber" scope="col"><cwms-translate code="bpo.bpono">BPO No</cwms-translate></th>
              <th class="BPOcomment" scope="col"><cwms-translate code="bpo.comment">BPO comment</cwms-translate></th>
              <th class="BPOvalfrom" scope="col"><cwms-translate code="bpo.validfrom">Valid from</cwms-translate></th>
              <th class="BPOvalto" scope="col"><cwms-translate code="bpo.validto">Valid to</cwms-translate></th>
              <th class="BPOlimit" scope="col"><cwms-translate code="bpo.limitamount">Limit ammount</cwms-translate></th>
            </tr>
          </thead>
          <tbody>
          ${renderBPO(context)(keyContact)}
          ${renderBPO(context)(keySubdiv)}
          ${renderBPO(context)(keyCompany)}
          </tbody>
        </table>`
        : html` <div class="no-items bold center"><cwms-translate code="addjob.nobpos">No Blanket Purchase Order's</cwms-translate></div> `;

function capitalize(s) {
    return s[0].toUpperCase() + s.slice(1);
}

const renderBPO = context => key => context["has" + capitalize(key)] ? html`
  <tr>
  <td colspan=7>${cwmsTranslateDirective("addjob.bpoassignedto", "BPO assigned to: {0}", context[key + "Name"])}</td>
  </tr>
  ${repeat(context.bpoResultsSet[key + "BPOs"], i => i.poId, renderBPOItemRow(context)(key + "BPOs"))}
` : html``

const renderBPOItemRow = context => key => item => html`
<tr>
  <td><input type="checkbox" .checked=${item.selected ?? false} @change=${_ => context.toggleSelectItem(item, key)}/></td>
  <td>${item.poNumber}</td>
  <td>${item.comment}</td>
  <td>${dateFormatDirective(item.durationFrom)}</td>
  <td>${dateFormatDirective(item.durationTo)}</td>
  <td>${currencyFormatWithZeroDirective(item.limitAmount)}</td>
</tr>
`;