import { css } from "../../../../thirdparty/lit-element/lit-element.js";

export const styling = (context) => css`
.initial-container {
    display:flex;
    justify-content:center;
    align-items:center;
    min-height:150px;
}

.comment {
    max-width: 60ch;
}

.pocom {
    width: 60%;
}

.podel, .poedit {
    width: 10%;
}

.ponum {
    width: 20%;
}

.edit-po .pocomment {
    width:60ch;
    min-height: 20ex;
}

.edit-po .ponumber {
    width: 60ch;
}
`;