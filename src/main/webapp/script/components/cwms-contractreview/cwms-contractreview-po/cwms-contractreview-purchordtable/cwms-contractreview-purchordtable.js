import { MetaDataObtainer } from "../../../../tools/metadataObtainer.js";
import { removeItemFromBPOLinkGenerator, bpoSelectionLinkGenerator, assignBPOToJobItemLinkGenerator, validPOsForJobItemLinkGenerator, assignItemToPOLinkGenerator, removeFromPOLinkGenerator, editPOLinkGenerator } from "../../../../tools/UrlGenerator.js";
import { CwmsAbstractWithPopup } from "../../../cwms-abstract-with-popup/cwms-abstract-with-popup.js";
import { styling } from "./cwms-contractreview-purchordtable.css.js";
import { editPOPopUpTemplate ,poPopUpTemplate ,bpoPopUpTemplate, template } from "./cwms-contractreview-purchordtable.html.js";

class CwmsContractreviewPurchordtable extends CwmsAbstractWithPopup {

    static get properties(){
        return {
            ...super.properties,
            jobItemBPOs:{type:Array},
            jobItemPOs:{type:Array},
            jobItemId:{type:Number},
            jobId:{type:Number},
            removeFromBPOTitle:{type:String},
            assigToBpoTitle:{type:String},
            addItemToPoTitle:{type:String},
            removeFromPOTitle:{type:String},
            editPOTitle:{type:String}
        };
    }

    static get styles(){
        return [...super.styles,styling()];
    }

    removeItemFromBPO(bpoId){
        fetch(removeItemFromBPOLinkGenerator(bpoId,this.jobItemId),{
            method:"PUT",
            credentials:"same-origin",
            headers:{...MetaDataObtainer.csrfHeader},
        })
        .then(res => res.ok?true:Promise.reject(res.statusText))
        .then(_ => this.jobItemBPOs = this.jobItemBPOs.filter(jipo => jipo.poId != bpoId));
    }

    selectBPOForItem(){
        this.popUpTitle = this.assigToBpoTitle;
        this.resolveContent = this.resolveBPOSelectContent;
        this.invokePopUp();
    }
    
    assignItemToBPO(bpoId){
        fetch(assignBPOToJobItemLinkGenerator(bpoId,this.jobItemId),{
            method:"PUT",
            credentials:"same-origin",
            headers:MetaDataObtainer.csrfHeader,
        })
        .then(res => res.ok?res.json():Promise.reject(res.statusText))
        .then(res => res.bpo).then(bpo => this.jobItemBPOs = [bpo]);
        this.closePopUp();
    }

    selectPOForItem(){
        this.popUpTitle = this.addItemToPOTitle;
        this.resolveContent = this.resolvePOSelectContent;
        this.invokePopUp();
    }

    assignItemToPO(poId){
        fetch(assignItemToPOLinkGenerator(poId, this.jobItemId),{
            method:"PUT",
            credentials:"same-origin",
            headers:MetaDataObtainer.csrfHeader,
        })
        .then(res => res.ok?res.json():Promise.reject(res.statusText))
        .then(jipo => this.jobItemPOs = [...this.jobItemPOs,jipo] );
        this.closePopUp();
    }

    editPOPopUp(po){
        this.popUpTitle = this.editPOTitle;
        this.resolveContent = _ => Promise.resolve(editPOPopUpTemplate(this)(po))
        this.invokePopUp();
    }

    savePO(po,e){
        const form = new FormData(e.currentTarget);
        const newPO = {...po, poNumber:form.get("poNumber"),comment:form.get("comment")};
        fetch(editPOLinkGenerator(newPO.poId,newPO.poNumber,newPO.comment),{
            method:"PUT",
            credentials:"same-origin",
            headers:MetaDataObtainer.csrfHeader
        }).then(res => res.ok?true:Promise.reject(res.statusText))
        .then(_ => this.jobItemPOs = this.jobItemPOs.map(jipo => jipo.poId == newPO.poId? newPO:jipo));
        this.closePopUp();
    }

    async resolvePOSelectContent(){
        const res = await fetch(validPOsForJobItemLinkGenerator(this.jobItemId))
        const data = await (res.ok? res.json():Promise.reject(res.statusText));
        return poPopUpTemplate(this)(data);
    }

    async resolveBPOSelectContent(){
        const res = await fetch(bpoSelectionLinkGenerator(this.jobId, this.jobItemId));
        const data = await (res.ok ? res.json() : Promise.reject(res.statusText));
        return bpoPopUpTemplate(this)(data);
    }


    removeFromPO(jipoId){
        fetch(removeFromPOLinkGenerator(jipoId), {
            method:"PUT",
            credentials:"same-origin",
            headers:MetaDataObtainer.csrfHeader
        })
        .then(res => res.ok?true:Promise.reject(res.statusText))
        .then(_ => this.jobItemPOs = this.jobItemPOs.filter(jipo => jipo.jipoId != jipoId));
    }

    constructor(){
        super();
        CwmsTranslate.messagePromise("jicontractreview.editthispo","Edit this PO")
            .then(msg => this.editPOTitle =msg)
        CwmsTranslate.messagePromise("jicontractreview.additemtopo","Add Item to PO")
            .then(msg => this.addItemToPOTitle = msg);
        CwmsTranslate.messagePromise("jicontractreview.assbpoonjob","Assing to BPO")
        .then(msg => this.assigToBpoTitle = msg);
        CwmsTranslate.messagePromise("jicontractreview.remitemfrompo","Remove Item from this PO")
        .then(msg => this.removeFromPOTitle = msg);
        CwmsTranslate.messagePromise("jicontractreview.string15","Remove Item from this BPO")
        .then(msg => this.removeFromBPOTitle = msg);
    }

    render(){
       return template(this);
    }

}

customElements.define("cwms-contractreview-purchordtable",CwmsContractreviewPurchordtable);