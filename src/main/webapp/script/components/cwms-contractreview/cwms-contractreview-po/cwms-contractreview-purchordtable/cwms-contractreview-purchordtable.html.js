import { html } from "../../../../thirdparty/lit-element/lit-element.js";
import {ifDefined} from "../../../../thirdparty/lit-html/directives/if-defined.js"
import { stylesMixin } from "../../../tools/applicationStylesMixinTemplate.js";
import { dateFormatDirective } from "../../../tools/formatingDirectives.js";

export const template = (context) => html`
  ${stylesMixin} ${context.renderPopUp()}
  <table class="default2" id="purchordtable" summary="This table displays all purchase orders">
    <thead>
      <tr>
        <td colspan="4">
          <cwms-translate code="jicontractreview.purchord">Purchase Order </cwms-translate>
          (<span class="poSizeSpan">${context.jobItemPOs?.length ?? 0}</span>)
        </td>
      </tr>
      <tr>
        <th class="ponum" scope="col"><cwms-translate code="jicontractreview.ponumber">PO Number</cwms-translate></th>
        <th class="pocom" scope="col"><cwms-translate code="comment">Comment</cwms-translate></th>
        <th class="poedit" scope="col"><cwms-translate code="edit">Edit</cwms-translate></th>
        <th class="podel" scope="col"><cwms-translate code="delete">Delete</cwms-translate></th>
      </tr>
    </thead>
    <tfoot>
      <tr>
        <td colspan="4">
          <a role="button" class="domthickbox mainlink-float" @click=${(_) => context.selectPOForItem()}>
            <cwms-translate code="jicontractreview.additemtopo">Add Item to PO</cwms-translate>
          </a>
        </td>
      </tr>
    </tfoot>
    <tbody>
      ${context.jobItemPOs?.length
        ? context.jobItemPOs.map(jobItemPOToRowTemplate(context))
        : html`
            <tr>
              <td colspan="4" class="center bold">
                <cwms-translate code="jicontractreview.string14">This job item is not assigned to any PO's</cwms-translate>
              </td>
            </tr>
          `}
    </tbody>
    <tbody>
      <tr>
        <th colspan="4"><cwms-translate code="jicontractreview.blankpo">Blanket Purchase Order</cwms-translate></th>
      </tr>
      <tr>
        <th><cwms-translate code="jicontractreview.bponumber">BPO Number</cwms-translate></th>
        <th colspan="2"><cwms-translate code="comment">Comment</cwms-translate></th>
        <th class="center"><cwms-translate code="delete">Delete</cwms-translate></th>
      </tr>
      ${context.jobItemBPOs?.length
        ? context.jobItemBPOs.map(jobItemBPOToRowTemplate(context))
        : html`
            <tr>
              <td colspan="4" class="center bold">
                <cwms-translate code="jicontractreview.string16">This job item is not assigned to the job BPO </cwms-translate>
                (<a role="button" class="mainlink" @click=${(_) => context.selectBPOForItem()}>
                  <cwms-translate code="jicontractreview.assbpoonjob">Assign to BPO</cwms-translate></a
                >)
              </td>
            </tr>
          `}
    </tbody>
  </table>
`;


const jobItemPOToRowTemplate = (context) => (jipo) => html`
  <tr id="po${jipo.poId}">
    <td>${jipo.poNumber}</td>
    <td>${jipo.comment}</td>
    <td class="center">
        <a role="button" @click=${_ => context.editPOPopUp(jipo)}>
      <img
        src="img/icons/note_edit.png"
        height="16"
        width="16"
        title=${context.editPOTitle}
        alt=${context.editPOTitle}
      />
      </a>
    </td>
    <td class="center">
        <a role="button" @click=${_ => context.removeFromPO(jipo.jipoId)}>
      <img
        src="img/icons/delete.png"
        height="16"
        width="16"
        title=${context.removeFromPOTitle}
        alt=${context.removeFromPOTitle}
      /></a>
    </td>
  </tr>
`;

const jobItemBPOToRowTemplate = (context) => (jipo) => html`
  <tr id="po${jipo.jipoId}">
    <td>${jipo.poNumber}</td>
    <td colspan="2">${jipo.poComment}</td>
    <td class="center">
      <a role="button" @click=${(_) => context.removeItemFromBPO(jipo.poId)}>
        <img src="img/icons/delete.png" height="16" width="16" title="${context.removeFromBPOTitle}" alt="${context.removeFromBPOTitle}" />
      </a>
    </td>
  </tr>
`;


export const poPopUpTemplate = context => data => html`
<table class="default2" id="potable" summary="This table displays all valid purchase orders">
    <thead>
        <tr>
            <th scope="col"> 
                <cwms-translate code="jicontractreview.purchord" >Purchase Order </cwms-translate>
            </th>
            <th scope="col"> <cwms-translate code="comment">Comment</cwms-translate></th>
            <th scope="col"> <cwms-translate code="jicontractreview.additemtopo" >Add Item to PO</cwms-translate> </th>
        </tr>
    </thead>
    <tbody>
        ${data?.length ?data.map(selectPORowTemplate(context)) :
        html`
        <tr><td colspan=11>
            <cwms-translate code="noresults">No results to display</cwms-translate>
        </td></tr>
        `}
    </tbody>
    <tfoot><tr><th colspan="11">&nbsp;</th></tr></tfoot>
</table>`;


const selectPORowTemplate = context => po => html`
    <tr>
        <td>${po.poNumber}</td>
        <td class="comment">${po.comment}</td>
        <td><a role="button" class="mainlink" @click=${_ => context.assignItemToPO(po.poId)}>
    <cwms-translate code="jicontractreview.additemtopo">Add item to PO</cwms-translate>
    </a></td>
    </tr>
`

export const bpoPopUpTemplate = context => data => html`
<table class="default2" id="bpotable" summary="This table displays the assigned blanket purchase order">
	<thead>
		<tr>
			<th id="bponum" scope="col"><cwms-translate code="bpo.bpono">BPO No</cwms-translate></th>
			<th id="bpocom" scope="col"><cwms-translate code="comment">Comment</cwms-translate></th>
			<th id="bpovalfrom" scope="col"><cwms-translate code="bpo.validfrom">Valid From</cwms-translate></th>
			<th id="bpovalto" scope="col"><cwms-translate code="bpo.validto">Valid To</cwms-translate></th>
			<th id="bpolimit" scope="col"><cwms-translate code="bpo.limitamount">Limit Ammount</cwms-translate></th>
			<th id="bpoassign" scope="col"><cwms-translate code="bpo.assignbpo">Asign BPO</cwms-translate></th>
		</tr>
	</thead>
	<tbody>
        ${selectBPOSectionTemplate(context)(data.CONTACT)}
        ${selectBPOSectionTemplate(context)(data.SUBDIV)}
        ${selectBPOSectionTemplate(context)(data.COMPANY)}
	</tbody>
</table>
`


const selectBPOSectionTemplate = (context) => (section) =>
  section?.length
    ? html`
        <tr>
          <td colspan="6" class="highlight-gold">${section[0].scope.message} BPOs</td>
        </tr>
        ${section.map(
          (bpo) => html`
            <tr class=${ifDefined(bpo.defaultForJob ? "higlight" : undefined)}>
              <td>${bpo.poNumber}</td>
              <td>${bpo.comment}</td>
              <td>${dateFormatDirective(bpo.durationFrom)}</td>
              <td>${dateFormatDirective(bpo.durationTo)}</td>
              <td>${bpo.limitAmount}</td>
              <td>
                <a role="button" class="mainlink" @click=${(_) => context.assignItemToBPO(bpo.poId)}>
                  <cwms-translate code="bpo.assingitem">Assign item</cwms-translate>
                </a>
              </td>
            </tr>
          `
        )}
      `
    : html``;


export  const editPOPopUpTemplate = (context) => (po) => html`
<form class="edit-po" @submit=${e =>{ e.preventDefault(); context.savePO(po,e)}}>
  <fieldset>
    <legend><cwms-translate code="jicontractreview.editthispo">Edit this PO</cwms-translate></legend>
    <ol>
      <li>
        <label><cwms-translate code="jicontractreview.ponumber">PO Number</cwms-translate></label>
        <input type="text" class="ponumber" name="poNumber" value=${po.poNumber} />
      </li>
      <li>
        <label><cwms-translate code="comment">Comment</cwms-translate></label>
        <textarea class="pocomment" name="comment" cols="50" rows="4">${po.comment}</textarea>
      </li>
      <li id="poSubmit"><label>&nbsp;</label>
            <button type="submit"><cwms-translate code="showitemsoncpo.savepo">Save PO</cwms-translate></button>
    </li>
    </ol>
  </fieldset>
</form>
`;