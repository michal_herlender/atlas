import { css } from "../../../../thirdparty/lit-element/lit-element.js";

export const styling = (context) => css`

        .title {
            background-color: var(--cwms-table-strong-backgound, #C6DAEC);
        }

        .bold {
            font-weight: bold;
        }

        .center {
            text-align: center;
        }

        table ~ table {
            margin-top:1rem;
        }
`;