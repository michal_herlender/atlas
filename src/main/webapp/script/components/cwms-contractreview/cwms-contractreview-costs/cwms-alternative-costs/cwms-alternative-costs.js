import { alternativeCostsLinkGenerator, updateCostSourceLinkGenerator } from "../../../../tools/UrlGenerator.js";
import "../../../cwms-link-and-popup/cwms-link-and-popup.js";
import { CwmsLinkAndPopUp } from "../../../cwms-link-and-popup/cwms-link-and-popup.js";
import { popUpContentTemplate as contentRenderer } from "./cwms-alternative-costs.html.js";
import {tableStyling} from "../../../tools/exchange-fromat-utils/tableRenderer.js";
import { styling } from "./cwms-alternative-costs.css.js";
import { MetaDataObtainer } from "../../../../tools/metadataObtainer.js";

class CwmsAlternativeCosts extends CwmsLinkAndPopUp {


    static get properties() {
        
        return {
         ...super.properties,
         jobItemId:{type:Number},
         loadingTitle:{type:String},
         viewingTitle:{type:String}
        };
    }

    constructor(){
        super()
        CwmsTranslate.messagePromise("jicontractreview.loadingAlternativeCostSources","Loading Alternative Cost Sources")
        .then(msg => this.loadingTitle = msg);
        CwmsTranslate.messagePromise("jicontractreview.viewingAvailableCostSources","Viewing Available Price Sources")
        .then(msg => this.viewingTitle = msg)
    }

    static get styles(){
        return [...super.styles,tableStyling,styling()]
    }

    async tabLabelProvider(code,defaultMessage){
        const msg = await CwmsTranslate.messagePromise(code, defaultMessage);
        switch(code){
            case "summary":
                return msg;
            case "docs.quotationitems":
                return `${msg} ( ${(this.content?.activeQuotationItems?.length ?? 0) +(this.content?.inactiveQuotationItems?.length ?? 0)} )`;
;
            case "instmod.instrumentmodelprices":
                return `${msg} ( ${this.content?.priceCatalogs?.length ?? 0} )`
            case "viewinstrument.jobhistory":
                return `${msg} ( ${this.content?.jobCosting?.length ?? 0} )`
            default: return msg;
        }
    }
    
    async resolveContent(){
        this.popUpTitle = this.loadingTitle;
       const res = await fetch(alternativeCostsLinkGenerator(this.jobItemId))
       const content =  await (res.ok?res.json():Promise.reject(res.statusText));
       this.content = content;
       this.popUpTitle = this.viewingTitle;
        return contentRenderer(this)(content);
    }

    updateCostSource(costId,type){
        fetch(updateCostSourceLinkGenerator(this.jobItemId,costId,type),{
            method:"PUT",
            headers:{...MetaDataObtainer.csrfHeader}
        })
        .then(res => res.ok?res.json():Promise.reject(res.statusText))
        .then(res => res.right? res.value:Promise.reject(res.value))
        .then(cost => {
            this.dispatchEvent(new CustomEvent("cost-source-updated",{detail:cost}))
            this.show =false;   
        });
    }
}

customElements.define("cwms-alternative-costs",CwmsAlternativeCosts);