import { html } from "../../../../thirdparty/lit-element/lit-element.js";
import "../../../cwms-translate/cwms-translate.js";
import "../../../cwms-dynamics-tabs/main.js";
import { instrumentModelPricesLinkGenerator, viewQuotationLinkGenerator } from "../../../../tools/UrlGenerator.js";
import { currencyFormatWithZeroDirective, currencyNameDirective, currencySymbolDirective, dateFormatDirective, dateFormatFunction } from "../../../tools/formatingDirectives.js";
import { editJobCostingItemLinkTemplate, editQuotationItemTemplate, jobCostingLinkTemplate } from "../common/templates.html.js";
import { cwmsTranslateDirective } from "../../../cwms-translate/cwms-translate-directive.js";

export const popUpContentTemplate = context => content => html`
    <div>
        <nav is="cwms-dynamic-subnav-holder">
            <cwms-dynamic-tab slot="summary" .tabLabelProvider=${context.tabLabelProvider("summary","Summary")}>
                ${instrumentSummaryTemplate(content.summary)}
            </cwms-dynamic-tab>
            <cwms-dynamic-tab slot="quotationItems" tabName="quotationItems" .tabLabelProvider=${context.tabLabelProvider("docs.quotationitems","Quotation Items")}>
                ${quotationItemsTemplate(context)(content)}
            </cwms-dynamic-tab>
            <cwms-dynamic-tab  tabName="instrumentModelPrice" .tabLabelProvider=${context.tabLabelProvider("instmod.instrumentmodelprices","Instrument Model Prices")}>
                ${instrumentModelPricesTemplate(context)(content)}
            </cwms-dynamic-tab>
            <cwms-dynamic-tab slot="jobHistory" tabName="jobHistory" .tabLabelProvider=${context.tabLabelProvider("viewinstrument.jobhistory", "Job History")}>
                ${jobHistoryTemplate(context)(content)}
            </cwms-dynamic-tab>

        </nav>
    </div>
`

const linkedQuoteLinkTemplate = quotation => html`<a target="_blank" .href=${viewQuotationLinkGenerator(quotation.id).toString()} class="mainlink">${quotation.label}</a></br> `;

const instrumentModelLinkTemplate= (modelId,modelName) => modelId? html`
<a .href=${instrumentModelPricesLinkGenerator(modelId).toString()}  class="mainlink">${modelName}</a>`:html``;

const instrumentSummaryTemplate = content => html`
<div id="ncs_summary-tab">
		<table class="">
			<thead>
				<tr>
					<td colspan="2" class="title">
						<cwms-translate code="jobitemlink.information" >Job Item Information</cwms-translate>
					</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<th class="sfield" scope="col">
						<cwms-translate code="clientcompany">Client Company</cwms-translate>
					</th>
					<td class="svalue" scope="col">
						${content?.companyName}
					</td>
				</tr>
				<tr>
					<th>
						<cwms-translate code="barcode">Barcode</cwms-translate> 
					</th>
					<td>
						${content?.plantId} 
					</td>
				</tr>
				<tr>
					<th>
						<cwms-translate code="servicetype">Service Type</cwms-translate> 
					</th>
					<td>
						${content?.serviceTypeLabel} 
					</td>
				</tr>
				<tr>
					<th>
						<cwms-translate code="instmodelname" >Intrument Model Name</cwms-translate>
					</th>
					<td>
                        ${instrumentModelLinkTemplate(content?.modelId,content?.modelName)}
					</td>
				</tr>
				<tr>
					<th>
						<cwms-translate code="viewjob.linkedquotes" >Linked Quotes</cwms-translate>
					</th>
					<td>
                        ${content?.linkedQuotes?.map(linkedQuoteLinkTemplate)}
					</td>
				</tr>
				<tr>
					<th>
						<cwms-translate code="company.contract" >Contract</cwms-translate>
					</th>
					<td>
						${content?.contractNumber ?? html`<cwms-translate code="viewjob.none">None</cwms-translate>`}
					</td>
				</tr>
				<tr>
					<th>
						<cwms-translate code="company.contract" >Contract</cwms-translate>
						 - 
						<cwms-translate code="quotation">Quotaion</cwms-translate> 
					</th>
					<td>
                        ${content?.contractNumber || content?.contractQuotation?linkedQuoteLinkTemplate(content?.contractQuotation):html`
                        <cwms-translate code="viewjob.none">None</cwms-translate>`}
					</td>
				</tr>
			</tbody>
		</table>
    </div>`
    

const quotationItemsTableTemplate = context => (items,titleCode,titleDefault,active=false) => html`
<table class="default4">
			<thead>
				<tr>
					<td colspan="11">
						<cwms-translate code=${titleCode} >${titleDefault}</cwms-translate>
						( ${items?.length ?? 0  } ) 
					</td>
				</tr>
				<tr>
					<th class="qsource" scope="col">
						<cwms-translate code="source" >Source</cwms-translate>
					</th>
					<th class="qlinked" scope="col">
						<cwms-translate code="linked" >Linked</cwms-translate>
					</th>
					<th class="qexpiry" scope="col">
						<cwms-translate code="createquot.expirydate" >Expiry Date</cwms-translate>
					</th>
					<th class="qqno" scope="col">
						<cwms-translate code="quoteno" >Quote no</cwms-translate>
					</th>
					<th class="qitemno" scope="col">
						<cwms-translate code="itemno" >Item No</cwms-translate>
					</th>
					<th class="qcost cost2pc" scope="col">
						<cwms-translate code="price" >Price</cwms-translate>
					</th>
					<th class="qdiscp cost2pc" scope="col">
						<cwms-translate code="discount" >Discount</cwms-translate> (%)
					</th>
					<th class="qdiscv cost2pc" scope="col">
						<cwms-translate code="discount">Discount</cwms-translate> 
					</th>
					<th class="qfinalc cost2pc" scope="col">
						<cwms-translate code="finalcost" >Final Price</cwms-translate>
					</th>
					<th class="qcurr cost2pc" scope="col">
						<cwms-translate code="currency" >Currency</cwms-translate>
					</th>
					<th class="qupd" scope="col">
						<cwms-translate code="update" >Update</cwms-translate>
					</th>
				</tr>
			</thead>
			<tbody>
                ${items && items.length >0?items.map(item => html`
                 <tr>
                    <td>${item.source}</td>
                    <td>${item.linked?html`<cwms-translate code="yes">Yes</cwms-translate>`:html``}</td>
                    <td>${dateFormatDirective(item.expiryDate)}</td>
                    <td>${linkedQuoteLinkTemplate({id:item.quoteId,label:item.quoteNo + " ver " + item.quotationVer})}</td>
                    <td>${editQuotationItemTemplate(item.itemId, item.itemNo)}</td>
                    <td>${currencyFormatWithZeroDirective(item.price,item.currencyCode)}</td>
                    <td>${item.discountRate} %</td>
                    <td>${currencyFormatWithZeroDirective(item.discount,item.currencyCode)}</td>
                    <td>${currencyFormatWithZeroDirective(item.finalPrice,item.currencyCode)}</td>
                    <td>${currencyNameDirective(item.currencyCode)}</td>
                    <td>${active?html`
                    <a role="button" class="mainlink" @click=${_ => context.updateCostSource(item.costId,"QUOTATION")}>
								<cwms-translate code="select">Select</cwms-translate> 
							</a>
                    `:html`
                    <cwms-translate code="company.inactive">Incative</cwms-translate>`}</td>
                </tr>
                `):html`
					<tr>
						<td colspan="11" class="bold center"><cwms-translate code="viewcapabilities.noresults" >No results for selected criteria</cwms-translate></td>
					</tr>
                `}
            </tbody>
        </table>
   
`

const quotationItemsTemplate = context => content => html`
    <div id="ncs_qitem-tab" >
		<div class="center">
            <cwms-translate code="viewjob.quotationsRegisteredAfter" >Searched linked quotations, contract quotations, and other quotations registered after</cwms-translate> ${dateFormatDirective(content.quotationRegDateAfter)}
        </div>
        ${quotationItemsTableTemplate(context)(content.activeQuotationItems,"costoverlay.activequotationitems","Active Quotation Items",true)}
        ${quotationItemsTableTemplate(context)(content.inactiveQuotationItems,"costoverlay.inactivequotationitems","Inactive Quotation Items")}
    </div>
`
const instrumentModelPricesTemplate = context => content => html`
<table class="default4">
			<thead>
				<tr>
					<td colspan="11">
						<cwms-translate code="instmod.instrumentmodelprices" >Instrument Model Prices</cwms-translate>
						( ${content.priceCatalogs?.length ?? 0  } ) 
					</td>
				</tr>
				<tr>
					<th class="msource" scope="col">
						<cwms-translate code="source" >Source</cwms-translate>
					</th>
					<th class="mtype" scope="col">
						<cwms-translate code="instmod.modeltype" >Model Type</cwms-translate>
					</th>
					<th class="mname" scope="col">
						<cwms-translate code="instmodelname">Instrument Model Name</cwms-translate> 
					</th>
                    <th class="mservice" scope="col">
                    	<cwms-translate code="servicetype" >Service Type</cwms-translate>
                    </th>
					<th class="mprice cost2pc" scope="col">
						<cwms-translate code="price">Price</cwms-translate> 
					</th>
					<th class="mcurrency cost2pc" scope="col">
						<cwms-translate code="currency" >Currency</cwms-translate>
					</th>
                    <th class="mupdate" scope="col">
                    	<cwms-translate code="update">Update</cwms-translate>
                    </th>
				</tr>
			</thead>
			<tbody>
                ${content.priceCatalogs && content.priceCatalogs.length >0?content.priceCatalogs.map(item => html`
                 <tr>
                    <td>${item.source}</td>
                    <td>${item.modelTypeName}</td>
                    <td>${instrumentModelLinkTemplate({id:item.modelId,label:item.modelName})}</td>
                    <td>${item.serviceType}</td>
                    <td>${currencyFormatWithZeroDirective(item.price,content.businessCurrencyCode)}</td>
                    <td>${currencyNameDirective(content.businessCurrencyCode)}</td>
                    <td>
                    <a role="button" class="mainlink" @click=${_ => context.updateCostSource(item.id,"MODEL")}>
								<cwms-translate code="select">Select</cwms-translate> 
							</a>
                    </td>
                </tr>
                `):html`
					<tr>
						<td colspan="11" class="bold center"><cwms-translate code="viewcapabilities.noresults" >No results for selected criteria</cwms-translate></td>
					</tr>
                `}
            </tbody>
        </table>
`
const jobHistoryTemplate = context => content => html`
<div id="ncs_jobpricing-tab" >
		
		<div class="center">
			${cwmsTranslateDirective("viewjob.costingsIssuedAfter","Searched job pricings issued after {0}",dateFormatFunction(content.quotationRegDateAfter))}   
		</div>
		<table class="default4" >
			<thead>
				<tr>
					<td colspan="9">
						<cwms-translate code="viewinstrument.jobhistory">Job History</cwms-translate>
						 ${content.jobCosting?.length ?? 0}
					</td>
				</tr>
				<tr>
					<th class="jno" scope="col">
						<cwms-translate code="docs.jobcosting" >Job Costing</cwms-translate>
					</th>
					<th class="jitemno" scope="col">
						<cwms-translate code="itemno" >Item No</cwms-translate>
					</th>
					<th class="jissued" scope="col">
						<cwms-translate code="issuedate" >Issue Date</cwms-translate>
					</th>
					<th class="jcost" scope="col">
						<cwms-translate code="price" >Price</cwms-translate>
					</th>
                    <th class="jdiscp" scope="col">
                    	<cwms-translate code="discount" >Discount</cwms-translate> (%)
                    </th>
					<th class="jdiscv cost2pc" scope="col">
						<cwms-translate code="discount" >Discount</cwms-translate>
					</th>
					<th class="jfinalc cost2pc" scope="col">
						<cwms-translate code="finalcost" >Final Price</cwms-translate>
					</th>
					<th class="jcurrency cost2pc" scope="col">
						<cwms-translate code="currency" >Currency</cwms-translate>
					</th>
                    <th class="jupdate" scope="col">
                    	<cwms-translate code="update" >Update</cwms-translate>
                    </th>
				</tr>
			</thead>
			<tbody>
			${content.jobCosting?.length ?content.jobCosting.map(cost =>  html`
					<tr>
						<td>
							${jobCostingLinkTemplate(cost.elementId,cost.elementLabel)}
						</td>
						<td>
							${editJobCostingItemLinkTemplate(cost.itemId,cost.itemLabel)}
						</td>
						<td>
							${dateFormatDirective(cost.issueDate)}
						</td>
						<td>
							${currencyFormatWithZeroDirective(cost.totalCost,cost.currencyCode)}
						</td>
						<td>
							${cost.discountRate} %
						</td>
						<td>
							${currencyFormatWithZeroDirective(cost.discountValue,cost.currencyCode)}
						</td>
						<td>
							${currencyFormatWithZeroDirective(cost.finalCost,cost.currencyCode)}
						</td>
						<td>
							${currencyNameDirective(cost.currencyCode)}
						</td>
						<td>
							<a role="button" class="mainlink" @click=${_ => context.updateCostSource(cost.costId,"JOB_COSTING")}>
								<cwms-translate code="select">Select</cwms-translate> 
							</a>
						</td>
					</tr>
				`):html`
					<tr>
						<td colspan="9" class="bold center"><cwms-translate code="viewcapabilities.noresults" >
							No results for selected criteria
						</cwms-translate></td>
					</tr>`}
			</tbody>													
		</table>
	</div>

`