import { css } from "../../../../thirdparty/lit-element/lit-element.js";


export const styling = (context) => css`
    ::slotted(input) {
        width:120px;
    }

    table {
        font-family: arial,helvetica,sans-serif;
    }

`;