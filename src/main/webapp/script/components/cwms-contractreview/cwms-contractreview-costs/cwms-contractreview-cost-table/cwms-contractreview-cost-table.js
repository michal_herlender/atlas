import { LitElement, html } from "../../../../thirdparty/lit-element/lit-element.js";
import { MetaDataObtainer } from "../../../../tools/metadataObtainer.js";
import { resetLinkedCostLinkGenerator } from "../../../../tools/UrlGenerator.js";
import { styling } from "./cwms-contractreview-cost-table.css.js";
import { template } from "./cwms-contractreview-cost-table.html.js";

class CwmsContractreviewCostTableInner extends LitElement {

    static get styles(){
        return [styling()];
    }

    static get properties(){
        return {
            costs:{type:Array},
            jobId:{type:Number},
            jobItemId:{type:Number},
            currencyCode:{type:String}
        };
    }

    refresh(){
        this.dispatchEvent(new CustomEvent("refresh",{detail:{costs:this.costs}}))
    }

    switchActive(active,costName){
       const costIndex = this.costs.findIndex(c => c.costType.name.toLowerCase()==costName)
       if(costIndex > -1) this.costs[costIndex].active = active;
       this.refresh();
    }

    
    getDiscountRateForCostType(costType)
    {
        return this.querySelector(`input[name="${costType.name.toLowerCase()}Cost.discountRate"]`);
    }

    getTotalCostForCostType(costType)
    {
        return this.querySelector(`input[name="${costType.name.toLowerCase()}Cost.totalCost"]`);
    }

    costSourceUpdated(costId){
        const costIndex = this.costs.findIndex(c => c.costId == costId)
        return event => {
            const cost = event.detail;
            this.getTotalCostForCostType(cost.costType).value = cost.totalCost;
            this.getDiscountRateForCostType(cost.costType).value = cost.discountRate;
            if(costIndex > -1) this.costs[costIndex] = cost;
            this.refresh();
        };
    }

    resetLinkedCost(costId){
        fetch(resetLinkedCostLinkGenerator(costId),{
            method:"PUT",
            headers:{...MetaDataObtainer.csrfHeader}
        })
        .then(res => res.ok?res.json():Promise.reject(res.statusText))
        .then(res => res.right? res.value:Promise.reject(res.value))
        .then(costSource => {
            const costIndex = this.costs.findIndex(c => c.costId == costId)
            if(costIndex > -1) this.costs[costIndex].costSource = costSource;
            this.refresh();
        })
        .catch(msg => this.errorMessage = msg);
    }

    render(){
       return template(this);
    }

}


class CwmsContractreviewCostTableWrapper extends LitElement {

    static get properties(){
        return {
            costs:{type:Array},
            jobItemId:{type:Number},
            jobId:{type:Number},
            currencyCode:{type:String}
        };
    }


    updateCost(cost){
        this.costs = [...this.costs.filter(c => c.costType.name != cost.costType.name),cost];
    }

    createRenderRoot(){
        return this;
    }

    refresh(e){
        this.costs = [...e.detail.costs];
    }

    render(){
       return html`
       <cwms-contractreview-cost-table-inner .jobItemId=${this.jobItemId} .jobId=${this.jobId} @refresh=${this.refresh}  .costs=${this.costs} .currencyCode=${this.currencyCode}>
        ${this.costs.map(cost => {
            const costName = cost.costType.name.toLowerCase();
            return html`
                    <input type="number" step="0.01" placeholder="0.00"  .value=${cost.totalCost} name="${costName}Cost.totalCost" id="${costName}Cost.totalCost" slot="${costName}Cost.totalCost" 
                        class="right" />
                    <input type="number" step="0.01" placeholder="0.00" .value=${cost.discountRate} slot="${costName}Cost.discountRate" id="${costName}Cost.discountRate" name="${costName}Cost.discountRate"
                         class="right" />
                    <input type="hidden" slot="${costName}Cost.active" id="${costName}Cost.active" name="${costName}Cost.active" value=${cost.active} />
            `;
        })}
           <div slot="inputs" class="hid">

           </div>
       </cwms-contractreview-cost-table-inner>
       `;
    }

}
customElements.define("cwms-contractreview-cost-table-inner",CwmsContractreviewCostTableInner);
customElements.define("cwms-contractreview-cost-table",CwmsContractreviewCostTableWrapper);