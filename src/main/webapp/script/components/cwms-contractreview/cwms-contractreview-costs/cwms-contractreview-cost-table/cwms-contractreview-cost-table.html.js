import { html } from "../../../../thirdparty/lit-element/lit-element.js";
import { stylesMixin } from "../../../tools/applicationStylesMixinTemplate.js";
import "../../../cwms-translate/cwms-translate.js";
import "../../../cwms-link-and-popup/cwms-link-and-popup.js";
import "../cwms-alternative-costs/cwms-alternative-costs.js";
import "../../../cwms-toggle-icons/cwms-toggle-icons.js";
import "../../../cwms-tooltips/cwms-entitiy-currency-conversion-tooltip/cwms-entitiy-currency-conversion-tooltip.js";
import { currencyFormatWithZeroDirective, currencySymbolDirective } from "../../../tools/formatingDirectives.js";
import {  viewQuotationLinkGenerator } from "../../../../tools/UrlGenerator.js";
import { editJobCostingItemLinkTemplate, editQuotationItemTemplate, jobCostingLinkTemplate } from "../common/templates.html.js";

export const template = (context) => html`
  ${stylesMixin}
  <table class="default2" id="jicontreview" summary="This table contains contract review costs">
    <thead>
      <tr>
        <th class="type" scope="col">
          <cwms-translate code="jicontractreview.costtype">Price type</cwms-translate>
        </th>
        <th class="costs cost2pc" scope="col">
          <cwms-translate code="jicontractreview.costs">Price</cwms-translate>
        </th>
        <th class="disc cost2pc" scope="col">
          <cwms-translate code="jicontractreview.discount">Discount</cwms-translate>
        </th>
        <th class="discval cost2pc" scope="col">
          <cwms-translate code="jicontractreview.discval">Discount Value</cwms-translate>
        </th>
        <th class="finalcost cost2pc" scope="col">
          <cwms-translate code="jicontractreview.finalcost">Final Price</cwms-translate>
        </th>
        <th class="costsource" colspan="2" scope="col">
          <cwms-translate code="jicontractreview.costsource">Price Source</cwms-translate>
        </th>
        <th class="addremove" scope="col">
          <cwms-translate code="jicontractreview.addrem">Add/Rem</cwms-translate>
        </th>
      </tr>
    </thead>
    <tbody>
      ${context.costs?.map(costTemplate(context))}
    </tbody>
  </table>
`;

const costTemplate = (context) => (cost) => {
  const costTypeNameUpperCase = cost.costType.name.toUpperCase();
  const costName = cost.costType.name.toLowerCase();
  const activeClassName = cost.active ? "vis" : "hid";
  const inactiveClassName = !cost.active ? "vis" : "hid";
  return html`
    <tr id="active${costTypeNameUpperCase}" class=${activeClassName}>
      <td>${cost.costType.message}</td>
      <td class="cost2pc" id="${costTypeNameUpperCase}totalcost">
        <span id="${costTypeNameUpperCase}totalcostSym">${currencySymbolDirective(context.currencyCode)}</span>
        <slot name=${`${costName}Cost.totalCost`}></slot>
        <br>
      </td>
      <td class="cost2pc" id="${costTypeNameUpperCase}discountrate">
        <slot name="${costName}Cost.discountRate"></slot> %
      </td>
      <td class="cost2pc" id="${costTypeNameUpperCase}discountvalue">
        <span id="${costName}Cost.discountValue">${currencyFormatWithZeroDirective(cost.discountValue, context.currencyCode)}</span>
        <br>
        
      </td>
      <td class="cost2pc" id="${costTypeNameUpperCase}finalcost">
        <span id="${costName}Cost.finalCost">${currencyFormatWithZeroDirective(cost.finalCost, context.currencyCode)}</span>
        <br>
        </td>
      <td id="${costTypeNameUpperCase}costsource">
        ${resolveCostSource(cost)}
      </td>
      <td style="width: 5%;">
        ${
           cost.costType.name == "CALIBRATION"
             ? html` 
              <a role="button" @click=${_=> context.resetLinkedCost(cost.costId)} class="mainlink" title="Reset this cost source">
               <cwms-translate code="jicertificates.reset">Reset</cwms-translate>
              </a>
              <br />
              <cwms-alternative-costs @cost-source-updated=${context.costSourceUpdated(cost.costId)} title="Switch this cost source"  jobItemId=${context.jobItemId}>
                <cwms-translate code="jicontractreview.switch">switch</cwms-translate>
              </cwms-alternative-costs> 
             `
            : html``
        }
      </td>
      <td class="center">
        <slot name="${costName}Cost.active"></slot>
        <cwms-toggle-icons .icons=${["img/icons/delete.png"]} @click=${_ => context.switchActive(false,costName)}></cwms-toggle-icons>       
      </td>
    </tr>
    <tr id=${`inactive${costTypeNameUpperCase}`} class=${inactiveClassName}>
      <td>${cost.costType.message}</td>
      <td colspan="6" class="center">
        <cwms-translate code="jicontractreview.nonespecified">None Specified</cwms-translate>
      </td>
      <td class="center">
        <cwms-toggle-icons .icons=${["img/icons/add.png"]} @click=${_ => context.switchActive(true,costName)}></cwms-toggle-icons>       
      </td>
    </tr>
    `;
};

const resolveCostSource = (cost) => {
  switch (cost.costSource) {
    case "QUOTATION":
      return html`
        <cwms-translate code="jicontractreview.quote">QUOTE</cwms-translate>
        ${quotationLink(cost.elementId, cost.elementLabel)}
        ${!cost.issued
          ? html`
              [<span style="color: red;" title="This quotation has not been issued"> <cwms-translate code="viewjob.unIssued">un-issued</cwms-translate> </span
              >]
            `
          : html``}
        ${cost.expired ? html` [<span style="color: red;" title="This quotation has expired">e</span>] ` : html``}
        <br />
        <cwms-translate code="item">Item</cwms-translate>&nbsp;
        ${editQuotationItemTemplate(cost.itemId,cost.itemNo)}
      `;
    case "JOB_COSTING":
    case "INSTRUMENT":  
          return html`
          <cwms-translate code=${cost.costSourceMessageCode}>${cost.costSource}</cwms-translate>
          <br/>
          ${jobCostingLinkTemplate(cost.elementId,cost.elementLabel)}
          <br/>
          ${editJobCostingItemLinkTemplate(cost.itemId,cost.itemNo)}
          `;
    default:
      return html`${cost.costSource}`  
  }
};

const quotationLink = (id, label) => html` <a .href=${viewQuotationLinkGenerator(id).toString()} class="mainlink">${label}</a> `;
