import { html } from "../../../../thirdparty/lit-element/lit-element.js";
import { editJobCostingItemLinkGenerator, editQuotationItemLinkGenerator, jobCostingLinkGenerator } from "../../../../tools/UrlGenerator.js";


export const  editQuotationItemTemplate = (itemId,label) => html`
        <a target="_blank" .href=${editQuotationItemLinkGenerator(itemId).toString()} class="mainlink">${label}</a>`;

export const  editJobCostingItemLinkTemplate = (itemId,label) => html`
        <a target="_blank" .href=${editJobCostingItemLinkGenerator(itemId).toString()} class="mainlink">${label}</a>`;

export const jobCostingLinkTemplate = (costingId,label)  => html`
        <a href="${jobCostingLinkGenerator(costingId).toString()}" target="_blank" class="mainlink">
        ${label}
    </a>`