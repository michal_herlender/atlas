import { LitElement } from "../../../thirdparty/lit-element/lit-element.js";
import { styling } from "./cwms-capabilityfilters-edit.css.js";
import { template } from "./cwms-capabilityfilters-edit.html.js";
import { tableStyles } from "../../tools/styles/table.css.js";
import { linkStyling } from "../../tools/styles/links.css.js";


class CwmsCapabilityFiltersEdit extends LitElement {

    #attachedInternals;
    #innerForm;

    get internals(){
        return this.#attachedInternals;
    }
    
    get innerForm(){
        if(this.#innerForm) return this.#innerForm;
        this.#innerForm =  this.shadowRoot?.querySelector("form");
        return this.#innerForm;
    }
    static get properties(){
        return {
            capabilities:{type:Array},
            serviceTypes:{type:Array},
            filterTypes:{type:Array},
            calibrationProcesess:{type:Array},
            workInstruktions:{type:Array},
            accreditedLabs:{type:Array},
            requirementTypes:{type:Array},
            categories:{type:Array}
        }
    }

    static get styles(){
        return [
            styling(),
            tableStyles,
            linkStyling()
        ]
    }

    static get formAssociated(){
        return true;
    }

    constructor(){
        super()
        this.capabilities = [];
        this.calibrationProcesess = [];
        this.workInstruktions = [];
        this.#attachedInternals = this.attachInternals();
    }

    addCapabilityFilter(){
        this.capabilities = [...this.capabilities,{}];
    }
    
    onUpdate(){
        this.#attachedInternals.setFormValue(new FormData(this.innerForm));
    }

    render(){
       return template(this);
    }

}


customElements.define("cwms-capabilityfilters-edit",CwmsCapabilityFiltersEdit);