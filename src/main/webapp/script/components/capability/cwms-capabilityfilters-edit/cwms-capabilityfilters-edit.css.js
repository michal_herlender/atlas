import { css } from "../../../thirdparty/lit-element/lit-element.js";

export const styling = (context) => css`

    .formContainer {
        display:flex;
        flex-wrap:wrap;
        flex-direction:row;
        margin-bottom:1rem;
        border: 1px solid var(--cwms-main-dark-background,#999);
    }

    .formElement {
        display:flex;
        flex-direction:column;
        border:1px solid var(--cwms-link-gray,#999);
        min-width:65px;
        flex-grow:2;
        min-height:90px;
    }

    .formElement .header {
        background-color:var(--cwms-main-gray-background, #dedede);
        font-weight:bold;
        font-size: 8pt;
        padding:4px;
        text-align: center;
        vertical-align:center;
    }

    .filterCalibrationProcess, .filterWorkInstruction {
        min-width:200px;
    }

    .filterDescription, .filterComments {
        min-width:50%;
    }

    .filterBestLab {
        flex-grow:2;
    }


    .formItem {
        vertical-align:middle;
        text-align: center;
        padding: 4px;
        background-color: var(--cwms-main-light-font-color,#fff);
        flex-grow:2;
        display:flex;
        align-items:center;
        justify-content:center;
    }

    .filterCategoryDepartment .formItem {
        display:grid;
        grid-template-columns: repeat(5,1fr);
        grid-template-rows: auto;
    }
    .filterCategoryDepartment .formItem div {
        display:grid;
        grid-template-columns: 30px 1fr;
        grid-template-rows: 1fr;
        text-align:initial;
    }
    .filterServiceType, .serviceTypeCell, .filterComments {
        width: 40%;
    }


    .filterCapability {
        width: 10%;
        flex-grow:2;
    }

    select, textarea  {
        font-size:100%;
        width:90%;
    }

    

`;