import {html} from "../../../thirdparty/lit-element/lit-element.js";
import {urlGenerator} from "../../../tools/UrlGenerator.js";
import {repeat} from "../../../thirdparty/lit-html/directives/repeat.js";
import {cwmsTranslateDirective} from "../../cwms-translate/cwms-translate-directive.js";

export const template = (context) => html`
    <fieldset>
        <form>
            ${context.capabilities?.map(editCapabilityFilterFormTemplate(context))}
            <div>
                <a role="button" @click=${_ => context.addCapabilityFilter()}>
                    <img src="${urlGenerator("img/icons/add.png").toString()}"/>
                    <cwms-translate code="editcapability.addcapabilityfilter">Add capability filter</cwms-translate>
                </a>
            </div>
        </form>
    </fieldset>
`;


const editCapabilityFilterFormTemplate = context => (capability,idx) => html`
<div class="formContainer">
    ${formElement("filterServiceType")(html`
    <cwms-translate code="servicetype">Service Type</cwms-translate>`)(
    selectWithGroupByListTemplate(context.serviceTypes)(`filters[${idx}].serviceTypeId`)(_ => context.onUpdate())(st => st.id)(st => st.shortName)(st => st.longName)(st => st.id === (capability?.serviceTypeId ?? 1))
)}
    ${formElement("filterCapability")(html`
    <cwms-translate code="viewcapability.capability">Capability
    </cwms-translate>`)(selectByListTemplate(context.filterTypes)(`filters[${idx}].filterType`)(_ => context.onUpdate())(ft => ft.key)(ft => cwmsTranslateDirective(ft.value, ft.key))(ft => ft.key === (capability?.filterType ?? "MAYBE"))
)}
    ${formElement("filterComments")(html`
    <cwms-translate code="viewcapability.comments">Internal Comments (Quotation)</cwms-translate>`)(html`<textarea rows="5"
                                                                                                       cols="80"
                                                                                                       name="filters[${idx}].internalComments"
                                                                                                       @change=${_ => context.onUpdate()}>${capability?.internalComments ?? ""}</textarea>`)}
    ${formElement("filterBestLab")(html`
    <cwms-translate code="viewcapability.bestlab">Best Lab</cwms-translate>`)(html`<input type="checkbox"
                                                                                          disabled
                                                                                          ?checked=${capability?.bestLab ?? false}
                                                                                          value="true"
                                                                                          name="filters[${idx}].bestLab"
                                                                                          @change=${_ => context.onUpdate()}/>
    <input type="hidden" value="false" name="filters[${idx}].bestLab"/>
    `)}
    ${formElement("filterDelete")(html`
    <cwms-translate code="delete">Delete</cwms-translate>`)(html`<input type="checkbox"
                                                                        ?checked=${capability?.delete ?? false}
                                                                        value="true"
                                                                        name="filters[${idx}].delete"
                                                                        @change=${_ => context.onUpdate()}/>`)}
    ${formElement("filterActive")(html`
    <cwms-translate code="active">Active</cwms-translate>`)(html`
    <input type="checkbox" ?checked=${capability?.active ?? true} value="true" name="filters[${idx}].active"
           @change=${_ => context.onUpdate()}/>
    <input type="hidden" value="false" name="filters[${idx}].active"/>`)}
    ${formElement("filterWorkInstruction")(cwmsTranslateDirective("editproc.defaultworkinstruction", "Default Work Instruktion"))(selectByListTemplate([{
    key: "",
    value: cwmsTranslateDirective("editproc.nodefaultworkinstruction", "No default work instruction")
}, ...context.workInstruktions])(`filters[${idx}].defaultWorkInstruktionId`)(_ => context.onUpdate())(cp => cp.key)(cp => cp.value)(wi => wi.key == capability?.workInstruktionId))}
    ${formElement("filterStandardTime")(cwmsTranslateDirective("editproc.estimatedtime", "Estimated time"))(html`
    <input disabled type="number" name="filters[${idx}].standardTime" value=${capability?.standardTime ?? 0}
           @change=${_ => context.onUpdate()}/>`)}
    ${formElement("filterDescription")(html`
    <cwms-translate code="viewcapability.externalComments">External Comments</cwms-translate>`)(html`<textarea
            rows="5" cols="80" name="filters[${idx}].externalComments"
            @change=${_ => context.onUpdate()}>${capability?.externalComments ?? ""}</textarea>`)}
</div>
`

const formElement = headerClass =>  caption => inputTemplate => html`
    <div class="formElement ${headerClass}">
        <span class="header">${caption}</span>
        <div class="formItem">
            ${inputTemplate}
        </div>
    </div>
`

const selectWithGroupByListTemplate  = listOfOptions => selectName => changeFunction => keyFunction => optGroupLabelFunction => valueFunction => selectedPredicate =>  html`
    <select @change=${changeFunction} name=${selectName}>
        ${repeat(listOfOptions,keyFunction,l => 
            html`
            <optgroup label=${optGroupLabelFunction(l)}>
                <option value=${keyFunction(l)}
                    ?selected=${(selectedPredicate??(_=>false))(l)}>
                    ${valueFunction(l)}
                </option>
            </optgroup>`)}
    </select>
`;


const selectByListTemplate  = listOfOptions=> selectName =>  changeFunction => keyFunction =>  valueFunction => selectedPredicate =>  html`
    <select @change=${changeFunction} name=${selectName} >
        ${repeat(listOfOptions,keyFunction,l => 
            html`
                <option value=${keyFunction(l)}
                    ?selected=${(selectedPredicate??(_=>false))(l)}>
                    ${valueFunction(l)}
                </option>
            `)}
    </select>
`;
