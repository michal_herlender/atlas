
import { LitElement } from "../../../thirdparty/lit-element/lit-element.js";
import { template } from "./cwms-capabilityfilter-active-switch.html.js";

class CwmsCapabilityFilterActiveSwitch extends LitElement {

    static get properties() {
        return {
            value:{type:Boolean,converter: (value, type) => (value == "true" ? true : false)},
            disabled:{type:Boolean,converter: (value, type) => (value == "true" ? true : false)}
        }
    }

    render(){
       return template(this);
    }

}


customElements.define("cwms-capabilityfilter-active-switch",CwmsCapabilityFilterActiveSwitch);