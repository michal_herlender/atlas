import { html } from "../../../thirdparty/lit-element/lit-element.js";

export const template = (context) => html`
    <input type="checkbox" ?checked=${context.value} ?disabled=${context.disabled} />
`;