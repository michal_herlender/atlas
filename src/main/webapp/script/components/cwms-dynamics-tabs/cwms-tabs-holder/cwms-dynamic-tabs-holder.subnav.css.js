import { css } from "../../../thirdparty/lit-element/lit-element.js";

export const styling = (context) => css`
  :host {
    display: flex;
    flex-direction: column;
  }

  .tab-nav-main {
    display: flex;
    height: 25px;
    align-content: center;
    justify-content: center;
    background: white;
    position:relative;
    margin-bottom: 1rem;
    gap: 1rem;
  }

  .tab-nav-main:before {
  content: '';
  position: absolute;
  top: 50%;
  left: 0;
  border-top: thin solid #999;
  background: transparent;
  width: 100%;
}

  .tab-element:hover,
  .tab-element.selected {
    	color: #000;
	border-top: 2px solid #666;
	border-bottom: 2px solid #666;
  }

   .tab-element {
	  border: 1px solid #666;
    color: #fff;
    display: block;
    text-decoration: none;
    position: relative;
    padding: 1px 7px 1px 7px;
    margin: 3px 0;
    line-height: 1.4;
    font-size: 90%;
    background-color: white;
    color: var(--cwms-main-dark-font-color, black);
    cursor: pointer;
    height:unset;
  }

  .tab {
    margin-top: 1px;
    padding:10px;
    background-color:#f2f2f2;
  }
`;
