import { LitElement, html, css } from "../../../thirdparty/lit-element/lit-element.js";
import { template } from "./cwms-dynamic-tabs-holder.html.js";
import { styling } from "./cwms-dynamic-tabs-holder.css.js";
import { styling as tabMenuStyling } from "./cwms-dynamic-tabs-holder.tab-menu.css.js";
import { styling as subNavStyling } from "./cwms-dynamic-tabs-holder.subnav.css.js";

class CwmsDynamicTabsHolder extends LitElement {

    static get properties(){
        return {
            variant:{type:String},
            switchOnlyOnClick:{type:Boolean},
            currentTab:{type:Object,attributes:false},
        };
    }

    static get styles(){
        let variantCss;
         return styling({});
    }

    set tabRegister(register){
        const oldValue = this.tabRegister;
        this.__tabRegister = register;
        this.requestUpdate("tabRegister",oldValue);
    }

    get tabRegister(){
        if(!this.__tabRegister)
            this.__tabRegister = new Map();
        return this.__tabRegister ;
    }

    subscribe(tabName,tab){
        var tabElement = {tabName,tab};
        if(this.tabRegister instanceof Map && this.tabRegister.size ==0)
           {
               tabElement = {...tabElement,className:"selected"};
               this.currentTab = tabElement;

           } 
       if(!this.tabRegister.has(tabName)) this.tabRegister.set(tabName,tabElement);
    }

    selectTab(tabName) {
        this.tabRegister = new Map(Array.from( this.tabRegister.values())
                    .map(t => ({...t,className:t.tabName==tabName?"selected":""}))
                     .map(v => [v.tabName,v]));
        const selectedTab = Array.from(this.tabRegister.values()).find(t => t.tabName == tabName);
        if(selectedTab)  this.currentTab = selectedTab;
    }

    render(){
       return template({register:Array.from(this.tabRegister.values()),
        selectTab:this.selectTab.bind(this),
        selectOnClickOnly:this.switchOnlyOnClick,
        currentTab:this.currentTab,
        variant:this.variant,
    });
    }

}



class CwmsDynamicTabMenuHolder extends CwmsDynamicTabsHolder {

    static get styles() {
        return tabMenuStyling({});
    }
}



class CwmsDynamicSubNavHolder extends CwmsDynamicTabsHolder {

    get switchOnlyOnClick(){return true;}


    static get styles() {
        return subNavStyling({});
    }
}

customElements.define("cwms-dynamic-tabs-holder",CwmsDynamicTabsHolder,{extends:'nav'});
customElements.define("cwms-dynamic-tabmenu-holder",CwmsDynamicTabMenuHolder, {extends:'nav'});
customElements.define("cwms-dynamic-subnav-holder",CwmsDynamicSubNavHolder, {extends:'nav'});