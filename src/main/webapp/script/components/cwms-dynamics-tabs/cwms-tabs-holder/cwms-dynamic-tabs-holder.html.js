import { html,css } from '../../../thirdparty/lit-element/lit-element.js'

export const template = (context) => html`
    <div class=${`tab-nav-main ${context.variant ?? ""}`}>
        ${context.register
        .map(tabHeaderTemplate(context))}
    </div>
    <div class="tab">
        <slot name=${context.currentTab.tabName}></slot>
        </div>
`;

const tabHeaderTemplate = context => tabEntry => html`
<div class=${`tab-element ${tabEntry.className}`}  @click=${_ => context.selectTab(tabEntry.tabName)}
            @mouseover=${context.selectOnClickOnly?undefined:_ => context.selectTab(tabEntry.tabName)}
>
    ${tabEntry.tab.tabLabel ?? tabEntry.tab.tabName}
    </div>
`

