import { css } from "../../../thirdparty/lit-element/lit-element.js";

export const styling = (context) => css`
  :host {
    display: flex;
    flex-direction: column;
  }

  .tab-nav-main {
    display: flex;
    height: 32px;
    background: #86a2ca; /* Old browsers */
    background: linear-gradient(
      to bottom,
      #86a2ca 0%,
      #a5d7f1 3%,
      #61bfed 6%,
      #61bfed 13%,
      #5cbdec 16%,
      #5cbdec 26%,
      #55bae9 29%,
      #55bae9 42%,
      #4bb5e6 45%,
      #4bb5e6 61%,
      #44b3e5 65%,
      #44b3e5 87%,
      #3bafe3 90%,
      #3bafe3 94%,
      #a5d7f1 97%,
      #86a2ca 100%
    );
  }

  .red.tab-nav-main {
    background: #c78b89; /* Old browsers */
    background: linear-gradient(
      to bottom,
      #c78b89 0%,
      #feccc3 4%,
      #e25f5c 7%,
      #df5855 15%,
      #df5855 22%,
      #db4f4c 26%,
      #d94a47 37%,
      #d64442 41%,
      #d64442 44%,
      #d4403d 48%,
      #d4403d 59%,
      #cc3333 63%,
      #cc3333 74%,
      #cc292a 78%,
      #cc292a 85%,
      #ca2225 89%,
      #ca2225 93%,
      #feccc3 96%,
      #c78b89 100%
    );
  }

  .red.tab-nav-main .tab-element:hover,
  .red.tab-nav-main .tab-element.selected {
    background: #c78b89; /* Old browsers */
    background: linear-gradient(
      to bottom,
      #c78b89 0%,
      #feccc3 4%,
      #721c14 7%,
      #b2261e 11%,
      #ac241b 15%,
      #ac241b 22%,
      #a72216 26%,
      #a72216 33%,
      #a22212 37%,
      #a22212 41%,
      #9b210a 44%,
      #9b210a 52%,
      #971f06 56%,
      #941f02 74%,
      #8c1e00 78%,
      #8c1e00 89%,
      #621900 93%,
      #feccc3 96%,
      #c78b89 100%
    );
  }

  .tab-nav-main .tab-element:hover,
  .tab-nav-main .tab-element.selected {
    background: #86a2ca; /* Old browsers */
    background: linear-gradient(
      to bottom,
      #86a2ca 0%,
      #a5d7f1 3%,
      #004d6e 6%,
      #006699 10%,
      #006699 19%,
      #00628d 23%,
      #00628d 45%,
      #005b84 48%,
      #005b84 71%,
      #00567b 74%,
      #00567b 90%,
      #004562 94%,
      #a5d7f1 97%,
      #86a2ca 100%
    );
  }

  .tab-nav-main .tab-element.selected:after,
  .tab-nav-main .tab-element:hover:after {
    content: "";
    border: 5px solid transparent;
    border-bottom-color: #a5d7f1;
    position: absolute;
    bottom: 0px;
    left: 50%;
    margin-left: -5px;
  }
  .red.tab-nav-main .tab-element.selected:after,
  .red.tab-nav-main .tab-element:hover:after {
    border-bottom-color: #feccc3;
  }

  .tab-nav-main .tab-element {
    color: #fff;
    display: block;
    float: left;
    text-decoration: none;
    padding: 8px 18px 0;
    text-transform: uppercase;
    font-weight: bold;
    height: 22px;
    position: relative;
  }
`;
