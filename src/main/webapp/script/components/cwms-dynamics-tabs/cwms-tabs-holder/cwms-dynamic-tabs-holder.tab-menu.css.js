import { css } from "../../../thirdparty/lit-element/lit-element.js";

export const styling = (context) => css`
  :host {
    display: flex;
    flex-direction: column;
  }

  .tab-nav-main {
    display: flex;
    height: 25px;
    background: transparent;
    gap:5px;
  }

  .tab-nav-main .tab-element:hover,
  .tab-nav-main .tab-element.selected {
    background-color: var(--cwms-main-gray-background, #dedede);
    color: var(--cwms-main-dark-font-color, black);
  }

  .tab-nav-main .tab-element {
    color: #fff;
    display: block;
    float: left;
    text-decoration: none;
    position: relative;
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
    padding: 5px 20px 5px 20px;
    background-color: var(--cwms-main-dark-background, #095ba5);
    color: var(--cwms-main-light-font-color, white);
    cursor: pointer;
    height:unset;
  }

  .tab {
    margin-top: 1px;
    padding:10px;
    background-color: var(--cwms-main-gray-background, #dedede);
  }
`;
