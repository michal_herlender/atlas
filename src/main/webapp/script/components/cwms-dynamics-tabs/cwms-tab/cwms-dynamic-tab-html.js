import { html } from '../../../thirdparty/lit-element/lit-element.js'

export const template = (context) => html`
<div>
    <slot></slot>
</div>
`;