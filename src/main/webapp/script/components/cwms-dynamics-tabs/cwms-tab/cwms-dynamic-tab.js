import { LitElement, html } from "../../../thirdparty/lit-element/lit-element.js";
import { template } from "./cwms-dynamic-tab-html.js";

class CwmsDynamicTab extends LitElement {

    static get properties(){
        return {
            tabName:{type:String},
            tabLabel:{type:String}
        }
    }

    get holder(){
        if(!this._holder)
            this._holder = this.closest("nav");
        return this._holder;
    }

    set tabLabelProvider(provider){
        this._labelProvieder = provider;
        if(provider instanceof Promise)
            provider.then(msg => this.tabLabel = msg)
        else
        provider?.subscribe?.(msg => this.tabLabel = msg);
    }

    get tabLabelProvider(){
        return this._labelProvieder;
    }

    set tabLabel(tabLabel){
        const oldValue = this.tabLabel;
        this._tabLabel = tabLabel;
        this.requestUpdate("tabLabel",oldValue);
        this.holder?.requestUpdate();
    }

    get tabLabel(){
        return this._tabLabel;
    }

    connectedCallback(){
        super.connectedCallback();
        const slotName =this.getAttribute("slot");
        if(slotName) this.tabName = slotName;
        else this.setAttribute("slot",this.tabName);
        this.holder?.subscribe(this.tabName,this);
    }

    render(){
       return template({});
    }

}


customElements.define("cwms-dynamic-tab",CwmsDynamicTab);