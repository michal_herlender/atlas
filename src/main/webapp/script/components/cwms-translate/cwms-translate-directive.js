import { directive } from "../../thirdparty/lit-html/lit-html.js";
import CwmsTranslate from "./cwms-translate.js";

export const cwmsTranslateDirective = directive((code,defaultMessage,...args) => part => {

        part.setValue(applyArgumentsToMessage(defaultMessage ?? code,args));
        CwmsTranslate.messagePromise(code,defaultMessage).then(msgT => {
            part.setValue(applyArgumentsToMessage(msgT,...args));
            part.commit();
        });
});

export const  applyArgumentsToMessage = (msg,...args)=>args.reduce((acc,value,indx)=>acc.replace(`{${indx}}`,value),msg);