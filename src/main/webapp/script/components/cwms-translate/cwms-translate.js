import {MetaDataObtainer} from "../../tools/metadataObtainer.js"

export default class CwmsTranslate extends HTMLElement {
	 
	static get translationsWorkerURL(){ return "script/tools/TranslationsSharedWebWorker.js" }
	static get translationsURL(){ return new Request("getTranslations.json").url; }
	/**
	 * Requires global variable 'i18locale' for locale (initialized in
	 * crocodileTemplate.tag)
	 */
	static get language() { return MetaDataObtainer.locale ?? i18locale;  }
	static get prefix() { return 'cwms_translation_' }
	static get cwmsVersionKey() { return 'cwms_version' }
	static get observedAttributes() { return ['code']; }

	/** Returns the language concatenated with the provided code and a prefix */
	static getFormattedMessageKey(code) {
		return CwmsTranslate.prefix+CwmsTranslate.language+'_'+code;
	}

     constructor() {
         super();
         
         // check for attribute 'code'
         this.translationCode = this.getAttribute('code');
         if (this.translationCode === undefined) {
             console.error('CwmsTransl : No Translation code is provided');
             return;
         }
     }
     
     connectedCallback() {
		 this.updateTranslation(this.translationCode);
     }

	 updateTranslation(code){
		CwmsTranslate.subscribeMessage(code, this.innerText, msg => {
			this.innerText = msg
		});
	 }
     
	 attributeChangedCallback(name, oldValue, newValue) {
		 if(name != 'code') return;
		 if(oldValue != newValue){
			this.translationCode = newValue;
		 }
	  }

     static getMessage(code, defaultMessage) {
    	 let translation = localStorage.getItem(CwmsTranslate.getFormattedMessageKey(code));
    	 if(translation)
    		 return translation;
    	 else {
    		 this.subscribeMessage(code, defaultMessage, (translationMsg, translationCode) => {
    			 if(translationMsg)
    				 console.debug('Translation with code: "',translationCode,'", downloaded: "',translationMsg,'".');
    			 else
    				 console.debug('Translation with code: "',translationCode,'" is queued to be downloaded.');
    		 });
    		 return defaultMessage;
    	 }
	 }

	 static messagePromise(code,defaultMessage){
		 return new Promise(resolve => this.subscribeMessage(code,defaultMessage,resolve))
	 }
	 
	 static subscribeMessage(code, defaultMessage, callback) {
		 const translation = localStorage.getItem(CwmsTranslate.getFormattedMessageKey(code));
		 if(translation) {
			 callback(translation, code);
		 } else {
			 callback(defaultMessage, code);
		 }
		 var worker = new SharedWorker(CwmsTranslate.translationsWorkerURL);
		 worker.port.start();
		 const csrfToken = document.querySelector("meta[name='_csrf']")?.getAttribute("content");
    	 worker.port.postMessage({"locale":CwmsTranslate.locale,"code":code, "url": CwmsTranslate.translationsURL, csrfToken});
    	 worker.port.addEventListener("message", event => {
    		 /** get cwms version */
    		 const cwmsVersion = event.data.version;
    		 // if there is a new version (or a different version)
    		 if(cwmsVersion != localStorage.getItem(CwmsTranslate.cwmsVersionKey)){
    			 // clear all storage
    			 localStorage.clear();
    			 // set the new version
    			 localStorage.setItem(CwmsTranslate.cwmsVersionKey, cwmsVersion);
    		 }
    		 /** get translation */
    		 const downloadedTranslation = event.data.translation;
    		 // save only if the translation exists AND, the cwmsVersion is a
    		 // number : in order to disable the cache while in Development
    		 // mode
    		 if(downloadedTranslation && !isNaN(cwmsVersion))
    			 localStorage.setItem(CwmsTranslate.getFormattedMessageKey(code), downloadedTranslation);
    		 
			callback(downloadedTranslation || defaultMessage, code);
			
        }, false);
	 }
}

window.customElements.define('cwms-translate', CwmsTranslate);

/**
 * put in Global scope, so it can be accessed in all modules in the same page
 * (static functions). P.S.1 : still have to import the translation module
 * cwms-translate.js. P.S.2 : Since this is a module, you can access the global
 * object 'CwmsTranslate' only from other modules or after the page is loaded
 * (onload event.., or in our init() function)
 */
globalThis.CwmsTranslate = CwmsTranslate;