import { html } from '../../thirdparty/lit-element/lit-element.js'
import { until } from '../../thirdparty/lit-html/directives/until.js';
import { urlWithParamsGenerator } from '../../tools/UrlGenerator.js';
import {stylesMixin} from '../tools/applicationStylesMixinTemplate.js'
import {  currencyFormatWithZeroDirective, dateFormatDirective } from "../tools/formatingDirectives.js";
import { authorityChecker } from "../../tools/AuthoritiesChecker.js";
import { popUpTemplate } from "../tools/popUpTemplate.html.js";
import CwmsTranslate from '../cwms-translate/cwms-translate.js';
import { DateUtils } from '../tools/date-utils.js';
import { ifDefined } from '../../thirdparty/lit-html/directives/if-defined.js';


export const template = (context) => html`
${stylesMixin}
${context.showPopUp?popUpTemplate({
	title: CwmsTranslate.messagePromise("editbpo.headtext","Edit BPO"),
	overlayContent:context.popUpContentTemplate(context),
	hide:_=>context.showPopUp=false
}):html``}
<table class="default2" id="BPOtable">
	<thead>
		<tr>
			<td colspan="7"><cwms-translate code="bpo.activetitle">Active Blanket Purchase Orders</cwms-translate> (<span id="activeBPOCount">${context.activeBPOs?.length ?? 0}</span>)</td>
		</tr>
		<tr>
			<th class="bpoPONum" scope="col"><cwms-translate code="bpo.ponum">PO Number</cwms-translate></th>
			<th class="bpoComment" scope="col"><cwms-translate code="bpo.comment">Comment</cwms-translate></th>
			<th class="bpoStartDate" scope="col"><cwms-translate code="bpo.startdate">Start Date</cwms-translate></th>
			<th class="bpoEndDate" scope="col"><cwms-translate code="bpo.enddate">End Date</cwms-translate></th>
			<th class="bpoLimit" scope="col"><cwms-translate code="bpo.limitamount">Limit Amount</cwms-translate></th>
			<th class="bpoUsage" scope="col"><cwms-translate code="usage">Usage</cwms-translate></th>
			<th class="bpoEdit" scope="col"><cwms-translate code="edit">Edit</cwms-translate></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="7">
			${secureLinkTemplate(context)(null)("BPO_FOR_COMPANY_CREATE")}
			</td>
		</tr>
	</tfoot>
    <tbody>
        ${ context.activeBPOs.length == 0 ? html`
				<tr class="center bold">
					<td colspan="7"><cwms-translate code="bpo.noactivebpos">There are no active BPOs</cwms-translate></td>
				</tr>`:context.activeBPOs.map(renderBpo(context))
        
        }
    </tbody>
	
	<thead>
		<tr>
			<td colspan="7"><cwms-translate code="bpo.inactivetitle">Inactive Blanket Purchase Orders</cwms-translate> (<span id="inactiveBPOCount">${context.inactiveBPOs?.length ?? 0}</span>)</td>
		</tr>
	</thead>
	
    <tbody>
        ${context.inactiveBPOs == 0 ? html`
				<tr class="center bold">
					<td colspan="7"><cwms-translate code="bpo.noinactivebpos">There are no inactive BPOs</cwms-translate></td>
				</tr>`:context.inactiveBPOs.map(renderBpo(context))
        
        }
    </tbody>
</table>
`;

const renderBpo = context => bpo => html`
    <tr>
		<td>${bpo.poNumber}</td>
		<td>${bpo.comment}</td>
		<td class="center">${dateFormatDirective(bpo.durationFrom)}</td>
		<td class="center">${dateFormatDirective(bpo.durationTo)}</td>
		<td class="right">${currencyFormatWithZeroDirective(bpo.limitAmount,bpo.currencyCode)}</td>
		<td class="center">
			<a class="mainlink-float" href=${urlWithParamsGenerator("viewbpousage.htm",{poid:bpo.poId}).toString()} >
				<cwms-translate code="showall">Show All</cwms-translate>
			</a>
		</td>
		<td class="center">
			${secureLinkTemplate(context)(bpo)("COMPANY_BPO_EDIT")}
		</td>

	</tr>
`;

const secureLinkTemplate = context => bpo => auth => until(authorityChecker.hasAuthority(auth)
.then(e => e?html`
<a role="button" class="mainlink-float"  @click=${_ => context.editBPO(auth,bpo)}>
  ${auth == "COMPANY_BPO_EDIT"?html`<img src="img/icons/form_edit.png" width="16" height="16" title="${context.textEditBPO}" alt="form edit icon"/>`:
  html`
	<cwms-translate code="createbpofor.linktext">Create BPO For </cwms-translate> ${context.unitName}
  `
  }
</a>
`:html``).catch(() =>html`No Authorities` ) ,html`linking `);


export const editBPOPopUpTemplate =  (bpo) => (config) => (context) => html`
  <div class="thickbox-box" id="BPOContent">
    <div class="hid warningBox1">
      <div class="warningBox2"><div class="warningBox3"></div></div>
    </div>
	<form @submit=${context.saveBPO(bpo)}>
    <fieldset>
      <legend>${until(config.legend,"BPO for")} </legend>
      <ol>
        <li>
          <label><cwms-translate code="bpo.ponum">BPO Number</cwms-translate>:</label><input type="text" id="bponumber" name="poNumber" value=${ifDefined(bpo?.poNumber)} />        </li>
        <li>
          <label><cwms-translate code="bpo.comment">Comment</cwms-translate>:</label><textarea id="bpocomment" name="comment" value=${ifDefined(bpo?.comment)} cols="50" rows="4">${bpo?.comment}</textarea
          >
        </li>
        <li class="relative">
          <label><cwms-translate code="bpo.startdate">Start Date</cwms-translate>:</label
          ><input
            type="date"
            id="bpostartdate"
            name="durationFrom"
            value=${ifDefined(DateUtils.dateToISODateString(bpo?.durationFrom))}
          />
        </li>
        <li class="relative">
          <label><cwms-translate code="bpo.enddate">End Date</cwms-translate>:</label
          ><input
            type="date"
            id="bpoenddate"
            name="durationTo"
            value=${ifDefined(DateUtils.dateToISODateString(bpo?.durationTo))}
          />
        </li>
        <li>
          <label><cwms-translate code="bpo.limitamount">Limit Amount</cwms-translate>:</label><input  id="bpolimit" name="limitAmount" value=${bpo? bpo.limitAmount : " "} />
        </li>
        <li><label><cwms-translate code="bpo.active">Active</cwms-translate>:</label><input type="checkbox" name="active" id="bpoactive" value=${true} ?checked=${bpo?.active} /></li>
        <li id="poSubmit">
          <label>&nbsp;</label
          ><button type="submit"
          >${until(config.submitLabel,"submit")}</button>
        </li>
      </ol>
    </fieldset></form>
  </div>
`;

export const bPOPopUpTemplate = (bpo) => (config) => (context) => popUpTemplate({
	title: config.title,
	overlayContent:editBPOPopUpTemplate(bpo)(config)(context),
	hide:_=>context.showPopUp=false
});
