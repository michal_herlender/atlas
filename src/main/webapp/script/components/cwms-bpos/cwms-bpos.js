import { LitElement, html } from "../../thirdparty/lit-element/lit-element.js";
import { styling } from "./cwms-bpos.css.js";
import { template, bPOPopUpTemplate } from "./cwms-bpos.html.js";
import CwmsTranslate from '../cwms-translate/cwms-translate.js';
import { urlGenerator } from "../../tools/UrlGenerator.js";
import { MetaDataObtainer } from "../../tools/metadataObtainer.js";

class CwmsBpos extends LitElement {

    _bPosList;
    _bPosMap;

    static get properties(){
        return {
            bPosList:{type:Array},
            unitName:{type:String},
            textEditBPO:{type:String},
            showPopUp:{Boolean},
            popUpContentTemplate:{type:Object},
            scope:{type:String},
            unitId:{type:Number},
        };
    }

    get activeBPOs() {
        return Array.from(this._bPosMap.values()).filter(e => e.active);
    }

    get inactiveBPOs() {
        return Array.from(this._bPosMap.values()).filter(e => !e.active);
    }

    /**
     * @param {any[]} list
     */
    set bPosList(list){
        if(! list instanceof Array) return;
        const oldMap = this._bPosMap;
        const map = new Map(list.map(e => [e.poId,e]));
        this._bPosMap = map;
        this._bPosList = list;
        this.requestUpdate("bposMap",oldMap);
    }

    get bPosList(){
        return this._bPosList;
    }

    constructor(){
        super();
        this.textEditBPO = "Edit";
        this.unitName ="You";
        this._bPosList = [];
        this._bPosMap = new Map();
    }

    static get styles(){
        return [styling()];
    }

    editBPO(auth,bpo){
        let titleCode,titleDefault,actionCode,actionDefault;
        if(auth=="BPO_FOR_COMPANY_CREATE"){
           [titleCode,titleDefault] = ["createbpo.linktext","Create BPO"];
           [actionCode,actionDefault] = ["create","Create"];
        }
        else {
           [titleCode,titleDefault] = ["editbpo.headtext","Edit BPO"];
           [actionCode,actionDefault] = ["edit","Edit"];
        }
        this.popUpContentTemplate = bPOPopUpTemplate(bpo)(
            (new BPOPopUpConfigBuilder)
            .withLegend("bpo.editBPOFor","{0} BPO For {1}")
            .withAction(actionCode,actionDefault)
            .withTitle(titleCode,titleDefault)
            .withUnit(this.unitName).withSubmit("showitemsoncbpo.savebpo","Save BPO")
            .build()
        );
        this.showPopUp = true;
    }

    saveBPO(bpo) {
        return e => {
            e.preventDefault();
            const f = new FormData(e.currentTarget);
            const o = Object.fromEntries(f.entries());
            const body = { ...bpo, unitId:bpo?.unitId??this.unitId, scope:bpo?.scope?.name ??this.scope,...o,active:this.shadowRoot.getElementById("bpoactive").checked}
            fetch(urlGenerator("bpo/save.json"),{
                method:"POST",
                credentials:"same-origin",
                headers:{...MetaDataObtainer.csrfHeader, "Content-Type": "application/json"},
                body:JSON.stringify(body)
            }).then(res => res.ok ? res.json():Promise.reject(res.statusText))
            .then(out => {
                this.appendBpo(out);
                this.showPopUp = false;
            },console.error);
        }

    }

    appendBpo(bpo){
        this._bPosMap.set(bpo.poId,bpo);
        this.requestUpdate();
    }

    render(){
       return template(this);
    }

}


class BPOPopUpConfigBuilder {
    _actionCode;_legendCode; _titleCode; _submitCode;_unitName;
    _actionDefault;_legendDefault; _titleDefault; _submitDefault;
    
    withAction(actionCode,actionDefault){
        this._actionCode = actionCode;
        this._actionDefault = actionDefault;
        return this;
    }

    withLegend(legendCode,legendDefault){
        this._legendCode = legendCode;
        this._legendDefault = legendDefault;
        return this;
    }   

    withTitle(titleCode,titleDefault){
        this._titleCode = titleCode;
        this._titleDefault = titleDefault;
        return this;
    }

    withSubmit(submitCode,submitDefault){
        this._submitCode = submitCode;
        this._submitDefault = submitDefault;
        return this;
    }

    withUnit(unitName){
        this._unitName = unitName;
        return this;
    }

    build(){
     return {
         legend:Promise.all([CwmsTranslate.messagePromise(this._legendCode,this._legendDefault),
         CwmsTranslate.messagePromise(this._actionCode,this._actionDefault)])
         .then(([legend,action]) => legend.replace("{0}",action).replace("{1}",this._unitName)),
         title:CwmsTranslate.messagePromise(this._titleCode,this._titleDefault),
         submitLabel:CwmsTranslate.messagePromise(this._submitCode,this._submitDefault),
     } ;  
    }
}

customElements.define("cwms-bpos",CwmsBpos);