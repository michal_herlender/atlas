import { html, render } from '../../thirdparty/lit-html/lit-html.js';
import {repeat} from '../../thirdparty/lit-html/directives/repeat.js'

export let htmlTemplate = (context) => html `

<vaadin-context-menu open-on="click">
	<vaadin-button theme="icon" id="hideShowColumns">
		<iron-icon icon="vaadin:grid-h"></iron-icon>
    	<cwms-translate code="columns" >Columns</cwms-translate> 
	</vaadin-button>
</vaadin-context-menu>

<vaadin-context-menu open-on="click" .context="${context}"
		.renderer="${hideShowRowsRadioGroupRenderer}">
	<vaadin-button theme="icon">
	    <iron-icon icon="vaadin:grid-v"></iron-icon>
	    <cwms-translate code="rows" >Rows</cwms-translate>
	</vaadin-button>
</vaadin-context-menu>

<vaadin-context-menu open-on="click" .context="${context}"
		.renderer="${hideShowEntriesRadioGroupRenderer}">
	<vaadin-button theme="icon">
	    <iron-icon icon="vaadin:area-select"></iron-icon>
	    <cwms-translate code="newjobitemsearch.resperpage" >Results Per Page</cwms-translate>
	</vaadin-button>
</vaadin-context-menu>

<div class="progressBarWrapper">
	<vaadin-progress-bar id="rowsProgressBar" ?hidden=${false} indeterminate value="0"></vaadin-progress-bar>
</div>
<vaadin-grid id="rowsGrid" .items="${context.items}"
		height-by-rows page-size="${context.pageSize}" theme="row-dividers compact column-borders row-stripes"
		column-reordering-allowed >
		
		${context.columns.length == 0 ? ``
		      : html`
					<vaadin-grid-column-group resizable auto-width header="ERP columns">
						<vaadin-grid-column auto-width path="index"	header="Excel columns">
						</vaadin-grid-column>
					</vaadin-grid-column-group>
		      `
		}
		${repeat(context.columns,(i,index)=>html`
			<vaadin-grid-column-group ?hidden="${index > context.firstColumnsToShow }" resizable auto-width 
					header="${i.efColumn}" excel-column="${i.excelColumn}" >
				<vaadin-grid-column auto-width ef-column="${i.efColumn}" excel-column="${i.excelColumn}"
					.renderer="${analysisRenderer}"	header="${i.excelColumn}" >
				</vaadin-grid-column>
			</vaadin-grid-column-group>
		`)}
		
		${context.columns.length == 0 ? ``
		      : html`
					<vaadin-grid-column-group resizable auto-width header="Actions">
						<vaadin-grid-column auto-width .renderer="${actionsRenderer}" job-name="${context.jobName}">
						</vaadin-grid-column>
					</vaadin-grid-column-group>
		      `
		}
    
</vaadin-grid>
    
<br/>

${ !context.hasPagesInfo ? '' : html`
<div style="float:left">
	${context.pagesWithWarnings.length == 0 ? `` : html`
		<span class="warning">
			<cwms-translate code="importbatch.page.with.warnings" >Pages with warnings</cwms-translate>:
			 ${context.pagesWithWarnings.join()}
		</span><br/>
	`}
	${context.pagesWithErrors.length == 0 ? `` : html`
		<span class="error">
			<cwms-translate code="importbatch.page.with.errors" >Pages with errors</cwms-translate>:
			${context.pagesWithErrors.join()}
		</span>
	`}
</div>
`}
    
<div style="float:right">
	<vaadin-button theme="icon" @click="${context.prevPage}" ?disabled="${context.currentPage==1}">
 		<iron-icon icon="vaadin:angle-left"></iron-icon>
	</vaadin-button>
	<span id="pageStats"> ${context.currentPage} / ${context.pageCount}</span> 
	<vaadin-button theme="icon" @click="${context.nextPage}" ?disabled="${context.currentPage==context.pageCount}">
 		<iron-icon icon="vaadin:angle-right"></iron-icon>
	</vaadin-button>
</div>
   
<br/>
<br/>
<br/>
`;

function analysisRenderer(root, column, rowData) {
	let excelColumn = column.getAttribute('excel-column');
	let cellData = rowData.item.data.filter(c=>c.excelColumn == excelColumn)[0];
	if(!column.hasAttribute('has-errors') && cellData.errors.length >0)
		column.setAttribute('has-errors', 'true');
	if(!column.hasAttribute('has-warnings') && cellData.warnings.length >0)
		column.setAttribute('has-warnings', 'true');
    render(
      html`
        <div>${cellData.value}</div>
        
        ${repeat(cellData.warnings,(i,index)=>html`
        	<div class="warning" >${i}</div>
        `)}
        ${repeat(cellData.errors,(i,index)=>html`
        	<div class="error" >${i}</div>
        `)}
      `, 
      root
    );
    root.style.fontSize = "10px";
}

function actionsRenderer(root,column,rowData){
	let jobName = column.getAttribute('job-name');
	let metadata = rowData.item.metadata;
	render(
	   html`
	   	${jobName == 'INSTRUMENT_IMPORT_JOB' ? ``
			: html`
       			<a target="_blank" href="viewinstrument.htm?plantid=${metadata}">${metadata}</a>
	   	`}
	   `
     , 
     root
   );
   root.style.fontSize = "10px";
}

function hideShowRowsRadioGroupRenderer(root, contextMenu, button){
	render(
		html`
			<vaadin-radio-group theme="vertical" style="font-size : 12px !important;">
			  <vaadin-radio-button @click="${contextMenu.context.rowsToDisplayFilterChanged}"
				value="ALL" checked>
					<cwms-translate code="all" >All</cwms-translate>
			 </vaadin-radio-button>
			 <vaadin-radio-button @click="${contextMenu.context.rowsToDisplayFilterChanged}"
			  	value="ONLY_ROWS_WITH_ERRORS" >
			  	<cwms-translate code="importbatch.rows.with.errors" >Only rows with errors</cwms-translate>
			 </vaadin-radio-button>
			 <vaadin-radio-button @click="${contextMenu.context.rowsToDisplayFilterChanged}"
			  	value="ONLY_ROWS_WITH_WARNINGS" >
			  	<cwms-translate code="importbatch.rows.with.warnings" >Only rows with warnings</cwms-translate>
			 </vaadin-radio-button>
			</vaadin-radio-group>
		`,
		root, {eventContext: contextMenu.context}
	);
}

function hideShowEntriesRadioGroupRenderer(root, contextMenu, button){
	render(
		html`
			<vaadin-radio-group theme="vertical" style="font-size : 12px !important;">
			  <vaadin-radio-button @click="${contextMenu.context.entriesToDisplayFilterChanged}"
			  	value="25" checked>25</vaadin-radio-button>
			  <vaadin-radio-button @click="${contextMenu.context.entriesToDisplayFilterChanged}"
			  	value="50" >50</vaadin-radio-button>
			  <vaadin-radio-button @click="${contextMenu.context.entriesToDisplayFilterChanged}"
			  	value="100" >100</vaadin-radio-button>
			</vaadin-radio-group>
		`,
		root, {eventContext: contextMenu.context}
	);
}



