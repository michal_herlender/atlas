import { css } from '../../thirdparty/lit-element/lit-element.js'

export let styling = (context) => css`

vaadin-grid {
	--lumo-font-size-s : 11px;
	--lumo-font-size-m : 13px;
}

vaadin-context-menu {
	display : contents !important;
}

vaadin-context-menu * {
	font-size : 13px !important;
}

.small-text {
	font-size : 10px !important;
}

.ef-row::slotted(vaadin-grid-cell-content) {
	background-color : lightblue;
}

.warning {
	color : orange;
}
.error {
	color : red;
}

.filterTable{
	margin : 20px;
}

`;