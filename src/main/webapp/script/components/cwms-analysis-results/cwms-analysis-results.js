import { htmlTemplate } from "./cwms-analysis-results.html.js";
import { styling } from "./cwms-analysis-results.css.js";
import { LitElement } from '../../thirdparty/lit-element/lit-element.js';
import { html, render } from '../../thirdparty/lit-html/lit-html.js';
import '../../thirdparty/@vaadin/vaadin-grid/all-imports.js';
import '../../thirdparty/@vaadin/vaadin-progress-bar/vaadin-progress-bar.js';
import '../../thirdparty/@vaadin/vaadin-button/vaadin-button.js';
import '../../thirdparty/@vaadin/vaadin-icons/vaadin-icons.js';
import '../../thirdparty/@vaadin/vaadin-context-menu/vaadin-context-menu.js';
import '../../thirdparty/@vaadin/vaadin-checkbox/vaadin-checkbox.js';
import '../../thirdparty/@vaadin/vaadin-radio-button/vaadin-radio-group.js';

export default class CwmsAnalysisResults extends LitElement {
	
	static get dataURL(){ return "getAllAnalysisResults.json" };

    static get properties() {
        return {
        	jobName : { attribute : 'job-name' },
        	stepExecutionId : { attribute : 'step-execution-id' },
        	efStartIndex : { attribute : 'ef-start-index', type : Number },
        	items : { type : Array },
            currentPage : { type : Number },
            pageCount : { type : Number },
            columns : { type : Array },
            firstColumnsToShow : { type : Number,  attribute : 'first-columns-to-show' },
            hasPagesInfo : { type : Boolean },
            pagesWithWarnings : { type : Array },
            pagesWithErrors : { type : Array },
            rowsToDisplay : { type : String },
            pageSize : { attribute : 'page-size', type : Number }
        };
    }
    
    static get styles() {
        return styling(this);
    }
    
    constructor(){
        super();
        this.currentPage = 1;
        this.columns = new Array();
        this.hasPagesInfo = false;
        this.pagesWithWarnings = new Array();
        this.pagesWithErrors = new Array();
        this.rowsToDisplay = 'ALL';
    }
    
    get grid() { return this.shadowRoot.querySelector('#rowsGrid'); }
    get progressBar() { return this.shadowRoot.querySelector('#rowsProgressBar'); }
    

    render() {  
        return htmlTemplate(this);
    }
    
    firstUpdated(){
    	this.refresh();
    }
    
    refresh(){
    	this.showProgress(true);
    	this.columns = new Array();
    	fetch(CwmsAnalysisResults.dataURL+'?batchStepExecutionId='+this.stepExecutionId+'&page=' + this.currentPage+ '&maxResults=' 
    			+ this.grid.pageSize+'&requestPagesInfo='+!this.hasPagesInfo+'&rowsToDisplay='+this.rowsToDisplay).then(res => res.text().then(text => {
    		let res = JSON.parse(text);
            this.items = res.results;
            this.readColumns();
            this.currentPage = res.currentPage;
            this.pageCount = res.pageCount;
            this.showProgress(false);
            if(!this.hasPagesInfo){
            	this.initializePagesInfo(res.metaData, res.resultsCount)
            	this.hasPagesInfo = true;
            }
        })); 
    }
    
    initializePagesInfo(pagesInfo, resultsCount){
    	let dataStartIndex = this.efStartIndex+1;
    	// loop on pages
    	for(let i=1;i<resultsCount;i++){
    		let startIndex = dataStartIndex + (i - 1) * this.grid.pageSize;
    		let endIndex = startIndex + this.grid.pageSize - 1;
    		let arr = pagesInfo.filter(i=>{return i.index >= startIndex && i.index <= endIndex});
    		if(arr.some(i=>i.hasError))
    			this.pagesWithErrors.push(i);
			if(arr.some(i=>i.hasWarning))
				this.pagesWithWarnings.push(i);
    	}
    }
    
    showProgress(show){
    	this.progressBar.hidden=!show;
    }
    
    readColumns(){
    	let columns = new Set();
    	this.items.flatMap(i=>i.data).forEach(c=> {
    		// we're using stringify in order to use Set (it works only with
			// strings)
    		columns.add(JSON.stringify({'efColumn':c.efColumn.fullName,'excelColumn':c.excelColumn}));
    	});
    	columns.forEach(c=>{
    		let jsonValue = JSON.parse(c);
    		this.columns.push(jsonValue);
    	});
    	
    	// init show/hide columns context menu
    	this.initShowHideContextMenu(this.columns);
    	
    }
    
    initShowHideContextMenu(columns) {
    	// put column in hide/show context menu
    	const contextMenu = this.shadowRoot.querySelector('vaadin-context-menu');
        contextMenu.renderer = function(root){
        	root.innerHTML = '';
        	columns.forEach(function(column, i) {
        		
        		const grid = document.querySelector('cwms-analysis-results').shadowRoot.querySelector('vaadin-grid')
            	const gridGroupColumn = grid.querySelector('vaadin-grid-column-group[excel-column="'+column.excelColumn+'"]');
        		const gridColumn = grid.querySelector('vaadin-grid-column[excel-column="'+column.excelColumn+'"]');
        		
        		const checkbox = window.document.createElement('vaadin-checkbox');
        		
                checkbox.style.display = 'block';
                checkbox.style.fontSize = '12px';
                if(gridColumn.hasAttribute('has-warnings'))
                	checkbox.style.color = 'orange';
                if(gridColumn.hasAttribute('has-errors'))
                	checkbox.style.color = 'red';
                
                checkbox.textContent = column.excelColumn+' ('+column.efColumn+')';
            	checkbox.checked = !gridGroupColumn.hidden;
                checkbox.addEventListener('change', function() {
                	gridGroupColumn.hidden = !checkbox.checked;
                });
                // Prevent the context menu from closing when clicking a
				// checkbox
                checkbox.addEventListener('click', function(e) {
                  e.stopPropagation();
                });
                root.appendChild(checkbox);
        	});
        };
    }
    
    prevPage() {
    	if(this.currentPage > 1) {
    		this.currentPage--;
    		this.refresh();
    	}
    }
    
    nextPage() {
    	if(this.currentPage < this.pageCount) {
    		this.currentPage++;
    		this.refresh();
    	}
    }
    
    rowsToDisplayFilterChanged(e){
    	this.rowsToDisplay = e.target.value;
		this.refresh();
    	e.preventDefault();
    }
    
    entriesToDisplayFilterChanged(e){
    	this.pageSize = e.target.value;
    	this.grid.pageSize = e.target.value;
    	this.hasPagesInfo = false;
    	this.pagesWithWarnings = new Array();
        this.pagesWithErrors = new Array();
		this.refresh();
    	e.preventDefault();
    }
    
};

window.customElements.define('cwms-analysis-results', CwmsAnalysisResults);