import { html } from '../../thirdparty/lit-element/lit-element.js'
import {  viewSynthesisLinkGenerator } from '../../tools/UrlGenerator.js';
import { ExchangeFormatFieldMapping } from '../tools/exchange-fromat-utils/utils.js';
import "../cwms-tooltips/cwms-jobitem-link-and-tooltip/cwms-jobitem-link-and-tooltip.js";
import "../cwms-tooltips/cwms-instrument-link-and-tooltip/cwms-instrument-link-and-tooltip.js";
import { popUpTemplate } from '../tools/popUpTemplate.html.js';

export const template = (context) => html`
                ${context.showPopup? html`${popUpTemplate({
                    title:html`<cwms-translate code="exchangeformat.duplicates.selectInstrument">Select instrument</cwms-translate>`,
                    overlayContent:context.popUpContent,
                    hide:context.hidePopUp})}`:html``}
<table id="table_company" class="default2 companySubdivs">
						<thead>
                            ${headerRenderer(context)}
						</thead>
                        <tbody style="padding: 0px">
                        ${context.items.map(renderRow(context))}
                        </tbody>
                    </table>
`;


const headerRenderer = context => {
    const {tableHeader} = context;
    const containsEfTrescalId = Object.values(tableHeader).findIndex(v => v.name=="ID_TRESCAL" ) > -1;
    return html`
        <tr>
            <th class="codefault title"><cwms-translate code="filesynthesis.tablefiledata">E.Format File Data</cwms-translate></th>
            ${!containsEfTrescalId?html`<th class="codefault erp">Id Trescal</th>`:html``}
            ${Object.keys(context.tableHeader).map(k => html`
                <th class="codefault erp">${k}</th>
            `)}
            <th class="codefault title" rowspan="2">Service Type</th>
            <th class="codefault title" rowspan="2">Statut</th>
        </tr>
        <tr>
            <th class="codefault title"><cwms-translate code="filesynthesis.tableerpdata">ERP Data</cwms-translate></th>
            ${!containsEfTrescalId?html`<th class="codefault file"><cwms-translate code="exchangeformat.fieldname.idtrescal">ID Trescal</cwms-translate></th>`:html``}
            ${Object.values(tableHeader).map(v => html`
								<th class="codefault file">${v.value}</th>
            `)}
        </tr>
    `;

};
const renderRow = context => (row,idx) =>{ 
    const containsEfTrescalId = Object.values(context.tableHeader).findIndex(v => v.name=="ID_TRESCAL" ) > -1;
    return html`
<tr>
    <td class="codefault title" data-row-index="${idx}">#${idx+1}</td>
    ${!containsEfTrescalId?html`
    <td>${row.idTrescal}</td>
    `:html``}
    ${Object.values(context.tableHeader).map(h => {
        const descriptor = ExchangeFormatFieldMapping[h.name];
     return   html`
        <td>${row[descriptor?.dtoField ?? h.name]}</td>
    `})}
    <td>${renderServiceTypes(context,row)}</td>
    <td>${renderStatus(context,row,idx)}</td>
</tr>
`}

const renderServiceTypes = (context,row) => html`
    <select @change=${context.selectServiceType(row)}>
        <option label="" value="" ></option>
        ${Object.entries(context.servicesType).map(([k,v]) => html`
            <option ?selected=${row.servicetypeid == k} value="${k}">${v}</option>
        `)}
    </select>
`;

const linkToInstrument = plantId => html`
<cwms-instrument-link-and-tooltip .plantid=${plantId}></cwms-instrument-link-and-tooltip>
`;

const renderStatus = (context,row,idx) => {
    const status = row.status;
    const spanStatus = html`<span class="${status.success?'green':'red'}">${status.message}</span>`;
    switch (status.name){
        case "IDENTIFIED":
        case "ON_OTHER_COMPANY":
        case "BER":
            return html`${spanStatus}<p>${linkToInstrument(row.idTrescal)}</p>`;
        case "IDENTIFIED_ON_OTHER_COMPANY_IN_SAME_GROUP":
            return html`${spanStatus} (${linkToInstrument(row.idTrescal)})
            ${renderLookUpInTheGroup(context,row)}
            `;
        case "NOT_IDENTIFIED_IN_CLIENT_COMPANY":
            return html`<span class="red">${status.message}</span>
            ${renderLookUpInTheGroup(context,row)}
            `
        case "ALREADY_ONPREBOOKING":
            return html`${spanStatus} : <a class="red prebooked"  href="${viewSynthesisLinkGenerator(row.statusData[0]).toString()}">${row.statusData[0]}</a>`;
        case "DUPLICATED_IN_TABLE":
            return html`${spanStatus} 
                <a role="button" class="mainlink"
                @click=${_ => {
                    const element = context.shadowRoot.querySelector(`[data-row-index=${row.statusData[0]}`);
                    element.scrollIntoView();
                }}
                >
                    L:#${row.statusData[0]} EF:#${row.statusData[1]}
                </a>`
        case "ALREADY_ONJOB":
            return html`${spanStatus} <cwms-jobitem-link-and-tooltip jobItemId="${row.statusData[0]}"
            jobItemNo=${row.statusData[1]}
            ></cwms-jobitem-link-and-tooltip> `
        default:
            return spanStatus
    }
}

const renderLookUpInTheGroup = (context,item) => context.groupId?  html`
<a class="mainLink" role="button" @click=${_ => context.searchInstrumentInGroup(item)}><cwms-translate code="exchangeformat.duplicates.searchInstrumentInGroup">Search instrument in the company group</cwms-translate></a>`:html``;

export const renderInputs = (items,tableHeader) =>  {
    return items.map(renderInputsForRow(tableHeader))
}

const renderInputsForRow = tableHeader => (row,idx) => html`
    <input type="hidden" name="asnItemsDTOList[${idx}].asnItemId" value="${ row.asnItemId }" />
    <input type="hidden" name="asnItemsDTOList[${idx}].idTrescal" value="${ row.idTrescal }" />
    <input type="hidden" name="asnItemsDTOList[${idx}].index" value="${ row.index }" />
    <input type="hidden" name="asnItemsDTOList[${idx}].status" value="${ row.status.name }" />
    <input type="hidden" name="asnItemsDTOList[${idx}].servicetypeid" value="${row.servicetypeid ?? ""}"/>
    ${Object.values(tableHeader).map(th => {
        const descriptor = ExchangeFormatFieldMapping[th.name];
        const dtoField = descriptor?.dtoField ?? th.name;
        return html`<input type="hidden" name="asnItemsDTOList[${idx}].${dtoField}" value="${row[dtoField] ?? ""}"/>`;
    })}
`;

