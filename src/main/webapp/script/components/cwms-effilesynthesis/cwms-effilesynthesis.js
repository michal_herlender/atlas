import { LitElement, html } from "../../thirdparty/lit-element/lit-element.js";
import { tableStyling } from "../tools/exchange-fromat-utils/tableRenderer.js";
import { styling } from "./cwms-effilesynthesis.css.js";
import { renderInputs, template } from "./cwms-effilesynthesis.html.js";
import store, { updateItems } from "./store.js";
import {template as spinnerTemplate} from  "../cwms-spinner/cwms-spinner.html.js";
import { queryInstrumentsFromCompanyGroup } from "../tools/exchange-fromat-utils/requestUtils.js";
import { popUpContentTemplate } from "../tools/exchange-fromat-utils/popupTemplate.js";

class CwmsEffilesynthesisInner extends LitElement {

    identifiedStatusTemplate = JSON.parse(`{"success":true,"messageCode":"exchangeformat.import.itemstatus.identified","name":"IDENTIFIED","message":"Identified"}`)

    static get properties() {
        return {
            items: { type: Array },
            tableHeader: { type: Object },
            servicesType: { type: Object },
            showPopup:{type:Boolean, attribute:false},
            groupId:{type:Number},
      popUpContent:{type:Object,attribute:false},
        };
    }


    constructor(){
        super();
    this.initialPopUpContent = html`
    <div class="initial-container">
    ${spinnerTemplate({})}
  </div>
    `;
    this.popUpContent = this.initialPopUpContent;
    }


  searchInstrumentInGroup(item) {
      this.showPopup = true;
      queryInstrumentsFromCompanyGroup(this.groupId,item)
      .then(insts => {
         this.possibleItems = insts;
          this.popUpContent = popUpContentTemplate({items:insts, selectInstrument:this.selectInstrument(item)})
        })
        .catch(console.error);
  }

  selectInstrument = item => instrument => {
    updateItems(this.items.map(r => {
        if(r.index == item.index){
            return {...r,idTrescal:instrument.plantId,serialNo:instrument.serialNo, 
                plantNo:instrument.plantNo, status:this.identifiedStatusTemplate};
        }
        return r;
    }));
    this.showPopup = false;
    this.popUpContent = this.initialPopUpContent;
  }

    selectServiceType(row){
        return e => {
            updateItems(this.items.map(r => {
                    if(r.index== row.index){
                        return {...r,servicetypeid:e.currentTarget.value};
                    }
                    return r;
                }
                ));
    };
}


    hidePopUp(){
        this.showPopup = false;
    }

    static get styles(){
        return [styling({}),tableStyling];
    }

    render(){
       return template(this);
    }

}


customElements.define("cwms-effilesynthesis-inner",CwmsEffilesynthesisInner);


class CwmsEffilesynthesisWraper extends LitElement {
    
    static get properties() {
        return {
            items: { type: Array },
            tableHeader: { type: Object },
            servicesType: { type: Object },
            groupId:{type:Number},
        };
    }


    constructor(){
        super();
        store.subscribe(() => {
            ({items:this.items} = store.getState());
        });
    }
createRenderRoot(){
    return this;
}

    updateItems(items){
        console.log(items);
        this.items = items;
    }

render(){
    return html`<cwms-effilesynthesis-inner 
        .items=${this.items}
        .tableHeader=${this.tableHeader}
        .servicesType=${this.servicesType}
        .groupId=${this.groupId}
        ></cwms-effilesynthesis-inner>
        ${renderInputs(this.items,this.tableHeader)}
        `
}

}

customElements.define("cwms-effilesynthesis",CwmsEffilesynthesisWraper);