import { css } from "../../thirdparty/lit-element/lit-element.js";

export const styling = (context) => css`
  .codefault,
  .codefault.title {
    text-align: center;
    width: 5%;
  }

  td.codefault {
    font-weight: normal;
  }

  .red {
    color: red;
    font-weight: bold;
  }

  .green {
    color: green;
    font-weight: bold;
  }


  [role=button] {
      cursor: pointer;
  }
  
  .mainLink {
    font-weight: bold;
    text-decoration: none;
    line-height: normal;
    color: var(--cwms-main-link-color,#0063be);
  }

  .mainLink:hover {
    text-decoration: underline;
    color: var(--cwms-red, #a80000);
  }

  .prebooked {
    border-bottom: solid 1px blue;
  }
`;
