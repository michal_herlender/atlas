import createStore from "../../tools/flypro.js";

export const UPDATE_ITEMS ="UPDATE_ITEMS";


function dispatch(state,command) {
    switch(command.type){
        case UPDATE_ITEMS:
            return {...state,items:command.items};
        default:
            return state;
    }
}

let efFileSythesisStore; export default efFileSythesisStore = createStore(dispatch,{});

export function updateItems(items){
    efFileSythesisStore.send({type:UPDATE_ITEMS,items});
}