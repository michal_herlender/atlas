import { html } from '../../thirdparty/lit-element/lit-element.js'

export const htmlTemplate = (context) => html `

    <p>Render a list:</p>
    <ul>
        ${context.list.map(
                (item, index) => html`
              <li>${index}: ${item}</li>
            `
        )}
    </ul>
    
`;