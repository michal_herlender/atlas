import {html, LitElement} from '../../thirdparty/lit-element/lit-element.js';
import {styling} from "./cwms-upcoming-work-job-link.css.js";
import {urlGenerator, urlWithParamsGenerator} from "../../tools/UrlGenerator.js";
import {structureStyles} from "../tools/styles/structure.css.js";
import {fieldsetStyles} from "../tools/styles/fieldset.css.js";
import {textStyles} from "../tools/styles/text.css.js";
import "../cwms-translate/cwms-translate.js";


export class CwmsUpcomingWorkJobLink extends LitElement{

    static get dataURL() { return "upcomingWorkJobLink" }

    static get properties() {
        return {
            onInput: {type: String},
            data: { type: Array, attribute: 'data-list', reflect: true },
            upcomingWorkId: { attribute: 'upcomingworkid', reflect: true },
            showMode: { attribute: 'show-mode', reflect: true },
        };
    }

    constructor(){
        super();
        this.data = new Array();
    }

    static get styles(){
        return [ styling,fieldsetStyles,textStyles, structureStyles]
    }

    getData(){
        fetch(urlWithParamsGenerator(CwmsUpcomingWorkJobLink.dataURL,{
            'upcomingWorkId':this.upcomingWorkId,
        })).then(res => res.ok?res.json():Promise.reject(res.statustText))
            .then(json => {
                this.data = json;
                console.log(this.data);
            });
    }

    delete(item){

        fetch(urlWithParamsGenerator(CwmsUpcomingWorkJobLink.dataURL,{
            'upcomingWorkJobLinkId':item.id,
        }),{
            method:"Delete"
        }).then(res => res.ok?res.json():Promise.reject(res.statustText))
            .then(out => {
                console.log(out);
            });
    }

    get onInput() {
        return this.renderRoot?.querySelector('input#jobNoInput') ?? null;
    }

    updateIem(){
        if (!this.onInput.value) {
            return;
        }
        let list = [
            ...this.data,
            {
                id : null,
                upcomingWorkId : this.upcomingWorkId,
                jobNo : this.onInput.value,
                active: true
            }
            ,
        ];
        this.data = list;
        fetch(urlGenerator(CwmsUpcomingWorkJobLink.dataURL),{
            method:"PUT",
            headers:{"Content-Type": "application/json"},
            body:JSON.stringify(list)
        }).then(res => {
            if (res.ok){
                console.log(res.json())
                this.data = list;
            }else {
                Promise.resolve(res.json())
                    .then(message =>  $j.prompt(message))
            }
        }).then(out => {
            console.log(out);
            this.onInput.value = '';
        })
            .catch(error => console.log(error));
    }

    addItem() {
        if (!this.onInput.value) {
            return;
        }
        let list = [
            ...this.data,
            {
                id : null,
                upcomingWorkId : this.upcomingWorkId,
                jobNo : this.onInput.value,
                active: true
            }
            ,
        ];
        this.data = list;
        this.onInput.value = '';
    }

    firstUpdated(changedProperties) {
        if (this.showMode !== 'ADD'){
            this.getData();
        }
    }

    refresh() {
        this.getData();
    }

    removeItem(item) {
        this.delete(item);
        this.data = this.data.filter((i) => i !== item);
    }

    render() {
        return html`
            ${this.showMode === "ADD" ? html`
                <ul>
                    <li>
                        <label>${i18n.t("coveringJobs", "Covering Jobs")}:</label>

                        <input id="jobNoInput" @change="${this.onInput}">
                        <a href="#" @click=${this.addItem}>
                            <img src="img/icons/add.png" width="16" height="16">
                        </a>
                    </li>
                    ${this.data.map(
                            (item,index) => html`
                                    <li>
                                        <label></label>
                                        <input type="text" disabled name="itemJobNo[]" value="${item.jobNo}" />
                                        <a href="#" @click=${() => this.removeItem(item)}>
                                            <img src="img/icons/delete.png" width="16" height="16">
                                        </a>
                                    </li>
                                `
                    )}
                </ul>
            `: html `
                ${this.showMode == "EDIT" ? html`
                    <ul>
                        <li>
                            <label>${i18n.t("coveringJobs", "Covering Jobs")}:</label>
                            
                            <input id="jobNoInput" @change="${this.onInput}">
                            <a href="#" @click=${this.updateIem}>
                                <img src="img/icons/add.png" width="16" height="16">
                            </a>
                        </li>
                        ${this.data.map(
                                (item,index) => html`
                                    <li>
                                        <label></label>
                                        <input type="text" disabled name="title" value="${item.jobNo}" />
                                        <a href="#" @click=${() => this.removeItem(item)}>
                                            <img src="img/icons/delete.png" width="16" height="16">
                                        </a>
                                    </li>
                                `
                        )}
                    </ul>
                ` : html``

                }`}`}

}
customElements.define('cwms-upcomingwork-job-link', CwmsUpcomingWorkJobLink);