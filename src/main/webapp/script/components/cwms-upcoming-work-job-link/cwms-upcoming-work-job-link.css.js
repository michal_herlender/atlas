import { css } from '../../thirdparty/lit-element/lit-element.js'

export const styling = css`
    ul { 
        list-style: none outside none; 
        margin:0; padding: 0; 
        }
    li {
        list-style: none;
        margin-bottom: 5px;
    }
    label {
        width: 140px !important;
    }
    `;