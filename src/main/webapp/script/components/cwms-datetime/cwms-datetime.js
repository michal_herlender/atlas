import { LitElement, html, css } from "../../thirdparty/lit-element/lit-element.js";
import {ifDefined} from "../../thirdparty/lit-html/directives/if-defined.js"
import { DateUtils } from "../tools/date-utils.js";
import { dateTimeFormatFunction } from "../tools/formatingDirectives.js";

class CwmsDatetimeInner extends LitElement {

    static get styles(){
        return css`
            input:invalid {
                box-shadow: 0 0 1.5px 1px red;
            }
        `;
    }

    static get properties(){
        return {
            name:{type:String},
            date:{type:Date,converter:DateUtils.dateTimeConverter },
            minDate:{type: Date, converter: DateUtils.dateTimeConverter},
            maxDate:{type: Date, converter: DateUtils.dateTimeConverter},
            stepTime:{type: Number},
            minTime:{type: String },
            maxTime:{type: String }
        }
    }

    constructor(){
        super();
    }

    connectedCallback(){
        super.connectedCallback();
        this.input = this.querySelector(`input[name="${this.name}"]`);
        if(this.input) this.input.value = this.date ? DateUtils.localDateTimeFormatFunction(this.date):"";
    }

    firstUpdated(){
        this.form = this.shadowRoot?.querySelector("form");
        this.dateInput = this.shadowRoot?.querySelector("input[name='date']");
        this.timeInput = this.shadowRoot?.querySelector("input[name='time']");
        this.form.checkValidity();
    }

    updateSlot(e){
        this.dateInput?.setCustomValidity("");
        this.input?.setCustomValidity("");
        this.form.checkValidity();
       const timeValue = this.timeInput?.value;
       const dateValue = this.dateInput?.value;
       const date = dateValue ? (!timeValue ? dateValue: `${dateValue}T${timeValue}`) : "";
       if(this.input) this.input.value = date ? date: "";
    }

    get dateString() {
        return DateUtils.dateToISODateString(this.date);
    }

    get timeString(){
        return this.date?.toLocaleTimeString([], { hour12: false })??"";
    }

    setCustomValidity(errorMessage) {
        this.dateInput?.setCustomValidity(errorMessage);
        this.input?.setCustomValidity(errorMessage);
        this.dateInput?.form?.reportValidity();
    }


    isInvalid(name){
        return e => {
            this.input.setCustomValidity(name);
        }
    }

    render(){
        return html`<form>
      <input name="date" type="date" value="${ifDefined(this.dateString)}" @change=${this.updateSlot}
        min=${ifDefined(DateUtils.dateToISODateString(this.minDate))} max=${ifDefined(DateUtils.dateToISODateString(this.maxDate))}
        @invalid=${this.isInvalid("time")}
        >
        <input name="time" type="time" value="${ifDefined(this.timeString)}" @change=${this.updateSlot}
        min=${ifDefined(this.minTime)} max=${ifDefined(this.maxTime)} step=${ifDefined(this.stepTime ? (this.stepTime * 60) : undefined)} 
        @invalid=${this.isInvalid("date")}
        />
        </form>
        <slot></slot>
        `
    }

}

 class CwmsDatetimeWrapper  extends LitElement {

    static get properties(){
        return {
            date:{type:Date,converter:DateUtils.dateTimeConverter },
            name:{type:String},
            minDate:{type: Date, converter: DateUtils.dateTimeConverter, attribute: "min-date"},
            maxDate:{type: Date, converter: DateUtils.dateTimeConverter, attribute: "max-date"},
            stepTime:{type: Number, attribute: "step-time"},
            minTime:{type: String, attribute: "min-time"},
            maxTime:{type: String, attribute: "max-time"}
        }
    }

  constructor() {
    super();
    this.name = "datetime";
  }

  createRenderRoot() {
    return this;
  }

  setCustomValidity(errorMessage) {
    this.querySelector("cwms-datetime-inner")?.setCustomValidity?.(errorMessage);
   }

  render() {
    return html`
        <cwms-datetime-inner .name=${this.name} .date=${this.date} .maxDate=${this.maxDate} .minDate=${this.minDate}
        .minTime=${this.minTime} .maxTime=${this.maxTime} .stepTime=${this.stepTime}>
            <input  style="display:none" name="${this.name}"/>
        </cwms-datetime-inner>
        `;
  }
};

customElements.define("cwms-datetime-inner",CwmsDatetimeInner);
customElements.define("cwms-datetime",CwmsDatetimeWrapper);