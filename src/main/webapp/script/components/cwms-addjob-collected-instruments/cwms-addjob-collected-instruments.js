import {LitElement} from "../../thirdparty/lit-element/lit-element.js";
import {styling} from "./cwms-addjob-collected-instruments.css.js";
import {template} from "./cwms-addjob-collected-instruments.html.js";
import casadeStore from "../cwms-cascade-search/store/store.js";
import {tableStyles} from "../tools/styles/table.css.js";
import {textStyles} from "../tools/styles/text.css.js";
import {urlWithParamsGenerator} from "../../tools/UrlGenerator.js";

class CwmsAddjobCollectedInstruments extends LitElement {

    static get properties() {
        return {
            name: {type: String},
            coid: {type: Number},
            items: {type: Array}
        };
    }

    static get formAssociated() {
        return true;
    }

    #internals;

    constructor() {
        super();
        this.#internals = this.attachInternals();
        casadeStore.subscribe(() => {
            const {coid} = casadeStore.getState();
            this.companyUpdated(coid);
        });
    }

    companyUpdated(coid){
        if (coid === this.coid) return;
        this.coid = coid;
        fetch(urlWithParamsGenerator("collectedInstruments/getForCompany.josn", {coid}))
            .then(res => res.ok ? res.json() : Promise.reject(res.statusText))
            .then(items => this.updateItems(items));
    }

    static get styles() {
        return [tableStyles, textStyles, styling()]
    }

    updateItems(items) {
        this.items = items;
        const formData = items.filter(i => i.selected ?? false).reduce((fd, i) => {
            fd.append(this.name, i.id);
            return fd;
        }, new FormData());
        this.#internals.setFormValue(formData);
    }


    toggleSelectItem(item) {
        this.updateItems(this.items.map(i => i.id === item.id ?
            {...i, selected: !(i.selected ?? false)} : i));
    }

    toggleSelectAllItems(e) {
        this.updateItems(this.items.map(i => ({...i, selected: e.currentTarget.checked})));
    }


    render(){
        return template(this);
    }

}

customElements.define("cwms-addjob-collected-instruments", CwmsAddjobCollectedInstruments);
