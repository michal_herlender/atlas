import {css} from "../../thirdparty/lit-element/lit-element.js";

export const styling = context => css`

    :host {
        display: inline-flex;
        background-color: var(--cwms-table-strong-backgound, purple);
        width: 80%;
        place-content: center;
    }

    .no-items {
        background-color: white;
        min-height: 25px;
        font-family: arial, helvetica, sans-serif;
        font-size: 94%;
        display: flex;
        place-items:center;
        place-content: center;
        flex-grow: 2;
    }

    table {
        width: 100%;
    }

    table .COselect {
        width: 5%;
    }
`;