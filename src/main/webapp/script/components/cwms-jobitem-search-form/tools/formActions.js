
export const SHOW_INSTRUMENTS_ACTION = "Show_Instruments";
export const SHOW_MODELS_ACTION = "Show_Models";
export const SHOW_SIMILAR_MODELS_ACTION = "Show_Similar_Models";
export const SEARCH_ACTION = "Search";
export const GROUP_SEARCH_ACTION = "GroupSearch";
export const TP_SEARCH_ACTION = "TPSearch";