import { MetaDataObtainer } from "../../../tools/metadataObtainer.js";

const INSTRUMENTS_SEARCH = "INSTRUMENTS";
const COMPANY_MODELS_SEARCH = "COMPANY_MODELS_SEARCH";
const MATCHING_MODELS_SEARCH = "MATCHING_MODELS_SEARCH";

export class InstrumentObtainer {
    static async getPages(searchParams){
        searchParams.set("instrumentsPageNo", 0 );
        searchParams.set("companyModelsPageNo", 0 );
        searchParams.set("allModelsPageNo", 0 );
        const res = await fetch(this.prepareUrl(searchParams));
        return res.ok ? res.json() : Promise.reject(res.statusText);
    }  

 static prepareUrl(searchParams, URL_TYPE) {
    let url;
    switch(URL_TYPE){
      case MATCHING_MODELS_SEARCH:
        url =  new URL("jobitem/newJobItemSearchMatchingModels.json",MetaDataObtainer.base)
        break;
      case COMPANY_MODELS_SEARCH:
        url =  new URL("jobitem/newJobItemSearchCompanyModels.json",MetaDataObtainer.base)
        break;
      case INSTRUMENTS_SEARCH:
        url =  new URL("jobitem/newJobItemSearchInstruments.json",MetaDataObtainer.base)
        break;
      default:
        url = new URL("jobitem/newJobItemSearch.json",MetaDataObtainer.base);
    }
    url.search = searchParams;
    return url;
  }

  static async getMatchingModelsPage(searchParams,pageNuber) {
    searchParams.set("allModelsPageNo",pageNuber);
    return this.getPage(this.prepareUrl(searchParams,MATCHING_MODELS_SEARCH));
  }

  static async getCompanyModelsPage(searchParams,pageNuber) {
    searchParams.set("companyModelsPageNo",pageNuber);
    return this.getPage(this.prepareUrl(searchParams,COMPANY_MODELS_SEARCH));
  }

  static async getInstrumentPage(searchParams,pageNuber){
    searchParams.set("instrumentsPageNo",pageNuber);
    return this.getPage(this.prepareUrl(searchParams, INSTRUMENTS_SEARCH));
  }


  static async getPage(url){
    const res = await fetch(url);
    return res.ok ? res.json(): Promise.reject(res.statusText);
  }
}