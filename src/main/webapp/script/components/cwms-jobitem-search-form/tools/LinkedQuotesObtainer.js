import { MetaDataObtainer } from "../../../tools/metadataObtainer.js";

const QUOTED_ALL_SEARCH = "QUOTED_ALL_SEARCH";
const INSTRUMENTS_SEARCH = "INSTRUMENTS";
const COMPANY_MODELS_SEARCH = "COMPANY_MODELS_SEARCH";
const MATCHING_MODELS_SEARCH = "MATCHING_MODELS_SEARCH";

export class LinketQuotesObtainer {

  static prepareUrl(searchParams, URL_TYPE) {
    let url ;
    switch(URL_TYPE){
      case INSTRUMENTS_SEARCH:
          url =  new URL("jobitem/quotedInstruments.json",MetaDataObtainer.base);
          break;
      case MATCHING_MODELS_SEARCH:
        url =  new URL("jobitem/quotedMatchingModels.json",MetaDataObtainer.base);
        break;
      case COMPANY_MODELS_SEARCH:
        url =  new URL("jobitem/getCompanyModelsPage.json",MetaDataObtainer.base);
        break;
      case QUOTED_ALL_SEARCH:
      default:
          url =  new URL("jobitem/quotedAll.json",MetaDataObtainer.base);
    }
    url.search = searchParams;
    return url;
  }

  static async getInstrumentPage(searchParams,pageNuber){
    searchParams.set("instrumentsPageNo",pageNuber ?? 0);
    return this.getPage(this.prepareUrl(searchParams, INSTRUMENTS_SEARCH));
  }

  static async getMatchingModelsPage(searchParams,pageNuber){
    searchParams.set("allModelsPageNo",pageNuber ?? 0);
    return this.getPage(this.prepareUrl(searchParams, MATCHING_MODELS_SEARCH));
  }

  static async getCompanyModelsPage(searchParams,pageNuber){
    searchParams.set("companyModelsPageNo",pageNuber ?? 0);
    return this.getPage(this.prepareUrl(searchParams, COMPANY_MODELS_SEARCH));
  }


  static async getPageAll(searchParams){
    searchParams.set("instrumentsPageNo", 0);
    searchParams.set("companyModelsPageNo", 0);
    searchParams.set("allModelsPageNo", 0);
    return this.getPage(this.prepareUrl(searchParams, QUOTED_ALL_SEARCH));
  }


  static async getPage(url){
    const res = await fetch(url);
    return res.ok ? res.json(): Promise.reject(res.statusText);
  }
}