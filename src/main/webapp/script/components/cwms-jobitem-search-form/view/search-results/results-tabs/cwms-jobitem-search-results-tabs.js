
import { LitElement, html } from "../../../../../thirdparty/lit-element/lit-element.js";
import { addItemsToBasket, addItemToBasket, storeSearchAllModelsResultsPage, storeSearchCompanyModelResultsPage, storeSearchInstrumentsResultsPage, toggleItemInBasket } from "../../../store/actions.js";
import store from "../../../store/store.js";
import { SHOW_INSTRUMENTS_ACTION, SHOW_MODELS_ACTION, SHOW_SIMILAR_MODELS_ACTION } from "../../../tools/formActions.js";
import { InstrumentObtainer } from "../../../tools/InstrumentObtainer.js";
import { LinketQuotesObtainer } from "../../../tools/LinkedQuotesObtainer.js";
import { AbstractTabConfigurationObject } from "../results-tab/cwms-results-tab.js";
import { instrumentRowRenderer, instrumentsHeaderRenderer, modelHeaderRenderer,  modelRowRenderer, template } from "./cwms-jobitem-search-results-tabs.html.js";


class InstrumentsTabConfiguration extends AbstractTabConfigurationObject {

  constructor(page, basket){
    super();
    const plantIds = basket.map(i => i.plantId);
    this.currentPage = page.currentPage;
    this.pageCount = page.pageCount;
    this.results = page.results;
    this.resultsCount = page.resultsCount;
    this.headerRenderer = instrumentsHeaderRenderer;
    this.emptyRowRenderer = () => html`<tbody><tr><td class="bold center" colspan=99>
      <cwms-translate code="newjobitemsearch.string5">No Results</cwms-translate></td></tr></tbody>`;
    this.rowRenderer = instrumentRowRenderer({
      isInstrumentInBasket: data => plantIds.indexOf(data.plantId) > -1, 
      toggleInBasket:data => e => {
      const tbody = e.target.closest("tbody");
      const select = tbody?.querySelector?.("select");
      const quotationItemId = select?.value ?? 0;
      const  {plantId:basketId} = data;
      const basketItem = {...data,basketId,basketType:"instrument",quotationItemId}
      toggleItemInBasket(basketItem);
    }});
    CwmsTranslate.subscribeMessage("instruments","Instruments",msg => this.tableHeader = msg);
  }

  loadPage(pageNumber) {
    const { searchParams } = store.getState();
    let instrumentPage;
    switch(searchParams.get("action")){
      case SHOW_INSTRUMENTS_ACTION:
      case SHOW_MODELS_ACTION:
      case SHOW_SIMILAR_MODELS_ACTION:
        instrumentPage = LinketQuotesObtainer.getInstrumentPage(searchParams,pageNumber)
        break
      default:
        instrumentPage = InstrumentObtainer.getInstrumentPage(searchParams,pageNumber)

    }
    instrumentPage
      .then(storeSearchInstrumentsResultsPage)
      .catch(console.error);
  }
}

class CompanyModelsTabConfiguration extends AbstractTabConfigurationObject {

  constructor(page, basket = []){
    super();
    this.currentPage = page?.currentPage ?? 0;
    this.pageCount = page?.pageCount ?? 0;
    this.results = page?.results ?? [];
    this.resultsCount = page?.resultsCount ?? 0;
    this.headerRenderer = modelHeaderRenderer;
    this.emptyRowRenderer = () => html`<tbody><tr><td class="bold center" colspan=99>
      <cwms-translate code="newjobitemsearch.string10">No Results</cwms-translate></td></tr></tbody>`;
    this.rowRenderer = modelRowRenderer({addToBasket:data => e => {
      const tbody = e.target.closest("tbody");
      const select = tbody?.querySelector?.("select");
      const quotationItemId = select?.value ?? 0;
      const quantity = tbody.querySelector("input[name='quantity'")?.valueAsNumber ?? 1;
      const  {modelId:basketId} = data;
      const basketItem =  {...data,basketId,basketType:"model",quotationItemId}
      addItemsToBasket(Array(quantity).fill().map(_ => ({...basketItem})));
    },
    countInBasket: data => basket.filter(bi => bi.modelId == data.modelId).length
  });
    CwmsTranslate.subscribeMessage("newjobitemsearch.distmodels","Company models",msg => this.tableHeader = msg);
  }

  loadPage(pageNumber) {
    const { searchParams } = store.getState();
    let companyModelsPage;
    switch(searchParams.get("action")){
      case SHOW_INSTRUMENTS_ACTION:
      case SHOW_MODELS_ACTION:
      case SHOW_SIMILAR_MODELS_ACTION:
        companyModelsPage = LinketQuotesObtainer.getCompanyModelsPage(searchParams,pageNumber)
        break
      default:
        companyModelsPage = InstrumentObtainer.getCompanyModelsPage(searchParams,pageNumber)

    }
    companyModelsPage
      .then(storeSearchCompanyModelResultsPage)
      .catch(console.error);
  }

}

class MatchingModelsTabConfiguration extends CompanyModelsTabConfiguration {

  constructor(page,basket){
    super(page,basket);
    this.emptyRowRenderer = () => html`<tbody><tr><td class="bold center" colspan=99>
      <cwms-translate code="newjobitemsearch.string8">No Results</cwms-translate></td></tr></tbody>`;
    CwmsTranslate.subscribeMessage("models","Models",msg => this.tableHeader = msg);
  }

  loadPage(pageNumber) {
    const { searchParams } = store.getState();
    let matchingModelsPage;
    switch(searchParams.get("action")){
      case SHOW_INSTRUMENTS_ACTION:
      case SHOW_MODELS_ACTION:
      case SHOW_SIMILAR_MODELS_ACTION:
        matchingModelsPage = LinketQuotesObtainer.getMatchingModelsPage(searchParams,pageNumber)
        break
      default:
        matchingModelsPage = InstrumentObtainer.getMatchingModelsPage(searchParams,pageNumber)

    }
    matchingModelsPage
      .then(storeSearchAllModelsResultsPage)
      .catch(console.error);
  }
}

class CwmsJobitemSearchResultsTabs extends LitElement {


  static get properties(){
    return {
      instrumentsConfiguration:{type:Object,attributes:false},
      companyModelsConfiguration:{type:Object,attributes:false},
      matchingModelsConfiguration:{type:Object,attributes:false},
      instrumentTabName:{type:String,attributes:false},
      companyModelsTabName:{type:String,attributes:false},
      matchingModelsConfiguration:{type:Object,attributes:false}
    };
  }

  constructor() {
    super();
    store.subscribe(() => {
      this.state = store.getState() ?? {};
      const {basket=[],instruments={},companyModels={},matchingModels={}} = this.state;
      this.instrumentsConfiguration = new InstrumentsTabConfiguration(instruments, basket);
      this.companyModelsConfiguration = new CompanyModelsTabConfiguration(companyModels, basket);
      this.matchingModelsConfiguration = new MatchingModelsTabConfiguration(matchingModels, basket);
    });
    CwmsTranslate.subscribeMessage("instruments","Instruments",msg => this.instrumentTabName = msg);
    CwmsTranslate.subscribeMessage("newjobitemsearch.distmodels","Company models", msg => this.companyModelsTabName = msg);
    CwmsTranslate.subscribeMessage("newjobitemsearch.allmodels","All models", msg => this.matchingModelsTabName = msg);
  }


    render(){
       return template({
         instrumentTabName:this.instrumentTabName,
         companyModelsTabName:this.companyModelsTabName,
         matchingModelsTabName:this.matchingModelsTabName,
         instrumentsConfiguration:this.instrumentsConfiguration,
         companyModelsConfiguration:this.companyModelsConfiguration,
         matchingModelsConfiguration:this.matchingModelsConfiguration,
       });
    }

}


customElements.define("cwms-jobitem-search-results-tabs",CwmsJobitemSearchResultsTabs);

