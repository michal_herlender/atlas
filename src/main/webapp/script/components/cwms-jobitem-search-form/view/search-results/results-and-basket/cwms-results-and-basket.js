import { LitElement, html } from "../../../../../thirdparty/lit-element/lit-element.js";
import store from "../../../store/store.js";
import "../../../../cwms-dynamics-tabs/main.js";

class CwsmResultsAndBasket extends LitElement {
    
    static get properties(){
        return {
            instruments: {type:Object, attributes:false},
            companyModels: {type:Object, attributes:false},
            matchingModels : {type:Object, attributes:false},
        };
    }


    constructor(){
        super();
        store.subscribe(() => {
             ({instruments:this.instruments, companyModels:this.companyModels, matchingModels:this.matchingModels} = store.getState());
        })
    }

    get hasAnyResults(){
        return !!this.instruments  ||this.companyModels ||this.matchingModels;
    }

  /*  get hasAnyResults() {
        return !!this.instruments?.resultsCount || !!this.companyModels?.resultsCount || !!this.matchingModels?.resultsCount;
    }

*/
    render(){
     return  this.hasAnyResults ? html`
        <nav is="cwms-dynamic-subnav-holder">
            <cwms-dynamic-tab tabName="Search Results">
                <cwms-jobitem-search-results-tabs></cwms-jobitem-search-results-tabs>
                <cwms-new-jobitem-search-basket></cwms-new-jobitem-search-basket>
            </cwms-dynamic-tab>
            <cwms-dynamic-tab tabName="Basket">
                <cwms-new-jobitem-search-basket></cwms-new-jobitem-search-basket>
            </cwms-dynamic-tab>
        </nav>
       `:html``;
    }

}


customElements.define("cwms-results-and-basket",CwsmResultsAndBasket);