import { LitElement, html } from "../../../../../thirdparty/lit-element/lit-element.js";
import { linkStyling } from "../../../../tools/styles/links.css.js";
import { tableStyles } from "../../../../tools/styles/table.css.js";
import { textStyles } from "../../../../tools/styles/text.css.js";
import { styling } from "./cwms-results-tab.css.js";
import { pageSelector, template } from "./cwms-results-tab.html.js";

class CwmsResultsTab extends LitElement {

    static get properties(){
        return {
            configuration:{type:Object,attributes:false},
            displayInPagination: { type: String },
            paginationOf: { type: String },
            paginationWithResults: { type: String },
            state: { type: Object },
        };
    }


    static get styles(){
        return [styling({}),linkStyling(),tableStyles, textStyles];
    }

  constructor() {
    super();
    ///DEBUG ONLY
    window.store = store;
    ///DEBUG ONLY ^^^^
    store.subscribe(() => {
      this.state = store.getState();
    });

    CwmsTranslate.subscribeMessage(
      "pagination.displayinpage",
      "Displaying page",
      (m) => (this.displayInPagination = m)
    );
    CwmsTranslate.subscribeMessage(
      "pagination.of",
      "of",
      (m) => (this.paginationOf = m)
    );
    CwmsTranslate.subscribeMessage(
      "pagination.withnbresults",
      "with {0} results",
      (m) => (this.paginationWithResults = m)
    );
  }

  get paginationInfo() {
    const withResults = this.paginationWithResults.replace(
      "{0}",
      this.configuration?.resultsCount ?? 0
    );
    return html`${this.displayInPagination}
    ${pageSelector({ pageCount:this.configuration?.pageCount, 
        currentPage:this.configuration?.currentPage ?? 0,
        loadPage: this.configuration?.loadPage?.bind?.(this) })}
    ${this.paginationOf} ${this.configuration?.pageCount ?? 0} ${withResults}`;
  }
  
  loadPageRelativeToCurrent(move) {
      this.configuration.loadPageRelativeToCurrent(move);
  }

    render(){
       return template({
      paginationInfo: this.paginationInfo,
      configuration:this.configuration,
      loadPage:  this.configuration?.loadPage,
      loadNextPage: (_) => this.loadPageRelativeToCurrent(1),
      loadPrevPage: (_) => this.loadPageRelativeToCurrent(-1),
      previousPage: this.configuration?.hasPreviousPage ?? false,
      nextPage: this.configuration?.hasNextPage ?? false,
       });
    }
}


customElements.define("cwms-results-tab",CwmsResultsTab);


export class AbstractTabConfigurationObject {
    constructor(){
        this.currentPage = 0;
        this.pageCount = 0;
        this.results = 0;
        this.resultsCount =0
        this.rowRenderer = (data) => "row";
        this.emptyRowRenderer = () =>  html`<tbody><tr><td class="bold center" colspan=99>No reuslts</td></tr></tbody>`
        
        this.tableSummary = "this is a table summary";
        this.tableHeader = "results"
    }

    get hasPreviousPage(){
        return this.currentPage > 1;
    }

    get hasNextPage(){
        return this.currentPage < this.pageCount;
    }

    loadPage(pageNumber){
        throw  "Not implemented";
    }

  loadPageRelativeToCurrent(move){
    this.loadPage(this.currentPage + move);
  }


}