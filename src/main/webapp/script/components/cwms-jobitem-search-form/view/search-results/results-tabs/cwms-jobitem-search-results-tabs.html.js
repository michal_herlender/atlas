import { html } from '../../../../../thirdparty/lit-element/lit-element.js'
import {ifDefined} from "../../../../../thirdparty/lit-html/directives/if-defined.js"
import { instrumentModelLinkGenerator } from '../../../../../tools/UrlGenerator.js';
import "../../../../cwms-dynamics-tabs/main.js";
import "../../../../cwms-toggle-icons/cwms-toggle-icons.js";
import "../../../../cwms-tooltips/cwms-instrument-link-and-tooltip/cwms-instrument-link-and-tooltip.js";
import "../../../../cwms-tooltips/cwms-company-link-and-tooltip/cwms-company-link-and-tooltip.js";
import "../results-tab/cwms-results-tab.js"

export const template = (context) => html`
    <nav is="cwms-dynamic-tabmenu-holder"  .switchOnlyOnClick=${true}>
        <cwms-dynamic-tab tabName="instruments" tabLabel=${`${context.instrumentTabName} (${context.instrumentsConfiguration?.resultsCount ?? 0})`}>
            
        <cwms-results-tab .configuration=${context.instrumentsConfiguration}></cwms-results-tab>
    </cwms-dynamic-tab>
        <cwms-dynamic-tab tabName="companyModels" tabLabel=${`${context.companyModelsTabName} (${context.companyModelsConfiguration?.resultsCount ?? 0})`}>
    <cwms-results-tab .configuration=${context.companyModelsConfiguration}></cwms-results-tab>
    </cwms-dynamic-tab>
        <cwms-dynamic-tab tabName="allModels" tabLabel=${`${context.matchingModelsTabName} (${context.matchingModelsConfiguration?.resultsCount ?? 0})`}>
    <cwms-results-tab .configuration=${context.matchingModelsConfiguration}></cwms-results-tab>
    </cwms-dynamic-tab>
</nav>
`;


export const instrumentsHeaderRenderer = (context) => html`
    <th><cwms-translate code="barcode">Barcode</cwms-translate></th>
    <th><cwms-translate code="formerbarcode">Former barcode</cwms-translate></th>
    <th><cwms-translate code="instmodelname">Instrument model name</cwms-translate></th>
    <th><cwms-translate code="serialno">Serial no</cwms-translate></th>
    <th><cwms-translate code="plantno">Plant no</cwms-translate></th>
    <th><cwms-translate code="company">Company</cwms-translate></th>
    <th><cwms-translate code="editinstr.defaultservicetype">Default Service Type</cwms-translate></th>
    <th class="nji_add"><cwms-translate code="add">Add</cwms-translate></th>
`;


export const modelHeaderRenderer = context => html`
    <th class="nji_desc"><cwms-translate code="instmodelname">Instrument Model Name</cwms-translate></th>
    <th><cwms-translate code="company">Company</cwms-translate></th>
    <th class="nji_qty"><cwms-translate code="newjobitemsearch.quantity">Quantity</cwms-translate></th>
    <th class="nji_inbasket"><cwms-translate code="newjobitemsearch.inbasket">In basket</cwms-translate></th>
    <th class="nji_add"><cwms-translate code="Add">Add</cwms-translate></th>
`


export const modelRowRenderer = (context) => (data) => html`<tbody><tr>
    <td >${modelLinkTemplate(data)}</td>
    <td>
        ${data.companyId?html`
          <cwms-company-link-and-tooltip class="mainlink"
            companyId=${data.companyId}
            companyName=${data.companyName}
            >${data.companyName}</cwms-company-link-and-tooltip
          >`:html``}
    </td>
    <td class="center"><input name="quantity" type="number" min=1 max=20 value=1></td>
    <td class="center">${context?.countInBasket?.(data) ?? 0}</td>
    <td class="center"><cwms-toggle-icons @click=${context.addToBasket(data)}
    .icons=${["img/icons/add.png"]}
></cwms-toggle-icons></td>
</tr>
    ${data.availableQuotations ?html`
    <tr>
        <td colspan=9>
            <cwms-translate code="quotationitem" >Quotation Item</cwms-translate>:
            <select id="qitem_inst${data.modelId}"  name="instQItemIds[${data.modelId}]" >
            <option value="0"><cwms-translate code="notlinked">Not linked</cwms-translate></option>
            ${renderQuoteOptions(data)}
        </select>
            </td></tr>
    `:html``}
</tbody>
`

const modelLinkTemplate = (data) => html`
<a class="mainlink" target="_blank" href=${instrumentModelLinkGenerator(data.modelId).toString()}>${data.modelName}</a>
`;

export const instrumentRowRenderer = (context) => (data) => html`<tbody>
    <tr class=${ifDefined(data.status == "BER"?"ber":undefined)}>
    <td><cwms-instrument-link-and-tooltip plantid=${data.plantId} ></cwms-instrument-link-and-tooltip></td>
    <td>${data.formerBarcode}</td>
    <td>${modelNameRendererTemplate(data)}</td>
    <td>${data.serialNo}</td>
    <td>${data.plantNo}</td>
    <td>
          <cwms-company-link-and-tooltip class="mainlink"
            companyId=${data.companyId}
            companyName=${data.companyName}
            >${data.companyName}</cwms-company-link-and-tooltip
          >
    </td>
    <td>${data.defaultServiceType}</td>
    <td class="center">${toggleInBasketTemplate(context)(data)}</td></tr>
    ${data.availableQuotations ?html`
    <tr>
        <td colspan=8>
            <cwms-translate code="quotationitem" >Quotation Item</cwms-translate>:
            <select id="qitem_inst${data.plantId}"  name="instQItemIds[${data.plantId}]" >
            <option value="0"><cwms-translate code="notlinked">Not linked</cwms-translate></option>
            ${renderQuoteOptions(data)}
        </select>
            </td></tr>
    `:html``}
    </tbody>
`;

const renderQuoteOptions = data => {
    const entries = Object.entries(data.availableQuotations);
    return entries.map(([k,v],idx) => html`<option ?selected=${idx==0} value=${k}>${v}</option>`)
}

export const modelNameRendererTemplate = data => html`
    <a href="${instrumentModelLinkGenerator(data.modelId).toString()}" target="_blank" class="mainlink">${data.modelName}</a>
    ${data.customerDescription?html`<br/><span class="results-annotation">${data.customerDescription}</span>`:html``}
`;


const toggleInBasketTemplate = context =>  data => {
  if( data.isOnCurrentJob) return html`
<cwms-translate code="newjobitemsearch.onjob">On Job</cwms-translate>`;
  if (data.isOnJob) return  html`
<cwms-translate code="newjobitemsearch.onanother">On Another Job</cwms-translate>
<cwms-translate code="job"></cwms-translate>
`;
return html`<cwms-toggle-icons @click=${context.toggleInBasket(data)}
    .icons=${context.isInstrumentInBasket(data)?["img/icons/basket_remove.png"]
 :   ["img/icons/add.png"]
}
></cwms-toggle-icons>`
}; 