import { html } from "../../../../../thirdparty/lit-element/lit-element.js";
import { stylesMixin } from "../../../../tools/applicationStylesMixinTemplate.js";

export const template = (context) => html`
  ${paginationInfo(context)}
  <table
    class="default2 cwms-table "
    summary=${context.configuration?.tableSummary ?? ""}
  >
    <thead>
      <tr>
        <td colspan="17">
          ${context.configuration?.tableHeader ?? "results"} (${context.configuration?.resultsCount ?? 0})
        </td>
      </tr>
      <tr>
        ${context.configuration?.headerRenderer?.({})}
      </tr>
      </thead>
        ${context.configuration?.resultsCount ?  
        context.configuration?.results?.map?.(r => html`${context.configuration.rowRenderer(r)}`)
      : context.configuration?.emptyRowRenderer() ?? "no results"
      }
     </table>
`;

const paginationInfo = context => html`
  <div class="page-navigation">
    <button @click=${context.loadPrevPage} .disabled=${!context.previousPage} ><cwms-translate code="pagination.prev">prev</cwms-translate></button>
    <div class="pagination-info">
        ${context.paginationInfo}
    </div>
    <button @click=${context.loadNextPage} .disabled=${!context.nextPage}><cwms-translate code="pagination.next">next</cwms-translate></button>
  </div>
`

export const pageSelector = context => html`
<select @change=${e => context.loadPage(e.currentTarget.value)}>${Array.from(Array(context.pageCount),(_,i)=>i+1).map(i =>html`
            <option value=${i} .selected=${i==context.currentPage}>${i}</option>`)}
</select>`;