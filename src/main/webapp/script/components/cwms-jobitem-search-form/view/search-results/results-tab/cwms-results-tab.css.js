import { css } from '../../../../../thirdparty/lit-element/lit-element.js'

export const styling = (context) => css`
    .page-navigation {
        display:grid;
        grid-template-rows: 1fr;
        grid-template-columns: 50px 1fr 50px;
        grid-auto-flow:column;
        grid-gap:2rem;

    }

    .pagination-info {
        text-align: center;
    }

    .ber {
        --cwms-main-link-color: var(--cwms-link-gray, #777);
        --cwms-main-link-color-hovered: var(--cwms-link-gray, #777);
        --cwms-main-link-text-decoration: line-through;
    }
`;