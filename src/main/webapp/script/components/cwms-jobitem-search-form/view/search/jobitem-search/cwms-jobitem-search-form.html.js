import { html } from '../../../../../thirdparty/lit-element/lit-element.js'
import { stylesMixin } from '../../../../tools/applicationStylesMixinTemplate.js'
import "../../../../search-plugins/cwms-company-search/cwms-company-search.js"
import "../../../../search-plugins/cwms-contact-search/cwms-contact-search.js"
import "../../../../search-plugins/cwms-description-search/cwms-description-search.js"
import "../../../../search-plugins/cwms-mfr-search/cwms-mfr-search.js"
import "../../../../search-plugins/cwms-domain-search/cwms-domain-search.js"
import "../../../../search-plugins/cwms-family-search/cwms-family-search.js"
import "../../../../search-plugins/cwms-salescategory-search/cwms-salescategory-search.js"
import "../../../../cwms-translate/cwms-translate.js"
import { GROUP_SEARCH_ACTION, SEARCH_ACTION, TP_SEARCH_ACTION } from '../../../tools/formActions.js'

export const template = (context) => html`
			${stylesMixin}
			<form @submit=${context.submitSearch}>
			<div class="container">
			<div class="column column-left">
									
				<div class="row" >
					<label><cwms-translate code="domain">Domain</cwms-translate>:</label>
                        <cwms-domain-search @selected=${e => context.onFormChange("domainId",e.detail?.id)}> Domain search</cwms-domain-search>
				</div>
				<div class="row" >
					<label for="family"><cwms-translate code="family">Family</cwms-translate>:</label>												
                    <cwms-family-search @selected=${e => context.onFormChange("familyId",e.detail?.id)}>Family Search</cwms-family-search>
                </div>											
				<div  class="row" id="descrip">
                       <label><cwms-translate code="sub-family">Sub-Family</cwms-translate>:</label>
                        <!-- float div left -->
                        <cwms-description-search domaintype="INSTRUMENTMODEL" @selected=${e => context.onFormChange("descId",e.detail?.key)}>description search</cwms-description-search>
		          </div>   
				<div class="row" >
					<label for="manufacturer"><cwms-translate code="manufacturer">Brand</cwms-translate>:</label>												
                        <cwms-mfr-search  @selected=${e => context.onFormChange("mfrId",e.detail?.id)}>manufacturer search</cwms-mfr-search>
                </div>
					<div class="row">
						<label><cwms-translate code="model">Model</cwms-translate>:</label>
							   <input id="model" name="model" tabindex="7" type="text" value=""></div>
																													
					<div class="row">
						<label><cwms-translate code="salescategory">Sales Category</cwms-translate>:</label>
							   
                        <cwms-salescategory-search  @selected=${e => context.onFormChange("salesCatId",e.detail?.id)}>Sales category search</cwms-salescategory-search>
</div>
					<div class="row">
						<label><cwms-translate code="newjobitemsearch.resperpage">Results per page</cwms-translate>:</label>
<select id="resultsPerPage" name="resultsPerPage"><option value="5">5</option><option value="10">10</option><option value="20" .selected=${true}>20</option><option value="50">50</option><option value="100">100</option></select>
					</div>
					<div class="row">
						<label><cwms-translate code="newjobitemsearch.ordresby">Order results by</cwms-translate>:</label>
							   <select id="orderBy" name="orderBy"><option value="SERIAL_NO" .selected=${true} >Serial No</option>
										<option value="BARCODE" >Barcode</option>
										<option value="PLANT_NO" >Plant No</option>
										</select></div>
									
									<div class="row">
									<label></label>
		<button  type="reset"  tabindex="0" @click=${context.clearForm}><cwms-translate code="clear">Clear form</cwms-translate></button>
		<button type="submit" name="action" value="${SEARCH_ACTION}"><cwms-translate code="search">Search</cwms-translate></button>
		<button type="submit" name="action" value="${GROUP_SEARCH_ACTION}"><cwms-translate code="newjobitemsearch.searchgroup">Search in Group</cwms-translate></button>
		<button type="submit" name="action" value="${TP_SEARCH_ACTION}"><cwms-translate code="newjobitemsearch.searcheverywhere">Search everywhere</cwms-translate></button>
		</div>
			</div>
			<div class="column column-right">
			
					<div class="row">
						<label for="barcode"><cwms-translate code="barcode">Barcode</cwms-translate>:</label>												
						<input type="text" name="barcode" id="plantid" value="" tabindex="13">
						</div>
					<div class="row">
						<label for="formerPlantId"><cwms-translate code="formerbarcode">Former Barcode</cwms-translate>:</label>												
						<input type="text" name="formerBarcode" id="formerbarcode" value="" tabindex="13">
						</div>
					<div class="row">
						<label for="serialNo"><cwms-translate code="serialno">Serial No</cwms-translate>:</label>												
						<input type="text" name="serialNo" id="serialno" value="" tabindex="13">
						</div>
					<div class="row">
						<label for="plantNo"><cwms-translate code="plantno">Plant No</cwms-translate>:</label>												
						<input type="text" name="plantNo" id="plantno" value="" tabindex="13">
						</div>
					<div class="row">
						<label for="customerDescription"><cwms-translate code="viewinstrument.customerdescription">Customer description</cwms-translate>:</label>												
						<input type="text" name="customerDescription" id="customerDescription" value="" tabindex="14">
						</div>
			<!-- end of right column -->
		
		</div>
		</div>
		</form>
		
`;


