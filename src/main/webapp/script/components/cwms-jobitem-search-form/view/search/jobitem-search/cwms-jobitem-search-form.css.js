import { css } from "../../../../../thirdparty/lit-element/lit-element.js";

export const styling = (context) => css`
  cwms-domain-search,
  cwms-salescategory-search,
  cwms-family-search,
  cwms-company-search,
  cwms-contact-search,
  cwms-description-search,
  cwms-mfr-search,
  input {
    width: 280px;
  }

  .row input[type="submit"] {
    width: 51px;
  }
  .row input[type="date"] {
    width: 211px;
  }

  .row .label {
    width: 68px;
    margin-right: 5px;
    text-align: center;
  }

  .buttons {
    padding-left: 1rem;
  }

  div.container {
    display: grid;
    grid-template-columns: 5fr 5fr;
    grid-template-rows: 1fr;
    grid-auto-flow: column;
    gap: 1rem;
  }

  div.column {
    display: flex;
    flex-direction: column;
  }

  div.row {
    padding-top: 5px;
    padding-bottom: 5px;
    border-bottom: 1px dotted #666;
    border-top: 0;
    display: flex;
    flex-direction: row;
  }

  .row .mfr {
    display: flex;
  }
`;
