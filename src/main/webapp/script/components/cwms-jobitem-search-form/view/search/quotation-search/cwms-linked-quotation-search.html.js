import { html } from "../../../../../thirdparty/lit-element/lit-element.js";
import "../../../../cwms-translate/cwms-translate.js";
import { stylesMixin } from "../../../../tools/applicationStylesMixinTemplate.js";
import { SHOW_INSTRUMENTS_ACTION, SHOW_MODELS_ACTION, SHOW_SIMILAR_MODELS_ACTION } from "../../../tools/formActions.js";

export const template = (context) => !!context?.linkedQuotes && context?.linkedQuotes?.length? html`
			${stylesMixin}
      <form @submit=${context.submitSearch}>
  <fieldset>
    <legend>
      <cwms-translate code="newjobitemsearch.searchlinkquot">
        Search Linked Quotations
      </cwms-translate>
    </legend>
    <ol>
      <li><div class="search-linked"><div>
        <label for="jobQuoteLinkId">
          <cwms-translate code="newjobitemsearch.searchquot">
            Search Quotation
          </cwms-translate>
          :
        </label>
        <select id="jobQuoteLinkId" name="jobQuoteLinkId">
            <option value=""><cwms-translate code="newjobitemsearch.alllinkquots">All Linked Quotations</cwms-translate></option>
            ${context.linkedQuotes.map(lq => html`
                <option value="${lq.id}">${lq.label}</option>
            `)

            }
        </select>
        <button type="submit" name="action" value="${SHOW_INSTRUMENTS_ACTION}" >
          <cwms-translate code="newjobitemsearch.showquotedinstruments">Show Quoted Instruments</cwms-translate>
        </button>
        <button type="submit" name="action" value="${SHOW_MODELS_ACTION}" >
          <cwms-translate code="newjobitemsearch.showmodels">Show Instrument Models</cwms-translate>
        </button>
        <button type="submit" name="action" value="${SHOW_SIMILAR_MODELS_ACTION}">
          <cwms-translate code="newjobitemsearch.showsimilarmodels">Show Similar Instrument Models</cwms-translate>
        </button></div>
				<div>		<label><cwms-translate code="newjobitemsearch.resperpage">Results per page</cwms-translate>:</label>
<select id="resultsPerPage" name="resultsPerPage"><option value="5">5</option><option value="10">10</option><option value="20" .selected=${true}>20</option><option value="50">50</option><option value="100">100</option></select></div>
</div>
      </li>
    </ol>
  </fieldset></form>
`:html``;
