import { LitElement } from "../../../../../thirdparty/lit-element/lit-element.js";
import { storeResetState, storeNewJobItemSearchParams, storeSearchResultsPage, storeSearchInstrumentsResultsPage } from "../../../store/actions.js";
import { LinketQuotesObtainer } from "../../../tools/LinkedQuotesObtainer.js";
import { styling } from "./cwms-linked-quotation-search.css.js";
import { template } from "./cwms-linked-quotation-search.html.js";

class CwmsLinkedQuotationSearch extends LitElement {

    static get properties(){
        return {
            linkedQuotes:{type:Array},
            companyId:{type:Number},
            jobId:{type:Number},
            formData:{type:Object},
        };
    }

    static get styles(){
        return styling({});
    }

  connectedCallback(){
    super.connectedCallback();
    this.formData = { jobId:this.jobId ?? 0, companyId:this.companyId};
  }

    submitSearch(event){
        event.preventDefault();
    storeResetState();
    const searchParams = new URLSearchParams(new FormData(event.target));
    searchParams.append(event.submitter.name,event.submitter.value);
    Object.entries(this.formData).forEach((e) =>
      searchParams.append(e[0], e[1])
    );
    storeNewJobItemSearchParams(searchParams);
    LinketQuotesObtainer.getPageAll(searchParams)
      .then((res) => {
        storeSearchResultsPage(res);
      })
      .catch(console.error);
    }

    render(){
       return template({
        submitSearch:this.submitSearch.bind(this),
        linkedQuotes:this.linkedQuotes

       });
    }

}


customElements.define("cwms-linked-quotation-search",CwmsLinkedQuotationSearch);