import { LitElement } from "../../../../../thirdparty/lit-element/lit-element.js";
import { template } from "./cwms-jobitem-search-form.html.js";
import { styling } from "./cwms-jobitem-search-form.css.js";
import {
  storeSearchResultsPage,
  storeResetState,
  storeNewJobItemSearchParams,
} from "../../../store/actions.js";
import { InstrumentObtainer } from "../../../tools/InstrumentObtainer.js";
import store from "../../../store/store.js";

class CwmsJobitemSearchForm extends LitElement {
  static get properties() {
    return {
      formData: { type: Object, attributes: false },
      searchBrandByText: { type: Boolean },
      coid:{type:Number},
      jobId:{type:Number},
      groupId:{type:Number},
    };
  }

  constructor() {
    super();
    ///DEBUG ONLY
    window.store = store;
    ///DEBUG ONLY ^^^^
  }

  connectedCallback(){
    super.connectedCallback();
    this.formData = {coid:this.coid ?? 0, groupId:this.groupId ?? 0, jobId:this.jobId ?? 0};
  }

  static get styles() {
    return styling({});
  }

  submitSearch(event) {
    event.preventDefault();
    storeResetState();
    const searchParams = new URLSearchParams(new FormData(event.target));
    searchParams.append(event.submitter.name,event.submitter.value);
    Object.entries(this.formData).forEach((e) =>
    {  if(e[1]) searchParams.append(e[0], e[1]) }
    );
    storeNewJobItemSearchParams(searchParams);
    InstrumentObtainer.getPages(searchParams)
      .then((res) => {
        storeSearchResultsPage(res);
      })
      .catch(console.error);
  }

  updateFormData(field, value) {
    this.formData = { ...this.formData, [field]: value };
  }


  clearForm(e){
    ["cwms-family-search","cwms-domain-search","cwms-description-search",
  "cwms-mfr-search", "cwms-salescategory-search "].map(s => this.shadowRoot.querySelector(s))
  .forEach(e => e.clear());
  }

  render() {
    return template({
      onFormChange: this.updateFormData.bind(this),
      submitSearch: this.submitSearch,
      clearForm:this.clearForm.bind(this),
      showGroupSearch: !!this.groupId
    });
  }
}

customElements.define("cwms-jobitem-search-form", CwmsJobitemSearchForm);
