import { css } from "../../../../thirdparty/lit-element/lit-element.js";

export const styling = css `

    .ber {
        --cwms-main-link-color: var(--cwms-link-gray, #777);
        --cwms-main-link-color-hovered: var(--cwms-link-gray, #777);
        --cwms-main-link-text-decoration: line-through;
    }

    .ber .plant-id {
        text-decoration: var(--cwms-main-link-text-decoration, line-through);
    }
`
