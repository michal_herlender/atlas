import {html, LitElement} from "../../../../thirdparty/lit-element/lit-element.js";
import {linkStyling} from "../../../tools/styles/links.css.js";
import {tableStyles} from "../../../tools/styles/table.css.js";
import {textStyles} from "../../../tools/styles/text.css.js";
import store from "../../store/store.js";
import {styling} from "./cwms-new-jobitem-search-basket.css.js";
import {renderBasketForm, template} from "./cwms-new-jobitem-search-basket.html.js";


class CwmsNewJobItemSearchBasket extends LitElement {

    static get properties() {
        return {
            addStraightToJob: {type: Boolean, attributes: false},
            basket: {type: Array, attributes: false},
            removeAllTitle: {type: String, attributes: false},
        };
    }

    static get styles(){
        return [styling,linkStyling(),tableStyles, textStyles];
    }


    constructor(){
        super();
        store.subscribe(() => {
            const {basket=[]} = store.getState();
            this.basket = basket;
        });
        CwmsTranslate.subscribeMessage("newjobitemsearch.string11","Remove all items from the basket",m => this.removeAllTitle=m);
    }

    render(){
       return template({
           basket:this.basket,removeAllTitle:this.removeAllTitle,
       });
    }

}



class CwmsNewJobItemSearchBasketWrapper extends LitElement {

    static get properties(){
        return {
            addStraightToJob: {type: Boolean, attributes: false},
            showSpinner: {type: Boolean, attributes: false},
            basket: {type: Array, attributes: false},
            bookingAddressId: {type: Number, attributes: false},
            removeAllTitle: {type: String, attributes: false},
        };
    }

    constructor() {
        super();
        this.showSpinner = false;
        this.addStraightToJob = true;
        store.subscribe(() => {
            const {basket=[], bookingAddressId=0} = store.getState();
            this.basket = basket;
            this.bookingAddressId = bookingAddressId;
        });
    }


    createRenderRoot(){
        return this;
    }

    onFormSubmit(e){
        window.showUnLoadWarning = false;
        queueMicrotask(() => this.showSpinner = true);
    }

    render(){
        return html`<cwms-new-jobitem-search-basket-inner></cwms-new-jobitem-search-basket-inner>
        ${renderBasketForm(this)}
        `
    }

}

customElements.define("cwms-new-jobitem-search-basket",CwmsNewJobItemSearchBasketWrapper);
customElements.define("cwms-new-jobitem-search-basket-inner",CwmsNewJobItemSearchBasket);