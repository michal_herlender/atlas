import {html} from "../../../../thirdparty/lit-element/lit-element.js";
import {ifDefined} from "../../../../thirdparty/lit-html/directives/if-defined.js"
import {MetaDataObtainer} from "../../../../tools/metadataObtainer.js";
import {stylesMixin} from "../../../tools/applicationStylesMixinTemplate.js";
import {clearBasket, removeItemFromBasket} from "../../store/actions.js";
import {modelNameRendererTemplate} from "../search-results/results-tabs/cwms-jobitem-search-results-tabs.html.js"
import "../../../cwms-spinner/cwms-spinner.js";

export const template = (context) => html`
    <div class="infobox">
        <h5>
            <cwms-translate code="newjobitemsearch.jobitembasket">Job Item Basket</cwms-translate>
        </h5>
        <a role="button" class="mainlink mainlink-float" @click=${e => clearBasket()} id="removelink"
           title=${context.removeAllTitle}>
            <cwms-translate code="newjobitemsearch.string12">Remove all Items</cwms-translate>
        </a>
        <div class="clear"></div>
        <table class="default2" id="basketitemstable"><thead>
            <tr>
                <td colspan="7">
                    <cwms-translate code="newjobitemsearch.jobitembasket">Job Item Basket</cwms-translate>
                    (<span class="basketCount">${context.basket.length}</span>)
                </td>
            </tr>
            <tr>
                <th class="njib_barcode" scope="col"><cwms-translate code="barcode">Barcode</cwms-translate></th>
                <th class="njib_barcode" scope="col"><cwms-translate code="formerbarcode">Former Barcode</cwms-translate></th>
                <th class="njib_instmod" scope="col"><cwms-translate code="newjobitemsearch.instrmodel">Instrument/Model</cwms-translate></th>
                <th class="njib_serial" scope="col"><cwms-translate code="serialno">Serial No</cwms-translate></th>
                <th class="njib_plant" scope="col"><cwms-translate code="plantno">Plant No</cwms-translate></th>
                <th class="njib_remove" scope="col"><cwms-translate code="remove">Remove</cwms-translate></th>
            </tr>
        </thead><tfoot>
            <tr><td colspan=12>&nbsp;</td></tr>
        </tfoot><tbody>
            ${context.basket.length == 0? html`
                <tr>
                    <td colspan="7" class="bold center">
                        <cwms-translate code="newjobitemsearch.string14">Your Job Item Basket Is Empty</cwms-translate>
                    </td>
                </tr>
            `:context.basket.map(renderBasketRow)}
        </tbody></table>
    </div>

`;

const renderBasketRow = basketItem => html`<tr class=${ifDefined(basketItem.status == "BER"?"ber":undefined)}>
    <td class="plant-id">${basketItem.plantId ?? "-" }</td>
    <td>${basketItem.formerBarcode && basketItem.formerBarcode != "" ?basketItem.formerBarcode: "-" }</td>
    <td>${modelNameRendererTemplate(basketItem)}
    ${basketItem.quotationItemId? html`
    <br/>
    ${basketItem.availableQuotations?.[basketItem.quotationItemId]}
    `:html``}</td>
    <td>${basketItem.serialNo ?? "-" }</td>
    <td>${basketItem.plantNo ?? "-" }</td>
    <td class="center"><cwms-toggle-icons @click=${e => removeItemFromBasket(basketItem)}
    .icons=${["img/icons/delete.png"]}
></cwms-toggle-icons></td>
</tr>`


export const renderBasketForm = context => html`
    ${stylesMixin}
    ${context.showSpinner ? html`
        <cwms-spinner></cwms-spinner>` : html``}
    <form method="POST" @submit=${context.onFormSubmit}>
        <input name="_csrf" type="hidden" value="${MetaDataObtainer.csrfToken}">
        <input name="contractReviewItems" type="hidden" value="false">
        <input type="hidden" name="bookingInAddrId" value="${context.bookingAddressId}"/>
        ${context.basket.map(renderBasketItemForm)}
        ${renderButtons(context)(context.basket)}
    </form>
`;

const renderButtons = context =>  basket => {
    if (!basket || basket.length == 0)
        return html``;
    const modelCnt = basket.filter(bi => bi.basketType =="model").length;
    const instrumentCnt = basket.length -modelCnt;
    return html`
        <div class="center">
            <button type="submit"  name="action" value="Add Items" >
                <cwms-translate code="addnewitemstojob.additems">Add Items</cwms-translate></button>
        ${modelCnt == 0 ? html`    
            
            <button type="submit" name="action" value="Add Straight to Job" 
        ?disabled=${!context.addStraightToJob} >
                <cwms-translate code="newjobitemsearch.string16">Add Straight To Job</cwms-translate>
            </button>`:html``}
        </div>
        ${modelCnt != 0 && instrumentCnt != 0? html`
						<p class="center attention">
							<span>
								<cwms-translate code="newjobitemsearch.string15">
                                    Note: Adding items to the job whilst there is a mixture of instruments and models in the basket will mess up the ordering of the new job items
                                </cwms-translate>
							</span>
						</p>`:html``}
    ` 
}




const renderBasketItemForm = (item,idx) => html`
    <input type="hidden" name="${`basketTypes[${idx}]`}" value="${item.basketType}"/>
    <input type="hidden" name="${`basketIds[${idx}]`}" value="${item.basketId}"/>
    <input type="hidden" name="${`basketQuotationItemIds[${idx}]`}" value="${item.quotationItemId ?? 0}"/>
`;