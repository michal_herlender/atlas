import {
  STORE_SEARCH_RESULTS_PAGE,
  STORE_SEARCH_RESULTS_CURRENT_PAGE_NUMBER,
  STORE_SEARCH_RESULTS_PAGE_COUNT,
  STORE_SEARCH_RESET_SEARCH,
  STORE_NEW_JOBITEM_SEARCH_PARAMS,
  STORE_CLEAR_BASKET,
  STORE_ADD_ITEM_TO_BASKET,
  SOTRE_REMOVE_FROM_BASKET,
  STORE_ADD_ITEMS_TO_BASKET,
  STORE_BOOKING_IN_ADDRESS,
} from "./actionsTypes.js";

let dispatch;
export default dispatch = function (state, commmand) {
  const {basket = [], bookingAddressId = 0} = state;
  switch (commmand.type) {
    case STORE_SEARCH_RESET_SEARCH:
      return {basket, bookingAddressId };
    case STORE_SEARCH_RESULTS_PAGE:
      return {
        ...state,
        ...commmand.page
      };
    case STORE_SEARCH_RESULTS_PAGE_COUNT:
      return { ...state, pageCount: commmand.pageCount };
    case STORE_SEARCH_RESULTS_CURRENT_PAGE_NUMBER:
      return { ...state, currentPageNumber: commmand.currentPageNumber };
    case STORE_NEW_JOBITEM_SEARCH_PARAMS:
      return {...state, searchParams:commmand.searchParams};
    case STORE_CLEAR_BASKET:
      return {...state, basket:[]};
    case STORE_ADD_ITEMS_TO_BASKET:
      return {...state, basket:[...basket,...uniqueBasketItems(commmand.items,basket)]};
    case STORE_ADD_ITEM_TO_BASKET:
      return {...state, basket:[...basket,{...commmand.item, basketNo:`${basket.length+1}-${Date.now()}`}]};
    case SOTRE_REMOVE_FROM_BASKET:
      let {basketNo } = commmand.item;
      if(!basketNo)
        basketNo = basket.find(bi => commmand.item.basketId).basketNo;

      return {...state, basket:basket.filter(i => i.basketNo != basketNo)}
    case STORE_BOOKING_IN_ADDRESS:
      return {...state,bookingAddressId:commmand.bookingAddressId}
    default:
      return state;
  }
};

function uniqueBasketItems(items,basket){
  const size = basket?.length ?? 0;
  return items.map((i,idx) => ({...i, basketNo:`${size+idx+1}-${Date.now()}`}));
}