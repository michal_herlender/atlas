import store from "./store.js";
import { STORE_SEARCH_RESULTS_PAGE, STORE_SEARCH_RESULTS_PAGE_COUNT, STORE_SEARCH_RESULTS_CURRENT_PAGE_NUMBER, STORE_SEARCH_RESET_SEARCH,  STORE_NEW_JOBITEM_SEARCH_PARAMS, STORE_ADD_ITEM_TO_BASKET, SOTRE_REMOVE_FROM_BASKET, STORE_CLEAR_BASKET, STORE_ADD_ITEMS_TO_BASKET, STORE_BOOKING_IN_ADDRESS  } from "./actionsTypes.js";

export function storeSearchAllModelsResultsPage(page){
    store.send({type:STORE_SEARCH_RESULTS_PAGE, page:{matchingModels:page}});
}

export function storeSearchCompanyModelResultsPage(page){
    store.send({type:STORE_SEARCH_RESULTS_PAGE, page:{companyModels:page}});
}

export function storeSearchInstrumentsResultsPage(page){
    store.send({type:STORE_SEARCH_RESULTS_PAGE, page:{instruments:page}});
}

export function storeSearchResultsPage(page){
    store.send({type:STORE_SEARCH_RESULTS_PAGE,page});
}

export function storeSearchResultsPageNumber(pageCount){
    store.send({type:STORE_SEARCH_RESULTS_PAGE_COUNT,pageCount});
}

export function storeSearchResultsCurrentPageNumber(currentPageNumber){
    store.send({type:STORE_SEARCH_RESULTS_CURRENT_PAGE_NUMBER,currentPageNumber})
}

export function storeResetState(){
    store.send({type:STORE_SEARCH_RESET_SEARCH});
}

export function storeNewJobItemSearchParams(searchParams){
    store.send({type:STORE_NEW_JOBITEM_SEARCH_PARAMS,searchParams});
}


export function addItemToBasket(item){
    store.send({type:STORE_ADD_ITEM_TO_BASKET,item});
}

export function addItemsToBasket(items){
    store.send({type:STORE_ADD_ITEMS_TO_BASKET,items});
}

export function removeItemFromBasket(item){
    store.send({type:SOTRE_REMOVE_FROM_BASKET,item});
}

export function toggleItemInBasket(item){
    const {basket = []} = store.getState() ;
    const basketItem = basket.find(i => i.basketId ==item.basketId);
    if(basketItem) removeItemFromBasket(basketItem)
    else addItemToBasket(item);
}


export function storeBookingInAddrId(bookingAddressId){
    store.send({type:STORE_BOOKING_IN_ADDRESS,bookingAddressId})
}

export function clearBasket(){
    store.send({type:STORE_CLEAR_BASKET});
}