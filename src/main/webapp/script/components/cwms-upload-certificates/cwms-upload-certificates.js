import { htmlTemplate } from "./cwms-upload-certificates.html.js";
import { styling } from "./cwms-upload-certificates.css.js";
import { LitElement } from "../../thirdparty/lit-element/lit-element.js";
/* Depends also on Jquery (read below)*/

window.customElements.define('cwms-upload-certificates', class CwmsUploadCertificates extends LitElement {
	
	// watched properties
    static get properties() {
        return {
            jobId : { attribute : 'job-id' },
            files : { type : Array },
            uploadResult : { type : Object },
            uploadProgress : { type : Number },
            firstTime : { type : Boolean}
        };
    }
    
    static get styles() {
        return styling;
    }
    
    constructor(){
        super();
        this.reset();
        this.ajaxCall = null;
        this.firstTime = true;
    }

    /* Overridden : see lit-element docs */
    render() {  
        return htmlTemplate(this);
    }
    
    show(){
    	this.shadowRoot.querySelector('cwms-overlay').show();
    }
    
    hide(){
    	if(this.shadowRoot.querySelector('#filesInput')!= null)
    		this.shadowRoot.querySelector('#filesInput').value = "";
    	
    	if(this.shadowRoot.querySelector('cwms-overlay').open)
    		this.shadowRoot.querySelector('cwms-overlay').hide();
    }
    
    uploadFiles(){
    	
    	this.shadowRoot.querySelector('#uploadButton').disabled = true;
    	// form data
    	var formdata = new FormData();
    	formdata.append('jobid', this.jobId);
    	this.files.forEach( f => {
    		if(this.firstTime || this.shadowRoot.querySelector('#fileList fieldset div span[filename="'+f.name+'"]').innerHTML != '')
    			formdata.append('files', f);
    	}
    	);
    	this.firstTime = false;
		
		// include the CSRF token in the headers
		var csrfHeader = $("meta[name='_csrf_header']").attr("content");
		var csrfToken = $("meta[name='_csrf']").attr("content");
    	// using Jquery instead of Fetch API because this one doesn't offer a
		// solution to track upload progress
    	this.ajaxCall = $.ajax({
	        url: 'uploadcert.json',
	        type: 'POST',
			data: formdata,
			headers: {
				[csrfHeader] : csrfToken
			},
			cache:false,
			processData: false,
			contentType: false,
			context: this,
	        xhr: function(){
				var myXhr = $.ajaxSettings.xhr();
	        	myXhr.upload.addEventListener("progress", ((e) => {
	        		if(e.lengthComputable){
	        			
                        var max = e.total;
                        var current = e.loaded;

                        var percentage = (current * 100)/max;
                        
                        this.context.uploadProgress = percentage;
                    }
				}).bind(this), false); 
				
	        	return myXhr;
	        },
	        success:function(data){
	        	if(this.uploadProgress == 100)
	        		this.shadowRoot.querySelector('#uploadButton').disabled = false;
	        	this.uploadProgress += 10;
	        	this.uploadResult = data;
	        },
	        error: function(data){
	        	this.uploadProgress += 10;
	        	this.uploadResult = data;
	        }
	    });
    	
    }
    
    cancel(){
    	if(this.ajaxCall != null)
    		this.ajaxCall.abort();
    	this.hide();
    	this.reset();
    }
    
    readFilesNames(e){
    	if(this.shadowRoot!= null){
    		this.reset();
    		var filesInput = this.shadowRoot.querySelector('#filesInput').files;
    		for (var i = 0; i < filesInput.length; ++i)
    			this.files.push(filesInput.item(i))
    	}
    }
    
    reset(){
    	this.uploadProgress = null;
    	this.uploadResult = null;
    	this.ajaxCall = null;
		this.files = new Array();
    }
    
});
