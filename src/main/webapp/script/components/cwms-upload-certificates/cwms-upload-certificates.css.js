import { css } from '../../thirdparty/lit-element/lit-element.js'

export let styling = css`

.upload-message {
	text-align: center;
}

progress {
	width : 70%;
}

#progressValue {
	display : block;
}

h3 {
	margin: 0px;
}

progress[value]::-webkit-progress-value::before {
	content: '80%';
 	position: absolute;
 	right: 0;
 	top: -125%;
}

.errorMsg {
	float: right;
   	color: red;
}

.list {
	border-bottom: 1px dotted;
}

`;