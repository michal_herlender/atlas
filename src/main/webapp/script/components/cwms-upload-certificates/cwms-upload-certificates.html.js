import { html, directive } from '../../thirdparty/lit-html/lit-html.js';
import {unsafeHTML} from '../../thirdparty/lit-html/directives/unsafe-html.js';

export let htmlTemplate = (context) => html `

<link rel="stylesheet" href="styles/theme/theme.css">

<cwms-overlay @cwms-overlay-closed="${context.cancel}" >

	<cwms-translate slot="cwms-overlay-title" code="viewjob.uploadcertfile">Upload certificates</cwms-translate>
	
	<div slot="cwms-overlay-body" >
	
		<form method="POST" enctype="multipart/form-data" id="fileUploadForm"> 								
					        
	        <input type="file" name="files" id="filesInput" multiple @change="${context.readFilesNames}"
	        	accept=".doc,.docx,.pdf" />					        
			<button type="button" id="uploadButton" @click="${context.uploadFiles}" >
				<cwms-translat2 code="viewjob.uploadcertfile">Upload</cwms-translat2>
			</button>				
			<button type="button" @click="${context.hide}" id="cancelButton2">
				<cwms-translate code="cancel">Cancel</cwms-translate>
			</button>
			
			</br>
		</form>
		
	    ${context.uploadProgress == null || context.files.length == 0 ? `` :
			html`<center>
					<!-- 110 % : 100% for file upload, extra 10% for server response -->
					<span id="progressValue" > ${formatProgressPrcntg(context.uploadProgress - 10) } </span>
					<progress value="${context.uploadProgress }" max="110"></progress>
				</center></br>`
		}
		
		${context.uploadResult == undefined  || context.uploadResult.message == null ? `` :
			html`<div class="upload-message">
				<h3 .style=${`color:${context.uploadResult.success ? 'green' : 'red'}`} > ${ context.uploadResult.message } </h3>
			</div>`
		}
		
		${context.files.length == 0 ? `` :
			html`<div id="fileList" class="center">
					<fieldset>
						${context.files.map((item,index) => formatFileRow(index, item.name, context.uploadResult ))}
					</fieldset>
				</div>` 
		}
		
	</div>

</cwms-overlay>

`;

const formatFileRow = directive((index, fileName, uploadResult) => (part) => {
	let errorMessage = "";
	if( uploadResult!= null && uploadResult.errors != undefined &&
			uploadResult.errors.filter(er=>er.fieldName === fileName).length != 0){
		errorMessage = uploadResult.errors.filter(er=>er.fieldName == fileName)[0].message;
	}
	part.setValue(unsafeHTML(`<div class="list" >${index+1} - ${fileName} <span class="errorMsg" filename="${fileName}">${errorMessage}</span></div>`));
});

const formatProgressPrcntg = directive(num => (part) => {
	let v = 0;
	if(num > 0)
		v = Math.round( num * 100 + Number.EPSILON ) / 100
	part.setValue(unsafeHTML(`${v} %`));
});
