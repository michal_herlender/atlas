import { css } from '../../../thirdparty/lit-element/lit-element.js'

export const styling = (context) => css`

        :host {
            display:block;
        }


        .mainlink{
            color: var(--link-color);
            text-decoration: var(--link-decoration)
        }
`;