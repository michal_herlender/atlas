import { LitElement, html, css } from "../../../thirdparty/lit-element/lit-element.js";
import { template } from "./cwms-generic-link-and-tooltip.html.js";
import { template  as spinnerTemplate } from "../../cwms-spinner/cwms-spinner.html.js";
import { styling } from "./cwms-generic-link-and-tooltip.css.js";
import { debounce, throttle } from "../../../tools/Debounce.m.js";
import { styling as tooltipStyling } from "./tooltip.css.js";
import { generalStyling as generalSpinnerStyling } from "../../cwms-spinner/cwms-spinner.css.js";
import { linkStyling } from "../../tools/styles/links.css.js";


export class CwmsGenericLinkAndTooltip extends LitElement {


    static get properties(){
        return {
            withCopy:{type:Boolean},
            withMouse:{type:Boolean},
            hovered:{type:Boolean},
            tooltipBody:{type:Object,attribute:false},
            hasSpace:{type:Boolean,attribute:false},
        };
    }

    constructor(){
        super();
        this.withMouse = true;
        this.tooltipBody = spinnerTemplate({});
    }

    requestTooltipBody(){
        return Promise.reject("not implemented");
    }

    connectedCallback(){
        super.connectedCallback();
        this.addEventListener("mouseout",_ => this.hovered = false);
    }

    mousoverFunction() {
                this.hovered = true;
                const initiator = this.shadowRoot.querySelector("img") ?? this.shadowRoot.querySelector("a");
                const space = (document.documentElement.clientWidth - this.getAbsoluteLeft(initiator));
                this.hasSpace = space > 500;
                this.requestTooltipBody().then(body => this.tooltipBody = body)
                .catch(console.error)
                ;

    }
    

    getAbsoluteLeft(element) {
        var oLeft = element.offsetLeft            
        while(element.offsetParent!=null) {   
            var oParent = element.offsetParent   
            oLeft += oParent.offsetLeft 
            element = oParent
        }
        return oLeft
    }

    static get styles(){
        return [styling({}),tooltipStyling,generalSpinnerStyling, linkStyling()];
    }

    render(){
       return template({
           objectName:this.objectName ?? "LINK",
           targetURL:this.targetURL ?? ".",
           hovered:this.hovered,
           hasSpace:this.hasSpace,
           tooltipBody:this.tooltipBody,
           withCopy:this.withCopy,
           withMouse:this.withMouse,
           mousoverFunction:this.mousoverFunction,
           onHover: flag => this.hovered = flag,
    });
    }
}




