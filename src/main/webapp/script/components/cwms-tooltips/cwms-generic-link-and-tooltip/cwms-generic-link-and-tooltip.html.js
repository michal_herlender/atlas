import { html } from "../../../thirdparty/lit-element/lit-element.js";
import { MetaDataObtainer } from "../../../tools/metadataObtainer.js";
import { stylesMixin } from "../../tools/applicationStylesMixinTemplate.js";
import "../../cwms-dynamics-tabs/main.js";

export const template = (context) => html`
  <span class="hovertarget" @mouseover=${context.mousoverFunction}>
  <a href=${context.targetURL} class="mainlink" 
  target="_blank" >
    ${context.objectName} </a
  >${context.withMouse?html`<img
    src=${MetaDataObtainer.base + "img/icons/mouse.png"}
    width="12"
    height="12"
    class="image_inl"
    alt=""
    title=""
  />`:html``}</span>
  ${context.withCopy?html`
            <img src="img/icons/bullet_clipboard.png" class="pad-left-small" width="10" height="10" @click=${_ => navigator.clipboard.writeText(context.objectName)} alt="Add job no to clipboard" title="Add job no to clipboard">
  `:html``}
  ${context.hovered ? tooltipTemplate(context): html``}
`;

const tooltipTemplate = (context) => html`
  <div class=${`tooltip ${!context.hasSpace?'left':''}`}>
    <div class="tooltip-header">
     ${context.hasSpace? html` <i class="arrow-left"></i>`:html``}
      <div class="tooltip-title">${context.tooltipTitle ?? ""}</div>
     ${!context.hasSpace? html` <i class="arrow-right"></i>`:html``}
    </div>
    <div class="tooltip-body">
      ${context.tooltipBody}
    </div>
  </div>
`;

