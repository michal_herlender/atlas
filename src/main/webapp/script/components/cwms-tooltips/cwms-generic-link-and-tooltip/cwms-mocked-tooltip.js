import {  html  } from "../../../thirdparty/lit-element/lit-element.js";
import { stylesMixin } from "../../tools/applicationStylesMixinTemplate.js";
import { CwmsGenericLinkAndTooltip } from "./cwms-generic-link-and-tooltip.js";

class ConstantTooltip extends CwmsGenericLinkAndTooltip {
    
    
    requestTooltipBody(){
        return Promise.resolve(
            html`
            ${stylesMixin}
  <link rel="stylesheet" href="styles/jQuery/jquery-jtip.css" />
            <div id="JT_copy"><div id="jobItemCostingTT">	
<div class="slatenav-main">
		<ul>
			<li>
				<a id="ttjc_jobitem-link" onmouseover=" event.preventDefault(); switchMenuFocus(Elements1, 'ttjc_jobitem-tab', false); " class="selected">
					Job Item</a>
			</li>
		</ul>
	</div>
	<div id="ttjc_jobitem-tab" class="vis clearL">												
		<!-- job sub navigation bar -->
		<div class="slatenav-sub">
			<ul>
				<li>
					<a onmouseover=" event.preventDefault(); switchMenuFocus(Elements2, 'ttjc_jisummary-tab', false); " id="ttjc_jisummary-link" class="selected">
						Summary</a>
				</li>
				<li>
					<a onmouseover=" event.preventDefault(); switchMenuFocus(Elements2, 'ttjc_jiactions-tab', false); " id="ttjc_jiactions-link">
						 Actions</a>
				</li>
				<li>
					<a onmouseover=" event.preventDefault(); switchMenuFocus(Elements2, 'ttjc_jicontrev-tab', false); " id="ttjc_jicontrev-link">
						 Contract Review</a>
				</li>
			</ul>
		</div>
		<div id="ttjc_jisummary-tab">
			<fieldset class="newtooltip">
				<ol style=" background: url( 'img/noimage.jpg') top right no-repeat; ">
					<li style=" width: 70%; ">
						<label> Instrument Model Name :</label>
						<div class="fleftTooltip" style=" width: 60%; ">
							DEPTH GAUGE 0 mm &lt; Depth max &lt; 300 mm (SFC)</div>
						<div style=" clear: left;  "></div>
					</li>
					<li>
						<label>Customer Description</label>
						<span>Ausschu�-Gewindelehrdorn</span>
					</li>
					<li style=" width: 70%; ">
						<label> Serial No :</label>
						<span>K0269</span>
					</li>
					<li style=" width: 70%; ">
						<label> Plant No :</label>
						<span>K0269</span>
					</li>
					<li style=" width: 70%; ">
						<label> Service Type :</label>
						<span>TC-SOC-IL - Traceable calibration with statement of compliance in laboratory</span>
					</li>
					<li style=" width: 70%; ">
						<label> Date In :</label>
						<span>25.11.2020</span>
					</li>
					<li style=" width: 70%; ">
						<label> Due Date :</label>
						<span>02.12.2020</span>
					</li>
					<li style=" width: 70%; ">
						<label> Date Complete :</label>
						<span></span>
					</li>
					<li>
						<label> Status :</label>																	
						<span>Awaiting contract review</span>
					</li>
				</ol>
			</fieldset>
		</div>
		<div id="ttjc_jiactions-tab" class="hid">
			<table class="default2">
				<thead>
					<tr>
						<td colspan="6"> Active Actions</td>
					</tr>
					<tr>															
						<th class="activity" scope="col"> Activity</th>
						<th class="compby" scope="col"> Started By</th>
						<th class="compon" scope="col"> Started On</th>
						<th class="endstatus" scope="col"> End Status</th>
						<th class="remarks" scope="col"> Remark</th>
						<th class="time" scope="col"> Time Spent</th>
					</tr>
				</thead>
				<tbody>
					<tr class="odd">																						
								<td>Unit received at goods in</td>
								<td>Michal Herlender</td>
								<td>25.11.2020 04:49</td>
								<td>Awaiting contract review</td>
								<td></td>
								<td>0 min</td>
							</tr>
					</tbody>
			</table>
		</div>
		<div id="ttjc_jicontrev-tab" class="hid">
			<fieldset class="newtooltip">
				<ol>
					<li>
						<label> Turnaround :</label>
						<span> 7</span>
					</li>
					<li>
						<label> Due Date :</label>
						<span> 02.12.2020</span>
					</li>
				</ol>
			</fieldset>
			<table class="default2">
				<thead>
					<tr>
						<td colspan="6"> Contract Review</td>
					</tr>
					<tr>
						<th class="costtype" scope="col"> Price Type</th>
						<th class="costs" scope="col"> Price</th>
						<th class="disc" scope="col"> Discount</th>
						<th class="discval" scope="col"> Discount Value</th>
						<th class="finalcost" scope="col"> Final Price</th>
						<th class="costsource" scope="col"> Price Source</th>
					</tr>
				</thead>
				<tbody>
					<tr class="odd">
								<td>CALIBRATION</td>
								<td>0.00 EUR</td>
								<td>0.00 %</td>
								<td>0.00 EUR</td>
								<td>0.00 EUR</td>
								<td>MANUAL</td>
							</tr>
						</tbody>
			</table>
			<table class="default2">
				<thead>
					<tr>
						<td colspan="3"> Contract Review History</td>
					</tr>
					<tr>
						<th class="reviewer" scope="col"> Reviewer</th>
						<th class="daterev" scope="col"> Date Reviewed</th>
						<th class="notes" scope="col"> Notes Left</th>
					</tr>
				</thead>
				<tbody>
					<tr class="odd center bold">
							<td colspan="3"> No contract review history held /td&gt;
						</td></tr>
					</tbody>
			</table>
		</div>
		</div>
	
</div></div>`
        )
    }
}

customElements.define("cwms-mocked-tooltip",ConstantTooltip);