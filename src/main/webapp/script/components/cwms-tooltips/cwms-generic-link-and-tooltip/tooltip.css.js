
import { css } from '../../../thirdparty/lit-element/lit-element.js'

export const styling =  css`
.tooltip {
  position: absolute;
  border: 2px solid var(--cwms-red,#A80000);
  border-top: none;
  background-color: white;
  color: black;
  width: var(--tooltip-width,400px);
  z-index:99;
  display: inline-block;
}

.tooltip.left {
  transform: translateX(-106%);
}

.tooltip .tooltip-header {
    background-color: var(--cwms-red,#A80000);
    display: flex;
    height: 23px;
}

.tooltip .tooltip-header .arrow-left {
      width: 0;
      height: 0;
      border-top: 10px solid transparent;
      border-right: 12px solid var(--cwms-red,#A80000);
      border-bottom: 10px solid transparent;
      transform: translateX(-120%);
}

.tooltip .tooltip-header .arrow-right {
      width: 0;
      height: 0;
      border-top: 10px solid transparent;
      border-left: 12px solid var(--cwms-red,#A80000);
      border-bottom: 10px solid transparent;
      transform: translateX(120%);
}

.tooltip .tooltip-header .tooltip-title {
    color: #eee;
    text-transform: uppercase;
    flex-grow: 2;
    justify-content: center;
    align-content: center;
    text-align: center;
    font-size: 120%;
    font-weight: bold;
    padding-top: 4px;
}

.spinner-container {
    display:flex;
    justify-content:center;
    align-items:center;
}

`
