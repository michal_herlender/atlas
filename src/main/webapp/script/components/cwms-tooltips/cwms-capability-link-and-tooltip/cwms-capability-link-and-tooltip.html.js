import { html } from '../../../thirdparty/lit-element/lit-element.js'
import CwmsTranslate from "../../cwms-translate/cwms-translate.js";
import { yesNoDirective, dateFormatWihtAltTextDirective  } from '../../tools/formatingDirectives.js';
import { until } from '../../../thirdparty/lit-html/directives/until.js';
import { cwmsTranslateDirective } from '../../cwms-translate/cwms-translate-directive.js';

export const tooltipBodyTemplate = (context) => html`
  <nav is="cwms-dynamic-tabs-holder">
    <cwms-dynamic-tab tabName="CAPABILITY" tabLabel=${until(CwmsTranslate.messagePromise("capability","capability"))}>
      <nav is="cwms-dynamic-tabs-holder" variant="red">
        <cwms-dynamic-tab tabName="SUMARY" tabLabel=${until(CwmsTranslate.messagePromise("summary","Summary"))}>
          ${jobSummaryTemplate(context?.out,context?.info)}
        </cwms-dynamic-tab>
      </nav>
    </cwms-dynamic-tab>
  </nav>
`;

const jobSummaryTemplate = (context,info) => html`
<fieldset class="newtooltip">
				<ol>
					<li>
						<label><cwms-translate code="name">Name</cwms-translate> :</label>
						<span>${context.name}</span>
					</li>
					<li>
						<label><cwms-translate code="description">Description</cwms-translate> :</label>
						<span>${context.description || "-"}</span>
					</li>
					<li>
						<label><cwms-translate code="reference">Reference</cwms-translate> :</label>
						<span>${context.reference}</span>
					</li>
					<li>
						<label><cwms-translate code="active">Active</cwms-translate> :</label>
						<span> ${yesNoDirective(context.active)}</span>
					</li>
					<li>
						<label><cwms-translate code="editproc.heading">Heading</cwms-translate> :</label>
						<span>
							${context.heading}
						</span>
					</li>
					<li>
						<label><cwms-translate code="company.department">Department</cwms-translate> :</label>
						<span>${context.department}</span>
					</li>
					<li>
						<label><cwms-translate code="capability.category">Category</cwms-translate> :</label>
						<span>${context.category}</span>
					</li>
					<li>
						<label><cwms-translate code="editservicecapability.calibrationprocess">Calibration Process</cwms-translate> :</label>
						<span>
							${context.calibrationProcess}
						</span>
					</li>
					<li>
						<label><cwms-translate code="capability.updateOn">Updated On</cwms-translate> :</label>
						<span>
							${dateFormatWihtAltTextDirective(context.updatedOn)}
						</span>
					</li>
				</ol>
			</fieldset>`;


const accreditationsLevelBlockTemplate = out=> info => 
	info?.calTypes?.flatMap(ct => 
		info.accredLevels.map(al =>
		 html`<li>
				<label class="labelhead">${ct.label} - ${cwmsTranslateDirective(al.messageCode,al.defaultMessage)}</label>
				<span>&nbsp;</span>
			</li>
`) )




