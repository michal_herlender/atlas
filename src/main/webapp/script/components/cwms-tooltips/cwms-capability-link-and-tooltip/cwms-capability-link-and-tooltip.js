import { fieldsetStyles } from "../../tools/styles/fieldset.css.js";
import { tableTooltipStyles } from "../../tools/styles/table.css.js";
import { generalTooltipStyles } from "../../tools/styles/tooltip.css.js";
import {CwmsGenericLinkAndTooltip} from "../cwms-generic-link-and-tooltip/cwms-generic-link-and-tooltip.js";
import { tooltipBodyTemplate } from "./cwms-capability-link-and-tooltip.html.js";
import { styling as tooltipStyling } from "./tooltip.css.js";
import { urlWithParamsGenerator } from "../../../tools/UrlGenerator.js";

class CwsmCapabilityLinkAndTooltip extends CwmsGenericLinkAndTooltip {

    static get properties(){
        return {id:{type:Number},
        reference:{type:String},
    };
    }

    constructor(){
      super();
    }

    static get styles(){
      return [...super.styles,  tooltipStyling, generalTooltipStyles, tableTooltipStyles, fieldsetStyles];
    }

    prepareUrl(id){
      return urlWithParamsGenerator("viewcapability.htm",{capabilityId:id});
    }


    get objectName(){
        return this.reference;
    }

    get targetURL(){
        return this.prepareUrl(this.id);
    }

    async requestTooltipBody(){
        const url =  urlWithParamsGenerator("tooltip/capability.json",{id:this.id});
        const res = await fetch(url);
      const res_1 = await (res.ok ? res.json() : Promise.reject(res.statusText));
      return tooltipBodyTemplate({
        out: res_1._1,
        info: res_1._2
      });
    }
}

customElements.define("cwms-capability-link-and-tooltip",CwsmCapabilityLinkAndTooltip);