import { css } from "../../../thirdparty/lit-element/lit-element.js";

export const styling = (context) => css`
    .newtooltip {
        max-height:700px;
        overflow-y:scroll;
    }
`;