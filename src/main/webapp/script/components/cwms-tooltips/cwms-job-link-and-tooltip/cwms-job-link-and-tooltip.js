import { MetaDataObtainer } from "../../../tools/metadataObtainer.js";
import { jobLinkGenerator } from "../../../tools/UrlGenerator.js";
import { tableStyling } from "../../tools/exchange-fromat-utils/tableRenderer.js";
import { fieldsetStyles } from "../../tools/styles/fieldset.css.js";
import { tableStyles, tableTooltipStyles } from "../../tools/styles/table.css.js";
import { generalTooltipStyles } from "../../tools/styles/tooltip.css.js";
import {CwmsGenericLinkAndTooltip} from "../cwms-generic-link-and-tooltip/cwms-generic-link-and-tooltip.js";
import { tooltipBodyTemplate } from "./cwms-job-link-and-tooltip.html.js";
import { styling as tooltipStyling } from "./tooltip.css.js";

class CwsmJobLinkAndTooltip extends CwmsGenericLinkAndTooltip {

    static get properties(){
        return {jobId:{type:Number},
        jobNo:{type:String},
        mainHeader:{type:String},
        summaryHeader:{type:String},
        actionHeader:{type:String},
        contractReviewHeader:{type:String},
        targetTab:{type:String}
    };
    }

    constructor(){
      super();
    }

    static get styles(){
      return [...super.styles, tooltipStyling, generalTooltipStyles, tableTooltipStyles, fieldsetStyles];
    }

    prepareUrl(id){
      if(this.targetTab)
        return jobLinkGenerator(id,{loadtab:this.targetTab});
      return jobLinkGenerator(id);
    }


    get objectName(){
        return this.jobNo;
    }

    get targetURL(){
        return this.prepareUrl(this.jobId);
    }

    async requestTooltipBody(){
        const url = new URL("tooltip/job.json",MetaDataObtainer.base);
        url.searchParams.append("jobId",this.jobId);
        const res = await fetch(url);
      const res_1 = await (res.ok ? res.json() : Promise.reject(res.statusText));
      return tooltipBodyTemplate({
        out: res_1,
      });
    }
}

customElements.define("cwms-job-link-and-tooltip",CwsmJobLinkAndTooltip);