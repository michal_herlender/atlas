import { html } from '../../../thirdparty/lit-element/lit-element.js'
import { stylesMixin } from '../../tools/applicationStylesMixinTemplate.js';
import CwmsTranslate from "../../cwms-translate/cwms-translate.js";
import { dateFormatWihtAltTextDirective, currencyFormatWithZeroDirective, dateFormatDirective } from '../../tools/formatingDirectives.js';
import { until } from '../../../thirdparty/lit-html/directives/until.js';

export const tooltipBodyTemplate = (context) => html`
  <nav is="cwms-dynamic-tabs-holder">
    <cwms-dynamic-tab tabName="JOB" tabLabel=${until(CwmsTranslate.messagePromise("job","JOB"))}>
      <nav is="cwms-dynamic-tabs-holder" variant="red">
        <cwms-dynamic-tab tabName="SUMARY" tabLabel=${until(CwmsTranslate.messagePromise("summary","Summary"))}>
          ${jobSummaryTemplate(context?.out)}
        </cwms-dynamic-tab>
        <cwms-dynamic-tab id="JT_copy" tabName="JOBITEM" tabLabel=${until(CwmsTranslate.messagePromise("vm_global.jobitem","Job Item"))} >
          ${jobItemTemplate(context?.out?.jobitems)}
        </cwms-dynamic-tab>
      </nav>
    </cwms-dynamic-tab>
  </nav>
`;

const jobSummaryTemplate = context => html`
<fieldset class="newtooltip">
				<ol>
					<li>
						<label><cwms-translate code="company">Company</cwms-translate> :</label>
						<span>${context.contact.subdiv.company.coname}</span>
					</li>
					<li>
						<label><cwms-translate code="subdivision">Subdivision</cwms-translate> :</label>
						<span>${context.contact.subdiv.subname}</span>
					</li>
					<li>
						<label><cwms-translate code="contact">Contact</cwms-translate> :</label>
						<span>${context.contact.name}</span>
					</li>
					<li>
						<label><cwms-translate code="telephone">Phone</cwms-translate> :</label>
						<span>${context.contact.telephone}</span>
					</li>
					<li>
						<label><cwms-translate code="company.email">E-mail</cwms-translate> :</label>
						<span>
							${context.contact.email? html`<a href="mailto:${context.contact.email}" class="mainlink">
									${context.contact.email}
								</a>`:html``}
						</span>
					</li>
					<li>
						<label><cwms-translate code="status">Status</cwms-translate> :</label>
						<span>${context.jobStatus.value}</span>
					</li>
					<li>
						<label><cwms-translate code="clientref">Client Ref</cwms-translate> :</label>
						<span>${context.clientRef}</span>
					</li>
					<li>
						<label><cwms-translate code="regdate">Reg Date</cwms-translate> :</label>
						<span>
							${dateFormatWihtAltTextDirective(context.regDate)}
						</span>
					</li>
					<li>
						<label><cwms-translate code="datein">Date in</cwms-translate> :</label>
						<span>
							${dateFormatWihtAltTextDirective(context.receiptDate)}
						</span>
					</li>
					<li>
						<label><cwms-translate code="datecomplete">Date complete</cwms-translate> :</label>
						<span>
							${dateFormatWihtAltTextDirective(context.dateComplete)}
						</span>
					</li>
				</ol>
			</fieldset>`;

const jobItemTemplate = jobItems => html`
<table class="default2 jitems" summary="This table shows all job items for the current job">
				<thead>
					<tr>
						<td colspan="6">
							<cwms-translate code="items">Items </cwms-translate> 
							( ${jobItems.length} ) 
						</td>
					</tr>
					<tr>
						<th class="item" scope="col"> <cwms-translate code="item" >Item</cwms-translate></th>
						<th class="inst" scope="col"> <cwms-translate code="instmodelname" >Instrument Model Name</cwms-translate></th>
						<th class="serial" scope="col"> <cwms-translate code="serialno" >Serial No</cwms-translate></th>
						<th class="plant" scope="col"> <cwms-translate code="plantno" >Plant No</cwms-translate></th>
						<th class="caltype" scope="col"> <cwms-translate code="servicetype" >Service Type</cwms-translate></th>
						<th class="status" scope="col"> <cwms-translate code="status" >Status</cwms-translate></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="6">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
					${jobItems.map(jiDto => html`
						<tr>
							<td class="item">${jiDto.itemno}</td>
							<td class="inst">Instrument model Name</td>
							<td class="serial">${jiDto.instrument.serialno}</td>
							<td class="plant">${jiDto.instrument.plantno}</td>
							<td class="caltype">${jiDto.serviceType.shortName}</td>
							<td class="status">${jiDto.state.value}</td>
						</tr>`)}
				</tbody>
			</table>
`;





