import { CwmsGenericLinkAndTooltip } from "../cwms-generic-link-and-tooltip/cwms-generic-link-and-tooltip.js";
import CwmsTranslate from "../../cwms-translate/cwms-translate.js";
import { currencyFormat, currencySymbolDirective } from "../../tools/formatingDirectives.js";
import { MetaDataObtainer } from "../../../tools/metadataObtainer.js";
import { html,css } from "../../../thirdparty/lit-element/lit-element.js";
import { currencyEntityBaseValueTooltipLinkGenerator } from "../../../tools/UrlGenerator.js";

class CwmsEtitiyCurrencyConversionTooltip extends CwmsGenericLinkAndTooltip {

    
    static get properties(){
        return {
            ...CwmsGenericLinkAndTooltip.properties,
            entityId:{type:Number},
            ammount:{type:Number},
            resultMsg:{type:String}
    };
    }

    static get styles(){
        return [...super.styles,css`
            :host{
                --tooltip-width:200px;
            }
        `]
    }

    constructor(){
        super();
        this.currencySymbol = html`${currencySymbolDirective(MetaDataObtainer.deafaultCurrency)}`
        CwmsTranslate.subscribeMessage("tooltips.monetaryValue","Monetary value is {0}", msg => this.resultMsg=msg)
    }


    get objectName(){
        return html`<cwms-translate code="showvaluein">Show value in</cwms-translate>&nbsp;${this.currencySymbol}`;
    }

    async requestTooltipBody(){
      const res = await fetch(currencyEntityBaseValueTooltipLinkGenerator(this.entityId, this.ammount),{
          credentials:"same-origin"
      });
        const data = await (res.ok ? res.json() : Promise.reject(res.statusText));
        const value = await (data.right? data.value: Promise.reject(data.value));
        return html`<div class="left" >${this.resultMsg.replaceAll("{0}",currencyFormat(value))}</div>`;

    }

}
customElements.define("cwms-entitiy-currency-conversion-tooltip",CwmsEtitiyCurrencyConversionTooltip)