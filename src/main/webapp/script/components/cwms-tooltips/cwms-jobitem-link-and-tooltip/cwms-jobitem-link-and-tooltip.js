import { MetaDataObtainer } from "../../../tools/metadataObtainer.js";
import { jobItemLinkGenerator } from "../../../tools/UrlGenerator.js";
import { fieldsetStyles } from "../../tools/styles/fieldset.css.js";
import { tableTooltipStyles } from "../../tools/styles/table.css.js";
import { generalTooltipStyles } from "../../tools/styles/tooltip.css.js";
import {CwmsGenericLinkAndTooltip} from "../cwms-generic-link-and-tooltip/cwms-generic-link-and-tooltip.js";
import { tooltipBodyTemplate } from "./cwms-jobitem-link-and-tooltip.html.js";
import { styling as tooltipStyling } from "./tooltip.css.js";

class CwsmJobItemLinkAndTooltip extends CwmsGenericLinkAndTooltip {

    static get properties(){
        return {jobItemId:{type:Number},
        jobItemNo:{type:String},
        mainHeader:{type:String},
        summaryHeader:{type:String},
        actionHeader:{type:String},
        contractReviewHeader:{type:String},
    };
    }

    constructor(){
      super();
      CwmsTranslate.subscribeMessage("vm_global.jobitem","Job Item",msg => this.mainHeader = msg);
      CwmsTranslate.subscribeMessage("summary","summary",msg => this.summaryHeader = msg);
      CwmsTranslate.subscribeMessage("viewinstrument.actions","Actions",msg => this.actionHeader = msg);
      CwmsTranslate.subscribeMessage("jicontractreview.contrrew","Contract review",msg => this.contractReviewHeader = msg);

    }

    static get styles(){
      return [...super.styles, tooltipStyling, generalTooltipStyles, tableTooltipStyles, fieldsetStyles];
    }

    prepareUrl(id){
      return jobItemLinkGenerator(id);
    }


    get objectName(){
        return this.jobItemNo;
    }

    get targetURL(){
        return this.prepareUrl(this.jobItemId);
    }

    async requestTooltipBody(){
        const url = new URL("tooltip/jobItem.json",MetaDataObtainer.base);
        url.searchParams.append("jobItemId",this.jobItemId);
        const res = await fetch(url);
      const res_1 = await (res.ok ? res.json() : Promise.reject(res.statusText));
      return tooltipBodyTemplate({
        mainHeader:this.mainHeader,
        summaryHeader:this.summaryHeader,
        out: res_1,
        actionHeader:this.actionHeader,
        contractReviewHeader:this.contractReviewHeader,
      });
    }
}

customElements.define("cwms-jobitem-link-and-tooltip",CwsmJobItemLinkAndTooltip);