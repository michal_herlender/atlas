import { html } from '../../../thirdparty/lit-element/lit-element.js'
import { stylesMixin } from '../../tools/applicationStylesMixinTemplate.js';
import "../../cwms-dynamics-tabs/main.js"
import "../../cwms-translate/cwms-translate.js";
import { dateFormatWihtAltTextDirective, currencyFormatWithZeroDirective, dateFormatDirective } from '../../tools/formatingDirectives.js';

export const tooltipBodyTemplate = (context) => html`
  <nav is="cwms-dynamic-tabs-holder">
    <cwms-dynamic-tab tabName=${context.mainHeader}>
      <nav is="cwms-dynamic-tabs-holder" variant="red">
        <cwms-dynamic-tab tabName=${context.summaryHeader}>
          ${jobItemSummaryTemplate(context?.out)}
        </cwms-dynamic-tab>
        <cwms-dynamic-tab tabName=${context.actionHeader} >
          ${actionsTemplate(context?.out?.actions)}
        </cwms-dynamic-tab>
        <cwms-dynamic-tab tabName=${context.contractReviewHeader}>
          ${contractReviewsTemplate(context?.out)}
        </cwms-dynamic-tab>
      </nav>
    </cwms-dynamic-tab>
  </nav>
`;


const jobItemSummaryTemplate = context => html`
<fieldset class="newtooltip">
    <ol>
        <li>
            <label><cwms-translate code="instmodelname">Instrument Model Name</cwms-translate>:</label>
            <span>${context.instrument.instrumentModelNameViaFields}</span>
        </li>
        <li>
            <label><cwms-translate code="viewinstrument.customerdescription">Customer Description</cwms-translate>:</label>
            <span>${context.instrument.customerDescription}</span>
        </li>
        <li>
            <label><cwms-translate code="serialno">Serial Number</cwms-translate>:</label>
            <span>${context.instrument.serialno}</span>
        </li> 
        <li>
            <label><cwms-translate code="plantno">Plant Number</cwms-translate>:</label>
            <span>${context.instrument.plantno}</span>
        </li> 

         <li>
            <label><cwms-translate code="servicetype">Service Type</cwms-translate>:</label>
            <span>${context.serviceType.bothNames}</span>
        </li>  
        <li>
            <label><cwms-translate code="datein">Date in</cwms-translate>:</label>
            <span>${dateFormatDirective(context.dateIn)}</span>
        </li> 
        <li>
            <label><cwms-translate code="duedate">Due Date</cwms-translate>:</label>
            <span>${dateFormatDirective(context.dueDate)}</span>
        </li>  
        <li>
            <label><cwms-translate code="datecomplete">Date Complete</cwms-translate>:</label>
            <span>${dateFormatDirective(context.dateComplete)}</span>

        </li> 
        <li>
            <label><cwms-translate code="status">Status</cwms-translate>:</label>
            <span>${context.state.value}</span>
        </li> 
     </ol>
</fieldset>
`;



const actionsTemplate = context => html`
    <table class="cwms-tooltip default2 instTooltipJob" summary="This table displays job information for this instrument">
				<thead>
					<tr>
						<td colspan="6">
							<cwms-translate code="jiactions.actactions">Actice Actions</cwms-translate> 
						</td>
					</tr>
					<tr>
						<th class="activity" scope="col">
							<cwms-translate code="jiactions.activity">Activity</cwms-translate>
						</th>
						<th class="compby" scope="col">
							<cwms-translate code="jiactions.startedby">Started By</cwms-translate>
						</th>
						<th class="compon" scope="col">
							<cwms-translate code="viewjob.startedon">Started on</cwms-translate>
						</th>
						<th class="endstatus" scope="col">
							<cwms-translate code="jiactions.endstatus">End Status</cwms-translate>
						</th>
						<th class="remarks" scope="col">
							<cwms-translate code="jiactions.remark">Remark</cwms-translate>
						</th>
						<th class="time" scope="col">
							<cwms-translate code="jiactions.timespent">Time spend</cwms-translate>
						</th>
					</tr>
				</thead>
        <tfoot>
            <tr>
                <td colspan="10">&nbsp; </td>
            </tr>
        </tfoot>
				<tbody>
                    ${context && context.length >0 ?
                    context.map(action => html`
                        <tr>
                            <td>${action.activity.value}</td>
                            <td>
                                ${action.startedBy.value}
                            </td>
                            <td>${dateFormatWihtAltTextDirective(action.startStamp)}</td>
                            <td>
                                ${action.endStatus.value}
                            </td>
                            <td class="tti_turn">${action.remark}</td>
                            <td>${action.timeSpent}<cwms-translate code="jobcost.mins">min</cwms-translate></td>
                        </tr>
                    `):html`
						<tr class="center bold">
							<td colspan="3"> <cwms-translate code="viewjob.none">None</cwms-translate> </td>
						</tr>
                    `}
				</tbody>
			</table>
`;

const contractReviewsTemplate = context => html`
			<fieldset class="newtooltip">
				<ol>
					<li>
						<label> <cwms-translate code="jicontractreview.turnaround">Turnaround</cwms-translate>:</label>
						<span> ${context.turn} </span>
					</li>
					<li>
						<label> <cwms-translate code="duedate">Due Date</cwms-translate>:</label>
						<span>${dateFormatWihtAltTextDirective(context.dueDate)}</span>
					</li>
				</ol>
            </fieldset>
            ${priceContractReviewTemplate(context.job.currency.value)(context.priceContractReview.prices
                .filter(p => p.active))}
            ${contractReviewsHistoryTemplate(context.contractReviews)}
`;
const priceContractReviewTemplate = currencyCode => prices => html`
<table class="default2 cwms-tooltip" >
				<thead>
					<tr>
						<td colspan="6">
							<cwms-translate code="jicontractreview.contrrew">Contract Review</cwms-translate>
						</td>
					</tr>
					<tr>
						<th class="costtype" scope="col"> <cwms-translate code="jicontractreview.costtype">Price Type</cwms-translate> </th>
						<th class="costs" scope="col"> <cwms-translate code="jicontractreview.costs"></cwms-translate>Price </th>
						<th class="disc" scope="col"> <cwms-translate code="jicontractreview.discount"></cwms-translate> Discount</th>
						<th class="discval" scope="col"> <cwms-translate code="jicontractreview.discval"></cwms-translate>Discount Value </th>
						<th class="finalcost" scope="col"> <cwms-translate code="jicontractreview.finalcost">Fina Price</cwms-translate> </th>
						<th class="costsource" scope="col"> <cwms-translate code="jicontractreview.costsource">Price Source</cwms-translate> </th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="6">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
                    ${prices && prices.length > 0?
                    prices.map(price =>  html`
							<tr>
								<td>
											${price.costType}
								</td>
								<td>
                                    ${currencyFormatWithZeroDirective(price.totalCost,currencyCode)}
								</td>
								<td>${price.discountRate} %
								</td>
								<td>
                                    ${currencyFormatWithZeroDirective(price.discountValue,currencyCode)}
								</td>
								<td>
                                    ${currencyFormatWithZeroDirective(price.finalCost,currencyCode)}
								</td>
								<td class="center">
									${price.costSource}
								</td>
                            </tr>`):html`
						<tr class="center bold">
							<td colspan="3"> <cwms-translate code="jicontractreview.string18">No contract review history held</cwms-translate> </td>
						</tr>
                            `}
				</tbody>
			</table>
`

const contractReviewsHistoryTemplate = contractReviews => html`
			<table class="default2 cwms-tooltip">
				<thead>
					<tr>
						<td colspan="3"> <cwms-translate code="jicontractreview.contrrevhist">
Contract Review History</cwms-translate> </td>
					</tr>
					<tr>
						<th class="reviewer" scope="col"> <cwms-translate code="jicontractreview.reviewer">Reviewer</cwms-translate> </th>
						<th class="daterev" scope="col"> <cwms-translate code="jicontractreview.datereved">Date Reviewed</cwms-translate> </th>
						<th class="notes" scope="col"> <cwms-translate code="jicontractreview.notesleft">Notes Left</cwms-translate> </th>
					</tr>
                </thead>
				<tfoot>
					<tr>
						<td colspan="6">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
                    ${contractReviews && contractReviews.length > 0?
					contractReviews.map(cr => html`
						<tr >
							<td>"${cr.reviewBy.value}</td>
							<td>${dateFormatWihtAltTextDirective(cr.reviewDate)}</td>
							<td>${cr.comments}</td>
						</tr>
					`):html`
						<tr class="center bold">
							<td colspan="3"> <cwms-translate code="jicontractreview.string18">No contract review history held</cwms-translate> </td>
						</tr>
					`}
				</tbody>
			</table>
`




