import { html } from '../../../thirdparty/lit-element/lit-element.js'
import { stylesMixin } from '../../tools/applicationStylesMixinTemplate.js';
import "../../cwms-translate/cwms-translate.js";

export const tooltipBodyTemplate = (context) => html`
  <nav is="cwms-dynamic-tabs-holder">
    <cwms-dynamic-tab tabName=${context.contactHeader}>
      <nav is="cwms-dynamic-tabs-holder" variant="red">
        <cwms-dynamic-tab tabName=${context.summaryHeader}>
          ${contactSummaryTemplate(context)}
        </cwms-dynamic-tab>
      </nav>
    </cwms-dynamic-tab>
  </nav>
`;


const contactSummaryTemplate = context => html`
${stylesMixin}
<fieldset class="newtooltip">
    <ol>
        <li>
            <label><cwms-translate code="name">Name</cwms-translate>:</label>
            <span>${context.contact.name}</span>
        </li>
        ${context.contact.defAddress? html`
        <li>
            <label><cwms-translate code="company.homeaddress">Home Address</cwms-translate>:</label>
            <div class="float-left" >
                ${context.contact.addr1}<br/>
               ${context.contact.addr2 ? html`${context.contact.addr2}<br/>`:html``}
               ${context.contact.addr3 ? html`${context.contact.addr3}<br/>`:html``}
               ${context.contact.town ? html`${context.contact.town}<br/>`:html``}
               ${context.contact.county ? html`${context.contact.county}<br/>`:html``}
               ${context.contact.postcode ? html`${context.contact.postcode}<br/>`:html``}

            </div>
        </li>`:html``}
        ${context.contact.position? html`
            <li>
                <label><cwms-translate code="position">Position</cwms-translate></label>
                <span>${context.contact.position}</span>
            </li>
        `:html``}
        <li>
            <label><cwms-translate code="telephone">Telephone</cwms-translate>:</label>
            <span>${context.contact.telephone}</span>
        </li>
        ${context.contact.mobile? html`
        <li>
            <label><cwms-translate code="mobile">Mobile</cwms-translate>:</label>
            <span>${context.contact.mobile}</span>
        </li>
        `:html``}
        <li>
            <label><cwms-translate code="fax">Fax</cwms-translate>:</label>
            <span>${context.contact.fax}</span>
        </li>
        <li>
            <label><cwms-translate code="email">Email</cwms-translate>:</label>
            <span><a href=${`mailto:${context.contact.email}`} class="thickbox mainlink" title=${`Send email to ${context.contact.name}`}>${context.contact.email}</a></span>
        </li>
        <li>
            <label><cwms-translate code="active">Active</cwms-translate>:</label>
            <span>${context.contact.active? 
            html`<cwms-translate code="active">Active</cwms-translate>`:
            html`<cwms-translate code="incactive">Inactive</cwms-translate>`}</span>
        </li>
        <li>
            <label><cwms-translate code="contact.contactPreference">Contact Preference</cwms-translate>:</label>
            <span>${context.contact.preference}</span>
        </li>
    </ol>
</fieldset>
`;