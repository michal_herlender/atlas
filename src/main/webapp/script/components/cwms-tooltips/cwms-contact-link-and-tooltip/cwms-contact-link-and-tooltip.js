import { MetaDataObtainer } from "../../../tools/metadataObtainer.js";
import {CwmsGenericLinkAndTooltip} from "../cwms-generic-link-and-tooltip/cwms-generic-link-and-tooltip.js";
import { tooltipBodyTemplate } from "./cwms-contact-link-and-tooltip.html.js";

class CwsmContactLinkAndTooltip extends CwmsGenericLinkAndTooltip {

    static get properties(){
        return {contactId:{type:Number},
                contactName:{type:String},
                contactHeader:{type:String},
                summaryHeader:{type:String},
    };
    }

    constructor(){
      super();
      CwmsTranslate.subscribeMessage("contact","contact",msg => this.contactHeader = msg);
      CwmsTranslate.subscribeMessage("summary","summary",msg => this.summaryHeader = msg);

    }

    prepareUrl(id){
        const url = new URL("viewperson.htm",MetaDataObtainer.base);
        url.searchParams.append("personid",id);
        return url;
    }


    get objectName(){
        return this.contactName;
    }

    get targetURL(){
        return this.prepareUrl(this.contactId)
    }

    async requestTooltipBody(){
        const url = new URL("contact.json",MetaDataObtainer.base);
        url.searchParams.append("personid",this.contactId);
        const res = await fetch(url);
      const res_1 = await (res.ok ? res.json() : Promise.reject(res.statusText));
      return tooltipBodyTemplate({
        contactHeader: this.contactHeader,
        summaryHeader: this.summaryHeader,
        contact: res_1
      });
    }
}

customElements.define("cwms-contact-link-and-tooltip",CwsmContactLinkAndTooltip);