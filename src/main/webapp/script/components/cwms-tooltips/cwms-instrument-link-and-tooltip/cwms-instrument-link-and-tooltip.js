import { css } from "../../../thirdparty/lit-element/lib/css-tag.js";
import { MetaDataObtainer } from "../../../tools/metadataObtainer.js";
import { linkStyling } from "../../tools/styles/links.css.js";
import {CwmsGenericLinkAndTooltip} from "../cwms-generic-link-and-tooltip/cwms-generic-link-and-tooltip.js";
import { tooltipBodyTemplate } from "./cwms-instrument-link-and-tooltip.html.js";
import { styling as tooltipStyling } from "./tooltip.css.js";

class CwsmInstrumentLinkAndTooltip extends CwmsGenericLinkAndTooltip {

    static get properties(){
        return {plantid:{type:Number},
        modelName:{type:String},
        displayPlantId:{type:Boolean},
        displayModel:{type:Boolean},
        mainHeader:{type:String},
        summaryHeader:{type:String},
        threadHeader:{type:String},
        jobsHeader:{type:String},
        certsHeader:{type:String},
        notesHeader:{type:String},
    };
    }

    constructor(){
      super();
      this.displayPlantId = true;
      this.displayModel = false;
      CwmsTranslate.subscribeMessage("instrument","instrument",msg => this.mainHeader = msg);
      CwmsTranslate.subscribeMessage("summary","summary",msg => this.summaryHeader = msg);
      CwmsTranslate.subscribeMessage("editinstr.thread","Thread",msg => this.threadHeader = msg);
      CwmsTranslate.subscribeMessage("jobs","Jobs",msg => this.jobsHeader = msg);
      CwmsTranslate.subscribeMessage("jicalibration.certs","Certificates",msg => this.certsHeader = msg);
      CwmsTranslate.subscribeMessage("notes","Notes",msg => this.notesHeader = msg);

    }

    static get styles(){
      return [...super.styles,tooltipStyling ];
    }

    prepareUrl(id){
        const url = new URL("viewinstrument.htm",MetaDataObtainer.base);
        url.searchParams.append("plantid",id);
        return url;
    }


    get objectName(){
        return `${this.displayPlantId?this.plantid:''} ${this.displayModel?this.modelName:''}`;
    }

    get targetURL(){
        return this.prepareUrl(this.plantid)
    }

    async requestTooltipBody(){
        const url = new URL("tooltip/instrument.json",MetaDataObtainer.base);
        url.searchParams.append("plantId",this.plantid);
        const res = await fetch(url);
      const res_1 = await (res.ok ? res.json() : Promise.reject(res.statusText));
      const res_2 = await (res_1.right?res_1.value:Promise.reject(res_1.value));
      return tooltipBodyTemplate({
        mainHeader:this.mainHeader,
        summaryHeader:this.summaryHeader,
        out: res_2,
        threadHeader:this.threadHeader,
        jobsHeader:this.jobsHeader,
        certsHeader:this.certsHeader,
        notesHeader:this.notesHeader
      });
    }
}

customElements.define("cwms-instrument-link-and-tooltip",CwsmInstrumentLinkAndTooltip);