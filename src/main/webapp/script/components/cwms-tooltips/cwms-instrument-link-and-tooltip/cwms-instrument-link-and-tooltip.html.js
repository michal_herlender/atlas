import { html } from '../../../thirdparty/lit-element/lit-element.js'
import { stylesMixin } from '../../tools/applicationStylesMixinTemplate.js';
import "../../cwms-translate/cwms-translate.js";
import { jobCostingLinkGenerator, jobItemLinkGenerator, jobLinkGenerator, jobItemCertificatesLinkGenerator } from '../../../tools/UrlGenerator.js';
import { dateFormatDirective, dateFormatWihtAltTextDirective, currencyFormatDirective, calibrationIntervalTemplate } from '../../tools/formatingDirectives.js';

export const tooltipBodyTemplate = (context) => html`
  <nav is="cwms-dynamic-tabs-holder">
    <cwms-dynamic-tab tabName=${context.mainHeader}>
      <nav is="cwms-dynamic-tabs-holder" variant="red">
        <cwms-dynamic-tab tabName=${context.summaryHeader}>
          ${instrumentSummaryTemplate(context?.out?.summary)}
        </cwms-dynamic-tab>
        ${context.out.useThreads? html`<cwms-dynamic-tab tabName=${context.threadHeader}>
          ${threadSummaryTemplate(context?.out?.summary)}
        </cwms-dynamic-tab>`:html``}
        <cwms-dynamic-tab tabName=${`${context.jobsHeader} (${context?.out?.jobItems?.length ?? 0}) `} >
          ${jobsTemplate(context?.out?.jobItems)}
        </cwms-dynamic-tab>
        <cwms-dynamic-tab tabName=${`${context.certsHeader} (${context?.out?.certificates?.length ?? 0})`}>
          ${certsTemplate(context?.out?.certificates)}
        </cwms-dynamic-tab>
        <cwms-dynamic-tab tabName=${`${context.notesHeader} (${context?.out?.notes?.length ?? 0})`}>
          ${notesTemplate(context?.out?.notes)}
        </cwms-dynamic-tab>
      </nav>
    </cwms-dynamic-tab>
  </nav>
`;


const instrumentSummaryTemplate = context => html`
${stylesMixin}
<fieldset class="newtooltip">
    <ol>
        <li>
            <label><cwms-translate code="barcode">Barcode</cwms-translate>:</label>
            <span>${context.plantId}</span>
        </li>
        <li>
            <label><cwms-translate code="instmodelname">instrument modelname</cwms-translate>:</label>
            <span>${context.modelName}</span>
        </li>
        <li>
            <label><cwms-translate code="viewinstrument.customerdescription">Customer Description</cwms-translate>:</label>
            <span>${context.customerDescription}</span>
        </li>
        ${context.modelMfrType == "MFR_GENERIC"?html`

        <li>
            <label><cwms-translate code="mfr">Mfr</cwms-translate>:</label>
            <span>${context.mfrName}</span>
        </li>
        <li>
            <label><cwms-translate code="model">Model</cwms-translate>:</label>
            <span>${context.model}</span>
        </li>
        <li>
            <label><cwms-translate code="editinstr.defaultservicetype">Default Service Type</cwms-translate>:</label>
            <span>${context.defaultServiceType}</span>
        </li>
        `:html``}

         <li>
            <label><cwms-translate code="company">Company name</cwms-translate>:</label>
            <span>${context.companyName}</span>
        </li>  
        <li>
            <label><cwms-translate code="owner">Owner</cwms-translate>:</label>
            <span>${context.contactName}</span>
        </li> 
        <li>
            <label><cwms-translate code="location">Location</cwms-translate>:</label>
            <span>${context.address}</span>
        </li>  
        <li>
            <label><cwms-translate code="plantno">Plant Number</cwms-translate>:</label>
            <span>${context.plantNo}</span>
        </li> 
        <li>
            <label><cwms-translate code="serialno">Serial Number</cwms-translate>:</label>
            <span>${context.serialNo}</span>
        </li> 
        <li>
            <label><cwms-translate code="instrumenttooltip.calfreq">Cal Freq</cwms-translate>:</label>
            <span>${calibrationIntervalTemplate(context.calFrequency,context.calFrequencyUnit)}</span>

        </li> 
        <li>
            <label><cwms-translate code="nextcal">Next Calibration</cwms-translate>:</label>
            <span>${dateFormatDirective(context.nextCalibrationDate)}</span>
        </li> 
     </ol>
</fieldset>
`;

const threadSummaryTemplate = context => html`
${stylesMixin}
<fieldset class="newtooltip">
    <ol>
        <li>
            <label><cwms-translate code="instrumenttooltip.size">Thread size</cwms-translate>:</label>
            <span>${context.threadSize ?? "-"}</span>
        </li>
        <li>
            <label><cwms-translate code="type">Type</cwms-translate>:</label>
            <span>${context.threadType ?? "-"}</span>
        </li>
        <li>
            <label><cwms-translate code="thread.wire">Thread wire</cwms-translate>:</label>
            <span>${context.threadWire ?? "-"}</span>
        </li>

        <li>
            <label><cwms-translate code="cylindricalstandard.cylindricalstandard">Cylindrical Standard</cwms-translate>:</label>
            <span>${context.threadStandardId ?? "-"}</span>
        </li>
        <li>
            <label><cwms-translate code="thread.threaduom">Thread UoM</cwms-translate>:</label>
            <span>${context.threadUom ?? "-"}</span>
        </li>
     </ol>
</fieldset>
`;



const jobsTemplate = context => html`
${stylesMixin}
    <table class="cwms-tooltip default2 instTooltipJob" summary="This table displays job information for this instrument">
				<thead>
					<tr>
						<td colspan="10">
							<cwms-translate code="jobs">Jobs</cwms-translate> (${context?.length ?? 0})
						</td>
					</tr>
					<tr>
						<th class="tti_jobno" scope="col">
							<cwms-translate code="jobno">Job no</cwms-translate>
						</th>
						<th class="tti_item" scope="col">
							<cwms-translate code="item">Item</cwms-translate>
						</th>
						<th class="tti_caltype" scope="col">
							<cwms-translate code="caltype">Caltype</cwms-translate>
						</th>
						<th class="tti_turn" scope="col">
							<cwms-translate code="instrumenttooltip.turn">Turn</cwms-translate>
						</th>
						<th class="tti_datein" scope="col">
							<cwms-translate code="instrumenttooltip.datein">Date in</cwms-translate>
						</th>
						<th class="tti_datecomp" scope="col">
							<cwms-translate code="instrumenttooltip.datecomp">Date complete</cwms-translate>
						</th>
						<th class="tti_caljobreviewcost" scope="col">
							<cwms-translate code="viewinstrument.caljobreviewcost">Calibration pricing Price </cwms-translate>
						</th>
						<th class="tti_calcost" scope="col">
							<cwms-translate code="viewinstrument.calcost">Calibration cost</cwms-translate>
						</th>
						<th class="tti_calinvoicedcost" scope="col">
							<cwms-translate code="viewinstrument.calinvoicedcost">Cal invoice cost</cwms-translate>
						</th>
						<th class="tti_repcost" scope="col">
							<cwms-translate code="instrumenttooltip.repcost">Repair cost</cwms-translate>
						</th>
					</tr>
				</thead>
        <tfoot>
            <tr>
                <td colspan="10">&nbsp; </td>
            </tr>
        </tfoot>
				<tbody>
                    ${context.map(ji => html`
                        <tr>
                            <td class="tti_job">
										<a href="${jobLinkGenerator(ji.jobId).toString()}" target="_blank" class="mainlink">
											${ji.jobNo}
										</a></td>
                            <td class="tti_item">
                            <a href="${jobItemLinkGenerator(ji.jobtemsId).toString()}" target="_blank" class="mainlink">${ji.itemNo}</a>
                            </td>
                            <td class="tti_caltype">${ji.serviceTypeShortName}</td>
                            <td class="tti_turn">${ji.turn}</td>
                            <td class="tti_datein">${dateFormatWihtAltTextDirective(ji.dateIn)}</td>
                            <td class="tti_datecomp">${dateFormatWihtAltTextDirective(ji.dateComplete)}</td>
                            <td class="tti_caljobreviewcost">
                            ${constingFormatTemplate(ji.jobCostingId ,ji.calibrationJobReviewCost ,ji.currencyCode)}
                            </td>
                            <td class="tti_calcost">${constingFormatTemplate(ji.jobCostingId,ji.calibrationCost,ji.currencyCode)}</td>
                            <td class="tti_calinvoicedcost">${constingFormatTemplate(ji.jobCostingId, ji.calibrationInvoiceCost,ji.currencyCode)}</td>
                            <td class="tti_repcost">${constingFormatTemplate(ji.jobCostingId, ji.repairCost,ji.currencyCode)}</td>
                        </tr>
                    `)}
				</tbody>
			</table>
`;

const certsTemplate = context => html`
${stylesMixin}
<table class="default2 cwms-tooltip" id="instTooltipCert" summary="This table displays certificate information for this instrument">
				<thead>
					<tr>
						<td colspan="5">
							<cwms-translate code="instrumenttooltip.certs">Certs</cwms-translate>
						</td>
					</tr>
					<tr>
						<th class="tti_cert" scope="col">
							<cwms-translate code="viewinstrument.certno">Certificate Number</cwms-translate>
						</th>
						<th class="tti_job" scope="col">
							<cwms-translate code="jobno">Job Number</cwms-translate>
						</th>
						<th class="tti_item" scope="col">
							<cwms-translate code="itemno">Item Number</cwms-translate>
						</th>
						<th class="tti_caldate" scope="col">
							<cwms-translate code="caldate">Calibration Date</cwms-translate>
						</th>
						<th class="tti_certdate" scope="col">
							<cwms-translate code="viewinstrument.certdate">Certificate Date</cwms-translate>
						</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="5">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
                    ${context.map(ci => html`
							<tr>
								<td>
											${ci.certificateNo}
								</td>
								<td>
									<a href="${jobLinkGenerator(ci.jobId).toString()}" target="_blank" class="mainlink">
										${ci.jobNo}
									</a>
								</td>
								<td class="center">
									<a href="${jobItemCertificatesLinkGenerator(ci.jobItemId).toString()}" target="_blank" class="mainlink">
										${ci.itemNo}
									</a>
								</td>
								<td class="center">
									${dateFormatDirective(ci.calibrationDate)}
								</td>
								<td class="center">
									${dateFormatDirective(ci.certificateDate)}
								</td>
							</tr>`)}
				</tbody>
			</table>
`;

const notesTemplate = context => html`
${stylesMixin}
<table class="default2 cwms-tooltip" id="instTooltipNotes" summary="This table displays all notes for this instrument">
				<thead>
					<tr>
						<td>
							<cwms-translate code="notes">Notes</cwms-translate> (${context?.length ?? 0})
						</td>
					</tr>
					<tr>
						<th>
							<cwms-translate code="instrumenttooltip.labelnote">Label/Notes</cwms-translate>
						</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
                    ${!context || context.length == 0 ? html`
							<tr>
								<td class="bold center">
									<cwms-translate code="instrumenttooltip.nonotes">There are no notes for this instrument</cwms-translate>
								</td>
                            </tr>`:context.map(ni => html`
								<tr>
									<td>
										<span class="bold">${ni.label}</span><br />
										${ni.note}
									</td>
								</tr>
                            `)}
				</tbody>
			</table>
`;

const constingFormatTemplate = (costingId,ammount,currencyCode) =>  {
    if(!ammount) return html``;
    if(!costingId)  return html`${currencyFormatDirective(ammount,currencyCode)}`;
    return html`
        <a href="${jobCostingLinkGenerator(costingId).toString()}" target="_blank" class="mainlink">
        ${currencyFormatDirective(ammount,currencyCode)}
    </a>
    `;
} 

