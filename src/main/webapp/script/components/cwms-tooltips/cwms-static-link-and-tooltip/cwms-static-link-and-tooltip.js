import { html } from "../../../thirdparty/lit-element/lit-element.js";
import { CwmsGenericLinkAndTooltip } from "../cwms-generic-link-and-tooltip/cwms-generic-link-and-tooltip.js";

class CwmsStaticLinkAndToolTip extends CwmsGenericLinkAndTooltip {

    #popUpWidth;

    static get properties(){
        return {...super.properties,
            objectName:{type:String},
            popUpWidth:{type:String}
        }
    }

    constructor(){
        super();
        this.withMouse = false;
    }

    get popUpWidth(){
        return this.#popUpWidth;
    }

    set popUpWidth(width){
        this.#popUpWidth = width;
        this.style.setProperty("--tooltip-width",width);
    }

    requestTooltipBody(){
    return Promise.resolve(html`<slot></slot>`)
    }
}



customElements.define("cwms-static-link-and-tooltip",CwmsStaticLinkAndToolTip);