import { html } from "../../../thirdparty/lit-element/lit-element.js";
import "../../cwms-translate/cwms-translate.js"
import { stylesMixin } from "../../tools/applicationStylesMixinTemplate.js";
import { MetaDataObtainer } from "../../../tools/metadataObtainer.js";

export const tooltipBodyTemplate = (context) => html`
<nav is="cwms-dynamic-tabs-holder">
    <cwms-dynamic-tab tabName=${context.companyHeader}>
      <nav is="cwms-dynamic-tabs-holder" variant="red">
        <cwms-dynamic-tab tabName=${context.summaryHeader}>
          ${companySummaryTemplate(context)}
        </cwms-dynamic-tab>
        <cwms-dynamic-tab tabName=${context.subdivHeader}>
            ${companySubdivisionsTemplate(context)}
        </cwms-dynamic-tab>
       </nav>
    </cwms-dynamic-tab>
</nav>
`;

const companySummaryTemplate = (context) => html`
${stylesMixin}
<fieldset class="newtooltip">
    <ol>
        <li>
            <label><cwms-translate code="name">Name t</cwms-translate>:</label>
            <span>${context.company.coname}</span>
        </li>
        <li>
            <label><cwms-translate code="instruments">Instruments</cwms-translate>:</label>
            <span>
            ${linkToInstrumentsTemplate(context.company.coid)}
            </span>
        </li>
        <li>
            <label><cwms-translate code="corole">Corole</cwms-translate>:</label>
            <span>${context.company.companyRole}</span>
        </li>
        <li>
            <label><cwms-translate code="active">Active</cwms-translate>:</label>
            <span>${context.company.active?html`<cwms-translate code="active">Active</cwms-translate>`
            :html`<cwms-translate code="company.inactive">Inactive</cwms-translate>` }</span>
        </li>
        <li>
            <label><cwms-translate code="company.onstop">On-Stop</cwms-translate>:</label>
            <span>${context.company.trusted?html`<cwms-translate code="company.onstop">On-Stop</cwms-translate>`:
            html`<cwms-translate code="company.trusted">Trusted</cwms-translate>`
            }</span>
        </li>
        </ol>
</fieldset>
`;


const linkToInstrumentsTemplate = coid => {
    const url = new URL("viewcompanyinstrument.html",MetaDataObtainer.base);
    url.searchParams.append("coid",coid);
    return html`<a class="mainlink" .href=${url.toString()} target="_blank"><cwms-translate code="instruments">instruments</cwms-translate></a>`
};

const linkToSubdivTemplate = ({key:subdivId,value:subdivName}) => {
    const url = new URL("viewsub.html",MetaDataObtainer.base);
    url.searchParams.append("subdivid",subdivId);
    return html`<a class="mainlink" .href=${url.toString()} target="_blank">${subdivName}</a>`
};
const companySubdivisionsTemplate = context => html`
${stylesMixin}
<fieldset class="newtooltip">
    <ol>
        ${context.company.subDivs.map(subdivRowTemplate)}
    </ol>
</fieldset>
`;

const subdivRowTemplate = (subdiv,idx) => html`
    <li>
        <span>${linkToSubdivTemplate(subdiv)}</span>
        </li>
`;