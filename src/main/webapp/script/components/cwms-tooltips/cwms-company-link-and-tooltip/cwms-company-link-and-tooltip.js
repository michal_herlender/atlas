import { MetaDataObtainer } from "../../../tools/metadataObtainer.js";
import { CwmsGenericLinkAndTooltip } from "../cwms-generic-link-and-tooltip/cwms-generic-link-and-tooltip.js";
import { tooltipBodyTemplate } from "./cwms-company-link-and-tooltip.html.js";
import CwmsTranslate from "../../cwms-translate/cwms-translate.js";

class CwsmCompanyLinkAndTooltip extends CwmsGenericLinkAndTooltip {

    static get properties(){
        return {
            ...CwmsGenericLinkAndTooltip.properties,
            companyId:{type:Number},
                companyName:{type:String},
            companyHeader:{type:String},
            summaryHeader:{type:String},
            subdivHeader:{type:String},
    };
    }

    constructor(){
      super();
      CwmsTranslate.subscribeMessage("company","company",msg => this.companyHeader = msg);
      CwmsTranslate.subscribeMessage("summary","summary",msg => this.summaryHeader = msg);
      CwmsTranslate.subscribeMessage("company.subdivs","subdiv",msg => this.subdivHeader = msg);

    }

    prepareUrl(companyId){
        const url = new URL("viewcomp.htm",MetaDataObtainer.base);
        url.searchParams.append("coid",companyId);
        return url;
    }

    requestTooltipBody(){
        const url = new URL("company.json",MetaDataObtainer.base);
        url.searchParams.append("coid",this.companyId);
        return fetch(url).then(res =>res.ok?res.json():Promise.reject(res.statusText))
        .then(res => tooltipBodyTemplate({companyHeader:this.companyHeader,
          subdivHeader:this.subdivHeader,
          summaryHeader:this.summaryHeader,
          company:res}))
    }

    get objectName(){
        return this.companyName;
    }

    get targetURL(){
       return this.prepareUrl(this.companyId);
    }

}

customElements.define("cwms-company-link-and-tooltip",CwsmCompanyLinkAndTooltip);