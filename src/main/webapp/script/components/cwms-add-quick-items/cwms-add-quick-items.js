import {html, LitElement} from "../../thirdparty/lit-element/lit-element.js";
import {styling} from "./cwms-add-quick-items.css.js";
import {htmlTemplate} from "./cwms-add-quick-items.html.js";
import {urlWithParamsGenerator} from "../../tools/UrlGenerator.js";
import {MetaDataObtainer} from "../../tools/metadataObtainer.js";

export const PROCEDURE_ID = "procId";
export const PROCEDURE_REFERENCE = "procReference";

class CwmsAddQuickItems extends LitElement {

    static get dataURL() {
        return "quickAddItemsValid.json"
    }

    static get postURL() {
        return "addquickitems.json"
    }

    static get instrumentURL() {
        return "viewinstrument.htm"
    }

    static get properties() {
        return {
            itemData: { type: Array },
            serviceTypes : { type: Array },
            bookingInAddresses :  { type: Array },
            jobDefaultCalTypeId : { type : Number },
            defaultTurnAround : { type : Number },
            defAddrId : { type : Number },
            loading : { type : Boolean },
            errorMessage : {type: String},
            jobid: { attribute: 'job-id' },
            coid: { attribute: 'company-id' }
        }
    }

    static getStyles() {
        return styling;
    }

    constructor() {
        super();
        this.itemData = [];
        this.errorMessage = '';
        this.loading = false;
    } 

    render() {
        return htmlTemplate(this);
    }


    /**
     * Add item from key down
     */ 
    addItemFromEnter(e){
        if (e.key === 'Enter')
            this.addItem(e.currentTarget.value);
    }
        
    /**
     * Add item from 'Add' button
     */ 
    addItemFromButton(){
        if(this.shadowRoot.getElementById("barcodeEntred"))
            this.addItem(this.shadowRoot.getElementById("barcodeEntred").value);       
    }

    addItem(barcode) {   

        // empty error messages
        this.errorMessage = '';

        let fetchData = true;  
        if (barcode){
            //check if item is duplicated in quick list 
            for(let i = 0; i< this.itemData.length; i++){
                if (this.itemData[i].plantid === parseInt(barcode)) {
                    document.querySelector('#addQuickItem').show();
                    this.errorMessage = '';
                    fetchData = false;
                    break;
                }         
            }
            if(fetchData) {

                this.loading = true;

                //fetch instrument data test if barcode exist deja
                fetch(urlWithParamsGenerator(CwmsAddQuickItems.dataURL,{plantid:barcode,jobid:this.jobid, coid:this.coid})) 
                    .then(res => res.ok ? res.json():Promise.reject(res.statusText))
                    .then(json => {
                        if (json.success === true) {

                            // add instrument
                            this.itemData.push(json.results[0].instrument);
                            // initialiaze data
                            if (this.serviceTypes === undefined)
                                this.serviceTypes = json.results[0].serviceTypes;
                            if (this.bookingInAddresses === undefined)
                                this.bookingInAddresses = json.results[0].bookingInAddresses;
                            if (this.jobDefaultCalTypeId === undefined)
                                this.jobDefaultCalTypeId = json.results[0].jobDefaultCalTypeId;
                            if (this.defAddrId === undefined)
                                this.defAddrId = json.results[0].defAddrId;
                            if (this.defaultTurnAround === undefined) {
                                this.defaultTurnAround = json.results[0].defaultTurnAround;
                            }

                        // request render
                        this.requestUpdate();
                        this.errorMessage = '';
                    
                    } else if(json.errors != null){
                        // message related to instrument entred (eg. This instrument is not owned by the company)
                        this.errorMessage = json.errors[0].message;
                    }
                    else    
                        // message related to compile errors (eg. Failed to convert value of type 'java.lang.String')
                        this.errorMessage = json.message;

                    this.loading = false;

                });
                
            }
         }
    } 

    submit() {

        // check if turnaround values is not empty
        if (Array.from(this.shadowRoot.querySelectorAll('.turn')).filter(el => el.value === '').length > 0) {
            this.errorMessage = html`<cwms-translate code="viewjob.turnaround" >Turnaround is required</cwms-translate>`;
            return;
        }

        //disable the submit button
        this.shadowRoot.getElementById("submitButton").disabled = true;

        let form = this.shadowRoot.getElementById("addquickitemsform");
        const formData = new FormData(form);
        // fetch data 
        fetch(CwmsAddQuickItems.postURL,{ method : 'POST', body: formData, 
            credentials:"same-origin",
            headers:MetaDataObtainer.csrfHeader
        })
            .then(res => res.ok?res.json():Promise.reject(res.statusText))
            .then(value => {
                if(this.shadowRoot.getElementById("autoPrintLabelCheckBoxId").checked){
                    return fetch(urlWithParamsGenerator("dymoselectedjobitem.json",{jobItemIds:value,printJobItemCount:false}))
             .then(res => res.ok ? res.json():Promise.reject(res.statusText))
            .then(json => {
                document.getElementById("printProgress").show();
                printDymoLabels(json, null, null, ()=>{window.location="viewjob.htm?jobid="+this.jobid;});
            })
                }
                else {
                    window.location=urlWithParamsGenerator("viewjob.htm",{jobid:this.jobid})
                }
            })
            .catch(alert);
    }

    updateField(fieldName,item){
        return value => {
            this.itemData = [...this.itemData.map(i => i.plantid === item.plantid ? {...i, [fieldName]: value} : i)];
        };
    }

    removeQuickItem (e){
        let index = e.currentTarget.getAttribute('data-index');
        this.itemData.splice(index, 1)
        this.requestUpdate();
    }

}

window.customElements.define('cwms-add-quick-items', CwmsAddQuickItems);