import { html } from "../../thirdparty/lit-element/lit-element.js";
import {repeat} from "../../thirdparty/lit-html/directives/repeat.js";
import { directive } from "../../thirdparty/lit-html/lit-html.js";
import { PROCEDURE_REFERENCE } from "../cwms-duplicates/common/tools/constants.js";
import "../search-plugins/cwms-modelcode-search/cwms-modelcode-search.js";
import "../search-plugins/cwms-capability-search/cwms-capability-search.js";
import { PROCEDURE_ID } from "./cwms-add-quick-items.js";

export let htmlTemplate = (context) => html `

<link rel="stylesheet" href="styles/structure/elements/table.css">
<link rel="stylesheet" href="styles/theme/theme.css">
<link rel="stylesheet" href="styles/text/text.css">
<link rel="stylesheet" href="styles/structure/structure.css">

    
    <div id="quickItemContainer">
        <fieldset id="quickAnchor">
            ${ context.errorMessage !== "" ? 
            html`<div class="warningBox1">
			    <div class="warningBox2">
				    <div class="warningBox3">${context.errorMessage}</div>
			    </div>
	        </div>` : ''      
            } 

            <legend>
                <cwms-translate code='viewjob.quickitems'>Quick Items</cwms-translate>
		    </legend>
            <ol>
                <li>
                    <label><cwms-translate code='viewjob.itembarc'>Item Barcode</cwms-translate>:</label>
                    <input id="barcodeEntred" type="text" @keydown="${context.addItemFromEnter}"/>
                    <button type="button" @click="${context.addItemFromButton}">
                        <cwms-translate code='add'>Add</cwms-translate>
                    </button>
                </li>
            </ol>
            <div>

                ${ context.loading ? 
                    html`<center>
                            <img src="img/icons/ajax-loader.gif" width="31" height="31" alt="" />
                        </center>` 
                    : '' 
                }

                ${ context.itemData.length === 0  ? '' :
                    html`<form action="addquickitems.json" id="addquickitemsform" method="POST">
                    <input type="hidden" name="jobId" value="${context.jobid}">
                    <table class="default4 first" id="quickItemTable">
                        <thead>
                            <tr id="confirmLocation">
                                <td colspan="8">
                                    <span>
                                        <cwms-translate code='newjobitemsearch.confirmloc'>Confirm Location:</cwms-translate>
                                    </span>
                                    <select name="bookingInAddrId" id="bookingIn">
                                        ${ repeat(context.bookingInAddresses, (addr, index)=>
                                            html`<option ?selected="${addr.key == context.defAddrId}" 
                                                value="${addr.key}">${addr.value}</option>`
                                        )}
                                    </select>
                                </td>
                            </tr>
                            <tr table-id="existTable" id="confirmLocation">
                                <th class="barcode" >
                                    <cwms-translate code='jobresults.barcode'>Barcode</cwms-translate>
                                </th>
                                <th class="inst" colspan="2">
                                    <cwms-translate code='instrument'>Instrument</cwms-translate>
                                </th>
                                <th colspan="2">
                                    <cwms-translate code='procedure'>Procedure</cwms-translate>
                                </th>
                                <th colspan="2">
                                    <cwms-translate code='workinstruction'>Work Instruction</cwms-translate>
                                </th>
                                <th class="del" class="center">
                                    <cwms-translate code='delete'>Delete</cwms-translate>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            ${ repeat(context.itemData, (item, index)=>formatRowData(context, item, index))}
                        </tbody>
                    </table>
                    <div>
                     <div id="autoPrintLabelDiv" style="display: flex; flex-direction: row-reverse;">
                        <label>
                            <cwms-translate code='edituserpreferences.autoprintlabel'></cwms-translate>
                        </label>
                        <input type="checkbox" value="auto print" id="autoPrintLabelCheckBoxId"/>
                     </div>
                    </div>
                    <div id="submitQuickItems" class="center">
                      <button type="button" id="submitButton" @click=${context.submit} >
                        <cwms-translate code="submit" >Submit</cwms-translate>
                      </button>
                    </div>
                </form>`} 
                </li>
            </div>
         </fieldset>
    </div>
`
const formatRowData = directive((context, item, index) => (part) => {
    part.setValue(html`
              <tr>
                <td class="barcode"><a href="${context.constructor.instrumentURL}?plantid=${item.plantid}&amp;ajax=instrument&amp;width=500" class="jconTip mainlink">${item.plantid}</a></td>
                <td class="inst" colspan="2">
                    ${item.instModel}
                    <input type="hidden" name="rows[${index}].barecode" value="${item.plantid}"/>
                </td>
                <td colspan="2">

                    <cwms-capability-search .subdivId=${context.subdivId??9} 
                    .defaultvalue=${item.procReference}
                    @selected=${e => {
                        context.updateField(PROCEDURE_ID,item)(e.detail?.id);
                        context.updateField(PROCEDURE_REFERENCE, item)(e.detail?.reference);
                    }}
                    ></cwms-capability-search>
                    <input type="hidden" name="${`rows[${index}].procId`}" .value=${item.procId} />
                </td>
                <td colspan="2">
                    <div>
						<cwms-modelcode-search modelid="${item.instModelId}" name="rows[${index}].workIstructionId" 
						defaultvalue ="${item.defaultWorkInstructionTitle}" value="${item.defaultWorkInstructionId}" ></cwms-modelcode-search>
					</div>
                </td>
                <td class="del">
                    <img src="img/icons/delete.png" class="delBtn" @click=${context.removeQuickItem } data-index="${index}"
                        title="Delete Quick Item" alt="" width="16" height="16" />
                </td>
              </tr>
              <tr>
                <td rowspan="2">&nbsp;</td>
                <td class="title1 cost bold"><cwms-translate code='viewjob.serialNoLabel'>Serial No:</cwms-translate></td>
                <td class="value1">${item.serialNo}</td>
                <td class="title2 cost bold"><cwms-translate code='viewjob.calTypeLabel'>ServiceType:</cwms-translate></td>
                <td class="value2">
                    <select class="calselect" name="rows[${index}].serviceTypeId">
                            ${ repeat(context.serviceTypes, (st)=>
                                html`<option ?selected="${st.key == item.serviceTypeSelected}" 
                                    value="${st.key}">${st.value}</option>`
                            )}
                      
					</select> 
                    <span style="color:green; font-size: 12px;" class="vis">
                        ${item.isInstrumentCalTypeCompatibleWithJobType ? 
                            html`<cwms-translate code='viewjob.quickitems.defaultservicetype.inst'></cwms-translate>`
                            : 
                            html`<cwms-translate code='viewjob.quickitems.defaultservicetype.job'></cwms-translate>`
                        }
                    </span>
                </td>
                <td class="title3 cost bold">
                    <cwms-translate code='viewjob.turnaroundLabel'>Turnaround:</cwms-translate>
                </td>
                <td class="value3">
                    <input type="number" class="turn" name="rows[${index}].turn" min="1" max="365" value="${context.defaultTurnAround}"/>
                </td>
                <td rowspan="2">&nbsp;</td>
              </tr>
              <tr>
                <td class="cost bold">
                     <cwms-translate code='viewjob.plantNoLabel'>Plant No:</cwms-translate>
                </td>
                <td>${item.plantNo}</td>
                <td class="cost bold">
                    <cwms-translate code='viewjob.reqCleaning'>Req Cleaning</cwms-translate>
                </td>
                <td colspan="3">
                    <input type="checkbox" class="cleanbox" name="rows[${index}].clean"/>
                </td>
              </tr>
    `)});