import { LitElement } from "../../thirdparty/lit-element/lit-element.js";
import { htmlTemplate } from "./cwms-po-table.html.js";
import { styling } from "./cwms-po-table.css.js";
import {} from "../cwms-translate/cwms-translate.js";
import '../../thirdparty/@vaadin/vaadin-grid/all-imports.js';

class CwmsPurchaseOrderTable extends LitElement {

    static get purchaseOrderURL() { return "purchaseOrders.json?jobId=_jobId&personId=_personId"; }
    static get createPoURL() { return "createPoOnJob.json?jobId=_jobId&poNumber=_poNumber&comment=_comment"; }
    static get editPoURL() { return "editPoOnJob.json?poId=_poId&poNumber=_poNumber&comment=_comment"; }
    static get updateJobDefaultPoURL() { return "updateJobDefaultPO.json?poId=_poId&jobId=_jobId"; }
    static get updateJobDefaultBpoURL() { return "updateJobDefaultBPO.json?poId=_poId&jobId=_jobId"; }
    static get poItemsURL() { return "purchaseOrderItems.json?poId=_poId"; }
    static get bpoItemsURL() { return "blanketPurchaseOrderItems.json?poId=_poId&jobId=_jobId"; }
    static get deactivatePoURL() { return "deactivatePO.json?poId=_poId"; }
    static get removeBpoFromJobURL() { return "removeJobBPO.json?bpoid=_poId&jobid=_jobId"; }

    static get properties() {
        return {
            // Attributes
            jobId: { attribute: 'job-id' },
            jobNo: { attribute: 'job-no' },
            personId: { attribute: 'person-id' },
            currency: { attribute: 'currency' },
            // Data
            purchaseOrders: { type: Array },
            purchaseOrderDetails: { type: Map },
            purchaseOrderItemsMap: { type: Map },
            purchaseOrderExpenseItemsMap: { type: Map },
            jobDefaultPO: { type: Number },
            blanketPurchaseOrders: { type: Array },
            blanketPurchaseOrderDetails: { type: Map },
            blanketPurchaseOrderItemsMap: { type: Map },
            blanketPurchaseOrderExpenseItemsMap: { type: Map },
            jobDefaultBPO: { type: Number },
            availableBPOs: { type: Array },
            // User Rights
            hasAuthToCreatePO: { type: Boolean },
            hasAuthToAddRemoveItems: { type: Boolean },
            hasAuthToEditPO: { type: Boolean },
            hasAuthToDeactivatePO: { type: Boolean },
            hasAuthToRemoveBPO: { type: Boolean },
            // Messages
            tableSummary: { type: String },
            imgPODefault: { type: String },
            imgPO: { type: String },
            imgPODeactive: { type: String },
            viewItems: { type: String },
            hideItems: { type: String },
            imgRemoveBPOFromJob: { type: String }
        }
    }

    static get styles() {
            return styling(this);
    }

    constructor() {
        super();
        CwmsTranslate.subscribeMessage("viewjob.tabledisplaysallclientpo", "This table displays all client purchase orders", (m) => this.tableSummary = m);
        CwmsTranslate.subscribeMessage("viewjob.imgpodefault", "Default PO", (m) => this.imgPODefault = m);
        CwmsTranslate.subscribeMessage("viewjob.imgpo", "Make PO Default", (m) => this.imgPO = m);
        CwmsTranslate.subscribeMessage("viewjob.imgpodeactive", "PO Deactivated By", (m) => this.imgPODeactive = m);
        CwmsTranslate.subscribeMessage("viewjob.viewitems", "View Items", (m) => this.viewItems = m);
        CwmsTranslate.subscribeMessage("viewjob.hideitems", "Hide Items", (m) => this.hideItems = m);
        CwmsTranslate.subscribeMessage("viewjob.imgrembpofromjob", "Remove BPO from job", (m) => this.imgRemoveBPOFromJob = m);
    }

    firstUpdated() {
        var url = CwmsPurchaseOrderTable.purchaseOrderURL.replace("_jobId", this.jobId);
        url = url.replace("_personId", this.personId);
        fetch(url)
        .then(res => {
            return res.json();
        })
        .then(dto => {
            this.purchaseOrders = dto.pos;
            this.purchaseOrderDetails = new Map();
            this.purchaseOrderItemsMap = new Map();
            this.purchaseOrderExpenseItemsMap = new Map();
            this.jobDefaultPO = dto.jobDefaultPO;
            this.blanketPurchaseOrders = dto.bpos;
            this.blanketPurchaseOrderDetails = new Map();
            this.blanketPurchaseOrderItemsMap = new Map();
            this.blanketPurchaseOrderExpenseItemsMap = new Map();
            this.jobDefaultBPO = dto.jobDefaultBPO;
            this.hasAuthToCreatePO = dto.createPO;
            this.hasAuthToAddRemoveItems = dto.addRemoveItems;
            this.hasAuthToEditPO = dto.editPO;
            this.hasAuthToDeactivatePO = dto.deactivatePO;
            this.hasAuthToRemoveBPO = dto.removeBPO;
            this.availableBPOs = dto.availableBPOs;
        });
    }

    render() {
        return htmlTemplate(this);
    }

    createPurchaseOrder(poNumber, comment) {
        var url = CwmsPurchaseOrderTable.createPoURL.replace("_jobId", this.jobId);
        url = url.replace("_poNumber", poNumber);
        url = url.replace("_comment", comment);
        const csrfToken = document.querySelector("meta[name='_csrf']").getAttribute("content");
        fetch(url, {
            method: 'post',
			headers: {
                "X-CSRF-TOKEN": csrfToken
            }
        }).then(res => {
            return res.json();
        }).then(newPO => {
            this.purchaseOrders.push(newPO);
            this.update();
        });
    }

    editPurchaseOrder(poId, poNumber, comment) {
        var url = CwmsPurchaseOrderTable.editPoURL.replace("_poId", poId);
        url = url.replace("_poNumber", poNumber);
        url = url.replace("_comment", comment);
        const csrfToken = document.querySelector("meta[name='_csrf']").getAttribute("content");
        fetch(url, {
            method: 'PUT',
			headers: {
                "X-CSRF-TOKEN": csrfToken
            }
        }).then(success => {
            this.purchaseOrders.forEach(po => {
                if(po.poId == poId) {
                    po.poNumber = poNumber;
                    po.comment = comment;
                }
            });
            this.update();
        });
    }

    updateJobDefaultPO(poId) {
        var url = CwmsPurchaseOrderTable.updateJobDefaultPoURL.replace("_poId", poId);
        url = url.replace("_jobId", this.jobId);
        const csrfToken = document.querySelector("meta[name='_csrf']").getAttribute("content");
        fetch(url, {
            method: 'post',
			headers: {
                "X-CSRF-TOKEN": csrfToken
            }
        }).then(success => {
            if(success) {
                this.purchaseOrders.forEach(po => po.jobDefault = (po.poId == poId));
                this.update();
            }
        });
    }

    updateJobDefaultBPO(poId) {
        var url = CwmsPurchaseOrderTable.updateJobDefaultBpoURL.replace("_poId", poId);
        url = url.replace("_jobId", this.jobId);
        const csrfToken = document.querySelector("meta[name='_csrf']").getAttribute("content");
        fetch(url, {
            method: 'post',
			headers: {
                "X-CSRF-TOKEN": csrfToken
            }
        }).then(success => {
            if(success) {
                this.blanketPurchaseOrders.forEach(bpo => bpo.defaultForJob = (bpo.poId == poId));
                this.update();
            }
        });
    }


    updatePOItems(e) {
        var poRow = e.target.closest('tr');
        var poId = poRow.getAttribute('po-id');
        if(this.purchaseOrderDetails.has(poId)) {
            this.purchaseOrderDetails.set(poId, !this.purchaseOrderDetails.get(poId));
            this.update();
        }
        else {
            var url = CwmsPurchaseOrderTable.poItemsURL.replace("_poId", poId);
            fetch(url)
            .then(res => {
                return res.json();
            })
            .then(dto => {
                this.purchaseOrderItemsMap.set(poId, dto.jobItems);
                this.purchaseOrderExpenseItemsMap.set(poId, dto.expenseItems);
                this.purchaseOrderDetails.set(poId, true);
                this.update();
            });
        }
    }

    updateBPOItems(e) {
        var bpoRow = e.target.closest('tr');
        var bpoId = bpoRow.getAttribute('bpo-id');
        if(this.blanketPurchaseOrderDetails.has(bpoId)) {
            this.blanketPurchaseOrderDetails.set(bpoId, !this.blanketPurchaseOrderDetails.get(bpoId));
            this.update();
        }
        else {
            this.blanketPurchaseOrderDetails.set(bpoId, true);
            var url = CwmsPurchaseOrderTable.bpoItemsURL.replace('_poId', bpoId);
            url = url.replace('_jobId', this.jobId);
            fetch(url)
            .then(res => {
                return res.json();
            })
            .then(dto => {
                this.blanketPurchaseOrderItemsMap.set(bpoId, dto.jobItems);
                this.blanketPurchaseOrderExpenseItemsMap.set(bpoId, dto.expenseItems);
                this.update();
            });
        }
    }

    addBPOToJob(bpoId) {
        var remainedAvailableBPOs = new Array();
        this.availableBPOs.forEach(bpo => {
            if(bpo.poId == bpoId) this.blanketPurchaseOrders.push(bpo);
            else remainedAvailableBPOs.push(bpo);
        });
        this.availableBPOs = remainedAvailableBPOs;
    }

    deactivatePO(poId) {
        const csrfToken = document.querySelector("meta[name='_csrf']").getAttribute("content");
        var url = CwmsPurchaseOrderTable.deactivatePoURL.replace('_poId', poId);
        fetch(url, {
			method: 'post',
			headers: {
                "X-CSRF-TOKEN": csrfToken
            }
        }).then(success => {
            this.purchaseOrders.forEach(po => {
                if(po.poId == poId) {
                    po.active = false;
                    po.countJobItems = 0;
                    po.countExpenseItems = 0;
                    po.jobDefault = false;
                    this.purchaseOrderDetails.delete(poId.toString());
                    this.purchaseOrderItemsMap.delete(poId.toString());
                    this.purchaseOrderExpenseItemsMap.delete(poId.toString());
                    this.update();
                }
            })
        });
    }

    removeBPOFromJob(e) {
        var bpoRow = e.target.closest('tr');
        var bpoId = bpoRow.getAttribute('bpo-id');
        const csrfToken = document.querySelector("meta[name='_csrf']").getAttribute("content");
        var url = CwmsPurchaseOrderTable.removeBpoFromJobURL.replace('_poId', bpoId);
        url = url.replace('_jobId', this.jobId);
		fetch(url, {
			method: 'post',
			headers: {
                "X-CSRF-TOKEN": csrfToken
            }
        }).then(success => {
            if(success) {
                var remainedBPOs = new Array();
                this.blanketPurchaseOrders.forEach(bpo => {
                    if(bpo.poId != bpoId) remainedBPOs.push(bpo);
                    else {
                        bpo.addedItems = 0;
                        bpo.addedExpenseItems = 0;
                        bpo.defaultForJob = false;
                        this.availableBPOs.push(bpo);
                    }
                });
                this.blanketPurchaseOrders = remainedBPOs;
                this.blanketPurchaseOrderDetails.delete(bpoId.toString());
                this.blanketPurchaseOrderItemsMap.delete(bpoId.toString());
                this.blanketPurchaseOrderExpenseItemsMap.delete(bpoId.toString());
            }
        });
    }
}

window.customElements.define('cwms-po-table', CwmsPurchaseOrderTable);