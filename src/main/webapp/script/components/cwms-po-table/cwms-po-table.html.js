import { html } from "../../thirdparty/lit-element/lit-element.js";
import { repeat } from "../../thirdparty/lit-html/directives/repeat.js";
import { dateFormatDirective, dateFormatFunction } from "../tools/formatingDirectives.js";

export let htmlTemplate = (context) => html`
<link rel="stylesheet" href="styles/structure/elements/table.css">
<link rel="stylesheet" href="styles/theme/theme.css">
<link rel="stylesheet" href="styles/text/text.css">
<table class="default4" id="clientpurchordtable" summary="${context.tableSummary}">
    <thead>
        <tr>
            <td colspan="8">
                <cwms-translate code='viewjob.clientpurchord'>Client Purchase Orders</cwms-translate>
                (<span class="cpoSizeSpan">${(context.purchaseOrders ? context.purchaseOrders.length : 0) + (context.blanketPurchaseOrders ? context.blanketPurchaseOrders.length : 0)}</span>)
                <span class="float-right">
                    ${context.hasAuthToCreatePO ? html`
                        <a href="" class="domthickbox mainlink-float"
                            onClick="event.preventDefault(); addEditPurchaseOrdContent('Create', '${context.jobNo}', '${context.jobId}', '');">
                            <cwms-translate code="viewjob.createpoforjob">Create PO for Job</cwms-translate>
                        </a>
                    ` : html``}
                </span>
            </td>
        </tr>
        ${context.purchaseOrders && context.purchaseOrders.length > 0 ? html`
            <tr>
                <th class="podef" scope="col"><cwms-translate code='viewjob.default'>Default</cwms-translate></th>
                <th class="ponum" scope="col"><cwms-translate code='viewjob.ponumber'>PO number</cwms-translate></th>
                <th colspan="2" class="pocom" scope="col"><cwms-translate code='comment'>Comment</cwms-translate></th>
                <th class="poitems" scope="col"><cwms-translate code='addjob.items'>Item Count</cwms-translate></th>
                <th class="poitems" scope="col">+/- <cwms-translate code='jobitems'>Item Count</cwms-translate></th></th>
                <th class="poedit" scope="col"><cwms-translate code='edit'>Edit</cwms-translate></th>
                <th class="podel" scope="col"><cwms-translate code='viewjob.deactivate'>Deactivate</cwms-translate></th>
            </tr>
        ` : html``}
    </thead>
    <tfoot>
		<tr>
            <td colspan="8">
                ${context.availableBPOs && context.availableBPOs.length == 0 || context.blanketPurchaseOrders && context.blanketPurchaseOrders.length == 0 ? html`` : html`
				<a href="" class="mainlink-float float-right" onclick="event.preventDefault(); viewAvailableBPOs(${context.personId}, ${context.jobId}, '${context.currency}');">
                    <cwms-translate code="addjob.linkbpo">Link job to existing BPO</cwms-translate>
				</a>`}
			</td>
		</tr>
	</tfoot>
    <tbody>
        ${context.purchaseOrders ? html`
            ${repeat(context.purchaseOrders, (po, index) => html`
                <tr po-id="${po.poId}">
                    <td>${po.active ? po.jobDefault ?
                        html`<img src="img/icons/po_default.png" width="24" height="16" id="defaultPOImg"
                            alt="${context.imgPODefault}" title="${context.imgPODefault}"/>` :
                        html`<img src="img/icons/po.png" width="16" height="16"
                            alt="${context.imgPO}" title="${context.imgPO}"
                            @click="${(e) => context.updateJobDefaultPO(po.poId)}"/>` :
                        html`<img src="img/icons/po_deactivate.png" width="16" height="16"
                            alt=${`${CwmsTranslate.getMessage("viewjob.imgpodeactive", "PO Deactivated By")}   ${po.deactivatedBy}  (${dateFormatFunction(po.deactivatedTime)})`}
                            title=${`${CwmsTranslate.getMessage("viewjob.imgpodeactive", "PO Deactivated By")} ${po.deactivatedBy} (${dateFormatFunction(po.deactivatedTime)}`} />`}
                    </td>
                    ${po.active ?
                        html`<td>
                            <span>${po.poNumber}</span>
                            <img src="img/icons/bullet_clipboard.png" width="10" height="10"
                                onclick="copyToClipboard(${po.poNumber});">
                            </td>` :
                        html`<td class="deactivated"><span>${po.poNumber}</span></td>`}
                    ${po.active ?
                        html`<td class="pocom" colspan="2">
                            <span>${po.comment}</span>
                            </td>` :
                        html`<td class="pocom" colspan="2" class="deactivated"><span>${po.comment}</span></td>`}
                    <td class="poitems">${po.countJobItems} / ${po.countExpenseItems}
                        ${po.active && (po.countJobItems + po.countExpenseItems > 0) ? html`
                        <a id="itemsLink${po.poId}">
                            ${context.purchaseOrderDetails.has(po.poId.toString()) && context.purchaseOrderDetails.get(po.poId.toString()) ?
                                html`
                                <img @click="${context.updatePOItems}" src="img/icons/items_hide.png" width="16" height="16"
                                    alt="${context.viewItems}" title="${context.viewItems}"
                                    class="image_inline" />` :
                                html`
                                <img @click="${context.updatePOItems}" src="img/icons/items.png" width="16" height="16"
                                    alt="${context.hideItems}" title="${context.hideItems}"
                                    class="image_inline" />`
                            }
                        </a>` : html``}
                    </td>
                    <td class="poitems">${po.active ? context.hasAuthToAddRemoveItems ?
                        html`<a onclick='addJobItemsToPO(${context.jobId}, ${po.poId});'><img src="img/icons/po_add_remove.png" width="16" height="16" class="img_marg_bot" /></a>` :
                        html`<img src="img/icons/po_add_remove.png" width="16" height="16" class="img_marg_bot" />` : html``}
                    </td>
                    <td class="poedit">${po.active ? context.hasAuthToEditPO ?
                        html`<a onclick="addEditPurchaseOrdContent('Edit', '${context.jobNo}', '${context.jobId}', '${po.poId}');"><img src="img/icons/note_edit.png" height="16" width="16"/></a>` :
                        html`<img src="img/icons/note_edit.png" height="16" width="16"/>` : html``}
                    </td>
                    <td class="podel">${po.active ? context.hasAuthToDeactivatePO ?
                        html`<img @click="${(e) => context.deactivatePO(po.poId)}" src="img/icons/po_deactivate.png" width="16" height="16"/>` :
                        html`<img src="img/icons/po_deactivate.png" width="16" height="16"/>` : html``}
                    </td>
                </tr>
                ${context.purchaseOrderDetails.has(po.poId.toString()) && context.purchaseOrderDetails.get(po.poId.toString()) ? html`
                    <tr>
                        <td colspan="8">
                            <table class="child_table" summary="This table lists all purchase order items">
                                <thead>
                                    <tr>
                                        <td colspan="6">
                                            ${context.purchaseOrderItemsMap.get(po.poId.toString()).length + context.purchaseOrderExpenseItemsMap.get(po.poId.toString()).length == 1 ?
                                                i18n.t("core.jobs:job.nbItemForPO", {
                                                    varNbItem : 1,
                                                    varPONumber : po.poNumber,
                                                    defaultValue : "There is " + 1 + " item for purchase order " + po.poNumber
                                                }) :
                                                i18n.t("core.jobs:job.nbItemsForPO", {
                                                    varNbItem : context.purchaseOrderItemsMap.get(po.poId.toString()).length + context.purchaseOrderExpenseItemsMap.get(po.poId.toString()).length,
                                                    varPONumber : po.poNumber,
                                                    defaultValue : "There are " + context.purchaseOrderItemsMap.get(po.poId.toString()).length
                                                            + " items for purchase order " + po.poNumber
                                                })
                                            }
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="poitemno" scope="col">
                                            <cwms-translate code="itemno">Item No</cwms-translate>
                                        </th>
                                        <th class="poinst" scope="col">
                                            <cwms-translate code="instrument">Instrument</cwms-translate>
                                        </th>
                                        <th class="poserial" scope="col">
                                            <cwms-translate code="serialno">Serial No</cwms-translate>
                                        </th>
                                        <th class="poplant" scope="col">
                                            <cwms-translate code="plantno">Plant No</cwms-translate>
					                    </th>
                                        <th class="pocalt" scope="col">
                                            <cwms-translate code="caltype">Cal Type</cwms-translate>
                                        </th>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <td colspan="6">&nbsp;</td>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        ${repeat(context.purchaseOrderItemsMap.get(po.poId.toString()), (poItem, index) => html`
                                            <tr>
                                                <td class="center">${poItem.itemNo}</td>
                                                <td>${poItem.instrument}</td>
                                                <td>${poItem.serialNo}</td>
                                                <td>${poItem.plantNo}</td>
                                                <td>${poItem.serviceType}</td>
                                            </tr>
                                        `)}
                                        <tr>
                                            <th class="poitemno" scope="col">
                                                <cwms-translate code="itemno">Item No</cwms-translate>
                                            </th>
                                            <th class="poinst" scope="col">
                                                <cwms-translate code="model">Model</cwms-translate>
                                            </th>
                                            <th colspan="2" scope="col">
                                                <cwms-translate code="comment">Comment</cwms-translate>
                                            </th>
                                            <th scope="col">
                                                <cwms-translate code="newjobitemsearch.quantity">Quantity</cwms-translate>
                                            </th>
                                        </tr>
                                        ${repeat(context.purchaseOrderExpenseItemsMap.get(po.poId.toString()), (poItem, index) => html`
                                            <tr>
                                                <td class="center">${poItem.itemNo}</td>
                                                <td>${poItem.serviceModel}</td>
                                                <td colspan="2">${poItem.comment}</td>
                                                <td>${poItem.quantity}</td>
                                            </tr>
                                        `)}
                                    </tbody>
                                </thead>
                            </table>
                        </td>
                    </tr>` : ``}
            `)}` : html`
            <tr>
				<td colspan="8" class="bold center">
                    <cwms-translate code="viewjob.noclientpoforjob">No client purchase orders for this job</cwms-translate>
                </td>
            </tr>`
        }
    </tbody>
    <tbody>
		<tr>
			<th colspan="8"><cwms-translate code="company.bpos">BPOs</cwms-translate/></th>
		</tr>
		<tr>
            <th class="podef" scope="col">
                <cwms-translate code="viewjob.default">Default</cwms-translate/>
            </th>
			<th><cwms-translate code="viewjob.bponum">BPO Number</cwms-translate></th>
			<th><cwms-translate code="bpo.comment">Comment</cwms-translate></th>
			<th><cwms-translate code="source">Source</cwms-translate></th>
            <th class="center">
                <cwms-translate code="addjob.items"/>Add Items</cwms-translate>
            </th>
            <th class="poitems" scope="col">
                +/- <cwms-translate code="accessoriesanddefects.jobitems">Job Items</cwms-translate>
            </th>
            <th class="right">
                <cwms-translate code="bpo.limitamount">Limit Amount</cwms-translate>
            </th>
            <th class="center">
                <cwms-translate code="accessoriesanddefects.delete">Delete</cwms-translate>
            </th>
        </tr>
        ${(context.blanketPurchaseOrders && context.blanketPurchaseOrders.length > 0) ? html`
            ${repeat(context.blanketPurchaseOrders, (bpo ) => html`
                <tr bpo-id="${bpo.poId}" class="even">
                    <td class="bpodef">
                        ${bpo.active ? bpo.defaultForJob ?
                            html`<img src="img/icons/po_default.png" width="24" height="16" id="defaultBPOImg"
                                alt="${context.imgPODefault}" title="${context.imgPODefault}"/>` :
                            html`<img src="img/icons/po.png" width="16" height="16"
                                alt="${context.imgPO}" title="${context.imgPO}"
                                @click="${(e) => context.updateJobDefaultBPO(bpo.poId)}"/>` :
                            html`<img src="img/icons/po_deactivate.png" width="16" height="16"
                                alt="${context.imgPODeactive}" title="${context.imgPODeactive}"/>`}
                    </td>
                    <td>${bpo.poNumber}</td>
                    <td>
                        (
                            <strong><cwms-translate code="viewjob.valid">Valid</cwms-translate>:</strong>
                            ${dateFormatDirective(bpo.durationFrom)}
                            <strong><cwms-translate code="viewjob.to">To</cwms-translate></strong>
                            ${dateFormatDirective(bpo.durationTo)}
                        )
                        ${bpo.comment ? html`(${bpo.comment})` : html``}
                    </td>
                    <td>
                        ${bpo.scope == 'CONTACT' ?
                                html`<cwms-translate code="contact">Contact</cwms-translate>` :
                            bpo.scope == 'SUBDIV' ?
                                html`<cwms-translate code="subdiv">Subdiv</cwms-translate>` :
                            bpo.scope == 'COMPANY' ?
                                html`<cwms-translate code="company">Company</cwms-translate>` :
                                html`<cwms-translate code="job">Job</cwms-translate>`
                        }
                    </td>
                    <td class="center">
                        ${bpo.addedItems + bpo.addedExpenseItems > 0 ? html`
                            <span id="bpo${bpo.poId}Size">${bpo.addedItems} / ${bpo.addedExpenseItems}</span>
                            <a id="itemsLink${bpo.poId}">
								<img @click="${context.updateBPOItems}" src="img/icons/items.png" width="16" height="16" alt="${context.viewItems}" title="${context.viewItems}" class="image_inline" />
                            </a>`:
                            html`<span id="bpo${bpo.poId}Size">0 / 0</span>`
                        }
                    </td>
                    <td class="poitems">
                        ${bpo.active ? context.hasAuthToAddRemoveItems ?
                            html`<a onclick="addJobItemsToBPO(${context.jobId}, ${bpo.poId}, '${bpo.poNumber}');"><img src="img/icons/po_add_remove.png" width="16" height="16" class="img_marg_bot" /></a>` :
                            html`<img src="img/icons/po_add_remove.png" width="16" height="16" class="img_marg_bot" />` : html``
                        }
					</td>
                    <td colspan="1" class="cost2pc">
                        ${bpo.limitAmount ? bpo.limitAmount.toLocaleString(i18locale ? i18locale : "en-GB", {
                            style: "currency",
                            currency: context.currency
                        }) : ''}
                    </td>
                    <td class="center">
                        ${context.hasAuthToRemoveBPO ?
                            html`<img @click="${context.removeBPOFromJob}" src="img/icons/delete.png" height="16" width="16" title="${context.imgRemoveBPOFromJob}" alt="${context.imgRemoveBPOFromJob}" />` :
                            html`<img src="img/icons/delete.png" height="16" width="16" title="${context.imgRemoveBPOFromJob}" alt="${context.imgRemoveBPOFromJob}" />`
                        }
					</td>
                </tr>
                ${context.blanketPurchaseOrderDetails.has(bpo.poId.toString()) && context.blanketPurchaseOrderDetails.get(bpo.poId.toString()) ? html`
                    <tr>
                        <td colspan="8">
                            <table class="child_table" summary="This table lists all purchase order items">
                                <thead>
                                    <tr>
                                        <td colspan="6">
                                            ${context.blanketPurchaseOrderItemsMap.get(bpo.poId.toString()).length + context.blanketPurchaseOrderExpenseItemsMap.get(bpo.poId.toString()).length == 1 ?
                                                i18n.t("core.jobs:job.nbItemForPO", {
                                                    varNbItem : 1,
                                                    varPONumber : bpo.poNumber,
                                                    defaultValue : "There is " + 1 + " item for purchase order " + bpo.poNumber
                                                }) :
                                                i18n.t("core.jobs:job.nbItemsForPO", {
                                                    varNbItem : context.blanketPurchaseOrderItemsMap.get(bpo.poId.toString()).length + context.blanketPurchaseOrderExpenseItemsMap.get(bpo.poId.toString()).length,
                                                    varPONumber : bpo.poNumber,
                                                    defaultValue : "There are " + (context.blanketPurchaseOrderItemsMap.get(bpo.poId.toString()).length + context.blanketPurchaseOrderExpenseItemsMap.get(bpo.poId.toString()).length)
                                                            + " items for purchase order " + bpo.poNumber
                                                })
                                            }
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="poitemno" scope="col">
                                            <cwms-translate code="itemno">Item No</cwms-translate>
                                        </th>
                                        <th class="poinst" scope="col">
                                            <cwms-translate code="instrument">Instrument</cwms-translate>
                                        </th>
                                        <th class="poserial" scope="col">
                                            <cwms-translate code="serialno">Serial No</cwms-translate>
                                        </th>
                                        <th class="poplant" scope="col">
                                            <cwms-translate code="plantno">Plant No</cwms-translate>
                                        </th>
                                        <th class="pocalt" scope="col">
                                            <cwms-translate code="caltype">Cal Type</cwms-translate>
                                        </th>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <td colspan="6">&nbsp;</td>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        ${repeat(context.blanketPurchaseOrderItemsMap.get(bpo.poId.toString()), (bpoItem, index) => html`
                                            <tr>
                                                <td class="center">${bpoItem.itemNo}</td>
                                                <td>${bpoItem.instrument}</td>
                                                <td>${bpoItem.serialNo}</td>
                                                <td>${bpoItem.plantNo}</td>
                                                <td>${bpoItem.serviceType}</td>
                                            </tr>
                                        `)}
                                        <tr>
                                            <th class="poitemno" scope="col">
                                                <cwms-translate code="itemno">Item No</cwms-translate>
                                            </th>
                                            <th class="poinst" scope="col">
                                                <cwms-translate code="model">Model</cwms-translate>
                                            </th>
                                            <th colspan="2" scope="col">
                                                <cwms-translate code="comment">Comment</cwms-translate>
                                            </th>
                                            <th scope="col">
                                                <cwms-translate code="newjobitemsearch.quantity">Quantity</cwms-translate>
                                            </th>
                                        </tr>
                                        ${repeat(context.blanketPurchaseOrderExpenseItemsMap.get(bpo.poId.toString()), (bpoItem, index) => html`
                                            <tr>
                                                <td class="center">${bpoItem.itemNo}</td>
                                                <td>${bpoItem.serviceModel}</td>
                                                <td colspan="2">${bpoItem.comment}</td>
                                                <td>${bpoItem.quantity}</td>
                                            </tr>
                                        `)}
                                    </tbody>
                                </thead>
                            </table>
                        </td>
                    </tr>` : html``
                }`
            )}` : html`
            <tr>
                <td colspan="8" class="bold center">
                <cwms-translate code="viewjob.nobpoass">No BPO's Assigned</cwms-translate> 
                (<a class="mainlink" style="color : #0063BE;" onclick="event.preventDefault(); viewAvailableBPOs(${context.personId}, ${context.jobId}, '${context.currency}');">
                    ${context.availableBPOs ? context.availableBPOs.length : 0}
                    <cwms-translate code="viewjob.available">available</cwms-translate>
                </a>)
                </td>
            </tr>
        `}
	</tbody>
</table>
`;