import * as litElementJs from '../../thirdparty/lit-element/lit-element.js'

export let styling = (context) => litElementJs.css`
.podef {
	width: 6%;
}
.ponum {
	width: 12%;
}
.pocom {
	width: 53%;
}
.poitems {
	width: 8%;
	text-align: center;
}
.poedit {
	width: 6%;
	text-align: center;
}
.podel {
	width: 7%;
	text-align: center;
}
.float-right {
	float: right;
}
.bold {
	font-weight:bold;
}
.center {
	margin-top: 10px;
}
`;