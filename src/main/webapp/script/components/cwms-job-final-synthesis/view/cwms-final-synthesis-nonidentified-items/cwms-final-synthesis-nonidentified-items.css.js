import { css } from '../../../../thirdparty/lit-element/lit-element.js'

export const styling = (context) => css`

    .info {
        padding: 0.4%;
        line-height: 1.9;
    }

    .bold {
        font-weight:bold;
    }

    .success {
        color:green;
        font-weight:bold;
    }

    .fail {
        color: red;
        font-weight:bold;
    }

    a {
        color:var(--cwms-main-link-color,#0063be);
        font-weight:bold;
    cursor: pointer;
}

.initial-container {
    display:flex;
    justify-content:center;
    align-items:center;
    min-height:150px;
}
`;