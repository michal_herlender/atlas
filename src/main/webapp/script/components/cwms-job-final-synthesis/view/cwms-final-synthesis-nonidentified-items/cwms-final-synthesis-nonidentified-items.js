import { LitElement, html } from "../../../../thirdparty/lit-element/lit-element.js";
import { styling } from "./cwms-final-synthesis-nonidentified-items.css.js";
import { template } from "./cwms-final-synthesis-nonidentified-items.html.js";
import { storeSynthesisNonIdentifiedItems, addSynthesisItem, removeNonIdentifiedItem } from "../../store/actions.js";
import { CwmsFinalSynthesisUtils, HeaderDescriptor  } from "../../../tools/exchange-fromat-utils/utils.js";
import { tableStyling } from "../../../tools/exchange-fromat-utils/tableRenderer.js";
import { generalStyling as generalSpinnerStyling } from "../../../cwms-spinner/cwms-spinner.css.js";
import {template as spinnerTemplate} from  "../../../cwms-spinner/cwms-spinner.html.js";
import { queryInstruments, queryInstrumentsFromCompanyGroup } from "../../../tools/exchange-fromat-utils/requestUtils.js";
import store from "../../store/store.js";
import { popUpContentTemplate } from "../../../tools/exchange-fromat-utils/popupTemplate.js";

class CwmsFinalSynthesisNonitentifiedItemsInner extends LitElement {
  static get properties() {
    return {
      items: { type: Array },
      tableHeader: { type: Object },
      showPopup:{type:Boolean, attribute:false},
      possibleItems:{type:Array},
      companyId:{type:Number},
      groupId:{type:Number},
      defaultNextStatusId:{type:Number},
      defaultServiceTypeId:{type:Number},
      popUpContent:{type:Object,attribute:false},
    };
  }

  static get styles(){
      return [styling({}),tableStyling, generalSpinnerStyling];
  }

  constructor() {
    super();
    this.initialPopUpContent = html`
    <div class="initial-container">
    ${spinnerTemplate({})}
  </div>
    `;
    this.popUpContent = this.initialPopUpContent;
    this.items = store.getState()?.nonIdentifiedItems ?? [];
    this.possibleItems = [];

    window.store = store;
    store.subscribe(() => {
      const { nonIdentifiedItems: items } = store.getState();
      this.items = items;
    });
  }

  get extendedHeader() {
    const headerEntries = Object.entries(this.tableHeader).map((e) => [
      e[1].name,
      {...(new HeaderDescriptor(e[0], e[1].value, true)),...e[1]},
    ]);
    return Object.fromEntries([
      ["index", new HeaderDescriptor("index", "Index in file")],
      ...headerEntries,
      ["status", new HeaderDescriptor("tablesynthesis.tablestatut", "Status")],
    ]);
  }

  groupItemsBySubFamily(items) {
    return items.reduce((acc, it) => {
      const key = it.subFamily ?? "noFamily";
      return { ...acc, [key]: [...(acc[key] ?? []), it] };
    }, {});
  }

  searchInstrument(item) {
      this.showPopup = true;
      queryInstruments(this.companyId,item.plantNo ??"",item.serialNo ??"")
      .then(insts =>{
         this.possibleItems = insts;
          this.popUpContent = popUpContentTemplate({items:insts.companyInstruments, selectInstrument:this.selectInstrument(item)})
        })
      .catch(console.error);
  }

  searchInstrumentInGroup(item) {
      this.showPopup = true;
      queryInstrumentsFromCompanyGroup(this.groupId,item)
      .then(insts => {
         this.possibleItems = insts;
          this.popUpContent = popUpContentTemplate({items:insts, selectInstrument:this.selectInstrument(item)})
        })
        .catch(console.error);
  }

  buildIdentifiedItem(item,instrument){
    const template = JSON.parse('{"logisticsComment":"","status":{"success":true,"messageCode":"exchangeformat.import.itemstatus.identified","message":"Identified"},"statusData":null,"serviceTypeSource":{"description":"Default From File","name":"FILE"}}');
    return {...item,idTrescal:instrument.plantId,...instrument,nextStatuId:this.defaultStatusId,
      servicetypeid:item.servicetypeid??this.defaultServiceTypeId,
      ...template};
  }

  selectInstrument = item => instrument => {
    addSynthesisItem(this.buildIdentifiedItem(item,instrument));
    removeNonIdentifiedItem(item);
    this.showPopup = false;
    this.popUpContent = this.initialPopUpContent;
  }

  render() {
    return template({
      items: this.groupItemsBySubFamily(this.items),
      itemsSize: this.items.length,
      tableHeader: this.extendedHeader,
      searchInstrument:this.searchInstrument.bind(this),
      searchInstrumentInGroup:this.searchInstrumentInGroup.bind(this),
      showPopup:this.showPopup,
      hidePopUp:_ => this.showPopup =false,
      popUpContent:this.popUpContent,
      groupId: this.groupId
        });
  }
}

customElements.define(
  "cwms-final-synthesis-nonidentified-items-inner",
  CwmsFinalSynthesisNonitentifiedItemsInner
);

class CwmsFinalSynthesisNonitentifiedItemsWrapper extends LitElement {

  createRenderRoot(){
      return this;
  }

  static get properties() {
    return {
      items: { type: Array },
      storedItems: { type: Array },
      statuses: { type: Array },
      tableHeader: { type: Object },
      companyId:{type:Number},
      groupId:{type:Number},
      serviceTypes: { type: Object },
    };
  }

  get defaultStatusId(){
     return this.statuses.filter((s) => s.id == 7 || s.id == 4054)?.[0]?.id ?? "";
  }

  get defaultServiceTypeId(){
    return parseInt(Object.keys(this.serviceTypes)[0]) ??0;
  }
  constructor(){
    super();
    this.statuses = [];
    store.subscribe(() => {
      const {nonIdentifiedItems} = store.getState();
      this.storedItems = nonIdentifiedItems;
    })
  }

  get items(){
    return  this._items;
  }

  set items(items){
    const oldItems = this.items;
    this._items = items;
    storeSynthesisNonIdentifiedItems(items);
    this.requestUpdate("items",oldItems);
  }

  inputsGenerator() {
    return  this.storedItems.map((item,idx) => [
      CwmsFinalSynthesisUtils.hiddenInput (
        CwmsFinalSynthesisUtils.generateInputnonIdentifiedName("index", idx),
        item.index
      ),
      ...CwmsFinalSynthesisUtils.headerBasedInputsGenerator(this.tableHeader,CwmsFinalSynthesisUtils.generateInputnonIdentifiedName)(item,idx)
      ]);
  }

  render() {
   return html`<cwms-final-synthesis-nonidentified-items-inner .tableHeader=${this.tableHeader} .companyId=${this.companyId} .groupId=${this.groupId} .defaultNextStatusId=${this.defaultStatusId} .defaultServiceTypeId=${this.defaultServiceTypeId}>
   ${this.inputsGenerator()}
</cwms-final-synthesis-nonidentified-items-inner>`;
  }
}

customElements.define(
  "cwms-final-synthesis-nonidentified-items",
  CwmsFinalSynthesisNonitentifiedItemsWrapper
);
