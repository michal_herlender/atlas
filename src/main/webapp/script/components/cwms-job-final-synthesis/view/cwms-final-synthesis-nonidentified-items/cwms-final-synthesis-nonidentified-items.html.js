import { html} from '../../../../thirdparty/lit-element/lit-element.js'
import { renderTableWithHeadersAndItemRenderer } from '../../../tools/exchange-fromat-utils/tableRenderer.js';
import { ExchangeFormatFieldMapping } from '../../../tools/exchange-fromat-utils/utils.js';
import "../../../cwms-translate/cwms-translate.js"
import { jobItemLinkGenerator } from '../../../../tools/UrlGenerator.js';
import {popUpTemplate} from "../../../tools/popUpTemplate.html.js";

export const template = (context) => html`
                ${context.showPopup? html`${popUpTemplate({
                    title:html`<cwms-translate code="exchangeformat.duplicates.selectInstrument">Select instrument</cwms-translate>`,
                    overlayContent:context.popUpContent,
                    hide:context.hidePopUp})}`:html``}
						<div class="info">
							<span><cwms-translate code="finalsynthesis.table.nonidentitle" >Non Identified Items</cwms-translate>: </span><span class="bold">( ${context.itemsSize} <cwms-translate code="newcalibration.items" >items</cwms-translate> )</span>
                        </div>
                        <slot></slot>
                        ${renderTableWithHeadersAndItemRenderer(headerRenderer,itemRenderer)(context)}
`;


const headerRenderer = context => headerEntry => {
    const {key,isErp,message} = headerEntry[1];
    if(isErp) return html`<th class="erp">${key}</th>`;
    return html`<th><cwms-translate code=${key}>${message}</cwms-translate></th>`;
}

const itemRenderer = context =>(header,item,idx) => {
    const descriptor = ExchangeFormatFieldMapping[header];
    switch(header){
        case "index": 
            return `#${item[descriptor?.dtoField ?? header] ??""} `
        case "status": return statusRenderer(item,context)
        default: 
            return `${item[descriptor?.dtoField ?? header] ??""} `
    }
}

function statusRenderer(item, context) {
  const { name,success, message, messageCode } = item.status;
  if (messageCode.includes("mulitpleinstrumentswithsameplantno"))
    return renderStatusForMultipleInCompany(item, context);
  let linkToItem;
  switch(name){
    case "NOT_IDENTIFIED_IN_CLIENT_COMPANY":
      return renderStatusForSearchInGroup(item,context);
    case "DUPLICATED_IN_TABLE":
      linkToItem = html`
        <cwms-translate code="index">index in file:</cwms-translate>
        ${item.statusData[1]}
      `;
      break;
    case "ALREADY_ONJOB":
      linkToItem = linkToJobItem(item.statusData[0]);
      break;
    case "ALREADY_ONPREBOOKING":
      linkToItem = html`
        : ${item.statusData[0]}
      `;
      break;
  }
  return html`
    <span class=${success ? "sucess" : "fail"}>
      <cwms-translate code=${messageCode}>${message}</cwms-translate>
      ${linkToItem}
    </span>
  `;
}

function renderStatusForMultipleInCompany(item,context){
    const {success,message,messageCode} = item.status;
        return html`<div><span class=${success ? "sucess" : "fail"}><cwms-translate code=${messageCode}>${message}</cwms-translate> </span>
        <a role="button" @click=${_ => context.searchInstrument(item)}><cwms-translate code="exchangeformat.duplicates.selectInstrument">select instrument</cwms-translate></a></div>`;
}

function renderStatusForSearchInGroup(item,context){
    const {success,message,messageCode} = item.status;
        return html`<div><span class=${success ? "sucess" : "fail"}><cwms-translate code=${messageCode}>${message}</cwms-translate> </span>
        ${context.groupId? html`<a role="button" @click=${_ => context.searchInstrumentInGroup(item)}><cwms-translate code="exchangeformat.duplicates.searchInstrumentInGroup">Search instrument in the company group</cwms-translate></a>`:html``}</div>`;
}

const linkToJobItem = jobItemId => html`
<a class="mainLink" target="_blank" .href=${jobItemLinkGenerator(jobItemId).toString()}>${jobItemId}</a>`;

