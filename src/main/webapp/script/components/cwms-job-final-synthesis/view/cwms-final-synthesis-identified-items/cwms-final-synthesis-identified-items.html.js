import { html } from '../../../../thirdparty/lit-element/lit-element.js'
import "../../../cwms-translate/cwms-translate.js"
import "../../../../thirdparty/@vaadin/vaadin-grid/all-imports.js";
import { instrumentLinkGenerator } from '../../../../tools/UrlGenerator.js';
import { renderTableWithHeadersAndItemRenderer } from '../../../tools/exchange-fromat-utils/tableRenderer.js';
import { ExchangeFormatFieldMapping } from '../../../tools/exchange-fromat-utils/utils.js';

export const template = (context) => html`
						<div class="info">
							<span><cwms-translate code="finalsynthesis.table.identitle" >Identified items</cwms-translate>: </span><span class="bold">( ${context.itemsSize} <cwms-translate code="newcalibration.items" >items</cwms-translate> )</span>
                        </div>
                        <slot></slot>
                        ${renderTableWithHeadersAndItemRenderer(headerRenderer,itemRenderer)(context)}

`;


const headerRenderer = context =>  headerEntry => {
    const {key,isErp,message} = headerEntry[1];
    if(isErp) return html`<th class="erp">${key}</th>`;
    if(key=="index")
        return html`<th>
                        <input type="checkbox" @change=${e => context.checkAll(e.currentTarget.checked)} />
                        <cwms-translate code=${key}>${message}</cwms-translate>
                    </th>`
    return html`<th><cwms-translate code=${key}>${message}</cwms-translate></th>`;

} 


const itemRenderer = context =>(header,item,idx) => {
    const descriptor = ExchangeFormatFieldMapping[header];
    const index = context.findItemIndex(item);
    switch (header){
        case "index": return html`#${item.index ??""}
        <input type="checkbox" .checked=${!!item.checked} @input=${e => context.updateField("checked",index,e.currentTarget.checked)} >
        `;
        case "status": return renderStatus(item.status,item.idTrescal ??"");
        case "comment": return html`<textarea @input=${e => context.updateField("logisticsComment",index,e.currentTarget.value)}>${item.logisticsComment??""}</textarea>`;
        case "nextStatus": return nexStatusRenderer(context,item,index);
        case "serviceType":  return renderServiceType(context,item.serviceTypeSource,item.servicetypeid,index)
        default: 
            return `${item[descriptor?.dtoField ?? header] ??""} `
    }
}


function nexStatusRenderer(context,item,idx) {

    const defaultStatusId = context.statuses.filter(s =>  s.id ==7 || s.id ==4054)?.[0]?.id ?? ''
    const statusId = item.nextStatuId ?? defaultStatusId;
    return html`<select name="nextStatus" @input=${e => context.updateField("nextStatuId",idx,e.currentTarget.value)}>
            ${context.statuses.map(s => html`<option value=${s.id} .selected=${s.id==statusId} >${s.description}</option>`)}
        </select>`;
}

function renderServiceType(context,soruceType,typeId,idx){
        const {serviceTypes,updateField} = context;
        return html`<select name="serviceType" @input=${e => updateField("servicetypeid",idx,e.currentTarget.value)}>
            ${Object.entries(serviceTypes).map(s => html`<option value=${s[0]} .selected=${s[0]==typeId}>${s[1]}</option>`)}
        </select>
                ${soruceType ?html` <span class="serviceInfo" >${ soruceType.description }</span>`:html``}
`;

}

function renderStatus(status,plantId){
    return html`
        <span class=${status.success?"success":""}><cwms-translate code=${status.messageCode}>${status.message}</cwms-translate></span> ( ${linkToInstrument(plantId)} )`;
}


const linkToInstrument = plantId => html`
<a class="mainLink" .href=${instrumentLinkGenerator(plantId).toString()}>${plantId}</a>`;