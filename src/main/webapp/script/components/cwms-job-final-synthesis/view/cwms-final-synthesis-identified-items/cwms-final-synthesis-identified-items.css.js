import { css } from '../../../../thirdparty/lit-element/lit-element.js'

export const styling = (context) => css`

    .info {
        padding: 0.4%;
        line-height: 1.9;
    }
    .bold {
        font-weight:bold;
    }
input, select, textarea, button
{
        font-size:100%;
        font-family:var(--cwms-main-font-family);
}

    select {
        margin:1px;
        margin-right:4px;
    }


    .success {
        color:green;
        font-weight:bold;
    }

    .mainLink {
        color:var(--cwms-main-link-color,#0063be);
        font-weight:bold;
    }

    .serviceInfo {
        color: green;
        font-size:9px;
    }
`;