import { LitElement, html } from "../../../../thirdparty/lit-element/lit-element.js";
import { styling } from "./cwms-final-synthesis-identified-items.css.js";
import { template } from "./cwms-final-synthesis-identified-items.html.js";
import { storeSynthesisItems } from "../../store/actions.js";
import store from "../../store/store.js";
import {  CwmsFinalSynthesisUtils, HeaderDescriptor  } from "../../../tools/exchange-fromat-utils/utils.js";
import { tableStyling } from "../../../tools/exchange-fromat-utils/tableRenderer.js";

class CwmsFinalSynthesisIdentifiedItems extends LitElement {
  static get properties() {
    return {
      items: { type: Array },
      tableHeader: { type: Object },
      serviceTypes: { type: Object },
      statuses: { type: Array },
    };
  }

  constructor(){
    super();
    this.items = store.getState()?.items ?? [];
    store.subscribe(() => {
        const {items} = store.getState();
        this.items = items;
    } );
  }

  get extendedHeader() {
    const headerEntries = Object.entries(this.tableHeader).map((e) => [
      e[1].name,
      {...(new HeaderDescriptor(e[0], e[1].value, true)),...e[1]},
    ]);
    return Object.fromEntries([
      ["index", new HeaderDescriptor("index", "Index in file")],
      ...headerEntries,
      ["status", new HeaderDescriptor("tablesynthesis.tablestatut", "Status")],
      ["comment", new HeaderDescriptor("comment", "Comment")],
      [
        "nextStatus",
        new HeaderDescriptor("finalsynthesis.table.nextstatus", "Next Status"),
      ],
      [
        "serviceType",
        new HeaderDescriptor("finalsynthesis.table.servicetype", "ServiceType"),
      ],
    ]);
  }

  static get styles() {
    return [styling({}),tableStyling];
  }

  groupItemsBySubFamily(items) {
    return items.reduce((acc, it) => {
      const key = it.subFamily ?? "noFamily";
      return { ...acc, [key]: [...(acc[key] ?? []), it] };
    }, {});
  }

  updateField(name, idx, value) {
    const hiddenInput = this.querySelector(
      `input[name="${CwmsFinalSynthesisUtils.generateInputName(name, idx)}"]`
    );
    if (hiddenInput) hiddenInput.value = value;
  }

  checkAll(value) {
     storeSynthesisItems(this.items.map((i) => ({ ...i, checked: value })));
  }

  checkGroup(entry, value) {
     storeSynthesisItems(this.items.map((it) => {
      if (it.subFamily == entry) return { ...it, checked: value };
      else return it;
    }));
  }

  findItemIndex(item){
    return this.items.findIndex(it => it.index ==item.index);
  }

  render() {
    return template({
      items: this.groupItemsBySubFamily(this.items),
      itemsSize: this.items.length,
      tableHeader: this.extendedHeader,
      statuses: this.statuses,
      serviceTypes: this.serviceTypes,
      updateField: this.updateField.bind(this),
      checkAll: this.checkAll.bind(this),
      checkGroup: this.checkGroup.bind(this),
      findItemIndex: this.findItemIndex.bind(this),
    });
  }
}


customElements.define(
  "cwms-final-synthesis-identified-items-inner",
  CwmsFinalSynthesisIdentifiedItems
);

class CwmsFinalSynthesisIdentifiedItemsWrapper extends LitElement {
  static get properties() {
    return {
      items: { type: Array },
      storedItems: { type: Array },
      tableHeader: { type: Object },
      serviceTypes: { type: Object },
      statuses: { type: Array },
    };
  }

  constructor(){
    super();
    store.subscribe(() => {
      const {items} = store.getState();
      this.storedItems = items;
    })
  }

  get items(){
    return  this._items;
  }

  set items(items){
    const oldItems = this.items;
    this._items = items;
    storeSynthesisItems(items);
    this.requestUpdate("items",oldItems);
  }

  createRenderRoot() {
    return this;
  }

  render() {
    return html`
      <cwms-final-synthesis-identified-items-inner
        .tableHeader=${this.tableHeader}
        .serviceTypes=${this.serviceTypes}
        .statuses=${this.statuses}
      >
        ${this.inputsGenerator(this.storedItems)}
      </cwms-final-synthesis-identified-items-inner>
    `;
  }

  inputsGenerator(items) {
    const defaultStatusId =
      this.statuses.filter((s) => s.id == 7 || s.id == 4054)?.[0]?.id ?? "";
    return items.map((item, idx) => [
      CwmsFinalSynthesisUtils.hiddenInput(
        CwmsFinalSynthesisUtils.generateInputName("index", idx),
        item.index
      ),
      CwmsFinalSynthesisUtils.hiddenInput(
        CwmsFinalSynthesisUtils.generateInputName("checked", idx),
        item.checked ?? ""
      ),
      ...CwmsFinalSynthesisUtils.headerBasedInputsGenerator(this.tableHeader,CwmsFinalSynthesisUtils.generateInputName)(item, idx),
     CwmsFinalSynthesisUtils.hiddenInput(
        CwmsFinalSynthesisUtils.generateInputName("logisticsComment", idx),
        item.logisticsComment ?? ""
      ),
     CwmsFinalSynthesisUtils.hiddenInput(
        CwmsFinalSynthesisUtils.generateInputName("nextStatuId", idx),
        item.nextStatuId ?? defaultStatusId
      ),
     CwmsFinalSynthesisUtils.hiddenInput(
        CwmsFinalSynthesisUtils.generateInputName("servicetypeid", idx),
        item.servicetypeid
      ),
     CwmsFinalSynthesisUtils.hiddenInput(
        CwmsFinalSynthesisUtils.generateInputName("serviceTypeSource", idx),
        item.serviceTypeSource?.name
      ),
      ...(this.tableHeader.hasOwnProperty("ID_TRESCAL")?[]:[CwmsFinalSynthesisUtils.hiddenInput(
        CwmsFinalSynthesisUtils.generateInputName("idTrescal", idx),
        item.idTrescal
      )]),
    ]);
  }
}

customElements.define(
  "cwms-final-synthesis-identified-items",
  CwmsFinalSynthesisIdentifiedItemsWrapper
);

