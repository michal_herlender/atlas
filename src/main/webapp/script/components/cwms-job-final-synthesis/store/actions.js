import store from "./store.js";
import { STORE_SYNTHESIS_ITEMS, STORE_SYNTHESIS_NONIDENIFIED_ITEMS, ADD_SYNTHESIS_ITEM, REMOVE_NONIDENTIFIED_ITEM } from "./actionsTypes.js";
import { flushDebouncers } from "../../../thirdparty/@polymer/polymer/lib/utils/debounce.js";


export function storeSynthesisItems(items){
    store.send({type:STORE_SYNTHESIS_ITEMS,items});
}

export function storeSynthesisNonIdentifiedItems(items){
    store.send({type:STORE_SYNTHESIS_NONIDENIFIED_ITEMS,items});
}

export function addSynthesisItem(item){
    store.send({type:ADD_SYNTHESIS_ITEM,item});
}

export function removeNonIdentifiedItem(item){
    store.send({type:REMOVE_NONIDENTIFIED_ITEM,index:item.index});
}