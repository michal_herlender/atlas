import { STORE_SYNTHESIS_ITEMS, STORE_SYNTHESIS_NONIDENIFIED_ITEMS, ADD_SYNTHESIS_ITEM, REMOVE_NONIDENTIFIED_ITEM } from "./actionsTypes.js"

let dispatch; export default dispatch = function(state,command){
    switch(command.type){
        case STORE_SYNTHESIS_ITEMS: 
            return {...state, items:command.items};
        case STORE_SYNTHESIS_NONIDENIFIED_ITEMS:
            return {...state, nonIdentifiedItems:command.items}
        case ADD_SYNTHESIS_ITEM:
            return {...state,items:[...state.items,command.item]}
        case REMOVE_NONIDENTIFIED_ITEM:
            return {...state,nonIdentifiedItems:state.nonIdentifiedItems.filter(i => i.index != command.index)}
        default: return state;
    }
}