import { html } from "../../../../thirdparty/lit-element/lit-element.js";
import "../../../cwms-translate/cwms-translate.js";
import "../basket-count/cwms-sourcehire-basket-count.js";

export const template = (context) => html`
  <link rel="stylesheet" href="styles/structure/elements/table.css" />
  <link rel="stylesheet" href="styles/theme/theme.css" />
  <link rel="stylesheet" href="styles/text/text.css" />
  <link rel="stylesheet" href="styles/structure/structure.css" />
  <div class="infobox">
    <!-- link to delete all items in the quote basket -->
    <a
      role="button"
      class="mainlink-float"
      @click=${context.removeAll}
      id="removelink"
      title=${context.removeAllTitle}
      ><cwms-translate code="hiresearchinstr.removeallitems"></cwms-translate
    ></a>
    <!-- clear floats and restore page flow -->
    <div class="clear"></div>
    <table
      class="default4 sourceHireInstBasket"
      summary="<spring:message code='hiresearchinstr.tableallinstrumentsinbasket'/>"
    >
      <thead>
        <tr>
          <td colspan="9">
            <cwms-sourcehire-basket-count></cwms-sourcehire-basket-count>
          </td>
        </tr>
        <tr>
          <th class="item" scope="col">
            <cwms-translate code="hirehome.item"></cwms-translate>
          </th>
          <th class="barcode" scope="col">
            <cwms-translate code="barcode"></cwms-translate>
          </th>
          <th class="desc" scope="col">
            <cwms-translate code="instmodelname"></cwms-translate>
          </th>
          <th class="serialno" scope="col">
            <cwms-translate code="serialno"></cwms-translate>
          </th>
          <th class="plantno" scope="col">
            <cwms-translate code="plantno"></cwms-translate>
          </th>
          <th class="caltype" scope="col">
            <cwms-translate code="hirehome.caltype"></cwms-translate>
          </th>
          <th class="ukascal" scope="col">
            <cwms-translate code="hiresearchinstr.ukascal"></cwms-translate>
          </th>
          <th class="hirecost" scope="col">
            <cwms-translate code="hirehome.hirecost"></cwms-translate>
          </th>
          <th class="remove" scope="col">
            <cwms-translate code="hiresearchinstr.remove"></cwms-translate>
          </th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <td colspan="9"></td>
        </tr>
      </tfoot>
      ${bodyTemplate(context)}
    </table>
    <div class="text-center padtop">
      <button @click=${context.submitTable}>
        ${context.onExistingHire
          ? context.addToHireEnquiryLabel
          : context.continueToEnquiryLabel}
      </button>
    </div>
  </div>
`;

const bodyTemplate = (context) => html`
  <tbody>
    ${!context.basket || context.basket.length == 0
      ? html` <tr>
          <td colspan="9" class="center bold">
            <cwms-translate
              code="hiresearchinstr.basketempty"
            ></cwms-translate>
          </td>
        </tr>`
      : context.basket.map(itemToRowTemlate(context))}
  </tbody>
`;

const itemToRowTemlate = (context) => (item, idx) => html`
  <tr>
    <td class="item">${idx + 1}</td>
    <td class="barcode">
      ${item.plantId}
    </td>
    <td class="desc">${item.fullInstrumentModelName}</td>
    <td class="serialno">${item.serialNo}</td>
    <td class="plantno">${item.plantNo}</td>
    <td class="caltype">
      <select name="basketCalTypeIds" @change=${context.onCalTypeChange(item)}>
        ${context.calibrationTypes.map(
          (ct, idx) => html`<option value="${ct.id}">${ct.shortName}</option>`
        )}
      </select>
    </td>
    <td class="ukascal">
      <input
        type="number"
        name="basketUKASCosts"
        step="0.01"
        @change=${context.changeUkas(item)}
        .value="${item.ukas || 0.0}"
      />
    </td>
    <td class="hirecost">
      <input
        type="number"
        name="basketHireCosts"
        step="0.01"
        .value=${item.hireCost || 0.0}
        @change=${context.changeHireCost(item)}
      />
    </td>
    <td class="remove">
      <a
        role="button"
        class="deleteAnchor"
        @click=${(_) => context.removeFromBasket(item)}
      ></a>
    </td>
  </tr>
`;
