import { LitElement } from "../../../../thirdparty/lit-element/lit-element.js";
import { template } from "./cwms-sourcehire-basket.html.js";
import Cwmstranslate from "../../../cwms-translate/cwms-translate.js";
import store from "../../store/store.js";
import {
  removeFromBasket,
  removeAllFromBasket,
  updateBasketItem,
} from "../../store/actions.js";
import { MetaDataObtainer } from "../../../../tools/metadataObtainer.js";

class CwmsSourceHireBasket extends LitElement {
  static get properties() {
    return {
      onExistingHire: {
        type: Boolean,
        converter: (value, type) => (value == "true" ? true : false),
      },
      defaultUkasCal: { type: Number },
      basket: { type: Array, attributes: false },
      continueToEnquiryLabel: { type: String, attributes: false },
      addToHireEnquiryLabel: { type: String, attributes: false },
      calibrationTypes: { type: Array },
    };
  }

  get contextPath() {
    return MetaDataObtainer.contextPath;
  }

  get csrfToken() {
    return MetaDataObtainer.csrfToken;
  }

  get base() {
    return `${location.origin}${this.contextPath}/`;
  }

  constructor() {
    super();
    Cwmstranslate.subscribeMessage(
      "hiresearchinstr.continuetoenquiry",
      "Continue To Hire Enquiry",
      (m) => (this.continueToEnquiryLabel = m)
    );
    Cwmstranslate.subscribeMessage(
      "hiresearchinstr.addtohireenquiry",
      "Add To Hire Enquiry",
      (m) => (this.addToHireEnquiryLabel = m)
    );
    store.subscribe(() => {
      const { basket } = store.getState();
      this.basket = basket || [];
    });
  }

  calTypeChange(item, event) {
    const value = event.currentTarget.value;
    updateBasketItem({
      ...item,
      ukas: value == 1 ? this.defaultUkasCal : 0,
      calTypeId: value,
    });
  }

  updatePrice(fieldName, item, event) {
    updateBasketItem({ ...item, [fieldName]: event.currentTarget.value });
  }

  submitTable() {
    const url = this.prepareUrl();
    fetch(url, {
      method: "POST",
      credentials: "same-origin",
      headers: {
        "X-CSRF-TOKEN": this.csrfToken,
      },
      redirect: "manual",
      body: this.prepareFormData(),
    })
      .then((res) => (res.ok ? res.json() : Promise.reject(res.statusText)))
      .then((res) => (res.right ? res.value : Promise.reject(res.value)))
      .then((res) => {
        const fullUrl = res.follow;
        window.location.href = fullUrl;
      }, console.error);
  }

  prepareFormData() {
    return this.basket.reduce((fd, item, idx) => {
      fd.append(`items[${idx}].plantId`, item.plantId || 0);
      fd.append(`items[${idx}].serialNo`, item.serialNo);
      fd.append(`items[${idx}].plantNo`, item.plantNo);
      fd.append(
        `items[${idx}].calTypeId`,
        item.calTypeId || this.calibrationTypes[0].id
      );
      fd.append(`items[${idx}].ukas`, item.ukas || "0.00");
      fd.append(`items[${idx}].description`, item.fullInstrumentModelName);
      fd.append(`items[${idx}].hireCost`, item.hireCost || "0.00");
      return fd;
    }, new URLSearchParams());
  }

  prepareUrl() {
    const url = new URL("hire/sourceHireInstruments.json", this.base);
    const params = new URLSearchParams(location.search);
    url.search = params;
    return url;
  }

  render() {
    return template({
      basket: this.basket,
      removeFromBasket: removeFromBasket,
      removeAll: removeAllFromBasket,
      continueToEnquiryLabel: this.continueToEnquiryLabel,
      addToHireEnquiryLabel: this.addToHireEnquiryLabel,
      onExistingHire: this.onExistingHire,
      onCalTypeChange: (item) => (event) => this.calTypeChange(item, event),
      changeUkas: (item) => (event) => this.updatePrice("ukas", item, event),
      changeHireCost: (item) => (event) =>
        this.updatePrice("hireCost", item, event),
      calibrationTypes: this.calibrationTypes,
      submitTable: this.submitTable,
    });
  }
}

customElements.define("cwms-sourcehire-basket", CwmsSourceHireBasket);
