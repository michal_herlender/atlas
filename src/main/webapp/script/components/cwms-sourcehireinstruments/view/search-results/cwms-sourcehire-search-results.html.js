import { html } from "../../../../thirdparty/lit-element/lit-element.js";
import "../../../cwms-translate/cwms-translate.js";

export const template = (context) => html`
  <link rel="stylesheet" href="styles/structure/elements/table.css" />
  <link rel="stylesheet" href="styles/theme/theme.css" />
  <link rel="stylesheet" href="styles/text/text.css" />
  <link rel="stylesheet" href="styles/structure/structure.css" />
  <table
    class="default4 sourceHireInstResults"
    summary="<spring:message code='hiresearchinstr.tabledisplayinstrumentsearchresult'/>"
  >
    <thead>
      <tr>
        <td colspan="6">
          <cwms-translate code="hiresearchinstr.hireinstrumentsavailable">
            no trans</cwms-translate
          >
          (<span class="hireInstSize">${context.results.length}</span>)
          <div class="onHireKey highlight-gold">
            <cwms-translate code="hiresearchinstr.onhire"
              >no trans</cwms-translate
            >
          </div>
        </td>
      </tr>
      <tr>
        <th class="add" scope="col">
          <cwms-translate code="hiresearchinstr.add">Add</cwms-translate>
        </th>
        <th class="barcode" scope="col">
          <cwms-translate code="barcode">barcode</cwms-translate>
        </th>
        <th class="plantno" scope="col">
          <cwms-translate code="plantno">Plant Number</cwms-translate>
        </th>
        <th class="desc" scope="col">
          <cwms-translate code="instmodelname">Description</cwms-translate>
        </th>
        <th class="serialno" scope="col">
          <cwms-translate code="serialno">Serial Number</cwms-translate>
        </th>
        <th class="hirecost" scope="col">
          <cwms-translate code="hirehome.hirecost">Cost</cwms-translate>
        </th>
      </tr>
    </thead>
    <tfoot>
      <tr>
        <td colspan="6">&nbsp;</td>
      </tr>
    </tfoot>
    ${tableBodyTemplate(context)}
  </table>
`;

const tableBodyTemplate = (context) =>
  html`
    <tbody>
      ${!context.results || context.results.length == 0
        ? html`<tr>
            <td colspan="6" class="bold center">
              <cwms-translate
                code="hiresearchinstr.typesearchcriteria"
              ></cwms-translate>
            </td>
          </tr>`
        : context.results.map(resultToRowTemplate(context))}
    </tbody>
  `;

export const resultToRowTemplate = (context) => (row) => html`
  <tr class="${!row.offHire && row.offHire != null ? "highlight-gold" : ""}">
    <td class="add">${renderBasketInteraction(context)(row)}</td>
    <td class="barcode">${row.plantId}</td>
    <td class="plantNo">${row.plantNo}</td>
    <td class="desc">
      <a
        href="viewinstrument.htm?plantid=${row.plantId}&ajax=instrument&width=500"
        class="jconTip ${row.scrapped ? "mainlink-strike" : "mainlink"}"
        name="${context.instumentInformationLabel}"
        id="inst${row.plantId}"
        >${row.fullInstrumentModelName}</a
      >
    </td>
    <td class="serialno">${row.serialNo}</td>
    <td class="hirecost">${row.hireCost}</td>
  </tr>
`;

const renderBasketInteraction = (context) => (row) => {
  if (row.scrapped) return html`<span>${context.scrappedLabel}</span>`;
  if (row.onExistingHire) return html`<span>${context.addedLabel}</span>`;
  return row.inBasket
    ? html`
        <a
          role="button"
          class="removeBasketAnchor"
          @click=${(_) => context.removeFromBasket(row)}
          title=${context.removeFromBasketTitle}
        ></a>
      `
    : html` <a
        role="button"
        class="addAnchor"
        @click=${(_) => context.addToBasket(row)}
        title=${context.addToBasketTitle}
      ></a>`;
};
