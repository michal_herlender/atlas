import { LitElement } from "../../../../thirdparty/lit-element/lit-element.js";
import {
  template,
  resultToRowTemplate,
} from "./cwms-sourcehire-search-results.html.js";
import Cwmstranslate from "../../../cwms-translate/cwms-translate.js";
import store from "../../store/store.js";
import { addToBasket, removeFromBasket } from "../../store/actions.js";

class CwmsSourceHireSearchResults extends LitElement {
  static get properties() {
    return {
      results: { type: Array },
      currentHireInstruments: { type: Array },
      basket: { type: Array, attributes: false },
      addToBasketTitle: { type: String, attributes: false },
      removeFromBasketTitle: { type: String, attributes: false },
      scrappedLabel: { type: String, attributes: false },
      addedLabel: { type: String, attributes: false },
    };
  }

  constructor() {
    super();
    Cwmstranslate.subscribeMessage(
      "scrapped",
      "Scrapped",
      (m) => (this.scrappedLabel = m)
    );
    Cwmstranslate.subscribeMessage(
      "added",
      "Added",
      (m) => (this.addedLabel = m)
    );
    Cwmstranslate.subscribeMessage(
      "addHireInstrToBasket",
      "Add hire instrument to basket",
      (m) => (this.addToBasketTitle = m)
    );
    Cwmstranslate.subscribeMessage(
      "removeHireInstrFromBasket",
      "Remove hire instrument from basket",
      (m) => (this.removeFromBasketTitle = m)
    );
  }

  connectedCallback() {
    super.connectedCallback();
    this.results = store.getState().searchResults || [];
    window.store = store;
    store.subscribe(() => {
      const { basket, searchResults } = store.getState();
      this.basket = basket || [];
      this.results = (searchResults || []).map((r) => ({
        ...r,
        inBasket: this.isInBasket(r.plantId),
        onExistingHire: this.onExistingHire(r.plantId),
      }));
      this.requestUpdate();
    });
  }

  isInBasket(plantId) {
    return this.basket.filter((r) => r.plantId == plantId).length > 0;
  }

  onExistingHire(plantId) {
    if (!this.currentHireInstruments) return false;
    return this.currentHireInstruments.filter((i) => i == plantId).length > 0;
  }

  render() {
    return template({
      results: this.results,
      scrappedLabel: this.scrappedLabel,
      addedLabel: this.addedLabel,
      addToBasket: addToBasket,
      removeFromBasket: removeFromBasket,
      addToBasketTitle: this.addToBasketTitle,
      removeFromBasketTitle: this.removeFromBasketTitle,
    });
  }
}

customElements.define(
  "cwms-sourcehire-search-results",
  CwmsSourceHireSearchResults
);

class CwmsSourceHireSearchResultRow extends LitElement {
  static get properties() {
    return {
      row: { type: Object, attributes: false },
    };
  }

  constructor() {
    super();
    this.basket = [];
  }

  connectedCallback() {
    super.connectedCallback();
    store.subscribe(() => {
      const { basket } = store.getState();
      this.basket = basket || [];
    });
  }

  isInBasket(plantId) {
    return this.basket.filter((r) => r.plantId == plantId).legth > 0;
  }

  render() {
    return resultToRowTemplate({ scrappedLabel: "ADD SCRAPPED TRANSLATION" })({
      ...this.row,
      inBasket: this.isInBasket(this.row.plantId),
    });
  }
}

customElements.define(
  "cwms-sourcehire-search-result-row",
  CwmsSourceHireSearchResultRow
);
