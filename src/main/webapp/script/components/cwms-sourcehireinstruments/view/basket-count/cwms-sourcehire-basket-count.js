import {
  LitElement,
  html,
} from "../../../../thirdparty/lit-element/lit-element.js";
import store from "../../store/store.js";
import "../../../cwms-translate/cwms-translate.js";

class CwmsSourceHireBasketCount extends LitElement {
  static get properties() {
    return { basketCount: { type: Number } };
  }

  constructor() {
    super();
    this.basketCount = 0;
    store.subscribe(() => {
      const { basket } = store.getState();
      this.basketCount = (basket || []).length;
    });
  }

  render() {
    return html`<cwms-translate code="hiresearchinstr.hireinstrumentbasket"
        >Hire Instrument Basket</cwms-translate
      >
      (<span class="basketCount">${this.basketCount}</span>) `;
  }
}

customElements.define(
  "cwms-sourcehire-basket-count",
  CwmsSourceHireBasketCount
);
