import { html } from "../../../../thirdparty/lit-element/lit-element.js";
import "../../../cwms-translate/cwms-translate.js";
import "../../../cwms-spinner-horizontal/cwms-spinner-horizontal.js";

export const template = (context) => html`
  <link rel="stylesheet" href="styles/structure/elements/table.css" />
  <link rel="stylesheet" href="styles/theme/theme.css" />
  <link rel="stylesheet" href="styles/text/text.css" />
  <link rel="stylesheet" href="styles/structure/structure.css" />
  <fieldset>
    <div class="displaycolumn">
      <ol>
        ${context.hasHire
          ? html` <li>
              <label class="width80">
                <cwms-translate code="hiresearchinstr.hiresource"
                  >Sourcing Hire Instruments For Hire Enquiry</cwms-translate
                >
                -
                <a href="viewhire.htm?id=${context.hireId}" class="mainlink">
                  ${context.hireName}
                </a>
              </label>
              <span>&nbsp;</span>
            </li>`
          : html``}
        <li>
          <label><cwms-translate code="barcode">Barcode</cwms-translate>:</label>
          <input
            type="text"
            name="plantid"
            size="10"
            @keyup=${context.searchInstruments}
          />
        </li>
        <li>
          <label><cwms-translate code="serialno">Serial Number</cwms-translate>:</label>
          <input
            type="text"
            name="serialno"
            @keyup=${context.searchInstruments}
          />
        </li>
        <li>
          <label><cwms-translate code="plantno">Plant Number</cwms-translate>:</label>
          <input
            type="text"
            name="plantno"
            id="plantno"
            @keyup=${context.searchInstruments}
          />
        </li>
        ${context.showSpinner? html`
        <li>
          <label>&nbsp;</label>
          <cwms-spinner-horizontal ></cwms-spinner-horizontal>
        </li>
        `:html``}
        <li>
          <label>&nbsp;</label>
          <span id="clear">
            <button @click=${context.clearForm}>
              <cwms-translate code="hiresearchinstr.clear">Clear</cwms-translate>
            </button>
          </span>
        </li>
      </ol>
    </div>
    <div class="displaycolumn">
      <ol>
        <li>
          <label><cwms-translate code="sub-family">Sub family</cwms-translate>:</label>
          <input type="text" name="desc" @keyup=${context.searchInstruments} />
        </li>
        <li>
          <label
            ><cwms-translate code="hirehome.manufacturer">Manufacturer</cwms-translate
            >:</label
          >
          <input type="text" name="mfr" @keyup=${context.searchInstruments} />
        </li>
        <li>
          <label
            ><cwms-translate code="hirehome.model">Model</cwms-translate>:</label
          >
          <input type="text" name="model" @keyup=${context.searchInstruments} />
        </li>
      </ol>
    </div>
  </fieldset>
`;
