import { LitElement } from "../../../../thirdparty/lit-element/lit-element.js";
import { template } from "./cwms-sourcehire-search.html.js";
import { storeSearchResults } from "../../store/actions.js";
import { debounce } from "../../../../tools/Debounce.m.js";

class CwsmSourceHireSearch extends LitElement {
  static get properties() {
    return {
      currentHireName: { type: String },
      currentHireId: { type: Number },
      showSpinner: {type:Boolean,attribute:false}
    };
  }

  constructor() {
    super();
    this.searchHireInstruments = debounce(
      this.searchHireInstruments.bind(this),
      500
    );
  }

  readForm() {
    const plantId = this.shadowRoot.querySelector("[name='plantid']").value;
    const plantNo = this.shadowRoot.querySelector("[name='plantno']").value;
    const serialNo = this.shadowRoot.querySelector("[name='serialno']").value;
    const subFamily = this.shadowRoot.querySelector("[name='desc']").value;
    const brand = this.shadowRoot.querySelector("[name='mfr']").value;
    const model = this.shadowRoot.querySelector("[name='model']").value;
    return new InstrumentSearchContext(
      plantId,
      plantNo,
      serialNo,
      subFamily,
      brand,
      model
    );
  }

  searchHireInstruments() {
    const queryContext = this.readForm();
    this.showSpinner = true;
    this.sendSearchQuery(queryContext).then(res => {this.showSpinner = false; return res;})
    .then(storeSearchResults, console.error);
  }

  get contextPath() {
    return document
      .querySelector("meta[name='_contextPath']")
      .getAttribute("content");
  }

  get csrfToken() {
    return document.querySelector("meta[name='_csrf']").getAttribute("content");
  }

  get base() {
    return `${location.origin}${this.contextPath}/`;
  }

  prepareUrl(queryContext) {
    const url = new URL("hire/searchInstruments.json", this.base);
    url.search = queryContext.toSearchParams();
    return url;
  }

  async sendSearchQuery(queryContext) {
    if (queryContext.isEmpty) return [];
    const r = await fetch(this.prepareUrl(queryContext), {
      method: "GET",
      credentials: "same-origin",
      headers: {
        "Content-Type": "application/json",
        "X-CSRF-TOKEN": this.csrfToken,
      },
    });
    const json = await (r.ok
      ? r.json()
      : Promise.reject(
          `Error on sending request. Server return with ${r.status}.`
        ));
    return json;
  }

  clearForm() {
    storeSearchResults([]);
    this.shadowRoot
      .querySelectorAll("input")
      .forEach((input) => (input.value = ""));
  }

  render() {
    return template({
      searchInstruments: this.searchHireInstruments,
      clearForm: this.clearForm,
      hireId: this.currentHireId,
      hireName: this.currentHireName,
      hasHire: !!this.currentHireId,
      showSpinner: this.showSpinner,
    });
  }
}

customElements.define("cwms-sourcehire-search", CwsmSourceHireSearch);

class InstrumentSearchContext {
  constructor(plantId, plantNo, serialNo, subFamily, brand, model) {
    this.plantId = plantId;
    this.plantNo = plantNo;
    this.serialNo = serialNo;
    this.subFamily = subFamily;
    this.brand = brand;
    this.model = model;
  }

  toSearchParams() {
    const searchParams = new URLSearchParams();
    this.plantId && searchParams.set("plantId", this.plantId);
    this.plantNo && searchParams.set("plantNo", this.plantNo);
    this.serialNo && searchParams.set("serialNo", this.serialNo);
    this.subFamily && searchParams.set("subFamily", this.subFamily);
    this.brand && searchParams.set("brand", this.brand);
    this.model && searchParams.set("model", this.model);
    return searchParams;
  }

  get isEmpty() {
    return !(
      this.plantId ||
      this.plantNo ||
      this.serialNo ||
      this.subFamily ||
      this.brand ||
      this.model
    );
  }
}
