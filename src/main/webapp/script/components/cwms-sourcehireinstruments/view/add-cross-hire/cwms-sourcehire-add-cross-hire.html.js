import { html } from "../../../../thirdparty/lit-element/lit-element.js";
import "../../../cwms-translate/cwms-translate.js";
import "../../../cwms-overlay/cwms-overlay.js";
import { popUpTemplate } from "../../../tools/popUpTemplate.html.js";

export const template = (context) => html`
  <link rel="stylesheet" href="styles/structure/elements/table.css" />
  <link rel="stylesheet" href="styles/theme/theme.css" />
  <link rel="stylesheet" href="styles/text/text.css" />
  <link rel="stylesheet" href="styles/structure/structure.css" />
  <a role="button" class="mainlink" @click=${context.addCrossPopUp}
    >${context.addCrossItemTitle}</a
  >
  ${context.showOverlay
    ? html`
        ${popUpTemplate({
          hide: context.closePopUp,
          title: context.addCrossItemTitle,
          overlayContent: popUpContentTemplate(context),
        })}
      `
    : html``}
`;

const popUpContentTemplate = (context) => html`
  <div id="addCrossHireItemOverlay">
    <fieldset>
      <ol class="nopadding">
        <li>
          <label
            ><cwms-translate code="itemDescription"
              >Item Description:</cwms-translate
            ></label
          ><input type="text" name="itemdesc" value="" />
        </li>
        <li>
          <label
            ><cwms-translate code="itemSerialNo"
              >Item Serial No:</cwms-translate
            ></label
          ><input type="text" name="itemserial" value="" />
        </li>
        <li>
          <label
            ><cwms-translate code="itemPlantNo"
              >Item Plant No:</cwms-translate
            ></label
          ><input type="text" name="itemplant" value="" />
        </li>
        <li>
          <button @click=${context.addCrossItem}>
            ${context.addCrossItemTitle}
          </button>
        </li>
      </ol>
    </fieldset>
  </div>
`;
