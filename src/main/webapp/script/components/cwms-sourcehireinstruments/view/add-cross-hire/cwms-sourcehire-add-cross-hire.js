import { LitElement } from "../../../../thirdparty/lit-element/lit-element.js";
import { template } from "./cwms-sourcehire-add-cross-hire.html.js";
import Cwmstranslate from "../../../cwms-translate/cwms-translate.js";
import { addToBasket } from "../../store/actions.js";

class CwmsSourceHireAddCrossHire extends LitElement {
  static get properties() {
    return {
      showOverlay: { type: Boolean },
      addCrossItemTitle: { type: String, attributes: false },
    };
  }

  constructor() {
    super();
    Cwmstranslate.subscribeMessage(
      "hiresearchinstr.addcrosshireitem",
      "Add Cross Hire Item",
      (m) => (this.addCrossItemTitle = m)
    );
  }

  addCrossItem() {
    const description = this.shadowRoot.querySelector("[name='itemdesc']")
      .value;
    const plantNo = this.shadowRoot.querySelector("[name='itemplant']").value;
    const serialNo = this.shadowRoot.querySelector("[name='itemserial']").value;
    addToBasket({ plantNo, serialNo, fullInstrumentModelName: description });
    this.showOverlay = false;
  }

  render() {
    return template({
      showOverlay: this.showOverlay,
      addCrossPopUp: (_) => (this.showOverlay = true),
      closePopUp: (_) => (this.showOverlay = false),
      addCrossItemTitle: this.addCrossItemTitle,
      addCrossItem: this.addCrossItem,
    });
  }
}

customElements.define(
  "cwms-sourcehire-add-cross-hire",
  CwmsSourceHireAddCrossHire
);
