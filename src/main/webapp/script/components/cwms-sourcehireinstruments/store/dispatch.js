import { ADD_TO_BASKET, STORE_SEARCH_RESULTS, REMOVE_FROM_BASKET, ADD_CROSS_HIRE_TO_BASKET, REMOVE_ALL_FROM_BASKET, UPDATE_BASKET_ITEM } from "./actionsTypes.js";

let dispatch; export default dispatch = function(state,commmand){
    var basket = [];
    switch(commmand.type){
        case STORE_SEARCH_RESULTS:
            return {...state, searchResults:commmand.results};
        case ADD_TO_BASKET:
        case ADD_CROSS_HIRE_TO_BASKET:
            basket = state.basket || [];
            return {...state, basket:[...basket,commmand.item]};
        case REMOVE_FROM_BASKET:
            basket = (state.basket || []).filter(i => i.plantId!=commmand.item.plantId);
            return {...state, basket};
        case REMOVE_ALL_FROM_BASKET:
            return {...state, basket:[]};
        case UPDATE_BASKET_ITEM:
            basket = (state.basket || []).map(it => it.plantId==commmand.item.plantId?commmand.item:it );
            return {...state,basket}
        default: return state;
    }
}