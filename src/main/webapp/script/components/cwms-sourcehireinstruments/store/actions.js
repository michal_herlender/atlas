import store from "./store.js";
import { STORE_SEARCH_RESULTS, ADD_TO_BASKET, REMOVE_FROM_BASKET, UPDATE_BASKET_ITEM ,REMOVE_ALL_FROM_BASKET } from "./actionsTypes.js";

export function storeSearchResults(results){
    store.send({type:STORE_SEARCH_RESULTS,results});
}

export function addToBasket(item){
    store.send({type:ADD_TO_BASKET,item});
}

export function removeFromBasket(item){
    store.send({type:REMOVE_FROM_BASKET,item})
}

export function removeAllFromBasket(){
    store.send({type:REMOVE_ALL_FROM_BASKET});
}

export function updateBasketItem(item){
    store.send({type:UPDATE_BASKET_ITEM,item});
}