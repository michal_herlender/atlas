export const STORE_SEARCH_RESULTS = "STORE_SEARCH_RESULTS";
export const ADD_TO_BASKET = "ADD_TO_BASKET";
export const UPDATE_BASKET_ITEM = "UPDATE_BASKET_ITEM";
export const REMOVE_FROM_BASKET = "REMOVE_FROM_BASKET";
export const REMOVE_ALL_FROM_BASKET = "REMOVE_ALL_FROM_BASKET";
export const ADD_CROSS_HIRE_TO_BASKET = "ADD_CROSS_HIRE_TO_BASKET";
