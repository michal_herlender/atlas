import "./view/search-results/cwms-sourcehire-search-results.js";
import "./view/search/cwms-sourcehire-search.js";
import "./view/add-cross-hire/cwms-sourcehire-add-cross-hire.js";
import "./view/basket/cwms-sourcehire-basket.js";
import "./view/basket-count/cwms-sourcehire-basket-count.js";
import "./store/store.js";