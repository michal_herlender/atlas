import { html } from "../../thirdparty/lit-element/lit-element.js";
import {repeat} from "../../thirdparty/lit-html/directives/repeat.js";
import { directive } from "../../thirdparty/lit-html/lit-html.js";
import { dateFormatDirective } from "../tools/formatingDirectives.js";

export let htmlTemplate = (context) => html `

<link rel="stylesheet" href="styles/structure/elements/table.css">
<link rel="stylesheet" href="styles/theme/theme.css">
    <table class="default4 itemsAtTP">
        <thead>
            <tr>
                <td colspan="5">
                    <b>${context.getTotalItems()}</b>
                    <cwms-translate code='tools.itemsat'>Items at</cwms-translate>
                    <b>${context.data.length}</b>
                    <cwms-translate code='company.subdivs'>Subdivisions</cwms-translate>
                </td>
            </tr>

            <tr>										
                <th class="items"><cwms-translate code='items' >Items</cwms-translate></th>
                <th class="corole"><cwms-translate code='company.role' >Company Role</cwms-translate></th>
                <th class="country"><cwms-translate code='country' >Country</cwms-translate></th>
                <th class="coname"><cwms-translate code='company' >Company</cwms-translate></th>							
                <th class="subname"><cwms-translate code='subdivision' >Subdivision</cwms-translate></th>							
            </tr>
        </thead>

        <tbody>
        
            ${ 
                repeat(context.data, (item, index)=> html`
                    <tr data-index="${index+1}">
                        <td>
                            <center>
                                ${item.itemCount}
                                <img @click="${context.open}" src="img/icons/items.png" width="16" height="16" class="basket" />
                            </center>
                        </td>
                        <td>${item.companyRole}</td>
                        <td>${item.country}</td>
                        <td>${item.compName}</td>
                        <td>${item.subdivName}</td>
                    </tr>

                    ${ index+1 != context.activeDetailsRowIndex ? `` : html`
                        <tr> 
                            <td colspan="5">
                                <div>            
                                    ${ formatDetailsTable(context, context.detailsRowFiltered) } 
                                </div> 
                            </td>               
                        </tr>
                    `
                    }
                `) 
            }
    </tbody>
        <tfoot>
            <tr>
                <td colspan="5">&nbsp;</td>
            </tr>
        </tfoot>
    </table>
`;

const formatDetailsTable = directive((context, detailsRowFiltered) => (part) => {
    part.setValue(repeat(detailsRowFiltered, (item, index)=> html`
        <table style="width: 100%;" class="child_table">
            <thead>
                <tr class="bold">
                    <td colspan="3">
                        <cwms-translate code="jobno">JobNo:</cwms-translate>
                        <span style=" font-weight: normal; ">
                            <a href="${context.jobURL}?jobid=${item[0].jobId}" >
                                ${item[0].jobNo}
                            </a>&nbsp;&nbsp;&nbsp;
                            <cwms-translate code="datein">Date In:</cwms-translate>
                            ${dateFormatDirective(item[0].dateIn)}
                        </span>
                    </td>
                    <td colspan="5">
                        <cwms-translate code="jithirdparty.tpcontactlabel">Third Party Contact:</cwms-translate>
                        <span style=" font-weight: normal; ">
                            ${item[0].name !== null ? item[0].name :  html`-<cwms-translate code="unknown">Unknown</cwms-translate>-`}
                            &nbsp;&nbsp;&nbsp;<cwms-translate code="email">Email:</cwms-translate> 
                            <a href="mailto:${item.email}" class="mainlink">${item[0].email}</a>   
                            &nbsp;&nbsp;&nbsp;<cwms-translate code="email.telephone">Telephone:</cwms-translate>
                            ${item[0].telephone !== null ? item[0].telephone : item[0].mobile !== null ? 
                                     item[0].mobile : ''
                            }
                        </span>
                    </td>
                </tr>
                <tr>
                   <th style="display:none;"></th>
                    <th scope="col" class="itemno">
                        <cwms-translate code="itemno">Item No</cwms-translate>
                    </th>
                    <th scope="col" class="clientcomp">
                        <cwms-translate code="client">Client</cwms-translate>
                    </th>
                    <th scope="col" class="itemdesc">
                        <cwms-translate code="instrument">Instrument</cwms-translate>
                    </th>
                    <th scope="col" class="serialno">
                        <cwms-translate code="serialno">Serial No</cwms-translate>
                    </th>
                    <th scope="col" class="statedesc">
                        <cwms-translate code="state">Status</cwms-translate>
                    </th>
                    <th scope="col" class="senton">
                        <cwms-translate code="jithirdparty.sentTPDelDate">Sent TP /<br> PO Del Date</cwms-translate>
                    </th>
                    <th scope="col" class="pono">
                        <cwms-translate code="jithirdparty.poNoIssued">PO No (Issued)</cwms-translate>
                    </th>
                    <th scope="col" class="daysaway">
                        <cwms-translate code="jithirdparty.daysAway">Days Away</cwms-translate>
                    </th>
                </tr>
            </thead>
            <tbody>
                     ${formatItemsSameJob(context, item)} 
            </tbody>
        </table>
    `));
});


const formatItemsSameJob = directive((context, detailsRow) => (part) => {
    part.setValue(repeat(detailsRow, (item, index)=> html`
                <tr class="${context.itemOverdue(item.overdue)}">
                    <td class="itemno">
                        <center>
                            <a href="${context.jobItemURL}?jobitemid=${item.jobItemId}">${item.itemNo}</a>
                        </center>
                    </td>
                    <td class="clientcomp">${item.clientCompany}</td>
                    <td class="itemdesc">${item.itemDesc}</td>
                    <td class="serialno">${item.serialNo}</td>
                    <td class="statedesc">${item.statusTranslation}</td>
                    <td class="senton"> ${dateFormatDirective(item.sentOn)}
                       ${ (item.poid !== null && item.overdue) ? 
                        html`/<br><span class="attention">${dateFormatDirective(item.poDelDate)}</span>`
                        :
                        html`/<br>${dateFormatDirective(item.poDelDate)}`
                         }
                    </td>
                    <td class="pono">
                       ${item.poid !== 0 ?
                        html`<a href="${springUrl}/viewpurchaseorder.htm?id=${item.poid}" target="_blank" class="mainlink">${item.pono}</a>`: 
                        html`<cwms-translate code="jithirdparty.notavailable">Not Available</cwms-translate>`
                        }
                        &nbsp; ${dateFormatDirective(item.poIssueDate)}
                    </td>
                    <td class="daysaway">${item.daysAway}</td>
                </tr>  
    `));
});
