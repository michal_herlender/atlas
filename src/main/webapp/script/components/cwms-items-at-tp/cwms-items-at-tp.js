import {LitElement} from "../../thirdparty/lit-element/lit-element.js";
import {styling} from "./cwms-items-at-tp.css.js";
import {htmlTemplate} from "./cwms-items-at-tp.html.js";
import "../cwms-translate/cwms-translate.js";
import {urlWithParamsGenerator} from "../../tools/UrlGenerator.js";

class CwmsItemAtTP extends LitElement {

    static get dataURL() {
        return "showItemsAtTP.json"
    };

    static get dataDetailURL() {
        return "showItemsAtSpecificTP.json"
    };

    static get jobURL() {
        return "viewjob.htm"
    };

    static get jobItemURL() {
        return "jiactions.htm"
    };

    static get properties() {
        return {
            data: { type: Array },
            detailsRow : { type : Array },
            detailsRowFiltered : { type : Array },
            activeDetailsRowIndex: { type: Number },
            jobURL: { type: String },
            jobItemURL: { type: String },
            subdivId: { attribute: 'subdiv-id' },
            businessSubdivId: { attribute: 'business-subdiv-id' }
        };
    }

    static getStyles() {
        return styling;
    }

    constructor() {
        super();
        this.data = [];
        this.detailsRowFiltered = [];
        this.addData();
    }

    render() {
        return htmlTemplate(this);
    }

    getTotalItems() {
        var items = 0;
        this.data.forEach(element => {
            items = items + element.itemCount;
        });
        return items;
    }

    itemOverdue(overdue){
        if(overdue){
            return "highlight";
        }
        return "";
    }

    open(e) {
        if (this.activeDetailsRowIndex === e.target.closest('tr').getAttribute('data-index'))
            this.activeDetailsRowIndex = null;
        else
            this.activeDetailsRowIndex = e.target.closest('tr').getAttribute('data-index');

        if(this.activeDetailsRowIndex>=1) {
            let businessSubdivId = this.data[this.activeDetailsRowIndex - 1].subdivId;
            //fetch details data
            fetch(urlWithParamsGenerator(CwmsItemAtTP.dataDetailURL, {subdivId: businessSubdivId}))
                .then(res => res.ok ? res.json() : Promise.reject(res.statusText))
                .then(json => {
                    this.detailsRow = json;
                    //group data by jobId
                    const detailsRowGrouped = this.groupBy(this.detailsRow, item => item.jobId);
                    this.detailsRowFiltered = Object.values(detailsRowGrouped);
                });
        }
    }

    groupBy(xs, f) {
        return xs.reduce((r, v, i, a, k = f(v)) => ((r[k] || (r[k] = [])).push(v), r), {});
      }

    addData() {
        this.data = [];
        this.jobURL = CwmsItemAtTP.jobURL;
        this.jobItemURL = CwmsItemAtTP.jobItemURL;

        fetch(CwmsItemAtTP.dataURL).then(res => res.ok ? res.json() : Promise.reject(res.statusText))
            .then(json =>
                this.data = json
            );
    }
}

window.customElements.define('cwms-items-at-tp', CwmsItemAtTP);