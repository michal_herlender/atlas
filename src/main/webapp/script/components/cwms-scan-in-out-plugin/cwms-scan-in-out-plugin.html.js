import { html } from "../../thirdparty/lit-element/lit-element.js";
import {repeat} from "../../thirdparty/lit-html/directives/repeat.js";


export let htmlTemplate = (context) => html `
		<link rel="stylesheet" href="styles/structure/elements/table.css">
		<link rel="stylesheet" href="styles/theme/theme.css">
		<link rel="stylesheet" href="styles/text/text.css">
		<link rel="stylesheet" href="styles/global.css" type="text/css" title="antech" />
		
	 ${ context.errorMessage !== "" ? 
         html`<div class="warningBox1">
			    <div class="warningBox2">
				    <div class="warningBox3">${context.errorMessage}</div>
			    </div>
	        </div>` : ''      
     } 
     
     
      ${ context.loading ? 
         	html`<center>
                      <img src="img/icons/ajax-loader.gif" width="31" height="31" alt="" />
            	</center>` : '' 
       }
       
       
     ${ context.successfulMessage !== "" ? 
         html`<div class="successBox1">
			    <div class="successBox2">
				    <div class="successBox3">${context.successfulMessage} </div>
			    </div>
	        </div>` : ''      
     } 
		
	<div class="displaycolumn">
		<fieldset>
			<legend>
				 <cwms-translate code='scanitemsinout.scanin'> scan in </cwms-translate>
			</legend>
			<ol>
				<li>
					<label> <cwms-translate code='scanitemsinout.confirmlocation'>Confirm Location :</cwms-translate> </label>
					<select id="currentAddr">
						${ context.businessAddresses.length < 1 ? html`<option> N/A </option> ` 
								: html`${repeat(context.businessAddresses, (item, index)=> html` 
										 	<option ?selected="${item.id == context.defaultaddresId}" value="${item.id}">  ${item.line1} - ${item.city} </option> `)
										 }
						 `}
					</select>
				</li>

				<li>
					<label> 
						<cwms-translate code='scanitemsinout.scanin'> scan in </cwms-translate> 
					</label>
					<input id="scanin" type="text" @keydown="${context.onentrekeyclicked}" />
				</li>
				<li>
					<label>
						<cwms-translate code='company.history'> History</cwms-translate>
					</label>
					
					<div id="scanInHistory" class="float-left"></div>
					<div class="clear"></div>
				</li>
			</ol>
		</fieldset>
	</div>
	
	<div class="displaycolumn">
		<fieldset>
			<legend>
				<cwms-translate code='scanitemsinout.scanout'> scan out </cwms-translate>
			</legend>
			<ol>
				<li>
					<label> <cwms-translate code='scanitemsinout.scanout'> scan out </cwms-translate> </label>
					<input type="text" id="scanout"   @keydown="${context.onentrekeyclicked}"  />
				</li>
				<li>
					<label>
						<cwms-translate code='company.history'> History</cwms-translate>
					</label>

					&nbsp;
					<div id="scanOutHistory" class="float-left"></div>
					<div class="clear"></div>
				</li>

			</ol>
		</fieldset>
	</div>
	
`