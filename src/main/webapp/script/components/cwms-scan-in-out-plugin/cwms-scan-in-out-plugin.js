import { htmlTemplate } from "./cwms-scan-in-out-plugin.html.js";
import { styling } from "./cwms-scan-in-out-plugin.css.js";
import { LitElement, html } from '../../thirdparty/lit-element/lit-element.js';
import { urlWithParamsGenerator } from "../../tools/UrlGenerator.js";
import {} from "../cwms-translate/cwms-translate.js";

class CwmsScanInOutPlugin extends LitElement {
	
	
	static get businessAddressesURL() { return "getbusinessAddresses.json" };
	static get scanOutURL() { return "scanOutDeliveryOrItem.json" };
	static get scanInURL() { return "scanInDeliveryOrItem.json" };
	
	
	static get properties() {
	        return {
				defaultaddresId: { attribute: 'defaultaddres-id' },
	            businessAddresses: { type: Array }, 
	            loading : { type : Boolean },
	            errorMessage : {type: String},
	            successfulMessage : { type: String },
	        };
	    }
	
	 static getStyles() { 
		 	return styling; 
		 }
	 
	 constructor() { 
		 	 super(); 
		 	 this.defaultAddress = new Array();
		 	 this.businessAddresses = new Array();
		 	 this.fetchbusinessAddresses();
		 	this.errorMessage = '';
		 	this.loading = false;
		 	this.successfulMessage = '';
		 }
	 
	 render() {
	        return htmlTemplate(this);
	    }
	 
	 onentrekeyclicked(e){
	        if(e.key=='Enter'){
	        	if(e.currentTarget.id=="scanout"){
		        	this.scanOut(e.currentTarget.value);
		        }else if(e.currentTarget.id=="scanin"){
		        	this.scanIn(e.currentTarget.value);
		        }
				this.shadowRoot.getElementById(e.currentTarget.id).value="";
	        }
	    }
	 
	 
	 scanOut(deliveryNoOrBarcode){
		 this.errorMessage = '';
		 this.successfulMessage = '';
		 this.loading = true;
		 fetch(urlWithParamsGenerator(CwmsScanInOutPlugin.scanOutURL,{deliveryNoOrBarcode:deliveryNoOrBarcode})) 
         .then(res => res.ok ? res.json():Promise.reject(res.statusText))
         .then(json => {
         if (json.success == true) {
        	 this.successfulMessage = `Scan Out was successful for ${deliveryNoOrBarcode}`;
        	 this.loading = false;
			 this.shadowRoot.getElementById('scanOutHistory').innerHTML += '<div><img src="img/icons/greentick.png" width="16" height="16">'+deliveryNoOrBarcode+'</div>';
         } else if(json.success == false){
             this.errorMessage = json.message;
             this.loading = false;
			 this.shadowRoot.getElementById('scanOutHistory').innerHTML += '<div><img src="img/icons/redcross.png" width="16" height="16">'+deliveryNoOrBarcode+'</div>';
         }
         
         });

	 }
	 
	 scanIn(deliveryNoOrBarcode){
		 this.errorMessage = '';
		 this.successfulMessage = '';
		 this.loading = true;
		 let locId = this.shadowRoot.getElementById("currentAddr").value
		 fetch(urlWithParamsGenerator(CwmsScanInOutPlugin.scanInURL,{deliveryNoOrBarcode:deliveryNoOrBarcode,addrId:locId})) 
         .then(res => res.ok ? res.json():Promise.reject(res.statusText))
         .then(json => {
         if (json.success == true) {
        	 this.successfulMessage = `Scan in was successful for ${deliveryNoOrBarcode}`;
        	 this.loading = false;
			 this.shadowRoot.getElementById('scanInHistory').innerHTML += '<div><img src="img/icons/greentick.png" width="16" height="16">'+deliveryNoOrBarcode+'</div>';
         } else if(json.success == false){
        	 this.errorMessage = json.message;
        	 this.loading = false;
			 this.shadowRoot.getElementById('scanInHistory').innerHTML += '<div><img src="img/icons/redcross.png" width="16" height="16">'+deliveryNoOrBarcode+'</div>';
         }
         
         });
	 }

	 
	 fetchbusinessAddresses(){
	    	this.businessAddresses = new Array();
	    	fetch(CwmsScanInOutPlugin.businessAddressesURL)
				.then(res => res.ok?res.json():Promise.reject(res.statusText))
				.then(json => {
	             this.businessAddresses = json;
	         });
	    }
	 
	 
	    refresh() {  
		 	 this.fetchbusinessAddresses();

	    }

	
}window.customElements.define('cwms-scan-in-out-plugin', CwmsScanInOutPlugin);
