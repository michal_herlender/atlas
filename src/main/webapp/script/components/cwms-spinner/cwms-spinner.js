import { LitElement, html } from "../../thirdparty/lit-element/lit-element.js";
import { styling, generalStyling } from "./cwms-spinner.css.js";
import  "../cwms-overlay/cwms-overlay.js";
import { template } from "./cwms-spinner.html.js";

class Spinner extends LitElement {
  static get styles() {
    return [styling,generalStyling];
  }

  render() {
    return template({});
  }
}

customElements.define("cwms-spinner", Spinner);
