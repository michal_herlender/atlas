import { html } from '../../thirdparty/lit-element/lit-element.js'

export const template = (context) => html`
        <div class="spinner-container">
          <div  class="lds-spinner">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
          </div>
        </div>
`;