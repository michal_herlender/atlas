import {html} from "../../thirdparty/lit-element/lit-element.js";
import {repeat} from "../../thirdparty/lit-html/directives/repeat.js";
import {cwmsTranslateDirective} from "../cwms-translate/cwms-translate-directive.js";


export const template = (context) => context.items && context.items.length > 0 ? html`
  <table class="default2" id="itemCOResults" summary="This table displays any called off items">
    <thead >
      <tr>
        <th class="COselect" scope="col">
          <span>
            <cwms-translate code="additems.selectall">Select All</cwms-translate> 
            <input type="checkbox"  class="checkbox" @change=${context.toggleSelectAllItems}/>
          </span>
        </th>
        <th class="CObarcode" scope="col"><cwms-translate code="barcode">Barcode</cwms-translate></th>
        <th class="COInst" scope="col"><cwms-translate code="instrument">Instrument</cwms-translate></th>
        <th class="COsubdiv" scope="col"><cwms-translate code="subdivision">Subdivision</cwms-translate></th>
        <th class="COPrevLoc" scope="col"><cwms-translate code="quotform.previousjobs">Previous Job</cwms-translate></th>
      </tr>
    </thead>
    <tbody>
    ${repeat(context.items,i => i.calloffId,renderCallOffItemRow(context))}
    </tbody>
  </table>`:html`
        <div class="no-items bold center"><cwms-translate code="calloffitems.noitems">There are no called off items</cwms-translate></div>
  `
;


const renderCallOffItemRow =  context => item => html`
<tr>
  <td><input type="checkbox" .checked=${item.selected ?? false} @change=${e => context.toggleSelectItem(item)}/></td>
  <td>${item.plantId}</td>
  <td>${item.instModel}</td>
  <td>${item.subname}</td>
  <td>${cwmsTranslateDirective("addjob.previousItemOnJob","Previously item: {0} on Job {1}",item.itemNo,item.jobno)}</td>
</tr>
`;