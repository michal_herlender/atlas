import {html, LitElement} from "../../thirdparty/lit-element/lit-element.js";
import {repeat} from "../../thirdparty/lit-html/directives/repeat.js";
import {tableStyles} from "../tools/styles/table.css.js";
import {textStyles} from "../tools/styles/text.css.js";
import {styling} from "./cwms-addjob-calloff-items.css.js";
import {template} from "./cwms-addjob-calloff-items.html.js";
import cascadeStore from "../cwms-cascade-search/store/store.js";
import {urlWithParamsGenerator} from "../../tools/UrlGenerator.js";

class CwmsAddJobCallOffItems extends LitElement {

    static get properties() {
        return {
            name: {type: String},
            coid: {type: Number},
            items: {type: Array}
        }
    }

    static get formAssociated() {
        return true;
    }

    #internals;

    constructor() {
        super();
        this.#internals = this.attachInternals();
        cascadeStore.subscribe(() => {
            const {coid} = cascadeStore.getState();
            this.companyUpdated(coid);
        })
    }

    companyUpdated(coid) {
        if (coid === this.coid) return;
        this.coid = coid;
        fetch(urlWithParamsGenerator("calloffitems/byCompany.json", {coid, active: true}))
            .then(res => res.ok ? res.json() : Promise.reject(res.statusText))
            .then(items => this.updateItems(items));
    }

    updateItems(items) {
        this.items = items;
        const formData = items.filter(i => i.selected ?? false).reduce((fd, i) => {
            fd.append(this.name, i.calloffId);
            return fd;
        }, new FormData());
        this.#internals.setFormValue(formData);
    }

    static get styles() {
        return [tableStyles, textStyles, styling()]
    }


    toggleSelectItem(item) {
        this.updateItems(this.items.map(i => i.calloffId === item.calloffId ?
            {...i, selected: !(i.selected ?? false)} : i));
    }

    toggleSelectAllItems(e){
        this.updateItems(this.items.map(i => ({...i,selected:e.currentTarget.checked})));
    }


    render(){
        return template(this);
    }
}

customElements.define("cwms-addjob-calloff-items", CwmsAddJobCallOffItems);


class CwsmAddJobCallOffItemsWrapper extends LitElement {

    static get properties(){
        return {
            items:{type:Array}
        }
    }

    constructor(){
        super();
        this.items = [];
    }
    createRenderRoot(){
        return this;
    }

    render(){
        return html`
        <cwms-addjob-calloff-items-inner .updateItems=${items => this.items = items} .items=${this.items}></cwms-addjob-calloff-items-inner>
        ${repeat(this.items.filter(i => i.selected ?? false),i => i.calloffId,i => html`
        <input type="hidden" .value=${i.calloffId} name="selectedCallOffItems"/>
        `)}
        `;
    }
}


customElements.define("cwms-addjob-calloff-items-wrapper", CwsmAddJobCallOffItemsWrapper);