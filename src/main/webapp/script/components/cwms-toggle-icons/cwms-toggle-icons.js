import { LitElement, html } from "../../thirdparty/lit-element/lit-element.js";
import { template } from "./cwms-toggle-icons.html.js";
import {styling} from  "./cwms-toggle-icons.css.js";

class CwmsToggleIcons extends LitElement {

    static getProperties(){
        return {icons:{type:Array},
            currentIcon:{type:String}
    }
    }

    static get styles(){
        return styling({})
    }

    get icons(){
        return this._icons
    }

    get cicuralIcons() {
        return this._cicuralIcons;
    }

   set icons(icons){
        const oldValue = this.icons;
        this._icons = icons ;
        this._cicuralIcons = new CircularBuffer(icons);
        this.currentIcon = this.cicuralIcons.element;
        this.requestUpdate("icons",oldValue);
    }

    toggleIcon(e){
        this.currentIcon = this.cicuralIcons.element;
        this.requestUpdate();
    }

    render(){
       return template({icon:this.currentIcon,
        click:this.toggleIcon.bind(this)
    });
    }

}


customElements.define("cwms-toggle-icons",CwmsToggleIcons);


class CircularBuffer {
    constructor(array){
        this._innerArray = [...array];
        this._currentIndex=0;
        this._length = array.length
    }

    get element(){
        const index = this._currentIndex;
        this._currentIndex = (index +1)%this._length;
        return this._innerArray[index];
    }

}