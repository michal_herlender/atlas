import { html } from '../../thirdparty/lit-element/lit-element.js'

export const template = (context) => html`
    <img src=${context.icon} @click=${context.click} role="button" />
`;