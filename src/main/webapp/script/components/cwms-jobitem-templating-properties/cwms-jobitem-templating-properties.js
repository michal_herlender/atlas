import {LitElement} from "../../thirdparty/lit-element/lit-element.js";
import {
  updateDefaultCalTypeLinkGenerator,
  updateDefaultServiceTypeUsageLinkGenerator,
  updateDefaultTurnLinkGenerator
} from "../../tools/UrlGenerator.js";
import {storeBookingInAddrId} from "../cwms-jobitem-search-form/store/actions.js";
import store from "../cwms-jobitem-search-form/store/store.js";
import {styling} from "./cwms-jobitem-templating-properties.css.js";
import {template} from "./cwms-jobitem-templating-properties.html.js";
import {MetaDataObtainer} from "../../tools/metadataObtainer.js"

class CwmsJobItemTemplatingProperties extends LitElement {
  static get properties() {
    return {
      jobId: {type: Number},
      jobType: {type: String},
      calTypes: {type: Array},
      locations: {type: Array},
      seletedTurnaround: {type: Number},
      selectedCalTypeId: {type: Number},
      bookingAddressId: { type: Number },
      defaultServiceTypeUsage: { type: Boolean, converter: (value, type) => (value == "true" ? true : false) },
    };
  }

  updateTurnaround(e) {
    const turn = e.currentTarget.value;
    this.preparePostFetch(updateDefaultTurnLinkGenerator(),{value:turn,jobId:this.jobId});
  }

  updateDefaultCalType(e){
    const typeId = e.currentTarget.value;
    this.preparePostFetch(updateDefaultCalTypeLinkGenerator(),{value:typeId,jobId:this.jobId});
  }


  updateDefaultServiceTypeUsage(e) {
      const usage= e.currentTarget.checked;
      this.preparePostFetch(updateDefaultServiceTypeUsageLinkGenerator(),{usage});
  }

  preparePostFetch(url, body) {
    return fetch(url, {
      method: "POST",
      credentials: "same-origin",
      headers: {
        "Content-Type": "application/json",
        ...MetaDataObtainer.csrfHeader
      },
      body: JSON.stringify(body),
    });
  }

  static get styles() {
    return styling({});
  }

  constructor() {
    super();
    this.seletedTurnaround = 7;
    store.subscribe(() => {
      const { bookingAddressId = 0 } = store.getState();
      this.bookingAddressId = bookingAddressId;
    });
  }


  connectedCallback(){
      super.connectedCallback();
      storeBookingInAddrId(this.bookingAddressId);
  }

  render() {
    return template(this);
  }
}

customElements.define(
  "cwms-jobitem-templating-properties",
  CwmsJobItemTemplatingProperties
);
