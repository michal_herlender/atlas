import { css } from '../../thirdparty/lit-element/lit-element.js'

export const styling = (context) => css`
    .fields {
       display: flex;
       flex-wrap: wrap;
    }

    .fields div {
        flex-basis: 50%;
    }
`;