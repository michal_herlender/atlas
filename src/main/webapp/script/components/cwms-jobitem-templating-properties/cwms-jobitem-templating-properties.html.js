import { html } from '../../thirdparty/lit-element/lit-element.js'
import { storeBookingInAddrId } from '../cwms-jobitem-search-form/store/actions.js';
import { stylesMixin } from '../tools/applicationStylesMixinTemplate.js';

export const template = (context) => html`
${stylesMixin}
    <form>
        <fieldset>
            <legend><cwms-translate code="newjobitemsearch.jobdefaults">Job Defaults</cwms-translate></legend>
<div class="fields">
					<div>
								<label><cwms-translate code="jobtype">Job Type</cwms-translate>:</label>
								<span>${context.jobType??""}</span>
						</div>
					<div>
						<label><cwms-translate code="jicontractreview.turnaround">Turnaround</cwms-translate>:</label>
							${selectTurnaroundTemplate(context) }
					</div>
					<div>
						<label><cwms-translate code="caltype">Caltype</cwms-translate></label>
                            <div>${selectCaltypeTemplate(context) }
                            ${defaultServiceTypeTemplate(context)}
                        </div>
					</div>
					<div>
						<label><cwms-translate code="newjobitemsearch.confirmloc">Confirm location</cwms-translate></label>
							${selectLocationTemplate(context) }
					</div></div>
        </fieldset>
    </form>
`;


const selectTurnaroundTemplate = context => html`
    <select @change=${e => context.updateTurnaround?.(e)}>
        ${Array(28).fill().map((_,i)=> html`<option ?selected=${i == context.seletedTurnaround} value=${i}>${i}</option>`) }
    </select>
`;

const selectLocationTemplate = context => html`
    <select @change=${e => storeBookingInAddrId(e.currentTarget.value)}>
    ${context.locations.map(l => html`<option ?selected=${l.key == context.bookingAddressId} value=${l.key}>${l.value}</option>`)}
    </select>
`;

const selectCaltypeTemplate = context => html`
    <select @change=${e => context.updateDefaultCalType?.(e)}>
    ${context.calTypes.map(ct => html`<option ?selected=${ct.id== context.selectedCalTypeId} value=${ct.id}>${ct.label}</option>`)} 
    </select>
` ;

const defaultServiceTypeTemplate = context => html`
        <br/>
        <input type="checkbox"  @change=${e => context.updateDefaultServiceTypeUsage?.(e)} ?checked=${context.defaultServiceTypeUsage}  />
        <cwms-translate code="newjobitemsearch.thisservicetypeonly" >Add instruments to basket using only this service type</cwms-translate>
`;