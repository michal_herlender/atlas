import { html } from "../../thirdparty/lit-element/lit-element.js";
import { repeat } from "../../thirdparty/lit-html/directives/repeat.js";
import { directive } from "../../thirdparty/lit-html/lit-html.js";

export let htmlTemplate = (context) => html`
<link rel="stylesheet" href="styles/structure/elements/table.css">
<link rel="stylesheet" href="styles/theme/theme.css">
<link rel="stylesheet" href="styles/text/text.css">
<div><cwms-translate code="address.transportmessage1"/></div>
<table class="default2 addressTransport1">
	<thead>
		<tr>
			<td colspan="6">
				<cwms-translate code="address.transportmessage2"/>
			</td>
		</tr>
		<tr>
			<th><cwms-translate code="businesscompany"/></th>
			<th><cwms-translate code="subdivision"/></th>
			<th><cwms-translate code="address.transportin"/></th>
            <th><cwms-translate code="address.transportout"/></th>
            <th><cwms-translate code="delete"/></th>
		</tr>
	</thead>
    <tbody>
        ${context.transportOptions ? repeat(context.transportOptions, (transportOption, index) => html`
		<tr>
			<td>${transportOption.businessCompany}</td>
			<td>${transportOption.businessSubdiv}</td>
			<td>
				${transportOption.localizedNameTransportIn ? html`${transportOption.localizedNameTransportIn}` : 
				transportOption.transportIn ? html`${transportOption.transportIn}` : html`<cwms-translate code="company.nonespecified"/>`}
			</td>
			<td>
				${transportOption.localizedNameTransportOut ? html`${transportOption.localizedNameTransportOut}` : 
				transportOption.transportOut ? html`${transportOption.transportOut}` : html`<cwms-translate code="company.nonespecified"/>`}
			</td>
			<td class="center">${transportOption.hasRightToDelete ? html`
				<img src="img/icons/delete.png" width="16" height="16"
					alt="${CwmsTranslate.getMessage('viewjob.imgdelete', 'Delete')}"
					title="${CwmsTranslate.getMessage('viewjob.imgdelete', 'Delete')}"
					@click=${(e) => context.delete(transportOption.addressSettingsId)}/>
			` : html``}</td>
		</tr>
        `) : ""}
	</tbody>
</table>
`;