import { LitElement } from "../../thirdparty/lit-element/lit-element.js";
import { htmlTemplate } from "./cwms-address-transportoptions.html.js";
import { styling } from "./cwms-address-transportoptions.css.js";
import {} from "../cwms-translate/cwms-translate.js";

class CwmsAddressTransportOptions extends LitElement {

    static get addressTransportOptionsURL() { return "addresstransportoptions.json?addressid=_addressId"; }
    static get deleteAddressSettingsURL() { return "deleteaddresssettingsforsubdiv.json?id=_id"; }

    static get properties() {
        return {
            addressId: { type: String },
            transportOptions: { type: Array }
        }
    }

    get styles() {
        return styling(this);
    }

    firstUpdated() {
        var url = CwmsAddressTransportOptions.addressTransportOptionsURL.replace("_addressId", this.addressId);
        fetch(url)
        .then(res => {
            return res.json();
        })
        .then(dto => {
            this.transportOptions = dto;
        });
    }

    render() {
        return htmlTemplate(this);
    }

    delete(addressSettingsId) {
        var url = CwmsAddressTransportOptions.deleteAddressSettingsURL.replace("_id", addressSettingsId);
        const csrfToken = document.querySelector("meta[name='_csrf']").getAttribute("content");
        fetch(url, {
            method: 'post',
			headers: {
                "X-CSRF-TOKEN": csrfToken
            }
        }).then(res => {
            return res.json();
        }).then(result => {
            if(result.success)
                this.transportOptions = this.transportOptions.filter(to => to.addressSettingsId != addressSettingsId);
            else
                alert(result.errors[0].message);
        });
    }
}

window.customElements.define('cwms-address-transportoptions', CwmsAddressTransportOptions);