import { CwmsAbstractWithPopup } from "../../cwms-abstract-with-popup/cwms-abstract-with-popup.js";
import CwmsTranslate from "../../cwms-translate/cwms-translate.js";
import { popUpContentTemplate, popUpReplaceTemplate } from "./cwms-edit-hire-item-popup.html.js";
import { fieldsetStyles } from "../../tools/styles/fieldset.css.js";
import { linkStyling } from "../../tools/styles/links.css.js";
import { textStyles } from "../../tools/styles/text.css.js";
import { tableStyles } from "../../tools/styles/table.css.js";
import { urlWithParamsGenerator } from "../../../tools/UrlGenerator.js";
import {MetaDataObtainer} from "../../../tools/metadataObtainer.js";

export const STATE_EDIT="editItem";
export const STATE_REPLACE="replace";
export const STATE_REPLACE_RESULTS="replace_results"

class CwmsEditHireItemPopUp extends CwmsAbstractWithPopup {

    #state;

    static get properties(){
        return {
            ...super.properties,
            hireItem:{type:Object},
            defaultUkasCal:{type:Number},
            enquiry:{type:Boolean}, 
            state:{type:String},
            servicetypes:{type:Array},
            barcode:{type:Number},
            errorMessage:{type:String},
            replacementRason:{type:String},
            searchResult:{type:Object}
        }
    }

    get value(){
        return this.hireItem;
    }

    get state(){
        return this.#state;
    }

    set popUpContentTemplate(_){
        //empty method as in the current implementation we do not want to store pop-up tempalte
    }


    get popUpContentTemplate(){
        return this.state==STATE_EDIT? popUpContentTemplate(this):popUpReplaceTemplate(this);
    }

    set state(state){
        const oldValue =this.state;
        this.#state = state;
        if(this.showPopUp) this.invokePopUp();
        this.requestUpdate("state",oldValue);
    }

    static get styles(){
        return [...super.styles, fieldsetStyles, linkStyling(), tableStyles, textStyles];
    }



    resolveContent(){
        return this.state==STATE_EDIT? popUpContentTemplate(this):popUpReplaceTemplate(this);
    }

    constructor(){
        super();
        this.state = STATE_EDIT;
        this.popUpTitle = CwmsTranslate.messagePromise("hireview.edititem.title","Edit Hire Item Values")
    }

    connectedCallback(){
        super.connectedCallback();
        this.showPopUp = true;
    }

    updateField(field,item){
        return e => {
            switch(field){
                case "barcode":
                    this.barcode = e.currentTarget.value;
                    break;
                case "replacementReason":
                    this.replacementRason = e.currentTarget.value;
                    break
                case "itemcaltype":
                    if(e.currentTarget.value == 1){
                        this.hireItem.ukasCalCost = this.defaultUkasCal;
                    }
                    else {
                        this.hireItem.ukasCalCost = 0;
                    }
                    this.requestUpdate();
                    break;
            }

        }
    }


    updateInstrument(e){
        e.preventDefault();
        const fd = new FormData(e.currentTarget);
        const params = Object.fromEntries(fd.entries());
        fetch(urlWithParamsGenerator("hireitem/updateHireItemValues.json",{...params,hireItemId:this.hireItem.id}),{
            method:"PUT",
            credentials:"same-origin",
            headers:MetaDataObtainer.csrfHeader
        })
        .then(res => res.ok? res.json():Promise.reject(res.statusText))
        .then(res => res.right? res.value:Promise.reject(res.value))
        .then(res => {
            this.hireItem = res
            this.dispatchEvent(new Event("change"))
            this.invokePopUp();
            this.showPopUp = false;
        })
        .catch(msg => this.errorMessage = msg)
    }

    render(){
        return this.renderPopUp();
    }

    searchReplacement(e){
        fetch(urlWithParamsGenerator("hireitem/searchReplacement.json",{barcode:this.barcode}))
        .then(res => res.ok?res.json():Promise.reject(res.statusText))
        .then(res => { 
            this.searchResult = res;
            if(!res) {
                this.errorMessage = "Hire instrument could not be found";
            }
            this.state = STATE_REPLACE_RESULTS
        }
            )
        .catch(err => this.errorMessage = err)
    }

    confirmReplacement(e){
        fetch(urlWithParamsGenerator("hireitem/replaceHireItemInstrument.json",{hireItemId:this.hireItem.id,
            newPlantId:this.barcode, replacementRason:this.replacementRason}),
            {
                method: "PUT",
                headers:MetaDataObtainer.csrfHeader,
                credentials:"same-origin"
            })
            .then(res => res.ok?res.json():Promise.reject(res.statusText))
            .then(res => res.right? window.location.reload():Promise.reject(res.value))
            .catch(msg => this.errorMessage = msg);

    }

}


customElements.define("cwms-edit-hire-item-popup",CwmsEditHireItemPopUp)