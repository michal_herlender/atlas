import { html } from "../../../thirdparty/lit-element/lit-element.js";
import "../../cwms-translate/cwms-translate.js";
import { cwmsTranslateDirective } from "../../cwms-translate/cwms-translate-directive.js";
import { STATE_REPLACE } from "./cwms-edit-hire-item-popup.js";

export const popUpContentTemplate = context => html`
    <form @submit=${context.updateInstrument}>
      <fieldset>
    ${context.errorMessage? html`
    <div class="warningBox1">
      <div class="warningBox2">
        <div class="warningBox3">
          ${context.errorMessage}
        </div></div>
    </div>`:html``}
        <ol class="nopadding">
          <li>
            <label><cwms-translate code="serviceType">Service Type</cwms-translate>:</label>
            <select name="newCaltypeId" @change=${context.updateField("itemcaltype",context.hireItem)}>
            ${context.servicetypes?.map(st => html`<option value=${st.key}>${st.value}</option>`)}
            </select>
          </li>
          <li>
            <label>
              <cwms-translate code="hireview.edititem.UKASCalCost">Accr. Cal Cost</cwms-translate>:
            </label>
            <input type="number" step="0.01" name="newUkasCalCost" .value=${context.hireItem.ukasCalCost} />
            <cwms-translate code="hireview.edititem.pleaseAskCustomerSvcForUKASCalCost"> Please ask Customer Services for a Accr. Cal Cost.</cwms-translate>
          </li>
          <li>
            <label>
              <cwms-translate code="hirecost">Hire Cost</cwms-translate>:
            </label>
            <input step="0.01" type="number" name="newHireCost" .value=${context.hireItem.hireCost} />
          </li>
          ${!context.enquiry?html`
            <li>
              <label>
                <cwms-translate code="hireview.edititem.replaceItemLabel">Replace Item</cwms-translate>:
              </label>
              <span>
                <a role="button" class="mainlink"  title=${cwmsTranslateDirective("hireview.edititem.replaceItemLabel","Replace item")} @click=${_ => context.state = STATE_REPLACE}><cwms-translate code="hireview.edititem.replaceItemLabel">Replace Item</cwms-translate></a>
              </span>
            </li>
          `:html``}
          <li>
            <label>&nbsp;</label>
            <input
              type="submit"
              value=${cwmsTranslateDirective("submit","Submit")}
              
            />
          </li>
        </ol>
      </fieldset>
    </form>
`

export const popUpReplaceTemplate = (context) => html`
  <fieldset>
    ${context.errorMessage? html`
    <div class="warningBox1">
      <div class="warningBox2">
        <div class="warningBox3">
          ${context.errorMessage}
        </div></div>
    </div>`:html``}
    <ol>
      <li>
        <label><cwms-translate code="web.barcode">Trescal Barcode</cwms-translate>:</label>
        <input type="number" id="newInstBarcode" @input=${context.updateField("barcode", {})} />
      </li>
      ${context.searchResult
        ? html`
            <li>
              <table class="default4 viewHireContract" summary="this table shows the replacement hire instrument to user">
                <thead>
                  <tr>
                    <th><cwms-translate code="web.barcode">Trescal Barcode</cwms-translate></th>
                    <th><cwms-translate code="plantno">Plant No</cwms-translate></th>
                    <th><cwms-translate code="description">Description</cwms-translate></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>${context.searchResult.plantId}</td>
                    <td>${context.searchResult.plantNo}</td>
                    <td>${context.searchResult.fullInstrumentModelName}</td>
                  </tr>
                </tbody>
              </table>
            </li>
            <li>
              <label>
                <cwms-translate code="hireview.edititem.replacementReason">Replacement Reason</cwms-translate>
              </label>
              <textarea name="replacereason" class="width70" rows="4" value="" @input=${context.updateField("replacementReason", {})}></textarea>
            </li>
          `
        : html``}
      <li>
        <label>&nbsp;</label>
        ${context.state == STATE_REPLACE
          ? html` <input type="button" value=${cwmsTranslateDirective("search", "Search")} @click=${context.searchReplacement} />`
          : html`
              <input
                type="button"
                value=${cwmsTranslateDirective("hireview.edititem.confirmReplacement", "Confirm Replacement")}
                @click=${context.confirmReplacement}
              />
            `}
      </li>
    </ol>
  </fieldset>
`;