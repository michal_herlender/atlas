import { css } from '../../../thirdparty/lit-element/lit-element.js'

export const styling = (context) => css`
    .del {
        width:3%;
    }

    .itemno,
    .ready
    {
        width: 5%;
        text-align: center;
    }

    .desc {
        width: 36%;
    }

    .plantno {
        width: 15%;
    }

    .caltype {
        width: 6%;
    }

    .ukascost, .hirecost {
        text-align: right; 
        width: 8%;
    }

    .hiredays {
        width: 10%;
        text-align: center;
    }

    .finalcost {
        width: 9%;
        text-align: right;
    }

    .inline {
        display:inline;
    }

    .del { 
        text-align: center;
        width: 3%;
    }


    .editHireItem {
        background: url(./img/icons/edit_details.png) no-repeat center center;
        padding: 8px;
        cursor: pointer;
    }


    .hiresuspended_false {
        background: url(./img/icons/pause.png) no-repeat center center;
        padding: 8px;
        cursor: pointer;
    }

    .hiresuspended_true {
        background: url(./img/icons/play.png) no-repeat center center;
        padding: 8px;
        cursor: pointer;
    }

    .reset {
	    background: url(./img/icons/reset.png) no-repeat center center;
        padding: 8px;
        cursor: pointer;
    }

    .cwms-tooltip table.default2 {
      margin: 6px auto;
      width: 98%;
    }

    .attention {
        color: var(--cwms-error-message-color,#c00);
    }

    .hi-del {
        float: right;
    }

    .highlight {
        background-color: var(--cwms-highlight-color,#FFCECE);
    }

    .highlight-gold {
        background-color: Gold;
    }

    textarea {
        width: 300px;
    }

    .container {
        width:100%;
        display:flex;
        justify-content:center;
        align-items:center;
        height:100px;
    }

    button.reset-off-hire {
        height: 41px;
        width: 100px;
    }
`;