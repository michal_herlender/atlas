import { LitElement, html } from "../../../thirdparty/lit-element/lit-element.js";
import { structureStyles } from "../../tools/styles/structure.css.js";
import { fieldsetStyles } from "../../tools/styles/fieldset.css.js";
import { tableStyles } from "../../tools/styles/table.css.js";
import { template as spinnerTemplate } from "../../cwms-spinner/cwms-spinner.html.js";
import { generalStyling } from "../../cwms-spinner/cwms-spinner.css.js";
import { styling } from "./cwms-hire-items.css.js";
import { template, toggleItemSuspensionContentTemplate, offHirePopUpTemplate, resetOffHireDatePopUpContentTemplate } from "./cwms-hire-items.html.js";
import { linkStyling } from "../../tools/styles/links.css.js";
import { textStyles } from "../../tools/styles/text.css.js";
import { applyArgumentsToMessage } from "../../cwms-translate/cwms-translate-directive.js";
import CwmsTranslate from "../../cwms-translate/cwms-translate.js";
import { popUpStyles } from "../../tools/styles/popUp.css.js";
import { popUpTemplateWithoutStyles as popUpTemplate } from "../../tools/popUpTemplate.html.js";
import { MetaDataObtainer } from "../../../tools/metadataObtainer.js";
import { urlWithParamsGenerator } from "../../../tools/UrlGenerator.js";
import { DateUtils } from "../../tools/date-utils.js";
import "../cwms-edit-hire-item-popup/cwms-edit-hire-item-popup.js";
import "../../cwms-spinner/cwms-spinner.js"

export const CHECKED_ITEM = "checked"
const ITEM_SUSPENDED ="suspended"


export const OFF_HIRE_STATE = "offHire";
const ITEM_EDIT_STATE="edit";
const INITIAL_STATE = "initial";
const TOGGLE_ITEM_SUSPENSION = "toggleItemSuspension";
const STATE_RESET_OFFHIRE="resetOffHire";

class CwmsHireItems extends LitElement {

    static get properties(){
        return {
            items:{type:Array},
            status:{type:Object},
            currencyCode:{type:String},
            enquiry:{type:Boolean, 
                converter: (value, type) => (value == "true" ? true : false),
            },
            servicetypes:{type:Array},
            defaultUkasCal:{type:Number},
            state:{type:String},
            popUpTemplate:{type:Object}
        }
    }

    get isAnyChecked() {
        return this.items.find(i => i.checked);
    }

    get showPopUp() {
        return this.state == OFF_HIRE_STATE || this.state == TOGGLE_ITEM_SUSPENSION
            || this.state == ITEM_EDIT_STATE || this.state == STATE_RESET_OFFHIRE
        ;
    }

    static get styles(){
        return [tableStyles, structureStyles, linkStyling() ,popUpStyles ,styling(), fieldsetStyles, textStyles, generalStyling];
    }


    editItem(item){
        this.popUpTemplate = html`<cwms-edit-hire-item-popup .enquiry=${this.enquiry} .hireItem=${item} .servicetypes=${this.servicetypes} .defaultUkasCal=${this.defaultUkasCal} @cwms-close-popup=${_ => this.state=INITIAL_STATE} @change=${this.updateField("hireItem",item)} ></cwms-edit-hire-item-popup>`
        this.state = ITEM_EDIT_STATE;
    }

    toggleItemSuspension(item){
        this.popUpTemplate = popUpTemplate(new ToggleItemSuspensionPopUpContext(item, _ => this.state = INITIAL_STATE, this.updateField(ITEM_SUSPENDED,item), c => this.popUpTemplate=popUpTemplate(c) ));
        this.state = TOGGLE_ITEM_SUSPENSION;
    }


    offHireItems(){
        this.state = OFF_HIRE_STATE;
        this.popUpTemplate =  this.items.filter(i=>i[CHECKED_ITEM]).map((i,ind,{length}) => (resolve) => 
            (new OffHirePopUpContext(i,_ => {
                    this.state = INITIAL_STATE;
                    resolve();
                    },ind+1==length, c => this.popUpTemplate=popUpTemplate(c)))
        ).reduce((p,c)=>p.then(_=> new Promise(r => {
            const context = c(r);
            this.state = OFF_HIRE_STATE;
            this.popUpTemplate = popUpTemplate(context)
        })),Promise.resolve())
    }

    resetOffHire(item){
        this.state = STATE_RESET_OFFHIRE;
        this.popUpTemplate = popUpTemplate(new ResetOffHirePopUpContext(item,_=>this.state=INITIAL_STATE, c => this.popUpTemplate=popUpTemplate(c)));
    }

    toggleAll(event) {
        this.items = [...this.items.map(i => ({...i,[CHECKED_ITEM]:event.currentTarget.checked}))];
    }

    updateField(field,item) {
        return e => {
            const value = field == CHECKED_ITEM?e.currentTarget.checked:e.currentTarget.value; 
            if(field=="hireItem")
                this.items = [...this.items.map(i => i.id == item.id?value:i)];
            this.items = [...this.items.map(i => i.id == item.id?{...i,[field]:value}:i)];
            this.requestUpdate("items")
        }
    }

    render(){
       return template(this);
    }

}

class PopUpContext {
    #title;
    #content;
    #onHide;
    constructor(title, content, hide) {
        this.#title = title;
        this.#content = content;
        this.#onHide = hide;
    }

    get title(){
        return this.#title;
    }

    set title(title){
        this.#title = title;
    }

    get overlayContent(){
        return this.#content;
    }

    set overlayContent(content) {
        this.#content = content;
    }

    get hide(){
        return this.#onHide;
    }
}


class ResetOffHirePopUpContext extends PopUpContext {

    #updatePopUpFunction;
    #hireItem;
    #errorMessage;

    set errorMessage(message){
        this.#errorMessage = message;
        this.#updatePopUpFunction(this);
    }

    get errorMessage(){
      return this.#errorMessage;
    }
    constructor(hireItem,hide,updatePopUpFunction){
        super(CwmsTranslate.messagePromise("hireview.resetOffHireDate.confirmAction","Are you sure you wish to reset the off hire date of this item?"),undefined,hide);
        this.#hireItem = hireItem;
        this.overlayContent =resetOffHireDatePopUpContentTemplate(this);
        this.#updatePopUpFunction = updatePopUpFunction;
    }

    resetOffHireDate(){
        this.overlayContent =html`<div class="container">${spinnerTemplate()}</div>`
        this.#updatePopUpFunction(this);
        fetch(urlWithParamsGenerator("hireitem/resetOffHireDate.json",{hireItemId:this.#hireItem.id}),{
            method:"PUT",
            credentials:"same-origin",
            headers:MetaDataObtainer.csrfHeader
        })
        .then(res => res.ok?res.json():Promise.reject(res.statusText))
        .then(res =>{
            if(res.right)
                window.location.reload();
            else
                this.errorMessage = res.value;
        }).catch(msg=> this.errorMessage = msg);
    }
}


class OffHirePopUpContext extends PopUpContext {
    #isLastOffHireItem;
    #hireItem;
    #errorMessage;
    #updatePopUpFunction;
    offHireCond;
    offHireDate;

    set errorMessage(message){
        this.#errorMessage = message;
        this.#updatePopUpFunction(this);
    }

    get errorMessage(){
      return this.#errorMessage;
    }

    get overlayContent(){
        return offHirePopUpTemplate(this)(this.#hireItem);
    }

    constructor(hireItem,hide,isLastOffHireItem, updatePopUpFunction) {
        super(undefined,undefined,hide);
        this.#hireItem = hireItem;
        this.title = CwmsTranslate.messagePromise("hireview.offhireitems.title","Off hire items");
        this.#isLastOffHireItem = isLastOffHireItem;
        this.#updatePopUpFunction = updatePopUpFunction;
        this.offHireDate=DateUtils.dateToISODateString(new Date);
    }


    updateField(field,item) {
        return e => {
            item[field] =e.currentTarget.value;
        }
    }

    offHireItem(item){
        fetch(urlWithParamsGenerator("hireitem/offHireItem.json",{hireItemId:item.id,offHireCond:this.offHireCond,offHireDate:this.offHireDate,accDTOs:[],genOffHireDoc:this.#isLastOffHireItem}),{
            method:"POST",
            headers:MetaDataObtainer.csrfHeader,
            credentials:"same-origin"
        })
        .then(res => res.ok?res.json():Promise.reject(res.statusText))
        .then(res => {
            if(!res.right){
                this.errorMessage = res.value;
            }
            else if (this.#isLastOffHireItem)
                    window.location.reload();
            else
                this.hide();
        })
        .catch(msg => this.errorMessage = msg);
    }
}


class ToggleItemSuspensionPopUpContext extends PopUpContext {

    #date;
    #itemUpdated;
    #hireItem;
    #errorMessage;
    #updatePopUpFunction;

    set errorMessage(message){
        this.#errorMessage = message;
        this.#updatePopUpFunction(this);
    }

    get errorMessage(){
      return this.#errorMessage;
    }

    get overlayContent(){
        return toggleItemSuspensionContentTemplate(this)(this.#hireItem);
    }

    constructor(hireItem,hide, itemUpdated,updatePopUpFunction){
        super(undefined,undefined,hide);
        this.#itemUpdated = itemUpdated;
        this.#hireItem = hireItem;
        this.#date=new Date;
        this.#updatePopUpFunction = updatePopUpFunction;
        this.title = (hireItem.suspeded? CwmsTranslate.messagePromise("hireview.togglesuspension.suspendtitle","Suspend Hire Item {0}"):CwmsTranslate.messagePromise("hireview.togglesuspension.completetitle","Complete Hire Item {0} Suspension")).then(msg => applyArgumentsToMessage(msg,hireItem.itemno) )
    }

    updateDate(e){
        this.#date = e.currentTarget.valueAsDate;
    }

    action(item){
        const url = item.suspended?urlWithParamsGenerator("hire/completeHireItemSuspension.json",{hireItemId:item.id,date:DateUtils.dateToISODateString(this.#date)}):
urlWithParamsGenerator("hire/suspendHireItem.json",{hireItemId:item.id,date:DateUtils.dateToISODateString(this.#date)});
				fetch(url,
				{
					method:"PUT",
					credentials:"same-origin",
					headers:MetaDataObtainer.csrfHeader
				}
				)
				.then(res => res.ok ? res.json():Promise.reject(res.statusText))
				.then(result => {
						// hire item suspended?
						if (result.right)
						{
                            this.#itemUpdated({currentTarget:{value:result.value}});
                            this.hide();
						}
						else
						{
                            this.errorMessage = result.value;
						}
					}
				)
				.catch(msg => this.errorMessage=msg);
				;
    }

}

customElements.define("cwms-hire-items",CwmsHireItems);