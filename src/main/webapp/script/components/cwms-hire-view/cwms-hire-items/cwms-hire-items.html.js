import {html} from '../../../thirdparty/lit-element/lit-element.js'
import {CHECKED_ITEM} from './cwms-hire-items.js';
import {cwmsTranslateDirective} from '../../cwms-translate/cwms-translate-directive.js';
import {
    currencyFormatWithZeroDirective,
    currencySymbolDirective,
    dateFormatDirective,
    dateFormatFunction
} from '../../tools/formatingDirectives.js';
import "../../cwms-tooltips/cwms-instrument-link-and-tooltip/cwms-instrument-link-and-tooltip.js";
import "../../cwms-tooltips/cwms-static-link-and-tooltip/cwms-static-link-and-tooltip.js";
import {DateUtils} from "../../tools/date-utils.js";
import "../../cwms-preset-comments/cwms-preset-comments.js";

export const template = (context) => html` 
${[
    context.showPopUp ? context.popUpTemplate : html``,
    context.enquiry ? enquiryItemsTemplate(context) : contractItemsTemplate(context)
]}`;
const enquiryItemsTemplate = context =>  html`
<table class="default4">
    <thead>
        <tr>
            <td colspan=10>
								<cwms-translate code="hirehome.hireitems">Hire Items</cwms-translate>
								(<span class="hireItemCount">${context.items?.length}</span>)
               ${context.isAnyChecked ? html`
								<a role="button" class="hi-del"
									@click=${_ => {
}} >
									<img src="img/icons/delete.png" height="16" width="16"
										alt=${cwmsTranslateDirective("hireview.deleteselectedhis", "")}
										title=${cwmsTranslateDirective("hireview.deleteselectedhis", "")} />
								</a>
               ` : html`<div class="hi-del attention"><cwms-translate code="hireview.tickitemstodelete">tick to delete item</cwms-translate></div>`}
            </td>
        </tr>
        <tr>
									<th class="itemno" scope="col"><cwms-translate code="hirehome.item">Item</cwms-translate></th>
									<th class="desc" scope="col"><cwms-translate code="instmodelname">Instrument Model Name</cwms-translate></th>
									<th class="plantno" scope="col"><cwms-translate code="hirehome.plantnoserial">Plant No (Serial)</cwms-translate></th>
									<th class="caltype" scope="col"><cwms-translate code="servicetype">ServiceType</cwms-translate></th>
									<th class="ukascost" scope="col"><cwms-translate code="hiresearchinstr.ukascal">Accr. Cal</cwms-translate> (${currencySymbolDirective(context.currencyCode)})</th>
									<th class="hirecost" scope="col"><cwms-translate code="hireview.dayhire">Day Hire</cwms-translate> (${currencySymbolDirective(context.currencyCode)})</th>											
									<th class="ready" scope="col"><cwms-translate code="hireview.ready">Ready</cwms-translate></th>
									<th class="del" scope="col">
										<input type="checkbox" name="hiActionAll"
                                        @input=${context.toggleAll}
										title=${cwmsTranslateDirective('hireview.select/deselect', "Select/Deselect")} />
									</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <td colspan=10></td>
        </tr>
    </tfoot>
    <tbody>
        ${context.items?.length ? 
        context.items.map(enquiryItemRowTemplate(context)): html`
            <tr>
                <td colspan="${cols}" class="text-center bold">
                    <cwms-translate code="hireview.nohireitemsadded">No Hire items added</cwms-translate>
                </td>
            </tr>
        `}
    </tbody>
</table>
`;


const enquiryItemRowTemplate = context => hi => html`
<tr>
    <td class="itemno">
        ${hi.offHire?html``:html`
            <div class="editHireItem inline" role="button" @click=${_ => context.editItem(hi)}
                title=${cwmsTranslateDirective('hireview.edithireitem',"Edit Hire Item")} >&nbsp;
            </div>
        `}
        ${hi.itemno}
    </td>
    <td>
        ${hi.plantId ? html`<cwms-instrument-link-and-tooltip .plantid=${hi.plantId} .modelName=${hi.modelName} ?displayModel=${true}></cwms-instrument-link-and-tooltip>` : hi.description}
    </td>
    <td>
        ${hi.plantNo} (${hi.serialNo})
    </td>
    <td>
        ${hi.calTypeShorName ?? html`<cwms-translate code="notapplicable" >Not Applicable</cwms-translate>`}
    </td>
    <td class="ukascost">
        ${currencyFormatWithZeroDirective(hi.ukasCalCost, context.currencyCode)}
    </td>
    <td>
        ${currencyFormatWithZeroDirective(hi.hireCost, context.currencyCode)}
    </td>
    <td class="ready">
        ${hi.plantId && hi.instHires.length == 0 ? readySuccessTemplate : hi.instHires
    .map((it, indx) => ({...it, indx}))
            .filter(it => !it._1)
            .map(it => it.indx ==0 && !it._2? readySuccessTemplate:readyCrossTemplate)}
    </td>
    <td class="del">
        ${context.status.name != "Complete"?html`
        <input type="checkbox" name="hItemAction" 
        ?checked=${hi[CHECKED_ITEM]}
        title="${cwmsTranslateDirective('hireview.ticktooffhire',"tick to off hire item")}"
        @click=${context.updateField(CHECKED_ITEM,hi)}/>`:html``}
    </td>
</tr>
`
const readyCrossTemplate = html`
    <img src="img/icons/redcross.png" width="16" height="16"
        title=${cwmsTranslateDirective('hireview.hireinstrumentnotavailable',"Hire Instrument Not Available For Hire")} />
`

const readySuccessTemplate = html`
"<img src='img/icons/success.png' width='16' height='16'
														title='${cwmsTranslateDirective("hireview.hireinstrumentavailable","Hire Instrument Available For Hire")}'/>"`

const contractItemsTemplate = context => html`
<table class="default4">
    <thead>
        <tr>
            <td colspan=11>
                <cwms-translate code="hirehome.hireitems">Hire Items</cwms-translate>
								(<span class="hireItemCount">${context.items?.length}</span>)
                ${context.isAnyChecked? html`
                    <a role="button"  class="hi-del mainlink" 
                        @click=${_ => context.offHireItems()}>
                        <cwms-translate code="hireview.offhire">Off Hire</cwms-translate>
                    </a>&nbsp;
                ` : context.status.accepted ? html`
                <div class="hi-del attention"><cwms-translate code="hireview.tickitemstooffhire">Tick Items To Off Hire</cwms-translate></div>
                ` : html``}
            </td>
        </tr>
        <tr>

									<th class="itemno" scope="col"><cwms-translate code="hirehome.item">Item</cwms-translate></th>
									<th class="desc" scope="col"><cwms-translate code="instmodelname">Instrument Model Name</cwms-translate></th>
									<th class="plantno" scope="col"><cwms-translate code="hirehome.plantnoserial">Plant No (Serial)</cwms-translate></th>
									<th class="caltype" scope="col"><cwms-translate code="servicetype">Service Type</cwms-translate></th>
									<th class="ukascost" scope="col"><cwms-translate code="hiresearchinstr.ukascal">Accr. Cal</cwms-translate> (${currencySymbolDirective(context.currencyCode)})</th>
									<th class="hirecost" scope="col"><cwms-translate code="hireview.dayhire" >Day Hire</cwms-translate> (${currencySymbolDirective(context.currencyCode)})</th>
									<th class="hiredays" scope="col"><cwms-translate code="hireview.hiredays">Hire Days</cwms-translate></th>
									<th class="finalcost" scope="col"><cwms-translate code="hirehome.finalcost">Final Price</cwms-translate> (${currencySymbolDirective(context.currencyCode)})</th>
									<th class="del" scope="col">
											<input type="checkbox" name="hiActionAll"
                                            @change=${context.toggleAll}
												title=${cwmsTranslateDirective('hireview.select/deselect', "Select/Deselect")} />
									</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <td colspan=11></td>
        </tr>
    </tfoot>

    <tbody>
        ${context.items?.length ? 
        context.items.map(contractItemRowTemplate(context)): html`
            <tr>
                <td colspan="${cols}" class="text-center bold">
                    <cwms-translate code="hireview.nohireitemsadded">No Hire items added</cwms-translate>
                </td>
            </tr>
        `}
    </tbody>
</table>
`;


const contractItemRowTemplate = context => hi => html`
<tr class=${hi.suspended?"highlight":!hi.offHire?"highlight-gold":""} >
    <td class="itemno">
        ${hi.offHire?html``:html`
            <div class="editHireItem inline" role="button" @click=${_ => context.editItem(hi)}
                title=${cwmsTranslateDirective('hireview.edithireitem',"Edit Hire Item")} >&nbsp;
            </div>
        `}
        ${hi.itemno}
    </td>
    <td>
        ${hi.plantId ? html`<cwms-instrument-link-and-tooltip .plantid=${hi.plantId} .modelName=${hi.modelName} ?displayModel=${true}></cwms-instrument-link-and-tooltip>` : hi.description}
    </td>
    <td>
        ${hi.plantNo} (${hi.serialNo})
    </td>
    <td>
        ${hi.calTypeShorName ?? html`<cwms-translate code="notapplicable" >Not Applicable</cwms-translate>`}
    </td>
    <td class="ukascost">
        ${currencyFormatWithZeroDirective(hi.ukasCalCost, context.currencyCode)}
    </td>
    <td class="hirecost">
        ${currencyFormatWithZeroDirective(hi.hireCost, context.currencyCode)}
    </td>
    <td class="hiredays">
        ${hi.hireDays ? hi.hireDays : ''}
        ${hi.offHire ? html`
        <cwms-static-link-and-tooltip .objectName=${`OH ${dateFormatFunction(hi.offHireDate)}`} popUpWidth="250px">
            <p class="padding2">${hi.offHireCondition}</p>
        </cwms-static-link-and-tooltip>`:html``}
        ${daySuspendedTemplate(hi)}
    </td>
    <td class="finalcost "> 
        ${hi.offHire? html`${currencyFormatWithZeroDirective(hi.finalCost,context.currencyCode)}`:hi.suspended? html`
        <span><cwms-translate code="hireview.suspended">Suspended</cwms-translate></span>
        <div class="hiresuspended_true inline" role="button" @click=${_ => context.toggleItemSuspension(hi)}
            title=${cwmsTranslateDirective('hireview.hireitemcurrentlysuspended',"hire item currently suspended")}>&nbsp;</div>

        `:
    html`
        <span><cwms-translate code="hiresearchinstr.onhire">On Hire</cwms-translate></span>
        <div role="button" @click=${_ => context.toggleItemSuspension(hi)} class="hiresuspended_false inline"
            title=${cwmsTranslateDirective('hireview.suspendhireitem',"suspend hire item")} >&nbsp;</div>
    `}
    </td>
    <td>
        ${context.status.name =="Complete" ||hi.offHire ? html`
        <div class="reset" role="button" @click=${_=>context.resetOffHire(hi)} title="${cwmsTranslateDirective("hireview.resetoffhiredate","Reset off hire date of item")}">
            &nbsp;
        </div>
        `:
    html`
        <input type="checkbox" name="hItemAction" 
        ?checked=${hi[CHECKED_ITEM]}
        title="${cwmsTranslateDirective('hireview.ticktooffhire',"tick to off hire item")}"
        @click=${context.updateField(CHECKED_ITEM,hi)}/>
    `}
    </td>
</tr>
`


const daySuspendedTemplate = hi => hi.daysSuspended>0 ?html`
    <cwms-static-link-and-tooltip .objectName=${`${hi.daysSuspended} suspended`} popUpWidth="250px">
    ${hi.suspendItems.length > 0 ? html`
    <div class="cwms-tooltip">
        <table class="default2 marg">
            <thead>
                <tr>
                    <th><cwms-translate code="hireview.no">NO</cwms-translate></th>
                    <th><cwms-translate code="hireview.startdate">Start Date</cwms-translate></th>
                    <th><cwms-translate code="hireview.enddate">End Date</cwms-translate></th>
                    <th><cwms-translate code="hireview.days">Days</cwms-translate></th>
                </tr>
            </thead>
            <tbody>
                ${hi.suspendItems.map((hsi,indx) => html`
                    <tr>
                        <td>${indx}</td>
                        <td>${dateFormatDirective(hsi.startDate)}</td>
                        <td>${dateFormatDirective(hsi.endDate)}</td>
                        <td>${hsi.daysSuspended}</td>
                    </tr>
                `)}
            </tbody>
        </table>
    </div>
    `:html``}
    </cwms-static-link-and-tooltip>
`:html``


export const toggleItemSuspensionContentTemplate = context => hi => html`
<div id="toggleHireItemSuspensionOverlay">
    <fieldset>
    ${context.errorMessage? html`
    <div class="warningBox1">
      <div class="warningBox2">
        <div class="warningBox3">
          ${context.errorMessage}
        </div></div>
    </div>`:html``}
        <ol class="nopadding">
            <li>
                <label>${hi.suspended?html`<cwms-translate code="hireview.enddate" >End Date</cwms-translate>`:html`<cwms-translate code="hireview.startdate" >Start Date</cwms-translate>`} </label> 
                <input type="date" name="suspenddate" id="suspenddate" .value=${DateUtils.dateToISODateString(new Date)} @change=${e => context.updateDate(e)}  />
            </li>								
            <li>
                <label>&nbsp;</label>
                <button @click=${_ =>  context.action(hi)}><cwms-translate code=${hi.suspended?"hireview.togglesuspension.activateitem":"hireview.togglesuspension.suspenditem"}>${hi.suspended?"Activate Item":"Suspend Item"}</cwms-translate>  </button>
            </li>
        </ol>
    </fieldset>
</div>
`;


export const offHirePopUpTemplate = (context) => (hi) =>
  html`
    <fieldset>
    ${context.errorMessage? html`
    <div class="warningBox1">
      <div class="warningBox2">
        <div class="warningBox3">
          ${context.errorMessage}
        </div></div>
    </div>`:html``}
      <ol class="nopadding">
        <li>
          <p class="bold">
              <cwms-translate code="hireview.offhire.reviewItem">
            Please review the item to be off hired below. Untick any accessories which have not been returned and choose an appropriate status. Any accessories
            marked as not returned will be displayed as such on the off hire note and will be de-activated pending review for the next enquiry.
            </cwms-translate>
          </p>
        </li>
        <li>
          <label><cwms-translate code="hireview.offhire.offHiringItem" >Off Hiring Item</cwms-translate>: </label>
          <div class="float-left padtop">
            <span class="bold"> ${hi.itemno} - ${hi.modelName} </span>
          </div>
          <div class="clear"></div>
        </li>
      </ol>
      <ol class="nopadding">
        <li>
          <label class="twolabel"><cwms-translate code="docs.offhirecondition">Hire Condition</cwms-translate>:</label>
          <cwms-preset-comments type="OFF_HIRE">
          <textarea name="offhirecond" id="offhirecond" @input=${context.updateField("offHireCond",context)} class="width70 presetComments OFF_HIRE" maxlength="100" rows="4" spellcheck="false"></textarea>
          </cwms-preset-comments>
        </li>
        <li>
          <label><cwms-translate code="hirevire.offhire.offHireDate">Off Hire Date</cwms-translate>:</label>
          <input type="date" name="offhiredate" id="offhiredate" @input=${context.updateField("offHireDate",context)} .value=${hi.offHireDate??DateUtils.dateToISODateString(new Date())} autocomplete="off" />
        </li>
        <li>
          <label>&nbsp;</label>
          <input type="button" value="Off Hire Item" @click=${_=>context.offHireItem(hi)} />
        </li>
      </ol>
    </fieldset>
  `;


export const resetOffHireDatePopUpContentTemplate = context => html`
<div class="container">
<button @click=${_ => context.resetOffHireDate()} class="reset-off-hire"><cwms-translate code="accept">Accept</cwms-translate></button> 
<span>&nbsp;</span>
<button @click=${_ => context.hide()} class="reset-off-hire"><cwms-translate code="cancel">Cancel</cwms-translate></button>
</div>
`