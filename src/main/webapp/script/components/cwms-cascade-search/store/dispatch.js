import {
    SELECT_ADDRESS,
    SELECT_COMPANY,
    SELECT_CONTACT,
    SELECT_LOCATION,
    SELECT_SUBDIV,
    UPDATE_PREFILL_FLAGS,
    UPDATE_PREFILLS,
    UPDATE_TERM
} from "./actionsTypes.js"

let dispatch; export default dispatch = function(state,command){
    switch(command.type){
        case SELECT_COMPANY:
            return {...state, coid: command.item.coid, coname: command.item.coname};
        case SELECT_SUBDIV:
            return {...state, subdivId:command.subdivId}
        case SELECT_ADDRESS:
            return {...state,addressId:command.addressId}
        case SELECT_CONTACT:
            return {...state,personId:command.personId}
        case SELECT_LOCATION:
            return {...state,locationId:command.locationId}
        case UPDATE_TERM:
            return {...state,term:command.term}
        case UPDATE_PREFILL_FLAGS:
            return {...state, prefillFlags:{...state.prefillFlags,...command.flags}}
        case UPDATE_PREFILLS:
            return {...state, prefills:command.prefills};
        default: return state;
    }
}