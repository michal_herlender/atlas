import store from "./store.js";
import {
    PREFILL_ADDRESS_FLAG,
    PREFILL_COMPANY_FLAG,
    PREFILL_CONTACT_FLAG,
    PREFILL_LOCATION_FLAG,
    PREFILL_SUBDIV_FLAG,
    SELECT_ADDRESS,
    SELECT_COMPANY,
    SELECT_CONTACT,
    SELECT_LOCATION,
    SELECT_SUBDIV,
    UPDATE_PREFILL_FLAGS,
    UPDATE_PREFILLS,
    UPDATE_TERM
} from "./actionsTypes.js";


export function updateTerm(term){
    store.send({type:UPDATE_TERM,term});
}

export function selectCompany(coid, coname) {
    store.send({type: SELECT_COMPANY, item: {coid, coname}});
}

export function selectSubdiv(subdivId) {
    store.send({type:SELECT_SUBDIV,subdivId});
}

export function selectContact(personId) {
    store.send({type:SELECT_CONTACT,personId});
}

export function selectLocation(locationId) {
    store.send({type:SELECT_LOCATION, locationId});
}

export function selectAddress(addressId) {
    store.send({type:SELECT_ADDRESS,addressId});
}

export function startPrefilling(prefills){
  const flags =  Object.keys(prefills).reduce((acc,flag) => {
        switch(flag){
            case "coid":
                return {...acc,[PREFILL_COMPANY_FLAG]:true}
            case "subdivId":
                return {...acc,[PREFILL_SUBDIV_FLAG]:true}
            case "personId":
                return {...acc, [PREFILL_CONTACT_FLAG]:true}
            case "locationId":
                return {...acc, [PREFILL_LOCATION_FLAG]:true}
            case "addressId":
                return {...acc, [PREFILL_ADDRESS_FLAG]:true}
        }
    },{});
    store.send({type:UPDATE_PREFILLS,prefills});
    store.send({type:UPDATE_PREFILL_FLAGS,flags});
}

export function prefillComplete(flag){
    store.send({type:UPDATE_PREFILL_FLAGS,flags:{[flag]:false}});
}



