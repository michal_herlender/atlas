import { LitElement } from "../../../../thirdparty/lit-element/lit-element.js";
import {template } from "./cwms-selectable-list.html.js";
import {styling} from "./cwms-selectable-list.css.js";

export const CWMS_SELECTABLE_LIST_SELECTED = "cwms-selectable-list-selected"

class CwmsSelectableList extends LitElement {
    static get properties(){
        return  {
            items:{type:Array},
            keyFunction:{type:Object},
            labelFunction:{type:Object},
            itemClassFunction:{type:Object},
            selectedItemKey:{type:Number},
        };
    }

    static get styles(){
        return styling();
    }

    constructor(){
        super();
        this.items = [];
        this.itemClassFunction = item => undefined;
        this.keyFunction = item => item.id;
        this.labelFunction = item => item;
    }

    select(item){
        return _ => {
            this.selectedItemKey = this.keyFunction(item);
            this.dispatchEvent(new CustomEvent(CWMS_SELECTABLE_LIST_SELECTED,{detail:{item}}))
        };
    }

    render(){
        return template(this);
    }

    calculateCssClasses(item){
        return this.itemClassFunction(item) || this.selectedItemKey == this.keyFunction(item)?
        `${this.itemClassFunction(item)??''} ${this.selectedItemKey == this.keyFunction(item)?'selected':''}`:undefined;
    }
}

customElements.define("cwms-selectable-list",CwmsSelectableList);