import {css} from "../../../../thirdparty/lit-element/lit-element.js";


export const styling = context => css`

    .results {
        height: 100px;
        background-color: var(--cwms-table-main-background,  yellow);
        border: 1px solid #666;
        border-style:inset;
        display:flex;
        flex-direction:column;
        overflow:auto;
        overflow-x:hidden;
    }

    .results > .attention {
        color: var(--cwms-red,red);
    }

    .results > * {
        cursor: pointer;
        border-bottom:1px dotted #999;
    }

    .results > *:hover {
        background-color: var(--cwms-table-secondary-backgound,orange);
    }

    .results > .selected {
        background-color: var(--cwms-table-strong-backgound,orange);
    }

`;