import { html } from "../../../../thirdparty/lit-element/lit-element.js";
import {repeat} from "../../../../thirdparty/lit-html/directives/repeat.js";
import {ifDefined} from "../../../../thirdparty/lit-html/directives/if-defined.js";

export const template = context => html`
<div class="results">
${repeat(context.items,item => context.keyFunction(item),item=>html`<span
    class=${ifDefined(context.calculateCssClasses(item))}
 @click=${context.select(item)}>${context.labelFunction(item)}</span>`)}
</div>
`