import {urlWithParamsGenerator} from "../../../../tools/UrlGenerator.js";
import {prefillComplete, selectAddress} from "../../store/actions.js";
import store from "../../store/store.js";
import CwmsTranslate from "../../../cwms-translate/cwms-translate.js";
import {CwmsCascadeBoxAbstract} from "../cwms-cascade-box-abstract/cwms-cascade-box-abstract.js";
import {PREFILL_ADDRESS_FLAG} from "../../store/actionsTypes.js";


export const CWMS_CASCADE_SELECT_ADDRESS_EVENT = "cwms-cascade-select-address";

class CwmsCascadeBoxAddress extends CwmsCascadeBoxAbstract {
    static get properties() {
        return {
            ...super.properties,
            addressType: {type: String},
            seletedSubdivId: {type: Number},
        }
    }

    get boxHead(){
        return CwmsTranslate.messagePromise("", "Matching Addresses")
    }

    constructor(){
        super();
        this.store = store;
        store.subscribe(() => {
            const {addressId,subdivId,prefillFlags:{[PREFILL_ADDRESS_FLAG]:prefillAddressFlag},prefills:{addressId:prefillAddressId}} = store.getState();
            this.selectedElementId = addressId;
            this.subdivisionUpdated(subdivId,prefillAddressId,prefillAddressFlag);
        });
    }

    subdivisionUpdated(subdivId,prefillAddressId,prefillAddressFlag){
        if(!subdivId || subdivId == this.seletedSubdivId) return;
        this.seletedSubdivId = subdivId;
        fetch(urlWithParamsGenerator("address/allActivesBySubdivAndType.json",{subdivId,addressType:this.addressType}))
            .then(res => res.ok? res.json():Promise.reject(res.statusText))
            .then(res => {
                this.items = res ?? [];
                if(this.items[0] && !prefillAddressFlag)
                    this.selectElement(this.items[0]);
                else if (prefillAddressFlag){
                    const address = this.items.filter(c => this.keyFunction(c) == prefillAddressId)[0];
                    address && this.selectElement(address);
                    prefillComplete(PREFILL_ADDRESS_FLAG);
                }
            });
    }

    selectElement(item){
        super.selectElement(item);
        selectAddress(this.keyFunction(item));
    }

    get selectEventName() {
        return CWMS_CASCADE_SELECT_ADDRESS_EVENT;
    }

    labelFunction(item){
        return `${item.addr1}, ${item.town}, ${item.postcode}`;
    }

    keyFunction(item){
        return item.addrid;
    }

    itemClassFunction(item){
        return item.subdivDefault?"attention":undefined;
    }

}

customElements.define("cwms-cascade-box-address",CwmsCascadeBoxAddress)