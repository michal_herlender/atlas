import {urlWithParamsGenerator} from "../../../../tools/UrlGenerator.js";
import {prefillComplete, selectSubdiv} from "../../store/actions.js";
import store from "../../store/store.js";
import CwmsTranslate from "../../../cwms-translate/cwms-translate.js";
import {CwmsCascadeBoxAbstract} from "../cwms-cascade-box-abstract/cwms-cascade-box-abstract.js";
import {PREFILL_COMPANY_FLAG, PREFILL_SUBDIV_FLAG} from "../../store/actionsTypes.js";


export const CWMS_CASCADE_SELECT_SUBDIV_EVENT = "cwms-cascade-select-subdiv";

class CwmsCascadeBoxSubdiv extends CwmsCascadeBoxAbstract {
    static get properties() {
        return {
            ...super.properties,
            seletedCompanyId: {type: Number},
        }
    }

    get boxHead(){
        return CwmsTranslate.messagePromise("company.matchingsubdivisions", "Matching Subdivisions")
    }

    constructor(){
        super();
        store.subscribe(() => {
            const {coid,subdivId,prefillFlags:{[PREFILL_SUBDIV_FLAG]:prefillSubdivFlag},prefills:{subdivId:prefillSubdivId}} = store.getState();
            this.selectedElementId = subdivId;
            this.companyUpdated(coid,prefillSubdivFlag,prefillSubdivId);
        });
    }

    companyUpdated(coid,prefillSubdivFlag,subdivId){
        if (!coid || coid === this.seletedCompanyId) return;
        this.seletedCompanyId = coid;
        fetch(urlWithParamsGenerator("subdiv/activeByCoid.json",{coid}))
            .then(res => res.ok? res.json():Promise.reject(res.statusText))
            .then(res => {
                this.items = res ?? [];
                if(this.items[0] && !prefillSubdivFlag)
                    {
                          this.selectElement(this.items[0]);
                    }
                else if (prefillSubdivFlag){
                    const subdiv = this.items.filter(s => this.keyFunction(s) === subdivId)[0];
                    subdiv && this.selectElement(subdiv);
                    prefillComplete(PREFILL_COMPANY_FLAG)
                }
                
            });
    }

    selectElement(item){
        super.selectElement(item)
        selectSubdiv(this.keyFunction(item));
    }

    get selectEventName() {
        return CWMS_CASCADE_SELECT_SUBDIV_EVENT;
    }

    labelFunction(item){
        return item.subname;
    }

    keyFunction(item){
        return item.subdivid;
    }

}

customElements.define("cwms-cascade-box-subdiv",CwmsCascadeBoxSubdiv)