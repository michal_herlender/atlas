import {urlWithParamsGenerator} from "../../../../tools/UrlGenerator.js";
import {selectLocation} from "../../store/actions.js";
import store from "../../store/store.js";
import CwmsTranslate from "../../../cwms-translate/cwms-translate.js";
import {CwmsCascadeBoxAbstract} from "../cwms-cascade-box-abstract/cwms-cascade-box-abstract.js";
import {PREFILL_LOCATION_FLAG} from "../../store/actionsTypes.js";


export const CWMS_CASCADE_SELECT_LOCATION_EVENT = "cwms-cascade-select-location";

class CwmsCascadeBoxLocation extends CwmsCascadeBoxAbstract {
    static get properties() {
        return {
            ...super.properties,
            addressType: {type: String},
            seletedAddressId: {type: Number},
        }
    }

    get boxHead(){
        return CwmsTranslate.messagePromise("company.matchinglocations", "Matching Locations")
    }

    constructor(){
        super();
        this.store = store;
        store.subscribe(() => {
            const {addressId,locationId,prefillFlags:{[PREFILL_LOCATION_FLAG]:prefillLocationFlag},prefills:{locationId:prefillLocationId}} = store.getState();
            this.selectedElementId = locationId;
            this.addressUpdated(addressId,prefillLocationId,prefillLocationFlag);
        });
    }

    addressUpdated(addressId,prefillLocationId,prefillLocationFlag){
        if(!addressId || addressId == this.seletedAddressId) return;
        this.seletedAddressId = addressId;
        fetch(urlWithParamsGenerator("location/allByAddressAndActiveStatus.json",{ addressId}))
            .then(res => res.ok? res.json():Promise.reject(res.statusText))
            .then(res => {
                this.items = res ?? [];
                if(this.items[0] && !prefillLocationFlag)
                  this.selectElement(this.items[0]);
                else if(prefillLocationFlag) {
                    const location = this.items.filter(c => this.keyFunction(c) == prefillLocationId)[0];
                    location && this.selectElement(location);
                    prefillComplete(PREFILL_LOCATION_FLAG);
                }
            });
    }


    selectElement(item){
        super.selectElement(item)
        selectLocation(this.keyFunction(item));
    }


    get selectEventName() {
        return CWMS_CASCADE_SELECT_LOCATION_EVENT;
    }

    labelFunction(item){
        return item.location;
    }

    keyFunction(item){
        return item.locationid;
    }

}

customElements.define("cwms-cascade-box-location",CwmsCascadeBoxLocation)