import {html} from "../../../../thirdparty/lit-html/lit-html.js";
import {urlGenerator, urlWithParamsGenerator} from "../../../../tools/UrlGenerator.js";

export const template = context =>
    [addCompanyTemplate(),
        addSubdivTemplate(context.coid, context.includeSubdiv),
        addAddressTemplate(context.subdivId, context.includeAddress),
        addContactTemplate(context.subdivId, context.includeContact),
        addLocationTemplate(context.addressId, context.includeLocation)
    ].flatMap(a => a).reduce((acc, cur) => html`${acc} - ${cur}`);

const addCompanyTemplate = () => [html`
<a class="mainLink" href=${urlGenerator("addcompany.htm").toString()} target="_blank" ><cwms-translate code="company.add">Add Company</cwms-translate></a>
`];

const addSubdivTemplate = (coid,includeSubdiv) => coid && includeSubdiv?[html`
<a class="mainLink" href=${urlWithParamsGenerator("addsubdiv.htm", {coid}).toString()} target="_blank" ><cwms-translate code="company.addsubdivision ">Add subdivid</cwms-translate></a>
`]:[];

const addAddressTemplate = (subdivid, includeAddress) => subdivid && includeAddress ? [
    html`<a class="mainLink" href=${urlWithParamsGenerator("addaddress.htm", {subdivid}).toString()} target="_blank" ><cwms-translate code="address.addaddress">Add Address</cwms-translate></a>`] : [];

const addContactTemplate = (subdivid,includeContact) => subdivid && includeContact?[
    html`<a class="mainLink" href=${urlWithParamsGenerator("addperson.htm", {subdivid}).toString()} target="_blank" ><cwms-translate code="contactadd.addcontact ">Add Contact</cwms-translate></a>`
]:[];

const addLocationTemplate = (addrid,includeLocation) => addrid && includeLocation?[
    html`<a class="mainLink" href=${urlWithParamsGenerator("location.htm", {addrid}).toString()} target="_blank" ><cwms-translate code="location.addlocation">Add Location</cwms-translate></a>`
]:[];