import { LitElement } from "../../../../thirdparty/lit-element/lit-element.js"
import { linkStyling } from "../../../tools/styles/links.css.js";
import store from "../../store/store.js";
import { template } from "./cwms-cascade-add-links.html.js";

class CwmsCascadeAddLinks extends LitElement {

    static get properties(){
        return {
            includeSubdiv:{type:Boolean},
            includeContact:{type:Boolean},
            includeAddress:{type:Boolean},
            includeLocation:{type:Boolean},
            coid:{type:Number},
            subdivId:{type:Number},
            addressId:{type:Number},
        };    
    }

    constructor(){
        super();
        store.subscribe(() => {
            const {coid,subdivId,addressId} = store.getState();
            this.coid = coid;
            this.subdivId = subdivId;
            this.addressId = addressId;
        });
    }

    static get styles(){
        return [linkStyling()];
    }

    render(){
        return template(this);
    }
}


customElements.define("cwms-cascade-add-links",CwmsCascadeAddLinks);