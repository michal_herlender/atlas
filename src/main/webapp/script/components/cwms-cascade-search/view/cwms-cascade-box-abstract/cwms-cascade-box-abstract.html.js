import { html } from "../../../../thirdparty/lit-html/lit-html.js";
import {until} from "../../../../thirdparty/lit-html/directives/until.js";
import "../cwms-selectable-list/cwms-selectable-list.js"

export const template = context => html`
        <div class="contacts cascade-element">
        <div class="head">${until(context.boxHead,"Matching items")}</div>
<cwms-selectable-list .items=${context.items} .labelFunction=${context.labelFunction} .keyFunction=${context.keyFunction} .selectedItemKey=${context.selectedElementId} .itemClassFunction=${context.itemClassFunction}
@cwms-selectable-list-selected=${context.select}
 ></cwms-selectable-list>
        </div>
`;