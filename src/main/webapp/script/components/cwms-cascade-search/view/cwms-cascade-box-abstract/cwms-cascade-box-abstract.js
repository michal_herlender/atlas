import { LitElement } from "../../../../thirdparty/lit-element/lit-element.js";
import { styling } from "./cwms-cascade-box-abstract.css.js";
import { template } from "./cwms-cascade-box-abstract.html.js";


export const CWMS_CASCADE_SELECT_CONTACT_EVENT = "cwms-cascade-select-contact";

export class CwmsCascadeBoxAbstract extends LitElement {
    static get properties(){
        return {
            selectedElementId:{type:Number},
            items:{type:Array},
        }
    }

    get selectEventName(){
        return "selected-item";
    }

    static get styles(){
        return styling();
    }

    get boxHead(){
        return Promise.resolve("Matching Items");
    }

    constructor(){
        super();
        this.items = [];
    }


    selectElement(item){
        this.dispatchEvent(new CustomEvent(this.selectEventName,{detail:{item},bubbles:true,composed:true}));
    }

    itemClassFunction(item){
        return undefined;
    }

    select(e) {
        this.selectElement(e.detail.item);
    }

    labelFunction(item){
        return item;
    }

    keyFunction(item){
        return item?.id ?? 0;
    }

    render(){
        return template(this);
    }
}
