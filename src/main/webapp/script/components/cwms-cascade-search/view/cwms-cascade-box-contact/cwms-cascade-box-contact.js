import {urlGenerator} from "../../../../tools/UrlGenerator.js";
import {prefillComplete, selectContact} from "../../store/actions.js";
import store from "../../store/store.js";
import {CwmsCascadeBoxAbstract} from "../cwms-cascade-box-abstract/cwms-cascade-box-abstract.js";
import CwmsTranslate from "../../../cwms-translate/cwms-translate.js";
import {PREFILL_CONTACT_FLAG} from "../../store/actionsTypes.js";


export const CWMS_CASCADE_SELECT_CONTACT_EVENT = "cwms-cascade-select-contact";

class CwmsCascadeBoxContact extends CwmsCascadeBoxAbstract {
    static get properties() {
        return {
            ...super.properties,
            selectedSubdivId: {type: Number},
        }
    }

    get boxHead(){
        return CwmsTranslate.messagePromise("company.matchingcontacts","Matching Contacts")
    }

    constructor(){
        super();
        store.subscribe(() => {
            const {personId,subdivId,prefillFlags:{[PREFILL_CONTACT_FLAG]:prefillContactFlag},prefills:{personId:prefillPersonId}} = store.getState();
            this.selectedElementId = personId;
            this.subdivisionUpdated(subdivId,prefillPersonId,prefillContactFlag);
        });
    }

    subdivisionUpdated(subdivId,prefillPersonId,prefillContactFlag){
        if (!subdivId || subdivId === this.selectedSubdivId) return;
        this.selectedSubdivId = subdivId;
        fetch(urlGenerator(`contactsearch/activesubdivcontacts/${subdivId}`))
            .then(res => res.ok? res.json():Promise.reject(res.statusText))
            .then(res => {
                this.items = res ?? [];
                if(this.items[0] && !prefillContactFlag)
                  this.selectElement(this.items.filter(c => c.defaultContact).shift() ?? this.items[0]);
                else if(prefillContactFlag) {
                    const contact = this.items.filter(c => this.keyFunction(c) === prefillPersonId)[0];
                    contact && this.selectElement(contact);
                    prefillComplete(PREFILL_CONTACT_FLAG);
                }
                else if (!this.items[0])
                	selectContact('');
            });
    }

    selectElement(item){
        super.selectElement(item)
        selectContact(this.keyFunction(item));
    }

    labelFunction(item){
        return item.name;
    }

    keyFunction(item) {
        return item.personid;
    }

    get selectEventName() {
        return CWMS_CASCADE_SELECT_CONTACT_EVENT;
    }

    itemClassFunction(item) {
        return item.defaultContact ? "attention" : undefined;
    }
}

customElements.define("cwms-cascade-box-contact",CwmsCascadeBoxContact)