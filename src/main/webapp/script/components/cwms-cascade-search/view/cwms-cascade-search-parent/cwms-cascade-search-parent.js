import {LitElement} from "../../../../thirdparty/lit-element/lit-element.js";
import {startPrefilling, updateTerm} from "../../store/actions.js";
import store from "../../store/store.js";
import {styling} from "./cwms-cascade-search-parent.css.js";
import {template} from "./cwms-cascade-search-parent.html.js";

class CwmsCascadeSearchParent extends LitElement {

    #internals;

    static get formAssociated() {
        return true;
    }

    static get properties() {
        return {
            cascadeRules: {type: Array},
            companyRoles: {type: Array},
            addressType: {type: String},
            selectedCoid: {type: Number},
            prefills: {type: Object}
        }
    }

    constructor() {
        super();
        this.prefills = {};
        this.cascadeRules = [];
        this.companyRoles = []
        this.#internals = this.attachInternals();
        store.subscribe(() => {
            const {coid, coname, subdivId, addressId, personId, locationId} = store.getState();
            const formData = new FormData();
            formData.set("coid", coid ?? '');
            formData.set("subdivid", subdivId ?? '');
            formData.set("addrid", addressId ?? '');
            formData.set("personid", personId ?? '');
            formData.set("locid", locationId ?? '');
            this.coid = coid;
            this.coname = coname;
            this.#internals.setFormValue(formData);

        })
    }

    connectedCallback() {
        super.connectedCallback();
        startPrefilling(this.prefills);
    }

    static get styles() {
        return [styling];
    }

    get includeSubdiv() {
        return this.cascadeRules.includes("subdiv");
    }

    get includeContact() {
        return this.includeSubdiv && this.cascadeRules.includes("contact");
    }

    get includeAddress() {
        return this.includeSubdiv && this.cascadeRules.includes("address");
    }

    get includeLocation() {
        return this.includeAddress && this.cascadeRules.includes("location");
    }


    updateInput(e) {
        updateTerm(e.currentTarget.value);
    }

    render() {
        return template(this);
    }
}


customElements.define("cwms-cascade-search-parent", CwmsCascadeSearchParent);
