import {css} from "../../../../thirdparty/lit-element/lit-element.js";


export const styling = css`
    :host() {
        display: block;
    }

    input {
        width: 340px;
    }

    .container {
        display:flex;
        flex-direction:column;
        width:810px;
    }


    .cascade {
        display: flex;
        flex-direction:row;
        flex-wrap: wrap;
        width:810px;
        margin-top:5px;
        row-gap: 2px;
    }


    .cascade-element, cwms-cascade-box-subdiv {
        flex-basis:50%;
    }



    .head {
        background-color: var(--cwms-main-dark-background,violet);
        border: 1px solid #666;
        text-align: center;
    }
    


    .results {
        height: 100px;
        background-color: var(--cwms-table-main-background,  yellow);
        border: 1px solid #666;
        border-style:inset;
        display:flex;
        flex-direction:column;
    }

    .results > * {
        cursor: pointer;
    }

    .results > *:hover {
        background-color: var(--cwms-table-secondary-backgound,orange);
    }

    .results > .selected {
        background-color: var(--cwms-table-strong-backgound,orange);
    }

`;