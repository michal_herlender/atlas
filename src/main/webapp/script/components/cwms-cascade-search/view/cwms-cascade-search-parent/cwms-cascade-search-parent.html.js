import {html} from "../../../../thirdparty/lit-element/lit-element.js";
import "../cwms-selectable-list/cwms-selectable-list.js";
import "../cwms-cascade-box-company/cwms-cascade-box-company.js";
import "../cwms-cascade-box-subdiv/cwms-cascade-box-subdiv.js";
import "../cwms-cascade-box-contact/cwms-cascade-box-contact.js";
import "../cwms-cascade-box-address/cwms-cascade-box-address.js";
import "../cwms-cascade-box-location/cwms-cascade-box-location.js";
import "../cwms-cascade-add-links/cwms-cascade-add-links.js";

export const template = context => html`
<div class="container">
    <input type="text" @keyup=${context.updateInput}/>
    <div class="cascade" > 
        <cwms-cascade-box-company .prefillCompanyId=${context.prefills.coid} .companyRoles=${context.companyRoles} class="cascade-element"></cwms-cascade-box-company>
        ${context.includeContact ? html` <cwms-cascade-box-contact class="cascade-element"></cwms-cascade-box-contact>` : emptyElementTemplate}
        ${context.includeSubdiv ? html`<cwms-cascade-box-subdiv class="cascade-element"></cwms-cascade-box-subdiv>` : emptyElementTemplate}
        ${context.includeAddress ? html`<cwms-cascade-box-address class="cascade-element" .addressType=${context.addressType}></cwms-cascade-box-address>` : emptyElementTemplate}
        ${context.includeLocation ? html`${emptyElementTemplate}     
        <cwms-cascade-box-location class="cascade-element"></cwms-cascade-box-location>` : html``}
    </div>
    <cwms-cascade-add-links ?includeAddress=${context.includeAddress} 
                            ?includeContact=${context.includeContact}
                            ?includeLocation=${context.includeLocation}
                            ?includeSubdiv=${context.includeSubdiv}

    ></cwms-cascade-add-links>
</div>
`;

const emptyElementTemplate = html`<div class="empty cascade-element"></div>`;
