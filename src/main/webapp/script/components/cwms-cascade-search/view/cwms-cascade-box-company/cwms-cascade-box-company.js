import {urlWithParamsGenerator} from "../../../../tools/UrlGenerator.js";
import {prefillComplete, selectCompany} from "../../store/actions.js";
import store from "../../store/store.js";
import CwmsTranslate from "../../../cwms-translate/cwms-translate.js";
import {CwmsCascadeBoxAbstract} from "../cwms-cascade-box-abstract/cwms-cascade-box-abstract.js";
import {PREFILL_COMPANY_FLAG} from "../../store/actionsTypes.js";


export const CWMS_CASCADE_SELECT_COMPANY_EVENT = "cwms-cascade-select-company";

class CwmsCascadeBoxCompany extends CwmsCascadeBoxAbstract {
    static get properties() {
        return {
            ...super.properties,
            companyRoles: {type: Array},
            term: {type: String},
            prefillCompanyId:{type:Number},
        }
    }

    get prefillCompanyId(){
        return this.__prefillCompanyId;
    }

    set prefillCompanyId(id){
        const oldValue = this.__prefillCompanyId;
        console.log("prefillId: ",id);
        this.__prefillCompanyId = id;
        this.loadPrefilledCompany(id);
        this.requestUpdate("prefillCompanyId",oldValue);
    }

    loadPrefilledCompany(id){
        if(!id) return;
        fetch(urlWithParamsGenerator("companyforplugin.json",{id}))
        .then(res => res.ok?res.json():Promise.reject(res.statusText))
        .then(res => {
            this.items = res ?? [];
            this.items[0] && this.selectElement(this.items[0]);
            prefillComplete(PREFILL_COMPANY_FLAG);
        })
    }

    get boxHead(){
        return CwmsTranslate.messagePromise("company.matchingcompanies", "Matching Companies")
    }

    constructor(){
        super();
        store.subscribe(() => {
            const {coid,term} = store.getState();
            this.selectedElementId = coid;
            this.termUpdated(term);
        });
    }

    prepareUrl(companyName){
        const url = urlWithParamsGenerator("companysearchrequest.json",{searchName:companyName,activeAnywhere:false,searchEverywhere:false,companyRoles:this.companyRoles});
        return url;
    }

    termUpdated(term){
        if (!term || term === this.term) return;
        this.term = term;
        fetch(this.prepareUrl(term))
            .then(res => res.ok? res.json():Promise.reject(res.statusText))
            .then(res => {
                this.items = res ?? [];
                this.items[0] && this.selectElement(this.items[0]);
            });
    }

    selectElement(item){
        super.selectElement(item);
        selectCompany(this.keyFunction(item), this.labelFunction(item));
    }

    get selectEventName() {
        return CWMS_CASCADE_SELECT_COMPANY_EVENT;
    }

    labelFunction(item){
        return item.coname;
    }

    keyFunction(item){
        return item.coid;
    }

}

customElements.define("cwms-cascade-box-company",CwmsCascadeBoxCompany)