import {html} from "../../thirdparty/lit-element/lit-element.js";
import {popUpTemplateWithoutStyles} from "../tools/popUpTemplate.html.js";
import '../cwms-translate/cwms-translate.js';
import {cwmsTranslateDirective} from "../cwms-translate/cwms-translate-directive.js";

export let htmlTemplate = (context) => html`

    ${showPopUpTemplate(context)}

    <table class="default4 width80 invPurchaseOrders">
        <thead>
        <tr>
            <td colspan="3">Invoice Purchase Orders (<span class="invPOSize">${context.pos.length}</span>)</td>
        </tr>
        <tr>
            <th class="ponum">PO Number</th>
            <th class="link">Link To Items</th>
            <th class="remove">Remove</th>
        </tr>
        </thead>

        <tbody>
        ${context.pos.map(po => html`
            <tr>
                <td class="ponum">${po.poNumber}</td>

                <td class="link">
                    <a @click=${context.linkToItem(po.poId)} class="mainlink limitAccess ">
                        Link To Items</a>
                </td>

                <td class="remove">
                    <a role="button" class="deleteAnchor limitAccess "
                       @click=${context.removePo(po.poId)} >
                        &nbsp;
                    </a>
                </td>
            </tr>
        `)}
        </tbody>

        <tfoot>
        <tr>
            <td colspan="3">
                <a @click=(${context.editInvPOPopUp}) class="mainlink limitAccess ">New Invoice PO</a>
            </td>
        </tr>
        </tfoot>

    </table>
`;

const showPopUpTemplate = (context) => {
    switch (context.showPopUp) {
        case "NEW_PO":
            return invoicePOPopUpTemplate(context);
        case "LINK_INVOICE_PO" :
            return invoiceLinkInvoicePOTemplate(context);
        default :
            return html``;
    }
}

export const invoicePOPopUpTemplate = (context) => popUpTemplateWithoutStyles({
    title: 'Create Invoice Purchase Order',
    overlayContent: editPOPopUpTemplate(context),
    hide: _ => context.showPopUp = false
});

const invoiceLinkInvoicePOTemplate = (context) => popUpTemplateWithoutStyles({
    title: 'Link Invoice PO',
    overlayContent: linkInvoicePOPopUpContentTemplate(context),
    hide: _ => context.showPopUp = false
});

const linkInvoicePOPopUpContentTemplate = (context) => html`
    <div id="linkInvoicePOToItemsOverlay">
        <table class="default4 linkInvoicePOItems">
            <thead>
            <tr>
                <td colspan="6" class="bold">Invoice Items Available (1)</td>
            </tr>
            <tr>
                <th class="add" scope="col"><input @input=${context.selectAll} ?checked=${context.selectedAll}
                                                   type="checkbox" id="selectAll"
                > All<br>
                </th>
                <th class="itemno" scope="col">Item</th>
                <th class="desc" scope="col">Description</th>
                <th class="inst" scope="col">Instrument / Job Services</th>
                <th class="serial" scope="col">Serial No</th>
                <th class="plant" scope="col">Plant No</th>
            </tr>
            </thead>
            <tbody>
            ${context.invoiceItems.map(invoiceitem => html`
                <tr>
                    <td class="add"><input @input=${context.selectInvoiceItem(invoiceitem.itemId)}
                                           ?checked=${invoiceitem.selected ?? false} type="checkbox"></td>

                    <td class="itemno">${invoiceitem.itemNo}</td>
                    ${invoiceitem.jobItemNo ? html`
                        <td>
                            ${cwmsTranslateDirective("invoice.itemonjob", "Item {0} on job {1}", invoiceitem.jobItemNo, invoiceitem.jobNoForJobItem)}
                        </td>
                        <td>${invoiceitem.instrumentModelName}</td>
                    ` : html`

                        <td>

                            ${cwmsTranslateDirective("invoice.serviceonjob", "Service {0} on job {1}", invoiceitem.jobExpenseItemNo, invoiceitem.jobNoForJobExpenseItem)}
                        </td>
                        <td>
                            ${invoiceitem.description || invoiceitem.jobServiceModelName}
                        </td>`
                    }

                    <td class="serial">${invoiceitem.serialNo}</td>
                    <td class="plant">${invoiceitem.plantNo}</td>
                </tr>
            `)}
            </tbody>
        </table>
        <div class="text-center padding"><input @click=${context.updateLinks} type="button" value="Update Links">
        </div>
    </div>
`

const editPOPopUpTemplate = (context) => html`

    <div id="newInvoicePOOverlay">
        <fieldset>
            <ol>
                <li>
                    <label>New Invoice PO:</label>
                    <input @input=${context.updatePoNumber} type="text" name="newInvoicePOValue" id="newInvoicePOValue"
                           value=""/>
                </li>
                <li>

                    <button @click=${context.addNewInvoicePO} type="submit">
                        <cwms-translate code="invoice.createinvoicepurchaseorder">Create Invoice Purchase Order
                        </cwms-translate>
                    </button>

                </li>
            </ol>
        </fieldset>
    </div>

`;
