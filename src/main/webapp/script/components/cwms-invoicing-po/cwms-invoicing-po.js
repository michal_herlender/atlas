import {LitElement} from "../../thirdparty/lit-element/lit-element.js";
import {htmlTemplate} from "./cwms-invoicing-po-html.js";
import "../cwms-translate/cwms-translate.js";
import {urlGenerator, urlWithParamsGenerator} from "../../tools/UrlGenerator.js";
import {tableStyles} from "../tools/styles/table.css.js";
import {linkStyling} from "../tools/styles/links.css.js";
import {fieldsetStyles} from "../tools/styles/fieldset.css.js";
import {popUpStyles} from "../tools/styles/popUp.css.js";
import {iconsStyles} from "../tools/styles/icons.css.js";

class CwmsInvoicePO extends LitElement {

    static get properties() {
        return {
            pos: {type: Array},
            invoiceId: {type: Number},
            invoiceItems: {type: Array},
            showPopUp: {type: String},
            poNumber: {type: String}
        };
    }

    static get styles() {
        return [
            tableStyles,
            linkStyling(),
            fieldsetStyles,
            popUpStyles,
            iconsStyles
        ];
    }

    constructor() {
        super();
        this.pos = [];
        this.invoiceItems = [];
        this.showPopUp = '';
    }

    connectedCallback() {
        super.connectedCallback();
    }

    render() {
        return htmlTemplate(this);
    }

    selectInvoiceItem(itemId) {
        return (e) => {
            this.invoiceItems = this.invoiceItems.map(invoiceitem => invoiceitem.itemId === itemId ? {
                ...invoiceitem,
                selected: e.currentTarget.checked
            } : invoiceitem)
        }
    }

    selectAll(e) {
        this.invoiceItems = this.invoiceItems.map(invoiceitem => ({...invoiceitem, selected: e.currentTarget.checked}));
    }

    get selectedAll() {
        return !this.invoiceItems.find(ii => !ii.selected);
    }

    updateLinks() {
        fetch(urlGenerator("invoicepo/updateinvoiceitemstopo.json"), {
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(
                {
                    invoiceId: this.invoiceId,
                    itemIds: this.invoiceItems.filter(ii => ii.selected).map(ii => ii.itemId),
                    poId: this.selectedPoId
                })
        }).then(res => res.ok ? res.json() : Promise.reject(res.statusText))
            .then(resp => resp.right ? resp.value : Promise.reject(resp.value))
            .then(_ => this.showPopUp = '')
            .catch(console.error)


    }

    addNewInvoicePO() {
        this.showPopUp = '';
        fetch(urlWithParamsGenerator("invoicepo/createinvoicepo.json"
                , {
                    invoiceId: this.invoiceId,
                    poNo: this.poNumber
                })
            , {method: "PUT"}
        ).then(res => res.ok ? res.json() : Promise.reject(res.statusText))
            .then(poE => poE.right ? poE.value : Promise.reject(poE.value))
            .then(po => this.pos = [...this.pos, po])
            .catch(console.error);
    }

    editInvPOPopUp() {
        this.showPopUp = 'NEW_PO';
    }

    linkToItem(poId) {
        return () => {
            this.selectedPoId = poId
            this.showPopUp = 'LINK_INVOICE_PO'
            this.getInvoiceItems(this.invoiceId, poId);
        }
    }

    updatePoNumber(event) {
        this.poNumber = event.currentTarget.value;
    };

    removePo(poId) {
        return () => {
            fetch(urlWithParamsGenerator("invoicepo/deleteinvoicepo.json", {poId}
            ), {method: "PUT"})

                .then(res => res.ok ? res.json() : Promise.reject(res.statusText))
                .then(poId => poId.right ? poId.value : Promise.reject(poId.value))
                .then(poId => this.pos = this.pos.filter(po => po.poId !== poId))
                .catch(console.error);

        }
    }

    getInvoiceItems(invoiceId, poId) {
        fetch(urlWithParamsGenerator("invoicepo/getinvoiceitems.json", {invoiceId, poId}
        ), {method: "GET"})
            .then(res => res.ok ? res.json() : Promise.reject(res.statusText))
            .then(invoiceItems => this.invoiceItems = invoiceItems)
            .catch(console.error);
    }
}

window.customElements.define('cwms-invoice-po', CwmsInvoicePO);