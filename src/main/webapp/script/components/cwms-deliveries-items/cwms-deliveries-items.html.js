import { html } from "../../thirdparty/lit-element/lit-element.js";
import {repeat} from "../../thirdparty/lit-html/directives/repeat.js";
import { directive } from "../../thirdparty/lit-html/lit-html.js";


export let htmlTemplate = (context) => html `
		<link rel="stylesheet" href="styles/structure/elements/table.css">
		<link rel="stylesheet" href="styles/theme/theme.css">
		<link rel="stylesheet" href="styles/text/text.css">
		<link rel="stylesheet" href="styles/global.css" type="text/css" title="antech" />
		
					<table class="default2" id="deliverylist">
						<thead>
							<tr>
								<td colspan="6"><cwms-translate code='currentdels'> Current Deliveries  </cwms-translate>  (${context.getTotalItems()}) </td>
							</tr>
							<tr>
								<th id="delno" scope="col"> <cwms-translate code='viewjob.delno'>Delivery No</cwms-translate> </th>
								<th id="company" scope="col"> <cwms-translate code='company'> Company </cwms-translate> </th>
								<th id="contact" scope="col"> <cwms-translate code='contact'> contact </cwms-translate> </th>
								<th id="type" scope="col"> 	<cwms-translate code='jobresults.type'> type </cwms-translate> </th>
								<th id="item" scope="col"> <cwms-translate code='addjob.items'> Items </cwms-translate> </th>
								<th id="deldate" scope="col"> <cwms-translate code='viewjob.deldate'> Delivery Date  </cwms-translate> </th>  
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="6">
									<a class="mainlink-float" href="createdelnote.htm?jobid=${context.jobId}&destination=CLIENT">Create Client Delivery Note</a>
									
									<a class="mainlink-float" href="selecttpcontact.htm?jobid=${context.jobId}">Create Third Party Delivery Note</a>
								</td>
							</tr>
						</tfoot>
						<tbody>
							
						 ${ context.data.length < 1 ? html` 
					        	<tr>
					            	<td colspan="8" class="bold center"><cwms-translate code='viewjob.string23'></cwms-translate></td>
					            </tr> ` 
							: html`${
							
							 	repeat(context.data, (item, index)=> html` 
							 		<tr data-index="${index+1}"  onmouseover=" $j(this).addClass('hoverrow'); " onmouseout=" $j(this).removeClass('hoverrow'); ">
							 			<td> <a class="mainlink" href="viewdelnote.htm?delid=${item.deliveryId}"> ${item.deliveryNumber} </a>  </td>
							 			<td> ${item.destContact.subdiv.company.coname} </td>
							 			<td> ${item.destContact.firstName} ${item.destContact.lastName}</td>
							 			<td>${item.type} </td>
							 			<td> <img @click="${context.showDeliveryItems}" src="img/icons/items.png" width="16" height="16" class="image_inline" />  </td>
							 			<td>${item.deliveryDate}</td>
							 		</tr>
							 		
							 		
							 		${ index+1 != context.activeDetailsRowIndex ? `` : html`
				                        <tr> 
				                            <td colspan="8">
				                               <div>          
				                                    ${ formatDetailsTable(context, context.detailsRow ) } 
				                                </div>  
				                            </td>               
				                        </tr>
				                    `
				                    }
							 		
							 	`)
							 
							 }
						`}
						</tbody>
					</table>
`


const formatDetailsTable = directive((context, detailsRow) => (part) => {
    part.setValue(html`
        <table style="width:100%;">
            <thead>
            	<tr>
            		<td colspan="5">
    					There are ${context.countDeliveyItems()}  delivery items
            		</td>
            	</tr>
                <tr>
                    <th class="item" scope="col" style="text-align: center;">
                        <cwms-translate code='viewjob.itemno'>Item No</cwms-translate>
                    </th>
                    <th class="model" scope="col" style="text-align: center;">
                        <cwms-translate code='instrument'>Instrument</cwms-translate>
                    </th>
                    <th class="model" scope="col" style="text-align: center;">
                        <cwms-translate code='serialno'>serial no</cwms-translate>
                    </th>
                    <th class="serial" scope="col" style="text-align: center;">
                        <cwms-translate code='plantno'>plant no</cwms-translate>
                    </th>
                    <th class="plant" scope="col" style="text-align: center;">
                       <cwms-translate code='viewinstrument.customerdescription'>Cusomer Description</cwms-translate>
                    </th>
                    
                </tr>
            </thead>
            <tbody>
            	${ detailsRow === undefined ? `` :
                     repeat(detailsRow, (item, index)=> html`
                     <tr>
                         <td>${item.jobItem.itemno}</td>
                         <td>${item.jobItem.instrument.instrumentModelNameViaFields}</td>
                         <td>${item.jobItem.instrument.serialno}</td>
                         <td>${item.jobItem.instrument.plantno}</td>
                         <td>${item.jobItem.instrument.customerDescription}</td>
                    </tr>
                     `)}
            </tbody>

       </table>   
    `
    )});










;