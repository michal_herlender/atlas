import { LitElement } from "../../thirdparty/lit-element/lit-element.js";
import { styling} from "./cwms-deliveries-items.css.js";
import { htmlTemplate} from "./cwms-deliveries-items.html.js";
import {} from "../cwms-translate/cwms-translate.js";


class CwmsDeliveriesItems extends LitElement {
	
    static get dataURL() { return "getDeliveriesByJob.json?jobId=" };
    static get dataDetailURL() { return "getJobDeliveryItems.json?deliveryId="};
	
    static get properties() {
        return {
            data: { type: Array },
            detailsRow : { type : Array },
            activeDetailsRowIndex: { type: Number },
            jobId: { attribute: 'job-id' },
            deliveryId: { attribute: 'delivery-id' }
        };
    }

    static getStyles() {
        return styling;
    }

    constructor() {
        super();
        this.data = new Array();
    }
    

    render() {
        return htmlTemplate(this);
    }
    
    getTotalItems() {
        var items = 0;
        this.data.forEach(element => {
            items = items + 1;
        });
        return items;
    }
    
    
    countDeliveyItems(){
    	var items = 0;
        this.detailsRow.forEach(element => {
            items = items + 1;
        });
        return items;
    }
    
    showDeliveryItems(e){  	
        if(this.activeDetailsRowIndex == e.target.closest('tr').getAttribute('data-index'))
            this.activeDetailsRowIndex = null;
        else
            this.activeDetailsRowIndex = e.target.closest('tr').getAttribute('data-index');
        
        if(this.activeDetailsRowIndex>=1) {
            let deliveryId = this.data[this.activeDetailsRowIndex-1].deliveryId;
            this.detailsRow = new Array();
        	fetch(CwmsDeliveriesItems.dataDetailURL+deliveryId).then(res => res.text().then(text => {
                 this.detailsRow = JSON.parse(text);
             })); 
            }
    	
    }
    
    fetchdata(){
    	this.data = new Array();
    	fetch(CwmsDeliveriesItems.dataURL+jobId).then(res => res.text().then(text => {
             this.data = JSON.parse(text);
         }));  
    }
    
    refresh() {  
    	this.fetchdata();
    }
	
}window.customElements.define('cwms-deliveries-items', CwmsDeliveriesItems);
