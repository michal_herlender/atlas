import { LitElement,css,html } from "../../thirdparty/lit-element/lit-element.js";

class CwmsContentVisibility extends LitElement
{
    static get styles(){
    return css`
        .content-visibility {
        content-visibility: auto;
    }   
            `;
    }

    render(){
        return html`
        <div class="content-visibility">
            <slot></slot>
        </div>
        `;
    }
}


customElements.define("cwms-content-visibility",CwmsContentVisibility);