import { html } from "../../thirdparty/lit-element/lit-element.js";
import {repeat} from "../../thirdparty/lit-html/directives/repeat.js";
import { directive } from "../../thirdparty/lit-html/lit-html.js";


export let htmlTemplate = (context) => html `
<link rel="stylesheet" href="styles/structure/elements/table.css">
<link rel="stylesheet" href="styles/theme/theme.css">
<link rel="stylesheet" href="styles/text/text.css">
<link rel="stylesheet" href="styles/global.css" type="text/css" title="antech" />

			<table class="default4 jobContractRev">
				<thead>
					<tr>
						<td colspan="5"><cwms-translate code='viewjob.string34'></cwms-translate> (${context.getTotalItems() })</td>
					</tr>
					<tr>											
						<th style="text-align:center" class="comments" scope="col"> <cwms-translate code='viewjob.comments'>Comment</cwms-translate> </th>
						<th style="text-align:center" class="items" scope="col"> <cwms-translate code='addjob.item'>Items</cwms-translate> </th>
						<th style="text-align:center" class="itemscovered" scope="col"> <cwms-translate code='viewjob.itemnoscovered'>Item No Couvered </cwms-translate> </th>
						<th style="text-align:center" class="revby" scope="col"> <cwms-translate code='viewjob.reviewby'>Review By </cwms-translate> </th>
						<th style="text-align:center" class="revdate" scope="col"> <cwms-translate code='viewjob.reviewdate'>Review Date </cwms-translate>  </th> 
					</tr>
				</thead>
				<tfoot>
							<tr>
								<td colspan="5">&nbsp;</td>
							</tr>
				</tfoot>
				<tbody>
					${ context.data.length < 1 ? html` 
					        	<tr>
					            	<td colspan="8" class="bold center"><cwms-translate code='viewjob.string23'></cwms-translate></td>
					            </tr> ` 
							: html`${
							
							 	repeat(context.data, (item, index)=> html` 
							 		<tr data-index="${index+1}" >
							 			<td style="text-align:center">  ${item.comments}  </td>
							 			<td style="text-align:center"> ${item.itemsNoCoverdSize} <img @click="${context.showCrItems}" src="img/icons/items.png" width="16" height="16" class="image_inline" style="cursor: pointer" />   </td>
							 			<td class="itemscovered" style="text-align:center ; width:30%"> 
							 				${item.itemsNoCoverd}
							 			 </td>
							 			<td style="text-align:center" > ${item.reviewBy.value} </td>
							 			<td style="text-align:center"> ${context.convertTimeStampToDate(item.reviewDate)}</td>
							 		</tr>
							 		
							 		
							 		${ index+1 != context.activeDetailsRowIndex ? `` : html`
				                        <tr> 
				                            <td colspan="8">
				                               <div>          
				                                    ${ formatDetailsTable(context, context.detailsRow ) } 
				                                </div>  
				                            </td>               
				                        </tr>
				                    `
				                    }
							 		
							 	`)
							 
							 }
						`}
				</tbody>
			</table>
`

const formatDetailsTable = directive((context, detailsRow) => (part) => {
    part.setValue(html`
        <table style="width:100%;">
            <thead>
            	<tr>
            		<td colspan="6">
    					There are ${context.countCrItems()}   items
            		</td>
            	</tr>
                <tr>
    				<th style="text-align:center" class="item" scope="col"> <cwms-translate code='viewjob.itemno'>Item No </cwms-translate> </th>
					<th style="text-align:center" class="inst" scope="col">  <cwms-translate code='addjobviawebservice.instrlinkstat'> Instrument Link Status </cwms-translate>  </th>  
					<th style="text-align:center" class="serial" scope="col"> <cwms-translate code='serialno'> Serial No </cwms-translate> </th>
					<th style="text-align:center" class="plant" scope="col"> <cwms-translate code='plantno'> plantno </cwms-translate> </th>
					<th style="text-align:center" class="caltype" scope="col"> <cwms-translate code='jobresults.caltype'> ServiceType </cwms-translate> </th>
					<th style="text-align:center" class="plant" scope="col"> <cwms-translate code='viewinstrument.customerdescription'> Customer Description </cwms-translate> </th>
                    
                </tr>
            </thead>
            <tbody>
            	${ detailsRow === undefined ? `` :
                     repeat(detailsRow, (item, index)=> html`
                     <tr>
                         <td style="text-align:center">${item.itemno}</td>
                         <td style="text-align:center">${item.instrument.instrumentModelNameViaFields}</td>
                         <td style="text-align:center">${item.instrument.serialno}</td>
                         <td style="text-align:center">${item.instrument.plantno}</td>
                         <td style="text-align:center"> ${item.serviceTypeShortNameTranslation} </td>
                         <td style="text-align:center">${item.instrument.customerDescription}</td>
                    </tr>
                     `)}
            </tbody>

       </table>   
    `
    )});