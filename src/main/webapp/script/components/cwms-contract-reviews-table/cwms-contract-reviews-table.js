import { LitElement } from "../../thirdparty/lit-element/lit-element.js";
import { styling} from "./cwms-contract-reviews-table.css.js";
import { htmlTemplate} from "./cwms-contract-reviews-table.html.js";
import {} from "../cwms-translate/cwms-translate.js";


class CwmsContractReviewsTable extends LitElement {
	
	 static get dataURL() { return "getContractReviewByJobId.json?jobId=" };
	 static get dataDetailURL() { return "getContractReviewItemsByCrId.json?crId="};
	 
	 static get properties() {
	        return {
	            data: { type: Array },
	            detailsRow : { type : Array },
	            activeDetailsRowIndex: { type: Number },
	            jobId: { attribute: 'job-id' },
	            CrId: { attribute: 'Cr-id' }
	        };
	    }

	    static getStyles() {
	        return styling;
	    }

	    constructor() {
	        super();
	        this.data = new Array();
	    }
	    

	    render() {
	        return htmlTemplate(this);
	    }
	    
	    getTotalItems() {
	        var items = 0;
	        this.data.forEach(element => {
	            items = items + 1;
	        });
	        return items;
	    }
	    
	    convertTimeStampToDate(timeStamp){
	        return new Date(timeStamp).toLocaleString("en-GB");
	    }
	    
	    countCrItems(){
	    	var items = 0;
	        this.detailsRow.forEach(element => {
	            items = items + 1;
	        });
	        return items;
	    }
	    
	    showCrItems(e){  	
	        if(this.activeDetailsRowIndex == e.target.closest('tr').getAttribute('data-index'))
	            this.activeDetailsRowIndex = null;
	        else
	            this.activeDetailsRowIndex = e.target.closest('tr').getAttribute('data-index');
	        
	        if(this.activeDetailsRowIndex>=1) {
	            let CrId = this.data[this.activeDetailsRowIndex-1].id;
	            this.detailsRow = new Array();
	        	fetch(CwmsContractReviewsTable.dataDetailURL+CrId).then(res => res.text().then(text => {
	                 this.detailsRow = JSON.parse(text);
	             })); 
	            }
	    	
	    }
	    
	    
	    fetchdata(){
	    	this.data = new Array();
	    	fetch(CwmsContractReviewsTable.dataURL+jobId).then(res => res.text().then(text => {
	             this.data = JSON.parse(text);
	         }));  
	    }
	    
	    refresh() {  
	    	this.fetchdata();
	    }
	    

	
}window.customElements.define('cwms-contract-reviews-table', CwmsContractReviewsTable);