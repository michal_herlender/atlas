import { MetaDataObtainer } from "../../../tools/metadataObtainer.js";

export class HireObtainer {
    static async getPage(searchParams, pageNumber){
        searchParams.set("pageNo",pageNumber ?? 0 );
        const res = await fetch(this.prepareUrl(searchParams));
        return res.ok ? res.json() : Promise.reject(res.statusText);
    }  

 static prepareUrl(searchParams) {
    const url = new URL("hire/searchHires.json",MetaDataObtainer.base);
    url.search = searchParams;
    return url;
  }
}