import {
  STORE_SEARCH_RESULTS_PAGE,
  STORE_SEARCH_RESULTS_CURRENT_PAGE_NUMBER,
  STORE_SEARCH_RESULTS_PAGE_COUNT,
  STORE_SEARCH_RESET_STATE,
  STORE_HIRE_SEARCH_PARAMS,
} from "./actionsTypes.js";
import { html } from "../../../thirdparty/lit-html/lit-html.js";

let dispatch;
export default dispatch = function (state, commmand) {
  var basket = [];
  switch (commmand.type) {
    case STORE_SEARCH_RESET_STATE:
      return {};
    case STORE_SEARCH_RESULTS_PAGE:
      return {
        ...state,
        ...commmand.page
      };
    case STORE_SEARCH_RESULTS_PAGE_COUNT:
      return { ...state, pageCount: commmand.pageCount };
    case STORE_SEARCH_RESULTS_CURRENT_PAGE_NUMBER:
      return { ...state, currentPageNumber: commmand.currentPageNumber };
    case STORE_HIRE_SEARCH_PARAMS:
      return {...state, searchParams:commmand.searchParams};
    default:
      return state;
  }
};
