import store from "./store.js";
import { STORE_SEARCH_RESULTS_PAGE, STORE_SEARCH_RESULTS_PAGE_COUNT, STORE_SEARCH_RESULTS_CURRENT_PAGE_NUMBER, STORE_SEARCH_RESET_STATE, STORE_HIRE_SEARCH_PARAMS  } from "./actionsTypes.js";

export function storeSearchResultsPage(page){
    store.send({type:STORE_SEARCH_RESULTS_PAGE,page});
}

export function storeSearchResultsPageNumber(pageCount){
    store.send({type:STORE_SEARCH_RESULTS_PAGE_COUNT,pageCount});
}

export function storeSearchResultsCurrentPageNumber(currentPageNumber){
    store.send({type:STORE_SEARCH_RESULTS_CURRENT_PAGE_NUMBER,currentPageNumber})
}

export function storeResetState(){
    store.send({type:STORE_SEARCH_RESET_STATE});
}

export function storeHireSearchParams(searchParams){
    store.send({type:STORE_HIRE_SEARCH_PARAMS,searchParams});
}