import { LitElement, html } from "../../../../thirdparty/lit-element/lit-element.js";
import store from "../../store/store.js";
import "../../../cwms-translate/cwms-translate.js";

class CwmsHireSearchResultsCount extends LitElement {

  static get properties() {
    return { resultsCount: { type: Number } ,
              currentPage:{type:Boolean}
  };
  }

  constructor() {
    super();
    this.resultsCount = 0;
    store.subscribe(() => {
      const { resultsCount,results } = store.getState();
      this.resultsCount = this.currentPage ? results?.length??0: resultsCount ?? 0;
    });
  }

  render() {
    return html`<cwms-translate code="hiresearchinstr.searchresults"
        >Search resuts</cwms-translate
      >
      (<span class="resultsCount">${this.resultsCount}</span>) `;
  }

}


customElements.define("cwms-hire-search-results-count",CwmsHireSearchResultsCount);