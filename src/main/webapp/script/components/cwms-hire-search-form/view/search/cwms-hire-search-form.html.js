import { html } from '../../../../thirdparty/lit-element/lit-element.js'
import { stylesMixin } from '../../../tools/applicationStylesMixinTemplate.js'
import "../../../search-plugins/cwms-company-search/cwms-company-search.js"
import "../../../search-plugins/cwms-contact-search/cwms-contact-search.js"
import "../../../search-plugins/cwms-description-search/cwms-description-search.js"
import "../../../search-plugins/cwms-mfr-search/cwms-mfr-search.js"
import "../../../cwms-translate/cwms-translate.js"
import { MetaDataObtainer } from '../../../../tools/metadataObtainer.js'

export const template = (context) => html`
			${stylesMixin}
			<form @submit=${context.submitSearch}>
			<div class="container">
			<div class="column column-left">
									
				<div class="row" >
					<label><cwms-translate code="company">Company</cwms-translate>:</label>
                        <cwms-company-search @selected=${e => context.onFormChange("coid",e.detail?.coid)}></cwms-company-search>
				</div>
				<div class="row" >
					<label for="personid"><cwms-translate code="hirehome.contact">Contact</cwms-translate>:</label>												
                    <cwms-contact-search @selected=${e => context.onFormChange("personid",e.detail?.personid)}>Contact search</cwms-contact-search>
                </div>											
				<div class="row" >
					<label for="serialno"><cwms-translate code="serialnumber">Serial number</cwms-translate>:</label>												
					<input name="serialno" type="text" id="serialno" value="" tabindex="4">
                </div>
				<div class="row" >
					<label for="plantno"><cwms-translate code="plantno">Plant No</cwms-translate>:</label>												
					<input name="plantno" type="text" id="plantno" value="" tabindex="5">
                </div>
				<div  class="row" id="descrip">
                       <label><cwms-translate code="sub-family">Sub-Family</cwms-translate>:</label>
                        <!-- float div left -->
                        <cwms-description-search @selected=${e => context.onFormChange("descid",e.detail?.key)}>description search</cwms-description-search>
		          </div>   
					<div  class="row" id="manufacturer">
						<label><cwms-translate code="manufacturer">Brand</cwms-translate>:</label>
						${renderBrandSearch(context)}
					</div>
					<div class="row">
						<label><cwms-translate code="model">Model</cwms-translate>:</label>
							   <input id="model" name="model" tabindex="7" type="text" value=""></div>
																													
									
									<div class="row">
									<label></label>
		<input type="submit" value="${context.submitLabel}"/>
		<button    tabindex="0"><cwms-translate code="hirehome.clearform">Clear form</cwms-translate></button>
		</div>
			</div>
			<div class="column column-right">
			
					<div class="row">
						<label for="jobno"><cwms-translate code="hirehome.hireno">Hire No</cwms-translate>:</label>
						<input type="text" name="hireNo" id="hireNo" value="" tabindex="12">
						</div>
					<div class="row">
						<label for="plantid"><cwms-translate code="barcode">Barcode</cwms-translate>:</label>												
						<input type="text" name="plantid" id="plantid" value="" tabindex="13">
						</div>
					<div class="row">
						<label for="clientRef"><cwms-translate code="hirehome.clientref">Client Ref</cwms-translate>:</label>												
						<input type="text" name="clientRef" id="clientRef" value="" tabindex="14">
						</div>
					<div class="row">
						<label for="purOrder"><cwms-translate code="hirehome.purchaseorder">Purchase Order</cwms-translate>:</label>												
						<input type="text" name="purOrder" id="purOrder" value="" tabindex="15">
						</div>
					<div class="row">
						<label for="statusid"><cwms-translate code="hirehome.hirestatus">Hire Status</cwms-translate>:</label>
						<select name="statusid">
							<option value="">All</option>
							${context.statusList.map(st => html`<option value=${st.key}>${st.value}</option>`)}
							</select>
						</div>
					<div class="row">
						<label for="bookedInDate1"><cwms-translate code="hirehome.hiredate">Hire Date</cwms-translate>:</label>
						
						<select name="hireDateBetween" id="hireDateBetween" tabindex="9" @change=${e => context.onFormChange("hireDateBetween",e.target.value=="true")} >
							<option value="false"> <cwms-translate code="hirehome.on">On</cwms-translate></option>
							<option value="true"> <cwms-translate code="hirehome.between">between</cwms-translate> </option>
						</select>
						<input type="date" name="hireDate1" id="hireDate1"  value="" tabindex="10">
						</div>
						${context.hireDateBetween ? html`
						<div class="row" > 
						<label></label>
							<span class="label"><cwms-translate code="hirehome.and">and</cwms-translate></span>
							<input type="date" name="hireDate2" id="hireDate2"  tabindex="11">
							</div>
							`:html``}
						
			
			</div>
			<!-- end of right column -->
		
		</div>
		</form>
		
`;


const renderBrandSearch = context => html`
<div class="mfr">
	${context.searchBrandByText?html`<input name="mfr" type="text" @change=${e => context.onFormChange("mfr",e.target.value)} />`:html` 
						<cwms-mfr-search @selected=${e => context.onFormChange("mfrid",e.detail?.id)}>MFR SEARCH PLUGIN</cwms-mfr-search>`}
	<div class="buttons">
	<a role="button" @click=${_ =>context.setSearchBrandByText(false)}><img src="${MetaDataObtainer.contextPath}/img/icons/searchplugin_sel.png" width="16" height="16" alt="Search Matching Mfrs" title="Search Matching Mfrs"></img>
	</a>
	<a role="button" @click=${_ =>context.setSearchBrandByText(true)}><img src="${MetaDataObtainer.contextPath}/img/icons/textsearch.png" width="16" height="16" alt="Search Text Only" title="Search Text Only"></img>
	</a>
	</div>
</div>
`
