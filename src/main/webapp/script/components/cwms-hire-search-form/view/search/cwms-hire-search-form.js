import {
  LitElement,
  html,
} from "../../../../thirdparty/lit-element/lit-element.js";
import { template } from "./cwms-hire-search-form.html.js";
import { styling } from "./cwms-hire-search-form.css.js";
import CwmsTranslate from "../../../cwms-translate/cwms-translate.js";
import {
  storeSearchResultsPage,
  storeResetState,
  storeHireSearchParams,
} from "../../store/actions.js";
import { HireObtainer } from "../../tools/HireObtainer.js";

class CwmsHireSearchForm extends LitElement {
  static get properties() {
    return {
      formData: { type: Object, attributes: false },
      statusList:{type:Array},
      searchBrandByText: { type: Boolean },
      submitLabel: { type: String },
    };
  }

  constructor() {
    super();
    this.formData = {};
    this.searchBrandByText = false;
    CwmsTranslate.subscribeMessage(
      "hirehome.submit",
      "Submit",
      (m) => (this.submitLabel = m)
    );
  }

  static get styles() {
    return styling({});
  }

  submitSearch(event) {
    event.preventDefault();
    storeResetState();
    const searchParams = new URLSearchParams(new FormData(event.target));
    Object.entries(this.formData).forEach((e) =>
      searchParams.append(e[0], e[1])
    );
    storeHireSearchParams(searchParams);
    HireObtainer.getPage(searchParams)
      .then((res) => {
        window.switchMenuFocus(
          window.menuElements,
          "search-results-tab",
          false
        );
        storeSearchResultsPage(res);
      })
      .catch(console.error);
  }

  updateFormData(field, value) {
    this.formData = { ...this.formData, [field]: value };
  }

  render() {
    return template({
      onFormChange: this.updateFormData.bind(this),
      searchBrandByText: this.searchBrandByText,
      setSearchBrandByText: (flag) => {
        this.searchBrandByText = flag;
      },
      hireDateBetween: this.formData.hireDateBetween,
      submitLabel: this.submitLabel,
      submitSearch: this.submitSearch,
      statusList:this.statusList,
    });
  }
}

customElements.define("cwms-hire-search-form", CwmsHireSearchForm);
