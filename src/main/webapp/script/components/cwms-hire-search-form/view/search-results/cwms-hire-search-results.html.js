import { html } from "../../../../thirdparty/lit-element/lit-element.js";
import { stylesMixin } from "../../../tools/applicationStylesMixinTemplate.js";
import "../result-count/cwms-hire-search-results-count.js";
import "../../../cwms-tooltips/cwms-company-link-and-tooltip/cwms-company-link-and-tooltip.js";
import "../../../cwms-tooltips/cwms-contact-link-and-tooltip/cwms-contact-link-and-tooltip.js";

export const template = (context) => html`
  ${stylesMixin}
  ${paginationInfo(context)}
  <table
    class="default4 hireSearchResults"
    summary=${context.tableHeaderSummary}
  >
    <thead>
      <tr>
        <td colspan="7">
          <cwms-hire-search-results-count .currentPage=${true}></cwms-hire-search-results-count>
        </td>
      </tr>
      <tr>
        <th class="hsrhireno" scope="col">
          <cwms-translate code="hirehome.hireno">Hire No</cwms-translate>
        </th>
        <th class="hsrconame" scope="col">
          <cwms-translate code="company">Company</cwms-translate>
        </th>
        <th class="hsrcontact" scope="col">
          <cwms-translate code="hirehome.contact">contact</cwms-translate>
        </th>
        <th class="hsrpurchaseord" scope="col">
          <cwms-translate code="hirehome.purchaseorder"
            >Purchse order</cwms-translate
          >
        </th>
        <th class="hsrclientref" scope="col">
          <cwms-translate code="hirehome.clientref"
            >Client ref</cwms-translate
          >
        </th>
        <th class="hsrhiredate" scope="col">
          <cwms-translate code="hirehome.hiredate">Hire Date</cwms-translate>
        </th>
        <th class="hsrstatus" scope="col">
          <cwms-translate code="hireresult.status"
            >Hire Status</cwms-translate
          >
        </th>
      </tr>
    </thead>
  </table>

  ${resultsTemplate(context)}

  <!-- Begin pagination section -->
  ${paginationInfo(context)}
`;

const resultsTemplate = (context) =>
  context.results?.map(resultTemplate(context)) ?? html``;

const resultTemplate = (context) => (hire) => html`
  <table
    class="default4 hireSearchResults"
    summary=${context.tableResultSummary}
  >
    <tbody>
      <tr @click=${(_) => context.openHire(hire)} >
        <td class="hsrhireno">
          <a
            target="_blank"
            href=${context.getHireUrl(hire)}
            class="mainlink"
            >${hire.hireNumber}</a
          >
        </td>
        <td class="hsrconame" @click=${e => e.stopPropagation()}>
          <cwms-company-link-and-tooltip class="mainlink"
            companyId=${hire.companyId}
            companyName=${hire.companyName}
            >${hire.companyName}</cwms-company-link-and-tooltip
          >
        </td>
        <td class="hsrcontact">
          <cwms-contact-link-and-tooltip class="mainlink"
            contactId=${hire.contactId}
            contactName=${hire.contactName}
            >${hire.contactName}</cwms-contact-link-and-tooltip
          >
        </td>
        <td class="hsrpurchaseord">${hire.poNumber}</td>
        <td class="hsrclientref">${hire.clientRef}</td>
        <td class="hsrhiredate">${dateFormatter(hire.hireDate)}</td>
        <td class="hsrstatus bold">${hire.hireStatus}</td>
      </tr>

        <tr>
      <td colspan="7" style=" padding: 0; ">
          ${hireItemsTemplate(context)(hire.items)}
      </td>
        </tr>
    </tbody>
  </table>
`;

const dateFormatter = (dateString) => Intl.DateTimeFormat({timeZone: 'Europe/Paris'}).format(new Date(dateString));

const hireItemsTemplate = (context) => (items) => items && items.length >  0 ?  html`
  <table class="child-table" summary=${context.tableItemsSummary}>
    <thead>
      <tr>
        <th class="hsritem" scope="col">
          <cwms-translate code="hirehome.item">Item</cwms-translate>
        </th>
        <th class="hsrinstr" scope="col">
          <cwms-translate code="hireresult.instrument"
            >Instrument</cwms-translate
          >
        </th>
        <th class="hsrserial" scope="col">
          <cwms-translate code="serialno">Serial Number</cwms-translate>
        </th>
        <th class="hsrplant" scope="col">
          <cwms-translate code="plantno">Plant Number</cwms-translate>
        </th>
        <th class="hsrcaltype" scope="col">
          <cwms-translate code="hirehome.caltype"
            >Calibration type</cwms-translate
          >
        </th>
      </tr>
    </thead>
    <tbody>
        ${items
            .map(item => ({...item, rowClass:item.suspended?"highlight":!item.offHire?"highlight-gold":""}))
            .map(item => html`
      <tr class=${item.rowClass}>
        <td class="hsritem">${item.itemNumber}</td>
        <td class="hsrinstr">
            ${item.instrument}
        </td>
        <td class="hsrserial">${item.serialNumber}</td>
        <td class="hsrplant">${item.plantNumber}</td>
        <td class="hsrcaltype">${item.calType}</td>
      </tr>`)}
    </tbody>
  </table>
`:html``;


const paginationInfo = context => html`
  <div class="page-navigation">
    <button @click=${context.loadPrevPage} .disabled=${!context.previousPage} ><cwms-translate code="pagination.prev">prev</cwms-translate></button>
    <div class="pagination-info">
        ${context.paginationInfo}
    </div>
    <button @click=${context.loadNextPage} .disabled=${!context.nextPage}><cwms-translate code="pagination.next">next</cwms-translate></button>
  </div>
`

export const pageSelector = context => html`
<select @change=${e => context.loadPage(e.currentTarget.value)}>${Array.from(Array(context.pageCount),(_,i)=>i+1).map(i =>html`
            <option value=${i} .selected=${i==context.currentPage}>${i}</option>`)}
</select>`;