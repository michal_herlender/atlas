import { css } from '../../../../thirdparty/lit-element/lit-element.js'

export const styling = (context) => css`

    .hireSearchResults {
        font-size: 11px;
    }

    .hireSearchResults >tbody >  tr:hover {
        background-color:#8FB3ED;
        color:#FFF;
        cursor:pointer;
    }

    .hireSearchResults tr td .mainlink {
        color:#0063be
    }

    .hireSearchResults tr:hover td .mainlink{
        color: #FFF;
       text-decoration: underline;
       --link-color: #FFF;
       --link-decoration: underline;
    }


    .page-navigation {
        display:grid;
        grid-template-rows: 1fr;
        grid-template-columns: 50px 1fr 50px;
        grid-auto-flow:column;
        grid-gap:2rem;

    }

    .pagination-info {
        text-align: center;
    }
`;