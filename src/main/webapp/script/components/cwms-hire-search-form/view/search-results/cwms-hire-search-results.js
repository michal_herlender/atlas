import {
  LitElement,
  html,
} from "../../../../thirdparty/lit-element/lit-element.js";
import { template, pageSelector } from "./cwms-hire-search-results.html.js";
import store from "../../store/store.js";
import { styling } from "./cwms-hire-search-results.css.js";
import { HireObtainer } from "../../tools/HireObtainer.js";
import { storeSearchResultsPage } from "../../store/actions.js";
import { MetaDataObtainer } from "../../../../tools/metadataObtainer.js";

class CwmsHireSearchResults extends LitElement {
  static get properties() {
    return {
      tableHeaderSummary: { type: String },
      tableResultSummary: { type: String },
      tableItemsSummary: { type: String },
      displayInPagination: { type: String },
      paginationOf: { type: String },
      paginationWithResults: { type: String },
      state: { type: Object },
    };
  }

  static get styles() {
    return styling({});
  }

  constructor() {
    super();
    ///DEBUG ONLY
    window.store = store;
    ///DEBUG ONLY ^^^^
    CwmsTranslate.subscribeMessage(
      "hireresult.tablesubitemssummary",
      "This table displays any hire items that appear on this hire",
      (m) => (this.tableItemsSummary = m)
    );
    CwmsTranslate.subscribeMessage(
      "hireresult.tablesearchresultsummary",
      "This table displays hires returned",
      (m) => (this.tableResultSummary = m)
    );
    CwmsTranslate.subscribeMessage(
      "hireresult.tablesummary",
      "This table lists all hire search results",
      (m) => (this.tableHeaderSummary = m)
    );
    CwmsTranslate.subscribeMessage(
      "pagination.displayinpage",
      "Displaying page",
      (m) => (this.displayInPagination = m)
    );
    CwmsTranslate.subscribeMessage(
      "pagination.of",
      "of",
      (m) => (this.paginationOf = m)
    );
    CwmsTranslate.subscribeMessage(
      "pagination.withnbresults",
      "with {0} results",
      (m) => (this.paginationWithResults = m)
    );
    store.subscribe(() => {
      this.state = store.getState();
    });
  }

  buildPaginationInfo() {
    const withResults = this.paginationWithResults.replace(
      "{0}",
      this.state?.results?.length ?? 0
    );
    return html`${this.displayInPagination}
    ${pageSelector({ ...this.state, loadPage: this.loadPage.bind(this) })}
    ${this.paginationOf} ${this.state?.pageCount ?? 0} ${withResults}`;
  }

  loadPageRelativeToCurrent(move) {
    const { currentPage } = this.state;
    this.loadPage(currentPage + move);
  }

  loadPage(pageNumber) {
    const { searchParams } = this.state;
    HireObtainer.getPage(searchParams, pageNumber)
      .then(storeSearchResultsPage)
      .catch(console.error);
  }

  getHireUrl(hire){
    const url = new URL("viewhire.htm",MetaDataObtainer.base);
    const searchParams = new URLSearchParams();
    searchParams.append("id",hire.hireId);
    url.search = searchParams;
    return url;
  }

  render() {
    return template({
      tableHeaderSummary: this.tableHeaderSummary,
      tableResultSummary: this.tableResultSummary,
      tableItemsSummary: this.tableItemsSummary,
      paginationInfo: this.buildPaginationInfo(),
      ...this.state,
      loadNextPage: (_) => this.loadPageRelativeToCurrent(1),
      loadPrevPage: (_) => this.loadPageRelativeToCurrent(-1),
      getHireUrl: this.getHireUrl,
      openHire: hire => window.open(this.getHireUrl(hire),"_blank"),
    });
  }
}

customElements.define("cwms-hire-search-results", CwmsHireSearchResults);
