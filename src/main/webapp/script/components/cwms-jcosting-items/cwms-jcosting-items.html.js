import { html } from "../../thirdparty/lit-element/lit-element.js";
import {repeat} from "../../thirdparty/lit-html/directives/repeat.js";
import { directive } from "../../thirdparty/lit-html/lit-html.js";
import { currencyFormatWithZeroDirective, dateTimeFormatDirective } from "../tools/formatingDirectives.js";

export let htmlTemplate = (context) => html `

    <link rel="stylesheet" href="styles/structure/elements/table.css">
    <link rel="stylesheet" href="styles/theme/theme.css">
    <link rel="stylesheet" href="styles/text/text.css">
    <table class="default4 jobCostings tablesorter">
        <thead>
            <tr> 
                <td colspan="8"><cwms-translate code='viewjob.jobcostings'>Job Prices</cwms-translate>
                                (${context.getTotalItems()})
                </td>
            </tr>
            <tr>
                <th class="costno" scope="col" style="text-align: center;">
                    <cwms-translate code='viewjob.costingno'>Price No</cwms-translate>
                </th>
                <th class="items" scope="col" style="text-align: center;">
                    <cwms-translate code='viewjob.costeditems'>Priced Items</cwms-translate>
                </th>
                <th class="costtype" scope="col" style="text-align: center;">
                    <cwms-translate code='jobresults.type'>Type</cwms-translate>
                </th>
                <th class="costingtype" scope="col" style="text-align: center;">
                    <cwms-translate code='viewjob.costtype' >Pricing Type</cwms-translate>
                </th>
                <th class="createdby" scope="col" style="text-align: center;">
                    <cwms-translate code='viewjob.creatonby'>Created On/By</cwms-translate>
                </th>
                <th class="createdby" scope="col" style="text-align: center;">
                    <cwms-translate code='viewjob.lastview'>Last View</cwms-translate>
                </th>
                <th class="updated" scope="col" style="text-align: center;">
                    <cwms-translate code='viewjob.lastupd'>Last Update</cwms-translate>
                </th>
                <th class="coststatus" scope="col" style="text-align: center;">
                    <cwms-translate code='jobresults.stat'>Status</cwms-translate>
                </th> 
            </tr>
        </thead>
        <tbody>
            ${ context.data.length < 1 ? html` 
                <tr>
                    <td colspan="8" class="bold center"><cwms-translate code='viewjob.string39'></cwms-translate></td>
                </tr>
            ` : html`${ 
                    repeat(context.data, (item, index)=> html`
                    <tr data-index="${index+1}">
                            <td class="costno">
                                <a href="${context.jobCostingURL}?id=${item.jobCostingId}&amp;ajax=jobcosting&amp;width=600" class="jconTip mainlink">
                                  ${item.jobNo}&nbsp;ver ${item.ver}
                                </a>
                                ${item.personid !== item.costContact ? '': html`<br/> ${item.costContactName}-${item.coname}`}
                            </td>
                        <td>
                            <center>
                                ${item.itemCount}
                                <img @click="${context.open}" src="img/icons/items.png" width="16" height="16" class="basket" />
                            </center>
                        </td>
                        <td>${item.type}</td>
                        <td>${item.pricingType}</td>
                        <td>${item.differenceDateCreated}<br>${dateTimeFormatDirective(item.createdOn)}/ <br>${item.createdBy}</td>
                        <td>${item.differenceDateViewed}<br>${dateTimeFormatDirective(item.lastViewedOn)}/ <br>${item.lastViewedBy}</td>
                        <td>${item.differenceDateUpdated}<br>${dateTimeFormatDirective(item.lastUpdateOn)}/ <br>${item.lastUpdateBy}</td>
                        <td>${item.status}</td>
                    </tr>

                    ${ index+1 != context.activeDetailsRowIndex ? `` : html`
                        <tr> 
                            <td colspan="8">
                               <div>          
                                    ${ formatDetailsTable(context, context.detailsRow, item.currency) } 
                                </div>  
                            </td>               
                        </tr>
                    `
                    }
                `) 
            }
        `}
        </tbody>
     </table>
`
const formatDetailsTable = directive((context, detailsRow, currency) => (part) => {
    part.setValue(html`
        <table style="width:100%;">
            <thead>
                <tr>
                    <th class="item" scope="col" style="text-align: center;">
                        <cwms-translate code='item'>Item</cwms-translate>
                    </th>
                    <th class="model" scope="col" style="text-align: center;">
                        <cwms-translate code='model'>Model</cwms-translate>
                    </th>
                    <th class="model" scope="col" style="text-align: center;">
                        <cwms-translate code='barcode'>Barcode</cwms-translate>
                    </th>
                    <th class="serial" scope="col" style="text-align: center;">
                        <cwms-translate code='serialno'>Serial No</cwms-translate>
                    </th>
                    <th class="plant" scope="col" style="text-align: center;">
                        <cwms-translate code='plantno'>Plant No</cwms-translate>
                    </th>
                    <th class="caltype" scope="col" style="text-align: center;">
                        <cwms-translate code='caltype'>Cal Type</cwms-translate>
                    </th>
                    <th class="customerdesc" scope="col" style="text-align: center;">
                        <cwms-translate code='instru.customerDescription'>Customer description</cwms-translate>
                    </th>
                    <th class="discval" scope="col" style="text-align: center;">
                        <cwms-translate code='jobcost.discount'>Discount</cwms-translate>
                    </th>
                    <th class="finalcost" scope="col" style="text-align: center;">
                        <cwms-translate code='jobcost.finalcost'>Final Price</cwms-translate>
                    </th>
                </tr>
            </thead>
            <tbody>
            	${ detailsRow === undefined ? `` :
                     repeat(detailsRow, (item, index)=> html`
                     <tr>
                         <td>${item.jobItemNo}</td>
                         <td>${item.modelName}</td>
                         <td>${item.instrumentId}</td>
                         <td>${item.instrumentSerialNo}</td>
                         <td>${item.instrumentPlantNo}</td>
                         <td>${item.serviceTypeShortNameTranslation}</td>
                         <td>${item.customerDescription}</td>
                         <td>${item.jobCostingType !== 'SITE' ? html`
                               ${currencyFormatWithZeroDirective(item.generalDiscountValue, currency)}` : html`N/A`}
                         </td>
                         <td>${item.jobCostingType !== 'SITE' ? html`
                               ${currencyFormatWithZeroDirective(item.finalCost, currency)}` : html`N/A`}
                         </td>
                    </tr>
                     `)}
            </tbody>

       </table>   
    `
    )});
