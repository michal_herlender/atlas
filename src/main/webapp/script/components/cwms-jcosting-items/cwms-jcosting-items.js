import { LitElement } from "../../thirdparty/lit-element/lit-element.js";
import { styling} from "./cwms-jcosting-items.css.js";
import { htmlTemplate} from "./cwms-jcosting-items.html.js";
import {} from "../cwms-translate/cwms-translate.js";
import { urlWithParamsGenerator } from "../../tools/UrlGenerator.js";

class CwmsJobCostingItems extends LitElement {

    static get dataURL() { return "showJobCostings.json" };
    static get dataDetailURL() { return "showJobCostingItems.json"};
    static get jobCostingURL() {return "viewjobcosting.htm"};

    static get properties() {
        return {
            data: { type: Array },
            detailsRow : { type : Array },
            activeDetailsRowIndex: { type: Number },
            jobId: { attribute: 'job-id' },
            jobCostingId: { attribute: 'job-costing-id' },
            jobCostingURL: { type: String },
        };
    }

    static getStyles() {
        return styling;
    }

    constructor() {
        super();
        this.data = new Array();
    }

    render() {
        return htmlTemplate(this);
    }

    getTotalItems() {
        var items = 0;
        this.data.forEach(element => {
            items = items + 1;
        });
        return items;
    }

    convertTimeStampToDate(timeStamp){
        return new Date(timeStamp).toLocaleString("en-GB");
    }

    open(e) {
        if(this.activeDetailsRowIndex == e.target.closest('tr').getAttribute('data-index'))
            this.activeDetailsRowIndex = null;
        else
            this.activeDetailsRowIndex = e.target.closest('tr').getAttribute('data-index');
        if(this.activeDetailsRowIndex>=1) {
            let jobCostingId = this.data[this.activeDetailsRowIndex-1].jobCostingId;
            //fetch details data
            fetch(urlWithParamsGenerator(CwmsJobCostingItems.dataDetailURL,{jobCostingId}))
            .then(res => res.ok?res.json():Promise.reject(res.statusText))
            .then(data =>  this.detailsRow = data);
        }
    }

    addData(){ 
        this.data = new Array();
        this.jobCostingURL = CwmsJobCostingItems.jobCostingURL;
        fetch(urlWithParamsGenerator(CwmsJobCostingItems.dataURL,{jobId}))
            .then(res => res.ok?res.json():Promise.reject(res.statusText))
            .then(data => this.data = data);  
    }

    /* public methods */
    refresh() {        
        // fetch data
        this.addData();
    }
}

window.customElements.define('cwms-jcosting-items', CwmsJobCostingItems);