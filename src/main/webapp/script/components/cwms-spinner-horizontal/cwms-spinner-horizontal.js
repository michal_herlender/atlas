import { LitElement, html } from "../../thirdparty/lit-element/lit-element.js";
import { styling } from "./cwms-spinner-horizontal.css.js";

class CwmsSpinnerHorizontal extends LitElement {

    static get properties(){
        return {spinnerWidth:{type:Number}}
    }

    static get styles(){
        return styling({});
    }
    render(){
       return html`
               <div class="spinner">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
    </div>
       `;
    }

}


customElements.define("cwms-spinner-horizontal",CwmsSpinnerHorizontal);