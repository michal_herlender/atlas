import { css } from '../../thirdparty/lit-element/lit-element.js'

export const styling = (context) => css`
    .spinner {
        display: inline-block;
        position: relative;
        width: 150px;
        height: 30px
    }

    .spinner div {
        transform-origin:  50% 50%;
        animation: spinner 1.2s linear infinite;
    }


    .spinner div:after {
        content: " ";
        display: block;
        position: absolute;
        top: 3px;
        left: 0px;
        width: 6px;
        height: 18px;
        border-radius: 20%;
        background-color:var(--cwms-main-dark-background,yellow);
      }

      .spinner div:nth-child(1) {
            transform: translateX(0%);
            animation-delay: -1.1s;
      }
      .spinner div:nth-child(2) {
        animation-delay: -1s;
      }
      .spinner div:nth-child(3) {
        transform: translateX(8%);
        animation-delay: -0.9s;
      }
      .spinner div:nth-child(4) {
        transform: translateX(16%);
        animation-delay: -0.8s;
      }
      .spinner div:nth-child(5) {
        transform: translateX(24%);
        animation-delay: -0.7s;
      }
      .spinner div:nth-child(6) {
        transform: translateX(32%);
        animation-delay: -0.6s;
      }
      .spinner div:nth-child(7) {
        transform: translateX(40%);
        animation-delay: -0.5s;
      }
      .spinner div:nth-child(8) {
        transform: translateX(48%);
        animation-delay: -0.4s;
      }
      .spinner div:nth-child(9) {
        transform: translateX(56%);
        animation-delay: -0.3s;
      }
      .spinner div:nth-child(10) {
        transform: translateX(64%);
        animation-delay: -0.2s;
      }
      .spinner div:nth-child(11) {
        transform: translateX(72%);
        animation-delay: -0.1s;
      }
      .spinner div:nth-child(12) {
        transform: translateX(80%);
        animation-delay: 0s;
      }

      @keyframes spinner {
        0% {
          opacity: 1;
        }
        100% {
          opacity: 0;
        }
      }
`;