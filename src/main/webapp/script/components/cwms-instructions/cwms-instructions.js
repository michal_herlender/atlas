import {LitElement} from "../../thirdparty/lit-element/lit-element.js";
import {styling} from "./cwms-instructions.css.js";
import {htmlTemplate} from "./cwms-instructions.html.js";
import {urlWithParamsGenerator} from "../../tools/UrlGenerator.js";
import {debounce} from "../../tools/Debounce.m.js";
import cascadeStore from "../cwms-cascade-search/store/store.js";

class CwmsInstructions extends LitElement {

    static get dataURL() {
        return "getInstructions.json"
    }

    static get properties() {
        return {
            data: {type: Array},
            allocatedSubdivid: {attribute: 'allocated-subdivid', reflect: true},
            linkCoid: {attribute: 'link-coid', reflect: true},
            linkSubdivid: {attribute: 'link-subdivid', reflect: true},
            linkContactid: {attribute: 'link-contactid', reflect: true},
            linkContractid: {attribute: 'link-contractid', reflect: true},
            linkJobid: {attribute: 'link-jobid', reflect: true},
            jobIds: {attribute: 'job-ids', reflect: true},
            jobItemIds: {type: Array, attribute: 'jobitem-ids', reflect: true},
            instructionTypes: {attribute: 'instruction-types', reflect: true},
            deliveryType: {attribute: 'delivery-type'},
            showMode: {attribute: 'show-mode', reflect: true},
        };
    }

    static get styles() {
        return styling;
      }

    constructor() {
        super();
        this.data = [];
        this.isRelated = false;
        this.showInHead = false;
        cascadeStore.subscribe(() => {
            const {coid, subdivId, personId} = cascadeStore.getState();
            if (coid) this.linkCoid = coid;
            if (subdivId) this.linkSubdivid = subdivId;
            if (personId) this.linkContactid = personId;
        })
        this.debouncedAddData = debounce(this.addData, 500);
    }

    render() {
        return htmlTemplate(this);
    }

    getTotalItems() {
        return this.data.length;
    }

    addData() {
        this.data = [];
        fetch(urlWithParamsGenerator(CwmsInstructions.dataURL, {
            'allocatedSubdivid': this.allocatedSubdivid,
            'linkCoids': this.linkCoid,
            'linkSubdivids': this.linkSubdivid,
            'linkContactids': this.linkContactid,
            'linkContractid': this.linkContractid,
            'linkJobid': this.linkJobid,
            'jobIds': this.jobIds,
            'jobitemIds': this.jobItemIds,
            'instructionTypes': this.instructionTypes,
            'deliveryType': this.deliveryType
        })).then(res => res.ok ? res.json() : Promise.reject(res.statustText))
            .then(json => {
                this.data = json;
            });
    }

    attributeChangedCallback(...args) {
        super.attributeChangedCallback(...args);
        this.debouncedAddData();
    }

    firstUpdated(changedProperties) {
        this.debouncedAddData();
    }

    refresh() {
        this.debouncedAddData();
    }

}

window.customElements.define('cwms-instructions', CwmsInstructions);