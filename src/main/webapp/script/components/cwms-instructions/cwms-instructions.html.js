import { html } from "../../thirdparty/lit-element/lit-element.js";
import {repeat} from "../../thirdparty/lit-html/directives/repeat.js";
import { directive } from "../../thirdparty/lit-html/lit-html.js";

export let htmlTemplate = (context) => html `
	<link rel="stylesheet" href="styles/structure/elements/table.css">
    <link rel="stylesheet" href="styles/theme/theme.css">
    <link rel="stylesheet" href="styles/structure/structure.css">
    <link rel="stylesheet" href="styles/text/text.css">
	
	${ context.showMode == 'BASIC' ? 
		html `
				<div class="maxheight">
				<fieldset>	
					<ol id="instructionList">
						${ context.data.length < 1 ? 
							html ` 
	                				<li>
	                					<span>
											<cwms-translate code='instruction.empty'>No instructions to display !</cwms-translate>
										</span>
	                				</li>
	            				` : 
	            			html `
	            					${ repeat(context.data, (item, index)=> 
	            						html `
		                    					<li id="instruction${item.id}">
													<div class="notes_note instructions_note">
														${(item.linkedToType == 'COMPANY' || item.linkedToType == 'SUBDIV') && item.isSubdivInstruction ? 
													html `
														<img src="img/icons/house.png" width="24" height="16" 
														alt='Setting is specific per business company subdivision' 
														title='Setting is specific per business company subdivision'/>
														` : 
													html `
														<img src="img/icons/building.png" width="24" height="16" 
														alt='Setting is specific per business company' 
														title='Setting is specific per business company'/>
														`
												 } 
												<cwms-translate code='${item.linkedTypeMessageCode}'>${item.linkedTypeMessageCode}</cwms-translate> : 
												${(item.linkedToType == 'COMPANY' || item.linkedToType == 'SUBDIV') && item.isSubdivInstruction ? 
													html `
														<strong>[${item.businessSubdiv}]</strong> 
														` : 
													html ``
												}
														<strong>${item.typeTranslation}:</strong><br/>
														<span class="${item.color}">${item.instruction}</span>
													</div>
													<div class="clear-0"></div>
												</li>
											`)
									}
								`
							}
					</ol>	
				</fieldset>	
				</div>
			` :
		html `
		${ context.showMode == 'IN_HEAD' ? html `
				<div class="clear" style="height: 1px;"></div>
				<div class="jobInstruction maxheight">
				${ context.data.length < 1 ? 
					html`<span>
							<cwms-translate code='instruction.empty'>No instructions to display !</cwms-translate>
						</span>` : 
            		html`${ repeat(context.data, (item, index)=> 
            			html`<strong class="jobInstructionType">${item.typeTranslation}:</strong>
							 <br />
							 ${(item.linkedToType == 'COMPANY' || item.linkedToType == 'SUBDIV') && item.isSubdivInstruction ? 
							 	html `<img src="img/icons/house.png" width="24" height="16" 
									  alt='Setting is specific per business company subdivision' 
									  title='Setting is specific per business company subdivision'/>
									` : 
								html `<img src="img/icons/building.png" width="24" height="16" 
									  alt='Setting is specific per business company' 
									  title='Setting is specific per business company'/>
								     `
							   } 
														
							   ${(item.linkedToType == 'COMPANY' || item.linkedToType == 'SUBDIV') && item.isSubdivInstruction ? 
							   		html `<strong>[${item.businessSubdiv}]</strong>` : 
							   		html ``
							   	}
														
							   <span class="${item.color}">${item.instruction}</span>
							   <br />
							`)}`
				 }
				 </div>` : 
				 html `
				 ${ context.showMode == 'RELATED' ? html `
				 	<div class="maxheight">
				 	<fieldset>
					<legend><cwms-translate code='instructions'>Instructions</cwms-translate></legend>
					<ol id="instructionList">
						${ context.data.length < 1 ? 
							html`
								<span>
									<cwms-translate code='instruction.empty'>No instructions to display !</cwms-translate>
								</span>
								` : 
            				html`
            				
            					${ repeat(context.data, (item, index)=> 
            						html`
										<li id="instruction${item.id}">
											<div class="notes_note instructions_note">
												<strong>${item.typeTranslation}:</strong><br />
												<cwms-translate code="${item.linkedTypeMessageCode}">${item.linkedTypeMessageCode}</cwms-translate>
												${item.linkedToName}
												<br/>
										
												${(item.linkedToType == 'COMPANY' || item.linkedToType == 'SUBDIV') && item.isSubdivInstruction ? 
													html `
														<img src="img/icons/house.png" width="24" height="16" 
														alt="Setting is specific per business company subdivision" 
														title="Setting is specific per business company subdivision"/>
														` : 
													html `
														<img src="img/icons/building.png" width="24" height="16" 
														alt="Setting is specific per business company" 
														title="Setting is specific per business company"/>
														`
												 } 
												
												${(item.linkedToType == 'COMPANY' || item.linkedToType == 'SUBDIV') && item.isSubdivInstruction ? 
													html `
														<strong>[${item.businessSubdiv}]</strong> 
														` : 
													html ``
												}
										
												<span class="${item.color}">${item.instruction}</span>
											</div>
											<div class="notes_editor">
												${ item.lastModifiedBy != null ? 
													html `
															${item.lastModifiedBy}
															${context.showRole != null && context.showRole ? 
																html `
																	(<i>${item.lastModifiedBy}</i>)
																	` : 
																html ``
															} 
														` : 
													html `
															<cwms-translate code="unknown">Unknown</cwms-translate>
														`
												}
												${item.lastModified != null ? 
													html `
															${new Date(item.lastModified).toLocaleString()} 
														` : 
													html ``
												}
											</div>
											${item.linkId != null ? 
												html `
														<div class="notes_edit instructions_edit">
															<a href="editinstruction.htm?linkid=${item.linkId}&entity=${item.linkedToType}" class="imagelink">
																<img src="img/icons/note_edit.png" width="16" height="16" alt="Edit Instruction" title="Edit Instruction" />
															</a>
															${item.linkedToType == 'JOB' ? 
																html `
																		<a href="deleteinstruction.htm?linkid=${item.linkId}&entity=${item.linkedToType}" class="imagelink">
																			<img src="img/icons/note_delete.png" width="16" height="16" alt="Delete Instruction" title="Delete Instruction" />
																		</a>
																	` : 
																html ``
															}
														</div>
													` : 
												html ``
											}
											<div class="clear-0"></div>
										</li> 
										`)
									}
								`
						}
					</ol></fieldset></div>
					` : html `
				${ context.showMode == 'TABLE' ? html `
				<div class="maxheight">
				<fieldset>
					<legend><cwms-translate code='instructions'>Instructions</cwms-translate></legend>
				<table id="instructiontable" class="default4 highlight_table" summary="This table displays any instructions">
	<thead>
		<tr>
			<th><cwms-translate code="instructiontype.readinstruction">Please read the following instructions before proceeding</cwms-translate></th>
		</tr>
	</thead>
	<tbody>
	${ context.data.length > 0 ? 
							html `
					${ repeat(context.data, (item, index)=> 
            						html`
						<tr>
							<td>
								${(item.linkedToType == 'COMPANY' || item.linkedToType == 'SUBDIV') && item.isSubdivInstruction ? 
													html `
														<img src="img/icons/house.png" width="24" height="16" 
														alt='Setting is specific per business company subdivision' 
														title='Setting is specific per business company subdivision'/>
														` : 
													html `
														<img src="img/icons/building.png" width="24" height="16" 
														alt='Setting is specific per business company' 
														title='Setting is specific per business company'/>
														`
												 } 
												
												${(item.linkedToType == 'COMPANY' || item.linkedToType == 'SUBDIV') && item.isSubdivInstruction ? 
													html `
														<strong>[${item.businessSubdiv}]</strong> 
														` : 
													html ``
												}
								<strong>${item.typeTranslation}&nbsp;:</strong>&nbsp;
								<span class="${item.color} bold">${item.instruction}</span>
							</td>
						</tr>
						`)}` : html `
							<tr>
							<td>
							<cwms-translate code='instructiontype.readinstruction'>No instructions to display !</cwms-translate>
							</td>
							</tr>
						`}
	</tbody>
</table></fieldset></div>
				
				` : html `
				${ context.showMode == 'BASIC_IN_HEAD' ? html `
				<div class="maxheight">
				<fieldset>
				${ context.data.length < 1 ? 
							html ` 
	                				<li>
	                					<span>
											<cwms-translate code='instructiontype.readinstruction'>No instructions to display !</cwms-translate>
										</span>
	                				</li>
	            				` : 
	            			html `
	            					
				
				<li>
				<div class="notes_note">
					${ repeat(context.data, (item, index)=> 
            						html`
            						
            						<strong class="jobInstructionType">${item.typeTranslation} :</strong>
														<br />
														<span class="${item.color}">${item.instruction}</span>
														<br />	
						`)}
						</div>
						</li>
						
						`}
						</fieldset></div>
				
				` : html ``}
				`}`
		}`}`}
		  
		`                
;
