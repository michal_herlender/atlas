import { LitElement, html } from "../../thirdparty/lit-element/lit-element.js";

class CwmsLocalTimezone extends LitElement {

    static get properties(){
        return {
            name:{type:String},
            timeZone:{type:String}
        }
    }

    constructor(){
        super();
        this.timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    }

    createRenderRoot(){
        return this;
    }

    render(){
       return html`<input type="hidden" name=${this.name} value=${this.timeZone} />`;
    }

}

customElements.define("cwms-local-timezone",CwmsLocalTimezone);