import { LitElement, html, TemplateResult } from "../../thirdparty/lit-element/lit-element.js";
import { generalStyling as spinnerStyling } from "../cwms-spinner/cwms-spinner.css.js";
import { template as spinnerTemplate } from "../cwms-spinner/cwms-spinner.html.js";
import { popUpTemplateWithoutStyles } from "../tools/popUpTemplate.html.js";
import { popUpStyles } from "../tools/styles/popUp.css.js";

export class CwmsAbstractWithPopup extends LitElement {

    #showPopUp;
    
    static get properties(){
        return {
            popUpTitle:{type:String},
            popUpContentTemplate:{type:Object}
        };
    }

    get showPopUp(){
        return this.#showPopUp;
    }

    set showPopUp(show){
        const oldValue = this.#showPopUp;
        this.dispatchEvent(new CustomEvent(show?"cwms-show-popup":"cwms-close-popup"))
        this.#showPopUp=show;
        this.requestUpdate("showPopUp",oldValue);
    }
    static get styles(){
        return [spinnerStyling, popUpStyles ];
    }

    constructor(){
        super();
        this.popUpTitle ="CWMS"
        this.popUpInitialTemplate = html`
            <div class="initial-container">
                ${spinnerTemplate ({})}
            </div>
        `;

        this.popUpContentTemplate =  this.popUpInitialTemplate;   }

    resolveContent(){
        return Promise.reject("Not implemented")
    }

    invokePopUp(){
        const resolvedContent = this.resolveContent();
        if(resolvedContent instanceof Promise){
            resolvedContent.then(content => this.popUpContentTemplate = content);
        }
        else if(resolvedContent instanceof TemplateResult)
            this.popUpContentTemplate = resolvedContent;
        this.showPopUp = true;
    }

    closePopUp(){
        this.showPopUp = false;
        this.popUpContentTemplate = this.popUpInitialTemplate;
    }


    renderPopUp(){
       return html`
            ${this.showPopUp?popUpTemplateWithoutStyles(
                {
                    title:this.popUpTitle,
                    overlayContent:this.popUpContentTemplate,
                    hide:_=>this.showPopUp=false
                }
            ):html``}
       `;
    }

}

