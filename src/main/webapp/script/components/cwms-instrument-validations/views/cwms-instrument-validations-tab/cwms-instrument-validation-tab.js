import { LitElement, html } from "../../../../thirdparty/lit-element/lit-element.js";
import { styling } from "./cwms-instrument-validation-tab.css.js";
import { template } from "./cwms-instrument-validation-tab.html.js";

class CwmsInstrumentValidationTab extends LitElement {

    static get properties(){
        return {
            validations:{type:Array},
            validationStatuses:{type:Array},
            viewActions:{type:String}
        }
    }

    constructor(){
        super();
        CwmsTranslate.messagePromise("viewinstrument.viewactions","View Actions")
        .then(m => this.viewActions = m);
    }

    static get styles(){
        return [styling()];
    }

    render(){
       return template(this);
    }

}


customElements.define("cwms-instrument-validation-tab",CwmsInstrumentValidationTab);