import {  html } from "../../../../thirdparty/lit-element/lit-element.js";
import { ifDefined } from "../../../../thirdparty/lit-html/directives/if-defined.js";
import { stylesMixin } from "../../../tools/applicationStylesMixinTemplate.js";
import { dateFormatDirective } from "../../../tools/formatingDirectives.js";

export const template = (context) => html`
${stylesMixin}
<h3>validations</h3>
<table class="default2" id="validations" summary='SUMMARY'>
	<thead>
		<tr>
			<td colspan="7"><cwms-translate code="viewinstrument.instrumentvalidationrequests">Instrument Validation Requests</cwms-translate> (${context.validations.length})</td>
		</tr>
		<tr>
			<th class="issue" scope="col"><cwms-translate code="viewinstrument.issue">Issue</cwms-translate></th>
			<th class="raiseon" scope="col"><cwms-translate code="viewinstrument.raisedon">Raised On</cwms-translate></th>
			<th class="raiseby" scope="col"><cwms-translate code="viewinstrument.raisedby">Raised By</cwms-translate></th>
			<th class="status" scope="col"><cwms-translate code="status">Status</cwms-translate></th>
			<th class="actions" scope="col"><cwms-translate code="viewinstrument.actions">Actions</cwms-translate></th>
			<th class="resolveby" scope="col"><cwms-translate code="viewinstrument.resolvedby">Resolved By</cwms-translate></th>
			<th class="resolveon" scope="col"><cwms-translate code="viewinstrument.resolvedon">Resolved On</cwms-translate></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="7">&nbsp;</td>
		</tr>
	</tfoot>
	<tbody>
        ${!context.validations?noValidationsTemplate(context):validationsTemplate(context)}
									</tbody>
								</table>
`;


const noValidationsTemplate = context => html`
				<tr>
					<td class="bold text-center" colspan="7"><cwms-translate code="viewinstrument.novalidationissuesreported">No validation issues have been reported for this instrument</cwms-translate></td>
				</tr>
`

const validationsTemplate = context => context.validations.map(issue => html`
					<tr id="issue${issue.id}" >
						<td><span class=${ifDefined(issue.status.requiringAttention?"attention":undefined)} >${issue.issue}</span></td>
						<td>${dateFormatDirective(issue.raisedOn)}</td>
						<td>${issue.raisedBy}</td>
						<td class="stat">${issue.status.name}</td>
						<td><span>${issue.actions.length} ${actionToggleTemplate(context)}</span> </td>
						${addActionOrResolvedTemplate(context)(issue)}
					</tr>
`);

const actionToggleTemplate = context => html`
<a  id="actionLink0" @click=${_ => {}} role="button" ><img src="img/icons/items.png" width="16" height="16" alt=${context.viewActions} title=${context.viewActions} class="image_inline"></a>`;



const addActionOrResolvedTemplate = context => issue => !issue.status.requiredAttention?html`
								<td>${issue.resolvedBy}</td>
								<td>${dateFormatDirective(issue.resolvedOn)}</td>
	`:html`
								<td colspan="2">
									<a role="button" class="domthickbox mainlink" @click=${_ => {}} 
									title=""><cwms-translate code="viewinstrument.addaction">Add Action</cwms-translate></a>
								</td>
	`;