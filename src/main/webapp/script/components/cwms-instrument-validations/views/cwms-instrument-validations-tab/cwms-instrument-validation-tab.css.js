import {  css } from "../../../../thirdparty/lit-element/lit-element.js";

export const styling = (context) => css`

    th {
        width:10%;
    }

    th.issue {
        width: 42%;
    }

    th.actions {
        width: 8%
    }
`;