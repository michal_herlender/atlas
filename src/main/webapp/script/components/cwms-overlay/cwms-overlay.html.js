import {
    html
} from '../../thirdparty/lit-html/lit-html.js';

export let htmlTemplate = (context) => html `

${ !context.open ? `` :
	html`
	<link rel="stylesheet" href="styles/theme/theme.css">
	
	<div class="overlay-container" style="visibility:${context.open ? `visible` : `hidden`};" >
	    <span class="helper"></span>
	    <div class="overlay-content" >
	        <div class="popupCloseButton" @click="${context.hide}" >&times;</div>
	        
			<div class="scrollable-content">
	        <div class="modal-header">
	            <h2><slot name="cwms-overlay-title" >Title Here (slot name = 'cwms-overlay-title')</slot></h2>
	        </div>
	        
	        <div class="modal-body">
	        	<slot name="cwms-overlay-body">Body Here (slot name = 'cwms-overlay-body')</slot> 
	        </div>
	    </div>
	    </div>
	</div>`
}

`;