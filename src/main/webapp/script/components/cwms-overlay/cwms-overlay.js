import { htmlTemplate } from "./cwms-overlay.html.js";
import { styling } from "./cwms-overlay.css.js";
import { LitElement } from '../../thirdparty/lit-element/lit-element.js';

export default class CwmsOverlay extends LitElement {

    // watched properties
    static get properties() {
        return {
            open : { type : Boolean }
        };
    }
    
    static get styles() {
        return styling(this);
    }
    
    constructor(){
        super();
        this.open = false;
    }

    /* Overridden : see lit-element docs */
    render() {  
        return htmlTemplate(this);
    }
    
    show(){
        this.open = true;
        this.dispatchEvent(new CustomEvent('cwms-overlay-opened'));
    }
    
    hide(){
        this.open = false;
        this.dispatchEvent(new CustomEvent('cwms-overlay-closed'));
    }
    
};

window.customElements.define('cwms-overlay', CwmsOverlay);