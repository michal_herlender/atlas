import { css } from '../../thirdparty/lit-element/lit-element.js'

export let styling = (context) => css`

.overlay-container {
    background:rgba(0,0,0,.4);
    display:block;
    height:100%;
    position:fixed;
    text-align:center;
    top:0;
    width:100%;
    z-index:10000;
}
.overlay-container .helper{
    display:inline-block;
    height:100%;
    vertical-align:middle;
}
.overlay-content {
    transition: visibility 0s linear 150ms, opacity 150ms;
    background: #DFEAF4;
    box-shadow: 10px 10px 60px #555;
    display: inline-block;
    vertical-align: middle;
    position: relative;
    border-radius: 4px;
    padding: 10px 10px;
    min-height: 140px;
    min-width: 450px;
    max-height:90vh;
}

.scrollable-content {
    max-height:90vh;
    overflow:hidden;
    overflow-y:auto;
}
.popupCloseButton {
    background-color: #fff;
    border: 3px solid #999;
    border-radius: 50px;
    cursor: pointer;
    display: inline-block;
    font-family: arial;
    font-weight: bold;
    position: absolute;
    top: -20px;
    right: -20px;
    font-size: 25px;
    line-height: 30px;
    width: 30px;
    height: 30px;
    text-align: center;
}
.popupCloseButton:hover {
    background-color: #ccc;
}

.modal-header {
	background: #9BBEDD;
    border-bottom: 1px solid #999;
    color : white;
}

.modal-header h2 {
    margin-top :5px;
    margin-bottom :5px;
}
  
.modal-body {
    text-align: left;
    margin: 10px;
}

`;