import { LitElement, html, css } from "../../thirdparty/lit-element/lit-element.js";
import { stylesMixin } from "../tools/applicationStylesMixinTemplate.js";

class CwmsSubmenuElement extends LitElement {

    static get properties(){
        return {
            anchor:{type:String},
            block:{type:String},
        }
    }
    

    static get styles(){
        return css`


        :host{
            display:block;
            margin-bottom:0;
            height:25px;
        }

        :host(.selected) a.subnavtab {
            background-color: var(--cwms-main-gray-background,#DEDEDE);
            color:var(--cwms-main-dark-font-color,black);
        }


        .subnavtab {
            display:block;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
            padding: 5px 20px 5px 20px;
            background-color: var(--cwms-main-dark-background,#095BA5);
            color:var(--cwms-main-light-font-color,white);    
        }
        `
    }


    connectedCallback(){
        super.connectedCallback();
        window.menuElements.push({anchor:this.anchor,block:this.block});
    }

    click(){
        window.switchMenuFocus(window.menuElements,this.block,false);
    }

    render(){
       return html`
      ${stylesMixin} 
       <a role="button"   class="subnavtab" @click=${this.click}><slot></slot></a>`;
    }

}


customElements.define("cwms-submenu-element",CwmsSubmenuElement);