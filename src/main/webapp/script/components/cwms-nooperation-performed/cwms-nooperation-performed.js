import { htmlTemplate } from "./cwms-nooperation-performed.html.js";
import { styling } from "./cwms-nooperation-performed.css.js";
import CwmsOverlay from "../cwms-overlay/cwms-overlay.js";
import { LitElement } from '../../thirdparty/lit-element/lit-element.js';

window.customElements.define('cwms-nooperation-performed', class CwmsNoOperationPerformed extends LitElement {
	
	static get outcomesURL(){ return "actionOutcomes.json" };
    
    // watched properties
    static get properties() {
        return {
            jobItemId : { attribute : 'jobitem-id' },
            activityId : { attribute : 'activity-id' },
        	actionOutcomes : { type : Array }
        };
    }

    static get styles() {
        return styling;
    }
    
    constructor(){
        super();
        this.actionOutcomes = new Array();
    }

    /* Overridden : see lit-element docs */
    render() {  
        return htmlTemplate(this);
    }
    
    firstUpdated(){
    	this.refresh();
    }
    
    refresh(){
        fetch(CwmsNoOperationPerformed.outcomesURL+'?id='+this.activityId)
        .then(res => res.ok?res.json():Promise.reject(res.statusText))
        .then(json => this.actionOutcomes = json);
    }
    
    show(){
    	this.shadowRoot.querySelector('cwms-overlay').show();
    }
    
    hide(){
		this.shadowRoot.querySelector('cwms-overlay').hide();
    }
    
});
