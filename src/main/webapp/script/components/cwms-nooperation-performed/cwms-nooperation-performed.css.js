import { css } from '../../thirdparty/lit-element/lit-element.js'

export let styling = css`

.button {
    border: none;
    color: #FFFFFF;
    padding: 8px 18px;
    text-align: bottom;
    margin-bottom: 16px 0 !important;
    text-decoration: none;
    font-size: 12px;
    cursor: pointer;
    background-color: #555555;
 }
 
 textarea {
 	width : 100%;
 	margin : 5px 0px;
 }

`;