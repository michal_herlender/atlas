import { html } from '../../thirdparty/lit-html/lit-html.js';
import {repeat} from '../../thirdparty/lit-html/directives/repeat.js'

export let htmlTemplate = (context) => html `

<link rel="stylesheet" href="styles/theme/theme.css">

<cwms-overlay>

	<cwms-translate slot="cwms-overlay-title" code="vm_global.noactionrequired" >No action required</cwms-translate>
	
	<div slot="cwms-overlay-body">
		<form method="POST">
			<input id="jobitemid" type="hidden" name="jobitemid" value="${context.jobItemId}">
            <input type="hidden" name="nooperationperformed" value="">
            <label class="twolabel"><cwms-translate code="jiactions.actoutcome" >Outcome</cwms-translate> :</label>
            <select name="aoid">
            	${repeat(context.actionOutcomes,(i,index)=>html`
            		<option value="${i.key}">${i.value}</option>
            	`)}
            </select>
            <br/>
            <label class="twolabel"><cwms-translate code="jiactions.remark" >Remark</cwms-translate> :</label>
            <div class="float-left width80">
                <textarea id="activityremark" name="activityremark"></textarea>
                <center>
                	<input type="submit" class="button" value="Save">
                	<button type="button" class="button"  @click="${context.hide}" ><cwms-translate code="cancel" >Cancel</cwms-translate></button>
                </center>
            </div>  
		</form>
	</div>

</cwms-overlay>

`;