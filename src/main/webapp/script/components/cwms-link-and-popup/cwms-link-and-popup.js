import { LitElement, html } from "../../thirdparty/lit-element/lit-element.js";
import { generalStyling as spinnerStyling } from "../cwms-spinner/cwms-spinner.css.js";
import {template as spinnerTemplate} from  "../cwms-spinner/cwms-spinner.html.js";
import { popUpTemplate } from "../tools/popUpTemplate.html.js";
import { styling } from "./cwms-link-and-popup.css.js";
import { linkStyling } from "../tools/styles/links.css.js";

/**
 * This component is meant to be use as either a parent class for the proper component or it coud be used as a child o other component. In the first case one can easily define the resolveContent method and rerturn the promise with the new popUpContentTemplate there. In the second case just set value to either popUpContentTemplate or to resolveContent method
 */
export class CwmsLinkAndPopUp extends LitElement {

    static get properties(){
        return {
            title:{type:String},
            popUpTitle:{type:String},
            show:{type:Boolean},
            popUpContentTemplate:{type:Object}
        };
    }

    static get styles(){
        return [spinnerStyling, styling({}), linkStyling()];
    }

    constructor(){
        super();
        this.popUpTitle ="CWMS"
        this.popUpContentTemplate = html`
            <div class="initial-container">
                ${spinnerTemplate({})}
            </div>
        `;
    }

    resolveContent(){
        return Promise.reject("Not implemented")
    }

    showPopup(){
        const resolvedContent = this.resolveContent?.();
        if(resolvedContent instanceof Promise){
            resolvedContent.then(content => this.popUpContentTemplate = content);
        }
        this.show = true;
    }


    render(){
       return html`
            ${this.show?popUpTemplate(
                {
                    title:this.popUpTitle,
                    overlayContent:this.popUpContentTemplate,
                    hide:_=>this.show=false
                }
            ):html``}
            <a class="mainLink" title=${this.title} role="button" @click=${_ => this.showPopup()}><slot></slot></a>
       `;
    }

}


customElements.define("cwms-link-and-popup",CwmsLinkAndPopUp);