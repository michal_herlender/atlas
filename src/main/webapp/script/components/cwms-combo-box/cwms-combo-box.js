import { LitElement, html,  TemplateResult } from "../../thirdparty/lit-element/lit-element.js";
import { styling } from "./cwms-combo-box.css.js";
import { template } from "./cwms-combo-box.html.js";

class CwmsComboBox extends LitElement {

  #showItems;
  #items;
  #defaultvalue;

  static get properties() {
    return {
      selectedItem:{type:Object,attribute:false},
      itemRenderer:{type:Object,attribute:false},
      valueRenderer:{type:Object,attribute:false},
      focusedItem:{type:Number,attribute:false},
      allowFreeText:{type:Boolean,attribute:false},
      defaultvalue : {type : String},
      showSelected:{type:Boolean},
      value:{type:Object}
    };
  }

  get defaultvalue(){
    return this.#defaultvalue;
  }

  set defaultvalue(value){
    const oldValue = this.defaultvalue;
    this.value = value;
    this.#defaultvalue = value;
    this.requestUpdate("defaultvalue",oldValue);
  }

  static get styles(){
      return styling;
  }

  get showItems(){
    return this.#showItems;
  }

  set showItems(showItems){
    const oldValue = this.showItems;
    if(!showItems && oldValue && this.showSelected)
      this.value = !!this.selectedItem? this.chooseValueRenderer()(this.selectedItem):
        this.allowFreeText?this.value:this.defaultvalue ?? '';
    this.#showItems = showItems;
    this.requestUpdate("showItems",oldValue);

  }

  get items(){
      return this.#items||[];
  }

  set items(items){
    const oldValue = this.items;
      if(items instanceof Array){
        this.#items = items;
        this.showItems = items.length > 0 ;
      } else {
          this.#items = [];
          this.showItems = false;
      }
      this.focusedItem = undefined;
    this.requestUpdate("items",oldValue);
  }
  
  get value(){
      return this.shadowRoot.querySelector("input")?.value;
  }

  set value(value){
      let input = this.shadowRoot.querySelector("input")
      if(!!input)  input.value = value;
  }


  constructor(){
      super();
      this.showItems = false;
      this.defaultvalue = "";
      this.allowFreeText = false;
      this.showSelected = true;
  }

  connectedCallback(){
    super.connectedCallback();
    this.addEventListener("keyup",this.onKeyUp.bind(this));
  }
  onInput(event){
  }

  onInputKeyUp(event){
    switch(event.key){
      case "Tab":
        this.showItems = false;
        event.stopImmediatePropagation();
      break;
      default:
    }
  }

  onKeyUp(event){
    switch(event.key){
      case "Tab":
        this.showItems = false;
        event.stopImmediatePropagation();
        break;
      case "Enter":
        if(this.focusedItem) this.selectItem(this.focusedItem);
        else if(this.items && this.items.length > 0) this.selectItem(this.items[0])
        else if(event.target.value)  this.selectItem(event.target.value);
        break;
      case "ArrowDown":
          this.moveFocus(1);
          break;
      case "ArrowUp":
        this.moveFocus(-1);
        break;
    }
  }

  blur(){
    this.showItems = false;
  }

  moveFocus(direction){
    if(this.items && this.items.length > 0 && this.items instanceof Array){
      const itemsNode = Array.from(this.shadowRoot.querySelectorAll(".item"));
      let idx;
      if(!this.focusedItem) 
        idx = 0;
      
      else
       idx = ((this.items.indexOf(this.focusedItem)+direction) +this.items.length) % this.items.length;
      this.focusedItem = this.items[idx];
      itemsNode[idx].focus();
    }
    
  }

  

  defaultItemRenderer(item){
    if(item instanceof String) return item;
    if(item instanceof Array) return item.join(", ");
    if(item instanceof Function) return item();
    return item.label || item.value || item.name || item.text;
  }

  selectItem(item){
    this.selectedItem = item;
    this.dispatchEvent(new CustomEvent("selected",{detail:item,bubbles:true,composed:true}));
    this.showItems = false;
    this.focusedItem = undefined;
    this.value =  item && this.showSelected ? this.chooseValueRenderer()(item) :"";
  }

  chooseValueRenderer(){
    return this.valueRenderer ?? this.chooseItemRenderer();
  }

  chooseItemRenderer(){
    return this.itemRenderer && this.itemRenderer instanceof Function?
        this.itemRenderer:this.defaultItemRenderer;
  }

  renderItems(){
    const itemRenderer = this.chooseItemRenderer();
      return this.showItems ? html`
      <div class="items">${this.items.map(item  => html`<div tabindex="-1" class="item" @click=${_ => this.selectItem(item)}>${itemRenderer(item)}</div>`)}
          </div>
      `  :html``;
  }

    render(){
       return template(this);
    }

}


customElements.define("cwms-combo-box",CwmsComboBox);