import { html } from '../../thirdparty/lit-element/lit-element.js'

export const template = (context) => html`
<input type="text" .value=${context.value ?? context.defaultvalue ?? ''} id ="" @input=${context.onInput}  @keyup=${context.onKeyUp}>
${context.renderItems()}
`;