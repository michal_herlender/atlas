import { css } from '../../thirdparty/lit-element/lit-element.js'

export const styling =  css`
:host{
    position:relative;
}

input {
    width:100%
}

.items {
    height: 120px;
    overflow: auto;
    overflow-x: hidden;
    font-size: 90%;
    text-align: left;
    position: absolute;
    left: 0px;
    top: 1rem;
    background: #E0ECFF;
    width: 100%;
    border: 1px solid #666;
    z-index:100;
}

.item {
    border-bottom: 1px dotted #999;
    padding-left: 4px;
    color: #444;
    display: block;
    width: 100%;
    height: 18px;
    line-height: 1.8;
    text-decoration: none;
    padding-left: 4px;
    overflow: hidden;
    white-space: nowrap;
    font-size: 90%;
    text-align: left;
}

.item:hover, .item:focus {
    background-color: #C3D9FF;
}

`;