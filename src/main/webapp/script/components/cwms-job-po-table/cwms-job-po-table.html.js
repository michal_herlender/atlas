import { html } from "../../thirdparty/lit-element/lit-element.js";
import {repeat} from "../../thirdparty/lit-html/directives/repeat.js";


export let htmlTemplate = (context) => html `
		<link rel="stylesheet" href="styles/structure/elements/table.css">
		<link rel="stylesheet" href="styles/theme/theme.css">
		<link rel="stylesheet" href="styles/text/text.css">
		<link rel="stylesheet" href="styles/global.css" type="text/css" title="antech" />
		
		
		<table class="default2" id="purchordtable" summary="This table displays all purchase orders">
			<thead>
				<tr>
					<td colspan="6"> <cwms-translate code='viewjob.purchord'>Purchase Orders</cwms-translate>  (${context.getTotalItems()}) </td>
				</tr>
				<tr>
					<th id="ponum" scope="col"> <cwms-translate code='viewjob.ponumber'>PO Number</cwms-translate> </th>
					<th id="pocomp" scope="col"> <cwms-translate code='company'> Company </cwms-translate> </th>
					<th id="posubdiv" scope="col"> <cwms-translate code='subdiv'> Subdivision </cwms-translate> </th>  
					<th id="pocreate" scope="col"> <cwms-translate code='viewjob.createdby'> Created By </cwms-translate>  </th>
					<th id="pocreatedate" scope="col"> <cwms-translate code='viewjob.createdon'> Created On </cwms-translate> </th>
					<th id="postatus" scope="col"> <cwms-translate code='jobresults.stat'> Status </cwms-translate> </th>
				</tr>
			</thead>
			
			<tfoot>
				<tr>
				 	<td colspan="6">
						<a class="mainlink-float" href="createpurchaseorder.htm"> <cwms-translate code='viewjob.creapurchord'> Create Purchase Order </cwms-translate> </a> 
					</td>
				</tr>
			</tfoot>
			<tbody>
				 ${ context.data.length < 1 ? html` 
					        	<tr>
					            	<td colspan="8" class="bold center"><cwms-translate code='viewjob.nopurchordforjob'></cwms-translate></td>
					            </tr> ` 
							: html`${
							
							 	repeat(context.data, (item, index)=> html` 
							 		<tr data-index="${index+1}">
							 			<td> <a class="mainlink" href="viewpurchaseorder.htm?id=${item.id}"> ${item.poNo} </a>  </td>
							 			<td> <a class="mainlink" href="viewcomp.htm?coid=${item.compId}"> ${item.companyName} </a> </td>
							 			<td> <a class="mainlink" href="viewsub.htm?subdivid=${item.subdivId}"> ${item.subName} </a> </td>
							 			<td> ${item.createdByName}</td>
							 			<td>${item.regDate}</td>
							 			<td>${item.status}</td>
							 		</tr>
							 	`)
							 
							 }
						`}
						</tbody>
		</table>
		

`