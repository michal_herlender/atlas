import { LitElement } from "../../thirdparty/lit-element/lit-element.js";
import { styling} from "./cwms-job-po-table.css.js";
import { htmlTemplate} from "./cwms-job-po-table.html.js";
import {} from "../cwms-translate/cwms-translate.js";


class CwmsJobPoTable extends LitElement {
	
	 static get dataURL() { return "getPurchaseOrderByJobId.json?jobId=" };
	 
	 static get properties() {
	        return {
	            data: { type: Array },
	            jobId: { attribute: 'job-id' },
	        };
	    }
	 
	 static getStyles() {
	        return styling;
	    }

	    constructor() {
	        super();
	        this.data = new Array();
	    }
	    

	    render() {
	        return htmlTemplate(this);
	    }
	    
	    getTotalItems() {
	        var items = 0;
	        this.data.forEach(element => {
	            items = items + 1;
	        });
	        return items;
	    }
	    
	    convertTimeStampToDate(timeStamp){
	        return new Date(timeStamp).toLocaleString("en-GB");
	    }
	    
	    fetchdata(){
	    	this.data = new Array();
	    	fetch(CwmsJobPoTable.dataURL+jobId).then(res => res.text().then(text => {
	             this.data = JSON.parse(text);
	         }));  
	    }
	    
	    refresh() {  
	    	this.fetchdata();
	    }
	
	
}window.customElements.define('cwms-job-po-table', CwmsJobPoTable);
