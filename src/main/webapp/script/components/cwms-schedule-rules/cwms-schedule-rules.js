import { LitElement, html } from "../../thirdparty/lit-element/lit-element.js";
import { htmlTemplate } from "./cwms-schedule-rules.html.js";
import {repeat} from "../../thirdparty/lit-html/directives/repeat.js";

class CwmsShowScheduleRules extends LitElement {

    static get dataURL() { return "manualreschedule.json?subdivid=" }

    static get properties() {
        return {
            data: { type: Array },
            subdivid: { attribute: 'subdiv-id' },
            index: { attribute: 'index' }
        }
    }

    constructor() {
        super();
        this.data = new Array();
    } 

    render() {
        return htmlTemplate(this);
    }

    /* public methods */
    refresh() {        
        // fetch data
        this.showData();
    }


    clean(){
        this.data = new Array();
    }

    showData(){ 
        this.data = new Array();
        fetch(CwmsShowScheduleRules.dataURL+this.subdivid).then(res => res.text().then(text => {
            this.data = JSON.parse(text);
        }));
    }

    rescheduleManual(trasportOptionId, date, event){
        if(document.getElementById('dtoNewTransportOptionId'+this.index)){
            document.getElementById('dtoNewTransportOptionId'+this.index).value = trasportOptionId; 
        }

        if(document.getElementById('dtoNewdate'+this.index)){
            document.getElementById('dtoNewdate'+this.index).value = date; 
        }

    }

}

window.customElements.define('cwms-schedule-rules', CwmsShowScheduleRules);