import { html } from "../../thirdparty/lit-element/lit-element.js";
import {repeat} from "../../thirdparty/lit-html/directives/repeat.js";
import { directive } from "../../thirdparty/lit-html/lit-html.js";
import { dateFormatDirective } from "../tools/formatingDirectives.js";


export let htmlTemplate = (context) => html `


<link rel="stylesheet" href="styles/structure/elements/table.css">
<link rel="stylesheet" href="styles/theme/theme.css">
<link rel="stylesheet" href="styles/text/text.css">
<link rel="stylesheet" href="styles/structure/structure.css">

<div>
    <fieldset>
	     <table class="default2" style="border-collapse: separate; border-spacing: 2px; table-layout: fixed;">
	     	<thead>
	     		<tr>
	     			 ${Object.entries(context.data ?? {}).map(([k,v]) => 
          					html`<th>${k}</th>`)}
             	</tr>
             </thead>
             <body>
          		<tr>
          			 ${Object.entries(context.data ?? {}).map(([k,v]) => 
          					html`<td style="vertical-align:top;">
          					${Object.entries(v ?? {}).map(([i,j]) => html`
          					 <span class="bold" style="text-decoration: underline;">${dateFormatDirective(i)}</span><br>
								${repeat(j, (item) => html` &nbsp;
									<input type="radio" name="date" value="test" @change="${e => context.rescheduleManual(item.transportOptionId, item.date, e)}"/>
									<span>${item.transportOptionName}</span><br>
								`)}<br>
							`)}	
          				
          				</td>`)}
          					
          		</tr>
             </body>
		</table>
    </fieldset>
</div>

`
