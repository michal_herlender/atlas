import { CwmsAbstractWithPopup } from "../cwms-abstract-with-popup/cwms-abstract-with-popup.js";
import { linkStyling } from "../tools/styles/links.css.js";
import { styling } from "./cwms-create-hire-contract-from-enquiry.css.js";
import { popUpContentFailureTemplate, popUpContentTemplate, template } from "./cwms-create-hire-contract-from-enquiry.html.js";
import CwmsTranslate  from "../cwms-translate/cwms-translate.js";
import { urlGenerator, urlWithParamsGenerator } from "../../tools/UrlGenerator.js";
import { fieldsetStyles } from "../tools/styles/fieldset.css.js";
import { structureStyles } from "../tools/styles/structure.css.js";
import { MetaDataObtainer } from "../../tools/metadataObtainer.js";
import { textStyles } from "../tools/styles/text.css.js";
import { tableStyles } from "../tools/styles/table.css.js";

class CwmsCreateHireContractFromEnquiry extends CwmsAbstractWithPopup {

    constructor(){
        super();
        CwmsTranslate.messagePromise("hireview.createcontract.title","Convert Enquiry To Hire Contract")
        .then(msg => this.popUpTitle = msg);
    }

    static get properties(){
        return {
            ...super.properties,
            hireId:{type:Number},
        }
    }


    static get styles(){
        return [...super.styles, styling(), linkStyling(), fieldsetStyles, structureStyles, textStyles, tableStyles ]
    }

    async resolveContent(){
       const res = await fetch(urlWithParamsGenerator("hire/validateHireItemAccessories.json", { hireId: this.hireId }));
        const c = await (res.ok ? res.json() : Promise.reject(res.statusText));

        return c.success?popUpContentTemplate(this)(c):popUpContentFailureTemplate(this)(c);
    }

    render(){
       return template(this);
    }

    submitForm(e){
        e.preventDefault();
        const fd = new FormData(e.currentTarget);
        fetch(urlGenerator("hire/convertEnquiryToContract.json"),{
            method:"POST",
            credentials:"same-origin",
            headers:MetaDataObtainer.csrfHeader,
            body:fd
        })
        .then(res => res.ok ? res.json():Promise.reject(res.statusText))
        .then(res => res.right?window.location = urlWithParamsGenerator("viewhire.htm",{id:this.hireId,loadtab:"filestab"}):this.errorMessage=res.value);

    }

}


customElements.define("cwms-create-hire-contract-from-enquiry",CwmsCreateHireContractFromEnquiry);