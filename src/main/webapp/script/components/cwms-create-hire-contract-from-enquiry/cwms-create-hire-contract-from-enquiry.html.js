import { html } from '../../thirdparty/lit-element/lit-element.js'

export const template = (context) => html`
${context.renderPopUp()}
(<a class="mainLink" role="button" @click=${_ => context.invokePopUp()}>Crete hire from enquiry</a>)
`;


export const popUpContentTemplate = context => _ => html`
    <div id="createHireContractOverlay">
        ${ context.errorMessage  ? 
            html`<div class="warningBox1">
			    <div class="warningBox2">
				    <div class="warningBox3">${context.errorMessage}</div>
			    </div>
	        </div>` : ''      
            } 
        <form @submit=${context.submitForm}>
        <input type="hidden" name="hireId" .value=${context.hireId}>
            <fieldset>
                <ol class="nopadding">
                    <li>
                        <label><cwms-translate code="clientpo">Client PO</cwms-translate></label>
                        <input type="text" name="clientpo" value="" />
                    </li>
                    <li>
                        <label><cwms-translate code="hireview.specialinstructions">Special Instruction</cwms-translate></label>
                        <textarea name="specialinst" class="width70" rows="5" value=""></textarea>
                    </li>
                </ol>
                <ol class="nopadding">
                    <li>
                        <label>&nbsp;</label>
                        <button><cwms-translate code="submit">Submit</cwms-translate></button>
                    </li>
                </ol>
            </fieldset>
        </form>
    </div>`;



export const popUpContentFailureTemplate = context => data => html`
    <div id="hireItemAccessoryFailureOverlay">
        <p class="bold"> 
            <cwms-translate code="hireview.createcontract.warnHireItemAccessoryFailure1">Before this hire enquiry can be converted to a hire contract all hire item accessories must be active and at a status of 'Available'.</cwms-translate>    
         </p> 
        <p class="bold"> 
            <cwms-translate code="hireview.createcontract.warnHireItemAccessoryFailure2">The following hire item accessories do not meet this criteria. Please update before continuing.</cwms-translate>    
        </p> 
        <table class="default4" summary="this table shows any item accessories which have failed validation"> 
            <thead> 
                <tr> 
                    <th class="text-center"><cwms-translate code="hireview.hireitem">Hire Item</cwms-translate></th> 
                    <th> <cwms-translate code="hireview.accessoryDesc">Accessory Desc</cwms-translate> </th> 
                    <th>  <cwms-translate code="active">Active</cwms-translate></th> 
                    <th>  <cwms-translate code="status">Status</cwms-translate></th> 
                </tr> 
            </thead> 
            <tbody>
                ${data.results.map(hia => html`
                <tr> 
                    <td class="text-center"> ${hia.itemno}  </td> 
                    <td>  ${hia.item}  </td> 
                    <td>
                        ${hia.active?html`
                        <cwms-translate code="active">Active</cwms-translate>`:html`
                        <cwms-translate code="">Not Active</cwms-translate>`}
                    </td> 
                    <td>  ${hia.status} </td>
                </tr>
                `)}
         </tbody> 
        </table>
	</div>
`