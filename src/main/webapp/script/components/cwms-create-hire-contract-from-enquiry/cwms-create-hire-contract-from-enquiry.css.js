import { css } from '../../thirdparty/lit-element/lit-element.js'

export const styling = (context) => css`
    :host {
        --cwms-overlay-content-width: 600px;
    }
`;