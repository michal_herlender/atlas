import { css } from '../../../thirdparty/lit-element/lit-element.js'

export const linkStyling = (context) => css`

  a[role="button"] {
    cursor: pointer;
  }

  .mainLink, .mainlink {
    font-weight: bold;
    text-decoration: var(--cwms-main-link-text-decoration,none);
    line-height: normal;
    color: var(--cwms-main-link-color,#0063be);
  }

  .mainLink:hover, .mainlink:hover {
    text-decoration: var(--cwms-main-link-text-decoration-hovered,underline);
    color: var(--cwms-main-link-color-hovered, #a80000);
  }

  .image_inl {
    margin-bottom: -2px;
  }
`;