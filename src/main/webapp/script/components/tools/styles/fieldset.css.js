import { css } from "../../../thirdparty/lit-element/lit-element.js";

export const fieldsetStyles = css`
legend {
    color:var( --cwms-main-dark-font-color, #000);
	border: 1px solid #888;
	background-color: var(--cwms-main-light-font-color,#FFF);
    font-weight: bold;
    font-size: 0.9em;
    margin-left: 10px;
    padding: 2px 4px 2px 4px;
}	

fieldset {
    border: 0;
    border-top: 1px solid #666;
    background:var(--cwms-main-gray-background, #DEDEDE);
}

fieldset ol {
    margin: 0;
    padding: 5px;
}

fieldset li {
    list-style-type: none;
    border-bottom: 1px dotted #666;
    border-top: 0;
    padding: 5px;
    min-height: 21px;
}

fieldset input {
    margin-right: 5px;
}


fieldset.newtooltip {
    padding: 1px;
}

label {
    display: inline-block;
    width: 150px;
}

label ~ * {
    display: inline-block;
}

label.width30 {
    width: 30%;
}

.modal-body fieldset {
    border-top: none;
    background-color: var(--cwms-main-light-background, #DFEAF4);
}
`;