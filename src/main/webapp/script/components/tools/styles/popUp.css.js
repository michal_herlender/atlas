import { css } from '../../../thirdparty/lit-element/lit-element.js'

export const popUpStyles = css`
.overlay-container {
    position: fixed;
    top: 0;
    left: 0;
    width: 100vw;
    height: 100vh;
    background: #eeea;
    z-index: 9999;
    display: flex;
    justify-content: center;
    align-items: center;
}
.overlay-container .helper{
    display:inline-block;
    height:100%;
    vertical-align:middle;
}
.overlay-content {
    transition: visibility 0s linear 150ms, opacity 150ms;
    background: var(--cwms-main-light-background,#DFEAF4);
    box-shadow: 10px 10px 60px #555;
    display: inline-block;
    vertical-align: middle;
    position: relative;
    border-radius: 4px;
    padding: 10px 10px;
    min-height: 140px;
    min-width: var(--cwms-overlay-content-width,450px);
    width: var(--cwms-overlay-content-width,auto);
    max-width: 90vw;
    max-height:90vh;
}

.scrollable-content {
    max-height:90vh;
    overflow:hidden;
    overflow-y:auto;
}
.popupCloseButton {
    background-color: #fff;
    border: 3px solid #999;
    border-radius: 50px;
    cursor: pointer;
    display: inline-block;
    font-family: arial;
    font-weight: bold;
    position: absolute;
    top: -8px;
    right: -8px;
    font-size: 20px;
    line-height: 15px;
    width: 15px;
    height: 15px;
    text-align: center;

}
.popupCloseButton:hover {
    background-color: #ccc;
}

.modal-header {
	background: var(--cwms-table-strong-backgound,#9BBEDD);
    border-bottom: 1px solid #999;
    color : white;
    height: 20px;
    padding-top: 4px;
    font-size: 112%;
    text-align: center;
}

  
.modal-body {
    text-align: left;
    margin: 10px;
}
`