import { css } from "../../../thirdparty/lit-element/lit-element.js";

export const structureStyles = css`
.infobox
{
	padding: 10px;
	margin-bottom: 20px;
}


.infobox h5
{
	margin: 6px 0 6px 0;
}


.infobox h5
{
	text-align: center;
}

.infobox, .listbox
{
	background: var(--cwms-infobox-gray-background,#F2F2F2);
}

a.mainlink-pad:link, a.mainlink-pad:visited, a.mainlink-float:link, a.mainlink-float:visited, a.mainlink-float[role="button"]
{
	padding: 0 20px 0;
}

a.mainlink-padr:link, a.mainlink-padr:visited, a.mainlink-padr:hover
{
	padding-right: 10px;
}

a.mainlink-pad:hover, a.mainlink-float:hover
{
	padding: 0 20px 0;
}

a.mainlink-float[role="button"], a.mainlink-float:link, a.mainlink-float:visited, a.mainlink-float:hover
{
	float: right;
}

.nopadding {
	padding: 0;
}
`;
