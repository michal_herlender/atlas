import {css} from "../../../thirdparty/lit-element/lit-element.js";

export const textStyles = css`
    .bold {
        font-weight: bold;
    }
    .center {
        text-align: center;
    }

    input, select, textarea, button {
       font-family: calibri, arial, helvetica, sans-serif;
       font-size: 100%;
    }  

    h5 {
        font-size: 100%;
    }

    .warningBox1 {
        border: 2px solid var(--cwms-warning1-border,#Fdd);
        margin-top: 10px;
        margin-bottom: 10px;
    }

    .warningBox2 {
        border: 2px solid var(--cwms-warning2-border,#FF8C8C) ;
    }
    .warningBox3 {
        padding: 10px;
        border: 2px solid var(--cwms-warning3-border,#F00) ;
        background-color:var(--cwms-warning3-background, #FFF4f4);
        text-align:center;
    }

    .error {
        color: var(--cwms-error-message-color,#c00);
    }
`;
