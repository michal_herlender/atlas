import {css} from "../../../thirdparty/lit-element/lit-element.js";

export const tableStyles = css`
  table.default4 {
    font-size: 94%;
    margin: 4px 0px;
    width: 100%;
    line-height: 1.2;
    border-collapse:collapse;
  }

  table th {
      text-align: center;
  }

  table.default4 th, table.default4 thead td, table.default4 tfoot td, table.default2 th {
    background-color: var(--cwms-main-gray-background, #dedede);
    border: 1px solid #999;
    font-size: 8pt;
    padding: 4px;
  }


  table.default4 tbody td, table.default4 tfoot td, table.default2 tbody tr td {
    padding: 4px;
    vertical-align: middle;
    color: var(--cwms-main-dark-font-color,#3D3D3D);
    border: 1px solid #999;
  }

  table.default4 {
    background-color: var(--cwms-main-light-font-color,#fff);
  }


  table.default2 {
    background-color:var(--cwms-table-gray-background,#cdcdcd);
    border: solid 1px #999;
    border-collapse:collapse;
  }

  table.default2 th {
    background-color: var(--cwms-main-gray-background, #dedede);
  }

  table.default2 tbody tr {
    background-color: white;
  }
  
 table.default2 {
    margin: 10px 0px 10px;
    width: 100%;
    line-height: normal;
  }


  table.default2 {
    background-color:var(--cwms-table-gray-background,#cdcdcd);
    border: solid 1px #999;
    border-collapse:collapse;
  }

  table.default2 th {
    background-color: var(--cwms-main-gray-background, #dedede);
  }

  table.default2 tbody tr {
    background-color: white;
  }

  .odd td {
    background-color: var(--cwms-table-odd-background,#F0F0F6);
  }

  .even td {
    background-color: var(--cwms-main-light-font-color,#fff)
  }

`;


export const tableTooltipStyles = css`

    table {
        color:black;
        width:100%;
        font-size:var(--cwms-main-table-font-size);
        font-family:var(--cwms-main-font-family);
        background-color:var(--cwms-main-light-font-color,#dadada);
    }

    table th,  table thead td, table tfoot td {
        background-color: var(--cwms-table-main-background, yellow);
    }

    table th {
        font-size:8pt;
    }

    table, th, td {
        border: solid 1px #999;
        border-collapse: collapse;
        padding:4px;
        text-align: left;
        vertical-align:midle;
    }

    td {

        text-align:center;
    }

    label {
      line-height: 1.8;
    }
`;