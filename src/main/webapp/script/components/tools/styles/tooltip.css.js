import { css } from "../../../thirdparty/lit-element/lit-element.js";

export const generalTooltipStyles = css`

fieldset.newtooltip {
  background-color: var(--cwms-table-main-background,#D9ECFF);
  border-bottom: 1px solid #999;
  font-size: 86%;
}

.default2 {
  margin: 6px auto;
  width: 98%;
}
`