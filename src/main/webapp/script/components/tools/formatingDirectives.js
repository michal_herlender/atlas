import { html } from "../../thirdparty/lit-element/lit-element.js";
import { directive } from "../../thirdparty/lit-html/lit-html.js";
import { MetaDataObtainer } from "../../tools/metadataObtainer.js";
import "../cwms-translate/cwms-translate.js";

const dateFormatter = Intl.DateTimeFormat(MetaDataObtainer.locale,{timeZone:MetaDataObtainer.serverTimeZone, dateStyle: 'medium'});

const dateTimeFormatter = Intl.DateTimeFormat(MetaDataObtainer.locale,{timeZone:MetaDataObtainer.serverTimeZone, dateStyle: 'medium', timeStyle:'short'});

const numericDateFormatter = new Intl.DateTimeFormat(MetaDataObtainer.locale,{year:"numeric",month:"2-digit"});

export const yearMonthFormatFunction = timestamp => {
    if(!timestamp) return "";
        const parts = numericDateFormatter.formatToParts(new Date(timestamp));
        const year = parts.filter(p => p.type=="year").map(p => p.value)[0];
        const month = parts.filter(p => p.type=="month").map(p => p.value)[0];
        return `${year}-${month}`;
} 
export const dateFormatFunction = timestamp => timestamp? dateFormatter.format(new Date(timestamp)):"";

export const dateTimeFormatFunction = timestamp => timestamp? dateTimeFormatter.format(new Date(timestamp)):"";

export const dateTimeFormatDirective = directive(timestamp=>part=> {if(timestamp) part.setValue(dateTimeFormatFunction(timestamp))});

export const dateFormatDirective = directive(timestamp=>part=> {if(timestamp) part.setValue(dateFormatFunction(timestamp))});

export const dateFormatWihtAltTextDirective = directive(timestamp=>part=>{
   if(timestamp) dateFormatDirective(timestamp)(part)
   else part.setValue(html`-- <cwms-translate code="instrumenttooltip.novalue">No value</cwms-translate> --`);
});

export const precentageFormatDirecive = directive(number => part => {if(number) part.setValue( Intl.NumberFormat(MetaDataObtainer.locale,{
      style: "percent",
      minimumFractionDigits: 0,
      maximumFractionDigits: 0
    }).format(number) )});

export const currencyFormatDirective = directive((ammount,currencyCode) => part => {
    if(!ammount) return;
    part.setValue(Intl.NumberFormat("default",{style:"currency",currency:currencyCode??MetaDataObtainer.defaultCurrency}).format(ammount));
});

export const currencyFormatWithZeroDirective = directive((ammount,currencyCode) => part => {
    if(!!ammount || ammount == 0  ) 
    part.setValue(Intl.NumberFormat("default",{style:"currency",currency:currencyCode??MetaDataObtainer.defaultCurrency}).format(ammount));
});

export const currencyFormat = (ammount,currencyCode) => Intl.NumberFormat("default",{style:"currency",currency:currencyCode??
MetaDataObtainer.defaultCurrency??
"EUR"}).format(ammount);

export const calibrationIntervalTemplate = (interval,unit) =>  {
            const unitCode = interval==1?`intervalunit.${unit?.toLowerCase()}`:`intervalunit.plural${unit?.toLowerCase()}`;
            return html`${interval} <cwms-translate code="intervalunit.pluralmonth">${unitCode}</cwms-translate>`
} ;

export const currencySymbolFunction = currencyCode => {
    if(!currencyCode) return "";
    return Intl.NumberFormat("default",{style:"currency",currency:currencyCode, currencyDisplay:"symbol"})
                    .formatToParts(0).filter(p => p.type =="currency")[0]?.value;
}

export const currencySymbolDirective = directive((currencyCode) => part => {
    if(!currencyCode) return;
    const currencySymbol = Intl.NumberFormat("default",{style:"currency",currency:currencyCode})
                    .formatToParts(0).filter(p => p.type =="currency")[0]?.value;
    part.setValue(currencySymbol);
});

export const currencyNameDirective = directive((currencyCode) => part => {
    if(!currencyCode) return;
    const currencySymbol = Intl.NumberFormat("default",{style:"currency",currency:currencyCode, currencyDisplay:"name"})
                    .formatToParts(0).filter(p => p.type =="currency")[0]?.value;
    part.setValue(currencySymbol);
});


export const countryNameDirective = directive((countryCode) => part => {
    const countryName = new Intl.DisplayNames(MetaDataObtainer.locale,{type:"region"});
    part.setValue(countryName.of(countryCode));
});


export const percentFormatDirective = directive(number => part => {
        const option = {
          style: 'percent',
          minimumFractionDigits: 0,
          maximumFractionDigits: 0
       };
       const nf =  new Intl.NumberFormat(MetaDataObtainer.locale,option);
       part.setValue(nf.format(number));
})


export const yesNoDirective = directive(value => part => {
        const code=value?"yes":"no";
        CwmsTranslate.messagePromise(code,code).then(v => {
            part.setValue(v);
            part.commit();
        });
});