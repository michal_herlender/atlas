import { html }  from "../../thirdparty/lit-element/lit-element.js ";
import { MetaDataObtainer } from "../../tools/metadataObtainer.js";
export const stylesMixin = html`
  <link rel="stylesheet" href="styles/structure/elements/table.css" />
  <link rel="stylesheet" href="styles/theme/theme.css" />
  ${MetaDataObtainer.isDevelopment? html`<link rel="stylesheet" href="styles/theme/test-theme.css" />`:html``}
  <link rel="stylesheet" href="styles/text/text.css" />
  <link rel="stylesheet" href="styles/structure/structure.css" />
`;
