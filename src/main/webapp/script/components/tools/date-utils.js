import { MetaDataObtainer } from "../../tools/metadataObtainer.js";

export class DateUtils {

  static dateFormat = Intl.DateTimeFormat("en-CA",{dateStyle:"short"});

  static dateTimeConverter = {
    fromAttribute: (value) => (value ? new Date(value) :undefined),
    toAttribute: (value) => DateUtils.localDateTimeFormatFunction(value) ,
  };

  static dateToISODateString(date) {
    return  date?this.dateFormat.format(date):undefined ;
  }

  static get timeZone(){
    return this.dateFormat.resolvedOptions.timeZone;
  }

  static localDateTimeFormatFunction(date){
    const format = Intl.DateTimeFormat("default",{year:"numeric",month:"2-digit",day:"2-digit",hour:"2-digit",minute:"2-digit"});
    const formatted = format.formatToParts(date).reduce((acc,o) => ({...acc,[o.type]:o.value}),{});
    return `${formatted.year}-${formatted.month}-${formatted.day}T${formatted.hour}:${formatted.minute}`;
  }
}
