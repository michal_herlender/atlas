import CwmsTranslate from '../cwms-translate/cwms-translate.js'

const _translationConfig = {
	"info":         CwmsTranslate.getMessage('datatables.info',"Info"),	
	"lengthMenu":	CwmsTranslate.getMessage('datatables.lengthMenu',"Show _MENU_ entries"),	
	"search": 		CwmsTranslate.getMessage('datatables.search',"search"),
	"paginate": {
        "first":      CwmsTranslate.getMessage('datatables.first',"first"),
        "last":       CwmsTranslate.getMessage('datatables.last',"last"),
        "next":       CwmsTranslate.getMessage('datatables.next',"next"),
        "previous":   CwmsTranslate.getMessage('datatables.previous',"previous")
    },
};

export function translationConfig(){
	return _translationConfig;
}