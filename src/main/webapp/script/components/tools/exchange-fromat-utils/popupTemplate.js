import { html } from "../../../thirdparty/lit-element/lit-element.js";
import { dateFormatDirective, precentageFormatDirecive } from "../formatingDirectives.js";

export const  popUpContentTemplate = context => html`

<table class="duplicates" summary="The potential duplicates for instrument">
				<thead>
					<tr>
						<th class="bar" scope="col"><cwms-translate code="addnewitemstojob.barcode">Barcode</cwms-translate></th>
						<th class="sub" scope="col"><cwms-translate code="addnewitemstojob.subdivision">Subdivision</cwms-translate></th>
						<th class="inst" scope="col"><cwms-translate code="addnewitemstojob.instrument">Instrument</cwms-translate></th>
						<th class="serial" scope="col"><cwms-translate code="addnewitemstojob.serialNo">Serial No</cwms-translate></th>
						<th class="plant" scope="col"><cwms-translate code="addnewitemstojob.plantNo">Plant No</cwms-translate></th>
						<th class="barcode" scope="col"><cwms-translate code="addnewitemstojob.formerBarcode">Former barcode</cwms-translate></th>
						<th class="lastJob" scope="col"><cwms-translate code="addnewitemstojob.lastJob">Last job in date</cwms-translate></th>
						<th class="lastCal" scope="col"><cwms-translate code="addnewitemstojob.lastCal">Last calibration date</cwms-translate></th>
						<th class="nextCal" scope="col"><cwms-translate code="addnewitemstojob.nexCal">Next calibration date</cwms-translate></th>
						<th class="similarity" scope="col"><cwms-translate code="addnewitemstojob.similarity">Similarity</cwms-translate></th>
						<th class="accept" scope="col"><cwms-translate code="addnewitemstojob.accept">Accept that suggestion</cwms-translate></th>
					</tr>
				</thead>
				<tbody>
                    ${context.items.map(instrumentTemplate(context))
                    }
				</tbody>
            </table>`
            

const instrumentTemplate = context => inst => html`
    <tr>
       <td>${inst.plantId}</td>             
       <td>${inst.subdivName}</td>             
       <td>${inst.description}</td>             
       <td>${inst.serialNo}</td>             
       <td>${inst.plantNo}</td>             
       <td>${inst.formerBarcode}</td>             
       <td>${dateFormatDirective(inst.lastJobInDate)}</td>             
       <td>${dateFormatDirective(inst.lastCalibrationDate)}</td>             
       <td>${dateFormatDirective(inst.nextCalibrationDate)}</td>             
       <td>${precentageFormatDirecive(inst.score)}</td>             
       <td><button @click=${_ => context.selectInstrument(inst)}><cwms-translate code="accept">Accept</cwms-translate></button></td>             

                </tr>
`; 