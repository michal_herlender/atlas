import {  html } from "../../../thirdparty/lit-element/lit-element.js";

export class CwmsFinalSynthesisUtils {
  static generateInputName(name, idx) {
    return `identifiedJobItemsFinalSynthDTO[${idx}].${name}`;
  }

  static generateInputnonIdentifiedName(name, idx) {
    return `nonIdentifiedJobItemsFinalSynthDTO[${idx}].${name}`;
  }

  static  headerBasedInputsGenerator = (tableHeader,nameGenerator) => (item, idx)  => {
    return Object.values(tableHeader).map((v) => {
      const descriptor = ExchangeFormatFieldMapping[v.name];
      if (!descriptor) console.warn("unable to match", v);
      return this.hiddenInput(
        nameGenerator(
          descriptor?.formField ?? v,
          idx
        ),
        item[descriptor?.dtoField ?? ""] ?? ""
      );
    });
  }

    static  hiddenInput(name, value) {
    return html` <input type="hidden" name=${name} value=${value} /> `;
  }
}

class ExchangeFormatFieldDescriptor {
  constructor(formField, dtoField) {
    this.formField = formField;
    this.dtoField = dtoField ?? formField;
  }
}

export const ExchangeFormatFieldMapping = {
  ID_TRESCAL: new ExchangeFormatFieldDescriptor("idTrescal", "idTrescal"),
  PLANT_NO: new ExchangeFormatFieldDescriptor("plantNo"),
  CUSTOMER_DESCRIPTION: new ExchangeFormatFieldDescriptor(
    "customerDescription"
  ),
  BRAND: new ExchangeFormatFieldDescriptor("brand"),
  MODEL: new ExchangeFormatFieldDescriptor("model"),
  SERIAL_NUMBER: new ExchangeFormatFieldDescriptor("serialNo"),
  EXPECTED_SERVICE: new ExchangeFormatFieldDescriptor("expectedService"),
  SERVICETYPE_ID: new ExchangeFormatFieldDescriptor("servicetypeid"),
  TECHNICAL_REQUIREMENTS: new ExchangeFormatFieldDescriptor(
    "technicalRequirements"
  ),
  CUSTOMER_COMMENTS: new ExchangeFormatFieldDescriptor("customerComments"),
  DELIVERY_DATE: new ExchangeFormatFieldDescriptor("deliveryDate"),
  URGENCY: new ExchangeFormatFieldDescriptor("urgency"),
  UNDER_GUARANTEE: new ExchangeFormatFieldDescriptor("underGuarantee"),
  PO_NUMBER: new ExchangeFormatFieldDescriptor("poNumber"),
  LOCATION: new ExchangeFormatFieldDescriptor("location"),
  CLIENT_REF: new ExchangeFormatFieldDescriptor("jobitemClientRef"),
  CAL_FREQUENCY: new ExchangeFormatFieldDescriptor("calibrationFrequency"),
  INTERVAL_UNIT: new ExchangeFormatFieldDescriptor("intervalUnit"),
};



export class HeaderDescriptor {
  constructor(key, defaultMessage = "", isErp = false) {
    this.key = key;
    this.isErp = isErp;
    this.message = defaultMessage;
  }
}
