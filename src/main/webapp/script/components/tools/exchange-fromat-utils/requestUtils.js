import { MetaDataObtainer } from "../../../tools/metadataObtainer.js";

export async function queryInstruments(coid,plantNo="",serialNo=""){
    const url = new URL("instrument/checkDuplicatesForInstrument.json",MetaDataObtainer.base);
    url.searchParams.append("coid",coid);
    url.searchParams.append("plantNo",plantNo);
    url.searchParams.append("serialNo",serialNo);
    const res = await fetch(url, {
        method: "GET",
        credentials: "same-origin",
        headers: {
            "Content-Type": "application/json",
        },
    });
    const json =  await (res.ok ? res.json() : Promise.reject(res.statusText));
    return await (json.isValid? Promise.resolve(json.result):Promise.reject(json.result));
    ;
}


export async function queryInstrumentsFromCompanyGroup(groupId, item){
    const url = new URL("instrument/lookForInstrumentInCompanyGroup.json",MetaDataObtainer.base);
    url.searchParams.append("groupId", groupId);
    url.searchParams.append("plantNo", item?.plantNo ?? "");
    url.searchParams.append("serialNo", item?.serialNo ?? "");
    url.searchParams.append("plantId", item?.plantId ?? "");

    const res = await fetch(url, {
        method: "GET",
        credentials: "same-origin",
        headers: {
            "Content-Type": "application/json",
        },
    });
    const json =  await (res.ok ? res.json() : Promise.reject(res.statusText));
    return json;
    ;
}