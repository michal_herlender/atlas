import { html, css  } from '../../../thirdparty/lit-element/lit-element.js'
import { repeat } from '../../../thirdparty/lit-html/directives/repeat.js'


export const renderTableWithHeadersAndItemRenderer = (headerRenderer,itemRenderer) => context  => {
    const {tableHeader:header,items} = context;
    const headerEntries = Object.entries(header);
    const headerSize = headerEntries.length;
    return  html`
    <table>
        <thead>
            <tr>
                ${headerEntries.map(headerRenderer(context))}
            </tr>
        </thead>
        <tbody>
            ${Object.entries(items).sort((e1,e2) => e1[0].localeCompare(e2[0])).map(entry => html`
            <tr>
                <td class="title" colspan=${headerSize}>
                    <input type="checkbox" id="parent_0" @change=${e => context.checkGroup(entry[0],e.currentTarget.checked)}>
                        <span>
                          <cwms-translate code="finalsynthesis.table.withsubfamily">Sub Family</cwms-translate>: ${entry[0]} (${entry[1].length}/${entry[1][0].subFamilySize})</span>
        </td></tr>
        ${repeat(entry[1],item => item.index,rowRenderer(context)(headerEntries,itemRenderer))}     
        `)}

        </tbody>
    </table>
`};


const rowRenderer = context => (header,itemRenderer) => (item,idx) => html`<tr>
            ${header.map(h => html`<td>${itemRenderer(context)(h[0],item,idx)}</td>`)}
            </tr>`
;


export const tableStyling = css`
    table {
        color:black;
        width:100%;
        font-size:var(--cwms-main-table-font-size);
        font-family:var(--cwms-main-font-family);
        background-color:var(--cwms-main-light-font-color,#dadada);
    }

    table th, .title, table thead td, table tfoot td {
        background-color: var(--cwms-table-main-background, yellow);
    }

    table th {
        font-size:8pt;
    }

    table td span {
        line-height: var(--cwms-line-height);
    }

    .title {
        text-align:left;
        font-weight:bold;
    }

    th.erp {
        background-color: var(--cwms-table-secondary-backgound, green);
    }

    th.file {
        background-color: var(--cwms-table-strong-backgound, green);
    }

    table, th, td {
        border: solid 1px #999;
        border-collapse: collapse;
        padding:4px;
        text-align: left;
        vertical-align:midle;
    }

    td {

        text-align:center;
    }
`