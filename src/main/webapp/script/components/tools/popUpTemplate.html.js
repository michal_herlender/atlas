import {html} from "../../thirdparty/lit-element/lit-element.js ";
import {until} from "../../thirdparty/lit-html/directives/until.js";

export const popUpTemplate = context => popUpTemplateWithOptionalStyles(context)(true);

export const popUpTemplateWithoutStyles = context => popUpTemplateWithOptionalStyles(context)(false);

export const popUpTemplateWithOptionalStyles = context => withStyles => html`
    ${withStyles ? html`
	<link rel="stylesheet" href="styles/theme/theme.css">
	<link rel="stylesheet" href="styles/structure/elements/popUpTemplate.css">
` : html``}
	<div class="overlay-container"  >
	    <span class="helper"></span>
	    <div class="overlay-content" >
	        <div class="popupCloseButton" @click="${context.hide}" >&times;</div>
	        
			<div class="scrollable-content">
	        <div class="modal-header">
	            ${context.title instanceof Promise ? until(context.title, html``) : context.title || ''}
	        </div>
	        
	        <div class="modal-body">
                     ${context.overlayContent || ''}
	        </div>
	    </div>
	    </div>
	</div>

` 