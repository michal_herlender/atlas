import {css} from "../../../../thirdparty/lit-element/lit-element.js";

export const styling = (context) => css`

.hidden {
    display:none;
}

.filters {
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    grid-template-rows: repeat(4,1fr);
    flex-direction: column;
    padding-top: 5px;
}

.colspan {
    grid-column-start:1;
    grid-column-end:span 3;
}

.filters-row {
    display:flex;
    gap:10px;
}

tr p {
    padding-left: initial;
}

tfoot {
    font-weight: bold;
}
`;