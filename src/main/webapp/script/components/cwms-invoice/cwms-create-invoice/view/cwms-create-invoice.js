import { LitElement, html } from "../../../../thirdparty/lit-element/lit-element.js";
import { storeAddressId, storeExpenseItems, storeFilteredExpenseItems, storeFilteredItems, storeInvoiceType, storeItems, updateExpenseItem, updateExpenseItems, updateItem, updateItems } from "../store/actions.js";
import store from "../store/store.js";
import { styling } from "./cwms-create-invoice.css.js";
import { template } from "./cwms-create-invoice.html.js";
import { repeat } from "../../../../thirdparty/lit-html/directives/repeat.js";
import {
    filterByBPONumber,
    filterByDateFrom,
    filterByDateTo,
    filterByDeliveryNo,
    filterByJob,
    filterByJobClientRef,
    filterByJobItemClientRef,
    filterByJobServiceClientRef,
    filterByJobStatus,
    filterByPONumbers,
    filterByPriceBox,
    filterBySubdiv,
    filterValueAll,
    filterValueNone
} from "../../common/filterConsts.js";

class CwmsCreateInvoiceInner extends LitElement {

    static get properties() {
        return {
            invoiceNumber: {type:Number},
            invoice: {type:Object},
            allItems:{type:Array},
            allExpenseItems:{type:Array},
            items:{type:Array},
            expenseItems:{type:Array},
			listJobSubdivs:{type:Object},
            /*priceBox:{type:Object},*/
			listJobNos:{type:Array},
			listJobStatus:{type:Array},
			listJobPOs:{type:Array},
			listJobBPOs:{type:Array},
			listJobClientRefs:{type:Array},
			listJobItemClientRefs:{type:Array},
			listExpenseItemClientRefs:{type:Array},
            invoiceTypes:{type:Object},
            periodicInvoices:{type:Array},
            notInvoicedReasons:{type:Array},
            filterOptions:{type:Object},
            showSources:{type:Boolean},
            itemsTotalPrice:{type:Number},
            servicesTotalPrice:{type:Number},
            totalPrice:{type:Number}
        };
    }


    constructor(){
        super();
        this.showSources = false;
        this.allExpenseItems = [];
        this.allItems = [];
        this.filterOptions = {};
        this.store = store;
        store.subscribe(() => {
            const {items=[],expenseItems=[]} = store.getState();
            if(this.allItems != items || this.allExpenseItems != expenseItems)
            {
                this.allItems = items ?? [];
                this.allExpenseItems = expenseItems ?? [];
                this.applyFilters();
            }
        })
    }

    set items(newItems){
        const oldItems = this.items;
        this._items = newItems;
        this.itemsTotalPrice = this.itemsFinalCost;
        this.requestUpdate("items",oldItems);
    }

    get items(){
        return this._items ?? [];
    }

    set expenseItems(newItems){
        const oldItems = this.expenseItems;
        this._expenseItems = newItems ??[];
        this.servicesTotalPrice = this.totalPriceJobServices;
        this.requestUpdate("expenseItems",oldItems);
    }

    get expenseItems(){
        return this._expenseItems ?? [];
    }

    set itemsTotalPrice(newPrice){
        const oldPrice = this.itemsTotalPrice;
        this._itemsTotalPrice = newPrice;
        this.totalPrice = newPrice + this.servicesTotalPrice;
        this.requestUpdate("itemsTotalPrice",oldPrice);
    }

    get itemsTotalPrice(){
        return this._itemsTotalPrice;
    }

    set servicesTotalPrice(newPrice){
        const oldPrice = this.itemsTotalPrice;
        this._servicesTotalPrice = newPrice;
        this.totalPrice = newPrice + this.itemsTotalPrice;
        this.requestUpdate("servicesTotalPrice",oldPrice);
    }

    get servicesTotalPrice(){
        return this._servicesTotalPrice;
    }


    connectedCallback(){
        super.connectedCallback();
        this.items =[...this.allItems];
        this.expenseItems =[...this.allExpenseItems];
        this.invoice.companyAddresses.forEach(address => address.selected = address.addressId == this.invoice.addressId);
    }

    static get styles(){
        return styling({})
    }

    notInvoiced(item,name,value){
        const newItem = {...item, notInvoiced:{...item.notInvoiced,[name]:value}};
        if(item.expenseItemId)
            updateExpenseItem(newItem);
        updateItem(newItem);
    }

    changeType(type) {
        storeInvoiceType(type);
    }

    changeAddress(addressId) {
        storeAddressId(addressId);
    }

    reasonByName(name){
        return this.notInvoicedReasons.filter(r => r.name == name)[0];
    }

    get itemsFinalCost() {
        return (this.items ?? []).filter(i => i.include ?? true).map(i => i.finalCost).reduce((a,b) => a+b,0);
    }

    get totalPriceJobServices(){
        return (this.expenseItems ??[]).filter(i => i.include ?? true).map(i => i.totalCost)
        .reduce((a,b) => a+b,0);
    }

    filter(fieldName){
        return e => {
            const value = e.currentTarget.value;
            if([filterByPriceBox].includes(fieldName))
                this.filterOptions = {...this.filterOptions,[fieldName]:e.currentTarget.checked}
            else if([filterByDateFrom,filterByDateTo].includes(fieldName))
                this.filterOptions = {...this.filterOptions,[fieldName]:e.currentTarget.valueAsDate}
            else this.filterOptions = {...this.filterOptions,[fieldName]:value}
        }
    }

    update(changedProperties){
        if(changedProperties.has("filterOptions"))
            this.applyFilters()
        super.update(changedProperties);
    }

    filterItem(item){
        return [filterByPriceBox, filterBySubdiv, filterByJob, filterByJobStatus, filterByDeliveryNo, filterByJobClientRef, filterByJobItemClientRef,filterByJobServiceClientRef, filterByPONumbers, filterByBPONumber, filterByDateTo, filterByDateFrom].every(fn => this.filterByEnum(fn,item))
    }

    applyFilters() {
        queueMicrotask(
        () => {
        this.items = this.allItems.filter(this.filterItem.bind(this));
        this.expenseItems = this.allExpenseItems.filter(this.filterItem.bind(this));
        storeFilteredItems(this.items);
        storeFilteredExpenseItems(this.expenseItems);
        });
        queueMicrotask(() => this.requestUpdate());
    }

    toggleInclude(item){
        storeItems(this.allItems
            .map(i => i.id == item.id?{...item,include:!item.include}:i))
    }

    toggleExpenseInclude(item){
        storeExpenseItems(this.allExpenseItems
            .map(i => i.expenseItemId == item.expenseItemId?{...item,include:!item.include}:i))
    }

    filterByEnum(fieldName,item){
        const filterValue = this.filterOptions[fieldName] ?? "ALL";

        if(filterValue == filterValueAll) return true;

        if(fieldName == filterByPriceBox)
            return (!item.totalCost && !item.finalCost) === filterValue;

        if(filterValue == filterValueNone)
            return !item[fieldName] || item[fieldName]?.length == 0 || item[fieldName] == {} 

        if(fieldName == filterByDeliveryNo)
            return item.deliveryItems.map(di => di.deliveryNo).includes(filterValue);

        if(fieldName==filterByDateFrom)
            return new Date(item.date) >= filterValue;

        if(fieldName==filterByDateTo)
            return new Date(item.date) <= filterValue;

        if(item[fieldName] instanceof Array)
            return item[fieldName].includes(filterValue);

        return item[fieldName] == filterValue;
    }

    toggleAllItems(e) {
        const include = e.currentTarget.checked;
        storeItems(this.allItems.map(i => ({...i,include})));
    }


    copyDown(item,idx){
        const newItemFunction = it =>  ({...it, notInvoiced:{...item.notInvoiced}});
        return _ => {
            if (item.expenseItemId)
                updateExpenseItems(this.expenseItems.filter((ei,iidx)=> !ei.include && iidx > idx)
                    .map(newItemFunction)
                );
            else 
              updateItems(this.items.filter((it,iidx) => !it.include && iidx > idx)
                .map(newItemFunction));
        };
    }
    
    toggleShowSources(){
        this.showSources = !this.showSources;
    }
    
    render(){
       return template(this);
    }

}


customElements.define("cwms-create-invoice-inner",CwmsCreateInvoiceInner);


class CwmsCreateInvoiceWrapper extends LitElement {

    static get properties() {
        return {
            invoiceNumber: {type:Number},
            invoice: {type:Object},
			listJobSubdivs:{type:Object},
            /*priceBox:{type:Object},*/
			listJobNos:{type:Array},
			listJobStatus:{type:Array},
			listJobPOs:{type:Array},
			listJobBPOs:{type:Array},
			listJobClientRefs:{type:Array},
			listJobItemClientRefs:{type:Array},
			listExpenseItemClientRefs:{type:Array},
            invoiceTypes:{type:Object},
            periodicInvoices:{type:Array},
            notInvoicedReasons:{type:Array},
            filteredItems:{type:Array},
            filteredExpenseItems:{type:Array},
            items:{type:Array},
            expenseItems:{type:Array},
            addressId:{type:Number},
            invoiceType:{type:Number},
        };
    }


    constructor(){
        super();
        this.filteredExpenseItems =[];
        this.filteredItems =[];
        this.expenseItems =[];
        this.items =[];
        store.subscribe(() => {
           const {items,expenseItems,addressId,invoiceType,filteredItems,filteredExpenseItems} = store.getState();
           this.items = items;
           this.expenseItems = expenseItems;
           this.filteredItems = filteredItems ?? [];
           this.filteredExpenseItems = filteredExpenseItems ?? [];
           this.invoiceType = invoiceType;
           this.addressId = addressId;

        }
    );
    }

    createRenderRoot(){
        return this;
    }

    connectedCallback(){
        super.connectedCallback();
        storeExpenseItems(this.invoice.expenseItems);
        storeItems(this.invoice.items);
        storeAddressId(this.invoice.addressId);
        storeInvoiceType(this.invoiceTypes[0].key);
    }

    render() {

        const filteredItemsIds = this.filteredItems.map(i => i.id);
        const filteredExpenseItemsIds = this.filteredExpenseItems.map(i => i.expenseItemId);
        return html`
        <cwms-create-invoice-inner 
            .invoiceNumber=${this.invoiceNumber}
            .invoice=${this.invoice}
            .listJobSubdivs=${this.listJobSubdivs}
            .listJobNos=${this.listJobNos}
            .listJobStatus=${this.listJobStatus}
            .listJobPOs=${this.listJobPOs}
            .listJobBPOs=${this.listJobBPOs}
            .listJobClientRefs=${this.listJobClientRefs}
            .listJobItemClientRefs=${this.listJobItemClientRefs}
            .listExpenseItemClientRefs=${this.listExpenseItemClientRefs}
            .invoiceTypes=${this.invoiceTypes}
            .periodicInvoices=${this.periodicInvoices}
            .notInvoicedReasons=${this.notInvoicedReasons}

        ></cwms-create-invoice-inner>
        <input type="hidden" name=${`proposedInvoice[${this.invoiceNumber-1}].addressId`} value=${this.addressId} />
        <input type="hidden" name=${`proposedInvoice[${this.invoiceNumber-1}].typeId`} value=${this.invoiceType} />
        ${repeat(this.items.map(i => filteredItemsIds.includes(i.id)? i:{...i,include:false} ), 
            i => i.id
        ,(i,idx) => html`
            <input type="hidden" name=${`proposedInvoice[${this.invoiceNumber-1}].items[${idx}].include`} value=${i.include} />
            ${!i.include && i.notInvoiced.selected ? html`
            <input type="hidden" name=${`proposedInvoice[${this.invoiceNumber-1}].items[${idx}].notInvoiced.selected`} value=${i.notInvoiced.selected} />
            <input type="hidden" name=${`proposedInvoice[${this.invoiceNumber-1}].items[${idx}].notInvoiced.reason`} value=${i.notInvoiced.reason?.name} />
            <input type="hidden" name=${`proposedInvoice[${this.invoiceNumber-1}].items[${idx}].notInvoiced.periodicInvoiceId`} value=${i.notInvoiced.periodicInvoiceId ?? 0} />
            <input type="hidden" name=${`proposedInvoice[${this.invoiceNumber-1}].items[${idx}].notInvoiced.comments`} value=${i.notInvoiced.comments} />
            `:html`` }
        `)}
        ${repeat(this.expenseItems.map(i => filteredExpenseItemsIds.includes(i.expenseItemId)?
                i:{...i,include:false}
            ),i=>i.expenseItemId, (i,idx) => html`
            <input type="hidden" name=${`proposedInvoice[${this.invoiceNumber-1}].expenseItems[${idx}].include`} value=${i.include} />
            ${!i.include && i.notInvoiced.selected ? html`
            <input type="hidden" name=${`proposedInvoice[${this.invoiceNumber-1}].expenseItems[${idx}].notInvoiced.selected`} value=${i.notInvoiced.selected} />
            <input type="hidden" name=${`proposedInvoice[${this.invoiceNumber-1}].expenseItems[${idx}].notInvoiced.reason`} value=${i.notInvoiced.reason?.name} />
            <input type="hidden" name=${`proposedInvoice[${this.invoiceNumber-1}].expenseItems[${idx}].notInvoiced.periodicInvoiceId`} value=${i.notInvoiced.periodicInvoiceId ?? 0} />
            <input type="hidden" name=${`proposedInvoice[${this.invoiceNumber-1}].expenseItems[${idx}].notInvoiced.comments`} value=${i.notInvoiced.comments} />
            `:html`` }
        `)}
        `;
    }
}

customElements.define("cwms-create-invoice",CwmsCreateInvoiceWrapper);