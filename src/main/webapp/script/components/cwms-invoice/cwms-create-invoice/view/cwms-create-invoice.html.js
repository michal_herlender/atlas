import { html } from "../../../../thirdparty/lit-element/lit-element.js";
import { until } from "../../../../thirdparty/lit-html/directives/until.js";
import { repeat } from "../../../../thirdparty/lit-html/directives/repeat.js";
import { ifDefined } from "../../../../thirdparty/lit-html/directives/if-defined.js";
import {stylesMixin} from "../../../tools/applicationStylesMixinTemplate.js";
import CwmsTranslate from "../../../cwms-translate/cwms-translate.js";
import "../../../cwms-tooltips/cwms-company-link-and-tooltip/cwms-company-link-and-tooltip.js";
import "../../../cwms-tooltips/cwms-job-link-and-tooltip/cwms-job-link-and-tooltip.js";
import "../../../cwms-tooltips/cwms-jobitem-link-and-tooltip/cwms-jobitem-link-and-tooltip.js";
import {  currencyFormatDirective, currencyFormatWithZeroDirective, dateFormatDirective } from "../../../tools/formatingDirectives.js";
import { MetaDataObtainer } from "../../../../tools/metadataObtainer.js";
import {  viewjobcostingLinkGenerator, viewQuotationLinkGenerator } from "../../../../tools/UrlGenerator.js";
import {
    filterByBPONumber,
    filterByDateFrom,
    filterByDateTo,
    filterByDeliveryNo,
    filterByJob,
    filterByJobClientRef,
    filterByJobItemClientRef,
    filterByJobServiceClientRef,
    filterByJobStatus,
    filterByPONumbers,
    filterByPriceBox,
    filterBySubdiv,
    filterValueAll,
    filterValueNone
} from "../../common/filterConsts.js";
import { secureDeliveryLinkTemplate } from "../../common/templates.html.js";
import { cwmsTranslateDirective } from "../../../cwms-translate/cwms-translate-directive.js";
import "../../../cwms-content-visibility/cwms-content-visibility.js";


export const template = (context) => html`
${stylesMixin}
<cwms-content-visibility>
    <fieldset>
    <legend><cwms-translate code="invoice.invoice">Invoice</cwms-translate>: ${context?.invoiceNumber}</legend>
    <ol>
        <li>
            <label><cwms-translate code="company">Company</cwms-translate>:</label>
            <cwms-company-link-and-tooltip companyId=${context?.invoice?.company?.coid}
            companyName=${context?.invoice?.company?.coname}
                ></cwms-company-link-and-tooltip>
        </li>
        <li>
            <label><cwms-translate code="jobcost.costingtype" >Pricing Type</cwms-translate>:</label>
            ${context?.invoice?.pricingType?.localizedName}        </li>
        <li>
            <label><cwms-translate code="viewjob.invoicetype" >Invoice Type</cwms-translate>:</label>
            <select name="invoiceType" @change=${e => context.changeType(e.currentTarget.value)}>
                ${repeat(context?.invoiceTypes??[],t => html`<option value=${t.key}>${t.value}</option>`)}
            </select>
        </li
        <li>
            <label><cwms-translate code="address">Address</cwms-translate>:</label>
            <select name="addressId" @change=${e => context.changeAddress(e.currentTarget.value)}>
            ${repeat(context?.invoice?.companyAddresses??[],addressOptionsTemplate(context))}
            </select>
        </li>
        <li>
            ${invoiceItemsFiltersTemplate(context)}
        </li>
        <li>
            ${invoiceItemsTableTemplate(context)}
        </li>
    </ol>
    </fieldset>
</cwms-content-visibility>
`;

const addressOptionsTemplate = context => address => html`
    <option value=${address.addressId} ?selected=${address.selected}>
            ${address.subdivName}, ${address.addressLine}, ${address.town} [${address.addressTypes.join(", ")}]
    </option>
`


const invoiceItemsFiltersTemplate = context => html`
<div class="filters">
   <div class="colspan"> ${filterBySubdivTemplate(context)}</div>
    <div class="colspan"> ${filterByPriceBoxTemplate(context)}</div>
       ${filterByJobNoTemplate(context)}
       ${filterByJobStatusTemplate(context)}
       ${filterByDelNoTemplate(context)}
       ${filterByJobItemClientRefTemplate(context)}
       ${filterByJobClientRefTemplate(context)}
       ${filterByJobServiceClientRefTemplate(context)}
       ${filterByPOTemplate(context)}
       ${filterByBPOTemplate(context)}
       <div></div>
       ${filterByDateFromTemplate(context)}
       ${filterByDateToTemplate(context)}
</div>
`;

const filterBySubdivTemplate = context => html`
  <label class="widthLabel"><cwms-translate code="periodiclinking.filterbysubdiv" >Filter by Subdivision</cwms-translate></label>
  <select class="subdivFilter filter" @change=${context.filter(filterBySubdiv)} >
      <option value="ALL"><cwms-translate code="all" >ALL</cwms-translate></option>
      ${repeat(Object.entries(context.listJobSubdivs??{})??[],([k,v]) => 
          html`
          <option value=${v}>${k}</option>
          `)}
  </select>
`;

const filterByPriceBoxTemplate = context => html`
  <label class="widthLabel"><cwms-translate code="periodiclinking.filterbypricebox" >Filter by Price</cwms-translate></label>
  <input type="checkbox" name="" id="" @change=${context.filter(filterByPriceBox)} >
  </select>
`;

const filterByJobNoTemplate = context => html`<div>
    ${filterByListTemplate(context.filter(filterByJob))("periodiclinking.filterbyjob","Filter by Job")(context.listJobNos)}
    </div>
`;


const filterByJobStatusTemplate = context => html`<div>
    ${filterByListTemplate(context.filter(filterByJobStatus))("periodiclinking.filterbyjobstatus","Filter by Job Status")(context.listJobStatus)}
</div>
`; 


const filterByDelNoTemplate = context => html`
<div>
    ${filterByListTemplate(context.filter(filterByDeliveryNo))("periodiclinking.filterbydelno","Filter by Delivery No")(
[...new Set(context?.invoice?.items?.flatMap(i => i.deliveryItems.filter((i)=> i.isClientDelivery))?.map(d => d.deliveryNo))]
    )}
</div>
`

const filterByJobItemClientRefTemplate = context => html`
<div>
    ${filterByListTemplate(context.filter(filterByJobItemClientRef))("periodiclinking.filterbyjobitemclientref","Filter by Job Item Client Ref")(context.listJobItemClientRefs)}
</div>
`;
const filterByJobClientRefTemplate= context => html`
<div>
    ${filterByListTemplate(context.filter(filterByJobClientRef))("periodiclinking.filterbyjobclientref","Filter by Job Client Ref")(context.listJobClientRefs)}
</div>`;

const filterByJobServiceClientRefTemplate = context => html`
<div>
    ${filterByListTemplate(context.filter(filterByJobServiceClientRef))("periodiclinking.filterbyjobserviceclientref","Filter by Job Service Client Ref")(context.listExpenseItemClientRefs)}
</div>`;

const filterByPOTemplate = context  => html`
<div>
    ${filterByListTemplate(context.filter(filterByPONumbers))("periodiclinking.filterbypo","Filter by PO",true)(context.listJobPOs)}
</div>
`
const filterByBPOTemplate = context  => html`
<div>
    ${filterByListTemplate(context.filter(filterByBPONumber))("periodiclinking.filterbybpo","Filter by BPO",true)(context.listJobBPOs)}
</div>
`
const filterByDateToTemplate = context => html`
<div>
    <label class="widthLabel"><cwms-translate code="periodiclinking.filterbydate">Filter By Date</cwms-translate> <cwms-translate code="system.to">To</cwms-translate>:</label>
    <input type='date' id='startDate' class="filter" @change=${context.filter(filterByDateTo)}   />
</div>
`;

const filterByDateFromTemplate = context => html`
<div>
    <label class="widthLabel"><cwms-translate code="periodiclinking.filterbydate">Filter By Date</cwms-translate> <cwms-translate code="system.from">From</cwms-translate>:</label>
    <input type='date' id='startDate' class="filter"  @change=${context.filter(filterByDateFrom)}  />
</div>
`

const filterByListTemplate = changeFunction => (labelCode,labelDefault,addNoneOption) => (list) => html`
<label for="deliveryNo" class="widthLabel"><cwms-translate code=${labelCode} >${labelDefault}</cwms-translate></label> 
<select class="jobStatusFilter filter" name=${labelCode} @change=${changeFunction} >
    <option value=${filterValueAll}>${cwmsTranslateDirective("all","ALL")}</option>
    ${addNoneOption?html`<option value=${filterValueNone}>${cwmsTranslateDirective("viewjob.none","None")}</option>`:html``}
    ${repeat(list??[],
    le => html`
    <option value=${le}>${le}</option>`)
}
</select>
`;

const invoiceItemsTableTemplate = (context) => html`
  <table
    class="default4"
    id="prepInvoice"
    summary=${until(CwmsTranslate.messagePromise("invoice.tableinvdetails", "This table displays all invoice details"))}
  >
    ${invoiceItemTableHead(context)}
    <tbody>
      ${repeat(context.items??[],i => i.id ,renderInvoiceItemRow(context))} 
      ${context.invoice?.items?.length ?? 0 > 0 ? invoiceItemSummaryTemplate(context) : html``}
    </tbody>
    ${context.expenseItems?.length > 0?
        [expenseItemsTableHeaderTemplat(context),
        html`<tbody>${repeat(context.expenseItems??[],expenseItemsRowTemplate(context))}</tbody>`,
        expenseItemsSummaryTemplate(context)
        ]
      : html``}
    ${itemsTableSummaryFooter(context)}
  </table>
`;


const invoiceItemTableHead = context => html`
<thead>
      <tr>
        <th colspan="11"><cwms-translate code="invoice.invoiceitems">Invoice items</cwms-translate>  (<span class="itemsSize">${context?.invoice?.items?.length ??0}</span>)</th>
      </tr>
      <tr>
        <th rowspan="2" class="include" scope="col">
          <input type="checkbox" ?checked=${true} @change=${context.toggleAllItems} />
        </th>
        <th rowspan="1" class="job" scope="col"><cwms-translate code="job">Job</cwms-translate></th>
        <th rowspan="1" class="item" scope="col"><cwms-translate code="vm_global.jobitem">Job item</cwms-translate></th>
        <th rowspan="1" class="jobstatus" scope="col"><cwms-translate code="jobstatus">Job Status</cwms-translate></th>
        <th rowspan="1" class="ponos" scope="col"><cwms-translate code="invoice.ponos">PO No (s)</cwms-translate></th>
        <th rowspan="1" colspan="2" class="rep cost2pc" scope="col"><cwms-translate code="costtype">Price Type</cwms-translate></th>
        <th rowspan="1" colspan="3" class="cal cost2pc" scope="col">
          <cwms-translate code="price">Price</cwms-translate> (${context?.invoice?.currency?.currencyCode ?? "EUR"})
        </th>
        <th rowspan="1" class="inv" scope="col"><cwms-translate code="invoice.invoiced">Invoiced</cwms-translate></th>
      </tr>
      <tr>
        <th colspan="2" class="delivery"><cwms-translate code="docs.deliverynote">Delivery Note</cwms-translate></th>
        <th class="clientrefitem"><cwms-translate code="jicontractreview.jobitemclientref">Job Item Client Ref</cwms-translate></th>
        <th class="plantno"><cwms-translate code="plantno">Plant No</cwms-translate></th>
        <th colspan="5" class="pricesource">
          <cwms-translate code="costsource">Price Source</cwms-translate>
          <span>
            (
            <a role="button" class="mainlink" @click=${context.toggleShowSources}
              ><cwms-translate code="invoice.showsources">Show sources</cwms-translate> |
              <cwms-translate code="invoice.hidesources">Hide sources</cwms-translate></a
            >
            )
          </span>
        </th>
        <th class="unsettledpo"><cwms-translate code="invoice.unsettledpos">Unsettled POs</cwms-translate></th>
      </tr>
    </thead>`

const invoiceItemSummaryTemplate = (context) => html`
  <tr>
    <td colspan="10">
      <cwms-translate code="invoice.totalamount.jobitem" >Total amount of jobitem</cwms-translate>
      <span id="costJobItemTotal" >${currencyFormatWithZeroDirective(context.itemsFinalCost,context?.invoice?.currency?.currencyCode)}</span>
    </td>
    ${context?.invoice?.pricingType =="SITE"?html`
        <td><cwms-translate code="totalcost">Total cost</cwms-translate> : <strong>${currencyFormatWithZeroDirective(context?.invoice?.totalCost,context?.invoice?.currency?.currencyCode)}</strong></td>
    `:html`<td></td>`}
  </tr>
`;

const renderInvoiceItemRow = context => (item,idx)  => html`
    <tr>
        <td rowspan="2">
            <input type="checkbox" class="includeCheck" @change=${_ => context.toggleInclude(item)}  value="true" ?checked=${item.include} ></checkbox>
        </td>
        <td><cwms-job-link-and-tooltip .withCopy=${true} jobId="${item.jobId}"
            jobNo=${item.jobNo}
            ></cwms-job-link-and-tooltip>
        </td>
        <td>
            <cwms-jobitem-link-and-tooltip  jobitemId="${item.jobItemId}"
            jobItemNo=${`${item.jobItemNo} / ${item.jobItemCount}`}
            ></cwms-jobitem-link-and-tooltip>
        </td> 
        <td>
            ${item.jobStatusName}
        </td>   
        <td>${[...item.poNumbers,...item.bpoNumbers].join(", ")}</td>
        <td colspan=2>${item.costType?.message??''}</td>
        <td colspan=3 class="cost2pc">${currencyFormatWithZeroDirective(item.finalCost,context?.invoice?.currency?.currencyCode ?? MetaDataObtainer.defaultCurrency)}</td>
        <td>
            <span class=${item.invoiceItemsCount > 0?'':'attention bold'}>
                ${item.invoiceItemsCount}&nbsp;<cwms-translate code="invoice.invoices" >Invoices</cwms-translate>
            </span>
        </td>
    </tr>
    <tr>
        <td colspan="2" >
            ${item.jobType == "SITE" ? html`
            SITE - ${dateFormatDirective(item.date)}
            `:html`${repeat(item.deliveryItems??[],secureDeliveryLinkTemplate("DELIVERY_NOTE_VIEW",true,false))}`}
        </td>
        <td>${item.jobItemClientRef}</td>
        <td>${item.plantNo}</td>
        <td colspan="5">${context.showSources?pInvoiceItemCostSourceTemplate(item):html``} </td>
        <td>
            ${item.allPOGoodsApproved?
                html`<span style="color: green"><cwms-translate code="no">No</cwms-translate></span>`:
                html`<span class="unsettledPOs" style="color: red"><cwms-translate code="yes">yes</cwms-translate></span>`
        }   
        </td>

    </tr>
    ${notInvoicedReasonsTemplate(context)(item,idx)}
`; 

const notInvoicedReasonsTemplate = context => (item,idx) => 
html`
    <tr class=${ifDefined(item.include?"hidden":undefined)}>
        <td>&nbsp;</td>
        <td colspan="10">
            <input type="checkbox" name="notinvoiced" value="true" @change=${e => context.notInvoiced(item,"selected",e.currentTarget.checked)} .checked=${item.notInvoiced.selected}></input> 
            <cwms-translate code="invoice.notinvoicedreason" >Not Invoiced Reason</cwms-translate>:
            <select @change=${e => context.notInvoiced(item,"reason",context.reasonByName(e.currentTarget.value))}  >
            ${repeat(context.notInvoicedReasons??[],r => html`<option ?selected=${r.name == item.notInvoiced.reason.name} value=${r.name}>${r.message}</option>`)}
            </select>
            <cwms-translate code="invoice.periodicinvoice" >Periodic Invoice</cwms-translate>:
            <select @change=${e => context.notInvoiced(item,"periodicInvoiceId",e.currentTarget.value)}>
                <option value="0" ><cwms-translate code="companyedit.na">NA</cwms-translate></option>
                ${repeat(context.periodicInvoices??[],pi => pi.id,pi => html`
                    <option ?selected=${pi.id == item.notInvoiced.periodicInvoiceId} value="${pi.id}">
                        ${pi.invno} - ${dateFormatDirective(pi.invoiceDate)} </option>
                `)}">
            </select>
            <cwms-translate code="comments" >Comments</cwms-translate>:
            <input name="comments" size="50" @change=${e => context.notInvoiced(item,"comments",e.currentTarget.value)} .value=${item.notInvoiced.comments ??""} />
            <button @click=${context.copyDown(item,idx)}><cwms-translate code="invoice.copydown">Copy Down</cwms-translate></button>
        </td>

    </tr>`;

const expenseItemsTableHeaderTemplat = (context) => html`
  <thead>
    <td colspan="11"><cwms-translate code="invoice.serviceitem">Serice Items</cwms-translate> (<span class="itemsSize">${context?.invoice?.expenseItems?.length}</span>)</td>
    <tr>
      <th class="include" scope="col" rowspan="2"></th>
      <th class="job" scope="col" rowspan="2"><cwms-translate code="job" >Job</cwms-translate></th>
      <th class="item" scope="col" rowspan="2"><cwms-translate code="invoice.serviceitem">Serice Items</cwms-translate> </th>
      <th class="jobstatus" scope="col"><cwms-translate code="jobstatus">Job Status</cwms-translate></th>
      <th class="ponos" scope="col" rowspan="2"><cwms-translate code="invoice.ponos">PO No (s)</cwms-translate> </th>
      <th class="comm" scope="col" colspan="2"><cwms-translate code="invoice.description">Description</cwms-translate> </th>
      <th class="singleprice" scope="col" rowspan="2"><cwms-translate code="singleprice">Single Price</cwms-translate> </th>
      <th class="quantity" scope="col" rowspan="2"><cwms-translate code="quantity" >Quantity</cwms-translate></th>
      <th class="service cost2pc" scope="col" rowspan="2"><cwms-translate code="invoice.serviceprice" >Price</cwms-translate> (${context?.invoice?.currency?.currencyCode ?? MetaDataObtainer.currencyCode})</th>
      <th class="inv" scope="col" rowspan="2"><cwms-translate code="invoice.invoiced">Invoiced</cwms-translate> </th>
    </tr>
    <tr>
      <th class="jobstatus" scope="col"><cwms-translate code="invoice.clientref.job.services">Job Service Client Ref</cwms-translate> </th>
      <th class="comm" scope="col" colspan="2"><cwms-translate code="invoice.comment">Comment</cwms-translate> </th>
    </tr>
  </thead>
`;

const expenseItemsRowTemplate = (context) => (item,idx) => html`
  <tr>
    <td rowspan="2">
      <input type="checkbox" value="true" ?checked=${item.include} @change=${_=>context.toggleExpenseInclude(item)}/>
    </td>
    <td rowspan="2"> 
        <cwms-job-link-and-tooltip jobId=${item.jobId} jobNo=${item.jobNo} ?withCopy=${true}></cwms-job-link-and-tooltip>
    </td>
    <td rowspan="2"> 
        <cwms-job-link-and-tooltip jobId=${item.jobId} jobNo=${`${item.expenseItemNo} / ${item.expenseItemsCount}`} ?withCopy=${true} targetTab="expenses-tab"></cwms-job-link-and-tooltip>
    </td>
    <td>${item.jobStatus}</td>
    <td rowspan="2">${[...item.poNumbers,...item.bpoNumbers].join(", ")}</td>
    <td colspan="2">
        ${item.model}
    </td>
    <td class="cost2pc" rowspan="2">
        ${currencyFormatDirective(item.singlePrice,item.currencyCode)}
   </td>
    <td class="cost2pc" rowspan="2">${item.quantity}</td>
    <td class="cost2pc" rowspan="2">
        ${currencyFormatDirective(item.totalCost,item.currencyCode)}
    </td>
    <td rowspan="2">
        <span class="${item.invoiceItemsCount > 0?'attention bold':''}">
            ${item.invoiceItemsCount}&nbsp;<cwms-translate code="invoice.invoices">Invoices</cwms-translate> 
        </span>
    </td>
  </tr>
  <tr>
    <td>${item.jobServiceClientRef}</td>
    <td colspan="2">${item.comment}</td>
  </tr>
    ${notInvoicedReasonsTemplate(context)(item,idx)}
`;


const expenseItemsSummaryTemplate = context => context?.invoice?.expenseItems?.length ?? 0 > 0?
 html `
<tbody>
    <tr>
        <td colspan="9"> <cwms-translate code="invoice.totalamount.jobservice" >Total amount of job services</cwms-translate>:</td>
        <td class="cost2pc">${currencyFormatWithZeroDirective(context.servicesTotalPrice,context?.invoice?.currency?.currencyCode)}
        </td>
        <td></td>
    </tr>
</tbody>
`:html``

const itemsTableSummaryFooter = (context) => html` <tfoot>
  <tr>
    <td colspan="9" >
      <cwms-translate code="invoice.totalamount">Total Ammount</cwms-translate> &nbsp;:&nbsp;&nbsp;
      </td>
    <td colspan=2>
      ${currencyFormatWithZeroDirective(context.totalPrice,context?.invoice?.currency?.currencyCode)}
    </td>
  </tr>
</tfoot>`;


const pInvoiceItemCostSourceTemplate = item =>
    item.costSource == 'CONTRACT_REVIEW'?
        item.quitationId ?
            html`[<cwms-translate code="vm_costs.cr" >CR</cwms-translate> - <a .href=${viewQuotationLinkGenerator(item.quitationId).toString()} class="mainlink">${item.quotationNo} ver ${item.quotationVer}</a>]`:
            html`<cwms-translate code="vm_costs.cr" >CR</cwms-translate>`
        : 
        item.costSource == 'JOB_COSTING'?
        html`[<cwms-translate code="vm_costs.jc" >JC</cwms-translate> - <a .href=${viewjobcostingLinkGenerator(item.jobCostingId).toString()}  class="mainlink">${item.quotationNo} ver ${item.quotationVer}</a>]`
        : html``
;

 