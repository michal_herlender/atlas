import store from "./store.js";
import {STORE_ADDRESS_ID, STORE_FILTERED_ITEMS, STORE_FILTERED_EXPENSE_ITEMS ,STORE_EXPENSE_ITEMS, STORE_INVOICE_TYPE, STORE_ITEMS} from "./actionsTypes.js";


export function storeItems(items){
    store.send({type:STORE_ITEMS,items});
}

export function storeExpenseItems(expenseItems){
    store.send({type:STORE_EXPENSE_ITEMS,expenseItems});
}

export function storeFilteredItems(items){
    store.send({type:STORE_FILTERED_ITEMS,items});
}

export function storeFilteredExpenseItems(expenseItems){
    store.send({type:STORE_FILTERED_EXPENSE_ITEMS,expenseItems});
}

export function storeAddressId(addressId){
    store.send({type:STORE_ADDRESS_ID,addressId});
}

export function storeInvoiceType(invoiceType){
    store.send({type:STORE_INVOICE_TYPE,invoiceType});
}


export function updateItems(itemsToUpdate){
    const {items} = store.getState();
    const itemsToUpdateIds = itemsToUpdate.map(i => i.id);
    storeItems(items.map(i => {
        const index = itemsToUpdateIds.indexOf(i.id);
        return index >= 0?itemsToUpdate[index]:i;
        }
        ));
}

export function updateExpenseItems(itemsToUpdate){
    const {expenseItems:items} = store.getState();
    const itemsToUpdateIds = itemsToUpdate.map(i => i.expenseItemId);
    storeExpenseItems(items.map(i => {
        const index = itemsToUpdateIds.indexOf(i.expenseItemId);
        return index >= 0?itemsToUpdate[index]:i;
        }
        ));
}

export function updateItem(item){
    const {items} = store.getState();
    storeItems(items.map(i => i.id == item.id?item:i));
}

export function updateExpenseItem(item){
    const {expenseItems} = store.getState();
    storeExpenseItems(expenseItems.map(i => i.expenseItemId == item.expenseItemId?item:i));
}
