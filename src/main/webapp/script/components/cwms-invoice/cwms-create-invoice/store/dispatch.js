import {STORE_ADDRESS_ID, STORE_EXPENSE_ITEMS, STORE_FILTERED_EXPENSE_ITEMS, STORE_FILTERED_ITEMS, STORE_INVOICE_TYPE, STORE_ITEMS} from "./actionsTypes.js"

let dispatch; export default dispatch = function(state,command){
    switch(command.type){
        case STORE_FILTERED_ITEMS: 
            return {...state, filteredItems:command.items};
        case STORE_FILTERED_EXPENSE_ITEMS:
            return {...state, filteredExpenseItems:command.expenseItems}
        case STORE_ITEMS: 
            return {...state, items:command.items};
        case STORE_EXPENSE_ITEMS:
            return {...state, expenseItems:command.expenseItems}
        case STORE_ADDRESS_ID:
            return {...state,addressId:command.addressId}
        case STORE_INVOICE_TYPE:
            return {...state,invoiceType:command.invoiceType}
        default: return state;
    }
}