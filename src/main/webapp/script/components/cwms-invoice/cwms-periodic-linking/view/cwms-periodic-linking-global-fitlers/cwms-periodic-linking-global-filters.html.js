import { html } from "../../../../../thirdparty/lit-element/lit-element.js";
import "../../../../cwms-tooltips/cwms-company-link-and-tooltip/cwms-company-link-and-tooltip.js";
import { filterByBusinessSubDiv, filterByMonthYear, filterValueAll } from "../../../common/filterConsts.js";


export const template = context => html`
<fieldset>
			<legend>
				<cwms-translate code="invoice.periodicinvoice.title" >Periodic Invoice Linking</cwms-translate>
			</legend>
			<ol>
				<li>
					<label class="bold width30">
						<cwms-translate code="invoice.invoicepreparationfor" >Invoice preparation for</cwms-translate>:
					</label>
					<span>
                        ${context.companyWide?html`
								<cwms-translate code="businesscompany" >Business Company</cwms-translate>
                        `:html`
								<cwms-translate code="businesssubdivision" > Business Subdivision</cwms-translate>
                        `}
					</span>
				</li>
				<li>
					<label class="bold width30">
						<cwms-translate code="businesscompany" >Business Company</cwms-translate>:
					</label>
					<span>${context.allocatedCompanyName ?? ""}</span>
				</li>
				<li>
					<label class="bold width30">
						<cwms-translate code="businesssubdivision" > Business Subdivision</cwms-translate>:
					</label>
					<span>
                        ${context.companyWide? html`
                        <cwms-translate code="all" >All</cwms-translate>
                        `:html`${context.allocatedSubdivName}`}
					</span>
				</li>
				<li>
					<label class="bold width30"><cwms-translate code="clientcompany" >Client Company</cwms-translate>:</label>
                    <cwms-company-link-and-tooltip companyId=${context.clientCompanyId} companyName=${context.clientCompanyName} ?withCopy=${true}></cwms-company-link-and-tooltip>
				</li>
				<li>
					<label class="bold width30"><cwms-translate code="periodiclinking.linktoperiodicinvoice" >Link to Periodic Invoice</cwms-translate>:*</label>
					<select @change=${context.updateInvoiceId} >
                        ${context.periodicInvoices.map(pi=> html`
                            <option value="${pi.key}" ?seleted=${context.periodicInvoiceId == pi.key}> ${pi.value}   </option>
                        `)}
                        </select>
					
				</li>
				<li>
                    <div id="subdivSelectorDiv" >
                        <label class="bold width30" for="orgid"><cwms-translate code="periodiclinking.filterbysubdiv" >Filter By Subdivision</cwms-translate>:</label>
                        <select id="subdivSelector"  @change=${context.filter(filterByBusinessSubDiv)}>
                            <option value=${filterValueAll}> -- <cwms-translate code="all">All</cwms-translate> -- </option>
                            ${context.subdivisions.map(([k,v]) => html`<option value=${k}>${v}</option>` )}
                        </select>
                    </div>
                </li>	
				<li>
					<div id="monthYearSelectorDiv" >
						<label class="bold width30" for="monthYearSelector"><cwms-translate code="periodiclinking.filterbyreturnmonth" >Filter By Return Month</cwms-translate>:</label>
						<select id="monthYearSelector" @change=${context.filter(filterByMonthYear)} >
							<option value=${filterValueAll}> -- <cwms-translate code="all">All</cwms-translate> -- </option>
                        ${context.monthYears.map(([k,v])=>html`<option value=${k} >${v}</option>`)}
						</select>
					</div>
				</li>
			</ol>
		</fieldset>`