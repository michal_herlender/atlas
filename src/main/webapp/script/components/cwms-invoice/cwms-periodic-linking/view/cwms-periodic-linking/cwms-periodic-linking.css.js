import {css} from "../../../../../thirdparty/lit-element/lit-element.js";

export const styling = (context) => css`

:host {
    display:block;
    background-color: var(--cwms-infobox-gray-background,#f2f2f2)
}

.filters {
    background-color: var(--cwms-main-gray-background,#cdcdcd);
    border: solid 1px #999;
    display: grid;
    gap: 10px;
    grid-template-columns: 1fr 1fr 1fr;
    grid-template-rows: repeat(4,1fr);
    flex-direction: column;
    padding: 5px;
}

.filters > div{
    display:flex;
    flex-direction: column;
    gap:10px;
}

.filters .flex-row {
    flex-direction:row;
}

.filters .flex-row label{
    display: flex;
    align-items:center;
}

.colspan-1-3 {
    grid-column-start:1;
    grid-column-end:span 3;
}

.colspan-1-2 {
    grid-column-start:1;
    grid-column-end:span 2;
}

.colspan-2-3 {
    grid-column-start:2;
    grid-column-end:span 3;
}


  table.default4 tbody  td  p {
      margin-top: 0;
      margin-bottom: 0;

}

.center {
    text-align:center;
}



.total-price {
    font-weight:bold;
    background-color: var(--cwms-main-gray-background, #dedede);
    border: 1px solid #999;
    font-size: 8pt;
    display:flex;
    flex-direction:row;
    justify-content: space-between;
    padding:4px;
}
`;