import { html } from "../../../../../thirdparty/lit-element/lit-element.js";
import { repeat } from "../../../../../thirdparty/lit-html/directives/repeat.js";
import { currencyFormatDirective, currencyFormatWithZeroDirective, dateFormatDirective } from "../../../../tools/formatingDirectives.js";
import "../../../../cwms-tooltips/cwms-jobitem-link-and-tooltip/cwms-jobitem-link-and-tooltip.js";
import "../../../../cwms-tooltips/cwms-job-link-and-tooltip/cwms-job-link-and-tooltip.js";
import { filterByBPONumber, filterByDateFrom, filterByDateTo, filterByDeliveryNo, filterByEmptyAmount, filterByJob, filterByJobClientRef, filterByJobItemClientRef, filterByJobServiceClientRef, filterByJobType, filterByPONumbers, filterBySubdiv, filterValueAll, filterValueNone } from "../../../common/filterConsts.js";
import { secureDeliveryLinkTemplate } from "../../../common/templates.html.js";
import { cwmsTranslateDirective } from "../../../../cwms-translate/cwms-translate-directive.js";

export const template = (context) => html`
    ${filtersTemplate(context)}
    ${propesedInvoiceItemsTableTemplate(context)}
    ${proposedInvoiceExpenseItemsTableTemplate(context)}
    ${totalPriceTemplate(context)}
`

const filtersTemplate = context => html`
<div class="filters">
   <div>${filterBySubdivTemplate(context)}</div><div></div><div></div>
   <div class="flex-row">${itemsWithEmptyPriceFitler(context) }</div><div></div><div></div>
       ${filterByJobNoTemplate(context)}
       ${filterByJobTypesTemplate(context)}
       ${filterByDelNoTemplate(context)}
       ${filterByJobItemClientRefTemplate(context)}
       ${filterByJobClientRefTemplate(context)}
       ${filterByJobServiceClientRefTemplate(context)}
       ${filterByPOTemplate(context)}
       ${filterByBPOTemplate(context)}
       <div></div>
       ${filterByDateFromTemplate(context)}
       ${filterByDateToTemplate(context)}
</div>
`
const filterBySubdivTemplate = context => html`
  <label class="widthLabel"><cwms-translate code="periodiclinking.filterbysubdiv" >Filter by Subdivision</cwms-translate></label>
  <select class="subdivFilter filter" @change=${context.filter(filterBySubdiv)} >
      <option value="ALL"><cwms-translate code="all" >ALL</cwms-translate></option>
      ${Object.entries(context.listJobSubdivs ?? {}).map(([k,v]) => 
          html`
          <option value=${v}>${k}</option>
          `)}
  </select>
`;

const itemsWithEmptyPriceFitler = context => html`
    <label><cwms-translate code="periodiclinking.selectonlyitemswithemptyprice" > Select only items with a total amount of 0 or empty</cwms-translate></label>
    <input type="checkbox" id="emptyAmount"   @change=${context.filter(filterByEmptyAmount)}/>
`
const filterByJobNoTemplate = context => html`<div>
    ${filterByListTemplate(context.filter(filterByJob))("periodiclinking.filterbyjob","Filter by Job")(context.listJobNos)}
    </div>
`;


const filterByJobTypesTemplate = context => html`<div>
    ${filterByListTemplate(context.filter(filterByJobType))("periodiclinking.filterbyjobtype","Filter by Job Type")(context.listJobTypes)}
</div>
`; 


const filterByDelNoTemplate = context => html`
<div>
    ${filterByListTemplate(context.filter(filterByDeliveryNo))("periodiclinking.filterbydelno","Filter by Delivery No")(
context.deliveryNos)}
</div>
`
const filterByJobItemClientRefTemplate = context => html`
<div>
    ${filterByListTemplate(context.filter(filterByJobItemClientRef))("periodiclinking.filterbyjobitemclientref","Filter by Job Item Client Ref")(context.listJobItemClientRefs)}
</div>
`;

const filterByJobClientRefTemplate= context => html`
<div>
    ${filterByListTemplate(context.filter(filterByJobClientRef))("periodiclinking.filterbyjobclientref","Filter by Job Client Ref")(context.listJobClientRefs)}
</div>`;

const filterByJobServiceClientRefTemplate = context => html`
<div>
    ${filterByListTemplate(context.filter(filterByJobServiceClientRef))("periodiclinking.filterbyjobserviceclientref","Filter by Job Service Client Ref")(context.listExpenseItemClientRefs)}
</div>`;

const filterByListTemplate = changeFunction => (labelCode,labelDefault,addNoneOption) => (list) => html`
<label for="deliveryNo" class="widthLabel"><cwms-translate code=${labelCode} >${labelDefault}</cwms-translate></label> 
<select class="jobStatusFilter filter" name=${labelCode} @change=${changeFunction} >
    <option value=${filterValueAll}>${cwmsTranslateDirective("all","ALL")}</option>
    ${addNoneOption?html`<option value=${filterValueNone}>${cwmsTranslateDirective("viewjob.none","None")}</option>`:html``}
    ${(list ??  [])
    .map(le => html`
    <option value=${le}>${le}</option>`)
}
</select>
`;

const filterByPOTemplate = context  => html`
<div>
    ${filterByListTemplate(context.filter(filterByPONumbers))("periodiclinking.filterbypo","Filter by PO",true)(context.listJobPOs)}
</div>
`
const filterByBPOTemplate = context  => html`
<div>
    ${filterByListTemplate(context.filter(filterByBPONumber))("periodiclinking.filterbybpo","Filter by BPO",true)(context.listJobBPOs)}
</div>
`
const filterByDateToTemplate = context => html`
<div>
    <label class="widthLabel"><cwms-translate code="periodiclinking.filterbydate">Filter By Date</cwms-translate> <cwms-translate code="system.to">To</cwms-translate>:</label>
    <input type='date' id='startDate' class="filter" @change=${context.filter(filterByDateTo)}   />
</div>
`;

const filterByDateFromTemplate = context => html`
<div>
    <label class="widthLabel"><cwms-translate code="periodiclinking.filterbydate">Filter By Date</cwms-translate> <cwms-translate code="system.from">From</cwms-translate>:</label>
    <input type='date' id='startDate' class="filter"  @change=${context.filter(filterByDateFrom)}  />
</div>
`


const propesedInvoiceItemsTableTemplate = context => html`
		<table class="default4" >
			<thead>
				
				<tr>
					<td colspan="10">
						<cwms-translate code="jobitems" >Job Items</cwms-translate> (<span class="itemsSize">${context.proposedInvoiceItems.length}</span>)
					</td>
				</tr>
				<tr>
					<th rowspan="2" class="include" scope="col">
						<input type="checkbox" ?checked=${true} @change=${context.toggleAllItems} />
					</th>
					<th rowspan="1" class="item" scope="col"><cwms-translate code="item">Item</cwms-translate> </th>
					<th rowspan="1" class="jobtype" scope="col"><cwms-translate code="jobtype" >Job Type</cwms-translate></th>
					<th rowspan="1" class="returntype" scope="col"><cwms-translate code="docs.deliverynote" >Delivery Note</cwms-translate></th>
					<th rowspan="1" class="returndate" scope="col"><cwms-translate code="periodiclinking.returndate" >Return Date</cwms-translate></th>
					<th rowspan="1" class="pono" scope="col"><cwms-translate code="invoice.ponos" >PO No (s)</cwms-translate></th>
					<th rowspan="1" colspan="2" class="calibration" scope="col"><cwms-translate code="costtype" >Price Type</cwms-translate></th>
					<th rowspan="1" colspan="2" class="sales" scope="col"><cwms-translate code="price">Price</cwms-translate> (${context.currencyCode})</th>
				
				</tr>
			</thead>
            ${context.proposedInvoiceItems ? html`
            <tfoot>
                <tr>
                    <td colspan=10>
                        
						<cwms-translate code="invoice.totalamount.jobitem" >Total amount</cwms-translate>:&nbsp;&nbsp; ${currencyFormatWithZeroDirective(context.itemsTotalPrice ?? 0,context.currencyCode)}
                    </td>
                </tr>
            </tfoot>
                        `:html``}
            <tbody>
                ${repeat(context.items,i => i.id,itemRowTemplate(context))}
            </tbody>
            </table>
`


const itemRowTemplate = context => item => html`
    <tr>
        <td class="center">
            <input type="checkbox" class="includeCheck" @change=${_ => context.toggleInclude(item)}  value="true" .checked=${item.include ?? true}  ></checkbox>
        </td>
        <td>
            <cwms-jobitem-link-and-tooltip  jobitemId="${item.jobItemId}"
            jobItemNo=${`${item.jobNo}.${item.jobItemNo}`}
            ></cwms-jobitem-link-and-tooltip>
        </td> 
        <td>${item.jobType.description}</td>
        <td>
            ${item.jobType.name=="SITE"?html`
                SITE - ${dateFormatDirective(item.dateComplete)}
            `:repeat(item.deliveryItems,secureDeliveryLinkTemplate("DELIVERY_NOTE_VIEW",true,false))}
        </td>
        <td>
            ${item.jobType.name == "SITE"?html`
                ${dateFormatDirective(item.lastActionDateStamp)}
            `:html`${dateFormatDirective(item.deliveryItems.reduce((l,r) => r.deliveryDate,""))}`}
        </td>
        <td>${[...item.poNumbers,...item.bpoNumbers].join(", ")}</td>
        <td colspan=2>${item.costType?.message??''}</td>
        <td colspan=3 class="cost2pc">${currencyFormatWithZeroDirective(item.finalCost,context.currencyCode )}</td>
    </tr>
`;


const proposedInvoiceExpenseItemsTableTemplate = context => html`
<table class="default4">
    ${proposedInvoiceExpenseItemsTableHeaderTemplate(context)}
    <tbody>
        ${repeat(context.expenseItems,i =>i.expenseItemId,proposedInvoiceExpenseItemsRowTemplate(context))}
    </tbody>
    <tfoot>
        <tr>
            <td colspan="9"><cwms-translate code="invoice.totalamount.jobservice">Total amount of job services</cwms-translate>:</td>
            <td>${currencyFormatWithZeroDirective(context.servicesTotalPrice,context.currencyCode)}
            </td>
        </tr>
    </tfoot>
</table>
`


const proposedInvoiceExpenseItemsTableHeaderTemplate = context => html`<thead>
					<tr>
						<td colspan="10">
							<cwms-translate code="invoice.serviceitem" >Service Items</cwms-translate> (<span>${context.proposedInvoiceExpenseItems.length}</span>) 
						</td>
					</tr>
					<tr>
						<th rowspan="2" class="include" scope="col">
							<input type="checkbox" @change=${context.toggleAllExpenseItems} ?checked=${true}>
						</th>
						<th rowspan="2" class="item" scope="col"><cwms-translate code="invoice.service" >Service</cwms-translate></th>
						<th rowspan="1" class="jobtype" scope="col"><cwms-translate code="jobtype" >Job Type</cwms-translate></th>
						<th rowspan="2" class="pono" scope="col"><cwms-translate code="invoice.ponos" >PO No (s)</cwms-translate></th>
						<th rowspan="2" class="returntype" scope="col"><cwms-translate code="periodiclinking.returntype" >Return Type</cwms-translate></th>
						<th rowspan="2" class="returndate" scope="col"><cwms-translate code="date" >Date</cwms-translate></th>
						<th rowspan="1" class="service" scope="col"><cwms-translate code="viewjob.service" >Description</cwms-translate></th>
						<th rowspan="2" class="singlePrice" scope="col"><cwms-translate code="singleprice" >Single Price</cwms-translate></th>
						<th rowspan="2" class="quantity" scope="col"><cwms-translate code="quantity" >Quantity</cwms-translate></th>
						<th rowspan="2" class="price" scope="col"><cwms-translate code="totalcost" >Total Price</cwms-translate> (${context.currencyCode})</th>
					</tr>
					<tr>
						<th  class="jobtype" scope="col"><cwms-translate code="invoice.clientref.job.services" >Job service Client Ref</cwms-translate></th>
						<th class="service" scope="col"><cwms-translate code="invoice.comment">Comment</cwms-translate></th>
					</tr>
				</thead>`;


const proposedInvoiceExpenseItemsRowTemplate = context => item => html`
    <tr>
        <td  rowspan="2" class="center">
            <input type="checkbox" class="includeCheck" @change=${_ => context.toggleExpenseInclude(item)}  value="true" .checked=${item.include ?? true}  ></checkbox>
        </td>
        <td rowspan="2">
            <cwms-job-link-and-tooltip  jobId="${item.jobId}" targetTab="expenses-tab"
            jobNo=${`${item.jobNo}.Service${item.expenseItemNo}`}
            ></cwms-job-link-and-tooltip>
        </td>
        <td>${item.jobType.description}</td>
        <td rowspan="2">${[...item.poNumbers,...item.bpoNumbers].join(", ")}</td>
        <td rowspan="2">
                ${item.jobType.name == 'SITE'?html`
                    <cwms-translate code="departmenttype.onsite" >On-Site</cwms-translate>`:html`
                    <cwms-translate code="periodiclinking.clientdelivery" >Client Delivery</cwms-translate>
                    `}
        </td>
        <td rowspan="2">${dateFormatDirective(item.date)}</td>
        <td>${item.model}</td>
        <td rowspan="2">${currencyFormatWithZeroDirective(item.singlePrice,context.currencyCode)}</td>
        <td rowspan=2>${item.quantity}</td>
        <td rowspan="2">${currencyFormatWithZeroDirective(item.totalCost,context.currencyCode)}</td>
    </tr>
    <tr>
        <td> ${item.jobServiceClientRef} </td>
        <td> ${item.comment} </td>
    </tr>
`

const totalPriceTemplate = context => html`
<div class="total-price">
    <span class="bold"><cwms-translate code="">Total amount</cwms-translate>:</span>
    <span>${currencyFormatWithZeroDirective(context.totalPrice,context.currencyCode)}</span>
</div>
`