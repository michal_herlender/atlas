import { LitElement, html } from "../../../../../thirdparty/lit-element/lit-element.js";
import { styling } from "./cwms-periodic-linking-global-filters.css.js";
import { template } from "./cwms-periodic-linking-global-filters.html.js";
import { fieldsetStyles } from "../../../../tools/styles/fieldset.css.js";
import { textStyles } from "../../../../tools/styles/text.css.js";
import store from "../../store/store.js";
import { updateGlobalFilters } from "../../store/actions.js";
import { MetaDataObtainer } from "../../../../../tools/metadataObtainer.js";
import { yearMonthFormatFunction } from "../../../../tools/formatingDirectives.js";

class CwmsPeriodicLinkingGlobalFilters extends LitElement {

    static get styles(){
        return [styling(),fieldsetStyles,textStyles]
    }

    static get properties(){
        return {
            clientCompanyName:{type:String},
            clientCompanyId:{type:Number},
            allocatedCompanyName:{type:String},
            allocatedSubdivName:{type:String},
            periodicInvoices:{type:Array},
            proposedInvoiceItems:{type:Array},
            proposedInvoiceExpenseItems:{type:Array},
            periodicInvoiceId:{type:Number},
        };
    }

    updateInvoiceId(e){
        const id = e.currentTarget.value;
        this.dispatchEvent(new CustomEvent("updateInvoiceId",{detail:{id}}));
    }
    

    get monthNamedateFormatter(){
        if(this.monthNameDateFormatter) return this.monthNameDateFormatter;
        this.monthNameDateFormatter = new Intl.DateTimeFormat(MetaDataObtainer.locale,{year:"numeric",month:"long"});
        return this.monthNameDateFormatter;
    }


    get monthYears(){  
        return Array.from(
            this.proposedInvoiceItems.map(i => i.jobType.name=="SITE"? i.lastActionDateStamp:i.deliveryItems.reduce((l,r) => r.deliveryDate,""))
            .filter(d => !!d)
            .reduce((l,r)=>l.set(yearMonthFormatFunction(r),this.monthNamedateFormatter.format(new Date(r))),new Map).entries());

    }

    get subdivisions() {
        const both = [...this.proposedInvoiceItems,
                        ...this.proposedInvoiceExpenseItems
                    ];
        const map = both.reduce((acc,v) => {
            acc.set(v.businessSubDivId,v.businessSubDivName)
            return acc;
        },new Map());
        return Array.from(map.entries());
    }

    constructor(){
        super();
        this.company = {};
        
        this.proposedInvoiceExpenseItems = [];
        this.proposedInvoiceItems = [];
        store.subscribe(() => {
            const {expenseItems,items} = store.getState();
            this.proposedInvoiceExpenseItems = expenseItems;
            this.proposedInvoiceItems = items;
        });
    }


    filter(fieldName){
        return e => {
            updateGlobalFilters(fieldName,e.currentTarget.value);
        }
    }

    render(){
        return  template(this);
    }
}


class CwmsPeriodicLinkingGlobalFiltersWrapper extends LitElement {

    static get properties(){
        return {
            clientCompanyName:{type:String},
            clientCompanyId:{type:Number},
            allocatedCompanyName:{type:String},
            allocatedSubdivName:{type:String},
            periodicInvoices:{type:Array},
            periodicInvoiceId:{type:Number},
            
        };
    }

    updateInvoiceId(e){
        this.periodicInvoiceId = e.detail.id;
    }

    createRenderRoot(){
        return this;
    }
    
    render(){
        return html`
        <cwms-periodic-linking-global-filers-inner 
            .allocatedCompanyName=${this.allocatedCompanyName} 
            .allocatedSubdivName=${this.allocatedSubdivName}
            .clientCompanyId=${this.clientCompanyId}
            .clientCompanyName=${this.clientCompanyName}
            .periodicInvoiceId=${this.periodicInvoiceId}
            .periodicInvoices=${this.periodicInvoices}
            @updateInvoiceId=${this.updateInvoiceId}
        ></cwms-periodic-linking-global-filers-inner>
        <input type="hidden" name="invoiceId" value=${this.periodicInvoiceId}>
        `;
    }
}
customElements.define("cwms-periodic-linking-global-filers-inner",CwmsPeriodicLinkingGlobalFilters);
customElements.define("cwms-periodic-linking-global-filers",CwmsPeriodicLinkingGlobalFiltersWrapper);