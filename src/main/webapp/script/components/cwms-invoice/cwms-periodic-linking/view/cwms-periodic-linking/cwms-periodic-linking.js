import { LitElement, html } from "../../../../../thirdparty/lit-element/lit-element.js";
import { styling } from "./cwms-periodic-linking.css.js";
import { template } from "./cwms-periodic-linking.html.js";
import { filterByBPONumber, filterByBusinessSubDiv, filterByDateFrom, filterByDateTo, filterByDeliveryNo, filterByEmptyAmount, filterByJob, filterByJobClientRef, filterByJobItemClientRef, filterByJobServiceClientRef,  filterByJobType, filterByMonthYear, filterByPONumbers, filterBySubdiv, filterValueAll, filterValueNone } from "../../../common/filterConsts.js";
import { MetaDataObtainer } from "../../../../../tools/metadataObtainer.js";
import { tableStyles } from "../../../../tools/styles/table.css.js";
import { linkStyling } from "../../../../tools/styles/links.css.js";
import { storeItems, storeExpenseItems, storeFilteredItems, storeFilteredExpenseItems } from "../../store/actions.js";
import { repeat } from "../../../../../thirdparty/lit-html/directives/repeat.js";
import  store  from  "../../store/store.js";
import { yearMonthFormatFunction } from "../../../../tools/formatingDirectives.js";

class CwmsPeriodicLinking extends LitElement {

    static get properties() {
        return {
            proposedInvoiceItems:{type:Array},
            proposedInvoiceExpenseItems:{type:Array},
            items:{type:Array},
            expenseItems:{type:Array},
			listJobSubdivs:{type:Object},
			listJobNos:{type:Array},
			listJobTypes:{type:Array},
			listJobPOs:{type:Array},
			listJobBPOs:{type:Array},
			listJobClientRefs:{type:Array},
			listJobItemClientRefs:{type:Array},
			listExpenseItemClientRefs:{type:Array},
            filterOptions:{type:Object},
            currencyCode:{type:String},
            itemsTotalPrice:{type:Number},
            servicesTotalPrice:{type:Number},
            totalPrice:{type:Number},
            globalFilters:{type:Object}
        };
    }


    get deliveryNos() {
        return [... new Set(this.proposedInvoiceItems.flatMap(i => i.deliveryItems.filter((d)=> d.isClientDelivery)).map(di => di.deliveryNo))];
    }

    get totalPriceJobItems(){
        return this.items.filter(i => i.include ?? true).map(i => i.finalCost).reduce((l,r) => l+r,0);
    }

    get totalPriceExpenseItems() {
        return this.expenseItems.filter(i=>i.include ?? true).map(i => i.totalCost).reduce((l,r) => l+r,0);
    }

    constructor(){
        super();
        this.filterOptions = {};
        this.proposedInvoiceItems = [];
        this.proposedInvoiceExpenseItems = [];
        this.currencyCode = MetaDataObtainer.defaultCurrency;
        store.subscribe(() => {
            const {items=[],expenseItems=[], globalFilters={}} = store.getState();
            if(this.proposedInvoiceItems != items || this.proposedInvoiceExpenseItems != expenseItems || (this.globalFilters != globalFilters && Object.entries(globalFilters).length >0))
            {
            this.globalFilters = globalFilters;
            this.filterOptions = {...this.filterOptions,...globalFilters};
            this.proposedInvoiceItems = items ?? [];
            this.proposedInvoiceExpenseItems = expenseItems ?? [];
            this.applyFilters();
            }
        });
    }


    set items(newItems){
        const oldItems = this.items;
        this._items = newItems;
        this.itemsTotalPrice = this.totalPriceJobItems;
        this.requestUpdate("items",oldItems);
    }

    get items(){
        return this._items ?? [];
    }

    set expenseItems(newItems){
        const oldItems = this.expenseItems;
        this._expenseItems = newItems ??[];
        this.servicesTotalPrice = this.totalPriceExpenseItems;
        this.requestUpdate("expenseItems",oldItems);
    }

    get expenseItems(){
        return this._expenseItems ?? [];
    }

    set itemsTotalPrice(newPrice){
        const oldPrice = this.itemsTotalPrice;
        this._itemsTotalPrice = newPrice;
        this.totalPrice = newPrice + this.servicesTotalPrice;
        this.requestUpdate("itemsTotalPrice",oldPrice);
    }

    get itemsTotalPrice(){
        return this._itemsTotalPrice;
    }

    set servicesTotalPrice(newPrice){
        const oldPrice = this.itemsTotalPrice;
        this._servicesTotalPrice = newPrice;
        this.totalPrice = newPrice + this.itemsTotalPrice;
        this.requestUpdate("servicesTotalPrice",oldPrice);
    }

    get servicesTotalPrice(){
        return this._servicesTotalPrice;
    }


    static get styles() {
        return [styling({}),tableStyles, linkStyling()];
    }

    filter(fieldName){
        return e => {
            const value = e.currentTarget.value;
            if([filterByDateFrom,filterByDateTo].includes(fieldName))
                this.filterOptions = {...this.filterOptions,[fieldName]:e.currentTarget.valueAsDate,}
            else if(fieldName == filterByEmptyAmount)
                this.filterOptions = {...this.filterOptions,[fieldName]:e.currentTarget.checked};
            else this.filterOptions = {...this.filterOptions,[fieldName]:value,}
        }
    }
    
    update(changedProperties){
        super.update(changedProperties);
        if(changedProperties.has("filterOptions"))
            this.applyFilters()
    }

    applyFilters() {
        queueMicrotask(
        () => {
        this.items = this.proposedInvoiceItems.filter(this.filterItem.bind(this));
        this.expenseItems = this.proposedInvoiceExpenseItems.filter(this.filterItem.bind(this));
        storeFilteredItems(this.items);
        storeFilteredExpenseItems(this.expenseItems);
        });
        queueMicrotask(() => this.requestUpdate());
    }

    filterItem(item){
        return [filterBySubdiv, filterByJob, filterByJobType, filterByDeliveryNo, filterByJobClientRef, filterByJobItemClientRef,filterByJobServiceClientRef, filterByPONumbers, filterByBPONumber, filterByDateTo, filterByDateFrom, filterByMonthYear, filterByBusinessSubDiv, filterByEmptyAmount].every(fn => this.filterByEnum(fn,item))
    }

    filterByEnum(fieldName,item){
        const filterValue = this.filterOptions[fieldName] ?? "ALL";
        if(filterValue == filterValueAll) return true;

        if(filterValue == filterValueNone)
            return !item[fieldName] || item[fieldName]?.length == 0 || item[fieldName] == {} 

        if(fieldName == filterByMonthYear){
            const returnDate = item.jobType.name=="SITE"? item.latstActionDateStamp:item.deliveryItems.reduce((l,r) => r.deliveryDate,"")
            return yearMonthFormatFunction(returnDate) == filterValue;    
        }
        if(fieldName == filterByEmptyAmount && filterValue)
            return item.finalCost == 0 || item.totalCost == 0;
        if(fieldName == filterByDeliveryNo)
            return item.deliveryItems.map(di => di.deliveryNo).includes(filterValue);

        if(fieldName==filterByDateFrom)
            return new Date(item.date) >= filterValue;

        if(fieldName==filterByDateTo)
            return new Date(item.date) <= filterValue;

        if(fieldName ==filterByJobType)
            return item[fieldName].name == filterValue;

        if(item[fieldName] instanceof Array)
            return item[fieldName].includes(filterValue);

        return item[fieldName] == filterValue;
    }


    toggleInclude(item){
        storeItems(this.proposedInvoiceItems
            .map(i => i.id == item.id?{...item,include:!item.include}:i))
    }

    toggleExpenseInclude(item){
        storeExpenseItems(this.proposedInvoiceExpenseItems
            .map(i => i.expenseItemId == item.expenseItemId?{...item,include:!item.include}:i))
    }

    toggleAllItems(e) {
        const include = e.currentTarget.checked;
        storeItems(this.proposedInvoiceItems.map(i => ({...i,include})));
    }
    
    toggleAllExpenseItems(e){
        const include = e.currentTarget.checked;
        storeExpenseItems(this.proposedInvoiceExpenseItems.map(i => ({...i,include})));
    }
    
    render(){
       return template(this);
    }

}


class CwmsPeriodicLinkingWrapper extends LitElement {

    static get properties() {
        return {
            proposedInvoiceItems:{type:Array},
            proposedInvoiceExpenseItems:{type:Array},
            items:{type:Array},
            expenseItems:{type:Array},
            filteredItems:{type:Array},
            filteredExpenseItems:{type:Array},
			listJobSubdivs:{type:Object},
			listJobNos:{type:Array},
			listJobTypes:{type:Array},
			listJobPOs:{type:Array},
			listJobBPOs:{type:Array},
			listJobClientRefs:{type:Array},
			listJobItemClientRefs:{type:Array},
			listExpenseItemClientRefs:{type:Array},
            currencyCode:{type:String},
        };
    }


    constructor(){
        super();
        this.expenseItems =[];
        this.items =[];
        this.filteredExpenseItems =[];
        this.filteredItems =[];
        store.subscribe(() => {
           const {items,expenseItems,filteredItems,filteredExpenseItems} = store.getState();
           if(this.items != items || this.expenseItems != expenseItems || this.filteredItems != filteredItems ||this.filteredExpenseItems != filteredExpenseItems){
                this.items = items ?? [];
                this.expenseItems = expenseItems ?? [];
                this.filteredExpenseItems = filteredExpenseItems;
                this.filteredItems = filteredItems;
           }
        }
    );
    }

    createRenderRoot(){
        return this;
    }

    connectedCallback(){
        super.connectedCallback();
        storeExpenseItems(this.proposedInvoiceExpenseItems);
        storeItems(this.proposedInvoiceItems);
    }


    render() {
        return html`
        <cwms-periodic-linking-inner
			.listJobSubdivs=${this.listJobSubdivs}
			.listJobNos=${this.listJobNos}
			.listJobTypes=${this.listJobTypes}
			.listJobPOs=${this.listJobPOs}
			.listJobBPOs=${this.listJobBPOs}
			.listJobClientRefs=${this.listJobClientRefs}
			.listJobItemClientRefs=${this.listJobItemClientRefs}
			.listExpenseItemClientRefs=${this.listExpenseItemClientRefs}
            .currencyCode=${this.currencyCode}

        ></cwms-periodic-linking-inner>
        ${repeat(this.filteredItems,i => i.id,(i) => i.include ?
            html`<input type="hidden" name="items" value="${i.jobItemId}" />` :
            html``
        )}
        ${repeat(this.filteredExpenseItems,i=>i.expenseItemId,(i) => i.include ?
            html`<input type="hidden" name="expenseItems" value=${i.expenseItemId} />` :
            html``
        )}
        `;
    }
}

customElements.define("cwms-periodic-linking",CwmsPeriodicLinkingWrapper);
customElements.define("cwms-periodic-linking-inner",CwmsPeriodicLinking);