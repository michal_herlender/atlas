import { STORE_EXPENSE_ITEMS,  STORE_FILTERED_EXPENSE_ITEMS,  STORE_FILTERED_ITEMS,  STORE_ITEMS, UPDATE_GLOBAL_FILTERS} from "./actionsTypes.js"

let dispatch; export default dispatch = function(state,command){
    switch(command.type){
        case STORE_FILTERED_ITEMS: 
            return {...state, filteredItems:command.items};
        case STORE_FILTERED_EXPENSE_ITEMS:
            return {...state, filteredExpenseItems:command.expenseItems}
        case STORE_ITEMS: 
            return {...state, items:command.items};
        case STORE_EXPENSE_ITEMS:
            return {...state, expenseItems:command.expenseItems}
        case UPDATE_GLOBAL_FILTERS:
            const {globalFilters={}} = state;
            return {...state, globalFilters:{...globalFilters,...command.globalFilters}};
        default: return state;
    }
}