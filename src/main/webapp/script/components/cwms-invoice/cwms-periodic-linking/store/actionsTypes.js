export const STORE_ITEMS = "STORE_ITEMS";
export const STORE_EXPENSE_ITEMS = "STORE_EXPENSE_ITEMS";
export const UPDATE_GLOBAL_FILTERS ="UPDATE_GLOBAL_FILTERS"
export const STORE_FILTERED_ITEMS = "STORE_FILTERED_ITEMS";
export const STORE_FILTERED_EXPENSE_ITEMS = "STORE_FILTERED_EXPENS_ITEMS";