import store from "./store.js";
import { STORE_EXPENSE_ITEMS,  STORE_FILTERED_EXPENSE_ITEMS,  STORE_FILTERED_ITEMS,  STORE_ITEMS, UPDATE_GLOBAL_FILTERS} from "./actionsTypes.js";


export function storeItems(items){
    store.send({type:STORE_ITEMS,items});
}

export function storeExpenseItems(expenseItems){
    store.send({type:STORE_EXPENSE_ITEMS,expenseItems});
}

export function updateItem(item){
    const {items} = store.getState();
    storeItems(items.map(i => i.id == item.id?item:i));
}

export function updateGlobalFilters(fieldName,value){
    store.send({type:UPDATE_GLOBAL_FILTERS,globalFilters:{[fieldName]:value}});
}

export function storeFilteredItems(items){
    store.send({type:STORE_FILTERED_ITEMS,items});
}

export function storeFilteredExpenseItems(expenseItems){
    store.send({type:STORE_FILTERED_EXPENSE_ITEMS,expenseItems});
}