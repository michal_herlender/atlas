import { html } from "../../../thirdparty/lit-element/lit-element.js";
import { until } from "../../../thirdparty/lit-html/directives/until.js";
import { authorityChecker } from "../../../tools/AuthoritiesChecker.js";
import { viewDelNoteLinkGenerator } from "../../../tools/UrlGenerator.js";
import { countryNameDirective, dateFormatDirective } from "../../tools/formatingDirectives.js";

export const secureDeliveryLinkTemplate = (auth,showClientDelivery,showThirdPartyDelivery) => delItem => 
{
    showClientDelivery = showClientDelivery ?? true;
    showThirdPartyDelivery = showThirdPartyDelivery ?? true;
    const showLink = (showClientDelivery && delItem.isClientDelivery) || (showThirdPartyDelivery || delItem.showThirdPartyDelivery);
    return showLink? until(authorityChecker.hasAuthority(auth)
.then(e =>html`<p> ${delItem.isClientDelivery? 'CD - ':delItem.isThirdPartyDelivery? 'TPD - ':''}  ${e?html`
<a role="button" class="mainlink"  .href=${viewDelNoteLinkGenerator(delItem.deliveryId).toString()} >
 ${delItem.deliveryNo} 
</a> 
`:html`<span>${delItem.deliveryNo}</span>`} (${dateFormatDirective(delItem.deliveryDate)}) ${countryNameDirective(delItem.countryCode)}</p>
`).catch(e =>{console.error(e); return html`No Authorities` }) ,html`linking `):html``;
};
