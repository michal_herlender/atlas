
export const filterByPriceBox = "filterByPriceBox"
export const filterBySubdiv ="subdivId";
export const filterByJob ="jobNo";
export const filterByJobStatus ="jobStatusName";
export const filterByJobType ="jobType";
export const filterByDeliveryNo ="deliveryNo";
export const filterByJobClientRef ="jobClientRef";
export const filterByJobItemClientRef ="jobItemClientRef";
export const filterByJobServiceClientRef ="jobSrviceClientRef";
export const filterByPONumbers ="poNumbers";
export const filterByBPONumber ="bpoNumbers";
export const filterByDateFrom ="dateFrom";
export const filterByDateTo ="dateTo";
export const filterByEmptyAmount ="emptyPrice"

export const filterValueAll="ALL";
export const filterValueNone="NONE";


export const filterByBusinessSubDiv ="businessSubDivId";
export const filterByMonthYear ="filterByMonthYear";
