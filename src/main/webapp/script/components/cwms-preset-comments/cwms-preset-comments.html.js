import { html } from '../../thirdparty/lit-element/lit-element.js'
import { cwmsTranslateDirective } from '../cwms-translate/cwms-translate-directive.js';

export const template = (context) => html`
  <div>
    <select class="presetCommentSelection width86" @input=${context.updateField("presetComment",context)}>
      <option value=""  ?selected=${true}>-- Select a preset comment to add --</option>
        ${context.comments?.map(c => html`<option value=${c.comment} title=${c.comment}>${c.comment}</option>`)}
    </select><a role="button" @click=${(_) => context.editComments()}><img
        src="img/icons/form_edit.png"
        width="16"
        height="16"
        alt=${cwmsTranslateDirective("presetcomment.editPresentComments", "Edit Preset Comments")}
        title=${cwmsTranslateDirective("presetcomment.editPresentComments", "Edit Preset Comments")} /></a>&nbsp;
        <a role="button" @click=${(_) => context.refreshComments()}><img
        src="img/icons/refresh.png"
        width="16"
        height="16"
        alt="${cwmsTranslateDirective("presetcomment.refreshPresetCommentList", "Refresh Preset Comment List")}title"
        ="Refresh Preset Comment List"/></a>
  </div>
  <slot></slot>
`;