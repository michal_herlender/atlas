import { LitElement, html } from "../../thirdparty/lit-element/lit-element.js";
import { styling } from "./cwms-preset-comments.css.js";
import { template } from "./cwms-preset-comments.html.js";
import { linkStyling } from "../tools/styles/links.css.js";
import { urlWithParamsGenerator } from "../../tools/UrlGenerator.js";

class CwmsPresetComments extends LitElement {

    #textarea;

    static get properties(){
        return {
            type:{type:String},
            comment:{type:String},
            comments:{type:Array}
        }
    }

    static get styles(){
        return [styling(), linkStyling()];
    }

    get textarea(){
        if(this.#textarea) return this.#textarea;
        const ta = this.querySelector("textarea");
        if(ta)
            this.#textarea = ta;
        return this.#textarea;
    }

    connectedCallback(){
        super.connectedCallback();
        this.refreshComments();
    }

    editComments(){
        window.open(urlWithParamsGenerator("editpresetcomments.htm",{type:this.type}));
    }

    refreshComments(){
        fetch(urlWithParamsGenerator("getpresetcomments.json",{commentType:this.type}))
        .then(res => res.ok?res.json():Promise.reject(res.statusText))
        .then(comments => this.comments = comments)
        .catch(console.error);

    }

    updateField(field,item){
        return e => {
            const value = e.currentTarget.value;
            this.comment = value;
            if(this.textarea){
                this.textarea.value = value;
                this.textarea.dispatchEvent(new Event("input"));
            }
        }
    }

    render(){
       return template(this);
    }

}


customElements.define("cwms-preset-comments",CwmsPresetComments);