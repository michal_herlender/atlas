import { css } from '../../thirdparty/lit-element/lit-element.js'

export const styling = (context) => css`
    
    :host {
        width: var(--cwms-preset-comments-width,300px);
        display:flex;
        flex-direction: column;

    }
`;