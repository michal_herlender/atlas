import { html } from "../../../../thirdparty/lit-element/lit-element.js";
import { urlWithParamsGenerator } from "../../../../tools/UrlGenerator.js";
import "../../common/cwms-duplicates-list/cwms-duplicates-list.js"


export const template = (context) => html`
    <slot></slot>
   ${context.hasWarning?html` <li class="errorContainer">
       <cwms-duplicates-list .companyInstruments=${context.companyInstruments}
        .residualGroupInstruments=${context.residualGroupInstruments}
        .groupName=${context.groupName}
        .acceptSuggestion=${inst => {window.location.assign(urlWithParamsGenerator("editinstrument.htm",{plantid:inst.plantId}))} }
        >
    </cwms-duplicates-list>
    </li>`:html``}
`;