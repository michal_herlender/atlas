import { LitElement } from "../../../../thirdparty/lit-element/lit-element.js";
import { urlWithParamsGenerator } from "../../../../tools/UrlGenerator.js";
import { SimpleInstrumentDto } from "../../logic/duplicateCheckService.js";
import CwmsTranslate from "../../../cwms-translate/cwms-translate.js";
import { duplicateCheckService } from "../../logic/duplicateCheckService.js";
import { template } from "./cwms-edit-instrument-duplicates.html.js";
import { styling } from "./cwms-edit-instrument-duplicates.css.js";

class CwmsEditInstrumentDuplicates extends LitElement {
  #coid;
  #existMessage;
  #disallowSamePlantNumbers;
  #plantNoInput;
  #serialNoInput;

  static get properties() {
    return {
      companyInstruments: { type: Array },
      residualGroupInstruments: { type: Array },
      groupName: { type: String },
      currentPlantId: {type:String},
      modelId: { type: Number },
      coid:{type:Number}
    };
  }

  static get styles() {
    return [styling];
  }

  get coid() {
    return this.#coid;
  }

  set coid(coid) {
    const oldValue = this.coid;
    if (oldValue == coid) return;
    this.#coid = coid;
    this.updateCompanyOption(coid);
  }

  constructor() {
    super();
    this.companyInstruments = [];
    this.residualGroupInstruments = [];
    // dirty way of tracking changes of the selected company in the CascadeSearchPlugin [CSP], we are waiting here for the <input name="coid"/> crated by the CSP and then we are observing input for the value changes
    const cascadeObserver = new MutationObserver(this.cascadeObserverCallback.bind(this));
    const rootOfSearchPlugin =  document.getElementById("contactSelector");
    if(rootOfSearchPlugin)
    	cascadeObserver.observe(rootOfSearchPlugin,{childList:true,subtree:true});
    this.coidObserver = new MutationObserver(this.coidChanged.bind(this));
    this.checkDuplicatesForInstrument = debounce(this.performDuplicateChecking, 500);
    CwmsTranslate.messagePromise("addnewitemstojob.instrument.instrumentExistsError", "Instrument already exists!!!").then((msg) => (this.#existMessage = msg));
  }

  cascadeObserverCallback(mutationList,observer) {
    mutationList.filter(mr => mr.type=="childList" && mr.target.id == "compSearchPlugin" )
    .flatMap(mr => Array.from(mr.addedNodes)).filter(n => n.name == "coid")
    .forEach(input => {
      this.coidObserver.observe(input,{ attributeFilter: ["value"], attributeOldValue: true })
      observer.disconnect();
    
    })
  }

  get plantNoInput() {
    if (this.#plantNoInput) return this.#plantNoInput;
    this.#plantNoInput = this.querySelector("[name='plantNo'");
    return this.#plantNoInput;
  }

  get serialNoInput() {
    if (this.#serialNoInput) return this.#serialNoInput;
    this.#serialNoInput = this.querySelector("[name='serialNo'");
    return this.#serialNoInput;
  }

  connectedCallback() {
    super.connectedCallback();
    this.plantNoInput?.addEventListener("keyup", (_) => this.checkDuplicatesForInstrument());
    this.serialNoInput?.addEventListener("keyup", (_) => this.checkDuplicatesForInstrument());
  }

  performDuplicateChecking() {
    duplicateCheckService
      .checkDuplicatesForInstrument(new SimpleInstrumentDto(this.coid, this.plantNoInput?.value, this.serialNoInput?.value, this.modelId))
      .then((result) => {
        this.companyInstruments = result.companyInstruments.filter(i => i.plantId != this.currentPlantId);
        this.residualGroupInstruments = result.residualGroupInstruments;
        this.groupName = result.groupName;
        if (this.hasPerfectMatch && this.#disallowSamePlantNumbers) {
          this.plantNoInput.setCustomValidity(this.#existMessage);
        } else {
          this.plantNoInput.setCustomValidity("");
        }
      })
      .catch((_) => {
        this.companyInstruments = [];
        this.plantNoInput.setCustomValidity("");
        this.residualGroupInstruments = [];
      });
  }

  get hasWarning() {
    return this.companyInstruments.length > 0;
  }

  get hasPerfectMatch() {
    return !!this.companyInstruments.find((i) => i.plantNo == this.plantNoInput.value && i.serialNo == this.serialNoInput.value);
  }

  coidChanged(records) {
    const coid = records[0].target.value;
    this.coid = coid;
  }

  updateCompanyOption(coid) {
    const url = urlWithParamsGenerator("settings/disallowSamePlantNumberForCompany.json", { coid });
    fetch(url)
      .then((res) => (res.ok ? res.json() : Promise.reject(res.statusText)))
      .then((result) => (this.#disallowSamePlantNumbers = result));
  }
  render() {
    return template(this);
  }
}

customElements.define("cwms-edit-instrument-duplicates", CwmsEditInstrumentDuplicates);
