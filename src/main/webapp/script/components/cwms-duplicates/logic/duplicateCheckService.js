import { urlWithParamsGenerator } from "../../../tools/UrlGenerator.js";
import store from "../cwms-new-jobitems/store/store.js";

class DuplicateCheckService {

  #basketInstruments;

  constructor(){
    this.#basketInstruments = [];
    store.subscribe(()=> {
            const {instruments} = store.getState();
            this.#basketInstruments = instruments ?? [];
    });
  }

    async checkDuplicatesForInstrument(instrument){
       return fetch(urlWithParamsGenerator("instrument/checkDuplicatesForInstrument.json",instrument))
        .then(res => res.ok? res.json():Promise.reject(res.statusText))
        .then(r =>
          r.isValid ? r.result : Promise.reject("no instruments was found")
        )
        .then(r => {
          const companyInstruments = r.companyInstruments.map(i =>
            ({...i, isOnJob: this.isInBasket(i) })
          );
          const residualGroupInstruments = r.residualGroupInstruments.map(i =>
           ({...i,  isOnJob: this.isInBasket(i)})
          );
          return {...r, 
            companyInstruments,
            residualGroupInstruments
          };
        })
    }

    isInBasket(instrument){
        return this.#basketInstruments.find(i => i.instId == instrument.instId);
    }

    simplifyId(id){
      return id ? id.replaceAll(/[\.\-\+_\(\)\]\{\}\[\]\s\/\\]/ig,"")
               .replaceAll(/i/ig,"1")
               .replaceAll(/o/ig,"0")
               .replaceAll(/z/ig,"2")
               .replaceAll(/l/ig,"1")
               .replaceAll(/s/ig,5)
               .replaceAll(/^0+/ig,""):id;
    }
}



export class SimpleInstrumentDto {
    constructor(coid,  plantNo, serialNo, modelId) {
      this.coid = coid;
      this.plantNo = plantNo;
      this.serialNo = serialNo;
      this.modelId = modelId;
      this.mfrReqForModel = 0;
      this.searchModelMatches = true;
    }
  }

export const duplicateCheckService =  new DuplicateCheckService();