import { LitElement, html } from "../../../../../thirdparty/lit-element/lit-element.js";
import { repeat } from "../../../../../thirdparty/lit-html/directives/repeat.js";
import { DateUtils } from "../../../../tools/date-utils.js";
import { linkStyling } from "../../../../tools/styles/links.css.js";
import { structureStyles } from "../../../../tools/styles/structure.css.js";
import { tableStyles } from "../../../../tools/styles/table.css.js";
import { textStyles } from "../../../../tools/styles/text.css.js";
import { storeBasketInstruments, storeBasketItems } from "../../store/actions.js";
import store from "../../store/store.js";
import { PROCEDURE_ID, SERVICE_TYPE, WORKINSTRUCTION_ID } from "../../../common/tools/constants.js";
import { styling } from "./cwms-newjobitem-basket.css.js";
import { template } from "./cwms-newjobitem-basket.html.js";

class CwmsNewJobItemBasketInner extends LitElement {

    static get properties(){
       return {
            jobId:{type:Number},
            jobNo:{type:String},
            subdivId:{type:Number},
            serviceTypes:{type:Array},
           basketInstruments:{type:Array}
       } ;
    }

    static get styles(){
        return [styling(),structureStyles, tableStyles, textStyles, linkStyling()];
    }

    get instrumentsCount(){
        return this.basketInstruments.length;
    }

    constructor(){
        super();
        this.basketInstruments = [];
        store.subscribe(() => {
            const {instruments:basketInstruments} = store.getState();
            this.basketInstruments = basketInstruments;
        });
    }

    updateFields(fieldName,currentItem){
        return e => {
            storeBasketInstruments(this.basketInstruments.map(i => i.instId == currentItem.instId?{...i, [fieldName]:e.currentTarget.value}:i ));
        };
    }

    copyField(fieldsNames,direction,index){
        const predicate = (idx) => direction == "up"?idx < index:direction=="down"?idx>index:false;
        const updateItem = (item,refenceObject) => fieldsNames.reduce((acc,f)=>({...acc,[f]:refenceObject[f]}),item);
        return value => {
            const newItems = this.basketInstruments.map((i,idx)=> predicate(idx)?updateItem(i,value):i);
            storeBasketInstruments(newItems);
        }
    }

    render(){
       return template(this);
    }

}

class CwmsNewJobItemBasketWrapper extends LitElement {

    static get properties(){
        return {
            jobId:{type:Number},
            subdivId:{type:Number},
            jobNo:{type:String},
            serviceTypes:{type:Array},
            basketItems:{type:Array},
            basketInstruments:{type:Array}
        };
    }

    constructor(){
        super();
        this.basketInstruments = [];
        store.subscribe(() => {
            const {instruments} = store.getState();
            this.basketInstruments = instruments;
        });

    }


    connectedCallback(){
        super.connectedCallback();
        storeBasketItems(this.basketItems);
    }


    createRenderRoot(){
        return this;
    }

    renderInstrumentsInputs(item) {
        return html`
        <input type="hidden" name="instDeletes[${item.ordinal}]" value="false"/>
        <input type="hidden" name="basketIds[${item.ordinal}]" value="${item.instId}" />
        <input type="hidden" name="basketParts[${item.ordinal}]" value="${item.part ??""}" />
        <input type="hidden" name="basketExistingGroupIds[${item.ordinal}]" value="${item.existingGroup??""}" />
        <input type="hidden" name="basketTempGroupIds[${item.ordinal}]" value="${item.tempGroup??""}" />
        <input type="hidden" name="basketTypes[${item.ordinal}]" value="instrument" />
        <input type="hidden" name="basketBaseItemIds[${item.ordinal}]" value="${item.baseItemId??""}" />
        <input type="hidden" name="basketQuotationItemIds[${item.ordinal}]" value="${item.quotationItemId??""}" />
        <input type="hidden" name="datesIn[${item.ordinal}]" value=${DateUtils.dateToISODateString(item.dateIn)??""} />
        <input type="hidden" name="bookInByContactIds[${item.ordinal}]"  value="${item.bookInByContactIds??""}" />
        <input type="hidden" name="itemProcIds[${item.ordinal}]" value="${item[PROCEDURE_ID]??""}" />
        <input type="hidden" name="itemWorkInstIds[${item.ordinal}]" value="${item[WORKINSTRUCTION_ID]??""}" />
        <input type="hidden" name="serviceTypeIds[${item.ordinal}]" value="${item[SERVICE_TYPE]??""}" />
        `;
    }

    render(){
        return html`
        <cwms-newjobitem-basket-innner 
        .subdivId=${this.subdivId}
        .jobId=${this.jobId} .jobNo=${this.jobNo} .serviceTypes=${this.serviceTypes}></cwms-newjobitem-basket-innner>
        ${repeat(this.basketInstruments,i=>i.instId,this.renderInstrumentsInputs)}
        `;
    }
}

customElements.define("cwms-newjobitem-basket-innner",CwmsNewJobItemBasketInner);
customElements.define("cwms-newjobitem-basket",CwmsNewJobItemBasketWrapper);

