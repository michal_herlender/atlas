import { css } from "../../../../../thirdparty/lit-element/lit-element.js";

export const styling = context => css`
cwms-instrument-link-and-tooltip {
    display: inline-block;
}

.quot-label {
    width: 9%;
}

.quot-value {
    width:23%;
}

.serialNo-label {
    width: 5%;
}

.serialNo-value {
    width: 5%;
}

.procedure-label, .workinstruction-label {
    width: 8%;
}

.procedure-value, .workinstruction-value {
    width: 25%;
}

.green {
    color: green;
    font-size: 12px;
}

cwms-capability-search {
    width: 180px;
}

`;