import {html} from "../../../../../thirdparty/lit-element/lit-element.js";
import {repeat} from "../../../../../thirdparty/lit-html/directives/repeat.js";
import {
    ADDRESS_ID,
    CAL_FREQ,
    CAL_FREQ_UNIT,
    CUSTOMER_DESCRIPTION,
    END_RANGE,
    INST_MODEL_NAME,
    ITEM_PO,
    ITEM_PO_NAME,
    MFR_INST_ID,
    MFR_INST_NAME,
    PERSON_ID,
    PLANT_NO,
    PROCEDURE_ID,
    PROCEDURE_REFERENCE,
    RANGE_UNIT,
    SERIAL_NO,
    SERVICE_TYPE,
    START_RANGE
} from "../../../common/tools/constants.js";
import {
    copyUpDownArrowsTemplate,
    modelLinkTemplate,
    serviceTypeSourceTemplate,
    serviceTypesSelectTemplate
} from "../../../common/tools/templates.html.js";
import "../../../../search-plugins/cwms-mfr-search/cwms-mfr-search.js"
import "../../../../search-plugins/cwms-po-search/cwms-po-search.js"
import "../../../common/cwms-duplicates-list/cwms-duplicates-list.js";


export const template = (context) => (model, idx) => html`
    <fieldset class=${fieldsetClassCalculator(context, idx)}>
        ${removeIconTemplate(context)(model)}
        ${context.hasPerfectMatch ? html`<span class="error"><cwms-translate code="addnewitemstojob.instrument.instrumentExistsError">Instrument already exists!!!</cwms-translate></span>` : html``}
        ${newInstrumentFormTemplate(context)(model, idx)}
    ${context.hasWarning?html`<cwms-duplicates-list .companyInstruments=${context.companyInstruments}
        .residualGroupInstruments=${context.residualGroupInstruments}
        .groupName=${context.groupName} .acceptSuggestion=${context.acceptInstrument(model)}
       ></cwms-duplicates-list>`:html``}
</fieldset>
`;

const selectContactTemplate = context => item => 
                        selectByListTemplate(context.contacts)(context.updateFields(PERSON_ID,item))(c => c.key)(c=>c.value)(c => c.key == item[PERSON_ID] );

const selectAddressTemplate = context => item =>
                        selectByListTemplate(context.addresses)(context.updateFields(ADDRESS_ID,item))(c=>c.key)(c => c.value)(a =>a.key == item[ADDRESS_ID]);

const selectByListTemplate  = listOfOptions=> changeFunction => keyFunction => valueFunction => selectedPredicate =>  html`
    <select @change=${changeFunction} >
        ${repeat(listOfOptions,keyFunction,l => 
            html`
            <option value=${keyFunction(l)}
                ?selected=${(selectedPredicate??(_=>false))(l)}>
                ${valueFunction(l)}
            </option>`)}
    </select>
`;


const removeIconTemplate = context => model => html`
<a  role="button" class="delete-icon" @click=${_ => context.confirmModelRemoval(model)} title="Remove Item">
                          <img src="img/icons/delete.png" width="16" height="16">
                        </a>
`
const fieldsetClassCalculator = (context,idx) => `${idx % 2 ==0 ? "even":"odd"} ${context.hasPerfectMatch?"warningBox3":context.hasWarning?"warningBox2":""}`


const newInstrumentFormTemplate = (context) => (model, idx) =>
  html`
      <table class="default4">
          <tbody>
          <tr>
              <td colspan="2">${modelLinkTemplate(model)}</td>
              <td class="procedure-label">
                  <span class="bold"><cwms-translate code="jobresults.procedure">Capability</cwms-translate>:</span>
              </td>
              <td class="procedure-value">
                  <cwms-capability-search
                          .hideDeactivated=${true}
                          .subdivId=${context.subdivId}
                          .defaultvalue=${model.procReference}
                          @selected=${(e) => {
                              context.updateFields(PROCEDURE_ID, model)({currentTarget: {value: e.detail?.id}});
                              context.updateFields(PROCEDURE_REFERENCE, model)({currentTarget: {value: e.detail?.reference}});
                          }}
                  ></cwms-capability-search>
                  ${copyUpDownArrowsTemplate(context)([PROCEDURE_ID, PROCEDURE_REFERENCE], idx)(model)()}
              </td>
              <td class="workinstruction-label">
            <span class="bold"><cwms-translate code="updateallitems.workinstr">Work Instruction</cwms-translate>:</span>
          </td>
          <td class="workinstruction-value">
            <div>
              <cwms-modelcode-search
                @selected=${(e) => {
                  context.updateFields(WORKINSTRUCTION_ID, model)({ currentTarget: { value: e.detail.id } });
                }}
                modelid="${model.modelId}"
                name=${`itemWorkInstIds[${model.oridnal}]`}
                defaultvalue="${model.workInstructionTitle ?? ""}"
                value="${model.workInstructionId ?? ""}"
              ></cwms-modelcode-search>
            </div>
            <!-- clear floats and restore page flow -->
            <div class="clear"></div>
          </td>
        </tr>
        <tr>
          <td class="mfr-label">
            <span class="bold"><cwms-translate code="addnewitemstojob.manufacturer">Manufacturer</cwms-translate></span>
          </td>
          <td class="mfr-value">
            ${model.modelType == "MFR_GENERIC"
              ? html` <cwms-mfr-search
                    .showLoadAll=${true}
                    @selected=${(e) => {
                      context.updateFields(MFR_INST_ID, model)({ currentTarget: { value: e.detail.id } });
                      context.updateFields(MFR_INST_NAME, model)({ currentTarget: { value: e.detail.label } });
                    }}
                    name=${`itemWorkInstIds[${model.oridnal}]`}
                    .defaultvalue=${model[MFR_INST_NAME] ?? model.mfrName ?? ""}
                  ></cwms-mfr-search>
                  ${copyUpDownArrowsTemplate(context)([MFR_INST_ID,MFR_INST_NAME], idx)(model)(m => m.modelType == "MFR_GENERIC")}`
              : html`${model.mfrName ?? ""}`}
          </td>
          <td class="contact-label">
            <span class="bold"><cwms-translate code="contact">Contact</cwms-translate>:</span>
          </td>
          <td class="contact-value">${selectContactTemplate(context)(model)} ${copyUpDownArrowsTemplate(context)([PERSON_ID], idx)(model)()}</td>
          <td class="calFreq-label">
            <span class="bold"><cwms-translate code="addnewitemstojob.calfreq">Cal Freq</cwms-translate>:</span>
          </td>
          <td class="calFreq-value">
            ${selectByListTemplate(Array.from({ length: 60 }, (_, i) => i + 1))(context.updateFields(CAL_FREQ, model))((i) => i)((i) => i)((i) => i == (model?.[CAL_FREQ]?? 12))}
            ${selectByListTemplate(context.calFrequencyUnits ?? [])(context.updateFields(CAL_FREQ_UNIT, model))((u) => u.key)((u) => u.value)(
              (u) => u.key == (model?.[CAL_FREQ_UNIT]?? "MONTH")
            )}
            ${copyUpDownArrowsTemplate(context)([CAL_FREQ, CAL_FREQ_UNIT], idx)(model)()}
          </td>
        </tr>
        <tr>
          <td>
            <span class="bold"><cwms-translate code="addnewitemstojob.modelName">Model</cwms-translate>:</span>
          </td>
          <td>
            ${model.modelType == "MFR_GENERIC"
              ? html`
                  <input .value=${model[INST_MODEL_NAME] ?? ""} @change=${context.updateFields(INST_MODEL_NAME, model)} />
                  ${copyUpDownArrowsTemplate(context)([INST_MODEL_NAME], idx)(model)()}
                `
              : html` ${model.modelName} `}
          </td>
          <td>
            <span class="bold"><cwms-translate code="address">Address</cwms-translate>:</span>
          </td>
          <td>${selectAddressTemplate(context)(model)} ${copyUpDownArrowsTemplate(context)([ADDRESS_ID], idx)(model)()}</td>
          <td>
            <span class="bold"><cwms-translate code="jobresults.itempo">Item PO</cwms-translate>:</span>
          </td>
          <td>
            <cwms-po-search
              jobId=${context.jobId}
              .defaultvalue=${model[ITEM_PO_NAME] ?? ""}
              ?showLoadAll=${true}
              @selected=${(e) => {
                context.updateFields(ITEM_PO, model)({ currentTarget: { value: e.detail.poId } });
                context.updateFields(ITEM_PO_NAME, model)({ currentTarget: { value: e.detail.poNumber } });
              }}
            ></cwms-po-search>
            ${copyUpDownArrowsTemplate(context)([ITEM_PO,ITEM_PO_NAME], idx)(model)()}
          </td>
        </tr>
        <tr>
          <td>
            <span class="bold"><cwms-translate code="jobresults.plantno">Plant No</cwms-translate>:</span>
          </td>
          <td>
            <input name="plantNo" .value=${model[PLANT_NO] ?? ""} @keyup=${context.updateFields(PLANT_NO, model)} />
            ${copyUpDownArrowsTemplate(context)([PLANT_NO], idx)(model)()}
            ${context.showDuplicatePlantNoWarning
              ? html`
                  <div class="warningBox1 warningBox2 warningBox3">
                    <cwms-translate code="addnewitemstojob.jobitem.warnDuplicatePlantNoFound"
                      >Some instrument you are trying to add have same plant number, please review before adding!</cwms-translate
                    >
                  </div>
                `
              : context.showSimilarNumbersWarning
              ? html`
                  <div class=" warningBox2">
                    <cwms-translate code="addnewitemstojob.jobitem.warnSimilarPlantNoFound"
                      >Some instrument you are trying to add have similar plant or serial number, please review before adding!</cwms-translate
                    >
                  </div>
                `
              : html``}
          </td>
          <td>
            <span class="bold"><cwms-translate code="addnewitemstojob.defaultservicetype">Default service type</cwms-translate>:</span>
          </td>
          <td class="servicetype-value">
            ${serviceTypesSelectTemplate(context)(model)} ${copyUpDownArrowsTemplate(context)([SERVICE_TYPE], idx)(model)()}
            ${serviceTypeSourceTemplate(model.serviceTypeSource ?? 0)}
          </td>
          <td>
            <span class="bold"><cwms-translate code="quotationitem">Quotation item</cwms-translate>:</span>
          </td>
          <td>${model.quotationItem.value}</td>
        </tr>
        <tr>
          <td>
            <span class="bold"><cwms-translate code="serialno">Serial No</cwms-translate>:</span>
          </td>
          <td class="serial-value">
            <input name="serialNo" .value=${model[SERIAL_NO] ?? ""} @keyup=${context.updateFields(SERIAL_NO, model)} />
            ${copyUpDownArrowsTemplate(context)([SERIAL_NO], idx)(model)()}
          </td>
          <td>
            <span class="bold"><cwms-translate code="viewinstrument.customerdescription">Customer Description</cwms-translate>:</span>
          </td>
          <td class="customer-description-value">
            <input name="customerDescription" @change=${context.updateFields(CUSTOMER_DESCRIPTION, model)} .value=${model[CUSTOMER_DESCRIPTION] ?? ""} />
          </td>
          <td>
            <span class="bold"><cwms-translate code="updateallitems.rangesize">Range/Size</cwms-translate>:</span>
          </td>
          <td class="range-value">
            <input type="text" .value=${model[START_RANGE]??""} size="4" @change=${context.updateFields(START_RANGE, model)} /> -
            <input type="text"  .value=${model[END_RANGE]??""} size="4" @change=${context.updateFields(END_RANGE, model)} />
            ${selectByListTemplate(context.rangeUnits)(context.updateFields(RANGE_UNIT, model))((u) => u.key)((u) => u.value)(u => u.key == model[RANGE_UNIT])}
            ${copyUpDownArrowsTemplate(context)([START_RANGE, END_RANGE, RANGE_UNIT], idx)(model)()}
          </td>
        </tr>
      </tbody>
    </table>
  `;