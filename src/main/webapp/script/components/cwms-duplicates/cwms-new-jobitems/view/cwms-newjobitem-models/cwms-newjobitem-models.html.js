import {html} from "../../../../../thirdparty/lit-element/lit-element.js";
import {repeat} from "../../../../../thirdparty/lit-html/directives/repeat.js";
import {jobLinkGenerator} from "../../../../../tools/UrlGenerator.js";
import {cwmsTranslateDirective} from "../../../../cwms-translate/cwms-translate-directive.js";
import "../cwms-newjobitem-model/cwms-newjobitem-model.js";
import "../../../../search-plugins/cwms-mfr-search/cwms-mfr-search.js";
import "../../../../search-plugins/cwms-po-search/cwms-po-search.js";
import {
    ADDRESS_ID,
    CAL_FREQ,
    CAL_FREQ_UNIT,
    CUSTOMER_DESCRIPTION,
    END_RANGE,
    INST_MODEL_NAME,
    ITEM_PO,
    MFR_INST_ID,
    MODEL_DELETED,
    PERSON_ID,
    PLANT_NO,
    PROCEDURE_ID,
    RANGE_UNIT,
    SERIAL_NO,
    SERVICE_TYPE,
    START_RANGE,
    WORKINSTRUCTION_ID
} from "../../../common/tools/constants.js";


export const template = context => html`
<div class="infobox" id="newjobmodels">
    <h5>
        <cwms-translate code="addnewitemstojob.newinstr" >New Instruments</cwms-translate> (<span
            class="newModCount">${context.activeModels?.length ??0}</span>)
        - <a class="mainlink" href="${jobLinkGenerator(context.jobId).toString()}">
        ${cwmsTranslateDirective("newjobitemsearch.backtojob","Back to Job {0}",context.jobNo ??"JOB NO")}
        </a>
    </h5>
    <table class="default4 newjobmodelhead">
        <thead>
            <tr>
                <td colspan="3" class="bold"><cwms-translate
                        code="addnewitemstojob.newinstr">New Instruments</cwms-translate>  (<span
                    class="newModCount">${context.activeModels?.length ??0}</span>)</td>
            </tr>
        </thead>
    </table>
    ${repeat(context.activeModels,m=>m.ordinal,renderNewInstrumentFormTemplate(context))}
    ${repeat(context.basketModels,m=>m.ordinal,renderNewInstrumentHiddenInputs(context))}
    ${autoprintLabelsTemplate(context)}
</div>
`

const renderNewInstrumentFormTemplate = context => (model,idx) => html`
    <cwms-newjobitem-model
                            .idx=${idx} .model=${model} .coid=${context.coid}
                            .subdivId=${context.subdivId}
                            .addresses=${context.addresses}
                            .contacts=${context.contacts}
                            .serviceTypes=${context.serviceTypes}
                            .jobId=${context.jobId}
                            .calFrequencyUnits=${context.calFrequencyUnits}
                            .rangeUnits=${context.rangeUnits}
                            .error=${context.errors?.[idx]}
    ></cwms-newjobitem-model>
`;

const renderNewInstrumentHiddenInputs = context => item => html`
        <input type="hidden" name="instDeletes[${item.ordinal}]" value="${item[MODEL_DELETED]??"false"}"/>
        <input type="hidden" name="basketIds[${item.ordinal}]" value="${item.modelId}" />
        <input type="hidden" name="basketParts[${item.ordinal}]" value="${item.part ??""}" />
        <input type="hidden" name="basketExistingGroupIds[${item.ordinal}]" value="${item.existingGroup??""}" />
        <input type="hidden" name="basketTempGroupIds[${item.ordinal}]" value="${item.tempGroup??""}" />
        <input type="hidden" name="basketTypes[${item.ordinal}]" value="model" />
        <input type="hidden" name="basketBaseItemIds[${item.ordinal}]" value="${item.baseItemId??""}" />
        <input type="hidden" name="basketQuotationItemIds[${item.ordinal}]" value="${item.quotationItemId??""}" /cwms->
        <input type="hidden" name="itemProcIds[${item.ordinal}]" value="${item[PROCEDURE_ID]??""}" />
        <input type="hidden" name="itemWorkInstIds[${item.ordinal}]" value="${item[WORKINSTRUCTION_ID]??""}" />
        <input type="hidden" name="serviceTypeIds[${item.ordinal}]" value="${item[SERVICE_TYPE]??""}" />
        <input type="hidden" name="instMfrIds[${item.ordinal}]" value="${item[MFR_INST_ID]??""}" />
        <input type="hidden" name="instPersonIds[${item.ordinal}]" value="${item[PERSON_ID]??context.contacts?.[0]?.key ??""}" />
        <input type="hidden" name="instCalFreq[${item.ordinal}]" value="${item[CAL_FREQ]??"12"}" />
        <input type="hidden" name="instCalUnitFreq[${item.ordinal}]" value="${item[CAL_FREQ_UNIT]??"MONTH"}" />
        <input type="hidden" name="instModelNames[${item.ordinal}]" value="${item[INST_MODEL_NAME]??""}" />
        <input type="hidden" name="instAddrIds[${item.ordinal}]" value="${item[ADDRESS_ID]??context.addresses?.[0]?.key ??""}" />
        <input type="hidden" name="itemPOs[${item.ordinal}]" value="${item[ITEM_PO]??""}" />
        <input type="hidden" name="instPlantNos[${item.ordinal}]" value="${item[PLANT_NO]??""}" />
        <input type="hidden" name="instSerialNos[${item.ordinal}]" value="${item[SERIAL_NO]??""}" />
        <input type="hidden" name="instCustomerDescs[${item.ordinal}]" value="${item[CUSTOMER_DESCRIPTION]??""}" />
        <input type="hidden" name="instStartRanges[${item.ordinal}]" value="${item[START_RANGE]??""}" />
        <input type="hidden" name="instEndRanges[${item.ordinal}]" value="${item[END_RANGE]??""}" />
        <input type="hidden" name="instUomIds[${item.ordinal}]" value="${item[RANGE_UNIT]??context.rangeUnits?.[0]?.key}" / >
`;


const autoprintLabelsTemplate = context => html`
						<div class="label-autoprint">
						<label><cwms-translate
									code="edituserpreferences.autoprintlabel">Automatic job item label printing</cwms-translate> </label>
						<input type="checkbox" value="auto print"
								id="autoPrintLabelCheckBoxId"
								?checked=${context.autoPrintLabels??false}
                                />
						</div>

`