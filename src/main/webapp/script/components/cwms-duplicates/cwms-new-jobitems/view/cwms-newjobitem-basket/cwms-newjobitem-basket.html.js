import {html} from "../../../../../thirdparty/lit-element/lit-element.js";
import {repeat} from "../../../../../thirdparty/lit-html/directives/repeat.js";
import {jobLinkGenerator} from "../../../../../tools/UrlGenerator.js";
import {cwmsTranslateDirective} from "../../../../cwms-translate/cwms-translate-directive.js";
import "../../../../cwms-tooltips/cwms-instrument-link-and-tooltip/cwms-instrument-link-and-tooltip.js";
import "../../../../search-plugins/cwms-modelcode-search/cwms-modelcode-search.js";
import "../../../../search-plugins/cwms-capability-search/cwms-capability-search.js";
import {PROCEDURE_ID, PROCEDURE_REFERENCE, SERVICE_TYPE, WORKINSTRUCTION_ID} from "../../../common/tools/constants.js";
import {
    copyUpDownArrowsTemplate,
    modelLinkTemplate,
    serviceTypeSourceTemplate,
    serviceTypesSelectTemplate
} from "../../../common/tools/templates.html.js";


export const template = context => html`
    <div class="infobox" id="newjobinsts">
        <h5>
            <cwms-translate code="addnewitemstojob.existinstr">Existing Instruments</cwms-translate>
                (${context.instrumentsCount ?? 0})
            - <a class="mainlink" href="${jobLinkGenerator(context.jobId).toString()}">
            ${cwmsTranslateDirective("newjobitemsearch.backtojob", "Back to Job {0}", context.jobNo ?? "JOB NO")}
        </a>
    </h5>
    
    <table class="default4 newjobinsts"
						summary="This table displays all instruments to be added to job">
        <thead>
            <tr>
                <td colspan="6">
                    <span class="bold">
                        <cwms-translate code="addnewitemstojob.existinstr">Existing Instruments</cwms-translate>  (${context.instrumentsCount ?? 0})
                    </span>
                </td>
            </tr>
        </thead>
    </table>
        <div class="newjobinstsbody">
            ${repeat(context.basketInstruments ?? [],i => i.instId,renderInstrumentBasketItem(context))}
        </div>
</div>
`;


const renderInstrumentBasketItem = context => (bi,indx) => html`
    <table class="default4 newjobinsts">
        <tbody class=${indx % 2 == 0? "even":"odd"}>
            <tr >
                <td colspan="2">
                    <cwms-instrument-link-and-tooltip plantid=${bi.instId}></cwms-instrument-link-and-tooltip>
                    ${modelLinkTemplate(bi)}
                </td>    
                <td class="plantNo-label"><span class="bold"><cwms-translate code="addnewitemstojob.plantNo" text="Plant No">Plant No</cwms-translate>:</span></td>
                <td class="plantNo-value">${bi.plantNo}</td>
                <td class="procedure-label">
                    <span class="bold"><cwms-translate code="jobresults.procedure">Capability</cwms-translate>:</span>
                </td>
                <td class="procedure-value">
                    <cwms-capability-search .subdivId=${context.subdivId} 
                                            .hideDeactivated=${true}
                    .defaultvalue=${bi.procReference}
                    @selected=${e => {
    context.updateFields(PROCEDURE_ID, bi)({currentTarget: {value: e.detail?.id}})
    context.updateFields(PROCEDURE_REFERENCE, bi)({currentTarget: {value: e.detail?.reference}})
}}></cwms-capability-search>
                    ${copyUpDownArrowsTemplate(context)([PROCEDURE_ID, PROCEDURE_REFERENCE], indx)(bi)()}
                </td>
                <td class="workinstruction-label">
                    <span class="bold"><cwms-translate code="updateallitems.workinstr">Work Instruction</cwms-translate>:</span>
                </td>
                <td class="workinstruction-value">
                    <div>
                        <cwms-modelcode-search @selected=${e=>{
                            context.updateFields(WORKINSTRUCTION_ID,bi)({currentTarget:{value:e.detail.id}})
                        }} modelid="${bi.modelId}" name=${`itemWorkInstIds[${bi.oridnal}]`} 
                        defaultvalue="${bi.workInstructionTitle ?? ""}" value="${bi.workInstructionId ?? ""}" ></cwms-modelcode-search>
                    </div>	
                    <!-- clear floats and restore page flow -->
                    <div class="clear"></div>
                </td>
            </tr>
            <tr>
                <td class="quot-label">
                    <span class="bold"><cwms-translate code="quotationitem">Quotation Item</cwms-translate>:</span>
                </td>
                <td class="quot-value">
                    ${bi.quotationItem.value}
                </td>
                <td class="serialNo-label"><span class="bold"><cwms-translate code="addnewitemstojob.serialNo">Serial No</cwms-translate>:</span></td>
                <td class="serialNo-value">${bi.serialNo}</td>
                <td class="servicetype-label">
                    <span class="bold"><cwms-translate code="addnewitemstojob.defaultservicetype">Default Service Type</cwms-translate>:</span>
                </td>
                <td class="servicetype-value">
                    ${serviceTypesSelectTemplate(context)(bi)}
                    ${copyUpDownArrowsTemplate(context)([SERVICE_TYPE],indx)(bi)()}
                    ${serviceTypeSourceTemplate(bi.serviceTypeSource ??0)}
                </td>
                <td class="calfreq-label">
                    <span class="bold"><cwms-translate
                        code="addnewitemstojob.calfreq">Cal Freq</cwms-translate> :</span>
                </td>
                <td class="calfreq-value">
                    ${calFrequencyTemplate(bi)}
                </td>
            </tr>
        </tbody>										
    </table>
`


const calFrequencyTemplate = item => 
html`${item.calFrequency??12} ${item.calFrequency>1 ? item.calFrequencyUnit?.plural??"Months":item.calFrequency?.singular??"Months"}`
