import {LitElement} from "../../../../../thirdparty/lit-element/lit-element.js";
import {linkStyling} from "../../../../tools/styles/links.css.js";
import {structureStyles} from "../../../../tools/styles/structure.css.js";
import {tableStyles} from "../../../../tools/styles/table.css.js";
import {textStyles} from "../../../../tools/styles/text.css.js";
import {moveItemToBasket, storeBasketModels} from "../../store/actions.js";
import {styling} from "./cwms-newjobitem-model.css.js";
import {template} from "./cwms-newjobitem-model.html.js";
import CwmsTranslate from "../../../../cwms-translate/cwms-translate.js";
import store from "../../store/store.js";
import {
    MODEL_DELETED,
    PLANT_NO,
    SERIAL_NO,
    SIPLIFIED_PLANT_NO,
    SIPLIFIED_SERIAL_NO
} from "../../../common/tools/constants.js";
import {duplicateCheckService, SimpleInstrumentDto} from "../../../logic/duplicateCheckService.js";
import {debounce} from "../../../../../tools/Debounce.m.js";

class CwmsNewJobItemModel extends LitElement {

    #model;
    #oldPlantNo;
    #oldSerialNo;
    #error;


    static get properties() {
        return {
            companyInstruments:{type:Array},
            residualGroupInstruments:{type:Array},
            groupName:{type:String},
            coid:{type:Number},
            idx:{type:Number},
            model:{type:Object},
            jobId:{type:Number},
            jobNo:{type:String},
            subdivId:{type:Number},
            contacts:{type:Array},
            addresses:{type:Array},
            rangeUnits:{type:Array},
            calFrequencyUnits:{type:Array},
            serviceTypes:{type:Array},
            basketModels:{type:Array},
            showDuplicatePlantNoWarning:{type:Boolean},
            showSimilarNumbersWarning:{type:Boolean},
            error:{type:Array}
        }
    }

    static get styles(){
        return [styling(),structureStyles, tableStyles, textStyles, linkStyling()];
    }

    get serialNo(){
        return this.shadowRoot.querySelector("[name='serialNo']");
    }

    get plantNo(){
        return this.shadowRoot.querySelector("[name='plantNo']");
    }

    get model(){
        return this.#model;
    }

    set model(model){
        const old = this.#model;
        this.#model = model;
        this.checkForSamePlantAndSerialNumbersInOtherModels(this.basketModels);
        if (this.coid) this.checkDuplicatesForInstrument(new SimpleInstrumentDto(this.coid, model.plantNo, model.serialNo, model.modelId));
        this.requestUpdate("model",old);
    }

    get error(){
        return this.#error;
    }

    set error(error) {
        const oldValue=this.error;
        this.#error = error;
        error?.map(e => {
            switch(e.fieldname){
                case "instSerialNos":
                    CwmsTranslate.messagePromise(e.messageCode,e.defaultMessage)
                    .then(msg => this.serialNo.setCustomValidity(msg));
                 break;
                case "instPlantNos":
                    CwmsTranslate.messagePromise(e.messageCode,e.defaultMessage)
                    .then(msg => this.plantNo.setCustomValidity(msg));

            }
        } );
        this.requestUpdate("error",oldValue);
    }

    constructor(){
        super();
        CwmsTranslate.messagePromise("addnewitemstojob.confirmremovenewitem","Are you sure you wish to delete this new job item?").then(msg => this.removalConfirmationMessage = msg);
        this.basketModels = [];
        store.subscribe(()=>{
            const {models} = store.getState();
            this.basketModels = models;
            this.checkForSamePlantAndSerialNumbersInOtherModels(models)
        })
        this.checkDuplicatesForInstrument = debounce(this.performDuplicateChecking,500);
    }

    copyField(fieldsNames,direction,index, copyPredicateFucntion ){
        const copyPredicate = copyPredicateFucntion ?? (_ => true);
        const predicate = (idx) => direction === "up" ? idx < index : direction === "down" ? idx > index : false;
        const updateItem = (item, refenceObject) => fieldsNames.reduce((acc, f) => ({
            ...acc,
            [f]: refenceObject[f]
        }), item);
        return value => {
            const models = this.basketModels.map((i,idx)=> copyPredicate(i) && predicate(idx)?updateItem(i,value):i);
            this.shadowRoot.querySelector(`[name='${fieldsNames[0]}']`)?.focus();
            storeBasketModels(models);
        }
    }

    performDuplicateChecking(model){
        if (this.#oldPlantNo === model.plantNo && this.#oldSerialNo === model.serialNo)
            return;
        this.#oldPlantNo = model.plantNo;
        this.#oldSerialNo = model.serialNo;
        duplicateCheckService.checkDuplicatesForInstrument(model)
        .then(res => {
            this.companyInstruments = res.companyInstruments;
            this.residualGroupInstruments = res.residualGroupInstruments;
            this.groupName = res.groupName;
            if(this.hasPerfectMatch){
                this.classList.add("cwms-duplicated-with-db-error");
            } else {
                this.classList.remove("cwms-duplicated-with-db-error");
            }
        })
        .catch(_ => {
            this.companyInstruments = [];
            this.residualGroupInstruments = [];
            this.classList.remove("cwms-duplicated-with-db-error");
        })
    }

    updateFields(fieldName,currentItem){
        return e => {
            let updatedItem;
            switch (fieldName) {
                case PLANT_NO:
                case SERIAL_NO:
                    const id = e.currentTarget.value;
                    const siplyfiedId = fieldName === PLANT_NO ? SIPLIFIED_PLANT_NO : SIPLIFIED_SERIAL_NO;
                    updatedItem = {
                        ...currentItem, [fieldName]: id
                        , [siplyfiedId]: duplicateCheckService.simplifyId(id)
                    };
                    this.checkDuplicatesForInstrument(new SimpleInstrumentDto(this.coid, updatedItem[PLANT_NO], updatedItem[SERIAL_NO], updatedItem.modelId));
                    break;
                default:
                    updatedItem = {...currentItem, [fieldName]: e.currentTarget.value};
                    break;
            }
            storeBasketModels(this.basketModels.map(i => i.ordinal === currentItem.ordinal ? updatedItem : i));
        };
    }

    get hasWarning(){
        return this.companyInstruments?.length >0 || this.residualGroupInstruments?.length > 0
    }   

    get hasPerfectMatch(){
        return this.companyInstruments?.find(i =>
                i.coid === this.coid &&
                i.serialNo === this.model.serialNo &&
                i.plantNo === this.model.plantNo
            )
    }

    confirmModelRemoval(model){
        if (confirm(this.removalConfirmationMessage) === true)
            this.updateFields(MODEL_DELETED, model)({currentTarget: {value: true}});
    }

    checkIfOtherModelHasSameNumbers(basketModels){
        if(!(this.model && (this.model.plantNo || this.model.serialNo)))
            return false;
        return basketModels?.find(i =>
            i.plantNo === this.model?.plantNo &&
            i.serialNo === this.model?.serialNo &&
            i.ordinal !== this.model?.ordinal)
    }

    checkIfOtherHasSimilarNumbers(basketModels) {
        if (!(this.model && (this.model.plantNo || this.model.serialNo)))
            return false;
        return basketModels?.find(i =>
            ((i.plantNo && i.simplifiedPlantNo === this.model?.simplifiedPlantNo) ||
                (i.serialNo && i.simplifiedSerialNo === this.model?.simplifiedSerialNo)) &&
            i.ordinal !== this.model?.ordinal
        )
    }

    checkForSamePlantAndSerialNumbersInOtherModels(basketModels){
        if(this.checkIfOtherModelHasSameNumbers(basketModels)){
            this.plantNo?.setCustomValidity("error");
            this.serialNo?.setCustomValidity("error");
            this.showDuplicatePlantNoWarning =true;
            this.classList.add("cwms-duplicated-with-same-plant-error")
        }
        else {
            this.plantNo?.setCustomValidity("");
            this.serialNo?.setCustomValidity("");
            this.showDuplicatePlantNoWarning = false;
            this.classList.remove("cwms-duplicated-with-same-plant-error")
        }
        this.showSimilarNumbersWarning = !!this.checkIfOtherHasSimilarNumbers(basketModels);
    }

    acceptInstrument(model){
        return instrument => {
            const {plantNo, serialNo, plantId:instId, description, calFrequencyUnits, calFrequency} = instrument;
            const newInstrument = {...model, instId, serialNo, plantNo,
                 modelName:description??model.modelName,
                calFrequency, calFrequencyUnits}
            moveItemToBasket(newInstrument);
        }
    }

    render(){
       return template(this)(this.model,this.idx);
    }

}


customElements.define("cwms-newjobitem-model",CwmsNewJobItemModel);