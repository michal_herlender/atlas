import { css } from '../../../../../thirdparty/lit-element/lit-element.js';

export const styling = (context) => css`


:not(fieldset:focus-within) cwms-duplicates-list {
    display:none;
}


fieldset {
    position: relative;
    border: 0;
    border-top: 1px solid #666;
    background: var(--cwms-main-gray-background,#dedede);
    margin: 10px 0 10px;
    padding-bottom: 10px;
    padding-top: 4px;
}

.delete-icon {
    position: absolute;
    top: 1rem;
    right: 1rem;
}

table {
    font-family:arial,helvetica,sans-serif;
}

.procedure-label, .workinstruction-label, .mfr-label {
    width: 8%;
}

.customer-description-value input {
    width: 93%;
}

select {
    width: 82%;
}

.calFreq-value select, .range-value select {
    width: auto;
}

.mfr-value {
    width:21%;
}

.procedure-value {
    width:32%;
}
cwms-capability-search {
    width: 330px;
}

input:invalid {
    border: solid 1px red;
}

`;