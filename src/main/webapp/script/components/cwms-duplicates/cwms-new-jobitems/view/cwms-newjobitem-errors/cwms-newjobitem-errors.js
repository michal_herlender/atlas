import { LitElement } from "../../../../../thirdparty/lit-element/lit-element.js";
import { storeErrors } from "../../store/actions.js";
import { textStyles } from "../../../../tools/styles/text.css.js";
import { styling } from "./cwms-newjobitem-errors.css.js";
import { template } from "./cwms-newjobitem-errors.html.js";

class CwmsNewJobitemErrors extends LitElement {

    #errors;

    static get styles(){
        return [styling(), textStyles];
    }

    static get properties(){
        return {
            errors:{type:Array}
        }
    }

    get errors(){
        return this.#errors;
    }

    set errors(errors){
        const oldValue = this.errors;
        this.#errors = errors;
        storeErrors(errors);
        this.requestUpdate("errors",oldValue);

    }

    render(){
       return template(this);
    }

}


customElements.define("cwms-newjobitem-errors",CwmsNewJobitemErrors);
