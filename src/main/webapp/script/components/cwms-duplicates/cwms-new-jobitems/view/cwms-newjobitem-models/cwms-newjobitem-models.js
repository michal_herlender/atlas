import { LitElement } from "../../../../../thirdparty/lit-element/lit-element.js";
import store from "../../store/store.js";
import { MODEL_DELETED } from "../../../common/tools/constants.js";
import { template } from "./cwms-newjobitem-models.html.js";

class CwmsNewJobItemModels extends LitElement {

    static get properties(){
        return {
            coid:{type:Number},
            jobId:{type:Number},
            jobNo:{type:String},
            subdivId:{type:Number},
            contacts:{type:Array},
            addresses:{type:Array},
            rangeUnits:{type:Array},
            calFrequencyUnits:{type:Array},
            serviceTypes:{type:Array},
            basketModels:{type:Array},
            errors:{type:Object},
            autoPrintLabels:{type:Boolean,
                converter: (value, type) => (value == "true" ? true : false)}
        };
    }

    get activeModels(){
        return this.basketModels?.filter(m=> !m[MODEL_DELETED]) ?? [];
    }

    constructor(){
        super();
        this.basketModels = [];
        store.subscribe(()=>{
            const {models,errors} = store.getState();
            this.basketModels = models;
            this.errors = errors;
        })
    }

    createRenderRoot(){
        return this;
    }


    firstUpdated(changedProperties){
        const autoPrint = this.querySelector(".label-autoprint");
        autoPrint.style.display="flex";
        autoPrint.style.flexDirection="row-reverse";
    }


    render(){
       return template(this);
    }

}


customElements.define("cwms-newjobitem-models",CwmsNewJobItemModels);