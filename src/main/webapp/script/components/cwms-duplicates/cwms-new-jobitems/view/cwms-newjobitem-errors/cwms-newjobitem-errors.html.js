import { html } from "../../../../../thirdparty/lit-element/lit-element.js";
import { cwmsTranslateDirective } from "../../../../cwms-translate/cwms-translate-directive.js";

export const template = (context) => context.errors? html`
    <div class="warningBox1">
      <div class="warningBox2">
        <div class="warningBox3">
          ${context.errors.map(e => html`<p>${cwmsTranslateDirective("model","Model")}[${e.idx}]: <cwms-translate code=${e.code}>${e.defaultMessage}</cwms-translate></p>`)}
        </div></div>
    </div>
`:html``;