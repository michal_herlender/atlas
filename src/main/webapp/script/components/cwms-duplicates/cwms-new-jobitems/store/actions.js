import store from "./store.js";
import { STORE_BASKET_INSTRUMENTS, STORE_BASKET_MODELS, STORE_MODELS_AND_INSTRUMENTS, STORE_ERRORS } from "./actionsTypes.js";


export function storeBasketInstruments(items){
    store.send({type:STORE_BASKET_INSTRUMENTS,items});
}

export function storeBasketModels(items){
    store.send({type:STORE_BASKET_MODELS,items});
}

export function storeBasketItems(items){
    const groupped = items.reduce((acc,it,ordinal) => it.instId ? {...acc, instruments:[...acc.instruments,{...it,ordinal}]}:
    {...acc,models:[...acc.models,{...it,ordinal}]},{instruments:[],models:[]});
    store.send({type:STORE_MODELS_AND_INSTRUMENTS,items:{models:groupped.models,instruments:groupped.instruments}});
}

export function moveItemToBasket(item){
    const { instruments,  models} = store.getState();
    store.send({type:STORE_MODELS_AND_INSTRUMENTS,items:{
            models:models.filter(i => i.ordinal != item.ordinal),
            instruments:[...instruments, item]
    }});
}


export function storeErrors(errors){
    const errorsMap = errors.reduce((acc,error)=>{
        const errorElements = acc[error.idx] ?? [];
        acc[error.idx] = [...errorElements,error];
        return acc;
    },{});
    store.send({type:STORE_ERRORS,errors:errorsMap});
}
