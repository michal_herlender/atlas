import { STORE_BASKET_INSTRUMENTS, STORE_BASKET_MODELS, STORE_MODELS_AND_INSTRUMENTS, STORE_ERRORS } from "./actionsTypes.js";

let dispatch; export default dispatch = function(state,command){
    switch(command.type){
        case STORE_BASKET_INSTRUMENTS: 
            return {...state, instruments:command.items};
        case STORE_BASKET_MODELS: 
            return {...state, models:command.items};
        case STORE_MODELS_AND_INSTRUMENTS:
            return {...state, models:command.items.models, instruments:command.items.instruments}
        case STORE_ERRORS:
            return {...state, errors:command.errors};
        default: return state;
    }
}