import { LitElement } from "../../../../thirdparty/lit-element/lit-element.js";
import { tableStyles } from "../../../tools/styles/table.css.js";
import { textStyles } from "../../../tools/styles/text.css.js";
import { styling } from "./cwms-duplicates-list.css.js";
import { template } from "./cwms-duplicates-list.html.js";

class CwmsDuplicatesList extends LitElement {

    static get properties(){
        return {
            companyInstruments:{type:Array},
            residualGroupInstruments:{type:Array}
        }
    }

    static get styles(){
        return [styling,tableStyles, textStyles];
    }

    constructor(){
        super();
        this.companyInstruments = [];
        this.residualGroupInstruments = [];
    }

    render(){
       return template(this);
    }

}


customElements.define("cwms-duplicates-list",CwmsDuplicatesList);