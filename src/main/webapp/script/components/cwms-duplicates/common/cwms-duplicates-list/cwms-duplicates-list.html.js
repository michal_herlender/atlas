import { html } from "../../../../thirdparty/lit-element/lit-element.js";
import {repeat} from "../../../../thirdparty/lit-html/directives/repeat.js";
import { cwmsTranslateDirective } from "../../../cwms-translate/cwms-translate-directive.js";
import "../../../cwms-tooltips/cwms-instrument-link-and-tooltip/cwms-instrument-link-and-tooltip.js"
import { dateFormatDirective, percentFormatDirective } from "../../../tools/formatingDirectives.js";

export const template = context => html`
		<div>
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
                        <cwms-translate code="addnewitemstojob.warnduplicateinstrfound">
                            Instrument you are trying to add have potential duplicates, please review before adding!
                        </cwms-translate>
					</div>
				</div>
			</div>
            ${duplicatesTableTemplate(context)}
        </div>

`;

const duplicatesTableTemplate = context => html`
<table class="default4" summary="This table any potential duplicates for instrument">
				<thead>
					<tr>
					<td colspan="12">
                        ${cwmsTranslateDirective("addnewitemstojob.detailduplicateinstrfound","There are at least {0} instrument(s) with similar serial numbers and/or plant numbers in the company",context.companyInstruments.length)}
					</td>
					</tr>
					<tr>
						<th class="bar" scope="col"><cwms-translate code="addnewitemstojob.barcode">Barcode</cwms-translate></th>
						<th class="com" scope="col"><cwms-translate code="addnewitemstojob.companyName">Company Name</cwms-translate></th>
						<th class="sub" scope="col"><cwms-translate code="addnewitemstojob.subdivision" >Subdivision</cwms-translate></th>
						<th class="inst" scope="col"><cwms-translate code="addnewitemstojob.instrument">Instrument</cwms-translate></th>
						<th class="serial" scope="col"><cwms-translate code="addnewitemstojob.serialNo">Serial No</cwms-translate></th>
						<th class="plant" scope="col"><cwms-translate code="addnewitemstojob.plantNo">Plat No</cwms-translate></th>
						<th class="barcode" scope="col"><cwms-translate code="addnewitemstojob.formerBarcode">Former Barcode</cwms-translate> </th>
						<th class="lastJob" scope="col"><cwms-translate code="addnewitemstojob.lastJob">Last job in date</cwms-translate></th>
						<th class="lastCal" scope="col"><cwms-translate code="addnewitemstojob.lastCal">Last calibration date</cwms-translate></th>
						<th class="nextCal" scope="col"><cwms-translate code="addnewitemstojob.nexCal">Next calibration date</cwms-translate></th>
						<th class="similarity" scope="col"><cwms-translate code="addnewitemstojob.similarity">Similarity</cwms-translate></th>
						<th class="accept" scope="col"><cwms-translate code="addnewitemstojob.accept">Accept that suggestion</cwms-translate></th>
					</tr>
				</thead>
				<tbody>
                    ${repeat(context.companyInstruments,i => i.plantId, instrumentRowTemplate(context))}
                    ${residualGroupInstrumentsTemplate(context)}
				</tbody>
			</table>
`

const instrumentRowTemplate = context => instrument => html`
			<tr>
				<td>
                <cwms-instrument-link-and-tooltip plantid=${instrument.plantId} ></cwms-instrument-link-and-tooltip>
            </td>
				<td>${instrument.companyName}</td>
				<td>${instrument.subdivName}</td>
				<td>${instrument.description}</td>
				<td>${instrument.serialNo}</td>
				<td>${instrument.plantNo ||''}</td>
				<td>${instrument.formerBarcode ||''}</td>
				<td>${dateFormatDirective(instrument.lastJobInDate)}</td>
				<td>${dateFormatDirective(instrument.lastCalibrationDate)}</td>
				<td>${dateFormatDirective(instrument.nextCalibrationDate)}</td>
				<td>${percentFormatDirective(instrument.score)}</td>
				<td class="accept" >${instrument.isActive?html`<cwms-translate code="addjob.onanotherjob">On Another Job</cwms-translate>`:instrument.isOnJob?html`
                    <cwms-translate code="newjobitemsearch.onjob">On Job</cwms-translate>`:html`
                     <button class="acceptInstrument" @click=${_ => context.acceptSuggestion?.(instrument)}>Accept</button>
                     `}
				</td>
			</tr>
`;

const residualGroupInstrumentsTemplate = context => context.residualGroupInstruments.length > 0? html`
    <tr><th colspan="12"><cwms-translate code="addnewitemstojob.topSimilarInGroup">
        Top most similar instruments in the group (excluding current company instruments)
    </cwms-translate>  </th></tr>
    ${repeat(context.residualGroupInstruments,i => i.plantId,instrumentRowTemplate(context))}
`:html``;