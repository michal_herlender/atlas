import { css } from "../../../../thirdparty/lit-element/lit-element.js";

export const styling = css`
    :host {
        display:block;
        margin:10px;
    }

    .accept {
        text-align:center;
    }

    table {
        font-family: arial,helvetica,sans-serif;
    }

    .bar {
        width: 7%;
    }

    .com {
        width:15%;
    }

    .sub {
        width: 8%;
    }
`;
