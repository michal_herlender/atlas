
export const SERVICE_TYPE="serviceTypeId"
export const PROCEDURE_ID="procId";
export const PROCEDURE_REFERENCE="procReference";
export const WORKINSTRUCTION_ID="workInstructionId";

export const INST_MODEL_NAME="instModelName";
export const PLANT_NO = "plantNo";
export const SIPLIFIED_PLANT_NO = "simplifiedPlantNo";
export const SERIAL_NO = "serialNo"
export const SIPLIFIED_SERIAL_NO = "simplifiedSerialNo";
export const CUSTOMER_DESCRIPTION = "customerDescription";
export const MFR_INST_ID="mfrInstId"
export const MFR_INST_NAME="mfrInstName"
export const PERSON_ID="instPersonId"
export const ADDRESS_ID="instAddressId"
export const CAL_FREQ="calFrequency"
export const CAL_FREQ_UNIT="calFrequencyUnit"
export const ITEM_PO="itemPO"
export const ITEM_PO_NAME="itemPOName"
export const START_RANGE="instStartRange"
export const END_RANGE="instEndRange"
export const RANGE_UNIT="instUomId"
export const MODEL_DELETED="instDelete";
