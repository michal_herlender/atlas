import { html } from "../../../../thirdparty/lit-element/lit-element.js";
import { repeat } from "../../../../thirdparty/lit-html/directives/repeat.js";
import { instrumentModelLinkGenerator } from "../../../../tools/UrlGenerator.js";
import { SERVICE_TYPE } from "./constants.js";


export const modelLinkTemplate = (data) => html`
<a class="mainlink" target="_blank" href=${instrumentModelLinkGenerator(data.modelId).toString()}>${data.modelName}</a>
`;

export const copyUpDownArrowsTemplate = context => (fieldsNames,indx) => value => copyPredicate =>  html`
    <a role="button" class="imagelink" @click=${_ => context.copyField(fieldsNames,"down",indx,copyPredicate)(value)} >
        <img src="img/icons/arrow_down.png" width="10" height="16" alt="Arrow down" title="Arrow down">
    </a>
    <a role="button" class="imagelink" @click=${_ => context.copyField(fieldsNames,"up",indx,copyPredicate)(value)} >
        <img src="img/icons/arrow_up.png" width="10" height="16" alt="Arrow Up" title="Arrow Up">
    </a>
`;

export const serviceTypeSourceTemplate = serviceTypeSource  => {switch(serviceTypeSource){
        case 1:
            return html`<span class="green"><cwms-translate code="viewjob.quickitems.defaultservicetype.quote">Default from quote</cwms-translate></span>` ;
        case 2:
            return html`<span class="green"><cwms-translate code="viewjob.quickitems.defaultservicetype.inst">Default from instrument</cwms-translate></span>` ;
        case 3:
            return html`<span class="green"><cwms-translate code="viewjob.quickitems.defaultservicetype.job">Default from job</cwms-translate></span>` ;
        default:
             return html``;
}}
export const serviceTypesSelectTemplate = context => item => context.serviceTypes?.length ? html`
    <select @change=${context.updateFields(SERVICE_TYPE,item)}>
        ${repeat(context.serviceTypes ?? [],st => st.key, 
            st => html`<option value=${st.key} ?selected=${st.key==item.serviceTypeId?true:false} >${st.value}</option>`)}
    </select>
`:html``;