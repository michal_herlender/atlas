import {
  LitElement,
  html,
} from "../../../thirdparty/lit-element/lit-element.js";
import { template } from "./cwms-email-search.html.js";
import { debounce } from "../../../tools/Debounce.m.js";
import CwmsTranslate from "../../cwms-translate/cwms-translate.js";
import { styling } from "./cwms-email-search.css.js";
import { urlWithParamsGenerator } from "../../../tools/UrlGenerator.js";

class EmailSearch extends LitElement {

  #comboBox;

  static get properties() {
    return {
      namePrefix: { type: String },
      coIds: { type: Array },
      emailList: { type: Array },
      viewAllMessage: { type: String, attribute: false },
      removeEmailMessage: { type: String, attribute: false },
      showAddCustom: { type: Boolean },
    };
  }

  get comboBox() {
    if (this.#comboBox) return this.#comboBox;
    this.#comboBox = this.shadowRoot.querySelector("cwms-combo-box");
    return this.#comboBox;
  }

  static get styles() {
    return styling;
  }

  constructor() {
    super();
    this.searchEmails = debounce(this.searchEmails.bind(this), 500);
    this.coIds = [];
    this.emailList = [];
    this.showAddCustom = false;
    this.namePrefix = "to";

    CwmsTranslate.subscribeMessage(
      "searchplugins.viewAll2",
      "View All",
      (m) => (this.viewAllMessage = m)
    );
    CwmsTranslate.subscribeMessage(
      "searchplugins.removeEmail2",
      "Remove Email",
      (m) => (this.removeEmailMessage = m)
    );
  }

  connectedCallback() {
    super.connectedCallback();
    if (!this.emailList instanceof Array) this.emailList = [];
  }

  searchAll() {
    this.searchEmails();
  }

  searchEmails(name) {
    const url = urlWithParamsGenerator("searchEmails.json",{coIds:this.coIds,name})
    fetch(url)
      .then((r) => (r.ok ? r.json() : Promise.reject(r.statusText)))
      .then((r) => (r.right ? r.value : Promise.reject(r.value)))
      .then((v) => {
        this.comboBox.items = v;
        this.requestUpdate();
      })
      .catch(console.warn);
  }

  onKeyUpHandler(event) {
    switch (event.key) {
      case "Escape":
      case "Alt":
      case "Shift":
      case "ArrowLeft":
      case "ArrowRight":
      case "ArrowDown":
      case "ArrowUp":
      case "Control":
      case "Enter":
        return;
    }

    const name = event.target.value;
    if (name) {
      this.showAddCustom = true;
      this.searchEmails(name);
    } else {
      this.showAddCustom = false;
      this.comboBox.items = [];
    }
  }

  renderSearchAll() {
    return this.coIds 
      ? html`
          <img
            src="img/icons/searchplugin_all.png"
            width="16"
            height="16"
            alt="${this.viewAllMessage}"
            title="${this.viewAllMessage}"
            @click=${this.searchAll.bind(this)}
          />
        `
      : html``;
  }

  renderEmail(person, idx) {
    const { personid, name, email } = person;
    const text =
      personid && personid > 0 ? `'${name}' - '${email}'` : `'${email}'`;
    return html`
      <div class="email">
        <span>
          <a
            ><img
              src="img/icons/bullet_delete.png"
              height="7"
              width="7"
              alt="${this.removeEmailMessage}"
              @click=${(_) => this.removeEmail(person, idx)}
              title="${this.removeEmailMessage}" /></a
          >${text}</span
        >
      </div>
    `;
  }

  removeEmail(item, idx) {
    this.emailList = this.emailList.filter((_, i) => i != idx);
    this.dispatchEvent(
      new CustomEvent("deselected", {
        detail: { item, idx },
        bubbles: true,
        composed: true,
      })
    );
  }

  itemRenderer(person) {
    if(!person) return;
    const text = Array.of(person.name??"",person.email??"").filter(s =>s.length>0).join(" - ");
    return text;
  }

  itemSelected(event) {
    this.emailList = [...this.emailList, event.detail];
    setTimeout(() => {
      this.comboBox.value  ="";
    }, 0);
  }

  renderAddCustomContact() {
    return this.showAddCustom &&
      this.comboBox.value &&
      this.comboBox.items.length == 0
      ? html`<button @click=${this.addCustomContact}>
          <cwms-translate code="email.add">Add</cwms-translate>
        </button>`
      : html``;
  }

  addCustomContact(event) {
    const email = this.comboBox.value;
    if (email) {
      const item = { email };
      this.emailList = [...this.emailList, item];
      this.dispatchEvent(
        new CustomEvent("selected", {
          detail: item,
          bubbles: true,
          composed: true,
        })
      );
      this.comboBox.value = "";
      this.requestUpdate();
    }
  }

  render() {
    return template({
      emailList: this.emailList.map(this.renderEmail.bind(this)),
      renderSearchAll: this.renderSearchAll.bind(this),
      renderAddCustomContact: this.renderAddCustomContact.bind(this),
      onKeyUp: this.onKeyUpHandler.bind(this),
      itemSelected: this.itemSelected,
      itemRenderer: this.itemRenderer,
    });
  }
}

customElements.define("cwms-email-search-inner", EmailSearch);

class CwmsEmailSearchWrapper extends LitElement {
  static get properties() {
    return {
      namePrefix: { type: String },
      coIds: { type: Array },
      emailList: { type: Array },
    };
  }

  constructor() {
    super();
    this.emailList = [];
    this.namePrefix = "to";
  }

  createRenderRoot() {
    return this;
  }

  renderEmails() {
    return this.emailList.map(this.renderEmail.bind(this));
  }

  itemSelected(event) {
    const detail = event.detail
    if(detail.constructor == String)
      this.emailList = [...this.emailList, {email:detail}];
    else
      this.emailList = [...this.emailList, event.detail];
  }

  itemDeselected(event) {
    const { item, idx } = event.detail;
    this.emailList = this.emailList.filter((_, i) => i != idx);
  }

  renderEmail(item) {
    return item.personid && item.personid > 0
      ? html`<input
          type="hidden"
          name="${`${this.namePrefix}Ids`}"
          value="${item.personid}"
        />`
      : html`<input
          type="hidden"
          name="${`${this.namePrefix}Emails`}"
          value="${item.email}"
        />`;
  }

  render() {
    return html`
      <cwms-email-search-inner
        namePrefix=${this.namePrefix}
        .coIds=${this.coIds}
        .emailList=${this.emailList}
        @selected=${this.itemSelected}
        @deselected=${this.itemDeselected}
      ></cwms-email-search-inner>
      ${this.renderEmails()}
    `;
  }
}

customElements.define("cwms-email-search", CwmsEmailSearchWrapper);
