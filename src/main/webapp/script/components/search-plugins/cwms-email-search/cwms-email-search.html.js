import { html } from "../../../thirdparty/lit-element/lit-element.js";
import "../../cwms-combo-box/cwms-combo-box.js";

export const template = (context) => html`
  <div class="container">
    <div class="input">
      <cwms-combo-box
        .allowFreeText=${true}
        class="input"
        .showSelected=${false}
        @keyup=${context.onKeyUp}
        .itemRenderer=${context.itemRenderer}
        @selected=${context.itemSelected}
      ></cwms-combo-box>
      ${context.renderSearchAll()}
      ${context.renderAddCustomContact()}
    </div>
    <div class="emailList">${context.emailList}</div>
  </div>
`;
