import { css } from '../../../thirdparty/lit-element/lit-element.js'

export const styling = css`
 .container {
    display: flex;
    flex-direction: column;
}
 .container .input {
    display: flex;
    flex-direction: row;
}
 .container div .input {
    width: 100%;
}

.emailList {
    display:flex;
    flex-direction:column;
}

.email {
    background-color: #FFF;
    border: 1px solid #999;
    box-sizing:border-box;
    width: 100%; 
    margin-top: 2px;
}

.email a {
    cursor:pointer;
}
.email span {
     display: block; 
     margin: 0; 
     padding: 0; 
     font-size: 90%; 
     line-height: 1.4; 
     border: 1px solid transparent;
}

.email img {
     margin: 0 8px 0 5px; 
}
`;