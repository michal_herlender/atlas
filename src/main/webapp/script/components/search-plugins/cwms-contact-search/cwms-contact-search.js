import { MetaDataObtainer } from "../../../tools/metadataObtainer.js";
import { CwmsAbstractSearch } from "../cwms-abstract-search/cwms-abstract-search.js";

class CwmsContactSearch extends CwmsAbstractSearch {

    static get properties(){
        return {
            companyRole:{type:String},
        }
    }

    prepareUrl(contactName){
        const url = new URL("searchContacts.json",MetaDataObtainer.base);
        const searchParams = new URLSearchParams();
        searchParams.append("fullName",contactName);
        searchParams.append("roles",this.companyRole??"CLIENT");
        searchParams.append("active",true);
        url.search = searchParams;
        return url;
    }

    
    renderItem(contact){
        return contact.name;
    }
}


customElements.define("cwms-contact-search",CwmsContactSearch);
