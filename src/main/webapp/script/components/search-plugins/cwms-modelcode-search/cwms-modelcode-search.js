import { MetaDataObtainer } from "../../../tools/metadataObtainer.js";
import { CwmsAbstractSearch, wrappElementMixin} from "../cwms-abstract-search/cwms-abstract-search.js";
import { styling } from "./cwms-modelcode-search.css.js";

class CwmsModelCodeSearch extends CwmsAbstractSearch {

    static get properties(){
        return {
            ...CwmsAbstractSearch.properties,
            modelid: {type: Number},
            defaultvalue : {type: String}
        };
    }

    constructor(){
        super();
        this.showLoadAll = true;
    }
    
    prepareUrl(term){
        const url = new URL("searchmodelcodetags.json", MetaDataObtainer.base);
        const searchParams = new URLSearchParams();
        searchParams.append("term",term);
        if(this.modelid)  searchParams.append("modelid",this.modelid)
        url.search = searchParams;
        return url;
    }
    
    static get styles(){
        return [styling({}),super.styles];
    }
    
    renderItem(item){
        return item.value;
    }
   
}

customElements.define("cwms-modelcode-search",wrappElementMixin(CwmsModelCodeSearch));
customElements.define("cwms-modelcode", CwmsModelCodeSearch);


