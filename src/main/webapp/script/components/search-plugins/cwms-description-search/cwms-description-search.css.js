import { css } from "../../../thirdparty/lit-element/lit-element.js";

export const styling = (context) => css`

  cwms-combo-box {
   width: 280px;
   display:inline-block;
  }

  
  div.row {
    padding-top: 5px;
    padding-bottom: 5px;
    display: flex;
    flex-direction: row;
  }

`;
