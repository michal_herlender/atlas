import { MetaDataObtainer } from "../../../tools/metadataObtainer.js";
import { CwmsAbstractSearch, wrappElementMixin} from "../cwms-abstract-search/cwms-abstract-search.js";
import { styling } from "./cwms-description-search.css.js";

class CwmsDescriptionSearch extends CwmsAbstractSearch {

    static get properties(){
        return {
            ...CwmsAbstractSearch.properties,
            domaintype:{type:String},
            defaultvalue : {type: String}
        };
    }
    
    prepareUrl(term){
        const url = new URL("searchdescriptiontags.json",MetaDataObtainer.base);
        const searchParams = new URLSearchParams();
        searchParams.append("term",term);
        if(this.domaintype)  searchParams.append("domaintype",this.domaintype)
        url.search = searchParams;
        return url;
    }
    
    static get styles(){
        return styling({});
    }
    
    renderItem(item){
        return item.value;
    }
}

customElements.define("cwms-description-search", CwmsDescriptionSearch);

customElements.define("cwms-description-search-generic",wrappElementMixin( CwmsDescriptionSearch));


