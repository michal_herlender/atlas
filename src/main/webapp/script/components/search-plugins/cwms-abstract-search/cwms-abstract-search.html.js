import { html } from '../../../thirdparty/lit-element/lit-element.js'
import "../../cwms-combo-box/cwms-combo-box.js"

export const template = (context) => html`
        <cwms-combo-box .defaultvalue =${context.defaultvalue ?? ''} @blur=${context.onFocusOut} @keyup=${e => context.onKeyUp(e,e.target.value)} .itemRenderer=${context.renderItem} .valueRenderer=${context.renderValue}></cwms-combo-box>
        ${context.showLoadAll? html`
          <img
            src="img/icons/searchplugin_all.png"
            width="16"
            height="16"
            alt="View All"
            title="View All"
            @click=${context.searchAll}
          />
        `:html``}
`;