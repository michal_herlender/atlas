import { LitElement, html, css } from "../../../thirdparty/lit-element/lit-element.js";
import { template } from "./cwms-abstract-search.html.js";
import { debounce } from "../../../tools/Debounce.m.js";


export class CwmsAbstractSearch extends LitElement {

    static get properties(){
        return {
            defaultvalue : {type: String},
            showLoadAll:{type:Boolean}
        };
    }

    static get styles(){
        return css`
            :host{
                display:inline-flex;
            }
            cwms-combo-box {
                width: 100%;
                margin-right: 10px;
                padding-right: 6px;
            }
        `;
    }
    
    constructor(){
        super();
        this.showLoadAll = false;
    }

    get comboBox(){
        return this.shadowRoot.querySelector("cwms-combo-box");
    }


    prepareUrl(term){
        throw new Error("Unimplemented method prepareUrl");
    }

    isSupportedKey(key){
        const unsuportedKeys = ["Tab","Alt","Shift","Control","Meta","Escape","CapsLock","NumLock","Enter","ArrowDown","ArrowUp","ArrowRight","ArrowLeft","PageDown","PageUp"];
        return !(unsuportedKeys.indexOf(key) > -1);
    }

    clear(){
        this.comboBox.selectItem(undefined);
    }
    
    selectAll(){
    	var e = new Event('keypress', {keyCode: 'a'});
    	this.onKeyUp(e, '%')
    }

    onKeyUp(event,term){
        if(!term) {
            this.comboBox.selectItem(undefined);
            this.defaultvalue="";
            if(document.querySelector('cwms-modelcode-search')){
            	document.querySelector('cwms-modelcode-search').lastElementChild.value = '';
            }
            return;
        }
        if(this.isSupportedKey(event.key)) {
            fetch( this.prepareUrl(term))
            .then(r => r.ok? r.json():Promise.reject(r.status))
            .then(r => {
                this.comboBox.items = r.slice(0,20);
                this.requestUpdate();
            }
            )
            .catch(console.error);
        }
    }

    renderItem(item){
        throw new Error("Unimplemented method renderItem");
    }
    
    render(){
       return template({
        onKeyUp: debounce(this.onKeyUp.bind(this),500),
        renderItem:this.renderItem,
        renderValue:this.renderValue,
        onFocusOut:_ => this.comboBox.showItems=false,
        defaultvalue: this.defaultvalue,
        showLoadAll: this.showLoadAll,
        searchAll: this.selectAll
       });
    }

}

export const wrappElementMixin = Base => class extends Base {

    #internals;

    static get formAssociated(){
        return true;
    }

    get form(){
        return this.#internals.form;
    }

    static get properties(){
        return {...Base.properties,name:{type:String},value:{type:String}};
    }

    constructor(){
        super();
        this.#internals = this.attachInternals();
    }


        connectedCallback(){
            super.connectedCallback();
            this.addEventListener("selected",e => {
                console.log(e);
                if(e.detail){
                	 this.value = e.detail.id
                     this.#internals.setFormValue(this.value)
                }
                })
        }

}
