import { MetaDataObtainer } from "../../../tools/metadataObtainer.js";
import { CwmsAbstractSearch } from "../cwms-abstract-search/cwms-abstract-search.js";

class CwmsMfrSearch extends CwmsAbstractSearch {


    prepareUrl(term){
        const url = new URL("searchmanufacturertags.json",MetaDataObtainer.base);
        const searchParams = new URLSearchParams();
        searchParams.append("term",term);
        url.search = searchParams;
        return url;
    }
    
    renderItem(item){
        return item.label;
    }
}


customElements.define("cwms-mfr-search",CwmsMfrSearch);
