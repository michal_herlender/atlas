import {MetaDataObtainer} from "../../../tools/metadataObtainer.js";
import {urlWithParamsGenerator} from "../../../tools/UrlGenerator.js";
import {CwmsAbstractSearch, wrappElementMixin} from "../cwms-abstract-search/cwms-abstract-search.js";

class CwmsCapabilitySearch extends CwmsAbstractSearch {

    static get properties() {
        return {
            ...CwmsAbstractSearch.properties,
            subdivId: {type: Number},
            hideDeactivated: {type: Boolean, converter: (value, type) => (value === "true")}
        };
    }

    constructor(){
        super();
        this.subdivId = MetaDataObtainer.allocatedSubdivId ;
        this.showLoadAll = true;
        this.hideDeactivated = false;
    }

    prepareUrl(term) {
        return urlWithParamsGenerator("capability/findBySubdivAndTerm.json", {
            term, subdivId: this.subdivId,
            hideDeactivated: this.hideDeactivated
        });
    }
    
    renderItem(item){
        return `${item.reference} - ${item.name}` ;
    }

    renderValue(item){
        return item.reference
    }
}


customElements.define("cwms-capability-search",CwmsCapabilitySearch);
customElements.define("cwms-capability-search-form",wrappElementMixin(CwmsCapabilitySearch));
