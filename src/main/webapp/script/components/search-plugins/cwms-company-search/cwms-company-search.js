import { MetaDataObtainer } from "../../../tools/metadataObtainer.js";
import { CwmsAbstractSearch } from "../cwms-abstract-search/cwms-abstract-search.js";

class CwmsCompanySearch extends CwmsAbstractSearch {

    static get properties(){
        return {
            companyRole:{type:String},
        }
    }

    prepareUrl(companyName){
        const url = new URL("companysearchrequest.json",MetaDataObtainer.base);
        const searchParams = new URLSearchParams();
        searchParams.append("searchName",companyName);
        searchParams.append("companyRoles",this.companyRole??"CLIENT");
        searchParams.append("activeAnywhere",false);
        searchParams.append("searchEverywhere",false);
        url.search = searchParams;
        return url;
    }

    renderItem(company){
        return company.coname;
    }
}


customElements.define("cwms-company-search",CwmsCompanySearch);
