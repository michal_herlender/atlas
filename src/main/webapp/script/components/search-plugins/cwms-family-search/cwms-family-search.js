import { MetaDataObtainer } from "../../../tools/metadataObtainer.js";
import { CwmsAbstractSearch } from "../cwms-abstract-search/cwms-abstract-search.js";

class CwmsFamilySearch extends CwmsAbstractSearch {

    prepareUrl(term){
        const url = new URL("searchfamilytags.json",MetaDataObtainer.base);
        const searchParams = new URLSearchParams();
        searchParams.append("term",term);
        searchParams.append("domaintype","INSTRUMENTMODEL");
        url.search = searchParams;
        return url;
    }
    
    renderItem(item){
        return item.value;
    }
}


customElements.define("cwms-family-search",CwmsFamilySearch);
