import { html } from "../../../thirdparty/lit-element/lit-element.js";
import { MetaDataObtainer } from "../../../tools/metadataObtainer.js";
import { urlWithParamsGenerator } from "../../../tools/UrlGenerator.js";
import { cwmsTranslateDirective } from "../../cwms-translate/cwms-translate-directive.js";
import { CwmsAbstractSearch } from "../cwms-abstract-search/cwms-abstract-search.js";

class CwmsPOSearch extends CwmsAbstractSearch {

    static get properties(){
        return {
            ...super.properties,
            jobId:{type:Number}
        };
    }

    prepareUrl(term){
       return urlWithParamsGenerator("searchPoByJobIdAndNumber.json",{jobId:this.jobId,poNumber:term});
    }
    
    renderItem(item){
        return html`<span class="number">${item.poNumber}</span> <span><small><i>(${item.isBPO?cwmsTranslateDirective("invoice.bpo","BPO"):cwmsTranslateDirective("addjob.po","PO")})</i></small></span>`;
    }

    renderValue(item){
        return item.poNumber;
    }
}


customElements.define("cwms-po-search",CwmsPOSearch);
