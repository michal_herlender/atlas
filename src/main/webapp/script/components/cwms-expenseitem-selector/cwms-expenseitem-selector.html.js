import { html } from "../../thirdparty/lit-element/lit-element.js";
import { repeat } from "../../thirdparty/lit-html/directives/repeat.js";
import { directive } from "../../thirdparty/lit-html/lit-html.js";

export let htmlTemplate = (context) => context.expenseItems && context.expenseItems.length > 0 ? html`
<select  name=${context.path}>
    <option value="">N/A</option>
    ${context.expenseItems.map(item => html`
        <option value="${item.id}">
            <cwms-translate code="job"></cwms-translate>
            ${item.jobNo}
            <cwms-translate code="item"></cwms-translate>
            ${item.itemNo}
            -
            ${item.modelName}
            ${item.comment ? html` - ${item.comment}` : html``}
        </option>
    `)}
</select>
` :
html`No job services available`;