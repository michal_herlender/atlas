import { LitElement } from "../../thirdparty/lit-element/lit-element.js";
import { htmlTemplate } from "./cwms-expenseitem-selector.html.js";
import { styling } from "./cwms-expenseitem-selector.css.js";
import {} from "../cwms-translate/cwms-translate.js";

class CwmsExpenseItemSelector extends LitElement {

    static get expenseItemsForInvoiceURL() { return "expenseitems.json?invoiceid=_invoiceId"; }

    static get properties() {
        return {
            path: { type: String },
            invoiceId: { type: String },
            expenseItems: { type: Array }
        }
    }

    get styles() {
        return styling(this);
    }

    firstUpdated() {
        var url = CwmsExpenseItemSelector.expenseItemsForInvoiceURL.replace("_invoiceId", this.invoiceId);
        fetch(url)
        .then(res => {
            return res.json();
        })
        .then(dtos => {
            this.expenseItems = dtos;
        });
    }

    render() {
        return htmlTemplate(this);
    }
}

window.customElements.define('cwms-expenseitem-selector', CwmsExpenseItemSelector);