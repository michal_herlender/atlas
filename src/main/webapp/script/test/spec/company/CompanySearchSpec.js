describe("company:CompanySearch", function(){

	beforeEach(function(){
		COROLES=[];
		loadFixtures('CompanySearchFixture.html');
	})

	it("should be able to add a company role.(Supplier)", function() {
		addCorole('Supplier', 'array');
		expect(COROLES).toContain('Supplier');
	});

	it("should be able to remove a company role.(Supplier)", function() {
		addCorole('Supplier', 'array');
		expect(COROLES).toContain('Supplier');
		addCorole('Supplier', 'array');
		expect(COROLES).not.toContain('Supplier');
	});

	it("should be able to select a single company role.(Utility)", function() {
		addCorole('Utility', 'single');
		expect(COROLES).toEqual(['Utility']);
		expect($j('#Utility')).toBeChecked();
	});

	it("should be able to select ALL coroles", function() {
		addCorole('All', 'array');
		expect(COROLES).toEqual(ALLCOROLES);
		expect($j('#compCoroles input')).toBeChecked();
	});

	it("should default to Client company role only", function() {
		addCorole('All', 'array');
		$j('#All').attr('checked',false);
		addCorole('All', 'array');
		expect(COROLES).toEqual(['Client']);
		expect($j('#Client')).toBeChecked();
		expect($j('#compCoroles input:not(#Client)')).not.toBeChecked();
	});

	it("should return 2 companies from mock service when searching for 't'", function() {
		runCompanyAjax(false,'t');
		expect($j('#companyDiv a').size()).toEqual(2);
	})

	it("should return 'Test Company' as first company from mock service when searching for 't'", function() {
		runCompanyAjax(false,'t');
		expect($j('#companyDiv a:first span:first').text()).toEqual('Test Company');
	})

	it("should return 'Client' as first corole from mock service when searching for 't'", function() {
		runCompanyAjax(false,'t');
		expect($j('#companyDiv a:first span:last').text()).toEqual('Client');
	})

	it("should empty result list and display 'No Companies Found' message when searching for something that doesnt's exist", function() {
		runCompanyAjax(false,'tt');
		expect($j('#companyDiv > div').text()).toEqual('No Companies Found');
	})

	it("should empty result list when searching for nowt", function() {
		runCompanyAjax(false,'t');
		runCompanyAjax(false,'');
		expect($j('#companyDiv > div').text()).toEqual('');
	})

	it("should empty result list when form is cleared", function() {
		runCompanyAjax(false,'t');
		clearForm();
		expect($j('#companyDiv a').size()).toEqual(0);
	})

})
