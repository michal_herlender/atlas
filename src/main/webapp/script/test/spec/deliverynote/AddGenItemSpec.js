describe("deliverynote:AddGenItem", function(){


  it("original label should remain when a new item is added", function(){
  	loadFixtures('AddGenItemFixture.html');
    addItem();
    expect($j('#addgendelitems ol li:eq(1) label').length).toEqual(1);
  })

  it("original lable containing the phrase -Item 2:- should remain when a new item is added", function(){
  	loadFixtures('AddGenItemFixture.html');
    addItem();
    expect($j('#addgendelitems ol li:eq(1) label').text()).toEqual('Item 2:');
  })

  it("original anchor should remain when a new item is added", function(){
		loadFixtures('AddGenItemFixture.html');
    addItem();
    expect($j('#addgendelitems ol li:eq(1) a').length).toEqual(1);
  })

  it("should add a new label when a new item is added", function(){
    loadFixtures('AddGenItemFixture.html');
    addItem();
    expect($j('#addgendelitems ol li:eq(2) label').length).toEqual(1);
  })

  xit("should add a new label containing the phrase -Item 3:- when a new item is added", function(){
    loadFixtures('AddGenItemFixture.html');
    addItem();
    expect($j('#addgendelitems ol li:eq(2) label').text()).toEqual('Item 3:');
  })

  xit("should add an anchor when a new item is added", function(){
    loadFixtures('AddGenItemFixture.html');
    addItem();
    expect($j('#addgendelitems ol li:eq(2) a').length).toEqual(1);
  })
  
  xit("should keep 2nd item label when a further item is added", function(){
    loadFixtures('AddGenItemFixture.html');
    addItem();
    addItem();
    expect(j('#addgendelitems ol li:eq(2) label').length).toEqual(1);
  })

})
