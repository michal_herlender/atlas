import {MetaDataObtainer} from "./metadataObtainer.js"
import { urlGenerator, urlWithParamsGenerator } from "./UrlGenerator.js";
import { paramStrgify } from './ObjectSerializer.js';
import { currencySymbolFunction, dateFormatFunction, dateTimeFormatFunction } from "../components/tools/formatingDirectives.js";

class CwmsMain {
    constructor(){
        this.metaDataObtainer =  MetaDataObtainer;
        this.urlGenerator = urlGenerator;
        this.urlWithParamsGenerator = urlWithParamsGenerator;
        this.paramStrgify = paramStrgify;
        this.dateFormatFunction = dateFormatFunction;
        this.dateTimeFormatFunction = dateTimeFormatFunction;
        this.currencySymbolFunction = currencySymbolFunction;
    }
}


//acces to the ES module base utilities for the legacy code
window.cwms = new CwmsMain();