/** 
 * Shared worker to download translation from server.
 * to inspect and debug on chrome : chrome://inspect/#workers
 * */
// contains ports and message codes to translate
var portsAndCodes = new Array();
var locale;
var url;
var csrfToken;

self.addEventListener("connect", function (event) {
  console.debug("worker connected");

  var port = event.ports[0];

  port.addEventListener("message", (e) => {
    console.debug("worker recevied :", event.data);

    locale = e.data.locale;
    csrfToken = e.data.csrfToken;
    url = e.data.url;
    if (e.data.code !== null)
      portsAndCodes.push({ port: port, code: e.data.code });
  });

  port.start();
});

//Implementation of shared worker thread code
setInterval(runEveryXSeconds, 4000);

function runEveryXSeconds() {
  var codesToTranslate = new Set(portsAndCodes.map((i) => i.code));

  if (codesToTranslate.size == 0) return;

  fetch(url, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "X-CSRF-TOKEN": csrfToken,
    },
    body: JSON.stringify([...codesToTranslate]),
  })
    .then((res) =>
      res.ok
        ? res.json()
        : Promise.reject(`failed to get translations: ${res.status}`)
    )
    .then(res => res.right?res.value:Promise.reject(res.value))
    .then((res) => {
      console.debug("res: ", res);
      // for each port, find correspondant codes and translations
      // and send them back
      const {translations,version} = res;
      portsAndCodes.forEach(pac => {
        const { code, port } = pac;
        const translation = translations[code];
        port.postMessage({translation, version});
      });

      portsAndCodes = new Array();
    })
    .catch(console.error);
}
