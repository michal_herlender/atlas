
export class MetaDataObtainer {
    
  static get contextPath() {
    return document
      .querySelector("meta[name='_contextPath']")?.getAttribute("content") ?? "" ;
  }
  
  static get csrfToken() {
    return document.querySelector("meta[name='_csrf']")?.getAttribute("content");
  }

  static get csrfHeaderParameter(){
    return document.querySelector("meta[name='_csrf_header']")?.getAttribute("content");
  }
  
  static get csrfHeader(){
    return {[this.csrfHeaderParameter]:this.csrfToken};
  }

  static get base() {
    return `${location.origin}${this.contextPath ?? ""}/`;
  }
  static get sytemType(){
    return document.querySelector("meta[name='_systemType']")?.getAttribute("content");
  }
  static get isDevelopment(){
    return this.sytemType =="development";
  }

  static get locale(){
    return document.querySelector("meta[name='_locale']")?.getAttribute("content") ?? "en";
  }

  static get defaultCurrency(){
    return document.querySelector("meta[name='_defaultCurrency']")?.getAttribute("content") ||"EUR";

  }

  static get serverTimeZone(){
    return document.querySelector("meta[name='_serverTimeZone']")?.getAttribute("content") ?? 'Europe/Paris';
  }

  static get allocatedSubdivId(){
    return window.allocatedSubdivId;
  }
}