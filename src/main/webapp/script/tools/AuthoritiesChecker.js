import { urlGenerator } from "./UrlGenerator.js";

class AuthoritiesChecker {
    _authorites;
    
    constructor(){
        this._authorites = fetch(urlGenerator("authorities/all.json"))
        .then(r=> r.ok ? r.json() : Promise.reject(r.statusText))
    }

    hasAuthority(authority){
        return this._authorites.then(a => a.includes(authority));
       }
}


export const authorityChecker = new AuthoritiesChecker();