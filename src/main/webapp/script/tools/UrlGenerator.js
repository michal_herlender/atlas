import {MetaDataObtainer} from "./metadataObtainer.js";

export function instrumentLinkGenerator(plantid){
    return urlWithParamsGenerator("viewinstrument.htm",{plantid});
}

export function instrumentModelLinkGenerator(modelid){
    return urlWithParamsGenerator("instrumentmodel.htm",{modelid});
}
export function instrumentModelPricesLinkGenerator(modelid){
    return urlWithParamsGenerator("instrumentmodelprices.htm",{modelid});
}

export function jobItemLinkGenerator(jobitemid){
    return urlWithParamsGenerator("jiactions.htm",{jobitemid});
}

export function jobItemCertificatesLinkGenerator(jobitemid){
    return urlWithParamsGenerator("jicertificates.htm",{jobitemid});
}

export function jobLinkGenerator(jobid,params){
    const additionalParameters = params ?? {};
    return urlWithParamsGenerator("viewjob.htm",{jobid, ...additionalParameters});
}

export function jobCostingLinkGenerator(id){
    return urlWithParamsGenerator("viewjobcosting.htm",{id});
}


export function editJobCostingItemLinkGenerator(id){
    return urlWithParamsGenerator("editjobcostingitem.htm",{id});
}


export function updateDefaultCalTypeLinkGenerator(){
    return urlGenerator("job/updateDefaultCalType.json");
}

export function updateDefaultTurnLinkGenerator(){
    return urlGenerator("job/updateDefaultTurn.json");
}

export function updateDefaultServiceTypeUsageLinkGenerator(){
    return urlGenerator("updateDefaultServiceTypeUsage.json");
}


export function viewSynthesisLinkGenerator(asnid) {
    return urlWithParamsGenerator("viewefsynthesis.htm",{asnid});
}

export function viewQuotationLinkGenerator(id){
    return urlWithParamsGenerator("viewquotation.htm",{id});
}

export function editQuotationItemLinkGenerator(id){
    return urlWithParamsGenerator("editquotationitem.htm",{id});
}


export function currencyEntityBaseValueTooltipLinkGenerator(entityId,ammount){
    return  urlWithParamsGenerator("tooltip/currency/baseValue.json",{entityId,ammount});
}

export function resetLinkedCostLinkGenerator(costId){
    return urlWithParamsGenerator("contractreview/resetLinkedCost.json",{costId});
}

export function alternativeCostsLinkGenerator(jobItemId){
    return urlWithParamsGenerator("costsource/alternatives.json",{jobItemId});
}

export function updateCostSourceLinkGenerator(jobItemId,costId,costSource){
    return urlWithParamsGenerator("contractreview/updateCostSource.json",{costId,jobItemId,costSource});
}


export function removeItemFromBPOLinkGenerator(bpoId,jobItemId){
     return urlWithParamsGenerator("removeitemfrombpo.json",{bpoId,jobItemId});
}

export function assignBPOToJobItemLinkGenerator(bpoId, jobItemId){
    return urlWithParamsGenerator("assignbpotojobitem.json",{bpoId, jobItemId});
}


export function bpoSelectionLinkGenerator(jobId,jobItemId){
    return urlWithParamsGenerator("bposelection.json",{jobId,jobItemId})
}

export function validPOsForJobItemLinkGenerator(jobItemId){
    return urlWithParamsGenerator("validPOsForJobItem.json",{jobItemId});
}


export function assignItemToPOLinkGenerator(poId, jobItemId){
    return urlWithParamsGenerator("assignItemToPO.json",{jobItemId, poId});
}

export function removeFromPOLinkGenerator(jobItemPOId){
    return urlWithParamsGenerator("deleteJobItemPObyId.json",{jobItemPOId});
}

export function editPOLinkGenerator(poId, poNumber, comment){
    return urlWithParamsGenerator("editPoOnJob.json",{poId, poNumber, comment})
}

export function viewDelNoteLinkGenerator(delid){
    return urlWithParamsGenerator("viewdelnote.htm",{delid})
}

export function viewjobcostingLinkGenerator(id){
    return urlWithParamsGenerator("viewjobcosting.htm",{id});
}

export function urlGenerator(path){
    return new URL(path,MetaDataObtainer.base);
}


export function urlWithParamsGenerator(path,params){
    const url = new URL(path,MetaDataObtainer.base);
    Object.entries(params).forEach((e) =>
        {
            if (Array.isArray(e[1])) e[1].forEach(ee => url.searchParams.append(e[0], ee));
            else
                e[1] !== undefined && url.searchParams.append(e[0], e[1])
        }
    );
    return url;
}


