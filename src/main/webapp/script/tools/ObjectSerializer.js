class ObjectToParamsSerializer {

    toSearchParams(obj){
        return new URLSearchParams(this.serialize(obj));
    }

    serialize(obj){
        return this.toListSerializer(obj).join("&");
    }

    toListSerializer(obj){
        const entries = Object.entries(obj);
        return entries.flatMap(this.serializeResolver.bind(this))
    }


    serializeResolver([name,value]){
        const type = typeof value;
        if(type == "string" ||type=="number")
            return [`${name}=${value}`]
        if(value instanceof Array)
            return value.flatMap((v,ind) => this.serializeResolver([`${name}[${ind}]`,v]) ) 
        if(value instanceof Object)
            return Object.entries(value).flatMap(([k,v]) => this.serializeResolver([`${name}.${k}`,v]))
        return []
    }

        serializeValue(value){
        const type = typeof value;
        if(type == "string" ||type=="number")
            return [value];
        if(value instanceof Array)
            return value.flatMap(v => this.serializeValue(v))
    }


}

export const paramStrgify = new ObjectToParamsSerializer();