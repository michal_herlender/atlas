

export  function debounce(callback, interval) {
  let debounceTimeoutId;

  return function(...args) {
    clearTimeout(debounceTimeoutId);
    debounceTimeoutId = setTimeout(() => callback.apply(this, args), interval);
  };
}

export  function throttle(callback, interval) {
    let enableCall = true;

    return function(...args) {
      if (!enableCall) return;

      enableCall = false;
      callback.apply(this, args);
      setTimeout(() => enableCall = true, interval);
    }
    }

window.debounce = debounce;    