/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: Matt Toseland
*
*	FILENAME		:	Procedures.js
*	DESCRIPTION		:	
*					: 	
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	12/05/2008 - MT - Created File
*
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'dwr/interface/proceditservice.js',
								'script/thirdparty/jQuery/jquery.tablesorter.js');
								
/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init() 
{
	$j('table.tablesorter').tablesorter(
	{
	    headers: {             
           	1: {sorter: false},
			4: {sorter: false},
			5: {sorter: false}
           }
	});
}
