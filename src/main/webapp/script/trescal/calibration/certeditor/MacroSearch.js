/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: Jamie Vinall
*
*	FILENAME		:	MacroSearch.js
*	DESCRIPTION		:	This javascript file is used on the macrosearch.vm page.
*					: 	
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	25/06/2007 - SM - Created File
*					:	03/11/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

var pageImports = new Array('script/trescal/core/utilities/Menu.js',
							'dwr/interface/certtemplateservice.js');

var unlocked = 1;

window.onbeforeunload = function()
{
	//make a SJAX call to remove all locks owned by the user as they leave the page, has to be 
	//synchronous otherwise the call dosen't have enough time to complete 
	$j.ajaxSetup({async: false});
	certtemplateservice.removeLocksOwnedByContact();
	$j.ajaxSetup({async: true});
};

function checkLock(certId, contactId){
	certtemplateservice.isLocked(certId, contactId, {
		callback:function(result) {
		if(result.results==false){
			$j('#locked').text("");
			$j("#save").removeAttr("disabled");
		}else{
			$j('#locked').text(result.message);
			$j("#save").attr("disabled", "disabled"); 
			$j('#certTempName').val($j('#selectedCertId').text());
		}
		}});
}

function updateSelected() {
	var objDropdown = document.getElementById('certTempName');
	var i = objDropdown.selectedIndex;
	var selected_text = objDropdown.options[i].text;
	$j('#selectedCert').text(selected_text);
	$j('#selectedCertId').text($j('#certTempName').val());

}

function clearList(selectbox) {
	var i;
	var objDropdown = document.getElementById(selectbox);
	for (i = objDropdown.options.length - 1; i >= 0; i--) {
		objDropdown.remove(i);
	}
}

function selectFunc(currentUserId) {
	var certId = ($j('#certTempName').val());

	// Load the certificate
	certtemplateservice.loadCert($j('#certTempName').val(), {
		callback : function(result) {
			if (result.success == true) {
				$j('#certReply').val(result.results);
				certtemplateservice.findCertTemplate(certId, {
					callback : function(result) {
						if (result.description != "") {
							$j('#descriptionLine').removeClass('hid').addClass(
									'vis')
							$j('#description').text(result.description)
						} else {
							$j('#descriptionLine').removeClass('vis').addClass(
									'hid')
						}
					}
				})
			} else {
				alert(i18n.t("calibration:couldNotLoadTemplate", "Could not load template"));
			}

			// select macro occurrences
			var select = document.getElementById("certTempName");
			positionCount = -1;
			resultCount = select.selectedIndex;
			next();
		}
	});


	updateSelected();

}

function save(currentUserId) {
	certtemplateservice.saveCert($j('#certReply').val(), $j('#selectedCertId')
			.text(), currentUserId, {
		callback : function(result) {
			if (result.success == false) {
				alert(i18n.t("calibration:templateSavingFailed", "Template was not saved successfully"))
			}
		}
	});
}

var macroResults;
var positionCount;
var resultCount;
var macroLength;
function go(currentUserId) {
	var macro = $j('#macro').val();
	$j('#errors').text(" ");
	document.getElementById("save").disabled = false;
	if (macro != '') {
		certtemplateservice.findMacro(macro,$j('#caseSensitive').is(':checked'),{
			callback : function(result) {
				if (result.success == true) {
					$j('#results').removeClass('hid').addClass('vis');
					$j('#message').text(result.message);

					result = result.results;

					// refresh the drop down list
					clearList('certTempName');
					var select = document.getElementById("certTempName");
					select.options.length = 0;
					for ( var i = 0; i < result.length; i++) {
						// populate drop down list
						select.options[select.options.length] = new Option(
							(result[i].template.name	+ " --- " + result[i].positions.length + " " + i18n.t("calibration:occurrences", "occurrences")),
							result[i].template.id);
						if (result[i].template.locked && result[i].template.lockedby.personid != currentUserId) {
							select.options[i].disabled = true;
							// give warnings for locked
							// templates
							$j('#errors').append(result[i].template.name
									+ " " + i18n.t("calibration:isLockedBy", {varFirstName : result[i].template.lockedby.firstName, varLastName : result[i].template.lockedby.lastName, 
										defaultValue : "is locked by "+ result[i].template.lockedby.firstName + " " + result[i].template.lockedby.lastName})
									+ "<br>");
						} else {
							select.options[i].disabled = false;
						}
					}
					document.getElementById('certTempName').value = $j('#selectedCertId').text();
					$j('#error').text("");

					macroResults = result;
					positionCount = -1;
					resultCount = 0;
					macroLength = macro.length;

					// Select the first unlocked template
					var i = 0;
					while ($j("#certTempName option:eq(" + i + ")").attr("disabled")) {
						i++;
					}
					$j("#certTempName option:eq(" + i + ")").attr('selected', true);

					// select the first occurrence of that
					// template
					selectFunc(currentUserId);
				} else {
					$j('#error').text(result.message);
					$j('#results').removeClass('vis').addClass('hid');
				}
			}
		});
	unlocked = 0;
	}
}

function next() {
	var el = document.getElementById("certReply");
	el.focus();
	if (el.defaultValue != el.value) {
		//if the template has been changed then update the macro positions
		certtemplateservice.updateMacroPositions($j('#macro').val(), macroResults[resultCount].template.id, 
		$j('#certReply').val(), $j('#caseSensitive').is(':checked'), {
			callback : function(result) {
				if (result.success == true) {
					el.defaultValue = el.value;
					macroResults[resultCount] = result.results;

					if (positionCount == macroResults[resultCount].positions.length - 1) {
						positionCount = -1;
					}
					positionCount++;

					var selectionStart = macroResults[resultCount].positions[positionCount].charNum;
					var selectionEnd = Number(macroResults[resultCount].positions[positionCount].charNum)
							+ macroLength;

					// if FireFox or Chrome
					if (el.setSelectionRange) {
						el.focus();
						el.setSelectionRange(selectionStart, selectionEnd);
						// Gets the height of each row in pixles so
						// that the page can be scrolled to a certain row
						var charHeight = Number(el.clientHeight)/ Number(el.rows);
						el.scrollTop = ((macroResults[resultCount].positions[positionCount].lineNum) - 10)
								* charHeight;
						// if IE
					} else if (el.createTextRange) {
						var range = el.createTextRange();
						range.collapse(true);
						range.moveEnd('character',selectionEnd);
						range.moveStart('character',selectionStart);
						range.select();
					}
				}
			}
		});
	} else {

		if (positionCount == macroResults[resultCount].positions.length - 1) {
			positionCount = -1;
		}
		positionCount++;

		var selectionStart = macroResults[resultCount].positions[positionCount].charNum;
		var selectionEnd = Number(macroResults[resultCount].positions[positionCount].charNum)
				+ macroLength;

		// if FireFox or Chrome
		if (el.setSelectionRange) {
			el.focus();
			el.setSelectionRange(selectionStart, selectionEnd);
			// Gets the height of each row in pixles so that
			// the page can be
			// scrolled to a certain row
			var charHeight = Number(el.clientHeight) / Number(el.rows);
			el.scrollTop = ((macroResults[resultCount].positions[positionCount].lineNum) - 10)
					* charHeight;
			// if IE
		} else if (el.createTextRange) {
			var range = el.createTextRange();
			range.collapse(true);
			range.moveEnd('character', selectionEnd);
			range.moveStart('character', selectionStart);
			range.select();
		}

	}
}

function selectAllText() {
	var el = document.getElementById("certReply");
	el.focus();
	el.select();
}

function unlock() {
	document.getElementById("save").disabled = true;
	certtemplateservice.removeLocksOwnedByContact();
	var el = document.getElementById("certReply");
	unlocked = 1;
}

function replace() {
	var el = document.getElementById("certReply");
	var str = $j('#certReply').val();

	var selectionStart = (macroResults[resultCount].positions[positionCount].charNum);
	var selectionEnd = Number(macroResults[resultCount].positions[positionCount].charNum)
			+ macroLength;
	var finalStr = str.substring(0, selectionStart) + $j('#replacement').val()
			+ str.substring(selectionEnd);

	$j('#certReply').val(finalStr);

	// scroll back to original position
	var selectionStart = macroResults[resultCount].positions[positionCount].charNum;
	var selectionEnd = Number(macroResults[resultCount].positions[positionCount].charNum)
			+ macroLength;

	// if FireFox or Chrome
	if (el.setSelectionRange) {
		el.focus();
		el.setSelectionRange(selectionStart, selectionEnd);
		// Gets the height of each row in pixles so that the page can be
		// scrolled to a certain row
		var charHeight = Number(el.clientHeight) / Number(el.rows);
		el.scrollTop = ((macroResults[resultCount].positions[positionCount].lineNum) - 10)
				* charHeight;
		// if IE
	} else if (el.createTextRange) {
		var range = el.createTextRange();
		range.collapse(true);
		range.moveEnd('character', selectionEnd);
		range.moveStart('character', selectionStart);
		range.select();
	}
}

function replaceAndNext() {
	replace();
	next();
}

function replaceAll() {
	var str = $j('#certReply').val();
	macro = "/" + $j('#macro').val() + "/g";
	macro = eval(macro);
	var replacement = $j('#replacement').val();
	str = str.replace(macro, replacement);
	$j('#certReply').val(str);

}

function previous() {
	var el = document.getElementById("certReply");
	el.focus();
	if (el.defaultValue != el.value) {//if the template has been changed then update the macro positions
		certtemplateservice.updateMacroPositions($j('#macro').val(), macroResults[resultCount].template.id,	$j('#certReply').val(),
		{callback : function(result) {
			if (result.success == true) {
				el.defaultValue = el.value;
				macroResults[resultCount] = result.results;
	
				if (positionCount == 0) {
					positionCount = macroResults[resultCount].positions.length;
				}
	
				positionCount--;
	
				var selectionStart = macroResults[resultCount].positions[positionCount].charNum;
				var selectionEnd = Number(macroResults[resultCount].positions[positionCount].charNum)
						+ macroLength;
	
				// if FireFox or Chrome
				if (el.setSelectionRange) {
					el.focus();
					el.setSelectionRange(selectionStart,
							selectionEnd);
					// Gets the height of each row in pixles
					// so
					// that the page can be
					// scrolled to a certain row
					var charHeight = Number(el.clientHeight)
							/ Number(el.rows);
					el.scrollTop = ((macroResults[resultCount].positions[positionCount].lineNum) - 10)
							* charHeight;
					// if IE
				} else if (el.createTextRange) {
					var range = el.createTextRange();
					range.collapse(true);
					range.moveEnd('character',selectionEnd);
					range.moveStart('character',selectionStart);
					range.select();
				}
			}
		}
	});
	} else {
		if (positionCount == 0) {
			positionCount = macroResults[resultCount].positions.length;
		}
		positionCount--;

		var selectionStart = macroResults[resultCount].positions[positionCount].charNum;
		var selectionEnd = Number(macroResults[resultCount].positions[positionCount].charNum)
				+ macroLength;

		// if FireFox or Chrome
		if (el.setSelectionRange) {
			el.focus();
			el.setSelectionRange(selectionStart, selectionEnd);
			var charHeight = Number(el.clientHeight) / Number(el.rows);
			el.scrollTop = ((macroResults[resultCount].positions[positionCount].lineNum) - 10)
					* charHeight;
			// if IE
		} else if (el.createTextRange) {
			var range = el.createTextRange();
			range.collapse(true);
			range.moveEnd('character', selectionEnd);
			range.moveStart('character', selectionStart);
			range.select();
		}
	}
}

function nextTemplate(currentUserId){
	// Select the next unlocked template
	var i = $j("#certTempName").attr("selectedIndex");
	i++;
	while ($j("#certTempName option:eq(" + i + ")").attr("disabled")) {
		i++;
	}
	$j("#certTempName option:eq(" + i + ")").attr('selected', true);

	// select the template
	selectFunc(currentUserId);
}
