/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: Sam Taylor
*
*	FILENAME		:	ProcedureEdit2.js
*	DESCRIPTION		:	
*					: 	
*	DEPENDENCIES	:   script/antech/calibration/keynavigation/ProcedureBuilderKeyNav.js
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	12/05/2008 - MT - Created File
*					: 	16/05/2008 - SH - Add formulae functionality created.
*					:	03/11/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/


var pageImports = new Array (	'script/trescal/core/utilities/Menu.js'	);

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array ( { element: 'ul.subnavtab a', corner: 'small transparent top' },
								{ element: 'div.tab-box', corner: 'small tr bottom'} );

window.onbeforeunload = function()
{
	$j("#validatonFormat").attr("selectedIndex", "0");
	$j("#macrovalue").attr("checked", "checked");

};

window.onscroll = function(){
	moveResults();
};
	
	
function addTableOptions(element){
	//create the specified table for result table elements
	var x = element.siblings(':text').val();
	var y = element.siblings(':text').next().val();
	var count=0;
	var cell ="<td><input id=\"";
	var cell2 ="\" onclick=\"return hide_box(this)\" ondblclick=\"return show_hide_box(this,200,25,'0')\" type=\"text\" style=\"width:100%;\"/></td>";
	var row = "<tr>";
	var table = "<table id=\"table\" style=\"width:100%;\">";
	while(count<x){//build up a row
		row=row+cell+count+cell2;
		count++;
	}
	row=row+"<td id=\"headCheck\"><input id = \"";
	row2="\" type=\"checkbox\"/></td></tr>";
	count=0;
	while(count<y){//use the row to build up the rest of the table
		table=table+row+eval("count*x")+row2;
		count++;
	}
	table = table+"</table><input style='float: right;' type='button' value='+' onClick='addRow($j(this))'><input type='button' style='float: right;' value='-' onClick='deleteRow($j(this))'><br><p>";
	
	element.parent().siblings('div').html(table);
	
	var format ="";
	count=0;
	while(count<x*y){
		format=format+count+",";
		count++;
	}
	element.siblings('#format').val(format);
}

function updateStyles(el){
	//update the hidden text input that contains the CSS information for the textarea
	var properties = new Array('font-weight', 'font-style', 'text-decoration', 'text-align');
	var i = 0;
	var string = "";
	while (i<4){
		string=string+properties[i]+": "+$j(el).siblings("textarea").css(properties[i])+"; ";
		i++;
	}
	$j(el).siblings(":text").val(string);
}

//creates a depressed button effect for the formatting buttons 
function boldToggle(el){
	if($j(el).siblings("textarea").css('font-weight')=='bold'){
		$j(el).siblings("textarea").css('font-weight', '');
		$j(el).css('border-top', '');
		$j(el).css('border-left', '');
		$j(el).css('border-right', '');
		$j(el).css('border-bottom', '');
	}else{
		$j(el).siblings("textarea").css('font-weight', 'bold');
		$j(el).css('border-top', '2px inset #eeeeee');
		$j(el).css('border-left', '2px inset #bbbbbb');
		$j(el).css('border-right', '2px inset  #dddddd');
		$j(el).css('border-bottom', '2px inset  #cccccc');
	}
	updateStyles(el);
}

function italicToggle(el){
	if($j(el).siblings("textarea").css('font-style')=='italic'){
		$j(el).siblings("textarea").css('font-style', '');
		$j(el).css('border-top', '');
		$j(el).css('border-left', '');
		$j(el).css('border-right', '');
		$j(el).css('border-bottom', '');
	}else{
		$j(el).siblings("textarea").css('font-style', 'italic');
		$j(el).css('border-top', '2px inset #eeeeee');
		$j(el).css('border-left', '2px inset #bbbbbb');
		$j(el).css('border-right', '2px inset  #dddddd');
		$j(el).css('border-bottom', '2px inset  #cccccc');
	}
	updateStyles(el);
}
function underlineToggle(el){
	if($j(el).siblings("textarea").css('text-decoration')=='underline'){
		$j(el).siblings("textarea").css('text-decoration', '');
		$j(el).css('border-top', '');
		$j(el).css('border-left', '');
		$j(el).css('border-right', '');
		$j(el).css('border-bottom', '');
	}else{
		$j(el).siblings("textarea").css('text-decoration', 'underline');
		$j(el).css('border-top', '2px inset #eeeeee');
		$j(el).css('border-left', '2px inset #bbbbbb');
		$j(el).css('border-right', '2px inset  #dddddd');
		$j(el).css('border-bottom', '2px inset  #cccccc');
	}
	updateStyles(el);
}
function leftToggle(el){
	$j(el).siblings("textarea").css('text-align', '');
	updateStyles(el);
}
function centerToggle(el){
	$j(el).siblings("textarea").css('text-align', 'center');
	updateStyles(el);
}
function rightToggle(el){
	$j(el).siblings("textarea").css('text-align', 'right');
	updateStyles(el);
}

function add(){
	var elArray=new Array();
		$j('#elements :selected').each(function(i, selected){
			elArray.push($j(selected).val());
		 });
		
	templateobjectservice.add(elArray, {callback:function(result) {
		if(result.success==true){
			$j('#pick').append(result.results);
			$j('#errorparent').addClass("hid");
		}else{
			$j('#errorparent').removeClass("hid");
			$j('#error').text(result.message);
		}
	}});
}
	
	function makeTemplate(submit){
		//submit is an optional boolean indicating if the form should be submited at the end of the function
		if(typeof(submit) === 'undefined'){
			submit=false;
		}
		
		if($j("#pick fieldset:first").attr("id")!="pageBreak"){//if there is not an initial pagebreak add one
			templateobjectservice.add(new Array("PAGE"), {callback:function(result) {
				if(result.success==true){
					$j('#pick').prepend(result.results);
					$j('#errorparent').addClass("hid");
				}else{
					$j('#errorparent').removeClass("hid");
					$j('#error').text(result.message);
				}
				
				if($j("#pick fieldset:last").attr("id")!="pageBreak"){//if there is no closing pagebreak either add one
					templateobjectservice.add(new Array("PAGE"), {callback:function(result) {
						if(result.success==true){
							$j('#pick').append(result.results);
							$j('#errorparent').addClass("hid");
						}else{
							$j('#errorparent').removeClass("hid");
							$j('#error').text(result.message);
						}
						$j("#pick select:last").val("2");
						makeTemplateProcess(submit);
					}});
				}else{
					makeTemplateProcess(submit);
				}
			}});
		}else if($j("#pick fieldset:last").attr("id")!="pageBreak"){//if there is an initial pagebreak but no closing one add one
			templateobjectservice.add(new Array("PAGE"), {callback:function(result) {
				if(result.success==true){
					$j('#pick').append(result.results);
					$j('#errorparent').addClass("hid");
				}else{
					$j('#errorparent').removeClass("hid");
					$j('#error').text(result.message);
				}
				$j("#pick select:last").val("2");
				makeTemplateProcess(submit);
			}});
		}else{
			makeTemplateProcess(submit);
		}
	}
	
	function makeTemplateProcess(submit){
		//make the template
		var elTypeArray = new Array();
		var i=1;
		var contentArray = new Array();
		//go though each element and extract all of the input:text, textarea, select and input:checkbox information from the form
		while (i<=($j("#pick").children().length)){
			var c=0;
			var content = new Array();
			var element = $j("#pick fieldset:nth-child("+i+")");
			var elType = $j("#elements option[value='"+element.children().attr("id")+"']").val();
			
			var inputs = $j(element).find('input:text');
			var texts = $j(element).find('textarea');
			var selects = $j(element).find('select');
			var checkboxs = $j(element).find('input:checkbox');
			var checkboxsvalue = $j();
			checkboxs.each(function(){
				checkboxsvalue=checkboxsvalue.add($j(this).val($j(this).is(':checked')));
			});
			//put all of the inputs in an array
			inputs=inputs.add(texts);
			inputs=inputs.add(selects);
			inputs=inputs.add(checkboxsvalue);
			inputs.each(function(){
				content[c]=$j(this).val();
				c++;
			});
			//add the array of inputs to the array of array of inputs and the element class of the element to the element type array
			elTypeArray[i-1]=elType;
			contentArray[i-1]=content;
			
			c=0;
			i++;
		}
		templateobjectservice.make(elTypeArray, contentArray,{callback:function(result) {
			if(result.success==true){
				$j('#dvsl').val(result.results);
				$j('#errorparent').addClass("hid");
			}else{
				$j('#errorparent').removeClass("hid");
				$j('#error').text(result.message);
			}
			//if required and there is no error then submit the form
			if(submit&&result.success){
				$j("#code").submit();
			}
		}});
	}
	
	function deconstruct(){
		var elArray = new Array();
		var elStringArray = new Array();
		var array = new Array(
				new Array(),
				new Array()			
		);
		
		//get the index of all element beginning and end tags 
		var str=$j('#dvsl').val();
		var i = 0;
		var pos1 = 1;
		var pos2 = 1;
		while (pos2>0){
			var pos1=str.indexOf("<!--@@", pos1+1)+6;
			var pos2=str.indexOf("@@-->", pos2+1);	
			if(pos2>0){
				array[0][i]=pos1;
				array[1][i]=pos2;
			}
			i++;
		}
		
		i=0;	
		while(i<array[0].length){
			//get the contents of the template between two element tags 
			elStringArray.push(str.slice(array[1][i]+3, array[0][i+1]-6));
			try{
				//attempt to get the type of the element 
				var elType = $j(("#elements option[value='"+str.slice(array[0][i], array[1][i])+"']")).val();
			}catch(err){
				$j('#errorparent').removeClass("hid");
				$j('#error').text(i18n.t("calibration:errorHandlingElement", "Error handling element named:") + ' ' +str.slice(array[0][i], array[1][i]));
				return;
			}
			elArray.push(elType);
			i++;
			i++;
		};
		templateobjectservice.extract(elArray, elStringArray, {callback:function(result) {
			if(result.success==true){
				$j('#pick').html(result.results);
				$j('#errorparent').addClass("hid");
			}else{
				$j('#errorparent').removeClass("hid");
				$j('#error').text(result.message);
			}
		}});
	}

function removeElement(element){
	element.parent().parent().parent().remove();
}

function moveElementUp(element){
	element.parent().parent().parent().insertBefore(element.parent().parent().parent().prev());
}

function moveElementDown(element){
	element.parent().parent().parent().insertAfter(element.parent().parent().parent().next());
}

//the next 3 functions are borrowed from http://www.jtricks.com/javascript/window/box.html 
function move_box(an, box) {
	  var cleft = 0;
	  var ctop = 0;
	  var obj = an;
	  while (obj.offsetParent) {
	    cleft += obj.offsetLeft;
	    ctop += obj.offsetTop;
	    obj = obj.offsetParent;
	  }
	  box.style.left = cleft + 'px';
	  ctop += an.offsetHeight + 8;
	  if (document.body.currentStyle &&
	    document.body.currentStyle['marginTop']) {
	    ctop += parseInt(
	      document.body.currentStyle['marginTop']);
	  }
	  box.style.top = ctop + 'px';
}

function hide_box(an){
	 var href = an.href;
	  var boxdiv = document.getElementById(href);

	  if (boxdiv != null) {
	      $j(boxdiv).remove();
	      return false;
	  }
}

var telement;
function show_hide_box(an, width, height, borderStyle) {
  var href = an.href;
  var boxdiv = document.getElementById(href);
  telement=an;
  
  if (boxdiv != null) {
    if (boxdiv.style.display=='none') {
      move_box(an, boxdiv);
      boxdiv.style.display='block';
    } else
    	$j(boxdiv).remove();
    return false;
  }

  boxdiv = document.createElement('div');
  boxdiv.setAttribute('id', href);
  boxdiv.style.display = 'block';
  boxdiv.style.position = 'absolute';
  boxdiv.style.width = width + 'px';
  boxdiv.style.height = height + 'px';
  boxdiv.style.border = borderStyle;
  boxdiv.style.backgroundColor = '#fff';

  var depresedStyle="style=\"border-top:2px inset #dce1e9; border-left:2px inset #dce1e9; border-right:2px inset #dce1e9; border-bottom:2px inset #dce1e9;\"";
	var bold="";
	if($j(an).css('font-weight')=='bold'){
		bold=depresedStyle;
	}
	var underline="";
	if($j(an).css('text-decoration')=='underline'){
		underline=depresedStyle;
	}
	var italic="";
	if($j(an).css('font-style')=='italic'){
		italic=depresedStyle;
	}
	
	var cellId = parseInt($j(an).attr('id'))+parseInt($j(an).parent().siblings('#headCheck').children(':checkbox').attr('id'));
	
	var contents = document.createElement("div");
	  contents.innerHTML = "<button type='button' "+bold+" onclick='boldToggle2(telement, $j(this))'><strong>B</strong></button>"
			+ "<button type='button' "+underline+" onclick='underlineToggle2(telement, $j(this))'><u>U</u></button>"		
			+ "<button type='button' "+italic+" onclick='italicToggle2(telement, $j(this))'><i>I</i></button>"
			+ "<input type='text' cols='50' rows='2' class='hid' value=\"font-weight: 400; font-style: normal; text-decoration: none; text-align: start; \"></input>"
			+ "&nbsp;&nbsp;"
			+ "<button type='button' onclick='leftToggle2(telement)'>=-</button>"
			+ "<button type='button' onclick='centerToggle2(telement)'>-=-</button>"
			+ "<button type='button' onclick='rightToggle2(telement)'>-=</button>";
	 contents.scrolling = 'no';
	 contents.frameBorder = '0';
	 contents.style.width = width + 'px';
	 contents.style.height = height + 'px';
	 contents.style.backgroundColor="#DEDEDE";
	 contents.src = href;

	 boxdiv.appendChild(contents);
	 document.body.appendChild(boxdiv);
	 move_box(an, boxdiv);
	 return false;
}
//these functions control the formatting for the table cells from the popup box 
function boldToggle2(el, button){
	//formats are stored in a hidden input box as a string. Each cell has its own ID which is worked out by adding the cells column number (starting at 0)
	//to the ID of the checkbox at the end of the row. The string containing the formatting for each cell is a comma delimeted list
	//of all of the cell IDs followed by the code for their formatting, for example the formatting for a normal 2x2 table would be '0,1,2,3'
	//if the first two cells were made bold and centre then the string would be '0bc,1bc,2,3'
	var cellId=parseInt($j(el).attr('id'))+parseInt($j(el).parent().siblings('#headCheck').children(':checkbox').attr('id'));
	var format = $j(el).parent().parent().parent().parent().parent().siblings('p').children('#format').val();
	var formatSub = format.substring(format.indexOf(cellId),format.indexOf(",",format.indexOf(cellId)));
	
	if($j(el).css('font-weight')=='bold'){
		$j(el).css('font-weight', '');
		$j(button).css('border-top', '');
		$j(button).css('border-left', '');
		$j(button).css('border-right', '');
		$j(button).css('border-bottom', '');
		formatSub = formatSub.replace("b","");
	}else{
		$j(el).css('font-weight', 'bold');
		$j(button).css('border-top', '2px inset #eeeeee');
		$j(button).css('border-left', '2px inset #bbbbbb');
		$j(button).css('border-right', '2px inset  #dddddd');
		$j(button).css('border-bottom', '2px inset  #cccccc');
		formatSub = formatSub.replace(cellId,cellId+"b");
	}
	format=format.substring(0,format.indexOf(cellId))+formatSub+format.substring(format.indexOf(",",format.indexOf(cellId)))
	$j(el).parent().parent().parent().parent().parent().siblings('p').children('#format').val(format);
}

function italicToggle2(el, button){
	var cellId=parseInt($j(el).attr('id'))+parseInt($j(el).parent().siblings('#headCheck').children(':checkbox').attr('id'));
	var format = $j(el).parent().parent().parent().parent().parent().siblings('p').children('#format').val();
	var formatSub = format.substring(format.indexOf(cellId),format.indexOf(",",format.indexOf(cellId)));
	if($j(el).css('font-style')=='italic'){
		$j(el).css('font-style', '');
		$j(button).css('border-top', '');
		$j(button).css('border-left', '');
		$j(button).css('border-right', '');
		$j(button).css('border-bottom', '');
		formatSub = formatSub.replace("i","");
	}else{
		$j(el).css('font-style', 'italic');
		$j(button).css('border-top', '2px inset #eeeeee');
		$j(button).css('border-left', '2px inset #bbbbbb');
		$j(button).css('border-right', '2px inset  #dddddd');
		$j(button).css('border-bottom', '2px inset  #cccccc');
		formatSub = formatSub.replace(cellId,cellId+"i");
	}
	format=format.substring(0,format.indexOf(cellId))+formatSub+format.substring(format.indexOf(",",format.indexOf(cellId)))
	$j(el).parent().parent().parent().parent().parent().siblings('p').children('#format').val(format);
}

function underlineToggle2(el, button){
	var cellId=parseInt($j(el).attr('id'))+parseInt($j(el).parent().siblings('#headCheck').children(':checkbox').attr('id'));
	var format = $j(el).parent().parent().parent().parent().parent().siblings('p').children('#format').val();
	var formatSub = format.substring(format.indexOf(cellId),format.indexOf(",",format.indexOf(cellId)));
	if($j(el).css('text-decoration')=='underline'){
		$j(el).css('text-decoration', '');
		$j(button).css('border-top', '');
		$j(button).css('border-left', '');
		$j(button).css('border-right', '');
		$j(button).css('border-bottom', '');
		formatSub = formatSub.replace("u","");
	}else{
		$j(el).css('text-decoration', 'underline');
		$j(button).css('border-top', '2px inset #eeeeee');
		$j(button).css('border-left', '2px inset #bbbbbb');
		$j(button).css('border-right', '2px inset  #dddddd');
		$j(button).css('border-bottom', '2px inset  #cccccc');
		formatSub = formatSub.replace(cellId,cellId+"u");
	}
	format=format.substring(0,format.indexOf(cellId))+formatSub+format.substring(format.indexOf(",",format.indexOf(cellId)))
	$j(el).parent().parent().parent().parent().parent().siblings('p').children('#format').val(format);
}

function leftToggle2(el){
	$j(el).css('text-align', '');
	var cellId=parseInt($j(el).attr('id'))+parseInt($j(el).parent().siblings('#headCheck').children(':checkbox').attr('id'));
	var format = $j(el).parent().parent().parent().parent().parent().siblings('p').children('#format').val();
	var formatSub = format.substring(format.indexOf(cellId),format.indexOf(",",format.indexOf(cellId)));
	formatSub = formatSub.replace("c","");
	formatSub = formatSub.replace("r","");
	format=format.substring(0,format.indexOf(cellId))+formatSub+format.substring(format.indexOf(",",format.indexOf(cellId)))
	$j(el).parent().parent().parent().parent().parent().siblings('p').children('#format').val(format);
}

function centerToggle2(el){
	$j(el).css('text-align', 'center');
	var cellId=parseInt($j(el).attr('id'))+parseInt($j(el).parent().siblings('#headCheck').children(':checkbox').attr('id'));
	var format = $j(el).parent().parent().parent().parent().parent().siblings('p').children('#format').val();
	var formatSub = format.substring(format.indexOf(cellId),format.indexOf(",",format.indexOf(cellId)));
	if(formatSub.indexOf("c")==-1){
		formatSub = formatSub.replace(cellId,cellId+"c");
		formatSub = formatSub.replace("r","");
		format=format.substring(0,format.indexOf(cellId))+formatSub+format.substring(format.indexOf(",",format.indexOf(cellId)));
	}
	$j(el).parent().parent().parent().parent().parent().siblings('p').children('#format').val(format);
}

function rightToggle2(el){
	$j(el).css('text-align', 'right');
	var cellId=parseInt($j(el).attr('id'))+parseInt($j(el).parent().siblings('#headCheck').children(':checkbox').attr('id'));
	var format = $j(el).parent().parent().parent().parent().parent().siblings('p').children('#format').val();
	var formatSub = format.substring(format.indexOf(cellId),format.indexOf(",",format.indexOf(cellId)));
	if(formatSub.indexOf("r")==-1){
		formatSub = formatSub.replace(cellId,cellId+"r");
		formatSub = formatSub.replace("c","");
		format=format.substring(0,format.indexOf(cellId))+formatSub+format.substring(format.indexOf(",",format.indexOf(cellId)));
	}
	$j(el).parent().parent().parent().parent().parent().siblings('p').children('#format').val(format);
}

function clickListen(el){
	//once an option is selected from the data selection select box an onclick event is created on all text input elements 
	$j("#pick input:text, #pick textarea").die();//remove any previous events
	$j("#pick input:text, #pick textarea").live("click",(function(){//create the new event
		if($j(el).attr('id')=='name'){//If its from the name tab
			if($j("#macrovalue").attr('checked')==true){
				$j(this).val($j(this).val()+"#getInputLabel('"+$j('#name :selected').val()+"')");
			}else{
				$j(this).val($j(this).val()+($j('#name :selected').attr('name')));
			}
			
		}else if($j(el).attr('id')=='validatorInput'){//If its from the Inputs tab
			if($j("#macrovalue").attr('checked')==true){
				var size=$j("#inputForm > input").size();
				if(size!=0){//if formatting is being applied  
					var inputs="($val,";
					var quotes = ($j("#validatonFormat :selected").attr('name').split(" | ")[1].split(","));
					for(var i=0;i<size;i++){//build up a list of formatting macro input arguments 
						if (quotes[i]=="true"&&($j("#inputForm > input").get(i).value.indexOf("$")==-1)){//if the argument is supposed to be surrounded with quotes and it dosent contain a velocity variable
							inputs=inputs+"'"+$j("#inputForm > input").get(i).value+"',";
						}else{
							inputs=inputs+$j("#inputForm > input").get(i).value+",";
						}
					}
					inputs=inputs.substring(0, inputs.length-1)+")";
					
					$j(this).val($j(this).val()+"#set($val = \"#getInput('"+$j('#validatorInput :selected').val()+"')\")\n"
							+$j("#validatonFormat :selected").val()+inputs);
				}else{
					$j(this).val($j(this).val()+"#getInput('"+$j('#validatorInput :selected').val()+"')");
				}
			}else{
				$j(this).val($j(this).val()+($j('#validatorInput :selected').attr('name')));
			}
			
			
		}else if($j(el).attr('id')=='formulaSelect'){//If its from the Formula tab
			if($j("#macrovalue").attr('checked')==true){
				var size=$j("#inputForm > input").size();
				if(size!=0){
					var inputs="";
					var inputs="($val,";
					var quotes = ($j("#validatonFormat :selected").attr('name').split(" | ")[1].split(","));
					for(var i=0;i<size;i++){
						if (quotes[i]=="true"&&($j("#inputForm > input").get(i).value.indexOf("$")==-1)){
							inputs=inputs+"'"+$j("#inputForm > input").get(i).value+"',";
						}else{
							inputs=inputs+$j("#inputForm > input").get(i).value+",";
						}
					}
					inputs=inputs.substring(0, inputs.length-1)+")";
					
					$j(this).val($j(this).val()+"#set($val = \"#getFormulaById('"+$j('#formulaSelect :selected').val()+"')\")\n"
							+$j("#validatonFormat :selected").val()+inputs);
				}else{
					$j(this).val($j(this).val()+"#getFormulaById('"+$j('#formulaSelect :selected').val()+"')");
				}
			}else{
				$j(this).val($j(this).val()+($j('#formulaSelect :selected').attr('name')));
			}
			
			
			}else if($j(el).attr('id')=='limits'){//If its from the Limits tab
				if($j("#macrovalue").attr('checked')==true){
					
					
					var size=$j("#inputForm > input").size();
					if(size!=0){
						var inputs="";
						var inputs="($val,";
						var quotes = ($j("#validatonFormat :selected").attr('name').split(" | ")[1].split(","));
						for(var i=0;i<size;i++){
							if (quotes[i]=="true"&&($j("#inputForm > input").get(i).value.indexOf("$")==-1)){
								inputs=inputs+"'"+$j("#inputForm > input").get(i).value+"',";
							}else{
								inputs=inputs+$j("#inputForm > input").get(i).value+",";
							}
						}
						inputs=inputs.substring(0, inputs.length-1)+")";
						
						if(/\$.*/.test($j("#limits :selected").val())){
							$j(this).val($j(this).val()+"#set($val = \"#getFormulaById('"+$j('#limits :selected').val().substr(1)+"')\")\n"
									+$j("#validatonFormat :selected").val()+inputs);
							
						}else if(/@.*/.test($j("#limits :selected").val())){
							$j(this).val($j(this).val()+"#set($val = \"#getInput('"+$j('#limits :selected').val().substr(1)+"')\")\n"
									+$j("#validatonFormat :selected").val()+inputs);
							
						}else{
							$j(this).val($j(this).val()+"#set($val = \"#getLimit('"+$j('#limits :selected').attr("name")+")\")\n"
									+$j("#validatonFormat :selected").val()+inputs);
							
						}
					}else{
						if(/\$.*/.test($j("#limits :selected").val())){
							$j(this).val($j(this).val()+"#getFormulaById('"+$j('#limits :selected').val().substr(1)+"')");
						}else if(/@.*/.test($j("#limits :selected").val())){
							$j(this).val($j(this).val()+"#getInput('"+$j('#limits :selected').val().substr(1)+"')");
						}else{
							$j(this).val($j(this).val()+"#getLimit('"+$j('#limits :selected').attr("name")+")");
						}
							
					}
				}else{
					$j(this).val($j(this).val()+($j('#limits :selected').text().substr(3)));
				}
			
			
		}else if($j(el).attr('id')=='value'){//If its from the validator result tab
			if($j("#macrovalue").attr('checked')==true){
				var size=$j("#inputForm > input").size();
				if(size!=0){
					var inputs="";
					var inputs="($val,";
					var quotes = ($j("#validatonFormat :selected").attr('name').split(" | ")[1].split(","));
					for(var i=0;i<size;i++){
						if (quotes[i]=="true"&&($j("#inputForm > input").get(i).value.indexOf("$")==-1)){
							inputs=inputs+"'"+$j("#inputForm > input").get(i).value+"',";
						}else{
							inputs=inputs+$j("#inputForm > input").get(i).value+",";
						}
					}
					inputs=inputs.substring(0, inputs.length-1)+")";
					
					$j(this).val($j(this).val()+"#set($val = \"#validatorInputSplit('"+$j('#value :selected').val()+"')\")\n"
							+$j("#validatonFormat :selected").val()+inputs);
				}else{
					$j(this).val($j(this).val()+"#validatorInputSplit('"+$j('#value :selected').val()+"')");
				}
			}else{
				$j(this).val($j(this).val()+($j('#value :selected').attr('name')));
			}
			
			
		}else{
			if($j("#macrovalue").attr('checked')==true){
				var size=$j("#inputForm > input").size();
				if(size!=0){
					var inputs="";
					var inputs="($val,";
					var quotes = ($j("#validatonFormat :selected").attr('name').split(" | ")[1].split(","));
					for(var i=0;i<size;i++){
						if (quotes[i]=="true"&&($j("#inputForm > input").get(i).value.indexOf("$")==-1)){
							inputs=inputs+"'"+$j("#inputForm > input").get(i).value+"',";
						}else{
							inputs=inputs+$j("#inputForm > input").get(i).value+",";
						}
					}
					
					inputs=inputs.substring(0, inputs.length-1)+")";
					
					$j(this).val($j(this).val()+"#set($val = \"#getValidatorResult('"+$j('#validators :selected').val()+"')\")\n"
							+$j("#validatonFormat :selected").val()+inputs);
				}else{
					$j(this).val($j(this).val()+"#getValidatorResult('"+$j('#validators :selected').val()+"')");
				}
			}else{
				$j(this).val($j(this).val()+removeHTMLTags($j('#validators :selected').attr('name')));
			}
		}
        $j("#pick input:text, textarea").die();
	}));		
}

function removeHTMLTags(input){// taken from http://javascript.internet.com/snippets/remove-html-tags.html
 		var strInputCode = input;
 	 	strInputCode = strInputCode.replace(/&(lt|gt);/g, function (strMatch, p1){
 		 	return (p1 == "lt")? "<" : ">";
 		});
 		var strTagStrippedText = strInputCode.replace(/<\/?[^>]+(>|$)/g, "");
 	return strTagStrippedText;
}

function buildInputs(unit){
	//create a list of inputs for the result formatter when one is selected
	var inputs = ($j("#validatonFormat :selected").attr('name').split(" | ")[0].split(","));
	var inputHTML="";
	
	if(inputs[0].length!=0){
		for(var i=0 ; i<inputs.length;i++){
			//if its a unit of measurement input use the default UOM 
			if (((inputs[i].toLowerCase().indexOf("unit")>-1)&&(inputs[i].toLowerCase().indexOf("measurement")>-1))||(inputs[i].toLowerCase().indexOf("uom")>-1)){
				inputHTML=inputHTML.concat(inputs[i]+":<br>" +
						"<input id='"+i+"' type='text' value='"+unit+"'/><br>");
			}else{
			inputHTML=inputHTML.concat(inputs[i]+":<br>" +
					"<input id='"+i+"' type='text'/><br>");
			}
		}
	}
	$j("#inputForm").html(inputHTML);
}

function switchTabs(selected, tab){
	$j('#tabs select').addClass('hid');
	$j(tab).removeClass('hid');
	$j('.selectedtab').removeClass('selectedtab');
	$j(selected).parent().addClass('selectedtab');
}


function addRow(el){//add a new row of inputs to a table
	var table=el.siblings('table');
	var x = ($j(table).find("tr:first>td").size()-1);//get the width of the table
	var y = ($j(table).find("tr").size());//get the height of the table
	
	if (x==-1){//If all of the rows have been removed us the x value from the initial table creation input box
		x = $j(table).parent().siblings("p").find('#tablex').val();
	}
	
	var row = "<tr>";
	var cell ="<td><input id=\"";
	var cell2 ="\" onclick=\"return hide_box(this)\" ondblclick=\"return show_hide_box(this,200,25,'0')\" type=\"text\" style=\"width:100%;\"/></td>";
	var rowbuilder="";
	
	for(var i=0;i<x;i++){//build up the row
		rowbuilder=rowbuilder+cell+i+cell2;
	}
	
	//add the check box
	row=row+rowbuilder+"<td id=\"headCheck\"><input id = \"";
	row2="\" type=\"checkbox\"/></td></tr>";
	table.find("tbody").append(row+(y*x)+row2);
	
	//update the hidden text input that keeps track of formatting with the new cell numbers
	for(var i=0;i<x;i++){
		$j(table).parent().siblings("p").find('#format').val($j(table).parent().siblings("p").find('#format').val()+((x*y+i)+","));
	}
}

function deleteRow(el){
	var table=el.siblings('table');
	var x = ($j(table).find("tr:first>td").size()-1);
	var y = ($j(table).find("tr").size());
	$j(table).find("tr:last-child").remove();
	$j(table).parent().siblings("p").find('#format').val($j(table).parent().siblings("p").find('#format').val().split(x*(y-1))[0]);
	
}

function moveResults(){
	//if the result box hits the top of the screen expand the div above it to move it down
	if($j(window).scrollTop()>$j("#results").offset().top){
		$j("#resultsMover").height($j(window).scrollTop()-$j("#resultsMover").position().top+5);
	}
	
	//if the result box hits the bottom of the screen compress the div above it to move it up
	if(($j(window).scrollTop()+$j(window).height())<($j("#results").offset().top+$j("#results").height())){
		$j("#resultsMover").height($j(window).scrollTop()-$j("#resultsMover").position().top+($j(window).height()-$j("#results").height())-5);
	}
}	

function toggleOptions(){
	//expand or collapse a limits sub values  
	var number=$j('#limits :selected').attr('name');
	$j("#limits option[name^='"+number+"\\',']").toggleClass('hid');
}

