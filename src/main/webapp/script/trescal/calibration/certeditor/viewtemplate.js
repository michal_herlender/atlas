/*******************************************************************************
 * CWMS (Calibration Workflow Management System)
 * 
 * Copyright 2006, Antech Calibration Services www.antech.org.uk
 * 
 * All rights reserved
 * 
 * Document author: Sam Taylor
 * 
 * FILENAME : DESCRIPTION : : DEPENDENCIES :
 * 
 * TO-DO 		: - 
 * KNOWN ISSUES : - 
 * HISTORY		:	03/11/2015 - TProvost - Manage i18n
 * 
 ******************************************************************************/


/**
 * this variable contains the page name which should be used when creating the
 * search again link in the header section
 */
var searchAgain = 'searchwork.htm';

/**
 * declarations for tab control
 */	
	var menuElements3 = new Array( 	{ anchor: 'pdf-link', block: 'pdf' },
          							{ anchor: 'preview-link', block: 'preview' },
           							{ anchor: 'xmlcontent-link', block: 'xmlcontent' } );
	
window.onbeforeunload = function()
{
	$j.ajaxSetup({async: false});
	certtemplateservice.removeLocksOwnedByContact();
	$j.ajaxSetup({async: true});
};
	
var leftExpandState = false;
var leftCloseState = false;
function leftExpand()//toggle the left half of the screen between normal and expanded
{
	if (leftExpandState == false)
	{
		leftExpandState = true;
		$j('#leftExpand').val("Compress");
		$j('div#rightbox').animate({marginLeft: '78%'}, 440), $j('div#leftbox').animate({width: '153%'}, 440);
		return
	}
	if (leftExpandState == true)
	{
		leftExpandState = false;
		$j('#leftExpand').val("Expand");
		$j('div#rightbox').animate({marginLeft: '50%'}, 440), $j('div#leftbox').animate({width: '97%'}, 440);
		return
	}
}	

function leftClose()//toggle the left half of the screen between normal and closed
{
	if (leftCloseState == false)
	{
		leftCloseState = true;
		$j('div#leftboxoutside').removeClass('vis').addClass('hid');
		$j('div#leftcollapsed').removeClass('hid').addClass('vis');
		$j('div#rightbox').animate({marginLeft: '5%'}, 440);		
		return
	}
	if (leftCloseState == true)
	{
		leftCloseState = false;
		$j('div#leftboxoutside').removeClass('hid').addClass('vis');
		$j('div#leftcollapsed').removeClass('vis').addClass('hid');
		$j('div#rightbox').animate({marginLeft: '50%'}, 440);
		return
	}
}

function closeArchivedCert(){
	//close the archived cert windows
	$j('#arcCertReply').removeClass('vis').addClass('hid');
	$j('#selectedArcCertLable').removeClass('vis').addClass('hid');
	$j('#certReply').animate({width: '100%'}, 440);	
	$j('#selectedArcCert').text('');
	$j('#previousTemplate').removeClass('vis').addClass('hid')
	$j('#arcpdf').removeClass('vis').addClass('hid');
	$j('#p2').addClass("hid");
	$j('#arcpdf').parent().width('0%');
}


function processArchivedCertDetails(id){
	//get all of the archived cert information
	archivedcerttemplateservice.findArchivedCertTemplateAjax(id,
	{callback:function(result) {
	var cert=result.results;
	$j('#editBy').text(cert.editby.name);
	$j('#editDate').text(cert.formattedDate);	
	$j('#currentVersion').text(cert.certtemplate.name);
	$j('#currentDate').text(cert.certtemplate.formattedDate);
	$j('#currentEditBy').text(cert.certtemplate.lasteditby.name);
	$j('#selectedArcCert').text(cert.name);	
	}});
	
	$j('#arcCertReply').removeClass('hid').addClass('vis');
	$j('#selectedArcCertLable').removeClass('hid').addClass('vis');	
	$j('#certReply').animate({width: '48%'}, 440);
}

function checkLock(certId, contactId){
	certtemplateservice.isLocked(certId, contactId, {
		callback:function(result) {
		if(result.results==false){
			$j('#locked').text("");
			$j("#save").removeAttr("disabled");
			$j("#editsubmit").removeAttr("disabled");
		}else{
			//if the template is locked then disable all save functions
			$j('#locked').text(result.message);
			$j("#save").attr("disabled", "disabled"); 
			$j("#editsubmit").attr("disabled", "disabled"); 
			$j('#certTempName').val($j('#selectedCertId').text());
		}
		}});
}

function updateSelected(){
	//update selected cert details when a new cert is selected
	$j('#selectedCert').text($j('#searchBox').find('> input[type="text"]:first').val());
	$j('#selectedCertId').text($j('input[name="templateID"]').val());
	$j('#certTempName option[selected="selected"]').text();
}

function getEditFormInfo(certId){
	//populate the edit tempalte form
	certtemplateservice.findCertTemplate(certId, {callback:function(result) { $j('#editTemplateDescription').val(result.description);}});
	var name = $j('#selectedCert').text();
	name = name.substring(0,name.lastIndexOf(".dvsl"));
	$j('#editTemplateName').val(name);
}

function newTemplate(contactid){
	certtemplateservice.newCert($j('#newTemplateName').val(), $j('#templateImport').val(), $j('#templateDescription').val(), contactid, 
	{
		callback:function(result) {
			if(result.success == false)
			{
				var allErrors = '';
				// show all error messages
				for(var i = 0; i < result.errorList.length; i++)
				{
					var error = result.errorList[i];
					allErrors = allErrors + 'Updating ' + error.field + ' failed, ' + error.defaultMessage + '<br/>';
				}
				
				$j('#successResult').html(allErrors);
			}
			else
			{
				$j('#successResult').text(i18n.t("calibration:addedSuccessfully", "Added Successfully"));		

			}
		}
	}); 
	selectFunc(contactid);
}

function editTemplate(contactid){
	certtemplateservice.updateCert($j('#editTemplateName').val(), $j('#selectedCertId').text(), $j('#editTemplateDescription').val(), contactid,
	{
		callback:function(result) {
		if(result != null){
			if(result.success == false)
			{ 
				
				var allErrors = '';
				// show all error messages
				for(var i = 0; i < result.errorList.length; i++)
				{
					var error = result.errorList[i];
					allErrors = allErrors + i18n.t("calibration:errorEditTemplate", {varField : error.field, varMessage : error.defaultMessage, defaultValue : "Updating " + error.field + " failed, " + error.defaultMessage}) + ' <br/>';
				}
				
				$j('#editSuccessResult').html(allErrors);
			}
			else
			{
				$j('#editSuccessResult').text(i18n.t("calibration:updatedSuccessfully", "Updated Successfully"));		
				updateSelected();
			}
		}
		}
	});
}

function selectFunc(currentUserId){
	var certId=($j('input[name="templateID"]').val());
// If the user owns the lock and is changing to a different cert then remove the lock
	certtemplateservice.moveLock($j('#selectedCertId').text(), currentUserId, certId);

// Load the certificate
	certtemplateservice.loadCert(certId,{
		callback:function(result) {
		if(result.success==true){
			$j('#certReply').val(result.results);
			certtemplateservice.findCertTemplate(certId, {callback:function(result) { 
				if(result.description!=""){
					$j('#descriptionLine').removeClass('hid').addClass('vis');
					$j('#description').text(result.description);
				}else{
					$j('#descriptionLine').removeClass('vis').addClass('hid');
				}
			}});
		}else{
			$j.prompt(i18n.t("calibration:couldNotLoadTemplate", "Could not load template"));
		}
		}});

// Update the selected cert spans
	updateSelected();

	getArcCerts();

// Check if the user owns the lock on the cert and return the appropriate message
	checkLock(certId, currentUserId);

// Get the name and description for the edit form
	getEditFormInfo(certId);

}

function save(currentUserId){
	certtemplateservice.saveCert($j('#certReply').val(), $j('#selectedCertId').text(), currentUserId, {callback:function(result) {if(result.success==false){$j.prompt(i18n.t("calibration:templateSavingFailed", "Template was not saved successfully"));}}});
}

function render(workId){
	//throw up a loading gif
	$j('#pdf').html("<img style='display: block; margin-left: auto; margin-right: auto;' src='img/icons/ajax-loader.gif' />");
	certtemplateservice.renderPDF(workId, $j('#selectedCertId').text(), $j('#calType').val(),{
		callback:function(result) {
			if(result.success==true){
				$j('#pdf').html(result.results);
			}else{
				var renderedPDF = result.message;
				$j('#pdf').html(renderedPDF);
			}
		}});
}

function arcRender(workId){
	//expand the archived pdf display 
	$j('#arcpdf').parent().width('50%');
	$j('#arcpdf').html("<img style='display: block; margin-left: auto; margin-right: auto;' src='img/icons/ajax-loader.gif' />");
	archivedcerttemplateservice.renderArchivedPDF(workId, $j('#archiveSelect').val(), $j('#calType').val(),{
		callback:function(result) {
		if(result.success==true){
			$j('#arcpdf').html(result.results);
		}else{
			$j('#arcpdf').html(result.message);
		}
	}});
}

function add(){
	//toggle the add/edit div
	$j('#newTemplate').slideToggle(440);
	$j('#p1').toggleClass("hid");
	getEditFormInfo($j('#selectedCertId').text());
}

function previous(){
	$j('#previousTemplate').removeClass('hid').addClass('vis');
	$j('#p2').removeClass("hid");
	getArcCerts();
}


function previousSelect(){
	processArchivedCertDetails($j('#archiveSelect').val());
	archivedcerttemplateservice.loadArcCert($j('#archiveSelect').val(), 
			{callback:function(result) {
			if (result.success=true){
				$j('#arcCertReply').text(result.results);
			}else{
				$j.prompt(i18n.t("calibration:failedtoLoadTemplate", "Failed to load template"));
			}
		}});
	$j('#selectedArcCert').text($j('#currentVersion').text());
}

function getArcCerts(){
	archivedcerttemplateservice.getArchivedCertTemplateByCertTemplateById($j('#selectedCertId').text(), 
			{callback:function(result) {
				if(result.results.length==0){
					$j('#archiveSelectPrime').text(i18n.t("calibration:noArchiveTemplateAvailable", "No Archive Template Available"));
					$j('#arcrenderbutton').attr("disabled", true);
				}else{				
					$j('#archiveSelectPrime').html('Template name:<select id="archiveSelect" onchange="previousSelect()"></select>');
					
					$j.each(result.results, function(i) { 
						$j('#archiveSelect').append($j("<option></option>").attr("value",result.results[i].id).text(result.results[i].name+': '+result.results[i].formattedDate));
					});
					
					if($j('#previousTemplate').attr('class')=='vis'){
						previousSelect();
					}
					$j('#arcrenderbutton').attr("disabled", false);
				}
			}});
}

function loadTempEditor(wid){
	var address="templateeditor.htm?wid="+wid+"&tid="+$j("input[name='templateID']").val();
	window.location.href=address;
}