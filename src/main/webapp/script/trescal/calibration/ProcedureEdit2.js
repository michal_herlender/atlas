/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: Matt Toseland
*
*	FILENAME		:	ProcedureEdit2.js
*	DESCRIPTION		:	
*					: 	
*	DEPENDENCIES	:   script/antech/calibration/keynavigation/ProcedureBuilderKeyNav.js
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	12/05/2008 - MT - Created File
*					: 	16/05/2008 - SH - Add formulae functionality created.
*					:	03/11/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

var pageImports = new Array (	'script/thirdparty/jQuery/jquery.stopwatch.js',
								'script/antech/calibration/keynavigation/ProcedureBuilderKeyNav.js',
								'script/trescal/core/utilities/Menu.js',
								'script/thirdparty/jQuery/jquery.colorpicker.js');

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array ( { element: 'ul.subnavtab a', corner: 'small transparent top' },
								{ element: 'div.tab-box', corner: 'small tr bottom'},
								{ element: 'div.treeviewbox', corner: 'small'} );


/**
 * this variable is used to flag whether changes have been made to the page since loading
 */
var pageModified = false;

/**
 * variable to flag the picklist options div present?
 */ 
var picklistOptionsFlag = false;
/**
 * variable to flag the formula options div present?
 */
var formulaOptionsFlag = false;
/**
 * variable to flag the formula text options div present?
 */
var formulaTextFlag = false;

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{
	// load javascript file for changing submenu navigation included in pageimports (see above)
	
	// apply nifty corners to elements
	loadScript.loadNifty(niftyElements);
	
	// navigation has been collapsed so expand image needs to be
	// made the visible one.
	if($j('#navigation').css('display') == 'none')
	{
		// hide collapse link in header
		$j('#collapseLink').removeClass().addClass('hid');
		// show expand link in header
		$j('#expandLink').removeClass().addClass('vis');
	}
	
	// anchor with class name of jTip present in page?
	// If so, load jTip js file and css file 
	if ($j('a.jTip').length || $j('a.jconTip').length)
	{
		// load stylesheet using the dom
		loadScript.loadStyle('styles/jQuery/jquery-jtip.css', 'screen');
		// load the javascript file if not already
		loadScript.loadScripts(new Array ('script/thirdparty/jQuery/jquery.jtip.js'),
		{
			callback: function()
			{
				JT_init();							
			}
		});
	}
	
	// initialise the stopwatch
	$j('span#timeSinceSave').stopwatch({fontWeight: 'bold'});
}

/**
 * this method applies change events to all elements when a step folder is selected
 */
function applyChangeEvents(element)
{
	// attach color picker to all inputs containing class colorpicker
	$j('input.colorpicker').attachColorPicker();
	
	// bind onchange event to all textarea's on the page
	$j('#' + element + ' textarea').bind('keypress', function()
	{
		pageModified = true;
		// append unsaved changes text to floating div
		$j('#calBuildFloatDiv > div:last').empty().css({color: 'red'}).text(i18n.t("calibration:procUnsavedChanges", "Procedure Unsaved Changes"));
	});
	
	// bind onchange event to all select boxes on the page
	$j('#' + element + ' select').bind('change', function()
	{
		pageModified = true;
		// append unsaved changes text to floating div
		$j('#calBuildFloatDiv > div:last').empty().css({color: 'red'}).text(i18n.t("calibration:procUnsavedChanges", "Procedure Unsaved Changes"));
	});
	
	// bind onclick event to all input's of type checkbox and button on page
	$j('#' + element + ' input[type="checkbox"], #' + element + ' input[type="button"], button').bind('click', function()
	{
		pageModified = true;
		// append unsaved changes text to floating div
		$j('#calBuildFloatDiv > div:last').empty().css({color: 'red'}).text(i18n.t("calibration:procUnsavedChanges", "Procedure Unsaved Changes"));
	});
	
	// bind onclick event to all input's of type text on page
	$j('#' + element + ' input[type="text"]').bind('keydown', function()
	{
		pageModified = true;
		// append unsaved changes text to floating div
		$j('#calBuildFloatDiv > div:last').empty().css({color: 'red'}).text(i18n.t("calibration:procUnsavedChanges", "Procedure Unsaved Changes"));
	});
}

/**
 * shows a confirmation message if the user is trying to navigate away from the page if
 * changes have been made without saving.
 */
window.onbeforeunload = function()
{
	if(pageModified)
	{
		pageModified = true;
		return i18n.t("calibration:alertLeavingPage", "Your changes have not been saved.\n\nIf you wish to save your changes then click the save button before leaving the page.");
	}
}

/**
 * this method retrieves all method names from the provided class name and appends them to a select form element.
 * 
 * @param {String} type either 'formula' or 'validator' depending on the formula use
 * @param {Object} className name of the class from which methods should be retrieved
 * @param {Integer} step number of the current step
 * @param {Integer} indexNo index no used to create id. If formula 'formulaNo' or if validator 'inputNo'	
 * @param {Object} classChange boolean value used to indicate if the class name has changed
 */
function getMethods(type, className, step, indexNo, classChange)
{
	// formula type being created/edited?
	if (type == 'formulae')
	{
		// create formulae method id
		var id = 'steps\\[' + step + '\\]\\.formulae\\[' + indexNo + '\\]\\.method';
	}
	else
	{
		// create validator method id
		var id = 'steps\\[' + step + '\\]\\.inputList\\[' + indexNo + '\\]\\.validatorFormula\\.method';
	}
	
	if (($j('select#' + id + ' option').length == 1) && ($j('select#' + id + ' option:first').is(':selected')) || classChange == true)
	{
		// empty select box and append loading message
		$j('select#' + id).empty().append('<option value="">' + i18n.t("loading", "Loading....") + '</option>');
		// variable to hold new options
		var content = '';
		// call dwr method to retrieve methods
		proceditservice.getMethods(className,
		{
			callback: function(methods)
			{
				// empty select box
				$j('select#' + id).empty();
				// add option for each method
				$j.each(methods, function(i){
					
					// add methods to string
	 				content += '<option value="' + methods[i] +'"';
	 				
	 				// select first option
					if(i == 0)
					{
						content += ' selected="selected"';
					}
					
	 				content += '>' + methods[i] +'</option>';
				});
				// append new options
				$j('select#' + id).append(content);
				// formula type being created/edited?
				if (type == 'formulae') 
				{
					// load formula options
					loadFormulaOptions('formulae', step, indexNo);
				}
				else
				{
					// load formula options
					loadFormulaOptions('validator', step, indexNo);
				}
			}
		});
	}
	else
	{
		// do nothing
	}	
} 


/**
declarations for tab control
*/	
	var menuElements3 = new Array( 	{ anchor: 'treeview-link', block: 'treeview' },
          							{ anchor: 'preview-link', block: 'preview' },
           							{ anchor: 'xmlcontent-link', block: 'xmlcontent' } );
	
	function setContent(xmlcontrol) {
		var xmlTextArea = document.getElementById(xmlcontrol);
		xml = xmlTextArea.value;
		proceditservice.setContent(xml);		
	}
	
	function parse(xmlcontrol) {
		var xmlTextArea = document.getElementById(xmlcontrol);
		xml = xmlTextArea.value;
	
		proceditservice.parseXMLString(xml,parseResult);				
	}
	
	function parseResult( dfs) {
		var resultsTextArea = document.getElementById("valerrors");
		resultsTextArea.value = dfs;
	}
	
	/**
	save output xml from the backing object 
	*/
	function saveXMLOutput() {
		outFile = document.getElementById("outputFile").value;
		proceditservice.getXMLDoc(outFile);
	}
	
	// array to hold ids of loaded steps
	var loadedStepsArray = [];
	
	// This method modified from Tom Duffy's tutorial at http://gethelp.devx.com/techtips/dhtml_pro/10min/10min0702/td072602-4.asp
	function showBranch(branch)
	{
		var objBranch = document.getElementById(branch).style;
		if(objBranch.display=="block")
			objBranch.display="none";
		else
			objBranch.display="block";
		// add change events to all elements within step
		if (branch.indexOf('stepbranch') != -1)
		{
			var loaded = false;
			$j.each(loadedStepsArray, function(i)
			{
				if (loadedStepsArray[i] == branch)
				{
					loaded = true;
				}
			});
			if (loaded == false)
			{
				// apply change events
				applyChangeEvents(branch);
				// put new step id into loaded array
				loadedStepsArray.push(branch);
			}			
		}
	}
	
	// image used for tree menu
	var openImg = new Image();
	openImg.src = 'img/icons/folder.png';
	
	var closedImg = new Image();
	closedImg.src = 'img/icons/folder_add.png';

/**
 * this method changes the src of image supplied
 * 
 * @param {Object} objImg the image object
 */ 
function swapFolder(objImg)
{
	if($j(objImg).attr('src').indexOf('img/icons/folder_add.png') > -1)
	{
		$j(objImg).attr('src', openImg.src);
	}
	else
	{
		$j(objImg).attr('src', closedImg.src);
	}			
}

/**
 * this method creates a div that contains all available picklist id's. This div is floated above the element
 * found using the elementId passed. This facilitates easy population of picklist input field.
 * 
 * @param {String} elementId the id of element to float box above and populate with result
 */
function showPicklistBox(elementId)
{	
	// if picklist options div already present on page
	if ($j('div#picklistOptions').length)
	{
		// picklist options in use?
		if (picklistOptionsFlag == false)
		{
			// remove from page
			$j('div#picklistOptions').remove();
		}
	}
	
	// get the location of current input on page (top, left)
	var offset = $j('#' + elementId).offset();
	// append the picklist options div to page
	var content =	'<div id="picklistOptions" onclick=" picklistOptionsFlag = false; setOptionsVisibility(); " onmouseover=" picklistOptionsFlag = true; " onmouseout=" picklistOptionsFlag = false; ">' +
						'<fieldset>' +
							'<ol>' +
								'<li>' +
									'<label>' + i18n.t("calibration:pickListLabel", "Picklist:") + '</label>' +
									'<select onclick=" event.cancelBubble = true; " onchange=" $j(\'#' + elementId + '\').val(this.value); picklistOptionsFlag = false; setOptionsVisibility(); ">' +
										'<option value="">' + i18n.t("calibration:pickList", "Picklist") + '</option>';
										$j('span#all_picklist li span[id^="picklist"]').each(function(i, n)
										{
											content = content + '<option value="' + $j(n).find('input[type="text"]:first').val() + '">' + $j(n).find('input[type="text"]:first').val() + '</option>';
										});
				content = content +	'</select>' +
								'</li>' +
							'</ol>' +
						'</fieldset>' +				
					'</div>';
	// append the picklist options div to page
	$j('body').append(content);
	// locate the picklist options div above input field
	$j('div#picklistOptions').css({ position: 'absolute', top: (offset.top - 74), left: offset.left });
}

/**
 * this method highlights the object which has been selected to be deleted by adding a background colour
 * of red.
 * 
 * @param {Object} checkbox the checkbox which has been clicked
 * @param {Object} object the element which is to be highlighted
 */
function highlightDeletion(checkbox, object, object2)
{
	// is checkbox checked?
	if ($j(checkbox).is(':checked'))
	{
		// add background colour to element
		$j(object).css({backgroundColor: '#FF5B5B'});
		// secondary object to highlight?
		if (object2)
		{
			// highlight secondary object
			$j(object2).css({backgroundColor: '#FF5B5B'});
		}
	} 
	else
	{
		// reset background colour of element
		$j(object).css({backgroundColor: 'transparent'});
		// secondary object to highlight?
		if (object2)
		{
			// highlight secondary object
			$j(object2).css({backgroundColor: 'transparent'});
		}
	} 
}

/**
 * this method checks for formulae options boxes and closes if necessary.
 */
function setOptionsVisibility()
{
	// formula options div already present on page?
	if ($j('div#formulaeOptions').length)
	{
		// formula options in use?
		if (formulaOptionsFlag == false)
		{
			// remove from page
			$j('div#formulaeOptions').remove();
		}
	}
	// formula text options div already present on page?
	if ($j('div#formulaeTextOptions').length)
	{
		// formula text options div in use?
		if (formulaTextFlag == false)
		{
			// remove from page
			$j('div#formulaeTextOptions').remove();
		}
	}
	// if picklist options div already present on page
	if ($j('div#picklistOptions').length)
	{
		// picklist options in use?
		if (picklistOptionsFlag == false)
		{
			// remove from page
			$j('div#picklistOptions').remove();
		}
	}
}

/**
 * this method creates the string list of parameters to be passed with the selected formula.
 * 
 * @param {String} type either 'formula' or 'validator' depending on the formula use
 * @param {Integer} step number of the current step
 * @param {Integer} indexNo index no used to create id. If formula 'formulaNo' or if validator 'inputNo'
 */
function createFormulaString(type, step, indexNo)
{
	// check formulae options visibility
	setOptionsVisibility();
	// formula type being created/edited?
	if (type == 'formulae')
	{
		// create display equation parameters id
		var paramsId = 'steps\\[' + step + '\\]\\.formulae\\[' + indexNo + '\\]\\.methodParams';
		// create hidden input id
		var inputId = 'steps\\[' + step + '\\]\\.formulae\\[' + indexNo + '\\]\\.input';
	}
	else
	{
		// create display equation parameters id
		var paramsId = 'steps\\[' + step + '\\]\\.inputList\\[' + indexNo + '\\]\\.validatorFormula\\.methodParams';
		// create hidden input id
		var inputId = 'steps\\[' + step + '\\]\\.inputList\\[' + indexNo + '\\]\\.validatorFormula\\.input';
	}
	// empty the hidden input value
	$j('#' + inputId).val('');
	// add new values from parameter input fields to hidden input string
	$j('#' + paramsId + ' table tbody input').each( function(i)
	{
		// this is first input field
		if (i == 0)
		{
			// input field has value
			if (this.value != '')
			{
				// add input field value to hidden input string
				$j('#' + inputId).val($j('#' + inputId).val() + this.value);
			}
		}
		else
		{
			// add input field value to hidden input string comma delimited
			$j('#' + inputId).val($j('#' + inputId).val() + ',' + this.value);
		}
	});	
}

/**
 * this method creates a repeating formula string in parameter input field
 * 
 * @param {String} type either 'formula' or 'validator' depending on the formula use
 * @param {Integer} step number of the current step
 * @param {Integer} indexNo index no used to create id. If formula 'formulaNo' or if validator 'inputNo'
 * @param {String} repeatVal the value from select element to be added to repeat param string
 */
function createRepeatingFormulaString(type, step, indexNo, repeatVal)
{
	// formula text options div already present on page?
	if ($j('div#formulaeTextOptions').length)
	{
		// formula text options div in use?
		if (formulaTextFlag == false)
		{
			// remove from page
			$j('div#formulaeTextOptions').remove();
		}
	}
	// formula type being created/edited?
	if (type == 'formulae')
	{
		// create input id
		var inputId = 'steps' + step + '_formulae' + indexNo + '_methodParam0';
	}
	else
	{
		// create input id
		var inputId = 'steps' + step + '_validator' + indexNo + '_methodParam0';
	}
	// input has content?
	if ($j('input#' + inputId).val() != '')
	{
		// add comma and value to input field
		$j('input#' + inputId).val($j('input#' + inputId).val() + ',' + repeatVal);
	}
	else
	{
		// add content to input field
		$j('input#' + inputId).val($j('input#' + inputId).val() + repeatVal);
	}
	// call method to create hidden input string
	createFormulaString(type, step, indexNo);
}

/**
 * this method creates a repeating string in an input field
 * 
 * @param {String} inputId the id of the input field value should be added to
 * @param {String} repeatVal the value to be added to input field
 */
function createGenRepeatingString(inputId, repeatVal)
{
	// formula text options div already present on page?
	if ($j('div#formulaeTextOptions').length)
	{
		// formula text options div in use?
		if (formulaTextFlag == false)
		{
			// remove from page
			$j('div#formulaeTextOptions').remove();
		}
	}
	// input has content?
	if ($j('input#' + inputId).val() != '')
	{
		// add comma and value to input field
		$j('input#' + inputId).val($j('input#' + inputId).val() + ',' + repeatVal);
	}
	else
	{
		// add content to input field
		$j('input#' + inputId).val($j('input#' + inputId).val() + repeatVal);
	}
	// check formulae options visibility
	setOptionsVisibility();
}

// array of id's which is used to check when loading a new formula options section
// that it does not already exist
var loadedFormulaOptions = [];

/**
 * this method loads the chosen (class + method) summary and the parameters which are needed
 * to run the formula.
 * 
 * @param {String} type either 'formula' or 'validator' depending on the formula use
 * @param {Integer} step number of the current step
 * @param {Integer} indexNo index no used to create id. If formula 'formulaNo' or if validator 'inputNo'
 */
function loadFormulaOptions(type, step, indexNo)
{	
	// formula type being created/edited?
	if (type == 'formulae')
	{
		// get value of equation select
		var equation = $j('#steps\\[' + step + '\\]\\.formulae\\[' + indexNo + '\\]\\.equation').val();
		// get value of method select
		var method = $j('#steps\\[' + step + '\\]\\.formulae\\[' + indexNo + '\\]\\.method').val();
		// create display equation summary id
		var summaryId = 'steps\\[' + step + '\\]\\.formulae\\[' + indexNo + '\\]\\.methodSummary';
		// create display equation parameters id
		var paramsId = 'steps\\[' + step + '\\]\\.formulae\\[' + indexNo + '\\]\\.methodParams';
		// create hidden input id
		var inputId = 'steps\\[' + step + '\\]\\.formulae\\[' + indexNo + '\\]\\.input';
	}
	else
	{
		// get value of equation select
		var equation = $j('#steps\\[' + step + '\\]\\.inputList\\[' + indexNo + '\\]\\.validatorFormula\\.equation').val();
		// get value of method select
		var method = $j('#steps\\[' + step + '\\]\\.inputList\\[' + indexNo + '\\]\\.validatorFormula\\.method').val();
		// create display equation summary id
		var summaryId = 'steps\\[' + step + '\\]\\.inputList\\[' + indexNo + '\\]\\.validatorFormula\\.methodSummary';
		// create display equation parameters id
		var paramsId = 'steps\\[' + step + '\\]\\.inputList\\[' + indexNo + '\\]\\.validatorFormula\\.methodParams';
		// create hidden input id
		var inputId = 'steps\\[' + step + '\\]\\.inputList\\[' + indexNo + '\\]\\.validatorFormula\\.input';
	}	
	// variable to indicate if method summary loaded already
	var loaded = false;
	// check all array entries for combination of summaryId and equation
	$j.each(loadedFormulaOptions, function(i){
		// we have an id and equation match?
		if ((summaryId == loadedFormulaOptions[i].summaryId) && (equation == loadedFormulaOptions[i].equation))
		{
			// set loaded to true
			loaded = true;
		}
		
	});
	// this section has not been loaded already with this combination? 
	if (loaded == false) 
	{
		// empty the formula summary and parameter list
		$j('#' + summaryId).empty();
		$j('#' + paramsId).empty();
		// call dwr service to get method summary and parameters
		proceditservice.getMethodSummary(equation, method, methodAnnotation, argAnnotation,
		{
			callback: function(annotation)
			{
				// error message returned?
				if (annotation.errorMessage != null) 
				{
					alert(annotation.errorMessage);
				}
				else 
				{
					// check array for summaryId, if found alter entry for this summaryId/equation combination
					$j.each(loadedFormulaOptions, function(x)
					{
						// summary id present in array
						if (summaryId == loadedFormulaOptions[x].summaryId)
						{
							// alter array entry to this combination
							loadedFormulaOptions[x] = ({
								summaryId: summaryId,
								equation: equation
							});
						}
					});
					// add method summary to page
					$j('#' + summaryId).text(annotation.methodSummary);
					// split parameters into array
					var methodParams = annotation.methodParameters.split(',');
					// variable to indicate if this is a repeating parameter
					var repeatParam = false;
					// is this a repeating parameter
					if (methodParams[0] == 'repeating')
					{
						// set repeatParam variable
						repeatParam = true;
						// create variable for parameter content
						var content = '';
						// add parameter text and input for each parameter in array
						for (i = 0; i < methodParams.length; i++) 
						{
														
							content = content + '<tr>' +
													'<td width="40%">' +
														methodParams[i] +
													'</td>' +
													'<td width="60%">' +
														'<input type="text" id="steps' + step +	'_' + type + indexNo + '_methodParam' +	i +	'" style=" width: 400px; " autocomplete="off" value="';
														// hidden input field has value
														if ($j('#' + inputId).val() != '') 
														{
															// add hidden input value
															content = content + $j('#' + inputId).val();	
														}
														content = content + '" onfocus=" showBox(\'' + type + '\', this.id, ' + step + ', ' + indexNo + ', ' + repeatParam + '); return false; " onkeyup=" searchParamOptions(\'' + type + '\', this.value, this.id, ' + step + ', ' + indexNo + ', ' + repeatParam + '); return false; " onblur=" if (procBuildKeyPress == true){}else{ createFormulaString(\'' + type + '\', ' + step + ', ' + indexNo + '); return false; } " /></td>' +
												'</tr>';
						}
						// append parameter values
						$j('#' + paramsId).append(	'<table>' +
														'<tbody>' +
															content +
														'</tbody>' +
													'</table>');
					}
					else
					{
						// split saved parameters into array
						var paramArray = $j('input#' + inputId).val().split(',');
						// empty the hidden input value
						$j('#' + inputId).val('');
						// create variable for parameter content
						var content = '';
						// add parameter text and input for each parameter in array
						for (i = 0; i < methodParams.length; i++) 
						{
							// replace any occurences of '<' or '>' in parameter text
							var stripLT = methodParams[i].replace('<', '&lt;');
							var stripGT = stripLT.replace('>', '&gt;');
							var param = stripGT;
							
							content = content + '<tr>' +
													'<td width="40%">' +
														param +
													'</td>' +
													'<td width="60%">' +
														'<input type="text" id="steps' + step +	'_' + type + indexNo + '_methodParam' +	i +	'" style=" width: 400px; " autocomplete="off" value="';
														if (paramArray[i]) 
														{
															content = content + paramArray[i];
															// this is first input field
															if (i == 0) 
															{
																// input field has value
																if (paramArray[i] != '') 
																{
																	// add input field value to hidden input string
																	$j('#' + inputId).val($j('#' + inputId).val() + paramArray[i]);
																}
															}
															else 
															{
																// add input field value to hidden input string comma delimited
																$j('#' + inputId).val($j('#' + inputId).val() + ',' + paramArray[i]);
															}
														}
														content = content + '" onfocus=" showBox(\'' + type + '\', this.id, ' + step + ', ' + indexNo + ', ' + repeatParam + '); return false; " onkeyup=" searchParamOptions(\'' + type + '\', this.value, this.id, ' + step + ', ' + indexNo + ', ' + repeatParam + '); return false; " onblur=" if (procBuildKeyPress == true){}else{ createFormulaString(\'' + type + '\', ' + step + ', ' + indexNo + '); return false; } " /></td>' +
												'</tr>';
						}
						// append parameter values
						$j('#' + paramsId).append(	'<table>' +
														'<tbody>' +
															content +
														'</tbody>' +
													'</table>');	
					}					
				}
				// bind onclick event to all input's of type text on page
				$j('#' + paramsId + ' input[type="text"]').bind('blur', function()
				{
					pageModified = true;
					// append unsaved changes text to floating div
					$j('#calBuildFloatDiv > div:last').empty().css({
						color: 'red'
					}).text(i18n.t("calibration:procUnsavedChanges", "Procedure Unsaved Changes"));
				});
			}
		});
	}
}

/**
 * this method monitors the characters being entered into an instruction field. If the symbols '@' or '$' are
 * entered then display the formulae options box to facilitate input/formula entry.
 * 
 * @param {String} inputFragment the string currently entered into the instruction field
 * @param {Integer} elementId the id of the field being monitored
 * @param {Integer} step the current step number
 */
function monitorInstructionInput(inputFragment, elementId, step)
{
	// get the last character entered
	var lastCharacter = inputFragment.charAt(inputFragment.length - 1);
	// last character '@' or '$'?
	if (lastCharacter == '@' || lastCharacter == '$')
	{
		// show the formulae options box
		showBox('instruction', elementId, step, 0, false);
	}
	else
	{
		// hide any options boxes
		setOptionsVisibility();
	}
}

/**
 * this method creates a div that contains all available inputs, functions and nominal files. This div is 
 * floated above the element found using the elementId passed. This facilitates easy population of method 
 * parameter input fields and instructions.
 * 
 * @param {String} type either 'formula', 'validator' or instruction
 * @param {String} elementId the id of the element we are appending the div and any value selected to
 * @param {Integer} step number of the current step
 * @param {Integer} indexNo index no used to create id. If formula 'formulaNo' or if validator 'inputNo'
 * @param {Boolean} repeatParam this indicates that the formula/validator requires one parameter with repeating values (i.e @1.2,@1.3,@1.4)
 */
function showBox(type, elementId, step, indexNo, repeatParam)
{	
	// if formulae options div already present on page
	if ($j('div#formulaeOptions').length)
	{
		// formula options in use?
		if (formulaOptionsFlag == false)
		{
			// remove from page
			$j('div#formulaeOptions').remove();
		}
	}
	// is this repeating parameter?
	if (repeatParam == true)
	{
		if (type == 'generalRepeater')
		{
			var selectChangeEvent = 'onchange=" formulaOptionsFlag = false; createGenRepeatingString(\'' + elementId + '\', this.value); "';
			var divClickEvent = 'onclick=" formulaOptionsFlag = false; setOptionsVisibility(); "';
		}
		else
		{
			var selectChangeEvent = 'onchange=" formulaOptionsFlag = false; createRepeatingFormulaString(\'' + type + '\', ' + step + ', ' + indexNo + ', this.value); "';
			var divClickEvent = 'onclick=" formulaOptionsFlag = false; createFormulaString(\'' + type + '\', ' + step + ', ' + indexNo + '); "';	
		}
	}
	else if (type == 'instruction')
	{
		var selectChangeEvent = 'onchange=" $j(\'#' + elementId + '\').val($j(\'#' + elementId + '\').val().substring(0, $j(\'#' + elementId + '\').val().length - 1) + this.value).focus(); formulaOptionsFlag = false; setOptionsVisibility(); "';
		var divClickEvent = 'onclick=" formulaOptionsFlag = false; setOptionsVisibility(); "';
	}
	else
	{
		var selectChangeEvent = 'onchange=" $j(\'#' + elementId + '\').val(this.value); formulaOptionsFlag = false; createFormulaString(\'' + type + '\', ' + step + ', ' + indexNo + '); $j(\'#' + elementId + '\').parent().parent().next().find(\'input:first\').focus(); "';
		var divClickEvent = 'onclick=" formulaOptionsFlag = false; createFormulaString(\'' + type + '\', ' + step + ', ' + indexNo + '); "';
	}
	
	// get the location of current input on page (top, left)
	var offset = $j('#' + elementId).offset();
	// append the function options div to page
	var content =	'<div id="formulaeOptions" ' + divClickEvent + ' onmouseover=" formulaOptionsFlag = true; " onmouseout=" formulaOptionsFlag = false; ">' +
						'<fieldset>' +
							'<ol>' +
								'<li>' +
									'<label>' + i18n.t("calibration:inputLabel", "Input:") + '</label>' +
									'<select onclick=" event.cancelBubble = true; " ' + selectChangeEvent + '>' +
										'<option value="">' + i18n.t("calibration:input", "Input") + '</option>';
										$j.each(inputListArray, function(i){
											content = content + '<option value="' + inputListArray[i].id + '">' + inputListArray[i].id + ' - ' + inputListArray[i].label + '</option>';
										});
				content = content +	'</select>' +
								'</li>' +
								'<li>' +
									'<label>' + i18n.t("calibration:formulaLabel", "Formula:") + '</label>' +
									'<select onclick=" event.cancelBubble = true; " ' + selectChangeEvent + '>' +
										'<option value="">' + i18n.t("calibration:formula", "Formula") + '</option>';
										$j.each(formulaListArray, function(i){
											content = content + '<option value="' + formulaListArray[i].id + '">' + formulaListArray[i].id + ' - ' + formulaListArray[i].label + '</option>';
										});
				if (type != 'instruction')
				{
					content = content + '</select>' +
								'</li>' +
								'<li>' +
									'<label>' + i18n.t("calibration:nominalLabel", "Nominal:") + '</label>' +
									'<select onclick=" event.cancelBubble = true; "  ' + selectChangeEvent + '>' +
										'<option value="">' + i18n.t("calibration:nominal", "Nominal") + '</option>';
										$j.each(nominalListArray, function(i){
											content = content + '<option value="' + nominalListArray[i] + '">' + nominalListArray[i] + '</option>';
										});
				}				
				content = content + '</select>' +
								'</li>' +
							'</ol>' +
						'</fieldset>' +				
					'</div>';
	// append the function options div to page
	$j('body').append(content);
	// type is instruction?
	if (type == 'instruction')
	{
		// locate the function options div above input field
		$j('div#formulaeOptions').css({ height: '120px', position: 'absolute', top: (offset.top - 122), left: offset.left });
	}
	else
	{
		// locate the function options div above input field
		$j('div#formulaeOptions').css({ position: 'absolute', top: (offset.top - 152), left: offset.left });
	}	
}

/**
 * this method allows the user to search available inputs, functions and nominal files by typing into the 
 * method parameter input field.  Any matches will be displayed in a div under the input field.  This method also
 * uses a key navigation file called (ProcedureBuilderKeyNav.js).
 * 
 * @param {String} type either 'formula' or 'validator' depending on the formula use
 * @param {Object} nameFragment string value obtained from the input field
 * @param {String} elementId id of the element we are appending the div and any value selected to
 * @param {Integer} step number of the current step
 * @param {Integer} indexNo index no used to create id. If formula 'formulaNo' or if validator 'inputNo'
 * @param {Boolean} repeatParam this indicates that the formula/validator requires one parameter with repeating values (i.e @1.2,@1.3,@1.4)
 */
function searchParamOptions(type, nameFragment, elementId, step, indexNo, repeatParam)
{
	// populate variable in key navigation file with current input id (ProcedureBuilderKeyNav.js)
	inputIdKeyNav = elementId;
	// check variable in key navigation file (ProcedureBuilderKeyNav.js)
	if (procBuildKeyPress == true)
	{
		procBuildKeyPress = false;
	}
	else 
	{
		// text fragment passed empty
		if (nameFragment.length < 1)
		{
			// remove from page
			$j('div#formulaeTextOptions').remove();
		}
		else
		{
			// define variable for matches
			var results = '';
			// search combined array for text matches
			$j.each(combinedSearchArray, function(i)
			{
				if (combinedSearchArray[i].id.substring(0, nameFragment.length) == nameFragment) 
				{
					var displayText = combinedSearchArray[i].id + ' - ' + combinedSearchArray[i].label;
					if (combinedSearchArray[i].label == '')
					{
						displayText = combinedSearchArray[i].id;
					}
					results = results + '<a href="#" id="' + combinedSearchArray[i].id + '" onmousedown=" event.cancelBubble = true; $j(\'#' + elementId + '\').val(this.id); formulaTextFlag = false; createFormulaString(\'' + type + '\', ' + step + ', ' + indexNo + '); $j(\'#' + elementId + '\').parent().parent().next().find(\'input:first\').focus(); ">' + displayText + '</a>';
				}
			});
			// results to show?
			if (results.length) 
			{
				// formulae text options div already present on page
				if ($j('div#formulaeTextOptions').length) 
				{
					// empty div
					$j('#formulaeTextOptions').empty().append(results);
				}
				else 
				{
					// get the location of current input on page (top, left)
					var offset = $j('#' + elementId).offset();
					// create div content for results
					var content = 	'<div id="formulaeTextOptions">' +
										results +
									'</div>';
					// append content to page
					$j('body').append(content);
					// locate the function options div above input field
					$j('div#formulaeTextOptions').css({
						position: 'absolute',
						top: (offset.top + 22),
						left: offset.left
					});
				}
				// select the first anchor
				$j('div#formulaeTextOptions a:first').addClass('selected');
				// reset the selected key navigation variable (ProcedureBuilderKeyNav.js)
				procBuildSelected = 0;
			}
			else
			{
				// remove the formula text options div from page
				$j('div#formulaeTextOptions').remove();
			}
		}
	}
}
 