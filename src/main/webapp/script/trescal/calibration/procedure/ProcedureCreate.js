var instructionno = 0;
var stepno = 0;
var inputno = 0;

function addInput()
{
	var inputLabel = document.createElement('input');
	inputLabel.setAttribute('id','inputs['+inputno+'].label');
	inputLabel.setAttribute('name','inputs['+inputno+'].label');
	inputLabel.setAttribute('type','text');

	var inputName = document.createElement('input');
	inputName.setAttribute('id','inputs['+inputno+'].name');
	inputName.setAttribute('name','inputs['+inputno+'].name');
	inputName.setAttribute('type','text');

	var inputDefaultValue = document.createElement('input');
	inputDefaultValue.setAttribute('id','inputs['+inputno+'].defaultValue');
	inputDefaultValue.setAttribute('name','inputs['+inputno+'].defaultValue');
	inputDefaultValue.setAttribute('type','text');

	var inputDataType = document.createElement('select');
	inputDataType.setAttribute('id','inputs['+inputno+'].datatype');
	inputDataType.setAttribute('name','inputs['+inputno+'].datatype');
	inputDataType.options[0] = new Option('bigdecimal','Big Decimal');
	inputDataType.options[1] = new Option('string','String');

	var inputNullable = document.createElement('input');
	inputNullable.setAttribute('id','inputs['+inputno+'].nullable');
	inputNullable.setAttribute('name','inputs['+inputno+'].nullable');
	inputNullable.setAttribute('type','checkbox');

	var inputs = document.getElementById('inputs');
	inputs.appendChild(document.createTextNode('Input '+inputno));
	inputs.appendChild(document.createElement('br'));

	inputs.appendChild(document.createTextNode('Label '));
	inputs.appendChild(inputLabel);
	inputs.appendChild(document.createElement('br'));

	inputs.appendChild(document.createTextNode('Name '));
	inputs.appendChild(inputName);
	inputs.appendChild(document.createElement('br'));

	inputs.appendChild(document.createTextNode('Default Value '));
	inputs.appendChild(inputDefaultValue);
	inputs.appendChild(document.createElement('br'));

	inputs.appendChild(document.createTextNode('Datatype '));
	inputs.appendChild(inputDataType);
	inputs.appendChild(document.createElement('br'));

	inputs.appendChild(document.createTextNode('Nullable '));
	inputs.appendChild(inputNullable);
	inputs.appendChild(document.createElement('br'));

	inputs.appendChild(document.createElement('hr'));

	inputno = inputno + 1;
}

function addInstruction()
{
	var instruction = document.createElement('input');
	instruction.setAttribute('id','instructions['+instructionno+'].instruction');
	instruction.setAttribute('name','instructions['+instructionno+'].instruction');
	instruction.setAttribute('type','text');

	var instructions = document.getElementById('instructions')
	instructions.appendChild(document.createTextNode('Instruction '+instructionno+' '));
	instructions.appendChild(instruction);
	instructions.appendChild(document.createElement('br'));

	instructionno = instructionno + 1;
}

function finishProcedure()
{
	document.getElementById('finish').value = "true";
	document.f1.submit();

}