/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	StepViewKeynav.js
*	DESCRIPTION		:	This file allows the user to navigate through the calibration runner using the navigation
					: 	cursor keys.
*	DEPENDENCIES	:   script/thirdparty/jQuery/jquery.js
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	06/12/2007 - SH - Created File
*
****************************************************************************************************************************************/


/**
 * start jquery keypress observer to check if the user has pressed the left/right arrow
 */
$j(document).keydown( function(e){

    var event = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
	
	// ctrl key is being pressed
	if (e.ctrlKey)
	{		
		// see if the left arrow (37) or right arrow (39) has been pressed?
		switch (event)
		{			
			// left arrow key has been pressed
			case 37: // call function to send user to the next page
					 formAction('prev');
					 break;
					 
			// right arrow key has been pressed
			case 39: // call function to send user to next page
					 formAction('next');					
					 break;
					 
			// B has been pressed
			//case 66: // call batch function
			//		 $j('a#continueBatch').trigger('onclick');
			//		 break;
		}
	}
});