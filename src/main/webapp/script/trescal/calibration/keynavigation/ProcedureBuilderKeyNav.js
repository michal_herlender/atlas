/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: Stuart Harrold
*
*	FILENAME		:	ProcedureBuilderKeyNav.js
*	DESCRIPTION		:	
*					: 	
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	16/05/2008 - SH - Created File
*
****************************************************************************************************************************************/

// variable to hold the currently selected anchor
var procBuildSelected = 0;
// variable to check for key presses
var procBuildKeyPress = false;
// variable used to store current input being used
var inputIdKeyNav = '';

$j(document).keydown(function(e) 
{
    var event = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
	
	switch (event)
	{
		//  up arrow key has been pressed
		case 38: procBuildKeyPress = true;
				 // check selected variable
				 if (procBuildSelected == 0)
				 {
				 	
				 }
				 // selected variable contains value greater than zero
				 else
				 {
				 	 // remove 'selected' class name from the currently selected element and add to the previous one, apply focus
					 $j('#formulaeTextOptions').find('> a').eq(procBuildSelected).removeClass().prev().addClass('selected').focus();
					 // remove one from the selected variable value
					 procBuildSelected = procBuildSelected - 1;
					 // then return focus to the input field
					 $j('#' + inputIdKeyNav).focus();
				 }
				 break;
				 
		// down arrow key has been pressed
		case 40: procBuildKeyPress = true;
				 // check selected variable is not greater than list length
				 if (procBuildSelected == $j('#formulaeTextOptions').find('> a').size() - 1)
				 {
				 
				 }
				 // selected variable contains value less than list length
				 else
				 {
				 	// remove 'selected' class name from the currently selected element and add to the next one, apply focus
					$j('#formulaeTextOptions').find('> a').eq(procBuildSelected).removeClass().next().addClass('selected').focus();
					// remove one from the selected variable value
					procBuildSelected = procBuildSelected + 1;
					// then return focus to the input field
					$j('#' + inputIdKeyNav).focus();
				 }
				 break;
				 
		// escape key has been pressed
		case 27: procBuildKeyPress = true;
				 break;
				 
		// enter key has been pressed
		case 13: procBuildKeyPress = true;
				 // trigger click event of anchor
			 	 $j('#formulaeTextOptions').find('> a').eq(procBuildSelected).trigger('mousedown');
				 break;
	}
	
});