/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	StepView.js
*	DESCRIPTION		:	Page specific javascript file. Loads all necessary javascript files and applies nifty corners.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	06/12/2007 - SH - Created File
*					:	03/11/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page.
 */
var pageImports = new Array (	'script/trescal/core/utilities/Menu.js', 
								'script/antech/calibration/keynavigation/StepViewKeynav.js',
								'dwr/interface/actionoutcomeservice.js',
								'dwr/interface/calibrationservice.js',
								'script/thirdparty/jQuery/jquery.thickbox.js' );

/** 
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = new Array( { anchor: 'procedure-link', block: 'procedure-tab' },
					      	  { anchor: 'procsummary-link', block: 'procsummary-tab' } );

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{
	// apply nifty corners to elements
	loadScript.loadNifty(niftyElements);
	// navigation has been collapsed so expand image needs to be
	// made the visible one.
	if($j('#navigation').css('display') == 'none')
	{
		// hide collapse link in header
		$j('#collapseLink').removeClass().addClass('hid');
		// show expand link in header
		$j('#expandLink').removeClass().addClass('vis');
	}	
	// anchor with class name of jTip present in page?
	// If so, load jTip js file and css file 
	if ($j('a.jTip').length || $j('a.jconTip').length)
	{
		// load stylesheet using the dom
		loadScript.loadStyle('styles/jQuery/jquery-jtip.css', 'screen');
		// load the javascript file if not already
		loadScript.loadScripts(new Array ('script/thirdparty/jQuery/jquery.jtip.js'),
		{
			callback: function()
			{
				JT_init();							
			}
		});
	}
	// check if the page has a frame as parent which means that this page is being viewed
	// as a preview on the procedure editing page.
	if (parent.frames.length > 0)
	{
		// hide all div children of the body tag except the last 
		$j('body > div').not(':last').hide();
		// hide the footer div within the maincontent div
		$j('div.footer').hide();
	}
	// apply focus to form element with id of first element
	$j('#firstelement').focus();
	// add event to document which checks for keypresses
	$j(document).keydown( function(e){

	    var event = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
		
		// see if the enter key (13) has been pressed?
		switch (event)
		{				 
			// enter key has been pressed
			case 13: 	// trigger the onclick event of update anchor
						$j('a#update').trigger('onclick');
						break;
		}
	});
	// load javascript file for creating preset comments
	loadScript.loadScriptsDynamic(new Array('script/trescal/core/utilities/plugins/PresetCommentJQPlugin.js'), true,
	{
		callback: function()
		{
			// loop through each textarea with class of presetComments
			$j('textarea.presetComments').each(function()
			{					
				if ($j(this).attr('class').indexOf('symbolBox') != -1)
				{
					// initialise plugin with symbol box
					$j(this).presetCommentJQPlugin({ symbolBox: true });	
				}
				else
				{
					// initialise plugin with symbol box
					$j(this).presetCommentJQPlugin({ symbolBox: false });
				}																													
			});
		}
	});
}

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );


/**
 * this function populates a hidden input field and submits the form. The value in the parameter passed causes different
 * actions to be performed (i.e. go to next page, go to previous page, update page, exit calibration runner) 
 * 
 * @param {String} action is a string which indicates what should be done
 */
function formAction(action)
{
	switch(action)
	{
		case 'next': 	// 1 is treated as 'next' in controller
						$j('#direction').attr('value', '1');
						break;
						
		case 'prev':	// -1 is treated as 'back' in controller
						$j('#direction').attr('value', '-1');
						break;
						
		case 'update':	// 0 is treated as 'update' in controller
						$j('#direction').attr('value', '0');
						break;
		
		case 'exit':	// -2 is treated as 'complete' in controller
						$j('#direction').attr('value', '-2');
						break;
		
		case 'gencert':	// -3 is treated as 'gen cert' in controller
						$j('#direction').attr('value', '-3');
						break;
						
		case 'batch':	// -4 is treated as 'batch' in controller
						$j('#direction').attr('value', '-4');
						break;	

		case 'repost':	// -5 is a repost of header info
						$j('#direction').attr('value', '-5');
						break;		
						
	}
	// submit the form
	$j('form#stepviewform').submit();
}

function thickboxUnableToCalContent(calId, personid, batchId)
{
	actionoutcomeservice.getActionOutcomesForActivity('Pre-calibration', false,
	{
		callback:function(calOutcomes)
		{
			// create content for thickbox
			var content =	'<div class="thickbox-box">' +
								// add warning box to display error messages if the link fails
								'<div class="hid warningBox1">' +
									'<div class="warningBox2">' +
										'<div class="warningBox3">' +
										'</div>' +
									'</div>' +
								'</div>' +
								'<fieldset>' +
									'<legend>' + i18n.t("calibration:unableToCalibrate", "Unable To Calibrate") + '</legend>' +
									'<ol>' +
										'<li>' +
											'<label>' + i18n.t("reasonLabel", "Reason:") + '</label>' +
											'<select id="selectActionOutcome">';
												$j.each(calOutcomes, function(i)
												{
													content = content + '<option value="' + calOutcomes[i].id + '">' + calOutcomes[i].description + '</option>';
												});
						content = content +	'</select>' +
										'</li>' +
										'<li>' +
											'<label>&nbsp;</label>' +
											'<input type="button" onclick=" completeAsUTC(' + calId + ', ' + personid + ', $j(\'#selectActionOutcome\').val(), ' + batchId + '); return false; " value="' + i18n.t("calibration:utc", "UTC") + '" />' +
										'</li>' +								 
									'</ol>' +
								'</fieldset>' +
							'</div>'; 	
			
			$j('#TB_ajaxContent').append(content);
		}
	});
}

function completeAsUTC(calId, completedById, actionOutcome, batchId)
{	
	calibrationservice.completeCalibrationAsUTC(calId, completedById, actionOutcome, null, 
	{
		callback:function(resWrap)
		{
			if(resWrap.success = true)
			{
				// close thickbox
				tb_remove();
				
				// unwrap results
				var cal = resWrap.results;
				
				// go back to batch
				window.location.href = "viewbatchcalibration.htm?batchid=" + batchId;
			}
			else
			{
				alert(i18n.t("calibration:errorCompleteAsUTC", "There was an error. Please contact Jamie."));
			}
		}
	});
}