/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	CertManagement.js
*	DESCRIPTION		:
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	11/11/2009 - SH - Created File
*
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' },
								{ element: 'ul.subnavtab a', corner: 'small transparent top' } );

/**
 * array of calendar objects containing the initial setup literals needed
 * 
 * @param {String} inputField is the id of the input field to which the calendar should be binded
 * @param {String} dateFormat is the format that the date should be displayed
 * @param {String} displayDates 'all' make all dates available
 * 								'future' show only dates in the future
 * 								'past' show only dates in the past
 * applied in init() function
 */
var jqCalendar = new Array 	({ 
							 	inputField: 'jicertDate',
							 	dateFormat: 'dd.mm.yy',
							 	displayDates: 'all'
							 },
							 { 
							 	inputField: 'groupcertDate',
							 	dateFormat: 'dd.mm.yy',
							 	displayDates: 'all'
							 });
							 
/** 
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = 	new Array( { anchor: 'jicerts-link', block: 'jicerts-tab' });

/**
 * do we want to display cert creation? Is this a site job?
 */
if (allowCertCreation)
{
	menuElements.push(	{ anchor: 'issuejicerts-link', block: 'issuejicerts-tab' },
						{ anchor: 'issuegroupcerts-link', block: 'issuegroupcerts-tab' });
}

/**
 * this method checks all inputs of type checkbox that are contained within the table 
 * 
 * @param {Boolean} select true if boxes are to be checked and vice versa
 * @param {String} parentElement id of table in which to search for inputs of type checkbox
 * @param {String} name the name of the input field we want to select
 */
function selectAllItems(select, parentElement, name)
{
	// check all
	if(select)
	{
		$j('.' + parentElement + ' > tbody > tr input:checkbox[name^="' + name + '"]').check('on');
		// now check if any of the inputs have onclick events and trigger
		$j('.' + parentElement + ' > tbody > tr input:checkbox[name^="' + name + '"][onclick]').each(function()
		{
			//  trigger the onclick event
			$j(this).trigger('onclick');		
		});
	}
	// uncheck all
	else
	{
		$j('.' + parentElement + ' > tbody > tr input:checkbox[name^="' + name + '"]').check('off');
		// now check if any of the inputs have onclick events and trigger
		$j('.' + parentElement + ' > tbody > tr input:checkbox[name^="' + name + '"][onclick]').each(function()
		{
			//  trigger the onclick event
			$j(this).trigger('onclick');		
		});
	}
}

/**
 * this method copies the value selected in the element to any others
 * present on the page in the direction specified.
 *
 * @param {String} direction the direction in which the value should be copied
 * @param {String} elementtype the type of element we are copying to
 * @param {Object} element element which we can obtain value and name for copying
 */
function copySelectedOption(direction, elementtype, element)
{
	// add marker after element
	$j(element).after('<span class="marker"></span>');
	// get the input name attribute
	var elementName = $j(element).attr('name');
	// variable to indicate the marker has been found
	var markerFound = false;
	// array to hold all inputs
	var inputArray = new Array();
	// copy information down the list
	if (direction == 'down')
	{				
		// does element name contain full stop?
		if (elementName.indexOf('.') != -1)
		{
			// get the end of the element name after full stop
			var elementEnd = elementName.substring((elementName.lastIndexOf('.') + 1), elementName.length);
			// create an array of input objects
			inputArray = $j.makeArray($j(elementtype + '[name$="' + elementEnd + '"]'));
		}
		else
		{
			// create an array of input objects
			inputArray = $j.makeArray($j(elementtype + '[name="' + $j(element).attr('name') + '"]'));
		}	
	}
	else
	{
		// does element name contain full stop?
		if (elementName.indexOf('.') != -1) 
		{
			// get the end of the element name after full stop
			var elementEnd = elementName.substring((elementName.lastIndexOf('.') + 1), elementName.length);
			// create an array of input objects
			inputArray = $j.makeArray($j(elementtype + '[name$="' + elementEnd + '"]'));
			// reverse the array
			inputArray.reverse();
		}
		else 
		{
			// create an array of input objects
			inputArray = $j.makeArray($j(elementtype + '[name="' + $j(element).attr('name') + '"]'));
			// reverse the array
			inputArray.reverse();
		}
	}
	
	// loop through all matching inputs
	$j.each(inputArray, function()
	{
		// marker found?
		if ($j(this).next().hasClass('marker'))
		{
			// set variable and remove marker
			markerFound = true;
			$j(this).next().remove();
		}
		// marker found?
		if (markerFound)
		{
			// copy value to input
			$j(this).val($j(element).val());
		}					
	});
}

/**
 * this method places the calibration date field in the correct table depending on the tab selected. This is
 * done this way as the same input field is required for ji cert issues and group cert issues.
 * 
 * @param {String} primaryDateField this is the date field we wish to use
 * @param {String} secondaryDateField this is the date field which was being used before this method was invoked
 */
function placeDateField(primaryDateField, secondaryDateField)
{
	// is the primary date field already in use?
	if ($j('#' + primaryDateField).attr('name') != "")
	{
		// do nothing as input is already in correct position
	}
	else
	{
		// move name to primary date field and add value from secondary date field
		$j('#' + primaryDateField).attr('name', 'calDate').val($j('#' + secondaryDateField).val());
		// remove name from secondary date field
		$j('#' + secondaryDateField).attr('name', '');
	}
}

function placeIssuerField(primaryIssuerField, secondaryIssuerField)
{
	// is the primary issuer field already in use?
	if ($j('#' + primaryIssuerField).attr('name') != "")
	{
		// do nothing as input is already in correct position
	}
	else
	{
		// move name to primary date field and add value from secondary date field
		$j('#' + primaryIssuerField).attr('name', 'issuerId').val($j('#' + secondaryIssuerField).val());
		// remove name from secondary date field
		$j('#' + secondaryIssuerField).attr('name', '');
	}
}

/**
 * this method enables the input checkboxes next to group items when a group is selected
 * 
 * @param {Boolean} checked indicates if the checkbox selected is currently checked
 * @param {String} grouptable string class name of the group table to select items
 */
function enableGroupItems(checked, grouptable)
{
	// has the group checkbox been selected?
	if (checked)
	{
		// enable all checkboxes for group items and pre-select them
		$j('table.' + grouptable + ' input:checkbox').attr('disabled', '').check('on');
	}
	else
	{
		// disable all checkboxes for group items and de-select them
		$j('table.' + grouptable + ' input:checkbox').attr('disabled', 'disabled').check('off');
	}
}

function printCalSpecificLabel(certid, recallOption)
{
	if(recallOption == 'PREDICT')
	{
		birtlabelservice.printCalSpecificLabelWithPredictedRecall(certid, 
		{
			callback: function(result)
			{
				return result.success;
			}
		});
	}
	else
	{
		var incRecallDate = (recallOption == 'INCLUDE') ? true : false;
		
		birtlabelservice.printCalSpecificLabel(certid, incRecallDate, 
		{
			callback: function(result)
			{
				return result.success;
			}
		});
	}
}

function printCertificateLabel(template, certlinkid, recallOption)
{
	var incRecallDate = (recallOption == 'PREDICT' || recallOption == 'INCLUDE') ? true : false;
	
	birtlabelservice.printCertificateLabel(certlinkid, template, incRecallDate, 
	{
		callback: function(result)
		{
			return result.success;
		}
	});
}

function printAllSelectedLabels()
{	
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/birtlabelservice.js'), true,
	{
		callback: function()
		{
			$j('input:checkbox[name^="labelCheckbox"]:checked').each(
				function(i, n)
				{
					eval($j(n).val());
				}
			);
		}
	});
}