/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	CertSearch.js
*	DESCRIPTION		:
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	06/05/2008 - JV - Created File
*
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'certNo';


/**
 * Array of autocomplete objects (multiple are possible) containing setup for 
 * jQueryUI initialization.
 * this is used in the init() function.
 */
var jquiAutocomplete = new Array(
	{
		inputFieldName: 'desc',
		inputFieldId: 'descid',
		source: 'searchdescriptiontags.json'
	},
	{
		inputFieldName: 'mfr',
		inputFieldId: 'mfrid',
		source: 'searchmanufacturertags.json'
	});

/**
 * array of calendar objects containing the initial setup literals needed
 * @param inputField (String) id of the input field to which the calendar should be binded
 * @param dateFormat (String) format that the date should be displayed
 * @param displayDates (String) 'all' make all dates available
 * 								'future' show only dates in the future
 * 								'past' show only dates in the past
 * applied in init() function.
 */	
//var jqCalendar = new Array (
//				 { 
//				 	inputField: 'issueDate1',
//				 	dateFormat: 'dd.mm.yy',
//				 	displayDates: 'all',
//					showWeekends: true
//				 },
//				 { 
//				 	inputField: 'issueDate2',
//				 	dateFormat: 'dd.mm.yy',
//				 	displayDates: 'all',
//					showWeekends: true
//				 },
//				 { 
//				 	inputField: 'calDate1',
//				 	dateFormat: 'dd.mm.yy',
//				 	displayDates: 'all',
//					showWeekends: true
//				 },
//				 { 
//				 	inputField: 'calDate2',
//				 	dateFormat: 'dd.mm.yy',
//				 	displayDates: 'all',
//					showWeekends: true
//				 });

/**
 * this method shows or hides the between date field
 * 
 * @param {boolean} between indicates whether to show the between date field
 * @param {String} id the id of the between element to display
 */
function changeDateOption(between, id)
{
	// show between?
	if(between == 'true')
	{
		// display between
		$j('#' + id).removeClass('hid').addClass('vis');
	}
	else
	{
		// hide between
		$j('#' + id).removeClass('vis').addClass('hid');
	}
}

