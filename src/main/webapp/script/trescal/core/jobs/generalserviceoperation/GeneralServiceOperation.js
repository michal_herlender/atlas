var pageImports = new Array(
		'script/trescal/core/jobs/jobitem/JobItemFunctions.js',
		'script/thirdparty/jQuery/jquery.boxy.js',
		'script/trescal/core/tools/labelprinting/dymo/DymoLabelFunctions.js',
		'script/thirdparty/jQuery/jquery.scrollTo.js');

function init() {
	// load stylesheet using the dom if not already
	loadScript.loadStyle('styles/jQuery/jquery-boxy.css', 'screen');
	// select the correct subnavigation link (general service operation)
	$j('#linkgeneralserviceoperation').addClass('selected');
	// user preference to use job item key navigation
	if (userPrefs.jikeynav) {
		// initialise key nav to move between job items
		initialiseKeyNavForJI();
	}
	
}

function checkAllItems(checked) {
	var inputs = document.getElementsByTagName("input");
	for (var i = 0, n = inputs.length; i < n; i++) {
		if (inputs[i].name.indexOf('otherJobItemsId') == 0) {
			inputs[i].checked = checked;
		}
	}
}

function changeOutcome(ele) {

	if ($j(ele).find(':selected').attr('class') == 'ON_HOLD') {
		$j('select').find('option[class=ON_HOLD]').attr('selected', 'selected');
	} else {
		var isOnHold = false;
		var items = $j('select');
		//if one on-hold item was selected
		for (var i = 0; i < items.length; i++) {
			if ($j(items[i]).find(':selected').attr('class') == 'ON_HOLD') {
				isOnHold = true;
				break;
			}
		}
		//if one on-hold item was selected set all other item with new selected value
		if (isOnHold) {
			var currentVal = $j(ele).find(':selected').attr('class');
			$j('select').find('option[class=' + currentVal + ']').attr(
					'selected', 'selected');
		} 
	}
	
	//if current selected value is SUCCESSFUL show the client decision required input
	if ($j(ele).find(':selected').attr('class') == 'COMPLETED_SUCCESSFULLY')
		$j('#clientDecisionRequired').removeClass('hid').addClass('show');
	else
		{
			var isSuccessful = false;
			var items = $j('select');
			//if one on-hold item was selected
			for (var i = 0; i < items.length; i++) {
				if ($j(items[i]).find(':selected').attr('class') == 'COMPLETED_SUCCESSFULLY') {
					isSuccessful = true;
					break;
				}
			}
			if(isSuccessful == false)
				$j('#clientDecisionRequired').removeClass('show').addClass('hid');
		}
}