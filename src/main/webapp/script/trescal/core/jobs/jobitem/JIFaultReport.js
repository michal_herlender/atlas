/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	JIFaultReport.js
*	DESCRIPTION		:	Page-specific javascript file.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 
*						
*	KNOWN ISSUES	:	-
*	HISTORY			:	29/10/2007 - JV - Created File
*					:	20/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array(	'script/trescal/core/jobs/jobitem/JobItemFunctions.js',
								'script/trescal/core/tools/labelprinting/dymo/DymoLabelFunctions.js',
								'script/thirdparty/jQuery/jquery.boxy.js',
								'script/thirdparty/jQuery/jquery.scrollTo.js' );


var jqCalendar = new Array(	{ 
	inputField: 'clientApprovalOn',
	dateFormat: 'dd.mm.yy',
	displayDates: 'all',
	showWeekends: true
});

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{



    // load stylesheet using the dom if not already
	loadScript.loadStyle('styles/jQuery/jquery-boxy.css', 'screen');
	// select the correct subnavigation link
	$j('#linkfaultreport').addClass('selected');
	// user preference to use job item key navigation
	if (userPrefs.jikeynav)
	{
		// initialise key nav to move between job items
		initialiseKeyNavForJI();
	}

    $j("#generatefailurereport").click(function () {
        var faultreportid = $j("#faultreportid").val();
        window.location.href = "birtfailurereport.htm?faultreportid=" + faultreportid;
    });

}

/**
 * this method creates the content for a new fault report comment jQuery boxy pop up
 * 
 * @param {String} type the type of fault report detail to create (i.e. Description, Action)
 * @param {Integer} faultRepId the id of the fault report to add detail
 * @param {Integer} personId the id of the person adding detail 
 */
function addOrEditFaultCommentContent(type, addOrEdit, faultRepId, faultCommentId, personId)
{	
	if(addOrEdit == 'edit')
	{
		// load the service javascript file if not already
		loadScript.loadScriptsDynamic(new Array('dwr/interface/faultreportdetailservice.js'), true,
		{
			callback: function()
			{
				faultreportdetailservice.ajaxFindFaultReportDetail(type, faultCommentId,
				{
					callback:function(results)
					{					
						// update of boolean field successful?
						if(results.success)
						{
							createAddOrEditContent(type, addOrEdit, faultRepId, faultCommentId, personId, results.results.description);
						}
					}
				});
			}
		});
	}
	else
	{
		createAddOrEditContent(type, addOrEdit, faultRepId, faultCommentId, personId, '');
	}
}

function createAddOrEditContent(type, addOrEdit, faultRepId, faultCommentId, personId, value)
{
	var topVerb = (addOrEdit == 'add') ? i18n.t("core.jobs:jobItem.createFault", "Create Fault") : i18n.t("core.jobs:jobItem.editFault", "Edit Fault");
	
	// variables to hold comment type text
	var typeA = '';
	var typeText = '';
	var typeIdText = '';
	// is this a fault description or action?
	if (type == 'Description')
	{
		typeA = i18n.t("description", "Description");
		typeText = i18n.t("core.jobs:jobItem.faultDescription", "DESCRIPTION OF ANY FAULT:");
		typeIdText = 'FAULT_DESCRIPTION';
	}
	else if(type == 'Action')
	{
		typeA = i18n.t("action", "Action");
		typeText = i18n.t("core.jobs:jobItem.faultAction", "REPAIR/RECOMMENDED ACTIONS:");
		typeIdText = 'FAULT_ACTION';
	}
	else
	{
		typeA = i18n.t("instruction", "Instruction");
		typeText = i18n.t("core.jobs:jobItem.faultInstruction", "CALIBRATION / REPAIR INSTRUCTIONS FOR THIRD PARTY DELIVERY NOTES:");
		typeIdText = 'FAULT_INSTRUCTION';
	}
	
	// create content to append in overlay
	var content = 	'<div id="faultCommentOverlay">' +
						// add warning box to display error messages if the insertion or update fails
						'<div class="hid warningBox1">' +
							'<div class="warningBox2">' +
								'<div class="warningBox3">' +
								'</div>' +
							'</div>' +
						'</div>' +
						'<fieldset>' +
							'<ol>' +										
								'<li>' +
									'<label class="twolabel marg-right">' + typeText + '</label>' +
									'<textarea id="commentText" class="width70 symbolBox presetComments ' + typeIdText + '" rows="4" value="' + value + '">' + value + '</textarea>' +
								'</li>' +
								'<li>' +
									'<label>&nbsp;</label>';
									if(addOrEdit == 'add')
									{
										content = content + '<input type="button" name="save" onclick=" $j(this).attr(\'disabled\', \'disabled\'); saveFaultComment(\'' + type + '\', ' + faultRepId + ', ' + personId + ', $j(\'textarea#commentText\').val()); return false; " value="' + i18n.t("add", "Add") + ' ' + typeA + '" />';
									}
									else
									{
										content = content + '<input type="button" name="update" onclick=" $j(this).attr(\'disabled\', \'disabled\'); editFaultComment(\'' + type + '\', ' + faultCommentId + ', ' + personId + ', $j(\'textarea#commentText\').val()); return false; " value="' + i18n.t("update", "Update") + ' ' + typeA + '" />';
									}
								content = content + '</li>' +
							'</ol>' +
						'</fieldset>' +
					'</div>';
	// create new overlay using content variable
	loadScript.createOverlay('dynamic', topVerb + ' ' + typeA, null, content, null, 40, 860, 'commentText');
	// initialise preset comments after small delay for pop up to appear
	setTimeout('loadScript.init(true);', 500);
}

/**
 * this method adds either a new fault report description or a fault report action to the
 * selected fault report.
 * 
 * @param {String} type the type of fault report detail to save (i.e. Description, Action)
 * @param {Integer} faultRepId the id of the fault report to add detail
 * @param {Integer} personId the id of the person adding detail
 * @param {String} value the value of detail to add
 */
function saveFaultComment(type, faultRepId, personId, value)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/faultreportdetailservice.js'), true,
	{
		callback: function()
		{
			// add fault report description?
			if (type == 'Description')
			{
				// call dwr service to insert new fault report description
				faultreportdetailservice.insertFaultReportDescription(faultRepId, personId, value,
				{
					callback: function(result)
					{
						// insert description not successful
						if(!result.success)
						{					
							// show error message in error warning box
							$j('div#faultCommentOverlay div:first').removeClass('hid').addClass('vis').find('.warningBox3').empty().append(loadScript.unwrapErrors(result.errorList));
						}
						else
						{	
							// message row present?			
							if ($j('table#frDescList tbody tr:first td').length < 2)
							{
								// remove message
								$j('table#frDescList tbody tr:first').remove();
							}
							// append the new description to the descriptions table
							$j('table#frDescList tbody').append('<tr>' +
																	'<td id="Description-' + result.results.id + '">' + result.results.description + '</td>' +
																	'<td>[' + result.results.recordedBy.name + ' ' + cwms.dateFormatFunction(result.results.lastModified) + ']</td>' +
																	'<td>' +
																		'<a href="#" onclick=" addOrEditFaultCommentContent(\'Description\', \'edit\', 0, ' + result.results.id + ', ' + personId + '); return false; " >' +
																			'<img width="16" height="16" title="' + i18n.t("editDescription", "Edit Description") + '" class="noteimg_padded" alt="' + i18n.t("editDescription", "Edit Description") + '" src="img/icons/note_edit.png" />' +
																		'</a>' +
																		'&nbsp;' +
																		'<a href="#" onclick=" removeFaultComment(this, \'Description\', ' + result.results.id + ', ' + personId + '); return false; " >' +
																			'<img width="16" height="16" title="' + i18n.t("deleteDescription", "Delete Description") + '" class="noteimg_padded" alt="' + i18n.t("deleteDescription", "Delete Description") + '" src="img/icons/note_delete.png" />' +
																		'</a>' +
																	'</td>' +
																'</tr>');
							// close the overlay
							loadScript.closeOverlay();
						} 
					}
				});
			}
			else if(type == 'Action')
			{
				// call dwr service to insert new fault report action
				faultreportdetailservice.insertFaultReportAction(faultRepId, personId, value,
				{
					callback: function(result)
					{
						// insert action not successful
						if(!result.success)
						{
							// show error message in error warning box
							$j('div#faultCommentOverlay div:first').removeClass('hid').addClass('vis').find('.warningBox3').empty().append(loadScript.unwrapErrors(result.errorList));
						}
						else
						{	
							// message row present?
							if ($j('table#frActList tbody tr:first td').length < 2)
							{
								// remove message
								$j('table#frActList tbody tr:first').remove();
							}			
							// append the new action to the actions table
							$j('table#frActList tbody').append(	'<tr>' +
																	'<td class="faultdetail" id="Action-' + result.results.id + '">' + result.results.description + '</td>' +
																	'<td class="modifiedby">[' + result.results.recordedBy.name + ' ' + cwms.dateFormatFunction(result.results.lastModified) + ']</td>' +
																	'<td>' +
																		'<a href="#" onclick=" addOrEditFaultCommentContent(\'Action\', \'edit\', 0, ' + result.results.id + ', ' + personId + '); return false; " >' +
																			'<img width="16" height="16" title="' + i18n.t("core.jobs:jobItem.editAction", "Edit Action") + '" class="noteimg_padded" alt="' + i18n.t("editAction", "Edit Action") + '" src="img/icons/note_edit.png" />' +
																		'</a>' +
																		'&nbsp;' +
																		'<a href="#" onclick=" removeFaultComment(this, \'Action\', ' + result.results.id + ', ' + personId + '); return false; " >' +
																			'<img width="16" height="16" title="' + i18n.t("core.jobs:jobItem.deleteAction", "Delete Action") + '" class="noteimg_padded" alt="' + i18n.t("deleteAction", "Delete Action") + '" src="img/icons/note_delete.png" />' +
																		'</a>' +
																	'</td>' +
																'</tr>');
							// close the overlay
							loadScript.closeOverlay();
						}
					}
				});
			}
			else
			{
				// call dwr service to insert new fault report action
				faultreportdetailservice.insertFaultReportInstruction(faultRepId, personId, value,
				{
					callback: function(result)
					{
						// insert instruction not successful
						if(!result.success)
						{
							// show error message in error warning box
							$j('div#faultCommentOverlay div:first').removeClass('hid').addClass('vis').find('.warningBox3').empty().append(loadScript.unwrapErrors(result.errorList));
						}
						else
						{	
							// message row present?
							if ($j('table#frInstList tbody tr:first td').length < 2)
							{
								// remove message
								$j('table#frInstList tbody tr:first').remove();
							}
							// create content for new instruction
							var content = 	'<tr>' +
												'<td class="faultdetail" id="Instruction-' + result.results.id + '">' + result.results.description + '</td>' +
												'<td class="showontpdel" id="showontpdn' + result.results.id + '">' +																	
													'<a href="#" onclick=" updateShowOnTPDelNote(' + result.results.id + ', true); return false; ">' +
														'<img src="img/icons/bullet-tick.png" width="10" height="10" alt="' + i18n.t("core.jobs:jobItem.showOnTPDelNote", "Show On TP Del Note") + '" title="' + i18n.t("core.jobs:jobItem.showOnTPDelNote", "Show On TP Del Note") + '" />' +
													'</a>' +
												'</td>' +
												'<td class="modifiedby">[' + result.results.recordedBy.name + ' ' + cwms.dateFormatFunction(result.results.lastModified) + ']</td>' +
												'<td>' +
													'<a href="#" onclick=" addOrEditFaultCommentContent(\'Instruction\', \'edit\', 0, ' + result.results.id + ', ' + personId + '); return false; " >' +
														'<img width="16" height="16" title="' + i18n.t("core.jobs:jobItem.editInstruction", "Edit Instruction") + '" alt="' + i18n.t("core.jobs:jobItem.editInstruction", "Edit Instruction") + '" src="img/icons/note_edit.png" class="noteimg_padded" />' +
													'</a>' +
													'&nbsp;' +
													'<a href="#" onclick=" removeFaultComment(this, \'Instruction\', ' + result.results.id + ', ' + personId + '); return false; " >' +
														'<img width="16" height="16" title="' + i18n.t("core.jobs:jobItem.deleteInstruction", "Delete Instruction") + '" alt="' + i18n.t("core.jobs:jobItem.deleteInstruction", "Delete Instruction") + '" src="img/icons/note_delete.png" class="noteimg_padded" />' +
													'</a>' +
												'</td>' +
											'</tr>';
							// append the new action to the actions table
							$j('table#frInstList tbody').append(content);
							// close the overlay
							loadScript.closeOverlay();	
						}
					}
				});
			}
		}
	});
}

/**
 * this function updates the show instruction on third party delivery note boolean field
 * 
 * @param {Integer} faultRepInstId is the id of the fault report instruction to update
 * @param {Boolean} showOnTPDN is the boolean value currently assigned
 */
function updateShowOnTPDelNote(faultRepInstId, showOnTPDN)
{
	// swap boolean value of showOnTPDN
	if (showOnTPDN)
	{
		showOnTPDN = false;
	}
	else
	{
		showOnTPDN = true;
	}
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/faultreportdetailservice.js'), true,
	{
		callback: function()
		{
			// dwr method to update the show instruction on third party delivery note document field
			faultreportdetailservice.updateFaultReportInstructionShowOnTPDN(faultRepInstId, showOnTPDN,
			{
				callback:function(results)
				{
					// update of boolean field successful?
					if(!results.success)
					{
						// display failure message
						$j.prompt(i18n.t("failureMessage", {errMsg : results.message, defaultValue : "Failed: " + results.message}));
					}
					else
					{
						// remove the old anchor and image from the table cell and append new one
						$j('#showontpdn' + faultRepInstId).empty().append('<a href="#" onclick=" updateShowOnTPDelNote(' + faultRepInstId + ', ' + showOnTPDN + '); return false; "></a>');
						// is the instruction to be shown on third party delivery notes?						
						if (showOnTPDN)														
						{
							// add tick image
							$j('#showontpdn' + faultRepInstId + ' a').append('<img src="img/icons/bullet-tick.png" width="10" height="10" alt="' + i18n.t("core.jobs:jobItem.showOnTPDelNote", "Show On TP Del Note") + '" title="' + i18n.t("core.jobs:jobItem.showOnTPDelNote", "Show On TP Del Note") + '" />');
						}
						else
						{
							// add cross image
							$j('#showontpdn' + faultRepInstId + ' a').append('<img src="img/icons/bullet-cross.png" width="10" height="10" alt="' + i18n.t("core.jobs:jobItem.hideOnTPDelNote", "Hide On TP Del Note") + '" title="' + i18n.t("core.jobs:jobItem.hideOnTPDelNote", "Hide On TP Del Note") + '" />');
						}
					}
				}
			});
		}
	});
}

function editFaultComment(type, faultRepDetailId, personId, value)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/faultreportdetailservice.js'), true,
	{
		callback: function()
		{
			faultreportdetailservice.editFaultReportDetail(type, faultRepDetailId, personId, value, 
			{
				callback:function(results)
				{
					// update of boolean field successful?
					if(!results.success)
					{
						if(results.message != null)
						{
							$j('div#faultCommentOverlay div:first').removeClass('hid').addClass('vis').find('.warningBox3').empty().append('<div>' + results.message + '</div>');
						}
						else
						{
							// show error message in error warning box
							$j('div#faultCommentOverlay div:first').removeClass('hid').addClass('vis').find('.warningBox3').empty().append(loadScript.unwrapErrors(results.errorList));
						}
					}
					else
					{
						// update
						$j('td#' + type + '-' + faultRepDetailId).html(results.results.description);
						// close the overlay
						loadScript.closeOverlay();
					}
				}
			});
		}
	});
}

function removeFaultComment(anchor, type, faultRepDetailId, personId)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/faultreportdetailservice.js'), true,
	{
		callback: function()
		{
			faultreportdetailservice.deactivateFaultReportDetail(type, faultRepDetailId, personId,
			{
				callback:function(results)
				{
					// update of boolean field successful?
					if(!results.success)
					{
						// display failure message
						$j.prompt('Error: ' + results.message);
					}
					else
					{
						// get first cell of table row
						var cell = $j(anchor).parent('td').parent('tr').find('td:first');
						// create new html content
						$j(cell).html('<del>' + cell.html() + '</del>');
						// instruction?
						if (type == 'Instruction')
						{
							// remove anchors for editing and deleting
							$j(anchor).parent('td').empty();
						}
					}
				}
			});
		}
	});
}

function toggleQualityCheck(showEditBoxes)
{
	if(showEditBoxes)
	{
		$j('div#editBerComment').addClass('vis').removeClass('hid');
		$j('div#persistedBerComment').addClass('hid').removeClass('vis');
	}
	else
	{
		$j('div#editBerComment').addClass('hid').removeClass('vis');
		$j('div#persistedBerComment').addClass('vis').removeClass('hid');
	}
}

var previousComment = '';

function toggleQualityCheckFromEdit(anchor)
{
	// user has just clicked to cancel editing
	if($j('input[name="berCheckPerformed"]').val() == 'true')
	{
		$j(anchor).html('(edit)');
		$j('input[name="berCheckPerformed"]').val('false');
		toggleQualityCheck(false);
		$j('textarea#editCommentText').val(previousComment);
	}
	// user has just clicked to start editing
	else
	{
		$j(anchor).html('(cancel)');
		$j('input[name="berCheckPerformed"]').val('true');
		toggleQualityCheck(true);
		previousComment = $j('textarea#editCommentText').val();
	}
}
