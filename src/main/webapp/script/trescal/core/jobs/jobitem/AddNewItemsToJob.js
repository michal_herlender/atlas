/****************************************************************************************************************************************
 *												CWMS (Calibration Workflow Management System)
 *			
 *												Copyright 2006, Antech Calibration Services
 *															www.antech.org.uk
 *		
 *															All rights reserved
 *
 *														Document author: 	Jamie Vinall
 *
 *	FILENAME		:	AddNewItemsToJob.js
 *	DESCRIPTION		:	This page specific javascript file.
 *	DEPENDENCIES	:   -
 *
 *	TO-DO			: 	-
 *	KNOWN ISSUES	:	-
 *	HISTORY			:	15/04/2008 - JV - Created File
 *					:	14/10/2015 - TProvost - Manage i18n
 *					:	14/10/2015 - TProvost - Fix bug in new version of Impromptu.js - Replace the "callback" property (property disappeared) by the "submit" property
 *					:							=> Changes made in function displayConfirmation
 *					:	21/10/2015 - TProvost - Fix bug on showUnLoadWarning when page is submit
 ****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
const pageImports = ['dwr/interface/instrumservice.js',
	'script/trescal/core/tools/labelprinting/dymo/DymoLabelFunctions.js',
	"script/trescal/core/utilities/Debounce.js",
	'script/thirdparty/jQuery/jquery.boxy.js',
	'script/thirdparty/jQuery/jquery.jtip.js'];

/**
 * shows a warning if the user is leaving a page which has items in a basket.
 * Only works in IE and Moz 1.7 +.
 */
let showUnLoadWarning = true;

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if
 * present. this allows onload coding to be included in the page specific
 * javascript file without adding to the body onload attribute.
 */
function init() {
	// load stylesheet using the dom if not already
	loadScript.loadStyle('styles/jQuery/jquery-boxy.css', 'screen', undefined);
	loadScript.loadStyle('styles/jQuery/jquery-jtip.css', 'screen', undefined);
	// check to see if serial number text field present
	if ($j('input[type="text"][name^="instSerialNos"]').length) {
		// apply focus to first input field
		$j('input[type="text"][name^="instSerialNos"]:first').focus();
	}
	// prevent internet explorer default action when escape key is clicked
	// single click will clear form element, double click will clear entire form

	/**
	 * shows a warning if the user is leaving a page which has itemsl.
	 */
	$j(window)
			.bind(
					'beforeunload',
					function() {
						if (showUnLoadWarning) {
							showUnLoadWarning = true;
							return i18n
									.t("core.jobs:jobItem.confirmLeavePage",
											"Are you sure you wish to leave this page before submitting the items?");
						}
	});

	document.getElementById("addnewitemstojobform").addEventListener("submit", (e) => {
		e.preventDefault();
		disableSubmit();
		showSpinner();
		window.showUnLoadWarning = false;
		const {confirmOnSubmit: displayConf, allowConRev, disallowSamePlantNumber} = e.currentTarget.dataset;
		if (!e.currentTarget.checkValidity()) return false;
		validateAndSubmit(displayConf === "true" || false, allowConRev || false, disallowSamePlantNumber || true);
		return false;
	});
}


function showSpinner() {
	const spinner = document.querySelector("cwms-spinner");
	spinner.classList.remove("hid");
}

function hideSpinner() {
	const spinner = document.querySelector("cwms-spinner");
	spinner.classList.add("hid");
}

function disableSubmit() {
	const button = document.querySelector("[type='submit']");
	button.disabled = true;
}

function enableSubmit() {
	const button = document.querySelector("[type='submit']");
	button.disabled = false;
}


/**
 * this method displays a confirmation message asking the user if they are sure
 * they wish to add their items to the job. Confirmation will either submit the
 * form or call method confirmContractReview() if contract reviewing is enabled.
 *
 * @param allowContractReview {Boolean}
 *            allowContractReview indicates if contract reviewing is enabled at
 *            goods in
 */
function displayConfirmation(allowContractReview) {
	// initial prompt for user
	hideSpinner();
	$j.prompt(i18n.t("core.jobs:jobItem.confirmAddItemsToJob",
		"Are you sure that you wish to add these items to the job?"), {
		submit: confirmcallback,
		close: confirmClosecallback,
		buttons: {
			Ok: true,
			Cancel: false
		},
		focus: 1
	});
	// callback method for initial prompt
	function confirmcallback(v, m) {
		// user confirmed action?
		if (m) {
			// is contract reviewing enabled?
			if (allowContractReview === true) {
				// call contract review method
				confirmContractReview();
			} else {
				// set unload warning variable
				showUnLoadWarning = false;
				showSpinner();
				// submit the form
				submitForm();
			}
		} else {
			// re-enable submit button on page
			enableSubmit();
			hideSpinner()
		}
	}
	function confirmClosecallback(){
		enableSubmit();
	}
}

/**
 * this method is called if contract reviewing at goods in is enabled. It asks
 * the user if the items have been successfully contract reviewed at this stage.
 */
function confirmContractReview() {
	// contract review prompt
	$j.prompt(i18n.t("core.jobs:jobItem.confirmContractReview",
			"Have these items been successfully contract reviewed?"), {
		submit : confirmCRcallback,
		buttons : {
			Yes : true,
			No : false
		},
		focus : 1
	});
	// contract review callback
	function confirmCRcallback(v, m) {
		// user confirmed action?
		if (m) {
			// set unload warning variable
			showUnLoadWarning = false;
			// set contract review hidden input to true and submit form
			$j('input[name="contractReviewItems"]').val(true);
			$j('form#addnewitemstojobform').submit();
		} else {
			// set unload warning variable
			showUnLoadWarning = false;
			// set contract review hidden input to false and submit form
			$j('input[name="contractReviewItems"]').val(false);
			$j('form#addnewitemstojobform').submit();
		}
	}
}


/**
 * this method checks all the instruments added via the form to see if there are
 * any possible duplicates already on the database and displays them in a jquery
 * boxy thickbox.
 *
 * @param displayConf {Boolean}
 *            coid the coid of the company we are trying to add items to
 * @param allowConRev {Boolean}
 *            displayConf indicates if the confirmation message should be
 *            displayed
 * @param disallowSamePlantNumber {Boolean}
 *            allowConRev indicates if we should call the contract review method
 *            once confirmed
 */


 function validateAndSubmit(displayConf,allowConRev,disallowSamePlantNumber){
	checkAgainstExactMatch(disallowSamePlantNumber)
		.then(() => proceedAddItems(displayConf, allowConRev),
			fields => {
				fields.forEach(fieldset => {
                    fieldset.scrollIntoView();
                    fieldset.focus();
                })
                hideSpinner();
			}
		)
 }

 function checkAgainstExactMatch(disallowSamePlantNumber){
    if(!disallowSamePlantNumber) return Promise.resolve({isValid:true});
    const warns = Array.from(document.querySelectorAll(".cwms-duplicated-with-same-plant-error, .cwms-duplicated-with-db-error" ));
    return warns.length > 0?Promise.reject(warns): Promise.resolve({isValid:true});
 }

function onInsertionCancel() {
	Boxy.get($j("#instrumentDuplicateBoxy")).hide();
	enableSubmit();
	hideSpinner();
}

function proceedAddItems(displayConf, allowConRev){
	// set unload warning variable
	showUnLoadWarning = false;
	// just hide boxy and continue
	confirmDuplicates(displayConf, allowConRev);
}

/**
 * this method  calls confirmContractReview() method
 *
 * @param displayConf {Boolean}
 *            displayConf indicates if the confirmation message should be
 *            displayed
 * @param allowConRev {Boolean}
 *            allowConRev indicates if we should call the contract review method
 *            once confirmed
 */
function confirmDuplicates(displayConf, allowConRev) {
	// display confirmation?
	if (displayConf) {
		displayConfirmation(allowConRev);
	}
	// contract review?
	else if (allowConRev) {
		confirmContractReview();
	} else {
		// set unload warning variable
		showUnLoadWarning = false;

		// /show loading box
		// loading image and message content
		const boxy = `<div id="printingLabelsBoxy" class="center-pad">
		<div class="boxyGridContainer">
			 <img src="img/icons/ajax-loader.gif" width="31" height="31" alt="" />
			 <h4>${i18n.t("core.jobs:jobItem.printingLabels",
			"Printing labels")}</h4>
		</div>
	<div class="btnCancel text-center marg-top marg-bot">
		<input type="button"
		value="${i18n.t("core.jobs:jobItem.cancelInsertion", "Cancel Insertion")}"
		onclick="onInsertionCancel()"
		/>
	</div>
	</div>`
		// if the auto print checkbox is checked flag
		const autoPrintLabels = document.getElementById('autoPrintLabelCheckBoxId')?.checked ?? false;
		if (autoPrintLabels) {
			// create new boxy pop up using content variable
			const printingLabelsBoxy = new Boxy(boxy, {
				title: i18n.t("core.jobs:jobItem.printingLabels",
					"Printing labels"),
				modal: true,
				unloadOnHide: true
			});
			printingLabelsBoxy.tween(320, 120);
			printingLabelsBoxy.show();
		}

        submitForm();
	}
}

function submitForm(){
		const autoPrintLabels = document.getElementById('autoPrintLabelCheckBoxId')?.checked ?? false;
		const redirectAction = () => {window.location = cwms.urlWithParamsGenerator("viewjob.htm",{jobid:jobId})};
		fetch(cwms.urlWithParamsGenerator("addnewitemstojob.json",{jobid:jobId,bookingInAddrId}),
			{
				method:"POST",
				headers:cwms.metaDataObtainer.csrfHeader,
				credentials:"same-origin",
				body:new FormData(document.getElementById("addnewitemstojobform")),
			}
		)
		.then(res => res.ok?res.json():Promise.reject(res.statusText))
		.then(res => { if(res.errors)
						processErrors(res.errors)
					else if (autoPrintLabels)
						printDymoSelectedJobItemLabel(res.jobItemIds,redirectAction)
					else 
						redirectAction();
					}		
			)
				.catch(console.error);
}

function processErrors(errors) {
	hideSpinner();
	enableSubmit();

	const errorsHandlerElement = document.querySelector("cwms-newjobitem-errors");
	if (errorsHandlerElement)
		errorsHandlerElement.errors = errors.map(e => {
			const match = e.field.match(/(.+)\[(\d+)]/);
			if (!match) return undefined;
			return {fieldname: match[1], idx: Number(match[2]), messageCode: e.code, defaultMessage: e.defaultMessage}
		});
}


