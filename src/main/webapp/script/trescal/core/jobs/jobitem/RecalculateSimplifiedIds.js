import { LitElement, css, html } from "../../../../thirdparty/lit-element/lit-element.js";
import  "../../../../components/cwms-translate/cwms-translate.js";
import { urlWithParamsGenerator } from "../../../../tools/UrlGenerator.js";

class RecalculateSimplifiedIds extends LitElement {
  static get properties() {
	  return { companies: { type: Array, attribute: false }, selected: { type: Object, attribute: false }, events: { type: Array, attribute: false } };
  }

  static get styles() {
    return css`
      .inputs {
        width: 30rem;
      }
      .events {
        display: flex;
        flex-direction: column;
        border: 2px solid #5cb900;
        background-color: #f8fff0;
        width: 30rem;
        color: #f50;
      }
      .selected {
        color: blue;
        font-size: initial;
      }
    `;
  }

  submit(event) {
    event.preventDefault();
    this.events = [];
    const url =  urlWithParamsGenerator("instrument/buildIndex.do",{coid:this.selected?.coid, nullsOnly:this.shadowRoot.getElementById("calculateNullsOnly").checked});
    const evtSource = new EventSource(url);
    evtSource.onmessage = (e) => this.appendEvent(e);
    evtSource.onerror = (e) => evtSource.close();
    evtSource.addEventListener("company_done", (e) => {
      this.appendEvent(e);
      evtSource.close();
    });
  }

  appendEvent(event) {
    console.log(event);
    this.events ? (this.events = [...this.events, event]) : (this.events = [event]);
  }
  onKeyUp(event) {
    let companyName = event.target.value;
    console.log(companyName);
    fetch(urlWithParamsGenerator("companysearchrequest.json",{searchName:companyName}))
      .then((r) => r.json())
      .then((r) => (this.companies = r.slice(0, 10)));
  }

  selectCompany(event) {
    this.selected = event.detail;
  }

  renderEvent(event) {
    return html`<span>${event.data}</span>`;
  }

  renderEvents() {
    return this.events && this.events.length > 0 ? html` <div class="events">${this.events.map(this.renderEvent.bind(this))}</div> ` : html``;
  }

  render() {
    return html`
      <form class="ids-calculator">
        <fieldset>
          <legend>Recalculate simplified instrument IDs for company</legend>
          <div class="inputs">
           <div class="row">
              <label>Options</label>
              <input type="checkbox" id="calculateNullsOnly" value=${true} checked="checked" />Recalculate Only Nulls
            </div>
           <div class="row">
              <label>Name of the company</label>
              <input type="text" name="companyName"  @keyup=${this.onKeyUp}/>
              <cwms-company-name-results .companies=${this.companies} @company-selected=${this.selectCompany} ></cwms-company-name-results>
            </div>
            <div class="row">
            ${this.selected ? html`<span class="selected">selected: ${this.selected.coname}</span> ` : html``}
          <button @click="${this.submit}">
            <cwms-translate code="submit">Recalculate ids</cwms-translate>
          </button>
            </div>
          </div>
        </fieldset>
      </form>
      ${this.renderEvents()}
    `;
  }
}

customElements.define("cwms-recalculate-ids", RecalculateSimplifiedIds);

class CompanyNameResult extends LitElement {
  static get properties() {
    return { companies: { type: Array, attribute: false } };
  }

  static get styles() {
    return css`
      .companies {
        display: flex;
        flex-direction: column;
        width: 30rem;
      }
      .companies span {
        background-color: #fafafa;
        border: solid 1pt #eaeaea;
        cursor: pointer;
      }
      .companies span:hover {
        border: 2px solid #5cb900;
        background-color: #dfa;
      }
    `;
  }

  selectCompany(company) {
    this.dispatchEvent(new CustomEvent("company-selected", { detail: company }));
  }

  renderCompany(c) {
    return html`<span @click=${(e) => this.selectCompany(c)}>${c.coname}</span>`;
  }
  render() {
    return this.companies && this.companies.length > 0 ? html` <div class="companies">${this.companies.map(this.renderCompany.bind(this))}</div> ` : html``;
  }
}

customElements.define("cwms-company-name-results", CompanyNameResult);
