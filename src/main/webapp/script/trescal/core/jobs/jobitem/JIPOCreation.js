
function init()
{
    $j("#createPurchaseOrderDialog").dialog({
        autoOpen: false,
        width: 1250,
        position: 'center',
        resizable: false,
        modal: true
    });

    $j("#createPurchaseOrderDialog").on('dialogclose', function(event) {
        $j('#createPurchaseOrderDialog').empty();
    });
}

function showCreatePurchaseOrderForm(){
    populateDialog($j("#currentJobItemId").val());
}

function populateDialog(jobItemId) {

    $j("#createPurchaseOrderDialog").dialog("open");
    $j('#createPurchaseOrderDialog').html('<div id="load" class="center-pad"><img src="img/icons/ajax-loader.gif" width="31" height="31" alt="" /></div>');

    $j.ajax({
        url: "purchaseorderdialogcontent.htm",
        data: {
            jobItemId: jobItemId
        },
        async: true
    })
    .done(function(fragment){
        $j('#createPurchaseOrderDialog').html(fragment);
        $j('#po_deldate').datepicker({
            defaultDate : null,
            changeMonth : true,
            changeYear : true,
            dateFormat : 'dd.mm.yy',
            minDate : 0
        });
        $j("select[name^='po_item_jobitemid']").change(function() {
            updatePOItem($j(this).val(),$j(this));
        });
        $j( '#createpurchaseorder' ).click( function() {
            compileAndInsertPurchaseOrder($j("#po_jobitemid").val());
        });
        loadScript.createCascadePlug();
        $j('#coname').focus();
    });
}

var index = 2;

function addNewPoItemToForm(){

    var regex = /^(.+?)(\d+)$/i;
    //var cloneParent = $j("items").children(":first");

    $j('#items table:first-child').clone(true)
        .appendTo($j("#items table:first-child").parent())
        .attr("id", "po_item" +  index)
        .find("*")
        .each(function() {
            var id = this.id || "";
            var match = id.match(regex) || [];
            if (match.length == 3) {
                // update the remove button onclick event so it points to the right index
                if(match[1] == 'po_item_remove'){
                    $j(this).attr("onclick", "removePoItem(" + index + "); return false; ")
                }
                //update all the ids with the new index
                this.id = match[1] + (index);
            }
        });
    index++

    // renumber all the po items
    var x = 1;
    $j('#po_items table[id^="po_item"]').each(function(i){
        if(x == 1){
            $j(this).removeClass('clonedpoitem');
        }else{
            $j(this).addClass('clonedpoitem');
        }
        $j(this).find('thead tr:first td:first').html(x);
        if($j(".po_item").size() > 1){
            $j(this).find('thead tr:first td:last a').show();
        }
        x++;
    });
}

/**
 * this method updates the poitem description textarea text with the ServiceType name
 * linked to the jobitem selected in the jobitem select dropdown
 * @param jobItemId
 * @param select
 */
function updatePOItem(jobItemId, select){

    var request = $j.ajax({
        type: "PUT",
        contentType: "application/json",
        url: "purchaseorderapi.json/updatedesc/" + jobItemId
    });

    request.done( function (desc) {
        select.closest(".po_item").find("textarea").val(desc);
    });

    request.fail(function( jqXHR, textStatus ) {
        alert( " PO Item Request failed: " + textStatus );
    });
}

/**
 * this method removes a purchase order item from the thickbox, resets the purchase order
 * item count and then re-numbers all purchase order items in the thickbox.
 * @param {Integer} id the id of the purchase order item to be deleted
 */
function removePoItem(id)
{
    // remove purchase order item from thickbox
    $j('#po_item' + id).remove();
    // reset the purchase order item count
    poitems = 1;
    // re-number all remaining purchase order items in thickbox
    $j('#po_items table[id^="po_item"]').each(function(i){
        // append new number
        $j(this).find('thead tr:first td:first').html(poitems);
        if($j(".po_item").size() == 1){
            console.log("Hide the last remove button");
            $j(this).find('thead tr:first td:last a').hide();
        }
        // add one to po count
        poitems++;
    });
}

/**
 * this method adds all purchase order item info to arrays.  The purchase order and items are created
 * using a dwr service.
 *
 * @param {Integer} jobitemid the id of the current job item so we know whether to update a row in table
 */
function compileAndInsertPurchaseOrder(jobitemid) {
    // disable the submit button to stop multiple presses
    $j('#createpurchaseorder').attr('disabled', true);
    // source all job item ids and add to array
    var jids = new Array();
    $j('select[id^="po_item_jobitemid"]').each(function (i, n) {
        jids.push($j(this).val());
    });
    // source all descriptions for purchase order items and add to array
    var descs = new Array();
    $j('textarea[id^="po_item_desc"]').each(function (i, n) {
        descs.push(n.value);
    });
    // source all prices for purchase order items and add to array
    var prices = new Array();
    $j('input[id^="po_item_value"]').each(function (i, n) {
        prices.push(n.value);
    });
    // source all discounts for purchase order items and add to array
    var discounts = new Array();
    $j('input[id^="po_item_disc"]').each(function (i, n) {
        discounts.push(n.value);
    });
    // source all nominal codes for purchase order items and add to array
    var nomidss = new Array();
    $j('select[id^="po_item_nominal"]').each(function (i, n) {
        nomidss.push(n.value);
    });
    // source all cost types for purchase order items and add to array
    var ctidss = new Array();
    $j('select[id^="po_item_costtype"]').each(function (i, n) {
        ctidss.push(n.value);
    });
    // source all purchase order item quantities and add to array
    var qtyss = new Array();
    $j('input[id^="po_item_qty"]').each(function (i, n) {
        qtyss.push(n.value);
    });

    // create new instance of a purchase order form using values from page and arrays
    var form = new AjaxPurchaseOrderForm($j('input#po_jobid').val(), jobitemid, $j('input#personid').val(), $j('input#addrid').val(), $j('select#po_retaddressid').val(), jids, descs, prices, discounts, nomidss, ctidss, qtyss, $j('select#currencyCode').val(), 
    		$j('input#po_deldate').val(), $j('input#po_clientref').val());

    //send ajax request
    var request = $j.ajax({
        type: "POST",
        contentType: "application/json",
        url: "purchaseorderapi.json",
        data: JSON.stringify(form),
        dataType: 'json'
    });

    //deal with response
    request.done(function (dto) {
        // check all purchase order items
        $j.each(dto.items, function (i, v) {
            // check to see if this is first row to be added to table
            if ($j('table#jiPurchaseOrders tbody tr:first td').length < 2) {
                // remove message row
                $j('table#jiPurchaseOrders tbody tr:first').remove();
            }
            // add row to purchase order table
            $j('table#jiPurchaseOrders tbody').prepend('<tr>' +
                '<td><a href="viewpurchaseorder.htm?id=' + dto.poId + '" class="mainlink">' + dto.poNo + '</a></td>' +
                '<td class="center">' + dto.items[i].itemno + '</td>' +
                '<td>' + dto.poCompanyName + '</td>' +
                '<td>' + dto.poContactName + '</td>' +
                '<td>' + dto.poFormattedRegDate + '</td>' +
                '<td>' + dto.poCreatedByName + '</td>' +
                '<td class="cost2pc">' + dto.poCurrencyERSymbol + dto.items[i].itemcost + '</td>' +
                '</tr>');
            // add the current job item state to header
            $j('span#currentState').text(dto.jobItemState);
        });

        $j('#warningBox1').hide();
        $j('#successBox1').show();
        $j('#po_link').html('<a href="viewpurchaseorder.htm?id=' + dto.poId + '" class="mainlink">' + dto.poNo + '</a>');
        // remove delivery date
        $j('#po_deldate').val('');
        // remove all purchase order items but leave item 1 so it can be cloned if required
        $j('.clonedpoitem').remove();
        $j('.removelink').hide();
        // clear cascading search text field and trigger event to clear results
        $j('input#coname').val('').trigger('keyup');
        // enable the submit button in thickbox
        $j('#createpurchaseorder').attr('disabled', false);
    });

    //Ajax request failed
    request.fail(function (jqXHR, textStatus) {
        // hide the success box if it's been shown already
        $j('#successBox1').hide();
        // empty the error warning box
        $j('#warningBox1').show();
        $j('#warningBox3').text(jqXHR.responseText);
        //re-enable the submit button
        $j('#createpurchaseorder').attr('disabled', false);
    });

}

/**
 * this is a purchase order form object.
 *
 * @param {Integer} jobid the id of the current job
 * @param {Integer} jobitemid the id of the current jobitem
 * @param {Integer} personid the id of the selected contact
 * @param {Integer} addressid the id of the selected address
 * @param {Integer} returnToAddressid the id of the selected return address
 * @param {Array} jobitemids array of job item ids
 * @param {Array} descs array of description values
 * @param {Array} prices array of price values
 * @param {Array} discounts array of discount values
 * @param {Array} nominalids array of nominal code ids
 * @param {Array} costtypeids array of cost type ids
 * @param {Array} qtys array of quantities
 * @param {String} deldate the delivery date for all purchase order items
 * @param {String} supplierref the supplier ref for purchase order
 */
function AjaxPurchaseOrderForm(jobid, jobitemid, personid, addressid, returnToAddressid, jobitemids, descs, prices, discounts, 
		nominalids, costtypeids, qtys, currencyCode, deliverydate, supplierref)
{
    // purchase order values
    this.jobid = jobid;
    this.jobitemid = jobitemid;
    this.personid = personid;
    this.addressid = addressid;
    this.returnToAddressid = returnToAddressid;
    this.supplierref = supplierref;
    // purchase order item values
    this.jobitemids = jobitemids;
    this.descs = descs;
    this.prices = prices;
    this.discounts = discounts;
    this.nominalids = nominalids;
    this.costtypeids = costtypeids;
    this.qtys = qtys;
    this.currencyCode = currencyCode;
    this.deliverydate = deliverydate;
}
