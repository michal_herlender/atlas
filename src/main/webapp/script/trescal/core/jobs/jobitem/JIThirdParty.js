/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	JIThirdParty.js
*	DESCRIPTION		:	Page-specific javascript file.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 
*						
*	KNOWN ISSUES	:	-
*	HISTORY			:	29/10/2007 - JV - Created File
*					:	20/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'script/trescal/core/jobs/jobitem/JobItemFunctions.js',
								'script/trescal/core/tools/labelprinting/dymo/DymoLabelFunctions.js',
								'script/thirdparty/jQuery/jquery.scrollTo.js',
                                'script/trescal/core/jobs/jobitem/JIPOCreation.js');


/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{
	// select the correct subnavigation link
	$j('#linkthirdparty').addClass('selected');
	// user preference to use job item key navigation
	if (userPrefs.jikeynav)	{
		// initialise key nav to move between job items
		initialiseKeyNavForJI();
	}
}

/**
 * removes the TPQuoteLink linking the current jobitem and a third party quotation item.
 * 
 * @param {Integer} id the id of the third party quote link
 */
function deleteTPQuoteLink(id)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/tpquotelinkservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to delete the third party quote link
			tpquotelinkservice.deleteTPQuoteLinkById(id);
			// remove the third party quote from table
			$j('#tpquotelink' + id).remove();
			// table empty and message should be appended?
			if ($j('table#TPQuotes tbody tr').length < 1)
			{
				// append no linked quotes message
				$j('table#TPQuotes tbody').append(	'<tr>' +
														'<td colspan="7" class="center bold">' + i18n.t("core.jobs:jobItem.noTPQuotation", "No Third Party Quotations Linked") + '</td>' +
													'</tr>');
			}
			// update linked quote counter
			$j('span#jiTPQuoteSize').text(parseInt($j('span#jiTPQuoteSize').text()) - 1);
		}
	});
}

/**
 * this method creates content for the thickbox which allows the user to link an existing third party quote
 * by retrieving all quotes which match the current job item instrument model.
 * 
 * @param {Object} modelid the id of the current instrument model
 * @param {Object} resultsYearFilter the number of years to search over
 * @param {Object} jobitemid the id of the current job item
 * @param {Object} requestedPage the page no being requested
 */
function linkTPQContent(modelid, resultsYearFilter, jobitemid, requestedPage)
{
	// add loading image and text to overlay content
	var messageContent = 	'<div id="linkTPQuoteOverlay" class="center-pad">' +
								'<img src="img/icons/ajax-loader.gif" width="31" height="31" alt="" />' +
								'<br /> <h4>' + i18n.t("core.jobs:jobItem.loadingLinkTPQuotation", "Loading Link Third Party Quotation") + '</h4>' +
							'</div>';
	// create initial overlay
	loadScript.createOverlay('dynamic', i18n.t("core.jobs:jobItem.linkTPQuotation", "Link Third Party Quotation"), null, messageContent, null, 20, 820, null);
	
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/tpquotationitemservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to retrieve all matching third party quote items
			tpquotationitemservice.searchAjaxTPQuotationItem(modelid, resultsYearFilter, false, null, requestedPage, 20,
			{
				callback:function(results)
				{					
					// create content for thickbox
					var content = 	'<div id="linkTPQuoteOverlay" class="center-pad">' +
										// add warning box to display error messages if the link fails
										'<div class="hid warningBox1">' +
											'<div class="warningBox2">' +
												'<div class="warningBox3">' +
												'</div>' +
											'</div>' +
										'</div>' +				
										'<table class="default4 jiTPQILink" summary="This table displays all third party quotes that can be linked to the current job item">' +
											'<thead>' +
												'<tr>' +
													'<td colspan="5">' +
														'<div style=" margin-top: -2px; " class="center">';
															if (results != null && results.resultsCount > 0)
															{
																if (results.previousPage == true)
																{
																	var prevPage = results.currentPage - 1;
																	content += '<a href="#" class="float-left mainlink" style=" padding-top: 4px; " onclick=" thickboxLinkTPQContent(' + modelid + ', ' + resultsYearFilter + ', ' + jobitemid + ', ' + prevPage + '); return false; " >&lt;&lt;&lt; ' + i18n.t("prev", "Prev") + '</a>';
																}
																else
																{
																	content += '<div class="float-left hid">&lt;&lt;&lt; ' + i18n.t("prev", "Prev") + '</div>';
																}																				
																if (results.nextPage == true)
																{
																	var nextPage = results.currentPage + 1;
																	content += '<a href="#" class="mainlink-float" style=" padding-top: 4px; " onclick=" thickboxLinkTPQContent(' + modelid + ', ' + resultsYearFilter + ', ' + jobitemid + ', ' + nextPage + '); return false; " >' + i18n.t("next", "Next") + ' &gt;&gt;&gt;</a>';
																}
																else
																{
																	content += '<div class="float-right hid">' + i18n.t("next", "Next") + ' &gt;&gt;&gt;</div>';
																}
																
																content += 	'<strong>' +
																				i18n.t("core.jobs:jobItem.displayingPage", "Displaying page") + ' ' +
																			'</strong>' +	
																			'<a href="#" id="hiddenSelectSubmit" class="hid" onclick=" thickboxLinkTPQContent(' + modelid + ', ' + resultsYearFilter + ', ' + jobitemid + ', $j(this).next().val()); return false; " ></a>' +
																			'<select onchange=" $j(\'a#hiddenSelectSubmit\').trigger(\'click\'); ">';
																				for (var i = 1; i < (results.pageCount + 1); i++)
																				{
																					if (i == results.currentPage)
																					{
																						content += '<option value="' + i + '" selected="selected">' + i + '</option>';
																					}
																					else
																					{
																						content += '<option value="' + i + '">' + i + '</option>';
																					}
																					if (i != results.pageCount)
																					{
																						content += '-';	
																					}
																				}
																content += 	'</select>' +
																			'<strong>';							
																				if (results.pageCount > 1)
																				{
																					content += i18n.t("core.jobs:jobItem.ofpageswithResults", {varPageCount : results.pageCount, varResultsCount : results.resultsCount, defaultValue : "of " + results.pageCount + " pages with " + results.resultsCount + " results"});
																				}
																					else
																				{
																					content += i18n.t("core.jobs:jobItem.ofpagewithResults", {varPageCount : results.pageCount, varResultsCount : results.resultsCount, defaultValue : "of " + results.pageCount + " page, with " + results.resultsCount + " results"});
																				} 
																content += 	'</strong>' +
																			// clear floats and restore page flow
																			'<div class="clear-0"></div>';
															}
															else
															{
																content += 	'<strong>' +
																				i18n.t("core.jobs:jobItem.searchReturnedNoResults", "Search returned no results") + 
																			'</strong>';
															}
										content +=	'</div>' +
													'</td>' +
												'</tr>';
												if (results.results.length > 0)
												{
													content += 	'<tr>' +
																	'<td colspan="5">' + i18n.t("core.jobs:jobItem.mfrAppearsOnFollowingTPQuotations", {varMfr : results.results[0].model.mfr.name + ' ' + results.results[0].model.model + ' ' + results.results[0].model.description.description, defaultValue :  "The <span class='bold'>" + results.results[0].model.mfr.name + " " + results.results[0].model.model + " " + results.results[0].model.description.description + "</span> appears on the following third party quotations:"}) + '</td>' +
																'</tr>';
												}										
									content +=	'<tr>' +
													'<th class="qno" scope="col">' + i18n.t("quoteNo", "Quote No") + '</th>' +
													'<th class="item" scope="col">' + i18n.t("item", "Item") + '</th>' +
													'<th class="comp" scope="col">' + i18n.t("company", "Company") + '</th>' +
													'<th class="cont" scope="col">' + i18n.t("contact", "Contact") + '</th>' +											
													'<th class="link" scope="col">' + i18n.t("link", "Link") + '</th>' +											
												'</tr>' +
											'</thead>' +											
											'<tbody>';
												if (results.results.length < 1)
												{
													content += 	'<tr>' +
																	'<td colspan="5" class="center bold">' + i18n.t("core.jobs:jobItem.noTPQuotationItemsMatched", "No third party quotation items matched the instrument model") + '</td>' +
																'</tr>';
												}
												else
												{
													$j.each(results.results, function(i)
													{
														// assign third party quote item
														var tpqitem = results.results[i];
														// add row for each matching quote item
														content +=	'<tr>' +
																		'<td>' + tpqitem.tpquotation.qno + '</td>' +
																		'<td class="center">' + tpqitem.itemno + '</td>' +
																		'<td>' + tpqitem.tpquotation.contact.sub.comp.coname + '</td>' +
																		'<td>' + tpqitem.tpquotation.contact.name + '</td>';
														if(tpqitem.linkedTo.length >= tpqitem.quantity)
															content += '<td>' + i18n.t("linked", "Linked") + '</td>';
														else content += '<td class="center"><a href="#" onclick=" linkTPQuoteToJobitem(' + tpqitem.id + ', ' + jobitemid + '); return false; " class="mainlink">' + i18n.t("link", "Link") + '</a></td>';
														content += '</tr>';
													});								
												}																				
								content += 	'</tbody>' +
										'</table>' +
									'</div>';
					// modify overlay and set new content
					loadScript.modifyOverlay(null, content, i18n.t("core.jobs:jobItem.linkTPQuotation", "Link Third Party Quotation"), false, 60, null, null);
				}
			});
		}
	});
}

/**
 * this method links an existing third party quote item to the current job item
 * 
 * @param {Integer} tpquoteitemid the third party quote item id
 * @param {Integer} jobitemid the current job item id
 */
function linkTPQuoteToJobitem(tpquoteitemid, jobitemid)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/tpquotelinkservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to link third party quote item to job item
			tpquotelinkservice.insertAjaxTPQuoteLink(jobitemid, tpquoteitemid,
			{
				callback:function(results)
				{
					if(!results.success)
					{
						if(results.errorList)
						{
							// empty the error warning box
							$j('div#linkTPQuoteOverlay div.warningBox3').empty();
							// variable to hold error messages
							var errors = '';
							$j.each(results.errorList, function(i)
							{
								// display error messages
								var error = results.errorList[i];
								errors = '<div>' + i18n.t("fieldError", {varErrField: error.field, varErrMsg:error.defaultMessage, defaultValue : "Field: " + error.field + " failed. Message:" + error.defaultMessage}) + '</div>';
							});
							// show error message in error warning box
							$j('div#linkTPQuoteOverlay div:first').removeClass('hid').addClass('vis').find('.warningBox3').append(errors);
						}
						else
						{
							// empty the error warning box
							$j('div#linkTPQuoteOverlay div.warningBox3').empty();
							// show error message in error warning box
							$j('div#linkTPQuoteOverlay div:first').removeClass('hid').addClass('vis').find('.warningBox3').append(results.message);
						}
					}
					else
					{
						// assign third party result objects
						var tpquoteitemlink = results.results;
						var tpquoteitem = tpquoteitemlink.tpQuoteItem;
						var tpquote = tpquoteitem.tpquotation;
									
						// table empty and message present?
						if ($j('table#TPQuotes tbody tr:first td').length < 2)
						{
							// append no linked quotes message
							$j('table#TPQuotes tbody tr:first').remove();
						}
						
						// append new table row for linked third party quote
						$j('#TPQuotes tbody ').prepend(	'<tr id="tpquotelink' + tpquoteitemlink.id + '">' +
															'<td>' +
																'<a href="' + springUrl + '/viewtpquote.htm?id=' + tpquote.id + '")" class="mainlink">' + tpquote.qno + '</a>' +
															'</td>' +
															'<td class="center">' + tpquoteitem.itemno + '</td>' +
															'<td>' + tpquote.contact.sub.comp.coname + '</td>' +
															'<td>' + tpquote.contact.name + '</td>' +
															'<td>' + tpquoteitemlink.contact.name + '</td>' +
															'<td>' + tpquoteitemlink.formattedDate + '</td>' +
															'<td class="center">' +
																'<a href="#" onclick=" deleteTPQuoteLink(' + tpquoteitemlink.id + '); return false; ">' +
																	'<img src="img/icons/delete.png" width="16" height="16" alt="' + i18n.t("core.jobs:jobItem.removeLinkedTPQuote", "Remove Linked Third Party Quote") + '" title="' + i18n.t("core.jobs:jobItem.removeLinkedTPQuote", "Remove Linked Third Party Quote") + '" />' +
																'</a>' +
															'</td>' +
														'</tr>');
														
						// update linked quote counter
						$j('span#jiTPQuoteSize').text(parseInt($j('span#jiTPQuoteSize').text()) + 1);
						// close the overlay						
						loadScript.closeOverlay();
					}
				}
			});
		}
	});
}

/**
 * this method creates content for the overlay which allows the user to record a third party quote
 * 
 * @param {Integer} jobitemid the id of the job item
 */
function recordTPQContent(jobitemid)
{
	// initialise content variable
	var content = '';
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/tpquotationservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to retrieve reference data for recording a third party quote
			tpquotationservice.getAjaxTPQuotationReferenceData(jobitemid, 
			{
				callback:function(results)
				{
					// assign currency and status lists
					var defaultCurrencyCode = results.defaultCurrencyCode;
					console.log('defaultCurrencyCode : '+defaultCurrencyCode);
					var currencyList = results.currencyList;
					var statusList = results.statusList;
					// get todays date (formatting adapted from date-utils.js, but can't use module here)
					var today = new Date();
					var dateFormat = Intl.DateTimeFormat("en-CA",{dateStyle:"short"});
					// create content for thickbox
					content +=	'<div id="recordTPQuoteOverlay">' +
									// add warning box to display error messages if the link fails
									'<div class="hid warningBox1">' +
										'<div class="warningBox2">' +
											'<div class="warningBox3">' +
											'</div>' +
										'</div>' +
									'</div>' +
									'<fieldset>' +
										'<ol>' +
											'<li>' +
												'<label>' + i18n.t("companyLabel", "Company:") + '</label>' +																							
												'<div id="cascadeSearchPlugin">' +
													'<input type="hidden" id="compCoroles" value="supplier,business" />' +
													'<input type="hidden" id="cascadeRules" value="subdiv,contact" />' +
													'<input type="hidden" id="customHeight" value="340" />' +
												'</div>' +																												
												'<div class="clear"></div>' +
											'</li>' +
											'<li>' +
												'<label>' + i18n.t("core.jobs:jobItem.tpQuotationNo", "TP Quotation No:") + '</label>' +
												'<input type="text" name="tpqr_tpqno" id="tpqr_tpqno" value="" tabindex="2" />' +
											'</li>' +																			
											'<li>' +
												'<label>' + i18n.t("core.jobs:jobItem.quotationDate", "Quotation Date:") + '</label>' +
												'<input type="date" name="tpqr_date" id="tpqr_date" value="' + dateFormat.format(today) + '" tabindex="3" />' +
											'</li>' +
											'<li>' +
												'<label>' + i18n.t("core.jobs:jobItem.calCostLabel", "Cal Cost:") + '</label>' +
												'<input type="text" name="tpqr_calcost" id="tpqr_calcost" value="0.00" tabindex="4" />' +
												'<a href="#" class="marg-left mainlink" onclick=" $j(this).addClass(\'hid\').parent().next().removeClass(); return false; ">' + i18n.t("core.jobs:jobItem.addCalDisc", "Add Cal Disc") + '</a>' +
											'</li>' +
											'<li class="hid">' +
												'<label>' + i18n.t("core.jobs:jobItem.calDiscount", "Cal Discount:") + '</label>' +
												'<input type="text" name="tpqr_calcostdisc" id="tpqr_calcostdisc" value="0.00" tabindex="5" /> &#37;' +
											'</li>' +
											'<li>' +
												'<label>' + i18n.t("core.jobs:jobItem.repairCost", "Repair Cost:") + '</label>' +
												'<input type="text" name="tpqr_repaircost" id="tpqr_repaircost" value="0.00" tabindex="6" />' +
												'<a href="#" class="marg-left mainlink" onclick=" $j(this).addClass(\'hid\').parent().next().removeClass(); return false; ">' + i18n.t("core.jobs:jobItem.addRepairDisc", "Add Repair Disc") + '</a>' +
											'</li>' +
											'<li class="hid">' +
												'<label>' + i18n.t("core.jobs:jobItem.repairDiscount", "Repair Discount:") + '</label>' +
												'<input type="text" name="tpqr_repaircostdisc" id="tpqr_repaircostdisc" value="0.00" tabindex="7" /> &#37;' +
											'</li>' +
											'<li>' +
												'<label>' + i18n.t("core.jobs:jobItem.adjustCost", "Adjust Cost:") + '</label>' +
												'<input type="text" name="tpqr_adjustcost" id="tpqr_adjustcost" value="0.00" tabindex="8" />' +
												'<a href="#" class="marg-left mainlink" onclick=" $j(this).addClass(\'hid\').parent().next().removeClass(); return false; ">' + i18n.t("core.jobs:jobItem.addAdjustDisc", "Add Adjust Disc") + '</a>' +
											'</li>' +
											'<li class="hid">' +
												'<label>' + i18n.t("core.jobs:jobItem.adjustDiscount", "Adjust Discount:") + '</label>' +
												'<input type="text" name="tpqr_adjustcostdisc" id="tpqr_adjustcostdisc" value="0.00" tabindex="9" /> &#37;' +
											'</li>' +
											'<li>' +
												'<label>' + i18n.t("core.jobs:jobItem.salesCost", "Sales Cost:") + '</label>' +
												'<input type="text" name="tpqr_salescost" id="tpqr_salescost" value="0.00" tabindex="10" />' +
												'<a href="#" class="marg-left mainlink" onclick=" $j(this).addClass(\'hid\').parent().next().removeClass(); return false; ">' + i18n.t("core.jobs:jobItem.addSalesDisc", "Add Sales Disc") + '</a>' +
											'</li>' +
											'<li class="hid">' +
												'<label>' + i18n.t("core.jobs:jobItem.salesDiscount", "Sales Discount:") + '</label>' +
												'<input type="text" name="tpqr_salescostdisc" id="tpqr_salescostdisc" value="0.00" tabindex="11" /> &#37;' +
											'</li>' +
											'<li>' +
												'<label>' + i18n.t("core.jobs:jobItem.inspectionCost", "Inspection Cost:") + '</label>' +
												'<input type="text" name="tpqr_inspectioncost" id="tpqr_inspectioncost" value="0.00" tabindex="12" />' +
											'</li>' +
											'<li>' +
												'<label>' + i18n.t("core.jobs:jobItem.itemDiscount", "Item Discount:") + '</label>' +
												'<input type="text" name="tpqr_itemdisc" id="tpqr_itemdisc" value="0.00" tabindex="13" /> &#37;' +
											'</li>' +
											'<li>' +
												'<label>' + i18n.t("core.jobs:jobItem.carriageIn", "Carriage In:") + '</label>' +
												'<input type="text" name="tpqr_carin" id="tpqr_carin" value="0.00" tabindex="14" />' +
											'</li>' +
											'<li>' +
												'<label>' + i18n.t("core.jobs:jobItem.carriageOut", "Carriage Out:") + '</label>' +
												'<input type="text" name="tpqr_carout" id="tpqr_carout" value="0.00" tabindex="15" />' +
											'</li>' +
											'<li>' +
												'<label>' + i18n.t("statusLabel", "Status:") + '</label>' +
												'<select name="tpqr_statusid" id="tpqr_statusid" tabindex="16">';
													$j.each(statusList, function(i)
													{													
														content += '<option value="' + statusList[i].statusid + '">' + statusList[i].name + '</option>';
													});
									content +=	'</select>' +
											'</li>' +
											'<li>' +
												'<label>' + i18n.t("currencyLabel", "Currency:") + '</label>' +
												'<select name="tpqr_currencyCode" id="tpqr_currencyCode" tabindex="17">';
													$j.each(currencyList, function(i)
													{
														var selectedText = currencyList[i].currencyCode == defaultCurrencyCode ? ' selected="selected"' : '';
														content += '<option value="' + currencyList[i].currencyCode + '"'+selectedText+'>' + currencyList[i].currencyCode + '</option>';
													});
									content += 	'</select>' +
											'</li>' +
											'<li>' +
												'<label>' + i18n.t("durationLabel", "Duration:") + '</label>' +
												'<input type="text" name="tpqr_duration" id="tpqr_duration" value="30" tabindex="18" /> ' + i18n.t("days", "days") +
											'</li>' +
											'<li>' +
												'<label>&nbsp;</label>' +
												'<input type="hidden" name="tpqr_jobitemid" id="tpqr_jobitemid" value="' + jobitemid + '" />' +
												'<input type="button" value="' + i18n.t("submit", "Submit") + '" onclick=" createTPQuote(); return false; " tabindex="19" />' +
											'</li>' +
										'</ol>' +
									'</fieldset>' +
								'</div>';
										
					// create new overlay using content variable
					loadScript.createOverlay('dynamic', i18n.t("core.jobs:jobItem.recordTPQuotation", "Record Third Party Quotation"), null, content, null, 70, 640, 'coname');
					// call main init function to create the cascading search plugin
					loadScript.createCascadePlug();									
				}
			});
		}
	});	
}

/**
* Function looks for inputs named tpqf_ and uses them to create a new Third Party Quotation.
*/
function createTPQuote()
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/tpquotationservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to create a third party quotation
			tpquotationservice.createAjaxTPQuotation(	$j('#personid').val(),
														$j('#tpqr_tpqno').val(),
														$j('#tpqr_date').val(),
														$j('#tpqr_calcost').val(),
														$j('#tpqr_calcostdisc').val(),
														$j('#tpqr_repaircost').val(),
														$j('#tpqr_repaircostdisc').val(),
														$j('#tpqr_adjustcost').val(),
														$j('#tpqr_adjustcostdisc').val(),
														$j('#tpqr_salescost').val(),
														$j('#tpqr_salescostdisc').val(),
														$j('#tpqr_inspectioncost').val(),
														$j('#tpqr_itemdisc').val(),
														$j('#tpqr_carin').val(),
														$j('#tpqr_carout').val(),
														$j('#tpqr_jobitemid').val(),
														$j('#tpqr_duration').val(),
														$j('#tpqr_currencyCode').val(),
														$j('#tpqr_statusid').val(), 
			{
				callback:function(results)
				{
					if(!results.success)
					{
						if(results.errorList)
						{
							// empty the error warning box
							$j('div#recordTPQuoteOverlay div.warningBox3').empty();
							// show error message in error warning box
							$j('div#recordTPQuoteOverlay div:first').removeClass('hid').addClass('vis').find('.warningBox3').append(loadScript.unwrapErrors(results.errorList));
						}
						else
						{
							// empty the error warning box
							$j('div#recordTPQuoteOverlay div.warningBox3').empty();
							// show error message in error warning box
							$j('div#recordTPQuoteOverlay div:first').removeClass('hid').addClass('vis').find('.warningBox3').append(results.message);
						}
					}
					else
					{
						// DTO used for third party results
						var tpqDto = results.results;
									
						// table empty and message present?
						if ($j('table#TPQuotes tbody tr:first td').length < 2)
						{
							// append no linked quotes message
							$j('table#TPQuotes tbody tr:first').remove();
						}
						
						/*
	private Integer tpqDto.tpQuoteId;
	private Integer tpqDto.tpQuoteItemLinkId;
	private Integer tpqDto.tpQuoteItemNo;
	private String tpqDto.tpQuoteQno;
	private String tpqDto.tpQuoteCompanyName;
	private String tpqDto.tpQuoteContactName;
	private String tpqDto.tpQuoteItemLinkContactName;
	private String tpqDto.tpQuoteItemLinkDate;
						 */
						
						// append new table row for linked third party quote
						$j('#TPQuotes tbody ').append(	'<tr id="tpquotelink' + tpqDto.tpQuoteItemLinkId + '">' +
															'<td>' +
																'<a href="viewtpquote.htm?id=' + tpqDto.tpQuoteId + '&amp;ajax=tpquote&amp;width=500" class="jconTip mainlink" name="' + i18n.t("tpQuoteInformation", "Third Party Quote Information") + '" id="tpquote' + tpqDto.tpQuoteId + '' + $j('table#TPQuotes tbody tr').length + '" tabindex="-1">' + tpqDto.tpQuoteQno + '</a> ' +
																'<img src="img/icons/mouse.png" width="12" height="12" class="image_inl" alt="" title="" />' +
															'</td>' +
															'<td class="center">' + tpqDto.tpQuoteItemNo + '</td>' +
															'<td>' + tpqDto.tpQuoteCompanyName + '</td>' +
															'<td>' + tpqDto.tpQuoteContactName + '</td>' +
															'<td>' + tpqDto.tpQuoteItemLinkContactName + '</td>' +
															'<td>' + tpqDto.tpQuoteItemLinkDate + '</td>' +
															'<td class="center">' +
																'<a href="#" onclick=" deleteTPQuoteLink(' + tpqDto.tpQuoteItemLinkId + '); return false; ">' +
																	'<img src="img/icons/delete.png" width="16" height="16" alt="' + i18n.t("core.jobs:jobItem.removeLinkedTPQuote", "Remove Linked Third Party Quote") + '" title="' + i18n.t("core.jobs:jobItem.removeLinkedTPQuote", "Remove Linked Third Party Quote") + '" />' +
																'</a>' +
															'</td>' +
														'</tr>');
														
						// update linked quote counter
						$j('span#jiTPQuoteSize').text(parseInt($j('span#jiTPQuoteSize').text()) + 1);
					}		
					// initialise tooltip
					JT_init();
					// close the overlay					
					loadScript.closeOverlay();
				}
			});
		}
	});
}

/**
 * this method creates content for the overlay which allows the user to request a third party quote
 * 
 * @param {Integer} jobitemid the id of the job item
 */
function requestThirdPartyQuoteContent(jobitemid)
{
	// initialise content variable
	var content = '';
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/costtypeservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to retrieve all active cost types
			costtypeservice.getAllActiveCostTypeDtos(
			{
				callback:function(costtypes)
				{
					// check cost types returned?
					if (costtypes.length > 0)
					{
						// get todays date (formatting adapted from date-utils.js, but can't use module here)
						var today = new Date();
						var dateFormat = Intl.DateTimeFormat("en-CA",{dateStyle:"short"});
						// create content for overlay
						content +=	'<div id="requestTPQuoteOverlay">' +
										// add warning box to display error messages if fails
										'<div class="hid warningBox1">' +
											'<div class="warningBox2">' +
												'<div class="warningBox3">' +
												'</div>' +
											'</div>' +
										'</div>' +
										'<fieldset>' +
											'<ol>' +
												'<li>' +
													'<label>' + i18n.t("companyLabel", "Company:") + '</label>' +																							
													'<div id="cascadeSearchPlugin">' +
														'<input type="hidden" id="compCoroles" value="supplier,business" />' +
														'<input type="hidden" id="cascadeRules" value="subdiv,contact" />' +
														'<input type="hidden" id="customHeight" value="340" />' +
													'</div>' +																												
													'<div class="clear"></div>' +
												'</li>' +
												'<li>' +
													'<label>' + i18n.t("core.jobs:jobItem.requestDueDate", "Request Due-Date:") + '</label>' +
													'<input type="date" name="rtpq_duedate" id="rtpq_duedate" value="' + dateFormat.format(today) + '" tabindex="2" />' +
												'</li>' +
												'<li>' +
												'<label class="ajaxlabel">' + i18n.t("core.jobs:jobItem.defaultCostTypes", "Default Cost-Types:") + '</label>';
													$j.each(costtypes, function(i, ct)
													{
														console.log(ct);
														content +=	'<input type="checkbox" name="rtpq_costtype" value="' + ct.key + '" /> ' + ct.value + '<br />';
													});
									content +=	'</li>' +
												'<li>' +
													'<label>&nbsp;</label>' +
													'<input type="hidden" name="rtpq_jobitemid" id="rtpq_jobitemid" value="' + jobitemid + '" />' +
													'<input type="button" value="' + i18n.t("submit", "Submit") + '" onclick=" createTPQuoteRequest(); return false; " tabindex="" />' +
												'</li>' +
											'</ol>' +
										'</fieldset>' +
									'</div>'; 	

									// create new overlay using content variable
									loadScript.createOverlay('dynamic', i18n.t("core.jobs:jobItem.requestTPQuotation", "Request Third Party Quotation"), null, content, null, 70, 520, 'coname');
									// call main init function to create the cascading search plugin
									loadScript.createCascadePlug();
					}
					else
					{
						$j.prompt('An error occured retrieving cost types for third party quotation request');
					}
				}
			});
		}
	});
}

/**
 * this method uses values provided in the content overlay to generate a third party quotation request
 */
function createTPQuoteRequest()
{
	// array of cost types selected
	var ctypesArray = new Array ();
	// get all cost types selected
	$j('div#requestTPQuoteOverlay input[name="rtpq_costtype"]:checked').each(function(i, n)
	{
		// add cost type id
		ctypesArray.push(n.value);
	});
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/tpquoterequestservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to create a third party quotation request for item
			tpquoterequestservice.createAjaxTPQuotationRequest($j('#rtpq_jobitemid').val(), $j('#personid').val(), $j('#rtpq_duedate').val(), ctypesArray, 
			{
				callback:function(results)
				{
					if(!results.success)
					{
						if(results.errorList)
						{
							// empty the error warning box
							$j('div#requestTPQuoteOverlay div.warningBox3').empty();
							// show error message in error warning box
							$j('div#requestTPQuoteOverlay div:first').removeClass('hid').addClass('vis').find('.warningBox3').append(loadScript.unwrapErrors(results.errorList));
						}
						else
						{
							// empty the error warning box
							$j('div#requestTPQuoteOverlay div.warningBox3').empty();
							// show error message in error warning box
							$j('div#requestTPQuoteOverlay div:first').removeClass('hid').addClass('vis').find('.warningBox3').append(results.message);
						}
					}
					else
					{
						// assign third party quotation request to variable
						var tpqrDto = results.results;															
						// table empty and message present?
						if ($j('table#TPQuoteReqs tbody tr:first td').length < 2)
						{
							// append no linked tp quote reqs message
							$j('table#TPQuoteReqs tbody tr:first').remove();
						}						
						// append new table row for linked third party quote request
						$j('table#TPQuoteReqs tbody ').append(	'<tr>' +
																	'<td><a href="viewtpquoterequest.htm?id=' + tpqrDto.id + '" class="mainlink">' + tpqrDto.requestNo + '</a></td>' +
																	'<td>' + tpqrDto.companyName + '</td>' +
																	'<td>' + tpqrDto.contactName + '</td>' +
																	'<td>' + tpqrDto.formattedReqDate + '</td>' +
																	'<td>' + tpqrDto.formattedDueDate + '</td>' +
																	'<td>' + tpqrDto.status + '</td>' +
																'</tr>');														
						// update linked quote counter
						$j('span.jiTPQuoteReqSize').text(parseInt($j('span.jiTPQuoteReqSize').text()) + 1);
					}		
					// close the overlay					
					loadScript.closeOverlay();
				}
			});
		}
	});
}

/**
 * this method adds select boxes to all boolean cells on third party requirement table
 * 
 * @param {Integer} id third party requirement id
 */
function editTPRequirement(id)
{
	// get all boolean fields in requirement row
	$j('tr#tpr' + id + ' td.booleanfield').each(function(i)
	{
		var selectTrue = '';
		var selectFalse = '';
		// current image has class tick?
		if($j(this).find('img').attr('class') == 't')
		{
			selectTrue = ' selected="selected" ';
		}
		else
		{
			selectFalse = ' selected="selected" ';
		}
		// add select to table cell
		$j(this).html(	'<select>' +
							'<option ' + selectTrue + ' value="true">' + i18n.t("yes", "Yes") + '</option>' + 
							'<option ' + selectFalse + ' value="false">' + i18n.t("no", "No") + '</option>' + 
						'</select>');
	});
	// hide edit link and show save link
	$j('a#edittpreqlink' + id).removeClass('vis').addClass('hid');
	$j('a#savetpreqlink' + id).removeClass('hid').addClass('vis');
}

/**
 * this method saves updates to third party requirements
 * 
 * @param {Integer} id third party requirement id
 */
function saveTPRequirement(id)
{
	// array of boolean values
	var bools = new Array ();
	// get all boolean fields in requirement row
	$j('tr#tpr' + id + ' td.booleanfield').each(function(i)
	{
		// get boolean value from select
		var bool = $j(this).find('select').val();
		// add boolean value to array
		bools.push(bool);
	});
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/tprequirementservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to update third party requirement
			tprequirementservice.ajaxUpdateTPRequirement(id, bools[0], bools[1], bools[2], bools[3],
			{
				callback:function(resWrap)
				{
					if(resWrap.success == false)
					{
						$j.prompt(i18n.t("core.jobs:jobItem.errorUpdatingTPrequirement", "An error occurred when updating third party requirements"));
					}
					else
					{	
						// assign third party requirement to variable
						var tpr = resWrap.results;
						// create boolean value array from wrapper values
						var bools = new Array();
						bools.push(tpr.investigation);
						bools.push(tpr.calibration);
						bools.push(tpr.repair);
						bools.push(tpr.adjustment);
						// get all boolean fields in requirement row
						$j('tr#tpr' + id + ' td.booleanfield').each(function(i)
						{
							if(bools[i] == true)
							{
								$j(this).html('<img src="img/icons/greentick.png" class="t" width="16" height="16" />');
							}
							else
							{
								$j(this).html('<img src="img/icons/redcross.png" class="c" width="16" height="16" />');
							}				
						});
						// populate the updated by table cell
						$j('td#tpr' + id + '-updated').html(tpr.updatedBy.name + '<br/>(' + formatDate(tpr.lastUpdated, 'hh:mm:ss - dd.MM.yyyy') + ')');
						// hide edit link and show save link
						$j('a#edittpreqlink' + id).removeClass('hid').addClass('vis');
						$j('a#savetpreqlink' + id).removeClass('vis').addClass('hid');
					}
				}
			});
		}
	});
}

/**
 * this method displays purchase orders to which this item can be linked
 * 
 * @param {Integer} jobId the current job id
 * @param {Integer} jobItemId the current job item id
 */
function displayPurchaseOrdersToLink(jobId, jobItemId)
{
	// add loading image and text to overlay content
	var messageContent = 	'<div id="displayPOsToLinkOverlay" class="center-pad">' +
								'<img src="img/icons/ajax-loader.gif" width="31" height="31" alt="" />' +
								'<br /> <h4>' + i18n.t("core.jobs:jobItem.loadingPOToLink", "Loading Purchase Orders To Link") + '</h4>' +
							'</div>';
	// create new overlay
	loadScript.createOverlay('dynamic', i18n.t("core.jobs:jobItem.displayPOToLink", "Display Purchase Orders To Link"), null, messageContent, null, 20, 820, null);
	
	$j.ajax({
		url: "displayLinkedPurchaseOrders.htm",
		data: {
			jobId: jobId,
			jobItemId: jobItemId
		},
		async: true
	})
	.done(function(content) {
		loadScript.modifyOverlay(null, content, i18n.t("core.jobs:jobItem.displayPOToLink", "Display Purchase Orders To Link"), false, 60, null, null);
	});
}

/**
 * this method displays all purchase order items on a purchase order
 * 
 * @param {Object} row table row
 * @param {Integer} orderid purchase order id
 * @param {String} appendTo id text for elements
 * @param {Integer} jobitemid job item id of CURRENT job item
 */
function displayLinkedPurchaseOrderItems(row, orderid, appendTo, jobitemid) {
	// purchase order items for this purchase order row have already been retrieved
	if ($j('#' + appendTo + '-child').length) {
		// purchase order items are currently hidden so make them visible
		if ($j('#' + appendTo + '-child').css('display') == 'none') {
			// remove image to show items and append hide items image
			$j('#' + appendTo).find('a:last').empty().append('<img src="img/icons/items_hide.png" width="16" height="16" alt="' + i18n.t("hideItems", "Hide Items") + '" title="' + i18n.t("hideItems", "Hide Items") + '" class="image_inline" />');
			// make the purchase order items row visible and add slide down effect
			$j('#' + appendTo + '-child').css({ display: '', visibility: 'visible' }).find('div:first').slideDown(1000);
		}
		// purchase order items are currently visible so hide them
		else {
			// remove image to hide items and append show items image
			$j('#' + appendTo).find('a:last').empty().append('<img src="img/icons/items.png" width="16" height="16" alt="' + i18n.t("showItems", "Show Items") + '" title="' + i18n.t("showItems", "Show Items") + '" class="image_inline" />');
			// add effect to slide up purchase order items and hide row
			$j('#' + appendTo + '-child div').slideUp(1000, function(){
				$j('#' + appendTo + '-child').css({ display: 'none', visibility: '' });
			});
		}
	}
	else {
		$j.ajax({
			url: "displayLinkedPurchaseOrderItems.htm",
			data: {
				purchaseOrderId: orderid,
				appendTo: appendTo,
				jobItemId: jobitemid
			},
			async: true
		}).done(function(content) {
			// append item row
			$j('#' + appendTo).after(content);
			// remove image to show items
			$j('#' + appendTo).find('a:last').empty().append('<img src="img/icons/items_hide.png" width="16" height="16" alt="' + i18n.t("hideItems", "Hide Items") + '" title="' + i18n.t("hideItems", "Hide Items") + '" class="image_inline" />');
			// add effect to slide down delivery items
			$j('#' + appendTo + '-child div').slideDown(1000);
		});
	}	
}

/**
 * this method links a job item to a po item and updates page content accordingly
 * 
 * @param {Integer} jid the job item id to be linked
 * @param {Integer} poiid the purchase order item id to be linked
 */
function linkJobItemToPoItem(jid, poiid) {
	$j.ajax({
		url: "linkJobItemToPoItem.htm",
		data: {
			jobItemId: jid,
			poItemId: poiid
		},
		async: true
	}).done(function(content) {
		// message in table?
		if ($j('table#jiPurchaseOrders tbody tr:first td').length < 2)
			// remove the message row
			$j('table#jiPurchaseOrders tbody tr:first').remove();
		// append to table
		$j('table#jiPurchaseOrders tbody').append(content);
		// update po's linked counter
		$j('span#posLinkedSize').text(parseInt($j('span#posLinkedSize').text()) + 1);
		// add text and remove anchor
		$j('a#linkPOI' + poiid).after('Linked').remove();
		// close overlay
		loadScript.closeOverlay();
	}).fail(function(error) {
		// empty the error warning box
		$j('div#displayPOsToLinkOverlay div.warningBox3').empty();
		// show error message in error warning box
		$j('div#displayPOsToLinkOverlay div:first').removeClass('hid').addClass('vis').find('.warningBox3').append(error);
	});
}

/**
 * this method creates the content for a user to add or edit a third party instruction
 * 
 * @param {String} type is either ('add' or 'edit')
 * @param {Integer} jobItemId id of the job item to add instruction for
 * @param {Integer} instructionId id of the instruction to edit if applicable
 * @param {String} instructionValue value of instruction to edit if applicable
 */
function createAddOrEditJITPInstruction(type, jobItemId, instructionId, instructionValue)
{
	// add or edit?
	var topVerb = (type == 'add') ? i18n.t("core.jobs:jobItem.createTPInstruction", "Create Third Party Instruction") : i18n.t("core.jobs:jobItem.editTPInstruction", "Edit Third Party Instruction");		
	// create content to append in overlay
	var content = 	'<div id="jitpInstructionOverlay">' +
						// add warning box to display error messages if the insertion or update fails
						'<div class="hid warningBox1">' +
							'<div class="warningBox2">' +
								'<div class="warningBox3">' +
								'</div>' +
							'</div>' +
						'</div>' +
						'<fieldset>' +
							'<ol>' +										
								'<li>' +
									'<label class="twolabel marg-right">' + i18n.t("core.jobs:jobItem.calInstructionsForTP", "CALIBRATION INSTRUCTIONS FOR THIRD PARTY DELIVERY NOTES:") + '</label>' +
									'<textarea id="commentText" class="width70 symbolBox presetComments THIRD_PARTY_INSTRUCTION" rows="4" value="' + instructionValue + '">' + instructionValue + '</textarea>' +
								'</li>' +
								'<li>' +
									'<label>&nbsp;</label>';
									if(type == 'add')
									{
										content += '<input type="button" name="save" onclick=" $j(this).attr(\'disabled\', \'disabled\'); saveJITPInstruction(' + jobItemId + ', $j(\'textarea#commentText\').val()); return false; " value="' + i18n.t("core.jobs:jobItem.addInstruction", "Add Instruction") + '" />';
									}
									else
									{
										content += '<input type="button" name="update" onclick=" $j(this).attr(\'disabled\', \'disabled\'); editJITPInstruction(' + instructionId + ', $j(\'textarea#commentText\').val()); return false; " value="' + i18n.t("core.jobs:jobItem.updateInstruction", "Update Instruction") + '" />';
									}
								content = content + '</li>' +
							'</ol>' +
						'</fieldset>' +
					'</div>';
	// create new overlay using content variable
	loadScript.createOverlay('dynamic', topVerb, null, content, null, 40, 860, 'commentText');
	// initialise preset comments after small delay for pop up to appear
	setTimeout('loadScript.init(true);', 500);
}

/**
 * this method adds a new third party instruction to a job item.
 * 
 * @param {Integer} jobItemId the id of the job item to add third party instruction for
 * @param {String} instructionValue the value of instruction to add
 */
function saveJITPInstruction(jobItemId, instructionValue)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/tpinstructionservice.js'), true,
	{
		callback: function()
		{			
			// call dwr service to insert new job item third party instruction
			tpinstructionservice.insertTPInstruction(jobItemId, instructionValue,
			{
				callback: function(result)
				{
					// insert instruction not successful
					if(!result.success)
					{
						// show error message in error warning box
						$j('div#jitpInstructionOverlay div:first').removeClass('hid').addClass('vis').find('.warningBox3').empty().append(loadScript.unwrapErrors(result.errorList));
					}
					else
					{	
						// message row present?
						if ($j('table.jitpInstList tbody tr:first td').length < 2)
						{
							// remove message
							$j('table.jitpInstList tbody tr:first').remove();
						}
						
						
						// create content for new instruction
						var content = 	'<tr>' +
											'<td class="jitpinstruction" id="Instruction-' + result.results.id + '">' + result.results.instruction + '</td>' +
											'<td class="showontpdel" id="showontpdn' + result.results.id + '">' +																	
												'<a href="#" onclick=" updateShowOnTPDelNote(' + result.results.id + ', true); return false; ">' +
													'<img src="img/icons/bullet-tick.png" width="10" height="10" alt="' + i18n.t("core.jobs:jobItem.showOnTPDelNote", "Show On TP Del Note") + '" title="' + i18n.t("core.jobs:jobItem.showOnTPDelNote", "Show On TP Del Note") + '" />' +
												'</a>' +
											'</td>' +
											'<td class="modifiedby">[' + result.results.recordedBy.name + ' ' + cwms.dateFormatFunction(result.results.lastModified) + ']</td>' +
											'<td>' +
												'<a href="#" onclick=" editJITPInstruction(' + result.results.id +','+result.results.instruction+'); return false; " >' +
													'<img width="16" height="16" title="' + i18n.t("core.jobs:jobItem.editInstruction", "Edit Instruction") + '" alt="' + i18n.t("core.jobs:jobItem.editInstruction", "Edit Instruction") + '" src="img/icons/note_edit.png" class="noteimg_padded" />' +
												'</a> ' +
												'&nbsp; ' +
												'<a href="#" onclick=" removeTPInstruction(this, ' + result.results.id + '); return false; " >' +
													'<img width="16" height="16" title="' + i18n.t("core.jobs:jobItem.deleteInstruction", "Delete Instruction") + '" alt="' + i18n.t("core.jobs:jobItem.deleteInstruction", "Delete Instruction") + '" src="img/icons/note_delete.png" class="noteimg_padded" />' +
												'</a>' +
											'</td>' +
										'</tr>';
						// append the new action to the actions table
						$j('table.jitpInstList tbody').prepend(content);
						// close the overlay
						loadScript.closeOverlay();	
						location.reload();
					}
				}
			});
		}
	});
}

/**
 * this method updates the instruction value for an existing instruction
 * 
 * @param {Integer} instructionId id of the instruction to be updated
 * @param {String} instructionValue new instruction content
 */
function editJITPInstruction(instructionId, instructionValue)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/tpinstructionservice.js'), true,
	{
		callback: function()
		{
			// call dwr method to update the third party instruction
			tpinstructionservice.updateTPInstruction(instructionId, instructionValue, 
			{
				callback:function(results)
				{
					// update of instruction successful?
					if(!results.success)
					{						
						// show error message in overlay
						$j('div#jitpInstructionOverlay div:first').removeClass('hid').addClass('vis').find('.warningBox3').empty().append('<div>' + results.message + '</div>');						
					}
					else
					{
						// update the instruction on page
						$j('td#Instruction-' + instructionId).html(results.results.instruction);
						// close the overlay
						loadScript.closeOverlay();
					}
				}
			});
		}
	});
	location.reload();
}

/**
 * this method de-activates a third party instruction and removes functionality to edit
 * 
 * @param {Object} anchor the anchor clicked
 * @param {Integer} instructionId id of the instruction to be de-activated
 */
function removeTPInstruction(anchor, instructionId)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/tpinstructionservice.js'), true,
	{
		callback: function()
		{
			// call dwr method to de-activate third party instruction
			tpinstructionservice.deactivateTPInstruction(instructionId,
			{
				callback:function(results)
				{
					// update of boolean field successful?
					if(!results.success)
					{
						// display failure message
						$j.prompt('Error: ' + results.message);
					}
					else
					{
						// get first cell of table row
						var cell = $j(anchor).parent('td').parent('tr').find('td:first');
						// create new html content
						$j(cell).html('<del>' + cell.html() + '</del>');
						// remove anchors for editing and deleting
						$j(anchor).parent('td').empty();
					}
				}
			});
		}
	});
}

/**
 * this method updates the show instruction on third party delivery note boolean field
 * 
 * @param {Integer} instructionId is the id of the instruction to update
 * @param {Boolean} showOnTPDN is the boolean value currently assigned
 */
function updateShowOnTPDelNote(instructionId, showOnTPDN)
{
	// swap boolean value of showOnTPDN
	showOnTPDN = (showOnTPDN) ? false : true;
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/tpinstructionservice.js'), true,
	{
		callback: function()
		{
			// dwr method to update the show instruction on third party delivery note document field
			tpinstructionservice.updateTPInstructionShowOnTPDN(instructionId, showOnTPDN,
			{
				callback:function(results)
				{
					// update of boolean field successful?
					if(!results.success)
					{
						// display failure message
						$j.prompt(i18n.t("failureMessage", {errMsg : results.message, defaultValue : "Failed: " + results.message}));
					}
					else
					{
						// remove the old anchor and image from the table cell and append new one
						$j('#showontpdn' + instructionId).empty().append('<a href="#" onclick=" updateShowOnTPDelNote(' + instructionId + ', ' + showOnTPDN + '); return false; "></a>');
						// is the instruction to be shown on third party delivery notes?						
						if (showOnTPDN)														
						{
							// add tick image
							$j('#showontpdn' + instructionId + ' a').append('<img src="img/icons/bullet-tick.png" width="10" height="10" alt="' + i18n.t("core.jobs:jobItem.showOnTPDelNote", "Show On TP Del Note") + '" title="' + i18n.t("core.jobs:jobItem.showOnTPDelNote", "Show On TP Del Note") + '" />');
						}
						else
						{
							// add cross image
							$j('#showontpdn' + instructionId + ' a').append('<img src="img/icons/bullet-cross.png" width="10" height="10" alt="' + i18n.t("core.jobs:jobItem.hideOnTPDelNote", "Hide On TP Del Note") + '" title="' + i18n.t("core.jobs:jobItem.hideOnTPDelNote", "Hide On TP Del Note") + '" />');
						}
					}
				}
			});
		}
	});
}


