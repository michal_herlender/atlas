/**
 * Array of autocomplete objects (multiple are possible) containing setup for 
 * jQueryUI initialization.
 * this is used in the init() function.
 * @param procedureSubdivId is initialized per procedure in JSP file 
 */
var jquiAutocomplete = new Array(
	{
		inputFieldName: 'procedureReferenceAndName',
		inputFieldId: 'procedureId',
		source: 'searchprocedures.json?subdivId='+procedureSubdivId+'\&modelId='+modelId+'&type='+type
	});

/**
 * Function called when a subdivision is selected 
 */
function subdivisionSelected(subdivId) {
	refreshProcedureAutocomplete(subdivId);
}

/**
 * Function which updates the subdivId var and refreshes the procedure autocomplete with a new subdivision ID
 * The stored name and ID are alos cleared
 */
function refreshProcedureAutocomplete(newSubdivId) {
	if (procedureSubdivId != newSubdivId) {
		procedureSubdivId = newSubdivId;
		searchAllProcedures();
		$j("#procedureReferenceAndName").autocomplete("close");
		$j("#procedureReferenceAndName").attr("value","");
		$j("#procedureReferenceAndName").autocomplete("search","");
		$j("#procedureId").attr("value","0");
	}
}

function searchAllProcedures() {
	if($j('#searchAllProcs').prop('checked')) {
		$j("#procedureReferenceAndName").autocomplete("option", "source", "searchprocedures.json?subdivId="+procedureSubdivId+'&type='+type);
	}
	else {
		$j("#procedureReferenceAndName").autocomplete("option", "source", "searchprocedures.json?subdivId="+procedureSubdivId+'\&modelId='+modelId+'&type='+type);
	}
}