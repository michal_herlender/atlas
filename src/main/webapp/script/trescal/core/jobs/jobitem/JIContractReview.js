/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	JIContractReview.js
*	DESCRIPTION		:	Page-specific javascript file.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 
*	KNOWN ISSUES	:	-
*	HISTORY			:	12/10/2007 - JV - Created File
*					:	20/10/2015 - TProvost - Manage i18n
*					:	20/10/2015 - TProvost - Minor fix on enable buttons
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array(	'script/trescal/core/tools/multicurrency.js',
								'script/trescal/core/jobs/jobitem/JobItemFunctions.js',
								'script/trescal/core/tools/labelprinting/dymo/DymoLabelFunctions.js',
								'script/thirdparty/jQuery/jquery.boxy.js',
								'script/thirdparty/jQuery/jquery.scrollTo.js');

/**
 * this variable holds an array of id objects, one for the navigation link and
 * one for the content area the link refers to. this array is passed to the
 * Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = 	new Array( { anchor: 'contractreview-link', block: 'contractreview-tab' },
					      	   { anchor: 'PO-link', block: 'PO-tab' },
					      	   { anchor: 'revhistory-link', block: 'revhistory-tab' } );	


/**
 * this method is called from the loadScript.init() function in CwmsMain.js if
 * present. this allows onload coding to be included in the page specific
 * javascript file without adding to the body onload attribute.
 */
function init()
{
	// load stylesheet using the dom if not already
	loadScript.loadStyle('styles/jQuery/jquery-boxy.css', 'screen');
	// select the correct subnavigation link
	$j('#linkcontractrev').addClass('selected');
	// user preference to use job item key navigation
	if (userPrefs.jikeynav)
	{
		// initialise key nav to move between job items
		initialiseKeyNavForJI();
	}
	$j("#bposelectiondialog").dialog({
		dialogClass: "dialog-no-close",
		title: i18n.t("core.jobs:jobItem.assignToBPO", "Assign To BPO"),
		autoOpen: false,
		modal: true,
		resizable: false,
		height: "auto",
		width: 600,
		buttons: [
            {
                text: i18n.t("common:close", "Close"),
                click: function () {
                    $j(this).dialog("close");
                }
            }]
	});
}

/**
 * this method checks all inputs of type checkbox that are contained within the
 * parentDiv parameter
 * 
 * @param {Boolean}
 *            select true if boxes are to be checked and vice versa
 * @param {String}
 *            parentDiv id of div in which to search for inputs of type checkbox
 * @param {Boolean}
 *            similiar indicates if we only want to check similiar items
 */
function selectAllItemsWithSimiliar(select, parentDiv, similiar)
{
	// check all or similiar
	if(select)
	{
		// select similiar items
		if (similiar)
		{
			// uncheck all items that are not marked as similiar and make sure
			// the checkbox we just selected
			// (i.e. select similiar) is not unchecked
			$j('#' + parentDiv + ' input:checkbox').not('[class="similiar"]').not('[id="selectSimiliar"]').check('off');
			// check all items that are marked as similiar
			$j('#' + parentDiv + ' input:checkbox[class="similiar"]').check('on');
		}
		else
		{
			// check all items
			$j('#' + parentDiv + ' input:checkbox').check('on');
			// make sure select similiar checkbox is not checked
			$j('#' + parentDiv + ' input:checkbox[id="selectSimiliar"]').check('off');	
		}
	}
	// uncheck all or similiar
	else
	{
		if (similiar)
		{
			// uncheck all items that are marked as similiar
			$j('#' + parentDiv + ' input:checkbox[class="similiar"]').check('off');
		}
		else
		{
			// uncheck all items
			$j('#' + parentDiv + ' input:checkbox').check('off');	
		}		
	}
}

/**
 * this method shows or hides bulk update form elements
 * 
 * @param {Boolean}
 *            state true or false (show or hide)
 */
function updateBulk(state)
{
	if(state == true)
	{
		// show the bulk update form elements
		$j('#copyoptions').removeClass().addClass('vis');
	}
	else
	{
		// hide the bulk update form elements
		$j('#copyoptions').removeClass().addClass('hid');
	}	
}

/**
 * delete's the onbehalf of entity for the given jobitem
 * 
 * @param {Integer}
 *            jobitemid the id of the item to reset
 * @param {Integer}
 *            behalfitemid the behalf of item to delete
 */
function removeItemOnBehalf(jobitemid, onBehalfItemId)
{
	$j.ajax({
		url: "deleteOnBehalfItem.json",
		data: {
			onBehalfItemId: onBehalfItemId
		},
		async: true
	}).done(function() {
		// remove the span containing on-behalf of company link
		$j('span#onbehalf').remove();
		// prepend message in it's place
		$j('#behalfdiv').prepend('<span>' + i18n.t("core.jobs:jobItem.removeItemOnBehalf", "This item is not recorded as being on-behalf of another company") + '</span>');
	}).fail(function(error) {
		alert("Sorry, an error occurs while deleting onbehalf item.");
	});
}

function toggleBypassWarning(checkbox)
{
	if($j(checkbox).is(':checked'))
	{
		$j('div#bypass-warning').removeClass('hid').addClass('vis');
	}
	else
	{
		$j('div#bypass-warning').removeClass('vis').addClass('hid');
	}
}

function toggleReserveCertificate(checkbox)
{
	if($j(checkbox).is(':checked'))
	{
		$j('div#reserve-certificate').removeClass('hid').addClass('vis');
		$j('li#certificate-date').removeClass('hid').addClass('vis');
	}
	else
	{
		$j('div#reserve-certificate').removeClass('vis').addClass('hid');
		$j('li#certificate-date').removeClass('vis').addClass('hid');
	}
}

function toggleComment(value,oldValueId,commentId){
	var oldValue = document.getElementById(oldValueId)?.value;
	if(value != oldValue)
		{
			document.getElementById(commentId)?.classList?.remove('hid');
		}
	else
	{
		document.getElementById(commentId)?.classList?.add('hid');
	}
}

function turnChange(turn)
{
	var oldTurn = $j('#oldTurn').val();
	if(oldTurn != turn)
		{
			$j("#dueDateComments").removeClass('hid');
		}
	else
	{
		$j("#dueDateComments").addClass('hid');
	}
}


function refreshContracts(jobItemId, calTypeId) {

	var response = $j.ajax({
		method: "GET",
		url: "contract/getsuitablecontracts",
		dataType: "json",
		data: {
			jobitemid: jobItemId,
			caltypeid: calTypeId
		}
	});

	response.done(function(contracts){
		var listItem = $j('<li></li>',{ id: 'contractchoice'});
		var label = $j('<label></label>').text(i18n.t("core.contract:contract", "Contract"));
		if ( contracts === undefined || contracts.length < 1 )  {
			var span = $j('<span></span>').text(i18n.t("core.contract:nocontract", "No suitable contract found"))
		}
		else {
			var select = $j('<select id="contractSelect" onchange="changeContract('+jobItemId+');"></select>').attr({name: 'contractId'});
			$j('<option></option>').attr({value: 0})
				.text('N/A')
				.appendTo(select);
			$j.each(contracts, function(i) {
				$j('<option></option>').attr({value: contracts[i].contractId})
					.text(contracts[i].contractDescription)
					.appendTo(select);
			});
			var span = $j('<span></span>').append(select);
		}
		listItem.append([label, span]);
		$j('li#contractchoice').replaceWith(listItem);
		// add instructions component
		listItem.append('<!-- displaying selected contract instructions --><cwms-instructions show-mode="RELATED" link-contractid="0"></cwms-instructions>');
		changeContract(jobItemId);
	});

	response.fail(function (jqXHR, textStatus) {
		alert("Request failed: " + textStatus);
	});

}

function changeContract(jobItemId) {
	var contractId = $j('#contractSelect').val();
	var contractNo = $j('#contractSelect option:selected').text();
	var calTypeId = $j('#calTypeSelect').val();
	fetch(cwms.urlWithParamsGenerator("contract/getcontractinformation",
		{jobItemId,contractId,calTypeId}
	),{
		method:"PUT",
		headers:cwms.metaDataObtainer.csrfHeader,
		credentials:"same-origin"
	}).then(res => res.ok?res.json():Promise.reject(res.statusText))
	.then(result =>  {
		if(result.turnaround) $j('#jiturn').val(result.turnaround);
		$j('#demands0').prop('checked', result.cleaning);
		$j('#demands1').prop('checked', result.sealing);
		$j('#demands2').prop('checked', result.engraving);
		$j('#contractnotocopy').html(' (' + contractNo + ')');
		var price = result.price;
		if(price) {
			const elementLabel = `${result.quotationNumber} ver ${result.quotationVersion}`;
			const table = document.querySelector("cwms-contractreview-cost-table");
			table?.updateCost?.({...price,
				costSource:"QUOTATION",
				elementId:result.quotationId,elementLabel,active:true});
		}
		if(contractId!=0){
			var content='';
			if(result.warrantyCalibration!=null){
				content='<span>'+result.warrantyCalibration+' '+i18n.t("core.contract:calibrationWarranty", "Calibration Days")+'  ('
					+i18n.t("core.contract:contract", "Contract") +')</span>';
			}
			if(result.warrantyRepair!=null){
				content+='<span>'+result.warrantyRepair+' '+i18n.t("core.contract:repairWarranty", "Repair Days")+'  ('
				+i18n.t("core.contract:contract", "Contract") +')</span>';
			}
			$j('#ContractSelectedNotSelected').addClass('hid');
			$j('#ContractSelected').removeClass('hid');
			$j('#ContractSelected').html(content);
		}
		else if(contractId==0){
			$j('#ContractSelected').addClass('hid');
			$j('#ContractSelectedNotSelected').removeClass('hid');
		}
		
	});
	
	// update link-contractid instruction web component attribute value
	const element = document.querySelector("li[id='contractchoice'] cwms-instructions");
	element.linkContractid = contractId;
	element.refresh();
}