var pageImports = new Array (	'script/trescal/core/jobs/jobitem/JobItemFunctions.js',
								'script/trescal/core/tools/labelprinting/dymo/DymoLabelFunctions.js',
								'script/thirdparty/jQuery/jquery.scrollTo.js');


function init()
{
	// load stylesheet using the dom if not already
	loadScript.loadStyle('styles/jQuery/jquery-boxy.css', 'screen');
	// select the correct subnavigation link (general service operation)
	$j('#linkgeneralserviceoperation').addClass('selected');
	// user preference to use job item key navigation
	if (userPrefs.jikeynav) {
		// initialise key nav to move between job items
		initialiseKeyNavForJI();
	}
}
