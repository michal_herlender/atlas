/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	NewJobItemSearch.js
*	DESCRIPTION		:	This page specific javascript file.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	09/04/2008 - SH - Created File
*					: 	19/10/2015 - TProvost - Manage i18n
*					:	21/10/2015 - TProvost - Fix bug in new version of Impromptu.js - Replace the "callback" property (property disappeared) by the "submit" property
*					:							=> Changes made in function confirmContractReview
****************************************************************************************************************************************/

/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'focusModel';



/**
 * shows a warning if the user is leaving a page which has items
 * in a basket. Only works in IE and Moz 1.7 +.
 */
var showUnLoadWarning = true;

window.onbeforeunload = function()
{
	if(parseInt($j('span.basketCount:first').text()) > 0 && showUnLoadWarning)
	{
		showUnLoadWarning = true;
		return i18n.t("warnLoseCurrentBasket", "If you leave this page you will lose the contents of your current basket.");	
	}
};




function addItemsFromPrebooking(type,jobid)
{
	var cancel = i18n.t("core.jobs:jobItem.cancel", "Cancel");
	var additems=i18n.t("core.jobs:jobItem.additems", "Add Items");
	
	$j.ajax({
	    url:'additemsfromprebooking.htm',
	    type:'GET',
	    data: {
			jobid: jobid
		},
		async: true,
	    }).done(function(content) {
	       	//display the popup
	       	$j.prompt(content,{
	       		title: i18n.t('core.jobs:jobItem.additemsfromPrebooking', 'Add Items From Prebooking'),
	       		buttons: { [additems]: true, [cancel]: false },
	       		submit: function (e, val, m, f) {                            
	                   if (val == true){
	                	 //if no prebooking is selected show a message
	                   	 if (!$j('input[name="asnId"]').is(':checked')) {
	                            alert(i18n.t('core.jobs:jobItem.selectPrebooking', 'please select a prebooking!!'));
	                        }
	                   	 else{
	                   		document.forms['additemtojob'].submit();
	                   	 }
	                   }	
	                   else
	                       console.log('Cancel!');
	               }
	       	});
	       	
	        }
	);
	
}



