/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	ViewCollectedInstruments.js
*	DESCRIPTION		:	Page-specific javascript file.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 
*						
*	KNOWN ISSUES	:	Doesn't work - could refactor to use ids of company.. but we may want to overhaul feature completely if reactivated. - GB
*	HISTORY			:	10/03/2008 - JV - Created File
*
****************************************************************************************************************************************/

function newJob(button, coid)
{
	$j('input:button').hide();

	$j(button).parent().find('li').each(
		function (i, n)
		{
			var id = $j(n).attr('id');
			console.log('Prepending'+id);
			$j(n).prepend('<input type="checkbox" value="' + id + '" name="selectedInsts" />');
		}
	);
	
	$j(button).parent().find('.cancel').show();
	
	$j('#coid').val(coid);
}

function cancel(button)
{
	$j(button).parent().find('input:checkbox').remove();
	$j(button).hide();
	$j('input:button.newjob').show();
}
