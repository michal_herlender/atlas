/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	JICertificates.js
*	DESCRIPTION		:	Page-specific javascript file.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 
*						
*	KNOWN ISSUES	:	-
*	HISTORY			:	31/10/2007 - JV - Created File
*					:	16/10/2015 - TProvost - Manage i18n
*						16/10/2015 - TProvost : Minor fix on enable button
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports =  new Array(	'script/trescal/core/jobs/jobitem/JobItemFunctions.js',
								'script/thirdparty/jQuery/jquery.scrollTo.js',
								'script/trescal/core/tools/labelprinting/dymo/DymoLabelFunctions.js',
								'script/trescal/core/utilities/PreviewCertificate.js');
								
/**
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = 	new Array( { anchor: 'existingcerts-link', block: 'existingcerts-tab' },
					      	   { anchor: 'newcert-link', block: 'newcert-tab' } );	
								
/**
 * array of calendar objects containing the initial setup literals needed
 * 
 * @param {String} inputField is the id of the input field to which the calendar should be binded
 * @param {String} dateFormat is the format that the date should be displayed
 * @param {String} displayDates 'all' make all dates available
 * 								'future' show only dates in the future
 * 								'past' show only dates in the past
 * applied in init() function
 */
var jqCalendar = new Array(	 { 
							 	inputField: 'calDate',
							 	dateFormat: 'dd.mm.yy',
							 	displayDates: 'all',
							 	showWeekends: true
							 },
							 { 
							 	inputField: 'certDate',
							 	dateFormat: 'dd.mm.yy',
							 	displayDates: 'all',
							 	showWeekends: true
							 });

/**
 * create array of descriptions to be used for password strength function
 */
var desc = new Array();
desc[0] = "Very Weak";
desc[1] = "Weak";
desc[2] = "Better";
desc[3] = "Medium";
desc[4] = "Strong";
desc[5] = "Strongest";

/**
 * this method is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{
	// select the correct subnavigation link
	$j('#linkcertificates').addClass('selected');
	// user preference to use job item key navigation
	if (userPrefs.jikeynav)
	{
		// initialise key nav to move between job items
		initialiseKeyNavForJI();
	}
	// i18n of descriptions for password strength function
	desc[0] = i18n.t("common:password.veryWeak", "Very Weak");
	desc[1] = i18n.t("common:password.weak", "Weak");
	desc[2] = i18n.t("common:password.better", "Better");
	desc[3] = i18n.t("common:password.medium", "Medium");
	desc[4] = i18n.t("common:password.strong", "Strong");
	desc[5] = i18n.t("common:password.strongest", "Strongest");
}

/**
 * this method calls Alligator to print a calibration label.
 * 
 * @param {Object} anchor anchor that has been clicked
 * @param {Integer} certid id of the certificate
 */
function printAlligatorLabel(labelType, certificateNumber)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", "https://localhost:58159/service/label?type="+labelType+"&certificate="+certificateNumber, false);
    console.log("GET to http://localhost:58058/service/label");
    xmlHttp.send();
}


/**
 * this method searches for a job by using the exact job no and displays all job items
 * it contains.
 * 
 * @param {Integer} originalJobItemId id of the original job item
 * @param {String} jobno number of the current job
 */
function searchJob(originalJobItemId, jobno)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/jobservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to get job by exact job no
			jobservice.getJobByExactJobNo(jobno, 
			{
				callback:function(job)
				{
					// remove any old job items from list
					$j('div#jItems').empty();
								
					// job is not null
					if(job != null)
					{
						// job has job items
						if(job.items.length > 0)
						{
							// variable to hold new job items
							var content = '';
							// loop through all job items
							$j.each(job.items, function(i){
								// add radio input field for job item
								content = content +	'<input type="radio" name="jobItemId" value="' + job.items[i].jobItemId + '" ';
								// is this original job item?
								if (job.items[i].jobItemId == originalJobItemId)
								{
									// check the radio button
									content = content + 'checked="checked" ';
								}
								// continue adding content
								content = content + '/>&nbsp;&nbsp;' + job.items[i].itemNo + ' - ';
								// this job item instrument has ranges?
								if(job.items[i].inst.ranges)
								{	
									// set range count variable
									var rangeCount = 0;
									// loop through instrument ranges
									$j.each(job.items[i].inst.ranges, function(x){
										// more than one range?
										if(rangeCount > 0)
										{
											// add range content
											content = content + '/ ' + job.items[i].inst.ranges[x].start.toFixed(1);
											if(job.items[i].inst.ranges[x].end)
											{
												content = content + ' - ' + job.items[i].inst.ranges[x].end.toFixed(1);
											}
											content = content + job.items[i].inst.ranges[x].uom.formattedSymbol + ' ';
										}
										else
										{
											// add range content
											content = content + job.items[i].inst.ranges[x].start.toFixed(1);
											if(job.items[i].inst.ranges[x].end)
											{
												content = content + ' - ' + job.items[i].inst.ranges[x].end.toFixed(1);
											}
											content = content + job.items[i].inst.ranges[x].uom.formattedSymbol + ' ';
										}
										// increase range count by one
										rangeCount = rangeCount + 1;
									});
								}
								// manufacturer required for model?
								if (job.items[i].inst.model.modelMfrType.mfrRequiredForModel == true)
								{
									// add specific manufacturer
									content = content + job.items[i].inst.model.mfr.name + ' ';													
								}
								else
								{
									// generic manufacturer present?
									if (job.items[i].inst.mfr)
									{
										// add generic manufacturer
										content = content + job.items[i].inst.mfr.name + ' ';
									}
								}
								// complete job item content
								content = content + job.items[i].inst.model.model + ' ' + 
													job.items[i].inst.model.description.description + '<br />';
																	
							});
							// append all job items to div
							$j('div#jItems').append(content);	
							// any job items checked?
							if($j("input[@name='jobItemId']:checked").length == 0)
							{
								// check first item in list
								$j("input[@name='jobItemId']:first").attr('checked','checked');
							}
							// show the submit button
							$j(':submit').parent().show();
						}
						else
						{	
							// show message for no items on job
							$j('div#jItems').append('<span>' + i18n.t("core.jobs:jobItem.noItemsOnJobNo", {varJobNo : jobno, defaultValue : "There are no items on job no: " + jobno}) + '</span>');
							// apply focus to the job number input field
							$j('#jobNo').focus();
							// hide the submit button
							$j(':submit').parent().hide();
						}
					}
					else
					{
						// show message for no matching job
						$j('div#jItems').append('<span>' + i18n.t("core.jobs:jobItem.noJobWithJobNo", {varJobNo : jobno, defaultValue : "There is no job with job no: " + jobno}) + '</span>');
						// apply focus to the job number input field
						$j('#jobNo').focus();
						// hide the submit button
						$j(':submit').parent().hide();
					}
				}
			});
		}
	});
}

/**
 * this method checks all inputs of type checkbox
 * 
 * @param {Boolean} select true if boxes are to be checked and vice versa
 */
function selectAllItemBoxes(selected)
{
	var check = (selected == true) ? 'checked' : '';
	
	$j('input:checkbox[name="otherJobItems"]').attr('checked', check);
}

/**
 * this method unlocks the selected procedure
 * 
 * @param {String} xindiceKey xindice key of procedure
 * @param {Object} jobitemid id of the current job item
 */
function unlockProc(xindiceKey, jobitemid)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/calibrationservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to unlock procedure
			calibrationservice.unlockCalAndProcedure(xindiceKey,
			{
				callback: function()
				{
					// redirect to job item calibration screen
					window.location.href = "jicalibration.htm?jobitemid=" + jobitemid;
				}
			});
		}
	});
}

function createCORSRequest(method, url) {
	var xhr = new XMLHttpRequest();
	if ("withCredentials" in xhr) {
		// XHR for Chrome/Firefox/Opera/Safari.
		xhr.open(method, url, true);
	}
	else if (typeof XDomainRequest != "undefined") {
		// XDomainRequest for IE.
		xhr = new XDomainRequest();
		xhr.open(method, url);
	}
	else {
		// CORS not supported.
		xhr = null;
	}
	return xhr;
}

/**
 * download a calibration specific label.
 * 
 * @param {Object} anchor anchor that has been clicked
 * @param {Integer} certid id of the certificate
 */
function downloadCalSpecificLabel(anchor, certLinkId)
{
	var includeRecallDate = $j(anchor).next('input:checkbox').is(':checked');
	document.location.replace("birtcalspeclabel?certLinkId="+certLinkId+"&includeRecallDate=" + includeRecallDate);
}

/**
 * this method calls a dwr service to print a small certificate label.
 * 
 * @param {Object} anchor anchor that has been clicked
 * @param {String} template name of template to use
 * @param {Integer} certlinkid id of the cert link
 * Note - certid is not used for single job item printing
 */
function downloadCertificateLabel(anchor, template, certlinkid, certid)
{
	var includeRecallDate = $j(anchor).next('input:checkbox').is(':checked');
	template = template.substring(5);
	document.location.replace("birtcallabel?certLinkId="+certlinkid+"&templateKey="+template+"&includeRecallDate=" + includeRecallDate);
}

/**
 * this method approves a certificate
 * 
 * @param {Integer} certid id of the certificate to be approved
 * @param {Integer} procid just in case procedure training record created
 * @param {String} procref just in case procedure training record created
 * @param {String} password user password to approve certificate
 * @param {Boolean} isProcTrainingCert is this an example of procedure training certificate
 * @param {Object} link button pressed to approve certificate
 */
function approveCert(certid, procid, procref, password, isProcTrainingCert, link)
{	
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/certificateservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to approve certificate
			certificateservice.approveCertificate(certid, password, isProcTrainingCert,
			{
				callback:function(result)
				{
					// enable link as the thickbox has been removed
					$j(link).attr('disabled', false);
					// close overlay
					loadScript.closeOverlay();
					// approval successful?
					if (result.success != true) 
					{
						// alert error message
						$j.prompt(result.message);
					}
					else
					{
						// change status text
						$j('tr#certificate' + certid + ' td:last').text(result.results.status.name);
						// remove list item
						$j(link).parent('span').parent('li').remove();
						// procedure training record?
						if (isProcTrainingCert)
						{
							// change status text
							$j('tr#certificate' + certid + ' td:last').html($j('tr#certificate' + certid + ' td:last').html() + ' (' + i18n.t("core.jobs:jobItem.ptrFor", "PTR for") + ' <a href="viewcapability.htm?capabilityId=' + procid + '&loadtab=proctraining-tab" class="mainlink" target="_blank">' + procref + '</a>)');
						}
					}
				}	
			});
		}
	});
}


/**
 * this function checks the strength of the password entered by the user and displays 
 * text and coloured bar dependant on that strength
 * 
 * @param {String} password password entered by the user in input field
 * @param {Object} li
 */
function passwordStrength(password, li)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/calibrationservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to check the password strength
			calibrationservice.checkPasswordStrength(password,
			{
				callback:function(score)
				{			
					// change text in div dependant on password strength
					$j(li).next().next().find('div:first > div:first').text(desc[score]);
					// change class of coloured div dependant on password strength
					$j(li).next().next().find('div:first> div:first').next().removeClass().addClass("strength" + score);
		        }	
			});
		}
	});
}

function unIssueCertificate(certid)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/certificateservice.js'), true,
	{
		callback: function()
		{
			if(confirm(i18n.t("core.jobs:jobItem.confirmDeleteGroupCert", "Are you sure you wish to delete this certificate? If it is a group certificate, it will be deleted from ALL items.")))
			{
				certificateservice.unIssueCertificate(certid, 
				{
					callback:function(resWrap)
					{
						if(resWrap.success == true)
						{
							// reload page to see changes
							window.location.reload(true);
						}
						else
						{
							$j.prompt(resWrap.message);
						}
					}
				});
			}
		}
	});
}

/**
 * this function sources the content for certificate options from the page and then displays in an overlay.
 * 
 * @param {Integer} divId id of the element to retrieve content from 
 */
function displayCertOptions(divId)
{
	// variable to hold add/remove po item content
	var content = '<div class="overflow" id="visibleBox">' + $j('div#' + divId).html() + '</div>';
	// create new overlay using content
	loadScript.createOverlay('dynamic', i18n.t("core.jobs:jobItem.certOptions", "Certificate Options"), null, content, null, 80, 800, null);
}

/**
 * this function sources the content for certificate linking from the page and then displays in an overlay.
 * 
 * @param {Integer} divId id of the element to retrieve content from 
 */
function displayCertLinking(divId)
{
	// variable to hold add/remove po item content
	var content = '<div id="certLinkingOverlay">' + $j('div#' + divId).html() + '</div>';
	// create new overlay using content
	loadScript.createOverlay('dynamic', i18n.t("core.jobs:jobItem.deleteOrLinkCert", "Delete Or Link Certificate"), null, content, null, 80, 880, null);
}

/**
 * this method links a certificate to an existing calibration
 * 
 * @param {Integer} certid the id of the certificate to be linked to the calibration
 * @param {Integer} calid the id of the calibration to be linked to the certificate
 */
function linkCertificateToCalibration(certid, calid)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/certificateservice.js'), true,
	{
		callback: function()
		{			
			// call dwr service to link a certificate to a calibration
			certificateservice.linkCertToCal(certid, calid,
			{
				callback:function(result)
				{
					if (result.success)
					{
						// get calibration process from results
						var process = result.results;
						// first hide the linking link
						$j('div#linkcert' + certid).parent().removeClass().addClass('hid');
						// show the cert options link
						$j('div#cert' + certid).parent().removeClass('hid').addClass('inline');
						// update the calibration process in table
						$j('table#jicerttable tr#certificate' + certid + ' td.jic_process').text(process);
						// close the overlay
						loadScript.closeOverlay();
					}
					else
					{
						// update overlay with error messages
						loadScript.modifyOverlay(null, result.message, null, true, null, null, null);
					}
				}
			});
		}
	});
}

/**
 * this method creates the content of the overlay used when adding notes to the certificate
 * 
 * @param {Integer} certid id of the certificate adding/editing note for
 * @param {String} certno number of the certificate adding/editing
 */
function addCertNote(certid, certno)
{
	// create content to be displayed in overlay
	var content = 	'<div id="certNoteOverlay">' +
						'<fieldset>' +							
							'<ol>' +
								'<li>' +
									'<label>' + i18n.t("noteLabel", "Note:") +'</label>' +
									'<span>' +
										'<textarea id="newNoteText" name="newNoteText" class="width70" rows="6" value=""></textarea>' +
									'</span>' +
								'</li>' +
								'<li>' +
									'<label>&nbsp;</label>' +
									'<input type="button" value="' + i18n.t("submit", "Submit") + '" onclick=" addNote(' + certid + ', \'' + certno + '\', $j(\'textarea#newNoteText\').val()); " />' +										
								'</li>' +
							'</ol>' +
						'</fieldset>' +
					'</div>';
	// create new overlay using content
	loadScript.createOverlay('dynamic', i18n.t("core.jobs:jobItem.addingNoteForCert", {varCertNo : certno, defaultValue : "Adding note for certificate " + certno}), null, content, null, 40, 620, 'newNoteText');
}

/**
 * this method adds a note for the certificate
 * 
 * @param {Integer} certid id of the certificate adding note for
 * @param {String} certno number of the certificate adding
 * @param {String} notetext string of new note
 */
function addNote(certid, certno, notetext)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/certnoteservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to add note text to certificate
			certnoteservice.insertCertNoteDWR(notetext, certid, 
			{
				callback:function(result)
				{
					// result is unsuccessful?
					if(!result.success)
					{
						// create variable to hold errors
						var errorString = loadScript.unwrapErrors(result.errorList);
						// add error list to warning box in thickbox
						$j('div#certNoteOverlay fieldset').before(	'<div class="warningBox1">' +
																		'<div class="warningBox2">' +
																			'<div class="warningBox3">' +
																				errorString +
																			'</div>' +
																		'</div>' +
																	'</div>');
						// apply focus to the note text field
						$j('textarea#newNoteText').focus();
					}
					else
					{	
						// unwrap results
						var note = result.results;
						// create content 
						var content = 	`<li>
											<div class="inlinenotes_note">  ${note.note} <br />
											( ${note.setBy.name}  - ${cwms.dateFormatFunction(note.setOnDate)}  )</div>
											<div class="inlinenotes_edit">
												&nbsp;
											</div>
											<div class="clear"></div>
										</li>`;
						// no notes for certificate on page?
						if ($j('div#certnote' + certid + ' li.noCertNotes').length > 0)
						{
							// remove message and append new row
							$j('div#certnote' + certid + ' li.noCertNotes').before(content).remove();
						}
						else
						{
							// append new li
							$j('div#certnote' + certid + ' fieldset ol').prepend(content);
						}
						// update note size
						$j('span.certNoteSize' + certid).text(parseInt($j('span.certNoteSize' + certid).text()) + 1);
						// close the overlay				
						loadScript.closeOverlay();
					}
				}
			});
		}
	});
}

/**
 * this method marks a certificate as a procedure training record
 * 
 * @param {Integer} certid id of the certificate to be marked
 * @param {Integer} procid id of the procedure related to
 * @param {String} procref reference number for procedure
 * @param {Object} anchor the link clicked to initiate this method
 */
function markAsTrainingCertificate(certid, procid, procref, anchor)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/proceduretrainingrecordservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to mark certificate as procedure training record
			proceduretrainingrecordservice.insertProcedureTrainingRecord(certid, procid, allocatedSubdivId,
			{
				callback:function(result)
				{
					// result is unsuccessful?
					if(result.success)
					{
						// create content for page
						var content = '<img src="img/icons/success.png" width="16" height="16" class="img_marg_bot" title="' + i18n.t("core.jobs:jobItem.certMarkedAsPTR", "Certificate marked as procedure training record") + '"/> <a href="viewcapability.htm?capabilityId=' + procid + '&loadtab=proctraining-tab" class="mainlink" target="_blank">' + i18n.t("core.jobs:jobItem.viewPTRForProc", {varProcRef : procref, defaultValue : "View Procedure Training Records For " + procref}) + '</a>';
						// remove anchor and display tick
						$j(anchor).after(content).remove();
						// change status text
						$j('tr#certificate' + certid + ' td:last').html($j('tr#certificate' + certid + ' td:last').html() + ' (' + i18n.t("core.jobs:jobItem.ptrFor", "PTR for") + ' <a href="viewcapability.htm?capabilityId=' + procid + '&loadtab=proctraining-tab" class="mainlink" target="_blank">' + procref + '</a>)');
					}
					else
					{
						// show error message to user
						loadScript.modifyOverlay(null, result.message, null, true, null, null, null);
					}
				}
			});
		}
	});
}

/**
 * this method marks a certificate as previewed by the engineer
 * 
 * @param {Object} anchor the link clicked to mark as previewed
 * @param {Integer} certId the id of the certificate to be marked as previewed
 */
function markCertificateAsPreviewed(anchor, certId)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/certificateservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to mark certificate as previewed
			certificateservice.markCertificateAsPreviewed(certId,
			{
				callback:function(result)
				{
					// result is successful?
					if(result.success)
					{
						// remove anchor and display tick
						$j(anchor).removeClass('cross').addClass('tick');
						// find messages next to sign and approve links
						$j('span.previewCertificateStatus').html('<img src="img/icons/greentick.png" width="16" height="16" class="marg-left img_marg_bot" title="' + i18n.t("core.jobs:jobItem.certPreviewedByEngineer", "Certificate previewed by engineer") + '" /> ' + i18n.t("core.jobs:jobItem.certPreviewedByEngineer", "Certificate previewed by engineer"));
					}
					else
					{
						// show error message to user
						$j.prompt(result.message);
					}
				}
			});
		}
	});
}

function certificateReplacement(jobitemId, certificateId, certType){
	var yes = i18n.t("yes", "Yes");
	var no = i18n.t("no", "No");
	
	var promptStates = {
		state0: {
			html:strings['certificatereplacement.confirmation'],
			buttons: { [no]: false, [yes]: true },
			submit:function(e,v,m,f){
				console.log(f);
				if(v){
					ajaxCertificateReplacement(jobitemId, certificateId, certType);
	            }
			}
		}
	};
	
	$j.prompt(promptStates);

	return false;
}

function ajaxCertificateReplacement(jobitemId, certificateId, certType) {
	$j.ajax({
		url: "certificate/replacement",
		method: "POST",
		data: {
			jobitemid: jobitemId,
			certificateid: certificateId,
			certType: certType
			},
		async: true
	}).done(function(result) {
		if(!result.success){
			//alert(result.message);
			if(result.message == 'Multiple WR')
			{
				var jiWrIds = result.results;
				//alert(jiWrIds);
				var titleAction = strings['certificatereplacement.selectwr.title'];
				var contentUri =  'selectworkrequirement.htm?jiwrids='+jiWrIds+'&certificateid='+certificateId+'&jobitemid='+jobitemId;
				
				// create new overlay
				loadScript.createOverlay('external', titleAction, contentUri, null, null, 80, 820, null);
			}
			else if(result.message == 'WR Inconsistent')
			{
				alert("The former capability is incorrect");
				
			} else {
				var ok = i18n.t("ok", "Ok");
				
				var promptStates = {
					state0: {
						html:result.message,
						buttons: { [ok]: true },
						submit:function(e,v,m,f){
							console.log(f);
						}
					}
				};
				
				$j.prompt(promptStates);
			}
		}
		else{
			
				document.location.reload(true);
		}
	}).fail(function(result) {
		alert("Error on creating accessory overlay");
	});
}

function supplementaryForChange(selectValue) {
	if(selectValue == '')
		$j("#supplementaryForCommentInput").removeClass('vis').addClass('hid');
	else
		$j("#supplementaryForCommentInput").removeClass('hid').addClass('vis');
}