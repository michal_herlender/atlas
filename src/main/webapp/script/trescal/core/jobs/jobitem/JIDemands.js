var pageImports = new Array (	'script/trescal/core/jobs/jobitem/JobItemFunctions.js',
								'script/trescal/core/tools/labelprinting/dymo/DymoLabelFunctions.js',
								'script/thirdparty/jQuery/jquery.scrollTo.js');

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if
 * present. this allows onload coding to be included in the page specific
 * javascript file without adding to the body onload attribute.
 */
function init() {
	// select the correct subnavigation link
	$j('#linkdemands').addClass('selected');
	// user preference to use job item key navigation
	if (userPrefs.jikeynav) {
		// initialise key nav to move between job items
		initialiseKeyNavForJI();
	}
}