
(function($j) {
  $j.templates(
    "duplicatesWarningContentTemplate",
    "#duplicatesWarningContentTemplate"
  );
  $j.templates(
    "addExistingInstrumentTemplate",
    "#addExistingInstrumentTemplate"
  );
  $j.templates("newJobInstTemplate", "#newJobInstTemplate");
  $j.fn.AddNewItemsToJob = function(options) {
    return this.each(function() {
      new AddNewItemsToJob(this, options);
    });
  };

  class AddNewItemsToJob {
    constructor(element, options) {
      this.element = element;
      this.options = options;
      this.basketIds = this.getBasketIds();

      $j("input[name^='instSerialNos'], input[name^='instPlantNos']", element).bind("input change",
      this.debounceAndThrottle(e => {
        this.checkForDuplicates(element);
      }));
      $j("input[name^='instPlantNos']",element).bind("input change",e => this.checkForAllSamePlants());
      $j(this.element).on(
        "click",
        "button.acceptInstrument",
        this.acceptRow.bind(this)
      );
    }

    debounceAndThrottle(callback){
       return debounce(callback,500);
    }

    acceptRow(e) {
      e.preventDefault();
      const element = e.currentTarget;
      const ids = element.dataset;
      const basketItem = this.copyBasketItemProperties(this.element, ids);
      this.appendItemToBasket(basketItem);
      this.removeErrorMessage(this.element);
      this.hideMe();
      $j("span.newModCount").text(
        parseInt($j("span.newModCount:first").text()) - 1
      );
      return false;
    }

    hideMe() {
      this.removeSuggestions();
      this.element.remove();
    }

    removeSuggestions() {
      $j(".cwms-instrument-plugin", $j(this.element)).remove();
    }

    copyBasketItemProperties(element, ids) {
      const type = "instrument";
      const input = element.querySelector("[name^='basketParts']");
      const inputName = input.name;
      const part = input.value;
      const existingGroup = element.querySelector(
        "[name^='basketExistingGroupIds']"
      ).value;
      const tempGroup = element.querySelector("[name^='basketTempGroupIds']")
        .value;
      const baseItemId = element.querySelector("[name^='basketBaseItemIds']")
        .value;
      const procId = element.querySelector("[name^='itemProcIds']").value;
      // get the procedure reference using the procId
      this.getProcReference(procId, baseItemId);
      
      
      const quotationItemId = element.querySelector(
        "[name^='basketQuotationItemIds']"
      ).value;
      const modelId = element.querySelector("[name^='basketIds']");
      const modelName = input.parentElement.querySelector("a").innerText;

      const match = /^.*\[(\d+)\]/.exec(inputName);
      const index = match ? match[1] : 0;
      return new BasketItemProperties(
        ids.plantid,
        ids.plantNumber,
        ids.serialNumber,
        part,
        existingGroup,
        tempGroup,
        type,
        baseItemId,
        procId,
        quotationItemId,
        index,
        modelId,
        ids.description || modelName
      );
    }

    appendItemToBasket(item) {
      const w = $j($j.render.addExistingInstrumentTemplate({ item }));
      const newJobInstBody = document.querySelector(".newjobinstsbody")
        ? $j(".newjobinstsbody")
        : this.createNewJobInstBody();
      newJobInstBody.append(w);
      JT_init();
      this.basketIds = this.getBasketIds();
      const count = $j("span.existInstCount");
      count.text(parseInt(count.text()) + 1);
    }

    createNewJobInstBody() {
      const form = $j("#addnewitemstojobform");
      const newJobModelsTable = $j("#newjobmodels", form);
      const w = $j($j.render.newJobInstTemplate());
      newJobModelsTable.after(w);
      return $j(".newjobinstsbody", form);
    }

    checkForDuplicates(element) {
      const instrument = this.collectInstrumentProperty(element);
      this.queryServerForDuplicatedInstrument(instrument);
    }

    queryServerForDuplicatedInstrument(instrument) {
      const contextPath = document
      .querySelector("meta[name='_contextPath']")?.getAttribute("content") ?? "" ;
      const base = `${location.origin}${contextPath ?? ""}/`
      const url = new URL("instrument/checkDuplicatesForInstrument.json",base);
      Object.entries(instrument).map( ([k,v]) => {
        url.searchParams.append(k,v);
      });
      fetch(url, {
        method: "GET",
        headers: {
        		"Content-Type": "application/json", 
        		},
      })
        .then(r => r.json())
        .then(r =>
          r.isValid ? r.result : Promise.reject("no instruments was found")
        )
        .then(this.checkForPerfectMatch(instrument))
        .then(r => {
          const companyInstruments = r.companyInstruments.map(i =>
            Object.assign(i, { isOnJob: this.isInBasket(i) })
          );
          const residualGroupInstruments = r.residualGroupInstruments.map(i =>
            Object.assign(i, { isOnJob: this.isInBasket(i) })
          );
          return Object.assign(r, {
            companyInstruments,
            residualGroupInstruments
          });
        })
        .then(r => {
          $j(".cwms-instrument-plugin", $j(this.element).parent()).remove();
          this.element.classList.add("warningBox2");
          const w = $j(
            $j.render.duplicatesWarningContentTemplate({ i18n, result: r })
          );
          w.addClass("cwms-instrument-plugin");
          $j(this.element).append(w);
          JT_init();
        })
        .catch(() => {
          this.element.classList.remove("warningBox2");
          this.removeSuggestions();
        });
    }

    checkForPerfectMatch(instrument) {
      return result => {
        const perfect = result.companyInstruments.find(
          i =>
            i.coid == instrument.coid &&
            i.serialNo == instrument.serialNo &&
            i.plantNo == instrument.plantNo &&
            i.modelId == instrument.modelId
        );
        if (perfect) {
          const message = i18n.t(
            "core.jobs:jobItem.instrumentExistsError",
            "Instrument already exists!!!"
          );
          this.addErrorMessageToElement(message, this.element);
          return Promise.resolve(result);
        } else {
          this.removeErrorMessage(this.element);
          return Promise.resolve(result);
        }
      };
    }

    removeErrorMessage(element) {
          element.classList.remove("warningBox3","duplicatedWithDbError");
          element.querySelectorAll("span.error").forEach(e => e.remove());
    }

    addErrorMessageToElement(message, element) {
      this.removeErrorMessage(element);
      element.classList.add("warningBox3","duplicatedWithDbError");
      const span = document.createElement("span");
      span.classList.add("error");
      span.innerText = message;
      element.prepend(span);
    }

    isInBasket(instrument) {
      const basketIds = this.getBasketIds();
      return basketIds.includes(instrument.plantId.toString());
    }

    getBasketIds() {
      const it = document.querySelector(".newjobinstsbody");
      const inputs = it ? it.querySelectorAll("[name^='basketIds']") : {};
      return Array.from(inputs).map(i => i.value);
    }

    collectAllInstruments(){
        return Array.from(document.querySelectorAll(".addNewItemsToJobPlugin:not(.hid)"))
            .map(this.collectInstrumentProperty);
    }

    collectInstrumentProperty(element) {
      const coid = element.querySelector('input[name^="coid"]').value;
      const plantNo = element.querySelector('input[name^="instPlantNo"]').value.trim().toUpperCase();
	    const modelId = element.querySelector('input[name^="instModelIds"]').value;
      const serialNo = element.querySelector('input[name^="instSerialNo"]').value.trim();
      return new SimpleInstrumentDto(coid,  plantNo, serialNo, modelId);
    }

    checkForAllSamePlants(){
        const element = document.querySelectorAll(".addNewItemsToJobPlugin");
        element.forEach(e => this.checkForSamePlants(e,e.querySelector("[name^='instPlantNos']")));
    }

    checkForSamePlants(element, currentInput){
        const instruments = this.collectAllInstruments();
        const currentInstrument = this.collectInstrumentProperty(element);
        if(!currentInstrument.plantNo) {
            currentInput.setCustomValidity("");
            return;
        }
        const residualInstruments = instruments.filter(inst =>
         inst.plantNo == currentInstrument.plantNo
         );
         if(residualInstruments.length > 1){
         const message = i18n.t("core.jobs:jobItem.warnDuplicatePantNoFound","Some instrument you are trying to add have same plant number, please review before adding!");
            if(this.options.disallowSamePlantNumber) currentInput.setCustomValidity(message);
            this.showDuplicatePlantNumberWarning(message,currentInput);
         }
         else {
            currentInput.setCustomValidity("");
            this.hideDuplicatePlantNumberWarning(currentInput);
         }
    }

    showDuplicatePlantNumberWarning(message,input){
        const container =  input.parentElement.parentElement.querySelector(".warningContainer");
        container.innerText = message;
        container.classList.remove("hid");
    }

    hideDuplicatePlantNumberWarning(input){
        const container =  input.parentElement.parentElement.querySelector(".warningContainer");
        container.classList.add("hid");
    }
    
    async getProcReference(procId, baseItemId){
    	const contextPath = document.querySelector("meta[name='_contextPath']")?.getAttribute("content") ?? "" ;
        const base = `${location.origin}${contextPath ?? ""}/`
        const url = new URL("proc.json", base);
        url.searchParams.append("procId", procId);

        let res = await fetch(url);
        let responseText = await res.json();
        document.getElementById('procReference'+baseItemId).value = responseText;
       return responseText;
    }


  }

  class SimpleInstrumentDto {
    constructor(coid,  plantNo, serialNo, modelId) {
      this.coid = coid;
      this.plantNo = plantNo;
      this.serialNo = serialNo;
      this.modelId = modelId;
      this.mfrReqForModel = 0;
      this.searchModelMatches = true;
    }
  }

  class BasketItemProperties {
    constructor(
      id,
      plantNo,
      serialNo,
      part,
      existingGroup,
      tempGroup,
      type,
      baseItemId,
      procId,
      quotationItemId,
      index,
      modelId,
      modelName
    ) {
      this.id = id;
      this.plantNo = plantNo;
      this.serialNo = serialNo;
      this.part = part;
      this.existingGroup = existingGroup;
      this.tempGroup = tempGroup;
      this.type = type;
      this.baseItemId = baseItemId;
      this.procId = procId;
      this.quotationItemId = quotationItemId;
      this.index = index;
      this.modelId = modelId;
      this.modelName = modelName;
    }
  }
})(jQuery);
