/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Richard Dean
*
*	FILENAME		:	OnBehalf.js
*	DESCRIPTION		:	Page-specific javascript file.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	12/01/2008 - Richard Dean - Created File
*					:	18/03/2008 - Stuart Harrold - Modified File
*
****************************************************************************************************************************************/

/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'coname';

/**
 * this method checks all inputs of type checkbox that are contained within the parentDiv parameter 
 * 
 * @param {Boolean} select true if boxes are to be checked and vice versa
 * @param {String} parentDiv id of div in which to search for inputs of type checkbox
 */
function selectAllItems(select, parentDiv)
{
	// check all
	if(select == true)
	{
		$j('#' + parentDiv + ' input:checkbox').check('on');
	}
	// uncheck all
	else
	{
		$j('#' + parentDiv + ' input:checkbox').check('off');
	}
}

/**
 * this method delete's the on-behalf of entity for the given jobitem.
 * 
 * @param {Integer} jobItemId the id of the item to reset
 * @param {Integer} onBehalfItemId the behalf of item to delete
 */
function removeItemOnBehalf(jobItemId, onBehalfItemId)
{
	$j.ajax({
		url: "deleteOnBehalfItem.json",
		data: {
			onBehalfItemId: onBehalfItemId
		},
		async: true
	}).done(function() {
		// remove the input field
		$j('#item' + jobItemId + ' input[type="checkbox"]').remove();
		// remove span containing on-behalf info
		$j('span').remove('#behalfinfo' + jobItemId);
		// prepend the original input checkbox
		$j('#item' + jobItemId).prepend('<input type="checkbox" value="' + jobItemId + '" name="jobitemids" />');
		
	}).fail(function(error) {
		alert("Sorry, an error occurs while deleting onbehalf item.");
	});
}