/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	JIActions.js
*	DESCRIPTION		:	Page-specific javascript file.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 
*	KNOWN ISSUES	:	-
*	HISTORY			:	15/10/2007 - JV - Created File
*					:	19/10/2015 - TProvost - Manage i18n
*					: 	19/10/2015 - TProvost - Minor fix in function changeChaseOptions : hide boxy if result = success)
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'script/trescal/core/jobs/jobitem/JobItemFunctions.js',
								'script/thirdparty/jQuery/jquery.boxy.js',
								'script/trescal/core/tools/labelprinting/dymo/DymoLabelFunctions.js',
								'script/thirdparty/jQuery/jquery.scrollTo.js' );

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{

	// load stylesheet using the dom if not already
	loadScript.loadStyle('styles/jQuery/jquery-boxy.css', 'screen');
	// select the correct subnavigation link
	$j('#linkactivities').addClass('selected');
	// user preference to use job item key navigation
	if (userPrefs.jikeynav)
	{
		// initialise key nav to move between job items
		initialiseKeyNavForJI();
	}
}					
					
/**
 * array of calendar objects containing the initial setup literals needed
 * 
 * @param {String} inputField is the id of the input field to which the calendar should be binded
 * @param {String} dateFormat is the format that the date should be displayed
 * @param {String} displayDates 'all' make all dates available
 * 								'future' show only dates in the future
 * 								'past' show only dates in the past
 * applied in init() function
 */
var jqCalendar = new Array(	{ 
								inputField: 'date',
								dateFormat: 'dd.mm.yy',
								displayDates: 'all'
							});

/**
 * this method starts an activity by making the section of page visible and calls
 * method @see getItemsWithSameState() to retrieve any items with the same status.
 * 
 * @param {String} type type of activity to begin (i.e. work, progress)
 * @param {Integer} jobitemid id of the job item
 */
function beginActivity(type, jobitemid)
{
	// type of activity?
	if (type == 'work')
	{
		// hide or show page elements
		$j('div#newactivitydiv').removeClass().addClass('vis');
		$j('div#newprogactivitydiv, div#newtransitdiv').removeClass().addClass('hid');
		// call method to retrieve any other job items with same status
		getItemsWithSameState(jobitemid, type);
	}
	else if (type == 'progress')
	{
		// hide or show page elements
		$j('div#newprogactivitydiv').removeClass().addClass('vis');
		$j('div#newactivitydiv, div#newtransitdiv').removeClass().addClass('hid');
		// call method to retrieve any other job items with same status
		getItemsWithSameState(jobitemid, type);
	}
	else
	{
		// hide or show page elements
		$j('div#newprogactivitydiv, div#newactivitydiv').removeClass().addClass('hid');
		$j('div#newtransitdiv').removeClass().addClass('vis');
	}
}

/**
 * this method shows list items required to complete a work activity.
 */
function completeNow()
{	
	// hide the list item containing buttons
	$j('li.startfields:last').removeClass().addClass('startfields hid');
	// show list items required to complete work activity
	$j('li.endfields').removeClass().addClass('endfields vis');
}

/**
 * this method gets the possible outcome statuses for selected activity
 * and re-populates the outcome status select box with this data
 * 
 * @param {int} activityId
 */
function changeOutcomeStatuses(activityId, activityText, hasLookup)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/outcomestatusservice.js', 'dwr/interface/actionoutcomeservice.js'), true,
	{
		callback: function()
		{
			if (hasLookup == true) 
			{
				$j('div#workendstatusfields select').removeClass('vis').addClass('hid');
				$j('div#workendstatusfields div').removeClass('hid').addClass('vis');
			}
			else 
			{
				// call dwr service to get outcome statuss for activity
				outcomestatusservice.getOutcomeStatusesForActivity(activityId, false, 
				{
					callback: function(outcomeStatuses)
					{
						var options = '';
						var i = 0;
						var selected = 0;
						while (i < outcomeStatuses.length)
						{
							if (outcomeStatuses[i].defaultOutcome == true)
							{
								selected = outcomeStatuses[i].status.stateid;
							}
							
							options +=	'<option value="' +	outcomeStatuses[i].status.stateid +	'">' +
											outcomeStatuses[i].status.description +
										'</option>';
							i++;
						}
						
						$j('div#workendstatusfields select').html(options);
						
						if (selected == 0) {
							// for some reason the last option is selected by default, 
							// so set this to first if there is not a proper default
							$j('div#workendstatusfields select > option:first').attr('selected', 'selected');
						}
						else {
							$j('div#workendstatusfields select > option[value="' + selected + '"]').attr('selected', 'selected');
						}
						
						$j('div#workendstatusfields select').removeClass('hid').addClass('vis');
						$j('div#workendstatusfields div').removeClass('vis').addClass('hid');
					}
				});
			}			
			// set action outcomes based on activity selected
			actionoutcomeservice.getActionOutcomesForActivityId(activityId, 
			{
				callback:function(actionOutcomes)
				{
					var options = '';
					var i = 0;
					var selected = 0;
					if (actionOutcomes.length > 1) 
					{
						while (i < actionOutcomes.length) 
						{
							if (actionOutcomes[i].defaultOutcome == true) 
							{
								selected = actionOutcomes[i].id;
							}					
							options += '<option value="' + actionOutcomes[i].id + '">' + actionOutcomes[i].description + '</option>';
							i++;
						}
					}
					else
					{
						options = '<option value="">-- ' + i18n.t("na", "N/A") + ' --</option>';
					}
					
					$j('select#workactionoutcome').html(options);
					
					if(selected == 0)
					{
						// for some reason the last option is selected by default, 
						// so set this to first if there is not a proper default
						$j('div#workendstatusfields select > option:first').attr('selected','selected');
					}
					else
					{
						$j('div#workendstatusfields select > option[value="' + selected + '"]').attr('selected','selected');
					}
				}
			});
		}
	});
}

/**
 * this method cancels a work activity.
 */
function cancelActivity()
{
	// hide the list items required to complete work activity
	$j('li.endfields').removeClass().addClass('endfields hid');
	// hide the list item containing buttons
	$j('li.startfields:last').removeClass().addClass('startfields vis');
}

/**
 * this method inserts a completed activity.
 * 
 * @param {Integer} jobitemid id of the job item
 * @param {String} type type of activity to complete (i.e. work, progress)
 */
function insertCompletedActivity(jobitemid, type)
{
	// get values to complete activity
	var actId = $j('#' + type + 'activity').val();
	var remark = $j('#' + type + 'remark').val();
	var timeSpent = $j('#' + type + 'timespent').val();
	var actionOutcomeId = $j('#' + type + 'actionoutcome').val();
	var saveAsJobNote = null;
	
	// activity type?
	if(type == 'work')
	{
		// get value to complete activity
		var endStatId = $j('#' + type + 'endstatus').val();
	}
	else
	{
		// progress activity so end status cannot be selected
		var endStatId = null;
		
		saveAsJobNote = ($j('input#progressjobnote').is(':checked')) ? true : false;
	}
	
	// get all checked items in same status job items list item
	var arrayOfItems = getCheckedItems(type);
	
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/jobitemactivityservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to complete activity
			jobitemactivityservice.newCompletedActivityDWR(actId, endStatId, jobitemid, arrayOfItems, remark, timeSpent, actionOutcomeId, saveAsJobNote, 
			{
				callback: function(result)
				{
					console.log("starting callback ");
					console.log(result);
					// call method to update the page
					displayActivity(result, type, 'complete');
					console.log("completing callback");
				}
			});
		}
	});
}



/**
 * this method inserts an incomplete activity.
 * 
 * @param {Integer} jobitemid id of the job item
 * @param {String} type type of activity to complete (i.e. work, progress)
 */
function insertIncompleteActivity(jobitemid, type)
{
	// get the activity
	var actId = $j('#' + type + 'activity').val();
	// get all checked items in same status job items list item
	var arrayOfItems = getCheckedItems(type);

	var data = {};
	data["jobItemId"] = jobitemid;
	data["activityId"] = actId;

	$j.ajax({
		type: "POST",
		contentType: "application/json",
		url: "create_incomplete_activity",
		data: JSON.stringify(data),
		dataType: 'json'})
		.done( function (result)
	{
		//displayActivity(result, type, 'incomplete');
		var lastStepNo = Number($j('#actions tr:not(.deleted):last td:first').text());
		// create new step number
		var stepNo = lastStepNo + 1;

		$j('#actions').append(
			'<tr id="activity' + result.activityId + '" class="activity">' +
			'<td class="center">' + stepNo + '</td>' +
			'<td class="bstatus">' + result.startStatus + '</td>' +
			'<td>' + result.description + '</td>' +
			'<td class="startby">' + result.startedBy + '<br />(' +cwms.dateTimeFormatFunction(result.startStamp)  + ')</td>' +
			'<td class="notcomplete" colspan="5"><input type="button" value="' + i18n.t("core.jobs:jobItem.completeActivity", "Complete Activity") + '" ' +
			'onclick=" completePendingActivity(' + result.jobItemId + ', \'' + type + '\'); return false; " /></td>' +
			'</tr>');

		// disable ability to add another activity before completing the one just added
		$j('span#newActivity, span#newProgActivity, div#newactivitydiv, input#backButton, input#noIncompleteButton').removeClass().addClass('hid');
		$j('input#incompleteButton').removeClass().addClass('vis');
		$j('a#overrideStatusLink').removeClass('vis').addClass('hid');
		$j('div#overrideStatusHolder').removeClass('vis').addClass('hid');

	});
}

/**
 * this method displays the section of page and list items necessary to complete a pending activity
 * as well as calling method @see getItemsWithSameState() to retrieve any items with the same status.
 * 
 * @param {Integer} jobitemid id of the job item
 * @param {String} type type of activity to complete (i.e. work, progress)
 */
function completePendingActivity(jobitemid, type)
{
	// display div
	$j('#newactivitydiv, input#incompleteButton').removeClass().addClass('vis');
	// display list items to complete pending activity
	$j('li.startfields').removeClass().addClass('startfields hid');
	$j('li.endfields').removeClass().addClass('endfields vis');
	// hide buttons and show incomplete button
	$j('input#noIncompleteButton, input#backButton').removeClass().addClass('hid');
	// call method to retrieve any other job items with same status
	getItemsWithSameState(jobitemid, type);
}

/**
 * this method updates a pending activity.
 * 
 * @param {Integer} jobitemid id of the job item
 * @param {String} type type of activity to complete (i.e. work, progress)
 */
function updatePendingActivity(jobItemId, type)
{
	// get values to update pending activity
	var endStatId = null;
	if(!$j('#' + type + 'endstatus').hasClass('hid'))
	{
		endStatId = $j('#' + type + 'endstatus').val();
	}
	
	var remark = $j('#' + type + 'remark').val();
	var timeSpent = $j('#' + type + 'timespent').val();
	var actionOutcomeId = $j('#' + type + 'actionoutcome').val();
	// get all checked items in same status job items list item
	var arrayOfItems = getCheckedItems(type);
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/jobitemactivityservice.js'), true,
	{
		callback: function()
		{
			// create the activity for any additional items selected
			$j.each(arrayOfItems, function(i)
			{
				// note - there is no need for a callback when completing the activity for an item that is not being displayed on this page
				// call dwr service to complete pending activity for each additional item
				jobitemactivityservice.completePendingActivity(arrayOfItems[i], endStatId, remark, timeSpent, actionOutcomeId);
			});
			// call dwr service to complete pending activity
			jobitemactivityservice.completePendingActivity(jobItemId, endStatId, remark, timeSpent, actionOutcomeId, 
			{
				callback: function(result)
				{
					// call method to update the page
					displayActivity(null, type, 'complete');
				}
			});
		}
	});
}

/**
 * this method updates the page after dwr calls to set activities.
 * 
 * @param {Object} result resultWrapper object returned from dwr call
 * @param {String} type type of activity to complete (i.e. work, progress)
 * @param {String} status status of the activity (i.e. complete, incomplete)
 */
function displayActivity(result, type, status)
{
	// there is no reason this should ever be null, but for some reason it sometimes is! 
	// we have seen that this freak blip mostly happens when an action is successful,
	// so just reload the current page for now to show the new activity list.
	if(result == null)
	{
		// reload the current page
		window.location.href = window.location;
		
		// return here as sometimes javascript decides to carry on and execute 
		// the rest of the method before the page refresh!
		return false;
	}
	
	// failed?
	if(result.success != true)
	{	
		// show all error messages
		if(result.message != null)
		{
			$j.prompt(result.message);
		}
		else if(result.errorList != null)
		{
			for(var i = 0; i < result.errorList.length; i++)
			{
				var error = result.errorList[i];
				$j.prompt(i18n.t("fieldError", {varErrField: error.field, varErrMsg:error.defaultMessage, defaultValue : "Field: " + error.field + " failed. Message:" + error.defaultMessage}));
			}
		}
	}
	else
	{
		// reset action table
		toggleActionColumns($j('#toggleActionColumns'), false);
		// unwrap activity result
		var activity = result.results;
		
		// actions set?
		if (parseInt($j('span.jiActionSize:first').text()) > 0)
		{
			// get current step number
			var lastStepNo = Number($j('#actions tr:not(.deleted):last td:first').text());
			// create new step number
			var stepNo = lastStepNo + 1;
		}
		else
		{
			// create first step
			var stepNo = 1;
		}		
		// status of activity
		if (status == 'complete')
		{
			// completing pending activity?
			if ($j('#actions tr#activity' + activity.id).length)
			{
				// remove pre-appended table row
				$j('#actions tr#activity' + activity.id).remove();
				// amend step number
				stepNo = stepNo - 1;
			}
			else if ($j('#actions tr:first td').length < 2)
			{
				// remove message row
				$j('#actions tr:first').remove();
			}
			// add the new activity to the table
			$j('#actions').append(	'<tr id="activity' + activity.id + '" class="activity">' +
										'<td class="center">' + stepNo + '</td>' + 
										'<td class="bstatus">' + activity.startStatus.description + '</td>' + 
										'<td>' + activity.activityDesc + '</td>' + 
										'<td class="startby">' + activity.startedBy.name + '<br />(' + cwms.dateTimeFormatFunction(activity.startStamp) + ')</td>' +
										'<td>' + activity.completedBy.name + '<br />(' + cwms.dateTimeFormatFunction(activity.endStamp) + ')</td>' +
										'<td>' + activity.endStatus.description + '</td>' + 
										'<td class="time center">' + activity.timeSpent + ' mins</td>' + 
										'<td class="remark">' + activity.remark + '</td>' + 
										'<td class="edit"><a href="#" onclick=" editAction($j(this).closest(\'tr\'), ' + activity.id + '); return false; "><img src="img/icons/note_edit.png" height="16" width="16" alt="' + i18n.t("core.jobs:jobItem.editAction", "Edit Action") + '" title="' + i18n.t("core.jobs:jobItem.editAction", "Edit Action") + '" /></a></td>' +
									'</tr>');
			// clean up display
			$j('span#newActivity, span#newProgActivity, input#noIncompleteButton').removeClass().addClass('vis');
			$j('input#incompleteButton, div#newactivitydiv, div#newprogactivitydiv').removeClass().addClass('hid');
			$j('li.startfields').removeClass().addClass('startfields vis');
			$j('li.endfields').removeClass().addClass('endfields hid');
			$j('a#overrideStatusLink').removeClass('hid').addClass('vis');	
			// change status of the item near top of page
			$j('span#currentState').text(activity.endStatus.description);
			// update action count in subnav
			$j('span.jiActionSize').text(parseInt($j('span.jiActionSize:first').text()) + 1);
			// load the service javascript file if not already
			loadScript.loadScriptsDynamic(new Array('dwr/interface/nextactivityservice.js'), true,
			{
				callback: function()
				{
					// put new allowed activities into select box
					nextactivityservice.getNextActivitiesForStatus(activity.endStatus.stateid, true,
					{
						callback:function(nextActivities)
						{	
							if (nextActivities.length > 0) 
							{
								var options = '';
								var i = 0;
								while (i < nextActivities.length)
								{							
									var lookupCls = "";
									if(nextActivities[i].activity.lookup)
									{
										lookupCls = "lookup";
									}
									
									options +=	'<option class="' +	lookupCls +	'" value="' + nextActivities[i].activity.stateid + '">' +
													nextActivities[i].activity.description +
												'</option>';							
									i++;
								}
								
								$j('select#workactivity').html(options);
								$j('select#workactivity > option:first').attr('selected', 'selected');
								
								var hasLookup = false;
								if(nextActivities[0].activity.lookup)
								{
									hasLookup = true;
								}
								
								// get outcome statuses for the first activity in list
								changeOutcomeStatuses(nextActivities[0].activity.stateid, nextActivities[0].activity.description, hasLookup);
							}
							else
							{
								$j('select#workactivity').parent('li').parent('ol').html(
									'<li>' + i18n.t("core.jobs:jobItem.noActivitiesAvailable", "There are no activities available to begin from this item's status.") + '</li>'
								);
							}
						}
					});
				}
			});
		}
		else
		{
			// message row present?
			if ($j('#actions tr:first td').length < 2)
			{
				// remove message row
				$j('#actions tr:first').remove();
			}
			// add pending activity to the table
			$j('#actions').append(	'<tr class="activity" id="activity' + activity.id + '">' +
										'<td class="center">' + stepNo + '</td>' + 
										'<td class="bstatus">' + activity.startStatus.description + '</td>' + 
										'<td>' + activity.activityDesc + '</td>' + 
										'<td class="startby">' + activity.startedBy.name + '<br />(' + cwms.dateTimeFormatFunction(activity.startStamp) + ')</td>' +
										'<td class="notcomplete" colspan="5"><input type="button" value="' + i18n.t("core.jobs:jobItem.completeActivity", "Complete Activity") + '" onclick=" completePendingActivity(' + activity.jobItem.jobItemId + ', \'' + type + '\'); return false; " /></td>' +
									'</tr>');
			// disable ability to add another activity before completing the one just added
			$j('span#newActivity, span#newProgActivity, div#newactivitydiv, input#backButton, input#noIncompleteButton').removeClass().addClass('hid');
			$j('input#incompleteButton').removeClass().addClass('vis');
			$j('a#overrideStatusLink').removeClass('vis').addClass('hid');			
			$j('div#overrideStatusHolder').removeClass('vis').addClass('hid');			
		}
		
		// reset the form
		resetForm(type);
	}
}

/**
 * this method resets form elements for the supplied activity type.
 * 
 * @param {String} type type of activity to complete (i.e. work, progress)
 */
function resetForm(type)
{
	// reset form elements (changed to just reset remark 2016-02-22 GB as multiple new progress activities failing)
	//$j('#' + type + 'remark, #' + type + 'activity, #' + type + 'endstatus').val('');
	$j('#' + type + 'remark').val('');
	$j('#' + type + 'timespent').val('5');
}

/**
 * this method retrieves all job items with the same status as the current one.
 * 
 * @param {Integer} jobitemid id of the job item
 * @param {String} type type of activity to complete (i.e. work, progress)
 */
function getItemsWithSameState(jobItemId, type)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/jobitemactivityservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to retrieve job items
			jobitemactivityservice.getJobItemsWithSameState(jobItemId, 
			{
				callback: function(items)
				{
					// items with the same state?
					if(items.length > 0)
					{
						// empty same state items div for activity type
						$j('div#' + type + 'SSItems').empty();
						// populate variable to hold same state items with select all link
						var content = 	'<div class="marg-bot">' +												
											'<input type="checkbox" onclick=" selectAllItems(this.checked, \'sameState' + type + 'Ids\', \'' + type + 'SSItems\'); " />' +
											i18n.t("all", "All") +
										'</div>';
						// add each same state job item to variable
						$j.each(items, function(i)
						{	
							// get definitive instrument model
							var instmodel = items[i].inst.definitiveInstrument;
							// add job item to same state items div
							content = content + '<div style=" height: 26px; "><input type="checkbox" name="sameState' + type + 'Ids" class="sameStateItem" value="' + items[i].jobItemId + '" /><a id="jobitem' + items[i].jobItemId + '" class="jconTip mainlink" tabindex="-1" name="' + i18n.t("jobItemInformation", "Job Item Information") + '" href="' + items[i].defaultPage + '?jobitemid=' + items[i].jobItemId + '&ajax=jobitem&width=500">' + items[i].itemNo + '</a> ' + instmodel + '</div>';					
						});
						// append same state job items content
						$j('div#' + type + 'SSItems').append(content);
						// initialise any itemno links
						JT_init();
					}
					else
					{
						// empty span and append message
						$j('div#' + type + 'SSItems').empty().append('<div>' + i18n.t("core.jobs:jobItem.noItemsWithSameStatus", "No items with same status") + '</div>');
					}
				}
			});
		}
	});
}

/**					
 *	this method returns an array of all job item ID's from checked checkboxes
 *	within the same state job item list.
 *
 * @param {String} type type of activity to complete (i.e. work, progress)
 */
function getCheckedItems(type)
{
	// define blank array
	var otherItems = new Array();
	// find all checked checkboxes within same state job items div
	$j('div#' + type + 'SSItems input:checkbox.sameStateItem:checked').each(function(i, n)
	{
		// add value to array
		otherItems[i] = n.value;
	});
	// return array
	return otherItems;
}

/**
 * this function retrieves the amount of minutes passed since the activity was started
 * 
 * @param {Integer} actId id of the activity to be checked
 */
function getMinutesSinceStart(actId)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/jobitemactivityservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to retrieve minutes passed since start of activity
			jobitemactivityservice.getMinutesFromActivityStart(actId,
			{
				callback:function(minutes)
				{
					// change value in input field
					$j('#worktimespent').val(minutes);
				}
			});
		}
	});
}

/**
 * this method toggles the visibility of different action types within the
 * activity table.
 * 
 * @param {String} type action type whose visibility should be toggled
 * @param {Object} checkbox action type checkbox
 */
function toggleActions(type, checkbox)
{
	// checkbox checked?
	if($j(checkbox).is(':checked'))
	{
		// show all actions of this type
		$j('tr.' + type).removeClass('hid').addClass('vis');
	}
	else
	{
		// hide all actions of this type
		$j('tr.' + type).removeClass('vis').addClass('hid');	
	}
}

/**
 * this method retrieves all transits between bases within business company.
 */

/**
 * this method retrieves all locations for a business company address.
 * 
 * @param {Integer} addrId id of address
 * @param {String} selectBoxId id of the select box to be updated
 */
function changeLocs(addrId, selectBoxId)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/locationservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to retrieve address locations
			locationservice.getAllAddressLocations(addrId, true, 
			{
				callback:function(locations)
				{
					// empty select element
					dwr.util.removeAllOptions(selectBoxId);
					// locations returned?
					if (locations.length > 0)
					{
						// add location options to select element
						dwr.util.addOptions(selectBoxId, locations, 'locationid', 'location');
					}
					else
					{
						// add option for no locations
						$j('#' + selectBoxId).append('<option value="">-- ' + i18n.t("core.jobs:jobItem.noLocations", "No Locations") + ' --</option>');
					}
					
				}	
			});
		}
	});
}

/**
 * this method allows the user to edit a remark made in the job item actions table
 * 
 * @param {Object} tr item action table row
 * @param {Integer} id identifier of the action
 */
function editAction(tr, id)
{
	// make fields editable
	var time = $j(tr).find('td.time').text();
	time = time.split(" ")[0];
	// get the remark value
	var remark = $j(tr).find('td.remark').text();
	// get id of item action row
	var trid = $j(tr).attr('id');
	// create content to be displayed in overlay
	var content = 	'<div id="editActionOverlay">' +
						'<fieldset>' +							
							'<ol>' +
								'<li>' +
									'<label>' +i18n.t("timeLabel", "Time:") + '</label>' +
									'<input type="text" size="2" id="editActionTime" value="' + time + '" />' +
								'</li>' +
								'<li>' +
									'<label>' + i18n.t("remarkLabel", "Remark:") + '</label>' +
									'<span>' +
										'<textarea id="editActionRemark" class="width70" rows="6" value="' + remark + '">' + remark + '</textarea>' +
									'</span>' +
								'</li>' +
								'<li>' +
									'<label>&nbsp;</label>' +
									'<input type="button" value="' + i18n.t("submit", "Submit") + '" onclick=" saveAction(\'' + trid + '\', ' + id + '); " />' +										
								'</li>' +
							'</ol>' +
						'</fieldset>' +
					'</div>';
	// create new overlay using content
	loadScript.createOverlay('dynamic', i18n.t("core.jobs:jobItem.editActionRemark", "Edit Action Remark"), null, content, null, 40, 620, 'editActionRemark');
}

/**
 * this method saves the updated action remark and time spent
 * 
 * @param {Object} trid item action row id attribute value
 * @param {Integer} id identifier of the action
 */
function saveAction(trid, id)
{	
	// get new field values
	var time = $j('div#editActionOverlay input#editActionTime').val();
	var remark = $j('div#editActionOverlay textarea#editActionRemark').val();
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/jobitemactionservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to update action
			jobitemactionservice.editAction(id, time, remark,
			{
				callback:function(result)
				{
					// updated successfully?
					if(!result.success)
					{	
						var error = '';
						// display error messages
						if(result.message != null)
						{
							error = result.message;					
						}
						else if(result.errorList != null)
						{
							error = loadScript.unwrapErrors(result.errorList);
						}
						// add error message to overlay
						$j('div#editActionOverlay').prepend('<div class="warningBox1">' +
																'<div class="warningBox2">' +
																	'<div class="warningBox3">' +
																		error +
																	'</div>' +
																'</div>' +
															'</div>');
					}
					else
					{
						// assign job item action object
						var jia = result.results;						
						// display new fields in the table
						$j('#' + trid).find('td.remark').html(jia.remark);
						$j('#' + trid).find('td.time').html(jia.timeSpent + ' ' + i18n.t("mins", "mins"));
						// close the overlay
						loadScript.closeOverlay();
					}
				}
			});
		}
	});
}

function loadOverrideOverlay(jobItemId) {
	$j.ajax({
		url: "loadOverrideOverlay.htm",
		type: "GET",
		data: {
			jobItemId: jobItemId
		},
		async: true
	}).done(function(content) {
		// create new boxy pop up using content variable
		var changeItemStatusBoxy = new Boxy(content, {title: i18n.t("core.jobs:jobItem.changeItemStatus", "Change Item Status"), modal: true, unloadOnHide: true});
		// show boxy pop up and adjust size
		changeItemStatusBoxy.tween(1000,400);
		// create the preset comments plugin
		$j('div#changeItemStatusBoxy textarea:first').css({width: '76%'}).presetCommentJQPlugin({ symbolBox: false });
	});
}

function loadHoldOverlay(jobItemId) {
	$j.ajax({
		url: "loadHoldOverlay.htm",
		type: "GET",
		data: {
			jobItemId: jobItemId
		},
		async: true
	}).done(function(content) {
		// create new boxy pop up using content variable
		var changeItemStatusBoxy = new Boxy(content, {title: i18n.t("core.jobs:jobItem.changeItemStatus", "Change Item Status"), modal: true, unloadOnHide: true});
		// show boxy pop up and adjust size
		changeItemStatusBoxy.tween(800,350);
		// create the preset comments plugin
		$j('div#changeItemStatusBoxy textarea:first').css({width: '76%'}).presetCommentJQPlugin({ symbolBox: false });
	});
}

/**
 * this method toggles the start status and started by columns in the job item actions table.
 * 
 * @param {Object} anchor the anchor clicked to toggle action columns
 * @param {Boolean} show this indicates whether we want to show or hide the columns
 */
function toggleActionColumns(anchor, show)
{
	if (show)
	{
		$j('#tableofactions thead tr:not(:last) td, #tableofactions tfoot tr td').attr('colspan', '9');
		$j('#tableofactions .bstatus, #tableofactions .startby').css({display: 'table-cell', width: '14%'});
		$j('#tableofactions .activity, #tableofactions .completeby, #tableofactions .estatus').css({width: '14%'});
		$j('#tableofactions .remark').css({width: '15%'});
		$j(anchor).after('<a href="#" id="toggleActionColumns" class="mainlink" onclick=" toggleActionColumns(this, false); return false; ">' + i18n.t("core.jobs:jobItem.hideStartStatus", "Hide Start Status") + '</a>').remove();
	}
	else
	{
		$j('#tableofactions thead tr:not(:last) td, #tableofactions tfoot tr td').attr('colspan', '7');
		$j('#tableofactions .bstatus, #tableofactions .startby').css({display: 'none', width: '0%'});
		$j('#tableofactions .activity').css({width: '24%'});
		$j('#tableofactions .completeby').css({width: '16%'});
		$j('#tableofactions .estatus').css({width: '22%'});
		$j('#tableofactions .remark').css({width: '23%'});
		$j(anchor).after('<a href="#" id="toggleActionColumns" class="mainlink" onclick=" toggleActionColumns(this, true); return false; ">' + i18n.t("core.jobs:jobItem.showStartStatus", "Show Start Status") + '</a>').remove();
	}
}

function getRemoteJobitem(jobItemId, webServiceId)
{
	loadScript.loadScriptsDynamic(new Array('dwr/interface/jobitemservice.js'), true,
	{
		callback: function()
		{
			webServiceId = 1;
			jobitemservice.getAjaxRemoteJobItemActions(jobItemId, webServiceId, 
			{
				callback: function(result)
				{
					result.success = true;
					if(result.success == true)
					{		
						var actions = result.results.actions;
						var content = '<br/><table class="default4"><tr><th>' + i18n.t("activity", "Activity") + '</th><th>' + i18n.t("startedBy", "Started By") + '</th><th>' + i18n.t("startedOn", "Started On") + '</th><th>' + i18n.t("endStatus", "End Status") + '</th><th>' + i18n.t("time", "Time") + '</th></tr>';
						if(actions != null && actions.length > 0)
						{
							for(var i = 0; i < actions.length; i++)
							{
								var action = actions[i];
								content+=	'<tr>' +
												'<td>'+action.activity+'</td>'+
												'<td>'+action.startedBy+'</td>'+
												'<td>'+cwms.dateTimeFormatFunction(action.formattedStartStamp)+'</td>'+
												'<td>'+(action.endStatus != null && action.endStatus != '' ? action.endStatus : ' - ON-GOING - ')+'</td>'+
												'<td>'+(action.timeSpent != null ? action.timeSpent : '&nbsp;')+'</td>'+
											'</tr>';
										if(action.remark != null && action.remark != '')
										{
								content+=	'<tr>' +
												'<td colspan="6" style=" border-top: none; padding-left: 20px; padding-right: 20px; "><i>'+action.remark+'</i></td>'+
											'</tr>';
										}
							}
						}
						content+='</table>';
						content+='<div class="float-right padtop">' + i18n.t("core.jobs:jobItem.timeResultsRetrieved", {varTimeTaken : result.results.timeTaken, defaultValue : "Results retrieved in " + result.results.timeTaken + " seconds"}) + '</div>';
						
						var systemChecked = $j('#webServiceSelector option:selected').text();
						var title = i18n.t("core.jobs:jobItem.actionsForItem", {varItemNo : result.results.itemno, varJobNo : result.results.jobno, varSystemChecked : systemChecked, defaultValue : "Actions for Item " + result.results.itemno + " on Job " + result.results.jobno + " on " + systemChecked});
						
						// create new overlay
						loadScript.createOverlay('dynamic', title, null, content, null, 50, 750, null);
					}
					else
					{
						alert(result.message);
					}
				}
			});
		}
	});
}

/**
 * 
 * 
 *
 */
function createChaseContent(jobItemId)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/jobitemservice.js'), true,
	{
		callback: function()
		{
			// call dwr service
			jobitemservice.findJobItem(jobItemId,
			{	
				callback:function(jobItem)
				{
					var checked1 = (jobItem.chasingActive) ? ' checked="checked" ' : '';
					var checked2 = (jobItem.neverToBeChased) ? ' checked="checked" ' : '';
				
					// start creating jquery boxy content string
					var content = 	'<div id="chaseJIBoxy" class="overflow">' +
										'<fieldset>' +
											'<legend>' + i18n.t("core.jobs:jobItem.changeChaseOptions", "Change chase options for job item") + '</legend>' +
											'<ol>' +
												'<li>' +
													'<label>' + i18n.t("core.jobs:jobItem.chasingActiveAtThisItem", "Chasing active at this status:") + '</label>' +
													'<input type="checkbox" id="chaseBox1" name="chaseBox1" value="true" ' + checked1 + '/>' +
												'</li>' +
												'<li>' +
													'<label>' + i18n.t("core.jobs:jobItem.neverChaseThisItem", "Never chase this item:") + '</label>' + 
													'<input type="checkbox" id="chaseBox2" name="chaseBox2" value="true" ' + checked2 + '/>' +
												'</li>' +
												'<li>' +
													'<label>&nbsp;</label>' +			
													'<input type="button" onclick=" this.disabled = true; changeChaseOptions(' + jobItemId + ', $j(\'#chaseBox1\').is(\':checked\'), $j(\'#chaseBox2\').is(\':checked\')); return false; " value="' + i18n.t("update", "Update") + '" />' +
												'</li>' +
											'</ol>' +
										'</fieldset>' +
									'</div>';	
					// create new boxy pop up using content variable
					var chaseJIBoxy = new Boxy(content, {title: '' + i18n.t("core.jobs:jobItem.changeChaseOptions", "Change chase options for job item"), modal: true, unloadOnHide: true});
					// show boxy pop up and adjust size
					chaseJIBoxy.tween(480,240);
				}
			});
		}
	});
}

/**
 *
 *
 */
function changeChaseOptions(jobItemId, chaseActive, neverChase)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/jobitemservice.js'), true,
	{
		callback: function()
		{
			// call dwr service
			jobitemservice.ajaxSetJobItemChaseOptions(jobItemId, chaseActive, neverChase,
			{	
				callback:function(result)
				{
					if(result.success)
					{
						// put green success box here
						Boxy.get($j('div#chaseJIBoxy')).hide();
						// then auto close
					}
					else
					{
						alert(result.message);
					}
				}
			});
		}
	});
}
