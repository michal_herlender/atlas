/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	JICalibration.js
*	DESCRIPTION		:	Page-specific javascript file for JICalibration.js
*	DEPENDENCIES	:   -
*
*	TO-DO			: 
*	KNOWN ISSUES	:	-
*	HISTORY			:	23/01/2008 - SH - Created File
*					:	14/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'script/trescal/core/jobs/jobitem/JobItemFunctions.js',
								'script/trescal/core/tools/labelprinting/dymo/DymoLabelFunctions.js',
								'script/thirdparty/jQuery/jquery.scrollTo.js');

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{
	// select the correct subnavigation link
	$j('#linkcalibration').addClass('selected');
	// user preference to use job item key navigation
	if (userPrefs.jikeynav)
	{
		// initialise key nav to move between job items
		initialiseKeyNavForJI();
	}
}

var stdsChanged = false;

/**
 * this method finds a calibration using the supplied id for which we can issue a certificate, this is the belated certificate
 * functionality.
 * 
 * @param {Integer} calId id of the calibration to issue certificate for
 * @param {Integer} plantId id of the instrument
 * @param {Integer} thisLinkId certlink id
 */
function findCalToIssueCert(calId, plantId, thisLinkId)
{
	// load javascript service file
	loadScript.loadScriptsDynamic(new Array('dwr/interface/calibrationservice.js'), true,
	{
		callback: function()
		{
			calibrationservice.findCalToIssueCert(calId, plantId, 
			{
				callback:function(calToCertify)
				{
					// create content to be displayed
					var content = 	'<div id="issueBelatedCertificateOverlay">' +
										'<form action="" method="post">' +
											'<fieldset>' +
												'<ol>' +
													'<li>' +
														'<label>' + i18n.t("certDurationLabel", "Cert Duration:") + '</label>' +
														'<input type="text" name="duration" id="duration" size="4" value="' + calToCertify.defaultDuration + '" />&nbsp;' +
														'<select name="unitCode" id="unitCode">';
														
														Object.keys(intervalUnits).forEach(key => {
															var selected = '';
															if(intervalUnits[key].id == calToCertify.defaultDurationUnitCode) {
																selected = ' selected ';
															}
															content += '<option value="' + intervalUnits[key].id + '"' + selected + '>' +
														    	intervalUnits[key].name +
														    '</option>'
														});
														
													content += '</select>' +
													'</li>' +
													'<li>' +
														'<label>' + i18n.t("core.jobs:jobItem.applyToItems", "Apply to item(s):") + '</label>' + '<br/><br/>';
														for(var key in calToCertify.calLinks) 
														{
															var dis = '';
															if(thisLinkId == key)
															{
																dis = ' disabled="disabled" ';
															}
															content += '<input type="checkbox" name="calLinkIdsToInclude" checked="checked" ' + dis + ' value="' + key + '" />&nbsp;' + 
															calToCertify.calLinks[key] + '<br/>';
														}
										content += 	'</li>' +
													'<li>' +
														'<label>&nbsp;</label>' +
														'<input type="hidden" name="calId" value="' + calId + '" />' +
														'<input type="hidden" name="calLinkIdsToInclude" value="' + thisLinkId + '" />' +
														'<input type="submit" value="' + i18n.t("submit", "Submit") + '" />' +
													'</li>' +
												'</ol>' +
											'</fieldset>' +
										'</form>' +
									'</div>';
					// create new boxy pop up using content
					loadScript.createOverlay('dynamic', i18n.t("core.jobs:jobItem.issueCertForCal", "Issue Certificate For Calibration"), null, content, null, 30, 680, 'duration');
				}
			});
		}
	});
}
