/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	JICallOffItem.js
*	DESCRIPTION		:	Page-specific javascript file.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 
*						
*	KNOWN ISSUES	:	-
*	HISTORY			:	06/11/2007 - JV - Created File
*
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'script/trescal/core/jobs/jobitem/JobItemFunctions.js',
								'script/trescal/core/tools/labelprinting/dymo/DymoLabelFunctions.js',
								'script/thirdparty/jQuery/jquery.scrollTo.js' );

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{
	// select the correct subnavigation link
	$j('#linkcalloffitem').addClass('selected');
	// user preference to use job item key navigation
	if (userPrefs.jikeynav)
	{
		// initialise key nav to move between job items
		initialiseKeyNavForJI();
	}
}

/**
 * this method checks all inputs of type checkbox that are contained within the table and
 * calls method to enabled textareas in row.
 * 
 * @param {Boolean} checkedValue true if boxes are to be checked and vice versa
 * @param {String} parentElement id of table in which to search for inputs of type checkbox
 */
function selectCallOffItems(checkedValue, parentElement)
{
	$j('#' + parentElement + ' input:checkbox').prop('checked',checkedValue);

	enableCheckedTextAreas();
}

/**
 * this method finds all checked checkboxes in the table and enables textarea and copy link
 * in the row that the checkbox appears.
 */
function enableCheckedTextAreas()
{
	// get all checkboxes in table
	$j('table#jobitemcalloff input:checkbox').each(function (i, n)
	{
		// checkbox checked?
		if($j(n).is(':checked'))
		{
			// get find textarea and anchor, make visible
			$j(n).parent().parent().find('td:last textarea').removeClass().addClass('vis').end().find('a').removeClass().addClass('mainlink vis');
		}
		else
		{
			// find textarea and anchor, clear and hide
			$j(n).parent().parent().find('td:last textarea').removeClass().addClass('hid').val('').end().find('a').removeClass().addClass('mainlink hid');
		}
	});
}

/**
 * this method copies the value of selected textarea to any other visible
 * textareas.
 * 
 * @param {Object} link anchor clicked
 */
function copyReason(link)
{
	// get value of textarea to be copied
	var reason = $j(link).siblings('textarea').val();
	// add value to all visible textareas
	$j('textarea.vis').val(reason);
}