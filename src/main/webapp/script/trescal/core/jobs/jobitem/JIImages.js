/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	JIImages.js
*	DESCRIPTION		:	Page-specific javascript file for JIImages.js
*	DEPENDENCIES	:   -
*
*	TO-DO			: 
*	KNOWN ISSUES	:	-
*	HISTORY			:	29/05/2009 - SH - Created File
*
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (
		'script/trescal/core/jobs/jobitem/JobItemFunctions.js',
		'script/trescal/core/tools/labelprinting/dymo/DymoLabelFunctions.js',
		'script/thirdparty/jQuery/jquery.scrollTo.js',
		);

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{
	// select the correct subnavigation link
	$j('#linkimages').addClass('selected');
	// user preference to use job item key navigation
	if (userPrefs.jikeynav)
	{
		// initialise key nav to move between job items
		initialiseKeyNavForJI();
	}
}