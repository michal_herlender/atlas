/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	CollectInstruments.js
*	DESCRIPTION		:	Page-specific javascript file.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 
*						
*	KNOWN ISSUES	:	-
*	HISTORY			:	07/03/2008 - JV - Created File
*					:	14/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/*
 * Array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'dwr/interface/instrumservice.js' );

/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'bc';

function checkBarcode(bc)
{
	instrumservice.findIntSafeInstrument(bc, 
	{
		callback:function(instrum)
		{
			if(instrum == null)
			{
				$j.prompt(i18n.t("core.jobs:jobItem.instrNotFound", "Instrument not found."));
			}
			else
			{
				$j('#instruments').append(
					'<li>' +
						instrum.plantid + ' ' + instrum.model.mfr.name + ' ' + instrum.model.model + ' ' + instrum.model.description.description +
						'<input type="hidden" name="plantIds" value="' + instrum.plantid + '" />' +
					'</li>'
				);	
			}
		}
	});
	
	$j('#bc').val('').focus();
}
