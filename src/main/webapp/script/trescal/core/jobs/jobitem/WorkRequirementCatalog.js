/**
 * Array of autocomplete objects (multiple are possible) containing setup for 
 * jQueryUI initialization.
 * this is used in the init() function.
 */
var jquiAutocomplete = new Array(
	{
		inputFieldName: 'subfamilyName',
		inputFieldId: 'subfamilyId',
		source: 'searchdescriptiontags.json'
	});

function showSubdivisionSelect() {
	$j('#spanSubdivDisplay').addClass('hid').removeClass('vis');
	$j('#spanSubdivSelect').addClass('vis').removeClass('hid');
	
	$j('#spanSubdivChangeLink').addClass('hid').removeClass('vis');
	$j('#spanSubdivResetLink').addClass('vis').removeClass('hid');
}

function hideSubdivisionSelect() {
	$j('#spanSubdivSelect').addClass('hid').removeClass('vis');
	$j('#spanSubdivDisplay').addClass('vis').removeClass('hid');
	
	$j('#spanSubdivResetLink').addClass('hid').removeClass('vis');
	$j('#spanSubdivChangeLink').addClass('vis').removeClass('hid');
}

function toggleGenericCapabilities(checked) {
	if (checked) {
		$j('#subfamilyName').addClass('hid').removeClass('vis');
		$j('#subfamilyGeneric').addClass('vis').removeClass('hid');
		
		$j('#includeAllCheckbox').addClass('hid').removeClass('vis');
		$j('#includeAllSpan').addClass('hid').removeClass('vis');
		$j('#includeAllGeneric').addClass('vis').removeClass('hid');
	}
	else {
		$j('#subfamilyGeneric').addClass('hid').removeClass('vis');
		$j('#subfamilyName').addClass('vis').removeClass('hid');
		
		$j('#includeAllGeneric').addClass('hid').removeClass('vis');
		$j('#includeAllCheckbox').addClass('vis').removeClass('hid');
		$j('#includeAllSpan').addClass('vis').removeClass('hid');
	}
}