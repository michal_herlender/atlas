/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	CallOffItems.js
*	DESCRIPTION		:	Page-specific javascript file.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 
*						
*	KNOWN ISSUES	:	-
*	HISTORY			:	05/11/2007 - JV - Created File
*					:	14/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

function newJob(button, coid)
{
	$j('input:button').hide();
	
	$j(button).parent().find('li').each(
		function (i, n)
		{
			var id = $j(n).attr('id');
			$j(n).prepend('<input type="checkbox" value="' + id + '" name="itemIds" />');
		}
	);
	
	$j(button).parent().find('.cancel').show();
	$j('input:submit').show();
	
	$j('#coid').val(coid);
}

// Checks or unchecks all the call off items for the specified subdivid 
function toggleAll(subdivid, isChecked) {
	var classSelector = '.checkbox_'+subdivid;
	$j(classSelector).each(function(){
		if($j(this).checked != isChecked) {
			$j(this).click();
		}
	});
}
