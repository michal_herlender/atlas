var focusElement = 'coname';

/**
 * this method checks all inputs of type checkbox that are contained within the parentDiv parameter 
 * 
 * @param {Boolean} select true if boxes are to be checked and vice versa
 * @param {String} parentDiv id of div in which to search for inputs of type checkbox
 */
function selectAllItems(select, parentDiv)
{
	// check all
	if(select == true)
	{
		$j('#' + parentDiv + ' input:checkbox').check('on');
	}
	// uncheck all
	else
	{
		$j('#' + parentDiv + ' input:checkbox').check('off');
	}
}
