/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	JIPAT.js
*	DESCRIPTION		:	Page-specific javascript file for JIPAT.js
*	DEPENDENCIES	:   -
*
*	TO-DO			: 
*	KNOWN ISSUES	:	-
*	HISTORY			:	22/03/2010 - JV - Created File
*
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array ('script/thirdparty/jQuery/jquery.scrollTo.js');

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{
	// select the correct subnavigation link
	$j('#linkpat').addClass('selected');
	// user preference to use job item key navigation
	if (userPrefs.jikeynav)
	{
		// initialise key nav to move between job items
		initialiseKeyNavForJI();
	}
}	

function printPATLabel(anchor, template, plantid, serialno, date) {
	template = template.substring(5);
	// Using Alligator
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("POST", "http://localhost:58058/service/printLabel", false);
    xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlHttp.send('{"plantId":"' + plantid + '","serial":"' + serialno + '","date":"' + date + '","templateKey":"' + template + '","request":"patlabel"}');
}	

/**
 * this method calls a dwr service to print a small certificate label.
 * 
 * @param {Object} anchor anchor that has been clicked
 * @param {String} template name of template to use
 * @param {Integer} certlinkid id of the cert link
 */
function downloadPATLabel(anchor, template, plantid, serialno, date)
{
	template = template.substring(5);
	document.location.replace("patlabel?plantId="+plantid+"&templateKey="+template+"&serial="+serialno+"&date="+date);
}