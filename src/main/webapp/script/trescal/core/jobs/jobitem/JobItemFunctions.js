/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	JobItemFunctions.js
*	DESCRIPTION		:	Imported on all pages in the jobitem directory.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-						
*	KNOWN ISSUES	:	-
*	HISTORY			:	12/10/2007 - JV - Created File
*					:	20/10/2015 - TProvost - Manage i18n
*					:	20/10/2015 - TProvost - Minor fix on enable buttons
*					:	20/10/2015 - TProvost - Fix bug in new version of Impromptu.js - Replace the "callback" property (property disappeared) by the "submit" property
*												=> Changes made in function confirmNewBarcodeScan
*					:	21/10/2015 - TProvost - Minor fix in function createWorkRequirement and updateJIWorkRequirement if department is null
*					:	10/05/2016 - TProvost - Get translatedTitle of nominalcode
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'ul#tabnav a', corner: 'small transparent top' },
								{ element: 'div.infobox', corner: 'normal' },
								{ element: 'ul.subnavtab a', corner: 'small transparent top' } );
		  					   
/**
 * array of calendar objects containing the initial setup literals needed
 * 
 * @param inputField (String) id of the input field to which the calendar should be binded
 * @param dateFormat (String) format that the date should be displayed
 * @param displayDates (String) 'all' make all dates available
 * 								'future' show only dates in the future
 * 								'past' show only dates in the past
 * applied in init() function.
 */	
if (typeof jqCalendar == 'undefined') 
{
	var jqCalendar = new Array();
}

function changeItem(newItemId)
{
	var pageURL = window.location.href.split('?')[0];
	if(pageURL.indexOf("completecalibration") < 0)
	{
		window.location.href = pageURL + '?jobitemid=' + newItemId + '&scrollto=' + $j(window).scrollTop();
	}
	else
	{
		window.location.href = "jicalibration.htm?jobitemid=" + newItemId;
	}
}

/**
 * variable used to flag whether the user has pressed the left or right arrow cursor keys
 */
var arrowKeyPress = false;
var keypressed = 0;

function initialiseKeyNavForJI()
{
	// scroll figure not empty?
	if (scrollto != '')
	{
		// scroll the page to correct location
		$j.scrollTo(scrollto, 800);
	}
	// initialise key nav for job items
	$j(document).keydown(function(e) 
	{				
		// ctrl key is being pressed
		if ((e.ctrlKey && $j('div.overlay_wrapper').length == 0))
		{
			// re-initialise variable each time ctrl key is pressed
			arrowKeyPress = false;		
			keypressed = 0;
			switch (e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0)
			{			
				// left arrow key has been pressed, just stop ajax from being initiated if pressed accidentally
				case 37: $j('input[type="button"]#jiPrevButton').trigger('onclick');
						 keypressed = event;
						 // set variable to indicate that the right arrow cursor has been pressed
						 arrowKeyPress = true;	
						 break;
						 
				// right arrow key has been pressed, just stop ajax from being initiated if pressed accidentally
				case 39: $j('input[type="button"]#jiNextButton').trigger('onclick');
						 keypressed = event;
						 // set variable to indicate that the right arrow cursor has been pressed
						 arrowKeyPress = true;
						 break;		
			}
		}		
	// capture keyup events and if ctrlkey then perform actions
	}).keyup( function(e){
		
		// ctrl key has been released and an arrow cursor key has been pressed so trigger event
		if (e.ctrlKey && arrowKeyPress == true)
		{
			switch (e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0)
			{			
				// left arrow key has been pressed, just stop ajax from being initiated if pressed accidentally
				case 37: $j('input[type="button"]#jiPrevButton').trigger('onclick');						 
						 // set variable to indicate that the right arrow cursor has been pressed
						 arrowKeyPress = false;	
						 break;
						 
				// right arrow key has been pressed, just stop ajax from being initiated if pressed accidentally
				case 39: $j('input[type="button"]#jiNextButton').trigger('onclick');
						 keypressed = event;
						 // set variable to indicate that the right arrow cursor has been pressed
						 arrowKeyPress = false;
						 break;		
			}
		}

	});	 
}

/**
 * this method loads the scrap instrument utility javascript and calls the appropriate method to execute
 * 
 * @param {Boolean} scrap indicates if we want to scrap or unscrap the instrument
 * @param {Integer} plantid the id of the instrument to be scrapped (set to status B.E.R.)
 * @param {Integer} jiid job item from which the instrument has been scrapped or null if done from instrument page
 */
function callConfirmScrapInstrumentChange(scrap, plantid, jiid)
{
	// load javascript service file
	loadScript.loadScriptsDynamic(new Array('script/trescal/core/instrument/ScrapInstrumentUtility.js'), true,
	{
		callback: function()
		{
			confirmScrapInstrumentChange(scrap, plantid, jiid);
		}
	});
}


/**
 * this method exports the item data to a text file which is used by labview
 * 
 * @param {Integer} jobItemId the job item id
 */
function exportLabviewData(jobItemId)
{
	// load javascript service file
	loadScript.loadScriptsDynamic(new Array('dwr/interface/jobitemservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to export job item info to labview text file
			jobitemservice.ajaxExportItemDataToLabview(jobItemId, 
			{
				callback:function(result)
				{
					// export successful
					if(result.success)
					{
						// use impromptu to inform user of result
						$j.prompt(i18n.t("core.jobs:jobItem.labviewDataExportedSuccessfully", "Your labview data has been exported successfully"));
					}
					else
					{
						// use impromptu to inform user of result
						$j.prompt(i18n.t("core.jobs:jobItem.labviewDataExportedError", "An error occured exporting your labview data"));
					}
				}
			});
		}
	});
}

/**
 * this method shows the change instrument elements
 * 
 * @param {Integer} jobitemId the id of the job item to change
 */
function confirmNewBarcodeScan(jobitemId,personid)
{	
	// create variable for message text
	var messagetext = i18n.t("core.jobs:jobItem.confirmChangeInstr", "Are you sure that you wish to change the instrument for this job item?");
	// initial prompt for user
	$j.prompt(messagetext,
	{ 
		submit: confirmcallback,
		buttons: 
		{ 
			Ok: true,
			Cancel: false 
		}, 
		focus: 1 
	});
	// callback method for initial prompt
	function confirmcallback(v,m)
	{
		// user confirmed action?
    	if (m)
		{
    		// create content for overlay
    		var content = 	'<div id="changeJIInstOverlay" class="overflow">' +
								'<fieldset>' +
									'<ol>' +
										'<li>' +
											'<label>' + i18n.t("core.jobs:jobItem.newBarcodeLabel", "New Barcode:") + '</label>' +
											'<input type="text" id="newbarcode" onkeypress=" if (event.keyCode==13){ changeInstrum(' + jobitemId + ', this.value,'+ personid + '); } " />	' +
										'</li>' +
									'</ol>' +
								'</fieldset>' +
							'</div>';
    		// create new overlay
    		loadScript.createOverlay('dynamic', i18n.t("core.jobs:jobItem.changeJobItemInstr", "Change Job Item Instrument"), null, content, null, 16, 620, 'newbarcode');
		}
	}
}

/**
 * this method changes the job item instrument
 * 
 * @param {Integer} jobitemid id of the job item
 * @param {String} barcode new instrument barcode 
 */
function changeInstrum(jobitemid, barcode,personid)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/jobitemservice.js'), true,
	{
		callback: function()
		{
			jobitemservice.changeJobItemInst(jobitemid, barcode,personid,
			{
				callback:function(result)
				{
					// close overlay
					loadScript.closeOverlay();
					// change instrument successful?
					if(result.success)
					{
						// reload the page
						location.reload(true);
					}
					else
					{
						// show error message to user
						$j.prompt(result.message);
					}
				}	
			});
		}
	});	
}

/**
 * this method tries to find the best matching nominal code for the cost type and job item
 * selected.
 * 
 * @param {Integer} costTypeId the id of selected cost type
 * @param {Integer} jobItemId the id of selected job item
 */
function findNominal(itemIndex, costTypeId, jobItemId)
{

	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/nominalcodeservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to find best matching nominal code
			nominalcodeservice.findAjaxBestMatchingNominalCode(ledgers, costTypeId, jobItemId,
			{
				callback:function(results)
				{
					// successful?
					if(results.success)
					{
						var nom = results.results;
						$j('#po_item_nominal' + itemIndex).val(nom.id);
						//$j('#suggestednominal').empty().append('Suggested Nominal: ' + nom.code + ': ' + nom.title + ' &nbsp;<a href="#" class="mainlink" onclick=" $j(\'#nominalId\').val(' + nom.id + '); return false; ">Switch</a>');
					}
					else
					{
						//alert('none found');
						//$j('#suggestednominal').empty().append('Suggested Nominal: No matches found');
					}
				}
			});
		}
	});
}

/**
 * this method selects all checkboxes within a div that has the supplied id and with the name provided.
 * 
 * @param {Boolean} select indicates if the checkboxes should be checked or un-checked
 * @param {String} name the name of the checkboxes we want to check
 * @param {String} parentDiv the id of the div in which we want to check checkboxes
 */
function selectAllItems(select, name, parentDiv)
{
	// check all
	if(select)
	{
		$j('#' + parentDiv + ' input[type="checkbox"][name="' + name + '"]').check('on');
	}
	// uncheck all
	else
	{
		$j('#' + parentDiv + ' input[type="checkbox"][name="' + name + '"]').check('off');
	}
}

function workRequirementCatalog(jobItemId, titleAction) {
	var contentUri =  'workrequirementlookup.htm?jobItemId='+jobItemId;
	// create new overlay
	loadScript.createOverlay('external', titleAction, contentUri, null, null, 90, 960, null);
}

/**
 * this method creates content to be applied in a new overlay for creating an
 * item work requirement.
 * 
 * @param {String} type the type of request made (i.e. 'Add' or 'Edit')
 * @param {Integer} jobItemId the id of this job item
 * @param {Integer} wrId the id of the work requirement if we are editing otherwise null
 */
function addEditWorkRequirement(type, jobItemId, wrId, modelId)
{
	var titleAction = '';
	if (type == 'Add') { titleAction = i18n.t("core.jobs:jobItem.addWorkRequirement", "Add Work Requirement"); }
	else { titleAction = i18n.t("core.jobs:jobItem.editWorkRequirement", "Edit Work Requirement"); }	
	var contentUri =  'workrequirementoverlay.htm?jobItemId='+jobItemId+'&modelId='+modelId;
	// job item work instruction id passed?
	if (wrId != null) {
		contentUri = contentUri + '&wrId='+wrId; 
	}
	// create new overlay
	loadScript.createOverlay('external', titleAction, contentUri, null, null, 80, 820, null);
}

/**
 * this method allows you to delete a work requirement as long as the work requirement
 * has not been completed and that it is not the job items next work requirement.
 * 
 * @param wrId id of the work requirement to be deleted
 * @param jobItemId id of the job item that the work requirement is on
 */
function deleteWorkRequirement(wrId, jiwrId, jobItemId)
{
	// load javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/workrequirementservice.js'), true,
	{
		callback:function()
		{
			// call dwr service to delete work requirement
			workrequirementservice.ajaxDeleteWorkRequirement(wrId, jobItemId, 
			{
				callback:function(result)
				{
					if(result.success)
					{
						// remove the job item work requirement from page
						$j('div#jiWorkReq' + jiwrId).remove();
						// reload the current page
						window.location.href = window.location.href;
					}
					else
					{
						// show error message
						$j.prompt(result.message);
					}
				}
			});
		}
	});
}

/**
 * this method sets a work requirement as the current job items next work requirement so long as
 * 
 * @param jobItemId
 * @param jiwrId
 * @return
 */
function setJobItemNextWorkRequirement(jobItemId, jiwrId)
{
	// load javascript service file
	loadScript.loadScriptsDynamic(new Array('dwr/interface/jobitemservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to update next work requirement for job item
			jobitemservice.ajaxSetJobItemNextWorkRequirement(jobItemId, jiwrId,
			{
				callback: function(result)
				{
					if (result.success) {
						// workflow should advance, reload page
						location.reload();
					}
					else {
						// show error message to user
						$j.prompt(result.message);
					}
				}
			});
		}
	});
}

/**
 * this method toggles the complete property of a job item work requirement. If it is complete then it will be set
 * to incomplete and vice versa
 * 
 * @param {Integer} jiwrId id of the job item work instruction to toggle
 */
function toggleCompleteWorkRequirement(jiwrId)
{
	// load javascript service file
	loadScript.loadScriptsDynamic(new Array('dwr/interface/jobitemworkrequirementservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to toggle job item work requirement complete
			jobitemworkrequirementservice.toggleCompleteJobItemWorkRequirement(jiwrId,
			{
				callback: function(result)
				{
					// complete toggle successful?
					if (result.success)
					{
						// assign job item work requirement to variable
						var jiwr = result.results;
						// is complete?
						if (jiwr.workRequirement.complete)
						{
							// change class of work requirement complete anchor so that tick is displayed and change the title attribute
							$j('div#jiWorkReq' + jiwr.id + ' div.actions a:first').removeClass().addClass('workReqCompleted').attr('title', i18n.t("core.jobs:jobItem.workRequirementCompleted", "Work Requirement Completed"));	
							// is the delete anchor currently visible?
							if ($j('div#jiWorkReq' + jiwr.id + ' div.actions a.workReqDelete').hasClass('vis'))
							{
								// delete anchor is visible so we should hide it because we should not be able to delete a completed job item work requirement
								$j('div#jiWorkReq' + jiwr.id + ' div.actions a.workReqDelete').removeClass('vis').addClass('hid');
							}
						}
						else
						{
							// change class of work requirement complete anchor so that a cross is displayed and change the title attribute
							$j('div#jiWorkReq' + jiwr.id + ' div.actions a:first').removeClass().addClass('workReqNotCompleted').attr('title',  i18n.t("core.jobs:jobItem.workRequirementNotCompleted", "Work Requirement Not Completed"));
							// is the delete anchor currently hidden?
							if ($j('div#jiWorkReq' + jiwr.id + ' div.actions a.workReqDelete').hasClass('hid'))
							{
								// is this job item work requirement currently set as the next job item work requirement?
								if (!$j('div#jiWorkReq' + jiwr.id + ' a:first').hasClass('nextWorkReq'))
								{
									// this is not the next job item work requirement and it is currently hidden so make is visible
									$j('div#jiWorkReq' + jiwr.id + ' div.actions a.workReqDelete').removeClass('hid').addClass('vis');
								}
							}
						}
					}
					else
					{
						// an error occured, display it
						$j.prompt(result.message);
					}
				}
			});
		}
	});
}

/**
 * this method loads all standards used for a calibration and displays them in a thickbox
 * where the user can delete current standards or add further standards.
 * 
 * @param {Integer} calId id of the calibration to load standards for
 */
function loadAddRemoveStandards(calId)
{
	// load javascript service file
	loadScript.loadScriptsDynamic(new Array('dwr/interface/standardusedservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to get all standards used for calibration
			standardusedservice.ajaxGetStandardsUsedForCal(calId,  
			{
				callback:function(standards)
				{		
					// create content to be displayed
					var content = 	'<div id="standardsOverlay">' +										
										'<div id="stdMsg" class="hid warningBox1">' +
											'<div class="warningBox2">' + 
												'<div class="warningBox3">' + 
												'</div>' + 
											'</div>' + 
										'</div>' + 
										'<table id="stdsTable" class="default4">' +
											'<thead>' +
												'<tr>' +
													'<th>' + i18n.t("barcode", "Barcode") + '</th>' + 
													'<th>' + i18n.t("instrument", "Instrument") + '</th>' + 
													'<th>' + i18n.t("remove", "Remove") + '</th>' + 
												'</tr>' +
											'</thead>' +
											'<tbody>';
												$j.each(standards, function(i, std)
												{
													content +=	'<tr id="instStd' + std.id + '">' +
																	'<td>' + std.inst.plantid + '</td>' +
																	'<td>' +
																		std.inst.definitiveInstrument + '<br/>' +
																		i18n.t("serialNoLabel", "Serial No:") + ' ' + std.inst.serialno + '<br/>' +
																		i18n.t("plantNoLabel", "Plant No:") + ' ' + std.inst.plantno +
																	'</td>' +
																	'<td><a class="mainlink" href="#" onclick=" removeStd(' + std.id + '); return false; ">' + i18n.t("remove", "Remove") + '</a></td>' +
																'</tr>';
												});
								content +=	'</tbody>' +
										'</table>' +
										'<input type="hidden" id="calibrationId" value="' + calId + '" />' +
										'<p>' + i18n.t("core.jobs:jobItem.scanBarcode", "Scan barcode of instrument standard to add:") + ' </p><input type="input" type="text" id="instStdId" onkeypress=" if (event.keyCode==13){ $j(\'#addStdButton\').trigger(\'onclick\'); }" />' +
										'<input id="addStdButton" type="button" value="' + i18n.t("add", "Add") + '" onclick=" addStd($j(\'input#instStdId\').val(), ' + calId + '); return false; " /> ' +
										'&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" value="' + i18n.t("close", "Close") + '" onclick=" loadScript.closeOverlay(); " />' +									
									'</div>';
					// create overlay
					loadScript.createOverlay('dynamic', i18n.t("core.jobs:jobItem.addOrRemoveStandardsForThisCal", "Add/Remove Standards For This Calibration"), null, content, null, 40, 680, null);
					// focus on po number input
					setTimeout('$j(\'div#standardsOverlay input#instStdId\').focus()', 1500);
				}
			});
		}
	});
}

/**
 * this method deletes a standard used on a calibration
 * 
 * @param {Integer} standardId id of the standard used record
 */
function removeStd(standardId)
{
	
	$j.ajax({
		url: "deletestandardusedoncalibration.json",
		data: {
			standardId: standardId
		},
		async: true
	}).done(function(result) {
		if(!result.success)
		{
			// add error message and un-hide
			$j('div#stdMsg').removeClass('hid').find('div.warningBox3').html('<div>' + result.message + '</div>');
		}
		else
		{
			// remove any old error messages
			$j('div#stdMsg').addClass('hid').find('div.warningBox3').html('');
			// remove from table
			$j('table#stdsTable tr#instStd' + standardId).remove();
			// set stds as changed
			stdsChanged = true;
		}
		// focus on barcode scanner ready for next item
		$j('div#standardsOverlay input#instStdId').val('').focus();
	});
	
}

/**
 * this method adds a standard to a calibration
 * 
 * @param {Integer} plantId id of the calibration standard instrument to add
 * @param {Integer} calId id of the calibration
 */
function addStd(plantId, calId)
{
	$j.ajax({
		url: "addstandardusedoncalibration.json",
		data: {
			plantId: plantId,
			calId : calId
		},
		async: true
	}).done(function(result) {
		if(!result.success)
		{
			// add error message and un-hide
			$j('div#stdMsg').removeClass('hid').find('div.warningBox3').html('<div>' + result.message + '</div>');
		}
		else
		{
			// remove any old error messages
			$j('div#stdMsg').addClass('hid').find('div.warningBox3').html('');						
			// unwrap instrument
			var std = result.results;						
			// add to table
			$j('table#stdsTable').append(
				'<tr id="instStd' + std.standardUsedId + '">' +
					'<td>' + std.plantId + '</td>' +
					'<td>' + 
						std.definitiveInstrument + '<br/>' +
						i18n.t("serialNoLabel", "Serial No:") + ' ' + std.serialNumber + '<br/>' +
						i18n.t("plantNoLabel", "Plant No:") + ' ' + std.plantNumber +									
					'</td>' +
					'<td><a class="mainlink" href="#" onclick=" removeStd(' + std.standardUsedId + ', ' + calId + '); return false; ">' + i18n.t("remove", "Remove") + '</a></td>' +
				'</tr>'
			);
			
			// set stds as changed
			stdsChanged = true;
		}					
		// focus on barcode scanner ready for next item
		$j('div#standardsOverlay input#instStdId').val('').focus();
		
	});
	
}

/**
 * if any calibration standards have been added or removed from a calibration then this method will update the
 * page and reflect those changes.
 * 
 * @param {Integer} calId id of the calibration which has been changed
 */
function updateCalStandardsOnPage(calId)
{
	// load javascript service file
	loadScript.loadScriptsDynamic(new Array('dwr/interface/standardusedservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to get all standards used for calibration
			standardusedservice.ajaxGetStandardsUsedForCal(calId,  
			{
				callback:function(standards)
				{
					// content variable
					var content = '';
					// standards returned?
					if (standards.length > 0)
					{
						// add each standard to new content for page
						$j.each(standards, function(i, std)
						{
							content += 	'<tr>' +
											'<td>' +
												'<a href="/viewinstrument.htm?plantid=' + std.inst.plantid + '" class="mainlink">' + std.inst.plantid + '</a>' +
											'</td>' +
											'<td>' +
												std.inst.definitiveInstrument + '<br />' +
												i18n.t("serialNoLabel", "Serial No:") + ' ' + std.inst.serialno + '<br/>' +
												i18n.t("plantNoLabel", "Plant No:") + ' ' + std.inst.plantno +
											'</td>' +
										'</tr>';
						});
					}
					else
					{
						content += 	'<tr>' +
										'<td colspan="2" class="center bold">' + i18n.t("core.jobs:jobItem.noStandardsForThisCal", "There are no standards for this calibration") + '</td>' +
									'</tr>';
					}
					// append content to page
					$j('div#calStandards' + calId + ' table tbody').empty().append(content);
					// update number of standards displayed
					$j('span.calStandardsSize' + calId).text(standards.length);
				}
			});
		}
	});
}

/**
 * this method returns the list of standards for a calibration as a string and copies it to the clipboard
 * so that the user can paste this into the word document if necessary
 * 
 * @param img the clipboard icon that was clicked to copy the list
 * @param calId the calibration ID to get the list of standards for
 */
function copyListOfStandardsToClipboard(img, calId)
{
	// load javascript service file
	loadScript.loadScriptsDynamic(new Array('dwr/interface/standardusedservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to retrieve the standards as string
			standardusedservice.ajaxGetStandardsUsedAsTabbedString(calId,
			{
				callback:function(str)
				{
					// call jquery clipboard function to copy this string
					$j(img).clipBoard(img, str, false, null);
				}
			});
		}
	});
}

function manageRepairTime(rcrId, jobitemId)
{
	var titleAction = i18n.t("core.jobs:jobItem.managerepairtime", "Entering production times Repair");
	var contentUri =  'managerepairtimeoverlay.htm?jobitemid='+jobitemId+'&rcrid='+rcrId;
	
	// create new overlay
	loadScript.createOverlay('external', titleAction, contentUri, null, null, 80, 820, null);
}
