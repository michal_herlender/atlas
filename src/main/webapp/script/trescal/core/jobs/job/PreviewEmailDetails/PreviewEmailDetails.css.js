import { css } from "../../../../../thirdparty/lit-element/lit-element.js";

export const styling = css`
  .editable :read-write {
    background-color: white;
    border: #eee 2px solid;
    border-style: inset;
    cursor: text;
  }
  .editable div:read-only::after {
    content: url("img/icons/edit_small.png");
  }

  .loader {
    display: flex;
    flex-direction: column;
    justify-content: center;
    height: 150px;
    align-items: center;
  }
`;
