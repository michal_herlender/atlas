import {
  LitElement,
  html,
  css
} from "../../../../../thirdparty/lit-element/lit-element.js";
import {
  previewEmailDetailsOverlay,
  recipientTemplate,
  ccPartTemplate,
  attachmentTemplate,
  attachmentsTemplate
} from "./PreviewEmailDetails.html.js";
import {styling} from "./PreviewEmailDetails.css.js";
import { debounce } from "../../../../../tools/Debounce.m.js";
import {sendJobItemFeedbackEmail, SendJobItemFeedbackEmailContext} from "../SendJobItemFeedbackEmail.js";
import CwmsTranslate  from "../../../../../components/cwms-translate/cwms-translate.js"

export const STATE_GENERATE ="GENERATE";
export const STATE_EDIT ="EDIT";
class PreviewEmailDetails extends LitElement {
  static get propeties() {
    return {
      emailWrapper: { type: Object, attribute: false },
      sendButtonCallback: { type: Object, attribute: false },
      systemCompId: { type: Number },
      currentConId: { type: Number },
      state: {type:String, attribute:false},
      warningMessage: { type: String, attribute: false }
    };
  }

  set emailWrapper(value) {
    const oldValue = this.emailWrapper;
    this._emailWrapper = value;
     this.editor.setData(value.body);
    this.requestUpdate("emailWrapper", oldValue);
          setInterval(_ => {this.emailWrapper.body = this.editor.getData();
         this.requestUpdate()},2000);
  }

  get emailWrapper() {
    return this._emailWrapper;
  }

  static get styles(){
    return styling;
  }

  constructor(){
    super();
    this.state = STATE_EDIT;
    CwmsTranslate.subscribeMessage("viewjob.generatingAndSendingFeedbackEmail",viewjob.generatingAndSendingFeedbackEmail
    ,(message) => this.sendMessage = message);
    CwmsTranslate.subscribeMessage("cancel","Cancel",(message)=>this.cancelMessage=message);
  }

  firstUpdated(){
    super.firstUpdated();
    const rt = this.querySelector("[slot='rich-text'] textarea");
    this.editor = CKEDITOR.replace(rt,{allowedContent :true,startupFocus: true});
  }

  show() {
    const overlay = this.shadowRoot.querySelector("cwms-overlay");
    overlay && overlay.show();
  }

  hide() {
    if (this.shadowRoot.querySelector("cwms-overlay").open)
      this.shadowRoot.querySelector("cwms-overlay").hide();
  }

  updated(cp){
    super.updated(cp);
      this.emailSubject = this.shadowRoot.querySelector(".subject");
  }

  buttonSendClicked(event) {
    this.dispatchEvent(new CustomEvent("send-button-clicked"));
    const {email:{recipients,entityId}} = this.emailWrapper;
    this.state = STATE_GENERATE;
    this.requestUpdate();
    sendJobItemFeedbackEmail(SendJobItemFeedbackEmailContext.createInstanceWithRecipientsAndContent(
      recipients,this.editor.getData(),this.emailSubject.textContent.trim(),entityId
    ))
    .then(this.processResponse.bind(this))
    .catch(message => {
      this.state=STATE_EDIT;
      this.warningMessage = message;
    })
    ;
  }

  processResponse(emailResult) {
    const {email:{entityId}} = this.emailWrapper;
      loadScript.populateEntityEmail(
        emailResult.email.id,
        this.currentConId,
        this.systemCompId,
        entityId,
      );
      document.getElementById("email-link").click();
      this.hide();
      this.state ="";
      this.dispatchEvent(new CustomEvent("send-completed"));
  }
  renderToReciepients(email) {
    return email.recipients
      .filter(r => r.type == "EMAIL_TO")
      .map(recipientTemplate);
  }

  renderCCRecipientsPart(email) {
    const recipients = email.recipients
      .filter(r => r.type == "EMAIL_CC")
      .map(recipientTemplate);
    if (recipients.length == 0) return html``;
    return ccPartTemplate({ recipients });
  }


  updateSubject(event){
    this.emailWrapper.subject = this.emailSubject.textContent;
  }


  toggleEditable(event){
    event.target.contentEditable =true;
  }

  renderAtachments(attachments) {
    if (!attachments) return html``;
    const renderedAttachments = attachments
      .map(AttachmentDto.fromList)
      .map(attachmentTemplate);
    return attachmentsTemplate({ attachments: renderedAttachments });
  }

  render() {
    if (!this.emailWrapper) return html``;
    const title = html`
      <span slot="cwms-overlay-title"
        >${i18n.t("core.system:mail.viewEmailDetails", {
          varSubject: this.emailWrapper.email.subject,
          defaultValue: "Preview Email" 
        })}</span
      >
    `;
    const context = {
      ...this.emailWrapper,
      state: this.state,
      onSend: this.buttonSendClicked,
      emailTitle: title,
      onCancel: this.hide,
      cancel: this.cancelMessage,
      sendMessage: this.sendMessage,
      warningMessage: this.warningMessage,
      toRecipients: this.renderToReciepients(this.emailWrapper.email),
      ccRecipients: this.renderCCRecipientsPart(this.emailWrapper.email),
      attachments: this.renderAtachments(this.emailWrapper.attachments),
      toggleEditable:this.toggleEditable,
      updateSubject:debounce(this.updateSubject.bind(this),500),
    };
    return previewEmailDetailsOverlay(context);
  }
}

customElements.define("cwms-preview-email-details", PreviewEmailDetails);

class AttachmentDto {
  static fromList(list) {
    if (list.length < 2) return null;
    return new AttachmentDto(list[0], list[1]);
  }

  constructor(file, fileName) {
    this.file = file;
    this.fileName = fileName;
  }
}
