import { html } from "../../../../../thirdparty/lit-element/lit-element.js";
import { STATE_GENERATE } from "./JobItemFeedback.js";
import "../../../../../components/search-plugins/cwms-email-search/cwms-email-search.js"

export const jobItemFeedbackTemplate = context => html`
  <link rel="stylesheet" href="styles/theme/theme.css" />
  <link rel="stylesheet" href="styles/structure/structure.css" />
  <link rel="stylesheet" href="styles/structure/elements/table.css" />
  <link rel="stylesheet" href="styles/text/text.css" />
  <cwms-overlay @cwms-overlay-closed="${context.cancel}">
    <cwms-translate slot="cwms-overlay-title" code="requestClientFeedback"
      >Request Client Feedback</cwms-translate>
    ${jobItemFeedbackOverlayBody(context)}
  </cwms-overlay>
`;

const jobItemFeedbackWarning = context => html`
  <div class="warningBox1">
    <div class="warningBox2">
      <div class="warningBox3">${context.warningMessage}</div>
    </div>
  </div>
`;

const jobItemFeedbackMessage = context => html`
  <div class="actionBox1">
    <div class="actionBox2">
      <div class="actionBox3">${context.message}</div>
    </div>
  </div>
`;

export const jobItemFeedbackOverlayBody = context =>
  context.state == STATE_GENERATE
    ? html`
        <div slot="cwms-overlay-body" class="loader">
          <img src="img/icons/ajax-loader.gif" width="31" height="31" alt="" />
          <h4>${context.message}</h4>
        </div>
      `
    : html`
        <div slot="cwms-overlay-body">
          <div id="feedbackReqOverlay" class="marg-bot">
            ${context.warningMessage ? jobItemFeedbackWarning(context) : ""}
            ${context.jobitems && context.jobitems.length > 0
              ? jobItemFeedbackItems(context)
              : ""}
          </div>
        </div>
      `;
const jobItemFeedbackItems = context => html`
    <fieldset>
      <ol>
        <li class="email-row">
        <div class="email-to">
          <label>
            <cwms-translate code="system.to">To:</cwms-translate>
            (<a
              role="button"
              @click=${context.initializeCCEmailSearchPlugin} 
              class="mainlink"
            >
              <cwms-translate code="system.cc">Cc</cwms-translate> </a
            >)</label
          >
          <div class="value-options">
              <cwms-email-search coid="${context.coid}" namePrefix="to" .emailList=${context.toEmailList} 
              @selected=${context.updateToList} @deselected=${context.updateToList}
              ></cwms-email-search>
          </div>
          </div>
          <div class="include-self">
							<input type="checkbox" name="includeSeflInReplay" value="true" 
              ?checked=${context.includeSelfInCarbonCopy}
              @change=${context.includeSelfChange}
               />
							<cwms-translate code="system.includeSelfInCarbonCopy">Include self in carbon copy</cwms-translate> (<em>${context.currentContact}</em>)
						</span>
          </div>
        </li>
        <li class="email-row ${!context.showCC && 'hid'}">
        <div class="email-cc">
          <label><cwms-translate code="system.cc">Cc</cwms-translate>:</label>
          <div class="value-options">
							<cwms-email-search coid="${context.coid}" namePrefix="cc" .emailList=${context.ccEmailList} ></cwms-email-search>
          </div>
          </div>
          <div></div>
        </li>
      </ol>
    </fieldset>
    <table
      class="default4 clientfdbkitems"
      summary="This table displays all items on the job"
    >
      <thead>
        <tr>
          <td colspan="6">
            <cwms-translate code="viewjob.jboItemsAvailableForFeedback">Job items available for feedback</cwms-translate>(${context.jobitems.length})
          </td>
        </tr>
        <tr>
          <th class="add" scope="col">
            <input
              type="checkbox"
              id="selectAll"
              @change=${context.selectAllItems}
            />
            <cwms-translate code="all">All</cwms-translate>
          </th>
          <th class="item" scope="col">
            <cwms-translate code="item">Item</cwms-translate>
          </th>
          <th class="inst" scope="col">
            <cwms-translate code="instrument">Instrument</cwms-translate>
          </th>
          <th class="serial" scope="col">
            <cwms-translate code="serialNo">Serial No</cwms-translate>
          </th>
          <th class="plant" scope="col">
            <cwms-translate code="plantNo">Plant No</cwms-translate>
          </th>
          <th class="caltype" scope="col">
            <cwms-translate code="calType">Cal Type</cwms-translate>
          </th>
        </tr>
      </thead>
    </table>
    ${context.renderJobItems(context.jobitems)}
    <div class="text-center">
      <p>
        <input type="checkbox" name="sendWOReview" @change=${
          context.noReviewChange
        }/>
        <cwms-translate code="viewjob.sendRequestFeedback"
          >Send to job contact without review</cwms-translate>
      </p>
      <button @click=${context.generateClientFdbkReq}>
        <cwms-translate code="viewjob.reqjobitfeed">Request Feedback</cwms-translate>
      </button>
    </div>
  </div>
`;

export const jobItemTemplate = context => {
  const {
    jobItem,
    feedbackDto: { selected },
    feedbackDto
  } = context;
  return html`
    <link rel="stylesheet" href="styles/theme/theme.css" />
    <link rel="stylesheet" href="styles/structure/structure.css" />
    <table
      class="default4 clientfdbkitems"
      summary="This table display a job item"
    >
      <tbody>
        <tr style=${`background:${jobItem.displayColor};`}>
          <td class="add">
            <input
              type="checkbox"
              name="reqfdbk"
              value=${jobItem.jobItemId}
              @click=${context.onSelect}
              ?checked=${selected}
            />
          </td>
          <td class="item">${jobItem.itemNo}</td>
          <td class="inst">${jobItem.modelName}</td>
          <td class="serial">${jobItem.serialNo}</td>
          <td class="plant">
            ${jobItem.plantNo == null ? "" : jobItem.plantNo}
          </td>
          <td class="caltype">
            ${jobItem.calTypeShortName}
          </td>
        </tr>
        ${selected
          ? html`
              <tr>
                <td colspan="6">
                  <div class="row">
                    <div class="row-element">
                      <slot name="slot-ji-${context.idx}"></slot>
                    </div>
                    <div class="row-element controls">
                      <div>
                        <input
                          type="checkbox"
                          name="confirmCalType"
                          ?checked=${feedbackDto.confirmCalType}
                          @change=${context.updateState}
                        />
                        <cwms-translate code="viewjob.confirmCalType">
                          Confirm Cal Type</cwms-translate>
                        <br />
                        <input
                          type="checkbox"
                          name="confirmTurnaround"
                          ?checked=${feedbackDto.confirmTurnaround}
                          @change=${context.updateState}
                        />
                        <cwms-translate code="viewjob.confirmTurnaround">
                          Confirm Turnaround</cwms-translate>
                      </div>
                      <div>
                        <input
                          type="checkbox"
                          name="confirmCalPoints"
                          ?checked=${feedbackDto.confirmCalPoints}
                          @change=${context.updateState}
                        />
                        <cwms-translate code="viewjob.confirmCalPoints">
                          Confirm Cal Points</cwms-translate>
                        <br />
                        <input
                          type="checkbox"
                          name="confirmReturn"
                          ?checked=${feedbackDto.confirmReturn}
                          @change=${context.updateState}
                        />
                        <cwms-translate code="viewjob.confirmItemReturn">
                          Confirm Item Return</cwms-translate>
                      </div>
                      <div>
                        <input
                          type="checkbox"
                          name="confirmAdjustment"
                          ?checked=${feedbackDto.confirmAdjustment}
                          @change=${context.updateState}
                        />
                        <cwms-translate code="viewjob.confirmAdjustment">
                          Confirm Adjustment</cwms-translate>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            `
          : html``}
      </tbody>
    </table>
  `;
};
export const noItemsTemplate = html`
  <tr class="bold center">
    <td colspan="6">
      <cwms-translate code="noItemsOnJob">No Items On Job</cwms-translate>
    </td>
  </tr>
`;
