/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	AccessoriesAndDefects.js
*	DESCRIPTION		:	Page-specific javascript file.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 
*						
*	KNOWN ISSUES	:	-
*	HISTORY			:	25/08/2009 - JV - Created File
*					:	15/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array ('script/thirdparty/jQuery/jquery.boxy.js', 
		'script/trescal/core/tools/labelprinting/dymo/DymoLabelFunctions.js');
								
/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{
	// load stylesheet using the dom if not already
	loadScript.loadStyle('styles/jQuery/jquery-boxy.css', 'screen');
	if(selectedItemId > 0) changeToItem(selectedItemId, selectedItemNo);
	else if(selectedGroupId > 0) changeToGroup(selectedGroupId);
	else {
		$j('table.addAccessory').addClass('hid');
		$j('div#accFreeText').html(i18n.t("core.jobs:job.accessoryNotesDisplayedHere", "Further accessory notes displayed here"));
	}
}

/**
 * this method displays all content for job item accessories
 * 
 * @param {Integer} jobItemId the current job item id
 * @param {Integer} jobItemNo the current job item number
 */
function changeToItem(jobItemId, jobItemNo)
{
	selectedItemId = jobItemId;
	selectedGroupId = 0;
	// clear all divs of text
	$j('div#accSpace, div#accFreeText').html('');
	// change headings for new group
	$j('span#accHeader').html(i18n.t("core.jobs:job.itemsaccessories", {jobItemNo: jobItemNo}));
	// show existing accessories on screen
	displayItemAccessories();
}

/**
 * this method displays all content for group accessories
 * 
 * @param {Integer} groupId the current group id
 */
function changeToGroup(groupId)
{
	selectedItemId = 0;
	selectedGroupId = groupId;
	// clear all divs of text
	$j('div#accSpace, div#accFreeText').html('');
	// change headings for new group
	$j('span#accHeader').html(i18n.t("core.jobs:job.groupsaccessories", {groupId: groupId}));
	// show existing accessories on screen
	displayGroupAccessories();
}

/**
 * this method displays all current accessories for an item
 */
function displayItemAccessories()
{
	// load item accessories
	$j.ajax({
		url: "showitemaccessories.htm",
		data: {
			jobItemId: selectedItemId
		},
		async: true
	}).done(function(savedAccessories) {
		// append content for accessories
		$j('div#accSpace').append(savedAccessories);
		// show add accessory
		$j('table.addAccessory').removeClass('hid');
	}).fail(function(result) {
		$j.prompt(result.responseText);
	});
	// load accessory free text
	$j.ajax({
		url: "showitemaccessoryfreetext.htm",
		data: {
			jobItemId: selectedItemId
		},
		async: true
	}).done(function(accessoryFreeText) {
		// empty free text box
		$j('div#accFreeText').html('');
		// append content for accessories
		$j('div#accFreeText').append(accessoryFreeText);
	}).fail(function(result) {
		$j.prompt(result.responseText);
	});
}

/**
 * this method displays all current accessories for a group
 */
function displayGroupAccessories()
{
	// load group accessories
	$j.ajax({
		url: "showgroupaccessories.htm",
		data: {
			groupId: selectedGroupId
		},
		async: true
	}).done(function(savedAccessories) {
		// append content for accessories
		$j('div#accSpace').append(savedAccessories);
		// show add accessory
		$j('table.addAccessory').removeClass('hid');
	}).fail(function(result) {
		$j.prompt(result.responseText);
	});
	// load accessory free text
	$j.ajax({
		url: "showgroupaccessoryfreetext.htm",
		data: {
			groupId: selectedGroupId
		},
		async: true
	}).done(function(accessoryFreeText) {
		// empty free text box
		$j('div#accFreeText').html('');
		// append content for accessories
		$j('div#accFreeText').append(accessoryFreeText);
	}).fail(function(result) {
		$j.prompt(result.responseText);
	});
}

/**
 * add an accessory
 * @param {Integer} accId accessory id
 * @param {Integer} quantity quantity
 */
function addAccessory(accId, quantity) {
	if(selectedItemId > 0) {
		$j.ajax({
			url: "additemaccessory.htm",
			data: {
				accessoryId: accId,
				jobItemId: selectedItemId,
				quantity: quantity},
			async: true
		}).done(function(newaccessoryline) {
			// is message of no accessories present
			if ($j('table.savedAccessoryList tbody tr:first td').length == 1) {
				// remove message row
				$j('table.savedAccessoryList tbody tr:first').remove();
				$j('span#printlabel').addClass('vis').removeClass('hid');
			}
			// append new accessory content to table
			$j('table.savedAccessoryList tbody').append(newaccessoryline);
			// update accessory count
			$j('span#accSize' + selectedItemId).text(parseInt($j('span#accSize' + selectedItemId).text()) + 1);
		}).fail(function(result) {
			$j.prompt(result.responseText);
		});
	}
	else if(selectedGroupId > 0) {
		$j.ajax({
			url: "addgroupaccessory.htm",
			data: {
				accessoryId: accId,
				groupId: selectedGroupId,
				quantity: quantity},
			async: true
		}).done(function(newaccessoryline) {
			// is message of no accessories present
			if ($j('table.savedAccessoryList tbody tr:first td').length == 1) {
				// remove message row
				$j('table.savedAccessoryList tbody tr:first').remove();
			}
			// append new accessory content to table
			$j('table.savedAccessoryList tbody').append(newaccessoryline);
			// update accessory count
			$j('span#accSize' + selectedGroupId).text(parseInt($j('span#accSize' + selectedGroupId).text()) + 1);
		}).fail(function(result) {
			$j.prompt(result.responseText);
		});
	}
}

/**
 * this method edits an accessory quantity
 */
function editQuantity(selectedRow) {
	var quantity = $j(selectedRow).find('td.qty').html();
	$j(selectedRow).find('td.qty').html('<input type="number" value=' + quantity + ' min="1"/>');
	$j(selectedRow).find('a.edit').addClass('hid');
	$j(selectedRow).find('a.save').removeClass('hid');
}

function saveQuantity(selectedRow) {
	var quantity = $j(selectedRow).find('td.qty > input').val();
	var accessoryId = $j(selectedRow).find('td.itemAccId').html();
	var saveURL = selectedItemId > 0 ? "saveitemaccessory.json" : "savegroupaccessory.json";
	$j.ajax({
		url: saveURL,
		data: {
			accessoryId: accessoryId,
			quantity: quantity},
		async: true
	}).done(function() {
		$j(selectedRow).find('td.qty').html(quantity);
		$j(selectedRow).find('a.edit').removeClass('hid');
		$j(selectedRow).find('a.save').addClass('hid');
	}).fail(function(result) {
		$j.prompt(result.responseText);
	});
}

/**
 * this method removes an item accessory
 * @param {Integer} itemAccessoryId id of item accessory
 */
function removeItemAccessory(itemAccessoryId, selectedRow) {
	$j.ajax({
		url: "removeitemaccessory.txt",
		data: {
			itemAccessoryId: itemAccessoryId},
		async: true
	}).done(function(noAccessoryString) {
		$j(selectedRow).remove();
		// update accessory count
		$j('span#accSize' + selectedItemId).text(parseInt($j('span#accSize' + selectedItemId).text()) - 1);
		if(noAccessoryString != '') {
			$j('table.savedAccessoryList tbody').append('<tr><td colspan="3">' + noAccessoryString + '</td></tr>');
			$j('span#printlabel').addClass('hid').removeClass('vis');
		}
	}).fail(function(result) {
		$j.prompt(result.responseText);
	});
}

/**
 * this method removes a group accessory
 * @param {Integer} groupAccessoryId id of group accessory
 */
function removeGroupAccessory(groupAccessoryId, selectedRow) {
	$j.ajax({
		url: "removegroupaccessory.txt",
		data: {
			groupId: selectedGroupId,
			groupAccessoryId: groupAccessoryId},
		async: true
	}).done(function(noAccessoryString) {
		$j(selectedRow).remove();
		// update accessory count
		$j('span#accSize' + selectedGroupId).text(parseInt($j('span#accSize' + selectedGroupId).text()) - 1);
		if(noAccessoryString != '')
			$j('table.savedAccessoryList tbody').append('<tr><td colspan="3">' + noAccessoryString + '</td></tr>');
	}).fail(function(result) {
		$j.prompt(result.responseText);
	});
}

/**
 * this method saves the free text for accessory type
 */
function updateFreeText(freeText) {
	var updateURL = selectedItemId > 0 ? "updateitemaccessoryfreetext.htm" : "updategroupaccessoryfreetext.htm";
	var updateData = selectedItemId > 0 ?
			{ jobItemId: selectedItemId, freeText: freeText } :
			{ groupId: selectedGroupId, freeText: freeText };
	$j.ajax({
		url: updateURL,
		data: updateData,
		async: true
	}).done(function(accessoryFreeText) {
		// empty free text box
		$j('div#accFreeText').html('');
		// append content for accessories
		$j('div#accFreeText').append(accessoryFreeText);
	}).fail(function(result) {
		$j.prompt(result.responseText);
	});
}

/**
* this method removes the free text for accessory type
*/
function removeFreeText() {
	var removeURL = selectedItemId > 0 ? "removeitemaccessoryfreetext.htm" : "removegroupaccessoryfreetext.htm";
	var removeData = selectedItemId > 0 ?
			{ jobItemId: selectedItemId } :
			{ groupId: selectedGroupId };
	$j.ajax({
		url: removeURL,
		data: removeData,
		async: true
	}).done(function(accessoryFreeText) {
		// empty free text box
		$j('div#accFreeText').html('');
		// append content for accessories
		$j('div#accFreeText').append(accessoryFreeText);
	}).fail(function(result) {
		$j.prompt(result.responseText);
	});
}

/**
 * this method updates checkbox values for a group
 */
function changeGroupField()
{
	$j.ajax({
		url: "updategroupfields.json",
		data: {
			groupId: selectedGroupId,
			calibrationGroup: $j('input#calGroup').prop('checked'),
			deliveryGroup: $j('input#delGroup').prop('checked')
		},
		async: true
	}).done(function() {}).fail(function(result) {
		$j.prompt(result.responseText);
	});
}

/**
 * this method updates the group allocation for a job item
 * 
 * @param {Integer} jobItemId id of the job item
 * @param {Integer} newGroupId id of the new group to assign
 */
function updateJobItemGroup(jobItemId, newGroupId)
{
	$j('img[id^="cascadeGroup_"]').removeClass("vis").addClass("hid");
	$j("#cascadeGroup_"+jobItemId).removeClass("hid").addClass("vis");
	$j.ajax({
		url: "updatejobitemgroup.json",
		data: {
			jobItemId : jobItemId,
			groupId: newGroupId
		},
		async: true
	}).done(function() {}).fail(function(result) {
		$j.prompt(result.responseText);
	});
}

/**
 * this method creates a new empty group
 *  
 * @param {Integer} jobId this is the id of the job for which we are creating a new group
 */
function createNewGroup(jobId)
{
	$j.ajax({
		url: "newjobitemgroup.json",
		data: {
			jobId: jobId
		},
		async: true
	})
	.done(function(result) {
		var groupId = result.groupId;
		// add link for new group to page
		$j('span[id^="grouplink"]:last').after(
				'<span id="grouplink' + groupId + '">&nbsp;&nbsp;'
				+ '<a href="" class="mainlink" onclick="changeToGroup(' + groupId + '); return false;">' + groupId + '</a> '
				+ '(<span id="accSize' + groupId + '">0</span>)</span>');
		// add option for new group to all selects
		$j('select[name="group"]').append('<option value="' + groupId + '">' + groupId + '</option>');
		// add one to group count
		$j('span#groupSize').text(parseInt($j('span#groupSize').text()) + 1);
	})
	.fail(function(result) {
		$j.prompt(result.responseText);
	});
}

/**
 * this method deletes an item group and ungroups all items
 * 
 * @param {Integer} groupId the id of the group
 * @param {Integer} jobId the id of the current job
 */
function deleteGroup(groupId)
{
	$j.ajax({
		url: "deletejobitemgroup.json",
		data: {
			groupId: groupId
		},
		async: true
	})
	.done(function(result) {
		selectedGroupId = 0;
		// find all items that were assigned to this group
		$j('table.accessoryJobItems tbody tr select[name="group"]').each(function() {
			// is this an item which was assigned to group being deleted?
			if (this.value == groupId) {
				// reset to no group
				$j(this).find('option:first').attr('selected', 'selected');
				// remove this group option
				$j(this).find('option[value="' + groupId + '"]').remove();
			}
			else {
				// remove group option
				$j(this).find('option[value="' + groupId + '"]').remove();
			}
		});
		// remove the group link from the page
		$j('span#grouplink' + groupId).remove();
		// hide accessory table
		$j('table.addAccessory').addClass('hid');
		// hide all free text action buttons
		$j('div#accFreeButtons a').removeClass('vis').addClass('hid');
		$j('span#accHeader').html('');
		$j('div#accSpace').html(i18n.t("core.jobs:job.accessoriesDisplayedHire", "Accessories for items or item groups are displayed here"));
		$j('div#accFreeText').html(i18n.t("core.jobs:job.accessoryNotesDisplayedHere", "Further accessory notes displayed here"));
		// subtract one from group count
		$j('span#groupSize').text(parseInt($j('span#groupSize').text()) - 1);
	})
	.fail(function(result) {
		$j.prompt(result.responseText);
	});
}

function cascadeGroup(jiId)
{
	var groupid = $j("#selectGroup_"+jiId).val();
	$j('img[id^="cascadeGroup_"]').each(function(index){
		var ji = parseInt($j(this).attr('id').replace('cascadeGroup_', ''));
		if(ji > jiId)
			{
			$j('#selectGroup_'+ji+' option[value="'+groupid+'"]').prop("selected", true);
			$j.ajax({
				url: "updatejobitemgroup.json",
				data: {
					jobItemId : ji,
					groupId: groupid
				},
				async: true
			}).done(function() {}).fail(function(result) {
				$j.prompt(result.responseText);
			});
			}
	});
}