import { html } from "../../../../../thirdparty/lit-element/lit-element.js";
import {unsafeHTML} from '../../../../../thirdparty/lit-html/directives/unsafe-html.js';
import '../../../../../components/cwms-translate/cwms-translate.js';
import {STATE_GENERATE} from "./PreviewEmailDetails.js";


export const previewEmailDetailsOverlay = context => html`
  <link rel="stylesheet" href="styles/theme/theme.css" />
  <link rel="stylesheet" href="styles/structure/structure.css" />
  <link rel="stylesheet" href="styles/structure/elements/table.css" />
  <link rel="stylesheet" href="styles/text/text.css" />
  <cwms-overlay @cwms-overlay-closed="${context.onCancel}" open=${true}>
    ${context.emailTitle}
    ${previewEmailDetails(context)}
  </cwms-overlay>
`;

const warningMessage = context => html`
  <div class="warningBox1">
    <div class="warningBox2">
      <div class="warningBox3">${context.warningMessage}</div>
    </div>
  </div>
`;

export const previewEmailDetails = context =>   context.state == STATE_GENERATE
    ? html`
        <div slot="cwms-overlay-body" class="loader">
          <img src="img/icons/ajax-loader.gif" width="31" height="31" alt="" />
          <h4>${context.sendMessage}</h4>
        </div>
      `:html`
  <div slot="cwms-overlay-body" id="previewemailOverlay">
    <fieldset>
            ${context.warningMessage ? warningMessage(context) : ""}
      <ol>
      ${sendButtonsTemplate(context)}
        <li>
          <label><cwms-translate code="system.to">To</cwms-translate></label>
          <div class="float-left padtop">
            ${context.toRecipients}
          </div>
        </li>
        ${context.ccRecipients}
        <li>
          <label
            ><cwms-translate code="system.subject">Subject</cwms-translate></label
          >
          <div class="subject editable float-left padtop" @input=${context.updateSubject} @click=${context.toggleEditable}>
            <div>${context.email.subject}</div>
          </div>
          <div class="clear-0"></div>
        </li>
        ${context.attachments}
        <li>
          <div class="width96" style=" padding: 10px; background: #fff; ">
            <slot name="rich-text"></slot>
          </div>
        </li>
      ${sendButtonsTemplate(context)}
      </ol>
    </fieldset>
    <div>&nbsp;</div>
  </div>
`;
export const ccPartTemplate = context => html`
  <li>
    <label><cwms-translate code="system.cc">Cc</cwms-translate>:</label>
    <div class="float-left padtop">
      ${context.recipients}
    </div>
    <div class="clear-0"></div>
  </li>
`;

export const recipientTemplate = context =>
  html`
    <div>${context.emailAddress}</div>
  `;

export const attachmentsTemplate = context => html`
  <li>
    <label><cwms-translate code="system.attachments">Attachments</cwms-translate>
      :</label>
    <div class="float-left padtop">
      ${context.attachments}
    </div>
    <div class="clear-0"></div>
  </li>
`;

export const attachmentTemplate = context =>
  html`
    <div>
      <a
        href="downloadfile.htm?filename=${context.file}"
        target="_blank"
        class="mainlink"
        >${context.fileName}</a
      >
    </div>
  `;


const sendButtonsTemplate = context => html`
        <li>
          <label>&nbsp;</label>
          <div class="float-left">
          <button @click=${context.onSend}><cwms-translate code="sendEmail">Send email</cwms-translate></button>
          <button @click=${context.onCancel}>${context.cancel}</button>
          </div>
          <div class="clear"></div>
        </li>
          `;
