/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	EditBPO.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	
*	KNOWN ISSUES	:	-
*	HISTORY			:	27/09/2007
*
****************************************************************************************************************************************/

/*
 * Array of calendar objects containing the initial setup literals needed
 * @param inputField (String) id of the input field to which the calendar should be binded
 * @param dateFormat (String) format that the date should be displayed
 * @param displayDates (String) 'all' make all dates available
 * 								'future' show only dates in the future
 * 								'past' show only dates in the past
 * Applied in init() function.
 */				
var jqCalendar = new Array ( { 
							 inputField: 'durationFrom',
							 dateFormat: 'dd.mm.yy',
							 displayDates: 'all'
							 },
							 { 
							 inputField: 'durationTo',
							 dateFormat: 'dd.mm.yy',
							 displayDates: 'all'
							 } );