/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	ChangeClient.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-						
*	KNOWN ISSUES	:	-
*	HISTORY			:	17/12/2007 - SH - Created File
*					:	14/10/2015 - TProvost - Manage i18n 	
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * this function checks that a new contact and address have been selected before the form is submitted
 */
function checkForm()
{
	if ($j('input#coid').val() == '' || $j('input#subdivid').val() == '' || $j('input#addrid').val() == '' || $j('input#personid').val() == '')
	{
		$j.prompt(i18n.t("core.jobs:job.mustSelectContactAndAddress", "You must select a new contact and address before submitting your changes"));
		return false;
	}
}