import { css } from "../../../../../thirdparty/lit-element/lit-element.js";

export const jobItemFeedbackOverlayStyles = css`
      #feedbackReqOverlay {
        width: 920px;
      }
      cwms-translate,
      table {
        font-size: 12px;
        line-height: 14px;
      }
    

.loader {
    display: flex;
    flex-direction: column;
    justify-content: center;
    height: 150px;
    align-items: center;
}

.email-row:not(.hid)  {
    display: flex;
    flex-direction: row;
    justify-content: stretch;
}

.email-to, .value-options, .email-cc {
    flex-grow:2;
    display:flex;
    justify-content:stretch;
}

.email-to .value-options cwms-email-search, .email-cc .value-options cwms-email-search {
    flex-grow:2;
}

.email-row > div {
    flex-basis:50%;
}

.email-row .include-self {
    display: flex;
    align-items: center;
    justify-content: center;
}

.actionBox1
{
	border: 2px solid #00FAFF;
}

.actionBox2
{
	border: 2px solid #00DFFF;
}

.actionBox3
{
	border: 2px solid #00B9B9;
	background-color: #F8FFF0;
}
`;

export const jobItemRowStyles = css`
    .row {
        display:flex;
        flex-direction:row;
        justify-content:space-between;
    }

    .row .row-element {
        flex-basis:49%
    }

    .row .controls {
        display:flex;
        flex-direction:row;
    }

    .row .controls cwms-translate, table {
        font-size:12px;
        line-height:14px;
    }

`; 