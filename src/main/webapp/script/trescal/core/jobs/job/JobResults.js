/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	JobResults.js
*	DESCRIPTION		:
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	13/12/2007 - SH - Created File
*
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );


/**
 * this variable contains the page name which should be used when creating the search again
 * link in the header section
 */
var searchAgain = 'jobsearch.htm';

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{
	// does the user with to highlight search criteria?
	if (highlight)
	{
		// loop over search criteria
		$j.each(searchCrit, function(i)
		{
			// any search criteria supplied?
			if (searchCrit[i].value != '')
			{
				// check each element on page with this class name
				$j('.' + searchCrit[i].name).each(function()
				{				
					// does the search criteria match?
					if ($j(this).text().toLowerCase().indexOf(searchCrit[i].value.toLowerCase()) != -1)
					{
						// get start location of matched text
						var start = $j(this).text().toLowerCase().indexOf(searchCrit[i].value.toLowerCase());
						// get end location of matched text
						var end = start + searchCrit[i].value.length;
						// assign the original text to variable
						var originalText = $j(this).text().slice(start, end);
						// replace the original text wrapped by span
						$j(this).html($j(this).text().replace(originalText, '<span class="' + searchCrit[i].classname + '">' + originalText + '</span>'));
					}
				});
			}
		});	
	}
}