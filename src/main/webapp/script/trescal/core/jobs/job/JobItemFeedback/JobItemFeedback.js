import {
  LitElement,
  html,
  css,
} from "../../../../../thirdparty/lit-element/lit-element.js";
import {
  jobItemFeedbackTemplate,
  jobItemTemplate,
} from "./JobItemFeedback.html.js";
import {
  jobItemRowStyles,
  jobItemFeedbackOverlayStyles,
} from "./JobItemFeedback.css.js";
import "../PreviewEmailDetails/PreviewEmailDetails.js";
import CwmsTranslate from "../../../../../components/cwms-translate/cwms-translate.js";
import { debounce } from "../../../../../tools/Debounce.m.js";
import {
  sendJobItemFeedbackEmail,
  SendJobItemFeedbackEmailContext,
} from "../SendJobItemFeedbackEmail.js";

export class JobItemFeedback {
  static createFeedbackRequestContent(jobid, coid, currentConId, systemCompId) {
    let jobItemFeedbackOverlay = document.querySelector(
      "cwms-jobitem-feedback-overlay"
    );
    jobItemFeedbackOverlay.currentConId = currentConId;
    jobItemFeedbackOverlay.systemCompId = systemCompId;
    
    const csrfToken = document.querySelector("meta[name='_csrf']").getAttribute("content")
    fetch(`jobitem/allJobItems?jobId=${jobid}`,
    		 {headers:{
    		        "X-CSRF-TOKEN": csrfToken,
    		      },}
    		 )
      .then((r) =>
        r.ok ? r.json() : Promise.reject(`Unable to query job items`)
      )
      .then((jobitems) =>
        jobitems && jobitems.length > 0
          ? jobitems
          : Promise.reject("No job items found")
      )
      .then((jobitems) => (jobItemFeedbackOverlay.jobitems = jobitems))
      .then(() => jobItemFeedbackOverlay.show())
      .catch(jobItemFeedbackOverlay.showWarning.bind(jobItemFeedbackOverlay));
  }
}

export const STATE_CREATE = "CREATE";
export const STATE_GENERATE = "GENERATE";
export const FEEDBACK_DTO_CHANGED = "feedbackDtoChanged";

class JobItemFeedbackOverlayWrapper extends LitElement {
  createRenderRoot() {
    return this;
  }

  static get properties() {
    return {
      jobId: { type: Number },
      jobitems: { type: Array, attribute: false },
      currentConId: { type: Number },
      currentContactEmail: { type: String },
      systemCompId: { type: Number },
    };
  }

  get innerOverlay() {
    if (!this._innerOverlay)
      this._innerOverlay = this.querySelector(
        "cwms-jobitem-feedback-overlay-inner"
      );
    return this._innerOverlay;
  }

  show() {
    this.innerOverlay.show();
  }

  showWarning(message) {
    this.innerOverlay.showWarning.bind(this.innerOverlay)(message);
  }

  render() {
    return html`<cwms-jobitem-feedback-overlay-inner
      .jobId=${this.jobId}
      .jobitems=${this.jobitems}
      .currentConId=${this.currentConId}
      .currentContactEmail=${this.currentContactEmail}
      .systemCompId=${this.systemCompId}
    >
      ${this.jobitems &&
      this.jobitems.map(
        (ji, idx) => html`<div slot="slot-ji-${idx}">
          <textarea cols="60" rows="3" name="feedbackComment"></textarea>
        </div>`
      )}
    </cwms-jobitem-feedback-overlay-inner>`;
  }
}

class JobItemFeedbackOverlay extends LitElement {
  static get properties() {
    return {
      jobId: { type: Number },
      jobitems: { type: Array, attribute: false },
      currentConId: { type: Number },
      currentContactEmail: { type: String },
      systemCompId: { type: Number },
      warningMessage: { type: String, attribute: false },
      message: { type: String, attribute: false },
      noReview: { type: Boolean },
      coid: { type: Number },
      state: { type: String, attribute: false },
      includeSelfInCarbonCopy: { type: Boolean, attribute: false },
      feedbackDtos: { type: Object, attribute: false },
      ccEmailList: { type: Array, attribute: false },
      toEmailList: { type: Array, attribute: false },
      showCC: { type: Boolean },
    };
  }

  static get styles() {
    return jobItemFeedbackOverlayStyles;
  }

  show() {
    this.shadowRoot.querySelector("cwms-overlay").show();
  }

  hide() {
    if (this.shadowRoot.querySelector("cwms-overlay").open)
      this.shadowRoot.querySelector("cwms-overlay").hide();
  }

  get jobitems() {
    return this._jobitems;
  }

  set jobitems(items) {
    const oldValue = this.jobitems;
    if (items instanceof Array) {
      this._jobitems = items;
      this.toEmailList = Array.from(
        items
          .map((t) => ({ personid: t.personId, name: t.name, email: t.email }))
          .reduce((acc, i) => acc.set(i.personid, i), new Map())
          .values()
      );
    } else {
      this._jobitems = [];
    }
    this.requestUpdate("jobitems", oldValue);
  }

  get toEmailSearch() {
    if (!this._toEmailSearch)
      this._toEmailSearch = this.shadowRoot.querySelector(
        "cwms-email-search[namePrefix='to']"
      );
    return this._toEmailSearch;
  }

  constructor() {
    super();
    this.state = STATE_CREATE;
    this.includeSelfInCarbonCopy = false;
    this.showCC = false;
    this.ccEmailList = [];
    this.toEmailList = [];
    CwmsTranslate.subscribeMessage(
      "viewjob.generatingAndSendingFeedbackEmail",
      "You have not selected any items, please select at least one item from the list",
      (message) => (this.warningMessageTranslation = message)
    );
    CwmsTranslate.subscribeMessage(
      "viewjob.generatingAndSendingFeedbackEmail",
      "Generating and Sending Feedback Email",
      (message) => (this.sendMessage = message)
    );
    CwmsTranslate.subscribeMessage(
      "viewjob.generatingFeedbackEmailForReview",
      "Generating Feedback Email For Review",
      (message) => (this.reviewMessage = message)
    );
  }

  updateToList(event) {
    this.toEmailList = [...this.toEmailSearch.emailList];
  }
  updateCcList(event) {
    this.ccEmailList = [...this.ccEmailSearch.emailList];
  }

  renderJobItems(jobitems) {
    if (!jobitems || !(jobitems instanceof Array) || jobitems.length == 0)
      return html``;
    const dtos =
      this.feedbackDtos ||
      jobitems
        .map((ji) => ({ jobItemId: ji.jobItemId }))
        .reduce((acc, cur) => ({ ...acc, [cur.jobItemId]: cur }), {});
    if (!this.feedbackDtos) this.feedbackDtos = dtos;

    return [...jobitems]
      .sort((ji1, ji2) => ji1.itemNo - ji2.itemNo)
      .map((ji, idx) => {
        const feedbackDto = (this.feedbackDtos &&
          this.feedbackDtos[ji.jobItemId]) || { jobItemId: ji.jobItemId };
        return html`
          <cwms-jobitem-row
            .idx=${idx}
            .jobItem=${ji}
            .feedbackDto=${feedbackDto}
            @feedbackDtoChanged=${this.feedbackDtoUpdated.bind(this)}
          >
            <slot name="slot-ji-${idx}" slot="slot-ji-${idx}"></slot>
          </cwms-jobitem-row>
        `;
      });
  }

  feedbackDtoUpdated(event) {
    const dto = event.detail;
    this.feedbackDtos = { ...this.feedbackDtos, [dto.jobItemId]: dto };
  }

  selectAllItems(event) {
    const value = event.currentTarget.checked;
    const oldValue = this.feedbackDtos;
    const dtos = Object.values(oldValue).map((d) => ({
      ...d,
      selected: value,
    }));
    this.feedbackDtos = dtos.reduce(
      (acc, cur) => ({ ...acc, [cur.jobItemId]: cur }),
      {}
    );
    this.requestUpdate();
  }

  onNoReviewChange(event) {
    this.noReview = event.currentTarget.checked;
  }

  onSubmit(event) {
    event.preventDefault();
    this.prepareEmail(false);
  }

  prepareEmail(forceSend) {
    const jobitemsRows = Array.from(
      this.shadowRoot.querySelectorAll("cwms-jobitem-row")
    );
    const itemsToBeSend = jobitemsRows
      .filter((el) => el.selected)
      .map((el) => el.feedbackDto);
    this.feedbackDtos = jobitemsRows.reduce(
      (acc, cur) => ({ ...acc, [cur.jobItem.jobItemId]: cur.feedbackDto }),
      {}
    );
    this.generateClientFeedback(
      SendJobItemFeedbackEmailContext.createInstanceWithItems(
        itemsToBeSend,
        this.toEmailList.filter((to) => to.personid).map((to) => to.personid),
        this.toEmailList.filter((to) => !to.personid).map((to) => to.email),
        this.ccEmailList.filter((cc) => cc.personid).map((cc) => cc.personid),
        this.ccEmailList.filter((cc) => !cc.personid).map((cc) => cc.email),
        !!this.noReview || forceSend,
        this.jobId,
        this.includeSelfInCarbonCopy
      )
    );
  }

  generateClientFeedback(emailContext) {
    if (!emailContext.items || emailContext.items.length < 1) {
      this.warningMessage = this.warningMessageTranslation;
      return;
    }
    this.message = emailContext.send ? this.sendMessage : this.reviewMessage;

    this.state = STATE_GENERATE;
    sendJobItemFeedbackEmail(emailContext)
      .then(this.processResponse.bind(this))
      .catch((message) => {
        this.state = STATE_CREATE;
        this.showWarning(message);
      });
  }

  processResponse(emailResult) {
    if (emailResult.send) {
      loadScript.populateEntityEmail(
        emailResult.email.id,
        this.currentConId,
        this.systemCompId,
        this.jobid
      );
      document.getElementById("email-link").click();
      this.hide();
    } else {
      const previewOrverlay = document.querySelector(
        "cwms-preview-email-details"
      );
      previewOrverlay.emailWrapper = emailResult;
      previewOrverlay.currentConId = this.currentConId;
      previewOrverlay.systemCompId = this.systemCompId;
      previewOrverlay.show();
      previewOrverlay.addEventListener("send-completed", () => this.hide());
    }
    this.state = STATE_CREATE;
  }

  initializeCCEmailSearchPlugin() {
    this.showCC = true;
    this.ccEmailSearch = this.shadowRoot.querySelector(
      "cwms-email-search[namePrefix='cc']"
    );
    this.ccEmailSearch.addEventListener(
      "selected",
      this.updateCcList.bind(this)
    );
    this.ccEmailSearch.addEventListener(
      "deselected",
      this.updateCcList.bind(this)
    );
    return false;
  }

  render() {
    const context = {
      coid: this.coid,
      warningMessage: this.warningMessage,
      message: this.message,
      jobitems: this.jobitems || [],
      cancel: this.hide,
      renderJobItems: this.renderJobItems.bind(this),
      selectAllItems: this.selectAllItems.bind(this),
      includeSelfChange: (e) => {
        this.includeSelfInCarbonCopy = !this.includeSelfInCarbonCopy;
      },
      includeSelfInCarbonCopy: this.includeSelfInCarbonCopy,
      currentContact: this.currentContactEmail,
      noReviewChange: this.onNoReviewChange,
      generateClientFdbkReq: this.onSubmit.bind(this),
      state: this.state,
      initializeCCEmailSearchPlugin: this.initializeCCEmailSearchPlugin.bind(
        this
      ),
      showCC: this.showCC,
      ccEmailList: this.ccEmailList,
      toEmailList: this.toEmailList,
      updateToList: this.updateToList,
    };
    return jobItemFeedbackTemplate(context);
  }

  showWarning(message) {
    this.warningMessage = message;
    this.show();
  }
}

customElements.define(
  "cwms-jobitem-feedback-overlay-inner",
  JobItemFeedbackOverlay
);
customElements.define(
  "cwms-jobitem-feedback-overlay",
  JobItemFeedbackOverlayWrapper
);

class JobItemRow extends LitElement {
  static get properties() {
    return {
      jobItem: { type: Object, attribute: false },
      feedbackDto: { type: Object, attribute: false },
      idx: { type: Number },
    };
  }

  get selected() {
    return this.feedbackDto.selected;
  }

  set selected(value) {
    this.feedbackDto= {...this.feedbackDto,seleted:value}
    this.dispatchEvent(
      new CustomEvent(FEEDBACK_DTO_CHANGED, { detail: this.feedbackDto })
    );
  }

  set feedbackDto(dto) {
    const oldValue = this._feedbackDto;
    this._feedbackDto = dto;
    this.requestUpdate("feedbackDto", oldValue)
      .then(()=> this.initEditor(dto.selected))
    ;
  }

  get feedbackDto() {
    if (!this._feedbackDto) {
      if (!this.jobItem) return null;
      this._feedbackDto = new ClientFeedbackRequestDTO(
        this.jobItem.jobItemId,
        false,
        false,
        false,
        false,
        false,
        "",
        false
      );
    }
    return this._feedbackDto;
  }

  static get styles() {
    return jobItemRowStyles;
  }

  constructor(){
    super();
    this.commentHandler = debounce(this.updateComment,300);
  }

  updateFeedbackDto(event) {
    const element = event.currentTarget;
    const field = element.name;
    const value =
      element.type == "checkbox" || element.type == "radiobox"
        ? element.checked
        : element.value;
    if (!this.feedbackDto)
      this.feedbackDto = new ClientFeedbackRequestDTO(
        this.jobItem.jobItemId,
        false,
        false,
        false,
        false,
        false,
        "",
        false
      );
    this.feedbackDto = { ...this.feedbackDto, [field]: value };
    this.dispatchEvent(
      new CustomEvent(FEEDBACK_DTO_CHANGED, { detail: this.feedbackDto })
    );
  }

  initEditor(seleted) {
    if(!seleted || this.editor) return;
    const slot = this.querySelector("slot[slot^='slot-ji']");
    slot.assignedElements().map((d) => {
      const textarea = d.querySelector("textarea");
      this.editor = CKEDITOR.replace(textarea, {
        toolbarGroups: [
          { name: "clipboard", groups: ["clipboard", "undo"] },
          {
            name: "paragraph",
            groups: ["list", "indent", "blocks", "align", "bidi"],
          },
        ],
        height: 100,
      });
      this.editor && this.editor.on("change",(ev)=>this.commentHandler(ev.editor.getData(),300));
    }    
    );

  }

  updateComment(text){
    this.feedbackDto = {...this.feedbackDto,feedbackComment:text}
    this.dispatchEvent(
      new CustomEvent(FEEDBACK_DTO_CHANGED, { detail: this.feedbackDto }));
  }

  toggleSelectRow() {
    const oldValue = this.feedbackDto;
    this.feedbackDto = { ...oldValue, selected: !oldValue.selected };
    this.dispatchEvent(
      new CustomEvent(FEEDBACK_DTO_CHANGED, { detail: this.feedbackDto })
    );
  }

  render() {
    const context = {
      jobItem: this.jobItem,
      feedbackDto: this.feedbackDto,
      onSelect: this.toggleSelectRow,
      updateState: this.updateFeedbackDto,
      idx: this.idx,
    };
    return this.jobItem ? jobItemTemplate(context) : html``;
  }
}

customElements.define("cwms-jobitem-row", JobItemRow);

class ClientFeedbackRequestDTO {
  constructor(
    jobItemId,
    confirmCalType,
    confirmTurnaround,
    confirmCalPoints,
    confirmReturn,
    confirmAdjustment,
    feedbackComment,
    selected
  ) {
    this.jobItemId = jobItemId;
    this.confirmCalType = confirmCalType;
    this.confirmTurnaround = confirmTurnaround;
    this.confirmCalPoints = confirmCalPoints;
    this.confirmReturn = confirmReturn;
    this.confirmAdjustment = confirmAdjustment;
    this.feedbackComment = feedbackComment;
    this.selected = selected;
  }
}
