/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	AddJob.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	Commenting of code.
*	KNOWN ISSUES	:	-
*	HISTORY			:	22/05/2007
*					:	15/10/2015 - TProvost - Manage i18n
*					:	16/10/2015 - TProvost - Add ".js" extension for dwr/interface/calloffitemservice and dwr/interface/collectedinstrumentservice
*					:	16/10/2015 - TProvost - Remove addContact() and addAdress() function because they are not used
*					:	2016-11-01 - Galen Beck - Added siteJobChange() method for site job variations
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'script/thirdparty/jQuery/jquery.tablesorter.js',
								'script/trescal/core/utilities/searchplugins/InstrumentSearchPlugin.js',
								'dwr/interface/instrumservice.js',
								'script/trescal/core/utilities/DOMUtils.js',
								'dwr/interface/calloffitemservice.js',
								'dwr/interface/collectedinstrumentservice.js',
								'dwr/interface/bposervice.js',
								'dwr/interface/locationservice.js',
								'script/thirdparty/jQuery/jquery.boxy.js'
								);
								

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init() {

	loadScript.loadStyle('styles/jQuery/jquery-boxy.css', 'screen');

	window.addEventListener("cwms-cascade-select-company", loadCurrencyCallback);
	window.addEventListener("cwms-cascade-select-address", getDefaulTransportOptions);
	window.addEventListener("cwms-cascade-select-subdiv", getPrebookingListForSelectedCompany);
}

//disable autofill/autocomplete on the company search plugin
setTimeout('$j("#compSearchPlugin #coname").attr("autocomplete","new-password")', 1600);


/**
 * variable to hold amount of po's that have been created on page
 */
var posCreated = 0;
var bposCreated = 0;


/**
 * this method shows / hides the booking in address and location selectors based on job type
 */
function jobTypeChange(jobtype) {
	if (jobtype == 'SITE') {
		$j('#bookedInAddrStandard').removeClass('vis').addClass('hid');
		$j('#bookedInAddrSite').removeClass('hid').addClass('vis');
		$j('#bookedInLocStandard').removeClass('vis').addClass('hid');
		$j('#bookedInLocSite').removeClass('hid').addClass('vis');
	}
	else {
		$j('#bookedInAddrStandard').removeClass('hid').addClass('vis');
		$j('#bookedInAddrSite').removeClass('vis').addClass('hid');
		$j('#bookedInLocStandard').removeClass('hid').addClass('vis');
		$j('#bookedInLocSite').removeClass('vis').addClass('hid');
	}
	
	if(jobtype == 'PREBOOKING') {
		
		$j('#standardradio').attr('disabled', true);
		$j('#fileradio').attr('checked', true).trigger('change');
		$j('#plantid').attr('disabled', true);
		$j('#availableprebookings').attr('disabled', true);
		$j('#search').prop('disabled', true);
		$j('#receiptdate').attr('disabled', true);
		$j('#pickupdate').attr('disabled', true);
		$j('#transportin').attr('disabled', true);
		$j('#carrier').attr('disabled', true);
		$j('#trackingnumber').attr('disabled', true);
		$j('#transportout').attr('disabled', true);
		$j('#numberofpackages').attr('disabled', true);
		$j('#packagetype').attr('disabled', true);
		$j('#image').prop('disabled', true);
		$j('#storagearea').attr('disabled', true);
		$j('#prebookingradio').attr('disabled', true);
	}
	else {
		$j('#standardradio').attr('disabled', false);
		$j('#fileradio').attr('checked', false);
		$j('#plantid').attr('disabled', false);
		$j('#availableprebookings').attr('disabled', false);
		$j('#search').prop('disabled', false);
		$j('#receiptdate').attr('disabled', false);
		$j('#pickupdate').attr('disabled', false);
		$j('#transportin').attr('disabled', false);
		$j('#carrier').attr('disabled', false);
		$j('#trackingnumber').attr('disabled', false);
		$j('#transportout').attr('disabled', false);
		$j('#numberofpackages').attr('disabled', false);
		$j('#packagetype').attr('disabled', false);
		$j('#image').prop('disabled', false);
		$j('#storagearea').attr('disabled', false);
		$j('#prebookingradio').attr('disabled', false);
	}
}

/**
 * this method creates the content of the boxy pop up used when creating a new purchase order
 */
function purchaseOrdContent()
{	
	// create content to be displayed
	var content = 	'<div id="purchaseOrderBoxy" class="overflow">' +
						'<fieldset>' +
							'<legend>' + i18n.t("core.jobs:createPO", "Create Purchase Order") + '</legend>' +
							'<ol>' +
								'<li>' +
									'<label>' + i18n.t("core.jobs:poNoLabel", "PO Number:") + '</label>' +
									'<input type="text" id="ponumber" name="ponumber" tabindex="100" value="" onkeypress=" if (event.keyCode == 13){ $j(this).parent().parent().find(\'input[type=button]\').trigger(\'click\'); } " />' +
								'</li>' +
								'<li>' +
									'<label>' + i18n.t("core.jobs:poCommentLabel", "PO Comment:") + '</label>' +
									'<textarea id="pocomment" name="pocomment" tabindex="101" value="" cols="50" rows="4"></textarea>' +
								'</li>' +
								'<li id="poSubmit">' +
									'<label>&nbsp;</label>' +
									'<input type="button" tabindex="103" onclick=" createPurchOrd(0,$j(\'#ponumber\').val(), $j(\'#pocomment\').val()); return false; " value="' + i18n.t("core.jobs:createPO", "Create PO") + '" />' +
								'</li>' +
							'</ol>' +
						'</fieldset>' +
					'</div>';
	// create new boxy pop up using content
	var purchaseOrderBoxy = new Boxy(content, {title: i18n.t("core.jobs:createPurchaseOrder", "Create Purchase Order"), unloadOnHide: true});
	// adapt size of boxy
	purchaseOrderBoxy.tween(680,260);
	// focus on po number input
	$j('div#purchaseOrderBoxy input#ponumber').focus();
}

/**
 * this function appends the newly created purchase order to the table
 * 
 * @param {String} ponumber is the purchase order number supplied by user
 * @param {String} pocomment is the comment supplied by the user
 */
function createPurchOrd(poid, ponumber, pocomment)
{
	// no PO's have been created so append new table to display PO's
	if (posCreated == 0)
	{
		$j('li#PO span').empty().append('<table id="posList" class="default3" summary="This table holds all purchase orders that have been created">' +
											'<thead>' +
												'<tr>' +
													'<th id="poDef" scope="col">' + i18n.t("default", "Default") + '</th>' +
													'<th id="poNum" scope="col">' + i18n.t("core.jobs:poNo", "PO Number") + '</th>' +
													'<th id="poCom" scope="col">' + i18n.t("core.jobs:poComment", "PO Comment") + '</th>' +
													'<th id="poDel" scope="col">' + i18n.t("remove", "Remove") + '</th>' +
												'</tr>' +
											'</thead>' +
											'<tfoot>' +
												'<tr>' +
													'<td colspan="4">' +
														'<a href="#" class="mainlink" onclick=" purchaseOrdContent(); return false; " title="">' + i18n.t("core.jobs:createPurchaseOrder", "Create Purchase Order") + '</a>' +
													'</td>' +
												'</tr>' +
											'</tfoot>' +
											'<tbody>' +
											'</tbody>' +
										'</table>' +
										'<div class="clear-0"></div>');
	}	
	// add one to the PO's created variable
	posCreated++;
	// create po row content
	var content = 	'<tr id="po' + posCreated + '">' +
						'<td>' +
							'<a href="#">' +
								'<img';
									// is this first po added?
									if (posCreated < 2)
									{
										// add img attributes for default po
										content += ' src="img/icons/po_default.png" id="defaultPOImg" width="24" height="16" ';
										// populate hidden input field with po id
										$j('input#defaultpo').val(posCreated);
									}
									else
									{
										// add img attributes for po
										content += ' src="img/icons/po.png" width="16" height="16" ';
									}
								content += 'alt="' + i18n.t("core.jobs:job.makePODefault", "Make PO Default") + '" title="' + i18n.t("core.jobs:job.makePODefault", "Make PO Default") + '" onclick=" updatePODefault(this, \'' + posCreated + '\'); return false; " />' +
							'</a>' +
						'</td>' +
						'<td>' + ponumber + '<input type="hidden" name="poidlist[' + posCreated + ']" value="' + poid + '" /><input type="hidden" name="polist[' + posCreated + ']" value="' + ponumber + '" /></td>' +
						'<td>' + (pocomment!=null?pocomment:'') + '<input type="hidden" name="pocomlist[' + posCreated + ']" value="' + pocomment + '" /></td>' +
						'<td class="center">' +
							'<a href="#" onclick=" deletePurchOrd(' + posCreated + '); return false; ">' +
								'<img src="img/icons/delete.png" height="16" width="16" title="' + i18n.t("core.jobs:job.deleteThisPO", "Delete this PO") + '" alt="' + i18n.t("core.jobs:job.deleteThisPO", "Delete this PO") + '" />' +
							'</a>' +
						'</td>' +
					'</tr>';
										
	// append new PO to the table
	$j('li#PO span table tbody').append(content);	
	// get the current purchase order boxy implementation and close it.
	if(poid == 0)
		Boxy.get($j('div#purchaseOrderBoxy')).hide();
}

function createPurchOrdFromPrebooking(prebookingid)
{
	if(prebookingid != undefined)
	{
		$j.ajax({
				url: "getPOsForSelectedPrebooking.json",
				data: {
					prebookingid: prebookingid
				},
				async: true
			}).done(function(data) {
				var html = '';
				for(var i=0;i<data.length;i++)
					{
					createPurchOrd(data[i].poId,data[i].poNumber,data[i].comment);
					}
					
			}).fail(function(result) {
				$j.prompt(result.responseText);
			});
	}
}

function selecteBPOFromPrebooking(bpoid)
{
	$j("input[name='bpo']").each(function( index ) {
			  if($j( this ).val() == bpoid) $j( this ).prop("checked", true);
			  else $j( this ).prop("checked", false);
		});
}

function deleteAllPurchOrd()
{
	var not_empty = false;
	while(posCreated > 0){
	// remove purchase order from table
	$j('li#PO span table tbody tr#po' + posCreated).remove();
	// subtract one from the PO's created variable
	posCreated--;
	not_empty = true;
	}
	
	if(not_empty == true)
		{
		$j('li#PO span').remove();
		$j('li#PO').append('<span><a href="#" class="mainlink" onclick=" purchaseOrdContent(); return false; " title="">' + i18n.t("core.jobs:createPO", "Create PO") + '</a></span>');
		}
}

/**
 * this function deletes a purchase order from the table
 * 
 * @param {Integer} id id created using the number of purchase orders created
 */
function deletePurchOrd(id)
{
	// remove purchase order from table
	$j('li#PO span table tbody tr#po' + id).remove();
	// subtract one from the PO's created variable
	posCreated--;
	// no PO's left so remove PO table and re-install link to add new PO
	if (posCreated == 0)
		{
			$j('li#PO span').remove();
			$j('li#PO').append('<span><a href="#" class="mainlink" onclick=" purchaseOrdContent(); return false; " title="">' + i18n.t("core.jobs:createPO", "Create PO") + '</a></span>');
		}
	else
		{
		// update other input items index
		$j('#posList tbody tr').each(function(index) {
			
			$j(this).find('input[name^=\'poidlist[\']').attr('name', 'poidlist['+(index + 1)+']');
			$j(this).find('input[name^=\'polist[\']').attr('name', 'polist['+(index + 1)+']');
			$j(this).find('input[name^=\'pocomlist[\']').attr('name', 'pocomlist['+(index + 1)+']');
			
		});
		}
}

/**
 * this function updates the chosen purchase order to be the default
 * 
 * @param {Object} img object to be manipulated
 * @param {Integer} id id created using the number of purchase orders created
 */
function updatePODefault(img, id)
{
	// this is not the current default
	if (img.id != 'defaultPOImg')
	{
		// get the current default PO image and change properties
		$j('#defaultPOImg').attr('src', 'img/icons/po.png')
							.attr('alt', i18n.t("core.jobs:job.makePODefault", "Make PO Default"))
							.attr('title', i18n.t("core.jobs:job.makePODefault", "'Make PO Default"))
							.attr('id', '')
							.attr('width', '16');					
		
		// change the properties of the chosen PO image
		img.src = 'img/icons/po_default.png';
		img.alt = i18n.t("core.jobs:job.defaultPO", "Default PO");
		img.title = i18n.t("core.jobs:job.defaultPO", "Default PO");
		img.id = 'defaultPOImg';
		img.width = '24';
		
		// populate hidden input field with po id
		$j('input#defaultpo').val(id);
	}
	else
	{
		return false;
	}		
}

function bookingInAddrChange(useLocations, addrId, defLocId, emptyText)
{
	if (useLocations)
	{
		locationservice.getAllAddressLocations(addrId, true, 
		{
			callback:function(locations)
			{
				$j('select#bookedInLocId').empty();
				dwr.util.addOptions('bookedInLocId', locations, 'locationid', 'location');
				
				if (locations.length == 0) 
				{
					$j('select#bookedInLocId').append('<option value="0">'+emptyText+'</option>');
				}
				else
				{
					if(defLocId != 0)
					{
						$j('select#bookedInLocId option[value=' + defLocId + ']').attr('selected','selected');
					}
				}
			}	
		});
	}
}

function getExchangesFormatForSelectedCompany()
{
	const url = window.location.href;
	const cascade = document.getElementById("cascade-search")
	const coid = cascade?.coid ?? document.getElementById("coid").value;
	const coname = cascade?.coname ?? document.querySelector("#compPlugResults a[class='selected'] span")?.textContent;

	if ((url.indexOf('addprebookingjob') !== -1 || $j('#fileradio').is(':checked')) && coid !== undefined && coid !== '') {

		$j.ajax({
			url: "getExchangeFormats.json",
			data: {
				coid,
				type: 'PREBOOKING'
			},
			async: true
		}).done(function (data) {
			const client_company_name = coname;
			var client_company_ef_options = '';
			var generic_ef_options = '';
			for (var i = 0; i < data.length; i++) {
				if (data[i].clientCompanyId == coid)
					client_company_ef_options += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
				else
					generic_ef_options += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
			}
			var html = '';
			if (client_company_ef_options != '') {
					html += '<option disabled>'+client_company_name+'</option>';
					html += client_company_ef_options;
					html += '<option disabled>-------------------------------</option>';
				}
				
				html += generic_ef_options;
					
				$j("#exchangeformat").html(html);
				$j("#ef_error_msg").addClass("hidden");
				if(html === '')
				{
					$j("#ef_error_msg").html("No exchanges format defined for this Company");
   					$j("#ef_error_msg").removeClass("hidden");
					return false;
				}
				else
				{
					// GB 2018-08-21 Commenting this 'clearing' out as bug fix - the updates should be isolated and not interfere with each other  
					$j("#availableprebookings").html("");
				}
		
			}).fail(function(result) {
				$j("#ef_error_msg").html(result.responseText);
				$j("#ef_error_msg").removeClass("hidden");
			});

	
	}
	else
		$j("#exchangeformat").html("");
}


function getDefaulTransportOptions(e) {

	var addrid = e.detail.item.addrid;

	// exit if addrid is empty
	if (addrid.length == 0)
		return false;

	fetch(cwms.urlWithParamsGenerator("getDefaulTransportOptions.json", {addrid}))
		.then(res => res.ok ? res.json() : Promise.reject(res.statusText))
		.then(data => {
			if (data.transportOptionIn != null)
				document.getElementById("transportIn").value = data.transportOptionIn;
			else
				document.getElementById("transportIn").value = document.querySelector("#transportIn option:first-child")?.value; // reset to first option

			if (data.transportOptionOut != null)
				document.getElementById("transportOut").value = data.transportOptionOut;
			else
				document.getElementById("transportOut").value = document.querySelector("#transportOut option:first-child")?.value; // reset to first option
		})
		.catch(alert)
}

function getPrebookingListForSelectedCompany(event) {
	const subdivid = event.detail.item.subdivid;
	const plantid = document.getElementById('plantid')?.value;
	document.getElementById('plantid').value = '';
	const plantno = document.getElementById('plantno')?.value;
	if (!isNaN(subdivid)) {
		let pid;
		let pno;
		if (plantid != undefined && plantid != '')
			pid = plantid;
		if (plantno != undefined && plantno != '')
			pno = plantno;
		fetch(cwms.urlWithParamsGenerator("getPrebookingForAddJob.json", {subdivid, plant_id: pid, plant_no: pno}))
			.then(res => res.ok ? res.json() : Promise.reject(res.statusText))
			.then(data => {
				var html = '';
				for (var i = 0; i < data.length; i++) {
					var formatedText = 'N°' + data[i].id + '_';
					if (data[i].createdOn != null && data[i].createdOn != undefined) {
						var d = new Date(data[i].createdOn);
						formatedText += d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
					}
					if (data[i].clientRef != null && data[i].clientRef != undefined && data[i].clientRef != '')
					formatedText += '(' + data[i].clientRef + ')';
				html += '<option value="'+data[i].id+'" data-bpoid="'+data[i].bpoid+'">'+ formatedText +'</option>';
				}
			
			deleteAllPurchOrd();
			if(i > 0)
				createPurchOrdFromPrebooking(data[0].id);	
				
			$j("#availableprebookings").html(html);
			if(html === '')
			{
				$j('#standardradio').attr('checked', 'checked');
				$j('#prebookingradio').attr("disabled", true);
				$j('#plantno').attr("disabled", true);
				$j('#search').attr("disabled", true);
				$j('#availableprebookings').attr("disabled", true);
				updateFormAction($j('#standardradio'), 'addjob');
				return false;
			} else {
				$j('#prebookingradio').attr('checked', 'checked');
				$j('#prebookingradio').attr("disabled", false);
				$j('#plantno').attr("disabled", false);
				$j('#search').attr("disabled", false);
				$j('#availableprebookings').attr("disabled", false);
				updateFormAction($j('#prebookingradio'), 'addjobfp');
			}

			}).catch(alert);
		
	}
}

function recordTypeChange(ele)
{
	if(ele.is(':checked')){
	$j("#availableprebookings").html("");
	$j("#exchangeformat").html("");
	deleteAllPurchOrd();
	}
}

function showCarrier(transportIn)
{
	if($j("#transportIn option:selected").text().startsWith('Courier'))
		{
			$j('#carrier_div').removeClass('hid').addClass('vis');
			$j('#trackingnumber_div').removeClass('hid').addClass('vis');
		}
	else
		{
			$j('#carrier_div').removeClass('vis').addClass('hid');
			$j('#trackingnumber_div').removeClass('vis').addClass('hid');
		}
}

function updateFormAction(ele, val)
{
	if(ele.is(':checked'))
		{
		var url = $j('#addjobform').attr('action');
		var newUrl = url.substring(0, (url.lastIndexOf('/')+1))+val+'.htm';
		$j('#addjobform').attr('action', newUrl);
		}
}

function checkOverrideDateInOnJobItems(checkbox) {
	if(checkbox.checked) {
		$j('#overrideDateInOnJobItemsDialog').dialog({
			autoOpen: false,
			resizable: false,
			height: "auto",
			width: 400,
			modal: true,
			buttons: {
	        	Yes: function() {
	        		$j(this).dialog('close');
	        	},
	        	No: function() {
	        		$j('#overrideDateInOnJobItems').prop('checked', false);
	        		$j(this).dialog('close');
	        	}
	        }
		});
		$j('#overrideDateInOnJobItemsDialog').dialog('open');
	}
	return true;
}

function selectAllItems(select, parentDiv)
{
	// check all
	if (select == true) {
		$j('#' + parentDiv + ' input:checkbox').check('on');
	}
	// uncheck all
	else {
		$j('#' + parentDiv + ' input:checkbox').check('off');
	}
}

function loadCurrencyCallback(event) {

	function createOption(value, label, selected) {
		const opt = document.createElement("option");
		opt.value = value;
		opt.selected = selected;
		opt.insertAdjacentHTML('beforeend', label);
		return opt;
	}

	const coid = event.detail.item.coid;
	fetch(cwms.urlWithParamsGenerator("supportedCurrency/companyOptions.json", {coid}))
		.then(res => res.ok ? res.json() : Promise.reject(res.statusText))
		.then(opts => {
			const options = opts.map(curOpt => createOption(curOpt.optionValue, 
					`${curOpt.currency} @  ${curOpt.defaultCurrencySymbol}1 = ${cwms.currencySymbolFunction(curOpt.currency)} ${curOpt.rate} 
					${curOpt.companyDefault ? '[<cwms-translate code="viewjob.compdefrate">Company Default Rate</cwms-translate>]' : curOpt.systemDefault ? '[<cwms-translate code="viewjob.systdefrate">System Default Rate</cwms-translate>]' : ""}`,
					curOpt.selected));
			document.getElementById("currencyCode")?.replaceChildren(...options);
		});
}
