/** 
 * File name : Editjob.js 
 * */


/*
 * Shows or hides client versus business address/location selector based on job
 * type
 */
function changeJobType(newJobType) {
	if (newJobType == 'SITE') {
		// Select booking in address from client locations
		$j('li#editCbiAddr').removeClass('hid').addClass('vis');
		// Only show client location options if they are defined
		if ($j('select#bookedInClientLocationId option').length > 0) {
			$j('li#editCbiLoc').removeClass('hid').addClass('vis');
		} else {
			$j('li#editCbiLoc').removeClass('vis').addClass('hid');
		}
		$j('li#editBbiAddr').removeClass('vis').addClass('hid');
		$j('li#editBbiLoc').removeClass('vis').addClass('hid');
	} else {
		// Select booking in address from business locations
		$j('li#editCbiAddr').removeClass('vis').addClass('hid');
		$j('li#editCbiLoc').removeClass('vis').addClass('hid');
		$j('li#editBbiAddr').removeClass('hid').addClass('vis');
		// Only show business location options if they are defined
		if ($j('select#bookedInBusinessLocationId option').length > 0) {
			$j('li#editBbiLoc').removeClass('hid').addClass('vis');
		} else {
			$j('li#editBbiLoc').removeClass('vis').addClass('hid');
		}
	}
}

/*
 * Updated to work with multiple types of addresses (booking in, return)
 * 2017-03-24 GB
 * 
 * parameters: addrId - database id of the address defLocId - database id of the
 * default location (upon original page load) domSelectId - DOM selector ID for
 * the select box to contain location options (e.g. locid for <select
 * id="locid"...) domLiId - DOM selector ID for the list item <li> which should
 * be shown/hidden based on locations present (e.g. delLoc for <li
 * id="delLoc"...)
 * 
 */
function addrChange(addrId, defLocId, domSelectId, domLiId) {
	
	$j.ajax({
		url: "addrChange.json",
		data: {
			addrId: addrId
		},
		async: true
	}).done(function(locations) {
		
		// remove all options from select
		$j('select#'+ domSelectId).empty();
		// add options from locations list
		$j.each(locations, function(i) {
		     $j('select#'+ domSelectId)
		         .append($j('<option value='+ locations[i].locationid +'>'+ locations[i].location +'</option>'));
		});
		
		if (locations.length < 1) {
			$j('li#' + domLiId).removeClass('vis').addClass('hid');
		} else {
			if (defLocId != 0) {
				$j(
						'select#' + domSelectId
								+ ' option[value='
								+ defLocId + ']').attr(
						'selected', 'selected');
			}
			$j('li#' + domLiId).removeClass('hid').addClass('vis');
		}
	});
}

function init() {
	$j('#overrideDateInOnJobItemsDialog').dialog({
		autoOpen: false,
		resizable: false,
		height: "auto",
		width: 400,
		modal: true,
		buttons: {
        	Yes: function() {
        		$j(this).dialog('close');
        	},
        	No: function() {
        		$j('#overrideDateInOnJobItems').prop('checked', false);
        		$j(this).dialog('close');
        	}
        }
	});
	
}

function checkOverrideDateInOnJobItems(checkbox) {
	if(checkbox.checked)
		$j('#overrideDateInOnJobItemsDialog').dialog('open');
	return true;
}