/*******************************************************************************
 * CWMS (Calibration Workflow Management System)
 * 
 * Copyright 2006, Antech Calibration Services www.antech.org.uk
 * 
 * All rights reserved
 * 
 * Document author: Stuart Harrold
 * 
 * FILENAME : ViewJob.js DESCRIPTION : DEPENDENCIES : -
 * 
 * TO-DO : Some commenting. Check and remove any of the page imports that are
 * unused. Check and remove any functions that are old and unused.
 * 
 * KNOWN ISSUES : - HISTORY : 26/10/2006 - SH - Created File 22/08/2007 - JV -
 * Edited File - General tidying/changing to adhere to new Job entity changes
 * 10/10/2007 - JV - Commented most functions. 16/10/2015 - TProvost - Manage
 * i18n 16/10/2015 - TProvost - Fix bug in new version of Impromptu.js - Replace
 * the "callback" property (property disappeared) by the "submit" property =>
 * Changes made in functions printJobSheet and invoiceNotRequired 16/10/2015 -
 * TProvost - Minor fix on enable button in functions addQuickItem,
 * printJobSheet, printJobList, printMultipleJobItemLabels 2016-11-16 - GB -
 * Removed deleteInstruction function as now handled by controller rather than
 * DWR 2016-12-06 - GB - Deleted old ActiveX Dymo Label printing code
 ******************************************************************************/


var jquiTabs = new Array({
	idName : 'tabs'
});

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = ['script/trescal/core/deliverynote/ViewDIUtility.js',
		'script/trescal/core/quotation/ViewQIUtility.js',
		'script/trescal/core/utilities/DOMUtils.js',
		'script/trescal/core/jobs/jobitem/JobItemFunctions.js',
		'script/trescal/core/tools/labelprinting/dymo/DymoLabelFunctions.js',
		'script/trescal/core/documents/ImageGalleryFunctions.js',
		'script/thirdparty/jQuery/jquery.boxy.js',
		'script/trescal/core/quotation/LinkQuotationUtility.js',
		'script/thirdparty/fileupload/upload.js',
		'dwr/interface/UploadMonitor.js',
		'script/trescal/core/tools/FileBrowserPlugin.js',
		
		];

/**
 * array of objects containing the element name and corner type which should be
 * applied. applied in init() function.
 */
var niftyElements = [{
	element : 'ul#tabnav a',
	corner : 'small transparent top'
}, {
	element : 'div.infobox',
	corner : 'normal'
}, {
	element : 'ul.subnavtab a',
	corner : 'small transparent top'
}];

/**
 * this variable holds an array of id objects, one for the navigation link and
 * one for the content area the link refers to. this array is passed to the
 * Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = [{
	anchor : 'jobitem-link',
	block : 'jobitem-tab'
}, {
	anchor : 'expenses-link',
	block : 'expenses-tab'
}, {
	anchor : 'advanced-link',
	block : 'advanced-tab'
}, {
	anchor : 'contractinstructions-link',
	block : 'contractinstructions-tab'
}, {
	anchor : 'instructions-link',
	block : 'instructions-tab'
}, {
	anchor : 'deliveries-link',
	block : 'deliveries-tab'
}, {
	anchor : 'clientpurchord-link',
	block : 'clientpurchord-tab'
}, {
	anchor : 'jobfiles-link',
	block : 'jobfiles-tab'
}, {
	anchor : 'printing-link',
	block : 'printing-tab'
}, {
	anchor : 'quickitems-link',
	block : 'quickitems-tab'
}, {
	anchor : 'invoices-link',
	block : 'invoices-tab'
}, {
	anchor : 'purchord-link',
	block : 'purchord-tab'
}, {
	anchor : 'contractrev-link',
	block : 'contractrev-tab'
}, {
	anchor : 'costs-link',
	block : 'costs-tab'
}, {
	anchor : 'email-link',
	block : 'email-tab'
}, {
	anchor : 'batchcal-link',
	block : 'batchcal-tab'
}, {
	anchor : 'jobsheet-link',
	block : 'jobsheet-tab'
}, {
	anchor : 'jobcourier-link',
	block : 'jobcourier-tab'
}, {
	anchor : 'newdeliveries-link',
	block : 'newdeliveries-tab'
}];

/**
 * this variable holds an array of id objects, one for the navigation link and
 * one for the content area the link refers to. this array is passed to the
 * Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements2 = [{
	anchor : 'jobitem_group-link',
	block : 'jobitem_group-tab'
}, {
	anchor : 'jobitem_default-link',
	block : 'jobitem_default-tab'
}];

/**
 * this variable holds an array of id objects, one for the navigation link and
 * one for the content area the link refers to. this array is passed to the
 * Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements3 = [{
	anchor : 'jcgeneral-link',
	block : 'jcgeneral-tab'
}, {
	anchor : 'jcbreakdown-link',
	block : 'jcbreakdown-tab'
}];

var jobServiceModelsWithPrice = [];

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if
 * present. this allows onload coding to be included in the page specific
 * javascript file without adding to the body onload attribute.
 */
function init() {
	// load stylesheet using the dom if not already
	loadScript.loadStyle('styles/jQuery/jquery-boxy.css', 'screen');
	// if there is already a quick item to scan in on this job
	if (quickitembc != 0) {
		// add the barcode to the quick item scan field and trigger the click
		// event
		$j('input#bc').val(quickitembc).next().trigger('keyup');
		// clear quickitembc so it is not called a second time
		quickitembc = 0;
	}
	changeServiceModelSearch();
	initUpdateDeliveryDetails();
}

/**
 * this function creates the content of the jquery boxy used when creating a new
 * purchase order
 * 
 * @param {String}
 *            type contains either 'Edit' or 'Create' depending on the
 *            functionality required
 * @param {String}
 *            jobno is a string containing the current job number
 * @param {Integer}
 *            id is the id of either a job if creating a new po or of a po if
 *            editing a po
 * @param {String}
 *            ponumber is the current po number used to prefill input field when
 *            editing
 * @param {String}
 *            pocomment is the current po comment used to prefill input field
 *            when editing
 */
function addEditPurchaseOrdContent(type, jobno, jobid, poid) {
	// start creating jquery boxy content string
	var titleAction = '';
	if (type == 'Create') {
		titleAction = i18n.t("core.jobs:createPOForJob", {
			varJobNo : jobno,
			defaultValue : type + " Purchase Order For Job " + jobno
		});
	} else {
		titleAction = i18n.t("core.jobs:editPOForJob", {
			varJobNo : jobno,
			defaultValue : "Edit Purchase Order For Job " + jobno
		});
	}
	$j.ajax({
		url : "jobitemsonclientpurchaseorder.htm",
		data : {
			titleAction : titleAction,
			poId : poid,
			jobId : jobid
		},
		async : true
	}).done(function(content) {
		// create new boxy pop up using content variable
		var createNewPOBoxy = new Boxy(content, {
			title : titleAction,
			modal : true,
			unloadOnHide : true
		});
		// show boxy pop up and adjust size
		createNewPOBoxy.tween(720, 340);
		// apply focus to po number field
		$j('div#createNewPOBoxy input#ponumber').focus();
	});
}

/**
 * this function creates a new purchase order for the currently selected job and
 * displays a message depending on the result.
 * 
 * @param {Integer}
 *            jobid is the id of the currently selected job
 */
function createPurchOrd(jobid) {
	// get po number
	var poNumber = $j('div#createNewPOBoxy input[name="ponumber"]').val();
	// get po comment
	var poComment = $j('div#createNewPOBoxy textarea[name="pocomment"]').val();
	var poTableElement = document.querySelector('cwms-po-table');
	poTableElement.createPurchaseOrder(poNumber, poComment);
	Boxy.get($j('div#createNewPOBoxy')).hide();
	// reload the view job page selecting the client po tab
	window.location.href = 'viewjob.htm?jobid=' + jobid + '&loadtab=clientpurchord-tab';
}

/**
 * this function updates an edited purchase order and amends the values
 * displayed in the table
 * 
 * @param {Integer}
 *            poid is the id of the currently selected purchase order
 * @param {Integer}
 *            jobid is the id of the currently selected job
 * @param {String}
 *            jobno is the job number of current job
 */
function editPurchOrd(poId, jobId, jobNo) {
	// get po number
	var poNumber = $j('div#createNewPOBoxy input[name="ponumber"]').val();
	// get po comment
	var poComment = $j('div#createNewPOBoxy textarea[name="pocomment"]').val();
	var poTableElement = document.querySelector('cwms-po-table');
	poTableElement.editPurchaseOrder(poId, poNumber, poComment);
	Boxy.get($j('div#createNewPOBoxy')).hide();
}

/*
 * timestamp value of last DWR call, checked against the current DWR call
 */
var lastUpdate = 0;

/**
 * this function creates the content of the jquery boxy used when viewing
 * available BPO's
 * 
 * @param {Integer}
 *            personid id of contact on the job
 * @param {Integer}
 *            jobid id of the current job
 * @param {String}
 *            currencySymbol the job currency symbol
 */
function viewAvailableBPOs(personid, jobid, currencySymbol) {
	function bpoRowFunction(jobid){
		return bpo => 
		`<tr>
			<td>${bpo.poNumber}</td>
			<td>${bpo.comment}</td>
			<td class="center">${cwms.dateFormatFunction(bpo.durationFrom)}</td>
			<td class="center">${cwms.dateFormatFunction(bpo.durationTo)}</td>
			<td class="center">${currencySymbol} ${bpo.limitAmount}</td>
			<td><a href="#" class="mainlink" onclick="event.preventDefault(); assignBPOToJob(${jobid},${bpo.poId})"><cwms-translate code="assign">Assign</cwms-translate>
			</a></td>
			</tr>`;
	}
	$j
			.ajax({
				url : "contactrelatedbpos.json",
				data : {
					personid : personid,
					jobid : jobid
				},
				async : true
			})
			.done(
					function(result) {
						var content = '';
						// blanket purchase orders returned
						var contactBPOs = result['CONTACT'];
						var subdivBPOs = result['SUBDIV'];
						var companyBPOs = result['COMPANY'];
						var sum = contactBPOs.length + subdivBPOs.length
								+ companyBPOs.length;
						if (sum > 0) {
							// start creating jquery boxy content string
							content += `<div id="viewAvailableBPOBoxy" class="overflow">
									<table class="default2" id="bpotable" summary="This table displays all available blanket purchase orders">
									<thead>
									<tr>
									<th id="bponum" scope="col">
									 <cwms-translte2 code="bpo.bpono">BPO No</cwms-translate>
									</th>
									<th id="bpocom" scope="col">
									 <cwms-translte2 code="bpo.comment">BPO Comment</cwms-translat2>
									</th>
									<th id="bpovalfrom" scope="col">
									 <cwms-translte2 code="bpo.validfrom">Valid From</cwms-translat2>
									</th>
									<th id="bpovalto" scope="col">
									 <cwms-translte2 code="bpo.validto">Valid To</cwms-translat2>
									</th>
									<th id="bpolimit" scope="col">
									 <cwms-translte2 code="bpo.limitammount">Limit Ammount</cwms-translat2>
									</th>
									<th id="bpoassign" scope="col">
									 <cwms-translte2 code="bpo.assignbpo">Assign BPO</cwms-translat2>
									</th>
									</tr>
									</thead><tbody>`;
							// contact BPO's returned
							if (contactBPOs.length > 0) {
								// create heading row for contact BPO's and add
								// to string
								content += `<tr>
										<th colspan="6">
									 <cwms-translte2 code="bpo.cpmtactpbo">Contact BPO's</cwms-translat2>:
										 ${contactBPOs[0].contactName} </th>
										</tr>`;
								// add row for each contact BPO returned to
								// string
								content = contactBPOs.map(bpoRowFunction(jobid)).reduce((acc,v) =>acc +v,content);
							}
							// subdivision BPO's returned
							if (subdivBPOs.length > 0) {
								// create heading row for subdivision BPO's and
								// add to string
								content += `<tr>
										<th colspan="6">
											<cwms-translate code="bpo.subdivisionsbpo">Subdivision BPO's</cwms-translate>: ${subdivBPOs[0].subdivName} </th>
											</tr>`;
								// add row for each subdivision BPO returned to
								// string
								content = subdivBPOs.map(bpoRowFunction(jobid)).reduce((acc,v) =>acc +v,content);
							}
							// company BPO's returned
							if (companyBPOs.length > 0) {
								// create heading row for the company BPO's and
								// add to string
								content += `<tr>
										<th colspan="6">
											<cwms-translate code="bpo.companypobs">Company BPO's</cwms-translate>: ${companyBPOs[0].companyName}  </th>
										</tr>`;
								// add row for each company BPO returned to
								// string
								content = companyBPOs.map(bpoRowFunction(jobid)).reduce((acc,v) =>acc +v,content);
							}
							// complete the string
							content += `</tbody></table></div>`;
						} else {
							// populate variable with message of no results
							content = '<div class="center bold">'
									+ i18n.t("core.jobs:job.noPBOsToDisplay",
											"No BPO's to display") + '</div>';
						}
						// create new boxy pop up using content variable
						var viewAvailableBPOBoxy = new Boxy(content, {
							title : i18n.t("core.jobs:job.viewAvailableBPOs",
									"View Available BPO's"),
							modal : true,
							unloadOnHide : true
						});
						// show boxy pop up and adjust size
						viewAvailableBPOBoxy.tween(720, 340);
						// has bpo table been created?
						if ($j('table#bpotable').length) {
							// apply zebra effect
							$j('table#bpotable').zebra();
						}
					});
}


/**
 * this function assigns an available BPO to the job
 * 
 * @param {Integer}
 *            jobid id of the current job
 * @param {Integer}
 *            bpoid id of the bpo to be assigned
 */
function assignBPOToJob(jobId, bpoId) {
	$j.ajax({
		url : "assignbpotojob.json",
		data : {
			jobid : jobId,
			bpoid : bpoId
		},
		async : true
	}).done(function(result) {
		var poTableElement = document.querySelector('cwms-po-table');
		poTableElement.addBPOToJob(bpoId);
		Boxy.get($j('div#viewAvailableBPOBoxy')).hide();
	});

	/*
	 * // load the service javascript file if not already
	 * loadScript.loadScriptsDynamic(new Array('dwr/interface/bposervice.js'),
	 * true, { callback: function() { // call dwr service to assign a BPO to job
	 * bposervice.addBPOToJob(jobid, bpoid, { callback:function(BpoWrap) {
	 * 
	 * }); } });
	 */
}

/**
 * this function removes a currently assigned BPO from the job
 * 
 * @param {Integer}
 *            bpoId id of the blanket purchase order
 * @param {Integer}
 *            jobid id of the current job
 * @param {Integer}
 *            personid id of the contact on job
 * @param {Integer}
 *            availBPOs number of available BPO's
 * 
 * GB 2016-09-27: Changed to remove call to BPOService.removeBPOFromJob, as this
 * was clearing the BPO, such that deleteBPOLinksToJob() was then doing nothing
 * as there was no BPO linked to job on second call.
 */
function removeBPO(bpoId, jobid, personid, availBPOs, currencySymbol) {
	$j.ajax({
		url : "removeJobBPO.json",
		data : {
			jobid : jobid,
			bpoid : bpoId
		},
		method : "POST",
		async : true
	}).done(
			function(content) {
				// remove child row
				// containing items if
				// present
				$j('table#clientpurchordtable tbody tr#child' + bpoId).remove();
				// append link to view
				// available BPO's
				$j('table#clientpurchordtable tbody tr#bpo' + bpoId).remove();
				if($j('table#clientpurchordtable tbody:first').next('tbody').find('tr[id^="bpo"]').length == 0) {
					$j('table#clientpurchordtable tbody:last tr:last').after(
									'<tr><td colspan="8" class="bold center">'
											+ i18n
													.t(
															"core.jobs:job.noPBOsAssigned",
															"'No BPO's Assigned")
											+ ' ('
											+ '<a href="#" class="mainlink" onclick=" viewAvailableBPOs('
											+ personid
											+ ', '
											+ jobid
											+ ', \''
											+ currencySymbol
											+ '\'); return false; " title="">'
											+ availBPOs
											+ ' '
											+ i18n
													.t(
															"core.jobs:job.available",
															"Available")
											+ '</a>'
											+ ')'
											+ '</td></tr>');
					$j('table#clientpurchordtable tfoot tr td a').addClass('hid');
				}
				// subtract one from the
				// purchase order menu item
				// count
				$j('span.cpoSizeSpan')
						.text(
								parseInt($j(
										'span.cpoSizeSpan:first')
										.text()) - 1);
				// remove BPO from job
				// header expanding pos
				$j(
						'div.expandingPOs div:first')
						.empty();
				// how many pos displayed?
				if ($j('div.expandingPOs > div:last > div').length > 1) {
					// work out height to
					// expand
					var expandPos = $j('div.expandingPOs > div:last > div').length * 16;
					// append new expand
					// link for pos
					$j(
							'div.expandingPOs > div:last > div:first')
							.append(
									'<a href="#" onclick=" $j(this).parent().parent().parent().animate( { height: \''
											+ expandPos
											+ '\' }, 1000 ); return false; ">'
											+ '<img src="img/icons/viewactions.png" width="16" height="16" class="img_inl_margleft" />'
											+ '</a>');
				}
			}).fail(function(error) {
		$j.prompt(error.message);
	});
}

/**
 * this function creates the content of the jquery boxy used when adding job
 * items to a purchase order
 * 
 * @param {Integer}
 *            jobId id of the current job
 * @param {Integer}
 *            poId id of the purchase order
 * @param {String}
 *            poNo number of the purchase order
 */
function addJobItemsToPO(jobId, poId) {
	$j.ajax({
		url : "addJobItemsToPO.htm",
		data : {
			jobId : jobId,
			poId : poId
		},
		async : true
	}).done(
			function(content) {
				// create new boxy pop up using content variable
				var addJobItemsToPOBoxy = new Boxy(content, {
					title : i18n.t("core.jobs:job.addJobItemsToPO",
							"Add Job Items To PO"),
					modal : true,
					unloadOnHide : true
				});
				// show boxy pop up and adjust size
				addJobItemsToPOBoxy.tween(900, 400);
				// apply zebra effect
				$j('table#jobitemtopotable').zebra();
			}).fail(function(error) {
		$j.prompt(error.message);
	});
}

/**
 * this function creates the content of the jquery boxy used when adding job
 * items to a blanket purchase order
 * 
 * @param {Integer}
 *            jobid id of the current job
 * @param {Integer}
 *            poid id of the blanket purchase order
 */
function addJobItemsToBPO(jobId, poId, poNo) {
	$j.ajax({
		url : "addJobItemsToBPO.htm",
		data : {
			jobId : jobId,
			poId : poId,
			poNo : poNo
		},
		async : true
	}).done(
			function(content) {
				// create new boxy pop up using content variable
				var addJobItemsToBPOBoxy = new Boxy(content, {
					title : i18n.t("core.jobs:job.addJobItemsToBPO",
							"Add Job Items To BPO"),
					modal : true,
					unloadOnHide : true
				});
				// show boxy pop up and adjust size
				addJobItemsToBPOBoxy.tween(900, 400);
				// apply zebra effect
				$j('table#jobitemtobpotable').zebra();
			}).fail(function(error) {
		$j.prompt(error.message);
	});
}

/**
 * this method finds all job items selected in the add items to purchase order
 * jquery boxy and calls a dwr method to update the purchase order.
 * 
 * @param {Integer}
 *            poid id of the purchase order items are to be added to/removed
 *            from
 */
function submitPurchaseOrderJobItems(poid, jobid) {
	// create new arrays for selected job items and services
	var jiarray = [];
	var jeiarray = [];
	// get all selected job items and services
	$j('table#jobitemtopotable input[name="jobitemforpo"]:checked').each(
			function(i, n) {
				jiarray.push(n.value);
			});
	$j('table#expenseitemtopotable input[name="jobserviceforpo"]:checked').each(
			function(i, n) {
				jeiarray.push(n.value);
			});
	// ajax call
	$j.ajax({
		url : "resetitemsonpo.json",
		data : {
			jobitemsid : jiarray,
			jobservicesid : jeiarray,
			poid : poid,
			jobid : jobid
		},
		method: "POST"
	})
	.done(
			function(result) {
				if (result == true) {
					var poTableElement = document.querySelector('cwms-po-table');
					poTableElement.purchaseOrderDetails.delete(poid.toString());
					poTableElement.purchaseOrderItemsMap.delete(poid.toString());
					poTableElement.purchaseOrderExpenseItemsMap.delete(poid.toString());
					var po = poTableElement.purchaseOrders.find(po => po.poId == poid);
					po.countJobItems = jiarray.length;
					po.countExpenseItems = jeiarray.length;
					poTableElement.update();
					Boxy.get($j('div#addJobItemsToPOBoxy')).hide();
				}
			}).fail(function(error) {
		$j.prompt(error.message);
	});
}

function submitBlanketPurchaseOrderJobItems(bpoid, bpono, jobid) {
	// create new arrays for selected job items and services
	document.getElementById("submitData").show();
	var jiarray = [];
	var jeiarray = [];
	// get all selected job items and services
	$j('table#jobitemtopotable input[name="jobitemforpo"]:checked').each(
			function(i, n) {
				jiarray.push(n.value);
			});
	$j('table#expenseitemtopotable input[name="jobserviceforpo"]:checked').each(
			function(i, n) {
				jeiarray.push(n.value);
			});
	// ajax call
	$j.ajax({
		url : "assignjobitemstobpo.json",
		data : {
			jobitemsid : jiarray,
			jobservicesid : jeiarray,
			bpoid : bpoid,
			jobid : jobid
		},
		method: "POST"
	})
	.done(
			function(result) {
				if (result == true) {
					var poTableElement = document.querySelector('cwms-po-table');
					poTableElement.blanketPurchaseOrderDetails.delete(bpoid.toString());
					poTableElement.blanketPurchaseOrderItemsMap.delete(bpoid.toString());
					poTableElement.blanketPurchaseOrderExpenseItemsMap.delete(bpoid.toString());
					var bpo = poTableElement.blanketPurchaseOrders.find(bpo => bpo.poId == bpoid);
					bpo.addedItems = jiarray.length;
					bpo.addedExpenseItems = jeiarray.length;
					poTableElement.update();
					document.getElementById("submitData").hide()
					Boxy.get($j('div#addJobItemsToPOBoxy')).hide();
				} 
			}).fail(function(error) {
		$j.prompt(error.message);
	});
}


function unassignejobitemAndJobServicesFromBPO(checkbox,jobitemId,jobserviceId,bpoid,jobid){
	
	if(checkbox.prop('checked')==false){
		$j.ajax({
			url : "unsignJobitemsAndJobServicesToBPO.json",
			data : {
				jobitemid : jobitemId,
				jobserviceid : jobserviceId,
				bpoid : bpoid,
				jobid : jobid
			},
			method: "POST"
		})
	}
}

/**
 * this method copies the selected option to all other select boxes in table
 * 
 * @param {String}
 *            id id of the object in which all selects should be copied
 * @param {String}
 *            elementtype the string representation of element type (e.g. input,
 *            select)
 * @param {String}
 *            element the element itself
 */
function copySelectedOption(id, elementtype, element) {
	$j(
			'#' + id + ' ' + elementtype + '[name="' + $j(element).attr('name')
					+ '"]').each(function(i, n) {
		$j(this).val($j(element).val());
	});
}

/**
 * this method checks all inputs of type checkbox that start with the given name
 * 
 * @param {Boolean}
 *            select true if boxes are to be checked and vice versa
 * @param {String}
 *            inputName name of the inputs to be checked
 */
function selectAllNamedInputs(select, inputName) {
	// check all
	if (select) {
		// check all checkboxes and trigger click event to display options
		$j('input:checkbox[name^="' + inputName + '"]').check('on').each(
				function(i) {
					if ($j(this).attr('onclick') != '') {
						$j(this).trigger('onclick');
					}
				});
	}
	// uncheck all
	else {
		// un-check all checkboxes and trigger click event to hide options
		$j('input:checkbox[name^="' + inputName + '"]').check('off').each(
				function(i) {
					if ($j(this).attr('onclick') != '') {
						$j(this).trigger('onclick');
					}
				});
	}
}

/**
 * this method checks all inputs of type checkbox that are contained within the
 * table
 * 
 * @param {Boolean}
 *            select true if boxes are to be checked and vice versa
 * @param {String}
 *            parentElement id of table in which to search for inputs of type
 *            checkbox
 */
function selectAll(select, parentElement) {
	// check all
	if (select) {
		$j('#' + parentElement + ' input:checkbox').check('on');
	}
	// uncheck all
	else {
		$j('#' + parentElement + ' input:checkbox').check('off');
	}
}

/**
 * this function creates the content used in an overlay which allows the user to
 * select job items to print labels for (migrated from DWR/javascript writing)
 * 
 * @param {Integer}
 *            jobid is the id of a job
 */
function printMultipleJobItemLabelsContent(contentUrl) {
	$j.get(contentUrl).done(
			function(content) {
				// create new overlay
				loadScript.createOverlay('dynamic', i18n.t(
						"core.jobs:job.printMultipleJobItemLabels",
						"Print Multiple Job Item Labels"), null, content, null,
						60, 920, null);
			});
}

/**
 * this method deletes a job quote link and removes the linked quotation from
 * the linked quotes table.
 * 
 * @param {Integer}
 *            id id of the linked quotation to be removed
 */
function removeJobQuoteLink(id) {
	$j
			.ajax({
				url : "deletejobquotelink.json",
				data : {
					jobQuoteLinkId : id
				},
				async : true
			})
			.done(
					function() {
						// remove the deleted job quote link from linked quotes
						// table
						$j('tr#quote' + id).remove();
						// check to see if all linked quotes have been deleted
						if ($j('table#linkedquotes tbody tr').length < 1) {
							$j('table#linkedquotes tbody')
									.append(
											'<tr>'
													+ '<td colspan="6" class="bold center">'
													+ i18n
															.t(
																	"core.jobs:job.noQuotationsLinkedToThisJob",
																	"No quotations have been linked to this job")
													+ '</td>' + '</tr>');
						}
						// subtract one from the linked quotes count
						$j('span#linkedCount').text(
								parseInt($j('span#linkedCount').text() - 1));
					}).fail(function(error) {
				$j.prompt(error.message);
			});
}




/**
 * this method creates a new empty item group
 * 
 * @param {Integer}
 *            jobId this is the id of the job for which we are creating a new
 *            group
 * @param {Integer}
 *            fastTrackTurn the number of days for a fast track item
 */
function createNewGroup(jobId, fastTrackTurn) {
	// load the service javascript file if not already
	loadScript
			.loadScriptsDynamic(
					new Array('dwr/interface/jobitemgroupservice.js'),
					true,
					{
						callback : function() {
							// call dwr service to create the new group
							jobitemgroupservice
									.newGroupForJob(
											jobId,
											{
												callback : function(result) {
													if (!result.success) {
														$j
																.prompt(result.message);
													} else {
														// assign new group to
														// variable
														var group = result.results;
														// add new group table
														// to page
														$j(
																'table#ungroupedItems')
																.before(
																		'<table class="default4 groupedItems" summary="">'
																				+ '<tbody id="group'
																				+ group.id
																				+ '">'
																				+ '<tr class="groupHead">'
																				+ '<th class="type">'
																				+ '<a href="#" class="mainlink" onclick=" deleteGroup('
																				+ group.id
																				+ ', '
																				+ jobId
																				+ ', '
																				+ fastTrackTurn
																				+ '); return false; ">'
																				+ '<img src="img/icons/delete.png" width="16" height="16" alt="'
																				+ i18n
																						.t(
																								"core.jobs:job.deleteGroup",
																								"Delete Group")
																				+ '" title="'
																				+ i18n
																						.t(
																								"core.jobs:job.deleteGroup",
																								"Delete Group")
																				+ '" class="img_marg_bot" />'
																				+ '</a> '
																				+ i18n
																						.t(
																								"core.jobs:job.group",
																								"Group")
																				+ ' '
																				+ group.id
																				+ '</th>'
																				+ '<th class="rest" colspan="7">&nbsp;</th>'
																				+ '</tr>'
																				+ '<tr class="groupMessage">'
																				+ '<td colspan="8" class="bold center">'
																				+ i18n
																						.t(
																								"core.jobs:job.noItemsToDisplayIngroup",
																								"There are no items to display in this group")
																				+ '</td>'
																				+ '</tr>'
																				+ '</tbody>'
																				+ '</table>');
														// add new group to
														// select boxes
														$j(
																'table.groupedItems select[name="groupselect"]')
																.append(
																		'<option value="'
																				+ group.id
																				+ '">'
																				+ group.id
																				+ '</option>');
														// add group to array
														groups.push(group.id);
													}
												}
											});
						}
					});
}

/**
 * this method deletes an item group and moves all items that were within that
 * group to the ungrouped table.
 * 
 * @param {Integer}
 *            groupId the id of the current group in which the job item is
 *            located
 * @param {Integer}
 *            jobId the id of the current job
 * @param {Integer}
 *            fastTrackTurn the number of days for a fast track item
 */
function deleteGroup(groupId, jobId, fastTrackTurn) {
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array(
			'dwr/interface/jobitemgroupservice.js'), true, {
		callback : function() {
			// call dwr service to delete group, any items in the group will be
			// set as un-grouped items
			jobitemgroupservice.deleteGroupFromJob(groupId, {
				callback : function(result) {
					if (!result.success) {
						$j.prompt(result.message);
					} else {
						// assign job items to variable
						var jobitems = result.results;
						// remove group from array
						$j.each(groups, function(i) {
							if (groups[i] == groupId) {
								groups.splice(i, 1);

							}
						});
						// loop through job items and add to ungrouped items
						$j.each(jobitems, function(i) {
							// call method to add ungrouped item
							addUngroupedItem(groupId, jobId,
									jobitems[i].jobItemId, jobitems[i].itemNo,
									0, fastTrackTurn,
									jobitems[i].calType.calTypeId);
							// update group number in main job items tab
							$j(
									'table#jobitemlist tbody tr#ji'
											+ jobitems[i].jobItemId
											+ ' td.group').html('');
						});
						// initialise all jquery tooltips
						JT_init();
						// remove the group table from the page
						$j('tbody#group' + groupId).parent().remove();
						// remove group id option from selects
						$j(
								'table.groupedItems select[name="groupselect"] option[value="'
										+ groupId + '"]').remove();
					}
				}
			});
		}
	});
}

/**
 * this method moves a job item from one group to another, slotting the item in
 * to the new group depending on the item no.
 * 
 * @param {Integer}
 *            groupId the id of the current group in which the job item is
 *            located
 * @param {Integer}
 *            jobItemId the id of the job item to be moved
 * @param {Integer}
 *            itemNo the item number of the job item to be moved
 * @param {Integer}
 *            newGroupId the id of the new group in which the job item is to be
 *            put
 * @param {Integer}
 *            fastTrackTurn the number of days for a fast track item
 * @param {Integer}
 *            calTypeId the caltype id of the job item to be passed to move
 *            group item method
 */

function moveGroupItem(jobItemId,itemNo,newGroupId,groupId) {
	$j.ajax({
		url: "updatejobitemgroup.json",
		data: {
			jobItemId : jobItemId,
			groupId: newGroupId
		},
		async: true
	}).done(function() {
		var tr=$j("#gji"+itemNo).remove();
		tr.detach();
		
		$j("#noelement").remove();
		
		$j("#group"+newGroupId).append(tr);
		
		if ($j('tbody#group'+ groupId+ ' tr').not('.groupHead').length < 1) {
			
			$j('tbody#group'+ groupId).append(
							'<tr class="groupMessage">'
									+ '<td colspan="8" class="bold center">'
										+ i18n.t("core.jobs:job.noItemsToDisplayIngroup","There are no items to display in this group")
									+ '</td>'
							+ '</tr>');
		}
	}).fail(function(result) {
		$j.prompt(result.responseText);
	});
	
}

/**
 * this method
 * 
 * @param {Integer}
 *            groupId the id of the current group in which the job item is
 *            located
 * @param {Integer}
 *            jobId the id of the current job
 * @param {Integer}
 *            jobItemId the id of the job item to be moved
 * @param {Integer}
 *            itemNo the item number of the job item to be moved
 * @param {Integer}
 *            newGroupId the id of the new group in which the job item is to be
 *            put
 * @param {Integer}
 *            fastTrackTurn the number of days for a fast track item
 * @param {Integer}
 *            calTypeId the caltype id of the job item to be passed to move
 *            group item method
 */
function addUngroupedItem(groupId, jobId, jobItemId, itemNo, newGroupId,
		fastTrackTurn, calTypeId) {
	// variable to hold select content
	var selectContent = '<select name="groupselect" onchange=" if (this.value != '
			+ newGroupId
			+ '){ moveGroupItem('
			+ newGroupId
			+ ', '
			+ jobId
			+ ', '
			+ jobItemId
			+ ', '
			+ itemNo
			+ ', this.value, '
			+ fastTrackTurn
			+ ', '
			+ calTypeId
			+ '); return false; } ">'
			+ '<option value="0">' + i18n.t("none", "None") + '</option>';
	$j.each(groups, function(i) {
		selectContent += '<option value="' + groups[i] + '">' + groups[i]
				+ '</option>';
	});
	selectContent += '</select>';
	// change the move select for the job item that is going to be moved
	$j('tbody#group' + groupId + ' tr#gji' + itemNo).find('td:last').html(
			selectContent);
	// does the new group have a message of no group items?
	if ($j('tbody#group' + newGroupId + ' tr.groupMessage').length > 0) {
		// remove the message
		$j('tbody#group' + newGroupId + ' tr.groupMessage').remove();
		// append the item to be moved into the new group table
		$j('tbody#group' + newGroupId).append(
				'<tr id="gji'
						+ itemNo
						+ '" style=" background-color: '
						+ $j('tbody#group' + groupId + ' tr#gji' + itemNo).css(
								'background-color')
						+ '; ">'
						+ $j('tbody#group' + groupId + ' tr#gji' + itemNo)
								.html() + '</tr>');
	} else {
		// holds id of the job item that the new one should be appended before
		// or after
		var placementId = '';
		// should the job item be appended before or after the placement id job
		// item
		var before = false;
		// empty array to be populated with job item ids
		var idArray = [];
		// get all job items rows apart from the header and add the ids to the
		// array
		$j('tbody#group' + newGroupId + ' tr').not('.groupHead').each(
				function(i) {
					idArray.push(this.id);
				});
		// loop through the id array and find location for new group item
		$j.each(idArray, function(i) {
			// job item to move is less than job item id in array?
			if (parseInt(itemNo) < parseInt(idArray[i].substring(3,
					idArray[i].length))) {
				// assign id to variable
				placementId = idArray[i];
				// set before variable to true
				before = true;
				// break out of loop as we have found location for new item
				return false;
				// the new item is to be appended before the first item in the
				// current group or
				// between two items in the current group
			} else if (parseInt(itemNo) > parseInt(idArray[i].substring(3,
					idArray[i].length))) {
				// assign id to variable
				placementId = idArray[i];
				// the new item is to be appended after the last item in the
				// current group
			}
		});
		// append before first or between two items?
		if (before) {
			// append the job item to move either before the first item in group
			// or between two items in group
			$j('tbody#group' + newGroupId).find('tr#' + placementId).before(
					'<tr id="gji'
							+ itemNo
							+ '" style=" background-color: '
							+ $j('tbody#group' + groupId + ' tr#gji' + itemNo)
									.css('background-color')
							+ '; ">'
							+ $j('tbody#group' + groupId + ' tr#gji' + itemNo)
									.html() + '</tr>');
		} else {
			// append the job item to move after the last item in group
			$j('tbody#group' + newGroupId).find('tr#' + placementId).after(
					'<tr id="gji'
							+ itemNo
							+ '" style=" background-color: '
							+ $j('tbody#group' + groupId + ' tr#gji' + itemNo)
									.css('background-color')
							+ '; ">'
							+ $j('tbody#group' + groupId + ' tr#gji' + itemNo)
									.html() + '</tr>');
		}
	}
}

/**
 * this method toggles the visibility of calibrations in the batch calibration
 * table
 * 
 * @param {Integer}
 *            rowcount the current row number
 * @param {String}
 *            linkId the id of the show calibrations link clicked
 */
function toggleCalibrations(rowcount, linkId) {
	// calibrations are currently hidden so make them visible
	if ($j('#batchchild' + rowcount).css('display') == 'none') {
		// remove image to show calibrations and append hide calibrations image
		$j('#' + linkId).empty().append(
				'<img src="img/icons/items_hide.png" width="16" height="16" alt="'
						+ i18n.t("core.jobs:job.hideCal", "Hide Calibrations")
						+ '" title="'
						+ i18n.t("core.jobs:job.hideCal", "Hide Calibrations")
						+ '" class="image_inline" />');
		// make the calibrations row visible and add slide down effect
		$j('#batchchild' + rowcount).removeClass('hid').addClass('vis').find(
				'div').slideDown(1000);
	}
	// calibrations are currently visible so hide them
	else {
		// remove image to hide calibrations and append show calibrations image
		$j('#' + linkId).empty().append(
				'<img src="img/icons/items.png" width="16" height="16" alt="'
						+ i18n.t("core.jobs:job.viewCal", "View Calibrations")
						+ '" title="'
						+ i18n.t("core.jobs:job.viewCal", "View Calibrations")
						+ '" class="image_inline" />');
		// add effect to slide up calibrations
		$j('#batchchild' + rowcount + ' div').slideUp(1000, function() {
			// hide row
			$j('#batchchild' + rowcount).removeClass('vis').addClass('hid');
		});
	}
}

/**
 * this method loads the necessary files to create a boxy pop up, creates the
 * content for the pop up and then creates an instance of the pop up to display
 * the delete job content.
 * 
 * @param {Integer}
 *            jobid the id of the job to delete
 */
function showDeleteJobForm(jobid) {
	// create content
	var content = '<div id="deleteJobBoxy" class="overflow">' +
	// add warning box to display error messages if the insertion or update
	// fails
	'<div class="hid warningBox1">' + '<div class="warningBox2">'
			+ '<div class="warningBox3">' + '</div>' + '</div>' + '</div>'
			+ '<fieldset>' + '<legend>'
			+ i18n.t("core.jobs:job.enterReasonForDeletion",
					"Enter a reason for this deletion:")
			+ '</legend>'
			+ '<ol>'
			+ '<li>'
			+ '<label>'
			+ i18n.t("reasonLabel", "Reason:")
			+ '</label>'
			+ '<textarea name="deletereason" id="deletereason" rows="3" cols="60"></textarea>'
			+ '</li>'
			+ '</ol>'
			+ '<ol>'
			+ '<li>'
			+ '<label>&nbsp;</label>'
			+ '<input type="button" value="'
			+ i18n.t("delete", "Delete")
			+ '" onclick="deleteJob('
			+ jobid
			+ ', $j(\'#deletereason\').val()); return false;"/>'
			+ '</li>'
			+ '</ol>' + '</fieldset>' + '</div>';
	// create new boxy pop up using content variable
	var deleteJobBoxy = new Boxy(content, {
		title : i18n.t("core.jobs:job.deleteJob", "Delete Job"),
		modal : true,
		unloadOnHide : true
	});
	// show boxy pop up and adjust size
	deleteJobBoxy.tween(600, 220);
}

/**
 * this method deletes a job (if allowed, due to limitations), closes the boxy
 * implementation and redirects the user.
 * 
 * @param {Integer}
 *            jobid the id of the job to be deleted
 * @param {String}
 *            reason the reason for deleting the job
 */
function deleteJob(jobid, reason) {
	// load the service javascript file if not already
	loadScript
			.loadScriptsDynamic(
					new Array('dwr/interface/jobservice.js'),
					true,
					{
						callback : function() {
							// call dwr service to delete the selected job
							jobservice
									.deleteJobWithChecks(
											jobid,
											reason,
											{
												callback : function(result) {
													// error occured deleting
													// job
													if (!result.success) {
														// variable for errors
														var errors = '';
														if (result.message == null) {
															// get errors from
															// list and add to
															// variable
															for (var i = 0; i < result.errorList.length; i++) {
																var error = result.errorList[i];
																errors += i18n
																		.t(
																				"fieldError",
																				{
																					varErrField : error.field,
																					varErrMsg : error.defaultMessage,
																					defaultValue : "Field: "
																							+ error.field
																							+ " failed. Message:"
																							+ error.defaultMessage
																				})
																		+ '<br />';
															}
														} else {
															// get single error
															// message and add
															// to variable
															errors += result.message;
														}
														// empty the error
														// warning box
														$j(
																'div#deleteJobBoxy div.warningBox3')
																.empty();
														// show error message in
														// error warning box
														$j(
																'div#deleteJobBoxy div:first')
																.removeClass(
																		'hid')
																.addClass('vis')
																.find(
																		'.warningBox3')
																.text(errors);
													} else {
														// get the current
														// delete job boxy
														// implementation and
														// close it.
														Boxy
																.get(
																		$j('div#deleteJobBoxy'))
																.hide();
														// it worked, so just
														// redirect away from
														// this page to the
														// deleted components
														// page
														window.location = springUrl
																+ '/deletedcomponent.htm';
													}
												}
											});
						}
					});
}

/**
 * To replace generateJobSheet() once accepted
 * 
 * @param jobid
 * @returns
 */

function generateNewJobSheet(jobid, url, message) {
	// loading image and message content
	var content = 	'<div class="text-center padding10">' +
						'<img src="img/icons/ajax-loader.gif" width="31" height="31" alt="" />' +
						'<br /> <h4>' + message + '</h4>' +
					'</div>';
	// create new overlay
	loadScript.createOverlay('dynamic', null, null, content, null, 18, 500, null);
	// load document generation page
	window.location.href = '' + url + '' + jobid;
}

/**
 * this method creates a job sheet. Note, this is the "old version" code!
 * 
 * @param {Integer}
 *            jobid the id of the job
 */
function generateOldJobSheet(jobid, newVersion) {
	// disable the submit buttons while the job sheet is being generated
	$j('#js_submit, #js_submit_p').attr('disabled', 'disabled');
	// loading image and message content
	var content = '<div class="center-pad">'
			+ '<img src="img/icons/ajax-loader.gif" width="31" height="31" alt="" />'
			+ '<br /> <h4>'
			+ i18n.t("core.jobs:job.generatingJobSheetDoc",
					"Generating Job Sheet Document") + '</h4>' + '</div>';
	// create overlay for message
	loadScript.createOverlay('dynamic', null, null, content, null, 18, 480,
			null);
	// load the service javascript file if not already
	loadScript
			.loadScriptsDynamic(
					new Array('dwr/interface/jobservice.js'),
					true,
					{
						callback : function() {
							// call dwr service to create job sheet
							jobservice
									.generateJobSheet(
											jobid,
											{
												callback : function(result) {
													// document generation
													// successful?
													if (result.success) {
														// job file root anchor
														// present?
														if ($j('a#fbRootAnchor').length) {
															// refresh all files
															// in the root of
															// job folder
															$j('a#fbRootAnchor')
																	.parent()
																	.removeClass()
																	.addClass(
																			'directory collapsed')
																	.end()
																	.trigger(
																			'onclick');
														}
														// switch the current
														// tab to the job files
														// tab
														switchMenuFocus(
																menuElements,
																'jobfiles-tab',
																false);
													} else {
														if (result.message) {
															$j
																	.prompt(result.message);
														} else {
															var errors = '';
															$j
																	.each(
																			result.errorList,
																			function(
																					i,
																					n) {
																				if (errors != '') {
																					errors += ', ';
																				}
																				errors += result.errorList[i].defaultMessage;
																			});
															$j.prompt(errors);
														}
													}
													// enable the submit buttons
													$j(
															'#js_submit, #js_submit_p')
															.attr('disabled',
																	false);
													// close overlay
													loadScript.closeOverlay();
												}
											});
						}
					});
}

/**
 * this method toggles the visibility of contract review items within table
 * 
 * @param {Integer}
 *            rowcount count of rows in table
 */
function toggleCRItems(rowcount) {
	// contract review items are currently hidden so make them visible
	if ($j('#crchild' + rowcount).css('display') == 'none') {
		// remove image to show items and append hide items image
		$j('#critemsLink' + rowcount).empty().append(
				'<img src="img/icons/items_hide.png" width="16" height="16" alt="'
						+ i18n.t("core.jobs:job.hideContractReviewItems",
								"Hide Contract Review Items")
						+ '" title="'
						+ i18n.t("core.jobs:job.hideContractReviewItems",
								"Hide Contract Review Items")
						+ '" class="image_inline" />');
		// make the contract review items row visible and add slide down effect
		$j('#crchild' + rowcount).css({
			display : '',
			visibility : 'visible'
		}).find('div').slideDown(1000);
	}
	// contract review items are currently visible so hide them
	else {
		// remove image to hide items and append show items image
		$j('#critemsLink' + rowcount).empty().append(
				'<img src="img/icons/items.png" width="16" height="16" alt="'
						+ i18n.t("core.jobs:job.showContractReviewItems",
								"Show Contract Review Items")
						+ '" title="'
						+ i18n.t("core.jobs:job.showContractReviewItems",
								"Show Contract Review Items")
						+ '" class="image_inline" />');
		// add effect to slide up contract review items and hide row
		$j('#crchild' + rowcount + ' div').slideUp(1000, function() {
			$j('#crchild' + rowcount).css({
				display : 'none',
				visibility : ''
			});
		});
	}
}

function invoiceNotRequired(jobId) {
	// no invoice req'd prompt
	$j.prompt(i18n.t("core.jobs:job.invoiceNotRequired",
			"Are you sure this job doesn't require invoicing?"), {
		submit : confirmNoInvoice,
		buttons : {
			Yes : true,
			No : false
		},
		focus : 1
	});

	// confirmation
	function confirmNoInvoice(v, m) {
		// user confirmed action?
		if (m) {
			loadScript.loadScriptsDynamic(new Array(
					'dwr/interface/jobservice.js'), true, {
				callback : function() {
					// call dwr service
					jobservice.noInvoiceRequired(jobId, {
						callback : function(resWrap) {
							if (resWrap.success == false) {
								alert(resWrap.message);
							} else {
								var job = resWrap.results;
								$j('span#jobStatusContainer').html(
										`${job.js.name} ( ${cwms.dateFormatFunction(job.dateComplete)})`
												);
							}
						}
					});
				}
			});
		}
	}
}

function toggleConfirmInvoiceInstructions(checkbox) {
	if ($j(checkbox).is(':checked')) {
		$j('span#invoicingLinks').removeClass('hid');
	} else {
		$j('span#invoicingLinks').addClass('hid');
	}
}

function addExpenseItem(jobId) {
	var modelId = $j('#newExpenseModel').val();
	var date = $j('#newExpenseItemDate').val();
	var minDate = $j('#newExpenseItemDate').attr('min');
	var serviceTypeId = $j('#newExpenseServiceType').val();
	var comment = $j('#newExpenseComment').val();
	var clientRef = $j('#newExpenseClientRef').val();
	var price = $j('#newExpensePrice').val();
	var quantity = $j('#newExpenseQuantity').val();
	var invoiceable = $j('#newExpenseInvoiceable').is(':checked');
	var table = $j('#jobServices').html();
	var waitMessage = i18n.t("core.jobs:expenses.wait", "Please wait");
	if(new Date(date) >= new Date(minDate)) {
		$j('#jobServices').html(waitMessage);
		$j.ajax({
			url : "addexpenseitem.json",
			data : {
				jobId : jobId,
				modelId : modelId,
				date : date,
				serviceTypeId : serviceTypeId,
				comment : comment,
				clientRef : clientRef,
				price : price,
				quantity : quantity,
				invoiceable: invoiceable
			},
			async : true
		}).done(function(expenseTable) {
			$j('#jobServices').html(expenseTable);
			$j('span#jobStatusContainer').html();

			fetch(cwms.urlWithParamsGenerator('job/jobstatus.json',{jobId}))
				.then(res => res.json())
				.then(status => document.querySelector('span#jobStatusContainer').innerHTML = status )

			changeServiceModelSearch();
		}).fail(function(error) {
			$j.prompt(error.message);
			$j('#jobServices').html(table);
		});
	} else
		$j.prompt("Please, choose a valid date after 1900");
}

function deleteExpenseItem(id, jobId) {
	var table = $j('#expenses-tab').html();
	var waitMessage = i18n.t("core.jobs:expenses.wait", "Please wait");
	$j('#jobServices').html(waitMessage);
	$j.ajax({
		url : "deleteexpenseitem.json",
		data : {
			id : id,
			jobId : jobId
		},
		async : true
	})
	.done(function(expenseTable) {
		$j('#jobServices').html(expenseTable);
		changeServiceModelSearch();
	})
	.fail(function(error) {
		var notDeletedMessage = i18n
				.t("core.jobs:expenses.cannotdeleted", "This job expense is already on a job costing or invoice. Hence, it cannot be deleted!");
		$j.prompt(notDeletedMessage);
		$j('#jobServices').html(table);
	});
}

function editExpenseItem(me, jobId) {
	// make all input fields in the row editable
	me.closest('.expenseItemRow').find('input').attr('readonly', false);
	me.closest('.expenseItemRow').find('.expenseItemTypeInput').addClass('hid');
	me.closest('.expenseItemRow').find('.expenseItemTypeSelect').removeClass(
			'hid');
	me.closest('.expenseItemRow').find('.expenseItemPriceFmt').addClass('hid');
	me.closest('.expenseItemRow').find('.expenseItemPrice').removeClass('hid');
	me.closest('.expenseItemRow').find('.expenseItemInvoiceable').attr('disabled', false);
	// change edit image to save image
	me.attr('src', 'img/icons/greentick.png');
	me.attr('onclick', 'saveExpenseItem($j(this), ' + jobId + ');');
}

function saveExpenseItem(me, jobId) {
	var table = $j('#jobServices').html();
	var myRow = me.closest('.expenseItemRow');
	var id = myRow.find('.expenseItemId').val();
	var date = myRow.find('.expenseItemDate').val();
	var minDate = myRow.find('.expenseItemDate').attr('min');
	if(new Date(date) >= new Date(minDate)) {
		var serviceTypeId = myRow.find('.expenseItemTypeSelect').val();
		var comment = myRow.find('.expenseItemComment').val();
		var clientRef = myRow.find('.expenseItemClientRef').val();
		var price = myRow.find('.expenseItemPrice').val();
		var quantity = myRow.find('.expenseItemQuantity').val();
		var invoiceable = myRow.find('.expenseItemInvoiceable').is(':checked');
		var waitMessage = i18n.t("core.jobs:expenses.wait", "Please wait");
		var quoteSelect = $j('#jobServiceQuotations');
		$j('#jobServices').html(waitMessage);
		$j.ajax({
			url : "editexpenseitem.json",
			data : {
				id : id,
				jobId : jobId,
				date : date,
				serviceTypeId : serviceTypeId,
				comment : comment,
				clientRef : clientRef,
				price : price,
				quantity : quantity,
				invoiceable: invoiceable
			},
			async : true
		}).done(function(expenseTable) {
			$j('#jobServices').html(expenseTable);
			changeServiceModelSearch();
		})
		.fail(function(error) {
			$j.prompt(error.message);
			$j('#jobServices').html(table);
			// make all input fields in the row editable
			me.closest('.expenseItemRow').find('input').attr('readonly', true);
			me.closest('.expenseItemRow').find('.expenseItemTypeInput').removeClass('hid');
			me.closest('.expenseItemRow').find('.expenseItemTypeSelect').addClass('hid');
			me.closest('.expenseItemRow').find('.expenseItemPriceFmt').removeClass('hid');
			me.closest('.expenseItemRow').find('.expenseItemPrice').addClass('hid');
			// change save image to edit image
			me.attr('src', 'img/icons/note_edit.png');
			me.attr('onclick', 'editExpenseItem($j(this), ' + jobId + ');');
		});
	} else
		alert("Please, choose a valid date after 1900");
}

// open image upload pop up
function openImageUploadPopUp(event, jobitemId, imageType) {

	addImageToEntity(jobitemId, imageType);

	event.stopImmediatePropagation();

	// intercept closing image upload pop up
	$j(document).on('click', '.close', function() {
		location.reload();
	});

}

// manage the selectable services on the job services tab
function changeServiceModelSearch() {
	var quoteId = $j('#serviceQuotationSelect').val();
	$j.ajax({
		url: "findSelectableServicesForNewJobServices.json",
		data: {
			quoteId: quoteId,
			jobId: jobId
		},
		async: true
	}).done(function(models) {
		$j('#newExpenseModel').empty();
		jobServiceModelsWithPrice = [];
		models.forEach(function(model) {
			jobServiceModelsWithPrice.push(model);
			$j('#newExpenseModel').append('<option value="' + model.id + '">' + model.name + (model.reference ? ' (' + model.reference + ')' : '') + '</option>');
		});
		setExpenseDefaultPrice();
	});
}

function setExpenseDefaultPrice() {
	var modelId = $j('#newExpenseModel').val();
	jobServiceModelsWithPrice.forEach(function(model) {
		if(model.id == modelId && model.finalPrice)
			$j('#newExpensePrice').val(model.finalPrice);
	});
}

function updateJobCourier(button)
{
	if(button.html() == 'Update')
		{
			$j('#jobCourierName').attr('disabled', false);
			$j('#jobCourierTrackingNumber').attr('disabled', false);
			button.html('Enregistrer');
		}
	else
		{
		$j.ajax({
			url : "updateJobCourier.json",
			method: "POST",
			data : {
				jobid: $j('#jobid').val(),
				jobCourierId: $j('#jobCourierId').val(),
				courierId: $j('#jobCourierName').val(),
				jobCourierTrackingNumber: $j('#jobCourierTrackingNumber').val(),
			},
			async : true
		}).done(function(content) {
			$j('#jobCourierName').attr('disabled', true);
			$j('#jobCourierTrackingNumber').attr('disabled', true);
			button.html('Update');
		});
		
		}
}

function initUpdateDeliveryDetails(){
	const form = document.getElementById("jobDeliveryForm");
	document
	const inputs = document.querySelectorAll('#jobDeliveryForm input')
	inputs.forEach((n) =>
				n.addEventListener("change",(e)=>{
					const formData = new FormData(form);
					const numberOfPackages = formData.get("numberOfPackages");
					if(!form.reportValidity()) {
						return; 
					}
					const csrfToken = document.querySelector("meta[name='_csrf']").getAttribute("content")
					fetch("updateJobDelivery.json",
				    {
						method: 'post',
						body: formData ,
					    headers: {
					     "X-CSRF-TOKEN": csrfToken}
					});
				}));
}

function setMessageVisibility(selectedStId, defaultStId, instrumentPlantid)
{
	if(selectedStId == defaultStId)
		$j('#frommessage_'+instrumentPlantid).removeClass('hid').addClass('vis');
	else
		$j('#frommessage_'+instrumentPlantid).removeClass('vis').addClass('hid');
}

function displayContractInstructions(contractId) {
	if($j('#contractInstruction_'+contractId).hasClass('hid')) {
		$j('#contractInstruction_'+contractId).removeClass('hid');
		$j('#contractInstruction_show_'+contractId).removeClass('show').addClass('hid');
		$j('#contractInstruction_hid_'+contractId).removeClass('hid').addClass('show');
	}
	else {
		$j('#contractInstruction_'+contractId).addClass('hid');
		$j('#contractInstruction_hid_'+contractId).removeClass('show').addClass('hid');
		$j('#contractInstruction_show_'+contractId).removeClass('hid').addClass('show');
	}
		
}