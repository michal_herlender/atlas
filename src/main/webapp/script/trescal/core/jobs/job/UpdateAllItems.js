/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	UpdateAllItems.js
*	DESCRIPTION		:
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	04/07/2011 - SH - Created File
*
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * this method copies the values selected in the range elements to any others
 * present on the page in the direction specified.
 *
 * @param {String} direction the direction in which the values should be copied
 * @param {String} input1 the first range input element to be copied
 * @param {String} input2 the second range input element to be copied
 * @param {String} select the unit of measurement select element to be copied
 */
function copyRangeSelection(direction, input1, input2, select)
{
	// add marker after element
	$j(select).after('<span class="marker"></span>');				
	// variable to indicate the marker has been found
	var markerFound = false;
	// arrays to hold all selects
	var selectArray = new Array();
	// copy information down the list
	if (direction == 'down')
	{								
		// create an array of select objects
		selectArray = $j.makeArray($j('select[name="' + $j(select).attr('name') + '"]'));					
	}
	else
	{				
		// create an array of select objects
		selectArray = $j.makeArray($j('select[name="' + $j(select).attr('name') + '"]'));
		// reverse the array
		selectArray.reverse();				
	}
	
	// loop through all matching selects
	$j.each(selectArray, function()
	{
		// marker found?
		if ($j(this).next().hasClass('marker'))
		{
			// set variable and remove marker
			markerFound = true;
			$j(this).next().remove();
		}
		// marker found?
		if (markerFound)
		{
			// copy values of input1, input2 and select to all
			// other occurences in form in the specified direction
			$j(this).prev().prev().val($j(input1).val());
			$j(this).prev().val($j(input2).val());
			$j(this).val($j(select).val());					
		}					
	});
}
