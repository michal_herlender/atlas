/**
 * Enables or disables input entries for a specific job item's inputs;
 * we only want to enter / select not invoiced reason when specifically indicating to skip
 * @param boxChecked
 * @param rowId
 */
function enableDisableNotInvoiced(boxChecked, jobItemId) {
	if (boxChecked) {
		$j('input#comments'+jobItemId).prop('disabled',false);
		$j('select#reason'+jobItemId).prop('disabled',false);
		$j('select#periodicInvoiceId'+jobItemId).prop('disabled',false);
	}
	else {
		$j('input#comments'+jobItemId).prop('disabled',true);
		$j('select#reason'+jobItemId).prop('disabled',true);
		$j('select#periodicInvoiceId'+jobItemId).prop('disabled',true);
	}
}