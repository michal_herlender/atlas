

export async function sendJobItemFeedbackEmail(emailContext) {
	const csrfToken = document.querySelector("meta[name='_csrf']").getAttribute("content")
	const r = await fetch("job/generateClientFeedbackRequest.json", {
        method: "POST",
        credentials: "same-origin",
        headers: {
            		"Content-Type": "application/json",
            		"X-CSRF-TOKEN": csrfToken },
        body: JSON.stringify(emailContext)
    });
    const json = await (r.ok ? r.json() : Promise.reject(`Error on sending request. Server return with ${r.status}.`));
    return json.right ? json.value : Promise.reject(json.value);
  }

export class SendJobItemFeedbackEmailContext{
    constructor(items,toIds,toEmails,ccIds,ccEmails,send,jobId,includeSelfInCarbonCopy,subject,body){
            this.items = items;
            this.toIds = toIds;
            this.toEmails = toEmails;
            this.ccIds = ccIds;
            this.ccEmails = ccEmails;
            this.send = send;
            this.jobId = jobId;
            this.includeSelfInCarbonCopy = includeSelfInCarbonCopy;
            this.subject = subject;
            this.body = body;
    }  

    static createInstanceWithItems(items,toIds,toEmails,ccIds,ccEmails,send,jobId,includeSelfInCarbonCopy){
        return new SendJobItemFeedbackEmailContext(items,toIds,toEmails,ccIds,ccEmails,send,jobId,includeSelfInCarbonCopy,null,null);
    }

    static createInstanceWithRecipientsAndContent(recipients,body,subject,jobId){
        const toRecipients = recipients.filter(r => r.type == "EMAIL_TO");
        const ccRecipients = recipients.filter(r => r.type == "EMAIL_CC");
        const toIds = toRecipients.filter(r => r.recipientContactId).map(r => r.recipientContactId);
        const ccIds = ccRecipients.filter(r => r.recipientContactId).map(r => r.recipientContactId);
        const toEmails = toRecipients.filter(r => !r.recipientContactId).map(r => r.emailAddress);
        const ccEmails = ccRecipients.filter(r => !r.recipientContactId).map(r => r.emailAddress);
        return new SendJobItemFeedbackEmailContext([],toIds,toEmails,ccIds,ccEmails,true,jobId,false,subject,body);
    } 
}