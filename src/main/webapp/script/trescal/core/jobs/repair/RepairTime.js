
var pageImports = new Array (	'script/thirdparty/jQuery/jquery.boxy.js'
								);

function init() {

	// load stylesheet using the dom if not already
	loadScript.loadStyle('styles/jQuery/jquery-boxy.css', 'screen');
	// load stylesheet using the dom
	loadScript.loadStyle('styles/jQuery/jquery.datetimepicker.css', 'screen');
	// javascript file for creating wysiwyg editor
	
	if(actionPerformed != undefined && actionPerformed)
		$j('#successMessage').fadeIn().delay(3000).fadeOut();
}

function preparEditRepairTime(ele, rtId)
{
	if(rtId > 0) {
	$j("#startDate_"+rtId).removeClass("hid").addClass("show");
	$j("#timeSpentHour_"+rtId).removeClass("hid").addClass("show");
	$j("#timeSpentMinute_"+rtId).removeClass("hid").addClass("show");
	$j("#technicianComment_"+rtId).removeClass("hid").addClass("show");
	$j("#validationController").removeClass("hid").addClass("show");
	$j("#startDate-"+rtId).removeClass("show").addClass("hid");
	$j("#timeSpentH_"+rtId).removeClass("show").addClass("hid");
	$j("#timeSpentM_"+rtId).removeClass("show").addClass("hid");
	$j("#technicianComment-"+rtId).removeClass("show").addClass("hid");
	}
	else
		{
		var tr = $j(ele).parent().parent();
		tr.find('input[id^=startDate_]').removeClass("hid").addClass("show");
		tr.find('input[id^=timeSpentHour_]').removeClass("hid").addClass("show");
		tr.find('input[id^=timeSpentMinute_]').removeClass("hid").addClass("show");
		tr.find('textarea[id^=technicianComment_]').removeClass("hid").addClass("show");
		$j('li[id^=validationController]').removeClass("hid").addClass("show");
		tr.find('span[id^=startDate-]').removeClass("show").addClass("hid");
		tr.find('span[id^=timeSpentH_]').removeClass("show").addClass("hid");
		tr.find('span[id^=timeSpentM_]').removeClass("show").addClass("hid");
		tr.find('span[id^=technicianComment-]').removeClass("show").addClass("hid");
		}

}

function cancelRepairTime()
{
	$j("input[id^=startDate_]").removeClass("show").addClass("hid");
	$j("input[id^=timeSpentHour_]").removeClass("show").addClass("hid");
	$j("input[id^=timeSpentMinute_]").removeClass("show").addClass("hid");
	$j("textarea[id^=technicianComment_]").removeClass("show").addClass("hid");
	$j("li[id^=validationController").removeClass("show").addClass("hid");
	$j("span[id^=startDate-]").removeClass("hid").addClass("show");
	$j("span[id^=timeSpentH_]").removeClass("hid").addClass("show");
	$j("span[id^=timeSpentM_]").removeClass("hid").addClass("show");
	$j("span[id^=technicianComment-]").removeClass("hid").addClass("show");
}

function addRepairTime(){
	$j("#action").val('ADD');
	$j("#repairTimeForm").submit();
}

function editRepairTime(){
	$j("#action").val('EDIT');
	$j("#repairTimeForm").submit();
}

function deleteRepairTime(rtId){
	$j("#action").val('DELETE');
	$j("#itemToDeleteId").val(rtId);
	$j("#repairTimeForm").submit();
}

