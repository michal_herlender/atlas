var menuElements = new Array({
	anchor : 'repairinspectionreport-link',
	block : 'repairinspectionreport-tab'
}, {
	anchor : 'repaircompletionreport-link',
	block : 'repaircompletionreport-tab'
});

/**
 * called in completionreport.js
 */
function initRepairReports() {

	// select the correct subnavigation link
	$j('#linkrepairs').addClass('selected');

}

function enableRcrTab()
{
	$j("#rcrTab").removeClass('wrapper');
	$j("#repaircompletionreport-link").removeClass('pointer-events-none');
}