var operationIndex, componentIndex;

function init() {
	
	// only the last init function is called, so we have to call other init in
	// other pages manually
	initRepairReports();
	initRir();
	
	initRcrOperationTableForDragAndDrop();

	updateRcrOperationIndex();

	updateRcrComponentIndex();

	disableDragAndDropForOperationsAddedInRiR();

	$j("#rcrForm #componentName").autocomplete({
		source : function(req, resp) {
			$j.ajax({
				url : 'searchComponent.json?keyword=' + getRcrComponentName(),
				async : false,
				dataType : 'json',
				success : function(data) {
					console.log(data);
					var res = data.map(function(d) {
						return {
							id : d.id,
							label : d.displayName,
							value : d.displayName
						};
					});
					console.log(res);
					resp(res);
				}
			});
		},
		select : function(event, ui) {
			console.log(">>", ui.item.label);
			console.log(">>", ui.item.value);
			// return false;
		}
	});
	
	updateRcrOperationsTotals();
	
	updateRcrComponentsTotals();
	
	/*JIRepairs*/
	// user preference to use job item key navigation
	if (userPrefs.jikeynav)
	{
		// initialise key nav to move between job items
		initialiseKeyNavForJI();
	}
	
	$j('#repairCompletionDate').focus(function(){
		$j('#lastTpCertDateDiv').toggleClass('hidden');
	});
	
	$j('#repairCompletionDate').blur(function(){
		$j('#lastTpCertDateDiv').toggleClass('hidden')
	});

	if(actionRcrPerformed != undefined && actionRcrPerformed)
		$j('#successMessageRcr').fadeIn().delay(3000).fadeOut();
}

/* --------------Operations--------------- */
function disableDragAndDropForOperationsAddedInRiR() {
	$j("#rcrForm #operationsTable tr[addedafterrirvalidation]").each(function(index) {
		
		// if has drag and drop icon
		var cnt = $j(this).find("img[class=dragNdropIcon]").length;
		
		if (cnt == 0) {
			$j(this).addClass('nodrag');
			$j(this).addClass('nodrop');
			$j(this).unbind("mousedown");
		}
	});
}

function initRcrOperationTableForDragAndDrop() {
	// Initialize the operation table
	$j("#rcrForm #operationsTable").tableDnD({
		onDragClass : "myDragClass",
		onDrop : function(table, rows) {
			updateRcrOperationsPositions();
		}
	});
}

function updateRcrOperationIndex() {
	operationIndex = $j("#rcrForm #operationsTable tbody tr[class!=emptyRow]").length;
}

function showRcrAdditionalFieldsForOperationType(ischecked, type) {
	if (ischecked && type === 'internal') {
		$j('.internalFields').css('display', 'inline');
		$j('.externalFields').css('display', 'none');
	} else if (ischecked && type === 'external') {
		$j('.externalFields').css('display', 'inline');
		$j('.internalFields').css('display', 'none');
	}
}

function addRcrOperation() {

	var isInternal = $j("#rcrForm input[name='optype']:checked").val() === 'INTERNAL';
	var operation = $j("#rcrForm #operationName").val();

	// operation text is blank
	if (!operation.trim()) {
		$j("#rcrForm #operationName").effect("highlight", {
			color : 'pink'
		}, 2500);
		$j("#rcrForm #operationName").val('');
		return;
	}
	var rowData;
	if (isInternal) {
		var minutes = $j("#rcrForm #internalTimeMinutes").val() == '' ? 0:$j("#rcrForm #internalTimeMinutes").val();
		var hours = $j("#rcrForm #internalTimeHours").val() == '' ? 0:$j("#rcrForm #internalTimeHours").val();
		// if minutes text is empty or minutes have negative value
		if ((!minutes.trim() && !hours.trim()) || (minutes < 1 && hours < 1) || minutes > 59) {
			$j("#rcrForm #internalTimeMinutes").effect("highlight", {
				color : 'pink'
			}, 2500);
			$j("#rcrForm #internalTimeMinutes").val('0');
		}
		if ((!minutes.trim() && !hours.trim()) || (hours < 1 && minutes < 1) || hours > 99) {
			$j("#rcrForm #internalTimeHours").effect("highlight", {
				color : 'pink'
			}, 2500);
			$j("#rcrForm #internalTimeHours").val('0');
			return;
		}
		// all good, add internal operation
		rowData = {
			name : operation,
			type : i18n.t("repairoperation.type.internal", "Internal"),
			timeMinutes : minutes,
			timeHours : hours,
			opType : 'INTERNAL',
			statusText : '<select name="rcrOperationsForm['+operationIndex+'].status">'+rcrOperationStatusOptions+'</select>'
		};
	} else {
		// external is checked
		var companyName = $j("#rcrForm .compSearchJQPlugin input[name=comptext]").val();
		var companyId = $j("#rcrForm .compSearchJQPlugin input[name=coid]").val();
		if (!companyName.trim() || !companyId.trim()) {
			$j("#rcrForm .compSearchJQPlugin input[name=comptext]").effect("highlight",
					{
						color : 'pink'
					}, 2500);
			$j("#rcrForm .compSearchJQPlugin input[name=comptext]").val('');
			return;
		}
		var delay = $j("#rcrForm #externalTime").val();
		// if delay text is empty or delay have negative value
		if (!delay.trim() || delay < 1) {
			$j("#rcrForm #externalTime").effect("highlight", {
				color : 'pink'
			}, 2500);
			$j("#rcrForm #externalTime").val('');
			return;
		}
		// with calibration
		var withCal = $j('#rcrForm #externalWithCal').is(':checked');
		var typeText = withCal ? i18n.t("repairoperation.type.externalwithcal",
				"External with calibration") : i18n.t(
				"repairoperation.type.external", "External");
		if(!isInternal){
			typeText += ' ('+companyName+')';
		}
		var type = withCal ? 'EXTERNAL_WITH_CALIBRATION' : 'EXTERNAL';
		rowData = {
			name : operation,
			type : typeText,
			tpCompany : companyName,
			tpCompanyId : companyId,
			time : delay,
			withCal : withCal,
			opType : type,
			statusText : '<select name="rcrOperationsForm['+operationIndex+'].status">'+rcrOperationStatusOptions+'</select>'
		};
	}

	var rowClass = operationIndex % 2 == 0 ? 'even' : 'odd';

	if (rowData != '') {

		// for external operation the format "H M" 
		// in the time/delay column must be replaced with D(days)
		var timeDelayH = "H";
		var timeDelayM = "M";
		if(rowData.opType === "EXTERNAL" || rowData.opType === "EXTERNAL_WITH_CALIBRATION"){
			timeDelayH = "D(days)";
			timeDelayM = "";
		}
		
		var html = '<tr addedafterrirvalidation="true" class="'
				+ rowClass
				+ '">'
				+ '<td class="center">'
				+ '<img class="dragNdropIcon" src="img/icons/scroller.png" />'
				+ '<span>'
				+ (operationIndex + 1)
				+ '</span><input class="frOperationId" type="hidden" id="rcrOperationsForm'
				+ operationIndex
				+ '.frOperationId" name="rcrOperationsForm['
				+ operationIndex
				+ '].frOperationId"/><input class="position" type="hidden" id="rcrOperationsForm'
				+ operationIndex
				+ '.position" name="rcrOperationsForm['
				+ operationIndex
				+ '].position"  value="'
				+ (operationIndex + 1)
				+ '"/>'
				+ '<input type="hidden" id="rcrOperationsForm'
				+ operationIndex
				+ '.addedAfterRiRValidation" name="rcrOperationsForm['
				+ operationIndex
				+ '].addedAfterRiRValidation"  value="true"/>'
				+'</td>'
				+ '<td class="operationname">'
				+ '<textarea class="autoresizing" id="rcrOperationsForm'
				+ operationIndex
				+ '.name" name="rcrOperationsForm['
				+ operationIndex
				+ '].name" style="width:100%">'
				+ rowData.name
				+ '</textarea></td>'
				+ '<td class="operationtype">'
				+ rowData.type
				+ '<input type="hidden" id="rcrOperationsForm'
				+ operationIndex
				+ '.type" name="rcrOperationsForm['
				+ operationIndex
				+ '].type"  value="'
				+ rowData.opType
				+ '"/>'
				+ '<input type="hidden" id="rcrOperationsForm'
				+ operationIndex
				+ '.coid" name="rcrOperationsForm['
				+ operationIndex
				+ '].coid"  value="'
				+ (rowData.tpCompanyId?rowData.tpCompanyId:'')
				+ '"/></td>'
				+ '<td class="operationtime" isinternal="'
				+ isInternal
				+ '" >'
				+ '<input type="text" onchange="updateRcrOperationsTotals();" id="rcrOperationsForm'
				+ operationIndex
				+ 'labourTimeHours" name="rcrOperationsForm['
				+ operationIndex
				+ '].labourTimeHours"  value="'
				+ rowData.timeHours
				+ '" style="width: 25px;"/>'
				+ timeDelayH +'&nbsp;<input type="text" onchange="updateRcrOperationsTotals();" id="rcrOperationsForm'
				+ operationIndex
				+ 'labourTimeMinutes" name="rcrOperationsForm['
				+ operationIndex
				+ '].labourTimeMinutes"  value="'
				+ rowData.timeMinutes
				+ '" style="width: 25px;"/>'
				+ timeDelayM +'</td>'
				+ '<td class="operationcost" isinternal="'
				+ isInternal
				+ '" >'
				+ '<input type="text" id="rcrOperationsForm'
				+ operationIndex
				+ '.cost" name="rcrOperationsForm['
				+ operationIndex
				+ '].cost" onchange="updateRcrOperationsTotals(); return false;"/></td>'
				+ '<td></td><td>'
				+ rowData.statusText
				+ '</td>'
				+ '<td class="center">'
				+ '<img src="img/icons/delete.png" height="16" width="16" alt="" title="" class="pointer delimg" onclick="deleteOperation(this)" id="'
				+ rowData.name + '" data-type="' + rowData.opType
				+ '" data-timeHours="' + rowData.timeHours
				+ '" data-timeMinutes="' + rowData.timeMinutes
				+ '" data-cost="0" />' + '</td>' + '</tr>';

		// remove empty row
		$j('#rcrForm #operationsTable .emptyRow').remove();
		// add new row
		$j("#rcrForm #operationsTable tbody").append(html);
		// empty fields
		emptyaddRcrOperationsField();
		// update count
		updateRcrOperationIndex();
		// update totals
		updateRcrOperationsTotals();
		// for drag and drop
		initRcrOperationTableForDragAndDrop();
		// active autoresizing for operation name input
		autoresizing();
	}
}

function updateRcrOperationItemIndex()
{
	var i = 0;
	$j("#rcrForm #operationsTable tbody tr").each(function() {
		var regexp = new RegExp("\\d+", "g");
		$j(this).find("input").each(function() {
			var id = $j(this).attr("id");
			var name = $j(this).attr("name");
			if(id != undefined)
				$j(this).attr("id", id.replace(regexp,i));
			if(name != undefined)
				$j(this).attr("name", name.replace(regexp,i));
			
			if($j(this).attr("class") == "position")
				$j(this).val(i+1);
		});
		
		$j(this).find("select").each(function() {
			var id = $j(this).attr("id");
			var name = $j(this).attr("name");
			if(id != undefined)
				$j(this).attr("id", id.replace(regexp,i));
			if(name != undefined)
				$j(this).attr("name", name.replace(regexp,i));
			});
		
		// update the TR index
		$j(this).find("td:first span").html(i+1);
		
		i++;
	});
}

function emptyaddRcrOperationsField() {
	$j("#rcrForm #operationName").val('');
	$j("#rcrForm #internalTimeHours").val('0');
	$j("#rcrForm #internalTimeMinutes").val('0');
	$j("#rcrForm .compSearchJQPlugin input[name=comptext]").val('');
	$j("#rcrForm #externalTime").val('');
}

function updateRcrOperationsTotals() {
	/*------ time / delay -------*/

	// total internal
	var totalInternalMinutes = 0;
	var totalInternalHours = 0;
	$j("#rcrForm .operationtime input[id$='labourTimeHours']").each(function() {
		var isInternal = $j(this).parent().is('[isinternal=true]');
		var val = $j(this).val();
		if (!isNaN(val) && val.trim() && isInternal) {
			totalInternalHours += parseFloat(val);
		}
	});
	$j("#rcrForm .operationtime input[id$='labourTimeMinutes']").each(function() {
		var isInternal = $j(this).parent().is('[isinternal=true]');
		var val = $j(this).val();
		if (!isNaN(val) && val.trim() && isInternal) {
			totalInternalMinutes += parseFloat(val);
		}
	});
	totalInternalHours += parseInt(totalInternalMinutes / 60);
	totalInternalMinutes = totalInternalMinutes % 60;
	var totalInternal = totalInternalMinutes + (totalInternalHours * 60);
	if (totalInternal != 0) {
		var minutesText = ' ' + i18n.t('mins', 'mins');
		var hoursText = ' ' + i18n.t('hours', 'hours');
		var totalInternalText = totalInternalHours > 0 ? totalInternalHours + hoursText + ' ' + totalInternalMinutes + minutesText:totalInternalMinutes + minutesText;
		$j("#rcrForm #opTIntTime").text(totalInternalText);
		$j("#rcrForm #opTIntTimeInMinutes").text(totalInternal);
	} else {
		$j("#rcrForm #opTIntTime").text('');
	}

	// total external
	var totalExternal = 0;
	$j('#rcrForm .operationtime input').each(function() {
		var isExternal = $j(this).parent().is('[isinternal=false]');
		var val = $j(this).val();
		if (!isNaN(val) && val.trim() && isExternal) {
			totalExternal += parseFloat(val);
		}
	});
	if (totalExternal != 0) {
		var daysText = ' ' + i18n.t('days', 'days');
		$j("#rcrForm #opTExtTime").text(totalExternal + daysText);
	} else {
		$j("#rcrForm #opTExtTime").text('');
	}

	// total time + delay
	var totalTime = '';
	if (totalExternal != 0)
		totalTime += $j("#rcrForm #opTExtTime").text() + ' ';
	if (totalInternal != 0)
		totalTime += $j("#rcrForm #opTIntTime").text();
	$j("#rcrForm #opTTime").text(totalTime);

	/*------ cost -------*/
	// cost internal
	var costInternal = 0;
	$j('#rcrForm .operationcost input').each(function() {
		var isInternal = $j(this).parent().is('[isinternal=true]');
		var val = $j(this).val();
		if (!isNaN(val) && val.trim() && isInternal) {
			costInternal += parseFloat(val);
		}
	});
	if (costInternal != 0) {
		$j("#rcrForm #opTIntCost").text(costInternal + ' ' + currencySymbol);
	} else {
		$j("#rcrForm #opTIntCost").text('');
	}

	// cost external
	var costExternal = 0;
	$j('#rcrForm .operationcost input').each(function() {
		var isInternal = $j(this).parent().is('[isinternal=true]');
		var val = $j(this).val();
		if (!isNaN(val) && val.trim() && !isInternal) {
			costExternal += parseFloat(val);
		}
	});
	if (costExternal != 0) {
		$j("#rcrForm #opTExtCost").text(costExternal + ' ' + currencySymbol);
	} else {
		$j("#rcrForm #opTExtCost").text('');
	}

	// total cost
	var costTotal = costInternal + costExternal;
	if (costTotal != 0)
		$j("#rcrForm #opTCost").text(costTotal + ' ' + currencySymbol);
	else
		$j("#rcrForm #opTCost").text('');

	// update page totals
	updateRcrPageTotals();

}

function deleteOperation(element) {
	var operationName = $j(element).closest("tr").find("td[class=operationname] input:first").val();
	// delete row
	$j(element).closest("tr").remove()
	// update totals
	updateRcrOperationsTotals();
	// update count
	updateRcrOperationIndex();
	// if count is 0, add the empty row
	if (operationIndex == 0) {
		var html = '<tr class="emptyRow"><td colspan="8" ></td></tr>';
		$j("#operationsTable tbody").append(html);
	}
	// delete linked components :
	$j("#rcrForm #componentsTable tr").filter(function(){
		var val = $j(this).find('td[class=componentOperation] select option:selected').text();
		if( val == operationName)
			return true;
		else
			return false;
	}).each(function(){
		var imgBtn = $j(this).find("td img");
		deleteRcrComponent(imgBtn);
	});
	
	updateRcrOperationItemIndex();
}

function updateRcrOperationsPositions() {
	$j("#rcrForm #operationsTable tbody tr td:first-child .position").each(
			function(operationIndex) {
				$j(this).val(operationIndex + 1);
			});
	$j("#rcrForm #operationsTable tbody tr td:first-child span").each(
			function(operationIndex) {
				$j(this).text(operationIndex + 1);
			});
}

/* ------------ Component ------------ */

function getRcrComponentName() {
	return $j("#rcrForm #componentName").val();
}

function addRcrComponent() {

	var component = $j("#rcrForm #componentName").val();

	// no operation yet
	if (operationIndex == 0) {
		$j('html, body').animate({
			scrollTop : $j("#rcrForm #operationName").offset().top - 100
		}, 300);
		$j("#rcrForm #operationName").effect("highlight", {
			color : 'pink'
		}, 2500);
		return;
	}

	// operation text is blank
	if (!component.trim()) {
		$j("#rcrForm #componentName").effect("highlight", {
			color : 'pink'
		}, 2500);
		$j("#rcrForm #componentName").val('');
		return;
	}

	var quantity = $j("#rcrForm #componentQuantity").val();
	// if minutes text is empty or minutes have negative value
	if (!quantity.trim() || quantity < 1) {
		$j("#rcrForm #componentQuantity").effect("highlight", {
			color : 'pink'
		}, 2500);
		$j("#rcrForm #componentQuantity").val('');
		return;
	}

	var cost = $j("#rcrForm #componentCost").val();
	// if minutes text is empty or minutes have negative value
	if (!cost.trim() || cost < 0) {
		$j("#rcrForm #componentCost").effect("highlight", {
			color : 'pink'
		}, 2500);
		$j("#rcrForm #componentCost").val('');
		return;
	}


	var rowData = {
		name : component,
		sourceText : i18n.t("repairreport.component.source.occasional", "Occasional"),
		source : 'OCCASIONAL',
		quantity : quantity,
		cost : cost,
		stateText : '<select name="rcrComponentsForm['+componentIndex+'].status">'+rcrComponentStatusOptions+'</select>'
	};

	var rowClass = componentIndex % 2 == 0 ? 'even' : 'odd';
	var operationsSelectHtml = getRcrOperationsSelectHtml();
	var firstOperationName = getFirstOperationName();

	if (rowData != '') {
		var html = '<tr class="'+rowClass+'">' + '<td class="center"><span>'
				+ (componentIndex + 1)
				+ '</span>'
				+ '<input type="hidden" id="rcrComponentsForm'
				+ componentIndex
				+ '.addedAfterRiRValidation" name="rcrComponentsForm['
				+ componentIndex
				+ '].addedAfterRiRValidation"  value="true"/>'
				+'</td>'
				+ '<td class="componentname">'
				+ '<textarea class="autoresizing" id="rcrComponentsForm'
				+ componentIndex
				+ '.name" name="rcrComponentsForm['
				+ componentIndex
				+ '].name" style="width:100%; text-align:left">'
				+ rowData.name
				+ '</textarea></td>'
				+ '<td class="componentOperation">'
				+ '<select onchange="componentOperationChange(this);" id="rcrComponentsForm'
				+ componentIndex
				+ 'freeRepairOperationId" name="rcrComponentsForm['
				+ componentIndex
				+ '].freeRepairOperationId">'+operationsSelectHtml+'</select>'
				+ '<input class="freeRepairOperationName" type="hidden" id="rcrComponentsForm'
				+ componentIndex
				+ 'freeRepairOperationName" name="rcrComponentsForm['
				+ componentIndex
				+ '].freeRepairOperationName"  value="'
				+ firstOperationName
				+ '"/></td>'
				+ '<td class="componentsource">'
				+ '<input type="hidden" id="rcrComponentsForm'
				+ componentIndex
				+ 'source" name="rcrComponentsForm['
				+ componentIndex
				+ '].source"  value="'
				+ rowData.source
				+ '"/>'
				+ rowData.sourceText
				+ '</td>'
				+ '<td class="componentquantity">'
				+ '<input type="text" id="rcrComponentsForm'
				+ componentIndex
				+ 'quantity" name="rcrComponentsForm['
				+ componentIndex
				+ '].quantity"  value="'
				+ rowData.quantity
				+ '" onchange="updateRcrComponentsTotals()"/></td>'
				+ '<td class="componentcost">'
				+ '<input type="text" id="rcrComponentsForm'
				+ componentIndex
				+ 'cost" name="rcrComponentsForm['
				+ componentIndex
				+ '].cost" value="'
				+ rowData.cost
				+ '" onchange="updateRcrComponentsTotals()"/></td>'
				+ '<td></td><td>'+rowData.stateText
				+ '<input type="hidden" id="rcrComponentsForm'
				+ componentIndex
				+ 'status" name="rcrComponentsForm['
				+ componentIndex
				+ '].status" value="'
				+ rowData.state
				+ '" />'
				+'</td>'
				+ '<td class="center">'
				+ '<img src="img/icons/delete.png" height="16" width="16" alt="" title="" class="delimg" onclick="deleteRcrComponent(this)" id="delete_'
				+ rowData.name + '" data-quantity="' + rowData.quantity
				+ '" data-cost="' + rowData.cost + '" />' + '</td>' + '</tr>';

		// remove empty row
		$j('#rcrForm #componentsTable .emptyRow').remove();
		// add new row
		$j("#rcrForm #componentsTable tbody").append(html);
		// empty fields
		emptyaddRcrComponentFields();
		// update count
		updateRcrComponentIndex();
		// update totals
		updateRcrComponentsTotals();
		// active autoresizing for component name input
		autoresizing();
	}
}

function updateRcrComponentItemIndex()
{
	var i = 0;
	$j("#rcrForm #componentsTable tbody tr").each(function() {
		var regexp = new RegExp("\\d+", "g");
		$j(this).find("input").each(function() {
			var id = $j(this).attr("id");
			var name = $j(this).attr("name");
			if(id != undefined)
				$j(this).attr("id", id.replace(regexp,i));
			if(name != undefined)
				$j(this).attr("name", name.replace(regexp,i));
			
		});
		
		$j(this).find("select").each(function() {
			var id = $j(this).attr("id");
			var name = $j(this).attr("name");
			if(id != undefined)
				$j(this).attr("id", id.replace(regexp,i));
			if(name != undefined)
				$j(this).attr("name", name.replace(regexp,i));
			});
		
		// update the TR index
		$j(this).find("td:first span").html(i+1);
		
		i++;
	});
}

function updateRcrComponentsTotals(){
	
	// quantity
	var quantity = 0;
	$j('#rcrForm .componentquantity input').each(function(){
		var val = $j(this).val();
		if (!isNaN(val) && val.trim())
			quantity += parseInt($j(this).val());
	});
	if(quantity != 0)
		$j("#rcrForm #compTQuantity").text(quantity);
	else
		$j("#rcrForm #compTQuantity").text('');
	
	// cost
	var cost = 0;
	$j('#rcrForm .componentcost input').each(function(){
		var val = $j(this).val();
		if (!isNaN(val) && val.trim())
			cost += parseFloat($j(this).val());
	});
	if(cost != 0)
		$j("#rcrForm #compTCost").text(cost + ' ' + currencySymbol);
	else
		$j("#rcrForm #compTCost").text('');
	
	// update page totals
	updateRcrPageTotals();
	
}	

function getRcrOperationsSelectHtml(){
	var html =''; 
	$j("#rcrForm #operationsTable tbody tr[class!=emptyRow]").each(function(){
		html += '<option value="'+$j(this).find('.frOperationId').val()+'" >'+$j(this).find('.operationname textarea:first').val()+'</option>';
	 });
	return html;
}

function getFirstOperationName(){
	return $j("#rcrForm #operationsTable tbody tr[class!=emptyRow]").first().find(".operationname").text().trim();
}

function emptyaddRcrComponentFields() {
	$j("#rcrForm #componentName").val('');
	$j("#rcrForm #componentQuantity").val('');
	$j("#rcrForm #componentCost").val('');
}

function updateRcrComponentIndex() {
	componentIndex = $j("#rcrForm #componentsTable tbody tr[class!=emptyRow]").length;
}

function deleteRcrComponent(element) {
	// delete row
	$j(element).closest("tr").remove()
	// update totals
	updateRcrComponentsTotals();
	// update count
	updateRcrComponentItemIndex();
	updateRcrComponentIndex();
	// if count is 0, add the empty row
	if (componentIndex == 0) {
		var html = '<tr class="emptyRow"><td colspan="9" ></td></tr>';
		$j("#rcrForm #componentsTable tbody").append(html);
	}
}

function componentOperationChange(select){
	var textVal = $j(select).find("option:selected").text().trim();
	$j(select).next(".freeRepairOperationName").val(textVal);
}

// --------------------------------------

function validateRcr(){
	var yes = i18n.t("yes", "Yes");
	var no = i18n.t("no", "No");
	
	var rcrTotalTime = parseInt($j("#rcrForm #opTIntTimeInMinutes").text());
	var rirTotalTime = parseInt($j("#rirForm #opTIntTimeInMinutes").text());
	
	var HHs = parseInt(rcrTotalTime / 60);
	var MMs = rcrTotalTime % 60;
	var Hs = parseInt(rirTotalTime / 60);
	var Ms = rirTotalTime % 60;
	
	var promptStates = {
		state0: {
			html:rcrTotalTime > rirTotalTime ? '<span style="color:red;"><b>'+i18n.t("core.jobs:jobItem.repairTimeWarrning",{H:Hs,M:Ms,HH:HHs,MM:MMs})+'</b></span><br/>'+strings['repaircompletionreport.confirmation']:'<b>'+i18n.t("core.jobs:jobItem.repairTimeWarrning",{H:Hs,M:Ms,HH:HHs,MM:MMs})+'</b><br/>'+strings['repaircompletionreport.validate'],
			buttons: { [no]: false, [yes]: true },
			submit:function(e,v,m,f){
				console.log(f);
				if(v){
					e.preventDefault();
					$j.prompt.goToState('state1');
	            }
			}
		},
		state1: {
			html:'<span>'+strings['jiactions.timespent']+': </span><input id="timespent" type="number" name="timespent" value="5">',
			buttons: { [yes]: true },
			//focus: ":input:first",
			submit:function(e,v,m,f){
				console.log(f);
				if(v){
					e.preventDefault();
					let timeSpentValue = $j("#timespent").val();
					console.log(timeSpentValue);
					var timeSpentInput = $j("<input>").attr("name","timeSpent").attr("type","hidden").val(timeSpentValue);
					$j('#rcrForm').append($j(timeSpentInput));
					var input = $j("<input>").attr("name","saveAndValidateRcr").attr("type","hidden").val("");
					$j('#rcrForm').append($j(input));
					$j("#rcrForm").submit();
	            }
			}
		}
	};
	
	$j.prompt(promptStates);

	return false;
}

function updateRcrPageTotals(){
	
	var costTotal = 0;
	
	// operations cost
	if(!isNaN(parseFloat($j('#rcrForm #opTCost').text())))
		costTotal += parseFloat($j('#rcrForm #opTCost').text());
	// components cost
	if(!isNaN(parseFloat($j('#rcrForm #compTCost').text())))
		costTotal += parseFloat($j('#rcrForm #compTCost').text());
	
	if(costTotal != 0)
		$j("#rcrForm #totalCost").text(costTotal + ' ' + currencySymbol);
	else
		$j("#rcrForm #totalCost").text('');
	
}

function showMisuseCommentRcr(isMisuse)
{
	if(isMisuse)
		$j("#misuseCommentTextRcr").removeClass("hid").addClass("show");
	else
		$j("#misuseCommentTextRcr").removeClass("show").addClass("hid");
		
}