var rirOperationIndex = 0, rirComponentIndex = 0;

//var jqCalendar = new Array ( { 
//	 inputField: 'estimatedReturnDate',
//	 dateFormat: 'dd.mm.yy',
//	 displayDates: 'all'
//});

function initRir() {

	initRirOperationTableForDragAndDrop();

	updateRirOperationIndex();

	updateRirComponentIndex();

	$j("#rirForm #componentName").autocomplete({
		source : function(req, resp) {
			$j.ajax({
				url : 'searchComponent.json?keyword=' + getRirComponentName(),
				async : false,
				dataType : 'json',
				success : function(data) {
					console.log(data);
					var res = data.map(function(d) {
						return {
							id : d.id,
							label : d.displayName,
							value : d.displayName
						};
					});
					console.log(res);
					resp(res);
				}
			});
		},
		select : function(event, ui) {
			console.log(">>", ui.item.label);
			console.log(">>", ui.item.value);
			$j("#rirForm #compid").val(ui.item.id);
			//return false;
		}
	});
	
	// update totals
	updateRirOperationsTotals();
	updateRirComponentsTotals();
	
	if(actionRirPerformed != undefined && actionRirPerformed)
		$j('#successMessageRir').fadeIn().delay(3000).fadeOut();
	
	// active autoresizing for operation and component name input
	autoresizing();
}

function initRirOperationTableForDragAndDrop() {
	// Initialize the operation table
	$j("#rirForm #operationsTable").tableDnD({
		onDragClass : "myDragClass",
		onDrop : function(table, rows) {
			updateRirOperationsPositions();
		}
	});
}

function updateRirOperationIndex() {
	rirOperationIndex = $j("#rirForm #operationsTable tbody tr[class!=emptyRow]").length;
}

function showAdditionalFieldsForRirOperationType(ischecked, type) {
	if (ischecked && type === 'internal') {
		$j('#rirForm .internalFields').css('display', 'inline');
		$j('#rirForm .externalFields').css('display', 'none');
	} else if (ischecked && type === 'external') {
		$j('#rirForm .externalFields').css('display', 'inline');
		$j('#rirForm .internalFields').css('display', 'none');
	}
}

function addRirOperation() {

	var isInternal = $j("#rirForm input[name='optype']:checked").val() === 'INTERNAL';
	var operation = $j("#rirForm #operationName").val();
	updateRirOperationIndex();

	// operation text is blank
	if (!operation.trim()) {
		$j("#rirForm #operationName").effect("highlight", {
			color : 'pink'
		}, 2500);
		$j("#rirForm #operationName").val('');
		return;
	}
	var rowData;
	if (isInternal) {
		var minutes = $j("#rirForm #internalTimeMinutes").val() == '' ? 0:$j("#rirForm #internalTimeMinutes").val();
		var hours = $j("#rirForm #internalTimeHours").val() == '' ? 0:$j("#rirForm #internalTimeHours").val();
		// if minutes text is empty or minutes have negative value
		if ((!minutes.trim() && !hours.trim()) || (minutes < 1 && hours < 1) || minutes > 59) {
			$j("#rirForm #internalTimeMinutes").effect("highlight", {
				color : 'pink'
			}, 2500);
			$j("#rirForm #internalTimeMinutes").val('0');
		}
		if ((!minutes.trim() && !hours.trim()) || (hours < 1 && minutes < 1) || hours > 99) {
			$j("#rirForm #internalTimeHours").effect("highlight", {
				color : 'pink'
			}, 2500);
			$j("#rirForm #internalTimeHours").val('0');
			return;
		}
		// all good, add internal operation
		rowData = {
			name : operation,
			type : i18n.t("repairoperation.type.internal", "Internal"),
			timeMinutes : minutes,
			timeHours : hours,
			opType : 'INTERNAL',
			statusText : '<select id="operationsForm'+rirOperationIndex+'.status" name="operationsForm['+rirOperationIndex+'].status">'+rirOperationStatusOptions+'</select>'
		};
	} else {
		// external is checked
		var companyName = $j("#rirForm .compSearchJQPlugin input[name=comptext]").val();
		var companyId = $j("#rirForm .compSearchJQPlugin input[name=coid]").val();
		if (!companyName.trim() || !companyId.trim()) {
			$j("#rirForm .compSearchJQPlugin input[name=comptext]").effect("highlight",
					{
						color : 'pink'
					}, 2500);
			$j("#rirForm .compSearchJQPlugin input[name=comptext]").val('');
			return;
		}
		var delay = $j("#rirForm #externalTime").val();
		// if delay text is empty or delay have negative value
		if (!delay.trim() || delay < 1) {
			$j("#rirForm #externalTime").effect("highlight", {
				color : 'pink'
			}, 2500);
			$j("#rirForm #externalTime").val('');
			return;
		}
		// with calibration
		var withCal = $j('#rirForm #externalWithCal').is(':checked');
		var typeText = withCal ? i18n.t("repairoperation.type.externalwithcal",
				"External with calibration") : i18n.t(
				"repairoperation.type.external", "External");
		var type = withCal ? 'EXTERNAL_WITH_CALIBRATION' : 'EXTERNAL';
		rowData = {
			name : operation,
			type : typeText,
			tpCompany : companyName,
			tpCoid : companyId,
			timeHours : delay,
			timeMinutes : 0,
			withCal : withCal,
			opType : type,
			statusText : '<select id="operationsForm'+rirOperationIndex+'.status" name="operationsForm['+rirOperationIndex+'].status">'+rirOperationStatusOptions+'</select>'
		};
	}

	var rowClass = rirOperationIndex % 2 == 0 ? 'even' : 'odd';

	// TODO: matnsach attribute addedafterrirvalidation f tr
	if (rowData != '') {
		
		// for external operation the format "H M" 
		// in the time/delay column must be replaced with D(days)
		
		var timeDelayH = "H";
		var timeDelayM = "M";
		if(rowData.opType === "EXTERNAL" || rowData.opType === "EXTERNAL_WITH_CALIBRATION"){
			timeDelayH = "D(days)";
			timeDelayM = "";
		}
		
		var html = '<tr class="'
				+ rowClass
				+ '">'
				+ '<td class="center">'
				+ '<img class="dragNdropIcon" src="img/icons/scroller.png" />'
				+ '<span>'
				+ (rirOperationIndex + 1)
				+ '</span>'
				+ '<input type="hidden" id="operationsForm'
				+ rirOperationIndex
				+ '.addedAfterRiRValidation" name="operationsForm['
				+ rirOperationIndex
				+ '].addedAfterRiRValidation"  value="false"/>'
				+ '<input type="hidden" id="operationsForm'
				+ rirOperationIndex
				+ '.coid" name="operationsForm['
				+ rirOperationIndex
				+ '].coid"  value="' + (rowData.tpCoid != undefined ? rowData.tpCoid:'') + '"/>'
				+ '<input type="hidden" id="operationsForm'
				+ rirOperationIndex
				+ '.id" name="operationsForm['
				+ rirOperationIndex
				+ '].id"/><input class="order" type="hidden" id="operationsForm'
				+ rirOperationIndex
				+ '.position" name="operationsForm['
				+ rirOperationIndex
				+ '].position"  value="'
				+ (rirOperationIndex + 1)
				+ '"/></td>'
				+ '<td class="operationname" opId="0">'
				
				+ '<textarea  class="autoresizing" id="operationsForm'
				+ rirOperationIndex
				+ '.name" name="operationsForm['
				+ rirOperationIndex
				+ '].name" style="width:100%">'
				+ rowData.name
				+ '</textarea></td>'
				+ '<td class="operationtype">'
				+ rowData.type + (rowData.opType != "INTERNAL" ? ' ('+rowData.tpCompany+')':"")
				+ '<input type="hidden" id="operationsForm'
				+ rirOperationIndex
				+ '.type" name="operationsForm['
				+ rirOperationIndex
				+ '].type"  value="'
				+ rowData.opType
				+ '"/></td>'
				+ '<td class="operationtime" isinternal="'
				+ isInternal
				+ '" >'
				+ '<input type="text" onchange="updateRirOperationsTotals();" id="operationsForm'
				+ rirOperationIndex
				+ 'labourTimeHours" name="operationsForm['
				+ rirOperationIndex
				+ '].labourTimeHours"  value="'
				+ rowData.timeHours
				+ '" style="width: 25px;"/>'
				+ timeDelayH +'&nbsp;'
				+ '<input type="text" onchange="updateRirOperationsTotals();" id="operationsForm'
				+ rirOperationIndex
				+ 'labourTimeMinutes" name="operationsForm['
				+ rirOperationIndex
				+ '].labourTimeMinutes"  value="'
				+ rowData.timeMinutes
				+ '" style="width: 25px;"/>'
				+ timeDelayM +'</td>'
				+ '<td class="operationcost" isinternal="'
				+ isInternal
				+ '" >'
				+ '<input type="text" id="operationsForm'
				+ rirOperationIndex
				+ '.cost" name="operationsForm['
				+ rirOperationIndex
				+ '].cost" onchange="updateRirOperationsTotals(); return false;"/></td>'
				+ '<td>'
				+ rowData.statusText
				+ '</td>'
				+ '<td class="center">'
				+ '<img src="img/icons/delete.png" height="16" width="16" alt="" title="" class="pointer delimg" onclick="deleteRirOperation(this)" id="'
				+ rowData.name + '" data-type="' + rowData.opType
				+ '" data-timeHours="' + rowData.timeHours
				+ '" data-timeMinutes="' + rowData.timeMinutes
				+ '" data-cost="0"/>' + '</td>' + '</tr>';

		// remove empty row
		$j('#rirForm #operationsTable .emptyRow').remove();
		// add new row
		$j("#rirForm #operationsTable tbody").append(html);
		// Add the new operation to component operation select
		$j('#rirForm select[id^=opSelect]').append("<option value=\"0\">"+rowData.name+"</option>");
		// empty fields
		emptyAddRirOperationsField();
		// update count
		updateRirOperationIndex();
		// update totals
		updateRirOperationsTotals();
		// for drag and drop
		initRirOperationTableForDragAndDrop();
		// active autoresizing for operation name input
		autoresizing();
	}
}

function updateRirOperationItemIndex()
{
	var i = 0;
	$j("#rirForm #operationsTable tbody tr").each(function() {
		$j(this).find("input").each(function() {
			var id = $j(this).attr("id");
			var name = $j(this).attr("name");
			if(id != undefined)
				$j(this).attr("id", id.replace(/[0-9]/g,i));
			if(name != undefined)
				$j(this).attr("name", name.replace(/[0-9]/g,i));
			
			if($j(this).attr("class") == "position")
				$j(this).val(i+1);
		});
		
		$j(this).find("select").each(function() {
			var id = $j(this).attr("id");
			var name = $j(this).attr("name");
			if(id != undefined)
				$j(this).attr("id", id.replace(/[0-9]/g,i));
			if(name != undefined)
				$j(this).attr("name", name.replace(/[0-9]/g,i));
			});
		
		i++;
	});
}

function emptyAddRirOperationsField() {
	$j("#rirForm #operationName").val('');
	$j("#rirForm #internalTimeHours").val('0');
	$j("#rirForm #internalTimeMinutes").val('0');
	$j("#rirForm .compSearchJQPlugin input[name=comptext]").val('');
	$j("#rirForm #externalTime").val('');
}

function updateRirOperationsTotals() {
	/*------ time / delay -------*/

	// total internal
	var totalInternalMinutes = 0;
	var totalInternalHours = 0;
	$j("#rirForm .operationtime input[id$='labourTimeHours']").each(function() {
		var isInternal = $j(this).parent().is('[isinternal=true]');
		var val = $j(this).val();
		if (!isNaN(val) && val.trim() && isInternal) {
			totalInternalHours += parseFloat(val);
		}
	});
	$j("#rirForm .operationtime input[id$='labourTimeMinutes']").each(function() {
		var isInternal = $j(this).parent().is('[isinternal=true]');
		var val = $j(this).val();
		if (!isNaN(val) && val.trim() && isInternal) {
			totalInternalMinutes += parseFloat(val);
		}
	});
	totalInternalHours += parseInt(totalInternalMinutes / 60);
	totalInternalMinutes = totalInternalMinutes % 60;
	var totalInternal = totalInternalMinutes + (totalInternalHours * 60);
	if (totalInternal != 0) {
		var minutesText = ' ' + i18n.t('mins', 'mins');
		var hoursText = ' ' + i18n.t('hours', 'hours');
		var totalInternalText = totalInternalHours > 0 ? totalInternalHours + hoursText + ' ' + totalInternalMinutes + minutesText:totalInternalMinutes + minutesText;
		$j("#rirForm #opTIntTime").text(totalInternalText);
		$j("#rirForm #opTIntTimeInMinutes").text(totalInternal);
	} else {
		$j("#rirForm #opTIntTime").text('');
	}

	// total external
	var totalExternal = 0;
	$j('#rirForm .operationtime input').each(function() {
		var isExternal = $j(this).parent().is('[isinternal=false]');
		var val = $j(this).val();
		if (!isNaN(val) && val.trim() && isExternal) {
			totalExternal += parseFloat(val);
		}
	});
	if (totalExternal != 0) {
		var daysText = ' ' + i18n.t('days', 'days');
		$j("#rirForm #opTExtTime").text(totalExternal + daysText);
	} else {
		$j("#rirForm #opTExtTime").text('');
	}

	// total time + delay
	var totalTime = '';
	if (totalExternal != 0)
		totalTime += $j("#rirForm #opTExtTime").text() + ' ';
	if (totalInternal != 0)
		totalTime += $j("#rirForm #opTIntTime").text();
	$j("#rirForm #opTTime").text(totalTime);

	/*------ cost -------*/
	// cost internal
	var costInternal = 0;
	$j('#rirForm .operationcost input').each(function() {
		var isInternal = $j(this).parent().is('[isinternal=true]');
		var val = $j(this).val();
		if (!isNaN(val) && val.trim() && isInternal) {
			costInternal += parseFloat(val);
		}
	});
	
	$j("#rirForm #opTIntCost").text(costInternal + ' ' + currencySymbol);

	// cost external
	var costExternal = 0;
	$j('#rirForm .operationcost input').each(function() {
		var isInternal = $j(this).parent().is('[isinternal=true]');
		var val = $j(this).val();
		if (!isNaN(val) && val.trim() && !isInternal) {
			costExternal += parseFloat(val);
		}
	});

	$j("#rirForm #opTExtCost").text(costExternal + ' ' + currencySymbol);

	// total cost
	var costTotal = costInternal + costExternal;
	$j("#rirForm #opTCost").text(costTotal + ' ' + currencySymbol);

	// update page totals
	updateRirPageTotals();

}

function deleteRirOperation(element) {
	var operationName = $j(element).closest("tr").find("td[class=operationname] input:first").val();
	// delete row
	$j(element).closest("tr").remove()
	// update totals
	updateRirOperationsTotals();
	// update count
	updateRirOperationIndex();
	// if count is 0, add the empty row
	if (rirOperationIndex == 0) {
		var html = '<tr class="emptyRow"><td colspan="8" ></td></tr>';
		$j("#rirForm #operationsTable tbody").append(html);
	}
	//delete linked components :
	$j("#rirForm #componentsTable tr").filter(function(){
		var val = $j(this).find("td[class=componentOperation] select option:selected").text();
		if( val.trim() == operationName.trim())
			return true;
		else
			return false;
	}).each(function(){
		var imgBtn = $j(this).find("td img");
		deleteRirComponent(imgBtn);
	});
	//delete operation from components select
	$j("#rirForm #componentsTable tr td select[id^=opSelect] option:contains('"+operationName+"')").remove();
	updateRirOperationItemIndex();
	updateRirOperationsPositions();
}

function updateRirOperationsPositions() {
	$j("#rirForm #operationsTable tbody tr td:first-child .order").each(
			function(rirOperationIndex) {
				$j(this).val(rirOperationIndex + 1);
			});
	$j("#rirForm #operationsTable tbody tr td:first-child span").each(
			function(rirOperationIndex) {
				$j(this).text(rirOperationIndex + 1);
			});
}

/* ------------ Component ------------ */

function getRirComponentName() {
	return $j("#rirForm #componentName").val();
}

function addRirComponent() {

	var component = $j("#rirForm #componentName").val();
	var componentId = $j("#rirForm #compid").val();
	updateRirOperationIndex();

	// no operation yet
	if (rirOperationIndex == 0) {
		$j('html, body').animate({
			scrollTop : $j("#rirForm #operationName").offset().top - 100
		}, 300);
		$j("#rirForm #operationName").effect("highlight", {
			color : 'pink'
		}, 2500);
		return;
	}

	// operation text is blank
	if (!component.trim()) {
		$j("#rirForm #componentName").effect("highlight", {
			color : 'pink'
		}, 2500);
		$j("#rirForm #componentName").val('');
		return;
	}

	var quantity = $j("#rirForm #componentQuantity").val();
	// if minutes text is empty or minutes have negative value
	if (!quantity.trim() || quantity < 1) {
		$j("#rirForm #componentQuantity").effect("highlight", {
			color : 'pink'
		}, 2500);
		$j("#rirForm #componentQuantity").val('');
		return;
	}

	var cost = $j("#rirForm #componentCost").val();

	var rowData = {
		id : componentId,
		name : component,
		source : i18n.t("repairreport.component.source.occasional", "Occasional"),
		sourceValue : 'OCCASIONAL',
		quantity : quantity,
		cost : cost,
		state : '<select id="componentsForm'+rirComponentIndex+'.status" name="componentsForm['+rirComponentIndex+'].status">'+rirComponentStatusOptions+'</select>'
	};

	var rowClass = rirComponentIndex % 2 == 0 ? 'even' : 'odd';
	var operationsSelectHtml = getRirOperationsSelectHtml();
	var defaultComponentStatus = 'Booked';

	if (rowData != '') {
		var html = '<tr  class="'+rowClass+'">' + '<td class="center"><span>'
				+ (rirComponentIndex + 1)
				+ '</span>'
				+ '</td>'
				+ '<td class="componentname">'
				+ '<input type="hidden" id="componentsForm'
				+ rirComponentIndex
				+ '.addedAfterRiRValidation" name="componentsForm['
				+ rirComponentIndex
				+ '].addedAfterRiRValidation"  value="false"/>'
				+ '<input type="hidden" id="componentsForm'
				+ rirComponentIndex
				+ '.componentId" name="componentsForm['
				+ rirComponentIndex
				+ '].componentId"  value="'+rowData.id+'"/>'
				+ '<textarea class="autoresizing" id="componentsForm'
				+ rirComponentIndex
				+ '.name" name="componentsForm['
				+ rirComponentIndex
				+ '].name"'
				+ ' style="width:100%; text-align:left" cols="90" rows="1">'
				+ rowData.name
				+ '</textarea></td>'
				+ '<td class="componentOperation">'
				+ '<select id="opSelect'+rirComponentIndex+'" onchange="componentSelectChange('+rirComponentIndex+', $j(this))">'+operationsSelectHtml+'</select>'
				+ '<input type="hidden" id="currentSelectedIdValue'+rirComponentIndex+'"'
				+ 'name="componentsForm['
				+ rirComponentIndex
				+ '].freeRepairOperationId"'
				+ 'value="" />'
				+ '<input type="hidden" id="currentSelectedNameValue'+rirComponentIndex+'"'
				+ 'name="componentsForm['
				+ rirComponentIndex
				+ '].freeRepairOperationName"'
				+ 'value="" />'
				+ '</td>'
				+ '<td class="componentsource">'
				+ '<input type="hidden" id="componentsForm'
				+ rirComponentIndex
				+ 'source" name="componentsForm['
				+ rirComponentIndex
				+ '].source" value="'
				+ rowData.sourceValue
				+ '"/>'
				+ rowData.source
				+ '</td>'
				+ '<td class="componentquantity">'
				+ '<input type="text" id="componentsForm'
				+ rirComponentIndex
				+ 'quantity" name="componentsForm['
				+ rirComponentIndex
				+ '].quantity"  value="'
				+ rowData.quantity
				+ '" onchange="updateRirComponentsTotals()"/></td>'
				+ '<td class="componentcost">'
				+ '<input type="text" id="componentsForm'
				+ rirComponentIndex
				+ 'cost" name="componentsForm['
				+ rirComponentIndex
				+ '].cost" value="'
				+ rowData.cost
				+ '" onchange="updateRirComponentsTotals()"/></td>'
				+ '<td>'
				+ rowData.state
				+'</td>'
				+ '<td class="center">'
				+ '<img src="img/icons/delete.png" height="16" width="16" alt="" title="" class="delimg" onclick="deleteRirComponent(this)" id="delete_'
				+ rowData.name + '" data-quantity="' + rowData.quantity
				+ '" data-cost="' + rowData.cost + '" />' + '</td>' + '</tr>';

		// remove empty row
		$j('#rirForm #componentsTable .emptyRow').remove();
		// add new row
		$j("#rirForm #componentsTable tbody").append(html);
		//update default select operation
		updateRirDefaultSelectOperation(rirComponentIndex);
		// empty fields
		emptyAddRirComponentFields();
		// update count
		updateRirComponentIndex();
		// update totals
		updateRirComponentsTotals();
		// active autoresizing for component name input
		autoresizing();
		
	}
}

function updateRirComponentItemIndex()
{
	var i = 0;
	$j("#rirForm #componentsTable tbody tr").each(function() {
		$j(this).find("input").each(function() {
			var id = $j(this).attr("id");
			var name = $j(this).attr("name");
			if(id != undefined)
				$j(this).attr("id", id.replace(/[0-9]/g,i));
			if(name != undefined)
				$j(this).attr("name", name.replace(/[0-9]/g,i));
			
		});
		
		$j(this).find("select").each(function() {
			var id = $j(this).attr("id");
			var name = $j(this).attr("name");
			if(id != undefined)
				$j(this).attr("id", id.replace(/[0-9]/g,i));
			if(name != undefined)
				$j(this).attr("name", name.replace(/[0-9]/g,i));
			});
		
		i++;
	});
}

function componentSelectChange(rirCompIndex, ele)
{
	$j('#rirForm #currentSelectedIdValue' + rirCompIndex).val($j(ele).val()); 
	$j('#rirForm #currentSelectedNameValue' + rirCompIndex).val($j(ele).find('option:selected').text());
}

function updateRirDefaultSelectOperation(rirCompIndex)
{
	$j("#currentSelectedIdValue"+rirCompIndex).val($j('#rirForm #opSelect'+rirCompIndex).find('option:first').val() != undefined ? $j('#rirForm #opSelect'+rirCompIndex).find('option:first').val():0);
	$j("#currentSelectedNameValue"+rirCompIndex).val($j('#rirForm #opSelect'+rirCompIndex).find('option:first').text());
}

function updateRirComponentsTotals(){
	
	// quantity
	var quantity = 0;
	$j('#rirForm .componentquantity input').each(function(){
		var val = $j(this).val();
		if (!isNaN(val) && val.trim())
			quantity += parseInt($j(this).val());
	});
	if(quantity != 0)
		$j("#rirForm #compTQuantity").text(quantity);
	else
		$j("#rirForm #compTQuantity").text('');
	
	// cost
	var cost = 0;
	$j('#rirForm .componentcost input').each(function(){
		var val = $j(this).val();
		if (!isNaN(val) && val.trim())
			cost += parseFloat($j(this).val());
	});
	if(cost != 0)
		$j("#rirForm #compTCost").text(cost + ' ' + currencySymbol);
	else
		$j("#rirForm #compTCost").text('');
	
	// update page totals
	updateRirPageTotals();
	
}	

function getRirOperationsSelectHtml(){
	var html =''; 
	$j("#rirForm #operationsTable tbody tr[class!=emptyRow]").each(function(){
				html += '<option value="'+$j(this).find('.operationname').attr('opId')+'">' + $j(this).find('.operationname textarea:first').val()+'</option>'
	 });
	return html;
}

function emptyAddRirComponentFields() {
	$j("#rirForm #componentName").val('');
	$j("#rirForm #componentQuantity").val('');
	$j("#rirForm #componentCost").val('');
}

function updateRirComponentIndex() {
	rirComponentIndex = $j("#rirForm #componentsTable tbody tr[class!=emptyRow]").length;
}

function deleteRirComponent(element) {
	// delete row
	$j(element).closest("tr").remove()
	// update totals
	updateRirComponentsTotals();
	// update count
	updateRirComponentItemIndex();
	updateRirComponentIndex();
	// if count is 0, add the empty row
	if (rirComponentIndex == 0) {
		var html = '<tr class="emptyRow"><td colspan="9" ></td></tr>';
		$j("#rirForm #componentsTable tbody").append(html);
	}
}

//--------------------------------------

function updateRirPageTotals(){
	
	var costTotal = 0;
	
	// operations cost 
	if(!isNaN(parseFloat($j('#rirForm #opTCost').text())))
		costTotal += parseFloat($j('#rirForm #opTCost').text());
	// components cost
	if(!isNaN(parseFloat($j('#rirForm #compTCost').text())))
		costTotal += parseFloat($j('#rirForm #compTCost').text());
	
	$j("#rirForm #totalCost").text(costTotal + ' ' + currencySymbol);

}

function showMisuseComment(isMisuse)
{
	if(isMisuse)
		$j("#misuseCommentText").removeClass("hid").addClass("show");
	else
		$j("#misuseCommentText").removeClass("show").addClass("hid");
		
}

function saveRirFrom(){
	var isTrescalWarranty = $j("#trescalWarrantyCheckBox").is(":checked");
	var isMisuse = $j("#misuseCheckBox").is(":checked");
	
	if(isTrescalWarranty && isMisuse)
		{
			var yes = i18n.t("yes", "Yes");
			var no = i18n.t("no", "No");
		
			var promptStates = {
				state0: {
					html:strings['repairinpectionreport.savewarning'],
					buttons: { [no]: false, [yes]: true },
					submit:function(e,v,m,f){
						console.log(f);
						if(v){
							e.preventDefault();
							var input = $j("<input>").attr("name","saveRir").attr("type","hidden").val("");
							$j('#rirForm').append($j(input));
							$j("#rirForm").submit();
			            }
					}
				}
			};
			
			$j.prompt(promptStates);
		}
	else
		{
			var input = $j("<input>").attr("name","saveRir").attr("type","hidden").val("");
			$j('#rirForm').append($j(input));
			$j("#rirForm").submit();
		}

	return false;
}

function validateRir(){
	var yes = i18n.t("yes", "Yes");
	var no = i18n.t("no", "No");
	
	var isTrescalWarranty = $j("#trescalWarrantyCheckBox").is(":checked");
	var isMisuse = $j("#misuseCheckBox").is(":checked");
	var isCompletedByTechnician = $j("#completedByTechnician").val() == 'true';

	if(isTrescalWarranty && isMisuse)
	var promptStates = {
			state0: {
				html:strings['repairinpectionreport.savewarning'],
				buttons: { [no]: false, [yes]: true },
				submit:function(e,v,m,f){
					console.log(f);
					if(v){
						e.preventDefault();
						$j.prompt.goToState('state1');
		            }
				}
			},
		state1: {
			html:isCompletedByTechnician ? strings['repairinpectionreport.validate']:strings['repairinpectionreport.complete'],
			buttons: { [no]: false, [yes]: true },
			submit:function(e,v,m,f){
				console.log(f);
				if(v){
					e.preventDefault();
					$j.prompt.goToState('state2');
	            }
			}
		},
		state2: {
			html:'<span>'+strings['jiactions.timespent']+': </span><input id="timespent" type="number" name="timespent" value="5">',
			buttons: { [yes]: true },
			//focus: ":input:first",
			submit:function(e,v,m,f){
				console.log(f);
				if(v){
					e.preventDefault();
					let timeSpentValue = $j("#timespent").val();
					console.log(timeSpentValue);
					var timeSpentInput = $j("<input>").attr("name","timeSpent").attr("type","hidden").val(timeSpentValue);
					$j('#rirForm').append($j(timeSpentInput));
					var input = $j("<input>").attr("name","saveAndValidateRir").attr("type","hidden").val("");
					$j('#rirForm').append($j(input));
					$j("#rirForm").submit();
	            }
			}
		}
	};
	else
		var promptStates = {
			state0: {
				html:isCompletedByTechnician ? strings['repairinpectionreport.validate']:strings['repairinpectionreport.complete'],
				buttons: { [no]: false, [yes]: true },
				submit:function(e,v,m,f){
					console.log(f);
					if(v){
						e.preventDefault();
						$j.prompt.goToState('state1');
		            }
				}
			},
			state1: {
				html:'<span>'+strings['jiactions.timespent']+': </span><input id="timespent" type="number" name="timespent" value="5">',
				buttons: { [yes]: true },
				//focus: ":input:first",
				submit:function(e,v,m,f){
					console.log(f);
					if(v){
						e.preventDefault();
						let timeSpentValue = $j("#timespent").val();
						console.log(timeSpentValue);
						var timeSpentInput = $j("<input>").attr("name","timeSpent").attr("type","hidden").val(timeSpentValue);
						$j('#rirForm').append($j(timeSpentInput));
						var input = $j("<input>").attr("name","saveAndValidateRir").attr("type","hidden").val("");
						$j('#rirForm').append($j(input));
						$j("#rirForm").submit();
		            }
				}
			}
		};
	
	$j.prompt(promptStates);

	return false;
}

function reviewRir(){
	var input = $j("<input>").attr("name","reviewAndSaveRir").attr("type","hidden").val("");
	$j('#rirForm').append($j(input));
	$j("#rirForm").submit();
}

function onChangeLeadTime(){
	var d = new Date();
	var leadTime = $j("#leadTime").val()
	if(!isNaN(leadTime)){
		d.setDate(new Date().getDate()+parseInt(leadTime));
		var dd = d.getDate();
		if(dd<10) {
		    dd='0'+dd;
		}
		var mm = d.getMonth()+1; 
		if(mm<10) {
		    mm='0'+mm;
		}
		var yyyy = d.getFullYear();
		var returnDate = dd+'.'+mm+'.'+yyyy;
		$j("#estimatedReturnDate").val(returnDate);
	}
}




