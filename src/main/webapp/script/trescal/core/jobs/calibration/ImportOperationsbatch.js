function companySelected(){
	var coid = $j('#coid').val();
	var subdivid = $j('#subdivid').val();
	getExchangeFormatForSelectedCompany(coid);
	getJobForSelectedCompany(subdivid);
}

function getExchangeFormatForSelectedCompany(coid) {
	if (coid != undefined) {
		$j.ajax({
			url : "getExchangeFormats.json",
			data : {
				coid : coid,
				type : 'OPERATIONS_IMPORTATION'
			},
			async : true
		}).done(
			function(data) {
				const combo = document.querySelector('#exchangeformat');
				combo.value = "";
				combo.items = data;
				combo.itemValuePath = 'id';
				combo.itemLabelPath = 'name';
				if(combo.items.length > 0)
					combo.value = combo.items[0].id;

		}).fail(function(result) {
			$j.prompt(result.responseText);
		});

	}
}

function getJobForSelectedCompany(subdivid){
	
	const csrfToken = document.querySelector("meta[name='_csrf']").getAttribute("content")
	const jobCombo = document.querySelector('#jobComboBox');
	jobCombo.value = "";
	fetch('activeJobsByClientJsonController.json?clientSubdiv='+subdivid,{
	        method : 'GET',
	        headers: {
	                "X-CSRF-TOKEN": csrfToken,
	                 }
	             })
	        .then(res => res.text().then(text =>{
	            jobCombo.items = JSON.parse(text);
	            jobCombo.itemValuePath = 'key';
	            jobCombo.itemLabelPath = 'value';
	            if(jobCombo.items.length >0)
	                jobCombo.value = jobCombo.items[0].key;
	        }));

	
}
customElements.whenDefined('iron-form').then(function() {
	document.querySelector('iron-form').addEventListener('iron-form-presubmit', function(e){
		document.querySelector('#loadingDialog').opened = true;
		var ajaxCall = new XMLHttpRequest();
		var form = document.getElementById('ironFormId').serializeForm();
		var formdata = transformParamsToFormData(form);
		// add file to formdata
		var fileElement = document.querySelector('#fileInput');
		formdata.append('file',fileElement.files[0])
		ajaxCall.open("POST","importoperationsbatch.json", true);   
		ajaxCall.setRequestHeader("X-CSRF-TOKEN", form._csrf);
		var currentValue = document.getElementById('rowsProgressBar').value;
		// progress listener
		ajaxCall.upload.onprogress = (e) => {
			while(currentValue <0.8){
				document.getElementById('rowsProgressBar').value = currentValue + 0.1;
				currentValue = document.getElementById('rowsProgressBar').value;
			}
		};
		ajaxCall.onreadystatechange = function(event) {
			if (ajaxCall.readyState === ajaxCall.DONE) {
				document.getElementById('rowsProgressBar').value =1;  
				submitListener(JSON.parse(ajaxCall.response).message);
			}
		};
		
		ajaxCall.send(formdata);
	});
});

function submitIronForm() {
	document.querySelector('iron-form').submit();
}
function transformParamsToFormData(data){
	var formdata = new FormData();
	for (var key in data) {
		if (data.hasOwnProperty(key)) {
			var value = data[key];
			if(Object.prototype.toString.call( value ) === '[object Array]'){
				for (var key2 in value) {
					formdata.append(key + '[' + key2 + ']', value[key2]);
				}
			}else{
				formdata.append(key, value);
			}
		}
	}
	return formdata;
}

function submitListener(message){
	// close loading dialog
	document.querySelector('#loadingDialog').opened = false;
	// get submit dialog
	let dialog = document.querySelector('#submitDialog');
	// get response data
	dialog.renderer =  function(root, dialog) {
      root.textContent = message;
      const okButton = window.document.createElement('vaadin-button');
      okButton.setAttribute('theme', 'primary small');
      okButton.textContent = 'OK';
      okButton.setAttribute('style', 'margin-right: 1em');
      okButton.addEventListener('click', function() {
        dialog.opened = false;
        document.location.reload(true);
      });
      root.appendChild(window.document.createElement('br'));
      root.appendChild(window.document.createElement('br'));
      root.appendChild(okButton);
    };
    dialog.opened = true;
}