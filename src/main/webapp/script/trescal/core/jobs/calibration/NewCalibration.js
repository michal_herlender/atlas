/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	NewCalibration.js
*	DESCRIPTION		:	Page-specific javascript file for newcalibration.vm.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 
*						
*	KNOWN ISSUES	:	-
*	HISTORY			:	26/11/2007 - JV - Created File
*					:	14/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'script/trescal/core/jobs/jobitem/JobItemFunctions.js',
								'script/trescal/core/tools/labelprinting/dymo/DymoLabelFunctions.js',
								'dwr/interface/defaultstandardservice.js',
								'dwr/interface/instrumservice.js',
								'dwr/interface/calibrationservice.js' );

function calProcess(upload, issue, sign, toolLink) {
    this.upload = upload;
    this.issue = issue;
    this.sign = sign;
    this.toolLink = toolLink;
}

/**
 * create array of descriptions to be used for password strength function
 */
var desc = new Array();
desc[0] = "Very Weak";
desc[1] = "Weak";
desc[2] = "Better";
desc[3] = "Medium";
desc[4] = "Strong";
desc[5] = "Strongest";

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if
 * present. this allows onload coding to be included in the page specific
 * javascript file without adding to the body onload attribute.
 */
function init()
{
	// i18n of descriptions for password strength function
	desc[0] = i18n.t("common:password.veryWeak", "Very Weak");
	desc[1] = i18n.t("common:password.weak", "Weak");
	desc[2] = i18n.t("common:password.better", "Better");
	desc[3] = i18n.t("common:password.medium", "Medium");
	desc[4] = i18n.t("common:password.strong", "Strong");
	desc[5] = i18n.t("common:password.strongest", "Strongest");
	
	// select the correct subnavigation link (calibration)
	$j('#linkcalibration').addClass('selected');
	
	// this checks a variable set in the VM to determine whether the any of the
	// following is necessary
	if(readyForCal)
	{
		// check if selected calibration process is CWMS
		checkIfCWMS($j('#processSelect').val());
		
		// Note, this could be done in the controller now (no dynamic change
		// possible to procedure) - if refactoring this code.
		if ($j('input[name="defaultProcId"]').val() != '') 
		{
			// this function checks if the current user is accredited to perform
			// the selected procedure and calibration type
			checkAccreditation($j('input[name="defaultProcId"]').val(), $j('#calTypeId').val(), currentPersonid);
		}
	}
}


/**
 * this function is fired each time a calibration process is chosen, it checks
 * if the chosen option is CWMS, and if so displays a new option to select a
 * CWMS procedure.
 * 
 * @param {Integer}
 *            calprocess id of the calibration process chosen in select box
 */
function checkIfCWMS(calprocess)
{
	// calibration process doesn't allow issue immediately
	if(calProcesses['p' + calprocess] != null && calProcesses['p' + calprocess].issue == false)
	{
		// show the CWMS section
		$j('li#cwmsstage').removeClass().addClass('vis');
		// show the WI standards section
		$j('tbody#wiStds').removeClass('hid').addClass('vis');
		// populate standards
		if($j('input[name="workInstructionId"]').val() != null)
		{
			getWiStds($j('input[name="workInstructionId"]').val());
		}
	}
	else
	{
		// hide the CWMS section
		$j('li#cwmsstage').removeClass().addClass('hid');
		// hide the WI standards section
		$j('tbody#wiStds').removeClass('vis').addClass('hid');
		// remove all standards from the section
		$j('tbody#wiStds tr:not(:first)').remove();
		// reset WI standards count
		$j('span#wiStdsSize').text(0);
		// calibration process has been chosen
		// reset the CWMS calibration procedure
		$j('#cwmsSelect').val(0);
	}
}

/**
 * this function checks the accreditation level held by the current user and
 * matches it against the selected procedure and calibration type. If the user
 * is accredited then they may continue.
 * 
 * @param {Integer}
 *            procedure id of procedure chosen in select box
 * @param {Integer}
 *            caltype id of caltype chosen in select box
 * @param {Integer}
 *            personid id of the current user
 */
function checkAccreditation(procedure, caltype, personid)
{
	// no procedure or caltype has been chosen
	if(procedure == 0 || caltype == 0)
	{
		// disable the continue button
		$j('input:submit').prop('disabled',true);
		// disable the multi start button
		$j('input#optionalMultiStartButton').prop('disabled',true);
	}
	else
	{
		// call dwr service to check the accreditation level of user for the
		// procedure and calibration type selected
		fetch(cwms.urlWithParamsGenerator("capabiltiyAuthorization/isAuthorizedFor.json",{capabilityId:procedure,calibrationTypeId:caltype,personId:personid}))
		.then(res => res.ok?res.json():Promise.reject(res.statusText))
		.then(status => {			
				// user is not accredited for this procedure
				if(!status.right)
				{
					// show the warning box and hide the success box
					$j('.warningBox1:last').show().next().hide();
					// disable the continue button
					$j('input:submit').prop('disabled', true);
					// disable the multi start button
					$j('input#optionalMultiStartButton').prop('disabled', true);
				}
				else
				{
					// show the success box and hide the warning box
					$j('.warningBox1:last').hide().next().show();
					// enable the continue button
					$j('input:submit').prop('disabled', false);					
					
					var levelText =  i18n.t("core.jobs:calibration.unknown", "unknown");
					if(status.value.defaultName == 'PERFORM')
					{
						levelText = i18n.t("core.jobs:calibration.perform", "perform");
						$j('#pwSpan').hide();
					}
					else if(status.value.defaultName == 'APPROVE')
					{
						levelText = i18n.t("core.jobs:calibration.approve", "approve");
						$j('#pwSpan').show();
					}
					else if(status.value.defaultName == 'SIGN')
					{
						levelText = i18n.t("core.jobs:calibration.sign", "sign");
						$j('#pwSpan').show();
					}
					else if(status.value.defaultName == 'SUPERVISED')
					{
						levelText = i18n.t("core.jobs:calibration.supervised", "perform (under supervision)");
						$j('#pwSpan').show();
					}
					// apply text to success box
					$j('#levelText').text(levelText);
				}
			}
		);
	}
}

function getWiStds(wiid)
{
	defaultstandardservice.getStandardsForWorkInstruction(wiid, null,
	{
		callback:function(stds)
		{
			displayStds('wi', stds);
		}
	});
}


function displayStds(type, stds)
{
	// clear table of previous procedure standards
	$j('table#procedurestds tbody#' + type + 'Stds tr:not(:first)').remove();
	
	// reset the procedure standards count
	$j('span#' + type + 'StdsSize').text(stds.length);
	
	// standards have been returned
	if(stds.length > 0)
	{							
		// loop through standards
		$j.each(stds, function(i)
		{
			// this default standard has been added to the additional procedure
			// standards table
			if($j('table#additionalstds tbody tr#addstd' + stds[i].instrument.plantid).length > 0)
			{
				// remove default standard from additional standards
				$j('table#additionalstds tbody tr#addstd' + stds[i].instrument.plantid).remove();
				// subtract one from additional standards count
				$j('span.addstdsSizeSpan').text(parseInt($j('span.addstdsSizeSpan').text()) - 1);
				// all additional standards have been removed
				if ($j('table#additionalstds tbody tr').length < 1)
				{
					// hide the additional standards table
					$j('table#additionalstds').removeClass().addClass('default2 hid');
				}
			}
				
			// variables for display
			var checked = '';
			var enforce = '';
			var rowcolor = '';
			var outOfCal = '';
			// use of default standard is enforced
			if(stds[i].enforceUse == true)
			{
				// populate variable to check input field
				checked = ' checked="checked"';
				// populate variable to display image for enforced standard
				enforce = '<img src="img/icons/bullet-tick.png" width="10" height="10" alt="' + i18n.t("core.jobs:calibration.enforceUse", "Enforce Use") + '" title="' + i18n.t("core.jobs:calibration.enforceUse", "Enforce Use") + '" />';
			}
			else
			{
				// populate variable to display image for non-enforced standard
				enforce = '<img src="img/icons/bullet-cross.png" width="10" height="10" alt="' + i18n.t("core.jobs:calibration.notEnforced", "Not Enforced") + '" title=' + i18n.t("core.jobs:calibration.notEnforced", "Not Enforced") + '" />';
			}
			// default procedure standard is out of calibration
			if(stds[i].instrument.outOfCalibration == true)
			{
				// populate variable to display message
				outOfCal = '<br/><span class="attention">' + i18n.t("outOfCal", "OUT OF CALIBRATION") + '</span>';
				// populate variable to highlight table row
				rowcolor = 'highlight';
				// uncalibrated standards allowed
				if (allowuncalibratedstds == false) 
				{
					// populate variable to disable the input field instead of
					// check
					// checked = ' disabled="disabled"';
					checked = ' ';
				}
			}
			// append the new default procedure standard to table
			$j('table#procedurestds tbody#' + type + 'Stds').append(
				'<tr id="std' + stds[i].instrument.plantid + '" class="' + rowcolor + '">' +
					'<td class="center">' +
						'<input type="checkbox" class="standardId" name="instrums" value="' + stds[i].instrument.plantid + '" onclick=" toggleStd(this); " ' + checked + ' /> ' + 
						'<input type="hidden" name="procStdsOnScreen" value="' + stds[i].id + '" />' +
					'</td>' +
					'<td>' +
						'<a href="viewinstrument.htm?plantid=' + stds[i].instrument.plantid + '&amp;ajax=instrument&amp;width=400" class="jconTip mainlink" name="' + i18n.t("instrInformation", "Instrument Information") + '" id="inst' + stds[i].instrument.plantid + '">' + 
							stds[i].instrument.plantid + 
						'</a>' +
					'</td>' +
					'<td>' +
						stds[i].instrument.model.mfr.name + ' ' + stds[i].instrument.model.model + ' ' + stds[i].instrument.model.description.description + ' ' + outOfCal +
					'</td>' +
					'<td>' + stds[i].instrument.serialno + '</td>' +
					'<td>' + stds[i].instrument.plantno + '</td>' +
					'<td class="center">' + enforce + '</td>' + 
				'</tr>');
		});
		
		// this script needs to be reloaded each time any new standards are
		// added so that the barcode tooltip works
		$j.getScript('script/thirdparty/jQuery/jquery.jtip.js');	
	}
	else
	{
		var word = '';
		if(type == 'p')
		{
			word = i18n.t("core.jobs:calibration.noDefaultStandardForProc", "No default standards for this procedure");
		}
		else if(type == 'wi')
		{
			word = i18n.t("core.jobs:calibration.noDefaultStandardForProc", "No default standards for this work instruction");
		}
		
		// append row for no default standards
		$j('table#procedurestds tbody#' + type + 'Stds')
			.append('<tr>' +
						'<td colspan="6" class="bold center">' + word +
						'</td>' +
					'</tr>');
	}
}

/**
 * this function creates the content of the thickbox used when adding additional
 * standards
 * 
 * @param {Boolean}
 *            allowuncalibratedstds indicates whether to allow uncalibrated
 *            standards
 */
function thickboxAdditionalStandardsContent()
{	
	// create thickbox content for additional standards
	var thickboxContent = 	'<div class="thickbox-box" id="additionalStdsTB">' +
								// add warning box to display error messages if
								// the insertion or update fails
								'<div class="hid warningBox1">' +
									'<div class="warningBox2">' +
										'<div class="warningBox3">' +
										'</div>' +
									'</div>' +
								'</div>' +
								'<fieldset>' +
									'<legend>' + i18n.t("core.jobs:calibration.additionalStantard", "Additional Standard") + '</legend>' +
									'<ol>' +
										'<li>' +
											'<label>' + i18n.t("barcodeLabel", "Barcode:") + '</label>' +
											'<input type="text" onkeypress=" if (event.keyCode==13) { findAdditionalStd($j(\'input#instBarcode\').val()); event.preventDefault(); } " id="instBarcode" name="instBarcode" />' +
										'</li>' +
										'<li>' +
											'<label>&nbsp;</label>' +
											'<input type="button" onclick=" findAdditionalStd($j(\'input#instBarcode\').val()); " value="' + i18n.t("add", "Add") + '" />' +
										'</li>' +
									'</ol>' +
								'</fieldset>' +
								'<table class="default2" id="addstandards" summary="This table displays standards temporarily before they are added to the page">' +
									'<thead>' +
										'<tr>' +
											'<th class="stdadd" scope="col">' + i18n.t("add", "Add") + '</th>' +
											'<th class="stdbar" scope="col">' + i18n.t("barcode", "Barcode") + '</th>' +
											'<th class="stdinst" scope="col">' + i18n.t("instrument", "Instrument") + '</th>' +
											'<th class="stdserial" scope="col">' + i18n.t("serialNo", "Serial No") + '</th>' +
											'<th class="stdplant" scope="col">' + i18n.t("plantNo", "Plant No") + '</th>' +
										'</tr>' +
									'</thead>' +
									'<tfoot>' +
										'<tr>' +
											'<td colspan="5">&nbsp;</td>' +
										'</tr>' +
									'</tfoot>' +
									'<tbody>' +
										'<tr>' +
											'<td colspan="5" class="center bold">' + i18n.t("addStandardsToTable", "Add Standards to table") + '</td>' +
										'</tr>' +
									'</tbody>' +
								'</table>' +
								'<div class="center">' +
									'<input type="button" value="' + i18n.t("addStandards", "Add Standards") + '" onclick=" addAdditionalStds(); " />' +
								'</div>' +
							'</div>';
	
	// return thickbox content
	return thickboxContent;
}

/**
 * this function finds the instrument using the plantid in the barcode field,
 * checks that it is a valid standard and adds it to the temporary table for
 * viewing before the list is submitted to the page.
 * 
 * @param {Integer}
 *            barcode plantid provided in the barcode field
 * @param {Boolean}
 *            allowuncalibratedstds indicates whether to allow uncalibrated
 *            standards
 */
function findAdditionalStd(barcode)
{	
	// check that instrument standard with this barcode is not already listed in
	// the
	// default procedure standards table or the additional standards table
	if($j('table#procedurestds tr#std' + barcode).length == 0 && $j('table#additionalstds tr#addstd' + barcode).length == 0)
	{
		// call dwr service to find this instrument standard
		instrumservice.findInstrumAsStandard(barcode,
		{
			callback:function(results)
			{	
				// error has occured finding instrument standard
				if(results.success != true)
				{
					// empty the error warning box
					$j('div#additionalStdsTB div.warningBox3').empty();
					// show error message in error warning box
					$j('div#additionalStdsTB div:first').removeClass('hid').addClass('vis').find('.warningBox3').text(results.message);
				}
				else
				{
					// set variable to instrument object
					var inst = results.results;
					// declare display variables
					var checked = ' checked="checked"';
					var outOfCal = '';
					var rowcolor = '';
					// this instrument standard is a standard
					if(inst.calibrationStandard == true)
					{
						// instrument standard is out of calibration
						if(inst.outOfCalibration == true)
						{
							// populate display variable with message
							outOfCal = '<br/><span class="attention">' + i18n.t("outOfCal", "OUT OF CALIBRATION") + '</span>';
							// populate display variable to highlight table row
							rowcolor = 'highlight';
							// check if uncalibrated instrument standards are
							// allowed
							if (allowuncalibratedstds == false) 
							{
								// populate variable to disable the input field
								// instead of check
								// checked = ' disabled="disabled"';
								checked = ' ';
							}
						}
						// thickbox table contains message of no instruments
						if ($j('div.thickbox-box table tbody tr td').length < 5)
						{
							// remove this message before adding a new row
							$j('div.thickbox-box table tbody tr:first').remove();
						}
						// variable to hold calibration indicator for instrument
						var calIndicator = '';
						// can the calibration timescale indicator be shown?
						if ((inst.inCalTimescalePercentage != null) && (inst.finalMonthOfCalTimescalePercentage != null))
						{											
							calIndicator +=	'<span class="relative">' +
												'<div class="calIndicatorBox">';
													// is this instrument out of
													// calibration?
													if ((inst.inCalTimescalePercentage == 100) && (inst.finalMonthOfCalTimescalePercentage == 100))
													{
														calIndicator += '<div class="outOfCalibration"></div>';
													}
													else
													{
														calIndicator += '<div class="inCalibration">' +
																			'<div class="inCalibrationLevel" style=" width: ' + inst.inCalTimescalePercentage + '%; "></div>' +
																		'</div>' +
																		'<div class="finalMonthOfCalibration">' +
																			'<div class="finalMonthOfCalibrationLevel" style=" width: ' + inst.finalMonthOfCalTimescalePercentage + '%; "></div>' +
																		'</div>';	
													}																				
								calIndicator +=	'</div>' +
											'</span>';
						}
						// append new instrument standard to the table
						$j('div.thickbox-box table tbody').append(	'<tr class="' + rowcolor + '">' + 
																		'<td class="center">' +
																			'<input type="checkbox" name="addStd" value="' + inst.plantid + '" ' + checked + ' /> ' + 
																			'<input type="hidden" name="outOfCal" id="outOfCal" value="' + inst.outOfCalibration + '" />' +
																			'<input type="hidden" name="modelid" id="modelid" value="' + inst.model.modelid + '" />' +
																		'</td>' +
																		'<td class="stdbar">' +
																			'<img src="img/icons/bullet-standard.png" width="12" height="12" class="image_inl" alt="' + i18n.t("standard", "Standard") + '" title="' + i18n.t("standard", "Standard") + '" /> ' +
																			calIndicator +
																			'<a href="viewinstrument.htm?plantid=' + inst.plantid + '" class="mainlink">' + inst.plantid + '</a>' +
																		'</td>' +
																		'<td>' + inst.model.mfr.name + ' ' + inst.model.model + ' ' + inst.model.description.description + outOfCal + ' </td>' +
																		'<td>' + inst.serialno + '</td>' +
																		'<td>' + inst.plantno + '</td>' +
																	'</tr>');
					}
				}
			}
		});
	}
	// instrument standard with this barcode is already listed in standards
	// table
	else
	{
		// display message
		$j.prompt(i18n.t("core.jobs:calibration.instrAlreadyOnTheList", {varBarcode : barcode, defaultValue : "Instrument with barcode " + barcode + " is already on the list of standards for this calibration"}));
	}
	// apply focus and clear the barcode input field
	$j('input#instBarcode').val('').focus();
}





/**
 * this function loops through all the additional procedure standards added to
 * the temporary table in thickbox which have been selected by ticking the
 * checkbox and then adds these procedure standards to the additional procedure
 * standards table on page.
 * 
 * @param {Boolean}
 *            allowuncalibratedstds indicates whether to allow uncalibrated
 *            standards
 */
function addAdditionalStds()
{	
	// loop through each instrument standard that has been added to the thickbox
	// table and is checked
	$j.each($j('table#addstandards tbody tr td input[name="addStd"]:checked'), function(i, n)
	{
		// additional standards table has no contents
		if ($j('table#additionalstds tbody tr').length == 0)
		{
			// display the additional standards table
			$j('table#additionalstds').removeClass().addClass('default2');
		}
		// declare display variables
		var checked = ' checked="checked"';
		var outOfCal = '';
		var rowcolor = '';
		// standard is out of calibration
		if($j(this).next().val() == 'true')
		{
			// populate display variable with message
			outOfCal = '<br/><span class="attention">' + i18n.t("outOfCal", "OUT OF CALIBRATION") + '</span>';
			// populate display variable to highlight table row
			rowcolor = 'highlight';
			// check if uncalibrated instrument standards are allowed
			if (allowuncalibratedstds == false) 
			{
				// populate variable to disable the input field instead of check
				// checked = ' disabled="disabled"';
				checked = ' ';
			}
			// find the original instrument text from thickbox table and remove
			// the 'out of cal' message so it
			// can be used when appended to the table below
			$j(this).parent().siblings(':eq(1)').find('span, br').remove();
		}
		// append new row to the additional standards table
		$j('table#additionalstds tbody').append('<tr id="addstd' + n.value + '" class="' + rowcolor + '">' +
													'<td class="center">' +
														'<input type="checkbox" class="standardId" name="instrums" value="' + n.value + '" ' + checked + ' /> ' + 
														'<input type="hidden" name="addStdsOnScreen" value="' + n.value + '" />' +
													'</td>' +
													'<td class="addstdbar">' +
														$j(this).parent().siblings(':eq(0)').find('a').remove().end().html() +
														'<a href="viewinstrument.htm?plantid=' + n.value + '&amp;ajax=instrument&amp;width=400" class="jconTip mainlink" name="' + i18n.t("instrInformation", "Instrument Information") + '" id="inst' + n.value + '">' + 
															n.value + 
														'</a>' +
														' <img src="img/icons/mouse.png" width="12" height="12" class="image_inl" alt="" title="" />' +
													'</td>' +
													'<td>' +
														'<a href="instrumentmodel.htm?modelid=' + $j(this).next().next().val() + '")" class="mainlink">' +
															 $j(this).parent().siblings(':eq(1)').text() +
														'</a>' +
														outOfCal +
													'</td>' +
													'<td>' + $j(this).parent().siblings(':eq(2)').text() + '</td>' +
													'<td>' + $j(this).parent().siblings(':eq(3)').text() + '</td>' +
												'</tr>');
														
		// add one to the additional procedure standards item count
		$j('span.addstdsSizeSpan').text(parseInt($j('span.addstdsSizeSpan').text()) + 1);		
					
	});
	// initialise tooltip anchor
	JT_init();	
	// close thickbox
	tb_remove();
}

/**
 * this function checks the strength of the password entered by the user and
 * displays text and coloured bar dependant on that strength
 * 
 * @param {String}
 *            password password entered by the user in input field
 */
function passwordStrength(password)
{
	// call dwr service to check the password strength
	calibrationservice.checkPasswordStrength(password,
	{
		callback:function(score)
		{
			// change text in div dependant on password strength
			$j('div#passwordDescription').text(desc[score]);
			// change class of coloured div dependant on password strength
        	$j('div#passwordStrength').removeClass().addClass("strength" + score);
        }	
	});
}

function toggleGroupCal(checkbox)
{
	if($j(checkbox).is(':checked'))
	{
		$j('div#groupCalItems').removeClass('hid').addClass('vis');
	}
	else
	{
		$j('div#groupCalItems').removeClass('vis').addClass('hid');
	}
}

function toggleStd(checkbox)
{
	var plantid = $j(checkbox).val();
	$j('table#procedurestds input:checkbox[value="' + plantid + '"]').attr('checked', $j(checkbox).attr('checked'));
}

function checkOnSubmit(form)
{
	if($j(form).find('input:checkbox[name="instrums"]:checked').length < 1)
	{
		if(confirm(i18n.t("core.jobs:calibration.noSelectedAnyCalStandards", "You have not selected any calibration standards. Are you sure you wish to begin this calibration?")))
		{
			$j(form).find('input:submit').attr('disabled', 'disabled');
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		$j(form).find('input:submit').attr('disabled', 'disabled');
		return true;
	}
}

function submitForm() {
	var form = $j('#newcalibrationform');
	var calProcess = $j('#processSelect').val();
	if(calProcesses['p' + calProcess] != null && calProcesses['p' + calProcess].toolLink != '') {
		$j('input#startFromTool').val(true);
		form = $j('#newcalibrationform');
		var url = $j(form).attr('action');
		var data = $j(form).serializeArray();
		data.push({name: 'jobItemId', value: $j('#jobItemId').html()});
		$j.ajax({
			type: 'POST',
			url: "startcalibrationtool.json",
			data: $j.param(data)
		}).done(function(result) {
			var oXHR = new XMLHttpRequest();
			oXHR.open("GET", result.toolLink, true);
			oXHR.onreadystatechange = function (oEvent) {
				if (oXHR.readyState === 4) {
					if (oXHR.status === 200) {
	            		console.log("Local Alligator response OK, status 200");
	            		form.submit();
	            	} else {
	            		console.log("Local Alligator response error, status:");
	            		console.log(oXHR.status);
	            		alert("Local Alligator response error, status:" + oXHR.status);
	            	}
				}
			};
			oXHR.send();
			console.log("Contacted local Alligator");
		}).fail(function(result) {
			alert(result.responseText);
		});
		return true;
	}
	// if there is no tool for the calibration process, submit the form directly
	else {
		form.submit();
		return true;
	}
}

/**
 * this method redirects the user to the multistart calibration page and uses
 * the job item id and job id to find items with the same state as the current
 * one
 * 
 * @param {Integer}
 *            jiid id of job item
 * @param {Integer}
 *            jobid id of job
 */
function redirectToMultiStart(jiid, jobid)
{
	window.location.href = 'multistart.htm?jobid=' + jobid + '&jiid=' + jiid;
}