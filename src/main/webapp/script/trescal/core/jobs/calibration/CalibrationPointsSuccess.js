/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	CalibrationPointsSuccess.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	08/10/2010
*					:	14/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * this method cleans up after cal points/requirements have been set
 * 
 * @param {String} type the type of calibration requirement being added/edited
 * @param {Integer} calreqid the id of the calibration requirement added/edited
 * @param {String} page the name of the page where the calibration requirement has been added
 * @param {Integer} genericid an id passed from the overlay when the calibration requirement was added/edited
 */
function cleanUpAfterPoints(type, calreqid, page, genericid)
{
	switch (page)
	{
		case 'quotation':
			updateQuotationPage(type, calreqid, page, genericid);
			break;
		case 'jiheader':
			updateJobItemHeader(type, calreqid, page, genericid);
			break;
		case 'instrument':
			updateInstrumentPage(type, calreqid, page, genericid);
			break;
		case 'instmodcompcalreqs':
			updateInstrumentModelCompanyCalReqsPage(type, calreqid, page, genericid);
			break;
		default:
			closePopup();
			break;
	}
}

/**
 * this method purely closes the overlay which is used to add/edit calibration requirements
 */
function closePopup()
{
	window.parent.loadScript.closeOverlay();
}

/**
 * this method is called when points have been added/edited on the view quotation page. This method will attempt to update
 * all links and notes for a particular model type on the quotation no matter how many times it appears.
 * 
 * @param {String} type the type of calibration requirement being added/edited
 * @param {Integer} calreqid the id of the calibration requirement added/edited
 * @param {String} page the name of the page where the calibration requirement has been added
 * @param {Integer} modelid id of the model for which calibration requirements were added/edited
 */
function updateQuotationPage(type, calreqid, page, id)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/calreqservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to retrieve the calibration requirement added/edited
			calreqservice.ajaxFindCalReq(calreqid,
			{
				callback:function(cr)
				{	
					// check cal req type
					if (type == 'instrument')
					{
						// variable for publish class
						var publishclass = 'instcalreq_highlight';
						// check publish property
						if (cr.publish)
						{
							publishclass = 'instcalreq_highlight_doc';
						}
						// update buttons when an instrument calibration requirement has been added
						$j('a.calReqButton-' + id, window.parent.document).siblings('a[class^="calReqButton"]').remove().end().closest('td').removeClass().addClass('notes ' + publishclass).end()
							.after('<a href="#" class="calReqButton-' + id + ' editPoints" onclick=" loadScript.createOverlay(\'external\', \'Edit Cal Points &amp; Requirements\', \'calibrationpoints.htm?type=instrument&page=quotation&calreqid=' + calreqid + '\', null, null, 80, 900, null); return false; " title="Edit Points &amp; Requirements"></a>')
							.remove();						
					}
					else
					{
						// variable for publish class
						var publishclass = 'compmodelcalreq_highlight';
						// check publish property
						if (cr.publish)
						{
							publishclass = 'compmodelcalreq_highlight_doc';
						}
						// update all buttons for this model on the page (may be more 
						// than one if the same model exists on the quote multiple times)
						$j('a.calReqButton-' + id, window.parent.document).closest('td').removeClass().addClass('notes ' + publishclass).end()
							.after('<a href="#" class="calReqButton-' + id + ' editPoints" onclick=" loadScript.createOverlay(\'external\', \'Edit Cal Points &amp; Requirements\', \'calibrationpoints.htm?type=companymodel&page=quotation&calreqid=' + calreqid + '\', null, null, 80, 900, null); return false; " title="Edit Points &amp; Requirements"></a>')
							.remove();
					}
					// create new list item for calibration requirements
					var content = 	'<li class="itemCalReq' + cr.id + '">' +
										'<div class="inlinenotes_note">';
										if (cr.pointSet)
										{
											content +=  '<span class="bold">Calibration Points:</span>' +
														'<span class="attention bold">' +
															'<span>';
																$j.each(cr.pointSet.points, function(i, p)
																{
																	content +=  '' + p.formattedPoint + ' ' + p.uom.formattedSymbol;
																	if (p.formattedRelatedPoint != '')
																	{
																		content += ' @ ' + p.formattedRelatedPoint + ' ' + p.relatedUom.formattedSymbol + '';
																	}
																	if ((i + 1) != cr.pointSet.points.length)
																	{
																		content += 	' | ';
																	}																	
																});
												content +=	'</span>' +
														'</span>' +
														'<br />';
										}
										else if (cr.range)
										{
											content +=	'<span class="bold">Calibration Range:</span>' +
														'<span class="attention bold">' + 
															cr.range.start;
															if (cr.range.end)
															{
																if (cr.range.end != '')
																{
																	content += ' to ' + cr.range.end;
																}
															}
															content += ' ' + cr.range.uom.formattedSymbol;
															if (cr.range.relational)
															{
																content +=	' @ ' + cr.range.formattedRelatedPoint + ' ' + cr.range.relatedUom.formattedSymbol;
															}
											content += 	'</span>' +
														'<br />';
										}
										else
										{
											content +=	'<span class="bold">Calibration Instructions:</span>' +
														'<br />';
										}
										if ((cr.privateInstructions) && (cr.privateInstructions != ''))
										{
											content +=	'<span class="attention bold">Private: ' + cr.privateInstructions.replace(/\n/gi, "&nbsp;<br />") + '</span>' +
														'<br />';
										}
										if ((cr.publicInstructions) && (cr.publicInstructions != ''))
										{
											content +=	'<span class="allgood bold">Public: ' + cr.publicInstructions.replace(/\n/gi, "&nbsp;<br />") + '</span>' +
														'<br />';
										}
								content += 	'<span class="bold">' + cr.source + '</span>' +
										'</div>' +
										'<div class="inlinenotes_edit">' +
											'<span>' +
												'<span class="bold attention">';
													if (cr.publish)
													{
														content +=	'Public Calibration Requirement';
													}
													else
													{
														content +=	'Private Calibration Requirement';
													}
									content +=	'</span>' +
												'<a href="#" class="editPoints" onclick=" loadScript.createOverlay(\'external\', \'Edit Cal Points &amp; Requirements\', \'calibrationpoints.htm?type=' + cr.classKey + '&page=' + page + '&calreqid=' + cr.id + '\', null, null, 80, 900, null); return false; " title="Edit Points &amp; Requirements">&nbsp;</a>' +		
												'<a href="#" class="deletePoints" onclick=" deactivateCalReqs(\'' + page + '\', ' + cr.id + ', \'' + cr.classKey + '\', ' + id + '); return false; " title="Remove Points &amp; Requirements">&nbsp;</a>' +
											'</span>' +
											'<br />' +
											'<span>';
											if (cr.lastModifiedBy)
											{
												content += cr.lastModifiedBy.name;
											}
											if (cr.lastModified)
											{
												content += ' (' + formatDate(cr.lastModified, 'dd.MM.yyyy - HH:mm:ss') + ')';
											}
							content +=	'</span>' + '</div>' +
										'<div class="clear-0"></div>' +
									'</li>';
					// check each occurence of this model in quote
					$j('a.calReqButton-' + id, window.parent.document).each(function(i)
					{
						// get note type value
						var notetype = $j(this, window.parent.document).next().val();
						// get the note type id value
						var notetypeid = $j(this, window.parent.document).next().next().val();
						// cal requirements present indicator
						var itemCalReqsPresent = false;
						// inline notes list present?
						if ($j('tr#' + notetype + notetypeid, window.parent.document).length > 0)
						{
							// check that the item cal requirements ol is present
							if ($j('tr#' + notetype + notetypeid + ' td:first fieldset ol.activeItemCalReqs', window.parent.document).length == 0)
							{
								// append this ordered list ready to receive list item
								$j('tr#' + notetype + notetypeid + ' td:first fieldset', window.parent.document).prepend('<ol class="activeItemCalReqs"></ol>');
							}
							// check for existing item cal requirement
							if ($j('tr#' + notetype + notetypeid + ' td:first fieldset ol.activeItemCalReqs li', window.parent.document).length == 1)
							{
								// set requirements present variable
								itemCalReqsPresent = true;
							}							
							// append new calibration requirement to existing note list
							$j('tr#' + notetype + notetypeid + ' td:first fieldset ol.activeItemCalReqs', window.parent.document).empty().prepend(content);
						}
						else
						{
							// create the new notes row for item
							var noterow = 	'<tr id="' + notetype + notetypeid + '" class="hid">' +
												'<td colspan="10">' +
													'<fieldset class="showActiveItemNotes">' +
														'<ol class="activeItemCalReqs">' +
															content +
														'</ol>' +
													'</fieldset>' +
												'</td>' +
											'</tr>';
							// add fieldset and list for inline notes
							$j('tr#item' + notetypeid, window.parent.document).after(noterow);
						}
						// no cal requirements existed before update?
						if (!itemCalReqsPresent)
						{
							// add one to the inline note count
							$j('#count' + notetype + notetypeid, window.parent.document).text(parseInt($j('#count' + notetype + notetypeid, window.parent.document).text()) + 1);
						}
						// inline notes hidden?
						if ($j('#' + notetype + notetypeid + '[class*=\'hid\']', window.parent.document).length)
						{
							// show the inline notes list
							$j('#' + notetype + notetypeid, window.parent.document).removeClass().addClass('vis');
						}
					});								
					// close overlay
					closePopup();
				}
			});
		}
	});
}

/**
 * this method is called when points have been added/edited/overridden on the job item header. This method will attempt to update
 * the list item containing the calibration requirement details.
 * 
 * @param {String} type the type of calibration requirement being added/edited
 * @param {Integer} calreqid the id of the calibration requirement added/edited
 * @param {String} page the name of the page where the calibration requirement has been added
 * @param {Integer} jobitemid id of the job item for which calibration requirements were added/edited/overridden
 */
function updateJobItemHeader(type, calreqid, page, jobitemid)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/calreqservice.js', 'script/trescal/core/tools/CalibrationRequirements.js'), true,
	{
		callback: function()
		{
			// call dwr service to retrieve the calibration requirement added/edited
			calreqservice.ajaxFindCalReq(calreqid,
			{
				callback:function(cr)
				{		
					// use calibration requirement and get content
					var content = newCalReqContent(cr, page, jobitemid);
					// first calibration requirement added (i.e. does not already have a model cr or an inst cr)
					if ($j('#previousCalReqListItemIdentifier', window.parent.document).length == 0)
					{
						// check that the list item containing calibration requirements exists
						if ($j('li.defaultCalReqListItemIdentifier', window.parent.document).length > 0)
						{
							// calibration requirements exists so remove from page
							$j('li.defaultCalReqListItemIdentifier', window.parent.document).after(content).remove();
						}
					}
					else
					{
						// check that the list item containing calibration requirements exists
						if ($j('li.' + $j('#previousCalReqListItemIdentifier', window.parent.document).val(), window.parent.document).length > 0)
						{
							// calibration requirements exists so remove from page
							$j('li.' + $j('#previousCalReqListItemIdentifier', window.parent.document).val(), window.parent.document).after(content).remove();
						}
					}					
					// close overlay
					closePopup();
				}
			});
		}
	});
}

/**
 * this method is called when points have been added/edited/overridden on the view instrument page. This method will attempt to update
 * the list item containing the calibration requirement details.
 * 
 * @param {String} type the type of calibration requirement being added/edited
 * @param {Integer} calreqid the id of the calibration requirement added/edited
 * @param {String} page the name of the page where the calibration requirement has been added
 * @param {Integer} plantid id of the instrument for which calibration requirements were added/edited/overridden
 */
function updateInstrumentPage(type, calreqid, page, plantid)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/calreqservice.js', 'script/trescal/core/tools/CalibrationRequirements.js'), true,
	{
		callback: function()
		{
			// call dwr service to retrieve the calibration requirement added/edited
			calreqservice.ajaxFindCalReq(calreqid,
			{
				callback:function(cr)
				{					
					// use calibration requirement and get content
					var content = newCalReqContent(cr, page, plantid);
					// first calibration requirement added (i.e. does not already have a model cr or an inst cr)
					if ($j('#previousCalReqListItemIdentifier', window.parent.document).length == 0)
					{
						// check that the list item containing calibration requirements exists
						if ($j('li.defaultCalReqListItemIdentifier', window.parent.document).length > 0)
						{
							// calibration requirements exists so remove from page
							$j('li.defaultCalReqListItemIdentifier', window.parent.document).after(content).remove();
						}
					}
					else
					{
						// check that the list item containing calibration requirements exists
						if ($j('li.' + $j('#previousCalReqListItemIdentifier', window.parent.document).val(), window.parent.document).length > 0)
						{
							// calibration requirements exists so remove from page
							$j('li.' + $j('#previousCalReqListItemIdentifier', window.parent.document).val(), window.parent.document).after(content).remove();
						}
					}					
					// close overlay
					closePopup();
				}
			});
		}
	});
}

/**
 * this method is called when points have been added/edited on the instrument model company calibration requirements page. This method will attempt to update
 * the table row containing the calibration requirement details links.
 * 
 * @param {String} type the type of calibration requirement being added/edited
 * @param {Integer} calreqid the id of the calibration requirement added/edited
 * @param {String} page the name of the page where the calibration requirement has been added
 * @param {Integer} modelid id of the instrument model for which calibration requirements were added/edited
 */
function updateInstrumentModelCompanyCalReqsPage(type, calreqid, page, modelid)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/calreqservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to retrieve the calibration requirement added/edited
			calreqservice.ajaxFindCalReq(calreqid,
			{
				callback:function(cr)
				{					
					// use calibration requirement and get content
					var content =	'<a href="#" class="editPoints" onclick=" loadScript.createOverlay(\'external\', \'Edit Cal Points &amp; Requirements\', \'calibrationpoints.htm?type=' + type + '&page=' + page + '&calreqid=' + cr.id + '\', null, null, 80, 900, null); return false; " title="Edit Points &amp; Requirements">&nbsp;</a>' +
									'<a href="#" class="deletePoints" onclick=" deactivateCalReqs(\'' + page + '\', ' + cr.id + ', \'' + type + '\', ' + modelid + '); return false; ">&nbsp;</a>';
					// check that the table row containing calibration requirements exists
					if ($j('tr.' + page + modelid, window.parent.document).length > 0)
					{
						// calibration requirements exists so remove from page
						$j('tr.' + page + modelid, window.parent.document).addClass('highlight-green').find('td:first').html(content);
					}		
					// close overlay
					closePopup();
				}
			});
		}
	});
}