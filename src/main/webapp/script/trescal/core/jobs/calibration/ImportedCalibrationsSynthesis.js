
var pageImports = new Array ('script/thirdparty/jQuery/jquery.dataTables.min.js',
		'script/thirdparty/jQuery/dataTables.responsive.min.js',
		'script/thirdparty/jQuery/dataTables.buttons.min.js',
		'https://cdn.rawgit.com/ashl1/datatables-rowsgroup/v1.0.0/dataTables.rowsGroup.js',
		'script/thirdparty/jQuery/buttons.colVis.min.js');

function init() {
	console.log('Local init : Calling updateJobSelector');
	updateJobSelector(selectedJobId);
	console.log('Local init : Calling initDatatable');
	initDatatable();
	console.log('Local init : Completed');
}

function initDatatable(){
	
	/*
	 * rowspan does not work with datatables plugin, multiple workarounds has to
	 * be put in place
	 */
	
	// columns to group
	var toGroup = new Array();
	$j('tr:nth-of-type(1) td[grouped]').each((i,t)=>{
		toGroup.push($j('tr:nth-of-type(1) td').index(t));
	});

	var table = $j('#importedCalibration').DataTable({
		paging: false,
		searching: false,
	    ordering:  false,
	    bInfo : false,
	    responsive:false,
	    autoWidth: false,
	    rowsGroup: toGroup,
	    columnDefs: [
	    	{ targets: '_all', defaultContent: '-'}
	    ],
	    dom: 'Bfrtip',
	    buttons: [ {
	        extend: 'colvis',
	        columns: '[data-type]',
	        text: i18n.t("importedcalibrations.columnsvisibility:Columns visibility", "Columns visibility")
	    } ]

	});
	
	// column visibility button
	$j('.toggle-vis').on( 'click', function (e) {
        e.preventDefault();
 
        // Get the column API object
        var column = table.column( $j(this).attr('data-column') );
 
        // Toggle the visibility
        column.visible( ! column.visible() );
    });
	
	// columns to hide by default
	var columnsToHide = new Array();
	var columnsLength = table.columns().data().length;
	for (var i = 0; i < columnsLength; i++) {
		if($j(table.column(i).header()).hasClass('hiddenByDefault')){
			table.column(i).visible(false);
		}
	}
	
	// table.draw();
	
}

function updateJobSelector(selectedJobId) {

	var jobSelector = $j("#jobSelector");

	$j(joJiWr)
			.each(
					function(index, job) {
						var opt = document.createElement("option");
						opt.value = job.jobId;
						opt.innerHTML = job.jobno;

						jobSelector.append(opt);

						// auto select job
						if (!selectedJobId) {
							/*
							 * first time page called,try to find a job that
							 * have jobitems that could have the same
							 * instruments
							 */

							// get jobitems
							var jiWr = joJiWr.find(function(element) {
								return element.jobId == job.jobId;
							}).jiWrs;

							$j(jiWr)
									.each(
											function(index, jobitem) {

												var atLeastOneInstrFound = $j
														.makeArray(
																$j(".calFirstRow"))
														.some(
																function(row) {
																	var plantid = $j(
																			row)
																			.attr(
																					'plantid');
																	var plantno = $j(
																			row)
																			.attr(
																					'plantno');

																	return jobitem.instPlantid == plantid
																			|| jobitem.instPlantno == plantno;
																});

												// select current jobitem
												if (atLeastOneInstrFound)
													// We manually call updateJobItemSelectors() once at end, rather than on each update. 
													$j(jobSelector).val(job.jobId);												
//													$j(jobSelector).val(
//															job.jobId).change();

												// exit loop
												// return false;
											});

						} else {
							if (job.jobId === selectedJobId)
								// We manually call updateJobItemSelectors() once at end, rather than on each update.
								$j(jobSelector).val(job.jobId);
//								$j(jobSelector).val(job.jobId).change();
						}

					});

	// update jobitem selectors
	updateJobItemSelectors();
}

function updateJobItemSelectors() {

	var selectedJobIdValue = $j("#jobSelector").val();
	
	console.log('Updating job item selectors for job id '+selectedJobIdValue);

//	var jiWr = joJiWr.find(function(element) {
//		return element.jobId == selectedJobIdValue;
//	}).jiWrs;
	
	var list = joJiWr.find(function(element) {
		return element.jobId == selectedJobIdValue;
	});
	
	if(list === undefined)
		return;

	var jiWr = list.jiWrs
	
	// update ji count
	$j("#jobitemCount").text(jiWr.length);

	// for each selector
	$j(".jobitemSelector").each(
			function(index, selector) {

				// clear selector
				$j(selector).empty();

				// a first empty option
				var emptyOpt = document.createElement("option");
				selector.append(emptyOpt);

				// preselected value if available
				var preselectedvalue = $j(selector).attr('preselectedvalue');

				// populate and auto select
				$j(jiWr).each(
						function(index) {

							var opt = document.createElement("option");
							opt.value = this.jobitemId;
							opt.innerHTML = "#" + this.itemNo + " "
									+ this.instPlantidText + " "
									+ this.instPlantnoText;

							var stateOpt = document.createElement("option");
							stateOpt.innerHTML = "   (" + this.state + ")";
							stateOpt.style = "color:gray;font-style:italic;";
							stateOpt.disabled = "disabled";

							selector.append(opt);
							selector.append(stateOpt);

							if (preselectedvalue == this.jobitemId) {
								// set preselected value
								$j(selector).val(this.jobitemId).change();
							}

							if (!preselectedvalue) {
								// auto select jobitem
								var trRow = $j(selector).parent().parent();
								var plantid = $j(trRow).attr('plantid');
								var plantno = $j(trRow).attr('plantno');

								if (plantid != ''
										&& plantid == this.instPlantid)
									$j(selector).val(this.jobitemId).change();

								if (plantno != ''
										&& plantno == this.instPlantno)
									$j(selector).val(this.jobitemId).change();

							}

						});

				// update link to jobitem anchor
				updateLinkToJobitemAnchor(selector);

				// update is contract review column
				updateContractReviewColumn(selector);

				// update workrequirements
				updateWorkrequirementSelector(selector);

				// hide/show 'add instrument' button
				hideShowAddInstrumentButton(selector);

			});

}

function updateLinkToJobitemAnchor(jobitemSelector) {

	var selectedJobitemIdValue = $j(jobitemSelector).val();
	var anchor = $j(jobitemSelector).next().next(".linktojobitem");

	if (selectedJobitemIdValue) {
		$j(anchor).show();
		$j(anchor).attr("href",
				"jiactions.htm?jobitemid=" + selectedJobitemIdValue);
	} else {
		$j(anchor).hide();
	}

}

function updateContractReviewColumn(jobitemSelector) {

	var parentTd = $j(jobitemSelector).parent();
	var contractReviewTd = $j(parentTd).next("td");
	var contractReviewedHiddenInput = $j(contractReviewTd).next(
			".contractReviewedHiddenInput");
	var selectedJobIdValue = $j("#jobSelector").val();
	var selectedJobitemIdValue = $j(jobitemSelector).val();

	var jw = joJiWr.find(function(element) {
		return element.jobId == selectedJobIdValue;
	}).jiWrs.find(function(element) {
		return element.jobitemId == selectedJobitemIdValue;
	});

	$j(contractReviewedHiddenInput).val(false);

	if (jw !== undefined && jw.contractReviewed !== undefined) {
		if (jw.contractReviewed == true) {
			$j(contractReviewTd).find(".yes").show();
			$j(contractReviewTd).find(".no").hide();
			$j(contractReviewedHiddenInput).val(true);
		} else if (jw.contractReviewed == false) {
			$j(contractReviewTd).find(".yes").hide();
			$j(contractReviewTd).find(".no").show();
		}
	} else {
		$j(contractReviewTd).find(".yes").hide();
		$j(contractReviewTd).find(".no").hide();
	}

}

function hideShowAddInstrumentButton(jobitemSelector) {

	var selectedJobIdValue = $j("#jobSelector").val();
	var jobitemidValue = $j(jobitemSelector).val();

	var parentTd = $j(jobitemSelector).parent();
	var actionTd = $j(parentTd).next(".actiontd");
	var addinstrumentbutton = $j(actionTd).next(".addinstrumentbutton");

	// if empty hide 'add instrument' button
	if (jobitemidValue != '') {
		$(addinstrumentbutton).hide();
	} else {
		$(addinstrumentbutton).show();
	}

}

function updateWorkrequirementSelector(jobitemSelector) {

	var selectedJobIdValue = $j("#jobSelector").val();
	var jobitemidValue = $j(jobitemSelector).val();

	// get wr selector
	var parentTr = $j(jobitemSelector).parent().parent();
	var workrequirementSelector = $j(parentTr).find('.workRequirementSelector');
	$j(workrequirementSelector).empty();
	
	// get service type id from 'SERVICETYPE_ID' or 'EXPECTED_SERVICE' columns
	var serviceTypeId = $j(parentTr).find('td[data-type=SERVICETYPE_ID]').attr('data-servicetypeid');
	if(!serviceTypeId)
		serviceTypeId = $j(parentTr).find('td[data-type=EXPECTED_SERVICE]').attr('data-servicetypeid');

	if (jobitemidValue) {

		var workrequirements = joJiWr.find(function(element) {
			return element.jobId == selectedJobIdValue;
		}).jiWrs.find(function(element) {
			return element.jobitemId == jobitemidValue;
		}).workrequirements;

		// iterate on wr and create options in correspondate select
		$j(workrequirements).each(function(index) {
			var opt = document.createElement("option");
			opt.value = this.wrId;
			opt.innerHTML = this.wrText;
			opt.setAttribute('data-servicetypeid', this.serviceTypeId);

			workrequirementSelector.append(opt);

			// if servicetypeid exists, and we have that service type id within
			// the list
			if(serviceTypeId 
					&& $j(workrequirementSelector).find('option[data-servicetypeid='+serviceTypeId+']').length>0){
				// get first wr with this service type id
				let wrid = $j(workrequirementSelector).find('option')
					.filter('[data-servicetypeid='+serviceTypeId+']').attr('value');
				// select wr
				$j(workrequirementSelector).val(wrid).change();
			}else if(this.nextWr) {
				// select the next wr
				$j(workrequirementSelector).val(this.wrId).change();
			}

		});

	}
}

function jobitemSelectorOnChange(jobitemSelector) {
	updateLinkToJobitemAnchor(jobitemSelector);
	updateContractReviewColumn(jobitemSelector);
	updateWorkrequirementSelector(jobitemSelector);
}

/**
 * submits data in a new window/tab
 */


function CheckautomaticContractReviewCheckBox() {

	var size = document.querySelectorAll(".automaticContractReview").length;
	var checkbox = document.querySelectorAll(".automaticContractReview");
	if (document.getElementById('auto-contractReview').checked == true) {
		for (i = 0; i < size; i++) {
			checkbox[i].checked = true;
		}
	} else if (document.getElementById('auto-contractReview').checked == false) {
		for (i = 0; i < size; i++) {
			checkbox[i].checked = false;
		}
	}
}

function selectedRecordsToImport() {
	var size = document.querySelectorAll("input.selectCheckbox").length;
	var checkbox = document.querySelectorAll("input.selectCheckbox");
	if (document.getElementById('selectRecordsToImport').checked == true) {
		for (i = 0; i < size; i++) {
			checkbox[i].checked = true;
		}
	} else if (document.getElementById('selectRecordsToImport').checked == false) {
		for (i = 0; i < size; i++) {
			checkbox[i].checked = false;
		}
	}

//	var size = document.querySelectorAll("#select-RecordsToImport").length;
//	var checkbox = document.querySelectorAll("#select-RecordsToImport");
//	if (document.getElementById('selectRecordsToImport').checked == true) {
//		for (i = 0; i < size; i++) {
//			checkbox[i].checked = true;
//		}
//	} else if (document.getElementById('selectRecordsToImport').checked == false) {
//		for (i = 0; i < size; i++) {
//			checkbox[i].checked = false;
//		}
//	}
}

function preventRedirectionsifNoRecordsSelected() {
	var nbOfcheckedCheckboxs = document
		.querySelectorAll("input.selectCheckbox:checked").length;
	if (nbOfcheckedCheckboxs == 0) {
	document.getElementById("warningBox").style.display = "block";
	event.preventDefault();
	}

//	var nbOfcheckedCheckboxs = document
//			.querySelectorAll("#select-RecordsToImport:checked").length;
//	if (nbOfcheckedCheckboxs == 0) {
//		document.getElementById("warningBox").style.display = "block";
//		event.preventDefault();
//	}
}

function workRequirementCatalog(workrequirementSelector) {
	
	var parentTr = $j(workrequirementSelector).parent().parent();
	
	// get current jobitem id
	var jobItemId = $j(parentTr).find('.jobitemSelector').val();
	
	// get service type id from 'SERVICETYPE_ID' or 'EXPECTED_SERVICE' columns
	var serviceTypeId = $j(parentTr).find('td[data-type=SERVICETYPE_ID]').attr('data-servicetypeid');
	if(!serviceTypeId)
		serviceTypeId = $j(parentTr).find('td[data-type=EXPECTED_SERVICE]').attr('data-servicetypeid');

	// prepare param for servicetype
	var serviceTypeIdParam = '';
	if(serviceTypeId)
		serviceTypeIdParam = '&serviceTypeId='+serviceTypeId;
	
	// create new overlay
	var contentUri =  'workrequirementlookup.htm?jobItemId='+jobItemId+serviceTypeIdParam;
	var title = i18n.t("core.jobs:jobItem.addWorkRequirement", "Add Work Requirement");
	loadScript.createOverlay('external', title, contentUri, null, null, 90, 960, null);
	
	// get overlay object
	var overlayApi = $j('div.overlay_wrapper').overlay({data: 'overlay'});
	
	// redifine close handler function(s) : close the overlay then submit a
	// 'newAnalysis'
	$j._data(overlayApi,'events').onClose.forEach((e,i)=>{
		e.handler = function(){
			overlayApi.close();
			$j('#newAnalysisButton').click();
		}
	});
}
