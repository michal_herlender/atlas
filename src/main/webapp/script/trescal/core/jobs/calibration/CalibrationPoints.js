/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Richard Dean
*
*	FILENAME		:	CalibrationPoints.js
*	DESCRIPTION		:	Page-specific javascript file.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 
*						
*	KNOWN ISSUES	:	-
*	HISTORY			:	10/01/2008 - RD - Created File
*					:	28/02/2008 - SH - modified file when tidied
*					:	14/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (
								'dwr/interface/pointsettemplateservice.js'	);

/** 
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
 var menuElements = new Array( 	{ anchor: 'points-link', block: 'points-tab' },
 								{ anchor: 'range-link', block: 'range-tab' },
 								{ anchor: 'textonly-link', block: 'textonly-tab' } );

/**
 * this method takes the value chosen in the first select box and cascades down the remaining select boxes
 * setting to the same value
 * 
 * @param {Object} listitem the list item in which the first select box is located
 * @param {Boolean} indicates if we want to update first list of uom selects or last
 */
function cascadeUOM(listitem, first)
{
	// updating first list of uom's?
	if (first)
	{
		// get all select boxes in list and set value the same as the first
		$j('ol#pointslist li:not(:first) select[name^="metrics"]').each(function(i, n){
			n.value = $j(listitem).find('select:first').val();
		});
	}
	else
	{
		// get all select boxes in list and set value the same as the first
		$j('ol#pointslist li:not(:first) select[name^="relativeMetrics"]').each(function(i, n){
			n.value = $j(listitem).find('select:last').val();
		});
	}
}

/**
 * this method locates the 'Create Falling' link next to the last points input with a value entered
 * 
 */
function assignPointsFalling()
{
	// remove the link previously created
	$j('ol#pointslist li:not(:first) a#pointsfalling').remove();
	// create new variable
	var id = 0;
	// loop through all points inputs
	$j('ol#pointslist li:not(:first) input[type="text"][name^="points"]').each(function(i, n){
		// input has a value
		if (n.value != '')
		{
			// assign value to variable
			id = i + 1;
		}
	});
	// do not append link when all inputs are blank
	if (id != 0)
	{
		// append anchor to the last input in list that has content
		$j($j('ol#pointslist li:not(:first) input[type="text"][name^="points"]:eq(' + id + ')').parent()).prev().append('<a href="#" class="mainlink" id="pointsfalling" onclick=" createPointsFalling(); return false; ">' + i18n.t("core.jobs:calibration.createFalling", "Create Falling") + '</a>');	
	}
}

/**
 * this method selects all points that have been added to the list prior to pressing the 'Create Falling' link and
 * then trims the array of blank values at end before removing the last value as this is the highest point and then assigns
 * the remaining values to point inputs (if there are not enough inputs in list then new ones are created)
 * 
 */
function createPointsFalling()
{
	// declare array
	var pointArray = [];
	// loop through all point inputs
	$j('ol#pointslist li:not(:first) input[type="text"][name^="points"]').each(function(i, n)
	{
		// add each point value to array
		pointArray.push(n.value);
	});
	// loop through array from end to beginning
	for(var x = pointArray.length - 1; x > 0; x--)
	{
		// end array value is empty
		if (pointArray[x] == '')
		{
			// remove the last array value
			pointArray.pop();
		}
		else
		{
			// break from loop
			break;
		}
	}
	// remove the last array value
	pointArray.pop();
	// blank points left on page less than array length
	if ($j('ol#pointslist li:not(:first) input[type="text"][name^="points"]:gt(' + pointArray.length + ')').length < pointArray.length)
	{
		// get amount of blank points we need by subtracting blank points left on page from the array
		var pointsToAdd = pointArray.length - $j('ol#pointslist li:not(:first) input[type="text"][name^="points"]:gt(' + pointArray.length + ')').length;
		// add points needed to page
		addPoint(pointsToAdd);
	}

	// loop through all point inputs after last with value
	$j('ol#pointslist li:not(:first) input[type="text"][name^="points"]:gt(' + pointArray.length + ')').each(function(i, n)
	{
		// check that its blank
		if (n.value == '')
		{
			// array has values
			if (pointArray.length)
			{
				// add last value in array to input value
				n.value = pointArray[pointArray.length - 1];
				// remove the last value in array
				pointArray.pop();
			}
		}
	});
	// locate the points falling link
	assignPointsFalling();
}

/**
 * this method adds a new list item containing calibration point form elements. The number of new calibration point
 * list items added is dependant on the count parameter passed 
 * 
 * @param {Integer} count amount of new calibration points to append
 * @param {Boolean} indicates if currently using relational values
 */
function addPoint(count)
{
	// get point size value from hidden input
	var pointsize = $j('#pointsize').val();
	// turn the uom list into a string list of select options
	var uomList = $j('ol#pointslist li:not(:first) select[name^="metrics"]:first').html();
	// loop through point count
	for(var i = 0; i < count; i++)
	{				
		// add one to point size
		pointsize++;
		// determine whether to display relational points
		var vis = $j('input#currentlyRelational').is(':checked') ? 'vis' : 'hid';
		// append new list item and form elements to list
		var content = 	'<li id="point' + pointsize + '">' +
							'<label>' + i18n.t("core.jobs:calibration.point", "Point") + ' ' + (pointsize + 1) + ':</label>' +
							'<input type="text" size="4" name="points[' + pointsize + ']" tabindex="' + (pointsize + 1) + '" value="" onkeyup=" assignPointsFalling(); return false; " />' +
							'<select style=" margin-left: 12px; " name="metrics[' + pointsize + ']">' +
								uomList	+	
							'</select> ' +
							'<img src="img/icons/arrow_down.png" style=" visibility: hidden; " width="10" height="16" alt="" title="" />' +				
							' <span class="' + vis + '">&nbsp;@&nbsp;</span> ' +
							'<input type="checkbox" onclick=" clearRelationalPoint(this); " class="' + vis + '" checked="checked" /> ' +
							'<span class="relationalPoint ' + vis + '">' +
								'<input type="text" size="4" name="relativePoints[' + pointsize + ']" value="" />' +
								'<select style=" margin-left: 12px; " name="relativeMetrics[' + pointsize + ']">' +
									uomList + 
								'</select>' +
							'</span>' +
														
							'</li>';
		
		if(pointsize >= 1)
		{
			$j('ol#pointslist li[id^="point"]:last').after(content);
		}
		else
		{
			$j('ol#pointslist li:first').after(content);
		}
	}
	// update the points size count
	$j('#pointsize').val(pointsize);
	// locate the points falling link
	assignPointsFalling();
}

/**
 * this method tests if the chbx object is ticked, if true then two further list items are appended containing
 * form elements to supply a template name and description, if false then the two list items are removed
 * 
 * @param {Object} chbx checkbox input field for choosing to save points as template
 */
function toggleSaveTemplate(chbx)
{
	// checked?
	if(chbx.checked == true)
	{
		// append two new list items after one containing checkbox
		$j('#savetemplate').after(	'<li>' +
										'<label>' + i18n.t("core.jobs:calibration.templateName", "Template Name:") + '</label>' +
										'<input name="template.title" id="template.title" value="" type="text"/>' +
									'</li>'+
									'<li>' +
										'<label>' + i18n.t("core.jobs:calibration.templateDescription", "Template Description:") + '</label>' +
										'<textarea name="template.description" id="template.description"></textarea>' +
									'</li>');
		// set value of hidden input to true		
		$j('#saveAsTemplate').val(true);
		// return false to stop page resetting
		return false;
	}
	else
	{
		// remove the two list items from page
		$j('#savetemplate').next().remove().end().next().remove();
		// set value of hidden input to false
		$j('#saveAsTemplate').val(false);
		// return false to stop page resetting
		return false;
	}
}

/**
 * this method loads a previously saved points template with the given id and places all values on the page
 * as a new set of points
 * 
 * @param {Integer} tempid id of the saved points template
 */
function loadPointsTemplate(tempid)
{
	// tempid empty?
	if(tempid != '')
	{
		// call dwr service to load the requested points template
		pointsettemplateservice.findPointSetTemplateWithPoints(tempid, 
		{
			callback:function(template)
			{
				// template has been returned
				if(template)
				{
					// template contains points
					if(template.points.length > 0)
					{
						// initialise list and point size value
						$j('ol#pointslist li[id^="point"]:gt(8)').remove();
						$j('#pointsize').val(8);
						
						// set length of recorded points list
						var recordedPointsLength = template.points.length - 1;
						// recorded points list longer than current points displayed on page
						if (recordedPointsLength > $j('#pointsize').val())
						{
							// calculate number of points to add
							var pointsToAdd = recordedPointsLength - $j('#pointsize').val();
							// add points to list
							addPoint(pointsToAdd);
						}								
						// iterate over the template points and set values of input fields and select boxes
						$j.each(template.points, function(i)
						{
							// assign object to variable
							var point = template.points[i];
							// get input field and assign value		
							$j('ol#pointslist li:not(:first) input[type="text"][name^="points"]:eq(' + i + ')').attr('value', point.point);
							// get select box and assign value
							$j('ol#pointslist li:not(:first) select[name^="metrics"]:eq(' + i + ')').attr('value', point.uom.id);
						});	
						// clear any remaining input fields
						$j('ol#pointslist li:not(:first) input[type="text"][name^="points"]:gt(' + recordedPointsLength + ')').attr('value', '');
						// reset any remaining select boxes
						$j('ol#pointslist li:not(:first) select[name^="metrics"]:gt(' + recordedPointsLength + ')').attr('value', 1);
						// locate the points falling link
						assignPointsFalling();	
					}
				}
			}
		});
	}
}

function toggleRelational(checkbox, alreadyRelational)
{
	if($j(checkbox).is(':checked'))
	{
		if (!alreadyRelational)
		{
			$j('span.relationalPoint').each(function(i)
			{
				$j(this).removeClass('hid').addClass('vis');
				$j(this).prev('input[type="checkbox"]').attr('checked', 'checked').removeClass('hid').addClass('vis').prev().removeClass('hid').addClass('vis');
			});
		}
		else
		{
			$j('span.relationalPoint').removeClass('hid').addClass('vis');
		}
	}
	else
	{
		$j('span.relationalPoint').each(function(i)
		{
			$j(this).removeClass('vis').addClass('hid');
			$j(this).prev('input[type="checkbox"]').attr('checked', '').removeClass('vis').addClass('hid').prev().removeClass('vis').addClass('hid');
			$j(this).find('input[type="text"]:first').val('');
			$j(this).find('select').find('option:first').attr('selected', 'selected');
		});	
	}
}

function clearRelationalPoint(checkbox)
{
	if (!$j(checkbox).is(':checked'))
	{
		$j(checkbox).next().find('input[type="text"]:first').val('');
		$j(checkbox).next().find('select').find('option:first').attr('selected', 'selected');
		$j(checkbox).next().removeClass('vis').addClass('hid');
	}
	else
	{
		$j(checkbox).next().removeClass('hid').addClass('vis');
	}
}

function toggleRelationalRange(checkbox)
{
	if($j(checkbox).is(':checked'))
	{
		$j('span.relationalRange').removeClass('hid').addClass('vis');
	}
	else
	{
		$j('span.relationalRange').removeClass('vis').addClass('hid');		
	}
}

/**
 * this method sets the publish value of the calibration requirement
 * 
 * @param {Object}
 * @param {Boolean}
 */
function publishCalReq(anchor, publish)
{
	// publish cal req?
	if (publish)
	{
		$j(anchor).removeClass().addClass('publishCalreq').next().removeClass().addClass('publishCalreq_inactive');
	}
	else
	{
		$j(anchor).removeClass().addClass('publishCalreq').prev().removeClass().addClass('publishCalreq_inactive');
	}
	$j(anchor).siblings('input').val(publish);
}
