/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	ViewStandardsUsageAnalysis.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	21/01/2021 - Laila MADANI - Created File
****************************************************************************************************************************************/

var pageImports = new Array ('script/trescal/core/utilities/DocGenPopup.js',
							 'script/trescal/core/tools/FileBrowserPlugin.js');

var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );
/** 
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = new Array( 	{ anchor: 'files-link', block: 'files-tab' },
 								{ anchor: 'update-link', block: 'update-tab' });