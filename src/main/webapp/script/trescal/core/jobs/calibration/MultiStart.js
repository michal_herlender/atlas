/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: Jamie Vinall
*
*	FILENAME		:	MultiStart.js
*	DESCRIPTION		:	This javascript file is used on the multistart.vm page.
*					: 	
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	08/09/2010 - JV - Created File
*					:	14/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * this function creates the content of the thickbox used when adding additional standards
 * 
 * @param {Boolean} allowuncalibratedstds indicates whether to allow uncalibrated standards
 */
function thickboxAdditionalStandardsContent()
{	
	// create thickbox content for additional standards
	var thickboxContent = 	'<div class="thickbox-box" id="additionalStdsTB">' +
								// add warning box to display error messages if the insertion or update fails
								'<div class="hid warningBox1">' +
									'<div class="warningBox2">' +
										'<div class="warningBox3">' +
										'</div>' +
									'</div>' +
								'</div>' +
								'<fieldset>' +
									'<legend>' + i18n.t("core.jobs:calibration.additionalStantard", "Additional Standard") + '</legend>' +
									'<ol>' +
										'<li>' +
											'<label>' + i18n.t("barcodeLabel", "Barcode:") + '</label>' +
											'<input type="text" onkeypress=" if (event.keyCode==13) { findAdditionalStd($j(\'input#instBarcode\').val()); event.preventDefault(); } " id="instBarcode" name="instBarcode" />' +
										'</li>' +
										'<li>' +
											'<label>&nbsp;</label>' +
											'<input type="button" onclick=" findAdditionalStd($j(\'input#instBarcode\').val()); " value="' + i18n.t("add", "Add") + '" />' +
										'</li>' +
									'</ol>' +
								'</fieldset>' +
								'<table class="default2" id="addstandards" summary="This table displays standards temporarily before they are added to the page">' +
									'<thead>' +
										'<tr>' +
											'<th class="stdadd" scope="col">' + i18n.t("add", "Add") + '</th>' +
											'<th class="stdbar" scope="col">' + i18n.t("barcode", "Barcode") + '</th>' +
											'<th class="stdinst" scope="col">' + i18n.t("instrument", "Instrument") + '</th>' +
											'<th class="stdserial" scope="col">' + i18n.t("serialNo", "Serial No") + '</th>' +
											'<th class="stdplant" scope="col">' + i18n.t("plantNo", "Plant No") + '</th>' +
										'</tr>' +
									'</thead>' +
									'<tfoot>' +
										'<tr>' +
											'<td colspan="5">&nbsp;</td>' +
										'</tr>' +
									'</tfoot>' +
									'<tbody>' +
										'<tr>' +
											'<td colspan="5" class="center bold">' + i18n.t("addStandardsToTable", "Add Standards to table") + '</td>' +
										'</tr>' +
									'</tbody>' +
								'</table>' +
								'<div class="center">' +
									'<input type="button" value="' + i18n.t("addStandards", "Add Standards") + '" onclick=" addAdditionalStds(); " />' +
								'</div>' +
							'</div>';
	
	// return thickbox content
	return thickboxContent;
}

/**					
 * this function finds the instrument using the plantid in the barcode field, checks that it is a valid standard
 * and adds it to the temporary table for viewing before the list is submitted to the page.
 *
 * @param {Integer} barcode plantid provided in the barcode field
 * @param {Boolean} allowuncalibratedstds indicates whether to allow uncalibrated standards
 */
function findAdditionalStd(barcode)
{	
	// check that instrument standard with this barcode is not already listed in the
	// default procedure standards table or the additional standards table
	if($j('table#procedurestds tr#std' + barcode).length == 0 && $j('table#additionalstds tr#addstd' + barcode).length == 0)
	{
		// load the service javascript file if not already	
		loadScript.loadScriptsDynamic(new Array('dwr/interface/instrumservice.js'), true,
		{
			callback: function()
			{
				// call dwr service to find this instrument standard
				instrumservice.findInstrumAsStandard(barcode,
				{
					callback:function(results)
					{	
						// error has occured finding instrument standard
						if(results.success != true)
						{
							// empty the error warning box
							$j('div#additionalStdsTB div.warningBox3').empty();
							// show error message in error warning box
							$j('div#additionalStdsTB div:first').removeClass('hid').addClass('vis').find('.warningBox3').text(results.message);
						}
						else
						{
							// set variable to instrument object
							var inst = results.results;
							// declare display variables
							var checked = ' checked="checked"';
							var outOfCal = '';
							var rowcolor = '';
							// this instrument standard is a standard
							if(inst.calibrationStandard == true)
							{
								// instrument standard is out of calibration
								if(inst.outOfCalibration == true)
								{
									// populate display variable with message
									outOfCal = '<br/><span class="attention">' + i18n.t("outOfCal", "OUT OF CALIBRATION") + '</span>';
									// populate display variable to highlight table row
									rowcolor = 'highlight';
									// check if uncalibrated instrument standards are allowed
									if (allowuncalibratedstds == false) 
									{
										// populate variable to disable the input field instead of check
										//checked = ' disabled="disabled"';
										checked = ' ';
									}
								}
								// thickbox table contains message of no instruments
								if ($j('div.thickbox-box table tbody tr td').length < 5)
								{
									// remove this message before adding a new row
									$j('div.thickbox-box table tbody tr:first').remove();
								}
								// variable to hold calibration indicator for instrument
								var calIndicator = '';
								// can the calibration timescale indicator be shown?
								if ((inst.inCalTimescalePercentage != null) && (inst.finalMonthOfCalTimescalePercentage != null))
								{											
									calIndicator +=	'<span class="relative">' +
														'<div class="calIndicatorBox">';
															// is this instrument out of calibration?
															if ((inst.inCalTimescalePercentage == 100) && (inst.finalMonthOfCalTimescalePercentage == 100))
															{
																calIndicator += '<div class="outOfCalibration"></div>';
															}
															else
															{
																calIndicator += '<div class="inCalibration">' +
																					'<div class="inCalibrationLevel" style=" width: ' + inst.inCalTimescalePercentage + '%; "></div>' +
																				'</div>' +
																				'<div class="finalMonthOfCalibration">' +
																					'<div class="finalMonthOfCalibrationLevel" style=" width: ' + inst.finalMonthOfCalTimescalePercentage + '%; "></div>' +
																				'</div>';	
															}																				
										calIndicator +=	'</div>' +
													'</span>';
								}
								// append new instrument standard to the table
								$j('div.thickbox-box table tbody').append(	'<tr class="' + rowcolor + '">' + 
																				'<td class="stdadd">' +
																					'<input type="checkbox" name="addStd" value="' + inst.plantid + '" ' + checked + ' /> ' + 
																					'<input type="hidden" name="outOfCal" id="outOfCal" value="' + inst.outOfCalibration + '" />' +
																					'<input type="hidden" name="modelid" id="modelid" value="' + inst.model.modelid + '" />' +
																				'</td>' +
																				'<td class="stdbar">' +
																					'<img src="img/icons/bullet-standard.png" width="12" height="12" class="image_inl" alt="' + i18n.t("standard", "Standard") + '" title="' + i18n.t("standard", "Standard") + '" /> ' +
																					calIndicator +
																					'<a href="viewinstrument.htm?plantid=' + inst.plantid + '" class="mainlink">' + inst.plantid + '</a>' +
																				'</td>' +
																				'<td class="stdinst">' + inst.model.mfr.name + ' ' + inst.model.model + ' ' + inst.model.description.description + outOfCal + ' </td>' +
																				'<td class="stdserial">' + inst.serialno + '</td>' +
																				'<td class="stdplant">' + inst.plantno + '</td>' +
																			'</tr>');
							}
						}
					}
				});
			}
		});
	}
	// instrument standard with this barcode is already listed in standards table
	else
	{
		// display message
		$j.prompt(i18n.t("core.jobs:calibration.instrAlreadyOnTheList", {varBarcode : barcode, defaultValue : "Instrument with barcode " + barcode + " is already on the list of standards for this calibration"}));
	}
	// apply focus and clear the barcode input field
	$j('input#instBarcode').val('').focus();
}





/**					
 * this function loops through all the additional procedure standards added to the temporary table in thickbox which have been selected
 * by ticking the checkbox and then adds these procedure standards to the additional procedure standards table on page.
 *
 * @param {Boolean} allowuncalibratedstds indicates whether to allow uncalibrated standards
 */
function addAdditionalStds()
{	
	// loop through each instrument standard that has been added to the thickbox table and is checked
	$j.each($j('table#addstandards tbody tr td input[name="addStd"]:checked'), function(i, n)
	{
		// additional standards table has no contents
		if ($j('table#additionalstds tbody tr').length == 0)
		{
			// display the additional standards table
			$j('table#additionalstds').removeClass().addClass('default2');
		}
		// declare display variables
		var checked = ' checked="checked"';
		var outOfCal = '';
		var rowcolor = '';
		// standard is out of calibration
		if($j(this).next().val() == 'true')
		{
			// populate display variable with message
			outOfCal = '<br/><span class="attention">' + i18n.t("outOfCal", "OUT OF CALIBRATION") + '</span>';
			// populate display variable to highlight table row
			rowcolor = 'highlight';
			// check if uncalibrated instrument standards are allowed
			if (allowuncalibratedstds == false) 
			{
				// populate variable to disable the input field instead of check
				//checked = ' disabled="disabled"';
				checked = ' ';
			}
			// find the original instrument text from thickbox table and remove the 'out of cal' message so it
			// can be used when appended to the table below
			$j(this).parent().siblings(':eq(1)').find('span, br').remove();
		}
		// append new row to the additional standards table
		$j('table#additionalstds tbody').append('<tr id="addstd' + n.value + '" class="' + rowcolor + '">' +
													'<td class="center">' +
														'<input type="checkbox" class="standardId" name="instrums" value="' + n.value + '" ' + checked + ' /> ' + 
														'<input type="hidden" name="addStdsOnScreen" value="' + n.value + '" />' +
													'</td>' +
													'<td class="addstdbar">' +
														$j(this).parent().siblings(':eq(0)').find('a').remove().end().html() +
														'<a href="viewinstrument.htm?plantid=' + n.value + '&amp;ajax=instrument&amp;width=400" class="jconTip mainlink" name="' + i18n.t("instrInformation", "Instrument Information") + '" id="inst' + n.value + '">' + 
															n.value + 
														'</a>' +
														' <img src="img/icons/mouse.png" width="12" height="12" class="image_inl" alt="" title="" />' +
													'</td>' +
													'<td>' +
														'<a href="instrumentmodel.htm?modelid=' + $j(this).next().next().val() + '")" class="mainlink">' +
															 $j(this).parent().siblings(':eq(1)').text() +
														'</a>' +
														outOfCal +
													'</td>' +
													'<td>' + $j(this).parent().siblings(':eq(2)').text() + '</td>' +
													'<td>' + $j(this).parent().siblings(':eq(3)').text() + '</td>' +
												'</tr>');
														
		// add one to the additional procedure standards item count
		$j('span.addstdsSizeSpan').text(parseInt($j('span.addstdsSizeSpan').text()) + 1);		
					
	});
	// initialise tooltip anchor
	JT_init();	
	// close thickbox
	tb_remove();
}

/**
 * this method checks all inputs of type checkbox that are contained within the table 
 * 
 * @param {Boolean} select true if boxes are to be checked and vice versa
 * @param {String} parentElement class of table in which to search for inputs of type checkbox
 */
function selectAllItems(select, parentElement)
{
	// check all
	if(select == true)
	{
		$j('.' + parentElement + ' input:checkbox[name^="cals"]').check('on');
	}
	// uncheck all
	else
	{
		$j('.' + parentElement + ' input:checkbox[name^="cals"]').check('off');
	}
}