/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	CalSearch.js
*	DESCRIPTION		:
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	07/05/2008 - JV - Created File
*
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'jobNo';

function changeStartDateOption(between)
{
	if(between == "false")
	{
		$j('#startDateSpan').removeClass('vis').addClass('hid');
	}
	else
	{
		$j('#startDateSpan').removeClass('hid').addClass('vis');
	}
}

/**
 * array of calendar objects containing the initial setup literals needed
 * @param inputField (String) id of the input field to which the calendar should be binded
 * @param dateFormat (String) format that the date should be displayed
 * @param displayDates (String) 'all' make all dates available
 * 								'future' show only dates in the future
 * 								'past' show only dates in the past
 * applied in init() function.
 */	
//var jqCalendar = new Array (
//				 { 
//				 	inputField: 'startDate1',
//				 	dateFormat: 'dd.mm.yy',
//				 	displayDates: 'all',
//					showWeekends: true
//				 },
//				 { 
//				 	inputField: 'startDate2',
//				 	dateFormat: 'dd.mm.yy',
//				 	displayDates: 'all',
//					showWeekends: true
//				 });

/**
 * Array of autocomplete objects (multiple are possible) containing setup for 
 * jQueryUI initialization.
 * this is used in the init() function.
 * @param procedureSubdivId is initialized in JSP file 
 */
var jquiAutocomplete = new Array(
	{
		inputFieldName: 'procedureReferenceAndName',
		inputFieldId: 'procedureId',
		source: 'searchprocedures.json?subdivId='+procedureSubdivId
	});
