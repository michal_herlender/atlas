import {
  LitElement,
  html,
  css
} from "../../../../thirdparty/lit-element/lit-element.js";
import CwmsTranslate from "../../../../components/cwms-translate/cwms-translate.js";
;

import  "../../../../components/cwms-spinner/cwms-spinner.js";

class VdiCalibrationsImport extends LitElement {
  static get properties() {
    return {
      caption: { type: String },
      warningMessage: { type: String, attribute: false },
      jobs: { type: Array, attribute: false },
      busy: {type:Boolean, attribute:false}
    };
  }

  createRenderRoot() {
    /**
     * Render template without shadow DOM. Note that shadow DOM features like
     * encapsulated CSS and slots are unavailable.
     */
    return this;
  }

  importXml(e) {
    e.preventDefault();
    if (!e.currentTarget.form.checkValidity()) {
      this.warningMessage = CwmsTranslate.getMessage(
        "vdi.noXml",
        "XML file is required"
      );
      return;
    } else this.warningMessage = "";
    const formData = new FormData(e.currentTarget.form);
    this.busy = true;
    const csrfToken = document.querySelector("meta[name='_csrf']").getAttribute("content")
    fetch("vdi/importCalibrations.json",
{
      method: "POST",
      headers: {
          "X-CSRF-TOKEN": csrfToken,
        },
      body: formData
    })
      .then(res =>
        res.ok
          ? res.json()
          : Promise.reject(new Error(`HTTP Error ${res.status}`))
      )
      .then(jobs =>
        jobs && jobs.length > 0
          ? jobs
          : Promise.reject(
              CwmsTranslate.getMessage(
                "vdi.noJobs",
                "No valid job was found in the imported file"
              )
            )
      )
      .then(jobs => {
        this.jobs = jobs;
      })
      .catch(message => (this.warningMessage = message))
      .finally(()=> this.busy=false);
  }

  render() {
    return html`
      <form class="vdi-import">
        <fieldset>
          <legend>${this.caption}</legend>
          <div class="displaycolumn-40">
            <ul>
              <li>
                <label
                  ><cwms-translate code="vdi.xmlInput"
                    >Vdi 2623 input file</cwms-translate
                  ></label
                >
                <input type="file" name="file" accept=".xml" required />
              </li>
              <li>
                <label
                  ><cwms-translate code="vdi.certificates"
                    >Certificats in PDF</cwms-translate
                  ></label
                >
                <input
                  type="file"
                  name="certificates"
                  accept=".pdf"
                  ?multiple="${true}"
                />
              </li>
              <li>
                <button @click="${this.importXml}">
                  <cwms-translate code="vdi.submit">Import file</cwms-translate>
                </button>
              </li>
            </ul>
          </div>
        </fieldset>
        ${this.busy?html`<cwms-spinner></cwms-spinner>`:html``}
        <cwms-vdi-import-result
          .jobs=${this.jobs}
          .showWarning=${this.showWarning}
        ></cwms-vdi-import-result>
        ${this.warningMessage
          ? VdiCalibrationsImportUtils.renderWarningBox(this.warningMessage)
          : ""}
      </form>
    `;
  }
}

customElements.define("cwms-vdi-calibrations-import", VdiCalibrationsImport);

class VdiImportResult extends LitElement {
  static get properties() {
    return { jobs: { type: Array, attribute: false } };
  }

  createRenderRoot() {
    /**
     * Render template without shadow DOM. Note that shadow DOM features like
     * encapsulated CSS and slots are unavailable.
     */
    return this;
  }

  renderJobs() {
    return html`
      <div class="successBox1 successBox2 successBox3">
        ${this.jobs.map(this.renderJob.bind(this))}
      </div>
    `;
  }

  renderJob(jobM) {
    let job = jobM._2;
    return job
      ? html`
          <div class="job">
            <span class="jobId"
              >Job with id: ${this.renderJobLink(job.jobId)}</span
            >
            ${this.renderInstruments(job.instruments)}
          </div>
        `
      : jobM._1
      ? VdiCalibrationsImportUtils.renderWarningBox(jobM._1)
      : html``;
  }

  renderInstrumentLink(jobId) {
    return html`
      <a href="viewinstrument.htm?plantid=${jobId}">${jobId}</a>
    `;
  }

  renderJobLink(jobId) {
    return html`
      <a href="viewjob.htm?jobid=${jobId}">${jobId}</a>
    `;
  }

  renderInstruments(instruments) {
    return instruments && instruments.length > 0
      ? html`
          <div class="instruments">
            ${instruments.map(this.renderInstrument.bind(this))}
          </div>
        `
      : html`<div class="instruments warningBox3">
        <cwms-translate code="vdi.noInstruments">No Instruments</cwms-translate></div>`;
  }

  renderInstrument(instrument) {
    return html`
      <span
        >instrument with id ${this.renderInstrumentLink(instrument.plantId)} and
        with the certificate ${instrument.certificateNumber}</span
      >
    `;
  }

  render() {
    return this.jobs && this.jobs.length > 0 ? this.renderJobs() : ``;
  }
}
customElements.define("cwms-vdi-import-result", VdiImportResult);

class VdiCalibrationsImportUtils {
  static renderWarningBox(warningMessage) {
    return html`
      <div class="warningBox1">
        <div class="warningBox2">
          <div class="warningBox3">
            ${warningMessage}
          </div>
        </div>
      </div>
    `;
  }
}
