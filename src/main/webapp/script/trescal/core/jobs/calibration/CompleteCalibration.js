/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	CompleteCalibration.js
*	DESCRIPTION		:	Page-specific javascript file for completecalibration.vm
*	DEPENDENCIES	:   -
*
*	TO-DO			:
*	KNOWN ISSUES	:	-
*	HISTORY			:	26/11/2007 - JV - Created File
*					:	14/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'script/trescal/core/jobs/jobitem/JobItemFunctions.js',
								'script/trescal/core/tools/labelprinting/dymo/DymoLabelFunctions.js',
								'script/trescal/core/utilities/PreviewCertificate.js');

/**
 * create array of descriptions to be used for password strength function
 */
var desc = new Array();
desc[0] = "Very Weak";
desc[1] = "Weak";
desc[2] = "Better";
desc[3] = "Medium";
desc[4] = "Strong";
desc[5] = "Strongest";

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{
	// select the correct subnavigation link
	$j('#linkcalibration').addClass('selected');
	
	// i18n of descriptions for password strength function
	desc[0] = i18n.t("common:password.veryWeak", "Very Weak");
	desc[1] = i18n.t("common:password.weak", "Weak");
	desc[2] = i18n.t("common:password.better", "Better");
	desc[3] = i18n.t("common:password.medium", "Medium");
	desc[4] = i18n.t("common:password.strong", "Strong");
	desc[5] = i18n.t("common:password.strongest", "Strongest");
}

/**
 * this function retrieves the amount of minutes passed since the calibration was started
 * 
 * @param {Integer} calid id of the calibration to be checked
 */
function getMinutesSinceStart(timeInputId, calid)
{
	// load javascript service file
	loadScript.loadScriptsDynamic(new Array('dwr/interface/calibrationservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to retrieve minutes passed since start of calibration
			calibrationservice.getMinutesFromCalibrationStart(calid,
			{
				callback:function(minutes)
				{
					// change value in input field
					$j('input#' + timeInputId).val(minutes);
				}
			});
		}
	});
}

/**
 * this function checks the strength of the password entered by the user and displays 
 * text and coloured bar dependant on that strength
 * 
 * @param {String} password password entered by the user in input field
 */
function passwordStrength(password)
{
	// load javascript service file
	loadScript.loadScriptsDynamic(new Array('dwr/interface/calibrationservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to check the password strength
			calibrationservice.checkPasswordStrength(password,
			{
				callback:function(score)
				{		
					// change text in div dependant on password strength
					$j('div#passwordDescription').text(desc[score]);
					// change class of coloured div dependant on password strength
		        	$j('div#passwordStrength').removeClass().addClass("strength" + score);
		        	// score should be greater than three before submission allowed
		        	if(score < 3)
		        	{
		        		$j('input#generatecertbutton').prop('disabled', true);
		        	}
		        	else
		        	{
		        		$j('input#generatecertbutton').prop('disabled', false);
		        	}
		        }	
			});
		}
	});
}

function changeOutcome(selectBox)
{
	var theOneBeingChanged = (selectBox != undefined) ? $j(selectBox).find('option:selected').parent('optgroup').attr('class') : null;
	var atLeastOneSuccess = false;
	//var atLeastOneIncomplete = false;
	var allIncomplete = false;
	var othersAreIncomplete = true;
	
	// there can now be multiple select outcome boxes (one per item being calibrated)
	// so check each of the results
	$j('select[name^="linkedOutcomes["]').each(
		function (i, n)
		{
			// get selected option group label for each of these selects
			 var optlabel = $j(n).find('option:selected').parent('optgroup').attr('class');
			 
			 if(optlabel.indexOf('Success') != -1)
			 {
				 atLeastOneSuccess = true;
			 }
//			 else if(optlabel.indexOf('Incomplete') != -1)
//			 {
//				 atLeastOneIncomplete = true;
//			 }
			 
			 if(theOneBeingChanged != null && theOneBeingChanged.indexOf('Incomplete') != -1)
			 {
				 allIncomplete = true;					 
			 }
			 
			 if(selectBox != null && selectBox != n && optlabel.indexOf('Incomplete') == -1)
			 {
				 othersAreIncomplete = false;
			 }
		}
	);
	
	// if there is at least one incomplete outcome
	if(allIncomplete)
	{
		// for all select boxes
		$j('select[name^="linkedOutcomes["]').each(
			function (i, n)
			{
				// change outcome to incomplete
				$j(n).find('optgroup[class="Incomplete"] > option:first').prop('selected', true);
			}
		);	
		
		// hide options for signing and uncheck all signing boxes
		$j('li[id^="signing"], li[id^="password"]').addClass('hid').removeClass('vis').find('input:checkbox').prop('checked',false);
		
		// and rename button
		$j('#completeButton').val(i18n.t("core.jobs:calibration.placeCalOnHold", "Place Calibration On Hold"));
	}
	// else if there is at least one success outcome
	else if(atLeastOneSuccess)
	{
		// provide options for signing
		$j('li[id^="signing"]').addClass('vis').removeClass('hid');
		
		// and rename button
		$j('#completeButton').val(i18n.t("core.jobs:calibration.completeCal", "Complete Calibration"));
		
		if(othersAreIncomplete)
		{
			// for all select boxes
			$j('select[name^="linkedOutcomes["]').each(
				function (i, n)
				{
					// for all selects apart from the one being changed
					if(n != selectBox)
					{
						// change outcome to success
						$j(n).find('optgroup[class^="Success"] > option:first').prop('selected', true);
					}
				}
			);
		}
	}
	// else all outcomes are failures
	else
	{
		
		// and rename button
		$j('#completeButton').val(i18n.t("core.jobs:calibration.completeCal", "Complete Calibration"));
		
		// hide options for signing and uncheck all signing boxes
		$j('li[id^="signing"], li[id^="password"]').addClass('hid').removeClass('vis').find('input:checkbox').prop('checked',false);
		
		if(othersAreIncomplete)
		{
			// for all select boxes
			$j('select[name^="linkedOutcomes["]').each(
				function (i, n)
				{
					// for all selects apart from the one being changed
					if(n != selectBox)
					{
						// change outcome to success
						$j(n).find('optgroup[class^="Success"] > option:first').prop('selected', true);
					}
				}
			);
		}
	}
}

function changeSignNow(checkbox)
{
	if($j(checkbox).is(':checked'))
	{
		$j('li[id^="password"]').addClass('vis').removeClass('hid');
	}
	else
	{
		$j('li[id^="password"]').addClass('hid').removeClass('vis');		
	}
}

/**
 * this method copies the value selected in the element to any others
 * present on the page in the direction specified.
 *
 * @param {String} direction the direction in which the value should be copied
 * @param {String} elementtype the type of element we are copying to
 * @param {Object} element element which we can obtain value and name for copying
 */
function copySelectedOption(direction, elementtype, element)
{
	// add marker after element
	$j(element).after('<span class="marker"></span>');
	// get the input name attribute
	var elementName = $j(element).attr('name');
	// variable to indicate the marker has been found
	var markerFound = false;
	// array to hold all inputs
	var inputArray = new Array();
	// copy information down the list
	if (direction == 'down')
	{				
		// does element name contain brace?
		if (elementName.indexOf('[') != -1)
		{
			// get the start of the element name before square brace
			var elementStart = elementName.substring(0, elementName.indexOf('['));
			// create an array of input objects
			inputArray = $j.makeArray($j(elementtype + '[name^="' + elementStart + '"]'));
		}
		else
		{
			// create an array of input objects
			inputArray = $j.makeArray($j(elementtype + '[name="' + $j(element).attr('name') + '"]'));
		}	
	}
	else
	{
		// does element name contain brace?
		if (elementName.indexOf('[') != -1)
		{
			// get the start of the element name before square brace
			var elementStart = elementName.substring(0, elementName.indexOf('['));
			// create an array of input objects
			inputArray = $j.makeArray($j(elementtype + '[name^="' + elementStart + '"]'));
			// reverse the array
			inputArray.reverse();
		}
		else 
		{
			// create an array of input objects
			inputArray = $j.makeArray($j(elementtype + '[name="' + $j(element).attr('name') + '"]'));
			// reverse the array
			inputArray.reverse();
		}
	}
	
	// loop through all matching inputs
	$j.each(inputArray, function()
	{
		// marker found?
		if ($j(this).next().hasClass('marker'))
		{
			// set variable and remove marker
			markerFound = true;
			$j(this).next().remove();
		}
		// marker found?
		if (markerFound)
		{
			// copy value to input
			$j(this).val($j(element).val());
		}					
	});
	
	// fire this again so that it can update options based on the new outcomes!
	changeOutcome(null);
}

function continueWithCalTool(jobItemId, calProcess) {
	$j.ajax({
		type: 'GET',
		url: "continuecalibrationtool.json",
		data: {
			jobItemId: jobItemId,
			calProcess: calProcess
		}
	}).done(function(toolContinueLink) {
		var oXHR = new XMLHttpRequest();
		oXHR.open("GET", toolContinueLink, true);
		oXHR.onreadystatechange = function (oEvent) {
			if (oXHR.readyState === 4) {
				if (oXHR.status === 200) {
	        		console.log("Local Alligator response OK, status 200");
	        		form.submit();
	        	} else {
	        		console.log("Local Alligator response error, status:");
	        		console.log(oXHR.status);
	        		alert("Local Alligator response error, status:" + oXHR.status);
	        	}
			}
		};
		oXHR.send();
		console.log("Contacted local Alligator");
	});
}