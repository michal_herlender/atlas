import {html, render} from '../../../../thirdparty/lit-html/lit-html.js';
import {repeat} from '../../../../thirdparty/lit-html/directives/repeat.js';

document.querySelector('#missingJobitems').addEventListener('click', createMissingJobitems);

let missingJobItemsErrorTemplate = (data) => html`
	<cwms-translate code="calibrationimportation.missingjobitems.instrumentsalreadyinjob">
		Instruments already exist other jobs
	</cwms-translate>
	<table class="default4" >
		<thead>
			<tr>
				<th><cwms-translate code="jobsheet.plantid">Trescal Id</cwms-translate></th>
				<th><cwms-translate code="instrumentsimport.template.column.optional.plantno">Client no</cwms-translate></th>
				<th><cwms-translate code="completecalibration.serialno">Serial no</cwms-translate></th>
				<th><cwms-translate code="job">Job</cwms-translate></th>
				<th><cwms-translate code="workflow.jobitem">Jobitem no</cwms-translate></th>
			</tr>
		</thead>
		<tbody>
			${repeat(data, (i,index)=> html`
				<tr>
					<td><a href="viewinstrument.htm?plantid=${i.plantId}" target="_blank">${i.plantId}</a></td>
					<td>${i.plantNo}</td>
					<td>${i.serialNo}</td>
					<td><a href="viewjob.htm?jobid=${i.jobId}" target="_blank">${i.jobNo}</a></td>
					<td><a href="jiactions.htm?jobitemid=${i.jobitemId}" target="_blank">${i.jobitemNo}</a></td>
				</tr>
			`)}
		</tbody>
	</table>
`;


function createMissingJobitems(e) {
		
	e.preventDefault();

	var nbOfitems = document.querySelectorAll('tr[instrumentidentified] .selectCheckbox:checked').length;
	if (nbOfitems == 0) {
		$j.prompt("There need to be at least one selected row with an identified instrument");
	} else {
		
		var selectedJobIdValue = $j("#jobSelector").val();
		
		let selectedInstruments = Array.from(document.querySelectorAll('tr[instrumentidentified] .selectCheckbox:checked'))
			.map(d=>d.parentNode.parentNode)
			.filter(d=>d.hasAttribute('plantid'))
			.map(d=>d.getAttribute('plantid'));
		
		const csrfToken = document.querySelector("meta[name='_csrf']").getAttribute("content")
		fetch('activeInstrumentsJsonController.json', {
			headers: {
				"X-CSRF-TOKEN": csrfToken,
				'Accept': 'application/json',
			    'Content-Type': 'application/json'
		    },
		    method: 'post',
		    body: JSON.stringify(selectedInstruments)
		  }).then(res => res.text().then(text => {
			  
			  let data = JSON.parse(text);
			  if(data.length == 0){
				  $j("#importedCalibrationsForm").attr("target", "_blank");
				  $j("#importedCalibrationsForm") .append('<input id="hiddenInput" type="hidden" name="createmissingjobitems" />');
				  $j("#importedCalibrationsForm").submit();
				  $j("#hiddenInput").remove();
				  $j("#importedCalibrationsForm").attr("target", "");
			  }else{
				  let contentContainer = document.querySelector('div[slot="cwms-overlay-body"]');
				  let result = missingJobItemsErrorTemplate(data);
				  render(result,contentContainer);
				  document.querySelector('#missingJobItemsValidation').show();
			  }
			  
		  }));
	}
}
