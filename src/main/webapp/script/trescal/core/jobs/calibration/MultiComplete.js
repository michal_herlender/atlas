/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: Jamie Vinall
*
*	FILENAME		:	MultiComplete.js
*	DESCRIPTION		:	This javascript file is used on the multistart.vm page.
*					: 	
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	09/09/2010 - JV - Created File
*
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/** 
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = 	new Array(	{ anchor: 'ongoing-link', block: 'ongoing-tab' },
								{ anchor: 'onhold-link', block: 'onhold-tab' } );

/**
 * this method checks all inputs of type checkbox that are contained within the table 
 * 
 * @param {Boolean} select true if boxes are to be checked and vice versa
 * @param {String} parentElement class of table in which to search for inputs of type checkbox
 */
function selectAllItems(select, parentElement)
{
	// check all
	if(select == true)
	{
		$j('.' + parentElement + ' input:checkbox[name^="onGoingCals"]').check('on');
	}
	// uncheck all
	else
	{
		$j('.' + parentElement + ' input:checkbox[name^="onGoingCals"]').check('off');
	}
}

/**
 * this method copies the value selected in the element to any others
 * present on the page in the direction specified.
 *
 * @param {String} direction the direction in which the value should be copied
 * @param {String} elementtype the type of element we are copying to
 * @param {Object} element element which we can obtain value and name for copying
 */
function copySelectedOption(direction, elementtype, element)
{
	// add marker after element
	$j(element).after('<span class="marker"></span>');				
	// variable to indicate the marker has been found
	var markerFound = false;
	// array to hold all inputs
	var inputArray = new Array();
	// copy information down the list
	if (direction == 'down')
	{								
		// create an array of input objects
		inputArray = $j.makeArray($j(elementtype + '[name$="' + $j(element).attr('name').substring(($j(element).attr('name').lastIndexOf('.') + 1), $j(element).attr('name').length) + '"]'));					
	}
	else
	{				
		// create an array of input objects
		inputArray = $j.makeArray($j(elementtype + '[name$="' + $j(element).attr('name').substring(($j(element).attr('name').lastIndexOf('.') + 1), $j(element).attr('name').length) + '"]'));
		// reverse the array
		inputArray.reverse();				
	}
	
	// loop through all matching inputs
	$j.each(inputArray, function()
	{
		// marker found?
		if ($j(this).next().hasClass('marker'))
		{
			// set variable and remove marker
			markerFound = true;
			$j(this).next().remove();
		}
		// marker found?
		if (markerFound)
		{
			// copy value to input
			$j(this).val($j(element).val());					
		}					
	});
}