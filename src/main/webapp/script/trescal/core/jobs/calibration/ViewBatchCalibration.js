/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	ViewBatchCalibration.js
*	DESCRIPTION		:	Page-specific javascript file for viewbatchcalibration.vm.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 
*						
*	KNOWN ISSUES	:	-
*	HISTORY			:	01/05/2008 - JV - Created File
*					:	14/01/2015 - TProvost - Manage i18n
*					:	14/10/2015 - TProvost - Fix bug in new version of Impromptu.js - Replace the "callback" property (property disappeared) by the "submit" property
*					:							=> Changes made in function checkItemsToBeInvoiced
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array ( 	'script/thirdparty/jQuery/jquery.boxy.js',
							 	'script/thirdparty/jQuery/jquery.tablednd.js' );
							 
/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * create array of descriptions to be used for password strength function
 */
	var desc = new Array();
	desc[0] = 'Very Weak';
	desc[1] = 'Weak';
	desc[2] = 'Better';
	desc[3] = 'Medium';
	desc[4] = 'Strong';
	desc[5] = 'Strongest';

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{
	// i18n of descriptions for password strength function
	desc[0] = i18n.t("common:password.veryWeak", "Very Weak");
	desc[1] = i18n.t("common:password.weak", "Weak");
	desc[2] = i18n.t("common:password.better", "Better");
	desc[3] = i18n.t("common:password.medium", "Medium");
	desc[4] = i18n.t("common:password.strong", "Strong");
	desc[5] = i18n.t("common:password.strongest", "Strongest");
	
	// load stylesheet using the dom if not already
	loadScript.loadStyle('styles/jQuery/jquery-boxy.css', 'screen');
	// initialise the drag and drop table to enable moving calibrations within the batch
	$j("#batchtable").tableDnD({
		onDragClass: 'dragging',
	    onDrop: function(table, row)
		{
            var rows = table.tBodies[0].rows;
			var rowId = Number(row.id);
			var newPos = null;

			// the following substring method removes the word 'batch' from the ID.
			// the word 'batch' has to be added to the batch ID initially so that 
			// if the batch ID matches a row ID on the table, there will be no jquery
			// confusion when getting back the correct row below. 
			var batchId = table.tBodies[0].id;
			batchId = batchId.substring(5);
			
			// if the new location has previous rows...
			if ($j('tr#' + rowId).prev('tr').length > 0)
			{
				// get the position it has been moved into
				newPos = Number($j('tr#' + rowId).prev('tr').attr('id'));
				
				// The Number() cast here must be used otherwise the numbers 
				// are compared incorrectly in the if condition below
			
				// if the row has been dragged UP...
				if (newPos < rowId)
				{
					// add 1 to the new row position
					newPos++;
				}
			}
			// else it must be the top position (1)
			else
			{
				newPos = 1;
			}
			
			// change the row ids back to incremental order so that the
			// user can move rows again without refreshing the page
			$j.each(rows, function(i, n)
			{	
				n.id = (i + 1);
			});
			// load the service javascript file if not already	
			loadScript.loadScriptsDynamic(new Array('dwr/interface/calibrationservice.js'), true,
			{
				callback: function()
				{
					// call the dwr method to change the positions in the database
					calibrationservice.changeCalibrationPositionInBatch(batchId, rowId, newPos);
				}
			});
	    }
	});
}

/**
 * this method calls a dwr service to re-order the batch by either the instrument serial number or the default
 * which is the job item no as the items would appear on the job.
 * 
 * @param {Boolean} bySerial this indicates whether the calibrations should be ordered by serial no
 * @param {Integer} batchId the id of the batch to be re-ordered
 */
function updateBatchCalibrationSortOrder(bySerial, batchId)
{
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/batchcalibrationservice.js'), true,
	{
		callback: function()
		{	
			// call dwr service to order the batch calibrations
			batchcalibrationservice.orderCalibrationsOnBatch(bySerial, batchId,
			{
				callback:function(result)
				{
					// re-order successful?
					if (result.success)
					{
						// re-load the page so calibrations are shown in new order
						location.reload(true);
					}
					else
					{
						// display error message to the user
						$j.prompt(result.message);
					}
				}
			});
		}
	});			
}

/**
 * this method sets the batch calibration to return to the view batch calibration page
 * after each calibration has completed.
 * 
 * @param {Integer} batchId id of the batch calibration
 * @param {Object} checkbox the checkbox object
 */
function changeChooseNext(batchId, checkbox)
{	
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/batchcalibrationservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to set choose next calibration
			batchcalibrationservice.setChooseNext(batchId, $j(checkbox).is(':checked'));
		}
	});
}

/**
 * this method starts or continues a calibration depending on the status passed in parameters
 * 
 * @param {Integer} calId id of the calibration
 * @param {Integer} userId personid of the user
 * @param {String} redirectLink link to redirect to
 * @param {String} status current status of item
 */
function startCalibration(calId, userId, redirectLink, status)
{
	// status on-going?
	if(status != "On-going")
	{
		// load the service javascript file if not already	
		loadScript.loadScriptsDynamic(new Array('dwr/interface/calibrationservice.js'), true,
		{
			callback: function()
			{
				// call dwr service to start calibration
				calibrationservice.ajaxStartCalibration(calId, userId, 
				{
					callback:function()
					{
						// redirect browser
						window.location.href = redirectLink;
					}	
				});
			}
		});
	}
	else
	{
		// redirect browser
		window.location.href = redirectLink;
	}
}

/**
 * this method creates the content for marking an item as unable to calibrate
 * 
 * @param {Integer} calId id of the calibration to mark as unable to calibrate
 * @param {Integer} personid id of the current contact
 */
function createUnableToCalContent(calId, personid, activityId)
{
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/actionoutcomeservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to get action outcomes for activity (was 'Pre-calibration')
			actionoutcomeservice.getActionOutcomesForActivityId(activityId,
			{
				callback:function(calOutcomes)
				{
					// create content for thickbox
					var content =	'<div id="unableToCalibrateOverlay">' +
										// add warning box to display error messages if the link fails
										'<div class="hid warningBox1">' +
											'<div class="warningBox2">' +
												'<div class="warningBox3">' +
												'</div>' +
											'</div>' +
										'</div>' +
										'<fieldset>' +
											'<ol>' +
												'<li>' +
													'<label>' + i18n.t("reasonLabel", "Reason:") + '</label>' +
													'<select id="selectActionOutcome">';
														$j.each(calOutcomes, function(i)
														{
															content = content + '<option value="' + calOutcomes[i].id + '">' + calOutcomes[i].description + '</option>';
														});
								content = content +	'</select>' +
												'</li>' +
												'<li>' +
													'<label>&nbsp;</label>' +
													'<input type="button" onclick=" this.disabled = true; completeAsUTC(' + calId + ', ' + personid + ', $j(\'#selectActionOutcome\').val()); return false; " value="' + i18n.t("core.jobs:calibration.utc", "UTC") + '" />' +
												'</li>' +								 
											'</ol>' +
										'</fieldset>' +
									'</div>';					
					// create new overlay using content
					loadScript.createOverlay('dynamic', i18n.t("core.jobs:calibration.unableToCalibrate", "Unable To Calibrate"), null, content, null, 20, 680, 'selectActionOutcome');
				}
			});
		}
	});
}

/**
 * this method completes the selected calibration as unable to calibrate.
 * 
 * @param {Integer} calId id of the calibration to mark
 * @param {Integer} completedById id of the contact performing task
 * @param {Integer} actionOutcome id of the outcome selected
 */
function completeAsUTC(calId, completedById, actionOutcome)
{
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/calibrationservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to mark calibration as UTC
			calibrationservice.completeCalibrationAsUTC(calId, completedById, actionOutcome, null, 
			{
				callback:function(result)
				{
					// successful?
					if(result.success)
					{
						// unwrap results
						var cal = result.results;						
						// update calibration status on screen
						$j('td#statusForCal' + cal.id).html(cal.status.name);						
						// remove option for utc
						$j('td#utcForCal' + cal.id).html('');					
						// close the overlay
						loadScript.closeOverlay();
					}
				}
			});
		}
	});
}

function toggleFields(checkbox, fieldId)
{	
	if($j(checkbox).is(':checked'))
	{		
		$j('#' + fieldId).removeClass('hid').addClass('vis');
	}
	else
	{
		$j('#' + fieldId).removeClass('vis').addClass('hid');
	}
}


	
/**
 * this function checks the strength of the password entered by the user and displays 
 * text and coloured bar dependant on that strength
 * 
 * @param {String} password password entered by the user in input field
 */
function passwordStrength(password)
{
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/calibrationservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to check the password strength
			calibrationservice.checkPasswordStrength(password,
			{
				callback:function(score)
				{
					// change text in div dependant on password strength
					$j('div#passwordDescription').text(desc[score]);
					// change class of coloured div dependant on password strength
		        	$j('div#passwordStrength').removeClass().addClass('strength' + score);
		        }	
			});
		}
	});
}

/**
 * this method deletes a batch calibration
 * 
 * @param {Integer} batchId id of the batch calibration to delete
 * @param {String} successPage name of the page to redirect to
 */
function deleteBatchCalibration(batchId, successPage)
{
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/batchcalibrationservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to delete batch calibration
			batchcalibrationservice.deleteBatchCalibrationWithChecks(batchId,
			{
				callback:function(result)
				{
					// batch deleted successfully
					if(result.success)
					{
						// redirect user
						window.location.href = successPage;
					}
					else
					{
						// show user message
						$j.prompt(result.message);
					}
				}
			});
		}
	});
}

/**
 * this method removes a calibration from the batch
 * 
 * @param {Integer} calId id of the calibration to remove
 */
function removeCalibrationFromBatch(calId)
{
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/batchcalibrationservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to remove calibration from the batch
			batchcalibrationservice.deleteCalibrationFromBatch(calId,
			{
				callback:function(result)
				{
					// removal successful?
					if(result.success)
					{
						// reload the view batch screen
						location.reload(true);
					}
					else
					{
						// show user message
						$j.prompt(result.message);
					}
				}
			});
		}
	});
}

function checkCertifyAccreditations(batchId, personId)
{
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/queuedcalibrationservice.js'), true,
	{
		callback: function()
		{
			queuedcalibrationservice.checkAccreditedForBatchCertificates(batchId, personId,
			{
				callback:function(resWrap)
				{
					if(resWrap.success == true)
					{
						$j.prompt(resWrap.message);
					}
					else
					{
						$j.prompt(resWrap.message);
					}
				}
			});
		}
	});
}

function issueCert(calId)
{
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/calibrationservice.js'), true,
	{
		callback: function()
		{
			calibrationservice.publishCertForBatchedCal(calId,
			{
				callback:function(resWrap)
				{
					if(resWrap.success == false)
					{
						$j.prompt(resWrap.message);
					}
					else
					{	
						window.location.reload();
					}
				}
			});
		}
	});
}

function completeCal(calId)
{
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/calibrationservice.js'), true,
	{
		callback: function()
		{
			calibrationservice.completeBatchedCal(calId,
			{
				callback:function(resWrap)
				{
					if(resWrap.success == false)
					{
						$j.prompt(resWrap.message);
					}
					else
					{
						window.location.reload();
					}
				}
			});
		}
	});
}

function checkForWordFile(link, certId)
{
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/certificateservice.js'), true,
	{
		callback: function()
		{
			certificateservice.wordFileExistsInCertificateDirectory(certId,
			{
				callback:function(wordFileExists)
				{
					if(wordFileExists == true)
					{
						$j(link).after('<img src="img/icons/word_tick.png" alt="' + i18n.t("core.jobs:calibration.checkFoWordFile", "Word file for this certificate exists in the directory") + '" title="' + i18n.t("core.jobs:calibration.checkFoWordFile", "Word file for this certificate exists in the directory") + '" />').remove();
					}
				}
			});
		}
	});
}

function checkItemsToBeInvoiced(batchId)
{	
	if($j('input:checkbox[name="invoice"]').is(":checked"))
	{
		var batchIds = [];
		batchIds.push(batchId);
		$j('input[name="selectedBatchIds"]:checked').each
		(
			function(i, n)
			{
				batchIds.push(n.value);
			}
		);
		// load the service javascript file if not already	
		loadScript.loadScriptsDynamic(new Array('dwr/interface/batchcalibrationservice.js'), true,
		{
			callback: function()
			{
				batchcalibrationservice.ajaxCheckItemsToInvoice(batchIds, $j('#invType').val(),
				{
					callback:function(resWrap)
					{
						if(resWrap.success == false)
						{
							$j.prompt(resWrap.message,
							{ 
								submit: confirmcallback,
								buttons: 
								{ 
									Ok: true,
									Cancel: false 
								}, 
								focus: 1 
							});
						}
						else
						{
							submitActionForm();
						}
					}
				});
			}
		});
	}
	else
	{
		submitActionForm();
	}
		
	function confirmcallback(v,m)
	{
    	if (m)
		{
    		submitActionForm();
		}
    	else
    	{
    		return;
    	}
	}
}

function submitActionForm()
{
	$j('form#batchActionForm input:submit').attr('disabled', 'disabled');
	document.batchActionForm.submit();
}


/**
 * this method creates the content for selecting certificates to re-print and then adds it to a new
 * jQuery boxy pop up.
 */
function reprintCertsBoxy()
{
	// load javascript for scrolling
	loadScript.loadScripts(new Array('script/thirdparty/jQuery/jquery.scrollTo.js'));
	// add content that is generated on the page
	var content = 	'<div id="reprintCertsBoxy" class="overflow marg-bot">' +
						'<div>' +
						// add warning box to display error messages if the move fails
						'<div class="hid warningBox1">' +
							'<div class="warningBox2">' +
								'<div class="warningBox3">' +
								'</div>' +
							'</div>' +
						'</div>' +
						$j('div#certsThatCanBeReprinted').html() +							
						'<div class="center">' +
							'<input type="button" value="' + i18n.t("core.jobs:calibration.printCert", "Print Certificates") + '" onclick=" reprintCertificates(this); return false; " />' +
						'</div>' +
						'</div>' +
					'</div>';						
	// create new boxy pop up using content variable
	var reprintCertsBoxy = new Boxy(content, {title: i18n.t("core.jobs:calibration.reprintCert", "Re-print Certificates"), modal: true, unloadOnHide: true});
	// show boxy pop up and adjust size
	reprintCertsBoxy.tween(360, 340);
}

/**
 * this method finds all certificates that have been checked in the reprint certificates boxy pop up
 * and adds them to an array before calling a dwr service to print the certs.
 * 
 * @param {Object} button the button that has been clicked
 */
function reprintCertificates(button)
{
	// disable the button so it cannot be pressed twice
	$j(button).attr('disabled', 'disabled');
	// create an empty array
	var certIds = [];
	// loop through all cert checkboxs that have been selected
	$j('div#reprintCertsBoxy table#reprintCertsTable input[name="selectedCertIds"]:checked').each(function(i)
	{
		// add certid to the array
		certIds.push(this.value);
	});
	// array contains certids?
	if(certIds.length > 0)
	{
		// load the service javascript file if not already	
		loadScript.loadScriptsDynamic(new Array('dwr/interface/queuedcalibrationservice.js'), true,
		{
			callback: function()
			{
				// call dwr service to reprint selected certs
				queuedcalibrationservice.ajaxReprintCertificates(certIds, 
				{
					callback:function()
					{
						// add message to the screen
						$j('div#reprintCertsBoxy').empty().append(	'<div class="successBox1">' +
																		'<div class="successBox2">' +
																			'<div class="successBox3 center-0">' +
																				i18n.t("core.jobs:calibration.certPrintedShortly", "The selected certificates will be printed shortly") +
																			'</div>' +
																		'</div>' +
																	'</div>');
						// get boxy implementation and close
						Boxy.get($j('div#reprintCertsBoxy')).tween(360, 60);
						// remove the message after a few seconds
						setTimeout('Boxy.get($j(\'div#reprintCertsBoxy\')).hide()', 2000);
					}
				});
			}
		});
	}
	else
	{
		
		// show error message in error warning box
		$j('div#reprintCertsBoxy div.warningBox1').removeClass('hid').addClass('vis').find('.warningBox3').empty().text(i18n.t("core.jobs:calibration.notSelectedCert", "You have not selected any certificates to be re-printed"));
		// scroll to top of window
		$j('div#reprintCertsBoxy').scrollTo( { top:0, left:0 }, 800 );
		// enable the button so it can be pressed
		$j(button).attr('disabled', '');
	}
}

/**
 * this method checks all inputs of type checkbox that are contained within the table 
 * 
 * @param {Boolean} select true if boxes are to be checked and vice versa
 * @param {String} parentElement id of table in which to search for inputs of type checkbox
 */
function selectAllItems(select, parentElement)
{
	// check all
	if(select == true)
	{
		$j('#' + parentElement + ' input:checkbox').check('on');
	}
	// uncheck all
	else
	{
		$j('#' + parentElement + ' input:checkbox').check('off');
	}
}
