/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	NewBatchCalibration.js
*	DESCRIPTION		:	Page-specific javascript file for newbatchcalibration.vm.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 
*						
*	KNOWN ISSUES	:	-
*	HISTORY			:	28/04/2008 - JV - Created File
*					:	14/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (
								'dwr/interface/calibrationservice.js',
								'dwr/interface/defaultstandardservice.js',
								'dwr/interface/WorkInstructionService.js');
								

/**
 * create array of descriptions to be used for password strength function
 */
	var desc = new Array();
	desc[0] = 'Very Weak';
	desc[1] = 'Weak';
	desc[2] = 'Better';
	desc[3] = 'Medium';
	desc[4] = 'Strong';
	desc[5] = 'Strongest';

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{
	// i18n of descriptions for password strength function
	desc[0] = i18n.t("common:password.veryWeak", "Very Weak");
	desc[1] = i18n.t("common:password.weak", "Weak");
	desc[2] = i18n.t("common:password.better", "Better");
	desc[3] = i18n.t("common:password.medium", "Medium");
	desc[4] = i18n.t("common:password.strong", "Strong");
	desc[5] = i18n.t("common:password.strongest", "Strongest");
}

/**
 * this method copies the value selected in the element to any others
 * present on the page in the direction specified.
 *
 * @param {String} direction the direction in which the value should be copied
 * @param {String} elementtype the type of element we are copying to
 * @param {Object} element element which we can obtain value and name for copying
 */
function copySelectedOption(direction, elementtype, element)
{
	// add marker after element
	$j(element).after('<span class="marker"></span>');
	// get the input name attribute
	var elementName = $j(element).attr('name');
	// variable to indicate the marker has been found
	var markerFound = false;
	// array to hold all inputs
	var inputArray = new Array();
	// copy information down the list
	if (direction == 'down')
	{				
		// does element name contain full stop?
		if (elementName.indexOf('.') != -1)
		{
			// get the end of the element name after full stop
			var elementEnd = elementName.substring((elementName.lastIndexOf('.') + 1), elementName.length);
			// create an array of input objects
			inputArray = $j.makeArray($j(elementtype + '[name$="' + elementEnd + '"]'));
		}
		else
		{
			// create an array of input objects
			inputArray = $j.makeArray($j(elementtype + '[name="' + $j(element).attr('name') + '"]'));
		}	
	}
	else
	{
		// does element name contain full stop?
		if (elementName.indexOf('.') != -1) 
		{
			// get the end of the element name after full stop
			var elementEnd = elementName.substring((elementName.lastIndexOf('.') + 1), elementName.length);
			// create an array of input objects
			inputArray = $j.makeArray($j(elementtype + '[name$="' + elementEnd + '"]'));
			// reverse the array
			inputArray.reverse();
		}
		else 
		{
			// create an array of input objects
			inputArray = $j.makeArray($j(elementtype + '[name="' + $j(element).attr('name') + '"]'));
			// reverse the array
			inputArray.reverse();
		}
	}
	
	// loop through all matching inputs
	$j.each(inputArray, function()
	{
		// marker found?
		if ($j(this).next().hasClass('marker'))
		{
			// set variable and remove marker
			markerFound = true;
			$j(this).next().remove();
		}
		// marker found?
		if (markerFound)
		{
			// copy value to input
			$j(this).val($j(element).val());
		}					
	});
}

/**
 * this method checks the current users accreditation for the selected procedure and
 * calibration type.
 * 
 * @param {Integer} procId id of the selected procedure
 * @param {Integer} calTypeId id of the selected calibration type
 * @param {Integer} personId id of the current user
 * @param {Object} row the current row in table
 */
function checkAccredited(procId, calTypeId, personId, row)
{
	// id's empty?
	if(calTypeId == "" || procId == "")
	{
		// clear accredited table cell
		$j(row).find('td.accredited').html('');
		// disable and uncheck checkbox
		$j(row).find('td.addtobatch input:checkbox').attr('disabled', 'disabled').attr('checked','');
	}
	else
	{
		// call dwr service to check accreditation level for procedure
		fetch(cwms.urlWithParamsGenerator("capabiltiyAuthorization/isAuthorizedFor.json",{capabilityId:procId,calibrationTypeId:calTypeId,personId}))
		.then(res => res.ok?res.json():Promise.reject(res.statusText))
		.then(accStatus => {			
				// accredited?
				// 20151019 - TProvost : Add test on null value
				if(accStatus != null && accStatus.right)
				{
					// add hidden input field and image to accredited table cell
					$j(row).find('td.accredited').html(
						'<input type="hidden" name="itemCals[' + $j(row).attr('id') + '].accredited" value="true" />' + 
						'<img src="img/icons/greentick.png" alt="' + i18n.t("core.jobs:calibration.accredited", "Accredited") + '" title="' + i18n.t("core.jobs:calibration.accredited", "Accredited") + '" width="16" height="16" />');
					// enable and check checkbox
					$j(row).find('td.addtobatch input:checkbox').attr('disabled', '').attr('checked','checked');
					// check the select all checkbox
					$j('#selectAll').attr('checked', 'checked');
				}
				else
				{
					// add hidden input field and image to accredited table cell
					$j(row).find('td.accredited').html(
						'<input type="hidden" name="itemCals[' + $j(row).attr('id') + '].accredited" value="false" />' + 
						'<img src="img/icons/redcross.png" alt="' + i18n.t("core.jobs:calibration.notAccredited", "Not accredited") + '" title="' + i18n.t("core.jobs:calibration.notAccredited", "Not accredited") + '" width="16" height="16" />');
					// disable and uncheck checkbox
					$j(row).find('td.addtobatch input:checkbox').attr('disabled', 'disabled').attr('checked','');
				}
			}
		);
	}
}

/**
 * this method checks all inputs of type checkbox that are contained within the table 
 * 
 * @param {Boolean} select true if boxes are to be checked and vice versa
 * @param {String} parentElement id of table in which to search for inputs of type checkbox
 */
function selectAllItems(select, parentElement)
{
	// check all
	if(select == true)
	{
		$j('#' + parentElement + ' input:checkbox.calcheck').not(':disabled').check('on');
	}
	// uncheck all
	else
	{
		$j('#' + parentElement + ' input:checkbox.calcheck').not(':disabled').check('off');
	}
}


/**
 * this function checks the strength of the password entered by the user and displays 
 * text and coloured bar dependant on that strength
 * 
 * @param {String} password password entered by the user in input field
 */
function passwordStrength(password)
{
	// call dwr service to check the password strength
	calibrationservice.checkPasswordStrength(password,
	{
		callback:function(score)
		{
			// change text in div dependant on password strength
			$j('div#passwordDescription').text(desc[score]);
			// change class of coloured div dependant on password strength
        	$j('div#passwordStrength').removeClass().addClass('strength' + score);
        }	
	});
}

function showStandardSelect(rowId, button)
{
	$j('div#stdSelect-' + rowId).addClass('vis').removeClass('hid');
	
	$j(button).parent('span').removeClass('vis').addClass('hid').next().removeClass('hid').addClass('vis');
}

function hideStandardSelect(rowId, button)
{
	$j('div#stdSelect-' + rowId).addClass('hid').removeClass('vis');
	
	$j(button).parent('span').removeClass('vis').addClass('hid').prev().removeClass('hid').addClass('vis');
}

function showStandardsForProcedure(rowId, procId)
{
	defaultstandardservice.getStandardsForProcedure(procId, null, 
	{
		callback:function(stds)
		{
			displayNewStds('proc', rowId, stds);
		}
	});
}

function showStandardsForWorkInstruction(rowId, wiId)
{
	defaultstandardservice.getStandardsForWorkInstruction(wiId, null, 
	{
		callback:function(stds)
		{
			displayNewStds('wi', rowId, stds);
		}
	});
}

function displayNewStds(type, rowId, stds)
{
	var i = 0;
	var html = '';
	while(i < stds.length)
	{
		var ooc = '';
		if(stds[i].instrument.outOfCalibration == true)
		{
			ooc = 'color: red;';
		}
		var enforced = '';
		if(stds[i].enforceUse == true)
		{
			enforced = 'font-weight: bold;';
		}
		var instmodel = '';
		// manufacturer required for model?
		if(stds[i].instrument.model.modelMfrType.mfrRequiredForModel == true)
		{
			if(stds[i].instrument.model.mfr.name != null)
			{
				instmodel += stds[i].instrument.model.mfr.name + ' ';
			}
			if(stds[i].instrument.model.model != null)
			{
				instmodel += stds[i].instrument.model.model + ' ';
			}
			if(stds[i].instrument.model.description.description != null)
			{
				instmodel += stds[i].instrument.model.description.description;
			}
		}
		else
		{
			if(stds[i].instrument.mfr.name != null)
			{
				instmodel += stds[i].instrument.mfr.name + ' ';
			}
			if(stds[i].instrument.model.model != null)
			{
				instmodel += stds[i].instrument.model.model + ' ';
			}
			if(stds[i].instrument.model.description.description != null)
			{
				instmodel += stds[i].instrument.model.description.description;
			}
		}

		html = html + 
			'<input name="itemCals[' + rowId + '].standardIds" checked="checked" onclick=" toggleDuplicates(this, ' + rowId + '); " type="checkbox" value="' + stds[i].instrument.plantid + '" />' +
			'&nbsp;<span style=" ' + ooc + ' ' + enforced + ' ">' + stds[i].instrument.plantid + ' : ' + instmodel + '</span><br/>';
		
		i++;
	}
	
	$j('div#' + type + 'Stds-' + rowId).html(html);
	
	if($j('div#stdSelect-' + rowId + ' input:checkbox:checked').length)
	{
		$j('span#stdCount-' + rowId).html($j('div#stdSelect-' + rowId + ' input:checkbox:checked').length);
	}	
}

function toggleDuplicates(checkbox, rowId)
{
	var plantid = $j(checkbox).val();
	var checked = $j(checkbox).attr('checked');
	
	$j('div#stdSelect-' + rowId + ' input:checkbox[value="' + plantid + '"]').attr('checked', checked);
	
	$j('span#stdCount-' + rowId).html($j('div#stdSelect-' + rowId + ' input:checkbox:checked').length);
}

function getCertTemplateForWorkInstruction(rowId, wiId)
{
	WorkInstructionService.findWorkInstruction(wiId,
	{
		callback:function(workInstruction)
		{
			if(workInstruction.defaultTemplate != null)
			{
				$j('select[name="itemCals[' + rowId + '].certTemplate"]').val(workInstruction.defaultTemplate);
			}
		}
	});
}

function changeProcess(selectedProcessName)
{
	if(selectedProcessName == "CWMS")
	{
		$j('tr.cwmsOnly, li.cwmsOnly').removeClass('hid').addClass('vis');
	}
	else
	{
		$j('tr.cwmsOnly, li.cwmsOnly').removeClass('vis').addClass('hid');
	}
}