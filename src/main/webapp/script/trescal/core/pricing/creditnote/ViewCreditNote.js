/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	ViewCreditNote.js
*	DESCRIPTION		:	Page-specific javascript file.
*	DEPENDENCIES	:   -
*
*	TO-DO			:						
*	KNOWN ISSUES	:	-
*	HISTORY			:	09/02/2011 - JV - Created File
*					:	26/10/2015 - TProvost - Minor fix on enable buttons
*					:	26/10/2015 - TProvost - Manage i18n
*					:	26/10/2015 - TProvost - Fix bug in new version of Impromptu.js - Replace the "callback" property (property disappeared) by the "submit" property
*												=> Changes made in function deleteItemFromCreditNote
*					:	10/05/2016 - TProvost - Get translatedTitle of nominalcode
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'script/trescal/core/utilities/DocGenPopup.js',
		'script/trescal/core/tools/FileBrowserPlugin.js');

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' },
								{ element: 'ul.subnavtab a', corner: 'small transparent top' } );

/** 
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = new Array( 	{ anchor: 'items-link', block: 'items-tab' },
								{ anchor: 'cndocs-link', block: 'cndocs-tab' },
								{ anchor: 'actions-link', block: 'actions-tab' },
								{ anchor: 'email-link', block: 'email-tab' } );

function init() {
	$j("#addEditCreditNoteItemOverlay").dialog({
		dialogClass: "dialog-no-close",
		title: "Edit Credit Note Item",
		autoOpen: false,
		modal: true,
		resizable: false,
		height: "auto",
		width: 800,
		buttons: [
			{
                text: i18n.t("common:save", "Save"),
                click: function () {
                	var id = $j("#addEditCreditNoteItemOverlay input#itemId").val();
                	var creditNoteId = $j("#creditNoteId").val();
                	var description = $j("#addEditCreditNoteItemOverlay input#description").val();
                	var nominalId = $j("#addEditCreditNoteItemOverlay select#nominal").val();
                	var nominalCode = $j("#addEditCreditNoteItemOverlay select#nominal option:selected").text();
                	var subdivId = $j("#addEditCreditNoteItemOverlay select#itemSubdiv").val();
                	var subdivName = $j("#addEditCreditNoteItemOverlay select#itemSubdiv option:selected").text();
                	var amount = $j("#addEditCreditNoteItemOverlay input#totalCost").val();
                	$j.ajax({
                		url: "updatecreditnoteitem.json",
                		type: "POST",
                		dataType: "json",
                		contentType: "application/json",
                		data: JSON.stringify({
                			id: id,
                			creditNoteId: creditNoteId,
                			description: description,
                			nominalId: nominalId,
                			subdivId: subdivId,
                			amount: amount
                		}),
                		async: true
                	}).done(function(result) {
                		var data = result.results[0];
                		if(id) {
                			var dataRow = $j("tr#cnItem" + id);
                		} else {
                			var dataRow = $j("#newItemRow").clone();
                			dataRow.attr("id", "cnItem" + data.itemId);
                			dataRow.removeClass("hid");
                			dataRow.find("td.item input").val(data.itemId);
                			dataRow.find("td.item input").after(data.itemNo);
                			if($j("table#creditNoteItems tbody tr[id^='cnItem']").length == 0 && $j("table#creditNoteItems tbody tr").length == 2 )
                				$j("table#creditNoteItems tbody tr:first").remove();
                			$j("table#creditNoteItems tbody tr:last").after(dataRow[0]);
                		}
                		dataRow.find("td.desc").html(description);
                		dataRow.find("td.nominal input").val(nominalId);
                		dataRow.find("td.nominal span.nominalCode").html(nominalCode);
                		dataRow.find("td.subdiv input").val(subdivId);
                		dataRow.find("td.subdiv span.subdivName").html(subdivName);
                		dataRow.find("td.amount input").val(amount);
                		dataRow.find("td.amount span.finalCost").html(new Intl.NumberFormat(i18locale, { minimumFractionDigits: 2}).format(amount));
                		var delLink = document.createElement("a");
                		delLink.setAttribute("href", "#");
                		delLink.setAttribute("onclick", "deleteItemFromCreditNote("+data.itemId+"); return false;");
                		delLink.setAttribute("class", "deleteAnchor limitAccess");
                		dataRow.find("td.del").html(delLink);
                		updatePrices(data);
						$j("#addEditCreditNoteItemOverlay").dialog("close");
                	});
            	}
			},
            {
                text: i18n.t("common:close", "Close"),
                click: function () {
                    $j(this).dialog("close");
                }
            }]
    });
}

function createTaxRow(description, rate, value, currencyCode) {
	var formattedRate = new Intl.NumberFormat(i18locale, { minimumFractionDigits: 2}).format(rate*100);
	var formattedValue = new Intl.NumberFormat(i18locale, { style: 'currency', currency: currencyCode }).format(value);
	var row = '<tr class="nobord">'
		+ '<td class="totalsum cost2pcTotal bold">'
		+ '<cwms-translate code="tax">Tax</cwms-translate> '
		+ description + ' (' + formattedRate + '%):'
		+ '</td>'
		+ '<td class="totalamount cost2pcTotal bold">'
		+ '<span id="creditNoteVatValue">'
		+ formattedValue
		+ '</span>'
		+ '</td>'
		+ '<td class="filler">&nbsp;</td>'
		+ '</tr>';
	return row;
}

/**
 * unlocks the form fields on the edit credit note tab and displays delete links for items and the
 * entire credit note.
 */
function unlockCreditNote() {
	// show any links that have limited access when quote issued
	$j('.limitAccess').removeClass('notvis');
	// hide locked display of item values
	$j('span.lockedDisplay').addClass('hid');
	// update message to success box
	$j('div#lockedmessage').empty().append(
			'<div class="successBox2">' +
				'<div class="successBox3">' +
					i18n.t("core.pricing:creditNote.unlockcreditNote", "Credit note has been temporarily unlocked for editing") +
				'</div>' +
			'</div>').attr('class', 'successBox1');
}

function editCreditNoteItem(link) {
	var datarow = $j(link).parents("tr");
	$j("#addEditCreditNoteItemOverlay #itemId").val($j(datarow).find(".item input").val());
	$j("#addEditCreditNoteItemOverlay #description").val($j(datarow).find(".desc").html());
	$j("#addEditCreditNoteItemOverlay #nominal").val($j(datarow).find(".nominal input").val());
	$j("#addEditCreditNoteItemOverlay #itemSubdiv").val($j(datarow).find(".subdiv input").val());
	$j("#addEditCreditNoteItemOverlay #totalCost").val($j(datarow).find(".amount input").val());
	$j("#addEditCreditNoteItemOverlay").dialog("open");
}

function addCreditNoteItem() {
	$j("#addEditCreditNoteItemOverlay #itemId").val("");
	$j("#addEditCreditNoteItemOverlay #description").val("");
	$j("#addEditCreditNoteItemOverlay #nominal").val($j("#nominal option:first").val());
	$j("#addEditCreditNoteItemOverlay #itemSubdiv").val($j("#allocatedSubdivid").val());
	$j("#addEditCreditNoteItemOverlay #totalCost").val(0);
	$j("#addEditCreditNoteItemOverlay").dialog("open");
}

/**
 * this method resets the add new credit note item form in overlay and re-enables submit button
 * 
 * @param {String} id identifier of overlay content
 */
function resetAddEditCreditNoteItemForm(id)
{
	// clear input fields reset select and enable button
	$j('#' + id + ' #newItemDesc, #' + id + ' #newItemAmount').val('');
	$j('#' + id + ' select#newItemNominal option:first').attr('selected', 'selected');
	$j('#' + id + ' select#newItemSubdiv option:first').attr('selected', 'selected');
	$j('#' + id + ' input[type="button"]').attr('disabled', false);
	// add focus
	$j('#' + id + ' #newItemDesc').focus();
}

/**
 * this method removes a credit note item from the database and then removes it from the page
 * 
 * @param {Integer} itemId id of the credit note item to be removed
 */
function deleteItemFromCreditNote(itemId) {
	// initial prompt for user
	$j.prompt(i18n.t("core.pricing:creditNote.confirmDeleteItem", "Are you sure you wish to delete this credit note item?<p>Click 'OK' to confirm the deletion or 'Cancel' to go back</p>"), {
		buttons: {
			Ok: true,
			Cancel: false
		},
		focus: 1,
		submit: function(message, confirm) {
			if (confirm) {
				$j.ajax({
					url: 'deletecreditnoteitem.json',
					method: 'POST',
					dataType: 'json',
					contentType: "application/json",
					data: JSON.stringify({
						"id": itemId
					})
				}).done(function (creditNote) {
					updatePrices(creditNote);
					// remove item
					$j('table.creditNoteItems tbody tr#cnItem' + itemId).remove();
					// credit note table has no items?
					if ($j("table#creditNoteItems tbody tr[id='newItemRow']").length == 1 && $j('table.creditNoteItems tbody tr').length == 1) {
						// add message row
						$j('table.creditNoteItems tbody  tr[id="newItemRow"]').before('<tr>' +
							'<td colspan="6" class="bold text-center">' + i18n.t("core.pricing:creditNote.noItemsOnThisCreditNote", "There are no items on this credit note") + '</td>' +
							'</tr>');
					}
				}).error(function (error) {
					alert("An error occurs on deleting this credit note item");
				});
			}
		}
	});
}

function updatePrices(creditNote) {
	$j("span#creditNoteTotalCost").html(new Intl.NumberFormat(i18locale, { style: 'currency', currency: creditNote.currencyCode }).format(creditNote.grossTotal));
	if(creditNote.taxes) {
		$j("table#taxtable>tbody").empty();
		creditNote.taxes.forEach(tax =>
			$j("table#taxtable>tbody").append(createTaxRow(tax.description, tax.rate, tax.tax, creditNote.currencyCode))
		);
	}
	else {
		// Shouldn't be necessary in future, but keep it for now to ensure work with old credit notes
		$j("span#creditNoteVatValue").html(new Intl.NumberFormat(i18locale, { style: 'currency', currency: creditNote.currencyCode }).format(creditNote.vatValue));
	}
	$j("span#creditNoteFinalCost").html(new Intl.NumberFormat(i18locale, { style: 'currency', currency: creditNote.currencyCode}).format(creditNote.netTotal));
}

/**
 * this method shows the form to be completed when a credit note is deleted
 * 
 * @param {Integer} crid the id of the credit note to be deleted
 */
function createDeleteCreditNoteContent(crid)
{
	var content = 	'<div class="delcrOverlay">' +
						// add warning box to display error messages if the insertion or update fails
						'<div class="hid warningBox1">' +
							'<div class="warningBox2">' +
								'<div class="warningBox3">' +
								'</div>' +
							'</div>' +
						'</div>' +
						'<fieldset>' +
							'<ol>' +
								'<li>' +
									'<label>' + i18n.t("reasonLabel", "Reason:") + '</label>' +
									'<textarea name="deletereason" id="deletereason" rows="6" class="width70"></textarea>' +
								'</li>' +								
								'<li>' +
									'<label>&nbsp;</label>' +
									'<input type="button" value="' + i18n.t("delete", "Delete") + '" onclick=" deleteCreditNote(' + crid + ', $j(\'#deletereason\').val()); return false;" />' +
								'</li>' +								
							'</ol>' +
						'</fieldset>' +
					'</div>';
										
	return content;
}

/**
 * this method calls a dwr service to delete a credit note
 * 
 * @param {Integer} crid the id of the credit note to be deleted
 * @param {String} reason the reason specified for the deletion of credit note
 */
function deleteCreditNote(crid, reason)
{
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/creditnoteservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to delete the credit note
			creditnoteservice.deleteCreditNote(crid, reason,
			{
				callback: function(results)
				{
					// error occured deleting the credit note
					if(!results.success)
					{
						// empty the error warning box
						$j('div.delinvOverlay div.warningBox3').empty();
						// variable to hold error messages
						var errors = '';
						$j.each(results.errorList, function(i)
						{
							// display error messages
							var error = results.errorList[i];
							errors ='<div>' + i18n.t("fieldError", {varErrField: error.field, varErrMsg:error.defaultMessage, defaultValue : "Field: " + error.field + " failed. Message:" + error.defaultMessage}) + '</div>';
						});
						// show error message in error warning box
						$j('div.delinvOverlay div:first').removeClass('hid').addClass('vis').find('.warningBox3').append(errors);
					}
					else
					{
						// it worked, so just redirect away from this page to the deleted component page
						window.location = springUrl + '/deletedcomponent.htm'; 
					}
				}
			});
		}
	});
}