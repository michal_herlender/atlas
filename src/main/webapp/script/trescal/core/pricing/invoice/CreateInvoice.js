/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	CreateInvoice.js
*	DESCRIPTION		:	Page-specific javascript file.
*	DEPENDENCIES	:   -
*
*	TO-DO			:						
*	KNOWN ISSUES	:	-
*	HISTORY			:	03/10/2008 - SH - Created File
*
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );


function init(){
	$j('.filter').prop('disabled', false);
	$j('#submitButton').prop('disabled', false);
}

function beforeSubmit(button) {
	$j(button).prop('disabled', true);
	var checkedItemsWithIncompletePOs = 0;
	$j('.unsettledPOs').each(function() {
		if($j(this).parents('tr').prev().find('.includeCheck').prop('checked')) checkedItemsWithIncompletePOs++;
	});
	if(checkedItemsWithIncompletePOs > 0) {
		$j('#beforeSubmitDialog').dialog({
			title: "Unsettled Purchase orders",
			open: function() {
				$j(this).html("There are job items on this invoice with unsettled purchase orders. Would you like to approve goods in for all of them?");
			},
			buttons: [
				{
					text: i18n.t("yes", "Yes"),
					click: function() {
						$j('#approveGoodsOnAllPOs').prop('checked', true);
						$j('#createinvoiceform').submit();
						$j('#beforeSubmitDialog').dialog("close");
					}
				},
				{
					text: i18n.t("no", "No"),
					click: function() {
						$j('#approveGoodsOnAllPOs').prop('checked', false);
						$j('#createinvoiceform').submit();
						$j('#beforeSubmitDialog').dialog("close");
					}
				},
				{
					text: i18n.t("cancel", "Cancel"),
					click: function() {
						$j('#beforeSubmitDialog').dialog("close");
					}
				}
			]
		});
		$j(button).prop('disabled', false);
	}
	else $j('#createinvoiceform').submit();
}


