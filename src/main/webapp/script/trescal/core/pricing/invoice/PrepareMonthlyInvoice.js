/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	PrepareMonthlyInvoice.js
*	DESCRIPTION		:	Page-specific javascript file.
*	DEPENDENCIES	:   -
*
*	TO-DO			:						
*	KNOWN ISSUES	:	-
*	HISTORY			:	03/10/2008 - SH - Created File
*					: 	27/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'coname';

/**
 * this variable contains the page name which should be used when creating the search again
 * link in the header section
 */
var searchAgain = 'preparemonthlyinvoice.htm';

/**
 * this method checks all inputs of type checkbox that are contained within the table 
 * 
 * @param {Boolean} select true if boxes are to be checked and vice versa
 * @param {String} parentElement id of table in which to search for inputs of type checkbox
 */
function selectAllItems(select, parentElement)
{
	// check all
	if(select == true)
	{
		$j('#' + parentElement + ' input:checkbox').not(':disabled').check('on');
	}
	// uncheck all
	else
	{
		$j('#' + parentElement + ' input:checkbox').not(':disabled').check('off');
	}
}

/**
 * this method adds a job number field to the form
 * 
 * @param {String} fieldname name of the input field to be added
 * @param (Object) link the link pressed by user to create new job number input
 */
function addJobNoField(fieldname, link){
	
	if($j(link).prev().val().trim()!=='' ){
		// change the link passed to remove the previous job number if required
		$j(link).parent().find('a').remove().end().append(	'<a href="#" onclick=" $j(this).parent().remove(); return false; ">' +
																'<img src="img/icons/delete.png" class="img_marg_bot" width="16" height="16" alt="' + i18n.t("core.pricing:invoice.removeJobNumber", "Remove Job Number") + '" title="' + i18n.t("core.pricing:invoice.removeJobNumber", "Remove Job Number") + '" />' +
															'</a>');
															
		// add new div containing job number input field and link to create another if necessary
		$j('#jobnoslist').append(	'<div class="marg-top-small">' +
										'<input type="text" name="' + fieldname + '" value="" /> ' +
										'<a href="#" onclick=" addJobNoField(\'' + fieldname + '\', this); return false; ">' +
											'<img src="img/icons/add.png" class="img_marg_bot" width="16" height="16" alt="' + i18n.t("core.pricing:invoice.addJobNumber", "Add Job Number") + '" title="' + i18n.t("core.pricing:invoice.addJobNumber", "Add Job Number") + '" />' +
										'</a>' +
									'</div>');
	}else{
		$j.prompt("Required fields!");
	}
}
/**
 * this method changes the invoice source from 'system' to 'external'
 * 
 * @param {String} src indicates which invoice source is required
 */
function changeInvoiceSource(src)
{
	if(src == 'SYSTEM')
	{
		// show system info
		$j('#nonsystem').css("display", "none");
		$j('#system').css("display", "block");
	}
	else
	{
		// show external info
		$j('#nonsystem').css("display", "block");
		$j('#system').css("display", "none");
	}
}

function onAddressChange(subdivId) {
	// update link-contractid instruction web component attribute value
	const element = document.querySelector("cwms-instructions");
	element.linkSubdivid = subdivId;
	element.refresh();
}