/**
 * Filename: PrepareJobItemInvoice.js
 * Author: Galen Beck
 * History: 2016-07-19 Created file
 */


/**
 * Job completed filter 30/03/2020
 * to filter job items completed with job complete/incomplete
 */
function filter(id) {
	
	var jobComplete = $j(".jobCompleteFilter").val();
	var totalji = 0;
	var totalje = 0;
	
	//show all	
	$j("#"+id+" tr[jobcomplete]").each(function() {
		$j(this).show();
		// show the total of job items
		$j(this).find('td.totalji.center')[0].style.display = '';
		$j(this).find('td.jijobcomplete.center')[0].style.display = 'none';
		$j(this).find('td.jijobnocomplete.center')[0].style.display = 'none';
		// show the total of job expenses
		$j(this).find('td.totalje.center')[0].style.display = '';
		$j(this).find('td.jejobcomplete.center')[0].style.display = 'none';
		$j(this).find('td.jejobnocomplete.center')[0].style.display = 'none';
		
		var url_createinvoice = $j(this).find('td.invoicecreate.center a')[0];
		var url_periodiclink = $j(this).find('td.periodiclinking.center a')[0];
		// update the create invoice link (complete = ALL)
		if(url_createinvoice){
			var query_string_createinvoice = url_createinvoice.search;
			var search_params_createinvoice = new URLSearchParams(query_string_createinvoice); 
			// new value of "complete" is set to "ALL"
			search_params_createinvoice.set('complete', 'ALL');
			// change the search property of the main url
			url_createinvoice.search = search_params_createinvoice.toString();
			// the new url string
			var new_url_createinvoice = url_createinvoice.toString();
			$j(this).find('td.invoicecreate.center a')[0].href = new_url_createinvoice;
		}
		// update the periodic link (complete = ALL)
		if(url_periodiclink){
			var query_string_periodiclink = url_periodiclink.search;
			var search_params_periodiclink = new URLSearchParams(query_string_periodiclink); 
			// new value of "complete" is set to "ALL"
			search_params_periodiclink.set('complete', 'ALL');
			// change the search property of the main url
			url_periodiclink.search = search_params_periodiclink.toString();
			// the new url string
			var new_url_periodiclink = url_periodiclink.toString();
			$j(this).find('td.periodiclinking.center a')[0].href = new_url_periodiclink;
		}
		
		totalji = parseInt(totalji) + parseInt($j(this).attr("jitotal"));
		totalje = parseInt(totalje) + parseInt($j(this).attr("jetotal"));
	});
	
	if(jobComplete != "ALL"){
		totalji = 0;
		totalje = 0;
	}
	// filter
	$j("#"+id+" tr[jobcomplete]").each(function(){
		if(jobComplete == "NO"){
			if(parseInt($j(this).find('td.jijobnocomplete.center')[0].innerText) == 0
					&& parseInt($j(this).find('td.jejobnocomplete.center')[0].innerText) == 0){
				$j(this).hide();
			}else{
				// show job items with incomplete job
				$j(this).find('td.totalji.center')[0].style.display = 'none';
				$j(this).find('td.jijobcomplete.center')[0].style.display = 'none';
				$j(this).find('td.jijobnocomplete.center')[0].style.display = '';
				// show job expenses with incomplete job
				$j(this).find('td.totalje.center')[0].style.display = 'none';
				$j(this).find('td.jejobcomplete.center')[0].style.display = 'none';
				$j(this).find('td.jejobnocomplete.center')[0].style.display = '';
			}	
			// 
			var url_createinvoice = $j(this).find('td.invoicecreate.center a')[0];
			var url_periodiclink = $j(this).find('td.periodiclinking.center a')[0];
			// update the create invoice link (complete = NO)
			if(url_createinvoice){
				var query_string_createinvoice = url_createinvoice.search;
				var search_params_createinvoice = new URLSearchParams(query_string_createinvoice); 
				// new value of "complete" is set to "NO"
				search_params_createinvoice.set('complete', 'NO');
				// change the search property of the main url
				url_createinvoice.search = search_params_createinvoice.toString();
				// the new url string
				var new_url_createinvoice = url_createinvoice.toString();
				$j(this).find('td.invoicecreate.center a')[0].href = new_url_createinvoice;
			}
			// update the periodic link (complete = NO)
			if(url_periodiclink){
				var query_string_periodiclink = url_periodiclink.search;
				var search_params_periodiclink = new URLSearchParams(query_string_periodiclink); 
				// new value of "complete" is set to "ALL"
				search_params_periodiclink.set('complete', 'NO');
				// change the search property of the main url
				url_periodiclink.search = search_params_periodiclink.toString();
				// the new url string
				var new_url_periodiclink = url_periodiclink.toString();
				$j(this).find('td.periodiclinking.center a')[0].href = new_url_periodiclink;
			}
			
			totalji = totalji + parseInt($j(this).attr("jiwithnocompletejob"));
			totalje = parseInt(totalje) + parseInt($j(this).attr("jewithnocompletejob"));
		}
		
		if(jobComplete == "YES"){
			// job items
			if(parseInt($j(this).find('td.jijobcomplete.center')[0].innerText) == 0 &&
					parseInt($j(this).find('td.jejobcomplete.center')[0].innerText) == 0){
				$j(this).hide();
			}else{
				// show job items with completed job
				$j(this).find('td.totalji.center')[0].style.display = 'none';
				$j(this).find('td.jijobcomplete.center')[0].style.display = '';
				$j(this).find('td.jijobnocomplete.center')[0].style.display = 'none';
				// show job expenses with completed job
				$j(this).find('td.totalje.center')[0].style.display = 'none';
				$j(this).find('td.jejobcomplete.center')[0].style.display = '';
				$j(this).find('td.jejobnocomplete.center')[0].style.display = 'none';
			}	
	
			var url_createinvoice = $j(this).find('td.invoicecreate.center a')[0];
			var url_periodiclink = $j(this).find('td.periodiclinking.center a')[0];
			// update the create invoice link (complete = YES)
			if(url_createinvoice){
				var query_string_createinvoice = url_createinvoice.search;
				var search_params_createinvoice = new URLSearchParams(query_string_createinvoice); 
				// new value of "complete" is set to "YES"
				search_params_createinvoice.set('complete', 'YES');
				// change the search property of the main url
				url_createinvoice.search = search_params_createinvoice.toString();
				// the new url string
				var new_url_createinvoice = url_createinvoice.toString();
				$j(this).find('td.invoicecreate.center a')[0].href = new_url_createinvoice;
			}
			// update the periodic link (complete = YES)
			if(url_periodiclink){
				var query_string_periodiclink = url_periodiclink.search;
				var search_params_periodiclink = new URLSearchParams(query_string_periodiclink); 
				// new value of "complete" is set to "YES"
				search_params_periodiclink.set('complete', 'YES');
				// change the search property of the main url
				url_periodiclink.search = search_params_periodiclink.toString();
				// the new url string
				var new_url_periodiclink = url_periodiclink.toString();
				$j(this).find('td.periodiclinking.center a')[0].href = new_url_periodiclink;
			}
			
			totalji = totalji + parseInt($j(this).attr("jiwithcompletejob"));
			totalje = parseInt(totalje) + parseInt($j(this).attr("jewithcompletejob"));
		}
    });
	
	// update the total of job items
	document.getElementById('countJobItems').innerHTML = '';
	document.getElementById('countJobItems').innerHTML = parseInt(totalji);
	// update the total of job expenses
	document.getElementById('countServiceItems').innerHTML = '';
	document.getElementById('countServiceItems').innerHTML = parseInt(totalje);
	
}