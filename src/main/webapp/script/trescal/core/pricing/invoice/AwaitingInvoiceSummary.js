
/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (
	'script/thirdparty/jQuery/jquery.boxy.js',
	'script/thirdparty/jQuery/jquery.tablesorter.js'	
);

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{
	// load stylesheet using the dom if not already
	loadScript.loadStyle('styles/jQuery/jquery-boxy.css', 'screen');
	
	//initialize subdiv filter in 'item to invoice' tab
	// Seems no longer defined/used (code remains below, for now)

	// make table columns sortable and apply zebra effect
    $j('table.tablesorter').tablesorter({widgets: ['zebra']});
}


function filter(id) {
	var jobComplete = $j(".jobCompleteFilter").val();
	
	//show all	
	$j("#"+id+" tr[jobcomplete]").each(function(){
		$j(this).show();
	});
	
	//filter
	$j("#"+id+" tr[jobcomplete]").each(function(){
		// criteria
		if(jobComplete == "YES"){
			if($j(this).find('td.jijobcomplete.center')[0].innerText == 0){
				$j(this).hide();
			}
			
		} else if (jobComplete == "NO"){
			if($j(this).find('td.jijobnocomplete.center')[0].innerText == 0){
				$j(this).hide();
			}
		}
    });
}
