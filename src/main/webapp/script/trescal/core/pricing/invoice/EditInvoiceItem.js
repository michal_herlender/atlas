/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	EditInvoiceItem.js
*	DESCRIPTION		:	Page-specific javascript file.
*	DEPENDENCIES	:   -
*
*	TO-DO			:						
*	KNOWN ISSUES	:	-
*	HISTORY			:	08/01/2009 - SH - Created File
*					:	27/10/2015 - TProvost - Minor fix on enable buttons
*					: 	27/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.listbox', corner: 'normal' } );

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */				   
function init() {
	if (invoiceBrokered) {
		// disable all form inputs
		$j('#editinvoiceitemform input, #editinvoiceitemform select, #editinvoiceitemform textarea').attr('disabled', 'disabled');
	}
	enableDisableItemSelectors();
}

/**
 * unlocks the form fields on the edit invoice item page
 */ 
function unlockInvoiceItem()
{
	// enable all input and select form elements on edit invoice item page
	$j('#editinvoiceitemform input, #editinvoiceitemform select, #editinvoiceitemform textarea').attr('disabled', false);
	// update message to success box
	$j('div#lockedmessage').empty().append(	'<div class="successBox2">' +
												'<div class="successBox3">' +
													i18n.t("core.pricing:invoice.unlockInvoiceItem", "Invoice item has been temporarily unlocked for editing") +
												'</div>' +
											'</div>').attr('class', 'successBox1');
}

/**
 * this method switches the invoice item between split costs and a single cost for the item.
 * 
 * @param {String} type the type of cost structure
 */
function switchSplitAndSingleCost(type)
{
	// single cost type?
	if(type == 'single')
	{
		$j('#splitcosts').hide();
		$j('#singlecosttotal').show();
		$j('#multiplecosttotal').hide();
		$j('#singlenominal').show();
	}
	else
	{
		$j('#splitcosts').show();
		$j('#singlecosttotal').hide();
		$j('#multiplecosttotal').show();
		$j('#singlenominal').hide();
	}
}

/**
 * this method shows or hides a cost type section
 * 
 * @param {String} caltype the name of the cost type to display or hide
 * @param {Boolean} show boolean value to indicate whether to show or hide cost type
 */
function switchCostTypeDisplay(costtype, show)
{
	if (show)
	{
		$j('#' + costtype + 'costs').show();
		$j('#' + costtype + 'add').hide();
		$j('#' + costtype + 'remove').show();
		$j('#item\\.' + costtype + 'Cost\\.active').val(true);
	}
	else
	{
		$j('#' + costtype + 'costs').hide();
		$j('#' + costtype + 'add').show();
		$j('#' + costtype + 'remove').hide();
		$j('#item\\.' + costtype + 'Cost\\.active').val(false);
	}	
}

function enableDisableItemSelectors() {
	$j('#jobItemSelector').prop('disabled', $j('#expenseItemSelector').val());
	$j('#expenseItemSelector').prop('disabled', $j('#jobItemSelector').val());
}