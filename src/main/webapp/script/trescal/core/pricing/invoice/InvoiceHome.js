/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	InvoiceHome.js
*	DESCRIPTION		:	Page-specific javascript file.
*	DEPENDENCIES	:   -
*
*	TO-DO			:						
*	KNOWN ISSUES	:	-
*	HISTORY			:	29/09/2008 - SH - Created File
*					: 	27/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (
	'script/thirdparty/jQuery/jquery.boxy.js',
	'script/thirdparty/jQuery/jquery.tablesorter.js'	
);

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' },
								{ element: 'ul.subnavtab a', corner: 'small transparent top' } );
								
/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'invno';			
/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{
	// load stylesheet using the dom if not already
	loadScript.loadStyle('styles/jQuery/jquery-boxy.css', 'screen');
	
	// make table columns sortable and apply zebra effect
    $j('table.tablesorter').tablesorter({widgets: ['zebra']});
}



function includeDeactivatedCompanies(obj) {
	var value='';
	var companyName = $j("#searchinvoiceform .compSearchJQPlugin input[name=comptext]").val();
	// clear company search ready for new deactivated value
	$j('div.compSearchJQPlugin').after(	'<div class="compSearchJQPlugin"></div>').remove();
	if(obj.checked == true){
		value = 'true';
	}
	// change deactivated value in hidden input
	$j('input[name="deactivated"]').val(value);
	// get the company text entered 
	$j('input[name="comptext"]').val(companyName);
	// initialize company search again
	$j('div.compSearchJQPlugin').compSearchJQPlugin({	field: 'coid',
														tabIndex: 1,
														compNamePrefill: companyName,
														compCoroles: 'client,business',
														deactivated : value });
	// apply focus to company input
	$j('div.compSearchJQPlugin input:first').focus();
	// trigger keyup event to reset values in plugin
	$j('input[name="comptext"]').trigger('keyup');
	
}