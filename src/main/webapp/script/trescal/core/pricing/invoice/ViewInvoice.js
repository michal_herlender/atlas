/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	ViewInvoice.js
*	DESCRIPTION		:	Page-specific javascript file.
*	DEPENDENCIES	:   -
*
*	TO-DO			:						
*	KNOWN ISSUES	:	-
*	HISTORY			:	03/10/2008 - SH - Created File
*					:	27/10/2015 - TProvost - Minor fix on enable buttons
*					: 	27/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'script/trescal/core/utilities/DocGenPopup.js',
		'script/trescal/core/tools/FileBrowserPlugin.js');

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' },
								{ element: 'ul.subnavtab a', corner: 'small transparent top' } );

/** 
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = new Array( 	{ anchor: 'items-link', block: 'items-tab' },
 								{ anchor: 'edit-link', block: 'edit-tab' },
 								{ anchor: 'nominals-link', block: 'nominals-tab' },
 								{ anchor: 'generate-link', block: 'generate-tab' },
		  					   	{ anchor: 'files-link', block: 'files-tab' },
		  					   	{ anchor: 'email-link', block: 'email-tab' },
								{ anchor: 'docs-link', block: 'docs-tab'},
								{ anchor: 'actions-link', block: 'actions-tab'}, 
								{ anchor: 'pos-link', block: 'pos-tab'},
								{ anchor: 'delivery-link', block: 'delivery-tab'},
								{ anchor: 'creditnotes-link', block: 'creditnotes-tab'},
								{ anchor: 'periodicLinking-link', block: 'periodicLinking-tab'},
								{ anchor: 'instructions-link', block: 'instructions-tab'});

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */	
function init()
{
	if (invoiceBrokered)
	{
		// disable all form inputs
		$j('.editInvoiceForm input, .editInvoiceForm select').attr('disabled', 'disabled');
	}
}

/**
 * array of calendar objects containing the initial setup literals needed
 * @param inputField (String) id of the input field to which the calendar should be binded
 * @param dateFormat (String) format that the date should be displayed
 * @param displayDates (String) 'all' make all dates available
 * 								'future' show only dates in the future
 * 								'past' show only dates in the past
 * applied in init() function.
 */	
//var jqCalendar = new Array(	{ 
//								inputField: 'invoicedate',
//								dateFormat: 'dd.mm.yy',
//								displayDates: 'all'
//							});

/**
 * unlocks the form fields on the edit invoice tab and displays delete links for items and the
 * entire quotation.
 */
function unlockInvoice()
{
	// enable all input and select form elements in edit quotation tab
	$j('.editInvoiceForm input, .editInvoiceForm select').attr('disabled', false);
	// show any links that have limited access when quote issued
	$j('a.limitAccess').removeClass('notvis');
	// update message to success box
	$j('div#lockedmessage').empty().append(	'<div class="successBox2">' +
												'<div class="successBox3">' +
													i18n.t("core.pricing:invoice.unlockInvoice", "Invoice has been temporarily unlocked for editing") +
												'</div>' +
											'</div>').attr('class', 'successBox1');
}

/**
 * this method creates the content necessary for linking invoice items to job/invoice purchase order numbers. If a job id
 * is passed then only the relevant invoice items are loaded for the job in question and relating to the purchase order number.
 * 
 * @param invId id of the invoice to load invoice items for
 * @param poid id of the purchase order number we want to link invoice items to
 * @param ponumber purchase order number we want to link invoice items to
 * @param type the type of purchase order number
 * @param jobid id of the job to load invoice items for
 * @param jobno number of the job to display
 */
function linkInvoicePOContent(invId, poid, ponumber, type, jobid, jobno)
{
	// variable to hold content
	var content = '';
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/invoiceitemservice.js'), true,
	{
		callback: function()
		{	
			// call dwr service to get all invoice items on invoice or restricted by jobid
			invoiceitemservice.getAllInvoiceItemsOnInvoiceDWR(invId, jobid,
			{
				callback:function(result)
				{
					// fetch invoice items successful
					if (result.success)
					{
						// assign list of items to variable
						var iis = result.results;
						// create content
						content +=	'<div id="linkInvoicePOToItemsOverlay">' +
										'<table class="default4 linkInvoicePOItems" summary="">' +
											'<thead>' +
												'<tr>' +
													'<td colspan="6" class="bold">';
														// job id passed to method?
														if (jobid != null)
														{
															content +=i18n.t("core.pricing:invoice.invoiceItemsAvailableOnJob", {varJobNo : jobno, varNbItems : iis.length, defautlValue : "Invoice Items Available On Job " + jobno + "(" + iis.length + ")"});
														}
														else
														{
															content +=i18n.t("core.pricing:invoice.invoiceItemsAvailable", {varJobNo : jobno, varNbItems : iis.length, defautlValue : "Invoice Items Available (" + iis.length + ")"});
														}
													content +=	'</td>' +
												'</tr>' +
												'<tr>' +
													'<th class="add" scope="col">' +
														'<input type="checkbox" id="selectAll" onclick=" selectAll(this.checked, \'linkInvoicePOItems\'); " /> ' + i18n.t("all", "All") + '<br />' +
													'</th>' +
													'<th class="itemno" scope="col">' + i18n.t("item", "Item") + '</th>' +
													'<th class="desc" scope="col">' + i18n.t("description", "Description") + '</th>' +
													'<th class="inst" scope="col">' + i18n.t("instrument", "Instrument") + '</th>' +
													'<th class="serial" scope="col">' + i18n.t("serialNo", "Serial No") + '</th>' +
													'<th class="plant" scope="col">' + i18n.t("plantNo", "Plant No") + '</th>' +
												'</tr>' +
											'</thead>' +											
											'<tbody>';
												// invoice items returned?
												if (iis.length > 0)
												{
													// add each invoice item to table
													$j.each(iis, function(i, ii)
													{
														content += 	'<tr>' +
																		'<td class="add">';																		
																		if (type == 'jobpo')
																		{
																			// option to add link to any PO/BPO that the item is CURRENTLY linked to on the job
																			if(ii.jobItem)
																			{
																				// item linked to po?
																				var itemLinkedPO = false;
																				// check all job item purchase orders
																				$j.each(ii.jobItem.jobItemPOs, function(i, jipo)
																				{
																					// variable to indicate po already on invoice item
																					var alreadyOnII = false;
																					// po linked to job item?
																					if(jipo.po)
																					{
																						// check each job item po for match with invoice item po
																						$j.each(ii.invItemPOs, function(x, iipo)
																						{
																							if(iipo.jobPO && iipo.jobPO.poId == jipo.po.poId)
																							{
																								// set already on invoice item
																								alreadyOnII = true;
																							}
																						});
																						// only display input for the po number we are linking
																						if (jipo.po.poId == poid)
																						{
																							// set item linked
																							itemLinkedPO = true;
																							// create checkbox html for invoice item
																							content +=	'<input type="checkbox" class="' + ii.id + '" name="invPOSelection"';
																										if (alreadyOnII == true)
																										{
																											content += 	' checked="checked"';
																										}
																								content +=	' />';
																						}																						
																					}																					
																				});
																				// was job item linked to this po?
																				if (!itemLinkedPO)
																				{
																					// po not linked to job item
																					content +=	i18n.t("notLinked", "Not Linked to job item");
																				}
																			}
																		}
																		else if (type == 'jobbpo')
																		{
																			// option to add link to any PO/BPO that the item is CURRENTLY linked to on the job
																			if(ii.jobItem)
																			{
																				// item linked to bpo?
																				var itemLinkedBPO = false;
																				// check all job item purchase orders
																				$j.each(ii.jobItem.jobItemPOs, function(i, jipo)
																				{
																					// variable to indicate po already on invoice item
																					var alreadyOnII = false;
																					// bpo linked to job item?
																					if(jipo.bpo)
																					{
																						// check each job item po for match with invoice item po
																						$j.each(ii.invItemPOs, function(x, iipo)
																						{
																							if(iipo.bpo && iipo.bpo.poId == jipo.bpo.poId)
																							{
																								// set already on invoice item
																								alreadyOnII = true;
																							}
																						});
																						// only display input for the po number we are linking
																						if (jipo.bpo.poId == poid)
																						{
																							// set item linked
																							itemLinkedBPO = true;
																							// create checkbox html for invoice item
																							content +=	'<input type="checkbox" class="' + ii.id + '" name="invPOSelection"';
																										if (alreadyOnII == true)
																										{
																											content += 	' checked="checked"';
																										}
																								content +=	' />';
																						}
																					}																					
																				});
																				// was job item item linked to this bpo?
																				if (!itemLinkedBPO)
																				{
																					// po not linked to job item
																					content +=	i18n.t("notLinked", "Not Linked");
																				}
																			}
																		}
																		else
																		{
																			// invoice item has po's?
																			if (ii.invItemPOs)
																			{
																				// variable to indicate po already on invoice item
																				var alreadyOnII = false;
																				// check each invoice item po
																				$j.each(ii.invItemPOs, function(x, iipo)
																				{																					
																					// invoice item po type
																					if (iipo.invPO)
																					{
																						// po id's match
																						if (iipo.invPO.poId == poid)
																						{
																							alreadyOnII = true;
																						}
																					}																					
																				});
																				content +=	'<input type="checkbox" class="' + ii.id + '" name="invPOSelection"';
																							if (alreadyOnII == true)
																							{
																								content += 	' checked="checked"';
																							}
																					content +=	' />';
																			}
																		}
															content +=	'</td>' +
																		'<td class="itemno">' + ii.itemno + '</td>';
																		// invoice item has job item
																		if (ii.jobItem)
																		{
																			content +=	'<td class="desc">' + i18n.t("core.pricing:invoice.itemOnJob", {varItemNo : ii.jobItem.itemNo, varJobNo : ii.jobItem.job.jobno, defaultvalue :  "Item " + ii.jobItem.itemNo + " on job " + ii.jobItem.job.jobno}) + 
																						'<td class="inst">' + ii.jobItem.inst.definitiveInstrument + '</td>' +
																						'<td class="serial">' + ii.jobItem.inst.serialno + '</td>' +
																						'<td class="plant">' + ii.jobItem.inst.plantno + '</td>';
																		}
																		else
																		{
																			content += 	'<td colspan="2">' + ii.description + '</td>' +
																						'<td class="serial">&nbsp;</td>' +
																						'<td class="plant">&nbsp;</td>';
																		}
														content +=	'</tr>';
													});
												}
												else
												{
													content += 	'<tr>' +
																	'<td colspan="6" class="bold text-center">' + i18n.t("core.pricing:invoice.noInvoiceItems", "No Invoice Items") + '</td>' +
																'</tr>';
												}
								content +=	'</tbody>' +
										'</table>' +
										'<div class="text-center padding">' +
											'<input type="button" value="' + i18n.t("core.pricing:invoice.updateLinks", "Update Links") + '" onclick=" linkInvoicePOToItems(' + poid + ', \'' + ponumber.replace("'","\\'") + '\', \'' + type + '\', ' + jobid + ', ' + invId+ '); return false; " />' +
										'</div>' +
									'</div>';
						// initialise overlay using html content
						loadScript.createOverlay('dynamic', i18n.t("core.pricing:invoice.linkInvoicePO", "Link Invoice PO"), null, content, null, 50, 780, null);
					}
					else
					{
						// display error message to user
						$j.prompt(result.message);
					}
				}
			});
		}
	});
}

/**
 * this method uses an array of selected invoice items and links/un-links them to either a job purchase order, a job blanket purchase
 * order or an invoice purchase order depending on the type passed.
 * 
 * @param poid id of the purchase order to link/un-link invoice items to
 * @param ponumber the purchase order number to link/un-link invoice items to
 * @param type the type of purchase order we are linking (i.e. jobpo, jobbpo or invpo)
 * @param jobid id of the job we are linking/un-linking invoice items for
 */
function linkInvoicePOToItems(poid, ponumber, type, jobid, invId)
{
	// create new array
	var iiarray = [];
	// get all selected checkboxes from the overlay
	$j('div#linkInvoicePOToItemsOverlay table tbody input[name="invPOSelection"]:checked').each(function(i, chbx)
	{
		// add selected checkbox class names to array (invoice item ids)
		iiarray.push($j(this).attr('class'));
	});	
	
	$j.ajax({
		url : "assignitemstopo.json",
		method : "POST",
		dataType : "json",
		data : {
			type : type,
			iiarray : iiarray,
			poid : poid,
			jobid : jobid,
			invId : invId
		},
		async : true
	}).done(function(result) {
		// assign invoice item purchase orders successful?
		if(result.success)
		{
			// refresh the page
			window.location.href = 'viewinvoice.htm?id=' + invId + '&loadtab=pos-tab';
			// hide the overlay
			
			loadScript.closeOverlay();
		}
		else
		{
			// display error message to user
			$j.prompt(result.message);
		}
	});
}

/**
 * this method checks all inputs of type checkbox that are contained within the table 
 * 
 * @param {Boolean} select true if boxes are to be checked and vice versa
 * @param {String} parentElement id of table in which to search for inputs of type checkbox
 */
function selectAll(select, parentElement)
{
	// check all
	if(select)
	{
		$j('.' + parentElement + ' input:checkbox').check('on');
	}
	// uncheck all
	else
	{
		$j('.' + parentElement + ' input:checkbox').check('off');
	}
}

/**
 * this method creates the content to add a new invoice purchase order
 * 
 * @param {Integer} invId id of the invoice to add a purchase order number for
 * @returns {String} html string
 */
function newInvoicePOContent(invId)
{
	// create form to add new invoice purchase order number
	var content = 	'<div id="newInvoicePOOverlay">' +
						'<fieldset>' +
							'<ol>' +
								'<li>' +
									'<label>' + i18n.t("core.pricing:invoice.newInvoicePO", "New Invoice PO:") + '</label>' +
									'<input type="text" name="newInvoicePOValue" id="newInvoicePOValue" value="" />' +
								'</li>' +
								'<li>' +
									'<label>&nbsp;</label>' +
									'<input type="button" value="' + i18n.t("core.pricing:invoice.createInvoicePO", "Create Invoice PO") + '" onclick=" this.disabled = true; addNewInvoicePO(' + invId + ', $j(\'input[name=newInvoicePOValue]\').val()); return false; " />' +
								'</li>' +
							'</ol>' +
						'</fieldset>' +
					'</div>';
	
	return content;
}

/**
 * this method adds a new invoice purchase order number and appends the new content necessary to the page
 * 
 * @param {Integer} invoiceid id of the invoice to add purchase order number for
 * @param {String} poNumber the new purchase order number
 */
function addNewInvoicePO(invoiceid, poNumber)
{
	// variable to hold content
	var content = '';
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/invoiceposervice.js'), true,
	{
		callback: function()
		{	
			// call dwr service to create the purchase order for invoice
			invoiceposervice.ajaxAddInvoicePO(invoiceid, poNumber, 
			{
				callback:function(result)
				{
					// purchase order added successfully?
					if(result.success)
					{
						// create new table row to be appended to page
						content +=	'<tr>' +
										'<td class="ponum">' + result.results.poNumber + '</td>' +
										'<td class="link"><a href="#" class="mainlink" onclick=" linkInvoicePOContent(' + invoiceid + ', ' + result.results.poId + ', \'' + poNumber.replace("'","\\'") + '\', \'invpo\', null, null); return false; ">' + i18n.t("core.pricing:invoice.linkToItems", "Link To Items") + '</a></td>' +
										'<td class="remove"><a href="#" class="deleteAnchor" onclick=" deleteInvoicePO(this, ' + result.results.poId + '); return false; ">&nbsp;</a></td>' +
									'</tr>';
						// check if table has any po rows?
						if ($j('table.invPurchaseOrders tbody tr:first td').length < 2)
						{
							// remove the message row in table
							$j('table.invPurchaseOrders tbody tr:first').remove();
						}
						// append the new invoice purchase order
						$j('table.invPurchaseOrders tbody').append(content);
						// update size of invoice purchase order table
						$j('span.invPOSize').text(parseInt($j('span.invPOSize').text()) + 1);
						// close overlay
						loadScript.closeOverlay();
					}
					else
					{
						// display error message to user
						$j.prompt(result.message);
					}
				}
			});
		}
	});
}

/**
 * this method deletes an invoice purchase order
 * 
 * @param {Object} link the link clicked to delete this invoice purchase order
 * @param {Integer} invPoId id of the invoice purchase order
 */
function deleteInvoicePO(link, invPoId)
{
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/invoiceposervice.js'), true,
	{
		callback: function()
		{
			// call dwr service to delete the invoice purchase order
			invoiceposervice.ajaxDeleteInvoicePO(invPoId,  
			{
				callback:function(result)
				{
					if(!result.success)
					{
						// display error message to user
						$j.prompt(result.message);
					}
					else
					{
						// remove the invoice purchase order from page
						$j(link).closest('tr').remove();
						// check if table has any po rows?
						if ($j('table.invPurchaseOrders tbody tr').length < 1)
						{
							// append the invoice purchase order message
							$j('table.invPurchaseOrders tbody').append(	'<tr>' +
																			'<td colspan="3" class="bold text-center">' + i18n.t("core.pricing:invoice.noInvoicePO", "No Invoice Purchase Orders") + '</td>' +
																		'</tr>');
						}
						// clear all existing purchase order links in table
						$j('table.displayCurrentInvoiceItemLinks tbody tr td div.invpo' + invPoId).remove();
						// update size of invoice purchase order table
						$j('span.invPOSize').text(parseInt($j('span.invPOSize').text()) - 1);
					}
				}
			});
		}
	});
}

/**
 * this method creates the content to add a new invoice delivery
 * 
 * @param {Integer} invId id of the invoice to add a delivery number for
 * @returns {String} html string
 */
function newInvoiceDeliveryContent(invId)
{
	// create form to add new invoice delivery number
	var content = 	'<div id="newInvoiceDelOverlay">' +
						'<fieldset>' +
							'<ol>' +
								'<li>' +
									'<label>' + i18n.t("core.pricing:invoice.newInvoiceDelivery", "New Invoice Delivery:") + '</label>' +
									'<input type="text" name="newInvoiceDelValue" id="newInvoiceDelValue" value="" />' +
								'</li>' +
								'<li>' +
									'<label>&nbsp;</label>' +
									'<input type="button" value="' + i18n.t("core.pricing:invoice.createInvoiceDelivery", "Create Invoice Delivery") + '" onclick=" addNewInvoiceDelivery(' + invId + ', $j(\'input[name=newInvoiceDelValue]\').val()); return false; " />' +
								'</li>' +
							'</ol>' +
						'</fieldset>' +
					'</div>';
	
	return content;
}

/**
 * this method adds a new manual delivery number to the invoice
 * 
 * @param invoiceid id of the invoice we are adding a new manual delivery for
 * @param delNumber the new invoice delivery number
 */
function addNewInvoiceDelivery(invoiceid, delNumber)
{
	// variable to hold content
	var content = '';
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/invoicedeliveryservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to add new invoice delivery
			invoicedeliveryservice.ajaxAddInvoiceDelivery(invoiceid, delNumber, 
			{
				callback:function(result)
				{
					if(result.success)
					{
						$j('#invDelList').append('<li>' + result.results.deliveryNo + '</li>');
						
						// create new table row to be appended to page
						content +=	'<tr>' +
										'<td class="delno">' + result.results.deliveryNo + '</td>' +
										'<td class="remove"><a href="#" class="deleteAnchor" onclick=" deleteInvoiceDelivery(this, ' + result.results.id + '); return false; ">&nbsp;</a></td>' +
									'</tr>';
						// check if table has any delivery rows?
						if ($j('table.invDeliveries tbody tr:first td').length < 2)
						{
							// remove the message row in table
							$j('table.invDeliveries tbody tr:first').remove();
						}
						// append the new invoice delivery
						$j('table.invDeliveries tbody').append(content);
						// update size of invoice delivery table
						$j('span.invDelSize').text(parseInt($j('span.invDelSize').text()) + 1);
						$j('span.invDelSize1').text($j('span.invDelSize').text());
						// close overlay
						loadScript.closeOverlay();
					}
					else
					{						
						// update overlay with error messages
						loadScript.modifyOverlay(null, result.message, null, true, null, null, null);
					}
				}
			});
		}
	});
}

/**
 * this method deletes an invoice delivery number
 * 
 * @param {Object} link the link clicked to delete this invoice delivery number
 * @param {Integer} invDelId id of the invoice delivery number
 */
function deleteInvoiceDelivery(link, invDelId)
{
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/invoicedeliveryservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to add new invoice delivery
			invoicedeliveryservice.ajaxDeleteInvoiceDelivery(invDelId,  
			{
				callback:function(result)
				{
					// deletion completed successfully?
					if(result.success)
					{
						// remove the invoice delivery from page
						$j(link).closest('tr').remove();
						// check if table has any delivery rows?
						if ($j('table.invDeliveries tbody tr').length < 1)
						{
							// append the invoice delivery message
							$j('table.invDeliveries tbody').append(	'<tr>' +
																			'<td colspan="3" class="bold text-center">' + i18n.t("core.pricing:invoice.noInvoiceDeliveries", "No Invoice Deliveries") + '</td>' +
																		'</tr>');
						}
						// update size of invoice deliveries table
						$j('span.invDelSize').text(parseInt($j('span.invDelSize').text()) - 1);
						$j('span.invDelSize1').text($j('span.invDelSize').text());
					}
					else
					{
						$j.prompt(result.message);
					}
				}
			});
		}
	});
}


/**
 * this method shows the form to be completed when an invoice is deleted
 * 
 * @param {Integer} invoiceid the id of the invoice to be deleted
 */
function createDeleteInvoiceContent(invoiceid)
{
	var content = 	'<div class="delinvOverlay">' +
						// add warning box to display error messages if the insertion or update fails
						'<div class="hid warningBox1">' +
							'<div class="warningBox2">' +
								'<div class="warningBox3">' +
								'</div>' +
							'</div>' +
						'</div>' +
						'<fieldset>' +
							'<ol>' +
								'<li>' +
									'<label>' + i18n.t("reasonLabel", "Reason:") + '</label>' +
									'<textarea name="deletereason" id="deletereason" rows="6" class="width70"></textarea>' +
								'</li>' +								
								'<li>' +
									'<label>&nbsp;</label>' +
									'<input type="button" value="' + i18n.t("delete", "Delete") + '" onclick=" deleteInvoice(' + invoiceid + ', $j(\'#deletereason\').val()); return false;" />' +
								'</li>' +								
							'</ol>' +
						'</fieldset>' +
					'</div>';
										
	return content;
}

/**
 * this method calls a dwr service to delete an invoice
 * 
 * @param {Integer} invoiceid the id of the invoice to be deleted
 * @param {String} reason the reason specified for the deletion of invoice
 */
function deleteInvoice(invoiceid, reason)
{
	$j.ajax({
		url : "deleteInvoice.json",
		method : "POST",
		traditional : true,
		dataType : "json",
		data : {
			invoiceid : invoiceid,
			reason : reason
		},
		async : true
	}).done(function(results) {
		// error occured deleting the invoice
		if(!results.success)
		{
			// empty the error warning box
			$j('div.delinvOverlay div.warningBox3').empty();
			// variable to hold error messages
			var errors = '';
			if(results.errorList != null) {
				$j.each(results.errorList, function(i)
				{
					// display error messages
					var error = results.errorList[i];
					errors ='<div>' + i18n.t("fieldError", {varErrField: error.field, varErrMsg:error.defaultMessage, defaultValue : "Field: " + error.field + " failed. Message:" + error.defaultMessage}) + '</div>';
				});
			}
			else if(results.message != null)
				errors ='<div>' + results.message + '</div>';
			// show error message in error warning box
			$j('div.delinvOverlay div:first').removeClass('hid').addClass('vis').find('.warningBox3').append(errors);
		}
		else
		{
			// it worked, so just redirect away from this page to the deleted invoices page
			window.location = springUrl + '/deletedcomponent.htm'; 
		}
	});

}

/**
 * this method deletes an invoice item
 * 
 * @param {Integer} itemid the id of the invoice item
 */  
function deleteItem(itemid)
{
	// delete the invoice item
	$j.ajax({
		url: "deleteinvoiceitem.json",
		type: "POST",
		contentType: "application/json",
		data: JSON.stringify(itemid),
		dataType: "json",
		async: true
	}).done(function(invoice) {
		// remove the deleted item table from the page
		$j('#invitem' + itemid).remove();
		// adjust Invoice item count
		$j('span.invItemSize').text(parseInt($j('span.invItemSize:first').text()) - 1);
		// update total cost value
		$j('span#invTotalCost').empty().append(invoice.totalCost + '');
		// update vat value
		$j('span#invVat').empty().append(invoice.vatValue + '');
		// update final cost value
		$j('span#invFinalCost').empty().append(invoice.finalCost + '');
		// renumber items
		$j('span.itemNumber').each(function(index, element) {
			$j(element).text(index + 1);
		});
	});
}

/**
 * this method toggles the visibility of quote item cost summaries
 * 
 * @param {String} classname the string value of class name
 * @param {String} anchorId the id of the anchor being clicked
 * @param {Integer} itemid the id of the quote item
 */
function toggleCostSummaryDisplay(classname, anchorId, itemid)
{
	// hide the quote item costs?
	if (anchorId.indexOf('hide') != -1)
	{
		// show/hide all anchor?
		if (anchorId.indexOf('all') != -1)
		{
			// hide all cost summary rows
			$j('tr[class^="' + classname + '"]').removeClass('vis').addClass('hid');
			// change anchor id and image on all rows
			$j('td.item a:has(img)').each(function(i)
			{
				// get current id of anchor
				var currentid = $j(this).attr('id');
				// get itemid from anchor id
				var itemno = currentid.substring(4, currentid.length);
				// remove old image, append new image and change id of anchor
				$j(this).empty()
						.attr('id', 'show' + itemno)
						.append('<img src="img/icons/showinfo.png" width="16" height="16" class="img_marg_bot" alt="' + i18n.t("showCostBreakdown", "Show cost breakdown") + '" title="' + i18n.t("showCostBreakdown", "Show cost breakdown") + '" />');
			});
		}
		else
		{
			// hide cost summary
			$j('tr[class^="' + classname + itemid + '"]').removeClass('vis').addClass('hid');
			// remove old image, append new image and change id of anchor
			$j('#' + anchorId).empty()
								.attr('id', 'show' + itemid)
								.append('<img src="img/icons/showinfo.png" width="16" height="16" class="img_marg_bot" alt="' + i18n.t("showCostBreakdown", "Show cost breakdown") + '" title="' + i18n.t("showCostBreakdown", "Show cost breakdown") + '" />');
		}
		
	}
	else
	{
		// show/hide all anchor?
		if (anchorId.indexOf('all') != -1) 
		{
			// show all cost summary rows, but not notes
			$j('tr[class^="' + classname + '"]:not([id^="tpqinote"])').removeClass('hid').addClass('vis');
			// change anchor id and image on all rows
			$j('td.item a:has(img)').each(function(i)
			{
				// get current id of anchor
				var currentid = $j(this).attr('id');
				// get itemid from anchor id
				var itemno = currentid.substring(4, currentid.length);
				// remove old image, append new image and change id of anchor
				$j(this).empty()
						.attr('id', 'hide' + itemno)
						.append('<img src="img/icons/hideinfo.png" width="16" height="16" class="img_marg_bot" alt="' + i18n.t("hideCostBreakdown", "Hide cost breakdown") + '" title="' + i18n.t("hideCostBreakdown", "Hide cost breakdown") + '" />');
			});
		}
		else
		{
			// show cost summary
			$j('tr[class^="' + classname + itemid + '"]:not([id^="tpqinote"])').removeClass('hid').addClass('vis');
			// remove old image, append new image and change id of anchor
			$j('#' + anchorId).empty()
								.attr('id', 'hide' + itemid)
								.append('<img src="img/icons/hideinfo.png" width="16" height="16" class="img_marg_bot" alt="' + i18n.t("hideCostBreakdown", "Hide cost breakdown") + '" title="' + i18n.t("hideCostBreakdown", "Hide cost breakdown") + '" />');
		}		
	}	
}

function resetInvoiceStatus(id, accountSoftwareName)
{
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/invoiceservice.js'), true,
	{
		callback: function()
		{
			invoiceservice.resetInvoiceAccountStatus(id,
			{
				callback:function(results)
				{
					if(results.success != true)
					{
						$j.prompt(i18n.t("core.pricing:errorResetStatus", "Error updating status, please contact an administrator"));
					}
					else
					{
						$j('#accountstatus').empty().append('<img src="img/icons/sync_pending.png" width="14" height="14" alt="' + i18n.t('core.pricing:pendingSynchro', 'Pending synchronisation') + '" title="' + i18n.t('core.pricing:pendingSynchro', 'Pending synchronisation') + '" class="image_inl" />' +
								' ' + i18n.t('core.pricing:pendingSynchroWithSoftware', {varSoftware : accountSoftwareName, defaultValue :  'Pending synchronisation with ' + accountSoftwareName}));
					}
				}
			});
		}
	});
}

function deleteInvoiceJobLink(id, anchor)
{
	// no id means that this joblink hasn't been persisted yet, 
	// so it is safe to just delete the field from the page
	if(id == '')
	{
		$j(anchor).parent().remove();
	}
	else
	{
		// load the service javascript file if not already	
		loadScript.loadScriptsDynamic(new Array('dwr/interface/invoicejoblinkservice.js'), true,
		{
			callback: function()
			{
				invoicejoblinkservice.ajaxDeleteJobLink(id,
				{
					callback:function(results)
					{
						if(results.success != true)
						{
							alert(results.message);
						}
						else
						{
							$j('#invoicejoblinkspan'+id).remove();
							$j('#invoicejoblink'+id).remove();
						}
					}
				});
			}
		});
	}
}

function getInvoiceJobLinks(invoiceid)
{
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/invoicejoblinkservice.js'), true,
	{
		callback: function()
		{
			invoicejoblinkservice.findForInvoice(invoiceid,
			{
				callback:function(results)
				{
					$j('#jobnoslist').empty();
					for(var i = 0; i < results.length; i++)
					{
						var joblink = results[i];
						$j('#jobnoslist').append('<div class="marg-top-small" id="invoicejoblink'+joblink.id+'">'+
								'<input type="text" id="joblink'+joblink.id+'jobno" value="'+joblink.jobno+'"/>'+
								'<input type="hidden" id="joblink'+joblink.id+'id" value="'+joblink.id+'"/>'+
								'<a href="#" onclick="deleteInvoiceJobLink('+joblink.id+', this); return false;"><img src="img/icons/delete.png"/></a>'+
							'</div>');
					}
					$j('#invoicejoblinkediting').show(); 
					
					// hide the edit link
					$j('#editjobnoslink').hide();
				}
			});
		}
	});
}

function addNewJobNoField()
{
	$j('#jobnoslist').append('<div class="marg-top-small" id="newinvoicejoblink">'+
			'<input type="text" value=""/>'+
			'<input type="hidden" id="joblinkid" value=""/>'+ 
			'<a href="#" onclick="deleteInvoiceJobLink(\'\', this); return false;"><img src="img/icons/delete.png"/></a>'+
		'</div>');
}

function saveJobNos(invoiceid)
{
	var invoiceJobLinks = new Array();
	
	// build a list of the current jobnos being edited
	$j('div#jobnoslist > div').each(function(i)
	{
		var jobNo = $j(this).find('input:first').val(); 
		var invoiceJobLinkId = $j(this).find('input:last').val();
		invoiceJobLinks.push(new InvoiceJobLinkAjaxDto(invoiceJobLinkId, invoiceid, jobNo));
	});
	// invoice job links?
	if(invoiceJobLinks.length > 0)
	{
		// load the service javascript file if not already	
		loadScript.loadScriptsDynamic(new Array('dwr/interface/invoicejoblinkservice.js', 'script/trescal/core/utilities/TooltipLinks.js'), true,
		{
			callback: function()
			{
				// call dwr service to update job links
				invoicejoblinkservice.ajaxSaveOrUpdateJobLinks(invoiceJobLinks,
				{
					callback:function(results)
					{
						if(!results.success)
						{
							var errors = '';
							$j.each(results.errorList, function(i)
							{
								// display error messages
								var error = results.errorList[i];
								errors ='<div>' + i18n.t("fieldError", {varErrField: error.field, varErrMsg:error.defaultMessage, defaultValue : "Field: " + error.field + " failed. Message:" + error.defaultMessage}) + '</div>';
							});
							if(errors != '')
							{
								$j.prompt(errors);
							}
							else if(results.message != '')
							{
								$j.prompt(results.message);
							}
						}
						else
						{
							// hide editing
							$j('#invoicejoblinkediting').hide(); 
							// show the edit link
							$j('#editjobnoslink').show();							
							// update the list of jobnos displayed on the page
							var content = '';
							$j(results.results).each(function(i, jl)
							{
								// create new content
								content += '<span id="invoicejoblinkspan' + jl.id + '">';
								// is this a system job?
								if(jl.systemJob == true)
								{
									content += 	jobnoLinkDWRInfo(jl.job.jobid, jl.jobno, i, true);
								}
								else
								{
									content += jl.jobno;
								}
								
								if((i+1) != results.results.length)
								{
									content+= ', ';
								}
								content += '</span>';
								if ((i + 1) % 3 == 0)
								{
									content +=	'<br />';
								}
							});
							// add new job links
							$j('#currentjoblinks').empty().append(content);
							// initialise tooltips
							JT_init();
						}
					}
				});
			}
		});
	}
}

function InvoiceJobLinkAjaxDto(id, invoiceId, jobNo)
{
	this.id = id;
	this.invoiceId = invoiceId;
	this.jobNo = jobNo;
}

function genInvoiceDocPopup(id, url, message)
{
	// loading image and message content
	var content = 	'<div class="text-center padding10">' +
						'<img src="img/icons/ajax-loader.gif" width="31" height="31" alt="" />' +
						'<br /> <h4>' + message + '</h4>' +
					'</div>';
	
	var summarized = $j('#rSummaryInvoice').is(':checked');
	var printDiscount = $j('#chkDiscount').is(':checked');
	var printUnitaryPrice = $j('#chkUnitaryPrice').is(':checked');
	var printQuantity = $j('#chkQuantity').is(':checked');
	var showAppendix = $j('#chkShowAppendix').is(':checked');
	var description = $j('#txtInvoiceDescription').val();
	var languageTag = $j('#languageTag').val();
	
	// create new overlay
	loadScript.createOverlay('dynamic', null, null, content, null, 18, 500, null);
	// load document generation page
	window.location.href = '' + url + '' + id + '&summarized=' + summarized + '&description=' + encodeURI(description) + '&printDiscount=' + printDiscount + 
		'&printUnitaryPrice=' + printUnitaryPrice + '&printQuantity=' + printQuantity + '&showAppendix=' + showAppendix + '&languageTag=' + languageTag;
}

function initInvoiceType()
{
	if ($j('#hidDetailedInvoiceType').val() == 'true')
	{
		if (!$j('#rDetailedInvoice').is(':checked')) $j('#rDetailedInvoice').prop('checked', true ); 
		if ($j('#rSummaryInvoice').is(':checked')) $("#rSummaryInvoice").removeAttr('checked');
	}
	else
	{
		if (!$j('#rSummaryInvoice').is(':checked')) $j('#rSummaryInvoice').prop('checked', true ); 
		if ($j('#rDetailedInvoice').is(':checked')) $('#rDetailedInvoice').removeAttr('checked');
	}
	
	manageInvoiceTypeFields($j('#hidDetailedInvoiceType').val());
}

function manageInvoiceTypeFields(detailed)
{
	if ($j('#hidIsCalibration').val() == 'true')
	{
		$j('#invoiceOptions').show();
		
		if (detailed == 'true' || detailed == true)
		{
			$j('#summarizedFields').hide();
			$j('#detailedFields').show();
		}
		else
		{
			$j('#summarizedFields').show();
			$j('#detailedFields').hide();
		}
	}
	else
	{
		$j('#invoiceOptions').hide();
	}
	
	if ($j('#hidPrintDiscount').val() == 'true')
	{
		if (!$j('#chkDiscount').is(':checked')) $j('#chkDiscount').prop('checked', true); 
	}
	else
	{
		if ($j('#chkDiscount').is(':checked')) $("#chkDiscount").removeAttr('checked');
	}

	if ($j('#hidPrintUnitaryPrice').val() == 'true')
	{
		if (!$j('#chkUnitaryPrice').is(':checked')) $j('#chkUnitaryPrice').prop('checked', true); 
	}
	else
	{
		if ($j('#chkUnitaryPrice').is(':checked')) $('#chkUnitaryPrice').removeAttr('checked');
	}
	
	if ($j('#hidPrintQuantity').val() == 'true')
	{
		if (!$j('#chkQuantity').is(':checked')) $j('#chkQuantity').prop('checked', true); 
	}
	else
	{
		if ($j('#chkQuantity').is(':checked')) $('#chkQuantity').removeAttr('checked');
	}
	if ($j('#hidShowAppendix').val() == 'true')
	{
		if (!$j('#chkShowAppendix').is(':checked')) $j('#chkShowAppendix').prop('checked', true); 
	}
	else
	{
		if ($j('#chkShowAppendix').is(':checked')) $j('#chkShowAppendix').removeAttr('checked');
	}
}
