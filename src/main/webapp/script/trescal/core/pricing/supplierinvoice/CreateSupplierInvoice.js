/**
 * Filename : trescal/core/pricing/supplierinvoice/CreateSupplierInvoice.js
 */

/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = "invoiceNumber";

/**
 * array of calendar objects containing the initial setup literals needed
 * 
 * @param {String} inputField is the id of the input field to which the calendar should be binded
 * @param {String} dateFormat is the format that the date should be displayed
 * @param {String} displayDates 'all' make all dates available
 * 								'future' show only dates in the future
 * 								'past' show only dates in the past
 * applied in init() function
 */

