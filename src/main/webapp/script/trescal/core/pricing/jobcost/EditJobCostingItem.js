/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	EditJobCostingItem.js
*	DESCRIPTION		:	Page specific javascript file.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	28/05/2008 - SH - Created File.
*					:	27/10/2015 - TProvost - Minor fix on enable buttons
*					: 	27/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'dwr/interface/tpquotationitemservice.js' );

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/** 
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = 	new Array( 	{ anchor: 'calibrationcosts-link', block: 'calibrationcosts-tab' },
								{ anchor: 'repaircosts-link', block: 'repaircosts-tab' },
								{ anchor: 'adjustmentcosts-link', block: 'adjustmentcosts-tab' },
								{ anchor: 'purchasecosts-link', block: 'purchasecosts-tab' } );

function toggleCalCostError(display)
{
	// display cal cost error
	if (display)
	{
		// show error and disable submit button
		$j('span#manualCalCostError').removeClass('hid');
		// disable all submit buttons and show error message
		$j('input[type="submit"]').each(function()
		{
			$j(this).attr('disabled', 'disabled').prev().removeClass('hid');
		});	
	}
	else
	{
		// hide error and enable submit button
		$j('span#manualCalCostError').addClass('hid');
		// enable all submit buttons and hide error message
		$j('input[type="submit"]').each(function()
		{
			$j(this).attr('disabled', false).prev().addClass('hid');
		});
	}
}

function calculateNewInspectionCost(calcost)
{
	// toggle error message off
	toggleCalCostError(false);
	// check calcost
	if(isNaN(calcost) || calcost == '')
	{
		toggleCalCostError(true);
	}
	else
	{
		// load the service javascript file if not already
		loadScript.loadScriptsDynamic(new Array('dwr/interface/jobcostinginspectioncostservice.js'), true,
		{
			callback: function()
			{
				// call dwr service to insert the new purchase order 
				jobcostinginspectioncostservice.calculateAjaxInspectionCost(calcost,
				{	
					callback: function(result)
					{
						if (result.success)
						{
							// new inspection cost based on calibration cost
							var inspection = result.results.toFixed(2);
							// update inspection cost on page
							$j('input#jciInspectionCost').val(inspection);
						}
					}
				});
			}
		});
	}
}

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{
	/**
	 * check all cost types for errors. if an error message has been displayed
	 * within any of the cost types then switch the focus to that tab. if multiple errors
	 * exist then the first tab working from left to right on the page will be focused on.
	 */
	if ($j('span.calibrationcosts-error').length > 0)
	{
		switchMenuFocus(menuElements, 'calibrationcosts-tab', false);
	}
	else if ($j('span.repaircosts-error').length > 0)
	{
		switchMenuFocus(menuElements, 'repaircosts-tab', false);
	}
	else if ($j('span.adjustmentcosts-error').length > 0)
	{
		switchMenuFocus(menuElements, 'adjustmentcosts-tab', false);
	}
	else if ($j('span.purchasecosts-error').length > 0)
	{
		switchMenuFocus(menuElements, 'purchasecosts-tab', false);
	}
}

/**
 * updates the display of a particular cost and also updates a hidden element which indicates if this
 * cost is to be included in the total cost.
 * 
 * @param {String} costType the name of the cost type being activated / deactivated.
 * @param {Object} link DOM reference to the link being clicked on to call this function.
 */
function toggleIncludeCost(costType, link)
{
	if($j('#item\\.'+costType+'Cost\\.active').val() == 'true')
	{
		$j('#' + costType + 'Main').removeClass().addClass('hid');
		$j('#' + costType + 'Message').removeClass().addClass('vis');
		$j('#item\\.' + costType + 'Cost\\.active').val(false);
		$j(link).text(i18n.t("add", "Add"));	
	}
	else
	{
		$j('#' + costType + 'Main').removeClass().addClass('vis');
		$j('#' + costType + 'Message').removeClass().addClass('hid');
		$j('#item\\.' + costType + 'Cost\\.active').val(true);
		$j(link).text(i18n.t("remove", "Remove"));
	}
}


/**
* Toggles between manual and system derived markup rate selection.
* @param markupSource
*/
function toggleRepMarkUpDisplay(markupSource)
{
	if(markupSource == 'SYSTEM_DERIVED')
	{
		$j('#fixedrepairmarkup').removeClass().addClass('vis');
		$j('#editablerepairmarkup').removeClass().addClass('hid');
	}
	else
	{
		$j('#fixedrepairmarkup').removeClass().addClass('hid');
		$j('#editablerepairmarkup').removeClass().addClass('vis');
	}
}

/**
* Shows and hides fields based on the selected TP Cost Source for the indicated cost type.<b> 
* @param costSource the ThirdCostSource (see ThirdCostSource enum)
* @param costname the lowercase cost name.
*/
function toggleTPCostSrc(costSource, costname)
{
	if(costSource == 'THIRD_PARTY_QUOTATION')
	{
		$j('#linked' + costname + 'cost').removeClass().addClass('vis');
		$j('#manual' + costname + 'cost').removeClass().addClass('hid');
	}
	else
	{
		$j('#linked' + costname + 'cost').removeClass().addClass('hid');
		$j('#manual' + costname + 'cost').removeClass().addClass('vis');
	}
}

function toggleTPMarkupSrc(markupSource, costname)
{
	if(markupSource == 'SYSTEM_DEFAULT')
	{
		$j('#' + costname + 'manualmarkup').removeClass().addClass('hid');
		$j('#' + costname + 'systemmarkup').removeClass().addClass('vis');
	}
	else
	{
		$j('#' + costname + 'manualmarkup').removeClass().addClass('vis');
		$j('#' + costname + 'systemmarkup').removeClass().addClass('hid');
	}
}

/**
* Toggles the display of third party costs for the given costtype to on.<b> 
* @param costname the lowercase name of the cost. 
*/
function displayTPCosts(costname)
{
	$j('#' + costname + 'TP').removeClass().addClass('vis');
}

/**
 * this method opens a thickbox window and calls @see searchTPCostsToLink() method which retrieves and displays
 * third party costs which can be linked.
 * 
 * @param {Integer} costtypeid id of the cost type 
 * @param {Integer} entityid id of the job costing
 * @param {Integer} modelid id of the instrument model
 * @param {String} defaultCurrencySym currency symbol
 * @param {String} entityCurrencyCode job costing currency code
 * @param {String} defaultCurrencyCode default currency code
 */
function displayTPCostSearch(costtypeid, entityid, modelid, defaultCurrencySym, entityCurrencyCode, defaultCurrencyCode)
{
	// display thickbox window and add content from method @see searchTPCostsToLink()
	tb_show(i18n.t("core.pricing:jobCost.linkTPCost", "Link Third Party Cost"), 'TB_dom?width=800&height=240', '', searchTPCostsToLink(costtypeid, entityid, modelid, defaultCurrencySym, entityCurrencyCode, defaultCurrencyCode));
}

/**
 * this method uses a dwr service to retrieve any third party costs which can be linked to the job costing item.
 * 
 * @param {Integer} costtypeid id of the cost type 
 * @param {Integer} entityid id of the job costing
 * @param {Integer} modelid id of the instrument model
 * @param {String} defaultCurrencySym currency symbol
 * @param {String} entityCurrencyCode job costing currency code
 * @param {String} defaultCurrencyCode default currency code
 */
function searchTPCostsToLink(costtypeid, entityid, modelid, defaultCurrencySym, entityCurrencyCode, defaultCurrencyCode)
{
	// call dwr service to retrieve all third party quotation costs which can be linked
	tpquotationitemservice.getMatchingItems(costtypeid, modelid,
	{
		callback: function(TPCosts)
		{			
			// empty any previous content from the thickbox
			$j('#TB_ajaxContent').empty();
			// create table to display third party quotation costs
			var thickboxContent =	// add warning box to display error messages if the link fails
									'<div class="hid warningBox1">' +
										'<div class="warningBox2">' +
											'<div class="warningBox3">' +
											'</div>' +
										'</div>' +
									'</div>' +
									'<table id="TPCostslinktable" class="default2" border="0" summary="">' +
										'<thead>' +
											'<tr>' +
												'<td colspan="8"></td>' +
											'</tr>' +
											'<tr>' +
												'<th class="TPCcomp" scope="col">' + i18n.t("company", "Company") + '</th>' +
												'<th class="TPCcont" scope="col">' + i18n.t("contact", "Contact") + '</th>' +
												'<th class="TPCqno" scope="col">' + i18n.t("quoteNo", "Quote No") + '</th>' +
												'<th class="TPCitem" scope="col">' + i18n.t("item", "Item") + '</th>' +
												'<th class="TPCcost" scope="col">' + i18n.t("finalCost", "Final Cost") + '</th>' +
												'<th class="TPClink" scope="col">' + i18n.t("link", "Link") + '</th>' +											
											'</tr>' +
										'</thead>' +
										'<tfoot>' +
											'<tr>' +
												'<td colspan="6">&nbsp;</td>' +
											'</tr>' +
										'</tfoot>' +
										'<tbody>' +
										'</tbody>' +
									'</table>';
									
			// append content to thickbox
			$j('#TB_ajaxContent').append(thickboxContent);
		
			// third party quotation costs returned
			if(TPCosts.length > 0)
			{
				// append title and number of results
				$j('table#TPCostslinktable thead tr:first td').append(i18n.t("core.pricing:jobCost.TPcosts", "Third Party Costs") + ' (' + TPCosts.length + ')');
				// add each third party quotation cost to table row
				$j.each(TPCosts, function(x)
				{
					// set item object
					var item = TPCosts[x];
					
					$j.each(item.costs, function(i){	
					
						// set cost object
						var cost = item.costs[i];
						
						if(cost.costType)
						{
							// cost active and matches the type we require (i.e. calibration, repair etc)
							if(cost.costType.active == true && cost.costType.typeid == costtypeid)
							{							
								// add each third party quotation cost to table
								$j('table#TPCostslinktable tbody').append(	'<tr id="TPC' + i + '">' +
																				'<td>' + item.tpquotation.contact.sub.comp.coname + '</td>' +
																				'<td>' + item.tpquotation.contact.name + '</td>' +
																				'<td>' + item.tpquotation.qno + '</td>' +																																							
																				'<td>' + item.itemno + '</td>' +
																				'<td>' + item.tpquotation.currency.currencyERSymbol + item.finalCost.toFixed(2) + '</td>' +
																				'<td>' +
																					'<a href="#" class="mainlink">' + i18n.t("core.pricing:jobCost.linkToCosting", "link to costing") + '</a>' +
																				'</td>' +
																			'</tr>');
								
								// add click event to the anchor in row appended above			
								$j('table#TPCostslinktable tbody tr:last td:last a').click(function(){ setTPQuotationAsCostSource(item, cost, entityid, defaultCurrencySym, entityCurrencyCode, defaultCurrencyCode); return false; });
							}
						}
					});
				});
			}
			else
			{
				// add row to inform of no results
				$j('table#TPCostslinktable tbody').append(	'<tr>' +
																'<td colspan="8" class="bold center">' +
																	i18n.t("core.pricing:jobCost.noTPCostsFound", "No third party costs found") +
																'</td>' +
															'</tr>');
			}			
			// apply zebra effect to table
			$j('table#TPCostslinktable').zebra();
		}
	});	
}

/**
 * sets the given cost from the given third party quotation item as the cost source for the given cost.
 * 
 * @param {Object} tpquoteitem third party quote item matched
 * @param {Object} cost cost object
 * @param {Integer} entityid id of the job costing
 * @param {String} defaultCurrencySym currency symbol
 * @param {String} entityCurrencyCode job costing currency code
 * @param {String} defaultCurrencyCode default currency code
 */
function setTPQuotationAsCostSource(tpquoteitem, cost, entityid, defaultCurrencySym, entityCurrencyCode, defaultCurrencyCode)
{
	// create content to be appended to third party quotation cost field
	var content = 	'<label>' + i18n.t("core.pricing:jobCost.tpQuotationCost", "TP Quotation Cost:") + '</label>' +
					'<div class="symbolbox">' + tpquoteitem.tpquotation.currency.currencyERSymbol + '</div>' +
					'<div class="valuebox">' +
						'<span class="valuetext">' +
							cost.finalCost.toFixed(2) +
						'</span>' +
						'<span class="symbolbox2">';
							if (defaultCurrencyCode != entityCurrencyCode)
							{
								content = content + '(<a href="?entityid=' + entityid + '&amp;value=' + cost.finalCost + '&amp;ajax=currency&amp;width=200" onclick="return(false);" class="jconTip mainlink" style=" background: none; padding: 0; " name="' + i18n.t("core.pricing:convertAmountToCurrencySym", {varCurrencySym : defaultCurrencySym, defaultValue :  "Convert this amount to " + defaultCurrencySym}) + '" id="" tabindex="-1">' + defaultCurrencySym + '</a>) ';
							}
					content += i18n.t("core.pricing:jobCost.tpQuotation", {varQuoteNo :  '<a href="' + springUrl + '/viewtpquote.htm?id=' + tpquoteitem.tpquotation.id + '" class="mainlink">' + tpquoteitem.tpquotation.qno + '</a>', varItemNo : tpquoteitem.itemno, varContactName : tpquoteitem.tpquotation.contact.sub.comp.coname,  defaultValue : 'TP Quotation <a href="' + springUrl + '/viewtpquote.htm?id=' + tpquoteitem.tpquotation.id + '" class="mainlink">' + tpquoteitem.tpquotation.qno + '</a> item ' + tpquoteitem.itemno + ' from ' + tpquoteitem.tpquotation.contact.sub.comp.coname + '&nbsp;'}) +
							' <a href="#" class="mainlink" onclick=" removeTPCost(' + cost.costType.typeid + ', ' + cost.costType.name + ', ' + entityid + ', ' + tpquoteitem.model.modelid + ', \'' + defaultCurrencySym + '\', \'' + entityCurrencyCode + '\', \'' + defaultCurrencyCode + '\'); return false; " >' + i18n.t("remove", "Remove") + '</a> |' +
							' <a href="#" class="mainlink domthickbox" onclick=" displayTPCostSearch(' + cost.costType.typeid + ', ' + entityid + ', ' + tpquoteitem.model.modelid + ', \'' + defaultCurrencySym + '\', \'' + entityCurrencyCode + '\', \'' + defaultCurrencyCode + '\'); return false; " >' + i18n.t("core.pricing:jobCost.findTPCosts", "Find TP Costs") + '</a>' +
						'</span>' +
					'</div>' +
					'<input type="hidden" name="' + cost.costType.name.toLowerCase() + 'CostId" value="' + cost.costid + '" />' +
					//<!-- clear floats and restore page flow -->
					'<div class="clear"></div>';
	// empty third party quotation cost list item and append new content
	$j(('#linked' + cost.costType.name + 'cost').toLowerCase()).empty().append(content);
	// close the thickbox
	tb_remove();
}

/**
 * removes the currently linked third party quotation cost for the given costtype
 * 
 * @param {Integer} costtypeid id of the cost type
 * @param {String} costtypename name of the cost type
 * @param {Integer} entityid id of the job costing
 * @param {Integer} modelid id of the instrument model
 * @param {String} defaultCurrencySym currency symbol
 * @param {String} entityCurrencyCode job costing currency code
 * @param {String} defaultCurrencyCode default currency code
 */
function removeTPCost(costtypeid, costtypename, entityid, modelid, defaultCurrencySym, entityCurrencyCode, defaultCurrencyCode)
{
	// create original content for third party quotation cost field
	var content = 	i18n.t("core.pricing:jobCost.noTPQuotationCalCostLinked", "No third party quotation calibration cost linked") + ' ' +
					'<a href="#" class="mainlink domthickbox" onclick=" displayTPCostSearch(' + costtypeid + ', ' + entityid + ', ' + modelid + ', \'' + defaultCurrencySym + '\', \'' + entityCurrencyCode + '\', \'' + defaultCurrencyCode + '\'); return false; " >' + i18n.t("core.pricing:jobCost.findTPCosts", "Find TP Costs") + '</a>' +
					'<input type="hidden" name="' + costtypename + 'CostId" value="" />';
	// empty third party quotation cost list item and append original content
	$j(('#linked' + costtypename + 'cost').toLowerCase()).empty().append(content);	
}