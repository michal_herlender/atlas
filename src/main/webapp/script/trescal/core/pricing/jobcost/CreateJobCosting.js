/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	CreateJobCosting.js
*	DESCRIPTION		:	Page specific javascript file.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	22/05/2008 - SH - Created File.
*					:	27/10/2015 - TProvost - Minor fix on enable buttons
*					: 	27/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * this method checks all inputs of type checkbox that are contained within the parentDiv parameter 
 * 
 * @param {Boolean} select true if boxes are to be checked and vice versa
 * @param {String} parentElement id of div in which to search for inputs of type checkbox
 */
function selectAllItems(select, parentElement)
{
	// check all
	if(select == true)
	{
		$j('#' + parentElement + ' input:checkbox').check('on');
	}
	// uncheck all
	else
	{
		$j('#' + parentElement + ' input:checkbox').check('off');
	}
}

// this string variable contains the cascade search plugin html used in method
// @see toggleContactSelection()
// var set on the init function for the i18n
var contactCascadeContent = '';

// this array holds original page html for company and contact list items
// used in method @see toggleContactSelection()
var contactSelectContent = [];

/**
 * this method toggles the contact selection elements depending on whether the user
 * has chosen a 'CLIENT' or 'WARRANTY' costing. 
 * 
 * @param {Object} selectElement select element from which the user chooses costing type
 */				
function toggleContactSelection(selectElement)
{
	// selected value 'WARRANTY'?
	if (selectElement.value == 'WARRANTY')
	{
		// is cascading search already present in list item?
		if ($j(selectElement).parent().prev().find('div#cascadeSearchPlugin').length)
		{
			// do nothing	
		}
		else
		{
			// save original company content to array
			contactSelectContent.push($j(selectElement).parent().prev().prev().html());
			// save original contact content to array
			contactSelectContent.push($j(selectElement).parent().prev().html());
			// remove the company list item
			$j(selectElement).parent().prev().prev().remove();
			// empty the contact list item and add new cascade content
			$j(selectElement).parent().prev().empty().html(contactCascadeContent);
			// call init() method to initialise cascade search plugin
			loadScript.init(true);
		}		
	}
	else
	{
		// is client select element already present in list item?
		if ($j(selectElement).parent().prev().find('select').length)
		{
			// do nothing
		}
		else
		{
			// append company list item back to page and add original content
			$j(selectElement).parent().prev().before('<li></li>').prev().html(contactSelectContent[0]);
			// empty the contact list item and append original contact select element
			$j(selectElement).parent().prev().empty().html(contactSelectContent[1]);
		}
	}
}

function addAdditionalContacts()
{
	// close the contact list
	var contacts = $j('#additionalPersonids').clone().attr('id', '');
	$j('#additionalcontactlist').append('<br/>').append(contacts);
}

function init()
{
	contactCascadeContent = '<label>' + i18n.t("core.pricing:jobCost.companyOrContact", "Company/Contact:") + '</label>' +
	'<div id="cascadeSearchPlugin">' +
		'<input type="hidden" id="compCoroles" value="client" />' +
		'<input type="hidden" id="cascadeRules" value="subdiv,contact" />' +
	'</div>' +
	'<div class="clear"></div>';
}
