/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Richard Dean
*
*	FILENAME		:	ViewJobCosting.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	*						
*	KNOWN ISSUES	:	-
*	HISTORY			:	03/03/2008 - SH - Created File
*					: 	27/10/2015 - TProvost - Manage i18n				
*
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'script/trescal/core/utilities/DocGenPopup.js',
		'script/trescal/core/tools/FileBrowserPlugin.js');

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' },
								{ element: 'ul.subnavtab a', corner: 'small transparent top' } );

/** 
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
 var menuElements = new Array( 	{ anchor: 'costingitems-link', block: 'costingitems-tab' },
 								{ anchor: 'editcosting-link', block: 'editcosting-tab' },
		  					   	{ anchor: 'costingfiles-link', block: 'costingfiles-tab' },
		  					   	{ anchor: 'email-link', block: 'email-tab' },
		  					   	{ anchor: 'clientdecision-link', block: 'clientdecision-tab' },
 								{ anchor: 'instruction-link', block: 'instruction-tab' });


 function init(){
		// load stylesheet using the dom
		loadScript.loadStyle('styles/jQuery/jquery.datetimepicker.css', 'screen');
		// javascript file for creating wysiwyg editor
		loadScript.loadScriptsDynamic(new Array('script/thirdparty/jQuery/jquery.datetimepicker.full.min.js'), true,
		{
			callback: function()
			{
				$j('#clientApprovalOn').datetimepicker({
					format:'d.m.Y - H:i',
					step: 15,
					minTime:'4:59',
					maxDate: $j.now(),
					validateOnBlur:false,
					defaultSelect:false,
					onSelectDate:function(ct,$i){
						var selectedDate = ct, currentDate = new Date();
						if(selectedDate.setHours(0,0,0,0) === currentDate.setHours(0,0,0,0))
							$j('#clientApprovalOn').datetimepicker({ maxTime : $j.now() });
						else
							$j('#clientApprovalOn').datetimepicker({ maxTime : '22:01' });
						}
				});
			}
		});
	}
 
/**
 * this function deletes the job costing from the database
 * 
 * @param {Integer} costingid is the id of the job costing to be deleted
 */
function deleteCosting(costingid)
{
	if(confirm(i18n.t("core.pricing:jobCost.confirmDeleteCosting", "Please note: this does not delete any documents produced for the costing, but the ENTIRE costing from the database so please do NOT continue if this costing version has been issued to the client. Click 'OK' to confirm the deletion or 'Cancel' to go back...")))
	{
		window.location.href = 'deletejobcosting.htm?id=' + costingid;
	}
}

/**
 * this method sets the status of the job costing to issued and updates the page accordingly
 * 
 * @param anchor the update status anchor clicked by user
 * @param costingId the id of the job costing to be set to issued
 */
function issueCosting(anchor, costingId)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/jobcostingservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to issue the costing
			jobcostingservice.ajaxIssueJobCosting(costingId,
			{
				callback: function(results)
				{
					if (results.success == true) 
					{
						// set costing object
						var costing = results.results;
						// update status name
						$j(anchor).parent().prev().empty().append(costing.status.name);
						// remove anchor to issue costing
						$j(anchor).parent().remove();
						location.reload();
					}
					else 
					{
						// display error message
						$j.prompt(results.message);
					}
				}
			});
		}
	});
}

function reopenCosting(anchor, costingId)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/jobcostingservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to issue the costing
			jobcostingservice.ajaxReopenJobCosting(costingId,
			{
				callback: function(results)
				{
					if (results.success == true) 
					{
						// set costing object
						var costing = results.results;
						// update status name
						$j(anchor).parent().prev().empty().append(costing.status.name);
						// remove anchor to issue costing
						$j(anchor).parent().remove();
						location.reload();
					}
					else 
					{
						// display error message
						$j.prompt(results.message);
					}
				}
			});
		}
	});
}

/**
 * this method deletes an item from the job costing and updates the final cost, vat and net cost values.
 * 
 * @param {Integer} itemid id of the job costing item to be deleted
 */		  					   	
function deleteItem(itemid)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/jobcostingitemservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to delete the job costing item
			jobcostingitemservice.ajaxDeleteJobCostingItem(itemid,
			{
				callback: function(results)
				{			
					if (results.success == true) 
					{
						// remove the deleted item from the page
						$j('table#jci' + itemid).remove();
						// set costing object
						var costing = results.results;
						// update total cost value
						$j('span#jciTotalCost').empty().append(costing.totalCost.toFixed(2));
						// update vat value
						$j('span#jciVatCost').empty().append(costing.vatValue.toFixed(2));
						// update final cost value
						$j('span#jciFinalCost').empty().append(costing.finalCost.toFixed(2));
					}
					else 
					{
						// display error message
						$j.prompt(results.message);
					}
				}
			});
		}
	});
}

function deleteExpenseItem(expenseItemId) {
	$j.ajax({
		url: "deletejobcostingexpenseitem.json",
		data: {
			expenseItemId: expenseItemId
		},
		async: true
	}).done(function(result) {
		// remove the deleted item from the page
		$j('table#jcei' + expenseItemId).remove();
		// update total cost value
		$j('span#jciTotalCost').empty().append(result.totalCost);
		// update vat value
		$j('span#jciVatCost').empty().append(result.vatValue);
		// update final cost value
		$j('span#jciFinalCost').empty().append(result.finalCost);
	});
}

/**
 * this method toggles the visibility of quote item cost summaries
 * 
 * @param {String} classname the string value of class name
 * @param {String} anchorId the id of the anchor being clicked
 * @param {Integer} itemid the id of the quote item
 */
function toggleCostSummaryDisplay(classname, anchorId, itemid)
{
	// hide the quote item costs?
	if (anchorId.indexOf('hide') != -1)
	{
		// show/hide all anchor?
		if (anchorId.indexOf('all') != -1)
		{
			// hide all cost summary rows
			$j('tr[class^="' + classname + '"]').removeClass('vis').addClass('hid');
			// change anchor id and image on all rows
			$j('td.item a:has(img)').each(function(i)
			{
				// get current id of anchor
				var currentid = $j(this).attr('id');
				// get itemid from anchor id
				var itemno = currentid.substring(4, currentid.length);
				// remove old image, append new image and change id of anchor
				$j(this).empty()
						.attr('id', 'show' + itemno)
						.append('<img src="img/icons/showinfo.png" width="16" height="16" class="img_marg_bot" alt="' + i18n.t("showCostBreakdown", "Show cost breakdown") + '" title="' + i18n.t("showCostBreakdown", "Show cost breakdown") + '" />');
			});
		}
		else
		{
			// hide cost summary
			$j('tr[class^="' + classname + itemid + '"]').removeClass('vis').addClass('hid');
			// remove old image, append new image and change id of anchor
			$j('#' + anchorId).empty()
								.attr('id', 'show' + itemid)
								.append('<img src="img/icons/showinfo.png" width="16" height="16" class="img_marg_bot" alt="' + i18n.t("showCostBreakdown", "Show cost breakdown") + '" title="' + i18n.t("showCostBreakdown", "Show cost breakdown") + '" />');
		}
		
	}
	else
	{
		// show/hide all anchor?
		if (anchorId.indexOf('all') != -1) 
		{
			// show all cost summary rows, but not notes
			$j('tr[class^="' + classname + '"]:not([id^="tpqinote"])').removeClass('hid').addClass('vis');
			// change anchor id and image on all rows
			$j('td.item a:has(img)').each(function(i)
			{
				// get current id of anchor
				var currentid = $j(this).attr('id');
				// get itemid from anchor id
				var itemno = currentid.substring(4, currentid.length);
				// remove old image, append new image and change id of anchor
				$j(this).empty()
						.attr('id', 'hide' + itemno)
						.append('<img src="img/icons/hideinfo.png" width="16" height="16" class="img_marg_bot" alt="' + i18n.t("hideCostBreakdown", "Hide cost breakdown") + '" title="' + i18n.t("hideCostBreakdown", "Hide cost breakdown") + '" />');
			});
		}
		else
		{
			// show cost summary
			$j('tr[class^="' + classname + itemid + '"]:not([id^="tpqinote"])').removeClass('hid').addClass('vis');
			// remove old image, append new image and change id of anchor
			$j('#' + anchorId).empty()
								.attr('id', 'hide' + itemid)
								.append('<img src="img/icons/hideinfo.png" width="16" height="16" class="img_marg_bot" alt="' + i18n.t("hideCostBreakdown", "Hide cost breakdown") + '" title="' + i18n.t("hideCostBreakdown", "Hide cost breakdown") + '" />');
		}		
	}	
}

function addAdditionalContacts(anchor)
{
	// clone select
	$j('select[name="additionalPersonids"]:first').clone().appendTo('#additionalcontactlist');
	// space
	$j('#additionalcontactlist').append(' ');
	// clone anchor
	$j(anchor).clone().appendTo('#additionalcontactlist');
	// break
	$j('#additionalcontactlist').append('<br />');
	// remove anchor
	$j(anchor).remove();
}

function displayAvailableFileToAdveso(jcid, jcversion, jobid)
{
	$j.ajax({
		url: "jobcostingfiles.json",
		data: {
			id : jcid,
			jobcostingversion : jcversion
		}
	}).done(function(result) {
		// remove the deleted item from the page
		console.log(result);
	
	var files = result.results;
	var content = 	'';
	if (files.length > 0)
	{
		// create list content
		content = 	'<div id="jobfiles-tab" class="vis"><div class="filebrowserbox" style="padding-top: 0px; padding-bottom: 0px;">'
			+'<div class="filebrowserinner" style=" width: 90%; "><ul class="fileTree">';
							$j.each(files, function(i)
							{
								// check file is not thumbs.db
								if (files[i].fileName != 'Thumbs.db')
								{
									var encodedFilePath = encodeURI(files[i].filePath);
									var encodedFileName = encodeURI(files[i].fileName);
									// is this a directory?
									content += '<li class="file ext_' + files[i].extension + '"><a href="downloadfile.htm?file=' + files[i].filePathEncrypted + '" class="underline" target="_blank" title="' + i18n.t("core.tools:file.lastModified", {varLastModif : files[i].lastModified, defaultValue : "(last modified: " + files[i].lastModified + ")"}) + '">';
									content += '&nbsp;<span style=" color: ' + files[i].highlightColour + '; ">' + files[i].fileName + '</span></a>&nbsp;';
									// is this directory?												
									if(!files[i].directory)
									{													
										content +=  '<span>';
										
										if (files[i].fileName.startsWith('Costing'))
										{
											// set button to send jobcosting to adveso
											content += '<a href="#" onclick="issueCostingToAdveso('+jobid+', \''+files[i].filePathEncrypted+'\');" class="fileutility">'+
												'<img src="img/icons/send_adveso.png" width="16" height="16" class="img_inl_margleft" alt="' + i18n.t("core.tools:file.makeAvailableToAdveso", "Make Available To Adveso") + '" title="' + i18n.t("core.tools:file.makeAvailableToAdveso", "Make Available To Adveso") + '" />'+
												'</a>';	
										}
										content += '</span>';													
									}												
									content +=	'</li>';
								}
							});
		content += '</ul></div></div></div>';		
	}
	else
		{
		content += '<div class="warningBox1"><div class="warningBox2"><div class="warningBox3">'
			+'<div class="center attention">'+i18n.t('jobcost.emptyfileslisterror','The Job Costing Files List is empty') +'</div></div></div></div>';
		}
	console.log("**** "+content);
	 loadScript.createOverlay('dynamic', i18n.t('core.pricing:jobCost.makeAvailableToAdveso', 'Make Job costing File available to Adveso'), null, content, null, '200px', '640px', null);
	});
}

function issueCostingToAdveso(jobId, filePathEncrypted)
{
	// load the service javascript file if not already
	console.log(jobId);
	
	$j.ajax({
		url: "issueCostingToAdveso.json",
		data: {
			jobid : jobId,
			attach: filePathEncrypted
		},
		async: true
	}).done(function(result) {
		// remove the deleted item from the page
		console.log(result);
		location.reload();
	});
}