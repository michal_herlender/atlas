/****************************************************************************************************************************************
 *												CWMS (Calibration Workflow Management System)
 *			
 *												Copyright 2006, Antech Calibration Services
 *															www.antech.org.uk
 *		
 *															All rights reserved
 *
 *														Document author: 	Stuart Harrold
 *
 *	FILENAME		:	ViewPurchaseOrder.js
 *	DESCRIPTION		:	Page-specific javascript file.
 *	DEPENDENCIES	:   -
 *
 *	TO-DO			:						
 *	KNOWN ISSUES	:	-
 *	HISTORY			:	13/08/2008 - SH - Created File
 *					:	27/10/2015 - TProvost - Manage i18n
 *					:	27/10/2015 - TProvost - Minor fix on enable buttons
 *                   :   2016-06-03 - Galen Beck - removed all Javascript pertaining to action items popup.
 ****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array('script/trescal/core/utilities/DocGenPopup.js',
		'script/trescal/core/tools/FileBrowserPlugin.js',
		'script/thirdparty/jQuery/jquery.boxy.js');

/**
 * Array of objects containing the element name and corner type which should be
 * applied. Applied in init() function.
 */
var niftyElements = new Array({
	element : 'div.infobox',
	corner : 'normal'
}, {
	element : 'ul.subnavtab a',
	corner : 'small transparent top'
});

/**
 * this variable holds an array of id objects, one for the navigation link and
 * one for the content area the link refers to. this array is passed to the
 * Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = new Array({
	anchor : 'items-link',
	block : 'items-tab'
}, {
	anchor : 'edit-link',
	block : 'edit-tab'
}, {
	anchor : 'goods-link',
	block : 'goods-tab'
}, {
	anchor : 'accounts-link',
	block : 'accounts-tab'
}, {
	anchor : 'goodsread-link',
	block : 'goodsread-tab'
}, {
	anchor : 'accountsread-link',
	block : 'accountsread-tab'
}, {
	anchor : 'nominals-link',
	block : 'nominals-tab'
}, {
	anchor : 'files-link',
	block : 'files-tab'
}, {
	anchor : 'email-link',
	block : 'email-tab'
}, {
	anchor : 'actions-link',
	block : 'actions-tab'
}, {
	anchor : 'instructions-link',
	block : 'instructions-tab'
});

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if
 * present. this allows onload coding to be included in the page specific
 * javascript file without adding to the body onload attribute.
 */

/**
 * Shows the PO cancellation warning if purchase order has one or more items and
 * all items are selected to be cancelled; otherwise hides the warning.
 */
function showHideCancelledWarning() {
	var countCancelled = 0;
	var countItems = 0;
	$j("input:checkbox[id^='goodsCancelled']").each(function() {
		if ($j(this).prop('checked'))
			countCancelled++;
		countItems++;
	});
	if (countItems > 0 && (countItems == countCancelled)) {
		$j("#poCancelledWarning").removeClass('hid').addClass('show');
	} else {
		$j("#poCancelledWarning").removeClass('show').addClass('hid');
	}
}

/**
 * this method deletes a purchase order item
 * 
 * @param {Integer}
 *            itemid the id of the purchase order item
 */
function deleteItem(itemid) {
	// load the service javascript file if not already
	loadScript
			.loadScriptsDynamic(
					new Array('dwr/interface/purchaseorderitemservice.js'),
					true,
					{
						callback : function() {
							// call dwr service to delete the purchase order
							// item
							purchaseorderitemservice
									.ajaxDeletePurchaseOrderItem(
											itemid,
											{
												callback : function(po) {
													// remove the deleted item
													// table from the page
													$j('#poitem' + itemid)
															.remove();
													// adjust PO item count
													$j('span.poItemSize')
															.text(
																	parseInt($j(
																			'span.poItemSize:first')
																			.text()) - 1);
													// update final cost value
													$j('span#pototal')
															.empty()
															.append(
																	po.finalCost
																			.toFixed(2)
																			+ '');
													// any items on po?
													if (po.items.length < 1) {
														// remove carriage and
														// total tables
														$j(
																'div.viewPOItems table')
																.not(':first')
																.remove();
														// append message table
														$j('div.viewPOItems')
																.append(
																		'<table class="default4" summary="This table displays message of no purchase order items">'
																				+ '<tbody>'
																				+ '<tr class="nobord">'
																				+ '<td class="center bold">'
																				+ i18n
																						.t(
																								"core.pricing:purchaseOrder.noPOItemsToDisplay",
																								"No purchase order items to display")
																				+ '</td>'
																				+ '</tr>'
																				+ '</tbody>'
																				+ '</table>');
													}
												}
											});
						}
					});
}

/**
 * this function deletes the PO from the database
 * 
 * @param {Integer}
 *            id is the id of the Order to be deleted
 */
function deleteOrder(id) {
	if (confirm(i18n
			.t(
					"core.pricing:purchaseOrder.confirmDeleteOrder",
					"This purchase order will now be deleted. Click 'OK' to confirm the deletion or 'Cancel' to go back"))) {
		window.location.href = 'deletepurchaseorder.htm?poid=' + id;
	}
}

/**
 * this method creates the delete purchase order overlay content
 * 
 * @param {Integer}
 *            orderid id of the purchase order to be deleted
 */
function showDeletePOContent(orderid) {
	// create content for delete purchase order overlay
	var content = '<div id="deletePurchaseOrderOverlay">'
			+
			// add warning box to display error messages if the insertion or
			// update fails
			'<div class="hid warningBox1">'
			+ '<div class="warningBox2">'
			+ '<div class="warningBox3">'
			+ '</div>'
			+ '</div>'
			+ '</div>'
			+ '<fieldset>'
			+ '<ol>'
			+ '<li>'
			+ '<label>'
			+ i18n.t("reasonLabel", "Reason:")
			+ '</label>'
			+ '<textarea name="deletereason" id="deletereason" rows="3" cols="60"></textarea>'
			+ '</li>' + '<li>' + '<label>&nbsp;</label>'
			+ '<input type="button" value="' + i18n.t("delete", "Delete")
			+ '" onclick=" this.disabled = true; deletePurchaseOrder('
			+ orderid + ', $j(\'#deletereason\').val()); return false;" />'
			+ '</li>' + '</ol>' + '</fieldset>' + '</div>';
	// show delete purchase order overlay
	loadScript.createOverlay('dynamic', i18n.t(
			"core.pricing:purchaseOrder.deletePO", "Delete Purchase Order"),
			null, content, null, 35, 580, 'deletereason');
}

/**
 * this method deletes the purchase order and if successful redirects the user
 * to the deleted components page
 * 
 * @param {Integer}
 *            orderid id of the purchase order to be deleted
 * @param {String}
 *            reason the reason the purchase order is being deleted
 */
function deletePurchaseOrder(orderid, reason) {
	// load the service javascript file if not already
	loadScript
			.loadScriptsDynamic(
					new Array('dwr/interface/purchaseorderservice.js'),
					true,
					{
						callback : function() {
							// call dwr service to delete the invoice
							purchaseorderservice
									.deleteOrder(
											orderid,
											reason,
											{
												callback : function(results) {
													// error occured deleting
													// the invoice
													if (!results.success) {
														// empty the error
														// warning box
														$j(
																'div#deletePurchaseOrderOverlay div.warningBox3')
																.empty();
														// variable to hold
														// error messages
														var errors = '';
														$j
																.each(
																		results.errorList,
																		function(
																				i) {
																			// display
																			// error
																			// messages
																			var error = results.errorList[i];
																			errors = '<div>'
																					+ i18n
																							.t(
																									"fieldError",
																									{
																										varErrField : error.field,
																										varErrMsg : error.defaultMessage,
																										defaultValue : "Field: "
																												+ error.field
																												+ " failed. Message:"
																												+ error.defaultMessage
																									})
																					+ '</div>';
																		});
														// show error message in
														// error warning box
														$j(
																'div#deletePurchaseOrderOverlay div:first')
																.removeClass(
																		'hid')
																.addClass('vis')
																.find(
																		'.warningBox3')
																.append(errors);
														// re-enable button
														$j(
																'div#deletePurchaseOrderOverlay input[type="button"]')
																.attr(
																		'disabled',
																		false);
													} else {
														// it worked, so just
														// redirect away from
														// this page to the
														// deleted invoices page
														window.location = springUrl
																+ '/deletedcomponent.htm';
													}
												}
											});
						}
					});
}

/**
 * this method checks all inputs of type checkbox that are contained within the
 * table
 * 
 * @param {Boolean}
 *            select true if boxes are to be checked and vice versa
 * @param {String}
 *            parentElement id of table in which to search for inputs of type
 *            checkbox
 * @param {String}
 *            elementName name of the element we want to check
 */
function selectAllItems(select, parentElement, elementName) {
	// check all
	if (select == true) {
		$j('#' + parentElement + ' input[name="' + elementName + '"]:checkbox')
				.not(':disabled').check('on');
	}
	// uncheck all
	else {
		$j('#' + parentElement + ' input[name="' + elementName + '"]:checkbox')
				.not(':disabled').check('off');
	}
}

/**
 * this method copies the value selected in the element to any others present on
 * the page in the direction specified.
 * 
 * @param {String}
 *            direction the direction in which the value should be copied
 * @param {String}
 *            elementtype the type of element we are copying to
 * @param {Object}
 *            element element which we can obtain value and name for copying
 */
function copySelectedOption(direction, elementtype, element) {
	// add marker after element
	$j(element).after('<span class="marker"></span>');
	// variable to indicate the marker has been found
	var markerFound = false;
	// array to hold all inputs
	var inputArray = new Array();
	// copy information down the list
	if (direction == 'down') {
		// create an array of input objects
		inputArray = $j.makeArray($j(elementtype + '[name="'
				+ $j(element).attr('name') + '"]'));
	} else {
		// create an array of input objects
		inputArray = $j.makeArray($j(elementtype + '[name="'
				+ $j(element).attr('name') + '"]'));
		// reverse the array
		inputArray.reverse();
	}

	// loop through all matching inputs
	$j.each(inputArray, function() {
		// marker found?
		if ($j(this).next().hasClass('marker')) {
			// set variable and remove marker
			markerFound = true;
			$j(this).next().remove();
		}
		// marker found?
		if (markerFound) {
			// copy value to input
			$j(this).val($j(element).val());
			// check if element has onchange event
			if ($j(this).attr('onchange')) {
				// trigger onchange event
				$j(this).trigger('onchange');
			}
		}
	});
}

/**
 * this method toggles the visibility of progress actions in the purchase order
 * item table
 * 
 * @param {Integer}
 *            rowcount the current row number
 * @param {String}
 *            linkId the id of the show progress actions link clicked
 */
function toggleProgressActions(rowcount, linkId) {
	// progress actions are currently hidden so make them visible
	if ($j('#itemprog' + rowcount).css('display') == 'none') {
		// remove image to show progress actions and append hide progress
		// actions image
		$j('#' + linkId)
				.empty()
				.append(
						'<img src="img/icons/hideactions.png" width="16" height="16" alt="'
								+ i18n
										.t(
												"core.pricing:purchaseOrder.hideProgressActions",
												"Hide Progress Actions")
								+ '" title="'
								+ i18n
										.t(
												"core.pricing:purchaseOrder.hideProgressActions",
												"Hide Progress Actions")
								+ '" />');
		// make the progress actions row visible and add slide down effect
		$j('#itemprog' + rowcount).removeClass('hid').addClass('vis').find(
				'div').slideDown(1000);
	}
	// progress actions are currently visible so hide them
	else {
		// remove image to hide progress actions and append show progress
		// actions image
		$j('#' + linkId)
				.empty()
				.append(
						'<img src="img/icons/viewactions.png" width="16" height="16" alt="'
								+ i18n
										.t(
												"core.pricing:purchaseOrder.viewProgressActions",
												"View Progress Actions")
								+ '" title="'
								+ i18n
										.t(
												"core.pricing:purchaseOrder.viewProgressActions",
												"View Progress Actions")
								+ '" />');
		// add effect to slide up progress actions
		$j('#itemprog' + rowcount + ' div').slideUp(1000, function() {
			// hide row
			$j('#itemprog' + rowcount).removeClass('vis').addClass('hid');
		});
	}
}

function resetOrderStatus(id, accountSoftwareName) {
	// load the service javascript file if not already
	loadScript
			.loadScriptsDynamic(
					new Array('dwr/interface/purchaseorderservice.js'),
					true,
					{
						callback : function() {
							purchaseorderservice
									.resetAccountStatus(
											id,
											{
												callback : function(results) {
													if (results.success != true) {
														$j
																.prompt(i18n
																		.t(
																				"core.pricing:errorResetStatus",
																				"Error updating status, please contact an administrator"));
													} else {
														window.location
																.reload();
													}
												}
											});
						}
					});
}

var copyCancelPopUpCLicked = false;
function copyCancelPopUp(popupText1, popupText2, poId) {
	var state0ButtonTexts = ' { "' + popupText1[2] + '":true,"' + popupText1[3]
			+ '":false } ';
	var state1ButtonTexts = ' { "' + popupText2[2] + '":1,"' + popupText2[3]
			+ '":0,"' + popupText2[4] + '":-1 } ';
	var copyPoWizard = {
		state0 : {
			title : [ popupText1[0] ],
			html : [ popupText1[1] ],
			buttons : JSON.parse(state0ButtonTexts),
			focus : 1,
			submit : function(e, v, m, f) {
				if (v) {
					$j.prompt.goToState('state1');
					return false;
				}
				$j.prompt.close();
			}
		},
		state1 : {
			title : [ popupText2[0] ],
			html : [ popupText2[1] ],
			buttons : JSON.parse(state1ButtonTexts),
			focus : 0,
			submit : function(e, v, m, f) {
				e.preventDefault();
				var cancel;
				if (v == 0) {
					cancel = true;
				} else if (v == 1) {
					cancel = false;
				} else {
					$j.prompt.close();
					return;
				}

				if (!copyCancelPopUpCLicked) {
					// make the ajax call
					$j.ajax({
						url : "copyPO.json?id=" + poId + "&cancel=" + cancel,
						dataType : 'json'
					}).done(function(res) {
						console.log(res);
						if (res.success === true) {
							// redirect to the newly created PO
							window.location.href = res.results;
						} else {
							$j.prompt.close();
							$j.prompt(res.message);
							copyCancelPopUpCLicked = false;
						}
					});
				}
				copyCancelPopUpCLicked = true;
				$j.prompt.close();
			}
		}
	};

	$j.prompt(copyPoWizard);

}
