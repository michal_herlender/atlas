/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	PurchaseOrderHome.js
*	DESCRIPTION		:	Page-specific javascript file.
*	DEPENDENCIES	:   -
*
*	TO-DO			:						
*	KNOWN ISSUES	:	-
*	HISTORY			:	13/08/2008 - SH - Created File
*					: 	27/10/2015 - TProvost - Manage i18n
*					:	27/10/2015 - TProvost - Minor fix on enable buttons
****************************************************************************************************************************************/


/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' },
								{ element: 'ul.subnavtab a', corner: 'small transparent top' } );
								
/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'pono';	
							
/**
 * array of calendar objects containing the initial setup literals needed
 * 
 * @param {String} inputField is the id of the input field to which the calendar should be binded
 * @param {String} dateFormat is the format that the date should be displayed
 * @param {String} displayDates 'all' make all dates available
 * 								'future' show only dates in the future
 * 								'past' show only dates in the past
 * applied in init() function
 */
//var jqCalendar = new Array(	{ 
//								inputField: 'date',
//								dateFormat: 'dd.mm.yy',
//								displayDates: 'all',
//								showWeekends: true
//							},
//							{ 
//								inputField: 'dateFrom',
//								dateFormat: 'dd.mm.yy',
//								displayDates: 'all',
//								showWeekends: true
//							},
//							{ 
//								inputField: 'dateTo',
//								dateFormat: 'dd.mm.yy',
//								displayDates: 'all',
//								showWeekends: true		
//							});

/**
 * this method creates quick purchase order overlay content.
 */
function quickPOContent()
{
	// create content for overlay
	var content =	'<div id="quickPurchaseOrderOverlay">' +
						// add warning box to display error messages
						'<div class="hid warningBox1">' +
							'<div class="warningBox2">' +
								'<div class="warningBox3">' +
								'</div>' +
							'</div>' +
						'</div>' +
						'<fieldset>' +
							'<ol>' +
								'<li>' +
									'<label>' + i18n.t("descriptionLabel", "Description:") + '</label>' +
									'<textarea name="description" id="description" class="width70" rows="6"></textarea>' +
								'</li>' +
								'<li>' +
									'<label>' + i18n.t("core.pricing:purchaseOrder.jobNo", "Jobno (optional):") + '</label>' +
									'<input type="text" name="jobno" id="jobno" value="" />' +
								'</li>' +
								'<li>' +
									'<label>&nbsp;</label>' +
									'<input type="button" value="' + i18n.t("save", "Save") + '" onclick=" saveQuickPO($j(\'#quickPurchaseOrderOverlay #description\').val(), $j(\'#quickPurchaseOrderOverlay #jobno\').val()); return false; " />' +
								'</li>' +
							'</ol>' +
						'</fieldset>' +
					'</div>'; 	
	// show in overlay
	loadScript.createOverlay('dynamic', i18n.t("core.pricing:purchaseOrder.createQuickPurchaseOrder", "Create Quick Purchase Order"), null, content, null, 34, 720, 'description');
}

/**
 * this method saves a quick purchase order to the database.
 * 
 * @param {String} description the description of quick purchase order to be added
 * @param {String} jobno the job number to be linked (optional)
 */
function saveQuickPO(description, jobno)
{ 
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/quickpurchaseorderservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to create the quick purchase order
			quickpurchaseorderservice.createQuickPurchaseOrder(description, jobno,
			{
				callback:function(results)
				{
					// purchase order added successfully
					if(results.success != true)
					{
						// error list returned?
						if(results.errorList)
						{
							// empty the error warning box
							$j('div#quickPurchaseOrderOverlay div.warningBox3').empty();
							// variable to hold error messages
							var errors = '';
							$j.each(results.errorList, function(i)
							{
								// display error messages
								var error = results.errorList[i];
								errors ='<div>' + i18n.t("fieldError", {varErrField: error.field, varErrMsg:error.defaultMessage, defaultValue : "Field: " + error.field + " failed. Message:" + error.defaultMessage}) + '</div>';
							});
							// show error message in error warning box
							$j('div#quickPurchaseOrderOverlay div:first').removeClass('hid').addClass('vis').find('.warningBox3').append(errors);
						}
						else
						{
							// empty the error warning box
							$j('div#quickPurchaseOrderOverlay div.warningBox3').empty();
							// show error message in error warning box
							$j('div#quickPurchaseOrderOverlay div:first').removeClass('hid').addClass('vis').find('.warningBox3').append(results.message);
						}
					}
					else
					{				
						// assign quick purchase order object								
						var qpo = results.results;
						// quick po table empty?
						if ($j('table.QPOtable tbody tr:first td').length < 2)
						{
							// remove message of no quick purchase orders
							$j('table.QPOtable tbody tr:first').remove();
						}
						// variable to hold rowcount
						var rowcount = ($j('table.QPOtable tbody tr').length + 1);
						// set class name
						var className = (rowcount % 2 == 0) ? 'even' : 'odd';
						// append message of no quick purchase orders
						$j('table.QPOtable tbody').append(	'<tr id="qpo' + qpo.id + '" class="' + className + '">' +
																'<td class="qpoNum">' + qpo.pono + '</td>' +
																'<td class="qpoDesc">' + qpo.description + '</td>' +
																'<td class="qpoCreateBy">' + qpo.createdBy.name + '</td>' +
																'<td class="qpoCreateOn">' + formatDate(qpo.createdOn, 'dd.MM.yyyy') + '</td>' +
																'<td class="qpoJobno">' + qpo.jobno + '</td>' +
																'<td class="qpoApprove">' +
																	'<a href="' + springUrl + '/createpurchaseorder.htm?quickpoid=' + qpo.id + '")" class="mainlink">' + i18n.t("core.pricing:purchaseOrder.approve", "Approve") + '</a>' +
																'</td>' +
																'<td class="qpoDel">' +
																	'<a href="#" onclick=" showDeleteQPOContent(' + qpo.id + '); return false; ">' +
																		'<img src="img/icons/delete.png" width="16" height="16" alt="' + i18n.t("core.pricing:purchaseOrder.deleteQuickPO", "Delete Quick Purchase Order") + '" title="' + i18n.t("core.pricing:purchaseOrder.deleteQuickPO", "Delete Quick Purchase Order") + '" />' +		
																	'</a>' +
																'</td>' +
															'</tr>');
						// add one to count
						$j('span.qpoSize').text(parseInt($j('span.qpoSize:first').text()) + 1);
						// close overlay
						loadScript.closeOverlay();
						// switch focus to outstanding quick purchase orders
						switchMenuFocus(menuElements, 'qpo-tab', false);
					}
				}
			});
		}
	});
}

/**
 * this method creates the delete quick purchase order overlay content
 * 
 * @param {Integer} orderid id of the quick purchase order to be deleted
 */
function showDeleteQPOContent(orderid)
{
	// create content for delete purchase order overlay
	var content = 	'<div id="deleteQuickPurchaseOrderOverlay">' +
						// add warning box to display error messages if the insertion or update fails
						'<div class="hid warningBox1">' +
							'<div class="warningBox2">' +
								'<div class="warningBox3">' +
								'</div>' +
							'</div>' +
						'</div>' +
						'<fieldset>' +
							'<ol>' +
								'<li>' +
									'<label>' + i18n.t("reasonLabel", "Reason:") + '</label>' +
									'<textarea name="deletereason" id="deletereason" rows="3" cols="60"></textarea>' +
								'</li>' +								
								'<li>' +
									'<label>&nbsp;</label>' +
									'<input type="button" value="' + i18n.t("delete", "Delete") + '" onclick=" this.disabled = true; deleteQuickPurchaseOrder(' + orderid + ', $j(\'#deletereason\').val()); return false;" />' +
								'</li>' +								
							'</ol>' +
						'</fieldset>' +
					'</div>';
	// show delete purchase order overlay
	loadScript.createOverlay('dynamic', i18n.t("core.pricing:purchaseOrder.deleteQuickPO", "Delete Quick Purchase Order"), null, content, null, 20, 580, 'deletereason');
}

/**
 * this method deletes a quick purchase order and redirects the .
 * 
 * @param {Integer} orderid id of the quick purchase order to be deleted
 * @param {String} reason the reason for deleting quick purchase order
 */
function deleteQuickPurchaseOrder(orderid, reason)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/quickpurchaseorderservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to delete the quick purchase order
			quickpurchaseorderservice.ajaxDeleteQuickPurchaseOrder(orderid, reason,
			{
				callback: function(results)
				{
					// error occured deleting the invoice
					if(!results.success)
					{
						// empty the error warning box
						$j('div#deleteQuickPurchaseOrderOverlay div.warningBox3').empty();
						// variable to hold error messages
						var errors = '';
						$j.each(results.errorList, function(i)
						{
							// display error messages
							var error = results.errorList[i];
							errors ='<div>' + i18n.t("fieldError", {varErrField: error.field, varErrMsg:error.defaultMessage, defaultValue : "Field: " + error.field + " failed. Message:" + error.defaultMessage}) + '</div>';
						});
						// show error message in error warning box
						$j('div#deleteQuickPurchaseOrderOverlay div:first').removeClass('hid').addClass('vis').find('.warningBox3').append(errors);
						// re-enable button
						$j('div#deleteQuickPurchaseOrderOverlay input[type="button"]').attr('disabled', false);
					}
					else
					{
						// delete quick purchase order from page
						$j('tr#qpo' + orderid).remove();
						// re-apply zebra effect in case it has been broken
						$j('table.QPOtable').zebra();
						// quick po table empty?
						if ($j('table.QPOtable tbody tr').length < 1)
						{
							// append message of no quick purchase orders
							$j('table.QPOtable tbody').append(	'<tr class="odd">' +
																	'<td colspan="6" class="center bold">' + i18n.t("core.pricing:purchaseOrder.noOutstandingQuickPOs", "There are no Outstanding Quick PO's") + '</td>' +
																'</tr>');
						}
						// subtract one from count
						$j('span.qpoSize').text(parseInt($j('span.qpoSize:first').text()) - 1);
						// close overlay
						loadScript.closeOverlay();
					}
				}
			});
		}
	});
}