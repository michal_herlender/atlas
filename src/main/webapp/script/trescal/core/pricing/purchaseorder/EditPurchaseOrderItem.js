/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	EditPurchaseOrderItem.js
*	DESCRIPTION		:	Page-specific javascript file.
*	DEPENDENCIES	:   -
*
*	TO-DO			:						
*	KNOWN ISSUES	:	-
*	HISTORY			:	14/08/2008 - SH - Created File
*					: 	27/10/2015 - TProvost - Manage i18n
*					:	27/10/2015 - TProvost - Minor fix on enable buttons
****************************************************************************************************************************************/


/**
 * Array of objects containing the element name and corner type which should be applied.
 * Applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * array of calendar objects containing the initial setup literals needed
 * 
 * @param {String} inputField is the id of the input field to which the calendar should be binded
 * @param {String} dateFormat is the format that the date should be displayed
 * @param {String} displayDates 'all' make all dates available
 * 								'future' show only dates in the future
 * 								'past' show only dates in the past
 * applied in init() function
 */				
//var jqCalendar = new Array ( { 
//								 inputField: 'deliveryDate',
//								 dateFormat: 'dd.mm.yy',
//								 displayDates: 'future',
//								 showWeekends: true
//							 });

/**
 * this method tries to find the best matching nominal code for the cost type and job item
 * selected.
 * 
 * @param {Integer} costTypeId the id of selected cost type
 * @param {Integer} jobItemId the id of selected job item
 */
function findNominal(costTypeId, jobItemId)
{
	console.log(costTypeId);
	console.log(jobItemId);
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/nominalcodeservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to find best matching nominal code
			nominalcodeservice.findAjaxBestMatchingNominalCode(ledgers, costTypeId, jobItemId,
			{
				callback:function(results)
				{
					// successful?
					if(results.success)
					{
						var nom = results.results;
						$j('#suggestednominal').empty().append(i18n.t("core.pricing:purchaseOrder.findNominal", {varNominal : nom.code + ': ' + nom.title + ' <a href="#" class="mainlink" onclick=" $j(\'#nominalId\').val(' + nom.id + '); return false; ">' + i18n.t('switch', 'Switch') + '</a>', 
							defaultValue : 'Suggested Nominal: ' + nom.code + ': ' + nom.title + ' <a href="#" class="mainlink" onclick=" $j(\'#nominalId\').val(' + nom.id + '); return false; ">Switch</a>'}));
					}
					else
					{
						$j('#suggestednominal').empty().append(i18n.t("core.pricing:purchaseOrder.noFindNominal", "Suggested Nominal: No matches found"));
					}
				}
			});
		}
	});
}

/**
 * Fired when the complete status of an item is updated. Checks if the item is complete, 
 * if so enables the goods & accounts approved fields, if not then disables these fields.
 * @return
 */
function updateOrderItemCompleteStatus(completeStatus, userAccountsApproved)
{
	if(completeStatus == 'COMPLETE')
	{
		// enable the goods approval
		$j('input[name="progress.goodsApproved"]').attr('disabled', false);
		
		if(userAccountsApproved == true)
		{
			// enable the accounts approval
			$j('input[name="progress.accountsApproved"]').attr('disabled', false);
		}
		else
		{
			// disable the accounts approval
			$j('input[name="progress.accountsApproved"]').attr('disabled', 'disabled').filter(':first').attr('checked', 'checked');
		}
	}
	else
	{
		// disable the accounts and goods approvals
		$j('input[name="progress.goodsApproved"]').attr('disabled', 'disabled').filter(':first').attr('checked', 'checked');
		// enable the accounts approval
		$j('input[name="progress.accountsApproved"]').attr('disabled', 'disabled').filter(':first').attr('checked', 'checked');	
	}
}
