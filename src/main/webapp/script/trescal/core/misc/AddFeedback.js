/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	AddFeedback.js
*	DESCRIPTION		:	Page specific javascript file.
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	20/12/2007 - SH - Created File
*
****************************************************************************************************************************************/


/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * variable which holds the address bar location of errored page   
 */
var URL = window.location;

/**
 * method to send the user back to the page on which the error occured
 */
function goBack()
{
	window.location.href = URL;
}