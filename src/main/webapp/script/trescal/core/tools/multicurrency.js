/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Richard Dean
*
*	FILENAME		:	multicurrency.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	07/06/2007
*					:	19/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/


function getBaseCurrencyValue(id, value)
{
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/gepservice.js'), true,
	{
		callback: function()
		{
			gepservice.getBaseCurrencyValue(id, value,
			{
				callback:function(result)
				{
					if(!result)
					{
						$j.prompt(i18n.t("core.tools:errorRetrievingCurrencyConv", "An error occurred retrieving the currency conversion"));
					}	
					else
					{
						$j.prompt(i18n.t("core.tools:monetaryValue", {varResult : result, defaultValue : "Monetary value is " + result}));	
					}
				}
			});
		}
	});
}