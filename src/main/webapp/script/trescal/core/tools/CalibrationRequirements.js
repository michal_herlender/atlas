/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	CalibrationRequirements.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	03/11/2010
*					:	22/10/2015 - TProvost - Manage i18n
*					:	22/10/2015 - TProvost - Fix bug in new version of Impromptu.js - Replace the "callback" property (property disappeared) by the "submit" property
*					:							=> Changes made in function deactivateCalReqs
****************************************************************************************************************************************/

 
/**
 * this method is called when a calibration requirement of any type is deleted/deactivated by the user. If a fall back calibration requirment is available/applicable
 * then this will be displayed in place of the one deleted/deactivated.
 * 
 * @param {String} page the name of the page where the calibration requirement has been deleted/deactivated
 * @param {Integer} calreqid the id of the calibration requirement deleted/deactivated
 * @param {String} type the type of calibration requirement being deleted/deactivated
 * @param {Integer} id varying id used depending on the page
 */
function deactivateCalReqs(page, calreqid, type, id, callback)
{
	// initial prompt for user
	$j.prompt(i18n.t("core.tools:cal.confirmDeleteCalReq", "Are you sure that you wish to remove these calibration requirements? (The system will fall back to other defaults if applicable)"),
	{ 
		submit: confirmcallback,
		buttons: 
		{ 
			Ok: true,
			Cancel: false 
		}, 
		focus: 1 
	});
	// callback method for initial prompt
	function confirmcallback(v,m)
	{
		// user confirmed action?
    	if (m)
		{
			// load the service javascript file if not already
			loadScript.loadScriptsDynamic(new Array('dwr/interface/calreqservice.js'), true,
			{
				callback: function()
				{
					// call dwr service to deactivate the calibration requirement
					calreqservice.ajaxDeactivateCalReqs(calreqid, type, 
					{
						callback:function(result)
						{
							// deactivated successfully?
							if(result.success)
							{
								// which page do we need to update?
								switch (page)
								{
									case 'quotation':	// have model calibration requirements been returned?
														if (result.results != null)
														{
															// get calibration results object
															var cr = result.results;
															// get the content
															// create new list item for calibration requirements
															var content = 	'<li class="itemCalReq' + cr.id + '">' +
																				'<div class="inlinenotes_note">';
																				if (cr.pointSet)
																				{
																					content +=  '<span class="bold">' + i18n.t("core.tools:cal.calPoints", "Calibration Points:") + '</span>' +
																								'<span class="attention bold">' +
																									'<span>';
																										$j.each(cr.pointSet.points, function(i, p)
																										{
																											content +=  '' + p.formattedPoint + ' ' + p.uom.formattedSymbol;
																											if (p.formattedRelatedPoint != '')
																											{
																												content += ' @ ' + p.formattedRelatedPoint + ' ' + p.relatedUom.formattedSymbol + '';
																											}
																											if ((i + 1) != cr.pointSet.points.length)
																											{
																												content += 	' | ';
																											}																	
																										});
																						content +=	'</span>' +
																								'</span>' +
																								'<br />';
																				}
																				else if (cr.range)
																				{
																					content +=	'<span class="bold">' + i18n.t("core.tools:cal.calRange", "Calibration Range:") + '</span>' +
																								'<span class="attention bold">' + 
																									cr.range.start;
																									if (cr.range.end)
																									{
																										if (cr.range.end != '')
																										{
																											content += ' - ' + cr.range.end;
																										}
																									}
																									content += ' ' + cr.range.uom.formattedSymbol;
																									if (cr.range.relational)
																									{
																										content +=	' @ ' + cr.range.formattedRelatedPoint + ' ' + cr.range.relatedUom.formattedSymbol;
																									}
																					content += 	'</span>' +
																								'<br />';
																				}
																				else
																				{
																					content +=	'<span class="bold">' + i18n.t("core.tools:cal.calInstructions", "Calibration Instructions:") + '</span>' +
																								'<br />';
																				}
																				if ((cr.privateInstructions) && (cr.privateInstructions != ''))
																				{
																					content +=	'<span class="attention bold">' + i18n.t("core.tools:cal.private", "Private:") + ' ' + cr.privateInstructions.replace(/\n/gi, "&nbsp;<br />") + '</span>' +
																								'<br />';
																				}
																				if ((cr.publicInstructions) && (cr.publicInstructions != ''))
																				{
																					content +=	'<span class="allgood bold">' + i18n.t("core.tools:cal.public", "Public:") + ' ' + cr.publicInstructions.replace(/\n/gi, "&nbsp;<br />") + '</span>' +
																								'<br />';
																				}
																		content += 	'<span class="bold">' + cr.translatedSource + '</span>' +
																				'</div>' +
																				'<div class="inlinenotes_edit">' +
																					'<span>' +
																						'<span class="bold attention">';
																							if (cr.publish)
																							{
																								content +=	i18n.t("core.tools:cal.publicCalReq", "Public Calibration Requirement");
																							}
																							else
																							{
																								content += i18n.t("core.tools:cal.privateCalReq", "Private Calibration Requirement");
																							}
																			content +=	'</span>' +
																						'<a href="#" class="editPoints" onclick=" loadScript.createOverlay(\'external\', \'' + i18n.t("core.tools:cal.editCalPointsAndReq", "Edit Cal Points &amp; Requirements") + '\', \'calibrationpoints.htm?type=' + cr.classKey + '&page=' + page + '&calreqid=' + cr.id + '\', null, null, 80, 900, null); return false; " title="' + i18n.t("core.tools:cal.editCalPointsAndReq", "Edit Cal Points &amp; Requirements") + '">&nbsp;</a>' +		
																						'<a href="#" class="deletePoints" onclick=" deactivateCalReqs(\'' + page + '\', ' + cr.id + ', \'' + cr.classKey + '\'); return false; " title="' + i18n.t("core.tools:cal.removeCalPointsAndReq", "Remove Points &amp; Requirements") + '">&nbsp;</a>' +
																					'</span>' +
																					'<br />' +
																					'<span>';
																					if (cr.lastModifiedBy)
																					{
																						content +=	cr.lastModifiedBy.name;
																					}
																					if (cr.lastModified)
																					{
																						content +=	' (' + formatDate(cr.lastModified, 'dd.MM.yyyy - HH:mm:ss') + ')';
																					}
																	content +=	'</span>'+'</div>' +
																				'<div class="clear-0"></div>' +
																			'</li>';
															// variable declared for later use
															var plantid = 0;
															// check each calibration requirement on items
															$j('li.itemCalReq' + calreqid).each(function(i)
															{
																// get plant and model id
																plantid = $j(this).closest('tr').prev().find('input.itemCalReqPlantId').val();
																modelid = $j(this).closest('tr').prev().find('input.itemCalReqModelId').val();
																// add this new content to page and remove the old
																$j(this).after(content).remove();
															});
															// variable for publish class
															var publishclass = 'instcalreq_highlight';
															// check publish property
															if (cr.publish)
															{
																publishclass = 'instcalreq_highlight_doc';
															}
															// update icons and links
															$j('a.calReqButton-' + plantid).closest('td').removeClass().addClass('notes ' + publishclass).end()
																.after('<a href="#" class="calReqButton-' + modelid + ' editPoints" onclick=" loadScript.createOverlay(\'external\', \'' + i18n.t("core.tools:cal.editCalPointsAndReq", "Edit Cal Points &amp; Requirements") + '\', \'calibrationpoints.htm?type=companymodel&page=quotation&calreqid=' + cr.id + '\', null, null, 80, 900, null); return false; " title="' + i18n.t("core.tools:cal.editCalPointsAndReq", "Edit Cal Points &amp; Requirements") + '">&nbsp;</a>' +
																		'<a href="#" class="calReqButton-' + plantid + ' addPoints" onclick=" loadScript.createOverlay(\'external\', \'' + i18n.t("core.tools:cal.addCalPointsAndReq", "Add Cal Points &amp; Requirements") + '\', \'calibrationpoints.htm?type=instrument&page=quotation&plantid=' + plantid + '\', null, null, 80, 900, null); return false; " title="' + i18n.t("core.tools:cal.addCalPointsAndReq", "Add Cal Points &amp; Requirements") + '">&nbsp;</a>')
																.remove();
														}
														else
														{
															// variables declared for later use
															var modelid = 0;
															var coid = 0;
															// check each calibration requirement on items
															$j('li.itemCalReq' + calreqid).each(function(i)
															{
																// get quote item id, note type, model id and company id update count before anything removed
																var notetype = $j(this).closest('tr').prev().find('input.itemCalReqNoteType').val();
																var quoteitemId = $j(this).closest('tr').prev().find('input.itemCalReqQuoteitemId').val();
																modelid = $j(this).closest('tr').prev().find('input.itemCalReqModelId').val();
																coid = $j(this).closest('tr').prev().find('input.itemCalReqCompId').val();
																// use model id to update counts
																$j('#count' + notetype + quoteitemId).text(parseInt($j('#count' + notetype + quoteitemId).text()) - 1);
																// check to see if any quote item notes exist?
																if ($j(this).closest('fieldset.showActiveItemNotes').find('ol').length > 1)
																{
																	// delete just calibration requirement list
																	$j(this).parent().remove();
																}
																else
																{
																	// no notes so remove the whole note row from table
																	$j(this).closest('tr').remove();
																}
															});
															// update all buttons for this model on the page (may be more 
															// than one if the same model exists on the quote multiple times)
															$j('a.calReqButton-' + id).siblings('a[class^="calReqButton"]').remove().end().closest('td').removeClass().addClass('notes').end()
															.after('<a href="#" class="calReqButton-' + id + ' addPoints" onclick=" loadScript.createOverlay(\'external\', \'' + i18n.t("core.tools:cal.addCalPointsAndReq", "Add Points &amp; Requirements") + '\', \'calibrationpoints.htm?type=instrument&page=quotation&plantid=' + id + '\', null, null, 80, 900, null); return false; " title="' + i18n.t("core.tools:cal.addCalPointsAndReq", "Add Cal Points &amp; Requirements") + '">&nbsp;</a>')
																.remove();	
														}								
														break;
														
									case 'instrument':	// have model calibration requirements been returned?
														if (result.results != null)
														{
															// get calibration results object
															var cr = result.results;
															// get the content
															var content = newCalReqContent(cr, page, id);
															// add this new content to page and remove the old
															$j('li.' + page + calreqid).after(content).remove();
														}
														else
														{
															// create content to add instrument calibration requirements
															var content = 	'<li class="defaultCalReqListItemIdentifier">' +
																				'<label class="bold">' + i18n.t("core.tools:cal.calReqs", "Calibration Reqs:") + '</label>' +
																				'<div class="padtop">' +
																					'<a href="#" class="addPointsInstrument mainlink" onclick=" loadScript.createOverlay(\'external\', \'' + i18n.t("core.tools:cal.addCalPointsAndReq", "Add Cal Points &amp; Requirements") + '\', \'calibrationpoints.htm?type=instrument&page=instrument&plantid=' + id + '\', null, null, 80, 900, null); return false; ">Add To Inst</a>' +
																					// float show history link
																					'<div class="float-right">';
																						// instrument page?
																						if (page == 'instrument')
																						{
																							content +=	'<a href="#" class="mainlink" onclick=" getHistoryForInstrumentCalReq(this, ' + calreqid + ', ' + id + '); return false; ">' + i18n.t("core.tools:cal.showHistory", "Show History") + '</a>';
																						}
																						else if (page == 'jiheader')
																						{
																							content +=	'<a href="#" class="mainlink" onclick=" getHistoryForJobItemCalReq(this, ' + calreqid + ', ' + id + '); return false; ">' + i18n.t("core.tools:cal.showHistory", "Show History") + '</a>';
																						}
																		content += 	'</div>' +
																				'</div>' +																				
																			'</li>';
															// check that the list item containing calibration requirements exists
															if ($j('li.' + page + calreqid).length > 0)
															{
																// calibration requirements exists so remove from page
																$j('li.' + page + calreqid).after(content).remove();
															}
														}
														
														break;
											
									case 'jiheader':	// have instrument or model calibration requirements been returned?
														if (result.results != null)
														{
															// get calibration results object
															var cr = result.results;															
															// get the content
															var content = newCalReqContent(cr, page, id);
															// add this new content to page and remove the old
															$j('li.' + page + calreqid).after(content).remove();
														}
														else
														{
															// get plant id
															var plantid =  $j('#currentJobItemPlantId', window.parent.document).val();
															// create content to add job item calibration requirements
															var content = 	'<li class="defaultCalReqListItemIdentifier">' +
																				'<label class="bold">' + i18n.t("core.tools:cal.calReqs", "Calibration Reqs:") + '</label>' +
																				'<div class="padtop">' +
																					'<a href="#" class="addPointsJobItem mainlink" onclick=" loadScript.createOverlay(\'external\', \'' + i18n.t("core.tools:cal.addCalPointsAndReq", "Add Cal Points &amp; Requirements") + '\', \'calibrationpoints.htm?type=jobitem&page=jiheader&jobitemid=' + id + '\', null, null, 80, 900, null); return false; ">' + i18n.t("core.tools:cal.addToJI", "Add To JI") + '</a>' +
																					'<a href="#" class="addPointsInstrument mainlink marg-left" onclick=" loadScript.createOverlay(\'external\', \'' + i18n.t("core.tools:cal.addCalPointsAndReq", "Add Cal Points &amp; Requirements") + '\', \'calibrationpoints.htm?type=instrument&page=jiheader&plantid=' + plantid + '\', null, null, 80, 900, null); return false; ">' + i18n.t("core.tools:cal.addToInstr", "Add To Inst") + '</a>' +
																					// float show history link
																					'<div class="float-right">';
																						// instrument page?
																						if (page == 'instrument')
																						{
																							content +=	'<a href="#" class="mainlink" onclick=" getHistoryForInstrumentCalReq(this, ' + calreqid + ', ' + id + '); return false; ">' + i18n.t("core.tools:cal.showHistory", "Show History") + '</a>';
																						}
																						else if (page == 'jiheader')
																						{
																							content +=	'<a href="#" class="mainlink" onclick=" getHistoryForJobItemCalReq(this, ' + calreqid + ', ' + id + '); return false; ">' + i18n.t("core.tools:cal.showHistory", "Show History") + '</a>';
																						}
																		content += 	'</div>' +
																				'</div>' +																				
																			'</li>';
															// check that the list item containing calibration requirements exists
															if ($j('li.' + page + calreqid).length > 0)
															{
																// calibration requirements exists so remove from page
																$j('li.' + page + calreqid).after(content).remove();
															}
														}
														
														break;
														
									case 'instmodcompcalreqs':	// no requirements should be returned
																if (result.results != null)
																{
																	// nothing here for the time being unless model calibration requirements added
																}
																else
																{																										
																	// create content to add instrument model company calibration requirements
																	var content = 	'<a href="#" class="addPoints" onclick=" loadScript.createOverlay(\'external\', \'' + i18n.t("core.tools:cal.addCalPointsAndReq", "Add Cal Points &amp; Requirements") + '\', \'calibrationpoints.htm?type=companymodel&page=instmodcompcalreqs&compid=' + instrumentModelCompanyCalReqCoid + '&modelid=' + id + '\', null, null, 80, 900, null); return false; " title="' + i18n.t("core.tools:cal.addCalPointsAndReq", "Add Cal Points &amp; Requirements") + '">&nbsp;</a>';
																	// check that the list item containing calibration requirements exists
																	if ($j('tr.' + page + id).length > 0)
																	{
																		// calibration requirements exists so remove from page
																		$j('tr.' + page + id).removeClass('highlight-green').find('td:first').html(content);
																	}
																}
																
																break;
								}								
							}
							else
							{
								// show user error message
								$j.prompt(result.message);	
							}
						}
					});
				}
			});
		}
    	
    	if(callback !== undefined)
    		callback();
    	
	};
}

/**
 * this method creates the content for a points list.
 * 
 * @param {Object} pointSet object containing list of points
 * @returns html string
 */
function showPoints(pointSet)
{
	// create point content
	var content = 	'<span>';
						$j.each(pointSet.points, function(i, p)
						{
							content += 	p.formattedPoint + ' ' + p.uom.formattedSymbol;
										if (p.formattedRelatedPoint != '')
										{
											content += 	' @ ' + p.formattedRelatedPoint + ' ' + p.relatedUom.formattedSymbol;
										}
										if ((i + 1) != pointSet.points.length)
										{
											content += 	' | ';
										}
						});
		content +=	'</span>';
		
	return content;
}

/**
 * this method is used to create generic calibration requirement content to be displayed in a list item
 * 
 * @param {Object} cr calibration requirement object used for population
 * @param {String} page the name of the page where the calibration requirement has been deleted/deactivated
 * @param {Integer} id id passed depending on the calibration requirement type
 * @returns {String} html string
 */
function newCalReqContent(cr, page, id)
{	
	// default plant id to use this id, if on job item will be sourced elsewhere
	var plantid = id;
	// what page?
	if (page == 'jiheader')
	{
		/* $j('#currentJobItemId').val() is retrieved from the job item header macro */
		id = $j('#currentJobItemId', window.parent.document).val();
	}
	// create new content
	var content = 	'<li class="' + page + cr.id + '">' +
						'<input type="hidden" id="previousCalReqListItemIdentifier" value="' + page + cr.id + '" />';
						if((page == 'instrument' && cr.classKey == 'companymodel') || (page == 'jiheader' && (cr.classKey == 'instrument' || cr.classKey == 'companymodel')))
						{
							content +=	'<div class="float-right padtop">';
							if(page == 'jiheader' && (cr.classKey == 'instrument' || cr.classKey == 'companymodel'))
							{					
								/* $j('#currentJobItemId').val() is retrieved from the job item header macro */
								content += 	'<a href="#" class="addPointsJobItem mainlink" onclick=" loadScript.createOverlay(\'external\', \'' + i18n.t("core.tools:cal.addCalPointsAndReq", "Add Cal Points &amp; Requirements") + '\', \'calibrationpoints.htm?type=' + cr.classKey + '&page=' + page + '&calreqid=' + cr.id + '&convert=true&jobitemid=' + id + '\', null, null, 80, 900, null); return false; ">' +
												i18n.t("core.tools:cal.overrideOnJI", "Override On JI") +
											'</a>';															
							}
							content += 	'&nbsp;&nbsp;';
							if ((page == 'instrument' || page == 'jiheader') && (cr.classKey == 'companymodel'))
							{								
								if (page == 'jiheader')
								{
									plantid =  $j('#currentJobItemPlantId', window.parent.document).val();
								}								
								content += 	'<a href="#" class="addPointsInstrument mainlink" onclick=" loadScript.createOverlay(\'external\', \'' + i18n.t("core.tools:cal.addCalPointsAndReq", "Add Cal Points &amp; Requirements") + '\', \'calibrationpoints.htm?type=' + cr.classKey + '&page=' + page + '&calreqid=' + cr.id + '&convert=true&plantid=' + plantid + '\', null, null, 80, 900, null); return false; ">' +
												i18n.t("core.tools:cal.overrideOnInstr", "Override On Inst") +
											'</a>';
							}
							content += '</div>';
						}
						if((page == 'instrument' && cr.classKey == 'instrument') || (page == 'jiheader' && cr.classKey == 'jobitem'))
						{
							content +=	'<div class="float-right">' +
											'<a href="#" class="editPoints" onclick=" loadScript.createOverlay(\'external\', \'' + i18n.t("core.tools:cal.editCalPointsAndReq", "Edit Cal Points &amp; Requirements") + '\', \'calibrationpoints.htm?type=' + cr.classKey + '&page=' + page + '&calreqid=' + cr.id + '\', null, null, 80, 900, null); return false; ">&nbsp;</a>' +
											'<a href="#" class="deletePoints" onclick=" deactivateCalReqs(\'' + page + '\', ' + cr.id + ', \'' + cr.classKey + '\', ' + id + '); return false; ">&nbsp;</a>' +
										'</div>';
						}
						// points saved on calibration requirements?
						if (cr.pointSet)
						{
							content += 	'<label class="bold">' + i18n.t("core.tools:cal.calPoints", "Calibration Points:") + '</label>' +
										'<div class="clear"></div>' +
										'<span class="attention bold">' + showPoints(cr.pointSet) + '</span><br />';
						}
						// range saved on calibration requirement?
						else if (cr.range)
						{
							content += 	'<label class="bold">' + i18n.t("core.tools:cal.calRange", "Calibration Range:") + '</label>' +
										'<div class="clear"></div>' +
										'<span class="attention bold">' + 
											cr.range.start;
											if (cr.range.end)
											{
												if (cr.range.end != '')
												{
													content += ' ' + i18n.t("core.tools:cal.to", "to") + ' ' + cr.range.end;
												}
											}
											content += ' ' + cr.range.uom.formattedSymbol;
											if (cr.range.relational)
											{
												content +=	' @ ' + cr.range.formattedRelatedPoint + ' ' + cr.range.relatedUom.formattedSymbol;
											}
							content += 	'</span><br />';
						}
						else
						{
							content += 	'<label class="bold calreqinstruct">' + i18n.t("core.tools:cal.calInstructions", "Calibration Instructions:") + '</label>' +
										'<div class="clear"></div>';
						}
						// has private instructions?
						if ((cr.privateInstructions) && (cr.privateInstructions != ''))
						{
							content += 	'<span class="attention bold">' + i18n.t("core.tools:cal.privateLabel", "Private:") + ' ' + cr.privateInstructions.replace(/\n/gi, "&nbsp;<br />") + '</span><br />';
						}
						// has public instructions?
						if ((cr.publicInstructions) && (cr.publicInstructions != ''))
						{
							content += 	'<span class="allgood bold">' + i18n.t("core.tools:cal.publicLabel", "Public:") + ' ' + cr.publicInstructions.replace(/\n/gi, "&nbsp;<br />") + '</span><br />';
						}
						content += 	'<span class="bold">';
										if (cr.publish)
										{
											content +=	i18n.t("core.tools:cal.published", "Published");
										}
										else
										{
											content +=	i18n.t("core.tools:cal.notpublished", "Not published");
										}
						content +=	'</span> - ' +
									'<span class="bold">' + cr.translatedSource + '</span>' +
									'<br />';
						// has last modified
						if (cr.lastModified && cr.lastModifiedBy)
						{				
							content +=	'<span class="smaller-text">' + i18n.t("core.tools:cal.lastModified", {varName : cr.lastModifiedBy.name, varDate : formatDate(cr.lastModified, 'dd.MM.yyyy - HH:mm:ss'), defaultValue : "Last modified by " + cr.lastModifiedBy.name + " on " + formatDate(cr.lastModified, 'dd.MM.yyyy - HH:mm:ss')}) + '</span>';
						}
						// float show history link
			content += 	'<div class="float-right">';
							// instrument page?
							if (page == 'instrument')
							{
								content +=	'<a href="#" class="mainlink" onclick=" getHistoryForInstrumentCalReq(this, ' + cr.id + ', ' + plantid + '); return false; ">' + i18n.t("core.tools:cal.showHistory", "Show History") + '</a>';
							}
							else if (page == 'jiheader')
							{
								content +=	'<a href="#" class="mainlink" onclick=" getHistoryForJobItemCalReq(this, ' + cr.id + ', ' + id + '); return false; ">' + i18n.t("core.tools:cal.showHistory", "Show History") + '</a>';
							}
			content += 	'</div>' +
					'</li>';
	// return the new calibration requirement content
	return content;
}

/**
 * this method gets the calibration requirement history for a specified instrument
 * 
 * @param {Object} link the link clicked to trigger this method
 * @param {Integer} currentCrId the current calibration requirement id
 * @param {Integer} plantid the id of the instrument we are retrieving calibration history for
 */
function getHistoryForInstrumentCalReq(link, currentCrId, plantid)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/calreqhistoryservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to deactivate the calibration requirement
			calreqhistoryservice.fetchCompleteCalReqHistoryForInstrument(plantid, 
			{
				callback: function(result) 
				{
					// calls method to create and display overlay
					displayHistoryInOverlay(link, currentCrId, result);
				}
			});
		}
	});
}

/**
 * this method gets the calibration requirement history for a specified job item
 * 
 * @param {Object} link the link clicked to trigger this method
 * @param {Integer} currentCrId the current calibration requirement id
 * @param {Integer} jobItemId the id of the job item we are retrieving calibration history for
 */
function getHistoryForJobItemCalReq(link, currentCrId, jobItemId)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/calreqhistoryservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to deactivate the calibration requirement
			calreqhistoryservice.fetchCompleteCalReqHistoryForJobItem(jobItemId, 
			{
				callback: function(result) 
				{
					// calls method to create and display overlay
					displayHistoryInOverlay(link, currentCrId, result);
				}
			});
		}
	});
}		
			
/**
 * this method displays the calibration history returned in an overlay and provides the functionality to load any
 * calibration history and view it in the same overlay window.
 * 
 * @param {Object} link the link clicked to trigger this method
 * @param {Integer} currentCrId the current calibration requirement id 
 * @param {Object} result result wrapper containing array of history objects
 */
function displayHistoryInOverlay(link, currentCrId, result)
{
	// retrieval of instrument history successful?
	if(result.success)
	{
		// assign history to variable
		var history = result.results;
		// create calibration history overlay content
		var content = 	'<div id="calReqsHistoryOverlay">' +
							'<table class="default4 calReqHistory" summary="This table shows calibration requirement history for an instrument">' +
								'<thead>' +
									'<tr>' +
										'<td colspan="5">&nbsp;</td>' +
									'</tr>' +
									'<tr>' +
										'<th class="changedon">' + i18n.t("core.tools:cal.changeOn", "Changed On") + '</th>' +
										'<th class="changedby">' + i18n.t("core.tools:cal.changeBy", "Changed By") + '</th>' +
										'<th class="activity">' + i18n.t("activity", "Activity") + '</th>' +
										'<th class="oldstate">' + i18n.t("core.tools:cal.oldState", "Old State") + '</th>' +
										'<th class="newstate">' + i18n.t("core.tools:cal.newState", "New State") + '</th>' +
									'</tr>' +
								'</thead>' +							
								'<tbody>';
									// history returned?
									if (history.length > 0)
									{
										$j.each(history, function(i, h)
										{
											content +=	'<tr>' + 
															'<td>' + cwms.dateTimeFormatFunction(h.time) + '</td>' +
															'<td>' + h.changeBy + '</td>' + 
															'<td>' + h.activity + '</td>' +
															getCellForCalReqIdComparison(currentCrId, h.fromId) +
															getCellForCalReqIdComparison(currentCrId, h.toId) +
														'</tr>';
															
											}
										);
									}
									else
									{
										content += 	'<tr>' +
														'<td colspan="5" class="text-center bold">' + i18n.t("core.tools:cal.noHistoryAvailable", "No history available") + '</td>' +
													'</tr>';
									}
					content +=	'</tbody>' +
							'</table>';
							if (history.length > 0)
							{
								content += '<p class="text-center bold">' + i18n.t("core.tools:cal.viewChanges", "Click on version above to view changes") + '</p>';
							}
				content +=	'</div>';
		// create overlay using content
		loadScript.createOverlay('dynamic', i18n.t("core.tools:cal.calHistory", "Calibration History"), null, content, null, 50, 740, null);
	}
	else
	{
		var errormessage = i18n.t("core.tools:cal.errRetrievingCalReqHistory", "Error while retrieving the calibration requirement history");
		// result wrapper has error message
		if (result.message != '')
		{
			// use message from result wrapper
			errormessage = result.message;
		}
		// display error message to user
		$j.prompt(errormessage);
	}
}

function getCellForCalReqIdComparison(currentCrId, compareToId)
{
	var str = '';
	if(compareToId <= 0)
	{
		str = '<td>' + i18n.t("core.tools:cal.none", "none") + '</td>';
	}
	else if(currentCrId == compareToId)
	{
		str = '<td>' + i18n.t("core.tools:cal.current", "current") + '</td>';
	}
	else
	{
		str = '<td><a href="#" class="mainlink" onclick=" fetchAndDisplayCalReq(' + compareToId + '); return false; ">ver ' + compareToId + '</a></td>';
	}
	return str;
}

/**
 * this method fetches a requested calibration requirement revision and displays it to the user in the overlay
 * 
 * @param {Integer} calReqId id of the calibration requirement
 */
function fetchAndDisplayCalReq(calReqId)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/calreqservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to find the calibration requirement
			calreqservice.ajaxFindCalReq(calReqId,
			{
				callback:function(calreq)
				{					
					// found successfully?
					if(calreq != null)
					{						
						// cal req already been displayed?
						if ($j('div#calReqsHistoryOverlay ol.calReqViewingArea').length)
						{
							// change content displayed in cal req viewing area
							$j('div#calReqsHistoryOverlay ol.calReqViewingArea').html(newCalReqContent(calreq, '', 0));
						}
						else
						{
							// create content to display this calibration requirement in overlay
							var content =	'<div class="padding">' +
												'<fieldset>' +
													'<ol class="calReqViewingArea">' +
														newCalReqContent(calreq, '', 0) +
													'</ol>' +
												'</fieldset>' +
											'</div>';
							// add new content to overlay
							$j('div#calReqsHistoryOverlay').find('p:last').remove().end().append(content);
						}
					}
				}
			});
		}
	});
}

function redirectToGroup(jobid){
	window.location.href = "accessoriesanddefects.htm?jobid="+jobid;
}