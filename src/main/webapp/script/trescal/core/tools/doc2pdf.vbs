'************************************************
'
' DOC2PDF.VBS 
'
'
' $Workfile: doc2pdf.vbs $
' $Author: $
' $Modtime: $
' $Archive: $
'
' $Revision: 1.1 $
'
' This script can create a PDF file from a Word document ' provided that you
' have Adobe Acrobat Distiller installed.
'

' Constants
Const WdPrintAllDocument = 0
Const WdDoNotSaveChanges = 0


' Global variables
Dim arguments
Set arguments = WScript.Arguments

' ***********************************************
' ECHOLOGO
'
' Outputs the logo information.
'
Function EchoLogo()
  If Not (arguments.Named.Exists("nologo") Or arguments.Named.Exists("n")) Then
    WScript.Echo "doc2pdf Version 1.0, Michael Suodenjoki 2003"
    WScript.Echo "=============================================================================="
    WScript.Echo ""
  End If
End Function

' ***********************************************
' ECHOUSAGE
'
' Outputs the usage information.
'
Function EchoUsage()
  If arguments.Count=0 Or arguments.Named.Exists("help") or arguments.Named.Exists("h") Then
    WScript.Echo "Generates a PDF file from a Word document using Adobe Distiller."
    WScript.Echo ""
    WScript.Echo "Usage: doc2pdf.vbs <options> <doc-file> [/o:<pdf-file>]"
    WScript.Echo ""
    WScript.Echo "Available Options:"
    WScript.Echo ""
    WScript.Echo " /nologo - Specifies that the logo shouldn't be displayed"
    WScript.Echo " /help - Specifies that this usage/help information should be displayed."
    WScript.Echo " /debug - Specifies that debug output should be displayed."
    WScript.Echo ""
    WScript.Echo "Parameters:"
    WScript.Echo ""
    WScript.Echo " /o:<pdf-file> Optionally specification of output file (PDF)."
    WScript.Echo " generate. E.g. dcdhelpconfig.xml"
    WScript.Echo ""
  End If 
End Function

' ***********************************************
' CHECKARGS
'
' Makes some preliminary checks of the arguments.
' Quits the application is any problem is found.
'


Function CheckArgs()
  ' Check that <doc-file> is specified
  If arguments.Unnamed.Count <> 1 Then
    ' Make sure that the log is clear
	'WScript.Echo "Error: Obligatory <doc-file> parameter missing!"
	Unlock()
    ' Let the java controller know that an error occured and to clean up any mess
      	WScript.Quit 1
  End If

  bShowDebug = arguments.Named.Exists("debug") Or arguments.Named.Exists("d")

End Function


' ***********************************************
' DOC2PDF
'
' Converts a Word document to PDF using Adobe
' Distiller.
'
' Input:
' sDocFile - Full path to Word document.
' sPDFFile - Optional full path to output file.
'
' If not specified the output PDF file
' will be the same as the sDocFile except
' file extension will be .pdf.
'
	
Dim logFile
logFile = False
Dim attempt
attempt = 0

	'******************
	'MAIN STARTS HERE
	' first check for the log file, if it is available then record then record that a new pdf session is beginning
	
	On Error resume next
	
	' record a log entry that doc2pdf is starting
	writetoCWMSlog "DOC2PDF", "Begin doc2pdf", "", "true" 
	
	ReadLog()
	while not logFile
		
		' The logfile is locked by another process so wait 2 seconds and try again
		attempt = attempt + 1
		WScript.Sleep 1000
	    
		' Untab to write out log of number of times the loop runs before adobe writer is freed up again
		'Const ForReading = 1, ForWriting = 2, ForAppending = 8
		'Dim fso, f
		'Set fso = CreateObject("Scripting.FileSystemObject")
		'Set f = fso.OpenTextFile("c:\out.txt", ForAppending, True)
		'f.WriteLine " " & attempt
		'f.Close
		
		'interate this for 15 seconds and if there is still no available pdf conversion give up
		if(attempt > 15) then
			writetoCWMSlog "DOC2PDF", "PDF-ing Locked", "ACTION INCOMPLETE", ""
			WScript.Quit 1
		end if
		ReadLog()
	wend
	
	if logFile then 'logfile was empty or did not exist so we can make use of the word documents
		' lock the logfile 
		WriteToLog() 
		
		' Convert the pdf
		DoPDF()	     
		
		'unlock the for the next process
		Unlock()    
	
	end if
	
	If Err.Number <> 0 Then
		' Make sure that the log is clear
		Unlock()
		' Let the java controller know that an error occured and to clean up any mess
		writetoCWMSlog "DOC2PDF", "", "ERROR - caught", "" 
			WScript.Quit 1
      	Err.Clear
	Else
		' No errors occured so exit gracefully
		writetoCWMSlog "DOC2PDF", "", "ACTION COMPLETE", "" 
		WScript.Quit 0
	End If
	
	


' read from the vblog - return true if empty and therefore are able to write, false if another process is currently writing
Sub ReadLog
	Dim objFSO
	Dim objFile
	Dim objReadFile
	Dim strContents
	Dim FileExist 
	
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	
	FileExist = objFSO.fileexists("C:\CWMSLogs\VBLog.txt")
	
	
	if FileExist Then
		
		Set objFile = objFSO.GetFile("C:\CWMSLogs\VBLog.txt")
		
		If objFile.Size > 0 Then 'Empty files cause error because vb is shit
			
			'Wscript.Echo "Reading file the first time:"
			Set objReadFile = objFSO.OpenTextFile("C:\CWMSLogs\VBLog.txt", 1) 'Prepare the file to be read
			strContents = objReadFile.ReadAll 'Read the entire file
			objReadFile.Close
		end if
		
		'Wscript.Echo strContents

		If strContents <> "" Then
			'Wscript.Echo "File has contents"
			logFile = false
		else
			'Wscript.Echo "File was empty"
			logFile = true
		end if
		

	else
		Set objFile = objFSO.CreateTextFile("C:\CWMSLogs\VBLog.txt")
		logFile = true
	End if

End Sub


' write to the vblog
Sub WriteToLog
	Dim objFSO
	Dim objFile
	
	Const ForWriting = 2 'this will overwrite the existing contents of the file
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	Set objFile = objFSO.OpenTextFile("C:\CWMSLogs\VBLog.txt", ForWriting)
	objFile.Write ("Locked ")
	objFile.Write Now
	objFile.Close

End Sub

' unlock the log file
Sub Unlock
	Dim objFSO
	Dim objFile
	Dim delfile
	
	dim filesys, demofile
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	Set objFile = objFSO.CreateTextFile ("C:\CWMSLogs\VBLog.txt", true)
End Sub	

	
Function DOC2PDF( sDocFile, sPDFFile )


	' first check that the doc2pdf function is not in use elsewhere
	' any active instance of doc2pdf will record its active status in a log file
	  
	  Dim fso ' As FileSystemObject
	  Dim wdo ' As Word.Application
	  Dim wdoc ' As Word.Document
	  Dim wdocs ' As Word.Documents
	  Dim sPrevPrinter ' As String
	  Dim oDistiller ' As PDFDistiller.PDFDistiller.1
	
	  Set oDistiller = CreateObject("PDFDistiller.PDFDistiller.1")
	  If oDistiller Is Nothing Then
		'WScript.Echo "Error: Cannot create PDF document. Adobe Acrobat Distiller is not available! Quiting..."
	    WScript.Quit 1
	  End If
	
	  Set fso = CreateObject("Scripting.FileSystemObject")
	  Set wdo = CreateObject("Word.Application")
	  Set wdocs = wdo.Documents
	
	  sTempFile = "C:\temp\" + fso.GetTempName()
	
	  sDocFile = fso.GetAbsolutePathName(sDocFile)
	
	  ' Debug outputs...
	  If bShowDebug Then
			'WScript.Echo "Doc file = '" + sDocFile + "'"
			'WScript.Echo "Temporary file = '" + sTempFile + "'"
			'WScript.Echo "PDF file = '" + sPDFFile + "'"
	  End If
	
	  sFolder = fso.GetParentFolderName(sDocFile)
	
	  If Len(sPDFFile)=0 Then
	    sPDFFile = fso.GetBaseName(sDocFile) + ".pdf"
	  End If
	
	  If Len(fso.GetParentFolderName(sPDFFile))=0 Then
	    sPDFFile = sFolder + "\" + sPDFFile
	  End If
	
	  ' Remember current active printer
	  sPrevPrinter = wdo.ActivePrinter
	
	  'wdo.ActivePrinter = "Acrobat PDFWriter"
	  wdo.ActivePrinter = "Adobe PDF"  'RICHARD and JAMIE and ANTYARMS06
	  'wdo.ActivePrinter = "Acrobat Distiller" 'ANTYARDC01 and ANTYARDB03
	
	  ' Open the Word document
	  Set wdoc = wdocs.Open(sDocFile)
	
	  ' Print the Word document to the Acrobat Distiller - will generate a postscript (.ps) (temporary) file
	  wdo.ActiveDocument.PrintOut False , , , sTempFile
	
	  ' This outcommented part was used while trying to use "Acrobat PDFWriter"
	  'Do While wdo.BackgroundPrintingStatus > 0
	  ' 'Do nothing - just wait for printing to finish before closing Word
	  'Loop
	
	  wdoc.Close WdDoNotSaveChanges
	  wdo.ActivePrinter = sPrevPrinter
	  wdo.Quit WdDoNotSaveChanges
	  Set wdo = Nothing
	
	
	  ' Debug output...
	  'If bShowDebug Then
	  'WScript.Echo " Distilling to '" + sPDFFile + "'"
	  'End If
	
	  ' Distill the postscript file to PDF
	  oDistiller.FileToPDF sTempFile, sPDFFile, "Print"
	  Set oDistiller = Nothing
	
	  ' Delete the temporary postscript file...
	  fso.DeleteFile( sTempFile )
	
	  Set fso = Nothing

End Function

' *** MAIN **************************************
Function DoPDF

	'Call EchoLogo()
	'Call EchoUsage()
	Call CheckArgs()
	
	writetoCWMSlog "DOC2PDF", arguments.Unnamed.Item(0), "", "" 

	Call DOC2PDF( arguments.Unnamed.Item(0), arguments.Named.Item("o") )
	
End Function
	
Set arguments = Nothing
	

	
' Function to record all vbscript / wscript actions carried out on server in log files
	Sub writetoCWMSlog(arg1, arg2, arg3, writeline)
	
	Dim objFSO
	Dim objFile
	Dim objReadFile
	Dim writeNewLine
	
	Dim text 
	Dim text2
	Dim func
	
	func = arg1
	text = arg2
	text2 = arg3
	writeNewLine = writeline
	
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	'Set objFile = objFSO.GetFile("C:\CWMSLogs\CWMS logs_logos.txt")
	Set objFile = objFSO.GetFile("C:\CWMSLogs\CWMS Script Log.txt")
	
	Set objReadFile = objFSO.OpenTextFile(objFile , 8)
	
	' insert a blank line as this is a new logging topic
	if writeNewLine = "true" then
		objReadFile.WriteLine ""
	end if
	
	objReadFile.WriteLine func & " :  " & Date & " " & Time & " " & text & " " & text2
	objReadFile.Close

	Set objFSO = Nothing
	Set objFile = Nothing
	Set objReadFile = Nothing

End Sub