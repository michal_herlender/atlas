/**
 * array of calendar objects containing the initial setup literals needed
 * 
 * @param {String} inputField is the id of the input field to which the calendar should be binded
 * @param {String} dateFormat is the format that the date should be displayed
 * @param {String} displayDates 'all' make all dates available
 * 								'future' show only dates in the future
 * 								'past' show only dates in the past
 * applied in init() function
 */
var jqCalendar = new Array 	({ 
							 	inputField: 'hiredate',
							 	dateFormat: 'dd.mm.yy',
							 	displayDates: 'all'
							 },
							 { 
							 	inputField: 'patdate',
							 	dateFormat: 'dd.mm.yy',
							 	displayDates: 'all'
							 });

function testDownloadNonDataBasedLabel(button, template)
{
	var qty = $j(button).parent('li').find('select').val();
	template = template.substring(5);
	document.location.replace("nondatabasedlabel?quantity="+qty+"&templateKey="+template);
}

function testDownloadStringDateLabel(button, template, serial, date)
{
	template = template.substring(5);
	document.location.replace("stringdatelabel?serial="+serial+"&templateKey="+template+"&date="+date);
}

function testDownloadPATLabel(anchor, template, plantid,serialno, date)
{	
	template = template.substring(5);
	if (plantid == '')
	{
		plantid = 123456;
	}
	document.location.replace("patlabel?plantId="+plantid+"&templateKey="+template+"&serial="+serialno+"&date="+date);
}

function test_Alligator_PrintNonDataBasedLabel(button, template, printerName)
{
	var qty = $j(button).parent('li').find('select').val();
	template = template.substring(5);
	// Using Alligator
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("POST", "http://localhost:58058/service/printLabel", false);
    xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlHttp.send('{"quantity":"' + qty + '","templateKey":"' + template + '","request":"nondatabasedlabel"' + ',"printerName": "' + printerName +'"}');
}

/**
 * Print Label
 * @param button
 * @param template
 * @param serial - actually the "String" to be printed, only "serial" supported by Alligator
 * @param date
 * @param printerName
 */
function test_Alligator_PrintStringDateLabel(button, template, serial, date, printerName)
{
	var dt = "";
	dt = date;
	template = template.substring(5);
	// Using Alligator
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("POST", "http://localhost:58058/service/printLabel", false);
    xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlHttp.send('{"serial":"' + serial + '","date":"' + date + '","templateKey":"' + template + '","request":"stringdatelabel"' + ',"printerName": "' + printerName +'"}');
}

function test_Alligator_PrintInstDateLabel(anchor, template, plantid, serialno, date, printerName)
{
	var dt = "";
	dt = date;
	template = template.substring(5);
	// Using Alligator
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("POST", "http://localhost:58058/service/printLabel", false);
    xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlHttp.send('{"plantId":"' + plantid + '","serial":"' + serialno + '","date":"' + date + '","templateKey":"' + template + '","request":"patlabel"' + ',"printerName": "' + printerName +'"}');
}

function testPrint(payload) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("POST", "http://localhost:58058/service/print", false);
    xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlHttp.send(payload);
}