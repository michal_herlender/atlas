/****************************************************************************************************************************************
*
*	FILENAME		:	DymoLabelFunctions.js
*	DESCRIPTION		:	Functionality for printing to Dymo Label Printer
*                       All Dymo printing code moved here to eliminate duplication
*	DEPENDENCIES	:   - 
*
*	TO-DO			: 	-						
*	KNOWN ISSUES	:	- DIV for certificate options is duplicated in DOM so selection by ids is unreliable!
*	HISTORY			:	2016-05-10 - GB - Created File 
*                       2016-11-21 - GB - Added functionality to print using Dymo javascript library
*                       (also retaining old IE functionality for now) - new methods use printDymo... calls
****************************************************************************************************************************************/

function printDymoCertLabel(certLinkId) {
	// TODO comment is not working correctly due to cert options being a duplicated div;
	// plan to move cert options to a JSP to fix
	var comment = $j('textarea#envelopeComment-' + certLinkId).text();
	var encodedComment = encodeURIComponent(comment);
	printLabelsFromJsonGet("dymocertificate.json?certlinkid="+certLinkId+"&freetext="+encodedComment);
}

function printDymoHireLabel(hireid) {
	printLabelsFromJsonGet("dymohirefolder.json?hireid="+hireid);
}

function printDymoDeliveryLabel(deliveryid) {
	printLabelsFromJsonGet("dymodelivery.json?deliveryid="+deliveryid);
}

function printDymoJobLabel(jobid) {
	printLabelsFromJsonGet("dymojobfolder.json?jobid="+jobid);
}

function printDymoJobItemAccessoryLabel(jobitemid) {
	printLabelsFromJsonGet("dymojobitemaccessory.json?jobitemid="+jobitemid);
}

function printDymoJobItemLabel(jobitemid,printJobItemCount) {
	var printJobItemCountParam;
	printJobItemCountParam = printJobItemCount !== false; // default
	printLabelsFromJsonGet("dymojobitem.json?jobitemid=" + jobitemid + "&printJobItemCount=" + printJobItemCountParam);
}

function printDymoSelectedJobItemLabel(jobitemids,aFunc) {
	
	var jobItemIdsParamList = "";
	//convert list to valid get params
	$j.each(jobitemids, function(index, value) {
		jobItemIdsParamList +="jobItemIds="+value+"&";
	});
	printLabelsFromJsonGetThenDoSomthing("dymoselectedjobitem.json?"+jobItemIdsParamList,aFunc);
}

function printDymoAllJobItemLabels(jobid) {
	printLabelsFromJsonGet("dymoalljobitem.json?jobid="+jobid);
}

function printDymoAllJobLabels(jobid) {
	printLabelsFromJsonGet("dymoalljob.json?jobid="+jobid);
}

/*
 * Generic method for use when label data can be obtained from single GET
 */
function printLabelsFromJsonGet(jsonLabelUrl) {
	$j.ajax({url:jsonLabelUrl, dataType:"json"}).done(printDymoLabels);
}

function printLabelsFromJsonGetThenDoSomthing(jsonLabelUrl,aFunc) {
	$j.ajax({url:jsonLabelUrl, dataType:"json",success:printDymoLabels}).done(aFunc);
}

/*
 * Function that posts a form and uses the result (Array of JSON objects)
 * to print job item and accessory labels, this requires the presence of the form
 * (e.g. built via DymoLabelJobItemController) 
 */
function printDymoJobItemLabels() {
	var postData = $j('#jobItemLabelsForm').serializeArray();
	var formUrl = $j('#jobItemLabelsForm').attr('action');
//	contentType: "application/x-www-form-urlencoded; charset=UTF-8", 
//	headers: {accept:"application/json;charset=UTF-8,text/html"}, 
	$j.ajax({
		url:formUrl, 
		type:"POST", 
		data:postData, 
		dataType:"json"
	}).done(printDymoLabels);
}

/*
 * Function that takes array of JSON label data to print, then uses Dymo javascript framework to print
 * Each XML file is retrieved as printing occurs; this allows variation in files (e.g. job item, then accessory)
 */  
function printDymoLabels(jsonLabelArray, textStatus, jqXHR, callback) {
	loadScript.loadScriptsDynamic(new Array('script/thirdparty/misc/DYMO.Label.Framework.3.0.js'), true, 
	{
		callback: function()
		{
			dymo.label.framework.init(function()
			{
				var checkEnvironmentResult = dymo.label.framework.checkEnvironment();
				if (checkEnvironmentResult.isWebServicePresent) {
					var printers = dymo.label.framework.getPrinters();
					if (printers != null) {
						if (printers.length > 0) {
							var printerName = null;
							// Search for and use first connected printer; 
							for (var printer of printers) {
								if (printer.isConnected) {
									printerName = printer.name;
									break;
								}
							}
							if (printerName != null) {
								console.log('printerName: '+printerName);
								$j.each(jsonLabelArray, function (jsonArrayIndex, jsonLabelData) {
									var xmlLabelTemplateUrl = jsonLabelData.labelTemplateUrl;
									$j.get(xmlLabelTemplateUrl).done(function(labelXml)
									{
										var paramsXml = dymo.label.framework.createLabelWriterPrintParamsXml ({ copies: jsonLabelData.copies });
										var labelSet = new dymo.label.framework.LabelSetBuilder();
										var record = labelSet.addRecord();
										// Iterates over each of the keys and values in response, add to labelSet
										// This approach was chosen as being easier / less coupled than 
										// generating / returning XML from the server
										// Note copies and labelTemplateUrl skipped in labelSetXml
										$j.each(jsonLabelData, function( key, value ){
											if ((key != 'copies') && (key != 'labelTemplateUrl')) {
												record.setText(key,value);
											}
										})
										var labelSetXml = labelSet.toString();
										dymo.label.framework.printLabel(printerName, paramsXml, labelXml, labelSetXml);
										if(callback !== undefined)
											callback();
									});
								});
							}
							else {
								alert("Error: 0 printers were connected out of "+printers.length+" Dymo printer(s)");
								if(callback !== undefined)
									callback();
							}
						}
						else {
							alert("Error: 0 Dymo printers available");
							if(callback !== undefined)
								callback();
						}
					}
					else {
						alert("Error: no Dymo printer information available");
						if(callback !== undefined)
							callback();
					}
				}
				else {
					alert("Error: Dymo web service not installed");
					if(callback !== undefined)
						callback();
				}
			});
		}
	});
}