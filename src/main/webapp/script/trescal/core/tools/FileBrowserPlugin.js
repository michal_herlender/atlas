/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	FileBrowserPlugin.js
*	DESCRIPTION		:	Initiates the service and defines functions used in the file browser plugin.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	
*						
*	KNOWN ISSUES	:	-
*	HISTORY			:	20/02/2008 - JV - Created File
*					:	22/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * this method gets all files for a directory and lists them in a file tree
 * 
 * @param {Object} anchor the anchor which has been clicked
 * @param {String} directory the current directory
 * @param {Integer} scid the system component id
 * @param {Integer} entityid the id of entity we are getting files for
 * @param {Integer} personid the id of the current user
 * @param {Boolean} allowEmail indicates if emailing is allowed
 * @param {Boolean} isEmailPlugin indicates if this is an email plugin
 * @param {Boolean} web indicates if this is a call from the client interface
 */
function getFilesForDirectory(anchor, directory, scid, entityid, personid, allowEmail, isEmailPlugin, web)
{
	// list item has class directory?
	if( $j(anchor).parent().hasClass('directory'))
	{
		// list item has class collapsed?
		if( $j(anchor).parent().hasClass('collapsed'))
		{
			// clean up existing list of files
			$j(anchor).parent().find('ul').remove();
			// add waiting image class
			$j(anchor).parent().addClass('wait');
			// load the service javascript file
			loadScript.loadScriptsDynamic(new Array('dwr/interface/filebrowserservice.js'), true,
			{
				callback: function()
				{
					// call dwr service to get all files in directory
					filebrowserservice.getFilesForDirectory(directory, scid, entityid,
					{
						callback:function(results)
						{
							// dwr call successful?
							if (results.success == true) 
							{
								var fileoffset = "";
								if(typeof web !== "undefined" && web == true) {fileoffset = "../";}
								
								// assign object
								var files = results.results;
								// files returned?
								if (files.length > 0)
								{
									// create list content
									var content = 	'<ul class="fileTree" style=" display: none; ">';
														$j.each(files, function(i)
														{
															// check file is not thumbs.db
															if (files[i].fileName != 'Thumbs.db')
															{
																var encodedFilePath = encodeURI(files[i].filePath);
																var encodedFileName = encodeURI(files[i].fileName);
																// is this a directory?
																if(files[i].directory)
																{	
																	var escapedSubdirName = directory != null ? directory + '/' + encodedFileName : encodedFileName; 
																	content += '<li class="directory collapsed">' +
																		'<a href="#" class="underline" ' +
																			'onclick="event.preventDefault(); ' +
																				'getFilesForDirectory(this, \'' + escapedSubdirName + '\', ' + scid + ', ' + entityid + ', ' + personid + ', ' + allowEmail + ' , ' + isEmailPlugin + ' , ' + web + '); " ' +
																			'title="' + i18n.t("core.tools:file.lastModified", {varLastModif : files[i].lastModified, defaultValue : "(last modified: " + files[i].lastModified + ")"}) + '">';
																}												 
																else
																{
																	// is this email plugin?
																	if (isEmailPlugin)
																	{
																		content += 	'<li class="file ext_' + files[i].extension + '">';
																					// attachments on email
																					if ($j('input[name="attachments"]').length)
																					{
																						// variable to indicate match
																						var match = false;
																						// loop through attachments
																						$j('input[name="attachments"]').each(function(x, n)
																						{
																							// current attachment and file match
																							if (n.value == files[i].filePath)
																							{
																								// set match to true
																								match = true;
																								// exit
																								return;
																							}																				
																						});
																						// no match
																						if (!match)
																						{
																							// add link to add as attachment
																							content +=	'<a href="#" onclick=" event.preventDefault(); addToComponentAttachments(this, \'' + files[i].filePathEncrypted + '\', \'' + encodedFileName + '\'); ">' +
																											'<img src="' + fileoffset +'img/icons/email_attach.png" width="16" height="16" class="img_marg_bot" alt="' + i18n.t("core.tools:file.attachTisDocument", "Attach This Document") + '" title="' + i18n.t("core.tools:file.attachTisDocument", "Attach This Document") + '" />' +
																										'</a>';
																						}																																				
																					}
																					else
																					{
																						// no attachments so add link to add as attachment
																						content +=	'<a href="#" onclick=" event.preventDefault(); addToComponentAttachments(this, \'' + files[i].filePathEncrypted + '\', \'' + encodedFileName + '\'); ">' +
																										'<img src="' + fileoffset +'img/icons/email_attach.png" width="16" height="16" class="img_marg_bot" alt="' + i18n.t("core.tools:file.attachTisDocument", "Attach This Document") + '" title="' + i18n.t("core.tools:file.attachTisDocument", "Attach This Document") + '" />' +
																									'</a>';
																					}																		
																					content += '<a href="downloadfile.htm?file=' + files[i].filePathEncrypted + '" target="_blank" class="underline" title="' + i18n.t("core.tools:file.lastModified", {varLastModif : files[i].formattedDate, defaultValue : "(last modified: " + files[i].formattedDate + ")"}) + '">';
																	}
																	else
																	{
																		// extension of current file (html or htm)?
																		if ((files[i].extension == 'html') || (files[i].extension == 'htm'))
																		{
																			content += '<li class="file ext_' + files[i].extension + '">';
																		}
																		else
																		{
																			content += '<li class="file ext_' + files[i].extension + '"><a href="downloadfile.htm?file=' + files[i].filePathEncrypted + '" class="underline" target="_blank" title="' + i18n.t("core.tools:file.lastModified", {varLastModif : files[i].formattedDate, defaultValue : "(last modified: " + files[i].formattedDate + ")"}) + '">';
																		}																		
																	}
																}										
																// extension of current file (html or htm)?
																if ((files[i].extension == 'html') || (files[i].extension == 'htm'))
																{
																	content += '&nbsp;<span>' + files[i].fileName + '</span>&nbsp;';
																}
																else
																{
																	content += '&nbsp;<span style=" color: ' + files[i].highlightColour + '; ">' + files[i].fileName + '</span></a>&nbsp;';
																}	
																// is this directory?												
																if(!files[i].directory)
																{													
																	content +=  '<span>';
																	if(files[i].filePath.indexOf("Certificates") == -1)
																	{
																		content += '<a href="#" onclick=" event.preventDefault(); deleteFile(this, \'' + files[i].fileNameEncrypted + '\', \'' + directory + '\', \'' + scid + '\', \'' + entityid + '\' ); ">' +
																			'<img src="' + fileoffset +'img/icons/delete.png" width="16" height="16" class="img_marg_bot" alt="' + i18n.t("core.tools:file.deleteThisFile", "Delete this file") + '" title="' + i18n.t("core.tools:file.deleteThisFile", "Delete this file") + '" />' +
																		'</a>&nbsp;';
																	}
																	// email allowed and not an email plugin
																	if ((allowEmail) && (files[i].extension != 'html') && (files[i].extension != 'htm') && (files[i].fileName.indexOf('Job Sheet') == -1))
																	{
																		// build string for url
																		var emailUrl = 'createemail.htm?syscompid=' + scid + '&objid=' + entityid + '&attach=' + files[i].filePathEncrypted;
																		// replace all single backslash with double backslash and spaces with plus symbol and colon with encoded
																		content += '<a href="#" onclick=" event.preventDefault(); loadScript.createOverlay(\'external\', \'' + i18n.t("core.tools:file.createEmail", "Create Email") + '\', \'' + emailUrl + '\', null, null, 64, 860, null); " class="fileutility">' +
																						'<img src="' + fileoffset +'img/icons/email.png" width="16" height="16" class="img_inl_margleft" alt="' + i18n.t("core.tools:file.sendAsEmailAttachment", "Send as email attachment") + '" title="' + i18n.t("core.tools:file.sendAsEmailAttachment", "Send as email attachment") + '" />' +
																					'</a>&nbsp;';	
																	}
																	content += '</span>';													
																}												
																content +=	'</li>';
															}
														});
									content += '</ul>';		
									// remove the waiting image class and append content
									$j(anchor).parent().removeClass('wait').append(content);
									// add new expanded class and use slide down effect
									$j(anchor).parent().removeClass('collapsed').addClass('expanded').find('ul:hidden').slideDown({ duration: 500 });
								}
								else
								{
									// remove the waiting image class and append content
									$j(anchor).parent().removeClass('wait').append('<ul class="fileTree" style=" display: none; "><li class="collapsed">-- ' + i18n.t("core.tools:file.emtpy", "Empty") + ' --</li></ul>');
									// add new expanded class and use slide down effect
									$j(anchor).parent().removeClass('collapsed').addClass('expanded').find('ul:hidden').slideDown({ duration: 500 });
								}										
							}
							else
							{
								// alert failure message
								$j.prompt(results.message);
							}
						}
					});
				}
			});			
		}
		else
		{
			// use slide up effect to hide files
			$j(anchor).parent().find('ul').slideUp({ duration: 500 });
			// remove expanded class and add collapsed class
			$j(anchor).parent().removeClass('expanded').addClass('collapsed');
		}
	}
	else
	{
		// do nothing
	}
}

/**
 * this method deletes a file from the file tree
 * 
 * @param {Object} anchor the anchor pressed
 * @param {String} fileName the name of the file to be deleted - encrypted and URL encoded
 * @param {String} subdirectoryName of the file to be deleted, if any
 * @param {Integer} scid the system component id
 * @param {Integer} entityid the id of entity we are getting files for
 */
function deleteFile(anchor, fileName, subdirectoryName, scid, entityid)
{
	$j.ajax({
		url: "deletefile.json",
		data: {
			fileName : fileName,
			subdirectoryName: subdirectoryName,
			scid: scid,
			entityid: entityid
		},
		async: true
	}).done(function(deleted) {
		// file deleted?
		if(deleted)
		{
			// get the root of file list
			var list = $j(anchor).parent().parent().parent();
			// remove the deleted file list item
			$j(anchor).parent().parent().remove();
			// any files left in list?
			if($j(list).find('li').length < 1)
			{
				// append empty list item
				$j(list).append('<li>-- ' + i18n.t("core.tools:file.empty", "Empty") + ' --</li>');
			}
			// file count present
			if ($j('span.fileResourceCount').length)
			{
				// update the file count
				$j('span.fileResourceCount').text(parseInt($j('span.fileResourceCount:first').text()) - 1);
			}
		}
		else
		{
			// alert failure message
			$j.prompt(i18n.t("core.tools:file.fileDoesNotExist", {varFilePath : fileName, defaultValue : "The file " + fileName + " does not exist or is a directory and cannot be deleted."}));
		}
	});
}

/**
 * this method adds a component attachment to an email
 * 
 * @param {Object} anchor the anchor pressed
 * @param {String} filePathEncrypted the path to file we are adding as attachment
 * @param {String} fileName the name of the file we are adding
 */
function addToComponentAttachments(anchor, filePathEncrypted, fileName)
{
	// remove link to attach file
	$j(anchor).remove();
	// attachments span hidden?
	if ($j('span#attachments').hasClass('hid'))
	{
		$j('span#attachments').removeClass();
	}
	var linebreak = '';
	// attachments already on email?
	if ($j('span#attachments input').length)
	{
		linebreak = '<br />';
	}
	// add file as attachment on email
	$j('span#attachments').append(linebreak + '<input type="checkbox" name="attachments" value="' + filePathEncrypted + '" checked="checked" /> ' + decodeURI(fileName));
	
}

/**
 * this method is called after a file has been successfully uploaded to the file
 * system. @see {upload.js}.
 */
function afterUpload()
{
	$j('a#fbRootAnchor').parent().removeClass().addClass('directory collapsed').end().trigger('onclick');
}