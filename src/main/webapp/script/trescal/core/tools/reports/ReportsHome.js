/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	jamiev
*
*	FILENAME		:	Reports.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	08/06/2010 - SH - Created File
*					:	22/10/2015 - TProvost - Manage i18n
*					:	2019-06-24 - Galen Beck - Converted last DWR reports to XlsxStreaming 
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	);

 /**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{
}