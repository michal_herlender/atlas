' VBScript program "CertSigner"
' 
' Used to open Microsoft Word as the chosen certificate
' and add the appropriate signature to the document
'
' Created by: Ross Tailby
' Date: 4/6/04
' Last modified: 20/10/08 (Jamie Vinall)

Dim arguments
Set arguments = WScript.Arguments

Function CheckArgs()
If arguments.Unnamed.Count <> 2 Then
	'MsgBox "No arguments were found!!"
End If
End Function

Call CheckArgs()

Dim certno ' As String
Dim user ' As String
Dim pwd ' As String
Dim docType ' As String
certno = arguments.Unnamed.Item(0)
docType = arguments.Unnamed.Item(1)

writetoCWMSlog "CERTSIGNER", certno, "", "true" 	
	
' Open an instance of Word and this certificate
Dim MsW ' As New Word.Application
Set MsW = WScript.CreateObject("Word.Application")
WScript.ConnectObject MsW, "MsW_"
Msw.Visible = False

Msw.Documents.Open certno

' Run the certificate signing Word Macro
If docType = "word" Then
	writetoCWMSlog "CERTSIGNER", certno, "word", ""
	MsW.Run("Normal.CWMS.FillInForm")
ElseIf docType = "metcal" Then
	writetoCWMSlog "CERTSIGNER", certno, "metcal", "" 
	MsW.Run("Normal.CWMS.signMetcal")
ElseIf docType = "metrosoft" Then
	writetoCWMSlog "CERTSIGNER", certno, "metrosoft", ""
	MsW.Run("Normal.CWMS.signMetrosoft")
Else
	MsW.Application.Quit
	WScript.Quit(1)
End If

MsW.Application.Quit

	If Err.Number <> 0 Then
		' Let the java controller know that an error occured and to clean up any mess
			writetoCWMSlog "CERTSIGNER", certno, "ERROR - caught", "" 
		Err.Clear
		WScript.Quit 1
      	Else
		' No errors occured so exit gracefully
		writetoCWMSlog "CERTSIGNER", certno, "ACTION COMPLETE", "" 
		WScript.Quit 0
	End If	
	
	
' Sub to record all vbscript / wscript actions carried out on server in log files
Sub writetoCWMSlog(arg1, arg2, arg3, writeline)
	
	Dim objFSO
	Dim objFile
	Dim objReadFile
	Dim writeNewLine
	
	Dim text 
	Dim text2
	Dim func
	
	func = arg1
	text = arg2
	text2 = arg3
	writeNewLine = writeline
	
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	Set objFile = objFSO.GetFile("C:\CWMSLogs\CWMS Script Log.txt")
	
	Set objReadFile = objFSO.OpenTextFile(objFile , 8)
	
	' insert a blank line as this is a new logging topic
	if writeNewLine = "true" then
		objReadFile.WriteLine ""
	end if
	
	objReadFile.WriteLine func & " :  " & Date & " " & Time & " " & text & " " & text2
	objReadFile.Close

	Set objFSO = Nothing
	Set objFile = Nothing
	Set objReadFile = Nothing

End Sub