/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	LocationChangerPlugin.js
*	DESCRIPTION		:	Initiates the job item service (if it is not already initialised) and defines functions used in the 
						location changer plugin.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	
*						
*	KNOWN ISSUES	:	-
*	HISTORY			:	11/03/2008 - JV - Created File
*					:	22/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

loadScript.loadScripts(new Array('dwr/interface/jobitemservice.js'));

function getCurrentLocation(locBc)
{
	jobitemservice.findEagerJobItemFromBarcode(locBc,
	{
		callback:function(ji)
		{
			if(ji == null)
			{
				$j('#locationchanger input#hiddenbc').val('');
				$j('#locationchanger div').hide();
				$j.prompt(i18n.t("core.tools:noFindJobItemFromInstrBarcode", "There are no active job items matching the instrument scanned."));
			}
			else
			{
				$j('#locationchanger span#instdetails').text(ji.inst.model.mfr.name + ' ' + ji.inst.model.model + ' ' + ji.inst.model.description.description);
				$j('#locationchanger span#currentaddr').text(ji.currentAddr.addr1 + ', ' + ji.currentAddr.town);
				if (ji.currentLoc)
				{
					$j('#locationchanger span#currentloc').text(ji.currentLoc.location);
				}
				$j('#locationchanger input#hiddenbc').val(locBc);
				$j('#locationchanger div').show();
			}
		}	
	});
}

function changeLocation()
{
	jobitemservice.changeItemLocation($j('#locationchanger input#hiddenbc').val(), $j('#locationchanger select#userlocations').val(),
	{
		callback:function()
		{
			$j('#locationchanger input#locationbc').val('');
			$j('#locationchanger input#hiddenbc').val('');
			$j('#locationchanger div').hide();
		}
	});
}
