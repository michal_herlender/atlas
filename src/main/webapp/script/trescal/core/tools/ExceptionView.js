
function showStackTrace(id)
{
	var element = document.getElementById(id);
	element.className = 'vis';

	var link = document.getElementById(id+'link');
	link.innerHTML = i18n.t("hideDetails", "Hide details");
	link.setAttribute('onclick','hideStackTrace('+id+')')
}

function hideStackTrace(id)
{
	var element = document.getElementById(id);
	element.className = 'hid';

	var link = document.getElementById(id+'link');
	link.innerHTML = i18n.t("showDetails", "Show details");
	link.setAttribute('onclick','showStackTrace('+id+')')
}

//no goddamn clipboard function seems to work
function copyText(id)
{
	var element = document.getElementById(id);
	var text = element.innerHTML;
	window.clipboardData.setData('Text',text);
}

function exceptionAjax(id)
{
	var comment = document.getElementById(id+'comment').value;
	var reviewed = document.getElementById(id+'reviewed').checked;

	exceptionservice.changeException(id, comment, reviewed);

	if(reviewed==false)
	{
		document.getElementById(id+'row').setAttribute('bgcolor','#FFBA44');
	}
	else
	{
		document.getElementById(id+'row').setAttribute('bgcolor','');
	}
}

