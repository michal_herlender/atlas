/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	ScanItemInOutPlugin.js
*	DESCRIPTION		:	Initiates the job item service (if it is not already initialised) and defines functions used in the 
						item scanner plugin.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	11/08/2008 - JV - Created File
*					:	22/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (
	'dwr/interface/deliveryservice.js', 
	'script/trescal/core/utilities/DOMUtils.js');

function despatch(deliveryNoOrBarcode)
{
	deliveryservice.despatchDeliveryOrItem(deliveryNoOrBarcode, 
	{
		callback:function(resWrap)
		{
			if(resWrap.success == false )
			{
				$j('div#scanItemsInOut').prev().removeClass('hid').find('div.warningBox3').html('<div>' + resWrap.message + '</div>');
				$j('div#scanOutHistory').prepend('<div><img src="img/icons/redcross.png" width="16" height="16">'+deliveryNoOrBarcode+'</div>');
			}
			else
			{
				if (!$j('div#scanItemsInOut').prev().hasClass('hid'))
				{
					$j('div#scanItemsInOut').prev().addClass('hid');
				}	
				var sucSpanId = 'added' + getSemiRandomInt();
				// add a success image and 3 second timeout to the basket form
				$j('input#scanout').after(	'<span id="' + sucSpanId + '">' +
													'<img src="img/icons/greentick.png" width="16" height="16" alt="' + i18n.t("success", "Success") + '" class="successgif img_vertmiddle" />' +
												'</span>');
														
				setTimeout(" $j('span').remove(\'#" + sucSpanId + "\');", 1200);
				$j('div#scanOutHistory').prepend('<div><img src="img/icons/greentick.png" width="16" height="16">'+deliveryNoOrBarcode+'</div>');
			}
			
			$j('#scanout').val('').focus();
		}
	});
}

function receive(deliveryNoOrBarcode)
{
	var addrId = $j('#currentAddr').val();
	deliveryservice.receiveDeliveryOrItem(deliveryNoOrBarcode, addrId, null, 
	{
		callback:function(resWrap)
		{
			if(resWrap.success == false)
			{
				$j('div#scanItemsInOut').prev().removeClass('hid').find('div.warningBox3').html('<div>' + resWrap.message + '</div>');
				$j('div#scanInHistory').prepend('<div><img src="img/icons/redcross.png" width="16" height="16">'+deliveryNoOrBarcode+'</div>');
			}
			else
			{
				if (!$j('div#scanItemsInOut').prev().hasClass('hid'))
				{
					$j('div#scanItemsInOut').prev().addClass('hid');
				}	
				var sucSpanId = 'added' + getSemiRandomInt();
				// add a success image and 3 second timeout to the basket form
				$j('input#scanin').after('<span id="' + sucSpanId + '">' +
													'<img src="img/icons/greentick.png" width="16" height="16" alt="' + i18n.t("success", "Success") + '" class="successgif img_vertmiddle" />' +
												'</span>');
														
				setTimeout(" $j('span').remove(\'#" + sucSpanId + "\');", 1200);
				$j('div#scanInHistory').prepend('<div><img src="img/icons/greentick.png" width="16" height="16">'+deliveryNoOrBarcode+'</div>');
			}
			
			$j('#scanin').val('').focus();
		}
	});
}

function emptyScanIn() {
	$j('div#scanInHistory').empty();
}

function emptyScanOut() {
	$j('div#scanOutHistory').empty();
}