/**
 *	FILENAME		:	EditCapabilityFilters.js
 *	HISTORY			:	2018-05-28 - Galen Beck - Created File
 *						2020-09-21 - Laila MADANI- Updated File
 */

function addFilterRow() {
	var sourceRowId = 'templaterow-filter';
	var targetRowId = 'row-filter-'+insertionFilter;
	var sourceRow = $j('#'+sourceRowId);
	var newRow = sourceRow.clone();
	// Change ID and name of this row
	newRow.attr('id', targetRowId);
	newRow.attr('class', 'vis');
	// Rename selects/inputs
	var renames = ['servicetypeid', 'filterType','comments','id'];
	$j(renames).each(function(index) {
		var oldId = 'template-filter-'+renames[index];
		var newName = 'filters['+insertionFilter+'].'+renames[index];
		var newId = 'filters'+insertionFilter+'.'+renames[index];
		console.log("Setting name "+newName+" on id "+oldId);
		console.log("Setting id "+newId+" on id "+oldId);
		newRow.find('#'+oldId).attr('name', newName).attr('id', newId);
	});
	// Rename checkboxes (these have a hidden input afterwards which also must be renamed!)
	var checkboxRenames = ['bestLab','delete'];
	$j(checkboxRenames).each(function(index) {
		var oldId = 'template-filter-'+checkboxRenames[index];
		var newName = 'filters['+insertionFilter+'].'+checkboxRenames[index];
		var newId = 'filters'+insertionFilter+'.'+checkboxRenames[index];
		var newHiddenName = '_filters['+insertionFilter+'].'+checkboxRenames[index];
		console.log("Setting name "+newName+" on id "+oldId);
		console.log("Setting id "+newId+" on id "+oldId);
		var element = newRow.find('#'+oldId).attr('name', newName).attr('id', newId);
		var nextElement = element.next();
		console.log("Setting hidden name "+newHiddenName+" on next element with name "+nextElement.attr('name'));
		nextElement.attr('name', newHiddenName);
	});
	
	newRow.insertBefore(sourceRow);
	
	// Increment global insertion index for next insertion
	insertionFilter++;
}