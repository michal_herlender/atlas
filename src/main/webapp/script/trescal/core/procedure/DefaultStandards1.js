/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	DefaultStandards.js
*	DESCRIPTION		:	Generic collection of functions used to edit and delete both ProcedureStandards (viewcapability.htm)
* 						and WorkInstructionStandards (viewworkinstruction.htm) - both inherit from DefaultStandard.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	24/02/2009 - SH - Created File
*					:	22/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * load the necessary dwr service files
 */
loadScript.loadScripts(	new Array(	'dwr/interface/instrumservice.js',
									'dwr/interface/defaultstandardservice.js',
									'script/thirdparty/jQuery/jquery.boxy.js' ));
 
 /**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{
	// load stylesheet using the dom if not already
	loadScript.loadStyle('styles/jQuery/jquery-boxy.css', 'screen');
}

/**
 * this method creates the content necessary for adding standards to a procedure or work instruction
 * and adds it to a pop up.
 * 
 * @param {String} tableid the id of the table to append new standards to
 * @param {Integer} id id of the current procedure or work instruction
 * @param {String} ref reference of the current procedure or work instruction
 * @param {String} name name of the current procedure or work instruction
 * @param {String} type is the DefaultStandardType (enum) (i.e WORKINSTRUCTION, PROCEDURE)
 */
function addStandardsContent(tableid, id, ref, name, type)
{		
	// create content
	var content = 	'<div id="addStandardsBoxy" class="overflow">' +
						// add warning box to display error messages if the link fails
						'<div class="hid warningBox1">' +
							'<div class="warningBox2">' +
								'<div class="warningBox3">' +
								'</div>' +
							'</div>' +
						'</div>' +
						'<fieldset>' +
							'<legend>' + i18n.t("core.procedure:addStandardFor", "Add standard for") + ' '
							if (type == 'WORKINSTRUCTION')
							{
								content += '' + ref + '</legend>';
							}
							else
							{
								content += '' + ref + ' (' + name + ')</legend>';
							}
				content +=	'<ol>' +
								'<li>' +
									'<label>' + i18n.t("barcodeLabel", "Barcode:") + '</label>' +
									'<input type="text" onkeypress=" if (event.keyCode==13) { findStd($j(\'div#addStandardsBoxy input#instBarcode\').val(), \'' + tableid + '\'); event.preventDefault(); } " id="instBarcode" name="instBarcode" />' +
								'</li>' +
								'<li>' +
									'<label>&nbsp;</label>' +
									'<input type="button" onclick=" findStd($j(\'div#addStandardsBoxy input#instBarcode\').val(), \'' + tableid + '\'); " value="' + i18n.t("add", "Add") + '" />' +
								'</li>' +
							'</ol>' +
						'</fieldset>' +
						'<table class="default2" id="addstandards" summary="This table displays standards temporarily before they are added to the database and page">' +
							'<thead>' +
								'<tr>' +
									'<th id="stdadd" scope="col">' + i18n.t("add", "Add") + '</th>' +
									'<th id="stdbar" scope="col">' + i18n.t("barcode", "Barcode") + '</th>' +
									'<th id="stdinst" scope="col">' + i18n.t("instrument", "Instrument") + '</th>' +
									'<th id="stdserial" scope="col">' + i18n.t("serialNo", "Serial No") + '</th>' +
									'<th id="stdplant" scope="col">' + i18n.t("plantNo", "Plant No") + '</th>' +
									'<th id="stdenforce" scope="col">' + i18n.t("core.procedure:enforce", "Enforce") + '</th>' +
								'</tr>' +
							'</thead>' +
							'<tfoot>' +
								'<tr>' +
									'<td colspan="6">&nbsp;</td>' +
								'</tr>' +
							'</tfoot>' +
							'<tbody>' +
								'<tr>' +
									'<td colspan="6" class="center bold">' + i18n.t("addStandardsToTable", "Add Standards to table") + '</td>' +
								'</tr>' +
							'</tbody>' +
						'</table>' +
						'<div class="center-padbot">' +
							'<input type="button" value="' + i18n.t("addStandards", "Add Standards") + '" onclick=" addStds(' + id + ', \'' + type + '\', \'' + tableid + '\'); " />' +
						'</div>' +								
					'</div>';
	// initialise new boxy pop up			
	var addStandardsBoxy = new Boxy(content, {title: i18n.t("core.procedure:loadingAvailableCostSources", "Loading Available Cost Sources"), unloadOnHide: true, modal: true});
	// show and size boxy pop up
	addStandardsBoxy.tween(780,300);
	// add focus to the barcode input field
	$j('div#addStandardsBoxy input#instBarcode').focus();
}

/**					
 * this function finds the instrument with the id in the barcode field, checks that it is a valid standard
 * to use on this procedure or work instruction, and adds it to the list of standards for this procedure or work instruction
 *
 * @param {Integer} barcode id provided in the barcode field
 * @param {String} tableid the id of the table to append new standards to
 */
function findStd(barcode, tableid)
{	
	// check that instrument standard with this barcode is not already listed in standards table
	if($j('table#' + tableid + ' tr#std' + barcode).length == 0)
	{
		// call dwr service to find instrument standard
		instrumservice.findInstrumAsStandard(barcode,
		{
			callback:function(results)
			{	
				// error has occured finding instrument standard
				if(!results.success)
				{
					if(results.errorList)
					{
						// empty the error warning box
						$j('div#addStandardsBoxy div.warningBox3').empty();
						// variable to hold error messages
						var errors = '';
						$j.each(results.errorList, function(i)
						{
							// display error messages
							var error = results.errorList[i];
							errors ='<div>' + i18n.t("fieldError", {varErrField: error.field, varErrMsg:error.defaultMessage, defaultValue : "Field: " + error.field + " failed. Message:" + error.defaultMessage}) + '</div>';
						});
						// show error message in error warning box
						$j('div#addStandardsBoxy div:first').removeClass('hid').addClass('vis').find('.warningBox3').append(errors);
					}
					else
					{
						// empty the error warning box
						$j('div#addStandardsBoxy div.warningBox3').empty();
						// show error message in error warning box
						$j('div#addStandardsBoxy div:first').removeClass('hid').addClass('vis').find('.warningBox3').append(results.message);
					}
				}
				else
				{
					// set variable to instrument object
					var inst = results.results;
					// declare 'Out Of Cal' variables
					var outOfCal = '';
					var rowcolor = '';
					// Out Of Cal so set variables
					if(inst.outOfCalibration == true)
					{
						outOfCal = '<br/><span class="attention">' + i18n.t("outOfCal", "OUT OF CALIBRATION") + '</span>';
						rowcolor = 'highlight';
					}
					// thickbox table contains message
					if ($j('div#addStandardsBoxy table tbody tr td').length < 5)
					{
						// remove this message before adding a new row
						$j('div#addStandardsBoxy table tbody tr:first').remove();
					}
					// append new instrument standard to the table
					$j('div#addStandardsBoxy table tbody').append(	'<tr class="' + rowcolor + '">' + 
																		'<td class="center">' +
																			'<input type="checkbox" name="addStd" value="' + inst.plantid + '" checked="checked" /> ' + 
																			'<input type="hidden" name="addStdsOnScreen" value="' + inst.plantid + '" />' +
																		'</td>' +
																		'<td>' +
																			'<a href="viewinstrument.htm?plantid=' + inst.plantid + '" class="mainlink">' + inst.plantid + '</a>' +
																		'</td>' +
																		'<td>' + inst.model.mfr.name + ' ' + inst.model.model + ' ' + inst.model.description.description + outOfCal + ' </td>' +
																		'<td>' + inst.serialno + '</td>' +
																		'<td>' + inst.plantno + '</td>' +
																		'<td class="center"><input type="checkbox" id="enforce' + inst.plantid + '" checked="checked" /></td>' + 
																	'</tr>');
				}
			}
		});
	}
	// instrument standard with this barcode is already listed in standards table
	else
	{
		$j.prompt(i18n.t("core.procedure:instrIsAlreadyInTheList", {varBarcode : barcode, defaultValue : "Instrument with barcode " + barcode + " is already in the list of standards"}));
	}
	// apply focus and clear the barcode input field
	$j('div#addStandardsBoxy input#instBarcode').val('').focus();
}

/**					
 * this function loops through all the standards added to the temporary table which have been selected
 * by ticking the checkbox and then adds these standards to the procedure or work instruction using dwr before displaying
 * them in the standard table provided.
 *
 * @param {Integer} procid id of the current procedure
 * @param {String} type is the DefaultStandardType (enum) (i.e WORKINSTRUCTION, PROCEDURE)
 * @param {String} tableid the id of the table to append new standards to
 */
function addStds(procid, type, tableid)
{	
	// loop through each instrument standard that has been added to the thickbox table and is checked
	$j.each($j('table#addstandards tbody tr td input[name="addStd"]:checked'), function(i, n)
	{		
		// set variable to boolean value indicating whether use of the instrument standard should be enforced
		var enforceUse = $j('input#enforce' + n.value).is(':checked');
		
		// call dwr service that will insert the new procedure standard
		defaultstandardservice.insertDefaultStandard(procid, type, n.value, enforceUse,
		{
			callback:function(results)
			{
				// error occured adding procedure standard
				if(!results.success)
				{
					if(results.errorList)
					{
						// empty the error warning box
						$j('div#addStandardsBoxy div.warningBox3').empty();
						// variable to hold error messages
						var errors = '';
						$j.each(results.errorList, function(i)
						{
							// display error messages
							var error = results.errorList[i];
							errors ='<div>' + i18n.t("fieldError", {varErrField: error.field, varErrMsg:error.defaultMessage, defaultValue : "Field: " + error.field + " failed. Message:" + error.defaultMessage}) + '</div>';
						});
						// show error message in error warning box
						$j('div#addStandardsBoxy div:first').removeClass('hid').addClass('vis').find('.warningBox3').append(errors);
					}
					else
					{
						// empty the error warning box
						$j('div#addStandardsBoxy div.warningBox3').empty();
						// show error message in error warning box
						$j('div#addStandardsBoxy div:first').removeClass('hid').addClass('vis').find('.warningBox3').append(results.message);
					}
				}
				else
				{
					// initialise content variable
					var content = '';
					// first procedure standard to be added
					if ($j('table#' + tableid + ' tbody tr:first td').length < 7)
					{
						// add row the empty procedure standards table
						$j('table#' + tableid + ' tbody tr:first').remove();	
					}
					// set variable to instrument object
					var inst = results.results.instrument;					
					// declare 'Out Of Cal' variables
					var outOfCal = '';
					var rowcolor = '';
					// Out Of Cal so set variables
					if(inst.outOfCalibration == true)
					{
						outOfCal = '<br/> <span class="attention">' + i18n.t("outOfCal", "OUT OF CALIBRATION") + '</span>';
						rowcolor = 'class="highlight"';
					}
					// add content to variable for new standard row
					content = 	'<tr id="std' + inst.plantid + '" ' + rowcolor + '>' +
									'<td>' +
										'<img src="img/icons/bullet-standard.png" width="12" height="12" class="image_inl" alt="' + i18n.t("standard", "Standard") + '" title="' + i18n.t("standard", "Standard") + '" /> ';
										// can the calibration timescale indicator be shown?
										if ((inst.inCalTimescalePercentage != null) && (inst.finalMonthOfCalTimescalePercentage != null))
										{											
											content +=	'<span class="relative">' +
															'<div class="calIndicatorBox">';
																// is this instrument out of calibration?
																if ((inst.inCalTimescalePercentage == 100) && (inst.finalMonthOfCalTimescalePercentage == 100))
																{
																	content += '<div class="outOfCalibration"></div>';
																}
																else
																{
																	content += '<div class="inCalibration">' +
																					'<div class="inCalibrationLevel" style=" width: ' + inst.inCalTimescalePercentage + '%; "></div>' +
																				'</div>' +
																				'<div class="finalMonthOfCalibration">' +
																					'<div class="finalMonthOfCalibrationLevel" style=" width: ' + inst.finalMonthOfCalTimescalePercentage + '%; "></div>' +
																				'</div>';	
																}																				
												content +=	'</div>' +
														'</span>';
										}												
							content +=	'<a href="viewinstrument.htm?plantid=' + inst.plantid + '&amp;ajax=instrument&amp;width=400" class="jconTip mainlink" name="' + i18n.t("instrInformation", "Instrument Information") + '" id="inst' + inst.plantid + '">' + inst.plantid + '</a>' +
										' <img src="img/icons/mouse.png" width="12" height="12" class="image_inl" alt="" title="" />' +
									'</td>' +
									'<td>';
										// is this a calibration standard?  It should be obviously!
										if (inst.calibrationStandard)
										{
											content += '<img src="img/icons/bullet-standard.png" width="12" height="12" class="image_inl" alt="' + i18n.t("standard", "Standard") + '" title="' + i18n.t("standard", "Standard") + '" /> ';
										}
										// mfr non-specific link
										if(inst.mfr)
										{
											content += 	'<a href="' + springUrl + '/editmfr.htm?mfrid=' + inst.mfr.mfrid + '" class="mainlink title="' + i18n.t("core.procedure:editThisManufacturer", "Edit this manufacturer") + '">' + inst.mfr.name + '</a> ' +
														'<a href="' + springUrl + '/instrumentmodel.htm?modelid=' + inst.model.modelid + '" class="mainlink" title="' + i18n.t("core.procedure:editThisModel", "Edit this model") + '">' + inst.model.model + ' ' + inst.model.description.description + '</a>';
										}
										// mfr specific link
										else
										{
											content += 	'<a href="' + springUrl + '/instrumentmodel.htm?modelid=' + inst.modelid + '" class="mainlink">' +
															inst.model.mfr.name + ' ' + inst.model.model + ' ' + inst.model.description.description +
														'</a>';
										}
							content +=	outOfCal +
									'</td>' +
									'<td>' + inst.serialno + '</td>' +
									'<td>' + inst.plantno + '</td>' +
									'<td>';
										if (inst.nextCalDueDate)
										{ 
											content += '' + formatDate(inst.nextCalDueDate, 'dd.MM.yyyy') + '';
										}
						content += 	'</td>' +
									'<td class="center">' +
										'<a href="#" onclick=" updateDefStdEnforceUse(\'' + tableid + '\', ' + results.results.id + ', ' + inst.plantid + '); return false; ">';
											// instrument standard use should be enforced 
											if(enforceUse == true)
											{
												content += '<img src="img/icons/bullet-tick.png" width="10" height="10" alt="" />';
											}
											// instrument standard use should not be enforced
											else
											{
												content += '<img src="img/icons/bullet-cross.png" width="10" height="10" alt="" />';	
											}
							content +=	'</a>' +
									'</td>' +
									'<td>' + formatDate(results.results.setOn, 'dd.MM.yyyy') + '<br />' + results.results.setBy.name + ']</td>' +
									'<td class="center">' +
										'<a href="#"  onclick=" deleteDefStd(\'' + tableid + '\', ' + inst.plantid + ', ' + results.results.id + '); return false; ">' +
											'<img src="img/icons/delete.png" width="16" height="16" alt="' + i18n.t("core.procedure:deleteProcStandard", "Delete Procedure Standard") + '" title="' + i18n.t("core.procedure:deleteProcStandard", "Delete Procedure Standard") + '" />' +
										'</a>' +
									'</td>' +
								'</tr>';
						
					// append new row to the standards table
					$j('table#' + tableid + ' tbody').append(content);															
					// add one to the procedure standards menu item count
					$j('span.stdsSizeSpan').text(parseInt($j('span.stdsSizeSpan:first').text()) + 1);
					// initialise the tooltips
					JT_init();																			
				}
			}
			
		});
	});
	
	// get the current boxy implementation and close it
	Boxy.get($j('div#addStandardsBoxy')).hide();
}

/**
 * this function deletes a standard from a procedure
 * 
 * @param {String} tableid the id of the table in which the standards should be updated
 * @param {Integer} barcode plantid of the instrument standard that is being deleted
 * @param {Integer} id id of the def standard to delete
 */
function deleteDefStd(tableid, barcode, id)
{
	// call dwr service to delete procedure standard
	defaultstandardservice.deleteDefaultStandard(id,
	{
		callback:function(results)
		{
			// error has occured with the deletion
			if(results.success != true)
			{	
				$j.prompt(results.message);
			}
			else
			{	
				// remove the deleted procedure standard from the table
				$j('table#' + tableid + ' tbody tr#std' + barcode).remove();
				// last procedure standard has been deleted
				if ($j('table#' + tableid + ' tbody tr').length < 1)
				{
					// add row to the empty procedure standards table
					$j('table#' + tableid + ' tbody').append(	'<tr>' +
																'<td colspan="8" class="bold center">' +
																	i18n.t("core.procedure:noStandardsToDisplay", "There are no standards to display") +
																'</td>' +
															'</tr>');	
				}
				
				// add one to the purchase order menu item count
				$j('span.stdsSizeSpan').text(parseInt($j('span.stdsSizeSpan:first').text()) - 1);
			}
		}
	});
}

/**
 * this function updates the enforce use status of an instrument standard
 * 
 * @param {String} tableid the id of the table in which the standards should be updated
 * @param {Integer} psid id of the procedure standard to update
 * @param {Integer} barcode plantid of the instrument standard
 */
function updateDefStdEnforceUse(tableid, psid, barcode)
{
	// call dwr service to update the procedure standards enforce use property
	defaultstandardservice.updateDefaultStandardEnforceUse(psid,
	{
		callback:function(results)
		{
			// error has occured with the update
			if(results.success != true)
			{
				$j.prompt(results.message);
			}
			else
			{
				// new enforce use status
				if (results.results.enforceUse == true)
				{
					// change the image to be displayed
					$j('table#' + tableid + ' tbody tr#std' + barcode + ' td:eq(5) img').attr('src', 'img/icons/bullet-tick.png');
				}
				else
				{
					// change the image to be displayed
					$j('table#' + tableid + ' tbody tr#std' + barcode + ' td:eq(5) img').attr('src', 'img/icons/bullet-cross.png');
				}
			}
		}
	});
}