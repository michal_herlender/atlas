/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	ViewProcedure.js
*	DESCRIPTION		:	This javascript file is used on the viewprocedure.vm page.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	16/01/2008 - SH - Created File
*					:	22/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'script/trescal/core/procedure/DefaultStandards1.js',
		'script/trescal/core/tools/FileBrowserPlugin.js');

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' },
								{ element: 'ul.subnavtab a', corner: 'small transparent top' } );

/** 
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = 	new Array( 	{ anchor: 'viewfilters-link', block: 'viewfilters-tab' },
								{ anchor: 'viewaccredusers-link', block: 'viewaccredusers-tab' },
								{ anchor: 'standards-link', block: 'standards-tab' },
								{ anchor: 'resources-link', block: 'resources-tab' },
								{ anchor: 'proctraining-link', block: 'proctraining-tab' });
								
/** 
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 * this array is populated in head section of viewprocedure.vm
 */
var menuElements2 = new Array();

/** 
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements3 = new Array( 	{ anchor: 'accred-link', block: 'accred-tab' },
								{ anchor: 'nonaccred-link', block: 'nonaccred-tab' } );



/**
 * variable which holds the id of clicked date input field
 * which is used in method @see {setDateFromDialog}.
 */
var idCurrentInput = '';

/**
 * this method populates the input field selected using id stored in
 * variable @see {idCurrentInput} with the value of parameter date.
 * 
 * @param {String} date the date passed from datepicker jquery plugin
 */
function setDateFromDialog(date)
{
	$j('#' + idCurrentInput).val(date);
}

/**
 * this method copies the selected option to all other select boxes in table 
 *
 * @param {String} tableid id of the table in which all selects should be copied
 * @param {Object} select select which we can obtain value and name for copying
 */
function copySelectedOption(tableid, elementtype, element)
{
	$j('#' + tableid + ' ' + elementtype + '[name="' + $j(element).attr('name') + '"]').each(function(i, n)
	{
		$j(this).val($j(element).val());
	})
}


/**
 * this method toggles the display of user accreditations in thickbox.
 * 
 * @param {Object} anchor the anchor clicked on.
 */
function toggleAccreditations(anchor)
{
	// get parent list item of anchor
	var listitem = $j(anchor).parent().parent();
	// Users accreditation box displayed?
	if ($j(listitem).find('div[class^="accreditations"]').css('display') == 'none')
	{				
		// append new anchor and image after the current one for hiding accreditations,
		// then remove the anchor and image for showing accreditations.
		$j(anchor).after('<a href="#" onclick=" toggleAccreditations(this); return false; ">' +
							'<img src="img/icons/hideinfo.png" width="16" height="16" class="img_marg_bot" alt="' + i18n.t("core.procedure:hideAccreditations", "Hide Accreditations") + '" title="' + i18n.t("core.procedure:hideAccreditations", "Hide Accreditations") + '" />' +
						'</a>').remove();
		// find the users accreditation div and use effect to show it
		$j(listitem).find('div[class^="accreditations"]').slideDown(1200);
	}
	else
	{
		// append new anchor and image after the current one for showing accreditations,
		// then remove the anchor and image for hiding accreditations.
		$j(anchor).after('<a href="#" onclick=" toggleAccreditations(this); return false; ">' +
							'<img src="img/icons/showinfo.png" width="16" height="16" class="img_marg_bot" alt="' + i18n.t("core.procedure:showAccreditations", "Show Accreditations") + '" title="' + i18n.t("core.procedure:showAccreditations", "Show Accreditations") + '" />' +
						'</a>').remove();
		// find the users accreditation div and use effect to hide it
		$j(listitem).find('div[class^="accreditations"]').slideUp(1200);					
	}
}

