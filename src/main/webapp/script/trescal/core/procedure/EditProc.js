/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	EditProc.js
*	DESCRIPTION		:	This javascript file is used on the editproc.vm page.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	16/01/2008 - SH - Created File.
*
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'procedure.reference';

/**
 * Array of autocomplete objects (multiple are possible) containing setup for 
 * jQueryUI initialization.
 * this is used in the init() function.
 */
var jquiAutocomplete = new Array(
	{
		inputFieldName: 'subfamilyName',
		inputFieldId: 'subfamilyId',
		source: 'searchdescriptiontags.json'
	});

/**
 * Configures the form for editing a capability record that belongs to a business company
 */
function showBusinessCapability() {
	$j("li.businessVisibility").addClass('vis').removeClass('hid');
	$j("li.supplierVisibility").addClass('hid').removeClass('vis');
}

/**
 * Configures the form for editing a capability record that belongs to a supplier company
 */
function showSupplierCapability() {
	$j("li.businessVisibility").addClass('hid').removeClass('vis');
	$j("li.supplierVisibility").addClass('vis').removeClass('hid');
}