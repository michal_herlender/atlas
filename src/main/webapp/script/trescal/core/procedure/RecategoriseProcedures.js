/**
 * File name: trescal/core/procedure/RecategoriseProcedures.js
 * 
 */

var pageImports = new Array (	'script/thirdparty/jQuery/jquery.tablesorter.js'
								);


function init() {
	// make table columns sortable and apply zebra effect
    $j('table.tablesorter').tablesorter({ headers: { 0: { sorter: false} } });
}