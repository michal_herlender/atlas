/**
 * Filename : EditProcedureCategories.js
 */

/**
 * adds a row to the user interface using a hidden template row as the source
 * @param deptid - string - database id of the department that we want to add a procedure for
 */
function addInputRow(deptid) {
	var sourceRowId = 'templaterow-'+deptid;
	var targetRowId = 'row-'+insertionIndex;
	var sourceRow = $j('#'+sourceRowId);
	var newRow = sourceRow.clone();
	// Change ID and name of this row
	newRow.attr('id', targetRowId);
	newRow.attr('class', 'vis');
	// Rename inputs
	var inputRenames = ['deptId','name','description','add'];
	$j(inputRenames).each(function(index) {
		var oldId = 'template_'+deptid+'_'+inputRenames[index];
		var newName = 'inputDTOs['+insertionIndex+'].'+inputRenames[index];
		console.log("Setting name "+newName+" on id "+oldId);
		newRow.find('input#'+oldId).attr('name', newName);
	});
	newRow.insertAfter(sourceRow);
	
	// Increment global insertion index for next insertion
	insertionIndex++;
	
	// Focus on the first text input of the newly created element
	$j('#'+targetRowId).find('input:text:visible:first').focus();
}

/**
 * 
 * @param element - img of the delete elemnent being clicked on 
 * @returns
 */

function deleteInputRow(element) {
	$j(element).closest('tr').remove();
}
