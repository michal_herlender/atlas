/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	EditContactProcedureAccreditation.js
*	DESCRIPTION		:	This javascript file is used on the editcontactprocedureaccreditation.vm page.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	17/01/2008 - SH - Created File
*
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * array of calendar objects containing the initial setup literals needed
 * 
 * @param {String} inputField is the id of the input field to which the calendar should be binded
 * @param {String} dateFormat is the format that the date should be displayed
 * @param {String} displayDates 'all' make all dates available
 * 								'future' show only dates in the future
 * 								'past' show only dates in the past
 * applied in init() function
 * populated in head section of editcontactprocedureaccreditation.vm
 */								
var jqCalendar = new Array ();
							 