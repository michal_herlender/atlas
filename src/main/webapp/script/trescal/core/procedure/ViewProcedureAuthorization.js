/**
 * this variable holds an array of id objects, one for the navigation link and
 * one for the content area the link refers to. this array is passed to the
 * Menu.js file to display the corresponding content area of the link clicked.
 * 
 * 
 */

var menuElements3 = new Array({
	anchor : 'accred-link',
	block : 'accred-tab'
}, {
	anchor : 'nonaccred-link',
	block : 'nonaccred-tab'
});


function procedureAuthorizationOverlay(procId){
	
	var titleAction = i18n.t("core.procedure:editAccreditations", "Edit Accreditations");
	var contentUri = 'viewProcedureAuthorization.html?procid='+procId;
	loadScript.createOverlay('external', titleAction, contentUri, null, null,80, 820, null);
}


/**
 * @param {Object}
 *            anchor the anchor clicked on.
 */
function toggleAccreditations(anchor) {
	// get parent list item of anchor
	var listitem = $j(anchor).parent().parent();
	// Users accreditation box displayed?
	if ($j(listitem).find('div[class^="accreditations"]').css('display') == 'none') {
		// append new anchor and image after the current one for hiding
		// accreditations,
		// then remove the anchor and image for showing accreditations.
		$j(anchor)
				.after(
						'<a href="#" onclick=" toggleAccreditations(this); return false; ">'
								+ '<img src="img/icons/hideinfo.png" width="16" height="16" class="img_marg_bot" alt="'
								+ i18n.t("core.procedure:hideAccreditations",
										"Hide Accreditations")
								+ '" title="'
								+ i18n.t("core.procedure:hideAccreditations",
										"Hide Accreditations") + '" />'
								+ '</a>').remove();
		// find the users accreditation div and use effect to show it
		$j(listitem).find('div[class^="accreditations"]').slideDown(1200);
	} else {
		// append new anchor and image after the current one for showing
		// accreditations,
		// then remove the anchor and image for hiding accreditations.
		$j(anchor)
				.after(
						'<a href="#" onclick=" toggleAccreditations(this); return false; ">'
								+ '<img src="img/icons/showinfo.png" width="16" height="16" class="img_marg_bot" alt="'
								+ i18n.t("core.procedure:showAccreditations",
										"Show Accreditations")
								+ '" title="'
								+ i18n.t("core.procedure:showAccreditations",
										"Show Accreditations") + '" />'
								+ '</a>').remove();
		// find the users accreditation div and use effect to hide it
		$j(listitem).find('div[class^="accreditations"]').slideUp(1200);
	}
}


function CopySelectedOptions(elementClass , element){
	
	$('.'+elementClass).each(function(i,n){
		$j(this).val($j(element).val());
	})

}
