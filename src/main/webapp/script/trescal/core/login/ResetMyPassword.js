class ResetMyPassword {
	
	constructor(form){
		form.onsubmit = this.onFormSubmit
		console.log("create Reset My Password instance");
	}
	
	onFormSubmit(e) {
		e.preventDefault();
		const form = e.target;
		const formData = new FormData(form);
		fetch(form.action,
		{
			method: form.method,
			body: formData,
		}).then(r => r.ok? r.json():Promise.reject(r.statusText))
		.then(b => {
			const parent = form.parentElement;
			form.remove();
			const message = document.createElement("span");
			message.innerText = b.message;
			parent.appendChild(message);
		})
	}
}


if(window.cwms == undefined) window.cwms = {};
window.cwms.resetMyPassword = new ResetMyPassword(document.getElementById('f1'));