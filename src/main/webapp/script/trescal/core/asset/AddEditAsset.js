/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	AddEditAsset.js
*	DESCRIPTION		:	-
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-						
*	KNOWN ISSUES	:	-
*	HISTORY			:	12/10/2010 - SH - Created File
*
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * Array of calendar objects containing the initial setup literals needed
 * @param inputField (String) id of the input field to which the calendar should be binded
 * @param dateFormat (String) format that the date should be displayed
 * @param displayDates (String) 'all' make all dates available
 * 								'future' show only dates in the future
 * 								'past' show only dates in the past
 * Applied in init() function.
 */				
//var jqCalendar = new Array ( { 
//								 inputField: 'asset\\.editableAddedOn',
//								 dateFormat: 'dd.mm.yy',
//								 displayDates: 'all'
//							 },
//							 { 
//								 inputField: 'asset\\.purchasedOn',
//								 dateFormat: 'dd.mm.yy',
//								 displayDates: 'all'
//							 });

/**
 * this method shows the custodian/primary user fields
 * 
 * @param {Object} li the list item from which checkbox changed
 * @param {Boolean} checked indicates if the checkbox is checked
 */
function toggleUserOptions(li, checked)
{
	// display custodian and primary user fields?
	if (checked)
	{
		// display custodian and primary user fields
		$j(li).next().removeClass('hid').addClass('vis').next().removeClass('hid').addClass('vis');
	}
	else
	{
		$j(li).next().removeClass('vis').addClass('hid').find('select option:first').attr('selected', 'selected').end()
				.next().removeClass('vis').addClass('hid').find('select option:first').attr('selected', 'selected');
	}
}