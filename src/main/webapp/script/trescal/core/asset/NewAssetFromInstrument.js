/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	NewAssetFromInstrument.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: *						
*	KNOWN ISSUES	:	-
*	HISTORY			:	14/10/2010 - SH - Created File
*
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'searchplantid';


/**
 * This method retrieves input values from the page and then calls a dwr service to retrieve items. Depending on the search type a callback
 * method is called to display the items.
 *
 */			
function searchBusinessInstruments()		
{	
	// holds total length of all inputs used for search
	var inputsLength = '';
	// get company coid
	var coid = $j('select[name="businesscompId"]').val();
	// get calibration standards
	var standards = ($j('input[name="standards"]').is(':checked')) ? true : false;
	// get value from the manufacturer input field
	var mfrname = $j('input[name="mfr"]').val();
	// get value from the model input field
	var modelname = $j('input[name="model"]').val();
	// get value from the description input field
	var descname = $j('input[name="desc"]').val();
	// add to total length
	inputsLength += (mfrname + modelname + descname);	
	// get value from the plantid input field
	var barcode = $j('input[name="searchplantid"]').val();
	// get value from the plant number input field
	var plantno = $j('input[name="plantno"]').val();
	// get value from the serial number input field
	var serialno = $j('input[name="serialno"]').val();
	// add to total length
	inputsLength += (plantno + serialno);	
		
	// get current timestamp to pass with ajax results
	var currentTime = new Date().getTime();
																																
	// total of all input fields used greater than one?
	if ((inputsLength.length > 1) || (barcode.length > 2))
	{
		// display loading image
		$j('span#loading').removeClass();
		// load the service javascript file if not already
		loadScript.loadScriptsDynamic(new Array('dwr/interface/instrumservice.js'), true,
		{
			callback: function()
			{
				// call dwr service to retrieve matching instruments
				instrumservice.searchBusinessInstrumentsForAssetsHQL(coid, standards, mfrname, modelname, descname, barcode, plantno, serialno,
				{
					callback:function(instrums)
					{	
						// call method to display instrument results					
						businessInstrumentsCallback(instrums, currentTime);
					}
				});
			}
		});
	}
	else
	{
		// remove all previously displayed instruments, append no result message
		$j('table tbody').empty().append(	'<tr>' +
												'<td colspan="5" class="bold center">' + i18n.t("core.asset:noResultFound", "Type search criteria to find matching business instruments") + '</td>' +
											'</tr>');
		// update count of instruments returned
		$j('span.businessCompanyInstSize').text(0);
		// hide loading image
		$j('span#loading').removeClass().addClass('hid');
	}																													
}



/**
 * this method displays all results retrieved from the search business instrument dwr service method
 *
 * @param {Array} instrums is an array of instrument objects returned from the server
 * @param {Date/Time} currentUpdate is the time of data retrieval from the server
 */			
function businessInstrumentsCallback(instrums, currentUpdate)
{	
	// this is a new search
	if (currentUpdate > loadScript.lastUpdate1)
	{
		// update value in the 'lastUpdate' literal
		loadScript.lastUpdate1 = currentUpdate;															
		// instrument results length is greater than zero?																														
		if(instrums.length > 0)
		{					
			// remove all previously displayed instruments
			$j('table tbody').empty();
			// variable to hold all instruments returned
			var content = '';																		
			// add table row for each instrument returned 
			$j.each(instrums, function(i, iawrap)
			{																													
				content += 	'<tr';
								if (iawrap.alreadyAsset)
								{
									content += 	' class="highlight-green"';
								}
					content += 	'>' +
								'<td class="add">';
									if (!iawrap.alreadyAsset)
									{
										if (!iawrap.scrapped)
										{
											content += 	'<a href="#" onclick=" addAsset(' + iawrap.plantid + '); return false; "><img src="img/icons/add.png" width="16" height="16" /></a>';
										}
									}
									else
									{
										content += '<a href="viewasset.htm?id=' + iawrap.assetid + '" class="mainlink">' + iawrap.assetno + '</a>';
									}
					content +=	'</td>' +
								'<td class="barcode">' + iawrap.plantid + '</td>' +
								'<td class="desc">';
									if (iawrap.calibrationStandard)
									{
										content += 	'<img src="img/icons/bullet-standard.png" width="12" height="12" class="image_inl" alt="' + i18n.t("standard", "Standard") + '" title="' + i18n.t("standard", "Standard") + '" /> ';
									}
									if (iawrap.scrapped)
									{
										content += 	'<img src="img/icons/bin_closed.png" width="16" height="16" class="image_inl" alt="' + i18n.t("unitBER", "Unit B.E.R.") + '" title="' + i18n.t("unitBER", "Unit B.E.R.") + '" /> ';
									}
						content +=	'<a href="viewinstrument.htm?plantid=' + iawrap.plantid + '&amp;ajax=instrument&amp;width=500" class="jconTip ';
										if (iawrap.scrapped)
										{
											content += 	'mainlink-strike';
										}
										else
										{
											content += 	'mainlink';
										}
										content += 	'" name="' + i18n.t("instrInformation", "Instrument Information") + '" id="inst' + iawrap.plantid + i + '">' + iawrap.mfr + ' ' + iawrap.model + ' ' + iawrap.description + 
									'</a> ' +
									'<img src="img/icons/mouse.png" width="12" height="12" class="image_inl" alt="" title="" />' +
								'</td>' +
								'<td class="serialno">' + iawrap.serialno + '</td>' +
								'<td class="plantno">' + iawrap.plantno + '</td>' +								
							'</tr>';			
			});																		
			// append all instruments returned to table
			$j('table tbody').append(content);
			// update count of instruments returned
			$j('span.businessCompanyInstSize').text(instrums.length);
			// hide loading image
			$j('span#loading').removeClass().addClass('hid');
			// initialise tooltips
			JT_init();
		}
		else
		{																		
			// remove all previously displayed instruments, append no result message
			$j('table tbody').empty().append(	'<tr>' +
													'<td colspan="5" class="bold center">' + i18n.t("core.asset:noResultFound", "Type search criteria to find matching business instruments") + '</td>' +
												'</tr>');
			// update count of instruments returned
			$j('span.businessCompanyInstSize').text(instrums.length);
			// hide loading image
			$j('span#loading').removeClass().addClass('hid');																	
		}	
	}
}

/**
 * this method adds the selected instrument plantid to the hidden input in form and then submits the page
 * 
 * @param plantid id of the instrument to be added as asset
 */
function addAsset(plantid)
{
	// add plantid to hidden input field in form
	$j('input[name="plantId"]').val(plantid);
	// submit the form
	$j('form').submit();
}