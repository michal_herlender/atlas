/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	ViewAsset.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: *						
*	KNOWN ISSUES	:	-
*	HISTORY			:	12/10/2010 - SH - Created File
*					:	06/10/2015 - TProvost - Manage i18n
*					:	06/10/2015 - TProvost - Fix bug in new version of Impromptu.js - Replace the "callback" property (property disappeared) by the "submit" property
*					:							=> Changes made in functions deleteAsset
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' },
								{ element: 'ul.subnavtab a', corner: 'small transparent top' } );

/**
 * this method prints a zebra label for asset
 * 
 * @param {Integer} assetId id of the asset to be used on label
 * @param {Boolean} incIP indicates if the ip address should be included
 */
function printAssetLabel(assetId, incIP)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/zebralabelservice.js'), true,
	{
		callback: function()
		{
			zebralabelservice.printAssetLabel(assetId, incIP, 
			{
				callback: function(result)
				{
					if (!result.success)
					{
						$j.prompt(result.message);
					}
					else
					{
						$j.prompt(i18n.t("core.asset:printAssetLabel", "Your asset label has been printed!"));
					}
				}
			});
		}
	});
}

/**
 * this function deletes the asset from the database
 * 
 * @param {Integer} assetId is the id of the asset to be deleted
 */
function deleteAsset(assetId)
{
	// initial prompt for user
	$j.prompt(i18n.t("core.asset:deleteAsset", "This asset will now be deleted from the database. Click 'OK' to confirm the deletion or 'Cancel' to go back"),
	{ 
		submit: confirmcallback,
		buttons: 
		{ 
			Ok: true,
			Cancel: false 
		}, 
		focus: 1 
	});
	// callback method for initial prompt
	function confirmcallback(v,m) {
		// user confirmed action?
    	if (v) {
			$j.ajax({
				url: "deleteasset.json",
				data: {
					assetId: assetId
				},
				async: true
			}).done((r) => {
				if(r.success)
					window.location.href = 'assetsearch.htm';
				else $j.prompt(r.message);
			})
		}
	}
}