function editValue(id) {
	$j('#max'+id).removeClass().addClass('hid');
	$j('#inputmax'+id).removeClass().addClass('vis');
	$j('#edit'+id).removeClass().addClass('hid');
	$j('#save'+id).removeClass().addClass('vis');

}

function saveValue(idPermissionLimit) {
	if(parseFloat($j('#max'+idPermissionLimit).text())!=$j('#inputmax'+idPermissionLimit).val()){
		$j.ajax({
			url: "editmaxpermissionlimit.json",
			data: {
				id: idPermissionLimit,
				max: $j('#inputmax'+idPermissionLimit).val()
			},
			async: true
		});
		
		$j('#max'+idPermissionLimit).text($j('#inputmax'+idPermissionLimit).val());
	}
	$j('#max'+idPermissionLimit).removeClass().addClass('vis');
	$j('#inputmax'+idPermissionLimit).removeClass().addClass('hid');
	$j('#edit'+idPermissionLimit).removeClass().addClass('vis');
	$j('#save'+idPermissionLimit).removeClass().addClass('hid');

}

