function addPermission(permission,groupId,index) {
	$j.ajax({
		url: "addPermission.json",
		data: {
			permission: permission,
			groupId: groupId
		},
		async: true
	}).done(function(newLine) {
		hideOrShowFilter(index,true)
	});
}

function hideOrShowFilter(i,isAdd) {
	if(isAdd){						
		$j('#'+i).val('enable');
		$j('#add'+i).removeClass("vis").addClass("hid");
		$j('#delete'+i).removeClass("hid").addClass("vis");
	}
	else 
	{
		$j('#'+i).val('unable');
		$j('#add'+i).removeClass("hid").addClass("vis");
		$j('#delete'+i).removeClass("vis").addClass("hid");
	}
}

function removePermission(permission, groupId, index) {
	$j.ajax({
		url: "removePermission.json",
		data: {
			permission: permission,
			groupId: groupId
		},
		async: true
	}).done(function() {
		hideOrShowFilter(index,false);
	});
}

function enableNameEdit() {
	$j("#nameInput").attr("disabled", false);
	$j("#enableEdit").addClass("hid");
	$j("#disableEdit").removeClass("hid");
}

function changeName(groupId) {
	$j("#disableEdit").addClass("hid");
	$j("#nameInput").attr("disabled", true);
	$j.ajax({
		url: "editPermissionGroupName.json",
		type: "POST",
		data: {
			groupId: groupId,
			groupName: $j("#nameInput").val()
		},
		async: true
	}).done(function() {
		$j("#enableEdit").removeClass("hid");
	});
}

function deleteOverlay(groupId) 
{
	// variable to hold content
	var content = '';
			// create content for delete perm group
			content += 	'<div id="deleteOverlayValidation">' +
							'<span>'+ i18n.t("core.admin:PermissionGroup.deletePermGroup","deletePermGroup") +'<span> '	+
							'<div><br></div>' +
							'<form style="text-align: center;" method="post" action="permissiongroups.htm/delete?groupId=' + groupId + '">' +
							'<input  type="submit" value="' + i18n.t("delete", "Delete") + '" />'	+
							'</form>'+
						'</div>';
			// create new overlay
			loadScript.createOverlay('dynamic', i18n.t("delete", "Delete"), null, content, null, 15, 250, null);
}

	$(document).ready(function() {
	    // Setup - add a text input to each footer cell
	    $('#permissionTable thead tr').clone(true).appendTo( '#permissionTable thead' );
	    $('#permissionTable thead tr:eq(1) th').each( function (i) {
	        var title = $(this).text();
	        if(i!=4)
	        {
	        	$(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    	        $( 'input', this ).on( 'keyup change', function () {
    	            if ( table.column(i).search() !== this.value ) {
    	                table
    	                    .column(i)
    	                    .search( this.value )
    	                    .draw();
    	            }
    	        } );
    	    }
	        else
	        	{
	        	$(this).html( ' <select>' +
    		 	 		' <option>Both</option>' +
    		 	 		' <option>Enable</option>'+
    		 	 		' <option>Unable</option>'+
    		 	  	  ' </select>' );
    	        $( 'select', this ).on( 'keyup change', function () {
    	            if ( table.column(i).search() !== this.value && this.value !== 'Both' ) 
    	            	table
    	                    .column(i)
    	                    .search( this.value )
    	                    .draw();
	                else
		            	table
    	                    .column(i)
    	                    .search('')
    	                    .draw();
    	            
    	        } );
	        	}
	    } );

	    var table = $('#permissionTable').DataTable( {
	    	  aLengthMenu: [
	    	        [25, 50, 100, 200, -1],
	    	        [25, 50, 100, 200, "All"]
	    	    ],
	    	    iDisplayLength: 25,
	        orderCellsTop: true,
	        fixedHeader: true
	    } );
	} );