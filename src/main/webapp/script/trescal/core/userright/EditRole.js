function addGroupToRole(roleId) {
	$j.ajax({
		url: "addGroupToRole.json",
		data: {
			groupId: $j("#newgroup").val(),
			roleId: roleId
		},
		async: true
	}).done(function(newLine) {
		$j("#groupTable tbody").append(newLine);
	});
}

function removeGroupFromRole(groupId, roleId, element) {
	$j.ajax({
		url: "removeGroupFromRole.json",
		data: {
			groupId: groupId,
			roleId: roleId
		},
		async: true
	}).done(function() {
		$(element).closest('tr').remove();
	});
}

function enableNameEdit() {
	$j("#nameInput").attr("disabled", false);
	$j("#enableEdit").addClass("hid");
	$j("#disableEdit").removeClass("hid");
}

function changeName(roleId) {
	$j("#disableEdit").addClass("hid");
	$j("#nameInput").attr("disabled", true);
	$j.ajax({
		url: "editUserRightRoleName.json",
		type: "POST",
		data: {
			roleId: roleId,
			roleName: $j("#nameInput").val()
		},
		async: true
	}).done(function() {
		$j("#enableEdit").removeClass("hid");
	});
}


function deleteOverlay(roleid) 
{
	// variable to hold content
	var content = '';
			// create content for delete perm group
			content += 	'<div id="deleteOverlayValidation">' +
							'<span>'+ i18n.t("core.admin:Role.deleteRole","deleteRole") +'<span> '	+
							'<div><br></div>' +
							'<form style="text-align: center;" method="post" action="editrole.htm/deleterole?roleId=' + roleid + '">' +
							'<input  type="submit" value="' + i18n.t("delete", "Delete") + '" />'	+
							'</form>'+
						'</div>';
			// create new overlay
			loadScript.createOverlay('dynamic', i18n.t("delete", "Delete"), null, content, null, 15, 250, null);
}
