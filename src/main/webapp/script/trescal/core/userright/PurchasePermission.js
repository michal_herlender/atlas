function editValue(id) {
	$j('#max'+id).removeClass().addClass('hid');
	$j('#inputmax'+id).removeClass().addClass('vis');
	$j('#edit'+id).removeClass().addClass('hid');
	$j('#save'+id).removeClass().addClass('vis');

}

function saveValue(idPurchasePermission) {
	if(parseFloat($j('#max'+idPurchasePermission).text())!=$j('#inputmax'+idPurchasePermission).val()){
		$j.ajax({
			url: "editPurchaseMaxValue.json",
			data: {
				id: idPurchasePermission,
				max: $j('#inputmax'+idPurchasePermission).val()
			},
			async: true
		});
		
		$j('#max'+idPurchasePermission).text($j('#inputmax'+idPurchasePermission).val());
	}
	$j('#max'+idPurchasePermission).removeClass().addClass('vis');
	$j('#inputmax'+idPurchasePermission).removeClass().addClass('hid');
	$j('#edit'+idPurchasePermission).removeClass().addClass('vis');
	$j('#save'+idPurchasePermission).removeClass().addClass('hid');

}

