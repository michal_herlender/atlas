var timeout;

function initializeTab(){

    $j('input.servicevalid').on('click', function(){
        updateValidServices(this);
    });
    $j('input.turnaround').on('change', function(){
        updateTurnaround(this);
    });
	
}

function updateServiceSettings(contractId, serviceTypeId, turnaround, add) {
    fetch(window.cwms.urlGenerator("contract/updateservice"),{
        method:"POST",
        headers:{
           "Content-Type": "application/json",
            ...window.cwms.metaDataObtainer.csrfHeader},
        body:JSON.stringify({contractId,serviceTypeId,turnaround,add})
    }).then(res => res.ok?true:Promise.reject(res.statusText))
    .catch(status => alert(`Request failed ${status}` ))
}

function updateTurnaround(input) {
	clearTimeout(timeout);
	timeout = setTimeout(function() {
	    var contractId = $j("input#contractid").val();
	    var serviceTypeId = $j(input).parents('tr').find('input.servicevalid').val();
	    var turnaround = $j(input).val();
	    var add = $j(input).parents('tr').find('input.servicevalid').prop('checked');
	    updateServiceSettings(contractId, serviceTypeId, turnaround, add);
	}, 500);
}

function updateValidServices(checkbox) {
    var contractId = $j("input#contractid").val();
    var serviceTypeId = $j(checkbox).val();
    var turnaround = $j(checkbox).parents('tr').find('input.turnaround').val();
    var add = $j(checkbox).prop('checked');
    updateServiceSettings(contractId, serviceTypeId, turnaround, add);
    $j(checkbox).parents('tr').find('input.turnaround').prop('disabled', !add);
}