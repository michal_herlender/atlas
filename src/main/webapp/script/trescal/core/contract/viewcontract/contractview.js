
function init() {

    $j('.menubutton').click(function (e) {

        var contractId = $j('#contractid').val();
        var tabType = $j(this).attr('id');

        var response = $j.ajax({
            type: "GET",
            contentType: "text/html",
            url: "contractviewtab/" + tabType + "/" + contractId
        });

        response.done(function(html){
            $j('#tabarea').html(html);
            initializeTab();
        });

        response.fail(function (jqXHR, textStatus) {
            alert("Request failed: " + textStatus);
        });

        $j('#subnav a').removeClass('selected');
        $j(this).addClass('selected');

        e.preventDefault();

    });

    $j('#subdivs').trigger('click');

}


