

function initializeTab(){

    $j("#addinstrumentbutton").on("click", function(){
        instrumentSearchDialog();
    });

    $j('#bulkaddbutton').on("click", function(){
        var contractId = $j("input#contractid").val();
        window.location.href = 'contractinstrumentbulkupload.htm?contractid=' + contractId;
    });

    const columns = [
        {
            "data": "plantId",
            "width": "15%"
        },
        {
            "data": "instrumentName",
            "width": "30%"
        },
        {
            "data": "serialNo",
            "width": "20%"
        },
        {
            "data": "subdivName",
            "width": "30%"
        },
        {
            "data": null,
            "defaultContent": "<button class='remove'>" + i18n.t("common:remove", "Remove")  + "</button>",
            "orderable": false,
            "width": "5%",
            "className": "dt-center"
        }
    ];
    const contractId = $j("input#contractid").val();
    const url = cwms.urlGenerator("contract/listinstruments")
   $j("#instrumentListTable").DataTable({
        "processing": true,
        "serverSide": true,
        "pageLength": 10,
        "ajax": (data,callback,settings) => {
            url.search = cwms.paramStrgify.toSearchParams({...data,contractId,conpanyId:0})
            fetch(url).then(res => res.ok?res.json():Promise.reject(res.statusText))
            .then(callback);
        },
        columns
    });


    $j("#instrumentListTable tbody").on( 'click', 'button.remove', function () {
        var row = $j("#instrumentListTable").DataTable().row($j(this).parents('tr'));
        var plantId = row.data().plantId;
        removeInstrument( plantId);
    });

    $j("#instSearchResultsTable").on('click', 'button.add', function() {
       addInstrument($j(this).data("instrumentid"));
    });

}


function instrumentSearchDialog() {

    if($j("#instsearchdialog").is(":data(dialog)")) {
        $j("#instsearchdialog").dialog("open")
    } else {
        $j("#instsearchdialog").dialog({
            dialogClass: "dialog-no-close",
            autoOpen: true,
            modal: true,
            width: 750,
            buttons: [
                {
                    text: i18n.t("common:close", "Close"),
                    click: function () {
                        $j(this).dialog("close");
                    }
                },
                {
                    text: i18n.t("common:search", "Search"),
                    click: function () {
                        if($j("div#instsearchdialog input#companyid").val() === "" || $j("div#instsearchdialog input#company").val() === ""){
                            alert("you must choose a client company");
                        } else {
                            $j(this).dialog("close");
                            instrumentSearchResultsDialog();
                        }
                    }
                }],
            open: function() {

                $j('input#company').autocomplete({
                    source : 'searchclienttags.json',
                    minLength : 3,
                    select : function(event, ui) {
                        var companyId = ui.item ? ui.item.id : "0";
                        $j("#companyid").val(companyId);
                        populateSubdivisionSelect(companyId);
                    },
                    change: function(event, ui) {
                        //If the field looses focus and a client wasn't chosen from the autocomplete then empty the text and id fields.
                        // But only if the company id has changed from the contract's company id.
                        //Forces the user to select from the list.
                        var clientCompanyId = $j("input#clientCompanyId").val();
                        var chosenId = $j("input#companyid").val();
                        var clientCompanyName = $j("span#clientCompanyName").text();
                        var chosenCompanyName = $j("input#company").val();
                        if ((ui.item == null || ui.item === undefined) && (chosenId !== clientCompanyId || chosenCompanyName !== clientCompanyName)) {
                            $j('input#company').val("");
                            $j("#comapnyid").val("");
                        }
                    }
                });
                populateSubdivisionSelect($j("#companyid").val());
            }
        });
    }
}

function instrumentSearchResultsDialog() {

    if($j("#instsearchresultsdialog").is(":data(dialog)")) {
        $j("#instsearchresultsdialog").dialog("open");
    } else {
        $j("#instsearchresultsdialog").dialog({
            dialogClass: "dialog-no-close",
            autoOpen: true,
            modal: true,
            width: 850,
            height: 600,
            buttons: [
                {
                    text: i18n.t("common:close", "Close"),
                    click: function () {
                        $j(this).dialog("close");
                    }
                },
                {
                    text: i18n.t("core.contract:changeclient", "Change Client"),
                    click: function () {
                        $j(this).dialog("close");
                        instrumentSearchDialog();
                    }
                }],
            open: function () {
                if($j.fn.dataTable.isDataTable("#instSearchResultsTable")) {
                    $j("#instSearchResultsTable").DataTable().ajax.reload(null,false);
                } else {
                    $j("#instSearchResultsTable").DataTable({
                        "processing": true,
                        "serverSide": true,
                        "pageLength": 10,
                        "ajax": (data,callback,settings) => {
                            const contractId = $j("input#contractid").val();
                            const companyId = $j("div#instsearchdialog input#companyid").val();
                            const subdivId = $j("div#instsearchdialog select#subdivid").val();
                            const url = cwms.urlGenerator("contract/listunlinkedinstruments");
                            url.search = cwms.paramStrgify.toSearchParams({contractId, companyId, subdivId, ...data});
                            fetch(url).then(res => res.ok?res.json():Promise.reject(res.statusText))
                            .then(callback);
                        },
                        "columns": [
                            {
                                "data": "plantId",
                                "width": "15%"
                            },
                            {
                                "data": "instrumentName",
                                "width": "30%"
                            },
                            {
                                "data": "serialNo",
                                "width": "20%"
                            },
                            {
                                "data": "subdivName",
                                "width": "30%"
                            },
                            {
                                "data": null,
                                "render": function (data, type, row, meta) {
                                    return "<button class='add' data-instrumentid='" + row['plantId'] + "'>" + i18n.t("common:add", "Add") + "</button>";
                                },
                                "orderable": false,
                                "width": "5%",
                                "className": "dt-center"
                            }
                        ]
                    });
                }
            }
        });
    }

}

function populateSubdivisionSelect(companyId){

    if(companyId > 0) {

        var response = $j.getJSON("subdivlookup/listforcompany/" + companyId);

        response.done(function (subdivs) {
            var content = '<option value="0">' + i18n.t("all","All") + '</option>';
            if (subdivs.length > 0) {
                // add all new subdivisons options to variable
                $j.each(subdivs, function (i) {
                    content += '<option value="' + subdivs[i].key + '">' + subdivs[i].value + '</option>';
                });
            }
            console.log("content: " + content);
            // empty select and add new options
            $j('select#subdivid').empty().append(content);
        });

        response.fail(function (jqXHR, textStatus) {
            alert("Request failed: " + textStatus);
        });

    }
}

function addInstrument(plantId) {

    var contractId = $j("input#contractid").val();

    var response = $j.ajax({
        method: "POST",
        url: "contract/addinstrument",
        dataType: "json",
              "beforeSend" : function(xhr) {
              xhr.setRequestHeader(window.cwms.metaDataObtainer.csrfHeaderParameter,
                window.cwms.metaDataObtainer.csrfToken
                );
            },
        data: {
            contractid: contractId,
            instrumentid: plantId
        }
    });

    response.done(function(){
        $j("#instrumentListTable").DataTable().ajax.reload(null,false);
        $j("#instSearchResultsTable").DataTable().ajax.reload(null,false);
    });

    response.fail(function (jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });

}

function removeInstrument(plantId) {

    var contractId = $j("input#contractid").val();

    var response = $j.ajax({
        method: "POST",
        url: "contract/removeinstrument",
        dataType: "json",
        data: {
            contractid: contractId,
            instrumentid: plantId
        }

    });

    response.done(function(){ $j("#instrumentListTable").DataTable().ajax.reload(null,false) });

    response.fail(function (jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });

}