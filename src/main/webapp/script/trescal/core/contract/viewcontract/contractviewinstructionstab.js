function initializeTab(){

    $j("#addeditinstructiondialog").dialog({
        dialogClass: "dialog-no-close",
        autoOpen: false,
        modal: true,
        width: 750,
        buttons: [
            {
                text: i18n.t("common:close", "Close"),
                click: function () {
                    $j(this).dialog("close");
                }
            },
            {
                text: i18n.t("common:add", "Add"),
                "id": "btnSelect",
                click: function () {
                    addEditInstruction($j(this).data('instructionId'));
                    $j(this).dialog("close");
                }
            }],
        open: function() {
            var instructionId = $j(this).data('instructionId');
            if(instructionId > 0) {
                populateAddEditInstructionDialog(instructionId);
            }
        }
    });

    $j("button#addinstruction").on("click", function () {
        $j("#addeditinstructiondialog").dialog().data('instructionId', 0);
        $j("#addeditinstructiondialog").dialog("open");
    });

    $j("a#editinstruction").on("click", function(event) {
        event.preventDefault();
        var btnName = i18n.t("common:save", "Save");
        $j('#addeditinstructiondialog').dialog().data('instructionId', $j(this).data('instructionid'));
        $j('#btnSelect').html('<span class="ui-button-text">' + btnName + '</span>');
        $j("#addeditinstructiondialog").dialog("open");
    });

    $j("a#deleteinstruction").on("click", function(event) {
        event.preventDefault();
        deleteInstruction($j(this).data('instructionid'));
    });
}

function addEditInstruction(instructionId) {

    var response = $j.ajax({
        method: "POST",
        url: "contract/addeditinstruction",
        dataType: "json",
        data: {
            instructionid: instructionId,
            contractid: $j("input#contractid").val(),
            instructiontype: $j("select#instructiontype").val(),
            clientDeliveryNote : $j("input#clientdelivery").prop('checked'),
            supplierDeliveryNote : $j("input#supplierdelivery").prop('checked'),
            instruction: $j("textarea#instructiontext").val()
        }
    });

    response.done(function () {
        $j("#addeditinstructiondialog").dialog("destroy");
        $j('#instructions').trigger('click');
    });

    response.fail(function( jqXHR, textStatus ) {
        alert( "Request failed: " + textStatus );
    });

}

function deleteInstruction(instructionId) {

    var response = $j.ajax({
        method: "POST",
        url: "contract/deleteinstruction",
        dataType: "json",
        data: {
            instructionid: instructionId,
            contractid: $j("input#contractid").val()
        }
    });

    response.done(function () {
        $j('#instructions').trigger('click');
    });

    response.fail(function( jqXHR, textStatus ) {
        alert( "Request failed: " + textStatus );
    });


}

function populateAddEditInstructionDialog(instructionId) {

    var response = $j.ajax({
        method: "GET",
        url: "contract/" + $j("input#contractid").val() + "/instruction/" + instructionId,
        dataType: "json"
    });

    response.done(function (instruction) {
        console.log(instruction);
        $j("#addeditinstructiondialog select#instructiontype").val(instruction.instructionType);
        
        if (instruction.clientDeliveryNote)
        	$j("#addeditinstructiondialog input#clientdelivery").prop('checked', true);
        else
        	$j("#addeditinstructiondialog input#clientdelivery").removeAttr('checked');

        if (instruction.supplierDeliveryNote)
        	$j("#addeditinstructiondialog input#supplierdelivery").prop('checked', true);
        else
        	$j("#addeditinstructiondialog input#supplierdelivery").removeAttr('checked');
        
        $j("#addeditinstructiondialog textarea#instructiontext").val(instruction.instructionText);
    });

    response.fail(function( jqXHR, textStatus ) {
        alert( "Request failed: " + textStatus );
    });


}

