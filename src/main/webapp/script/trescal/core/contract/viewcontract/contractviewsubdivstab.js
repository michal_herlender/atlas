

function initializeTab(){

    $j("#addsubdivbutton").on("click", function(){
        $j("#subdivchoicedialog").dialog("open");
    });

    $j("#subdivtablebody").on("click", "button.remove", function(){
        removeSubdiv($j(this).data("subdivid"));
    });

    $j( "#subdivchoicedialog" ).dialog({
        dialogClass: "dialog-no-close",
        autoOpen: false,
        modal: true,
        width: 500,
        buttons: [
            {
                text: i18n.t("common:close", "Close"),
                click: function () {
                    $j(this).dialog("close");
                }
            },
            {
                text: i18n.t("core.contract:addselected", "Add Selected"),
                click: function () {
                    var subdivIds = [];
                    $j("#subdivchoicedialog :checked").each(function(){
                        subdivIds.push($j(this).val());
                    });
                    addSubdivs(subdivIds);
                    $j(this).dialog("close");
                }
            }
        ]
    });
}


function addSubdivs(subdivIds){

    var contractId = $j('#contractid').val();
    var payload = {
        contractId: contractId,
        subdivIds: subdivIds
    };

    var response = $j.ajax({
        url: "contract/addsubdivs",
        type: "POST",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(payload)
    });

    response.done(function() {
        $j("#subdivchoicedialog").dialog("destroy");
        $j('#subdivs').trigger('click');
    });

    response.fail(function( jqXHR, textStatus ) {
        alert( "Request failed: " + textStatus );
    });

}

function removeSubdiv(subdivId){

    var contractId = $j('#contractid').val();

    var response = $j.ajax({
        url: "contract/removesubdiv",
        type: "POST",
        data: {
            contractid: contractId,
            subdivid: subdivId
        }
    });

    response.done(function() {
        $j("#subdivchoicedialog").dialog("destroy");
        $j('#subdivs').trigger('click');
    });

    response.fail(function( jqXHR, textStatus ) {
        alert( "Request failed: " + textStatus );
    });

}

