function initializeTab() {

	$j("#adddemandbutton").on("click", function() {
		addDemandDialog();
	});

	$j("#demandTable").DataTable(
			{
				processing : true,
				serverSide : true,
				pageLength : 10,
				ajax:(data,callback,settings) => {
					const contractId = $j("input#contractid").val();
					const url = cwms.urlGenerator("contract/listdemands")
					url.search = cwms.paramStrgify.toSearchParams({contractId, ...data})
					fetch(url).then(res => res.ok?res.json():Promise.reject(res.statusText))
					.then(callback)
				},
				rowId : function(data) {
					return 'demand' + data.id;
				},
				columns : [
						{
							data : "domain",
							width : "30%"
						},
						{
							data : "subfamily",
							width : "30%"
						},
						{
							data : "demands.CLEANING",
							render : function(data, type, row) {
								return '<input type="checkbox" '
										+ (data ? 'checked' : '')
										+ ' onclick="return false;"/>';
							},
							width : "10%"
						},
						{
							data : "demands.SEALING",
							render : function(data, type, row) {
								return '<input type="checkbox" '
										+ (data ? 'checked' : '')
										+ ' onclick="return false;"/>';
							},
							width : "10%"
						},
						{
							data : "demands.ENGRAVING",
							render : function(data, type, row) {
								return '<input type="checkbox" '
										+ (data ? 'checked' : '')
										+ ' onclick="return false;"/>';
							},
							width : "10%"
						},
						{
							data : null,
							defaultContent : "<button class='remove'>"
									+ i18n.t("common:remove", "Remove")
									+ "</button>",
							orderable : false,
							width : "10%",
							className : "dt-center"
						} ]
			});

	$j("#demandTable tbody").on('click', 'button.remove', function() {
		var row = $j("#demandTable").DataTable().row($j(this).parents('tr'));
		deleteContractDemand(row.data().id);
	});
}

function addDemandDialog() {
	if ($j("#adddemanddialog").is(":data(dialog)")) {
		$j("#adddemanddialog").dialog("open");
	} else {
		$j("#adddemanddialog").dialog({
			dialogClass : "dialog-no-close",
			autoOpen : true,
			modal : true,
			width : 750,
			buttons : [ {
				text : i18n.t("common:add", "Add"),
				click : function() {
					$j.ajax({
						type: 'POST',
						url: "contract/adddemand",
						data: {
							contractId: $j("input#contractid").val(),
							domainId: $j('input#domainId').val(),
							subfamilyId: $j('input#subfamilyId').val(),
							cleaning: $j('input#cleaning').prop('checked'),
							sealing: $j('input#sealing').prop('checked'),
							engraving: $j('input#engraving').prop('checked')
						}
					}).done(function(succeed) {
						$j('#demandTable').DataTable().ajax.reload(null, false);
						$j("#adddemanddialog").dialog("close");
					});
				}
			}, {
				text : i18n.t("common:close", "Close"),
				click : function() {
					$j("#adddemanddialog").dialog("close");
				}
			} ],
			open : function() {
				$j('input#domain').autocomplete({
                    source : 'searchdomaintags.json',
                    minLength : 2,
                    select : function(event, ui) {
                    	$j("input#domainId").val(ui.item.id);
    					$j("input#domain").val(ui.item.label);
                    }
                });
				$j('input#subfamily').autocomplete({
                    source : 'searchdescriptiontags.json',
                    minLength : 3,
                    select : function(event, ui) {
                    	$j("input#subfamilyId").val(ui.item.id);
    					$j("input#subfamily").val(ui.item.label);
                    }
                });
			}
		});
	}
}

function deleteContractDemand(demandId) {
	$j.ajax({
		type : 'POST',
		url : 'contract/deletedemand',
		data : {
			demandId : demandId
		},
		async : true
	}).done(function(succeed) {
		if (succeed)
			$j('#demandTable').DataTable().ajax.reload(null, false);
	});
}