
var pageImports = ['script/trescal/core/quotation/LinkQuotationUtility.js'];

//var jqCalendar = [
//    {
//        inputField: 'startDate',
//        dateFormat: 'yy-mm-dd',
//        displayDates: 'all',
//        showWeekends: true
//    },
//    {
//        inputField: 'endDate',
//        dateFormat: 'yy-mm-dd',
//        displayDates: 'all',
//        showWeekends: true
//    }];

function init() {

    $j('#choosebpodialog').dialog({
        dialogClass: "dialog-no-close",
        autoOpen: false,
        modal: true,
        width: 750,
        buttons: [
        {
            text: i18n.t("common:close", "Close"),
            click: function () {
                $j(this).dialog("close");
            }
        }]
    });

    $j('#linkQuote').on('click',function(event){
        event.preventDefault();
        var companyID = $j("[name='clientCompanyId']").val();
        if( companyID > 0 ) {
            linkQuotation(null, companyID, null, false, false, 'contract');
        }
    });

    $j('#linkBpo').on('click', function(event) {
        event.preventDefault();
        var companyID = $j("[name='clientCompanyId']").val();
        if( companyID > 0 ) {
            $j('#choosebpodialog').dialog('open');
        }
    });
    
    $j('#choosebpodialog').on('click','button.linkbpo',function(){
    	$j('input#bpoNumber').val($j(this).data('bponumber'));
    	$j('input#bpoId').val($j(this).data('bpoid'));
    	$j('#choosebpodialog').dialog('close');
    })
    
    if($j("[name='clientCompanyId']").val() > 0){
    	getBPOs();
    }

}

/*
This function is called by the CompanySearchJQPlugin when a company is chosen.
 */
function companySearchJQPluginMethod() {
    getContacts();
    getCurrency();
    getBPOs();
}


/**
 * Get the contacts based on the client company selected.
 */
function getContacts() {

    var companyID = $j("[name='clientCompanyId']").val();
    var defaultContactId;
    var businessContactId;

    var clientContactResponse = $j.getJSON("contactlookup/defaultforclient/" + companyID);

    clientContactResponse.done(function(clientid) {
        defaultContactId = clientid;
        console.log("contact: " + defaultContactId);

        var businessContactResponse = $j.getJSON("contactlookup/defaultBusinessCompanyContactForClient/" + companyID);

        businessContactResponse.done(function(businessid) {
            businessContactId = businessid;
            console.log("business contact: " + businessContactId);

            $j("#trescalManagerId").val(businessContactId);
            $j("#invoicingManagerId").val(businessContactId);


            var contactListResponse = $j.getJSON("contactlookup/listallatcompany/" + companyID);

            contactListResponse.done(function (cons) {
                var content = '';
                if (cons.length > 0) {

                    // add all new contact options to variable
                    $j.each(cons, function (i) {
                        content += '<option value="' + cons[i].key + '"';
                        if(cons[i].key === defaultContactId) {
                            content += ' SELECTED ';
                        }
                        content += '>' + cons[i].value + '</option>';
                    });
                }
                // empty select and add new options
                $j('#contact').empty().append(content);
            });

            contactListResponse.fail(function (jqXHR, textStatus) {
                alert("Request failed: " + textStatus);
            });
        });

        businessContactResponse.fail(function (jqXHR, textStatus) {
                alert("Request failed: " + textStatus);
        });

    });

    clientContactResponse.fail(function (jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });




}


/**
 * Get the currency symbol based on the client company selected
 */

function getCurrency() {

    var companyID = $j("[name='clientCompanyId']").val();
    var response = $j.get("currencylookup/currencysymbolforcompany/" + companyID);

    response.done(function (currencyKeyValue) {
        $j('.currencysymbol').html(currencyKeyValue.value);
        $j('#currencyId').val(currencyKeyValue.key);
    });

    response.fail(function (jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
}

/**
 * get active BPOs for client company once it's selected.
 */
function getBPOs() {

    var companyId = $j("[name='clientCompanyId']").val();

    var response = $j.get("contract/listactivebposforcompany/" + companyId);

    response.done(function(bpos) {
        if(bpos.length > 0) {
            var tableBody = "<tbody>";
            $j.each(bpos, function (i) {
                tableBody += "<tr><td class='bponumber'>" + bpos[i].bpoNumber + 
                	"</td><td class='date'>" + bpos[i].startDate + "</td><td class='date'>" + bpos[i].endDate + 
                	"</td><td class='value'>" + bpos[i].value + "</td><td class='value'>" + bpos[i].spend + 
                	"</td><td class='action'><button class='linkbpo' data-bpoid='" + bpos[i].bpoId + "' data-bponumber='" + bpos[i].bpoNumber + "'>" + 
                	i18n.t("common:link", "Link") + "</button></tr>";
            })
            tableBody += "</tbody>";
            $j('#choosebpodialog tbody').replaceWith(tableBody);
        }
    })

    response.fail(function (jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
}

function removeQuotation() {
	$j('#quoteId').val('');
	$j('#quoteNumber').val('');
}

function removeBPO() {
	$j('#bpoId').val('');
	$j('#bpoNumber').val('');
}