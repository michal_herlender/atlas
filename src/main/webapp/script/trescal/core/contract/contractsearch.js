

//This function is called by the CompanySearchJQPlugin when a company is chosen.
function companySearchJQPluginMethod() {
    getsubdivisons();
}

function getsubdivisons() {

    var companyID = $j("[name='clientCompanyId']").val();

    var response = $j.getJSON("subdivlookup/listforcompany/" + companyID);

    response.done(function (subdivs) {
        var content = '<option value="0">' + i18n.t("all","All") + '</option>';
        if (subdivs.length > 0) {

            // add all new subdivisons options to variable
            $j.each(subdivs, function (i) {
                content += '<option value="' + subdivs[i].key + '">' + subdivs[i].value + '</option>';
            });
        }
        // empty select and add new options
        $j('#subdiv').empty().append(content);
    });

    response.fail(function (jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });

}


