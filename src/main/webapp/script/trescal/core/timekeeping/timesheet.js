/**
 * array of objects containing the element name and corner type which should be
 * applied. applied in init() function.
 */
var niftyElements = new Array({
	element : 'div.infobox',
	corner : 'normal'
}, {
	element : 'ul.subnavtab a',
	corner : 'small transparent top'
});

function createValidation(date) {
	$j.prompt("There is no possibility to add or change time sheet entries after creation. You'd like to create a time sheet for validation now?", {
		submit: function(event, confirm) {
			if(confirm)
				$j.ajax({
					url: "timesheet.htm",
					data: {
						validation: "create",
						date: date
					},
					async: true
				}).done(function(result) {
					$j("tr.createentry").remove();
					$j("button#validateButton").remove();
				});
		},
		buttons: {
			Yes: true,
			No: false
		},
		focus: 1
	});
}

function unhideTimeRelated() {
	$j("tbody#timeRelatedBody").removeClass("hid");
	$j("img#unhideTimeRelatedImg").addClass("hid");
	$j("img#hideTimeRelatedImg").removeClass("hid");
	$j("tr#timeRelatedHeadRow").removeClass("hid");
}

function hideTimeRelated() {
	$j("tbody#timeRelatedBody").addClass("hid");
	$j("img#unhideTimeRelatedImg").removeClass("hid");
	$j("img#hideTimeRelatedImg").addClass("hid");
	$j("tr#timeRelatedHeadRow").addClass("hid");
}

function unhideNotTimeRelated() {
	$j("tbody#notTimeRelatedBody").removeClass("hid");
	$j("img#unhideNotTimeRelatedImg").addClass("hid");
	$j("img#hideNotTimeRelatedImg").removeClass("hid");
	$j("tr#notTimeRelatedHeadRow").removeClass("hid");
}

function hideNotTimeRelated() {
	$j("tbody#notTimeRelatedBody").addClass("hid");
	$j("img#unhideNotTimeRelatedImg").removeClass("hid");
	$j("img#hideNotTimeRelatedImg").addClass("hid");
	$j("tr#notTimeRelatedHeadRow").addClass("hid");
}