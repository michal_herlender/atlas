/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	ViewTPQuoteRequest.js
*	DESCRIPTION		:	Page specific javascript for the viewtpquoterequest velocity page.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	04/06/2007 - SH - Created File.
*					:	28/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array ( 	'dwr/interface/tpquoterequestitemservice.js',
								'dwr/interface/costtypeservice.js',
								'script/trescal/core/utilities/DocGenPopup.js',
								'script/trescal/core/tools/FileBrowserPlugin.js');

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' },
								{ element: 'ul.subnavtab a', corner: 'small transparent top' } );

/**
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = 	new Array( { anchor: 'tpquoteitems-link', block: 'tpquoteitems-tab' },
					      	   { anchor: 'editquote-link', block: 'editquote-tab' },
					      	   { anchor: 'quotedocs-link', block: 'quotedocs-tab' },
							   { anchor: 'quoteemails-link', block: 'quoteemails-tab' },
							   { anchor: 'instructions-link', block: 'instructions-tab' } );

/**
 * Deletes a TPQuoteRequestItem from a TPQuoteRequest.
 * @param (itemid) the id of the TPQuoteRequestItem to delete
 */
function deleteTPQRItem(itemid)
{
	var dfb = itemid;
	tpquoterequestitemservice.deleteTPQuoteRequestItem(itemid, 
	{
			callback:function(dataFromServer)
			{
				cleanUpDeleteTPQRItem(dataFromServer, dfb);
			}
		}
	);
}

/**
 * Callback function that tidies up after a TPQuoteRequestItem deletion.
 */
function cleanUpDeleteTPQRItem(dfs, dfb)
{
	// remove the TPQuoteRequestItem from the page
	$j('tr').remove('#item' + dfb);
}

/**
 * Remove a cost type from a TPQuoteRequestItem
 * @param (typeid) the id of the CostType to remove
 * @param (itemid) the id of the TPQuoteRequestItem to remove the CostType from
 */
function removeCostType(typeid, itemid)
{
	var dfb = new Array(typeid, itemid);
	tpquoterequestitemservice.removeCostType(typeid, itemid,
		{
			callback:function(dataFromServer)
			{
				cleanUpRemoveCostType(dataFromServer, dfb, itemid);
			}
		}
	);		
}

/**
 * Callback function that tidies up after a CostType has been removed from a TPQuoteRequestItem
 * @param (dfs) data passed back from the server
 * @param (dfb) array containing the typeid of the TPQuoreRequestItem and the itemid of the CostType
 */
function cleanUpRemoveCostType(dfs, dfb)
{
	// remove the span containing the cost type from the document
	$j('span').remove('#cost' + dfb[0] + 'item' + dfb[1]);
	// display the link which shows available cost types
	$j('#showavailablecosttypes' + dfb[1]).removeClass().addClass('vis');
	// display message that no cost types have been added
	if ($j('#itemcosttypes' + dfb[1] + ' span').length == 1)
	{
		$j('#nocosttype' + dfb[1]).removeClass().addClass('vis');
	}
}

/**
* For the given item show a list of available CostTypes
* i.e. show the a list of CostTypes not yet assigned to the item
* @param (itemid) the id of the TPQuoteRequestItem
*/
function showAvailableCostTypes(itemid)
{
	// only make the ajax call if the user hasn't already done so 
	// (i.e. if the CostTypes list for THIS item does not already exist)
	if($j('#newcosts' + itemid).length == 0)
	{
		tpquoterequestitemservice.getAvailableTPQuoteRequestCostTypeDtos(itemid,
		{
				callback:function(dataFromServer)
				{
					cleanUpShowAvailableCostTypes(dataFromServer, itemid);
				}
			}
		);
	}	
}

/**
 * Callback from #showAvailableCostTypes
 * @param (dfs) List of CostType objects
 * @param (dfb) the id of the TPQuoteRequestItem
 */
function cleanUpShowAvailableCostTypes(dfs, itemid)
{
	// create span to wrap all necessary elements for showing available cost types
	$j('#costtypes' + itemid).append(	'<span id="newcosts' + itemid + '">' +
											// create a select list displaying the available cost types
											'<select name="addcosttype' + itemid + '" id="addcosttype' + itemid + '">' +
												// OPTIONS APPENDED BELOW
											'</select>' +
											// add a link to add the selected cost type
											'<a href="#">' +
												'<img src="img/icons/bullet_add.png" width="7" height="7" title="' + i18n.t("core.tpcosts:addCostType", "Add cost type") + '" alt="' + i18n.t("core.tpcosts:addCostType", "Add cost type") + '" />' +
											'</a>' +
											'&nbsp;&nbsp;' +
											// add a close link to remove the select options and make the add cost type link visible
											'<a href="#" onclick=" $j(\'span\').remove(\'#newcosts\'' + itemid + '); $j(\'#showavailablecosttypes\'' + itemid + ').removeClass().addClass(\'vis\'); return false; " >' +
												'<img src="img/icons/bullet_delete.png" width="7" height="7" alt="' + i18n.t("core.tpcosts:removeCostTypeList", "Remove cost type list") + '" title="' + i18n.t("core.tpcosts:removeCostTypeList", "Remove cost type list") + '" />' +
											'</a>' +
										'</span>');
	
	// append caltype options to the select box created above					
	$j.each(dfs, function(i){
		$j('#newcosts' + itemid + ' select').append('<option value="' + dfs[i].key + '">' + dfs[i].value + '</option>');
	});
	
	// bind an onclick event to the anchor above which adds the selected cost type
	$j('#newcosts' + itemid + ' a:first').bind('click', function(){
		
		if($j('#addcosttype' + itemid + ' option').length > 0)
		{
			addCostType($j('#addcosttype' + itemid).val(), $j('#addcosttype' + itemid + ' option:selected').text(), itemid);
			if ($j('#addcosttype' + itemid + ' option').length == 1)
			{
				$j('#showavailablecosttypes' + itemid).removeClass().addClass('hid');
			}
			else
			{
				$j('#showavailablecosttypes' + itemid).removeClass().addClass('vis');
			}
			return false;
		}
		
	});
	
	// hide the link and image which shows available cost types
	$j('#showavailablecosttypes' + itemid).removeClass().addClass('hid');
	
}

/**
 * Add the given CostType to the given TPQuoteRequestItem
 * @param (typeid) the id of the CostType
 * @param (typename) the name of the CostType being added
 * @param (itemid) the id of the TPQuoteRequestItem
 */
function addCostType(typeid, typename, itemid)
{
	// if message that no cost types have been added is visible then hide
	if ($j('#nocosttype' + itemid).attr('class') == 'vis')
	{
		// hide message
		$j('#nocosttype' + itemid).removeClass().addClass('hid');
	}
	
	// create object for this type
	var dfb = { typeid: typeid, typename: typename, itemid: itemid };
	
	tpquoterequestitemservice.addCostType(typeid, itemid, 
	{
			callback:function(dataFromServer)
			{
				cleanUpAddCostType(dataFromServer, dfb);
			}
		}
	);		
}

/**
 * Callback method from #addCostType which will display a new CostType
 * added to the TPQuoteRequestItem.
 * @param (dfs) null
 * @param (dfb) object literal containing (TPQuoteRequestItem) itemid, (CostType) typeid, (CostType) typename
 */
function cleanUpAddCostType(dfs, dfb)
{
	// remove the options list and area
	$j('span').remove('#newcosts' + dfb.itemid);
	
	// add the cost type to the cost types list 
	$j('#itemcosttypes' + dfb.itemid).append(	'<span id="cost' + dfb.typeid + 'item' + dfb.itemid + '">&nbsp;' +
													dfb.typename + '&nbsp;' +
													'<a href="#" onclick=" removeCostType(' + dfb.typeid + ', ' + dfb.itemid + '); return false; ">' +
														'<img src="img/icons/bullet_delete.png" width="7" height="7" alt="' + i18n.t("core.tpcosts:removeCostType", "Remove cost type") + '" title="' + i18n.t("core.tpcosts:removeCostType", "Remove cost type") + '" />' +
													'</a> ' +
												'</span>');
}

/**
 * this function appends the div needed to create a cascading search plugin to the page and
 * calls the initialisation function which builds the search plugin and prefills any data if needed.
 * 
 * @param {String} coid is  the company id to be preloaded
 * @param {Integer} subdivid is the subdivision id to be preloaded
 * @param {Integer} personid is the contact id to be preloaded
 */
function createCascadingSearchPlugin(coid, subdivid, personid)
{
	// has the cascading search plugin already been created
	if ($j('div#cascadeSearchPlugin').length)
	{
		// do nothing
	}
	else
	{
		// load the service javascript file if not already	
		loadScript.loadScriptsDynamic(new Array('script/trescal/core/utilities/searchplugins/CascadeSearchPlugin.js'), true,
		{
			callback: function()
			{
				// remove the hidden input field for contact currently on page
				// append div in which the multi model search plugin will be created
				$j('input#tempId').after(	'<div id="cascadeSearchPlugin">' +
												'<input type="hidden" id="compCoroles" value="client,prospect" />' +
												'<input type="hidden" id="cascadeRules" value="subdiv,contact" />' +
												'<input id="prefillIds" type="hidden" value="' + coid + ',' + subdivid + ',' + personid + '" />' +
											'</div>' +																														
											// clear floats and restore page flow
											'<div class="clear"></div>').parent().removeClass().addClass('vis').end().remove();
				// initialise the multi model search plugin
				CascadeSearchPlugin.init();
				// change name of personid input
				$j('#cascadeSearchPlugin input[name="personid"]').attr('name', 'contactId');
			}
		});
	}
}