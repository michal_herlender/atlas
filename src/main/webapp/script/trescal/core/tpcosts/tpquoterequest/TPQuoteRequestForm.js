/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	TPQuoteRequestForm.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	01/06/2007 - SH - Created File
*					:	28/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/



/**
 * this method checks all inputs of type checkbox that are contained within the table 
 * 
 * @param {Boolean} select true if boxes are to be checked and vice versa
 * @param {String} parentElement id of table in which to search for inputs of type checkbox
 */
function selectAllItems(select, parentElement)
{
	// check all
	if(select == true)
	{
		$j('#' + parentElement + ' input:checkbox').check('on');
	}
	// uncheck all
	else
	{
		$j('#' + parentElement + ' input:checkbox').check('off');
	}
}

/**
 * function used in the multi contact search plugin.  The chosen contact in the list is added to the contact name section
 * along with the submit button for the form.
 */
function customFunc(name, personid, subname, coname)
{
	// empty the contact name section
	$j('#contactname').empty();
	// append the contacts name and company info along with the submit button (disabled)
	$j('#contactname').append(i18n.t("core.tpcosts:creatingTPQuotationRequest", {varName : '<strong>' + name + '</strong>', varConame : coname + ' (' + subname + ')', defaultValue : "Creating third party quotation request to <strong>" + name + "</strong> of " + coname + " (" + subname + ")"}) + ' <input type="submit" name="submit" id="submit" value="' + i18n.t("continue", "Continue") + '" disabled="disabled" />');
	setTimeout("$j('#contactname :submit').attr('disabled', false);", 200);
	return false;
}