/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	TPQuoteRequestAddModel.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	23/07/2007 - SH - Created File.
*
****************************************************************************************************************************************/


/*
 * Array of objects containing the element name and corner type which should be applied.
 * Applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' },
								{ element: 'div.results', corner: 'normal' } );
								
/*
 * Element to be focused on when the page loads.
 * Applied in init() function.
 */
var focusElement = 'mfr';

/**
 * Array of autocomplete objects (multiple are possible) containing setup for 
 * jQueryUI initialization.
 * this is used in the init() function.
 */
var jquiAutocomplete = new Array(
		{
			inputFieldName: 'salesCat',
			inputFieldId: 'salesCatId',
			source: 'searchsalescategorytags.json'
		},
		{
			inputFieldName: 'familyNm', 
			inputFieldId: 'familyId',
			source: 'searchfamilytags.json'
		},
		{
			inputFieldName: 'domainNm', 
			inputFieldId: 'domainId',
			source: 'searchdomaintags.json'
		},
		{
			inputFieldName: 'descNm',
			inputFieldId: 'descId',
			source: 'searchdescriptiontags.json'
		});
