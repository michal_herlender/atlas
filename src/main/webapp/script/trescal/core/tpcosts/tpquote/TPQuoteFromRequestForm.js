/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	TPQuoteFromRequestForm.js
*	DESCRIPTION		:	Page specific javascript file.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	16/07/2008 - SH - Created File
*
****************************************************************************************************************************************/


var focusElement = 'tpqno';

/**
 * this method checks all inputs of type checkbox of the name provided 
 * 
 * @param {Boolean} check true if boxes are to be checked and vice versa
 * @param {String} name the name of checkbox inputs to be selected
 */
function selectAllItems(check, name)
{
	// check all
	if(check)
	{
		$j('input[type="checkbox"]').each(function() {
		     this.checked = true;
		  });
	}
	// uncheck all
	else
	{
		$j('input[type="checkbox"]').each(function() {
		     this.checked = false;
		  });
	}
}
