/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	EditBulkTPQuotationItem.js
*	DESCRIPTION		:	-
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	15/07/2008 - SH - Created File
*
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * on page load a list of all CostTypes for this TPQuotationItem is loaded.
 * costs that are not included are hidden from view. This function toggles
 * the visibility of the CostTypes as they are 'added/removed' by the user.
 * 
 * @param {Object} typeId
 * @param {Object} typeName
 * @param {Object} itemid
 * @param {Object} iteration
 */
function addRemoveCostType(typeId, typeName, itemid, iteration)
{
	// check typeId
	if (typeId != -1)
	{
		// is the selected cost active?
		if($j('#tpQitems\\[' + iteration + '\\]\\.' + typeName + '\\.active').val() == 'true')
		{
			// set the cost type inactive
			$j('#cost' + itemid + typeId).removeClass().addClass('hid');
			$j('#tpQitems\\[' + iteration + '\\]\\.' + typeName + '\\.active').val(false);		
			// get the name of the cost
			typeName = typeName.substring(0, typeName.indexOf('Cost'));
			// any cost types in select
			if ($j('#remCosts' + itemid + ' option').length < 1)
			{
				$j('#availableCosts' + itemid).removeClass().addClass('vis');
			}
			// add the cost to the available list
			$j('#remCosts' + itemid).append('<option value="' + typeId + '">' + typeName + '</option>');			
			// are there any cost types active left?
			if($j('#costs' + itemid + ' li[id^="cost"][class="vis"]').length < 1)
			{
				// make message of no active cost types visible
				$j('#noCosts' + itemid).removeClass().addClass('vis');
			}				
			// reset the hidden values back to 0.00 --> so they don't fail validation
			$j('#tpQitems\\[' + iteration + '\\]\\.' + typeName + 'Cost\\.totalCost' + itemid).val('0.00');
			$j('#tpQitems\\[' + iteration + '\\]\\.' + typeName + 'Cost\\.discountRate' + itemid).val('0.00');			
		}
		else
		{
			// are there any cost types active?
			if($j('#costs' + itemid + ' li[id^="cost"][class="vis"]').length < 1)
			{
				// make message of no active cost types hidden
				$j('#noCosts' + itemid).removeClass().addClass('hid');
			}
			// set the cost type active
			$j('#cost' + itemid + typeId).removeClass().addClass('vis');
			$j('#tpQitems\\[' + iteration + '\\]\\.' + typeName + '\\.active').val(true);			
			// remove the item from the available list
			$j('#remCosts' + itemid + ' option[value="' + typeId + '"]').remove();
			// any cost types left in select
			if ($j('#remCosts' + itemid + ' option').length < 1)
			{
				$j('#availableCosts' + itemid).removeClass().addClass('hid');
			}
		}
	}
	else
	{
		// do nothing
	}
}