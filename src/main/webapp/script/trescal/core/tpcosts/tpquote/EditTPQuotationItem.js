/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	EditTPQuotationItem.js
*	DESCRIPTION		:	-
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	14/07/2008 - SH - Created File
*
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * on page load a list of all CostTypes for this TPQuotationItem is loaded.
 * costs that are not included are hidden from view. This function toggles
 * the visibility of the CostTypes as they are 'added/removed' by the user.
 * 
 * @param {String} typeId the id of the cost type to be actioned
 * @param {String} typeName the name of the cost type
 */
function addRemoveCostType(typeId, typeName)
{	
	// selected cost type currently active?	
	if($j('#tpQitem\\.' + typeName + '\\.active').val() == 'true')
	{
		// set the cost type inactive
		$j('#cost' + typeId).removeClass().addClass('hid');
		$j('#tpQitem\\.' + typeName + '\\.active').val(false);		
		// get the name of the cost
		typeName = typeName.substring(0, typeName.indexOf('Cost'));		
		// add the cost to the available list
		$j('#remCosts').append('<option value="' + typeId + '">' + typeName + '</option>');
		// are there any cost types active left?
		if($j('#activecosts li[id^="cost"][class="vis"]').length < 1)
		{
			// make message of no active cost types visible
			$j('#noCosts').removeClass().addClass('vis');
		}
	}
	else
	{
		// are there any cost types active?
		if($j('#activecosts li[id^="cost"][class="vis"]').length < 1)
		{
			// make message of no active cost types hidden
			$j('#noCosts').removeClass().addClass('hid');
		}
		// set the cost type active
		$j('#cost' + typeId).removeClass().addClass('vis');
		$j('#tpQitem\\.' + typeName + '\\.active').val(true);				
		// remove the item from the available list
		$j('#remCosts option[value="' + typeId + '"]').remove();
	}
}