/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	AddItemFromQuote.js
*	DESCRIPTION		:	Page specific javascript file.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	17/07/2008 - SH - Created File
*
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * this method checks all inputs of type checkbox of the name provided 
 * 
 * @param {Boolean} check true if boxes are to be checked and vice versa
 * @param {String} name the name of checkbox inputs to be selected
 */
function selectAllItems(check, name)
{
	// check all
	if(check == true)
	{
		$j('input:checkbox[name="' + name + '"]').check('on');
	}
	// uncheck all
	else
	{
		$j('input:checkbox[name="' + name + '"]').check('off');
	}
}