/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	ViewTPQuote.js
*	DESCRIPTION		:	-
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	12/09/2007
*					:	28/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'script/trescal/core/utilities/DOMUtils.js',							
								'script/trescal/core/utilities/DocGenPopup.js',
								'script/trescal/core/tools/FileBrowserPlugin.js');

								
/**
 * this variable holds an array of id objects, one for the navigation link and
 * one for the content area the link refers to. this array is passed to the
 * Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = 	new Array( { anchor: 'items-link', block: 'items-tab' },
					      	   { anchor: 'edit-link', block: 'edit-tab' },
							   { anchor: 'docs-link', block: 'docs-tab' },
							   { anchor: 'email-link', block: 'email-tab' } );
							   
/**
 * this method toggles the visibility of quote item cost summaries
 * 
 * @param {String}
 *            classname the string value of class name
 * @param {String}
 *            anchorId the id of the anchor being clicked
 * @param {Integer}
 *            itemid the id of the quote item
 */
function toggleCostSummaryDisplay(classname, anchorId, itemid)
{
	// hide the quote item costs?
	if (anchorId.indexOf('hide') != -1)
	{
		// show/hide all anchor?
		if (anchorId.indexOf('all') != -1)
		{
			// hide all cost summary rows
			$j('tr[class^="' + classname + '"]').removeClass('vis').addClass('hid');
			// change anchor id and image on all rows
			$j('td.item a').each(function(i)
			{
				// get current id of anchor
				var currentid = $j(this).attr('id');
				// get itemid from anchor id
				var itemno = currentid.substring(4, currentid.length);
				// remove old image, append new image and change id of anchor
				$j(this).empty()
						.attr('id', 'show' + itemno)
						.append('<img src="img/icons/showinfo.png" width="16" height="16" class="img_marg_bot" alt="' + i18n.t("showCostBreakdown", "Show cost breakdown") + '" title="' + i18n.t("showCostBreakdown", "Show cost breakdown") + '" />');
			});
		}
		else
		{
			// hide cost summary
			$j('tr[class^="' + classname + itemid + '"]').removeClass('vis').addClass('hid');
			// remove old image, append new image and change id of anchor
			$j('#' + anchorId).empty()
								.attr('id', 'show' + itemid)
								.append('<img src="img/icons/showinfo.png" width="16" height="16" class="img_marg_bot" alt="' + i18n.t("showCostBreakdown", "Show cost breakdown") + '" title="' + i18n.t("showCostBreakdown", "Show cost breakdown") + '" />');
		}
		
	}
	else
	{
		// show/hide all anchor?
		if (anchorId.indexOf('all') != -1) 
		{
			// show all cost summary rows, but not notes
			$j('tr[class^="' + classname + '"]:not([id^="tpqinote"])').removeClass('hid').addClass('vis');
			// change anchor id and image on all rows
			$j('td.item a').each(function(i)
			{
				// get current id of anchor
				var currentid = $j(this).attr('id');
				// get itemid from anchor id
				var itemno = currentid.substring(4, currentid.length);
				// remove old image, append new image and change id of anchor
				$j(this).empty()
						.attr('id', 'hide' + itemno)
						.append('<img src="img/icons/hideinfo.png" width="16" height="16" class="img_marg_bot" alt="' + i18n.t("hideCostBreakdown", "Hide cost breakdown") + '" title="' + i18n.t("hideCostBreakdown", "Hide cost breakdown") + '" />');
			});
		}
		else
		{
			// show cost summary
			$j('tr[class^="' + classname + itemid + '"]:not([id^="tpqinote"])').removeClass('hid').addClass('vis');
			// remove old image, append new image and change id of anchor
			$j('#' + anchorId).empty()
								.attr('id', 'hide' + itemid)
								.append('<img src="img/icons/hideinfo.png" width="16" height="16" class="img_marg_bot" alt="' + i18n.t("hideCostBreakdown", "Hide cost breakdown") + '" title="' + i18n.t("hideCostBreakdown", "Hide cost breakdown") + '" />');
		}		
	}	
}

/**
 * this function deletes the job costing from the database
 * 
 * @param {Integer}
 *            costingid is the id of the job costing to be deleted
 */
function deleteTPQuote(tpquoteid)
{
	if(confirm(i18n.t("core.tpcosts:confirmDeleteTPQuote", "Please note: this does not delete any documents produced for the third party quote, but the ENTIRE third party quote from the database. Click 'OK' to confirm the deletion or 'Cancel' to go back...")))
	{
		window.location.href = 'deletetpquote.htm?id=' + tpquoteid;
	}
}

/**
 * this method selects/de-selects all quote item delete checkboxes
 * 
 * @param {Boolean}
 *            indicates whether all delete quote item checkboxes should be
 *            checked
 * @param {Integer}
 *            headingId the id of the heading to check all items for
 * @param {Integer}
 *            caltypeId the id of the calibration type
 */
function toggleQIDeleteItems(check)
{
	// check all?
	if (check)
	{
		$j('input[type="checkbox"][name="qiDelete"]').check('on');
		monitorCheckedItems();
	}
	else
	{
		$j('input[type="checkbox"][name="qiDelete"]').check('off');
		monitorCheckedItems();
	}
}

/**
 * this method checks if any quote item delete checkboxes have been selected and
 * displays the delete button if necessary
 */
function monitorCheckedItems()
{
	// any quote item delete checkboxes checked?
	if($j('input[type="checkbox"][name="qiDelete"]:checked').length > 0)
	{
		$j('.QIDel').removeClass('hid');
	}
	else
	{
		$j('.QIDel').addClass('hid');
	}
}

/**
 * this method compiles all of the quote items selected to be deleted into a
 * comma separated string and appends them to a url for deletion
 */
function compileAndSubmitQisToDelete(quoteId) {
	// initial prompt for user
	$j
			.prompt(i18n.t("core.tpcosts:confirmDeleteQuotationItem", "Are you sure you wish to delete these quotation items?"),
					{
						title : i18n.t("core.tpcosts:confirmDelete", "Confirm Delete"),
						buttons : {
							Ok : true,
							Cancel : false
						},
						focus : 1,
						submit : function(e, v, m, f) {
							if (v) {
								// string for comma separated string of quote
								// item ids
								var delIds = '';
								// any quote item delete checkboxes checked?
								if ($j('input[type="checkbox"][name="qiDelete"]:checked').length > 0) {
										$j('input[type="checkbox"][name="qiDelete"]:checked').each(function(i, n) {
												// not first iteration?
												if (i > 0) {
													delIds += ',' + n.value;
												} else {
													delIds += n.value;
												}
										});
									// load the service javascript file if not
									// already
									loadScript.loadScriptsDynamic(
													new Array(
															'dwr/interface/tpquotationitemservice.js'),
													true,
													{
														callback : function() {
															tpquotationitemservice
																	.ajaxDeleteTPQuotationItems(
																			delIds,
																			{
																				callback : function(
																						result) {
																					if (result.success) {
																						window.location.href = 'viewtpquote.htm?id='
																								+ quoteId;
																					} else {
																						$j
																								.prompt(result.message);
																					}
																				}
																			});
														}
													});
								} else {
									$j
											.prompt(i18n.t("core.tpcosts:notChosenAnyQuoteItemsToDeleted", "You have not chosen any quote items to be deleted"));
								}
							}
						}
					});

}

function preparePurchaseOrderFromTPQuotation(tpQuotationId) {
	$j.ajax({
		url: "preparePurchaseOrderFromTPQuotation.htm",
		data: {
			tpQuotationId: tpQuotationId
		},
		async: true
	}).done(function(overlayContent) {
		loadScript.createOverlay('dynamic', 'Select items for purchase order', null, overlayContent, null, 34, 720, 'description');
	}).fail(function(result) {
		$j.prompt(result.responseText);
	});
}