/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	TPQuotationAddModel.js
*	DESCRIPTION		:	This page specific javascript file
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	09/07/2008 - SH - Created File.
*					:	28/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'script/trescal/core/utilities/DOMUtils.js',
								'script/thirdparty/jQuery/jquery.jtip.js' );

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' },
								{ element: 'div.results', corner: 'normal' },
								{ element: 'div.quotebasket', corner: 'normal' } );

/** 
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = 	new Array( { anchor: 'searchresults-link', block: 'searchresults-tab' },
					      	   { anchor: 'searchbasket-link', block: 'searchbasket-tab' });


/**
 * Array of autocomplete objects (multiple are possible) containing setup for 
 * jQueryUI initialization.
 * this is used in the init() function.
 */
var jquiAutocomplete = new Array(
		{
			inputFieldName: 'salesCat',
			inputFieldId: 'salesCatId',
			source: 'searchsalescategorytags.json'
		},
		{
			inputFieldName: 'familyNm', 
			inputFieldId: 'familyId',
			source: 'searchfamilytags.json'
		},
		{
			inputFieldName: 'domainNm', 
			inputFieldId: 'domainId',
			source: 'searchdomaintags.json'
		},
		{
			inputFieldName: 'descNm',
			inputFieldId: 'descId',
			source: 'searchdescriptiontags.json'
		});

/**
 * shows a warning if the user is leaving a page which has items
 * in a basket. Only works in IE and Moz 1.7 +.
 */
var showUnLoadWarning = true;

window.onbeforeunload = function()
{
	if(parseInt($j('span.basketCount:first').text()) > 0 && showUnLoadWarning)
	{
		showUnLoadWarning = true;
		return i18n.t("warnLoseCurrentBasket", "If you leave this page you will lose the contents of your current basket.");
	}
}

/**
 * variable to hold id of the last model viewed
 * this is used to close the additem section when a new model is selected
 */
var searchidStore = '';

/**
 * this variable holds the options used in the qty select box
 * options are created once the first time a model is clicked on and
 * then reused thereafter
 */
var qtyOptions = '';

/**
 * when the user selects an instrument to add to the quote basket this method creates a section
 * below for the user to select the calibration type/s and quantity to add to the basket.
 * 
 * @param {Integer} modelid is the id of the model which is to have a load item section applied
 * @param {String} modelname is the name of the selected model
 */
function loadAddItemSection(modelid, modelname)
{
	// this model has no add item section already	
	if($j('#addform' + modelid).length == 0)
	{
		// find the currently selected anchor and highlight with mouseover colour
		$j('#model' + modelid + ' a:first').css({ background: '#DEDEDE' });
		
		// load options required for the qty select box if they have not already 
		if (qtyOptions == '')
		{
			// use dom util 'prepareArray' to create array, loop through all occurences and add option string to variable
			$j.each(prepareArray(maxQuantity), function(x, m)
			{
				qtyOptions = qtyOptions + '<option value="' + m + '">' + m + '</option>';							
			});
		}
		
		// create content for the add item section	
		var content = 	// create outer div for add item section and set display style to 'none' for the slide effect
						'<div class="searchItemOptions" id="addform' + modelid + '" style=" display: none; ">' +
							// add break above fieldset to create a bigger margin above the fieldset (ie bug causes flicker if css border or padding used)
							'<br />' +
							// create fieldset for add items section
							'<fieldset>' +
								// create legend for add item section
							'<legend>' + i18n.t("addingForModelName", {varModelName : modelname, defaultValue : "Adding for " + modelname}) + '</legend>' +
								// create link and image for closing the add item section
								'<a href="#" class="float-right" onclick=" closeLoadSection(\'' + modelid + '\'); return false; ">' +
									'<img src="img/icons/cancel.png" width="16" height="16" class="closegif" alt="' + i18n.t("close", "Close") + '" title="' + i18n.t("close", "Close") + '" />' +
								'</a>' +
								// create ordered list for add item section
								'<ol>' +
									// append a list item for headings
									'<li>' +
										'<div style=" float: left; width: 150px; font-weight: bold; ">' + i18n.t("calType", "CalType") + '</div>' +
										'<div style=" float: left; width: 110px; font-weight: bold; ">' + i18n.t("qty", "Qty") + '</div>' +
										'<div class="clear"></div>' +
									'</li>';
								
									// add list item to the ordered list for each caltype				
									$j.each(calTypes, function(i, n)
									{										
										// generate the id's of select boxes and input field
										var calType = calTypes[i];
										var qtyFieldName = 'qty' + calTypes[i].id + modelid;
																	
										content = content +	// append a list item for each caltype to the ordered list in previous append
															'<li id="caltype' + calTypes[i].id + modelid + '">' +
																// add a calibration type label
																'<label>' + calTypes[i].sname + '</label>' +
																// add a quantity option list
																'<select id="' + qtyFieldName + '">' +
																	qtyOptions +
																'</select>' +
																'<a href="#" class="mainlink padleft40" onclick=" addToBasket(\'standard\', ' + modelid + ', \'' + escape(modelname) + '\', ' + calTypes[i].id + ', $j(\'#' + qtyFieldName + '\').val()); return false; ">' + i18n.t("addToBasket", "Add to basket") + '</a>' +
															'</li>';
									});								
		content = content + 	'</ol>' +
								// add a 'show basket' link
								'<a href="#" class="mainlink" id="quotebasketlink" onclick=" switchMenuFocus(menuElements, \'searchbasket-tab\', false); return false; ">' + i18n.t("goToBasket", "Go to basket") + '</a>  |  ' +
								// add a 'search' link
								'<a href="#search" class="mainlink" onclick=" clearForm(); return false; ">' + i18n.t("searchAgain", "Search Again") + '</a>' +
							'</fieldset>' +
						'</div>';
		// append the add item content
		$j('#model' + modelid).after(content);		
		// set qty select box selected option to that of qty selected in defaults
		$j('select[id*="qty"]').val($j('#defaultQty').val());		
		// add slide effect to main div and start diplaying
		$j('#addform' + modelid).slideDown(1200);		
	}
	
	// a model adding section has been created
	if(searchidStore != '')
	{
		// model adding section exists on the page
		if(($j('#model' + searchidStore).length) && (searchidStore != modelid))
		{
			// this function closes the current model adding section
			closeLoadSection(searchidStore);
		}
	}	
	// if more than one model is present in result list then add searchid variable
	// so it can be closed when the next model is selected
	if ($j('#searchresults-tab li').size() > 1)
	{
		searchidStore = modelid;
	}						
}

/**
 * this function removes a previously selected models add item section
 * and removes the selected background colour
 * 
 * @param {Integer} modelid is used to find the list item which we are closing
 */
function closeLoadSection(modelid)
{
	// find the current selection and set background colour to default for first anchor
	$j('#model' + modelid + ' a:first').css({ background: '' });
	// slide up the add item section and then remove from page
	$j('#addform' + modelid).slideUp(800, function(){
		// remove previous model adding area
		$j('div').remove('#addform' + modelid);
	});
}

/**
 * variable restricting the maximum basket size
 */
var BASKETMAXQTY = 50;

/**
 * adds the selected instrument to the third party quote item basket
 *
 * @param {String} addtype the type of addition (i.e. 'standard' or 'default')
 * @param {Integer} modelid the id of the selected model 
 * @param {String} modelname the name of the selected model
 * @param {String} caltype the calibration type selected
 * @param {Integer} qty the quantity selected
 */
function addToBasket(addtype, modelid, modelname, caltypeid, qty)
{
	// load options required for the qty select box if they have not already 
	if (qtyOptions == '')
	{
		// use dom util 'prepareArray' to create array, loop through all occurences and add option string to variable
		$j.each(prepareArray(maxQuantity), function(x, m)
		{
			qtyOptions = qtyOptions + '<option value="' + m + '">' + m + '</option>';							
		});
	}	
	// basket qty has reached the maximum allowed so alert the user
	if(parseInt($j('span.basketCount:first').text()) >= BASKETMAXQTY)
	{
		$j.prompt(i18n.t("maxBasketSizeReached", "The maximum basket size has been reached."));
	}
	else
	{
		// the empty basket message is present so remove it
		if ($j('table#basketitemstable tbody tr:first td').length < 2)
		{
			// remove message
			$j('table#basketitemstable tbody').empty();
		}		
		// string to store all caltype options created below		
		caltypeOptions = '';
		// populate variable with string for each option available in caltypes
		$j.each(calTypes, function(i)
		{
			if (caltypeid == calTypes[i].id)
			{
				caltypeOptions = caltypeOptions + '<option value="' + calTypes[i].id + '" selected="selected">' + calTypes[i].sname + '</option>';
			}
			else
			{
				caltypeOptions = caltypeOptions + '<option value="' + calTypes[i].id + '">' + calTypes[i].sname + '</option>';	
			}			
		});
		// declare variable to hold class name
		var className = '';
		// if modulus of basket count returns naught then class name is 'even' else 'odd'
		if ((parseInt($j('span.basketCount:first').text()) + 1) % 2 == 0)
		{
			className = 'even';
		}
		else
		{
			className = 'odd';
		}				
		// append new item row to basket
		$j('table#basketitemstable').append(	// add new row for model in basket												
												'<tr id="basketitem' + modelid + '" class="' + className + '">' +
													'<td class="center">' + (parseInt($j('span.basketCount:first').text()) + 1) + '</td>' +
													'<td>' + 
														unescape(modelname) +
														'<input type="hidden" name="modelIds" value="' + modelid + '" />' +
														'<input type="hidden" name="modelNames" value="' + unescape(modelname) + '" />' +
														'<input type="hidden" name="itemNos" value="' + (parseInt($j('span.basketCount:first').text()) + 1) + '" />' +
													'</td>' +
													'<td class="center">' +
														'<select name="qtys" id="quantities' + (parseInt($j('span.basketCount:first').text()) + 1) + '">' +
															qtyOptions +
														'</select>' +
													'</td>' +
													'<td class="center">' +
														'<select name="calTypeIds" id="caltype' + (parseInt($j('span.basketCount:first').text()) + 1) + '">' +
															caltypeOptions +
														'</select>' +
													'</td>' +
													'<td class="center">' +
														'<a href="#" class="mainlink" onclick=" deleteFromBasket(\'item\', \'basketitem' + modelid + '\'); return false; ">' +
															'<img src="img/icons/delete.png" width="16" height="16" alt="' + i18n.t("deleteFromBasket", "Delete from basket") + '" title="' + i18n.t("deleteFromBasket", "Delete from basket") + '" />' +
														'</a>' +
													'</td>' +														
												'</tr>');			
		// add one to the current basket count
		$j('span.basketCount').text(parseInt($j('span.basketCount:first').text()) + 1);
				
		// if user has added items from search results 'standard' then show tick image in form
		if (addtype == 'standard')
		{
			var sucSpanId = 'added' + getSemiRandomInt();
			// add a success image and 3 second timeout to the basket form
			$j('#caltype' + calTypes[caltypeid].id + modelid).append(	'<span id="' + sucSpanId + '">' +
																			'<img src="img/icons/greentick.png" width="16" height="16" alt="' + i18n.t("success", "Success") + '" class="successgif img_vertmiddle" />' +
																		'</span>');
													
			setTimeout(" $j('span').remove(\'#" + sucSpanId + "\');", 1000);
		}				
	}
}

/**
 * this function deletes an item or all items from the quote basket and displays the basket
 * empty message if no items remain.
 * 
 * @param {String} type 'item' if single deletion, 'all' if all quote items to be deleted
 * @param {Integer} basketitemid is the id of the basket item to remove
 */
function deleteFromBasket(type, basketitemid)
{
	// one item to be removed
	if (type == 'item')
	{
		// remove tbody from basket item table
		$j('tr#' + basketitemid).remove();
		// recalculate the basketCount
		$j('span.basketCount').text(parseInt($j('span.basketCount:first').text()) - 1);
		// get all item number cells from table
		$j('table#basketitemstable tbody tr').each(function(i)
		{
			// renumber item no cell
			$j(this).find('td:first').text(i + 1);
			// sort table row class names
			if (((i + 1) % 2) == 0)
			{
				$j(this).removeClass().addClass('even');
			}
			else
			{
				$j(this).removeClass().addClass('odd');
			}
		});
	}
	// all items are to be removed
	else
	{
		// confirm deletion of all items
		var delAll = confirm(i18n.t("confirmDeleteAllItems", "Are you sure you wish to delete all items?"));
		// confirmation given 
		if(delAll)
		{ 
			// remove all tbodies from basket item table
			$j('table#basketitemstable tbody').empty();
			// recalculate the basketCount
			$j('span.basketCount').text('0');
		}
	}
	// basket is empty
	if (parseInt($j('span.basketCount:first').text()) < 1)
	{
		// display message
		$j('table#basketitemstable').append('<tr>' +
												'<td colspan="8" class="center bold">' + i18n.t("core.tpcosts:quotationItemBasketIsEmpty", "Your Quotation Item Basket Is Empty") + '</td>' +
											'</tr>');
	}	
}

/**
 * when the user selects the 'Add Defaults' link beside an instrument this method is called which gets 
 * all values specified in the add defaults section at the head of the page. Using these values an instrument
 * is added to the quote basket
 * 
 * @param {Event} e is a window event used to stop event bubbling
 * @param {Integer} modelid the id of the selected model
 * @param {String} modelname the name of the selected model
 */
function addDefaults(e, modelid, modelname)
{
	// stop event propogation so that the advanced adding options is not displayed
	if (!e)
	{
		var e = window.event;
	}
	
	e.cancelBubble = true;
	
	if(e.stopPropagation)
	{
		e.stopPropagation();
	}
	
	// declare variables to hold default calibration types selected
	var defaultTypes = [];
	// get default qty value
	var defaultQty = $j('#defaultQty').val();
	// get all inputs within the searchdefaults div
	// iterate through inputs and find all default cal types selected and add to array
	$j('#searchdefaults input:checked').each(function(i, n)
	{
		defaultTypes.push([n.value]);
	});	
	// if no calibration types have been selected show a warning message else
	if (defaultTypes.length < 1)
	{
		$j.prompt(i18n.t("notSelectedCalType", "You have not selected the calibration type you require"));
	}
	else
	{
		// add items to basket for each calibration type selected
		$j.each(defaultTypes, function(i)
		{																																							
			addToBasket('default', modelid, modelname, defaultTypes[i], defaultQty);
		});							
					
		var sucSpanId = 'added' + getSemiRandomInt();
		// add a success image and 3 second timeout to the basket form
		$j('#default' + modelid).append('<span id="' + sucSpanId + '">' +
											'<img src="img/icons/greentick.png" width="16" height="16" class="successgif" alt="' + i18n.t("success", "Success") + '" style=" vertical-align: top; " />' +
										'</span>');
												
		setTimeout(" $j('span').remove(\'#" + sucSpanId + "\');", 1000);
	}
}

/**
 * clear search form and apply focus to the mfr field
 */
function clearForm()
{
	$j('input[name="mfrtext"], input[name="search\\.mfrId"], input[name="search\\.mfrNm"], input[name="search\\.model"], input[name="desctext"], input[name="search\\.descId"], input[name="search\\.descNm"], input#descNm').val('');
	$j('input[name="domainNm"],input[name="domainId"],input[name="familyNm"],input[name="familyId"],input[name="salesCat"],input[name="salesCatId"]').val('');
	$j('input#mfr').focus();
}