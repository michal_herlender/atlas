/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	TPQuoteSearch.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	Commenting of code.
*	KNOWN ISSUES	:	-
*	HISTORY			:	04/06/2007
*
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'qno';

/**
 * Array of autocomplete objects (multiple are possible) containing setup for 
 * jQueryUI initialization.
 * this is used in the init() function.
 */
var jquiAutocomplete = new Array(
				{
					inputFieldName: 'description',
					inputFieldId: 'descid',
					source: 'searchdescriptiontags.json'
				},
				{
					inputFieldName: 'mfr', 
					inputFieldId: 'mfrid',
					source: 'searchmanufacturertags.json'
				});

/**
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
 var menuElements = new Array(	{ anchor: 'linksearch', block: 'searchquote' },
		  					  	{ anchor: 'linkquote', block: 'createquote' } );