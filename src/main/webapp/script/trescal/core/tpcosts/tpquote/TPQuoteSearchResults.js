/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	TPQuoteSearchResults.js
*	DESCRIPTION		:	Page specific javascript file for tpquotesearchresults velocity page.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	23/10/2007 - SH - Created File.
*
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'script/thirdparty/jQuery/jquery.tablesorter.js' );

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * this variable contains the page name which should be used when creating the search again
 * link in the header section
 */
var searchAgain = 'tpquotesearch.htm';

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{
	/**
	 * creates a table with sortable columns and gives the rows a zebra style effect 
	 * by looking for a table with the classname of 'tablesorter'
	 */ 
	$j(document).ready(function() 
	    { 
			// make table columns sortable and apply zebra effect
	        $j('table.tablesorter').tablesorter({widgets: ['zebra']});
			// give the first row a class of 'hoverrow' for key navigation
			$j('table.tablesorter tbody tr:first').removeClass().addClass('hoverrow');
	    } 
	);
}