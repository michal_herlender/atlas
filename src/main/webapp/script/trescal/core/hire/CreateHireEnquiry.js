/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	CreateHireEnquiry.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	10/03/2011 - SH - Created File
*
****************************************************************************************************************************************/


/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'coname';


/**
 * this method toggles the display of the freehand contact elements should the user wish to add
 * a different delivery contact/address for the hire.
 * 
 * @param {Boolean} checked has user checked this box?
 */
function toggleFreehandContact(checked)
{
	// show freehand contact elements
	if (checked)
	{
		// show freehand elements
		$j('li.freehand').removeClass('hid');
	}
	else
	{
		// hide and clear freehand elements
		$j('li.freehand').addClass('hid').find('input[type="text"]').val('');
	}
}