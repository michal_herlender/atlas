/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	ViewHire.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	29/03/2011 - SH - Created File
*					:	12/10/2015 - TProvost - Manage i18n
*					:	13/10/2015 - TProvost - Fix bug in new version of Impromptu.js - Replace the "callback" property (property disappeared) by the "submit" property
*										=> Changes made in functions compileAndSubmitHisToDelete, sendEnquiryEmailConfirmation and resetOffHireDate
*					:   25/12/2020 - LM - Updated File - remove the existing scheduling functionality
****************************************************************************************************************************************/

var pageImports = new Array ( 'script/trescal/core/tools/labelprinting/dymo/DymoLabelFunctions.js',
		'script/trescal/core/tools/FileBrowserPlugin.js',	
		'script/trescal/core/utilities/DocGenPopup.js');


/** 
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
 var menuElements = new Array( 	{ anchor: 'items-link', block: 'items-tab' },
 								{ anchor: 'edit-link', block: 'edit-tab' },
 								{ anchor: 'despatch-link', block: 'despatch-tab' },
		  					   	{ anchor: 'files-link', block: 'files-tab' },
		  					   	{ anchor: 'email-link', block: 'email-tab' },
		  					   	{ anchor: 'invoices-link', block: 'invoices-tab' },
		  					   	{ anchor: 'printing-link', block: 'printing-tab'} );

 /**
  * array of calendar objects containing the initial setup literals needed
  * 
  * @param inputField (String) id of the input field to which the calendar should be binded
  * @param dateFormat (String) format that the date should be displayed
  * @param displayDates (String) 'all' make all dates available
  * 								'future' show only dates in the future
  * 								'past' show only dates in the past
  * applied in init() function.
  */								








/**
 * 
 * @param {String} carriageCost hire carriage cost
 * @param {String} carriageType hire carriage type (i.e 'collection', 'courier')
 * @param {Integer} cdId id of existing courier despatch
 * @param {String} cdConsigNo courier despatch consignment number
 * @param {String} cdDesDate courier depatch date
 * @param {Integer} cdCourierId id of chosen courier
 * @param {Integer} cdCourierTypeId id of chosen courier delivery type
 * 
 * @returns {hireContractDespatchDTO}
 */
function hireDespatchDTO(carriageCost, carriageType, cdId, cdConsigNo, cdDesDate, cdCourierId, cdCourierTypeId)
{
	this.carriageCost = carriageCost;
	this.carriageType = carriageType;
	this.cdId = cdId;
	this.cdConsigNo = cdConsigNo;
	this.cdDesDate = cdDesDate;
	this.cdCourierId = cdCourierId;
	this.cdCourierTypeId = cdCourierTypeId;
}

/**
 * this method sources all the relevant hire despatch information from overlay
 * 
 * @param {Object} overlayDiv div in which all despatch information defined
 * @returns
 */
function sourceHireDespatch(overlayDiv)
{
	// declare variables
	var carriagetype = '', cdconsigno = '', cddesdate = '';
	var cdid = 0, cdcourierid = 0, cdcouriertypeid = 0;
	var carriagecost = '0.00';
	// get all values from overlay form
	carriagetype = $j(overlayDiv).find('select[name="carriagetype"]').val();
	carriagecost = $j(overlayDiv).find('input[name="carriagecost"]').val();
	// get correct values
	switch (carriagetype)
	{	
		case 'courier': cdid = ($j(overlayDiv).find('select[name="selectdespatch"]').length > 0) ? $j(overlayDiv).find('select[name="selectdespatch"]').val() : 0;
						// check for existing courier despatch
						if (cdid == 0)
						{
							cdconsigno = $j(overlayDiv).find('input[name="consigno"]').val();
							cddesdate = $j(overlayDiv).find('input[name="despatchdate"]').val();
							cdcourierid = $j(overlayDiv).find('select[name="selectcourier"]').val();
							cdcouriertypeid = $j(overlayDiv).find('select[name="selecttype"]').val();
						}
						break;
	}
	// return new despatch dto
	return new hireDespatchDTO(carriagecost, carriagetype, cdid, cdconsigno, cddesdate, cdcourierid, cdcouriertypeid);
}


/**
 * this method displays the correct form elements depending on the carriage type selected.
 * 
 * @param {Object} li list item object to display elements after
 * @param {String} type the type of carriage selected (i.e. 'collection', 'courier')
 * @param {Integer} personId id of the hire contact
 * @param {Integer} addressId id of the hire address
 * @param {Integer} subdivId id of the hire subdiv
 */
function selectCarriageType(li, type, personId, addressId, subdivId)
{
	// containing div id
	var divid = $j(li).closest('div').attr('id');
	// variable to hold new content
	var content = '';
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('script/trescal/core/deliverynote/CDUtility.js'), true,
	{
		callback: function()
		{
			switch (type)
			{
				// hire contact will collect?
				case 'collect': // is courier content appended?
								if ($j('div#' + divid + ' ol#cdOptions').length)
								{
									// remove from the page
									$j('div#' + divid + ' ol#cdOptions').remove();
								}
								// remove any carriage options appended to page
								$j(li).parent().find('li.carriageOption').remove();
								// break
								break;
				// courier will be used?
				case 'courier': // remove any carriage options appended to page
								$j(li).parent().find('li.carriageOption').remove();
								// create courier despatch options
								content +=	'<ol id="cdOptions" class="nopadding">' +
												'<li>' +
													'<label>' + i18n.t("core.hire:selectCarriageType.newCourierDespatch", "New Courier Despatch") + '</label>' +
													'<span>&nbsp;</span>' +
												'</li>' +
											'</ol>';
								// append courier content to page
								$j(li).parent().after(content);
								// load courier despatch options
								changeDespatchOption('new', '', null, subdivId);
								// break
								break;
								
			}
		}
	});
}

/**
 * this method creates the content for user to select despatch type
 * 
 * @param {Integer} personId id of hire contact
 * @param {Integer} addressId id of hire address
 * @param {Integer} subdivId id of hire subdiv 
 * @returns {String} content
 */
function createHireDespatchTypeContent(personId, addressId, subdivId)
{
	// content variable
	var content = 	'<li>' +
						'<label>' + i18n.t("core.hire:createHireDespatchType.carriageType", "Carriage Type:") + '</label>' +
						'<select name="carriagetype" onchange=" selectCarriageType($j(this).parent(), this.value, ' + personId + ', ' + addressId + ', ' + subdivId + '); return false; ">' +
							'<option value="collect" selected="selected">' + i18n.t("collection", "Collection") + '</option>' +
							'<option value="courier">' + i18n.t("courier", "Courier") + '</option>' +
						'</select>' +
					'</li>' +
					'<li>' +
						'<label>' + i18n.t("core.hire:createHireDespatchType.carriageCost", "Carriage Cost:") + '</label>' +
						'<input type="text" name="carriagecost" value="0.00" />' +
						'<select onchange=" $j(this).prev().val(this.value); return false; ">' +
							'<option value="0.00">' + i18n.t("core.hire:createHireDespatchType.quickOptions", "quick options") + '</option>' +
							'<option value="15.00">15.00</option>' +
							'<option value="22.50">22.50</option>' +
							'<option value="45.00">45.00</option>' +
							'<option value="60.00">60.00</option>' +
						'</select>' +
					'</li>';
	// return this content
	return content;
}

/**
 * this method creates an overlay which requests confirmation from the user that they would like to remove the current
 * despatch type from the system and hire.
 * 
 * @param {Integer} hireId id of the hire
 * @param {Integer} cdId id of the courier despatch if applicable, else 0
 * @param {Integer} personId id of the contact
 * @param {Integer} addressId id of the address
 * @param {Integer} subdivId id of the subdivision
 */
function confirmHireDespatchTypeChange(hireId, cdId, personId, addressId, subdivId)
{
	// create overlay content
	var content = 	'<div id="changeHireDespatchOverlay">' +
						'<fieldset>' +
							'<ol class="nopadding">' +
								'<li>' +
									'<div class="bold">';
										if (cdId != 0)
										{
											content += 	'<p>' + i18n.t("core.hire:changeHireDespatchType.warnDeleteCourierDespatch", "This action will attempt to delete courier despatch, if it is safe to do so. Otherwise it will just be removed from this hire contract.") + '</p>';
										}
							content +=	'<p>' + i18n.t("core.hire:changeHireDespatchType.action", "Please click 'Continue' to go ahead and change the despatch type or 'Cancel' to return to the hire page.") + '</p>' +
									'</div>' +
								'</li>' +
							'</ol>' +
							'<ol class="nopadding">' +
								'<li>' +
									'<label>&nbsp;</label>' +
									'<input type="button" value="' + i18n.t("continue", "Continue") + '" onclick=" changeHireDespatchType(' + hireId + ', ' + cdId + ', ' + personId + ', ' + addressId + ', ' + subdivId + '); " />' +
									'<input type="button" value="' + i18n.t("cancel", "Cancel") + '" onclick=" loadScript.closeOverlay(); " />' +
								'</li>' +
							'</ol>' +
						'</fieldset>' +
					'</div>';
	// create new overlay
	loadScript.createOverlay('dynamic', i18n.t("core.hire:changeHireDespatchType.confirmChange", "Confirm Change Of Hire Despatch Type"), null, content, null, 30, 580, null);
}

/**
 * this method provides the user with the option to change the despatch type originally selected for this hire and informs them
 * what action was performed whilst removing the previous despatch type.
 * 
 * @param {Integer} hireId id of hire
 * @param {Integer} cdId id of courier despatch, else 0
 * @param {Integer} personId id of the contact
 * @param {Integer} addressId id of the address
 * @param {Integer} subdivId id of the subdivision
 */
function changeHireDespatchType(hireId, cdId, personId, addressId, subdivId)
{
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/hireservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to convert hire enquiry to contract
			hireservice.removeHireDespatchType(hireId, cdId,
			{
				callback: function(result)
				{
					// removal of previous despatch type successful?
					if (result.success)
					{
						// create overlay content
						var content = 	'<div id="changeHireDespatchOverlay">' +
											'<div class="warningBox1">' +
												'<div class="warningBox2">' +
													'<div class="warningBox3">' +
														result.message +
													'</div>' +
												'</div>' +
											'</div>' +
											'<fieldset>' +
												'<ol class="nopadding">' +
													createHireDespatchTypeContent(personId, addressId, subdivId) +
												'</ol>' +
												'<ol class="nopadding">' +
													'<li>' +
														'<label>&nbsp;</label>' +
														'<input type="button" value="' + i18n.t("submit", "Submit") + '" onclick=" this.disabled = true; changeHireDespatchTypeAction(' + hireId + ', $j(this).closest(\'div#changeHireDespatchOverlay\'), ' + subdivId + '); " />' +
													'</li>' +
												'</ol>' +
											'</fieldset>' +
										'</div>';
						// modify existing overlay with new content
						loadScript.modifyOverlay(null, content, i18n.t("core.hire:changeHireDespatchType.newDespatchType", "New Hire Despatch Type"), null, 50, 580, null);
					}
					else
					{
						// update overlay with error messages
						loadScript.modifyOverlay(null, result.message, null, true, null, null, null);
						// re-enable button
						$j('div#changeHireDespatchOverlay input[type="button"]').attr('disabled', '');
					}
				}
			});
		}
	});
}

/**
 * this method performs the action of saving the new hire despatch type
 * 
 * @param {Integer} hireId id of the hire to be converted
 * @param {Object} overlayDiv the overlay where values provided for conversion
 */
function changeHireDespatchTypeAction(hireId, overlayDiv, allocatedSubdivId)
{
	// source the hire despatch information
	var hireDesDTO = sourceHireDespatch(overlayDiv);
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/hireservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to update hire despatch info
			hireservice.updateHireDespatch(hireId, null, hireDesDTO, allocatedSubdivId,
			{
				callback: function(result)
				{
					// convert to hire contract successful?
					if (result.success)
					{
						// reload the current page switching focus to files tab
						window.location.href = 'viewhire.htm?id=' + hireId + '';
					}
					else
					{
						// update overlay with error messages
						loadScript.modifyOverlay(null, result.message, null, true, null, null, null);
						// re-enable button
						$j('div#changeHireDespatchOverlay input[type="button"]').attr('disabled', '');
					}
				}
			});
		}
	});
}



/**
 * this method allows the user to void a hire enquiry or a hire contract.
 * 
 * @param {Integer} hireId id of the hire
 * @param {String} hireNo number of the hire
 * @param {Integer} hireItemCount count of items on hire
 * @param {String} company name of company hire is for
 */
function voidHire(hireId, hireNo, hireItemCount, company)
{
	// create overlay content
	var content = 	'<div id="voidHireOverlay">' +
						'<fieldset>' +
							'<ol class="nopadding">' +
								'<li>' +
									'<p>' + i18n.t("core.hire:voidHire.info", {hireNo : hireNo, hireItemCount : hireItemCount, company : company, defaultValue : "To confirm you wish to void hire " + hireNo + " containing " + hireItemCount + " items for company " + company + " please provide a valid reason in the text box below:"}) + '</p>' +
								'</li>' +
								'<li>' +
									'<label>' + i18n.t("core.hire:voidHire.reason", "Reason For Void:") + '</label>' +
									'<textarea name="voidreason" class="width70" rows="5" value=""></textarea>' +
								'</li>' +								
								'<li>' +
									'<label>&nbsp;</label>' +
									'<input type="button" value="' + i18n.t("core.hire:voidHire.voidHire", "Void Hire") + '" onclick=" this.disabled = true; voidHireAction(' + hireId + ', $j(\'div#voidHireOverlay textarea[name=voidreason]\').val()); return false; " />' +
								'</li>' +
							'</ol>' +
						'</fieldset>' +
					'</div>';
	// create new overlay
	loadScript.createOverlay('dynamic', i18n.t("core.hire:voidHire.recordReason", "Record reason for voiding this hire..."), null, content, null, 40, 580, null);
}

/**
 * this method performs the void hire action during which the hire is set to status of 'Rejected'.
 * 
 * @param {Integer} hireId id of the hire
 * @param {String} reason the reason for voiding this hire
 */
function voidHireAction(hireId, reason)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/hireservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to void hire
			hireservice.voidHire(hireId, reason,
			{
				callback:function(result)
				{
					// hire contract voided?
					if (result.success)
					{
						// reload the current page
						window.location.reload();
					}
					else
					{
						// update overlay with error messages
						loadScript.modifyOverlay(null, result.message, null, true, null, null, null);
						// re-enable button
						$j('div#voidHireOverlay input[type="button"]').attr('disabled', '');
					}
				}
			});
		}
	});
}







/**
 * this method checks all inputs of type checkbox that are contained within the parent element 
 * 
 * @param {Boolean} select true if boxes are to be checked and vice versa
 * @param {String} parentElement class of element in which to search for inputs of type checkbox
 */
function selectAll(select, parentElement)
{
	// check all
	if(select)
	{
		$j('.' + parentElement + ' input:checkbox').check('on');
	}
	// uncheck all
	else
	{
		$j('.' + parentElement + ' input:checkbox').check('off');
	}
}

/**
 * this method sends hire enquiry confirmation email
 * 
 * @param {Integer} hireId id of hire to send confirmation for
 * @param {Boolean} isEnquiry is this still a hire enquiry?
 */
function sendEnquiryEmailConfirmation(hireId, currentConId, systemCompId, isEnquiry)
{
	// check this is an enquiry?
	if (isEnquiry)
	{
		// initial prompt for user
		$j.prompt(i18n.t("core.hire:sendEnquiryEmail.confirmAction", "Are you sure you wish to send this enquiry confirmation email?"),
		{ 
			submit: confirmcallback,
			buttons: 
			{ 
				Ok: true,
				Cancel: false 
			}, 
			focus: 1 
		});
		// callback method for initial prompt
		function confirmcallback(v,m)
		{
			// user confirmed action?
	    	if (m)
			{
				// load the service javascript file if not already
				loadScript.loadScriptsDynamic(new Array('dwr/interface/hireservice.js'), true,
				{
					callback: function()
					{
						hireservice.sendEnquiryEmailConfirmation(hireId, 
						{
							callback:function(result)
							{
								if (result.success)
								{
									// get email wrapper
									var emailWrap = result.results;							
									// update the emails tab
									loadScript.populateEntityEmail(emailWrap.email.id, currentConId, systemCompId, hireId);
									// switch focus to job files tab
									switchMenuFocus(menuElements, 'email-tab', false);													
								}
								else
								{
									// display error message to user
									$j.prompt(result.message);
								}
							}
						});
					}
				});
			}
		};
	}
	else
	{
		// inform user this is not an enquiry
		$j.prompt(i18n.t("core.hire:sendEnquiryEmail.notAnEnquiry", "This is no longer an enquiry so confirmation email cannot be sent"));
	}
}

/**
 * this method moves the hire from 'Awaiting Invoice' to 'Complete'. Can only be actioned by a member of the accounts department
 * 
 * @param {Integer} hireId id of the hire to be set to 'Complete'
 * @param {Object} statusDiv the div in which the status is displayed
 */
function completeHireContract(hireId, statusDiv)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/hireservice.js'), true,
	{
		callback: function()
		{
			hireservice.completeHireContract(hireId, 
			{
				callback:function(result)
				{
					// completion of hire successful?
					if (result.success)
					{
						// get hire
						var hire = result.results;
						// add complete status to page
						$j(statusDiv).html('<span class="bold">' + hire.status.name + '</span>');					
					}
					else
					{
						// display error message to user
						$j.prompt(result.message);
					}
				}
			});
		}
	});
}

/**
 * this method toggles the display of the freehand contact elements should the user wish to add
 * a different delivery contact/address for the hire.
 * 
 * @param {Boolean} checked has user checked this box?
 */
function toggleFreehandContact(checked)
{
	// show freehand contact elements
	if (checked)
	{
		// show freehand elements
		$j('li.freehand').removeClass('hid');
	}
	else
	{
		// hide and clear freehand elements
		$j('li.freehand').addClass('hid').find('input[type="text"]').val('');
	}
}