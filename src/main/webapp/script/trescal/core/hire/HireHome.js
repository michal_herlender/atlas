/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	SearchHireHistory.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	03/03/2011 - SH - Created File
*					:	12/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' },
								 );

/** 
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = new Array( 	{ anchor: 'enquiries-link', block: 'enquiries-tab' },
 								{ anchor: 'contracts-link', block: 'contracts-tab' },
 								{ anchor: 'awaitinginvoice-link', block: 'awaitinginvoice-tab' },
 								{ anchor: 'accountsvarification-link', block: 'accountsvarification-tab' });



/**
 * this method drops down the items for a hire contract
 * 
 * @param {Object} anchor link clicked to view/hide items
 * @param {Object} itemsrow row containing hire items
 */
function viewHireItems(anchor, itemsrow)
{
	// find div which holds hire items
	var div = $j(itemsrow).find('div:first');
	// hire items are currently hidden so make them visible
	if ($j(div).css('display') == 'none')
	{
		// remove image to show items and append hide items image
		$j(anchor).empty().append('<img src="img/icons/items_hide.png" width="16" height="16" alt="' + i18n.t("core.hire:hideHireItems", "Hide Hire Items") + '" title="' + i18n.t("core.hire:hideHireItems", "Hide Hire Items") + '" class="image_inline" />');
		// make the hire items row visible and add slide down effect
		$j(itemsrow).removeClass('hid').find('td > div').slideDown(1000);
	}
	// hire items are currently visible so hide them
	else
	{
		// remove image to hide items and append show items image
		$j(anchor).empty().append('<img src="img/icons/items.png" width="16" height="16" alt="' + i18n.t("core.hire:showHireItems", "Show Hire Items") + '" title="' + i18n.t("core.hire:showHireItems", "Show Hire Items") + '" class="image_inline" />');
		// add effect to slide up hire items and hide row
		$j(div).slideUp(1000, function()
		{
			$j(itemsrow).addClass('hid');
		});
	}
}

/**
 * this method accounts verifies a hire so it can be converted to a contract
 * 
 * @param {Object} anchor link clicked to verify
 * @param {Integer} hireId id of hire to be verified
 */
function accountsVerifyHire(anchor, hireId)
{
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/hireservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to accounts verify hire
			hireservice.accountsVerifyHire(hireId,
			{
				callback: function(result)
				{
					// update hire successful?
					if (result.success)
					{
						// replace anchor with tick image
						$j(anchor).after('<img src="img/icons/success.png" width="16" height="16" title="' + i18n.t("core.hire:hireVerified", "Hire verified") + '" />').remove();
					}
					else
					{
						// show error message to user
						$j.prompt(result.message);
					}
				}
			});
		}
	});
}

/**
 * this method allows the user to suspend all hire items for a designated period.
 */
function suspendAllHireItemsContent()
{
	// create overlay content
	var content = 	'<div id="suspendAllHireItemsOverlay">' +
						'<div class="warningBox1">' +
							'<div class="warningBox2">' +
								'<div class="warningBox3">' +
									i18n.t("core.hire:suspendAllHireItems", "This action should only be performed when the suspension period you are specifying is in the past") +
								'</div>' +
							'</div>' +
						'</div>' +
						'<fieldset>' +
							'<ol class="nopadding">' +
								'<li>' +
									'<label>' + i18n.t("core.hire:startDate", "Start Date:") + '</label>' +
									'<input type="text" name="startsuspenddate" id="startsuspenddate" value="' + formatDate(new Date(), 'dd.MM.yyyy') + '" readonly="readonly" autocomplete="off" />' +
								'</li>' +
								'<li>' +
									'<label>' + i18n.t("core.hire:endDate", "End Date:") + '</label>' +
									'<input type="text" name="endsuspenddate" id="endsuspenddate" value="' + formatDate(new Date(), 'dd.MM.yyyy') + '" readonly="readonly" autocomplete="off" />' +
								'</li>' +
								'<li>' +
									'<label>&nbsp;</label>' +
									'<input type="button" value="' + i18n.t("core.hire:suspendAllItems", "Suspend All Items") + '" onclick=" this.disabled = true; suspendAllHireItemsAction($j(\'div#suspendAllHireItemsOverlay input[name=startsuspenddate]\').val(), $j(\'div#suspendAllHireItemsOverlay input[name=endsuspenddate]\').val()); return false; " />' +
								'</li>' +
							'</ol>' +
						'</fieldset>' +
					'</div>';
	// create new overlay
	loadScript.createOverlay('dynamic', + i18n.t("core.hire:suspendAllHireItemsForPeriod", "Suspend All Hire Items For Period"), null, content, null, 28, 580, null);
	// create a calendar for the suspend start and end date field
	loadScript.createJQCal( { 
 							 inputField: 'startsuspenddate',
 							 dateFormat: 'dd.mm.yy',
 							 displayDates: 'all',
 							 showWeekends: true
 							 });
	loadScript.createJQCal(	{ 
							 inputField: 'endsuspenddate',
							 dateFormat: 'dd.mm.yy',
							 displayDates: 'all',
							 showWeekends: true
							});
}

/**
 * this method performs the action of suspending all hire items for the period specified
 * 
 * @param {String} startdate the date to start the suspension from
 * @param {String} enddate the date to end the suspension
 */
function suspendAllHireItemsAction(startdate, enddate)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/hiresuspenditemservice.js', 'script/trescal/core/utilities/TooltipLinks.js'), true,
	{
		callback: function()
		{			
			// call dwr service to suspend all hire items
			hiresuspenditemservice.suspendAllHireItems(startdate, enddate,
			{
				callback:function(result)
				{
					// hire items suspended?
					if (result.success)
					{
						// check for any failed items?
						var failedItems = result.results;
						// any failed items?
						if (failedItems.length > 0)
						{
							// create successful content
							var content = 	'<div id="suspendAllHireItemsOverlay">' +
												'<div class="warningBox1">' +
													'<div class="warningBox2">' +
														'<div class="warningBox3">' +
															i18n.t("core.hire:suspensionHireItemsFailure", "Suspension of hire items completed with failures - the following items were not suspended because the hire has yet to commence.") +
														'</div>' +
													'</div>' +
												'</div>' +
												'<table class="default4" summary="displays items that could not be suspended">' +
													'<thead>' +
														'<tr>' +
															'<td colspan="4">' + i18n.t("core.hire:itemsCouldNotBeSuspended", "Items that could not be suspended") + '</td>' +
														'</tr>' +
														'<tr>' +
															'<th>' + i18n.t("item", "Item") + '</th>' +
															'<th>' + i18n.t("description", "Description") + '</th>' +
															'<th>' + i18n.t("plantNo", "Plant No") + '</th>' +
															'<th>' + i18n.t("core.hire:hireNo", "Hire No") + '</th>' +
														'</tr>' +
													'</thead>' +
													'<tbody>';
														$j.each(failedItems, function(i, item)
														{
															content +=	'<tr>' +
																			'<td>' + item.itemno + '</td>' +
																			'<td>' + item.hireInst.inst.definitiveInstrument + '</td>' +
																			'<td>' + item.hireInst.inst.plantno + '</td>' +
																			'<td><a class="mainlink" href="viewhire.htm?id=' + item.hire.id + '" target="_blank">' + item.hire.hireno + '</a></td>' +
																		'</tr>';
														});
										content +=	'</tbody>' +
												'</table>' +
												'<div class="text-center marg-top">' +
													'<input type="button" value="' + i18n.t("close", "Close") + '" onclick=" loadScript.closeOverlay(); " />' +
												'</div>' +
											'</div>';
							// update overlay with error messages
							loadScript.modifyOverlay(null, content, i18n.t("core.hire:suspendHireItemsFailure", "Suspend Hire Items Completed With Failures"), false, 40, null, null);
						}
						else
						{
							// create successful content
							var content = 	'<div id="suspendAllHireItemsOverlay">' +
												'<div class="successBox1">' +
													'<div class="successBox2">' +
														'<div class="successBox3">' +
															i18n.t("core.hire:suspensionHireItemsSuccessfully", "Suspension of all hire items completed successfully") +
														'</div>' +
													'</div>' +
												'</div>' +
												'<div class="text-center">' +
													'<input type="button" value="' + i18n.t("close", "Close") + '" onclick=" loadScript.closeOverlay(); " />' +
												'</div>' +
											'</div>';
							// update overlay with error messages
							loadScript.modifyOverlay(null, content, i18n.t("core.hire:suspendHireItemsSuccessfully", "Suspend Hire Items Completed Successfully"), false, null, null, null);
						}
					}
					else
					{
						// update overlay with error messages
						loadScript.modifyOverlay(null, result.message, null, true, null, null, null);
						// re-enable button
						$j('div#suspendAllHireItemsOverlay input[type="button"]').attr('disabled', '');
					}
				}
			});			
		}
	});
}

/**
 * clear search form and apply focus to the mfr field
 */
function clearForm()
{
	// clear all current fields
	$j('input[name="comptext"], input[name="coid"], input[name="personid"], input[name="firstName"], input[name="lastName"],' +
		'input[name="serialno"], input[name="plantno"], input[name="mfrtext"], input[name="mfrid"], input[name="mfr"],' +
		'input[name="model"], input[name="desctext"], input[name="descid"], input[name="desc"], input[name="hireno"],' +
		'input[name="plantid"], input[name="clientRef"], input[name="purOrder"], input[name="hireDate1"],' +
		'input[name="hireDate2"]').val('');
	// reset selects
	$j('select[name="hireDateBetween"] option:first').attr('selected', 'selected').trigger('change');
	// reset checkboxes
	$j('input[name="onHire"]').attr('checked', '');
	// add focus back to hire no field
	$j('input[name="hireno"]').focus();
}