/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	SourceHireInstruments.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-					
*	KNOWN ISSUES	:	-
*	HISTORY			:	14/03/2011 - SH - Created File
*					:	12/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/


/** 
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = 	new Array( { anchor: 'searchresults-link', block: 'searchresults-tab' },
					      	   { anchor: 'searchbasket-link', block: 'searchbasket-tab' });


/**
 * shows a warning if the user is leaving a page which has items
 * in a basket. Only works in IE and Moz 1.7 +.
 */
var showUnLoadWarning = true;

/**
 * perform check before the user leaves the page
 */
window.onbeforeunload = function()
{
	if(parseInt($j('span.basketCount:first').text()) > 0 && showUnLoadWarning)
	{
		showUnLoadWarning = true;
		return i18n.t("warnLoseCurrentBasket", "If you leave this page you will lose the contents of your current basket.");
	}
};

