/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	ImageGalleryFunctions.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   - script/thirdparty/jQuery/jquery.galleriffic.js 
*                       - (If using gallerific gallery support e.g. with gallerificContent div)
*                       - dwr/interface/imageservice.js
*                       - dwr/interface/imagetagservice.js
*	TO-DO			: 	
*	KNOWN ISSUES	:	-
*	HISTORY			:	29/05/2009
*					:	08/10/2015 - TProvost - Manage i18n
*					:	2017-11-06 - Galen Beck: Cleaned up javascript import logic.
*					:	             Loaded imageservice and imagetagservice on demand per usual DWR conventions
*					:	             Page can be called either as a script import from page (OK), OR by pageImports variable (better)
*					:	             No other dependency loading needed now (all dynamic.)
*					:	             If using gallerificContent div, this script will auto load from Cwms_Main.js
****************************************************************************************************************************************/

/**
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements_img = new Array( 	{ anchor: 'webcam-link', block: 'webcam-tab' },
									{ anchor: 'upload-link', block: 'upload-tab' });
									
/**
 * global variable to hold galleriffic content
 */ 
//var gallerifficContent = '';

var video;

function handleVideo(stream) {
    video.srcObject = stream;
}

function videoError(e) {}

function snap(entityType, entityId) {
	if(!video.paused) {
		video.pause()
	}
	$j("#videoElement").addClass("hid");
	$j("#saveSnapshot").addClass("hid");
	$j('.warningBox1').removeClass('hid');
	$j('.warningBox3').empty().append('Please wait until webcam snapshot has finished uploading.')
	var canvas = document.createElement('canvas');
	canvas.width = 640;
	canvas.height = 480;
	var context = canvas.getContext('2d');
	context.drawImage(video, 0, 0, canvas.width, canvas.height);
	var formData = new FormData();
	formData.append("image", canvas.toDataURL("image/jpeg", 0.8));
	formData.append("entityType", entityType);
	formData.append("entityId", entityId);
	$j.ajax({
	    url: 'uploadimage.json',
	    data: formData,
	    dataType: 'json',
	    processData: false,
	    contentType: false,
	    type: 'post',
	    success: function(id) {
	    	imgUploadCount++;
	    	$j('div#webcamImageList').append(createImagePreview(entityId, entityType, 'webcam', 280, 210, imgUploadCount, tagArray, id));
	    	$j('.warningBox1').addClass('hid');
	    	$j('.warningBox3').empty();
	    },
	    error: function() {
	    	$j('.warningBox3').empty().append('An error occurs while saving the webcam snapshot.');
	    	$j('#videoElement').removeClass('hid');
	    	$j("#saveSnapshot").removeClass("hid");
	    	video.play();
	    }
	});
}

/**
 * this method is called from the init() method of the page specific javascript file.
 */
//function gallerifficInit()
//{
//	console.log("gallerificInit start");
//	// add initial galleriffic content to variable
//	gallerifficContent = $j('div.gallerifficContent').html();
//	// initialise the galleriffic plugin
//	initialiseGalleriffic();
//	console.log("gallerificInit finish");
//}

/**
 * this method initialises the galleriffic plugin with all necessary options set.
 */
function initialiseGalleriffic()
{
	console.log("initialiseGalleriffic() start");
	// initially set opacity on thumbs
	var onMouseOutOpacity = 0.67;	
	// add additional styling for hover effect on thumbs
	$j('div.gallerifficContent #thumbs-adv ul.thumbs li').css('opacity', onMouseOutOpacity)
		.hover
		(
			function()
			{
				$j(this).not('.selected').fadeTo('fast', 1.0);
			}, 
			function()
			{
				$j(this).not('.selected').fadeTo('fast', onMouseOutOpacity);
			}
		);		
	// initialize advanced galleriffic gallery
	$j('div.gallerifficContent #gallery-adv').galleriffic('div.gallerifficContent #thumbs-adv', 
	{
		delay:                  2000,
		numThumbs:              4,
		preloadAhead:           0,
		enableTopPager:         true,
		enableBottomPager:      true,
		imageContainerSel:      '#slideshow-adv',
		controlsContainerSel:   '#controls-adv',
		captionContainerSel:    '#caption-adv',
		loadingContainerSel:    '#loading-adv',
		renderSSControls:       true,
		renderNavControls:      true,
		playLinkText:           i18n.t("core.document:playSlideshow", "Play Slideshow"),
		pauseLinkText:          i18n.t("core.document:pauseSlideshow", "Pause Slideshow"),
		prevLinkText:           i18n.t("core.document:prevLinkText", "&lsaquo; Previous Photo"),
		nextLinkText:           i18n.t("core.document:nextLinkText", "Next Photo &rsaquo;"),
		nextPageLinkText:       i18n.t("core.document:nextPageLinkText", "Next &rsaquo;"),
		prevPageLinkText:       i18n.t("core.document:prevPageLinkText", "&lsaquo; Prev"),
		enableHistory:          false,
		autoStart:              false,
		onChange:               function(prevIndex, nextIndex) {
			$j('div.gallerifficContent #thumbs-adv ul.thumbs').children().eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end().eq(nextIndex).fadeTo('fast', 1.0);
		},
		onTransitionOut:        function(callback) {
			$j('div.gallerifficContent #slideshow-adv, #caption-adv').fadeOut('fast', callback);
		},
		onTransitionIn:         function() {
			$j('div.gallerifficContent #slideshow-adv, #caption-adv').fadeIn('fast');
		},
		onPageTransitionOut:    function(callback) {
			$j('div.gallerifficContent #thumbs-adv ul.thumbs').fadeOut('fast', callback);
		},
		onPageTransitionIn:     function() {
			$j('div.gallerifficContent #thumbs-adv ul.thumbs').fadeIn('fast');
		}
	});
	console.log("initialiseGalleriffic() finish");
}

/**
 * this method deletes an image via the galleriffic display
 * 
 * @param {Integer} imgId the id of the image we are deleting
 * @param {Integer} entityId the id of the entity from which we are deleting the image
 * @param {String} entityType the type of entity from which we are deleting the image
 */
function deleteGalleryImage(imgId, entityId, entityType)
{
	// load the image javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/imageservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to delete image
			imageservice.ajaxDeleteImage(imgId,
			{
				callback: function(result)
				{
					// result successful
					if (result.success)
					{
						// assign dto to variable if applicable
						var imgdto = result.results;
						var gallerifficContent = $j('div.gallerifficContent').html();
						// empty current galleriffic content, append original content, find list item containing image to remove
						$j('div.gallerifficContent').empty().append(gallerifficContent).find('li#systemimg' + imgId).remove();
						// update the count in image tab count
						$j('span.entityImageSize').text(parseInt($j('span.entityImageSize:first').text()) - 1);	
						// have all images been deleted?
						if ($j('div.gallerifficContent ul li').length < 1)
						{
							// add the no image logo to galleriffic plugin
							$j('div.gallerifficContent ul').append(	'<li class="thumbitem" id="emptyImage">' +
																		'<a class="thumb" href="img/noimage.png" title="">' +
																			'<img src="img/noimage.png" alt="" height="60" width="80" />' +
																		'</a>' +
																		'<div class="caption">' +
																			'<div class="download"></div>' +
																			'<div class="image-title">' +
																				i18n.t("core.document:addNewImage", "Add New Image") + ' ' +
																				'<a href="#" onclick=" addImageToEntity(' + entityId + ', \'' + entityType + '\'); return false; " title="">' +
																					'<img src="img/icons/picture_add.png" width="16" height="16" alt="' + i18n.t("core.document:addImages", "Add Images") + '" title="' + i18n.t("core.document:addImages", "Add Images") + '" />' +
																				'</a>' +
																			'</div>' +
																			'<div class="image-desc"></div>' +
																			'<div class="image-desc"></div>' +
																		'</div>' +
																	'</li>');	
						}
						// page header image content variable
						var imageContent = '';
						// job item image deleted so try and retrieve next instrument image available or an image for the job item instrument model
						if (entityType == 'JOBITEM')
						{
							// has image dto been returned?
							if (imgdto != null)
							{
								// instrument model image retrieved
								imageContent +=	'<img src="' + imgdto.imgSrc + '" alt="" title="" /><br />';
												// image type returned
												if (imgdto.imgType == 'JOBITEM')
												{
													// no more images for this job item so must be from previous job item
													if (parseInt($j('span.entityImageSize:first').text()) == 0)
													{
														imageContent +=	i18n.t("core.document:sourcePrevJI", "Source (Prev JI)");
													}
													else
													{
														imageContent +=	i18n.t("core.document:sourceJobItem", "Source (Job Item)");
													}
												}
												else
												{
													// image sourced from instrument model
													imageContent +=	i18n.t("core.document:sourceInstModel", "Source (Inst Model)");
												}
							}
							else
							{
								// no image available content
								imageContent +=	'<img src="img/noimage.png" width="150" height="113" alt="' + i18n.t("core.document:noImageAvailable", "No Image Available") + ' title="' + i18n.t("core.document:noImageAvailable", "No Image Available") + '" class="noImage" />';
							}
							// update default image
							updateDefaultImage(imageContent, entityId, entityType);
						}
						else
						{
							// has image dto been returned?
							if (imgdto != null)
							{
								// image available content
								imageContent +=	'<img src="' + imgdto.imgSrc + '" alt="" title="" /><br />' + i18n.t("core.document:sourceInstModel", "Source (Inst Model)");
								// update default image
								updateDefaultImage(imageContent, entityId, entityType);
							}
							else
							{
								// no image available content
								imageContent +=	'<img src="img/noimage.png" width="150" height="113" alt="' + i18n.t("core.document:noImageAvailable", "No Image Available") + ' title="' + i18n.t("core.document:noImageAvailable", "No Image Available") + '" class="noImage" />';
								// update default image
								updateDefaultImage(imageContent, entityId, entityType);
							}					
						}	
						// add new galleriffic content to variable
//						gallerifficContent = $j('div.gallerifficContent').html();
						// initialise the galleriffic plugin
						initialiseGalleriffic();
					}
					else
					{
						// show error message
						$j.prompt(result.message);
					}
				}
			});
		}
	});
}

/**
 * this method updates the default image displayed in the page header
 * 
 * @param {String} imageContent image html content
 * @param {Integer} entityId id of the entity updating image for
 * @param {String} entityType the type of entity updating image for
 */
function updateDefaultImage(imageContent, entityId, entityType)
{
	// update the default image for entity
	$j('div.defaultEntityImage').empty().append(	imageContent +
													'<div class="imgadd">' +
														'<a href="#" onclick=" addImageToEntity(' + entityId + ', \'' + entityType + '\'); return false; " title="">' +
															'<img src="img/icons/picture_add.png" width="16" height="16" alt="' + i18n.t("core.document:addImages", "Add Images") + '" title="' + i18n.t("core.document:addImages", "Add Images") + '" />' +
														'</a>' +
													'</div>');
}

/**
 * global array to hold image tags retrieved when add boxy thickbox is first initialised
 */ 
var tagArray = [];

/**
 * this method creates the content we need to populate a jquery boxy thickbox which can be used to add images to an
 * entity.  This boxy thickbox allows the user to add images from the web-cam folder or upload any image from the file
 * system.
 * 
 * @param {Integer} entityId the id of the entity that we want to add images to
 * @param {String} entityType the type of entity that we want to add images to
 */
function addImageToEntity(entityId, entityType)
{
	// variable to hold content
	var content = '';
	// load the image javascript services if not already
	loadScript.loadScriptsDynamic(new Array(
			'dwr/interface/imageservice.js',
			'dwr/interface/imagetagservice.js', 
			'script/trescal/core/utilities/Menu.js',
			'script/thirdparty/fileupload/upload.js',
			'dwr/interface/UploadMonitor.js'), true,
	{
		callback: function()
		{
			// call dwr service to retrieve all image tags
			imagetagservice.getAllImageTags(
			{
				callback:function(tags)
				{
					// add the retrieved tags to global array
					tagArray = tags;
					// create content for adding image by either webcam or uploading
					content += 	'<div id="addImageOverlay">' +
									// add warning box to display error messages if the move fails
									'<div class="hid warningBox1">' +
										'<div class="warningBox2">' +
											'<div class="warningBox3">' +
											'</div>' +
										'</div>' +
									'</div>' +
									'<!-- sub navigation menu which displays and hides sections of page -->' +
									'<div id="subnav">' +
										'<dl>' +
											'<dt><a href="#" id="webcam-link" onclick=" switchMenuFocus(menuElements_img, \'webcam-tab\', false); return false; " class="selected">' + i18n.t("core.document:webCamImages", "Web-Cam Images") + '</a></dt>' +
											'<dt><a href="#" id="upload-link" onclick=" switchMenuFocus(menuElements_img, \'upload-tab\', false); return false; ">' + i18n.t("core.document:uploadImages", "Upload Image(s)") + '</a></dt>' +
										'</dl>' +
									'</div>' +
									'<div id="webcam-tab">' +
										'<video autoplay controls id="videoElement"></video>' +
										'<input type="button" id="saveSnapshot" value="' + i18n.t("core.document:addimage", "Add Image") + '" onclick="snap(\'' + entityType + '\', ' + entityId + ');"/>' +
										'<input type="hidden" value="' + entityType + '" name="entityType"/>' +
										'<input type="hidden" value="' + entityId + '" name="entityId"/>' +
										'<div id="webcamImageList"></div>' +
									'</div>' +
									'<!-- end of sub navigation -->';
					content +=		'<div id="upload-tab" class="hid">' +
										'<div class="uploadManager text-center">' +
											'<div class="marg-bot-small"><strong>' + i18n.t("core.document:browseFile", "Browse to the file you wish to upload and click the 'Upload File' button") + '</strong></div>' +
											// IFRAME used as target to stop page redirect/refresh after submitting -->
											'<iframe id="target_upload" name="target_upload" src="" class="hid"></iframe>' +
											'<form method="post" class="fileuploadform" action="uploadimage.htm" enctype="multipart/form-data" onsubmit=" startProgress(10); " target="target_upload" >' +    
												'<div>' +
													'<input type="file" name="file" value="" id="imagefileuploadfile" />' +
													'<input type="submit" id="uploadbutton10" value="' + i18n.t("core.document:uploadFile", "Upload File") + '" />' +
													'<input type="button" value="' + i18n.t("cancel", "Cancel") + '" onclick=" window.stop(); " />' +
													'<input type="hidden" value="' + entityType + '" name="entityType"/>' +
													'<input type="hidden" value="' + entityId + '" name="entityId"/>' +
													'<div id="progressBar10" class="hid">' +
														'<!-- this span displays the upload progress text -->' +
														'<span></span>' +
														'<!-- this div is the progress bar container -->' +
														'<div>' +
															'<!-- background of this div is updated to show upload progress -->' +
															'<div></div>' +
														'</div>' +
													'</div>' +
												'</div>' +    
											'</form>' +
										'</div>' +
										'<div id="uploadImageList"></div>' +										
									'</div>' +
								'</div>';					
					// create new overlay using content variable
					loadScript.createOverlay('dynamic', i18n.t("core.document:addImage", "Add Image"), null, content, null, 400, 700, null);
					video = document.querySelector("#videoElement");
					navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia || navigator.oGetUserMedia;
					if (navigator.getUserMedia) {       
					    navigator.getUserMedia({video: true}, handleVideo, videoError);
					}
				}
			});
		}
	});
}

/**
 * this method creates content for one image that is to be shown as an image preview in the addImageOverlay thickbox.
 *   
 * @param {Integer} entityId the id of the entity that we want to add images to
 * @param {String} entityType the type of entity that we want to add images to
 * @param {String} imgType depicts the type of image we are previewing (i.e. 'web-cam' or 'upload')
 * @param {String} imgSrc the path to the image we are previewing
 * @param {Integer} imgWidth the width of the image we are previewing
 * @param {Integer} imgHeight the height of the image we are previewing
 * @param {Integer} imgId the id for the image we are previewing
 * @param {Array} tagArray an array of tag objects
 */
function createImagePreview(entityId, entityType, imgType, imgWidth, imgHeight, imgId, tagArray, imgdbId)
{
	// create the preview content for image
	var content = 	'<div class="imageBox" id="' + imgType + 'img' + imgId + '">' +									
						'<div class="float-left imageHolder">' +										
							'<img src="displayimage?id=' + imgdbId + '" width="' + imgWidth + '" height="' + imgHeight + '" alt="" title="" />';
							/*
							'<input type="checkbox" class="imageSelect" value="' + imgType + ',' + imgSrc + ',' + imgId + '" name="imagechkbx" />' +
							*/
				content +=	'<a href="#" class="imageDelete" onclick=" deleteTempImage(\'' + imgType + '\', ' + imgId + '); return false; " >' +
								'<img src="img/icons/cancel.png" width="16" height="16" alt="' + i18n.t("core.document:deleteImage", "Delete Image") + '" title="' + i18n.t("core.document:deleteImage", "Delete Image") + '" />' +
							'</a>' +										
						'</div>' +
						'<div class="float-right imageTagHolder">' +									
							'<div class="comment">' + i18n.t("comment", "Comment") + '</div>' +
							'<div class="commentText">' +																
								'<textarea class="imgDesc width96" ></textarea>' +
							'</div>' +
							'<div class="tagged">' + i18n.t("core.document:taggedAs", "Tagged As") + ' </div>' +									
							'<div id="image' + imgId + 'tags" class="tags">' +								
								'<ol>';
									// add all tag options to content
									$j.each(tagArray, function(i)
									{
										var checked = '';
										if (tagArray[i].tag == 'goods in')
										{
											checked = 'checked="checked"';
										}
										content += 	'<li>' +												
														'<label>' + tagArray[i].tag + '</label>' +
														'<input type="checkbox" value="' + tagArray[i].tag + '" name="imgtags" ' + checked + ' />' +
													'</li>';
									});
					content +=	'</ol>' +												
				 			'</div>' +							
							'<p>' +
								'<input type="button" value="' + i18n.t("core.document:saveThisImage", "Save This Image") + '" name="save" onclick=" this.disabled = true; saveImageToEntity(' + imgdbId +',' + entityId + ', \'' + entityType + '\', \'' + imgType + '\', ' + imgId + ', this); return false; "/>' +
							'</p>' +
						'</div>' +
						'<div class="clear-0">&nbsp;</div>' +
					'</div>';
	// return image preview content	
	return content;
}


/**
 * global variable to assign new id for each image uploaded
 */ 
var imgUploadCount = 0;

/**
 * this method is called after an image has been uploaded and appends the preview of the image to the
 * addImageOverlay thickbox.
 *  
 * @param {String} imgSrc path of the image file
 * @param {Integer} imgWidth width of the image to create preview for
 * @param {Integer} imgHeight height of the image to create preview for
 * @param {String} entityType the type of entity that we want to add images to
 * @param {Integer} entityId the id of the entity that we want to add images to
 */
function afterImageUpload(imgWidth, imgHeight, entityType, entityId, imgdbId)
{
	// add one to image upload count
	imgUploadCount++;
	// we've got a bunch more work to do here now - allow tagging, commenting of image
	$j('div#uploadImageList').append(createImagePreview(entityId, entityType, 'upload', imgWidth, imgHeight, imgUploadCount, tagArray, imgdbId));
	// is the image option div hidden?
	if ($j('div#addImageOverlay div#uploadImgOptions').hasClass('hid'))
	{
		// make the image options visible
		$j('div#addImageOverlay div#uploadImgOptions').removeClass('hid').addClass('vis');
	}
}

/**
 * this method saves a single image to an entity.
 * 
 * @param {Integer} entityId the id of the entity that we want to add images to
 * @param {String} entityType the type of entity that we want to add images to
 * @param {String} imgType depicts the type of image we are previewing (i.e. 'web-cam' or 'upload')
 * @param {String} imgPath the path to the image we are previewing
 * @param {Integer} imgId the id for the image we are previewing
 * @param {Object} button the button clicked to add these images
 */
function saveImageToEntity(imgdbId, entityId, entityType, imgType, imgId, button)
{
	// create new array for image tags
	var tags = new Array(new ImageTag('', 0, ''));
	// get all checked image tags for image
	$j.each($j('div#' + imgType + 'img' + imgId + ' input[type="checkbox"][name="imgtags"]:checked'), function(i, n)
	{
		// add image tag to array
		tags.push(new ImageTag(imgId, imgdbId, n.value));
	});
	// if tag length still 1 and empty?
	if ((tags.length == 1) && (tags[0].image == ''))
	{
		tags = null;
	}
	// put image path and description text into arrays
	//var imageA = new Array(imgPath);
	var descA = new Array($j('div#' + imgType + 'img' + imgId + ' textarea.imgDesc').val());
	var idA = [imgdbId];
	// load the image javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/imageservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to add image to entity
			imageservice.addImagesToEntity(idA, descA, entityId, tags, entityType,
			{
				callback:function(result)
				{
					// addition of image success?
					if (result.success)
					{
						// assign image object
						var image = result.results[0];
						// remove the saved image from available images list
						$j('div#' + imgType + 'img' + imgId).remove();
						// is this a webcam image?
						if (imgType == 'webcam')
						{
							$j('#videoElement').removeClass("hid");
							$j('#saveSnapshot').removeClass("hid");
							video.play();	
						}
						else
						{
							// all uploaded images saved?
							if (imgUploadCount < 1) {
								// make the image options hidden and uncheck select all checkbox
								$j('div#addImageOverlay div#uploadImgOptions').removeClass('vis').addClass('hid').end().find('input[type="checkbox"]').attr('checked', '');
							}				
						}
						// Refresh gallerific, if it is being used (note, this would be better done with an AJAX call for content!)
						if ($j('div.gallerifficContent').length){
							// initialise content variable
							var content = '';
							// get count of images already saved to entity
							var imgCount = (parseInt($j('span.entityImageSize').text()) + 1);
							// create content for new image in galleriffic code
							content += 	'<li class="thumbitem" id="systemimg' + image.id + '">' +							
											'<a class="thumb" href="displayimage?id=' + image.id + '" title="">' +
												'<img src="displaythumbnail?id=' + image.id + '"/>' +
											'</a>' +
											'<div class="caption">' +
												'<div class="download">' +
													'<div class="download">' +
													'<a href="#" onclick=" addImageToEntity(' + entityId + ', \'' + entityType + '\'); return false; " title="">' +
														'<img src="img/icons/picture_add.png" width="16" height="16" alt="' + i18n.t("core.document:addImages", "Add Images") + '" title="' + i18n.t("core.document:addImages", "Add Images") + '" />' +
													'</a>' +
													'&nbsp;&nbsp;&nbsp;' +
													'<a href="displayimage?id=' + image.id + '">' + i18n.t("core.document:downloadOriginal", "Download Original") + '</a>' +
													'&nbsp;&nbsp;&nbsp;' +
													'<a href="#" onclick=" deleteGalleryImage(' + image.id + ', ' + entityId + ', \'' + entityType + '\'); return false; ">' +
														'<img src="img/icons/redcross.png" width="16" height="16" alt="Delete This Image" title="Delete This Image" />' +
													'</a>' +
												'</div>' +
												'<div class="image-title">Image ' + imgCount + '</div>' +
												'<div class="image-desc">' +
													'<strong>Description: &nbsp;</strong>';
													if (image.description != '')
													{
														content += image.description; 
													}
													else
													{
														content += i18n.t("noComment", "No Comment");
													}
									content += 	'</div>' +
												'<div class="image-desc">' +
													'<strong>' + i18n.t("core.document:taggedAs", "Tagged As:") + ' </strong>';
													if ((image.tags != null) && (image.tags.length > 0))
													{
														$j.each(image.tags, function(i)
														{
															if (i == 0)
															{
																content += image.tags[i].tag;
															}
															else
															{
																content += ', ' + image.tags[i].tag;
															}
														});
													}
													else
													{
														content += i18n.t("core.document:noTagsAssigned", "No Tags Assigned");
													}											
									content += 	'</div>' +
											'</div>' +
										'</li>';
							// empty current galleriffic content, append original content, 
							var gallerifficContent = $j('div.gallerifficContent').html();
							$j('div.gallerifficContent').empty().append(gallerifficContent);
							// is the gallery empty in which case an image with class 'emptyImage' will be present?
							if ($j('div.defaultEntityImage').length > 0)
							{
								// find list item containing image to remove
								$j('div.gallerifficContent').find('li#emptyImage').remove();
								// create page header content
								var pagecontent = 	'<img src="displaythumbnail?id=' + image.id + '" alt="" title="" /><br />';
													if (entityType == 'JOBITEM')
													{
														pagecontent +=	i18n.t("core.document:sourceJobItem", "Source (Job Item)");
													}
									pagecontent	+=	'<div class="imgadd">' +
														'<a href="#" onclick=" addImageToEntity(' + entityId + ', \'' + entityType + '\'); return false; " title="">' +
															'<img src="img/icons/picture_add.png" width="16" height="16" alt="' + i18n.t("core.document:addImages", "Add Images") + '" title="' + i18n.t("core.document:addImages", "Add Images") + '" />' +
														'</a>' +
													'</div>';
								// update the default image for entity
								$j('div.defaultEntityImage').empty().append(pagecontent);	
							}
							// append new image to galleriffic content
							$j('div.gallerifficContent ul').prepend(content);
							// update the count in image tab count
							$j('span.entityImageSize').text(parseInt($j('span.entityImageSize').text()) + 1);
							// add new galleriffic content to variable
//							var gallerifficContent = $j('div.gallerifficContent').html();
							// re-initialise the galleriffic plugin
							initialiseGalleriffic();
						}
					}
					else
					{
						// show error message in error warning box
						$j('div#addImageOverlay div:first').removeClass('hid').addClass('vis').find('.warningBox3').empty().append('<div>' + result.message + '</div>');
					}
					// enable button
					$j(button).attr('disabled', '');										
				}
			});
		}
	});
}

/**
 * this method saves multiple images to an entity.
 * 
 * @param {Integer} entityId the id of the entity that we want to add images to
 * @param {String} entityType the type of entity that we want to add images to
 * @param {String} imgListId id of the div in which we are looking for images to add
 * @param {String} imgChkbxName name of the checkboxes that are used to select images
 * @param {Object} button the button clicked to add these images
 */
function saveImagesToEntity(entityId, entityType, imgListId, imgChkbxName, button)
{
	// initialise arrays to hold image info
	var urlA = new Array();
	var desA = new Array();
	var tags = new Array();
	var imgIdA = new Array();
	var globalImgType = '';	
	// get all checkboxes that have been checked within the provided image list
	$j('div#addImageOverlay div#' + imgListId + ' input[type="checkbox"][name="' + imgChkbxName + '"]:checked').each(function(i, n)
	{		
		// split comma delimited value of checkbox into array
		var valueArray = n.value.split(',');
		// add values from array into variables
		var imgType = valueArray[0];
		// first iteration of loop?
		if (i == 0)
		{
			globalImgType = imgType;	
		}
		var imgPath = valueArray[1];
		var imgId = valueArray[2];
		// add image path to array
		urlA.push(imgPath);
		// add image description to array
		desA.push($j('div#' + imgType + 'img' + imgId + ' textarea.imgDesc').val());		
		// get all image tags for image
		$j.each($j('div#' + imgType + 'img' + imgId + ' input[type="checkbox"][name="imgtags"]:checked'), function(x, m)
		{
			// add image tag to array
			tags.push(new ImageTag(imgPath, m.value));
		});
		// add img ids to array
		imgIdA.push(imgId);
	});
	// has the array been populated with values?
	if(urlA.length > 0)
	{
		// load the image javascript file if not already
		loadScript.loadScriptsDynamic(new Array('dwr/interface/imageservice.js'), true,
		{
			callback: function()
			{
				// call dwr service to save all images
				imageservice.addImagesToEntity(urlA, desA, entityId, tags, entityType, null,
				{
					callback:function(result)
					{
						// have all images been saved successfully?
						if (result.success)
						{
							// add all saved images to page galleriffic plugin
							$j.each(result.results, function(i)
							{
								// assign image object
								var image = result.results[i];
								// remove the saved image from available images list
								$j('div#' + globalImgType + 'img' + imgIdA[i]).remove();
								// is this a webcam image?
								if (globalImgType == 'webcam')
								{
									// update the available webcam image count
									$j('div#addImageOverlay span#availableWebcamSize').text(parseInt($j('span#availableWebcamSize').text()) - 1);							
								}
								else
								{
									// all uploaded images saved?
									if (--imgUploadCount < 1)
									{
										// make the image options hidden and uncheck select all checkbox
										$j('div#addImageOverlay div#uploadImgOptions').removeClass('vis').addClass('hid').end().find('input[type="checkbox"]').attr('checked', '');
									}				
								}						
								// Refresh gallerific content, if it is being used (note, this would be better done with an AJAX call for content!)
								if ($j('div.gallerifficContent').length){
									// initialise content variable
									var content = '';
									// get count of images already saved to entity
									var imgCount = (parseInt($j('span.entityImageSize').text()) + 1);
									// create content for new image in galleriffic code
									content += 	'<li class="thumbitem" id="systemimg' + image.id + '">' +							
													'<a class="thumb" href="' + image.relativeFilePath + '" title="">' +
														'<img src="displaythumbnail?id=' + image.id + '" alt="" height="60" width="80" />' +
													'</a>' +
													'<div class="caption">' +
														'<div class="download">' +
															'<a href="#" onclick=" addImageToEntity(' + entityId + ', \'' + entityType + '\'); return false; " title="">' +
																'<img src="img/icons/picture_add.png" width="16" height="16" alt="' + i18n.t("core.document:addImages", "Add Images") + '" title="' + i18n.t("core.document:addImages", "Add Images") + '" />' +
															'</a>' +
															'&nbsp;&nbsp;&nbsp;' +
															'<a href="' + image.relativeFilePath + '">' + i18n.t("core.document:downloadOriginal", "Download Original") + '</a>' +
															'&nbsp;&nbsp;&nbsp;' +
															'<a href="#" onclick=" deleteGalleryImage(' + image.id + '); return false; ">' +
																'<img src="img/icons/redcross.png" width="16" height="16" alt="" title="" />' +
															'</a>' +
														'</div>' +
														'<div class="image-title">' + i18n.t("core:document:image", "Image") + ' ' + imgCount + '</div>' +
														'<div class="image-desc">' +
															'<strong>' + i18n.t("descriptionLabel", "Description:") + ' </strong>';
															if (image.description != '')
															{
																content += image.description; 
															}
															else
															{
																content += i18n.t("noComment", "No Comment");
															}
											content += 	'</div>' +
														'<div class="image-desc">' +
															'<strong>' + i18n.t("core.document:taggedAs", "Tagged As:") + ' </strong>';
															if (image.tags.length > 0)
															{
																$j.each(image.tags, function(i)
																{
																	if (i == 0)
																	{
																		content += image.tags[i].tag;
																	}
																	else
																	{
																		content += ', ' + image.tags[i].tag;
																	}
																});
															}
															else
															{
																content += i18n.t("core.document:noTagsAssigned", "No Tags Assigned");
															}											
											content += 	'</div>' +
													'</div>' +
												'</li>';
									var gallerifficContent = $j('div.gallerifficContent').html();
									// empty current galleriffic content, append original content, 
									$j('div.gallerifficContent').empty().append(gallerifficContent);
									// is the gallery empty in which case empty image list item will be present?
									if ($j('div.defaultEntityImage > img.noImage').length > 0)
									{
										// find list item containing image to remove
										$j('div.gallerifficContent').find('li#emptyImage').remove();
										// update the default image for entity
										$j('div.defaultEntityImage').empty().append(	'<img src="displaythumbnail?id=' + image.id + '" alt="" title="" />' +
																						'<div class="imgadd">' +
																							'<a href="#" onclick=" addImageToEntity(' + entityId + ', \'' + entityType + '\'); return false; " title="">' +
																								'<img src="img/icons/picture_add.png" width="16" height="16" alt="' + i18n.t("core.document:addImages", "Add Images") + '" title="' + i18n.t("core.document:addImages", "Add Images") + '" />' +
																							'</a>' +
																						'</div>');	
									}
									// append new image to galleriffic content
									$j('div.gallerifficContent ul').append(content);
									// update the count in image tab count
									$j('span.entityImageSize').text(parseInt($j('span.entityImageSize').text()) + 1);
									// add new galleriffic content to variable
//									gallerifficContent = $j('div.gallerifficContent').html();		
								}
							});
							// is this a webcam image?
							if (globalImgType == 'webcam') 
							{
								// have all images been saved?
								if (parseInt($j('div#addImageOverlay span#availableWebcamSize').text()) < 1)
								{
									// hide the image options div and uncheck select all checkbox
									$j('div#addImageOverlay div#webcamImgOptions').removeClass('vis').addClass('hid').end().find('input[type="checkbox"]').attr('checked', '');
								}
							}						
							// Re-initialise the galleriffic plugin, if it is being used
							if ($j('div.gallerifficContent').length){
								initialiseGalleriffic();					
							}
						}
						else
						{
							// show error message in error warning box
							$j('div#addImageOverlay div:first').removeClass('hid').addClass('vis').find('.warningBox3').empty().append('<div>' + result.message + '</div>');
						}
						// enable button
						$j(button).attr('disabled', '');
					}
				});
			}
		});
	}
	else
	{
		// enable button
		$j(button).attr('disabled', '');
	}
}

/**
 * image tag wrapper
 * 
 * @param {String} image the image path
 * @param {int} imageId the image Id
 * @param {String} tag the tag name
 */
function ImageTag(image,imageId, tag)
{
	this.image = image;
	this.imageId = imageId
	this.tag = tag;
}

/**
 * this method deletes an image from the list of available images, either web-cam or upload.
 * 
 * @param {String} imgPath the path to the image we are previewing
 * @param {String} imgType depicts the type of image we are previewing (i.e. 'web-cam' or 'upload')
 * @param {Integer} imgId the id for the image we are previewing
 */
function deleteTempImage(imgType, imgId)
{
	// remove image from page
	$j('div#' + imgType + 'img' + imgId).remove();
	// is this a webcam image?
	if (imgType == 'webcam')
	{
		// update the available webcam image count
		$j('div#addImageOverlay span#availableWebcamSize').text(parseInt($j('div#addImageOverlay span#availableWebcamSize').text()) - 1);
		// have all images been deleted?
		if (parseInt($j('div#addImageOverlay span#availableWebcamSize').text()) < 1)
		{
			// hide the image options div and uncheck select all checkbox
			$j('div#addImageOverlay div#webcamImgOptions').removeClass('vis').addClass('hid').end().find('input[type="checkbox"]').attr('checked', '');
		}	
	}
	else
	{
		// all uploaded images saved?
		if (--imgUploadCount < 1)
		{
			// make the image options hidden and uncheck select all checkbox
			$j('div#addImageOverlay div#uploadImgOptions').removeClass('vis').addClass('hid').end().find('input[type="checkbox"]').attr('checked', '');
		}				
	}	
}

/**
 * this method deletes multiple images from the list of available images, either web-cam or upload.
 * 
 * @param {String} imgListId id of the div in which we are looking for images to add
 * @param {String} imgChkbxName name of the checkboxes that are used to select images
 * @param {Object} button the button clicked to add these images
 */
function deleteSelectedTempImages(imgListId, imgChkbxName, button)
{
	// get all checkboxes that have been checked within the provided image list
	$j('div#addImageOverlay div#' + imgListId + ' input[type="checkbox"][name="' + imgChkbxName + '"]:checked').each(function(i, n)
	{
		// split comma delimited value of checkbox into array
		var valueArray = n.value.split(',');
		// add values from array into variables
		var imgType = valueArray[0];		
		var imgPath = valueArray[1];
		var imgId = valueArray[2];
		// delete selected image from list
		deleteTempImage(imgType, imgId);		
	});
	// enable button
	$j(button).attr('disabled', '');
}

/**
 * this method selects all checkboxes within a div that has the supplied id and with the name provided.
 * 
 * @param {Boolean} select indicates if the checkboxes should be checked or un-checked
 * @param {String} name the name of the checkboxes we want to check
 * @param {String} parentDiv the id of the div in which we want to check checkboxes
 */
function selectAllImageItems(select, name, parentDiv)
{
	// check all
	if(select)
	{
		$j('#' + parentDiv + ' input[type="checkbox"][name="' + name + '"]').check('on');
	}
	// uncheck all
	else
	{
		$j('#' + parentDiv + ' input[type="checkbox"][name="' + name + '"]').check('off');
	}
}