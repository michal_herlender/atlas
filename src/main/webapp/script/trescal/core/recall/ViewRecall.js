/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	ViewRecall.js
*	DESCRIPTION		:	This is a page specific javascript file .
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	13/08/2010 - SH - Created File.
*					:	22/10/2015 - TProvost - Manage i18n
*					:   18/03/2021 - Laïla MADANI - Updated File
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array ( 'script/trescal/core/tools/FileBrowserPlugin.js' );

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

function filterByCompany(){
	var selectedCoid = $j('#clientSelector').val();
	if(selectedCoid == 0){
		window.location.href = window.location.href.split("?")[0];
	} else {
		var url = window.location.href.split("?")[0];  
		url += '?coid='+selectedCoid;
		window.location.href = url;
	}
}
/**
 * this function retrieves all recall items which are linked to the recallid and the personid or addressid depending
 * on the recall type supplied using dwr, if recall items have already been retrieved then no DWR call is made.
 * 
 * @param {Object} event window event used to cancel any event bubbling
 * @param {Integer} rowcount integer value of the row for which recall items should be found and displayed
 * @param {String} recalltype the type of recall (i.e. 'CONTACT' or 'ADDRESS')
 * @param {Integer} recallid id of the recall to retrieve items
 * @param {Integer} personid id of the contact to retrieve items for if 'CONTACT' recall type
 * @param {Integer} addrid id of the address to retrieve items for if 'ADDRESS' recall type
 * @param {Boolean} ignoredOnSetting indicates if this recall has been ignored on a system default setting
 */
function getRecallItems(event, rowcount, recalltype, recallid, personid, addrid, ignoredOnSetting)
{
	var personId = null;
	var addrId = null;
	// check personid and addrid
	if (personid != '')
	{
		personId = parseInt(personid);
	}
	if (addrid != '')
	{
		addrId = parseInt(addrid);
	}
	// cancel event bubbling
	event.cancelBubble = true;	
	// recall items for this recall row have already been retrieved
	if ($j('#child' + rowcount).length)
	{
		// recall items are currently hidden so make them visible
		if ($j('#child' + rowcount).css('display') == 'none')
		{
			// remove image to show items and append hide items image
			$j('#itemsLink' + rowcount).empty().append('<img src="img/icons/items_hide.png" width="16" height="16" alt="' + i18n.t("core.recall:showRecallItems", "Hide Recall Items") + '" title="' + i18n.t("core.recall:hideRecallItems", "Hide Recall Items") + '" class="image_inline" />');
			// make the recall items row visible and add slide down effect
			$j('#child' + rowcount).css({ display: '', visibility: 'visible' }).find('div:first').slideDown(1000);
		}
		// recall items are currently visible so hide them
		else
		{
			// remove image to hide items and append show items image
			$j('#itemsLink' + rowcount).empty().append('<img src="img/icons/items.png" width="16" height="16" alt="' + i18n.t("core.recall:showRecallItems", "Show Recall Items") + '" title="' + i18n.t("core.recall:hideRecallItems", "Show Recall Items") + '" class="image_inline" />');
			// add effect to slide up recall items and hide row
			$j('#child' + rowcount + ' div').slideUp(1000, function()
			{
				$j('#child' + rowcount).css({ display: 'none', visibility: '' });
			});
		}
	}
	// recall items for this recall row have not been retrieved so call DWR
	else
	{
		// load javascript service file
		loadScript.loadScriptsDynamic(new Array('dwr/interface/recallitemservice.js'), true,
		{
			callback: function()
			{	
				// call dwr service to get items for recall
				recallitemservice.getRecallItemsForRecallType(recalltype, recallid, personId, addrId, 
				{
					callback:function(result)
					{			
						displayRecallItems(result, rowcount, ignoredOnSetting);
					}
				});							
			}
		});		
	}		
}

/**
 * this function displays the recall items retrieved by (@see getRecallItems())
 * 
 * @param (recallitems) list of recall item objects
 * @param (rowcount) integer value of the row for which recall items should be found and displayed
 * @param {Boolean} ignoredOnSetting indicates if this recall has been ignored on a system default setting
 */
function displayRecallItems(result, rowcount, ignoredOnSetting)
{
	// result successful?
	if (result.success)
	{
		// assign recall items to variable
		var ris = result.results;
		// work out the class to be used for child row dependant on rowcount (odd, even)
		var classname = 'odd';
		if(rowcount % 2 == 0)
		{
			classname = 'even';
		}
		// if the parent row is currently highlighted with hoverrow then add to the child
		if ($j('#parent' + rowcount).attr('class') == 'hoverrow')
		{
			/**
			 * this variable is used in the @see (SearchResultsKeyNav.js) file to flag that a row has a child
			 * and that it is currently highlighted with the 'hoverrow' class name
			 */  
			hasChild = true;
		}
		// delitems length is greater than zero
		if(ris.length > 0)
		{
			// load javascript service file
			loadScript.loadScriptsDynamic(new Array('script/trescal/core/utilities/TooltipLinks.js'), true,
			{
				callback: function()
				{
					// create content for recall items											
					var content = 	'<tr id="child' + rowcount + '" class="' + classname + '">' +
										'<td colspan="7">' +
											'<div style=" display: none; ">' +
												'<table class="child_table" summary="This table displays any recall items for this recall contact/address">' +
													'<thead>' +
														'<tr>' +
															'<td colspan="6">';
																if (ris.length != 1)
																{
																	content += i18n.t("core.recall:displayingRecallItems", {varNbItem : ris.length, defaultValue : "Displaying " + ris.length + " recall items"});
																}
																else
																{
																	content += i18n.t("core.recall:displayingRecallItem", {varNbItem : ris.length, defaultValue : "'Displaying " + ris.length + "' recall item"});
																}
												content += 	'</td>' +
														'</tr>' +
														'<tr>' +
															'<th scope="col" class="barcode">' + i18n.t("barcode", "Barcode") + '</th>' +  
															'<th scope="col" class="inst">' + i18n.t("instrument", "Instrument") + '</th>' +
															'<th scope="col" class="serial">' + i18n.t("serialNo", "Serial No") + '</th>' +
															'<th scope="col" class="plant">' + i18n.t("plantNo", "Plant No") + '</th>' +
															'<th scope="col" class="duedate">' + i18n.t("core.recall:dueDate", "Due Date") + '</th>' +
															'<th scope="col" class="recalled">' + i18n.t("core.recall:recalled", "Recalled") + '</th>' +
														'</tr>' +
													'</thead>' +
													'<tfoot>' +
														'<tr>' +
															'<td colspan="6">&nbsp;</td>' +
														'</tr>' +
													'</tfoot>' +
													'<tbody>';
														// add a row to the recall items table for each recall item returned
														$j.each(ris, function(i, ri)
														{
															content += 	'<tr';
																			if (ri.excludeFromNotification || ignoredOnSetting)
																			{
																				content += 	' class="highlight"';
																			}
																content += 	'>' +
																			'<td class="barcode">' + instrumentLinkDWRInfo(ri.instrument, rowcount, true, false, true, 0) + '</td>' +	
																			'<td class="inst">' + ri.instrument.definitiveInstrument + '</td>' +
																			'<td class="serial">' + ri.instrument.serialno + '</td>' +
																			'<td class="plant">' + ri.instrument.plantno + '</td>' +
																			'<td class="duedate">' + formatDate(ri.instrument.nextCalDueDate, 'dd.MM.yyyy') + '</td>' +
																			'<td class="recalled">';
																				if (!ri.excludeFromNotification)
																				{
																					content += 	'<img src="img/icons/bullet-tick.png" height="10" width="10" title="' + i18n.t("core.recall:itemRecalledAsNormal", "Item recalled as normal") + '"/>';
																				}
																				else
																				{
																					content += 	'<img src="img/icons/bullet-cross.png" height="10" width="10" title="' + i18n.t("core.recall:itemIgnoredFromRecall", "Item ignored from recall") + '"/>';
																				}
																content +=	'</td>' +
																		'</tr>';
														});
									content +=		'</tbody>' +
												'</table>' +
											'</div>' +
										'</td>' +
									'</tr>';
											
					// remove image to show items and append image to hide items
					$j('#itemsLink' + rowcount).empty().append('<img src="img/icons/items_hide.png" width="16" height="16" alt="' + i18n.t("core.recall:showRecallItems", "Hide Recall Items") + '" title="' + i18n.t("core.recall:showRecallItems", "Hide Recall Items") + '" class="image_inline" />');
					// append a new child table row below the parent for this recall where recall items will be listed
					$j('#parent' + rowcount).after(content);			
					// add effect to slide down recall items
					$j('#child' + rowcount + ' div').slideDown(1000);
					// initialise tooltips
					JT_init();
				}
			});
		}
	}
	else
	{
		// show error message
		$j.prompt(result.message);
	}
}


