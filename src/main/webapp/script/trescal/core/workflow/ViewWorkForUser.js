/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	ViewWorkForUser.js
*	DESCRIPTION		:	This page specific javascript file.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	28/05/2008 - JV - Created File
*					:	29/10/2015 - TProvost - Manage i18n
*                       2019-07-25 - Galen Beck - added table sorter
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'script/thirdparty/jQuery/jquery.tablesorter.js');

/** 
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = new Array( 	{ anchor: 'allocated-link', block: 'allocated-tab' },
								{ anchor: 'activities-link', block: 'activities-tab' },
		  					   	{ anchor: 'available-link', block: 'available-tab' },
								{ anchor: 'upcoming-link', block: 'upcoming-tab' });
								
/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' },
								{ element: 'ul.subnavtab a', corner: 'small transparent top' } );

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init() 
{
	$j('#tableallocated').tablesorter(
	{
	    headers: {
           	1: {sorter: false},
			6: {sorter: false},
			7: {sorter: false}
           }
	});
	$j('#tableavailable').tablesorter(
	{
	    headers: {
           	1: {sorter: false},
			2: {sorter: false},
           }
	});
}

/**
 * this method hides and shows lab work depending on the users checkbox selections on the page
 *
 */
function filterLabView()
{
	// loop through all lab checkboxes
	$j('input[name^="filterlabs"]').each(function()
	{
		// this lab not checked?
		if (!this.checked)
		{	
			// get the table row for any related work to this lab and hide it
			$j('tr[id^="dept' + this.value + '-"]').addClass('hid');
		}
		else
		{
			// display the selected lab row
			$j('tr[id^="dept' + this.value + '-"]').removeClass('hid');
		}
	});
}

/**
 * this method selects/de-selects all labs and calls method to show or hide rows depending on selection
 * 
 * @param {Boolean} checked indicates if the all checkbox has been checked
 */
function selectAllLabs(checked)
{
	// is all checked?
	if (checked)
	{
		// check all labs
		$j('input[name^="filterlabs"]').check('on');
		// trigger onclick event of first lab checkbox
		$j('input[name^="filterlabs"]:first').trigger('onclick');
	}
	else
	{
		// un-check all labs
		$j('input[name^="filterlabs"]').check('off');
		// trigger onclick event of first lab checkbox
		$j('input[name^="filterlabs"]:first').trigger('onclick');
	}
}

/**
 * this method sets allocated work to completed
 * 
 * @param {Object} checkbox the checkbox input element selected
 * @param {Integer} allocatedWorkId id of the allocated work to update
 */
function completeAllocatedWork(checkbox, allocatedWorkId)
{
	// confirm action
	if (confirm('Set this work as complete?') == true)
	{
		// load the service javascript file if not already	
		loadScript.loadScriptsDynamic(new Array('dwr/interface/engineerallocationservice.js'), true,
		{
			callback: function()
			{	
				// call dwr service to set allocated work as complete
				engineerallocationservice.completeAllocatedWork(allocatedWorkId, 
				{
					callback:function(resWrap)
					{
						// update successful?
						if(resWrap.success)
						{
							var tbody = $j(checkbox).closest('tbody');
							
							// remove row from allocated work table
							$j(checkbox).closest('tr').remove();
							
							// update counts
							var count = $j('span#allocatedWorkCount1').html();
							count = count - 1;
							$j('span[id^="allocatedWorkCount"]').html(count);
							
							if(count < 1)
							{
								$j(tbody).append('<tr><td colspan="8" class="text-center bold">' + i18n.t("core.workflow:noItemsAllocated", "No items allocated") + '</td></tr>');
							}
						}
					}
				});
			}
		});
	}
}

/**
 * this method creates the add/edit pop up functionality for adding/editing a work allocation message.
 * 
 * @param {String} action string defining the action required (i.e. 'add' or 'edit')
 * @param {Integer} allocatedWorkId id of the allocated work to add message to
 * @param {Integer} messageId id of message to be edited if applicable
 * @param {String} editText current message text for editing if applicable
 * @param {Integer} currentConId the current
 */
function createAddEditMessageBox(action, allocatedWorkId, messageId, editText, currentConId)
{
	// un-escape message text
	editText =  unescape(editText);
	// create title depending on action
	var title = (action == 'add') ? i18n.t("core.workflow:addMessageToWorkAllocation", "Add Message To Work Allocation") : i18n.t("core.workflow:editMessageToWorkAllocation", "Edit Message For Work Allocation");
	// create button content depending on action
	var button = (action == 'add') ? '<input type="button" onclick=" this.disabled = true; addMessageToAllocatedWork(' + allocatedWorkId + ', $j(\'textarea#messageBox\').val(), ' + currentConId + '); return false; " value="' + i18n.t("core.workflow:addMessage", "Add Message") + '" class="submit" />' 
									: '<input type="button" onclick=" this.disabled = true; editMessageForAllocatedWork(' + messageId + ', $j(\'textarea#messageBox\').val(), ' + allocatedWorkId + ', ' + currentConId + '); return false; " value="' + i18n.t("core.workflow:saveMessage", "Save Message") + '" class="submit" />';
	// create variable to hold content
	var content = 	'<div id="addMessageOverlay">' +
						'<div class="warningBox1 hid">' +
							'<div class="warningBox2">' +
								'<div class="warningBox3">' +
								'</div>' +
							'</div>' +
						'</div>' +
						'<fieldset>' +
							'<ol>' +
								'<li>' +
									'<label>' + i18n.t("core.workflow:messageLabel", "Message:") + '</label>' +
									'<textarea id="messageBox" rows="5" class="width70" value="' + editText + '">' + editText + '</textarea>' +
								'</li>' +
								'<li>' +
									'<label>&nbsp;</label>' +
									// button for adding or editing
									button +
									'<input type="button" onclick=" loadScript.closeOverlay(); return false; " value="' + i18n.t("close", "Close") + '" />' +	
								'</li>' +
							'</ol>' +
						'</fieldset>' +
					'</div>';
	// create new overlay
	loadScript.createOverlay('dynamic', title, null, content, null, 30, 600, 'messageBox');	
}

/*
 * Returns contact initials, as it's a transient method in DWR
 */
function getInitials(contact) {
	var result = '';
	if ((contact.firstName != null) && (contact.firstName.length > 0)) result += contact.firstName.charAt(0);
	if ((contact.lastName != null) && (contact.lastName.length > 0)) result += contact.lastName.charAt(0);
	return result;
}

/**
 * this method edits a message for allocated work
 * 
 * @param {Integer} messageId id of the allocated work message to update
 * @param {String} message the string message to update
 * @param {Integer} allocatedWorkId id of the allocated work to add message to
 * @param {Integer} currentConId the current contact id
 */
function editMessageForAllocatedWork(messageId, message, allocatedWorkId, currentConId)
{
	// content variable
	var content = '';
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/engineerallocationmessageservice.js'), true,
	{
		callback: function()
		{	
			// call dwr service to add message
			engineerallocationmessageservice.ajaxEditEngineerAllocationMessage(messageId, message, 
			{
				callback:function(result)
				{
					// update message successful?
					if(result.success)
					{
						// assign message to variable
						var msg = result.results;
						// create content to append to page						
						content +=	'<em>' + msg.message + '</em>&nbsp;' +
									getInitials(msg.setBy) + ' (' + formatDate(msg.setOn, 'dd/MM') + ') ';
									if (currentConId == msg.setBy.personid)
									{
										content += 	'<a href="#" class="editMessage" onclick=" createAddEditMessageBox(\'edit\', ' + allocatedWorkId + ', ' + msg.id + ', \'' + escape(msg.message) + '\', ' + currentConId + '); return false; ">' +
														'<img src="img/icons/edit_small.png" width="12" height="12" title="' + i18n.t("core.workflow:editMessage", "Edit Message") + '" />' +
													'</a> ' +
													'&nbsp;' +
													'<a href="#" class="deleteMessage" onclick=" deleteAllocatedWorkMessage(this, ' + msg.id + '); return false; ">' +
														'<img src="img/icons/small_delete.png" width="12" height="12" title="' + i18n.t("core.workflow:deleteMessage", "Delete Message") + '" />' +
													'</a>';
									}						
						// append new content to page
						$j('div#awmessage' + messageId).empty().append(content);						
						// close overlay
						loadScript.closeOverlay();
					}
				}
			});
		}
	});
}

/**
 * this method adds a message to allocated work
 * 
 * @param {Integer} allocatedWorkId id of the allocated work to add message to
 * @param {String} message the string message to add to allocated work
 * @param {Integer} currentConId the current contact id
 */
function addMessageToAllocatedWork(allocatedWorkId, message, currentConId)
{
	// content variable
	var content = '';
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/engineerallocationmessageservice.js'), true,
	{
		callback: function()
		{	
			// call dwr service to add message
			engineerallocationmessageservice.ajaxAddEngineerAllocationMessage(allocatedWorkId, message, 
			{
				callback:function(result)
				{
					// addition of message successful?
					if(result.success)
					{
						// assign message to variable
						var msg = result.results;
						// create content to append to page
						content +=	'<div id="awmessage' + msg.id + '" class="width90 padding2 ';
										if ($j('div#awmessages' + allocatedWorkId + ' > div').length > 1)
										{
											content +=	'border-top';
										}
										content += '">' +
										'<em>' + msg.message + '</em>&nbsp;' +
										getInitials(msg.setBy) + ' (' + formatDate(msg.setOn, 'dd/MM') + ') ';
										if (currentConId == msg.setBy.personid)
										{
											content += 	'<a href="#" class="editMessage" onclick=" createAddEditMessageBox(\'edit\', ' + allocatedWorkId + ', ' + msg.id + ', \'' + escape(msg.message) + '\', ' + currentConId + '); return false; ">' +
															'<img src="img/icons/edit_small.png" width="12" height="12" title="' + i18n.t("core.workflow:editMessage", "Edit Message") + '" />' +
														'</a> ' +
														'&nbsp;' +
														'<a href="#" class="deleteMessage" onclick=" deleteAllocatedWorkMessage(this, ' + msg.id + '); return false; ">' +
															'<img src="img/icons/small_delete.png" width="12" height="12" title="' + i18n.t("core.workflow:deleteMessage", "Delete Message") + '" />' +
														'</a>';
										}
						content +=	'</div>';
						// append new content to page
						$j('div#awmessages' + allocatedWorkId).append(content);						
						// close overlay
						loadScript.closeOverlay();
					}
				}
			});
		}
	});
}

/**
 * this method deletes an allocated work message if the current contact is the author of the message
 * 
 * @param {Object} element the anchor clicked to delete message
 * @param {Integer} messageId id of the message to be deleted
 */
function deleteAllocatedWorkMessage(element, messageId)
{
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/engineerallocationmessageservice.js'), true,
	{
		callback: function()
		{	
			// call dwr service to add message
			engineerallocationmessageservice.ajaxDeleteEngineerAllocationMessage(messageId, 
			{
				callback:function(result)
				{
					// addition of message successful?
					if(result.success)
					{
						// remove message from page
						$j(element).closest('div').remove();
					}
					else
					{
						// show error message to user
						$j.prompt(result.message);
					}
				}
			});
		}
	});
}
							