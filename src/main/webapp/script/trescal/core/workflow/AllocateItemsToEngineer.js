/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	AllocateItemsToEngineer.js
*	DESCRIPTION		:	This page specific javascript file.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	28/02/2011 - JV - Created File
*					:	29/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

var count = 0;

/** 
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
 var menuElements = new Array( 	{ anchor: 'unallocated-link', block: 'unallocated-tab' },
 								{ anchor: 'allocated-link', block: 'allocated-tab' } );
 
 /**
  * array of objects containing the element name and corner type which should be applied.
  * applied in init() function.
  */
 var niftyElements = new Array ( { element: 'div.infobox', corner: 'normal' } );


function displayAllocationOptions(jobItemId, deptId)
{
	$j.ajax({
		url: "allocateitemtoengineer.htm",
		data: {
			jobItemId: jobItemId,
			deptId: deptId
		},
		async: true
	}).done(function(content) {
		loadScript.createOverlay('dynamic', i18n.t("core.workflow:allocateItemToEngineer", "Allocate Item To Engineer"), null, content, null, "400px", 600, 'person-' + jobItemId);
	})
}

function allocateItem(jobItemId)
{
	var personId = $j('div#allocateItemOverlay select#person-' + jobItemId).val();
	var date = $j('div#allocateItemOverlay select#date-' + jobItemId).val();
	var time = $j('div#allocateItemOverlay select#time-' + jobItemId).val();
	var message = $j('div#allocateItemOverlay textarea#message-' + jobItemId).val();
	var dateUntil = $j('div#allocateItemOverlay input#dateUntil-' + jobItemId).val();
	var timeUntil = $j('div#allocateItemOverlay input#timeUntil-' + jobItemId).val();
	var deptId = $j('div#allocateItemOverlay input#dept-' + jobItemId).val();
	
	$j('div#itemAllocations').append(
		'<div id="item-' + jobItemId + '">' + 
			'<input type="hidden" name="itemAllocations[' + count + '].jobItemId" value="' + jobItemId + '" />' +
			'<input type="hidden" name="itemAllocations[' + count + '].personId" value="' + personId + '" />' +
			'<input type="hidden" name="itemAllocations[' + count + '].deptId" value="' + deptId + '" />' +
			'<input type="hidden" name="itemAllocations[' + count + '].date" value="' + date + '" />' +
			'<input type="hidden" name="itemAllocations[' + count + '].time" value="' + time + '" />' +
			'<input type="hidden" name="itemAllocations[' + count + '].dateUntil" value="' + dateUntil + '" />' +
			'<input type="hidden" name="itemAllocations[' + count + '].timeUntil" value="' + timeUntil + '" />' +
			'<input type="hidden" name="itemAllocations[' + count + '].message" value="' + message + '" />' +
		'</div>'
	);
	
	count++;
	$j('a#linkAlloc-' + jobItemId).before($j('div#allocateItemOverlay select#person-' + jobItemId + ' option:selected').text()).remove();
	
	// close overlay
	loadScript.closeOverlay();
}

function deallocateItem(jobItemId)
{
	if (confirm(i18n.t("core.workflow:confirmDeallocateItem", "Are you sure you wish to de-allocate this item?")) == true)
	{
		$j('div#itemAllocations').append(
			'<div id="item-' + jobItemId + '">' + 
				'<input type="hidden" name="itemAllocations[' + count + '].jobItemId" value="' + jobItemId + '" />' +
				'<input type="hidden" name="itemAllocations[' + count + '].deallocation" value="true" />' +
			'</div>'
		);
	
		count++;
		
		$j('div#allocated-' + jobItemId).remove();
	}
}

function chooseLongCalForItem(checked, jobItemId)
{
	if(checked)
	{
		$j('div#longcal-' + jobItemId).removeClass('hid').addClass('vis');
		updateEndTime(jobItemId);
	}
	else
	{
		$j('div#longcal-' + jobItemId).removeClass('vis').addClass('hid');
		$j('div#allocateItemOverlay input#dateUntil-' + jobItemId).val('');
		$j('div#allocateItemOverlay input#timeUntil-' + jobItemId).val('');
	}
}

function updateEndTime(jobItemId)
{
	if($j('input#longcalcheck-' + jobItemId).is(':checked'))
	{
		var startDate = $j('div#allocateItemOverlay select#date-' + jobItemId).val();
		var startTime = $j('div#allocateItemOverlay select#time-' + jobItemId).val();
		var halfDays = $j('div#allocateItemOverlay select#halfdays-' + jobItemId).val();
		
		// load the service javascript file if not already
		loadScript.loadScriptsDynamic(new Array('dwr/interface/engineerallocationservice.js'), true,
		{
			callback: function()
			{			
				// call dwr service to delete upcoming work
				engineerallocationservice.ajaxGetFinishDate(startDate, startTime, halfDays, 
				{
					callback: function (resWrap)
					{
						if(resWrap.success)
						{
							$j('div#allocateItemOverlay input#dateUntil-' + jobItemId).val(resWrap.results.formattedDate);
							$j('div#allocateItemOverlay input#timeUntil-' + jobItemId).val(resWrap.results.time);
							$j('div#allocateItemOverlay span#calculatedFinish-' + jobItemId).html(i18n.t("core.workflow:finishTime", "Finish Time:") + ' ' + resWrap.results.formattedDateAndTime);
						}
						else
						{
							$j('div#allocateItemOverlay span#calculatedFinish-' + jobItemId).html(i18n.t("error", "Error:") + ' ' + resWrap.message);
						}
					}
				});
			}
		});
	}
}
