/**
 * Filename : EditItemStatus.js
 *
 * Contains common functions used by multiple screens
 */

/**
 * adds a state group row to the user interface using a hidden template row as the source
 */
function addStateGroupRow() {
	var sourceRowId = 'templaterow-sgl';
	var targetRowId = 'row-sgl-'+insertionStateGroupLinks;
	var sourceRow = $j('#'+sourceRowId);
	var newRow = sourceRow.clone();
	// Change ID and name of this row
	newRow.attr('id', targetRowId);
	newRow.attr('class', 'vis');
	// Rename inputs and selects
	var renames = ['id','stateGroup','linkType','delete'];
	$j(renames).each(function(index) {
		var oldId = 'template-sgl-'+renames[index];
		var newName = 'stateGroupLinks['+insertionStateGroupLinks+'].'+renames[index];
		var newId = 'stateGroupLinks'+insertionStateGroupLinks+'.'+renames[index];
		console.log("Setting name "+newName+" on id "+oldId);
		console.log("Setting id "+newId+" on id "+oldId);
		newRow.find('#'+oldId).attr('name', newName).attr('id', newId);
	});
	newRow.insertBefore(sourceRow);
	
	// Increment global insertion index for next insertion
	insertionStateGroupLinks++;
	
	// Focus on the first select input of the newly created element
	$j('#'+targetRowId).find('select:visible:first').focus();
}

/**
 * 
 * @param element - img of the delete elemnent being clicked on 
 * @returns
 */

function deleteInputRow(element) {
	$j(element).closest('tr').remove();
}