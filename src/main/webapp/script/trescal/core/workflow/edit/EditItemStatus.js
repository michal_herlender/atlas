/**
 * Filename : EditItemStatus.js
 */



/**
 * adds a next activity row to the user interface using a hidden template row as the source
 */
function addNextActivityRow() {
	var sourceRowId = 'templaterow-activity';
	var targetRowId = 'row-activity-'+insertionNextActivities;
	var sourceRow = $j('#'+sourceRowId);
	var newRow = sourceRow.clone();
	// Change ID and name of this row
	newRow.attr('id', targetRowId);
	newRow.attr('class', 'vis');
	// Rename selects/inputs
	var renames = ['id','activityId','manualEntryAllowed','delete'];
	$j(renames).each(function(index) {
		var oldId = 'template-activity-'+renames[index];
		var newName = 'nextActivities['+insertionNextActivities+'].'+renames[index];
		var newId = 'nextActivities'+insertionNextActivities+'.'+renames[index];
		console.log("Setting name "+newName+" on id "+oldId);
		console.log("Setting id "+newId+" on id "+oldId);
		newRow.find('#'+oldId).attr('name', newName).attr('id', newId);
	});
	newRow.insertBefore(sourceRow);
	
	// Increment global insertion index for next insertion
	insertionNextActivities++;
	
	// Focus on the first select input of the newly created element
	$j('#'+targetRowId).find('select:visible:first').focus();
}