/**
 * Filename : EditItemActivity.js
 */

/**
 * Change handler indicating that a new action outcome row should be added
 * A hidden template row is used to create the new row 
 */
function addActionOutcomeRow() {
	var sourceRowId = 'templaterow-ao';
	var targetRowId = 'row-ao-'+insertionActionOutcome;
	var sourceRow = $j('#'+sourceRowId);
	var newRow = sourceRow.clone();
	// Change ID and name of this row
	newRow.attr('id', targetRowId);
	newRow.attr('class', 'vis');
	// Rename selects/inputs
	var renames = ['type','typename','value','id','active','defaultOutcome','positiveOutcome','delete'];
	$j(renames).each(function(index) {
		var oldId = 'template-ao-'+renames[index];
		var newName = 'actionOutcomeDtos['+insertionActionOutcome+'].'+renames[index];
		var newId = 'actionOutcomeDtos'+insertionActionOutcome+'-'+renames[index];
		console.log("Setting name "+newName+" on id "+oldId);
		console.log("Setting id "+newId+" on id "+oldId);
		newRow.find('#'+oldId).attr('name', newName).attr('id', newId);
	});
	newRow.insertBefore(sourceRow);
	
	// Increment global insertion index for next insertion
	insertionActionOutcome++;
	
	// Focus on the first select input of the newly created element
	$j('#'+targetRowId).find('select:visible:first').focus();	
}

/**
 * Change handler indicating that the action outcome type has changed.
 * The template row's type column (span + hidden input) and value column (select) are
 * refreshed to match the new action outcome type. 
 */
function handleActionOutcomeTypeChange() {
	var actionOutcomeType = $j("#selectActionOutcomeType").val();
	$j("input#template-ao-type").val(actionOutcomeType);
	$j("span#template-ao-typename").empty().append(actionOutcomeType);
	// Remove old select options, add new from array
	var selectValue = $j("select#template-ao-value"); 
	selectValue.empty();
	var newOptions = actionOutcomeValues.get(actionOutcomeType);
	$j.each(newOptions, function(index) {
		selectValue.append("<option value='"+newOptions[index]+"'>"+newOptions[index]+"</option>");
	});
}

/**
 * Change handler indicating that a lookup will be connected to the activity
 * The lookup selection is shown, the status selection is hidden.
 * Enabler for action outcomes is shown
 * All areas related to action outcomes are displayed, if action outcomes are enabled. 
 */
function showLookup() {
	$j("li#connected-lookup").addClass('vis').removeClass('hid');
	$j("li#connected-status").addClass('hid').removeClass('vis');
	$j("li#action-outcome-enabled").addClass('vis').removeClass('hid');
	if ($j("#radioAoEnabled").attr('checked')) {
		showActionOutcomes();
	}
	else {
		hideActionOutcomes();
	}
	
}

/**
 * Change handler indicating that a status will be connected to the activity
 * The lookup selection is hidden, the status selection is shown.
 * Enabler for action outcomes is hidden
 * All other areas related to action outcomes are hidden. 
 */
function showStatus() {
	$j("li#connected-lookup").addClass('hid').removeClass('vis');
	$j("li#connected-status").addClass('vis').removeClass('hid');
	$j("li#action-outcome-enabled").addClass('hid').removeClass('vis');
	hideActionOutcomes();
}

function showActionOutcomes() {
	$j("li#action-outcome-type").addClass('vis').removeClass('hid');
	$j("div#section-actionoutcome").addClass('vis').removeClass('hid');
}

function hideActionOutcomes() {
	$j("li#action-outcome-type").addClass('hid').removeClass('vis');
	$j("div#section-actionoutcome").addClass('hid').removeClass('vis');
}