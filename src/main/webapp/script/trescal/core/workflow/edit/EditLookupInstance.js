/**
 * Filename : EditLookupFunction.js
 */

/**
 * Change handler indicating that a new outcome status row should be added
 * A hidden template row is used to create the new row 
 */
function addOutcomeStatusRow() {
	var sourceRowId = 'templaterow-os';
	var targetRowId = 'row-os-'+insertionOutcomeStatus;
	var sourceRow = $j('#'+sourceRowId);
	var newRow = sourceRow.clone();
	// Change ID and name of this row
	newRow.attr('id', targetRowId);
	newRow.attr('class', 'vis');
	// Rename selects/inputs (note, selected lookups are handled separately)
	var renames = ['id','lookupOrStatus','lookupId','statusId','delete'];
	$j(renames).each(function(index) {
		var oldId = 'template-os-'+renames[index];
		var newName = 'outcomeStatusDtos['+insertionOutcomeStatus+'].'+renames[index];
		var newId = 'outcomeStatusDtos'+insertionOutcomeStatus+'.'+renames[index];
		console.log("Setting name "+newName+" on id "+oldId);
		console.log("Setting id "+newId+" on id "+oldId);
		newRow.find('#'+oldId).attr('name', newName).attr('id', newId);
	});
	// Rename selected lookups radio buttons
	var selectedrenames = [];
	for (var i = 0; i < maxMessageCount; i++) {
		selectedrenames.push('selectedlookup-'+i);
	}
	$j(selectedrenames).each(function(index) {
		var oldId = 'template-os-'+selectedrenames[index];
		var newName = 'selectedLookups['+index+']';
		var newId = 'selectedlookup-'+index+'-'+insertionOutcomeStatus;
		console.log("Setting name "+newName+" on id "+oldId);
		console.log("Setting id "+newId+" on id "+oldId);
		console.log("Setting value "+insertionOutcomeStatus+" on id "+oldId);
		newRow.find('#'+oldId).attr('name', newName).attr('id', newId).attr('value',insertionOutcomeStatus);
	});
	
	newRow.insertBefore(sourceRow);
	
	// Increment global insertion index for next insertion
	insertionOutcomeStatus++;
	
	// Focus on the first select input of the newly created element
	$j('#'+targetRowId).find('select:visible:first').focus();		
}

/**
 * Change handler indicating that a new lookup function has been selected.
 * The lookup function area is re-written with the results of the new lookup.
 * The selection area for lookup wiring is also changed (columns of radio buttons disabled or enabled)
 */
function selectLookupFunction(lookupfunction) {
	$j.ajax({
		url: "lookupresultstable.htm?function="+lookupfunction,
		method: "GET",
		async: true
	}).done(function(result) {
		// Replace the existing table with the new table
		$j("div#section-lookupresults").html(result);
		// Update the current message count
		currentMessageCount = $j("#lookup-message-count").attr("value");
		// Update the radio buttons for any visible selctions for lookups
		for (var row = 0; row < insertionOutcomeStatus; row++) {
			for (var col = 0; col < maxMessageCount; col++) {
				var selradioid = "#selectedlookup-"+col+"-"+row; 
				if (col < currentMessageCount) $j(selradioid).attr('disabled',false);
				else $j(selradioid).attr('disabled','disabled');
			}
		}
		// Update the radio buttons for hidden template rows
		for (var col = 0; col < maxMessageCount; col++) {
			var selradioid = "#template-os-selectedlookup-"+col; 
			if (col < currentMessageCount) $j(selradioid).attr('disabled',false);
			else $j(selradioid).attr('disabled','disabled');
		}
		
	});
}

/**
 * Change handler for the lookup vs status selector for an individual outcome status
 * Hides or shows the relevant selector
 * 
 * var element: the lookup vs status select element
 */
function toggleOutcomeStatus(element) {
	if (element.value == "true") {
		// Lookup is selected
		$j(element).closest("tr").find(".innerstatus").addClass("hid").removeClass("vis");
		$j(element).closest("tr").find(".innerlookup").addClass("vis").removeClass("hid");
	}
	else {
		// Status is selected
		$j(element).closest("tr").find(".innerlookup").addClass("hid").removeClass("vis");
		$j(element).closest("tr").find(".innerstatus").addClass("vis").removeClass("hid");
	}
}