/**
 * Filename : EditHook.js
 */

var focusElement = 'name';

/**
 * Change handler indicating that a new hook activity row should be added
 * A hidden template row is used to create the new row 
 */
function addHookActivityRow() {
	var sourceRowId = 'templaterow-ha';
	var targetRowId = 'row-ha-'+insertionHookActivity;
	var sourceRow = $j('#'+sourceRowId);
	var newRow = sourceRow.clone();
	// Change ID and name of this row
	newRow.attr('id', targetRowId);
	newRow.attr('class', 'vis');
	// Rename selects/inputs
	var renames = ['id','beginActivity','completeActivity','statusOrActivity','startActivityId','startStatusId','finishActivityId','delete'];
	
	$j(renames).each(function(index) {
		var oldId = 'template-ha-'+renames[index];
		var newName = 'hookActivityDtos['+insertionHookActivity+'].'+renames[index];
		var newId = 'hookActivityDtos'+insertionHookActivity+'.'+renames[index];
		console.log("Setting name "+newName+" on id "+oldId);
		console.log("Setting id "+newId+" on id "+oldId);
		newRow.find('#'+oldId).attr('name', newName).attr('id', newId);
	});
	
	newRow.insertBefore(sourceRow);
	
	// Increment global insertion index for next insertion
	insertionHookActivity++;
	
	// Focus on the first select input of the newly created element
	$j('#'+targetRowId).find('select:visible:first').focus();		
}


/**
 * Change handler for the status vs activity selector for an individual hook activity
 * Hides or shows the relevant selector
 * 
 * var element: the status vs activity select element
 */
function toggleHookActivity(element) {
	if (element.value == "true") {
		// Status is selected
		$j(element).closest("tr").find(".inneractivity").addClass("hid").removeClass("vis");
		$j(element).closest("tr").find(".innerstatus").addClass("vis").removeClass("hid");
	}
	else {
		// Activity is selected
		$j(element).closest("tr").find(".innerstatus").addClass("hid").removeClass("vis");
		$j(element).closest("tr").find(".inneractivity").addClass("vis").removeClass("hid");
	}
}