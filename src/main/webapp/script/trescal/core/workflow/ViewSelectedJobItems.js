/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	ViewSelectedJobItems.js
*	DESCRIPTION		:	Page-specific javascript file.
*	DEPENDENCIES	:   -
*
*	TO-DO			:						
*	KNOWN ISSUES	:	-
*	HISTORY			:	01/10/2008 - SH - Created File
*
****************************************************************************************************************************************/


/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

function filterJobs()
{
	var transOptId = $j('select#transOptSelector').val();
	var jobType = $j('select#jobTypeSelector').val();
	var companyId = $j('select#companySelector').val();
	var subdivId = $j('select#subdivSelector').val();
	var personId = $j('select#csrSelector').val();
	var stateId = $j('select#stateSelector').val();
	var procId = $j('select#procSelector').val();
	var businessSubId = $j('select#businessSubSelector').val();
	
	// job item filters
	// unhide all child rows
	$j('tr.child-row').removeClass('hid').addClass('vis');
	// filter by CSR
	if(personId == -1) $j('tr.child-row').not('.csr--').removeClass('vis').addClass('hid');
	else if(personId > 0) $j('tr.child-row').not('.csr-' + personId + '-').removeClass('vis').addClass('hid');
	// filter by state
	if(stateId != null && stateId != 0) $j('tr.child-row').not('.state-' + stateId + '-').removeClass('vis').addClass('hid');
	// filter by procedure
	if(procId != null && procId != 0) $j('tr.child-row').not('.proc-' + procId + '-').removeClass('vis').addClass('hid');
	// filter by service type
	var serviceTypeIds= [];
	$j.each($j("input[name='serviceType']:not(:checked)"), function(){
		serviceTypeIds.push($j(this).val());
    });
	for (st of serviceTypeIds) {
		$j('tr.child-row.st-' + st + '-').removeClass('vis').addClass('hid');
	}
	
	// job filters
	// unhide all parent tables
	$j('table.parent-table').removeClass('hid').addClass('vis');
	// filter by transport option
	if(transOptId == -1) $j('table.parent-table').not('.transOpt--').removeClass('vis').addClass('hid');
	else if(transOptId != 0) $j('table.parent-table').not('.transOpt-' + transOptId + '-').removeClass('vis').addClass('hid');
	// filter by job type
	if(jobType != 0) $j('table.parent-table').not('.jobType-' + jobType + '-').removeClass('vis').addClass('hid');
	// filter by company
	if(companyId != 0) $j('table.parent-table').not('.company-' + companyId + '-').removeClass('vis').addClass('hid');
	// filter by subdiv
	if(subdivId != 0) $j('table.parent-table').not('.subdiv-' + subdivId + '-').removeClass('vis').addClass('hid');
	// filter by business subdiv
	if(businessSubId != null && businessSubId != 0) $j('table.parent-table').not('.businesssub-' + businessSubId + '-').removeClass('vis').addClass('hid');
	
	// hide all parent tables without visible child rows
	$j('.parent-table').each(function() {
		var hasUnhiddenChild = false;
		$j(this).find('tr.child-row').each(function() {
			if(!($j(this).hasClass('hid'))) hasUnhiddenChild = true;
		});
		if(!hasUnhiddenChild) $j(this).removeClass('vis').addClass('hid');
	});
	
	// update count at top of page
	var transOptStr = '';
	if(transOptId != 0) transOptStr = transOptId + '-';
	$j('span#itemCount').html($j('table[class*="transOpt-' + transOptStr + '"][class*="vis"] table tbody tr.vis').length);
}

function selectAll(){
	if(document.getElementById('selectAll').checked){
		    $j('input[type="checkbox"]').each(function() {
		      this.checked = true;
		    });
	}
	else {
		$j('input[type="checkbox"]').each(function() {
		      this.checked = false;
		    });
	}
}

function showCheckboxes(){
	var checkboxes = document.getElementById("checkboxes");
	if(checkboxes.style.display == "block"){
		checkboxes.style.display = "none";
	}
	else {
		 checkboxes.style.display = "block";
	}
}


