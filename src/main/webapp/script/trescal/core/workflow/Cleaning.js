/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	Cleaning.js
*	DESCRIPTION		:	This page specific javascript file.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	16/06/2010 - JV - Created File
*					:	29/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

function updateRemark()
{
	var text = "";
	if ($j('input:checkbox#inspected').is(':checked')) 
	{
		text += i18n.t("core.workflow:inspected", "Inspected") + " - " + $j("input[name='inspectstatus']:checked").attr('value') + ". ";
	}
	if ($j('input:checkbox#cleaned').is(':checked'))
	{
		text += i18n.t("core.workflow:cleaned", "Cleaned") + ". ";
	}		
	if ($j('input:checkbox#integrity').is(':checked')) 
	{
		text += i18n.t("core.workflow:integritySeals", "Integrity seals") + " " + $j("input[name='integritystatus']:checked").val() + ". ";
	}
	$j('textarea#remark').val(text);
}

var focusElement = 'plantid';

function init()
{
	// load stylesheet using the dom if not already
	loadScript.loadStyle('styles/jQuery/jquery-boxy.css', 'screen');
	
	updateRemark();
}

function addItems(barcode)
{
	var existe = false;
	$j('input[name="plantids"]').each(function(){
		if($j(this).val() == barcode) 
			{
				existe = true;
			}
	});
	if(!existe)
		$j('div#itemsHistory').prepend('<div><img src="img/icons/greentick.png" width="16" height="16"><input type="hidden" name="plantids" value="'+barcode+'"/>'+barcode+'</div>');			
}
