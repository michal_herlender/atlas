/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	ViewUpcomingWork.js
*	DESCRIPTION		:	This page specific javascript file.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	16/07/2009 - JV - Created File
*					:	29/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'script/thirdparty/jQuery/jquery.boxy.js');

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

var allocationBodiesWithMessages = [];

var data = new Array();

 /**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{
	// load stylesheet using the dom if not already
	loadScript.loadStyle('styles/jQuery/jquery-boxy.css', 'screen');
}

/**
 * this method adds upcoming work to the calendar
 * 
 * @param {boolean} add indicates whether we are adding new upcoming work or updating existing upcoming work
 */
function addUpcomingWork(add, upcomingId)
{
	// get required field values from the jquery thickbox
	var title = $j('div#createUpcomingWorkBoxy input[name="title"]').val();
	var desc = $j('div#createUpcomingWorkBoxy textarea[name="desc"]').val();
	var deptid = $j('div#createUpcomingWorkBoxy select[name="department"]').val();
	var responsibleuserid = $j('div#createUpcomingWorkBoxy select[name="businessuser"]').val();
	var coid = $j('div#createUpcomingWorkBoxy input[name="coid"]').val();
	var list =$j('div#createUpcomingWorkBoxy input[name="inputJobNo[]"]').val();
	var personid = 0;
	// personid provided?
	if ($j('div#createUpcomingWorkBoxy input[name="personid"]').length)
	{
		personid = $j('div#createUpcomingWorkBoxy input[name="personid"]').val();
	}
	var start = $j('div#createUpcomingWorkBoxy input[name="startDate"]').val();
	var end = $j('div#createUpcomingWorkBoxy input[name="dueDate"]').val();
	var jobNoList = new Array();


	if (add) {
		$j('ul#jobnoslist > li').each(function(i){
			let jobNo = $j(this).find('input:first').val();
			jobNoList.push(new UpcomingWorkJobLinkAjaxDto(null,null,jobNo,null,true));
		});
		let addUpcomingWorkDto = new UpcomingWorkDto(title,desc,deptid,coid,personid,responsibleuserid,start,end,null,jobNoList);

		fetch("upcomingWork/add", {
			method: "POST",
			headers: {"Content-Type": "application/json"},
			body: JSON.stringify(addUpcomingWorkDto)
		}).then(res => {
			if (res.ok) {
				location.reload();
				console.log(res.json())
			} else {
				Promise.resolve(res.json())
					.then(message => $j.prompt(message))
			}
		})
	}else{
		let updateUpcomingWorkDto = new UpcomingWorkDto(title,desc,deptid,coid,personid,responsibleuserid,start,end,upcomingId,jobNoList);

		fetch("upcomingWork/update", {
			method: "PUT",
			headers: {"Content-Type": "application/json"},
			body: JSON.stringify(updateUpcomingWorkDto)
		}).then(res => {
			if (res.ok) {
				location.reload();
				console.log(res.json())
			} else {
				Promise.resolve(res.json())
					.then(message => $j.prompt(message))
			}
		})
	}
}

/**
 * this method creates the content for the upcoming work thickbox and initialises any search plugins
 * 
 * @param {String} type the type indicates if we are adding or editing
 * @param {Integer} upcomingId the id of the upcoming work if editing
 * @param {Integer} coid the id of the selected company if editing
 * @param {String} coname the name of the selected company if editing
 * @param {Integer} subdivid the id of the selected subdivision if editing
 * @param {Integer} contpid the id of the selected contact if editing
 * @param {Integer} respuserid the id of the selected responsible user if editing
 * @param {String} title the title of the upcoming work if editing
 * @param {String} description the description of the upcoming work if editing
 * @param {Integer} deptid the id of the selected department if editing
 * @param {String} startDate the selected start date if editing
 * @param {String} dueDate the selected due date if editing
 */
function createUpcomingWork(type, upcomingId, coid, coname, subdivid, contpid, respuserid, title, description, deptid, startDate, dueDate)
{
	// load stylesheet using the dom
	loadScript.loadStyle('styles/jQuery/jquery-ui.css', 'screen');
	// load the javascript files required
	loadScript.loadScriptsDynamic(new Array('dwr/interface/departmentservice.js',
											'script/trescal/core/utilities/searchplugins/jquery/CompanySearchJQPlugin.js',
											'script/thirdparty/jQuery/jquery.ui.js'), true,
	{
		callback: function()
		{
			// call dwr service to retrieve all current laboratory departments
			departmentservice.getByTypeAndSubdiv('LABORATORY', subdivid,
			{
				callback: function(depts)
				{

					// create content for upcoming work
					var titleAction = (type == 'Edit') 	? i18n.t("core.workflow:editUpcomingWork", "Edit Upcoming Work")
														: i18n.t("core.workflow:addUpcomingWork", "Add Upcoming Work");
					var content = 	'<div id="createUpcomingWorkBoxy" class="overflow">' +
										'<fieldset>' +
											'<legend>' + titleAction + '</legend>' +
											'<ol>' +
												'<li>' +
													'<label>' + i18n.t("companyLabel", "Company:") + ' *</label>' +
													'<div class="float-left">' +
														'<div class="compSearchJQPlugin"></div>' +
													'</div>' +
													'<div class="clear"></div>' +
												'</li>' +
												'<li id="contactLink">' +
													'<label>' + i18n.t("core.workflow:contactOptionalLabel", "Contact: (optional)") + '</label>' +
													'<span>' +
														'<a href="#" class="mainlink" onclick=" initialiseContactSelector($j(this).parent().parent(), ' + coid + ', \'' + coname + '\', ' + subdivid + ', ' + contpid + '); return false; ">' + i18n.t("core.workflow:addCompanyContact", "Add Company Contact") + '</a>' +
													'</span>' +
												'</li>' +
												'<li>' +
													'<label>' + i18n.t("core.workflow:departmentLabel", "Department:") + ' *</label>';
													if (depts.length == 0) {
														content += i18n.t("core.workflow:noDepartmentsExist", "No departments exist!");
													}
													else {
														content += '<select name="department">';
														$j.each(depts, function(i)
														{
															content += '<option value="' + depts[i].deptid + '" ';
															if (depts[i].deptid == deptid)
															{
																content += 'selected="selected" ';
															}
															content += '>' + depts[i].name + '</option>';
														});
													}
										content += 	'</select>' +
												'</li>' +
												'<li>' +
													'<label>' + i18n.t("core.workflow:titleLabel", "Title:") + '</label>' +
													'<input type="text" name="title" value="' + title + '" tabindex="102" />' +
												'</li>' +
												'<li>' +
													'<label>' + i18n.t("core.workflow:businessUserLabel", "Business User:") + ' *</label>' +
													'<select name="businessuser">';
														// if editing default to current selection, but if adding default to logged in user
														var defaultUserId = (respuserid != null && respuserid != 0) ? respuserid : currentContactId;
														$j.each(businessUsers, function(i)
														{
															content += '<option value="' + businessUsers[i].id + '" ';
															if (businessUsers[i].id == defaultUserId)
															{
																content += 'selected="selected" ';
															}
															content += '>' + businessUsers[i].name + '</option>';
														});
										content += 	'</select>' +
												'</li>' +
												'<li>' +
													'<label>' + i18n.t("descriptionLabel", "Description:") + '</label>' +
													'<textarea type="text" name="desc" value="' + description + '" cols="50" rows="4" tabindex="103">' + description + '</textarea>' +
												'</li>';
												if (type == 'Edit') {
													content += '<li>' +
													'<cwms-upcomingwork-job-link upcomingworkid="' + upcomingId + '" show-mode="EDIT"></cwms-upcomingwork-job-link>' +
													'</li>'
												}else if (type == 'Add'){
													content += '<li>' +
														'<ul class="jobList" id="jobnoslist" style="padding: 0;border-bottom: none !important;">'+
															'<p style="padding-left: 0 !important;">'+
																'<label>' + i18n.t("coveringJobs", "Covering Jobs") +':'+ '</label>'+
																'<a href="#" onclick="addJobNo()">'+
																'<img src="img/icons/add.png" width="16" height="16">'+
																'</a>'+

															'</p>'+

														'</ul>'+

														'</li>'
												}
										content += '<li>' +
													'<label>' + i18n.t("startDateLabel", "Start Date:") + ' *</label>' +
													'<input type="date" id="startDate" name="startDate" value="' + startDate + '" tabindex="104" autocomplete="off" />' +
												'</li>' +
												'<li>' + 
													'<label>' + i18n.t("endDateLabel", "End Date:") + ' *</label>' +
													'<input type="date" id="dueDate" name="dueDate" value="' + dueDate + '" tabindex="105" autocomplete="off" />' +
												'</li>' +
												'<li>' +
													'<label>&nbsp;</label>';
													if (depts.length > 0) {
														if (type == 'Add')
														{
															content += '<input type="button" value="' + i18n.t("core.workflow:addWork", "Add Work") + '" tabindex="106" onclick=" this.disabled = true; addUpcomingWork(true, null); return false; " /> ';	
														}
														else
														{
															content += '<input type="button" value="' + i18n.t("core.workflow:updateWork", "Update Work") + '" tabindex="106" onclick=" this.disabled = true; addUpcomingWork(false, ' + upcomingId + '); return false; " /> ';
														}
													}
													else {
														content += i18n.t("core.workflow:noDepartmentsExist", "No departments exist!");
													}
												content += '<input type="button" value="' + i18n.t("cancel", "Cancel") + '" tabindex="107" onclick=" Boxy.get($j(\'div#createUpcomingWorkBoxy\')).hide(); " />' +											
				 							 	'</li>' +
				 							'</ol>' + 
										'</fieldset>' +
									'</div>';
					// create new boxy pop up using content variable
					var createUpcomingWorkBoxy = new Boxy(content, {title: titleAction, modal: true, unloadOnHide: true});
					// show boxy pop up and adjust size
					createUpcomingWorkBoxy.tween(780,500);
					// initialise company search plugin
					$j('div#createUpcomingWorkBoxy div.compSearchJQPlugin').compSearchJQPlugin({	field: 'coid',
																									tabIndex: '101',
																									compNamePrefill: coname,
																									compIdPrefill: coid,
																									compCoroles: 'client,supplier,prospect,utility,business'});
					// apply focus to company input
					$j('div.compSearchJQPlugin input:first').focus();
					// initialise contact selector if editing upcoming work with contact selected
					if ((coid != 0) && (subdivid != 0) && (contpid != 0))
					{
						// initialise and prefill the contact selector
						initialiseContactSelector($j('li#contactLink'), coid, coname, subdivid, contpid);
					}
				}
			});
		}
	});			
}

/**
 * this method deletes the selected upcoming work from the database and refreshes the page.
 * 
 * @param {Integer} upcomingId the id of the upcoming work to delete
 */
function deleteUpcomingWork(upcomingId)
{

	fetch("upcomingWork/delete?upcomingWorkId="+upcomingId, {
		method: "DELETE"
	}).then(res => {
		if (res.ok) {
			location.reload();
			console.log(res.json())
		} else {
			Promise.resolve(res.json())
				.then(message => $j.prompt(message))
		}
	});
}

/**
 * this method initialises the cascading search plugin as the user has requested to add a contact
 * as well as company to the upcoming work
 * 
 * @param {Object} li list item from in which the method was called
 * @param {Integer} coid the id of the company prevoiusly selected
 * @param {String} coname the name of the company previously selected
 * @param {Integer} subdivid the id of the subdivision that could already have been selected
 * @param {Integer} contpid the id of the contact that could already have been selected
 */
function initialiseContactSelector(li, coid, coname, subdivid, contpid)
{
	// create variable to hold prefill values
	var prefillId = '';
	// company, subdiv and contact ids provided?
	if ((coid != null) && (subdivid != null) && (contpid != null))
	{
		// populate prefill values
		prefillId = '<input type="hidden" id="prefillIds" name="prefillIds" value="' + coid + ',' + subdivid + ',' + contpid + ',0" />';
	}
	// find company search plugin, remove it, append new cascading search, find label and change text, find and change anchor
	$j(li).prev().find('div').remove().end().append('<div id="cascadeSearchPlugin">' +
														'<input type="hidden" id="compCoroles" value="client,supplier,prospect,utility,business" />' +
														'<input type="hidden" id="cascadeRules" value="subdiv,contact" />' +
														'<input type="hidden" id="customHeight" value="340" />' +
														prefillId +
													'</div>' +																												
													'<div class="clear"></div>').next().find('label').text('Company Only:').end().find('span').empty()
													.append('<a href="#" class="mainlink" onclick=" initialiseCompanySelector($j(this).parent().parent(), ' + coid + ', \'' + coname + '\', ' + subdivid + ', ' + contpid + '); return false; ">' + i18n.t("core.workflow:chooseCompanyOnly", "Choose Company Only") + '</a>');
	// call main init function to create the cascading search plugin
	loadScript.createCascadePlug();	
}

/**
 * this method initialises the company search plugin as the user has requested to add just a company
 * without a contact to the upcoming work
 * 
 * @param {Object} li list item from in which the method was called
 * @param {Integer} coid the id of the company prevoiusly selected
 * @param {String} coname the name of the company previously selected
 * @param {Integer} subdivid the id of the subdivision that could already have been selected
 * @param {Integer} contpid the id of the contact that could already have been selected
 */
function initialiseCompanySelector(li, coid, coname, subdivid, contpid)
{
	// find cascade search plugin, remove it, append new company search, find label and change text, find and change anchor
	$j(li).prev().find('div').remove().end().append('<div class="float-left">' +
														'<div class="compSearchJQPlugin"></div>' +
													'</div>' +
													'<div class="clear"></div>').next().find('label').text('Contact: (optional)').end().find('span').empty()
													.append('<a href="#" class="mainlink" onclick=" initialiseContactSelector($j(this).parent().parent(), ' + coid + ', \'' + coname + '\', ' + subdivid + ', ' + contpid + '); return false; ">' + i18n.t("core.workflow:addCompanyContact", "Add Company Contact") + '</a>');
	// values available to initialise?
	if ((coid != 0) && (coname != ''))
	{
		// initialise the company search plugin with prefill values
		$j('div#createUpcomingWorkBoxy div.compSearchJQPlugin').compSearchJQPlugin({field: 'coid',																				
																					compCoroles: 'client,supplier,prospect,utility,business',
																					tabIndex: '101',
																					compNamePrefill: coname,
																					compIdPrefill: coid
																					});
	}
	else
	{
		// initialise the company search plugin without prefill values
		$j('div#createUpcomingWorkBoxy div.compSearchJQPlugin').compSearchJQPlugin({field: 'coid',																				
																					compCoroles: 'client',
																					tabIndex: '101'});
	}	
}

/**
 * this method hides and shows lab work depending on the user's checkbox selections on the page
 * 
 * @param {Integer} datesSize the amount of days being displayed
 */
function filterLabView(datesSize)
{
	// loop through all lab checkboxes
	$j('input[name^="filterlabs"]').each(function()
	{
		// this lab not checked?
		if (!this.checked)
		{	
			// get the table row for any related work to this lab and hide it
			$j('div[id^="' + this.value + '"]').closest('tr').fadeOut('slow').removeClass().addClass('labhidden');
			// all work in the current week has been hidden?
			if ($j('div[id^="' + this.value + '"]').closest('tbody').find('tr.labvisible').length < 1)
			{
				$j('div[id^="' + this.value + '"]').closest('tbody').each(function(index) {
					// is message that all work has been hidden present?
					if(!$j(this).find('tr.message').length > 0) {
						// show message that all work in the current week has been hidden
				    	$j(this).append('<tr class="message">' +
								'<td class="text-center" colspan="' + datesSize + '"> -- <span class="attention">' + i18n.t("core.workflow:workExistsButHasBeenHidden", "Work exists for this week but has been hidden - check all laboratories above to view") + '</span> -- </td>' +
							'</tr>');
				    }
				});			
			}
		}
		else
		{
			// is message that all work has been hidden present?
			if ($j('div[id^="' + this.value + '"]').closest('tbody').find('tr.message').length > 0)
			{
				// remove the message that all work has been hidden
				$j('div[id^="' + this.value + '"]').closest('tbody').find('tr.message').remove();
			}
			// display the selected lab row
			$j('div[id^="' + this.value + '"]').closest('tr').fadeIn('slow').removeClass().addClass('labvisible');
		}
	});
}

/**
 * this method selects/de-selects all labs and calls method to show or hide rows depending on selection
 * 
 * @param {Boolean} checked indicates if the all checkbox has been checked
 */
function selectAllLabs(checked)
{
	// is all checked?
	if (checked)
	{
		// check all labs
		$j('input[name^="filterlabs"]').check('on');
		// trigger onclick event of first lab checkbox
		$j('input[name^="filterlabs"]:first').trigger('onclick');
	}
	else
	{
		// un-check all labs
		$j('input[name^="filterlabs"]').check('off');
		// trigger onclick event of first lab checkbox
		$j('input[name^="filterlabs"]:first').trigger('onclick');
	}
}

/**
 * this method hides and shows allocated items depending on the user's checkbox selections on the page
 * 
 * @param {Integer} datesSize the amount of days being displayed
 */
function filterAllocations(datesSize)
{	
	// loop through all lab checkboxes
	$j('input[name^="filterallocations"]').each(function()
	{
		// this lab not checked?
		if (!this.checked)
		{	
			// get the table row for any related work to this lab and hide it
			$j('div[id^="allocation-' + this.value + '"]').each(function(index) {
				$j(this).closest('tr').fadeOut('slow').removeClass().addClass('labhidden');
				if($j(this).closest('tr').closest('tbody').find('tr.labvisible').length < 1)
				{
					var thisId = $j(this).closest('tr').closest('tbody').attr('id');
					var alreadyInArray = false;
					for (var i = 0; i < allocationBodiesWithMessages.length; i++) {
					    if(allocationBodiesWithMessages[i] == thisId)
					    {
					    	alreadyInArray = true;
					    }
					}
					
					if(!alreadyInArray)
					{
						$j(this).closest('tr').closest('tbody').append('<tr class="message">' +
								'<td class="text-center" colspan="' + datesSize + '"> -- <span class="attention">' + i18n.t("core.workflow:itemExistButHaveBeenHidden", "Item allocations exist for this week but have been hidden - check all laboratories above to view") + '</span> -- </td>' +
							'</tr>');
						allocationBodiesWithMessages.push(thisId);
					}
				}
			});
			
		}
		else
		{
			// is message that all work has been hidden present?
			if ($j('div[id^="allocation-' + this.value + '"]').closest('tbody').find('tr.message').length > 0)
			{
				// remove the message that all work has been hidden
				$j('div[id^="allocation-' + this.value + '"]').closest('tbody').find('tr.message').remove();
				
				var thisId = $j('div[id^="allocation-' + this.value + '"]').closest('tbody').attr('id');
				// remove the message that all work has been hidden
				for(var i = allocationBodiesWithMessages.length-1; i >= 0; i--)
				{
				    if(allocationBodiesWithMessages[i] == thisId)
				    {
				    	allocationBodiesWithMessages.splice(i,1);
				    }
				}
			}
			// display the selected lab row
			$j('div[id^="allocation-' + this.value + '"]').closest('tr').fadeIn('slow').removeClass().addClass('labvisible');
		}
	});
	
	$j(bodies).each(function(index) {
		// is message that all work has been hidden present?
			// show message that all work in the current week has been hidden
	    	$j(this).append('<tr class="message">' +
					'<td class="text-center" colspan="' + datesSize + '"> -- <span class="attention">' + i18n.t("core.workflow:itemExistButHaveBeenHidden", "Item allocations exist for this week but have been hidden - check all laboratories above to view") + ' -- </td>' +
				'</tr>');
	});
}

/**
 * this method selects/de-selects all allocated items and calls method to show or hide rows depending on selection
 * 
 * @param {Boolean} checked indicates if the all checkbox has been checked
 */
function selectAllAllocations(checked)
{
	// is all checked?
	if (checked)
	{
		// check all labs
		$j('input[name^="filterallocations"]').check('on');
		// trigger onclick event of first lab checkbox
		$j('input[name^="filterallocations"]:first').trigger('onclick');
		// empty array
		allocationBodiesWithMessages = [];
	}
	else
	{
		// un-check all labs
		$j('input[name^="filterallocations"]').check('off');
		// trigger onclick event of first lab checkbox
		$j('input[name^="filterallocations"]:first').trigger('onclick');
	}
}

function addNewJobNoField()
{
	$j('#jobnoslist').append('<div class="marg-top-small" id="newinvoicejoblink">'+
		'<input type="text" value=""/>'+
		'<input type="hidden" id="joblinkid" value=""/>'+
		'<a href="#" onclick="deleteInvoiceJobLink(\'\', this); return false;"><img src="img/icons/delete.png"/></a>'+
		'</div>');
}

function getUpcomingWorkJobLinks(upcomingWorkid)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/upcomingWorkJobLinkService.js'), true,
		{
			callback: function()
			{
				upcomingWorkJobLinkService.getJobByUpcomingWorkJobId(upcomingWorkid,
					{
						callback:function(results)
						{
							$j('#jobnoslist').empty();
							for(var i = 0; i < results.length; i++)
							{
								var upcomingWorkJobLink = results[i];
								$j('#jobnoslist').append('<div class="marg-top-small" id="upcomingworkjoblink'+upcomingWorkJobLink.id+'">'+
									'<input type="text" id="joblink'+upcomingWorkJobLink.id+'jobno" value="'+upcomingWorkJobLink.jobno+'"/>'+
									'<input type="hidden" id="joblink'+upcomingWorkJobLink.id+'id" value="'+upcomingWorkJobLink.id+'"/>'+
									'<a href="#" onclick="deleteUpcomingWorkJobLink('+upcomingWorkJobLink.id+', this); return false;"><img src="img/icons/delete.png"/></a>'+
									'</div>');
							}
							$j('#upcomingworkjoblinkediting').show();

							// hide the edit link
							$j('#editjobnoslink').hide();
						}
					});
			}
		});
}

function deleteUpcomingWorkJobLink(id, anchor)
{
	// no id means that this joblink hasn't been persisted yet,
	// so it is safe to just delete the field from the page
	if(id == '')
	{
		$j(anchor).parent().remove();
	}
	else
	{
		// load the service javascript file if not already
		loadScript.loadScriptsDynamic(new Array('dwr/interface/upcomingWorkJobLinkService.js'), true,
			{
				callback: function()
				{
					upcomingWorkJobLinkService.ajaxDeleteUpcomingWorkJobLink(id,
						{
							callback:function(results)
							{
								if(results.success !== true)
								{
									alert(results.message);
								}
								else
								{
									$j('#upcomingworkjoblinkspan'+id).remove();
									$j('#upcomingworkjoblink'+id).remove();
								}
							}
						});
				}
			});
	}
}

function addJobNo() {
	let html = '';
	html += '<li style="padding-left: 141px;border-bottom: none !important;">';
	html += '<label></label>';
	html += '<input type="text"  name="inputJobNo[]" value="" />';
	html += '<a href="#" onclick="removeJobNo(this); return false;">';
	html += '<img src="img/icons/delete.png" width="16" height="16">';
	html += '</a>';
	$j('#jobnoslist').append(html);
}

function removeJobNo(anchor) {
	$j(anchor).parent().remove();
}


function UpcomingWorkJobLinkAjaxDto(id,upcomingWorkId,jobNo,jobId,active){
	this.id = id;
	this.upcomingWorkId = upcomingWorkId;
	this.jobNo = jobNo;
	this.jobId = jobId;
	this.active = active;
}

function UpcomingWorkDto (title,desc,deptId,coId,personId,userResponsibleId,start,end,upcomingWorkId,jobNoList){
	this.upcomingWorkId = upcomingWorkId;
	this.title = title;
	this.desc = desc;
	this.deptId = deptId;
	this.coId = coId;
	this.personId = personId;
	this.userResponsibleId = userResponsibleId;
	this.start = start;
	this.end = end;
	this.jobNoList = jobNoList;
}
