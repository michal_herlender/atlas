/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	WorkflowBrowser.js
*	DESCRIPTION		:	This page specific javascript file.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	2015-10-16 GB Created file
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array ('script/thirdparty/jQuery/jstree/jstree.min.js');

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{
	// load stylesheet using the dom if not already
	loadScript.loadStyle('styles/jQuery/jstree/style.min.css', 'screen');
	
	// inline data demo
	$j('#data').jstree({
		'core' : {
			'data' : [
				{ "text" : "Root node", "children" : [
						{ "text" : "Child node 1" },
						{ "text" : "Child node 2" }
				]}
			]
		}
	});

	// ajax demo
	$j('#ajax').jstree({
		'core' : {
			'data' : {
				"url" : "./workflowbrowser.json",
				"dataType" : "json" // needed only if you do not supply JSON headers
			}
		}
	});
	
	$j('#ajax').on('loaded.jstree', function() {
		$j.jstree.reference('#ajax').open_all();
	});
}