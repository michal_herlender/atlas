/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	ViewQueuedCalibrations.js
*	DESCRIPTION		:	This page specific javascript file.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	12/06/2008 - JV - Created File
*
****************************************************************************************************************************************/

function checkAllOnPage(checkbox)
{
	$j('input:checkbox:not(:disabled)').attr('checked', $j(checkbox).is(':checked'));
}

function checkAllOnJob(jobId, checkbox)
{	
	$j('tr.jobId-' + jobId + ' > td > input:checkbox:not(:disabled)').attr('checked', $j(checkbox).is(':checked'));
}
