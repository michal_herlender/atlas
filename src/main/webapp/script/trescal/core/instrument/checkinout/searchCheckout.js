var pageImports = new Array('script/thirdparty/jQuery/jquery.dataTables.min.js');

function hidselector(val, id) {
	if (val == 'between') {
		$j('#' + id).removeClass('hid').addClass('vis');
	} else if (val == 'on') {
		$j('#' + id).removeClass('vis').addClass('hid');
	}
}


function init() {
	initdatatable();
}


function initdatatable() {
	var table = $j('#checkOutInstruments').DataTable({
		"pageLength" : 25,
	});
}