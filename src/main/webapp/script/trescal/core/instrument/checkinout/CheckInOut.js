var pageImports = new Array('script/thirdparty/jQuery/jquery.dataTables.min.js');

function init() {
	initdatatable();
}

function performCheckOut(val,val2) {
	var barCode = val;
	var jobNo = val2;

	$j.ajax({
		url : "performCheckOut.json?barcode=" + barCode + "&jobno=" + jobNo,
		dataType : 'json'
	}).done(function(res) {
		console.log(res);
		if (res.success === true) {
			// refresh page
			window.location.reload();
		} else {
			$j("#barcode").val('');
			$j("#jobNo").val('');
			$j.prompt(res.message);
		}
	});
}

function initdatatable() {
	var table = $j('#checkOutInstruments').DataTable({
		"pageLength" : 25,
	});

	var table = $j('#checkOutInstrumentsWithnoConfidenceCheck').DataTable({
		"pageLength" : 25,
	});
}

function displayMassCheckInLink() {
	// any quote item delete checkboxes checked?
	if ($j('input[type="checkbox"][name="checkInInstru"]:checked').length > 0) {
		$j('.checkInLink').removeClass('hid');
	} else {
		$j('.checkInLink').addClass('hid');
	}
}

function checkInInstruments() {

	var copyIds = '';
	if ($j('input[type="checkbox"][name^="checkInInstru"]:checked').length > 0) {
		$j('input[type="checkbox"][name^="checkInInstru"]:checked').each(
				function(i, n) {
					// not first iteration?
					if (i > 0) {
						copyIds += ',' + n.value;
					} else {
						copyIds += n.value;
					}
				});

		performMultipaleCheckIn(copyIds);

	} else {
		// show message to user
		$j.prompt('You have not chosen any instruments');
	}
}

function performMultipaleCheckIn(BarCodes) {

	$j.ajax({
		url : "performCheckIn.json",
		method : "POST",
		traditional : true,
		dataType : "json",
		data : {
			barcode : BarCodes
		},
		async : true
	}).done(function(data) {
		if (data.success == true) {
			location.reload();
		}
	});

}

