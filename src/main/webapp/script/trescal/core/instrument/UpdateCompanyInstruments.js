/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	UpdateCompanyInstrument.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	14/12/2010
*
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array ( 	'script/trescal/core/utilities/SubdivToContactFilter.js' );

/**
 * Array of objects containing the element name and corner type which should be applied.
 * Applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * this method is called from the company search jquery plugin when a new company is selected
 */
function companySearchJQPluginMethod()
{
	// refresh the search for subdivisions
	var coid = $j('*[name="companyid"]').val();
	var selectName = 'subdivid';
	getActiveCompanySubdivs(coid, selectName);
	// refresh the searches for addresses and contacts
	getActiveSubdivAddrsAndConts('0', $j('#addressid'), $j('#personid'), true, '');
}