/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	SearchInstrument.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	25/10/2007 - SH - Created File
*					:	30/0/2015 - TProvost - Fix location of SubdivSearchPlugin.js file
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'script/trescal/core/utilities/searchplugins/SubdivSearchPlugin.js' );

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'model';

/**
 * Array of autocomplete objects (multiple are possible) containing setup for 
 * jQueryUI initialization.
 * this is used in the init() function.
 */
var jquiAutocomplete = new Array(
				{
					inputFieldName: 'desc',
					inputFieldId: 'descid',
					source: 'searchdescriptiontags.json'
				},
				{
					inputFieldName: 'family',
					inputFieldId: 'familyid',
					source: 'searchfamilytags.json'
				},
				{
					inputFieldName: 'mfr',
					inputFieldId: 'mfrid',
					source: 'searchmanufacturertags.json'
				});

/**
 * this function changes the state of the company search plugin. initial page load displays a separate company and client search
 * plugin which can be used independantly. When the user chooses to cascade the company search new results for subdivision and contact
 * are created and the original contact search is hidden from view. Removing the cascade has the opposite effect.
 * 
 * @param {Boolean} cascade is a boolean value indicating whether the cascade effect should be initiated
 */
function toggleCascade(cascade)
{
	if (cascade == true)
	{
		// hide the list item which contains the default contact search plugin
		$j('#defaultContact, #defaultCompany').removeClass().addClass('hid');
		// remove the original search plugins from the page
		$j('.compSearchJQPlugin, #contSearchPlugin').remove();
		// append the new cascade search plugin div
		$j('ol#cascadingSearch li label').after('<div id="cascadeSearchPlugin">' +
													//<!-- company search results displayed here -->
													'<input type="hidden" id="cascadeRules" value="subdiv,contact" />' +
													'<input type="hidden" id="compCoroles" value="client,business" />' +
												'</div>');
		// show the ordered list which contains the cascading search
		$j('#cascadingSearch').removeClass().addClass('vis');
		// load the cascading search script and initialise
		$j.getScript('script/trescal/core/utilities/searchplugins/CascadeSearchPlugin.js', cascade = function()
		{
			CascadeSearchPlugin.init();
		});
		// clear the search focus variable
		searchFOCUS = '';
	}
	else
	{
		// remove the cascade search plugin object so that contact search plugin can be re-initialised
		delete CascadeSearchPlugin;
		// show the list item which contains the default contact search plugin
		$j('#defaultContact, #defaultCompany').removeClass().addClass('vis');
		// remove the original search plugins from the page
		$j('#cascadeSearchPlugin').remove();
		// append the new company and contact search plugin div's
		$j('li#defaultCompany div:first').append(	'<div class="compSearchJQPlugin">' +
														'<input type="hidden" name="field" value="coid" />' +
														'<input type="hidden" name="compCoroles" value="client,business" />' +
														'<input type="hidden" name="tabIndex" value="1" />' +
													'</div>');
		$j('li#defaultContact label').after(	'<div id="contSearchPlugin">' +
													//<!-- company search results displayed here -->
													'<input type="hidden" id="contCoroles" value="client,business" />' +
												'</div>');
		// initialise company and contact search plugins
		loadScript.init(true);
		// show the ordered list which contains the cascading search
		$j('#cascadingSearch').removeClass().addClass('hid');
		// clear the search focus variable
		searchFOCUS = '';
	}
}