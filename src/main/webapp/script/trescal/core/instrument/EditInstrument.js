/****************************************************************************************************************************************
 *												CWMS (Calibration Workflow Management System)
 *
 *												Copyright 2006, Antech Calibration Services
 *															www.antech.org.uk
 *
 *															All rights reserved
 *
 *														Document author: 	Stuart Harrold
 *
 *	FILENAME		:	EditInstrument.js
 *	DESCRIPTION		:
 *	DEPENDENCIES	:   -
 *
 *	TO-DO			: 	-
 *	KNOWN ISSUES	:	-
 *	HISTORY			:	05/11/2007
 *					:	13/10/2015 - TProvost - Manage i18n
 ****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array(
  "script/thirdparty/jQuery/jquery.boxy.js",
  "script/thirdparty/jQuery/jquery.jtip.js"
);



/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute.
 */
function init() {
  // load stylesheet using the dom if not already
  loadScript.loadStyle("styles/jQuery/jquery-boxy.css", "screen");
  loadScript.loadStyle("styles/jQuery/jquery-jtip.css", "screen");
  loadScript.loadStyle("styles/addnewitemstojob.css", "screen");

  checkUsageFieldVisibility();

  $j("#usages").on("change", function() {
    checkUsageFieldVisibility();
  });

  
  onStatusChange($j('#status'));
}

function checkUsageFieldVisibility() {
  if ($j("#usages").is(":checked")) {
    $j(".usageField").show();
  } else {
    $j(".usageField").hide();
  }
}

function checkChangeCompany() {
  if ($j("#onActiveJob").val() == "true") {
    if ($j("#oldCoid").val() != $j("#coid").val()) {
      if (
        confirm(
          "This instrument is on an active job. Would you like to update the on-behalf-of information on the job item?"
        )
      ) {
        $j("#updateOnBehalf").val(true);
      }
    }
  }
}


/**
 * this function appends the div needed to create a cascading search plugin to the page and
 * calls the initialisation function which builds the search plugin and prefills any data if needed.
 *
 * @param {String} coid is  the company id to be preloaded
 * @param {Integer} subdivid is the subdivision id to be preloaded
 * @param {Integer} personid is the contact id to be preloaded
 * @param {Integer} addrid is the address id to be preloaded
 * @param {Integer} locationid is the location id to be preloaded
 */
function createCascadingSearchPlugin(
  coid,
  subdivid,
  personid,
  addrid,
  locationid
) {
  // has the cascading search plugin already been created
  if ($j("div#cascadeSearchPlugin").length) {
    // do nothing
  } else {
    // load the service javascript file if not already
    loadScript.loadScriptsDynamic(
      new Array(
        "script/trescal/core/utilities/searchplugins/CascadeSearchPlugin.js"
      ),
      true,
      {
        callback: function() {
          // remove the hidden input fields for company, subdivision, contact and address currently on page
          $j("div#tempIds").remove();
          // append div in which the multi model search plugin will be created
          $j("li#contactSelector")
            .removeClass()
            .addClass("vis")
            .append(
              '<div id="cascadeSearchPlugin">' +
                '<input type="hidden" id="compCoroles" value="client,business" />' +
                '<input type="hidden" id="cascadeRules" value="subdiv,contact,address" />' +
                '<input id="prefillIds" type="hidden" value="' +
                coid +
                "," +
                subdivid +
                "," +
                personid +
                "," +
                addrid +
                ", " +
                locationid +
                '" />' +
                '<input type="hidden" id="searchLocations" value="true" />' +
                '<input type="hidden" id="noLocation" value="true" />' +
                "</div>" +
                // clear floats and restore page flow
                '<div class="clear"></div>'
            );
          // initialise the multi model search plugin
          CascadeSearchPlugin.init();
        }
      }
    );
  }
}

function setupHireInstInsertion(checkbox, hiddenInput) {
  // checkbox checked?
  if ($j(checkbox).is(":checked")) {
    // try and get model id
    var modelid = 0;
    // check models returned?
    if (
      $j('input[type="radio"][name="modelid"]').length > 0 &&
      $j('input[type="radio"][name="modelid"]:checked').length > 0
    ) {
      modelid = $j('input[type="radio"][name="modelid"]:checked').val();
    } else if ($j('input[type="hidden"][name="modelid"]').length > 0) {
      modelid = $j('input[type="hidden"][name="modelid"]').val();
    }

    // modelid found?
    if (modelid != null && modelid != 0) {
      // load the service javascript file if not already
      loadScript.loadScriptsDynamic(
        new Array("dwr/interface/hiremodelservice.js"),
        true,
        {
          callback: function() {
            // call dwr service to get hire model plant code
            hiremodelservice.getHireModelPlantCode(modelid, {
              callback: function(result) {
                // range deletion successful
                if (result.success) {
                  // adding hire instrument
                  $j(hiddenInput).val("true");
                  $j(".hireInst")
                    .removeClass("hid")
                    .addClass("vis");
                  $j("span.hirePlantReview").html(
                    i18n.t("core.instrument:hiremodel.hireModelPlantCode", {
                      modelName: result.results,
                      defaultValue:
                        "Your plant number should be prefixed with the following code for your chosen model <span class='bold'>" +
                        result.results +
                        "</span>, please check your entry"
                    }) +
                      ' (<span class="bold">\'' +
                      $j('input[type="text"][name="plantno"]').val() +
                      "'</span>)"
                  );
                } else {
                  // uncheck hire
                  $j("#hirecheckbox").removeAttr("checked");
                  // request user selects valid hire model
                  $j.prompt(result.message);
                }
              }
            });
          }
        }
      );
    } else {
      // uncheck hire
      $j(checkbox).attr("checked", "");
      // request user selects valid hire model
      $j.prompt(
        i18n.t(
          "core.instrument:hiremodel.selectValidHireModel",
          "You must select a valid hire model before continuing with this form"
        )
      );
    }
  } else {
    // not adding hire instrument
    $j(hiddenInput).val("false");
    $j(".hireInst")
      .removeClass("vis")
      .addClass("hid");
  }
}


function selectAllWI(modelid){
	const element = document.querySelector('cwms-modelcode');
	if(element){
		element.modelid = modelid;
		element.selectAll();
	}
	
}

function onStatusChange(ele) {
	let selectedStatus = $j(ele).val();
	let isFirst = true;
	$j('#usageTypeId option').each(function(index) {
		if($j(this).attr('data-linkedstatus') == selectedStatus) {
			$j(this).removeClass('hid').addClass('vis');
			if(isFirst) {
				$j(this).attr('selected','selected');
				isFirst = false;
			}
		}
		else {
			$j(this).removeClass('vis').addClass('hid');
			$j(this).removeAttr('selected');
		}
	});
}