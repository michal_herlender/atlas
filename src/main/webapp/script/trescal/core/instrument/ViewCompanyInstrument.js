/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	ViewCompanyInstrument.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	05/11/2007
*					:	13/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array ( 	'script/trescal/core/utilities/SubdivToContactFilter.js' );

/** 
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = 	new Array( { anchor: 'searchresults-link', block: 'searchresults-tab' },
					      	   { anchor: 'searchbasket-link', block: 'searchbasket-tab' });

/**
 * Array of autocomplete objects (multiple are possible) containing setup for 
 * jQueryUI initialization.
 * this is used in the init() function.
 */
var jquiAutocomplete = new Array(
				{
					inputFieldName: 'descNm',
					inputFieldId: 'descId',
					source: 'searchdescriptiontags.json'
				});

/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'search\\.barcode';

/**
 * Array of objects containing the element name and corner type which should be applied.
 * Applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * shows a warning if the user is leaving a page which has items
 * in a basket. Only works in IE and Moz 1.7 +.
 */
var showUnLoadWarning = true;

/**
 * perform check before the user leaves the page
 */
window.onbeforeunload = function()
{
	if(parseInt($j('span.basketCount:first').text()) > 0 && showUnLoadWarning)
	{
		showUnLoadWarning = true;
		return i18n.t("warnLoseCurrentBasket", "If you leave this page you will lose the contents of your current basket.");
	}
};

/**
 * this method generates the basket content for an instrument
 * 
 * @param {Object} tr table row of instrument to be added to basket
 * @param {Integer} instcount the current instrument count
 * @param {Integer} iteration how many iterations of loop have been performed?
 * @returns {String} content html for basket
 */
function generateBasketContentForSelection(tr, instcount, iteration)
{
	// create new content variable
	var content = '';
	// get values
	var plantid = $j.trim($j(tr).find('td.barcode').text());
	var definst = $j.trim($j(tr).find('td.desc a').text().replace(/"/g, '&quot;'));
	var serial = $j.trim($j(tr).find('td.serialno').text());
	var plant = $j.trim($j(tr).find('td.plantno').text());	
	// first iteration?
	if (iteration == 0)
	{
		// the empty basket message is present so remove it
		if ($j('table.companyInstBasket tbody:first tr:first td').length == 1)
		{
			// remove message
			$j('table.companyInstBasket tbody:first').remove();
		}
	}
	// add one to instrument count
	instcount += 1;	
	// add new content to variable
	content +=	// add new tbody for instrument in basket
				'<tbody id="basketitem' + plantid + '">' +
					'<tr>' +
						'<td class="center">' + (instcount) + '</td>' +
						'<td>' + plantid + '<input type="hidden" name="basketIds" value="' + plantid + '" /></td>' +
						'<td>' + definst + '<input type="hidden" name="basketDefInsts" value="' + definst + '" /></td>' +
						'<td>' + serial + '<input type="hidden" name="basketSerials" value="' + serial + '" /></td>' +
						'<td>' + plant + '<input type="hidden" name="basketPlants" value="' + plant + '" /></td>' +
						'<td class="center">' +
							'<a href="#" class="deleteAnchor" onclick=" deleteFromBasket(\'' + plantid + '\'); return false; ">&nbsp;</a>' +
						'</td>' +														
					'</tr>' +
				'</tbody>';
	// return content
	return content;
}


/**
 * indicates the current maximum basket count allowed
 */
var MAXBASKETCOUNT = 250;	

/**
 * this method adds or removes a single instrument to/from the basket
 * 
 * @param {Object} anchor the anchor selected by user
 * @param {Boolean} addToBasket instrument selected to be added to basket?
 * @param {Integer} addAll boolean indicating if all results should be added to basket
 */
function toggleInstrumentInBasket(anchor, addToBasket, addAll)
{
	// check basket has not exceeded maximum basket count
	if (parseInt($j('span.basketCount:first').text()) < MAXBASKETCOUNT)
	{	
		// add to basket
		if (addToBasket)
		{
			// create new array
			var rowArray = [];
			// check if adding all?
			if (!addAll)
			{
				// add single table row
				rowArray.push($j(anchor).closest('tr'));
				// get single plant id value
				var plantid = $j(anchor).attr('id').substring(($j(anchor).attr('id').indexOf('instAdd') + 7), $j(anchor).attr('id').length);
			}
			else if ($j('table.companyInstResults tbody td.add a.addAnchor').length)
			{
				// loading image and message content
				var content = 	'<div class="text-center padding10">' +
									'<img src="img/icons/ajax-loader.gif" width="31" height="31" alt="" />' +
									'<br /> <h4>' + i18n.t("addingInstrToBasket", "Adding instruments to basket - please wait...") + '</h4>' +
								'</div>';
				// create overlay
				loadScript.createOverlay('dynamic', null, null, content, null, 14, 500, null);
				// find out how many items we can add to basket before maximum reached
				var basketItemsRemaining = MAXBASKETCOUNT - parseInt($j('span.basketCount:first').text());
				// build array of rows to be added from results
				$j('table.companyInstResults tbody td.add a.addAnchor').each(function(i, a)
				{
					// check we can still add to basket
					if (i < basketItemsRemaining)
					{
						// add all eligible rows to array
						rowArray.push($j(a).closest('tr'));
					}
				});
			}			
			// create new content variable
			var content = '';
			// get current basket count
			var instcount = parseInt($j('span.basketCount:first').text());
			// add each row
			$j.each(rowArray, function(i, row)
			{				
				// create basket content and check
				content += generateBasketContentForSelection(row, instcount, i);
				// add one to instrument count
				instcount++;
			});
			// the empty basket message is present so remove it
			if ($j('table.companyInstBasket tbody:first tr:first td').length == 1)
			{
				// remove message
				$j('table.companyInstBasket tbody:first').remove();
			}	
			// append new item row to basket
			$j('table.companyInstBasket').append(content);
			// add new inst count to current basket count in submenu
			$j('span.basketCount').text(parseInt(instcount));
			// anchor supplied for single basket addition?
			if (anchor != null)
			{
				// update icon in results
				$j(anchor).after('<a href="#" class="removeBasketAnchor" id="instAdd' + plantid + '" onclick=" toggleInstrumentInBasket(this, false, false); return false; " title="' + i18n.t("removeInstrFromBasket", "Remove instrument from basket") + '">&nbsp;</a>').remove();
			}
			else
			{
				// update anchors of all instruments added to basket
				$j('table.companyInstResults tbody td.add a.addAnchor').each(function(i, a)
				{
					// check we can still add to basket
					if (i < basketItemsRemaining)
					{
						// get the plant id value
						var aplantid = a.id.substring((a.id.indexOf('instAdd') + 7), a.id.length);
						// update icon in results
						$j(a).after('<a href="#" class="removeBasketAnchor" id="instAdd' + aplantid + '" onclick=" toggleInstrumentInBasket(this, false, false); return false; " title="' + i18n.t("removeInstrFromBasket", "Remove instrument from basket") + '">&nbsp;</a>').remove();
					}
				});
				// check basket has not exceeded maximum basket count
				if (parseInt($j('span.basketCount:first').text()) >= MAXBASKETCOUNT)
				{
					// warn user their basket is full
					$j.prompt(i18n.t("instrBasketHasReachedMaxCapacity", {varMax : MAXBASKETCOUNT, defaultValue : "Your basket has reached " + MAXBASKETCOUNT + " instruments which is currently the maximum allowed"}));
				}
				// close overlay
				loadScript.closeOverlay();
			}						
		}
		else
		{
			// remove item from basket
			deleteFromBasket(plantid);
		}
	}
	else
	{
		// add to basket
		if (addToBasket)
		{
			// prompt user that basket has reached maximum capacity
			$j.prompt(i18n.t("instrBasketHasReachedMaxCapacity", {varMax : MAXBASKETCOUNT, defaultValue : "Your basket has reached " + MAXBASKETCOUNT + " instruments which is currently the maximum allowed"}));
		}
		else
		{
			// remove item from basket
			deleteFromBasket(plantid);
		}
	}
}

/**
 * this method checks the basket content to make sure at least one instrument has been added before an action is performed
 * 
 * @param {Object} button element pressed to submit data
 * @param {String} submitType type of form submission used to populate hidden input for the correct functionality
 */
function checkBasketContents(button, submitType)
{
	// the empty basket message is present so remove it
	if ($j('table.companyInstBasket tbody:first tr:first td').length == 1)
	{
		$j.prompt(i18n.t("notAddedAnyInstrToBasket", "You have not added any instruments to your basket"));
	}
	else
	{
		// disable the button
		$j(button).attr('disabled', 'disabled');
		// populate submit type hidden input
		$j('#submitType').val(submitType);
		// submit the form
		$j(button).closest('form').find('input[type="submit"]').trigger('click');
	}
}

/**
 * this method deletes either one or all instruments from the basket
 * 
 * @param {Integer} plantid id of the instrument to be removed or null if all should be removed
 */
function deleteFromBasket(plantid)
{
	// plantid null?
	if (plantid != null)
	{
		// remove instrument from basket
		$j('table.companyInstBasket tbody#basketitem' + plantid).remove();
		// uncheck the instrument in search results
		$j('#instAdd' + plantid).after('<a href="#" class="addAnchor" id="instAdd' + plantid + '" onclick=" toggleInstrumentInBasket(this, true, false); return false; "' + i18n.t("addInstrToBasket", "Add instrument to basket") + '">&nbsp;</a>').remove();
		// subtract one from the current basket count in submenu
		$j('span.basketCount').text(parseInt($j('span.basketCount:first').text()) - 1);
	}
	else
	{
		// update anchors of all instruments removed from basket
		$j('table.companyInstResults tbody td.add a.removeBasketAnchor').each(function(i, a)
		{			
			// get the plant id value
			var aplantid = a.id.substring((a.id.indexOf('instAdd') + 7), a.id.length);
			// remove all instrument from basket
			$j('table.companyInstBasket tbody#basketitem' + aplantid).remove();
			// update icon in results
			$j(a).after('<a href="#" class="addAnchor" id="instAdd' + aplantid + '" onclick=" toggleInstrumentInBasket(this, true, false); return false; " title="' + i18n.t("addInstrToBasket", "Add instrument to basket") + '">&nbsp;</a>').remove();	
			// subtract one from the current basket count in submenu
			$j('span.basketCount').text(parseInt($j('span.basketCount:first').text()) - 1);
		});		
	}
	// the basket is empty?
	if ($j('table.companyInstBasket tbody:first tr').length < 1)
	{
		// add message
		$j('table.companyInstBasket').append(	'<tbody>' +
													'<tr>' +
														'<td colspan="10" class="center bold">' + i18n.t("instrBasketIsEmpty", "Your Instrument Basket Is Empty") + '</td>' +
														'</tr>' +
												'</tbody>');
	}
}