(function($j) {
  $j.templates(
    "duplicatesWarningContentTemplate",
    "#duplicatesWarningContentTemplate"
  );
  $j.views.converters("timestampConverter", function(timestamp) {
    const date = new Date(timestamp);
    const isValid = date && date.getTime && !isNaN(date.getTime());
    return isValid ? date.toLocaleDateString() : "";
  });
  $j.views.converters("scoreConverter", function(score) {
    const option = {
      style: "percent",
      minimumFractionDigits: 0,
      maximumFractionDigits: 0
    };
    const nf = new Intl.NumberFormat(i18locale || "en-US", option);
    return nf.format(score);
  });
  $j.fn.CreateNewInstrument = function(options) {
    return this.each(function() {
      new CreateInstrumentJqueryPlugin(this, options);
    });
  };

  class CreateInstrumentJqueryPlugin {
    constructor(element, options) {

      //extremously dirty way of checking if CascadeSearchPlugin has change coid
      this.coidObserver  = new MutationObserver(this.coidChanged.bind(this));
      this.coidObserver.observe(document.getElementById("coid"),{attributeFilter:["value"],attributeOldValue:true});

      this.element = element;
      this.options = Object.assign({currentPlantId: 0},options);
      $j("input[name='plantNo'], input[name='serialNo']", element).on(
        "keyup",
        debounce(
        e => {
          this.checkForDuplicates(element);
        },500)
      );
      $j(this.element).on(
        "click",
        "button.acceptInstrument",
        this.acceptRow.bind(this)
      );
    }

    coidChanged(records){
      const oldCoid = records[0].target.oldValue;
      const coid = records[0].target.value;
      if(oldCoid!=coid)
        this.updateCompayOption(coid);
    }

    updateCompayOption(coid){
      const url = window.cwms.urlWithParamsGenerator("settings/disallowSamePlantNumberForCompany.json",{coid});
      fetch(url).then(res => res.ok?res.json():Promise.reject(res.statusText))
      .then(result => this.options.disallowSamePlantNumber = result);

    }

    acceptRow(e) {
      e.preventDefault();
      const element = e.currentTarget;
      const plantId = element.dataset.plantid;
      window.location.search = `plantid=${plantId}`;
    }

    checkForDuplicates(element) {
      const instrument = this.collectInstrumentProperty(element);
      this.queryServerForDuplicatedInstrument(instrument);
    }

    collectInstrumentProperty(element) {
      const coid = element.querySelector('input[name="coid"]').value;
      const plantNo = element.querySelector('input[name="plantNo"]').value.trim().toUpperCase();
      const serialNo = element.querySelector('input[name="serialNo"]').value.trim();
      const modelId = element.querySelector('input[name="modelid"]').value;
      return new SimpleInstrumentDto(coid, plantNo, serialNo, modelId);
    }

    queryServerForDuplicatedInstrument(instrument) {
      const contextPath = document
      .querySelector("meta[name='_contextPath']")?.getAttribute("content") ?? "" ;
      const base = `${location.origin}${contextPath ?? ""}/`
      const url = new URL("instrument/checkDuplicatesForInstrument.json",base);
      Object.entries(instrument).map( ([k,v]) => {
        url.searchParams.append(k,v);
      });
      fetch(url, {
        method: "GET",
        headers: { "Content-Type": "application/json" },
      })
        .then(r => r.json())
        .then(r =>r.isValid ? r.result : Promise.reject("no instruments was found"))
        .then(r => ({...r,companyInstruments:this.filterOutCurrentInstrument(this.options.currentPlantId)(r.companyInstruments)}))
        .then(this.checkForPerfectMatch(instrument))
        .then(r => {
          this.removeSuggestions();
          const warningMessage =this.options.currentPlantId == 0? i18n.t("core.jobs:jobItem.warnDuplicateInstrFound", {"groupName":r.groupName},
                                								"Some instruments you are trying to add have potential duplicates, please review before adding!"):i18n.t("core.instrument:editInstr.duplicateInstr");
          const w = $j(
            $j.render.duplicatesWarningContentTemplate({ i18n, result: r, warningMessage })
          );
          w.addClass("cwms-instrument-plugin");
          $j(".errorContainer", this.element).append(w);
          this.showSuggestions();
          JT_init();
        })
        .catch(() => {
          this.removeSuggestions();
        });
    }

    filterOutCurrentInstrument(currentPlantId){
        return  companyInstruments => companyInstruments
        .filter(i => i.plantId != currentPlantId);
    }

    showSuggestions() {
      this.element.querySelector(".errorContainer").classList.remove("hid");
    }
    removeSuggestions() {
      $j(".errorContainer .cwms-instrument-plugin", $j(this.element)).remove();
      this.element.querySelector(".errorContainer").classList.add("hid");
    }

    checkForPerfectMatch(instrument) {
      return result => {
        const perfect = result.companyInstruments
        .find(
          i =>
            i.coid == instrument.coid &&
            i.serialNo == instrument.serialNo &&
            i.plantNo == instrument.plantNo &&
            i.modelId == instrument.modelId
        );
        if (perfect) {
          const message = i18n.t(
            "core.jobs:jobItem.instrumentExistsError",
            "Instrument already exists!!!"
          );
          if(this.options.disallowSamePlantNumber){
              this.element
            .querySelector("input[name='plantNo']")
            .setCustomValidity(message);
          this.element
            .querySelector("input[name='serialNo']")
            .setCustomValidity(message);
          }
          this.addErrorMessage(
            message,
            this.element.querySelector(".errorContainer")
          );
          return Promise.resolve(result);
        } else {
          this.element.classList.remove("warningBox3");
          this.element.querySelectorAll("span.error").forEach(e => e.remove());
              this.element
                .querySelector("input[name='plantNo']")
                .setCustomValidity("");
              this.element
                .querySelector("input[name='serialNo']")
                .setCustomValidity("");
          return Promise.resolve(result);
        }
      };
    }
    addErrorMessage(message, element) {
      element.classList.add("warningBox3");
      const span = document.createElement("span");
      span.classList.add("error");
      span.innerText = message;
      element.prepend(span);
    }
  }

  class SimpleInstrumentDto {
    constructor(coid, plantNo, serialNo, modelId) {
      this.coid = coid;
      this.plantNo = plantNo;
      this.serialNo = serialNo;
      this.modelId = modelId;
      this.mfrReqForModel = 0;
      this.searchModelMatches = true;
    }
  }
})(jQuery);
