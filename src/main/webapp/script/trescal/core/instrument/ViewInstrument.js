/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	viewInstrument.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	05/11/2007
*					:	25/09/2015 - TProvost - Manage i18n
*					:	10/11/2015 - TProvost - Fix bug on enable buttons
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array ( 	'script/thirdparty/jQuery/jquery.boxy.js',
							  	'script/trescal/core/instrument/ScrapInstrumentUtility.js',
							  	'script/trescal/core/tools/FileBrowserPlugin.js',
							  	'script/thirdparty/jQuery/jquery.dataTables.min.js');

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{

    /**
     *              Recall Rule Functionality
     */
    //dialog box initialization
    $j("#dialog").dialog({
        autoOpen: false,
        width: 700,
        open: function (event, ui) {
            //do nothing
        }
    });

    $j("#calExtensionDialog").dialog({
        autoOpen: false,
        width: 500,
        open: function (event, ui) {
            //do nothing
        }
    });



	$j( "button.newRecallRule" ).click(function () {

        $j("#dialog").dialog("open");

	});

	// Behaviours for add recall rule dialog

    // toggle next cal date input box when backdate to last cal tickbox is clicked
    $j('#applyfromcert').click(function() {
        $j('div#dialog #recallCalibrateDate').toggle();
        $j('div#dialog #recallCalibrateCert').toggle();
    });

    //hide 'backdate to last cal' selector and 'update instrument interval' tick box
    // if recall requirement other than FULL_CALIBRATION is selected
    $j('#requirementTypeId').on('change', function() {
        if(this.value == 'FULL_CALIBRATION'){
            if($j('#hascert').val() == 'true'){
                $j('div#dialog #applyfromcert').attr('checked', true);
                $j('div#dialog #linkToCert').show();
                $j('div#dialog #recallCalibrateCert').show();
                $j('div#dialog #calibrateOn').val('');
                $j('div#dialog #recallCalibrateDate').hide();
            }else{
                $j('div#dialog #applyfromcert').attr('checked', false);
                $j('div#dialog #linkToCert').hide();
                $j('div#dialog #recallCalibrateCert').hide();
                $j('div#dialog #recallCalibrateDate').show();
            }
            $j('div#dialog #updateInstrument').attr('checked',true);
            $j('div#dialog #updateInstrumentDiv').show();
        } else {
            $j('div#dialog #applyfromcert').attr('checked', false);
            $j('div#dialog #linkToCert').hide();
            $j('div#dialog #recallCalibrateCert').hide();
            $j('div#dialog #recallCalibrateDate').show();
            $j('div#dialog #updateInstrument').attr('checked',false);
            $j('div#dialog #updateInstrumentDiv').hide();
        }
        if(this.value == 'SERVICE_TYPE') {
        	$j('div#dialog #serviceTypeSelector').show();
        } else {
        	$j('div#dialog #serviceTypeSelector').hide();
        }
    });

    // create a calendar for the recall date input field
//    loadScript.createJQCal( {
//        inputField: 'calibrateOn',
//        dateFormat: 'dd.mm.yy',
//        displayDates: 'all',
//        showWeekends: true
//    });

    //Prevent non numeric input into cal period
    $j("#interval").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($j.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
            // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    // submit the form to create new recall rule when button pressed
    $j('#createRecallRuleBtn').click( function() {
        insertRecallRule($j('#plantid').val() ,
            $j('#interval').val(),
            $j('#calibrateOn').val(),
            $j('#applyfromcert').is(':checked'),
            $j('#hascert').val(),
            $j('#requirementTypeId').val(),
            $j('#intervalUnitId').val(),
            $j('#updateInstrument').is(':checked'),
            $j('#serviceTypeId').val());
    });

	
	$j( 'table.instrumentActiveRecallRules' ).on( 'click', 'a.deactivate', function(event) {
        event.preventDefault();
        removeRecallRule(
            $j(this).siblings('.activeRuleId').val(),
            $j('#plantid').val(),
            false,
            $j('#hascert').val());
    });

	$j( 'table.instrumentActiveRecallRules' ).on( 'click', 'a.deactivateandreset', function(event) {
        event.preventDefault();
        removeRecallRule(
            $j(this).siblings('.activeRuleId').val(),
            $j('#plantid').val(),
            true,
            $j('#hascert').val());
    });

	$j( 'table.instrumentInactiveRecallRules' ).on( 'click', 'a.reactivate', function(event){
        event.preventDefault();
        reactivateRecallRule(
            $j(this).siblings('.inactiveRuleId').val(),
            $j('#plantid').val(),
            false,
            $j('#hascert').val())
    });

	$j( 'table.instrumentInactiveRecallRules' ).on( 'click', 'a.reactivateandreset', function(event){
        event.preventDefault();
        reactivateRecallRule(
            $j(this).siblings('.inactiveRuleId').val(),
            $j('#plantid').val(),
            false,
            $j('#hascert').val())
    });

    /**
     *
     * Cal Extension functionality
     * Only if #calExtensionDiv exists. It won't exist if the customer doesn't have the cal extension setting
     *
     */
    if( $j( '#calExtensionDiv').length ) {

        initializeCalExtensionDetails(plantid);

        $j( '#addOrEditCalExtensionButton' ).click( function() {
            $j("#calExtensionDialog").dialog("open");
        });

        $j('#calextensiontype').on('change', function() {
            if ( this.value == "PERCENTAGE"){
                $j('#extensiontimeunits').hide();
                $j('#percentagesymbol').show();
            } else {
                $j('#extensiontimeunits').show();
                $j('#percentagesymbol').hide();
            }
        });

        $j( '#addoreditcalextension' ).click( function() {
            if(!$j('#extensionvalue').val()) {
                $j.prompt("Please enter a value");
            } else {
                addOrEditCalExtension(plantid,
                    $j('#calextensiontype').val(),
                    $j('#extensionvalue').val(),
                    $j('#extensiontimeunits').val());
            }
        });

        $j( '#deletecalextension' ).click( function() {
            deleteCalExtension(plantid);
        });

        //make sure value is an integer
        $j("#extensionvalue").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter
            if ($j.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
                // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: Ctrl+C
                (e.keyCode == 67 && e.ctrlKey === true) ||
                // Allow: Ctrl+X
                (e.keyCode == 88 && e.ctrlKey === true) ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }


        });



    }
    
    var table = $j('#checkOutInstruments').DataTable({
		"pageLength" : 25,
	});

}
							
/**
 * Array of objects containing the element name and corner type which should be applied.
 * Applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' },
								{ element: 'ul.subnavtab a', corner: 'small transparent top' } );

/**
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = 	new Array(	{ anchor: 'jobs-link', block: 'jobs-tab' },
								{ anchor: 'certs-link', block: 'certs-tab' },
								{ anchor: 'calhistory-link', block: 'calhistory-tab' },
								{ anchor: 'files-link', block: 'files-tab' },
								{ anchor: 'recall-link', block: 'recall-tab' },
								{ anchor: 'validation-link', block: 'validation-tab' },
								{ anchor: 'rep-link', block: 'rep-tab' }, 
								{ anchor: 'images-link', block: 'images-tab' },
								{ anchor: 'standards-link', block: 'standards-tab' },
								{ anchor: 'history-link', block: 'history-tab' },
								{ anchor: 'quotes-link', block: 'quotes-tab' },
								{ anchor: 'flexiblefields-link', block: 'flexiblefields-tab' },
								{ anchor: 'complementary-link', block: 'complementary-tab' },
								{ anchor: 'measurementpossibilities-link', block: 'measurementpossibilities-tab' },
								{ anchor: 'Checkinout-link', block: 'Checkinout-tab' });

/**
 * this method creates a recall rule for this instrument and if applyfromcert is true then
 * updates the next calibration due date.
 * 
 * @param {Integer} plantid the id of the instrument
 * @param {Integer} interval the new recall interval in months
 * @param {Date} calibrateOn the date on which calibration should take place
 * @param {Boolean} applyfromcert if true then updates the recall date of the instrument from last certificate
 * @param {Boolean} hasCert indicates if the instrument has a previous certificate
 * @param {Integer} requirementId the ordinal value for the chosen RecallRequirementType
 * @param {Integer} unitId the ordinal value for the chosen IntervalUnit
 */
function insertRecallRule(plantid, interval, calibrateOn, applyfromcert, hasCert, requirementId, unitId, updateInstrument,servicetypeid)
{
	//set up post data
	var data = {};
	data["plantId"] = plantid;
	data["interval"] = interval;
    data["calibrateOn"] = calibrateOn;
    data["recallFromCert"] = applyfromcert;
    data["unit"] = unitId;
    data["requirement"] = requirementId;
    data["updateInstrument"] = updateInstrument;
    data["serviceTypeId"] = servicetypeid;

    //send ajax request
    var request = $j.ajax({
        	type: "POST",
        	contentType: "application/json",
        	url: "addrecallrule.json",
        	data: JSON.stringify(data),
        	dataType: 'json'});
    
    //deal with response
    request.done( function (dto) {
    	// insertion of recall rule failed
		if(dto.success != true)
		{
			// show error message in error warning box
			$j('div#addRecallRuleBoxy div:first').removeClass('hid').addClass('vis').find('.warningBox3').empty().html(dto.errorMessage);
		} else {
			//Create content for new row in active rule table
			var activeContent = 	'<tr>' +
								'<td class="interval"><span>' + dto.interval + '</span> ' + dto.units + ' ' + '</td>' +
								'<td>' + dto.ruleNextDate + '</td>' +
								'<td>' + dto.requirement + '</td>' +
								'<td>' + i18n.t("core.instrument:viewInstr.coverThisInstrument", "Covers just this instrument") + '</td>' +
								'<td>' + dto.setBy + ' (' + dto.setOn + ')</td>' +
								'<td>' +															
									'<input type="hidden" name="activeRuleId" class="activeRuleId" value="' + dto.recallRuleId + '" />' +
										'<a href="#" id="deactivate" class="mainlink deactivate" title="' + i18n.t("core.instrument:viewInstr.removeRuleAndResetDateInfo", "Deactivates this recall rule but leaves the current recall date intact") + '">' + i18n.t("core.instrument:viewInstr.removeRule", "Remove rule") + '</a>';
										if(hasCert == 'true' && dto.requirementCode == 'FULL_CALIBRATION')
										{
											activeContent += ' | ' +
											'<a href="#" id="deactivateandreset" class="mainlink deactivateandreset" title="' + i18n.t("core.instrument:viewInstr.removeRuleAndResetDate", "Deactivates this recall rule and resets the recall date using the next applicable active recall rule") + '">' + i18n.t("core.instrument:viewInstr.removeRuleAndResetDate", "Remove rule and reset cal date") + '</a>';
										}																									
								activeContent +=  	'</td>' + '</tr>';
			
			
			
			// Cache selector for table row containing active rule with same requirement type.
			var existingActiveRuleRow = $j("table.instrumentActiveRecallRules td")
			.filter(function() { return $j(this).text() == dto.requirement; })
			.closest("tr");
			
			//Is there an existing active rule for same requirement type?
			if (existingActiveRuleRow.length > 0) 
			{
				// inactive recall rules table is empty?
				if ($j('table.instrumentInactiveRecallRules tbody tr:first td').length < 2)
				{
					// remove the inactive table message
					$j('table.instrumentInactiveRecallRules tbody tr:first').remove();
				}					
			
				// create content for new row in inactive recall rules table using some values from active recall rules table
				// and some values from the results of newly inserted recall rule
				var inactiveContent = 	'<tr id="inactive' + existingActiveRuleRow.find('td:last input').val() + '">' +
											'<td class="interval">' + existingActiveRuleRow.find('td:eq(0)').text() + '</td>' +
											'<td>' + existingActiveRuleRow.find('td:eq(1)').text() + '</td>' +
											'<td>' + existingActiveRuleRow.find('td:eq(2)').text() + '</td>' +
											'<td>' + existingActiveRuleRow.find('td:eq(3)').text() + '</td>' +
											'<td>' + dto.setBy + '(' + dto.setOn +  ')</td>' +
										'</tr>';
											
				// append new inactive recall rule to end of table
				$j('table.instrumentInactiveRecallRules tbody').append(inactiveContent);
				
				// update inactive recall count
				$j('span#inactiveRecallCount').text(parseInt($j('span#inactiveRecallCount').text()) + 1);
				
				// remove the old active recall rule
				existingActiveRuleRow.remove();
			}
			
			// append new active recall rule to table
			$j('table.instrumentActiveRecallRules tbody tr:first').before(activeContent);
			
			// update active recall count
			$j('span#activeRecallCount').text(parseInt($j('span#activeRecallCount').text()) + 1);

			//update dates
            var spanStyle = '';
            // instrument out of cal
            switch(dto.requirementCode){
                case 'FULL_CALIBRATION':
                    if(dto.instOutOfCal)
                    {
                        spanStyle = ' class="attention" ';
                    }
                    // add the new calibration due date for instrument to page
                    $j('span.nextCalDueDate').html('<span ' + spanStyle + '>' + (dto.instNextCalDate != null ? dto.instNextCalDate : ' ' ) + '</span>');
                    // add the new calibration recall rule interval to page
                    $j('span.recallRuleInterval').text(dto.interval);
                    // add correct recall rule interval unit
                    $j('span.recallRuleIntervalUnit').text(dto.units);
                    // add the new recall date to page using method @see getRecallPeriod() and value of next calibration due date
                    $j('span#nextCalRecall').text(getRecallPeriod(dto.instNextCalDateInms));

                    // variable for cal timescale icon content
                    var calTimescalePercentage = '';
                    if ((dto.inCalTimescalePercentage == 100) && (dto.finalMonthOfCalTimescalePercentage == 100)) {
                        calTimescalePercentage += '<div class="outOfCalibration"></div>';
                    }
                    else {
                        calTimescalePercentage += '<div class="inCalibration">' +
                            '<div class="inCalibrationLevel" style=" width: ' + dto.inCalTimescalePercentage + '%; "></div>' +
                            '</div>' +
                            '<div class="finalMonthOfCalibration">' +
                            '<div class="finalMonthOfCalibrationLevel" style=" width: ' + dto.finalMonthOfCalTimescalePercentage + '%; "></div>' +
                            '</div>';
                    }
                    // populate calibration timescale percentage icon content
                    $j('div.calIndicatorBox').html(calTimescalePercentage)
                    if(updateInstrument){
                        $j('span.instCalFrequency').text(dto.interval);
                        $j('span.instCalFrequencyUnits').text(dto.units);
                    }
                    break;
                case 'INTERIM_CALIBRATION':
                    // add the new interim calibration due date for instrument to page
                    if(dto.interimCalOverdue)
                    {
                        spanStyle = ' class="attention" ';
                    }
                    $j('span.interimCalDate').html('<span ' + spanStyle + '>' + dto.instNextInterimCalDate + '</span>');
                    // add the new calibration recall rule interval to page
                    $j('span.interimCalInterval').text(dto.interval);
                    // add correct recall rule interval unit
                    $j('span.interimCalUnit').text(dto.units);
                    break;
                case 'MAINTENANCE':
                    // add the new calibration due date for instrument to page
                    if(dto.maintenanceOverdue)
                    {
                        spanStyle = ' class="attention" ';
                    }
                    $j('span.maintenanceDate').html('<span ' + spanStyle + '>' + dto.instNextMaintenanceDate + '</span>');
                    // add the new calibration recall rule interval to page
                    $j('span.maintenanceInterval').text(dto.interval);
                    // add correct recall rule interval unit
                    $j('span.maintenanceUnit').text(dto.units);
                    break;
            }

            if(dto.requirement='FULL_CALIBRATION') {

            }
			// close the jquery dialog
            $j("#dialog").dialog("close");
            //update cal extension
            if( $j( '#calExtensionDiv').length ) {
                initializeCalExtensionDetails(plantid);
            }
		}
	  });
    
    //Ajax request failed
    request.fail(function( jqXHR, textStatus ) {
    	  alert( "Request failed: " + textStatus );
    	});
}

/**
 * this method deactivates the rule identified by the given id and if resetNextCal is true then it recalculates the
 * recall due date.
 *
 * @param {Integer} ruleid the id of the rule to deactivate
 * @param {Integer} plantid the barcode of the current instrument
 * @param {Boolean} resetNextCal true will update the next cal due date of the instrument
 * @param {Boolean} hasCert indicates if the instrument has a previous certificate
 */
function removeRecallRule(ruleid, plantid, resetNextCal, hasCert)
{
    //set up post data
    var data = {};
    data["plantId"] = plantid;
    data["recallRuleId"] = ruleid;
    data["updateRecallDate"] = resetNextCal;

    //send ajax request
    var request = $j.ajax({
        type: "POST",
        contentType: "application/json",
        url: "deactivaterecallrule.json",
        data: JSON.stringify(data),
        dataType: 'json'});

    //deal with response
    request.done( function (dto) {
        if(!dto.success)
        {
            // show error message
            $j.prompt(dto.errorMessage);
        }
        else
        {
            // Cache the selector for the table row containing active rule to be deactivated.
            var rowToDeactivate = $j("table.instrumentActiveRecallRules td input")
                .filter(function() { return $j(this).val() == ruleid; })
                .closest("tr");


            //create content for row in inactive rules table
            var inactiveContent = 	'<tr id="inactive' + ruleid + '">' +
                '<td class="interval">' + rowToDeactivate.find('td:eq(0)').text() + '</td>' +
                '<td>' + rowToDeactivate.find('td:eq(1)').text() + '</td>' +
                '<td>' + rowToDeactivate.find('td:eq(2)').text() + '</td>' +
                '<td>' + rowToDeactivate.find('td:eq(3)').text() + '</td>' +
                '<td>' + dto.deactivatedBy + '(' + cwms.dateFormatFunction(dto.deactivatedOn) +  ')</td>' +
            '</tr>';

            // inactive recall rules table is empty?
            if ($j('table.instrumentInactiveRecallRules tbody tr:first td').length < 2)
            {
                // remove the inactive table message
                $j('table.instrumentInactiveRecallRules tbody tr:first').remove();
            }
            // append new inactive recall rule to end of table
            $j('table.instrumentInactiveRecallRules tbody').append(inactiveContent);
            // remove the old recall rule
            rowToDeactivate.remove();
            // update inactive recall count
            $j('span#inactiveRecallCount').text(parseInt($j('span#inactiveRecallCount').text()) + 1);
            // update active recall count
            $j('span#activeRecallCount').text(parseInt($j('span#activeRecallCount').text()) - 1);


            // calibration due date reset?
            if (resetNextCal == true && dto.requirementCode == 'FULL_CALIBRATION')
            {
                // variable for next cal due date span
                var spanStyle = '';
                // instrument out of cal
                if(dto.instOutOfCal)
                {
                    spanStyle = ' class="attention" ';
                }
                // add the new calibration due date for instrument to page
                $j('span.nextCalDueDate').html('<span ' + spanStyle + '>' + cwms.dateFormatFunction(dto.instNextCalDate) + '</span>');
                // add the new calibration recall rule interval to page
                $j('span.recallRuleInterval').text(dto.interval);
                // add correct recall rule interval unit
                $j('span.recallRuleIntervalUnit').text(dto.Units);
                // add the new recall date to page using method @see getRecallPeriod() and value of next calibration due date
                $j('span#nextCalRecall').text(getRecallPeriod(dto.instNextCalDateInms));
                // add the new recall date to page using method @see getRecallPeriod() and value of next calibration due date
                $j('span#nextCalRecall').text(getRecallPeriod(dto.instNextCalDate));
                // variable for cal timescale icon content
                var calTimescalePercentage = '';
                if ((instrument.inCalTimescalePercentage == 100) && (instrument.finalMonthOfCalTimescalePercentage == 100))
                {
                    calTimescalePercentage += '<div class="outOfCalibration"></div>';
                }
                else
                {
                    calTimescalePercentage += 	'<div class="inCalibration">' +
                        '<div class="inCalibrationLevel" style=" width: ' + dto.inCalTimescalePercentage + '%; "></div>' +
                        '</div>' +
                        '<div class="finalMonthOfCalibration">' +
                        '<div class="finalMonthOfCalibrationLevel" style=" width: ' + dto.finalMonthOfCalTimescalePercentage + '%; "></div>' +
                        '</div>';
                }
                // populate calibration timescale percentage icon content
                $j('div.calIndicatorBox').html(calTimescalePercentage);
            }
            //remove interval if rule being deactivated is maintenance or interim_cal
            if(dto.requirementCode == 'MAINTENANCE'){
                $j('span.maintenanceInterval').text(i18n.t("core.instrument:viewInstr.norecallrule", "No Recall Rule"));
                $j('span.maintenanceUnit').text("");
            }
            if(dto.requirementCode == 'INTERIM_CALIBRATION'){
                $j('span.interimCalInterval').text(i18n.t("core.instrument:viewInstr.norecallrule", "No Recall Rule"));
                $j('span.interimCalUnit').text("");
            }

        }
    });

    //Ajax request failed
    request.fail(function( jqXHR, textStatus ) {
        alert( "Request failed: " + textStatus );
    });

}

/**
 * this method reactivates the rule identified by the given id and if resetNextCal is true then  it
 * recalculates the recall due date based on the reactivated rule and the last certificate cal date.
 *
 * @param {Integer} ruleid the id of the rule to deactivate
 * @param {Integer} plantid the barcode of the current instrument
 * @param {Boolean} resetNextCal true will update the next cal due date of the instrument
 * @param {Boolean} hasCert indicates if the instrument has a previous certificate
 */
function reactivateRecallRule(ruleid, plantid, resetNextCal, hasCert)
{
    //set up post data
    var data = {};
    data["plantId"] = plantid;
    data["recallRuleId"] = ruleid;
    data["updateRecallDate"] = resetNextCal;

    //send ajax request
    var request = $j.ajax({
        type: "POST",
        contentType: "application/json",
        url: "reactivaterecallrule.json",
        data: JSON.stringify(data),
        dataType: 'json'});

    //deal with response
    request.done( function (dto) {
        if(!dto.success)
        {
            // show error message
            $j.prompt(dto.errorMessage);
        }
        else
        {
            // Cache the selector for the table row containing inactive rule to be activated.
            var rowToActivate = $j("table.instrumentInactiveRecallRules td input")
                .filter(function() { return $j(this).val() == ruleid; })
                .closest("tr");
            // Cache selector for table row containing active rule with same requirement type.
            var existingActiveRuleRow = $j("table.instrumentActiveRecallRules td")
                .filter(function() { return $j(this).text() == dto.requirement; })
                .closest("tr");

            //Is there an existing active rule for same requirement type?
            if (existingActiveRuleRow.length > 0)
            {
                // create content for new row in inactive recall rules table using some values from active recall rules table
                // and some values from the results of newly activated recall rule
                var inactiveContent = 	'<tr id="inactive' + existingActiveRuleRow.find('td:last input').val() + '">' +
                    '<td class="interval">' + existingActiveRuleRow.find('td:eq(0)').text() + '</td>' +
                    '<td>' + existingActiveRuleRow.find('td:eq(1)').text() + '</td>' +
                    '<td>' + existingActiveRuleRow.find('td:eq(2)').text() + '</td>' +
                    '<td>' + dto.deactivatedBy + '</td>' +
                    '<td>' + dto.deactivatedOn + '</td>' +
                    '<td>' +
                    '<input type="hidden" name="inactiveRuleId" class="inactiveRuleId" value="' + existingActiveRuleRow.find('input.activeRuleId')  + '" />' +
                    '<a href="#" class="mainlink reactivate" title="' + i18n.t("core.instrument:viewInstr.reactivateRuleInfo", "Reactivates this recall rule but leaves the current recalld date intact") + '">' + i18n.t("core.instrument:viewInstr.reactivateRule", "Reactivate Rule") + '</a>';
                if(hasCert == 'true' && dto.requirementCode == 'FULL_CALIBRATION')
                {
                    inactiveContent = inactiveContent + ' | ' +
                        '<a href="#" class="mainlink reactivateandreset" title="' + i18n.t("core.instrument:viewInstr.reactivateRuleAndRecalculDateInfo", "Reactivates this recall rule and resets the recall date using the next applicable active recall rule") + '">' + i18n.t("core.instrument:viewInstr.reactivateRuleAndRecalculDate", "Reactivate rule and recalculate cal date") + '</a>';
                }
                '</td>' +
                '</tr>';

                // append new inactive recall rule to end of table
                $j('table.instrumentInactiveRecallRules tbody').append(inactiveContent);

                // update inactive recall count
                $j('span#inactiveRecallCount').text(parseInt($j('span#inactiveRecallCount').text()) + 1);

                // update active recall count
                $j('span#activeRecallCount').text(parseInt($j('span#activeRecallCount').text()) - 1);

                // remove the old active recall rule
                existingActiveRuleRow.remove();
            }

            // create content for new row in the active recall rules table
            var activeContent = 	'<tr>' +
                '<td class="interval"><span>' + dto.interval + '</span> ' + dto.units + ' ' + '</td>' +
                '<td>' + dto.requirement + '</td>' +
                '<td>' + i18n.t("core.instrument:viewInstr.coverThisInstrument", "Covers just this instrument") + '</td>' +
                '<td>' + dto.setBy + '</td>' +
                '<td>' + dto.setOn + '</td>' +
                '<td>' +
                '<input type="hidden" name="activeRuleId" class="activeRuleId" value="' + dto.recallRuleId + '" />' +
                '<a href="#" id="deactivate" class="mainlink deactivate" title="' + i18n.t("core.instrument:viewInstr.removeRuleAndResetDateInfo", "Deactivates this recall rule but leaves the current recall date intact") + '">' + i18n.t("core.instrument:viewInstr.removeRule", "Remove rule") + '</a>';
            if(hasCert == 'true' && dto.requirementCode == 'FULL_CALIBRATION')
            {
                activeContent += ' | ' +
                    '<a href="#" id="deactivateandreset" class="mainlink deactivateandreset" title="' + i18n.t("core.instrument:viewInstr.removeRuleAndResetDate", "Deactivates this recall rule and resets the recall date using the next applicable active recall rule") + '">' + i18n.t("core.instrument:viewInstr.removeRuleAndResetDate", "Remove rule and reset cal date") + '</a>';
            }
            activeContent +=  	'</td>' + '</tr>';
            // append new active recall rule to table
            $j('table.instrumentActiveRecallRules tbody tr:first').before(activeContent);
            // update active recall count
            $j('span#activeRecallCount').text(parseInt($j('span#activeRecallCount').text()) + 1);

            // remove row from deactivated rules table
            rowToActivate.remove();
            $j('span#inactiveRecallCount').text(parseInt($j('span#inactiveRecallCount').text()) - 1);

            // inactive recall rules table is empty?
            if (parseInt($j('span#inactiveRecallCount').text()) < 1)
            {
                // add inactive table message
                $j('table.instrumentInactiveRecallRules tbody').append(	'<tr>' +
                    '<td colspan="6" class="bold center">' + i18n.t("core.instrument:viewInstr.noInactiveRecallRules", "There are no inactive recall rules for this instrument") + '</td>' +
                    '</tr>');
            }

            switch(dto.requirementCode){
                case 'FULL_CALIBRATION':
                    // add the new calibration recall rule interval to page
                $j('span.recallRuleInterval').text(dto.interval);

                // calibration due date reset?
                if (resetNextCal == true)
                {
                    // variable for next cal due date span
                    var spanStyle = '';
                    // instrument out of cal
                    if(dto.instOutOfCal)
                    {
                        spanStyle = ' class="attention" ';
                    }
                    // add the new calibration due date for instrument to page
                    $j('span.nextCalDueDate').html('<span ' + spanStyle + '>' + dto.instNextCalDate + '</span>');
                    // add the new recall date to page using method @see getRecallPeriod() and value of next calibration due date
                    $j('span#nextCalRecall').text(getRecallPeriod(dto.instNextCalDateInms));
                }
                // variable for cal timescale icon content
                var calTimescalePercentage = '';
                if ((dto.inCalTimescalePercentage == 100) && (dto.finalMonthOfCalTimescalePercentage == 100))
                {
                    calTimescalePercentage += '<div class="outOfCalibration"></div>';
                }
                else
                {
                    calTimescalePercentage += 	'<div class="inCalibration">' +
                        '<div class="inCalibrationLevel" style=" width: ' + dto.inCalTimescalePercentage + '%; "></div>' +
                        '</div>' +
                        '<div class="finalMonthOfCalibration">' +
                        '<div class="finalMonthOfCalibrationLevel" style=" width: ' + dto.finalMonthOfCalTimescalePercentage + '%; "></div>' +
                        '</div>';
                }
                // populate calibration timescale percentage icon content
                $j('div.calIndicatorBox').html(calTimescalePercentage);
                break;
            case 'INTERIM_CALIBRATION':
                // add the new interim calibration due date for instrument to page
                if(dto.interimCalOverdue)
                {
                    spanStyle = ' class="attention" ';
                }
                $j('span.interimCalDate').html('<span ' + spanStyle + '>' + dto.instNextInterimCalDate + '</span>');
                // add the new calibration recall rule interval to page
                $j('span.interimCalInterval').text(dto.interval);
                // add correct recall rule interval unit
                $j('span.interimCalUnit').text(dto.units);
                break;
            case 'MAINTENANCE':
                // add the new calibration due date for instrument to page
                if(dto.maintenanceOverdue)
                {
                    spanStyle = ' class="attention" ';
                }
                $j('span.maintenanceDate').html('<span ' + spanStyle + '>' + dto.instNextMaintenanceDate + '</span>');
                // add the new calibration recall rule interval to page
                $j('span.maintenanceInterval').text(dto.interval);
                // add correct recall rule interval unit
                $j('span.maintenanceUnit').text(dto.units);
                break;
            }
        }
    });

    //Ajax request failed
    request.fail(function( jqXHR, textStatus ) {
        alert( "Request failed: " + textStatus );
    });
}

/**
 * This function checks if the instrument has a cal extension and displays it with an edit button if it has
 * If the instrument doesn't currently have a cal extension then an add button is displayed instead.
 * These things are added inside the #calExtensionDiv
 *
 * @param plantid
 */
function initializeCalExtensionDetails(plantid){
    var content = '';
    var formatedExtensionEndDate = '';
    //get cal extension for instrument if any
    fetch(window.cwms.urlWithParamsGenerator("calextension/get.json",{plantid}))
	.then(res => res.ok?res.json():Promise.reject(res.statusText))
	.then(dto =>  {
        if (dto.success == true) {
            //Add info for extension
            dateColor = (dto.instrumentInExtension ? "Green" : (dto.instrumentPostExtension ? "Red" : "Grey"));
            content = i18n.t("core.instrument:viewInstr.calextended","This instrument has a calibration extension of ");
            content = content + dto.value;
            if (dto.type == 'TIME') {
                content = content + " " + dto.timeUnitText;
            }
            if (dto.type == 'PERCENTAGE') {
                content = content + " %";
            }

            //Update display of extension
            $j('#calExtensionDetails').text(content);
            $j('#extendedDate').text(cwms.dateFormatFunction(dto.extensionEndDate));
            $j('#extendedDate').css("color",dateColor);
            $j('#extendedDate').css("font-weight", !dto.instrumentPreExtension ? "bold" : "normal");
            $j('#extendedDate').css("font-style", dto.instrumentPreExtension ? "italic" : "normal");
            $j('#extendedDateMessage').show();
            $j('#addOrEditCalExtensionButton').text(i18n.t("core.instrument:viewInstr.editcalextension", "Edit Calibration Extension"));
            $j('#addOrEditCalExtensionButton').show();

            //Update extension dialog ready for user to edit the extension.
            $j('#calextensiontype').val(dto.type);
            $j('#extensionvalue').val(dto.value);
            if ( dto.type == 'TIME' ) {
                $j('extensiontimeunits').val(dto.timeUnit);
            }
            $j('#addoreditcalextension').text(i18n.t("edit", "Edit"));
            $j('#deletecalextension').show();

        } else {
            //Add message and button to add if extension doesn't exist
            content = i18n.t("core.instrument:viewInstr.nocalextension", "This instrument does not have a calibration extension");
            $j('#calExtensionDetails').text(content);
            $j('#extendedDate').text('');
            $j('#extendedDateMessage').hide();
            $j('#addOrEditCalExtensionButton').text(i18n.t("core.instrument:viewInstr.addcalextension", "Add Calibration Extension"));
            $j('#addOrEditCalExtensionButton').show();

            //Update extension dialog ready for user to add an extension.
            $j('#calextensiontype').val('TIME');
            $j('#extensionvalue').val(1);
            if ( dto.type == 'TIME' ) {
                $j('#extensiontimeunits').val('MONTH');
            }
            $j('#addoreditcalextension').text(i18n.t("add", "Add"));
            $j('#deletecalextension').hide();

        }
    })
	.catch(textStatus =>  {
		console.errpr(textStatus);
    });
}

function addOrEditCalExtension(plantid, extensiontype, extensionvalue, extensiontimeunits){
    if ( extensiontype == "TIME" && extensiontimeunits == null ) {
        //this should never happen
        alert( "something has gone wrong: no time interval for cal extension of time type" );
    } else {
        //set up post data
        var data = {};
        data["plantid"] = plantid;
        data["type"] = extensiontype;
        data["value"] = extensionvalue;
        data["timeUnit"] = extensiontimeunits;

        //send ajax request
        fetch(window.cwms.urlGenerator("calextension/addorupdate.json"),{
            method: "POST",
			headers:{...window.cwms.metaDataObtainer.csrfHeader,
						"Content-Type":"application/json; charset=utf-8"
			},
            body: JSON.stringify(data),
        }).then(res => res.ok?res.json():Promise.reject(res.statusText))
		.then(dto =>  {
            if (!dto.success) {
                // show error message
                $j.prompt(dto.message);
            }
            else {
                initializeCalExtensionDetails(dto.plantid);
            }
            $j("#calExtensionDialog").dialog("close");
        }).catch(textStatus =>  {
            alert( `Request failed: ${textStatus}` );
        });
    }
}

function deleteCalExtension(plantid){

    var request = $j.ajax({
        type: "GET",
        contentType: "application/json",
        url: "calextension/delete.json?plantid=" + plantid});

    //deal with response
    request.done( function (dto) {
        if (!dto.success) {
            // show error message
            $j.prompt(dto.message);
        }
        else {
            initializeCalExtensionDetails(dto.plantid);
        }
        $j("#calExtensionDialog").dialog("close");
    });

    //Ajax request failed
    request.fail(function( jqXHR, textStatus ) {
        alert( "Request failed: " + textStatus );
    });

}

						
/**
 * this method toggles the visibility of actions in the validation table
 * 
 * @param {Integer} rowcount the current row number
 * @param {String} linkId the id of the show actions link clicked
 */
function toggleActions(rowcount, linkId)
{
	// actions are currently hidden so make them visible
	if ($j('#validationchild' + rowcount).hasClass('hid'))
	{
		// remove image to show actions and append hide actions image
		$j('#' + linkId).empty().append('<img src="img/icons/items_hide.png" width="16" height="16" alt="' + i18n.t("hideActions", "Hide Actions") + '" title="' + i18n.t("hideActions", "Hide Actions") + '" class="image_inline" />');
		// make the actions row visible and add slide down effect
		$j('#validationchild' + rowcount).removeClass('hid').addClass('vis').find('div:first').slideDown(1000);
	}
	// actions are currently visible so hide them
	else
	{
		// remove image to hide actions and append show actions image
		$j('#' + linkId).empty().append('<img src="img/icons/items.png" width="16" height="16" alt="' + i18n.t("viewActions", "View Actions") + '" title="' + i18n.t("viewActions", "View Actions") + '" class="image_inline" />');
		// add effect to slide up actions
		$j('#validationchild' + rowcount + ' div').slideUp(1000, function()
		{
			// hide row
			$j('#validationchild' + rowcount).removeClass('vis').addClass('hid');
		});
	}	
}

/**
 * this method creates the thickbox content for adding a new instrument validation action.
 * 
 * @param {Integer} issueid the id of the instrument issue.
 * @param {Integer} statusid the id of the current status of the issue.
 */
function thickboxActionContent(issueid, statusid)
{
	// variable for thickbox content
	var content = '';
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/instrumentvalidationstatusservice.js'), true,
	{
		callback: function()
		{
			// get a list of all available instrumentvalidationstatus entities
			instrumentvalidationstatusservice.getAllInstrumentValidationStatuss(
			{
				callback:function(statuslist)
				{
					// create content for thickbox to add new validation action
					content =	'<div class="thickbox-box">' +
								// add warning box to display error messages if the link fails
								'<div class="hid warningBox1">' +
									'<div class="warningBox2">' +
										'<div class="warningBox3">' +
										'</div>' +
									'</div>' +
								'</div>' +
								'<fieldset>' +
									'<legend>' + i18n.t("core.instrument:viewInstr.addValidationAction", "Add Validation Action") + '</legend>' +
									'<ol>' +
										'<li>' +
											'<label>' + i18n.t("core.instrument:viewInstr.actionLabel", "Action:") + '</label>' +
											'<textarea name="action" id="action" rows="4" cols="50"></textarea>' +
										'</li>' +
										'<li>' +
											'<label>' + i18n.t("statusLabel", "Status:") + '</label>' +
											'<select name="issuestatusid" id="issuestatusid">';
												$j.each(statuslist, function(i)
												{
													if (statusid == statuslist[i].statusid)
													{
														content = content + '<option value="' + statuslist[i].statusid + '" selected="selected">' + statuslist[i].name + '</option>';
													}
													else
													{
														content = content + '<option value="' + statuslist[i].statusid + '">' + statuslist[i].name + '</option>';
													}													
												});
						content = content +	'</select>' +
										'</li>' +
										'<li>' +
											'<label>&nbsp;</label>' +
											'<input type="button" value="' + i18n.t("save", "Save") + '" name="Save" onclick=" insertValidationAction(' + issueid + ', $j(\'#action\').val(), $j(\'#issuestatusid\').val()); return false; "/>' +
										'</li>' +
									'</ol>' +
								'</fieldset>' +
							'</div>';
							
					// append thickbox content
					$j('#TB_ajaxContent').append(content);		
				}
			});
		}
	});
}

/**
 * saves the given action as a new instrumentvalidationaction and updates the status of the 
 * instrumentvalidationissue to match that of the statusid.
 * 
 * @param {Integer} issueid the id of the instrument issue.
 * @param {String} action the action string.
 * @param {Integer} statusid the new status id.
 */
function insertValidationAction(issueid, action, statusid)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/instrumentvalidationactionservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to insert the instrument validation action
			instrumentvalidationactionservice.insert(issueid, action, statusid, 
			{
				callback:function(results)
				{
					if(results.success != true)
					{
						// empty the error warning box
						$j('div.thickbox-box div.warningBox3').empty();
						// variable to hold error messages
						var errors = '';
						$j.each(results.errorList, function(i)
						{
							// display error messages
							var error = results.errorList[i];
							errors = '<div>' + i18n.t("fieldError", {varErrField: error.field, varErrMsg:error.defaultMessage, defaultValue : "Field: " + error.field + " failed. Message:" + error.defaultMessage}) + '</div>';
						});
						// show error message in error warning box
						$j('div.thickbox-box div:first').removeClass('hid').addClass('vis').find('.warningBox3').append(errors);
					}
					else
					{
						// assign object
						var action = results.results;
						// append new action to the appropriate table
						$j('#actionlist' + action.issue.id).append(	'<tr>' +
																		'<td>' + action.action + '</td>' +
																		'<td>' + action.underTakenBy.name + '</td>' +
																		'<td>' + action.formattedUnderTakenOn + '</td>' +
																	'</tr>');
						// update the status of validation issue
						$j('tr#issue' + issueid + ' td.stat').text(action.issue.status.name);
						// update the action size count
						$j('tr#issue' + issueid + ' td span:last').text(parseInt($j('tr#issue' + issueid + ' td span:last').text()) + 1);
						// new status been set to resolved?
						if (!action.issue.status.requiringAttention)
						{
							// remove the red text from issue
							$j('tr#issue' + issueid + ' td span:first').removeClass();
							// remove the add action link and table cell
							$j('tr#issue' + issueid + ' td:last').remove();
							// add resolved by and resolved on table data
							$j('tr#issue' + issueid).append('<td>' + action.issue.resolvedBy.name + '</td>' +
															'<td>' + action.issue.formattedResolvedOn + '</td>');
						}
						// close thickbox
						tb_remove();
					}	
				}
			});
		}
	});
}







/**
 * this method calculates and returns a string representation of the next recall due
 * date based on the given date. Subtracts a month from the given date.
 * 
 * @param {Object} calDueDate the next calibration due date of instrument
 */
function getRecallPeriod(calDueDate)
{
	// calibration due date?
	if (calDueDate != null)
	{
		try 
		{
			// create new date
			var cal = new Date();
			// set date to calibration due date
			cal.setTime(calDueDate);
			// add one to month as javascript months run from (0 - 11), then subtract one month for recall
			cal.setMonth((cal.getMonth() + 1) - 1);
			// if recall month is zero? previous year?
			if (cal.getMonth() == 0)
			{
				// set year to previous
				cal.setFullYear(cal.getFullYear() + -1);
			}
			// return month in words and full year
			return getMonthInWords(cal.getMonth()).concat(' ').concat(cal.getFullYear());
		} 
		catch (e) 
		{
			// return message			
			return i18n.t("unableGetRecallDate", "Unable to get recall date");
		}
	}
	else
	{
		// return message
		return i18n.t("unableCalculateDueDate", "Unable to calculate due date");
	}
}

/**
 * this method returns the month in words for an integer value.
 * 
 * @param {Object} intMonth the integer value of month
 */
function getMonthInWords(intMonth)
{
	var strMonth = null;
	switch (intMonth)
	{
		case 0:
			strMonth = i18n.t("common:date.december", "December");
			break;
		case 1:
			strMonth = i18n.t("common:date.january", "January");
			break;
		case 2:
			strMonth = i18n.t("common:date.february", "February");
			break;
		case 3:
			strMonth = i18n.t("common:date.march", "March");
			break;
		case 4:
			strMonth = i18n.t("common:date.april", "April");
			break;
		case 5:
			strMonth = i18n.t("common:date.may", "May");
			break;
		case 6:
			strMonth = i18n.t("common:date.june", "June");
			break;
		case 7:
			strMonth = i18n.t("common:date.july", "July");
			break;
		case 8:
			strMonth = i18n.t("common:date.august", "August");
			break;
		case 9:
			strMonth = i18n.t("common:date.september", "September");
			break;
		case 10:
			strMonth = i18n.t("common:date.october", "October");
			break;
		case 11:
			strMonth = i18n.t("common:date.november", "November");
			break;
	}

	return strMonth;
}



function generateRecallResponseContent(recallitemid, rowcount)
{		
	// variable to hold content
	var content = '';
	// load javascript for boxy pop up if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/recallresponsestatusservice.js'), true,
	{
		callback: function()
		{
			recallresponsestatusservice.getAllRecallResponseStatusDWR(
			{
				callback: function(result)
				{
					// retrieval of status successful?
					if (result.success)
					{
						// add status's to variable
						var status = result.results;
						// create content to add new recall rule
						var content =	'<div id="addRecallResponseNoteBoxy" class="overflow boxy">' +
											// add warning box to display error messages
											'<div class="hid warningBox1">' +
												'<div class="warningBox2">' +
													'<div class="warningBox3">' +
													'</div>' +
												'</div>' +
											'</div>' +								
											'<fieldset>' +
												'<ol>' +
													'<li>' +
														'<label>' + i18n.t("core.instrument:viewInstr.recallStatusLabel", "Recall Status:") + '</label>' +
														'<select name="statusid" id="statusid">';
															$j.each(status, function(i, s)
															{
																content += '<option value="' + s.statusid + '">' + s.description + '</option>';
															});
											content +=	'</select>' +
													'</li>' +
													'<li>' +
														'<label>' + i18n.t("core.instrument:viewInstr.recallCommentLabel", "Recall Comment:") + '</label>' +
														'<textarea name="comment" id="comment" cols="80" rows="5"></textarea>' +
													'</li>' +
													'<li>' +
														'<label>&nbsp;</label>' +
														'<input type="button" value="' + i18n.t("addNote", "Add Note") + '" onclick=" insertRecallResponseNote(' + recallitemid + ', ' + rowcount + '); "/>' +
													'</li>' +
												'</ol>' +
											'</fieldset>' +
										'</div>';										
						// create new boxy pop up using content variable
						var addRecallResponseNoteBoxy = new Boxy(content, {title: i18n.t("core.instrument:viewInstr.addNewRecallResponseNote", "Add New Recall Response Note"), modal: true, unloadOnHide: true});
						// show boxy pop up and adjust size
						addRecallResponseNoteBoxy.tween(680, 240);
					}
					else
					{
						$j.prompt(result.message);
					}
				}
			});
		}
	}); 	
}

function insertRecallResponseNote(recallitemid, rowcount)
{
	// get status id and comment
	var statusid =  $j('div#addRecallResponseNoteBoxy select#statusid').val();
	var comment = $j('div#addRecallResponseNoteBoxy textarea#comment').val();
	// variable to hold content
	var content = '';
	// load javascript for boxy pop up if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/recallresponsenoteservice.js'), true,
	{
		callback: function()
		{
			recallresponsenoteservice.insertRecallResponseNoteDWR(recallitemid, statusid, comment,
			{
				callback: function(result)
				{
					// insertion of recall response note successful?
					if (result.success)
					{
						// assign recall response note to variable
						var note = result.results;
						// check the row we are adding to is not hidden?
						if ($j('tr#child' + rowcount).hasClass('hid'))
						{
							// show the table row and div
							$j('tr#child' + rowcount).removeClass('hid').addClass('vis').find('div:first').css({display: 'block'});
							// remove image to show items and append hide items image
							$j('#rrLink' + rowcount).empty().append('<img src="img/icons/items_hide.png" width="16" height="16" alt="' + i18n.t("core.instrument:viewInstr.hideRecallResponseNotes", "Hide Recall Response Notes") + '" title="' + i18n.t("core.instrument:viewInstr.hideRecallResponseNotes", "Hide Recall Response Notes") + '" class="image_inline" />');
						}
						// check for message row
						if ($j('tr#child' + rowcount + ' table tbody tr:first td').length < 2)
						{
							// remove the row
							$j('tr#child' + rowcount + ' table tbody tr:first').remove();
						}
						// create content
						var content = 	'<tr id="rrn' + note.noteid + '">' +
											'<td class="status">' + note.recallResponseStatus.description + '</td>' +
											'<td class="comments">' + note.comment + '</td>' +
											'<td class="setby">' + note.setBy.name + '</td>' +
											'<td class="seton">' + formatDate(note.setOn, 'hh:mm:ss') + ' ' + formatDate(note.setOn, 'dd.MM.yyyy') + '</td>' +
											'<td class="del">' +
												'<a href="#" onclick=" event.preventDefault(); deleteRecallResponseNote(' + note.noteid + ', ' + rowcount + '); " title="' + i18n.t("core.instrument:viewInstr.deleteRecallResponseNote", "Delete Recall Response Note") + '" class="deleteAnchor">&nbsp;</a>' +
											'</td>' +
										'</tr>';
						// add new row for note
						$j('tr#child' + rowcount + ' table tbody').prepend(content);
						// update the recall response note count
						$j('span#recallResponseNoteCount' + rowcount).text(parseInt($j('span#recallResponseNoteCount' + rowcount).text()) + 1);
						// get current boxy and close
						Boxy.get($j('div#addRecallResponseNoteBoxy')).hide();
						
					}
					else
					{
						// show error message in error warning box
						$j('div#addRecallResponseNoteBoxy div:first').removeClass('hid').addClass('vis').find('.warningBox3').empty().html(result.message);
					}
				}
			});
		}
	});
}

/**
 * this method shows or hides the appropriate recall response notes table.
 * 
 * @param (Integer) rowcount integer value of the row for which items should be found and displayed
 */
function toggleRecallResponses(rowcount)
{	
	// items are currently hidden so make them visible
	if ($j('#child' + rowcount).hasClass('hid'))
	{
		// remove image to show items and append hide items image
		$j('#rrLink' + rowcount).empty().append('<img src="img/icons/items_hide.png" width="16" height="16" alt="' + i18n.t("core.instrument:viewInstr.hideRecallResponseNotes", "Hide Recall Response Notes") + '" title="' + i18n.t("core.instrument:viewInstr.hideRecallResponseNotes", "Hide Recall Response Notes") + '" class="image_inline" />');
		// make the delivery items row visible and add slide down effect
		$j('#child' + rowcount).removeClass('hid').addClass('vis').find('div:first').slideDown(1000);
	}
	// delivery items are currently visible so hide them
	else
	{
		// remove image to hide items and append show items image
		$j('#rrLink' + rowcount).empty().append('<img src="img/icons/items.png" width="16" height="16" alt="' + i18n.t("core.instrument:viewInstr.showRecallResponseNotes", "Show Recall Response Notes") + '" title="' + i18n.t("core.instrument:viewInstr.showRecallResponseNotes", "Show Recall Response Notes") + '" class="image_inline" />');
		// add effect to slide up delivery items and hide row
		$j('#child' + rowcount + ' div').slideUp(1000, function(){
			$j('#child' + rowcount).removeClass('vis').addClass('hid');
		});
	}
}

/**
 *	this method deletes a recall response note from the database and removes the
 * 	appropriate row from the page.
 *
 *	@param {Integer} noteid the id of the note to be deleted
 *	@param (Integer) rowcount integer value of the row for which items should be found and displayed
 */
function deleteRecallResponseNote(noteid, rowcount)
{
	// load javascript for boxy pop up if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/recallresponsenoteservice.js'), true,
	{
		callback: function()
		{
			recallresponsenoteservice.deleteRecallResponseNoteDWR(noteid,
			{
				callback: function(result)
				{
					// deletion of recall response note successful?
					if (result.success)
					{
						// add new row for note
						$j('tr#child' + rowcount + ' table tbody tr#rrn' + noteid).remove();
						// last note has been deleted?
						if ($j('tr#child' + rowcount + ' table tbody tr').length < 1)
						{
							// add message of no recall response notes
							$j('tr#child' + rowcount + ' table tbody').append(	'<tr>' +
																					'<td colspan="5" class="bold text-center">' + i18n.t("viewInstr:noRecallResponse", "No Recall Response Notes") + '</td>' +
																				'</tr>');
						}
						// update the recall response note count
						$j('span#recallResponseNoteCount' + rowcount).text(parseInt($j('span#recallResponseNoteCount' + rowcount).text()) - 1);
					}
					else
					{
						// show error message
						$j.prompt(result.message);
					}
				}
			});
		}
	});
}

// setup the current page number for calibrations
var calPage = 1;

/**
 * this method loads any more available calibrations for the instrument
 * 
 * @param {Object} anchor the link clicked
 * @param {Integer} plantid the instrument's id.
 * @param {Integer} resPerPage the number of results to fetch.
 * @param {Integer} the current rowcount figure used in ids to make them unique
 */
function fetchMoreCalibrationsForInstrument(anchor, plantid, resPerPage, rowcount)
{
	// change anchor text
	$j(anchor).text(i18n.t("loading", "Loading....."));
	// update page count
	calPage = calPage + 1;
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/calibrationservice.js', 'script/trescal/core/utilities/TooltipLinks.js'), true,
	{
		callback: function()
		{
			// call dwr service to retrieve any further calibrations for this instrument
			calibrationservice.getAllCalibrationsPerformedWithInstrumentStandard(plantid, calPage, resPerPage,
			{
				callback:function(results)
				{
					// results returned?
					if (results.length > 0)
					{
						// variable to hold new content
						var content = '';
						// create new page content using results
						$j.each(results, function(i, c)
						{								
							// construct content
							content += 	'<tr>' +
											'<td>';
												$j.each(c.links, function(x, cl)
												{
													content += 	i18n.t("item", "Item") + ' ' +
																jobitemLinkDWRInfo('jicalibration.htm', cl.ji.jobItemId, cl.ji.itemNo, rowcount) + ' ';
													rowcount++;
													content += 	i18n.t("onJob", "On Job") + ' ' +
																jobnoLinkDWRInfo(cl.ji.job.jobid, cl.ji.job.jobno, rowcount, false);
													rowcount++;
													content += 	'<br />';
												});
								content +=	'</td>';
											if (c.completedBy != null)
											{
												content += 	'<td>' + c.completedBy.name + '<br />' + cwms.dateTimeFormatFunction(c.completeTime) + '</td>';
											}
											else
											{
												content += 	'<td>-</td>';
											}											
								content +=	'<td>' + c.calType.serviceType.shortName + '</td>' +
											'<td>' + c.calProcess.process + '</td>' +
											'<td>' + procedureLinkDWRInfo(c.proc, rowcount, false) + '</td>';
											rowcount++;											
								content +=	'<td>' +
												'<span class="calStandardsSize' + c.id + '">' + c.standardsUsed.length + '</span> ' +
												// hover over this anchor and a tooltip is displayed showing the contents of the div created below
												'<a href="?id=#calStandards' + c.id + '&amp;width=450" title="' + i18n.t("core.instrument:viewInstr.standardsUsed", "Standards Used") + '" id="tip' + c.id + '" class="jTip">' +
													'<img src="img/icons/standard.png" width="16" height="16" class="image_inl" alt="" />' +
												'</a>' +
												// contents of this div are displayed in tooltip created above
												'<div id="calStandards' + c.id + '" class="hid">' +
													'<table class="default2" summary="This table displays all standards used on calibration">' +
														'<thead>' +
															'<tr>' +
																'<th colspan="2">' +
																	'<div class="float-left">' + i18n.t("core.instrument:viewInstr.standardsUsed", "Standards Used") + ' (<span class="calStandardsSize' + c.id + '">' + c.standardsUsed.length + '</span>)</div>' +
																'</th>' +
															'</tr>' +
														'</thead>' +
														'<tbody>';															
															if (c.standardsUsed.length < 1)
															{
																content +=	'<tr>' +
																				'<td colspan="2" class="center bold">' + i18n.t("core.instrument:viewInstr.noStandardsUsed", "There are no standards for this calibration") + '</td>' +
																			'</tr>';
															}
															else
															{
																$j.each(c.standardsUsed, function(x, stdUsd)
																{
																	content +=	'<tr>' +
																					'<td>' +
																						'<a href="viewInstrument.htm?plantid=' + stdUsd.inst.plantid + '" class="mainlink" target="_blank">' + stdUsd.inst.plantid + '</a>' + 
																					'</td>' +
																					'<td>' +
																						stdUsd.inst.definitiveInstrument + '<br />' +
																						i18n.t("serialNoLabel", "Serial No:") + ' ' + stdUsd.inst.serialno + '<br />' +
																						i18n.t("plantNoLabel", "Plant No:") + ' ' + stdUsd.inst.plantno +
																					'</td>' +
																				'</tr>';
																});
															}															
											content +=	'</tbody>' +
													'</table>' +
												'</div>' + 
											'</td>' +
											'<td>' +
												c.certs.length + ' ' +
												// hover over this anchor and a tooltip is displayed showing the contents of the div created below
												'<a href="?id=#certificate' + c.id + '&amp;width=450" title="Certificates" id="certificatetip' + c.id + '" class="jTip">' +
													'<img src="img/icons/certificate.png" width="16" height="16" class="image_inl" alt="" />' +
												'</a>' +
												// contents of this div are displayed in tooltip created above
												'<div id="certificate' + c.id + '" class="hid">' +
													'<table class="default2" id="jicalcertificates" summary="This table displays all certificates on calibration">' +
														'<thead>' +
															'<tr>' +
																'<th colspan="3">' + i18n.t("certificates", "Certificates") + ' (' + c.certs.length + ')</th>' +
															'</tr>' +
															'<tr>' +
																'<th id="certno" scope="col">' + i18n.t("certNo", "Cert No") + '</th>' +
																'<th id="certstat" scope="col">' + i18n.t("status", "Status") + '</th>' +
																'<th id="certdurtn" scope="col">' + i18n.t("certDuration", "Cert Duration") + '</th>' +
															'</tr>' +
														'</thead>' +
														'<tbody>';
															if (c.certs.length < 1)
															{
																content += 	'<tr>' +
																				'<td colspan="3" class="center bold">' + i18n.t("instrumenView.noCertForThisCal", "There are no certificates for this calibration") + '</td>' +
																			'</tr>';
															}
															else
															{
																$j.each(c.certs, function(x, c)
																{
																	content +=	'<tr>' +
																					'<td>';
																						$j.each(c.links, function(z, cl)
																						{
																							if((cl.certFilePath != null) && (cl.certFilePath != ''))
																							{
																								content +=	'<a href="file:///' + cl.certFilePath + '" target="_blank" class="mainlink">' + c.certno + '</a>';
																							}
																							else
																							{
																								content +=	'' + c.certno + '';
																							}
																						});
																		content +=	'</td>' +
																					'<td>' + c.status.name + '</td>' +
																					'<td>' + c.duration + ' ' + i18n.t("months", "months") + '</td>' +
																				'</tr>';
																});																
															}
											content +=	'</tbody>' +
													'</table>' +
												'</div>' +
											'</td>' +
											'<td>' + c.status.name + '</td>' +
										'</tr>';	
						});
						// update the current rowcount value
						$j('input[name="instStandardCurrentRowcount"]').val(rowcount);
						// append new instrument standard calibrations to table
						$j('table.instStandardCalibrations > tbody').append(content);
						// amend instrument calibration result size
						$j('span.instStandardCalSize').text(parseInt($j('span.instStandardCalSize:first').text()) + results.length);
						// full set of results returned?
						if (results.length < resPerPage)
						{
							// change anchor text
							$j(anchor).text(i18n.t("core.instrument:viewInstr.allInstrCalLoaded", "All Instrument Calibrations Loaded"));
						}
						else
						{
							// change anchor text
							$j(anchor).text(i18n.t("loadMore", "Load More..."));
						}
						// initialise tooltips
						JT_init();
					}
					else
					{
						// change anchor text
						$j(anchor).text(i18n.t("core.instrument:viewInstr.allInstrCalLoaded", "All Instrument Calibrations Loaded"));
					}
				}
			});
		}
	});
}

/**
 * this method sources available accessories for a hire instrument and displays them in an overlay.
 * 
 * @param {Integer} hireInstId id of the hire instrument
 */
function updateAccessories(hireInstId)
{
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/hireaccessoryservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to get all accessories for hire instrument
			hireaccessoryservice.getAccessoriesForHireInstrumentAndModel(hireInstId,
			{
				callback: function(result)
				{
					// successful result?
					if (result.success)
					{
						// add dto to variable
						var dto = result.results;
						// create overlay content
						var content = 	'<div id="updateAccessoriesOverlay">' +
											'<table class="default4 activeHireAccessories" summary="display any active hire instrument accessories">' +
												'<thead>' +
													'<tr>' +
														'<td class="bold" colspan="3">' + i18n.t("core.instrument:viewInstr.activeHireInstrAccessories", "Active Hire Instrument Accessories") + ' (' + dto.activeHireInstAccessories.length + ') (Tick to de-activate)</td>' +
													'</tr>' +
													'<tr>' +
														'<th class="item">' + i18n.t("item", "Item") + '</th>' +
														'<th class="status">' + i18n.t("status", "Status") + '</th>' +
														'<th class="add">' +
															'<input type="checkbox" id="selectAll" onclick=" selectAll(this.checked, \'activeHireAccessories\'); " /> ' + i18n.t("all", "All") + '<br />' +
														'</th>' +
													'</tr>' +
												'</thead>' +
												'<tbody>';
													// any to display?
													if (dto.activeHireInstAccessories.length > 0)
													{
														$j.each(dto.activeHireInstAccessories, function(i, acc)
														{
															content +=	'<tr>' +
																			'<td class="item">' + acc.item + '</td>' +
																			'<td class="status">' + acc.status.name + '</td>' +
																			'<td class="add"><input type="checkbox" name="existActiveAccSelector" value="' + acc.hireAccessoryId + '" /></td>' +
																		'</tr>';
														});
													}
													else
													{
														content +=	'<tr>' +
																		'<td colspan="3" class="bold text-center">' + i18n.t("core.instrument:viewInstr.noActiveAccessoriesLinked", "No Active Accessories Linked To This Instrument") + '</td>' +
																	'</tr>';
													}
									content +=	'</tbody>' +
											'</table>' +
											'<table class="default4 deactiveHireAccessories" summary="display any deactive hire instrument accessories">' +
												'<thead>' +
													'<tr>' +
														'<td class="bold" colspan="3">' + i18n.t("core.instrument:viewInstr.inactiveHireInstrAccessoriesLinked", "In-Active Hire Instrument Accessories") + ' (' + dto.deactivatedHireInstAccessories.length + ') ' + i18n.t("core.instrument:viewInstr.tickToReactivate", "Tick to Re-activate") + ' )</td>' +
													'</tr>' +
													'<tr>' +
														'<th class="item">' + i18n.t("item", "Item") + '</th>' +
														'<th class="status">' + i18n.t("status", "Status") + '</th>' +
														'<th class="add">' +
															'<input type="checkbox" id="selectAll" onclick=" selectAll(this.checked, \'deactiveHireAccessories\'); " />' + i18n.t("all", "All") + '<br />' +
														'</th>' +
													'</tr>' +
												'</thead>' +
												'<tbody>';
													// any to display?
													if (dto.deactivatedHireInstAccessories.length > 0)
													{
														$j.each(dto.deactivatedHireInstAccessories, function(i, acc)
														{
															content +=	'<tr>' +
																			'<td class="item">' + acc.item + '</td>' +
																			'<td class="status">' + acc.status.name + '</td>' +
																			'<td class="add"><input type="checkbox" name="existInActiveAccSelector" value="' + acc.hireAccessoryId + '" /></td>' +
																		'</tr>';
														});
													}
													else
													{
														content +=	'<tr>' +
																		'<td colspan="3" class="bold text-center">' + i18n.t("core.instrument:viewInstr.noInactiveAccessories", "No In-active Accessories Linked To This Instrument") + '</td>' +
																	'</tr>';
													}
									content +=	'</tbody>' +
											'</table>' +
											'<div>&nbsp;</div>' +
											'<table class="default4 modelAccessories" summary="display any model accessories available">' +
												'<thead>' +
													'<tr>' +
														'<td colspan="2">' + i18n.t("core.instrument:viewInstr.addAvailableModelAccessories", "Add Available Model Accessories") + ' (' + dto.distinctModelAccessories.length + ')</td>' +
													'</tr>' +
													'<tr>' +
														'<th class="item">Item</th>' +
														'<th class="add">' +
															'<input type="checkbox" id="selectAll" onclick=" selectAll(this.checked, \'modelAccessories\'); " />' + i18n.t("all", "All") + '<br />'
														'</th>' +
													'</tr>' +
												'</thead>' +
												'<tbody>';
													// any to display?
													if (dto.distinctModelAccessories.length > 0)
													{
														$j.each(dto.distinctModelAccessories, function(i, item)
														{
															content +=	'<tr>' +
																			'<td class="item">' + item + '</td>' +
																			'<td class="add"><input type="checkbox" name="modelAccSelector" value="' + escape(item) + '" /></td>' +
																		'</tr>';
														});
													}
													else
													{
														content +=	'<tr>' +
																		'<td colspan="2">' + i18n.t("core.instrument:viewInstr.noModelAccessoriesAvailable", "No Model Accessories Available") + '</td>' +
																	'</tr>';
													}
									content +=	'</tbody>' +
											'</table>' +
											'<table class="default4 newAccessory" summary="">' +
												'<thead>' +
													'<tr>' +
														'<td colspan="2">' + i18n.t("core.instrument:viewInstr.createNewAccessory", "Create New Accessory") + '</td>' +
													'</tr>' +
													'<tr>' +
														'<th class="add">' + i18n.t("add", "Add") + '</th>' +
														'<th class="item">' + i18n.t("add", "Add") + '</th>' +
													'</tr>' +
												'</thead>' +
												'<tbody>' +
													'<tr>' +
														'<td class="add"><input type="checkbox" class="widthAuto" name="newAcc" /></td>' +
														'<td class="item"><input type="text" name="newAccValue" /></td>' +
													'</tr>' +
												'</tbody>' +
											'</table>' +
											'<div class="text-center">' +
												'<input type="button" value="' + i18n.t("save", "Save") + '" onclick=" this.disabled = true; updateHireInstrumentAccessories(' + hireInstId + ' ); return false; " />' +
											'</div>' +
										'</div>';
						// create new overlay
						loadScript.createOverlay('dynamic', i18n.t("core.instrument:viewInstr.updateInstrAccessories", "Update Instrument Accessories"), null, content, null, 70, 680, null);
					}
					else
					{
						// show error message
						$j.prompt(result.message);
					}
				}
			});
		}
	});
}

/**
 * this method updates the hire accessories for an instrument
 * 
 * @param {Integer} hireInstId id of the hire instrument
 */
function updateHireInstrumentAccessories(hireInstId)
{
	// create new array for add/remove existing active accessories
	var existActiveAccArray = [];
	// get all selected hire accessories
	$j('div#updateAccessoriesOverlay table.activeHireAccessories tbody input[name="existActiveAccSelector"]:checked').each(function(i, acc)
	{
		existActiveAccArray.push(acc.value);
	});	
	// create new array for add/remove existing inactive accessories
	var existInActiveAccArray = [];
	// get all selected hire accessories
	$j('div#updateAccessoriesOverlay table.deactiveHireAccessories tbody input[name="existInActiveAccSelector"]:checked').each(function(i, acc)
	{
		existInActiveAccArray.push(acc.value);
	});
	// create new array for model accessory strings
	var modelAccArray = [];
	// get all selected model accessory strings
	$j('div#updateAccessoriesOverlay table.modelAccessories tbody input[name="modelAccSelector"]:checked').each(function(i, modacc)
	{
		modelAccArray.push(unescape(modacc.value));
	});
	// boolean to indicate new hire accessory
	var newAcc = $j('div#updateAccessoriesOverlay table.newAccessory input[name="newAcc"]').is(':checked');
	var newAccValue = $j('div#updateAccessoriesOverlay table.newAccessory input[name="newAccValue"]').val();
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/hireaccessoryservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to update hire item accessories
			hireaccessoryservice.updateInstrumentAccessories(hireInstId, existActiveAccArray, existInActiveAccArray, modelAccArray, newAcc, newAccValue,
			{
				callback:function(result)
				{
					// update hire instrument accessories successful?
					if (result.success)
					{				
						// get hire instrument
						var hi = result.results;
						// create new page content for active and inactive hire accessories
						var activeContent = '';
						var inactiveContent = '';
						// check all hire instrument accessories
						$j.each(hi.hireInstAccessories, function(i, acc)
						{
							// create content
							var content = 	'<div class="hireAccItem">' + 
												'<span id="hireAcc' + acc.hireAccessoryId + '">' + acc.item + '</span>' + 
												'<a href="#" class="edithireaccessory" onclick=" editHireAccessoryContent(' + acc.hireAccessoryId + ', $j(this).prev().text(), $j(this).parent()); return false; " title="' + i18n.t("core.instrument:viewInstr.editHireAccessoryDesc", "Edit hire accessory description") + '"></a>' +
											'</div>' +
											'<div class="hireAccStatus">' +
												'<select id="hireAcc' + acc.hireAccessoryId + '" onchange=" updateHireAccessoryStatus(this.id, ' + acc.hireAccessoryId + ', this.value); return false; ">';
													// add all options
													$j.each(hireAccStateOptions, function(x, opt)
													{
														content +=	'<option value="' + opt.statusid + '"';
																		// current status id of accessory
																		if (opt.statusid == acc.status.statusid)
																		{
																			content +=	' selected="selected"';
																		}
														content +=	'>' + opt.statusname + '</option>';
													});
									content +=	'</select>' +
											'</div>' +
											'<div class="clear"></div>';
							// active accessory
							if (acc.active)
							{
								activeContent += content;
							}
							else
							{
								inactiveContent +=	content;
							}
						});
						// add accessories to page
						$j('li.activeHireInstAccessories > div:first').html(activeContent);
						$j('li.inactiveHireInstAccessories > div:first').html(inactiveContent);
						// check content produced and apply headings or message
						if (activeContent != '')
						{
							$j('li.activeHireInstAccessories > div:first').prepend(	'<div class="bold hireAccItem">' + i18n.t("item", "Item") + '</div>' +
																					'<div class="bold hireAccStatus">' + i18n.t("status", "Status") + '</div>' +
																					'<div class="clear"></div>');
						}
						else
						{
							$j('li.activeHireInstAccessories > div:first').html(i18n.t("core.instrument:viewInstr.noActiveAccessories", "No Active Accessories"));
						}
						// check content produced and apply headings or message
						if (inactiveContent != '')
						{
							$j('li.inactiveHireInstAccessories > div:first').prepend(	'<div class="bold hireAccItem">' + i18n.t("item", "Item") + '</div>' +
																						'<div class="bold hireAccStatus">' + i18n.t("status", "Status") + '</div>' +
																						'<div class="clear"></div>');
						}
						else
						{
							$j('li.inactiveHireInstAccessories > div:first').html(i18n.t("core.instrument:viewInstr.noInactiveAccessories", "No In-Active Accessories"));
						}
						// close the overlay
						loadScript.closeOverlay();
					}
					else
					{
						// update overlay with error messages
						loadScript.modifyOverlay(null, result.message, null, true, null, null, null);
						// re-enable button
						$j('div#updateAccessoriesOverlay input[type="button"]').attr('disabled', false);
					}
				}
			});
		}
	});
}

/**
 * this method 
 * 
 * @param {String} elementid id of select changed
 * @param {Integer} hireAccId id of the hire accessory
 * @param {Integer} hireAccStatusId id of the new hire accessory status
 */
function updateHireAccessoryStatus(elementid, hireAccId, hireAccStatusId)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/hireaccessoryservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to update hire accessory status
			hireaccessoryservice.updateHireAccessoryStatus(hireAccId, hireAccStatusId,
			{
				callback:function(result)
				{
					// update hire accessory status successful?
					if (result.success)
					{
						// show updated text
						$j('#' + elementid).after('<span class="allgood"> ' + i18n.t("updated", "Updated") + '<span>');
						// remove after few seconds
						setTimeout('$j(\'#' + elementid + '\').next().remove()' ,2000);
					}
					else
					{
						// show message of update failure
						$j.prompt(result.message);
					}
				}
			});
		}
	});					
}

/**
 * this method creates the content for an overlay which provides the user with the ability to update
 * a hire accessory description
 * 
 * @param {Integer} hireAccId id of the hire accessory
 * @param {String} currentDescription the current description to be modified
 */
function editHireAccessoryContent(hireAccId, currentDescription)
{
	// create content for overlay
	var content =	'<div id="editHireAccessoryOverlay">' +
						// add warning box to display error messages
						'<div class="hid warningBox1">' +
							'<div class="warningBox2">' +
								'<div class="warningBox3">' +
								'</div>' +
							'</div>' +
						'</div>' +
						'<fieldset>' +
							'<ol>' +
								'<li>' +
									'<label>' + i18n.t("core.instrument:viewInstr.accessoryDescLabel", "Accessory Description:") + '</label>' +
									'<textarea name="description" id="description" class="width70" rows="4" value="' + currentDescription + '">' + currentDescription + '</textarea>' +
								'</li>' +
								'<li>' +
									'<label>&nbsp;</label>' +
									'<input type="button" value="' + i18n.t("save", "Save") + '" onclick=" this.disabled = true; editHireAccessoryAction(' + hireAccId + ', $j(\'#description\').val()); return false; " />' +
								'</li>' +
							'</ol>' +
						'</fieldset>' +
					'</div>'; 	
	// show in overlay
	loadScript.createOverlay('dynamic', i18n.t("editHireAccessoryDesc", "Edit Hire Accessory Description"), null, content, null, 34, 720, 'description');
}

/**
 * this method updates the hire accessory description with the new one provided
 * 
 * @param {Integer} hireAccId id of the hire accessory
 * @param {String} newDescription the new description to be saved
 */
function editHireAccessoryAction(hireAccId, newDescription)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/hireaccessoryservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to update the hire accessory description
			hireaccessoryservice.updateHireAccessoryDescription(hireAccId, newDescription,
			{
				callback:function(result)
				{
					// hire accessory description updated successfully?
					if(result.success)
					{
						// get hire accessory
						var ha = result.results;
						// update hire accessory description on page
						$j('span#hireAcc' + hireAccId).text(ha.item);
						// close overlay
						loadScript.closeOverlay();
					}
					else
					{	
						// empty the error warning box
						$j('div#editHireAccessoryOverlay div.warningBox3').empty();
						// show error message in error warning box
						$j('div#editHireAccessoryOverlay div:first').removeClass('hid').addClass('vis').find('.warningBox3').append(results.message);
						// enable button
						$j('div#editHireAccessoryOverlay input[type="button"]:last').attr('disabled', false);
					}
				}
			});
		}
	});
}

/**
 * this method checks all inputs of type checkbox that are contained within the table 
 * 
 * @param {Boolean} select true if boxes are to be checked and vice versa
 * @param {String} parentElement id of table in which to search for inputs of type checkbox
 */
function selectAll(select, parentElement)
{
	// check all
	if(select)
	{
		$j('.' + parentElement + ' input:checkbox').check('on');
	}
	// uncheck all
	else
	{
		$j('.' + parentElement + ' input:checkbox').check('off');
	}
}

/**
 * this method creates the flexible field content to be displayed in a jquery boxy thickbox
 * 
 * @param {String} flexiblefieldid id of the flexible field
 * @param {String} target the old id of the control
 * @param {Object} anchor the anchor element clicked on the page
 * @param {Object} currentRange the div element of the current range
 * @param {Object} rangeContainer the div element which contains all ranges
 */
function editFlexibleField(flexiblefieldid, target, anchor, currentRange, rangeContainer)
{
	// variables for edit information
	var flexid = '';
	var defid = '';
	var plantid = '';
	var ftype = '';
	var name = '';
	var value = '';
	var libraryid = ''
	var libraries = '';
	
	// set global variables for use in other methods
	currentRangeGlobal = currentRange;
	rangeContainerGlobal = rangeContainer;
	anchorGlobal = anchor;
	
	if (currentRange != null)
	{
		flexid = $j(currentRange).find('#rid' + flexiblefieldid).val();
		defid = $j(currentRange).find('#rdefid' + flexiblefieldid).val();
		plantid = $j(currentRange).find('#rplantid' + flexiblefieldid).val();
		ftype = $j(currentRange).find('#rtype' + flexiblefieldid).val();
		name = $j(currentRange).find('#rname' + flexiblefieldid).val();
		value = $j(currentRange).find('#rvalue' + flexiblefieldid).val();	
		libraryid = $j(currentRange).find('#rLibraryID' + flexiblefieldid).val();	
		libraries = $j(currentRange).find('#rLibraries' + flexiblefieldid).val();	
	}
	
	// create content
	var	content = 	'<div id="editInstrumentFieldBoxy" class="overflow">' +
						'<div class="warningBox1 hid">' +
							'<div class="warningBox2">' +
								'<div class="warningBox3">' +
								'</div>' +
							'</div>' +
						'</div>' +
						'<fieldset>' +
							'<legend>'+ i18n.t("core.instrument:viewInstr.editFlexibleField", "Edit Instrument Field") + '</legend>' +
							'<ol>' +
								'<li>' +
									'<label>' + name + '</label>' +
									'<input type="hidden" name="id" value="' + flexid + '" />';
	
	switch (ftype)
		{
			case 'BIT':
				{
					if (value == 'true')
					{
						content +=	'<input type="checkbox" name="chkvalue" tabindex="100" checked="' + value + '" />' + '</li>';
					}
					else
					{
						content +=	'<input type="checkbox" name="chkvalue" tabindex="100" />' + '</li>';
					}
					break;
				}
			case 'DATETIME':
				{
					content +=	'<input type="text" id="datvalue" name="datvalue" value="' + value + '" class="date" readonly="readonly" tabindex="100" />'
					
					break;
				}
			case 'SELECTION':
				{
					content +=	'<select id="selvalue" name="selvalue">';
					if (libraries != undefined)
					{
						var json = JSON.parse(libraries);
						json.libraries.forEach(function addOption(value) 
						{ 
							if (libraryid == value.id)
							{
								content +=	'<option value="' + value.id + '" selected="selected">' + value.name + '</option>'; 
							}
							else
							{
								content +=	'<option value="' + value.id + '">' + value.name + '</option>'; 
							}
						});
					}
					content +=	'</select>';
					break;
				}
			default:
				{
					content +=	'<input type="text" name="txtvalue" value="' + (value == '$cf.value' ? '' : value) + '" tabindex="100" />' + '</li>';
					break;
				}
		}
	
	content +=	'<li>' +
				'<label>&nbsp;</label>';

	content += '<input type="button" value="' + i18n.t("core.instrument:viewInstr.updateFlexibleField", "Save") + '" onclick=" submitFlexibleField(\'' + ftype + '\', ' + flexid + ',\'' + target + '\', ' + defid + ', ' + plantid + '); return false; " tabindex="103" />';								
	
	content +=	'</li>' +
			'</ol>' + 
		'</fieldset>' +
	'</div>';
	
	// create new boxy pop up using content variable
	var editInstrumentFieldBoxy = new Boxy(content, {title: i18n.t("core.instrument:viewInstr.editFlexibleField", "Edit Instrument Field"), modal: true, unloadOnHide: true});
	// show boxy pop up and adjust size
	editInstrumentFieldBoxy.tween(480, 220);
	
	var jqFlexCalendar = new Array (
			 { 
			 	inputField: 'datvalue',
			 	dateFormat: 'dd.mm.yy',
			 	displayDates: 'all',
				showWeekends: true
			 });		
		
	// load stylesheet using the dom
	loadScript.loadStyle('styles/jQuery/jquery-ui.css', 'screen');
	// load the jquery calendar javascript file
	loadScript.loadScriptsDynamic(new Array('script/thirdparty/jQuery/jquery.ui.js'), true,
	{
		callback: function()
		{
			$j.each(jqFlexCalendar, function(i)
			{
				// create jquery calendars
				loadScript.createJQCal(jqFlexCalendar[i]);
			});
		}
	});	
}

/**
 * this method takes the information from the form in the thickbox to update 
 * a flexible field
 * @param {String} ftype field type (VARCHAR, BIT, FLOAT...)
 * @param {String} flexid of the flexible field to edit 
 */
function submitFlexibleField(ftype, flexid, target, defid, plantid)
{	
	// get values from thickbox
	var value = '';
	var valueToDisplay = '';
	
	switch (ftype)
	{
		case 'BIT':
			{
				if ($j('div#editInstrumentFieldBoxy input[name="chkvalue"]').prop('checked')) {
					value = '1';
				}
				else {
					value = '0';
				}
				break;
			}
		case 'DATETIME':
		{
			value = $j('div#editInstrumentFieldBoxy input[name="datvalue"]').val();	
			break;
		}
		case 'SELECTION':
		{
			value = $j('div#editInstrumentFieldBoxy select[name="selvalue"]').val();	
			valueToDisplay = $j('div#editInstrumentFieldBoxy select[name="selvalue"] option:selected').text();	
			break;
		}
		default:
		{
			value = $j('div#editInstrumentFieldBoxy input[name="txtvalue"]').val();	
			break;
		}
	}
	
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/instrumentfieldvalueservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to update felxible field
			instrumentfieldvalueservice.editFlexibleField(flexid, ftype, value, defid, plantid,
			{
				callback: function(result)
				{	
					// was update successful?
					if (result.success)
					{
						// get boxy instance
						var editInstrumentFieldBoxy = Boxy.get($j('div#editInstrumentFieldBoxy')).tween(480,80);
						
						// wait and then close thickbox
						setTimeout('Boxy.get($j(\'div#editInstrumentFieldBoxy\')).hide()', 1200);		
						
						switch (ftype)
						{
							case 'BIT':
							{
								if (value == '1')
								{
									$j('#' + target + defid).attr('checked', true);
								}
								else
								{
									$j('#' + target + defid).removeAttr('checked');
								}
								break;
							}
							case 'SELECTION':
							{
								$j('#' + target + defid).text(valueToDisplay);	
								break;
							}
							default:
							{
								$j('#' + target + defid).text(value);	
								break;
							}
						}
						
						$j('#rvalue' + defid).val(value);	
					}
					else
					{
						// display error message to user
						$j('div#editInstrumentFieldBoxy .warningBox1').removeClass('hid')
																		.find('.warningBox3')
																		.empty()
																		.append('<div class="text-center attention">' + result.message + '</div>');
					}
				}
			});
		}
	});
}	