function getExchangeFormatForSelectedCompany() {
	var coid = $j('#coid').val();
	if (coid != undefined) {
		$j.ajax({
			url : "getExchangeFormats.json",
			data : {
				coid : coid,
				type : 'INSTRUMENTS_IMPORTATION'
			},
			async : true
		}).done(
			function(data) {
				var content = '';
				for (var i = 0; i < data.length; i++)
					content += '<option value="' + data[i].id + '">'
								+ data[i].name + '</option>'

					$j('#exchangeformat').html(content);

				}).fail(function(result) {
			$j.prompt(result.responseText);
		});

	}
}