

var pageImports = new Array (	'script/trescal/core/utilities/DocGenPopup.js',
								'script/thirdparty/jQuery/jquery.boxy.js' );

var menuElements = 	new Array( 
					{ anchor: 'technicianpart-link', block: 'technicianpart-tab' },
		      	   	{ anchor: 'managerpart-link', block: 'managerpart-tab' }, 
		      	   	{ anchor: 'clientpart-link', block: 'clientpart-tab' }, 
		      	   	{ anchor: 'faultreportolddata-link', block: 'faultreportolddata-tab' },
		      	   	{ anchor: 'froutcome-link', block: 'froutcome-tab' } 
					);


/**
 * this method is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init(){
	
}