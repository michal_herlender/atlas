

function stateSelectOnChange(){
	if($j('#stateSelect').val() === 'OTHER'){
		$j('#otherStateLi').removeClass('hid');
	}else{
		$j('#otherStateLi').addClass('hid');
	}
}

function onClickDispensation() {
	console.log('batata')
	if($j('#dispensation').is(':checked'))
		$j('#dispensationCommentsTextArea').removeClass('hid');
	else
		$j('#dispensationCommentsTextArea').addClass('hid');
}

function onChangeClientApprovalSelect(){
	if($j('#clientApprovalSelect').val()=='false'){
		$j('#clientOutcomeSelect').removeClass('hid');
		$j('#clientOutcomeText').removeClass('hid');
	}else{
		$j('#clientOutcomeSelect').addClass('hid');
		$j('#clientOutcomeText').addClass('hid');
	}
}

function showHideOther(ele)
{
	if(ele.is(':checked'))
		$j('#otherStateLi').removeClass('hid').addClass('vis'); 
	else
		$j('#otherStateLi').removeClass('vis').addClass('hid');
}
