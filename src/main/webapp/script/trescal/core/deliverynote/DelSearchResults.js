/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: Jamie Vinall
*
*	FILENAME		:	DelSearchResults.js
*	DESCRIPTION		:	This javascript file is used on the delsearchresults.vm page.
*					: 	
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	24/09/2007 - JV - Created File
* 						03/10/2007 - SH - Complete rewrite of code to jquery
*
****************************************************************************************************************************************/				

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array ( 'script/trescal/core/deliverynote/ViewDIUtility.js' );

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * this variable contains the page name which should be used when creating the search again
 * link in the header section
 */
var searchAgain = 'delsearch.htm';
												