/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: Jamie Vinall
*
*	FILENAME		:	DelSearch.js
*	DESCRIPTION		:	This javascript file is used on the delsearch.vm page.
*					: 	
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	21/09/2007 - JV - Created File
* 					:	10/10/2007 - SH - JQuery functions and comment
*
****************************************************************************************************************************************/

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{
	// delay the focus on cascading search plugin company text input
	setTimeout("$j('input[name=comptext]').focus()", 1400);
}

/**
 * Array of calendar objects containing the initial setup literals needed
 * @param inputField (String) id of the input field to which the calendar should be binded
 * @param dateFormat (String) format that the date should be displayed
 * @param displayDates (String) 'all' make all dates available
 * 								'future' show only dates in the future
 * 								'past' show only dates in the past
 * Applied in init() function.
 */				
//var jqCalendar = new Array ( { 
//							 inputField: 'date',
//							 dateFormat: 'dd.mm.yy',
//							 displayDates: 'all'
//							 } );

/**
 * this function changes the corole value stored in the 'compCoroles' hidden input field
 * when changed the text field and search list are emptied ready for a search using new corole
 */
function changeCorole(value)
{
	// clear company search ready for new corole
	$j('div.compSearchJQPlugin').after(	'<div class="compSearchJQPlugin"></div>').remove();
	// change corole in hidden input
	$j('input[name="compCoroles"]').val(value);
	// initialise company search again
	$j('div.compSearchJQPlugin').compSearchJQPlugin({	field: 'coid',
														tabIndex: 6,
														compCoroles: value});
	// add focus to input
	$j('input[name="comptext"]').focus();
}