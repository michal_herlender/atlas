/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	AddGenItems.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	18/09/2007
* 					: 	08/10/2007 - SH - JQuery functions and comment
*					:	07/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'firstitem';

/**
 * global variable to hold the general delivery item count
 */
var genitemCount = 1;

/**
 * this method adds one general delivery item to the list and creates the option of adding
 * another one below
 * 
 * @param {Object} div div where the new general delivery item form elements should be added
 */
function addItem(div)
{	
	// empty the div content and append new textarea with focus and anchor for removing item
	$j(div).empty().append(	'<textarea class="width80 presetComments GENERAL_DELIVERY_ITEM" rows="3" name="itemlist' + genitemCount + '"></textarea>' +
							'<a href="#" onclick=" removeItem($j(this).parent().parent()); return false; " class="marg-left">' +
								'<img src="img/icons/delete.png" height="16" width="16" alt="' + i18n.t("removeItem", "Remove item") + '" title="' + i18n.t("removeItem", "Remove item") + '" />' +
							'</a>').find('textarea').focus();
	// create a new list item after the current where the option of adding another general delivery item is appended
	$j(div).parent().after(	'<li>' +
								'<label>' + i18n.t("core.deliverynote:itemNoOrder", { noOrder : genitemCount, defaultValue : "Item " + (genitemCount + 2) + ":"}) + '</label>' +
								'<div class="float-left width80 padtop">' +
									'<a href="#" onclick=" addItem($j(this).parent()); return false; ">' +
										'<img src="img/icons/add.png" height="16" width="16" alt="' + i18n.t("addItem", "Add item") + '" title="' + i18n.t("addItem", "Add item") + '" />' +
									'</a>' +
								'</div>' +
								'<!-- clear floats and restore page flow -->' +
								'<div class="clear"></div>' +
							'</li>');
	// initialise preset comment plugin
	$j(div).find('textarea.presetComments').presetCommentJQPlugin({ symbolBox: false });
	// add one to the general delivery item count
	genitemCount++;
}

/**
 * this method removes a general delivery item from the list and re-orders the list including item no's,
 * textarea name.
 * 
 * @param {Object} li the list item containing general delivery item to be removed
 */
function removeItem(li)
{
	// check item count to make sure there is at least one?
	if (genitemCount > 1)
	{
		// remove the chosen list item and item description
		$j(li).remove();
		// get all remaining list items
		$j('.generaldelitems ol li:not(:last)').each(function(i, n)
		{
			// re-order label text
			$j(this).find('label').text(i18n.t("core.deliverynote:itemNoOrder", {noOrder : i, defaultValue : "Item " + (i + 1) + ":"}));
			// re-order input name and id attributes
			$j(this).find('textarea').attr('name', 'itemlist' + i);
		});
		// subtract one from general delivery item count
		genitemCount--;
	}	
}