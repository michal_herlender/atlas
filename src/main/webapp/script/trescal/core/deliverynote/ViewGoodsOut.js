/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	ViewGoodsOut.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			: 	27/01/2009 - JV - Created file
*
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{
	// trigger onclick event of business company input
	$j('input[type="radio"][name="businessCompanySelection"]:first').trigger('onclick');
	var addressid = $j('th.opts:first').attr("addressid");
	showAllInSub(addressid);
}


/**
 * this method shows all collections and deliveries options for a subdivision
 * 
 * @param {Integer} subdivId the id of the subdivision we want to show
 */
function showAllInSub(subdivId)
{
	// remove selected class from all table cells with id beginning with 'opt'
	$j('table#despatchOptions tbody tr td[id^="opt"]').removeClass('selected');
	// add 'selected' class to all table cells relating to subdivision
	$j('table#despatchOptions tbody tr td[id^="opt' + subdivId + '"]').addClass('selected');
	// hide all collection and delivery option divs
	$j('div[id^="divBusSub"].vis').removeClass('vis').addClass('hid');
	// hide all tables within collection and delivery option divs
	$j('div[id^="divBusSub"] table.vis').removeClass('vis').addClass('hid');
	// show the selected delivery and collection option div and tables within
	$j('div#divBusSub-' + subdivId).removeClass('hid').addClass('vis').find('table').removeClass('hid').addClass('vis');
}

/**
 * this method shows a single collection and delivery option
 * 
 * @param {Integer} optId the id of the collection and delivery option we want to show
 * @param {Object} subdivId the id of the subdivision the collection and delivery option is within
 */
function showSingleOption(optId, subdivId)
{
	// remove selected class from all table cells with id beginning with 'opt'
	$j('table#despatchOptions tbody tr td[id^="opt"]').removeClass('selected');
	// add 'selected' class to table cell relating to delivery and collection option
	$j('table#despatchOptions tbody tr td#opt-' + subdivId + '-' + optId).addClass('selected');
	// hide all collection and delivery option divs
	$j('div[id^="divBusSub"].vis').removeClass('vis').addClass('hid');
	// hide all tables within collection and delivery option divs
	$j('div[id^="divBusSub"] table.vis').removeClass('vis').addClass('hid');
	// show the selected delivery and collection option div
	$j('div#divBusSub-' + subdivId).removeClass('hid').addClass('vis');
	// show the selected delivery and collection option table
	$j('table#tableOpt-' + subdivId + '-' + optId).removeClass('hid').addClass('vis');
}

/**
 * this method highlights table cells for items in different state groups
 * 
 * @param {Object} checkbox the checkbox object that has been selected/de-selected
 */
function highlightBox(checkbox)
{
	// checkbox checked?
	if($j(checkbox).is(':checked'))
	{
		// add colour class to table cell
		$j('td.' + $j(checkbox).attr('name')).addClass($j(checkbox).attr('name') + 'style');
	}
	else
	{
		// remove colour class to table cell
		$j('td.' + $j(checkbox).attr('name')).removeClass($j(checkbox).attr('name') + 'style');		
	}
}

/**
 * this method highlights table cells for items at different locations
 * 
 * @param {Object} radio the radio input object that has been selected/de-selected
 */
function highlightLocationBox(radio)
{
	// radio checked?
	if($j(radio).is(':checked'))
	{
		// add colour class to table cell
		$j('td[class*="comp-"] > div.businessCompany').addClass('notvis');
		$j('td.' + $j(radio).attr('class') + ' > div.businessCompany').removeClass('notvis');		
	}
}

function testing(addrId)
{	
	jobservice.getJobsAndItemsNotInStateGroupForAddress(stateGroupArray, addrId,
	{
		callback:function(result)
		{
			$j.prompt(DWRUtil.toDescriptiveString(result, 5));	
		}
	});
}
