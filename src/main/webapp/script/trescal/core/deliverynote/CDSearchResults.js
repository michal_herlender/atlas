/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: Jamie Vinall
*
*	FILENAME		:	CDSearchResults.js
*	DESCRIPTION		:	This javascript file is used on the cdsearchresults.vm page.
*					: 	
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	02/07/2007 - JV - Created File
* 					:	10/10/2007 - SH - JQuery functions and comment
*					:	07/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'dwr/interface/cdservice.js' );     						

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * this variable contains the page name which should be used when creating the search again
 * link in the header section
 */
var searchAgain = 'cdsearch.htm';

/**
 * this function retrieves all deliveries linked to a courier despatch id supplied using dwr,
 * if deliveries have already been retrieved then no DWR call is made.
 * 
 * @param {Event} event is a window event used to cancel any event bubbling
 * @param {Integer} rowcount is an integer value of the row for which deliveries should be found and displayed
 * @param {Integer} id is a courier despatch id
 */
function getDeliveries(event, rowcount, id)
{
	// cancel event bubbling
	event.cancelBubble = true;
	
	// deliveries for this courier despatch row have already been retrieved
	if ($j('#child' + rowcount).length)
	{
		// deliveries are currently hidden so make them visible
		if ($j('#child' + rowcount).css('display') == 'none')
		{
			// remove image to show deliveries
			$j('#deliveryLink' + rowcount).empty();
			// append image to hide deliveries
			$j('#deliveryLink' + rowcount).append('<img src="img/icons/deliveries_hide.png" width="16" height="16" alt="' + i18n.t("core.deliverynote:hideDeliveries", "Hide deliveries") + '" title="' + i18n.t("core.deliverynote:hideDeliveries", "Hide deliveries") + '" class="image_inline" />');
			// make the deliveries row visible
			$j('#child' + rowcount).css({ display: '', visibility: 'visible' });
			// add effect to slide down deliveries
			$j('#child' + rowcount + ' div').slideDown(1000);
		}
		// deliveries are currently visible so hide them
		else
		{
			// remove image to hide deliveries
			$j('#deliveryLink' + rowcount).empty();
			// append image to show deliveries
			$j('#deliveryLink' + rowcount).append('<img src="img/icons/deliveries.png" width="16" height="16" alt="' + i18n.t("core.deliverynote:showDeliveries", "Show deliveries") + '" title="' + i18n.t("core.deliverynote:showDeliveries", "Show deliveries") + '" class="image_inline" />');
			// add effect to slide up deliveries and hide row
			$j('#child' + rowcount + ' div').slideUp(1000, function(){
				$j('#child' + rowcount).css({ display: 'none', visibility: '' });
			});
		}
	}
	// deliveries for this courier despatch row have not been retrieved so call DWR
	else
	{
		cdservice.getDeliveriesForCourierDespatch(id, 
		{
			callback:function(dataFromServer)
			{
				var currentTime = new Date().getTime();
				displayDeliveries(dataFromServer, currentTime, rowcount);
			}
		});
	}
}

/**
 * time value of last DWR call, checked against the current DWR call
 */
var lastUpdate = 0;

/**
 * this function displays the deliveries retrieved by @method getDeliveries()
 * 
 * @param {Array} deliveries is an array of delivery objects
 * @param {Date/Time} currentUpdate is a date/time value of the current search
 * @param {Integer} rowcount is an integer value of the row for which deliveries should be found and displayed
 */
function displayDeliveries(deliveries, currentUpdate, rowcount)
{
	// this is a new search
	if (currentUpdate > lastUpdate)
	{
		// update lastUpdate variable
		lastUpdate = currentUpdate;
		
		// work out the class to be used for child row dependant on rowcount (odd, even)
		var classname = 'odd';
		if(rowcount % 2 == 0)
		{
			classname = 'even';
		}
		// if the parent row is currently highlighted with hoverrow then add to the child
		if ($j('#parent' + rowcount).attr('class') == 'hoverrow')
		{
			classname = 'hoverrow';
			/**
			 * this variable is used in the @see (SearchResultsKeyNav.js) file to flag that a row has a child
			 * and that it is currently highlighted with the 'hoverrow' class name
			 */  
			hasChild = true;
		}
		
		// deliveries length is greater than zero
		if(deliveries.length > 0)
		{
			// append a new child table row below the parent for this courier despatch where deliveries will be listed
			$j('#parent' + rowcount).after(	'<tr id="child' + rowcount + '" class="' + classname + '">' +
												'<td colspan="5">' +
													// table of deliveries added below
												'</td>' +
											'</tr>');
								
			// append div containing a table for the deliveries to be listed in to the new child table row
			$j('#child' + rowcount + ' td').append(	'<div style=" display: none; ">' +
														'<table class="child_table" summary="This table lists deliveries for a certain courier despatch">' +
															'<thead>' +
																'<tr>' +
																	'<td colspan="4">' +
																		// Text filled here in separate code below
																	'</td>' +
																'</tr>' +
																'<tr>' +
																	'<th scope="col" class="cddelno">' + i18n.t("core.deliverynote:deliveryNo", "Delivery No") + '</th>' +  
																	'<th scope="col" class="cddeljob">' + i18n.t("jobNo", "Job No") + '</th>' +
																	'<th scope="col" class="cddelcomp">' + i18n.t("company", "Company") + '</th>' +							
																'</tr>' +
															'</thead>' +
															'<tfoot>' +
																'<tr>' +
																	'<td colspan="4">&nbsp;</td>' +
																'</tr>' +
															'</tfoot>' +
															'<tbody>' +
																// rows are populated in separate code below
															'</tbody>' +
														'</table>' +
													'</div>');
													
			// depending on the amount of deliveries returned, display message in the thead section					
			if (deliveries.length == 1)
			{
				$j('#child' + rowcount + ' thead tr:first td').append(i18n.t("core.deliverynote:oneDelivery", {number : deliveries.length, defaultValue : "There is " + deliveries.length + " delivery for this courier despatch"}));
			}
			else
			{
				$j('#child' + rowcount + ' thead tr:first td').append(i18n.t("core.deliverynote:severalDeliveries", {number : deliveries.length, defaultValue : "There are " + deliveries.length + " deliveries for this courier despatch"}));
			}
											
			
			// add a row to the deliveries table for each delivery returned
			$j.each(deliveries, function(i){
				
				var jobno = '';
				if (deliveries[i].job)
				{
					jobno = deliveries[i].job.jobno;
				}
				else
				{
					jobno = '-- ' + i18n.t("N/A", "N/A") + ' --';
				}
				$j('#child' + rowcount + ' tbody').append(	'<tr>' +
																'<td>' + deliveries[i].deliveryno + '</td>' +
																'<td>' + jobno + '</td>' +
																'<td>' + deliveries[i].contact.sub.comp.coname + '</td>' +
															'</tr>');																		
			});
						
			// remove image to show deliveries
			$j('#deliveryLink' + rowcount).empty();
			// append image to hide deliveries
			$j('#deliveryLink' + rowcount).append('<img src="img/icons/deliveries_hide.png" width="16" height="16" alt="' + i18n.t("core.deliverynote:hideDeliveries", "Hide deliveries") + '" title="' + i18n.t("core.deliverynote:hideDeliveries", "Hide deliveries") + '" class="image_inline" />');
			// add effect to slide down deliveries
			$j('#child' + rowcount + ' div').slideDown(1000);
		}
	}
}