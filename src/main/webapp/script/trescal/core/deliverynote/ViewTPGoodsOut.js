/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	ViewTPGoodsOut.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			: 	02/06/2009 - JV - Created file
*
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * this method highlights table cells for items in different state groups
 * 
 * @param {Object} checkbox the checkbox object that has been selected/de-selected
 */
function highlightBox(checkbox)
{
	// checkbox checked?
	if($j(checkbox).is(':checked'))
	{
		// add colour class to table cell
		$j('td.' + $j(checkbox).attr('name')).addClass($j(checkbox).attr('name') + 'style');
	}
	else
	{
		// remove colour class to table cell
		$j('td.' + $j(checkbox).attr('name')).removeClass($j(checkbox).attr('name') + 'style');		
	}
}