/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: Jamie Vinall
*
*	FILENAME		:	AssignSchedule.js
*	DESCRIPTION		:	This javascript file is used on the assignschedule.vm page.
*					: 	
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	15/08/2007 - JV - Created File
*					:   25/12/2020 - LM - Updated File
*
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/** 
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = 	new Array(	{ anchor: 'existingsched-link', block: 'existingsched-tab' },
								{ anchor: 'newsched-link', block: 'newsched-tab' } );

/**
 * array of calendar objects containing the initial setup literals needed
 * 
 * @param {String} inputField is the id of the input field to which the calendar should be binded
 * @param {String} dateFormat is the format that the date should be displayed
 * @param {String} displayDates 'all' make all dates available
 * 								'future' show only dates in the future
 * 								'past' show only dates in the past
 * applied in init() function
 */				
//var jqCalendar = new Array ( { 
//							 inputField: 'scheduleDate',
//							 dateFormat: 'dd.mm.yy',
//							 displayDates: 'future'
//							 } );

function init()
{
	if($j('#entryDate')[0].checked == true){
		$j('#scheduleDate').addClass('vis').removeClass('hid');
	}
	
}

function showDateEntry() {
	$j('#scheduleDate').addClass('vis').removeClass('hid');
}

function hideDateEntry() {
	$j('#scheduleDate').addClass('hid').removeClass('vis');
}

function getScheduleDetails(subdivid) {
	
	timeout = setTimeout(function() {
		$j("#transportOptionIn").removeClass('vis').addClass('hid');
		$j("#transportOptionOut").removeClass('vis').addClass('hid');
		$j('#transportOptionId option[value=""]').attr('selected','selected');
		// get the address id
		var addrid = $j('select#addrid').val();
		if(addrid) {	
			$j.ajax({
				url: "getScheduleResult.json",
				data: {
					addrid: addrid,
					subdivid: subdivid
				},
				async: true
			}).done(function(result) {
				if( result.transportOptionInId){
					$j("#transportOptionIn").removeClass('hid').addClass('vis');
					$j("#transportOptionInName").text(result.transportOptionInName);
				} else {
					$j("#transportOptionIn").removeClass('vis').addClass('hid');
				}
				
				if( result.transportOptionOutId) {
					$j("#transportOptionOut").removeClass('hid').addClass('vis');
					$j("#transportOptionOutName").text(result.transportOptionOutName);
				} else {
					$j("#transportOptionOut").removeClass('vis').addClass('hid');
				}
				
				if(result.transportOptionOutId && result.transportOptionInId && result.transportOptionInId === result.transportOptionOutId){
					$j('#transportOptionId option[value='+ result.transportOptionInId +']').attr('selected','selected');
				}
										
			});		
		}		
	}, 500);

}
