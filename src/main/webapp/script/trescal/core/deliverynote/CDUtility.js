/****************************************************************************************************************************************
 *												CWMS (Calibration Workflow Management System)
 *			
 *												Copyright 2006, Antech Calibration Services
 *															www.antech.org.uk
 *		
 *															All rights reserved
 *
 *														Document author: Stuart Harrold
 *
 *	FILENAME		:	CDUtility.js
 *	DESCRIPTION		:	This javascript file is imported into multiple page specific javascript files such as
 * 					: 	DelNoteSummary.js, CreateDelNote.js, CreateGenDelNote.js
 *					: 	
 *	DEPENDENCIES	:   
 *
 *	TO-DO			: 	-
 *	KNOWN ISSUES	:	-
 *	HISTORY			:	08/10/2007 - SH - Created file
 * 					: 	08/10/2007 - SH - JQuery functions and comment
 *					:	07/10/2015 - TProvost - Manage i18n
 ****************************************************************************************************************************************/

/**
 * todays date in (dd.mm.yyyy) format
 */
var deldate = formatDate(new Date(), 'dd.MM.yyyy');

/**
 * this function changes the courier despatch types when a different courier is chosen from the @see (selectcourier)
 * select box.
 * 
 * @param {Integer} courierid is the id of the courier who we need to get courier despatch types for. 
 */
function changeTypes(courierindex) {
	// create variable for options
	var options = '';
	if (courierindex != undefined) {

		var types = couriers[courierindex].cdts;
		// append options to the select box for each despatch type returned
		$j.each(types, function(i) {
			options += '<option value="' + types[i].id + '"';
			if (types[i].defaultForCourier == true) {
				options += ' selected="selected"';
			}
			options += '>' + types[i].name + '</option>';
		});
	} else
		options = '<option selected="selected"> -- N/A -- </option>';
	// empty the despatch type select box and append new options
	$j('#selecttype').html(options);

}

/**
 * this function changes the options available when the user is assigning a courier despatch depending on the option chosen
 * 
 * @param {String} option is the value chosen by user (existing depatch, new despatch or default for clearing despatch options)
 * @param {Date} date value in format 'dd/mm/yyyy' to pre-populate the date field when creating a new courier despatch
 */
function changeDespatchOption(option, date, orgid) {
	// user has chosen to view existing courier despatches created over last week
	if (option == 'exists') {
		// remove all but first and last list items from list to clear
		$j('#cdOptions li:not(:first):not(:last)').remove('li');
		// load the service javascript file if not already	
		updateExistingCDSelect(existingCDs, orgid);

	}
	// user has chosen to create a new courier despatch
	else if (option == 'new') {
		// remove all but first and last list items from list to clear
		$j('#cdOptions li:not(:first):not(:last)').remove('li');

		// append list item containing label and input field for despatch date in list
		$j('#cdOptions li:first')
				.after(
						'<li>'
								+ '<label>'
								+ i18n.t("core.deliverynote:consigNo",
										"Consignment No:")
								+ '</label>'
								+ '<input type="text" name="consigno" id="consigno" />'
								+ '</li>'
								+ '<li id="cdDesDate">'
								+ '<label>'
								+ i18n.t("core.deliverynote:despatchDate",
										"Despatch Date:")
								+ '</label>'
								+ '<input type="date" value="'
								+ date
								+ '" id="despatchdate" name="despatchdate"/>'
								+ '</li>');

		if (orgid != null) {

			// create variable for options
			var options = '';
			// create variable for default courier id, but set to id 1 in case there is no default
			var defCourierIndex = -1;
			// append options to the select box for each courier returned
			$j.each(couriers, function(i) {
				options += '<option value="' + i + '" data-index="' + i + '"';
				if (defCourierIndex == -1 && couriers[i].defaultCourier == true) {
					options += ' selected="selected"';
					defCourierIndex = i;
				}
				options += '>' + couriers[i].name + '</option>';
			});

			if (options === '')
				options = '<option selected="selected"> -- N/A -- </option>';

			// append list item containing label and select box for selecting courier in list
			$j('#cdOptions li#cdDesDate')
					.after(
							'<li id="cdSelCourier">'
									+ '<label>'
									+ i18n.t("core.deliverynote:courier",
											"Courier:")
									+ '</label>'
									+ '<select id="selectcourier" name="selectcourier" onchange=" changeTypes($j(\'#selectcourier option:selected\').data(\'index\')); return false; ">'
									+ options + '</select>' + '</li>');
			// append list item containing label and select box for selecting despatch type in list
			$j('#cdOptions li#cdSelCourier').after(
					'<li id="cdSelType">'
							+ '<label>'
							+ i18n.t("core.deliverynote:despatchType",
									"Despatch Type:") + '</label>'
							+ '<select id="selecttype" name="selecttype">' +
							// add a blank option for initialisation purposes otherwise IE creates select about 5mm wide
							'<option value=""></option>' + '</select>'
							+ '</li>');
			// call function to get the despatch types for the first courier returned
			if (defCourierIndex != -1)
				changeTypes(defCourierIndex);

		} else {

			// create variable for options
			var options = '';
			// create variable for default courier id, but set to id 1 in case there is no default
			var defCourierIndex = -1;
			// append options to the select box for each courier returned
			$j.each(couriers, function(i) {
				options += '<option value="' + couriers[i].courierid
						+ '" data-index="' + i + '"';
				if (defCourierIndex == -1 && couriers[i].defaultCourier == true) {
					options += ' selected="selected"';
					defCourierIndex = i;
				}
				options += '>' + couriers[i].name + '</option>';
			});
			// append list item containing label and select box for selecting courier in list
			$j('#cdOptions li#cdDesDate')
					.after(
							'<li id="cdSelCourier">'
									+ '<label>'
									+ i18n.t("core.deliverynote:courier",
											"Courier:")
									+ '</label>'
									+ '<select id="selectcourier" name="selectcourier" onchange=" changeTypes($j(\'#selectcourier option:selected\').data(\'index\')); return false; ">'
									+ options + '</select>' + '</li>');
			// append list item containing label and select box for selecting despatch type in list
			$j('#cdOptions li#cdSelCourier').after(
					'<li id="cdSelType">'
							+ '<label>'
							+ i18n.t("core.deliverynote:despatchType",
									"Despatch Type:") + '</label>'
							+ '<select id="selecttype" name="selecttype">' +
							// add a blank option for initialisation purposes otherwise IE creates select about 5mm wide
							'<option value=""></option>' + '</select>'
							+ '</li>');
			// call function to get the despatch types for the first courier returned
			if (defCourierIndex != -1)
				changeTypes(defCourierIndex);

		}

	}
	// user has chosen the default option and so list should be returned to its original state
	else {
		// remove all but first and last list items from list to clear
		$j('#cdOptions li:not(:first):not(:last)').remove('li');
	}
}

/**
 * this function updates the existing courier despatch options in @see (selectdespatch) select box.
 * if no options are available then section for creating a new courier depatch is assembled
 * 
 * @param {Array} existingCDs is an array of existing courier depatch objects created over last week
 */
function updateExistingCDSelect(existingCDs, orgid) {
	// existingCDs contains courier despatches
	if (existingCDs.length > 0) {
		// create variable for options
		var options = '';
		// append options to select box for each existing courier despatch
		$j.each(existingCDs, function(i) {
			options += '<option value="' + existingCDs[i].id + '">'
					+ existingCDs[i].consignmentno + ' - '
					+ existingCDs[i].cdTypeDescription + ' - '
					+ cwms.dateFormatFunction(existingCDs[i].despatchDate) + '</option>';
		});
		// append new list item containing label and select box to list
		$j('#cdOptions li:first').after(
				'<li>'
						+ '<label>'
						+ i18n.t("core.deliverynote:selectDespatch",
								"Select Despatch:") + '</label>'
						+ '<select id="selectdespatch" name="selectdespatch">'
						+ options + '</select>' + '</li>');
	}
	// no existing courier despatches were present so create new courier despatch section 
	else {
		changeDespatchOption('new', deldate, orgid);
	}
}
