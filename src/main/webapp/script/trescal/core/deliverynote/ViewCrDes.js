/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: Jamie Vinall
*
*	FILENAME		:	ViewCrDes.js
*	DESCRIPTION		:	This javascript file is used on the viewcrdes.vm page.
*					: 	
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	27/07/2007 - JV - Created File
* 					:	09/07/2007 - SH - JQuery modifications made.
*
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array ( 'script/trescal/core/deliverynote/ViewDIUtility.js',
							  'dwr/interface/courierservice.js' );
								

/**
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = 	new Array(	{ anchor: 'deliveries-link', block: 'deliveries-tab' },
								{ anchor: 'editcrdes-link', block: 'editcrdes-tab' } );


/**
 * this function changes the courier despatch types when a different courier is chosen from the @see (selectcourier)
 * select box.
 * 
 * @param {Integer} courierid is the id of the courier who we need to get courier despatch types for. 
 */
function changeTypes(courierid)
{
	$j.ajax({
		url: "courierdespatchtypes.json",
		data: {
			courierId: courierid
		},
		async: true
	}).done(function(types) {
		// empty the despatch type select box
		$j('#cdTypeId').empty();
		// default selected boolean
		var defaultSelected = false;
		// append options to the select box for each despatch type returned
		$j.each(types, function(i){
			var selected = '';
			if (types[i].defaultForCourier) {
				selected = ' selected="selected"';
				defaultSelected = true;
			}
			$j('#cdTypeId').append('<option value="' + types[i].id + '" ' + selected + '>' + types[i].name + '</option>');
		});
		if(!defaultSelected) {
			// set the first option in select box to 'selected'
			$j('#cdTypeId option:first').attr('selected', 'selected');
		}
	}).fail(function(error) {
		$j.prompt(error.message);
	});
}