/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: Jamie Vinall
*
*	FILENAME		:	GenDelNoteSummary.js
*	DESCRIPTION		:	This javascript file is used on the gendelnotesummary.vm page.
*					: 	
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	07/07/2010 - SH - Created File
*					:	07/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'dwr/interface/delitemservice.js',
								'dwr/interface/deliveryservice.js',
								'script/trescal/core/tools/labelprinting/dymo/DymoLabelFunctions.js',	
								'script/trescal/core/tools/FileBrowserPlugin.js',
								'script/trescal/core/deliverynote/CDUtility.js',
								'script/trescal/core/utilities/DocGenPopup.js' );
								
/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' },
								{ element: 'ul.subnavtab a', corner: 'small transparent top' } );
								
/** 
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = 	new Array(	{ anchor: 'deliveryitems-link', block: 'deliveryitems-tab' },
								{ anchor: 'despatch-link', block: 'despatch-tab' },
								{ anchor: 'editdelivery-link', block: 'editdelivery-tab' },
								{ anchor: 'files-link', block: 'files-tab' },
								{ anchor: 'emails-link', block: 'emails-tab' } );

/**
 * this variable is used to indicate whether the delivery note contains despatched
 * items
 */
var containsDespatchedItems = false;
							
/**
 * this function checks whether the item has been despatched and if it has displays a confirmation box to the
 * user. Depending on the response the action will either be cancelled or @see (performDelete()) for the deletion of item.
 * 
 * @param {Integer} deliveryid is the id of the delivery note we are viewing
 * @param {Integer} itemid is the id of the delivery item to be removed from the delivery note
 * @param {Boolean} despatched is a boolean value indicating whether the item has already been delivered
 */
function deleteItem(deliveryid, itemid, despatched)
{	
	if(despatched) 
	{
		var confirmation = confirm(i18n.t("core.deliverynote:confirmDeleteItem", "This item has been despatched. Click 'OK' to delete the item anyway or 'Cancel' to cancel the deletion."));
		
		if(confirmation)
		{
			performDelete(deliveryid, itemid);
		}
	}
	else
	{
		performDelete(deliveryid, itemid);	
	}
}

/**
 * this function deletes delivery items from the delivery. The item is also removed from the delivery items table on the page.
 *
 * @param {Integer} deliveryid is the id of the delivery note we are viewing
 * @param {Integer} itemid is the id of the delivery item to be removed from the delivery note 
 */
function performDelete(deliveryid, itemid) 
{
	// make ajax call to delete item from database
	delitemservice.deleteDeliveryItemById(itemid,
	{
		callback:function(result)
		{
			// remove delivery item from page
			$j('tr#item' + itemid).remove();
			// re-number delivery items
			$j('#deliveryitems table tbody tr[id^="item"]').each(function(i)
			{
				// re-number all delivery items
				$j(this).find('td:first').text((i + 1));
			});
			// update the delivery item counts
			$j('span.delitemCount').text(parseInt($j('span.delitemCount:first').text()) - 1);
			// check if all delivery items have been deleted?
			if (parseInt($j('span.delitemCount:first').text()) < 1)
			{
				$j('#deliveryitems table tbody').append('<tr class="odd">' +
						'<td colspan="4" class="center bold">' + i18n.t("core.deliverynote:noDeliveryItems", "There are no delivery items to display") + '</td>' +
				'</tr>');
			}
			// is the add delivery item link hidden?
			if($j('#addDILink').attr('class') == 'hid')
			{
				// show link
				$j('#addDILink').removeClass().addClass('vis');
			}
			// add zebra colour effect back to table
			$j('#deliveryitems table tbody tr[id^="item"]:odd').removeClass().addClass('odd');
			$j('#deliveryitems table tbody tr[id^="item"]:even').removeClass().addClass('even');
		}
	});	
	// make ajax call to set containsDespatchedItems value
	deliveryservice.containsDespatchedItems(deliveryid,
	{
		callback:function(result)
		{
			containsDespatchedItems = result;
		}
	});
}

/**
 * this function checks if the delivery contains despatched items and then displays a confirmation box to the user.
 * depending on their response the action will either be cancelled or the delivery note will be deleted
 * 
 * @param {String} link is a string containing an href location
 */
function deleteDeliveryNote(link) 
{
	if(containsDespatchedItems)
	{
		var confirmation = confirm(i18n.t("core.deliverynote:confirmDeleteDeliveryNote", "This delivery note contains items that have been despatched. Click 'OK' to delete the delivery note anyway or 'Cancel' to cancel the deletion."));

		if(confirmation)
		{
			window.location.href = link;
		}
	}
	else
	{
		window.location.href = link;
	}
}

function addrChange(addrId, defLocId)
{
	$j.ajax({
		url: "addrChange.json",
		data: {
			addrId: addrId
		},
		async: true
	}).done(function(locations) {
		
		// remove all options from select
		$j('select#locid').empty();
		// add options from locations list
		$j.each(locations, function(i) {
		     $j('select#locid')
		         .append($j('<option value='+ locations[i].locationid +'>'+ locations[i].location +'</option>'));
		});
		
		if (locations.length < 1) 
		{
			$j('li#delLoc').hide();
		}
		
		else
		{
			if(defLocId != 0)
			{
				$j('select#locid option[value=' + defLocId + ']').attr('selected','selected');
			}
			$j('li#delLoc').show();
		}
	});	
}