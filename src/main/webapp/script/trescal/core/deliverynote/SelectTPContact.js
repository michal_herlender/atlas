/****************************************************************************************************************************************
 *												CWMS (Calibration Workflow Management System)
 *			
 *												Copyright 2006, Antech Calibration Services
 *															www.antech.org.uk
 *		
 *															All rights reserved
 *
 *														Document author: 	Jamie Vinall
 *
 *	FILENAME		:	SelectTPContact.js
 *	DESCRIPTION		:	
 *	DEPENDENCIES	:   -
 *
 *	TO-DO			: 	-
 *	KNOWN ISSUES	:	-
 *	HISTORY			:	04/07/2007 - JV - Created File
 *
 ****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be
 * applied. applied in init() function.
 */
var niftyElements = new Array({
	element : 'div.infobox',
	corner : 'normal'
});

/**
 * element to be focused on when the page loads. applied in init() function.
 */
var focusElement = 'coname';

/**
 * select company role 
 * 		ALL: client, supplier and business 
 * 		CLIENT: client
 * 		BUSINESS: supplier, business
 */
function companyRole(role) {

	if (role == 'ALL') {
		document.getElementById("compCoroles").value = "client,supplier,business";
		$j("#coname").keyup();

	} else if (role == 'CLIENT') {
		document.getElementById("compCoroles").value = "client";
		$j("#coname").keyup();

	} else if (role == 'BUSINESS') {
		document.getElementById("compCoroles").value = "supplier,business";
		$j("#coname").keyup();
	}
}