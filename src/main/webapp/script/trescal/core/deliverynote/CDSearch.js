/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: Jamie Vinall
*
*	FILENAME		:	CDSearch.js
*	DESCRIPTION		:	This javascript file is used on the cdsearch.vm page.
*					: 	
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	02/07/2007 - JV - Created File
* 					:	10/10/2007 - SH - JQuery functions and comment
*
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'consignmentno';

/**
 * array of calendar objects containing the initial setup literals needed
 * 
 * @param {String} inputField is the id of the input field to which the calendar should be binded
 * @param {String} dateFormat is the format that the date should be displayed
 * @param {String} displayDates 'all' make all dates available
 * 								'future' show only dates in the future
 * 								'past' show only dates in the past
 * applied in init() function
 */				
//var jqCalendar = new Array ( { 
//							 inputField: 'date',
//							 dateFormat: 'dd.mm.yy',
//							 displayDates: 'all'
//							 } );        
        
function changeTypes(courierid) { 
	// not the first option in select box
	if (courierid != 0) { 
		// remove all options except the first from select box
		$j('#couriertypeid option:not(:first)').remove();
		$j.ajax({
			url: "courierdespatchtypes.json",
			data: {
				courierId: courierid
			},
			async: true
		}).done(function(types) {
			// append an option to courier types select box for each courier despatch type returned
			$j.each(types, function(i) {
				$j('#couriertypeid').append('<option value="' + types[i].id + '">' + types[i].name + '</option>');
			});
			// set first option as selected
			$j('#couriertypeid option:first').attr('selected', 'selected');
		}).fail(function(error) {
			$j.prompt(error.message);
		});
	}
	else {
		// remove all options except the first from select box
		$j('#couriertypeid option:not(:first)').remove();
	}
}

/**
 * this function changes the corole value stored in the 'compCoroles' hidden input field
 * when changed the text field and search list are emptied ready for a search using new corole
 */
function changeCorole(value)
{
	// clear company search ready for new corole
	$j('div.compSearchJQPlugin').after(	'<div class="compSearchJQPlugin"></div>').remove();
	// change corole in hidden input
	$j('input[name="compCoroles"]').val(value);
	// initialise company search again
	$j('div.compSearchJQPlugin').compSearchJQPlugin({	field: 'coid',
														tabIndex: 6,
														compCoroles: value});
	// add focus to input
	$j('input[name="comptext"]').focus();
}