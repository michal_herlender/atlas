/****************************************************************************************************************************************
*												
CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: Jamie Vinall
*
*	FILENAME		:	CreateDelNote.js
*	DESCRIPTION		:	This javascript file is used on the createdelnote.vm page.
*					: 	
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	02/07/2007 - JV - Created File
*					:	07/10/2015 - TProvost - Manage i18n
*					:	08/10/2015 - TProvost - Changed jquery-antechplugins.js To jquery.antechplugins.js
*					:	08/10/2015 - TProvost - Fixes bug for shown/hide Del Note
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */

var table;

var pageImports = new Array ('script/trescal/core/deliverynote/CDUtility.js',
								'script/thirdparty/jQuery/jquery.antechplugins.js',
								'script/thirdparty/jQuery/jquery.dataTables.min.js');

/**
 * array of objects containing the element name and corner type which should be
 * applied. applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * array of calendar objects containing the initial setup literals needed
 * 
 * @param {String}
 *            inputField is the id of the input field to which the calendar
 *            should be binded
 * @param {String}
 *            dateFormat is the format that the date should be displayed
 * @param {String}
 *            displayDates 'all' make all dates available 'future' show only
 *            dates in the future 'past' show only dates in the past applied in
 *            init() function
 */							
//var jqCalendar = new Array ( { 
//							 inputField: 'deliverydate',
//							 dateFormat: 'yy-mm-dd',
//							 displayDates: 'all'
//							 } );

function init(){
	var url = new URL(top.location);
	var jobId = url.searchParams.get('jobid');
	var jobItemId =url.searchParams.get('jobitemid');
	if(jobId==null){
		var url2= new URL(document.getElementById('linkToJob').href);
		jobId= url2.searchParams.get('jobid');
	}
	var destination=url.searchParams.get('destination');
	initTable(jobId, jobItemId, destination);
	
	addrChange($j("#addressid").val());
}

function cascadeAddressSearchCallback()
{
	addrChange($j("#addrid").val());
}

function onSubmit(jobId, deliveryType) {
	var data = {};
	var accessories=[];
	var freeText=[];

	/* collect all data from input fields */
	var formArray = $j('#delnoteform').serializeArray();
    formArray.forEach(function(i) {
    	data[i.name] = i.value;
    });
    $j('#itemTable').DataTable().$('input.accessories[type=checkbox]').each(function () {
        if (this.checked)
        	accessories.push($j(this).val());
    });
    $j('#itemTable').DataTable().$('input.freeText[type=checkbox]').each(function () {
        if (this.checked)
        	freeText.push($j(this).val());
    });
    
    /* get the data of Courier Despatch */
    var despatchDate= $j("#despatchdate").val();
    var selectType= $j("#selecttype :selected").val();
    var consignmentno =document.getElementById("consigno");
    var selectdespatch= $j("#selectdespatch :selected").val();
    
    if(despatchDate && selectType ){
    	data["despatchDate"] = despatchDate;
    	data["selectType"] = selectType;
    }

    if(selectdespatch){
    	data["selectdespatch"] = selectdespatch;
    }
    if(consignmentno){
    	data["consignmentno"] = consignmentno.value;
    }
    /* add list of selected job items */
	var selectedItems = $j('#itemTable').DataTable().$('input.jobItem[type=checkbox]').serializeArray().map(i => i.value);
    data["items"] = selectedItems;
    data["accessoryItems"] = accessories;
    data["jobId"] = jobId;
    data["deliveryType"]= deliveryType;
    data["accessoryFreeText"]= freeText;
    
    if(selectedItems.length>0){
    	document.getElementById("submitData").show();
		fetch("createDeliveryNote.json",{
			method:"POST",
			headers:{...window.cwms.metaDataObtainer.csrfHeader,
						"Content-Type":"application/json; charset=utf-8"
			},
			body:JSON.stringify(data)
		}).then(res => res.ok? res.json():Promise.reject(res.statusText))
		.then(
    	(result) => {
    		if(result.success){
    			window.location.href = result.message;
    			document.getElementById("submitData").hide();
    		}else{
    			var errors=[];
    			result.errors.forEach(function(element) {
    				errors.push(element.message+" \n");
    			});
    			document.getElementById("submitData").hide();
    			alert(result.message +" \n"+errors);
    		}
    	})
		.catch( console.error);
	
    } else 
    	alert (i18n.t("core.deliverynote:noitem", "At least one item must be on the delivery note."));
}

/**
 * this method gets all locations for an address
 * 
 * @param {Integer}
 *            addrId the id of the address to get locations for
 * @param {Integer}
 *            defLocId
 */
function addrChange(addrId, defLocId)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/locationservice.js'), true,
	{
		callback: function()
		{
			// dwr service to get all address locations
			locationservice.getAllAddressLocations(addrId, true, 
			{
				callback:function(locations)
				{			
					dwr.util.removeAllOptions('locid');
					dwr.util.addOptions('locid', locations, 'locationid', 'location');
					
					if (locations.length < 1) 
					{
						$j('li#delLoc').hide();
					}
					else
					{
						if(defLocId != 0)
						{
							$j('select#locid option[value=' + defLocId + ']').attr('selected','selected');
						}
						$j('li#delLoc').show();
					}
				}	
			});
		}
	});
	
}

function switchCompany(){
	console.log("here")
	$j("#companyname, #contactselector, #addressselector, #alternatcompanyselector").toggleClass( "vis").toggleClass( "hid" );
	
}

/**
 * this method updates the show instruction on third party delivery note boolean
 * field
 * 
 * @param {Integer}
 *            instructionId is the id of the instruction to update
 * @param {Boolean}
 *            showOnTPDN is the boolean value currently assigned
 */
function updateShowOnTPDelNoteJIInst(instructionId, showOnTPDN)
{
	// swap boolean value of showOnTPDN
	showOnTPDN = (showOnTPDN) ? false : true;
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/tpinstructionservice.js'), true,
	{
		callback: function()
		{
			// dwr method to update the show instruction on third party delivery
			// note document field
			tpinstructionservice.updateTPInstructionShowOnTPDN(instructionId, showOnTPDN,
			{
				callback:function(results)
				{
					// update of boolean field successful?
					if(!results.success)
					{
						// display failure message
						$j.prompt(i18n.t("failureMessage", {errMsg : results.message, defaultValue : "Failed: " + results.message}));
					}
					else
					{
						// remove the old anchor and image from the table cell
						// and append new one
						$j('#showontpdn' + instructionId).empty().append('<a href="#" onclick=" updateShowOnTPDelNoteJIInst(' + instructionId + ', ' + showOnTPDN + '); return false; "></a>');
						// is the instruction to be shown on third party
						// delivery notes?
						if (showOnTPDN)														
						{
							// add tick image
							$j('#showontpdn' + instructionId + ' a').append('<img src="img/icons/bullet-tick.png" width="10" height="10" alt="' + i18n.t("core.deliverynote:showDelNote", "Show On TP Del Note") + '" title="' + i18n.t("core.deliverynote:showDelNote", "Show On TP Del Note") + '" />');
						}
						else
						{
							// add cross image
							$j('#showontpdn' + instructionId + ' a').append('<img src="img/icons/bullet-cross.png" width="10" height="10" alt="' + i18n.t("core.deliverynote:hideDelNote", "Hide On TP Del Note") + '" title="' + i18n.t("core.deliverynote:hideDelNote", "Hide On TP Del Note") + '" />');
						}
					}
				}
			});
		}
	});
}

function checkItem(cb, table, barcode, i) {
	if(!cb.checked) {
		var accs = table.row('#instrument'+barcode).node().cells[i];
		if(accs.firstElementChild) {
			$j('#accessoryWarning').removeClass('hid');
			// instrument has accessories
			var accString = '';
           // accs.each(function(i) {
           // if(i > 0) accString += ', ';
           // accString +=
			// $j(this).find('span.accessoryText').html().replace('\n','
           // ').trim();
           // });
			$j('p#accString').remove();
			$j('p#accWarningText').after('<p id="accString">' + accString + '</p>');
			$j('button#addWithAcc').unbind('click');
			$j('button#addWithAcc').click(function() {
				event.preventDefault();
				var c=accs.getElementsByTagName('input');
				for (var i = 0; i < c.length; i++) {
				        if (c[i].type == 'checkbox') {
				            c[i].checked = true;
				        }
				    }
				checkItemAndCreateHistory(cb, barcode);
				$j('#accessoryWarning').addClass('hid');
			});
			$j('button#rejectAcc').unbind('click');
			$j('button#rejectAcc').click(function() {
				event.preventDefault();
				rejectItemAndCreateHistory(barcode);
				$j('#accessoryWarning').addClass('hid');
				$j('#rejectWarning3').removeClass('hid');
			});
		}
		else checkItemAndCreateHistory(cb, barcode);
	}
}

function checkItemAndCreateHistory(cb, barcode) {
	cb.checked = true;
	$j('div#scanHistory').prepend('<div><img src="img/icons/greentick.png" width="16" height="16">' + barcode + '</div>');
	$j('#barcodeScan').val('');
	var successItems = parseInt($j('input#successCounter').val());
	successItems += 1;
	$j('input#successCounter').val(successItems);
	var unselectedItems = parseInt($j('input#unselectCounter').val());
	unselectedItems -= 1;
	$j('input#unselectCounter').val(unselectedItems);
	
	checkReturnToAddressAndContactCompatibility(cb);
}

function rejectItemAndCreateHistory(barcode) {
	$j('div#scanHistory').prepend('<div><img src="img/icons/redcross.png" width="16" height="16">' + barcode + '</div>');
	$j('#barcodeScan').val('');
	var rejectedItems = parseInt($j('input#rejectCounter').val());
	rejectedItems += 1;
	$j('input#rejectCounter').val(rejectedItems);
}

function updateInstructions() {
	var jobItemIds = [];
	$j('.jobItemCheck:checkbox:checked').each(function() {
		jobItemIds.push(parseInt($j(this).attr('name')
				.replace('.addToDeliveryNote','')
				.replace('[','')
				.replace('items','')
				.replace(']','')));
	});
	$j.ajax({
		url: 'deliveryNoteInstructions.json',
		type: 'GET',
		data: {
			jobItemIds: jobItemIds
		}
	}).done(function(instructions) {
		$j('#instructiontable > tbody').empty();
		instructions.forEach(function(instruction) {
			var tableRow = '';
			tableRow += '<tr><td>';
			if(instruction.linkedTo == 'SUBDIV')
				tableRow += '<img src="img/icons/house.png" width="24" height="16"/>';
			else if(instruction.linkedTo == 'COMPANY')
				tableRow += '<img src="img/icons/building.png" width="24" height="16"/>';
			tableRow += '<strong>' + instruction.type + ' : </strong>';
			tableRow += '<span class="' + instruction.color + ' bold">';
			tableRow += instruction.instruction;
			tableRow += '</span></td></tr>';
			$j('#instructiontable > tbody').append(tableRow);
		});
	});
}

function updateStatistics(cb) {
	var unselectedItems = parseInt($j('input#unselectCounter').val());
	if(cb.checked) unselectedItems -= 1;
	else unselectedItems += 1;
	$j('input#unselectCounter').val(unselectedItems);
	updateInstructions();
}


/**
 * this method filter Job Items that can be assigned to delivery and Items from
 * other jobs
 */
function initTable(jobId, jobItemId, destination) {
	if(jobItemId==null)
		jobItemId=0;
	$j.ajax({
		 url: "deliveryNoteItemsOnJob.json?jobId=" + jobId + "&destination="+ destination +"&jobItemId="+jobItemId,
		 type: "GET"
	 }).done(function(jobItemData) {
		table=$j('#itemTable').DataTable({
			 data : jobItemData,
			 createdRow: function( row, data, dataIndex ) {
			      $j(row).attr('id', 'instrument'+data.plantId);
			      
			      if(data.jobitemReturnToAddressId)
			    	  $j(row).attr('data-returntoaddressid', data.jobitemReturnToAddressId);
			      else
			    	  $j(row).attr('data-returntoaddressid', data.jobReturnToAddressId);
			      
			      if(data.jobitemReturnToContactId)
			      	$j(row).attr('data-returntocontactid', data.jobitemReturnToContactId);
			      else
			    	$j(row).attr('data-returntocontactid', data.jobReturnToContactId);
			      
			      if(data.jobId == jobId) {
			    	  $j(row).attr('style', 'background-color: #F0F0F6;');
			      } else {
			    	  $j(row).attr('style', 'background-color: #ffffff;');
			      }
			  },
			 columns : [
				 { data : {}, render : checkBoxRenderer, orderable : false },
				 { data : "index", visible : false },
				 { data : "jobNo" },
				 { data : "itemNo" },
				 { data : "jobItemPOs" },
				 { data : "jobClientRef" },
				 { data : "jobItemClientRef" },
				 { data : "itemAccessories", render: itemAccessoriesRenderer, sClass : "accessories" },
				 { data : "groupId" },
				 { data : "instrumentModelName" },
				 { data : "jobitemReturnToAddress" },
				 { data : "jobitemReturnToContact" },
				 { data : "instrumentContact" },
				 { data : "instrumentAddress" },
				 { data : "instrumentLocation" },
				 { data : "instrumentSerialNo" },
				 { data : "instrumentPlantNo" },
				 { data : "transportOut" }
			 ],
			 "order": [[ 1, "asc" ]],
			 "iDisplayLength": 100,
			 "columnDefs": [
			        {"className": "dt-center", "targets": "_all"},
			      ],
			 "oLanguage": {
				 "sInfo": i18n.t("core.deliverynote:showing", "Showing") + " _START_ "+ i18n.t("core.tools:cal.to", "to") + " _END_ " +
				 	i18n.t("common:of", "of") + " _TOTAL_ " + i18n.t("common:items", "items"),
				 "sselect": true,
				 "sLengthMenu": i18n.t("core.deliverynote:show", "Show") + " _MENU_ "+ i18n.t("core.deliverynote:entries", "entries"),
				 "oPaginate": {
					 "sFirst": i18n.t("core.deliverynote:first", "First"),
					 "sPrevious": i18n.t("core.deliverynote:previous", "Previous"),
					 "sNext": i18n.t("core.deliverynote:next", "Next"),
					 "sLast": i18n.t("core.deliverynote:last", "Last")
				 }
			 }
		 });
				
			var selectableCount = 0;
			for (i = 0; i < jobItemData.length; i++) {
				if (jobItemData[i].onBehalfOfName !=null && jobItemData[i].onBehalfOfId || (Array.isArray(jobItemData[i].tpInstructions) && jobItemData[i].tpInstructions.length) ){
					table.row(i).child($j(format(destination, jobId, jobItemData[i].onBehalfOfName, jobItemData[i].onBehalfOfId, jobItemData[i].tpInstructions ))).show();
				}
				if (jobItemData[i].readyForDelivery) selectableCount++;
			};
		    $j('input#unselectCounter').val(selectableCount);
			
		// select All
		 $j("#selectAllTab").on( "click", function(e) {
			 selectAllItems(this.checked, table);
		 });
		 
		// quick delnote item scan
		  $j('#barcodeScan').on( 'keyup', function(e) {
			  quickItemScan(table,$j(this).val());
		  });
					
		// Setup - add a text input to each footer cell
	    $j('#itemTable thead tr').clone(true).appendTo( '#itemTable thead' );
	    $j('#itemTable thead tr:eq(1) th').each( function (i) {
	    	if (i==0){
	    		{
		        	$j(this).html( ' <select>' +
	    		 	 		' <option value="">--'+i18n.t("core.deliverynote:all", "All")+'--</option>' +
	    		 	 		' <option>'+i18n.t("core.deliverynote:notready", "not ready")+'</option>'+
	    		 	 		' <option>'+i18n.t("core.deliverynote:ready", "ready!")+'</option>'+
	    		 	  	    ' </select>' );
	    	        $j('select', this ).on( 'keyup change', function () {
	    	            if ( table.column(i).search() !== this.value && this.value !== '--'+i18n.t("core.deliverynote:all", "All")+'--' ) 
	    	            	table
	    	                    .column(i)
	    	                    .search( this.value )
	    	                    .draw();
		                else
			            	table
	    	                    .column(i)
	    	                    .search('')
	    	                    .draw();    
	    	        } );
		        	}
	    	}
	    	else if (i==6){
		        	$j(this).html( ' <select>' +
	    		 	 		' <option>--'+i18n.t("core.deliverynote:all", "All")+'--</option>' +
	    		 	  	  ' </select>' );
	    	}
	    	else{
	    		var width = '50px';
	    		if(i==2)
	    			width ='10px';
	    		if(i==1 || i==9  || i== 11)
	    			width = '60px';
	    		if ($j(this).text() !== '--'+i18n.t("core.deliverynote:all", "All")+'--') {
	    			var select = $j('<input title="'+i18n.t("core.deliverynote:all", "All")+'" type="text" list="'+i+'" style="width: '+ width +'; margin: 0 auto;"/> <datalist id="'+i+'"><option value=""></option></datalist>')
	    					.on('click', function() {
	    						$j(this).val('');
	    						table.column( i+1 )
			            		     .search('')
			            		     .draw();
	    						})
		            	    .appendTo( $j(this).empty())
		            	    .on( 'keyup change', function () {
		            	    		table.column( i+1 )
				            		     .search(this.value)
				            		     .draw();
		            		});
						table.column( i+1 ).data().unique().sort().each( function ( d, j ) {
							if(d!=null){
								select.append( '<option value="'+d+'"></option>' );
							}
						});
			}
	    }
	    }); 
	 });
}

/**
 * this method checks all inputs of type checkbox that are contained within the
 * table
 * 
 */
function quickItemScan(table, barcode){
	if (event.keyCode === 13) {
        $j('#rejectWarning1').addClass('hid');
    	$j('#rejectWarning2').addClass('hid');
    	$j('#rejectWarning3').addClass('hid');
    	$j('#accessoryWarning').addClass('hid');
    	var cb=table.row('#instrument'+barcode).node();
    	if(!cb) {
    		$j('#rejectWarning1').removeClass('hid');
    		rejectItemAndCreateHistory(barcode);
    	}
    	else {
    		
    		if (cb){
    			var cbs=table.row('#instrument'+barcode).node().cells[0].firstElementChild;
    			var i=6;
    		}
    		var oneChecked =false;
    		var allChecked = true;
    			if(cbs.checked) oneChecked =true ;
    			else allChecked = false;
    		if(!oneChecked) {
    				checkItem(cbs, table, barcode, i);
    		}
    		else if(allChecked) {
    			$j('#rejectWarning2').removeClass('hid');
    			rejectItemAndCreateHistory(barcode);
    		}
    		else {
    			if(confirm('Some job items with this instrument are already checked, others not. Do you like to check it all?')) {
    					checkItem(cbs, table, barcode, i);
    			}
    			else rejectItemAndCreateHistory(barcode);
    		}
    	}
    	updateInstructions();
    }
}

/**
 * this method checks all inputs of type checkbox that are contained within the
 * table
 * 
 * @param {Boolean}
 *            select true if boxes are to be checked and vice versa
 * @param table
 *            in which to search for inputs of type checkbox
 */
function selectAllItems(select, table) {

	let filteredRows = table.rows({filter: 'applied'});
	filteredRows.every( function ( rowIdx, tableLoop, rowLoop ) {
		
		var row = this;
		
		// check row checkbox
		let mainCheckBox = row.node().cells[0].querySelector("input[type='checkbox']");
		if(mainCheckBox !== null && select != $(mainCheckBox).is(':checked')){
			mainCheckBox.checked = select;
			checkReturnToAddressAndContactCompatibility(mainCheckBox);
			// refresh instructions
			refreshInstructions(mainCheckBox);
		}
		
		// get the index of the accessories column
		var columnAccessoriesIndex = row.cell('.accessories').node().cellIndex;
		// check accessories checkbox
		row.node().cells[columnAccessoriesIndex].querySelectorAll("input[type='checkbox']").forEach( function (cb) {
			cb.checked = select;
		});
	});
	
	$j('input#unselectCounter').val($j('.jobItem:checkbox:not(:checked)').length);
	
}


/**
 * this method creates html content to show data in child row of table
 * 
 * @param jobId :
 *            the id of the current job
 * @param onBehalfOfName :
 *            on-behalf address if exist
 * @param onBehalfOfId :
 *            on-behalf id if exist
 * @param tpInstructions :
 *            the Third Party instructions
 * 
 */

function format(destination, jobId, onBehalfOfName, onBehalfOfId, tpInstructions) {
	var content = '';
	var tableContent = '';
	var countInstructions = 0;
	if(onBehalfOfName!=null){
		content +='<div>'+i18n.t("core.deliverynote:onBehalf", "On-Behalf")+':&nbsp;'+onBehalfOfName+' &nbsp;&nbsp;&nbsp;<a href="createdelnote.htm?jobid='+jobId+'&destination=CLIENT&onbehalfid='+onBehalfOfId+'">'+i18n.t("core.deliverynote:onbehalflink", "Create Delivery Note For this Address")+'</div><br>';
	}
	if(tpInstructions){
		var size=tpInstructions.length;
	    // tpInstructions
	    if(size>0 && destination == 'THIRDPARTY'){			
	    	$j.each(tpInstructions, function(i) {
	    		if (tpInstructions[i].shownOnTPDelNote){
	    			var imgSrc='<img src="img/icons/bullet-tick.png" width="10" height="10" alt="'+i18n.t("core.deliverynote:showDelNote", "Show On TP Del Note")+'" title="'+i18n.t("core.deliverynote:showDelNote", "Show On TP Del Note")+'"/>';
	    		}	
	    		else{
	    			var imgSrc='<img src="img/icons/bullet-cross.png" width="10" height="10" alt="'+i18n.t("core.deliverynote:hideDelNote", "Hide On TP Del Note")+'" title="'+i18n.t("core.deliverynote:hideDelNote", "Hide On TP Del Note")+'"/>';
	    		}
	    		if (tpInstructions[i].active){
	    			tableContent += 
	    				'<tr><td class="instruction">'+tpInstructions[i].instruction+'</td>'+
	    				'<td class="recordedby" >'+tpInstructions[i].contactName+'&nbsp;&nbsp;' +tpInstructions[i].lastModified+'</td>'+
	    				'<td class="showontpdel" id="showontpdn'+tpInstructions[i].tpInstructionId+'"><a href="#" onclick="event.preventDefault(); updateShowOnTPDelNoteJIInst('+tpInstructions[i].tpInstructionId+','+tpInstructions[i].shownOnTPDelNote+');">'+imgSrc+'</a>'+
	    				'</td>'+                                                                                                                               
	    				'</tr>';
	    			countInstructions++;
	    		}
	    	});
	    	if (countInstructions == 0){
	    		tableContent +='<tr>'+    
					'<td colspan="3">'+i18n.t("core.deliverynote:noActiveTP", "No Active Third Party Instructions To Display")+'</td></tr>';
	    	}
	    }
		}
		if (tableContent!='' && destination == 'THIRDPARTY'){
			tableContent='<table class="default4 tpInstructions">'+
				'<tbody><tr>'+ 
				'<td colspan="2" class="bold">'+i18n.t("core.deliverynote:tpinstructions", "Third party instructions for item")+'</td>'+    
				'<td class="showontpdel bold">'+i18n.t("core.deliverynote:showOnDelNote", "Show On Del Note")+'</td>'+    
				'</tr>'+ tableContent+
				' </tbody>'+
				'</table>';
		}
		content+=tableContent;
		
		
	return content;	
}

function checkBoxRenderer(data, type, row) {
	 if(data.readyForDelivery)
		 return $j('#itemTableCheckBoxTemplate').html().replace('jobItemId', data.jobItemId);							 
	 else
		 return i18n.t("core.deliverynote:notready", "not ready");
}

function itemAccessoriesRenderer( data, type, row ) {
	 var freeText=row.accessoryFreeText;
	 var accessories = '';
	 if(data){
		 // loop through all the row details to build
			// output
		 // string
		 for (i = 0; i < data.length; i++) {
			 accessories += '<img width="16" height="16" title="" alt="" src="img/icons/arrow_merge.png"/>';
			 accessories += '<input type="checkbox" class="accessories" name="accessoriesitems" value="'+data[i].itemAccessoryId+'"/>';
			 accessories += data[i].accessoryName +'&nbsp x'+ data[i].quantity + '<br>';
        		}
	 	}
	 if(freeText != undefined){
        	accessories += '<img width="16" height="16" title="" alt="" src="img/icons/arrow_merge.png"/>';
        	accessories += '<input type="checkbox" class="freeText" name="accessoryFreeText" value="'+row.jobItemId+'"/>'
        	accessories += freeText;
        }
     return accessories;
}

function handleMainCheckBoxChange(checkbox) {
	$j('input#unselectCounter').val($j('.jobItem:checkbox:not(:checked)').length);
	
	checkReturnToAddressAndContactCompatibility(checkbox);
	
	// update link-contractid instruction web component attribute value
	refreshInstructions(checkbox);
}

function refreshInstructions(checkbox) {
	const element = document.querySelector("cwms-instructions");
	if(element !== null) {
		var currentVal = element.jobItemIds;
		if($j(checkbox).is(':checked')) {
			if(currentVal === undefined || currentVal === '')
				currentVal = $j(checkbox).val();
			else
				currentVal += ','+$j(checkbox).val();
		}
		else {
			currentVal = currentVal.replace($j(checkbox).val(),'');
			currentVal = currentVal.replace(',,',',');
			if(currentVal.startsWith(','))
				currentVal = currentVal.substring(1);
			if(currentVal.endsWith(','))
				currentVal = currentVal.slice(0, -1);
				
		}
		element.jobItemIds = currentVal;
		element.refresh();
		
	}
}

function checkReturnToAddressAndContactCompatibility(checkbox){
	
	let params = (new URL(document.location)).searchParams;
	let destination = params.get('destination');
	
	if(checkbox.checked && destination === 'CLIENT'){
	
		let mainAddressId = $j("#addressid").val();
		let mainContactId = $j("#contactid").val();
		
		// show incompatibility warning about either the return to address or
		// the contact
		let row = $j(checkbox).parent().parent();
		
		if( row.attr('data-returntoaddressid') !== mainAddressId || row.attr('data-returntocontactid') !== mainContactId )
			document.querySelector("#incompatibilityAlertPopup").show()	
	}
}
