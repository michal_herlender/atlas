/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: Stuart Harrold
*
*	FILENAME		:	ViewDIUtility.js
*	DESCRIPTION		:	This javascript file is imported into multiple page specific javascript files such as
* 					: 	DelSearchResults.js, ViewCrDes.js
*					: 	
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	08/10/2007 - SH - Created file
* 					: 	08/10/2007 - SH - JQuery functions and comment
* 					:	08/01/2009 - SH - Updated code to use different dwr service for general/job deliveries
*					:	07/01/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/


/**
 * this function retrieves all delivery items which are linked to the delivery id supplied using dwr,
 * if delivery items have already been retrieved then no DWR call is made.
 * 
 * @param (Object) event window event used to cancel any event bubbling
 * @param (Integer) rowcount integer value of the row for which delivery items should be found and displayed
 * @param (Integer) id id of the delivery
 * @param (Boolean) isGeneral boolean value indicating if the delivery is a general delivery, else it is a job delivery
 */
function getDeliveryItems(event, rowcount, id, isGeneral)
{	
	// cancel event bubbling
	event.cancelBubble = true;	
	// delivery items for this delivery row have already been retrieved
	if ($j('#child' + rowcount).length)
	{
		// delivery items are currently hidden so make them visible
		if ($j('#child' + rowcount).css('display') == 'none')
		{
			// remove image to show items and append hide items image
			$j('#itemsLink' + rowcount).empty().append('<img src="img/icons/items_hide.png" width="16" height="16" alt="' + i18n.t("core.deliverynote:hideDeliveryItems", "Hide Delivery Items") + '" title="' + i18n.t("core.deliverynote:hideDeliveryItems", "Hide Delivery Items") + '" class="image_inline" />');
			// make the delivery items row visible and add slide down effect
			$j('#child' + rowcount).css({ display: '', visibility: 'visible' }).find('div:first').slideDown(1000);
		}
		// delivery items are currently visible so hide them
		else
		{
			// remove image to hide items and append show items image
			$j('#itemsLink' + rowcount).empty().append('<img src="img/icons/items.png" width="16" height="16" alt="' + i18n.t("core.deliverynote:showDeliveryItems", "Show Delivery Items") + '" title="' + i18n.t("core.deliverynote:showDeliveryItems", "Show Delivery Items") + '" class="image_inline" />');
			// add effect to slide up delivery items and hide row
			$j('#child' + rowcount + ' div').slideUp(1000, function(){
				$j('#child' + rowcount).css({ display: 'none', visibility: '' });
			});
		}
	}
	// delivery items for this delivery row have not been retrieved so call DWR
	else
	{
		// load javascript service file
		loadScript.loadScriptsDynamic(new Array('dwr/interface/delitemservice.js'), true,
		{
			callback: function()
			{
				// get current time
				var currentTime = new Date().getTime();
				// is this a general delivery
				if (isGeneral)
				{
					// call dwr service to get items for general delivery
					delitemservice.getItemsForGeneralDelivery(id, 
					{
						callback:function(delitems)
						{				
							displayDeliveryItems(delitems, currentTime, rowcount, isGeneral);
						}
					});
				}
				else
				{
					// call dwr service to get items for job delivery
					delitemservice.getItemsForJobDelivery(id, 
					{
						callback:function(delitems)
						{				
							console.log(delitems);
							displayDeliveryItems(delitems, currentTime, rowcount, isGeneral);
						}
					});
				}
			}
		});		
	}		
}

/**
 * time value of last DWR call, checked against the current DWR call
 */
var lastUpdate = 0;

/**
 * this function displays the delivery items retrieved by (@see getDeliveryItems())
 * 
 * @param (delitems) list of delivery item objects
 * @param (currentUpdate) timestamp of the current search
 * @param (rowcount) integer value of the row for which delivery items should be found and displayed
 * @param (Boolean) isGeneral boolean value indicating if the delivery is a general delivery, else it is a job delivery
 */
function displayDeliveryItems(delitems, currentUpdate, rowcount, isGeneral)
{
	// this is a new search
	if (currentUpdate > lastUpdate)
	{
		// update lastUpdate variable
		lastUpdate = currentUpdate;
		
		// work out the class to be used for child row dependant on rowcount (odd, even)
		var classname = 'odd';
		if(rowcount % 2 == 0)
		{
			classname = 'even';
		}
		// if the parent row is currently highlighted with hoverrow then add to the child
		if ($j('#parent' + rowcount).attr('class') == 'hoverrow')
		{
			/**
			 * this variable is used in the @see (SearchResultsKeyNav.js) file to flag that a row has a child
			 * and that it is currently highlighted with the 'hoverrow' class name
			 */  
			hasChild = true;
		}
		
		// delitems length is greater than zero
		if(delitems.length > 0)
		{
			// create content for delivery items											
			var content = 	'<tr id="child' + rowcount + '" class="' + classname + '">' +
								'<td colspan="7">';
																
			// general delivery items?
			if (isGeneral)
			{
				content +=	'<div style=" display: none; ">' +
								'<table class="child_table" summary="This table displays any items on a general delivery">' +
									'<thead>' +
										'<tr>' +
											'<td colspan="2">';
												if (delitems.length == 1) 
												{
													content += i18n.t("core.deliverynote:oneDelItem", {nbOfItems : delitems.length, defaultValue : "There is " + delitems.length + " delivery item"});
												}
												else
												{
													content += i18n.t("core.deliverynote:severalDelItems", {nbOfItems : delitems.length, defaultValue : "There are " + delitems.length + " delivery items"});
												}
								content +=	'</td>' +
										'</tr>' +
										'<tr>' +
											'<th scope="col" class="delgenitem">' + i18n.t("itemNo", "Item No") + '</th>' +  
											'<th scope="col" class="delgendesc">' + i18n.t("itemDescription", "Item Description") + '</th>' +						
										'</tr>' +
									'</thead>' +
									'<tfoot>' +
										'<tr>' +
											'<td colspan="2">&nbsp;</td>' +
										'</tr>' +
									'</tfoot>' +
									'<tbody>';
										// add a row to the delivery items table for each delivery item returned
										$j.each(delitems, function(i){
											
											content +=	'<tr>' +
															'<td class="center">' + (i + 1) + '</td>' +
															'<td>' + delitems[i].itemDescription + '</td>' +
														'</tr>';																		
										});
						content +=	'</tbody>' +
								'</table>' +
							'</div>';
			}
			else
			{
				content +=	'<div style=" display: none; ">' +
								'<table class="child_table" summary="This table displays any items on a job delivery">' +
									'<thead>' +
										'<tr>' +
											'<td colspan="5">';
												if (delitems.length == 1) 
												{
													content += i18n.t("core.deliverynote:oneDelItem", {nbOfItems : delitems.length, defaultValue : "There is " + delitems.length + " delivery item"});
												}
												else
												{
													content += i18n.t("core.deliverynote:severalDelItems", {nbOfItems : delitems.length, defaultValue : "There are " + delitems.length + " delivery items"});
												}
								content +=	'</td>' +
										'</tr>' +
										'<tr>' +
											'<th scope="col" class="delitem">' + i18n.t("itemNo", "Item No") + '</th>' +  
											'<th scope="col" class="delinst">' + i18n.t("instrument", "Instrument") + '</th>' +
											'<th scope="col" class="delserial">' + i18n.t("serialNo", "Serial No") + '</th>' +
											'<th scope="col" class="delplant">' + i18n.t("plantNo", "Plant No") + '</th>' +  
											'<th scope="col" class="delcustomerDesc">' + i18n.t("customerdescription", "Customer Description") + '</th>' + 
										'</tr>' +
									'</thead>' +
									'<tfoot>' +
										'<tr>' +
											'<td colspan="5">&nbsp;</td>' +
										'</tr>' +
									'</tfoot>' +
									'<tbody>';
										// add a row to the delivery items table for each delivery item returned
										$j.each(delitems, function(i)
										{
											content += 	'<tr>' +
															'<td class="center">' + delitems[i].jobitem.itemNo + '</td>' +
															'<td>' + delitems[i].jobitem.inst.definitiveInstrument + '</td>' +
															'<td>' + delitems[i].jobitem.inst.serialno + '</td>' +
															'<td>' + delitems[i].jobitem.inst.plantno + '</td>' +
															'<td>' + delitems[i].jobitem.inst.customerDescription + '</td>' +
														'</tr>';																		
										});
						content +=	'</tbody>' +
								'</table>' +
							'</div>';
			}
			
			content += 		'</td>' +
						'</tr>';
									
			// remove image to show items and append image to hide items
			$j('#itemsLink' + rowcount).empty().append('<img src="img/icons/items_hide.png" width="16" height="16" alt="' + i18n.t("core.deliverynote:hideDeliveryItems", "Hide Delivery Items") + '" title="' + i18n.t("core.deliverynote:hideDeliveryItems", "Hide Delivery Items") + '" class="image_inline" />');
			// append a new child table row below the parent for this delivery where delivery items will be listed
			$j('#parent' + rowcount).after(content);			
			// add effect to slide down delivery items
			$j('#child' + rowcount + ' div').slideDown(1000);
		}
	}
}