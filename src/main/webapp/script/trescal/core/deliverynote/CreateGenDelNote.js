/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: Jamie Vinall
*
*	FILENAME		:	CreateGenDelNote.js
*	DESCRIPTION		:	This javascript file is used on the creategendelnote.vm page.
*					: 	
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	02/07/2007 - JV - Created File
*
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'script/trescal/core/deliverynote/CDUtility.js');

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'contactid';


function addrChange(addrId, defLocId)
{
	$j.ajax({
		url: "addrChange.json",
		data: {
			addrId: addrId
		},
		async: true
	}).done(function(locations) {
		
		// remove all options from select
		$j('select#locid').empty();
		// add options from locations list
		$j.each(locations, function(i) {
		     $j('select#locid')
		         .append($j('<option value='+ locations[i].locationid +'>'+ locations[i].location +'</option>'));
		});
		
		if (locations.length < 1) 
		{
			$j('li#delLoc').hide();
		}
		
		else
		{
			if(defLocId != 0)
			{
				$j('select#locid option[value=' + defLocId + ']').attr('selected','selected');
			}
			$j('li#delLoc').show();
		}
	});	
}
