/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	EditModel.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	23/10/2007 - SH - Created File
*					:	14/10/2015 - TProvost - Manage i18n
*						2017-09-18 Moved unused range editing code to EditModelRange.js
****************************************************************************************************************************************/

/**
 * load service necessary for searching manufacturers
 */
var pageImports = new Array('dwr/interface/manuservice.js');


/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * Array of autocomplete objects (multiple are possible) containing setup for 
 * jQueryUI initialization.
 * this is used in the init() function.
 */
var jquiAutocomplete = new Array(
				{
					inputFieldName: 'salesCategoryText',
					inputFieldId: 'salesCategoryId',
					source: 'searchsalescategorytags.json'
				},
				{
					inputFieldName: 'descname', 
					inputFieldId: 'descid',
					source: 'searchdescriptiontags.json'
				});

/**
 * this method copies the selected option to all other select boxes in table 
 *
 * @param {String} tableid id of the table in which all selects should be copied
 * @param {Object} select select which we can obtain value and name for copying
 */
function copySelectedOption(tableid, elementtype, element)
{
	// get element name attribute
	var elementName = $j(element).attr('name');
	// get end of element name after last full stop
	var elementEnd = elementName.substring(elementName.lastIndexOf('.'), elementName.length);
	// get all matching elements
	$j('#' + tableid + ' ' + elementtype + '[name$="' + elementEnd + '"]').each(function(i, n)
	{
		$j(this).val($j(element).val());
	})
}





