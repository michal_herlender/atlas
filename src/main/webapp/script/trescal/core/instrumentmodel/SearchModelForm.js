/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	SearchModelForm.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	23/10/2007 - SH - Created File
*						2015-06-12 GB - Mutli-language description search
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'mfr';

/**
 * Array of autocomplete objects (multiple are possible) containing setup for 
 * jQueryUI initialization.
 * this is used in the init() function.
 */
var jquiAutocomplete = new Array(
				{
					inputFieldName: 'salesCat',
					inputFieldId: 'salesCatId',
					source: 'searchsalescategorytags.json'
				},
				{
					inputFieldName: 'desc', 
					inputFieldId: 'descId',
					source: 'searchdescriptiontags.json',
					afterSelect: updateCharacteristics
				},
				{
					inputFieldName: 'familyNm', 
					inputFieldId: 'familyId',
					source: 'searchfamilytags.json'
				},
				{
					inputFieldName: 'domainNm', 
					inputFieldId: 'domainId',
					source: 'searchdomaintags.json'
				},
				{
					inputFieldName: 'mfrtext', 
					inputFieldId: 'mfrId',
					source: 'searchmanufacturertags.json'
				}
				);

function updateCharacteristics() {
	var subfamilyId = $j('input#descId').val();
	$j('li#characteristicLi').remove();
	$j.ajax({
		url: "characteristics.json",
		data: {
			subfamilyId: subfamilyId
		},
		async: true
	}).done(function(result) {
		$j('li#subfamilyLi').after(result);
	});
	return false;
}