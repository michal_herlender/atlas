/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	ModelCompanyCalReqs.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	04/11/2020 - AYB - Created File
*						13/01/2021 - LM - Updated File
****************************************************************************************************************************************/

function addCompanyCalReq(coid, modelid){

	var title = i18n.t("core.instrumentmodel:addCalPointsAndReqs", "Add Cal Points &amp; Requirements") ;
	
	var url = `calibrationpoints.htm?type=companymodel&page=instmodcompcalreqs&compid=${coid}&modelid=${modelid}`;
	
	loadScript.createOverlay('external', title, url, null, null, 80, 900, null); 
	 
	return false; 
	
}

function modifyCompanyCalReq(calreqid){
	
	var title = i18n.t("core.instrumentmodel:editCalPointsAndReqs", "Edit Cal Points &amp; Requirements");
	
	var url = `calibrationpoints.htm?type=companymodel&amp;page=instmodcompcalreqs&amp;calreqid=${calreqid}`;
	
	loadScript.createOverlay('external', title, url, null, null, 80, 900, null); 
	
	return false; 
	
}

function deactivateCalReq(calreqid, modelid){
	 
	deactivateCalReqs('instmodcompcalreqs', calreqid, 'companymodel', modelid, ()=> {
		location.reload();
	}); 
	 	
}

function resetForm(){
	window.location = window.location.href;
}

/**
 * Array of autocomplete objects (multiple are possible) containing setup for 
 * jQueryUI initialization.
 * this is used in the init() function.
 */
var jquiAutocomplete = new Array(
				{
					inputFieldName: 'desc', 
					inputFieldId: 'descId',
					source: 'searchdescriptiontags.json',
					afterSelect: updateCharacteristics
				},
				{
					inputFieldName: 'mfr', 
					inputFieldId: 'mfrId',
					source: 'searchmanufacturertags.json'
				}
			);

function updateCharacteristics() {
	var subfamilyId = $j('input#descId').val();
	$j('li#characteristicLi').remove();
	$j.ajax({
		url: "characteristics.json",
		data: {
			subfamilyId: subfamilyId
		},
		async: true
	}).done(function(result) {
		$j('li#subfamilyLi').after(result);
	});
	return false;
}