/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	EditMfr.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	29/10/2007 - SH - Created File
*
****************************************************************************************************************************************/
/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array ( 'script/trescal/core/tools/FileBrowserPlugin.js' );

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = 	new Array( 	{ anchor: 'resources-link', block: 'resources-tab' },
								{ anchor: 'models-link', block: 'models-tab' },
								{ anchor: 'instruments-link', block: 'instruments-tab' } );