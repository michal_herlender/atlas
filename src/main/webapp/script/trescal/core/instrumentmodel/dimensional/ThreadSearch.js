/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	ThreadSearch.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	04/12/2008 - SH - Created File
*
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'size';

/**
 * this method shows or hides the edit link depending on the value passed from select box
 * 
 * @param {String} type the select type to be edited or not
 * @param {String} value selected option
 */
function showHideEditLink(type, value)
{
	// value blank?
	if(value == '')
	{
		// hide the edit link
		$j('#edit' + type).hide();
	}
	else
	{
		// show the edit link
		$j('#edit' + type).show();
	}
}
	