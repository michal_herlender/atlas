/**
 * 
 */

function initializeServices(coId, modelId) {
	$j('#initServicesLink').addClass("hid");
	$j.ajax({
		url: "initializeinstrumentmodelservices.json",
		data: {
			modelId: modelId
		},
		async: true
	}).done(function(services) {
		services.forEach(function(service) {
			$j('#modelviewservices'+coId).append(
					"<tr>" +
						"<td>" + service.subdiv + "</td>" +
						"<td>" + service.costType + "</td>" +
						"<td>" + service.calibrationType + "</td>" +
						"<td>" + service.procedure + "</td>" +
						"<td>" + service.calibrationProcess + "</td>" +
						"<td>" + service.checkSheet + "</td>" +
						"<td><a href='editmodelservices.htm?id=" + service.id + "'>" + editString + "</a></td>" +
					"</tr>");
		});
	}).fail(function(error) {
		alert("Error");
	});
}

function copyServices(modelId) {
	$j("#dialog_oes").dialog({
		resizable: false,
		height: "auto",
		width: 400,
		modal: true,
		buttons: [
			{
				text: i18n.t("yes", "Yes"),
				click: function() {
					$j.ajax({
						url: "copyinstrumentmodelservices.json",
		        		data: {
		        			modelId: modelId,
		        			replace: true
		        		},
		        		dataType: 'text',
		        		async: true
		        	}).done(function(counterString) {
		        		alert(counterString);
		        	}).fail(function(error) {
		        		alert("Error");
		        	});
					$j(this).dialog("close");
				}
			},
			{
				text: i18n.t("no", "No"),
				click: function() {
		        	$j.ajax({
		        		url: "copyinstrumentmodelservices.json",
		        		data: {
		        			modelId: modelId,
		        			replace: false
		        		},
		        		dataType: 'text',
		        		async: true
		        	}).done(function(counterString) {
		        		alert(counterString);
		        	}).fail(function(error) {
		        		alert("Error");
		        	});
		        	$j(this).dialog( "close" );
				}
			}
		]
	});
}