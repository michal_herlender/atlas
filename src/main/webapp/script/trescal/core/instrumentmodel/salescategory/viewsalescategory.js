/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array ('script/thirdparty/jQuery/jquery.dataTables.min.js');

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init() {
    $j('#salescategories').dataTable( {
        "ajax": {"url" : "allsalescategories.json","dataSrc" : ""},
        "columns": [
						{ "data": 'id' },
						{ "data": 'name' },
						{ "data": 'active' }					
					],
    	"columnDefs": [
    	               {targets: 0, visible: false, searchable: false},
    	               {targets: 1, visible: true, searchable: true},
    	               {targets: 2, visible: false, searchable: false}],
    	               "language": {
    	       				"url": 'script/thirdparty/jQuery/i18nDataTable/'+locale+'.json'
    	       		}
    } );
}