/**
 * Array of autocomplete objects (multiple are possible) containing setup for 
 * jQueryUI initialization.
 * this is used in the init() function.
 */
var jquiTabs = new Array(
	{
		idName: 'tabs'
	}
);