/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array ('script/thirdparty/jQuery/jquery.dataTables.min.js');

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init() {
	console.log("********** "+locale);
    $j('#families').dataTable( {
        "ajax": {"url" : "allfamilies.json","dataSrc" : ""},
        "columns": [
        						{ data: 'familyid' },
        						{ data: 'name' },
        						{ data: 'active' },
        						{ data: 'tmlid' },
        						{ data: 'familyid',
												"fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
															$j(nTd).html("<a href='editinstrumentmodelfamily.htm?id="+oData.familyid+"'>" + i18n.t("core.instrumentmodel:editOrDelete", "Edit/Delete") + "</a>");
												}
										}						
    							],
    		"columnDefs": [{"targets": [ 0,4 ],"visible": false,"searchable": false},{className: 'dt-center', targets: [2,3,4]}],
    		"language": {
    			"url": 'script/thirdparty/jQuery/i18nDataTable/'+locale+'.json'
    		}

    } );
}