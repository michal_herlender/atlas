/**
  *	FILENAME		:	addeditflexiblefielddefinition.js
  * HISTORY         :	SC - 2016-12-13 - Created File (Commit 2297)
  * 					GB - Changed to define init() function (called by Cwms_Main.js after initialization)
  **/

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{
	
	//Function to show and hide library value input area
	$j("#FieldType").on('change', function() {
 		if (this.value == "4") {
        	$j('#Values').show();
    	} else {
        	$j('#Values').hide();
    	}
	});
	
	
	//Function to add new library values
	$j('#addbtn').click(function() {
		var numberofvalues = $j("#valuelist").children().length;
		var newValue = $j("#newValue").val();
		$j("#valuelist").append(
				"<li>" +
				"<input type=\"text\" value=\"" + newValue + "\" name=\"values[" + numberofvalues + "].libraryValueName\" class=\"libraryValue\"/>" +
				"<input type=\"hidden\" value=\"0\" name=\"values[" + numberofvalues + "].libraryValueId\"/>" +
				"</li>");
		$j("#newValue").val("");
	});

}