/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	SubDiv.js
*	DESCRIPTION		:	Page specific javascipt for the subdiv velocity page.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	26/10/2006 - SH - Created File
* 					:	24/08/2007 - SH - JQueried
*					:	06/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'script/trescal/core/utilities/ScriptedUtils.js',
								'dwr/interface/bposervice.js',
								'script/trescal/core/company/BPOFunctions.js' );

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' },
								{ element: 'ul.subnavtab a', corner: 'small transparent top' } );

/** 
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = 	new Array( { anchor: 'contact-link', block: 'contact-tab' },
					      	   { anchor: 'address-link', block: 'address-tab' },
					      	   { anchor: 'bpo-link', block: 'bpo-tab' },
							   { anchor: 'instructions-link', block: 'instructions-tab' },
							   { anchor: 'categories-link', block: 'categories-tab' }, 
					      	   { anchor: 'settings-link', block: 'settings-tab' }, 
							   { anchor: 'exchangeformat-link', block: 'exchangeformat-tab' }, 
							   { anchor: 'advesoactivities-link', block: 'advesoactivities-tab' },
							   { anchor: 'subdivs-link', block: 'subdivs-tab' }
   ) ;

/**
 * if departments variable contains a value of true then add another object to the menuElements array
 * for the departments tab
 */					      	 
if (departments == true)
{
	menuElements.push({ anchor: 'department-link', block: 'department-tab' });
}
					      	   
/** 
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements2 = new Array( { anchor: 'contactActive-link', block: 'contactActive-tab' },
					      	   { anchor: 'contactInactive-link', block: 'contactInactive-tab' } );
					      	   
/** 
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements3 = new Array( { anchor: 'addressActive-link', block: 'addressActive-tab' },
					      	   { anchor: 'addressInactive-link', block: 'addressInactive-tab' } );		
		

/**
 * this function accepts values from the page and assigns them to hidden input
 * fields on the page then forwards the user onto the correct page depending on which
 * link they clicked
 *
 * @param {Integer}	personid is the id of a subdivision contact
 * @param {Integer}	subdivid is the id of a subdivision
 * @param {String} a is a string containing the users choice of link
 * @param {Integer}	addressid is the id of a subdivision address
 */
function submitPage(personid, subdivid, a, addressid)
{
	$j('#personid').attr('value', personid);
	$j('#subdivid').attr('value', subdivid);
	$j('#addrid').attr('value', addressid);
	
	switch (a)
	{
		case 'new':			$j('#subdivform').attr('action', springUrl + '/addperson.htm');
							break;
		
		case 'newadd':		$j('#subdivform').attr('action', springUrl + '/addperson.htm');
							break;
						
		case 'oldadd':		$j('#subdivform').attr('action', springUrl + '/viewaddress.htm');
							break;
						
		case 'viewperson':	$j('#subdivform').attr('action', springUrl + '/viewperson.htm');
							break;
	}
		
	$j('#subdivform').submit();
}

/**
 * this function updates a chosen contact for a subdivision and sets it to be the default
 */
function updateSubdivContactDefault(subdivid, personid, img)
{
	if (img.id != 'defaultUser')
	{
		// load the service javascript file if not already	
		loadScript.loadScriptsDynamic(new Array('dwr/interface/subdivservice.js'), true,
		{
			callback: function()
			{
				// call dwr service to change the default contact
				subdivservice.updateSubdivContactDefault(subdivid, personid,
				{
					callback: function()
					{
						// get the current default contact image and change properties
						$j('#defaultUser').attr('src', 'img/icons/user.png')
										  .attr('alt', i18n.t("core.company:subdiv.makeUserDefault", "Make User Default"))
										  .attr('title', i18n.t("core.company:subdiv.makeUserDefault", "Make User Default"))
										  .attr('id', '');
						// change the properties of the chosen contact image
						img.src = 'img/icons/user_default.png';
						img.alt = i18n.t("core.company:subdiv.defaultUser", "Default User");
						img.title = i18n.t("core.company:subdiv.defaultUser", "Default User");
						img.id = 'defaultUser';
					}
				});
			}
		});
	}
	else
	{
		return false;
	}
}

/**
 * this function updates a chosen address for a subdivision and sets it to be the default
 */
function updateSubdivAddressDefault(subdivid, addressid, img)
{
	if (img.id != 'defaultAddr')
	{
		// load the service javascript file if not already	
		loadScript.loadScriptsDynamic(new Array('dwr/interface/subdivservice.js'), true,
		{
			callback: function()
			{
				// call dwr service to change the default address
				subdivservice.updateSubdivAddressDefault(subdivid, addressid,
				{
					callback: function()
					{
						// get the current default address image and change properties
						$j('#defaultAddr').attr('src', 'img/icons/house.png')
										  .attr('alt', i18n.t("core.company:subdiv.makeAddressDefault", "Make Address Default"))
										  .attr('title', i18n.t("core.company:subdiv.makeAddressDefault", "Make Address Default"))
										  .attr('id', '');
						// change the properties of the chosen address image
						img.src = 'img/icons/house_default.png';
						img.alt = i18n.t("core.company:subdiv.defaultAddress", "Default Address");
						img.title = i18n.t("core.company:subdiv.defaultAddress", "Default Address");
						img.id = 'defaultAddr';
					}
				});
			}
		});
	}
	else
	{
		return false;
	}
}

/**
 * this function creates the warning message to display when the user has chosen to
 * activate/deactivate a subdivision
 */
function createWarningMessage(action, subdivision)
{
	// create warning text variable
	var warningText = '';
	// de-activate sudivision?
	if (action == 'deactivate')
	{
		var warningText = 	'<span>' + i18n.t("core.company:subdiv.warnOnDeactivate", {subdiv : subdivision, defaultValue :  "Please be aware that <strong>deactivating</strong> (" + subdivision + ") will result in the following:"}) + '</span>' +
							'<ul>' +
								'<li>' + i18n.t("core.company:subdiv.resultOfDeactivate1", {subdiv : subdivision, defaultValue : subdivision + " will no longer appear in any search lists"}) + '</li>' +
								'<li>' + i18n.t("core.company:subdiv.resultOfDeactivate2", {subdiv : subdivision, defaultValue : "It will not be possible to book jobs in against contacts for " + subdivision}) + '</li>' +
							'</ul>';
	}
	else
	{
		var warningText = 	'<span>' + i18n.t("core.company:subdiv.warnOnActivate", {subdiv : subdivision, defaultValue : "Please be aware that <strong>activating</strong> (" + subdivision + ") will result in the following:"}) + '</span>' +
							'<ul>' +
								'<li>' + i18n.t("core.company:subdiv.resultOfActivate1", {subdiv : subdivision, defaultValue : subdivision + " will now appear in search lists"}) + '</li>' +
								'<li>' + i18n.t("core.company:subdiv.resultOfActivate2", {subdiv : subdivision, defaultValue : "It will be possible to book jobs in against contacts for " + subdivision}) + '</li>' +
							'</ul>';
	}
	
	var message = '<div class="warningBox1">' +
						'<div class="warningBox2">' +
							'<div class="warningBox3">' +
								warningText +
							'</div>' +
						'</div>' +
					'</div>';
					
	return message;
}

/**
 * deletes the department identified by the given id from the department table.
 */
function deleteDep(departmentid)
{
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/departmentservice.js'), true,
	{
		callback: function()
		{
			departmentservice.deleteDepartment(departmentid,
			{
				callback:function(dataFromServer)
				{
					cleanUpDeleteDepartment(dataFromServer, departmentid);
				}
			});
		}
	});
}

/**
 * callback function for @method deleteDep() it deletes the chosen department and
 * amends the department count if all departments have been deleted then display empty row
 */
function cleanUpDeleteDepartment(dfs, departmentid)
{
	// replace previous count with new value
	$j('#depCount').text(parseInt($j('#depCount').text()) - 1);
	// if department count is zero then add empty row to table
	if (parseInt($j('#depCount').text()) == 0)
	{
		// append empty row to table
		$j('tbody#deptbody').append(	'<tr class="odd">' +
											'<td colspan="4" class="center bold">' + i18n.t("core.company:subdiv.assignedNoDepartments", "This subdivision has been assigned no departments") + '</td>' +
										'</tr>');
	}	
	// remove the deleted row from the table
	$j('tr').remove('#department' + departmentid);
}

/**
 * script used in activities management client subdivision side
 */
function sendData()
{
	$j("#subdiv_manage_act_form").submit();
}

function deleteOActivity(id, subdivid){
	var yes = i18n.t("yes", "Yes");
	var no = i18n.t("no", "No");
	var buttonsObj = {};
	buttonsObj[no] = false;
	buttonsObj[yes] = true;
	
	$j.prompt(i18n.t("delete", "delete")+" ?", {
		buttons: buttonsObj,
		submit: function(e,v,m,f){
			
			console.log("Value clicked was: "+ v);
			if(v){
				// make the ajax call
				$j.ajax({
					  url: "deleteOActivity.json?id="+id+"&subdivid="+subdivid,
					  dataType:'json'
				}).done(function(res) {
					console.log(res);
					if(res.success===true){
						// refresh page
						window.location.reload();
					}else{
						$j.prompt.close();
						$j.prompt(res.message);
					}
				});
			}
		}
	});
}

function addNewActivity(type, activity, istransferable, isdisplayed)
{
	var options = '<option value=""></option>';
	itemactivitiestype.forEach(function(element) {
		if(element != 'UNDEFINED')
		options += '<option value="'+element+'">'+element.replace("_"," ")+'</option>';
	});
	console.log(options);
	$j.prompt('<table><tr><td><span width="60px">'+type+' :</span></td><td><select name="type" onchange="getActivities($j(this).val())">'+options+'</select></td></tr><tr><td><span width="60px">'+activity+' :</span></td><td><select name="activity" id="activity" class="maxwidth-240"></select></td></tr><tr><td><span width="60px">'+istransferable+' :</span></td><td><input type="checkbox" name="istransferable" id="istransferable"/></td></tr><tr><td><span width="60px">'+isdisplayed+' :</span></td><td><input type="checkbox" name="isdisplayed" id="isdisplayed"/></td></tr></table>', {
		title: "Add Activity",
		buttons: { "Add": true, "Close": false },
		persistent: false,
		submit: function(e,v,m,f){
			if(v){
				e.preventDefault();
				console.log('Add ------------');
				addNewActivityAJAX($j("#activity").val(), ($j('#istransferable:checkbox:checked').length > 0), ($j('#isdisplayed:checkbox:checked').length > 0), $j("#subdivid").val());
			}
		}
		
	});
}

function addNewActivityAJAX(id, istransferable, isdisplayed, subdivid)
{
	// make the ajax call
	$j.ajax({
		  url: "addNewOActivity.json",
		  dataType:'json',
		  data:{id:id, istransferable:istransferable, isdisplayed:isdisplayed, subdivid:subdivid},
		  method:'POST'
	}).done(function(res) {
		console.log(res);
		if(res.success===true){
			// refresh page
			window.location.reload();
		}else{
			$j.prompt.close();
			$j.prompt(res.message);
		}
	});
}

function getActivities(val)
{
	console.log(val+"-*-*-*-*-*-*-*-*-*-*-*-*-");
	// make the ajax call
	$j.ajax({
		  url: "getActivities.json?type="+val,
		  dataType:'json'
	}).done(function(res) {
		console.log(res);
		if(res!==null && res!==undefined){
			// refresh page
			console.log(res);
			var html = '';
			console.log(res.length+"/*/*/*/*/*/*/**/**/*/**/*");
			for(var i=0;i < res.length; i++)
				html += '<option value="'+res[i].id+'">'+res[i].description+'</option>'
			$j("#activity").html(html);
		}else{
			//$j.prompt.close();
			console.log(res);
			$j("#activity").html('');
		}
	});
}
