/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	ViewCompanyList.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	27/10/2020 - Laila MADANI - Created File
****************************************************************************************************************************************/

/** 
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = new Array( 	{ anchor: 'members-link', block: 'members-tab' },
 								{ anchor: 'edit-link', block: 'edit-tab' },
								{ anchor: 'usage-link', block: 'usage-tab'});

function addNewMember(companylistid) {
	
	var cancel = i18n.t("core.jobs:jobItem.cancel", "Cancel");
	var addnewmember = i18n.t("add", "Create");
	
	$j.ajax({
	    url:'addcompanylistmember.htm',
	    type:'GET',
	    data: {
	    	companylistid: companylistid
		},
		async: true,
	    }).done(function(content) {
	       	//display the popup
	       	$j.prompt(content,{
	       		title: i18n.t('core.company:companylist.addMember', 'Add Company List Member'),
	       		buttons: { [addnewmember]: true, [cancel]: false },
	       		submit: function (e, val, m, f) {                            
	                   if (val == true) {
	                   		document.forms['addmembertocompanylist'].submit();
	                   	 }
	                   else
	                       console.log('Cancel!');
	               }
	       	});
	       	
	     });	
}

function addNewCompanyListUsage(companylistid){
	
	var cancel = i18n.t("core.jobs:jobItem.cancel", "Cancel");
	var addnewuser = i18n.t("add", "Create");
	
	$j.ajax({
	    url:'addcompanylistusage.htm',
	    type:'GET',
	    data: {
	    	companylistid: companylistid
		},
		async: true,
	    }).done(function(content) {
	       	//display the popup
	       	$j.prompt(content,{
	       		title: i18n.t('core.company:companylist.addUser', 'Add Company List User'),
	       		buttons: { [addnewuser]: true, [cancel]: false },
	       		submit: function (e, val, m, f) {                            
	                   if (val == true) {
	                   		document.forms['addnewcompanylistusage'].submit();
	                   	 }
	                   else
	                       console.log('Cancel!');
	               }
	       	});
	       	
	     });
}

function deleteCompanyListMember(companyListMemberId){
	$j.ajax({
		url: "deletecompanylistmember.json",
		type: "POST",
		data: {
			companyListMemberId: companyListMemberId
		},
		async: true
	}).done(function(isDeleted) {
		// reload the view company list page
		window.location.reload(true);
	});
}

function deleteCompanyListUsage(companyListUsageId, companyListId){
	$j.ajax({
		url: "deletecompanylistusage.json",
		type: "POST",
		data: {
			companyListUsageId: companyListUsageId
		},
		async: true
	}).done(function(isDeleted) {
		// reload the view company list page with the "usage-tab"
		window.location.href = 'viewcompanylist.htm?id=' + companyListId + '&loadtab=usage-tab';
	});
	
}

