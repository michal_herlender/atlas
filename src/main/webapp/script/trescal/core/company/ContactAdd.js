/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	ContactAdd.js
*	DESCRIPTION		:	Page specific javascript for ContactAdd.vm page.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	
*	KNOWN ISSUES	:	-
*	HISTORY			:	16/07/2007 - SH - Created File
*
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'dwr/interface/addressservice.js',
								'dwr/engine.js',
								'dwr/util.js' );

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

function showWebLogin() {
	$j('li.weblogin').addClass('vis').removeClass('hid');
}

function hideWebLogin() {
	$j('li.weblogin').addClass('hid').removeClass('vis');
}

/* this makes sure that the prefix dont get deleted */
function keepPrefix(prefix){
	var field=$j('#contacthrid');
	var oldvalue=field.val();
    setTimeout(function () {
        if(field.val().indexOf(prefix) !== 0) {
            $j(field).val(oldvalue);
        } 
    }, 1);
}

function getTitlesAutoComplet(){
	$j('#titles').autocomplete({
	    source : function(requete, reponse){ 
	    	$j.ajax({
	                url : 'getTitles.json', 
	                dataType : 'json',
	                data : {
	                	coid : $j('#compid').val() 
	                },
	                success : function(data){
	                	reponse($j.map(data, function(objet){
	                        return objet.title;
	                    }));
	                }
	            });
	        }
	});
}
