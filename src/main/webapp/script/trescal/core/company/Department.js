/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	Department.js
*	DESCRIPTION		:	Page specific javascript for the departmentadd velocity page.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	18/07/2007 - SH - Created File
*
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' },
								{ element: 'ul.subnavtab a', corner: 'small transparent top' } );
								
/** 
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = 	new Array( { anchor: 'addresses-link', block: 'addresses-tab' },
					      	   { anchor: 'members-link', block: 'members-tab' },
					      	   { anchor: 'add-link', block: 'add-tab' },
					      	   { anchor: 'categories-link', block: 'categories-tab'});

/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'departmentName';