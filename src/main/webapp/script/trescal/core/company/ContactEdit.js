/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	ContactEdit.js
*	DESCRIPTION		:	Javascript code to control the changing of a contacts details.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	13/12/2006 - SH - Created File
*					: 	06/10/2015 - TProvost - Manage i18n
					:   07/10/2015 - TProvost - Fixed bug for Create/Add RepeatSchedule
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'script/trescal/core/utilities/ScriptedUtils.js',								
								'script/trescal/core/company/BPOFunctions.js');

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'contact.firstName';

/**
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = 	new Array( { anchor: 'profile-link', block: 'profile-tab' },
					      	   { anchor: 'accounts-link', block: 'accounts-tab' },
					      	   { anchor: 'weblogin-link', block: 'weblogin-tab' },
					      	   { anchor: 'mailgroups-link', block: 'mailgroups-tab' },
					      	   { anchor: 'departments-link', block: 'departments-tab' },
					      	   { anchor: 'settings-link', block: 'settings-tab' },
					      	   { anchor: 'bpo-link', block: 'bpo-tab' },
					      	   { anchor: 'repeatschedules-link', block: 'repeatschedules-tab' },
					      	   { anchor: 'userprefs-link', block: 'userprefs-tab' },
					      	   { anchor: 'proctraining-link', block: 'proctraining-tab' },
					      	   { anchor: 'userrights-link', block: 'userrights-tab' });

function changeUrSubdivs(coid) {
	$j("#urSubdivs").empty();
	$j("#urSubdivs").append(subdivsOf[coid.value]);
}

function changeUserRightSubdivs(coid) {
	$j("#selectUserRightSubdiv").empty();
	$j("#selectUserRightSubdiv").append(subdivsOf[coid.value]);
}


/**
 * Send a request to add a user right. On success manipulate the current view in the right way.
 * @param personid
 * @param deleteMessage localized delete message
 */
function addNewUserRight(personId, deleteMessage) {
	fetch(cwms.urlGenerator("adduserright.json"),{
		method:"post",
		headers:{...cwms.metaDataObtainer.csrfHeader,
						"Content-Type":"application/json; charset=utf-8"
		},
		body:JSON.stringify({
			personId: personId,
			subdivId: document.getElementById('selectUserRightSubdiv')?.value,
			roleId: document.getElementById('selectUserRole').value
		})
	})
	.then(res => res.ok?res.text():Promise.reject(res.statusText))
	.then(function(userRoleLine) {
		$j('#userRoleTableBody #newUserRole').before(userRoleLine);
	})
	.catch(console.error);
}

function deleteNewUserRight(userRoleId) {
	$j.ajax({
		url: "deleteuserright.json",
		type: "POST",
		data: {
			userRoleId: userRoleId
		},
		async: true
	}).done(function(isDeleted) {
		if(isDeleted) $j("#userRoleRow"+userRoleId).remove();
	});
}

/**
 * checks that the contact has already been created and if true displays the update button
 */
function enableUpdate()
{
	if ($j('#personid').attr('value') != 0)
	{
		$j('#update').attr('disabled', '');
	}
}

/**
 * submits the page and form, creates a new contact for the chosen company
 */
function submitNew()
{
	$j('#contactform').attr('action', springUrl + '/addperson.htm').submit();
}

/**
 * this function resets the user's password 
 * and sends a reminder email to the specified contact
 * 
 * @param {Integer} personid is the id of a contact
 */
function resetPassword(personid) {
	const contextPath = document
	  .querySelector("meta[name='_contextPath']")?.getAttribute("content") ?? "" ;
    const baseURL = `${location.origin}${contextPath ?? ""}/`;
    const csrfToken = document.querySelector("meta[name='_csrf']")?.getAttribute("content");
    const csrfHeaderParameter = document.querySelector("meta[name='_csrf_header']")?.getAttribute("content");
    const csrfHeader = {[csrfHeaderParameter]:csrfToken};

	var url = new URL("resetPassword.json",baseURL);
	url.searchParams.set("personId",personid);
	fetch(url,{
		method: 'PUT',
		headers:{...csrfHeader},
	}).then(r => r.ok? r.json():Promise.reject(r.statusText))
	.then(function(response) {
		$j.prompt(response.message);
	});
}

/**
 * this function creates the warning message to display when the user has chosen to
 * activate/deactivate a contact
 * 
 * @param {String} action is a string value that holds either (activate/deactivate) depending on message to be created
 * @param {String} contact is the name of the contact that is to be set to (active/inactive)
 */
function createWarningMessage(action, contact)
{
	if (action == 'deactivate')
	{
		var warningText = 	'<span>' + i18n.t("core.company:contact.warnOngDeactivate", {contact : contact, defaultValue : "Please be aware that <strong>deactivating</strong> (" + contact + ") will result in the following:"}) + '</span>' +
							'<ul>' +
								'<li>' + i18n.t("core.company:contact.resultOfDeactivate1", {contact : contact, defaultValue : contact + " will no longer appear in any search lists"}) + '</li>' +
								'<li>' + i18n.t("core.company:contact.resultOfDeactivate2", {contact : contact, defaultValue : "It will not be possible to book jobs in against " + contact}) + '</li>' +
							'</ul>';
	}
	else
	{
		var warningText = 	'<span>' + i18n.t("core.company:contact.warnOnActivate", {contact : contact, defaultValue : "Please be aware that <strong>activating</strong> (" + contact + ") will result in the following:"}) + '</span>' +
							'<ul>' +
								'<li>' + i18n.t("core.company:contact.resultOfActivate1", {contact : contact, defaultValue : contact + " will now appear in search lists"}) + '</li>' +
								'<li>' + i18n.t("core.company:contact.resultOfActivate2", {contact : contact, defaultValue : "It will not be possible to book jobs in against " + contact}) + '</li>' +
							'</ul>';
	}
		
	var message = '<div class="warningBox1">' +
						'<div class="warningBox2">' +
							'<div class="warningBox3">' +
								warningText +
							'</div>' +
						'</div>' +
					'</div>';
					
	return message;
}

/**
 * this method creates the content for the thickbox in which repeat collections can be assigned
 * to a contact.
 * 
 * @param (String) name the name of the contact we are adding a repeat collection for
 * @param (Integer) personid the id of the contact we are adding a repeat collection for
 */
function createRepeatScheduleContent(name, personid)
{
	var thickboxContent = 	'<div class="thickbox-box">' +
								// add warning box to display error messages
								'<div class="hid warningBox1">' +
									'<div class="warningBox2">' +
										'<div class="warningBox3">' +
										'</div>' +
									'</div>' +
								'</div>' +
								'<fieldset>' +
									'<legend>' + i18n.t("core.company:contact.createRepeatSchedule", {name : name, defaultValue : "Create Repeat Schedule For " + name}) + '</legend>' +
									'<ol>' +
										'<li>' +
											'<label>' + i18n.t("typeLabel", "Type:") + '</label>' +
											'<span>' + i18n.t("collection", "Collection") + '</span>' +
										'</li>' +
										'<li>' +
											'<label>' + i18n.t("addressLabel", "Address:") + '</label>' +
											'<select id="repSchAddr">' +
												contAddressOptions +
											'</select>' +
										'</li>' +
										'<li>' +
											'<label>' + i18n.t("core.company:contact.transportOption", "Transport Option:") + '</label>' +												
											'<select id="repSchTrans">' +
												transOptions +
											'</select>' +
										'</li>' +
										'<li>' +
											'<label>' + i18n.t("notesLabel", "Notes:") + '</label>' +												
											'<textarea id="repSchNotes" cols="50" rows="5">' + i18n.t("core.company:contact.repeatScheduleFor", {name : name, defaultValue : "This is a repeat schedule for " + name}) + '</textarea>' +
										'</li>' +
										'<li>' +
											'<label>&nbsp;</label>' +												
											'<input type="button" value="' + i18n.t("core.company:contact.addSchedule", "Add Schedule") + '" onclick=" addRepeatSchedule(' + personid + ', $j(\'#repSchAddr\').val(), $j(\'#repSchNotes\').val(), $j(\'#repSchTrans\').val()); return false; " />' +
										'</li>' +
									'</ol>' +
								'</fieldset>' +
							'</div>';
							
	return thickboxContent;
}

/**
 * this method adds a repeat collection for the selected contact
 * 
 * @param {Integer} personid the contact id to assign repeat collection
 * @param {Integer} addrid the address id for repeat collection
 * @param {String} notes notes for the repeat collection
 * @param {Integer} transoptid id of the selected transport option
 */
function addRepeatSchedule(personid, addrid, notes, transoptionid)
{
	$j.ajax({
		url: "createschedulerepeat.json",
		data: {
			addrid: addrid,
			personid: personid,
			notes: notes,
			transoptionid: transoptionid
		},
		async: true
	}).done(function(result) {
		if(result.success){
			// assign new repeat schedule and reload the parent page
			parent.location.reload();
		}
		else
		{
			// empty the error warning box
			$j('div.thickbox-box div.warningBox3').empty();
							
			// show error message in error warning box
			$j('div.thickbox-box div:first').removeClass('hid').addClass('vis').find('.warningBox3').append(result.message);
		}
	});
}

/**
 * this method de-activates a repeat schedule.
 * 
 * @param {Integer} rsid id of the repeat schedule to be de-activated
 * @param {Object} link anchor clicked by user
 */
function deactivateRepeatSchedule(rsid, link)
{
	$j.ajax({
		url: "deactivaterepeatschedule.json",
		data: {
			rsid: rsid
		},
		async: true
	}).done(function(result) {
		if(result.success)
		{
			// assign schedule to variable
			var schedule = result.results;
			// update display
			$j(link).parent().html(i18n.t("core.company:contact.deactivated", "De-activated") + '<br />' + schedule.deactivatedByName + '<br />(' + cwms.dateFormatFunction(schedule.deactivatedOn) + ')');
		}
		else
		{
			$j.prompt(result.message);
		}
	});
}

/**
 * 
 * @param activate
 * @param userprefid
 * @param instructiontypeid
 * @return
 */
function insertUserInstructionType(element, userprefid, instructiontypeid)
{
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/userinstructiontypeservice.js'), true,
	{
		callback: function()
		{
			// call dwr method to insert the user instruction type
			userinstructiontypeservice.insertUserInstructionTypeDWR(userprefid, instructiontypeid,
			{
				callback: function(result)
				{
					// result successful
					if (result.success)
					{
						// assign user instruction type
						var uit = result.results;
						// change class and onclick event of element
						$j(element).after('<div class="tick" title="' + i18n.t("core.company:contact.viewingInstructionType", "Viewing this instruction type") + '" onclick=" deleteUserInstructionType(this, ' + uit.id + ', ' + userprefid + ', \'' + instructiontypeid + '\'); return false; ">&nbsp;</div>').remove();
					}
					else
					{
						// display error message
						$j.prompt(result.message);
					}
				}
			});			
		}
	});
}

function deleteUserInstructionType(element, userinstructiontypeid, userprefid, instructiontypeid)
{
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/userinstructiontypeservice.js'), true,
	{
		callback: function()
		{
			// call dwr method to delete the user instruction type
			userinstructiontypeservice.deleteUserInstructionTypeDWR(userinstructiontypeid,
			{
				callback: function(result)
				{
					// result successful
					if (result.success)
					{
						// change class and onclick event of element
						$j(element).after('<div class="cross" title="' + i18n.t("core.company:contact.notViewingInstructionType", "Not viewing this instruction type") + '" onclick=" insertUserInstructionType(this, ' + userprefid + ', \'' + instructiontypeid + '\'); return false; ">&nbsp;</div>').remove();
					}
					else
					{
						// display error message
						$j.prompt(result.message);
					}
				}
			});
		}
	});
}

/* this makes sure that the prefix dont get deleted */
function keepPrefix(prefix){
	var field=$j('#contacthrid');
	var oldvalue=field.val();
    setTimeout(function () {
        if(field.val().indexOf(prefix) !== 0) {
            $j(field).val(oldvalue);
        } 
    }, 1);
}


function getTitlesAutoComplet(selector){
	$j('#titles').autocomplete({
	    source : function(requete, reponse){ 
	    	$j.ajax({
	                url : 'getTitles.json', 
	                dataType : 'json',
	                data : {
						coid : $j('#compid').val(),
						searchText: requete.term
	                },
	                success : function(data){
	                	reponse($j.map(data, function(objet){
	                        return objet.title;
	                    }));
	                }
	            });
	        }
	});
}
