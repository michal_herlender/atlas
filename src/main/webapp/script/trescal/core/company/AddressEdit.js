/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	AddressEdit.js
*	DESCRIPTION		:	This is a page specific javascript file.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	21/06/2007 - SH - Created File
*					:	06/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array ('script/trescal/core/utilities/ScriptedUtils.js');

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'addr1';

function changeVatOverride(radioButton)
{
	var override = radioButton.val();
	if(override == 'true')
	{
		$j('#vatcode').addClass('vis').removeClass('hid');
	}
	else
	{
		$j('#vatcode').addClass('hid').removeClass('vis');
	}
}

/**
 * this function creates the warning message to display when the user has chosen to
 * activate/deactivate an address
 * 
 * @param {String} action is a string indicating whether the deactivate/activate warning message should be created
 * @param {String} address is a string containing the first line of address to be activated/deactivated
 */
function createWarningMessage(action, address)
{
	if (action == 'deactivate')
	{
		var warningText = 	'<span>' + i18n.t("core.company:address.warnOnDeactivate", {address : address, defaultValue : "Please bef aware that deactivating (" + address + ") will result in the following:"}) + '</span>' +
							'<ul>' +
								'<li>' + i18n.t("core.company:address.resultOfDeactivate", {address : address, defaultValue : address + " will no longer appear in any search lists"}) + '</li>' +
							'</ul>';
	}
	else
	{
		var warningText = 	'<span>' + i18n.t("core.company:address.warnOnActivate", {address : address, defaultValue : "Please be aware that activating (" + address + ") will result in the following:"}) + '</span>' +
							'<ul>' +
								'<li>' + i18n.t("core.company:address.resultOfActivate", {address : address, defaultValue : address + " will now appear in search lists"}) + '</li>' +
							'</ul>';
	}
	
	var message = '<div class="warningBox1">' +
						'<div class="warningBox2">' +
							'<div class="warningBox3">' +
								warningText +
							'</div>' +
						'</div>' +
					'</div>';
					
	return message
}

function negateValidation() {
	document.querySelector("#validator").value = "";
	document.querySelector("#validatedon").value = "";
}

function validateAddress() {
	const csrfToken = document.querySelector("meta[name='_csrf']").getAttribute("content");
	var selectedCountry = document.querySelector('#countryId');
    fetch("validateaddress.json", {
        method: 'post',
		headers: {
			Accept: "application/json",
			"Content-Type": "application/json",
            "X-CSRF-TOKEN": csrfToken
        },
        body: JSON.stringify({
        	line1: document.querySelector("#addr1").value,
        	line2: document.querySelector("#addr2").value,
        	line3: document.querySelector("#addr3").value,
        	city: document.querySelector("#town").value,
        	region: document.querySelector("#county").value,
        	postalCode: document.querySelector("#postcode").value,
        	country: selectedCountry.options[selectedCountry.selectedIndex].label
        }),
    }).then(result => {
        return result.json();
    }).then(dto => {
    	if(dto.successful) {
    		var address = dto.validatedAddress;
	    	document.querySelector("#addr1").value = address.line1;
	    	document.querySelector("#addr2").value = address.line2;
	    	document.querySelector("#addr3").value = address.line3;
	    	document.querySelector("#town").value = address.city;
	    	document.querySelector("#county").value = address.region;
	    	document.querySelector("#postcode").value = address.postalCode;
	    	document.querySelector("#latitude").value = address.latitude;
	    	document.querySelector("#longitude").value = address.longitude;
	    	document.querySelector("#validator").value = dto.validator;
	    	document.querySelector("#validatedon").value = dto.validatedOn;
    	}
    });
}