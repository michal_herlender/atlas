/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	CompanyAccounts.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	12/02/2020  - Created File
*
****************************************************************************************************************************************/

function getTitlesAutoComplet(){
	$j('#title').autocomplete({
	    source : function(requete, reponse){ 
	    	$j.ajax({
	                url : 'getTitles.json', 
	                dataType : 'json',
	                data : {
	                	coid : $j('#compid').val() 
	                },
	                success : function(data){
	                	reponse($j.map(data, function(objet){
	                        return objet.title;
	                    }));
	                }
	            });
	        }
	});
}