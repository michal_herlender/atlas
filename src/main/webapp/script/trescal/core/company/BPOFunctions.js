/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	BPOFunctions.js
*	DESCRIPTION		:	These functions are imported on pages that deal with BPOs.
							(i.e. company.vm, subdiv.vm, editperson.vm)
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	
*	KNOWN ISSUES	:	-
*	HISTORY			:	26/09/2007
*					:	06/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
loadScript.loadScripts(new Array('script/thirdparty/jQuery/jquery.tablesorter.js'));

/**
 * variable which holds the id of clicked date input field
 * which is used in method @see {setDateFromDialog}.
 */
var idCurrentInput = '';

/**
 * this method populates the input field selected using id stored in
 * variable @see {idCurrentInput} with the value of parameter date.
 * 
 * @param {String} date the date passed from datepicker jquery plugin
 */
function setDateFromDialog(date)
{
	$j('#' + idCurrentInput).val(date);
}

/**
 * this function creates the content of the thickbox used when creating or editing a blanket purchase order
 * 
 * @param {String} type contains either 'Edit' or 'Create' depending on the functionality required
 * @param {String} entityName is a string containing the entity name (i.e. Company name, Subdiv Name, Contact Name)
 * @param {Integer} id is the id of either a Company/Subdiv/Contact if creating a new bpo or of a bpo if editing a bpo
 * @param {Integer} typecode the code of which bpo to create: 1 = Company, 2 = Subdiv, 3 = Contact
 * @param {String} bponumber is the current bpo number used to prefill input field when editing
 * @param {String} bpocomment is the current bpo comment used to prefill input field when editing
 * @param {String} bpostartdate is the current bpo start date used to prefill input field when editing
 * @param {String} bpoenddate is the current bpo end date used to prefill input field when editing
 * @param {Number} bpolimit is the current bpo limit used to prefill input field when editing
 * @param {Boolean} active indicates whether the BPO is active
 */
function thickboxBPOContent(type, entityName, id, typecode, bponumber, bpocomment, bpostartdate, bpoenddate, bpolimit, active)
{
	// load datepicker stylesheet using the dom if not already 
	loadScript.loadStyle('styles/jQuery/jquery-ui.css', 'screen');
	// load the datepicker javascript file if not already
	loadScript.loadScripts(new Array('script/thirdparty/jQuery/jquery.ui.js'));
	// start creating thickbox content string
	var thickboxContent = 	'<div class="thickbox-box" id="BPOContent">' +
								// add warning box to display error messages if the insertion or update fails
								'<div class="hid warningBox1">' +
									'<div class="warningBox2">' +
										'<div class="warningBox3">' +
										'</div>' +
									'</div>' +
								'</div>' +
								'<fieldset>' +
									'<legend>' + i18n.t("core.company:bpo.title", {typeAction : type, subdiv : entityName, defaultValue : type + " Blanket Purchase Order For " + unescape(entityName)}) + '</legend>' +
									'<ol>' +
										'<li>' +
											'<label>' + i18n.t("core.company:bpo.number", "BPO Number:") + '</label>' +
											'<input type="text" id="bponumber" name="bponumber" value="' + bponumber + '" />' +
											'<span id="poNumber" class="hid attention">' + i18n.t("core.company:bpo.invalidEntry", "Invalid Entry") + '</span>' +
										'</li>' +
										'<li>' +
											'<label>' + i18n.t("core.company:bpo.comment", "BPO Comment:") + '</label>' +
											'<textarea id="bpocomment" name="bpocomment" value="' + bpocomment + '" cols="50" rows="4">' + bpocomment + '</textarea>' +
											'<span id="comment" class="hid attention">' + i18n.t("core.company:bpo.invalidEntry", "Invalid Entry") + '</span>' +
										'</li>' +
										'<li class="relative">' +
											'<label>' + i18n.t("core.company:bpo.startDate", "BPO Start Date:") + '</label>' +
											'<input type="text" id="bpostartdate" name="bpostartdate" value="' + bpostartdate + '" onclick="$j(\'#bpostartdate\').datepicker({ dateFormat: \'dd.mm.yy\', beforeShowDay: $j.datepicker.noWeekends });$j(\'#bpostartdate\').datepicker(\'show\');" readonly="readonly" />' +
											'<span id="durationFrom" class="hid attention">' + i18n.t("core.company:bpo.invalidEntry", "Invalid Entry") + '</span>' +
										'</li>' +
										'<li class="relative">' +
											'<label>' + i18n.t("core.company:bpo.endDate", "BPO End Date:") + '</label>' +
											'<input type="text" id="bpoenddate" name="bpoenddate" value="' + bpoenddate + '" onclick="$j(\'#bpoenddate\').datepicker({ dateFormat: \'dd.mm.yy\', beforeShowDay: $j.datepicker.noWeekends });$j(\'#bpoenddate\').datepicker(\'show\');" readonly="readonly" />' +
											'<span id="durationTo" class="hid attention">' + i18n.t("core.company:bpo.invalidEntry", "Invalid Entry") + '</span>' +
										'</li>' +
										'<li>' +
											'<label>' + i18n.t("core.company:bpo.limit", "BPO Limit:") + '</label>' +
											'<input type="text" id="bpolimit" name="bpolimit" value="' + bpolimit + '" />' +
											'<span id="limitAmount" class="hid attention">' + i18n.t("core.company:bpo.invalidEntry", "Invalid Entry") + '</span>' +
										'</li>';
	// editing an existing BPO				
	if (type == 'Edit')
	{
		thickboxContent = thickboxContent + '<li>' +
												'<label>' + i18n.t("core.company:bpo.active", "Active:") + '</label>';
		// existing BPO is active so tick checkbox
		if (active == true)
		{
			thickboxContent = thickboxContent + '<input type="checkbox" name="active" id="bpoactive" checked="checked" />' +
											'</li>';
		}
		// existing BPO is inactive so don't tick checkbox
		else
		{
			thickboxContent = thickboxContent + '<input type="checkbox" name="active" id="bpoactive" />' +
											'</li>';	
		}
	}
										
	thickboxContent = thickboxContent + '<li id="poSubmit">' +
											'<label>&nbsp;</label>';
	
	// creating a new blanket purchase order					
	if (type == 'Create')
	{
		// add button for creating blanket purchase order to the string
		thickboxContent = thickboxContent + '<input type="button" onclick=" createBPO(' + id + ', \'' + escape(entityName) + '\', ' + typecode + ', $j(\'#bponumber\').val(), $j(\'#bpocomment\').val(), $j(\'#bpostartdate\').val(), $j(\'#bpoenddate\').val(), $j(\'#bpolimit\').val()); return false; " value="' + i18n.t("core.company:bpo.create", "Create BPO") + '" />';
	}
	// editing an existing blanket purchase order
	else
	{
		// add button for editing blanket purchase order to the string
		thickboxContent = thickboxContent + '<input type="button" onclick=" editBPO(' + id + ', \'' + escape(entityName) + '\', ' + typecode + ', $j(\'#bponumber\').val(), $j(\'#bpocomment\').val(), $j(\'#bpostartdate\').val(), $j(\'#bpoenddate\').val(), $j(\'#bpolimit\').val(), $j(\'input#bpoactive\').is(\':checked\')); return false; " value="' + i18n.t("core.company:bpo.save", "Save PO") + '" />';
	}
	// complete thickbox content string
	thickboxContent = thickboxContent +	
										'</li>' +
									'</ol>' +
								'</fieldset>' +
							'</div>';
	
	// return thickbox content
	return thickboxContent;
}

/**
 * this function creates a new  blanket purchase order for the chosen entity and adds it's details
 * to a table on the page.
 * 
 * @param {Integer} id is the id of either a Company/Subdiv/Contact
 * @param {String} entityName is a string containing the entity name (i.e. Company name, Subdiv Name, Contact Name)
 * @param {Integer} typecode the code of which bpo to create: 1 = Company, 2 = Subdiv, 3 = Contact
 * @param {String} bponumber is the current bpo number supplied by the user
 * @param {String} bpocomment is the current bpo comment supplied by the user
 * @param {String} bpostartdate is the current bpo start date supplied by the user
 * @param {String} bpoenddate is the current bpo end date supplied by the user
 * @param {String} bpolimit is the current bpo limit supplied by the user
 */
function createBPO(id, entityName, typecode, bponumber, bpocomment, bpostartdate, bpoenddate, bpolimit)
{	
	$j.ajax({
		url: "createbpo.json",
		data: {
			typecode: typecode,
			number: bponumber,
			comment: bpocomment,
			start: bpostartdate,
			end: bpoenddate,
			limit: bpolimit,
			id: id
		},
		async: true
	}).done(function(result) {
		if(result.hasErrors)
		{
			// empty the error warning box
			$j('div#BPOContent div.warningBox3').empty();
			// hide any old error messages
			$j('span.attention').removeClass('vis').addClass('hid');
			for(var i = 0; i < result.errors.length; i++)
			{
				var error = result.errors[i];
				// show error message next to failed field
				$j('span#' + error.key).removeClass('hid').addClass('vis');
				// show error message in error warning box
				$j('div#BPOContent div:first').removeClass('hid').addClass('vis').find('.warningBox3').append(error.value + '<br />');
			}
		}
		else
		{
			// unwrap results
			var bpo = result.bpo;
			// if this is the first blanket purchase order to be created then remove the empty table message
			if ($j('table#BPOtable tbody:first tr:first td').length == 1)
			{
				$j('table#BPOtable tbody:first tr:first').remove();
			}
			// append the new blanket purchase order to the blanket purchase order table tbody
			$j('table#BPOtable tbody:first').append(	'<tr id="active' + bpo.poId + '">' +
															'<td>' + bpo.poNumber + '</td>' +
															'<td>' + bpo.comment + '</td>' +
															'<td class="center">' + bpo.durationFrom + '</td>' +
															'<td class="center">' + bpo.durationTo + '</td>' +
															'<td class="right">' + bpo.limitAmount.toFixed(2) + '</td>' +
															'<td class="center">' +
															'<a class="mainlink-float" href="'+springUrl+'/viewbpousage.htm?poid='+bpo.poId+'">' +
															i18n.t("common:showAll", "Show All") +
															'</a>' +
															'</td>' +
															'<td class="center">' +
																'<a href="#" class="mainlink-float" id="thickbox" onclick="tb_show(\'' + i18n.t("core.company:bpo.edit", "Edit Blanket Purchase Order") + '\', \'TB_dom?width=600&height=400\', \'\', thickboxBPOContent(\'Edit\', \'' + entityName + '\', ' + bpo.poId + ', ' + typecode + ', \'' + bpo.poNumber + '\', \'' + bpo.comment + '\', \'' + bpo.durationFrom + '\', \'' + bpo.durationTo + '\', ' + bpo.limitAmount + ', true)); return false; " title="">' +
																	'<img src="img/icons/form_edit.png" width="16" height="16" title="' + i18n.t("core.company:bpo.edit", "Edit Blanket Purchase Order") + '" alt="' + i18n.t("core.company:bpo.edit", "Edit Blanket Purchase Order") + '" />' +
																'</a>' +
															'</td>' +
														'</tr>');
			// add one to the active blanket purchase order count
			$j('span#activeBPOCount').text(parseInt($j('span#activeBPOCount').text()) + 1);
			// add one to the blanket purchase order count in tabbed menu
			$j('span#mainBPOCount').text(parseInt($j('span#mainBPOCount').text()) + 1);						
			// close thickbox
			tb_remove();
		}
	});
}

/**
 * this function updates an edited blanket purchase order, removes the current row from the table and appends a new row
 * with the updated details 
 * 
 * @param {Integer} id is the id of either a Company/Subdiv/Contact
 * @param {String} entityName is a string containing the entity name (i.e. Company name, Subdiv Name, Contact Name)
 * @param {Integer} typecode the code of which bpo to create: 1 = Company, 2 = Subdiv, 3 = Contact
 * @param {String} bponumber is the current bpo number supplied by the user
 * @param {String} bpocomment is the current bpo comment supplied by the user
 * @param {String} bpostartdate is the current bpo start date supplied by the user
 * @param {String} bpoenddate is the current bpo end date supplied by the user
 * @param {String} bpolimit is the current bpo limit supplied by the user
 * @param {Boolean} bpoactive is boolean value indicating whether the user wants to activate/deactivate bpo
 * 
 */
function editBPO(id, entityName, typecode, bponumber, bpocomment, bpostartdate, bpoenddate, bpolimit, bpoactive)
{
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/bposervice.js'), true,
	{
		callback: function()
		{
			// call dwr service to update the blanket purchase order
			bposervice.editBPO(id, bponumber, bpocomment, bpostartdate, bpoenddate, bpolimit, bpoactive,
			{	
				callback:function(result)
				{
					// update failed
					if(result.success != true)
					{
						// empty the error warning box
						$j('div#BPOContent div.warningBox3').empty();
						// hide any old error messages
						$j('span.attention').removeClass('vis').addClass('hid');
						
						for(var i = 0; i < result.errorList.length; i++)
						{
							// add current error object to variable
							var error = result.errorList[i];
							// show error message next to failed field
							$j('span#' + error.field).removeClass('hid').addClass('vis');
							// show error message in error warning box
							$j('div#BPOContent div:first').removeClass('hid').addClass('vis').find('.warningBox3').append(error.defaultMessage + '<br />');
						}
					}
					else
					{
						// unwrap results
						var bpo = result.results;
						// BPO is now active
						if (bpo.active)
						{
							// if this is the first active blanket purchase order then remove the empty tbody message
							if ($j('table#BPOtable tbody:first tr:first td').length == 1)
							{
								$j('table#BPOtable tbody:first tr:first').remove();
							}
							// create new content for row
							var activeContent = '<td>' + bpo.poNumber + '</td>' +
												'<td>' + bpo.comment + '</td>' +
												'<td class="center">' + bpo.formattedDateFrom + '</td>' +
												'<td class="center">' + bpo.formattedDateTo + '</td>' +
												'<td class="right">' + bpo.limitAmount.toFixed(2) + '</td>' +
												'<td class="center">' +
													'<a class="mainlink-float" href="'+springUrl+'/viewbpousage.htm?poid='+bpo.poId+'">' +
													i18n.t("common:showAll", "Show All") +
													'</a>' +
												'</td>' +
												'<td class="center">' +
													'<a href="#" class="mainlink-float" id="thickbox" onclick=" tb_show(\'' + i18n.t("core.company:bpo.edit", "Edit Blanket Purchase Order") + '\', \'TB_dom?width=600&height=400\', \'\', thickboxBPOContent(\'Edit\', \'' + entityName + '\', ' + bpo.poId + ', ' + typecode + ', \'' + bpo.poNumber + '\', \'' + bpo.comment + '\', \'' + bpo.formattedDateFrom + '\', \'' + bpo.formattedDateTo + '\', ' + bpo.limitAmount + ', true)); return false; " title="">' +
														'<img src="img/icons/form_edit.png" width="16" height="16" title="' + i18n.t("core.company:bpo.edit", "Edit Blanket Purchase Order") + '" alt="' + i18n.t("core.company:bpo.edit", "Edit Blanket Purchase Order") + '" />' +
													'</a>' +
												'</td>';
							// if this BPO is already active then empty old content and append new content
							if ($j('table#BPOtable tbody:first tr#active' + bpo.poId).length == 1)
							{
								$j('table#BPOtable tbody:first tr#active' + bpo.poId).empty().append(activeContent);
							}
							// add to inactive tbody
							else
							{
								// remove BPO from the inactive table
								$j('table#BPOtable tbody:last tr#inactive' + bpo.poId).remove();
								// subtract one from the inactive blanket purchase order count
								$j('span#inactiveBPOCount').text(parseInt($j('span#inactiveBPOCount').text()) - 1);
								// add one to the active blanket purchase order count
								$j('span#activeBPOCount').text(parseInt($j('span#activeBPOCount').text()) + 1);
								// last inactive blanket purchase order has been removed so display message
								if ($j('table#BPOtable tbody:last tr').length < 1)
								{
									$j('table#BPOtable tbody:last').append(	'<tr class="center bold">' +
																				'<td colspan="8">' + i18n.t("core.company:bpo.noInactive", "There are no inactive BPOs") + '</td>' +
																			'</tr>');
								}
								// append the new active blanket purchase order to the active blanket purchase order tbody
								$j('table#BPOtable tbody:first').append(	'<tr id="active' + bpo.poId + '">' +
																				activeContent +
																			'</tr>');
							}
						}
						else
						{
							// if this is the first inactive blanket purchase order then remove the empty tbody message
							if ($j('table#BPOtable tbody:last tr:first td').length == 1)
							{
								$j('table#BPOtable tbody:last tr:first').remove();
							}
							// create new content for row
							var inactiveContent = 	'<td>' + bpo.poNumber + '</td>' +
													'<td>' + bpo.comment + '</td>' +
													'<td class="center">' + bpo.formattedDateFrom + '</td>' +
													'<td class="center">' + bpo.formattedDateTo + '</td>' +
													'<td class="right">' + bpo.limitAmount.toFixed(2) + '</td>' +
													'<td class="center">' +
													'<a class="mainlink-float" href="'+springUrl+'/viewbpousage.htm?poid='+bpo.poId+'">' +
													i18n.t("common:showAll", "Show All") +
													'</a>' +
													'</td>' +
													'<td class="center">' +
														'<a href="#" class="mainlink-float" id="thickbox" onclick=" tb_show(\'' + i18n.t("core.company:bpo.edit", "Edit Blanket Purchase Order") + '\', \'TB_dom?width=600&height=400\', \'\', thickboxBPOContent(\'Edit\', \'' + entityName + '\', ' + bpo.poId + ', ' + typecode + ', \'' + bpo.poNumber + '\', \'' + bpo.comment + '\', \'' + bpo.formattedDateFrom + '\', \'' + bpo.formattedDateTo + '\', ' + bpo.limitAmount + ', false)); return false; " title="">' +
															'<img src="img/icons/form_edit.png" width="16" height="16" title="' + i18n.t("core.company:bpo.edit", "Edit Blanket Purchase Order") + '" alt="' + i18n.t("core.company:bpo.edit", "Edit Blanket Purchase Order") + '" />' +
														'</a>' +
													'</td>';
							// if this BPO is already inactive then empty old content and append new content
							if ($j('table#BPOtable tbody:last tr#inactive' + bpo.poId).length == 1)
							{
								$j('table#BPOtable tbody:last tr#inactive' + bpo.poId).empty().append(inactiveContent);
							}
							else
							{
								// remove BPO from the active tbody
								$j('table#BPOtable tbody:first tr#active' + bpo.poId).remove();
								// subtract one from the active blanket purchase order count
								$j('span#activeBPOCount').text(parseInt($j('span#activeBPOCount').text()) - 1);
								// add one to the active blanket purchase order count
								$j('span#inactiveBPOCount').text(parseInt($j('span#inactiveBPOCount').text()) + 1);
								// last active blanket purchase order has been removed so display message
								if ($j('table#BPOtable tbody:first tr').length < 1)
								{
									$j('table#BPOtable tbody:first').append('<tr class="center bold">' +
																				'<td colspan="8">' + i18n.t("core.company:bpo.noInactive", "There are no active BPOs") + '</td>' +
																			'</tr>');
								}
								// append the new inactive blanket purchase order to the inactive blanket purchase order tbody
								$j('table#BPOtable tbody:last').append(	'<tr id="inactive' + bpo.poId + '">' +
																			inactiveContent +	
																		'</tr>');
							}
						}						
						// close thickbox
						tb_remove();
					}
				}
			});
		}
	});
}
