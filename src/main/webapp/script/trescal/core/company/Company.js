/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	Company.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	26/10/2006 - SH - Created File
*					:	06/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	
								'script/trescal/core/utilities/ScriptedUtils.js',
								'script/trescal/core/company/BPOFunctions.js',
								'script/thirdparty/jQuery/jquery.scrollTo.js',
								'script/trescal/core/tools/FileBrowserPlugin.js',
								'script/thirdparty/jQuery/jquery.boxy.js');

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' },
								{ element: 'ul.subnavtab a', corner: 'small transparent top' } );

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init() {

	// load stylesheet using the dom if not already
	loadScript.loadStyle('styles/jQuery/jquery-boxy.css', 'screen');
	
}

/**
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements;
menuElements = [
	{anchor: 'subdivs-link', block: 'subdivs-tab'},
    {anchor: 'accred-link', block: 'accred-tab'},
	{anchor: 'bankaccounts-link', block: 'bankaccounts-tab'},
	{anchor: 'bpo-link', block: 'bpo-tab'},
	{anchor: 'history-link', block: 'history-tab'},
	{anchor: 'files-link', block: 'files-tab'},
	{anchor: 'instructions-link', block: 'instructions-tab'},
	{anchor: 'settings-link', block: 'settings-tab'},
	{anchor: 'finance-link', block: 'finance-tab'},
	{anchor: 'usergroups-link', block: 'usergroups-tab'},
	{anchor: 'flexiblefields-link', block: 'flexiblefields-tab'},
	{anchor: 'exchangeformat-link', block: 'exchangeformat-tab'},
	{anchor: 'couriers-link', block: 'couriers-tab'}
];

	
/**
 * this method scrolls a div with overflow set to scroll to a specified point.
 * 
 * @param {String} contact indicates if contact is 'existing' or 'custom'
 * @param {String} address indicates if address is 'existing' or 'custom'
 */
function scrollToSelected(contact, address)
{
	// existing contact?
	if (contact == 'EXISTING')
	{
		// scroll existing contact div to the checked contact input
		$j('div#existingContact').scrollTo( 'input:checked', 0 );
	}
	// existing address?
	if (address == 'EXISTING')
	{
		// scroll existing address div to the checked address input
		$j('div#existingAddress').scrollTo( 'input:checked', 0 );
	}	
}

/**
 * this function sets the chosen subdivision as the default
 * 
 * @param {Integer} coid is the id of the company whose subdivision should be set
 * @param {Integer} subdivid is the id of the subdivision to be set as default
 * @param {Object} img which has been clicked on and whose attributes need changing
 */
function updateCompanySubdivDefault(coid, subdivid, img)
{
	if (img.id != 'defaultSubdiv')
	{
		// change the default subdivision
		$j.ajax({
			url: "updatecompanysubdivdefault.json",
			data: {
				coid: coid,
				subdivid: subdivid
				},
			async: true
		});
		// get the current default subdivision image and change properties
		$j('#defaultSubDiv').attr('src', 'img/icons/building.png')
							.attr('alt', i18n.t("core.company:company.makeSubdivDefault", "Make Subdivision Default"))
							.attr('title', i18n.t("core.company:company.makeSubdivDefault", "Make Subdivision Default"))
							.attr('id', '');
		// change the properties of the chosen subdivision image
		img.src = 'img/icons/building_default.png';
		img.alt = i18n.t("core.company:company.defaultSubdiv", "Default Subdivision");
		img.title = i18n.t("core.company:company.defaultSubdiv", "Default Subdivision");
		img.id = 'defaultSubDiv';
	}
	else
	{
		return false;
	}
	
}

/**
 * this function creates the warning message to display when the user has chosen to
 * activate/deactivate a company
 * 
 * @param {String} action is a string indicating whether the deactivate/activate warning message should be created
 * @param {String} company is a string containing the name of the company that is to be (activated/deactivated)
 */
function createActivationWarningMessage(action, company)
{
	if (action == 'deactivate')
	{
		var warningText = 	'<span>' + i18n.t("core.company:company.warnOnDeactivate", {company : company, defaultValue : "Please be aware that <strong>deactivating</strong> (" + company + ") will result in the following:"}) + '</span>' +
							'<ul>' +
								'<li>' + i18n.t("core.company:company.resultOfDeactivate1", {company : company, defaultValue : company + " will no longer appear in any search lists"}) + '</li>' +
								'<li>' + i18n.t("core.company:company.resultOfDeactivate2", {company : company, defaultValue : "It will not be possible to book jobs in against contacts for " + company}) + '</li>' +
							'</ul>';
	}
	else
	{
		var warningText = 	'<span>' + i18n.t("core.company:company.warnOnActivate", {company : company, defaultValue : "Please be aware that <strong>activating</strong> (" + company + ") will result in the following:"}) + '</span>' +
							'<ul>' +
								'<li>' + i18n.t("core.company:company.resultOfActivate1", {company : company, defaultValue : company + " will now appear in search lists"}) + '</li>' +
								'<li>' + i18n.t("core.company:company.resultOfActivate2", {company : company, defaultValue : "It will be possible to book jobs in against contacts for " + company}) + '</li>' +
							'</ul>';
	}
	
	var message = '<div class="warningBox1">' +
						'<div class="warningBox2">' +
							'<div class="warningBox3">' +
								warningText +
							'</div>' +
						'</div>' +
					'</div>';
					
	return message
}

/**
 * this function creates a warning message to display when the user has chosen to
 * set company to (Onstop/Trusted)
 * 
 * @param {String} action is a string value that holds either (onstop/trusted) depending on message to be created
 * @param {String} coname is the name of the company that is to be set to (onstop/trusted)
 */
function createOnstopWarningMessage(action, coname)
{
	// unescape company name
	coname = unescape(coname);
	// create warning text variable
	var warningText = '';
	// action?
	if (action == 'onstop')
	{
		warningText += 	'<span>' + i18n.t("core.company:company.warnOnStop", {company : coname, defaultValue : "Please be aware that setting (' + coname + ') to <strong>onstop</strong> will result in the following:"}) + '</span>' +
						'<ul>' +
							'<li>' + i18n.t("core.company:company.resultOfStop1",  {company : coname, defaultValue : coname + " will no longer appear in any search lists"}) + '</li>' +
							'<li>' + i18n.t("core.company:company.resultOfStop2",  {company : coname, defaultValue : "It will not be possible to book jobs in against contacts for " + coname}) + '</li>' +
						'</ul>';
	}
	else
	{
		warningText += 	'<span>' + i18n.t("core.company:company.warnOnTrust", {company : coname, defaultValue : "Please be aware that setting (' + coname + ') to <strong>trusted</strong> will result in the following:"}) + '</span>' +
						'<ul>' +
							'<li>' + i18n.t("core.company:company.resultOfTrust1", {company : coname, defaultValue : coname + " will now appear in search lists"}) + '</li>' +
							'<li>' + i18n.t("core.company:company.resultOfTrust2", {company : coname, defaultValue : "It will be possible to book jobs in against contacts for " + coname}) + '</li>' +
						'</ul>';
	}
	
	var message = '<div class="warningBox1">' +
						'<div class="warningBox2">' +
							'<div class="warningBox3">' +
								warningText +
							'</div>' +
						'</div>' +
					'</div>';

	return message; 
}

/**
 * this function is intended for use when toggling a company between (onstop/trusted)
 * 
 * @param {String} title the title of overlay
 * @param {String} coname is the name of the company we are toggling
 * @param {Integer} coid is the id of the company we are toggling
 * @param {Integer} personid is the id of the user who is setting this status
 * @param {Boolean} onstop is the value to toggle (e.g. if currently true then change to false)
 * @param {String} warningMessage contains the warning text to display
 */
function toggleContentForOnstop(title, coname, coid, personid, onstop, warningMessage)
{
	// unescape company name
	coname = unescape(coname);
	// populate variables with text to use in overlay and onstop variable to true
	// in preparation for creating thickbox content when onstop parameter is false.
	var objText = i18n.t("core.company:company.setCompanyToStop", {company : coname, defaultValue : "Set " + coname + " to On-stop"});
	var labelText = i18n.t("core.company:company.setToStop", "Set to On-stop");
	var onstopObj = true;
	
	// onstop parameter is in fact true so reset all variables with text to use in thickbox window
	// and onstopObj variable to false.
	if (onstop == true)
	{
		objText = i18n.t("core.company:company.setCompanyToTrusted", {company : coname, defaultValue : "Set " + coname + " to Trusted"});
		labelText = i18n.t("core.company:company.setToTrusted", "Set to Trusted");
		onstopObj = false;
	}
	
	// create content to be displayed in overlay using previously populated variables
	var content = 	'<div id="toggleCompanyOnstopOverlay">' +
						warningMessage +
						'<fieldset>' +
							'<ol>' +
								'<li>' +
									'<p class="bold">' + objText + '</p>' +
								'</li>';
								if (onstopObj)
								{
									content += 	'<li>' +
													'<label>' + i18n.t("core.company:company.stopReason", "On-stop Reason:") + '</label>' +
													'<textarea name="onstopreason" id="onstopreason" rows="4" class="width70"></textarea>' +
												'</li>';
								}
					content +=	'<li>' +
									'<label>' + labelText + '</label>' +
									'<span><a href="#" onclick="event.preventDefault(); updateOnstopStatus(\'' + escape(coname) + '\', ' + coid + ', ' + onstopObj + '); " class="mainlink">' + labelText + '</a></span>' +
								'</li>' +
							'</ol>' +
						'</fieldset>' +
					'</div>';
	// create overlay
	loadScript.createOverlay('dynamic', title, null, content, null, 42, 720, null);
}

/**
 * this function updates the status of a company to either 'onstop' or 'trusted'
 * 
 * @param {String} coname is the name of the company we are updating
 * @param {Integer} coid is the id of the company we are updating
 * @param {Boolean} onstop is the value to toggle (e.g. if currently true then change to false)
 * @param {Integer} personid is the id of the user who is updating this status 
 */
function updateOnstopStatus(coname, coid, onstop)
{
	coname = unescape(coname);
	// text to be displayed once the dwr call has been completed
	var returntext = '';
	// if setting to onstop we need to get the reason
	var onstopreason = '';
	// status that the object has been changed to
	var currentStatusKey = 'trusted';
	// status that the object had before this change
	var previousStatusKey = 'on-stop';
	if (onstop) {
		onstopreason = $j('div#toggleCompanyOnstopOverlay textarea#onstopreason').val();
		currentStatusKey = 'on-stop';
		previousStatusKey = 'trusted';
	}
	$j.ajax({
		url: "changecompanyonstop.json",
		data: {
			coid: coid,
			stopit: onstop,
			reason: onstopreason},
		async: true
	}).done(function(result) {
		// create the message to return
		returntext = 	'<p class="center">' + i18n.t("core.company:company.updateStatus", {currentStatus : currentStatusKey, defaultValue : "The status of this company has been set to <strong>" + currentStatusKey + "</strong>."}) + '</p>';
		// set company to onstop
		if (onstop)
		{
			$j('#subdivform').after('<div id="onstop" class="warningBox1">' +
										'<div class="warningBox2">' +
											'<div class="warningBox3">' +
												'<div class="center-0">' +
													i18n.t("core.company:company.currentStatus", {company : coname, user : result.user, date : result.date, defaultValue : coname + " is currently <strong>On-stop!</strong> (Set by " + result.user + " on " + result.date + ")"}) + '<br/>' +
													'<span class="bold">' + onstopreason + '</span>' +																
												'</div>' +
											'</div>' +
										'</div>' +
									'</div>');
								
			$j('div#subdivs-tab tbody td.name a').before('<img src="img/icons/flag_red.png" width="12" height="12" alt="' + i18n.t("core.company:company.subdivCompanyStop", "Subdivision Company Onstop") + '" title="' + i18n.t("core.company:company.subdivCompanyStop", "Subdivision Company Onstop") + '" /> ');
		}
		else
		{
			$j('div').remove('#onstop');
			$j('div#subdivs-tab tbody td.name img').remove();
		}
		$j('#' + currentStatusKey).removeClass().addClass('vis');
		$j('#' + previousStatusKey).removeClass().addClass('hid');
		// modify overlay
		loadScript.modifyOverlay(null, returntext, '', false, 42, 720, null);
	}).fail(function(result) {
		// show error message to user
		loadScript.modifyOverlay(null, result.message, '', true, null, null, null);
	});
}

/**
 * this function delete the bankaccount if its not used in companysettings
 * 
 * @param baid {Integer} bank account id
 * @param coid {Integer} company id
 */
function deleteBankAccount(baid, coid) {
	
	$j.prompt(i18n.t("bankaccountdelete", "Are you sure you wish to delete this Bank Account?" ),
			{
				submit: deleteBA,
				buttons:
				{
					Yes: true,
					No: false
				},
				focus: 2
			});
	
	function deleteBA(v,m) {
		if (m) {
			$j.ajax({
				url: "deletebankaccount.json",
				data: {
					groupid: baid}
			}).done(function(result) {
				// create the message to return
				returntext = "";
				if (!result) 
				{
					$j.prompt(i18n.t("core.company:company.bankaccountinuse", "Bank Account is used in Company Settings and cannot be deleted."));
				}
				else
				{
					window.location.href = 'viewcomp.htm?loadtab=bankaccounts-tab&coid=' + coid + '';
				}
				
			}).fail(function(result) {
				// show error message to user
				$j.prompt(result.message);
			});
		}
	}
}