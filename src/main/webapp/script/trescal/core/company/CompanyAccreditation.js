/**
 * array of calendar objects containing the initial setup literals needed
 * 
 * @param {String} inputField is the id of the input field to which the calendar should be binded
 * @param {String} dateFormat is the format that the date should be displayed
 * @param {String} displayDates 'all' make all dates available
 * 								'future' show only dates in the future
 * 								'past' show only dates in the past
 * applied in init() function
 */						
var jqCalendar = new Array (); 

/**
 * loop through the 'idArray' which is populated in the head section of the page with all the id's
 * for company accreditation date fields using 'compAccredDate' text followed by the
 * '$supplier.supplierid' velocity variable to distinguish.  Each id is used to generate a new calendar
 * instance.
 */
//for (i = 0; i < idArray.length; i++)
//{
//	jqCalendar.push({inputField: idArray[i], dateFormat: 'dd.mm.yy', displayDates: 'future' });
//}