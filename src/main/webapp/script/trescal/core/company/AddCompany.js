/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	AddCompany.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	Commenting of code.
*	KNOWN ISSUES	:	-
*	HISTORY			:	26/10/2006
*					:	16/12/2015 - Fix bug on changeRole : use "val" (fix value) function and not "html" function (name depend of the language) to get the role of the company
*****************************************************************************************************************************************/


/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'coname';
		
function changeRole(selectBox)
{
	var role = selectBox.val();
	if(role == 'BUSINESS')
	{
		$j('#business').addClass('vis').removeClass('hid');
		$j('#client').addClass('hid').removeClass('vis');
	}
	else if(role == 'CLIENT')
	{
		$j('#client').addClass('vis').removeClass('hid');
		$j('#business').addClass('hid').removeClass('vis');
	}
	else
	{
		$j('#client').addClass('hid').removeClass('vis');
		$j('#business').addClass('hid').removeClass('vis');
	}
}