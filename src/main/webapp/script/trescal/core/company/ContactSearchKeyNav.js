/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	ContactSearchKeyNav.js
*	DESCRIPTION		:	This file contains the keyboard navigation code which allows the user to navigate
* 					:	up and down the contact search results box and hit enter to select a contact.
* 					:	Have put this code in a separate file because it needs to be loaded after the jquery.js file. 
*
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	12/07/2007 - SH - Created File
*
****************************************************************************************************************************************/

/**
 * start jquery keypress observer to check if the user has pressed the up/down arrow or the
 * enter key when searching for contacts
 */ 
$j(document).ready( function() {
 
 	$j(document).keydown( function(e){
    
    var event = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
		
		// see if the down arrow (40) or up arrow (38) has been pressed?
		switch (event)
		{
			
			//  up arrow key has been pressed
			case 38: // check selected variable is not equal to zero
					 if (SELECTED == 0)
					 {
					 	
					 }
					 // selected variable contains value greater than zero so move up
					 else
					 {
					 	 // remove 'selected' class name from the currently selected element
						 $j('#contactDiv div').eq(SELECTED).removeClass();
						 // remove one from the selected variable value
						 SELECTED = SELECTED - 1;
						 // add 'selected' class name to the newly selected element
						 $j('#contactDiv div').eq(SELECTED).addClass('selected');
						 // put focus on selected anchor so window scrolls
						 $j('#contactDiv div .conname')[SELECTED].focus();
						 // then return focus to the input field
						 $j('#lastName').focus();
					 }
					 // set this trigger variable to true so that the company ajax is not called
					 contactKeyPress = true;
					 break;
					 
			// down arrow key has been pressed
			case 40: // check selected variable is not equal to the number of anchors
					 if (SELECTED == $j('#contactDiv div').size() - 1)
					 {
					 
					 }
					 // selected variable contains value less than anchors length so move down
					 else
					 {
					 	// remove 'selected' class name from the currently selected element
						$j('#contactDiv div').eq(SELECTED).removeClass();
						// remove one from the selected variable value
						SELECTED = SELECTED + 1;
						// add 'selected' class name to the newly selected element
						$j('#contactDiv div').eq(SELECTED).addClass('selected');
						// put focus on selected anchor so window scrolls
						$j('#contactDiv div .conname')[SELECTED].focus();
						// then return focus to the input field
						$j('#lastName').focus();
					 }
					 // set this trigger variable to true so that the company ajax is not called
					 contactKeyPress = true;
					 break;
			
			// enter key has been pressed
			case 13: // set keypress variable to true so that the ajax function is not called
					 contactKeyPress = true;
					 // add href value from the selected anchor to the browser address bar
					 window.location.href = $j('#contactDiv div').eq(SELECTED).find('a:first').attr('href');
					 break;
					 
			// left arrow key has been pressed
			case 37: // set keypress variable to true so that the ajax function is not called.
					 // no other functionality, included this code only to stop the list being reloaded
					 // if this key is pressed mistakenly 
					 contactKeyPress = true;
					 break;
					 
			// right arrow key has been pressed
			case 39: // set keypress variable to true so that the ajax function is not called.
					 // no other functionality, included this code only to stop the list being reloaded
					 // if this key is pressed mistakenly 
					 contactKeyPress = true;
					 break;
					 
		}
	});
});