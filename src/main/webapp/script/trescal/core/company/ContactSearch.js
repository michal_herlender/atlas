/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	ContactSearch.js
*	DESCRIPTION		:	This file contains a method which retrieves a list of contacts
*					  	These contacts are displayed as a list of anchors within a div
* 						Once submitted the user is taken to the contact profile page
* 
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	26/10/2006 - SH - Created File
*					:	06/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/


/**
 * Array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'script/trescal/core/company/ContactSearchKeyNav.js');

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'lastName';

/**
 * variable used to control the company ajax when a cursor key is pressed
 * (true) then either the up/down cursor arrow has been pressed and the company ajax should not be loaded
 * (false) then neither of these keys has been pressed and the company ajax can be loaded
 */
var contactKeyPress = false;

/**
 * global variable which holds the last time ajax call 'contactCallback' was last implemented
 */ 
var contactLastUpdate = 0;

/**
 * this function clears the results div and all input fields
 */
function clearForm()
{
	// clear input fields
	$j('#firstName').attr('value', '');
	$j('#lastName').attr('value', '').focus();
	// remove all contact anchors from the contact div
	$j('#contactDiv').empty();
}

function onClickCompanyRole() {
	var allChecked = true;
	ALLCOROLES.forEach(function(companyRole) {
		if(!$j('#'+companyRole).attr('checked')) allChecked = false;
	});
	if(allChecked) {
		$j('#searchAll').attr('type', 'hidden');
		$j('#searchDefault').attr('type', 'button');
	}
	else {
		$j('#searchAll').attr('type', 'button');
		$j('#searchDefault').attr('type', 'hidden');
	}
}

function selectOnly(selectRole) {
	ALLCOROLES.forEach(function(companyRole) {
		$j('#'+companyRole).removeAttr('checked');
	});
	$j('#'+selectRole).attr('checked', true);
	$j('#searchAll').attr('type', 'button');
	$j('#searchDefault').attr('type', 'hidden');
}

function selectAll() {
	$j('.cbrole').attr('checked', 'checked');
	$j('#searchAll').attr('type', 'hidden');
	$j('#searchDefault').attr('type', 'button');
}

function selectDefault() {
	selectOnly('CLIENT');
	$j('#searchAll').attr('type', 'button');
	$j('#searchDefault').attr('type', 'hidden');
}

/**
 * this function retrieves all contacts from the database whose name matches the string fragments passed
 * to it, the returned data is then passed on to @method contactCallback() for display
 *
 * @param {String} firstName is the value entered in the first name input field
 * @param {String} lastName	is the value entered in the last name input field
 */
function runContactAjax(firstName, lastName)
{
	var selectedRoles = [];
	ALLCOROLES.forEach(function(companyRole) {
		if ($j('#'+companyRole).attr('checked')) {selectedRoles.push(companyRole)}
	});
	// the up/down arrow key has been pressed so set value back to false and skip ajax loading
	if (contactKeyPress == true)
	{
		contactKeyPress = false;
	}
	else
	{
		// remove all company anchors from the company div
		$j('#contactDiv').empty();
		// this div displays the loading image and text when a search has been activated
		$j('#contactDiv').append(	'<div id="loading-search">' +
										'<img src="img/icons/loading-anim.gif" class="img_marg_bot" height="18" width="80" alt="" />' +
									'</div>');
		// set active variable to true to return all currently active companies
		var active = true;
		// if user has ticked the de-active checkbox then return all currently
		// inactive companies
		if ($j('#active').prop('checked'))
		{
			active = false;
		}
		console.log("active : "+active);
		var currentTime = new Date().getTime();
		if(firstName.length > 0 || lastName.length > 0)
		{
			$j.ajax({
				url: "contactsearchrequest.json",
				data: {
					firstName: firstName,
					lastName: lastName,
					active: active,
					companyRoles: selectedRoles},
				async: true
			}).done(function(dataFromServer) {
				contactCallback(dataFromServer, currentTime);
			}).fail(function(result) {
				alert(i18n.t("core.company:search.contactSearchFailed", "Contact search request unsuccessful"));
			});
		}
		else
		{
			// remove all previously displayed contacts from the contact div
			$j('#contactDiv').empty();
			// hide the loading image and text
			$j('#loading').removeClass().addClass('hid');
		}
	}
}

/**
 * this function displays the results returned from the server for @method runContactAjax()
 *
 *	@param {Array} contacts is an array of contact objects returned from the server
 *	@param {Date/Time} currentUpdate is a date/time value of data retrieval from the server
 */
function contactCallback(contacts, currentUpdate)
{
	// this is a new search
	if (currentUpdate > contactLastUpdate)
	{
		// update value in the contactLastUpdate variable
		contactLastUpdate = currentUpdate
		
		// at least one contact returned
		if(contacts == null)
		{
			$j.prompt(i18n.t("core.company:search.noCoroleSelected", "No corole was selected, please select a corole before searching"));
		}
		else if(contacts.length > 0)
		{
			// remove all previously displayed contacts from the contact div
			$j('#contactDiv').empty();
			// create variable to hold contacts results
			var content = '';
			// add links (contact + company) for each contact returned
			$j.each(contacts, function(i)
			{
				content = content + '<div>' +
										'<a href="viewperson.htm?personid=' + contacts[i].personid + '" class="conname">' + contacts[i].name + '</a>' +
										'<a href="viewcomp.htm?coid=' + contacts[i].coid + '" class="compname">' + contacts[i].coname + ' - ' + contacts[i].subname +' (' + contacts[i].corole + ')</a>' +
									'</div>';
			});
			// append all contacts to list
			$j('#contactDiv').append(content);
			// add 'selected' class name to first div in list
			$j('#contactDiv div:first').addClass('selected');
		}
		else
		{
			// remove all previously displayed contacts from the contact div
			$j('#contactDiv').empty();
			// create div to display message when no contacts have been returned
			$j('#contactDiv').append('<div class="bold center" style=" padding: 5px; ">' + i18n.t("core.company:search.noConFound", "No Contacts Found") + '</a>');
		}	
		
	}
	// hide the loading image and text
	$j('#loading').removeClass().addClass('hid');
}
