/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	CompanySearch.js
*	DESCRIPTION		:	This file contains functions to get a list of contacts using DWR from the database and display them in
*					  	a select drop down.  Once submitted the user is taken to the contact profile page.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	26/10/2006 - SH - Created File
*					:	06/10/2015	- TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array();

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'coname';

/**
 * @param {Date/Time} companyLastUpdate is a date/time value to test dwr ajax call
 */
var companyLastUpdate = 0;

/**
 * variable used to control the company ajax when a cursor key is pressed
 * (true) then either the up/down cursor arrow has been pressed and the company ajax should not be loaded
 * (false) then neither of these keys has been pressed and the company ajax can be loaded
 */
var companyKeyPress = false;

function onClickCompanyRole() {
	var allChecked = true;
	ALLCOROLES.forEach(function(companyRole) {
		if(!$j('#'+companyRole).attr('checked')) allChecked = false;
	});
	if(allChecked) {
		$j('#searchAll').attr('type', 'hidden');
		$j('#searchDefault').attr('type', 'button');
	}
	else {
		$j('#searchAll').attr('type', 'button');
		$j('#searchDefault').attr('type', 'hidden');
	}
}

function selectOnly(selectRole) {
	ALLCOROLES.forEach(function(companyRole) {
		$j('#'+companyRole).removeAttr('checked');
	});
	$j('#'+selectRole).attr('checked', true);
	$j('#searchAll').attr('type', 'button');
	$j('#searchDefault').attr('type', 'hidden');
}

function selectAll() {
	$j('.cbrole').attr('checked', 'checked');
	$j('#searchAll').attr('type', 'hidden');
	$j('#searchDefault').attr('type', 'button');
}

function selectDefault() {
	selectOnly('CLIENT');
	$j('#searchAll').attr('type', 'button');
	$j('#searchDefault').attr('type', 'hidden');
}

/**
 * this function retrieves all companies from the database whose name matches the string fragment passed
 * to it, the returned data is then passed on to @method companyCallback() for display
 *
 * @param {Boolean} searchHistory indicates if the user would like to search the company name history
 * @param {String} nameFragment is the value entered in the company input field on page
 */
function runCompanyAjax(nameFragment)
{
	var selectedRoles = [];
	ALLCOROLES.forEach(function(companyRole) {
		if ($j('#'+companyRole).attr('checked')) {selectedRoles.push(companyRole)}
	});
	if (companyKeyPress) {
		// the up/down arrow key has been pressed so set value back to false and skip ajax loading
		companyKeyPress = false;
	}
	else {
		// reset the selected anchor for key navigation
		SELECTED = 0;
		// remove all company anchors from the company div
		$j('#companyDiv').empty();
		// this div displays the loading image and text when a search has been activated
		$j('#companyDiv').append(
				'<div id="loading-search">' +
				'<img src="img/icons/loading-anim.gif" class="img_marg_bot" height="18" width="80" alt="" />' +
				'</div>');
		// set variable to true to retrieve all active companies
		var active = $j('input[name=activeradio]:checked').val() == 'active';
		var activeAnywhere = $j('input[name=activeradio]:checked').val() == 'activeanywhere';
		var searchField = $j('input[name=historyradio]:checked').val();
		var lookupOldNames = $j('input[name=historyradio]:checked').val() == 'HISTORIC_NAME';
		// set date/time value to test ajax call
		var currentTime = new Date().getTime();
		// text entered has a length greater than zero (stops unconstrained retrieval)
		if (nameFragment.length > 0) {
			fetch(cwms.urlWithParamsGenerator("companysearchrequest.json",{
					searchName: nameFragment,
					active: active,
					activeAnywhere: activeAnywhere,
					searchField: searchField,
					companyRoles: selectedRoles
			})).then(res => res.ok?res.json():Promise.reject(res.statusText))
			.then(data => companyCallback(lookupOldNames,data,currentTime))
			.catch(() => alert(i18n.t("core.company:search.companySearchFailed", "Company search request unsuccessful")));
		}
		else
		{
			// remove all company anchors from the company div
			$j('#companyDiv').empty();
			// hide the loading image and text
			$j('#loading').removeClass().addClass('hid');
		}
	}
}

/**
 * this function displays the results returned from the server for @method runCompanyAjax()
 *
 * @param {Boolean} historic are we to display historic company names?
 * @param {Array} companies is an array of company objects returned from the server
 * @param {Date/Time} lastUpdate is a date/time value of data retrieval from the server
 */
function companyCallback(historic, companies, lastUpdate)
{
	// this is the latest company callback
	if (lastUpdate > companyLastUpdate)
	{
		// update value in the companyLastUpdate variable
		companyLastUpdate = lastUpdate;
		// at least one company returned
		if(companies == null)
		{
			$j.prompt(i18n.t("core.company:search.noCoroleSelected", "No corole was selected, please select a corole before searching"));
		}
		else if(companies.length > 0)
		{
			// empty all companies from the div
			$j('#companyDiv').empty();
			// create variable to hold companies results
			var content = '';
			// add link for each company returned
			$j.each(companies, function (i, comp)
			{
				content += 	'<a href="' + 'viewcomp.htm?coid=' + comp.coid + '" ';
							if (!historic)
							{
								if (comp.blocked)
								{
									content += 'class="compBlocked" title="' + i18n.t("core.company:search.compNotAccredited", "Company is not accredited") + '"';
								}
							}
							else
							{
								content += 'class="companyHist" title="' + i18n.t("core.company:search.prevCompName", "Previous company name") + '"';
							}
				content +=	'><span class="compname">' + comp.coname + '';
							if (historic)
							{
								content += ' <span style=" font-size: smaller; ">[' + i18n.t("core.company:search.now", "Now") + ' ' + comp.newconame + ']</span>';
							}
				content +=	'</span>' +
							'<span class="corolename">' + comp.corole + '</span>' +
							'<span class="legalidentifier">' + comp.legalIdentifier + '</span></a>';
			});
			// append all companies to list
			$j('#companyDiv').append(content);
			// add 'selected' class name to first anchor
			$j('#companyDiv a:first').addClass('selected');
		}
		else
		{
			// remove all company anchors from the company div
			$j('#companyDiv').empty();
			// add message when no companies have been returned
			$j('#companyDiv').append('<div class="bold center" style=" padding: 5px; ">' + i18n.t("core.company:search.noCompFound", "No Companies Found") + '</div>');
		}

	}
	// hide the loading image and text
	$j('#loading').removeClass().addClass('hid');
}

/**
 * variable used to hold the position of the currently selected company in the list
 */
var SELECTED = 0;

/**
 * start jquery keypress observer to check if the user has pressed the up/down arrow or the
 * enter key when searching for companies
 */
function init()
{

 	$j(document).keydown( function(e){

    var event = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;

		// see if the down arrow (40) or up arrow (38) has been pressed?
		switch (event)
		{

			//  up arrow key has been pressed
			case 38: // check selected variable is not equal to zero
					 if (SELECTED == 0)
					 {

					 }
					 // selected variable contains value greater than zero so move up
					 else
					 {
					 	 // remove 'selected' class name from the currently selected element
						 $j('#companyDiv a').eq(SELECTED).removeClass('selected');
						 // remove one from the selected variable value
						 SELECTED = SELECTED - 1;
						 // add 'selected' class name to the newly selected element
						 $j('#companyDiv a').eq(SELECTED).addClass('selected');
						 // put focus on selected anchor so window scrolls
						 $j('#companyDiv a')[SELECTED].focus();
						 // then return focus to the input field
						 $j('#coname').focus();
					 }
					 // set this trigger variable to true so that the company ajax is not called
					 companyKeyPress = true;
					 break;

			// down arrow key has been pressed
			case 40: // check selected variable is not equal to the number of anchors
					 if (SELECTED == $j('#companyDiv a').size() - 1)
					 {

					 }
					 // selected variable contains value less than anchors length so move down
					 else
					 {
					 	// remove 'selected' class name from the currently selected element
						$j('#companyDiv a').eq(SELECTED).removeClass('selected');
						// remove one from the selected variable value
						SELECTED = SELECTED + 1;
						// add 'selected' class name to the newly selected element
						$j('#companyDiv a').eq(SELECTED).addClass('selected');
						// put focus on selected anchor so window scrolls
						$j('#companyDiv a')[SELECTED].focus();
						// then return focus to the input field
						$j('#coname').focus();
					 }
					 // set this trigger variable to true so that the company ajax is not called
					 companyKeyPress = true;
					 break;

			// enter key has been pressed
			case 13: // set keypress variable to true so that the ajax function is not called
					 companyKeyPress = true;
					 // add href value from the selected anchor to the browser address bar
					 window.location.href = $j('#companyDiv a').eq(SELECTED).attr('href');
					 break;

			// left arrow key has been pressed
			case 37: // set keypress variable to true so that the ajax function is not called.
					 // no other functionality, included this code only to stop the list being reloaded
					 // if this key is pressed mistakenly
					 companyKeyPress = true;
					 break;

			// right arrow key has been pressed
			case 39: // set keypress variable to true so that the ajax function is not called.
					 // no other functionality, included this code only to stop the list being reloaded
					 // if this key is pressed mistakenly
					 companyKeyPress = true;
					 break;
		}
	});
}
