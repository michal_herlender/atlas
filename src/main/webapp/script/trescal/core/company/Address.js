/** 
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = 	new Array( { anchor: 'transport-link', block: 'transport-tab' },
					      	   { anchor: 'locations-link', block: 'locations-tab' },
					      	   { anchor: 'plantillas-link', block: 'plantillas-tab' } ) ;