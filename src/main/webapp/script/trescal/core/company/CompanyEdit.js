/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	CompanyEdit.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	Commenting of code.
*	KNOWN ISSUES	:	-
*	HISTORY			:	26/10/2006 - SH - Created File
*
****************************************************************************************************************************************/


/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'coname';

/**
 * Trigger upon checkbox selection for changing the primary business contact to one from
 * the currently logged in company.
 * 
 * if value is true (checked) we show the select box, if not we hide it (vice versa for display)
 * 
 */
function toggleBusinessContactSelect(value) {
	if (value) {
		$j('#businesscontact-select').removeClass('hid').addClass('vis');
		$j('#businesscontact-display').removeClass('vis').addClass('hid');
	}
	else {
		$j('#businesscontact-select').removeClass('vis').addClass('hid');
		$j('#businesscontact-display').removeClass('hid').addClass('vis');
	}
}


function toggleCurrencySource(radio)
{
	if(radio.value == 'true')
	{
		$j('#specificcurrency').removeClass('hid').addClass('vis');
		$j('#rateli').removeClass('hid').addClass('vis');
	}
	else
	{
		$j('#specificcurrency').removeClass('vis').addClass('hid');
		$j('#rateli').removeClass('vis').addClass('hid');
	}
}

function toggleRateSource(radio)
{
	if(radio.value == 'true')
	{
		$j('#specificcurrency').removeClass('hid').addClass('vis');
		$j('#rateli').removeClass('hid').addClass('vis');
		
		// set the symbol of the currently selected currency into the rate list
		updateRateSymbol($j('#currencyList').val());
	}
	else
	{
		$j('#specificcurrency').removeClass('vis').addClass('hid');
		$j('#rateli').removeClass('vis').addClass('hid');
	}
}

function updateRateSymbol(currencyid)
{
	var symbol = '';
	for(var i = 0; i < currencies.length; i++)
	{
		if(currencies[i].id == currencyid)
		{
			symbol = currencies[i].symbol;
		}
	}
	$j('#currentCurSym').empty().append(symbol);
}

function currency(id,symbol, name, defaultrate)
{
	this.id = id;
	this.symbol = symbol;
	this.name = name;
	this.defaultrate = defaultrate;
}

var currencies = new Array();

function checkForNameChange(oldName, newName)
{
	if(oldName == newName)
	{
		// uncheck the save change box
		$j('span#nameChangeFields input:checkbox').attr('checked', '');
		// hide the save fields
		$j('span#nameChangeFields').addClass('hid').removeClass('vis');
	}
	else
	{
		// check the save change box
		$j('span#nameChangeFields input:checkbox').attr('checked', 'checked');
		// show the save fields
		$j('span#nameChangeFields').addClass('vis').removeClass('hid');
	}
}
