/** 
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = 	new Array( 	{ anchor: 'calibration-link', block: 'calibration-tab' },
								{ anchor: 'repair-link', block: 'repair-tab' },
								{ anchor: 'adjustment-link', block: 'adjustment-tab' },
								{ anchor: 'purchase-link', block: 'purchase-tab' } );