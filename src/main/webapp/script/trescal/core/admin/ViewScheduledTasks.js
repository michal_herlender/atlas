/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: Jamie Vinall
*
*	FILENAME		:	viewscheduledtasks.js
*	DESCRIPTION		:	
*					: 	
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	04/02/2010 - JV - Created File
*					:	02/10/2015 - TProvost - Manage i18n
*					:	02/10/2015 - TProvost - Use "prompt" function instead "confirm" function to display message and dialog box
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );


function runTask(quartzJobName)
{
	// initial prompt for user
	$j.prompt(i18n.t("core.admin:scheduledTask.warningRun", "Only continue if you have considered all consequences... Are you 100% positive that you wish to run this task now?"),
	{ 
		submit: confirmcallback,
		buttons: 
		{ 
			Ok: true,
			Cancel: false 
		}, 
		focus: 1 
	});
	// callback method for initial prompt
	function confirmcallback(v,m)
	{
		// user confirmed action?
    	if (m)
		{
    		// load the service javascript file if not already	
    		loadScript.loadScriptsDynamic(new Array('dwr/interface/scheduledtaskservice.js'), true,
    		{
    			callback: function()
    			{
		    		scheduledtaskservice.triggerScheduledTaskNow(quartzJobName, 
		    		{ 
		    			callback:function(resWrap)
		    			{ 
		    				$j.prompt(resWrap.message);
		    			}
		    		});
    			}
    		});
		}	
	}
}

function switchTaskOnOrOff(anchor, taskId, setTo)
{
	var message = (setTo) 
		? i18n.t("core.admin:scheduledTask.warningActivate", "Are you sure you wish to activate this task?") 
		: i18n.t("core.admin:scheduledTask.warningInactivate", "Are you sure you wish to de-activate this task? It will no longer run at its scheduled times until re-activated.");
	var result = (setTo) 
		? i18n.t("core.admin:scheduledTask.resultActivation", "Task has been activated to run at its scheduled times.") 
		: i18n.t("core.admin:scheduledTask.resultInactivation", "Task has been de-activated.");
	var html = (setTo) 
		? '<a href="#" onclick=" switchTaskOnOrOff(this, ' + taskId + ', false); return false; " title="' + i18n.t("core.admin:scheduledTask.clickToActivate", "click to de-activate") + '"><img src="img/icons/greentick.png" width="16" height="16" /></a>' 
		: '<a href="#" onclick=" switchTaskOnOrOff(this, ' + taskId + ', true); return false; " title="' + i18n.t("core.admin:scheduledTask.clickToInactivate", "click to activate") + '"><img src="img/icons/redcross.png" width="16" height="16" /></a>';
	
	$j.prompt(message,
	{ 
		submit: confirmcallback,
		buttons: 
		{ 
			Ok: true,
			Cancel: false 
		}, 
		focus: 1 
	});
	// callback method for initial prompt
	function confirmcallback(v,m)
	{
		// user confirmed action?
    	if (m)
		{	
			// load the service javascript file if not already	
			loadScript.loadScriptsDynamic(new Array('dwr/interface/scheduledtaskservice.js'), true,
			{
				callback: function()
				{
					scheduledtaskservice.switchScheduledTaskOnOrOff(taskId, setTo, 
					{
						callback:function()
						{ 
							$j(anchor).parent('td').html(html);
						
							$j.prompt(result);
						}
					});
				}
			});
		}
	}	
}