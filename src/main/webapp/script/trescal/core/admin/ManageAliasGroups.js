var pageImports = new Array ('script/thirdparty/jQuery/jquery.dataTables.min.js');


function init(){
	initdatatable();
}


function initdatatable(){
	var table= $j('#aliasgroups').DataTable({
	    "pageLength":25,
		searching: false,
	    ordering:  false,
	    bInfo : false,		
	});
	
	 // Add event listener for opening and closing details
    $j('#aliasgroups tbody').on('click', 'td.details-control', function () {
        var tr = $j(this).closest('tr');
        var row = table.row( tr );
        
        var val = $j(this).find("#aliasgroupid").val();
  
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
            $j(this).find("#close").addClass("hid");
            $j(this).find("#open").removeClass("hid");
        }
        else {
            // Open this row
            row.child( aliasesTable(val) ).show();
            tr.addClass('shown');
            $j(this).find("#open").addClass("hid");
            $j(this).find("#close").removeClass("hid");
        }
    } );
	
}

function aliasesTable(aliasgroupid){
	var table='';
	var data = aliases.filter(c=>{
		return c.aliasgroupid.includes(aliasgroupid);
	});
	
	table ='<table class="default4 companySubdivs" style="margin :0px">'+
		'<thead>'+
			'<th class="center" colspan="0"  class="codefault">#</th>'+
			'<th class="center">colmun</th>'+
			'<th class="center">value</th>'+
			'<th class="center">alias</th>'+
		'</thead>'+
	' <tbody>';
	for(i = 0; i < data.length; i++){
		table +='<tr>'+
					'<td class="center"><span>'+(i+1)+'</span></td>'+
					'<td class="center">'+ data[i].column +'</td>'+
					'<td class="center">'+ data[i].translation +'</td>'+
					'<td class="center">'+ data[i].alias +'</td>'+ 
				'</tr>';
	}
	
	table +='</tbody> </table>';
	return table;
}

function deleteAliasGroup(aliasGroupId){
	
	var yes = i18n.t("yes", "Yes");
	var no = i18n.t("no", "No");
	$j.prompt(i18n.t("delete", "delete")+" ?", {
		buttons: { [no]: false, [yes]: true },
		submit: function(e,v,m,f){
			
			if(v){
				// make the ajax call
				$j.ajax({
					  url: "deleteAliasGroup.json?id="+aliasGroupId,
					  dataType:'json'
				}).done(function(res) {
					console.log(res);
					if(res.success===true){
						// refresh page
						window.location.reload();
					}else{
						$j.prompt.close();
						$j.prompt(res.message);
					}
				});
			}
		}
	});
	
}