
/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array ('script/thirdparty/jQuery/jquery.boxy.js');

 /**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{
	// load stylesheet using the dom if not already
	loadScript.loadStyle('styles/jQuery/jquery-boxy.css', 'screen');
}

function showCourierDetails(courierid, ele){
	$j(ele).removeClass('show').addClass('hid');
	$j("#hidButton_"+courierid).removeClass('hid').addClass('show');
	$j("#courierDetails_"+courierid).removeClass('hid').addClass('show');
}

function hidCourierDetails(courierid, ele){
	$j(ele).removeClass('show').addClass('hid');
	$j("#showButton_"+courierid).removeClass('hid').addClass('show');
	$j("#courierDetails_"+courierid).removeClass('show').addClass('hid');
}

function updateCourierDefault(companyid, courierid, ele) {

		$j.ajax({
			url: "updateDefaultCourierForCompany.json",
			method: "POST",
			data: {
				coid: companyid,
				courierid: courierid
				}
		}).done(function(result) {
			if (result) 
			{
				$j("a[id^='nonDefaultButton_']").removeClass('hid').addClass('show');
				$j("a[id^='defaultButton_']").removeClass('show').addClass('hid');
				$j("#nonDefaultButton_"+courierid).removeClass('show').addClass('hid');
				$j("#defaultButton_"+courierid).removeClass('hid').addClass('show');
			}
			
		}).fail(function(result) {
			// show error message to user
			$j.prompt(result.message);
		});
}

function editCourier(courierid, ele) {
	// create content to be displayed
	var content = 	'<div id="editCourierBoxy" class="overflow">' +
						'<fieldset>' +
							'<legend>' + i18n.t("editcourier.headtext", "Edit Courier") + '</legend>' +
							'<ol>' +
								'<li>' +
									'<label>' + i18n.t("addcourier.couriername", "Courier Name") + '</label>' +
									'<input type="text" id="couriername" tabindex="1" value="'+$j("#courierLabel_"+courierid).text()+'" class="width90" />' +
									'<input type="hidden" id="courierid" tabindex="10" value="'+courierid+'" />' +
								'</li>' +
								'<li>' +
									'<label>' + i18n.t("addcourier.webtrackurl", "Web Track URL") + '</label>' +
									'<textarea id="webtrackurl" tabindex="2" value="'+$j("#courierWebTrackURL_"+courierid).val()+'" rows="2" cols="72" >' +
									$j("#courierWebTrackURL_"+courierid).val() +
									'</textarea>' +
								'</li>' +
							'</ol>' +
							'<div style="text-align:center">' +
								'<input type="button" tabindex="103" onclick=" saveCourier($j(\'#courierid\').val(), $j(\'#couriername\').val(), $j(\'#webtrackurl\').val()); return false; " value="' + i18n.t("save", "Save") + '" />' +
							'</div>' +
						'</fieldset>' +
					'</div>';
	// create new boxy pop up using content
	var editCourierBoxy = new Boxy(content, {title: i18n.t("editcourier.headtext", "Edit Courier"), unloadOnHide: true});
	// adapt size of boxy
	editCourierBoxy.tween(520,210);
	// focus on po number input
	$j('div#editCourierBoxy input#couriername').focus();
}

function saveCourier(courierid, couriername, webtrackurl) {

	$j.ajax({
		url: "updateCourier.json",
		method: "POST",
		data: {
			courierid: courierid,
			name: couriername,
			webtrackurl: webtrackurl
			}
	}).done(function(result) {
		if (result) 
		{
			$j('#successMessage').fadeIn().delay(2000).fadeOut();
			$j("#courierLabel_"+courierid).html(couriername);
			$j("#courierWebTrackURL_"+courierid).val(webtrackurl);
			$j("#courierWebTrackURL_"+courierid).text(webtrackurl);
			Boxy.get($j('div#editCourierBoxy')).hide();
		}
		
	}).fail(function(result) {
		// show error message to user
		$j.prompt(result.message);
	});
}

function cancelCourier(courierid, ele) {
	$j("#courierLabel_"+courierid).removeClass('hid').addClass('show');
	$j("#courierText_"+courierid).removeClass('show').addClass('hid').val($j("#courierLabel_"+courierid).html());
	$j("#editButton_"+courierid).removeClass('hid').addClass('show');
	$j("#deleteButton_"+courierid).removeClass('hid').addClass('show');
	$j("#saveButton_"+courierid).removeClass('show').addClass('hid');
	$j("#cancelButton_"+courierid).removeClass('show').addClass('hid');
}

function deleteCourier(courierid, ele) {

	if(confirm(strings['removecourier.confirmationmessage'])){
		
	$j.ajax({
		url: "deleteCourier.json",
		method: "POST",
		data: {
			courierid: courierid
			}
	}).done(function(result) {
		if (result.success) 
		{
			$j('#successMessage').fadeIn().delay(2000).fadeOut();
			$j("#courier"+courierid).remove();
			$j("#courierDetails_"+courierid).remove();
			var courierCount = parseInt($j("#courierCount").html());
			courierCount--;
			$j("#courierCount").html(courierCount);
		}
		else {
			$j.prompt(result.message);
		}
		
	}).fail(function(result) {
		// show error message to user
		$j.prompt(result.message);
	});
				 }
		

	return false;
}

function updateCourierDespDefault(courierid, cdtid, ele) {

	$j.ajax({
		url: "updateDefaultCourierDespatchTypeForCourier.json",
		method: "POST",
		data: {
			courierid: courierid,
			cdtid: cdtid
			}
	}).done(function(result) {
		if (result) 
		{
			$j("a[id^='nonDefaultCDButton_"+courierid+"']").removeClass('hid').addClass('show');
			$j("a[id^='defaultCDButton_"+courierid+"']").removeClass('show').addClass('hid');
			$j("#nonDefaultCDButton_"+courierid+"_"+cdtid).removeClass('show').addClass('hid');
			$j("#defaultCDButton_"+courierid+"_"+cdtid).removeClass('hid').addClass('show');
		}
		
	}).fail(function(result) {
		// show error message to user
		$j.prompt(result.message);
	});
}

function editCourierDesp(courierid, cdtid, ele) {
$j("#courierDespLabel_"+courierid+"_"+cdtid).removeClass('show').addClass('hid');
$j("#courierDespText_"+courierid+"_"+cdtid).removeClass('hid').addClass('show');
$j("#editCDButton_"+courierid+"_"+cdtid).removeClass('show').addClass('hid');
$j("#deleteCDButton_"+courierid+"_"+cdtid).removeClass('show').addClass('hid');
$j("#saveCDButton_"+courierid+"_"+cdtid).removeClass('hid').addClass('show');
$j("#cancelCDButton_"+courierid+"_"+cdtid).removeClass('hid').addClass('show');
}

function saveCourierDesp(courierid, cdtid, ele) {

$j.ajax({
	url: "updateCourierDespatchType.json",
	method: "POST",
	data: {
		courierid: courierid,
		cdtid: cdtid,
		description: $j("#courierDespText_"+courierid+"_"+cdtid).val()
		}
}).done(function(result) {
	if (result.success) 
	{
		$j('#successMessage').fadeIn().delay(2000).fadeOut();
		$j("#courierDespLabel_"+courierid+"_"+cdtid).html($j("#courierDespText_"+courierid+"_"+cdtid).val());
		$j("#courierDespLabel_"+courierid+"_"+cdtid).removeClass('hid').addClass('show');
		$j("#courierDespText_"+courierid+"_"+cdtid).removeClass('show').addClass('hid');
		$j("#editCDButton_"+courierid+"_"+cdtid).removeClass('hid').addClass('show');
		$j("#deleteCDButton_"+courierid+"_"+cdtid).removeClass('hid').addClass('show');
		$j("#saveCDButton_"+courierid+"_"+cdtid).removeClass('show').addClass('hid');
		$j("#cancelCDButton_"+courierid+"_"+cdtid).removeClass('show').addClass('hid');
	}
	
}).fail(function(result) {
	// show error message to user
	$j.prompt(result.message);
});
}

function cancelCourierDesp(courierid, cdtid, ele) {
$j("#courierDespLabel_"+courierid+"_"+cdtid).removeClass('hid').addClass('show');
$j("#courierDespText_"+courierid+"_"+cdtid).removeClass('show').addClass('hid');
$j("#courierDespText_"+courierid+"_"+cdtid).val($j("#courierDespLabel_"+courierid+"_"+cdtid).text());
$j("#editCDButton_"+courierid+"_"+cdtid).removeClass('hid').addClass('show');
$j("#deleteCDButton_"+courierid+"_"+cdtid).removeClass('hid').addClass('show');
$j("#saveCDButton_"+courierid+"_"+cdtid).removeClass('show').addClass('hid');
$j("#cancelCDButton_"+courierid+"_"+cdtid).removeClass('show').addClass('hid');
}

function deleteCourierDesp(courierid, cdtid, ele) {

	if(confirm(strings['courierdespatchtype.remove.confirmationmessage'])) {
					$j.ajax({
						url: "deleteCourierDespatchType.json",
						method: "POST",
						data: {
							courierid: courierid,
							cdtid: cdtid
							}
					}).done(function(result) {
						if (result.success) 
						{
							$j('#successMessage').fadeIn().delay(2000).fadeOut();
							$j("#cdt"+cdtid).remove();
							var courierDespCount = parseInt($j("#courierDespCount_"+courierid).html());
							courierDespCount--;
							$j("#courierDespCount_"+courierid).html(courierDespCount);
						}
						else {
							$j.prompt(result.message);
						}
						
					}).fail(function(result) {
						// show error message to user
						$j.prompt(result.message);
					});
	            }

	return false;
}

function addNewCourierContent() {
		// create content to be displayed
		var content = 	'<div id="newCourierBoxy" class="overflow">' +
							'<fieldset>' +
								'<legend>' + i18n.t("addcourier.headtext", "Add New Courier") + '</legend>' +
								'<ol>' +
									'<li>' +
										'<label>' + i18n.t("addcourier.couriername", "Courier Name") + '</label>' +
										'<input type="text" id="couriername" tabindex="1" value="" class="width90" />' +
										'<span id="errorMessage" class="error hid">' + strings['repairinspection.validator.mandatory'] + '</span>' +
									'</li>' +
									'<li>' +
										'<label>' + i18n.t("addcourier.webtrackurl", "Web Track URL") + '</label>' +
										'<textarea id="webtrackurl" tabindex="2" value="" rows="2" cols="72" />' +
									'</li>' +
								'</ol>' +
								'<div style="text-align:center">' +
									'<input type="button" tabindex="103" onclick=" addNewCourier($j(\'#couriername\').val(), $j(\'#webtrackurl\').val()); return false; " value="' + i18n.t("save", "Save") + '" />' +
								'</div>' +
							'</fieldset>' +
						'</div>';
		// create new boxy pop up using content
		var newCourierBoxy = new Boxy(content, {title: i18n.t("addcourier.headtext", "Add New Courier"), unloadOnHide: true});
		// adapt size of boxy
		newCourierBoxy.tween(520,210);
		// focus on po number input
		$j('div#newCourierBoxy input#couriername').focus();
}

function addNewCourier(couriername, webtrackurl) {

	$j.ajax({
		url: "addNewCourier.json",
		method: "POST",
		data: {
			couriername: couriername,
			webtrackurl: webtrackurl
			}
	}).done(function(result) {
		if (result.success) 
		{
			$j('#successMessage').fadeIn().delay(2000).fadeOut();
			var content = '<tr id="courier'+result.message+'">' +
								'<td>' +
								'<a href="#"><img src="img/icons/add.png" id="showButton_'+result.message+'" class="show" width="16" height="16" id="defaultCourier" alt="" title="" onclick=" showCourierDetails('+result.message+', this); return false; " /></a>' +
								'<a href="#"><img src="img/icons/delete.png" id="hidButton_'+result.message+'" class="hid" width="16" height="16" id="defaultCourier" alt="" title="" onclick=" hidCourierDetails('+result.message+', this); return false; " /></a>' +
							'</td>' +
							'<td>' +
								'<a href="#" id="defaultButton_'+result.message+'" class="hid"><img src="img/icons/courier_default.png" width="16" height="16" id="defaultCourier" alt="" title="" /></a>' +
								'<a href="#" id="nonDefaultButton_'+result.message+'" class="show"><img src="img/icons/courier.png" width="16" height="16" id="courier" alt="" title="" onclick=" updateCourierDefault('+coid+', '+result.message+', this); return false; " /></a>' +
							'</td>' +
							'<td>' +
								'<span id="courierLabel_'+result.message+'" class="show">'+couriername+'</span>' +
								'<input type="text" id="courierText_'+result.message+'" class="hid" value="'+couriername+'"/>' +
								'<input type="hidden" id="courierWebTrackURL_'+result.message+'" value="'+webtrackurl+'"/>' +
							'</td>' +
							'<td class="center">' +
								'<a href="#" id="editButton_'+result.message+'" class="pad-left-small" onclick="editCourier('+result.message+', this); return false; "><img src="img/icons/edit_details.png" width="16" height="16" alt="" title="" /></a>' +
								'<a href="#" id="deleteButton_'+result.message+'" class="pad-left-small" onclick="deleteCourier('+result.message+', this); return false; "><img src="img/icons/delete.png" width="16" height="16" alt="" title="" /></a>' +
								'<a href="#" id="saveButton_'+result.message+'" class="hid pad-left-small" onclick="saveCourier('+result.message+', this); return false; "><img src="img/icons/greentick.png" width="16" height="16" alt="" title="" /></a>' +
								'<a href="#" id="cancelButton_'+result.message+'" class="hid pad-left-small" onclick="cancelCourier('+result.message+', this); return false; "><img src="img/icons/cancel.png" width="16" height="16" alt="" title="" /></a>' +
							'</td>' +
						'</tr>' +
						'<tr id="courierDetails_'+result.message+'" class="hid">' +
							'<td colspan="4">' +
								'<table class="default2" id="courierDespTable_'+result.message+'">' +
									'<thead>' +
										'<tr>' +
											'<td colspan="3">' +
												i18n.t("viewbatchcalibration.courdespattype","Courier despatch type") +' (<span id="courierDespCount_'+result.message+'">0</span>)' +
												'<span class="float-right width40">' +
													'<button id="newCourierDespCancelButton_'+result.message+'" class="hid float-right" onclick="cancelNewCourierDesp('+result.message+');">'+ i18n.t("cancel", "") + '</button>' +
													'<button id="newCourierDespAddButton_'+result.message+'" class="hid float-right" onclick="addNewCourierDesp($j(\'#newCourierDespDesc_'+result.message+'\').val(),'+result.message+');">'+i18n.t("add", "") +'</button>' +
													'<span id="newCourierDespDescErrorMsg_'+result.message+'" class="error hid float-right">' + strings['repairinspection.validator.mandatory'] + '</span>' +
													'<input type="text" id="newCourierDespDesc_'+result.message+'" class="hid float-right width40" value=""/>' +
													'<span id="newCourierDespDescLabel_'+result.message+'" class="hid float-right">'+ i18n.t("description", "")+' :</span>' +
													'<a href="#" id="addNewCourierDespatchTypeContentLink_'+result.message+'" class="mainlink float-right" onclick="event.preventDefault(); addNewCourierDespatchTypeContent('+result.message+');">' +
														i18n.t("viewcouriers.adddespatchtype","New courier despatch type") +
													'</a>' +
												'</span>' +
											'</td>' +
										'</tr>' +
										'<tr>' +
											'<th class="defaultCourier" scope="col">' + i18n.t("company.default", "Default") + '</th>' +
											'<th class="courierName" scope="col">' + i18n.t("description", "Description") + '</th>' +
											'<th class="action" scope="col">' + i18n.t("viewquot.actions", "Actions") + '</th>' +
										'</tr>' +
									'</thead>' +
									'<tbody>' +
										'<tr class="center bold">' +
											'<td colspan="3">'+i18n.t("showfilesforsc.empty", "Empty")+'</td>' +
										'</tr>' +
									'</tbody>' +
								'</table>' +
							'</td>' +
						'</tr>';
			$j("#courierTable tr[id^='courier']:last").length > 0 ? $j("#courierTable tr[id^='courier']:last").after(content):$j("#courierTable tbody").html(content);
					 
//		    var w = $j(window);
//		    var row = $j('#courierTable tbody tr[id=\'courier'+result.message+'\']');
//
//		    if (row.length){
//		    	var sc = row.offset().top + w.height();
//		        w.scrollTop( sc );
//		    }
			
			var courierCount = parseInt($j("#courierCount").html());
			courierCount++;
			$j("#courierCount").html(courierCount);
			// hide newCourierBoxy
			Boxy.get($j('div#newCourierBoxy')).hide();
		}
		else {
			$j("#errorMessage").removeClass("hid");
		}
		
	}).fail(function(result) {
		// show error message to user
		$j.prompt(result.message);
	});
	}

function addNewCourierDespatchTypeContent(courierid) {
	$j("#newCourierDespDescLabel_"+courierid).removeClass('hid').addClass('show');
	$j("#newCourierDespDesc_"+courierid).removeClass('hid').addClass('show');
	$j("#newCourierDespAddButton_"+courierid).removeClass('hid').addClass('show');
	$j("#newCourierDespCancelButton_"+courierid).removeClass('hid').addClass('show');
	$j("#addNewCourierDespatchTypeContentLink_"+courierid).removeClass('show').addClass('hid');
	$j("#newCourierDespDesc_"+courierid).focus();	
}

function addNewCourierDesp(description, courierid) {
	if(description == ''){
		$j("#newCourierDespDescErrorMsg_"+courierid).removeClass("hid");
	}
	else {
		$j.ajax({
			url: "addNewCourierDespatchType.json",
			method: "POST",
			data: {
				courierid: courierid,
				description: description
				}
		}).done(function(result) {
			if (result.success) 
			{
				var cdtId = result.results.id;
				var content = '<tr id="cdt'+cdtId+'">' +
									'<td>' +
									'<a href="#" id="defaultCDButton_'+courierid+'_'+cdtId+'" class="hid"><img src="img/icons/default_box.png" width="16" height="16" alt="" title="" /></a>' +
									'<a href="#" id="nonDefaultCDButton_'+courierid+'_'+cdtId+'" class="show"><img src="img/icons/box.png" width="16" height="16" id="courier" alt="" title="" onclick=" updateCourierDespDefault('+courierid+', '+cdtId+', this); return false; " /></a>' +
								'</td>' +
								'<td>' +
									'<span id="courierDespLabel_'+courierid+'_'+cdtId+'" class="show">'+description+'</span>' +
									'<input type="text" id="courierDespText_'+courierid+'_'+cdtId+'" class="hid width60" value="'+description+'"/>' +
								'</td>' +
								'<td class="center">' +
									'<a href="#" id="editCDButton_'+courierid+'_'+cdtId+'" class="pad-left-small" onclick="editCourierDesp('+courierid+', '+cdtId+', this); return false; "><img src="img/icons/edit_details.png" width="16" height="16" alt="" title="" /></a>' +
									'<a href="#" id="deleteCDButton_'+courierid+'_'+cdtId+'" class="pad-left-small" onclick="deleteCourierDesp('+courierid+', '+cdtId+', this); return false; "><img src="img/icons/delete.png" width="16" height="16" alt="" title="" /></a>' +
									'<a href="#" id="saveCDButton_'+courierid+'_'+cdtId+'" class="hid pad-left-small" onclick="saveCourierDesp('+courierid+', '+cdtId+', this); return false; "><img src="img/icons/greentick.png" width="16" height="16" alt="" title="" /></a>' +
									'<a href="#" id="cancelCDButton_'+courierid+'_'+cdtId+'" class="hid pad-left-small" onclick="cancelCourierDesp('+courierid+', '+cdtId+', this); return false; "><img src="img/icons/cancel.png" width="16" height="16" alt="" title="" /></a>' +
								'</td>' +
							'</tr>';
				
				$j("#courierDespTable_"+courierid+" tr[id^='cdt']:last").length > 0 ? $j("#courierDespTable_"+courierid+" tr[id^='cdt']:last").after(content):$j("#courierDespTable_"+courierid+" tbody").html(content);
						
				$j("#courierDespTable_"+courierid+" tbody tr").last().focus();
	
				var courierDespCount = parseInt($j("#courierDespCount_"+courierid).html());
				courierDespCount++;
				$j("#courierDespCount_"+courierid).html(courierDespCount);
				
				cancelNewCourierDesp(courierid);
				$j("#newCourierDespDescErrorMsg_"+courierid).addClass("hid");
			}
			
		}).fail(function(result) {
			// show error message to user
			$j.prompt(result.message);
		});
	}
}

function cancelNewCourierDesp(courierid) {
	$j("#newCourierDespDescLabel_"+courierid).removeClass('show').addClass('hid');
	$j("#newCourierDespDesc_"+courierid).removeClass('show').addClass('hid');
	$j("#newCourierDespAddButton_"+courierid).removeClass('show').addClass('hid');
	$j("#newCourierDespCancelButton_"+courierid).removeClass('show').addClass('hid');
	$j("#addNewCourierDespatchTypeContentLink_"+courierid).removeClass('hid').addClass('show');
	$j("#newCourierDespDesc_"+courierid).val('');
	$j("#newCourierDespDescErrorMsg_"+courierid).addClass("hid");
}