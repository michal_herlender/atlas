/****************************************************************************************************************************************
*															All rights reserved
*
*	FILENAME		:	ManagePaymentTerms.js
*
****************************************************************************************************************************************/

var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * this function allows the user to edit payment terms
 * 
 * @param {Integer} id id of the object being changed (paymenttermsid)
 * @param {String} code existing code
 * @param {String} term existing term
 */
function changePaymentTerms(id, code, term)
{
	// create content to be displayed in thickbox window using previously populated variables
	var overlayContent = 	'<div>' +
								'<fieldset>' +
									'<ol>' +
										'<li>' +
											'<label>' + i18n.t("code", "Code") + '</label>' +
											'<input type="text" id="code" value="' + code + '"></input>' +
										'</li>' +
										'<li>' +
											'<label>' + i18n.t("description", "Description") + '</label>' +
											'<input type="text" id="term" value="' + term + '"></input>' +
										'</li>' +
									'</ol>' +
								'</fieldset>' +
								'<div class="center">' + 
									'<input type="submit" id="submit" value="' + i18n.t("submit", "Submit") + '" onclick=" this.disabled = true; updatePaymentTerm(' + id + ', $j(\'#code\').val(), $j(\'#term\').val()); " />' +
								'</div>' +
							'</div>';
	
	// create new overlay
	loadScript.createOverlay('dynamic', 'Change Payment Term', null, overlayContent, null, 48, 880, null);
}

/*
 * this function is called from the onclick event in 'changePaymentTerms()' function 
 * @param (id) - Integer id of the object (paymenttremsid)
 * @param (code) - String code of paymentTerms 
 * @param (term) - String description of pymentTerms
 */
function updatePaymentTerm(id, code, term)
{
	$j.ajax({
		type: "POST",
		url: "changepaymentterm.htm",
		data: {paymenttermsid: id, code: code, description: term},
		async: false, 
	})
	loadScript.closeOverlay();
}

/*
 * this function shows/hide a table of translations 
 * @param (id) - Integer id of the object 
 * @param (show) - Boolean show or hide
 */
function getPaymentTermsTranslations(id, show)
{
	if (show)
	{
		// remove image to show items
		$j('#itemsLink' + id).empty();
		// append image to hide items
		$j('#itemsLink' + id).append('<img src="img/icons/items_hide.png" width="16" height="16" alt="' + i18n.t("hideItems", "Hide Items") + '" title="' + i18n.t("hideItems", "Hide Items") + '" class="image_inline" />');
		// make the purchase order items row visible
		$j('#pttr' + id).removeClass('hid').addClass('vis');
		// add effect to slide down purchase order items
		$j('#pttr' + id + ' div').slideDown(1000);
	}
	else
	{
		// remove image to hide items
		$j('#itemsLink' + id).empty();
		// append image to show items
		$j('#itemsLink' + id).append('<img src="img/icons/items.png" width="16" height="16" alt="' + i18n.t("viewItems", "View Items") + '" title="' + i18n.t("viewItems", "View Items") + '"class="image_inline" />');
		// add effect to slide up purchase order items and hide row
		$j('#pttr' + id + ' div').slideUp(1000, function(){
			$j('#pttr' + id).removeClass('vis').addClass('hid');
		});
	}
}

/*
 * this function change the current translation 
 * @param (id) - Integer id of the object (paymenttermsid)
 * @param (locale) - String language of translation
 * @param (show) - Boolean show information or edit information
 */
function changePaymentTermTranslation(id, locale, show)
{
	// remove image to show items
	if (show == 'undefined' || !show)
	{
		$j('#edit1' + id + locale).removeClass('hid').addClass('vis');
		$j('#edit2' + id + locale).removeClass('hid').addClass('vis');
		$j('#show1' + id + locale).removeClass('vis').addClass('hid');
		$j('#show2' + id + locale).removeClass('vis').addClass('hid');
	}
	else
	{
		$j('#edit1' + id + locale).removeClass('vis').addClass('hid');
		$j('#edit2' + id + locale).removeClass('vis').addClass('hid');
		$j('#show1' + id + locale).removeClass('hid').addClass('vis');
		$j('#show2' + id + locale).removeClass('hid').addClass('vis');
	}
}

/*
 * this function save the current translation 
 * @param (id) - Integer id of the object (paymenttermsid)
 * @param (locale) - String language of translation
 * @param (text) - String translation
 */
function savePaymentTermTranslation(id, locale, text)
{
	$j.ajax({
		type: "POST",
		url: "changepaymenttermtranslation.htm",
		data: {paymenttermsid: id, locale: locale, translation: text},
		async: false, 
	})
	changePaymentTermTranslation(id, locale.toString(), false)
}