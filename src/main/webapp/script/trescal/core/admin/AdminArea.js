/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	AdminArea.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*						
*	KNOWN ISSUES	:	-
*	HISTORY			:	26/10/2006 - SH - Created File
*					:	02/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/
/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'dwr/interface/contactservice.js'	);


var roleCount = 0;
var typeCount= 0;

function addType()
{
	var typeType = document.createElement('input');
	typeType.setAttribute('id','departmentType['+typeCount+'].type');
	typeType.setAttribute('name','departmentType['+typeCount+'].type');
	typeType.setAttribute('type','text');

	var typeDescription = document.createElement('input');
	typeDescription.setAttribute('id','departmentType['+typeCount+'].description');
	typeDescription.setAttribute('name','departmentType['+typeCount+'].description');
	typeDescription.setAttribute('type','text');

	var typeSpan = document.getElementById('types');
	typeSpan.appendChild(document.createTextNode('Type '));
	typeSpan.appendChild(typeType);
	typeSpan.appendChild(document.createTextNode(' Description '));
	typeSpan.appendChild(typeDescription);
	typeSpan.appendChild(document.createElement('br'));

	typeCount = typeCount + 1;
}

function addRole()
{
	var roleRole = document.createElement('input');
	roleRole.setAttribute('id','departmentRole['+roleCount+'].role');
	roleRole.setAttribute('name','departmentRole['+roleCount+'].role');
	roleRole.setAttribute('type','text');

	var roleDescription = document.createElement('input');
	roleDescription.setAttribute('id','departmentRole['+roleCount+'].description');
	roleDescription.setAttribute('name','departmentRole['+roleCount+'].description');
	roleDescription.setAttribute('type','text');

	var roleSpan = document.getElementById('roles');
	roleSpan.appendChild(document.createTextNode('Role '));
	roleSpan.appendChild(roleRole);
	roleSpan.appendChild(document.createTextNode(' Description '));
	roleSpan.appendChild(roleDescription);
	roleSpan.appendChild(document.createElement('br'));

	roleCount = roleCount + 1;
}