/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: Jamie Vinall
*
*	FILENAME		:	EditPrinter.js
*	DESCRIPTION		:	Page-specific javascript file.
*					: 	
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	02/06/2008 - JV - Created File
*                       2016-05-13 - GB - Refactored addTray()
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */


/*
 * Adds a row to the table of printer trays 
 */
function addTrayRow()
{	
	$j('#notrays').remove();
	
	var trayIndex = $j('#printerTrays tr').length
	var trayNo = trayIndex + 1;
	$j('#tdTrayNo').text(trayNo);
	var rowPrototype = $j('tbody#rowPrototype').html()
		.replace('tdTrayNo','trayNo'+trayNo)
		.replace('namePaperTypes','paperTypes['+trayIndex+']')
		.replace('namePaperSources','paperSources['+trayIndex+']')
		.replace('nameMediaTrayRefs','mediaTrayRefs['+trayIndex+']');
	
	$j('#printerTrays').append(rowPrototype);
}

/*
 * Deletes a tray from the printer and if successful then deletes the row from the printer tray UI 
 */
function ajaxDeleteTray(printerid, rowIndex, trayid) {
	
}