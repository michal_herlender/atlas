
$j(document).ready( function() {
	var types = '${itemActivityType}';
});

function sendData()
	{
		$j("#admin_manage_act_form").submit();
	}

function deleteActivity(id){
	var yes = i18n.t("yes", "Yes");
	var no = i18n.t("no", "No");
	$j.prompt(i18n.t("delete", "delete")+" ?", {
		buttons: { [no]: false, [yes]: true },
		submit: function(e,v,m,f){
			
			if(v){
				// make the ajax call
				$j.ajax({
					  url: "deleteActivity.json?id="+id,
					  dataType:'json'
				}).done(function(res) {
					if(res.success===true){
						// refresh page
						window.location.reload();
					}else{
						$j.prompt.close();
						$j.prompt(res.message);
					}
				});
			}
		}
	});
}

/**
 * this method creates content to be applied in a new overlay for creating an
 * item work requirement.
 * 
 * @param {String} type the type of request made (i.e. 'Add' or 'Edit')
 * @param {Integer} jobItemId the id of this job item
 * @param {Integer} jiwrId the id of the job item work requirement if we are editing otherwise null
 */
function addNewActivity(type, activity, istransferable, isdisplayed)
{
	var options = '<option value=""></option>';
	itemactivitiestype.forEach(function(element) {
		if(element != 'UNDEFINED')
		options += '<option value="'+element+'">'+element.replace("_"," ")+'</option>';
	});
	console.log(options);
	$j.prompt('<table><tr><td><span width="60px">'+type+' :</span></td><td><select name="type" onchange="getActivities($j(this).val())">'+options+'</select></td></tr><tr><td><span width="60px">'+activity+' :</span></td><td><select name="activity" id="activity" class="maxwidth-240"></select></td></tr><tr><td><span width="60px">'+istransferable+' :</span></td><td><input type="checkbox" name="istransferable" id="istransferable"/></td></tr><tr><td><span width="60px">'+isdisplayed+' :</span></td><td><input type="checkbox" name="isdisplayed" id="isdisplayed"/></td></tr></table>', {
		title: "Add Activity",
		buttons: { "Add": true, "Close": false },
		persistent: false,
		submit: function(e,v,m,f){
			if(v){
				e.preventDefault();
				console.log('Add ------------');
				addNewActivityAJAX($j("#activity").val(),($j('#istransferable:checkbox:checked').length > 0),($j('#isdisplayed:checkbox:checked').length > 0));
			}
		}
		
	});
}

function addNewActivityAJAX(id, istransferable, isdisplayed)
{
	// make the ajax call
	$j.ajax({
		  url: "addNewActivity.json",
		  dataType:'json',
		data:{
			 id:id, 
			 istransferable:istransferable, 
			 isdisplayed:isdisplayed
		  }
	}).done(function(res) {
		if(res.success===true){
			// refresh page
			window.location.reload();
		}else{
			$j.prompt.close();
			$j.prompt(res.message);
		}
	});
}

function getActivities(val)
{
	console.log(val+"-*-*-*-*-*-*-*-*-*-*-*-*-");
	// make the ajax call
	$j.ajax({
		  url: "getActivities.json?type="+val,
		  dataType:'json'
	}).done(function(res) {
		if(res!==null && res!==undefined){
			// refresh page
			var html = '';
			for(var i=0;i < res.length; i++)
				html += '<option value="'+res[i].id+'">'+res[i].description+'</option>'
			$j("#activity").html(html);
		}else{
			//$j.prompt.close();
			$j("#activity").html('');
		}
	});
}