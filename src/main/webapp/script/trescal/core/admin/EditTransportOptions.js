/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	EditTransportOptions.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	HISTORY			:	30/11/2020 - Laïla MADANI - Created File
*
****************************************************************************************************************************************/

function createTransportOption() {
	
	var cancel = i18n.t("core.jobs:jobItem.cancel", "Cancel");
	var addoption = i18n.t("add", "Create");
	
	$j.ajax({
	    url:'addtransportoption.htm',
	    type:'GET',
		async: true,
	    }).done(function(content) {
	       	//display the popup
	       	$j.prompt(content,{
	       		title: i18n.t('core.admin:transportOption.addTransportoption', 'Add Transport Option'),
	       		buttons: { [addoption]: true, [cancel]: false },
	       		submit: function (e, val, m, f) { 
	                   if (val == true) {
	                	   e.preventDefault();
	                	   validateAndSubmit();
	                	   return false;
	                   }
	                   else
	                       console.log('Cancel!');
	               }
	       	});
	       	
	     });	
}


function validateAndSubmit(){
	// check if a transport method is selected
	   var methodId = $j('#methodId')[0].value;
	   if( methodId == 0) {
		   alert(i18n.t('core.admin:transportOption.selectTransportMethod', 'Please select a transport method!!'));
	   }
	   else {
		   var emptyTime = "";
		   $j(".dayOfweek").each(function(index){
			if($j(".dayOfweek")[index].children[1].checked == true && $j(".dayOfweek")[index].children[1].disabled == false){
				if($j(".dayOfweek")[index].children[3].value === "") {
					emptyTime = $j(".dayOfweek")[index].children[1].value;
				}
			}
		  });
		  // check if a time is entered when a day is checked
		  if(emptyTime) {
			  alert(i18n.t('core.admin:transportOption.selectTime', 'Please select a Time!'));
		  }
		  else {
			// check if the localized name is unique
			  $j.ajax({
 				url: "checkLocalisedNameUniqueness.json",
 				dataType:'json',
 				data:{
 					localizedName: $j('#localizedName')[0].value,
 					transportMethodId: methodId
 				  }
 			}).done(function(result) {
 				if(!result) {
 					alert(i18n.t('core.admin:transportOption.localizedNameAlreadyExist', 'There is already a transport option with this localized name'));
 				} else {
 					$j('#addtransportoption')[0].submit();
 				}
 			});
		  }
		  
	   }
}


function isOptionPerDayOfWeek(transportMethodId, subdivid) {
	if(transportMethodId != 0){
		// make the ajax call
		$j.ajax({
			url: "isOptionPerDayOfWeek.json",
			dataType:'json',
			data:{
				transportMethodId: transportMethodId,
				subdivid: subdivid
			  }
		}).done(function(result) {
			// disable scheduleRule
			if(result){
				$j(".isoptionPerDayOfWeek").each(function(index){
					if($j(".isoptionPerDayOfWeek")[index].disabled == true){
						$j(".isoptionPerDayOfWeek")[index].disabled = false;
					}
				});
			} else {
				$j(".isoptionPerDayOfWeek").each(function(index){
					if($j(".isoptionPerDayOfWeek")[index].disabled == false){
						$j(".isoptionPerDayOfWeek")[index].disabled = true;
					}
				});
			}
		});
	}
}
