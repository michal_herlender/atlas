function CheckAll() {
	var size = document.querySelectorAll(".ResendNotification").length;
	var checkbox = document.querySelectorAll(".ResendNotification");

	if (document.getElementById('MainResendNotification').checked == true) {
		for (i = 0; i < size; i++) {
			checkbox[i].checked = true;
		}
	} else if (document.getElementById('MainResendNotification').checked == false) {
		for (i = 0; i < size; i++) {
			checkbox[i].checked = false;
		}
	}

}

function hideForSuccesStatus() {

	var size = document.querySelectorAll(".ResendNotification").length;
	var checkbox = document.querySelectorAll(".ResendNotification");
	if (document.getElementById('status').value == "SUCCESS") {
		document.getElementById('MainResendNotification').style.visibility = "hidden";
		for (i = 0; i < size; i++) {
			checkbox[i].style.visibility = "hidden";
		}

	} else {
		document.getElementById('MainResendNotification').style.visibility = "visible";
		for (i = 0; i < size; i++) {
			checkbox[i].style.visibility = "visible";
		}
	}
}

function resendNotification() {
	var size = document.querySelectorAll(".ResendNotification").length;
	var checkbox = document.querySelectorAll(".ResendNotification");

	var eventIdsArray = [];

	for (i = 0; i < size; i++) {
		if (checkbox[i].checked == true) {
			eventIdsArray.push(parseInt(checkbox[i].value));
		}
	}

	if (eventIdsArray.length > 0) {
		$j
				.ajax({
					url : "resendnotification.json",
					method : "POST",
					traditional : true,
					dataType : "json",
					data : {
						idsNotification : eventIdsArray,
						status : $j("select[name=status]").val(),
						entityClass : $j("select[name=entityClass]").val(),
						operationType : $j("select[name=operationType]").val(),
						pageNo : $j("select[name=pageNo]").val()
					},
					async : true
				})
				.done(
						function(data) {
							console.log(data);
							var html = '';
							for (var i = 0; i < data.length; i++)
								html += '<tr><td>'
										+ data[i].entityclass
										+ '</td><td>'
										+ ((data[i].jobItemId == undefined) ? '-- | '
												+ ((data[i].plantid != undefined) ? data[i].plantid
														: '--')
												: '<a href="jiactions.htm?jobitemid='
														+ data[i].jobItemId
														+ '&amp;ajax=jobitem&amp;width=580" class="jconTip mainlink" title="Information sur le job item" id="jobitem'
														+ data[i].jobItemId
														+ '" tabindex="-1"> '
														+ data[i].jobNo
														+ '.'
														+ data[i].jobItemNo
														+ '</a> | '
														+ ((data[i].plantid != undefined) ? data[i].plantid
																: '--'))
										+ '</td><td>'
										+ data[i].operationtype
										+ '</td><td>'
										+ ((data[i].lastErrorMessage != undefined) ? data[i].lastErrorMessage
												: '')
										+ '</td><td> <center> <input  type="checkbox" id="ResendEvent" Class="ResendEvent" value="'
										+ data[i].id
										+ '"/> </center></td></tr>';
							console.log(html);
							$j("#table_sendnotifications tbody").html(html);
						}).fail(function(result) {
					$j.prompt(result.responseText);
				});
	}
}
