/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: Jamie Vinall
*
*	FILENAME		:	ViewPrintFileRules.js
*	DESCRIPTION		:	This javascript file is used on the viewprintfilesrules.vm page.
*					: 	
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	24/11/2008 - JV - Created File
*
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * this method deletes a print file rule
 * 
 * @param {Integer} id the id of print file rule to delete
 */
function deletePrintFileRule(id)
{
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/printfileruleservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to delete print file rule
			printfileruleservice.deletePrintFileRuleById(id, 
			{
				callback:function()
				{
					$j('#rule-' + id).remove();
				}	
			});
		}
	});
}
