/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	EditUserPreferences.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	20/05/2010 - SH - Created File
*
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * Shows controls for editing Alligator settings at contact level 
 */
function showAlligatorSettings() {
	$j('li#alligatorProduction').addClass('vis').removeClass('hid');
	$j('li#alligatorTest').addClass('vis').removeClass('hid');
}

/**
 * Hides controls for editing Alligator settings at contact level 
 */
function hideAlligatorSettings() {
	$j('li#alligatorProduction').addClass('hid').removeClass('vis');	
	$j('li#alligatorTest').addClass('hid').removeClass('vis');	
}