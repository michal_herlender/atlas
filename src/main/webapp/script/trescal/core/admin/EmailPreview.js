/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{
	// Updates the preview iframe using div hidden contents
	var iframeHtml = $j('#preview_div').html();
	$j('#preview_iframe').contents().find('body').html(iframeHtml);
}