/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	CreditCard.js
*	DESCRIPTION		:	Page specific javascript for the credit card velocity page.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	20/07/2007 - SH - Created File
*
****************************************************************************************************************************************/


/*
 * Array of objects containing the element name and corner type which should be applied.
 * Applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/*
 * Element to be focused on when the page loads.
 * Applied in init() function.
 */
var focusElement = 'cardno1';