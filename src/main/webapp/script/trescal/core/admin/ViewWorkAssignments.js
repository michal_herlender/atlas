/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: Jamie Vinall
*
*	FILENAME		:	ViewWorkAssignments.js
*	DESCRIPTION		:	This javascript file is used on the viewworkassignments.vm page.
*					: 	
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	25/06/2007 - JV - Created File
*					: 	02/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

function showDepartment(deptName) {
	$j('div[id^="depttype"]').removeClass('vis').addClass('hid');
	$j('#depttype' + deptName).removeClass('hid').addClass('vis');
}