/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: Jamie Vinall
*
*	FILENAME		:	ViewCouriers.js
*	DESCRIPTION		:	This javascript file is used on the viewcouriers.vm page.
*					: 	
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	25/06/2007 - JV - Created File
*					:	02/10/2015 - TPRovost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );
						
/**
 * indicates if editing of courier types is allowed
 */
var allowEdit = true;

/**
 * this method allows the user to edit the courier name
 * 
 * @param {Object} anchor the anchor clicked to initialise edit functionality
 * @param {Integer} courierId id of the courier to update name for
 */
function editCourierName(anchor, courierId)
{
	// allow editing?
	if (allowEdit)
	{		
		// get the current courier name
		var currentName = $j(anchor).parent().prev().text().trim();
		// show edit inputs
		$j(anchor).parent().prev().html('<input type="text" id="newCourierName" value="' + currentName + '" />').end()
					.html('<input type="button" value="' + i18n.t("core.admin:courrier.change", "Change") + '" onclick=" submitCourierName(this, $j(this).parent().prev().find(\'input\').val(), ' + courierId + '); return false; " />');
	}
	else
	{
		// show message to user
		$j.prompt(i18n.t("core.admin:courrier.doNotHavePermission", "You do not have permission to edit couriers"));
	}
}

/**
 * this method submits the new courier name via a dwr service and updates the page if successful
 * 
 * @param {Object} button the button clicked to update courier name
 * @param {String} newname the new name of courier
 * @param {Integer} courierId id of the courier to update name for
 */
function submitCourierName(button, newname, courierId)
{
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/courierservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to update courier name
			courierservice.editCourier(courierId, newname, null,
			{
				callback: function(result)
				{
					// update name successful?
					if (result.success)
					{
						// get courier from results
						var c = result.results;
						// update the page
						$j(button).parent().prev().html(c.name).end()
									.html(	'<a href="#" class="mainlink" onclick=" editCourierName(this, ' + c.courierid + '); return false; ">' +
												'<img src="img/icons/note_edit.png" height="16" width="16" class="img_marg_bot" alt="' + i18n.t("core.admin:courrier.editCourrier", "Edit Courier") + '" title="' + i18n.t("core.admin:courrier.editCourrier", "Edit Courier") + '" />' +
											'</a>');
					}
					else
					{
						// show user message
						$j.prompt(result.message);
					}
				}
			});
		}
	});
}

/**
 * this method allows the user to edit the courier tracking url
 * 
 * @param {Boolean} exists the courier tracking url exists
 * @param {Object} anchor the anchor clicked to initialise edit functionality
 * @param {Integer} courierId id of the courier to update tracking url for
 */
function editCourierTrackingUrl(exists, anchor, courierId)
{
	// allow editing?
	if (allowEdit)
	{		
		// variable for current url
		var currentUrl = '';
		// url exists already?
		if (exists)
		{
			// get the current courier name
			currentUrl = $j(anchor).parent().prev().find('span').text().trim();
			// show edit inputs
			$j(anchor).parent().prev().html('<input type="text" id="newCourierUrl" value="' + currentUrl + '" />').end()
						.html('<input type="button" value="' + i18n.t("core.admin:courrier.change", "Change") + '" onclick=" submitCourierTrackingUrl(this, $j(this).parent().prev().find(\'input\').val(), ' + courierId + '); return false; " />');
		}
		else
		{
			// show edit inputs
			$j(anchor).parent().before('<div class="float-left width80 nowrap">' +
											'<input type="text" id="newCourierUrl" value="' + currentUrl + '" />' +
										'</div>').end()
										.after('<div class="float-left">' +
													'<input type="button" value="' + i18n.t("core.admin:courrier.change", "Change") + '" onclick=" submitCourierTrackingUrl(this, $j(this).parent().prev().find(\'input\').val(), ' + courierId + '); return false; " />' +
												'</div><div class="clear"></div>').end().remove();
		}
	}
	else
	{
		// show message to user
		$j.prompt(i18n.t("core.admin:courrier.doNotHavePermission", "You do not have permission to edit couriers"));
	}
}

/**
 * this method allows the user to edit despatch types
 * 
 * @param {Object} anchor the anchor clicked to initialise despatch type editing
 * @param {Integer} typeid id of the despatch type to be edited
 */
function editDespatchType(anchor, typeid)
{
	// allow editing?
	if (allowEdit)
	{		
		// get the current despatch type
		var currentDesc = $j(anchor).parent().prev().text().trim();
		// show edit inputs
		$j(anchor).parent().prev().html('<input type="text" value="' + currentDesc + '" />').end()
					.html('<input type="button" value="' + i18n.t("core.admin:courrier.change", "Change") + '" onclick=" submitDespatchType(this, $j(this).parent().prev().find(\'input\').val(), ' + typeid + '); return false; " />');
	}
	else
	{
		// show message to user
		$j.prompt(i18n.t("core.admin:courrier.doNotHavePermission", "You do not have permission to edit couriers"));
	}
}

/**
 * this method submits the new despatch type description via a dwr service and updates the page if successful
 * 
 * @param {Object} button the button clicked to update despatch type description
 * @param {String} newdesc the new description of despatch type
 * @param {Integer} typeId id of the despatch type to update
 */
function submitDespatchType(button, newdesc, typeId)
{
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/cdtypeservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to update despatch type description
			cdtypeservice.editType(typeId, newdesc,
			{
				callback: function(result)
				{
					// update description successful?
					if (result.success)
					{
						// get despatch type from results
						var dt = result.results;
						// update the page
						$j(button).parent().prev().html(newdesc).end()
									.html(	'<a href="#" onclick=" editDespatchType(this, ' + typeId + '); return false; ">' +
												'<img src="img/icons/note_edit.png" height="16" width="16" class="img_marg_bot" alt="' + i18n.t("core.admin:courrier.editDespatchType", "Edit Despatch Type") + '" title="' + i18n.t("core.admin:courrier.editDespatchType", "Edit Despatch Type") + '" />' +
											'</a>' +
											' &nbsp; ' +
											'<a href="#" onclick=" removeDespatchType(this, ' + typeId + '); return false; ">' +
												'<img src="img/icons/delete.png" height="16" width="16" class="img_marg_bot" alt="' + i18n.t("core.admin:courrier.deleteDispatch", "Delete Despatch Type") + '" title="' + i18n.t("core.admin:courrier.deleteDispatch", "Delete Despatch Type") + '" />' +
											'</a>');
					}
					else
					{
						// show user message
						$j.prompt(result.message);
					}
				}
			});
		}
	});
}

/**
 * this method removes the selected courier despatch type
 * 
 * @param {Object} anchor the anchor clicked to initialise removal
 * @param {Integer} typeId the id of the courier despatch type to be removed
 */
function removeDespatchType(anchor, typeId)
{
	$j.ajax({
		url: "deletecourierdespatchtype.json",
		data: {
			id: typeId
		},
		async: true
	}).done(function() {
		// remove the courier despatch type from the page
		$j(anchor).parent().prev().remove().end().next().remove().end().remove();
	}).fail(function(error) {
		$j.prompt(error.message);
	});
}

/**
 * this method allows a user to add a courier despatch type
 * 
 * @param {Object} anchor the link clicked to initialise courier depatch type addition
 * @param {Integer} courierid id of the courier to add despatch type to
 */
function addDespatchType(anchor, courierid)
{
	// allow editing?
	if (allowEdit)
	{	
		// show add despatch type inputs
		$j(anchor).parent().html('<input type="text" value="" /><input type="button" value="' + i18n.t("add", "Add") + '" onclick=" submitNewDespatchType(this, $j(this).prev().val(), ' + courierid + '); return false; " />');	
	}
	else
	{
		// show message to user
		$j.prompt(i18n.t("core.admin:courrier.doNotHavePermission", "You do not have permission to edit couriers"));
	}
}

/**
 * this method submits the new despatch type description via a dwr service and updates the page if successful
 * 
 * @param {Object} button the button clicked to submit new despatch type
 * @param {String} description description for new despatch type
 * @param {Integer} courierid id of the courier to add despatch type to
 */
function submitNewDespatchType(button, description, courierid)
{
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/cdtypeservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to add new despatch type
			cdtypeservice.newDespatchTypeForCourier(courierid, description,
			{
				callback:function(result)
				{
					// addition successful?
					if (result.success)
					{
						// get courier despatch type from result
						var cdt = result.results;
						// append new content to page
						$j(button).parent().parent().append('<div class="float-left padding2 width30">' +
																cdt.name +
															'</div>' +
															'<div class="float-left padding2">' +
																'<a href="#" onclick=" editDespatchType(this, ' + cdt.id + '); return false; ">' +
																	'<img src="img/icons/note_edit.png" height="16" width="16" class="img_marg_bot" alt="' + i18n.t("core.admin:courrier.editDespatchType", "Edit Despatch Type") + '" title="' + i18n.t("core.admin:courrier.editDespatchType", "Edit Despatch Type") + '" />' +
																'</a>' +
																' &nbsp; ' +
																'<a href="#" onclick=" removeDespatchType(this, ' + cdt.id + '); return false; ">' +
																	'<img src="img/icons/delete.png" height="16" width="16" class="img_marg_bot" alt="' + i18n.t("core.admin:courrier.deleteDespatchType", "Delete Despatch Type") + '" title="' + i18n.t("core.admin:courrier.deleteDespatchType", "Delete Despatch Type") + '" />' +
																'</a>' +
															'</div>' +
															'<div class="clear"></div>' +
															'<div class="float-left padding2"><a href="#" class="mainlink" onclick=" addDespatchType(this, ' + courierid + '); return false; ">' + i18n.t("core.admin:courrier.addDespatchType", "Add Despatch Type") + '</a></div>');
						// remove button and input
						$j(button).parent().remove();
					}			
				}	
			});
		}
	});
}


