/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: Jamie Vinall
*
*	FILENAME		:	ViewPrinters.js
*	DESCRIPTION		:	This javascript file is used on the viewprinters.vm page.
*					: 	
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	25/06/2007 - JV - Created File
*					:	02/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * this method deletes a printer
 * 
 * @param {Integer} printerId id of the printer to be deleted
 */
function deletePrinter(printerId)
{
	// confirm deletion of printer
	if(confirm(i18n.t("core.admin:printer.warningOnDelete", "Please note that any business contacts or addresses that have this as a default printer will no longer have a default printer. Are you sure you wish to delete this printer?")) == true)
	{
		// load the service javascript file if not already	
		loadScript.loadScriptsDynamic(new Array('dwr/interface/printerservice.js'), true,
		{
			callback: function()
			{
				// call dwr service to delete printer
				printerservice.deletePrinterById(printerId, 
				{
					callback:function()
					{
						// remove printer from page
						$j('#printer-' + printerId).remove();
					}	
				});
			}
		});
	}
}

/**
 * this method deletes a label printer
 * 
 * @param {Integer} printerId id of the printer to be deleted
 */
function deleteLabelPrinter(printerId)
{
	// confirm deletion of printer
	if(confirm(i18n.t("core.admin:printer.warningOnDelete", "Please note that any business contacts or addresses that have this as a default printer will no longer have a default printer. Are you sure you wish to delete this printer?")) == true)
	{
		// load the service javascript file if not already	
		loadScript.loadScriptsDynamic(new Array('dwr/interface/labelprinterservice.js'), true,
		{
			callback: function()
			{
				// call dwr service to delete printer
				labelprinterservice.deleteLabelPrinterById(printerId, 
				{
					callback:function()
					{
						// remove printer from page
						$j('#lprinter-' + printerId).remove();
					}	
				});
			}
		});
	}
}
