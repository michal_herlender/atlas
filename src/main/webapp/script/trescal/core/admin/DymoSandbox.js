/*******************************************************************************
 * Document author: Galen Beck
 * 
 * FILENAME : DymoSandbox.js
 * DESCRIPTION : This javascript file is used to try out Dymo label printing :
 * HISTORY : 2016-11-17 - GB - Created File
 * 
 ******************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array ('script/thirdparty/misc/DYMO.Label.Framework.3.0.js');

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{
	dymo.label.framework.init(startupCallback);
}

function startupCallback()
{
	dymo.label.framework.getPrintersAsync().then(function(printers) {
		$j('#printercount').text(printers.length.toString());
		var printerText = '';
		for(var i = 0; i < printers.length; i++)
		{
			printerText += printers[0].name;
			printerText += '<br/>';
		}
		$j('#printerlist').html(printerText);
	});
	var checkEnvironmentResult = dymo.label.framework.checkEnvironment();
	if (checkEnvironmentResult.isWebServicePresent) {
		$j('#webservicepresent').text("Yes");
	}
	else {
		$j('#webservicepresent').text("No");
	}
}

function testHireLabel() {
	 var labelUri = "labels/dymo/hiretagfolder.label";
	 $j.get(labelUri).done(function(labelXml) {
		 var printers = dymo.label.framework.getPrinters();
		 if (printers != null) {
			 if (printers.length > 0) {
				// TODO search for and use first connected printer?
				var printerName = printers[0].name;
				var paramsXml = dymo.label.framework.createLabelWriterPrintParamsXml ({ copies: 1 });
				var labelSet = new dymo.label.framework.LabelSetBuilder();
				var record = labelSet.addRecord();

				$j.ajax({url:"hirelabeldemo.json", dataType:"json"}).done(function(hireLabel) {
					// Iterates over each of the keys and values in response, add to labelSet
					// This approach was chosen as being easier / less coupled than 
					// generating / returning XML from the server  
					$j.each(hireLabel, function( key, value ){
						record.setText(key,value);
					})
					var labelSetXml = labelSet.toString(); 
					dymo.label.framework.printLabel(printerName, paramsXml, labelXml, labelSetXml);
				});
			 }
			 else {
				 alert("Error: 0 printers available");
			 }
		 }
		 else {
			 alert("Error: no printer information available");
		 }
	});
}