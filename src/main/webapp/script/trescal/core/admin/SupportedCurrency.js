/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: Jamie Vinall
*
*	FILENAME		:	SupportedCurrency.js
*	DESCRIPTION		:	This javascript file is used on the supportedcurrency.vm page.
*					: 	
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	18/10/2011 - SH - Created File
*
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * this method sets the editing visibility to on or off
 * 
 * @param {String} currencyCode the currency
 * @param {Boolean} edit true if editing, false if note
 */
function toggleEdit(currencyCode, edit)
{
	// editing?
	if(edit)
	{
		$j('#' + currencyCode + 'notediting').hide();
		$j('#' + currencyCode + 'editing').show();
	}
	else
	{
		$j('#' + currencyCode + 'notediting').show();
		$j('#' + currencyCode + 'editing').hide();
	}
}

/**
 * this method saves a new currency rate
 * 
 * @param {Integer} currencyId id of the currency to change rate for
 * @param {String} rate new exchange rate
 */
function saveNewCurrencyRate(currencyId, rate, currencyCode)
{
	$j.ajax({
		url:'sc/supportedcurrency.json',
	    type:'POST',
		datatype:'JSON',
		async: true,
		data: {
			currencyId : currencyId,
			exchangeRate : rate
		}
	}).done(function(result) {
		$j('#'+currencyId+'ratesetby').empty().append(result.rateLastSetBy);
		$j('#'+currencyId+'rateseton').empty().append(result.rateLastSet);
		$j('#'+currencyId+'displayrate').empty().append(rate);

		 var hist = result.rateHistory;

		$j('#'+currencyId+'historyhead').after('<tr><td>'+hist.rate+'</td>'
		  							+'<td>'+hist.dateFrom+'</td>'
		  							+'<td>'+hist.dateTo+'</td>'
		  							+'<td>'+hist.setBy+'</td></tr>');
							
		  					// update the history count for the currency

							  var count = parseInt($j('#'+currencyId+'historysize').html()) + 1;
		  					$j('#'+currencyId+'historysize').empty().append(count);

		toggleEdit(currencyCode, false);

	});
}

/**
 * this method toggles the visibility of history display
 * 
 * @param {Integer} currencyId the id of the currency to view history for
 */
function toggleHistoryDisplay(currencyId)
{
	// get table
	var theTable = $j('#' + currencyId + 'history');
	// table visible?
	if(theTable.hasClass("vis"))
	{
		theTable.removeClass("vis").addClass("hid");
	}
	else
	{
		theTable.removeClass("hid").addClass("vis");
	}
}
