/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	ManageReverseTraceabilityReport.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*						
*	KNOWN ISSUES	:	-
*	HISTORY			:	25/01/2021 - Laïla MADANI - Created File
*
****************************************************************************************************************************************/


function enableReverseTraceabilityForOtherSubdiv() {
	
	var cancel = i18n.t("core.jobs:jobItem.cancel", "Cancel");
	var saveButton = i18n.t("save", "Save");
	
	$j.ajax({
	    url:'enablereversetraceabilitysettings.htm',
	    type:'GET',
		async: true,
	    }).done(function(content) {
	       	//display the popup
	       	$j.prompt(content,{
	       		title: i18n.t('core.admin:reverseTraceability.enableReverseTraceability', 'Enable Reverse Traceability Report for a subdivision'),
	       		buttons: { [saveButton]: true, [cancel]: false },
	       		submit: function (e, val, m, f) { 
	                   if (val == true) {
	                	   e.preventDefault();
	                	   validateAndSubmit();
	                	   return false;
	                   }
	                   else
	                       console.log('Cancel!');
	               }
	       	});
	       	
	     });	
}

function validateAndSubmit(){
	// check if the start date isn't empty
	var startDate = $j('#startDate')[0].value;
	if(!startDate) {
		alert(i18n.t('core.admin:reverseTraceability.selectStartDate', 'Please select a start Date!'));
	}
	else {
		// submit the form
		$j('#enablereversetraceabilitysettings')[0].submit();
 	}
}
