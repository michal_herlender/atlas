

function deleteSystemImage(id){
	
	var yes = i18n.t("yes", "Yes");
	var no = i18n.t("no", "No");
	$j.prompt(i18n.t("delete", "delete")+" ?", {
		buttons: { [no]: false, [yes]: true },
		submit: function(e,v,m,f){
			
			if(v){
				// make the ajax call
				$j.ajax({
					  url: "deleteSystemImage.json?id="+id,
					  dataType:'json'
				}).done(function(res) {
					console.log(res);
					if(res.success===true){
						// refresh page
						window.location.href = "managebusinesscomplogo.htm";
					}
				});
			}
		}
	});
	
}