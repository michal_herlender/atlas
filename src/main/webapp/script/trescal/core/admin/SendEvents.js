
function CheckAll() {
	var size = document.querySelectorAll(".ResendEvent").length;
	var checkbox = document.querySelectorAll(".ResendEvent");

	if (document.getElementById('MainResendEvent').checked == true) {
		for (i = 0; i < size; i++) {
			checkbox[i].checked = true;
		}
	} else if (document.getElementById('MainResendEvent').checked == false) {
		for (i = 0; i < size; i++) {
			checkbox[i].checked = false;
		}
	}

}

function hideForSuccesStatus() {

	var size = document.querySelectorAll(".ResendEvent").length;
	var checkbox = document.querySelectorAll(".ResendEvent");
	if (document.getElementById('status').value == "SUCCESS") {
		document.getElementById('MainResendEvent').style.visibility = "hidden";
		for (i = 0; i < size; i++) {
			checkbox[i].style.visibility = "hidden";
		}

	} else {
		document.getElementById('MainResendEvent').style.visibility = "visible";
		for (i = 0; i < size; i++) {
			checkbox[i].style.visibility = "visible";
		}
	}
}

function submitForm(actionValue) {
	$j("#action").val(actionValue);
	$j("#mainform").submit();
}