/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	Instruction.js
*	DESCRIPTION		:	Page specific javascript for the instruction velocity page.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	18/07/2007 - SH - Created File
*
****************************************************************************************************************************************/


/*
 * Array of objects containing the element name and corner type which should be applied.
 * Applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/*
 * Element to be focused on when the page loads.
 * Applied in init() function.
 */
var focusElement = 'instructiontext';

function additionalOptions(select)
{
	var selected = $j('select#instructiontype').val();
	if(selected == "CARRIAGE")
	{
		$j('#additionalOptions').removeClass('hid').addClass('vis');
	}
	else
	{
		$j('#additionalOptions').removeClass('vis').addClass('hid');
	}
}