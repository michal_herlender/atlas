/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	EmailSearchResults.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	12/02/2008
*
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'dwr/interface/authenticationservice.js',
								'dwr/interface/emailservice.js',
								'script/trescal/core/system/ViewEmailDetails.js');
								
/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );


/**
 * this variable contains the page name which should be used when creating the search again
 * link in the header section
 */
var searchAgain = 'searchemail.htm';
