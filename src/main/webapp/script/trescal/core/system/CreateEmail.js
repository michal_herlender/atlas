/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	CreateEmail.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	06/08/2009
*
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array ( 'script/trescal/core/tools/FileBrowserPlugin.js' );

function init(){
	loadScript.loadStyle('styles/structure/elements/email.css', 'screen');
}
/**
 * this method shows a chosen file browser in the email plugin
 * 
 * @param {Object} div the div which contains link pressed
 * @param {String} typeid the id of element to be shown
 */
function showFileBrowser(div, typeid)
{
	// hid this div
	$j(div).removeClass()
			.addClass('hid')
			// hide all siblings
			.siblings('div')
			.removeClass()
			.addClass('hid')
			.end()
			// show file browser div
			.siblings('div#' + typeid)
			.removeClass()
			.addClass('vis')
			.end()
			// show the reset link
			.siblings('div:last')
			.removeClass()
			.addClass('vis');
}

/**
 * this method resets the file browser links
 * 
 * @param {Object} div the div which contains link pressed
 */
function resetFileBrowsing(div)
{
	// hid this div
	$j(div).removeClass()
			.addClass('hid')
			// hide all siblings
			.siblings('div')
			.removeClass()
			.addClass('hid')
			.end()
			// file browser links made visible
			.siblings('div[id^="fileLink"]')
			.removeClass()
			.addClass('vis')
			.end();
}
