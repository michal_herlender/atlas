/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	ViewEmailDetails.js
*	DESCRIPTION		:	Methods used in showemail macro.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 							
*	KNOWN ISSUES	:	-
*	HISTORY			:	27/05/2008 - SH - Created File
*					:	22/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/


/**
 * this method checks that the currently logged in user has permission to view the content of the email
 * being requested.
 * 
 * @param {Integer} id1 currently logged in personid
 * @param {Integer} id2 personid of the email sender
 * @param {Boolean} published boolean indicating if the sender chose to keep email private
 * @param {Integer} emailId id of the email to be retrieved
 * @param {String} anchorId id of the email anchor
 */
function getAuthentication(id1, id2, published, emailId, anchorId)
{
	// email published?
	if(published == false)
	{
		// load javascript service file
		loadScript.loadScriptsDynamic(new Array('dwr/interface/authenticationservice.js'), true,
		{
			callback: function()
			{
				// call dwr service and check privileges
				authenticationservice.allowPrivileges(id1, id2, allocatedSubdivId,
				{
					callback:function(allowed)
					{
						// authentication passed?
						if(allowed)
						{
							// create boxy and populate with email content
							viewEmailDetails(true, emailId, anchorId);						
						}
						else
						{
							// create boxy and populate with message indicating privilege failure and minimum email content
							viewEmailDetails(false, emailId, anchorId);
						}
					}
				});
			}
		});
	}
	else
	{
		// create boxy and populate with email content
		viewEmailDetails(true, emailId, anchorId);
	}
}									

/**
 * this method creates the email content to be shown in an overlay once authentication has been completed.
 * 
 * @param {Boolean} authenticated boolean indicating if the user passed authentication
 * @param {Integer} emailId id of the email to be retrieved
 * @param {String} anchorId id of the email anchor
 */
function viewEmailDetails(authenticated, emailId, anchorId)
{
	// variable for re-print content
	var content = '';
	// load javascript service file
	loadScript.loadScriptsDynamic(new Array('dwr/interface/emailservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to retrieve the email content
			emailservice.getCompleteEmail(emailId,
			{
				callback:function(results)
				{	
					var ccCount = 0;
					// check for carbon copy
					$j.each(results.email.recipients, function(i)
					{
						if (results.email.recipients[i].type == 'EMAIL_CC')
						{
							ccCount++;
						}
					});
					// create email content
					content += 	'<div id="emailOverlay">';
						content += '<div class="clear"></div>';
									// failed authentication?
									if (!authenticated)
									{
										// add warning box to display error messages if the link fails
							content +=	'<div class="warningBox1">' +
											'<div class="warningBox2">' +
												'<div class="warningBox3">' +
													'<p>' + i18n.t("core.system:mail.permissionDeclinedToViewContent", "Permission has been declined to view the contents of this email.") + '</p>' +
												'</div>' +
											'</div>' +
										'</div>';
									}								
							content +=	'<fieldset>' +											
											'<ol>' +
												'<li>' +
													'<label>' + i18n.t("core.system:mail.to", "To:") + '</label>' +
													'<div class="float-left padtop">';
														// add each recipient
														$j.each(results.email.recipients, function(i)
														{													
															if (results.email.recipients[i].type == 'EMAIL_TO')
															{
																content = content + '<div>' + results.email.recipients[i].emailAddress + '</div>';
															}													
														});
										content += 	'</div>' +
													'<div class="clear-0"></div>' +
												'</li>';
												if (ccCount > 0)
												{
													content += 	'<li>' +
																	'<label>' + i18n.t("core.system:mail.cc", "Cc") + ':</label>' +
																	'<div class="float-left padtop">';
																		// add each carbon copy
																		$j.each(results.email.recipients, function(i)
																		{
																			if (results.email.recipients[i].type == 'EMAIL_CC')
																			{
																				content = content + '<div>' + results.email.recipients[i].emailAddress + '</div>';
																			}
																		});
														content += 	'</div>' +
																	'<div class="clear-0"></div>' +
																'</li>';
												}
									content +=	'<li>' +
													'<label>' + i18n.t("core.system:mail.sentOnBy", "Sent On/By:") + '</label>' +
													'<span>' + cwms.dateTimeFormatFunction(results.email.sentOn) + ' / ' + results.email.sentBy.name + '</span>' +
												'</li>' +
												'<li>' +
													'<label>' + i18n.t("core.system:mail.sentFrom", "Sent From:") + '</label>' +
													'<span>' + results.email.from + '</span>' +
												'</li>' +
												'<li>' +
													'<label>' + i18n.t("core.system:mail.subject", "Subject:") + '</label>' +
													'<div class="float-left padtop">' + 
														'<div>' + results.email.subject + '</div>' +
													'</div>' +
													'<div class="clear-0"></div>' +
												'</li>' +
												'<li>' +
													'<label>' + i18n.t("core.system:mail.attachments", "Attachments:") + '</label>' +
													'<div class="float-left padtop">';
														// passed authentication?
														if (authenticated)
														{
															if (results.attachments.length > 0)
															{
																$j.each(results.attachments, function(i)
																{															
																	content += '<div><a href="downloadfile.htm?file=' + results.attachments[i][2] + '" target="_blank" class="mainlink">' + results.attachments[i][0] + '</a></div>';
																});
															}
															else
															{
																content += '<div>' + i18n.t("core.system:mail.noAttachmentsSent", "No Attachments Sent") + '</div>';
															}													
														}
														else
														{
															content += '<div>' + i18n.t("core.system:mail.permissionDeclined", "Permission Declined") + '</div>';	
														}												
										content +=	'</div>' +
													'<div class="clear-0"></div>' +
												'</li>' + 
												'<li>' +														
													'<div class="width96" style=" padding: 10px; background: #fff; ">';
														// passed authentication?
														if (authenticated)
														{
															content += '<div id="emailBody">' + results.body + '</div>';
														}
														else
														{
															content += '<div>' + i18n.t("core.system:mail.permissionDeclined", "Permission Declined") + '</div>';	
														}												
										content +=	'</div>' +														
												'</li>' +  
											'</ol>' +
										'</fieldset>' +
										'<div>&nbsp;</div>' +
									'</div>';
											
						// create overlay to display email
						loadScript.createOverlay('dynamic', i18n.t("core.system:mail.viewEmailDetails", {varSubject : results.email.subject, defaultValue : "View Email '" + results.email.subject + "'"}), null, content, null, 74, 860, '');				
				}
			});
		}
	});
}


/**
 * this method creates the content to review an email before it is sent
 * 
 * @param {Object} emailWrapper  java object wrapper containing email, email body and any attachments
 * @param {Boolean} newoverlay do we require a new overlay or modify exising?
 * @param {String} sendButtonContent the content of the send button with click event required
 */
function previewEmailDetails(emailWrapper, newoverlay, sendButtonContent)
{
	// variable for re-print content
	var content = '';
	// count cc's
	var ccCount = 0;
	// check for carbon copy
	$j.each(emailWrapper.email.recipients, function(i, recip)
	{
		if (recip.type == 'EMAIL_CC')
		{
			ccCount++;
		}
	});
	// create email content
	content += 	'<div id="previewemailOverlay">' +									
						'<fieldset>' +											
							'<ol>' +
								'<li>' +
									'<label>&nbsp;</label>' +
									'<div class="float-left">' +
										sendButtonContent +
										'<input type="button" value="' + i18n.t("cancel", "Cancel") + '" onclick=" window.parent.loadScript.closeOverlay(); return false; " />' +
									'</div>' +
									'<div class="clear"></div>' +
								'</li>' +
								'<li>' +
									'<label>' + i18n.t("core.system:mail.to", "To:") + '</label>' +
									'<div class="float-left padtop">';
										// add each recipient
										$j.each(emailWrapper.email.recipients, function(i, recip)
										{													
											if (recip.type == 'EMAIL_TO')
											{
												content += '<div>' + recip.emailAddress + '</div>';
											}													
										});
						content += 	'</div>' +
									'<div class="clear-0"></div>' +
								'</li>';
								if (ccCount > 0)
								{
									content += 	'<li>' +
													'<label>' + i18n.t("core.system:mail.cc", "Cc") + ':</label>' +
													'<div class="float-left padtop">';
														// add each carbon copy
														$j.each(emailWrapper.email.recipients, function(i, recip)
														{
															if (recip.type == 'EMAIL_CC')
															{
																content += '<div>' + recip.emailAddress + '</div>';
															}
														});
										content += 	'</div>' +
													'<div class="clear-0"></div>' +
												'</li>';
								}
					content +=	'<li>' +
									'<label>' + i18n.t("core.system:mail.subject", "Subject:") + '</label>' +
									'<div class="float-left padtop">' + 
										'<div>' + emailWrapper.email.subject + '</div>' +
									'</div>' +
									'<div class="clear-0"></div>' +
								'</li>';
								// attachments?
								if (emailWrapper.attachments)
								{
									if (emailWrapper.attachments.length > 0)
									{
										content +=	'<li>' +
														'<label>' + i18n.t("core.system:mail.attachments", "Attachments:") + '</label>' +
														'<div class="float-left padtop">';															
															$j.each(emailWrapper.attachments, function(i)
															{															
																content += '<div><a href="downloadfile.htm?filename=' + emailWrapper.attachments[i][2] + '" target="_blank" class="mainlink">' + emailWrapper.attachments[i][0] + 'Test Name</a></div>';
															});										
											content +=	'</div>' +
														'<div class="clear-0"></div>' +
													'</li>';
									}
								}
					content +=	'<li>' +														
									'<div class="width96" style=" padding: 10px; background: #fff; ">' +
										'<div id="emailBody">' + emailWrapper.body + '</div>' +
									'</div>' +														
								'</li>' +
								'<li>' +
									'<label>&nbsp;</label>' +
									'<div class="float-left">' +
										sendButtonContent +
										'<input type="button" value="' + i18n.t("cancel", "Cancel") + '" onclick=" window.parent.loadScript.closeOverlay(); return false; " />' +
									'</div>' +
									'<div class="clear"></div>' +
								'</li>' +
							'</ol>' +
						'</fieldset>' +
						'<div>&nbsp;</div>' +
					'</div>';
		// require new overlay?
		if (!newoverlay)
		{
			// modify an existing overlay
			loadScript.modifyOverlay(null, content, i18n.t("core.system:mail.viewEmailDetails", {varSubject : emailWrapper.email.subject, defaultValue : "Preview Email '" + emailWrapper.email.subject  + "'"}), null, 74, 920, null);
		}
		else
		{
			// create overlay to display email
			loadScript.createOverlay('dynamic', i18n.t("core.system:mail.viewEmailDetails", {varSubject : emailWrapper.email.subject, defaultValue : "Preview Email '" + emailWrapper.email.subject  + "'"}), null, content, null, 74, 920, '');
		}
}