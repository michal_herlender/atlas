/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	SentEmail.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	29/05/2008
*					:	22/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * this method cleans up after an email has been sent. Populates the entity email table and if a costing updates the issue link
 */
function cleanUpAfterEmail()
{
	// has an email been sent from within thickbox?
	if ($j('input[name="emailId"]').length)
	{
		populateEntityEmail($j('input[name="emailId"]').val(), $j('input[name="currentContactId"]').val(), $j('input[name="sysCompId"]').val(), $j('input[name="entityId"]').val());
	}
	// has an email been sent for a costing
	if ($j('input[name="costingId"]').length)
	{
		// load the service javascript file if not already
		loadScript.loadScriptsDynamic(new Array('dwr/interface/jobcostingservice.js'), true,
		{
			callback: function()
			{
				jobcostingservice.findAjaxEagerJobCosting($j('input[name="costingId"]').val(),
				{
					callback:function(result)
					{
						// retrieve job costing successful?
						if (result.success)
						{
							// set costing object
							var costing = result.results;
							// update status name
							$j("a#issueCostingLink", window.parent.document).parent().prev().empty().append(costing.status.name);
							// remove anchor to issue costing
							$j("a#issueCostingLink", window.parent.document).parent().remove();
						}
						else
						{
							// show user the message
							$j.prompt(result.message);
						}
					}
				});
			}
		});
	}
}

/**
 * this method populates the email table on any entity page
 * 'window.parent.document' used because this page is displayed within an iframe and so we need to reference the parent
 * 
 * @param {Integer} emailId the id of the email to display
 * @param {Integer} ccId the id of the current contact
 * @param {Integer} sysCompId id of the system component
 * @param {Integer} entityId id of the entity email was added to
 */
function populateEntityEmail(emailId, ccId, sysCompId, entityId)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/emailservice.js'), true,
	{
		callback: function()
		{
			emailservice.getAjaxCompleteEmail(emailId,
			{
				callback:function(result)
				{
					// retrieve email successful?
					if (result.success)
					{
						// assign email wrapper to variable
						var emailWrap = result.results;							
						// create new page content
						var content = 	'<tr id="email' + emailWrap.email.id + '">' +
											'<td class="tofield">';
												$j.each(emailWrap.email.recipients, function(i, r)
												{
													if (r.type.type == 'EMAIL_TO')
													{
														content += '' + r.emailAddress + '<br/>';
													}
												});
								content += 	'</td>' +
											'<td class="ccfield">';
												$j.each(emailWrap.email.recipients, function(i, r)
												{
													if (r.type.type == 'EMAIL_CC')
													{
														content += '' + r.emailAddress + '<br/>';
													}
												});
								content += 	'</td>' +
											'<td class="subject">' + emailWrap.email.subject + '</td>' +
											'<td class="senton">' + cwms.dateTimeFormatFunction(emailWrap.email.sentOn) + '</td>' +
											'<td class="sentby">' + emailWrap.email.sentBy.name + '</td>' +
											'<td class="view">' +
												'<a href="#" id="email_anchor' + emailWrap.email.id + '" onclick=" getAuthentication(' + ccId + ', ' + emailWrap.email.sentBy.personid + ', ' + emailWrap.email.publish + ', ' + emailWrap.email.id + ', this.id); return false; ">' +
													'<img src="img/icons/email_open.png" width="16" height="16" alt="' + i18n.t("core.system:mail.viewEmailDetails", "View Email Details") + '" title="' + i18n.t("core.system:mail.viewEmailDetails", "View Email Details") + '" />' +
												'</a>' +								
											'</td>' +
											'<td class="resend">' +
												'<a href="#" onclick=" loadScript.createOverlay(\'external\', \'' + i18n.t("core.system:mail.resendEmail", "Re-send Email") + '\', \'createemail.htm?syscompid=' + sysCompId + '&objid=' + entityId + '&emailid=' + emailWrap.email.id + '\', null, null, 74, 860, null); return false; " class="float-right padright20">' +
													'<img src="img/icons/email_go.png" width="16" height="16" alt="' + i18n.t("core.system:mail.resendEmail", "Re-send Email") + '" title="' + i18n.t("core.system:mail.resendEmail", "Re-send Email") + '" class="img_marg_bot" />' +
												'</a>' +								
											'</td>' +
										'</tr>';
						// is this the first email to be added?
						if ($j('div#entityEmailDisplay table tbody tr:first td', window.parent.document).length < 2)
						{
							// remove the first table row
							$j('div#entityEmailDisplay table tbody tr:first', window.parent.document).remove();
						}
						// append email to page
						$j('div#entityEmailDisplay table tbody', window.parent.document).append(content);
						// update email counts on page
						$j('span.entityEmailDisplaySize', window.parent.document).text(parseInt($j('span.entityEmailDisplaySize:first', window.parent.document).text()) + 1);
					}
					else
					{
						// show user the message
						$j.prompt(result.message);
					}
				}
			});	
		}
	});
}