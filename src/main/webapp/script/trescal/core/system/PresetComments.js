/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Jamie Vinall
*
*	FILENAME		:	PresetComments.js
*	DESCRIPTION		:	Page specific javascript for the presetcomments.vm page.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	23/10/2007 - JV - Created File
*					:   16/04/2009 - SH - JavaScript code updated.
*					:	22/10/2015 - TProvost - Manage i18n
*					:	22/10/2015 - TProvost - Minor fix on enable button in functions addSavedComment
*					:	22/10/2015 - TProvost - Fix bug in new version of Impromptu.js - Replace the "callback" property (property disappeared) by the "submit" property
*					:							=> Changes made in function deleteSavedComment
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * this method creates content for adding or editing saved comments
 * 
 * @param {String} action the action we wish to perform (i.e Add/Edit)
 * @param {Integer} id if editing the id of the comment to be edited
 * @param {String} comment if editing the comment to be edited
 * @param {String} commentType the type of comment to be added/edited
 * @param {String} noteType if adding saved comments for notes the note type we are using
 */
function savedComment(action, id, comment, commentType, noteType) {
	var titleAction = '';
	var overlayUrl = '';
	if (action == 'Add') {
		titleAction = i18n.t("core.system:presetComment.addSavedComment", "Add Saved Comment");
		overlayUrl = 'presetcommentaddoverlay.htm';
	}
	else {
		titleAction = i18n.t("core.system:presetComment.editSavedComment", "Edit Saved Comment");
		overlayUrl = 'presetcommenteditoverlay.htm';
	}
	$j.ajax({
		url: overlayUrl,
		data: {
			commentId: id,
			commentType: commentType,
			noteType: noteType
		},
		async: true
	}).done(function(content) {
		// create overlay
		loadScript.createOverlay('dynamic', titleAction, null, content, null, 40, 820, 'comment');
		// add spell check functionality
		loadScript.addSpellCheck($j('div#savedCommentOverlay textarea#comment'));
	});
}

/**
 * this method saves a comment and adds it to the saved comment table on the page
 * 
 * @param {Object} button the pressed button object
 */
function addSavedComment(button) {
	// disable the button so it cannot be pressed twice
	$j(this).attr('disabled','disabled'); 
	var presetComment = {
			categoryId: $j('select#categoryId').val(),
			label: $j('input#label').val(),
			comment: $j('textarea#comment').val(),
			commentType: $j('input#commentType').val()
	};
	$j.ajax({
		url: "addpresetcomment.json",
		type: "POST",
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify(presetComment),
		async: true
	}).done(function(entry) {
		// check if this is the first row in table?
		if ($j('table#savedCommentTable tbody tr:first td').length < 2) {
			// remove the message row from table
			$j('table#savedCommentTable tbody tr:first').remove();
		}
		// add the new saved comment to the table
		$j('table#savedCommentTable tbody').append(entry);
		// update the saved comment count
		$j('span#savedCommentSize').text(parseInt($j('span#savedCommentSize').text()) + 1);
		// close the overlay
		loadScript.closeOverlay();
	}).fail(function(error) {
		$j('.warningBox3').append(error.responseJSON.message);
		$j('.warningBox1').removeClass('hid');
		$j(button).removeAttr('disabled');
	});
}

/**
 * this method updates a saved comment and updates the page display
 * 
 * @param {Object} button the pressed button object
 */
function editSavedComment(button) {
	// disable the button so it cannot be pressed twice
	$j(button).attr('disabled', 'disabled');
	var id = $j('input#id').val();
	var label = $j('input#label').val();
	var comment = $j('textarea#comment').val();
	var categoryName = $j('select#categoryId').find('option:selected').html();
	var presetComment = {
			id: id,
			categoryId: $j('select#categoryId').val(),
			label: label,
			comment: comment,
			commentType: $j('input#commentType').val()
	};
	$j.ajax({
		url: "editpresetcomment.json",
		type: "POST",
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify(presetComment),
		async: true
	}).done(function(data, status, xhr) {
		// update the comment in table cell
		$j('tr#row' + id + ' td:eq(0)').html(categoryName);
		$j('tr#row' + id + ' td:eq(1)').html(label);
		$j('tr#row' + id + ' td:eq(2)').html(comment);
		// close the overlay
		loadScript.closeOverlay();
	}).fail(function(error) {
		$j('.warningBox3').append(error.responseText);
		$j('.warningBox1').removeClass('hid');
		$j(button).removeAttr('disabled');
	});
}

/**
 * this method deletes a saved comment and removes it from the page
 * 
 * @param {Integer} id the id of the saved comment that has been edited
 */
function deleteSavedComment(id)
{
	if(confirm(i18n.t("core.system:presetComment.confirmDeleteSavedComment", "Are you sure that you wish to delete this comment?"))) {
		$j.ajax({
			url: "deletepresetcomment.json",
			type: "POST",
			dataType: "json",
			data: {
				commentId: id
			},
			async: true
		}).done(function() {
			// remove saved comment row from table
			$j('#row' + id).remove();
			// check if this is the last row to be removed
			if ($j('table#savedCommentTable tbody tr').length < 1) {
				// remove the message row from table
				$j('table#savedCommentTable tbody').append(
						'<tr>' +
							'<td colspan="5" class="text-center bold">' + i18n.t("core.system:presetComment.noCommentsSaved", "No Comments Saved") + '</td>' +
						'</tr>');
			}
			// update the saved comment count
			$j('span#savedCommentSize').text(parseInt($j('span#savedCommentSize').text()) - 1);
		}).fail(function(error) {
			alert("Sorry, an error occurs while deleting preset comment.");
		});
	}
}