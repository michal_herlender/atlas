/**
 * FILENAME: script/trescal/core/system/AddEmailTemplate.js
 */

/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'subject';