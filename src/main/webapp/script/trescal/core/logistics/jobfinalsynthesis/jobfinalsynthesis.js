function checkChild(index) {
	if ($j('input[id=parent_' + index + ']').is(':checked')) {
		$j('input[id^=child_' + index + '_]').each(function() {
			$j(this).attr('checked', true);
		});
	} else {
		$j('input[id^=child_' + index + '_]').each(function() {
			$j(this).attr('checked', false);
		});
	}
}

function checkParent(index_p, index_c) {
	if ($j('input[id=child_' + index_p + '_' + index_c + ']').is(':checked')) {
		var all_checked = true;
		$j('input[id^=child_' + index_p + '_]').each(function() {
			if (all_checked == true && !$j(this).is(':checked')) {
				all_checked = false;
			}
		});
		if (all_checked)
			$j('input[id=parent_' + index_p + ']').attr('checked', true);
	} else {
		var all_nonchecked = true;
		$j('input[id^=child_' + index_p + '_]').each(function() {
			if (all_nonchecked == true && $j(this).is(':checked')) {
				all_nonchecked = false;
			}
		});
		if (all_nonchecked)
			$j('input[id=parent_' + index_p + ']').attr('checked', false);
	}
}

function checkAllItems(ischecked)
{
	if(ischecked)
		{
			$j('input[id^=parent_]').attr('checked', true);
			$j('input[id^=child_]').attr('checked', true);
		}
	else
		{
			$j('input[id^=parent_]').attr('checked', false);
			$j('input[id^=child_]').attr('checked', false);
		}
}

function checkItemByPlantid(barcode) {
	if ($j("input[data-barcode='" + barcode + "']").length > 0) {
		// check
		$j("input[data-barcode='" + barcode + "']").first().prop("checked",
				true);
		// highlight check box
		$j("input[data-barcode='" + barcode + "']").first().parent().effect(
				"highlight", {
					color : 'lightgreen'
				}, 500);
		// highlight input box
		$j("#barcodeScanInput").effect("highlight", {
			color : 'lightgreen'
		}, 500);
	}
}
function checkItemByPlantno(plantno) {
	if ($j("input[data-plantno='" + plantno + "']").length > 0) {
		$j("input[data-plantno='" + plantno + "']").first().prop("checked",
				true);
		// highlight check box
		$j("input[data-plantno='" + plantno + "']").first().parent().effect(
				"highlight", {
					color : 'lightgreen'
				}, 500);
		// highlight input box
		$j("#plantnoScanInput").first().effect("highlight", {
			color : 'lightgreen'
		}, 500);
	}
}