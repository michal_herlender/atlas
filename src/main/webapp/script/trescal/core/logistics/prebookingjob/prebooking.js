
/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */	

function deleteAsn(ansid){
	var yes = i18n.t("yes", "Yes");
	var no = i18n.t("no", "No");
	$j.prompt(i18n.t("delete", "delete")+" ?", {
		buttons: { [no]: false, [yes]: true },
		submit: function(e,v,m,f){
			
			console.log("Value clicked was: "+ v);
			if(v){
				// make the ajax call
				window.location.replace("deleteprebooking.htm?asnid="+ansid);
			}
		}
	});
}
