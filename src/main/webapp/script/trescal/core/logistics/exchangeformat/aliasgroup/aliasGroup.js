

function addEditAliasGroup(action, aliasgroupid) {

	var titleAction = '';
	var contentUri = 'editaliasgroupoverlay.htm';
	if (action == 'add') {
		titleAction = i18n.t("core.admin:aliasGroup.addAliasGroup","Add Alias Group");
	} else if (action == 'edit') {
		titleAction = i18n.t("core.admin:aliasGroup.editAliasGroup","Edit Alias Group");
		contentUri = contentUri + '?aliasgroupid=' + aliasgroupid;
		
	}
	// create new overlay
	loadScript.createOverlay('external', titleAction, contentUri, null, null,80, 820, null);
}


// add a line to the table
function addAliasField(){

	var field = $j('#aliasField').find(":selected").text();
	var index = document.getElementById("aliasFieldsTable").getElementsByTagName("tr").length;

	if(field!==''){
		var html='<tr class="even">' + 
		'<td class="center"><span>'+ (index-1) +'</span></td>'+
		'<td class="center">' +$j('#aliasField').find(":selected").text() +
			'<input type="hidden" id="aliasesForm'+(index-1)+'.effn" name="aliasesForm['+(index-1)+'].effn" value="'+$j('#aliasField').find(":selected").val()+'">'+
		'</td>' +
		'<td class="center">'+
			'<select id="fieldvalues_'+(index-1)+'" name="aliasesForm['+(index-1)+'].value">'+
					
			'</select>'+
		'</td>' +
		'<td class="center"> <input type="text" id="aliasesForm'+(index-1)+'.alias" name="aliasesForm['+(index-1)+'].alias" /></td>'+ 
		'<td class="center"> <img src="img/icons/delete.png" height="16" width="16" alt="" title="" class="delimg" onclick="deleteAliasField(this)" style="cursor: pointer;"/>'+
		'</tr>';
		
		$j("#aliasFieldsTable tbody").append(html);
	}
	
}

// delete a row form the table
function deleteAliasField(row){
	var item = $j(row);
	item.closest("tr").remove();
	updatePosition();
}


function initSelectValues(val){
	// filter using the passed in val
	var table = columnvalues.filter(c=>{
		return c.type.includes(val);
	});

	// select initialization
	var index = document.getElementById("aliasFieldsTable").getElementsByTagName("tr").length;
	var select = $j('#fieldvalues_'+(index-2));
	select.html('');
	
	// populating select with options
	for(i = 0; i < table.length; i++){
			select.append('<option value="'+table[i].name+'">'+table[i].translation+' </option>');
	}	
}


function filterBySelectedValue(val){
	// filter the table using the selected value
	if(val!=null){
		$j("#aliasGroupDetailTable tbody tr").addClass("hid");
		$j("tr#bodyTr_"+val+"").removeClass("hid");
	}
}


function updatePosition() {

	$j("#aliasFieldsTable tbody tr td:first-child span").each(
			function(index) {
				$j(this).text(index + 1);
	});
	
	updateIndexes();
}

function updateIndexes(){

	$j("#aliasFieldsTable tbody tr td:first-child input").each(
			function(index) {
				$j(this).attr("name","aliasesForm["+index+"].aliasId");
	});
	
	$j("#aliasFieldsTable tbody tr td:nth-child(2) input").each(
			function(index) {
				$j(this).attr("name","aliasesForm["+index+"].effn");
	});
	
	$j("#aliasFieldsTable tbody tr td:nth-child(3) select").each(
			function(index) {
				$j(this).attr("name","aliasesForm["+index+"].value");
	});
	
	$j("#aliasFieldsTable tbody tr td:nth-child(4) input").each(
			function(index) {
				$j(this).attr("name","aliasesForm["+index+"].alias");
	});
}














