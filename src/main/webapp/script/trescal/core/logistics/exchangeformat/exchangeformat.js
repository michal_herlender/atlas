var pageImports = new Array('script/thirdparty/jQuery/jquery.tablednd.js');

var index;

function init() {
	// Initialise the table
	$j("#table_ef").tableDnD({
		onDragClass : "myDragClass",
		onDrop : function(table, rows) {
			console.log(rows);
			updateEchangeFormatPosition();
		}
	});

	index = $j("#table_ef tbody tr").length;
	
	afterFieldNameItemsLoad();
}

function deleteFieldItem(row) {
	var item = $j(row);
	// get the deleted back to select
	var name = item.attr('data-field-name');
	var id = item.attr('data-field-id');
	// The 'Undefined' field remains in the select box, so it can be added multiple times
	if (name != 'Undefined')
		$j('#fieldNameItems').append(
				'<option value="'
						+id
						+ '">'
						+name
						+ '</option>');
	// deleted row
	item.closest("tr").remove();
	updatePosition();
}

function afterFieldNameItemsLoad()
{
	$j("#table_ef tbody tr td:nth-child(2) input").each(
			function(index) {
				var optionValue = $j(this).val();
				console.log(optionValue);
				// We keep the UNDEFINED value as it's OK to have multiple times
				if (optionValue != 'UNDEFINED') {
					$j("#fieldNameItems option[value='"+$j(this).val()+"']").remove();
				}
			});
	
	if(type == undefined || type == '')
		onFieldsNameChange('PREBOOKING');
	else
		onFieldsNameChange(type);
}

function addFieldItem() {
	var field = $j('#fieldNameItems').find(":selected").text();
	if (field != '') {
		var html = '<tr class="even">' + '<td class="center"><span>'
				+ (index + 1)
				+ '</span><input type="hidden" id="exchangeFormatFieldNameDetailsForm'
				+ index
				+ '.id" name="exchangeFormatFieldNameDetailsForm['
				+ index
				+ '].id"/><input class="position" type="hidden" id="exchangeFormatFieldNameDetailsForm'
				+ index
				+ '.position" name="exchangeFormatFieldNameDetailsForm['
				+ index
				+ '].position"  value="'
				+ (index + 1)
				+ '"/></td>'
				+ '<td class="name">'
				+ $j('#fieldNameItems').find(":selected").text()
				+ '<input type="hidden" id="exchangeFormatFieldNameDetailsForm'
				+ index
				+ '.fieldName" name="exchangeFormatFieldNameDetailsForm['
				+ index
				+ '].fieldName"  value="'
				+ $j('#fieldNameItems').find(":selected").val()
				+ '"/></td>'
				+ '<td class="center"><input type="checkbox" id="exchangeFormatFieldNameDetailsForm'
				+ index
				+ '.mandatory" name="exchangeFormatFieldNameDetailsForm['
				+ index
				+ '].mandatory"/></td>'
				+ '<td class="center"><input type="text" id="exchangeFormatFieldNameDetailsForm'
				+ index
				+ '.templateName" name="exchangeFormatFieldNameDetailsForm['
				+ index
				+ '].templateName"/></td>'
				+ '<td class="center">'
				+ '<img src="img/icons/delete.png" height="16" width="16" alt="" title="" class="delimg" onclick="deleteFieldItem(this)" id="delete_'
				+ field
				+ '" data-field-name="'
				+ field
				+ '" data-field-id="'
				+ $j('#fieldNameItems').find(":selected").val()
				+ '"/>'
				+ '<!-- span onMouseOver="this.style.color=\'#0F0\'" onMouseOut="this.style.color=\'#00F\'">delete</span-->'
				+ '</td>' + '</tr>';
		// We want to allow adding multiple 'Undefined' fields
		if ($j('#fieldNameItems').find(":selected").val() != 'UNDEFINED') {
			$j('#fieldNameItems').find(":selected").remove();
		}
		$j("#table_ef tbody").append(html);
		index++;
		$j("#table_ef").tableDnD({
			onDragClass : "myDragClass",
			onDrop : function(table, rows) {
				console.log(rows);
				updatePosition();

			}
		});

		$j('#table_ef tbody tr td img[id^="delete"]').click(
				function() {
					console.log($j(this).attr('name'));
					var name = $j(this).attr('name');
					if ($j(
							'#table_ef tbody tr td span[name="' + name
									+ '"]').data('field-name') != undefined)
						$j('#fieldNameItems').append(
								'<option value="'
										+ $j(
												'#table_ef tbody tr td span[name="'
														+ name + '"]').data(
												'field-id')
										+ '">'
										+ $j(
												'#table_ef tbody tr td span[name="'
														+ name + '"]').data(
												'field-name') + '</option>');
					$j(this).closest("tr").remove();
					index--;
					updatePosition();

				});

	}
}

function updateEchangeFormatPosition() {
	$j("#table_ef tbody tr td:first-child .position").each(
			function(index) {
				console.log($j(this).attr('name'));
				$j(this).val(index + 1);
			});
	$j("#table_ef tbody tr td:first-child span").each(
			function(index) {
				$j(this).text(index + 1);
			});
}

function chooseSubdivOrCompanyRadioChanged(){
	$j("#businessCompanySelector").toggleClass("hidden");
	$j("#businessSubdivSelector").toggleClass("hidden");
}


	
function onFieldsNameChange(val) {
	
	//table filter using TYPE constraint
	var table = columnsTypes.filter(c=>{
		return c.types.includes(val);
		}
	);
	
	//select initialization 
	var select = $j('#fieldNameItems');
	select.html('');
	for(i = 0; i < table.length; i++)
		{
		if(! $j('#table_ef tbody tr input[value="'+table[i].name+'"]').length > 0 || table[i].name == 'UNDEFINED')
		select.append('<option value="'+table[i].name+'">'+table[i].translation+'</option>');
		}
	
	//sort select options
	var options = $j('#fieldNameItems option');
	var arr = options.map(function(_, o) { return { t: $j(o).text(), v: o.value }; }).get();
	arr.sort(function(o1, o2) { return o1.t > o2.t ? 1 : o1.t < o2.t ? -1 : 0; });
	options.each(function(i, o) {
	  o.value = arr[i].v;
	  $j(o).text(arr[i].t);
	});
}
