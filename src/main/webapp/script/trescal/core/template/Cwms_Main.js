/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	Cwms_Main.js
*	DESCRIPTION		:	Javascript file run on every page load.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	Created by SH 06/06/2007
*					:	21/09/2015 - TProvost - Internationalization of the javascript :
*							=> Add import library i18next-latest.js
*							=> Add initialization of i18n in the perform function
*					:	22/10/2015 - TProvost - Manage i18n
*					:	26/10/2015 - TProvost - Fix bug in function restrictSpaces (avoids displaying several times the prompt msg)
****************************************************************************************************************************************/

/*
 *  Provide a no-conflict instance of jQuery (In CWMS2, had been manually added to jquery.js)
 */
var $j = jQuery.noConflict();

/**
 * this variable is used by the restrictSpaces function in order to avoids displaying several times the prompt msg
 */
var userPrompted = false;

/**
 * array of javascript files to import into cwms_main template
 */

var i18n = {
	init: () => {},
	t:s => s
}

var mainImports = new Array (	'script/thirdparty/jQuery/jquery.antechplugins.js',
								'script/thirdparty/jQuery/jquery.impromptu.js',
								'script/trescal/core/utilities/Date.js',
								'dwr/engine.js',
								'dwr/util.js',
								'dwr/interface/dwrservice.js',
								'script/thirdparty/i18next/i18next-latest.js'
								);
		
/**
 * array which holds all the javascript files which have been loaded into the page in this file.
 */
var loadedSCRIPTS = new Array();

/**
 * array which holds all the stylesheet files which have been loaded into the page in this file.
 */
var loadedSTYLES = new Array();

/**
 * contains object literals for importing javascript files and checking whether a specific menu
 * tab should be loaded when returning to a page
 */
var loadScript = 
{	
		
	/**
	 * these three variables can be used in any page specific javascript file to check the
	 * time value of last DWR call, checked against the current DWR call.
	 */
	lastUpdate1:	0,
	lastUpdate2:	0,
	lastUpdate3:	0,
	lastUpdate4:	0,
	
	/**
	 * imports the javascript file passed in libraryName
	 * 
	 * @param {String} libraryName is a javacript file path to be loaded
	 */
  	require: 	function(libraryName) 
				{
				    // inserting via DOM fails in Safari 2.0, so brute force approach
				    document.write('<script type="text/javascript" src="' + libraryName + '"></script>');
			  	},
	
	/**
	 * iterates through the array passed and imports all javascript files if they
	 * are not already present on the page.
	 * 
	 * @param {Array} Imports is a string array of javascript file paths to load
	 */
	loadScriptImports:	function(Imports) 
						{
							// loop through each script to load
							$j.each( Imports, function(i, n)
							{
								var loaded = false;
								
								// loop through the loaded scripts and check if
								// the one to be loaded is present 
								$j.each( loadedSCRIPTS, function(x, l)
								{
									// already loaded
									if (n == l)
									{
										loaded = true;
									}
								});
								// not loaded
								if (loaded == false)
								{ 
									// load
									loadScript.require(n);
									// add to loaded array
									loadedSCRIPTS.push(n);
								}
							});
														
						},
	
	/**
	 * Applies corners to any elements passed within the array
	 * 
	 * @param {Object} Elements is an array of objects containing an element name to apply corners to and the type of corners to apply.
	 */
	loadNifty:		function(Elements)
					{
						loadScript.loadScriptsDynamic(new Array('script/thirdparty/niftyCorners/niftycube.js'), true,
						{
							callback: function()
							{
								$j.each(Elements, function(i)
								{
									Nifty(Elements[i].element, Elements[i].corner);
								});
							}
						});
					},
					
	/**
	 * this method creates the master symbolbox content which is used in various places on system (PresetCommentJQPlugin.js, Note.js, SymbolJQPlugin.js)
	 * 
	 * add id value to table cell so copy and paste functionality works.
	 */
	masterSymbolBoxContent:		function()
								{
									var content =	'<div class="symbolBoxDiv">' +
														'<div title="' + i18n.t("core.template:closeSymbolBox", "Close Symbol Box") + '">&nbsp;</div>' +
														'<table class="default2">' +
															'<tbody>' +
																'<tr>' +
																	'<td id="symEuro" title="' + i18n.t("core.template:addEuroSymbol", "Add Euro Symbol") + '">&euro;</td>' +											
																	'<td id="symDollar" title="' + i18n.t("core.template:addDollarSymbol", "Add Dollar Symbol") + '">&#36;</td>' +
																	'<td id="symTimes" title="' + i18n.t("core.template:addTimesSymbol", "Add Times Symbol") + '">&times;</td>' +
																	'<td id="symDivide" title="' + i18n.t("core.template:addDivideSymbol", "Add Divide Symbol") + '">&divide;</td>' +
																'</tr>' +
																'<tr>' +
																	'<td id="symPi" title="' + i18n.t("core.template:addPSymbol", "Add Pi Symbol") + '">&#960;</td>' +											
																	'<td id="symAlphaDelta" title="' + i18n.t("core.template:addAlphaDeltaSymbol", "Add Alpha Delta Symbol") + '">&#916;</td>' +
																	'<td id="symDegree" title="' + i18n.t("core.template:addDegreeSymbol", "Add Degree Symbol") + '">&deg;</td>' +
																	'<td id="symPlusMinus" title="' + i18n.t("core.template:addPlusMinusSymbol", "Add Plus/Minus Symbol") + '">&plusmn;</td>' +
																'</tr>' +
																'<tr>' +
																	'<td id="symMicro" title="' + i18n.t("core.template:addMicroSymbol", "Add Micro Symbol") + '">&micro;</td>' +
																	'<td id="symOmega" title="' + i18n.t("core.template:addOmegaSymbol", "Add Omega Symbol") + '">&#8486;</td>' +
																	'<td id="symSuper2" title="' + i18n.t("core.template:addSuperscriptTwo", "Add Superscript Two") + '">-&#178;</td>' +
																	'<td id="symSuper3" title="' + i18n.t("core.template:addSuperscriptThree", "Add Superscript Three") + '">-&#179;</td>' +
																'</tr>' +
																'<tr>' +
																	'<td id="symSuper4" title="' + i18n.t("core.template:addSuperscriptFour", "Add Superscript Four") + '">-&#8308;</td>' +
																	'<td id="symSuper5" title="' + i18n.t("core.template:addSuperscriptFive", "Add Superscript Five") + '">-&#8309;</td>' +
																	'<td id="symSuper6" title="' + i18n.t("core.template:addSuperscriptSix", "Add Superscript Six") + '">-&#8310;</td>' +
																	'<td id="symSuper7" title="' + i18n.t("core.template:addSuperscriptSeven", "Add Superscript Seven") + '">-&#8311;</td>' +
																'</tr>' +
																'<tr>' +
																	'<td id="symSuper8" title="' + i18n.t("core.template:addSuperscriptEight", "Add Superscript Eight") + '">-&#8312;</td>' +
																	'<td id="symQuarter" title="' + i18n.t("core.template:addQuarterSymbol", "Add Quarter Symbol") + '">&frac14;</td>' +
																	'<td id="symHalf" title="' + i18n.t("core.template:addHalfSymbol", "Add Half Symbol") + '">&frac12;</td>' +
																	'<td id="symThreeQuarter" title="' + i18n.t("core.template:addThreeQuarterSymbol", "Add Three Quarter Symbol") + '">&frac34;</td>' +
																'</tr>' +
																'<tr>' +
																	'<td id="symSub2" title="' + i18n.t("core.template:addSubscriptTwo", "Add Subscript Two") + '">-&#8322;</td>' +
																	'<td id="symSub3" title="' + i18n.t("core.template:addSubscriptThree", "Add Subscript Three") + '">-&#8323;</td>' +
																	'<td id="symSub4" title="' + i18n.t("core.template:addSubscriptFour", "Add Subscript Four") + '">-&#8324;</td>' +
																	'<td id="" title="">&nbsp;</td>' +																	
																'</tr>' +
															'</tbody>' +
														'</table>' +
													'</div>';
									
									return content;
								},
					
	/**
	 * Applies focus to the element passed when the page is loaded.
	 * 
	 * @param {String} Elem contains the string id of the element to apply focus to
	 */
	focusOn:		function(Elem)
					{
						// check id of element first
						if ($j('#' + Elem).length > 0)
						{
							$j('#' + Elem).focus();
						}
						// else try the name?
						else if ($j('*[name="' + Elem + '"]').length > 0)
						{
							$j('*[name="' + Elem + '"]').focus();
						}						
					},
					
	/**
	 * this method unwraps errors from an error list
	 * 
	 * @param {Array} errorList error list array
	 */
	unwrapErrors:	function(errorList)
					{
						// variable to hold error messages
						var errors = '';
						// get all errors
						$j.each(errorList, function(i)
						{
							// retrieve each error
							var error = errorList[i];
							// add to string
							errors ='<div>' + i18n.t("fieldError", {varErrField: error.field, varErrMsg:error.defaultMessage, defaultValue : "Field: " + error.field + " failed. Message:" + error.defaultMessage}) + '</div>';
						});
						// return errors
						return errors;
					},

	/**
	 * Creates a jquery calendar with the parameters given in the jqCalendar array of objects
	 * 
	 * @param {Object} jqCalendarSetup contains all the parameters needed to create the calendar (input id, date format and dates to display)
	 */		
	createJQCal:	function(jqCalendarObj)
					{						
						// get new date object
						var today = new Date();
											
						// set the max or min date value and year range depending on the value 
						// submitted in the displayDates literal
						switch (jqCalendarObj.displayDates)
						{
							case 'past':	$j('input#' + jqCalendarObj.inputField).datepicker({defaultDate: null, changeMonth: true, changeYear: true, dateFormat: jqCalendarObj.dateFormat, yearRange: '-25:+00', beforeShowDay: (jqCalendarObj.showWeekends) ? null : $j.datepicker.noWeekends, maxDate: new Date(today.getFullYear(), today.getMonth(), today.getDate()), hideIfNoPrevNext: true});
											$j('input.' + jqCalendarObj.inputClass).datepicker({defaultDate: null, changeMonth: true, changeYear: true, dateFormat: jqCalendarObj.dateFormat, yearRange: '-25:+00', beforeShowDay: (jqCalendarObj.showWeekends) ? null : $j.datepicker.noWeekends, maxDate: new Date(today.getFullYear(), today.getMonth(), today.getDate()), hideIfNoPrevNext: true});
											break;
							case 'future':	$j('input#' + jqCalendarObj.inputField).datepicker({defaultDate: null, changeMonth: true, changeYear: true, dateFormat: jqCalendarObj.dateFormat, yearRange: '-00:+10', beforeShowDay: (jqCalendarObj.showWeekends) ? null : $j.datepicker.noWeekends, minDate: new Date(today.getFullYear(), today.getMonth(), today.getDate()), hideIfNoPrevNext: true});
											$j('input.' + jqCalendarObj.inputClass).datepicker({defaultDate: null, changeMonth: true, changeYear: true, dateFormat: jqCalendarObj.dateFormat, yearRange: '-00:+10', beforeShowDay: (jqCalendarObj.showWeekends) ? null : $j.datepicker.noWeekends, minDate: new Date(today.getFullYear(), today.getMonth(), today.getDate()), hideIfNoPrevNext: true});
											break;
							case 'all':		$j('input#' + jqCalendarObj.inputField).datepicker({defaultDate: null, changeMonth: true, changeYear: true, dateFormat: jqCalendarObj.dateFormat, yearRange: '-25:+10', beforeShowDay: (jqCalendarObj.showWeekends) ? null : $j.datepicker.noWeekends});
											$j('input.' + jqCalendarObj.inputClass).datepicker({defaultDate: null, changeMonth: true, changeYear: true, dateFormat: jqCalendarObj.dateFormat, yearRange: '-25:+10', beforeShowDay: (jqCalendarObj.showWeekends) ? null : $j.datepicker.noWeekends});
											break;
							case 'custom':	$j('input#' + jqCalendarObj.inputField).datepicker({defaultDate: null, changeMonth: true, changeYear: true, dateFormat: jqCalendarObj.dateFormat, beforeShowDay: (jqCalendarObj.showWeekends) ? null : $j.datepicker.noWeekends, minDate: jqCalendarObj.minDate, hideIfNoPrevNext: true});
											$j('input.' + jqCalendarObj.inputClass).datepicker({defaultDate: null, changeMonth: true, changeYear: true, dateFormat: jqCalendarObj.dateFormat, beforeShowDay: (jqCalendarObj.showWeekends) ? null : $j.datepicker.noWeekends, minDate: jqCalendarObj.minDate, hideIfNoPrevNext: true});
											break;
						}												
					},
					
	/**
	 * creates an text search autocomplete with the parameters given in the jquiAutocompleteSetup object
	 * 
	 * @param {Object} jquiAutocompleteSetup object with initialization parameters:
	 * inputFieldName - (html id of input field that autocomplete is being created for, e.g. "descName") 
	 * source - (source data to search, e.g. "searchdescription.json")
	 * inputFieldId - (html id of hidden input field - e.g. "descId" - containing default value selected, 0 if no selection)
	 * afterSelect - OPTIONAL function reference to call after selection occurs  
	 * onChange - OPTIONAL function reference to call after change occurs
 	 */
	createJQUIAutocomplete:	
		function (jquiAutocompleteSetup)
		{
			$j('input#'+jquiAutocompleteSetup.inputFieldName).autocomplete(
					{
						source : jquiAutocompleteSetup.source,
						minLength : 3,
						select : function(event, ui) {
							$j("#"+jquiAutocompleteSetup.inputFieldId).val(ui.item ? ui.item.id : "0");
							if(jquiAutocompleteSetup.afterSelect) jquiAutocompleteSetup.afterSelect();
						},
						change: function (event, ui) { 
							$j("#"+jquiAutocompleteSetup.inputFieldId).val(ui.item ? ui.item.id : "0");
							if(jquiAutocompleteSetup.onChange) jquiAutocompleteSetup.onChange(); 
						}
					});
		},					

	/**
	 * creates an jquery tabs with the parameters given in the jquiTabsSetup object
	 * 
	 * @param {Object} jquiTabsSetup object with initialization parameter:
	 * idName - (name of the main tab div) 
 	 */
	createJQUITabs:	function(jquiTabsSetup) 
		{

		    $j( "#"+jquiTabsSetup.idName ).tabs();

		},
	
	/**
	 * Creates a company search plugin by loading all the necessary scripts and then
	 * calling the init method in CompanySearchPlugin.js.
	 */		
	createCompPlug:	function()
					{
						// load the company specific javascript files
						loadScript.loadScriptsDynamic(new Array('script/trescal/core/utilities/searchplugins/CompanySearchPlugin.js'), true,
						{
							callback: function()
							{
								// call the init function in CompanySearchPlugin.js to create search
								CompanySearchPlugin.init(false);
							}
						});
					},
	
	/**
	 * Creates a contact search plugin by loading all the necessary scripts and then
	 * calling the init method in ContactSearchPlugin.js.
	 */
	createContPlug:	function()
					{
						// load the contact specific javascript files
						loadScript.loadScriptsDynamic(new Array('script/trescal/core/utilities/searchplugins/ContactSearchPlugin.js'), true,
						{
							callback: function()
							{
								// call the init function in ContactSearchPlugin.js to create search
								ContactSearchPlugin.init(false);
							}
						});
					},
						
	/**
	 * Creates a multiple input contact search plugin by loading all the necessary scripts and then
	 * calling the init method in MultiContactSearchPlugin.js.
	 */
	createMultiContPlug:	function()
							{
								// load the multi contact specific javascript files
								loadScript.loadScriptsDynamic(new Array('script/trescal/core/utilities/searchplugins/MultiContactSearchPlugin.js'), true,
								{
									callback: function()
									{
										// call the init function in MultiContactSearchPlugin.js to create search
										MultiContactSearchPlugin.init();
									}
								});
							},
					
	/**
	 * Creates a company/subdiv/address/contact search plugin by loading all the necessary scripts and then
	 * calling the init method in CsacSearchPlugin.js.
	 */		
	createCascadePlug:	function()
						{						
							// load the cascade script
							loadScript.loadScriptsDynamic(new Array('script/trescal/core/utilities/searchplugins/CascadeSearchPlugin.js'), true,
							{
								callback: function()
								{
									// call the init function in CompanySearchPlugin.js to create search
									CascadeSearchPlugin.init();
								}
							});
						},
		
	/**
	 * Toggles the navigation menu visibility between true and false.  If false then a session variable
	 * is set and the navigation collapsed until the user clicks on the expand link.  If true then the session
	 * variable is set and the navigation expanded until the user clicks on the collapse link.
	 * 
	 * @param {String} navdisplay is the string value which indicates whether the menu should be collapsed or expanded
	 */		
	menuVisibility:	function(navdisplay)
					{
						// navigation should be collapsed
						if (navdisplay == 'collapse')
						{
							// collapse the navigation div
							$j('div#navigation').animate({opacity: 'hide', width: 0}, 1800);
							// collapse the maincontent div's left margin to width of page
							$j('div#maincontent').animate({marginLeft: 0.1}, 2200);
							// hide the collapse link and image
							$j('#collapseLink').removeClass('vis').addClass('hid');
							// show the expand link and image
							$j('#expandLink').removeClass('hid').addClass('vis');
							// call dwr service to set session variable to false
							// navigation will now be collapsed until the user clicks on the expand link
							dwrservice.setMenuVisibility(false);
						}
						else if (navdisplay == 'expand')
						{
							// expand maincontent div's left margin to 161px
							$j('div#maincontent').animate({marginLeft: 161}, 1800);
							// expand the navigation div
							$j('div#navigation').animate({opacity: 'show', width: 150}, 2200);
							// hide the expand link and image
							$j('#expandLink').removeClass('vis').addClass('hid');
							// show the collapse link and image
							$j('#collapseLink').removeClass('hid').addClass('vis');
							// call dwr service to set session variable to true
							// navigation will now be expanded until the user clicks on the collapse link
							dwrservice.setMenuVisibility(true);
						}
					},
	
	/**
	 * Loads javascript files passed from literals.  If any of the javascript files
	 * have already been loaded then they will be skipped.
	 * 
	 * @param {Array} Scripts is a string array of javascript file paths to be loaded
	 * @param {Object} func callback function
	 * 
	 * GB 2016-1-6 updated from $j.getScript to $j.ajax to ensure caching on
	 * TODO: determine whether loadScriptsDynamic can be a replacement?
	 * GB 2016-2-4 added async:false, changed cache 'true' to true, changed success to done
	 */	
	loadScripts:	function(Scripts, func)
					{
						// loop through each script to load
						$j.each( Scripts, function(i, n)
						{							
							// not loaded
							if ($j.inArray(n, loadedSCRIPTS) == -1)
							{
								// add to loaded script to array
								loadedSCRIPTS.push(n);
								// load
								$j.ajax({
									url: n,
									async: false,
									dataType: 'script',
									cache: true,
									done: function(){}
								});
								// last iteration of loop
								if (i == (Scripts.length - 1))
								{
									// call callback function if present
									if (func){func.callback();}
								}
							}
							else
							{
								// last iteration of loop
								if (i == (Scripts.length - 1))
								{
									// call callback function if present
									if (func){func.callback();}
								}
							}
						});					
					},
				
	/**
	 * Loads javascript files passed from literals.  If any of the javascript files
	 * have already been loaded then they will be skipped.
	 * 	
	 * @param {Array} Scripts is a string array of javascript file paths to be loaded
	 * @param {boolean} sync do we require syncronous loading of javascript file
	 * @param {func} func callback method provided
	 * 
	 * GB 2015-1-6 updated from $j.getScript to $j.ajax to ensure caching on
	 * TODO: determine why async loading is being used?
	 * GB 2016-2-4 changed cache 'true' to true, changed success to done
	 */	
	loadScriptsDynamic:	function(Scripts, sync, func)
						{
							// loop through each script to load
							$j.each( Scripts, function(i, n)
							{							
								// not loaded
								if ($j.inArray(n, loadedSCRIPTS) == -1)
								{
									console.log('loading: '+n);
									// add to loaded script to array
									loadedSCRIPTS.push(n);
									// load
									$j.ajax({
											url: n,
											dataType: 'script',
											type: 'GET',
											cache: true,
											async: !sync}) 
										.done(function(data, textStatus, jqXHR){
											console.log(textStatus);
											console.log('done: '+n);
											console.log("typeof data: "+typeof data);
											if (typeof data == 'string') {
												console.log("length: "+data.length);
											}
											// last iteration of loop
											if (i == (Scripts.length - 1))
											{
												// call callback function if present
												if (func){
													func.callback();
													console.log('ran callback');
												}
											}
										})
										.fail(function (jqXHR, textStatus, errorThrown) {
											console.log('error: '+n);
											console.log(textStatus);
									        console.log(errorThrown);
									        alert('error loading: '+n);
									    });
								}
								else
								{
									console.log('skipping: '+n);
									// last iteration of loop
									if (i == (Scripts.length - 1))
									{
										// call callback function if present
										if (func){
											func.callback();
											console.log('ran callback');
										}
									}
								}
							});
						},
	
	/**
	 * Loads stylesheet file passed.  If the stylesheet file
	 * has already been loaded then it will be skipped.
	 * 
	 * @param {String} href stylesheet file path to be loaded
	 * @param {String} media the media stylesheet should adhere to
	 * @param {Object} func callback function
	 */				
	loadStyle:		function(href, media, func)
					{
						// not loaded
						if ($j.inArray(href, loadedSTYLES) == -1)
						{
							// add to loaded styles to array
							loadedSTYLES.push(href);
							// load style into head of page
							$j('head').append('<link rel=\'stylesheet\' type=\'text/css\' href=\'' + href + '\' media=\'' + media + '\' />');
							// call callback function if present
							if (func){ func.callback(); }
						}
						else
						{
							// call callback function if present
							if (func){ func.callback(); }
						}
					},
					
	/**
	 * this method builds and returns the html content necessary to create an overlay thickbox
	 * 
	 * @return {String} content the html content for overlay
	 */
	buildOverlay:		function()
						{
							// create html content and add to variable
							var content = 	'<div class="overlay_wrapper">' +												
												'<div class="overlay_inner">' +														
													'<div class="overlay_bg_wrapper">' +
														'<div class="overlay_bg overlay_bg_n"></div>' +
														'<div class="overlay_bg overlay_bg_ne"></div>' +
														'<div class="overlay_bg overlay_bg_e"></div>' +
														'<div class="overlay_bg overlay_bg_se"></div>' +
														'<div class="overlay_bg overlay_bg_s"></div>' +
														'<div class="overlay_bg overlay_bg_sw"></div>' +
														'<div class="overlay_bg overlay_bg_w"></div>' +
														'<div class="overlay_bg overlay_bg_nw"></div>' +
													'</div>' +
													'<div class="overlay_contentholder">' +
														'<div class="overlay_title hid"></div>' +
														'<div class="overlay_content"></div>' +
													'</div>' +												
												'</div>' +
											'</div>';
							// return html content
							return content;
						},
	
	/**
	 * 
	 * @param title
	 * @param wrap
	 */
	addOverlayTitle:	function(title, wrap)
						{
							// has title been passed?
							if (title != null)
							{
								// check for existing title
								if ($j(wrap).hasClass('hid'))
								{
									// add title to wrapper
									$j(wrap).removeClass('hid').addClass('vis').html(title);
								}
								else
								{
									// change current title element
									$j(wrap).html(title);
								}
							}
							else
							{
								// remove any title
								$j(wrap).removeClass('vis').addClass('hid').empty();
							}
						},
	
	/**
	 * this method closes the current overlay instance
	 */
	closeOverlay:		function()
						{
							// get current overlay api
							api = $j('div.overlay_wrapper').overlay({data: 'overlay'});
							// close the overlay
							api.close();
						},
					
	/**
	 * this method allows the content of an overlay to be changed/modified as well as the height for
	 * larger content.
	 * 
	 * @param {String} href link to new external page if required
	 * @param {String} html new html content if required
	 * @param {String} title the title to be applied to overlay, if any? (null if no title)
	 * @param {Boolean} error indicates if this is an error message that should be prepended
	 * @param {Integer} height the height of overlay as a percentage
	 * @param {Integer} width the width of overlay set in pixels
	 * @param {String} focusElement an element id to apply focus to within overlay content
	 */
	modifyOverlay:		function(href, html, title, error, height, width, focusElement)
						{
							// get objects required
							var wrapper = $j('div.overlay_wrapper');
							var contentwrap = wrapper.find('div.overlay_contentholder > .overlay_content');
							var titlewrap = wrapper.find('div.overlay_contentholder > .overlay_title');
							// link not null?
							if (href != null)
							{								
								// empty content and apply new page
								contentwrap.empty().load(href, function()
								{
									loadScript.init(true);
								});							
				            	// title?
				            	loadScript.addOverlayTitle(title, titlewrap);
							}
							else if (html != null)
							{
								if (!error)
								{
									// empty content and apply new html
									contentwrap.empty().append(html);
									// title?
					            	loadScript.addOverlayTitle(title, titlewrap);
								}
								else
								{
									// check error box not already present
									if (contentwrap.find('div.warningBox1').length)
									{
										// if so remove it from overlay content
										contentwrap.find('div.warningBox1').remove();
									}
									// retrieve an existing overlay, prepend new html as it is error message
									contentwrap.prepend('<div class="warningBox1"><div class="warningBox2"><div class="warningBox3">' + html + '</div></div></div>');
								}
							}
							// new height to be applied
							var modifyHeight = '';
							// height not null? 
							if (height != null)
							{
								if (height.toString().indexOf('px') != -1)
								{
									// change height
									modifyHeight = height;
								}
								else
								{
									// change height
									modifyHeight = height + '%';
								}
							}
							else
							{
								modifyHeight = height + '%';
							}
							// new height requested?
							if (height != null && width != null)
							{								
								// modify height and width of overlay
								wrapper.animate( { height: modifyHeight, width: width + 'px' }, { queue: false, duration: 1000 } );	
							}
							else if (height != null)
							{
								// modify height of overlay
								wrapper.animate( { height: modifyHeight }, { queue: false, duration: 1000 } );
							}
							else if (width != null)
							{
								// modify width of overlay
								wrapper.animate( { width: width + 'px' }, { queue: false, duration: 1000 } );
							}
							// apply focus?
				            if (focusElement != '' && focusElement != null)
				            {
				               	// apply focus
				            	$j('#' + focusElement).focus();
				            }
						},
					
	/**
	 * this method creates an overlay thickbox.  Depending on the values passed the overlay content can come from an external
	 * url or it can be appended from html passed in the method parameters. The height and width of the overlay can also be set
	 * 
	 * @param {String} type the type of overlay (current values 'dynamic', 'external', 'inline')
	 * @param {String} title the title to be applied to overlay, if any? (null if no title)
	 * @param {String} href the url of external content
	 * @param {String} html content provided in the form of a html string
	 * @param {String} id id of an element which contains inline content
	 * @param {Integer} height the height of overlay as a percentage or you can define as pixels if postfix included (i.e. '50px')
	 * @param {Integer} width the width of overlay set in pixels	 
	 * @param {String} focusElement an element id to apply focus to within overlay content
	 */
	createOverlay:		function(type, title, href, html, id, height, width, focusElement)
						{
							// load required javascript if it has not already
							loadScript.loadScriptsDynamic(new Array ('script/thirdparty/jQuery/jquery.tools.overlay.js'), true,
							{
								callback: function()
								{
									// append div for overlay content
									$j('body').append(loadScript.buildOverlay());
									// get objects required
									var wrapper = $j('div.overlay_wrapper');
									var contentwrap = wrapper.find('div.overlay_contentholder > .overlay_content');
									var titlewrap = wrapper.find('div.overlay_contentholder > .overlay_title');
									// width not null?
									if (width != null)
									{
										// change width and height if necessary
										wrapper.width(width + 'px');
									}
									// height not null?
									if (height != null)
									{
										if (height.toString().indexOf('px') != -1)
										{
											// change height
											wrapper.height(height);
										}
										else
										{
											// change height
											wrapper.height(height + '%');
										}										
									}
									// dynamic content type?
									if (type == 'dynamic')
									{
										// initialise overlay using html content
										wrapper.overlay(
										{ 											
											top: '4%',
											left: 'center',
											speed: 0,
											mask: 
											{ 
										        color: '#666', 
										        loadSpeed: 400, 
										        opacity: 0.5 
										    },
											onBeforeLoad: function()
											{
									            // load the page specified
									            contentwrap.append(html);
									            // title?
									           	loadScript.addOverlayTitle(title, titlewrap);
									        },
											onClose: function()
											{
												// find and remove overlay and wrapper
		        								$j('body').find('div.overlay_wrapper').next().remove().end().remove();
		    								},
		    								target: 'div.overlay_wrapper',
		    								load: true	
										});								
									}
									else if (type == 'inline')
									{
										// initialise overlay using html content
										wrapper.overlay(
										{ 											
											top: '4%',
											left: 'center',
											speed: 0,
											mask: 
											{ 
										        color: '#666', 
										        loadSpeed: 400, 
										        opacity: 0.5 
										    },
											onBeforeLoad: function()
											{
									            // load the page specified
									            contentwrap.append($j('#' + id).children());
									            // remove the elements appended from page
									            $j('#' + id).empty();
									            // title?
									           	loadScript.addOverlayTitle(title, titlewrap);
									        },
											onClose: function()
											{
												// add elements in overlay back to page
												$j('#' + id).append(contentwrap.children());
												// find and remove overlay and wrapper
		        								$j('body').find('div.overlay_wrapper').next().remove().end().remove();
		    								},
		    								target: 'div.overlay_wrapper',
		    								load: true									        
										});	
									}
									else
									{
										// initialise overlay using external url content
										wrapper.overlay(
										{ 											
											top: '4%',
											left: 'center',
											speed: 0,
											mask: 
											{ 
										        color: '#666', 
										        loadSpeed: 400, 
										        opacity: 0.5
										    },
											onBeforeLoad: function()
											{
									            // add iframe after content wrapper and then remove
									            contentwrap.parent().css({overflow: 'hidden'}).end().after('<iframe id="iframe-id" src="' + href + '" width="100%" height="96.5%" marginwidth="0" marginheight="0" frameborder="0" border="0"></iframe>').remove();
									            // title?
									           	loadScript.addOverlayTitle(title, titlewrap);
									        },
											onClose: function()
											{ 
												// find and remove overlay and wrapper
		        								$j('body').find('div.overlay_wrapper').next().remove().end().remove();
		        								parent.location.reload(true);
		    								},
		    								target: 'div.overlay_wrapper',
		    								load: true	
										});									
									}
									// apply focus?
						            if (focusElement != '' && focusElement != null)
						            {
						               	// apply focus
						            	setTimeout(function(){ $j('#' + focusElement).focus(); }, 400);
						            }
								}
							});
						},
						
	/**
	 * this method populates the email table on any entity page
	 * 
	 * @param {Integer} emailId the id of the email to display
	 * @param {Integer} ccId the id of the current contact
	 * @param {Integer} sysCompId id of the system component
	 * @param {Integer} entityId id of the entity we are appending email to
	 */
	populateEntityEmail: 	function(emailId, ccId, sysCompId, entityId)
							{
								// load the service javascript file if not already
								loadScript.loadScriptsDynamic(new Array('dwr/interface/emailservice.js'), true,
								{
									callback: function()
									{
										emailservice.getAjaxCompleteEmail(emailId,
										{
											callback:function(result)
											{
												// retrieve email successful?
												if (result.success)
												{
													// assign email wrapper to variable
													var emailWrap = result.results;							
													// create new page content
													var content = 	'<tr id="email' + emailWrap.email.id + '">' +
																		'<td>';
																			$j.each(emailWrap.email.recipients, function(i, r)
																			{
																				if (r.type == 'EMAIL_TO')
																				{
																					content += '' + r.emailAddress + '<br/>';
																				}
																			});
															content += 	'</td>' +
																		'<td>';
																			$j.each(emailWrap.email.recipients, function(i, r)
																			{
																				if (r.type == 'EMAIL_CC')
																				{
																					content += '' + r.emailAddress + '<br/>';
																				}
																			});
															content += 	'</td>' +
																		'<td>' + emailWrap.email.subject + '</td>' +
																		'<td>' + cwms.dateTimeFormatFunction(emailWrap.email.sentOn) + '</td>' +
																		'<td>' + emailWrap.email.sentBy.name + '</td>' +
																		'<td class="center">' +
																			'<a href="#" id="email_anchor' + emailWrap.email.id + '" class="domthickbox" onclick=" getAuthentication(' + ccId + ', ' + emailWrap.email.sentBy.personid + ', ' + emailWrap.email.publish + ', ' + emailWrap.email.id + ', this.id); return false; ">' +
																				'<img src="img/icons/email_open.png" width="16" height="16" alt="' + i18n.t("core.template:viewEmailDetails", "View Email Details") + '" title="' + i18n.t("core.template:View Email Details", "View Email Details") + '" />' +
																			'</a>' +								
																		'</td>' +
																		'<td class="resend">' +
																			'<a href="#" onclick=" loadScript.createOverlay(\'external\', \'Re-send Email\', \'createemail.htm?syscompid=' + sysCompId + '&objid=' + entityId + '&emailid=' + emailWrap.email.id + '\', null, null, 74, 860, null); return false; " class="float-right padright20">' +
																				'<img src="img/icons/email_go.png" width="16" height="16" alt="' + i18n.t("core.template:reSendEmail", "Re-send Email") + '" title="' + i18n.t("core.template:reSendEmail", "Re-send Email") + '" class="img_marg_bot" />' +
																			'</a>' +								
																		'</td>' +
																	'</tr>';
													// is this the first email to be added?
													if ($j('div#entityEmailDisplay table tbody tr:first td').length < 2)
													{
														// remove the first table row
														$j('div#entityEmailDisplay table tbody tr:first').remove();
													}
													// append email to page
													$j('div#entityEmailDisplay table tbody').append(content);
													// update email counts on page
													$j('span.entityEmailDisplaySize').text(parseInt($j('span.entityEmailDisplaySize:first').text()) + 1);
												}
												else
												{
													// show user the message
													$j.prompt(result.message);
												}
											}
										});	
									}
								});
							},
	
	/**
	 * this method adds spell check functionality to the element passed
	 * 
	 * @param {Object} element the element we wish to add spell checking to
	 */
	addSpellCheck:	function(element)
					{
						// error element
						var errorElement = false;
						// is there an error element after textarea
						if ($j(element).next().hasClass('attention'))
						{
							errorElement = true;						
						}
						// add spell check functionality and modify html
						$j(element).css({width: '100%'})									
									.wrap('<div class="float-left width70"></div>').parent()
									.prepend('<a href="#" class="float-right" onclick=" loadScript.spellCheck($j(this).next(), null); return false; ">' +
												'<img src="img/icons/spellcheck.png" width="16" height="16" alt="' + i18n.t("core.template:checkSpelling", "Check Spelling") + '" title="' + i18n.t("core.template:checkSpelling", "Check Spelling") + '" />' +
											 '</a>')									
									.after('<div class="clear-0"></div>');
						// is there an error element to move?
						if (errorElement)
						{
							// move the error element
							$j(element).parent().parent().find('.attention').appendTo($j(element).parent());
						}
					},
			
	/**
	 * this method spell checks the two elements passed using ie spell so this is only
	 * valid for internet explorer running on windows
	 * 
	 * @param {Object} element1 the first element to be spell checked
	 * @param {Object} element2 the second element to be spell checked
	 */
	spellCheck: function(element1, element2)
				{
					var node1 = null;
					var node2 = null;
					if (element1 != null)
					{
						node1 = $j(element1).get(0);
					}
					if (element2 != null)
					{
						node2 = $j(element2).get(0);
					}					
					var myAgent = navigator.userAgent.toLowerCase();
					var is_win = ((myAgent.indexOf("win") != -1) 
			           || (myAgent.indexOf("16bit") != -1));			
					try 
					{
					    var tmpis = new ActiveXObject("ieSpell.ieSpellExtension");
					    if (node1 != null && node2 != null)
					    {
					    	tmpis.CheckDocumentNode2(node1, true);
					    	tmpis.CheckDocumentNode2(node2, false);
					    }
					    else
					    {
					    	tmpis.CheckDocumentNode2(node1, false);
					    }
					}
					catch(exception)
					{
						if ($j.browser.msie && is_win)
						{
							$j.prompt(i18n.t("core.template:spellCheckPlugin", "Please contact your system administrator and request the installation of this spell check plugin"));
					    }
					}					
				},
	
	/**
	 * this method restricts the user from entering spaces in the text element passed
	 * 
	 * @param {Object} element the element in which we want to restrict the addition of spaces
	 */	
	restrictSpaces: function(element)
					{
						// load the javascript file if not already
						loadScript.loadScriptsDynamic(new Array('script/thirdparty/jQuery/jquery.textchange.js'), true,
						{
							callback: function()
							{
								// add text change event to element passed
								$j(element).bind('textchange', function()
								{
									// add value of element to variable
									var value = $j(this).val();
									// set boolean value so multiple user prompts are not opened
									//var userPrompted = false;
									// element value contains space and user has not already been prompted
									if ((value.indexOf(' ') != -1) && (!userPrompted))
									{
										// set user being prompted
										userPrompted = true;
										// prompt the user that spaces are not allowed here
										$j.prompt(i18n.t("core.template:spacesNotAllowedInField", "<p>Spaces are restricted in this field</p>" + "<p>Click 'OK' or press 'Enter' to continue</p>"),
										{ 
											submit: confirmcallback,
											close : confirmcallback,
											buttons: 
											{ 
												OK: true
											}, 
											focus: 0
										});	
										
										// callback method for initial prompt
										function confirmcallback(v,m)
										{
											// user confirmed action?
									    	if (v)
											{
									    		// replace all spaces added by user and set into new variable
												var newvalue = value.replace(/ /g, '');
												// set new value into element and apply focus
												$j(element).val(newvalue).focus();
												// set user prompted
												userPrompted = false;
												// trigger keyup event to reset values in plugin (i.e. the text change plugin used saves the previous
												// text entered so this still has the space in it. By triggering the keyup event manually these values are reset.
												$j(element).trigger('keyup');					
											}
										}
									}
								});
							}
						});
					},
					
	/**
	 * this method allows to internationalize months and days
	 */
	i18nDate:	function()
	{
		// load the javascript file if not already
		loadScript.loadScriptsDynamic(new Array('script/trescal/core/utilities/Date.js'), true,
		{
			callback: function()
			{
				// call the i18nDate function in Date.js to init i18n of months and days
				i18nDate();
			}
		});
	},
	
	/**
	 * function called from onload attribute of body tag.  Changes the selected subnavigation tab depending on
	 * the value held in LOADTAB and checks for an array of elements to apply nifty corners to.
	 */		
	init:  	function(perform)
			{
				if (perform == true)
				{
					// Internationalization init
					i18n.init({
				        lng: i18locale,
				        getAsync: false,
				        ns: { namespaces: ['calibration', 'common', 'core.admin', 'core.asset', 'core.company', 'core.contract', 'core.deliverynote', 'core.document', 'core.equation', 'core.hire',
				                           'core.instrument', 'core.instrumentmodel', 'core.jobs', 'core.quotation', 'core.pricing', 'core.procedure', 'core.recall', 
				                           'core.schedule', 'core.system', 'core.template', 'core.tools', 'core.tpcosts', 'core.utilities', 'core.workflow'], defaultNs: 'common'},
				        useLocalStorage: false,
				        useDataAttrOptions: true,
				        resGetPath: 'locales/__lng__/__ns__.json',
				        debug: true,				//Debug output
				        fallbackLng: 'en',			//Set fallback language
				        fallbackOnNull: true,		//Handling of null values
				        fallbackOnEmpty: true,		//Handling of empty string values
				        fallbackToDefaultNS: true,	//Set fallback namespace
				        shortcutFunction: 'defaultValue',
				        cache: true					// Prevent jquery.ajax from adding timestamp
				    	});
					
					$j('#allocatedSubdivForm').append("<input type=hidden name='currentUrl' value=" + window.location.href + "></input>");
					
					$j("#allocatedSubdivid").change(function() {
						$j("#allocatedSubdivForm").submit();
					});
					
					// dwr.util.useLoadingMessage();
					
					// internationalisation of date (months and days)
					loadScript.i18nDate();
					

                    // Using gallerific?  If so, load the image gallery javascript and initialise it
					if ($j('div.gallerifficContent').length) {
						// load the stylesheet for galleriffic image viewer
						loadScript.loadStyle('styles/jQuery/jquery-galleriffic.css', 'screen');
						loadScript.loadScriptsDynamic(new Array(
							'script/trescal/core/documents/ImageGalleryFunctions.js',
							'script/thirdparty/jQuery/jquery.galleriffic.js'), true,
                            {
                                callback: function()
                                {
                                	initialiseGalleriffic();
                                }
                            });
                    }
					// element with this id present in page? If so, create company search plugin
					if ($j('div#compSearchPlugin').length)
					{	
						// create the company search plugin
						loadScript.createCompPlug();				
					}
					// element with this id present in page? If so, create contact search plugin
					if ($j('div#contSearchPlugin').length)
					{
						// create the contact search plugin
						loadScript.createContPlug();
					}
					// element with this id present in page? If so, create multi contact search plugin
					if ($j('div#multiContSearchPlugin').length)
					{
						// create the contact search plugin
						loadScript.createMultiContPlug();
					}
					// element with this id present in page? If so, create comp/subdiv/addr/cont plugin
					if ($j('div#cascadeSearchPlugin').length)
					{
						// create the comp/subdiv/cont/addr search plugin
						loadScript.createCascadePlug();
					}				
					// textarea with class name of presetComments present on page
					if ($j('textarea.presetComments').length > 0)
					{
						// load javascript file for creating preset comments
						loadScript.loadScriptsDynamic(new Array('script/trescal/core/utilities/plugins/PresetCommentJQPlugin.js'), true,
						{
							callback: function()
							{
								// loop through each textarea with class of presetComments
								$j('textarea.presetComments').each(function()
								{					
									if ($j(this).attr('class').indexOf('symbolBox') != -1)
									{
										// initialise plugin with symbol box
										$j(this).presetCommentJQPlugin({ symbolBox: true });	
									}
									else
									{
										// initialise plugin with symbol box
										$j(this).presetCommentJQPlugin({ symbolBox: false });
									}																													
								});
							}
						});
					}
					// this loads the javascript for any symbol boxes that may be created in the preset comments plugin also
					// textarea with class name of symbolbox present on page
					if ($j('textarea.symbolBox').length > 0)
					{
						// load javascript file for creating symbol box
						loadScript.loadScriptsDynamic(new Array('script/trescal/core/utilities/plugins/SymbolJQPlugin.js'), true,
						{
							callback: function()
							{
								// loop through each textarea with class of symbolBox
								$j('textarea.symbolBox').each(function()
								{
									if ($j(this).attr('class').indexOf('presetComments') < 0)
									{
										// initialise plugin
										$j(this).symbolJQPlugin();	
									}																													
								});
							}
						});
					}
									
					if ($j('div.compSearchJQPlugin').length > 0)
					{
						// javascript file for creating company search plugin
						loadScript.loadScriptsDynamic(new Array('script/trescal/core/utilities/searchplugins/jquery/CompanySearchJQPlugin.js'), true,
						{
							callback: function()
							{
								$j('div.compSearchJQPlugin').each(function(i, n)
								{
									if (!$j(this).find('> div').length) 
									{
										$j(this).compSearchJQPlugin({	field: $j(this).find('> input[name="field"]').val(),
																		tabIndex: $j(this).find('> input[name="tabIndex"]').val(),
																		copyOption: $j(this).find('> input[name="copyOption"]').val(),
																		compNamePrefill: $j(this).find('> input[name="compNamePrefill"]').val(),
																		compIdPrefill: $j(this).find('> input[name="compIdPrefill"]').val(),
																		compCoroles: $j(this).find('> input[name="compCoroles"]').val(),
																		deactivated: $j(this).find('> input[name="deactivated"]').val(),
																		triggerMethod: $j(this).find('> input[name="triggerMethod"]').val()});				
									}															
								});
							}
						});
					}
					if ($j('div.mfrSearchJQPlugin').length > 0)
					{
						// javascript file for creating mfr search plugin
						loadScript.loadScriptsDynamic(new Array('script/trescal/core/utilities/searchplugins/jquery/MfrSearchJQPlugin.js'), true,
						{
							callback: function()
							{
								$j('div.mfrSearchJQPlugin').each(function(i, n)
								{
									if (!$j(this).find('> div').length) 
									{
										$j(this).mfrSearchJQPlugin({fieldId: $j(this).find('> input[name="fieldId"]').val(),
																	fieldName: $j(this).find('> input[name="fieldName"]').val(),
																	textOnlyOption: $j(this).find('> input[name="textOnlyOption"]').val(),
																	tabIndex: $j(this).find('> input[name="tabIndex"]').val(),
																	copyOption: $j(this).find('> input[name="copyOption"]').val(),
																	mfrNamePrefill: $j(this).find('> input[name="mfrNamePrefill"]').val(),
																	mfrIdPrefill: $j(this).find('> input[name="mfrIdPrefill"]').val(),
																	zIndex: $j(this).find('> input[name="zIndex"]').val() });
									}															
								});
							}
						});
					}
					if ($j('div.compDescSearchJQPlugin').length > 0)
					{
						// javascript file for creating component description search plugin
						loadScript.loadScriptsDynamic(new Array('script/trescal/core/utilities/searchplugins/jquery/CompDescriptionSearchJQPlugin.js'), true,
						{
							callback: function()
							{
								$j('div.compDescSearchJQPlugin').each(function(i, n)
								{
									if (!$j(this).find('> div').length) 
									{
										$j(this).compDescSearchJQPlugin({fieldId: $j(this).find('> input[name="fieldId"]').val(),
																		fieldName: $j(this).find('> input[name="fieldName"]').val(),
																		textOnlyOption: $j(this).find('> input[name="textOnlyOption"]').val(),
																		tabIndex: $j(this).find('> input[name="tabIndex"]').val(),
																		compDescNamePrefill: $j(this).find('> input[name="compDescNamePrefill"]').val(),
																		compDescIdPrefill: $j(this).find('> input[name="compDescIdPrefill"]').val()});
									}															
								});
							}
						});
					}
					if ($j('div.threadSearchJQPlugin').length > 0)
					{
						// javascript file for creating procedure search plugin
						loadScript.loadScriptsDynamic(new Array('script/trescal/core/utilities/searchplugins/jquery/ThreadSearchJQPlugin.js'), true,
						{
							callback: function()
							{
								$j('div.threadSearchJQPlugin').each(function(i, n)
								{
									if (!$j(this).find('> div').length) 
									{										
										$j(this).threadSearchJQPlugin({field: $j(this).find('> input[name="field"]').val(),
																		selectfield: $j(this).find('> input[name="selectfield"]').val(),
																		customFunc: $j(this).find('> input[name="customFunc"]').val(),
																		copyOption: $j(this).find('> input[name="copyOption"]').val(),
																		threadTypeId: $j(this).find('> input[name="threadTypeId"]').val(),																		
																		defaultThreadId: $j(this).find('> input[name="defaultThreadId"]').val(),
																		defaultThreadText: $j(this).find('> input[name="defaultThreadText"]').val()});	
									}
								});
							}
						});
					}
					if ($j('div.workinstSearchJQPlugin').length > 0)
					{
						// javascript file for creating procedure search plugin
						loadScript.loadScriptsDynamic(new Array('script/trescal/core/utilities/searchplugins/jquery/WorkInstSearchJQPlugin.js'), true,
						{
							callback: function()
							{
								$j('div.workinstSearchJQPlugin').each(function(i, n)
								{
									if (!$j(this).find('> div').length) 
									{
										$j(this).workinstSearchJQPlugin({field: $j(this).find('> input[name="field"]').val(),
																			copyOption: $j(this).find('> input[name="copyOption"]').val(),
																			copyParent: $j(this).find('> input[name="copyParent"]').val(),
																			customFunc: $j(this).find('> input[name="customFunc"]').val(),
																			defaultWiApproved: $j(this).find('> input[name="defaultWiApproved"]').val(),
																			defaultWiId: $j(this).find('> input[name="defaultWiId"]').val(),
																			defaultWiText: $j(this).find('> input[name="defaultWiText"]').val()});
									}									
								});
							}
						});
					}
					if ($j('div.certTemplateSearchJQPlugin').length > 0)
					{
						// javascript file for creating certificate template search plugin
						loadScript.loadScriptsDynamic(new Array('script/trescal/core/utilities/searchplugins/jquery/CertTemplateSearchJQPlugin.js'), true,
						{
							callback: function()
							{
								$j('div.certTemplateSearchJQPlugin').each(function(i, n)
								{
									if (!$j(this).find('> div').length) 
									{
										$j(this).certTemplateSearchJQPlugin({fieldId: $j(this).find('> input[name="fieldId"]').val(),
																				fieldName: $j(this).find('> input[name="fieldName"]').val(),
																				tabIndex: $j(this).find('> input[name="tabIndex"]').val(),
																				copyOption: $j(this).find('> input[name="copyOption"]').val(),
																				certTempNamePrefill: $j(this).find('> input[name="certTempNamePrefill"]').val(),
																				certTempIdPrefill: $j(this).find('> input[name="certTempIdPrefill"]').val(),
																				zIndex: $j(this).find('> input[name="zIndex"]').val() });
									}															
								});
							}
						});
					}
					if ($j('div.poSearchJQPlugin').length > 0)
					{
						// javascript file for creating po search plugin
						loadScript.loadScriptsDynamic(new Array('script/trescal/core/utilities/searchplugins/jquery/POSearchJQPlugin.js'), true,
						{
							callback: function()
							{
								$j('div.poSearchJQPlugin').each(function(i, n)
								{
									if (!$j(this).find('> div').length) 
									{
										$j(this).poSearchJQPlugin({field: $j(this).find('> input[name="field"]').val(), 
																defaultpo: $j(this).find('> input[name="defaultPO"]').val(),
																jobid: $j(this).find('> input[name="jobid"]').val(),
																copyOption: $j(this).find('> input[name="copyOption"]').val()});
									}									
								});
							}
						});
					}
					// sub navigation exists?
					if ($j('#subnav').length || $j('.subnavtab').length)
					{
						// javascript file for changing submenu navigation
						loadScript.loadScriptsDynamic(new Array('script/trescal/core/utilities/Menu.js'), true,
						{
							callback: function()
							{
								// LOADTAB contains data?
								if (LOADTAB)
								{
									// switch the focus to sub navigation tab held in LOADTAB
									switchMenuFocus(menuElements, LOADTAB, false);
									// function to be called after tab has loaded
									// method present?
									if (typeof loadtabFunc != 'undefined')
									{
										// call method
										loadtabFunc();
									}
								}
							}
						});
					}
					// niftyElements variable exists?
					if ( typeof niftyElements != 'undefined')
					{
						// apply nifty corners to elements
						loadScript.loadNifty(niftyElements);
					}
					// searchAgain variable exists?
					if ( typeof searchAgain != 'undefined')
					{
						// create search again img/link
						$j('span.headsearch').append('<a href="' + searchAgain + '" class="imagelink"><img src="img/icons/search.png" width="16" height="16" alt="' + i18n.t("searchAgain", "Search Again") + '" title="' + i18n.t("searchAgain", "Search Again") + '" /></a>');
					}
					// jCalendar object exists?
					if ( typeof jqCalendar != 'undefined')
					{
						// load stylesheet using the dom
						loadScript.loadStyle('styles/jQuery/jquery-ui.css', 'screen');
						// load the jquery calendar javascript file
						loadScript.loadScriptsDynamic(new Array('script/thirdparty/jQuery/jquery.ui.js'), true,
						{
							callback: function()
							{
								$j.each(jqCalendar, function(i)
								{
									// create jquery calendars
									loadScript.createJQCal(jqCalendar[i]);
								});
							}
						});	
					}
					if ($j('div#tabs').length)
					{
						// load datepicker stylesheet using the dom if not already 
						loadScript.loadStyle('styles/jQuery/jquery-ui.css', 'screen');
						// load the datepicker javascript file if not already
						loadScript.loadScriptsDynamic(new Array('script/thirdparty/jQuery/jquery.ui.js'), true,
						{
							callback: function()
							{
								$j('#tabs ul').tabs();
							}
						});
					}					
					if ($j('textarea.editor').length > 0)
					{
						// Note: Removed jWSIWYG editor 2016-05-07 because of incompatibility with "clearfix"
						// instructure.css.  Replaced with the smaller jHtmlArea editor.
						// trumowyg editor was flaky on enter events with IE 11.
						loadScript.loadStyle('script/thirdparty/jQuery/jHtmlArea/jHtmlArea.css', 'screen');
						loadScript.loadScriptsDynamic(new Array('script/thirdparty/jQuery/jHtmlArea/jHtmlArea-0.8.js'), true,
						{
							callback: function()
							{
								$j('textarea.editor').htmlarea();
							}
						});
					}
					if ($j('.addSpellCheck').length > 0)
					{
						$j('.addSpellCheck').each(function(i)
						{
							loadScript.addSpellCheck($j(this));
						});
					}
					if ($j('input.colorpicker').length > 0)
					{
						// load stylesheet using the dom
						loadScript.loadStyle('styles/jQuery/jquery-colorpicker.css', 'screen');
						// javascript file for creating wysiwyg editor
						loadScript.loadScriptsDynamic(new Array('script/thirdparty/jQuery/jquery.colorpicker.js'), true,
						{
							callback: function()
							{
								$j('input.colorpicker').attachColorPicker();
							}
						});
					}				
					// jCalendar or jquiAutocomplete objects exists - both initialized with jQueryUI
					if ( (typeof jqCalendar != 'undefined') || (typeof jquiAutocomplete != 'undefined') || (typeof jquiTabs != 'undefined') )
					{
						// load stylesheet using the dom
						loadScript.loadStyle('styles/jQuery/jquery-ui.css', 'screen');
						// load the jquery UI javascript file
						loadScript.loadScriptsDynamic(new Array('script/thirdparty/jQuery/jquery.ui.js'), false,
						{
							callback: function()
							{
								if ( typeof jqCalendar != 'undefined' )
								{
									$j.each(jqCalendar, function(i){
										// create jquery calendars
										loadScript.createJQCal(jqCalendar[i]);
									});
								}
								if ( typeof jquiAutocomplete != 'undefined' )
								{
									$j.each(jquiAutocomplete, function(i){
										// create jquery autocompletes
										loadScript.createJQUIAutocomplete(jquiAutocomplete[i]);
									});
								}
								if ( typeof jquiTabs != 'undefined' )
								{
									$j.each(jquiTabs, function(i){
										// create jquery autocompletes
										loadScript.createJQUITabs(jquiTabs[i]);
									});
								}
							}
						});	
					}					
					// focusElement variable exists?
					if ( typeof focusElement != 'undefined')
					{
						if ($j('#' + focusElement.toString()))
						{
							// call object literal to focus on element
							loadScript.focusOn(focusElement.toString());
						}
					}
					// element with this id present in page? 
					// If so, check if css display property set to 'none'
					if ($j('#navigation').length)
					{
						// navigation has been collapsed so expand image needs to be
						// made the visible one.
						if($j('#navigation').css('display') == 'none')
						{
							// hide collapse link in header
							$j('#collapseLink').removeClass().addClass('hid');
							// show expand link in header
							$j('#expandLink').removeClass().addClass('vis');
						}				
					}				
					// element with this id present in page?
					// if so, load thickbox js file and css file
					if ($j('.domthickbox, .thickbox').length)
					{
						// load stylesheet using the dom
						loadScript.loadStyle('styles/jQuery/jquery-thickbox.css', 'screen');
						// load the javascript file if not already
						loadScript.loadScriptsDynamic(new Array ('script/thirdparty/jQuery/jquery.thickbox.js'), true);
					}
					// anchor with class name of jTip present in page?
					// If so, load jTip js file and css file 
					if (($j('a.jTip').length || $j('a.jconTip').length) && userPrefs.tooltips)
					{
						// load stylesheet using the dom
						loadScript.loadStyle('styles/jQuery/jquery-jtip.css', 'screen');
						// load the javascript file if not already
						loadScript.loadScriptsDynamic(new Array ('script/thirdparty/jQuery/jquery.jtip.js'), true,
						{
							callback: function()
							{
								JT_init();
							}
						});
					}
					// table tbody with id of keynavResults present in page?
					// If so, load SearchResultsKeyNav js file
					if ($j('tbody#keynavResults').length)
					{
						// load the javascript file if not already
						loadScript.loadScriptsDynamic(new Array ('script/trescal/core/utilities/keynavigation/SearchResultsKeyNav.js'), true);
					}
					// image with class of clipboard present in page?
					// If so, load DOMUtils javascript
					if ($j('img.clipboard').length)
					{
						// load the javascript file if not already
						loadScript.loadScriptsDynamic(new Array('script/trescal/core/utilities/DOMUtils.js'), true);
					}			
					if (typeof init != 'undefined')
					{						
						init();
					}
				}
				else
				{
					if (typeof init != 'undefined')
					{						
						init();
					}
				}				
			}
};

function logout() {
	$j("#logout").submit();
}

/**
 * loads all javascript files in the mainImports array if we are not qunit testing
 * 
 * @param {Array} mainImports is a string array of javascript file paths to be loaded.
 */
if (typeof QUnitTesting == 'undefined')
{
	loadScript.loadScriptImports(mainImports);	
}


/**
 * if pageImports is defined then load all javascript files in the array.
 */
if ( typeof pageImports != 'undefined')
{
	loadScript.loadScriptImports(pageImports);
}

/**
 * function called when presetComment is selected.
 */
function presetCommentSelected(select)
{
	console.log(select);
	$j('#label').val(select.find(':selected').data('label'));
	var note = select.val();
	$j('#noteBody').htmlarea('pasteHTML',note.replace(/\n/g,"</br>"));
}
