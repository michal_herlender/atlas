/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: Jamie Vinall
*
*	FILENAME		:	ScheduledDeliveries.js
*	DESCRIPTION		:	This javascript file is used on the createschedule.vm page.
*
*	HISTORY			:	30/12/2020 - Laïla MADANI - Created File
****************************************************************************************************************************************/



function init()
{
	for (i = 0; i < delievriesCount; i++) {
		// show the client receipt date input, in the case we have the validation error in this input
		var completeCheckbox = document.getElementById('dto'+i+'.scheduledDeliveryStatus2');
		if(completeCheckbox && completeCheckbox.value == 'COMPLETE' && completeCheckbox.checked == true){		
			$j('input[name="dto['+i+'].clientReceiptDate"]').addClass('vis').removeClass('hid');
		}
		// checked pending checkbox, in the case that we have a validation error in rescheduled_manual input
		var rescheduledCheckbox = document.getElementById('dto'+i+'.scheduledDeliveryStatus4');
		var pendingCheckbox = document.getElementById('dto'+i+'.scheduledDeliveryStatus1');
		if(rescheduledCheckbox && rescheduledCheckbox.value == 'RESCHEDULED_MANUAL' && rescheduledCheckbox.checked == true){
			pendingCheckbox.checked = true;
		}
	}
	
}

function showDateEntry(name) {
	$j('input[name="'+name+'"]').addClass('vis').removeClass('hid');
}

function hidDateEntry(name) {
	$j('input[name="'+name+'"]').addClass('hid').removeClass('vis');
}

function showScheduleRulesData(id){
	document.querySelector('#'+ id +' > td > cwms-schedule-rules').refresh();
	$j('#'+ id).addClass('vis').removeClass('hid');
	document.querySelector('#'+ id +' > td > cwms-schedule-rules').clean();
}

function hidScheduleRulesData(id) {
	$j('#'+ id).addClass('hid').removeClass('vis');
	document.querySelector('#'+ id +' > td > cwms-schedule-rules').clean();
}


