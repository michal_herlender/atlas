/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: Jamie Vinall
*
*	FILENAME		:	CreateSchedule.js
*	DESCRIPTION		:	This javascript file is used on the createschedule.vm page.
*					: 	
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	02/08/2007 - JV - Created File
*						16/12/2020 - LM - Updated File
****************************************************************************************************************************************/

/**
 * array of calendar objects containing the initial setup literals needed
 * 
 * @param inputField (String) id of the input field to which the calendar should be binded
 * @param dateFormat (String) format that the date should be displayed
 * @param displayDates (String) 'all' make all dates available
 * 								'future' show only dates in the future
 * 								'past' show only dates in the past
 * applied in init() function.
 */	
					 
/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'coname';

function init()
{
	if($j('#entryDate')[0].checked == true){
		$j('#scheduleDate').addClass('vis').removeClass('hid');
	}
	
}

/**
 * this function changes the corole value stored in the 'compCoroles' hidden input field
 * when changed the text field and search list are emptied ready for a search using new corole
 */
function changeCorole(value)
{
	// clear the input field
	$j('#coname').attr('value', '').focus();
	// clear the results list
	$j('#compPlugResults').empty();
	$j('#subdivPlugResults').empty();
	$j('#contPlugResults').empty();
	$j('#addrPlugResults').empty();
	// change corole in hidden input
	$j('#compCoroles').attr('value', value);
	// clear coid hidden input field
	$j('#coid').attr('value', '');
}

function getScheduleDetails() {
	timeout = setTimeout(function() {
		$j("#transportOptionIn").removeClass('vis').addClass('hid');
		$j("#transportOptionOut").removeClass('vis').addClass('hid');
		$j("#schedulesCreated").removeClass('vis').addClass('hid');
		$j('#transportOptionId option[value=""]').attr('selected','selected');
		if(document.getElementById('addrPlugResults')){
			if(document.getElementById('addrPlugResults').children[0] && document.getElementById('addrPlugResults').children[0].tagName === 'A'){
				var addrid = document.getElementById('addrPlugResults').children[0].attributes[1].value;
				$j.ajax({
					url: "getScheduleResult.json",
					data: {
						addrid: addrid
					},
					async: true
				}).done(function(result) {
					$j("#schedulesTable tbody tr").remove();
					if( result.transportOptionInId){
						$j("#transportOptionIn").removeClass('hid').addClass('vis');
						$j("#transportOptionInName").text(result.transportOptionInName);
					} else {
						$j("#transportOptionIn").removeClass('vis').addClass('hid');
					}
					
					if( result.transportOptionOutId) {
						$j("#transportOptionOut").removeClass('hid').addClass('vis');
						$j("#transportOptionOutName").text(result.transportOptionOutName);
					} else {
						$j("#transportOptionOut").removeClass('vis').addClass('hid');
					}
					
					if(result.transportOptionOutId && result.transportOptionInId && result.transportOptionInId === result.transportOptionOutId){
						$j('#transportOptionId option[value='+ result.transportOptionInId +']').attr('selected','selected');
					}
					
					if(result.schedulesCreatedForAddressMap) {
						for (const [key, value] of Object.entries(result.schedulesCreatedForAddressMap)) {
						    const html = '<tr><td><a href=viewschedule.htm?schid='+ key +'>'+ result.schedulesCreatedForAddressMap[key] +'</a></td></tr>';
						    $j("#schedulesTable tbody").append(html);
					   }
						$j("#schedulesCreated").removeClass('hid').addClass('vis');
					}
											
			});		
		}
	}
}, 500);

}

function showDateEntry() {
	$j('#scheduleDate').addClass('vis').removeClass('hid');
}

function hideDateEntry() {
	$j('#scheduleDate').addClass('hid').removeClass('vis');
}
