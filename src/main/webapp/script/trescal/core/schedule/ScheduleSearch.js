/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: Jamie Vinall
*
*	FILENAME		:	ScheduleSearch.js
*	DESCRIPTION		:	This javascript file is used on the schedulesearch.vm page.
*					: 	
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	09/08/2007 - JV - Created File
*					:	22/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{
	// delay the focus on cascading search plugin company text input
	setTimeout("$j('input[name=comptext]').focus()", 1400);
}

/**
 * array of calendar objects containing the initial setup literals needed
 * 
 * @param inputField {String} id of the input field to which the calendar should be binded
 * @param dateFormat {String} format that the date should be displayed
 * @param displayDates {String} 'all' make all dates available
 * 								'future' show only dates in the future
 * 								'past' show only dates in the past
 * Applied in init() function.
 */								
//var jqCalendar = new Array ( { 
//							 inputField: 'dateSingle',
//							 dateFormat: 'dd.mm.yy',
//							 displayDates: 'all'
//							 },
//							 {
//							 inputField: 'dateBegin',
//							 dateFormat: 'dd.mm.yy',
//							 displayDates: 'all'
//							 },
//							 {
//							 inputField: 'dateEnd',
//							 dateFormat: 'dd.mm.yy',
//							 displayDates: 'all' 
//							 } );							

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );
	
/**
 * this function changes the corole value stored in the 'compCoroles' hidden input field
 * when changed the text field and search list are emptied ready for a search using new corole
 */
function changeCorole(value)
{
	// clear company search ready for new corole
	$j('div.compSearchJQPlugin').after(	'<div class="compSearchJQPlugin"></div>').remove();
	// change corole in hidden input
	$j('input[name="compCoroles"]').val(value);
	// initialise company search again
	$j('div.compSearchJQPlugin').compSearchJQPlugin({	field: 'coid',
														tabIndex: 6,
														compCoroles: value});
	// add focus to input
	$j('input[name="comptext"]').focus();
}

/**
 * this function toggles the options available when the user is choosing which dates to search over
 * 
 * @param {String} type is the datetype chosen by the user, this is used to locate the list item to display
 */
function toggleDateFields(type)
{
	var types = new Array('0', '1', '2');
	
	$j.each(types, function(i)
	{
		if (types[i] == type)
		{
			$j('#datetype' + types[i]).removeClass().addClass('vis');
		}
		else
		{
			$j('#datetype' + types[i]).removeClass().addClass('hid');	
		}
	});
}

/**
 * this function checks that a date(s) has been specified if the user has chosen to search over a single
 * date or a date range
 */
function checkDateFields()
{
	if ($j('#datetype').val() == '1' && $j('#dateSingle').val() == '')
	{
		$j.prompt(i18n.t("core.schedule:infoSearchUsingSingleDate", "When searching using a single date this must be supplied before searching"));
		$j('#datetype').focus();
		return false;
	}
	else if($j('#datetype').val() == '2')
	{
		if($j('#dateBegin').val() == '' || $j('#dateEnd').val() == '')
		{
			$j.prompt(i18n.t("core.schedule:infoSearchUsingRangeDate", "When searching using a date range these must be supplied before searching'"));
			return false;
		}
	}
	
	return true;
}