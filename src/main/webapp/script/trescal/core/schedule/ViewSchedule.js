/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: Jamie Vinall
*
*	FILENAME		:	ViewSchedule.js
*	DESCRIPTION		:	This javascript file is used on the viewschedule.vm page.
*					: 	
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	06/08/2007 - JV - Created File
*					:	17/10/2007 - SH - JQuery functions and comment
*					:	22/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * Array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'script/trescal/core/utilities/Note.js',
								'script/trescal/core/deliverynote/ViewDIUtility.js' );
								
/**
 * Array of objects containing the element name and corner type which should be applied.
 * Applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' },
								{ element: 'ul.subnavtab a', corner: 'small transparent top' } );
								
/**
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = 	new Array(	{ anchor: 'deliveries-link', block: 'deliveries-tab' },
								{ anchor: 'schedequip-link', block: 'schedequip-tab' },
								{ anchor: 'editschedule-link', block: 'editschedule-tab' } );


/**
 * Array of calendar objects containing the initial setup literals needed
 * 
 * @param inputField {String} id of the input field to which the calendar should be binded
 * @param dateFormat {String} format that the date should be displayed
 * @param displayDates {String} 'all' make all dates available
 * 								'future' show only dates in the future
 * 								'past' show only dates in the past
 * Applied in init() function.
 */							
//var jqCalendar = new Array ( { 
//							 inputField: 'scheduleDate',
//							 dateFormat: 'dd.mm.yy',
//							 displayDates: 'all'
//							 } );

/**
 * this function remove a delivery from the schedule.  The delivery is also removed
 * from the table of deliveries displayed on the page.
 * 
 * @param {Integer} delid is the id of the delivery to be removed.
 * @param {Integer} rowcount is the row number of the delivery to be deleted.
 */
function removeDeliveryFromSchedule(delid, rowcount)
{
	// remove the delivery from the schedule
	$j.ajax({
		url: "removefromschedule.htm",
		data: {
			delid: delid
		},
		async: true
	});
	
	// remove the table row containing the delivery
	$j('tr').remove('#parent' + rowcount);
	// remove child table containing delivery items if present
	$j('tr').remove('#child' + rowcount);
	
	// get the current count of deliveries and subtract one then re-append
	$j('#delCount').text(parseInt($j('#delCount').text()) - 1);
	
	// if there are no more deliveries then add message
	if (parseInt($j('#delCount').text()) == 0)
	{
		$j('#scheddeliveries table tbody').append(	'<tr class="odd">' +
														'<td colspan="4" class="center bold">' + i18n.t("core.schedule:noDeliveriesToDisplay", "There are no deliveries to display for this schedule") + '</td>' +
													'</tr>');
	}
	else
	{
		// re-order the colour of rows in table
		$j('#scheddeliveries table tbody tr[id^="parent"]:odd').removeClass().addClass('odd');
		$j('#scheddeliveries table tbody tr[id^="parent"]:even').removeClass().addClass('even');
		// add the 'hoverrow' class to first row for key navigation
		$j('#scheddeliveries table tbody tr:first').removeClass().addClass('hoverrow');
	}
	
	location.reload(true);
}

/**
 * this function deletes the schedule from the database
 * 
 * @param {Integer} schId is the id of the schedule to be deleted
 */
function deleteSchedule(schId)
{
	if(confirm(i18n.t("core.schedule:deleteSchedule", "This schedule will now be deleted from the database. Click 'OK' to confirm the deletion or 'Cancel' to go back")))
	{
		window.location.href = 'deleteschedule.htm?schid=' + schId;
	}
}