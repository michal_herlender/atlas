/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: Jamie Vinall
*
*	FILENAME		:	ScheduleUpdater.js
*	DESCRIPTION		:	This javascript file is used on the createschedule.vm page.
*
*	HISTORY			:	29/12/2020 - Laïla MADANI - Created File
****************************************************************************************************************************************/

var jqCalendar = new Array ( { 
							 inputField: 'scheduleDate',
							 dateFormat: 'dd.mm.yy',
							 displayDates: 'all'
							 } );

function scheduledDeliverySubmit() {
	
	if($j('input[type="radio"][name="transportOptionId"]:checked').length > 0){
		document.getElementById('scheduleUpdater').submit();
	}
	else {
		alert(i18n.t("core.jobs:job.noSelectedAnyItems", "You have not selected any items, please select at least one item from the list"));
	}
}

function checkDate(index) {
	$j('#scheduleDate'+ index)[0].checked = true;
}