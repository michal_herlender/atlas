/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: Jamie Vinall
*
*	FILENAME		:	ShowSchedules.js
*	DESCRIPTION		:	This javascript file is used on the showschedules.vm page.
*					: 	
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	08/08/2007 - JV - Created File
*					:	18/10/2007 - SH - JQuery functions and comment
*					:	01/10/2015 - TProvost - Fix bug in new version of Impromptu.js - Replace the "callback" property (property disappeared) by the "submit" property
*					:							=> Changes made in functions deleteScheduleConfirmation and sendMailConfirmation
*					:	22/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * Array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'dwr/interface/scheduleservice.js' );
								
/**
 * Array of objects containing the element name and corner type which should be applied.
 * Applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * this variable contains the page name which should be used when creating the search again
 * link in the header section
 */
var searchAgain = 'schedulesearch.htm';

/**
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = 	new Array(	{ anchor: 'agreedsched-link', block: 'agreedsched-tab' },
								{ anchor: 'provisionalsched-link', block: 'provisionalsched-tab' } );

/**
 * this method toggles the visibility of schedules depending on the business company subdivisions
 * 
 * @param {Integer} subdivid the id of the subdivision to be displayed. 0 if all.
 */
function toggleDespatchedFromTransportOption(optionid)
{
	if (optionid != 0)
	{
		// hide any other subdivision entries
		$j('.rowClass').each(function(i){
			$j(this).removeClass('vis').addClass('hid')
		});
		// show the currently selected subdivision
		$j('tr[class~="transportoption' + optionid + '"]').each(function(i){
			$j(this).removeClass('hid').addClass('vis');
		});
	}
	else
	{
		// show all subdivisions
		$j('.rowClass').each(function(i){
			$j(this).removeClass('hid').addClass('vis')
		});
	}
}

/**
 * this function deletes a schedule from the database and concurrently from the page
 * 
 * @param {Integer} schId is the id of the schedule to be deleted
 * @param {String} status is the status of the schedule to be deleted
 * @param {String} delCompany the name of the company being deleted
 */								
function deleteScheduleConfirmation(schId, status, delCompany)
{
	// initial prompt for user
	$j.prompt(i18n.t("core.schedule:deleteScheduleConfirmation", {varCompName : delCompany, defaultValue : "The schedule for " + delCompany + " will now be deleted from the database. Please confirm this action?"}),
	{ 
		submit: confirmcallback,
		buttons: 
		{ 
			Ok: true,
			Cancel: false 
		}, 
		focus: 1 
	});
	// callback method for initial prompt
	function confirmcallback(v,m)
	{
		// user confirmed action?
    	if (m)
		{
			// ajax call to delete schedule
			scheduleservice.deleteScheduleById(schId);
			// remove schedule table
			$j('#schedule' + schId).remove();
			$j('#schedule1' + schId).remove();
			
			if (status == 'Provisional')
			{
				// get the current count of provisional schedules and subtract one then re-append
				$j('span.sizeSpanP').text(parseInt($j('span.sizeSpanP:first').text()) - 1);
				
				// if there are no more provisional schedules then add message
				if (parseInt($j('span.sizeSpanP:first').text()) == 0)
				{
					$j('#scheduleresults table:last tbody').append(	'<tr class="odd">' +
																		'<td colspan="6" class="center bold">' + i18n.t("core.schedule:noProvisionalScheduleToDisplay", "There are no provisional schedule results to display") + '</td>' +
																	'</tr>');
				}
			}
			else
			{
				// get the current count of agreed schedules and subtract one then re-append
				$j('span.sizeSpanA').text(parseInt($j('span.sizeSpanA:first').text()) - 1);
				
				// if there are no more agreed schedules then add message
				if (parseInt($j('span.sizeSpanA:first').text()) == 0)
				{
					$j('#scheduleresults table:first tbody').append('<tr class="odd">' +
																		'<td colspan="6" class="center bold">' + i18n.t("core.schedule:noAgreedScheduleToDisplay", "There are no agreed schedule results to display") + '</td>' +
																	'</tr>');
				}
			}
		}
	}
}

/** 
 * this method displays a confirmation message asking the user if they are sure they wish to send an email.
 */
function sendEmailConfirmation(schId)
{	
	// initial prompt for user
	$j.prompt(i18n.t("core.schedule:sendEmailConfirmation", "Are you sure you wish to send this schedule confirmation email?"),
	{ 
		submit: confirmcallback,
		buttons: 
		{ 
			Ok: true,
			Cancel: false 
		}, 
		focus: 1 
	});
	// callback method for initial prompt
	function confirmcallback(v,m)
	{
		// user confirmed action?
    	if (m)
		{			
			// send schedule email
    		// make ajax call to send the email request
    		scheduleservice.sendConfirmationEmail(schId,
    		{
    			callback: function(confirm)
    			{
	    			// alert the user to the status of the email request
	    			$j.prompt(confirm);
    			}
    		});	
		}
	}
}
