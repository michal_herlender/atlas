/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	NewManufacturer.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	24/10/2007 - SH - Created File
*					:	30/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * time value of last DWR call, checked against the current DWR call
 */
var newmanUpdate = 0;

/**
 * this function uses dwr to populate a list with manufacturer names, this list appears in a thickbox
 * when the user wishes to add a new manufacturer to the database. 
 * 
 * @param {String} nameFragment all or part of the manufactuer name
 */
function ajaxManu(nameFragment)
{
	// text has been entered into input field
	if (nameFragment.length > 0)
	{
		// get current timestamp to pass with ajax results
		var currentTime = new Date().getTime();
		
		// call dwr method to retrieve alphabetically ordered, active manufacturers
		manuservice.searchSortedActiveMfr(nameFragment, 
		{
			callback:function(mfrs)
			{
				// call method to display manufacturers
				newManufacturerCallback(mfrs, currentTime, nameFragment);
			}
		});
	}
	// input field is empty
	else
	{
		// remove all previously displayed manufacturers
		$j('div#createManufacturerOverlay #newmfrResults').empty();
		// hide the insert button
		$j('div#createManufacturerOverlay #insert').removeClass().addClass('hid');
	}
}

/**
 * this function displays all manufacturers which are currently stored in the database that have been
 * returned by method @see ajaxManu().
 * 
 * @param {Array} mfrs is an array of manufacturer objects  
 * @param {Date/Time} currentUpdate is the current timestamp of search
 * @param {String} nameFragment all or part of the manufactuer name
 */
function newManufacturerCallback(mfrs, currentUpdate, nameFragment)
{
	// this is a new search
	if (currentUpdate > newmanUpdate)
	{
		// update value in 'newmanUpdate'
		newmanUpdate = currentUpdate;
		// at least one mfr returned
		if(mfrs.length > 0)
		{
			// remove all previously displayed manufacturers
			$j('div#createManufacturerOverlay #newmfrResults').empty();
			// add div for each manufacturer returned to results list
			$j.each(mfrs, function(i)
			{
				$j('div#createManufacturerOverlay #newmfrResults').append('<div><span>' + mfrs[i].name + '</span></div>');													
			});
			// current string matches a company already in the database
			if (mfrs[0].name.toLowerCase() == nameFragment.toLowerCase())
			{
				// hide the insert button
				$j('div#createManufacturerOverlay #insert').removeClass().addClass('hid');
			}
			// current string is a valid new company
			else
			{
				// hide the insert button
				$j('div#createManufacturerOverlay #insert').removeClass().addClass('vis');
			}										
		}
		else
		{
			// remove all previously displayed manufacturers and append message
			$j('div#createManufacturerOverlay #newmfrResults').empty().append('<div class="center-0 bold"><span>' + i18n.t("core.utilities:noMatchingManufacturers", "No Matching Manufacturers") + '</span></div>');
			// make insert button visible
			$j('div#createManufacturerOverlay #insert').removeClass().addClass('vis');
		}
	}
}

/**
 * this function uses dwr to insert a new manufacturer into the database. If the insertion is successful
 * then a message is diplayed within the thickbox detailing the success. Else if the insertion is declined
 * then appropriate error messages are displayed within the thickbox.
 */
function insertMfr()
{
	// get value of the new manufacturer to be inserted
	var newManufacturer = $j('div#createManufacturerOverlay #newManufacturer').val();
	// call dwr method to insert the new manufacturer
	manuservice.insert(newManufacturer, 
	{
		callback:function(results)
		{
			// insertion failed
			if(!results.success)
			{
				// warning box is not already present
				if ($j('div#createManufacturerOverlay .warningBox1').length == 0)
				{
					// remove successbox if present
					$j('div#createManufacturerOverlay').remove('.successBox1');
					// append the new warningbox before fieldset
					$j('div#createManufacturerOverlay  fieldset').before(	'<div class="warningBox1">' +
																				'<div class="warningBox2">' +
																					'<div class="warningBox3">' +
																						// new warning message goes here
																					'</div>' +
																				'</div>' +
																			'</div>');
				}
				// empty the current warningbox and append message
				$j('div#requestNewManufacturerOverlay .warningBox3').empty().append('<h5 class="center-0 attention">' + i18n.t("core.utilities:addManufacturerFailed", "New Manufacturer Failed") + '</h5>');
				// loop through errorlist
				$j.each(results.errorList, function(i, n)
				{
					// name field of manufacturer failed
					if (n.field == 'name')
					{
						// display error message next to the name field
						$j('div#createManufacturerOverlay #errorMessage').append(n.defaultMessage);
					}
					else
					{
						// other type of error so display in main warningbox
						$j('div#requestNewManufacturerOverlay .warningBox3').append('<div class="text-center attention">' + i18n.t("fieldError", {varErrField: n.field, errMsg : n.defaultMessage, defaultValue : "Field: " + n.field + " failed. Message:" + n.defaultMessage}) + '</div>');	
					}
				});
			}
			// insertion was a success
			else
			{
				// remove warningbox if present
				$j('div#createManufacturerOverlay').remove('.warningBox1').remove('.successBox1');
				// empty the errormessage span
				$j('div#createManufacturerOverlay #errorMessage').empty();
				// append new successbox before fieldset
				$j('div#createManufacturerOverlay fieldset').before('<div class="successBox1">' +
																		'<div class="successBox2">' +
																			'<div class="successBox3">' +
																				i18n.t("core.utilities:addManufacturerSucced", {varName : results.results.name, defaultValue : "New manufacturer " + results.results.name + " added successfully"}) +
																			'</div>' +
																		'</div>' +
																	'</div>');
				// empty value in new manufacturer input field
				$j('div#createManufacturerOverlay #newManufacturer').attr('value', '');
				// hide the insert button
				$j('div#createManufacturerOverlay #insert').removeClass().addClass('hid');
				// remove all previously displayed manufacturers
				$j('div#createManufacturerOverlay #newmfrResults').empty();	
			}	
		}
	});
}

/**
 * this function creates content when adding a new manufacturer
  *
  * @param {boolean} content for tickbox (true) or overlay (false)
 */
function createManufacturerContent(forTickbox)
{
	// create content to be displayed in overlay window using previously populated variables
	var content =	'<div id="createManufacturerOverlay">' +
						'<div class="warningBox1">' +
							'<div class="warningBox2">' +
								'<div class="warningBox3">' +
									'<div>' + i18n.t("core.utilities:infoRequestNewManufacturer", "Enter your new manufacturer name in the input field below, only when there are no matches in the current database will the insert button be available to you.") + '</div>' +
								'</div>' +
							'</div>' +
						'</div>' +
						'<fieldset>' +
							'<ol>' +
								'<li>' +
								'<label for="manufacturer">' + i18n.t("core.utilities:manufacturerNameLabel", "Manufacturer Name:") + '</label>' +
									'<span>' +
										'<input type="text" name="newManufacturer" id="newManufacturer" onkeyup="ajaxManu(this.value, \'new\'); return false;" autocomplete="off" />' +
										'<input type="submit" name="insert" id="insert" value="' + i18n.t("core.utilities:plugins.insert", "Insert") + '" class="hid" onclick=" insertMfr(); "/>' +
										'<span id="errorMessage" class="attention"></span>' +
									'</span>' +
								'</li>' +
								'<li>' +
								'<label>' + i18n.t("core.utilities:manufacturerMatchesLabel", "Manufacturer Matches:") + '</label>' +
									'<span>' +
										'<div style=" width: 40%; float: left; ">' +
										'<div class="generalHeader"><span>' + i18n.t("core.utilities:matchingManufacturers", "Matching Manufacturers") + '</span></div>' +
											// create and append the div in which manufacturer results will be displayed
											'<div id="newmfrResults" class="generalResults">' +
												// results are displayed here
											'</div>' +
										'</div>' +
									'</span>' +
									'<div class="clear"></div>' +
								'</li>' +
							'</ol>' +
						'</fieldset>' +
					'</div>';
	if (forTickbox == true)
	{
		// return thickbox content
		return content;
	}
	else 
	{
		// create overlay
		loadScript.createOverlay('dynamic', i18n.t("core.utilities:requestNewManufacturer", "Request New Manufacturer"), null, content, null, 40, 680, 'newManufacturer');
	}

}