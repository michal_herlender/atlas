/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	DocGenPopup.js
*	DESCRIPTION		:	This javascript is used when a document is being generated to show a popup and animated image.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	28/01/2009 - SH - Created File
*
****************************************************************************************************************************************/

/**
 * this method displays an image and text when the user has chosen to generate a document
 * 
 * @param {Integer} id id of the entity used to create the document
 * @param {String} url the location of the page we are redirecting to for document generation
 * @param {String} message the message to be displayed in pop up
 */
function genDocPopup(id, url, message)
{
	// loading image and message content
	var content = 	'<div class="text-center padding10">' +
						'<img src="img/icons/ajax-loader.gif" width="31" height="31" alt="" />' +
						'<br /> <h4>' + message + '</h4>' +
					'</div>';
					
	// create new overlay
	loadScript.createOverlay('dynamic', null, null, content, null, 18, 500, null);
	// load document generation page
	window.location.href = '' + url + '' + id;
}