/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	TooltipLinks.js
*	DESCRIPTION		:	This is a javascript version of velocity macros which are used to create tooltips for different entities such
*					:	as instruments, companies, contacts etc...
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	03/09/2010 - SH - Created File.
*					:	19/10/2015 - TProvost - Add test on null value of instrument.model.modelDefaults
*					:	30/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/


function instrumentLinkDWRInfo(instrument, rowcount, displayBarcode, displayName, displayCalTimescale, caltypeid,model_name)
{
	// create variable for content
	var content = '';
	// display timescale percentage?
	if (displayCalTimescale)
	{
		content += 	'<div class="float-left relative">';
			// display timescale
			if ((instrument.inCalTimescalePercentage != null) && (instrument.wholeCalTimescalePercentage != null) && (instrument.finalMonthOfCalTimescalePercentage != null))
			{
				content += 	'<div class="calIndicatorBox" ';
				 				if (instrument.wholeCalTimescalePercentage > 0)
				 				{
				 					content += 	'title="' + i18n.t("core.utilities:calPeriodPassed", "Calibration Period Passed") + ' - ' + instrument.wholeCalTimescalePercentage + '&#37;"';
				 				}
				 				else
				 				{
				 					content += 	'title="' + i18n.t("core.utilities:unableToCalculate", "Unable To Calculate") + '"';
				 				}
				content += 	'>';
					if ((instrument.inCalTimescalePercentage == 100) && (instrument.wholeCalTimescalePercentage == 100) && (instrument.finalMonthOfCalTimescalePercentage == 100))
					{
						content += 	'<div class="outOfCalibration"></div>';
					}
					else
					{
						content += 	'<div class="inCalibration">' +
										'<div class="inCalibrationLevel" style=" width: ' + instrument.inCalTimescalePercentage + '%; "></div>' +
									'</div>' +
									'<div class="finalMonthOfCalibration">' +
										'<div class="finalMonthOfCalibrationLevel" style=" width: ' + instrument.finalMonthOfCalTimescalePercentage + '%; "></div>' +
									'</div>';	
					}
				content +=	'</div>';				
			}
		content += 	'</div>';
	}		
	if (instrument.calibrationStandard == true)
	{
		content += 	'<img src="img/icons/bullet-standard.png" width="12" height="12" class="image_inl" alt="' + i18n.t("standard", "Standard") + '" title="' + i18n.t("standard", "Standard") + '" /> ';
	}
	if (instrument.scrapped == true)
	{
		content += 	'<img src="img/icons/bin_closed.png" width="16" height="16" class="image_inl" alt="' + i18n.t("unitBER", "Unit B.E.R.") + '" title="' + i18n.t("unitBER", "Unit B.E.R.") + '" /> ';
	}
	// main anchor content
	content += 	'&nbsp;&nbsp;<a href="viewinstrument.htm?plantid=' + instrument.plantid + '&amp;ajax=instrument&amp;width=500" class="jconTip ';
					if (instrument.scrapped == true)
					{
						content += 	'mainlink-strike';
					}
					else
					{
						content += 	'mainlink';
					}
					content += 	'" name="' + i18n.t("instrInformation", "Instrument Information") + '" id="inst' + instrument.plantid + rowcount + '">';
					if (displayBarcode)
					{
						content += 	instrument.plantid;
					}
					if (displayName)
					{
						if (displayBarcode)
						{
							content += ' ';
						} else {
						    
                            content +=	model_name;
						}

					}
	content +=	'</a>' + 
				showRolloverMouseIcon();
	
	return content;
}

function jobnoLinkDWRInfo(jobid, jobno, rowcount, copy)
{
	var content = 	'<a href="viewjob.htm?jobid=' + jobid + '&amp;ajax=job&amp;width=600" class="jconTip mainlink" name="' + i18n.t("jobInformation", "Job Information") + '" id="job' + jobno + '' + rowcount + '" tabindex="-1">' + jobno + '</a>' +
					showRolloverMouseIcon();
					if (copy)
					{
						content += ' <img src="img/icons/bullet_clipboard.png" width="10" height="10" onclick=" $j(this).prev().prev().clipBoard(this, null, false, null); " alt="' + i18n.t("core.utilities:addJobNoToClipboard", "Add job number to clipboard") + '" title="' + i18n.t("core.utilities:addJobNoToClipboard", "Add job number to clipboard") + '" />';
					}
				
	return content;
}

function jobitemLinkDWRInfo(jiDefPage, jiId, jiItemNo, rowcount)
{
	var content = 	'<a href="' + jiDefPage + '?jobitemid=' + jiId + '&amp;ajax=jobitem&amp;width=580" class="jconTip mainlink" name="' + i18n.t("jobItemInformation", "Job Item Information") + '" id="jobitem' + jiId + '' + rowcount + '" tabindex="-1">' + jiItemNo + '</a>' +
					showRolloverMouseIcon();
	
	return content;
}

function procedureLinkDWRInfo(proc, rowcount, displayName)
{
	var content = 	'<a href="viewcapability.htm?capabilityId=' + proc.id + '&amp;ajax=procedure&amp;width=500" class="jconTip mainlink" name="' + i18n.t("procedureInformation", "Procedure Information") + '" id="procedure' + proc.id + rowcount + '" tabindex="-1">' + proc.reference + '';
					if (displayName)
					{
						content +=	' ' + proc.name;
					}
					content +=	'</a>' +
					showRolloverMouseIcon();
	
	return content;
}

function showRolloverMouseIcon()
{
	return ' <img src="img/icons/mouse.png" width="12" height="12" class="image_inl" alt="" title="" />';
}