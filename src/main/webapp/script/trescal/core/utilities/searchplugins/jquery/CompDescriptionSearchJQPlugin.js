/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	CompDescriptionSearchJQPlugin.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   script/thirdparty/jQuery/jquery.js
*	OPTIONS:			FIELDID			:	this is the name of the desc id field to be passed
*						FIELDNAME		:	this is the name of the desc text field to be passed
*						TEXTONLYOPTION	:	this tells the plugin if a desc text search is required
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	30/04/2009 - SH - Created File
*					:	29/10/2015 - TProvost - Fix bug on test of the version of browser
*					:	30/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

(function( $j )
{
	// variable to indicate whether any navigation keys have been pressed to stop ajax
	var keyPress = false;
	// variable to indicate which anchor in list is currently selected - used for key navigation
	var selected = 0;
	// variable to hold time value of last DWR call, checked against the current DWR call
	var lastUpdate = 0;
    
    $j.fn.compDescSearchJQPlugin = function( options )
    {
        var options = $j.extend({
			
			/**
			 * name of the hidden input field for comp desc id
			 */
			fieldId:		'',
			/**
			 * name of the input field for comp desc name
			 */
			fieldName: 		'',
			/**
			 * indicates if this search plugin should allow comp descrip text searches as well as
			 * matching comp desc searches. This is used on search forms to allow part name searches.
			 */
			textOnlyOption: 'false',
			/**
			 * the current search type we are using (i.e. matched comp descrips (descid) or comp descrip text (desctext))
			 */
			searchType:		'descid',
			/**
			 * tab index for text input field
			 */
			tabIndex:		0,
			/**
			 * prefill the name of the comp description
			 */
			compDescNamePrefill:	'',
			/**
			 * prefill the id of the comp description
			 */
			compDescIdPrefill:	'',
			/**
			 * container div
			 */
			container: 		'compDescContainerJQPlugin',
            /**
			 * class name of the results div
			 */
			resultsDiv: 	'compDescResultsJQPlugin',
			/**
			 * class name of the footer div
			 */
			footDiv: 		'compDescFootJQPlugin'
			
        }, options);

        return this.each(function()
        {
            compDescSearchJQPlugin(this, options);
        });
    };

    function compDescSearchJQPlugin( element, options )
    {
        return this instanceof compDescSearchJQPlugin
            ? this.init(element, options)
            : new compDescSearchJQPlugin(element, options);
    }

    $j.extend(compDescSearchJQPlugin.prototype,
    {
        original : null,
        options  : {},

        element  : null,
        searchDiv   : null,

		/**
		 * plugin initialisation function
		 * 
		 * @param {Object} element is the current plugin div to manipulate
		 * @param {Object} options are options for the current plugin
		 */
        init : function( element, options )
        {			
			// initialise search div
            this.searchDiv = element;
			// initialise options
            this.options = options || {};
			
            // script to load for dwr service
			var Scripts = new Array (	'dwr/interface/componentdescriptionservice.js' );
			// load the common javascript files for this plugin
			loadScript.loadScripts(Scripts);
			
			// call method to draw plugin search
            this.drawSearch(this.options.textOnlyOption);
			
			// set this div object
			var self = this;
			
			// add keyup and keypress events to the text input and bind method to call comp desc dwr service
			$j( $j(this.searchDiv).find('input[type="text"]:first') ).keyup(function() { self.runCompDescAjax(this.value, self); return false; })
																		.keypress(function(event) { if (event.keyCode==13){ return false; } });
			// add click event to the close image in footer div and bind method to close the plugin
			$j( $j(this.searchDiv).find('div.compDescContainerJQPlugin img:last') ).click(function() { self.closePlugin(); return false; });
			// allow text searches as well as matching comp desc searches
			if (this.options.textOnlyOption == 'true') 
			{
				// add click event to the link anchor's and bind methods
				$j($j(this.searchDiv).find('> a.textOnlyOption')).each(function(i)
				{				
					// first anchor?
					if (i < 1)
					{
						// add click event
						$j(this).click(function()
						{
							// method changes the current use of input to search matching comp descs
							self.searchByMatchingCompDesc(this, self);							
							return false;
						});	
					}
					else
					{
						// add click event
						$j(this).click(function()
						{
							// method changes the current use of input to search using text
							self.searchByCompDescText(this);							
							return false;
						});		
					}				
				});
			}
			
			/**
			 * start jquery keypress observer to check if the user has pressed the up/down arrow or the
			 * enter/esc key when searching for comp descriptions
			 * 
			 */ 	
			$j( $j(this.searchDiv) ).keydown(function(e) 
			{			
			    var event = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
				
				switch (event)
				{
					//  up arrow key has been pressed
					case 38: keyPress = true;
							 // check selected variable
							 if (selected == 0)
							 {
							 	
							 }
							 // selected variable contains value greater than zero
							 else
							 {
							 	 // remove 'selected' class name from the currently selected element and add to the previous one, apply focus
								 $j(this).find('> div div:first a').eq(selected).removeClass().prev().addClass('selected').focus();
								 // remove one from the selected variable value
								 selected = selected - 1;
								 // then return focus to the input field
								 $j(this).find('input[type="text"]:first').focus();
							 }
							 break;
							 
					// down arrow key has been pressed
					case 40: keyPress = true;
							 // check selected variable is not greater than list length
							 if (selected == $j(this).find('> div div:first a').size() - 1)
							 {
							 
							 }
							 // selected variable contains value less than list length
							 else
							 {
							 	// remove 'selected' class name from the currently selected element and add to the next one, apply focus
								$j(this).find('> div div:first a').eq(selected).removeClass().next().addClass('selected').focus();
								// remove one from the selected variable value
								selected = selected + 1;
								// then return focus to the input field
								$j(this).find('input[type="text"]:first').focus();
							 }
							 break;
							 
					// escape key has been pressed
					case 27: keyPress = true;
							 // call function to close plugin
							 self.closePlugin();
							 break;
							 
					// enter key has been pressed
					case 13: keyPress = true;
							 // trigger click event of anchor
						 	 $j(this).find('> div div:first a').eq(selected).trigger('click');
							 break;
					
					// left arrow key has been pressed, just stop ajax from being initiated if pressed accidentally
					case 37: keyPress = true;
							 break;
							 
					// right arrow key has been pressed, just stop ajax from being initiated if pressed accidentally
					case 39: keyPress = true;
							 break;
							 
					// tab key has been pressed, just stop ajax from being initiated if pressed accidentally
					case 9: keyPress = true;
							break;
				}
				
			});
        },
		
		/**
		 * method to draw plugin search
		 * 
		 * @param textOnlyOption boolean value indicating if the plugin should also allow text only (i.e. no search for matching mfr's)
		 */
        drawSearch : function(textOnlyOption)
        {
			var content = 	'<input type="text" name="desctext" autocomplete="off" ';
							if (this.options.tabIndex != 0)
							{
								content += 'tabindex="' + this.options.tabIndex + '" ';
							}
							content += 'value="' + this.options.compDescNamePrefill + '" />';
							if (textOnlyOption == 'true')
							{
								content += 	'<a href="#" class="textOnlyOption">' +
												'<img src="img/icons/searchplugin_sel.png" width="16" height="16" alt="' + i18n.t("core.utilities:searchplugins.searchMatchingDesc", "Search Matching Descriptions") + '" title="' + i18n.t("core.utilities:searchplugins.searchMatchingDesc", "Search Matching Descriptions") + '" />' +
											'</a>' +
											'<a href="#" class="textOnlyOption">' + 
												'<img src="img/icons/textsearch.png" width="16" height="16" class="searchimage" alt="' + i18n.t("core.utilities:searchplugins.searchTextOnly", "Search Text Only") + '" title="' + i18n.t("core.utilities:searchplugins.searchTextOnly", "Search Text Only") + '" />' +
											'</a>';	
							}
							else
							{
								content += '<img src="img/icons/searchplugin.png" width="16" height="16" alt="' + i18n.t("core.utilities:searchplugins.searchMatchingDesc", "Search Matching Descriptions") + '" title="' + i18n.t("core.utilities:searchplugins.searchMatchingDesc", "Search Matching Descriptions") + '" />';
							}
		content = content +	'<div class="' + this.options.container + '">' +
								// create and append the div in which comp descriptions results will be displayed
								'<div class="' + this.options.resultsDiv + '">' +
									// results are displayed here
								'</div>' +
								// create and append footer with close button
								'<div class="' + this.options.footDiv + '"><span style=" line-height: 18px; margin-right: 18px; ">(' + i18n.t("core.utilities:searchplugins.escKey", "Esc Key") + ') ' + i18n.t("core.utilities:searchplugins.or", "or") + ' </span><img src="img/icons/bullet-cross.png" width="10" height="10" alt="' + i18n.t("core.utilities:searchplugins.closeDescriptionSearch", "Close Description Search") + '" title="' + i18n.t("core.utilities:searchplugins.closeDescriptionSearch", "Close Description Search") + '" class="pluginClose" /></div>' +
								'<input type="hidden" name="' + this.options.fieldId + '" value="' + this.options.compDescIdPrefill + '" />' +
							'</div>';
						
			// append plugin content to main div
			$j(this.searchDiv).append(content);
			// mozilla or opera browser						
			if ($j.browser != null && ($j.browser.mozilla != null || $j.browser.opera != null))
			{
				$j(this.searchDiv).parent('li').css({ paddingBottom: '4px'});
			}
			// other browsers
			else
			{
				$j(this.searchDiv).parent('li').css({ paddingBottom: '3px'});
			}
			
        },
		
		/**
		 * method which accepts the name fragment provided from the input field and
		 * calls the dwr service to retrieve comp descriptions
		 * 
		 * @param {String} nameFragment text entered into input field which should be used to search comp descriptions
		 * @param {Object} self reference to this object
		 */
		runCompDescAjax : function(nameFragment, self)
		{					
			// navigation keys have been pressed so set value back to false and skip ajax loading
			if (keyPress == true)
			{
				keyPress = false;
			}
			else
			{
				// another search is in use
				if ( ($j( $j(this.searchDiv).find('> div div:first a') ).length < 1) && ($j('div[class*="ResultsJQPlugin"] > a').length > 0) )
				{
					// reset input field
					$j( $j(this.searchDiv).find('input[type="text"]:first') ).attr('value', '');
					// alert user that another email plugin is in use
					$j.prompt(i18n.t("core.utilities:searchplugins.anotherPluginIsInUse", "Another plugin is in use, please close before continuing!"));
					// exit method
					return false;
				}																
				// remove all previously displayed comp descriptions
				$j( $j(this.searchDiv).find('> div div:first') ).empty();
				// display the comp description results container
				$j( $j(this.searchDiv).find('> div div') ).css({zIndex: 100, display: 'block'});																	
				// this span displays the loading image and text when a search has been activated
				$j( $j(this.searchDiv).find('> div div:first') ).append('<div class="loading-plug">' +
																			'<img src="img/icons/loading-anim.gif" class="img_marg_bot" height="18" width="80" alt="" />' +
																		'</div>');				
				// get current timestamp to pass with ajax results
				var currentTime = new Date().getTime();
				// only execute the ajax functionality if the text entered has a length
				// greater than zero (stops unconstrained retrieval)
				if (nameFragment.length > 1)
				{
					// call dwr service to retrieve comp descriptions
					componentdescriptionservice.searchSortedAllComponentDescriptionsHQL(nameFragment,
					{
						callback:function(descs)
						{
							// call method to display comp descriptions returned
							self.compDescCallback(descs, currentTime, self);
						}
					});
				}
				// text entered has a length of 0
				else
				{														
				 	// hide the comp description results container
					$j( $j(this.searchDiv).find('> div div') ).css({zIndex: -1, display: 'none'});
					// remove all previously displayed comp descriptions
					$j( $j(this.searchDiv).find('> div div:first') ).empty();
					// clear id hidden input field
					$j( $j(this.searchDiv).find('> div input[type="hidden"]') ).val('');				
				}
			}
			
		},
		
		/**
		 * this method appends all comp descriptions returned from the dwr service to the results div
		 * 
		 * @param {Array} descs is an array of comp descriptions returned from the dwr service
		 * @param {Date/Time} currentUpdate is a timestamp retrieved when the search was initiated
		 * @param {Object} self reference to this object
		 */
		compDescCallback : function(descs, currentUpdate, self) 
		{
			// this is a new search
			if (currentUpdate > lastUpdate)
			{
				// update value in the 'lastUpdate' variable
				lastUpdate = currentUpdate;
				
				// comp descriptions length is greater than zero
				if(descs.length > 0)
				{
					// remove all previously displayed comp descriptions
					$j( $j(this.searchDiv).find('> div div:first') ).empty();
					// declare variable to hold results content
					var content = '';
					// add anchor for each comp description returned to results variable
					$j(descs).each(function(i)
					{	
						content = content + '<a href="#" id="' + descs[i].id + '"><span>' + descs[i].description + '</span></a>';																																			
					});
					// append results variable to results div
					$j( $j(this.searchDiv).find('> div div:first') ).append(content);
					
					// loop through all anchors appended to results div
					$j( $j(this.searchDiv).find('> div div:first a') ).each(function(i)
					{
						// apply click event to anchor
						$j( $j(this) ).click( function(){ self.selectCompDescId(this.id, $j(this).text(), self); return false; } );
					});			
					// add 'selected' class name to first anchor in results div
					$j( $j(this.searchDiv).find('> div div:first a:first') ).addClass('selected');
					// initialise the selected variable to zero
					selected = 0;																						
				}
				else
				{
					// remove all previously displayed comp descriptions
					$j( $j(this.searchDiv).find('> div div:first') ).empty().append('<div class="center bold marg-top">' + i18n.t("core.utilities:searchplugins.noDescriptionsFound", "No Descriptions Found") + '</div>');
				}
			}
		},
		
		/**
		 * this method selects the chosen comp description and adds the name to the text input field and adds the
		 * mfrid to the hidden input field after which the results div is emptied and hidden
		 * 
		 * @param {Integer} descid id of the comp description chosen
		 * @param {String} desc name of the comp description chosen
		 * @param {Object} self reference to this object
		 */
		selectCompDescId : function (descid, desc, self)
		{
			// add comp description id to the hidden input field
			$j( $j(this.searchDiv).find('> div input[type="hidden"]') ).val(descid);
			// add comp description text to the search text input field
			$j( $j(this.searchDiv).find('> input[type="text"]:first') ).val(desc);
			// empty the results div
			$j( $j(this.searchDiv).find('> div div:first') ).empty();
			// hide the search results box and footer
			$j( $j(this.searchDiv).find('> div div') ).css({zIndex: -1, display: 'none'});
		},
		
		/**
		 * this method changes the search type to text search instead of matching comp descriptions search
		 * 
		 * @param {Object} anchor the anchor clicked to change search type
		 */
		searchByCompDescText : function (anchor)
		{
			if (this.options.searchType == 'descid')
			{
				// close the plugin if it is present and clear fields
				this.closePlugin();
				// change the search comp descs image to unticked
				$j(anchor).prev()
							.find('img')
							.before('<img src="img/icons/searchplugin.png" width="16" height="16" alt="' + i18n.t("core.utilities:searchplugins.searchMatchingDesc", "Search Matching Descriptions") + '" title="' + i18n.t("core.utilities:searchplugins.searchMatchingDesc", "Search Matching Descriptions") + '" />')
							.remove();
				// change the search text image to ticked
				$j(anchor).find('img')
							.after('<img src="img/icons/textsearch_sel.png" width="16" height="16" class="searchimage" alt="' + i18n.t("core.utilities:searchplugins.searchTextOnly", "Search Text Only") + '" title="' + i18n.t("core.utilities:searchplugins.searchTextOnly", "Search Text Only") + '" />')
							.remove();
				// unbind the keyup event to the text input and change it's name to the supplied option value
				$j( $j(this.searchDiv).find('input[type="text"]:first') ).attr('name', this.options.fieldName).unbind('keyup').unbind('keypress');
				// change the search type option
				this.options.searchType = 'desctext';		
			}		
		},
		
		/**
		 * this method changes the search type to matching comp descriptions instead of text search
		 * 
		 * @param {Object} anchor the anchor clicked to change search type
		 * @param {Object} self reference to this object
		 */
		searchByMatchingCompDesc : function (anchor, self)
		{
			if (this.options.searchType == 'desctext')
			{
				// change the search text image to unticked
				$j(anchor).next()
							.find('img')
							.after('<img src="img/icons/textsearch.png" width="16" height="16" class="searchimage" alt="' + i18n.t("core.utilities:searchplugins.searchTextOnly", "Search Text Only") + '" title="' + i18n.t("core.utilities:searchplugins.searchTextOnly", "Search Text Only") + '" />')
							.remove();
				// change the search descs image to ticked
				$j(anchor).find('img')
							.after('<img src="img/icons/searchplugin_sel.png" width="16" height="16" alt="' + i18n.t("core.utilities:searchplugins.searchMatchingDesc", "Search Matching Descriptions") + '" title="' + i18n.t("core.utilities:searchplugins.searchMatchingDesc", "Search Matching Descriptions") + '" />')
							.remove();				
				// add keyup event to the text input, change text input name back to original value, clear input and apply focus
				$j( $j(this.searchDiv).find('input[type="text"]:first') )
										.attr('name', 'desctext')
										.keyup(function() { self.runCompDescAjax(this.value, self); return false; })
										.keypress(function(event) { if (event.keyCode==13){ return false; } })
										.val('')
										.focus();
				// change the search type option
				this.options.searchType = 'descid';		
			}		
		},
	
		/**
		 * this function closes the search plugin and clears all inputs
		 */				
		closePlugin : function()
		{
			// clear the text input field and apply focus 
			$j( $j(this.searchDiv).find('input[type="text"]:first') ).attr('value', '').focus();
			// clear id in the hidden input field
			$j( $j(this.searchDiv).find('> div input[type="hidden"]') ).val('');
			// empty the results div
			$j( $j(this.searchDiv).find('> div div:first') ).empty();
			// hide the search results box and footer
			$j( $j(this.searchDiv).find('> div div') ).css({zIndex: -1, display: 'none'});
		}
        
    });
})(jQuery);