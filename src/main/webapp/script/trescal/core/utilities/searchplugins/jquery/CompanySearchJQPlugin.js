/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	CompanySearchJQPlugin.js
*	DESCRIPTION		:	 
*
*	DEPENDENCIES	:   script/thirdparty/jQuery/jquery.js
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	08/01/2009 - SH - Created File
*					:	29/10/2015 - TProvost - Fix bug on test of the version of browser
*					:	30/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

(function( $j )
{
	// variable to indicate whether any navigation keys have been pressed to stop ajax
	var keyPress = false;
	// variable to indicate which anchor in list is currently selected - used for key navigation
	var selected = 0;
	// variable to hold time value of last DWR call, checked against the current DWR call
	var lastUpdate = 0;
    
    $j.fn.compSearchJQPlugin = function( options )
    {
        var options = $j.extend({
			
			/**
			 * name of the hidden input field
			 */
			field:			'',
			/**
			 * list of coroles to search over
			 */
			compCoroles:		'',
			/**
			 * include deactivated companies
			 */
			deactivated:	'false',
			/**
			 * copy option allows the user to add a image link which will
			 * copy the chosen mfr to all other occurences on page. This option should
			 * only be provided for the first occurence of plugin.
			 */
			copyOption: 	'false',
			/**
			 * trigger method with a default name when company is selected
			 * the default method will need to be defined in the javascript on the page in question
			 */
			triggerMethod: 	'false',
			/**
			 * tab index for text input field
			 */
			tabIndex:		0,
			/**
			 * prefill the name of the company
			 */
			compNamePrefill:	'',
			/**
			 * prefill the id of the company
			 */
			compIdPrefill:	'',
			/**
			 * container div
			 */
			container: 		'compContainerJQPlugin',
            /**
			 * class name of the results div
			 */
			resultsDiv: 	'compResultsJQPlugin',
			/**
			 * class name of the footer div
			 */
			footDiv: 		'compFootJQPlugin',
			/**
			 * corole array used for company search
			 */
			coroles:		[]
        
        }, options);

        return this.each(function()
        {
            compSearchJQPlugin(this, options);
        });
    };

    function compSearchJQPlugin( element, options )
    {
        return this instanceof compSearchJQPlugin
            ? this.init(element, options)
            : new compSearchJQPlugin(element, options);
    }

    $j.extend(compSearchJQPlugin.prototype,
    {
        original : null,
        options  : {},

        element  : null,
        searchDiv   : null,

		/**
		 * plugin initialisation function
		 * 
		 * @param {Object} element is the current plugin div to manipulate
		 * @param {Object} options are options for the current plugin
		 */
        init : function( element, options )
        {			
			// initialise search div
            this.searchDiv = element;
			// initialise options
            this.options = options || {};			
			// separate coroles and put into array
			this.options.coroles = this.options.compCoroles.split(',');			
			// call method to draw plugin search
            this.drawSearch();
			
			// set this div object
			var self = this;
			
			// add keyup event to the text input and bind method to call company dwr service
			$j( $j(this.searchDiv).find('input[type="text"]:first') ).keyup(function() { self.runCompAjax(this.value, self); return false; });
			// add click event to the close image in footer div and bind method to close the plugin
			$j( $j(this.searchDiv).find('div.compContainerJQPlugin img:last') ).click(function() { self.closePlugin(); return false; });
			// copy selected company to other fields required
			if (this.options.copyOption == 'true') 
			{
				// add click event to the anchor and bind method to call copy option method
				// for text input field and hidden input field
				$j($j(this.searchDiv).find('> a')).each(function(i)
				{
					// variable to hold direction of copy
					var direction = 'up';
					// first anchor
					if (i < 1)
					{
						direction = 'down';
					}										
					// add click function to copy down anchor
					$j(this).click(function()
					{
						// call copy function
						self.copyOption($j(this).parent().find('input[type="text"]:first'), direction);
						// call copy function
						self.copyOption($j(this).parent().find('div:first input[type="hidden"]:last'), direction);
						return false;
					});					
				});
			}
			
			/**
			 * start jquery keypress observer to check if the user has pressed the up/down arrow or the
			 * enter/esc key when searching for mfr's
			 * 
			 */ 	
			$j( $j(this.searchDiv) ).keydown(function(e) 
			{			
			    var event = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
				
				switch (event)
				{
					//  up arrow key has been pressed
					case 38: keyPress = true;
							 // check selected variable
							 if (selected == 0)
							 {
							 	
							 }
							 // selected variable contains value greater than zero
							 else
							 {
							 	 // remove 'selected' class name from the currently selected element and add to the previous one, apply focus
								 $j(this).find('> div div:first a').eq(selected).removeClass('selected').prev().addClass('selected').focus();
								 // remove one from the selected variable value
								 selected = selected - 1;
								 // then return focus to the input field
								 $j(this).find('input[type="text"]:first').focus();
							 }
							 break;
							 
					// down arrow key has been pressed
					case 40: keyPress = true;
							 // check selected variable is not greater than list length
							 if (selected == $j(this).find('> div div:first a').size() - 1)
							 {
							 
							 }
							 // selected variable contains value less than list length
							 else
							 {
							 	// remove 'selected' class name from the currently selected element and add to the next one, apply focus
								$j(this).find('> div div:first a').eq(selected).removeClass('selected').next().addClass('selected').focus();
								// remove one from the selected variable value
								selected = selected + 1;
								// then return focus to the input field
								$j(this).find('input[type="text"]:first').focus();
							 }
							 break;
							 
					// escape key has been pressed
					case 27: keyPress = true;
							 // call function to close plugin
							 self.closePlugin();
							 break;
							 
					// enter key has been pressed
					case 13: keyPress = true;
							 // trigger click event of anchor
						 	 $j(this).find('> div div:first a').eq(selected).trigger('click');
							 break;
					
					// left arrow key has been pressed, just stop ajax from being initiated if pressed accidentally
					case 37: keyPress = true;
							 break;
							 
					// right arrow key has been pressed, just stop ajax from being initiated if pressed accidentally
					case 39: keyPress = true;
							 break;
							 
					// tab key has been pressed, just stop ajax from being initiated if pressed accidentally
					case 9: keyPress = true;
							break;
				}
				
			});
        },
		
		/**
		 * method to draw plugin search
		 */
        drawSearch : function()
        {
        	var content = 	'<input type="text" name="comptext" onkeypress=" if (event.keyCode==13){ return false; } " autocomplete="off" ';
							if (this.options.tabIndex != 0)
							{
								content += 'tabindex="' + this.options.tabIndex + '" ';
							}							
							content += 'value="' + this.options.compNamePrefill + '" /><img src="img/icons/searchplugin.png" width="16" height="16" alt="' + i18n.t("", "Option Search") + '" title="' + i18n.t("", "Option Search") + '" />';
							// copy options required
							if 	(this.options.copyOption == 'true')
							{
								content +=	'<a href="#">' +
												'<img src="img/icons/arrow_down.png" width="10" height="16" alt="' + i18n.t("copySelectionDown", "Copy selection down") + '" title="' + i18n.t("copySelectionDown", "Copy selection down") + '" />' +
											'</a>' + 
											'<a href="#">' +
												'<img src="img/icons/arrow_up.png" width="10" height="16" alt="' + i18n.t("copySelectionUp", "Copy selection up") + '" title="' + i18n.t("copySelectionUp", "Copy selection up") + '" />' +
											'</a>';
							}
					content +=	'<div class="' + this.options.container + '">' +
								// create and append the div in which company results will be displayed
								'<div class="' + this.options.resultsDiv + '">' +
									// results are displayed here
								'</div>' +
								// create and append footer with close button
								'<div class="' + this.options.footDiv + '"><span style=" line-height: 18px; margin-right: 18px; ">(' + i18n.t("core.utilities:searchplugins.escKey", "Esc Key") + ') ' + i18n.t("core.utilities:searchplugins.or", "or") + ' </span><img src="img/icons/bullet-cross.png" width="10" height="10" alt="' + i18n.t("core.utilities:searchplugins.closeCompanySearch", "Close Company Search") + '" title="' + i18n.t("core.utilities:searchplugins.closeCompanySearch", "Close Company Search") + '" class="pluginClose" /></div>' +
								'<input type="hidden" name="' + this.options.field + '" value="' + this.options.compIdPrefill + '" />' +
							'</div>';
						
			// append plugin content to main div
			$j(this.searchDiv).append(content);
			// mozilla or opera browser						
			if ($j.browser != null && ($j.browser.mozilla != null || $j.browser.opera != null))
			{
				$j(this.searchDiv).parent('li').css({ paddingBottom: '4px'});
			}
			// other browsers
			else
			{
				$j(this.searchDiv).parent('li').css({ paddingBottom: '3px'});
			}
			
        },
		
		/**
		 * method which accepts the name fragment provided from the input field and
		 * calls the dwr service to retrieve companies
		 * 
		 * @param {String} nameFragment text entered into input field which should be used to search companies
		 * @param {Object} self reference to this object
		 */
		runCompAjax : function(nameFragment, self)
		{					
			// navigation keys have been pressed so set value back to false and skip ajax loading
			if (keyPress == true)
			{
				keyPress = false;
			}
			else
			{
				// another search is in use
				if ( ($j( $j(this.searchDiv).find('> div div:first a') ).length < 1) && ($j('div.' + this.options.resultsDiv + ' > a').length > 0) )
				{
					// reset input field
					$j( $j(this.searchDiv).find('input[type="text"]:first') ).attr('value', '');
					// alert user that another email plugin is in use
					$j.prompt(i18n.t("core.utilities:searchplugins.anotherPluginIsInUse", "Another plugin is in use, please close before continuing!"));
					// exit method
					return false;
				}																
				// remove all previously displayed companies
				$j( $j(this.searchDiv).find('> div div:first') ).empty();
				// display the company results container
				$j( $j(this.searchDiv).find('> div div') ).css({zIndex: 100, display: 'block'});																	
				// this span displays the loading image and text when a search has been activated
				$j( $j(this.searchDiv).find('> div div:first') ).append('<div class="loading-plug">' +
																			'<img src="img/icons/loading-anim.gif" class="img_marg_bot" height="18" width="80" alt="" />' +
																		'</div>');				
				// get current timestamp to pass with ajax results
				var currentTime = new Date().getTime();
				// only execute the ajax functionality if the text entered has a length
				// greater than zero (stops unconstrained retrieval)
				if (nameFragment.length > 1)
				{
					var coroles = this.options.coroles;
					var deactivated = this.options.deactivated;
					// load the service javascript file if not already
					loadScript.loadScriptsDynamic(new Array('dwr/interface/companyservice.js'), true,
					{
						callback: function()
						{
							// call dwr service to retrieve companies
							companyservice.searchCompanyByRolesHQL(nameFragment, true, deactivated, coroles,
							{
								callback:function(comps)
								{
									// call method to display companies returned
									self.compsCallback(comps, currentTime, self);
								}
							});
						}
					});
				}
				// text entered has a length of 0
				else
				{														
				 	// hide the company results container
					$j( $j(this.searchDiv).find('> div div') ).css({zIndex: -1, display: 'none'});
					// remove all previously displayed companies
					$j( $j(this.searchDiv).find('> div div:first') ).empty();
					// clear compid hidden input field
					$j( $j(this.searchDiv).find('> div input[type="hidden"]') ).val('');				
				}
			}
			
		},
		
		/**
		 * this method appends all companies returned from the dwr service to the results div
		 * 
		 * @param {Array} comps is an array of companies returned from the dwr service
		 * @param {Date/Time} currentUpdate is a timestamp retrieved when the search was initiated
		 * @param {Object} self reference to this object
		 */
		compsCallback : function(comps, currentUpdate, self) 
		{
			// this is a new search
			if (currentUpdate > lastUpdate)
			{
				// update value in the 'lastUpdate' variable
				lastUpdate = currentUpdate;
				
				// companies length is greater than zero
				if(comps.length > 0)
				{
					// remove all previously displayed companies
					$j( $j(this.searchDiv).find('> div div:first') ).empty();
					// declare variable to hold results content
					var content = '';
					// add anchor for each company returned to results variable
					$j(comps).each(function(i)
					{	
						content += '<a href="#" id="' + comps[i].coid + '" ';
						if (comps[i].blocked)
						{								
							content += 'class="compBlocked" title="' + i18n.t("core.utilities:searchplugins.compIsNotAccredited", "Company is not accredited") + '"';
						}	
						content += '><span>' + comps[i].coname + '</span></a>';																																			
					});
					// append results variable to results div
					$j( $j(this.searchDiv).find('> div div:first') ).append(content);
					
					// loop through all anchors appended to results div
					$j( $j(this.searchDiv).find('> div div:first a') ).each(function(i)
					{
						// apply click event to anchor
						$j( $j(this) ).click( function(){ self.selectCompId(this.id, $j(this).text(), self); return false; } );
					});			
					// add 'selected' class name to first anchor in results div
					$j( $j(this.searchDiv).find('> div div:first a:first') ).addClass('selected');
					// initialise the selected variable to zero
					selected = 0;																						
				}
				else
				{
					// remove all previously displayed companies
					$j( $j(this.searchDiv).find('> div div:first') ).empty().append('<div class="center bold">' + i18n.t("core.utilities:searchplugins.noCompaniesFound", "No Companies Found") + '</div>');
				}
			}
		},
		
		/**
		 * this method selects the chosen company and adds the name to the text input field and adds the
		 * coid to the hidden input field after which the results div is emptied and hidden
		 * 
		 * @param {Integer} coid id of the company chosen
		 * @param {String} coname name of the company chosen
		 * @param {Object} self reference to this object
		 */
		selectCompId : function (coid, coname, self)
		{
			// add company id to the hidden input field
			$j( $j(this.searchDiv).find('> div input[type="hidden"]') ).val(coid);
			// add company text to the search text input field
			$j( $j(this.searchDiv).find('> input[type="text"]:first') ).val(coname);
			// empty the results div
			$j( $j(this.searchDiv).find('> div div:first') ).empty();
			// hide the search results box and footer
			$j( $j(this.searchDiv).find('> div div') ).css({zIndex: -1, display: 'none'});
			// trigger a defaultly named method to perform action on page when new company id selected
			if (this.options.triggerMethod == 'true') 
			{
				// call this method which can be defined in page specific javascript file
				companySearchJQPluginMethod();
			}
		},
	
		/**
		 * this function closes the search plugin and clears all inputs
		 */				
		closePlugin : function()
		{
			// clear the text input field and apply focus 
			$j( $j(this.searchDiv).find('input[type="text"]:first') ).attr('value', '').focus();
			// clear company id in the hidden input field
			$j( $j(this.searchDiv).find('> div input[type="hidden"]') ).val('');
			// empty the results div
			$j( $j(this.searchDiv).find('> div div:first') ).empty();
			// hide the search results box and footer
			$j( $j(this.searchDiv).find('> div div') ).css({zIndex: -1, display: 'none'});
		},
		
		/**
		 * this method copies the values selected in this plugin instance to any others
		 * present on the page.
		 *
		 * @param {Object} element element which we can obtain value and name for copying
		 * @param {String} direction the direction in which the value should be copied
		 */
		copyOption : function (element, direction)
		{
			// add marker after element
			$j(element).after('<span class="marker"></span>');
			// copy information down the list
			if (direction == 'down')
			{
				// variable to indicate the marker has been found
				var downMarkerFound = false;
				// loop through all matching inputs
				$j('input[name="' + $j(element).attr('name') + '"]').each(function()
				{
					// marker found?
					if ($j(this).next().hasClass('marker'))
					{
						// set variable and remove marker
						downMarkerFound = true;
						$j(this).next().remove();
					}
					// marker found?
					if (downMarkerFound)
					{
						// copy value to input
						$j(this).val($j(element).val());
					}					
				});
			}
			else
			{
				// create an array of input objects
				var inputArray = $j.makeArray($j('input[name="' + $j(element).attr('name') + '"]'));
				// reverse the array
    			inputArray.reverse();
				// variable to indicate the marker has been found
				var upMarkerFound = false;
				// loop through all matching inputs
				$j.each(inputArray, function()
				{
					// marker found?
					if ($j(this).next().hasClass('marker'))
					{
						// set variable and remove marker
						upMarkerFound = true;
						$j(this).next().remove();
					}
					// marker found?
					if (upMarkerFound)
					{
						// copy value to input
						$j(this).val($j(element).val());
					}					
				});
			}
		}
        
    });
})(jQuery);