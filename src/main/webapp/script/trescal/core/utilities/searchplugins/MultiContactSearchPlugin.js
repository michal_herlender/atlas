/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	MultiContactSearchPlugin.js
*	DESCRIPTION		:	Javascript file imported when creating a multiple input contact search on a page
* 					:	Code creates four input fields, three for searching contacts (company, subdiv, contact)
* 					:	and the other for holding the personid of a	selected contact, a results section is then 
* 					:	created below to display contacts returned.	Further code uses dwr to get contact results 
* 					:	and display them on the page.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	File Created - SH - 14/09/2007
*					:	30/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/
 
 								/**
	 							 * this literal is called when the page loads if an element with the id of 'contSearchPlugin' is found on the page.
								 * this function then loads all the necessary scripts it needs.
								 *
	 							 */
	MultiContactSearchPlugin =  {		init:	function() 	
												{
													// scripts to load
													var Scripts = new Array (	
																				'dwr/interface/contactservice.js' );
													
													// load the common javascript files for this plugin
													loadScript.loadScriptsDynamic(Scripts, true);
													
													// set coroles to be searched over
													MultiContactSearchPlugin.setCoroles();
													// get business company ID
													MultiContactSearchPlugin.setBusinessCoId();
													// draw normal search plugin
													MultiContactSearchPlugin.drawSearch();
													
													// input field which supplies page name to redirect to is present?
													if ($j('input#anchorRedirect').length)
													{
														// add value of input field to literal
														MultiContactSearchPlugin.anchorRedirect = $j('input#anchorRedirect').val();
													}
													// input field which indicates that a custom function should be used is present?
													if ($j('input#customFunction').length)
													{
														// value is true so set literal to true also
														if ($j('input#customFunction').val() == 'true')
														{
															MultiContactSearchPlugin.customFunction = true;
														}
													}
																													
												},
								/**
								 * if the input with an id of 'multiContCoroles' is present and contains coroles separated by comma's then
								 * add these values to the coroles literal array. If not present then add default 'client' corole.
								 */
								setCoroles: 	function() 	
												{
													if ($j('input#multiContCoroles').length)
													{
														$j('input#multiContCoroles').each( function()
														{
															MultiContactSearchPlugin.coroles = this.value.split(',');
														});
													}
													else
													{
														MultiContactSearchPlugin.coroles = 'CLIENT';
													}
												},
								/**
								 * Set bussinessCoId from hidden input on page if present
								 */
								setBusinessCoId:   	function()
													{
														if ($j('input#businessCoId').length)
														{
															MultiContactSearchPlugin.businessCoId = $j('input#businessCoId').val();
														}
														else
														{
															MultiContactSearchPlugin.businessCoId = 0;
														}
													},
													
								/**
								 * The results will be restricted to companies active for this business company ID
								 */
								businessCoId:			0,
								
								/**
								 * array of company roles sourced by setCoroles
								 */
								coroles:				[],
								/**
								 * form name to include in anchor when submitting
								 */
								anchorRedirect: 		'',
								/**
								 * allows the user to write their own custom function in the page specific javascript file.
								 * This function must be named 'customFunction' and will be passed default parameters of
								 * personid, name, subname and coname.
								 */
								customFunction: 		false,								
								/**
								 * function to trigger when contact selected
								 */
								triggerFunction:		'',
								/**
								 * id of the company name input field
								 */
								compInput:				'companyName',
								/**
								 * id of the subdivision name input field
								 */
								subdivInput:			'subdivName',
								/**
								 * id of the contact name input field
								 */
								contactInput:			'contactName',
								/**
								 * id of the hidden input field used to pass the contact personid
								 */
								idInput:				'personid',
								/**
								 * id of the main div declared in the page where the search should be created
								 */
								multiContBoxDiv: 		'multiContSearchPlugin',
								/**
								 * id of the div in which the headings for the search are created
								 */
								multiContHeadDiv:		'pluginHeader',
								/**
								 * id of the div in which the search results are to be displayed
								 */
								resultsDiv: 			'multiContPlugResults',
								/**
								 * class name used on the results div to apply styles
								 */
								styleResDiv:			'pluginResults',
								/**
								 * gets the parent form of the firstName input field which is used by key navigation when submitting
								 */
								searchForm: 	$j('input#personid').each( function() 
																	{
     																	return this.form;
																	}),
								/**
								 * id of the form element on which focus should be placed
								 */
								inputFocus:				'',
								/**
								 * boolean value used to flag if a navigation cursor key has been pressed so no ajax is called
								 */
								keyPress:				false,
								/**
								 * value of the currently selected element in the list
								 */
								selected:				0,
								/**
								 * time value of last DWR call, checked against the current DWR call
								 */
								lastUpdate:				0,
								/**
								 * Keeps track of the current plugin results sort order
								 */
								currentSearchOrder: 	'company',
								/**
								 * creates the input fields and results div on the page for a normal contact search plugin
								 */			
								drawSearch:		function()	
												{
													$j('#' + MultiContactSearchPlugin.multiContBoxDiv).append('<input type="hidden" id="' + MultiContactSearchPlugin.idInput + '" name="' + MultiContactSearchPlugin.idInput + '" />' +
															'<fieldset>' +
																'<legend>' +i18n.t("core.utilities:searchplugins.contactSearch", "Contact Search")+' <a href="#" onclick="MultiContactSearchPlugin.resetForm(); return false; " class="mainlink">(' + i18n.t("core.utilities:searchplugins.refreshSearch", "Refresh Search") + ')</a></legend>' +
																'<ol id="multiContact">' +
																	'<li>' +
																		'<label for="companyName">' + i18n.t("companyLabel", "Company:") + '</label>' +
																		// create and append company input in which the user will search for contacts
																		'<input type="text" name="' + MultiContactSearchPlugin.compInput + '" id="' + MultiContactSearchPlugin.compInput + '" onkeyup=" MultiContactSearchPlugin.runMultiContactAjax(\'company\'); return false; " autocomplete="off" />' +
																		// this span displays the loading image and text when a search has been activated
																		'<span id="loading-comp" class="hid">' +
																			'<img src="img/icons/loading-anim.gif" class="img_marg_bot" height="18" width="80" alt="" />' +
																		'</span>' +
																	'</li>' +
																	'<li>' +
																		'<label for="subdivName">' + i18n.t("subdivisionLabel", "Subdivision:") + '</label>' +
																		// create and append subdivision input in which the user will search for contacts
																		'<input type="text" name="' + MultiContactSearchPlugin.subdivInput + '" id="' + MultiContactSearchPlugin.subdivInput + '" onkeyup=" MultiContactSearchPlugin.runMultiContactAjax(\'subdiv\'); return false; " autocomplete="off" />' +
																		// this span displays the loading image and text when a search has been activated
																		'<span id="loading-sub" class="hid">' +
																			'<img src="img/icons/loading-anim.gif" class="img_marg_bot" height="18" width="80" alt="" />' +
																		'</span>' +
																	'</li>' +
																	'<li>' +
																		'<label for="contactName">' + i18n.t("contactLabel", "Contact:") + '</label>' +
																		// create and append contact input in which the user will search for contacts
																		'<input type="text" name="' + MultiContactSearchPlugin.contactInput + '" id="' + MultiContactSearchPlugin.contactInput + '" onkeyup=" MultiContactSearchPlugin.runMultiContactAjax(\'contact\'); return false; " autocomplete="off" />' +
																		// this span displays the loading image and text when a search has been activated
																		'<span id="loading-cont" class="hid">' +
																			'<img src="img/icons/loading-anim.gif" class="img_marg_bot" height="18" width="80" alt="" />' +
																		'</span>' +
																	'</li>' +
																	'<li>' +
																		'<label class="twolabel" for="sortOrder">' + i18n.t("core.utilities:searchplugins.orderByLabel", "Order By:") + '</label>' +
																		// create and append contact input in which the user will search for contacts
																		'<input type="radio" name="searchOrder" value="company" onmouseup=" MultiContactSearchPlugin.changeSearchOrder(this.value); return false; " checked="checked" /> ' + i18n.t("company", "Company") + '<br />' +
																		'<input type="radio" name="searchOrder" value="contact" onmouseup=" MultiContactSearchPlugin.changeSearchOrder(this.value); return false; " /> ' + i18n.t("contact", "Contact") +
																	'</li>' +
																'</ol>' +
																'<div>' +
																	// create and append the results header div and span
																	'<div class="' + MultiContactSearchPlugin.multiContHeadDiv + '">' +
																		'<span class="multiContHead">' + i18n.t("contact", "Contact") + '</span>' +
																		'<span class="multiSubHead">' + i18n.t("subdivision", "Subdivision") + '</span>' +
																		'<span class="multiCompHead">' + i18n.t("company", "Company") + '</span>' +
																	'</div>' +
																	// create and append the div in which company results will be displayed
																	'<div id="' + MultiContactSearchPlugin.resultsDiv + '" class="' + MultiContactSearchPlugin.styleResDiv + '">' +
																		// This div is used to diplay the ajax results
																	'</div>' +
																'</div>' +
															'</fieldset>');
												},
																																				
								/**
								 * This function accepts a value entered by the user and calls an ajax method to retrieve
								 * contacts matching this value from the server.  The returned data is then passed on to function
								 * (contactCallback) for displaying on the page.
								 * @param firstName - Value entered in the first name text field.
								 * @param lastName - Value entered in the last name text field.
								 */			
								runMultiContactAjax: 	function (field)
														{
															// populate the searchFOCUS variable with this 'MultiContactSearchPlugin' object
															// for use when cursor key navigation is being used
															searchFOCUS = MultiContactSearchPlugin;
															
															// the up/down arrow key has been pressed so set value back to false and skip ajax loading
															if (MultiContactSearchPlugin.keyPress == true)
															{
																MultiContactSearchPlugin.keyPress = false;
															}
															else
															{
																// function to make the loading image and text visible/hidden for (comp, subdiv, cont)
																MultiContactSearchPlugin.checkLoading(field, 'vis');
																
																// get value from the company input field
																var compname = $j('#' + MultiContactSearchPlugin.compInput).val();
																// get value from the subdivision input field
																var subname = $j('#' + MultiContactSearchPlugin.subdivInput).val();
																// get value from the contact input field
																var contname = $j('#' + MultiContactSearchPlugin.contactInput).val();
																
																// get current timestamp to pass with ajax results
																var currentTime = new Date().getTime();
																
																// one of the input fields has had a value entered
																if ((compname.length + subname.length + contname.length) > 1)
																{																		
																	contactservice.searchContactsHQL(contname, subname, compname, MultiContactSearchPlugin.coroles, MultiContactSearchPlugin.currentSearchOrder, MultiContactSearchPlugin.businessCoId,
																	{
																		callback:function(dataFromServer)
																		{
																			MultiContactSearchPlugin.multiContactCallback(dataFromServer, currentTime, field);
																		}
																	});
																}
																else
																{
																	// remove all previously displayed contacts
																	$j('#' + MultiContactSearchPlugin.resultsDiv).empty();
																	// clear value in the hidden personid field
																	$j('#' + MultiContactSearchPlugin.idInput).attr('value', '');
																	
																	// function to make the loading image and text visible/hidden for (comp, subdiv, cont)
																	MultiContactSearchPlugin.checkLoading(field, 'hid');
													
																}
															}
														},
								
								/**
								 * this function displays the results returned from the server for ajax call 'runContactAjax' and 'runCascContactAjax'
								 * if this plugin is to be cascaded further by showing addresses
								 * search results then a new dwr search should be instantiated
								 * @param contacts - array of contact objects returned from the server.
								 * @param lastUpdate - time of data retrieval from the server.
								 * @param subdivid - subdivid used to get the contact results will also be needed to get the address results
								 */			
								multiContactCallback: 	function(contacts, currentUpdate, field)
														{
														
															// this is a new search
															if (currentUpdate > MultiContactSearchPlugin.lastUpdate)
															{
																// update value in the 'lastUpdate' literal
																MultiContactSearchPlugin.lastUpdate = currentUpdate;
																// create variable to hold contacts results
																var content = '';
																// contacts length is greater than zero																															
																if(contacts.length > 0)
																{
																	// remove all previously displayed contacts
																	$j('#' + MultiContactSearchPlugin.resultsDiv).empty();
																	
																	if (MultiContactSearchPlugin.anchorRedirect != '')
																	{
																		// add anchor for each contact returned
																		// the anchor href attribute should populated with the anchorRedirect value
																		$j.each(contacts, function(i){
																				
																			content = content + '<a href="' + MultiContactSearchPlugin.anchorRedirect + '.htm?personid=' + contacts[i].personid + '" id="' + contacts[i].personid + '" rel="anchorRedirect" style=" position: relative; ">' +
																									'<span class="multiContSpan">' + contacts[i].name + '</span>' +
																									'<span class="multiSubSpan">' + contacts[i].subname + '</span>' +
																									'<span class="multiCompSpan">' + contacts[i].coname + '</span>' +
																								'</a>';					
																		});
																	}
																	else if (MultiContactSearchPlugin.customFunction == true)
																	{
																		// add anchor for each contact returned
																		// an onclick function called 'customFunction' is added which passes default info (personid, name, subdiv, coname)
																		// so the user can create their own custom function in the page specific javascript file.
																		$j.each(contacts, function(i){
																				
																			content = content + '<a href="#" id="' + contacts[i].personid + '" onclick=" MultiContactSearchPlugin.linkOnclick(' + contacts[i].personid + '); customFunc(\'' + contacts[i].name + '\', ' + contacts[i].personid + ', \'' + contacts[i].subname + '\', \'' + contacts[i].coname + '\'); return false; " rel="customFunc" style=" position: relative; ">' +
																									'<span class="multiContSpan">' + contacts[i].name + '</span>' +
																									'<span class="multiSubSpan">' + contacts[i].subname + '</span>' +
																									'<span class="multiCompSpan">' + contacts[i].coname + '</span>' +
																								'</a>';					
																		});
																	}
																	else
																	{
																		// add anchor for each contact returned
																		// the usual default MultiContactSearchPlugin.linkOnclick function is added to the onclick which just changes focus and sets personid
																		// in the hidden input field 
																		$j.each(contacts, function(i){
																				
																			content = content + '<a href="#" id="' + contacts[i].personid + '" onclick=" MultiContactSearchPlugin.linkOnclick(' + contacts[i].personid + ');  return false; " rel="standard" style=" position: relative; ">' +
																									'<span class="multiContSpan">' + contacts[i].name + '</span>' +
																									'<span class="multiSubSpan">' + contacts[i].subname + '</span>' +
																									'<span class="multiCompSpan">' + contacts[i].coname + '</span>' +
																								'</a>';					
																		});
																	}
																	// append all contacts to list
																	$j('#' + MultiContactSearchPlugin.resultsDiv).append(content);																																									
																	// populate the personid hidden input field with the current personid
																	$j('#' + MultiContactSearchPlugin.idInput).attr('value', contacts[0].personid);
																	// add 'selected' class name to first anchor
																	$j('#' + MultiContactSearchPlugin.resultsDiv + ' a:first').addClass('selected');
																	// custom function supplied?
																	if (MultiContactSearchPlugin.customFunction == true)
																	{
																		// trigger onclick event of first anchor
																		$j('#' + MultiContactSearchPlugin.resultsDiv + ' a:first').trigger('onclick');
																	}
																	// initialise the selected literal to zero
																	MultiContactSearchPlugin.selected = 0;
																	
																	// function to make the loading image and text visible/hidden for (comp, subdiv, cont)
																	MultiContactSearchPlugin.checkLoading(field, 'hid');
																																																					
																}
																else
																{
																	// remove all previously displayed contacts
																	$j('#' + MultiContactSearchPlugin.resultsDiv).empty();
																	// clear value in the hidded personid field
																	$j('#' + MultiContactSearchPlugin.idInput).attr('value', '');
																	// create div to display message when no contacts have been returned
																	$j('#' + MultiContactSearchPlugin.resultsDiv).append('<div class="bold center" style=" padding: 5px; ">' + i18n.t("core.utilities:searchplugins.noContactsFound", "No Contacts Found") + '</div>');
																	
																	// function to make the loading image and text visible/hidden for (comp, subdiv, cont)
																	MultiContactSearchPlugin.checkLoading(field, 'hid');
																}	
															}
														},
															
								/**
								 *
								 *	This function removes all results from the contactlist div (id=contactlist) and then
								 *	clears all values in the input search boxes.
								 * 
								 */
								resetForm:		function()
												{
													$j('#' + MultiContactSearchPlugin.resultsDiv).empty();
													$j('#' + MultiContactSearchPlugin.compInput).attr('value', '').focus();
													$j('#' + MultiContactSearchPlugin.subdivInput).attr('value', '');
													$j('#' + MultiContactSearchPlugin.contactInput).attr('value', '');
													$j('#' + MultiContactSearchPlugin.idInput).attr('value', '');
												},
								
								/**
								 *
								 * This function checks the 'field' value passed which is the last input field used by the user and
								 * then sets the visibility of the loading span to either visible or hidden depending on the value passed
								 * in 'visibility'.
								 * @param (field) string value identifying the input field used by the user (i.e. 'company', 'contact').
								 * @param (visibility) class that you want to assign the loading span ('vis' or 'hid').
								 * 
								 */
								checkLoading:	function(field, visibility)
												{
													switch (field)
													{
														case 'company':	$j('#loading-comp').removeClass().addClass(visibility);
																		break;
																		
														case 'subdiv':	$j('#loading-sub').removeClass().addClass(visibility);
																		break;
																	
														case 'contact':	$j('#loading-cont').removeClass().addClass(visibility);
																		break;
																		
														case 'order':	$j('#loading-comp').removeClass().addClass(visibility);
																		$j('#loading-sub').removeClass().addClass(visibility);
																		$j('#loading-cont').removeClass().addClass(visibility);
																		break;
													}
												},
								
								/**
								 * this function moves the 'selected' classname to an anchor which the user has clicked on and populates the hidden input
								 * field with the selected contacts personid.
								 * @param personid - id of the address being clicked
								 */
								linkOnclick: 	function(personid)
												{
													// populate the personid hidden input field with the id of this anchor (ie personid)
													$j('#' + MultiContactSearchPlugin.idInput).attr('value', personid);
													// remove the 'selected' classname from the currently selected item
													$j('#' + MultiContactSearchPlugin.resultsDiv + ' a').eq(MultiContactSearchPlugin.selected).removeClass();
													$j('#' + MultiContactSearchPlugin.resultsDiv + ' a').each( function(i){
														// id of this anchor matches the personid passed
														if (this.id == personid)
														{
															// add 'selected' classname to this anchor
															this.className = 'selected';
															// populate the selected literal with this value
															MultiContactSearchPlugin.selected = i;
														}														
													});
													
													// populate 'searchFOCUS' with the 'ContactSearchPlugin' object
													// for use when cursor key navigation is being used
													searchFOCUS = MultiContactSearchPlugin;
													
													return false;
												},
														
								/**
								 * this method changes the order of the results from company/subdiv/contact to just contact or vice-versa.
								 * 
								 * @param {String} value 'company' to indicate order by company or 'contact' to order by contact 
								 */							
								changeSearchOrder: 	function(value)
													{
														if (value == MultiContactSearchPlugin.currentSearchOrder)
														{
															// do nothing as this is the current search order
														}
														else
														{
															MultiContactSearchPlugin.currentSearchOrder = value;
															MultiContactSearchPlugin.runMultiContactAjax('order');
														}	
													}																						
								};
