/****************************************************************************************************************************************
 *                                                CWMS (Calibration Workflow Management System)
 *
 *                                                Copyright 2006, Antech Calibration Services
 *                                                            www.antech.org.uk
 *
 *                                                            All rights reserved
 *
 *                                                        Document author:    Stuart Harrold
 *
 *    FILENAME        :    SubdivSearchPlugin.js
 *    DESCRIPTION        :    Javascript file imported as part of a cascading search plugin
 *                    :    Code creates an input field for holding the subdivid of a selected subdivision,
 *                    :    and a results section is then created below to display subdivisions returned.
 *                    :    Further code uses dwr to get subdivision results and display them on the page
 *    DEPENDENCIES    :   -
 *
 *    TO-DO            :    -
 *    KNOWN ISSUES    :    -
 *    HISTORY            :    File Created - SH - 25/07/2007
 *                    :    30/10/2015 - TProvost - Manage i18n
 ****************************************************************************************************************************************/

/**
 * this literal is called when the page loads if an element with the id of 'subdivSearchPlugin' is found on the page.
 * this function then loads all the necessary scripts it needs and checks whether the search should be cascaded
 * further than this point? The cascade search literal is then populated with the value passed in cascadeSearch
 * and the drawSearch() function is called to create the search box.
 *
 * @param {Boolean} cascade is a boolean value to indicate whether this search should be cascaded further or stop at this point
 * @param {Array} cascadeSearch is an array passed from CascadeSearchPlugin object to indicate what (if any) search should be performed
 *                    or displayed after this (i.e. search for contacts or addresses?).
 */
SubdivSearchPlugin = {
    init: function (cascade, cascadeSearch) {
        // scripts to load
        var Scripts = new Array("script/thirdparty/jQuery/jquery.scrollTo.js");

        // load the common javascript files for this plugin
        loadScript.loadScripts(Scripts);

        // set to true if this plugin is to be cascaded further
        if (cascade == true) {
            SubdivSearchPlugin.cascade = true;
        }
        // cascade search not empty?
        if (cascadeSearch != "") {
            // add array values to the SubdivSearchPlugin literal
            SubdivSearchPlugin.cascadeSearch = cascadeSearch;
        }
        // draw search plugin
        SubdivSearchPlugin.drawSearch();
    },
    /**
     * false by default, if true then this cascading search should continue and use the selected subdivision subdivid to
     * call a dwr search for either contact or address results depending on the values held in the cascadeSearch literal?
     */
    cascade: false,
    /**
     * array to hold values passed from the CascadeSearchPlugin object to indicate what (if any) search should be performed
     * or displayed after this (i.e. search for contacts or addresses?)
     */
    cascadeSearch: [],
    /**
     * id of the hidden input field used to pass the subdivision id
     */
    idInput: "subdivid",
    /**
     * id of the main div declared in the page where the search should be created
     */
    boxDiv: "subdivSearchPlugin",
    /**
     * id of the div in which the headings for the search are created
     */
    headDiv: "pluginHead",
    /**
     * id of the div in which the search results are to be displayed
     */
    resultsDiv: "subdivPlugResults",
    /**
     * class name used on the results div to apply styles
     */
    styleResDiv: "pluginResults",
    /**
     * value of the currently selected element in the list
     */
    selected: 0,
    /**
     * time value to check the current ajax search
     */
    NOW: 0,
    /**
     * creates the input field and results div on the page
     */
    drawSearch: function () {
        $j("#" + SubdivSearchPlugin.boxDiv).append(
            '<input type="hidden" id="' +
            SubdivSearchPlugin.idInput +
            '" name="' +
            SubdivSearchPlugin.idInput +
            '" />' +
            '<div id="subdiv-container">' +
            // create and append the header div
            '<div class="' +
            SubdivSearchPlugin.headDiv +
            '">' +
            i18n.t("core.utilities:searchplugins.matchingSubdivisions", "Matching Subdivisions") +
            "</div>" +
            // create and append the div in which company results will be displayed
            '<div id="' +
            SubdivSearchPlugin.resultsDiv +
            '" class="' +
            SubdivSearchPlugin.styleResDiv +
            '">' +
            // subdivision results go here
            "</div>" +
            "</div>"
        );
    },
    /**
     * this function accepts a coid passed and calls an ajax method to retrieve subdivisions which match this value
     * from the server.  The returned data is then passed on to function (subdivCallback) for displaying on the page.
     *
     * @param {Integer} coid is passed to this literal from a company search literal.
     */
    runSubdivAjax: function (coid) {
        if (SubdivSearchPlugin.cascade == false) {
            // display the subdivision results container
            $j("#" + SubdivSearchPlugin.resultsDiv + ", div#subdiv-container ." + SubdivSearchPlugin.headDiv).css({
                zIndex: 100,
                display: "block"
            });
        }

        // remove all previously displayed subdivisions
        $j("#" + SubdivSearchPlugin.resultsDiv).empty();

        // this span displays the loading image and text when a search has been activated
        $j("#" + SubdivSearchPlugin.resultsDiv).append(
            '<div class="loading-plug">' + '<img src="img/icons/loading-anim.gif" class="img_marg_bot" height="18" width="80" alt="" />' + "</div>"
        );

        // only execute the following code if a coid has been passed
        if (coid != "") {
            // get current timestamp to pass with ajax results
            var currentTime = new Date().getTime();
            // load the service javascript file if not already
            loadScript.loadScriptsDynamic(new Array("dwr/interface/subdivservice.js"), true, {
                callback: function () {
                    // call subdiv dwr function using the company coid
                    subdivservice.getAllActiveCompanySubdivsHQL(coid, {
                        callback: function (dataFromServer) {
                            SubdivSearchPlugin.subdivCallback(dataFromServer, currentTime);
                        },
                    });
                },
            });
            // cascade plugin?
            if (typeof CascadeSearchPlugin != "undefined") {
                // subdiv link present?
                if ($j("#" + CascadeSearchPlugin.linksDiv + " span#cascAddSub").length) {
                    // change href attribute content
                    $j("#" + CascadeSearchPlugin.linksDiv + " span#cascAddSub a").attr("href", springUrl + "/addsubdiv.htm?coid=" + coid);
                } else {
                    // append link for adding subdivision
                    $j("#" + CascadeSearchPlugin.linksDiv).append(
                        '<span id="cascAddSub">&nbsp;-&nbsp;<a href="' +
                        springUrl +
                        "/addsubdiv.htm?coid=" +
                        coid +
                        '" target="_blank" class="mainlink">' +
                        i18n.t("core.utilities:searchplugins.addSubdiv", "Add Subdiv") +
                        "</a></span>"
                    );
                }
            }
            // check to see if called off and collected items should be loaded
            if (CascadeSearchPlugin.searchItems == true) {
                // coid is not blank
                if (coid != "") {
                    // load the service javascript file if not already
                    loadScript.loadScriptsDynamic(new Array("dwr/interface/calloffitemservice.js", "dwr/interface/collectedinstrumentservice.js"), true, {
                        callback: function () {
                            // call dwr service to get all called off items
                            calloffitemservice.getCallOffItemsForCompany(coid, true, {
                                callback: function (items) {
                                    // items returned?
                                    if (items.length > 0) {
                                        // empty the table body
                                        $j("table#itemCOResults tbody").empty();
                                        // initialise content variable
                                        var itemContent = "";
                                        // add table row for each item returned
                                        $j.each(items, function (i) {
                                            // add rows to string variable
                                            itemContent =
                                                itemContent +
                                                "<tr>" +
                                                '<td class="center">' +
                                                '<input style=" width: auto; " type="checkbox" name="selectedCallOffItems" value="' +
                                                items[i].id +
                                                '">' +
                                                "</td>" +
                                                "<td>" +
                                                items[i].offItem.inst.plantid +
                                                "</td>" +
                                                "<td>" +
                                                items[i].offItem.inst.model.mfr.name +
                                                " " +
                                                items[i].offItem.inst.model.model +
                                                " " +
                                                items[i].offItem.inst.model.description.description +
                                                "</td>" +
                                                "<td>" +
                                                items[i].offItem.inst.con.sub.subname +
                                                "</td>" +
                                                "<td>" +
                                                "(" +
                                                i18n.t("core.utilities:searchplugins.previousItemOnJob", {
                                                    varItemNo: items[i].offItem.itemNo,
                                                    varJobNo: items[i].offItem.job.jobno,
                                                    defaultValue: "Previously item: " + items[i].offItem.itemNo + " on Job " + items[i].offItem.job.jobno,
                                                }) +
                                                ")" +
                                                "</td>" +
                                                "</tr>";
                                        });
                                        // show the table head section
                                        $j("table#itemCOResults thead").removeClass().addClass("vis");
                                        // append new rows to table
                                        $j("table#itemCOResults tbody").append(itemContent);
                                    } else {
                                        // empty the table body
                                        $j("table#itemCOResults tbody").empty();
                                        // hide the table head section
                                        $j("table#itemCOResults thead").removeClass().addClass("hid");
                                        // append message of no called off items
                                        $j("table#itemCOResults tbody").append(
                                            "<tr>" + '<td class="bold center">' + i18n.t("core.utilities:searchplugins.noCollectedItems", "No Called Off Items") + "</td>" + "</tr>"
                                        );
                                    }
                                },
                            });
                            // call dwr service to get all collected items
                            collectedinstrumentservice.getCollectedInstrumentsForCompany(coid, {
                                callback: function (insts) {
                                    // items returned?
                                    if (insts.length > 0) {
                                        // empty the table body
                                        $j("table#itemCResults tbody").empty();
                                        // initialise content variable
                                        var instContent = "";
                                        // add table row for each item returned
                                        $j.each(insts, function (i) {
                                            // add rows to string variable
                                            instContent =
                                                instContent +
                                                "<tr>" +
                                                '<td class="center">' +
                                                '<input style=" width: auto; " type="checkbox" name="selectedCollectedInsts" value="' +
                                                insts[i].id +
                                                '">' +
                                                "</td>" +
                                                "<td>" +
                                                insts[i].inst.plantid +
                                                "</td>" +
                                                "<td>" +
                                                insts[i].inst.model.mfr.name +
                                                " " +
                                                insts[i].inst.model.model +
                                                " " +
                                                insts[i].inst.model.description.description +
                                                "</td>" +
                                                "</tr>";
                                        });
                                        // show table head section
                                        $j("table#itemCResults thead").removeClass().addClass("vis");
                                        // append new rows to table
                                        $j("table#itemCResults tbody").append(instContent);
                                    } else {
                                        // empty the table body
                                        $j("table#itemCResults tbody").empty();
                                        // hide table head section
                                        $j("table#itemCResults thead").removeClass().addClass("hid");
                                        // append message of no collected items
                                        $j("table#itemCResults tbody").append(
                                            "<tr>" + '<td class="bold center">' + i18n.t("core.utilities:searchplugins.noCollectedItems", "No Collected Items") + "</td>" + "</tr>"
                                        );
                                    }
                                },
                            });
                        },
                    });
                }
            }
        } else {
            // remove all previously displayed subdivisions
            $j("#" + SubdivSearchPlugin.resultsDiv).empty();
            // clear value in the subdivid hidden input field
            $j("#" + SubdivSearchPlugin.idInput).attr("value", "");
            // hide the subdivision results container
            $j("#" + SubdivSearchPlugin.resultsDiv + ", div#subdiv-container ." + SubdivSearchPlugin.headDiv).css({
                zIndex: -1,
                display: "none"
            });
            // search is to be cascaded further
            if (SubdivSearchPlugin.cascade == true) {
                // search is to be cascaded further using one criteria
                if (SubdivSearchPlugin.cascadeSearch.length == 1) {
                    switch (SubdivSearchPlugin.cascadeSearch[0]) {
                        // criteria to be cascaded is 'address' so call ajax using first subdivid
                        case "address":
                            AddressSearchPlugin.runAddressAjax("");
                            break;
                        // criteria to be cascaded is 'contact' so call ajax using first subdivid
                        case "contact":
                            ContactSearchPlugin.runCascContactAjax("");
                            break;
                    }
                }
                // search is to be cascaded further using two criteria
                else {
                    // criteria to be cascaded is 'address' and 'contact' so call ajax using first subdivid
                    AddressSearchPlugin.runAddressAjax("");
                    ContactSearchPlugin.runCascContactAjax("");
                }
                // subdiv link present?
                if ($j("#" + CascadeSearchPlugin.linksDiv + " span#cascAddSub").length) {
                    // remove old add subdiv link
                    $j("#" + CascadeSearchPlugin.linksDiv + " span#cascAddSub").remove();
                }
            }
        }
    },
    /**
     * this function displays the results returned from the server for ajax call 'runSubdivAjax'
     * if this plugin is to be cascaded further by showing address or contact
     * search results then new dwr searches should be instantiated depending on the values passed in
     * the cascadeSearch array? (i.e. 'contact' or 'address' or both)
     *
     * @param {Array} subdivs is an array of subdivision objects returned from the server.
     * @param {Date/Time} currentUpdate is a date/time value of data retrieval from the server.
     */
    subdivCallback: function (subdivs, currentUpdate) {
        // this is a new search
        if (currentUpdate > SubdivSearchPlugin.NOW) {
            // update value in the 'NOW' literal
            SubdivSearchPlugin.NOW = currentUpdate;

            // subdivs length is greater than zero
            if (subdivs.length > 0) {
                // remove all previously displayed subdivisions
                $j("#" + SubdivSearchPlugin.resultsDiv).empty();
                // declare variable to hold subdiv results
                var content = "";
                // add anchor for each subdivision returned
                $j.each(subdivs, function (i) {
                    content =
                        content +
                        '<a href="#" id="' +
                        subdivs[i].subdivid +
                        '" onclick="SubdivSearchPlugin.linkOnclick(this.id); return false;"><span>' +
                        subdivs[i].subname +
                        "</span></a>";
                });
                // append content to results box
                $j("#" + SubdivSearchPlugin.resultsDiv).append(content);
                // this search is to be prefilled
                if (typeof CascadeSearchPlugin != "undefined" && CascadeSearchPlugin.prefillIds.length > 0 && CascadeSearchPlugin.prefillFinalised == false) {
                    // trigger the linkOnclick function using the subdivid value in 'prefillIds' array
                    // to select the chosen subdivision and load the necessary contacts and addresses
                    if (CascadeSearchPlugin.prefillIds[1] != "") {
                        SubdivSearchPlugin.linkOnclick(parseInt(CascadeSearchPlugin.prefillIds[1]));
                    } else {
                        SubdivSearchPlugin.linkOnclick(subdivs[0].subdivid);
                    }

                    // search is cascaded no further than subdivision so set finalised
                    // boolean variable to true
                    if (CascadeSearchPlugin.prefillIds.length == 2) {
                        CascadeSearchPlugin.prefillFinalised = true;
                    }
                } else {
                    if (CascadeSearchPlugin.autoselect) {
                        // populate the subdivid hidden input field with the current subdivid
                        $j("#" + SubdivSearchPlugin.idInput).attr("value", subdivs[0].subdivid);
                        // add 'selected' class name to first anchor
                        $j("#" + SubdivSearchPlugin.resultsDiv + " a:first").addClass("selected");
                        // initialise the selected literal to zero
                        SubdivSearchPlugin.selected = 0;
                        // search is to be cascaded further
                        if (SubdivSearchPlugin.cascade == true) {
                            // search is to be cascaded further using one criteria
                            if (SubdivSearchPlugin.cascadeSearch.length == 1) {
                                switch (SubdivSearchPlugin.cascadeSearch[0]) {
                                    // criteria to be cascaded is 'address' so call ajax using first subdivid
                                    case "address":
                                        AddressSearchPlugin.runAddressAjax(subdivs[0].subdivid);
                                        break;
                                    // criteria to be cascaded is 'contact' so call ajax using first subdivid
                                    case "contact":
                                        ContactSearchPlugin.runCascContactAjax(subdivs[0].subdivid);
                                        break;
                                }
                            }
                            // search is to be cascaded further using two criteria
                            else {
                                // criteria to be cascaded is 'address' and 'contact' so call ajax using first subdivid
                                AddressSearchPlugin.runAddressAjax(subdivs[0].subdivid);
                                ContactSearchPlugin.runCascContactAjax(subdivs[0].subdivid);
                            }
                        }
                    }
                }
            } else {
                // remove all previously displayed subdivisions
                $j("#" + SubdivSearchPlugin.resultsDiv).empty();
                // clear value in the subdivid hidden input field
                $j("#" + SubdivSearchPlugin.idInput).attr("value", "");
                // create a div to display that no results have been returned
                $j("#" + SubdivSearchPlugin.resultsDiv).append(
                    '<div class="bold center" style=" padding: 5px; ">' + i18n.t("core.utilities:searchplugins.noSubdivisionsFound", "No Subdivisions Found") + "</div>"
                );
            }
        }

        // execute callback function if exists
        if (CascadeSearchPlugin.subdivCallBackFunction != "") window[CascadeSearchPlugin.subdivCallBackFunction]();
    },
    /**
     * this function moves the 'selected' classname to an anchor which the user has clicked on and populates the hidden input
     * field with the selected subdivisions subdivid. If this plugin is to be cascaded further by showing address or contact
     * search results then new dwr searches should be instantiated depending on the values passed in the cascadeSearch array?
     * (i.e. 'contact' or 'address' or both)
     *
     * @param {Integer} subdivid is the id of the subdivision being clicked
     */
    linkOnclick: function (subdivid) {
        // populate the subdivid hidden input field with the id of this anchor (ie subdivid)
        $j("#" + SubdivSearchPlugin.idInput).attr("value", subdivid);
        // remove the 'selected' classname from the currently selected item
        $j("#" + SubdivSearchPlugin.resultsDiv + " a")
            .eq(SubdivSearchPlugin.selected)
            .removeClass();
        $j("#" + SubdivSearchPlugin.resultsDiv + " a").each(function (i) {
            // id of this anchor matches the subdivid passed
            if (this.id == subdivid) {
                // add 'selected' classname to this anchor
                this.className = "selected";
                // populate the selected literal with this value
                SubdivSearchPlugin.selected = i;

                // search is to be cascaded further
                if (SubdivSearchPlugin.cascade == true) {
                    // search is to be cascaded further using one criteria
                    if (SubdivSearchPlugin.cascadeSearch.length == 1) {
                        switch (SubdivSearchPlugin.cascadeSearch[0]) {
                            // criteria to be cascaded is 'address' so call ajax using first subdivid
                            case "address":
                                AddressSearchPlugin.runAddressAjax(subdivid);
                                break;
                            // criteria to be cascaded is 'contact' so call ajax using first subdivid
                            case "contact":
                                ContactSearchPlugin.runCascContactAjax(subdivid);
                                break;
                        }
                    }
                    // search is to be cascaded further using two criteria
                    else {
                        // criteria to be cascaded is 'address' and 'contact' so call ajax using first subdivid
                        AddressSearchPlugin.runAddressAjax(subdivid);
                        ContactSearchPlugin.runCascContactAjax(subdivid);
                    }
                }
            }
        });
        // scroll to the selected subdiv
        $j("#" + SubdivSearchPlugin.resultsDiv).scrollTo("a.selected", 0);

        // execute callback function if exists
        if (CascadeSearchPlugin.subdivCallBackFunction != "") window[CascadeSearchPlugin.subdivCallBackFunction]();

        // wait for any contact or address searches to be populated and then set
        // the finalised boolean variable to true
        if (typeof CascadeSearchPlugin != "undefined") {
            setTimeout("CascadeSearchPlugin.prefillFinalised = true;", 2000);
        }
        return false;
    },
};
