/****************************************************************************************************************************************
 *                                                CWMS (Calibration Workflow Management System)
 *
 *                                                Copyright 2006, Antech Calibration Services
 *                                                            www.antech.org.uk
 *
 *                                                            All rights reserved
 *
 *                                                        Document author:    Stuart Harrold
 *
 *    FILENAME        :    CascadeSearchPlugin.js
 *    DESCRIPTION        :    Javascript file imported when creating a Cascading Search Plugin (company/subdiv/address/contact) on a page
 *                    :    CWMS_Main.js looks for the inclusion of a div with the id of 'cascadeSearchPlugin' and if found this objects init() function is called
 *                    :    This object is then responsible for checking which searches should be included.  Separate plugins
 *                    :    for company/subdivision/address/contact are then used to build the cascading plugin accordingly.
 *    DEPENDENCIES    :   -
 *
 *    TO-DO            :    -
 *    KNOWN ISSUES    :    -
 *    HISTORY            :    File Created - SH - 09/08/2007
 *                    :    30/10/2015 - TProvost - Manage i18n
 ****************************************************************************************************************************************/

/*
 * this literal is called when the page loads if an element with the id of 'cascadeSearchPlugin' is found on the page,
 * it checks for values supplied in the hidden input field and calls the createRequired literal for each search
 * box required by the user
 */
CascadeSearchPlugin = {
    init: function () {
        // cascadeRules input field present in page?
        if ($j("input#cascadeRules").length) {
            // cascade rules have been supplied (i.e. address,contact)
            if ($j("input#cascadeRules").val() != "") {
                // reset cascade rules for changing cascade functionality
                CascadeSearchPlugin.cascadeRules = [];
                // convert string into array
                $j("input#cascadeRules").each(function () {
                    CascadeSearchPlugin.cascadeRules = this.value.split(",");
                });
            } else {
                CascadeSearchPlugin.cascadeRules = [];
            }
        }
        // prefillIds input field present in page?
        if ($j("input#prefillIds").length) {
            // prefill values have been supplied (i.e. coid,subdivid,addressid,personid)
            if ($j("input#prefillIds").val() != "") {
                // convert string into array
                $j("input#prefillIds").each(function () {
                    CascadeSearchPlugin.prefillIds = this.value.split(",");
                });
            }
        }
        // address type input field present in page?
        if ($j("input#addressType").length) {
            // address type input field value is empty?
            if ($j("input#addressType").val() != "") {
                // add value to literal
                CascadeSearchPlugin.addressType = $j("input#addressType").val();
            }
        }
        // searchItems input field present in page?
        if ($j("input#searchItems").length) {
            // search items value has been supplied
            if ($j("input#searchItems").val() == "true") {
                CascadeSearchPlugin.searchItems = true;
            }
        }
        // searchBPO input field present in page?
        if ($j("input#searchBPO").length) {
            // search bpo value has been supplied
            if ($j("input#searchBPO").val() == "true") {
                CascadeSearchPlugin.searchBPO = true;
            }
        }
        // no location input field present in page?
        if ($j("input#noLocation").length) {
            // no location value has been supplied
            if ($j("input#noLocation").val() == "true") {
                CascadeSearchPlugin.noLocation = true;
            }
        }
        // search locations input field present in page?
        if ($j("input#searchLocations").length) {
            // search locations value has been supplied
            if ($j("input#searchLocations").val() == "true") {
                CascadeSearchPlugin.searchLocations = true;
            }
        }
        // loadCurrency input field present in page?
        if ($j("input#loadCurrency").length) {
            // loadCurrency value has been supplied
            if ($j("input#loadCurrency").val() == "true") {
                CascadeSearchPlugin.loadCurrency = true;
            }
        }
        // customHeight input field present in page?
        if ($j("input#customHeight").length) {
            // customHeight value has been supplied
            if ($j("input#customHeight").val() != "") {
                CascadeSearchPlugin.customHeight = $j("input#customHeight").val();
            }
        }
        // source discount input field present in page?
        if ($j("input#sourceDiscount").length) {
            // source discount value has been supplied
            if ($j("input#sourceDiscount").val() == "true") {
                CascadeSearchPlugin.sourceDiscount = true;
            }
        }
        // source subdivCallBackFunction input field present in page?
        if ($j("input#subdivCallBackFunction").length) {
            // source subdivCallBackFunction value is not empty
            if ($j("input#subdivCallBackFunction").val() != "") {
                CascadeSearchPlugin.subdivCallBackFunction = $j("input#subdivCallBackFunction").val();
            }
        }
        // source contactCallBackFunction input field present in page?
        if ($j("input#contactCallBackFunction").length) {
            // source contactCallBackFunction value is not empty
            if ($j("input#contactCallBackFunction").val() != "") {
                CascadeSearchPlugin.contactCallBackFunction = $j("input#contactCallBackFunction").val();
            }
        }
        // source addressCallBackFunction input field present in page?
        if ($j("input#addressCallBackFunction").length) {
            // source callBackFunction value is not empty
            if ($j("input#addressCallBackFunction").val() != "") {
                CascadeSearchPlugin.addressCallBackFunction = $j("input#addressCallBackFunction").val();
            }
        }

        // source discount input field present in page?
        if ($j("input#sourceNominal").length) {
            // source discount value has been supplied
            if ($j("input#sourceNominal").val() == "true") {
                CascadeSearchPlugin.sourceNominal = true;
            }
        }
        if ($j("input#autoselect").length) {
            if ($j("input#autoselect").val() == "true") {
                CascadeSearchPlugin.autoselect = true;
            } else {
                CascadeSearchPlugin.autoselect = false;
            }
        }
        // set literal visible to true
        CascadeSearchPlugin.visible = true;

        // create the company search div
        CascadeSearchPlugin.createRequired("company", true, "", {
            callback: function () {
                if (CascadeSearchPlugin.cascadeRules.length == 0) {
                    CompanySearchPlugin.cascade = false;
                    CompanySearchPlugin.visible = true;
                    // create nothing but company search
                    // change height of plugin area used
                    $j("#" + CascadeSearchPlugin.pageDiv).css({height: "150px"});
                    // if this cascading search should be prefilled with selections then add value into the coname
                    // input field and perform a company search using the first value in 'prefillIds' array
                    if (CascadeSearchPlugin.prefillIds.length > 0) {
                        CompanySearchPlugin.runCompanyIdAjax(CascadeSearchPlugin.prefillIds[0]);
                    }
                    CascadeSearchPlugin.prefillFinalised = false;
                } else if (CascadeSearchPlugin.cascadeRules.length == 1) {
                    // create the subdivision search div
                    CascadeSearchPlugin.createRequired(CascadeSearchPlugin.cascadeRules[0], false, "", {
                        callback: function () {
                            // if this cascading search should be prefilled with selections then add value into the coname
                            // input field and perform a company search using the first value in 'prefillIds' array
                            if (CascadeSearchPlugin.prefillIds.length > 0) {
                                CompanySearchPlugin.runCompanyIdAjax(CascadeSearchPlugin.prefillIds[0]);
                            }
                            CascadeSearchPlugin.prefillFinalised = false;
                        },
                    });
                } else if (CascadeSearchPlugin.cascadeRules.length == 2) {
                    // create the subdivision search div and single cascade of either contact or address
                    CascadeSearchPlugin.createRequired(CascadeSearchPlugin.cascadeRules[0], true, new Array(CascadeSearchPlugin.cascadeRules[1]), {
                        callback: function () {
                            CascadeSearchPlugin.createRequired(CascadeSearchPlugin.cascadeRules[1], false, "", {
                                callback: function () {
                                    // if this cascading search should be prefilled with selections then add value into the coname
                                    // input field and perform a company search using the first value in 'prefillIds' array
                                    if (CascadeSearchPlugin.prefillIds.length > 0) {
                                        CompanySearchPlugin.runCompanyIdAjax(CascadeSearchPlugin.prefillIds[0]);
                                    }
                                    CascadeSearchPlugin.prefillFinalised = false;
                                },
                            });
                        },
                    });
                } else if (CascadeSearchPlugin.cascadeRules.length == 3) {
                    // create the subdivision search div and cascade both the contact and address search divs
                    CascadeSearchPlugin.createRequired(
                        CascadeSearchPlugin.cascadeRules[0],
                        true,
                        new Array(CascadeSearchPlugin.cascadeRules[1], CascadeSearchPlugin.cascadeRules[2]),
                        {
                            callback: function () {
                                CascadeSearchPlugin.createRequired(CascadeSearchPlugin.cascadeRules[1], true, "", {
                                    callback: function () {
                                        CascadeSearchPlugin.createRequired(CascadeSearchPlugin.cascadeRules[2], false, "", {
                                            callback: function () {
                                                // if this cascading search should be prefilled with selections then add value into the coname
                                                // input field and perform a company search using the first value in 'prefillIds' array
                                                if (CascadeSearchPlugin.prefillIds.length > 0) {
                                                    CompanySearchPlugin.runCompanyIdAjax(CascadeSearchPlugin.prefillIds[0]);
                                                }
                                                CascadeSearchPlugin.prefillFinalised = false;
                                            },
                                        });
                                    },
                                });
                            },
                        }
                    );
                }
            },
        });
    },

    /*
     * creates the required div for a specific search box (i.e. company, subdiv, address, contact),
     * imports the javascript file required and then calls the init() function within that file which is
     * responsible for the setup of that search.
     */
    createRequired: function (choice, cascade, afterSubdiv, func) {
        switch (choice) {
            case "company":
                $j("#" + CascadeSearchPlugin.pageDiv).append('<div id="' + CascadeSearchPlugin.compDiv + '"></div>');
                // load the service javascript file if not already
                loadScript.loadScriptsDynamic(new Array("script/trescal/core/utilities/searchplugins/CompanySearchPlugin.js"), true, {
                    callback: function () {
                        // call the init function in CompanySearchPlugin.js to create search
                        CompanySearchPlugin.init(cascade);
                        // cascade search plugin should be visible always
                        if (CascadeSearchPlugin.visible == true) {
                            // display the company results container
                            $j("#" + CompanySearchPlugin.resultsDiv + ", div#comp-container ." + CompanySearchPlugin.headDiv).css({
                                zIndex: 1000,
                                display: "block"
                            });
                            // set literal in company plugin object
                            CompanySearchPlugin.visible = CascadeSearchPlugin.visible;
                            // remove the close image
                            $j("#" + CompanySearchPlugin.boxDiv + " img").remove();
                            // change input width
                            $j("#" + CompanySearchPlugin.boxDiv + ' input[type="text"]').css({width: "97%"});
                        }
                        // cascade plugin should search for called of items and collected items
                        if (CascadeSearchPlugin.searchItems == true) {
                            // append two list items after cascading search to display items
                            var calloffitems = "";
                            if (CascadeSearchPlugin.entityType == "job")
                                calloffitems =
                                    "<li>" +
                                    "<label>" +
                                    i18n.t("core.utilities:searchplugins.callOffItemsLabel", "Called Off Items:") +
                                    "</label>" +
                                    '<table class="default2" id="itemCOResults" summary="This table displays any called off items">' +
                                    '<thead class="hid">' +
                                    "<tr>" +
                                    '<th class="COselect" scope="col"><span>' +
                                    i18n.t("select", "Select") +
                                    '<input type="checkbox" onclick=" selectAllItems(this.checked, \'itemCOResults\');" class="checkbox"/>' +
                                    i18n.t("all", "All") +
                                    "</span>" +
                                    "</th>" +
                                    '<th class="CObarcode" scope="col">' +
                                    i18n.t("barcode", "Barcode") +
                                    "</th>" +
                                    '<th class="COInst" scope="col">' +
                                    i18n.t("instrument", "Instrument") +
                                    "</th>" +
                                    '<th class="COsubdiv" scope="col">' +
                                    i18n.t("subdivision", "Subdivision") +
                                    "</th>" +
                                    '<th class="COPrevLoc" scope="col">' +
                                    i18n.t("core.utilities:searchplugins.previousJob", "Previous Job") +
                                    "</th>" +
                                    "</tr>" +
                                    "</thead>" +
                                    "<tbody>" +
                                    "<tr>" +
                                    '<td class="bold center">' +
                                    i18n.t("core.utilities:searchplugins.noCallOffItems", "No Called Off Items") +
                                    "</td>" +
                                    "</tr>" +
                                    "</tbody>" +
                                    "</table>" +
                                    "</li>";
                            $j("#" + CascadeSearchPlugin.pageDiv)
                                .parent()
                                .after(
                                    calloffitems +
                                    /* Hide collected items for Madrid */
                                    '<li class = "hid">' +
                                    "<label>" +
                                    i18n.t("core.utilities:searchplugins.collectedItemsLabel", "Collected Items:") +
                                    "</label>" +
                                    '<table class="default2" id="itemCResults" summary="This table displays any called off items">' +
                                    '<thead class="hid">' +
                                    "<tr>" +
                                    '<th class="Cselect" scope="col">' +
                                    i18n.t("select", "Select") +
                                    "</th>" +
                                    '<th class="Cbarcode" scope="col">' +
                                    i18n.t("barcode", "Barcode") +
                                    "</th>" +
                                    '<th class="Cinst" scope="col">' +
                                    i18n.t("instrument", "Instrument") +
                                    "</th>" +
                                    "</tr>" +
                                    "</thead>" +
                                    "<tbody>" +
                                    "<tr>" +
                                    '<td class="bold center">' +
                                    i18n.t("core.utilities:searchplugins.noCollectedItems", "No Collected Items") +
                                    "</td>" +
                                    "</tr>" +
                                    "</tbody>" +
                                    "</table>" +
                                    "</li>" +
                                    "<li>" +
                                    "<label" +
                                    i18n.t("bpoLabel", ">BPO:") +
                                    "</label>" +
                                    '<table class="default2" id="itemBPOResults" summary="This table displays any BPOs">' +
                                    '<thead class="hid">' +
                                    "<tr>" +
                                    '<th class="BPOselect" scope="col">' +
                                    i18n.t("select", "Select") +
                                    "</th>" +
                                    '<th class="BPOnumber" scope="col">' +
                                    i18n.t("core.utilities:searchplugins.bpoNumber", "BPO number") +
                                    "</th>" +
                                    '<th class="BPOcomment" scope="col">' +
                                    i18n.t("core.utilities:searchplugins.bpoComment", "BPO comment") +
                                    "</th>" +
                                    '<th class="BPOvalfrom" scope="col">' +
                                    i18n.t("core.utilities:searchplugins.bpoValidFrom", "Valid From") +
                                    "</th>" +
                                    '<th class="BPOvalto" scope="col">' +
                                    i18n.t("core.utilities:searchplugins.bpoValidTo", "Valid To") +
                                    "</th>" +
                                    '<th class="BPOlimit" scope="col">' +
                                    i18n.t("core.utilities:searchplugins.bpoLimit", "Limit") +
                                    "</th>" +
                                    "</tr>" +
                                    "</thead>" +
                                    "<tbody>" +
                                    "<tr>" +
                                    '<td class="bold center">' +
                                    i18n.t("core.utilities:searchplugins.noBPOs", "No Blanket Purchase Order's") +
                                    "</td>" +
                                    "</tr>" +
                                    "</tbody>" +
                                    "</table>" +
                                    "</li>"
                                );
                        }
                        // if the cascade search plugin should be visible then extend the page to suit
                        if (CascadeSearchPlugin.visible == true) {
                            if (CascadeSearchPlugin.customHeight != "") {
                                $j("#" + CascadeSearchPlugin.pageDiv).css({height: CascadeSearchPlugin.customHeight + "px"});
                            } else {
                                // searching locations?
                                if (CascadeSearchPlugin.searchLocations) {
                                    $j("#" + CascadeSearchPlugin.pageDiv).css({height: "352px"});
                                    var absTop = "336px";
                                } else {
                                    $j("#" + CascadeSearchPlugin.pageDiv).css({height: "252px"});
                                    var absTop = "234px";
                                }
                                if ($j("input#cascadeRules").val() == "") {
                                    absTop = "130px";
                                }
                                if (CascadeSearchPlugin.enableAdd) {
                                    $j("#" + CascadeSearchPlugin.pageDiv).append(
                                        '<div id="' +
                                        CascadeSearchPlugin.linksDiv +
                                        '" style=" width: 600px; position: absolute; top: ' +
                                        absTop +
                                        '; left: 0; ">' +
                                        '<span><a href="' +
                                        springUrl +
                                        '/addcompany.htm" target="_blank" class="mainlink">' +
                                        i18n.t("core.utilities:searchplugins.addCompany", "Add Company") +
                                        "</a></span>" +
                                        "</div>"
                                    );
                                }
                            }
                        }
                        if (func) {
                            func.callback();
                        }
                    },
                });
                break;

            case "subdiv":
                $j("#" + CascadeSearchPlugin.pageDiv).append('<div id="' + CascadeSearchPlugin.subDiv + '"></div>');
                loadScript.loadScriptsDynamic(new Array("script/trescal/core/utilities/searchplugins/SubdivSearchPlugin.js"), true, {
                    callback: function () {
                        // call the init function in CompanySearchPlugin.js to create search
                        SubdivSearchPlugin.init(cascade, afterSubdiv);
                        if (CascadeSearchPlugin.visible == true) {
                            // display the subdivision results container
                            $j("#" + SubdivSearchPlugin.resultsDiv + ", div#subdiv-container ." + SubdivSearchPlugin.headDiv).css({
                                zIndex: 1000,
                                display: "block"
                            });
                        }

                        if (func) {
                            func.callback();
                        }
                    },
                });
                break;

            case "address":
                $j("#" + CascadeSearchPlugin.pageDiv).append('<div id="' + CascadeSearchPlugin.addrDiv + '"></div>');
                // searching for locations
                if (CascadeSearchPlugin.searchLocations) {
                    $j("#" + CascadeSearchPlugin.pageDiv).append('<div id="' + CascadeSearchPlugin.locDiv + '"></div>');
                }
                loadScript.loadScriptsDynamic(new Array("script/trescal/core/utilities/searchplugins/AddressSearchPlugin.js"), true, {
                    callback: function () {
                        // call the init function in CompanySearchPlugin.js to create search
                        AddressSearchPlugin.init(cascade);
                        if (CascadeSearchPlugin.visible == true) {
                            // display the address results container
                            $j("#" + AddressSearchPlugin.resultsDiv + ", div#addr-container ." + AddressSearchPlugin.headDiv).css({
                                zIndex: 1000,
                                display: "block"
                            });
                            // searching for locations?
                            if (AddressSearchPlugin.searchLocations) {
                                // display the location results container
                                $j("#" + AddressSearchPlugin.locResultsDiv + ", div#loc-container ." + AddressSearchPlugin.headDiv).css({
                                    zIndex: 1000,
                                    display: "block"
                                });
                            }
                        }
                        if (func) {
                            func.callback();
                        }
                    },
                });
                break;

            case "contact":
                $j("#" + CascadeSearchPlugin.pageDiv).append('<div id="' + CascadeSearchPlugin.contDiv + '"></div>');
                loadScript.loadScriptsDynamic(new Array("script/trescal/core/utilities/searchplugins/ContactSearchPlugin.js"), true, {
                    callback: function () {
                        // call the init function in CompanySearchPlugin.js to create search
                        ContactSearchPlugin.init(cascade);
                        if (CascadeSearchPlugin.visible == true) {
                            // display the contact results container
                            $j("#" + ContactSearchPlugin.resultsDiv + ", div#cont-container ." + ContactSearchPlugin.headDiv).css({
                                zIndex: 1000,
                                display: "block"
                            });
                        }
                        if (func) {
                            func.callback();
                        }
                    },
                });
                break;
        }
    },

    /**
     * this method toggles the visibility of the subdivision and address search plugin when there
     * is the need for the user to select either a company or a company and an address id
     *
     * @param {Object} anchor the anchor which is clicked to toggle plugins
     */
    toggleAddressSearch: function (anchor) {
        // subdiv and address plugins have been created
        if (CascadeSearchPlugin.cascadeRules.length != 0) {
            // empty the cascade rules array
            CascadeSearchPlugin.cascadeRules = [];
            // change height of plugin area used to fit in page better when only
            // company search plugin is visible
            $j("#" + CascadeSearchPlugin.pageDiv).css({height: "148px"});
            $j("#" + CascadeSearchPlugin.linksDiv).css({top: "130px"});
            $j("#" + CascadeSearchPlugin.linksDiv + " span:not(':first')").remove();
            // remove subdivision and address search plugins
            $j("div#subdivSearchPlugin, div#addrSearchPlugin").remove();
            // re-run company search using the selected company
            CompanySearchPlugin.runCompanyAjax(
                $j("#" + CompanySearchPlugin.resultsDiv + " a")
                    .eq(CompanySearchPlugin.selected)
                    .text()
            );
            // change text in anchor
            $j(anchor).text(i18n.t("core.utilities:searchplugins.selectAddress", "Select Address"));
        } else {
            // populate array with the necessary values to create subdiv and address
            // search plugins
            CascadeSearchPlugin.cascadeRules.push("subdiv", "address");
            // change height of plugin area used to accomodate subdiv and address
            $j("#" + CascadeSearchPlugin.pageDiv).css({height: "252px"});
            $j("#" + CascadeSearchPlugin.linksDiv).css({top: "234px"});
            // create the subdivision search plugin div
            CascadeSearchPlugin.createRequired("subdiv", true, new Array("address"));
            // create the address search plugin div
            CascadeSearchPlugin.createRequired("address", false, "");
            // re-run company search using the selected company
            setTimeout("CompanySearchPlugin.runCompanyAjax($j('#' + CompanySearchPlugin.resultsDiv + ' a').eq(CompanySearchPlugin.selected).text());", 500);
            // change value in anchor
            $j(anchor).text(i18n.t("core.utilities:searchplugins.removeAddress", "Remove Address"));
        }
    },
    /**
     * custom height for the cascade search plugin
     */
    customHeight: "",
    /**
     * boolean value which defines if the cascading search should be visible
     * or hidden
     */
    visible: false,
    /**
     * boolean value set to true when prefilling of the cascading
     * search has been completed and normal functionality can be
     * returned
     */
    prefillFinalised: false,
    /**
     * boolean value set to true when items should be retrieved for
     * the selected company
     */
    searchItems: false,
    /**
     * boolean value set to true when bpo's should be retrieved for
     * the selected contact
     */
    searchBPO: false,
    /**
     * indicates if we give option of no location
     */
    noLocation: false,
    /**
     * boolean value set to true when locations should be retrieved for
     * the selected address
     */
    searchLocations: false,
    /**
     * boolean value set to true when currencies should be retrieved for
     * the selected company
     */
    loadCurrency: false,
    /**
     * boolean value set to true when discounts need to be sourced for purchase orders
     */
    sourceDiscount: false,
    /**
     * boolean value set to true when nominal need to be sourced for purchase orders
     */
    sourceNominal: false,
    /**
     * array of id's which will be used to prefill the contents of the
     * cascading search if they have been supplied.
     */
    prefillIds: [],
    /**
     * this value tells the address search which address types to display
     */
    addressType: "",
    /**
     * array to hold values passed in the hidden input field ('contact', 'address' or both)
     */
    cascadeRules: [],
    /**
     * id of the main element on page in which the cascade search will be created
     */
    pageDiv: "cascadeSearchPlugin",
    /**
     * id of the element in which the company search should be created
     */
    compDiv: "compSearchPlugin",
    /**
     * id of the element in which the subdivision search should be created
     */
    subDiv: "subdivSearchPlugin",
    /**
     * id of the element in which the address search should be created
     */
    addrDiv: "addrSearchPlugin",
    /**
     * id of the element in which the location search results should be displayed
     */
    locDiv: "locSearchPlugin",
    /**
     * id of the element in which the contact search should be created
     */
    contDiv: "contSearchPlugin",
    /**
     * id of the element in which links are appended for adding entity types (i.e company, subdiv)
     */
    linksDiv: "cascLinks",
    /**
     *  Subdiv call Back Function
     */
    subdivCallBackFunction: "",
    /**
     *  Subdiv call Back Function
     */
    contactCallBackFunction: "",
    /**
     *  Subdiv call Back Function
     */
    addressCallBackFunction: "",
    /**
     * entity type ('prebooking' or 'job')
     */
    entityType: "job",
    /**
     * select automatically the first option on each subsearch
     */
    autoselect: true,
    /**
     * contains links to add new company, subdivision etc.
     */
    enableAdd: true,
};
