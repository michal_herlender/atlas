/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Loeung Sothy
*
*	FILENAME		:	InstrumentSearchPlugin.js
*	DESCRIPTION		:	Javascript file to display a popup when searching an instrument which returns many results
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	File Created - SL - 15/12/2015
****************************************************************************************************************************************/

var pageImports = new Array (	'script/thirdparty/jQuery/jquery.tablesorter.js',
								'dwr/interface/instrumservice.js',
								'script/thirdparty/jQuery/jquery.boxy.js' );

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * this method displays a popup when searching an instrument which returns many results
 * 
 * @param {String} redirectUrl url to redirect after clicking on the hyperlink
 * @param {String} queryParameter Label of the query parameter to add in the URL
 * @param {String} capturedControl Full id of the control which has the captured text
 * @param {Object} anchor the anchor element clicked on the page
 * @param {Object} currentRange the div element of the current range
 * @param {Object} rangeContainer the div element which contains all ranges
 */
function showInstruments(redirectUrl, queryParameter, capturedControl, anchor, currentRange, rangeContainer, reloadPage, hideOnClick)
{	
	var capturedText = $j(capturedControl).val();
	var escapeCharacter = window.location.pathname.indexOf('?') > -1 ? '&' : '?';
	var quote = '&quot;';
	
	if (capturedText != null && capturedText != "")
	{
		instrumservice.findInstrumByBarCode(capturedText,
		{
			callback:function(instruments)
			{		
				if (instruments != null && instruments.length != 0)
				{
					if (instruments.length == 1) 
					{
						if (reloadPage)
						{
							window.location = redirectUrl + escapeCharacter + queryParameter + '=' + escape(instruments[0].plantid);
						}
					}
					else
					{
						// create content
						var	content = 	'<div id="showInstrumentsBoxy" class="overflow">' +
											'<div class="warningBox1 hid">' +
												'<div class="warningBox2">' +
													'<div class="warningBox3">' +
													'</div>' +
												'</div>' +
											'</div>' +
											'<fieldset>' +
												'<legend>' + i18n.t("common:selectInstrument", "Select instrument") + '</legend>' +
												'<table class="default2 instFound" summary="This table lists all instrument found by barcode and former barcode">' + 
													'<thead>' + 
		                        					'<tr>' + 
		                        						'<th class="image" scope="col">&nbsp;</th>' + 
		                        						'<th class="formerbarcode" scope="col">' + i18n.t("common:formerBarcode", "Former barcode") + '</th>' + 
		                        						'<th class="plantid" scope="col">' + i18n.t("common:barcode", "Trescal barcode") + '</th>' + 
		                        						'<th class="customer" scope="col">' + i18n.t("common:customer", "Customer") + '</th>' + 	
		                        						'<th class="manufacturer" scope="col">' + i18n.t("common:manufacturer", "Manufacturer") + '</th>' + 	
		                        						'<th class="model" scope="col">' + i18n.t("common:model", "Model") + '</th>' + 	
		                        						'<th class="subfamily" scope="col">' + i18n.t("common:subfamily", "SubFamily") + '</th>' + 																			
		                        					'</tr>' +
												'</thead>' +
		                                    	'<tfoot>' +
		                                    		'<tr>' +
		                                    			'<td colspan="7">&nbsp;</td>' +
		                                    		'</tr>' +
		                                    	'</tfoot>' + 
		                                    	'<tbody>';
						
						for(var i=0; i<instruments.length; i++)
						{ 	
													content += '<tr>';
														content += '<td class="image">';
														if (instruments[i].model.images != null)
														{
															content += '<img src="' + instruments[i].model.images[0].relativeFilePath + '" width="50" height="50" alt="" title="" />';
														}
														else
														{
															content += '<img src="img/noimage.png" width="50" height="50" alt="" title="" class="noImage" />';
														}
														content += '</td>';
														
														var barcodeContent = instruments[i].formerBarCode;
														if (barcodeContent == null || barcodeContent.length == 0) {
															// Prevents unselectable content when no former barcode
															barcodeContent = i18n.t("common:none", "None");
														}
														
														
														if (reloadPage)
														{
															content += '<td class="formerbarcode"><a href=' + window.location.pathname + escapeCharacter + queryParameter + '=' + escape(instruments[i].plantid) + '>' + barcodeContent + '</a></td>';
														}
														else
														{
															content += '<td class="formerbarcode"><a class="close" href="#" onclick="event.preventDefault(); initializeCapturedField(' + quote + capturedControl + quote + ',' + instruments[i].plantid + ');">' + barcodeContent + '</a></td>';
														}
														
														content += '<td class="plantid">' + instruments[i].plantid + '</td>';
														content += '<td class="customer">' + instruments[i].con.sub.subname + '</td>';
														
														if (instruments[i].model.modelMfrType == "MFR_SPECIFIC")
														{
															content += '<td class="manufacturer">' + instruments[i].model.mfr.name + '</td>';
														}
														else
														{
															content += '<td class="manufacturer">' + instruments[i].mfr.name + '</td>';
														}
														
														content += '<td class="model">' + instruments[i].model.model + '</td>';
														content += '<td class="subfamily">' + instruments[i].model.description.description + '</td>';
													content += '</tr>';
						}
						
						content +=					
													'</tbody>' +
												'</table>' +
											'</fieldset>' +
										'</div>';
						
						// create new boxy pop up using content variable
						var showInstrumentsBoxy = new Boxy(content, {title: i18n.t("common:selectInstrument", "Select instrument"), modal: true, unloadOnHide: true});
						showInstrumentsBoxy.center(anchor);
						showInstrumentsBoxy.show();
					}
					
					if(instruments.length != undefined && instruments.length > 0)
					{
						addItems(instruments[0].plantid);
						$j(capturedControl).val('');
					}
				}
			}	
		});
	}
}

function initializeCapturedField(field, value)
{
	$j(field).val(value);
	$j(parent).find("div.showInstrumentsBoxy").hide();
}