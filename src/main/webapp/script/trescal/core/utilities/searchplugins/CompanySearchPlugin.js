/****************************************************************************************************************************************
 *                                                CWMS (Calibration Workflow Management System)
 *
 *                                                Copyright 2006, Antech Calibration Services
 *                                                            www.antech.org.uk
 *
 *                                                            All rights reserved
 *
 *                                                        Document author:    Stuart Harrold
 *
 *    FILENAME        :    CompanySearchPlugin.js
 *    DESCRIPTION        :    Javascript file imported when creating a company search on a page
 *                    :    Code creates two input fields, one for searching companies and the other for holding the coid of a
 *                    :    selected company, a results section is then created below to display companies returned.
 *                    :    Further code uses dwr to get company results and display them on the page
 *                    :    This plugin is also used to search companies in the cascading search plugin.
 *    DEPENDENCIES    :   -
 *
 *    TO-DO            :    -
 *    KNOWN ISSUES    :    -
 *    HISTORY            :    File Created - SH - 25/07/2007
 *                    :    29/10/2015 - Galen Beck - Updated .get to findDWRCompany as we have now separated out the DWR methods from the CompanyService.
 *                    :    27/10/2015 - TProvost - Fix bug on CompanyService : replace call "companyservice.findCompanyById" function by "companyservice.get" function
 *                    :    29/10/2015 - TProvost - Fix bug on test of the version of browser
 *                    :    30/10/2015 - TProvost - Manage i18n
 *                    :    19/11/2018 - Egbert Fohry - Replaced findDWRCompany by Ajax call
 ****************************************************************************************************************************************/

/**
 * this literal is called when the page loads if an element with the id of 'compSearchPlugin' is found on the page.
 * this function then loads all the necessary scripts it needs and checks whether this plugin is part of a
 * cascading search. The drawSearch() and setCoroles() functions are then called to create the search and set company
 * roles to be searched over.
 * @param cascade - boolean value to indicate whether this search should be cascaded
 */
CompanySearchPlugin = {
    init: function (cascade) {
        // scripts to load
        var Scripts = new Array("script/trescal/core/utilities/keynavigation/SearchPluginsKeyNav.js");

        // load the common javascript files for this plugin
        loadScript.loadScripts(Scripts);

        // set to true if this plugin is part of a cascading search
        if (cascade == true) {
            CompanySearchPlugin.cascade = true;
        } else {
            CompanySearchPlugin.cascade = false;
            CompanySearchPlugin.visible = false;
        }
        // company search has hidden input for tabindex
        if ($j("input#compindex").length) {
            // set tabindex value
            CompanySearchPlugin.tabindex = $j("input#compindex").val();
        }
        // draw search plugin
        CompanySearchPlugin.drawSearch();
        // set the coroles to be searched over
        CompanySearchPlugin.setCoroles();
    },
    /**
     * if the input with an id of 'compCoroles' is present and contains coroles separated by comma's then
     * add these values to the coroles literal array. If not present then add default 'client' corole.
     */
    setCoroles: function () {
        // input of id 'compCoroles' is present in page
        if ($j("input#compCoroles").length) {
            $j("input#compCoroles").each(function () {
                // add each corole to array
                CompanySearchPlugin.coroles = this.value.split(",");
            });
        } else {
            // no input present so set default corole
            CompanySearchPlugin.coroles = "client";
        }
    },
    /**
     * tab index for input field
     */
    tabindex: 0,
    /**
     * array of coroles provided in the input field 'compCoroles'.
     */
    coroles: [],
    /**
     * false by default, if true then this company search is being used as part of the cascading search plugin.
     */
    cascade: false,
    /**
     * id of the company name input field
     */
    nameInput: "coname",
    /**
     * id of the hidden input field used to pass the company coid
     */
    idInput: "coid",
    /**
     * id of the main div declared in the page where the search should be created
     */
    boxDiv: "compSearchPlugin",
    /**
     * id of the div in which the headings for the search are created
     */
    headDiv: "pluginHead",
    /**
     * id of the div in which the search results are to be displayed
     */
    resultsDiv: "compPlugResults",
    /**
     * class name used on the results div to apply styles
     */
    styleResDiv: "pluginResults",
    /**
     * stores id of the form in which this search resides
     */
    searchFormId: "",
    /**
     * id of the form element on which focus should be placed
     */
    inputFocus: "coname",
    /**
     * boolean value used to flag if a navigation cursor key has been pressed so no ajax is called
     */
    keyPress: false,
    /**
     * this boolean value denotes whether the search should be visible or not
     */
    visible: false,
    /**
     * value of the currently selected element in the list
     */
    selected: 0,
    /**
     * time value of last DWR call, checked against the current DWR call
     */
    lastUpdate: 0,
    /**
     * creates the input fields and results div on the page for normal search
     */
    drawSearch: function () {
        $j("#" + CompanySearchPlugin.boxDiv).append(
            '<input type="hidden" id="' +
            CompanySearchPlugin.idInput +
            '" name="' +
            CompanySearchPlugin.idInput +
            '" />' +
            // create and append the input in which the user will search for companies
            // set autocomplete to off and add the onkeyup event to capture key presses
            '<input type="text" id="' +
            CompanySearchPlugin.nameInput +
            '" name="' +
            CompanySearchPlugin.nameInput +
            '" onkeyup="CompanySearchPlugin.runCompanyAjax(this.value); return false;" tabindex="' +
            CompanySearchPlugin.tabindex +
            '" autocomplete="off" /><img src="img/icons/searchplugin.png" width="16" height="16" alt="' +
            i18n.t("core.utilities:searchplugins.optionSearch", "Option Search") +
            '" title="' +
            i18n.t("core.utilities:searchplugins.optionSearch", "Option Search") +
            '" />' +
            '<div id="comp-container">' +
            // create and append header
            '<div class="' +
            CompanySearchPlugin.headDiv +
            '">' +
            i18n.t("core.utilities:searchplugins.matchingCompanies", "Matching Companies") +
            '<img src="img/icons/bullet-cross.png" width="10" height="10" alt="' +
            i18n.t("core.utilities:searchplugins.closeCompanySearch", "Close Company Search") +
            '" title="' +
            i18n.t("core.utilities:searchplugins.closeCompanySearch", "Close Company Search") +
            '" class="pluginClose" onclick=" CompanySearchPlugin.closePlugin(); return false; " /></div>' +
            // create and append the div in which company results will be displayed
            '<div id="' +
            CompanySearchPlugin.resultsDiv +
            '" class="' +
            CompanySearchPlugin.styleResDiv +
            '">' +
            // results are displayed here
            "</div>" +
            "</div>"
        );
        if ($j.browser != null && ($j.browser.mozilla != null || $j.browser.opera != null)) {
            $j("#" + CompanySearchPlugin.boxDiv)
                .parent("li")
                .css({paddingBottom: "4px"});
        } else {
            $j("#" + CompanySearchPlugin.boxDiv)
                .parent("li")
                .css({paddingBottom: "3px"});
        }

        if (this.form) {
            // find out the id of the form in which this search resides and add to literal
            $j("input#" + CompanySearchPlugin.nameInput).each(function () {
                // return the form object
                if (this.form) {
                    CompanySearchPlugin.searchFormId = this.form.id;
                }
            });
        }
    },
    /**
     * this function accepts a value entered by the user and calls an ajax method to retrieve
     * companies matching this value from the server.  The returned data is then passed on to function
     * (companyCallback) for displaying on the page.
     *
     * @param {String} nameFragment - value entered by the user in the input field
     */
    runCompanyAjax: function (nameFragment) {
        // check that another search plugin is not currently in use
        if (typeof searchFOCUS !== "undefined" && searchFOCUS != CompanySearchPlugin && searchFOCUS != "") {
            searchFOCUS.closePlugin();
        }

        // the up/down arrow key has been pressed so set value back to false and skip ajax loading
        if (CompanySearchPlugin.keyPress == true) {
            CompanySearchPlugin.keyPress = false;
        } else {
            // populate the searchFOCUS variable with this 'CompanySearchPlugin' object
            // for use when cursor key navigation is being used
            searchFOCUS = CompanySearchPlugin;

            // set the coroles to be searched over
            CompanySearchPlugin.setCoroles();

            // remove all previously displayed companies
            $j("#" + CompanySearchPlugin.resultsDiv).empty();

            // display the company results container
            $j("#" + CompanySearchPlugin.resultsDiv + ", div#comp-container ." + CompanySearchPlugin.headDiv).css({
                zIndex: 100,
                display: "block"
            });

            // this span displays the loading image and text when a search has been activated
            $j("#" + CompanySearchPlugin.resultsDiv).append(
                '<div class="loading-plug">' + '<img src="img/icons/loading-anim.gif" class="img_marg_bot" height="18" width="80" alt="" />' + "</div>"
            );

            // set active variable to true to return all currently active companies
            var active = true;
            // set deactivated variable to false to not include deactivated companies
            var deactivated = false;

            // get current timestamp to pass with ajax results
            var currentTime = new Date().getTime();

            // only execute the ajax functionality if the text entry has a length
            // greater than zero (stops unconstrained retrieval)
            if (nameFragment.length > 0) {
                // load the service javascript file if not already
                loadScript.loadScriptsDynamic(new Array("dwr/interface/companyservice.js"), true, {
                    callback: function () {
                        companyservice.searchCompanyByRolesHQL(nameFragment, active, deactivated, CompanySearchPlugin.coroles, {
                            callback: function (dataFromServer) {
                                CompanySearchPlugin.companyCallback(dataFromServer, currentTime);
                            },
                        });
                    },
                });
            } else {
                if (CompanySearchPlugin.visible == false) {
                    // hide the company results container
                    $j("#" + CompanySearchPlugin.resultsDiv + ", div#comp-container ." + CompanySearchPlugin.headDiv).css({
                        zIndex: -1,
                        display: "none"
                    });
                }
                // remove all previously displayed companies
                $j("#" + CompanySearchPlugin.resultsDiv).empty();
                // clear value in the coid hidden input field
                $j("#" + CompanySearchPlugin.idInput).attr("value", "");
                // clear value in variable @see SearchPluginsKeyNav.js
                searchFOCUS = "";
                // this is a cascading search so clear all search boxes
                if (CompanySearchPlugin.cascade == true) {
                    if (CompanySearchPlugin.visible == true) {
                        // this is a cascading search so clear all search boxes but do not display message
                        CompanySearchPlugin.clearSearch(false);
                        // cascade plugin?
                        if (typeof CascadeSearchPlugin != "undefined") {
                            // remove add subdiv link
                            $j("#" + CascadeSearchPlugin.linksDiv + " span#cascAddSub").remove();
                            // remove add contact link
                            $j("#" + CascadeSearchPlugin.linksDiv + " span#cascAddCont").remove();
                            // remove add address link
                            $j("#" + CascadeSearchPlugin.linksDiv + " span#cascAddAddr").remove();
                        }
                    } else {
                        SubdivSearchPlugin.runSubdivAjax("");
                    }
                }
            }
        }

        if (nameFragment == undefined || nameFragment == "") {
            $j("#availableprebookings").find("option").remove();
            // empty the table body
            $j("table#itemBPOResults tbody").empty();
            // hide the table head section
            $j("table#itemBPOResults thead").removeClass().addClass("hid");
            // append message of no called off items
            $j("table#itemBPOResults tbody").append(
                "<tr>" + '<td class="bold center">' + i18n.t("core.utilities:searchplugins.noBPOs", "No Blanket Purchase Order's") + "</td>" + "</tr>"
            );
        }
    },
    /**
     * this function accepts an id and calls an ajax method to retrieve a company matching
     * this value from the server.  The returned data is then passed on to function
     * (companyCallback) for displaying on the page.
     *
     * @param {String} nameFragment - value entered by the user in the input field
     */
    runCompanyIdAjax: function (id) {
        // populate the searchFOCUS variable with this 'CompanySearchPlugin' object
        // for use when cursor key navigation is being used
        searchFOCUS = CompanySearchPlugin;

        // set the coroles to be searched over
        CompanySearchPlugin.setCoroles();

        // remove all previously displayed companies
        $j("#" + CompanySearchPlugin.resultsDiv).empty();

        // display the company results container
        $j("#" + CompanySearchPlugin.resultsDiv + ", div#comp-container ." + CompanySearchPlugin.headDiv).css({
            zIndex: 100,
            display: "block"
        });

        // this span displays the loading image and text when a search has been activated
        $j("#" + CompanySearchPlugin.resultsDiv).append(
            '<div class="loading-plug">' + '<img src="img/icons/loading-anim.gif" class="img_marg_bot" height="18" width="80" alt="" />' + "</div>"
        );

        // get current timestamp to pass with ajax results
        var currentTime = new Date().getTime();
        if (id != 0) {
            $j.ajax({
                url: "companyforplugin.json",
                data: {
                    id: id,
                },
                async: true,
            }).done(function (companyList) {
                $j("#" + CompanySearchPlugin.nameInput).attr("value", companyList[0].coname);
                CompanySearchPlugin.companyCallback(companyList, currentTime);
            });
        }
    },
    /**
     * this function displays the results returned from the server for ajax call 'runCompanyAjax'
     * if this plugin is part of a cascading search plugin a dwr call to retrieve all subdivisions
     * is made by passing the coid to the runSubdivAjax literal in the SubdivSearchPlugin object
     *
     * @param {Array} companies is an array of company objects returned from the server
     * @param {Date/Time} lastUpdate is a timestamp of data retrieval from the server
     */
    companyCallback: function (companies, currentUpdate) {
        // this is a new search
        if (currentUpdate > CompanySearchPlugin.lastUpdate) {
            // update value in the 'lastUpdate' literal
            CompanySearchPlugin.lastUpdate = currentUpdate;

            // companies length is greater than zero
            if (companies.length > 0) {
                // remove all previously displayed companies
                $j("#" + CompanySearchPlugin.resultsDiv).empty();
                // declare variable to hold company results
                var content = "";
                // add anchor for each company returned
                $j.each(companies, function (i, comp) {
                    content += '<a href="#" id="' + comp.coid + '" ';
                    if (!comp.blocked) {
                        content += 'onclick=" CompanySearchPlugin.linkOnclick(' + comp.coid + ", '" + escape(comp.coname) + "'); return false;\"";
                    } else {
                        content +=
                            'onclick=" return false; " class="compBlocked" title="' +
                            i18n.t("core.utilities:searchplugins.compIsNotAccredited", "Company is not accredited") +
                            '"';
                    }
                    content += "><span>" + comp.coname + "</span></a>";
                });
                // append content to results box
                $j("#" + CompanySearchPlugin.resultsDiv).append(content);
                // populate the coid hidden input field with the current coid
                $j("#" + CompanySearchPlugin.idInput).attr("value", companies[0].coid);
                // add 'selected' class name to first anchor
                $j("#" + CompanySearchPlugin.resultsDiv + " a:first").addClass("selected");
                // initialise the selected literal to zero
                CompanySearchPlugin.selected = 0;

                // do we need to load currency info for selected company?
                if (typeof CascadeSearchPlugin != "undefined") {
                    if (CascadeSearchPlugin.loadCurrency == true) {
                        CompanySearchPlugin.loadCurrency(companies[0].coid);
                    }
                }
                // this plugin is part of a cascading search plugin so a dwr call to retrieve all subdivisions is made
                // results are sent to the SubdivSearchPlugin 'subdivCallback' object literal
                if (CompanySearchPlugin.cascade == true && CascadeSearchPlugin.cascadeRules.length > 0) {
                    // check that company is not blocked?
                    if (!companies[0].blocked) {
                        // initiate subdivision search
                        SubdivSearchPlugin.runSubdivAjax(companies[0].coid);
                    } else {
                        // this is a cascading search so clear all search boxes and display message
                        CompanySearchPlugin.clearSearch(true);
                    }
                }
            } else {
                if (CompanySearchPlugin.cascade == true) {
                    // this is a cascading search so clear all search boxes and display message
                    CompanySearchPlugin.clearSearch(true);
                }
                // remove all previously displayed companies and create a div to display that no results have been returned
                $j("#" + CompanySearchPlugin.resultsDiv)
                    .empty()
                    .append(
                        '<div class="bold center" style=" padding: 5px; ">' + i18n.t("core.utilities:searchplugins.nCompaniesFound", "No Companies Found") + "</div>"
                    );
                // clear value in the coid hidden input field
                $j("#" + CompanySearchPlugin.idInput).attr("value", "");
                // cascade plugin?
                if (typeof CascadeSearchPlugin != "undefined") {
                    // remove add subdiv link
                    $j("#" + CascadeSearchPlugin.linksDiv + " span#cascAddSub").remove();
                    // remove add contact link
                    $j("#" + CascadeSearchPlugin.linksDiv + " span#cascAddCont").remove();
                    // remove add address link
                    $j("#" + CascadeSearchPlugin.linksDiv + " span#cascAddAddr").remove();
                }
            }
        }
    },
    /**
     * this function moves the 'selected' classname to an anchor which the user has clicked on and populates the hidden input
     * field with the selected companies coid. If this plugin is part of a cascading search plugin a dwr call to retrieve all
     * subdivisions is made by passing the coid to the runSubdivAjax literal in the SubdivSearchPlugin object.
     *
     * @param {Integer} coid is the id of the company being clicked
     * @param {String} coname is the name of the company being clicked
     */
    linkOnclick: function (coid, coname) {
        coname = unescape(coname);
        if (CompanySearchPlugin.cascade == false) {
            //
            if (typeof CascadeSearchPlugin != "undefined" && CascadeSearchPlugin.visible == false) {
                // hide the company results container
                $j("#" + CompanySearchPlugin.resultsDiv + ", div#comp-container ." + CompanySearchPlugin.headDiv).css({
                    zIndex: -1,
                    display: "none"
                });
            }
            // populate the coname input field with the name of the selected company
            $j("#" + CompanySearchPlugin.nameInput).attr("value", coname);
            // populate the coid hidden input field with the id of this anchor (ie coid)
            $j("#" + CompanySearchPlugin.idInput).attr("value", coid);
            // form is part of a search page
            if (CompanySearchPlugin.searchFormId != "") {
                if (CompanySearchPlugin.searchFormId.indexOf("search") != -1) {
                    // apply focus to the submit button so user can quickly initiate search
                    $j("#" + CompanySearchPlugin.searchFormId + ' input[type="submit"]').focus();
                }
            } else {
                // apply focus to the first form element in the next list item
                $j("#" + CompanySearchPlugin.boxDiv)
                    .parent("li")
                    .next("li")
                    .find('input:not([type="hidden"]), select')
                    .eq(0)
                    .focus();
            }
            // clear value in variable @see SearchPluginsKeyNav.js
            searchFOCUS = "";
        } else {
            // populate the coid hidden input field with the id of this anchor (ie coid)
            $j("#" + CompanySearchPlugin.idInput).attr("value", coid);
            // remove the 'selected' classname from the currently selected item
            $j("#" + CompanySearchPlugin.resultsDiv + " a")
                .eq(CompanySearchPlugin.selected)
                .removeClass();
            $j("#" + CompanySearchPlugin.resultsDiv + " a").each(function (i) {
                // id of this anchor matches the coid passed
                if (this.id == coid) {
                    // add 'selected' class to this anchor
                    this.className = "selected";
                    // populate the selected literal with this value
                    CompanySearchPlugin.selected = i;

                    // this plugin is part of a cascading search plugin so a dwr call to retrieve all subdivisions is made
                    // results are sent to the SubdivSearchPlugin 'subdivCallback' object literal
                    if (CompanySearchPlugin.cascade == true && CascadeSearchPlugin.cascadeRules.length > 0) {
                        SubdivSearchPlugin.runSubdivAjax(coid);
                    }
                }
            });

            // do we need to load currency info for selected company?
            if (typeof CascadeSearchPlugin != "undefined") {
                if (CascadeSearchPlugin.loadCurrency == true) {
                    CompanySearchPlugin.loadCurrency(coid);
                }
            }
            // populate 'searchFOCUS' with the 'CompanySearchPlugin' object
            // for use when cursor key navigation is being used
            searchFOCUS = CompanySearchPlugin;
        }
    },

    /**
     * this method loads currency info for the selected company
     */
    loadCurrency: function (coid) {
        // load the service javascript file if not already
        loadScript.loadScriptsDynamic(new Array("dwr/interface/supportedcurrencyservice.js"), true, {
            callback: function () {
                // load the company currency list
                supportedcurrencyservice.getCompanyCurrencyOptions(coid, {
                    callback: function (opts) {
                        // build a new currency select list
                        var content = "";
                        for (var i = 0; i < opts.length; i++) {
                            var curOpt = opts[i];
                            content += '<option value="' + curOpt.optionValue + '"';
                            if (curOpt.selected == true) {
                                content += 'selected="selected"';
                            }
                            content += ">";
                            content += curOpt.currency.currencyCode;
                            content += " @ " + curOpt.defaultCurrencySymbol + "1 = " + curOpt.currency.currencySymbol + "" + curOpt.rate;
                            if (curOpt.companyDefault == true) {
                                content += " [" + i18n.t("core.utilities:searchplugins.compDefaultRate", "Company Default Rate") + "]";
                            } else if (curOpt.systemDefault == true) {
                                content += " [" + i18n.t("core.utilities:searchplugins.systemDefaultRate", "System Default Rate") + "]";
                            }
                            content += "</option>";
                        }
                        $j("#currencyCode").empty().append(content);
                    },
                });
            },
        });
    },

    /**
     * this function closes the search plugin and clears all inputs
     */
    closePlugin: function () {
        $j("#" + CompanySearchPlugin.nameInput).attr("value", "");
        $j("#" + CompanySearchPlugin.idInput).attr("value", "");
        $j("#" + CompanySearchPlugin.resultsDiv + ", div#comp-container ." + CompanySearchPlugin.headDiv).css({
            zIndex: -1,
            display: "none"
        });

        /** clear value in variable @see SearchPluginsKeyNav.js */
        searchFOCUS = "";
    },

    /**
     * this function clears all search boxes of content, empties the values from the input fields and
     * displays a message of no results returned if necessary
     *
     * @param {Boolean} message is a boolean value (true if message to be displayed)
     */
    clearSearch: function (message) {
        // remove all previously displayed subdivisions
        $j("#" + SubdivSearchPlugin.resultsDiv).empty();
        // clear value in the subdivid hidden input field
        $j("#" + SubdivSearchPlugin.idInput).attr("value", "");
        if (message == true) {
            // create a div to display that no results have been returned
            $j("#" + SubdivSearchPlugin.resultsDiv).append(
                '<div class="bold center" style=" padding: 5px; ">' + i18n.t("core.utilities:searchplugins.NoSubdivisionsFound", "No Subdivisions Found") + "</div>"
            );
        }

        // search is to be cascaded further
        if (SubdivSearchPlugin.cascade == true) {
            // search being cascaded further using one criteria
            if (SubdivSearchPlugin.cascadeSearch.length == 1) {
                switch (SubdivSearchPlugin.cascadeSearch[0]) {
                    // criteria being cascaded is 'address' so empty contents
                    case "address":
                        $j("#" + AddressSearchPlugin.resultsDiv).empty();
                        // clear value in the addrid hidden input field
                        $j("#" + AddressSearchPlugin.idInput).attr("value", "");
                        if (message == true) {
                            // create a div to display that no results have been returned
                            $j("#" + AddressSearchPlugin.resultsDiv).append(
                                '<div class="bold center" style=" padding: 5px; ">' + i18n.t("core.utilities:searchplugins.noAddressesFound", "No Addresses Found") + "</div>"
                            );
                        }
                        break;
                    // criteria being cascaded is 'contact' so empty contents
                    case "contact":
                        $j("#" + ContactSearchPlugin.resultsDiv).empty();
                        // clear value in the hidden personid field
                        $j("#" + ContactSearchPlugin.idInput).attr("value", "");
                        if (message == true) {
                            // create div to display message when no contacts have been returned
                            $j("#" + ContactSearchPlugin.resultsDiv).append(
                                '<div class="bold center" style=" padding: 5px; ">' + i18n.t("core.utilities:searchplugins.noContactsFound", "No Contacts Found") + "</div>"
                            );
                        }
                        break;
                }
            }
            // search being cascaded further using two criteria
            else {
                // criteria being cascaded is 'address' and 'contact' so empty contents
                $j("#" + AddressSearchPlugin.resultsDiv).empty();
                // clear value in the addrid hidden input field
                $j("#" + AddressSearchPlugin.idInput).attr("value", "");

                $j("#" + ContactSearchPlugin.resultsDiv).empty();
                // clear value in the hidden personid field
                $j("#" + ContactSearchPlugin.idInput).attr("value", "");

                if (message == true) {
                    // create a div to display that no results have been returned
                    $j("#" + AddressSearchPlugin.resultsDiv).append(
                        '<div class="bold center" style=" padding: 5px; ">' + i18n.t("core.utilities:searchplugins.noContactsFound", "No Addresses Found") + "</div>"
                    );
                    // create div to display message when no contacts have been returned
                    $j("#" + ContactSearchPlugin.resultsDiv).append(
                        '<div class="bold center" style=" padding: 5px; ">' + i18n.t("core.utilities:searchplugins.noContactsFound", "No Contacts Found") + "</div>"
                    );
                }
            }
        }
    },
};
