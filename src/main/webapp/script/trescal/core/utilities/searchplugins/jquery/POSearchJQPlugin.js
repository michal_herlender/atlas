/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	poSearchJQPlugin.js
*	DESCRIPTION		:	 
*
*	DEPENDENCIES	:   script/thirdparty/jQuery/jquery.js
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	15/07/2008 - SH - Created File
*					:	29/10/2015 - TProvost - Fix bug on test of the version of browser
*					:	30/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

(function( $j )
{
	// variable to indicate whether any navigation keys have been pressed to stop ajax
	var keyPress = false;
	// variable to indicate which anchor in list is currently selected - used for key navigation
	var selected = 0;
	// variable to hold time value of last DWR call, checked against the current DWR call
	var lastUpdate = 0;
    
    $j.fn.poSearchJQPlugin = function( options )
    {
        var options = $j.extend({
			
			/**
			 * name of the input field
			 */
			field:			'',
			/**
			 * the job id
			 */
			jobid: 			'',
			/**
			 * default po if supplied
			 */
			defaultpo:		'',
			/**
			 * copy option allows the user to add a image link which will
			 * copy the chosen po to all other occurences on page. This option should
			 * only be provided for the first occurence of plugin.
			 */
			copyOption: 	'false',
			/**
			 * copy parent is an element in which all copies should be performed
			 * this should be defined with the jquery prefix for id or class name (i.e. #test, .test)
			 */
			copyParent:		'',
			/**
			 * container div
			 */
			container: 		'poContainerJQPlugin',
            /**
			 * class name of the results div
			 */
			resultsDiv: 	'poResultsJQPlugin',
			/**
			 * class name of the footer div
			 */
			footDiv: 		'poFootJQPlugin'
			
        }, options);

        return this.each(function()
        {
            poSearchJQPlugin(this, options);
        });
    };

    function poSearchJQPlugin( element, options )
    {
        return this instanceof poSearchJQPlugin
            ? this.init(element, options)
            : new poSearchJQPlugin(element, options);
    }

    $j.extend(poSearchJQPlugin.prototype,
    {
        original : null,
        options  : {},

        element  : null,
        searchDiv   : null,

		/**
		 * plugin initialisation function
		 * 
		 * @param {Object} element is the current plugin div to manipulate
		 * @param {Object} options are options for the current plugin
		 */
        init : function( element, options )
        {			
			// initialise search div
            this.searchDiv = element;
			// initialise options
            this.options = options || {};
			
            // script to load for dwr service
			var Scripts = new Array (	'dwr/interface/jobservice.js' );
			// load the common javascript files for this plugin
			loadScript.loadScripts(Scripts);
			
			// call method to draw plugin search
            this.drawSearch();
			
			// set this div object
			var self = this;
			// get job id
			var jobid = this.options.jobid;
			// add keyup event to the text input and bind method to call PO dwr service
			$j( $j(this.searchDiv).find('input[type="text"]:first') ).keyup(function() { self.runPOAjax(this.value, jobid, self); return false; });
			// add click event to the close image in footer div and bind method to close the plugin
			$j( $j(this.searchDiv).find('div.poContainerJQPlugin img:last') ).click(function() { self.closePlugin(); return false; });
			// copy selected po to other fields required
			if (this.options.copyOption == 'true') 
			{
				// add option to variable as it is not available in loop
				var copyParent = this.options.copyParent;
				// add click event to the anchor and bind method to call copy option method
				// for text input field and hidden input field
				$j($j(this.searchDiv).find('> a')).each(function(i)
				{
					// variable to hold direction of copy
					var direction = 'up';
					// first anchor
					if (i < 1)
					{
						direction = 'down';
					}
										
					// add click function to copy down anchor
					$j(this).click(function()
					{
						// call copy function
						self.copyOption($j(this).parent().find('input[type="text"]:first'), direction, copyParent);
						return false;
					});	
				});
			}
			
			/**
			 * start jquery keypress observer to check if the user has pressed the up/down arrow or the
			 * enter/esc key when searching for po's
			 * 
			 */ 	
			$j( $j(this.searchDiv) ).keydown(function(e) 
			{			
			    var event = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
				
				switch (event)
				{
					//  up arrow key has been pressed
					case 38: keyPress = true;
							 // check selected variable
							 if (selected == 0)
							 {
							 	
							 }
							 // selected variable contains value greater than zero
							 else
							 {
							 	 // remove 'selected' class name from the currently selected element and add to the previous one, apply focus
								 $j(this).find('> div div:first a').eq(selected).removeClass().prev().addClass('selected').focus();
								 // remove one from the selected variable value
								 selected = selected - 1;
								 // then return focus to the input field
								 $j(this).find('input[type="text"]:first').focus();
							 }
							 break;
							 
					// down arrow key has been pressed
					case 40: keyPress = true;
							 // check selected variable is not greater than list length
							 if (selected == $j(this).find('> div div:first a').size() - 1)
							 {
							 
							 }
							 // selected variable contains value less than list length
							 else
							 {
							 	// remove 'selected' class name from the currently selected element and add to the next one, apply focus
								$j(this).find('> div div:first a').eq(selected).removeClass().next().addClass('selected').focus();
								// remove one from the selected variable value
								selected = selected + 1;
								// then return focus to the input field
								$j(this).find('input[type="text"]:first').focus();
							 }
							 break;
							 
					// escape key has been pressed
					case 27: keyPress = true;
							 // call function to close plugin
							 self.closePlugin();
							 break;
							 
					// enter key has been pressed
					case 13: keyPress = true;
							 // trigger click event of anchor
						 	 $j(this).find('> div div:first a').eq(selected).trigger('click');
							 break;
					
					// left arrow key has been pressed, just stop ajax from being initiated if pressed accidentally
					case 37: keyPress = true;
							 break;
							 
					// right arrow key has been pressed, just stop ajax from being initiated if pressed accidentally
					case 39: keyPress = true;
							 break;
							 
					// tab key has been pressed, just stop ajax from being initiated if pressed accidentally
					case 9: keyPress = true;
							break;
				}
				
			});
        },
		
		/**
		 * method to draw plugin search
		 */
        drawSearch : function()
        {
			var content = 	'<input type="text" name="' + this.options.field + '" value="' + this.options.defaultpo + '" onkeypress=" if (event.keyCode==13){ return false; } " autocomplete="off" /><img src="img/icons/searchplugin.png" width="16" height="16" alt="PO Search" title="PO Search" />';
							// copy options required
							if 	(this.options.copyOption == 'true')
							{
								content = content +	'<a href="#">' +
														'<img src="img/icons/arrow_down.png" width="10" height="16" alt="' + i18n.t("copySelectionDown", "Copy selection down") + '" title="' + i18n.t("copySelectionDown", "Copy selection down") + '" />' +
													'</a>' + 
													'<a href="#">' +
														'<img src="img/icons/arrow_up.png" width="10" height="16" alt="' + i18n.t("copySelectionUp", "Copy selection up") + '" title="' + i18n.t("copySelectionUp", "Copy selection up") + '" />' +
													'</a>';
							}
		content = content +	'<div class="' + this.options.container + '">' +
								// create and append the div in which po results will be displayed
								'<div class="' + this.options.resultsDiv + '">' +
									// results are displayed here
								'</div>' +
								// create and append footer with close button
								'<div class="' + this.options.footDiv + '"><span style=" line-height: 18px; margin-right: 18px; ">(' + i18n.t("core.utilities:searchplugins.escKey", "Esc Key") + ') ' + i18n.t("core.utilities:searchplugins.or", "or") + ' </span><img src="img/icons/bullet-cross.png" width="10" height="10" alt="' + i18n.t("core.utilities:searchplugins.closePOSearch", "Close PO Search") + '" title="' + i18n.t("core.utilities:searchplugins.closePOSearch", "Close PO Search") + '" class="pluginClose" /></div>' +
							'</div>';
						
			// append plugin content to main div
			$j(this.searchDiv).append(content);
			// mozilla or opera browser						
			if ($j.browser != null && ($j.browser.mozilla != null || $j.browser.opera != null))
			{
				$j(this.searchDiv).parent('li').css({ paddingBottom: '4px'});
			}
			// other browsers
			else
			{
				$j(this.searchDiv).parent('li').css({ paddingBottom: '3px'});
			}
			
        },
		
		/**
		 * method which accepts the name fragment provided from the input field and
		 * calls the dwr service to retrieve manufacturers
		 * 
		 * @param {String} nameFragment text entered into input field which should be used to search PO's
		 * @param {Integer} jobId the id of the current job
		 * @param {Object} self reference to this object
		 */
		runPOAjax : function(nameFragment, jobId, self)
		{			
			// navigation keys have been pressed so set value back to false and skip ajax loading
			if (keyPress == true)
			{
				keyPress = false;
			}
			else
			{
				// another search is in use
				if ( ($j( $j(this.searchDiv).find('> div div:first a') ).length < 1) && ($j('div.' + this.options.resultsDiv + ' > a').length > 0) )
				{
					// reset input field
					$j( $j(this.searchDiv).find('input[type="text"]:first') ).attr('value', '');
					// alert user that another plugin is in use
					$j.prompt(i18n.t("core.utilities:searchplugins.anotherPluginIsInUse", "Another plugin is in use, please close before continuing!"));
					// exit method
					return false;
				}							
				// get current timestamp to pass with ajax results
				var currentTime = new Date().getTime();
				// only execute the ajax functionality if the text entered has a length
				// greater than zero (stops unconstrained retrieval)
				if (nameFragment.length > 0)
				{					
					// call dwr service to retrieve PO's
					jobservice.searchMatchingJobPOs(jobId, nameFragment,
					{
						callback:function(pos)
						{
							// call method to display PO's returned
							self.poCallback(pos, currentTime, self);
						}
					});
				}
				// text entered has a length of 0
				else
				{														
				 	// hide the PO results container
					$j( $j(this.searchDiv).find('> div div') ).css({zIndex: -1, display: 'none'});
					// remove all previously displayed PO's
					$j( $j(this.searchDiv).find('> div div:first') ).empty();				
				}
			}
			
		},
		
		/**
		 * this method appends all po's returned from the dwr service to the results div
		 * 
		 * @param {Array} pos is an array of pos returned from the dwr service
		 * @param {Date/Time} currentUpdate is a timestamp retrieved when the search was initiated
		 * @param {Object} self reference to this object
		 */
		poCallback : function(pos, currentUpdate, self) 
		{
			// this is a new search
			if (currentUpdate > lastUpdate)
			{
				// update value in the 'lastUpdate' variable
				lastUpdate = currentUpdate;
				// PO's length is greater than zero
				if(pos.length > 0)
				{
					// remove all previously displayed PO's
					$j( $j(this.searchDiv).find('> div div:first') ).empty();
					// display the PO results container
					$j( $j(this.searchDiv).find('> div div') ).css({zIndex: 100, display: 'block'});
					// remove all previously displayed PO's
					$j( $j(this.searchDiv).find('> div div:first') ).empty();
					// declare variable to hold results content
					var content = '';
					// add anchor for each PO/BPO returned to results variable
					$j(pos).each(function(i)
					{
						content = content + '<a href="#" id="' + pos[i].poId + '"><span class="number">' + pos[i].poNumber + '</span>';
						if (pos[i].BPO)
						{
							content = content + '<span class="type">' + i18n.t("core.utilities:searchplugins.bpo", "BPO") + '</span></a>';					
						}
						else
						{
							content = content + '<span class="type">' + i18n.t("core.utilities:searchplugins.po", "PO") + '</span></a>';
						}	
																																									
					});
					// append results variable to results div
					$j( $j(this.searchDiv).find('> div div:first') ).append(content);
					
					// loop through all anchors appended to results div
					$j( $j(this.searchDiv).find('> div div:first a') ).each(function(i)
					{
						// apply click event to anchor
						$j( $j(this) ).click( function(){ self.selectPOId($j(this).children('span:first').text(), self); return false; } );
					});			
					// add 'selected' class name to first anchor in results div
					$j( $j(this.searchDiv).find('> div div:first a:first') ).addClass('selected');
					// initialise the selected variable to zero
					selected = 0;																					
				}
				else
				{
					// hide the PO results container
					$j( $j(this.searchDiv).find('> div div') ).css({zIndex: -1, display: 'none'});
					// remove all previously displayed PO's
					$j( $j(this.searchDiv).find('> div div:first') ).empty();
				}
			}
		},
		
		/**
		 * this method selects the chosen PO and adds the name to the text input field and adds the
		 * name to the hidden input field after which the results div is emptied and hidden
		 * 
		 * @param {String} po name of the po chosen
		 * @param {Object} self reference to this object
		 */
		selectPOId : function (po, self)
		{
			// add PO text to the search text input field
			$j( $j(this.searchDiv).find('> input[type="text"]:first') ).val(po);
			// empty the results div
			$j( $j(this.searchDiv).find('> div div:first') ).empty();
			// hide the search results box and footer
			$j( $j(this.searchDiv).find('> div div') ).css({zIndex: -1, display: 'none'});
		},
	
		/**
		 * this function closes the search plugin and clears all inputs
		 */				
		closePlugin : function()
		{
			// clear the text input field and apply focus 
			$j( $j(this.searchDiv).find('input[type="text"]:first') ).focus();
			// empty the results div
			$j( $j(this.searchDiv).find('> div div:first') ).empty();
			// hide the search results box and footer
			$j( $j(this.searchDiv).find('> div div') ).css({zIndex: -1, display: 'none'});
		},
		
		/**
		 * this method copies the values selected in this plugin instance to any others
		 * present on the page.
		 *
		 * @param {Object} element element which we can obtain value and name for copying
		 * @param {String} direction the direction in which the value should be copied
		 * @param {String} copyParent is an element in which all po plugins appear and should be copied in
		 */
		copyOption : function (element, direction, copyParent)
		{
			// add marker after element
			$j(element).after('<span class="marker"></span>');
			// get the input name attribute
			var elementName = $j(element).attr('name');
			// variable to indicate the marker has been found
			var markerFound = false;
			// array to hold all inputs
			var inputArray = new Array();
			// format copy parent variable accordingly if not empty
			if (copyParent != '')
			{
				copyParent += ' ';
			}
			// default direction is to copy information down the list
			// does element name contain brace?
			if (elementName.indexOf('[') != -1)
			{
				// get the start of the element name before (first) square brace
				var elementStart = elementName.substring(0, elementName.indexOf('['));
				// create an array of input objects
				inputArray = $j.makeArray($j(copyParent + 'input[name^="' + elementStart + '"]'));
			}
			else
			{
				// create an array of input objects
				inputArray = $j.makeArray($j(copyParent + 'input[name="' + $j(element).attr('name') + '"]'));
			}	
			if (direction == 'up')
			{				
				// reverse the array
    			inputArray.reverse();
			}
			// loop through all matching inputs
			$j.each(inputArray, function()
			{
				// marker found?
				if ($j(this).next().hasClass('marker'))
				{
					// set variable and remove marker
					markerFound = true;
					$j(this).next().remove();
				}
				// marker found?
				if (markerFound)
				{
					// copy value to input
					$j(this).val($j(element).val());
				}					
			});		
		}
        
    });
})(jQuery);