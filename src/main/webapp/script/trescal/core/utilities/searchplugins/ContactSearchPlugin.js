/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	ContactSearchPlugin.js
*	DESCRIPTION		:	Javascript file imported when creating a contact search on a page
* 					:	Code creates two input fields, one for searching contacts and the other for holding the personid of a
* 					:	selected contact, a results section is then created below to display contacts returned.
* 					:	Further code uses dwr to get contact results and display them on the page
* 					: 	This plugin is also used to search contacts in the cascading search plugin.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	File Created - SH - 25/07/2007
*					:	29/10/2015 - TProvost - Fix bug on test of the version of browser
*					:	30/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/
 
/**
 * this literal is called when the page loads if an element with the id of 'contSearchPlugin' is found on the page.
 * this function then loads all the necessary scripts it needs and checks whether the search should be cascaded
 * further than this point or not, else we create a normal contact search?
 * 
 * @param {Boolean} cascade is a boolean value to indicate whether this search should be cascaded
 */
ContactSearchPlugin = {
	init: function (cascade) {
		// scripts to load
		var Scripts = new Array("script/trescal/core/utilities/keynavigation/SearchPluginsKeyNav.js", "script/thirdparty/jQuery/jquery.scrollTo.js");
		// load the common javascript files for this plugin
		loadScript.loadScripts(Scripts);
		// set to true if this plugin is to be cascaded further
		if (cascade == true) {
			ContactSearchPlugin.cascade = true;
			ContactSearchPlugin.inCascade = true;
			// draw cascade search plugin
			ContactSearchPlugin.drawCascade();
			// put group id rules into object literal array
		}
			// do not cascade any further but check whether the CascadeSearchPlugin has been
			// defined? If so, then we should draw the cascade search as this is the last search
		// criteria
		else {
			// this is last search criteria so draw cascade search
			if (typeof CascadeSearchPlugin != "undefined") {
				ContactSearchPlugin.cascade = false;
				ContactSearchPlugin.inCascade = true;
				// draw cascade search plugin
				ContactSearchPlugin.drawCascade();
				// put group id rules into object literal array
			}
			// no cascade search so draw the normal contact search plugin
			else {
				// contact search has hidden input for tabindex
				if ($j("input#contindex").length) {
					// set tabindex value
					ContactSearchPlugin.tabindex = $j("input#contindex").val();
				}
				// contact search has hidden input for first name to be prefilled
				if ($j("input#firstnamePrefill").length) {
					// set first name value
					ContactSearchPlugin.firstnamePrefill = $j("input#firstnamePrefill").val();
				}
																		// contact search has hidden input for last name to be prefilled
				if ($j("input#lastnamePrefill").length) {
					// set last name value
					ContactSearchPlugin.lastnamePrefill = $j("input#lastnamePrefill").val();
				}
																		ContactSearchPlugin.setCoroles();
																		// draw normal search plugin
																		ContactSearchPlugin.drawSearch();
																	}
																}
															},
	/**
	 * if the input with an id of 'contCoroles' is present and contains coroles separated by comma's then
	 * add these values to the coroles literal array. If not present then add default 'client' corole.
	 */
	setCoroles: function () {
		if ($j("input#contCoroles").length) {
			$j("input#contCoroles").each(function () {
				ContactSearchPlugin.coroles = this.value.split(",");
			});
		} else {
			ContactSearchPlugin.coroles = "client";
		}
	},
	/**
	 * first name to be prefilled
	 */
	firstnamePrefill: "",
	/**
	 * last name to be prefilled
	 */
	lastnamePrefill: "",
	/**
	 * tab index for input field
	 */
	tabindex: 0,
	/**
	 * array of company roles sourced by setCoroles
	 */
	coroles: [],
	/**
	 * indicates whether this search is in a cascade
	 */
	inCascade: false,
	/**
	 * false by default, if true then this cascading search should continue and use the selected subdivision subdivid to
	 * call a dwr search for address results
	 */
	cascade: false,
	/**
	 * id of the first name input field
	 */
	firstInput: "firstName",
	/**
	 * id of the last name input field
	 */
	lastInput: "lastName",
	/**
	 * id of the hidden input field used to pass the contact personid
	 */
	idInput: "personid",
	/**
	 * id of the main div declared in the page where the search should be created
	 */
	boxDiv: "contSearchPlugin",
	/**
	 * id of the div in which the headings for the search are created
	 */
	headDiv: "pluginHead",
	/**
	 * id of the div in which the search results are to be displayed
	 */
	resultsDiv: "contPlugResults",
	/**
	 * class name used on the results div to apply styles
	 */
	styleResDiv: "pluginResults",
	/**
	 * stores id of the form in which this search resides
	 */
	searchFormId: "",
	/**
	 * id of the form element on which focus should be placed
	 */
	inputFocus: "lastName",
	/**
	 * boolean value used to flag if a navigation cursor key has been pressed so no ajax is called
	 */
	keyPress: false,
	/**
	 * value of the currently selected element in the list
	 */
	selected: 0,
	/**
	 * time value of last DWR call, checked against the current DWR call
								 */
								lastUpdate:		0,
	/**
	 * creates the input fields and results div on the page for a normal contact search plugin
	 */
	drawSearch: function () {
		$j("#" + ContactSearchPlugin.boxDiv).append(
			'<input type="hidden" id="' +
			ContactSearchPlugin.idInput +
			'" name="' +
			ContactSearchPlugin.idInput +
			'" />' +
			'<div class="searchfields">' +
			// create and append the inputs in which the user will search for contacts
			// set autocomplete to off and add the onkeyup event to capture key presses
			i18n.t("core.utilities:plugins.firstNameShortLabel", "First:") +
			' <input type="text" value="' +
			ContactSearchPlugin.firstnamePrefill +
			'" id="' +
			ContactSearchPlugin.firstInput +
			'" name="' +
			ContactSearchPlugin.firstInput +
			'" onkeyup="ContactSearchPlugin.runContactAjax(this.value, $j(\'#' +
			ContactSearchPlugin.lastInput +
			'\').val()); return false;" tabindex="' +
			ContactSearchPlugin.tabindex +
			'" autocomplete="off" />' +
			i18n.t("core.utilities:plugins.lastNameShortLabel", "Last:") +
			' <input type="text" value="' +
			ContactSearchPlugin.lastnamePrefill +
			'" id="' +
			ContactSearchPlugin.lastInput +
			'" name="' +
			ContactSearchPlugin.lastInput +
			'" onkeyup="ContactSearchPlugin.runContactAjax($j(\'#' +
			ContactSearchPlugin.firstInput +
			'\').val(), this.value); return false;" tabindex="' +
			(parseInt(ContactSearchPlugin.tabindex) + 1) +
			'" autocomplete="off" />' +
			"</div>" +
			'<img src="img/icons/searchplugin.png" width="16" height="16" alt="' +
			i18n.t("core.utilities:searchplugins.optionSearch", "Option Search") +
			'" title="' +
			i18n.t("core.utilities:searchplugins.optionSearch", "Option Search") +
			'" />' +
			'<div id="cont-container">' +
			// create and append header
			'<div class="' +
			ContactSearchPlugin.headDiv +
			'">' +
			i18n.t("core.utilities:searchplugins.matchingContacts", "Matching Contacts") +
			'<img src="img/icons/bullet-cross.png" width="10" height="10" alt="' +
			i18n.t("core.utilities:searchplugins.closeContactSearch", "Close Contact Search") +
			'" title="' +
			i18n.t("core.utilities:searchplugins.closeContactSearch", "Close Contact Search") +
			'" class="pluginClose" onclick=" ContactSearchPlugin.closePlugin(); return false; " /></div>' +
			// create and append the div in which contact results will be displayed
			'<div id="' +
			ContactSearchPlugin.resultsDiv +
			'" class="' +
			ContactSearchPlugin.styleResDiv +
			'">' +
			// results are displayed here
			"</div>" +
			"</div>"
		);

		if ($j.browser.mozilla != null || $j.browser.opera != null) {
			$j("#" + ContactSearchPlugin.boxDiv)
				.parent("li")
				.css({paddingBottom: "4px"});
		} else {
			$j("#" + ContactSearchPlugin.boxDiv)
				.parent("li")
				.css({paddingBottom: "3px"});
		}

		if (this.form) {
			// find out the id of the form in which this search resides and add to literal
			$j("input#" + ContactSearchPlugin.firstInput).each(function () {
				// return the form object
				ContactSearchPlugin.searchFormId = this.form.id;
			});
		}
	},
	/**
	 * creates the input fields and results div on the page for a cascading contact search plugin
	 */
	drawCascade: function () {
		$j("#" + ContactSearchPlugin.boxDiv).append(
			'<input type="hidden" id="' +
			ContactSearchPlugin.idInput +
			'" name="' +
			ContactSearchPlugin.idInput +
			'" />' +
			'<div id="cont-container">' +
			// create and append the header div
			'<div class="' +
			ContactSearchPlugin.headDiv +
			'">' +
			i18n.t("core.utilities:searchplugins.matchingContacts", "Matching Contacts") +
			"</div>" +
			// create and append the div in which company results will be displayed
			'<div id="' +
			ContactSearchPlugin.resultsDiv +
			'" class="' +
			ContactSearchPlugin.styleResDiv +
			'">' +
			// results are displayed here
			"</div>" +
			"</div>"
		);

		if (this.form) {
			// find out the id of the form in which this search resides and add to literal
			$j("input#" + CompanySearchPlugin.nameInput).each(function () {
				// return the form object
				ContactSearchPlugin.searchFormId = this.form.id;
			});
		}
												},

	/**
	 * This function accepts a value entered by the user and calls an ajax method to retrieve
	 * contacts matching this value from the server.  The returned data is then passed on to function
	 * (contactCallback) for displaying on the page.
	 *
	 * @param {String} firstName is the value entered in the first name text field.
	 * @param {String} lastName is the value entered in the last name text field.
	 */
	runContactAjax: function (firstName, lastName) {
		// check that another search plugin is not currently in use
		if (searchFOCUS != ContactSearchPlugin && searchFOCUS != "") {
			searchFOCUS.closePlugin();
		}

		// the up/down arrow key has been pressed so set value back to false and skip ajax loading
		if (ContactSearchPlugin.keyPress == true) {
			ContactSearchPlugin.keyPress = false;
		} else {
			// populate the searchFOCUS variable with this 'ContactSearchPlugin' object
			// for use when cursor key navigation is being used
			searchFOCUS = ContactSearchPlugin;

			// remove all previously displayed contacts
			$j("#" + ContactSearchPlugin.resultsDiv).empty();

			if (ContactSearchPlugin.cascade == false) {
				// display the contact results container
				$j("#" + ContactSearchPlugin.resultsDiv + ", div#cont-container ." + ContactSearchPlugin.headDiv).css({
					zIndex: 100,
					display: "block"
				});
			}
			// this span displays the loading image and text when a search has been activated
			$j("#" + ContactSearchPlugin.resultsDiv).append(
				'<div class="loading-plug">' + '<img src="img/icons/loading-anim.gif" class="img_marg_bot" height="18" width="80" alt="" />' + "</div>"
			);

			// set active variable to true to return all currently active contacts
			var active = true;

			// get current timestamp to pass with ajax results
			var currentTime = new Date().getTime();

			// only execute the ajax functionality if the text entry has a length greater
			// than zero (stops unconstrained retrieval)
			if (firstName.length > 0 || lastName.length > 0) {
				// load the service javascript file if not already
				loadScript.loadScriptsDynamic(new Array("dwr/interface/contactservice.js"), true, {
					callback: function () {
						contactservice.searchContactByRolesHQL(firstName, lastName, active, ContactSearchPlugin.coroles, {
							callback: function (dataFromServer) {
								ContactSearchPlugin.contactCallback(dataFromServer, currentTime);
							},
						});
					},
				});
			} else {
				if (ContactSearchPlugin.cascade == false) {
					// hide the contact results container
					$j("#" + ContactSearchPlugin.resultsDiv + ", div#cont-container ." + ContactSearchPlugin.headDiv).css({
						zIndex: -1,
						display: "none"
					});
					// clear value in variable @see SearchPluginsKeyNav.js
					searchFOCUS = "";
				}
				// remove all previously displayed contacts
				$j("#" + ContactSearchPlugin.resultsDiv).empty();
				// clear value in the hidden personid field
				$j("#" + ContactSearchPlugin.idInput).attr("value", "");
			}
													}
												},
	/**
	 * this function accepts a subdivid passed and calls an ajax method to retrieve addresses which match this value
	 * from the server.  The returned data is then passed on to function (contactCallback) for displaying on the page.
	 *
	 * @param {Integer} subdivid is the value passed to this literal from a subdivision search literal.
	 */
	runCascContactAjax: function (subdivid) {
		// display the contact results container
		$j("#" + ContactSearchPlugin.resultsDiv + ", div#cont-container ." + ContactSearchPlugin.headDiv).css({
			zIndex: 100,
			display: "block"
		});

		// remove all previously displayed contacts
		$j("#" + ContactSearchPlugin.resultsDiv).empty();

		// this span displays the loading image and text when a search has been activated
		$j("#" + ContactSearchPlugin.resultsDiv).append(
			'<div class="loading-plug">' + '<img src="img/icons/loading-anim.gif" class="img_marg_bot" height="18" width="80" alt="" />' + "</div>"
		);

		// only execute the following code if a subdivid has been passed
		if (subdivid != "") {
			// get current timestamp to pass with ajax results
			var currentTime = new Date().getTime();
			// load the service javascript file if not already
			loadScript.loadScriptsDynamic(new Array("dwr/interface/contactservice.js"), true, {
				callback: function () {
					// call subdiv dwr function using the subdivid
					contactservice.getAllActiveSubdivContactsHQL(subdivid, {
						callback: function (dataFromServer) {
							ContactSearchPlugin.contactCallback(dataFromServer, currentTime, subdivid);
						},
					});
				},
			});

			// cascade plugin?
			if (typeof CascadeSearchPlugin != "undefined") {
				// contact link present?
				if ($j("#" + CascadeSearchPlugin.linksDiv + " span#cascAddCont").length) {
					// remove old add contact link
					$j("#" + CascadeSearchPlugin.linksDiv + " span#cascAddCont a").attr("href", springUrl + "/addperson.htm?subdivid=" + subdivid);
				} else {
					// append link for adding contact
					$j("#" + CascadeSearchPlugin.linksDiv).append(
						'<span id="cascAddCont">&nbsp;-&nbsp;<a href="' +
						springUrl +
						"/addperson.htm?subdivid=" +
						subdivid +
						'" target="_blank" class="mainlink">' +
						i18n.t("core.utilities:searchplugins.addContact", "Add Contact") +
						"</a></span>"
					);
				}
			}
		} else {
			// remove all previously displayed contacts
			$j("#" + ContactSearchPlugin.resultsDiv).empty();
			// clear value in the hidded personid field
			$j("#" + ContactSearchPlugin.idInput).attr("value", "");
			// hide the contact results container
			$j("#" + ContactSearchPlugin.resultsDiv + ", div#cont-container ." + ContactSearchPlugin.headDiv).css({
				zIndex: -1,
				display: "none"
			});
			// contact link present?
			if ($j("#" + CascadeSearchPlugin.linksDiv + " span#cascAddCont").length) {
				// remove old add contact link
				$j("#" + CascadeSearchPlugin.linksDiv + " span#cascAddCont").remove();
			}
														}
													},
	/**
	 * this function displays the results returned from the server for ajax call 'runContactAjax' and 'runCascContactAjax'
	 * if this plugin is to be cascaded further by showing addresses
	 * search results then a new dwr search should be instantiated
	 *
	 * @param {Array} contacts is an array of contact objects returned from the server.
	 * @param {Date/Time} currentUpdate is a date/time value of data retrieval from the server.
	 * @param {Integer} subdivid is the id used to get the contact results will also be needed to get the address results
	 */
	contactCallback: function (contacts, currentUpdate, subdivid) {
		// this is a new search
		if (currentUpdate > ContactSearchPlugin.lastUpdate) {
			// update value in the 'lastUpdate' literal
			ContactSearchPlugin.lastUpdate = currentUpdate;

			// contacts length is greater than zero
			if (contacts.length > 0) {
				// remove all previously displayed contacts
				$j("#" + ContactSearchPlugin.resultsDiv).empty();

				// CascadeSearchPlugin has been defined?
				if (typeof CascadeSearchPlugin != "undefined") {
					// this search is to be prefilled
					if (CascadeSearchPlugin.prefillIds.length > 0 && CascadeSearchPlugin.prefillFinalised == false) {
						// this variable is used to hold the index of personid in the 'prefillIds'
						// array according to how the cascade rules array was set
						var prefillLocation = 0;
						// contact was the first value stored in 'cascadeRules' so it should be
						// the third value supplied in 'prefillIds' array
						if (CascadeSearchPlugin.cascadeRules[1] == "contact") {
							prefillLocation = 2;
						}
							// contact was the second value stored in 'cascadeRules' so it should be
						// the fourth value supplied in 'prefillIds' array
						else {
							prefillLocation = 3;
						}
																		// variable used to store the index of the contact who matches the personid
																		// supplied in the 'prefillIds' array
																		var prefillId = 0;
																		// declare variable to hold contact results
						var content = "";
																		// add anchor for each contact returned
						$j.each(contacts, function (i) {
							// personid's match so populate variable
							if (CascadeSearchPlugin.prefillIds[prefillLocation] == contacts[i].personid) {
								prefillId = i;
							}
							content +=
								'<a href="#" id="' +
								contacts[i].personid +
								'" onclick="event.preventDefault(); ContactSearchPlugin.linkOnclick(this.id, \'\');"><span>' +
								contacts[i].name +
								"</span></a>";
						});

						// append content to results box
						$j("#" + ContactSearchPlugin.resultsDiv).append(content);
						// populate the personid hidden input field with the personid of preselected contact
						$j("#" + ContactSearchPlugin.idInput).attr("value", contacts[prefillId].personid);
						// add 'selected' class name to the preselected contact
						$j("#" + ContactSearchPlugin.resultsDiv + " a")
							.eq(prefillId)
							.addClass("selected");
						// scroll to the selected contact
						$j("#" + ContactSearchPlugin.resultsDiv).scrollTo("a.selected", 0);
						// populate the selected literal with the index of the preselected contact
						ContactSearchPlugin.selected = prefillId;
						// check to see if BPOs should be loaded
						if (CascadeSearchPlugin.searchBPO == true) {
							ContactSearchPlugin.searchBPO(contacts[prefillId].personid);
						}
						// check to see if purchase order discounts should be found?
						if (CascadeSearchPlugin.sourceDiscount == true) {
							ContactSearchPlugin.sourceDiscount(contacts[prefillId].personid);
						}
						if (CascadeSearchPlugin.sourceNominal == true) {
							ContactSearchPlugin.sourceNominal(contacts[prefillId].personid);
						}
					} else {
						// declare variable to hold contact results
						var content = "";
						var selectHighlighted = false;
						var highlightedIndex = -1;
						// add anchor for each contact returned
						$j.each(contacts, function (i) {
							content +=
								'<a href="#" id="' + contacts[i].personid + '" onclick="event.preventDefault(); ContactSearchPlugin.linkOnclick(this.id, \'\');"><span ';

							if (contacts[i].highlight || contacts[i].defaultContact) {
								if (contacts[i].defaultContact) {
									content +=
										'class="defcont attention" title="' +
										i18n.t("core.utilities:searchplugins.subdivDefaultAddress", "This is the subdivision default contact") +
										'"';
								}
								if (contacts[i].highlight) {
									content +=
										'class="calofficer attention" title="' +
										i18n.t("core.utilities:searchplugins.subdivCalOfficer", "This is the subdivision calibration officer") +
										'"';
								}
								// calibration officer found
								selectHighlighted = true;
								// index has not been set
								if (highlightedIndex == -1) {
									highlightedIndex = i;
								}
							}

							content += ">" + contacts[i].name + "</span></a>";
																		});
																		// append content to results box
						$j("#" + ContactSearchPlugin.resultsDiv).append(content);
																		// calibration officer contact highlighted
																		if (selectHighlighted && CascadeSearchPlugin.autoselect)
																		{
																			if ($j('#' + ContactSearchPlugin.resultsDiv + ' span.calofficer:first').length > 0) {
																				// add 'selected' class name to first highlighted anchor
																				$j("#" + ContactSearchPlugin.resultsDiv + " span.calofficer:first")
																					.parent()
																					.addClass("selected");
																			} else {
																				// add 'selected' class name to first highlighted anchor
																				$j("#" + ContactSearchPlugin.resultsDiv + " span.defcont:first")
																					.parent()
																					.addClass("selected");
																			}
																			// scroll to the selected contact
																			$j("#" + ContactSearchPlugin.resultsDiv).scrollTo("a.selected", 0);
																			// initialise the selected literal to highlighted index
																			ContactSearchPlugin.selected = highlightedIndex;
																			// populate the personid hidden input field with the current personid
																			$j("#" + ContactSearchPlugin.idInput).attr("value", contacts[highlightedIndex].personid);
																			// check to see if BPOs should be loaded
																			if (CascadeSearchPlugin.searchBPO == true) {
																				ContactSearchPlugin.searchBPO(contacts[highlightedIndex].personid);
																			}
																			// check to see if purchase order discounts should be found?
																			if (CascadeSearchPlugin.sourceDiscount == true) {
																				ContactSearchPlugin.sourceDiscount(contacts[highlightedIndex].personid);
																			}
																			if (CascadeSearchPlugin.sourceNominal == true) {
																				ContactSearchPlugin.sourceNominal(contacts[highlightedIndex].personid);
																			}
																		} else {
																			if (CascadeSearchPlugin.autoselect) {
																				// add 'selected' class name to first anchor
																				$j("#" + ContactSearchPlugin.resultsDiv + " a:first").addClass("selected");
																				// initialise the selected literal to zero
																				ContactSearchPlugin.selected = 0;
																				// populate the personid hidden input field with the current personid
																				$j("#" + ContactSearchPlugin.idInput).attr("value", contacts[0].personid);
																				// check to see if BPOs should be loaded
																				if (CascadeSearchPlugin.searchBPO == true) {
																					ContactSearchPlugin.searchBPO(contacts[0].personid);
																				}
																				// check to see if purchase order discounts should be found?
																				if (CascadeSearchPlugin.sourceDiscount == true) {
																					ContactSearchPlugin.sourceDiscount(contacts[0].personid);
																				}
																				if (CascadeSearchPlugin.sourceNominal == true) {
																					ContactSearchPlugin.sourceNominal(contacts[0].personid);
																				}
																			}
																		}
					}
				} else {
					// declare variable to hold contact results
					var content = "";
					// add anchor for each contact returned
					$j.each(contacts, function (i) {
						content =
							content +
							'<a href="#" id="' +
							contacts[i].personid +
							'" onclick="event.preventDefault(); ContactSearchPlugin.linkOnclick(' +
							contacts[i].personid +
							", '" +
							escape(contacts[i].name) +
							"');\"><span>" +
							contacts[i].name +
							"</span></a>";
					});
																	// append content to results box
					$j("#" + ContactSearchPlugin.resultsDiv).append(content);
																	// populate the personid hidden input field with the current personid
					$j("#" + ContactSearchPlugin.idInput).attr("value", contacts[0].personid);
																	// add 'selected' class name to first anchor
					$j("#" + ContactSearchPlugin.resultsDiv + " a:first").addClass("selected");
					// initialise the selected literal to zero
					ContactSearchPlugin.selected = 0;
				}

				// search is to be cascaded further
				if (ContactSearchPlugin.cascade == true) {
					// criteria to be cascaded is 'address' so call ajax using subdivid
					AddressSearchPlugin.runAddressAjax(subdivid);
				}
			} else {
				// remove all previously displayed contacts and create div to display message when no contacts have been returned
				$j("#" + ContactSearchPlugin.resultsDiv)
					.empty()
					.append('<div class="bold center" style=" padding: 5px; ">' + i18n.t("core.utilities:searchplugins.noContactsFound", "No Contacts Found") + "</div>");
				// clear value in the hidded personid field
				$j("#" + ContactSearchPlugin.idInput).attr("value", "");
			}
														}	
														
														// execute callback function if exists
		if (CascadeSearchPlugin.contactCallBackFunction != "") window[CascadeSearchPlugin.contactCallBackFunction]();
													},
	/**
	 * this function moves the 'selected' classname to an anchor which the user has clicked on and populates the hidden input
	 * field with the selected contacts personid.
	 *
	 * @param {Integer} personid is the id of the address being clicked
	 */
	linkOnclick: function (personid, name) {
		name = unescape(name);
		if (ContactSearchPlugin.inCascade == false) {
			var firstname = name.substring(0, name.lastIndexOf(" "));
			var lastname = name.substring(name.lastIndexOf(" ") + 1, name.length);
			// hide the contacts results container
			$j("#" + ContactSearchPlugin.resultsDiv + ", div#cont-container ." + ContactSearchPlugin.headDiv).css({
				zIndex: -1,
				display: "none"
			});
			$j("#" + ContactSearchPlugin.firstInput).attr("value", firstname);
			$j("#" + ContactSearchPlugin.lastInput).attr("value", lastname);
			// populate the personid hidden input field with the id of this anchor (ie personid)
			$j("#" + ContactSearchPlugin.idInput).attr("value", personid);
			// apply focus to the first form element in the next list item
			$j("#" + ContactSearchPlugin.boxDiv)
				.parent("li")
				.next("li")
				.find('input:not([type="hidden"]), select')
				.eq(0)
				.focus();
			// form is part of a search page
			if (ContactSearchPlugin.searchFormId) {
				if (
					$j("#" + ContactSearchPlugin.searchFormId)
						.attr("id")
						.indexOf("search") != -1
				) {
					// apply focus to the submit button so user can quickly initiate search
					$j("#" + ContactSearchPlugin.searchFormId + ' input[type="submit"]').focus();
				}
			}
			// clear value in variable @see SearchPluginsKeyNav.js
			searchFOCUS = "";
		} else {
			// populate the personid hidden input field with the id of this anchor (ie personid)
			$j("#" + ContactSearchPlugin.idInput).attr("value", personid);
			// remove the 'selected' classname from the currently selected item
			$j("#" + ContactSearchPlugin.resultsDiv + " a")
				.eq(ContactSearchPlugin.selected)
				.removeClass();
			$j("#" + ContactSearchPlugin.resultsDiv + " a").each(function (i) {
				// id of this anchor matches the personid passed
				if (this.id == personid) {
					// add 'selected' classname to this anchor
					this.className = "selected";
					// populate the selected literal with this value
					ContactSearchPlugin.selected = i;
				}
			});
			// form is part of a search page
			if (ContactSearchPlugin.searchFormId != "") {
				if (
					$j("#" + ContactSearchPlugin.searchFormId)
						.attr("id")
						.indexOf("search") != -1
				) {
					// apply focus to the submit button so user can quickly initiate search
					$j("#" + ContactSearchPlugin.searchFormId + ' input[type="submit"]').focus();
				}
			} else {
				// apply focus to the first form element in the next list item
				$j("#" + ContactSearchPlugin.boxDiv)
					.parent("li")
					.next("li")
					.find('input:not([type="hidden"]), select')
					.eq(0)
					.focus();
			}
														// CascadeSearchPlugin is defined so don't add ContactSearchPlugin object to searchFOCUS
			if (typeof CascadeSearchPlugin != "undefined") {
				// check to see if BPOs should be loaded
				if (CascadeSearchPlugin.searchBPO == true) {
					ContactSearchPlugin.searchBPO(personid);
				}
				// check to see if purchase order discounts should be found?
				if (CascadeSearchPlugin.sourceDiscount == true) {
					ContactSearchPlugin.sourceDiscount(personid);
				}
				if (CascadeSearchPlugin.sourceNominal == true) {
					ContactSearchPlugin.sourceNominal(personid);
				}
			} else {
				// populate 'searchFOCUS' with the 'ContactSearchPlugin' object
				// for use when cursor key navigation is being used
				searchFOCUS = ContactSearchPlugin;
			}
													}
												},

	searchBPO: function (personid) {
		// load the service javascript file if not already
		loadScript.loadScriptsDynamic(new Array("dwr/interface/bposervice.js"), true, {
			callback: function () {
				// call dwr service to retrieve all available BPO's related to job
				bposervice.getAllBPOsRelatedToJob(personid, {
					callback: function (BPOs) {
						// BPO's returned
						if (BPOs.size > 0) {
							// empty the table body
							$j("table#itemBPOResults tbody").empty();
							// initialise content variable
							var itemContent = "";
							// contact BPO's returned
							if (BPOs.contactBPOs.length > 0) {
								// append contact heading row to string
								itemContent += "<tr>" + '<td colspan="7">BPO assigned to: ' + BPOs.contactName + "</td>" + "</tr>";

								// append row for each contact BPO to string
								$j.each(BPOs.contactBPOs, function (i) {
									itemContent +=
										"<tr>" +
										'<td class="center">' +
										'<input style=" width: auto; " type="radio" name="bpo" value="' +
										BPOs.contactBPOs[i].poId +
										'">' +
										"</td>" +
										"<td>" +
										BPOs.contactBPOs[i].poNumber +
										"</td>" +
										"<td>" +
										BPOs.contactBPOs[i].comment +
										"</td>" +
										'<td class="center">' +
										BPOs.contactBPOs[i].formattedDateFrom +
										"</td>" +
										'<td class="center">' +
										BPOs.contactBPOs[i].formattedDateTo +
										"</td>" +
										'<td class="center">' +
										BPOs.currencyERSymbol +
										"&nbsp;" +
										BPOs.contactBPOs[i].limitAmount +
										"</td>" +
										"</tr>";
								});
																		}
																		
																		// subdivision BPO's returned
							if (BPOs.subdivBPOs.length > 0) {
								// append subdivision heading row to string
								itemContent +=
									"<tr>" +
									'<td colspan="7">' +
									i18n.t("core.utilities:searchplugins.bpoAssignedTo", {
										varName: BPOs.subdivName,
										defaultValue: "BPO assigned to: " + BPOs.subdivName
									}) +
									"</td>" +
									"</tr>";

								// append row for each subdivision BPO to string
								$j.each(BPOs.subdivBPOs, function (i) {
									itemContent +=
										"<tr>" +
										'<td class="center">' +
										'<input style=" width: auto; " type="radio" name="bpo" value="' +
										BPOs.subdivBPOs[i].poId +
										'">' +
										"</td>" +
										"<td>" +
										BPOs.subdivBPOs[i].poNumber +
										"</td>" +
										"<td>" +
										BPOs.subdivBPOs[i].comment +
										"</td>" +
										'<td class="center">' +
										BPOs.subdivBPOs[i].formattedDateFrom +
										"</td>" +
										'<td class="center">' +
										BPOs.subdivBPOs[i].formattedDateTo +
										"</td>" +
										'<td class="center">' +
										BPOs.currencyERSymbol +
										"&nbsp;" +
										BPOs.subdivBPOs[i].limitAmount +
										"</td>" +
										"</tr>";
																			});
																		}
																		
																		// company BPO's returned
							if (BPOs.companyBPOs.length > 0) {
								// append company heading row to string
								itemContent +=
									"<tr>" +
									'<td colspan="7">' +
									i18n.t("core.utilities:searchplugins.bpoAssignedTo", {
										varName: BPOs.companyName,
										defaultValue: "BPO assigned to: " + BPOs.companyName
									}) +
									"</td>" +
									"</tr>";

								// append row for each company BPO to string
								$j.each(BPOs.companyBPOs, function (i) {
									itemContent +=
										"<tr>" +
										'<td class="center">' +
										'<input style=" width: auto; " type="radio" name="bpo" value="' +
										BPOs.companyBPOs[i].poId +
										'">' +
										"</td>" +
										"<td>" +
										BPOs.companyBPOs[i].poNumber +
										"</td>" +
										"<td>" +
										BPOs.companyBPOs[i].comment +
										"</td>" +
										'<td class="center">' +
										BPOs.companyBPOs[i].formattedDateFrom +
										"</td>" +
										'<td class="center">' +
										BPOs.companyBPOs[i].formattedDateTo +
										"</td>" +
										'<td class="center">' +
										BPOs.currencyERSymbol +
										"&nbsp;" +
										BPOs.companyBPOs[i].limitAmount +
										"</td>" +
										"</tr>";
								});
							}
							// show the table head section
							$j("table#itemBPOResults thead").removeClass().addClass("vis");
							// append new rows to table
							$j("table#itemBPOResults tbody").append(itemContent);

							if ($j("#availableprebookings").find(":selected").data("bpoid") != undefined)
								selecteBPOFromPrebooking($j("#availableprebookings").find(":selected").data("bpoid"));
						} else {
							// empty the table body
							$j("table#itemBPOResults tbody").empty();
							// hide the table head section
							$j("table#itemBPOResults thead").removeClass().addClass("hid");
							// append message of no called off items
							$j("table#itemBPOResults tbody").append(
								"<tr>" + '<td class="bold center">' + i18n.t("core.utilities:searchplugins.noBPOs", "No Blanket Purchase Order's") + "</td>" + "</tr>"
							);
						}
					},
				});

				// Instructions initialization
				// execute callback function if exists
				if (CascadeSearchPlugin.addressCallBackFunction != "") window[CascadeSearchPlugin.addressCallBackFunction]();
			},
		});
												},

	/**
	 * this method loads any default discount found for the supplied contact, subdiv, company
	 *
	 * @param {Integer} personid the id of the contact to look for discount
	 */
	sourceDiscount: function (personid) {
		var request = $j.ajax({
			type: "GET",
			contentType: "application/json",
			url: "purchaseorderapi.json/sourcediscount/" + personid,
		});

		request.done(function (discRate) {
			// check that hidden input is not already present?
			if ($j("div#" + CascadeSearchPlugin.pageDiv + " input#cascadeDiscountValue").length) {
				// remove old cascade discount input
				$j("div#" + CascadeSearchPlugin.pageDiv + " input#cascadeDiscountValue").remove();
			}
														// add new cascade discount input value
			$j("div#" + CascadeSearchPlugin.pageDiv).append('<input type="hidden" id="cascadeDiscountValue" value="' + discRate + '" />');
														// add discount to all existing discount inputs
			$j('input[id^="po_item_disc"]').each(function () {
				// add discount value to each input found
				this.value = discRate;
				/*alert("SourceDiscount!!");*/
			});
													});
				
												    request.fail(function( jqXHR, textStatus ) {
												        alert( " Discount request failed: " + textStatus );
												    });
												},
	/**
	 * this method updates the nominal code depending on whether the person/supplier
	 * is an internal Trescal company
	 *
	 * @param {Integer} personid the id of the contact to look to see if it's internal
	 */
	sourceNominal: function (personid) {
		var request = $j.ajax({
			type: "GET",
			contentType: "application/json",
			url: "purchaseorderapi.json/sourcenominal/" + personid,
		});

		request.done(function (nominalID) {
			$j('select[id^="po_item_nominal"]').each(function () {
				// select the nominal in each PO item dropdown
				this.value = nominalID;
			});
													});
				
												    request.fail(function( jqXHR, textStatus ) {
												        alert( " Nominal request failed: " + textStatus );
												    });
												},
	/**
	 * this function closes the search plugin and clears all inputs
	 */
	closePlugin: function () {
		$j("#" + ContactSearchPlugin.firstInput).attr("value", "");
		$j("#" + ContactSearchPlugin.lastInput).attr("value", "");
		$j("#" + ContactSearchPlugin.idInput).attr("value", "");
		$j("#" + ContactSearchPlugin.resultsDiv + ", div#cont-container ." + ContactSearchPlugin.headDiv).css({
			zIndex: -1,
			display: "none"
		});

		/** clear value in variable @see SearchPluginsKeyNav.js */
		searchFOCUS = "";
	},
};
