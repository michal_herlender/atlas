/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	ThreadSearchJQPlugin.js
*	DESCRIPTION		:	 
*
*	DEPENDENCIES	:   script/thirdparty/jQuery/jquery.js
*						
*						IMPORTANT - THIS PLUGIN REQUIRES A JAVASCRIPT ARRAY OF THREAD TYPE ID'S AND TYPES
*						TO BE SET IN THE HEAD SECTION OF THE PAGE IN WHICH IT IS TO BE IMPLEMENTED.						
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	05/09/2008 - SH - Created File
*					:	29/10/2015 - TProvost - Fix bug on test of the version of browser
*					:	30/10/2015 - TProvost - Manage i18n 
****************************************************************************************************************************************/

(function( $j )
{
	// variable to indicate whether any navigation keys have been pressed to stop ajax
	var keyPress = false;
	// variable to indicate which anchor in list is currently selected - used for key navigation
	var selected = 0;
	// variable to hold time value of last DWR call, checked against the current DWR call
	var lastUpdate = 0;
    
    $j.fn.threadSearchJQPlugin = function( options )
    {
        var options = $j.extend({
			
			/**
			 * name of the hidden input field
			 */
			field:			'',
			/**
			 * name of the select field
			 */
			selectfield: 	'',
			/**
			 * function to be performed when selection is changed?
			 */
			customFunc: 	'',
			/**
			 * copy option allows the user to add a image link which will
			 * copy the chosen thread to all other occurences on page. This option should
			 * only be provided for the first occurence of plugin.
			 */
			copyOption: 	'false',
			/**
			 * default thread if supplied
			 */
			defaultThreadId:		'',
			/** 
			 * default thread text
			 */
			defaultThreadText: 	'',
			/**
			 * thread type id for filtering results
			 */
			threadTypeId: 		'',
			/**
			 * thread options
			 */
			threadOptions: 		'',
			/**
			 * container div
			 */
			container: 		'threadContainerJQPlugin',
            /**
			 * class name of the results div
			 */
			resultsDiv: 	'threadResultsJQPlugin',
			/**
			 * class name of the footer div
			 */
			footDiv: 		'threadFootJQPlugin'
			
        }, options);

        return this.each(function()
        {
            threadSearchJQPlugin(this, options);
        });
    };

    function threadSearchJQPlugin( element, options )
    {
        return this instanceof threadSearchJQPlugin
            ? this.init(element, options)
            : new threadSearchJQPlugin(element, options);
    }

    $j.extend(threadSearchJQPlugin.prototype,
    {
        original : null,
        options  : {},

        element  : null,
        searchDiv   : null,

		/**
		 * plugin initialisation function
		 * 
		 * @param {Object} element is the current plugin div to manipulate
		 * @param {Object} options are options for the current plugin
		 */
        init : function( element, options )
        {			
			// initialise search div
            this.searchDiv = element;
			// initialise options
            this.options = options || {};
			
            // script to load for dwr service
			var Scripts = new Array (	'dwr/interface/threadservice.js' );
			// load the common javascript files for this plugin
			loadScript.loadScripts(Scripts);
			
			// set this div object
			var self = this;
			
			// check for thread type array
			if (threadArray)
			{
				// variable for all thread type options
				var threadOptions = '<option value="">All</option>';
				$j.each(threadArray, function(i)
				{
					if (threadArray[i].type != '')
					{
						threadOptions += '<option value="' + threadArray[i].id + '">' + threadArray[i].type + '</option>';
					}					
				});
				this.options.threadOptions = threadOptions;
			}
		
			// call method to draw plugin search
            this.drawSearch();
			
			// add keyup event to the text input and bind method to call thread dwr service
			$j( $j(this.searchDiv).find('input[type="text"]:first') ).keyup(function() { self.runThreadAjax(this.value, self); return false; });
			// add click event to the image following the text input field which retrieves all results
			$j( $j(this.searchDiv).find('img:first') ).click(function() { self.runThreadAjax('%', self); return false; });
			// add click event to the close image in footer div and bind method to close the plugin
			$j( $j(this.searchDiv).find('div.threadContainerJQPlugin img:last') ).click(function() { self.closePlugin(); return false; });
			// add change event to thread type select
			$j( $j(this.searchDiv).find('select[name="' + this.options.selectfield + '"]') )
									.change(function() { self.updateThreadCode(this.value, self); return false; })
									.val(this.options.threadTypeId);
																							
			// copy selected thread to other fields required
			if (this.options.copyOption == 'true') 
			{
				// add click event to the anchor and bind method to call copy option method
				// for text input field and hidden input field
				$j($j(this.searchDiv).find('> a')).each(function(i)
				{
					// variable to hold direction of copy
					var direction = 'up';
					// first anchor
					if (i < 1)
					{
						direction = 'down';
					}
										
					// add click function to copy down anchor
					$j(this).click(function()
					{
						// call copy function
						self.copyOption($j(this).parent().find('input[type="text"]:first'), direction, false, $j(this).siblings('select'));
						// call copy function
						self.copyOption($j(this).parent().find('div:first input[type="hidden"]:last'), direction, true, null);
						return false;
					});		
				});
			}
			
			/**
			 * start jquery keypress observer to check if the user has pressed the up/down arrow or the
			 * enter/esc key when searching for thread's
			 * 
			 */ 	
			$j( $j(this.searchDiv) ).keydown(function(e) 
			{			
			    var event = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
				
				switch (event)
				{
					//  up arrow key has been pressed
					case 38: keyPress = true;
							 // check selected variable
							 if (selected == 0)
							 {
							 	
							 }
							 // selected variable contains value greater than zero
							 else
							 {
							 	 // remove 'selected' class name from the currently selected element and add to the previous one, apply focus
								 $j(this).find('> div div:first a').eq(selected).removeClass().prev().addClass('selected').focus();
								 // remove one from the selected variable value
								 selected = selected - 1;
								 // then return focus to the input field
								 $j(this).find('input[type="text"]:first').focus();
							 }
							 break;
							 
					// down arrow key has been pressed
					case 40: keyPress = true;
							 // check selected variable is not greater than list length
							 if (selected == $j(this).find('> div div:first a').size() - 1)
							 {
							 
							 }
							 // selected variable contains value less than list length
							 else
							 {
							 	// remove 'selected' class name from the currently selected element and add to the next one, apply focus
								$j(this).find('> div div:first a').eq(selected).removeClass().next().addClass('selected').focus();
								// remove one from the selected variable value
								selected = selected + 1;
								// then return focus to the input field
								$j(this).find('input[type="text"]:first').focus();
							 }
							 break;
							 
					// escape key has been pressed
					case 27: keyPress = true;
							 // call function to close plugin
							 self.closePlugin();
							 break;
							 
					// enter key has been pressed
					case 13: keyPress = true;
							 // trigger click event of anchor
						 	 $j(this).find('> div div:first a').eq(selected).trigger('click');
							 break;
					
					// left arrow key has been pressed, just stop ajax from being initiated if pressed accidentally
					case 37: keyPress = true;
							 break;
							 
					// right arrow key has been pressed, just stop ajax from being initiated if pressed accidentally
					case 39: keyPress = true;
							 break;
							 
					// tab key has been pressed, just stop ajax from being initiated if pressed accidentally
					case 9: keyPress = true;
							break;
				}
				
			});
        },
		
		/**
		 * method to draw plugin search
		 */
        drawSearch : function()
        {
        	var content = 	'<select name="' + this.options.selectfield + '" style=" width: 160px; ">' +
								this.options.threadOptions +
							'</select><br />' +							
							'<input type="text" name="threadtext" value="' + this.options.defaultThreadText + '" onkeypress=" if (event.keyCode==13){ return false; } " autocomplete="off" />' +
							'<img src="img/icons/searchplugin_all.png" width="16" height="16" alt="' + i18n.t("core.utilities:searchplugins.viewAll", "View All") + '" title="' + i18n.t("core.utilities:searchplugins.viewAll", "View All") + '" />';
							// copy options required
							if 	(this.options.copyOption == 'true')
							{
								content = content +	'<a href="#">' +
														'<img src="img/icons/arrow_down.png" width="10" height="16" alt="' + i18n.t("copySelectionToAll", "Copy selection to all") + '" title="' + i18n.t("copySelectionToAll", "Copy selection to all") + '" />' +
													'</a>' + 
													'<a href="#">' +
														'<img src="img/icons/arrow_up.png" width="10" height="16" alt="' + i18n.t("copySelectionUp", "Copy selection up") + '" title="' + i18n.t("copySelectionUp", "Copy selection up") + '" />' +
													'</a>';
							}
				content +=	'<div class="' + this.options.container + '">' +
								// create and append the div in which thread results will be displayed
								'<div class="' + this.options.resultsDiv + '">' +
									// results are displayed here
								'</div>' +
								// create and append footer with close button
								'<div class="' + this.options.footDiv + '">' +
									'<a href="thread.htm" target="_blank" class="mainlink float-left" style=" line-height: 18px; margin-left: 8px; ">' + i18n.t("core.utilities:searchplugins.addThread", "Add Thread") + '</a>' +
									'<span style=" line-height: 18px; margin-right: 18px; ">(' + i18n.t("core.utilities:searchplugins.escKey", "Esc Key") + ') ' + i18n.t("core.utilities:searchplugins.or", "or") + ' </span><img src="img/icons/bullet-cross.png" width="10" height="10" alt="' + i18n.t("core.utilities:searchplugins.closeThreadSearch", "Close Thread Search") + '" title="' + i18n.t("core.utilities:searchplugins.closeThreadSearch", "Close Thread Search") + '" class="pluginClose" /></div>' +
								'<input type="hidden" name="' + this.options.field + '" value="' + this.options.defaultThreadId + '" />' +
							'</div>';
						
			// append plugin content to main div
			$j(this.searchDiv).append(content);
			// mozilla or opera browser						
			if ($j.browser != null && ($j.browser.mozilla != null || $j.browser.opera != null))
			{
				$j(this.searchDiv).parent('li').css({ paddingBottom: '4px'});
			}
			// other browsers
			else
			{
				$j(this.searchDiv).parent('li').css({ paddingBottom: '3px'});
			}
			
        },
		
		/**
		 * method which accepts the name fragment provided from the input field and
		 * calls the dwr service to retrieve threads
		 * 
		 * @param {String} nameFragment text entered into input field which should be used to search threads
		 * @param {Object} self reference to this object
		 */
		runThreadAjax : function(nameFragment, self)
		{			
			// navigation keys have been pressed so set value back to false and skip ajax loading
			if (keyPress == true)
			{
				keyPress = false;
			}
			else
			{
				// another search is in use
				if ( ($j( $j(this.searchDiv).find('> div div:first a') ).length < 1) && ($j('div.' + this.options.resultsDiv + ' > a').length > 0) )
				{
					// reset input field
					$j( $j(this.searchDiv).find('input[type="text"]:first') ).attr('value', '');
					// alert user that another email plugin is in use
					$j.prompt(i18n.t("core.utilities:searchplugins.anotherPluginIsInUse", "Another plugin is in use, please close before continuing!"));
					// exit method
					return false;
				}																
				// remove all previously displayed threads
				$j( $j(this.searchDiv).find('> div div:first') ).empty();
				// display the thread results container
				$j( $j(this.searchDiv).find('> div div') ).css({zIndex: 100, display: 'block'});																	
				// this span displays the loading image and text when a search has been activated
				$j( $j(this.searchDiv).find('> div div:first') ).append('<div class="loading-plug">' +
																			'<img src="img/icons/loading-anim.gif" class="img_marg_bot" height="18" width="80" alt="" />' +
																		'</div>');				
				// get current timestamp to pass with ajax results
				var currentTime = new Date().getTime();
				// only execute the ajax functionality if the text entered has a length
				// greater than zero (stops unconstrained retrieval)
				if ((nameFragment.length > 1) || (nameFragment == '%'))
				{
					if ($j( $j(this.searchDiv).find('> input[name="threadTypeId"]') ).val() != '')
					{
						this.options.threadTypeId = $j( $j(this.searchDiv).find('> input[name="threadTypeId"]') ).val()
					}
					else
					{
						this.options.threadTypeId = null;
					}
					// call dwr service to retrieve threads
					threadservice.searchThreadsHQL(nameFragment, this.options.threadTypeId,
					{
						callback:function(threads)
						{
							// call method to display threads returned
							self.threadCallback(threads, currentTime, self);
						}
					});
				}
				// text entered has a length of 0
				else
				{														
				 	// hide the thread results container
					$j( $j(this.searchDiv).find('> div div') ).css({zIndex: -1, display: 'none'});
					// remove all previously displayed threads
					$j( $j(this.searchDiv).find('> div div:first') ).empty();
					// clear procid hidden input field
					$j( $j(this.searchDiv).find('> div input[type="hidden"]') ).val('');				
				}
			}
			
		},
		
		/**
		 * this method appends all threads returned from the dwr service to the results div
		 * 
		 * @param {Array} threads is an array of threads returned from the dwr service
		 * @param {Date/Time} currentUpdate is a timestamp retrieved when the search was initiated
		 * @param {Object} self reference to this object
		 */
		threadCallback : function(threads, currentUpdate, self) 
		{
			// this is a new search
			if (currentUpdate > lastUpdate)
			{
				// update value in the 'lastUpdate' variable
				lastUpdate = currentUpdate;
				
				// threads length is greater than zero
				if(threads.length > 0)
				{
					// remove all previously displayed threads
					$j( $j(this.searchDiv).find('> div div:first') ).empty();
					// declare variable to hold results content
					var content = '';
					// add anchor for each thread returned to results variable
					$j(threads).each(function(i)
					{	
						content = content + '<a href="#" id="' + threads[i].id + '<->' + threads[i].size + '"><span>' + threads[i].size + '</span></a>';																																			
					});
					// append results variable to results div
					$j( $j(this.searchDiv).find('> div div:first') ).append(content);
					
					// loop through all anchors appended to results div
					$j( $j(this.searchDiv).find('> div div:first a') ).each(function(i)
					{
						// apply click event to anchor
						$j( $j(this) ).click( function(){ self.selectThreadId(this.id, self); return false; } );
					});			
					// add 'selected' class name to first anchor in results div
					$j( $j(this.searchDiv).find('> div div:first a:first') ).addClass('selected');
					// initialise the selected variable to zero
					selected = 0;																						
				}
				else
				{
					// remove all previously displayed threads
					$j( $j(this.searchDiv).find('> div div:first') ).empty().append('<div class="center bold">' + i18n.t("core.utilities:searchplugins.noThreadsFound", "No Threads Found") + '</div>');
				}
			}
		},
		
		/**
		 * this method selects the chosen thread and adds the name to the text input field and adds the
		 * procid to the hidden input field after which the results div is emptied and hidden
		 * 
		 * @param {String} threadIdAndSize id and size of the thread chosen
		 * @param {Object} self reference to this object
		 */
		selectThreadId : function (threadIdAndSize, self)
		{
			// add id and size to array
			var threadArray = threadIdAndSize.split('<->');
			// add thread id to the hidden input field
			$j( $j(this.searchDiv).find('> div input[type="hidden"]') ).val(threadArray[0]);
			// add thread text to the search text input field
			$j( $j(this.searchDiv).find('> input[type="text"]:first') ).val(threadArray[1]);
			// empty the results div
			$j( $j(this.searchDiv).find('> div div:first') ).empty();
			// hide the search results box and footer
			$j( $j(this.searchDiv).find('> div div') ).css({zIndex: -1, display: 'none'});			
			// custom method to call?
			if(this.options.customFunc != '')
			{
				// trigger the onclick event of customFunc input
				$j( $j(this.searchDiv).find('> input[name="' + this.options.customFunc + '"]') ).trigger('onclick');
			} 
		},
	
		/**
		 * this function closes the search plugin and clears all inputs
		 */				
		closePlugin : function()
		{
			// clear the text input field and apply focus 
			$j( $j(this.searchDiv).find('input[type="text"]:first') ).attr('value', '').focus();
			// clear thread id in the hidden input field
			$j( $j(this.searchDiv).find('> div input[type="hidden"]') ).val('');
			// empty the results div
			$j( $j(this.searchDiv).find('> div div:first') ).empty();
			// hide the search results box and footer
			$j( $j(this.searchDiv).find('> div div') ).css({zIndex: -1, display: 'none'});
		},
		
		/**
		 * this method updates the thread code used by the search plugin
		 */
		updateThreadCode : function(threadId, self)
		{
			// add new thread type id to input field
			$j( $j(this.searchDiv).find('input[name="threadTypeId"]') ).val(threadId);
			// clear the text input field and apply focus 
			$j( $j(this.searchDiv).find('input[type="text"]:first') ).attr('value', '').focus();
			// clear thread id in the hidden input field
			$j( $j(this.searchDiv).find('> div input[type="hidden"]') ).val('');
			// another search is in use
			if ( ($j(this.searchDiv).find('div.' + this.options.resultsDiv + ' > a').length > 0) || ($j( $j(this.searchDiv).find('> div div') ).css('display') == 'block') )
			{				
				// close plugin
				self.closePlugin();
				// exit method
				return false;
			}
		},
		
		/**
		 * this method copies the values selected in this plugin instance to any others
		 * present on the page.
		 *
		 * @param {Object} element element which we can obtain value and name for copying
		 * @param {String} direction the direction in which the value should be copied
		 * @param {Boolean} triggerFunction indicates if the custom function should be triggered
		 */
		copyOption : function (element, direction, triggerFunction, select)
		{
			// add marker after element
			$j(element).after('<span class="marker"></span>');
			// get the input name attribute
			var elementName = $j(element).attr('name');
			// assign option to variable
			var customFunc = this.options.customFunc;
			// variable to indicate the marker has been found
			var markerFound = false;
			// array to hold all inputs
			var inputArray = new Array();
			// copy information down the list
			if (direction == 'down')
			{				
				// does element name contain full stop?
				if (elementName.indexOf('.') != -1)
				{
					// get the end of the element name after full stop
					var elementEnd = elementName.substring((elementName.lastIndexOf('.') + 1), elementName.length);
					// create an array of input objects
					inputArray = $j.makeArray($j('input[name$="' + elementEnd + '"]'));
				}
				else
				{
					// create an array of input objects
					inputArray = $j.makeArray($j('input[name="' + $j(element).attr('name') + '"]'));
				}	
			}
			else
			{
				// does element name contain full stop?
				if (elementName.indexOf('.') != -1) 
				{
					// get the end of the element name after full stop
					var elementEnd = elementName.substring((elementName.lastIndexOf('.') + 1), elementName.length);
					// create an array of input objects
					inputArray = $j.makeArray($j('input[name$="' + elementEnd + '"]'));
					// reverse the array
	    			inputArray.reverse();
				}
				else 
				{
					// create an array of input objects
					inputArray = $j.makeArray($j('input[name="' + $j(element).attr('name') + '"]'));
					// reverse the array
	    			inputArray.reverse();
				}
			}
			
			// loop through all matching inputs
			$j.each(inputArray, function()
			{
				// marker found?
				if ($j(this).next().hasClass('marker'))
				{
					// set variable and remove marker
					markerFound = true;
					$j(this).next().remove();
				}
				// marker found?
				if (markerFound)
				{
					// copy value to input
					$j(this).val($j(element).val());
					// select supplied?
					if (select)
					{
						$j(this).prev().prev().val($j(select).val());
					}
					// custom function option has value?
					if ((customFunc != '') && (triggerFunction == true)) 
					{
						// trigger the onclick event of customFunc input
						$j(this).parent().parent().find('input[name="' + customFunc + '"]').trigger('onclick');
					}
				}					
			});		
		}
        
    });
})(jQuery);