/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	MfrSearchJQPlugin.js
*	DESCRIPTION		:	 
*
*	DEPENDENCIES	:   script/thirdparty/jQuery/jquery.js
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	21/02/2008 - SH - Created File
*					:	29/10/2015 - TProvost - Fix bug on test of the version of browser
*					:	30/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

(function( $j )
{
	// variable to indicate whether any navigation keys have been pressed to stop ajax
	var keyPress = false;
	// variable to indicate which anchor in list is currently selected - used for key navigation
	var selected = 0;
	// variable to hold time value of last DWR call, checked against the current DWR call
	var lastUpdate = 0;
    
    $j.fn.mfrSearchJQPlugin = function( options )
    {
        var options = $j.extend({
			
			/**
			 * name of the hidden input field for mfr id
			 */
			fieldId:		'',
			/**
			 * name of the input field for mfr name
			 */
			fieldName: 		'',
			/**
			 * indicates if this search plugin should allow mfr text searches as well as
			 * matching mfr searches. This is used on search forms to allow part name searches.
			 */
			textOnlyOption: 'false',
			/**
			 * the current search type we are using (i.e. matched mfrs (mfrid) or mfr text (mfrtext))
			 */
			searchType:		'mfrid',
			/**
			 * copy option allows the user to add a image link which will
			 * copy the chosen mfr to all other occurences on page. This option should
			 * only be provided for the first occurence of plugin.
			 */
			copyOption: 	'false',
			/**
			 * copy parent is an element in which all copies should be performed
			 * this should be defined with the jquery prefix for id or class name (i.e. #test, .test)
			 */
			copyParent:		'',
			/**
			 * tab index for text input field
			 */
			tabIndex:		0,
			/**
			 * prefill the name of the manufacturer
			 */
			mfrNamePrefill:	'',
			/**
			 * prefill the id of the manufacturer
			 */
			mfrIdPrefill:	'',
			/**
			 * container div
			 */
			container: 		'mfrContainerJQPlugin',
            /**
			 * class name of the results div
			 */
			resultsDiv: 	'mfrResultsJQPlugin',
			/**
			 * class name of the footer div
			 */
			footDiv: 		'mfrFootJQPlugin',
			/**
			 * z-index value of results
			 */
			zIndex: 		100
			
        }, options);

        return this.each(function()
        {
            mfrSearchJQPlugin(this, options);
        });
    };

    function mfrSearchJQPlugin( element, options )
    {
        return this instanceof mfrSearchJQPlugin
            ? this.init(element, options)
            : new mfrSearchJQPlugin(element, options);
    }

    $j.extend(mfrSearchJQPlugin.prototype,
    {
        original : null,
        options  : {},

        element  : null,
        searchDiv   : null,

		/**
		 * plugin initialisation function
		 * 
		 * @param {Object} element is the current plugin div to manipulate
		 * @param {Object} options are options for the current plugin
		 */
        init : function( element, options )
        {			
			// initialise search div
            this.searchDiv = element;
			// initialise options
            this.options = options || {};
			
			// call method to draw plugin search
            this.drawSearch(this.options.textOnlyOption);
			
			// set this div object
			var self = this;
			
			// add keyup event to the text input and bind method to call manufacturer dwr service
			$j( $j(this.searchDiv).find('input[type="text"]:first') ).keyup(function() { self.runMfrAjax(this.value, self); return false; })
																		.keypress(function(event) { if (event.keyCode==13){ return false; } });
			// add click event to the close image in footer div and bind method to close the plugin
			$j( $j(this.searchDiv).find('div.mfrContainerJQPlugin img:last') ).click(function() { self.closePlugin(); return false; });
			// copy selected mfr to other fields required
			if (this.options.copyOption == 'true') 
			{
				// add option to variable as it is not available in loop
				var copyParent = this.options.copyParent;
				// add click event to the anchor and bind method to call copy option method
				// for text input field and hidden input field
				$j($j(this.searchDiv).find('> a')).each(function(i)
				{
					// variable to hold direction of copy
					var direction = 'up';
					// first anchor
					if (i < 1)
					{
						direction = 'down';
					}
										
					// add click function to copy down anchor
					$j(this).click(function()
					{
						// call copy function for text value
						self.copyOption($j(this).parent().find('input[type="text"]:first'), direction, copyParent);
						// call copy function for id of manufacturer
						self.copyOption($j(this).parent().find('div:first input[type="hidden"]:last'), direction, copyParent);
						return false;
					});					
				});
			}
			// allow text searches as well as matching mfr searches
			if (this.options.textOnlyOption == 'true') 
			{
				// add click event to the link anchor's and bind methods
				$j( $j(this.searchDiv).find('> a.textOnlyOption') ).each(function(i)
				{				
					// first anchor?
					if (i < 1)
					{
						// add click event
						$j(this).click(function()
						{
							// method changes the current use of input to search matching mfrs
							self.searchByMatchingMfr(this, self);							
							return false;
						});	
					}
					else
					{
						// add click event
						$j(this).click(function()
						{
							// method changes the current use of input to search using text
							self.searchByMfrText(this);							
							return false;
						});		
					}				
				});
			}
			// check prefill options
			if (this.options.mfrNamePrefill != '' || (this.options.mfrIdPrefill != '' && this.options.mfrIdPrefill != 0))
			{
				if (this.options.mfrNamePrefill == '' && this.options.mfrIdPrefill != '')
				{
					// load the required mfr by using the prefill id
					self.runMfrAjaxUsingId(this.options.mfrIdPrefill, self);
				}
				else if (this.options.mfrNamePrefill != '' && this.options.mfrIdPrefill == '')
				{
					// trigger click event to change to text search
					$j( $j(this.searchDiv).find('> a.textOnlyOption:last') ).trigger('click');
					// call method to populate plugin for prefill
					self.populatePlugin(this.options.mfrNamePrefill, '', self);
				}
				else
				{
					// call method to populate plugin for prefill
					self.populatePlugin(this.options.mfrNamePrefill, this.options.mfrIdPrefill, self);
				}
			}
			
			/**
			 * start jquery keypress observer to check if the user has pressed the up/down arrow or the
			 * enter/esc key when searching for mfr's
			 * 
			 */ 	
			$j( $j(this.searchDiv) ).keydown(function(e) 
			{			
			    var event = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
				
				switch (event)
				{
					//  up arrow key has been pressed
					case 38: keyPress = true;
							 // check selected variable
							 if (selected == 0)
							 {
							 	
							 }
							 // selected variable contains value greater than zero
							 else
							 {
							 	 // remove 'selected' class name from the currently selected element and add to the previous one, apply focus
								 $j(this).find('> div div:first a').eq(selected).removeClass().prev().addClass('selected').focus();
								 // remove one from the selected variable value
								 selected = selected - 1;
								 // then return focus to the input field
								 $j(this).find('input[type="text"]:first').focus();
							 }
							 break;
							 
					// down arrow key has been pressed
					case 40: keyPress = true;
							 // check selected variable is not greater than list length
							 if (selected == $j(this).find('> div div:first a').size() - 1)
							 {
							 
							 }
							 // selected variable contains value less than list length
							 else
							 {
							 	// remove 'selected' class name from the currently selected element and add to the next one, apply focus
								$j(this).find('> div div:first a').eq(selected).removeClass().next().addClass('selected').focus();
								// remove one from the selected variable value
								selected = selected + 1;
								// then return focus to the input field
								$j(this).find('input[type="text"]:first').focus();
							 }
							 break;
							 
					// escape key has been pressed
					case 27: keyPress = true;
							 // call function to close plugin
							 self.closePlugin();
							 break;
							 
					// enter key has been pressed
					case 13: keyPress = true;
							 // hide the search results box and footer
							 if ($j(this).find('> div div').css('display') == 'none')
							 {
							 	// get the parent form
							 	var form = $j(this).closest('form');
								// submit form
							 	$j(form).find('input[type="submit"]').trigger('click');
							 }							 
							 // trigger click event of anchor
						 	 $j(this).find('> div div:first a').eq(selected).trigger('click');							 
							 break;
					
					// left arrow key has been pressed, just stop ajax from being initiated if pressed accidentally
					case 37: keyPress = true;
							 break;
							 
					// right arrow key has been pressed, just stop ajax from being initiated if pressed accidentally
					case 39: keyPress = true;
							 break;
							 
					// tab key has been pressed, just stop ajax from being initiated if pressed accidentally
					case 9: keyPress = true;
							break;
				}
				
			});
        },
		
		/**
		 * method to draw plugin search
		 * 
		 * @param textOnlyOption boolean value indicating if the plugin should also allow text only (i.e. no search for matching mfr's)
		 */
        drawSearch : function(textOnlyOption)
        {
			var content = 	'<input type="text" name="mfrtext" autocomplete="off" ';
							if (this.options.tabIndex != 0)
							{
								content += 'tabindex="' + this.options.tabIndex + '" ';
							}
							content += 'value="' + this.options.mfrNamePrefill + '" />';
							if (textOnlyOption == 'true')
							{
								content += 	'<a href="#" class="textOnlyOption">' +
												'<img src="img/icons/searchplugin_sel.png" width="16" height="16" alt="' + i18n.t("core.utilities:searchplugins.searchMatchingMfr", "Search Matching Mfrs") + '" title="' + i18n.t("core.utilities:searchplugins.searchMatchingMfr", "Search Matching Mfrs") + '" />' +
											'</a>' +
											'<a href="#" class="textOnlyOption">' + 
												'<img src="img/icons/textsearch.png" width="16" height="16" class="searchimage" alt="' + i18n.t("core.utilities:searchplugins.searchTextOnly", "Search Text Only") + '" title="' + i18n.t("core.utilities:searchplugins.searchTextOnly", "Search Text Only") + '" />' +
											'</a>';	
							}
							else
							{
								content += '<img src="img/icons/searchplugin.png" width="16" height="16" alt="' + i18n.t("core.utilities:searchplugins.searchMatchingMfr", "Search Matching Mfrs") + '" title="' + i18n.t("core.utilities:searchplugins.searchMatchingMfr", "Search Matching Mfrs") + '" />';
							}
							// copy options required
							if 	(this.options.copyOption == 'true')
							{
								content +=	'<a href="#">' +
												'<img src="img/icons/arrow_down.png" width="10" height="16" alt="' + i18n.t("copySelectionDown", "Copy selection down") + '" title="' + i18n.t("copySelectionDown", "Copy selection down") + '" />' +
											'</a>' + 
											'<a href="#">' +
												'<img src="img/icons/arrow_up.png" width="10" height="16" alt="' + i18n.t("copySelectionUp", "Copy selection up") + '" title="' + i18n.t("copySelectionUp", "Copy selection up") + '" />' +
											'</a>';
							}
		content = content +	'<div class="' + this.options.container + '">' +
								// create and append the div in which manufacturer results will be displayed
								'<div class="' + this.options.resultsDiv + '">' +
									// results are displayed here
								'</div>' +
								// create and append footer with close button
								'<div class="' + this.options.footDiv + '"><span style=" line-height: 18px; margin-right: 18px; ">(' + i18n.t("core.utilities:searchplugins.escKey", "Esc Key") + ') ' + i18n.t("core.utilities:searchplugins.or", "or") + ' </span><img src="img/icons/bullet-cross.png" width="10" height="10" alt="' + i18n.t("core.utilities:searchplugins.closeManufacturerSearch", "Close Manufacturer Search") + '" title="' + i18n.t("core.utilities:searchplugins.closeManufacturerSearch", "Close Manufacturer Search") + '" class="pluginClose" /></div>' +
								'<input type="hidden" name="' + this.options.fieldId + '" value="' + this.options.mfrIdPrefill + '" />' +
							'</div>';
						
			// append plugin content to main div
			$j(this.searchDiv).append(content);
			// mozilla or opera browser						
			if ($j.browser != null && ($j.browser.mozilla != null || $j.browser.opera != null))
			{
				$j(this.searchDiv).parent('li').css({ paddingBottom: '4px'});
			}
			// other browsers
			else
			{
				$j(this.searchDiv).parent('li').css({ paddingBottom: '3px'});
			}
			
        },
        
        /**
		 * method which loads the required mfr using the prefill mfrid if no mfr name has been supplied
		 * 
		 * @param {Integer} mfrId the id of the mfr to be loaded
		 * @param {Object} self reference to this object
		 */
        runMfrAjaxUsingId : function(mfrId, self)
        {
			// only execute the ajax functionality if the mfrid has been supplied
			if (mfrId != null && mfrId != '')
			{
				// load the service javascript file if not already
				loadScript.loadScriptsDynamic(new Array('dwr/interface/manuservice.js'), true,
				{
					callback: function()
					{
						// call dwr service to retrieve manufacturers
						manuservice.findMfrDWR(mfrId,
						{
							callback:function(result)
							{
								if (result.success)
								{		
									// get the mfr
									var mfr = result.results;
									// call method to populate plugin for prefill
									self.populatePlugin(mfr.name, mfr.mfrid, self);
								}
							}
						});
					}
				});
			}
        },
        
        /**
         * this method populates the mfr text field and the mfr id field with the supplied values
         * 
         * @param {String} mfrname the name of the mfr
         * @param {String} mfrid the id of the mfr
         * @param {Object} self reference to this object
         */
        populatePlugin : function(mfrname, mfrid, self)
        {
        	// populate mfr name input field
        	$j( $j(this.searchDiv).find('input[type="text"]:first') ).val(mfrname);
        	// populate mfrid hidden input field
			$j( $j(this.searchDiv).find('> div input[type="hidden"]') ).val(mfrid);
        },
		
		/**
		 * method which accepts the name fragment provided from the input field and
		 * calls the dwr service to retrieve manufacturers
		 * 
		 * @param {String} nameFragment text entered into input field which should be used to search manufacturers
		 * @param {Object} self reference to this object
		 */
		runMfrAjax : function(nameFragment, self)
		{					
			// navigation keys have been pressed so set value back to false and skip ajax loading
			if (keyPress == true)
			{
				keyPress = false;
			}
			else
			{
				// another search is in use
				if ( ($j( $j(this.searchDiv).find('> div div:first a') ).length < 1) && ($j('div[class*="ResultsJQPlugin"] > a').length > 0) )
				{
					// reset input field
					$j( $j(this.searchDiv).find('input[type="text"]:first') ).val('');
					// alert user that another email plugin is in use
					$j.prompt(i18n.t("core.utilities:searchplugins.anotherPluginIsInUse", "Another plugin is in use, please close before continuing!"));
					// exit method
					return false;
				}																
				// remove all previously displayed manufacturers
				$j( $j(this.searchDiv).find('> div div:first') ).empty();
				// display the manufacturer results container
				$j( $j(this.searchDiv).find('> div div') ).css({zIndex: this.options.zIndex, display: 'block'});																	
				// this span displays the loading image and text when a search has been activated
				$j( $j(this.searchDiv).find('> div div:first') ).append('<div class="loading-plug">' +
																			'<img src="img/icons/loading-anim.gif" class="img_marg_bot" height="18" width="80" alt="" />' +
																		'</div>');				
				// get current timestamp to pass with ajax results
				var currentTime = new Date().getTime();
				// only execute the ajax functionality if the text entered has a length
				// greater than zero (stops unconstrained retrieval)
				if (nameFragment.length > 0)
				{
					// load the service javascript file if not already
					loadScript.loadScriptsDynamic(new Array('dwr/interface/manuservice.js'), true,
					{
						callback: function()
						{
							// call dwr service to retrieve manufacturers
							manuservice.searchSortedActiveMfrHQL(nameFragment,
							{
								callback:function(mfrs)
								{
									// call method to display manufacturers returned
									self.mfrCallback(mfrs, currentTime, self);
								}
							});
						}
					});
				}
				// text entered has a length of 0
				else
				{														
				 	// hide the manufacturer results container
					$j( $j(this.searchDiv).find('> div div') ).css({zIndex: -1, display: 'none'});
					// remove all previously displayed manufacturers
					$j( $j(this.searchDiv).find('> div div:first') ).empty();
					// clear mfrid hidden input field
					$j( $j(this.searchDiv).find('> div input[type="hidden"]') ).val('');				
				}
			}
			
		},
		
		/**
		 * this method appends all manufacturers returned from the dwr service to the results div
		 * 
		 * @param {Array} mfrs is an array of manufacturers returned from the dwr service
		 * @param {Date/Time} currentUpdate is a timestamp retrieved when the search was initiated
		 * @param {Object} self reference to this object
		 */
		mfrCallback : function(mfrs, currentUpdate, self) 
		{
			// this is a new search
			if (currentUpdate > lastUpdate)
			{
				// update value in the 'lastUpdate' variable
				lastUpdate = currentUpdate;
				
				// manufacturers length is greater than zero
				if(mfrs.length > 0)
				{
					// remove all previously displayed manufacturers
					$j( $j(this.searchDiv).find('> div div:first') ).empty();
					// declare variable to hold results content
					var content = '';
					// add anchor for each manufacturer returned to results variable
					$j(mfrs).each(function(i)
					{	
						content = content + '<a href="#" id="' + mfrs[i].mfrid + '"><span>' + mfrs[i].name + '</span></a>';																																			
					});
					// append results variable to results div
					$j( $j(this.searchDiv).find('> div div:first') ).append(content);
					
					// loop through all anchors appended to results div
					$j( $j(this.searchDiv).find('> div div:first a') ).each(function(i)
					{
						// apply click event to anchor
						$j( $j(this) ).click( function(){ self.selectMfrId(this.id, $j(this).text(), self); return false; } );
					});			
					// add 'selected' class name to first anchor in results div
					$j( $j(this.searchDiv).find('> div div:first a:first') ).addClass('selected');
					// initialise the selected variable to zero
					selected = 0;																						
				}
				else
				{
					// remove all previously displayed manufacturers
					$j( $j(this.searchDiv).find('> div div:first') ).empty().append('<div class="center bold">' + i18n.t("core.utilities:searchplugins.noManufacturersFound", "No Manufacturers Found") + '</div>');
				}
			}
		},
		
		/**
		 * this method selects the chosen manufacturer and adds the name to the text input field and adds the
		 * mfrid to the hidden input field after which the results div is emptied and hidden
		 * 
		 * @param {Integer} mfrid id of the manufacturer chosen
		 * @param {String} mfr name of the manufacturer chosen
		 * @param {Object} self reference to this object
		 */
		selectMfrId : function (mfrid, mfr, self)
		{
			// add manufacturer id to the hidden input field
			$j( $j(this.searchDiv).find('> div input[type="hidden"]') ).val(mfrid);
			// add manufacturer text to the search text input field
			$j( $j(this.searchDiv).find('> input[type="text"]:first') ).val(mfr);
			// empty the results div
			$j( $j(this.searchDiv).find('> div div:first') ).empty();
			// hide the search results box and footer
			$j( $j(this.searchDiv).find('> div div') ).css({zIndex: -1, display: 'none'});
		},
		
		/**
		 * this method changes the search type to text search instead of matching manufacturer search
		 * 
		 * @param {Object} anchor the anchor clicked to change search type
		 */
		searchByMfrText : function (anchor)
		{
			if (this.options.searchType == 'mfrid')
			{
				// close the plugin if it is present and clear fields
				this.closePlugin();
				// change the search mfrs image to unticked
				$j(anchor).prev()
							.find('img')
							.before('<img src="img/icons/searchplugin.png" width="16" height="16" alt="' + i18n.t("core.utilities:searchplugins.searchMatchingMfr", "Search Matching Mfrs") + '" title="' + i18n.t("core.utilities:searchplugins.searchMatchingMfr", "Search Matching Mfrs") + '" />')
							.remove();
				// change the search text image to ticked
				$j(anchor).find('img')
							.after('<img src="img/icons/textsearch_sel.png" width="16" height="16" class="searchimage" alt="' + i18n.t("core.utilities:searchplugins.searchTextOnly", "Search Text Only") + '" title="' + i18n.t("core.utilities:searchplugins.searchTextOnly", "Search Text Only") + '" />')
							.remove();
				// unbind the keyup event to the text input and change it's name to the supplied option value
				$j( $j(this.searchDiv).find('input[type="text"]:first') ).attr('name', this.options.fieldName).unbind('keyup').unbind('keypress');
				// change the search type option
				this.options.searchType = 'mfrtext';		
			}		
		},
		
		/**
		 * this method changes the search type to matching manufacturer id instead of text search
		 * 
		 * @param {Object} anchor the anchor clicked to change search type
		 * @param {Object} self reference to this object
		 */
		searchByMatchingMfr : function (anchor, self)
		{
			if (this.options.searchType == 'mfrtext')
			{
				// change the search text image to unticked
				$j(anchor).next()
							.find('img')
							.after('<img src="img/icons/textsearch.png" width="16" height="16" class="searchimage" alt="' + i18n.t("core.utilities:searchplugins.searchTextOnly", "Search Text Only") + '" title="' + i18n.t("core.utilities:searchplugins.searchTextOnly", "Search Text Only") + '" />')
							.remove();
				// change the search mfrs image to ticked
				$j(anchor).find('img')
							.after('<img src="img/icons/searchplugin_sel.png" width="16" height="16" alt="' + i18n.t("core.utilities:searchplugins.searchMatchingMfr", "Search Matching Mfrs") + '" title="' + i18n.t("core.utilities:searchplugins.searchMatchingMfr", "Search Matching Mfrs") + '" />')
							.remove();				
				// add keyup event to the text input, change text input name back to original value, clear input and apply focus
				$j( $j(this.searchDiv).find('input[type="text"]:first') )
										.attr('name', 'mfrtext')
										.keyup(function() { self.runMfrAjax(this.value, self); return false; })
										.keypress(function(event) { if (event.keyCode==13){ return false; } })
										.val('')
										.focus();
				// change the search type option
				this.options.searchType = 'mfrid';		
			}		
		},
	
		/**
		 * this function closes the search plugin and clears all inputs
		 */				
		closePlugin : function()
		{
			// clear the text input field and apply focus 
			$j( $j(this.searchDiv).find('input[type="text"]:first') ).attr('value', '').focus();
			// clear manufacturer id in the hidden input field
			$j( $j(this.searchDiv).find('> div input[type="hidden"]') ).val('');
			// empty the results div
			$j( $j(this.searchDiv).find('> div div:first') ).empty();
			// hide the search results box and footer
			$j( $j(this.searchDiv).find('> div div') ).css({zIndex: -1, display: 'none'});
		},
		
		/**
		 * this method copies the values selected in this plugin instance to any others
		 * present on the page.
		 *
		 * @param {Object} element element which we can obtain value and name for copying
		 * @param {String} direction the direction in which the value should be copied
		 * @param {String} copyParent is an element in which all mfr plugins appear and should be copied in
		 */
		copyOption : function (element, direction, copyParent)
		{
			// add marker after element
			$j(element).after('<span class="marker"></span>');
			// get the input name attribute
			var elementName = $j(element).attr('name');
			// variable to indicate the marker has been found
			var markerFound = false;
			// array to hold all inputs
			var inputArray = new Array();
			// format copy parent variable accordingly if not empty
			if (copyParent != '')
			{
				copyParent += ' ';
			}
			// default direction is to copy information down the list
			// does element name contain brace?
			if (elementName.indexOf('[') != -1)
			{
				// get the start of the element name before (first) square brace
				var elementStart = elementName.substring(0, elementName.indexOf('['));
				// create an array of input objects
				inputArray = $j.makeArray($j(copyParent + 'input[name^="' + elementStart + '"]'));
			}
			else
			{
				// create an array of input objects
				inputArray = $j.makeArray($j(copyParent + 'input[name="' + $j(element).attr('name') + '"]'));
			}	
			if (direction == 'up')
			{				
				// reverse the array
    			inputArray.reverse();
			}
			// loop through all matching inputs
			$j.each(inputArray, function()
			{
				// marker found?
				if ($j(this).next().hasClass('marker'))
				{
					// set variable and remove marker
					markerFound = true;
					$j(this).next().remove();
				}
				// marker found?
				if (markerFound)
				{
					// copy value to input
					$j(this).val($j(element).val());
				}					
			});		
		}
        
    });
})(jQuery);