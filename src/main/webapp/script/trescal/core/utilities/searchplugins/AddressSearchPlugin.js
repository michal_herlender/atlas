/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	AddressSearchPlugin.js
*	DESCRIPTION		:	Javascript file imported as part of a cascading search plugin
* 					:	Code creates an input field for holding the addrid of a selected address, 
* 					:	and a results section is then created below to display addresses returned.
* 					:	Further code uses dwr to get address results and display them on the page
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	File Created - SH - 25/07/2007
*					:	30/10/2015 - TProvost - Manage i18n
*					:	27/11/2015 - TProvost - Fixed bug on filling the "locations" (bug in runLocationAjax fucntion) 
****************************************************************************************************************************************/

							/**
 							 * this literal is called when the page loads if an element with the id of 'addrSearchPlugin' is found on the page.
							 * this function then loads all the necessary scripts it needs and checks whether the search should be cascaded
							 * further than this point? The drawSearch() function is called to create the search box.
							 * 
							 * @param cascade - boolean value to indicate whether this search should be cascaded
 							 */
    AddressSearchPlugin = {	init:			function(cascade) 	
														{
															// scripts to load
															var Scripts = new Array (	'script/thirdparty/jQuery/jquery.scrollTo.js' );																						
															// load the common javascript files for this plugin
															loadScript.loadScripts(Scripts);
															
															// set to true if this plugin is to be cascaded further
															if (cascade == true)
															{
																AddressSearchPlugin.cascade = true;
															}
															// set search locations
															AddressSearchPlugin.searchLocations = CascadeSearchPlugin.searchLocations;
															// draw search plugin
															AddressSearchPlugin.drawSearch();
															// set addressType string value
															AddressSearchPlugin.addressType = CascadeSearchPlugin.addressType;
															// set no location visibility
															AddressSearchPlugin.noLocation = CascadeSearchPlugin.noLocation;
														},
							/**
							 * string value tells the address search which type to display
							 */
							addressType:	'',
							/**
							 * false by default, if true then this cascading search should continue and use the selected subdivision subdivid to
							 * call a dwr search for contact results
							 */
							cascade:		false,							
							/**
							 * id of the hidden input field used to pass the address id
							 */
							idInput: 		'addrid',
							/**
							 * id of the main div declared in the page where the search should be created
							 */
							boxDiv: 		'addrSearchPlugin',
							/**
							 * id of the div in which the headings for the search are created
							 */
							headDiv: 		'pluginHead',
							/**
							 * id of the div in which the search results are to be displayed
							 */
							resultsDiv: 	'addrPlugResults',
							/**
							 * class name used on the results div to apply styles
							 */
							styleResDiv: 	'pluginResults',
							/**
							 * indicates if we give option of no location
							 */
							noLocation:			false,
							/**
							 * boolean value set to true when locations should be retrieved for
							 * the selected address
							 */
							searchLocations:	false,
							/**
							 * id of the hidden input field used to pass the location id
							 */
							locIdInput: 		'locid',
							/**
							 * id of the main div declared in the page where the search should be created
							 */
							locBoxDiv: 		'locSearchPlugin',
							/**
							 * id of the div in which the search results are to be displayed
							 */
							locResultsDiv: 	'locPlugResults',							
							/**
							 * value of the currently selected element in the list
							 */
							selected:		0,
							/**
							 * value of the currently selected location element in the list
							 */
							locSelected:	0,
							/**
							 * time value of last DWR call, checked against the current DWR call for addresses
							 */
							lastUpdate:		0,
							/**
							 * time value of last DWR call, checked against the current DWR call for locations
							 */
							lastUpdate2:	0,
							/**
							 * creates the input field and results div on the page
							 */				
							
							onChangeFunction: '',
							
							drawSearch:		function() 	
														{
															$j('#' + AddressSearchPlugin.boxDiv).append(	'<input type="hidden" id="' + AddressSearchPlugin.idInput + '" name="' + AddressSearchPlugin.idInput + '" />' +
																											'<div id="addr-container">' +
																												// create and append the header div
																												'<div class="' + AddressSearchPlugin.headDiv + '">' + i18n.t("core.utilities:searchplugins.matchingAddresses", "Matching Addresses") + '</div>' +
																												// create and append the div in which address results will be displayed
																												'<div id="' + AddressSearchPlugin.resultsDiv + '" class="' + AddressSearchPlugin.styleResDiv + '">' +
																													// address results go here
																												'</div>' +
																											'</div>');
															// searching for locations?						
															if (AddressSearchPlugin.searchLocations)
															{
																// add location search results container
																$j('#' + AddressSearchPlugin.locBoxDiv).append(	'<input type="hidden" id="' + AddressSearchPlugin.locIdInput + '" name="' + AddressSearchPlugin.locIdInput + '" />' +
																												'<div id="loc-container">' +
																													// create and append the header div
																													'<div class="' + AddressSearchPlugin.headDiv + '">' + i18n.t("core.utilities:searchplugins.matchingLocations", "Matching Locations") + '</div>' +
																													// create and append the div in which location results will be displayed
																													'<div id="' + AddressSearchPlugin.locResultsDiv + '" class="' + AddressSearchPlugin.styleResDiv + '">' +
																														// location results go here
																													'</div>' +
																												'</div>');
															}															
													  	},
							/**
							 * this function accepts a subdivid passed and calls an ajax method to retrieve addresses which match this value
							 * from the server.  The returned data is then passed on to function (addressCallback) for displaying on the page.
							 * 
							 * @param (Integer) subdivid - value passed to this literal from a subdivision search literal.
							 */
							runAddressAjax:	function(subdivid)
														{
															// display the address results container
															$j('#' + AddressSearchPlugin.resultsDiv + ', div#addr-container .' + AddressSearchPlugin.headDiv).css({zIndex: 100, display: 'block'});
															
															// remove all previously displayed addresses
															$j('#' + AddressSearchPlugin.resultsDiv).empty();
															
															// this span displays the loading image and text when a search has been activated
															$j('#' + AddressSearchPlugin.resultsDiv).append('<div class="loading-plug">' +
																												'<img src="img/icons/loading-anim.gif" class="img_marg_bot" height="18" width="80" alt="" />' +
																											'</div>');
															
															// only execute the following code if a subdivid has been passed
															if (subdivid != '')
															{
																// get current timestamp to pass with ajax results
																var currentTime = new Date().getTime();
																															
																// load the service javascript file if not already
																loadScript.loadScriptsDynamic(new Array('dwr/interface/addressservice.js'), true,
																{
																	callback: function()
																	{
																		// call subdiv dwr function using the subdivid
																		addressservice.getAllActiveSubdivAddressesHQL(subdivid, AddressSearchPlugin.addressType,
																		{
																			callback:function(dataFromServer)
																			{
																				AddressSearchPlugin.addressCallback(dataFromServer, currentTime, subdivid);
																			}
																		});
																	}
																});
																// cascade plugin?
																if (typeof CascadeSearchPlugin != 'undefined')
																{
																	// address link present?
																	if ($j('#' + CascadeSearchPlugin.linksDiv + ' span#cascAddAddr').length) {
																		// remove old add address link
																		$j('#' + CascadeSearchPlugin.linksDiv + ' span#cascAddAddr a').attr('href', springUrl + '/addaddress.htm?subdivid=' + subdivid);
																	}
																	else {
																		// append link for adding address
																		$j('#' + CascadeSearchPlugin.linksDiv).append('<span id="cascAddAddr">&nbsp;-&nbsp;<a href="' + springUrl + '/addaddress.htm?subdivid=' + subdivid + '" target="_blank" class="mainlink">' + i18n.t("core.utilities:searchplugins.addAddress", "Add Address") + '</a></span>');
																	}
																}
															}
															else
															{
																// remove all previously displayed addresses
																$j('#' + AddressSearchPlugin.resultsDiv).empty();
																// clear value in the addrid hidden input field
																$j('#' + AddressSearchPlugin.idInput).attr('value', '');
																// hide the address results container
																$j('#' + AddressSearchPlugin.resultsDiv + ', div#addr-container .' + AddressSearchPlugin.headDiv).css({zIndex: -1, display: 'none'});
																// address link present?
																if ($j('#' + CascadeSearchPlugin.linksDiv + ' span#cascAddAddr').length)
																{
																	// remove old add address link
																	$j('#' + CascadeSearchPlugin.linksDiv + ' span#cascAddAddr').remove();
																}
															}
															
														},
							/**
							 * this function displays the results returned from the server for ajax call 'runAddressAjax'
							 * if this plugin is to be cascaded further by showing contacts
							 * search results then a new dwr search should be instantiated 
							 * 
							 * @param addresses - array of address objects returned from the server.
							 * @param lastUpdate - time of data retrieval from the server.
							 * @param subdivid - subdivid used to get the address results will also be needed to get the contact results
							 */
							addressCallback: function(addresses, currentUpdate, subdivid) 
														{
															
															// this is a new search
															if (currentUpdate > AddressSearchPlugin.lastUpdate)
															{
																// update value in the 'lastUpdate' literal
																AddressSearchPlugin.lastUpdate = currentUpdate;
																
																// addresses length is greater than zero
																if(addresses.length > 0)
																{
																	// remove all previously displayed addresses
																	$j('#' + AddressSearchPlugin.resultsDiv).empty();
																	
																	// this search is to be prefilled
																	if (CascadeSearchPlugin.prefillIds.length > 0 && CascadeSearchPlugin.prefillFinalised == false)
																	{
																		// this variable is used to hold the index of addressid in the 'prefillIds'
																		// array according to how the cascade rules array was set
																		var prefillLocation = 0;
																		// address was the first value stored in 'cascadeRules' so it should be
																		// the third value supplied in 'prefillIds' array
																		if (CascadeSearchPlugin.cascadeRules[1] == 'address')
																		{
																			prefillLocation = 2;
																		}
																		// address was the second value stored in 'cascadeRules' so it should be
																		// the fourth value supplied in 'prefillIds' array
																		else
																		{
																			prefillLocation = 3;
																		}
																		// variable used to store the index of the address which matches the addressid
																		// supplied in the 'prefillIds' array
																		var prefillId = 0;
																		// declare variable to hold address results
																		var content = '';
																		// add anchor for each company returned
																		$j.each(addresses, function(i){
																			// addressid's match so populate variable
																			if (CascadeSearchPlugin.prefillIds[prefillLocation] == addresses[i].addrid)
																			{
																				prefillId = i;
																			}
																			
																			content = content + '<a href="#" id="' + addresses[i].addrid + '" onclick="AddressSearchPlugin.linkOnclick(this.id); return false; "><span>' + addresses[i].addr1 + ', ' + addresses[i].town + ', ' + addresses[i].postcode + '</span></a>';
																																					
																		});
																		// append content to results box
																		$j('#' + AddressSearchPlugin.resultsDiv).append(content);
																		// searching for locations also
																		if (AddressSearchPlugin.searchLocations)
																		{
																			// call method to get locations for addressid
																			AddressSearchPlugin.runLocationAjax(addresses[prefillId].addrid);
																		}
																		// populate the addrid hidden input field with the preselected addrid
																		$j('#' + AddressSearchPlugin.idInput).attr('value', addresses[prefillId].addrid);
																		// add 'selected' class name to preselected address
																		$j('#' + AddressSearchPlugin.resultsDiv + ' a').eq(prefillId).addClass('selected');
																		// scroll to the selected address
																		$j('#' + AddressSearchPlugin.resultsDiv).scrollTo( 'a.selected', 0 );
																		// populate the selected literal with the index of the preselected address
																		AddressSearchPlugin.selected = prefillId;
																		
																	}
																	else
																	{
																		// declare variable to hold address results
																		var content = '';
																		var selectHighlighted = false;
																		var highlightedIndex = -1;
																		// add anchor for each company returned
																		$j.each(addresses, function(i)
																		{	
																			// create start of address content
																			content += '<a href="#" id="' + addresses[i].addrid + '" onclick="AddressSearchPlugin.linkOnclick(this.id); return false; "><span ';
																			// is this the default subdivision address?
																			if (addresses[i].subdivDefault)
																			{																				
																				content += 'class="defadd attention" title="' + i18n.t("core.utilities:searchplugins.subdivDefaultAddress", "This is the subdivision default address") + '"';																																							
																				// calibration officer found
																				selectHighlighted = true;
																				// index has not been set
																				if (highlightedIndex == -1)
																				{
																					highlightedIndex = i;
																				}
																			}
																			// complete address content
																			content += '>' + addresses[i].addr1 + ', ' + addresses[i].town + ', ' + addresses[i].postcode + '</span></a>';
																		});
																		// append content to results box
																		$j('#' + AddressSearchPlugin.resultsDiv).append(content);
																		// default subdivision address highlighted
																		if (selectHighlighted == true)
																		{
																			// searching for locations also
																			if (AddressSearchPlugin.searchLocations)
																			{
																				// call method to get locations for addressid
																				AddressSearchPlugin.runLocationAjax(addresses[highlightedIndex].addrid);
																			}
																			// default subdivision address
																			if ($j('#' + AddressSearchPlugin.resultsDiv + ' span.defadd:first').length > 0)
																			{
																				// add 'selected' class name to first highlighted anchor
																				$j('#' + AddressSearchPlugin.resultsDiv + ' span.defadd:first').parent().addClass('selected');
																			}																			
																			// scroll to the selected address
																			$j('#' + AddressSearchPlugin.resultsDiv).scrollTo( 'a.selected', 0 );				
																			// initialise the selected literal to highlighted index
																			AddressSearchPlugin.selected = highlightedIndex;
																			// populate the addressid hidden input field with the current addressid
																			$j('#' + AddressSearchPlugin.idInput).attr('value', addresses[highlightedIndex].addrid);																			
																		}
																		else
																		{
																			// searching for locations also
																			if (AddressSearchPlugin.searchLocations)
																			{
																				// call method to get locations for addressid
																				AddressSearchPlugin.runLocationAjax(addresses[0].addrid);
																			}																	
																			// populate the addrid hidden input field with the current addrid
																			$j('#' + AddressSearchPlugin.idInput).attr('value', addresses[0].addrid);
																			// add 'selected' class name to first anchor
																			$j('#' + AddressSearchPlugin.resultsDiv + ' a:first').addClass('selected');
																			// initialise the selected literal to zero
																			AddressSearchPlugin.selected = 0;
																		}
																	}
																	
																	// search is to be cascaded further
																	if (AddressSearchPlugin.cascade == true)
																	{
																		// criteria to be cascaded is 'contact' so call ajax using subdivid
																		ContactSearchPlugin.runCascContactAjax(subdivid);
																	}
																										
																}
																else
																{
																	// remove all previously displayed addresses
																	$j('#' + AddressSearchPlugin.resultsDiv).empty();
																	// clear value in the addrid hidden input field
																	$j('#' + AddressSearchPlugin.idInput).attr('value', '');
																	// create a div to display that no results have been returned
																	$j('#' + AddressSearchPlugin.resultsDiv).append('<div class="bold center" style=" padding: 5px; ">' + i18n.t("noAddressesFound", "No Addresses Found") + '</div>');																	
																}	
															}			
															
															// execute callback function if exists
															if(CascadeSearchPlugin.addressCallBackFunction != '')
																window[CascadeSearchPlugin.addressCallBackFunction]();
															
														},
							/**
							 * this function moves the 'selected' classname to an anchor which the user has clicked on and populates the hidden input
							 * field with the selected addresses addrid.
							 * 
							 * @param addrid - id of the address being clicked
							 */
							linkOnclick: function(addrid)
												{
													// populate the addrid hidden input field with the id of this anchor (ie addrid)
													$j('#' + AddressSearchPlugin.idInput).attr('value', addrid);
													// remove the 'selected' classname from the currently selected item
													$j('#' + AddressSearchPlugin.resultsDiv + ' a').eq(AddressSearchPlugin.selected).removeClass();
													$j('#' + AddressSearchPlugin.resultsDiv + ' a').each( function(i){
														// id of this anchor matches the addrid passed
														if (this.id == addrid)
														{
															// add 'selected' classname to this anchor
															this.className = 'selected';
															// populate the selected literal with this value
															AddressSearchPlugin.selected = i;
															// searching for locations also
															if (AddressSearchPlugin.searchLocations)
															{
																// call method to get locations for addressid
																AddressSearchPlugin.runLocationAjax(addrid);
															}		
														}														
													});
													return false;
												},
												
							/**
							 * this function accepts an addrid passed and calls an ajax method to retrieve locations which match this value
							 * from the server.  The returned data is then passed on to function (locationCallback) for displaying on the page.
							 * 
							 * @param (Integer) addrid - value passed to this literal from an address search literal.
							 */
							runLocationAjax:	function(addrid)
												{
													// display the address results container
													$j('#' + AddressSearchPlugin.locResultsDiv + ', div#loc-container .' + AddressSearchPlugin.headDiv).css({zIndex: 100, display: 'block'});
												
													// remove all previously displayed locations
													$j('#' + AddressSearchPlugin.locResultsDiv).empty();
													
													// this span displays the loading image and text when a search has been activated
													$j('div#loc-container #' + AddressSearchPlugin.locResultsDiv).append(	'<div class="loading-plug">' +
																																'<img src="img/icons/loading-anim.gif" class="img_marg_bot" height="18" width="80" alt="" />' +
																															'</div>');
													
													// only execute the following code if an addrid has been passed
													if (addrid != '')
													{
														// get current timestamp to pass with ajax results
														var currentTime = new Date().getTime();
														// load the service javascript file if not already
														loadScript.loadScriptsDynamic(new Array('dwr/interface/locationservice.js'), true,
														{
															callback: function()
															{																
																// call location dwr function using the addrid
																locationservice.getAllActiveAddressLocationsHQL(addrid, true,
																{
																	callback:function(dataFromServer)
																	{																
																		AddressSearchPlugin.locationCallback(dataFromServer, currentTime, addrid);
																	}
																});
															}
														});
														// cascade plugin?
														if (typeof CascadeSearchPlugin != 'undefined')
														{																	
															// location link present?
															if ($j('#' + CascadeSearchPlugin.linksDiv + ' span#cascAddLoc').length)
															{
																// remove old add location link
																$j('#' + CascadeSearchPlugin.linksDiv + ' span#cascAddLoc a').attr('href', springUrl + '/location.htm?addrid=' + addrid);
															}
															else
															{
																// append link for adding locations
																$j('#' + CascadeSearchPlugin.linksDiv).append('<span id="cascAddLoc">&nbsp;-&nbsp;<a href="' + springUrl + '/location.htm?addrid=' + addrid + '" target="_blank" class="mainlink">' + i18n.t("core.utilities:searchplugins.addLocation", "Add Location") + '</a></span>');
															}																	
														}
													}
													else
													{
														// remove all previously displayed locations
														$j('#' + AddressSearchPlugin.locResultsDiv).empty();
														// clear value in the locationid hidden input field
														$j('#' + AddressSearchPlugin.locIdInput).attr('value', '');
														// hide the location results container
														$j('#' + AddressSearchPlugin.locResultsDiv + ', div#loc-container .' + AddressSearchPlugin.headDiv).css({zIndex: -1, display: 'none'});
														// location link present?
														if ($j('#' + CascadeSearchPlugin.linksDiv + ' span#cascAddLoc').length)
														{
															// remove old add location link
															$j('#' + CascadeSearchPlugin.linksDiv + ' span#cascAddLoc').remove();
														}
													}
													
												},
												
							/**
							 * this function displays the results returned from the server for ajax call 'runLocationAjax'. 
							 * 
							 * @param locations - array of location objects returned from the server.
							 * @param lastUpdate - time of data retrieval from the server.
							 */
							locationCallback: 	function(locations, currentUpdate) 
												{
													
													// this is a new search
													if (currentUpdate > AddressSearchPlugin.lastUpdate2)
													{
														// update value in the 'lastUpdate' literal
														AddressSearchPlugin.lastUpdate2 = currentUpdate;
														
														// locations length is greater than zero
														if(locations.length > 0)
														{
															// remove all previously displayed locations
															$j('#' + AddressSearchPlugin.locResultsDiv).empty();
															
															// this search is to be prefilled
															if (CascadeSearchPlugin.prefillIds.length > 0 && CascadeSearchPlugin.prefillFinalised == false)
															{
																// this variable is used to hold the index of locationid in the 'prefillIds' array
																var prefillLocation = 4;
																// variable used to store the index of the location which matches the locationid
																// supplied in the 'prefillIds' array
																var prefillId = null;
																// declare variable to hold location results
																var content = '';													
																// add anchor for each location returned
																$j.each(locations, function(i)
																{
																	// locationid's match so populate variable
																	if (CascadeSearchPlugin.prefillIds[prefillLocation] == locations[i].locationid)
																	{
																		prefillId = i;
																	}																	
																	content += '<a href="#" id="' + locations[i].locationid + '" onclick="AddressSearchPlugin.locLinkOnclick(this.id); return false;"><span>' + locations[i].location + '</span></a>';
																																			
																});
																// append content to results box
																$j('#' + AddressSearchPlugin.locResultsDiv).append(content);
																
																if (prefillId != null)
																{
																	// do we allow selection of no location?
																	if (AddressSearchPlugin.noLocation)
																	{
																		// prepend no location option			
																		$j('#' + AddressSearchPlugin.locResultsDiv).prepend('<a href="#" id="" onclick="AddressSearchPlugin.locLinkOnclick(this.id); return false;"><span>--- ' + i18n.t("core.utilities:searchplugins.notKnownOrSpecified", "Not Known/Specified") + ' ---</span></a>');	
																		// add one to prefill id for no location option
																		prefillId++;
																	}
																	// populate the locationid hidden input field with the preselected locationid
																	$j('#' + AddressSearchPlugin.locIdInput).attr('value', locations[prefillId].locationid);
																	// add 'selected' class name to preselected location
																	$j('#' + AddressSearchPlugin.locResultsDiv + ' a').eq(prefillId).addClass('selected');
																	// scroll to the selected location
																	$j('#' + AddressSearchPlugin.locResultsDiv).scrollTo( 'a.selected', 0 );
																	// populate the selected literal with the index of the preselected location
																	AddressSearchPlugin.locSelected = prefillId;
																}
																else
																{
																	// do we allow selection of no location?
																	if (AddressSearchPlugin.noLocation)
																	{
																		// prepend no location option			
																		$j('#' + AddressSearchPlugin.locResultsDiv).prepend('<a href="#" id="" onclick="AddressSearchPlugin.locLinkOnclick(this.id); return false;"><span>--- ' + i18n.t("core.utilities:searchplugins.notKnownOrSpecified", "Not Known/Specified") + ' ---</span></a>');	
																	}
																	// populate the locationid hidden input field with the preselected locationid
																	$j('#' + AddressSearchPlugin.locIdInput).attr('value', '');
																	// add 'selected' class name to preselected location
																	$j('#' + AddressSearchPlugin.locResultsDiv + ' a').eq(0).addClass('selected');
																	// scroll to the selected location
																	$j('#' + AddressSearchPlugin.locResultsDiv).scrollTo( 'a.selected', 0 );
																	// populate the selected literal with the index of the preselected location
																	AddressSearchPlugin.locSelected = 0;
																}																																
															}
															else
															{
																// declare variable to hold location results
																var content = '';
																// add anchor for each location returned
																$j.each(locations, function(i)
																{																			
																	content = content + '<a href="#" id="' + locations[i].locationid + '" onclick="AddressSearchPlugin.locLinkOnclick(this.id); return false;"><span>' + locations[i].location + '</span></a>';
																});
																// append content to results box
																$j('#' + AddressSearchPlugin.locResultsDiv).append(content);
																// do we allow selection of no location?
																if (AddressSearchPlugin.noLocation)
																{
																	// prepend no location option			
																	$j('#' + AddressSearchPlugin.locResultsDiv).prepend('<a href="#" id="" onclick="AddressSearchPlugin.locLinkOnclick(this.id); return false;"><span>--- ' + i18n.t("core.utilities:searchplugins.notKnownOrSpecified", "Not Known/Specified") + ' ---</span></a>');	
																	// populate the locationid hidden input field with the current locationid
																	$j('#' + AddressSearchPlugin.locIdInput).attr('value', '');
																}
																else
																{
																	// populate the locationid hidden input field with the current locationid
																	$j('#' + AddressSearchPlugin.locIdInput).attr('value', locations[0].locationid);
																}																
																// add 'selected' class name to first anchor
																$j('#' + AddressSearchPlugin.locResultsDiv + ' a:first').addClass('selected');
																// initialise the selected literal to zero
																AddressSearchPlugin.locSelected = 0;
															}									
																								
														}
														else
														{
															// remove all previously displayed locations
															$j('#' + AddressSearchPlugin.locResultsDiv).empty();
															// clear value in the locationid hidden input field
															$j('#' + AddressSearchPlugin.locIdInput).attr('value', '');
															// create a div to display that no results have been returned
															$j('#' + AddressSearchPlugin.locResultsDiv).append('<div class="bold center" style=" padding: 5px; ">' + i18n.t("core.utilities:searchplugins.NoLocationsFound", "No Locations Found") + '</div>');																	
														}											
													}															
												},
												
							/**
							 * this function moves the 'selected' classname to an anchor which the user has clicked on and populates the hidden input
							 * field with the selected locations locationid.
							 * 
							 * @param locationid - id of the location being clicked
							 */
							locLinkOnclick: function(locationid)
												{
													// populate the locationid hidden input field with the id of this anchor (ie locationid)
													$j('#' + AddressSearchPlugin.locIdInput).attr('value', locationid);
													// remove the 'selected' classname from the currently selected item
													$j('#' + AddressSearchPlugin.locResultsDiv + ' a').eq(AddressSearchPlugin.locSelected).removeClass();
													$j('#' + AddressSearchPlugin.locResultsDiv + ' a').each( function(i){
														// id of this anchor matches the locationid passed
														if (this.id == locationid)
														{
															// add 'selected' classname to this anchor
															this.className = 'selected';
															// populate the selected literal with this value
															AddressSearchPlugin.locSelected = i;	
														}														
													});
													return false;
												}				  				
													
							}
							