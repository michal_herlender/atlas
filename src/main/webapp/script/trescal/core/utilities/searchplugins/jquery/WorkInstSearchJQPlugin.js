/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	WorkInstSearchJQPlugin.js
*	DESCRIPTION		:	 
*
*	DEPENDENCIES	:   script/thirdparty/jQuery/jquery.js
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	05/09/2008 - SH - Created File
*					:	29/10/2015 - TProvost - Fix bug on test of the version of browser
*					:	30/10/2015 - TPRovost - Manage i18n
****************************************************************************************************************************************/

(function( $j )
{
	// variable to indicate whether any navigation keys have been pressed to stop ajax
	var keyPress = false;
	// variable to indicate which anchor in list is currently selected - used for key navigation
	var selected = 0;
	// variable to hold time value of last DWR call, checked against the current DWR call
	var lastUpdate = 0;
    
    $j.fn.workinstSearchJQPlugin = function( options )
    {
        var options = $j.extend({
			
			/**
			 * name of the hidden input field
			 */
			field:			'',
			/**
			 * function to be performed when selection is changed?
			 */
			customFunc: 	'',
			/**
			 * copy option allows the user to add a image link which will
			 * copy the chosen work inst to all other occurences on page. This option should
			 * only be provided for the first occurence of plugin.
			 */
			copyOption: 	'false',
			/**
			 * copy parent is an element in which all copies should be performed
			 * this should be defined with the jquery prefix for id or class name (i.e. #test, .test)
			 */
			copyParent:		'',
			/**
			 * default work instruction current version approved
			 */
			defaultWiApproved: 	'',
			/**
			 * default work instruction id
			 */
			defaultWiId:		'',
			/** 
			 * default work instruction text
			 */
			defaultWiText: 	'',
			/**
			 * container div
			 */
			container: 		'workinstContainerJQPlugin',
            /**
			 * class name of the results div
			 */
			resultsDiv: 	'workinstResultsJQPlugin',
			/**
			 * class name of the footer div
			 */
			footDiv: 		'workinstFootJQPlugin'
			
        }, options);

        return this.each(function()
        {
            workinstSearchJQPlugin(this, options);
        });
    };

    function workinstSearchJQPlugin( element, options )
    {
        return this instanceof workinstSearchJQPlugin
            ? this.init(element, options)
            : new workinstSearchJQPlugin(element, options);
    }

    $j.extend(workinstSearchJQPlugin.prototype,
    {
        original : null,
        options  : {},

        element  : null,
        searchDiv   : null,

		/**
		 * plugin initialisation function
		 * 
		 * @param {Object} element is the current plugin div to manipulate
		 * @param {Object} options are options for the current plugin
		 */
        init : function( element, options )
        {			
			// initialise search div
            this.searchDiv = element;
			// initialise options
            this.options = options || {};
			
            // script to load for dwr service
			var Scripts = new Array (	'dwr/interface/WorkInstructionService.js' );
			// load the common javascript files for this plugin
			loadScript.loadScripts(Scripts);
			
			// set this div object
			var self = this;
			
			// call method to draw plugin search
            this.drawSearch();
					
			// add keyup event to the text input and bind method to call work instruction dwr service
			$j( $j(this.searchDiv).find('input[type="text"]:first') ).keyup(function() { self.runWorkInstAjax(this.value, self); return false; });
			// add click event to the image following the text input field which retrieves all results
			$j( $j(this.searchDiv).find('img:first') ).click(function() { self.runWorkInstAjax('%', self); return false; });
			// add click event to the close image in footer div and bind method to close the plugin
			$j( $j(this.searchDiv).find('div.workinstContainerJQPlugin img:last') ).click(function() { self.closePlugin(); return false; });
			
			// copy selected work instruction to other fields required
			if (this.options.copyOption == 'true') 
			{
				// add option to variable as it is not available in loop for some reason
				var copyParent = this.options.copyParent;
				// add click event to the anchor and bind method to call copy option method
				// for text input field and hidden input field
				$j($j(this.searchDiv).find('> a')).each(function(i)
				{
					// variable to hold direction of copy
					var direction = 'up';
					// first anchor
					if (i < 1)
					{
						direction = 'down';
					}
										
					// add click function to copy down anchor
					$j(this).click(function()
					{
						// call copy function
						self.copyOption($j(this).parent().find('input[type="text"]:first'), direction, false, copyParent);
						// call copy function
						self.copyOption($j(this).parent().find('div:first input[type="hidden"]:last'), direction, true, copyParent);
						return false;
					});
				});
			}
			
			/**
			 * start jquery keypress observer to check if the user has pressed the up/down arrow or the
			 * enter/esc key when searching for work inst's
			 * 
			 */ 	
			$j( $j(this.searchDiv) ).keydown(function(e) 
			{			
			    var event = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
				
				switch (event)
				{
					//  up arrow key has been pressed
					case 38: keyPress = true;
							 // check selected variable
							 if (selected == 0)
							 {
							 	
							 }
							 // selected variable contains value greater than zero
							 else
							 {
							 	 // remove 'selected' class name from the currently selected element and add to the previous one, apply focus
								 $j(this).find('> div div:first a').eq(selected).removeClass().prev().addClass('selected').focus();
								 // remove one from the selected variable value
								 selected = selected - 1;
								 // then return focus to the input field
								 $j(this).find('input[type="text"]:first').focus();
							 }
							 break;
							 
					// down arrow key has been pressed
					case 40: keyPress = true;
							 // check selected variable is not greater than list length
							 if (selected == $j(this).find('> div div:first a').size() - 1)
							 {
							 
							 }
							 // selected variable contains value less than list length
							 else
							 {
							 	// remove 'selected' class name from the currently selected element and add to the next one, apply focus
								$j(this).find('> div div:first a').eq(selected).removeClass().next().addClass('selected').focus();
								// remove one from the selected variable value
								selected = selected + 1;
								// then return focus to the input field
								$j(this).find('input[type="text"]:first').focus();
							 }
							 break;
							 
					// escape key has been pressed
					case 27: keyPress = true;
							 // call function to close plugin
							 self.closePlugin();
							 break;
							 
					// enter key has been pressed
					case 13: keyPress = true;
							 // trigger click event of anchor
						 	 $j(this).find('> div div:first a').eq(selected).trigger('click');
							 break;
					
					// left arrow key has been pressed, just stop ajax from being initiated if pressed accidentally
					case 37: keyPress = true;
							 break;
							 
					// right arrow key has been pressed, just stop ajax from being initiated if pressed accidentally
					case 39: keyPress = true;
							 break;
							 
					// tab key has been pressed, just stop ajax from being initiated if pressed accidentally
					case 9: keyPress = true;
							break;
				}
				
			});
        },
		
		/**
		 * method to draw plugin search
		 */
        drawSearch : function()
        {
			var content = 	'<input type="text" name="witext"';
							if ((this.options.defaultWiApproved != '') && (this.options.defaultWiApproved != 'CURRENT'))
							{
								content += ' class="highlight"';
							}
				content += 	' value="' + this.options.defaultWiText + '" onkeypress=" if (event.keyCode==13){ return false; } " autocomplete="off" />' +
							'<img src="img/icons/searchplugin_all.png" width="16" height="16" alt="' + i18n.t("core.utilities:searchplugins.viewAll", "View All") + '" title="' + i18n.t("core.utilities:searchplugins.viewAll", "View All") + '" />';
							// copy options required
							if 	(this.options.copyOption == 'true')
							{
								content = content +	'<a href="#">' +
														'<img src="img/icons/arrow_down.png" width="10" height="16" alt="' + i18n.t("copySelectionDown", "Copy selection down") + '" title="' + i18n.t("copySelectionDown", "Copy selection down") + '" />' +
													'</a>' + 
													'<a href="#">' +
														'<img src="img/icons/arrow_up.png" width="10" height="16" alt="' + i18n.t("copySelectionUp", "Copy selection up") + '" title="' + i18n.t("copySelectionUp", "Copy selection up") + '" />' +
													'</a>';
							}
		content = content +	'<div class="' + this.options.container + '">' +
								// create and append the div in which work instruction results will be displayed
								'<div class="' + this.options.resultsDiv + '">' +
									// results are displayed here
								'</div>' +
								// create and append footer with close button
								'<div class="' + this.options.footDiv + '"><span style=" line-height: 18px; margin-right: 18px; ">(' + i18n.t("core.utilities:searchplugins.escKey", "Esc Key") + ') ' + i18n.t("core.utilities:searchplugins.or", "or") + ' </span><img src="img/icons/bullet-cross.png" width="10" height="10" alt="' + i18n.t("core.utilities:searchplugins.closeWorkInstructionSearch", "Close Work Instruction Search") + '" title="' + i18n.t("core.utilities:searchplugins.closeWorkInstructionSearch", "Close Work Instruction Search") + '" class="pluginClose" /></div>' +
								'<input type="hidden" name="' + this.options.field + '" value="' + this.options.defaultWiId + '" />' +
							'</div>';
						
			// append plugin content to main div
			$j(this.searchDiv).append(content);
			// mozilla or opera browser						
			if ($j.browser != null && ($j.browser.mozilla != null || $j.browser.opera != null))
			{
				$j(this.searchDiv).parent('li').css({ paddingBottom: '4px'});
			}
			// other browsers
			else
			{
				$j(this.searchDiv).parent('li').css({ paddingBottom: '3px'});
			}
			
        },
		
		/**
		 * method which accepts the name fragment provided from the input field and
		 * calls the dwr service to retrieve work instructions
		 * 
		 * @param {String} nameFragment text entered into input field which should be used to search work instructions
		 * @param {Object} self reference to this object
		 */
		runWorkInstAjax : function(nameFragment, self)
		{			
			// navigation keys have been pressed so set value back to false and skip ajax loading
			if (keyPress == true)
			{
				keyPress = false;
			}
			else
			{
				// another search is in use
				if ( ($j( $j(this.searchDiv).find('> div div:first a') ).length < 1) && ($j('div.' + this.options.resultsDiv + ' > a').length > 0) )
				{
					// reset input field
					$j( $j(this.searchDiv).find('input[type="text"]:first') ).attr('value', '');
					// alert user that another email plugin is in use
					$j.prompt(i18n.t("core.utilities:searchplugins.anotherPluginIsInUse", "Another plugin is in use, please close before continuing!"));
					// exit method
					return false;
				}																
				// remove all previously displayed work instructions
				$j( $j(this.searchDiv).find('> div div:first') ).empty();
				// display the work instruction results container
				$j( $j(this.searchDiv).find('> div div') ).css({zIndex: 100, display: 'block'});																	
				// this span displays the loading image and text when a search has been activated
				$j( $j(this.searchDiv).find('> div div:first') ).append('<div class="loading-plug">' +
																			'<img src="img/icons/loading-anim.gif" class="img_marg_bot" height="18" width="80" alt="" />' +
																		'</div>');				
				// get current timestamp to pass with ajax results
				var currentTime = new Date().getTime();
				// only execute the ajax functionality if the text entered has a length
				// greater than zero (stops unconstrained retrieval)
				if ((nameFragment.length > 1) || (nameFragment == '%'))
				{
					// call dwr service to retrieve work instructions
					WorkInstructionService.searchWorkInstructionsHQL(nameFragment,
					{
						callback:function(workinsts)
						{
							// call method to display work instructions returned
							self.workInstCallback(workinsts, currentTime, self);
						}
					});
				}
				// text entered has a length of 0
				else
				{														
				 	// hide the work instruction results container
					$j( $j(this.searchDiv).find('> div div') ).css({zIndex: -1, display: 'none'});
					// remove all previously displayed work instructions
					$j( $j(this.searchDiv).find('> div div:first') ).empty();
					// clear workinstid hidden input field
					$j( $j(this.searchDiv).find('> div input[type="hidden"]') ).val('');				
				}
			}
			
		},
		
		/**
		 * this method appends all work instructions returned from the dwr service to the results div
		 * 
		 * @param {Array} workinsts is an array of work instructions returned from the dwr service
		 * @param {Date/Time} currentUpdate is a timestamp retrieved when the search was initiated
		 * @param {Object} self reference to this object
		 */
		workInstCallback : function(workinsts, currentUpdate, self) 
		{
			// this is a new search
			if (currentUpdate > lastUpdate)
			{
				// update value in the 'lastUpdate' variable
				lastUpdate = currentUpdate;
				
				// work instructions length is greater than zero
				if(workinsts.length > 0)
				{
					// remove all previously displayed work instructions
					$j( $j(this.searchDiv).find('> div div:first') ).empty();
					// declare variable to hold results content
					var content = '';
					// add anchor for each work instruction returned to results variable
					$j(workinsts).each(function(i)
					{	
						content += '<a href="#" id="' + workinsts[i].id + '<->' + workinsts[i].title + '<->' + workinsts[i].state + '"';
						if (workinsts[i].state != 'CURRENT')
						{
							content += ' title="' + i18n.t("core.utilities:searchplugins.workInstructionNotApporved", "WI Not Approved") + '"><span class="attention">';
						}
						else
						{
							content += ' title="' + i18n.t("core.utilities:searchplugins.workInstructionApproved", "WI Approved") + '"><span>';
						}
						content += '' + workinsts[i].title + '</span></a>';																																			
					});
					// append results variable to results div
					$j( $j(this.searchDiv).find('> div div:first') ).append(content);
					
					// loop through all anchors appended to results div
					$j( $j(this.searchDiv).find('> div div:first a') ).each(function(i)
					{
						// apply click event to anchor
						$j( $j(this) ).click( function(){ self.selectWorkInstId(this.id, self); return false; } );
					});			
					// add 'selected' class name to first anchor in results div
					$j( $j(this.searchDiv).find('> div div:first a:first') ).addClass('selected');
					// initialise the selected variable to zero
					selected = 0;																						
				}
				else
				{
					// remove all previously displayed work instructions
					$j( $j(this.searchDiv).find('> div div:first') ).empty().append('<div class="center bold">' + i18n.t("core.utilities:searchplugins.noWorkInstructionsFound", "No Work Instructions Found") + '</div>');
				}
			}
		},
		
		/**
		 * this method selects the chosen work instruction and adds the name to the text input field and adds the
		 * workinstid to the hidden input field after which the results div is emptied and hidden
		 * 
		 * @param {String} workinstIdAndTitle id and name of the work instruction chosen
		 * @param {Object} self reference to this object
		 */
		selectWorkInstId : function (workinstIdAndTitle, self)
		{
			// add id and reference to array
			var workinstArray = workinstIdAndTitle.split('<->');
			// add work instruction id to the hidden input field
			$j( $j(this.searchDiv).find('> div input[type="hidden"]') ).val(workinstArray[0]);
			
			// is work instruction approved?
			if (workinstArray[2] != 'CURRENT')
			{
				// add work instruction text to the search text input field and add highlight class as work instruction is not approved
				$j( $j(this.searchDiv).find('> input[type="text"]:first') ).val(workinstArray[1]).attr('class', 'highlight');
			}
			else
			{
				// add work instruction text to the search text input field and clear class as work instruction is approved
				$j( $j(this.searchDiv).find('> input[type="text"]:first') ).val(workinstArray[1]).attr('class', '');
			}
			// empty the results div
			$j( $j(this.searchDiv).find('> div div:first') ).empty();
			// hide the search results box and footer
			$j( $j(this.searchDiv).find('> div div') ).css({zIndex: -1, display: 'none'});
			// custom method to call?
			if(this.options.customFunc != '')
			{
				// trigger the onclick event of customFunc input
				$j( $j(this.searchDiv).find('> input[name="' + this.options.customFunc + '"]') ).trigger('onclick');
			} 
		},
	
		/**
		 * this function closes the search plugin and clears all inputs
		 */				
		closePlugin : function()
		{
			// clear the text input field and apply focus 
			$j( $j(this.searchDiv).find('input[type="text"]:first') ).attr('value', '').focus();
			// clear work instruction id in the hidden input field
			$j( $j(this.searchDiv).find('> div input[type="hidden"]') ).val('');
			// empty the results div
			$j( $j(this.searchDiv).find('> div div:first') ).empty();
			// hide the search results box and footer
			$j( $j(this.searchDiv).find('> div div') ).css({zIndex: -1, display: 'none'});
		},
		
		/**
		 * this method copies the values selected in this plugin instance to any others
		 * present on the page.
		 *
		 * @param {Object} element element which we can obtain value and name for copying
		 * @param {String} direction the direction in which the value should be copied
		 * @param {Boolean} triggerFunction indicates if the custom function should be triggered
		 * @param {String} copyParent is an element in which all work inst plugins appear and should be copied in
		 */
		copyOption : function (element, direction, triggerFunction, copyParent)
		{
			// add marker after element
			$j(element).after('<span class="marker"></span>');
			// get the input name attribute
			var elementName = $j(element).attr('name');
			// assign option to variable
			var customFunc = this.options.customFunc;
			// variable to indicate the marker has been found
			var markerFound = false;
			// array to hold all inputs
			var inputArray = new Array();
			// format copy parent variable accordingly if not empty
			if (copyParent != '')
			{
				copyParent += ' ';
			}
			// copy information down the list
			if (direction == 'down')
			{				
				// does element name contain full stop?
				if (elementName.indexOf('.') != -1)
				{
					// get the end of the element name after full stop
					var elementEnd = elementName.substring((elementName.lastIndexOf('.') + 1), elementName.length);
					// create an array of input objects
					inputArray = $j.makeArray($j(copyParent + 'input[name$="' + elementEnd + '"]'));
				}
				else
				{
					// create an array of input objects
					inputArray = $j.makeArray($j(copyParent + 'input[name="' + $j(element).attr('name') + '"]'));
				}	
			}
			else
			{
				// does element name contain full stop?
				if (elementName.indexOf('.') != -1) 
				{
					// get the end of the element name after full stop
					var elementEnd = elementName.substring((elementName.lastIndexOf('.') + 1), elementName.length);
					// create an array of input objects
					inputArray = $j.makeArray($j(copyParent + 'input[name$="' + elementEnd + '"]'));
					// reverse the array
	    			inputArray.reverse();
				}
				else 
				{
					// create an array of input objects
					inputArray = $j.makeArray($j(copyParent + 'input[name="' + $j(element).attr('name') + '"]'));
					// reverse the array
	    			inputArray.reverse();
				}
			}
			
			// loop through all matching inputs
			$j.each(inputArray, function()
			{
				// marker found?
				if ($j(this).next().hasClass('marker'))
				{
					// set variable and remove marker
					markerFound = true;
					$j(this).next().remove();
				}
				// marker found?
				if (markerFound)
				{
					// copy value to input
					$j(this).val($j(element).val());
					// custom function option has value?
					if ((customFunc != '') && (triggerFunction == true)) 
					{
						// trigger the onclick event of customFunc input
						$j(this).parent().parent().find('input[name="' + customFunc + '"]').trigger('onclick');
					}
				}					
			});	
		}        
    });
})(jQuery);