/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	PresetCommentJQPlugin.js
*	DESCRIPTION		:	This plugin creates a select box and link above the textarea which allows the user to select a preset comment
*					: 	which is loaded into the select box using dwr.  The link takes the user to the edit preset comments page.
*
*	DEPENDENCIES	:   script/thirdparty/jQuery/jquery.js
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	12/03/2008 - SH - Created File
*					:	26/10/2015 - TProvost - Add test on value of $j.browser	
*					:	30/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

(function( $j )
{
    $j.fn.presetCommentJQPlugin = function( options ) {
        var options = $j.extend({
			symbolBox: 	false
		}, options);
        return this.each(function()
        {
            presetCommentJQPlugin(this, options);
        });
    };

    function presetCommentJQPlugin( element, options )
    {
        return this instanceof presetCommentJQPlugin
            ? this.init(element, options)
            : new presetCommentJQPlugin(element, options);
    }

    $j.extend(presetCommentJQPlugin.prototype,
    {
        original 		: null,
        options  		: {},

        element  		: null,
        textArea   		: null,
		textAreaWidth	: null,
		lastUpdate		: null,
		firstTime 		: true,

		/**
		 * plugin initialisation function
		 * 
		 * @param {Object} element is the current textarea used to initialise plugin
		 * @param {Object} options are options for the current plugin
		 */
        init : function( element, options )
        {
			// initialise textarea
            this.textArea = element;
			// initialise options
            this.options = options || {};		
			// get the width of the current textarea on page
			this.textAreaWidth = $j( $j(this.textArea) ).width();
			
			if(this.textAreaWidth == 0)
			{
				this.textAreaWidth = 500;
			}
			
			// set this textarea object
			var self = this;
			// variable to indicate whether editor being used
			var editor = false;
			// source the preset comment type required from class attribute
			var classAtt = $j( $j(this.textArea) ).attr('class');
			// initialise type variable
			var type = '';
			// check to see if presetComments class present
			if (classAtt.indexOf('presetComments ') != -1)
			{
				type = classAtt.substring((classAtt.indexOf('presetComments ') + 15), classAtt.length);
			}
			else
			{
				type = classAtt;	
			}
			// using editor?
			if (classAtt.indexOf('editor ') != -1)
			{
				editor = true;				
				// set width of editor
				this.textAreaWidth = 660;
			}
			// create and draw the preset comment div, select box and link
			this.drawPresetComments(type, editor, self);
			// gets all preset comments for this comment type
			this.getPresetCommentOptions(type, editor, self);
			// symbol box to be created?
			if (this.options.symbolBox)
			{
				// draw div and link to open symbol box
				this.drawSymbolLink(self);
			}
        },
		
		/**
		 * this method creates and draws preset comments
		 * 
		 * @param {String} type the type of preset comments we are using
		 * @param {Boolean} editor is an editor being used
		 * @param {Object} self reference to this object
		 */
        drawPresetComments : function(type, editor, self)
        {			
        	// create the preset comment content and add to variable
			var content = 	'<div class="float-left relative" style=" width: ' + this.textAreaWidth + 'px; ">' +
								'<select class="presetCommentSelection width86">' +
									'<option value="" selected="selected"> -- ' + i18n.t("core.utilities:plugins.selectPresetCommentToAdd", "Select a preset comment to add") + ' -- </option>' +
								'</select>' +
								'<a href="#">' +
									'<img src="img/icons/form_edit.png" width="16" height="16" alt="' + i18n.t("core.utilities:plugins.editPresentComments", "Edit Preset Comments") + '" title="' + i18n.t("core.utilities:plugins.editPresentComments", "Edit Preset Comments") + '" />' +
								'</a>&nbsp;' +
								'<a href="#">' +
									'<img src="img/icons/refresh.png" width="16" height="16" alt="' + i18n.t("core.utilities:plugins.refreshPresetCommentList", "Refresh Preset Comment List") + '" title="' + i18n.t("core.utilities:plugins.refreshPresetCommentList", "Refresh Preset Comment List") + '" />' +
								'</a>&nbsp;';
								// ie browser being used?
								if ($j.browser && $j.browser.msie)
								{
									content +=	'<a href="#">' +
													'<img src="img/icons/spellcheck.png" width="16" height="16" alt="' + i18n.t("core.utilities:checkSpelling", "Check Spelling") + '" title="' + i18n.t("core.utilities:checkSpelling", "Check Spelling") + '" />' +
												'</a>';
								}
				content += 	'</div>';
			// editor in use
			if (editor)
			{
				if ($j(this.textArea).parent().attr('id') === undefined)
				{
					// add the content before the textarea
					$j( $j(this.textArea) ).before(content);
					// add click event to load preset comments and onchange event to add the chosen preset comment to the textarea
					$j( $j(this.textArea).prev().find('select:first') ).change(function(){ self.addCommentAndReset(this, editor, self); return false; });
					// add click event to the edit preset comment anchor which takes the user to the edit preset comments page
					$j( $j(this.textArea).prev().find('a:eq(0)') ).click(function(){ self.editPresetComments(type); return false; });
					// add click event to refresh the preset comment list
					$j( $j(this.textArea).prev().find('a:eq(1)') ).click(function(){ self.getPresetCommentOptions(type, editor, self); return false; });
					// ie browser being used?
					if ($j.browser && $j.browser.msie)
					{
						// add click event to spellcheck
						$j( $j(this.textArea).prev().find('a:eq(2)') ).click(function(){ loadScript.spellCheck($j(this).parent().next(), null); return false; });
					}
				}
				else
				{
					// add the content before the textarea
					$j( $j(this.textArea) ).parent().before(content);
					// add click event to load preset comments and onchange event to add the chosen preset comment to the textarea
					$j( $j(this.textArea).parent().prev().find('select:first') ).change(function(){ self.addCommentAndReset(this, editor, self); return false; });
					// add click event to the edit preset comment anchor which takes the user to the edit preset comments page
					$j( $j(this.textArea).parent().prev().find('a:eq(0)') ).click(function(){ self.editPresetComments(type); return false; });
					// add click event to refresh the preset comment list
					$j( $j(this.textArea).parent().prev().find('a:eq(1)') ).click(function(){ self.getPresetCommentOptions(type, editor, self); return false; });
					// ie browser being used?
					if ($j.browser && $j.browser.msie)
					{
						// add click event to spellcheck
						$j( $j(this.textArea).parent().prev().find('a:eq(2)') ).click(function(){ loadScript.spellCheck($j(this).parent().next().find('iframe').contents().find('body'), null); return false; });
					}
				}
			}
			else
			{
				// add the content before the textarea
				$j( $j(this.textArea) ).before(content);
				// add click event to load preset comments and onchange event to add the chosen preset comment to the textarea
				$j( $j(this.textArea).prev().find('select:first') ).change(function(){ self.addCommentAndReset(this, editor, self); return false; });
				// add click event to the edit preset comment anchor which takes the user to the edit preset comments page
				$j( $j(this.textArea).prev().find('a:eq(0)') ).click(function(){ self.editPresetComments(type); return false; });
				// add click event to refresh the preset comment list
				$j( $j(this.textArea).prev().find('a:eq(1)') ).click(function(){ self.getPresetCommentOptions(type, editor, self); return false; });
				// ie browser being used?
				if ($j.browser && $j.browser.msie)
				{
					// add click event to spellcheck
					$j( $j(this.textArea).prev().find('a:eq(2)') ).click(function(){ loadScript.spellCheck($j(this).parent().next(), null); return false; });
				}
			}									
        },
		
		/**
		 * this method retrieves all saved preset comments for the selected type
		 * 
		 * @param {String} type the type of preset comments we are retrieving
		 * @param {Boolean} editor is an editor being used
		 * @param {Object} self reference to this object
		 */
		getPresetCommentOptions : function(type, editor, self)
		{
			// get current timestamp to pass with dwr results
			var currentTime = new Date().getTime();
			// ajax call to retrieve all preset comments of selected type
			$j.ajax({
				url: "getpresetcomments.json",
				data: {
					commentType: type
				},
				async: true
			}).done(function(presetComments) {
				// call method to add preset comments to the select box
				self.addPresetCommentOptions(presetComments, currentTime, editor, self);
			}).fail(function(error) {
				alert("Sorry, an error occurs while loading preset comments.");
			});						
		},
		
		/**
		 * this method adds the retrieved preset comments to the select box
		 * 
		 * @param {Object} coms arraylist of preset comments
		 * @param {Object} currentUpdate the time the preset comments where retrieved
		 * @param {Boolean} editor is an editor being used
		 * @param {Object} self reference to this object
		 */
		addPresetCommentOptions : function(coms, currentUpdate, editor, self)
		{
			// this is a new search
			if (currentUpdate > this.lastUpdate)
			{
				// update value in the 'lastUpdate' variable
				this.lastUpdate = currentUpdate;				
				// variable to hold preset comment options
				var options = '';
				// have any comments been returned?
				if (coms.length > 0)
				{
					// get all preset comments and add to options
					$j.each(coms, function(i)
					{
						options += '<option value="' + coms[i].comment + '" title="' + coms[i].comment + '">' + coms[i].comment + '</option>';
					});	
				}
				// editor in use
				if (editor)
				{
					// remove any existing options
					$j('select.presetCommentSelection option:not(:first)').remove();
					// add preset comment options to the select box
					$j('select.presetCommentSelection option:first').after(options);
				}
				else
				{
					// remove any existing options
					$j( $j(this.textArea).prev().find('select option:not(:first)') ).remove();
					// add preset comment options to the select box
					$j( $j(this.textArea).prev().find('select option:first') ).after(options);
				}	
			}								
		},
		
		/**
		 * this method gets the preset comment value selected and adds it to the textarea
		 * 
		 * @param {Object} selectBox preset comment select box object
		 * @param {Boolean} editor is an editor being used
		 * @param {Object} this implementation of preset comment plugin
		 */				
		addCommentAndReset : function(selectBox, editor, self)
		{
			// type error in htmlarea - due to compatibility issues with jQuery 1.9 and larger?
			// reset the select list to the default option before
			var txt = selectBox.value;
			$j(selectBox).val('');
			// editor in use
			if (editor) {
				// add the selected preset comment to the editor
				$j('textarea.editor').htmlarea("pasteHTML", '<p>' + txt + '</p>');
			}
			else {
				// add the selected preset comment to the textarea
				$j( $j(this.textArea) ).val($j( $j(this.textArea) ).val() + txt + ' ');
			}
		},
		
		/**					
		 * this method directs the user to the edit preset comments page which will list comments for the given type
		 *
		 * @param {String} type the type of preset comments we are using
		 */						
		editPresetComments: function(type)
		{
			window.open("editpresetcomments.htm?type=" + type);
		},
		
		/**
		 * this method creates content for the link to open the symbol box and appends it before the textarea
		 * 
		 * @param {Object} this implementation of plugin
		 */
		drawSymbolLink : function(self)
		{
			// div and link to open symbol box content
			var content = 	'<span class="relative">' +
								'<a href="#" class="initialiseSymbolBox">' +
									'<img src="img/icons/symbol_add.png" id="addSymbolLink" width="16" height="16" alt="' + i18n.t("core.utilities:addSymbol", "Add Symbol") + '" title="' + i18n.t("core.utilities:addSymbol", "Add Symbol") + '" />' +
								'</a>' +
							'</span>';
			// add content before the textarea
			$j( $j(this.textArea).prev() ).append(content);
			// add click event to draw symbol box on demand
			$j( $j(this.textArea).prev().find('a.initialiseSymbolBox') ).click(function() { self.drawSymbolBox(self); return false; });
		},
		
		/**
		 * method to draw symbol box
		 * 
		 * @param {Object} this implementation of plugin
		 */
        drawSymbolBox : function(self)
        {
			// content for the symbol box that is created above the textarea
			var content = loadScript.masterSymbolBoxContent();
			// append the symbol boxy before the textarea and locate appropriately
			$j( $j(this.textArea).prev().find('a.initialiseSymbolBox').parent() ).append(content).find('div.symbolBoxDiv').css({ position: 'absolute', top: '-1px', right: '18px'});
			// add click events to all table cells
			$j( $j(this.textArea).prev().find('div.symbolBoxDiv table tbody td') ).click(function(){ self.addSymbol(self, this); return false; });
			// add click event to the first div that will close the symbol box
			$j( $j(this.textArea).prev().find('div.symbolBoxDiv div:first') ).click(function(){ self.closePlugin($j(this).parent()); return false; });
        },
		
		/**
		 * this method adds a symbol to the textarea
		 */
		addSymbol : function(self, tableCell)
		{
			// add the selected symbol to the textarea
			$j( $j(this.textArea) ).val($j( $j(this.textArea) ).val() + $j(tableCell).text().replace('-', ''));
			// focus on text area
			$j( $j(this.textArea) ).focus();
			// call method to remove plugin
			self.closePlugin($j(tableCell).closest('div'));
/**	
 * IE specific code not working for IE 11 so removed this and always use non-IE code
 * 
			// Internet Explorer being used?
			if ($j.browser.msie)
			{
				// copy and paste symbol
				$j(tableCell).clipBoard(null, $j(tableCell).text().replace('-', ''), true, $j( $j(this.textArea) ).attr('id'));
			}
			else
			{
				// add the selected symbol to the textarea
				$j( $j(this.textArea) ).val($j( $j(this.textArea) ).val() + $j(tableCell).text().replace('-', ''));
				// focus on text area
				$j( $j(this.textArea) ).focus();
				// call method to remove plugin
				self.closePlugin($j(tableCell).closest('div'));
			}
*/	
		},
		
		/**
		 * this function removes the symbol plugin from the page
		 * 
		 * @param {Object} div div to be removed from page
		 */				
		closePlugin : function(div)
		{
			// remove the symbol selector from the page
			$j( $j(div) ).remove();
		}
        
    });
})(jQuery);
												