/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	SymbolJQPlugin.js
*	DESCRIPTION		:	This plugin creates a link above the selected textarea which creates a table of symbols that
*					: 	can be selected and added to the textarea.
*
*	DEPENDENCIES	:   script/thirdparty/jQuery/jquery.js
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	09/03/2008 - SH - Created File
*					:	30/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

(function( $j )
{
	    
    $j.fn.symbolJQPlugin = function( options )
    {
        var options = $j.extend({/* options can be added here if neccessary */}, options);

        return this.each(function()
        {
            symbolJQPlugin(this, options);
        });
    };

    function symbolJQPlugin( element, options )
    {
        return this instanceof symbolJQPlugin
            ? this.init(element, options)
            : new symbolJQPlugin(element, options);
    }

    $j.extend(symbolJQPlugin.prototype,
    {
        original 		: null,
        options  		: {},

        element  		: null,
        textArea   		: null,
		offSet	 		: [],
		textAreaWidth	: null,
		symbolBoxHeight	: null,

		/**
		 * plugin initialisation function
		 * 
		 * @param {Object} element is the current plugin div to manipulate
		 * @param {Object} options are options for the current plugin
		 */
        init : function( element, options )
        {			
			// initialise textarea
            this.textArea = element;
			// initialise options
            this.options = options || {};			
			// set this textarea object
			var self = this;
			// get the width of the current textarea on page
			this.textAreaWidth = $j( $j(this.textArea) ).width();
			// draw div and link to open symbol box
			this.drawSymbolLink(self);
        },
		
		/**
		 * this method creates content for the link to open the symbol box and appends it before the textarea
		 * 
		 * @param {Object} this implementation of plugin
		 */
		drawSymbolLink : function(self)
		{
			// div and link to open symbol box content
			var content = 	'<div class="float-left text-right" style=" width: ' + this.textAreaWidth + 'px; ">' +
								'<a href="#">' +
									'<img src="img/icons/symbol_add.png" id="addSymbolLink" width="16" height="16" alt="' + i18n.t("core.utilities:addSymbol", "Add Symbol") + '" title="' + i18n.t("core.utilities:addSymbol", "Add Symbol") + '" />' +
								'</a>' +
							'</div>';
			// add content before the textarea
			$j( $j(this.textArea) ).before(content);
			// add click event to draw symbol box on demand
			$j( $j(this.textArea).prev().find('a') ).click(function() { self.drawSymbolBox(self); return false; });
		},
		
		/**
		 * method to draw symbol box
		 * 
		 * @param {Object} this implementation of plugin
		 */
        drawSymbolBox : function(self)
        {
			// get the location of current textarea on page (top, left)
			this.offSet = $j( $j(this.textArea) ).offset();
			// content for the symbol box that is created above the textarea
			var content = loadScript.masterSymbolBoxContent();
			// append the symbol boxy before the textarea
			$j( $j(this.textArea) ).after(content);
			// get the height of the div we just added
			this.symbolBoxHeight = $j( $j(this.textArea).next() ).height();
			// locate the symbol box above textarea
			$j( $j(this.textArea).next() ).css({ position: 'absolute', top: (this.offSet.top - this.symbolBoxHeight), left: (this.offSet.left + (this.textAreaWidth - 116))});
			// add click events to all table cells
			$j( $j(this.textArea).next().find('table tbody td') ).click(function(){ self.addSymbol(self, this); return false; });
			// add click event to the first div that will close the symbol box
			$j( $j(this.textArea).next().find('div:first') ).click(function(){ self.closePlugin($j(this).parent()); return false; });
        },
		
		/**
		 * this method adds a symbol to the textarea
		 */
		addSymbol : function(self, tableCell)
		{
			// add the selected symbol to the textarea
			$j( $j(this.textArea) ).val($j( $j(this.textArea) ).val() + $j(tableCell).text().replace('-', ''));
			// call method to remove plugin
			self.closePlugin($j(tableCell).closest('div'));
/**	
 * IE specific code not working for IE 11 so removed this and always use non-IE code
 * 
			// Internet Explorer being used?
			if ($j.browser.msie)
			{
				// copy and paste symbol
				$j(tableCell).clipBoard(null, $j(tableCell).text().replace('-', ''), true, $j( $j(this.textArea) ).attr('id'));
			}
			else
			{
				// add the selected symbol to the textarea
				$j( $j(this.textArea) ).val($j( $j(this.textArea) ).val() + $j(tableCell).text().replace('-', ''));
				// call method to remove plugin
				self.closePlugin($j(tableCell).closest('div'));
			}
*/	
	
		},
		
		/**
		 * this function removes the symbol plugin from the page
		 *
		 * @param {Object} div div to be removed from page
		 */				
		closePlugin : function(div)
		{
			// remove the symbol selector from the page
			$j( $j(div) ).remove();
		}
        
    });
})(jQuery);