/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	Menu.js
*	DESCRIPTION		:	Javascript to control showing / hiding of all menus.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	Commenting of code.
*	KNOWN ISSUES	:	-
*	HISTORY			:	26/10/2006 - SH - Created File
* 					:	11/10/2007 - SH - JQuery function and comment
*
****************************************************************************************************************************************/

/**					
*	
* 	Switch the focus of the specific menu given in the parameters.
*
* 	@param  menuElements - this variable holds an array of id objects, one for the navigation link (anchor) and one for the content area the link refers to (block).
*	@param	focusItem - the id of the element being put into focus.
*	@param 	focusInput - boolean value, true/false whether focus will be put onto the first input field of type text in the block to be selected
*						
*/
function switchMenuFocus(menuElements, focusItem, focusInput)
{
	// loop through the array of objects
	$j.each(menuElements, function(i)
	{
		// if this is the anchor/block being requested then display
		if(menuElements[i].block == focusItem)
		{
			// change the class name of the anchor being selected to 'selected'
			$j('#' + menuElements[i].anchor).removeClass().addClass('selected');
			
			// change the class name of the block being selected to visible
			$j('#' + menuElements[i].block).removeClass('hid').addClass('vis');
			
			// focusInput is true
			if (focusInput)
			{
				$j('#' + menuElements[i].block + ' input[type="text"]:first').focus();
			}
			// apply focus to the current anchor so the cursor is not
			// left hanging in the middle of a page
			else
			{
				$j('#' + menuElements[i].anchor).focus();
			}
		}
		// hide all other block elements and removed class names from the anchors
		else
		{		
			// change the class name of all other anchor elements to be empty
			$j('#' + menuElements[i].anchor).removeClass('selected');				
			// change the class name of all other block elements to hidden
			$j('#' + menuElements[i].block).removeClass('vis').addClass('hid');	
		}
	});
}
