/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	DOMUtils.js
*	DESCRIPTION		:	File of regularly used DOM Utilities for CWMS.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	Commenting of code.
*	KNOWN ISSUES	:	-
*	HISTORY			:	26/10/2006 - SH - Created File
*
****************************************************************************************************************************************/


function insertAfter(newElement, targetElement)
{
	var parent = targetElement.parentNode;
	if(parent.lastChild == targetElement)
	{
		parent.appendChild(newElement);
	}
	else
	{
		parent.insertBefore(newElement, targetElement.nextSibling);
	}
}

function removeChildren(parent)
{
	while(parent.hasChildNodes())
	{
		parent.removeChild(parent.childNodes[0]);
	}
}

function prepareArray(fieldSize)
{
	var list = new Array();
	for(var i = 1; i <= fieldSize; i++)
	{
		list.push(i);
	}
	return list;
}

/**
* Creates a select list populated with data from an array of arrays. 
* valuePos is the index in the 2nd array where the option value can be found
* namePos is the index in the 2nd array where the option display value can be found
**/
function createSelectFrom2DArray(dArray, valuePos, namePos, name, id, defvalue)
{
	var input = document.createElement('select');
	input.id = id;
	input.name = name;
	for(var i = 0; i < dArray.length; i++)
	{
		var optionItem = document.createElement('option');			 
		optionItem.value = dArray[i][valuePos];
        optionItem.appendChild(document.createTextNode(dArray[i][namePos]));
        input.appendChild(optionItem);
	}
	if(defvalue)
	{
		input.value = defvalue;
	}
	return input;
}

function deleteElement(id)
{
	if(elementExists(id))
	{
		try
		{
			var deleteElement = document.getElementById(id);
			deleteElement.parentNode.removeChild(deleteElement);
		}
		catch(e){}
	}
}

/**
* Tests if the element with the given ID acutally exists.
*/
function elementExists(elementId)
{	
	var exists = false;
	if(elementId != null && elementId != '')
	{
		try
		{
			exists = elementObjectExists(document.getElementById(elementId));
		}
		catch(e){exists = false;}
	}
	return exists;	
}

function elementObjectExists(elementObj)
{
	var exists = false;
	try
	{
		if(elementObj != null)
		{
			exists = true;
		}
	}
	catch(e){exists = false;}
	return exists;	
}

function getSemiRandomInt()
{
	return Math.floor(Math.random()*100000001);
}
