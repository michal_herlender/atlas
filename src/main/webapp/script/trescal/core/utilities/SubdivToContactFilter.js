/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	SubdivToContactFilter.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	22/05/2009 - SH - Created File
*					: 	30/10/2015 - TProvost - Manage i18n
*					:	2018-12-11 - GB - added support for address selector using controller / jsp fragment
*					:	2018-12-14 - GB - added support for contact selector using controller / jsp fragment
****************************************************************************************************************************************/

// load javascript for boxy pop up if not already
loadScript.loadScriptsDynamic(new Array(	'dwr/interface/addressservice.js',
											'dwr/interface/contactservice.js'), true);

/**
 * this method accepts a subdiv id and gets all active addresses and contacts for that subdivision before adding
 * them to the address select and contact select objects passed.
 * 
 * @param {Integer} subdivid the id of the subdivision to get addresses for
 * @param {Object} selectAddr the address select we want to add address results to
 * @param {Object} selectCont the contact select we want to add contact results to
 * @param {Boolean} allOption indicates whether an all option should be included
 * @param {Integer/String} allOptionValue the value required when all option selected (''/0)
 */
function getActiveSubdivAddrsAndConts(subdivid, selectAddr, selectCont, allOption, allOptionValue)
{	
	if (subdivid != '')
	{
		// call dwr service to retrieve active subdiv addresses
		addressservice.getAllActiveSubdivAddressesHQL(subdivid, "",
		{
			callback:function(addrs)
			{
				// addresses returned?
				if (addrs.length > 0)
				{
					// new address options variable
					var content = '';
					// all option?
					if (allOption)
					{
						content += '<option value="' + allOptionValue + '">' + i18n.t("core.utilities:searchAllAddresses", "Search All Addresses") + '</option>';
					}
					// add all new address options to variable
					$j.each(addrs, function(i, a)
					{
						content += '<option value="' + a.addrid + '" title="' + a.addr1 + ',' + a.addr2 + ',' + a.town + ',' + a.county + '">' + a.addr1 + ', ' + a.addr2 + ', ' + a.town + ', ' + a.county + '</option>';
					});
					// empty select and add new options then trigger the onchange event to cascade contact search for selected address
					$j(selectAddr).empty().append(content).trigger('onchange');
				}
				else
				{
					// empty select and add no address message
					$j(selectAddr).empty().append('<option value="' + allOptionValue + '">' + i18n.t("core.utilities:searchAllAddresses", "Search All Addresses") + '</option>');
				}
			}
		});
		// call dwr service to retrieve active subdiv contacts
		contactservice.getAllActiveSubdivContactsHQL(subdivid,
		{
			callback:function(cons)
			{
				// contacts returned?
				if (cons.length > 0)
				{
					// new contact options variable
					var content = '';
					// all option
					if (allOption)
					{
						content += '<option value="' + allOptionValue + '">' + i18n.t("core.utilities:searchAllContacts", "Search All Contacts") + '</option>';
					}
					// add all new contact options to variable
					$j.each(cons, function(i, c)
					{
						content += '<option value="' + c.personid + '">' + c.name + '</option>';
					});
					// empty select and add new options
					$j(selectCont).empty().append(content);
				}
				else
				{
					// empty select and add no contact message
					$j(selectCont).empty().append('<option value="' + allOptionValue + '">' + i18n.t("core.utilities:searchAllContacts", "Search All Contacts") + '</option>');
				}
			}
		});		
	}
	else
	{
		// all address variable
		var addrContent = '<option value="">' + i18n.t("core.utilities:selectsubdivfirst", "Select Subdivision First") + '</option>';
		// empty select and add new option
		$j(selectAddr).empty().append(addrContent);	
		// all address variable
		var contContent = '<option value="">' + i18n.t("core.utilities:selectsubdivfirst", "Select Subdivision First") + '</option>';
		// empty select and add new option
		$j(selectCont).empty().append(contContent);	
	}	
}

/**
 * this method accepts a subdiv id and gets all active addresses for that subdivision as a replacement
 * for the address select passed.  This uses a JSP fragment / controller instead of DWR.
 * 
 * @param {Integer} subdivid the id of the subdivision to get addresses for
 * @param {String} selectName the html name (and id) of the select we want to add address results to
 */
function getActiveSubdivAddrs(subdivId, selectName) {

	$j.ajax({
		url: "addressselect.htm",
		data: {
			subdivId: subdivId,
			selectName: selectName
		},
		async: true
	}).done(function(result) {
		$j("#"+selectName).html(result);
	});
}

/**
 * this method accepts a company id and gets all active subdivisions for that company as a replacement
 * for the subdivision select passed.  This uses a JSP fragment / controller instead of DWR.
 * 
 * @param {Integer} companyId the id of the company to get subdivisions for
 * @param {String} selectName the html name (and id) of the select we want to add subdivision results to
 */
function getActiveCompanySubdivs(companyId, selectName) {

	$j.ajax({
		url: "subdivselect.htm",
		data: {
			companyId: companyId,
			selectName: selectName
		},
		async: true
	}).done(function(result) {
		$j("#"+selectName).html(result);
	});
}