/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	TooltipDWRInfo.js
*	DESCRIPTION		:	This javascript file is responsible for loading dwr services and retrieving data for
* 					: 	creating tooltip pop up boxes. 
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	Created by SH 30/08/2007
*					:	02/11/2015 - TProvost - Manage i18n
*					:	20/10/2016 - TProvost - Manage i18n
*                   :   2020-11-18 : Galen Beck - removed 'instrumentmodel' as a supported type, broken and replaced in the 3 places used
****************************************************************************************************************************************/

/**
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var Elements1 = new Array();
var Elements2 = new Array();
var Elements3 = new Array();
var Elements4 = new Array();
var Elements5 = new Array();

/**
 * this function checks the type of tooltip requested and loads the correct javascript service then calls a method
 * to retrieve desired details
 * 
 * @param {String} request type of tooltip pop-up content required
 * @param {Integer} id id of the entity for which tooltip content is required
 * @param {Integer} value value passed if converting a currency
 */
function filterTooltipRequest(request, id, value)
{
	// Empty existing menu navigation arrays
	Elements1.length = 0;
	Elements2.length = 0;
	Elements3.length = 0;
	Elements4.length = 0;
	Elements5.length = 0;
	
	// load the menu navigation javascript file if not already
	loadScript.loadScriptsDynamic(new Array('script/trescal/core/utilities/Menu.js'), true);
	// check which ajax call we should be making (i.e. contact, company)
	switch (request)
	{
		case 'contact':			// check that these files have not already been loaded
								if (typeof tb_show == 'undefined')
								{
									// load thickbox stylesheet using the dom for email link
									loadScript.loadStyle('styles/jQuery/jquery-thickbox.css', 'screen');
									// load the thickbox javascript file for email link
									loadScript.loadScriptsDynamic(new Array('script/thirdparty/jQuery/jquery.thickbox.js'), true);
								}
								getContactInfo(id);
								$j('#JT').show();
								break;
						
		case 'subdiv':			getSubdivInfo(id);
								$j('#JT').show();
								break;
								
		case 'company':			// load the companyservice javascript file if not already
								getCompanyInfo(id);
								$j('#JT').show();								
								break;
						
		case 'job':				getJobInfo(id);
								$j('#JT').show();
								break;
						
		case 'instrument':		getInstrumentInfo(id);
								$j('#JT').show();
								break;
							
		case 'jobitem':			getJobItemInfo(id);
								$j('#JT').show();
																
								break;
								
		case 'jobcostingitem':	// load the jobcostingitemservice javascript file if not already
								loadScript.loadScriptsDynamic(new Array('dwr/interface/jobcostingitemservice.js'), true,
								{
									callback: function()
									{
										getJobCostingItemInfo(id);
										$j('#JT').show();
									}
								});
																
								break;
								
		case 'jobcosting':		// load the jobcostingservice javascript file if not already
								loadScript.loadScriptsDynamic(new Array('dwr/interface/jobcostingservice.js'), true,
								{
									callback: function()
									{
										getJobCostingInfo(id);
										$j('#JT').show();
									}
								});
																
								break;
							
		case 'currency':		// load the gepservice javascript file if not already
								loadScript.loadScriptsDynamic(new Array('dwr/interface/gepservice.js'), true,
								{
									callback: function()
									{
										getCurrencyInfo(id, value);
										$j('#JT').show();
									}
								});
															
								break;
								
		case 'qialtcosts':		// more than one id passed?
								if (id.indexOf(',') != -1)
								{
									// split id's into array
									var idArray = id.split(',');
								}
								// load the quotationitemservice javascript file if not already
								loadScript.loadScriptsDynamic(new Array('dwr/interface/quotationitemservice.js'), true,
								{
									callback: function()
									{
										getQIAlternativeCostsInfo(idArray[0], idArray[1], idArray[2], idArray[3], idArray[4]);
										$j('#JT').show();
									}
								});
								
								break;
								
		case 'quotation':		// load the quotation service javascript file if not already
								loadScript.loadScriptsDynamic(new Array('dwr/interface/quotationservice.js'), true,
								{
									callback: function()
									{
										getQuotationInfo(id);
										$j('#JT').show();
									}
								});
								
								break;
								
		case 'tpquote':			// load the tpquotationservice javascript file if not already
								loadScript.loadScriptsDynamic(new Array('dwr/interface/tpquotationservice.js'), true,
								{
									callback: function()
									{
										getTPQuotationInfo(id);
										$j('#JT').show();
									}
								});
																	
								break;
								
		case 'addrjobs':		// load the jobservice javascript file if not already
								loadScript.loadScriptsDynamic(new Array('dwr/interface/jobservice.js'), true,
								{
									callback: function()
									{
										getAddrJobsInfo(id);
										$j('#JT').show();
									}
								});
								
								break;
								
		case 'jobitemgroup':	// load the jobitemgroupservice javascript file if not already
								loadScript.loadScriptsDynamic(new Array('dwr/interface/jobitemgroupservice.js'), true,
								{
									callback: function()
									{
										getJobItemGroupInfo(id);
										$j('#JT').show();
									}
								});
								
								break;

	}
}

/**
 * this is the callback method for function getStatusInfo()
 * 
 * @param {Object} contacts is an array of objects containing all contacts name and status
 */
function createStatusContent(contacts)
{	
	var content = 	'<table class="default2" id="userStatus" summary="This table displays all currently logged in users and their status">' +
						'<thead>' +
							'<tr>' +
								'<td colspan="3">' + i18n.t("core.utilities:tooltip.loggedInUsers", {varSessionTimeout : sessionTimeout, varNbUsers : contacts.length, defaultValue : "Logged In Users Over Last " + sessionTimeout + " Minutes (" + contacts.length + ")"}) + '</td>' +
							'</tr>' +
							'<tr>' +
								'<th scope="col" class="suser">' + i18n.t("core.utilities:tooltip.user", "User") + '</th>' +
								'<th scope="col" class="sstatus">' + i18n.t("status", "Status") + '</th>' +
								'<th scope="col" class="semail">' + i18n.t("core.utilities:tooltip.email", "Email") + '</th>' +
							'</tr>' +
						'</thead>' +
						'<tbody>';
	
	$j.each(contacts, function(i){
		content = content + '<tr>' +
								'<td>' + contacts[i].name + '</td>';
								if (contacts[i].status != null)
								{
									content = content + '<td>' + contacts[i].status + '</td>';
								}
								else
								{
									content = content + '<td>&nbsp;</td>';
								}
		content = content + 	'<td><a href="#" onclick=" loadScript.createOverlay(\'external\', \'' + i18n.t("core.utilities:tooltip.createEmail", "Create Email") + '\', \'createemail.htm?personid=' + contacts[i].personid + '\', null, null, 64, 860, null); return false; " class="mainlink" title="">' + contacts[i].email + '</a></td>' +
							'</tr>';
	});
	
	content = content + '</tbody>' +
						'</table>';
	
	$j('#JT_copy').append(content);
	
	// calls jquery.thickbox.js function to initiate thickbox anchor	
	tb_init('table.default2 a.thickbox');
	
	/**
	 * set hover functionality of pop up.  ON mouse over do nothing and on mouse out close the pop up.
	 */ 
	$j('#JT').hover(function()	{
									mouseOverSignal = true;
								},
					function()	{
									$j('#JT').remove();
									mouseOverSignal = false; 
									return false;
								});														
}

/**
 * this method retrieves contact information for the personid provided.
 * this information is then appended to the tooltip pop up.
 * 
 * @param {Integer} personid is the id of the person for whom information is required
 */
function getContactInfo(personid)
{	
	// add submenu elements to array
	Elements1.push(	{ anchor: 'ttc_contact-link', block: 'ttc_contact-tab' });
					
	// add submenu elements to array
	Elements2.push(	{ anchor: 'ttc_summary-link', block: 'ttc_summary-tab' } );

	// call contact dwr function using the personid
	$j.ajax({
		url: "contact.json",
		dataType: 'json',
		data: {personid: personid},
		async: false
	}).done(function(contact) {
		createContactContent(contact);
	});
	
	$j('div').remove('.JT_loader');
}

/**
 * this is the callback method for function getContactInfo()
 * 
 * @param {Object} contact is an ContactDTO object containing all contacts information
 */
function createContactContent(contact)
{	
	var contActive = i18n.t("core.utilities:active", "Active");
	var activeStyle = '';
	
	// contact is currently active
	if (contact.active != true)
	{
		contActive = i18n.t("core.utilities:inactive", "Active");
		activeStyle = 'class="inactiveTT"';
	}

	var content = 	// main div
					'<div id="contactTT" ' + activeStyle + '>' +	
						// main navigation bar
						'<div class="slatenav-main">' +
							'<ul>' +
								'<li><a href="#" id="ttc_contact-link" onmouseover=" switchMenuFocus(Elements1, \'ttc_contact-tab\', false); return false; " class="selected">' + i18n.t("contact", "Contact") + '</a></li>' +
							'</ul>' +
						'</div>' +
						'<div id="ttc_contact-tab" class="vis clearL">' +													
							// contact sub navigation bar
							'<div class="slatenav-sub">' +
								'<ul>' +
									'<li><a href="#" onmouseover=" switchMenuFocus(Elements2, \'ttc_summary-tab\', false); return false; " id="ttc_summary-link" class="selected">' + i18n.t("core.utilities:tooltip.summary", "Summary") + '</a></li>' +
								'</ul>' +
							'</div>' +
							// contact summary tab
							'<div id="ttc_summary-tab">' +
								'<fieldset class="newtooltip">' +
									'<ol>' +
										'<li>' +
											'<label>' + i18n.t("nameLabel", "Name:") + '</label>' +
											'<span>' + contact.name + '</span>' +
										'</li>';
										// if a default address has been set for this contact then add to the list.
										if (contact.defAddress)
										{
											content +=	'<li>' +
															'<label>' + i18n.t("core.utilities:tooltip.homeAddressLabel", "Home Address:") + '</label>' +
															'<div class="float-left">' +
																contact.addr1 + '<br/>';
																if (contact.addr2 != '')
																{
																	content += 	contact.addr2 + '<br />';
																}
																if (contact.addr3 != '')
																{
																	content += 	contact.addr3 + '<br />';
																}
													content += 	contact.town + '<br />';
																if (contact.county != '')
																{
																	content += 	contact.county + '<br />';
																}
																if (contact.postcode != '')
																{
																	content += 	contact.postcode + '';
																}
													content +=	'</div>' +
															'<div class="clear"></div>' +
														'</li>';
										}
										// contact position present?
										if (contact.position != '')
										{
											content += 	'<li>' +
															'<label>' + i18n.t("core.utilities:positionLabel", "Position:") + '</label>' +
															'<span>' + contact.position + '</span>' +
														'</li>';
										}
							content += 	'<li>' +
											'<label>' + i18n.t("telephoneLabel", "Telephone:") + '</label>' +
											'<span>' + contact.telephone + '</span>' +
										'</li>';
										// mobile number present?
										if (contact.mobile != null && contact.mobile != '' && contact.mobile != 'undefined')
										{
											content += 	'<li>' +
															'<label>' + i18n.t("core.utilities:mobileLabel", "Mobile:") + '</label>' +
															'<span>' + contact.mobile + '</span>' +
														'</li>';
										}
							content += 	'<li>' +
											'<label>' + i18n.t("core.utilities:faxLabel", "Fax:") + '</label>' +
											'<span>' + contact.fax + '</span>' +
										'</li>' +
										'<li>' +
											'<label>' + i18n.t("emailLabel", "Email:") + '</label>' +
											'<span><a href="mailto:' + contact.email + '" class="thickbox mainlink" title="' + i18n.t("core.utilities:tooltip.sendEmailTo", {varContactName : contact.name, defaultValue : "Send email to " + contact.name}) + '">' + contact.email + '</a></span>' +
										'</li>' +
										'<li>' +
											'<label>' + i18n.t("core.utilities:tooltip.activeLabel", "Active:") + '</label>' +
											'<span>' + contActive + '</span>' +
										'</li>' +
										'<li>' +
											'<label>' + i18n.t("core.utilities:tooltip.contactPreferenceLabel", "Contact Preference:") + '</label>' +
											'<span>' + contact.preference + '</span>' +
										'</li>';
				content = content + '</ol>' +
								'</fieldset>' + 
							'</div>' +
							// end of summary tab
						'</div>' +
						// end of contact tab
					'</div>';
					// end of main div
	
	$j('#JT_copy').append(content);
	
	// calls jquery.thickbox.js function to initiate thickbox anchor	
	tb_init('fieldset.tooltip a.thickbox');
	
	/**
	 * set hover functionality of pop up.  ON mouse over do nothing and on mouse out close the pop up.
	 */ 
	$j('#JT').hover(function()	{
									mouseOverSignal = true;
								},
					function()	{
									$j('#JT').remove();
									mouseOverSignal = false; 
									return false;
								});														
}

/**
 * this method retrieves subdiv information for the subdivid provided.
 * this information is then appended to the tooltip pop up.
 * 
 * @param {Integer} subdivid is the id of the subdivision for whom information is required
 */
function getSubdivInfo(subdivid)
{
	// add submenu elements to array
	Elements1.push(	{ anchor: 'tts_subdiv-link', block: 'tts_subdiv-tab' });			
	// add submenu elements to array
	Elements2.push(	{ anchor: 'tts_summary-link', block: 'tts_summary-tab' } );
	$j.ajax({
		url: "subdiv.json",
		dataType: 'json',
		data: {subdivid: subdivid},
		async: false
	}).done(function(subdiv) {
		createSubdivContent(subdiv);
	});
	$j('div').remove('.JT_loader');
}

/**
 * this is the callback method for function getSubdivInfo()
 * 
 * @param {Object} subdiv is an object containing all subdivisions information
 */
function createSubdivContent(subdiv)
{
	// variables for optional content
	var defaultAddress = '-- Not Set --';
	var defaultContact = '-- Not Set --';
	var subActive = i18n.t("core.utilities:active", "Active");
	var activeStyle = '';
	// subdiv is currently active
	if (subdiv.active != true)
	{
		subActive = i18n.t("core.utilities:inactive", "Inactive");
		activeStyle = 'class="inactiveTT"';
	}
	var content = 	// main div
					'<div id="subdivTT" ' + activeStyle + '>' +	
						// main navigation bar
						'<div class="slatenav-main">' +
							'<ul>' +
								'<li><a href="#" id="tts_subdiv-link" onclick="event.preventDefault();" onmouseover="event.preventDefault(); switchMenuFocus(Elements1, \'tts_subdiv-tab\', false);" class="selected">' + i18n.t("subdivision", "Subdivision") + '</a></li>' +
							'</ul>' +
						'</div>' +
						'<div id="tts_subdiv-tab" class="vis clearL">' +													
							// subdivision sub navigation bar
							'<div class="slatenav-sub">' +
								'<ul>' +
									'<li><a href="#" onclick="event.preventDefault();" onmouseover="event.preventDefault(); switchMenuFocus(Elements2, \'tts_summary-tab\', false);" id="tts_summary-link" class="selected">' + i18n.t("core.utilities:tooltip.summary", "Summary") + '</a></li>' +
								'</ul>' +
							'</div>' +
							// subdivision summary tab
							'<div id="tts_summary-tab">' +
								'<fieldset class="newtooltip">' +
									'<ol>' +
										'<li>' +
											'<label>' + i18n.t("companyLabel", "Company:") + '</label>' +
											'<span>' + subdiv.company + '</span>' +
										'</li>' +
										'<li>' +
											'<label>' + i18n.t("core.utilities:tooltip.activeLabel", "Active:") + '</label>' +
											'<span>' + subActive + '</span>' +
										'</li>' +
										'<li>' +
											'<label>' + i18n.t("core.utilities:tooltip.defaultAddressLabel", "Default Address:") + '</label>' +
											'<span>' + subdiv.defaultAddress + '</span>' +
										'</li>' +
										'<li>' +
											'<label>' + i18n.t("core.utilities:tooltip.defaultContactLabel", "Default Contact:") + '</label>' +
											'<span>' + subdiv.defaultContact + '</span>' +
										'</li>' +
									'</ol>' +
								'</fieldset>' +
							'</div>' +
							// end of summary tab
						'</div>' +
						// end of subdiv tab
					'</div>';
					// end of main div
							
	
	$j('#JT_copy').append(content);
												
	/**
	 * set hover functionality of pop up.  ON mouse over do nothing and on mouse out close the pop up.
	 */ 
	$j('#JT').hover(function()	{
									mouseOverSignal = true;
								},
					function()	{
									$j('#JT').remove();
									mouseOverSignal = false; 
									return false;
								});											
}

/**
 * this method retrieves company information for the coid provided.
 * this information is then appended to the tooltip pop up.
 * 
 * @param {Integer} coid is the id of the company for whom information is required
 */
function getCompanyInfo(coid)
{
	// add submenu elements to array
	Elements1.push(	{ anchor: 'ttc_company-link', block: 'ttc_company-tab' });
					
	// add submenu elements to array
	Elements2.push(	{ anchor: 'ttc_summary-link', block: 'ttc_summary-tab' },
					{ anchor: 'ttc_subdivs-link', block: 'ttc_subdivs-tab' } );
	// call company dwr function using the coid
	$j.ajax({
		url: "company.json",
		dataType: 'json',
		data: {coid: coid},
		async: false
	}).done(function(company) {
		createCompanyContent(company);
	});
		
	$j('div').remove('.JT_loader');
}

/**
 * this is the callback method for function getCompanyInfo()
 * 
 * @param {Object} company is an CompanyDTO object containing all company information
 */
function createCompanyContent(company)
{
	// set variable values for active and onstop parameters
	var compActive = 'Active';
	var compOnstop = 'Trusted';
	var activeStyle = '';
	// company is currently inactive
	if (company.active != true)
	{
		compActive = 'Inactive';
		activeStyle = 'class="inactiveTT"';
	}
	// company is currently onstop
	if (company.onStop == true)
	{
		compOnstop = 'On-stop';
	}
	
	var content = 	// main div
					'<div id="companyTT" ' + activeStyle + '>' +	
						// main navigation bar
						'<div class="slatenav-main">' +
							'<ul>' +
								'<li><a href="" id="ttj_job-link" onclick="event.preventDefault();" onmouseover=" switchMenuFocus(Elements1, \'ttc_company-tab\', false); return false; " class="selected">' + i18n.t("company", "Company") + '</a></li>' +
							'</ul>' +
						'</div>' +
						'<div id="ttc_company-tab" class="vis clearL">' +													
							// company sub navigation bar
							'<div class="slatenav-sub">' +
								'<ul>' +
									'<li><a href="" onclick="event.preventDefault();" onmouseover=" switchMenuFocus(Elements2, \'ttc_summary-tab\', false); return false; " id="ttc_summary-link" class="selected">' + i18n.t("core.utilities:tooltip.summary", "Summary") + '</a></li>' +
									'<li><a href="" onclick="event.preventDefault();" onmouseover=" switchMenuFocus(Elements2, \'ttc_subdivs-tab\', false); return false; " id="ttc_subdivs-link">' + i18n.t("subdivisions", "Subdivisions") + '</a></li>' +
								'</ul>' +
							'</div>' +
							// company summary tab
							'<div id="ttc_summary-tab">' +
								'<fieldset class="newtooltip">' +
									'<ol>' +
										'<li>' +
											'<label>' + i18n.t("nameLabel", "Name:") + '</label>' +
											'<span>' + company.coname + '</span>' +
										'</li>' +
										'<li>' +
											'<label>' + i18n.t("core.utilities:tooltip.instrumentsLabel", "Instruments:") + '</label>' +
											'<span><a href="viewcompanyinstrument.htm?coid=' + company.coid + '")" target="_blank" class="mainlink">' + i18n.t("core.utilities:tooltip.instruments", "Instruments") + '</a></span>' +
										'<li>' +
											'<label>' + i18n.t("core.utilities:tooltip.coroleLabel", "Corole:") + '</label>' +
											'<span>' + company.companyRole + '</span>' +
										'</li>' +
										'<li>' +
											'<label>' + i18n.t("core.utilities:tooltip.activeLabel", "Active:") + '</label>' +
											'<span>' + compActive + '</span>' +
										'</li>' +
										'<li>' +
											'<label>' + i18n.t("core.utilities:tooltip.onStopLabel", "On-stop:") + '</label>' +
											'<span>' + compOnstop + '</span>' +
										'</li>' +
									'</ol>' +
								'</fieldset>' +
							'</div>' +
							// end of company summary tab
							'<div id="ttc_subdivs-tab" class="hid">' +
								'<fieldset class="newtooltip">' +
									'<ol>';

	// loop through all subdivisions for this company
		$j.each(company.subDivs, function(i){
			var def = "";
			if (company.subDivs[i].key == company.defSubdivId)
			{
				var def = '<strong>(' + i18n.t("default", "Default") + ')</strong>';
			}
			
			// this is not the first subdivision so display default text only
			if (i > 0)
			{
				content = content + '<li>' +
										'<label>&nbsp;</label>' +
										'<span><a href="viewsub.htm?subdivid=' + company.subDivs[i].key + '" target="_blank" class="mainlink">' + company.subDivs[i].value + '</a> ' + def + '</span>' +
									'</li>';
			}
			// this is the first subdivision so display default text and label
			else
			{
				content = content + '<li>' +
										'<label>' + i18n.t("subdivisionsLabel", "Subdivisions:") + '</label>' +
										'<span><a href="viewsub.htm?subdivid=' + company.subDivs[i].key + '" target="_blank" class="mainlink">' + company.subDivs[i].value + '</a> ' + def + '</span>' +
									'</li>';
			}
		});
	
				content = content + '</ol>' +
								'</fieldset>' +
							'</div>' +
							// end of subdiv tab
						'</div>' +
						// end of company tab
					'</div>';
					// end of main div
	
	$j('#JT_copy').append(content);
	
	/**
	 * set hover functionality of pop up.  ON mouse over do nothing and on mouse out close the pop up.
	 */ 
	$j('#JT').hover(function()	{
									mouseOverSignal = true;
								},
					function()	{
									$j('#JT').remove();
									mouseOverSignal = false; 
									return false;
								});
}

/**
 * this method retrieves job information for the jobno provided.
 * this information is then appended to the tooltip pop up.
 * 
 * @param {Integer} jobid id of the job for which information is required
 */
function getJobInfo(jobid)
{	
	// add submenu elements to array
	Elements1.push(	{ anchor: 'ttj_job-link', block: 'ttj_job-tab' });
					
	// add submenu elements to array
	Elements2.push(	{ anchor: 'ttj_summary-link', block: 'ttj_summary-tab' },
					{ anchor: 'ttj_items-link', block: 'ttj_items-tab' });
	
	var tooltipUrl = "tooltipjob.htm?jobid=" + jobid;
	createContentFromURL(tooltipUrl);
}

/**
 * General function to create tooltip overlay from html content fragment obtained via URL. 
 * 
 * @param tooltipUrl
 * @returns
 */
function createContentFromURL(tooltipUrl)
{
	console.log("Getting content from "+tooltipUrl);
	
    var response = $j.ajax({
        type: "GET",
        contentType: "text/html",
        url: tooltipUrl
    });

    response.done(function(html){
    	$j('#JT_copy').append(html);

    	/**
    	 * set hover functionality of pop up.  ON mouse over do nothing and on mouse out close the pop up.
    	 */ 
    	$j('#JT').hover(function()	{
    									mouseOverSignal = true;
    								},
    					function()	{
    									$j('#JT').remove();
    									mouseOverSignal = false; 
    									return false;
    								});								
    	$j('div').remove('.JT_loader');
    });

    response.fail(function (jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
}


/**
 * this method retrieves instrument information for the plantid provided.
 * this information is then appended to the tooltip pop up.
 * 
 * @param {Integer} plantid id of the instrument for which information is required
 */
function getInstrumentInfo(plantid)
{	
	// add submenu elements to array
	Elements1.push( { anchor: 'tti_instrum-link', block: 'tti_instrum-tab' });
	// add submenu elements to array
	Elements2.push(	{ anchor: 'tti_summary-link', block: 'tti_summary-tab' },
					{ anchor: 'tti_thread-link', block: 'tti_thread-tab' },
					{ anchor: 'tti_jobs-link', block: 'tti_jobs-tab' },
					{ anchor: 'tti_certs-link', block: 'tti_certs-tab' },
					{ anchor: 'tti_notes-link', block: 'tti_notes-tab' });
	$j.ajax({
		url: "instrumenttooltip.json",
		data: {plantId: plantid},
		async: false
	}).done(function(tooltip) {
		$j('#JT_copy').append(tooltip);
		/**
		 * set hover functionality of pop up.  ON mouse over do nothing and on mouse out close the pop up.
		 */ 
		$j('#JT').hover(
			function() {
				mouseOverSignal = true;
			},
			function() {
				$j('#JT').remove();
				mouseOverSignal = false; 
				return false;
			});
	});
	// remove the loading image from jTip
	$j('div').remove('.JT_loader');
}

/**
 * this method retrieves instructions for the id provided.
 * this information is then appended to the tooltip pop up.
 * 
 * @param {Integer} id id of the job for which instructions should be retrieved
 */
function getInstructionInfo(id)
{	
	// call instruction dwr service method using id
	instructionservice.getAllInstructionsRelatedToJobDWR(id,
	{
		callback:function(instructions)
		{
		//	$j.prompt(DWRUtil.toDescriptiveString(instructions, 3));
			createInstructionContent(instructions);	
		}
	});
		
	$j('div').remove('.JT_loader');
}

/**
 * this method retrieves currency conversion for the entity id and value provided.
 * this information is then appended to the tooltip pop up.
 * 
 * @param {Integer} id id of the main entity that has an exchange rate set
 * 						(This is looked up and used to convert the currency amount)
 * @param {Integer} value the value of the currency to be converted.
 */
function getCurrencyInfo(id, value)
{	
	gepservice.getBaseCurrencyValue(id, value,
	{
		callback:function(result)
		{
			var currencytext = '';
			
			if(!result)
			{
				currencytext += '<div>' + i18n.t("core.utilities:tooltip.errorRetrievingCurrencyConv", "An error occurred retrieving the currency conversion") + '</div>';
			}	
			else
			{
				currencytext += '<div>' + i18n.t("core.utilities:tooltip.monetaryValue", {varResult : result, defaultValue : "Monetary value is " + result}) + '</div>';	
			}
			
			$j('#JT_copy').append(currencytext);
			
			/**
			 * set hover functionality of pop up.  ON mouse over do nothing and on mouse out close the pop up.
			 */ 
			$j('#JT').hover(function()	{
											mouseOverSignal = true;
										},
							function()	{
											$j('#JT').remove();
											mouseOverSignal = false; 
											return false;
										});
		}
	});
		
	$j('div').remove('.JT_loader');
}

/**
 * this method retrieves quote item alternative price information for the modelid, caltypeid and coid provided.
 * this information is then appended to the tooltip pop up.
 * 
 * @param {Integer} modelid id of the instrumentmodel for which information is required
 * @param {Integer} caltypeid id of the calibration type
 * @param {Integer} coid id of the company quotation is for
 */
function getQIAlternativeCostsInfo(modelid, caltypeid, coid, busid, excludequoteid)
{	
	// add submenu elements to array
	Elements1.push(	{ anchor: 'ttjc_jobitemcosts-link', block: 'ttjc_jobitemcosts-tab' });
										
	// add submenu elements to array
	Elements3.push(	{ anchor: 'ttjc_lqc-link', block: 'ttjc_lqc-tab'},
					{ anchor: 'ttjc_ulqc-link', block: 'ttjc_ulqc-tab'},
					{ anchor: 'ttjc_pl-link', block: 'ttjc_pl-tab'},
					{ anchor: 'ttjc_mjc-link', block: 'ttjc_mjc-tab'},
					{ anchor: 'ttjc_model-link', block: 'ttjc_model-tab'});

	// call instrument dwr function using the plantid
	quotationitemservice.getAjaxAlternativeCalibrationCosts(modelid, caltypeid, coid, busid, excludequoteid,
	{
		callback:function(results)
		{
			createJobCostingItemContent(results, true, false, true);	
		}
	});
		
	$j('div').remove('.JT_loader');
}

/**
 * this method retrieves job item information for the jobitemid provided.
 * this information is then appended to the tooltip pop up.
 * 
 * @param {Integer} jobitemid is the id of the job item for which information is required
 */
function getJobItemInfo(jobitemid)
{	
	// add submenu elements to array
	Elements1.push(	{ anchor: 'ttjc_jobitem-link', block: 'ttjc_jobitem-tab' });

	// add submenu elements to array
	Elements2.push(	{ anchor: 'ttjc_jisummary-link', block: 'ttjc_jisummary-tab'},
					{ anchor: 'ttjc_jiactions-link', block: 'ttjc_jiactions-tab'},
					{ anchor: 'ttjc_jicontrev-link', block: 'ttjc_jicontrev-tab'});

	createContentFromURL("tooltipjobitem.htm?jobitemid=" + jobitemid);
}

/**
 * this method retrieves job item information for the jobitemid provided.
 * this information is then appended to the tooltip pop up.
 * 
 * @param {Integer} jobitemid is the id of the job item for which information is required
 */
function getJobItemInfo_Deprecated(jobitemid)
{	
	// add submenu elements to array
	Elements1.push(	{ anchor: 'ttjc_jobitem-link', block: 'ttjc_jobitem-tab' },
					{ anchor: 'ttjc_job-link', block: 'ttjc_job-tab'});
					
	// add submenu elements to array
	Elements2.push(	{ anchor: 'ttjc_jisummary-link', block: 'ttjc_jisummary-tab'},
					{ anchor: 'ttjc_jifault-link', block: 'ttjc_jifault-tab'},
					{ anchor: 'ttjc_jicontrev-link', block: 'ttjc_jicontrev-tab'},
					{ anchor: 'ttjc_jicals-link', block: 'ttjc_jicals-tab'},
					{ anchor: 'ttjc_jiactions-link', block: 'ttjc_jiactions-tab'},
					{ anchor: 'ttjc_jirequire-link', block: 'ttjc_jirequire-tab'});
					
	// add submenu elements to array
	Elements3.push( { anchor: 'ttjc_jitems-link', block: 'ttjc_jitems-tab'});

	// call job item dwr function using the jobitemid
	jobitemservice.getDetailedJobItem(jobitemid,
	{
		callback:function(results)
		{
			createJobCostingItemContent(results, false, true, false);	
		}
	});
		
	$j('div').remove('.JT_loader');
}

/**
 * this method retrieves price list item information for the jobcostingitemid provided.
 * this information is then appended to the tooltip pop up.
 * 
 * @param {Integer} jobcostingitemid id of the job costing item for which information is required
 */
function getJobCostingItemInfo(jobcostingitemid)
{	
	// add submenu elements to array
	Elements1.push(	{ anchor: 'ttjc_jobitem-link', block: 'ttjc_jobitem-tab' },
					{ anchor: 'ttjc_jobitemcosts-link', block: 'ttjc_jobitemcosts-tab' });
					
	// add submenu elements to array
	Elements2.push(	{ anchor: 'ttjc_jisummary-link', block: 'ttjc_jisummary-tab'},
					{ anchor: 'ttjc_jifault-link', block: 'ttjc_jifault-tab'},
					{ anchor: 'ttjc_jicontrev-link', block: 'ttjc_jicontrev-tab'},
					{ anchor: 'ttjc_jicals-link', block: 'ttjc_jicals-tab'},
					{ anchor: 'ttjc_jiactions-link', block: 'ttjc_jiactions-tab'},
					{ anchor: 'ttjc_jirequire-link', block: 'ttjc_jirequire-tab'});
					
	// add submenu elements to array
	Elements3.push(	{ anchor: 'ttjc_lqc-link', block: 'ttjc_lqc-tab'},
					{ anchor: 'ttjc_ulqc-link', block: 'ttjc_ulqc-tab'},
					{ anchor: 'ttjc_pl-link', block: 'ttjc_pl-tab'},
					{ anchor: 'ttjc_mjc-link', block: 'ttjc_mjc-tab'},
					{ anchor: 'ttjc_ijc-link', block: 'ttjc_ijc-tab'},
					{ anchor: 'ttjc_model-link', block: 'ttjc_model-tab'});

	// call job costing item dwr function using the jobcostingitemid
	jobcostingitemservice.getAjaxDetailedJobItemAndAlternativeCosts(jobcostingitemid,
	{
		callback:function(results)
		{
			createJobCostingItemContent(results, true, true, false);	
		}
	});
	
	// remove the loading image from jTip
	$j('div').remove('.JT_loader');
}

/**
 * this is the callback method for function getJobCostingItemInfo()
 * 
 * @param {Object} results is an object containing all job costing item information
 * @param {Boolean} displayCosts depicts if costs section should be shown
 * @param {Boolean} displayJobItem depicts if job item section should be shown
 * @param {Boolean} qiAlternativeCosts depicts if quote item alternative costs are being shown
 */
function createJobCostingItemContent(results, displayCosts, displayJobItem, qiAlternativeCosts)
{
	// initialise job item variable
	var jobitem = null;
	// costs to be displayed?
	if (displayCosts == true)
	{
		if (qiAlternativeCosts != true)
		{
			// set job item object
			jobitem = results.results.jobitem;
		}
	}
	else
	{
		// set job item object
		jobitem = results.jobitem;
	}
	// initialise variables
	var calcosts = null;
	var costsources = null;
	var linkedQC = null;
	var allQC = null;
	var jobCost = null;
	var catalogPrice = null;
	var instjobCost = null;
	var translatedServiceTypeName = null;
	var translatedFullModelName = null;
	// costs to be displayed?
	if (displayCosts == true)
	{		
		// alternative costs
		if (qiAlternativeCosts == true)
		{
			// set calibration costs object
			calcosts = results.results;
			// set cost sources array
			costsources = calcosts.costSources;
			if (calcosts.linkedQuotationsCosts != null)
			{
				// set linked quotation costs array
				linkedQC = calcosts.linkedQuotationCosts;
			}
			else
			{
				// set linked quotation costs array
				linkedQC = [];
			}			
			// set all quotation costs array
			allQC = calcosts.unLinkedQuotationCosts;
			// set job costing array
			jobCost = calcosts.jobCostingCosts;
			// set model default object
			catalogPrice = calcosts.catalogPrices[0];
			currency = calcosts.currency
			translatedServiceTypeName = calcosts.translatedServiceTypeName;
			translatedFullModelName = calcosts.translatedFullModelName;
		}
		else
		{
			// set calibration costs object
			calcosts = results.results.calCosts;
			// set cost sources array
			costsources = calcosts.costSources;
			// set linked quotation costs array
			linkedQC = calcosts.linkedQuotationCosts;
			// set all quotation costs array
			allQC = calcosts.unLinkedQuotationCosts;
			// set job costing array
			jobCost = calcosts.jobCostingCosts;
			// set instrument job costing array
			instjobCost = calcosts.plantJobCostingCosts;
			// set model default object
			catalogPrice = calcosts.catalogPrices[0];
			
			translatedServiceTypeName = calcosts.translatedServiceTypeName;
			translatedFullModelName = calcosts.translatedFullModelName;
		}
	}

	var content = 	// main div
					'<div id="jobItemCostingTT">' +	
						// main navigation bar
						'<div class="slatenav-main">' +
							'<ul>';
								// job item and costs to be displayed?
								if (displayJobItem == true && displayCosts == true)
								{
									content += 	'<li><a href="#" id="ttjc_jobitem-link" onmouseover=" switchMenuFocus(Elements1, \'ttjc_jobitem-tab\', false); return false; " class="selected">' + i18n.t("jobItem", "Job Item") + '</a></li>' +
												'<li><a href="#" id="ttjc_jobitemcosts-link" onmouseover=" switchMenuFocus(Elements1, \'ttjc_jobitemcosts-tab\', false); return false; ">' + i18n.t("core.utilities:tooltip.calCosts", "Calibration Costs") + '</a></li>';
								}								
								// costs to be displayed?
								else if (displayCosts == true)
								{
									content += '<li><a href="#" id="ttjc_jobitemcosts-link" onmouseover=" switchMenuFocus(Elements1, \'ttjc_jobitemcosts-tab\', false); return false; " class="selected">' + i18n.t("core.utilities:tooltip.calCosts", "Calibration Costs") + '</a></li>';
								}
								// job item to be displayed?
								else
								{
									content += 	'<li><a href="#" id="ttjc_jobitem-link" onmouseover=" switchMenuFocus(Elements1, \'ttjc_jobitem-tab\', false); return false; " class="selected">' + i18n.t("jobItem", "Job Item") + '</a></li>';
								}
				content +=	'</ul>' +
						'</div>';
						
						// job item to be displayed?
						if (displayJobItem == true)
						{
							// variable to hold image path
							var image = '';
							// image added to job item?
							if (jobitem.images && jobitem.images.length > 0)
							{
								// set image path
								image = 'displaythumbnail?id=' + jobitem.images[0].id + '';
							}
							else
							{
								if (results.ipdto != null)
								{
									image = results.ipdto.imgSrc;
								}
								else
								{
									image = 'img/noimage.jpg';
								}
							}
							// job item tab for main tab index
							content += '<div id="ttjc_jobitem-tab" class="vis clearL">' +											
											// job item sub navigation bar
											'<div class="slatenav-sub">' +
												'<ul>' +
													'<li><a href="#" onmouseover=" switchMenuFocus(Elements2, \'ttjc_jisummary-tab\', false); return false; " id="ttjc_jisummary-link" class="selected">' + i18n.t("core.utilities:tooltip.summary", "Summary") + '</a></li>' +
													'<li><a href="#" onmouseover=" switchMenuFocus(Elements2, \'ttjc_jiactions-tab\', false); return false; " id="ttjc_jiactions-link">' + i18n.t("core.utilities:tooltip.actions", "Actions") + '</a></li>' +
													'<li><a href="#" onmouseover=" switchMenuFocus(Elements2, \'ttjc_jifault-tab\', false); return false; " id="ttjc_jifault-link">' + i18n.t("core.utilities:tooltip.faultReport", "Fault Report") + '</a></li>' +
													'<li><a href="#" onmouseover=" switchMenuFocus(Elements2, \'ttjc_jicontrev-tab\', false); return false; " id="ttjc_jicontrev-link">' + i18n.t("core.utilities:tooltip.contractReview", "Contract Review") + '</a></li>' +
													'<li><a href="#" onmouseover=" switchMenuFocus(Elements2, \'ttjc_jicals-tab\', false); return false; " id="ttjc_jicals-link">' + i18n.t("calibration", "Calibration") + '</a></li>' +									
												'</ul>' +
											'</div>' +
										
											// job item summary for sub tab index
											'<div id="ttjc_jisummary-tab">' +
												'<fieldset class="newtooltip">' +
													'<ol style=" background: url(' + image + ') top right no-repeat; ">' +
														'<li style=" width: 70%; ">' +
															'<label>' + i18n.t("instrumentLabel", "Instrument Model Name:") + '</label>' +
															'<div class="fleftTooltip" style=" width: 60%; ">' + results.fullModelName + '</div>' +
															'<div style=" clear: left;  "></div>' +
														'</li>' +
														'<li style=" width: 70%; ">' +
															'<label>' + i18n.t("serialNoLabel", "Serial No:") + '</label>' +
															'<span>' + jobitem.inst.serialno + '</span>' +
														'</li>' +
														'<li style=" width: 70%; ">' +
															'<label>' + i18n.t("plantNoLabel", "Plant No:") + '</label>' +
															'<span>' + jobitem.inst.plantno + '</span>' +
														'</li>' +
														'<li style=" width: 70%; ">' +
															'<label>' + i18n.t("calTypeLabel", "Calibration Type:") + '</label>' +
															'<span>' + results.calTypeLongName + '</span>' +
														'</li>' +
														'<li style=" width: 70%; ">' +
															'<label>' + i18n.t("core.utilities:tooltip.dateInLabel", "Date In:") + '</label>' +
															'<span>' + cwms.dateFormatFunction(jobitem.dateIn) + '</span>' +
														'</li>' +
														'<li style=" width: 70%; ">' +
															'<label>' + i18n.t("dueDateLabel", "Due Date:") + '</label>' +
															'<span>' + jobitem.formattedDueDate + '</span>' +
														'</li>' +
														'<li style=" width: 70%; ">' +
															'<label>' + i18n.t("core.utilities:tooltip.dateCompleteLabel", "Date Complete:") + '</label>' +
															'<span>' + cwms.dateFormatFunction(jobitem.dateComplete) + '</span>' +
														'</li>' +
														'<li>' +
															'<label>' + i18n.t("core.utilities:tooltip.laboratoryLabel", "Laboratory:") + '</label>' +
															'<span>';
																var depts = '';
																$j.each(jobitem.workRequirements, function(indexI, jiWorkReq){
																	if(jiWorkReq.workRequirement.department != null)
																	{
																		depts = depts + jiWorkReq.workRequirement.department.name;
																	}
																	if(indexI + 1 != jobitem.workRequirements.length)
																	{
																		depts = depts + ', ';
																	}
																});
								content = content + depts + '</span>' +
														'</li>' +
														'<li>' +
															'<label>' + i18n.t("statusLabel", "Status:") + '</label>';																	
															if (jobitem.state) 
															{
																content += '<span>' + jobitem.state.description + '</span>';
															}
															else 
															{
																content += '<span>&nbsp;</span>';
															}												
									content = content +	'</li>' +
														'<li>' +
															'<label>' + i18n.t("core.utilities:tooltip.requirementsLabel", "Requirements:") + '</label>';																	
															// variable used to store requirement for contract review section
															var jiRequirements = '';
															// job item requirements?
															if (jobitem.req.length > 0)
															{
																content += '<div class="fleftTooltip">' +
																				'<dl>';
																					// add any job item requirements to page
																					$j.each(jobitem.req, function(i)
																					{
																						// check it has not been deleted
																						if (!jobitem.req[i].deleted)
																						{
																							content += '<dt>' + jobitem.req[i].createdBy.name + ' - ' + cwms.dateFormatFunction(jobitem.req[i].createdDate) + '</dt><dd>' + jobitem.req[i].requirement + '</dd>';
																							jiRequirements += '<dt>' + jobitem.req[i].createdBy.name + ' - ' + cwms.dateFormatFunction(jobitem.req[i].createdDate) + '</dt><dd>' + jobitem.req[i].requirement + '</dd>';
																						}
																					});
																	content +=	'</dl>' +
																					'</div>' +
																					// clear floats and restore page flow -->
																					'<div class="clear"></div>';																				
															}
															else
															{
																// add message of no requirements
																content += '<span>' + i18n.t("core.utilities:tooltip.noRequirementsForJI", "No requirements for job item") + '</span>';
																jiRequirements = '<span>' + i18n.t("core.utilities:tooltip.noContractReviewRequirements", "No contract review requirements") + '</span>';
															}													
											content += '</li>' +
													'</ol>' +
												'</fieldset>' +
											'</div>' +
											// end of job item summary tab for sub index
											
											// job item actions tab
											'<div id="ttjc_jiactions-tab" class="hid">' +
												'<table class="default2" summary="This table shows job item activities">' +
													'<thead>' +
														'<tr>' +
															'<td colspan="6">' + i18n.t("core.utitities:tooltip.activities", "Activities") + '</td>' +
														'</tr>' +
														'<tr>' +															
															'<th class="activity" scope="col">' + i18n.t("activity", "Activity") + '</th>' +
															'<th class="endstatus" scope="col">' + i18n.t("endStatus", "End Status") + '</th>' +
															'<th class="remarks" scope="col">' + i18n.t("core.utitities:tooltip.remarks", "Remarks") + '</th>' +
															'<th class="time" scope="col">' + i18n.t("time", "Time") + '</th>' +
															'<th class="compby" scope="col">' + i18n.t("completedBy", "Completed By") + '</th>' +
															'<th class="compon" scope="col">' + i18n.t("completedOn", "Completed On") + '</th>' +
														'</tr>' +
													'</thead>' +
													'<tbody>';
														// job item actions?
														if (jobitem.actions.length > 0)
														{
															// add actions to page
															$j.each(jobitem.actions, function(i)
															{															
																var activityDesc = '';
																var endStatus = '';
																var remark = '';
																var timeSpent = '';
																var completeBy = '';
																var endTime = '';
																
																if (jobitem.actions[i].activityDesc)
																{
																	activityDesc = jobitem.actions[i].activityDesc;
																}
																if (jobitem.actions[i].endStatus)
																{
																	endStatus = jobitem.actions[i].endStatus.description;
																}																
																if (jobitem.actions[i].remark)
																{
																	remark = jobitem.actions[i].remark;
																}
																if (jobitem.actions[i].timeSpent)
																{
																	timeSpent = jobitem.actions[i].timeSpent;
																}
																if (jobitem.actions[i].completedBy)
																{
																	completeBy = jobitem.actions[i].completedBy.name;
																}
																if (jobitem.actions[i].endStamp)
																{
																	endTime = jobitem.actions[i].endStamp;
																}
																																		
																		content += '<tr class="odd">' +																						
																						'<td>' + activityDesc + '</td>' +
																						'<td>' + endStatus + '</td>' +
																						'<td>' + remark + '</td>' +
																						'<td>' + timeSpent + ' mins</td>' +
																						'<td>' + completeBy + '</td>' +
																						'<td>' + cwms.dateTimeFormatFunction(endTime) + '</td>' +
																					'</tr>';
															});
														}
														else
														{
															// add message of no actions
															content += '<tr class="odd center bold">' +
																			'<td colspan="6">' + i18n.t("core.utilities:tooltip.noActionsToDisplay", "No actions to display") + '</td>' +
																		'</tr>';
														}											
										content += '</tbody>' +
												'</table>' +
											'</div>' +
											// end of job item actions tab													 			
											// job item fault report tab
											'<div id="ttjc_jifault-tab" class="hid">' +
												'<fieldset class="newtooltip">';
													// fault report present?
													if (jobitem.faultRep != null)
													{
														// set fault report object
														var faultRep = jobitem.faultRep;
														// add job item fault report content to page																			
																content += '<div class="displaycolumn">' +
																				'<ol>' +
																					'<li>' +
																						'<label>' + i18n.t("core.utilities:tooltip.befMesurements", "Bef R/A Measurements:") + '</label>' +
																						'<span>' + faultRep.preRecord + '</span>' +
																					'</li>' +
																					'<li>' +
																						'<label>' + i18n.t("core.utilities:tooltip.befCerts", "Bef R/A Certificate:") + '</label>' +
																						'<span>' + faultRep.preCert + '</span>' +
																					'</li>' +
																					'<li>' +
																						'<label>' + i18n.t("core.utilities:tooltip.leadTimeLabel", "Lead Time:") + '</label>' +
																						'<span>';
																							if (faultRep.leadTime)
																							{
																								content += faultRep.leadTime + ' ' + i18n.t("days", "days");
																							}
																			content +=	'</span>' +
																					'</li>' +
																				'</ol>' +
																			'</div>' +
																			'<div class="displaycolumn">' +
																				'<ol>' +
																					'<li>' +
																						'<label>' + i18n.t("core.utilities:tooltip.estRepairTimeLabel", "Est Repair Time:") + '</label>' +
																						'<span>' + faultRep.estRepairTime + ' ' + i18n.t("minutes", "minutes") + '</span>' +
																					'</li>' +
																					'<li>' +
																						'<label>' + i18n.t("core.utilities:tooltip.estAdjsutTimeLabel", "Est Adjust Time:") + '</label>' +
																						'<span>' + faultRep.estAdjustTime + ' ' + i18n.t("minutes", "minutes") + '</span>' +
																					'</li>' +
																					'<li>' +
																						'<label>' + i18n.t("core.utilities:tooltip.costLabel", "Cost:") + '</label>' +
																						'<span>';
																							if (faultRep.cost)
																							{
																								content += jobitem.job.currency.currencySymbol + faultRep.cost;
																							}
																			content += 	'</span>' +
																					'</li>' +
																				'</ol>' +
																			'</div>' +
																			// clear floats and restore page flow -->
																			'<div class="clear"></div>' +
																			'<ol>' +
																				'<li>' +
																					'<label>' + i18n.t("core.utilities:tooltip.faultDescLabel", "Fault Descriptions:") + '</label>';																							
																					// fault report descriptions?
																					if (faultRep.descriptions.length > 0)
																					{
																						content += '<div class="fleftTooltip">' +
																										'<dl>';
																						// add fault report descriptions to page
																						$j.each(faultRep.descriptions, function(i)
																						{
																							content += '<dt>';
																							if (faultRep.descriptions[i].recordedBy)
																							{
																								content += faultRep.descriptions[i].recordedBy.name;
																							}
																							content += ' - ' + cwms.dateFormatFunction(faultRep.descriptions[i].lastModified) + '</dt><dd>' + faultRep.descriptions[i].description + '</dd>';
																						});
																						content +=	'</dl>' +
																								'</div>' +
																								// clear floats and restore page flow -->
																								'<div class="clear"></div>';
																					}
																					else
																					{
																						// add message of no descriptions
																						content += '<span>' + i18n.t("core.utilities:tooltip.noFaultReportDesc", "No fault report descriptions") + '</span>';
																					}																			
																	content += 	'</li>' +
																				'<li>' +
																					'<label>' + i18n.t("core.utilities:tooltip.faultActionsLabel", "Fault Actions:") + '</label>';																							
																					// fault report actions?
																					if (faultRep.actions.length > 0) 
																					{
																						content += '<div class="fleftTooltip">' +
																										'<dl>';
																						// add fault report actions to page
																						$j.each(faultRep.actions, function(i)
																						{
																							content += '<dt>';
																							if (faultRep.actions[i].recordedBy)
																							{
																								content += faultRep.actions[i].recordedBy.name;
																							}
																							content += ' - ' + cwms.dateFormatFunction(faultRep.actions[i].lastModified) + '</dt><dd>' + faultRep.actions[i].description + '</dd>';
																						});
																						content +=	'</dl>' +
																								'</div>' +
																								// clear floats and restore page flow -->
																								'<div class="clear"></div>';
																					}
																					else
																					{
																						// add message of no actions
																						content += '<span>' + i18n.t("core.utilities:tooltip.noFaultReportActions", "No fault report actions") + '</span>';
																					}																			
																	content +=	'</li>' +
																			'</ol>';
													}
													else
													{
														// add message of no fault report
														content += '<ol>' +
																		'<li>' +
																			'<span>' + i18n.t("core.utilities:tooltip.itemHasNoFaultReport", "This item has no fault report") + '</span>' +
																		'</li>' +
																	'</ol>';
													}
									content += 	'</fieldset>' +
											'</div>' +
											// end of job item fault report tab
											
											// job item contract review tab
											'<div id="ttjc_jicontrev-tab" class="hid">' +
												'<fieldset class="newtooltip">' +
													'<ol>' +
														'<li>' +
															'<label>' + i18n.t("turnaroundLabel", "Turnaround:") + '</label>' +
															'<span>' + jobitem.turn + '</span>' +
														'</li>' +
														'<li>' +
															'<label>' + i18n.t("core.utilities:tooltip.returnDateLabel", "Return Date:") + '</label>' +
															'<span>' + jobitem.formattedDueDate + '</span>' +
														'</li>' +
														'<li>' +
															'<label>' + i18n.t("core.utilities:tooltip.requirementsLabel", "Requirements:") + '</label>';
															// job item requirements?
															if (jobitem.req.length > 0)
															{
																content += jiRequirements;																				
															}
															else
															{
																// add message of no requirements
																content += jiRequirements;
															}	
											content += '</li>' +
													'</ol>' +
												'</fieldset>' +
												'<table class="default2" summary="This table shows contract review costs">' +
													'<thead>' +
														'<tr>' +
															'<td colspan="6">' + i18n.t("core.utilities:tooltip.contractReviewCosts", "Contract Review Costs") + '</td>' +
														'</tr>' +
														'<tr>' +
															'<th class="costtype" scope="col">' + i18n.t("core.utilities:tooltip.costType", "Cost Type") + '</th>' +
															'<th class="costs" scope="col">Costs</th>' +
															'<th class="disc" scope="col">' + i18n.t("discount", "Discount") + '</th>' +
															'<th class="discval" scope="col">' + i18n.t("core.utilities:tooltip.discountValue", "Discount Value") + '</th>' +
															'<th class="finalcost" scope="col">' + i18n.t("finalCost", "Final Cost") + '</th>' +
															'<th class="costsource" scope="col">' + i18n.t("costSource", "Cost Source") + '</th>' +
														'</tr>' +
													'</thead>' +
													'<tbody>';
														// job item contract review costs?
														if (jobitem.costs)
														{															
															// add contract review costs to page
															$j.each(jobitem.costs, function(i)
															{																
																// contract review cost active?
																if (jobitem.costs[i].active == true)
																{
																	// add contract review costs to page
																	content += '<tr class="odd">' +
																					'<td>' + jobitem.costs[i].costType.name + '</td>' +
																					'<td>' + jobitem.job.currency.currencySymbol + jobitem.costs[i].totalCost.toFixed(2) + '</td>' +
																					'<td>' + jobitem.costs[i].discountRate + '%</td>' +
																					'<td>' + jobitem.job.currency.currencySymbol + jobitem.costs[i].discountValue.toFixed(2) + '</td>' +
																					'<td>' + jobitem.job.currency.currencySymbol + jobitem.costs[i].finalCost.toFixed(2) + '</td>';
																					// costs to be displayed?
																					if (displayCosts == true)
																					{
																						// source is quotation?
																						if (jobitem.costs[i].costSrc == "QUOTATION")
																						{	
																							// problem loading this info
																							// add links to quotation																			
																							// content += '<td>' + jobitem.costs[i].costSrc + ' <a href="viewquotation.htm?id=' + jobitem.costs[i].linkedCost.quotationCalibrationCost.quoteItem.quotation.id + '" target="_blank" title="' + i18n.t("core.utilities:tooltip.viewThisQuote", "View this quote") + '" class="mainlink">' + jobitem.costs[i].linkedCost.quotationCalibrationCost.quoteItem.quotation.qno + '</a> Item <a href="#" onclick=" window.open(\'editquotationitem.htm?id=' + jobitem.costs[i].linkedCost.quotationCalibrationCost.quoteItem.id + '\'); " class="mainlink">' + jobitem.costs[i].linkedCost.quotationCalibrationCost.quoteItem.itemno + '</a></td>';
																							content += '<td>' + jobitem.costs[i].costSrc + '</td>';
																						}																																																					
																						else
																						{
																							// add source
																							content += '<td>' + jobitem.costs[i].costSrc + '</td>';
																						}
																					}
																					else
																					{
																						// add source
																						content += '<td>' + jobitem.costs[i].costSrc + '</td>';
																					}																																					
																	content +=	'</tr>';
																}
															});
														}
														else
														{
															// add message of no contract review costs
															content += '<tr class="odd center bold">' +
																			'<td colspan="6">' + i18n.t("core.utilities:tooltip.noContractReviewCosts", "No contract review cost") + '</td>' +
																		'</tr>';
														}
														
										content +=	'</tbody>' +
												'</table>' +
												'<table class="default2" summary="This table shows contract review history for job item">' +
													'<thead>' +
														'<tr>' +
															'<td colspan="3">' + i18n.t("core.utilities:tooltip.contractReviewHistory", "Contract Review History") + '</td>' +
														'</tr>' +
														'<tr>' +
															'<th class="reviewer" scope="col">' + i18n.t("core.utilities:tooltip.reviewer", "Reviewer") + '</th>' +
															'<th class="daterev" scope="col">' + i18n.t("core.utilities:tooltip.dateReviewed", "Date Reviewed") + '</th>' +
															'<th class="notes" scope="col">' + i18n.t("core.utilities:tooltip.notesLeft", "Notes Left") + '</th>' +
														'</tr>' +
													'</thead>' +
													'<tbody>';
														// contract review items?
														if (jobitem.contactReviewItems.length > 0)
														{
															// add contract review history to page
															$j.each(jobitem.contactReviewItems, function(i){
																content += '<tr class="odd">' +
																				'<td>' + jobitem.contactReviewItems[i].review.reviewBy.name + '</td>' +
																				'<td>' + cwms.dateFormatFunction(jobitem.contactReviewItems[i].review.reviewDate) + '</td>' +
																				'<td>' + jobitem.contactReviewItems[i].review.comments + '</td>' +
																			'</tr>';
															});
														}
														else
														{
															// add message of no contract review history
															content += '<tr class="odd center bold">' +
																			'<td colspan="3">' + i18n.t("core.utilities:tooltip.noContractReviewHistory", "No contract review history") + '</td>' +
																		'</tr>';
														}										
										content += '</tbody>' +
												'</table>' +
											'</div>' +
											// end of job item contract review tab
											
											// job item calibrations tab
											'<div id="ttjc_jicals-tab" class="hid">' +
												'<table class="default2" summary="This table shows job item calibrations">' +
													'<thead>' +
														'<tr>' +
															'<td colspan="6">' + i18n.t("core.utilities:tooltip.calibrations", "Calibrations") + '</td>' +
														'</tr>' +
														'<tr>' +
															'<th class="sdate" scope="col">' + i18n.t("core.utilities:tooltip.startDateTime", "Start Date/Time") + '</th>' +
															'<th class="tech" scope="col">' + i18n.t("core.utilities:tooltip.technician", "Technician") + '</th>' +
															'<th class="caltype" scope="col">' + i18n.t("calType", "Service Type") + '</th>' +
															'<th class="process" scope="col">' + i18n.t("core.utilities:tooltip.process", "Process") + '</th>' +
															'<th class="proc" scope="col">' + i18n.t("procedure", "Procedure") + '</th>' +
															'<th class="status" scope="col"">' + i18n.t("status", "Status") + '</th>' +
														'</tr>' +
													'</thead>' +
													'<tbody>';
														// job item calibrations?
														if (jobitem.calLinks.length > 0)
														{
															var startedBy = '';
															// add calibrations to page
															$j.each(jobitem.calLinks, function(i){
																
																if (jobitem.calLinks[i].cal.startedBy)
																{
																	startedBy = jobitem.calLinks[i].cal.startedBy.name;
																}
																content += '<tr class="odd">' +
																				'<td>' + cwms.dateTimeFormatFunction(jobitem.calLinks[i].cal.startTime) + '</td>' +
																				'<td>' + startedBy + '</td>' +
																				'<td class="center">' + jobitem.calLinks[i].cal.calType.shortName + '</td>' +
																				'<td class="center">' + jobitem.calLinks[i].cal.calProcess.process + '</td>' +
																				'<td class="center">' + jobitem.calLinks[i].cal.proc.reference + '</td>' +
																				'<td>' + jobitem.calLinks[i].cal.status.name + '</td>' +
																			'</tr>';
															});
														}
														else
														{
															// add message of no job item calibrations
															content += '<tr class="odd center bold">' +
																			'<td colspan="6">' + i18n.t("core.utilities:tooltip.noCalsToDisplay", "No calibrations to display") + '</td>' +
																		'</tr>';
														}											
										content += '</tbody>' +
												'</table>' +
											'</div>' +
											// end of job item calibrations tab
																
										'</div>';
										// end of job item tab for main tab index										

						}						
						// display costs?
						if (displayCosts == true)
						{
							// job item costs tab for main tab index
							content += '<div id="ttjc_jobitemcosts-tab"';
													if (displayJobItem == false) 
													{
														content += 'class="vis">';
													}
													else 
													{
														content += 'class="hid">';
													}									
													// calibration costs sub navigation bar
										content +=	'<div class="slatenav-sub">' +
														'<ul>' +
															'<li><a href="#" onmouseover=" switchMenuFocus(Elements3, \'ttjc_lqc-tab\', false); return false; " id="ttjc_lqc-link" class="selected">' + i18n.t("linkedQuotesCosts", "Linked Quotes Costs") + ' (' + linkedQC.length + ')</a></li>' +
															'<li><a href="#" onmouseover=" switchMenuFocus(Elements3, \'ttjc_ulqc-tab\', false); return false; " id="ttjc_ulqc-link">' + i18n.t("allQuotesCosts", "All Quotes Costs") + ' (' + allQC.length + ')</a></li>' +
															'<li><a href="#" onmouseover=" switchMenuFocus(Elements3, \'ttjc_mjc-tab\', false); return false; " id="ttjc_mjc-link">' + i18n.t("modelCostings", "Model Costings") + ' (' + jobCost.length + ')</a></li>';
															if (qiAlternativeCosts != true)
															{
																content += '<li><a href="#" onmouseover=" switchMenuFocus(Elements3, \'ttjc_ijc-tab\', false); return false; " id="ttjc_ijc-link">' + i18n.t("instCostings", "Inst Costings") + ' (' + instjobCost.length + ')</a></li>';	
															}
												content += 	'<li><a href="#" onmouseover=" switchMenuFocus(Elements3, \'ttjc_model-tab\', false); return false; " id="ttjc_model-link">' + i18n.t("core.utilities:tooltip.modelDefault", "Model Default") + '</a></li>' +									
														'</ul>' +
													'</div>' +													
													// div containing linked quotation costs table
													'<div id="ttjc_lqc-tab">' +
														'<table class="default2" summary="This table displays all linked quotation costs">' +
															'<thead>' +
																'<tr>' +
																	'<td colspan="6">' + i18n.t("linkedQuotationCosts", "Linked Quotation Costs") + ' (' + linkedQC.length + ')</td>' +
																'</tr>' +
																'<tr>' +
																	'<th class="qno" scope="col">' + i18n.t("quoteNo", "Quote No") + '</th>' +
																	'<th class="itemno" scope="col">' + i18n.t("itemNo", "Item No") + '</th>' +
																	'<th class="costcomp" scope="col">' + i18n.t("company", "Company") + '</th>' +
																	'<th class="costcont" scope="col">' + i18n.t("contact", "Contact") + '</th>' +
																	'<th class="cost" scope="col">' + i18n.t("core.utilities:tooltip.discountAndCalCost", "Discount/Cal Cost") + '</th>' +																	
																	'<th class="final" scope="col">' + i18n.t("core.utilities:tooltip.finalCalCost", "Final Cal Cost") + '</th>' +
																'</tr>' +
															'</thead>' +
															'<tfoot>' +
																'<tr>' +
																	'<td colspan="6">&nbsp;</td>' +
																'</tr>' +
															'</tfoot>' +
															'<tbody>';
															// linked quote costs?
															if (linkedQC == null || linkedQC.length < 1)
															{
																// add message of no linked quote costs
																content += 	'<tr>' +
																				'<td colspan="6" class="bold center">' + i18n.t("noLinkedQuotationsCostsFound", "No linked quotation costs found") + '</td>' +
																			'</tr>';
															}
															else
															{		
																// add linked quote costs to page								
																$j.each(linkedQC, function(i, lqc)
																{																	
																	content += '<tr>' +
																					'<td class="qno"><a href="viewquotation.htm?id=' + lqc.quoteItem.quotation.id + '" target="_blank" title="' + i18n.t("core.utilities:tooltip.viewThisQuote", "View this quote") + ' class="mainlink">' + lqc.quoteItem.quotation.qno + ' ver ' + lqc.quoteItem.quotation.ver + '</a></td>' +
																					'<td class="itemno"><a href="editquotationitem.htm?id=' + lqc.quoteItem.id + '" target="_blank" title="' + i18n.t("core.utilities:tooltip.viewQuoteItem", "View quote item") + ' class="mainlink">' + i18n.t("item", "Item") + ' ' + lqc.quoteItem.itemno + '</a></td>' +
																					'<td class="costcomp">' + lqc.quoteItem.quotation.contact.sub.comp.coname + '</td>' +
																					'<td class="costcont">' + lqc.quoteItem.quotation.contact.name + '</td>' +
																					'<td class="cost">'; 
																						if (lqc.discountValue > 0)
																						{
																							content += '(' + lqc.discountRate.toFixed(2) + '&#37; - ' + lqc.quoteItem.quotation.currency.currencyERSymbol + lqc.discountValue.toFixed(2) + ')';
																						}	
																		content += 	' ' + lqc.quoteItem.quotation.currency.currencyERSymbol + lqc.totalCost.toFixed(2) + '</td>' +
																					'<td class="final">' + lqc.quoteItem.quotation.currency.currencyERSymbol + lqc.finalCost.toFixed(2) + '</td>' +
																				'</tr>';																	
																});
															}															
												content += '</tbody>' +													
														'</table>' +
													'</div>' +
													// div containing all quotation costs table
													'<div id="ttjc_ulqc-tab" class="hid">' +
														'<table class="default2" summary="This table displays all quotation costs">' +
															'<thead>' +
																'<tr>' +
																	'<td colspan="6">' + i18n.t("allQuotationCosts", "All Quotation Costs") + ' (' + allQC.length + ')</td>' +
																'</tr>' +
																'<tr>' +
																	'<th class="qno" scope="col">' + i18n.t("quoteNo", "Quote No") + '</th>' +
																	'<th class="itemno" scope="col">' + i18n.t("itemNo", "Item No") + '</th>' +
																	'<th class="costcomp" scope="col">' + i18n.t("company", "Company") + '</th>' +
																	'<th class="costcont" scope="col">' + i18n.t("contact", "Contact") + '</th>' +
																	'<th class="cost" scope="col">' + i18n.t("core.utilities:tooltip.discountAndCalCost", "Discount/Cal Cost") + '</th>' +																	
																	'<th class="final" scope="col">' + i18n.t("core.utilities:tooltip.finalCalCost", "Final Cal Cost") + '</th>' +
																'</tr>' +
															'</thead>' +
															'<tfoot>' +
																'<tr>' +
																	'<td colspan="6">&nbsp;</td>' +
																'</tr>' +
															'</tfoot>' +
															'<tbody>';
															// quote costs?
															if (allQC.length < 1)
															{
																// add message of no quote costs
																content += 	'<tr>' +
																				'<td colspan="6" class="bold center">' + i18n.t("noQuotationCostsFound", "No quotation costs found") + '</td>' +
																			'</tr>';
															}
															else
															{
																// add quote costs to page
																$j.each(allQC, function(i, qc)
																{																	
																	content += '<tr>' +
																					'<td class="qno"><a href="viewquotation.htm?id=' + qc.quoteItem.quotation.id + '" target="_blank" title="' + i18n.t("core.utilities:tooltip.viewThisQuote", "View this quote") + '" class="mainlink">' + qc.quoteItem.quotation.qno + ' ver ' + qc.quoteItem.quotation.ver + '</a></td>' +
																					'<td class="itemno"><a href="editquotationitem.htm?id=' + qc.quoteItem.id + '" target="_blank" title="View quote item" class="mainlink">' + i18n.t("item", "Item") + ' ' + qc.quoteItem.itemno + '</a></td>' +
																					'<td class="costcomp">' + qc.quoteItem.quotation.contact.sub.comp.coname + '</td>' +
																					'<td class="costcont">' + qc.quoteItem.quotation.contact.name + '</td>' +
																					'<td class="cost">';
																						if (qc.discountValue > 0)
																						{
																							content += '(' + qc.discountRate + '&#37; - ' + qc.quoteItem.quotation.currency.currencyERSymbol + qc.discountValue.toFixed(2) + ')';
																						}	
																		content += 	' ' + qc.quoteItem.quotation.currency.currencyERSymbol + qc.totalCost.toFixed(2) + '</td>' +
																					'<td class="final">' + qc.quoteItem.quotation.currency.currencyERSymbol + qc.finalCost.toFixed(2) + '</td>' +
																				'</tr>';
																});
															}															
												content += '</tbody>' +													
														'</table>' +
													'</div>' +					
													// div containing model job costings table
													'<div id="ttjc_mjc-tab" class="hid">' +
														'<table class="default2" summary="This table displays all model job costings">' +
															'<thead>' +
																'<tr>' +
																	'<td colspan="7">' + i18n.t("modelCostings", "Model Costings") + ' (' + jobCost.length + ')</td>' +
																'</tr>' +
																'<tr>' +
																	'<th class="jcno" scope="col">' + i18n.t("jobCostingNo", "Job Costing No") + '</th>' +
																	'<th class="itemno" scope="col">' + i18n.t("itemNo", "Item No") + '</th>' +
																	'<th class="costcomp" scope="col">' + i18n.t("company", "Company") + '</th>' +
																	'<th class="costcont" scope="col">' + i18n.t("contact", "Contact") + '</th>' +
																	'<th class="caltypeturn" scope="col">' + i18n.t("core.utilities:tooltip.calTypeAndTurn", "Caltype/Turn") + '</th>' +
																	'<th class="cost" scope="col">' + i18n.t("core.utilities:tooltip.discountAndCalCost", "Disc/Cal Cost") + '</th>' +																	
																	'<th class="final" scope="col">' + i18n.t("core.utilities:tooltip.finalCalCost", "Final Cal Cost") + '</th>' +
																'</tr>' +
															'</thead>' +
															'<tfoot>' +
																'<tr>' +
																	'<td colspan="7">&nbsp;</td>' +
																'</tr>' +
															'</tfoot>' +
															'<tbody>';
															// model job costings?
															if (jobCost.length < 1)
															{
																// add message of no model job costings
																content += 	'<tr>' +
																				'<td colspan="7" class="bold center">' + i18n.t("noModelJobCostingsFound", "No model job costings found") + '</td>' +
																			'</tr>';
															}
															else
															{
																// add model job costings to page
																$j.each(jobCost, function(i, jc)
																{																	
																	content += 	'<tr style=" background-color: ' + jc.jobCostingItem.jobItem.calType.displayColour + '">' +
																					'<td class="jcno"><a href="viewjobcosting.htm?id=' + jc.jobCostingItem.jobCosting.id + '" target="_blank" class="mainlink">' + jc.jobCostingItem.jobCosting.identifier + '</a></td>' +
																					'<td class="itemno"><a href="editjobcostingitem.htm?id=' + jc.jobCostingItem.id + '" target="_blank" class="mainlink">' + i18n.t("item", "Item") + ' ' + jc.jobCostingItem.itemno + '</a></td>' +
																					'<td class="costcomp">' + jc.jobCostingItem.jobCosting.contact.sub.comp.coname + '</td>' +
																					'<td class="costcont">' + jc.jobCostingItem.jobCosting.contact.name + '</td>' +
																					'<td class="caltypeturn">';
																	if (jc.jobCostingItem.jobItem.calType.serviceType != null) {
																		content += jc.jobCostingItem.jobItem.calType.serviceType.shortName;
																	}
																	content += ' / ' + jc.jobCostingItem.jobItem.turn + '</td>' +
																					'<td class="cost">';
																						if (jc.discountValue > 0)
																						{
																							content += '(' + jc.discountRate.toFixed(2) + '&#37; - ' + jc.jobCostingItem.jobCosting.currency.currencyERSymbol + jc.discountValue.toFixed(2) + ')';
																						}	
																		content += 	' ' + jc.jobCostingItem.jobCosting.currency.currencyERSymbol + jc.totalCost.toFixed(2) + '</td>' +
																					'<td class="final">' + jc.jobCostingItem.jobCosting.currency.currencyERSymbol + jc.finalCost.toFixed(2) + '</td>' +
																				'</tr>';
																});
															}
															
												content += '</tbody>' +													
														'</table>' +
													'</div>';
													if (qiAlternativeCosts != true)
													{
																	// div containing instrument job costings table
														content += 	'<div id="ttjc_ijc-tab" class="hid">' +
																		'<table class="default2" summary="This table displays all instrument job costings">' +
																			'<thead>' +
																				'<tr>' +
																					'<td colspan="5">' + i18n.t("instrCostings", "Instrument Costings") + ' (' + instjobCost.length + ')</td>' +
																				'</tr>' +
																				'<tr>' +
																					'<th class="jcno" scope="col">' + i18n.t("jobCostingNo", "Job Costing No") + '</th>' +
																					'<th class="itemno" scope="col">' + i18n.t("itemNo", "Item No") + '</th>' +
																					'<th class="caltypeturn" scope="col">' + i18n.t("core.utilities:tooltip.calTypeAndTurn", "Cal Type/Turn") + '</th>' +
																					'<th class="cost" scope="col">' + i18n.t("core.utilities:tooltip.discountAndCalCost", "Discount/Cal Cost") + '</th>' +																					
																					'<th class="final" scope="col">' + i18n.t("core.utilities:tooltip.finalCalCost", "Final Cal Cost") + '</th>' +
																				'</tr>' +
																			'</thead>' +
																			'<tfoot>' +
																				'<tr>' +
																					'<td colspan="5">&nbsp;</td>' +
																				'</tr>' +
																			'</tfoot>' +
																			'<tbody>';
																			// instrument job costings?
																			if (instjobCost.length < 1)
																			{
																				// add message of no instrument job costings
																				content += 	'<tr>' +
																								'<td colspan="5" class="bold center">' + i18n.t("noInstrJobCostingsFound", "No instrument job costings found") + '</td>' +
																							'</tr>';
																			}
																			else
																			{
																				// add instrument job costings to page
																				$j.each(instjobCost, function(i, ijc)
																				{																	
																					content += 	'<tr style=" background-color: ' + ijc.jobCostingItem.jobItem.calType.displayColour + '">' +
																									'<td class="jcno"><a href="viewjobcosting.htm?id=' + ijc.jobCostingItem.jobCosting.id + '" target="_blank" class="mainlink">' + ijc.jobCostingItem.jobCosting.identifier + '</a></td>' +
																									'<td class="itemno"><a href="editjobcostingitem.htm?id=' + ijc.jobCostingItem.id + '" target="_blank" class="mainlink">' + i18n.t("item", "Item") + ' ' + ijc.jobCostingItem.itemno + '</a></td>' +
																									'<td class="caltypeturn">' + ijc.jobCostingItem.jobItem.calType.shortName + ' / ' + ijc.jobCostingItem.jobItem.turn + '</td>' +
																									'<td class="cost">';
																										if (ijc.discountValue > 0)
																										{
																											content += '(' + ijc.discountRate.toFixed(2) + '&#37; - ' + ijc.jobCostingItem.jobCosting.currency.currencyERSymbol + ijc.discountValue.toFixed(2) + ')';
																										}
																						content += 	' ' + ijc.jobCostingItem.jobCosting.currency.currencyERSymbol + ijc.totalCost.toFixed(2) + '</td>' +
																									'<td class="final">' + ijc.jobCostingItem.jobCosting.currency.currencyERSymbol + ijc.finalCost.toFixed(2) + '</td>' +
																								'</tr>';
																				});
																			}
																			
																content += '</tbody>' +													
																		'</table>' +
																	'</div>';
													}												
													// div containing model default costs table
										content +=	'<div id="ttjc_model-tab" class="hid">' +
														'<table class="default2" summary="This table displays the model default cost">' +
															'<thead>' +
																'<tr>' +
																	'<td colspan="3">' + i18n.t("modelDefaultCosts", "Model Default Price") + '</td>' +
																'</tr>' +
																'<tr>' +
																	'<th class="mod" scope="col">' + i18n.t("modelName", "Model Name") + '</th>' +
																	'<th class="caltype" >' + i18n.t("calType", "Service Type") + '</th>' +
																	'<th class="cost cost2pc" scope="col">' + i18n.t("calCost", "Calibration Price") + '</th>' +
																'</tr>' +
															'</thead>' +
															'<tfoot>' +
																'<tr>' +
																	'<td colspan="3">&nbsp;</td>' +
																'</tr>' +
															'</tfoot>' +
															'<tbody>';
										
										
																// model default cost?
																if (!catalogPrice)
																{
																	// add message of no model default cost
																	content += 	'<tr>' +
																					'<td colspan="3" class="bold center">' + i18n.t("noDefaultModelCostFound", "No default model price found") + '</td>' +
																				'</tr>';
																}
																else
																{
																	// add model default cost to page
																	content += 	'<tr>' +
																					'<td>'+ translatedFullModelName + '</td>' +
																					'<td>' + translatedServiceTypeName + '</td>' +
					                                                                '<td class="cost2pc">' + catalogPrice + ' '+
					                                                                currency.currencyERSymbol +
																					'</td></tr>';
																}
																
												content +=	'</tbody>' +													
														'</table>' +
													'</div>' +
											'</div>';
											// end of job item costs tab for main tab index
						}
						
		content +=	'</div>';
					// end of main div
															
	$j('#JT_copy').append(content);
	
	/**
	 * set hover functionality of pop up.  ON mouse over do nothing and on mouse out close the pop up.
	 */ 
	$j('#JT').hover(function()	{
									mouseOverSignal = true;
								},
					function()	{
									$j('#JT').remove();
									mouseOverSignal = false; 
									return false;
								});			
}


/**
 * this method retrieves job costing information for the jobcostingid provided.
 * this information is then appended to the tooltip pop up.
 * 
 * @param {Integer} jobcostingid id of the job costing for which information is required
 */
function getJobCostingInfo(jobcostingid)
{	
	// add submenu elements to array
	Elements1.push(	{ anchor: 'ttjc_costing-link', block: 'ttjc_costing-tab' });
					
	// add submenu elements to array
	Elements2.push(	{ anchor: 'ttjc_costsummary-link', block: 'ttjc_costsummary-tab'},
					{ anchor: 'ttjc_contactsummary-link', block: 'ttjc_contactsummary-tab'});

	// call job costing dwr function using the jobcostingid
	jobcostingservice.findAjaxEagerJobCosting(jobcostingid,
	{
		callback:function(results)
		{
			createJobCostingContent(results);	
		}
	});
	
	// remove the loading image from jTip
	$j('div').remove('.JT_loader');
}


/**
 * this is the callback method for function getJobCostingInfo()
 * 
 * @param {Object} costing is an object containing all job costing information
 */
function createJobCostingContent(costing)
{	
	//$j.prompt(DWRUtil.toDescriptiveString(costing.results.items[0].jobItem, 2));
	
	if (costing.success)
	{
		var cost = costing.results;
	
		var content = 	// main div
						'<div id="jobcostingTT">' +	
							// main navigation bar
							'<div class="slatenav-main">' +
								'<ul>' +
									'<li><a href="#" id="ttjc_costing-link" onmouseover=" switchMenuFocus(Elements1, \'ttjc_costing-tab\', false); return false; " class="selected">' + i18n.t("jobCosting", "Job Costing") + '</a></li>' +
								'</ul>' +
							'</div>' +
							'<div id="ttjc_costing-tab" class="vis clearL">' +													
								// job costing sub navigation bar
								'<div class="slatenav-sub">' +
									'<ul>' +
										'<li><a href="#" onmouseover=" switchMenuFocus(Elements2, \'ttjc_costsummary-tab\', false); return false; " id="ttjc_costsummary-link" class="selected">' + i18n.t("core.utilities:tooltip.summary", "Summary") + '</a></li>' +
										'<li><a href="#" onmouseover=" switchMenuFocus(Elements2, \'ttjc_contactsummary-tab\', false); return false; " id="ttjc_contactsummary-link">' + i18n.t("contact", "contact") + '</a></li>' +
									'</ul>' +
								'</div>' +
								// job costing summary tab
								'<div id="ttjc_costsummary-tab">' +
									'<fieldset class="newtooltip">' +
										'<div class="displaycolumn">' +
											'<ol>' +
												'<li>' +
													'<label>' + i18n.t("core.utilities:tooltip.costingNoLabel", "Costing No:") + '</label>' +
													'<span>' + cost.identifier + '</span>' +
												'</li>' +												
												'<li>' +
													'<label>' + i18n.t("core.utilities:tooltip.costingTypeLabel", "Costing Type:") + '</label>' +
													'<span>' + cost.costingType.type + '</span>' +
												'</li>' +
												'<li>' +
													'<label>' + i18n.t("core.utilities:tooltip.createdByLabel", "Created By:") + '</label>' +
													'<span>';
														if (cost.createdBy)
														{
															content += cost.createdBy.name;
														}
										content += 	'</span>' +
												'</li>' +
												'<li>' +
													'<label>' + i18n.t("core.utilities:tooltip.createdOnLabel", "Created On:") + '</label>' +
													'<span>' + formatDate(cost.regdate, "dd.MM.yyyy") + '</span>' +
												'</li>' +
											'</ol>' +
										'</div>' +
										'<div class="displaycolumn">' +
											'<ol>' +										
												'<li>' +
													'<label>' + i18n.t("core.utilities:tooltip.lastUpdateLabel", "Last Update:") + '</label>' +
													'<span>' + cost.lastUpdateBy.name + ' - ' + formatDate(cost.lastUpdateOn, "dd.MM.yyyy") + '</span>' +
												'</li>' +
												'<li>' +
													'<label>' + i18n.t("statusLabel", "Status:") + '</label>' +
													'<span>' + cost.status.name + '</span>' +
												'</li>' +										
												'<li>' +
													'<label>' + i18n.t("currencyLabel", "Currency:") + '</label>' +
													'<span>' + cost.currency.currencyERSymbol + ' - ' + cost.currency.currencyCode + '</span>' +
												'</li>' +
												'<li>' +
													'<label>' + i18n.t("jobLabel", "Job:") + '</label>' +
													'<span>' + cost.job.jobno + '</span>' +
												'</li>' +								
											'</ol>' +
										'</div>' +
									'</fieldset>' +
									'<table class="default2" summary="This table displays the job costing items and their cost">' +
										'<thead>' +
											'<tr>' +
												'<td colspan="4">' +
													i18n.t("core.utilities:tooltip.jobCostingItems", "Job Costing Items") + ' (' + cost.items.length + ') - ' +
													'<a href="#" class="mainlink" id="showall" onclick=" toggleCostSummary(\'jccosts\', this.id, 0); return false; ">' + i18n.t("core.utilities:tooltip.expandAll", "Expand All") + '</a> | ' +
													'<a href="#" class="mainlink" id="hideall" onclick=" toggleCostSummary(\'jccosts\', this.id, 0); return false; ">' + i18n.t("core.utilities:tooltip.collapseAll", "Collapse All") + '</a>' +
												'</td>' +
											'</tr>' +
											'<tr>' +
												'<th scope="col">' + i18n.t("itemNo", "Item No") + '</th>' +
												'<th scope="col">' + i18n.t("modelName", "Model Name") + '</th>' +
												'<th scope="col">' + i18n.t("calType", "Caltype") + '</th>' +
												'<th scope="col" class="text-right">' + i18n.t("cost", "Cost") + '</th>' +
											'</tr>' +
										'</thead>' +
										'<tfoot>' +
											'<tr>' +
												'<td colspan="4">&nbsp;</td>' +
											'</tr>' +
										'</tfoot>' +
										'<tbody>';
											$j.each(cost.items, function(i)
											{										
												// assign model object
												var jobitem = cost.items[i].jobItem;
												var inst = cost.items[i].jobItem.inst;
												content += 	'<tr style=" background: ' + jobitem.calType.serviceType.displayColour + '; ">' +
																'<td class="item">' + 
																	jobitem.itemNo +
																	'<a href="#" id="show' + cost.items[i].itemno + '" onclick=" toggleCostSummary(\'jccosts\', this.id, ' + cost.items[i].itemno + '); return false; ">' +
																		'<img src="img/icons/showinfo.png" width="16" height="16" class="img_marg_bot" alt="' + i18n.t("showCostBreakdown", "Show cost breakdown") + '" title="' + i18n.t("showCostBreakdown", "Show cost breakdown") + '" />' +
																	'</a>' +
																'</td>' +
																'<td>' + inst.definitiveInstrument + '</td>' +
																'<td>' + jobitem.calType.serviceType.shortName + '</td>' +
																'<td class="text-right">' + cost.currency.currencyERSymbol + '' + cost.items[i].finalCost.toFixed(2) + '</td>' +
															'</tr>';
												if (cost.items[i].calibrationCost)
												{
													if (cost.items[i].calibrationCost.active == true)
													{
														content += 	'<tr class="jccosts' + cost.items[i].itemno + ' hid">' +
																		'<td>&nbsp;</td>' +
																		'<td colspan="2">' + cost.items[i].calibrationCost.costType.name + '</td>' +
																		'<td class="text-right">' + cost.currency.currencyERSymbol + '' + cost.items[i].calibrationCost.finalCost.toFixed(2) + '</td>' +
																	'</tr>';
													}												
												}
												if (cost.items[i].adjustmentCost)
												{
													if (cost.items[i].adjustmentCost.active == true)
													{
														content += 	'<tr class="jccosts' + cost.items[i].itemno + ' hid">' +
																		'<td>&nbsp;</td>' +
																		'<td colspan="2">' + cost.items[i].adjustmentCost.costType.name + '</td>' +
																		'<td class="text-right">' + cost.currency.currencyERSymbol + '' + cost.items[i].adjustmentCost.finalCost.toFixed(2) + '</td>' +
																	'</tr>';
													}												
												}
												if (cost.items[i].repairCost)
												{
													if (cost.items[i].repairCost.active == true)
													{
														content += 	'<tr class="jccosts' + cost.items[i].itemno + ' hid">' +
																		'<td>&nbsp;</td>' +
																		'<td colspan="2">' + cost.items[i].repairCost.costType.name + '</td>' +
																		'<td class="text-right">' + cost.currency.currencyERSymbol + '' + cost.items[i].repairCost.finalCost.toFixed(2) + '</td>' +
																	'</tr>';
													}												
												}
												if (cost.items[i].salesCost)
												{
													if (cost.items[i].salesCost.active == true)
													{
														content += 	'<tr class="jccosts' + cost.items[i].itemno + ' hid">' +
																		'<td>&nbsp;</td>' +
																		'<td colspan="2">' + cost.items[i].salesCost.costType.name + '</td>' +
																		'<td class="text-right">' + cost.currency.currencyERSymbol + '' + cost.items[i].salesCost.finalCost.toFixed(2) + '</td>' +
																	'</tr>';
													}												
												}
											});
						content = content +	'<tr>' +
												'<td colspan="3" class="text-right bold">' + i18n.t("core.utilities:tooltip.totalCostLabel", "Total Cost:") + '</td>' +
												'<td class="text-right bold">' + cost.currency.currencyERSymbol + '' + cost.totalCost.toFixed(2) + '</td>' +
											'</tr>' +
											'<tr>' +
												'<td colspan="3" class="text-right bold">' + i18n.t("core.utilities:tooltip.carriageLabel", "Carriage:") + '</td>' +
												'<td class="text-right bold">' + cost.currency.currencyERSymbol + '' + cost.carriage.toFixed(2) + '</td>' +
											'</tr>' +
											'<tr>' +
												'<td colspan="3" class="text-right bold">(' + cost.vatRate + '%) ' + i18n.t("vatLabel", "VAT:") + '</td>' +
												'<td class="text-right bold">' + cost.currency.currencyERSymbol + '' + cost.vatValue.toFixed(2) + '</td>' +
											'</tr>' +
											'<tr>' +
												'<td colspan="3" class="text-right bold">' + i18n.t("core.utilities:tooltip.netTotalLabel", "Net Total:") + '</td>' +
												'<td class="text-right bold">' + cost.currency.currencyERSymbol + '' + cost.finalCost.toFixed(2) + '</td>' +
											'</tr>' +
										'</tbody>' +													
									'</table>' + 
								'</div>' +
								// end of job costing summary tab
								// job costing contact summary tab
								'<div id="ttjc_contactsummary-tab" class="hid">' +
									'<fieldset class="newtooltip">' +
										'<ol>' +																				
											'<li>' +
												'<label' + i18n.t("companyLabel", "Company:") + '</label>' +
												'<span>' + cost.contact.sub.comp.coname + '</span>' +
											'</li>' +										
											'<li>' +
												'<label>' + i18n.t("subdvisionLabel" , "Subdivison:") + '</label>' +
												'<span>' + cost.contact.sub.subname + '</span>' +
											'</li>' +
											'<li>' +
												'<label>' + i18n.t("contactLabel" , "Contact:") + '</label>' +
												'<span>' + cost.contact.name + '</span>' +
											'</li>' +
											'<li>' +
												'<label>' + i18n.t("emailLabel" , "Email:") + '</label>' +
												'<span><a href="mailto:' + cost.contact.email + '" class="mainlink">' + cost.contact.email + '</a></span>' +
											'</li>' +
											'<li>' +
												'<label>' + i18n.t("telephoneLabel" , "Telephone:") + '</label>' +
												'<span>' + cost.contact.telephone + '</span>' +
											'</li>' +																		
										'</ol>' +
									'</fieldset>' +
								'</div>' +
								// end of job costing contact summary tab
							'</div>' +
							// end of tp quote tab
						'</div>';
						// end of main div
	
		$j('#JT_copy').append(content);
		
		/**
		 * set hover functionality of pop up.  ON mouse over do nothing and on mouse out close the pop up.
		 */ 
		$j('#JT').hover(function()	{
										mouseOverSignal = true;
									},
						function()	{
										$j('#JT').remove();
										mouseOverSignal = false; 
										return false;
									});	
	}							
}


/**
 * this method retrieves quote information for the id provided.
 * this information is then appended to the tooltip pop up.
 * 
 * @param {Integer} id is the id of the quotation for which information is required
 */
function getQuotationInfo(id)
{	
	// add submenu elements to array
	Elements1.push(	{ anchor: 'ttq_q-link', block: 'ttq_q-tab' });
					
	// add submenu elements to array
	Elements2.push(	{ anchor: 'ttq_items-link', block: 'ttq_items-tab'},
					{ anchor: 'ttq_summary-link', block: 'ttq_summary-tab' } );

	// call quotation dwr function using the id
	quotationservice.findQuotationDWR(id,
	{
		callback:function(result)
		{
			// quote retrieval successful?
			if (result.success)
			{
				createQuotationContent(result.results);
			}
			else
			{
				$j.prompt(result.message);
			}
		}
	});
		
	$j('div').remove('.JT_loader');
}

/**
 * this is the callback method for function getQuotationInfo()
 * 
 * @param {Object} quote is an object containing all quote information
 */
function createQuotationContent(quote)
{
	// variables for optional content
	var regDate = '-';
	var dueDate = '-';
	
	if (quote.regdate)
	{
		regDate = quote.formattedRegDate;
	}
	if (quote.duedate)
	{
		dueDate = quote.duedate;
	}

	var content = 	// main div
					'<div id="quoteTT">' +	
						// main navigation bar
						'<div class="slatenav-main">' +
							'<ul>' +
								'<li><a href="#" id="ttq_q-link" onmouseover=" switchMenuFocus(Elements1, \'ttq_q-tab\', false); return false; " class="selected">Quote</a></li>' +
							'</ul>' +
						'</div>' +
						'<div id="ttq_q-tab" class="vis clearL">' +													
							// quote sub navigation bar
							'<div class="slatenav-sub">' +
								'<ul>' +
									'<li><a href="#" onmouseover=" switchMenuFocus(Elements2, \'ttq_items-tab\', false); return false; " id="ttq_items-link" class="selected">' + i18n.t("items", "Items") + ' ';
			if (quote.quotationitems != null) {
				content += '(' + quote.quotationitems.length + ')';
			}
				content += '</a></li>' +
									'<li><a href="#" onmouseover=" switchMenuFocus(Elements2, \'ttq_summary-tab\', false); return false; " id="ttq_summary-link">' + i18n.t("core.utilities:tooltip.summary", "Summary") + '</a></li>' +							
								'</ul>' +
							'</div>' +
							// div containing tp quote items
							'<div id="ttq_items-tab">' +
								'<table class="default2 quoteTooltip" summary="This table displays the quote items">' +
									'<thead>' +										
										'<tr>' +
											'<th class="itemno" scope="col">' + i18n.t("item", "Item") + '</th>' +
											'<th class="desc" scope="col">' + i18n.t("description", "Description") + '</th>' +
											'<th class="refno" scope="col">' + i18n.t("core.utilities:tooltip.referenceNo", "Reference No") + '</th>' +
											'<th class="caltype" scope="col">' + i18n.t("calType", "Cal Type") + '</th>' +
											'<th class="unitcost" scope="col">' + i18n.t("core.utilities:tooltip.unitCost", "Unit Cost") + '</th>' +
											'<th class="qty" scope="col">' + i18n.t("qty", "Qty") + '</th>' +
											'<th class="cost" scope="col">' + i18n.t("core.utilities:tooltip.totalCost", "Total Cost") + '</th>' +
										'</tr>' +
									'</thead>' +									
									'<tbody>';
										// quote has headings
										// (added check for null quoteheadings as not returned for performance reasons 2017-02-02 GB)
										if ((quote.quoteheadings != null) && (quote.quoteheadings.length > 0))
										{
											// check all quote headings
											$j.each(quote.quoteheadings, function(x, heading)
											{
												// heading has quote items
												if (heading.quoteitems.length > 0)
												{
													// set cal type variable
													var caltypeid = 0;
													// check all quote items
													$j.each(heading.quoteitems, function(i, item)
													{	
														if (item.partOfBaseUnit != true)
														{
															// check calibration type
															if (caltypeid != item.caltype.calTypeId)
															{
																content += 	'<tr>' +
																				'<td colspan="7" class="tooltipQHeading">' + heading.headingName + ' - ' + item.caltype.serviceType.longName + '</td>' +
																			'</tr>';
															}														
															content += 	'<tr style=" background: ' + item.caltype.displayColour + '; ">' +
																			'<td class="itemno">' + 
																				item.itemno +
																			'</td>' +
																			'<td class="desc">';
																				if (item.inst)
																				{
																					content +=	item.inst.definitiveInstrument;
																				}
																				else
																				{
																					content +=	item.model.definitiveModel;
																				} 
																content +=	'</td>' +
																			'<td class="refno">' + item.referenceNo + '</td>' +
																			'<td class="caltype">' + item.caltype.serviceType.shortName + '</td>' +
																			'<td class="unitcost">' + quote.currency.currencyERSymbol + '' + item.unitCost.toFixed(2) + '</td>' +
																			'<td class="qty">' + item.quantity + '</td>' +
																			'<td class="cost">' + quote.currency.currencyERSymbol + '' + item.finalCost.toFixed(2) + '</td>' +
																		'</tr>';
															if (item.modules.length > 0)
															{
																// check all quote item modules
																$j.each(item.modules, function(z, mod)
																{														
																	content += 	'<tr height="16" style=" height: 14px; background: ' + mod.caltype.serviceType.displayColour + '; ">' +
																					'<td class="itemno">' + 
																						'<img src="img/icons/arrow_merge_small.png" height="12" width="12" alt="' + i18n.t("core.utilities:tooltip.partOfBaseUnit", "Part of base unit") + '" title="' + i18n.t("core.utilities:tooltip.partOfBaseUnit", "Part of base unit") + '" />' +
																					'</td>' +
																					'<td class="desc">';  
																						if (mod.inst)
																						{
																							content +=	mod.inst.definitiveInstrument;
																						}
																						else
																						{
																							content +=	mod.model.definitiveModel;
																						} 
																		content +=	'</td>' +
																					'<td class="refno">' + mod.referenceNo + '</td>' +
																					'<td class="caltype">' + mod.caltype.serviceType.shortName + '</td>' +																					
																					'<td class="cost">' + quote.currency.currencyERSymbol + '' + mod.finalCost.toFixed(2) + '</td>' +
																				'</tr>';
																});
															}
														}
														caltypeid = item.caltype.calTypeId;
													});
												}
												else
												{													
													content += 	'<tr>' +
																	'<td colspan="7" class="tooltipQHeading">' + heading.headingName + '</td>' +
																'</tr>' +
																'<tr>' +
																	'<td colspan="7" class="text-center bold">' + i18n.t("core.utilities:tolltip.noItemsAddedToThisQuoteHeading", "No items have been added to this quote heading") + '</td>' +
																'</tr>';
												}
											});										
										}
										else
										{
											content += 	'<tr>' +
															'<td colspan="7" class="text-center bold">' + i18n.t("core.utilities:tolltip.noItemsAddedToThisQuotation", "No items have been added to this quotation") + '</td>' +
														'</tr>';
										}
							content +=	'<tr>' +
											'<td colspan="6" class="text-right bold">' + i18n.t("totalLabel", "Total:") + '</td>' +
											'<td class="text-right bold">' + quote.currency.currencyERSymbol + '' + quote.baseFinalCostFormatted + '</td>' +
										'</tr>' +																		
									'</tbody>' +													
								'</table>' +
							'</div>' +
							// quote summary tab
							'<div id="ttq_summary-tab" class="hid">' +
								'<fieldset class="newtooltip">' +
									'<ol>' +
										'<li>' +
											'<label>' + i18n.t("quoteNoLabel", "Quote No:") + '</label>' +
											'<span>' + quote.qno + '</span>' +
										'</li>' +
										'<li>' +
											'<label>' + i18n.t("statusLabel", "Status:") + '</label>' +
											'<span>' + quote.quotestatus.name + '</span>' +
										'</li>' +	
										'<li>' +
											'<label>' + i18n.t("companyLabel", "Company:") + '</label>' +
											'<span>' + quote.contact.sub.comp.coname + '</span>' +
										'</li>' +										
										'<li>' +
											'<label>' + i18n.t("subdvisionLabel", "Subdivison:") + '</label>' +
											'<span>' + quote.contact.sub.subname + '</span>' +
										'</li>' +
										'<li>' +
											'<label>' + i18n.t("contactLabel", "Contact:") + '</label>' +
											'<span>' + quote.contact.name + '</span>' +
										'</li>' +
										'<li>' +
											'<label>' + i18n.t("telephoneLabel", "Telephone:") + '</label>' +
											'<span>' + quote.contact.telephone + '</span>' +
										'</li>' +
										'<li>' +
											'<label>' + i18n.t("emailLabel" , "Email:") + '</label>' +
											'<span><a href="mailto:' + quote.contact.email + '" class="mainlink">' + quote.contact.email + '</a></span>' +
										'</li>' +																			
										'<li>' +
											'<label>' + i18n.t("core.utilites:tooltip.sourceByLabel", "Sourced By:") + '</label>' +
											'<span>';
												if (quote.sourcedBy)
												{
													content += quote.sourcedBy.name;
												}											
								content += 	'</span>' +
										'</li>' +							
										'<li>' +
											'<label>' + i18n.t("core.utilites:tooltip.regDateLabel", "Reg Date:") + '</label>' +
											'<span>' + regDate + '</span>' +
										'</li>' +
										'<li>' +
											'<label>' + i18n.t("dueDateLabel", "Due Date:") + '</label>' +
											'<span>' + dueDate + '</span>' +
										'</li>' +
									'</ol>' +
								'</fieldset>' +
							'</div>' +
							// end of quote summary tab
						'</div>' +
						// end of tp quote tab
					'</div>';
					// end of main div

	$j('#JT_copy').append(content);
	
	/**
	 * set hover functionality of pop up.  ON mouse over do nothing and on mouse out close the pop up.
	 */ 
	$j('#JT').hover(function()	{
									mouseOverSignal = true;
								},
					function()	{
									$j('#JT').remove();
									mouseOverSignal = false; 
									return false;
								});		
}











/**
 * this method retrieves third party quote information for the id provided.
 * this information is then appended to the tooltip pop up.
 * 
 * @param {Integer} id is the id of the third party for which information is required
 */
function getTPQuotationInfo(id)
{	
	// add submenu elements to array
	Elements1.push(	{ anchor: 'tttq_tpq-link', block: 'tttq_tpq-tab' });
					
	// add submenu elements to array
	Elements2.push(	{ anchor: 'tttq_summary-link', block: 'tttq_summary-tab' },
					{ anchor: 'tttq_items-link', block: 'tttq_items-tab'} );

	// call third party dwr function using the id
	tpquotationservice.findEagerTPQuotation(id,
	{
		callback:function(tpquote)
		{
			createTPQuotationContent(tpquote);	
		}
	});
		
	$j('div').remove('.JT_loader');
}

/**
 * this is the callback method for function getTPQuotationInfo()
 * 
 * @param {Object} tpquote is an object containing all third party quote information
 */
function createTPQuotationContent(tpquote)
{	
	// variables for optional content
	var regDate = '-- ' + i18n.t("core.utilities:tooltip.notSet", "Not Set") + ' --';
	
	if (tpquote.regdate)
	{
		regDate = tpquote.formattedRegDate;
	}

	var content = 	// main div
					'<div id="tpquoteTT">' +	
						// main navigation bar
						'<div class="slatenav-main">' +
							'<ul>' +
								'<li><a href="#" id="tttq_tpq-link" onmouseover=" switchMenuFocus(Elements1, \'tttq_tpq-tab\', false); return false; " class="selected">' + i18n.t("core.utilities:tooltip.tpQuote", "TP Quote") + '</a></li>' +
							'</ul>' +
						'</div>' +
						'<div id="ttj_job-tab" class="vis clearL">' +													
							// job sub navigation bar
							'<div class="slatenav-sub">' +
								'<ul>' +
									'<li><a href="#" onmouseover=" switchMenuFocus(Elements2, \'tttq_summary-tab\', false); return false; " id="tttq_summary-link" class="selected">' + i18n.t("core.utilities:tooltip.summary", "Summary") + '</a></li>' +
									'<li><a href="#" onmouseover=" switchMenuFocus(Elements2, \'tttq_items-tab\', false); return false; " id="tttq_items-link">' + i18n.t("items", "Items") + '</a></li>' +							
								'</ul>' +
							'</div>' +
							// job item summary tab
							'<div id="tttq_summary-tab">' +
								'<fieldset class="newtooltip">' +
									'<ol>' +
										'<li>' +
											'<label>' + i18n.t("quoteNoLabel", "Quote No:") + '</label>' +
											'<span>' + tpquote.qno + '</span>' +
										'</li>' +									
										'<li>' +
											'<label>' + i18n.t("companyLabel" , "Company:") + '</label>' +
											'<span>' + tpquote.contact.sub.comp.coname + '</span>' +
										'</li>' +										
										'<li>' +
											'<label>' + i18n.t("subdvisionLabel" , "Subdivison:") + '</label>' +
											'<span>' + tpquote.contact.sub.subname + '</span>' +
										'</li>' +
										'<li>' +
											'<label>' + i18n.t("contactLabel" , "Contact:") + '</label>' +
											'<span>' + tpquote.contact.name + '</span>' +
										'</li>' +
										'<li>' +
											'<label>' + i18n.t("telephoneLabel" , "Telephone:") + '</label>' +
											'<span>' + tpquote.contact.telephone + '</span>' +
										'</li>' +
										'<li>' +
											'<label>' + i18n.t("emailLabel" , "Email:") + '</label>' +
											'<span><a href="mailto:' + tpquote.contact.email + '" class="mainlink">' + tpquote.contact.email + '</a></span>' +
										'</li>' +
										'<li>' +
											'<label>' + i18n.t("statusLabel", "Status:") + '</label>' +
											'<span>' + tpquote.status.name + '</span>' +
										'</li>' +																		
										'<li>' +
											'<label>' + i18n.t("core.utilities:tooltip.validityLabel", "Validity:") + '</label>' +
											'<span>' + tpquote.duration + ' days</span>' +
										'</li>' +
										'<li>' +
											'<label>' + i18n.t("core.utilities:tooltip.regByLabel", "Reg By:") + '</label>' +
											'<span>' + tpquote.createdBy.name + '</span>' +
										'</li>' +							
										'<li>' +
											'<label>' + i18n.t("core.utilities:tooltip.regDateLabel", "Reg Date:") + '</label>' +
											'<span>' + regDate + '</span>' +
										'</li>' +										
									'</ol>' +
								'</fieldset>' +
							'</div>' +
							// end of tp quote summary tab
							// div containing tp quote items
							'<div id="tttq_items-tab" class="hid">' +
								'<table class="default2" summary="This table displays the third party quote items">' +
									'<thead>' +
										'<tr>' +
											'<td colspan="4">' +
												i18n.t("core.utilities:tooltip.tpQuoteItems", "Third Party Quote Items") + ' (' +  tpquote.items.length + ') - ' +
												'<a href="#" class="mainlink" id="showall" onclick=" toggleCostSummary(\'tpqcosts\', this.id, 0); return false; ">' + i18n.t("core.utilities:tooltip.expandAll", "Expand All") + '</a> | ' +
												'<a href="#" class="mainlink" id="hideall" onclick=" toggleCostSummary(\'tpqcosts\', this.id, 0); return false; ">' + i18n.t("core.utilities:tooltip.collapseAll", "Collapse All") + '</a>' +
											'</td>' +
										'</tr>' +
										'<tr>' +
											'<th scope="col">' + i18n.t("itemNo", "Item No") + '</th>' +
											'<th scope="col">' + i18n.t("modelName", "Model Name") + '</th>' +
											'<th scope="col">' + i18n.t("calType", "Cal Type") + '</th>' +
											'<th scope="col" class="text-right">' + i18n.t("cost", "Cost") + '</th>' +
										'</tr>' +
									'</thead>' +
									'<tfoot>' +
										'<tr>' +
											'<td colspan="4">&nbsp;</td>' +
										'</tr>' +
									'</tfoot>' +
									'<tbody>';
										$j.each(tpquote.items, function(i)
										{										
											// assign model object
											var model = tpquote.items[i].model;
											content = content + '<tr style=" background: ' + tpquote.items[i].caltype.displayColour + '; ">' +
																	'<td class="item">' + 
																		tpquote.items[i].itemno +
																		'<a href="#" id="show' + tpquote.items[i].itemno + '" onclick=" toggleCostSummary(\'tpqcosts\', this.id, ' + tpquote.items[i].itemno + '); return false; ">' +
																			'<img src="img/icons/showinfo.png" width="16" height="16" class="img_marg_bot" alt="' + i18n.t("showCostBreakdown", "Show cost breakdown") + '" title="' + i18n.t("showCostBreakdown", "Show cost breakdown") + '" />' +
																		'</a>' +
																	'</td>' +
																	'<td>' + model.definitiveModel + '</td>' +
																	'<td>' + tpquote.items[i].caltype.shortName + '</td>' +
																	'<td class="text-right">' + tpquote.currency.currencyERSymbol + '' + tpquote.items[i].finalCost.toFixed(2) + '</td>' +
																'</tr>';
											if (tpquote.items[i].calibrationCost)
											{
												if (tpquote.items[i].calibrationCost.active == true)
												{
													content = content + '<tr class="tpqcosts' + tpquote.items[i].itemno + ' hid">' +
																			'<td>&nbsp;</td>' +
																			'<td colspan="2">' + tpquote.items[i].calibrationCost.costType.name + '</td>' +
																			'<td class="text-right">' + tpquote.currency.currencyERSymbol + '' + tpquote.items[i].calibrationCost.finalCost.toFixed(2) + '</td>' +
																		'</tr>';
												}												
											}
											if (tpquote.items[i].adjustmentCost)
											{
												if (tpquote.items[i].adjustmentCost.active == true)
												{
													content = content + '<tr class="tpqcosts' + tpquote.items[i].itemno + ' hid">' +
																			'<td>&nbsp;</td>' +
																			'<td colspan="2">' + tpquote.items[i].adjustmentCost.costType.name + '</td>' +
																			'<td class="text-right">' + tpquote.currency.currencyERSymbol + '' + tpquote.items[i].adjustmentCost.finalCost.toFixed(2) + '</td>' +
																		'</tr>';
												}												
											}
											if (tpquote.items[i].repairCost)
											{
												if (tpquote.items[i].repairCost.active == true)
												{
													content = content + '<tr class="tpqcosts' + tpquote.items[i].itemno + ' hid">' +
																			'<td>&nbsp;</td>' +
																			'<td colspan="2">' + tpquote.items[i].repairCost.costType.name + '</td>' +
																			'<td class="text-right">' + tpquote.currency.currencyERSymbol + '' + tpquote.items[i].repairCost.finalCost.toFixed(2) + '</td>' +
																		'</tr>';
												}												
											}
											if (tpquote.items[i].salesCost)
											{
												if (tpquote.items[i].salesCost.active == true)
												{
													content = content + '<tr class="tpqcosts' + tpquote.items[i].itemno + ' hid">' +
																			'<td>&nbsp;</td>' +
																			'<td colspan="2">' + tpquote.items[i].salesCost.costType.name + '</td>' +
																			'<td class="text-right">' + tpquote.currency.currencyERSymbol + '' + tpquote.items[i].salesCost.finalCost.toFixed(2) + '</td>' +
																		'</tr>';
												}												
											}
										});
					content = content +	'<tr>' +
											'<td colspan="3" class="text-right bold">Total:</td>' +
											'<td class="text-right bold">' + tpquote.currency.currencyERSymbol + '' + tpquote.baseFinalCostFormatted + '</td>' +
										'</tr>' +																		
									'</tbody>' +													
								'</table>' +
							'</div>' +
						'</div>' +
						// end of tp quote tab
					'</div>';
					// end of main div

	$j('#JT_copy').append(content);
	
	/**
	 * set hover functionality of pop up.  ON mouse over do nothing and on mouse out close the pop up.
	 */ 
	$j('#JT').hover(function()	{
									mouseOverSignal = true;
								},
					function()	{
									$j('#JT').remove();
									mouseOverSignal = false; 
									return false;
								});								
}

/**
 * this method toggles the visibility of quote item cost summaries
 * 
 * @param {String} classname the string value of class name
 * @param {String} anchorId the id of the anchor being clicked
 * @param {Integer} itemid the id of the quote item
 */
function toggleCostSummary(classname, anchorId, itemid)
{
	// hide the quote item costs?
	if (anchorId.indexOf('hide') != -1)
	{
		// show/hide all anchor?
		if (anchorId.indexOf('all') != -1)
		{
			// hide all cost summary rows
			$j('tr[class^="' + classname + '"]').removeClass('vis').addClass('hid');
			// change anchor id and image on all rows
			$j('td.item a').each(function(i)
			{
				// get current id of anchor
				var currentid = $j(this).attr('id');
				// get itemid from anchor id
				var itemno = currentid.substring(4, currentid.length);
				// remove old image, append new image and change id of anchor
				$j(this).empty()
						.attr('id', 'show' + itemno)
						.append('<img src="img/icons/showinfo.png" width="16" height="16" class="img_marg_bot" alt="' + i18n.t("showCostBreakdown", "Show cost breakdown") + '" title="' + i18n.t("showCostBreakdown", "Show cost breakdown") + '" />');
			});
		}
		else
		{
			// hide cost summary
			$j('tr').each(function(i)
			{
				if ($j(this).hasClass('' + classname + itemid + ''))
				{
					$j(this).removeClass('vis').addClass('hid');
				}
			});
			// remove old image, append new image and change id of anchor
			$j('#' + anchorId).empty()
								.attr('id', 'show' + itemid)
								.append('<img src="img/icons/showinfo.png" width="16" height="16" class="img_marg_bot" alt="' + i18n.t("showCostBreakdown", "Show cost breakdown") + '" title="' + i18n.t("showCostBreakdown", "Show cost breakdown") + '" />');
		}
		
	}
	else
	{
		// show/hide all anchor?
		if (anchorId.indexOf('all') != -1) 
		{
			// show all cost summary rows, but not notes
			$j('tr[class^="' + classname + '"]:not([id^="tpqinote"])').removeClass('hid').addClass('vis');
			// change anchor id and image on all rows
			$j('td.item a').each(function(i)
			{
				// get current id of anchor
				var currentid = $j(this).attr('id');
				// get itemid from anchor id
				var itemno = currentid.substring(4, currentid.length);
				// remove old image, append new image and change id of anchor
				$j(this).empty()
						.attr('id', 'hide' + itemno)
						.append('<img src="img/icons/hideinfo.png" width="16" height="16" class="img_marg_bot" alt="' + i18n.t("hideCostBreakdown", "Hide cost breakdown") + '" title="' + i18n.t("hideCostBreakdown", "Hide cost breakdown") + '" />');
			});
		}
		else
		{
			// show cost summary
			$j('tr').each(function(i)
			{
				if ($j(this).hasClass('' + classname + itemid + ''))
				{
					$j(this).removeClass('hid').addClass('vis');
				}
			});
			// remove old image, append new image and change id of anchor
			$j('#' + anchorId).empty()
								.attr('id', 'hide' + itemid)
								.append('<img src="img/icons/hideinfo.png" width="16" height="16" class="img_marg_bot" alt="' + i18n.t("hideCostBreakdown", "Hide cost breakdown") + '" title="' + i18n.t("hideCostBreakdown", "Hide cost breakdown") + '" />');
		}		
	}	
}


/**
 * this method retrieves jobs which are linked to the address id provided and are not in a stategroup.
 * PLEASE NOT THAT THE STATEGROUP ARRAY HAS TO BE CONSTRUCTED ON THE PAGE FOR THIS TO WORK!!!!
 * 
 * @param {Integer} id is the id of the address to find jobs for
 */
function getAddrJobsInfo(id)
{	
	// add submenu elements to array
	Elements1.push(	{ anchor: 'tttq_tpq-link', block: 'tttq_tpq-tab' });

	// call dwr service using the id provided and stategroup array
	jobservice.getJobsAndItemsNotInStateGroupForAddress(stateGroupArray, id,
	{
		callback:function(result)
		{
			// variable to hold job number for checking purposes
			var jncheck = '';
			// loop through results
			$j.each(result.results, function(i)
			{
				// assign item object to variable
				var item = result.results[i];
				// check for new job number
				if (item.jobNo != jncheck)
				{
					// add submenu elements to array
					Elements2.push(	{ anchor: 'a' + item.jobNo.replace(/\//g, '') + '-link', block: 't' + item.jobNo.replace(/\//g, '') + '-tab' } );	
				}
				// add item job number to check job number variable
				jncheck = item.jobNo;
			});
			createAddrJobsContent(result);	
		}
	});
		
	$j('div').remove('.JT_loader');
}



/**
 * this is the callback method for function getAddrJobsInfo()
 * 
 * @param {Object} job is an object containing all jobs information
 */
function createAddrJobsContent(result)
{	
	// variable holding main content
	var content = 	// main div
					'<div id="addrJobsTT">' +	
						// main navigation bar
						'<div class="slatenav-main">' +
							'<ul>' +
								'<li><a href="#" id="ttaj_job-link" onmouseover=" switchMenuFocus(Elements1, \'ttaj_job-tab\', false); return false; " class="selected">' + i18n.t("core.utilities:tooltip.jobs", "Jobs") + '</a></li>' +
							'</ul>' +
						'</div>' +
						'<div id="ttaj_job-tab" class="vis clearL">' +													
							// job sub navigation bar
							'<div class="slatenav-sub">' +
								'<ul>';
									// initialise tabbed content variable
									var tabbedContent = '';
									// variable to hold job number for checking purposes
									var jncheck = '';
									// loop through results
									$j.each(result.results, function(i)
									{
										// assign item object to variable
										var item = result.results[i];
										// check for new job number
										if (item.jobNo != jncheck)
										{
											if (i > 0)
											{
												tabbedContent += 	'</tbody>' +
																'</table>' +
															'</div>';
											}
											content += '<li><a href="#" onmouseover=" switchMenuFocus(Elements2, \'t' + item.jobNo.replace(/\//g, '') + '-tab\', false); return false; " id="a' + item.jobNo.replace(/\//g, '') + '-link" ';
											tabbedContent += '<div id="t' + item.jobNo.replace(/\//g, '') + '-tab" '; 
											if (i == 0)
											{
												content += 'class="selected" ';
												tabbedContent += 'class="vis">';
											}
											else
											{
												tabbedContent += 'class="hid">';
											}
											content += '>' + item.jobNo + '</a></li>';
											tabbedContent += 	'<table class="default2" summary="">' +
																	'<thead>' +
																		'<tr>' +
																			'<th scope="col">' + i18n.t("itemNo", "Item No") + '</th>' +
																			'<th scope="col">' + i18n.t("modelName", "Instrument Model Name") + '</th>' +
																			'<th scope="col">' + i18n.t("core.utilities:tooltip.itemDueDate", "Item Due Date") + '</th>' +
																			'<th scope="col"">' + i18n.t("status", "Status") + '</th>' +																			
																		'</tr>' +
																	'</thead>' +
																	'<tfoot>' +
																		'<tr>' +
																			'<td colspan="6">&nbsp;</td>' +
																		'</tr>' +
																	'</tfoot>' +
																	'<tbody>';
										}
										tabbedContent += 	'<tr>' +
																'<td>' + item.itemNo + '</td>' +																
																'<td>' + item.inst.definitiveInstrument + '</td>' +
																'<td>' + formatDate(item.itemDueDate, 'dd.MM.yyyy') + '</td>' +
																'<td>' + item.state + '</td>' +
															'</tr>'; 
										// add item job number to check job number variable
										jncheck = item.jobNo;
									});
					content +=	'</ul>' +
							'</div>' +
							tabbedContent +
							'</tbody>' +
							'</table>' +
							'</div>' +										
						'</div>' +
						// end of job tab
					'</div>';
					// end of main div
	
	$j('#JT_copy').append(content);
	
	/**
	 * set hover functionality of pop up.  ON mouse over do nothing and on mouse out close the pop up.
	 */ 
	$j('#JT').hover(function()	{
									mouseOverSignal = true;
								},
					function()	{
									$j('#JT').remove();
									mouseOverSignal = false; 
									return false;
								});								
}


/**
 * this method retrieves all job items that belong to a job item group
 * 
 * @param {Integer} id is the id of the group to find job items for
 */
function getJobItemGroupInfo(id)
{	
	// add submenu elements to array
	Elements1.push(	{ anchor: 'ttg-ji-link', block: 'ttg-ji-tab' },
					{ anchor: 'ttg-opt-link', block: 'ttg-opt-tab'} );
	
	// call dwr service using the id provided and stategroup array
	jobitemgroupservice.ajaxGetGroupWithItems(id,
	{
		callback:function(result)
		{
			if (result.success) {
				createJobItemGroupContent(result.results);
				$j('div').remove('.JT_loader');
			}
			else {
				$j.prompt(result.message);
				$j('div').remove('.JT_loader');
			}				
		}
	});
}

/**
 * this is the callback method for function getJobItemGroupInfo()
 * 
 * @param {Object} job is an object containing all jobs information
 */
function createJobItemGroupContent(group)
{	
	// add boolean field checkboxes for group
	var withCaseChecked = (group.withCase == true) ? ' checked="checked" ' : '';
	var calGroupChecked = (group.calibrationGroup == true) ? ' checked="checked" ' : '';
	var delGroupChecked = (group.deliveryGroup == true) ? ' checked="checked" ' : '';
	// variable holding main content
	var content = 	// main div
					'<div id="jobItemGroupTT">' +	
						// main navigation bar
						'<div class="slatenav-main">' +
							'<ul>' +
								'<li><a href="#" id="ttg-ji-link" onmouseover=" switchMenuFocus(Elements1, \'ttg-ji-tab\', false); return false; " class="selected">' + i18n.t("core.utilities:tooltip.groupJobItems", "Group Job Items") + '</a></li>' +
								'<li><a href="#" id="ttg-opt-link" onmouseover=" switchMenuFocus(Elements1, \'ttg-opt-tab\', false); return false; ">' + i18n.t("core.utilities:tooltip.groupOptions", "Group Options") + '</a></li>' +
							'</ul>' +
						'</div>' +						
						// div containing tp quote items
						'<div id="ttg-ji-tab">' +
							'<table class="default2" summary="This table displays all job items in group">' +
								'<thead>' +
									'<tr>' +
										'<td colspan="5">' +
											i18n.t("core.utilities:tooltip.groupJobItems", "Group Job Items") +
										'</td>' +
									'</tr>' +
									'<tr>' +
										'<th scope="col" class="">' + i18n.t("itemNo", "Item No") + '</th>' +
										'<th scope="col" class="">' + i18n.t("description", "Description") + '</th>' +
										'<th scope="col" class="">' + i18n.t("serialNo", "Serial No") + '</th>' +
										'<th scope="col" class="">' + i18n.t("plantNo", "Plant No") + '</th>' +
										'<th scope="col" class="">' + i18n.t("calType", "Cal Type") + '</th>' +
									'</tr>' +
								'</thead>' +
								'<tfoot>' +
									'<tr>' +
										'<td colspan="5">&nbsp;</td>' +
									'</tr>' +
								'</tfoot>' +
								'<tbody>';									
									$j.each(group.items, function(i)
									{						
										// assign ji to variable
										var ji = group.items[i];										
										// create table row content				
										content += 	'<tr>' +
														'<td class="">' + ji.itemNo + '</td>' +
														'<td class="">' + ji.inst.definitiveInstrument + '</td>' +
														'<td class="">' + ji.inst.serialno + '</td>' +
														'<td class="">' + ji.inst.plantno + '</td>' +
														'<td class="">' + ji.calType.shortName + '</td>' +
													'</tr>';
									});																						
					content += 	'</tbody>' +													
							'</table>' +
						'</div>' +
						// end of job items
						// job item summary tab
						'<div id="ttg-opt-tab" class="hid">' +
							'<fieldset class="newtooltip">' +
								'<ol>' +
									'<li>' +
										'<label>' + i18n.t("core.utilities:tooltip.groupCaseLabel", "Group Case:") + '</label>' +
										'<input type="checkbox" name="withCase" ' + withCaseChecked + ' onclick=" changeGroupField(this, ' + group.id + '); " />' +
									'</li>' +									
									'<li>' +
										'<label>' + i18n.t("core.utilities:tooltip.calGroupLabel", "Calibration Group:") + '</label>' +
										'<input type="checkbox" name="calGroup" ' + calGroupChecked + ' onclick=" changeGroupField(this, ' + group.id + '); " />' +
									'</li>' +
									'<li>' +
										'<label>' + i18n.t("core.utilities:tooltip.deliveryGroupLabel", "Delivery Group:") + '</label>' +
										'<input type="checkbox" name="delGroup" ' + delGroupChecked + ' onclick=" changeGroupField(this, ' + group.id + '); " />' +
									'</li>' +
								'</ol>' +
							'</fieldset>' +
						'</div>' +
						// end of group options
					'</div>';
					// end of main div
	
	$j('#JT_copy').append(content);
	
	/**
	 * set hover functionality of pop up.  ON mouse over do nothing and on mouse out close the pop up.
	 */ 
	$j('#JT').hover(function()	{
									mouseOverSignal = true;
								},
					function()	{
									$j('#JT').remove();
									mouseOverSignal = false; 
									return false;
								});							
}

/**
 * this method updates checkbox values for a group
 * 
 * @param {Object} checkbox the checkbox object
 * @param {Integer} groupId id of the current group
 */
function changeGroupField(checkbox, groupId)
{
	// get checkbox name
	var name = $j(checkbox).attr('name');
	// is checkbox checked?
	var val = $j(checkbox).is(':checked');
	// get the value of select
	var withCase = (name == 'withCase') ? val : null;
	var calGroup = (name == 'calGroup') ? val : null;
	var delGroup = (name == 'delGroup') ? val : null;
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/jobitemgroupservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to update group fields
			jobitemgroupservice.ajaxUpdateGroupFields(groupId, withCase, calGroup, delGroup, 
			{
				callback:function(result)
				{
					// method failed?
					if(!result.success)
					{
						$j.prompt(i18n.t("error", "Error:") + ' ' + result.message);
					}
				}
			});
		}
	});
}