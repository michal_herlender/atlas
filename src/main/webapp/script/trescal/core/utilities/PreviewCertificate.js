/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	Preview Certificate.js
*	DESCRIPTION		:	This file has a method which calls a dwr service to create a preview of an existing certificate or
*					:	preview a certificate for a calibration which has yet to have a certificate created.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	11/07/2011 - SH - Created File
*					:	30/10/2015 - Manage i18n
****************************************************************************************************************************************/

/**
 * this method calls a dwr service which creates a preview of a certificate based on the details of a completed calibration, if a certificate
 * id is passed then a preview of an existing certificate will be created.
 * 
 * @param {Object} link the anchor clicked to instantiate this method
 * @param {Integer} certId id of a certificate to be previewed
 * @param {Integer} calId id of a calibration which a certificate should be created and previewed for
 * @param {String} certTemplate the name of the certificate template to be used for the preview
 */
function createCertificatePreview(link, certId, calId, certTemplate)
{
	// add loading image after link
	$j(link).removeClass('previewCertificate').addClass('previewCertLoad').text(i18n.t("core.utilities:generatingPreview", "Generating Preview......"));
	// load javascript service file
	loadScript.loadScriptsDynamic(new Array('dwr/interface/certificateservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to create certificate preview
			certificateservice.createCertificatePreview(certId, calId, certTemplate,
			{
				callback:function(result)
				{
					// preview certificate successful?
					if (result.success || 1 == 1)
					{
						// get name of file to display
						var fileName = result.results;
						// reset link
						$j(link).removeClass('previewCertLoad').addClass('previewCertificate hid').text(i18n.t("core.utilities:certificatePreview", "Certificate Preview"));
						// add link
						$j(link).after(	'<a href="' + springUrl + '/temp/' + fileName + '" class="mainlink previewCertificate" target="_blank">' + i18n.t("core.utilities:viewGeneratedCertificate", "View Generated Certificate") + '</a>' +
										'<a href="#" class="mainlink resetPreviewCert" onclick=" event.cancelBubble = true; $j(this).prev().prev().removeClass(\'hid\').next().remove(); $j(this).remove(); return false; ">' + i18n.t("reset", "Reset") + '</a>');
					}
					else
					{
						// reset link
						$j(link).removeClass('previewCertLoad').addClass('previewCertificate').text(i18n.t("core.utilities:certificatePreview", "Certificate Preview"));
						// display error message to user
						$j.prompt(result.message);
					}
				}
			});
		}
	});
}