/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	Note.js
*	DESCRIPTION		:	This file has all functions connected with modifying note status's.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	Commenting of code.
*	KNOWN ISSUES	:	-
*	HISTORY			:	19/06/2007 - SH - Created File
*					: 	25/09/2015 - TProvost - Fix bug in addNoteBoxy function : check if notecommentdto is null
*					:	02/11/2015 - TProvost - Manage i18n  
*					:	10/11/2015 - TProvost - Remove the init function and load directly stylesheet
*                   :   2016-10-03 - Galen Beck - added support for addPrivate, addPublic defaults 
*                   :   2016-10-04 - Galen Beck - implemented proper escaping of HTML preset comments
****************************************************************************************************************************************/

// load jquery boxy javascript if not already
loadScript.loadScripts(new Array('script/thirdparty/jQuery/jquery.boxy.js'));

// load stylesheet using the dom if not already
loadScript.loadStyle('styles/jQuery/jquery-boxy.css', 'screen');

/**
 *	Create objects for note editing sections
 *	@param (string) name is the name of the section from which a note is being passed
 *	@param (string) publish is the name of the section to which a note should be published (ie public to private)
 *	@param (string) status is the name of the section to which a note should be passed when being deleted or re-activated
 */
var publicA = 	{	relname:	'publicA',
					publish:	'privateA',
					relstatus:	'publicD'
				};
					
var publicD	=	{	relname:	'publicD',
					relstatus:	'publicA'
				};
					
var privateA = 	{	relname: 	'privateA',
					publish:	'publicA',
					relstatus:	'privateD'
				};
					
var privateD =	{	relname: 	'privateD',
					relstatus:	'privateA'
				};

/**
 * this method creates a new jquery boxy pop up which contains form elements for adding a new note to the system,
 * note services are loaded if they have not been already along with the style sheet and javascript file for creating
 * a new jquery modal window.  Finally javascript is loaded to turn the textarea into a new wysiwyg text editor window.
 * 
 * Changed 2016-10-03 GB: Added options of having 'addPublic', 'addPrivate', while 'add' remains private
 * Changed 2016-10-04 GB: Changed to encode / decode preset comment text correctly, refactored DTOs to match. 
 * 
 * @param {String} action the type of action to perform (i.e. 'add' or 'edit', or 'addPublic', 'addPrivate')
 * @param {Boolean} inlinenote indicates if this is an inline note we are adding
 * @param {String} notetype the type of note we are adding
 * @param {Integer} notetypeid the id of the entity for which we are adding a note
 * @param {Integer} personid the id of the user adding the note
 * @param {Integer} noteId the id of note to be edited or 0 if new note
 * @param {Integer} colspan the number of columns to span when adding a new inline note
 */
function addNoteBoxy(action, inlinenote, notetype, notetypeid, personid, noteId, colspan)
{
	// variable to hold modal window content			
	var content = '';		
	// load the service javascript file if not already
	$j.ajax({
		url: "initializenoteboxy.htm",
		data: {
			noteType: notetype,
			noteId: noteId,
			entityId: notetypeid,
			privateOnlyNotes: privateOnlyNotes,
			inlineNote: inlinenote,
			colspan: colspan
		},
		async: true
	}).done(function(content) {
		// create new boxy pop up using note form content
		var noteBoxy = new Boxy(content, {title: i18n.t("core.utilities:note.addNote", "Add Note"), unloadOnHide: true});
		// adapt size of boxy (was 800 x 380 until 2016-05-07)
		noteBoxy.tween(800,600);
		// apply focus to note label field
		$j('input#noteLabel').focus();
		// Replaced jwysiwyg with jHtmlArea 2016-05-07 GB, see Cwms_Main.js for remarks												
		loadScript.loadStyle('script/thirdparty/jQuery/jHtmlArea/jHtmlArea.css', 'screen');
		loadScript.loadScriptsDynamic(new Array('script/thirdparty/jQuery/jHtmlArea/jHtmlArea-0.8.js'), true, {
			callback: function() {
				$j('textarea#noteBody').htmlarea();
			}
		});
	}).fail(function(error) {
		$j.prompt(error.message);
	});
}

/**
 * method to draw symbol box
 * 
 * @param {Object} this implementation of plugin
 */
function drawSymbolBox(link)
{
	// content for the symbol box that is created above the textarea
	var content = loadScript.masterSymbolBoxContent();
	// append the symbol boxy before the textarea and locate appropriately
	$j(link).parent().append(content).find('div.symbolBoxDiv').css({ position: 'absolute', top: '-1px', right: '18px'});
	// add click events to all table cells
	$j(link).parent().find('div.symbolBoxDiv table tbody td').click(function(){ addSymbol(this); return false; });
	// add click event to the first div that will close the symbol box
	$j(link).parent().find('div.symbolBoxDiv div:first').click(function(){ closeSymbolPlugin($j(this).parent()); return false; });
}

/**
 * this method adds a symbol to the textarea
 * 
 * @param {Object} tableCell the table cell containing symbol clicked
 */
function addSymbol(tableCell)
{
	// add the selected symbol to the textarea
	$j('#noteBody').htmlarea('pasteHTML', $j(tableCell).text().replace('-', ''));
	// focus on text area
	$j('div#noteBoxy textarea#noteBody').focus();
	// call method to remove plugin
	closeSymbolPlugin($j(tableCell).closest('div'));

/**	
 * IE specific code not working for IE 11 so removed this and always use non-IE code
 * 
	// Internet Explorer being used?
	if ($j.browser.msie)
	{
		// copy and paste symbol
		$j(tableCell).clipBoard(null, $j(tableCell).text().replace('-', ''), true, 'noteBody');
	}
	else
	{
		// add the selected symbol to the textarea
		$j('div#noteBoxy textarea#noteBody').val($j('div#noteBoxy textarea#noteBody').val() + $j(tableCell).text().replace('-', ''));
		// focus on text area
		$j('div#noteBoxy textarea#noteBody').focus();
		// call method to remove plugin
		closeSymbolPlugin($j(tableCell).closest('div'));
	}
*/		
}

/**
 * this function removes the symbol plugin from the page
 * 
 * @param {Object} div div to be removed from page
 */				
function closeSymbolPlugin(div)
{
	// remove the symbol selector from the page
	$j(div).remove();
}

/**
 * Utility function that just looks at the current note popup and gathers any data
 * regarding whether or not the note should be saved back as a preset comment, what
 * category to save the comment to or the name of a new category to save to.
 * @return
 */
function getSaveCommentInfo()
{
	var commentInfo = new Array(3);
	
	var saveComment = false;
	var saveCatId = null;
	var saveCatName = '';
	
	// do some extra processing here to check if the note is to be saved
	if($j('#savecomment').length > 0 && $j('#savecomment').attr('checked'))
	{
		saveComment = true;
		saveCatId = $j('#savecommentcategoryid').val();
		saveCatName = $j('#savecommentcategoryname').val();
	}
	commentInfo[0] = saveComment;
	commentInfo[1] = saveCatId;
	commentInfo[2] = saveCatName;
	
	return commentInfo;
}

/**
 * this method inserts a new note
 * 
 * @param {Boolean} inlinenote indicates if this is an inline note we are adding
 * @param {String} label the label text entered
 * @param {String} note the body text entered
 * @param {Boolean} publish indicates if the note is to be published
 * @param {Integer} personid the id of the user adding the note
 * @param {String} notetype the type of note we are adding
 * @param {Integer} notetypeid the id of the entity for which we are adding a note
 * @param {Integer} colspan the number of columns to span when adding a new inline note
 */
function insertNote(inlinenote, label, note, publish, personid, notetype, notetypeid, colspan)
{
	var saveComment = $j('#savecomment').prop('checked');
	var presetCommentCategoryId = saveComment ? $j('#savecommentcategoryid').val() : -1;
	$j.ajax({
		url: "insertnote.htm",
		data: {
			note: note,
			label: label,
			noteType: notetype,
			entityId: notetypeid,
			publish: publish,
			presetCommentCategoryId: presetCommentCategoryId,
			inlineNote: inlinenote
		},
              "beforeSend" : function(xhr) {
              xhr.setRequestHeader(window.cwms.metaDataObtainer.csrfHeaderParameter,
                window.cwms.metaDataObtainer.csrfToken
                );
            },
		method: "POST",
		async: true
	}).done(function(result) {
		if (!inlinenote) {
			// assign relationship
			var relationship = publish ? publicA : privateA;
			// if the count value in the tab is currently zero then remove
			// the list item which displays no notes are present
			if (parseInt($j('#' + relationship.relname + '-count').text()) == 0)
				$j('#' + relationship.relname + '-list').empty();
			// add one to the counter
			$j('#' + relationship.relname + '-count').text(parseInt($j('#' + relationship.relname + '-count').text()) + 1);
			// create a new list item in new list and give original id
			$j('#' + relationship.relname + '-list').prepend(result);
			// if user is showing only private notes then remove link to publish
			if (!publish) $j('span.publishLink').remove();
			// switch the focus from the old note heading tab to the new note heading tab (ie public to private).
			switchMenuFocus(menuElementsNotes, relationship.relname + '-tab', 1);
		}
		else {
			if ($j('tr#' + notetype + notetypeid).length > 0) {
				// check that the field set is present
				if ($j('tr#' + notetype + notetypeid + ' td:first fieldset').length == 0) {
					// append fieldset
					$j('tr#' + notetype + notetypeid + ' td:first').append('<fieldset class="showActiveItemNotes"></fieldset>');
				}
				// check that the notes ol is present
				if ($j('tr#' + notetype + notetypeid + ' td:first fieldset ol.activeItemNotes').length == 0) {
					// append this ordered list ready to receive list item
					$j('tr#' + notetype + notetypeid + ' td:first fieldset').append('<ol class="activeItemNotes"></ol>');
				}
				// append new inline note to list
				$j('tr#' + notetype + notetypeid + ' td:first fieldset ol.activeItemNotes').prepend(result);
			}
			else {
				// create the new notes row for item
				var noterow = '<tr id="' + notetype + notetypeid + '" class="hid">' +
									'<td colspan="' + colspan + '">' +
										'<fieldset class="showActiveItemNotes">' +
											'<ol class="activeItemNotes">' +
													result +
											'</ol>' +
										'</fieldset>' +
									'</td>' +
								'</tr>';
				// is this a job costing inline note?
				if (notetype == 'JOBCOSTINGITEMNOTE') {
					// create fieldset and list for inline notes
					$j('tr#item' + notetypeid).parent().append(noterow);
				}
				else {
					// create fieldset and list for inline notes
					$j('tr#item' + notetypeid).after(noterow);
				}
			}
			// add one to the inline note count
			$j('#count' + notetype + notetypeid).text(parseInt($j('#count' + notetype + notetypeid).text()) + 1);
			// inline notes hidden?
			if ($j('#' + notetype + notetypeid + '[class*=\'hid\']').length) {
				// show the inline notes list
				$j('#' + notetype + notetypeid).removeClass().addClass('vis');
			}
		}
		// get the current note boxy implementation and close it. 
		Boxy.get($j('div#noteBoxy')).hide();
	}).fail(function(error) {
		$j.prompt(error.message);
	});
}

/**
 * this method updates a current note
 * 
 * @param {Boolean} inlinenote indicates if this is an inline note we are adding
 * @param {Integer} noteid the id of the note to be updated
 * @param {Boolean} currentlyPublished indicates if the note is currently published
 * @param {String} label the label text entered
 * @param {String} note the body text entered
 * @param {Boolean} publish indicates if the note is to be published
 * @param {Integer} personid the id of the user adding the note
 * @param {String} notetype the type of note we are adding
 * @param {Integer} notetypeid the id of the entity for which we are adding a note
 */
function updateNote(inlinenote, noteid, currentlyPublished, label, note, publish, personid, notetype, notetypeid)
{
	var saveComment = $j('#savecomment').prop('checked');
	var presetCommentCategoryId = saveComment ? $j('#savecommentcategoryid').val() : -1;
	$j.ajax({
		url: "updatenote.htm",
		data: {
			note: note,
			label: label,
			noteId: noteid,
			noteType: notetype,
			entityId: notetypeid,
			publish: publish,
			presetCommentCategoryId: presetCommentCategoryId,
			inlineNote: inlinenote
		},
		method: "POST",
		async: true
	}).done(function(result) {
		if(!inlinenote) {
			var relationship = publish ? publicA : privateA;
			if (currentlyPublished != publish) {
				// get old publish value and assign relationship
				var oldrelationship = currentlyPublished ? publicA : privateA;
				// delete the note to be moved from the current list
				$j('#' + oldrelationship.relname + '-list li#note' + noteid).remove();
				// get count value in the current tab, subtract one and then re-append
				$j('#' + oldrelationship.relname + '-count').text(parseInt($j('#' + oldrelationship.relname + '-count').text()) - 1);
				// if the count value in current tab becomes zero then create a list
				// item to display that no notes are available
				if (parseInt($j('#' + oldrelationship.relname + '-count').text()) == 0)
					$j('#' + oldrelationship.relname + '-list').append('<li>' + i18n.t("core.utilities:note.noNotesToDisplay", "There are currently no notes to display under this tab") + '</li>');
				// if the count value in the new tab is currently zero then remove
				// the list item which displays no notes are present
				if (parseInt($j('#' + relationship.relname + '-count').text()) == 0)
					$j('#' + relationship.relname + '-list').empty();
				// add one to the new count
				$j('#' + relationship.relname + '-count').text(parseInt($j('#' + relationship.relname + '-count').text()) + 1);
				// create a new list item in new list and give original id
				$j('#' + relationship.relname + '-list').prepend(result);
			}
			else {
				// create a new list item in new list and give original id
				var oldLi = $j('#' + relationship.relname + '-list li#note' + noteid);
				oldLi.after(result);
				oldLi.remove();
			}
			// if user is showing only private notes then remove link to publish
			if (privateOnlyNotes) $j('span.publishLink').remove();
			// switch the focus from the old note heading tab to the new note heading tab (ie public to private).
			switchMenuFocus(menuElementsNotes, relationship.relname + '-tab', 1);
		}
		else {
			// add new note content
			$j('li#note_' + notetype + noteid).replaceWith(result);
		}
		Boxy.get($j('div#noteBoxy')).hide();
	}).fail(function(error) {
		$j.prompt(error.message);
	});
}

/**
 * this method updates a notes status to active or de-active.
 * 
 * @param {Integer} noteid the id of the note
 * @param {Boolean} active true (active) false (deactive)
 * @param {Integer} personid the id of the contact updating this note
 * @param {String} notetype the type of note we are adding
 * @param {Integer} notetypeid the id of the entity for which we are adding a note
 * @param {object} relationship the object used in tabbed notes section to declare relationships between actions. @see Note.js
 */
function changeNoteStatus(noteid, active, personid, notetype, notetypeid, relationship)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/noteservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to update note status
			noteservice.changeNoteStatus(noteid, active, personid, notetype, 
			{
				callback:function(dataFromServer)
				{
					// relationship defined?
					if (relationship == '' || relationship == null)
					{
						// call method to update inline note
						cleanUpInlineNoteChangeStatus(dataFromServer, notetype, notetypeid);
					}
					else
					{
						// call method to update tabbed note
						cleanUpTabbedNoteChangeStatus(dataFromServer, personid, relationship, notetype);
					}
				}
			});
		}
	});
}

/**
 * this method tidies up after method @see changeNoteStatus() when an inline note has been deleted (de-activated).
 * 
 * @param {Object} the note returned from status update
 * @param {String} notetype the type of note we are adding
 * @param {Integer} notetypeid the id of the entity for which we are adding a note
 */
function cleanUpInlineNoteChangeStatus(note, notetype, notetypeid)
{
	// delete the current deactivated row from the deactivated list
	$j('li#note_' + notetype + note.noteid).remove();
	// update the note count
	$j('#count' + notetype + notetypeid).text(parseInt($j('#count' + notetype + notetypeid).text()) - 1);
	// count equals 0?
	if (($j('tr#' + notetype + notetypeid + ' fieldset ol.activeItemNotes li').length == 0) && ($j('tr#' + notetype + notetypeid + ' fieldset ol.activeItemCalReqs li').length == 0))
	{
		// remove the note row from table
		$j('tr#' + notetype + notetypeid).remove();
	}
	else if (($j('tr#' + notetype + notetypeid + ' fieldset ol.activeItemNotes li').length == 0) && ($j('tr#' + notetype + notetypeid + ' fieldset ol.activeItemCalReqs li').length > 0))
	{
		// remove the note ordered list
		$j('tr#' + notetype + notetypeid + ' fieldset ol.activeItemNotes').remove();
	}
}

/**
 * this method tidies up after method @see changeNoteStatus() when a tabbed note has been deleted (de-activated).
 *
 * @param {Object} the note returned from status update
 * @param {Integer} the personid of the contact updating this note
 * @param {Object} relationship object used in tabbed notes section to declare relationships between actions. @see Note.js
 * @param {String} notetype the type of note we are adding
 */
function cleanUpTabbedNoteChangeStatus(note, personid, relationship, notetype)
{
	// text to display in link
	var linkText = i18n.t("core.utilities:note.makePrivate", "Make Private");
	var getNoteText = 'priv';
	// note published?
	if (note.publish == false)
	{
		linkText = i18n.t("core.utilities:note.makePublic", "Make Public");
		getNoteText = 'pub';
	}	
	// delete the note to be moved from the current list
	$j('li').remove('#note' + note.noteid);
	
	// get count value in the current tab, subtract one and then re-append
	$j('#' + relationship.relname + '-count').text(parseInt($j('#' + relationship.relname + '-count').text()) - 1);
	
	// if the count value in current tab becomes zero then create a list
	// item to display that no notes are available
	if (parseInt($j('#' + relationship.relname + '-count').text()) == 0)
	{
		$j('#' + relationship.relname + '-list').append('<li>' + i18n.t("core.utilities:note.noNotesToDisplay", "There are currently no notes to display under this tab") + '</li>');
	}
	
	// if the count value in the new tab is currently zero then remove
	// the list item which displays no notes are present
	if (parseInt($j('#' + relationship.relstatus + '-count').text()) == 0)
	{
		$j('#' + relationship.relstatus + '-list').empty();
	}
	// add one to new count
	$j('#' + relationship.relstatus + '-count').text(parseInt($j('#' + relationship.relstatus + '-count').text()) + 1);
	
	// see if a note label is present and add to variable
	var noteLabel = '';
	if (note.label != '' && note.label != null)
	{
		noteLabel = '<strong><span class="notelabel_' + getNoteText + note.noteid + '">' + note.label + '</span>:</strong><br />';
	}
	
	// note has been deleted and should be moved into the deleted notes section
	if (!note.active)
	{	
		// create a new list item in new list and give original id
		$j('#' + relationship.relstatus + '-list').prepend(	'<li id="note' + note.noteid + '">' +
																// create a new div containing note
																'<div class="notes_note">' +
																	noteLabel +
																	note.note.replace(/\n/gi, "&nbsp;<br />") +
																'</div>' +
																// create a new div containing editor details
																'<div class="notes_editor">' +
																	note.deactivatedBy.name + '<br />' +
																	cwms.dateFormatFunction(note.deactivatedOn) +
																'</div>' +
																// create a new div containing the reactivate link
																'<div class="notes_edit">' +
																	// create anchor which allows the user to re-activate the note
																	'( <a href="#" class="mainlink" title="' + i18n.t("core.utilities:note.reActivate", "Re-Activate") + '" onclick="changeNoteStatus(' + note.noteid + ', ' + !note.active + ', ' + personid + ', \'' + notetype + '\', \'\', ' + relationship.relstatus + '); return false; ">' + i18n.t("core.utilities:note.reActivate", "Re-Activate") + '</a> )' +
																'</div>' +
																// create a new div to clear floats
																'<div class="clear-0"></div>' +
															'</li>');
	}
	else
	{
		// create a new list item in new list and give original id
		$j('#' + relationship.relstatus + '-list').prepend(	'<li id="note' + note.noteid + '">' +
																// create a new div containing note
																'<div class="notes_note">' +
																	noteLabel +
																	'<span class="note_' + getNoteText + note.noteid + '">' + note.note.replace(/\n/gi, "&nbsp;<br />") + '</span>' +
																'</div>' +
																// create a new div containing editor details
																'<div class="notes_editor">' +
																	note.setBy.name + '<br />' +
																	cwms.dateTimeFormatFunction(note.setOn) +
																'</div>' +
																'<div class="notes_edit">' +
																	// create first anchor which will be link to make note either public or private
																	// set onclick and use the location object to find the path to take which will 
																	// allow the user to keep swapping the note between public and private
																	'<span class="publishLink">(&nbsp;<a href="#" class="publishLink mainlink" onclick="publish(' + note.noteid + ', ' + !note.publish + ', ' + personid + ', \'' + notetype + '\', 0, \'\', ' + relationship.relstatus + '); return false; ">' + linkText + '</a>&nbsp;)&nbsp;</span>' +
																	// the second anchor with image appended allows the user to edit the note
																	'<a href="#" onclick=" addNoteBoxy(\'edit\', false, \'' + notetype + '\', 0, ' + personid + ', ' + note.noteid + ', 0); return false; ">' +
																		'<img src="img/icons/note_edit.png" width="16" height="16" alt="' + i18n.t("core.utilities:note.editNote", "Edit Note") + '" title="' + i18n.t("core.utilities:note.editNote", "Edit Note") + '" class="noteimg_padded" />' +
																	'</a>&nbsp;' +
																	// the third anchor with image appended allows the user to delete the note
																	'<a href="#" onclick="changeNoteStatus(' + note.noteid + ', ' + !note.active + ', ' + personid + ', \'' + notetype + '\', \'\', ' + relationship.relstatus + '); return false; ">' +
																		'<img src="img/icons/note_delete.png" class="noteimg_padded" width="16" height="16" alt="' + i18n.t("core.utilities:note.deleteNote", "Delete Note") + '" title="' + i18n.t("core.utilities:note.deleteNote", "Delete Note") + '" />' +
																	'</a>' +
																'</div>' +
																// create a new div to clear floats
																'<div class="clear-0"></div>' +
															'</li>');
		
		// if user is showing only private notes then remove link to publish
		if (privateOnlyNotes == true)
		{
			$j('span.publishLink').remove();
		}
	}
	
	// switch the focus from the old note heading tab to the new note heading tab (ie public to private).
	switchMenuFocus(menuElementsNotes, relationship.relstatus + '-tab', 1);
}

/**
 * this method updates a note to be either publicly visible or private.
 * 
 * @param {Integer} noteid the id of the note to update
 * @param {Boolean} publish true if public, false if private
 * @param {Integer} personid the id of the contact updating this note
 * @param {String} notetype the type of note we are adding
 * @param {Integer} notetypeid the id of the entity for which we are adding a note
 * @param {linkId} the id of the link which will need to be changed
 * @param {Object} relationship object used in tabbed notes section to declare relationships between actions. @see Note.js
 */
function publish(noteid, publish, personid, notetype, notetypeid, linkId, relationship)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/noteservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to change publish status
			noteservice.publish(noteid, publish, personid, notetype, 
			{
				callback:function(dataFromServer)
				{
					// relationship defined?
					if (relationship == '' || relationship == null)
					{
						// call method to update inline note
						cleanUpInlineNotePublish(dataFromServer, publish, personid, notetype, notetypeid, linkId);
					}
					else
					{
						// call method to update tabbed note
						cleanUpTabbedNotePublish(dataFromServer, publish, personid, relationship, notetype);
					}
				}
			});
		}
	});
}

/**
 * this method tidies up after method @see publish() when an inline notes publish status has been changed.
 * 
 * @param {Object} the note returned from publish update
 * @param {Boolean} publish true if public, false if private
 * @param {Integer} personid the id of the contact updating this note
 * @param {String} notetype the type of note we are adding
 * @param {Integer} notetypeid the id of the entity for which we are adding a note
 * @param {String} linkId the id of the link which will need to be changed
 */
function cleanUpInlineNotePublish(note, publish, personid, notetype, notetypeid, linkId)
{
	var getNoteText = '';
	var originalNoteText = '';
	// publish?	
	if (publish)
	{
		originalNoteText = 'priv';
		getNoteText = 'pub';
		// change onclick event and text of anchor
		$j('#' + linkId).after('<a href="#" id="' + linkId + '" onclick=" publish(' + note.noteid + ', false, ' + personid + ', \'' + notetype + '\', ' + notetypeid + ', \'' + linkId + '\', \'\', \'\'); return false; " class="mainlink">' + i18n.t("core.utilities:note.public", "Public") + ' - ' + i18n.t("core.utilities:note.makePrivate", "Make Private") + '</a>')
						.remove();
	}
	else
	{
		originalNoteText = 'pub';
		getNoteText = 'priv';
		// change onclick event and text of anchor
		$j('#' + linkId).after('<a href="#" id="' + linkId + '" onclick=" publish(' + note.noteid + ', true, ' + personid + ', \'' + notetype + '\', ' + notetypeid + ', \'' + linkId + '\', \'\', \'\'); return false; " class="mainlink">' + i18n.t("core.utilities:note.private", "Private") + ' - ' + i18n.t("core.utilities:note.makePublic", "Make Public") + '</a>')
						.remove();
	}
	// update the edit note link with new publish status
	$j('#publishNote' + note.noteid).next().empty().append('<a href="#" onclick=" addNoteBoxy(\'edit\', true, \'' + notetype + '\', ' + notetypeid + ', ' + personid + ', ' + note.noteid + ', 0); return false; "><img src="img/icons/note_edit.png" width="16" height="16" alt="' + i18n.t("core.utilities:note.editNote", "Edit Note") + '" title="' + i18n.t("core.utilities:note.editNote", "Edit Note") + '" class="noteimg_padded" /></a>');	
	// update span class of label and note
	if ($j('span.notelabel_' + originalNoteText + note.noteid + '').length)
	{
		$j('span.notelabel_' + originalNoteText + note.noteid + '').attr('class', $j('span.notelabel_' + originalNoteText + note.noteid + '').attr('class').replace(originalNoteText + note.noteid, getNoteText + note.noteid));
	}
	$j('span.note_' + originalNoteText + note.noteid + '').attr('class', $j('span.note_' + originalNoteText + note.noteid + '').attr('class').replace(originalNoteText + note.noteid, getNoteText + note.noteid));
}

/**
 * this method tidies up after method @see publish() when a tabbed notes publish status has been changed.
 *  
 * @param {Object} the note returned from publish update
 * @param {Boolean} publish true if public, false if private
 * @param {Integer} personid the id of the contact updating this note
 * @param {Object} relationship object used in tabbed notes section to declare relationships between actions. @see Note.js
 * @param {String} notetype the type of note we are adding
 */
function cleanUpTabbedNotePublish(note, publish, personid, relationship, notetype)
{
	// text to display in link
	var linkText = i18n.t("core.utilities:note.makePrivate", "Make Private");
	var getNoteText = 'priv';
	// note published?
	if (note.publish == false)
	{
		linkText = i18n.t("core.utilities:note.makePublic", "Make Public");
		getNoteText = 'pub';
	}
	
	// delete the note to be moved from the current list
	$j('li').remove('#note' + note.noteid);
	
	// get count value in the current tab, subtract one and then re-append
	$j('#' + relationship.relname + '-count').text(parseInt($j('#' + relationship.relname + '-count').text()) - 1);
	
	// if the count value in current tab becomes zero then create a list
	// item to display that no notes are available
	if (parseInt($j('#' + relationship.relname + '-count').text()) == 0)
	{
		$j('#' + relationship.relname + '-list').append('<li>' + i18n.t("core.utilities:note.noNotesToDisplay", "There are currently no notes to display under this tab") + '</li>');
	}
	
	// if the count value in the new tab is currently zero then remove
	// the list item which displays no notes are available
	if (parseInt($j('#' + relationship.publish + '-count').text()) == 0)
	{
		$j('#' + relationship.publish + '-list').empty();
	}
	
	$j('#' + relationship.publish + '-count').text(parseInt($j('#' + relationship.publish + '-count').text()) + 1);
	
	// see if a note label is present and add to variable
	var noteLabel = '';
	if (note.label != '' && note.label != null)
	{
		noteLabel = '<strong><span class="notelabel_' + getNoteText + note.noteid + '">' + note.label + '</span>:</strong><br />';
	}
	
	// create a new list item in new list and give original id	
	$j('#' + relationship.publish + '-list').prepend('<li id="note' + note.noteid + '">' +
														// create a new div containing note
														'<div class="notes_note">' +
															noteLabel + 
															'<span class="note_' + getNoteText + note.noteid + '">' + note.note.replace(/\n/gi, "&nbsp;<br />") + '</span>' +
														'</div>' +
														// create a new div containing editor details
														'<div class="notes_editor">' +
															note.setBy.name + '<br />' +
															cwms.dateTimeFormatFunction(note.setOn)+
														'</div>' +
														'<div class="notes_edit">' +
															// create first anchor which will be link to make note either public or private
															// set onclick and use the location object to find the path to take which will 
															// allow the user to keep swapping the note between public and private
															'<span class="publishLink">(&nbsp;<a href="#" class="mainlink" onclick="publish(' + note.noteid + ', ' + !note.publish + ', ' + personid + ', \'' + notetype + '\', 0, \'\', ' + relationship.publish + '); return false; ">' + linkText + '</a>&nbsp;)&nbsp;</span>' +
															// the second anchor with image appended allows the user to edit the note
															'<a href="#" onclick=" addNoteBoxy(\'edit\', false, \'' + notetype + '\', 0, ' + personid + ', ' + note.noteid + ', 0); return false; ">' +
																'<img src="img/icons/note_edit.png" width="16" height="16" alt="' + i18n.t("core.utilities:note.editNote", "Edit Note") + '" title="' + i18n.t("core.utilities:note.editNote", "Edit Note") + '" class="noteimg_padded" />' +
															'</a>&nbsp;' +
															// the third anchor with image appended allows the user to delete the note
															'<a href="#" onclick="changeNoteStatus(' + note.noteid + ', ' + !note.active + ', ' + personid + ', \'' + notetype + '\', \'\', ' + relationship.publish + '); return false; "><img src="img/icons/note_delete.png" class="noteimg_padded" width="16" height="16" alt="' + i18n.t("core.utilities:note.deleteNote", "Delete Note") + '" title="' + i18n.t("core.utilities:note.deleteNote", "Delete Note") + '" /></a>' +
														'</div>' +
														// create a new div to clear floats
														'<div class="clear-0"></div>' +
													'</li>');
	
	// switch the focus from the old note heading tab to the new note heading tab (ie public to private).
	switchMenuFocus(menuElementsNotes, relationship.publish + '-tab', 1);
}

/**
 * toggles the display of notes between showing all and hiding all.
 * 
 * @param {Object} anchor the link clicked
 * @param {String} noteType the type of note we are displaying or hiding
 * @param {String} visibility string indicating visible or hidden
 * @param {Integer} entityId id of the parent entity on which we are toggling notes
 */
function toggleNoteDisplay(anchor, noteType, visibility, entityId)
{	
	// set visibility to visible?
	if (visibility == 'vis')
	{
		// append anchor to make notes hidden and remove current anchor
		$j('.' + $j(anchor).attr('class')).after(	'<a href="#" onclick=" toggleNoteDisplay(this, \'' + noteType + '\', \'hid\', ' + entityId + '); return false;" class="toggleNoteDisplay">' +
														'<img src="img/icons/note_collapse.png" width="20" height="16" alt="' + i18n.t("core.utilities:note.collapseAllNotes", "Collapse All Notes") + '" title="' + i18n.t("core.utilities:note.collapseAllNotes", "Collapse All Notes") + '" />' +
													'</a>').remove();
		// make all notes visible
		$j('tr[id^="' + noteType + '"]').removeClass('hid').addClass(visibility);
		// toggling quotation item notes?
		if (noteType == 'QUOTEITEMNOTE')
		{
			// call dwr service to set session variable to true
			// quote notes for this quotation will remain visible for this user
			dwrservice.setNoteVisibility('sessQuoteItemNoteVisibility' + entityId, true);
		}
	}
	else
	{
		// append anchor to make notes visible and remove current anchor
		$j('.' + $j(anchor).attr('class')).after(	'<a href="#" onclick=" toggleNoteDisplay(this, \'' + noteType + '\', \'vis\', ' + entityId + '); return false;" class="toggleNoteDisplay">' +
														'<img src="img/icons/note_expand.png" width="20" height="16" alt="' + i18n.t("core.utilities:note.expandAllNotes", "Expand All Notes") + '" title="' + i18n.t("core.utilities:note.expandAllNotes", "Expand All Notes") + '" />' +
													'</a>').remove();
		// make all notes hidden
		$j('tr[id^="' + noteType + '"]').removeClass('vis').addClass(visibility);
		// toggling quotation item notes?
		if (noteType == 'QUOTEITEMNOTE')
		{
			// call dwr service to set session variable to false
			// quote notes for this quotation will be hidden for this user
			dwrservice.setNoteVisibility('sessQuoteItemNoteVisibility' + entityId, false);
		}
	}
}
