/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	ContactWeb.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	Commenting of code.
*	KNOWN ISSUES	:	-
*	HISTORY			:	26/10/2006 - SH - Created File
*
****************************************************************************************************************************************/



var edress = 'web_at_antech_dot_org_dot_uk';

function get_edress()
{
	var re= /_at_/gi;
	edress = edress.replace(re,'@');
	var re= /_dot_/gi;
	edress = edress.replace(re,'.');
	var the_link="mai" + "lto" +":"+edress;
	window.location=the_link;
} 
