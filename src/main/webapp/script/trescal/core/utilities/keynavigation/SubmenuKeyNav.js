/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	SubmenuKeyNav.js
*	DESCRIPTION		:	This javascript file is imported into a page using the init() onload method if it detects that
* 					:	a submenu is present.  This file is imported in conjunction with the prototype.js file.
* 					:	Once loaded this file allows the user to navigate through the submenu using the CTRL + LEFT/RIGHT cursor keys
* 					:  	A new submenu is selected when the user releases the control key.
*	DEPENDENCIES	:   script/thirdparty/jQuery/jquery.js
*
*	TO-DO			: 	Commenting of code.
*	KNOWN ISSUES	:	-
*	HISTORY			:	08/06/2007 - SH - Created File
*
****************************************************************************************************************************************/

/**
 * start jquery keypress observer to check if the user has pressed a combination of ctrlkey + left/right arrows,
 * if so then we should move the sub navigation along to the next link, when the ctrl key is released then the
 * onclick event of the anchor should be triggered if there is one, else use anchor href to redirect user.
 */ 

/**
 * variable used to check if this file has been loaded when jsunit testing
 */
var SubmenuKeyNav = '';

/**
 * variable used to flag whether the user has pressed the left or right arrow cursor keys
 */
var arrowKeyPress = false;

/**
 * variable used to contain the current submenu id (could be 'submenu' or 'submenu2')
 */
var submenu = 'subnav';

// capture keydown events and if ctrlkey + left/right cursor arrows then perform actions
$j(document).keydown( function(e)
{
	// ctrl key is being pressed
	if ((e.ctrlKey && typeof Boxy == null) || (e.ctrlKey && typeof Boxy != null && !Boxy.isModalVisible()))
	{
		// re-initialise variable each time ctrl key is pressed
		arrowKeyPress = false;
	
		// control key and right arrow cursor are being pressed
		if (e.keyCode == 39)
		{
			// variable to hold the index of the next anchor to select
			var nextAnchor = 0;
			
			$j('#' + submenu + ' a').each(function(i){
				// this is the currently selected anchor
				if (this.className == 'selected')
				{
					// remove 'selected' class from anchor
					this.className = '';
					// populate nextAnchor with the index of the next anchor
					nextAnchor = i + 1;
					// nextAnchor is equal to number of anchors so populate with the first index
					if (nextAnchor == $j('#' + submenu + ' a').length)
					{
						nextAnchor = 0;
					}
					// style the chosen anchor
					$j('#' + submenu + ' a').eq(nextAnchor).addClass('selected');
					// return false ends the jquery each loop
					return false;
				}
			});
			// set variable to indicate that the right arrow cursor has been pressed
			arrowKeyPress = true;
			
		}
		// control key and left arrow cursor are being pressed
		else if (e.keyCode == 37)
		{
			// variable to hold value of previous anchor to select
			var prevAnchor = 0;
			 
			$j('#' + submenu + ' a').each(function(i){
				// this is the currently selected anchor
				if (this.className == 'selected')
				{
					// remove 'selected' class from anchor
					this.className = '';
					// populate prevAnchor with the index of the previous anchor
					prevAnchor = i - 1;
					// prevAnchor is less than 0 so populate with the last index
			 		if (prevAnchor < 0)
			 		{
			 			prevAnchor = $j('#' + submenu + ' a').length - 1;
			 		}
					// style the chosen anchor
					$j('#' + submenu + ' a').eq(prevAnchor).addClass('selected');
					// return false ends the jquery each loop
					return false;
				}
			});
			// set variable to indicate that the right arrow cursor has been pressed
			arrowKeyPress = true;
			
		}
		// control key and down arrow cursor are being pressed
		else if (e.keyCode == 40)
		{
			if (submenu == 'subnav')
			{
				if ($j('#subnav2').length)
				{
					// variable to hold value of previous anchor to select
					var prevAnchor = 0;
					 
					$j('#' + submenu + ' a').each(function(i){
						// this is the currently selected anchor
						if (this.className == 'selected')
						{
							// remove 'selected' class from anchor
							this.className = '';
							// populate prevAnchor with the index of the previous anchor
							prevAnchor = 0;
							// set variable to new submenu
							submenu = 'subnav2';
							// style the chosen anchor
							$j('#' + submenu + ' a').eq(prevAnchor).addClass('selected');
							// return false ends the jquery each loop
							return false;
						}
					});
					// set variable to indicate that the right arrow cursor has been pressed
					arrowKeyPress = true;
				}
			}	
		}
		// control key and up arrow cursor are being pressed
		else if (e.keyCode == 38)
		{
			if (submenu == 'subnav2')
			{
				if ($j('#subnav').length)
				{
					// variable to hold value of previous anchor to select
					var prevAnchor = 0;
					 
					$j('#' + submenu + ' a').each(function(i){
						// this is the currently selected anchor
						if (this.className == 'selected')
						{
							// remove 'selected' class from anchor
							this.className = '';
							// populate prevAnchor with the index of the previous anchor
							prevAnchor = 0;
							// set variable to new submenu
							submenu = 'subnav';
							// style the chosen anchor
							$j('#' + submenu + ' a').eq(prevAnchor).addClass('selected');
							// return false ends the jquery each loop
							return false;
						}
					});
					// set variable to indicate that the right arrow cursor has been pressed
					arrowKeyPress = true;
				}
			}	
		}		
	}

// capture keyup events and if ctrlkey then perform actions
}).keyup( function(e){
	
	// ctrl key has been released and an arrow cursor key has been pressed so trigger event
	if (e.ctrlKey && arrowKeyPress == true)
	{
		// get the anchor in the sub navigation list that is selected
		$j('#' + submenu + ' a.selected').each(function(i, item){
			
			// check to see if there is an onclick attribute and if it has any content
			// onclick is not present or empty then send to location in href attribute
			if(!item.onclick)
			{
				window.location.href = item.href;
			}
			// onclick is present and has a value so check the 
			// browser agent and fire the onclick event.
			else
			{
				// if ie then use the fireEvent function (ie only)
				if ($j.browser.msie)
				{
					item.fireEvent('onclick');
				}
				// else use the dispatchEvent dom method
				else
				{
					var evt = document.createEvent("MouseEvents");
					evt.initMouseEvent("click", true, true, window,
					0, 0, 0, 0, 0, false, false, false, false, 0, null);
					item.dispatchEvent(evt);
				}
			}			
		});
	}

});	 