/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	SearchResultsKeyNav.js
*	DESCRIPTION		:	Jquery based javascript file which is automatically imported into the page when a tbody with an id of
* 					: 	'keynavResults' is present in the page.  This code checks page events for keypresses and if one of the keycodes
* 					:	matches that of the arrow cursor keys then the corresponding action will be taken. 
*
*	DEPENDENCIES	:   script/thirdparty/jQuery/jquery.js
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	28/08/2007 - SH - Created File
*
****************************************************************************************************************************************/

/*
 * variable to hold the location of the currently selected result
 */
var selectedResult = 0;

var hasChild = false;

if ($j('table#jobsearchresults tbody tr#child0').length)
{
	hasChild = true;
}

/*
 * function to get the original class name applied to the row
 */
function getOriginalClass()
{
	// variable to hold the class name
	var classname = '';
	
	// does modulus result match zero, if so class is even
	if (selectedResult % 2 == 0)
	{
		classname = 'even';
	}
	// modulus result is odd, so class is odd
	else
	{
		classname = 'odd';
	}
	
	// return the classname variable
	return classname;
}

/*
 * start jquery keypress observer to check if the user has pressed the up/down arrow or the
 * enter key when searching for companies or contacts.
 */
$j(document).keydown( function(e){

    var event = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
	
	// see if the down arrow (40) or up arrow (38) has been pressed?
	switch (event)
	{
		
		//  up arrow key has been pressed
		case 38: 	// beginning of the list has been reached so cannot move up
					if (selectedResult == 0)
					{
					}
					else
					{
						// we are in search results with parent and children rows
						// tbody with id 'keynavResults' has table rows with id's that start with 'parent'
						if ($j('tbody#keynavResults > tr[id^="parent"]').size() != 0)
						{
							// remove the selected 'hoverrow' class and revert back to original class
							$j('tbody#keynavResults > tr[id^="parent"]').eq(selectedResult).removeClass().addClass(getOriginalClass());
							// this row has a child so revert this too
							if (hasChild == true)
							{
								// remove the selected 'hoverrow' class and revert back to original class
								$j('tbody#keynavResults > tr#child' + selectedResult).removeClass().addClass(getOriginalClass());
								hasChild = false;
							}
							
							// subtract one from the selectedResult value
							selectedResult = selectedResult - 1;
							
							// remove the original class applied and add the selected 'hoverrow' class to row
							$j('tbody#keynavResults > tr[id^="parent"]').eq(selectedResult).removeClass().addClass('hoverrow');		
							// this row has a child so add selected 'hoverrow' class to this too
							if ($j('tbody#keynavResults > tr#child' + selectedResult).length)
							{
								// set variable to true so we know next time that a child row needs changing
								hasChild = true;
								// remove the original class applied and add the selected 'hoverrow' class to row
								$j('tbody#keynavResults > tr#child' + selectedResult).removeClass().addClass('hoverrow');
							}
							
						}
						// this is a normal table of results which have no children
						else
						{
							if ($j('tbody#keynavResults > tr').eq(selectedResult).attr('class') != 'hoverrow')
							{
								$j('tbody#keynavResults > tr').each(function(i, n){
									if (n.className == 'hoverrow even' || n.className == 'hoverrow odd')
									{
										selectedResult = i;
										// remove the selected class and add original class applied
										$j('tbody#keynavResults > tr').eq(selectedResult).removeClass().addClass(getOriginalClass());
										// remove one from the selectedResult value
										selectedResult = selectedResult - 1;
										// remove the original class applied and add the selected class
										$j('tbody#keynavResults > tr').eq(selectedResult).removeClass().addClass('hoverrow');
										$j('tbody#keynavResults > tr a')[selectedResult].focus();
										window.scrollBy(0, 1);
									}
								});
							}
							else
							{
								// remove the selected class and add original class applied
								$j('tbody#keynavResults > tr').eq(selectedResult).removeClass().addClass(getOriginalClass());
								// remove one from the selectedResult value
								selectedResult = selectedResult - 1;
								// remove the original class applied and add the selected class
								$j('tbody#keynavResults > tr').eq(selectedResult).removeClass().addClass('hoverrow');
								$j('tbody#keynavResults > tr a')[selectedResult].focus();
								window.scrollBy(0, 1);
							}
						}
						
					}
					break;
				 
		// down arrow key has been pressed
		case 40: 	// end of the list has been reached so cannot move down
					if (selectedResult == $j('tbody#keynavResults > tr[id^="parent"]').size() - 1)
					{
					}
					else
					{
						// we are in search results with parent and children rows
						// tbody with id 'keynavResults' has table rows with id's that start with 'parent'
						if ($j('tbody#keynavResults > tr[id^="parent"]').size() != 0)
						{
							// remove the selected 'hoverrow' class and revert back to original class
							$j('tbody#keynavResults > tr[id^="parent"]').eq(selectedResult).removeClass().addClass(getOriginalClass());
							// this row has a child so revert this too
							if (hasChild == true)
							{
								// remove the selected 'hoverrow' class and revert back to original class
								$j('tbody#keynavResults > tr#child' + selectedResult).removeClass().addClass(getOriginalClass());
								hasChild = false;
							}
							
							// add one to the selectedResult value
							selectedResult = selectedResult + 1;
							
							// remove the original class applied and add the selected 'hoverrow' class to row
							$j('tbody#keynavResults > tr[id^="parent"]').eq(selectedResult).removeClass().addClass('hoverrow');					
							// this row has a child so add selected 'hoverrow' class to this too
							if ($j('tbody#keynavResults > tr#child' + selectedResult).length)
							{
								// set variable to true so we know next time that a child row needs changing
								hasChild = true;
								// remove the original class applied and add the selected 'hoverrow' class to row
								$j('tbody#keynavResults > tr#child' + selectedResult).removeClass().addClass('hoverrow');
							}
							
						}
						// this is a normal table of results which have no children
						else
						{
							if (selectedResult == $j('tbody#keynavResults > tr').size() - 1)
							{
							}
							else
							{
								if ($j('tbody#keynavResults > tr').eq(selectedResult).attr('class') != 'hoverrow')
								{
									$j('tbody#keynavResults > tr').each(function(i, n)
									{
										if (n.className == 'hoverrow even' || n.className == 'hoverrow odd')
										{
											selectedResult = i;
											// remove the selected class and add original class applied
											$j('tbody#keynavResults > tr').eq(selectedResult).removeClass().addClass(getOriginalClass());
											// remove one from the selectedResult value
											selectedResult = selectedResult + 1;
											// remove the original class applied and add the selected class
											$j('tbody#keynavResults > tr').eq(selectedResult).removeClass().addClass('hoverrow');
											$j('tbody#keynavResults > tr a')[selectedResult].focus();
											window.scrollBy(0, 1);
										}
									});
								}
								else
								{
									// remove the selected class and add original class applied
									$j('tbody#keynavResults > tr').eq(selectedResult).removeClass().addClass(getOriginalClass());
									// remove one from the selectedResult value
									selectedResult = selectedResult + 1;
									// remove the original class applied and add the selected class
									$j('tbody#keynavResults > tr').eq(selectedResult).removeClass().addClass('hoverrow');
									$j('tbody#keynavResults > tr a')[selectedResult].focus();
									window.scrollBy(0, 1);
								}
							}
						}
											
					}
					break;
				 
				 
		// enter key has been pressed
		case 13: 	// we are in search results with parent and children rows
					// tbody with id 'keynavResults' has table rows with id's that start with 'parent'
					if ($j('tbody#keynavResults > tr[id^="parent"]').size() != 0)
					{
						// trigger the onclick event on the selected row in results
						$j('tbody#keynavResults > tr[id^="parent"]').eq(selectedResult).trigger('onclick');
					}
					else
					{
						// trigger the onclick event on the selected row in results
						$j('tbody#keynavResults > tr').eq(selectedResult).trigger('onclick');
					}
					break;
		
	}
	
});
	

