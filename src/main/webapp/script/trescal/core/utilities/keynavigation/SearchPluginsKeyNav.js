/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	SearchPluginsKeyNav.js
*	DESCRIPTION		:	Jquery based javascript file which is automatically imported into the page when adding a company or contact
* 					: 	search plugin, a cascading plugin or a multi contact search plugin.  This code checks page events for keypresses
* 					:	and if one of the keycodes matches that of the arrow cursor keys then the corresponding action will be taken. 
*
*	DEPENDENCIES	:   script/thirdparty/jQuery/jquery.js
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	26/07/2007 - SH - Created File
*
****************************************************************************************************************************************/

/**
 * this variable is used to hold the object of either the company or contact search plugin depending on which one
 * is currently being used or has focus.  This object is used to source literal values throughout this file.
 * 
 * literal values returned from either the CompanySearchPlugin.js or ContactSearchPlugin.js objects of same name. 
 */
var searchFOCUS = '';

/**
 * variable used to check if this file has been loaded when jsunit testing
 */
var SearchPluginsKeyNav = '';

/**
 * start jquery keypress observer to check if the user has pressed the up/down arrow or the
 * enter key when searching for companies or contacts.
 */ 
 
$j(document).keydown( function(e){

    var event = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
	
	// see if the down arrow (40) or up arrow (38) has been pressed?
	switch (event)
	{
		
		//  up arrow key has been pressed
		case 38: if (searchFOCUS != '')
				 {
					 // check selected literal in either company or contact object is not equal to zero
					 if (searchFOCUS.selected == 0)
					 {
					 	
					 }
					 // selected literal contains value greater than zero so can be moved up
					 else
					 {
					 	 // remove 'selected' class name from the currently selected element and add to the previous one.
						 // add id of the newly selected element to the hidden input form id field (ie coid, personid) and apply focus
						 $j('#' + searchFOCUS.resultsDiv + ' a').eq(searchFOCUS.selected).removeClass('selected').prev().addClass('selected').attr('id', function(){$j('#' + searchFOCUS.idInput).attr('value', this.id);}).focus();
						 // remove one from the selected literal value
						 searchFOCUS.selected = searchFOCUS.selected - 1;
						 // then return focus to the input field
						 $j('#' + searchFOCUS.inputFocus).focus();
						 // a company search plugin is present?
						 if (typeof CompanySearchPlugin != 'undefined')
						 {
						 	 // current search focus is on the company search plugin which is being cascaded
							 if (searchFOCUS == CompanySearchPlugin && CompanySearchPlugin.cascade == true && CascadeSearchPlugin.cascadeRules.length > 0)
							 {
							 	// get the coid
								var coid = $j('#' + searchFOCUS.resultsDiv + ' a').eq(searchFOCUS.selected).attr('id');
								// check for blocked company
								if ($j('#' + searchFOCUS.resultsDiv + ' a').eq(searchFOCUS.selected).attr('class').indexOf('compBlocked') != -1)
								{
									// this is a cascading search so clear all search boxes and display message
									CompanySearchPlugin.clearSearch(true);
								}
								else
								{
								 	// call subdiv search plugin run ajax method using selected companies coid
									SubdivSearchPlugin.runSubdivAjax(coid);
								}
								// do we need to load currency info for selected company?
								if (CascadeSearchPlugin.loadCurrency == true)
								{
									CompanySearchPlugin.loadCurrency(coid);
								}
							 }
						 }
					 }
					 
					 // set this literal to true so that the ajax function is not called
					 searchFOCUS.keyPress = true;
					 break;
				 }
				 
		// down arrow key has been pressed
		case 40: if (searchFOCUS != '')
				 {	
				 	// check selected literal in either company or contact object is not equal to the number of anchors returned
					 if (searchFOCUS.selected == $j('#' + searchFOCUS.resultsDiv + ' a').size() - 1)
					 {
					 
					 }
					 // selected literal contains value less than anchors length so can be moved down
					 else
					 {
					 	// remove 'selected' class name from the currently selected element and add to the next one.
						// add the id of the newly selected element to the hidden form id field (ie coid, personid) and apply focus
						$j('#' + searchFOCUS.resultsDiv + ' a').eq(searchFOCUS.selected).removeClass('selected').next().addClass('selected').attr('id', function(){$j('#' + searchFOCUS.idInput).attr('value', this.id);}).focus();
						// remove one from the selected literal value
						searchFOCUS.selected = searchFOCUS.selected + 1;
						// then return focus to the input field
						$j('#' + searchFOCUS.inputFocus).focus();
						
						// a company search plugin is present?
						if (typeof CompanySearchPlugin != 'undefined')
						{
							// current search focus is on the company search plugin which is being cascaded
							if (searchFOCUS == CompanySearchPlugin && CompanySearchPlugin.cascade == true && CascadeSearchPlugin.cascadeRules.length > 0)
							{
								// get the coid
								var coid = $j('#' + searchFOCUS.resultsDiv + ' a').eq(searchFOCUS.selected).attr('id');
								// check for blocked company
								if ($j('#' + searchFOCUS.resultsDiv + ' a').eq(searchFOCUS.selected).attr('class').indexOf('compBlocked') != -1)
								{
									// this is a cascading search so clear all search boxes and display message
									CompanySearchPlugin.clearSearch(true);
								}
								else
								{
								 	// call subdiv search plugin run ajax method using selected companies coid
									SubdivSearchPlugin.runSubdivAjax(coid);
								}
								// do we need to load currency info for selected company?
								if (CascadeSearchPlugin.loadCurrency == true)
								{
									CompanySearchPlugin.loadCurrency(coid);
								}
							}
						}
					 }
					 
					 // set this literal to true so that the ajax function is not called
					 searchFOCUS.keyPress = true;
					 break;
				 }
				 
		// escape key has been pressed
		case 27: 	if (searchFOCUS != '')
					{
						if (typeof CascadeSearchPlugin != 'undefined')
						{
							if (searchFOCUS != CascadeSearchPlugin)
							{
								searchFOCUS.closePlugin();
							}
						}
						else
						{
							searchFOCUS.closePlugin();
						}
						
						// set this literal to true so that the ajax function is not called
						searchFOCUS.keyPress = true;
						// clear value in variable @see SearchPluginsKeyNav.js
						searchFOCUS = '';
						break;
					}
				 
		// enter key has been pressed
		case 13: if (searchFOCUS != '')
				 {
				 	 // variable used to indicate if an action has been performed
				 	 var performedAction = false;
				 	 // company search plugin is present?
				 	 if (typeof CompanySearchPlugin != 'undefined')
				 	 {
				 	 	 // current search focus is on company search plugin which is being cascaded
					 	 if (searchFOCUS == CompanySearchPlugin && CompanySearchPlugin.cascade == true)
						 {
						 	// do nothing
						 	performedAction = true;
						 }
				 	 }
				 	 // multi contact search plugin is present?
				 	 if (typeof MultiContactSearchPlugin != 'undefined')
				 	 {
				 	 	 // current search focus is on multi contact search plugin
						 if (searchFOCUS == MultiContactSearchPlugin)
						 {
							// search has results?
							if ($j('#' + searchFOCUS.resultsDiv + ' a').length > 0)
							{
								// 'anchorRedirect' literal has content?
							 	if (searchFOCUS.anchorRedirect != '')
							 	{
							 		// set window location to that of the href attribute in selected anchor
							 		window.location.href = $j('#' + searchFOCUS.resultsDiv + ' a').eq(searchFOCUS.selected).attr('href');
							 		performedAction = true;
							 	}
							 	// 'customFunction' literal is true
							 	else if (searchFOCUS.customFunction == true)
							 	{
							 		// trigger the onclick event in the selected anchor
							 		$j('#' + searchFOCUS.resultsDiv + ' a').eq(searchFOCUS.selected).trigger('onclick');
							 		performedAction = true;
							 	}
							}
						 }
				 	 }					 
					 // if no other action has been performed then submit form
					 if (performedAction == false)
					 {
					 	e.preventDefault();
						
						if (typeof ContactSearchPlugin != 'undefined')
						{
							if (searchFOCUS == ContactSearchPlugin)
							{
								var name = $j('#' + searchFOCUS.resultsDiv + ' a').eq(searchFOCUS.selected).text();
								var firstName = name.substring(0, name.indexOf(' '));
								var lastName = name.substring(name.indexOf(' ') + 1, name.length);
								// populate the first and last name input fields
								$j('#' + searchFOCUS.firstInput).attr('value', firstName);
								$j('#' + searchFOCUS.lastInput).attr('value', lastName);
							}
							else
							{
								// populate the coname input field with the name of the selected company
								$j('#' + searchFOCUS.nameInput).attr('value', $j('#' + searchFOCUS.resultsDiv + ' a').eq(searchFOCUS.selected).text());
							}			
						}
						else
						{
							// populate the name input field with the name of the selected
							$j('#' + searchFOCUS.nameInput).attr('value', $j('#' + searchFOCUS.resultsDiv + ' a').eq(searchFOCUS.selected).text());
						}
						// hide the company results container
						$j('#' + searchFOCUS.boxDiv + ' #' + searchFOCUS.resultsDiv + ', #' + searchFOCUS.boxDiv + ' .' + searchFOCUS.headDiv).css({zIndex: -1, display: 'none'});
						// populate the id hidden input field with the id of this anchor (ie coid)
						$j('#' + searchFOCUS.idInput).attr('value', $j('#' + searchFOCUS.resultsDiv + ' a').eq(searchFOCUS.selected).attr('id'));
					 	// apply focus to the first form element in the next list item
						$j('#' + searchFOCUS.boxDiv).parent('li').next('li').find('input:not([type="hidden"]), select').eq(0).focus();
						// form is part of a search page
						if (searchFOCUS.searchFormId)
						{
							if ($j('#' + searchFOCUS.searchFormId).attr('id').indexOf('search') != -1)
							{
								// apply focus to the submit button so user can quickly initiate search
								$j('#' + searchFOCUS.searchFormId + ' input[type="submit"]:first').focus();
							}
						}						
					 }
					 // set this literal to true so that the ajax function is not called
					 searchFOCUS.keyPress = true;
					 // clear value in variable @see SearchPluginsKeyNav.js
					 searchFOCUS = '';
					 
					 break;
				 }
		
		// left arrow key has been pressed
		case 37: if (searchFOCUS != '')
				 {
				 	 // set keypress variable to true so that the ajax function is not called.
					 // no other functionality, included this code only to stop the list being reloaded
					 // if this key is pressed mistakenly 
					 searchFOCUS.keyPress = true;
					 break;
				 }
				 
		// right arrow key has been pressed
		case 39: if (searchFOCUS != '')
				 {
					 // set keypress vriable to true so that the ajax function is not called.
					 // no other functionality, included this code only to stop the list being reloaded
					 // if this key is pressed mistakenly 
					 searchFOCUS.keyPress = true;
					 break;
				 }
				 
		// tab key has been pressed
		case 9:	if (searchFOCUS != '')
				{
				 // set keypress vriable to true so that the ajax function is not called.
				 searchFOCUS.keyPress = true;
				 break;
				}
	}
	
});
