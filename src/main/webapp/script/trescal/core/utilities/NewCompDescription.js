/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	NewCompDescription.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	24/10/2007 - SH - Created File
*					:	02/11/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * time value of last DWR call, checked against the current DWR call
 */
var compdescUpdate = 0;

/**
 * load service necessary for searching component descriptions
 */
loadScript.loadScripts(new Array('dwr/interface/componentdescriptionservice.js'));

/**
 * this function uses dwr to populate a list with component descriptions, the list appears in a thickbox
 * when the user wishes to add a new component description to the database. 
 * 
 * @param {String} nameFragment all or part of the component description text
 */
function ajaxCompDesc(nameFragment)
{
	// text has been entered into the input field
	if (nameFragment.length > 0)
	{
		// get current timestamp to pass with ajax results
		var currentTime = new Date().getTime();
		
		// call dwr method to retrieve alphabetically ordered component descriptions
		componentdescriptionservice.searchSortedAllComponentDescriptions(nameFragment, 
		{
			callback:function(descs)
			{
				// call method to display component descriptions
				newCompDescriptionCallback(descs, currentTime, nameFragment);
			}
		});
	}
	else
	{
		// remove all previously displayed component descriptions
		$j('#newcompdescResults').empty();
		// hide the insert button
		$j('#insertCompDesc').removeClass().addClass('hid');
	}
}

/**
 * this function displays all component descriptions which are currently stored in the database that have been
 * returned by method @see ajaxCompDesc().
 * 
 * @param {Array} descs is an array of description objects  
 * @param {Date/Time} currentUpdate is the current timestamp of search
 * @param {String} nameFragment all or part of the component description text
 */
function newCompDescriptionCallback(descs, currentUpdate, nameFragment)
{
	// this is a new search
	if (currentUpdate > compdescUpdate)
	{
		// update value in 'compdescUpdate'
		compdescUpdate = currentUpdate;
		// at least one description returned
		if(descs.length > 0)
		{
			// remove all previously displayed component descriptions
			$j('#newcompdescResults').empty();
			// add div for each component description returned to results list
			$j.each(descs, function(i){
				$j('#newcompdescResults').append('<div><span>' + descs[i].description + '</span></div>');												
			});
			// current string matches a component description already in the database
			if (descs[0].description.toLowerCase() == nameFragment.toLowerCase())
			{
				// hide the insert button
				$j('#insertCompDesc').removeClass().addClass('hid');
			}
			// current string is a valid new component description
			else
			{
				// hide the insert button
				$j('#insertCompDesc').removeClass().addClass('vis');
			}										
		}
		else
		{
			// remove all previously displayed component descriptions
			$j('#newcompdescResults').empty();
			// append no matches message
			$j('#newcompdescResults').append('<div class="center-0 bold"><span>' + i18n.t("core.utilities:noMatchingDesc", "No Matching Descriptions") + '</span></div>');
			// make insert button visible
			$j('#insertCompDesc').removeClass().addClass('vis');
		}
	}
}

/**
 * this function uses dwr to insert a new component description into the database. If the insertion
 * is successful then a message is diplayed within the thickbox detailing the success. Else if the insertion
 * is declined then appropriate error messages are displayed within the thickbox.
 */
function insertCompDesc()
{
	// get value of the new component description to be inserted
	var newCompDescription = $j('#newCompDescription').val();
	// call dwr method to insert the new component description
	componentdescriptionservice.insert(newCompDescription, 
	{
		callback:function(results)
		{
			// insertion failed
			if(results.success != true)
			{
				// warning box is not already present
				if ($j('.warningBox1').length == 0)
				{
					// remove successbox if present
					$j('div').remove('.successBox1');
					// append the new warningbox before fieldset
					$j('.thickbox-box fieldset').before('<div class="warningBox1">' +
															'<div class="warningBox2">' +
																'<div class="warningBox3">' +
																	// new warning message goes here
																'</div>' +
															'</div>' +
														'</div>');
				}
				// empty the current warningbox
				$j('.warningBox3').empty();
				// append message to warningbox
				$j('.warningBox3').append('<h5 class="center-0 attention">' + i18n.t("core.utilities:newComponentDescFailed", "New Component Description Failed") + '</h5>');
				// loop through errorlist						
				$j.each(results.errorList, function(i, n){
					// description field of description failed
					if (n.field == 'description')
					{
						// display error message next to the description field
						$j('#errorMessage').append(n.defaultMessage);
					}
					else
					{
						// other type of error so display in main warningbox
						$j('.warningBox3').append('<div class="center attention">' + i18n.t("fieldError", {varErrField: n.field, errMsg : n.defaultMessage, defaultValue : "Field: " + n.field + " failed. Message:" + n.defaultMessage}) + '</div>');
					}
				});
			}
			// insertion was a success
			else
			{
				// remove warningbox if present
				$j('div').remove('.warningBox1');
				// append a successbox
				$j('div').remove('.successBox1');
				// empty the errormessage span
				$j('#errorMessage').empty();
				// append new successbox before fieldset
				$j('.thickbox-box fieldset').before('<div class="successBox1">' +
														'<div class="successBox2">' +
															'<div class="successBox3">' +
																'<h5 class="center-0">' + i18n.t("core.utilities:newComponentDescSucced", {varDesc : results.results.description, defaultValue : "New component description " + results.results.description + " added successfully"}) + '</h5>' +
															'</div>' +
														'</div>' +
													'</div>');
				// empty value in new description input field
				$j('#newCompDescription').attr('value', '');
				// hide the insert button
				$j('#insertCompDesc').removeClass().addClass('hid');
				// remove all previously displayed manufacturers
				$j('#newcompdescResults').empty();					
			}	
		}
	});
}

/**
 * this function creates content for the thickbox when adding a new component description
 */
function createCompDescriptionContent()
{
	// create content to be displayed in thickbox window using previously populated variables
	thickboxContent = 	'<div class="thickbox-box">' +
							'<div class="warningBox1">' +
								'<div class="warningBox2">' +
									'<div class="warningBox3">' +
										'<div>' + i18n.t("core.utilities:enterNewComponentDesc", "Enter your new component description in the input field below, only when there are no matches in the current database will the insert button be available to you.") + '</div>' +
									'</div>' +
								'</div>' +
							'</div>' +
							'<fieldset>' +
								'<legend>' + i18n.t("core.utilities:addNewComponentDesc", "Add New Component Description") + '</legend>' +
								'<ol>' +
									'<li>' +
										'<label for="description">' + i18n.t("descriptionLabel", "Description:") + '</label>' +
										'<span>' +
											'<input type="text" name="newCompDescription" id="newCompDescription" onkeyup="ajaxCompDesc(this.value, \'new\'); return false;" autocomplete="off" />' +
											'<input type="submit" name="insertCompDesc" id="insertCompDesc" value="' + i18n.t("core.utilities:plugins.insert", "Insert") + '" class="hid" onclick=" insertCompDesc(); "/>' +
											'<span id="errorMessage" class="attention"></span>' +
										'</span>' +
									'</li>' +
									'<li>' +
										'<label>' + i18n.t("core.utilities:descriptionMatchesLabel", "Description Matches:") + '</label>' +
										'<span>' +
											'<div style=" width: 40%; float: left; ">' +
												'<div class="generalHeader"><span>' + i18n.t("core.utilities:matchingComponentDesc", "Matching Component Descriptions") + '</span></div>' +
												// create and append the div in which description results will be displayed
												'<div id="newcompdescResults" class="generalResults">' +
													// results are displayed here
												'</div>' +
											'</div>' +
										'</span>' +
										'<div class="clear"></div>' +
									'</li>' +
								'</ol>' +
							'</fieldset>' +
						'</div>';
		
	// return thickbox content
	return thickboxContent;
}