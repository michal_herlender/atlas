/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	ScriptedUtils.js
*	DESCRIPTION		:	File of scripts used regularly within CWMS.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	Commenting of code.
*	KNOWN ISSUES	:	-
*	HISTORY			:	26/10/2006 - SH - Created File
*					:	02/11/2015 - Tprovost - Fix bug on enable buttons
*					:	02/11/2015 - TProvost - Manage i18n
*					:	09/11/2015 - TProvost - Fix some bugs in functions slideTermsAndCond and updateActivationStatus
****************************************************************************************************************************************/

//  jump menu script
function MM_jumpMenu(targ, selObj, restore)
{
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if(restore)
  {
	  selObj.selectedIndex=0;
  }
}

// Open new window - fixed sized - initialised to the url given as a parameter and centered onscreen
function popupNewWin(url)
{
	helpWindow = window.open(url, 'mywindow', 'width=450, height=400, scrollbars=no, toolbars=yes');
    helpWindow.moveTo((screen.width-420)/2, (screen.height-310)/2);
    helpWindow.focus();

}

function popupNewWinHeight(url, hor, vert)
{
	helpWindow = window.open(url,'mywindow','width='+hor+',height='+vert+',scrollbars=no,toolbars=yes');
	helpWindow.moveTo((screen.width-420)/2, (screen.height-310)/2);
    helpWindow.focus();

}

function popupNewWinHeightScrolls(url, hor, vert)
{
	helpWindow = window.open(url,'mywindow','width='+hor+',height='+vert+',scrollbars=yes,toolbars=yes');
	helpWindow.moveTo((screen.width-420)/2, (screen.height-310)/2);
    helpWindow.focus();

}

function popupNewWinHeightScrollsRaised(url, hor, vert){
	helpWindow = window.open(url,'mywindow','width='+hor+',height='+vert+',scrollbars=yes,toolbars=yes, alwaysRaised=yes, z-lock=yes');
	//helpWindow =3D window.open ("popup.html", "MyWindow","menu=3Dno,toolbar=3Dno,location=3Dno,scrollbars=3Dno,width=3D800,height=3D=600,resizable=3Dno");
    helpWindow.moveTo((screen.width-420)/2, (screen.height-310)/2);
    helpWindow.focus();

}

function closeWindow(){
	if(window.opener.location != null)
	{
		window.opener.location.reload();
		window.close(); 
	}
	return true;
}

function Display(strobj,display, e, lpos, tpos){
document.getElementById(strobj).style.visibility = display;
	if(display != "hidden")
	{ 
	 var yel = findCoords(e);
		 document.getElementById(strobj).style.left = yel[0] - lpos; 
		 document.getElementById(strobj).style.top = yel[1] - tpos; 
	}
}
// find the exact ccoords of a mouse-click 
// completely browser independant - even for I.E. version 6 dtd definitions!
function findCoords(e) {
   if( !e ) { e = window.event; } if( !e || ( typeof( e.pageX ) != 'number' && typeof( e.clientX ) != 'number' ) ) { return [ 0, 0 ]; }
   if( typeof( e.pageX ) == 'number' ) { var posX = e.pageX; var posY = e.pageY; } else {
      var posX = e.clientX; var posY = e.clientY;
      if( !( ( window.navigator.userAgent.indexOf( 'Opera' ) + 1 ) || ( window.ScriptEngine && ScriptEngine().indexOf( 'InScript' ) + 1 ) || window.navigator.vendor == 'KDE' ) ) {
         if( document.documentElement && ( document.documentElement.scrollTop || document.documentElement.scrollLeft ) ) {
            posX += document.documentElement.scrollLeft; posY += document.documentElement.scrollTop;
         } else if( document.body && ( document.body.scrollTop || document.body.scrollLeft ) ) {
            posX += document.body.scrollLeft; posY += document.body.scrollTop;
         }
      }
      return [ posX, posY ];
   }
}

// switch an element's display style 
function DisplayPrint(strobj,display)
{
	document.getElementById(strobj).style.visibility = display;
}

function matchFieldSelect (field, select, value) {
  var property = value ? 'value' : 'text';
  var found = false;
  
  for (var i = 0; i < select.options.length; i++)
    //if ((found = select.options[i][property].indexOf(field.value) == 0))
	if ((found = select.options[i][property].toUpperCase().indexOf(field.value.toUpperCase()) == 0))
      break;
	  
  if (found) 
  {
    if ((i + 10) < select.options.length)
    	select.selectedIndex = i + 10;
    else
    	select.selectedIndex = select.options.length - 1;
    	select.selectedIndex = i;
  }
  else    
  {
  	select.selectedIndex = -1;
  }
  
  if (field.createTextRange) {
    var cursorKeys ="8;46;37;38;39;40;33;34;35;36;45;"
    if (cursorKeys.indexOf(event.keyCode+";") == -1) {
      var r1 = field.createTextRange()
      var oldValue = r1.text;
      var newValue = found ? select.options[i][property] : oldValue;
      if (newValue != field.value) {
        field.value = newValue
        var rNew = field.createTextRange()
        rNew.moveStart('character', oldValue.length) 
        rNew.select()
      }
    }
  }
}


// count the number of chars in a string, update a second field with the current count, if the count excedes a set limit then switch textfield text red
function countChars(text, countfield, fieldLength){

	var count = text.length;
	var counter = fieldLength - count;
	document.getElementById(countfield).innerHTML  = counter;
	if(count >= fieldLength)
	{
		document.getElementById(countfield).className = 'Normal_font_red';
	}
	else
	{
		document.getElementById(countfield).className = '';
	}
}

function DisplaySub(strobj,display){
	document.getElementById(strobj).style.visibility = display;
}

// apply the given style to the given element
function switchStyle(el, cl){
	document.getElementById(el).className= cl;
}

// apply the given style to the list of elements in the array
function switchStyles(idList, style){
	for(var i = 0; i < idList.length; i++)
	{
		document.getElementById(idList[i]).className= style;
	}
}

// pass an array of arrays
// apply each style in each array to its given element
function switchStyleList(idStyleArray)
{
	for(var i = 0; i < idStyleArray.length; i++)
	{
		document.getElementById(idStyleArray[i][0]).className = idStyleArray[i][1];
	}
}

// pass an array of elements
// if element id matches the selected id then change to main style, else change to the other
function switchStyleList(idArray, matchCriteria, style1, style2)
{
	// loop through all of the elements
	for(var i = 0; i < idArray.length; i++)
	{
		// if the element matches the selected element then switch to style 1
		if(idArray[i] == matchCriteria)
		{
			document.getElementById(idArray[i]).className= style1;
		}
		else
		{
			document.getElementById(idArray[i]).className= style2;
		}
	}
}

function submitForm(formObj)
{
	document.formObj.submit();
}

function submitForm(formObj, submitTo)
{
	document.formObj.action = submitTo;
	document.formObj.submit();
}

// function to ensure that a specified field has a specified value
// return true / false on outcome
// optional alert message if condition not satisfied
function checkFields(field, notValue, message)
{
	if(document.getElementById(field) == null)
	{
		if(message != '')
		{
			$j.prompt(message);
		}
		return false;
	}
	else if(document.getElementById(field).value == notValue)
	{
		if(message != '')
		{
			$j.prompt(message);
		}
		return false;
	}
	else
	{
		return true;
	}
}

// load a list with items from an array, filter using the filter
function loadListFromArray(listarray, listload, filter, firstItemText)
{
	// first clear the existing list
	for (i = listload.options.length; i >= 0; i--) 
	{ 
		listload.options[i] = null; 
	} 
	listload.length = 0;
	
	j = 0;
	// add default start text 
	if(firstItemText != '')
	{
		listload.options[0] = new Option(firstItemText);
		listload.options[0].value = '';
		j = 1;
	}
	
	// now loop through and add the new options and their values from the array
	if (listarray != null) 
	{ 
		for (i = 0; i < listarray.length; i++) 
		{ 
			if(listarray[i][0] == filter)
			{
				listload.options[j] = new Option(listarray[i][3]); 
				listload.options[j].value = listarray[i][2]; 
				j++;
			} 
		} 
		listload.options[0].selected = true; 
	}
}

// write the given string to a field
function writeToField(field, value)
{
	document.getElementById(field).value = value;
}

// append the given string to a field
function appendToField(field, value)
{
	document.getElementById(field).value = document.getElementById(field).value.concat(value);
}

// reset the given menu / list to the specified index
function resetList(list, index)
{
	list.selectedIndex = index;
}

// pop up a yes/no dialog box displaying the specified message. If the user confirms yes then submit the specified link.
function confirmAction (action, message)
{
	var confirmThis = confirm (message);
	if(confirmThis)
	{
		document.location = action;
	}
	else
	{
		return false;
	}
}


// empty a list/menu (selectCtrl) and then fill it with the contents of an array (itemArray)
function fillSelectFromArray(selectCtrl, itemArray, coid) { 
	var i, j; 
	var prompt; 
	
	// empty existing items 
	for (i = selectCtrl.options.length; i >= 0; i--) 
	{ 
		selectCtrl.options[i] = null; 
	} 
	selectCtrl.length = 0;
	
	j = 0;
	if (itemArray != null) 
	{ 
		// add new items 
		for (i = 0; i < itemArray.length; i++) 
		{ 
			if(coid == itemArray[i][2])
			{
				selectCtrl.options[j] = new Option(itemArray[i][1]); 
				selectCtrl.options[j].value = itemArray[i][0]; 
				j++;
			} 
		} 
	}	
} 

// Test if the given element is 'on' if so switch if off and vice-versa
// styles kept in style arrays
var A_row = new Array('rowselected', 'Normal_font');
var A_visibility = new Array('hid', 'vis');

function switchStyleOnOff(id, array)
{
	// get the style array to use
	if(array == 'A_row')
	{
		array = A_row;
	}
	else if(array == 'A_visibility')
	{
		array = A_visibility;
	}
	
	if(document.getElementById(id).className == array[0])
	{
		document.getElementById(id).className = array[1];
	}
	else
	{
		document.getElementById(id).className = array[0];
	}
}

// write the text value to the element specified
function writeToTextField(text, element, type)
{
	if (text == '') 
	{
 		document.getElementById(element).value="";
	}
	else if ((text>="x1") && (text<="x5"))
	{
    	document.getElementById(element).value+=text + ", ";
	}
    else if(type == 'acc')
    {	
		document.getElementById(element).value+= "with " + text + " ";
	}
	else 
	{
		document.getElementById(element).value += text;
	}
	document.getElementById(element).focus = true;;
	return(true);
}

// return the currently selected button value from a radio group
function getSelectedRadio(buttonGroup) 
{
   // returns the array number of the selected radio button or -1 if no button is selected
   if (buttonGroup[0]) 
   { 
   	  // if the button group is an array (one button is not an array)
      for (var i=0; i<buttonGroup.length; i++) 
	  {
         if (buttonGroup[i].checked) 
		 {
            return buttonGroup[i].value;
         }
      }
   } 
   else 
   {
      if (buttonGroup.checked) 
	  { 
	  	buttonGroup.value; 
	  }
   }
   return null;
} 

/**
* DOM method used for checking XML for whitespace elements between nodes.
* For example: <aNode></aNode>
* In Mozilla: document.getElementById('aNode').childNodes.length will return 1 (i.e. 1 whitespace child textnode
* In IE: the above will return 0 (IE doesen't think whitespace consititues separate textnode)
*/
function getNodeValue(node)
{
	var value = '';
	if(node.childNodes == null)
	{
		return '';
	}
	else if(node.childNodes.length < 1)
	{
		return '';
	}
	else
	{
		return node.firstChild.nodeValue;
	}
}

/**
* Horrible little function that just tests if the current browser is in the
* IE family.
*/ 
function isIE()
{
	var isie = false;
	if(navigator.appName == "Microsoft Internet Explorer")
	{
		 isie = true;
	}
	return isie;
}

/**
* Tests if the element with the given ID acutally exists.
*/
function elementExists(elementId)
{	
	var exists = false;
	if(elementId != null && elementId != '')
	{
		try
		{
			exists = elementObjectExists(document.getElementById(elementId));;
		}
		catch(e){exists = false;}
	}
	return exists;	
}

function elementObjectExists(elementObj)
{
	var exists = false;
	try
	{
		if(elementObj != null)
		{
			exists = true;
		}
	}
	catch(e){exists = false;}
	return exists;	
}

function hideSelects(visibility)
{
	var selects = document.getElementsByTagName('select');
	for(i = 0; i < selects.length; i++) 
	{
		selects[i].style.visibility = visibility;
	}
}

function deleteElement(id)
{
	if(elementExists(id))
	{
		try
		{
			var deleteElement = document.getElementById(id);
			deleteElement.parentNode.removeChild(deleteElement);
		}
		catch(e){$j.prompt(i18n.t("core.utilities:unableToDeleteElement", "unable to delete element"));}
	}
}

function getDocImage(fileName)
{
	var imageSrc = '';
	
	var fileType = 'unknown';
	var lio = fileName.lastIndexOf('.');
	if(lio > 0)
	{
		fileType = fileName.substring(lio + 1, fileName.length);
	}
	
	if(fileType != 'unknown')
	{
		if(fileType == 'html' || fileType == 'htm')
		{
			imageSrc = 'img/icons/doc_icons/html.gif';
		}
		else if(fileType == 'doc' || fileType == 'rtf')
		{
			imageSrc = 'img/icons/doc_icons/word_small.gif';
		}
		else if(fileType == 'xls' || fileType == 'csv')
		{
			imageSrc = 'img/icons/doc_icons/csv_small.jpg';
		}
		else if(fileType == 'pdf')
		{
			imageSrc = 'img/icons/doc_icons/pdf_small.gif';
		}
	}
	return imageSrc;
}

function getFileExtension(filePath)
{
	var fileType = 'unknown';
	var lio = filePath.lastIndexOf('.');
	if(lio > 0)
	{
		fileType = filePath.substring(lio + 1, filePath.length);
	}
	return fileType;
}

/**
* Return the filename of the given filepath. 
* @param filePath - the full absoulte / relative filepath.
*/
function getFileName(filePath)
{
	var fileName = 'file';
	if(filePath.lastIndexOf('\\') > 0)
	{
		fileName = filePath.substring(filePath.lastIndexOf('\\') + 1, filePath.length);
	}
	else if(filePath.lastIndexOf('/') > 0)
	{
		fileName = filePath.substring(filePath.lastIndexOf('/') + 1, filePath.length);
	}
	return fileName;
}

/*
 * Utility function to check all checkboxes with a specific name within a form.
 * @param (String) name of form to get all checkboxes.
 * @param (String) name of the checkbox elements.
 * @param (String) value to append to checkbox (true, false).
 * @param (String) id of the anchor which is used to select all.
 */
function SetAllCheckBoxes(FormName, FieldName, CheckValue, id)
{
	if(!document.forms[FormName])
	{	
		return;
	}
	var objCheckBoxes = document.forms[FormName].elements[FieldName];
	if(!objCheckBoxes)
	{
		return;
	}
	var countCheckBoxes = objCheckBoxes.length;
	if(!countCheckBoxes)
	{	
		objCheckBoxes.checked = CheckValue;
	}
	else
	{	// set the check value for all check boxes
		for(var i = 0; i < countCheckBoxes; i++)
		{	
			objCheckBoxes[i].checked = CheckValue;
		}
	}
	removeChildren(id);
	if (CheckValue == true)
	{
		id.appendChild(document.createTextNode(i18n.t("core.utilities:deselectAll", "De-Select All")));
		id.onclick = function()
						{
							SetAllCheckBoxes(FormName, FieldName, false, this); return false;
						}
	}
	else
	{
		id.appendChild(document.createTextNode(i18n.t("core.utilities:selectAll", "Select All")));
		id.onclick = function()
						{
							SetAllCheckBoxes(FormName, FieldName, true, this); return false;
						}
	}
}

/*
 * Utility function to check all checkboxes within a specific element.
 * @param (String) id of element to get all checkboxes.
 * @param (String) FieldName name of the checkbox elements (if left blank then all checkboxes in id will be selected/deselected).
 * @param (String) CheckValue value to append to checkbox (true, false).
 * @param (String) anchorId of the anchor which is used to select all.
 * @param (String) extraText optional text which can be appended to the end of anchor.
 */
function SetAllElementCheckBoxes(id, FieldName, CheckValue, anchorId, extraText)
{
	if($j('#' + id).length == 0)
	{	
		return;
	}
	var objCheckBoxes = $j('#' + id + ' input');
	if(!objCheckBoxes)
	{
		return;
	}
	var countCheckBoxes = objCheckBoxes.length;
	if(!countCheckBoxes)
	{	
		objCheckBoxes.checked = CheckValue;
	}
	else
	{	// set the check value for all check boxes
		for(var i = 0; i < countCheckBoxes; i++)
		{	
			if (FieldName == '')
			{
				objCheckBoxes[i].checked = CheckValue;
			}
			else if (objCheckBoxes[i].getAttribute('name') == FieldName)
			{
				objCheckBoxes[i].checked = CheckValue;
			}
		}
	}
	if (CheckValue == true)
	{
		$j(anchorId).text(i18n.t("core.utilities:deselectAll", "De-Select All") + ' ' + extraText);
		anchorId.onclick = function()
						{
							SetAllElementCheckBoxes(id, FieldName, false, this, extraText); return false;
						}
	}
	else
	{
		$j(anchorId).text(i18n.t("core.utilities:selectAll", "Select All") + ' ' + extraText);
		anchorId.onclick = function()
						{
							SetAllElementCheckBoxes(id, FieldName, true, this, extraText); return false;
						}
	}
}

/**
 * Utility function to append the given text to a textarea.
 * @param {String} text the text to append to the textarea.
 * @param {String} appendToId the id of the textarea element to append the text to.
 */
function appendTextToTextarea(text, appendToId)
{
	if(text)
	{
		if(text != "")
		{
			try
			{
				var textArea = document.getElementById(appendToId);
				textArea.value = textArea.value + " " + text;
			}
			catch(e){$j.prompt(e);}		
		}
	}
}

/**
 * Toggles the display of the element passed as argument 1 to
 * the opposite of it's current display ('none' or 'block'). 
 * Also changes the text value of the 'a' element according to the
 * display status.
 * @param displayId the id of the element to display or hide
 * @param a the anchor element calling this method
 * @param array of descriptions to show as the a link text - 0 = display, 1 = hidden
 * @requires DOMUtils.js
 */
function switchDivAndHrefDisplay(displayId, a, text)
{
	try
	{
		var displayElement = $(displayId);
		if(displayElement.style.display == 'none')
		{
			displayElement.style.display = 'block';
			removeChildren(a);
			a.appendChild(document.createTextNode(text[0]));
		}
		else
		{
			displayElement.style.display = 'none';
			removeChildren(a);
			a.appendChild(document.createTextNode(text[1]));
		}
	}
	catch(e)
	{
		$j.prompt(e);
	}	
}

/**
 * Toggles the display of the element passed as argument 1 to
 * the opposite of it's current display ('none' or 'block'). 
 * Also changes the text value of the 'a' element according to the
 * display status.
 * @param elementid the id of the element to display or hide
 * @param effect the type of effect to apply to area
 * @param a the anchor element calling this method
 * @param text array of descriptions to show as the a link text - 0 = display, 1 = hidden
 * @requires DOMUtils.js, prototype.js, effects.js
 */
function slideTermsAndCond(elementid, a)
{
	try
	{	
		// anchor contains the string 'View' so show info and change link text
		//if (a.firstChild.nodeValue.indexOf('View') != -1)
		if ($j('#' + elementid)[0].style.display == 'none')
		{
			$j('#' + elementid).slideToggle(2000);
			$j(a).empty().text(i18n.t("core.utilities:hide", "Hide"));
		}
		else
		{
			$j('#' + elementid).slideToggle(2000);
			$j(a).empty().text(i18n.t("core.utilities:view", "View"));
		}
	}
	catch(e)
	{
		$j.prompt(e);
	}	
}

/**
 * this function is intended for use when toggling a company, subdivision, contact or address between being
 * active or inactive.
 * 
 * @param {String} type string type of object being toggled (e.g. Company, Subdivision, Contact, Address)
 * @param {Integer} id id of the object being toggled (e.g coid, subdivid, personid, addrid)
 * @param {String} name name of company, subdivision, contact or address
 * @param {Boolean} activate boolean value to toggle (e.g. if currently true then change to false)
 * @param {String} warningMessage message to be displayed to user
 * @param {Integer} coid id of the company, null if not necessary
 */
function toggleThickboxContentForActivation(type, id, name, activate, warningMessage, coid)
{
	// populate variables with text to use in thickbox window and activation variable to false
	// in preparation for creating thickbox content when activate parameter is true.
	var objText = null;
	if (type == 'Address') 
		objText = i18n.t("core.utilities:deactivateAddress", "Deactivate Address");
	else if (type == 'Company')
		objText = i18n.t("core.utilities:deactivateCompany", "Deactivate Company");
	else if (type == 'Contact')
		objText = i18n.t("core.utilities:deactivateContact", "Deactivate Contact");
	else if (type == 'Subdivision')
		objText = i18n.t("core.utilities:deactivateSubdiv", "Deactivate Subdivision");	
	var labelText = i18n.t("core.utilities:deactivationReasonLabel", "Deactivation Reason:");
	var activateObj = false;
	
	// activate parameter is in fact false so reset all variables with text to use in thickbox window
	// and activation variable to true.
	if (activate == false)
	{
		if (type == 'Address') 
			objText = i18n.t("core.utilities:activateAddress", "Activate Address");
		else if (type == 'Company')
			objText = i18n.t("core.utilities:activateCompany", "Activate Company");
		else if (type == 'Contact')
			objText = i18n.t("core.utilities:activateContact", "Activate Contact");
		else if (type == 'Subdivision')
			objText = i18n.t("core.utilities:activateSubdiv", "Activate Subdivision");
		labelText = i18n.t("core.utilities:activateReasonLabel", "Activate Reason:");
		activateObj = true;
	}
	
	// create content to be displayed in thickbox window using previously populated variables
	var overlayContent = 	'<div id="toggleActivationOverlay">' +
								warningMessage +
								'<fieldset>' +
									'<ol>' +
										'<li>' +
											'<label>' + labelText + '</label>' +
											'<textarea name="reason" id="reason" rows="6" class="width80"></textarea>' +
										'</li>' +
									'</ol>' +
								'</fieldset>' +
								'<div class="center">' + 
									'<input type="submit" id="submit" name="' + i18n.t("submit", "Submit") + '" value="' + objText + '" onclick=" this.disabled = true; updateActivationStatus(\'' + type + '\', ' + id + ', ' + activateObj + ', $j(\'#reason\').val()); " />' +
								'</div>' +
							'</div>';
	// de-activating contact
	if(type == 'Contact' && activate == true)
	{	
		 $j.getJSON("checkContactDeactivationStatus.json", {personId: id},
		    		function(data){
		    			var activeJobs = data['activeJobs'];
		    			var instCount = data['instcount'];
		    			
			    			if((instCount != null) && (instCount > 0) || (activeJobs != null) && (activeJobs > 0))
							{
			    				// override overlay content because the contact owns instruments
								overlayContent = 	'<div id="toggleActivationOverlay">' +
														'<div class="warningBox1">' +
															'<div class="warningBox2">' +
																'<div class="warningBox3">' +
																	'<p>';
																	if (instCount != 1){
																		overlayContent += i18n.t("core.utilities:contactOwnerOfNbInstrs", {varNbInstr : instCount, defaultValue : "This contact is still the owner of " + instCount + "instruments."}) + '</p>';
																	}
																	else{
																		overlayContent += i18n.t("core.utilities:contactOwnerFOfNbInstr", {varNbInstr : instCount, defaultValue : "This contact is still the owner of " + instCount + "instrument."}) + '</p>';
																	}
																	if(activeJobs != 1){
																		overlayContent += i18n.t("core.utilities:contactOwnerOfNbJob", {varNbInstr : activeJobs, defaultValue : "This contact is still the owner of " + activeJobs + "Jobs."}) + '</p>';
																	}else{
																		overlayContent += i18n.t("core.utilities:contactOwnerOfNbJob", {varNbInstr : activeJobs, defaultValue : "This contact is still the owner of " + activeJobs + "job."}) + '</p>';
																	}
																	
													overlayContent +=	'<p>' + i18n.t("core.utilities:contactInfoDeactivation", {varPage : '<a href="viewcompanyinstrument.htm?coid=' + coid + '" class="mainlink" target="_blank" onclick=" loadScript.closeOverlay(); ">Company Instrument Search Page</a>', defaultValue : 'Please visit the <a href="viewcompanyinstrument.htm?coid=' + coid + '" class="mainlink" target="_blank" onclick=" loadScript.closeOverlay(); ">Company Instrument Search Page</a> ' + 
																	'to re-assign all instruments before returning to de-activate this contact.'}) + '</p>' +
																'</div>' +
															'</div>' +
														'</div>' +
													'</div>';
							}
			    			
			    		
			    			// create new overlay
			    			loadScript.createOverlay('dynamic', objText + ' - ' + name, null, overlayContent, null, 40, 880, null);
						
		    		});
	}
	else if(type == 'Address' && activate == true)
	{
		// load the service javascript file if not already	
		loadScript.loadScriptsDynamic(new Array('dwr/interface/addressservice.js'), true,
		{
			callback: function()
			{
				addressservice.checkAddressDeactivationStatus(id, 
				{
					callback:function(instcount)
					{
						// instruments to be moved from contact?
						if((instcount != null) && (instcount > 0))
						{							
							// override overlay content because the contact owns instruments
							overlayContent = 	'<div id="toggleActivationOverlay">' +
													'<div class="warningBox1">' +
														'<div class="warningBox2">' +
															'<div class="warningBox3">' +
																'<p>';
																if (instcount != 1)
																{
																	overlayContent += i18n.t("core.utilities:addressDesignatedForNbInstrs", {varNbInstr : instcount, defaultValue : "This is still the designated address for " + instcount + "instruments."}) + '</p>';
																}
																else
																{
																	overlayContent += i18n.t("core.utilities:addressDesignatedForNbInstr", {varNbInstr : instcount, defaultValue : "This is still the designated address for " + instcount + "instrument."}) + '</p>';
																}

											overlayContent +=	'<p>' + i18n.t("core.utilities:addressInfoDeactivation", {varPage : '<a href="viewcompanyinstrument.htm?coid=' + coid + '" class="mainlink" target="_blank" onclick=" loadScript.closeOverlay(); ">Company Instrument Search Page</a>', defaultValue : 'Please visit the <a href="viewcompanyinstrument.htm?coid=' + coid + '" class="mainlink" target="_blank" onclick=" loadScript.closeOverlay(); ">Company Instrument Search Page</a> ' + 
															'to re-assign all instruments before returning to de-activate this address.'}) + '</p>' +
														'</div>' +
													'</div>' +
												'</div>' +
											'</div>';
						}						
						// create new overlay
						loadScript.createOverlay('dynamic', objText + ' - ' + name, null, overlayContent, null, 40, 880, null);
					}
				});
			}
		});
	}
	else
	{
		// create new overlay
		loadScript.createOverlay('dynamic', objText + ' - ' + name, null, overlayContent, null, 40, 880, null);
	}
}

/*
 * this function is called from the onclick event in 'toggleThickboxContentForActivation()' function and
 * calls a dwr function dependant on the string contained within the object parameter.  The dwr function
 * either activates or deactivates the object passed (e.g. company, subdivision, contact, address).
 * @param (type) - String name of object being toggled (e.g. Company, Subdivision, Contact, Address)
 * @param (id) - Integer id of the object being toggled (e.g coid, subdivid, personid, addrid)
 * @param (activate) - boolean value to toggle (e.g. if currently true then change to false)
 * @param (reason) - String containing the reason for activation or deactivation.
 */
function updateActivationStatus(type, id, activate, reason)
{
	// text to be displayed once the dwr call has been completed
	var returntext = '';
	// status that the object has been changed to
	var currentStatusKeyId = "active";
	var currentStatusKey = i18n.t("core.utilities:active", "active");
	// status that the object had before this change
	var previousStatusKeyId = "inactive";
	var previousStatusKey = i18n.t("core.utilities:inactive", "inactive");
	if (activate == false)
	{
		currentStatusKey = i18n.t("core.utilities:inactive", "inactive");
		currentStatusKeyId = "inactive";
		previousStatusKey = i18n.t("core.utilities:active", "active");
		previousStatusKeyId = "active";
	}
	// find the string object passed
	switch (type)
	{
		case 'Company': {
				$j.ajax({
					url: "changecompanyactive.json",
					data: {coid: id, activate: activate, reason: reason},
				}).done(function(date) {
					// create the message to return
					returntext = '<p class="center bold">' + i18n.t("core.utilities:companyStatusSet", {varStatus : '<strong>' + currentStatusKey + '</strong>', defaultValue : "The status of this company has been set to <strong>" + currentStatusKey + "</strong>."}) + '</p>';
					if (activate)
					{
						$j('div#subdivs-tab tbody td.name a').removeClass().addClass('mainlink');
						$j('#warningBoxInactive').removeClass().addClass('hid');
					}
					else
					{
						$j('div#subdivs-tab tbody td.name a').removeClass().addClass('mainlink-strike');
						$j('#warningBoxInactive').removeClass().addClass('vis');
						$j('#deactDateSpan').html(cwms.dateFormatFunction(date));
						
					}
					// modify the overlay for success
					loadScript.modifyOverlay(null, returntext, i18n.t("core.utilities:companySuccess", "Company Success"), null, 20, 880, null);
					// update page
					$j('#' + currentStatusKeyId).removeClass().addClass('vis');
					$j('#' + previousStatusKeyId).removeClass().addClass('hid');
				}).fail(function(result) {
					// modify the overlay for error
					loadScript.modifyOverlay(null, result.message, i18n.t("core.utilities:companyCouldNotBeUpdated", "Company could not be updated"), true, null, null, null);
					// enable submit button
					$j('div#toggleActivationOverlay input[type="submit"]').attr('disabled', false);
				});
				if (reason.trim() == "") {
					$j('#noDeactReason').removeClass().addClass("vis");
					$j('#deactReason').removeClass().addClass("hid");
				}
				else {
					$j('#noDeactReason').removeClass().addClass("hid");
					$j('#deactReason').removeClass().addClass("vis").html(reason);
				}
				break;
			}
		case 'Subdivision': {
				$j.ajax({
					url: "changesubdivactive.json",
					data: {subdivid: id, activate: activate, reason: reason},
					async: false
				}).done(function(date) {
					// create the message to return
					returntext = '<p class="center bold">' + i18n.t("core.utilities:subdivStatusSet", {varStatus : '<strong>' + currentStatusKey + '</strong>', defaultValue : "The status of this subdivision has been set to <strong>" + currentStatusKey + "</strong>."}) + '</p>';
					if (activate)
					{
						$j('div#contactActive-tab tbody td.name a').removeClass().addClass('jconTip mainlink');
						$j('#warningBoxInactive').removeClass().addClass('hid');
					}
					else
					{
						$j('div#contactActive-tab tbody td.name a').removeClass().addClass('jconTip mainlink-strike');
						$j('#warningBoxInactive').removeClass().addClass('vis');
						$j('#deactDateSpan').html(cwms.dateFormatFunction(date));
					}
					// modify the overlay for success
					loadScript.modifyOverlay(null, returntext, i18n.t("core.utilities:subdivSuccess", "Subdivision Success"), null, 20, 880, null);
					// update page
					$j('#' + currentStatusKeyId).removeClass().addClass('vis');
					$j('#' + previousStatusKeyId).removeClass().addClass('hid');
				}).fail(function(result) {
					// modify the overlay for error
					loadScript.modifyOverlay(null, result.message, i18n.t("core.utilities:subdivCouldNotBeUpdated", "Subdivision could not be updated"), true, null, null, null);
					// enable submit button
					$j('div#toggleActivationOverlay input[type="submit"]').attr('disabled', false);
				});
				break;
			}				
		case 'Address': {
				$j.ajax({
					url: "changeaddressactive.json",
					data: {addressid: id, activate: activate, reason: reason},
					async: false
				}).done(function() {
					// create the message to return
					returntext = '<p class="center bold">' + i18n.t("core.utilities:addressStatusSet", {varStatus : '<strong>' + currentStatusKey + '</strong>', defaultValue : "The status of this address has been set to <strong>" + currentStatusKey + "</strong>."}) + '</p>';
					// modify the overlay for success
					loadScript.modifyOverlay(null, returntext, i18n.t("core.utilities:addressSuccess", "Address Success"), null, 20, 880, null);
					// update page
					$j('#' + currentStatusKeyId).removeClass().addClass('vis');
					$j('#' + previousStatusKeyId).removeClass().addClass('hid');
					if (activate) $j('#deactivewarning').empty();
				}).fail(function(result) {
					// modify the overlay for error
					loadScript.modifyOverlay(null, result.message, i18n.t("core.utilities:addressCouldNotBeUpdated", "Address could not be updated"), true, null, null, null);													
					// enable submit button
					$j('div#toggleActivationOverlay input[type="submit"]').attr('disabled', false);
				});
				break;
			}
		case 'Contact': {
				$j.ajax({
					url: "changecontactactive.json",
					data: {personid: id, activate: activate, reason: reason},
					async: false
				}).done(function() {
					// create the message to return
					returntext = '<p class="center bold">' + i18n.t("core.utilities:contactStatusSet", {varStatus : '<strong>' + currentStatusKey + '</strong>', defaultValue : "The status of this contact has been set to <strong>" + currentStatusKey + "</strong>."}) + '</p>';
					if (activate) {
						$j('#warningBoxInactive').removeClass().addClass('hid');
					}
					else {
						$j('#warningBoxInactive').removeClass().addClass('vis');
					}
					// modify the overlay for success
					loadScript.modifyOverlay(null, returntext, i18n.t("core.utilities:contactSuccess", "Contact Success"), null, 20, 880, null);
					// update page
					$j('#' + currentStatusKeyId).removeClass().addClass('vis');
					$j('#' + previousStatusKeyId).removeClass().addClass('hid');
				}).fail(function(result) {
					// modify the overlay for error
					loadScript.modifyOverlay(null, result.message, i18n.t("core.utilities:contactCouldNotBeUpdated", "Contact could not be updated"), true, null, null, null);
					// enable submit button
					$j('div#toggleActivationOverlay input[type="submit"]').attr('disabled', false);
				});
				break;
			}
	}
}