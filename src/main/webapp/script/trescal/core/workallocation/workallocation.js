
var jquiAutocomplete = new Array(
    {
        inputFieldName: 'desc',
        inputFieldId: 'descId',
        source: 'searchdescriptiontags.json'
    });


var menuElements = new Array( 	{ anchor: 'unallocated-link', block: 'unallocated-tab' },
                                { anchor: 'allocated-link', block: 'allocated-tab' });


var menuElements2 = new Array(
    { anchor: 'searchresult-link', block: 'searchresult-tab' },
    { anchor: 'basket-link', block: 'basket-tab' });


var niftyElements = new Array ( { element: 'div.infobox', corner: 'normal' } );


function changeDateOption(between, id)
{
    // show between?
    if(between == 'true')
    {
        // display between
        $j('#' + id).removeClass('hid').addClass('vis');
    }
    else
    {
        // hide between
        $j('#' + id).removeClass('vis').addClass('hid');
    }
}

function populateSubLabSelect(){
    let LabId = $j("#laboratoryId :selected").val();

    $j("#subLabId").find("option").remove().end().append('<option value="">---- ALL ----</option>').val('');

    $j.ajax(
        {
            url: "getSubLaboratoriesByLabId.json?LabId="+LabId,
            dataType: 'json',
            success: function(data)
            {
                $j.each(data, function( index, value ) {
                    let opt = document.createElement("option");
                    opt.value = value.id;
                    opt.innerHTML = value.name +"  "+ value.description;
                    $j("#subLabId").append(opt);
                });
            }
        });


}


function populatClientSubdivSelect(){

    let clientCompanyId = $j("#clientCompanyId :selected").val();
    $j("#clientSubdivId").find("option").remove().end().append('<option value="">---- ALL ----</option>').val('');

    $j.ajax(
        {
            url: "getSubdivsByCoid.json?coid="+clientCompanyId,
            dataType: 'json',
            success: function(data)
            {
                $j.each(data, function( index, value ) {
                    let opt = document.createElement("option");
                    opt.value = value.subdivid;
                    opt.innerHTML = value.subname;
                    $j("#clientSubdivId").append(opt);
                });
            }
        });

}

function switchfielset(id){
    if(id=='searchfieldset'){
        $j('#' + id).removeClass('hid').addClass('vis');
        $j('#basketfieldset').removeClass('vis').addClass('hid');

    }else if(id='basketfieldset'){
        $j('#' + id).removeClass('hid').addClass('vis');
        $j('#searchfieldset').removeClass('vis').addClass('hid');
    }
}


function selectAllItemsOfJob(jobId){
    if($j('#searchresult-tab #searchResults-'+jobId+' tbody tr td:first').find('input').prop('checked')==true){
        $j('#searchresult-tab #itemsOfJob-'+jobId+' tbody tr').each(function(index, tr) {
            $j(tr).children('td:first').find('input').prop('checked', true);
        });
    }else if($j('#searchresult-tab #searchResults-'+jobId+' tbody tr td:first').find('input').prop('checked')==false){
        $j('#searchresult-tab #itemsOfJob-'+jobId+' tbody tr').each(function(index, tr) {
            $j(tr).children('td:first').find('input').prop('checked', false);
        });
    }
}

function copyDown(id,index,prefix,className){
    let valuer = document.getElementById(id).value;
    let counter = document.getElementsByClassName(className).length;
    for (let i = index; i < counter; i++) {
        document.getElementById(prefix+"-"+i).value = valuer;
    }
}

function copyUp(id,index,prefix,className){
    let valuer = document.getElementById(id).value;
    for (let j = index; j >= 0; j--) {
        document.getElementById(prefix+"-"+j).value = valuer;
    }
}


function populateAllocatedSubLabSelect(){
    let LabId = $j("#laboratoryId_2 :selected").val();

    $j("#subLabId_2").find("option").remove().end().append('<option value="">---- ALL ----</option>').val('');

    $j.ajax(
        {
            url: "getSubLaboratoriesByLabId.json?LabId="+LabId,
            dataType: 'json',
            success: function(data)
            {
                $j.each(data, function( index, value ) {
                    let opt = document.createElement("option");
                    opt.value = value.id;
                    opt.innerHTML = value.name +"  "+ value.description;
                    $j("#subLabId_2").append(opt);
                });
            }
        });


}


function populatAllocatedClientSubdivSelect(){

    let clientCompanyId = $j("#clientCompanyId_2 :selected").val();
    $j("#clientSubdivId_2").find("option").remove().end().append('<option value="">---- ALL ----</option>').val('');

    $j.ajax(
        {
            url: "getSubdivsByCoid.json?coid="+clientCompanyId,
            dataType: 'json',
            success: function(data)
            {
                $j.each(data, function( index, value ) {
                    let opt = document.createElement("option");
                    opt.value = value.subdivid;
                    opt.innerHTML = value.subname;
                    $j("#clientSubdivId_2").append(opt);
                });
            }
        });

}

function copyDownForAllocateItems(id,index,prefix,className){
    let valuer = document.getElementById(id).value;
    let counter = document.getElementsByClassName(className).length;
    for (let i = index; i < counter; i++) {
        document.getElementById(prefix+"-"+i).value = valuer;
    }
}

function copyUpForAllocateItems(id,index,prefix,className){
    let valuer = document.getElementById(id).value;
    for (let j = index; j >= 0; j--) {
        document.getElementById(prefix+"-"+j).value = valuer;
    }
}

function showAlloctedItems(id){
    if( $j("[id="+id+"]").hasClass("hid")){
        $j("[id="+id+"]").removeClass("hid");
    }else if($j("[id="+id+"]").hasClass("hid") == false){
        $j("[id="+id+"]").addClass("hid");
    }
}


function reallocateitem(allocationId,EngineerId,capabiltyId,allocationMessageId) {

    var contentUri = 'reallocteoverlay.htm';
    contentUri = contentUri + '?allocationId=' + allocationId +"&EngineerId="+EngineerId+"&capabiltyId="+capabiltyId+"&allocationMessageId="+allocationMessageId;
    loadScript.createOverlay('external', "Edit", contentUri, null, null,80, 820, null);
}

function deallocatitem(allocationId){
    var yes = i18n.t("yes", "Yes");
    var no = i18n.t("no", "No");
    $j.prompt(i18n.t("delete", "delete")+" ?", {
        buttons: { [no]: false, [yes]: true },
        submit: function(e,v,m,f){

            if(v){
                // make the ajax call
                $j.ajax({
                    url: "deleteallocation.json?allocationId="+allocationId,
                    dataType:'json'
                }).done(function(res) {
                    console.log(res);
                    if(res.success===true){
                        // refresh page
                        window.location.reload();
                    }
                });
            }
        }
    });

}

function submitoverlay(id,allocationMessageId){
    let engineerId = $j("#engineer_overlay").val();
    let comments = $j("#cmt_overlay").val();

    $j.ajax({
        url: "reallocation.json?allocationId="+id+"&engineerId="+engineerId+"&comment="+comments+"&allocationMessageId="+allocationMessageId,
        dataType:'json'
    }).done(function(res) {
        if(res.success===true){
            // refresh page
            window.location.reload();
        }
    });

}

function resetForm(){
    window.location.href = "newallocateitemstoengineer.htm";
    document.getElementById("myForm").reset();
}

function addRequiredToProdDate(){
    let autoSchedualing = document.getElementById("basketForm.autoSheduling").value;
    if(autoSchedualing=="NO"){
        let counter = document.getElementsByClassName("itemDate").length;
        for (let i = 0; i < counter; i++) {
            document.getElementById("date-"+i).setAttribute("required", "required");
        }
    }else if(autoSchedualing=="YES"){
        let counter = document.getElementsByClassName("itemDate").length;
        for (let i = 0; i < counter; i++) {
            document.getElementById("date-"+i).removeAttribute("required");
        }
    }
}


function removeItemFromBasket(tableId,trId){
    var rowCount = $j("#BasketitemsOfJob-"+tableId+" tr").length - 1;
    if(rowCount > 1){
        $j("tr [id="+trId+"]").remove();
    }else{
        $j("#basketItems-"+tableId).remove();
    }
}

function checkAllItems(checkBoxesClass,checkBoxId){
    if($j("#"+checkBoxId).prop('checked')==true){
        $j("."+checkBoxesClass).prop('checked', true);
    }else if($j("#"+checkBoxId).prop('checked')==false){
        $j("."+checkBoxesClass).prop('checked', false);
    }
}




