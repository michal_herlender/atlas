/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	RecallToQuotation.js
*	DESCRIPTION		:	-
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	22/09/2010
*					: 	28/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * array of calendar objects containing the initial setup literals needed
 * @param inputField (String) id of the input field to which the calendar should be binded
 * @param dateFormat (String) format that the date should be displayed
 * @param displayDates (String) 'all' make all dates available
 * 								'future' show only dates in the future
 * 								'past' show only dates in the past
 * applied in init() function.
 */								

/**
 * this method loads all prohibition items that would have appeared on this recall, any instruments on
 * a current job are filtered out.
 * 
 * @param {Object} anchor object to manipulate
 * @param {Integer} recallid id of the recall
 * @param {String} recallType type of this recall (i.e. 'CONTACT' or 'ADDRESS')
 * @param {Integer} id identifier of either a contact or address
 */
function loadProhibitionInstruments(anchor, recallid, recallType, id)
{
	// set rowcount to large figure
	var rowcount = 1000;
	// change anchor text
	$j(anchor).text('Loading.....');
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/recallservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to retrieve any prohibition instruments
			recallservice.getProhibitionListDWR(recallid, recallType, id,
			{
				callback:function(result)
				{
					// result successful?
					if (result.success)
					{
						// any prohibition items returned?
						if (result.results.length > 0)
						{
							// load javascript service file
							loadScript.loadScriptsDynamic(new Array('script/trescal/core/utilities/TooltipLinks.js'), true,
							{
								callback: function()
								{
									// variable to hold new content
									var content = '';
									// add all prohibition items to table and display
									$j.each(result.results, function(i, inst)
									{
										content += 	'<tr>' +
														'<td class="addtoquote"><input type="checkbox" name="toQuotePlantIds" value="' + inst.plantid + '" /></td>' +
														'<td class="barcode">' + instrumentLinkDWRInfo(inst, rowcount, true, false, true, 0) + '</td>' +
														'<td class="inst">' + inst.definitiveInstrument + '</td>' +
														'<td class="serial">' + inst.serialno + '</td>' +
														'<td class="plant">' + inst.plantno + '</td>' +
														'<td class="duedate">' + formatDate(inst.nextCalDueDate, 'dd.MM.yyyy') + '</td>' +
													'</tr>';
									});
									// add this new content to the prohibition table
									$j('table.prohibitionItems tbody').empty().append(content).closest('table').removeClass('hid');
									// content for table head count
									content = 	'<tr>' +
									'<td colspan="6">';
										if (result.results.length != 1)
										{
											content += i18n.t("core.quotation:nbItemsInProhibitionList", {varNbItem : result.results.length, defaultValue : result.results.length + " items in prohibition list"});
										}
										else
										{
											content += i18n.t("core.quotation:nbItemInProhibitionList", {varNbItem : result.results.length, defaultValue : result.results.length + " item in prohibition list"});
										}
									content += 	'</td>' +
										'</tr>';
									// add count of prohibition items retrieved to table head
									$j('table.prohibitionItems thead').prepend(content);
									// add text to anchor
									$j(anchor).text(i18n.t("core.quotation:prohibitionInstrLoaded", "Prohibition Instruments Loaded"));
									// increment rowcount
									rowcount++;
								}
							});
						}
						else
						{
							// just show the table with message
							$j('table.prohibitionItems').removeClass('hid');
							// add text to anchor
							$j(anchor).text(i18n.t("core.quotation:prohibitionInstrLoaded", "Prohibition Instruments Loaded"));
						}
					}
					else
					{
						// show message to user
						$j.prompt(result.message);
					}
				}
			});
		}
	});
}

/**
 * this method loads all future recall items for the given amount of months that could have appeared on this recall,
 * any instruments on a current job are filtered out.
 * 
 * @param {Object} anchor object to manipulate
 * @param {Integer} recallid id of the recall
 * @param {String} recallType type of this recall (i.e. 'CONTACT' or 'ADDRESS')
 * @param {Integer} id identifier of either a contact or address
 * @param {Integer} months number of months ahead to retrieve instruments
 */
function loadFutureInstruments(anchor, recallid, recallType, id, months)
{
	// set rowcount to large figure
	var rowcount = 2000;
	// change anchor text
	$j(anchor).text('Loading.....');
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/recallservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to retrieve any future recall instruments
			recallservice.getFutureRecallListDWR(recallid, recallType, id, months,
			{
				callback:function(result)
				{
					// result successful?
					if (result.success)
					{
						// any future recall items returned?
						if (result.results.length > 0)
						{
							// load javascript service file
							loadScript.loadScriptsDynamic(new Array('script/trescal/core/utilities/TooltipLinks.js'), true,
							{
								callback: function()
								{
									// variable to hold new content
									var content = '';
									// add all future recall items to table and display
									$j.each(result.results, function(i, inst)
									{
										content += 	'<tr>' +
														'<td class="addtoquote"><input type="checkbox" name="toQuotePlantIds" value="' + inst.plantid + '" /></td>' +
														'<td class="barcode">' + instrumentLinkDWRInfo(inst, rowcount, true, false, true, 0) + '</td>' +
														'<td class="inst">' + inst.definitiveInstrument + '</td>' +
														'<td class="serial">' + inst.serialno + '</td>' +
														'<td class="plant">' + inst.plantno + '</td>' +
														'<td class="duedate">' + formatDate(inst.nextCalDueDate, 'dd.MM.yyyy') + '</td>' +
													'</tr>';
									});
									// add this new content to the future recall table
									$j('table.futureItems tbody').empty().append(content).closest('table').removeClass('hid');
									// content for table head count
									content = 	'<tr>' +
													'<td colspan="6">';
														if (result.results.length != 1)
														{
															content += i18n.t("core.quotation:nbItemsInFutureRecallList", {varNbItem : result.results.length, defaultValue : result.results.length + " items in future recall list"});
														}
														else
														{
															content += i18n.t("core.quotation:nbItemInFutureRecallList", {varNbItem : result.results.length, defaultValue : result.results.length + " item in future recall list"});
														}
										content += 	'</td>' +
												'</tr>';
									// add count of future recall items retrieved to table head
									$j('table.futureItems thead').prepend(content);
									// add text to anchor
									$j(anchor).text(i18n.t("core.quotation:futureRecallInstrLoaded", "Future Recall Instruments Loaded"));
									// increment rowcount
									rowcount++;
								}
							});
						}
						else
						{
							// just show the table with message
							$j('table.futureItems').removeClass('hid');
							// add text to anchor
							$j(anchor).text(i18n.t("core.quotation:futureRecallInstrLoaded", "Future Recall Instruments Loaded"));
						}
					}
					else
					{
						// show message to user
						$j.prompt(result.message);
					}
				}
			});
		}
	});
}

/**
 * this method checks all inputs of type checkbox that are contained within the specified
 * table
 * 
 * @param {Boolean} select true if boxes are to be checked and vice versa
 * @param {String} parentElement id of table in which to search for inputs of type checkbox
 */
function selectAllItems(select, parentElement)
{
	// check all
	if(select == true)
	{
		$j('.' + parentElement + ' input[name="toQuotePlantIds"]:checkbox').not(':disabled').check('on');
	}
	// uncheck all
	else
	{
		$j('.' + parentElement + ' input[name="toQuotePlantIds"]:checkbox').not(':disabled').check('off');
	}
}