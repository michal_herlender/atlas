/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*														All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	AddInstModelToQuote.js
*	DESCRIPTION		:	This page specific javascript file contains methods which facilitate the addition of instruments to the
* 					:  	quote basket.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	21/05/2007 - SH - Created File.
* 					:  	10/07/2007 - SH - Commenting Check.
*					:	28/10/2015 - TProvost - Manage i18n
*					:	28/10/2015 - TPRovost - Replace "input[@name=...]" by "input[name=...]" because this causes an error (syntax error, unrecognized expression)
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = ['script/trescal/core/utilities/DOMUtils.js',
								'script/trescal/core/tools/multicurrency.js',
								'script/thirdparty/jQuery/jquery.jtip.js'];

/**
 * Array of autocomplete objects (multiple are possible) containing setup for 
 * jQueryUI initialization.
 * this is used in the init() function.
 */
var jquiAutocomplete = [{
			inputFieldName: 'salesCat',
			inputFieldId: 'salesCatId',
			source: 'searchsalescategorytags.json'
		},
		{
			inputFieldName: 'familyNm',
			inputFieldId: 'familyId',
			source: 'searchfamilytags.json'
		},
		{
			inputFieldName: 'domainNm',
			inputFieldId: 'domainId',
			source: 'searchdomaintags.json'
		},
		{
			inputFieldName: 'descNm',
			inputFieldId: 'descId',
			source: 'searchdescriptiontags.json'
		},
		{
			inputFieldName: 'mfrtext',
			inputFieldId: 'dmfrId',
			source: 'searchmanufacturertags.json'
		}];

var subMenuElements = 	new Array( 
	   	   { anchor: 'quotationitem-link', block: 'quotationitem-tab'},
	   	   { anchor: 'catalogprice-link', block: 'catalogprice-tab' },
	   	   { anchor: 'manual-link', block: 'manual-tab' },
	);

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = [{ element: 'div.infobox', corner: 'normal' },
								{ element: 'div.results', corner: 'normal' },
								{ element: 'div.quotebasket', corner: 'normal' }];

/** 
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = 	[{ anchor: 'searchresults-link', block: 'searchresults-tab' },
					      	   { anchor: 'searchbasket-link', block: 'searchbasket-tab' }];

/**
 * shows a warning if the user is leaving a page which has items
 * in a basket. Only works in IE and Moz 1.7 +.
 */
var showUnLoadWarning = true;

window.onbeforeunload = function()
{
	if(parseInt($j('span.basketCount:first').text()) > 0 && showUnLoadWarning)
	{
		showUnLoadWarning = true;
		return i18n.t("warnLoseCurrentBasket", "If you leave this page you will lose the contents of your current basket.");
	}
};

/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'search\\.model';

/**
 * variable to hold id of the last model viewed
 * this is used to close the additem section when a new model is selected
 */
var searchidStore = '';

/**
 * this variable holds the options used in the qty select box
 * options are created once the first time a model is clicked on and
 * then reused thereafter
 */
var qtyOptions = '';

/**
 * this variable holds the options used in the heading select box
 * options are created once the first time a model is clicked on and
 * then reused thereafter
 */
var headingOptions = '';

/**
 * this function removes a previously selected models add item section
 * and removes the selected background colour
 * 
 * @param {Integer} modelid is used to find the list item which we are closing
 */
function closeLoadSection(modelid)
{
	// find the current selection and set background colour to default for first anchor
	$j('#model' + modelid + ' a:first').removeClass();
	// find the currently selected modules div if present and set background color to default
	if ($j('#module' + modelid).length > 0)
	{
		$j('#module' + modelid).removeClass().addClass('modulebox');
	}
	// slide up the add item section and then remove from page
	$j('#addform' + modelid).slideUp(800, function(){
		// remove previous model adding area
		$j('div').remove('#addform' + modelid);
	});
}

/**
 * when the user selects an instrument to add to the quote basket this method creates a section
 * below for the user to select the calibration type/s and quantity to add to the basket.
 * 
 * @param {Integer} modelid is the id of the model which is to have a load item section applied
 * @param {String} modelname is the name of the selected model
 * @param {String}  currencySymbol the currency symbol of the currency for this quotation.
 * @param {Integer} quoteid the id of the Quotation.
 * @param {String} currencyCode the currency code for the currency of this quotation
 * @param {Integer} coid id of the company that we are producing quote for 
 * @param {Integer} businesscoid business company id that is generating the quote
 */
function loadAddItemSection(modelid, modelname, currencyERSymbol, quoteid, currencyCode, coid, businesscoid)
{


		if($j('#addform' + modelid).length == 0)
		{


			// find the currently selected anchor and highlight with
			// mouseover colour
			$j('#model' + modelid + ' a:first').addClass('selected');


			// find the currently selected modules div if present
			// and set background to mouseover colour
			if ($j('#module' + modelid).length > 0)
			{
				$j('#module' + modelid).removeClass().addClass('modulebox_selected');
			}


			// load options required for the qty select box if they
			// have not already
			if (qtyOptions == '')
			{
				// use dom util 'prepareArray' to create array, loop
				// through all occurences and add option string to
				// variable
				$j.each(prepareArray(maxQuantity),
					function(x, m){
						qtyOptions = qtyOptions + '<option value="' + m + '">' + m + '</option>';
					}
				);
			}


			// load options required for the heading select box if
			// they have not already
			if (headingOptions == '')
			{
				// create heading options for use when populating
				// select boxes
				$j.each(quoteHeadings, function(z){
					headingOptions = headingOptions + '<option value="' + quoteHeadings[z].id + '">' + quoteHeadings[z].name + '</option>';
				});
			}


			// create content for the add item section
			var content = 	// create outer div for add item section
							// and set display style to 'none' for
							// the slide effect
							'<div class="searchItemOptions" id="addform' + modelid + '" style=" display: none; ">' +
							// add break above fieldset to
							// create a bigger margin above the
							// fieldset (ie bug causes flicker
							// if css border or padding used)
							'<br />' +
							// create fieldset for add items
							// section
							'<fieldset>' +
							// create legend for add item
							// section
							'<legend>' + i18n.t("addingForModelName", {varModelName : modelname, defaultValue : "Adding for " + modelname}) + '</legend>' +
							// create link and image for
							// closing the add item section
							'<a href="#" class="float-right" onclick=" closeLoadSection(\'' + modelid + '\'); return false; ">' +
							'<img src="img/icons/cancel.png" width="16" height="16" class="closegif" alt="' + i18n.t("close", "Close") + '" title="' + i18n.t("close", "Close") + '" />' +
							'</a>' +
							// create ordered list for add
							// item section
							'<ol>' +
							// append a list item for
							// headings
							'<li>' +
							'<div style=" float: left; width: 105px; font-weight: bold; ">' + i18n.t("calType", "CalType") + '</div>' +
							'<div style=" float: left; width: 100px; font-weight: bold; ">' + i18n.t("modelDefaultCost", "Model Default Price") + '</div>' +
							'<div style=" float: left; width: 42px; font-weight: bold; ">' + i18n.t("qty", "Qty") + '</div>' +
							'<div style=" float: left; width: 200px; font-weight: bold; ">' + i18n.t("core.quotation:heading", "Heading") + '</div>' +
							'<div style=" float: left; width: 165px; font-weight: bold; ">' + i18n.t("plantNo", "Plant No") + '</div>' +
							'<div style=" float: left; width: 157px; font-weight: bold; ">' + i18n.t("cost", "Cost") + '</div>' +
							'<div style=" float: left; width: 200px; font-weight: bold; ">' + i18n.t("comment", "Comment") + '</div>' +
							'<div class="clear"></div>' +
							'</li>';

			var caltypeIds = calTypes.map(function(value,index) {return value[0];});

			var payload = {
                modelId: modelid,
                calTypeIds: caltypeIds,
                businessCoId: businesscoid,
                currencyCode: currencyCode,
                clientCoId: coid,
                excludeQuotationId: quoteid
            }

            $j.ajax({
                    method: "POST",
                    url: "quotepricelookup/preferredcalprice",
                	 contentType: "application/json",
                    async: false,
                    data: JSON.stringify(payload),
                    dataType: 'json',
                    success: function (dtos) {

                        $j.each(dtos, function (i, dto) {

                            // generate the id's of select boxes and input field
                            var qtyFieldName = 'ct' + dto.calTypeId + 'qty' + modelid;
                            var headerFieldName = 'ct' + dto.calTypeId + 'heading' + modelid;
                            var plantFieldName = 'ct' + dto.calTypeId + 'plant' + modelid;
                            var costFieldName = 'ct' + dto.calTypeId + 'cost' + modelid;
                            var commentFieldName = 'ct' + dto.calTypeId + 'comment' + modelid;
                            // create a default empty cost for this calibration
                            var cost = currencyERSymbol + '<input type="text" id="' + costFieldName + '" name="' + costFieldName + '" value="0.00" />';

                            defCostFound = true;
                            cost = currencyERSymbol + ' <input type="text" id="' + costFieldName + '" name="' + costFieldName + '" value="' + dto.finalCost + '" />';
                            comment = ' <input type="text" id="' + commentFieldName + '" name="' + commentFieldName + '" disabled value="' + dto.comments + '" style = "width: 330px;" />';

                            	content = content +	// append a list item for each
                                // caltype to the ordered list
                                // in previous append
                                '<li id="' + 'ct' + dto.calTypeId + 'model' + modelid + '">' +
                                // add a calibration type
                                // label
                                '<label style="width: 105;"><a href="?qialternativecostsid=' + modelid + ',' + dto.calTypeId + ',' + coid + ',' + businesscoid + quoteid + '&amp;ajax=qialtcosts&amp;width=600" onclick="return(false);" class="jTip mainlink" name="Quote Item Alternative Price Information" id="qialternativecosts' + modelid + i + '" tabindex="-1"><img src="img/icons/search_costs.png" width="20" height="16" alt="' + i18n.t("core.quotation:alternativeCosts", "Alternative Costs") + '" title="' + i18n.t("core.quotation:alternativeCosts", "Alternative Costs") + '" /></a>&nbsp;' + dto.calTypeName + '</label>' +
                                // add catalog price
                                '<label style="width: 100;">' + currencyERSymbol + ' ' + dto.finalCost + '</label>' +

                                // add a quantity option
                                // list
                                '<select id="' + qtyFieldName + '" style=" width: 42px; ">' +
                                qtyOptions +
                                '</select>' +
                                // add a heading option -
                                // set the maxlength to stop
                                // page overflows
                                // set the selected heading
                                // to be the current
                                // quotation default
                                '<select id="' + headerFieldName + '" style=" width: 200px; ">' +
                                headingOptions +
                                '</select>' +
                                // add a plantno input field
                                '<input type="text" id="' + plantFieldName + '" name="' + plantFieldName + '" value="" />' +
                                cost +
                                comment +
                                ' &nbsp;<a href="#" class="mainlink" onclick=" addToBasket(' + modelid + ',' + dto.calTypeId +
                                ', $j(\'#' + qtyFieldName + '\').val(), $j(\'#' + headerFieldName + '\').val(), $j(\'#' + plantFieldName + '\').val(), $j(\'#' + costFieldName + '\').val(),\'' + dto.source + '\' ,\'standard\', true, \'' +
                                currencyCode + '\', ' + quoteid + ', \'' + currencyERSymbol + '\'); return false; ">' + i18n.t("addToBasket", "Add to basket") + '</a>' +
                                '</li>';
                        });

                    },
                error: function (jqXHR, textStatus) {
                alert("Request failed: " + textStatus);
            }
        });




			content = content + 	'</ol>' +
								// add a 'show basket' link
								'<a href="#" class="mainlink" id="quotebasketlink" onclick=" switchMenuFocus(menuElements, \'searchbasket-tab\', false); return false; ">' + i18n.t("goToBasket", "Go to basket") + '</a>  |  ' +
								// add a 'search' link
								'<a href="#search" class="mainlink" onclick=" clearForm(); $j(\'#mfr\').focus(); return false; ">' + i18n.t("searchAgain", "Search Again") + '</a>' +
								'</fieldset>' +
								'</div>';



			$j('#model' + modelid).after(content);



			// set qty select box selected option to that of qty
			// selected in defaults
			$j('select[id*="qty"]').val($j('#defaultQuoteQty').val());



			// add slide effect to main div and start diplaying
			$j('#addform' + modelid).slideDown(1200);



		}

		// a model adding section has been created
		if(searchidStore != '')
		{
			// model adding section exists on the page
			if(($j('#model' + searchidStore).length) && (searchidStore != modelid))
			{
				// this function closes the current model adding
				// section
				closeLoadSection(searchidStore);
			}
		}


		// if more than one model is present in result list then add
		// searchid variable
		// so it can be closed when the next model is selected
		if ($j('#searchresults-tab li').size() > 1)
		{
				searchidStore = modelid;
		}


	// initiate jtip tooltips
	JT_init();

}

/**
 * this function deletes an item or all items from the quote basket and displays the basket
 * empty message if no items remain.
 * 
 * @param {String} type 'item' if single deletion, 'all' if all quote items to be deleted
 * @param {Integer} basketitemid is the id of the basket item to remove
 */
function deleteFromBasket(type, basketitemid)
{
	// one item to be removed which could include all modules
	if (type == 'item')
	{
		// remove tbody from basket item table
		$j('tbody#' + basketitemid).remove();
		// recalculate the basketCount
		$j('span.basketCount').text(parseInt($j('span.basketCount:first').text()) - 1);
		// get all item number cells from table
		$j('table#basketitemstable tbody').each(function(i)
		{
			// renumber item no cell
			$j(this).find('tr:first td:first').text(i + 1);
			// sort table row class names
			if (((i + 1) % 2) == 0)
			{
				$j(this).find('tr:first').removeClass().addClass('even');
			}
			else
			{
				$j(this).find('tr:first').removeClass().addClass('odd');
			}
		});
	}
	// all items are to be removed
	else
	{
		// confirm deletion of all items
		var delAll = confirm(i18n.t("confirmDeleteAllItems", "Are you sure you wish to delete all items?"));
		// confirmation given 
		if(delAll)
		{ 
			// remove all tbodies from basket item table
			$j('table#basketitemstable tbody').remove();
			// recalculate the basketCount
			$j('span.basketCount').text('0');
		}
	}
	// basket is empty
	if (parseInt($j('span.basketCount:first').text()) < 1)
	{
		// display message
		$j('table#basketitemstable').append('<tbody>' +
												'<tr>' +
													'<td colspan="9" class="center bold">' + i18n.t("core.quotation:quotationItemBasketIsEmpty", "Your Quotation Item Basket Is Empty") + '</td>' +
												'</tr>' +
											'</tbody>');
	}
	
}

/**
 * when the user has selected to add an instrument to the quotation basket this method adds the instrument
 * and if it is a base unit then any modules that are attached will also be added
 *
 * @param {Integer} modelid is the id of the selected model (e.g. 870). used in id's
 * @param {Array} caltype is an array containing the caltype id and name (e.g. [1, UKAS])
 * @param {Integer} qty is the quantity of model to add to basket
 * @param {Integer} headingid is the id of heading under which this model should be added
 * @param {String} plantno is the supplied plant no for this model
 * @param {Decimal} price is the price
 * @param {String} addtype defines whether the user has added this model using the 'standard' load section
 * 					  or they have clicked on the add defaults
 * @param {Boolean} addmodules is a boolean value defines whether modules should be added
 * @param {String} curCode the ISO 4217 currency code of the currency for this quotation
 * @param {int} quoteid the id of the quotation.
 * @param {String} currencyERSymbol the entity reference symbol for the system default currency
 */
function addToBasket(modelid, caltype, qty, headingid, plantno, price, source, addtype, addmodules, curCode, quoteid, currencyERSymbol)
{
	
	// load options required for the qty select box if they have not already 
	if (qtyOptions == '')
	{
		// use dom util 'prepareArray' to create array, loop through all occurences and add option string to variable
		$j.each(prepareArray(50), function(x, m){
			qtyOptions = qtyOptions + '<option value="' + m + '">' + m + '</option>';							
		});
	}
	
	// load options required for the heading select box if they have not already 
	if (headingOptions == '')
	{
		// create heading options for use when populating select boxes
		$j.each(quoteHeadings, function(z){
			headingOptions = headingOptions + '<option value="' + quoteHeadings[z].id + '">' + quoteHeadings[z].name + '</option>';
		});
	}
	
	// basket qty has reached the maximum allowed so alert the user
	if(parseInt($j('span.basketCount:first').text()) >= BASKETMAXQTY)
	{
		$j.prompt(i18n.t("maxBasketSizeReached", "The maximum basket size has been reached."));
	}
	else
	{
		// the empty basket message is present so remove it
		if ($j('table#basketitemstable tbody:first tr:first td').length == 1)
		{
			// remove message
			$j('table#basketitemstable tbody:first').remove();
		}
		
		
		// generate new itemno for id's
		var itemno = maxItemNo;
		var caltypeShortName = '';
									
		// get caltype short name
		$j.each(calTypes, function(i){
			if (calTypes[i][0] == caltype){
				caltypeShortName = calTypes[i][1];
			}
		});
		// declare variable to hold class name
		var className = '';
		// if modulus of basket count returns naught then class name is 'even' else 'odd'
		if ((parseInt($j('span.basketCount:first').text()) + 1) % 2 == 0)
		{
			className = 'even';
		}
		else
		{
			className = 'odd';
		}
				
		// append new item row to basket
		$j('table#basketitemstable').append(	// add new tbody for model in basket
												'<tbody id="basketitem' + itemno + '">' +
													'<tr class="' + className + '">' +
														'<td class="center">' + (parseInt($j('span.basketCount:first').text()) + 1) + '</td>' +
														'<td>' + 
															$j('a#getmodelname' + modelid).text() +
															'<input type="hidden" name="itemnos" value="' + itemno + '" />' +
															'<input type="hidden" name="ids" value="' + modelid + '" />' +
														'</td>' +
														'<td class="center">' +
														caltypeShortName +
														    '<input type="hidden" name="caltype" value="' + caltype + '" />' +
														'</td>' +
														'<td class="center">' +
															'<select name="quantities" id="quantities' + itemno + '">' +
																qtyOptions +
															'</select>' +
														'</td>' +
														'<td class="center">' +
															'<select name="headingids" class="quotebasketselect" id="headingids' + itemno + '">' +
																headingOptions +
															'</select>' +
														'</td>' +
														'<td class="center">' +
															'<input type="text" name="plantnos" value="' + plantno + '" />' +
														'</td>' +
														'<td class="center">' + 
															'<span id="spanSource'+itemno+'">'+ source +'</span>'+
															'<input type="hidden" id="costSource'+itemno+'" name="sourceCost" value="' + source + '" />' +
															'<br><a href="#" onclick="switchSourceCost('+modelid+','+caltype+','+quoteid+','+ itemno+')">'+ i18n.t("core.pricing:purchaseOrder.switch", "Switch") +'</a>' +
														'</td>' +
														'<td class="center">' +
															$j('input#quoteCurrency').val() +
															' <input type="text" name="calcosts" id="calcost' + itemno + '" value="' + price + '" style="text-align: right" onclick="updateCostSource(\'' + itemno + '\')"/>' +
														'</td>' +
														'<td class="center">' + 
															'<a href="#" class="mainlink" onclick=" deleteFromBasket(\'item\', \'basketitem' + itemno + '\'); return false; "><img src="img/icons/delete.png" width="16" height="16" alt="' + i18n.t("deleteFromBasket", "deleteFromBasket") + '" title="' + i18n.t("deleteFromBasket", "deleteFromBasket") + '" /></a>' +
														'</td>' +														
													'</tr>' +
												'</tbody>');

		// set the selected caltype
		$j('#caltype' + itemno).val(caltype[0]);
		// set the selected qty
		$j('#quantities' + itemno).val(qty);
		// set the selected heading
		$j('#headingids' + itemno).val(headingid);
		// add one to maxItemNo
		maxItemNo = maxItemNo + 1;
		
		// add one to the current basket count in submenu
		$j('span.basketCount').text(parseInt($j('span.basketCount:first').text()) + 1);
				
		// if user has added items from search results 'standard' then show tick image in form
		if (addtype == 'standard')
		{
			var sucSpanId = 'added' + getSemiRandomInt();
			// add a success image and 3 second timeout to the basket form
			$j('#' + caltype[1] + modelid).append(	'<span id="' + sucSpanId + '">' +
														'<img src="img/icons/greentick.png" width="16" height="16" alt="' + i18n.t("success", "Success") + '" class="successgif img_vertmiddle" />' +
													'</span>');
													
			setTimeout(" $j('span').remove(\'#" + sucSpanId + "\');", 800);
		}
		
		// Modules are to be added with this model
		if (addmodules == true)
		{
			// array of Modules (see object Module) that belong to the item being added
			var modulesList = [];
			
			// get all input elements in the model's modules element that match the name 'module' and are checked and add to array
			$j('#module' + modelid + ' input[type="checkbox"][name="module"]:checked').each(function(i, n){
				modulesList.push(new Module(n.id, n.value, maxItemNo, itemno));
				maxItemNo = maxItemNo + 1;
			});
			
			// add all the modules as items in the quotation basket
			$j.each(modulesList, function(i){
				addModuleToBasket(modulesList[i], 'basketitem' + itemno, className);
			});
		}
		
		// initiate jtip tooltips
		JT_init();	
	}

}

/**
 * for every module that is attached to a base unit this method will be called to add them
 * to the quotation item basket
 * 
 * @param {Object} module is a module object containing all relevant information
 * @param {String} basketitemid is the id of the basket item tbody under which the modules should be added
 * @param {String} className is the class that should be applied to rows
 */
function addModuleToBasket(module, basketitemid, className)
{	
	// add this module to the parent base unit's tbody
	$j('tbody#' + basketitemid).append(	'<tr id="moditem' + module.itemno + '" class="' + className + '">' +
											'<td class="center">' +
												'<img src="img/icons/arrow_merge.png" width="16" height="16" alt="' + i18n.t("module", "Module") + '" title="' + i18n.t("module", "Module") + '" />' +
											'</td>' +																	
											'<td colspan="6">' +
												module.description + 
												'<input type="hidden" name="moduleitemnos" value="' + module.itemno + '" />' +
												'<input type="hidden" name="modulemodelids" value="' + module.modelid + '" />' +
												'<input type="hidden" name="modulepartofs" value="' + module.partof + '" />' +
											'</td>' +
											'<td class="center">' +																												
												'<a href="#" class="mainlink" onclick=" $j(\'tbody#' + basketitemid + ' tr#moditem' + module.itemno + '\').remove(); return false; " ><img src="img/icons/delete.png" width="16" height="16" alt="' + i18n.t("deleteFromBasket", "Delete from basket") + '" title="' + i18n.t("deleteFromBasket", "Delete from basket") + '" /></a>' +
											'</td>' +
										'</tr>');
}

/**
 * when the user selects the 'Add Defaults' link beside an instrument this method is called which gets 
 * all values specified in the add defaults section at the head of the page. Using these values an instrument
 * is added to the quote basket
 * 
 * @param {Event} e is a window event used to stop event bubbling
 * @param {Integer} quoteid id of the quote we are adding defaults for
 * @param {Integer} modelid is the id of the selected model (e.g. 870). used in id's
 * @param {String} name is the name of the selected model
 * @param {String} curCode the ISO 4217 currency code of the currency for this quotation
 * @param {String} currencySymbol the currency symbol for the logged in user's currency
 */
function addDefaults(e, quoteid, modelid, name, curCode, companyid, currencySymbol, clientCompanyId, excludeQuoteId)
{
	console.log("symbol: " + currencySymbol);

	// stop event propogation so that the advanced adding options is not displayed
	if (!e)
	{
		var e = window.event;
	}
	
	e.cancelBubble = true;
	
	if(e.stopPropagation)
	{
		e.stopPropagation();
	}

	// declare variables to hold default calibration types selected and default module addition
	var defaultModules = false;
	var defaultTypes = [];
	// get default heading, default qty values
	var defaultHeading = $j('#defaultQuoteHeading').val();
	var defaultQty = $j('#defaultQuoteQty').val();
	// get all inputs within the searchdefaults div
	// iterate through inputs and find all default cal types selected and add to array
	$j('#searchdefaults input[name="defaultCaltypes"]:checked').each(function(i, n){
		defaultTypes.push([n.value]);
	});
	// iterate through inputs and find whether the user has selected to add modules
	$j('#searchdefaults input[name="defaultAddModules"]:checked').each(function(x, m){
		if (m.value == 'true')
		{
			defaultModules = true;
		}
	});
	
	// if no calibration types have been selected show a warning message else
	// add items to basket for each calibration type selected
	if (defaultTypes.length < 1)
	{
		$j.prompt(i18n.t("notSelectedCalType", "You have not selected the calibration type you require"));
	}
	else
	{

        var integerDefaultTypes = defaultTypes.map(function(value,index) {return parseInt(value);});

        var payload = {
            modelId: modelid,
            calTypeIds: integerDefaultTypes,
            businessCoId: companyid,
            currencyCode: curCode,
            clientCoId: clientCompanyId,
            excludeQuotationId: excludeQuoteId
        };


		$j.ajax({
            method: "POST",
            url: "quotepricelookup/preferredcalprice",
            contentType: "application/json",
            async: false,
            data: JSON.stringify(payload),
            dataType: 'json',
            success: function (dtos) {
				$j.each(dtos, function (i, dto) {
                    addToBasket(modelid, integerDefaultTypes[i], defaultQty, defaultHeading, '', dto.finalCost, dto.source,
                    		'default', defaultModules, curCode, quoteid, currencySymbol);
                });
			},
			error: function (jqXHR, textStatus) {
				alert("Request failed: " + textStatus);
			}
		});

		var sucSpanId = 'added' + getSemiRandomInt();
		// add a success image and 3 second timeout to the basket form
		$j('#default' + modelid).append('<span id="' + sucSpanId + '">' +
											'<img src="img/icons/greentick.png" width="16" height="16" class="successgif" alt="' + i18n.t("success", "Success") + '" style=" vertical-align: top; " />' +
										'</span>');

		setTimeout(" $j('span').remove(\'#" + sucSpanId + "\');", 800);
	}

}

/**
 * clear search form and apply focus to the mfr field
 */
function clearForm()
{
	// clear all current fields
	$j('input[name="mfrtext"], input[name="search\\.mfrId"], input[name="search\\.mfrNm"], input[name="search\\.model"], input[name="desctext"], input[name="search\\.descId"], input[name="search\\.descNm"], input#descNm').val('');
	$j('input[name="domainNm"],input[name="domainId"],input[name="familyNm"],input[name="familyId"],input[name="salesCat"],input[name="salesCatId"]').val('');
	$j('input[name="search\\.excludeQuarantined"]').prop('checked',false);
	$j('select#modelTypeSelectorChoice').val('0');
	// add focus back to mfr field
	$j('input[name="search\\.model"]').focus();
}

function checkForm()
{
	if($j('input#salesCat').val() == '' 
		&& $j('input#modeltext').val() == '' 
		&& $j('input#descNm').val() == '' 
		&& $j('input#mfrtext').val() == ''
		&& $j('input#familyNm').val() == '' 
		&& $j('input#domainNm').val() == ''){
			alert('Please Enter A Search Term');
			return false;
	}
	return true;
}

function switchSourceCost(modelid, caltypeid, quoteid, itemno) {
	var cancel = i18n.t("core.jobs:jobItem.cancel", "Cancel");
	var save = i18n.t("save", "Save");
	$j.ajax({
	    url:'availablepricesources.htm',
	    type:'GET',
	    data: {
	    	modelid: modelid,
	    	caltypeid: caltypeid,
			quoteid : quoteid
		},
		async: true,
	    }).done(function(content) {
	       	//display the popup
	       	$j.prompt(content,{
	       		title: "Viewing Available Price Sources",
	       		buttons: { [save]: true, [cancel]: false },
	       		submit: function (e, val, m, f) {                            
	                   if (val == true){
	                	 //if no prebooking is selected show a message
	                   	 if (!$j('input[name="priceSelected"]').is(':checked')) {
	                   		alert("please select a price!");
	                        }
	                   	 else{
	                   		 var price = $j('input[name="priceSelected"]:checked')[0].value;
	                   		 var source = $j('input[name="priceSelected"]:checked')[0].id;
	                   		 var finalPrice;
	                   		 if(price == 'Manual'){
	                   			finalPrice = $j('#manualPrice')[0].value;
	                   		 } else {
	                   			 finalPrice = price;
	                   		 }
	                   		 if(finalPrice){
	                   			 // update hidden price input value 
	 	                 		$j('#calcost'+itemno)[0].value = finalPrice;
	 	                 		// display the price value
	 	                 		$j('#spanSource'+itemno).text(source);
	 	                 		// update hidden cost source value
	 	                 		$j('#costSource'+itemno)[0].value = source;
	                   		 }	
	                   	 }
	                   }	
	                   else
	                       console.log('Cancel!');
	               }
	       	});       	
	      }
	);
}

function updateCostSource(itemno){
	// update the source input value
	if($j('#costSource'+itemno)[0]){
		$j('#costSource'+itemno)[0].value = 'MANUAL';
	}
	// update the source input value
	if($j('#spanSource'+itemno)){
		$j('#spanSource'+itemno).text('MANUAL');
	} 	
}
