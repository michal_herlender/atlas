/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	CreateHeadingForm.js
*	DESCRIPTION		:	This javascript file is used on the createheadingform.vm page.
*					: 	
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	15/01/2007 - SH - Created File
*					:	28/10/2015 - TProvost - Minor fix on enable buttons
****************************************************************************************************************************************/

/*
 * Array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'script/trescal/core/utilities/ScriptedUtils.js'	 );

/*
 * Array of objects containing the element name and corner type which should be applied.
 * Applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' },
							   	{ element: 'div.results', corner: 'normal' } );


/* this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
 var menuElements = new Array( { anchor: 'currentheadingslink', block: 'currentheadingsarea' },
		  					   { anchor: 'newheadinglink', block: 'newheadingarea' } );

 /**
  * this function is called from the loadScript.init() function in CwmsMain.js if present.
  * this allows onload coding to be included in the page specific javascript file without adding
  * to the body onload attribute. 
  */
function init()
{
	if ($j('div.warningBox1').length)
	{
		switchMenuFocus(menuElements, 'newheadingarea', 1);
	}
}

/**
 * this method moves a quotation heading . If the heading is moved
 * successfully then the page is reloaded.
 * 
 * @param {Integer} oldPosId id of the heading in old position
 * @param {Integer} newPosId id of the heading in the new position
 * @param {Integer} quotationId id of the quotation the headings reside within
 */
function moveQuoteHeading(oldPosId, newPosId, quotationId)
{
	// disable move heading item button
	$j('.moveQHButton').attr('disabled', 'disabled');
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/quoteheadingservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to move the quotation heading
			quoteheadingservice.moveQuoteHeading(oldPosId, newPosId, quotationId,
			{
				callback: function(result)
				{
					// move heading successful?
					if (result.success)
					{
						// reload the current page
						window.location.href = window.location;
					}
					else
					{
						// display error message
						$j.prompt(result.message);
						// enable move quote heading button
						$j('.moveQHButton').attr('disabled', false);
					}
				}
			});
		}
	});
}