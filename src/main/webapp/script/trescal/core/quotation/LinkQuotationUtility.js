/**
 * this method creates a new thickbox for displaying linked quotations and calls
 * another method to create the content to display within it
 *
 * @param {Integer}
 *            jobid id of the current job
 * @param {Integer}
 *            coid id of the current company
 * @param {String}
 *            years number of years to display
 * @param {Boolean}
 *            showExpired indicates whether to display expired quotes
 * @param {Boolean}
 *            showUnIssued indicates whether to display non-issued quotes
 */
function linkQuotation(jobid, coid, years, showExpired, showUnIssued, linkTo) {
    // display thickbox window and add content from method below
    tb_show(i18n.t("core.jobs:job.linkQuotation", "Link Quotation"),
        'TB_dom?width=800&height=400', '', searchQuotationsToLink(jobid,
            coid, years, showExpired, showUnIssued, linkTo));
}

/**
 * this method creates the thickbox content when displaying linked quotes
 *
 * @param {Integer}
 *            jobid id of the current job
 * @param {Integer}
 *            coid id of the current company
 * @param {String}
 *            years number of years to display
 * @param {Boolean}
 *            showExpired indicates whether to display expired quotes
 * @param {Boolean}
 *            showUnIssued indicates whether to display non-issued quotes
 */
function searchQuotationsToLink(jobid, coid, years, showExpired, showUnIssued, linkTo) {
    $j.ajax({
        url: "searchcompanyquotations.htm",
        data: {
            coId: coid,
            jobId: jobid,
            years: years,
            showExpired: showExpired,
            showUnIssued: showUnIssued,
            linkTo: linkTo
        },
        async: true
    }).done(function(content) {
        $j('#TB_ajaxContent').empty();
        $j('#TB_ajaxContent').html(content);
        $j('select#lqexpired option[value=' + showExpired + ']').attr('selected','selected');
        $j('select#lqissued option[value=' + showUnIssued + ']').attr('selected','selected');
        $j('select#lqyears').val(years);
        $j('table#jobquotelinktable').zebra();
    });
}

/**
 * this method links the current job to a quotation chosen by the user in the
 * link quotation thickbox and appends a new row to the linked quotes table.
 * 
 * @param {Integer}
 *            jobid id of the job to be linked
 * @param {Integer}
 *            quoteid id of the quote to be linked
 */
function insertJobQuoteLink(jobid, quoteid) {
	var acceptStatus = $j('#acceptStatusSelect').val();
	$j.ajax({
		url : "linkjobtoquotation.htm",
		data : {
			jobId : jobid,
			quoteId : quoteid,
			acceptStatus: acceptStatus
		},
		async : false
	}).done(
			function(linkedQuote) {
				// check to see if no quotes have been linked and message row is
				// present
				if ($j('table#linkedquotes tbody tr:first td').length == 1) {
					// remove message row
					$j('table#linkedquotes tbody tr:first').remove();
				}
				// append row to linked quote table for new linked quote
				$j('table#linkedquotes tbody').append(linkedQuote);
				// add text to quote row in thickbox to indicate that it has
				// been linked
				$j('#availquotelink' + quoteid).before('linked');
				// remove the 'link to quote' link
				$j('#availquotelink' + quoteid).remove();
				// add one to the linked quotes count
				$j('span#linkedCount').text(
						parseInt($j('span#linkedCount').text()) + 1);
			}).fail(
			function(error) {
				// show error message in error warning box
				$j('div#TB_ajaxContent div:first').removeClass('hid').addClass(
						'vis').find('.warningBox3').empty().append(
						error.responseText);
			});
}

/*
 * This method inserts the quote number and quote id into the fields with ids quoteNumber & quoteId respectively.
 * It also updates the accepted status of the quote if that option has been selected in the pop up.
 *
 */
function linkQuotetoContract(quoteId, quoteNumber){

	// update fields
	$j("#quoteNumber").val(quoteNumber);
	$j('#quoteId').val(quoteId);

	//update quote status
	var acceptStatus = $j('#acceptStatusSelect').val();
	var response = $j.ajax({
		url: "acceptquote.htm",
		data: {
			quoteId: quoteId,
			acceptStatus: acceptStatus
		},
		async: false
	});

	response.fail(function (jqXHR, textStatus) {
		alert("Request failed: " + textStatus);
	});
	
}

function filterQuotationStatus(){
	var selectStatus = $j('#acceptStatusSelect').val();
	if(selectStatus != 2){
		$j("tr[id^='qparent']").each(function() {
			$j(this).removeClass('hid').addClass('vis');
		});
	} else {
		$j("tr[id^='qparent']").each(function() {
			if($j(this).attr("status") === 'true'){
				$j(this).removeClass('hid').addClass('vis');
			} else {
				$j(this).removeClass('vis').addClass('hid');
			}
		});
	}
}