import {
  LitElement,
  html,
  css,
} from "../../../thirdparty/lit-element/lit-element.js";
import  "../../../components/cwms-translate/cwms-translate.js";

class CwmsClientAcceptDateOn extends LitElement {
  static get styles() {
    return css`
      input:invalid {
        border: solid 1px red;
      }
    `;
  }

  static get properties() {
    return {
      quotationId: { type: String },
      acceptedOn: { type: String },
      acceptedBy: { type: String },
      formOpen: { type: Boolean, attribute: false },
      errorMessage: { type: String, attribute: false },
    };
  }

  get acceptedOn() {
      return this._acceptedOn;
  }

  set acceptedOn(value){
    const oldValue = this.acceptedOn;
    if(value instanceof Date) this._acceptedOn = value;
    else if(value instanceof String || typeof(value) =="string"){
        try {
            if(value) this._acceptedOn = new Date(value);
        }
        catch (e){
            this.errorMessage ="Accept on date in unknown format";
        }
    }
    else {
        this.errorMessage ="Unkown type of accept on.";
        return;
    }
    
    this.formOpen = false;
    this.requestUpdate("acceptedOn",oldValue);
  }

  get contextPath() {
    return document
      .querySelector("meta[name='_contextPath']")
      .getAttribute("content");
  }

  get csrfToken() {
    return document.querySelector("meta[name='_csrf']").getAttribute("content");
  }

  acceptDate(evenet) {
    evenet.preventDefault();
    const dateInput = this.shadowRoot.querySelector(
      "input[name='clientacceptedon']"
    );
    if (!dateInput.checkValidity()) {
      return;
    }
    const date = this.shadowRoot.querySelector("input[name='clientacceptedon']")
      .value;
    this.sendAcceptDate(date);
  }

  sendAcceptDate(date) {
    const base = `${location.origin}${this.contextPath}/`;
    const url = new URL("updatequotationstatus.json", base);
    fetch(url, {
      method: "POST",
      body: JSON.stringify({ id: this.quotationId, acceptedOn: date }),
      headers: {
        Accept: "application/json",
        "X-CSRF-TOKEN": this.csrfToken,
        "Content-Type": "application/json",
      },
    })
      .then((r) => (r.ok ? r.json() : Promise.reject(r.status)))
      .then((r) => (r.right ? r.value : Promise.reject(r.value)))
      .then((v) => {
        this.acceptedOn = new Date(v.acceptedOn);
        this.acceptedBy = v.acceptedBy;
      })
      .catch((m) => (this.errorMessage = m));
  }

  renderForm() {
    return this.formOpen
      ? html`
          <form id="updatequotationstatus" action="javascript:0">
            <input type="hidden" name="id" value="${this.quotationId}" />
            <input type="hidden" name="accepting" value="${true}" />
            <div id="clientApprovalOnDialog">
              <span>
                <cwms-translate
                  code="viewquot.clientacceptedon"
                ></cwms-translate
                >:
              </span>
              <input
                id="clientacceptedon"
                type="date"
                name="clientacceptedon"
                required
              />
              <button @click=${this.acceptDate.bind(this)}>
                <cwms-translate code="save"></cwms-translate>
              </button>
              <button @click=${(e) => (this.formOpen = false)}>
                <cwms-translate code="cancel"></cwms-translate>
              </button>
            </div>
          </form>
        `
      : html``;
  }

  renderFormLink() {
    return this.acceptedOn
      ? html``
      : html`
          <a
            role="button"
            class="mainlink"
            @click=${(e) => (this.formOpen = true)}
          >
            <cwms-translate code="viewquot.recordacceptance"></cwms-translate
          ></a>
        `;
  }

  renderError() {
    return this.errorMessage
      ? html` <span class="error">${this.errorMessage}</span> `
      : html``;
  }

  renderAccepted(acceptedOn,acceptedBy){
      return html `
        ${acceptedOn.toLocaleDateString()} ${acceptedBy?html`- ${acceptedBy}`:html``}
      `;
  }

  render() {
    return html`
      <link rel="stylesheet" href="styles/theme/theme.css" />
      <link rel="stylesheet" href="styles/text/text.css" />
      <link rel="stylesheet" href="styles/structure/structure.css" />
        ${this.acceptedOn? this.renderAccepted(this.acceptedOn,this.acceptedBy):
             html` 
         <div id="acceptedlink">
          ${this.renderFormLink()} ${this.renderForm()} ${this.renderError()}
        </div>`}
    `;
  }
}

customElements.define("cwms-client-accept-date-on", CwmsClientAcceptDateOn);
