/****************************************************************************************************************************************
 *												CWMS (Calibration Workflow Management System)
 *
 *												Copyright 2006, Antech Calibration Services
 *															www.antech.org.uk
 *
 *															All rights reserved
 *
 *														Document author: 	Stuart Harrold
 *
 *	FILENAME		:	QuotationRequestToQuotation.js
 *	DESCRIPTION		:	Page specific javascript file. Loads all necessary javascript files and applies nifty corners.
 *	DEPENDENCIES	:
 *
 *	TO-DO			: 	-
 *	KNOWN ISSUES	:	-
 *	HISTORY			:	14/10/2009 - SH - Created File
 *
 ****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array({ element: "div.infobox", corner: "normal" });

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute.
 */
function init() {
  $j("#conversionutilitydialog").dialog({
    autoOpen: false,
    width: 1000,
    height: 500,
    modal: true,
  });

  $j("#convert").click(function () {
    $j("#conversionutilitydialog").dialog("open");
  });

  $j("#addfreehand").click(e => {
    e.preventDefault();
    submitCompanyToContactDetails();
  });
}

function submitCompanyToContactDetails() {
  const form = document.querySelector("div#conversionutilitydialog form");
  const formData = new FormData(form);

  const data = Array.from(formData.entries())
    .map((p) => ({ [p[0]]: p[1] }))
    .reduce((acc, pp) => ({ ...acc, ...pp }), {});

  fetch("freehandcontactconversion.json",
		    {
				method: 'post',
				body: formData ,
			    headers: {
			    	Accept: "application/json",
			        "X-CSRF-TOKEN": formData.get("_csrf"),
			        "Content-Type": "application/json",
			    },
			    body: JSON.stringify(data)
			})
			 .then((r) => (r.ok ? r.json() : Promise.reject(r.status)))
    .then(this.freeandDoneFuntion, (status) =>
      alert("Request failed: " + status)
    );
}

function freeandDoneFuntion(result) {
  // reset error messages
  $j("span.error").hide();

  // insertion successful?
  if (result.failed) {
    $j.each(result.errors, function (i) {
      // retrieve each error
      var error = result.errors[i];
      //Dis[lay error next to correct field
      errorSpan = $j('input[name="' + error["key"] + '"]').siblings(
        "span.error"
      );
      errorSpan.text(error["value"]);
      errorSpan.show();
    });
    $j("#globalerrors").show();
    $j("#conversionutilitydialog").scrollTop(0);
  } else {
    // close the dialog
    $j("#conversionutilitydialog").dialog("close");
    // check for cascading search plugin
    if ($j("div#cascadeSearchPlugin").length) {
      // add coname to input and trigger event
      $j('div#cascadeSearchPlugin input[name="coname"]')
        .val(result.coname)
        .trigger("keyup");
    }
    // remove button and signal added
    $j("#convert")
      .remove()
      .end()
      .append(
        '<span style=" color: green; "><img src="img/icons/success.png" width="16" height="16" class="image_inline3" /> ' +
          i18n.t("added", "Added") +
          "</span>"
      )
      .removeClass("padtop");
  }
}
