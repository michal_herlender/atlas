/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	CreateQuote.js
*	DESCRIPTION		:	Page specific javascript file. Loads all necessary javascript files and applies nifty corners.
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	26/10/2006 - SH - Created File
*					:	28/10/2015 - TProvost - Manage i18n
*					:	28/10/2015 - TProvost - Fix bug in new version of Impromptu.js - Replace the "callback" property (property disappeared) by the "submit" property
*												=> Changes made in function deleteQuoteRequest
****************************************************************************************************************************************/

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' },
								{ element: 'ul.subnavtab a', corner: 'small transparent top' } );

/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'qno';
/**
 * Array of autocomplete objects (multiple are possible) containing setup for 
 * jQueryUI initialization.
 * this is used in the init() function.
 */
var jquiAutocomplete = new Array(
				{
					inputFieldName: 'description',
					inputFieldId: 'descid',
					source: 'searchdescriptiontags.json'
				});

/**
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = 	new Array( 	{ anchor: 'homequote-link', block: 'homequote-tab' },
								{ anchor: 'searchquote-link', block: 'searchquote-tab' },
					      	  	{ anchor: 'createquote-link', block: 'createquote-tab' });
							   
/**
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements1 = 	new Array( 	{ anchor: 'awaitingcompletion-link', block: 'awaitingcompletion-tab' },									
					      	   		{ anchor: 'quoterequest-link', block: 'quoterequest-tab' },
									{ anchor: 'recentlyissued-link', block: 'recentlyissued-tab'});

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{
	$j(document).keydown( function(e)
	{
	    var event = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
		
		// see if the enter key has been pressed?
		switch (event)
		{
			// enter key has been pressed
			case 13: 	// check search tab is visible
						if ($j('#searchquote-tab').hasClass('vis'))
						{
							// trigger click event of form submit button
							$j('#searchquoteform').find('input[type="submit"]').trigger('click');
						}						
						break;
		}
	});
	
	populateSelectFilters('#quoterequest-tab',sourcedByContBySubdivRequest);
	populateSelectFilters('#awaitingcompletion-tab',sourcedByContBySubdivAC);
	populateSelectFilters('#recentlyissued-tab',sourcedByContBySubdivIssued);
	
}

/**
 * array of calendar objects containing the initial setup literals needed
 * @param inputField (String) id of the input field to which the calendar should be binded
 * @param dateFormat (String) format that the date should be displayed
 * @param displayDates (String) 'all' make all dates available
 * 								'future' show only dates in the future
 * 								'past' show only dates in the past
 * applied in init() function.
 */	
/**
 * this method shows or hides the between date field
 * 
 * @param {boolean} between indicates whether to show the between date field
 * @param {String} id the id of the between element to display
 */
function changeDateOption(between, id)
{
	// show between?
	if(between == 'true')
	{
		// display between
		$j('#' + id).removeClass('hid').addClass('vis');
	}
	else
	{
		// hide between
		$j('#' + id).removeClass('vis').addClass('hid');
	}
}

/**
 * this method deletes a quick quotation request
 * 
 * @param {String} qString the text used to make up id
 * @param {Integer} qrid the id of the quote request
 * @param {String} qcomp the name of the company we are deleting quote request for
 */
function deleteQuoteRequest(qString, qrid, qcomp)
{
	// initial prompt for user
	$j.prompt(i18n.t("core.quotation:confirmDeleteQuoteRequest", {varCompName : qcomp, defaultValue : "Are you sure you wish to delete the quotation request for " + qcomp + "'?"}),
	{ 
		submit: confirmcallback,
		buttons: 
		{ 
			Ok: true,
			Cancel: false 
		}, 
		focus: 1 
	});
	// callback method for initial prompt
	function confirmcallback(v,m)
	{
		// user confirmed action?
    	if (m)
		{
    		// load the service javascript file if not already
    		loadScript.loadScriptsDynamic(new Array('dwr/interface/quotationrequestservice.js'), true,
    		{
    			callback: function()
    			{
    				// delete quotation request
    				quotationrequestservice.deleteQR(qrid,
    				{
    					callback: function(result)
    					{
    						if (result.success)
    						{
    							// remove quotation request from page
    							$j('#' + qString + qrid).remove();
    						}
    						else
    						{
    							// show failure message
    							$j.prompt(result.message);
    						}
    					}
    				});
    			}
    		});
		}
	}
}

/* populate subdiv select from json object */
function populateSelectFilters(tabId,dataSource){
	var subdivs = Object.keys(dataSource);
	var subDivSelector = $j(tabId+' .subDivSelector');
	var sourcedBySelector = $j(tabId+' .sourcedBySelector');
	var selectedSubdivId = tabId == '#quoterequest-tab' ? quoteRequesteSubdivId:tabId == '#awaitingcompletion-tab'?quoteSubdivId:undefined;
	var selectedContactId = tabId == '#quoterequest-tab' ? quoteRequesteContactId:tabId == '#awaitingcompletion-tab'?quoteContactId:undefined;	
	
	subdivs.sort(function(a,b){
		var x = a.split(':')[1].trim().toLowerCase();
		var y = b.split(':')[1].trim().toLowerCase();

		return x < y ? -1 : x > y ? 1 : 0;
	});
	
	var contacts = new Array();
	$j.each(subdivs,function(key,value) {
		subdiv = value.split(':');
		var subdivSelected = selectedSubdivId==subdiv[0].trim()?'selected':'';
		subDivSelector.append('<option value=' + subdiv[0].trim() + ' '+subdivSelected+'>' + subdiv[1].trim() + '</option>');
		
		//get associated contacts, and merge into array
		contacts.push.apply(contacts,dataSource[value]);
		
	});
	
	contacts.sort(function(a,b){
		var x = a.value.toLowerCase();
		var y = b.value.toLowerCase();

		return x < y ? -1 : x > y ? 1 : 0;
	});
	
	//populate sourcedBySelector
	$j.each(contacts,function(key,res) {
		var contactSelected = selectedContactId==res.key?'selected':'';
		sourcedBySelector.append('<option value=' + res.key + ' '+contactSelected+'>' + res.value + '</option>');
	});
	
	//
	if(selectedSubdivId != 0)
		subDivSelectorOnChange(tabId, sourcedByContBySubdivRequest);	
	
}

/* on change of subdiv select popultate contact select */
function subDivSelectorOnChange(tabId,dataSource){
	
	//get key and value of selected subdiv
	var selectedValue = $j(tabId+' .subDivSelector').val();
	var selectedText = $j(tabId+' .subDivSelector option:selected').text();
	
	var sourcedBySelector = $j(tabId+' .sourcedBySelector');
	var selectedContactId = tabId == '#quoterequest-tab' ? quoteRequesteContactId:tabId == '#awaitingcompletion-tab'?quoteContactId:undefined;
	//reset selector
	sourcedBySelector.find('option[value!="0"]').remove();
	
	var contacts = new Array();
	if(selectedValue != 0){
		//get associated contacts
		contacts = dataSource[selectedValue+': '+selectedText];
	}else{
		//get all contacts
		$j.each(dataSource,function(key,value){
			contacts.push.apply(contacts, $j.map(value,function(n,i){return n;}));
		});
	}
	
	//sort
	contacts.sort(function(a,b){
		var x = a.value.toLowerCase();
		var y = b.value.toLowerCase();

		return x < y ? -1 : x > y ? 1 : 0;
	});
	
	//popultae selector
	$j.each(contacts,function(key,res) {
		var contactSelected = selectedContactId==res.key?'selected':'';
		sourcedBySelector.append('<option value=' + res.key + ' '+contactSelected+'>' + res.value + '</option>');
	});
	
	//filter
//	filterQuotations(tabId);
}

function filterQuotations(tabId){
	
	var selectedSubDivId = $j(tabId+' .subDivSelector').val();
	var selectedContactId = $j(tabId+' .sourcedBySelector').val();
	
	//contact filter
	if(selectedContactId != 0){
		$j(tabId+" tr[sourcedBy="+selectedContactId+"]").show();
		$j(tabId+" tr[sourcedBy]").filter(function(){
			return $j(this).attr("sourcedBy")!=selectedContactId;
		}).each(function(){$j(this).hide()});
	}else{
		$j(tabId+" tr[sourcedBy]").filter(function(){
			return $j(this).attr("sourcedBy")!=0;
		}).show();
	}
	
	//subdiv filter
	if(selectedContactId==0){
		if(selectedSubDivId != 0){
			$j(tabId+" tr[subdiv="+selectedSubDivId+"]").show();
			$j(tabId+" tr[subdiv]").filter(function(){
				return $j(this).attr("subdiv")!=selectedSubDivId;
				}).each(function(){$j(this).hide()});
		}else{
			$j(tabId+" tr[subdiv]").filter(function(){
				return $j(this).attr("subdiv")!=0;
			}).show();
		}
	}
	
	//update quotations count
	$j(tabId+" .quotationCount").text('('+$j(tabId+" tr[sourcedBy]").filter(":visible").size()+')')
}

function submitSearchForm(formId)
{
	var input = $j("<input>")
	    .attr("type", "hidden")
	    .attr("name", "search").val("true");
	$j('#'+formId).append(input);
	$j('#'+formId).submit();
}
