/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	ViewQuotationRequest.js
*	DESCRIPTION		:	Page specific javascript file. Loads all necessary javascript files and applies nifty corners.
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	14/10/2009 - SH - Created File
*
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array ( 'script/trescal/core/tools/FileBrowserPlugin.js' );

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );
