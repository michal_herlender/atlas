/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	QuoteForm.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	Commenting of code.
*	KNOWN ISSUES	:	-
*	HISTORY			:	07/06/2007
*					:	28/10/2015 - TProvost - Manage i18n 
****************************************************************************************************************************************/

/**
 * Array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'script/trescal/core/utilities/DOMUtils.js' );

/**
 * Array of objects containing the element name and corner type which should be applied.
 * Applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' },
								{ element: 'ul.subnavtab a', corner: 'small transparent top' } );
								
/**
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = 	new Array( { anchor: 'empty-link', block: 'empty-tab' },
					      	   { anchor: 'jobs-link', block: 'jobs-tab' },
							   { anchor: 'recalls-link', block: 'recalls-tab' },
							   { anchor: 'instruments-link', block: 'instruments-tab' } );

/**
 * Element to be focused on when the page loads.
 * Applied in init() function.
 */
var focusElement = 'clientRef';


/**
 * array of calendar objects containing the initial setup literals needed
 * @param inputField (String) id of the input field to which the calendar should be binded
 * @param dateFormat (String) format that the date should be displayed
 * @param displayDates (String) 'all' make all dates available
 * 								'future' show only dates in the future
 * 								'past' show only dates in the past
 * applied in init() function.
 */								

/**
 * this method switches the selected radio button for quotation creation type when
 * a new tab is selected.
 * 
 * @param {String} name the name of the creation type input
 * @param {String} divid the id of the div in which we want to select radio button
 */		 
function switchCreationType(name, divid)
{
	// set the selected creation type in selected div
	$j('div#' + divid + ' input[type="radio"][name="' + name + '"]:first').attr('checked', 'checked');
	if(divid === "empty-tab"){
	    $j('#submit').prop("disabled", false);
	} else {
	    $j('#submit').prop("disabled", true);
	}
}

function sourceClicked()
{
    $j('#submit').prop("disabled", false);
}

/**
 * this method loads more jobs for the selected company from which a quotation can be created.
 * 
 * @param {String} tableid the id of the element to append rows to
 * @param {Integer} coid id of the company to retrieve jobs for
 * @param {Integer} resPerPage number of results to return
 * @param {Integer} pageNo current page number
 * @param {Object} anchor the anchor clicked to call this method
 */
function loadJobsForCreation(tableid, coid, resPerPage, pageNo)
{
	// set counter for tooltips
	var rowcount = (pageNo * resPerPage);
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/jobservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to retrieve jobs for company
			jobservice.ajaxQueryJobByCoid(coid, allocatedSubdivId, resPerPage, pageNo,
			{
				callback:function(result)
				{
					// add jobs to variable
					var jobs = result.results;
					// have jobs been returned?
					if(jobs.length > 0)
					{
						// initialise variable for content
						var content = '';
						// add retrieved jobs to content
						$j.each(jobs, function(i)
						{
							// add job object to variable
							var job = jobs[i];
							// increment rowcount
							rowcount++;
							// add fields to content
							content += 	'<tr>' +
											'<td class="source"><input type="radio" name="quoteFromJobId" value="' + job.jobid + '" onclick="sourceClicked();" /></td>' +
											'<td class="jobno">' +
												'<a href="viewjob.htm?jobid=' + job.jobid + '&amp;ajax=job&amp;width=540" class="jconTip mainlink" name="' + i18n.t("jobInformation", "Job Information") + '" id="job' + job.jobid + rowcount + '" tabindex="-1">' + job.jobno + '</a> ' +
												'<img src="img/icons/mouse.png" width="12" height="12" class="image_inl" alt="" title="" />' + 
											'</td>' +											
											'<td class="contact">';
												if (job.companyOnstop)
												{
													content += '<img src="img/icons/flag_red.png" width="12" height="12" alt="' + i18n.t("contactCompanyOnStop", "Contact Company Onstop") + '" title="' + i18n.t("contactCompanyOnStop", "Contact Company Onstop") + '" />';
												}
												content += '<a href="viewperson.htm?personid=' + job.personid + '&amp;ajax=contact&amp;width=380" ';
												if (!job.contactActive)
												{
													content += 'class="jconTip mainlink-strike" ';
												}
												else
												{
													content += 'class="jconTip mainlink" ';
												}
									content += 	'name="' + i18n.t("contactInformation", "Contact Information") + '" id="contact' + job.personid + rowcount + '" tabindex="-1">' + job.contact + '</a> ' +
												'<img src="img/icons/mouse.png" width="12" height="12" class="image_inl" alt="" title="" />' +
											'</td>' +
											'<td class="purchaseord">' + job.po + '</td>'+
											'<td class="clientref">' + job.clientref + '</td>'+
											'<td class="datein">' + job.datein + '</td>' +
										'</tr>';							
						});
						// append new rows to table
						$j('table#' + tableid + ' tbody').append(content);	
											
						// test if more jobs available and if so change link
						if(jobs.morePages)
						{
							// empty table footer and append new link to retrieve more jobs
							$j('table#' + tableid + ' tfoot tr:first td').empty()
																		.append('<a href="#" onclick=" loadJobsForCreation($j(this).closest(\'table\').attr(\'id\'), ' + coid + ', ' + resPerPage + ', ' + (pageNo + 1) + '); return false; " class="mainlink">' + i18n.t("core.quotation:fetchNextJobResults", {varNbResPerPage : resPerPage, defaultValue : "Fetch Next " + resPerPage + " Job Results"}) + '</a>');
						}
						else
						{
							// empty table footer
							$j('table#' + tableid + ' tfoot tr:first td').empty().append('&nbsp;');
						}
						// initialise all tooltips
						JT_init();
					}
					else
					{
						// empty table footer
						$j('table#' + tableid + ' tfoot tr:first td').empty().append('&nbsp;');
					}
				}
			});
		}
	});
}
