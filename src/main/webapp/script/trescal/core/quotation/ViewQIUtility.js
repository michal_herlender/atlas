/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: Stuart Harrold
*
*	FILENAME		:	ViewQIUtility.js
*	DESCRIPTION		:	This javascript file is imported into multiple page specific javascript files such as
* 					: 	ViewJob.js
*					: 	
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	10/03/2008 - SH - Created file
*					: 	28/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/


/**
 * this function retrieves all quotation items which are linked to the quotation id supplied using dwr,
 * if quotation items have already been retrieved then no DWR call is made.
 *
 * @param {Integer} rowcount is the row number we currently want to display
 * @param {Integer} id id of the quotation for which items should be retrieved
 * @param {String} quoteno the quote number for which items are being displayed
 */
function getQuoteItems(rowcount, id, quoteno)
{
	// quotation items for this quote row have already been retrieved
	if ($j('#qichild' + rowcount).length)
	{
		// quotation items are currently hidden so make them visible
		if ($j('#qichild' + rowcount).css('display') == 'none')
		{
			// remove image to show items
			$j('#qitemsLink' + rowcount).empty();
			// append image to hide items
			$j('#qitemsLink' + rowcount).append('<img src="img/icons/items_hide.png" width="16" height="16" alt="' + i18n.t("core.quotation:hideQuotationItems", "Hide Quotation Items") + '" title="' + i18n.t("core.quotation:hideQuotationItems", "Hide Quotation Items") + '" class="image_inline" />');
			// make the quotation items row visible
			$j('#qichild' + rowcount).css({ display: '', visibility: 'visible' });
			// add effect to slide down quotation items
			$j('#qichild' + rowcount + ' div').slideDown(1000);
		}
		// quotation items are currently visible so hide them
		else
		{
			// remove image to hide items
			$j('#qitemsLink' + rowcount).empty();
			// append image to show items
			$j('#qitemsLink' + rowcount).append('<img src="img/icons/items.png" width="16" height="16" alt="' + i18n.t("core.quotation:showQuotationItems", "Show Quotation Items") + '" title="' + i18n.t("core.quotation:showQuotationItems", "Show Quotation Items") + '" class="image_inline" />');
			// add effect to slide up quotation items and hide row
			$j('#qichild' + rowcount + ' div').slideUp(1000, function(){
				$j('#qichild' + rowcount).css({ display: 'none', visibility: '' });
			});
		}
	}
	// quotation items for this quote row have not been retrieved so call DWR
	else
	{
		// load javascript service file
		loadScript.loadScriptsDynamic(new Array('dwr/interface/quotationitemservice.js'), true,
		{
			callback: function()
			{
				// call dwr service to retrieve all quote items
				quotationitemservice.getDWRItemsForQuotation(id,
				{
					callback: function(quoteitemdtos)
					{
						var currentTime = new Date().getTime();
						// call method to display these quote items
						displayQuoteItems(quoteitemdtos, currentTime, rowcount, quoteno);
					}
				});
			}
		});
	}
}

/*
 * time value of last DWR call, checked against the current DWR call
 */
var lastUpdate = 0;

/**
 * this function displays the quotation items retrieved by (@see getQuoteItems())
 * 
 * @param {Array} quoteitems array of quotation items
 * @param {Date/Time} currentUpdate timestamp of the current search
 * @param {Integer} rowcount integer value of the row for which quotation items should be found and displayed
 */
function displayQuoteItems(quoteitemdtos, currentUpdate, rowcount, quoteno)
{
	// this is a new search
	if (currentUpdate > lastUpdate)
	{
		// update lastUpdate variable
		lastUpdate = currentUpdate;
		
		// work out the class to be used for child row dependant on rowcount (odd, even)
		var classname = 'odd';
		if(rowcount % 2 == 0)
		{
			classname = 'even';
		}

		// quoteitems length is greater than zero
		if(quoteitemdtos.length > 0)
		{
			// append a new child table row below the parent for this quote where quotation items will be listed
			$j('#qparent' + rowcount).after(	'<tr id="qichild' + rowcount + '" class="' + classname + '">' +
													'<td colspan="8">' +
														// table of quotation items added below
													'</td>' +
												'</tr>');
			// append div containing a table for the quotation items to be listed in to the new child table row
			$j('#qichild' + rowcount + ' td').append(	'<div style=" display: none; ">' +
															'<table class="child_table" summary="This table lists all quotation items for a particular quote">' +
																'<thead>' +
																	'<tr>' +
																		'<td colspan="2">' +
																			// Text filled here in separate code below
																		'</td>' +
																	'</tr>' +
																	'<tr>' +
																		'<th class="lqitemno" scope="col">' + i18n.t("itemNo", "Item No") + '</th>' +  
																		'<th class="lqinst" scope="col">' + i18n.t("instrument", "Instrument") + ' - ' + i18n.t("instrumentmodelname", "Instrument Model Name") + '</th>' +							
																	'</tr>' +
																'</thead>' +
																'<tfoot>' +
																	'<tr>' +
																		'<td colspan="2">&nbsp;</td>' +
																	'</tr>' +
																'</tfoot>' +
																'<tbody>' +
																	// rows are populated in separate code below
																'</tbody>' +
															'</table>' +
														'</div>');
																
			// display message in the thead section					
			$j('#qichild' + rowcount + ' thead tr:first td').append(i18n.t("core.quotation:nbQuotationItemsForQuoteNo", {varNbItems : quoteitemdtos.length, varQuoteNo : quoteno, defaultValue : "(" + quoteitemdtos.length + ") Quotation Items For " + quoteno}));
			
			// variables for holding table content and current heading and caltype info
			var content = '';
			var heading = '';
			var caltype = '';
			// add a row to the quotation items table for each quotation item returned
			$j.each(quoteitemdtos, function(i){
				
				// heading name has changed
				if (quoteitemdtos[i].headingName != heading)
				{
						// add row with new heading and first caltype
						content += '<tr style=" background-color: ' + quoteitemdtos[i].calTypeDisplayColor + '; ">' +
										'<td colspan="2">' + quoteitemdtos[i].headingName + ' - ' + quoteitemdtos[i].calTypeShortName + '</td>' +
									'</tr>';		
				}
				// heading name has not changed
				else
				{
					// has the calibration type changed
					if (quoteitemdtos[i].calTypeShortName != caltype)
					{
						// add row with the new calibration type
						content= content + 	'<tr style=" background-color: ' + quoteitemdtos[i].calTypeDisplayColor + '; ">' +
												'<td colspan="2">' + heading + ' - ' + quoteitemdtos[i].calTypeShortName + '</td>' +
											'</tr>';
					}
				}
				content += '<tr style=" background-color: ' + quoteitemdtos[i].calTypeDisplayColor + '; ">' +
									'<td class="center">' + quoteitemdtos[i].itemNo + '</td>' +
									'<td>' + quoteitemdtos[i].plantId + quoteitemdtos[i].fullModelName + '</td>' +
								'</tr>';
				// update variables with the current values
				heading = quoteitemdtos[i].headingName;
				caltype = quoteitemdtos[i].calTypeShortName;
																						
			});
			// append quote item rows to table
			$j('#qichild' + rowcount + ' tbody').append(content);
			// remove image to show items
			$j('#qitemsLink' + rowcount).empty();
			// append image to hide items
			$j('#qitemsLink' + rowcount).append('<img src="img/icons/items_hide.png" width="16" height="16" alt="' + i18n.t("core.quotation:hideQuotationItems", "Hide Quotation Items") + '" title="' + i18n.t("core.quotation:hideQuotationItems", "Hide Quotation Items") + '" class="image_inline" />');
			// add effect to slide down delivery items
			$j('#qichild' + rowcount + ' div').slideDown(1000);
		}
	}
}