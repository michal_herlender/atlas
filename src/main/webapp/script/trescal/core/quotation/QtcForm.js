/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	QtcForm.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	Commenting of code.
*	KNOWN ISSUES	:	-
*	HISTORY			:	22/06/2007
*
****************************************************************************************************************************************/


/*
 * Array of objects containing the element name and corner type which should be applied.
 * Applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/*
 * Element to be focused on when the page loads.
 * Applied in init() function.
 */
var focusElement = 'generalConditions';

/* this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = 	new Array( 	{ anchor: 'systemconditions-link', block: 'systemconditions-tab' },
								{ anchor: 'customconditions-link', block: 'customconditions-tab' } );