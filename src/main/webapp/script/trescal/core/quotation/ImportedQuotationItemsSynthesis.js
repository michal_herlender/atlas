/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	ImportedQuotationItemsSynthesis.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	01/06/2020
*
****************************************************************************************************************************************/

/**
 * this method disable the click on submit button after the first click
 * and this to prevent the user from clicking on the form submission 
 * button more than once
 * 
 */
function disableMeAfterFirstClick(btn) {
	btn.className = "disabled";
    btn.onclick = function(){return false}
}