/**
 *	FILENAME		:	EditQuotation.js
 */

function init()
{
	if($j('#statusid option:selected').data('accepted') == 'yes')
		$j('#clientacceptedon').removeClass('hid').addClass('show');
	else
		$j('#clientacceptedon').removeClass('show').addClass('hid');
}

/**
 * Array of calendar objects containing the initial setup literals needed
 * @param inputField (String) id of the input field to which the calendar should be binded
 * @param dateFormat (String) format that the date should be displayed
 * @param displayDates (String) 'all' make all dates available
 * 								'future' show only dates in the future
 * 								'past' show only dates in the past
 * Applied in init() function.
 */								
//var jqCalendar = new Array ( { 
//								 inputField: 'reqDate',
//								 dateFormat: 'dd.mm.yy',
//								 displayDates: 'all',
//								 showWeekends: true
//							 },
//							 { 
//								 inputField: 'duedate',
//								 dateFormat: 'dd.mm.yy',
//								 displayDates: 'all',
//								 showWeekends: true
//							 },
//							 { 
//								 inputField: 'contractStartDate',
//								 dateFormat: 'dd.mm.yy',
//								 displayDates: 'all',
//								 showWeekends: true
//							 },
//							 { 
//								 inputField: 'clientAcceptanceOn',
//								 dateFormat: 'dd.mm.yy',
//								 displayDates: 'past'
//							 }
//							);

/**
 * this function appends the div needed to create a cascading search plugin to the page and
 * calls the initialisation function which builds the search plugin and prefills any data if needed.
 * 
 * @param {String} coid is  the company id to be preloaded
 * @param {Integer} subdivid is the subdivision id to be preloaded
 * @param {Integer} personid is the contact id to be preloaded
 */
function createCascadingSearchPlugin(coid, subdivid, personid)
{
	// has the cascading search plugin already been created
	if ($j('div#cascadeSearchPlugin').length)
	{
		// do nothing
	}
	else
	{
		// load the service javascript file if not already	
		loadScript.loadScriptsDynamic(new Array('script/trescal/core/utilities/searchplugins/CascadeSearchPlugin.js'), true,
		{
			callback: function()
			{
				// remove the hidden input field for contact currently on page
				// append div in which the multi model search plugin will be created
				$j('input#tempId').after(	'<div id="cascadeSearchPlugin">' +
												'<input type="hidden" id="compCoroles" value="client,prospect,business" />' +
												'<input type="hidden" id="cascadeRules" value="subdiv,contact" />' +
												'<input id="prefillIds" type="hidden" value="' + coid + ',' + subdivid + ',' + personid + '" />' +
											'</div>' +																														
											// clear floats and restore page flow
											'<div class="clear"></div>').parent().removeClass().addClass('vis').end().remove();
				// initialise the multi model search plugin
				CascadeSearchPlugin.init();
			}
		});
	}
}

function showClientAcceptanceDateInput(accepted){
	console.log(accepted);
	if(accepted == 'yes')
		$j('#clientacceptedon').removeClass('hid').addClass('show');
	else
		$j('#clientacceptedon').removeClass('show').addClass('hid');
}
