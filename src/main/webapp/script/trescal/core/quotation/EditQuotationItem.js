/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: Richard Dean
*
*	FILENAME		:	editquotationitem.js
*	DESCRIPTION		:	This javascript file is used on the editquotationitem.vm page.
*					: 	
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	31/01/2007 - RD - Created File
*					:	28/10/2015 - TProvost - Minor fix on enable buttons
*					:	28/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'dwr/interface/quotationitemservice.js',
								'script/trescal/core/tools/multicurrency.js',
								'script/trescal/core/utilities/DOMUtils.js',
								'script/thirdparty/jQuery/jquery.boxy.js');

/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' },
							    { element: 'ul.subnavtab a', corner: 'small transparent top' } );
							    
/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'plantno';

var subMenuElements = 	new Array( 
	   	   { anchor: 'jobcosting-link', block: 'jobcosting-tab' },
	   	   { anchor: 'invoiceprice-link', block: 'invoiceprice-tab'},
	   	   { anchor: 'quotationitem-link', block: 'quotationitem-tab'},
	   	   { anchor: 'catalogprice-link', block: 'catalogprice-tab' },
	   	   { anchor: 'contractreview-link', block: 'contractreview-tab' },
	   	   { anchor: 'manual-link', block: 'manual-tab' },
	);


/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */				   
function init()
{
	// load stylesheet using the dom if not already
	loadScript.loadStyle('styles/jQuery/jquery-boxy.css', 'screen');
	if (quoteIssued)
	{
		// disable all form inputs
		$j('#editquotationitem input, #editquotationitem select').attr('disabled', 'disabled');
	}
}

/**
 * unlocks the form fields on the edit quotation item page
 */ 
function unlockQuotationItem()
{
	// enable all input and select form elements on edit quotation item page
	$j('#editquotationitem input, #editquotationitem select').attr('disabled', false);
	// update message to success box
	$j('div#lockedmessage').empty().append(	'<div class="successBox2">' +
												'<div class="successBox3">' +
													i18n.t("core.quotation:quotationItemTemporaryUnlocked", "Quotation item has been temporarily unlocked for editing") +
												'</div>' +
											'</div>').attr('class', 'successBox1');
}


/**
 * this method updates the display of a particular cost and also updates a hidden element
 * which indicates if this cost is to be included in the total cost
 * 
 * @param {String} costType the name of the cost type being activated / deactivated.
 * @param {Object} link DOM reference to the link being clicked on to call this function.
 * @param {String} costTypeActive the name of DOM hidden element indicating if the cost is active or not.
 */
function toggleIncludeCost(costType, link, costTypeActive)
{
	if($j('#' + costTypeActive).length)
	{	
		if($j('#' + costTypeActive).val() == 'true')
		{
			$j('#' + costType + 'Main').css('display', 'none');
			$j('#' + costTypeActive).val(false);
			$j(link).text(i18n.t("add", "Add"));
		}
		else
		{
			$j('#' + costType + 'Main').css('display', 'block');
			$j('#' + costTypeActive).val(true);
			$j(link).text(i18n.t("remove", "Remove"));
		}
	}
}

/**
 * this method toggles the visibility of the third party manual price input and the
 * linked quote third party price input
 * 
 * @param {Object} liManual list item object for manual third party price
 * @param {Object} liQuotation list item object for linked third party price
 * @param {String} costSrc string to indicate whether the current cost source is 'Manual' or 'Quotation'
 */
function toggleManualCost(liManual, liQuotation, costSrc)
{
	// show manual costs in form
	if (costSrc == "MANUAL")
	{
		$j(liManual).removeClass('hid').addClass('vis');
		$j(liQuotation).removeClass('vis').addClass('hid');
	}
	else
	{
		$j(liManual).removeClass('vis').addClass('hid');
		$j(liQuotation).removeClass('hid').addClass('vis');
	}
}

/**
 * this method creates the content to be displayed in a jquery boxy thickbox for third party quotation items that are
 * suitable to be linked to the current quotation item based on the given costtypeid and modelid.
 * 
 * @param {Integer} quoteitemid the id of the quotation item we link back to
 * @param {Integer} costtype the cost type (enum text) we are looking to match when searching third party quotation items
 * @param {Integer} modelid the id of the model we are looking to match when searching third party quotation items
 */
function displayTPCostSearch(quoteitemid, costtype, modelid)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/tpquotationitemservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to retrieve any matching third party quotation items
			tpquotationitemservice.getMatchingItems(costtype, modelid,
			{
				callback:function(tpqItems)
				{
					// start creating jquery boxy content string
					var content = 	'<div id="linkTPQuoteBoxy" class="overflow">' +
										'<table class="default4" summary="This table lists all third party quote items that can be linked">' +
											'<thead>' +
												'<tr>' +
													'<td colspan="10">';
														// depending on the amount of third party quote items returned, display message in the thead section					
														if (tpqItems.length == 1)
														{
															content += i18n.t("nbTPQuoteItemCanBeLinked", {varNbItems : tpqItems.length, defaultValue : "There is " + tpqItems.length + " third party quote item that can be linked"});
														}
														else
														{
															content += i18n.t("nbTPQuoteItemsCanBeLinked", {varNbItems : tpqItems.length, defaultValue : "There are " + tpqItems.length + " third party quote items that can be linked"});
														}
										content +=	'</td>' +
												'</tr>' +
												'<tr>' +
													'<th class="item" scope="col">' + i18n.t("item", "Item") + '</th>' +  
													'<th class="qno" scope="col">' + i18n.t("quoteNo", "Quote No") + '</th>' +
													'<th class="desc" scope="col">' + i18n.t("description", "Description") + '</th>' +
													'<th class="comp" scope="col">' + i18n.t("company", "Company") + '</th>' +
													'<th class="cont" scope="col">' + i18n.t("contact", "Contact") + '</th>' +
													'<th class="tpqno" scope="col">' + i18n.t("core.quotation:tpQuoteNo", "TP Quote No") + '</th>' +
													'<th class="daterec" scope="col">' + i18n.t("core.quotation:dateRec", "Date Rec") + '</th>' +													
													'<th class="caltype" scope="col">' + i18n.t("calType", "Cal Type") + '</th>' +
													'<th class="cost" scope="col">' + i18n.t("cost", "Cost") + '</th>' +
													'<th class="link" scope="col">' + i18n.t("link", "Link") + '</th>' +
												'</tr>' +
											'</thead>' +
											'<tfoot>' +
												'<tr>' +
													'<td colspan="10">&nbsp;</td>' +
												'</tr>' +
											'</tfoot>' +
											'<tbody>';										
												// add a row to the third party quote item table for each item returned
												$j.each(tpqItems, function(i, item)
												{
													// loop through the costs and match the cost type we require
													$j.each(item.costs, function(x, cost)
													{
														// is a cost type?
														if(cost.costType)
														{
															// this is the matching cost type required?
															
															if(cost.costType == costtype.toUpperCase())
															{
																content += 	'<tr>' +
																				'<td class="item">' + item.itemno + '</td>' +
																				'<td class="qno"><a href="viewtpquote.htm?id=' + item.tpquotation.id + '" target="_blank" class="mainlink">' + item.tpquotation.qno + '</a></td>' +
																				'<td class="desc">' + item.model.mfr.name + ' ' + item.model.model + ' ' + item.model.description.description + '</td>' +
																				'<td class="comp">' + item.tpquotation.contact.sub.comp.coname + '</td>' +
																				'<td class="cont">' + item.tpquotation.contact.name + '</td>' +
																				'<td class="tpqno">' + item.tpquotation.tpqno + '</td>' +
																				'<td class="daterec">' + item.tpquotation.formattedRegDate + '</td>' +
																				'<td class="caltype">' + (item.caltype.serviceType != null ? '' + item.caltype.serviceType.shortName : '') + '</td>' +
																				'<td class="cost">' + item.tpquotation.currency.currencyERSymbol + '' + cost.finalCost.toFixed(2) + '</td>' +
																				'<td class="link"><a href="#" class="mainlink" onclick=" setTPQuotationAsCostSource(' + quoteitemid + ', '+item.id + ', ' + cost.costType.typeid + '); return false; ">' + i18n.t("link", "Link") + '</a></td>' +
																			'</tr>';
															}
														}														
													});
												});
								content += 	'</tbody>' +
										'</table>' +
									'</div>';
					// create new boxy pop up using content variable
					var linkTPQuoteBoxy = new Boxy(content, {title: i18n.t("core.quotation:linkTPQuoteItemToQuoteItem", "Link third party quote item to quotation item"), modal: true, unloadOnHide: true});
					// show boxy pop up and adjust size
					linkTPQuoteBoxy.tween(860,300);					
				}
			});
		}
	});
}

/**
 * this method sets the given cost from a third party quotation item as the cost source for the given cost.
 * 
 * @param {Integer} quoteitemId the id of the quotation item that we are linking to
 * @param {Integer} tpqitemId the id of the third party quotation item to be linked
 * @param {Integer} costId the id of the cost to be linked
 */
function setTPQuotationAsCostSource(quoteitemId, tpqitemId, costId)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/tpquotationitemservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to find the chosen third party quotation item
			tpquotationitemservice.findTPQuotationItemWithCosts(tpqitemId,
			{
				callback:function(tpqItem)
				{
					// loop through the costs and match the cost type we require
					$j.each(tpqItem.costs, function(i, cost)
					{
						// is a cost type?
						if(cost.costType)
						{		
							// this is the matching cost type required?
							if(cost.costType.typeid == costId)
							{
								// if different currencies
								if(tpqItem.tpquotation.currency.currencyId != quoteCurrencyId)
								{
									tpquotationitemservice.getTPQuoteItemInConvertedCurrency(tpqItem.id, quoteitemId, cost.costType, 
									{
										callback:function(convertedValues)
										{
											// create content to append to page
											var content = i18n.t("core.quotation:tpQuotation", {varQuoteNo :  '<a href="' + springUrl + '/viewtpquote.htm?id=' + tpqItem.tpquotation.id + '" class="mainlink">' + tpqItem.tpquotation.qno + '</a>', varItemNo : tpqItem.itemno, defaultValue : 'TP Quotation <a href="' + springUrl + '/viewtpquote.htm?id=' + tpqItem.tpquotation.id + '" class="mainlink">' + tpqItem.tpquotation.qno + '</a> item ' + tpqItem.itemno}) + '<br />' + 
											'<b>(' + i18n.t("convertedCurrency", {varCurrencySymb : tpqItem.tpquotation.currency.currencyERSymbol, varFinalCost : cost.finalCost.toFixed(2), defaultvalue : "Converted currency</i> - originally " + tpqItem.tpquotation.currency.currencyERSymbol + "" + cost.finalCost.toFixed(2)}) + ')</b><br/>' +
											tpqItem.tpquotation.contact.sub.comp.coname + ' - ' +
											'<a href="#" onclick=" removeTPCost(\'' + cost.costType.name + '\'); return false; " class="mainlink">' + i18n.t("remove", "Remove") + '</a><br />';
											// add cost to input
											$j('div#linked' + cost.costType.toLowerCase() + 'cost').prev().find('input').val(convertedValues.cost);
											// add any carriage costs to fields
											$j('div#' + cost.costType.name + 'Costs input[name="quotationitem\\.' + cost.costType.toLowerCase() + 'Cost\\.tpCarriageOut"]').val(convertedValues.carriageOut);
											$j('div#' + cost.costType.name + 'Costs input[name="quotationitem\\.' + cost.costType.toLowerCase() + 'Cost\\.tpCarriageIn"]').val(convertedValues.carriageIn);
											// add content to page
											$j('div#linked' + cost.costType.toLowerCase() + 'cost span:first').empty().prepend(content);
											// add the id of the third party cost to be used
											$j('input[name="' + cost.costType.toLowerCase() + 'CostId"]').val(cost.costid);
											// close the boxy implementation
											Boxy.get($j('div#linkTPQuoteBoxy')).hide();
										}
									});
								}
								else
								{
									// create content to append to page
									var content = i18n.t("core.quotation:tpQuotation", {varQuoteNo :  '<a href="' + springUrl + '/viewtpquote.htm?id=' + tpqItem.tpquotation.id + '" class="mainlink">' + tpqItem.tpquotation.qno + '</a>', varItemNo : tpqItem.itemno, defaultValue : 'TP Quotation <a href="' + springUrl + '/viewtpquote.htm?id=' + tpqItem.tpquotation.id + '" class="mainlink">' + tpqItem.tpquotation.qno + '</a> item ' + tpqItem.itemno}) + '<br />' + 
										tpqItem.tpquotation.contact.sub.comp.coname + ' - ' + 
										'<a href="#" onclick=" removeTPCost(\'' + cost.costType + '\'); return false; " class="mainlink">' + i18n.t("remove", "Remove") + '</a><br />';
									// add cost to input
									$j('div#linked' + cost.costType.toLowerCase() + 'cost').prev().find('input').val(cost.finalCost.toFixed(2));
									// add any carriage costs to fields
									$j('div#' + cost.costType + 'Costs input[name="quotationitem\\.' + cost.costType.toLowerCase() + 'Cost\\.tpCarriageOut"]').val(tpqItem.carriageOut.toFixed(2));
									$j('div#' + cost.costType + 'Costs input[name="quotationitem\\.' + cost.costType.toLowerCase() + 'Cost\\.tpCarriageIn"]').val(tpqItem.carriageIn.toFixed(2));
									// add content to page
									$j('div#linked' + cost.costType.toLowerCase() + 'cost span:first').empty().prepend(content);
									// add the id of the third party cost to be used
									$j('input[name="' + cost.costType.toLowerCase() + 'CostId"]').val(cost.costid);
									// close the boxy implementation
									Boxy.get($j('div#linkTPQuoteBoxy')).hide();
								}
							}
						}
					});
				}
			});
		}
	});
}

/**
 * this method removes the currently linked third party cost for the given costtype
 * 
 * @costtypename the name of the costtype that this cost is being removed from
 */
function removeTPCost(costtypename)
{
	// create content to append to page
	var content = i18n.t("core.quotation:noTPQuoteCostLinked", "No TP Quote Cost Linked -");
	// clear cost in input
	$j('div#linked' + costtypename.toLowerCase() + 'cost').prev().find('input').val('0.00');
	// remove any carriage costs
	$j('div#' + costtypename + 'Costs input[name="quotationitem\\.' + costtypename.toLowerCase() + 'Cost\\.tpCarriageOut"]').val('0.00');
	$j('div#' + costtypename + 'Costs input[name="quotationitem\\.' + costtypename.toLowerCase() + 'Cost\\.tpCarriageIn"]').val('0.00');
	// add content to page
	$j('div#linked' + costtypename.toLowerCase() + 'cost span:first').empty().prepend(content);
	// clear the id of the third party cost to be used
	$j('input[name="' + costtypename.toLowerCase() + 'CostId"]').val('');
}

/**
 * this method checks all inputs of type checkbox that are contained within the parent element 
 * 
 * @param {Boolean} select true if boxes are to be checked and vice versa
 * @param {String} parentElement id of table in which to search for inputs of type checkbox
 */
function selectAllItems(select, parentElement, inputClass)
{
	// check all
	if(select == true)
	{
		if (inputClass != null)
		{
			$j('.' + parentElement + ' input:checkbox[class="' + inputClass + '"]').check('on');	
		}
		else
		{
			$j('.' + parentElement + ' input:checkbox').check('on');
		}
	}
	// uncheck all
	else
	{
		if (inputClass != null)
		{
			$j('.' + parentElement + ' input:checkbox[class="' + inputClass + '"]').check('off');
		}
		else
		{
			$j('.' + parentElement + ' input:checkbox').check('off');	
		}		
	}
}

function switchSourceCost(quoteid, plantid, modelid, servicetypeid, name){
	var cancel = i18n.t("core.jobs:jobItem.cancel", "Cancel");
	var save = i18n.t("save", "Save");
	$j.ajax({
	    url:'availablepricesources.htm',
	    type:'GET',
	    data: {
	    	plantid: plantid,
			quoteid : quoteid,
			modelid : modelid,
			servicetypeid : servicetypeid
		},
		async: true,
    }).done(function(content) {
    	//display the popup
       	$j.prompt(content,{
       		title: "Viewing Available Price Sources",
       		buttons: { [save]: true, [cancel]: false },
       		submit: function (e, val, m, f) {                            
                   if (val == true){
                	 //if no prebooking is selected show a message
                   	 if (!$j('input[name="priceSelected"]').is(':checked')) {
                   		alert("please select a price!");
                        }
                   	 else{
                   		 var price = $j('input[name="priceSelected"]:checked')[0].value;
                   		 var source = $j('input[name="priceSelected"]:checked')[0].id;
                   		 var finalPrice;
                   		 if(price == 'Manual'){
                   			finalPrice = $j('#manualPrice')[0].value;
                   		 } else {
                   			 finalPrice = price;
                   		 }
                   		 if(finalPrice){
                   			 // update the price input value
                   			 document.getElementById('cost'+name).value = finalPrice;
                   			 // update the price source hidden value
                   			 document.getElementById('costSource'+name).value = source;
                   			 // update the source input value
                   			 document.getElementById('spanSource'+name).innerText = source;
                   		 }	
                   	 }
                   }	
                   else
                       console.log('Cancel!');
               }
       	});       	
      });
}

function updateCostSource(name){
	// update the price source hidden value
	if(document.getElementById('costSource'+name)){
		document.getElementById('costSource'+name).value = 'MANUAL';
	}
	// update the source input value
	if( document.getElementById('spanSource'+name)){
		 document.getElementById('spanSource'+name).innerText = 'MANUAL';
	}	 	
}



