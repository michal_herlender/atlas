/****************************************************************************************************************************************
*	FILENAME		:	scheduledquotationrequest.js
*	DESCRIPTION		:	This javascript file is used on the scheduledquotationrequest.vm page.
*					: 	
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	28/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

var pageImports = new Array (	'script/trescal/core/utilities/DOMUtils.js' );

var menuElements = 	new Array( { anchor: 'outstanding-link', block: 'outstanding-tab' },
   	   { anchor: 'new-link', block: 'new-tab' },
   	   { anchor: 'old-link', block: 'old-tab' });



function toggleDateVisibility(showDateEntry) {
	if (showDateEntry) {
		$j('li#daterange').addClass('vis').removeClass('hid');
	}
	else {
		$j('li#daterange').addClass('hid').removeClass('vis');
	}
}