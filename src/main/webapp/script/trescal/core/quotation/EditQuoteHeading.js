/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	EditQuoteHeading.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	11/06/2007
*
****************************************************************************************************************************************/


/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * this method selects/de-selects all quote item checkboxes for caltype
 * 
 * @param {Boolean} indicates whether all delete quote item checkboxes should be checked
 * @param {Integer} caltypeId the id of the calibration type
 */
function toggleQIDeleteItems(check, caltypeId)
{
	// check all?
	if (check)
	{
		$j('input[type="checkbox"][class="headItem_' + caltypeId + '"]').check('on');
	}
	else
	{
		$j('input[type="checkbox"][class="headItem_' + caltypeId + '"]').check('off');
	}
}