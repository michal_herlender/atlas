function disableControls() {
	$j('#input_subject').attr('disabled','disabled');
	$j('#textarea_body').attr('disabled','disabled');
}

function enableControls() {
	$j('#input_subject').attr('disabled',false);
	$j('#textarea_body').attr('disabled',false);
}