/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	ViewQuotation.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	Commenting of code.
*	KNOWN ISSUES	:	-
*	HISTORY			:	07/06/2007
*					:	28/10/2015 - TProvost - Minor fix on enable buttons
*					:	28/10/2015 - TProvost - Manage i18n
*					:	28/10/2015 - TProvost - Fix bug in new version of Impromptu.js - Replace the "callback" property (property disappeared) by the "submit" property
*												=> Changes made in function compileAndSubmitQisToDelete
*					:	28/10/2015 - TProvost - Fix bug on multicompany : add parameter subdivId at functions copyQuotationItemsToHeadingAndCaltype and	addModulesToBaseUnit
*					:	02/11/2015 - TProvost - Add import ScriptedUtils.js
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'script/trescal/core/utilities/DocGenPopup.js',
								'script/trescal/core/utilities/ScriptedUtils.js',
								'script/trescal/core/tools/FileBrowserPlugin.js',
								'script/thirdparty/jQuery/jquery.tablesorter.js',
								'script/thirdparty/jQuery/jquery.scrollTo.js');

/**
 * array of objects containing the element name and corner type which should be
 * applied. applied in init() function.
 */
var niftyElements = new Array (	{ element: 'ul#termsandconditions h3', corner: 'top' },
							    { element: 'ul#tabnav a', corner: 'small transparent top' },
								{ element: 'div.infobox', corner: 'normal' },
								{ element: 'ul.subnavtab a', corner: 'small transparent top' } );

/**
 * this variable holds an array of id objects, one for the navigation link and
 * one for the content area the link refers to. this array is passed to the
 * Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = 	new Array( 	{ anchor: 'viewquotationitems-link', block: 'viewquotationitems-tab' },
								{ anchor: 'summary-link', block: 'summary-tab' },
								{ anchor: 'quotedocs-link', block: 'quotedocs-tab' },
								{ anchor: 'tpquoterequests-link', block: 'tpquoterequests-tab' },
								{ anchor: 'editquotation-link', block: 'editquotation-tab' },
								{ anchor: 'priorstatushistory-link', block: 'priorstatushistory-tab' },
								{ anchor: 'termsandcond-link', block: 'termsandcond-tab' },
								{ anchor: 'jobs-link', block: 'jobs-tab' },
								{ anchor: 'quoterequests-link', block: 'quoterequests-tab' },
								{ anchor: 'email-link', block: 'email-tab' },
								{ anchor: 'importquotationitem-link', block: 'importquotationitem-tab' },
								{ anchor: 'instructions-link', block: 'instructions-tab' }
								);

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if
 * present. this allows onload coding to be included in the page specific
 * javascript file without adding to the body onload attribute.
 */	
function init()
{
	// scroll top marker not empty?
	if (scrollTopMarker != '')
	{
		// scroll the page to correct location and offset slightly so not right
		// at top of page
		$j.scrollTo($j('#' + scrollTopMarker), 600, { offset: {top: -140, left: 0} });
	}
	// make table columns sortable and apply zebra effect
    $j('table.tablesorter').tablesorter();
    
    $j(".readonly").keydown(function(e){
        e.preventDefault();
    });
}

var jqCalendar = new Array ( 
		{ 
			 inputField: 'clientacceptedon',
			 dateFormat: 'dd.mm.yy',
			 displayDates: 'past'
	});

/**
 * this method sets a marker on the quotation which uses the row id made up of
 * 'item' + the quote item id. this id is set on the session and retrieved on
 * page load so that the position can be restored when reloaded.
 * 
 * @param {Object}
 *            anchor the link clicked to set a marker
 * @param {Integer}
 *            quoteid the id of the quotation
 * @param {Integer}
 *            qiid the id of the quotation item
 */
function setScrollTopAttribute(anchor, quoteid, qiid)
{
	// anchor has already been marked in which case we want to remove the marker
	if ($j(anchor).attr('class').trim() == 'quotationMarked')
	{
		// set original style back into attribute
		$j(anchor).closest('tr').attr('style', 'background-color: ' + $j('a.quotationMarked').next().val() + ';');
		// reset marker with unmarked class
		$j(anchor).removeClass().addClass('quotationMarker');
		// call dwr service to set session variable to new scroll top value
		dwrservice.setScrollTopMarker('sessSetScrollTopMarker' + quoteid, '');
	}
	else
	{
		// check to see if quotation has already been marked?
		if ($j('a.quotationMarked').length > 0)
		{
			// set original style back into attribute
			$j('a.quotationMarked').closest('tr').attr('style', 'background-color: ' + $j('a.quotationMarked').next().val() + ';');
			// reset marker with unmarked class
			$j('a.quotationMarked').removeClass().addClass('quotationMarker');
		}
		// set marker on new anchor
		$j(anchor).removeClass().addClass('quotationMarked');
		// add highlighting to row which is parent of anchor
		$j(anchor).closest('tr').attr('style', $j(anchor).closest('tr').attr('style') + ' background-color: #B9DCFF;');
		// call dwr service to set session variable to new scroll top value
		dwrservice.setScrollTopMarker('sessSetScrollTopMarker' + quoteid, $j(anchor).closest('tr').attr('id'));
	}
}

/**
 * unlocks the edit links and displays delete links for items and the entire
 * quotation.
 */
function unlockQuotation()
{
	// show any links that have limited access when quote issued
	$j('a.limitAccess').removeClass('notvis');
	// update message to success box
	$j('div#lockedmessage').empty().append(	'<div class="successBox2">' +
												'<div class="successBox3">' +
													i18n.t("core.quotation:quotationTemporaryUnlocked", "Quotation has been temporarily unlocked for editing") +
												'</div>' +
											'</div>').attr('class', 'successBox1');
	// unlock any disabled menu options
	$j('input[name^="qiDelete_"][disabled="disabled"], input[name="qiDeleteAll"][disabled="disabled"]').each(function(i)
	{
		$j(this).attr('disabled', false);
	});
}

/**
 * this function deletes the quotation from the database
 * 
 * @param {Integer}
 *            quoteId is the id of the schedule to be deleted
 */
function deleteQuote(quoteId)
{
	if(confirm(i18n.t("core.quotation:confirmDeleteQuote", "This quotation will now be deleted from the database. Click 'OK' to confirm the deletion or 'Cancel' to go back")))
	{
		
		$j.ajax({
			url : "deleteQuotation.json",
			method : "POST",
			traditional : true,
			dataType : "json",
			data : {
				quoteId : quoteId
			},
			async : true
		}).done(function(data) {
			if(data.success==true){
				window.location.href = 'createquote.htm';
			} else {
				// show user message
				$j.prompt(result.message);
			}
		});
	}
}

/**
 * Updates's the showtotalheadingvalue setting of the quotation to the given
 * boolean value.
 * 
 * @param {object}
 *            anchor anchor object to be replaced
 * @param {boolean}
 *            show if true then the quotation total value is displayed.
 * @param {int}
 *            quoteid the id of the quotation to update.
 */
function updateHeadingTotalVisibility(anchor, show, quoteid)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/quotationservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to update the quotation heading total visibility
			quotationservice.updateQuotationHeadingTotalVisibility(show, quoteid,
			{
				callback:function(results)
				{
					// update quote total failed?
					if(!results.success)
					{
						$j.prompt(i18n.t("core.quotation:errorUpdatingHeadingTotal", "Updating show heading total setting failed.") + ' ' + results.message);	
					}
					else
					{
						// get class value from anchor
						var classname = $j(anchor).attr('class');
						// change all heading anchors on page
						$j('a.' + classname).each(function(i)
						{
							// showing quote heading total?
							if (show)
							{
								$j(this).after(	'<a href="#" class="showTotalHeadingValue' + quoteid + '" onclick=" updateHeadingTotalVisibility(this, false, ' + quoteid + '); return false; ">' +
													'<img src="img/icons/page_tick.png" width="21" height="19" class="img_marg_bot" alt="' + i18n.t("core.quotation:totalDisplayed", "Total displayed on documents created") + '" title="' + i18n.t("core.quotation:totalDisplayed", "Total displayed on documents created") + '" />' +
												'</a> ').remove();
							}
							else
							{
								$j(this).after(	'<a href="#" class="showTotalHeadingValue' + quoteid + '" onclick=" updateHeadingTotalVisibility(this, true, ' + quoteid + '); return false; ">' +
													'<img src="img/icons/page_cross.png" width="21" height="19" class="img_marg_bot" alt="' + i18n.t("core.quotation:totalNotDisplayed", "Total not displayed on documents created") + '" title="' + i18n.t("core.quotation:totalNotDisplayed", "Total not displayed on documents created") + '" />' +
												'</a> ').remove();
						}
						});
					}	
				}
			});
		}
	});
}

/**
 * Updates's the showtotalheadingcaltypevalue setting of the quotation to the
 * given boolean value.
 * 
 * @param {object}
 *            anchor anchor object to be replaced
 * @param {boolean}
 *            show if true then the quotation total value is displayed.
 * @param {int}
 *            quoteid the id of the quotation to update.
 */
function updateHeadingCaltypeTotalVisibility(anchor, show, quoteid)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/quotationservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to update the quotation heading total visibility
			quotationservice.updateQuotationHeadingCaltypeTotalVisibility(show, quoteid,
			{
				callback:function(results)
				{
					// update quote total failed?
					if(!results.success)
					{
						$j.prompt(i18n.t("core.quotation:errorUpdatingHeadingCalTypeTotal", "Updating show heading caltype total setting failed.") + ' ' + results.message);	
					}
					else
					{
						// get class value from anchor
						var classname = $j(anchor).attr('class');
						// change all heading anchors on page
						$j('a.' + classname).each(function(i)
						{
							// showing quote heading total?
							if (show)
							{
								$j(this).after(	'<a href="#" class="showTotalHeadingCaltypeValue' + quoteid + '" onclick=" updateHeadingCaltypeTotalVisibility(this, false, ' + quoteid + '); return false; ">' +
													'<img src="img/icons/page_tick.png" width="21" height="19" class="img_marg_bot" alt="' + i18n.t("core.quotation:totalDisplayed", "Total displayed on documents created") + '" title="' + i18n.t("core.quotation:totalDisplayed", "Total displayed on documents created") + '" />' +
												'</a> ').remove();
							}
							else
							{
								$j(this).after(	'<a href="#" class="showTotalHeadingCaltypeValue' + quoteid + '" onclick=" updateHeadingCaltypeTotalVisibility(this, true, ' + quoteid + '); return false; ">' +
													'<img src="img/icons/page_cross.png" width="21" height="19" class="img_marg_bot" alt="' + i18n.t("core.quotation:totalNotDisplayed", "Total not displayed on documents created") + '" title="' + i18n.t("core.quotation:totalNotDisplayed", "Total not displayed on documents created") + '" />' +
												'</a> ').remove();
						}
						});
					}	
				}
			});
		}
	});
}

/**
 * Updates's the showtotalquotecaltypevalue setting of the quotation to the
 * given boolean value.
 * 
 * @param {object}
 *            anchor anchor object to be replaced
 * @param {boolean}
 *            show if true then the quotation caltype total values are
 *            displayed.
 * @param {int}
 *            quoteid the id of the quotation to update.
 */
function updateQuotationCaltypeTotalVisibility(anchor, show, quoteid)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/quotationservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to update the quotation caltype total visibility
			quotationservice.updateQuotationCaltypeTotalVisibility(show, quoteid,
			{
				callback:function(results)
				{
					// update quote total failed?
					if(!results.success)
					{
						$j.prompt(i18n.t("core.quotation:errorUpdatingQuotationCalTypeTotal", "Updating show quotation caltype total setting failed.") + ' ' + results.message);	
					}
					else
					{
						// get class value from anchor
						var classname = $j(anchor).attr('class');
						// change all caltype anchors on page
						$j('a.' + classname).each(function(i)
						{
							// showing quote heading total?
							if (show)
							{
								$j(this).after(	'<a href="#" class="showTotalQuoteCaltypeValue' + quoteid + '" onclick=" updateQuotationCaltypeTotalVisibility(this, false, ' + quoteid + '); return false; ">' +
													'<img src="img/icons/page_tick.png" width="21" height="19" class="img_marg_bot" alt="' + i18n.t("core.quotation:totalDisplayed", "Total displayed on documents created") + '" title="' + i18n.t("core.quotation:totalDisplayed", "Total displayed on documents created") + '" />' +
												'</a> ').remove();
							}
							else
							{
								$j(this).after(	'<a href="#" class="showTotalQuoteCaltypeValue' + quoteid + '" onclick=" updateQuotationCaltypeTotalVisibility(this, true, ' + quoteid + '); return false; ">' +
													'<img src="img/icons/page_cross.png" width="21" height="19" class="img_marg_bot" alt="' + i18n.t("core.quotation:totalNotDisplayed", "Total not displayed on documents created") + '" title="' + i18n.t("core.quotation:totalNotDisplayed", "Total not displayed on documents created") + '" />' +
												'</a> ').remove();
						}
						});
					}	
				}
			});
		}
	});
}

/**
 * Updates's the showtotalquotevalue setting of the quotation to the given
 * boolean value.
 * 
 * @param {object}
 *            anchor anchor object to be replaced
 * @param {boolean}
 *            show if true then the quotation total value is displayed.
 * @param {int}
 *            quoteid the id of the quotation to update.
 */
function updateTotalVisibility(anchor, show, quoteid)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/quotationservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to update the quotation total visibility
			quotationservice.updateQuotationTotalVisibility(show, quoteid,
			{
				callback:function(results)
				{
					// update quote total failed?
					if(!results.success)
					{
						$j.prompt(i18n.t("core.quotation:errorUpdatingTotal", "Updating show total setting failed.") + ' ' + results.message);	
					}
					else
					{
						// showing quote total?
						if (show)
						{
							$j(anchor).after(	'<a href="#" onclick=" updateTotalVisibility(this, false, ' + quoteid + '); return false; ">' +
													'<img src="img/icons/page_tick.png" width="21" height="19" class="img_marg_bot" alt="' + i18n.t("core.quotation:totalDisplayed", "Total displayed on documents created") + '" title="' + i18n.t("core.quotation:totalDisplayed", "Total displayed on documents created") + '" />' +
												'</a> ').remove();
						}
						else
						{
							$j(anchor).after(	'<a href="#" onclick=" updateTotalVisibility(this, true, ' + quoteid + '); return false; ">' +
													'<img src="img/icons/page_cross.png" width="21" height="19" class="img_marg_bot" alt="' + i18n.t("core.quotation:totalNotDisplayed", "Total not displayed on documents created") + '" title="' + i18n.t("core.quotation:totalNotDisplayed", "Total not displayed on documents created") + '" />' +
												'</a> ').remove();
						}
					}	
				}
			});
		}
	});
}

/**
 * this method creates the content for an overlay to add a module to a base unit
 * 
 * @param {Integer}
 *            qiid the id of the quotation item which is the base unit requiring
 *            more modules
 * @param {Integer}
 *            subdivId id of the subdiv to assign the quotation item
 */
function addModulesToBaseUnit(qiid)
{
	// initialise content variable
	var content = '';
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/quotationitemservice.js'), true,
	{
		callback: function()
		{
			quotationitemservice.getModulesAvailableForBaseUnitDWR(qiid, allocatedSubdivId,
			{
				callback:function(result)
				{
					// retrieve module list successful?
					if (result.success)
					{
						// add list of quote item module wrappers to variable
						var qimodwraps = result.results;
						// check qimodwraps size
						if (qimodwraps.length > 0)
						{
							// start creating overlay content
							content += 	'<div id="addModuleToBaseUnitOverlay">' +
											'<table class="default4 moduleToBaseUnit" summary="Lists all modules associated with the base unit passed">' +
												'<thead>' +
													'<tr>' +
														'<td colspan="5">' + i18n.t("core.quotation:nbModulesAvaibles", "Modules Available") + ' (' + qimodwraps.length + ')</td>' +
													'</tr>' +
													'<tr>' +
														'<th class="add">' + i18n.t("add", "Add") + '</th>' +
														'<th class="desc">' + i18n.t("description", "Description") + '</th>' +
														'<th class="plantno">' + i18n.t("plantNo", "Plant No") + '</th>' +
														'<th class="cost">' + i18n.t("cost", "Cost") + '</th>' +
														'<th class="qty">' + i18n.t("qty", "Qty") + '</th>' +
													'</tr>' +
												'</thead>' +
												'<tbody>';													
													// list all available
													// modules
													$j.each(qimodwraps, function(i, wrap)
													{		
														// assign module to
														// variable
														var mod = wrap.impo.module;
														// create a default
														// empty cost for this
														// calibration
														var cost = '0.00';
														// check for default
														// model cost
														if ((wrap.modelCalCost != null) && (wrap.modelCalCost != ''))
														{
															cost = wrap.modelCalCost.toFixed(2);
														}
														// generate content for
														// module
														content += 	'<tr>' +
																		'<td class="add">' +
																			'<input type="checkbox" name="moduleToAdd" value="' + mod.modelid + '"/>' +
																		'</td>' +
																		'<td class="desc">' + mod.definitiveModel + '</td>' +
																		'<td class="plantno"><input type="text" name="modulePlantNo" size="10" /></td>' +
																		'<td class="cost"><input type="text" name="moduleCost" value="' + cost + '" size="6" /></td>' +
																		'<td class="qty"><input type="text" name="moduleqty" value="1" size="1" /></td>' +
																	'</tr>';
													});																							
									content +=	'</tbody>' +
											'</table>' +
											'<div class="text-center marg-top marg-bot">' +		
												'<input type="button" onclick=" this.disabled = true; submitModulesToBaseUnit(' + qiid + '); return false; " value="' + i18n.t("core.quotation:addModules", "Add Modules") + '" />' +
											'</div>' +
										'</div>';
							// create new overlay
							loadScript.createOverlay('dynamic', i18n.t("core.quotation:addModulesToQuotationBaseUnit", "Add Modules To Quotation Base Unit"), null, content, null, 60, 680, null);
						}
						else
						{
							// display message to user
							$j.prompt(i18n.t("core.quotation:noModulesFoundforBaseUnit", "No Modules Found For Base Unit"));
						}
						
					}
					else
					{
						// display message to user
						$j.prompt(result.message);
					}
				}
			});
		}
	});
}

/**
 * dto object to pass values to dwr service
 * 
 * @param {Integer}
 *            modelId id of module model
 * @param {String}
 *            plantNo string plant no
 * @param {Integer}
 *            qty amount of modules of this type
 * @param {String}
 *            unitCost quotation cost for new module
 * 
 * @returns {moduleToBaseUnitDTO}
 */
function ModuleToBaseUnitDTO(modelId, plantNo, qty, unitCost)
{
	this.modelId = modelId;
	this.plantNo = plantNo;
	this.qty = qty;
	this.unitCost = unitCost;	
}

/**
 * this method creates the dto object necessary for modules which require adding
 * to quote item base unit
 * 
 * @param {Integer}
 *            qiid id of the quote item base unit
 */
function submitModulesToBaseUnit(qiid)
{
	// get modules to be added
	var dtoArray = [];
	// get selected
	$j('div#addModuleToBaseUnitOverlay input[name="moduleToAdd"]:checked').each(function(i, input)
	{
		// get table row for selected module
		var row = $j(input).closest('tr');
		var modelId = this.value;
		var plantNo = $j(row).find('input[name="modulePlantNo"]').val();
		var qty = $j(row).find('input[name="moduleqty"]').val();
		var cost = $j(row).find('input[name="moduleCost"]').val();
		// add dto to dto array
		dtoArray.push(new ModuleToBaseUnitDTO(modelId, plantNo, qty, cost));
	});	
	// dto array populated?
	if (dtoArray.length > 0)
	{
		// load the service javascript file if not already
		loadScript.loadScriptsDynamic(new Array('dwr/interface/quotationitemservice.js'), true,
		{
			callback: function()
			{
				// call dwr service to add modules to base unit
				quotationitemservice.addModulesToQuotationItemBaseUnit(qiid, dtoArray, session.getAttribrute("allocatedSubdiv").key,
				{
					callback:function(result)
					{
						// addition of modules successful?
						if (result.success)
						{
							// reload the quotation page
							window.location.reload(true);
						}
						else
						{
							// show error message to user
							loadScript.modifyOverlay(null, result.message, null, true, null, null, null);
						}
					}
				});
			}
		});
	}
	else
	{
		// modify overlay and show error message
		loadScript.modifyOverlay(null, i18n.t("core.quotation:selectOneModule", "Please select at least one module!"), null, true, null, null, null);
		// enable button
		$j('div#addModuleToBaseUnitOverlay input[type="button"]').attr('disabled', false);
	}
}

/**
 * Creates an ajax call which returns a list of quotestatus status's that are
 * 'accepted' and puts them in a drop-down list and creates a link to issue the
 * quote.
 * 
 * @param url
 *            the basic url to submit to to accept a quotation.
 */
function acceptQuotation(url)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/statusservice.js'), true,
	{
		callback: function()
		{
			statusservice.ajaxFindAllAcceptedStatus(
			{
				callback:function(statusList)
				{
					if(statusList.length > 1)
					{
						// more than one status is 'accepted' for quotations,
						// create a drop down box + link
						$j('#acceptedlink').hide();
						// create content
						var content = 	'<select name="acceptedstatusid" id="acceptedstatusid">';
											$j.each(statusList, function(i)
											{
												content += '<option value="' + statusList[i].statusid + '" ';
												if (i == 1)
												{
													content += 'selected="selected" ';
												}
												content += '>' + statusList[i].name + '</option>';
											});
							content += 	'</select>' +
										'<a href="#" onclick="window.location=\'' + url + '\'+\'&statusid=\'+$j(\'#acceptedstatusid\').val();">' + i18n.t("accept", "Accept") + '</a>' +
										' | <a href="#" onclick="$j(\'#acceptedoptions\').empty();$j(\'#acceptedlink\').show();">' + i18n.t("cancel", "Cancel") + '</a>';
						// append status content
						$j('#acceptedoptions').append(content);
					}
					else
					{
						// only one status for accepted, so just submit away
						window.location = url;
					}
				}
			});
		}
	});
}

/**
 * this method selects/de-selects all quote item delete checkboxes
 * 
 * @param {Boolean}
 *            indicates whether all delete quote item checkboxes should be
 *            checked
 * @param {Integer}
 *            headingId the id of the heading to check all items for
 * @param {Integer}
 *            caltypeId the id of the calibration type
 */
function toggleQIDeleteItems(check, headingId, caltypeId)
{
	// check all?
	if (check)
	{
		$j('input[type="checkbox"][name="qiDelete_' + headingId + '_' + caltypeId + '"]').check('on');
		monitorCheckedItems();
	}
	else
	{
		$j('input[type="checkbox"][name="qiDelete_' + headingId + '_' + caltypeId + '"]').check('off');
		monitorCheckedItems();
	}
}

/**
 * this method checks if any quote item delete checkboxes have been selected and
 * displays the delete button if necessary
 */
function monitorCheckedItems()
{
	// any quote item delete checkboxes checked?
	if($j('input[type="checkbox"][name^="qiDelete_"]:checked').length > 0)
	{
		$j('.QIDel, .QICopy').removeClass('hid');
	}
	else
	{
		$j('.QIDel, .QICopy').addClass('hid');
	}
}

/**
 * this method compiles all of the quote items selected to be deleted into a
 * comma separated string and appends them to a url for deletion
 */
function compileAndSubmitQisToDelete(quoteId, personId, subdivId)
{
	// initial prompt for user
	$j.prompt(i18n.t("core.quotation:confirmDeleteQuotationItem", "Are you sure you wish to delete these quotation items?"),
	{ 
		submit: confirmcallback,
		buttons: 
		{ 
			Ok: true,
			Cancel: false 
		}, 
		focus: 1 
	});
	// callback method for initial prompt
	function confirmcallback(v,m)
	{
		// user confirmed action?
    	if (m)
		{
			// string for comma separated string of quote item ids
			var delIds = '';
			// any quote item delete checkboxes checked?
			if($j('input[type="checkbox"][name^="qiDelete_"]:checked').length > 0)
			{
				$j('input[type="checkbox"][name^="qiDelete_"]:checked').each(function(i, n)
				{
					// not first iteration?
					if (i > 0)
					{
						delIds += ',' + n.value;
					}
					else
					{
						delIds += n.value;
					}
				});
				// load the service javascript file if not already
				loadScript.loadScriptsDynamic(new Array('dwr/interface/quotationitemservice.js'), true,
				{
					callback: function()
					{
						quotationitemservice.ajaxDeleteQuotationItems(quoteId, delIds, personId, subdivId,
						{
							callback: function(result)
							{
								if(result.success)
								{
									window.location.href = 'viewquotation.htm?id=' + quoteId;
								}
								else
								{
									$j.prompt(result.message);
								}
							}
						});
					}
				});
			}
			else
			{
				$j.prompt(i18n.t("core.quotation:notChosenAnyQuoteItemsToDeleted", "You have not chosen any quote items to be deleted"));
			}
		}
	}
}

// setup the current page number for job quote links
var jqlPage = 1;

var rowcount = 6;


/**
 * this method compiles all of the quote items selected to be copied to a new
 * heading and calibration type, items selected should be validated to make sure
 * they are models as an instrument quote item can only appear in one heading
 * and calibration type.
 */
function compileAndSubmitQisToCopy(quoteId)
{	
	// string for comma separated string of quote item ids
	var copyIds = '';
	// any quote item copy checkboxes checked?
	if($j('input[type="checkbox"][name^="qiDelete_"]:checked').length > 0)
	{
		$j('input[type="checkbox"][name^="qiDelete_"]:checked').each(function(i, n)
		{
			// not first iteration?
			if (i > 0)
			{
				copyIds += ',' + n.value;
			}
			else
			{
				copyIds += n.value;
			}
		});
		// create content to be displayed in overlay
		var content = 	'<div id="copyQuotationItemsOverlay">' +
							'<fieldset>' +
								'<ol>' +
									'<li>' +
										'<p>' + i18n.t("core.quotation:selectQuotationHeadingAndCalType", "Please select the quotation heading and the calibration type you would like to copy your selected items to.") + '</p>' +
									'</li>' +
									'<li>' +
										'<label>' + i18n.t("core.quotation:headingLabel", "Heading:") + '</label>' +
										'<select id="qhforcopy">';
											$j.each(quoteHeadArray, function(x, qh)
											{
												content +=	'<option value="' + qh.headingId + '">' + qh.headingName + '</option>';
											});
							content +=	'</select>' +
									'</li>' +
									'<li>' +
										'<label>' + i18n.t("calTypeLabel", "Cal Type:") + '</label>' +
										'<select id="ctforcopy">';
											$j.each(caltypeArray, function(y, ct)
											{
												content +=	'<option value="' + ct.calTypeId + '">' + ct.calTypeName + '</option>';
											});
							content +=	'</select>' +
									'</li>' +
									'<li>' +
										'<label>&nbsp;</label>' +
										'<input type="button" value="' + i18n.t("core.quotation:copyItems", "Copy Items") + '" onclick=" this.disabled = true; validateAndCopyItems(\'' + copyIds + '\' , $j(\'select#qhforcopy\').val(), $j(\'select#ctforcopy\').val(), allocatedSubdivId); return false; "/>' +
									'</li>' +
								'</ol>' +
							'</fieldset>' +
						'</div>';
		// create overlay using content
		loadScript.createOverlay('dynamic', i18n.t("core.quotation:copyQuotationItems", "Copy quotation items"), null, content, null, 50, 580, null);
		
	}
	else
	{
		// show message to user
		$j.prompt('You have not chosen any quote items to copy');
	}
}

/**
 * this method validates and copies quotation items selected
 * 
 * @param {String}
 *            copyIds comma separated string of quotation item ids
 * @param {Integer}
 *            headingId id of the heading to add items to
 * @param {Integer}
 *            calTypeId id of the calibration type to assign to items
 * @param {Integer}
 *            subdivId id
 */
function validateAndCopyItems(copyIds, headingId, calTypeId, subdivId)
{
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/quotationitemservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to validate and copy quotation items
			quotationitemservice.copyQuotationItemsToHeadingAndCaltype(copyIds, headingId, calTypeId, subdivId,
			{
				callback: function(result)
				{
					// copy successful?
					if(result.success)
					{
						// reload the current page so new items are displayed
						// and totals re-calculated
						window.location.reload(true);
					}
					else
					{
						// show user error message
						loadScript.modifyOverlay(null, result.message, null, true, null, null, null);
					}
				}
			});
		}
	});
}

/**
 * this method loads any more available job costing calibration costs for this
 * model.
 * 
 * @param {Object}
 *            anchor the link clicked
 * @param {Integer}
 *            quoteid the quotation id.
 * @param {Integer}
 *            resPerPage the number of results to fetch.
 */
function fetchMoreJQLinks(anchor, quoteid, resPerPage)
{
	// change anchor text
	$j(anchor).text(i18n.t("loading", "Loading....."));
	// update page count
	jqlPage = jqlPage + 1;
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/jobquotelinkservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to retrieve any further job quote links
			jobquotelinkservice.findJobQuoteLinksResultsetDWR(quoteid, jqlPage, resPerPage,
			{
				callback:function(result)
				{
					// result returned?
					if (result.success)
					{
						// set results into variable
						var jqLinks = result.results;
						// results returned greater than zero?
						if (jqLinks.length > 0)
						{
							// variable to hold new content
							var content = '';
							// create new page content using results
							$j.each(jqLinks, function(i, link)
							{
								// assign job to variable
								var job = link.job;							
								// construct content
								content += 	'<tr ';
												if (rowcount % 2 == 0)
												{
													content += 'class="even"';
												}
												else
												{
													content += 'class="odd"';
												}
									content += 	'>' +
												'<td class="lj_jobno">' +
													'<a href="viewjob.htm?jobid=' + job.jobid + '&amp;ajax=job&amp;width=600" class="jconTip mainlink" name="' + i18n.t("jobInformation", "Job Information") + '" id="job' + job.jobid + '' + rowcount + '" tabindex="-1">' + job.jobno + '</a> ' +
													'<img src="img/icons/mouse.png" width="12" height="12" class="image_inl" alt="" title="" />' +
												'</td>' +
												'<td class="lj_comp">';
													if (job.con.sub.comp.onStop)
													{
														content += '<img src="img/icons/flag_red.png" width="12" height="12" alt="' + i18n.t("companyOnStop", "Company Onstop") + '" title="' + i18n.t("companyOnStop", "Company Onstop") + '" /> ';
													}
										content +=	'<a href="viewcomp.htm?coid=' + job.con.sub.comp.coid + '&amp;ajax=company&amp;width=400" ';
													if (!job.con.sub.comp.active)
													{
														content += 'class="jconTip mainlink-strike" ';	
													}
													else
													{
														content += 'class="jconTip mainlink" ';
													}
										content += 	'name="' + i18n.t("companyInformation", "Company Information") + '" id="company' + job.con.sub.comp.coid + '' + rowcount + '" tabindex="-1">' + job.con.sub.comp.coname + '</a> ' +
													'<img src="img/icons/mouse.png" width="12" height="12" class="image_inl" alt="" title="" />' +
												'</td>' +
												'<td class="lj_cont">';
													if (job.con.sub.comp.onStop)
													{
														content += '<img src="img/icons/flag_red.png" width="12" height="12" alt="' + i18n.t("contactCompanyOnStop", "Contact Company Onstop") + '" title="' + i18n.t("contactCompanyOnStop", "Contact Company Onstop") + '" /> ';
													}
										content +=  '<a href="viewperson.htm?personid=' + job.con.personid + '&amp;ajax=contact&amp;width=380" ';
													if ((!job.con.sub.comp.active) || (!job.con.sub.active) || (!job.con.active))
													{
														content += 'class="jconTip mainlink-strike" ';
													}
													else
													{
														content += 'class="jconTip mainlink" ';
													}
													content += 'name="' + i18n.t("contactInformation", "Contact Information") + '" id="contact' + job.con.personid + '' + rowcount + '" tabindex="-1">' + job.con.name + '</a> ' +
													'<img src="img/icons/mouse.png" width="12" height="12" class="image_inl" alt="" title="" />' +
												'</td>' +
												'<td class="lj_linkon">' + formatDate(link.linkedOn, 'dd.MM.yyyy') + '</td>' +
												'<td class="lj_linkby">' + link.linkedBy.name + '</td>' +
											'</tr>';
								// increment rowcount
								rowcount++;			
							});
							// amend job quote link result size
							$j('span.jqLinkSize').text(parseInt($j('span.jqLinkSize:first').text()) + jqLinks.length);
							// append new job quote links to table
							$j('table.quoteJobQuoteLinks tbody').append(content);			
							// initialise tooltips
							JT_init();
							// full set of results returned?
							if (jqLinks.length < resPerPage)
							{
								// change anchor text
								$j(anchor).text(i18n.t("core.quotation:allJobQuoteLinksLoaded", "All Job Quote Links Loaded"));
							}
							else
							{
								// change anchor text
								$j(anchor).text(i18n.t("loadMore", "Load More..."));
							}
						}
						else
						{
							// change anchor text
							$j(anchor).text(i18n.t("core.quotation:allJobQuoteLinksLoaded", "All Job Quote Links Loaded"));
						}
					}
					else
					{
						// show error message to user
						$j.prompt(result.message);
					}
				}
			});
		}
	});
}

/**
 * this method moves a quotation item within a heading and caltype section. If
 * the item is moved successfully then the page is reloaded.
 * 
 * @param {Integer}
 *            oldPosId id of the item in old position
 * @param {Integer}
 *            newPosId id of the item in the new position
 * @param {Integer}
 *            headingId id of the heading the items reside within
 * @param {Integer}
 *            calTypeId id of the caltype the items reside within
 */
function moveQuotationItem(oldPosId, newPosId, headingId, calTypeId)
{
	// disable all move quote item buttons
	$j('.moveQIButton').attr('disabled', 'disabled');
	// load the service javascript file if not already
	loadScript.loadScriptsDynamic(new Array('dwr/interface/quotationitemservice.js'), true,
	{
		callback: function()
		{
			// call dwr service to move the quotation items
			quotationitemservice.moveQuotationItem(oldPosId, newPosId, headingId, calTypeId,
			{
				callback: function(result)
				{
					// move item successful?
					if (result.success)
					{
						// reload the current page
						window.location.href = window.location.href;
					}
					else
					{
						// display error message
						$j.prompt(result.message);
						// enable all move quote item buttons
						$j('.moveQIButton').attr('disabled', false);
					}
				}
			});
		}
	});
	
}

function modifyQuotationItemsOverlay(quoteId){
	// string for comma separated string of quote item ids
	var copyIds = '';
	// any quote item copy checkboxes checked?
	if($j('input[type="checkbox"][name^="qiDelete_"]:checked').length > 0)
	{
		$j('input[type="checkbox"][name^="qiDelete_"]:checked').each(function(i, n)
		{
			// not first iteration?
			if (i > 0)
			{
				copyIds += ',' + n.value;
			}
			else
			{
				copyIds += n.value;
			}
		});
		// create content to be displayed in overlay
		var content = 	'<div id="copyQuotationItemsOverlay">' +
							'<fieldset>' +
								'<ol>' +
									'<li>' +
										'<label>' + i18n.t("discountpercentage", "Discount Percentage : ") + '</label>' +
											'<input type="text" id="discount" />' +
									'</li>' +
									'<li>' +
										'<label>' + i18n.t("calTypeLabel", "Cal Type:") + '</label>' +
										'<select id="ctforcopy">';
												content +=	'<option value="0">' + i18n.t("selectcalType", "Please Select a service Type : ") + '</option>';
											$j.each(caltypeArray, function(y, ct)
											{
												content +=	'<option value="' + ct.calTypeId + '">' + ct.calTypeName + '</option>';
											});
												content +=	'</select>' +
									'</li>' +
									'<li>' +
										'<label>' + i18n.t("price", "Price : ") + '</label>' + 
											'<input type="text" id="price" />' +
									'</li>' +
									'<li>' +
										'<label>&nbsp;</label>' +
										'<input type="button" value="' + i18n.t("core.quotation:modifyItems", "Modify Items") + '" onclick=" this.disabled = true; modifyQuotationItems(\'' + copyIds + '\',\''+quoteId+'\'); return false; "/>' +
									'</li>' +
								'</ol>' +
							'</fieldset>' +
						'</div>';
		// create overlay using content
		loadScript.createOverlay('dynamic', i18n.t("core.quotation:modifyItems", "Modify items"), null, content, null, 35, 580, null);
		
		console.log(copyIds);
		
	}
	else
	{
		// show message to user
		$j.prompt('You have not chosen any quote items to copy');
	}
}

function modifyQuotationItems(copyIds,quoteId){
	
	console.log($j('input#discount').val());
	
	$j.ajax({
		url : "updateQuotationItems.json",
		method : "POST",
		traditional : true,
		dataType : "json",
		data : {
			copyIds : copyIds,
			calTypId : $j('select#ctforcopy').val(),
			price : $j('input#price').val(),
			discount : $j('input#discount').val(),
			quoteId : quoteId
		},
		async : true
	}).done(function(data) {
		if(data.success==true){
			location.reload();
		}
	});
}
