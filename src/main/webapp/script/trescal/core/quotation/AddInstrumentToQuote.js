/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	AddInstrumentToQuote.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	05/11/2007
*					: 	28/10/2015 - TProvost - Manage i18n
*					:	28/10/2015 - TProvost - Minor fix on enable buttons
*					:	29/10/2015 - TProvost - Fix bug on currencySym
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array ( 	'script/trescal/core/utilities/SubdivToContactFilter.js');

/** 
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = 	new Array( { anchor: 'searchresults-link', block: 'searchresults-tab' },
					      	   { anchor: 'searchbasket-link', block: 'searchbasket-tab' },
);

var subMenuElements = 	new Array( 
   	   { anchor: 'jobcosting-link', block: 'jobcosting-tab' },
   	   { anchor: 'invoiceprice-link', block: 'invoiceprice-tab'},
   	   { anchor: 'quotationitem-link', block: 'quotationitem-tab'},
   	   { anchor: 'catalogprice-link', block: 'catalogprice-tab' },
   	   { anchor: 'contractreview-link', block: 'contractreview-tab' },
   	   { anchor: 'manual-link', block: 'manual-tab' },
);

/**
 * Array of autocomplete objects (multiple are possible) containing setup for 
 * jQueryUI initialization.
 * this is used in the init() function.
 */
var jquiAutocomplete = new Array(
				{
					inputFieldName: 'descNm',
					inputFieldId: 'descId',
					source: 'searchdescriptiontags.json'
				},
				{
					inputFieldName: 'mfrNm',
					inputFieldId: 'mfrId',
					source: 'searchmanufacturertags.json'
				});

/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'search\\.barcode';

/**
 * Array of objects containing the element name and corner type which should be applied.
 * Applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * shows a warning if the user is leaving a page which has items
 * in a basket. Only works in IE and Moz 1.7 +.
 */
var showUnLoadWarning = true;

/**
 * perform check before the user leaves the page
 */
window.onbeforeunload = function()
{
	if(parseInt($j('span.basketCount:first').text()) > 0 && showUnLoadWarning)
	{
		showUnLoadWarning = true;
		return i18n.t("warnLoseCurrentBasket", "If you leave this page you will lose the contents of your current basket.");
	}
};

/**
 * indicates the current maximum basket count allowed
 */
var MAXBASKETCOUNT = 50;	

/**
 * this method adds or removes a single instrument to/from the basket
 * 
 * @param {Object} anchor the anchor selected by user
 * @param {Boolean} addToBasket instrument selected to be added to basket?
 * @param {Integer} plantid id of the instrument to be toggled
 * @param {Integer} quoteid id of the quotation we are adding items to
 * @param {String} currencySym current quotation currency symbol 
 */
function toggleInstrumentInBasket(anchor, addToBasket, plantid, quoteid, currencySym, subdivid)
{
	// check basket has not exceeded maximum basket count
	if (parseInt($j('span.basketCount:first').text()) < MAXBASKETCOUNT)
	{	
		// add to basket
		if (addToBasket)
		{
			// create new array
			var instArray = [];
			// check if plant id supplied for single unit to be added to basket?
			if (plantid != null)
			{
				// add instrument plantid
				instArray.push(plantid);
			}
			else if ($j('table.quoteInstResults tbody td.add a.addAnchor').length)
			{
				// loading image and message content
				var content = 	'<div class="text-center padding10">' +
									'<img src="img/icons/ajax-loader.gif" width="31" height="31" alt="" />' +
									'<br /> <h4>' + i18n.t("addingInstrToBasket", "Adding instruments to basket - please wait...") + '</h4>' +
								'</div>';
				// create overlay
				loadScript.createOverlay('dynamic', null, null, content, null, 14, 500, null);
				// find out how many items we can add to basket before maximum reached
				var basketItemsRemaining = MAXBASKETCOUNT - parseInt($j('span.basketCount:first').text());
				// build array of items to be added from results
				$j('table.quoteInstResults tbody td.add a.addAnchor').each(function(i, a)
				{
					// check we can still add to basket
					if (i < basketItemsRemaining)
					{
						// add all eligible instruments to array
						instArray.push(a.id.substring((a.id.indexOf('instAdd') + 7), a.id.length));
					}
				});
			}
			// add instrument to quote basket
			$j.ajax({
				url: 'addinstrumenttoquotebasket.json',
				method: "POST",
				data: {
					instArray: instArray,
					quoteid: quoteid,
					subdivid : subdivid
				},
				async: true
			}).done(function(result) {
				// create new content variable
				var content = '';
				// add result wrappers list to variable
				var wrappers = result.results;
				// add each wrapper to basket
				$j.each(wrappers, function(i, wrap)
				{								
					// get values
					var plantid = wrap.plantid;
					var serial = wrap.serialno;
					var plant = wrap.plantno;
					var source = wrap.source;
					var basketCount = parseInt($j('span.basketCount:first').text());
					// the empty basket message is present so remove it
					if ($j('table.quoteInstBasket tbody:first tr:first td').length == 1)
					{
						// remove message
						$j('table.quoteInstBasket tbody:first').remove();
					}
					// create content for basket
					content += 	// add new tbody for instrument in basket
								'<tbody id="basketitem' + plantid + '">' +
									'<tr>' +
										'<td class="item">' + (basketCount + 1) + '</td>' +
										'<td class="barcode">' + plantid + '<input type="hidden" name="basketIds['+basketCount+']" value="' + plantid + '" /></td>' +
										'<td class="inst">' + wrap.fullInstrumentModelName + '<input type="hidden" name="basketDefInsts['+basketCount+']" value="' + wrap.fullInstrumentModelName + '" /></td>' +
										'<td class="serial">' + serial + '<input type="hidden" name="basketSerials['+basketCount+']" value="' + serial + '" /></td>' +
										'<td class="plant">' + plant + '<input type="hidden" name="basketPlants['+basketCount+']" value="' + plant + '" /></td>' +
										'<td class="heading">' +
											'<select name="basketHeadings['+basketCount+']">';
												$j.each(headings, function(v, qh)
												{
													content += 	'<option value="' + qh.headingId + '"';
																	if (qh.systemDefault)
																	{
																		content +=	' selected="selected"';
																	}
													content +=	'>' + qh.headingName + '</option>';
												});													
								
					content +=	'</select>' +
											'<a href="#" class="copyDownLink" onclick="  copySelectedOption(\'down\', \'select\', $j(this).siblings(\'select\')); return false; " title="' + i18n.t("copySelectionDown", "Copy selection down") + '">&nbsp;</a>' +
											'<a href="#" class="copyUpLink" onclick="  copySelectedOption(\'up\', \'select\', $j(this).siblings(\'select\')); return false; " title="' + i18n.t("copySelectionUp", "Copy selection up") + '">&nbsp;</a>' +
										'</td>' +
										'<td class="source">'+
											'<span id="spanSource' + plantid +'">' + source + '</span>' + 
											'<input type="hidden" id="basketSources'+plantid+'" name="basketSources['+basketCount+']" value="' + source + '" /></br>' +	
											'<a href="#" onclick="switchSourceCost('+plantid+','+quoteid+')">'+i18n.t("core.pricing:purchaseOrder.switch", "Switch")+'</a>'+
										'</td>' +
										'<td class="caltype">' + 
											'<select name="basketCaltypes['+basketCount+']" onchange="reevaluatePrice(' + plantid + ',' + quoteid + ');" id="caltype' + plantid + '">';
												$j.each(caltypes, function(v, ct)
												{
													content += 	'<option value="' + ct.calTypeId + '"';
																	if (ct.calTypeId == wrap.calTypeId)
																	{
																		content +=	' selected="selected"';
																	}
													content +=	'>' + ct.calTypeName + '</option>';
												});													
								
					content +=			'</select>' +
										'</td>' +														
										'<td class="cost" id="price' + plantid + '">' + currencySym +
											'<span id="spanCosts' + plantid +'">' + wrap.finalCost + '</span>'+
										'</td>' +
										'<td class="remove">' +
											'<input type="hidden" name="basketCosts['+basketCount+']" id="basketCosts' + plantid + '" value="' + wrap.finalCost + '" />' +
											'<a href="#" class="deleteAnchor" onclick=" deleteFromBasket(' + plantid + ',' + quoteid + ', ' + '\'' + currencySym + '\'' + ',' + subdivid + '); return false; ">&nbsp;</a>' +
										'</td>' +														
									'</tr>' +
								'</tbody>';			
					// add one to the current basket count in sub menu
					$j('span.basketCount').text(basketCount + 1);
				});
				// append new item row to basket
				$j('table.quoteInstBasket').append(content);
				// anchor supplied for single basket addition?
				if (anchor != null)
				{
					// update icon in results
					$j(anchor).after('<a href="#" class="removeBasketAnchor" id="instAdd' + plantid + '" onclick=" toggleInstrumentInBasket(this, false, ' + plantid + ', ' + quoteid + ', ' + '\'' + currencySym + '\'' + ',' + subdivid + '); return false; " title="' + i18n.t("removeInstrFromBasket", "Remove instrument from basket") + '">&nbsp;</a>').remove();
				}
				else
				{
					// update anchors of all instruments added to basket
					$j('table.quoteInstResults tbody td.add a.addAnchor').each(function(i, a)
					{
						// check we can still add to basket
						if (i < basketItemsRemaining)
						{
							// get the plant id value
							var aplantid = a.id.substring((a.id.indexOf('instAdd') + 7), a.id.length);
							// update icon in results
							$j(a).after('<a href="#" class="removeBasketAnchor" id="instAdd' + aplantid + '" onclick=" toggleInstrumentInBasket(this, false, ' + aplantid + ', ' + quoteid + ', ' + '\'' + currencySym + '\'' + ',' + subdivid + '); return false; " title="' + i18n.t("removeInstrFromBasket", "Remove instrument from basket") + '">&nbsp;</a>').remove();
						}
					});
					// check basket has not exceeded maximum basket count
					if (parseInt($j('span.basketCount:first').text()) >= MAXBASKETCOUNT)
					{
						// warn user their basket is full
						$j.prompt(i18n.t("instrBasketHasReachedMaxCapacity", {varMax : MAXBASKETCOUNT, defaultValue : "Your basket has reached " + MAXBASKETCOUNT + " instruments which is currently the maximum allowed"}));
					}
					// close overlay
					loadScript.closeOverlay();
				}
			});		
		}
		else
		{
			// remove item from basket
			deleteFromBasket(plantid, quoteid, currencySym, subdivid);
		}
	}
	else
	{
		// add to basket
		if (addToBasket)
		{
			// prompt user that basket has reached maximum capacity
			$j.prompt(i18n.t("instrBasketHasReachedMaxCapacity", {varMax : MAXBASKETCOUNT, defaultValue : "Your basket has reached " + MAXBASKETCOUNT + " instruments which is currently the maximum allowed"}));
		}
		else
		{
			// remove item from basket
			deleteFromBasket(plantid, quoteid, currencySym, subdivid);
		}
	}
}


function reevaluatePrice(plantid, quoteid){
	var caltypeid = $j('#caltype' + plantid).val();
	$j.ajax({
	    url:'availablepricesource.json',
	    type:'GET',
	    data: {
	    	plantid: plantid,
			quoteid : quoteid,
			caltypeid : caltypeid
		},
		async: true,
	    }).done(function(wrap) {
       		 
	    	// change hidden price input value 
          	$j('#basketCosts'+plantid)[0].value = wrap.finalCost;
          	// display the price value
          	$j('#spanCosts'+plantid).text(wrap.finalCost);
          	// change hidden source input value 
          	$j('#basketSources'+plantid)[0].value = wrap.source;
          	// display the source value
          	$j('#spanSource'+plantid).text(wrap.source);
       		 
	    });
}

/**
 * this method checks the basket content to make sure at least one instrument has been added before an action is performed
 * 
 * @param {Object} button element pressed to submit data
 * @param {String} submitType type of form submission used to populate hidden input for the correct functionality
 */
function submitBasketContents(button, submitType)
{
	// the empty basket message is present so remove it
	if ($j('table.quoteInstBasket tbody:first tr:first td').length == 1)
	{
		$j.prompt(i18n.t("notAddedAnyInstrToBasket", "You have not added any instruments to your basket"));
	}
	else
	{
		// disable the button
		$j(button).attr('disabled', 'disabled'); 
		// populate submit type hidden input
		$j('input#submitType').val(submitType);
		// submit the form
		$j('input#submitSearch').trigger('click');
	}
}

/**
 * this method deletes either one or all instruments from the basket
 * 
 * @param {Integer} plantid id of the instrument to be removed or null if all should be removed
 * @param {Integer} quoteid id of the quote we are adding instruments to
 */
function deleteFromBasket(plantid, quoteid, currencySym, subdivid)
{
	// plantid null?
	if (plantid != null)
	{
		// remove instrument from basket
		$j('table.quoteInstBasket tbody#basketitem' + plantid).remove();
		// uncheck the instrument in search results
		$j('#instAdd' + plantid).after('<a href="#" class="addAnchor" id="instAdd' + plantid + '" onclick=" toggleInstrumentInBasket(this, true, ' + plantid + ', ' + quoteid + ', ' + '\'' + currencySym + '\'' + ',' + subdivid + '); return false; " title="' + i18n.t("addInstrToBasket", "Add instrument to basket") + '">&nbsp;</a>').remove();
		// subtract one from the current basket count in submenu
		$j('span.basketCount').text(parseInt($j('span.basketCount:first').text()) - 1);
	}
	else
	{
		// update anchors of all instruments removed from basket
		$j('table.quoteInstResults tbody td.add a.removeBasketAnchor').each(function(i, a)
		{			
			// get the plant id value
			var aplantid = a.id.substring((a.id.indexOf('instAdd') + 7), a.id.length);
			// remove all instrument from basket
			$j('table.quoteInstBasket tbody#basketitem' + aplantid).remove();
			// update icon in results
			$j(a).after('<a href="#" class="addAnchor" id="instAdd' + aplantid + '" onclick=" toggleInstrumentInBasket(this, true, ' + aplantid + ', ' + quoteid + ', ' + '\'' + currencySym + '\'' +  ',' + subdivid + '); return false; " title="Add instrument to basket">&nbsp;</a>').remove();
			// subtract one from the current basket count in submenu
			$j('span.basketCount').text(parseInt($j('span.basketCount:first').text()) - 1);
		});		
	}
	// the basket is empty?
	if ($j('table.quoteInstBasket tbody:first tr').length < 1)
	{
		// add message
		$j('table.quoteInstBasket').append(	'<tbody>' +
												'<tr>' +
													'<td colspan="10" class="center bold">' + i18n.t("instrBasketIsEmpty", "Your Instrument Basket Is Empty") + '</td>' +
													'</tr>' +
											'</tbody>');
	}
}

/**
 * this method copies the value selected in the element to any others
 * present on the page in the direction specified.
 *
 * @param {String} direction the direction in which the value should be copied
 * @param {String} elementtype the type of element we are copying to
 * @param {Object} element element which we can obtain value and name for copying
 */
function copySelectedOption(direction, elementtype, element)
{
	// add marker after element
	$j(element).after('<span class="marker"></span>');				
	// variable to indicate the marker has been found
	var markerFound = false;
	// array to hold all inputs
	var inputArray = new Array();
	// copy information down the list
	if (direction == 'down')
	{								
		// create an array of input objects
		inputArray = $j.makeArray($j(elementtype + '[name="' + $j(element).attr('name') + '"]'));					
	}
	else
	{				
		// create an array of input objects
		inputArray = $j.makeArray($j(elementtype + '[name="' + $j(element).attr('name') + '"]'));
		// reverse the array
		inputArray.reverse();				
	}
	
	// loop through all matching inputs
	$j.each(inputArray, function()
	{
		// marker found?
		if ($j(this).next().hasClass('marker'))
		{
			// set variable and remove marker
			markerFound = true;
			$j(this).next().remove();
		}
		// marker found?
		if (markerFound)
		{
			// copy value to input
			$j(this).val($j(element).val());					
		}					
	});
}

/**
 * this method disables certain attributes which are specific to searching a single companies instruments
 * 
 * @param {Object} input the checkbox which has been checked/unchecked
 * @param {Boolean} checked is the checkbox checked?
 */
function searchAllCompanyInstruments(input, checked)
{
	// search all companies has been selected
	if (checked)
	{
		// set correct value in input
		$j(input).val(true);
		// reset subdiv, address and personid select and disable
		$j('select[name="subdivid"] option:first, select[name="addressid"] option:first, select[name="personid"] option:first').attr('selected', 'selected').parent().attr('disabled', 'disabled');
		// remove company specific text
		$j('fieldset#companySearchText legend:first').after('<legend class="attention">' + i18n.t("core.quotation:searchingInstForAllCompanies", "Searching Instruments For All Companies") + '</legend>').addClass('hid');
	}
	else
	{
		// set correct value in input
		$j(input).val(false);
		// enable subdiv, address and personid selects
		$j('select[name="subdivid"], select[name="addressid"], select[name="personid"]').attr('disabled', false);
		// add company specific text
		$j('fieldset#companySearchText legend:first').removeClass('hid').next().remove();
	}
}

function switchSourceCost(plantid, quoteid){
	var cancel = i18n.t("core.jobs:jobItem.cancel", "Cancel");
	var save = i18n.t("save", "Save");
	var caltypeid = $j('#caltype' + plantid).val();
	$j.ajax({
	    url:'availablepricesources.htm',
	    type:'GET',
	    data: {
	    	plantid: plantid,
			quoteid : quoteid,
			caltypeid : caltypeid
		},
		async: true,
	    }).done(function(content) {
	       	//display the popup
	       	$j.prompt(content,{
	       		title: "Viewing Available Price Sources",
	       		buttons: { [save]: true, [cancel]: false },
	       		submit: function (e, val, m, f) {                            
	                   if (val == true){
	                	 //if no prebooking is selected show a message
	                   	 if (!$j('input[name="priceSelected"]').is(':checked')) {
	                   		alert("please select a price!");
	                        }
	                   	 else{
	                   		 var price = $j('input[name="priceSelected"]:checked')[0].value;
	                   		 var source = $j('input[name="priceSelected"]:checked')[0].id;
	                   		 var finalPrice;
	                   		 if(price == 'Manual'){
	                   			finalPrice = $j('#manualPrice')[0].value;
	                   		 } else {
	                   			 finalPrice = price;
	                   		 }
	                   		 if(finalPrice){
	                   			 // change hidden price input value 
	 	                 		$j('#basketCosts'+plantid)[0].value = finalPrice;
	 	                 		// display the price value
	 	                 		$j('#spanCosts'+plantid).text(finalPrice);
	 	                 		 // change hidden source input value 
	 	                 		$j('#basketSources'+plantid)[0].value = source;
	 	                 		// display the source value
	 	                 		$j('#spanSource'+plantid).text(source);
	                   		 }	
	                   	 }
	                   }	
	                   else
	                       console.log('Cancel!');
	               }
	       	});       	
	      }
	);
}