/**
 * array of objects containing the element name and corner type which should be applied.
 * applied in init() function.
 */
var niftyElements = new Array (	{ element: 'div.infobox', corner: 'normal' } );

/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'mfr';

/**
 * Array of autocomplete objects (multiple are possible) containing setup for 
 * jQueryUI initialization.
 * this is used in the init() function.
 */
var jquiAutocomplete = new Array(
				{
					inputFieldName: 'salesCat',
					inputFieldId: 'salesCatId',
					source: 'searchsalescategorytags.json'
				},
				{
					inputFieldName: 'descNm', 
					inputFieldId: 'descId',
					source: 'searchdescriptiontags.json'
				},
				{
					inputFieldName: 'familyNm', 
					inputFieldId: 'familyId',
					source: 'searchfamilytags.json'
				},
				{
					inputFieldName: 'domainNm', 
					inputFieldId: 'domainId',
					source: 'searchdomaintags.json'
				},
				{
					inputFieldName: 'mfrNm', 
					inputFieldId: 'mfrId',
					source: 'searchmanufacturertags.json'
				}
				
				);

