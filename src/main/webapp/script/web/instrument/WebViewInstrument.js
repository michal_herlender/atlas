/****************************************************************************************************************************************
 *                                                CWMS (Calibration Workflow Management System)
 *
 *                                                Copyright 2006, Antech Calibration Services
 *                                                            www.antech.org.uk
 *
 *                                                            All rights reserved
 *
 *                                                        Document author:    Stuart Harrold
 *
 *    FILENAME        :    WebViewInstrument.js
 *    DESCRIPTION        :
 *    DEPENDENCIES    :   -
 *
 *    TO-DO            :    -
 *    KNOWN ISSUES    :    -
 *    HISTORY            :    01/07/2009
 *                    :    01/10/2015 - TProvost - Manage i18n
 *                    :    01/10/2015 - TProvost - Refactoring code : remove functions locales "requestCertificate" and "sendRequest" and imports the library "WebCertSearchResults.js" instead
 *                    :    13/10/2015 - TProvost - Use WebScrapInstrumentUtility.js instead ScrapInstrumentUtility.js
 ****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 * unfortunately datetimepicker is still used by certificate acceptance code
 */
var pageImports = ['../script/thirdparty/jQuery/jquery.boxy.js',
    '../script/web/instrument/WebScrapInstrumentUtility.js',
    '../script/trescal/core/tools/FileBrowserPlugin.js',
    '../script/thirdparty/jQuery/jquery.datetimepicker.full.min.js',
    '../script/web/jobs/certificate/WebCertAcceptance.js'];


var jquiAutocomplete = [{
        inputFieldName: 'desc',
        inputFieldId: 'descid',
        source: 'searchdescriptiontags.json'
    },
    {
        inputFieldName: 'mfr',
        inputFieldId: 'mfrid',
        source: 'searchmanufacturertags.json'
    }];

function init() {


    $j("#searchdialog").dialog({
        autoOpen: false,
        width: 500,
        open: function () {
            // position dialog
            var win = $j(window);
            $j(this).parent().css({
                position: 'absolute',
                left: (win.width() - $j(this).parent().outerWidth()) / 2,
                top: ((win.height() - $j(this).parent().outerHeight()) / 2) + 100
            });
        }
    });

    //dialog box initialization
    $j("#searchresultsdialog").dialog({
        autoOpen: false,
        width: 800,
		height: 500,
        modal: true,
        open: function (e, ui) {

            //Take focus away from close button
            $j(this).siblings(".ui-dialog-titlebar")
                .find("button").blur();

            //insert results table
            createResultsTable();

            // position dialog
            var win = $j(window);
            $j(this).parent().css({
                position: 'absolute',
                left: (win.width() - $j(this).parent().outerWidth()) / 2,
                top: ((win.height() - $j(this).parent().outerHeight()) / 2) + 100
            });
        }
    });

    $j("#trendAnalysis").click(function () {
        var plantid = $j("#plantid").text();
        window.location.href = "instrumentaldrift.htm?plantid=" + plantid;
    });

    // load stylesheet using the dom if not already
    loadScript.loadStyle('../styles/jQuery/jquery-boxy.css', 'screen');
    loadScript.loadStyle('../styles/jQuery/jquery.datetimepicker.css', 'screen');

    $j("#recordUse").click(function () {
        var plantid = $j("#plantid").text();
        recordUse(plantid);
    });

    $j("#linkreplacement").click(function() {
        // clear all input fields to stop the autocomplete id getting messed up
        $j('div#searchdialog').find('input:not(:checkbox)').val('');
        $j("#searchdialog").dialog("open");
	});

    $j("#searchagain").on('click', function() {
        // clear all input fields to stop the autocomplete id getting messed up
        $j('div#searchdialog').find('input:not(:checkbox)').val('');
        $j("#searchresultsdialog").dialog("close");
        $j("#searchdialog").dialog("open");
    });

    $j("#startsearch").click(function () {
        $j("#searchresultsdialog").data('plantId', $j("#barcode").val());
        $j("#searchdialog").dialog("close");
        $j("#searchresultsdialog").dialog("open");
    });

    $j('#searchresultsdialog').on('click', 'tr.selectable', function () {
        addReplacement($j("#plantid").text(), $j(this).find("td:eq(0)").text());
        $j("#searchresultsdialog").dialog("close");
    });

    $j("#unlinkreplacement").click(function () {
        removeReplacement($j("#plantid").text());
    });


    certAcceptanceInitialization();

    $j('#incal').change(function () {
        if ($j(this).is(':checked')) {
            $j(this).attr('value', 'true');
        } else {
            $j(this).attr('value', 'false');
        }
    });
}


/**
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var menuElements = [{anchor: 'jobs-link', block: 'jobs-tab'},
    {anchor: 'certs-link', block: 'certs-tab'},
    {anchor: 'calhistory-link', block: 'calhistory-tab'},
    {anchor: 'history-link', block: 'history-tab'},
    {anchor: 'recall-link', block: 'recall-tab'},
    {anchor: 'validation-link', block: 'validation-tab'},
    {anchor: 'rep-link', block: 'rep-tab'},
    {anchor: 'files-link', block: 'files-tab'},
    {anchor: 'flexiblefields-link', block: 'flexiblefields-tab'},
    {anchor: 'complementary-link', block: 'complementary-tab'}];

//{ anchor: 'image-link', block: 'image-tab' });

/**
 * this method toggles the visibility of actions in the validation table
 *
 * @param {Integer} rowcount the current row number
 * @param {String} linkId the id of the show actions link clicked
 */
function toggleActions(rowcount, linkId) {
    // actions are currently hidden so make them visible
    if ($j('#child' + rowcount).css('display') == 'none') {
        // remove image to show actions and append hide actions image
        $j('#' + linkId).empty().append('<img src="../img/icons/items_hide.png" width="16" height="16" alt="' + i18n.t("hideActions", "Hide Actions") + '" title="' + i18n.t("hideActions", "Hide Actions") + '" class="image_inline" />');
        // make the actions row visible and add slide down effect
        $j('#child' + rowcount).removeClass('hid').addClass('vis').find('div').slideDown(1000);
    }
    // actions are currently visible so hide them
    else {
        // remove image to hide actions and append show actions image
        $j('#' + linkId).empty().append('<img src="../img/icons/items.png" width="16" height="16" alt="' + i18n.t("hideActions", "Hide Actions") + '" title="' + i18n.t("hideActions", "Hide Actions") + '" class="image_inline" />');
        // add effect to slide up actions
        $j('#child' + rowcount + ' div').slideUp(1000, function () {
            // hide row
            $j('#child' + rowcount).removeClass('vis').addClass('hid');
        });
    }
}

function createValidationIssue(plantid) {
    // variable to hold content
    var content = '';
    // load stylesheet using the dom if not already
    loadScript.loadStyle('../styles/jQuery/jquery-boxy.css', 'screen');
    // load javascript for boxy pop up if not already
    loadScript.loadScriptsDynamic(new Array('../script/thirdparty/jQuery/jquery.boxy.js'), false,
        {
            callback: function () {
                // create content to be displayed
                content += '<div id="addValidationIssueBoxy" class="overflow boxy">' +
                    '<div class="hid warningBox1">' +
                    '<div class="warningBox2">' +
                    '<div class="warningBox3">' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<fieldset>' +
                    '<ol>' +
                    '<li>' +
                    '<div>' +
                    i18n.t("web:webViewInstr.validationIssue.createIssueInfo", "If there is an issue with this instrument's information then please supply details in the form below") +
                    '</div>' +
                    '</li>' +
                    '<li>' +
                    '<label for="issue">' + i18n.t("web:webViewInstr.validationIssue.instrIssue", "Instrument Issue:") + '</label>' +
                    '<textarea name="issue" id="issue" cols="60" rows="5"></textarea>' +
                    '</li>' +
                    '<li>' +
                    '<label>&nbsp;</label>' +
                    '<input type="submit" value="' + i18n.t("submit", "Submit") + '" onclick=" this.disabled = true; insertIssue(' + plantid + ', $j(\'textarea#issue\').val()); return false; " />' +
                    '</li>' +
                    '</ol>' +
                    '</fieldset>' +
                    '</div>';
                // create new boxy pop up using content variable
                var addValidationIssueBoxy = new Boxy(content, {
                    title: i18n.t("web:webViewInstr.validationIssue.provideValidationIssue", "Provide Validation Issue"),
                    unloadOnHide: true
                });
                // show boxy pop up and adjust size
                addValidationIssueBoxy.tween(540, 180);
            }
        });
}

/**
 * this method inserts a new validation issues provided by the user into the database for action by a company
 * employee
 *
 * @param {Integer} plantid the id of the instrument the issue has been raised against
 * @param {String} issue the description of the issue the user has
 */
function insertIssue(plantid, issue) {
    // load the service javascript file if not already
    loadScript.loadScriptsDynamic(new Array('dwr/interface/instrumentvalidationissueservice.js'), true,
        {
            callback: function () {
                // call dwr method to insert the instrument validation issue
                instrumentvalidationissueservice.webInsertValidationIssue(plantid, issue,
                    {
                        callback: function (results) {
                            // insertion failed
                            if (results.success != true) {
                                // close jquery boxy thickbox
                                Boxy.get($j('div#addValidationIssueBoxy')).hide();
                                // display error message
                                $j.prompt(results.message);
                            }
                            // insertion was a success
                            else {
                                // assign issue object to variable
                                var issue = results.results;
                                // create content to add to page
                                var content = '<tr>' +
                                    '<td>' + issue.issue + '</td>' +
                                    '<td>' + formatDate(issue.raisedOn, "dd.MM.yyyy") + '</td>' +
                                    '<td>';
                                if (issue.raisedBy != null) {
                                    content += issue.raisedBy.name;
                                }
                                content += '</td>' +
                                    '<td>- ' + i18n.t("web:webViewInstr.insertIssue", "Awaiting Action") + ' -</td>' +
                                    '<td>- ' + i18n.t("web:webViewInstr.insertIssue", "Awaiting Action") + ' -</td>' +
                                    '<td>' + issue.status.name + '</td>' +
                                    '</tr>';
                                // add content to instrument validation issues
                                $j('table.instrumentValidationIssues tbody').prepend(content);
                                // close jquery boxy thickbox
                                Boxy.get($j('div#addValidationIssueBoxy')).hide();
                            }
                        }
                    });
            }
        });
}




/**
 * this method creates a recall rule for this instrument and if applyfromcert is true then
 * updates the next calibration due date.
 *
 * @param {Integer} plantid the id of the instrument
 * @param {Integer} interval the new recall interval in months
 * @param {Date} calibrateOn the date on which calibration should take place
 * @param {Boolean} applyfromcert if true then updates the recall date of the instrument from last certificate
 * @param {Boolean} hasCert indicates if the instrument has a previous certificate
 */
function insertRecallRule(plantid, interval, calibrateOn, applyfromcert, hasCert) {
    // load the service javascript file if not already
    loadScript.loadScriptsDynamic(new Array('dwr/interface/recallruleservice.js'), true,
        {
            callback: function () {
                // call dwr service to insert a new recall rule
                recallruleservice.insert(plantid, interval, calibrateOn, applyfromcert,
                    {
                        callback: function (results) {
                            // insertion of recall rule failed
                            if (results.success != true) {
                                // variable to hold error messages
                                var errors = '';
                                $j.each(results.errorList, function (i) {
                                    // display error messages
                                    var error = results.errorList[i];
                                    errors = '<div>' + i18n.t("fieldError", {
                                        varErrField: error.field,
                                        varErrMsg: error.defaultMessage,
                                        defaultValue: "Field: " + error.field + " failed. Message:" + error.defaultMessage
                                    }) + '</div>';
                                });
                                // show error message in error warning box
                                $j('div#addRecallRuleBoxy div:first').removeClass('hid').addClass('vis').find('.warningBox3').empty().append(errors);
                            }
                            else {
                                // assign object
                                var recall = results.results;
                                // recall rule already applied to this instrument?
                                if ($j('table.instrumentActiveRecallRules tbody tr').length > 1) {
                                    // inactive recall rules table is empty?
                                    if ($j('table.instrumentInactiveRecallRules tbody tr:first td').length < 2) {
                                        // remove the inactive table message
                                        $j('table.instrumentInactiveRecallRules tbody tr:first').remove();
                                    }
                                    // make sure first active recall rule is not a blanket (contact, subdiv or company) recall rule
                                    if ($j('table.instrumentActiveRecallRules tbody tr:first').attr('id') != 'blanketRule') {
                                        // create content for new row in inactive recall rules table using some values from active recall rules table
                                        // and some values from the results of newly inserted recall rule
                                        var inactiveContent = '<tr id="inactive' + $j('table.instrumentActiveRecallRules tbody tr:first td:last input').val() + '">' +
                                            '<td class="interval">' + $j('table.instrumentActiveRecallRules tbody tr:first td:eq(0)').text() + '</td>' +
                                            '<td>' + $j('table.instrumentActiveRecallRules tbody tr:first td:eq(1)').text() + '</td>' +
                                            '<td>' + recall.setBy.name + '</td>' +
                                            '<td>' + cwms.dateFormatFunction(recall.setOn) + '</td>' +
                                            '<td>' +
                                            '<a href="#" onclick=" reactivateRecallRule(' + $j('table.instrumentActiveRecallRules tbody tr:first td:last input').val() + ', ' + plantid + ', false, ' + hasCert + '); return false; " class="mainlink" title="' + i18n.t("web:webViewInstr.recall.reactivateRuleInfo", "Reactivates this recall rule but leaves the current recalld date intact") + '">' + i18n.t("web:webViewInstr.recall.reactivateRule", "Reactivate Rule") + '</a>';
                                        if (hasCert == true) {
                                            inactiveContent = inactiveContent + ' | ' +
                                                '<a href="#" onclick=" reactivateRecallRule(' + $j('table.instrumentActiveRecallRules tbody tr:first td:last input').val() + ', ' + plantid + ', true, ' + hasCert + '); return false;" class="mainlink" title="' + i18n.t("web:webViewInstr.recall.reactivateRuleAndRecalculDateInfo", "Reactivates this recall rule and resets the recall date using the next applicable active recall rule") + '">' + i18n.t("web:webViewInstr.recall.reactivateRuleAndRecalculDate", "Reactivate rule and recalculate cal date") + '</a>';
                                        }
                                        '</td>' +
                                        '</tr>';
                                        // append new inactive recall rule to end of table
                                        $j('table.instrumentInactiveRecallRules tbody').append(inactiveContent);
                                        // update inactive recall count
                                        $j('span#inactiveRecallCount').text(parseInt($j('span#inactiveRecallCount').text()) + 1);
                                    }
                                }
                                // create content for new row in the active recall rules table
                                var activeContent = '<tr>' +
                                    '<td class="interval"><span>' + recall.interval;
                                if (recall.interval == 1) {
                                    activeContent += '</span> ' + i18n.t("month", "month") + ' ';
                                }
                                else {
                                    activeContent += ' ' + i18n.t("months", "months") + ' ';
                                }
                                activeContent += i18n.t("web:webViewInstr.recall.followingCalibration", "following calibration") + '</td>' +
                                    '<td>';
                                if (recall.instrument) {
                                    activeContent += i18n.t("web:webViewInstr.recall.coverThisInstrument", "Covers just this instrument");
                                }
                                else {
                                    activeContent += i18n.t("web:webViewInstr.recall.coverAllInstruments", "Covering all instruments belonging to ");
                                    if (recall.con) {
                                        activeContent += '(' + i18n.t("contact", "Contact") + ')' + '$rule.con.name';
                                    }
                                    else if (recall.sub) {
                                        activeContent += '(' + i18n.t("subdivision", "Subdivision") + ')' + '$rule.sub.subname';
                                    }
                                    else if (recall.comp) {
                                        activeContent += '(' + i18n.t("company", "Company") + ')' + '$rule.comp.coname';
                                    }
                                }
                                activeContent += '</td>' +
                                    '<td>' + recall.setBy.name + '</td>' +
                                    '<td>' + cwms.dateFormatFunction(recall.setOn) + '</td>' +
                                    '<td>' +
                                    '<input type="hidden" name="activeRuleId" value="' + recall.id + '" />' +
                                    '<a href="#" onclick=" removeRecallRule(' + recall.id + ', ' + plantid + ', false, ' + hasCert + '); return false; " class="mainlink" title="' + i18n.t("web:webViewInstr.recall.removeRuleAndResetDateInfo", "Deactivates this recall rule but leaves the current recall date intact") + '">' + i18n.t("web:webViewInstr.recall.removeRule", "Remove rule") + '</a>';
                                if (hasCert == true) {
                                    activeContent += ' | ' +
                                        '<a href="#" onclick=" removeRecallRule(' + recall.id + ', ' + plantid + ', true, ' + hasCert + '); return false; " class="mainlink" title="' + i18n.t("web:webViewInstr.recall.removeRuleAndResetDate", "Deactivates this recall rule and resets the recall date using the next applicable active recall rule") + '">' + i18n.t("web:webViewInstr.recall.removeRule", "Remove rule and reset cal date") + '</a>';
                                }
                                activeContent += '</td>' +
                                    '</tr>';
                                // recall rule already applied to this instrument?
                                if (($j('table.instrumentActiveRecallRules tbody tr').length > 1) && ($j('table.instrumentActiveRecallRules tbody tr:first').attr('id') != 'blanketRule')) {
                                    // append new active recall rule to table and remove the old recall rule
                                    $j('table.instrumentActiveRecallRules tbody tr:first').before(activeContent).remove();
                                }
                                else {
                                    // append new active recall rule to table
                                    $j('table.instrumentActiveRecallRules tbody tr:first').before(activeContent);
                                    // update active recall count
                                    $j('span#activeRecallCount').text(parseInt($j('span#activeRecallCount').text()) + 1);
                                }
                                // variable for next cal due date span
                                var spanStyle = '';
                                // instrument out of cal
                                if (recall.instrument.outOfCalibration) {
                                    spanStyle = ' class="attention" ';
                                }
                                // add the new calibration due date for instrument to page
                                $j('span.nextCalDueDate').html('<span ' + spanStyle + '>' + recall.instrument.formattedNextCalDueDate + '</span>');
                                // add the new calibration recall rule interval to page
                                $j('span.recallRuleInterval').text(recall.interval);
                                // add the new recall date to page using method @see getRecallPeriod() and value of next calibration due date
                                $j('span#nextCalRecall').text(getRecallPeriod(recall.instrument.nextCalDueDate));
                                // variable for cal timescale icon content
                                var calTimescalePercentage = '';
                                if ((recall.instrument.inCalTimescalePercentage == 100) && (recall.instrument.finalMonthOfCalTimescalePercentage == 100)) {
                                    calTimescalePercentage += '<div class="outOfCalibration"></div>';
                                }
                                else {
                                    calTimescalePercentage += '<div class="inCalibration">' +
                                        '<div class="inCalibrationLevel" style=" width: ' + recall.instrument.inCalTimescalePercentage + '%; "></div>' +
                                        '</div>' +
                                        '<div class="finalMonthOfCalibration">' +
                                        '<div class="finalMonthOfCalibrationLevel" style=" width: ' + recall.instrument.finalMonthOfCalTimescalePercentage + '%; "></div>' +
                                        '</div>';
                                }
                                // populate calibration timescale percentage icon content
                                $j('div.calIndicatorBox').html(calTimescalePercentage);
                                // close the jquery boxy thickbox
                                Boxy.get($j('div#addRecallRuleBoxy')).hide();
                            }
                        }
                    });
            }
        });
}

/**
 * this method calculates and returns a string representation of the next recall due
 * date based on the given date. Subtracts a month from the given date.
 *
 * @param {Object} calDueDate the next calibration due date of instrument
 */
function getRecallPeriod(calDueDate) {
    // calibration due date?
    if (calDueDate != null) {
        try {
            // create new date
            var cal = new Date();
            // set date to calibration due date
            cal.setTime(calDueDate);
            // add one to month as javascript months run from (0 - 11), then subtract one month for recall
            cal.setMonth((cal.getMonth() + 1) - 1);
            // if recall month is zero? previous year?
            if (cal.getMonth() == 0) {
                // set year to previous
                cal.setFullYear(cal.getFullYear() + -1);
            }
            // return month in words and full year
            return `${new Intl.DateTimeFormat(i18locale,{month:"long"}).format(cal)} ${cal.getFullYear()}`;
        }
        catch (e) {
            // return message
            return i18n.t("unableGetRecallDate", "Unable to get recall date");
        }
    }
    else {
        // return message
        return i18n.t("unableCalculateDueDate", "Unable to calculate due date");
    }
}

/**
 * this method deactivates the rule identified by the given id and if resetToNext is true then re-applies
 * the next applicable recall rule to the instrument and recalculates the recall due date.
 *
 * @param {Integer} ruleid the id of the rule to deactivate
 * @param {Integer} plantid the barcode of the current instrument
 * @param {Boolean} resetNextCal true will update the next cal due date of the instrument
 * @param {Boolean} hasCert indicates if the instrument has a previous certificate
 */
function removeRecallRule(ruleid, plantid, resetNextCal, hasCert) {
    // load the service javascript file if not already
    loadScript.loadScriptsDynamic(new Array('dwr/interface/recallruleservice.js'), true,
        {
            callback: function () {
                // call dwr service to deactivate recall rule
                recallruleservice.deactivateRecallRule(ruleid, plantid, resetNextCal,
                    {
                        callback: function (result) {
                            if (!result.success) {
                                // show error message
                                $j.prompt(result.message);
                            }
                            else {
                                // assign object
                                var inactrecall = result.results.inactiveRecallRule;
                                // create content for new row in inactive recall rules table using some values from active recall rules table
                                // and some values from the results of newly inserted recall rule
                                var inactiveContent = '<tr id="inactive' + ruleid + '">' +
                                    '<td class="interval">' + $j('table.instrumentActiveRecallRules tbody tr:first td:eq(0)').text() + '</td>' +
                                    '<td>' + $j('table.instrumentActiveRecallRules tbody tr:first td:eq(1)').text() + '</td>' +
                                    '<td>' + inactrecall.deactivatedBy.name + '</td>' +
                                    '<td>' + inactrecall.formattedDeactivatedOn + '</td>' +
                                    '<td>' +
                                    '<a href="#" onclick=" reactivateRecallRule(' + ruleid + ', ' + plantid + ', false, ' + hasCert + '); return false; " class="mainlink" title="' + i18n.t("web:webViewInstr.recall.reactivateRuleInfo", "Reactivates this recall rule but leaves the current recalld date intact") + '">' + i18n.t("web:webViewInstr.recall.reactivateRule", "Reactivate rule") + '</a>';
                                if (hasCert == true) {
                                    inactiveContent = inactiveContent + ' | ' +
                                        '<a href="#" onclick=" reactivateRecallRule(' + ruleid + ', ' + plantid + ', true, ' + hasCert + '); return false;" class="mainlink" title="' + i18n.t("web:webViewInstr.recall.reactivateRuleAndRecalculDateInfo", "Reactivates this recall rule and resets the recall date using the next applicable active recall rule") + '">' + i18n.t("web:webViewInstr.recall.reactivateRuleAndRecalculDate", "Reactivate rule and recalculate cal date") + '</a>';
                                }
                                '</td>' +
                                '</tr>';
                                // inactive recall rules table is empty?
                                if ($j('table.instrumentInactiveRecallRules tbody tr:first td').length < 2) {
                                    // remove the inactive table message
                                    $j('table.instrumentInactiveRecallRules tbody tr:first').remove();
                                }
                                // append new inactive recall rule to end of table
                                $j('table.instrumentInactiveRecallRules tbody').append(inactiveContent);
                                // remove the old recall rule
                                $j('table.instrumentActiveRecallRules tbody tr:first').remove();
                                // update inactive recall count
                                $j('span#inactiveRecallCount').text(parseInt($j('span#inactiveRecallCount').text()) + 1);
                                // update active recall count
                                $j('span#activeRecallCount').text(parseInt($j('span#activeRecallCount').text()) - 1);
                                // add the new calibration recall rule interval to page
                                $j('span.recallRuleInterval').text($j('table.instrumentActiveRecallRules tbody tr:first td:eq(0) span').text());
                                // calibration due date reset?
                                if (resetNextCal == true) {
                                    // variables used when updating the calibration timescale icon
                                    // values retrieved from instrument and/or recall rule
                                    var interval = 0;
                                    var instrument = null;
                                    // assign active recall rules to variable
                                    var actrecalls = result.results.activeRecallRule;
                                    // assign instrument object to variable
                                    var inst = result.results.instrument;
                                    // is there active recall rules remaining?
                                    if (actrecalls.length > 0) {
                                        // assign new recall interval from first active recall returned
                                        interval = actrecalls[0].interval;
                                        // check it is not null
                                        if (inst != null) {
                                            // assign new instrument
                                            instrument = inst;
                                        }
                                    }
                                    else {
                                        // check it is not null
                                        if (inst != null) {
                                            // no active recall rules remaining. Revert to instrument defaults
                                            // assign new recall interval from instrument default
                                            interval = inst.calFrequency;
                                            // assign new instrument
                                            instrument = inst;
                                        }
                                    }
                                    // variable for next cal due date span
                                    var spanStyle = '';
                                    // instrument out of cal
                                    if (instrument.outOfCalibration) {
                                        spanStyle = ' class="attention" ';
                                    }
                                    // add the new calibration due date for instrument to page
                                    $j('span.nextCalDueDate').html('<span ' + spanStyle + '>' + instrument.formattedNextCalDueDate + '</span>');
                                    // add the new calibration recall rule interval to page
                                    $j('span.recallRuleInterval').text(interval);
                                    // add the new recall date to page using method @see getRecallPeriod() and value of next calibration due date
                                    $j('span#nextCalRecall').text(getRecallPeriod(instrument.nextCalDueDate));
                                    // variable for cal timescale icon content
                                    var calTimescalePercentage = '';
                                    if ((instrument.inCalTimescalePercentage == 100) && (instrument.finalMonthOfCalTimescalePercentage == 100)) {
                                        calTimescalePercentage += '<div class="outOfCalibration"></div>';
                                    }
                                    else {
                                        calTimescalePercentage += '<div class="inCalibration">' +
                                            '<div class="inCalibrationLevel" style=" width: ' + instrument.inCalTimescalePercentage + '%; "></div>' +
                                            '</div>' +
                                            '<div class="finalMonthOfCalibration">' +
                                            '<div class="finalMonthOfCalibrationLevel" style=" width: ' + instrument.finalMonthOfCalTimescalePercentage + '%; "></div>' +
                                            '</div>';
                                    }
                                    // populate calibration timescale percentage icon content
                                    $j('div.calIndicatorBox').html(calTimescalePercentage);
                                }
                            }
                        }
                    });
            }
        });
}

/**
 * this method reactivates the rule identified by the given id and if resetToNext is true then reapplies
 * the next applicable recall rule to the instrument and recalculates the recall due date.
 *
 * @param {Integer} ruleid the id of the rule to deactivate
 * @param {Integer} plantid the barcode of the current instrument
 * @param {Boolean} resetNextCal true will update the next cal due date of the instrument
 * @param {Boolean} hasCert indicates if the instrument has a previous certificate
 */
function reactivateRecallRule(ruleid, plantid, resetNextCal, hasCert) {
    // load the service javascript file if not already
    loadScript.loadScriptsDynamic(new Array('dwr/interface/recallruleservice.js'), true,
        {
            callback: function () {
                // call dwr service to active recall rule
                recallruleservice.reactivateRecallRule(ruleid, plantid, resetNextCal,
                    {
                        callback: function (results) {
                            if (!results.success) {
                                $j.prompt(results.message);
                            }
                            else {
                                // assign object
                                var recall = results.results;
                                // recall rule already applied to this instrument?
                                if ($j('table.instrumentActiveRecallRules tbody tr').length > 1) {
                                    // make sure first active recall rule is not a blanket (contact, subdiv or company) recall rule
                                    if ($j('table.instrumentActiveRecallRules tbody tr:first').attr('id') != 'blanketRule') {
                                        // create content for new row in inactive recall rules table using some values from active recall rules table
                                        // and some values from the results of newly inserted recall rule
                                        var inactiveContent = '<tr id="inactive' + $j('table.instrumentActiveRecallRules tbody tr:first td:last input').val() + '">' +
                                            '<td class="interval">' + $j('table.instrumentActiveRecallRules tbody tr:first td:eq(0)').text() + '</td>' +
                                            '<td>' + $j('table.instrumentActiveRecallRules tbody tr:first td:eq(1)').text() + '</td>' +
                                            '<td>' + recall.setBy.name + '</td>' +
                                            '<td>' + cwms.dateFormatFunction(recall.setOn) + '</td>' +
                                            '<td>' +
                                            '<a href="#" onclick=" reactivateRecallRule(' + $j('table.instrumentActiveRecallRules tbody tr:first td:last input').val() + ', ' + recall.instrument.plantid + ', false, ' + hasCert + '); return false; " class="mainlink" title="' + i18n.t("web:webViewInstr.recall.reactivateRuleInfo", "Reactivates this recall rule but leaves the current recalld date intact") + '">' + i18n.t("web:webViewInstr.recall.reactivateRule", "Reactivate rule") + '</a>';
                                        if (hasCert == true) {
                                            inactiveContent = inactiveContent + ' | ' +
                                                '<a href="#" onclick=" reactivateRecallRule(' + $j('table.instrumentActiveRecallRules tbody tr:first td:last input').val() + ', ' + recall.instrument.plantid + ', true, ' + hasCert + '); return false;" class="mainlink" title="' + i18n.t("web:webViewInstr.recall.reactivateRuleAndRecalculDateInfo", "Reactivates this recall rule and resets the recall date using the next applicable active recall rule") + '">' + i18n.t("web:webViewInstr.recall.reactivateRuleAndRecalculDate", "Reactivate rule and recalculate cal date") + '</a>';
                                        }
                                        '</td>' +
                                        '</tr>';
                                        // append new inactive recall rule to end of table
                                        $j('table.instrumentInactiveRecallRules tbody').append(inactiveContent);
                                        // update active recall count
                                        $j('span#inactiveRecallCount').text(parseInt($j('span#inactiveRecallCount').text()) + 1);
                                    }
                                }
                                // create content for new row in the active recall rules table
                                var activeContent = '<tr>' +
                                    '<td class="interval">' + $j('tr#inactive' + ruleid).find('td:eq(0)').text() + '</td>' +
                                    '<td>' + $j('tr#inactive' + ruleid).find('td:eq(1)').text() + '</td>' +
                                    '<td>' + recall.setBy.name + '</td>' +
                                    '<td>' + cwms.dateFormatFunction(recall.setOn) + '</td>' +
                                    '<td>' +
                                    '<input type="hidden" name="activeRuleId" value="' + recall.id + '" />' +
                                    '<a href="#" onclick=" removeRecallRule(' + recall.id + ', ' + plantid + ', false, ' + hasCert + '); return false; " class="mainlink" title="' + i18n.t("web:webViewInstr.recall.removeRuleInfo", "Deactivates this recall rule but leaves the current recall date intact") + '">' + i18n.t("web:webViewInstr.recall.removeRule", "Remove rule") + '</a>';
                                if (hasCert == true) {
                                    activeContent = activeContent + ' | ' +
                                        '<a href="#" onclick=" removeRecallRule(' + recall.id + ', ' + plantid + ', true, ' + hasCert + '); return false; " class="mainlink" title="' + i18n.t("web:webViewInstr.recall.reactivateRuleAndRecalculDateInfo", "Deactivates this recall rule and resets the recall date using the next applicable active recall rule") + '">' + i18n.t("web:webViewInstr.recall.removeRuleAndResetDate", "Remove rule and reset cal date") + '</a>';
                                }
                                activeContent += '</td>' +
                                    '</tr>';
                                // recall rule already applied to this instrument?
                                if (($j('table.instrumentActiveRecallRules tbody tr').length > 1) && ($j('table.instrumentActiveRecallRules tbody tr:first').attr('id') != 'blanketRule')) {
                                    // append new active recall rule to table and remove the old recall rule
                                    $j('table.instrumentActiveRecallRules tbody tr:first').before(activeContent).remove();
                                }
                                else {
                                    // append new active recall rule to table
                                    $j('table.instrumentActiveRecallRules tbody tr:first').before(activeContent);
                                    // update active recall count
                                    $j('span#activeRecallCount').text(parseInt($j('span#activeRecallCount').text()) + 1);
                                }
                                // add the new calibration recall rule interval to page
                                $j('span.recallRuleInterval').text(recall.interval);
                                // inactive recall rules table is empty?
                                if (parseInt($j('span#inactiveRecallCount').text()) < 1) {
                                    // add inactive table message
                                    $j('table.instrumentInactiveRecallRules tbody').append('<tr>' +
                                        '<td colspan="5" class="bold center">' + i18n.t("web:webViewInstr.recall.noInactiveRecallRules", "There are no inactive recall rules for this instrument") + '</td>' +
                                        '</tr>');
                                }
                                // calibration due date reset?
                                if (resetNextCal == true) {
                                    // variable for next cal due date span
                                    var spanStyle = '';
                                    // instrument out of cal
                                    if (recall.instrument.outOfCalibration) {
                                        spanStyle = ' class="attention" ';
                                    }
                                    // add the new calibration due date for instrument to page
                                    $j('span.nextCalDueDate').html('<span ' + spanStyle + '>' + recall.instrument.formattedNextCalDueDate + '</span>');
                                    // add the new recall date to page using method @see getRecallPeriod() and value of next calibration due date
                                    $j('span#nextCalRecall').text(getRecallPeriod(recall.instrument.nextCalDueDate));
                                }
                                // variable for cal timescale icon content
                                var calTimescalePercentage = '';
                                if ((recall.instrument.inCalTimescalePercentage == 100) && (recall.instrument.finalMonthOfCalTimescalePercentage == 100)) {
                                    calTimescalePercentage += '<div class="outOfCalibration"></div>';
                                }
                                else {
                                    calTimescalePercentage += '<div class="inCalibration">' +
                                        '<div class="inCalibrationLevel" style=" width: ' + recall.instrument.inCalTimescalePercentage + '%; "></div>' +
                                        '</div>' +
                                        '<div class="finalMonthOfCalibration">' +
                                        '<div class="finalMonthOfCalibrationLevel" style=" width: ' + recall.instrument.finalMonthOfCalTimescalePercentage + '%; "></div>' +
                                        '</div>';
                                }
                                // populate calibration timescale percentage icon content
                                $j('div.calIndicatorBox').html(calTimescalePercentage);
                                // remove inactive row from table
                                $j('tr#inactive' + ruleid).remove();
                                // update active recall count
                                $j('span#inactiveRecallCount').text(parseInt($j('span#inactiveRecallCount').text()) - 1);
                            }
                        }
                    });
            }
        });
}

/**
 * this method creates the flexible field content to be displayed in a jquery boxy thickbox
 *
 * @param {String} flexiblefieldid id of the flexible field
 * @param {String} target the old id of the control
 * @param {Object} anchor the anchor element clicked on the page
 * @param {Object} currentRange the div element of the current range
 * @param {Object} rangeContainer the div element which contains all ranges
 */
function editFlexibleField(flexiblefieldid, target, anchor, currentRange, rangeContainer) {
    // variables for edit information
    var flexid = '';
    var defid = '';
    var plantid = '';
    var ftype = '';
    var name = '';
    var value = '';
    var libraryid = ''
    var libraries = '';

    // set global variables for use in other methods
    currentRangeGlobal = currentRange;
    rangeContainerGlobal = rangeContainer;
    anchorGlobal = anchor;

    if (currentRange != null) {
        flexid = $j(currentRange).find('#rid' + flexiblefieldid).val();
        defid = $j(currentRange).find('#rdefid' + flexiblefieldid).val();
        plantid = $j(currentRange).find('#rplantid' + flexiblefieldid).val();
        ftype = $j(currentRange).find('#rtype' + flexiblefieldid).val();
        name = $j(currentRange).find('#rname' + flexiblefieldid).val();
        value = $j(currentRange).find('#rvalue' + flexiblefieldid).val();
        libraryid = $j(currentRange).find('#rLibraryID' + flexiblefieldid).val();
        libraries = $j(currentRange).find('#rLibraries' + flexiblefieldid).val();
    }

    // create content
    var content = '<div id="editInstrumentFieldBoxy" class="overflow">' +
        '<div class="warningBox1 hid">' +
        '<div class="warningBox2">' +
        '<div class="warningBox3">' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<fieldset>' +
        '<legend>Edit Instrument Field</legend>' +
        '<ol>' +
        '<li>' +
        '<label>' + name + '</label>' +
        '<input type="hidden" name="id" value="' + flexid + '" />';

    switch (ftype) {
        case 'BIT': {
            if (value == 'true') {
                content += '<input type="checkbox" name="chkvalue" tabindex="100" checked="' + value + '" />' + '</li>';
            }
            else {
                content += '<input type="checkbox" name="chkvalue" tabindex="100" />' + '</li>';
            }
            break;
        }
        case 'DATETIME': {
            content += '<input type="text" id="datvalue" name="datvalue" value="' + value + '" class="date" readonly="readonly" tabindex="100" />'

            break;
        }
        case 'SELECTION': {
            content += '<select id="selvalue" name="selvalue">';
            if (libraries != undefined) {
                var json = JSON.parse(libraries);
                json.libraries.forEach(function addOption(value) {
                    if (libraryid == value.id) {
                        content += '<option value="' + value.id + '" selected="selected">' + value.name + '</option>';
                    }
                    else {
                        content += '<option value="' + value.id + '">' + value.name + '</option>';
                    }
                });
            }
            content += '</select>';
            break;
        }
        default: {
            content += '<input type="text" name="txtvalue" value="' + (value == '${cf.value}' ? '' : value) + '" tabindex="100" />' + '</li>';
            break;
        }
    }

    content += '<li>' +
        '<label>&nbsp;</label>';

    content += '<input type="button" value="' + i18n.t("core.instrument:updateflexiblefield", "Save") + '" onclick=" submitFlexibleField(\'' + ftype + '\', ' + flexid + ',\'' + target + '\', ' + defid + ', ' + plantid + '); return false; " tabindex="103" />';

    content += '</li>' +
        '</ol>' +
        '</fieldset>' +
        '</div>';

    // create new boxy pop up using content variable
    var editInstrumentFieldBoxy = new Boxy(content, {
        title: i18n.t("core.instrument:editflexiblefield", "Edit Instrument Field"),
        modal: true,
        unloadOnHide: true
    });
    // show boxy pop up and adjust size
    editInstrumentFieldBoxy.tween(480, 220);

    var jqFlexCalendar = new Array(
        {
            inputField: 'datvalue',
            dateFormat: 'dd.mm.yy',
            displayDates: 'all',
            showWeekends: true
        });

    // load stylesheet using the dom
    loadScript.loadStyle('../styles/jQuery/jquery-ui.css', 'screen');
    // load the jquery calendar javascript file
    loadScript.loadScriptsDynamic(new Array('../script/thirdparty/jQuery/jquery.ui.js'), true,
        {
            callback: function () {
                $j.each(jqFlexCalendar, function (i) {
                    // create jquery calendars
                    loadScript.createJQCal(jqFlexCalendar[i]);
                });
            }
        });
}

/**
 * this method takes the information from the form in the thickbox to update
 * a flexible field
 * @param {String} ftype field type (VARCHAR, BIT, FLOAT...)
 * @param {String} id of the flexible field to edit
 * @param {String} target the old id of the control
 */
function submitFlexibleField(ftype, flexid, target, defid, plantid) {
    // get values from thickbox
    var value = '';
    var valueToDisplay = '';

    switch (ftype) {
        case 'BIT': {
            if ($j('div#editInstrumentFieldBoxy input[name="chkvalue"]').prop('checked')) {
                value = '1';
            }
            else {
                value = '0';
            }
            break;
        }
        case 'DATETIME': {
            value = $j('div#editInstrumentFieldBoxy input[name="datvalue"]').val();
            break;
        }
        case 'SELECTION': {
            value = $j('div#editInstrumentFieldBoxy select[name="selvalue"]').val();
            valueToDisplay = $j('div#editInstrumentFieldBoxy select[name="selvalue"] option:selected').text();
            break;
        }
        default: {
            value = $j('div#editInstrumentFieldBoxy input[name="txtvalue"]').val();
            break;
        }
    }

    // load the service javascript file if not already
    loadScript.loadScriptsDynamic(new Array('dwr/interface/instrumentfieldvalueservice.js'), true,
        {
            callback: function () {
                // call dwr service to update felxible field
                instrumentfieldvalueservice.editFlexibleField(flexid, ftype, value, defid, plantid,
                    {
                        callback: function (result) {
                            // was update successful?
                            if (result.success) {
                                // get boxy instance
                                var editInstrumentFieldBoxy = Boxy.get($j('div#editInstrumentFieldBoxy')).tween(480, 80);

                                // wait and then close thickbox
                                setTimeout('Boxy.get($j(\'div#editInstrumentFieldBoxy\')).hide()', 1200);

                                switch (ftype) {
                                    case 'BIT': {
                                        if (value == '1') {
                                            $j('#' + target + defid).attr('checked', true);
                                        }
                                        else {
                                            $j('#' + target + defid).removeAttr('checked');
                                        }
                                        break;
                                    }
                                    case 'SELECTION': {
                                        $j('#' + target + defid).text(valueToDisplay);
                                        break;
                                    }
                                    default: {
                                        $j('#' + target + defid).text(value);
                                        break;
                                    }
                                }

                                $j('#rvalue' + defid).val(value);
                            }
                            else {
                                // display error message to user
                                $j('div#editInstrumentFieldBoxy .warningBox1').removeClass('hid')
                                    .find('.warningBox3')
                                    .empty()
                                    .append('<div class="text-center attention">' + result.message + '</div>');
                            }
                        }
                    });
            }
        });
}



function recordUse(plantid) {

    var request = $j.ajax({
        url: "recordinstrmentuse.json/" + plantid,
        method: "PUT",
        dataType: "json"
    });

    request.done(function (responseDTO) {
        if (responseDTO.success) {
            $j("#numberOfUses").text(responseDTO.numberOfUsesSinceCal);
            if (responseDTO.numberOfUsesSinceCal >= responseDTO.permittedNumberOfUses) {
                $j("#recordUseTr").hide();
                $j("#usageSpan").removeClass().addClass("usagesbad");
            } else if (responseDTO.numberOfUsesSinceCal === (responseDTO.permittedNumberOfUses - 1)) {
                $j("#usageSpan").removeClass().addClass("usagesdodgy");
            }
        } else {
            alert(responseDTO.message);
        }
    });

    request.fail(function (jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });


}

function createResultsTable() {

    //remove any previous table that might be in the dialog
    $j("#searchResultsTable").remove();
    $j("#searchagaindiv").hide();
    $j("#loaderDiv").show();

    var data = {};
    data["plantId"] = $j("#searchresultsdialog").data("plantId");
    data["serialNo"] = $j("#serialno").val();
    data["plantNo"] = $j("#plantno").val();
    data["mfrId"] = $j("#mfrid").val();
    data["descId"] = $j("#descid").val();
    data["inCal"] = $j("#incal").val();
    data["modelText"] = $j("#model").val();

    //send ajax request
    var request = $j.ajax({
        type: "POST",
        contentType: "application/json",
        url: "replacementsearch.json",
        data: JSON.stringify(data),
        dataType: 'json'
    });

    request.done(function (data) {

        $j("#loaderDiv").hide();
        var searchresultstable = $j('<table></table>').attr({id: "searchResultsTable"});
        searchresultstable.addClass("default");

        //Add table header
        $j('<thead><tr><th>' +
            i18n.t("barcode", "Barcode") +
            '</th><th>' +
            i18n.t("mfr", "Instrument Model Name") +
            '</th><th>' +
            i18n.t("serialNo", "Serial No") +
            '</th><th>' +
            i18n.t("plantNo", "Plant No") +
            '</th><th>' +
            i18n.t("dueDate", "Due Date") +
            '</th></tr></thead>').appendTo(searchresultstable);
        //add table body
        var tablebody = $j('<tbody></tbody>').appendTo(searchresultstable);

        if(data.length == 0){
            row = $j('<tr></tr>').appendTo(tablebody);
            $j('<td colspan="5">' + i18n.t("noresults", "No results found") + '</td>').appendTo(row);
        } else{
            $j.each(data, function(k, v) {
                row = $j('<tr></tr>').appendTo(tablebody);
                row.addClass("selectable");
                $j('<td>' + v.plantId + '</td>').appendTo(row);
                $j('<td>' + v.modelName + '</td>').appendTo(row);
                $j('<td>' + v.serialNo + '</td>').appendTo(row);
                $j('<td>' + v.plantNo + '</td>').appendTo(row);
                $j('<td>' + (v.nextCalDate ? v.nextCalDate : '') + '</td>').appendTo(row);
            });
        }

        // add the table to the dialog
        searchresultstable.appendTo("#searchresultsdialog");
        $j("#searchagaindiv").show();
    });

    request.fail(function (jqXHR, textStatus) {
        alert( "Search request failed: " + textStatus );
    });

}

function addReplacement(plantId, replacementId) {
    var data = {};
    data["replacementId"] = replacementId;

    var request = $j.ajax({
        type: "PUT",
        contentType: "application/json",
        url: "replacementinstrument.json/" + plantId,
        data: JSON.stringify(data),
        dataType: 'json'
    });

    request.done(function (data) {
        var newRow = '<tr id="replacementInstrument">' +
                        '<td><a href="viewinstrument.htm?plantid=' + data.barcode + '"class="mainlink">' + data.barcode+ '</a></td>' +
            '<td>' + data.instrumentName + '</td>' +
            '<td>' + data.replacedBy + '</td>' +
            '<td>' + data.replacedOn + '</td>' +
            '</tr>';

        $j('#replacementInstrument').replaceWith(newRow);
        $j('#linkreplacement').hide();
        $j('#unlinkreplacement').show();
    });

    request.fail(function (jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
}

function removeReplacement(plantId) {
    var request = $j.ajax({
        type: "DELETE",
        contentType: "application/json",
        url: "replacementinstrument.json/" + plantId
    });

    request.done(function () {
        var newRow = '<tr id="replacementInstrument">' +
            '<td colspan="4">' +
            i18n.t("web:webViewInstr.replacement.none", "This instrument has not been replaced") +
            '</td>' +
            '</tr>';
        $j('#replacementInstrument').replaceWith(newRow);
        $j('#unlinkreplacement').hide();
        $j('#linkreplacement').show();
    });

    request.fail(function (jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
}



