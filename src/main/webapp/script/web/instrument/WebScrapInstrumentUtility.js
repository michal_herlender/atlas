/****************************************************************************************************************************************
*	FILENAME		:	WebScrapInstrumentUtility.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	13/10/2015 - TProvost - Created File since ScrapInstrumentUtility.js
*					:	13/10/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * this method confirms with the user that they do wish to scrap the instrument (set to status B.E.R.)
 * 
 * @param {Boolean} scrap indicates if we want to scrap or unscrap the instrument
 * @param {Integer} plantid the id of the instrument to be scrapped (set to status B.E.R.)
 * @param {Integer} jiid job item from which the instrument has been scrapped or null if done from instrument page
 */
function confirmScrapInstrumentChange(scrap, plantid, jiid)
{
	// create variable for message text
	var messagetext = '';

	// scrapping?
	if (scrap)
	{
		messagetext += 	i18n.t("web:webScrapInstrUtility.confirmScrapInstr", "Are you sure that you wish to scrap this instrument?") + '<br />' +
						i18n.t("web:webScrapInstrUtility.instrSetToBER", "This instrument will be set to 'B.E.R.' and will not appear on any future recalls?");
	}
	else
	{
		messagetext += 	i18n.t("web:webScrapInstrUtility.confirmUnscrapInstr", "Are you sure that you wish to return this instrument to service?") + '<br />' +
						i18n.t("web:webScrapInstrUtility.instrSetToCirculation", "This instrument will be set to 'In circulation' and will start to appear on future recalls?");
	}


	// initial prompt for user
	$j.prompt(messagetext,
	{ 
		submit: confirmcallback,
		buttons: 
		{ 
			Ok: true,
			Cancel: false 
		}, 
		focus: 1 
	});
	// callback method for initial prompt
	function confirmcallback(v,m)
	{
		// user confirmed action?
    	if (m)
		{
    		if (scrap)
    		{
    			// call method to scrap the selected instrument
    			scrapInstrument(plantid, jiid);
    		}
    		else
    		{
    			// call method to return the instrument to service
    			unscrapInstrument(plantid);
    		}
		}
	}
}

/**
 * this method calls a dwr method to scrap an instrument (set to status B.E.R.)
 * 
 * @param {Integer} plantid the id of the instrument to be scrapped (set to status B.E.R.)
 * @param {Integer} jiid job item from which the instrument has been scrapped or null if done from instrument page
 */
function scrapInstrument(plantid, jiid)
{
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/instrumservice.js'), true,
	{
		callback: function()
		{	
			// call dwr service to scrap instrument
			instrumservice.scrapInstrument(plantid, jiid,
			{
				callback:function(result)
				{
					// scrapping of instrument successful
					if (result.success)
					{
						// reload the current page
						window.location.reload(true);
					}
					else
					{
						// show error message to user
						$j.prompt(result.message);
					}
				}
			});
		}
	});
}

/**
 * this method calls a dwr method to return an instrument to service (set to status 'In circulation')
 * 
 * @param {Integer} plantid the id of the instrument to be scrapped (set to status 'In circulation')
 */
function unscrapInstrument(plantid)
{
	// load the service javascript file if not already	
	loadScript.loadScriptsDynamic(new Array('dwr/interface/instrumservice.js'), true,
	{
		callback: function()
		{	
			// call dwr service to scrap instrument
			instrumservice.unscrapInstrument(plantid,
			{
				callback:function(result)
				{
					// scrapping of instrument successful
					if (result.success)
					{
						// reload the current page
						window.location.reload(true);
					}
					else
					{
						// show error message to user
						$j.prompt(result.message);
					}
				}
			});
		}
	});
}