/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	WebSearchInstrument.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	12/05/2009 - SH - Created File
*
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'../script/web/utilities/SubdivToContactFilter.js' );

/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'model';

/**
 * Array of description objects (multiple are possible) containing setup for 
 * jQueryUI initialization.
 * 
 * this is used in the init() function.
 */
var jquiAutocomplete = new Array(
				{
					inputFieldName: 'desc',
					inputFieldId: 'descid',
					source: 'searchdescriptiontags.json'
				},
				{
					inputFieldName: 'mfr', 
					inputFieldId: 'mfrid',
					source: 'searchmanufacturertags.json'
				});

/**
 * this method changes the available input fields when searching on/between dates for
 * a matching calibration date.
 * 
 * @param {Object} between indicates if the user is requesting to search between two dates
 */
function changeCalDueDateOption(between)
{
	// value false?
	if(between == "false")
	{
		// hide the date span for searching between two dates
		$j('span#calDueDateSpan').removeClass('vis').addClass('hid');
	}
	else
	{
		// show the date span for searching between two dates
		$j('span#calDueDateSpan').removeClass('hid').addClass('vis');
	}
}

function changeLastCalDateOption(between)
{
	// value false?
	if(between == "false")
	{
		// hide the date span for searching between two dates
		$j('span#lastCalDateSpan').removeClass('vis').addClass('hid');
	}
	else
	{
		// show the date span for searching between two dates
		$j('span#lastCalDateSpan').removeClass('hid').addClass('vis');
	}
}


/**
 * this function is called from the loadScript.init() function in Web_Main.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{
	// javascript file for creating contact search plugin
	loadScript.loadScriptsDynamic(new Array('../script/thirdparty/jQuery/jquery.preloadimage.js'), false,
	{
		callback: function()
		{
			$j.preloadImages('../img/web/searching.gif');
		}
	});

    $j("#remaininguses").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($j.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
            // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

	$j('#location').autocomplete({
        minLength : 3,
		source : function(request, response) {
			$j.getJSON("searchlocations.json",
				{
					coid: $j('#companyid').val(),
					subdivid: $j('#subdivid').val(),
					addressid: $j('#addressid').val(),
					term: request.term
				},
				response);
        },
		delay: 300,
        select : function(event, ui) {
            $j("#locationid").val(ui.item ? ui.item.id : "0");
        },
	});

}
