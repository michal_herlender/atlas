/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	WebEditInstrument.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	19/05/2009 - SH - Created File
*					:	01/10/2015 - TProvost - Manage i18n
*					: 	01/10/2015 - TProvost - Fix bugs in checkDuplicate function : enable/disable submit button + display name of model instr
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = ['dwr/interface/instrumservice.js',
					'../script/thirdparty/jQuery/jquery.boxy.js'];


var jquiAutocomplete = new Array(
		{
			inputFieldName: 'mfrname', 
			inputFieldId: 'mfrid',
			source: 'searchmanufacturertags.json'
		}
		);


/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{
	// load stylesheet using the dom if not already
	loadScript.loadStyle('../styles/jQuery/jquery-boxy.css', 'screen');

	//event handler for addresses
    $j('#subdivid').on('change',function () {
    	getActiveAddresses();
    });
    
    //event handler for contacts
    $j('#addrid').on('change',function () {
    	getLocations();
    });
    
    //dialog box initialization
    $j("#dialog").dialog({
        autoOpen: false,
        modal: true,
        width: 700,
        open: function (event, ui) {
        	//create dialog form
        	createForm();
        }
    });
    
    //event handler for add location button click
    $j('#addlocation').click(function (event) {
    	event.preventDefault ? event.preventDefault() : event.returnValue = false;
        $j('#dialog').dialog('open');
        $j('#location').focus();
    });
    
    //event handler for dialog save button
    $j('#savelocation').click(function (event){
    	event.preventDefault();
    	if($j('#location').val().trim() != ""){
    		insertLocation($j('#addrid').val(), $j('#location').val());
    	}
    	$j("#dialog").dialog("close");
    });

    checkUsageFieldVisibility();

    $j('#usages').on('change',function () {
        checkUsageFieldVisibility();
    });
    
    onStatusChange($j('#statusid'));
}

function checkUsageFieldVisibility() {
    if ( $j('#usages').is(":checked") ) {
        $j('.usageField').show();
    } else {
        $j('.usageField').hide();
    }
}

function createForm() {
	//remove any previous form
	$j('#location').val('');
	//get the address id.
	var addr1 =  $j('#addrid option:selected').text();
	$j('#addr1').text(addr1);
}

function getActiveAddresses(){
	var subdivid = $j('#subdivid').val();
	var response = $j.getJSON("addressapi/subdivdeliveryaddresslist/" + subdivid);
	response.done(function(addrs){
			var content = '';
			// addresses returned?
			if (addrs.length > 0){
				// add all new address options to variable
				$j.each(addrs, function(i){
					content += '<option value="' + addrs[i].key + '">' + addrs[i].value + '</option>';
				});
			}
			$j('#addrid').empty().append(content);
			getLocations();
			getContacts();
		}
    );
}

function getContacts(){
	var subdivid = $j('#subdivid').val();
	var response = $j.getJSON("contactapi/subdivcontactlist/" + subdivid);
	response.done(function(cons){
			// new contact options variable
			var content = '';
			if (cons.length > 0){
				// add all new contact options to variable
				$j.each(cons, function(i){
					content += '<option value="' + cons[i].key + '">' + cons[i].value + '</option>';
				});
			}
			// empty select and add new options
			$j('#personid').empty().append(content);
		}
    );
}

function getLocations(callback){
	var addrid = $j('#addrid').val();
	$j.getJSON("getaddresslocations.json", 
		{addrid: addrid},
		function(locations, status){
			// new contact options variable
			var content = '';
			content += '<option value="">' + i18n.t("web:webEditInstr.location.selectLocation", "Select a location") + '</option>';
			if (locations.length > 0){
				// add all new contact options to variable
				$j.each(locations, function(i){
					content += '<option value="' + locations[i].key + '">' + locations[i].value + '</option>';
				});
			}
			// empty select and add new options
			$j('#locid').empty().append(content);
			if(callback){
				callback();
			}
		}
    );
	
}

/**
 * this method adds a new location for an address
 * 
 * @param {Integer} addressid for the location
 * @param {String} the location text
 */
function insertLocation(addrid, locationText)
{
	//set up post data
	var data = {};
	data["key"] = addrid;
	data["value"] = locationText;

    //send ajax request
    var request = $j.ajax({
        	type: "POST",
        	contentType: "application/json",
        	url: "addlocation.json",
        	data: JSON.stringify(data),
        	dataType: 'json'});
    
    request.done( function (locid) {
    	getLocations(function(){
    		$j('#locid').val(locid);
    	});
    	
    });
}

/**
 * this method accepts the plant number and serial number supplied when adding an instrument and checks that there
 * are no instruments currently on the system that match, if so they are displayed in a boxy thickbox and the user has to
 * confirm that they wish to continue.
 * 
 * @param {Integer} coid the id of the company in which we should check for duplicates
 * @param {Boolean} perform indicates if the duplicate check should be performed
 */
function checkDuplicateInstruments(coid, perform)
{
	// should we perform check?
	if ((coid != null) && (perform))
	{
		// declare serial no and plant no values
		var serialno = null;
		var plantno = null;
		// get serial no and plant no values from the page
		serialno = $j('input[name="serialno"]').val();
		plantno = $j('input[name="plantno"]').val();
		// serial number and plant number null?
		if ((serialno != null) && (serialno != '') && (plantno != null))
		{
			// call dwr service to check for instrument duplicates based on serial no and plant no
			instrumservice.getSimilarIns(coid, plantno, serialno, null, null, false,
			{
				callback:function(instList)
				{
					// duplicates returned?
					if (instList.length > 0)
					{
						// variable to hold content
						var content = '';
						// load stylesheet using the dom if not already
						loadScript.loadStyle('../styles/jQuery/jquery-boxy.css', 'screen');
						// load javascript for boxy pop up if not already
						loadScript.loadScriptsDynamic(new Array('../script/thirdparty/jQuery/jquery.boxy.js'), false,
						{
							callback: function()
							{	
								// create content to display duplicates
								content += 	'<div id="checkDuplicateInstsBoxy" class="overflow marg-bot">' +
												// add warning box to display error messages if the move fails
												'<div class="warningBox1">' +
													'<div class="warningBox2">' +
														'<div class="warningBox3">' +
															i18n.t("web:webEditInstr.checkDuplicate.info", "The following instruments belonging to your company have been found with the same serial/plant number. " +
																	"If you wish to proceed with this insertion then click on the \'Continue\' button, alternatively you can click the 'Cancel' button to exit this instrument addition.") +	
														'</div>' +
													'</div>' +
												'</div>' +
												'<table class="default" summary="This table displays any potential conflicts in serial/plant numbers when adding an instrument">' +
													'<thead>' +
														'<tr>' +
															'<td colspan="5">' + i18n.t("web:webEditInstr.checkDuplicate.detail", {nb : instList.length, defaultValue : "Conflicting Instruments (" + instList.length + ")"}) + '</td>' +
														'</tr>' +
														'<tr>' +
															'<th class="mfr" scope="col">' + i18n.t("mfrShort", "Mfr") + '</th>' +
															'<th class="model" scope="col">' + i18n.t("model", "Model") + '</th>' +
															'<th class="desc" scope="col">' + i18n.t("descShort", "Desc") + '</th>' +														
															'<th class="plant" scope="col">' + i18n.t("plantNo", "Plant No") + '</th>' +
															'<th class="serial" scope="col">' + i18n.t("serialNo", "Serial No") + '</th>' +
														'</tr>' +
													'</thead>' +
													'<tfoot>' +
														'<tr>' +
															'<td colspan="5">&nbsp;</td>' +
														'</tr>' +
													'<tbody>';
								$j.each(instList, function(i)
								{
									content += 	'<tr>' +
													'<td>';
									(instList[i].model.mfr.genericMfr) ? content += instList[i].mfr.name : content += instList[i].model.mfr.name;
									content +=	'</td>' +
												'<td>' + instList[i].model.model + '</td>' +
												'<td>' + instList[i].model.description.description + '</td>' +
												'<td>' + instList[i].plantno + '</td>' +
												'<td>' + instList[i].serialno + '</td>' +
											'</tr>';
								});
								content += 	'</tbody>' +
										'</table>' +
										'<div class="marg-top text-center">' +
											'<input type="button" value="' + i18n.t("continue", "Continue") + '" onclick=" Boxy.get($j(\'div#checkDuplicateInstsBoxy\')).hide(); checkDuplicateInstruments(null, false); " />' +
											'<input type="button" value="' + i18n.t("cancel", "Cancel") + '" onclick=" $j(\'input.submitInstrument\').attr(\'disabled\', false); Boxy.get($j(\'div#checkDuplicateInstsBoxy\')).hide(); " />' +
										'</div>';
								// create new boxy pop up using content variable
								var checkDuplicateInstsBoxy = new Boxy(content, {title: i18n.t("web:webEditInstr.checkDuplicate.title", "Duplicate Instrument Serial/Plant Numbers Found"), modal: true, unloadOnHide: true});
								// show boxy pop up and adjust size
								checkDuplicateInstsBoxy.tween(720, 280);
							}						
						});
					}
					else
					{
						// no duplicates so submit the form
						$j('form#editinstrumentform').submit();
					}
				}
			});	
		}
		else
		{
			// cannot check for duplicates without serial no so submit the form
			$j('form#editinstrumentform').submit();
		}
	}
	else
	{
		// perform is false so submit the form
		$j('form#editinstrumentform').submit();
	}
}

function updateLocations()
{
	// selected address id
	var addrid = $j('select#addrid').val();
	// current instrument location if it has one
	var currentInstLoc = $j('input#currentInstLoc').val();
	// start location option content
	var options = '<option value=""> -- ' + i18n.t("web:webEditInstr.location.noSpecLocWithAdr", "No specific location within address") + ' -- </option>';
	// get check all addresses
	$j.each(addrArray, function(i, address)
	{
		if (address.addrid == addrid)
		{
			$j.each(address.locArray, function(x, location)
			{
				options += 	'<option value="' + location.locid + '" ';
				if (currentInstLoc != 0)
				{
					if(location.locid == selectedLocId)
					{
						options += 'selected="' + i18n.t("selected", "selected") + '"';
					}
				}				
	 			options += '>' + location.locname + '</option>';
			});
			// address has locations?			
			if(address.locArray.length < 1)
			{
				// append location options
				$j('#locationSelect').empty().append(options);
				// show location options
				$j('#locationLi').addClass('hid').removeClass('vis');
			}
			else
			{
				// hide location options
				$j('#locationLi').addClass('vis').removeClass('hid');
			}
		}
	});
}

function onStatusChange(ele) {
	let selectedStatus = $j(ele).val();
	let isFirst = true;
	$j('#usageTypeId option').each(function(index) {
		if($j(this).attr('data-linkedstatus') == selectedStatus) {
			$j(this).removeClass('hid').addClass('vis');
			if(isFirst) {
				$j(this).attr('selected','selected');
				isFirst = false;
			}
		}
		else {
			$j(this).removeClass('vis').addClass('hid');
			$j(this).removeAttr('selected');
		}
	});
}