var jqCalendar = new Array ( { 
								 inputField: 'calDate',
								 dateFormat: 'dd.mm.yy',
								 displayDates: 'past',
								 showWeekends: true
							 },
                            {
                                inputField: 'certDate',
                                dateFormat: 'dd.mm.yy',
                                displayDates: 'past',
                                showWeekends: true
                            });



function init()
{
	$j('#caltype').on('change', function() {
		 var selected = $j(this).find('option:selected');
	     var requirementType = selected.data('requirementtype');
	     if(requirementType == 'FULL_CALIBRATION'){
	    	 $j('.calonly').show();
	    	 $j('#datelabel').text(i18n.t("calibrationDate", "Calibration Date"));
	     } else if(requirementType == 'MAINTENANCE'){
	    	 $j('.calonly').hide();
	    	 $j('#datelabel').text(i18n.t("maintenancedate", "Maintenance Date"));
	     } else if(requirementType == 'INTERIM_CALIBRATION'){
	    	 $j('.calonly').hide();
	    	 $j('#datelabel').text(i18n.t("interimcaldate", "Interim Cal Date"));
	     }
	});

    //Prevent non numeric input into deviation
    $j("#deviation").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($j.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
            // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

}