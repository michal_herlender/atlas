/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	TermsAndConditions.js
*	DESCRIPTION		:
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	15/07/2009 - SH - Created File
*					: 	30/09/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'dwr/interface/calibrationservice.js');

/**
 * create array of descriptions to be used for password strength function
 */
var desc = new Array();
desc[0] = "Very Weak";
desc[1] = "Weak";
desc[2] = "Better";
desc[3] = "Medium";
desc[4] = "Strong";
desc[5] = "Strongest";

function init()
{
	// i18n of descriptions for password strength function
	desc[0] = i18n.t("common:password.veryWeak", "Very Weak");
	desc[1] = i18n.t("common:password.weak", "Weak");
	desc[2] = i18n.t("common:password.better", "Better");
	desc[3] = i18n.t("common:password.medium", "Medium");
	desc[4] = i18n.t("common:password.strong", "Strong");
	desc[5] = i18n.t("common:password.strongest", "Strongest");
}

/**
 * this function checks the strength of the password entered by the user and displays 
 * text and coloured bar dependant on that strength
 * 
 * @param {String} password password entered by the user in input field
 * @param {Object} div the div object we want to update with text and class name
 */
function passwordStrength(password, div)
{
	// call dwr service to check the password strength
	calibrationservice.checkPasswordStrength(password,
	{	
		callback:function(score)
		{
			if (password.length < 1)
			{
				// change text in div dependant on password strength
				$j(div).find('span:first').text(i18n.t("pwdNotEntered", "Password not entered"));
				// change class of coloured div dependant on password strength
				$j(div).removeClass().addClass("strength" + score);
			}
			else
			{
				// change text in div dependant on password strength
				$j(div).find('span:first').text(desc[score]);
				// change class of coloured div dependant on password strength
				$j(div).removeClass().addClass("strength" + score);	
			}			
        }	
	});
}