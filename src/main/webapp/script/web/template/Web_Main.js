/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	Cwms_Main.js
*	DESCRIPTION		:	Javascript file run on every page load.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	Created by SH 06/06/2007
*					:	21/09/2015 - TP - Internationalization of the javascript :
*							- Add import library i18next-latest.js
*							- Add initialization of i18n in the perform function
*					:	10/11/2015 - TProvost - Fix bug in function restrictSpaces (avoids displaying several times the prompt msg)
****************************************************************************************************************************************/

/*
 *  Provide a no-conflict instance of jQuery (In CWMS2, had been manually added to jquery.js)
 */
var $j = jQuery.noConflict();

/**
 * this variable is used by the restrictSpaces function in order to avoids displaying several times the prompt msg
 */
//var userPrompted = false;

/**
 * array of javascript files to import into cwms_main template
 */
var mainImports = new Array (	'../script/thirdparty/i18next/i18next-latest.js',	
								'../script/trescal/core/utilities/Date.js',
								'../script/thirdparty/jQuery/jquery.impromptu.js',
								'../script/thirdparty/jQuery/jquery.antechplugins.js',
								'dwr/engine.js',
								'dwr/util.js');

/**
 * array which holds all the javascript files which have been loaded into the page in this file.
 */
var loadedSCRIPTS = new Array ();

/**
 * array which holds all the stylesheet files which have been loaded into the page in this file.
 */
var loadedSTYLES = new Array();

/**
 * contains object literals for importing javascript files and checking whether a specific menu
 * tab should be loaded when returning to a page
 */
var loadScript = 
{	
	/**
	 * these three variables can be used in any page specific javascript file to check the
	 * time value of last DWR call, checked against the current DWR call.
	 */
	lastUpdate1:	0,
	lastUpdate2:	0,
	lastUpdate3:	0,
	lastUpdate4:	0,
	
	/**
	 * imports the javascript file passed in libraryName
	 * 
	 * @param {String} libraryName is a javacript file path to be loaded
	 */
  	require: 	function(libraryName) 
				{
			    // inserting via DOM fails in Safari 2.0, so brute force approach
			    document.write('<script type="text/javascript" src="' + libraryName + '"></script>');
			  	},
	
	/**
	 * iterates through the array passed and imports all javascript files if they
	 * are not already present on the page.
	 * 
	 * @param {Array} Imports is a string array of javascript file paths to load
	 */
	loadScriptImports:	function(Imports) 
						{
							// loop through each script to load
							$j.each( Imports, function(i, n)
							{
								var loaded = false;
								
								// loop through the loaded scripts and check if
								// the one to be loaded is present 
								$j.each( loadedSCRIPTS, function(x, l)
								{
									// already loaded
									if (n == l)
									{
										loaded = true;
									}
								});
								// not loaded
								if (loaded == false)
								{ 
									// load
									loadScript.require(n);
									// add to loaded array
									loadedSCRIPTS.push(n);
								}
							});
														
						},
					
	/**
	 * Applies focus to the element passed when the page is loaded.
	 * 
	 * @param {String} Elem contains the string id of the element to apply focus to
	 */
	focusOn:		function(Elem)
					{
						$j('#' + Elem).focus();
					},

	/**
	 * Creates a jquery calendar with the parameters given in the jqCalendar array of objects
	 * 
	 * @param {Object} jqCalendarSetup contains all the parameters needed to create the calendar (input id, date format and dates to display)
	 */		
	createJQCal:	function(jqCalendarObj)
					{						
						// get new date object
						var today = new Date();
																		
						// set the max or min date value and year range depending on the value 
						// submitted in the displayDates literal
						switch (jqCalendarObj.displayDates)
						{
							case 'past':	$j('input#' + jqCalendarObj.inputField).datepicker({defaultDate: null, changeMonth: true, changeYear: true, dateFormat: jqCalendarObj.dateFormat, yearRange: '-05:+00', beforeShowDay: (jqCalendarObj.showWeekends) ? null : $j.datepicker.noWeekends, maxDate: 0});
											break;
							case 'future':	$j('input#' + jqCalendarObj.inputField).datepicker({defaultDate: null, changeMonth: true, changeYear: true, dateFormat: jqCalendarObj.dateFormat, yearRange: '-00:+05', beforeShowDay: (jqCalendarObj.showWeekends) ? null : $j.datepicker.noWeekends, minDate: (jqCalendarObj.minDate) ? jqCalendarObj.minDate : 0});
											break;
							case 'all':		$j('input#' + jqCalendarObj.inputField).datepicker({defaultDate: null, changeMonth: true, changeYear: true, dateFormat: jqCalendarObj.dateFormat, yearRange: '-05:+05', beforeShowDay: (jqCalendarObj.showWeekends) ? null : $j.datepicker.noWeekends});
											break;
						}												
					},
				
				
	/**
	 * creates a collection or quotation request search and basket
	 */					
	createWebPlug:		function()
							{
								// load the multi model specific javascript files
								loadScript.loadScriptsDynamic(new Array('../script/web/utilities/searchplugins/WebSearchPlugin.js'), false,
								{
									callback: function()
									{
										// call the init function in WebSearchPlugin.js to create search
										WebSearchPlugin.init();
									}
								});
							},
					
	/**
	 * creates a description search autocomplete with the parameters given in the jquiAutocompleteSetup object
	 * 
	 * @param {Object} jquiAutocompleteSetup object with initialization parameters:
	 * inputFieldName - (name to submit to server, e.g. "descId") 
	 * source - (source data to search, e.g. "searchdescription.json")
	 * inputFieldId - (optional value to default in form, e.g. "Multimeter")
	 */
	createJQUIAutocomplete:	
		function (jquiAutocompleteSetup)
		{
			$j('input#'+jquiAutocompleteSetup.inputFieldName).autocomplete(
					{
						source : jquiAutocompleteSetup.source,
						minLength : 3,
						select : function(event, ui) {
								$j("#"+jquiAutocompleteSetup.inputFieldId).val(ui.item ? ui.item.id : "0");
						},
					});
		},
											
						
		
	/**
	 * Loads javascript files passed from literals.  If any of the javascript files
	 * have already been loaded then they will be skipped.
	 * 
	 * @param {Array} scripts is a string array of javascript file paths to be loaded
	 * @param {boolean} sync do we require synchronous loading of javascript file
	 * @param {func} func callback method provided
	 * GB 2016-02-04: Updated to use 'done' instead of 'success' handler and fixed cache 'true' to true
	 * GB 2016-03-04: Added same debug messages as in main handler to help troubleshoot json loading issues
	 */	
	loadScriptsDynamic:	function(scripts, sync, func)
						{
							// loop through each script to load
							$j.each(scripts, function(i, n)
							{							
								// not loaded
								if ($j.inArray(n, loadedSCRIPTS) == -1)
								{
									// add to loaded script to array
									loadedSCRIPTS.push(n);
									// load
									$j.ajax({
										url: n,
										dataType: 'script',
										type: 'GET',
										cache: true,
										async: !sync})
									.done(function(data, textStatus, jqXHR){
										console.log(textStatus);
										console.log('done: '+n);
										console.log("typeof data: "+typeof data);
										if (typeof data == 'string') {
											console.log("length: "+data.length);
										}
										// last iteration of loop
										if (i == (scripts.length - 1))
										{
											// call callback function if present
											if (func){
												func.callback();
												console.log('ran callback');
											}
										}
									})
									.fail(function (jqXHR, textStatus, errorThrown) {
										console.log('error: '+n);
										console.log(textStatus);
								        console.log(errorThrown);
								        alert('error loading: '+n);
								    });
								}
								else
								{
									console.log('skipping: '+n);
									// last iteration of loop
									if (i == (scripts.length - 1))
									{
										// call callback function if present
										if (func){
											func.callback();
											console.log('ran callback');
										}
									}
								}
							});
						},
	
	/**
	 * Loads stylesheet file passed.  If the stylesheet file
	 * has already been loaded then it will be skipped.
	 * 
	 * @param {String} href stylesheet file path to be loaded
	 * @param {String} media the media stylesheet should adhere to
	 * @param {Object} func callback function
	 */		
	loadStyle:		function(href, media, func)
					{
						// not loaded
						if ($j.inArray(href, loadedSTYLES) == -1)
						{							
							// add to loaded styles to array
							loadedSTYLES.push(href);
							// load style into head of page
							$j('head').append('<link rel=\'stylesheet\' type=\'text/css\' href=\'' + href + '\' media=\'' + media + '\' />');
							// call callback function if present
							if (func){ func.callback(); }
						}
						else
						{
							// call callback function if present
							if (func){ func.callback(); }
						}
					},
	
	/**
	 * Swaps out the submit button for a loading image on search form pages
	 * 
	 * @param {Object} li the li element inside which the submit button resides
	 */	
	applySearchingImage:	function(li)
							{								
								$j(li).find('label')
										.html('<strong>' + i18n.t("searching", "Searching...") + '</strong>')
										.end()
										.find('input')
										.replaceWith('<img src="../img/web/searching.gif" width="128" height="15" class="searchingImage" />');
								
								return true;								
							},
							
	/**
	 * this method toggles the visibility of items in table
	 * 
	 * @param {Integer} entityId the id of the entity with items
	 * @param {String} itemtype the type of items to be displayed/hidden
	 * @param {String} linkId the id of the show items link clicked
	 */
	toggleItemDisplay: 	function(entityId, itemtype, linkId)
						{
							// actions are currently hidden so make them visible
							if ($j('#child' + entityId).css('display') == 'none')
							{
								// remove image to show actions and append hide actions image
								var imgtext = i18n.t("hideItemsInTable", {varItemType : itemtype, defaultValue : "Hide " + itemtype + " Items"});
								$j('#' + linkId).empty().append('<img src="../img/icons/items_hide.png" width="16" height="16" alt="' + imgtext + '" title="' + imgtext + '" class="image_inline" />');
								// make the actions row visible and add slide down effect
								$j('#child' + entityId).removeClass('hid').addClass('vis').find('div').slideDown(1000);
							}
							// actions are currently visible so hide them
							else
							{
								// remove image to hide actions and append show actions image
								var imgtext = i18n.t("viewItemsInTable", {varItemType : itemtype, defaultValue : "View " + itemtype + " Items"});
								$j('#' + linkId).empty().append('<img src="../img/icons/items.png" width="16" height="16" alt="' + imgtext + '" title="' + imgtext + '" class="image_inline" />');
								// add effect to slide up actions
								$j('#child' + entityId + ' div').slideUp(1000, function()
								{
									// hide row
									$j('#child' + entityId).removeClass('vis').addClass('hid');
								});
							}	
						},
						
	/**
	 * this method restricts the user from entering spaces in the text element passed
	 * 
	 * @param {Object} element the element in which we want to restrict the addition of spaces
	 */	
	restrictSpaces: function(element)
					{
						// load the javascript file if not already
						loadScript.loadScriptsDynamic(new Array('../script/thirdparty/jQuery/jquery.textchange.js'), true,
						{
							callback: function()
							{
								// add text change event to element passed
								$j(element).bind('textchange', function()
								{									
									// add value of element to variable
									var value = $j(this).val();									
									// set boolean value so multiple user prompts are not opened
									//var userPrompted = false;
									// element value contains space and user has not already been prompted
									if ((value.indexOf(' ') != -1) && (!userPrompted))
									{										
										// set user being prompted
										userPrompted = true;
										// prompt the user that spaces are not allowed here
										var messageText = i18n.t("spacesNotAllowedInField", 
												"<p>Spaces are restricted in this field</p>" + 
												"<p>Click 'OK' or press 'Enter' to continue</p>");
										$j.prompt(messageText,
										{ 
											submit: confirmcallback,
											close: confirmcallback,
											buttons: 
											{ 
												OK: true
											}, 
											focus: 0
										});	
										
										// callback method for initial prompt
										function confirmcallback(v,m)
										{
											// user confirmed action?
									    	if (v)
											{
									    		// replace all spaces added by user and set into new variable
												var newvalue = value.replace(/ /g, '');
												// set new value into element and apply focus
												$j(element).val(newvalue).focus();
												// set user prompted
												userPrompted = false;
												// trigger keyup event to reset values in plugin (i.e. the text change plugin used saves the previous
												// text entered so this still has the space in it. By triggering the keyup event manually these values are reset.
												$j(element).trigger('keyup');					
											}
										}
									}									
								});
							}
						});
					},
							
	/**
	 * this method allows to internationalize months and days
	 */		
	i18nDate:	function()
	{
		// load the javascript file if not already
		loadScript.loadScriptsDynamic(new Array('../script/trescal/core/utilities/Date.js'), true,
		{
			callback: function()
			{
				// call the i18nDate function in Date.js to init i18n of months and days
				i18nDate();
			}
		});
	},				
					
	/**
	 * function called from onload attribute of body tag.  Changes the selected subnavigation tab depending on
	 * the value held in LOADTAB and checks for an array of elements to apply nifty corners to.
	 */		
	init:  	function(perform)
			{
				if (perform == true)
				{
					
					// Internationalization init
					i18n.init({
				        lng: i18locale,
				        getAsync: false,
				        ns: { namespaces: ['common', 'web'], defaultNs: 'common'},
				        useLocalStorage: false,
				        useDataAttrOptions: true,
				        resGetPath: '../locales/__lng__/__ns__.json',
				        debug: true,				//Debug output
				        fallbackLng: 'en',			//Set fallback language
				        fallbackOnNull: true,		//Handling of null values
				        fallbackOnEmpty: true,		//Handling of empty string values
				        fallbackToDefaultNS: true,	//Set fallback namespace
				        shortcutFunction: 'defaultValue',
					    cache: true					// Prevent jquery.ajax from adding timestamp
				    });
					
					// internationalisation of date (months and days)
					loadScript.i18nDate();
					
					// element with this id present in page? If so, create web search plugin
					if ($j('div.webSearchPlugin').length)
					{
						// create the web search plugin
						loadScript.createWebPlug();
					}									
					// sub navigation exists?
					if ($j('#subnav').length || $j('.subnavtab').length)
					{
						// javascript file for changing submenu navigation
						loadScript.loadScriptsDynamic(new Array('../script/trescal/core/utilities/Menu.js'), false,
						{
							callback:function()
							{
								// LOADTAB contains data?
								if (LOADTAB)
								{
									// switch the focus to sub navigation tab held in LOADTAB
									switchMenuFocus(menuElements, LOADTAB, false);
								}
							}
						});
					}				
					// jCalendar object or jquiAutocomplete exists - both initialized with jQueryUI
					if ( (typeof jqCalendar != 'undefined') ||
						 (typeof jquiAutocomplete != 'undefined') )
					{
						// load stylesheet using the dom
						loadScript.loadStyle('../styles/jQuery/jquery-ui.css', 'screen');
						// load the jquery UI javascript file
						loadScript.loadScriptsDynamic(new Array('../script/thirdparty/jQuery/jquery.ui.js'), false,
						{
							callback: function()
							{
								if ( typeof jqCalendar != 'undefined' )
								{
									$j.each(jqCalendar, function(i){
										// create jquery calendars
										loadScript.createJQCal(jqCalendar[i]);
									});
								}
								if ( typeof jquiAutocomplete != 'undefined' )
								{
									$j.each(jquiAutocomplete, function(i){
										// create jquery autocompletes
										loadScript.createJQUIAutocomplete(jquiAutocomplete[i]);
									});
								}
							}
						});	
					}														
					// focusElement variable exists?
					if ( typeof focusElement != 'undefined')
					{
						if ($j('#' + focusElement.toString()))
						{
							// call object literal to focus on element
							loadScript.focusOn(focusElement.toString());
						}
					}				
					// anchor with class name of jTip present in page?
					// If so, load jTip js file and css file 
					if ($j('a.jTip').length || $j('a.jconTip').length)
					{
						// load stylesheet using the dom
						loadScript.loadStyle('../styles/jQuery/web/jquery-jtip.css', 'screen');
						// load the javascript file if not already
						loadScript.loadScriptsDynamic(new Array ('../script/thirdparty/jQuery/jquery.jtip.js'), false,
						{
							callback: function()
							{
								JT_init();							
							}
						});
					}
					if (typeof init != 'undefined')
					{						
						init();
					}					
				}
				else
				{
					if (typeof init != 'undefined')
					{						
						init();
					}
				}				
			},
			
			/**
			 * this method builds and returns the html content necessary to create an overlay thickbox
			 * 
			 * @return {String} content the html content for overlay
			 */
			buildOverlay:		function()
								{
									// create html content and add to variable
									var content = 	'<div class="overlay_wrapper">' +												
														'<div class="overlay_inner">' +														
															'<div class="overlay_bg_wrapper">' +
																'<div class="overlay_bg overlay_bg_n"></div>' +
																'<div class="overlay_bg overlay_bg_ne"></div>' +
																'<div class="overlay_bg overlay_bg_e"></div>' +
																'<div class="overlay_bg overlay_bg_se"></div>' +
																'<div class="overlay_bg overlay_bg_s"></div>' +
																'<div class="overlay_bg overlay_bg_sw"></div>' +
																'<div class="overlay_bg overlay_bg_w"></div>' +
																'<div class="overlay_bg overlay_bg_nw"></div>' +
															'</div>' +
															'<div class="overlay_contentholder">' +
																'<div class="overlay_title hid"></div>' +
																'<div class="overlay_content"></div>' +
															'</div>' +												
														'</div>' +
													'</div>';
									// return html content
									return content;
								},
			
			/**
			 * 
			 * @param title
			 * @param wrap
			 */
			addOverlayTitle:	function(title, wrap)
								{
									// has title been passed?
									if (title != null)
									{
										// check for existing title
										if ($j(wrap).hasClass('hid'))
										{
											// add title to wrapper
											$j(wrap).removeClass('hid').addClass('vis').html(title);
										}
										else
										{
											// change current title element
											$j(wrap).html(title);
										}
									}
									else
									{
										// remove any title
										$j(wrap).removeClass('vis').addClass('hid').empty();
									}
								},
			
			/**
			 * this method closes the current overlay instance
			 */
			closeOverlay:		function()
								{
									// get current overlay api
									api = $j('div.overlay_wrapper').overlay({data: 'overlay'});
									// close the overlay
									api.close();
								},
							
			/**
			 * this method allows the content of an overlay to be changed/modified as well as the height for
			 * larger content.
			 * 
			 * @param {String} href link to new external page if required
			 * @param {String} html new html content if required
			 * @param {String} title the title to be applied to overlay, if any? (null if no title)
			 * @param {Boolean} error indicates if this is an error message that should be prepended
			 * @param {Integer} height the height of overlay as a percentage
			 * @param {Integer} width the width of overlay set in pixels
			 * @param {String} focusElement an element id to apply focus to within overlay content
			 */
			modifyOverlay:		function(href, html, title, error, height, width, focusElement)
								{
									// get objects required
									var wrapper = $j('div.overlay_wrapper');
									var contentwrap = wrapper.find('div.overlay_contentholder > .overlay_content');
									var titlewrap = wrapper.find('div.overlay_contentholder > .overlay_title');
									// link not null?
									if (href != null)
									{								
										// empty content and apply new page
										contentwrap.empty().load(href, function()
										{
											loadScript.init(true);
										});							
						            	// title?
						            	loadScript.addOverlayTitle(title, titlewrap);
									}
									else if (html != null)
									{
										if (!error)
										{
											// empty content and apply new html
											contentwrap.empty().append(html);
											// title?
							            	loadScript.addOverlayTitle(title, titlewrap);
										}
										else
										{
											// check error box not already present
											if (contentwrap.find('div.warningBox1').length)
											{
												// if so remove it from overlay content
												contentwrap.find('div.warningBox1').remove();
											}
											// retrieve an existing overlay, prepend new html as it is error message
											contentwrap.prepend('<div class="warningBox1"><div class="warningBox2"><div class="warningBox3">' + html + '</div></div></div>');
										}
									}
									// new height to be applied
									var modifyHeight = '';
									// height not null? 
									if (height != null)
									{
										if (height.toString().indexOf('px') != -1)
										{
											// change height
											modifyHeight = height;
										}
										else
										{
											// change height
											modifyHeight = height + '%';
										}
									}
									else
									{
										modifyHeight = height + '%';
									}
									// new height requested?
									if (height != null && width != null)
									{								
										// modify height and width of overlay
										wrapper.animate( { height: modifyHeight, width: width + 'px' }, { queue: false, duration: 1000 } );	
									}
									else if (height != null)
									{
										// modify height of overlay
										wrapper.animate( { height: modifyHeight }, { queue: false, duration: 1000 } );
									}
									else if (width != null)
									{
										// modify width of overlay
										wrapper.animate( { width: width + 'px' }, { queue: false, duration: 1000 } );
									}
									// apply focus?
						            if (focusElement != '' && focusElement != null)
						            {
						               	// apply focus
						            	$j('#' + focusElement).focus();
						            }
								},
							
			/**
			 * this method creates an overlay thickbox.  Depending on the values passed the overlay content can come from an external
			 * url or it can be appended from html passed in the method parameters. The height and width of the overlay can also be set
			 * 
			 * @param {String} type the type of overlay (current values 'dynamic', 'external', 'inline')
			 * @param {String} title the title to be applied to overlay, if any? (null if no title)
			 * @param {String} href the url of external content
			 * @param {String} html content provided in the form of a html string
			 * @param {String} id id of an element which contains inline content
			 * @param {Integer} height the height of overlay as a percentage or you can define as pixels if postfix included (i.e. '50px')
			 * @param {Integer} width the width of overlay set in pixels	 
			 * @param {String} focusElement an element id to apply focus to within overlay content
			 */
			createOverlay:		function(type, title, href, html, id, height, width, focusElement)
								{
									// load required javascript if it has not already
									loadScript.loadScriptsDynamic(new Array ('../script/thirdparty/jQuery/jquery.tools.overlay.js'), true,
									{
										callback: function()
										{
											// append div for overlay content
											$j('body').append(loadScript.buildOverlay());
											// get objects required
											var wrapper = $j('div.overlay_wrapper');
											var contentwrap = wrapper.find('div.overlay_contentholder > .overlay_content');
											var titlewrap = wrapper.find('div.overlay_contentholder > .overlay_title');
											// width not null?
											if (width != null)
											{
												// change width and height if necessary
												wrapper.width(width + 'px');
											}
											// height not null?
											if (height != null)
											{
												if (height.toString().indexOf('px') != -1)
												{
													// change height
													wrapper.height(height);
												}
												else
												{
													// change height
													wrapper.height(height + '%');
												}										
											}
											// dynamic content type?
											if (type == 'dynamic')
											{
												// initialise overlay using html content
												wrapper.overlay(
												{ 											
													top: '4%',
													left: 'center',
													speed: 0,
													mask: 
													{ 
												        color: '#666', 
												        loadSpeed: 400, 
												        opacity: 0.5 
												    },
													onBeforeLoad: function()
													{
											            // load the page specified
											            contentwrap.append(html);
											            // title?
											           	loadScript.addOverlayTitle(title, titlewrap);
											        },
													onClose: function()
													{
														// find and remove overlay and wrapper
				        								$j('body').find('div.overlay_wrapper').next().remove().end().remove();
				    								},
				    								target: 'div.overlay_wrapper',
				    								load: true	
												});								
											}
											else if (type == 'inline')
											{
												// initialise overlay using html content
												wrapper.overlay(
												{ 									
													top: '4%',
													left: 'center',
													speed: 0,
													mask: 
													{ 
												        color: '#666', 
												        loadSpeed: 400, 
												        opacity: 0.5 
												    },
													onBeforeLoad: function()
													{
											            // load the page specified
											            contentwrap.append($j('#' + id).children());
											            // remove the elements appended from page
											            $j('#' + id).empty();
											            // title?
											           	loadScript.addOverlayTitle(title, titlewrap);
											        },
													onClose: function()
													{
														// add elements in overlay back to page
														$j('#' + id).append(contentwrap.children());
														// find and remove overlay and wrapper
				        								$j('body').find('div.overlay_wrapper').next().remove().end().remove();
				    								},
				    								target: 'div.overlay_wrapper',
				    								load: true									        
												});	
											}
											else
											{
												// initialise overlay using external url content
												wrapper.overlay(
												{ 											
													top: '4%',
													left: 'center',
													speed: 0,
													mask: 
													{ 
												        color: '#666', 
												        loadSpeed: 400, 
												        opacity: 0.5
												    },
													onBeforeLoad: function()
													{
											            // add iframe after content wrapper and then remove
											            contentwrap.parent().css({overflow: 'hidden'}).end().after('<iframe src="' + href + '" width="100%" height="96.5%" marginwidth="0" marginheight="0" frameborder="0" border="0"></iframe>').remove();
											            // title?
											           	loadScript.addOverlayTitle(title, titlewrap);
											        },
													onClose: function()
													{ 
														// find and remove overlay and wrapper
				        								$j('body').find('div.overlay_wrapper').next().remove().end().remove();
				        								parent.location.reload(true);
				    								},
				    								target: 'div.overlay_wrapper',
				    								load: true	
												});									
											}
											// apply focus?
								            if (focusElement != '' && focusElement != null)
								            {
								               	// apply focus
								            	setTimeout(function(){ $j('#' + focusElement).focus(); }, 400);
								            }
										}
									});
								},
}

function logout() {
	$j('#logout').submit();
}

/**
 * loads all javascript files in the mainImports array.
 * 
 * @param {Array} mainImports is a string array of javascript file paths to be loaded.
 */
loadScript.loadScriptImports(mainImports);

/**
 * load impromptu styles and javascript for displaying messages to the user
 */
loadScript.loadStyle('../styles/jQuery/jquery-impromptu.css', 'screen');

/**
 * if pageImports is defined then load all javascript files in the array.
 */
if ( typeof pageImports != 'undefined')
{
	loadScript.loadScriptImports(pageImports);
}