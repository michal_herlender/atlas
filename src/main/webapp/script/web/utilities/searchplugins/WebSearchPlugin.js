/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	WebSearchPlugin.js
*	DESCRIPTION		:	Javascript file imported when creating a web search plugin on a page
* 
*	DEPENDENCIES	:   -
*
*					:	QUOTATION BASKET INPUTS - plantid, modelid, qty, plantno, serialno, quotetype, caltype, comment, commentdesc
*					: 	COLLECTION BASKET INPUTS - plantid, modelid, qty, plantno, serialno, caltype, turn, faulty, faultydesc
*
*					:	<div class="webSearchPlugin">
*					:		Type of plugin required, either 'Quotation' or 'Collection'
*					:		<input type="hidden" name="pluginType" value="Collection" />
*					:		Turnaround options supplied from enum - all options should be added comma delimited from page reference data.
*					:		<input type="hidden" name="turnOpts" value="STANDARD-STANDARD, FAST_TRACK-FAST TRACK" />
*					:		Company coid used to search instruments
*					:		<input type="hidden" name="pluginCoid" value="1793" />
*					:		Business Subdiv id used to determine cal types
*					:		<input type="hidden" name="pluginBusinessSubdivid" value="1794" />
*					:		Name of system company
*					:		<input type="hidden" name="pluginSysComp" value="Antech" />
*					:		Telephone number of system company
*					:		<input type="hidden" name="pluginSysTel" value="01493 440600" />
*					:	</div>
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	File Created - SH - 21/11/2008
*					:	Fix image location - TP - 16/07/2015
*					: 	25/09/2015 - TProvost - Manage i18n
*					: 	2016-03-04: Added Business Subdiv id (required for cal types)
****************************************************************************************************************************************/
 
 	/**
 	 * load impromptu styles and javascript for displaying messages to the user
 	 */
	loadScript.loadStyle('../styles/jQuery/jquery-impromptu.css', 'screen');
	loadScript.loadScriptsDynamic(new Array('../script/thirdparty/jQuery/jquery.impromptu.js'), false);

  

 	/**
	 * this keeps a count of the number of items in our basket
	 */
 	var webSearchPluginBasketCount = 0;
 
 								/**
	 							 * this literal is called when the page loads if an element with the id of 'modelSearchPlugin' is found on the page.
								 * this function then loads all the necessary scripts it needs
	 							 */
	WebSearchPlugin =  			{			init:	function() 	
											{
												// scripts to load
												var Scripts = new Array (	'../script/thirdparty/jQuery/jquery.tooltip.js',
																			'dwr/interface/instrumentmodelservice.js',
																			'dwr/interface/instrumservice.js',
																			'dwr/interface/caltypeserv.js');
										
												// load the common javascript files for this plugin
												loadScript.loadScriptsDynamic(Scripts, true,
												{
													callback: function()
													{
														// turnaround options hidden input present?
														if (($j('input[name="turnOpts"]').length) && ($j('input[name="turnOpts"]').val() != ''))
														{
															// add all turnaround options to array
															var test = $j('input[name="turnOpts"]').val().split('(').join('');
															test = test.split(')').join('');
															test = test.split(' ').join('');
															WebSearchPlugin.turnOpts = test.split(',');
														}
														// Business Subdiv id hidden input present?
														if (($j('input[name="pluginBusinessSubdivid"]').length) && ($j('input[name="pluginBusinessSubdivid"]').val() != ''))
														{
															// set coid value pluginBusinessSubdivid
															WebSearchPlugin.businessSubdivid = $j('input[name="pluginBusinessSubdivid"]').val();
															console.log("businessSubdivid: "+WebSearchPlugin.businessSubdivid);
														}
														// Get locale info
														if (($j('input[name="pluginLanguage"]').length) && ($j('input[name="pluginLanguage"]').val() != ''))
														{
															// set coid value pluginBusinessSubdivid
															WebSearchPlugin.language = $j('input[name="pluginLanguage"]').val();
															console.log("language: "+WebSearchPlugin.language);
														}
														if (($j('input[name="pluginCountry"]').length) && ($j('input[name="pluginCountry"]').val() != ''))
														{
															// set coid value pluginBusinessSubdivid
															WebSearchPlugin.country = $j('input[name="pluginCountry"]').val();
															console.log("country: "+WebSearchPlugin.country);
														}

													
														// populate qtyOptions
														WebSearchPlugin.createOptions();
														// draw normal search plugin
														WebSearchPlugin.drawSearch();											
														// plugin type hidden input present?
														if ($j('input[name="pluginType"]').length)
														{
															// quotation plugin?
															if ($j('input[name="pluginType"]').val() == 'Quotation')
															{
																// draw quotation basket
																WebSearchPlugin.drawQuoteBasket();
																// set plugin type
																WebSearchPlugin.pluginType = $j('input[name="pluginType"]').val();
																// set current basket
																WebSearchPlugin.currentBasket = WebSearchPlugin.quoteBasket;
															}
															else
															{
																// draw collection basket
																WebSearchPlugin.drawCollectBasket();
																// set plugin type
																WebSearchPlugin.pluginType = $j('input[name="pluginType"]').val();
																// set current basket
																WebSearchPlugin.currentBasket = WebSearchPlugin.collectBasket;
															}
														}														
														// add plugin type to header
														if (WebSearchPlugin.pluginType == 'Quotation')
															{
																$j('#pluginType').text(i18n.t("web:webSearchPlugin.quoteReqItemSearchAndBasket", "Quotation Request Item Search And Basket"));												
															}
														else
															{
																$j('#pluginType').text(i18n.t("web:webSearchPlugin.collectionReqItemSearchAndBasket", "Collection Request Item Search And Basket"));												
															}
														// coid hidden input present?
														if (($j('input[name="pluginCoid"]').length) && ($j('input[name="pluginCoid"]').val() != ''))
														{
															// set coid value
															WebSearchPlugin.coid = $j('input[name="pluginCoid"]').val();
															console.log("coid: "+WebSearchPlugin.coid);
														}
														// system company name hidden input present?
														if (($j('input[name="pluginSysComp"]').length) && ($j('input[name="pluginSysComp"]').val() != ''))
														{
															// set company name value
															WebSearchPlugin.sysComp = $j('input[name="pluginSysComp"]').val();
														}	
														// system company telephone number hidden input present?
														if (($j('input[name="pluginSysTel"]').length) && ($j('input[name="pluginSysTel"]').val() != ''))
														{
															// set telephone number value
															WebSearchPlugin.sysTel = $j('input[name="pluginSysTel"]').val();
														}
														// populate basket from recall hidden input present?
														if (($j('input[name="populateFromRecall"]').length) && ($j('input[name="populateFromRecall"]').val() != ''))
														{
															// set recall values
															WebSearchPlugin.recallPrefillOpts = $j('input[name="populateFromRecall"]').val().split(',');
															// now use these values to populate basket with recall items
															loadScript.loadScriptsDynamic(new Array('dwr/interface/recallitemservice.js'), true,
															{
																callback: function()
																{
																	// call dwr service to retrieve recall items
																	recallitemservice.getRecallItemsForRecallType(WebSearchPlugin.recallPrefillOpts[1], WebSearchPlugin.recallPrefillOpts[0], WebSearchPlugin.recallPrefillOpts[2], WebSearchPlugin.recallPrefillOpts[3],
																	{
																		callback: function(result)
																		{
																			// source items successful?
																			if (result.success)
																			{
																				// assign recall items to variable
																				var ris = result.results;
																				// add each recall item to basket
																				$j.each(ris, function(i, ri)
																				{
																					// get correct mfr
																					var mfr = '';																					
																					// check model mfr type
																					if (ri.instrument.mfr != null)
																					{
																						mfr = ri.instrument.mfr.name;
																					}
																					else
																					{
																						mfr = ri.instrument.model.mfr.name;
																					}
																					// call add item to basket method
																					WebSearchPlugin.itemToBasket(null, ri.instrument.plantid, ri.instrument.model.modelid, mfr, ri.instrument.model.model, ri.instrument.model.description.description, 1, ri.instrument.plantno, ri.instrument.serialno, ri.instrument.scrapped);
																				});
																			}
																			else
																			{
																				// display error message
																				$j.prompt(result.message);
																			}
																		}
																	});
																}
															});
														}
													}
												});




											},

								/**
								 * coid passed to the plugin from page
								 */
								coid: 				0,
								/**
								 * business subdiv id passed to the plugin from page
								 */
								businessSubdivid:	0,
								/**
								 * recall prefill options supplied from page via hidden input
								 */
								recallPrefillOpts:	[],
								/**
								 * holds the name of system company
								 */
								sysComp:			'Not Set',
								/**
								 * holds the telephone number of system company
								 */
								sysTel:				'Not Set',
								/**
								 * type of plugin we are creating (i.e. quotation or collection)
								 */
								pluginType: 		'',
								/**
								 * turnaround options supplied from page via hidden inputs
								 */
								turnOpts:			[],
								/**
								 * type of basket we are using (i.e. quotation request or collection request
								 */
								currentBasket:		'',
								/**
								 * indicates the current search type being used (i.e. instrument or model)
								 */
								searchType:			'instrument',
								/**
								 * main search div
								 */	
								searchDiv: 			'webSearchPlugin',
								/**
								 * container div
								 */
								container:			'websearch-container',
								/**
								 * search results div
								 */
								resultsDiv:			'websearch-results',				
								/**
								 * id of the manufacturer name input field
								 */
								mfrName:			'mfrName',
								/**
								 * id of the model name input field
								 */
								modelName:			'modelName',
								/**
								 * id of the Manufacturer id in put field
								 */
								mfrId:              'mfrId',
								/**
								 * id of the description name input field
								 */
								descName:			'descName',
								/**
								 * id of the description ID input field
								 */
								descId:			'descId',
								/**
								 * id of the barcode input field
								 */
								barcodeField:		'barcodeField',
								/**
								 * id of the plant number input field
								 */
								plantField:			'plantField',
								/**
								 * id of the serial number input field
								 */
								serialField:		'serialField',	
								/**
								 * name of the form element for selecting search type
								 */
								selector:			'searchtype',
								/**
								 * class name of the quotation request basket table
								 */
								quoteBasket: 		'quoteBasket',
								/**
								 * class name of the collection request basket table
								 */
								collectBasket:		'collectBasket',
								/**
								 * holds qty options for numeric select box (1-50)
								 */
								qtyOptions:			'',
								/**
								 * holds quote type options
								 */
								qtypeOptions: 		'<option value="" selected="selected">' + i18n.t("select", "Select") + '</option>' +
													'<option value="Calibration">Calibration</option>',
													//'<option value="Sales">Sales</option>' +
													//'<option value="Rental">Rental</option>',
								/**
								 * holds calibration type options
								 */
								ctypeOptions:		'',
								/**
								 * calibration type options array
								 */
								ctypeOptionsArray: 	new Array(),
								/**
								 * holds turnaround options
								 */
								turnOptions:		'',
								/**
								 * holds faulty options
								 */
								faultyOptions:		'',
								/**
								 * holds comment options
								 */
								commentOptions:		'',
								/**
								 * keeps count of how many items we have added to basket
								 */
								itemCount:			0,
								/**
								 * time value of last DWR call, checked against the current DWR call
								 */
								lastUpdate:			0,
								/**
								 * model table head content
								 */
								modelTableHead:		'<table class="webResults" cellpadding="0" cellspacing="0" border="0" summary="">' +
														'<thead>' +
															'<tr>' +
																'<th scope="col" class="Mdesc">' + i18n.t("mfr", "Instrument Model Name") + '</th>' +
																'<th scope="col" class="Mqty">' + i18n.t("qty", "Qty") + '</th>' +
																'<th scope="col" class="Madd">' + i18n.t("addToBasket", "Add To Basket") + '</th>' +
															'</tr>' +
														'</thead>' +
														'<tbody>',
								/**
								 * model table foot content
								 */
								modelTableFoot:			'</tbody>' +
													'</table>',
								/**
								 * instrument table head content
								 */
								instTableHead:		'<table class="webResults" cellpadding="0" cellspacing="0" border="0" summary="">' +
														'<thead>' +
															'<tr>' +
																'<th scope="col" class="Ibarcode">' + i18n.t("barcode", "Barcode") + '</th>' +
																'<th scope="col" class="Idesc">' + i18n.t("mfr", "Instrument Model Name") + '</th>' +
																'<th scope="col" class="Iplant">' + i18n.t("plantNo", "Plant No") + '</th>' +
																'<th scope="col" class="Iserial">' + i18n.t("serialNo", "Serial No") + '</th>' +																																		
																'<th scope="col" class="Iadd">' + i18n.t("addToBasket", "Add To Basket") + '</th>' +
															'</tr>' +
														'</thead>' +
														'<tbody>',
								/**
								 * instrument table foot content
								 */						
								instTableFoot:			'</tbody>' +
													'</table>',									
								/**
								 * creates one box containing all search fields for a model and instrument search and a second box
								 * containing the results container and search div.
								 */			
								drawSearch:		function()	
												{
		
									$j('div.' + WebSearchPlugin.searchDiv).append(
														'<div class="box1">' +															
															'<div id="pluginType" class="text-large text-center bold relative"></div>' +															
															'<div class="websearch-list">' +
																'<ol>' +
																	'<li style=" height: 40px; ">' +
																		'<label for="' + WebSearchPlugin.selector + '">' + i18n.t("searchTypeLabel", "Search Type:") + '</label>' +																		
																		// choose to search over models or instruments
																		'<input type="radio" name="' + WebSearchPlugin.selector + '" value="instrument" onclick=" WebSearchPlugin.changeSearchType(this.value); " checked="checked" /> ' + i18n.t("instrument", "Instrument") + '<br>' +
																		'<input type="radio" name="' + WebSearchPlugin.selector + '" value="model" onclick=" WebSearchPlugin.changeSearchType(this.value); " /> ' + i18n.t("model", "Instrument Model") +
																		'<input type="button" name="resetform" value="' + i18n.t("search", "search") + '" class="float-right" onclick=" WebSearchPlugin.runWebAjax(WebSearchPlugin.searchType); return false;  " />' +
	 																'</li>' +
																'</ol>' +
															'</div>' +
															'<div class="websearch-list">' +
																'<ol>' +
																	'<li style=" height: 40px; ">' +
																		'<label>&nbsp;</label>' +
																		'<span id="loading" class="hid">' +
																			'<img src="../img/web/searching.gif" width="128" height="15" alt="' + i18n.t("loadingResults", "Loading results") + '" title="' + i18n.t("web:webSearchPlugin.search.loadingresults", "Loading results") + '" />' +
	 																	'</span>' +
																		'<span>&nbsp;</span>' +
																	'</li>' +
																'</ol>' +
															'</div>' +
															'<div class="clear-0"></div>' +
															'<div class="websearch-list">' +															
																'<ol>' +
																    '<li>' +
                                                                    	'<label for="' + WebSearchPlugin.descName + '">' + i18n.t("subFamilyLabel", "Sub Family:") + '</label>' +
                                                                    	// create and append description input in which the user will search for models/instruments
                                                                    	'<input type="text" name="' + WebSearchPlugin.descName + '" id="' + WebSearchPlugin.descName + '"  autocomplete="off" />' +
                                                                    	'<input type="hidden" name="' + WebSearchPlugin.descId + '" id="' + WebSearchPlugin.descId + '"  autocomplete="off" />' +
                                                                    '</li>' +
																	'<li>' +
																		'<label for="' + WebSearchPlugin.mfrName + '">' + i18n.t("manufacturerLabel", "Brand:") + '</label>' +
																		// create and append manufacturer input in which the user will search for models/instruments
																		'<input type="text" name="' + WebSearchPlugin.mfrName + '" id="' + WebSearchPlugin.mfrName + '"  autocomplete="off" />' +
                                                                    	'<input type="hidden" name="' + WebSearchPlugin.mfrId + '" id="' + WebSearchPlugin.mfrId + '"  autocomplete="off" />' +
																	'</li>' +
																	'<li>' +
																		'<label for="' + WebSearchPlugin.modelName + '">' + i18n.t("modelLabel", "Model:") + '</label>' +
																		// create and append model input in which the user will search for models/instruments
																		'<input type="text" name="' + WebSearchPlugin.modelName + '" autocomplete="off" />' +
																	'</li>' +

																'</ol>' +																
															'</div>' +
															'<div class="websearch-list">' +
																'<ol>' +																
																	'<li>' +
																		'<label for="' + WebSearchPlugin.barcodeField + '">' + i18n.t("barcodeLabel", "Barcode:") + '</label>' +
																		// create and append barcode input in which the user will search for instruments
																		'<input type="text" name="' + WebSearchPlugin.barcodeField + '"  autocomplete="off" />' +
																	'</li>' +
																	'<li>' +
																		'<label for="' + WebSearchPlugin.plantField + '">' + i18n.t("plantNoLabel", "Plant No:") + '</label>' +
																		// create and append plant number input in which the user will search for instruments
																		'<input type="text" name="' + WebSearchPlugin.plantField + '"  autocomplete="off" />' +
																	'</li>' +
																	'<li>' +
																		'<label for="' + WebSearchPlugin.serialField + '">' + i18n.t("serialNoLabel", "Serial No:") + '</label>' +
																		// create and append serial number input in which the user will search for instruments
																		'<input type="text" name="' + WebSearchPlugin.serialField + '" onkeyup=" autocomplete="off" />' +
																	'</li>' +
																'</ol>' +																
															'</div>' +
															'<div class="clear-0"></div>' +
														'</div>' +
														'<div class="box2">' +																													
															'<div class="' + WebSearchPlugin.container + '" style=" display: none; ">' +															
																// create and append the div in which model results will be displayed
																'<div class="' + WebSearchPlugin.resultsDiv + '">' +																
																	// This div is used to diplay the ajax results
																'</div>' +
															'</div>' +
														'</div>');
																													
												},								
												
								/**
								 * this method creates the quotation basket
								 */			
								drawQuoteBasket:	function()	
													{
														$j('div.' + WebSearchPlugin.searchDiv + ' div.box2').append(
															'<table class="' + WebSearchPlugin.quoteBasket + '" cellpadding="0" cellspacing="0" border="0" summary="">' +
																'<thead>' +
																	'<tr>' +
																		'<th colspan="8">' + i18n.t("web:webSearchPlugin.quotationReqBasket", "Quotation Request Basket") + '</th>' +
																	'</tr>' +
																	'<tr>' +
																		'<th class="QBdesc" scope="col">' + i18n.t("itemDescription", "Item Description") + '</th>' +																		
																		'<th class="QBqty" scope="col">' + i18n.t("qty", "Qty") + '</th>' +
																		'<th class="QBplant" scope="col">' + i18n.t("plantNo", "Plant No") + '</th>' +
																		'<th class="QBserial" scope="col">' + i18n.t("serialNo", "Serial No") + '</th>' +
																		'<th class="QBqtype" scope="col">' + i18n.t("quoteType", "Quote Type") + '</th>' +																		
																		'<th class="QBctype" scope="col">' + i18n.t("calType", "Cal Type") + '</th>' +
																		'<th class="QBcomment" scope="col">' + i18n.t("comment", "Comment") + '</th>' +
																		'<th class="QBdel" scope="col">' + i18n.t("del", "Del") + '</th>' +
																	'</tr>' +
																'</thead>' +
															'</table>' +
															'<table class="' + WebSearchPlugin.quoteBasket + '" cellpadding="0" cellspacing="0" border="0" summary="">' +
																'<tbody>' +
																	'<tr>' +
																		'<td colspan="8" class="text-center">' + i18n.t("web:webSearchPlugin.noItemsInQuoteReqBasket", "You currently have no items in your quote request basket") + '</td>' +
																	'</tr>' +
																'</tbody>' +
															'</table>' +
															'<table class="' + WebSearchPlugin.quoteBasket + '" cellpadding="0" cellspacing="0" border="0" summary="">' +
																'<tfoot>' +
																	'<tr>' +
																		'<td colspan="8">&nbsp;</td>' +
																	'</tr>' +
																'</tfoot>' +
															'</table>');
													},
													
								/**
								 * this method creates the collection basket
								 */			
								drawCollectBasket:	function()	
													{
														$j('div.' + WebSearchPlugin.searchDiv + ' div.box2').append(
															'<table class="' + WebSearchPlugin.collectBasket + '" cellpadding="0" cellspacing="0" border="0" summary="">' +
																'<thead>' +
																	'<tr>' +
																		'<th colspan="5">' + i18n.t("web:webSearchPlugin.collectionReqBasket", "Collection Request Basket") + '</th>' +
																	'</tr>' +
																	'<tr>' +
																		'<th class="CBdesc" scope="col">' + i18n.t("instrumentmodelname", "Instrument Model Name") + '</th>' +
																		'<th class="CBqty" scope="col">' + i18n.t("qty", "Qty") + '</th>' +
																		'<th class="CBplant" scope="col">' + i18n.t("plantNo", "Plant No") + '</th>' +
																		'<th class="CBserial" scope="col">' + i18n.t("serialNo", "Serial No") + '</th>' +
																		'<th class="CBdel" scope="col">' + i18n.t("del", "Del") + '</th>' +
																	'</tr>' +
																'</thead>' +
															'</table>' +
															'<table class="' + WebSearchPlugin.collectBasket + '" cellpadding="0" cellspacing="0" border="0" summary="">' +
																'<tbody>' +
																	'<tr>' +
																		'<td colspan="5" class="text-center">' + i18n.t("web:webSearchPlugin.noItemsInCollectionReqbasket", "You currently have no items in your collection request basket") + '</td>' +
																	'</tr>' +
																'</tbody>' +
															'</table>' +
															'<table class="' + WebSearchPlugin.collectBasket + '" cellpadding="0" cellspacing="0" border="0" summary="">' +
																'<tfoot>' +
																	'<tr>' +
																		'<td colspan="5">&nbsp;</td>' +
																	'</tr>' +
																'</tfoot>' +
															'</table>');
													},
																												
								/**
								 * This method retrieves input values from the page and then calls a dwr service to retrieve items depending on
								 * the search type passed, this can be either 'instrument' or 'model'. Depending on the search type a callback
								 * method is called to display the items.
								 * 
								 * @param {String} searchtype this is either 'instrument' or 'model'
								 */			
								runWebAjax: 	function (searchtype)		
												{	
													// holds total length of all inputs used for search
													var inputsLength = '';
													var barcode = '';
													var plantno = '';
													var serialno = '';
													// get value from the manufacturer input field
													var mfrname = $j('input[name="' + WebSearchPlugin.mfrName + '"]').val();
													var mfrid = $j('input[name="' + WebSearchPlugin.mfrId + '"]').val();
													// get value from the model input field
													var modelname = $j('input[name="' + WebSearchPlugin.modelName + '"]').val();
													// get value from the description input fields
													var descname = $j('input[name="' + WebSearchPlugin.descName + '"]').val();
													var descid = $j('input[name="' + WebSearchPlugin.descId + '"]').val();
													
												
													// add to total length
													inputsLength += (mfrname + modelname + descname);
													// search type instrument so get extra search parameters
													if (searchtype == 'instrument')
													{
														// get value from the barcode input field
														barcode = $j('input[name="' + WebSearchPlugin.barcodeField + '"]').val();
														// get value from the plant number input field
														plantno = $j('input[name="' + WebSearchPlugin.plantField + '"]').val();
														// get value from the serial number input field
														serialno = $j('input[name="' + WebSearchPlugin.serialField + '"]').val();
														// add to total length
														inputsLength += (plantno + serialno);
													}
														
													// get current timestamp to pass with ajax results
													var currentTime = new Date().getTime();
																																												
													// total of all input fields used greater than one?
													if ((inputsLength.length > 1) || (barcode.length > 2))
													{
														// display loading image
														$j('span#loading').removeClass().next().addClass('hid');
														// search type 'instrument'?	
														if (searchtype == 'instrument')
														{
															instrumservice.websearchInstrumentsHQL(WebSearchPlugin.coid, mfrname, modelname, descname, descid, barcode, plantno, serialno,
															{
																callback:function(instrums)
																{	
																	// call method to display instrument results					
																	WebSearchPlugin.webInstrumentCallback(instrums, currentTime);
																}
															});
														}
														else
														{
															instrumentmodelservice.searchInstrumentModelsDWR(mfrname, modelname, descname, descid, mfrid,
															{
																callback:function(models)
																{
																	// call method to display model results
																	WebSearchPlugin.webModelCallback(models, currentTime);
																}
															});
														}														
													}
													else
													{
														// remove all previously displayed models
														$j('div.' + WebSearchPlugin.resultsDiv).empty();
														// hide container
														$j('div.' + WebSearchPlugin.container).css({display: 'none'});
													}																													
												},							

								/**
								 * this method displays all results retrieved from the model dwr service method
								 *
								 * @param {Array} models is an array of model objects returned from the server
								 * @param {Date/Time} currentUpdate is the time of data retrieval from the server
								 */			
								webModelCallback: 		function(models, currentUpdate)
														{
															// this is a new search
															if (currentUpdate > WebSearchPlugin.lastUpdate)
															{
																// update value in the 'lastUpdate' literal
																WebSearchPlugin.lastUpdate = currentUpdate;															
																// models length is greater than zero																															
																if(models.length > 0)
																{					
																	// remove all previously displayed models and display container
																	$j('div.' + WebSearchPlugin.resultsDiv).empty().parent().css({display: 'block'});
																	// variable to hold all models returned
																	var content = '';
																	// add table row for each model returned 
																	$j.each(models, function(i)
																	{																				
																		content += 	'<tr>' +
																						'<td>';
																							var mfr = '';
																							if(models[i].mfr != 'System Generic')
																							{
																								mfr = models[i].mfr;
																							}
																				content += 	models[i].fullModelName +
																						'</td>' +
																						'<td class="text-center">' +
																							'<select>' +
																								WebSearchPlugin.qtyOptions +
																							'</select>' +
																						'</td>' +
																						'<td class="text-center">' +
																							'<button type="button" class="transparent" onclick=" WebSearchPlugin.itemToBasket($j(this), \'\', ' + models[i].modelid + ', \'' + encodeURIComponent(models[i].mfr) + '\', \'' + encodeURIComponent(models[i].model) + '\', \'' + encodeURIComponent(models[i].description) + '\', $j(this).parent().prev().find(\'select\').val(), \'\', \'\', null,\'' + models[i].fullModelName + '\'); return false; ">' +
																								'<img src="../img/icons/add.png" width="16" height="16" alt="' + i18n.t("addToBasket", "Add To Basket") + '" title="' + i18n.t("addToBasket", "Add To Basket") + '"/>' +
																							'</a>' +
																						'</td>' +
																					'</tr>';			
																	});																	
																	// append all models returned to table
																	$j('div.' + WebSearchPlugin.resultsDiv).append(	WebSearchPlugin.modelTableHead +
																														content +
																													WebSearchPlugin.modelTableFoot);
																	// hide loading image
																	$j('span#loading').addClass('hid').next().removeClass();																																																																																																																	
																}
																else
																{
																	// remove all previously displayed models, append no result message, display container
																	$j('div.' + WebSearchPlugin.resultsDiv).empty()																											
																											.append(WebSearchPlugin.modelTableHead +
																														'<tr>' +
																															'<td colspan="3" class="text-center bold">' + i18n.t("web:webSearchPlugin.noModelsFound", "No Models Found Matching Your Criteria") + '</td>' +
																														'</tr>' +
																													WebSearchPlugin.modelTableFoot)
																											.parent()
																											.css({display: 'block'});
																	// hide loading image
																	$j('span#loading').addClass('hid').next().removeClass();															
																}	
															}
														},
														
								/**
								 * this method displays all results retrieved from the instrument dwr service method
								 *
								 * @param {Array} instrums is an array of instrument objects returned from the server
								 * @param {Date/Time} currentUpdate is the time of data retrieval from the server
								 */			
								webInstrumentCallback: 		function(instrums, currentUpdate)
															{	
																// this is a new search
																if (currentUpdate > WebSearchPlugin.lastUpdate)
																{
																	// update value in the 'lastUpdate' literal
																	WebSearchPlugin.lastUpdate = currentUpdate;															
																	// instrument results length is greater than zero?																														
																	if(instrums.length > 0)
																	{					
																		// remove all previously displayed instruments and display container
																		$j('div.' + WebSearchPlugin.resultsDiv).empty().parent().css({display: 'block'});
																		// variable to hold all instruments returned
																		var content = '';																		
																		// add table row for each instrument returned 
																		$j.each(instrums, function(i, inst)
																		{																													
																			content += 	'<tr>' +
																							'<td>' +
																								'<span';
																								if (inst.scrapped == true)
																								{
																									content +=	' class="strike"';
																								}
																					content += 	'>' +
																									inst.plantid +
																								'</span>' +
																								// this input is purely for indicating if an instrument has already been added to the basket
																								'<input type="hidden" name="inBasketIndicator" value="' + inst.plantid + '" />' + 
																							'</td>' +
																							'<td>';																								
																								if (inst.scrapped == true)
																								{
																									content += 	'<img src="../img/icons/bin_closed.png" width="16" height="16" class="image_inl" alt="' + i18n.t("web:webSearchPlugin.unitBER", "Unit B.E.R") + '" title="' + i18n.t("web:webSearchPlugin.unitBER", "Unit B.E.R") + '" /> ';
																								}
																					content +=	'<span';
																								if (inst.scrapped == true)
																								{
																									content +=	' class="strike"';
																								}
																					content += 	'>' +
																					 				inst.fullModelName +
																					 			'</span>' +
																							'</td>' +
																							'<td>' + inst.plantno + '</td>' +
																							'<td>' + inst.serialno + '</td>' +
																							'<td class="text-center">' +
																								'<button class="transparent" type="button" onclick=" WebSearchPlugin.itemToBasket($j(this), ' + inst.plantid + ', ' + inst.modelid + ', \'' + encodeURIComponent(inst.mfr) + '\', \'' + encodeURIComponent(inst.model) + '\', \'' + encodeURIComponent(inst.description) + '\', 1, \'' + inst.plantno + '\', \'' + inst.serialno + '\', ' + inst.scrapped + ', \'' + inst.fullModelName + '\'); return false; ">' +
																									'<img src="../img/icons/add.png" width="16" height="16" alt="' + i18n.t("addToBasket", "Add To Basket") + '" title="' + i18n.t("addToBasket", "Add To Basket") + '" />' +
																								'</a>' +
																							'</td>' +
																						'</tr>';			
																		});																		
																		// append all instruments returned to table
																		$j('div.' + WebSearchPlugin.resultsDiv).append(	WebSearchPlugin.instTableHead +
																															content +
																														WebSearchPlugin.instTableFoot);																													
																		// loop through basket items and find all plantids	
																		$j('table.' + WebSearchPlugin.currentBasket + ' input[name="plantid"]').each(function(i, n)
																		{	
																			// in basket indicator input with value of plant id in basket?
																			if ($j('div.' + WebSearchPlugin.resultsDiv + ' table input[name="inBasketIndicator"][value="' + n.value + '"]').length)
																			{
																				// use in basket indicator input to move up dom tree and find the anchor which holds add image and replace with in basket image
																				$j('div.' + WebSearchPlugin.resultsDiv + ' table input[name="inBasketIndicator"][value="' + n.value + '"]')
																						.parent()
																						.siblings('td:last')
																						.find('a')
																						.attr('disabled', 'disabled')
																						.empty()
																						.append('<img src="../img/icons/items.png" width="16" height="16" alt="' + i18n.t("web:webSearchPlugin.instrAddedToBasket", "Instrument Added To Basket") + '" title="' + i18n.t("web:webSearchPlugin.instrAddedToBasket", "Instrument Added To Basket") + '" />');
																			}
																		});																		
																		// hide loading image
																		$j('span#loading').addClass('hid').next().removeClass();																																																																																													
																	}
																	else
																	{																		
																		// remove all previously displayed instruments, append no result message, display container
																		$j('div.' + WebSearchPlugin.resultsDiv).empty()																												
																												.append(WebSearchPlugin.instTableHead +
																															'<tr>' +
																																'<td colspan="5" class="text-center bold">' + i18n.t("web:webSearchPlugin.noInstrFound", "No Instruments Found Matching Your Criteria") + '</td>' +
																															'</tr>' +
																														WebSearchPlugin.instTableFoot)
																												.parent()
																												.css({display: 'block'});
																		// hide loading image
																		$j('span#loading').addClass('hid').next().removeClass();																	
																	}	
																}
															},
								
								/**
								 * this method just decides whether we are using a quotation or collection request plugin and
								 * directs the item to the correct basket
								 *
								 * @param {Object} anchor the anchor object that has been clicked
								 * @param {Integer} plantid id of instrument if it is one
								 * @param {Integer} modelid id of the model to add
								 * @param {String} mfr name of the selected item
								 * @param {String} model model of the selected item
								 * @param {String} desc description of the selected item
								 * @param {String} qty selected qty of item
								 * @param {String} plantno plant number of the selected item
								 * @param {String} serialno serial number of the selected item
								 * @param {Boolean} scrapped is the item scrapped or null if coming from model search
								 */										
								itemToBasket:		function(anchor, plantid, modelid, mfr, model, desc, qty, plantno, serialno, scrapped, fullmodelname)
													{													
														// anchor has been disabled?
														if ((anchor == null) || (!$j(anchor).attr('disabled')))
														{
															// this is a quotation search?
															if (WebSearchPlugin.pluginType == 'Quotation')
															{
																// add quote item to basket
																WebSearchPlugin.quoteItemToBasket(anchor, plantid, modelid, mfr, model, desc, qty, plantno, serialno, scrapped, fullmodelname);
															}
															else
															{
																// add collect item to basket
																WebSearchPlugin.collectItemToBasket(anchor, plantid, modelid, mfr, model, desc, qty, plantno, serialno, scrapped, fullmodelname);
															}
															// add one to basket count
															webSearchPluginBasketCount++;
														}
														else
														{
															// anchor is disabled which means instrument is already in basket
															return;
														}														
													},
													
								/**
								 * this method adds the selected item to the quotation request basket
								 *
								 * @param {Object} anchor the anchor object that has been clicked
								 * @param {Integer} plantid id of instrument if it is one
								 * @param {Integer} modelid id of the model to add
								 * @param {String} mfr name of the selected item
								 * @param {String} model model of the selected item
								 * @param {String} desc description of the selected item
								 * @param {String} qty selected qty of item
								 * @param {String} plantno plant number of the selected item
								 * @param {String} serialno serial number of the selected item
								 * @param {Boolean} scrapped is the item scrapped or null if coming from model search
								 */			
								quoteItemToBasket:	function(anchor, plantid, modelid, mfr, model, desc, qty, plantno, serialno, scrapped, fullmodelname)
													{																															
														// is first table in basket message?
														if ($j('table.' + WebSearchPlugin.quoteBasket + ':first').next().find('tbody tr td').length < 2)
														{
															// remove message table
															$j('table.' + WebSearchPlugin.quoteBasket + ':first').next().remove();
														}
														var classname = 'odd';
														// tables in basket even?
														if (($j('table.' + WebSearchPlugin.quoteBasket).length % 2) == 0)
														{
															classname = 'even';
														}
														// create content for quotation basket table
														var content = 	'<table class="' + WebSearchPlugin.quoteBasket + '" cellpadding="0" cellspacing="0" border="0" summary="">' +
																			'<tbody>';
																				if ((scrapped != null) && (scrapped))
																				{
																					content += 	'<tr class="' + classname + ' attention bold">' +
																									'<td colspan="5" class="messageRow">' +
																									i18n.t("web:webSearchPlugin.itemNoteBER", "Item Note: Please be aware the following item has a status of B.E.R and may no longer be in service") +	
																									'</td>' +
																								'</tr>';
																				}
																	content +=	'<tr class="' + classname + '">' +
																					'<td class="QBdesc">';
																						if ((scrapped != null) && (scrapped))		
																						{
																							content += 	'<img src="../img/icons/bin_closed.png" width="16" height="16" class="image_inl" alt="' + i18n.t("web:webSearchPlugin.unitBER", "Unit B.E.R") + '" title="' + i18n.t("web:webSearchPlugin.unitBER", "Unit B.E.R") + '" /> ';
																						}																							
																			content +=	'<span';
																							if ((scrapped != null) && (scrapped))
																							{
																								content +=	' class="strike"';
																							}
																				content += 	'>' + fullmodelname + '' +
																						'</span>' +
																						'<input type="hidden" name="requestItems[' + WebSearchPlugin.itemCount + '].plantid" value="' + plantid + '" />' +
																						'<input type="hidden" name="requestItems[' + WebSearchPlugin.itemCount + '].modelid" value="' + modelid + '" />' +
																					'</td>' +																			
																					'<td class="QBqty">' +
																						qty + 
																						'<input type="hidden" name="requestItems[' + WebSearchPlugin.itemCount + '].qty" value="' + qty + '" />' +
																					'</td>' +
																					'<td class="QBplant">' + 
																						plantno + 
																						'<input type="hidden" name="requestItems[' + WebSearchPlugin.itemCount + '].plantno" value="' + plantno + '" />' +
																					'</td>' +
																					'<td class="QBserial">' + 
																						serialno + 
																						'<input type="hidden" name="requestItems[' + WebSearchPlugin.itemCount + '].serialno" value="' + serialno + '" />' +
																					'</td>' +
																					'<td class="QBqtype">' +
																						'<select name="requestItems[' + WebSearchPlugin.itemCount + '].quotetype" style="width: 76px">' +
																							WebSearchPlugin.qtypeOptions +
																						'</select>';
																						if (WebSearchPlugin.itemCount < 1)
																						{
																							content += 	'<a href="#" class="imagelink copydown" onclick=" WebSearchPlugin.copySelectedOption(\'down\', \'select\', $j(this).siblings(\'select\')); return false; " title="' + i18n.t("web:webSearchPlugin.copyDownToAllItems", "Copy down to all items") + '">&nbsp;</a>';
																						}
																		content += 	'</td>' +																					
																					'<td class="QBctype">' +
																						'<select name="requestItems[' + WebSearchPlugin.itemCount + '].caltypeid" style="width: 150px">' +
																							'<option value="" selected="selected">' + i18n.t("select", "Select") + '</option>' +
																							WebSearchPlugin.ctypeOptions +
																						'</select>';
																						if (WebSearchPlugin.itemCount < 1)
																						{
																							content += 	'<a href="#" class="imagelink copydown" onclick=" WebSearchPlugin.copySelectedOption(\'down\', \'select\', $j(this).siblings(\'select\')); return false; " title="' + i18n.t("web:webSearchPlugin.copyDownToAllItems", "Copy down to all items") + '">&nbsp;</a>';
																						}
																		content +=	'</td>' +
																					'<td class="QBcomment">' +
																						'<select name="requestItems[' + WebSearchPlugin.itemCount + '].comment" onchange=" WebSearchPlugin.checkComment(this.value, $j(this).parent().parent()); return false; ">' +
																							WebSearchPlugin.commentOptions +
																						'</select>' +
																					'</td>' +
																					'<td class="QBdel">' +
																						'<a href="#" onclick=" WebSearchPlugin.removeItem(\'' + plantid + '\', $j(this).parent().parent().parent().parent()); return false; ">' +
																							'<img src="../img/icons/delete.png" width="16" height="16" alt="' + i18n.t("deleteToBasket", "Delete To Basket") + '" title="' + i18n.t("deleteToBasket", "Delete To Basket") + '" /> ' +
																						'</a>' +
																					'</td>' +
																				'</tr>' +
																				'<tr class="hid ' + classname + '">' +
																					'<td colspan="8">' +
																						i18n.t("commentLabel", "Comment:") + '&nbsp;&nbsp;<input type="text" class="comment" name="requestItems[' + WebSearchPlugin.itemCount + '].commentDesc" value="" />' +
																					'</td>' +
																				'</tr>' +
																			'</tbody>' +
																		'</table>';																		
														// append quotation basket content to page
														$j('table.' + WebSearchPlugin.quoteBasket + ':last').before(content);
														// update the item count
														WebSearchPlugin.itemCount++;
														// is search type instrument?
														if((anchor != null) && (WebSearchPlugin.searchType == 'instrument'))
														{
															// disable add anchor and change image to item in basket
															$j(anchor).attr('disabled', 'disabled')
																		.empty()
																		.append('<img src="../img/icons/items.png" width="16" height="16"' + i18n.t("web:webSearchPlugin.instrAddedToBasket", "Instrument Added To Basket") + '" title="' + i18n.t("web:webSearchPlugin.instrAddedToBasket", "Instrument Added To Basket") + '" />');
														}								
													},
													
								/**
								 * this method adds the selected item to the collection request basket
								 *
								 * @param {Object} anchor the anchor object that has been clicked
								 * @param {Integer} plantid id of instrument if it is one
								 * @param {Integer} modelid id of the model to add
								 * @param {String} mfr name of the selected item
								 * @param {String} model model of the selected item
								 * @param {String} desc description of the selected item
								 * @param {String} qty selected qty of item
								 * @param {String} plantno plant number of the selected item
								 * @param {String} serialno serial number of the selected item
								 * @param {Boolean} scrapped is the item scrapped or null if coming from model search
								 */			
								collectItemToBasket:	function(anchor, plantid, modelid, mfr, model, desc, qty, plantno, serialno, scrapped)
														{	
															// is first table in basket message?
															if ($j('table.' + WebSearchPlugin.collectBasket + ':first').next().find('tbody tr td').length < 2)
															{
																// remove message table
																$j('table.' + WebSearchPlugin.collectBasket + ':first').next().remove();
															}
															var classname = 'odd';
															// tables in basket even?
															if (($j('table.' + WebSearchPlugin.collectBasket).length % 2) == 0)
															{
																classname = 'even';
															}																																		
															// create content for collection basket table
															var content = 	'<table class="' + WebSearchPlugin.collectBasket + '" cellpadding="0" cellspacing="0" border="0" summary="">' +
																				'<tbody>';
																					if ((scrapped != null) && (scrapped))
																					{
																						content += 	'<tr class="' + classname + ' attention bold">' +
																										'<td colspan="5" class="messageRow">' +
																											i18n.t("web:webSearchPlugin.itemNoteBER", "Item Note: Please be aware the following instrument has a status of B.E.R and may no longer be in service") +
																										'</td>' +
																									'</tr>';
																					}
																		content +=	'<tr class="' + classname + '">' +
																						'<td class="CBdesc">';
																							if ((scrapped != null) && (scrapped))
																							{
																								content += 	'<img src="../img/icons/bin_closed.png" width="16" height="16" class="image_inl" alt="' + i18n.t("web:webSearchPlugin.unitBER", "Unit B.E.R") + '" title="' + i18n.t("web:webSearchPlugin.unitBER", "Unit B.E.R") + '" /> ';
																							}
																				content +=	'<span';
																								if ((scrapped != null) && (scrapped))
																								{
																									content +=	' class="strike"';
																								}
																					content += 	'>' + decodeURIComponent(desc) + ' - ' + decodeURIComponent(mfr) + ' - ' + decodeURIComponent(model) +  '' +
																							'</span>' +
																							'<input type="hidden" name="requestItems[' + WebSearchPlugin.itemCount + '].plantid" value="' + plantid + '" />' +
																							'<input type="hidden" name="requestItems[' + WebSearchPlugin.itemCount + '].modelid" value="' + modelid + '" />' +
																						'</td>' +																			
																						'<td class="CBqty">' +
																							qty + 
																							'<input type="hidden" name="requestItems[' + WebSearchPlugin.itemCount + '].qty" value="' + qty + '" />' +
																						'</td>' +
																						'<td class="CBplant">' + 
																							plantno +
																							'<input type="hidden" name="requestItems[' + WebSearchPlugin.itemCount + '].plantno" value="' + plantno + '" />' +
																						'</td>' +
																						'<td class="CBserial">' + 
																							serialno + 
																							'<input type="hidden" name="requestItems[' + WebSearchPlugin.itemCount + '].serialno" value="' + serialno + '" />' +
																						'</td>' +																																											
																						'<td class="CBdel">' +
																							'<a href="#" onclick=" WebSearchPlugin.removeItem(\'' + plantid + '\', $j(this).parent().parent().parent().parent()); return false; ">' +
																								'<img src="../img/icons/delete.png" width="16" height="16" alt="' + i18n.t("deleteBasketItem", "Delete Basket Item") + '" />' +
																							'</a>' +
																						'</td>' +
																					'</tr>' +
																					'<tr class="' + classname + '">' +
																						'<td class="child_container" colspan="5">' +
																							'<table class="child_table" cellpadding="0" cellspacing="0" border="0">' +
																								'<tbody>' +
																									'<tr class="' + classname + '">' +
																										'<th class="CBctype_title">' + i18n.t("calTypeLabel", "Service Type:") + '</th>' + 
																										'<td class="CBctype">' +
																											'<input type="hidden" name="requestItems[' + WebSearchPlugin.itemCount + '].supported" value="true" />' +
																											'<input type="hidden" name="requestItems[' + WebSearchPlugin.itemCount + '].supportDesc" value="" />' +
																											'<select name="requestItems[' + WebSearchPlugin.itemCount + '].caltypeid" class="default" onchange=" WebSearchPlugin.checkCalibrationSupport($j(this).find(\'option:selected\'), $j(this).parent().parent()); return false; ">' +
																												'<option value="" class="default" selected="selected">' + i18n.t("select", "Select") + '</option>' +
																												WebSearchPlugin.ctypeOptions;																								
																							content +=		'</select>';
																											if (WebSearchPlugin.itemCount < 1)
																											{
																												content += 	'<a href="#" class="imagelink copydown" onclick=" WebSearchPlugin.copySelectedOption(\'down\', \'select\', $j(this).siblings(\'select\')); return false; " title="' + i18n.t("web:webSearchPlugin.copyDownToAllItems", "Copy down to all items") + '">&nbsp;</a>';
																											}
																							content += 	'</td>' +
																										'<th class="CBcturn_title">';
																										if (WebSearchPlugin.itemCount < 1)
																										{
																											content += '<a href="#" class="turnHelpTrigger"><img src="../img/icons/help.png" width="16" height="16" /></a>';
																										}
																							content += 	'&nbsp;' + i18n.t("turnaroundLabel", "Turnaround:") + '</th>' +
																										'<td class="CBcturn">' +
																											'<select name="requestItems[' + WebSearchPlugin.itemCount + '].turn">' +
																												WebSearchPlugin.turnOptions +
																											'</select>';
																											if (WebSearchPlugin.itemCount < 1)
																											{
																												content += 	'<a href="#" class="imagelink copydown" onclick=" WebSearchPlugin.copySelectedOption(\'down\', \'select\', $j(this).siblings(\'select\')); return false; " title="' + i18n.t("web:webSearchPlugin.copyDownToAllItems", "Copy down to all items") + '">&nbsp;</a>';
																											}
																							content += 	'</td>' +
																										'</td>' +
																										'<th class="CBfaulty_title">' + i18n.t("faultyLabel", "Faulty:") + '</th>' +
																										'<td class="CBfaulty">' +
																											'<select name="requestItems[' + WebSearchPlugin.itemCount + '].faulty" onchange=" WebSearchPlugin.checkFaulty(this.value, $j(this).closest(\'table\').parent().parent()); return false; ">' + 
																												WebSearchPlugin.faultyOptions +
																											'</select>';
																											if (WebSearchPlugin.itemCount < 1)
																											{
																												content += 	'<a href="#" class="imagelink copydown" onclick=" WebSearchPlugin.copySelectedOption(\'down\', \'select\', $j(this).siblings(\'select\')); return false; " title="' + i18n.t("web:webSearchPlugin.copyDownToAllItems", "Copy down to all items") + '">&nbsp;</a>';
																											}
																							content += 	'</td>' +
																										'</td>' +
																										'<th class="CBitempo_title">' + i18n.t("itemPOLabel", "Item PO:") + '</th>' +
																										'<td class="CBitempo">' +
																											'<input type="text" name="requestItems[' + WebSearchPlugin.itemCount + '].itempo" value="" />';
																											if (WebSearchPlugin.itemCount < 1)
																											{
																												content += 	'<a href="#" class="imagelink copydown" onclick=" WebSearchPlugin.copySelectedOption(\'down\', \'input\', $j(this).siblings(\'input\')); return false; " title="' + i18n.t("web:webSearchPlugin.copyDownToAllItems", "Copy down to all items") + '">&nbsp;</a>';
																											}
																							content += 	'</td>' +
																										'<td class="CBitemcomment">' +
																											'<a href="#" class="imagelink itemcomment" onclick=" WebSearchPlugin.toggleItemComment($j(this).closest(\'table\').parent().parent()); return false; " title="' + i18n.t("web:webSearchPlugin.makeItemNote", "Make a note about this item") + '">&nbsp;</a>' +
																										'</td>' +
																									'</tr>' +
																								'</tbody>' +
																							'</table>' +
																						'</td>' +
																					'</tr>' +
																					'<tr class="hid ' + classname + '">' +
																						'<td colspan="8">' +
																							i18n.t("faultDescriptionLabel", "Fault Description:") + '&nbsp;&nbsp;<input type="text" class="faulty" name="requestItems[' + WebSearchPlugin.itemCount + '].faultyDesc" value="" />' +
																						'</td>' +
																					'</tr>' +
																					'<tr class="hid ' + classname + '">' +
																						'<td colspan="8">' +
																							i18n.t("itemCommentLabel", "Item Comment:") + '&nbsp;&nbsp;<input type="text" class="faulty" name="requestItems[' + WebSearchPlugin.itemCount + '].commentDesc" value="" />' +
																						'</td>' +
																					'</tr>' +
																				'</tbody>' +
																			'</table>';
																			if (WebSearchPlugin.itemCount < 1)
																			{
																				content += 	'<div id="turnHelp" class="tooltip">' +
																								'<div>' +
																									i18n.t("web:webSearchPlugin.noItemsInBasket", 
																									"<strong>FAST TRACK</strong><br />" +
																									"<span>Turnaround three (3) working days from receipt of equipment, subject to additional charge of 50% of unit cost plus return carriage.</span><br />" +
																									"<strong>SAME DAY</strong><br />" +
																									"<span>Turnaround one (1) working day from receipt of equipment, subject to additional charge of 100% of unit cost. Equipment must be pre-booked and carriage costs will apply.</span><br />" +
																									"<strong>STANDARD - UKAS/Traceable to National Standards</strong><br />" +
																									"<span>Turnaround seven (7) working days from receipt of equipment.</span><br />") +
																								'</div>' +
																							'</div>';
																			}
															// append collection basket content to page
															$j('table.' + WebSearchPlugin.collectBasket + ':last').before(content);
															// is search type instrument?
															if((anchor != null) && (WebSearchPlugin.searchType == 'instrument'))
															{
																// disable add anchor and change image to item in basket
																$j(anchor).attr('disabled', 'disabled')
																			.empty()
																			.append('<img src="../img/icons/items.png" width="16" height="16"' + i18n.t("web:webSearchPlugin.instrAddedToBasket", "Instrument Added To Basket") + '" title="' + i18n.t("web:webSearchPlugin.instrAddedToBasket", "Instrument Added To Basket") + '" />');
															}
															if (WebSearchPlugin.itemCount < 1)
															{
																// initialise tooltip
																$j('.turnHelpTrigger').tooltip(
																{ 
		 														    // each trashcan image works as a trigger 
															        tip: '#turnHelp', 
															        // custom positioning 
															        position: ['top', 'center'],
																	// use a simple show/hide effect 
															        effect: 'slideup',
																	// there is no delay when the mouse is moved out of the trigger 
															        delay: 0,
																	// change trigger opacity slowly to 0.8 
																    onBeforeShow: function() {
																		// alter default width of tooltip
																        $j('#turnHelp').width(400);
																    } 
															    });
															}
															// update the item count
															WebSearchPlugin.itemCount++;
														},
														
								/**
								 * this method copies the value selected in the element to any others
								 * present on the page in the direction specified.
								 *
								 * @param {String} direction the direction in which the value should be copied
								 * @param {String} elementtype the type of element we are copying to
								 * @param {Object} element element which we can obtain value and name for copying
								 */
								copySelectedOption: function(direction, elementtype, element)
								{
									// add marker after element
									$j(element).after('<span class="marker"></span>');				
									// variable to indicate the marker has been found
									var markerFound = false;
									// array to hold all inputs
									var inputArray = new Array();
									// copy information down the list
									if (direction == 'down')
									{								
										// create an array of input objects
										inputArray = $j.makeArray($j(elementtype + '[name$="' + $j(element).attr('name').substring($j(element).attr('name').lastIndexOf('.'), $j(element).attr('name').length) + '"]'));					
									}
									else
									{				
										// create an array of input objects
										inputArray = $j.makeArray($j(elementtype + '[name$="' + $j(element).attr('name').substring($j(element).attr('name').lastIndexOf('.'), $j(element).attr('name').length) + '"]'));
										// reverse the array
										inputArray.reverse();				
									}
									
									// loop through all matching inputs
									$j.each(inputArray, function()
									{
										// marker found?
										if ($j(this).next().hasClass('marker'))
										{
											// set variable and remove marker
											markerFound = true;
											$j(this).next().remove();
										}
										// marker found?
										if (markerFound)
										{
											// copy value to input
											$j(this).val($j(element).val());
											// has this element got an onchange event
											if ($j(this).attr('onchange'))
											{
												// trigger the onchange event of element
												$j(this).trigger('onchange');
											}
										}					
									});
								},
														
								/**
								 * this method checks if the item has been marked as faulty and displays/hides the fault description
								 * row as necessary.
								 * 
								 * @param {String} value value of the fault select
								 * @param {Object} row the row select appears in
								 */
								checkFaulty:		function(value, row)
													{
														// select value 'true'?
														if (value == 'true')
														{
															// display the fault row
															$j(row).next().removeClass('hid').addClass('vis');
														}
														else
														{
															// hide the fault row and empty input field
															$j(row).next().removeClass('vis').addClass('hid').find('input[name="faulty"]').val('').focus();
														}
													},
													
								/**
								 * this method toggles the visibility of the item comment field.
								 * 
								 * @param {String} value value of the fault select
								 * @param {Object} row the row anchor appears in
								 */
								toggleItemComment:	function(row)
													{
														// check if row already visible
														if ($j(row).next().next().hasClass('vis'))
														{
															// hide the comment row
															$j(row).next().next().removeClass('vis').addClass('hid');		
														}
														else
														{
															// display the comment row
															$j(row).next().next().removeClass('hid').addClass('vis');															
														}
													},
										
								/**
								 * this method checks if the item has been marked as comment and displays/hides the fault description
								 * row as necessary.
								 * 
								 * @param {String} value value of the fault select
								 * @param {Object} row the row select appears in
								 */
								checkComment:		function(value, row)
													{
														// select value 'Comment'?
														if (value == 'Comment')
														{
															// display the fault row
															$j(row).next().removeClass('hid').addClass('vis');
														}
														else
														{
															// hide the fault row and empty input field
															$j(row).next().removeClass('vis').addClass('hid').find('input[name="comment"]').val('');
														}
													},
								
								/**
								 * this method checks the calibration support of an instrument model and displays a message
								 * accordingly
								 * 
								 * @param {Object} option the option selected
								 * @param {Object} row the row select appears in
								 */		
								checkCalibrationSupport: 	function(option, row)
															{
																// assign class in option to variable
																var className = $j(option).attr('class');																										
																// does message row exist?
																if ($j(row).next().find('td').length < 2)
																{
																	// remove the next row as it has a message
																	$j(row).next().remove();
																	// reset values in hidden inputs
																	$j(row).find('input[name$="supported"]').val('true');
																	$j(row).find('input[name$="supportDesc"]').val('');
																}												
																// is calibration supported?
																if ((className == 'THIRD') || (className == 'OEM'))
																{
																	// message text to display
																	var msgText = i18n.t("web:webSearchPlugin:noteCalThirdSupport", {sysComp : WebSearchPlugin.sysComp, defaultValue : "Note: The calibration of the item above is not specifically supported by " + WebSearchPlugin.sysComp + " to this standard meaning a third party will be used."});
																	// add row after item																	
																	$j(row).after(	'<tr>' +
																						'<td colspan="9" class="' + className + '">' +
																							msgText +
																						'</td>' +
																					'</tr>');
																	// add values to hidden inputs
																	$j(row).find('input[name$="supported"]').val('false');
																	$j(row).find('input[name$="supportDesc"]').val(msgText);
																}
																else if (className == 'UNSUPPORTED')
																{	
																	// message text to display
																	var msgText = i18n.t("web:webSearchPlugin:noteCalUnsupported", {sysComp : WebSearchPlugin.sysComp, sysTel : WebSearchPlugin.sysTel, defaultValue : 
																			"Note: The calibration of the item above is not specifically supported by " + WebSearchPlugin.sysComp + " or any of our third parties to this standard." +
																			"Please contact our customer services department (" + WebSearchPlugin.sysTel + ")"});
																	// add row after item																
																	$j(row).after(	'<tr>' +
																						'<td colspan="9" class="' + className + '">' +
																							msgText +
																						'</td>' +
																					'</tr>');
																	// add values to hidden inputs
																	$j(row).find('input[name$="supported"]').val('false');
																	$j(row).find('input[name$="supportDesc"]').val(msgText);
																}
																// add background color to select
																$j(option).parent().removeClass().addClass(className);
															},
													
								/**
								 * this method changes the search type from model to instrument or vice versa
								 * 
								 * @param {String} searchtype the type of search to implement (i.e. 'instrument' or 'model')
								 */
								changeSearchType: 	function(searchtype)
													{
														// change the search type value in plugin
														WebSearchPlugin.searchType = searchtype;
														// search type is model?
														if (searchtype == 'model')
														{
															// hide search inputs not needed for model search
															$j('div.websearch-list:last').addClass('hid');															
														}
														else
														{
															// show search inputs needed for instrument search
															$j('div.websearch-list:last').removeClass('hid');
														}
														// clear search form inputs and any results
														WebSearchPlugin.resetForm();
													},
								
								/**
								 * this method creates all options needed for select boxes in plugin
								 */		
								createOptions : 	function ()
													{
														// call dwr service to retrieve all calibration types
														caltypeserv.getTranslatedCalibrationTypes(WebSearchPlugin.language,WebSearchPlugin.country,
														{
															callback: function(servicetypes)
															{												
																// add option for each calibration type to options variable				
																$j.each(servicetypes, function(i)
																{
																	// add caltypes to array
																	WebSearchPlugin.ctypeOptionsArray.push({caltypeid: servicetypes[i].serviceTypeId, caltype: servicetypes[i].shortName});																
																	// add options to variable
																	WebSearchPlugin.ctypeOptions += '<option value="' + servicetypes[i].serviceTypeId + '">' + servicetypes[i].longName + '</option>';
																});
															}
														});
														// create qty options from 1 to 50
														for (var i = 1; i < 51; i++)
														{
															WebSearchPlugin.qtyOptions += '<option value="' + i + '">' + i + '</option>';
														}
														// add options for turnaround
														WebSearchPlugin.turnOptions = 	'<option value="" selected="selected">' + i18n.t("select", "Select") + '</option>';
																						$j.each(WebSearchPlugin.turnOpts, function(i)
																						{
																							WebSearchPlugin.turnOptions += 	'<option value="' + WebSearchPlugin.turnOpts[i].substring(0, WebSearchPlugin.turnOpts[i].indexOf('-')) + '" ';
																																// defaulting to STANDARD for sake of NG really. This needs to be a company preference or something
																																if (WebSearchPlugin.turnOpts[i].substring(0, WebSearchPlugin.turnOpts[i].indexOf('-')) == 'STANDARD')
																																{
																																	WebSearchPlugin.turnOptions += 'selected="selected"';
																																}
																							WebSearchPlugin.turnOptions += 	'>' + WebSearchPlugin.turnOpts[i].substring((WebSearchPlugin.turnOpts[i].indexOf('-') + 1), WebSearchPlugin.turnOpts[i].indexOf('+')) + ' (' + WebSearchPlugin.turnOpts[i].substring((WebSearchPlugin.turnOpts[i].indexOf('+') + 1), WebSearchPlugin.turnOpts[i].length) + ')</option>';
																						});																						
														// add options for faulty instrument	
														WebSearchPlugin.faultyOptions = '<option value="" selected="selected">' + i18n.t("select", "Select") + '</option>' +
																						'<option value="false">Not Faulty</option>' +
																						'<option value="true">Faulty</option>';
														// add options for comment instrument	
														WebSearchPlugin.commentOptions = 	'<option value="" selected="selected">' + i18n.t("noComment", "No Comment") + '</option>' +
																							'<option value="Comment">' + i18n.t("comment", "Comment") + '</option>';
													},
													
								/**
								 * this method removes a basket item from the current basket
								 * 
								 * @param {Integer} plantid id of an instrument if present
								 * @param {Object} table this is the table to be deleted
								 */
								removeItem:			function(plantid, table)
													{
														// remove the table from basket
														$j(table).remove();														
														// use in basket indicator input to move up dom tree and find the anchor which holds add image and replace with in basket image
														$j('div.' + WebSearchPlugin.resultsDiv + ' table input[name="inBasketIndicator"][value="' + plantid + '"]')
																.parent()
																.siblings('td:last')
																.find('a')
																.attr('disabled', '')
																.empty()
																.append('<img src="../img/icons/add.png" width="16" height="16" alt="" title="" />');																											
														// are there any remaining tables in the basket table?
														if ($j('table.' + WebSearchPlugin.currentBasket).length < 3)
														{
															// which basket are we using?
															if (WebSearchPlugin.pluginType == 'Quotation') 
															{
																// add empty quotation basket message
																$j('table.' + WebSearchPlugin.currentBasket + ':first').after(	'<table class="' + WebSearchPlugin.collectBasket + '" cellpadding="0" cellspacing="0" border="0" summary="">' +	
																																	'<tbody>' +
																																		'<tr>' +
																																			'<td colspan="7" class="text-center">' + i18n.t("web:webSearchPlugin.noItemsInQuoteReqBasket", "You currently have no items in your quote request basket") + '</td>' +
																																		'</tr>' +
																																	'</tbody>' +
																																'</table>');
															}
															else
															{
																// add empty collection basket message
																$j('table.' + WebSearchPlugin.currentBasket + ':first').after(	'<table class="' + WebSearchPlugin.quoteBasket + '" cellpadding="0" cellspacing="0" border="0" summary="">' +
																																	'<tbody>' +
																																		'<tr>' +
																																			'<td colspan="8" class="text-center">' + i18n.t("web:webSearchPlugin.noItemsInCollectionReqbasket", "You currently have no items in your collection request basket") + '</td>' +
																																		'</tr>' +
																																	'</tbody>' +
																																'</table>');
															}															
														}
														// which basket are we using?
														if (WebSearchPlugin.pluginType == 'Quotation') 
														{
															// loop through all remaining basket items
															$j('table.' + WebSearchPlugin.currentBasket + ':not(:first):not(:last)').each(function(i)
															{
																// variable for new classname
																var classname = 'odd';
																// even row
																if ((i % 2) == 0)
																{
																	// set classname
																	classname = 'even';
																}
																// reset class name of the current table rows
																$j(this).find('tbody tr').removeClass().addClass(classname);
																// has the comment row been displayed?
																if ($j(this).find('tbody tr:first').next().hasClass('vis'))
																{
																	// update class in comment row and make visible
																	$j(this).find('tbody tr:first')
																			.next()
																			.removeClass()
																			.addClass('vis ' + classname);
																}
																else
																{
																	// update class in comment row and make visible
																	$j(this).find('tbody tr:first')
																			.next()
																			.removeClass()
																			.addClass('hid ' + classname);
																}
															});
														}
														else
														{
															// loop through all remaining basket items
															$j('table.' + WebSearchPlugin.currentBasket + ':not(:first):not(:last)').each(function(i)
															{
																// variable for new classname
																var classname = 'odd';
																// even row
																if ((i % 2) == 0)
																{
																	// set classname
																	classname = 'even';
																}
																// reset class name of the current table rows
																$j(this).find('> tbody:first tr:first')
																		.removeClass()
																		.addClass(classname).next()
																		.removeClass().addClass(classname)
																		.find('table.child_table tbody tr:first')
																		.removeClass().addClass(classname);
																// has the faulty row been displayed?
																if ($j(this).find('> tbody tr:last').prev().hasClass('vis'))
																{
																	// update class in faulty row and make visible
																	$j(this).find('> tbody tr:last').prev()
																			.removeClass()
																			.addClass('vis ' + classname);
																}
																else
																{
																	// update class in faulty row and make visible
																	$j(this).find('> tbody tr:last').prev()
																			.removeClass()
																			.addClass('hid ' + classname);
																}
																// has the comment row been displayed?
																if ($j(this).find('> tbody tr:last').hasClass('vis'))
																{
																	// update class in faulty row and make visible
																	$j(this).find('> tbody tr:last')
																			.removeClass()
																			.addClass('vis ' + classname);
																}
																else
																{
																	// update class in faulty row and make visible
																	$j(this).find('> tbody tr:last')
																			.removeClass()
																			.addClass('hid ' + classname);
																}
															});
														}
														// remove one from the basket count
														webSearchPluginBasketCount--;														
													},
															
								/**
								 *	this method empties the results div and clears all input field for a new search to be implemented
								 */
								resetForm:			function()
													{
														// clear all input fields
														$j('div.' + WebSearchPlugin.resultsDiv).empty()
																									.parent()
																									.css({display: 'none'});
														$j('input[name="' + WebSearchPlugin.mfrName + '"]').attr('value', '').focus();
														$j('input[name="' + WebSearchPlugin.modelName + '"]').attr('value', '');
														$j('input[name="' + WebSearchPlugin.descName + '"]').attr('value', '');
														$j('input[name="' + WebSearchPlugin.barcodeField + '"]').attr('value', '');
														$j('input[name="' + WebSearchPlugin.plantField + '"]').attr('value', '');
														$j('input[name="' + WebSearchPlugin.serialField + '"]').attr('value', '');											
													}																																	
								};

/**
 * this method checks the basket item count to see if only one item has been added, if this is the case
 * then a message is shown informing the user that they can add more items to this basket before submitting.
 * 
 * @param {String} basketType basket type working with (i.e. 'collection' or 'quotation').
 */
function checkBasketItemsSize(basketType)
{
	// create variable for more collection items message text
	var messagetext = 	i18n.t("web:webSearchPlugin.msgCheckBasketItemsSize", 
						"<p>We notice that you have only added one item to your collection request.</p>" +
						"<p>Please be aware that you can add more items to your collection by clearing the search form (Reset Search Button) and entering your next piece of search criteria.</p>" +
						"<p>Please click continue to carry on with the submission of this collection request or cancel to add more items.</p>");
	
	// firstly check if we have only one item
	if (basketType == 'collection' && webSearchPluginBasketCount == 1)
	{
		// initial prompt for user
		$j.prompt(messagetext,
		{ 
			submit: confirmcallback,
			buttons: 
			{ 
				Continue: true,
				Cancel: false 
			}, 
			focus: 1
		});
	}
	else
	{
		// validate basket items
		checkBasketContents(basketType);
	}
}

// callback method for initial prompt
function confirmcallback(v,m)
{
	// user confirmed action?
	if (m)
	{
		// validate basket items
		checkBasketContents(basketType);
	}
}

/**
 * check that the basket in use is not empty or has un-selected fields
 * 
 * @param {String} basketType the type of basket we are checking
 */
function checkBasketContents(basketType)
{
	// variable to see if validation has failed
	var passedValidation = true;
	// firstly check that we have items in our basket
	if (!webSearchPluginBasketCount > 0)
	{
		if (basketType == 'collection')
			{
			$j.prompt(i18n.t("web:webSearchPlugin.noAddedItemsInCollectionBasket", "You have not added any items to your collection basket, please select your collection items before proceeding."));
			}
		else
			{
			$j.prompt(i18n.t("web:webSearchPlugin.noAddedItemsInQuoteBasket", "You have not added any items to your quote basket, please select your quote items before proceeding."));
			}
		passedValidation = false;
		return false;
	}
	else
	{
		// this is a collection basket
		if (basketType == 'collection')
		{
			// initialise count
			var count = 1;
			// check all caltype entries have been selected
			$j('table.' + WebSearchPlugin.collectBasket + ' select[name$="caltypeid"]').each(function()
			{
				if (this.value == '')
				{
					$j.prompt(i18n.t("web:webSearchPlugin.noCalTypeSelected", {itemNo : count, defaultValue : "A calibration type for item " + count + " should be selected before you proceed."}));
					passedValidation = false;
					return false;
				}
				// check that the turnaround option in this row has been selected
				if ($j(this).parent().next().find('select').val() == '')
				{
					$j.prompt(i18n.t("web:webSearchPlugin.noTurnOptionSelected", {itemNo : count, defaultValue : "A turnaround option for item " + count + " should be selected before you proceed."}));
					passedValidation = false;
					return false;
				}
				// check that the faulty option in this row has been selected
				if ($j(this).parent().next().next().find('select').val() == '')
				{
					$j.prompt(i18n.t("web:webSearchPlugin.noFaultyOptionSelected", {itemNo : count, defaultValue : "A faulty option for item " + count + " should be selected before you proceed."}));
					passedValidation = false;
					return false;	
				}
				// check if item faulty?
				if ($j(this).parent().next().next().find('select').val() == 'true')
				{
					if ($j(this).parent().parent().next().find('input[type="text"]').val() == '')
					{
						$j.prompt(i18n.t("web:webSearchPlugin.noFaultyProvided", {itemNo : count, defaultValue : "A fault description for item " + count + " should be provided as it is marked as faulty."}));
						passedValidation = false;
						return false;	
					}
				}
				// add one to count
				count++;			
			});
		}
		else
		{
			// all quote type entries have been selected
			$j('table.' + WebSearchPlugin.quoteBasket + ' select[name$="quotetype"]').each(function()
			{
				if (this.value == '')
				{
					$j.prompt(i18n.t("web:webSearchPlugin.noQuoteTypeSelected", "A quotation type should be selected for all your basket items before you proceed."));
					passedValidation = false;
					return false;
				}
				// check that the calibration type option in this row has been selected
				if ($j(this).parent().next().find('select').val() == '')
				{
					$j.prompt(i18n.t("web:webSearchPlugin.noCalTypeSelectedForAllItems", "A calibration type should be selected for all your basket items before you proceed."));
					passedValidation = false;
					return false;
				}
			});
		}
	}
	// this is a collection basket
	if (basketType == 'collection')
	{
		// call main validation method in WebCreateCollection.js with this result
		validateCollectionFields(passedValidation);
	}
	else
	{
		// call main validation method in WebCreateQuoteRequest.js with this result
		validateQuotationFields(passedValidation);
	}		
}
