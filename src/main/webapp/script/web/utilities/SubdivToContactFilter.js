/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	SubdivToContactFilter.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	22/05/2009 - SH - Created File
*					: 	25/09/2015 - TProvost - Manage i18n
*
****************************************************************************************************************************************/


/**
 * this method accepts a subdiv id and gets all active addresses and contacts for that subdivision before adding
 * them to the address select and contact select objects passed.
 * 
 * @param {Integer} subdivid the id of the subdivision to get addresses for
 * @param {Object} selectAddr the address select we want to add address results to
 * @param {Object} selectCont the contact select we want to add contact results to
 * @param {Boolean} allOption indicates whether an all option should be included
 * @param {Integer/String} allOptionValue the value required when all option selected (''/0)
 */
function getActiveSubdivAddrsAndConts(subdivid, selectAddr, selectCont, allOption, allOptionValue)
{	
	if (subdivid != '')
	{
		var addressResponse = $j.get('addressapi/activesubdivaddresses/' + subdivid);

		addressResponse.done(function(addrs)
        {
            // addresses returned?
            if (addrs.length > 0)
            {
                // new address options variable
                var content = '';
                // all option?
                if (allOption)
                {
                    content += '<option value="' + allOptionValue + '">' + i18n.t("web:subdivToContactFilter.searchAllAddresses", "Search All Addresses") + '</option>';
                }
                // add all new address options to variable
                $j.each(addrs, function(i)
                {
                    content += '<option value="' + addrs[i].addrid + '">' + addrs[i].addr1 + '</option>';
                });
                // empty select and add new options then trigger the onchange event to cascade contact search for selected address
                $j(selectAddr).empty().append(content).trigger('onchange');
            }
            else
            {
                // empty select and add no address message
                $j(selectAddr).empty().append('<option value="' + allOptionValue + '">' + i18n.t("web:subdivToContactFilter.searchAllAddresses", "Search All Addresses") + '</option>');
            }
        });

		addressResponse.fail(function( jqXHR, textStatus ) {
            alert( "Request failed: " + textStatus );
        });

		var contactResponse = $j.get("contactapi/activesubdivcontacts/" + subdivid);

		contactResponse.done(function(cons)
        {
            // contacts returned?
            if (cons.length > 0)
            {
                // new contact options variable
                var content = '';
                // all option
                if (allOption)
                {
                    content += '<option value="' + allOptionValue + '">' + i18n.t("web:subdivToContactFilter.searchAllContacts", "Search All Contacts") + '</option>';
                }
                // add all new contact options to variable
                $j.each(cons, function(i)
                {
                    content += '<option value="' + cons[i].personid + '">' + cons[i].name + '</option>';
                });
                // empty select and add new options
                $j(selectCont).empty().append(content);
            }
            else
            {
                // empty select and add no contact message
                $j(selectCont).empty().append('<option value="' + allOptionValue + '">' + i18n.t("web:subdivToContactFilter.searchAllContacts", "Search All Contacts") + '</option>');
            }
        });

		contactResponse.fail(function( jqXHR, textStatus ) {
            alert( "Request failed: " + textStatus );
        });

	}
	else
	{
		// all address variable
		var addrContent = '<option value="">' + i18n.t("web:subdivToContactFilter.searchAllAddresses", "Search All Addresses") + '</option>';
		// empty select and add new option
		$j(selectAddr).empty().append(addrContent);	
		// all address variable
		var contContent = '<option value="">' + i18n.t("web:subdivToContactFilter.searchAllContacts", "Search All Contacts") + '</option>';
		// empty select and add new option
		$j(selectCont).empty().append(contContent);	
	}	
}
