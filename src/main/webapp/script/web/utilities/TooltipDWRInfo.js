/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	TooltipDWRInfo.js
*	DESCRIPTION		:	This javascript file is responsible for loading dwr services and retrieving data for
* 					: 	creating tooltip pop up boxes. 
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	Created by SH 30/08/2007
*
****************************************************************************************************************************************/

/**
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
var Elements1 = new Array();
var Elements2 = new Array();
var Elements3 = new Array();
var Elements4 = new Array();
var Elements5 = new Array();

/**
 * this function checks the type of tooltip requested and loads the correct javascript service then calls a method
 * to retrieve desired details
 * 
 * @param {String} request type of tooltip pop-up content required
 * @param {Integer} id id of the entity for which tooltip content is required
 * @param {Integer} value value passed if converting a currency
 */
function filterTooltipRequest(request, id, value)
{
	// load the menu navigation javascript file if not already
	// loadScript.loadScriptsDynamic(new Array('../script/trescal/core/utilities/Menu.js'));
	// check which ajax call we should be making (i.e. contact, company)
	switch (request)
	{									
		case 'itemstatus':		// load the service javascript file if not already
								loadScript.loadScriptsDynamic(new Array('dwr/interface/jobitemactionservice.js'), true,
								{
									callback: function()
									{
										getStatusInfo(id);
										$j('#JT').show();
									}
								});
															
								break;								
	}
}

/**
 * this method retrieves job item status information.
 * this information is then appended to the tooltip pop up.
 * 
 */
function getStatusInfo(id)
{	
	// call dwr service to retrieve all job item status's
	jobitemactionservice.webGetJobItemActions(id,
	{
		callback:function(result)
		{
			if (result.success)
			{
				createStatusContent(result.results);	
			}		
		}
	});
	
	$j('div').remove('.JT_loader');
}

/**
 * this is the callback method for function getStatusInfo()
 * 
 * @param {Object} actions is a list of job item action objects
 */
function createStatusContent(actions)
{
	var content = 	'<table class="default TTjistatus" summary="This table displays all actions for a job item">' +
						'<thead>' +							
							'<tr>' +
								'<th scope="col" class="activity">' + i18n.t("activity", "Activity") + '</th>' +
								'<th scope="col" class="status">' + i18n.t("status", "Status") + '</th>' +
								'<th scope="col" class="compby">' + i18n.t("completedBy", "Completed By") + '</th>' +
								'<th scope="col" class="compon">' + i18n.t("completedOn", "Completed On") + '</th>' +
							'</tr>' +
						'</thead>' +
						'<tbody>';
	// show all job item actions apart from deletions and overrides
	$j.each(actions, function(i)
	{
		content += 	'<tr>' +
		'<td>' + actions[i].activityDesc + '</td>' +
		'<td>' + actions[i].endStatus + '</td>' +
		'<td>' + actions[i].name + '</td>' +
		'<td>' + new Date(actions[i].endStamp).toLocaleString("en-GB") + '</td>' +
		'</tr>';	
	});
	
	content += 	'</tbody>' +
			'</table>';
	
	$j('#JT_copy').append(content);
	
	/**
	 * set hover functionality of pop up.  ON mouse over do nothing and on mouse out close the pop up.
	 */ 
	$j('#JT').hover(function()	{
									mouseOverSignal = true;
								},
					function()	{
									$j('#JT').remove();
									mouseOverSignal = false; 
									return false;
								});														
}