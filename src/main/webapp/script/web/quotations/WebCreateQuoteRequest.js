/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	WebCreateQuoteRequest.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	22/07/2009 - SH - Created File
*
****************************************************************************************************************************************/

/** 
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
 var menuElements = new Array( 	{ anchor: 'viewquotation-link', block: 'viewquotation-tab' },
 								{ anchor: 'requestquotation-link', block: 'requestquotation-tab' },
								{ anchor: 'issuedquotation-link', block: 'issuedquotation-tab' },
								{ anchor: 'acceptedquotation-link', block: 'acceptedquotation-tab'} );
 
 /**
  * Array of autocomplete objects (multiple are possible) containing setup for 
  * jQueryUI initialization.
  * this is used in the init() function.
  */
 var jquiAutocomplete = new Array(
			{
				inputFieldName: 'descName',
				inputFieldId: 'descId',
				source: 'searchdescriptiontags.json'
			},
			{
				inputFieldName: 'mfrName',
				inputFieldId: 'mfrId',
				source: 'searchmanufacturertags.json'
			}
 );

/**
 * this method calls a dwr service to validate quotation form fields
 * 
 * @param {boolean} basketValPassed boolean indicating if the javascript basket validation has passed.
 */
function validateQuotationFields(basketValPassed)
{
	// basket has passed javascript validation?
	if (basketValPassed)
	{
		// disable the submit button
		$j('input[name="submitQuotation"]').attr('disabled', true);
		// load dwr service
		loadScript.loadScriptsDynamic(new Array('dwr/interface/quotationservice.js'), true,
		{
			callback:function()
			{
				// get fields for validation
				var poNumber = $j('#poNumber').val();
				// call dwr service to check validation fields
				quotationservice.webValidateQuoteRequest(poNumber,
				{
					callback:function(result)
					{
						// insertion failed
						if(!result.success)
						{
							// remove any previous error images
							$j('span.error').remove();
							// is warning box already visible?
							if ($j('div.warningBox1').hasClass('hid'))
							{
								$j('div.warningBox1').removeClass('hid');
							}	
							// empty any previous error messages
							$j('div.warningBox3 > div').empty();
							// look for all errors							
							$j.each(result.errorList, function(i)
							{								
								// add current error object to variable
								var error = result.errorList[i];
								// add error message to page head
								$j('div.warningBox3').find('div').append(error.defaultMessage + '<br />');
								// is there already an error indicator next to field
								if (!$j('#' + error.field).next().hasClass('error'))
								{
									// show error message next to failed field
									$j('#' + error.field).after('<span class="error"><img src="../img/web/asterisk.png" width="16" height="16" /></span>');	
								}	
							});
							// focus on error at top of page
							$j('a#errorMessage').focus();
							// enable button
							$j('input[name="submitQuotation"]').attr('disabled', false);
						}
						else
						{
							// all validation has passed, submit the form
							$j('form[name="quotationform"]').submit();
						}
					}
				});
				
			}
		});
	}
	else
	{
		// enable button
		$j('input[name="submitQuotation"]').attr('disabled', false);
		return false;
	}
}