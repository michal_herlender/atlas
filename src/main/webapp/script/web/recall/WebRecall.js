/** Filename /script/web/recall/WebRecall.js */

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'../script/web/utilities/SubdivToContactFilter.js' );


/**
 */
function submitForm(startDateText, finishDateText) {
    $j("input[name='startDate']").val(startDateText);
    $j("input[name='finishDate']").val(finishDateText);
    $j("#recallform").submit();
}