/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	WebCreateCollection.js
*	DESCRIPTION		:	
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	20/03/2009 - SH - Created File
*					: 	28/09/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
* array of javascript filenames to be loaded into the page (was in main import but not loading - needed?)
*/
var pageImports = new Array (	'dwr/interface/addressservice.js');

/** 
 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
 */
 var menuElements = new Array( 	{ anchor: 'viewcollection-link', block: 'viewcollection-tab' },
 								{ anchor: 'requestcollection-link', block: 'requestcollection-tab' });

/**
 * array of calendar objects containing the initial setup literals needed
 * @param inputField (String) id of the input field to which the calendar should be binded
 * @param dateFormat (String) format that the date should be displayed
 * @param displayDates (String) 'all' make all dates available
 * 								'future' show only dates in the future
 * 								'past' show only dates in the past
 * applied in init() function.
 */				
var jqCalendar = new Array ( { 
								 inputField: 'customDate',
								 dateFormat: 'dd.mm.yy',
								 displayDates: 'future',
								 minDate: +2
							 } );

/**
 * this function is called from the loadScript.init() function in Web_Main.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */


function init()
{
	setCollectionType();
}

/**
 * this method gets the transport option and suggested collection date for the company address selected
 */
function setCollectionType()
{
	// get the address id
	var addrId = $j('select#addrId').val();
	var businessSubdivId = $j('#businessSubdivId').val();
	
	// address id empty?
	if (addrId != '')
	{
		// call dwr service to get transport option
		addressservice.getTransportOptionIn(addrId, businessSubdivId, 
		{
			callback: function(type)
			{
				if(type != null)
				{
					// populate and show span
					$j('span#collmethod').parent('li').addClass('vis').removeClass('hid');
					$j('span#collday').parent('li').addClass('vis').removeClass('hid');
					$j('span#collmethod').text(type.method.method);
					$j('span#collday').text(type.dayText);
					
					if (type.method.optionPerDayOfWeek == true)
					{
						// call dwr service to get available collection dates
						addressservice.webGetNextNCollectionDates(addrId, businessSubdivId, 4,
						{
							callback: function(dates)
							{								
								var options = '';
								for (i = 0;i < dates.length;i++)
								{
									options += '<option value="' + dates[i] + '">' + dates[i] + '</option>';
								}		
								$j('#rrDate').empty().append(options);
							}
						});
						
						$j('#rrDate').parent('li').addClass('vis').removeClass('hid');
						$j('#customDate').parent('li').addClass('hid').removeClass('vis');
					}
					else
					{
						// hide span
						$j('span#collday').parent('li').addClass('hid').removeClass('vis');
						
						$j('#customDate').parent('li').addClass('vis').removeClass('hid');
						
						$j('#rrDate').empty().append('<option value="">' + i18n.t("web:webCreateCollection.noDateAvailable", "No Date Available") + '</option>');
						$j('#rrDate').parent('li').addClass('hid').removeClass('vis');
					}
				}
				else
				{
					// hide span	
					$j('span#collmethod').parent('li').addClass('hid').removeClass('vis');
					$j('span#collday').parent('li').addClass('hid').removeClass('vis');
					
					$j('#customDate').parent('li').addClass('vis').removeClass('hid');
					
					$j('#rrDate').empty().append('<option value="">' + i18n.t("web:webCreateCollection.noDateAvailable", "No Date Available") + '</option>');
					$j('#rrDate').parent('li').addClass('hid').removeClass('vis');
				}							
			}
		});	
	}
	else
	{
		// hide span	
		$j('span#collmethod').parent('li').addClass('hid').removeClass('vis');
		$j('span#collday').parent('li').addClass('hid').removeClass('vis');
		
		$j('#customDate').parent('li').addClass('vis').removeClass('hid');
		
		$j('#rrDate').empty().append('<option value="">' + i18n.t("web:webCreateCollection.noDateAvailable", "No Date Available") + '</option>');
		$j('#rrDate').parent('li').addClass('hid').removeClass('vis');
	}
}

/**
 * this method calls a dwr service to validate collection form fields
 * 
 * @param {boolean} basketValPassed boolean indicating if the javascript basket validation has passed.
 */
function validateCollectionFields(basketValPassed)
{
	// basket has passed javascript validation?
	if (basketValPassed)
	{
		// disable the submit button
		$j('input[name="submitCollection"]').attr('disabled', true);
		// load dwr service
		loadScript.loadScriptsDynamic(new Array('dwr/interface/scheduleservice.js'), true,
		{
			callback:function()
			{
				// get fields for validation
				var addrId = $j('#addrId').val();
				var rrDate = $j('#rrDate').val();
				var customDate = $j('#customDate').val();
				var poCal = $j('#poNumberCal').val();
				var poRep = $j('#poNumberRep').val();
				var furtherInfo = $j('#furtherInformation').val();
				// call dwr service to check validation fields
				scheduleservice.webValidateScheduleRequest(addrId, rrDate, customDate, poCal, poRep, furtherInfo,
				{
					callback:function(result)
					{
						// insertion failed
						if(!result.success)
						{
							// remove any previous error images
							$j('span.error').remove();
							// is warning box already visible?
							if ($j('div.warningBox1').hasClass('hid'))
							{
								$j('div.warningBox1').removeClass('hid');
							}	
							// empty any previous error messages
							$j('div.warningBox3 > div').empty();
							// look for all errors							
							$j.each(result.errorList, function(i)
							{								
								// add current error object to variable
								var error = result.errorList[i];
								// add error message to page head
								$j('div.warningBox3').find('div').append(error.defaultMessage + '<br />');
								// is there already an error indicator next to field
								if (!$j('#' + error.field).next().hasClass('error'))
								{
									// show error message next to failed field
									$j('#' + error.field).after('<span class="error"><img src="../img/web/asterisk.png" width="16" height="16" /></span>');	
								}	
							});
							// focus on error at top of page
							$j('a#errorMessage').focus();
							// enable button
							$j('input[name="submitCollection"]').attr('disabled', false);
						}
						else
						{
							// all validation has passed, submit the form
							$j('form[name="collectionform"]').submit();
						}
					}
				});
				
			}
		});
	}
	else
	{
		// enable button
		$j('input[name="submitCollection"]').attr('disabled', false);
		return false;
	}
}
			