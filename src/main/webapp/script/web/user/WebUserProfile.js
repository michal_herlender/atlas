/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	WebUserProfile.js
*	DESCRIPTION		:
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	03/06/2009 - SH - Created File
*					: 	30/09/2015 - TProvost - Manage i18n
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'dwr/interface/calibrationservice.js',
								'dwr/interface/userservice.js' );

/**
 * create array of descriptions to be used for password strength function
 */
var desc = new Array();
desc[0] = "Very Weak";
desc[1] = "Weak";
desc[2] = "Better";
desc[3] = "Medium";
desc[4] = "Strong";
desc[5] = "Strongest";

/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{
	// load stylesheet using the dom if not already
	loadScript.loadStyle('../styles/jQuery/jquery-boxy.css', 'screen');
	// i18n of descriptions for password strength function
	desc[0] = i18n.t("common:password.veryWeak", "Very Weak");
	desc[1] = i18n.t("common:password.weak", "Weak");
	desc[2] = i18n.t("common:password.better", "Better");
	desc[3] = i18n.t("common:password.medium", "Medium");
	desc[4] = i18n.t("common:password.strong", "Strong");
	desc[5] = i18n.t("common:password.strongest", "Strongest");
}

/**
 * this method shows a jquery boxy thickbox which allows the user to change their password
 * 
 * @param {Integer} personid the id of the user who wishes to change their password
 */
function changePassword(personid)
{
	// variable to hold content
	var content = '';
	// load stylesheet using the dom if not already
	loadScript.loadStyle('../styles/jQuery/jquery-boxy.css', 'screen');
	// load javascript for boxy pop up if not already
	loadScript.loadScriptsDynamic(new Array('../script/thirdparty/jQuery/jquery.boxy.js'), false,
	{
		callback: function()
		{
			// create content for change password request
			content += 	'<div id="changePasswordBoxy" class="overflow">' +
							// add warning box to display error messages if the move fails
							'<div class="hid warningBox1">' +
								'<div class="warningBox2">' +
									'<div class="warningBox3">' +
									'</div>' +
								'</div>' +
							'</div>' +
							// add success box to display message when updated
							'<div class="hid successBox1">' +
								'<div class="successBox2">' +
									'<div class="successBox3">' +
									'</div>' +
								'</div>' +
							'</div>' +
							'<fieldset>' +
								'<ol>' +
									'<li>' +
										'<label>' + i18n.t("web:webUserProfile.oldPwdLabel", "Old Password:") + '</label>' +
										'<input type="password" class="password" id="currentPW" value="" />' +
									'</li>' +
									'<li>' +
										'<label>' + i18n.t("web:webUserProfile.newPwdLabel", "New Password:") + '</label>' +
										'<input type="password" class="password" id="newPW" value="" onkeypress=" if (event.keyCode==13){ event.preventDefault(); } " onkeyup=" passwordStrength(this.value, $j(this).next()); " />' +
										'<div id="strengthMeter" class="strength0" style=" display: inline; padding: 4px 40px 4px; "><span class="passwordDescription">' + i18n.t("pwdNotEntered", "Password not entered") + '</span></div>' +
									'</li>' +
									'<li>' +
										'<label>' + i18n.t("web:webUserProfile.confirmNewPwdLabel", "Confirm New Password:") + '</label>' +
										'<input type="password" class="password" id="confirmPW" value="" />' +
									'</li>' +
									'<li>' +
										'<label>&nbsp;</label>' +
										'<input type="button" name="changePW" class="button" value="' + i18n.t("confirm", "Confirm") + '" onclick=" submitPasswordChange(' + personid + ', $j(\'input#currentPW\').val(), $j(\'input#newPW\').val(), $j(\'input#confirmPW\').val()); return false; " />' +
										'<input type="button" name="cancel" class="button" value="' + i18n.t("cancel", "Cancel") + '" onclick=" Boxy.get($j(\'div#changePasswordBoxy\')).hide(); " />' +
									'</li>' +
								'</ol>' +
							'</fieldset>' +
						'</div>';
			// create new boxy pop up using content variable
			var changePasswordBoxy = new Boxy(content, {title: i18n.t("web:webUserProfile.changePwd", "Change Password"), modal: true, unloadOnHide: true});
			// show boxy pop up and adjust size
			changePasswordBoxy.tween(540, 150);
		}
	});
}

/**
 * this method submits the new password values to be updated via a dwr service
 * 
 * @param {Integer} personid the id of the user who wants to update their password
 * @param {String} currentPW the current password of the user
 * @param {String} newPW the new password the user wishes to use
 * @param {String} confirmPW the confirmation of the users new password
 */
function submitPasswordChange(personid, currentPW, newPW, confirmPW)
{
	// call dwr service to update users password
	userservice.webUpdateUserPassword(personid, currentPW, newPW, confirmPW,
	{
		callback:function(result)
		{
			// password update was successful?
			if (result.success) 
			{
				// clear all password fields
				$j('div#changePasswordBoxy input.password').val('');
				// clear the strength meter div
				$j('div#strengthMeter').find('span:first').empty().end().removeClass().addClass('strength0');
				// is warning box hidden?
				if (!$j('div.warningBox1').hasClass('hid'))
				{
					// if not hide it
					$j('div.warningBox1').addClass('hid');
				}
				// display success message
				$j('div.successBox1').removeClass('hid').find('div.successBox3').html('<div>' + result.message + '</div>');
			}
			else 
			{
				// is success box hidden?
				if (!$j('div.successBox1').hasClass('hid'))
				{
					// if not hide it
					$j('div.successBox1').addClass('hid');
				}
				// display failure message
				$j('div.warningBox1').removeClass('hid').find('div.warningBox3').html('<div>' + result.message + '</div>');
			}
			// change size of boxy to accommodate message
			Boxy.get($j('div#changePasswordBoxy')).tween(540, 240);
		}
	});
}



/**
 * this function checks the strength of the password entered by the user and displays 
 * text and coloured bar dependant on that strength
 * 
 * @param {String} password password entered by the user in input field
 * @param {Object} div the div object we want to update with text and class name
 */
function passwordStrength(password, div)
{
	// call dwr service to check the password strength
	calibrationservice.checkPasswordStrength(password,
	{	
		callback:function(score)
		{
			if (password.length < 1)
			{
				// change text in div dependant on password strength
				$j(div).find('span:first').text(i18n.t("pwdNotEntered", "Password not entered"));
				// change class of coloured div dependant on password strength
				$j(div).removeClass().addClass("strength" + score);
			}
			else
			{
				// change text in div dependant on password strength
				$j(div).find('span:first').text(desc[score]);
				// change class of coloured div dependant on password strength
				$j(div).removeClass().addClass("strength" + score);	
			}			
        }	
	});
}