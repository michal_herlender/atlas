/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	WebJobSearch.js
*	DESCRIPTION		:
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	
*
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'../script/web/utilities/SubdivToContactFilter.js' );

/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'jobno';

/**
 * this function is called from the loadScript.init() function in Web_Main.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */
function init()
{
	// javascript file for creating contact search plugin
	loadScript.loadScriptsDynamic(new Array('../script/thirdparty/jQuery/jquery.preloadimage.js'), false,
	{
		callback: function()
		{
			$j.preloadImages('../img/web/searching.gif');
		}
	});
}

/**
 * array of calendar objects containing the initial setup literals needed
 * @param inputField (String) id of the input field to which the calendar should be binded
 * @param dateFormat (String) format that the date should be displayed
 * @param displayDates (String) 'all' make all dates available
 * 								'future' show only dates in the future
 * 								'past' show only dates in the past
 * applied in init() function.
 */	
//var jqCalendar = new Array (
//				 { 
//				 	inputField: 'bookedInDate1',
//				 	dateFormat: 'dd.mm.yy',
//				 	displayDates: 'all',
//					showWeekends: true
//				 },
//				 { 
//				 	inputField: 'bookedInDate2',
//				 	dateFormat: 'dd.mm.yy',
//				 	displayDates: 'all',
//					showWeekends: true
//				 },
//				 { 
//				 	inputField: 'completedDate1',
//				 	dateFormat: 'dd.mm.yy',
//				 	displayDates: 'all',
//					showWeekends: true
//				 },
//				 { 
//				 	inputField: 'completedDate2',
//				 	dateFormat: 'dd.mm.yy',
//				 	displayDates: 'all',
//					showWeekends: true
//				 });

/**
 * Array of description objects (multiple are possible) containing setup for 
 * jQueryUI initialization.
 * 
 * this is used in the init() function.
 */
var jquiAutocomplete = new Array(
				{
					inputFieldName: 'desc',
					inputFieldId: 'descid',
					source: 'searchdescriptiontags.json'
				},
				{
					inputFieldName: 'mfr',
					inputFieldId: 'mfrid',
					source: 'searchmanufacturertags.json'
				});

/**
 * this method shows or hides the between date field
 * 
 * @param {boolean} between indicates whether to show the between date field
 * @param {String} id the id of the between element to display
 */
function changeDateOption(between, id)
{
	// show between?
	if(between == 'true')
	{
		// display between
		$j('#' + id).removeClass('hid').addClass('vis');
	}
	else
	{
		// hide between
		$j('#' + id).removeClass('vis').addClass('hid');
	}
}
