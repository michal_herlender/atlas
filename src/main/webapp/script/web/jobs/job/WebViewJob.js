
function init()
{
	
	//creates table of standards used inside jquery dialog
    function createTable() {
    	//remove any previous table that might be in the dialog
        $j("#standardsUsedTable").remove();
    	//get job item id relating to button pressed that was added to data by click handler.
    	var jobItemId = $j("#dialog").data('jobitemid');
        //send get request for json data containing list of standards used for this job item.
        $j.getJSON("getstandardsused.json", 
    		{jobitemid: jobItemId},
    		function(data, status){
    			//Create table
    			standardsusedtable = $j('<table></table>').attr({ id: "standardsUsedTable"});
    			standardsusedtable.addClass("default viewjob");
    			//Add table header
    			$j('<thead><tr><th>' + 
    						i18n.t("barcode", "Barcode") + 
    					'</th><th>' + 
    						i18n.t("mfr", "Instrument Model Name") + 
    					'</th><th>' + 
    						i18n.t("serialNo", "Serial No") + 
    					'</th><th>' + 
    						i18n.t("plantNo", "Plant No") + 
    					'</th><th>' +
    						i18n.t("web:webViewJob.calDateAtTimeOfUse", "Cal Date at Time of Use") +
    					'</th><th>' + 
    						i18n.t("web:webViewJob.dateOfUse", "Date of Use") +
    					'</th><th>' +
    						i18n.t("certificate", "Certificate") +
    					'</th></tr></thead>').appendTo(standardsusedtable);
    			//add table body
    			tablebody = $j('<tbody></tbody>').appendTo(standardsusedtable);
    			//create row for each standard
    			$j.each(data, function(k, v) {
    				row = $j('<tr></tr>').appendTo(tablebody);
    				
    				if(v.inCalAtTimeOfUse == true){
    					row.addClass("good");
    				} else {
    					row.addClass("bad");
    				}
    				
    				if(v.certId != null && v.fileAvailable == true){
    					var link = '<a href="viewcert.htm?cert=' + v.certId + '"><img src="../img/doctypes/pdf-results.png" width="36" height="35"/></a>';
    				} else {
    					var link = '<img src="../img/doctypes/pdf-unavailable.png" width="36" height="35"/>';
    				}
    				
    				//add cells for each field of data for this standard
    				$j('<td>' + v.plantId + '</td>').appendTo(row);
    				$j('<td>' + v.translatedModelName + '</td>').appendTo(row);
    				$j('<td>' + v.serialNumber + '</td>').appendTo(row);
    				$j('<td>' + v.plantNumber + '</td>').appendTo(row);
    				$j('<td>' + v.calDueDateAtTimeOfUse + '</td>').appendTo(row);
    				$j('<td>' + v.dateOfUse + '</td>').appendTo(row);
    				$j('<td>' + link + '</td>').appendTo(row);
	        	// add the table to the dialog
    			standardsusedtable.appendTo("#dialog");
    		});
        });
    }

    //dialog box initialization
    $j("#dialog").dialog({
        autoOpen: false,
        width: 800,
        open: function (event, ui) {
             createTable();
        }
    });

    //event handler for show standards button click, adds jobitemid obtained from button value to the dialog data
    $j('button.showstandards').click(function () {
        $j("#dialog").data('jobitemid',$j(this).val()).dialog("open");
    });
    
    
	
}

