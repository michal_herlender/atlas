

function init()
{
	$j("#certvalidation").click(function () {
		var certid = $j("#certid").val();
		window.location.href = "certacceptancedoc.htm?certid=" + certid;
    });

	$j("#obsoletify").click(function () {

		var certid = $j("#certid").val();

        var request = $j.ajax({
            url: "certtoggleobsolete.json/" + certid,
            method: "PUT",
            dataType: "json"
        });

        request.done(function() {
            $j("#isitobsolete").text(i18n.t("yes", "Yes"));
            $j('#obsoletify').hide();
            $j('#unobsoletify').show();
        });

        request.fail(function( jqXHR, textStatus ) {
            alert( "Request failed: " + textStatus );
        });

	});

	$j("#unobsoletify").click(function () {

        var certid = $j("#certid").val();

        var request = $j.ajax({
            url: "certtoggleobsolete.json/" + certid,
            method: "PUT",
            dataType: "json"
        });

        request.done(function() {
            $j("#isitobsolete").text(i18n.t("no", "No"));
            $j('#obsoletify').show();
            $j('#unobsoletify').hide();
        });

        request.fail(function( jqXHR, textStatus ) {
            alert( "Request failed: " + textStatus );
        });

    });
}