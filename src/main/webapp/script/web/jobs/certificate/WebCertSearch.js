/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	WebCertSearch.js
*	DESCRIPTION		:
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	30/04/2009 - SH - Created File
*
****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 */
var pageImports = new Array (	'../script/web/utilities/SubdivToContactFilter.js' );

/**
 * array of calendar objects containing the initial setup literals needed
 * @param inputField (String) id of the input field to which the calendar should be binded
 * @param dateFormat (String) format that the date should be displayed
 * @param displayDates (String) 'all' make all dates available
 * 								'future' show only dates in the future
 * 								'past' show only dates in the past
 * applied in init() function.
 */	
var jqCalendar = [{
				 	inputField: 'issueDate1',
				 	dateFormat: 'dd.mm.yy',
				 	displayDates: 'all',
				 	showWeekends: true
				 },
				 {
				 	inputField: 'issueDate2',
				 	dateFormat: 'dd.mm.yy',
				 	displayDates: 'all',
				 	showWeekends: true
				 },
				 {
				 	inputField: 'calDate1',
				 	dateFormat: 'dd.mm.yy',
				 	displayDates: 'all',
				 	showWeekends: true
				 },
				 {
				 	inputField: 'calDate2',
				 	dateFormat: 'dd.mm.yy',
				 	displayDates: 'all',
				 	showWeekends: true
				 },
				{
					inputField: 'jobItemDateComplete1',
					dateFormat: 'dd.mm.yy',
					displayDates: 'all',
					showWeekends: true
				},
				{
					inputField: 'jobItemDateComplete2',
					dateFormat: 'dd.mm.yy',
					displayDates: 'all',
					showWeekends: true
				}];
/**
 * Array of description objects (multiple are possible) containing setup for
 * jQueryUI initialization.
 * this is used in the init() function.
 */
var jquiAutocomplete = [{
        inputFieldName: 'desc',
        inputFieldId: 'descid',
        source: 'searchdescriptiontags.json'
    },
    {
        inputFieldName: 'mfr',
        inputFieldId: 'mfrid',
        source: 'searchmanufacturertags.json'
    }];

/**
 * this function is called from the loadScript.init() function in Web_Main.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute. 
 */ 
function init()
{
	// javascript file for creating contact search plugin
	loadScript.loadScriptsDynamic(new Array('../script/thirdparty/jQuery/jquery.preloadimage.js'), false,
	{
		callback: function()
		{
			$j.preloadImages('../img/web/searching.gif');
		}
	});

    $j("#unAcceptedCertsOnly1").click(function(){
        if(this.checked) {
            $j('input[name=\"acceptDate1\"]').val("");
            $j('input[name=\"acceptDate2\"]').val("");
            $j("#acceptdateinputs").hide();
        } else {
            $j("#acceptdateinputs").show();
		}
	});
}

/**
 * element to be focused on when the page loads.
 * applied in init() function.
 */
var focusElement = 'certNo';

/**
 * this method changes the available input fields when searching on/between dates for
 * a matching issue date.
 * 
 * @param {String} between indicates if the user is requesting to search between two dates
 */
function changeIssueDateOption(between)
{
	// value false?
	if(between === 'false')
	{
		// hide the date span for searching between two dates
		$j('span#issueDateSpan').removeClass('vis').addClass('hid');
	}
	else
	{
		// show the date span for searching between two dates
		$j('span#issueDateSpan').removeClass('hid').addClass('vis');
	}
}

/**
 * this method changes the available input fields when searching on/between dates for
 * a matching calibration date.
 * 
 * @param {String} between indicates if the user is requesting to search between two dates
 */
function changeCalDateOption(between)
{
	// value false?
	if(between === 'false')
	{
		// hide the date span for searching between two dates
		$j('span#calDateSpan').removeClass('vis').addClass('hid');
	}
	else
	{
		// show the date span for searching between two dates
		$j('span#calDateSpan').removeClass('hid').addClass('vis');
	}
}

function changeAcceptDateOption(between)
{
    // value false?
    if(between === 'false')
    {
        // hide the date span for searching between two dates
        $j('span#acceptDateSpan').removeClass('vis').addClass('hid');
    }
    else
    {
        // show the date span for searching between two dates
        $j('span#acceptDateSpan').removeClass('hid').addClass('vis');
    }
}

function changeJiCompletionDateOption(between)
{
	// value false?
	if(between === 'false')
	{
		// hide the date span for searching between two dates
		$j('span#jobItemCompletionDateSpan').removeClass('vis').addClass('hid');
	}
	else
	{
		// show the date span for searching between two dates
		$j('span#jobItemCompletionDateSpan').removeClass('hid').addClass('vis');
	}
}

