

function certAcceptanceInitialization() {

    //Initialize dialogs
    $j("#validationdialog").dialog({
        autoOpen: false,
        width: 500,
        modal: true,
        open: function () {
            // position dialog
            var win = $j(window);
            $j(this).parent().css({
                position: 'absolute',
                left: (win.width() - $j(this).parent().outerWidth()) / 2,
                top: ((win.height() - $j(this).parent().outerHeight()) / 2) + 100
            });
            //Set today's date
//            var d = new Date();
//            var curr_day = d.getDate();
//            var curr_month = d.getMonth() + 1; //Months are zero based
//            var curr_year = d.getFullYear();
//            $j("#validationdialog-date").val(curr_day + "/" + curr_month + "/" + curr_year);

            var certno = $j("#validationdialog").data('certno');
            $j("#validationdialog-certno").text(certno);
            addCalInstructions();
            addCustomerAcceptanceCriteria();
            addDeviation();
            $j("#validationdialog-note").focus();
        }
    });

    $j("#viewvalidationdialog").dialog({
        autoOpen: false,
        width: 500,
        modal: true,
        open: function () {
            // position dialog
            var win = $j(window);
            $j(this).parent().css({
                position: 'absolute',
                left: (win.width() - $j(this).parent().outerWidth()) / 2,
                top: ((win.height() - $j(this).parent().outerHeight()) / 2) + 100
            });

            populateViewValidationDialog();
        }
    });

//    $j("#bulkvalidationcheckdialog").dialog({
//        autoOpen: false,
//        width: 1000,
//        height: 500,
//        modal: true,
//        open: function() {
//            populateBulkValidationCheckDialog();
//        }
//    });


    //Set up event handlers

    $j('a.accept').click(function(event){
        event.preventDefault();
        var certid = $j(this).attr("data-certid");
        var certno = $j(this).attr("data-certno");
        $j("#validationdialog")
            .data('certid',certid)
            .data('certno',certno)
            .dialog("open");
    });

    $j('td.validation').on('click','a.viewacceptance', function(event){
        event.preventDefault();
        var validationid = $j(this).attr("data-acceptanceid");
        $j('#viewvalidationdialog')
            .data('validationid',validationid)
            .dialog('open');
    });

    $j('#validationdialog-accept').click(function(){
    	let dateValidated = $j("#validationdialog-date").val();
    	if(dateValidated == undefined || dateValidated == '')
    		$j('#validationrequired-date').removeClass('hid').addClass('vis');
    	else {
	        acceptrejectcert('accepted');
	        $j("#validationdialog").dialog("close");
    	}
    });

    $j('#validationdialog-reject').click(function(){
    	let dateValidated = $j("#validationdialog-date").val();
    	if(dateValidated == undefined || dateValidated == '')
    		$j('#validationrequired-date').removeClass('hid').addClass('vis');
    	else {
        acceptrejectcert('rejected');
        $j("#validationdialog").dialog("close");
    	}
    });
    
    $j('#validationdialog-reset').click(function(){
    	resetcert();
        $j("#validationdialog").dialog("close");
    });

//    $j('#acceptcerts').click(function () {
//        $j("#bulkvalidationcheckdialog").dialog("open");
//    });

//    $j('#bulkvalidationcheckdialog-cancel').click(function(){
//       $j('#bulkvalidationcheckdialog').dialog("close");
//    });
//
//    $j('#bulkvalidationcheckdialog-accept').click(function(){
//        bulkAcceptCerts(true);
//        $j('#bulkvalidationcheckdialog').dialog("close");
//    });
//    
//    $j('#bulkvalidationcheckdialog-reject').click(function(){
//        bulkAcceptCerts(false);
//        $j('#bulkvalidationcheckdialog').dialog("close");
//    });

    $j('#downloadacceptdocs').click(function () {

    });
}





function addCalInstructions() {

    var certid = $j("#validationdialog").data('certid');

    //send ajax request
    var request = $j.ajax({
        type: "GET",
        contentType: "application/json",
        url: "certificateacceptance/instructions/" + certid
    });

    request.done(function (results) {
        var instructions = '';
        $j.each(results, function (k, v) {
            if (k > 0) {
                instructions = instructions.concat("<br>");
            }
            instructions = instructions.concat(v);
        });
        if (instructions != null) {
            $j("#validationdialog-calinstructions").html(instructions);
        }

    });

    request.fail(function (jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });

}

function addCustomerAcceptanceCriteria() {

    var certid = $j("#validationdialog").data('certid');

    //send ajax request
    var request = $j.ajax({
        type: "GET",
        contentType: "application/json",
        url: "certificateacceptance/acceptancecriteria/" + certid
    });

    request.done(function (results) {
        var criteria = '';
        $j.each(results, function (k, v) {
            if (k > 0) {
                criteria = criteria.concat("<br>");
            }
            criteria = criteria.concat(v);
        });
        if (criteria != null) {
            $j("#validationdialog-customeracceptancecriteria").html(criteria);
        }

    });

    request.fail(function (jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });

}

function addDeviation() {

    var certid = $j("#validationdialog").data('certid');

    //send ajax request
    var request = $j.ajax({
        type: "GET",
        contentType: "application/json",
        url: "certificateacceptance/deviation/" + certid
    });

    request.done(function (result) {
       $j("#validationdialog-deviation").html(result);
    });

    request.fail(function (jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });

}

function acceptrejectcert(action) {

		var content = {};
	    content["certificateId"] = $j("#validationdialog").data('certid');
	    content["dateValidated"] = $j("#validationdialog-date").val();
	    content["note"] = $j("#validationdialog-note").val();
	    content["status"] = action;
	    
	    //send ajax request
	    var request = $j.ajax({
	        type: "POST",
	        contentType: "application/json",
	        url: "certificateacceptance/add",
	        data: JSON.stringify(content),
	        dataType: 'json'
	    });
	
	    request.done(function (result) {
	        
	    	var cell = $j('<span></span>');
	        cell.addClass("acceptreject");
	        var anchor = $j('<a href="#"></a>').appendTo(cell);
	        anchor.addClass("mainlink viewacceptance");
	        anchor.attr("data-acceptanceid",result.certValidationId);
	        var span = $j('<span ></span>').appendTo(anchor);
	        span.addClass("accepteddate");
	        span.addClass(action === 'accepted' ? 'certaccepted' : 'certrejected');
	        span.text($j("#validationdialog-date").val());
	        $j('a[data-certid=' + result.certificateId + ']').closest('tr').find('span.acceptreject').replaceWith(cell);
            $j("#validationdialog-date").val('');
            $j("#validationdialog-note").val('');
	   
	    });
	
	    request.fail(function (jqXHR, textStatus) {
	        alert("Request failed: " + jqXHR.responseText);
	    });
}

function resetcert() {
	let certificateId = $j("#viewvalidationdialog-certid").val();
    //reset ajax request
    var request = $j.ajax({
        type: "POST",
        url: "certificateacceptance/reset",
        contentType: "application/json",
        data: JSON.stringify(certificateId),
        dataType: 'json' 
    });

    request.done(function (result) {
    	document.location.reload();
    });

    request.fail(function (jqXHR, textStatus) {
        alert("Request failed: " + jqXHR.responseText);
    });
}

function populateViewValidationDialog() {
    var validationid = $j("#viewvalidationdialog").data('validationid');
    var request = $j.ajax({
        type: 'GET',
        contentType: 'application/json',
        url: 'certificateacceptance/' + validationid
    });

    request.done(function (result) {
        if(result.rejected) {
            $j("#viewvalidationdialog-approvedbylabel").hide();
            $j("#viewvalidationdialog-rejectedbylabel").show();
        } else {
            $j("#viewvalidationdialog-approvedbylabel").show();
            $j("#viewvalidationdialog-rejectedbylabel").hide();
        }
        $j("#viewvalidationdialog-certno").text(result.certificateNo);
        $j("#viewvalidationdialog-certid").val(result.certificateId);
        $j("#viewvalidationdialog-status").text(result.statusName);
        $j("#viewvalidationdialog-date").text(result.approvedDate);
        $j("#viewvalidationdialog-who").text(result.approvedBy);
        $j("#viewvalidationdialog-note").text(result.note);
    });

    request.fail(function (jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
}

//function  populateBulkValidationCheckDialog() {
//    //empty tables
//    $j('#bulkvalidationcheckdialog-toaccept tbody tr').remove();
//    $j('#bulkvalidationcheckdialog-allreadyaccepted tbody tr').remove();
//    let idsStr = JSON.parse(sessionStorage.getItem("selectedIds"));
//    let ids = idsStr.map(function(item) {
//        return parseInt(item, 10);
//    });
//
//    var request = $j.ajax({
//        type: "POST",
//        url: "certificateacceptance/bulkcheck",
//        data: {
//        	ids : ids,
//        } 
//    });
//
//    request.done(function (result) {
//
//       if(result.notValidated.length > 0) {
//           $j('#bulkvalidationcheckdialog-toacceptdiv').show();
//           $j('#bulkvalidationcheckdialog-accept').removeAttr("disabled");
//           $j('#bulkvalidationcheckdialog-reject').removeAttr("disabled");
//           var table = $j('#bulkvalidationcheckdialog-toaccept');
//           $j.each(result.notValidated, function(k,cert){
//               var row = $j('<tr></tr>').appendTo(table);
//               $j('<td></td>').text(cert.certNo).appendTo(row);
//               var modelNameCell = $j('<td></td>').appendTo(row);
//               var plantNoCell = $j('<td></td>').appendTo(row);
//               var serialNoCell = $j('<td></td>').appendTo(row);
//               $j.each(cert.instruments, function(k, instrument) {
//                   $j('<div></div>').text(instrument.modelName).appendTo(modelNameCell);
//                   $j('<div></div>').text(instrument.plantNo).appendTo(plantNoCell);
//                   $j('<div></div>').text(instrument.serialNo).appendTo(serialNoCell);
//               });
//               $j('<td></td>').text(cert.calDate).appendTo(row);
//           });
//       } else {
//           $j('#bulkvalidationcheckdialog-toacceptdiv').hide();
//           $j('#bulkvalidationcheckdialog-accept').attr("disabled", true);
//           $j('#bulkvalidationcheckdialog-reject').attr("disabled", true);
//       }
//
//       if(result.validated.length > 0) {
//           $j('#bulkvalidationcheckdialog-allreadyaccepteddiv').show();
//           var table = $j('#bulkvalidationcheckdialog-allreadyaccepted');
//           $j.each(result.validated, function(k, cert){
//               var row = $j('<tr></tr>').appendTo(table);
//               $j('<td></td>').text(cert.certNo).appendTo(row);
//               var modelNameCell = $j('<td></td>').appendTo(row);
//               var plantNoCell = $j('<td></td>').appendTo(row);
//               var serialNoCell = $j('<td></td>').appendTo(row);
//               $j.each(cert.instruments, function(k, instrument) {
//                   $j('<div></div>').text(instrument.modelName).appendTo(modelNameCell);
//                   $j('<div></div>').text(instrument.plantNo).appendTo(plantNoCell);
//                   $j('<div></div>').text(instrument.serialNo).appendTo(serialNoCell);
//               });
//               var acceptedDateCell = $j('<td></td>').appendTo(row);
//               var acceptedDateSpan = $j('<span></span>').appendTo(acceptedDateCell);
//               acceptedDateSpan.addClass('accepteddate');
//               if(cert.verificationStatus === 'Accepted') {
//                   acceptedDateSpan.addClass('certaccepted');
//               } else {
//                   acceptedDateSpan.addClass('certrejected');
//               }
//               acceptedDateSpan.text(cert.verificationDate);
//           });
//       } else {
//           $j('#bulkvalidationcheckdialog-allreadyaccepteddiv').hide();
//       }
//
//    });
//
//    request.fail(function (jqXHR, textStatus) {
//        alert("Request failed: " + jqXHR.responseText);
//    });
//
//
//
//}

//function includeAppectedRejectedCertsChange(ele) {
//	if($j(ele).is(":checked") && $j('#bulkvalidationcheckdialog-accept').is(":disabled")) {
//		$j('#bulkvalidationcheckdialog-accept').removeAttr("disabled");
//        $j('#bulkvalidationcheckdialog-reject').removeAttr("disabled");
//	}
//	else if(!$j(ele).is(":checked") && !$j('#bulkvalidationcheckdialog-accept').is(":disabled") && $j('#bulkvalidationcheckdialog-toaccept tbody').children().length == 0) {
//		$j('#bulkvalidationcheckdialog-accept').attr("disabled", true);
//        $j('#bulkvalidationcheckdialog-reject').attr("disabled", true);
//	}
//}


function bulkAcceptCerts(accept) {

	let idsStr = JSON.parse(sessionStorage.getItem("selectedIds"));
    let ids = idsStr.map(function(item) {
        return parseInt(item, 10);
    });
    let includeARCerts = $j('#includeAppectedRejectedCerts').is(":checked")
    
    var request = $j.ajax({
        type: "POST",
        url: "certificateacceptance/bulkadd",
        data: {
        	ids : ids,
        	accept : accept,
        	includeARCerts : includeARCerts,
        }
    });

    request.done(function (results) {

        //set accepted date for any certs on this page that were just bulk accepted.
        var d = new Date();
        var curr_day = ("0" + d.getDate()).slice(-2);
        var curr_month = ("0" + (d.getMonth() + 1)).slice(-2)
        var curr_year = d.getFullYear().toString().substr(-2);
        var today = curr_day + "/" + curr_month + "/" + curr_year;

        results.forEach(function (certIdAndValidationId) {
            var anchorToFind = $j('a[data-certid=' + certIdAndValidationId['key'] + ']');
            
            //is that cert on this page?
            if (anchorToFind.length > 0) {
                //set date
                var cell = $j('<span></span>');
                cell.addClass("acceptreject");
                var anchor = $j('<a href="#"></a>').appendTo(cell);
                anchor.addClass("mainlink viewacceptance");
                anchor.attr("data-acceptanceid",certIdAndValidationId['value']['key']);
                var span = $j('<span ></span>').appendTo(anchor);
                if(accept)
                	span.addClass("accepteddate certaccepted");
                else
                	span.addClass("accepteddate certrejected");
                span.text(today);
                anchorToFind.closest('tr').find('span.acceptreject').replaceWith(cell);
            }
            else {
            	anchorToFind = $j('a[data-acceptanceid=' + certIdAndValidationId['value']['value'] + ']');
            	if (anchorToFind.length > 0) {
                    //set date
                    var cell = $j('<span></span>');
                    cell.addClass("acceptreject");
                    var anchor = $j('<a href="#"></a>').appendTo(cell);
                    anchor.addClass("mainlink viewacceptance");
                    anchor.attr("data-acceptanceid",certIdAndValidationId['value']['value']);
                    var span = $j('<span ></span>').appendTo(anchor);
                    if(accept)
                    	span.addClass("accepteddate certaccepted");
                    else
                    	span.addClass("accepteddate certrejected");
                    span.text(today);
                    anchorToFind.closest('tr').find('span.acceptreject').replaceWith(cell);
                }
            }
        });


    });


    request.fail(function (jqXHR, textStatus) {
        alert("Request failed: " + jqXHR.responseText);
    });

}