/****************************************************************************************************************************************
 *                                                CWMS (Calibration Workflow Management System)
 *
 *                                                Copyright 2006, Antech Calibration Services
 *                                                            www.antech.org.uk
 *
 *                                                            All rights reserved
 *
 *                                                        Document author:    Stuart Harrold
 *
 *    FILENAME        :    WebCertSearchResults.js
 *    DESCRIPTION        :
 *    DEPENDENCIES    :   -
 *
 *    TO-DO            :    -
 *    KNOWN ISSUES    :    -
 *    HISTORY            :    05/05/2009 - SH - Created File
 *                    :    10/10/2015 - Tprovost - Manage i18n
 ****************************************************************************************************************************************/

/**
 * array of javascript filenames to be loaded into the page
 * unfortunately datetimepicker is still used by certificate acceptance code
 */
var pageImports = [
    '../script/thirdparty/jQuery/jquery.impromptu.js',
    '../script/web/jobs/certificate/WebCertAcceptance.js',
    '../script/thirdparty/jQuery/jquery.datetimepicker.full.min.js'];


/**
 * this function is called from the loadScript.init() function in CwmsMain.js if present.
 * this allows onload coding to be included in the page specific javascript file without adding
 * to the body onload attribute.
 */
function init() {
    // load stylesheet using the dom if not already
    loadScript.loadStyle('../styles/jQuery/jquery-boxy.css', 'screen');
    loadScript.loadStyle('../styles/jQuery/jquery.datetimepicker.css', 'screen');

    // if we have just arrived from search form reset list of selected ids
    // otherwise reselect any certificates on this page that were previously selected.
    if ($j("#initialResultsPage").val() === "true") {
        sessionStorage.setItem("selectedIds", "[]");
        $j('#initialResultsPage').val(false);
    } else {
        checkSelected();
    }


    $j('input[name=\"certSelector\"]').click(function () {
        var clickedId = this.id.substring(1, this.id.length);
        var storedIds = JSON.parse(sessionStorage.getItem("selectedIds"));
        if (this.checked) {
            storedIds.push(clickedId);
        } else {
            var index = storedIds.indexOf(clickedId);
            if (index > -1) {
                storedIds.splice(index, 1);
            }
        }
        $j("#totalselected").text(storedIds.length);
        if(storedIds.length > 0) {
            $j('button.selectedcertaction').removeAttr("disabled");
        } else {
            $j('button.selectedcertaction').attr("disabled", true);
        }
        // save new list into session storage
        sessionStorage.setItem("selectedIds", JSON.stringify(storedIds));
    });


    $j('#selectall').click(function () {
        var storedIds = JSON.parse(sessionStorage.getItem("selectedIds"));
        if(this.checked) {
            $j('input[name="certSelector"]').each(function () {
            	if($j(this).prop('checked') == true){
            		var index = storedIds.indexOf(this.id.substring(1, this.id.length));
            		if (index > -1) {
            			storedIds.splice(index, 1);
            		}
            	}
                $j(this).prop('checked',true);
                storedIds.push(this.id.substring(1, this.id.length));
            });
        } else {
            $j('input[name="certSelector"]').each(function () {
                $j(this).prop('checked',false);
                var index = storedIds.indexOf(this.id.substring(1, this.id.length));
                if (index > -1) {
                    storedIds.splice(index, 1);
                }
            });
        }
        $j("#totalselected").text(storedIds.length);
        if(storedIds.length > 0) {
            $j('button.selectedcertaction').removeAttr("disabled");
        } else {
            $j('button.selectedcertaction').attr("disabled", true);
        }
        // save new list into session storage
        sessionStorage.setItem("selectedIds", JSON.stringify(storedIds));
    });

    $j('#searchagain').click(function () {
        window.location.replace("certsearch.htm")
    });

    $j('#clearselections').click(function(){
        sessionStorage.setItem("selectedIds", "[]");
        $j('input[name="certSelector"]:checked').each(function () {
            $j(this).prop('checked',false);
        });
        $j("#totalselected").text(0);
//        $j('#acceptcerts').attr("disabled", true);
    });


    $j('#downloadcerts').click(function () {
        var storedIds = JSON.parse(sessionStorage.getItem("selectedIds"));
        var storedIdsString = storedIds.join(',');
        var form = $j("<form method='post' action='bulkcertdownload.zip'></form>");
        form.append($j('<input type="hidden" name="certids" value="' + storedIdsString + '"/>'));
        $j('body').append(form);
        form.submit();
    });

    $j('#downloadacceptdocs').click(function() {
        var storedIds = JSON.parse(sessionStorage.getItem("selectedIds"));
        var storedIdsString = storedIds.join(',');
        var form = $j("<form method='post' action='bulkacceptancedownload.zip'></form>");
        form.append($j('<input type="hidden" name="certids" value="' + storedIdsString + '"/>'));
        $j('body').append(form);
        form.submit();
    });

    // In WebCertAcceptance.js
    certAcceptanceInitialization();

}


//function to set correct certs back to selected when returning to a page
function checkSelected() {

    //get selected Ids from sessionstorage
    var certIdsSelected = JSON.parse(sessionStorage.getItem("selectedIds"));

    //set certs selected count
    $j("#totalselected").text(certIdsSelected.length);

    if(certIdsSelected.length > 0) {
//        $j('#acceptcerts').removeAttr("disabled");
        //loop through selectedIds
        certIdsSelected.forEach(function (id) {
            var checkbox = $j('#C' + id);
            //is that cert on this page?
            if (checkbox.length > 0) {
                //set checkbox to checked
                checkbox.prop('checked', true);
            }
        });
    } else {
//        $j('#acceptcerts').attr("disabled", true);
    }
}

function showCertificateValidation(certid, ele) {
	//alert(certid);
	if($j('#vaidationRow_'+certid).hasClass('hid')) {
		$j('#vaidationRow_'+certid).removeClass('hid').addClass('vis');
		$j(ele).attr('src', '../img/icons/delete.png');
		$j('#validatestatuses').removeAttr('disabled');
		getCalInstructions(certid);
		getCustomerAcceptanceCriteria(certid);
		getDeviation(certid);
	}
	else {
		$j('#vaidationRow_'+certid).removeClass('vis').addClass('hid');
		$j(ele).attr('src', '../img/icons/add.png');
		console.log($j("tr[id^='vaidationRow_']").hasClass('vis'));
		if(!$j("tr[id^='vaidationRow_']").hasClass('vis'))
			$j('#validatestatuses').attr('disabled', true);
	}
}

function getCalInstructions(certid) {

    //send ajax request
	//alert(certid);
    var request = $j.ajax({
        type: "GET",
        contentType: "application/json",
        url: "certificateacceptance/instructions/" + certid
    });

    request.done(function (results) {
        var instructions = '';
        $j.each(results, function (k, v) {
            if (k > 0) {
                instructions = instructions.concat("<br>");
            }
            instructions = instructions.concat(v);
        });
        if (instructions != null) {
            $j("#validationdialog-calinstructions_"+certid).html(instructions);
        }

    });

    request.fail(function (jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });

}

function getCustomerAcceptanceCriteria(certid) {

    //send ajax request
	//alert(certid);
    var request = $j.ajax({
        type: "GET",
        contentType: "application/json",
        url: "certificateacceptance/acceptancecriteria/" + certid
    });

    request.done(function (results) {
        var criteria = '';
        $j.each(results, function (k, v) {
            if (k > 0) {
                criteria = criteria.concat("<br>");
            }
            criteria = criteria.concat(v);
        });
        if (criteria != null) {
            $j("#validationdialog-customeracceptancecriteria_"+certid).html(criteria);
        }

    });

    request.fail(function (jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });

}

function getDeviation(certid) {

    //send ajax request
	//alert(certid);
    var request = $j.ajax({
        type: "GET",
        contentType: "application/json",
        url: "certificateacceptance/deviation/" + certid
    });

    request.done(function (result) {
       $j("#validationdialog-deviation_"+certid).html(result);
    });

    request.fail(function (jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });

}

function validateCertificateStatuses() {
	let data = new Array();
	let isValidated = true;
	$j("tr[id^='vaidationRow_']").each(function(index) {
		if($j(this).hasClass('vis')) {
			let rowId = $j(this).attr('id');
			let certId = rowId.replace('vaidationRow_', '');
			let certValDate = $j('#certValidationDate_'+certId).val();
			let certValStatus = $j('#certValidationStatus_'+certId).val();
			if(certValDate == undefined || certValDate == ''){
				isValidated = false;
				$j('#certRequiredDate_'+certId).removeClass('hid').addClass('vis');
			}
			if(certValStatus == undefined || certValStatus == ''){
				isValidated = false;
				$j('#certRequiredStatus_'+certId).removeClass('hid').addClass('vis');
			}
			data.push({
				certificateId: certId,
				status: $j('#certValidationStatus_'+certId).val(),
				dateValidated: $j('#certValidationDate_'+certId).val(),
				note: $j('#certValidationNote_'+certId).val()
			});
		}
	});
	//console.log(data);
	if(isValidated) {
	var request = $j.ajax({
        type: "POST",
        contentType: "application/json",
        url: "certificateacceptance/bulkadd",
        data: JSON.stringify(data),
        dataType: 'json'
    });

    request.done(function (results) {

        results.forEach(function (certIdAndValidationId) {
        	//set accepted date for any certs on this page that were just bulk accepted.
            var valDate = parseDate($j('#certValidationDate_'+certIdAndValidationId['key']).val()).toLocaleDateString();
            var valStatus = $j('#certValidationStatus_'+certIdAndValidationId['key']).val();
            var anchorToFind = $j('a[data-certid=' + certIdAndValidationId['key'] + ']');
            
            //is that cert on this page?
            if (anchorToFind.length > 0) {
                //set date
                var cell = $j('<span></span>');
                cell.addClass("acceptreject");
                var anchor = $j('<a href="#"></a>').appendTo(cell);
                anchor.addClass("mainlink viewacceptance");
                anchor.attr("data-acceptanceid",certIdAndValidationId['value']['key']);
                var span = $j('<span ></span>').appendTo(anchor);
                if(valStatus == 'ACCEPTED')
                	span.addClass("accepteddate certaccepted");
                else if(valStatus == 'REJECTED')
                	span.addClass("accepteddate certrejected");
                else
                	span.addClass("accepteddate certpending");
                span.text(valDate);
                anchorToFind.closest('tr').find('span.acceptreject').replaceWith(cell);
            }
            else {
            	anchorToFind = $j('a[data-acceptanceid=' + certIdAndValidationId['value']['value'] + ']');
            	if (anchorToFind.length > 0) {
                    //set date
                    var cell = $j('<span></span>');
                    cell.addClass("acceptreject");
                    var anchor = $j('<a href="#"></a>').appendTo(cell);
                    anchor.addClass("mainlink viewacceptance");
                    anchor.attr("data-acceptanceid",certIdAndValidationId['value']['key']);
                    var span = $j('<span ></span>').appendTo(anchor);
                    if(valStatus == 'ACCEPTED')
                    	span.addClass("accepteddate certaccepted");
                    else if(valStatus == 'REJECTED')
                    	span.addClass("accepteddate certrejected");
                    else
                    	span.addClass("accepteddate certpending");
                    span.text(valDate);
                    anchorToFind.closest('tr').find('span.acceptreject').replaceWith(cell);
                }
            }
        });


    });


    request.fail(function (jqXHR, textStatus) {
        alert("Request failed: " + jqXHR.responseText);
    });
    
    $j("tr[id^='vaidationRow_']").each(function() {
    	if($j(this).hasClass('vis')) {
    	var prevRow = $j(this).prev();
    	$j(this).removeClass('vis').addClass('hid');
    	prevRow.find('td:first img').attr('src', '../img/icons/add.png');
    	}
    });
    $j('#validatestatuses').attr('disabled', true);
	}
}


