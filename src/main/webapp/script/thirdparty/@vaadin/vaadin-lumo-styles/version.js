class Lumo extends HTMLElement {
  static get version() {
    return '1.6.0';
  }
}

customElements.define('vaadin-lumo-styles', Lumo);

export { Lumo };
