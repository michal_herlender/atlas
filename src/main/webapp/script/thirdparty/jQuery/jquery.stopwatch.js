/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*
*												Copyright 2008, Antech Calibration Services
*															www.antech.org.uk
*
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	jquery.stopwatch.js
*	DESCRIPTION		:	 
*	DEPENDENCIES	:   
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			:	26/02/2008 - SH - Created File
*	OPTIONS			: 	fontSize - provides font size to timer 
*						fontWeight - provides font weight to timer
*						(e.g. $j('div').stopwatch({font: '16', fontWeight: 'bold'}) )
*
****************************************************************************************************************************************/

(function( $j )
{
	var timercount = 0;
   
    var timestart  = null;
    
    $j.fn.stopwatch = function( options )
    {
        var options = $j.extend({
			
			/**
			 * font size
			 */
			fontSize:		'12',
			
			/**
			 * font weight
			 */
			fontWeight:		'normal'
			
        }, options);

        return this.each(function()
        {
            StopWatch(this, options);
        });
    };

    function StopWatch( element, options )
    {
        return this instanceof StopWatch
            ? this.init(element, options)
            : new StopWatch(element, options);
    }

    $j.extend(StopWatch.prototype,
    {
        original : null,
        options  : {},

        element  : null,
        searchDiv   : null,
		
		init : function( element, options )
        {
			// initialise element
            this.element = element;
			// initialise options
            this.options = options || {};
			
			var self = this;
			
			this.drawStopWatch(self);
			
			this.start(self);
		},
		
		drawStopWatch : function(self){
			// append plugin content to main div
			$j(this.element).append('<span id="timearea">00:00:00</span>');
			$j(this.element).find('span')
							.css('font-size', this.options.fontSize + 'px')
							.css('font-weight', this.options.fontWeight);
		},

        showtimer : function(self) {
  			if(timercount) {
  				clearTimeout(timercount);
				clockID = 0;
			}
			if(!timestart){
				timestart = new Date();
			}
			var timeend = new Date();
			var timedifference = timeend.getTime() - timestart.getTime();
			timeend.setTime(timedifference);
			var hours_passed = timeend.getHours();
			if (hours_passed <10){
				hours_passed = '0' + hours_passed;
			}
			var minutes_passed = timeend.getMinutes();
			if(minutes_passed <10){
				minutes_passed = '0' + minutes_passed;
			}
			var seconds_passed = timeend.getSeconds();
			if(seconds_passed <10){
				seconds_passed = '0' + seconds_passed;
			}
			$j(this.element).find('span#timearea').text(hours_passed + ':' + minutes_passed + ':' + seconds_passed);
			timercount = setTimeout(function(){self.showtimer(self)}, 1000);
		},
		
        start : function(self){
			timestart   = new Date();
			$j(this.element).find('span#timearea').text('00:00:00');
			timercount = setTimeout(function(){self.showtimer(self)}, 1000);
      	}
		
		/*
		stop : function() {
			if(timercount) {
				clearTimeout(timercount);
				timercount  = 0;
				var timeend = new Date();
				var timedifference = timeend.getTime() - timestart.getTime();
				timeend.setTime(timedifference);
				var minutes_passed = timeend.getMinutes();
				if(minutes_passed <10){
					minutes_passed = '0' + minutes_passed;
				}
				var seconds_passed = timeend.getSeconds();
				if(seconds_passed <10){
					seconds_passed = '0' + seconds_passed;
				}
				var milliseconds_passed = timeend.getMilliseconds();
				if(milliseconds_passed <10){
					milliseconds_passed = '00' + milliseconds_passed;
				}
				else if(milliseconds_passed <100){
					milliseconds_passed = '0' + milliseconds_passed;
				}
              $j(this.element).find('span#timearea').text(minutes_passed + ':' + seconds_passed + '.' + milliseconds_passed);
          }
          timestart = null;
      	},
		
		reset : function() {
        	timestart = null;
			$j(this.element).find('span#timearea').text('00:00:00');
		}
		*/
        
    });
})(jQuery);
