/****************************************************************************************************************************************
*												CWMS (Calibration Workflow Management System)
*			
*												Copyright 2006, Antech Calibration Services
*															www.antech.org.uk
*		
*															All rights reserved
*
*														Document author: 	Stuart Harrold
*
*	FILENAME		:	jquery-antechplugins.js
*	DESCRIPTION		:	file containing jquery plugins.
*	DEPENDENCIES	:   -
*
*	TO-DO			: 	-
*	KNOWN ISSUES	:	-
*	HISTORY			: 	09/10/2007 - SH - Created file
*
****************************************************************************************************************************************/

/**
 * this jquery plugin toggles checkboxes (checks/unchecks them)
 */
jQuery.fn.toggleCheck = function() 
{
	return this.each(function() 
   	{
		this.checked = !this.checked;
    });
};

/**
 * this jquery plugin selects all checkboxes
 */
jQuery.fn.check = function(mode)
{
	// if mode is undefined, use 'on' as default
	var mode = mode || 'on';
	
	return this.each(function()
	{
		switch(mode) 
		{
			case 'on':		this.checked = true;
					 		break;
			
			case 'off':		this.checked = false;
			 				break;
						
			case 'toggle':	this.checked = !this.checked;
			 				break;
		}
	});
};

/**
 * this jquery plugin applies a zebra effect to table rows
 */
jQuery.fn.zebra = function()
{
	// declare class names
	var even = 'even';
	var odd = 'odd';
	// find table tbody and all rows, then filter
	// by odd and even applying new class name
	jQuery(this).find('tbody')
				.find('tr')
				.filter(':even')
				.removeClass()
				.addClass(even)
				.end()
				.filter(':odd')
				.removeClass()
				.addClass(odd);
};

jQuery.fn.clipBoard = function(img, copytext, paste, pasteElemId)
{
	// Try it! (Internet Explorer being used?)
	try {
		// img null
		if (img != null) {
			// change image to selected
			jQuery(img).attr('width', '14').attr('src', 'img/icons/bullet_clipboard_selected.png');
		}
		// append temporary textarea to page
		jQuery(this).parent().prepend('<textarea id="copytocliptext" contentEditable="true"></textarea>');
		// variable to hold copy text
		var copyText = '';
		// check for copy text passed as parameter
		if (copytext != null && copytext != '') {
			copyText = copytext;
		}
		else {
			// has the text been broken?
			if (($j(this).html().indexOf('<br>') != -1) || ($j(this).html().indexOf('<BR>') != -1)) {
				copyText = this.html().replace(/\n/g,'').trim();
				copyText = jQuery.trim(copyText.replace(/<br>/g, '\n').replace(/<BR>/g, '\n').replace(/\t/g, '').replace(/&amp;/g, '&'));
			}
			else {
				copyText = jQuery.trim(jQuery(this).html().replace(/&amp;/g, '&').replace(/\t/g, ''));
			}
		}
		// add copy text to textarea
		jQuery('textarea#copytocliptext').text(copyText);
		var copyTextarea = document.querySelector('textarea#copytocliptext');
		copyTextarea.select();
		document.execCommand("copy");
		// paste?
		if (paste) {
			// call method to paste symbol into element
			Paste(document.getElementById(pasteElemId));
			// method to paste symbol into element
			function Paste(theObject)
			{				
				var tempval=eval(theObject);
				if (document.all)
				{
					tempval.focus();
					document.execCommand("Paste");
				}				
			}
		}
	}
	catch(ex)
	{
		jQuery.prompt('This functionality is not supported by your browser');	
	}
	finally {
		// remove textarea
		jQuery('textarea#copytocliptext').remove();
		// img null
		if (img != null)
		{
			// delay changing the image back by 2 seconds so user sees the action
			jQuery(img).animate({fadeOut: 'fast'}, 2000, function()
			{
				jQuery(this).attr('width', '10').attr('src', 'img/icons/bullet_clipboard.png');
			});
		}		
	}
};

/**
 * selectbox-utils for jQuery
 * 
 * Copyright (c) 2007 Yoshiomi KURISU
 * Licensed under the MIT (MIT-LICENSE.txt)  licenses.
 * 
 * @example  $('#year1').numericOptions({from:2007,to:2011});
 * @example  $('#month1').numericOptions({from:1,to:12,selectedIndex:5,namePadding:2});
 * @example  $('#date1').numericOptions().datePulldown({year:$('#year1'),month:$('#month1')});
 * 
 */
(function() {
	 
	jQuery.fn.numericOptions = function(settings){
		settings = jQuery.extend({
			remove:true
			,from:1
			,to:31
			,selectedIndex:0
			,valuePadding:0
			,namePadding:0
		},settings);
		//error check
		if(!(settings.from+'').match(/^\d+$/)||!(settings.to+'').match(/^\d+$/)||!(settings.selectedIndex+'').match(/^\d+$/)||!(settings.valuePadding+'').match(/^\d+$/)||!(settings.namePadding+'').match(/^\d+$/)) return;
		if(settings.from > settings.to) return;
		if(settings.to - settings.from < settings.selectedIndex) return;
		//add options
		if(settings.remove) this.children().remove();
		var padfunc = function(v,p){
			if((''+v).length < p){
				for(var i = 0,l = p - (v+'').length;i < l ;i++){
					v = '0' + v;
				}
			}
			return v;			
		};
		for(var i=settings.from,j=0;i<=settings.to;i++,j++){
			this.each(function(){
				this.options[j] = new Option(padfunc(i,settings.namePadding),padfunc(i,settings.valuePadding));
				});
		}
		this.each(function(){
				if(jQuery.browser.opera){
					this.options[settings.selectedIndex].defaultSelected = true;
				}else{
					this.selectedIndex = settings.selectedIndex;
				}
			});
		return this;
	};
	//
	jQuery.fn.datePulldown = function(settings){
		if(!settings.year || !settings.month) return ;
		var y = settings.year;
		var m = settings.month;
		if(!y.val() || !m.val()) return;
		if(!y.val().match(/^\d{1,4}$/)) return;
		if(!m.val().match(/^[0][1-9]$|^[1][1,2]$|^[0-9]$/)) return;

		var self = this;
		var fnc = function(){
			var tmp = new Date(new Date(y.val(),m.val()).getTime() - 1000);
			var lastDay = tmp.getDate() - 0;
			self.each(function(){
				var ind = (this.selectedIndex<lastDay-1)?this.selectedIndex:lastDay-1;
				this.selectedIndex = ind;
				$(this).numericOptions({to:lastDay,selectedIndex:ind});
			});
		};
		y.change(fnc);
		m.change(fnc);
		return this;	
	};
	
})(jQuery);
