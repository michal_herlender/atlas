(function ($j) {
	
	$j.event.special.textchange = {
		
		setup: function (data, namespaces) {
			$j(this).bind('keyup.textchange', $j.event.special.textchange.handler);
			$j(this).bind('cut.textchange paste.textchange input.textchange', $j.event.special.textchange.delayedHandler);
		},
		
		teardown: function (namespaces) {
			$j(this).unbind('keyup', a.event.special.textchange.handler);
			$j(this).unbind('cut paste input', a.event.special.textchange.delayedHandler);
		},
		
		handler: function (event) {
			$j.event.special.textchange.triggerIfChanged($j(this));
		},
		
		delayedHandler: function (event) {
			var element = $j(this);
			setTimeout(function () {
				$j.event.special.textchange.triggerIfChanged(element);
			}, 25);
		},
		
		triggerIfChanged: function (element) {
		  var current = element.attr('contenteditable') ? element.html() : element.val();
			if (current !== element.data('lastValue')) {
				element.trigger('textchange',  element.data('lastValue'));
				element.data('lastValue', current);
			}
		}
	};
	
	$j.event.special.hastext = {
		
		setup: function (data, namespaces) {
			$j(this).bind('textchange', $j.event.special.hastext.handler);
		},
		
		teardown: function (namespaces) {
			$j(this).unbind('textchange', $j.event.special.hastext.handler);
		},
		
		handler: function (event, lastValue) {
			if ((lastValue === '' || lastValue === undefined) && lastValue !== $j(this).val()) {
				$j(this).trigger('hastext');
			}
		}
	};
	
	$j.event.special.notext = {
		
		setup: function (data, namespaces) {
			$j(this).bind('textchange', $j.event.special.notext.handler);
		},
		
		teardown: function (namespaces) {
			$j(this).unbind('textchange', $j.event.special.notext.handler);
		},
		
		handler: function (event, lastValue) {
			if ($j(this).val() === '' && $j(this).val() !== lastValue) {
				$j(this).trigger('notext');
			}
		}
	};	

})(jQuery);