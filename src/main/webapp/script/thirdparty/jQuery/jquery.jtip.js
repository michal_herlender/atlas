/**
 * JTip
 * By Cody Lindley (http://www.codylindley.com)
 * Under an Attribution, Share Alike License
 * JTip is built on top of the very light weight jquery library.
 * 
 * Last modification :
 * - 2006-09-21 - Rey Bango and Karl Swedberg - fix issue with FF 
 * - 2016-10-20 - TP - fix issue with title containing quotes
 * - 2019-02-21 - Galen Beck - fix issue where Tooltip script was reloaded for each new overlay! 
 * 
 */

/**
 * variable to measure timeout length
 */
var timer = 0;
/**
 * variable used globally to load mouseover events
 */
var mouseOver = false;

/**
 * variable to flag whether the tooltip is being moused over
 */
var mouseOverSignal = false;

/**
 * variable to flag whether the tooltop javascript file has been loaded
 */
var tooltipScriptLoaded = false;

// on page load (as soon as its ready) call JT_init
// this is now called from CWMS_Main.js init() function when looking for jtip elements
//$j(document).ready(JT_init);

function JT_init()
{

	// 9/21/06 - Rey Bango added hide() method to correct an issue with FF
	$j("a.jTip").hover(	function()
								{
								mouseOver = true;
								// 20161020 - TP - Escape the quote character in the title to prevent bug when the JT_Show function is called
								timer = setTimeout(	"if ((mouseOver == true) && (!$j('#JT').length))" +
													"{" +
													"JT_show( '" + this.href + "', '" + this.id + "', '" + this.title.replace(/'/g, "%27") + "');" +
													"}" +
													"", 200);
								},
						function()
								{
									mouseOver = false;
									setTimeout('if (mouseOverSignal == false)$j(\'#JT\').remove();', 200);
									clearTimeout(timer);
								})
				.click(function(){return false;});
	/*
    * separate jtip function created by Stuart which looks for an anchor with a class of jconTip
    * if present, when hovered over a timer is initiated and after 1.5 seconds the tooltip is shown
    * if the user mouses out of the hover then the timer is reset.
    * GB 2015-10-15 modified slightly to check for href attribute and use native behavior if present
    * as a double request was occurring sometimes.
    */
   $j("a.jconTip").hover(	function()
				   					{
										clearTimeout(timer);
				   						mouseOver = true;
				   						// 20161020 - TP - Escape the quote character in the title to prevent bug when the JT_Show function is called
				   						timer = setTimeout(	"if ((mouseOver == true) && (!$j('#JT').length))" +
															"{" +
															"JT_show( '" + this.href + "', '" + this.id + "', '" + this.title.replace(/'/g, "%27") + "');" +
															"}" +
															"", 1400);													           						
				   					},
				   			function()
				   					{
										mouseOver = false;
										setTimeout('if (mouseOverSignal == false)$j(\'#JT\').remove();', 200);
										clearTimeout(timer);
				   					})
				   .click(function()
						   			{
					   					if ($j(this).attr('onclick'))
					   					{
					   						return false;
					   					}
					   					else if ($j(this).attr('href'))
					   					{
					   						return true;
					   					}
					   					else
					   					{
					   						window.location.href = this.href;
					   						return false;
					   					} 
					   				});
	
	/**
	 * loop through all anchors with the 'jconTip' class name that do not have an id and
	 * create a new custom id using the 'RandomId' function.
	 */
	$j("a.jconTip").each(function(i)
	{
		if (this.id == '')
		{
			var randomId = RandomId();
			this.id = randomId;
		}
	});
	
	/**
	 * this function creates a random set of characters for custom id's
	 */
	function RandomId()
	{
		strChars = 'ABCDEFGHIJKLMNOPQRSTUVWXTZ';
		var randomLength = 9;
	     
		var randomstring = '';
		for (var i = 0; i < randomLength; i++)
		{
			var rnum = Math.floor(Math.random() * strChars.length);
			randomstring += strChars.substring(rnum, rnum + 1);
		}
	   	
		return randomstring;
	} 
}

function JT_show(url,linkId,title)
{
	if(title == false)title="&nbsp;";
	// 20161020 - TP - Unescape the quote character in the title (escape in the JT_init() function) to display it correctly
	title = title.replace(/%27/g, "'");
	var de = document.documentElement;
	var w = self.innerWidth || (de&&de.clientWidth) || document.body.clientWidth;
	var hasArea = w - getAbsoluteLeft(linkId);
	var clickElementy = getAbsoluteTop(linkId) - 3; //set y position
	
	var queryString = url.replace(/^[^\?]+\??/,'');
	var params = parseQuery( queryString );
	if(params['width'] === undefined){params['width'] = 250};
	if(params['link'] !== undefined){
	$j('#' + linkId).bind('click',function(){window.location = params['link']});
	$j('#' + linkId).css('cursor','pointer');
	}
	if(hasArea>((params['width']*1)+75)){
		$j("body").append("<div id='JT' style='width:"+params['width']*1+"px'><div id='JT_arrow_left'></div><div id='JT_close_left'>"+title+"</div><div id='JT_copy'><div class='JT_loader'><div></div></div>");//right side
		var arrowOffset = getElementWidth(linkId) + 11;
		var clickElementx = getAbsoluteLeft(linkId) + arrowOffset; //set x position
	}else{
		$j("body").append("<div id='JT' style='width:"+params['width']*1+"px'><div id='JT_arrow_right' style='left:"+((params['width']*1)+1)+"px'></div><div id='JT_close_right'>"+title+"</div><div id='JT_copy'><div class='JT_loader'><div></div></div>");//left side
		var clickElementx = getAbsoluteLeft(linkId) - ((params['width']*1) + 15); //set x position
	}
	
	$j('#JT').css({left: clickElementx+"px", top: clickElementy+"px"});
   	/*
	 * this check was added by Stuart.  If a parameter of 'id' has been included within the url then
	 * the JT_loader image should be hidden and the tooltip should be displayed with the contents of an
	 * element hidden on the page whose id is stored in the 'id' parameter. 
	 */
	if (params['id'] && !params['ajax'])
	{		
		$j('div').remove('.JT_loader');
		$j('#JT_copy').append($j(params['id']).html());
		$j('#JT').show();
		$j('#JT').hover(	function()	{
											mouseOverSignal = true;
										},
							function()	{
											$j('#JT, #JT_arrow_left, #JT_arrow_right').hide().remove();
											mouseOverSignal = false; 
											return false;
										});
	}
	/*
	 * this check was added by Stuart.  If a parameter of 'ajax' has been included within the url then
	 * the tooltip should be displayed with the contents of a DWR call made within TooltipDWRInfo.js.
	 */
	else if (params['ajax'])
	{
		// set default tooltip dwr info file
		var script = 'script/trescal/core/utilities/TooltipDWRInfo.js';
		// if this is a web request set new path
		if (params['web'])
		{
			script = '../script/web/utilities/TooltipDWRInfo.js';
		}			
		// load tooltip content file (changed from .getScript to regular loader for caching)
		loadScript.loadScriptsDynamic(new Array(script), true,
		{
			callback: function()
			{	
				if(params['ajax'] == 'currency')
				{
					// value passed over is not numeric
					if (!IsNumeric(params[1]))
					{
						// get value from the input field with id passed
						var valueToConvert = document.getElementById(params[1]).value;
						// add value back to original parameter
						params[1] = valueToConvert;
					}
					// call method to select correct content to display
					filterTooltipRequest(params['ajax'], params[0], params[1]);
				}
				else
				{					
					// call method to select correct content to display
					filterTooltipRequest(params['ajax'], params[0], '');
				}
				// set variable to indicate that the tooltip script file has been loaded
				tooltipScriptLoaded = true;
			}
		});
	}
	/*
	 * this check was added by Stuart.  If a parameter of 'helpid' has been included within the url then
	 * the tooltip should be displayed with the contents of a DWR call to help messages.
	 */
	else if (params['helpid'])
	{
		// dwr call to go here
		
		$j('div').remove('.JT_loader');
		$j('#JT_copy').append('<p>This is where help info can be displayed</p>');
		$j('#JT').show();
		$j('#JT').hover(	function()	{
											mouseOverSignal = true;
										},
							function()	{
											$j('#JT, #JT_arrow_left, #JT_arrow_right').hide().remove();
											mouseOverSignal = false; 
											return false;
										});
	}
	/*
	 * this is the default action which was present before Stuart's intervention
	 */
	else
	{
		$j('#JT').show();
		$j('#JT_copy').load(url);
	}
	
} // end JT_show()

function getElementWidth(objectId) {
	x = document.getElementById(objectId);
	return x.offsetWidth;
}

function getAbsoluteLeft(objectId) {
	// Get an object left position from the upper left viewport corner
	o = document.getElementById(objectId)
	oLeft = o.offsetLeft            // Get left position from the parent object
	while(o.offsetParent!=null) {   // Parse the parent hierarchy up to the document element
		oParent = o.offsetParent    // Get parent object reference
		oLeft += oParent.offsetLeft // Add parent left position
		o = oParent
	}
	return oLeft
}

function getAbsoluteTop(objectId) {
	// Get an object top position from the upper left viewport corner
	o = document.getElementById(objectId)
	oTop = o.offsetTop            // Get top position from the parent object
	while(o.offsetParent!=null) { // Parse the parent hierarchy up to the document element
		oParent = o.offsetParent  // Get parent object reference
		oTop += oParent.offsetTop // Add parent top position
		o = oParent
	}
	return oTop
}

function parseQuery ( query ) {
   var Params = new Object ();
   if ( ! query ) return Params; // return empty object
   var Pairs = query.split(/[;&]/);
   for ( var i = 0; i < Pairs.length; i++ ) {
      var KeyVal = Pairs[i].split('=');
      if ( ! KeyVal || KeyVal.length != 2 ) continue;
      var key = unescape( KeyVal[0] );
      var val = unescape( KeyVal[1] );
      val = val.replace(/\+/g, ' ');
      Params[key] = val;
	  Params[i] = val;
   }
   return Params;
}

function blockEvents(evt) {
              if(evt.target){
              evt.preventDefault();
              }else{
              evt.returnValue = false;
              }
}

function IsNumeric(sText)
{
	var ValidChars = "0123456789.";
	var IsNumber=true;
	var Char;

	for (i = 0; i < sText.length && IsNumber == true; i++) 
	{ 
		Char = sText.charAt(i); 
		if (ValidChars.indexOf(Char) == -1) 
		{
		IsNumber = false;
		}
	}
	return IsNumber;
}
