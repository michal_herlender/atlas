/* Licence:
*   Use this however/wherever you like, just don't blame me if it breaks anything.
*
* Credit:
*   If you're nice, you'll leave this bit:
*
*   Class by Pierre-Alexandre Losson -- http://www.telio.be/blog
*   email : plosson@users.sourceforge.net
*
* Some minor modifications done by Gunnar Hillert, gunnar@hillert.com
* Modifications made by Richard Dean.
* Conversion to jQuery Stuart Harrold.
*/

var countId = 0;

function refreshProgress()
{
    UploadMonitor.getUploadInfo(updateProgress);
}

function updateProgress(uploadInfo)
{
	if (uploadInfo.inProgress)
    {	
        var fileIndex = uploadInfo.fileIndex;
		// calculate percentage of file already uploaded
        var progressPercent = Math.ceil((uploadInfo.bytesRead / uploadInfo.totalSize) * 100);
		// change the upload progress text
        $j('#progressBar' + countId + ' span').text('Upload in progress: ' + progressPercent + '%, transfered ' + uploadInfo.bytesRead + ' of ' + uploadInfo.totalSize + ' bytes');
        // change the upload progress bar width dependant on percentage of file already uploaded       
	    $j('#progressBar' + countId + ' div > div').css({ width: parseInt(progressPercent) + '%' });
		// set timeout
        window.setTimeout('refreshProgress()', 500);
	}
	else if(uploadInfo.beingVerified)
	{		
		window.setTimeout('refreshProgress()', 500);
	}
    else
    {
		if(uploadInfo.failedFileType)
		{
			alert('Filetype not allowed!!.');
		}
		else if(uploadInfo.failedFileSize)
		{
			alert('File size too large, max upload size ' + uploadInfo.maxFileSize + ' bytes');
		}
		else
		{
			// change the upload progress bar width dependant on percentage of file already uploaded       
	    	$j('#progressBar' + countId + ' div > div').css({ width: '100%', background: '#A4D176' });
			// change the count of file resources available
			$j('#progressBar' + countId + ' span').css({ fontWeight: 'bold'}).text('File "' + uploadInfo.fileName + '" uploaded successfully');
			// change the count of file resources available
			$j('.fileResourceCount').text(parseInt($j('.fileResourceCount:first').text()) + 1);
		}
		
		// enable the upload button
		$j('#uploadbutton' + countId).removeAttr('disabled');
		// enable file upload button and clear contents of input field
        $j('#file1' + countId).removeAttr('disabled').attr('value', '');
		// hide the progress bar
		setTimeout("$j('#progressBar' + countId).removeClass().addClass('hid');", 2000);
		// this checks for a method called after upload which can be included in
		// the page specific javascript file if extra functionality is required after upload
		if (typeof afterUpload != 'undefined')
		{
			afterUpload(uploadInfo.fileName);
		}
		if (typeof afterImageUpload != 'undefined')
		{
			if(uploadInfo.form)
			{
				// create new javascript image object to minipulate width and height if necessary
				var image = new Image();
				// when loaded action
				image.onload = function()
				{
					// find longest edge of image
					var longestEdge = (image.height >= image.width) ? image.height : image.width;
					// set max width length (taken from 'imageHolder' width/height)
					var imageWidthMaxEdge = 280;					
					// image larger than display area?
					if (imageWidthMaxEdge < longestEdge)
					{
						// get scale value
						var scale = imageWidthMaxEdge / longestEdge;
						// re-scale image
						image.width = (image.width * scale);
						image.height = (image.height * scale);
					}
					afterImageUpload(image.width, image.height, uploadInfo.form.entityType, uploadInfo.form.entityId, uploadInfo.fileId);
				};
				// load image using path
				//image.src = uploadInfo.form.relativeImagePath;
				image.src = "displayimage?id=" + uploadInfo.fileId;
			}
		}
	}
	return true;
}

function startProgress(count)
{
	// set countId variable for use in ids
	countId = count;
	// show the progress bar
	$j('#progressBar' + countId).removeClass().addClass('vis');
	// set the upload progress bar width to zero
	$j('#progressBar'  + countId + ' div > div').css({ width: '0px' });
	// set the upload progress text to default value
    $j('#progressBar'  + countId + ' span').text('upload in progress: 0%');
	// disable the upload button
    $j('#uploadbutton' + countId).attr('disabled', 'disabled');
    
	// wait a little while to make sure the upload has started ..
    window.setTimeout("refreshProgress()", 500);
	// show the progress bar
    $j('#progressBar' + countId).removeClass().addClass('vis');
	return true;
}
