<%@ tag language="java" display-name="showInstructions" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image"%>

<%@ attribute name="instructions" required="true" type="java.lang.Object" %>

<c:if test="${not empty instructions}">
	<table id="instructiontable" class="default4 highlight_table" summary="This table displays any instructions">
		<thead>
			<tr>
				<th><spring:message code="instructiontype.readinstruction" /></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="instruction" items="${instructions}">
				<tr>
					<td>
						<c:choose>
							<c:when test="${instruction.isSubdivInstruction}">
								<image:businessSubdivisionSpecific/>
								<strong>[${instruction.businessSubdiv}]</strong>
							</c:when>
							<c:when test="${instruction.linkedToType == 'JOB' || instruction.linkedToType == 'CONTRACT'}">
							</c:when>
							<c:otherwise>
								<image:businessCompanySpecific/>
							</c:otherwise>
						</c:choose>
						<strong>
							<spring:message code="${instruction.linkedToType.messageCode}"/>
							(${instruction.linkedToName}) -
							<spring:message code="${instruction.type.messageCode}"/>:
						</strong>
						<span
							<c:choose>
								<c:when test='${datetools.isDateAfterXTimescale(-1, "m", instruction.lastModified)}'>
									class="red bold"
								</c:when>
								<c:when test='${datetools.isDateAfterXTimescale(-3, "m", instruction.lastModified)}'>
									class="amber bold"
								</c:when>
								<c:otherwise>
									class="brown bold"
								</c:otherwise>
							</c:choose>
						>
							${instruction.instruction}
						</span>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>