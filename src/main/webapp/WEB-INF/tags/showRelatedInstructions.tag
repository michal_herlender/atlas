<%@ tag language="java" display-name="showRelatedInstructions" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image"%>

<%@ attribute name="job" required="true" type="java.lang.Object" %>
<%@ attribute name="highlightTypes" required="true" type="java.lang.Object" %>

<c:set var="highlightList" value='${highlightTypes.split(",")}'/>
<!-- nifty corners div -->
<div class="infobox">
	<c:if test="${ job.contractInstructionsSize > 0 }">
		<br/>
		<span class="bold error accesslink"><spring:message code="warning" /> : </span>
		<span class="error"><spring:message code="viewjob.contractinstrs.warning" /></span>
	</c:if>
	<h5><spring:message code="viewjob.jobinstr"/> <a href="addinstruction.htm?jobid=${job.jobid}" class="mainlink-float"><spring:message code="jifaultreport.addinstr"/></a></h5>
	<!-- clears floats and restores page flow -->
	<div class="clear"></div>
	<cwms-instructions link-jobid="${ job.jobid }" show-mode="RELATED"></cwms-instructions>
<!-- 	<fieldset> -->
<%-- 		<legend><spring:message code="company.instructions"/></legend> --%>
<!-- 		<ol id="instructionList"> -->
<%-- 			<c:choose> --%>
<%-- 				<c:when test="${job.relatedInstructionLinks.size() < 1}"> --%>
<%-- 					<li><spring:message code="company.therearecurrentlynoinstructionstodisplay"/></li> --%>
<%-- 				</c:when> --%>
<%-- 				<c:otherwise> --%>
<%-- 					<c:forEach var="i" items="${job.relatedInstructionLinks}"> --%>
<%-- 						<c:set var="bgColor" value=""/> --%>
<%-- 						<c:forEach var="h" items="${highlightList}"> --%>
<%-- 							<c:if test="${h == i.instruction.instructiontype.type}"> --%>
<%-- 								<c:set var="bgColor" value="background-color: yellow;"/> --%>
<%-- 							</c:if> --%>
<%-- 						</c:forEach> --%>
<%-- 						<li id="instruction${i.instruction.instructionid}" style="${bgColor}"> --%>
<!-- 							<div class="notes_note instructions_note"> -->
<%-- 								<c:set var="isCompanyOrSubdiv" value="${i.instructionEntity == 'COMPANY' || i.instructionEntity == 'SUBDIV'}" /> --%>
<%-- 								<strong>${i.instruction.instructiontype.type}:</strong><br /> --%>
<%-- 								<spring:message code="${i.instructionEntity.messageCode}"/> --%>
<%-- 								<c:choose> --%>
<%-- 									<c:when test="${i.instructionEntity.name() == 'CONTACT'}"> --%>
<%-- 										${i.contact.name} --%>
<%-- 									</c:when> --%>
<%-- 									<c:when test="${i.instructionEntity.name() == 'SUBDIV'}"> --%>
<%-- 										${i.subdiv.subname} --%>
<%-- 									</c:when> --%>
<%-- 									<c:when test="${i.instructionEntity.name() == 'COMPANY'}"> --%>
<%-- 										${i.company.coname} --%>
<%-- 									</c:when> --%>
<%-- 								</c:choose> --%>
<!-- 								<br/> -->
<%-- 								<c:choose> --%>
<%-- 									<c:when test="${isCompanyOrSubdiv && i.subdivInstruction }"> --%>
<%-- 										<image:businessSubdivisionSpecific/> --%>
<%-- 									</c:when> --%>
<%-- 									<c:otherwise> --%>
<%-- 										<image:businessCompanySpecific/> --%>
<%-- 									</c:otherwise> --%>
<%-- 								</c:choose> --%>
<%-- 								<c:if test="${isCompanyOrSubdiv && i.subdivInstruction }"> --%>
<%-- 									<strong>[${i.businessSubdiv.subname}]</strong>  --%>
<%-- 								</c:if> --%>
<%-- 								<c:choose> --%>
<%-- 									<c:when test='${datetools.isDateAfterXTimescale(-1, "m", i.instruction.lastModified)}'> --%>
<%-- 										<span class="red">${i.instruction.instruction}</span> --%>
<%-- 									</c:when> --%>
<%-- 									<c:when test='${datetools.isDateAfterXTimescale(-3, "m", i.instruction.lastModified)}'> --%>
<%-- 										<span class="amber">${i.instruction.instruction}</span> --%>
<%-- 									</c:when> --%>
<%-- 									<c:otherwise> --%>
<%-- 										<span class="brown">${i.instruction.instruction}</span> --%>
<%-- 									</c:otherwise> --%>
<%-- 								</c:choose> --%>
<!-- 							</div> -->
<%-- 							<div class="notes_editor"><t:showLastModified versionedEntity="${i.instruction}"/></div> --%>
<!-- 							<div class="notes_edit instructions_edit"> -->
<%-- 								<a href="editinstruction.htm?linkid=${i.id}&entity=${i.instructionEntity}" class="imagelink"> --%>
<%-- 									<img src="img/icons/note_edit.png" width="16" height="16" alt="<spring:message code='company.editinstruction'/>" title="<spring:message code='company.editinstruction'/>" /> --%>
<!-- 								</a> -->
<%-- 								<c:if test="${i.instructionEntity.name() == 'JOB'}"> --%>
<%-- 									<a href="deleteinstruction.htm?linkid=${i.id}&entity=${i.instructionEntity}" class="imagelink"> --%>
<%-- 										<img src="img/icons/note_delete.png" width="16" height="16" alt="<spring:message code='company.deleteinstruction'/>" title="<spring:message code='company.deleteinstruction'/>" /> --%>
<!-- 									</a> -->
<%-- 								</c:if> --%>
<!-- 							</div> -->
<!-- 							clear floats and restore page flow -->
<!-- 							<div class="clear-0"></div> -->
<!-- 						</li> -->
<%-- 					</c:forEach> --%>
<%-- 				</c:otherwise> --%>
<%-- 			</c:choose> --%>
<!-- 		</ol>					 -->
<!-- 	</fieldset> -->
</div>