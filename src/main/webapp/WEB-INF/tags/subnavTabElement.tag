<%@ tag language="java" display-name="subnavTabElement" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ attribute name="tab" required="true" type="java.lang.String" %>
<%@ attribute name="selectedTab" required="true" type="java.lang.String" %>
<%@ attribute name="url" required="true" type="java.lang.String" %>
<%@ attribute name="titleCode" required="true" type="java.lang.String" %>
<%@ attribute name="displayCode" required="true" type="java.lang.String" %>
<%--
	Individual navigation tab element, multiple tabs are meant to be included in a structure such as:

	<div id="subnav">
		<dl>
			... multiple tabs go here ...
		<dl>
	</div>	 

	Parameters:
	
	@tab is the identifier of this tab, e.g. "edit", <a> link will be given an id of tab-link e.g. edit-link
	@selectedTab is an internal String identifier indicating which tab is selected, matching the tab field
	@url is the URL to be displayed for the tab (via <c:url>)
	@titleCode is the message code to be used for looking up the localized text for the title (hover text)
	@displayCode is the message code to be used for the displayed text of the link 
	(avoiding repetitive logic to test which is active is primarily why this is encapsulated into a tab)
--%>
<dt>
	<a href="<c:url value="${url}" />"
		<c:if test="${tab == selectedTab }">class="selected"</c:if>
		id="${tab}-link" title="<spring:message code="${titleCode}"/>">
		<spring:message code="${displayCode }"/>
	</a>
</dt>