<%@ tag language="java" display-name="showCalReqAsListItem" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%@ attribute name="calReqs" required="true" type="java.lang.Object" %>
<%@ attribute name="page" required="true" type="java.lang.Object" %>
<%@ attribute name="ji" required="true" type="java.lang.Object" %>
<%@ attribute name="inst" required="true" type="java.lang.Object" %>
	
<script>
	// load note javascript file
	loadScript.loadScriptsDynamic(new Array('script/trescal/core/tools/CalibrationRequirements.js'), false);
</script>

<c:choose>
	<c:when test="${calReqs == null}">
		<li class="defaultCalReqListItemIdentifier">
			<label><spring:message code="showcalreqaslistitem.calreqs" />:</label>
			<c:choose>
				<c:when test="${page == 'instrument'}">
					<div class="padtop">
					<spring:message var="overlayTitle" code="showcalreqaslistitem.addcalpointsrequirements" />
					<cwms:securedJsLink permission="INSTRUMENT_CAL_REQ_ADD" collapse="True" classAttr="addPointsInstrument mainlink" onClick="loadScript.createOverlay('external', '${overlayTitle}', 'calibrationpoints.htm?type=instrument&page=instrument&plantid=${inst.plantid}', null, null, 80, 900, null); return false;">
						<spring:message code="showcalreqaslistitem.addtoinst" />
					</cwms:securedJsLink>
					</div>
					<div class="float-right">
						<cwms:securedJsLink permission="INSTRUMENT_CAL_REQ_IN_SHOW"  collapse="True" classAttr="mainlink" onClick="getHistoryForInstrumentCalReq(this, 0, ${inst.plantid}); return false;">
							<spring:message code="showcalreqaslistitem.showhistory" />
						</cwms:securedJsLink>				
					</div>
				</c:when>
				<c:when test="${page == 'jiheader'}">
					<div class="padtop">
						<a href="#" class="addPointsJobItem mainlink" onclick="loadScript.createOverlay('external', '<spring:message code="showcalreqaslistitem.addcalpointsrequirements" />', 'calibrationpoints.htm?type=jobitem&page=jiheader&jobitemid=${ji.jobItemId}', null, null, 80, 900, null); return false;"><spring:message code="showcalreqaslistitem.addtoji" /></a>
						<a href="#" class="addPointsInstrument mainlink marg-left" onclick="loadScript.createOverlay('external', '<spring:message code="showcalreqaslistitem.addcalpointsrequirements" />', 'calibrationpoints.htm?type=instrument&page=jiheader&plantid=${ji.inst.plantid}', null, null, 80, 900, null); return false;"><spring:message code="showcalreqaslistitem.addtoinst" /></a>
					</div>
					<div class="float-right">
						<a href="#" class="mainlink" onclick="getHistoryForJobItemCalReq(this, 0, ${ji.jobItemId}); return false;"><spring:message code="showcalreqaslistitem.showhistory" /></a>
					</div>
				</c:when>
			</c:choose>
		</li>
	</c:when>
	<c:otherwise>
		<li class="${page}${calReqs.id}">
			<input type="hidden" id="previousCalReqListItemIdentifier" value="${page}${calReqs.id}"/>
			<c:if test="${page == 'instrument' && calReqs.classKey == 'companymodel' || page == 'jiheader' && (calReqs.classKey == 'instrument' || calReqs.classKey == 'companymodel')}">
				<div class="float-right padtop">
					<c:if test="${page == 'jiheader' && (calReqs.classKey == 'instrument' || calReqs.classKey == 'companymodel')}">
						<a href="#" class="addPointsJobItem mainlink" onclick="loadScript.createOverlay('external', '<spring:message code="showcalreqaslistitem.addcalpointsrequirements" />', 'calibrationpoints.htm?type=${calReqs.classKey}&page=${page}&calreqid=${calReqs.id}&convert=true&jobitemid=${ji.jobItemId}', null, null, 80, 900, null); return false;">
							<spring:message code="showcalreqaslistitem.overrideonji" />
						</a>
					</c:if>
					&nbsp;&nbsp;
					<c:if test="${page == 'instrument' && calReqs.classKey == 'companymodel' || page == 'jiheader' && calReqs.classKey == 'companymodel'}">
						<a href="#" class="addPointsInstrument mainlink" onclick="loadScript.createOverlay('external', '<spring:message code="showcalreqaslistitem.addcalpointsrequirements" />', 'calibrationpoints.htm?type=${calReqs.classKey}&page=${page}&calreqid=${calReqs.id}&convert=true&plantid=${inst.plantid}', null, null, 80, 900, null); return false;">
							<spring:message code="showcalreqaslistitem.overrideoninst" />
						</a>
					</c:if>
				</div>
			</c:if>
			<c:if test="${page == 'instrument' && calReqs.classKey == 'instrument' || page == 'jiheader' && calReqs.classKey == 'jobitem'}">
				<div class="float-right">
					<spring:message var="overlayEditTitle" code="showcalreqaslistitem.editcalpointsrequirements" />
					<cwms:securedJsLink permission="INSTRUMENT_CAL_REQ_EDIT" collapse="True" classAttr="editPoints" onClick="loadScript.createOverlay('external', '${overlayEditTitle} ', 'calibrationpoints.htm?type=${calReqs.classKey}&page=${page}&calreqid=${calReqs.id}', null, null, 80, 900, null); return false; ">
						&nbsp;
					</cwms:securedJsLink>
					<c:choose>
						<c:when test="${page == 'instrument' && calReqs.classKey == 'instrument'}">
							<cwms:securedJsLink permission="INSTRUMENT_CAL_REQ_DELETE" collapse="True" classAttr="deletePoints" onClick="deactivateCalReqs('${page}', ${calReqs.id}, '${calReqs.classKey}', ${inst.plantid}); return false;">
								&nbsp;
							</cwms:securedJsLink>													
						</c:when>
						<c:when test="${page == 'jiheader' && calReqs.classKey == 'jobitem'}">
							<a href="#" class="deletePoints" onclick="deactivateCalReqs('${page}', ${calReqs.id}, '${calReqs.classKey}', ${ji.jobItemId}); return false;">&nbsp;</a>
						</c:when>
					</c:choose>
				</div>
			</c:if>
			<c:choose>
				<c:when test="${calReqs.pointSet != null}">
					<label class="bold"><spring:message code="calibrationpoints.calpoints" />:</label>
					<div class="clear"></div>
					<span class="attention bold">
						<c:forEach var="point" items="${calReqs.pointSet.points}" varStatus="loopStatus">
							<c:if test="${!loopStatus.first}">
							|
							</c:if>
							${point.getFormattedPoint()} ${point.uom.formattedSymbol}
							<c:if test="${point.getFormattedRelatedPoint() != ''}">
								@ ${point.getFormattedRelatedPoint()} ${point.relatedUom.formattedSymbol}
							</c:if>
						</c:forEach>
					</span><br />
				</c:when>
				<c:when test="${calReqs.range != null}">
					<label class="bold"><spring:message code="calibrationpoints.calrange" />:</label>
					<div class="clear"></div>
					<span class="attention bold">
						${calReqs.range} 
						<c:if test="${calReqs.range.relational}"> 
							@ ${calReqs.range.getFormattedRelatedPoint()} ${calReqs.range.relatedUom.formattedSymbol}
						</c:if>
					</span><br />
				</c:when>
				<c:otherwise>
					<label class="bold calreqinstruct"><spring:message code="calibrationpoints.calinstructions" />:</label>
					<div class="clear"></div>
				</c:otherwise>
			</c:choose>
			<c:if test="${calReqs.privateInstructions != null && calReqs.privateInstructions.trim() != ''}">
				<span class="attention bold"><spring:message code="private"/>: ${calReqs.privateInstructions}</span>
				<br />
			</c:if>
			<c:if test="${calReqs.publicInstructions != null && calReqs.publicInstructions.trim() != ''}">
				<span class="allgood bold"><spring:message code="public"/>: ${calReqs.publicInstructions}</span>
				<br />
			</c:if>
			<span class="bold">
				<c:choose>
					<c:when test="${calReqs.publish}">
						<spring:message code="note.published"/>
					</c:when>
					<c:otherwise>
						<spring:message code="note.notpublished"/>
					</c:otherwise>
				</c:choose>
			</span> -
			<span class="bold"><spring:message code="${calReqs.source}" arguments="${calReqs.sourceParameter}"/></span>
			<br />
			<span class="smaller-text">
				<spring:message code="instmod.lastmodified" />: <t:showLastModified versionedEntity="${calReqs}" showRole="true" />
			</span>
			<div class="float-right">
				<c:set var="calRegsId" value="0"/>
				<c:if test="${calReqs != null}">
					<c:set var="calRegsId" value="${calReqs.id}"/>
				</c:if>
				<c:choose>
					<c:when test="${page == 'instrument'}">
						<a href="#" class="mainlink" onclick="getHistoryForInstrumentCalReq(this, ${calRegsId}, ${inst.plantid}); return false;"><spring:message code="showcalreqaslistitem.showhistory" /></a>
					</c:when>
					<c:when test="${page == 'jiheader'}">
						<a href="#" class="mainlink" onclick="getHistoryForJobItemCalReq(this, ${calRegsId}, ${ji.jobItemId}); return false; "><spring:message code="showcalreqaslistitem.showhistory" /></a>
					</c:when>
				</c:choose>
			</div>		
		</li>
	</c:otherwise>
</c:choose>