<%@ tag language="java" display-name="jobitemProjectionLinkInfo" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="jobitem" required="true" type="java.lang.Object" %>
<%@ attribute name="jobno" required="true" type="java.lang.Object" %>
<%@ attribute name="itemssize" required="true" type="java.lang.Object" %>
<%@ attribute name="jicount" required="true" type="java.lang.Object" %>
<%@ attribute name="rowcount" required="true" type="java.lang.Object" %>
<%@ attribute name="showJobNo" required="false" type="java.lang.Object" %>

<a href="${jobitem.defaultPage}?jobitemid=${jobitem.jobItemId}&amp;ajax=jobitem&amp;width=580" 
		class="jconTip mainlink" 
		title="<spring:message code='jobitemlink.information' />" 
		id="jobitem${jobitem.jobItemId}${rowcount}" 
		tabindex="-1">
	<c:if test="${not empty showJobNo and showJobNo}">${jobno}.Item</c:if>${jobitem.itemno}<c:if test="${jicount}"> / ${itemssize}</c:if>
</a>
<links:showRolloverMouseIcon/>