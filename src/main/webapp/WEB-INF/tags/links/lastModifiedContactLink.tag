<%@ tag language="java" display-name="lastModifiedContactLink" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ attribute name="entity" description="Auditable or Versioned entity" required="true" type="java.lang.Object" %>

<c:choose>
	<c:when test="${not empty entity.lastModifiedBy}">
		<links:contactLinkDWRInfo contact="${entity.lastModifiedBy}" rowcount="1" />								
	</c:when>
	<c:otherwise>
		<spring:message code="unknown" />
	</c:otherwise>
</c:choose>
<c:if test="${not empty entity.lastModified}">
	<fmt:formatDate value="${entity.lastModified}" pattern="(dd.MM.yyyy - HH:mm:ss)"/>
</c:if>