<%@ tag language="java" display-name="jobitemLinkInfo" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="jobItemId" required="true" type="java.lang.Integer" %>
<%@ attribute name="jobItemNo" required="true" type="java.lang.String" %>
<%@ attribute name="jobItemCount" required="false" type="java.lang.Integer" %>
<%@ attribute name="defaultPage" required="false" type="java.lang.String" %>
<%@ attribute name="contractReviewed" required="false" type="java.lang.Boolean" %>
<%--
	if defaultPage is provided, it will be used for page url;
	otherwise if contractReviewed is provided and true, jiactions.htm will be used;
	otherwise if contractReviewed is provided and false, jicontractreview.htm will be used;
	otherwise will default to jiactions.htm.
--%>
<c:choose>
	<c:when test="${defaultPage != null}">
		<c:set var="pageURI" value="${defaultPage}" />
	</c:when>
	<c:when test="${contractReviewed != null && contractReviewed}">
		<c:set var="pageURI" value="jiactions.htm" />
	</c:when>
	<c:when test="${contractReviewed != null && not contractReviewed}">
		<c:set var="pageURI" value="jicontractreview.htm" />
	</c:when>
	<c:otherwise>
		<c:set var="pageURI" value="jiactions.htm" />
	</c:otherwise>
</c:choose>


<a href="${pageURI}?jobitemid=${jobItemId}&amp;ajax=jobitem&amp;width=580" class="jconTip mainlink" title="<spring:message code='jobitemlink.information' />" id="jobitem${jobItemId}" tabindex="-1">
	${jobItemNo}<c:if test="${jobItemCount != null}"> / ${jobItemCount}</c:if>
</a>
<links:showRolloverMouseIcon/>