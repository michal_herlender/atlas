<%@ tag language="java" display-name="showNotesLinks" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ attribute name="notecounter" required="true" type="java.lang.Object" %>
<%@ attribute name="noteType" required="true" type="java.lang.Object" %>
<%@ attribute name="noteTypeId" required="true" type="java.lang.Object" %>
<%@ attribute name="colspan" required="true" type="java.lang.Object" %>

<%--
* Migrated from #showNotesLinks velocity macro
* Macro to display links for adding and viewing notes
* @param notecounter - count of all active notes for this item
* @param noteTypeId - the id of entity we are attaching note too
* @param noteType - the type of note we are adding
* @param colspan - the number of columns to span when adding note
--%>

<script type='text/javascript'>
	// load note javascript file
	loadScript.loadScripts(new Array('script/trescal/core/utilities/Note.js'));
	// variable already exists?
	if (typeof privateOnlyNotes == 'undefined') {
		// assign global variable for private only notes
		var privateOnlyNotes = ${privateOnlyNotes};
	}
	var addCommandNotes = 'addPublic';
	if (privateOnlyNotes) {
		addCommandNotes = 'addPrivate';
	}
</script>
<!--notecounter is the number of notes and is displayed in brakets after the image-->
<!--noteType is the Name of the Note e.g. "DELIVERYITEMNOTE"-->
<!--noteTypeId is the number of the note-->
<!--notetype and noteTypeId used for get the right tablerow-->
<span>
	<a href="#" onclick=" 
		if ($j('#${noteType}${noteTypeId}').length) { 
			if ($j('#${noteType}${noteTypeId}[class*=\'hid\']').length) { 
				$j('#${noteType}${noteTypeId}').removeClass().addClass('vis'); } 
			else { 
				$j('#${noteType}${noteTypeId}').removeClass().addClass('hid'); } 
			} 
		return false; " >
		<img src="img/icons/note.png" width="16" height="16" alt="<spring:message code='notes.viewnote' />" title="<spring:message code='notes.viewnote' />" class="noteimg_inline" />
	</a> (<span id="count${noteType}${noteTypeId}">${notecounter}</span>)
	<a href="#" onclick=" event.preventDefault(); addNoteBoxy(addCommandNotes, true, '${noteType}', ${noteTypeId}, ${currentContact.personid}, 0, ${colspan}); " title="<spring:message code='notes.addnote' />">
		<img src="img/icons/note_add.png" height="16" width="16" title="<spring:message code='notes.addnote' />" alt="<spring:message code='notes.addnote' />" class="noteimg_inline" />
	</a>
</span> 