<%@ tag language="java" display-name="contactLinkDWRInfo" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="contact" required="true" type="java.lang.Object" %>
<%@ attribute name="rowcount" required="true" type="java.lang.Object" %>

<c:if test="${contact != null}">
	<c:choose>
		<c:when test="${contact.active && contact.subdiv.active && contact.subdiv.company.active}">
			<c:set var="linkClass" value="jconTip mainlink"/>
		</c:when>
		<c:otherwise>
			<c:set var="linkClass" value="jconTip mainlink-strike"/>
		</c:otherwise>
	</c:choose>
	<c:if test="${contact.subdiv.company.onstop}">
		<img src="img/icons/flag_red.png" width="12" height="12" alt="<spring:message code='viewmod.contactcompanyonstop' />" title="<spring:message code='viewmod.contactcompanyonstop' />" />
	</c:if>
	<a href="viewperson.htm?personid=${contact.personid}&amp;ajax=contact&amp;width=380" class="${linkClass}" title="<spring:message code='company.contactinformation' />" id="contact${contact.personid}${rowcount}" tabindex="-1">${contact.name}</a>
	<links:showRolloverMouseIcon/>
</c:if>