<%@ tag language="java" display-name="outOfCalibrationTimescale" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="inCalTimescalePercentage" required="true" type="java.lang.Object" %>
<%@ attribute name="wholeTimescalePercentage" required="true" type="java.lang.Object" %>
<%@ attribute name="finalMonthOfCalTimescalePercentage" required="true" type="java.lang.Object" %>

<c:if test="${inCalTimescalePercentage != null && wholeTimescalePercentage != null && finalMonthOfCalTimescalePercentage != null}">
	<c:choose>
		<c:when test="${wholeTimescalePercentage > 0}">
			<spring:message code="instrlink.calperiodpassed" var="msg" />
            <c:set var="title" value="${msg} - ${wholeTimescalePercentage}&#37;"/>
		</c:when>
		<c:otherwise>
			<spring:message code="instrlink.unabletocalculate" var="title"/>
		</c:otherwise>

	</c:choose>
	<div class="calIndicatorBox" title='${ title }'>
		<c:choose>
			<c:when test="${inCalTimescalePercentage == 100 && wholeTimescalePercentage == 100 && finalMonthOfCalTimescalePercentage == 100}">
				<div class="outOfCalibration"></div>
			</c:when>
			<c:otherwise>
				<div class="inCalibration">
					<div class="inCalibrationLevel" style=" width: ${inCalTimescalePercentage}%; "></div>
				</div>
				<div class="finalMonthOfCalibration">
					<div class="finalMonthOfCalibrationLevel" style=" width: ${finalMonthOfCalTimescalePercentage}%; "></div>
				</div>
			</c:otherwise>
		</c:choose>
	</div>				
</c:if>