<%@ tag language="java" display-name="contactLinkDWRInfo" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="contact" required="true" type="java.lang.Object" %>
<%@ attribute name="rowcount" required="true" type="java.lang.Object" %>

<c:if test="${contact != null}">
	<c:set var="allocatedCompanyId" value="${sessionScope.allocatedCompany.getKey()}"/>
	<c:set var="contactActive" value="${contact.active}"/>
	<c:set var="subdivActive" value="${contact.sub.active}"/>
	<c:set var="companyActive" value="${false}"/>
	<c:set var="onStop" value="${true}"/>
	<c:forEach var="settings" items="${contact.sub.comp.settingsForAllocatedCompanies}">
		<c:if test="${settings.organisation.getId() == allocatedCompanyId}">
			<c:if test="${settings.active}">
				<c:set var="companyActive" value="${true}"/>
			</c:if>
			<c:if test="${!settings.onStop}">
				<c:set var="onStop" value="${false}"/>
			</c:if>
		</c:if>
	</c:forEach>
	<c:choose>
		<c:when test="${contactActive && subdivActive && companyActive}">
			<c:set var="linkClass" value="jconTip mainlink"/>
		</c:when>
		<c:otherwise>
			<c:set var="linkClass" value="jconTip mainlink-strike"/>
		</c:otherwise>
	</c:choose>
	<c:if test="${onStop}">
		<img src="img/icons/flag_red.png" width="12" height="12" alt="<spring:message code='viewmod.contactcompanyonstop' />" title="<spring:message code='viewmod.contactcompanyonstop' />" />
	</c:if>
	<a href="viewperson.htm?personid=${contact.personid}&amp;ajax=contact&amp;width=380" class="${linkClass}" title="<spring:message code='company.contactinformation' />" id="contact${contact.personid}${rowcount}" tabindex="-1">${contact.name}</a>
	<links:showRolloverMouseIcon/>
</c:if>