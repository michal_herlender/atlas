<%@ tag language="java" display-name="procedureLinkDWRInfo" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="capability" required="true" type="java.lang.Object" %>
<%@ attribute name="displayName" required="false" type="java.lang.Object" %>
<%@ attribute name="rowcount" required="false" type="java.lang.Object" %>

<script type="module" src="script/components/cwms-tooltips/cwms-capability-link-and-tooltip/cwms-capability-link-and-tooltip.js"></script>
<cwms-capability-link-and-tooltip reference="${capability.reference}" id=${capability.id} ></cwms-capability-link-and-tooltip>