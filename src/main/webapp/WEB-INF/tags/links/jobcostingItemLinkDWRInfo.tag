<%@ tag language="java" display-name="jobcostingItemLinkDWRInfo" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="jobcostingitem" required="true" type="java.lang.Object" %>
<%@ attribute name="rowcount" required="true" type="java.lang.Object" %>

<!-- 	this macro creates a hook which is used when displaying all the supplied job costing items details in a
	tooltip style pop up.
	See Javascript files:
		jquery.jtip.js
		TooltipDWRInfo.js
	@param ($jobitem) - job costing item object from which we can retrieve (jobcostingitemid)
	@param ($rowcount) - count used in id when job item is displayed in results list so pop up appears in right row,
						 if single occurrence of jobitemid then use 0 as parameter.
 -->

<a href="${jobcostingitem.jobItem.defaultPage}?jobcostingitemid=${jobcostingitem.id}&amp;ajax=jobcostingitem&amp;width=680&amp;jobitemid=${jobcostingitem.jobItem.jobItemId}" class="jconTip mainlink" title="<spring:message code='jobcostingitemlink.jobcostingiteminformation' />" id="jobcostingitem${jobcostingitem.id}${rowcount}" tabindex="-1">${jobcostingitem.jobItem.itemNo}</a>
<links:showRolloverMouseIcon/>