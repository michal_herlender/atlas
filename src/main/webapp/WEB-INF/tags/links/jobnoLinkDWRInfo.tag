<%@ tag language="java" display-name="jobnoLinkDWRInfo" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>

<%@ attribute name="jobid" required="true" type="java.lang.Object" %>
<%@ attribute name="jobno" required="true" type="java.lang.Object" %>
<%@ attribute name="job" required="false" type="java.lang.Object" %>
<%@ attribute name="expenseitem" required="flase" type="java.lang.Object" %>
<%@ attribute name="jobservice" required="flase" type="java.lang.Boolean" %>
<%@ attribute name="serviceitems" required="flase" type="java.lang.Boolean" %>
<%@ attribute name="copy" required="true" type="java.lang.Object" %>
<%@ attribute name="rowcount" required="true" type="java.lang.Object" %>

<c:set var="expensestab" value=""></c:set>
<c:if test="${jobservice}">
<c:set var="expensestab" value="&loadtab=expenses-tab"></c:set>
</c:if>
<cwms:securedLink permission="JOB_VIEW" classAttr="jconTip mainlink" parameter="?jobid=${jobid}&amp;ajax=job&amp;width=600${expensestab}" 
id="job${jobno}${rowcount}" >
   <c:if test="${!serviceitems}">
	${jobno} <c:if test="${jobservice}">.Service${expenseitem.itemNo}</c:if>
	</c:if>
	<c:if test="${serviceitems}">${expenseitem.itemNo} / ${job.expenseItems.size()}</c:if>
</cwms:securedLink>
<links:showRolloverMouseIcon/>
<c:if test="${copy}">
	<img src="img/icons/bullet_clipboard.png" class="pad-left-small" width="10" height="10" onclick=" $j(this).prev().prev().clipBoard(this, null, false, null); " alt="<spring:message code='joblink.addjobtoclipboard' />" title="<spring:message code='joblink.addjobtoclipboard' />" />
</c:if>