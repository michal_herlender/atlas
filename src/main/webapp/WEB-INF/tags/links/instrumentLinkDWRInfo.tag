<%@ tag language="java" display-name="instrumentLinkDWRInfo"
	description="velocityOrder: instrument rowcount displayBarcode displayName displayCalTimescale caltypeid"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ attribute name="instrument" required="true" type="org.trescal.cwms.core.instrument.entity.instrument.Instrument"%>
<%@ attribute name="rowcount" required="true" type="java.lang.Integer"%>
<%@ attribute name="displayBarcode" required="true" type="java.lang.Boolean"%>
<%@ attribute name="displayName" required="true" type="java.lang.Boolean"%>
<%@ attribute name="displayCalTimescale" required="true" type="java.lang.Boolean"%>
<%@ attribute name="caltypeid" required="false" type="java.lang.Integer"%>
<%@ attribute name="displayInstClientId" required="false" type="java.lang.Boolean"%>

<c:if test="${displayCalTimescale}">
	<div class="float-left relative">
	    	<spring:message code="instrlink.calperiodpassed" var="msg" />
	    	<cwms:calTimescaleGraphic calIntervalUnit="${instrument.calFrequencyUnit}" calInterval="${instrument.calFrequency}" calDueDate="${instrument.nextCalDueDate}" hoverMessageBegining="${msg}"/>
	</div>
</c:if>
<c:if test="${instrument.calibrationStandard}">
	<img src="img/icons/bullet-standard.png" width="12" height="12"
		class="image_inl" alt="<spring:message code='instrlink.standard' />"
		title="<spring:message code='instrlink.standard' />" />
</c:if>
<c:if test="${instrument.scrapped}">
	<img src="img/icons/bin_closed.png" width="16" height="16"
		class="image_inl" alt="<spring:message code='instrlink.unitber' />"
		title="<spring:message code='instrlink.unitber' />" />
</c:if>

<c:set var="classSecured" value="jconTip " />

<c:choose>
	<c:when test="${instrument.scrapped}">
		<c:set var="classSecured" value="jconTip mainlink-strike" />
	</c:when>
	<c:otherwise>
		<c:set var="classSecured" value="jconTip mainlink" />
	</c:otherwise>
</c:choose>


<cwms:securedLink permission="INSTRUMENT_VIEW"
	classAttr="${classSecured}"
	parameter="?plantid=${instrument.plantid}&amp;ajax=instrument&amp;width=500">
	<c:if test="${displayBarcode}">
		<c:out value="${instrument.plantid} " />
	</c:if>
	<c:if test="${displayName}"> <cwms:showinstrument instrument="${instrument}" />
	</c:if>
	<c:if test="${displayInstClientId}">
		( <spring:message code='plantno' /> : <c:out value="${instrument.plantno} " /> )
	</c:if>
</cwms:securedLink>


<links:showRolloverMouseIcon />