<%@ tag language="java" display-name="contactLinkInfo" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="personId" required="true" type="java.lang.Integer" %>
<%@ attribute name="contactName" required="true" type="java.lang.String" %>
<%@ attribute name="active" required="true" type="java.lang.String" %>
<%@ attribute name="rowcount" required="true" type="java.lang.Object" %>

<a href="viewperson.htm?personid=${personId}&amp;ajax=contact&amp;width=380" class="jconTip mainlink<c:if test='${!active}'>-strike</c:if>" title="<spring:message code='company.contactinformation' />" id="contact${personId}${rowcount}" tabindex="-1">${contactName}</a>
<links:showRolloverMouseIcon/>