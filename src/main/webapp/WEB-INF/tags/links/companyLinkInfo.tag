<%@ tag language="java" display-name="companyLinkInfo" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="companyId" required="true" type="java.lang.Integer" %>
<%@ attribute name="companyName" required="true" type="java.lang.String" %>
<%@ attribute name="active" required="true" type="java.lang.Boolean" %>
<%@ attribute name="onStop" required="true" type="java.lang.Boolean" %>
<%@ attribute name="copy" required="true" type="java.lang.Object" %>
<%@ attribute name="rowcount" required="true" type="java.lang.Object" %>

<c:if test="${onStop}">
	<img src="img/icons/flag_red.png" width="12" height="12" alt="<spring:message code='viewmod.companyonstop' />" title="<spring:message code='viewmod.companyonstop' />" />
</c:if>
<a href="viewcomp.htm?coid=${companyId}&amp;ajax=company&amp;width=400" class="jconTip mainlink<c:if test='${!active}'>-strike</c:if>" title="<spring:message code='company.companyinformation' />" id="company${companyId}${rowcount}" tabindex="-1">${companyName}</a>
<links:showRolloverMouseIcon/>
<c:if test="${copy}">
	<img src="img/icons/bullet_clipboard.png" width="10" height="10" onclick=" $j(this).prev().prev().clipBoard(this, null, false, null); " alt="<spring:message code='company.addcomptoclipboard' />" title="<spring:message code='company.addcomptoclipboard' />" />
</c:if>