<%@ tag language="java" display-name="companyLinkDWRInfo"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>

<%@ attribute name="company" required="true" type="java.lang.Object"%>
<%@ attribute name="copy" required="true" type="java.lang.Object"%>
<%@ attribute name="rowcount" required="true" type="java.lang.Object"%>

<c:set var="onStop" value="true" />
<c:set var="allocatedCompanyId"
	value="${sessionScope.allocatedCompany.getKey()}" />
<c:set var="linkClass" value="jconTip mainlink-strike" />
<c:forEach var="settings"
	items="${company.settingsForAllocatedCompanies}"> 
 	<c:if test="${settings.organisation.getId() == allocatedCompanyId}"> 
 		<c:if test="${settings.active}"> 
 			<c:set var="linkClass" value="jconTip mainlink" /> 
 		</c:if> 
 		<c:if test="${!settings.onStop}"> 
 			<c:set var="onStop" value="false" /> 
 		</c:if> 
 	</c:if> 
 </c:forEach> 
<c:if test="${onStop}">
	<img src="img/icons/flag_red.png" width="12" height="12"
		alt="<spring:message code='viewmod.companyonstop' />"
		title="<spring:message code='viewmod.companyonstop' />" />
</c:if>
	<cwms:securedLink permission="COMPANY_VIEW" classAttr="${linkClass}" id="company${company.coid}${rowcount}" parameter="?coid=${company.coid}&amp;ajax=company&amp;width=400">
		${company.coname}
	</cwms:securedLink>

<links:showRolloverMouseIcon />
<c:if test="${copy}">
	<img src="img/icons/bullet_clipboard.png" class="pad-left-small"
		width="10" height="10"
		onclick=" $j(this).prev().prev().clipBoard(this, null, false, null); "
		alt="<spring:message code='company.addcomptoclipboard' />"
		title="<spring:message code='company.addcomptoclipboard' />" />
</c:if>