<%@ tag language="java" display-name="subdivLinkDWRInfo" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ attribute name="subdivName" required="true" type="java.lang.String" %>
<%@ attribute name="subdivId" required="true" type="java.lang.Integer" %>
<%@ attribute name="subdivActive" required="true" type="java.lang.Boolean" %>
<%@ attribute name="companyActive" required="true" type="java.lang.Boolean" %>
<%@ attribute name="companyOnStop" required="true" type="java.lang.Boolean" %>
<%@ attribute name="rowcount" required="true" type="java.lang.Integer" %>

<c:choose>
	<c:when test="${subdivActive && companyActive}">
		<c:set var="jconTip" value="jconTip mainlink"/>
	</c:when>
	<c:otherwise>
		<c:set var="jconTip" value="jconTip mainlink-strike"/>
	</c:otherwise>
</c:choose>
<c:if test="${companyOnStop}">
	<img src="img/icons/flag_red.png" width="12" height="12" alt="<spring:message code='company.subdivisioncompanyonstop' />" title="<spring:message code='company.subdivisioncompanyonstop' />" />
</c:if>
<cwms:securedLink permission="SUBDIV_VIEW" classAttr="${jconTip}" id="subdiv${subdivId}${rowcount}" parameter="?subdivid=${subdivId}&amp;ajax=subdiv&amp;width=400">
	${subdivName}
</cwms:securedLink>
<links:showRolloverMouseIcon/>