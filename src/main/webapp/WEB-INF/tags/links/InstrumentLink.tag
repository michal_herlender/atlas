<%@ tag language="java" display-name="instrumentLinkDWRInfo" description="velocityOrder: instrument rowcount displayBarcode displayName displayCalTimeScale caltypeid" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ attribute name="rowcount" required="true" type="java.lang.Integer" %>
<%@ attribute name="displayBarcode" required="true" type="java.lang.Boolean" %>
<%@ attribute name="displayName" required="true" type="java.lang.Boolean" %>
<%@ attribute name="displayCalTimescale" required="true" type="java.lang.Boolean" %>
<%@ attribute name="calDueDate" required="true" type="java.time.LocalDate" %>
<%@ attribute name="calInterval" required="true" type="java.lang.Integer" %>
<%@ attribute name="calIntervalUnit" required="true" type="org.trescal.cwms.core.system.enums.IntervalUnit" %>
<%@ attribute name="isCalStandard" required="true" type="java.lang.Boolean" %>
<%@ attribute name="isScrapped" required="true" type="java.lang.Boolean" %>
<%@ attribute name="plantId" required="true" type="java.lang.Integer" %>
<%@ attribute name="modelName" required="true" type="java.lang.String" %>
<%@ attribute name="modelFromInstrument" required="true" type="java.lang.String" %>
<%@ attribute name="manufacturerFromInstrument" required="true" type="java.lang.String" %>
<%@ attribute name="modelIsGeneric" required="true" type="java.lang.Boolean" %>

<c:if test="${displayCalTimescale}">
    <div class="float-left relative">
        <spring:message code="instrlink.calperiodpassed" var="msg" />
        <cwms:calTimescaleGraphic calDueDate="${calDueDate}" calInterval="${calInterval}"
                                  calIntervalUnit="${calIntervalUnit}" hoverMessageBegining="${msg}"/>
    </div>
</c:if>
<c:if test="${isCalStandard}">
    <img src="img/icons/bullet-standard.png" width="12" height="12" class="image_inl" alt="<spring:message code='instrlink.standard' />" title="<spring:message code='instrlink.standard' />" />
</c:if>
<c:if test="${isScrapped}">
    <img src="img/icons/bin_closed.png" width="16" height="16" class="image_inl" alt="<spring:message code='instrlink.unitber' />" title="<spring:message code='instrlink.unitber' />" />
</c:if>
<a href="viewinstrument.htm?plantid=${plantId}&amp;ajax=instrument&amp;width=500" class='jconTip <c:choose><c:when test="${isScrapped}">mainlink-strike</c:when><c:otherwise>mainlink</c:otherwise></c:choose>' title="<spring:message code='instrlink.information' />" id="inst${plantId}${rowcount}">
    <c:if test="${displayBarcode}">
        ${plantId}
    </c:if>
    <c:if test="${displayName}">
        ${modelName}
        <c:if test="${modelIsGeneric}">
        	<c:if test="${manufacturerFromInstrument != '/'}">
        		&nbsp${manufacturerFromInstrument}
        	</c:if>
            <c:if test="${modelFromInstrument != '/'}">
            	&nbsp${modelFromInstrument}
            </c:if>
        </c:if>
    </c:if>
</a>

<links:showRolloverMouseIcon/>