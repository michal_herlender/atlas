<%@ tag language="java" display-name="jobItemDeliveryLink" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<%-- 
    Tag to show deliveries related to a job item with <br> delimiter
    Additional parameters indicate the type of deliveries that should be shown
    Author Galen Beck - 2016-09-15
--%>

<%@ attribute name="jobItem" required="true" type="java.lang.Object" %>
<%@ attribute name="showClientDeliveries" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showThirdPartyDeliveries" required="false" type="java.lang.Boolean" %>
<%@ attribute name="displayCountry" required="false" type="java.lang.Boolean" %>
<%@ attribute name="authorized" required="false" type="java.lang.Boolean" %>

<c:forEach var="deliveryItem" items="${jobItem.deliveryItems}">
	<c:set var="showDelivery" value="false" />
	<c:if test="${showClientDeliveries && deliveryItem.delivery.isClientDelivery()}">
		CD - 
		<c:set var="showDelivery" value="true" />
	</c:if>
	<c:if test="${showThirdPartyDeliveries && deliveryItem.delivery.isThirdPartyDelivery()}">
		TPD - 
		<c:set var="showDelivery" value="true" />
	</c:if>	
	<c:if test="${showDelivery}">
		<cwms:securedLink permission="DELIVERY_NOTE_VIEW" authorized="${authorized}" id="jobdeliveryitem${deliveryItem.delitemid}" classAttr="mainlink" parameter="?delid=${deliveryItem.delivery.deliveryid}">
			${deliveryItem.delivery.deliveryno}
		</cwms:securedLink>
		(<fmt:formatDate value="${deliveryItem.delivery.deliverydate}" pattern="dd.MM.yyyy" />)
		<c:if test="${displayCountry && not empty deliveryItem.delivery.address.country}">
			${deliveryItem.delivery.address.country.localizedName}
		</c:if>
		<br>
	</c:if>
</c:forEach>