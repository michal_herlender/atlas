<%@ tag language="java" display-name="tpquoteLinkDWRInfo" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="tpquote" required="true" type="java.lang.Object" %>
<%@ attribute name="rowcount" required="true" type="java.lang.Object" %>

<a href="viewtpquote.htm?id=${tpquote.id}&amp;ajax=tpquote&amp;width=500" class="jconTip mainlink" 
   title="<spring:message code='tpquotelink.information' />" id="tpquote${tpquote.id}${rowcount}" tabindex="-1">
   ${tpquote.qno}
</a>
<links:showRolloverMouseIcon/>