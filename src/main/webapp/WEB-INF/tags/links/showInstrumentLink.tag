<%@ tag language="java" display-name="showInstrumentLink" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>

<%@ attribute name="instrument" required="true" type="java.lang.Object" %>

<cwms:securedLink permission="INSTRUMENT_VIEW" classAttr="mainlink" parameter="?plantid=${instrument.plantid}">
	${instrument.plantid}
</cwms:securedLink>
