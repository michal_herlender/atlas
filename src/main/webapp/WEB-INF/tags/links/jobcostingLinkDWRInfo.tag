<%@ tag language="java" display-name="jobcostingLinkDWRInfo" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="jobcosting" required="true" type="java.lang.Object" %>
<%@ attribute name="showno" required="true" type="java.lang.Object" %>
<%@ attribute name="rowcount" required="true" type="java.lang.Object" %>

<a href="viewjobcosting.htm?id=${jobcosting.id}&amp;ajax=jobcosting&amp;width=600" class="jconTip mainlink" title="<spring:message code='jobcostinglink.jobcostinginformation' />" id="jobcosting${jobcosting.id}${rowcount}" tabindex="-1">
	<c:if test="${showno}">
		${jobcosting.qno} ver
	</c:if>
	${jobcosting.ver}
</a>
<links:showRolloverMouseIcon/>