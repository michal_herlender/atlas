<%@ tag language="java" display-name="subdivLinkDWRInfo" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ attribute name="subdiv" required="true" type="java.lang.Object" %>
<%@ attribute name="rowcount" required="true" type="java.lang.Object" %>

<c:set var="onStop" value="${true}"/>
<c:set var="allocatedCompanyId" value="${sessionScope.allocatedCompany.getKey()}"/>
<c:set var="active" value="${subdiv.active}"/>
<c:set var="companyActive" value="${false}"/>
<c:forEach var="settings" items="${subdiv.comp.settingsForAllocatedCompanies}">
	<c:if test="${settings.organisation.getId() == allocatedCompanyId}">
		<c:if test="${settings.active}">
			<c:set var="companyActive" value="${true}"/>
		</c:if>
		<c:if test="${!settings.onStop}">
			<c:set var="onStop" value="${false}"/>
		</c:if>
	</c:if>
</c:forEach>
<c:choose>
	<c:when test="${active && companyActive}">
		<c:set var="jconTip" value="jconTip mainlink"/>
	</c:when>
	<c:otherwise>
		<c:set var="jconTip" value="jconTip mainlink-strike"/>
	</c:otherwise>
</c:choose>
<c:if test="${onStop}">
	<img src="img/icons/flag_red.png" width="12" height="12" alt="<spring:message code='company.subdivisioncompanyonstop' />" title="<spring:message code='company.subdivisioncompanyonstop' />" />
</c:if>
<cwms:securedLink permission="SUBDIV_VIEW" classAttr="${jconTip}" id="subdiv${subdiv.subdivid}${rowcount}" parameter="?subdivid=${subdiv.subdivid}&amp;ajax=subdiv&amp;width=400">
	${subdiv.subname}
</cwms:securedLink>
<links:showRolloverMouseIcon/>