<%@ tag language="java" display-name="quoteLinkInfo" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="quoteId" required="true" type="java.lang.Integer" %>
<%@ attribute name="quoteNo" required="true" type="java.lang.String" %>
<%@ attribute name="quoteVer" required="true" type="java.lang.Integer" %>
<%@ attribute name="rowcount" required="true" type="java.lang.Object" %>
<%@ attribute name="showversion" required="true" type="java.lang.Object" %>


<a href="viewquotation.htm?id=${quoteId}&amp;ajax=quotation&amp;width=650" class="jconTip mainlink" title="<spring:message code='quotelink.information' />" id="quote${quote.id}${rowcount}" tabindex="-1">
	${quoteNo}
	<c:if test="${showversion}">ver ${quoteVer}</c:if>
</a>
<links:showRolloverMouseIcon/>