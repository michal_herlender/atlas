<%@ tag language="java" display-name="showQuotationLink" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="quotation" required="true" type="java.lang.Object" %>

<a href="viewquotation.htm?id=${quotation.id}" title="<spring:message code='quotelink.viewquote' />" class="mainlink">
	${quotation.qno} ver ${quotation.ver}
</a>