<%@ tag language="java" display-name="subdivLinkDWRInfo"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ attribute name="subdiv" required="true" type="java.lang.Object"%>
<%@ attribute name="rowcount" required="true" type="java.lang.Object"%>

<c:choose>
	<c:when test="${subdiv.active && subdiv.company.active}">
		<c:set var="jconTip" value="jconTip mainlink" />
	</c:when>
	<c:otherwise>
		<c:set var="jconTip" value="jconTip mainlink-strike" />
	</c:otherwise>
</c:choose>
<c:if test="${subdiv.company.onstop}">
	<img src="img/icons/flag_red.png" width="12" height="12"
		alt="<spring:message code='company.subdivisioncompanyonstop' />"
		title="<spring:message code='company.subdivisioncompanyonstop' />" />
</c:if>
<cwms:securedLink permission="SUBDIV_VIEW" classAttr="${jconTip}"
	id="subdiv${subdiv.subdivid}${rowcount}"
	parameter="?subdivid=${subdiv.subdivid}&amp;ajax=subdiv&amp;width=400">
	${subdiv.subname}
</cwms:securedLink>
<links:showRolloverMouseIcon />