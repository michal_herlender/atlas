<%@ tag language="java" display-name="jobcostingItemLinkDWRInfo" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="jobitemdefaultpage" required="true" type="java.lang.Object" %>
<%@ attribute name="jobitemid" required="true" type="java.lang.Object" %>
<%@ attribute name="jobitemno" required="true" type="java.lang.Object" %>
<%@ attribute name="jobcostingitemid" required="true" type="java.lang.Object" %>
<%@ attribute name="rowcount" required="true" type="java.lang.Object" %>

<!-- 	
	@param ($jobitemdefaultpage) - jobitem defaultpage
	@param ($jobitemid) - jobitem id
	@param ($jobitemno) - jobitem number
	@param ($jobcostingitemid) - jobitem number
	@param ($rowcount) - count used in id when job item is displayed in results list so pop up appears in right row,
						 if single occurrence of jobitemid then use 0 as parameter.
 -->

<a href="${jobitemdefaultpage}?jobcostingitemid=${jobcostingitemid}&amp;ajax=jobcostingitem&amp;width=680&amp;jobitemid=${jobitemid}" class="jconTip mainlink" title="<spring:message code='jobcostingitemlink.jobcostingiteminformation' />" id="jobcostingitem${jobcostingitemid}${rowcount}" tabindex="-1">${jobitemno}</a>
<links:showRolloverMouseIcon/>
