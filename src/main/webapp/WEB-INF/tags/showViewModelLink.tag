<%@ tag language="java" display-name="showViewModelLink" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%@ attribute name="instrumentmodel" required="true" type="org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel" %>
<%@ attribute name="classValue" required="false" type="java.lang.String" %>
<%@ attribute name="target" required="false" type="java.lang.String" %>
<%@ attribute name="instrument" required="false" type="org.trescal.cwms.core.instrument.entity.instrument.Instrument" %>


<a href="<c:url value="/instrumentmodel.htm?modelid=${instrumentmodel.modelid}"/>" 
	<c:choose>
		<c:when test="${!(empty classValue)}">
			class="${classValue}"
		</c:when>
		<c:otherwise>
			class="mainlink"		 
		</c:otherwise>
	</c:choose>
	<c:if test="${!(empty target)}">
		target="${target}"
	</c:if>
	>
	<c:choose>
		<c:when test="${not empty instrument}">
			<cwms:showinstrument instrument="${instrument}"/>
		</c:when>
		<c:otherwise>
			<cwms:showmodel instrumentmodel="${instrumentmodel}"/>
		</c:otherwise>
	</c:choose>
</a>