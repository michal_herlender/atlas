<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<div id="validationdialog" title="<spring:message code='certificationvalidation.title'/>" style="display: none; ">

    <fieldset>
        <ol>
            <li>
                <Legend id="validationdialog-certno" style="font-weight: bold"></Legend>
            </li>
            <li>
                <label><spring:message code="editinstr.calibrationinstructions"/></label>
                <span id="validationdialog-calinstructions"></span>
            </li>
            <li>
                <label><spring:message code="instrument.customeracceptancecriteria"/></label>
                <span id="validationdialog-customeracceptancecriteria"></span>
            </li>
            <li>
                <label><spring:message code="certificate.deviation"/></label>
                <span id="validationdialog-deviation"></span>
            </li>
            <li>
                <label for="validationdialog-date"><spring:message code="certificationvalidation.date"/></label>
                <%--invisible text input box that is neccessary to stop datepicker flying open as soon as dialog loads --%>
                <input type="text" style="width: 0; height: 0; top: -100px; position: absolute;"/>
                <input type="date" id="validationdialog-date" size="12"
                class="float-left" onclick="$j('#validationrequired-date').removeClass('vis').addClass('hid');"/>
                <span style="color:red;" class="hid " id="validationrequired-date"><spring:message code="repairinspection.validator.mandatory" /></span>
            </li>
            <li>
                <label for="validationdialog-note"><spring:message code="certificationvalidation.note"/></label>
                <textarea id="validationdialog-note" rows="2" cols="30"></textarea>
            </li>

        </ol>
        <div class="center">
            <button id="validationdialog-accept"><spring:message code="certificationvalidation.accept"/></button>
            <button id="validationdialog-reject"><spring:message code="certificationvalidation.reject"/></button>
        </div>


    </fieldset>

</div>


<div id="viewvalidationdialog" style="display: none;">
    <fieldset>
        <ol>
            <li>
                <%-- invisible input box to take focus away from dialog close icon as it looks ugly with focus --%>
                <input type="text" style="width: 0; height: 0; top: -100px; position: absolute;"/>
                <Legend id="viewvalidationdialog-certno" style="font-weight: bold"></Legend>
                <input type="hidden" id="viewvalidationdialog-certid" />
            </li>
            <li>
                <label><spring:message code="status"/></label>
                <span id="viewvalidationdialog-status"></span>
            </li>
            <li>
                <label><spring:message code="certificationvalidation.date"/></label>
                <span id="viewvalidationdialog-date"></span>
            </li>
            <li>
                <label id="viewvalidationdialog-approvedbylabel" ><spring:message code="certificationvalidation.approvedby"/></label>
                <label id="viewvalidationdialog-rejectedbylabel" ><spring:message code="certificationvalidation.rejectedby"/></label>
                <span id="viewvalidationdialog-who"></span>
            </li>
            <li>
                <label><spring:message code="certificationvalidation.note"/></label>
                <span id="viewvalidationdialog-note"></span>
            </li>
        </ol>
        <div class="center">
        	<%-- Reset warning no longer required; new "pending" certificate validation record is entered to reset --%>
        	<%-- <p><b><spring:message code="certificationvalidation.reset.warning" /></b></p> --%>
            <button id="validationdialog-reset" class="center" ><spring:message code="certificationvalidation.reset"/></button>
        </div>
    </fieldset>


</div>


<%-- <div id="bulkvalidationcheckdialog" title="<spring:message code='certificationvalidation.bulktitle'/>" style="display: none;"> --%>

<!--     <div id="bulkvalidationcheckdialog-toacceptdiv" style="display: none"> -->
<%--         <h3><spring:message code="certificationvalidation.acceptrejectthesecerts"/></h3> --%>
<!--         <table id="bulkvalidationcheckdialog-toaccept" class="default"> -->
<!--             <thead> -->
<!--                 <tr> -->
<%--                     <th class="certno"><spring:message code="web.certificateno"/></th> --%>
<%--                     <th class="inst"><spring:message code="web.instrumentdesc" /></th> --%>
<%--                     <th class="plantno"><spring:message code="web.plantno" /></th> --%>
<%--                     <th class="serial"><spring:message code="web.serialno" /></th> --%>
<%--                     <th class="caldate"><spring:message code="web.caldate" /></th> --%>
<!--                 </tr> -->
<!--             </thead> -->
<!--             <tbody> -->

<!--             </tbody> -->
<!--         </table> -->
<!--     </div> -->

<!--     <div id="bulkvalidationcheckdialog-allreadyaccepteddiv" style="display: none"> -->
<%--         <h3><spring:message code="certificationvalidation.certsalreadyaccepted"/></h3> --%>
<!--         <table id="bulkvalidationcheckdialog-allreadyaccepted" class="default"> -->
<!--             <thead> -->
<!--             <tr> -->
<%--                 <th class="certno"><spring:message code="web.certificateno"/></th> --%>
<%--                 <th class="inst"><spring:message code="web.instrumentdesc" /></th> --%>
<%--                 <th class="plantno"><spring:message code="web.plantno" /></th> --%>
<%--                 <th class="serial"><spring:message code="web.serialno" /></th> --%>
<%--                 <th class="accepted"><spring:message code="acceptedon"/></th> --%>
<!--             </tr> -->
<!--             </thead> -->
<!--             <tbody> -->

<!--             </tbody> -->
<!--         </table> -->
       
<!--         <input type="checkbox" id="includeAppectedRejectedCerts" class="marg-top marg-bot" onchange="includeAppectedRejectedCertsChange(this);"/> -->
<%--         <b><spring:message code="certificationvalidation.includeacceptedrejectedcerts" /></b> --%>
<!--     </div> -->

<!--     <hr/> -->
<!--     <div class="center"> -->
<%--         <button id="bulkvalidationcheckdialog-accept"><spring:message code="certificationvalidation.accept"/></button> --%>
<%--         <button id="bulkvalidationcheckdialog-reject"><spring:message code="certificationvalidation.reject"/></button> --%>
<%--         <button id="bulkvalidationcheckdialog-cancel"><spring:message code="cancel"/></button> --%>
<!--     </div> -->

<!-- </div> -->