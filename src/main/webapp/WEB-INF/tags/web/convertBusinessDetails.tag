<%@ tag language="java" display-name="convertBusinessDetails" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ attribute name="businessDetails" required="true" type="java.lang.Object" %>

	<script type='text/javascript'>
		var businessDetails = new Object({company: '${businessDetails.company}', 
										  webEmail: '${businessDetails.webEmail}', 
										  salesEmail: '${businessDetails.salesEmail}', 
										  tel: '${businessDetails.tel}', 
										  fax: '${businessDetails.fax}'});
	</script>
