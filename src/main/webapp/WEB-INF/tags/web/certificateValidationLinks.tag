<%@ tag language="java" display-name="certificateValidationLinks"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%-- Tag to display table cell containing certificate customer acceptance status and provide
links to accept cert or view details of acceptance for those certs are already accepted or rejected --%>

<%@ attribute name="cert" required="true"
	type="org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate"
	description="The certificate that this validation applies to"%>
<%@ attribute name="validationrRequired" required="true"
	type="java.lang.Boolean"
	description="Does this customer use the customer certificate accept feature"%>
<%@ attribute name="validationAllowed" required="true"
	type="java.lang.Boolean"
	description="Does this user have permission to accept certificates"%>

<c:if test="${validationrRequired}">
	<td class="validation" align="center">
		<span class="acceptreject">
			<c:set var="latestValidation" value="${cert.validation}" />
			<c:choose>
				<c:when
					test="${cert.status eq 'SIGNED' || cert.status eq 'SIGNING_NOT_SUPPORTED'}">
					<c:choose>
						<c:when test="${(latestValidation != null) && (latestValidation.status != 'PENDING')}">
							<%-- ACCEPTED or REJECTED -- allows user to view status --%> 
							<a href="#" class="mainlink viewacceptance"
								data-acceptanceid="${latestValidation.id}"> <c:set
									var="dateStyle"
									value="${latestValidation.status == 'ACCEPTED' ? 'certaccepted' : 'certrejected'}" />
								<span class="accepteddate ${dateStyle}"> <fmt:formatDate
										value="${latestValidation.datevalidated}" type="DATE"
										dateStyle="SHORT" />
							</span>
							</a>
						</c:when>
						<c:when test="${validationAllowed && (latestValidation == null) || (latestValidation.status == 'PENDING')}">
							<%-- PENDING - Allows user to record acceptance / rejection again --%>
							<a href="#" class="mainlink accept" data-certid="${cert.certid}"
								data-certno="${cert.certno}"> <spring:message
									code="certificate.validate.acceptreject" />
							</a>
						</c:when>
						<c:otherwise>
							<spring:message code="certificate.validate.pendingaccept" />
						</c:otherwise>
					</c:choose>
				</c:when>
				<c:otherwise>
					<spring:message code="certificate.notsigned" />
				</c:otherwise>
			</c:choose>
		</span>
	</td>
</c:if>