<%@ tag language="java" display-name="instrumentLinkWeb" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ attribute name="scrapped" required="true" type="java.lang.Object" %>
<%@ attribute name="modelname" required="true" type="java.lang.Object" %>
<%@ attribute name="plantid" required="true" type="java.lang.Object" %>
<%@ attribute name="customerDescription" required="true" type="java.lang.Object" %>
<%@ attribute name="displayCalTimescale" required="true" type="java.lang.Object" %>
<%@ attribute name="calIntervalUnit" required="true" type="org.trescal.cwms.core.system.enums.IntervalUnit" %>
<%@ attribute name="calInterval" required="true" type="java.lang.Integer" %>
<%@ attribute name="calDueDate" required="true" type="java.time.LocalDate" %>
<%@ attribute name="instrumentmodel" required="false" type="org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel" %>


<c:set var="aclass" value="mainlink" />
<c:if test="${scrapped==true}">
    <img src="../img/icons/bin_closed.png" width="16" height="16" class="image_inl" alt="<spring:message code="lookupresultmessage.repairoutcome.unitber" />" title="<spring:message code="lookupresultmessage.repairoutcome.unitber" />" />
    <c:set var="aclass" value="mainlink-strike" />
</c:if>

<div title="${modelname}">
    <a href="viewinstrument.htm?plantid=${plantid}" class="${aclass}">
        <c:choose>
            <c:when test="${customerDescription!=NULL && (customerDescription!='')}">
                ${customerDescription}
            </c:when>
            <c:when test="${modelname != NULL && modelname != ''}">
                ${modelname}
            </c:when>
            <c:when test="${instrumentmodel != NULL}">
                <cwms:showmodel instrumentmodel="${instrumentmodel}" hideTypology="true" />
             </c:when>
            <c:otherwise>

            </c:otherwise>
        </c:choose>
    </a>
</div>
<div class="relative">
    <c:if test="${displayCalTimescale==true}">
    	<spring:message code="instrlink.calperiodpassed" var="msg" />
    	<cwms:calTimescaleGraphic calIntervalUnit="${calIntervalUnit}" calInterval="${calInterval}" calDueDate="${calDueDate}" hoverMessageBegining="${msg}"/>
    </c:if>
</div>
