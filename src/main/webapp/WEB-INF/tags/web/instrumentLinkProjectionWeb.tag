<%@ tag language="java" display-name="instrumentLinkProjectionWeb" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ attribute name="instrumentDto" required="true" type="org.trescal.cwms.core.jobs.certificate.dto.WebCertificateInstrumentLinkProjectionDTO" %>
<%@ attribute name="displayCalTimescale" required="true" type="java.lang.Object" %>


	
<c:set var="aclass" value="mainlink" />
<c:if test="${instrumentDto.scrapped == true}">
	<img src="../img/icons/bin_closed.png" width="16" height="16" class="image_inl" alt="<spring:message code="lookupresultmessage.repairoutcome.unitber" />" title="<spring:message code="lookupresultmessage.repairoutcome.unitber" />" />
	<c:set var="aclass" value="mainlink-strike" />
</c:if>

<div title='<cwms:showmodelprojection modelName="${instrumentDto.modelName}" subfamilyTypology="${instrumentDto.subfamilyTypology}" hideTypology="true" />'>
	<a href="viewinstrument.htm?plantid=${instrumentDto.plantid}" class="${aclass}">
	    <c:choose>
		    <c:when test="${not empty instrumentDto.customerDescription}">
		        ${instrumentDto.customerDescription}
		    </c:when>
		    <c:otherwise>
		        <cwms:showinstrumentprojection modelName="${instrumentDto.modelName}" subfamilyTypology="${instrumentDto.subfamilyTypology}"
			 	modelMfrType="${instrumentDto.modelMfrType}" mfrName="${instrumentDto.mfrName}" instModelName="${instrumentDto.instModelName}"
			 	mfrGeneric="${instrumentDto.mfrGeneric}" hideTypology="true"/>    
		    </c:otherwise>
		</c:choose>
	</a>
</div>
<div class="relative">
	<c:if test="${displayCalTimescale==true && instrumentDto.status!='DO_NOT_RECALL'}">
	    	<spring:message code="instrlink.calperiodpassed" var="msg" />
	    	<cwms:calTimescaleGraphic calIntervalUnit="${instrumentDto.calibrationIntervalUnit}" calInterval="${instrumentDto.calibrationInterval}" calDueDate="${instrumentDto.nextCalDueDate}" hoverMessageBegining="${msg}"/>
	</c:if>
</div>
