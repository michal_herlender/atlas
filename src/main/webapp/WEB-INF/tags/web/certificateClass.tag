<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<%@ attribute name="certificateClass" required="true" type="org.trescal.cwms.core.jobs.certificate.entity.CertificateClass" %>

<c:choose>
   <c:when test = "${!empty certificateClass}">
       <span style="align-content: center">(<c:out value="${certificateClass.description}"/>)</span>
   </c:when>
   <c:otherwise>
                                   			
   </c:otherwise>
</c:choose>
