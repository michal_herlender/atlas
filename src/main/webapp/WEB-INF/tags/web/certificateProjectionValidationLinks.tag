<%@ tag language="java" display-name="certificateValidationLinks"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%-- Tag to display table cell containing certificate customer acceptance status and provide
links to accept cert or view details of acceptance for those certs are already accepted or rejected --%>

<%@ attribute name="certStatus" required="true"
	type="org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum"
	description="The certificate Id that this validation applies to"%>
<%@ attribute name="certValidationId" required="false"
	type="java.lang.Integer"
	description="The certificate validation Id that this validation applies to"%>
<%@ attribute name="certValidationStatus" required="false"
	type="org.trescal.cwms.core.jobs.certificate.entity.validation.CertificateValidationStatus"
	description="The certificate validation Status that this validation applies to"%>
<%@ attribute name="certValidationDateValidated" required="false"
	type="java.util.Date"
	description="The certificate validation date validated that this validation applies to"%>
<%@ attribute name="certId" required="false"
	type="java.lang.Integer"
	description="The certificate Id that this validation applies to"%>
<%@ attribute name="certNo" required="false"
	type="java.lang.String"
	description="The certificate Number that this validation applies to"%>
<%@ attribute name="validationrRequired" required="true"
	type="java.lang.Boolean"
	description="Does this customer use the customer certificate accept feature"%>
<%@ attribute name="validationAllowed" required="true"
	type="java.lang.Boolean"
	description="Does this user have permission to accept certificates"%>

<c:if test="${validationrRequired}">
	<td class="validation nopadding" align="center">
		<span class="acceptreject nomargin nopadding">
			<c:choose>
				<c:when
					test="${certStatus eq 'SIGNED' || certStatus eq 'SIGNING_NOT_SUPPORTED'}">
					<c:choose>
						<c:when test="${(certValidationId != null) && (certValidationStatus != 'PENDING')}">
							<%-- ACCEPTED or REJECTED -- allows user to view status --%> 
							<a href="#" class="mainlink viewacceptance nomargin nopadding"
								data-acceptanceid="${certValidationId}"> <c:set
									var="dateStyle"
									value="${certValidationStatus == 'ACCEPTED' ? 'certaccepted' : certValidationStatus == 'REJECTED' ? 'certrejected' : 'certpending'}" />
								<span class="accepteddate ${dateStyle}"><fmt:formatDate value="${certValidationDateValidated}" dateStyle="SHORT"/></span>
							</a>
						</c:when>
						<c:when test="${validationAllowed && ((certValidationId == null) || (certValidationStatus == 'PENDING'))}">
							<%-- PENDING - Allows user to record acceptance / rejection again --%>
							<a href="#" class="mainlink accept" data-certid="${certId}"
								data-certno="${certNo}"> <spring:message
									code="certificate.validate.acceptreject" />
							</a>
						</c:when>
						<c:otherwise>
							<spring:message code="certificate.validate.pendingaccept" />
						</c:otherwise>
					</c:choose>
				</c:when>
				<c:otherwise>
					<spring:message code="certificate.notsigned" />
				</c:otherwise>
			</c:choose>
		</span>
	</td>
</c:if>