<%@ tag language="java" display-name="convertContactDetails" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ attribute name="currentContact" required="true" type="java.lang.Object" %>

	<script type='text/javascript'>
		var contactDetails = new Object({fname: '${currentContact.firstName}', name: '${currentContact.name}', email: '${currentContact.email}'});
	</script>

