<%@ tag language="java" display-name="jiStatusHistoryIcon" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!-- #*
	creates the tooltip pop up for viewing previous job item status's
	
	@param ($ji) the job item
*# -->
<%@ attribute name="ji" required="true" type="java.lang.Object" %>

<div class="float-right">
	<a href="?itemstatusid=${ji.jobItemId}&amp;ajax=itemstatus&amp;web=true&amp;width=600" onclick="return(false);" class="jconTip mainlink" title="<spring:message code='job.result.jobitemstatus' />" id="jis${ji.jobItemId}" tabindex="-1">
		<img src="../img/icons/viewactions.png" width="16" height="16" alt="<spring:message code='job.result.viewjobstatushistory' />" title="<spring:message code='job.result.viewjobstatushistory' />" />
	</a>
</div>