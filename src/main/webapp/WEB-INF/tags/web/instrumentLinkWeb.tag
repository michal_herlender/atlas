<%@ tag language="java" display-name="instrumentLinkWeb" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ attribute name="instrument" required="true" type="java.lang.Object" %>
<%@ attribute name="displayCalTimescale" required="true" type="java.lang.Object" %>

<!-- 
	@param ($instrument) - instrument object from which we can retrieve (plantid)	
	@param ($displayTimescale) - boolean value indicating whether to display calibration timescale indicator
 -->
	
<c:set var="aclass" value="mainlink" />
<c:if test="${instrument.scrapped}">
	<img src="../img/icons/bin_closed.png" width="16" height="16" class="image_inl" alt="<spring:message code="lookupresultmessage.repairoutcome.unitber" />" title="<spring:message code="lookupresultmessage.repairoutcome.unitber" />" />
	<c:set var="aclass" value="mainlink-strike" />
</c:if>

<div title='<cwms:showmodel instrumentmodel="${instrument.model}" hideTypology="true" />'>
	<a href="viewinstrument.htm?plantid=${instrument.plantid}" class="${aclass}">
	    <c:choose>
		    <c:when test="${not empty instrument.customerDescription}">
		        ${instrument.customerDescription}
		    </c:when>
		    <c:otherwise>
		        <cwms:showinstrument instrument="${instrument}" hideTypology="true"/>    
		    </c:otherwise>
		</c:choose>
	</a>
</div>
<div class="relative">
	<c:if test="${displayCalTimescale==true && instrument.status!='DO_NOT_RECALL'}">
	    	<spring:message code="instrlink.calperiodpassed" var="msg" />
	    	<cwms:calTimescaleGraphic calIntervalUnit="${instrument.calFrequencyUnit}" calInterval="${instrument.calFrequency}" calDueDate="${instrument.nextCalDueDate}" hoverMessageBegining="${msg}"/>
	</c:if>
</div>
