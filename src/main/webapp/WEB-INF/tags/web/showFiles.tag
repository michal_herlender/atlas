<%@ tag language="java" display-name="showFiles" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>

<%@ attribute name="entity" required="true" type="java.lang.Object" %>
<%@ attribute name="sc" required="true" type="java.lang.Object" %>
<%@ attribute name="id" required="true" type="java.lang.Integer" %>
<%@ attribute name="identifier" required="true" type="java.lang.String" %>
<%@ attribute name="rootFiles" required="true" type="java.lang.Object" %>
<%@ attribute name="rootTitle" required="true" type="java.lang.String" %>
<%@ attribute name="deleteFiles" required="true" type="java.lang.Boolean" %>


<%--
*
* Note - this is used for the client web portal only, not yet replaced with showFilesForSc due to /web/ path (could add web parameter?) 
*
* Displays a folder structure that allows the user to navigate through a component's file system.
* @param $entity the (component) entity (e.g. the Job, Quotation object, etc).
* @param $sc the SystemComponent object related to the entity type.
* @param $id the ID of the entity referenced above.
* @param $identifier the String identifier of the entity referenced above (e.g. the Job No, Quotation No, etc).
* @param $rootFiles the files in the root directory of the component. (FileBrowserWrapper)
* @param $rootTitle the text displayed next to the root folder
* @param $deleteFiles boolean to indicate whether files can be deleted
--%>

<%-- 
2) Upload Via neues JS als Popup Box
3) Delete Request Via AJAX?? 
--%>

<script type="module" src="../script/components/cwms-overlay/cwms-overlay.js" ></script>
<c:set var="ver" value=""/>

<div>
	<c:set var="style" value=" width: 90%; "/>
	<div class="filebrowserinner" style="${style}">
		<c:choose>
			<c:when test="${empty rootFiles}">
				<p>The root file wrapper was not provided.</p>
			</c:when>
			<c:otherwise>
				<div class="float-right">
					<a href="#" onclick=" event.preventDefault(); document.querySelector('cwms-upload-file').show(); " class="mainlink" title="<spring:message code="jicertificates.uplfile"/>">
						<spring:message code="jicertificates.uplfile"/>
					</a>
				</div>
				<ul class="fileTree">
					<c:set var="directoryClass" value="directory expanded"/>
					<li class="${directoryClass}">
						<a href="#" id="fbRootAnchor" class="underline" onclick=" event.preventDefault(); getFilesForDirectory(this, null, ${sc.componentId}, ${id}, ${currentContact.personid}, 'false', 'false'); ">
							${rootTitle}
						</a>
						
							<ul class="fileTree">
								<c:choose>
									<c:when test="${empty rootFiles.getFiles()}">
										<li>-- <spring:message code="showfilesforsc.empty"/> --</li>
									</c:when>
									<c:otherwise>
										<c:forEach var="fw" items="${rootFiles.getFiles()}">
											<c:if test="${fw.fileName != 'Thumbs.db'}">
												<c:set var="escapedFileName"><spring:escapeBody javaScriptEscape="true">${fw.fileName}</spring:escapeBody></c:set>
												<c:choose>
													<c:when test="${fw.isDirectory()}">
														 <li class="directory collapsed">
															<a href="#" class="underline" onclick=" event.preventDefault(); getFilesForDirectory(this, '${escapedFileName}', ${sc.componentId}, ${id}, ${currentContact.personid}, 'false', 'false');" title="(<spring:message code="showfilesforsc.lastmodified"/>: ${fw.lastModified})">
																&nbsp;<span style="color: ${fw.highlightColour};">${fw.fileName}</span>
															</a>
			                                              </li>
													</c:when>
													<c:otherwise>
			                                            <li class="file ext_${fw.extension}">&nbsp;
															<c:choose>
																<c:when test="${(fw.extension == 'html') || (fw.extension == 'htm')}">
			                                                		<span>${fw.fileName}</span>
																</c:when>
																<c:otherwise>
																	<a href="downloadfile.htm?file=${fw.filePathEncrypted}" class="underline" target="_blank" title="(<spring:message code="showfilesforsc.lastmodified"/>: ${fw.lastModified})">&nbsp;
																		<span style="color: ${fw.highlightColour};">${fw.fileName}</span>
																	</a>
																</c:otherwise>
															</c:choose>
															<c:if test="${!fw.directory}">
																<span>
																	<c:if test="${deleteFiles}">
																		<a href="#" class="fileutility" onclick=" event.preventDefault(); deleteFile(this, '${fw.fileNameEncrypted}', '', ${sc.componentId}, ${id}); ">
																			<img src="../img/icons/delete.png" width="16" height="16" class="img_marg_bot" alt="<spring:message code="showfilesforsc.deletethisfile"/>" title="<spring:message code="showfilesforsc.deletethisfile"/>" />
																		</a>
																	</c:if>
																</span>
															</c:if>												
														</li>
													</c:otherwise>
												</c:choose>
											</c:if>							
										</c:forEach>
									</c:otherwise>
								</c:choose>
							</ul>
					</li>
				</ul>				
				<!-- clear floats and restore page flow -->
				<div class="clear"></div>
				
			</c:otherwise>
		</c:choose>
	</div>
</div>