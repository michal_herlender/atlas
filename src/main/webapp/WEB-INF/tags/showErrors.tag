<%@ tag language="java" display-name="showErrors" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%-- 
	Custom tag to display Spring bind errors in a header, e.g. at top of edit page.
	By default only "global" errors are displayed
	The path should be the bind path to which the form is bound, e.g. path="form.*"
	If all errors are to be shown (including "field" errors) then set showFieldErrors="true"   
--%>
<%@ attribute name="path" required="true" type="java.lang.String" %>
<%@ attribute name="showFieldErrors" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showFieldName" required="false" type="java.lang.Boolean" %>

<spring:bind path="${path}">
	<c:if test="${status.error}">
		<div class="warningBox1">
			<div class="warningBox2">
				<div class="warningBox3">
					<h5 class="center-0 attention">
						<spring:message code="error.save"/>
					</h5>
					<div class="attention">
						<c:forEach var="e" items="${status.errors.globalErrors}">
							<div class="center attention">
								<spring:message message="${e}" arguments="${e.arguments}"/>
							</div>
						</c:forEach>
						<c:if test="${showFieldErrors}">
							<c:forEach var="e" items="${status.errors.fieldErrors}">
								<div class="center attention">
									<c:if test="${ empty showFieldName || showFieldName }">
										<c:out value="${e.field}"/>
										:
									</c:if>
									<spring:message message="${e}"/>
								</div>
							</c:forEach>
						</c:if>
					</div>
				</div>
			</div>
		</div>							
	</c:if>
</spring:bind>