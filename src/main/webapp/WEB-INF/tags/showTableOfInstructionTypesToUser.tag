<%@ tag language="java" display-name="showTableOfInstructionTypesToUser" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image"%>

<%@ attribute name="instructionLinks" required="true" type="java.lang.Object" %>
<%@ attribute name="userInstructionTypes" required="true" type="java.lang.Object" %>
<%@ attribute name="stringType1" required="true" type="java.lang.Object" %>
<%@ attribute name="stringType2" required="true" type="java.lang.Object" %>

<table id="instructiontable" class="default4 highlight_table" summary="This table displays any instructions">
	<thead>
		<tr>
			<th><spring:message code="instructiontype.readinstruction" /></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="link" items="${instructionLinks}">
			<c:forEach var="uit" items="${userInstructionTypes}">
				<c:if test="${uit.instructionType == link.instruction.instructiontype}">
					<c:if test="${stringType1.toUpperCase() == link.instruction.instructiontype || stringType2.toUpperCase() == link.instruction.instructiontype}">
						<tr>
							<td>
								<c:set var="isCompanyOrSubdiv" value="${link.instructionEntity == 'COMPANY' || link.instructionEntity == 'SUBDIV'}" />
								<c:choose>
									<c:when test="${isCompanyOrSubdiv && link.subdivInstruction }">
										<image:businessSubdivisionSpecific/>
									</c:when>
									<c:otherwise>
										<image:businessCompanySpecific/>
									</c:otherwise>
								</c:choose>
								<c:if test="${isCompanyOrSubdiv && link.subdivInstruction }">
									<strong>[${link.businessSubdiv.subname}]</strong> 
								</c:if>
								<strong>${link.instruction.instructiontype.type}&nbsp;:</strong>&nbsp;
								<span
									<c:choose>
										<c:when test='${datetools.isDateAfterXTimescale(-1, "m", link.instruction.lastModified)}'>
											class="red bold"
										</c:when>
										<c:when test='${datetools.isDateAfterXTimescale(-3, "m", link.instruction.lastModified)}'>
											class="amber bold"
										</c:when>
										<c:otherwise>
											class="brown bold"
										</c:otherwise>
									</c:choose>
								>
									${link.instruction.instruction}
								</span>
							</td>
						</tr>
					</c:if>
				</c:if>
			</c:forEach>
		</c:forEach>
	</tbody>
</table>