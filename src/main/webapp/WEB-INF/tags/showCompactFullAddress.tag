<%@ tag language="java" display-name="showCompactFullAddress" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>

<%@ attribute name="address" required="true" type="java.lang.Object" %>
<%@ attribute name="copy" required="true" type="java.lang.Object" %>
<%@ attribute name="edit" required="true" type="java.lang.Object" %>

<c:set var="count" value="1"/>
<c:if test='${address.addr1 != ""}'>
	<c:set var="count" value="${count + 1}"/>
</c:if>
<c:if test='${address.addr2 != ""}'>
	<c:set var="count" value="${count + 1}"/>
</c:if>
<c:if test='${address.addr3 != ""}'>
	<c:set var="count" value="${count + 1}"/>
</c:if>
<c:if test='${address.town != ""}'>
	<c:set var="count" value="${count + 1}"/>
</c:if>
<c:if test='${address.county != ""}'>
	<c:set var="count" value="${count + 1}"/>
</c:if>
<c:if test='${address.postcode != ""}'>
	<c:set var="count" value="${count + 1}"/>
</c:if>
<c:set var="expandAddress" value="${count * 16}"/>

<div class="expandingAddress float-left">
	<c:if test='${address.addr1 != ""}'>
			<a href="#" class="mainlink" onClick="$j(this).parent().animate( { height: '${expandAddress}' }, 1000 );  return false;">
			${address.addr1}
			</a>&nbsp;&nbsp;
		<c:if test="${copy}">
			<img src="img/icons/bullet_clipboard.png" width="10" height="10" onclick=" $j(this).parent().find('div').clipBoard(this, null, false, null); " alt="Add address to clipboard" title="Add address to clipboard" />&nbsp;&nbsp;
		</c:if>
		<br /><br/>
		<div>
			<c:if test='${address.addr2 != ""}'>${address.addr2}<br /></c:if>
			<c:if test='${address.addr3 != ""}'>${address.addr3}<br /></c:if>
			<c:if test='${address.town != ""}'>${address.town}<br /></c:if>
			<c:if test='${address.county != ""}'>${address.county}<br /></c:if>
			<c:if test='${address.postcode != ""}'>${address.postcode}<br /></c:if>
		</div>
	</c:if>
</div>
<c:if test="${edit}">
	<div class="float-right">
		<cwms:securedLink permission="ADDRESS_VIEW" classAttr="mainlink" parameter="?addrid=${address.addrid}">
			<spring:message code="edit"/>
		</cwms:securedLink>
	</div>
</c:if>