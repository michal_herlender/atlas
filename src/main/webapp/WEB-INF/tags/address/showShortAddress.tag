<%@ tag language="java" display-name="showShortAddress" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="address" required="true" type="java.lang.Object" %>
<%-- #macro(showShortAddress $address) --%>
<c:set var="addressText" value=""/>
<c:if test="${not empty address.addr1}"><c:set var="addressText" value="${addressText} ${address.addr1}, "/></c:if>
<c:if test="${not empty address.addr2}"><c:set var="addressText" value="${addressText} ${address.addr2}, "/></c:if>
<c:if test="${not empty address.town}"><c:set var="addressText" value="${addressText} ${address.town}, "/></c:if>
<c:if test="${not empty address.county}"><c:set var="addressText" value="${addressText} ${address.county}, "/></c:if>
${addressText}