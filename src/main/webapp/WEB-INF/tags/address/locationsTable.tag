<%@ tag language="java" display-name="locationsTable" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="address" required="true" type="java.lang.Object" %>
<%@ attribute name="instrumentCount" required="true" type="java.lang.Object" %>
<%@ attribute name="instrumentCountMap" required="true" type="java.lang.Object" %>

<table class="default2 addressLocations">
	<thead>
		<tr>
			<td colspan="4">
				<spring:message code="company.locations"/>: ${address.locations.size()}
			</td>
		</tr>
		<tr>
			<th><spring:message code="company.active"/></th>
			<th><spring:message code="company.location"/></th>
			<th><spring:message code="numberofinstruments"/></th>
			<th></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="4">
				<a href="location.htm?addrid=${address.addrid}" class="mainlink-float" title="<spring:message code="company.addnewlocation" />">
					<spring:message code="company.addnewlocation" />
				</a>
			</td>
		</tr>
	</tfoot>
	<tbody>
		<c:forEach var="location" items="${address.locations}">
			<tr>
				<td><spring:message code="${location.active}"/></td>
				<td><c:out value="${location.location}"/></td>
				<td><c:out value="${instrumentCountMap.get(location.locationid)}"/></td>
				<td>
					<a href="location.htm?locid=${location.locationid}" class="mainlink">
						<spring:message code="edit"/>
					</a>
				</td>
			</tr>
		</c:forEach>
		<tr class="odd">
			<td></td>
			<td><spring:message code="cdsearch.any"/></td>
			<td>${instrumentCount}</td>
			<td></td>
		</tr>
	</tbody>
</table>