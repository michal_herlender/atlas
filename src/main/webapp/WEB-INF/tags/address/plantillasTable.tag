<%@ tag language="java" display-name="plantillasTable" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="address" required="true" type="java.lang.Object" %>
<%@ attribute name="plantillasSettings" required="true" type="java.lang.Object" %>
<%@ attribute name="syncToPlantillas" required="true" type="java.lang.Object" %>
<%@ attribute name="usesPlantillas" required="true" type="java.lang.Object" %>

<table class="default2 plantillasCompany">
	<thead>
		<tr>
			<td colspan="4">
				<spring:message code="company.plantillassettings"/>
			</td>
		</tr>
		<tr>
			<th class="name" scope="col"><spring:message code="name"/></th>
			<th class="value" scope="col"><spring:message code="company.value"/></th>
		</tr>
	</thead>
		<tr>
			<td><spring:message code="company.businesscompanyusesplantillas"/></td>
			<td><spring:message code="${usesPlantillas}"/></td>
		</tr>
		<tr class="odd">
			<td><spring:message code="company.synctoplantillas"/></td>
			<td><spring:message code="${syncToPlantillas}"/></td>
		</tr>
	</tbody>
</table>

<table class="default2 plantillasAddress">
	<thead>
		<tr>
			<td colspan="4">
				<spring:message code="address.plantillassettings"/>
				<a href="plantillasedit.htm?addrid=${address.addrid}" class="mainlink-float" title="<spring:message code="address.editplantillassettings" />">
					<spring:message code="address.editplantillassettings" />
				</a>
			</td>
		</tr>
		<tr>
			<th class="name" scope="col"><spring:message code="name"/></th>
			<th class="value" scope="col"><spring:message code="company.value"/></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><spring:message code="plantillas.formercustomerid"/></td>
			<td>${plantillasSettings.formerCustomerId}</td>
		</tr>
		<tr class="odd">
			<td><spring:message code="plantillas.putnextdatecerts"/></td>
			<td>
				<c:choose>
					<c:when test="${not empty plantillasSettings}"><spring:message code="${plantillasSettings.nextDateOnCertificate}"/></c:when>
					<c:otherwise><spring:message code="false"/></c:otherwise>
				</c:choose>
			</td>
		</tr>
		<tr>
			<td><spring:message code="plantillas.textuncertaintyfailure"/></td>
			<td>${plantillasSettings.textUncertaintyFailure}</td>
		</tr>
		<tr class="odd">
			<td><spring:message code="plantillas.texttolerancefailure"/></td>
			<td>${plantillasSettings.textToleranceFailure}</td>
		</tr>
		<tr>
			<td><spring:message code="plantillas.sendcertsbyftp"/></td>
			<td>
				<c:choose>
					<c:when test="${not empty plantillasSettings}"><spring:message code="${plantillasSettings.sendByFtp}"/></c:when>
					<c:otherwise><spring:message code="false"/></c:otherwise>
				</c:choose>
			</td>
		</tr>
		<tr class="odd">
			<td><spring:message code="plantillas.ftpserver"/></td>
			<td>${plantillasSettings.ftpServer}</td>
		</tr>
		<tr>
			<td><spring:message code="plantillas.ftpuser"/></td>
			<td>${plantillasSettings.ftpUser}</td>
		</tr>
		<tr class="odd">
			<td><spring:message code="plantillas.ftppassword"/></td>
			<td>${plantillasSettings.ftpPassword}</td>
		</tr>
		<tr>
			<td><spring:message code="plantillas.metraclient"/></td>
			<td>
				<c:choose>
					<c:when test="${not empty plantillasSettings}"><spring:message code="${plantillasSettings.metraClient}"/></c:when>
					<c:otherwise><spring:message code="false"/></c:otherwise>
				</c:choose>
			</td>
		</tr>
		<tr>
			<td><spring:message code="company.contract"/></td>
			<td>
				<c:choose>
					<c:when test="${not empty plantillasSettings}"><spring:message code="${plantillasSettings.contract}"/></c:when>
					<c:otherwise><spring:message code="false"/></c:otherwise>
				</c:choose>
			</td>
		</tr>
	</tbody>
</table>