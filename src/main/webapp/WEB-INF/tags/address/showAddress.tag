<%@ tag language="java" display-name="showAddress" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ attribute name="address" required="true" type="java.lang.Object" %>
<%@ attribute name="copy" required="true" type="java.lang.Boolean" %>
<%@ attribute name="separator" required="true" type="java.lang.String" %>
<%@ attribute name="vis" required="true" type="java.lang.Boolean" %>

<!-- showAddress address="" copy="" separator="" vis="" -->
<c:set var="addressText" value=""/>
<c:if test='${address.addr1 != ""}'>
	<c:set var="addressText" value='${address.addr1}'/>
</c:if>
<c:if test='${address.addr2 != ""}'>
	<c:set var="addressText" value='${addressText.concat(separator).concat(address.addr2)}'/>
</c:if>
<c:if test='${address.town != ""}'>
	<c:set var="addressText" value='${addressText.concat(separator).concat(address.town)}'/>
</c:if>
<c:if test='${address.county != ""}'>
	<c:set var="addressText" value='${addressText.concat(separator).concat(address.county)}'/>
</c:if>
<c:if test='${address.postcode != ""}'>
	<c:set var="addressText" value='${addressText.concat(separator).concat(address.postcode)}'/>
</c:if>
<div class=<c:choose><c:when test="${vis}">"inline"</c:when><c:otherwise>"hid"</c:otherwise></c:choose>>${addressText}</div>
<c:if test="${copy}">
	<c:choose>
		<c:when test="${vis}">
			<br />
		</c:when>
		<c:otherwise>
			&nbsp;&nbsp;
		</c:otherwise>
	</c:choose>
	<img src="img/icons/bullet_clipboard.png" width="10" height="10" onclick=" $j(this).parent().find('div').clipBoard(this, null, false, null); " alt="<spring:message code='addresstag.toclipboard'/>" title="<spring:message code='addresstag.toclipboard'/>" />
</c:if>