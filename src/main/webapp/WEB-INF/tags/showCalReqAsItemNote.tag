<%@ tag language="java" display-name="showCalReqAsItemNote" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="instrument" tagdir="/WEB-INF/tags/instrument"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%@ attribute name="calReqs" required="true" type="java.lang.Object" %>
<%@ attribute name="ji" required="true" type="java.lang.Object" %>
<%@ attribute name="page" required="true" type="java.lang.Object" %>

<%--	Displays the given calibration requirements as an item note   --%>

<li class="itemCalReq${calReqs.id}">
	<div class="inlinenotes_note">
		<c:choose>
			<c:when test="${not empty calReqs.pointSet}">
				<span class="bold"><spring:message code="calibrationpoints.calpoints" />:</span>
				<span class="attention bold"><instrument:showPoints pointSet="${calReqs.pointSet}" /></span>
				<br />
			</c:when>
			<c:when test="${not empty calReqs.range}">
				<span class="bold"><spring:message code="calibrationpoints.calrange" />:</span>
				<span class="attention bold">
					${calReqs.range} 
					<c:if test="${calReqs.range.relational}">
						@ ${calReqs.range.getFormattedRelatedPoint()} ${calReqs.range.relatedUom.formattedSymbol}
					</c:if>
				</span>
				<br />
			</c:when>
			<c:otherwise>
				<span class="bold"><spring:message code="calibrationpoints.calinstructions" />:</span>
				<br />
			</c:otherwise>
		</c:choose>
		<c:if test="${calReqs.privateInstructions != null && calReqs.privateInstructions.trim().length() > 0}">
			<span class="attention bold"><spring:message code="system.private" />: ${calReqs.privateInstructions.replaceAll("\\n","<br/>")}</span>
			<br />
		</c:if>
		<c:if test="${calReqs.publicInstructions != null && calReqs.publicInstructions.trim().length() > 0}">
			<span class="allgood bold"><spring:message code="system.public" />: ${calReqs.publicInstructions.replaceAll("\\n","<br/>")}</span>
			<br />
		</c:if>
 		<span class="bold"><spring:message code="${calReqs.source}" arguments="${calReqs.sourceParameter}" /></span>
	</div>
	<div class="inlinenotes_edit">
		<span>
			<span class="bold">
				<c:choose>
					<c:when test="${calReqs.publish}">
						<spring:message code="note.published" />
					</c:when>
					<c:otherwise>
						<spring:message code="note.notpublished" />
					</c:otherwise>
				</c:choose>
			</span>
			<a href="#" class="editPoints" onclick=" loadScript.createOverlay('external', '<spring:message code="showcalreqaslistitem.editcalpointsrequirements" />', 'calibrationpoints.htm?type=${calReqs.classKey}&page=${page}&calreqid=${calReqs.id}', null, null, 80, 900, null); return false; " title="<spring:message code='showcalreqaslistitem.editcalpointsrequirements' />"></a>
			<c:choose>
				<c:when test="${calReqs.classKey == 'instrument'}">
					<c:set var="plantid" value="${calReqs.inst.plantid}"/>
				</c:when>
				<c:otherwise>
					<c:set var="plantid" value="null"/>
				</c:otherwise>
			</c:choose>
			<a href="#" class="deletePoints" onclick=" deactivateCalReqs('${page}', ${calReqs.id}, '${calReqs.classKey}', ${plantid}); return false; " title="Remove Points &amp; Requirements"></a>
		</span>
		<br />
		<span>
			<c:if test="${not empty calReqs.lastModifiedBy}">
				${calReqs.lastModifiedBy.name}
			</c:if>
			<c:if test="${not empty calReqs.lastModified}">
				(<fmt:formatDate pattern="dd.MM.yyyy - hh:mm:ss a" value="${calReqs.lastModified}"/>)
			</c:if>
		</span>
	</div>
	<div class="clear-0"></div>
</li>