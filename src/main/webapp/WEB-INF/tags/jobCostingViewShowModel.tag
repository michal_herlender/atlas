<%@ tag language="java" display-name="showViewModelLink" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ attribute name="modelnametranslation" required="true" type="java.lang.Object" %>
<%@ attribute name="instrumentsubfamilytypology" required="true" type="java.lang.Object" %>
<%@ attribute name="hidetypology" required="false" type="java.lang.Object" %>

<c:choose>
	<c:when test="${ empty modelnametranslation }">
		null
	</c:when>
	<c:otherwise>
		${modelnametranslation}
		<c:if test="${(empty hidetypology or !hidetypology) and instrumentsubfamilytypology eq 'UNDEFINED'}">
			(${instrumentsubfamilytypology})
		</c:if>
	</c:otherwise>
</c:choose>