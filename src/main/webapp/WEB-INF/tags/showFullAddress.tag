<%@ tag language="java" display-name="showFullAddress" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ attribute name="address" required="true" type="java.lang.Object" %>
<%@ attribute name="copy" required="true" type="java.lang.Object" %>
<%@ attribute name="separator" required="true" %>

<c:set var="addressText" value=""/>
<c:if test="${address.addr1 != \"\"}">
	<c:set var="addressText" value="${addressText}${address.addr1}${separator}"/>
</c:if>
<c:if test="${address.addr2 != \"\"}">
	<c:set var="addressText" value="${addressText}${address.addr2}${separator}"/>
</c:if>
<c:if test="${address.addr3 != \"\"}">
	<c:set var="addressText" value="${addressText}${address.addr3}${separator}"/>
</c:if>
<c:if test="${address.town != \"\"}">
	<c:set var="addressText" value="${addressText}${address.town}${separator}"/>
</c:if>
<c:if test="${address.county != \"\"}">
	<c:set var="addressText" value="${addressText}${address.county}${separator}"/>
</c:if>
<c:if test="${address.postcode != \"\"}">
	<c:set var="addressText" value="${addressText}${address.postcode}${separator}"/>
</c:if>
<c:if test="${address.country != \"\"}">
	<c:set var="addressText" value="${addressText}${address.country.localizedName}"/>
</c:if>
<div class="inline">
	${addressText}
</div>
<c:if test="${copy}">
	<img src="img/icons/bullet_clipboard.png" width="10" height="10" onclick=" $j(this).parent().find('div').clipBoard(this, null, false, null); " alt="Add address to clipboard" title="Add address to clipboard" />
</c:if>