<!DOCTYPE HTML>

<%@ tag language="java" display-name="webTemplate" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<c:set var="req" value="${pageContext.request}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="uri" value="${req.requestURI}" />
<c:set var="loadtab" value="" />
<c:if test="${not empty req.getParameter('loadtab')}">
	<c:set var="loadtab" value="${req.getParameter('loadtab')}" />
</c:if>

<%@ attribute name="idheader" required="true" type="java.lang.String" %>
<%@ attribute name="scriptPart" fragment="true" required="false" %>
<%@ attribute name="restrictedView" required="false" %>
<%@ attribute name="globalElements" fragment="true" required="false" %>

<html>
	<head>
		<link rel="SHORTCUT ICON" href="../img/favicon.png" />
		<link rel="stylesheet" href="../styles/global_web.css" type="text/css" media="screen" />
		
		<c:if test="${systemType == 'development'}">
			<link rel="stylesheet" href="../styles/theme/theme_web_test.css" type="text/css" />
		</c:if>
		
		<link rel="stylesheet" href="../styles/jQuery/jquery-ui.css" type="text/css" media="screen" />
		<sec:csrfMetaTags />
		<jsp:invoke fragment="scriptPart"/>
		<script type='text/javascript'>
			// get value of loadtab parameter from querystring
			// used to show the correct tab when subnavigation bar is used
			var LOADTAB = '${loadtab}';

			// get string for certificate seal 
			var sslCertCode = '${sslCert}';

			// set variable to i18n of javascript
			var i18locale = '${rc.locale.language}';
			
			// set variable to default context of project
			var springUrl = '${req.contextPath}';
			
		</script>
		<script type='text/javascript' src='../script/thirdparty/jQuery/jquery.js'></script>
		<script type='text/javascript' src='../script/thirdparty/jQuery/jquery.ui.js'></script>
		<script type='text/javascript' src='../script/thirdparty/jQuery/jquery-migrate-1.2.1.js'></script> 
		<script type='text/javascript' src='../script/web/template/Web_Main.js'></script>

		<title><spring:message code="web.pagetitle" arguments="${businessDetails.company}" /></title>
	</head>
	
	<body onload="loadScript.init(true);">
		<div class="headLink">
			<div class="headLinkBox">
				<div class="loggedInUser">					
					<spring:message code="web.loggedinuser" arguments="${currentContact.name}" />
					<a href="" onclick="event.preventDefault(); logout();">
						<img src="../img/icons/logout.png" width="16" height="16" alt="<spring:message code="web.logout"/>" title="<spring:message code="web.logout"/>" />
					</a>
					<form:form id="logout" action="../logout" method="post" class="hid">
						<input type="submit" value="logout"/>
					</form:form>
				</div>

				<div class="headLinkList">								
					<a href="<c:url value="/web/downloadusermanual.htm" />"><spring:message code="web.usermanual" /></a>
					|					
					<a href="<c:url value="/web/websearchinstrumentbyuser.htm" />"><spring:message code="web.myinstruments" /></a>
					|					
					<a href="<c:url value="/web/myprofile.htm" />"><spring:message code="web.myprofile" /></a>
					|
					<a href="<c:url value="/web/downloads.htm" />"><spring:message code="web.downloads" /></a>
					|
					<a href="#" onclick=" event.preventDefault(); window.location='addfeedback.htm?url=' + window.location.href"><spring:message code="web.providefeedback" /></a>
					|
					<a href="<c:url value="/web/contact.htm" />"><spring:message code="web.contactus" /></a>
				</div>
				<!-- clear floats and restore page flow -->
				<div class="clear"></div>
			</div>
		</div>	
		
		<div class="mainBox">
			<!-- main box head div contains logos and links for common functionality -->
			<div class="mainBoxHead" style=" vertical-align: bottom; ">
				<!-- Trescal logo -->		
				<img src="../img/logo_colour_white.png" alt="${businessDetails.company}" title="${businessDetails.company}" /><br />
				<div class="laserlogo"></div>
				<!-- main box head links div contains images and links for email contacts -->
				<div class="mainBoxHeadLinks">

					<a style="color:white" href="<c:url value="/web/changelocale.htm"/>"> 
						<%= response.getLocale().getDisplayLanguage(response.getLocale()) %>
						(<%= response.getLocale().getDisplayCountry(response.getLocale()) %>)
					</a>
					
					<!-- email the web team link -->
					<div>
						<a href="mailto:${businessDetails.webEmail}" class="small_link email"><spring:message code="web.emailwebteam" /></a>
					</div>
					<!-- email the customer services team link -->
					<div>
						<a href="mailto:${businessDetails.salesEmail}" class="small_link email"><spring:message code="web.emailcustomerservicesteam" /></a>
					</div>
				</div>
				<!-- end of main box head links -->
			</div>
			<!-- end of main box div -->
			
			<!-- main box tabs contains all navigation tabs -->
			<div class="mainBoxTabs">
				<div class="mainBoxTabsContainer">
					<ul>
						<li <c:if test="${idheader=='home'}"> id="current" </c:if>><a href="<c:url value="/web/home.htm" />"><spring:message code="web.home" /></a></li>
						<li <c:if test="${idheader=='cert'}"> id="current" </c:if>><a href="<c:url value="/web/certsearch.htm" />"><spring:message code="web.certificates" /></a></li>
						<li <c:if test="${idheader=='quote'}"> id="current" </c:if>><a href="<c:url value="/web/quoterequest.htm" />"><spring:message code="web.quotes" /></a></li>
						<!-- Hide collected items for Madrid -->
						<li class="hid" <c:if test="${idheader=='collection'}"> id="current" </c:if>><a href="<c:url value="/web/createcollection.htm" />"><spring:message code="web.collections" /></a></li>
						<li <c:if test="${idheader=='recall'}"> id="current" </c:if>><a href="<c:url value="/web/webrecall.htm" />"><spring:message code="web.recall" /></a></li>
						<li <c:if test="${idheader=='instrumentstatus'}"> id="current" </c:if>><a href="<c:url value="/web/equipmentstatus.htm?display=default" />"><spring:message code="web.instrumentstatus" /></a></li>
						<li <c:if test="${idheader=='job'}"> id="current" </c:if>><a href="<c:url value="/web/jobs.htm" />"><spring:message code="web.jobs" /></a></li>
						<li <c:if test="${idheader=='instrument'}"> id="current" </c:if>><a href="<c:url value="/web/searchinstrument.htm" />"><spring:message code="web.searchinstruments" /></a></li>
						<c:if test="${userAddInstRule != 'NONE'}">
							<li <c:if test="${idheader=='newinstrument'}"> id="current" </c:if>><a href="<c:url value="/web/addinstrumentmodelsearch.htm" />"><spring:message code="web.addinstrument" /></a></li>
						</c:if>
					</ul>
			  </div>
				<div class="clear"></div>
			</div>
			<!-- end of navigation tabs div -->
			
			<!-- main box body div contains content for page -->
			<div class="mainBoxBody">
				<jsp:doBody/>
				

				<div class="mainBoxBodyContentFooter">
					<jsp:useBean id="date" class="java.util.Date" />
					&copy;&nbsp;<fmt:formatDate value='${date}' pattern='yyyy' />&nbsp;Trescal
				</div>
			</div>
			<!-- end of main box body div -->
		</div>
	</body>
</html>