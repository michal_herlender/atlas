<%@ tag language="java" display-name="showResultsPagination" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="rs" required="true" type="java.lang.Object" %>
<%@ attribute name="pageNoId" required="true" type="java.lang.Object" %>

<!-- Begin pagination section -->
<div class="text-center">
	<c:choose>
		<c:when test="${rs.resultsCount == 0}">
			<strong><spring:message code="pagination.noresults" /></strong>
		</c:when>
		<c:otherwise>
			<c:choose>
				<c:when test="${pageNoId == ''}">
					<c:set var="pageNoIdVar" value="pageNo"/>
				</c:when>
				<c:otherwise>
					<c:set var="pageNoIdVar" value="${pageNoId}"/>
				</c:otherwise>
			</c:choose>
			<c:if test="${rs.previousPage}">
				<c:set var="prevPage" value="${rs.currentPage - 1}"/>
			</c:if>
			<input type="submit" name="prev" id="previouspage" value="<spring:message code="pagination.prev"/>" class="float-left"
				   onclick=" $j('#${pageNoIdVar}').attr('value', ${prevPage}); " <c:if test="${!rs.previousPage}">disabled="disabled"</c:if> />
			<c:if test="${rs.nextPage}">
				<c:set var="nextPage" value="${rs.currentPage + 1}"/>
			</c:if>
			<input type="submit" name="next" id="nextpage" value="<spring:message code="pagination.next"/>" class="float-right"
				   onclick=" $j('#${pageNoIdVar}').attr('value', ${nextPage}); " <c:if test="${!rs.nextPage}">disabled="disabled"</c:if> />
			<strong><spring:message code="pagination.displayingpage" /></strong>
			<input type="submit" name="hiddenSelectSubmit" class="hid" onclick=" $j('#${pageNoIdVar}').attr('value', $j(this).next().val()); " />
			<select onchange=" $j(this).prev().trigger('click'); ">
				<c:set var="beginPage" value="${rs.currentPage - 10 > 1 ? rs.currentPage - 10 : 1}" />
				<c:set var="endPage" value="${rs.pageCount - rs.currentPage > 10 ? rs.currentPage + 10 : rs.pageCount}" />
				<c:forEach var="i" begin="${beginPage}" end="${endPage}">
					<c:choose>
						<c:when test="${i == rs.currentPage}">
							<option value="${i}" selected="selected">${i}</option>
						</c:when>
						<c:otherwise>
							<option value="${i}">${i}</option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select>
			<strong>											
				<spring:message code="pagination.of"/><c:out value=" ${rs.pageCount} " /><c:choose><c:when test="${rs.pageCount > 1}"><spring:message code="pagination.pages"/></c:when><c:otherwise><spring:message code="pagination.page"/></c:otherwise></c:choose><spring:message code="pagination.withnbresults" arguments="${rs.resultsCount}"/>
			</strong>
			<!-- clear floats and restore page flow -->
			<div class="clear"></div>
		</c:otherwise>
	</c:choose>
</div>
<!-- End pagination section -->