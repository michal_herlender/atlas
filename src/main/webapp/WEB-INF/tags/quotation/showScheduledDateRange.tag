<%@ tag language="java" display-name="showScheduledDateRange" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ attribute name="dateRange" required="true" type="java.lang.Object" %>

<c:if test="${not empty dateRange}">
	<span>
		<c:choose>
			<c:when test="${not empty dateRange.startDate and not empty dateRange.finishDate}">
				<fmt:formatDate pattern="dd.MM.yyyy" value="${dateRange.startDate}" />
				<spring:message code="to" />
				<fmt:formatDate pattern="dd.MM.yyyy" value="${dateRange.finishDate}" />
			</c:when>
			<c:when test="${not empty dateRange.startDate}">
				&ge;<fmt:formatDate pattern="dd.MM.yyyy" value="${dateRange.startDate}" />
			</c:when>
			<c:when test="${not empty dateRange.finishDate}">
				&le;<fmt:formatDate pattern="dd.MM.yyyy" value="${dateRange.finishDate}" />
			</c:when>
			<c:otherwise>
				<spring:message code="schedule.alldates" />
			</c:otherwise>
		</c:choose>
	</span>
	<span>
		<c:choose>
			<c:when test="${dateRange.includeEmptyDates}">
				<spring:message code="schedquotrequ.includeblankdates" />
			</c:when>
			<c:otherwise>
				<spring:message code="schedquotrequ.excludeblankdates" />
			</c:otherwise>
		</c:choose>
	</span>
</c:if>