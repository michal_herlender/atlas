<%--
	Specialized tag for use on quotation items (and documents) that shows customer description when populated
	Converted from velocity macro #showInstrument which was also only used on quotations
	GB 2017-11-13
--%>
<%@ tag language="java" display-name="showInstrument" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%@ attribute name="instrument" required="true" type="java.lang.Object" %>

<cwms:besttranslation translations="${instrument.model.nameTranslations}"/>
<c:if test="${not empty instrument.customerDescription}">
	<c:out value=" / ${instrument.customerDescription}" />
</c:if>