<%@ tag language="java" display-name="showCosts" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="costs" tagdir="/WEB-INF/tags/costs" %>

<%@ attribute name="pricingitem" required="true" type="java.lang.Object" %>
<%@ attribute name="currency" required="true" type="java.lang.Object" %>
<%@ attribute name="cost" required="true" type="java.lang.Object" %>
<%@ attribute name="entityId" required="true" type="java.lang.Object" %>
<%@ attribute name="defaultCurrency" required="true" type="java.lang.Object" %>

<%--
* Displays an editable set of fields for editing a single cost type for the pricing item.
* @param $pricingitem the pricing item to display
* @param $currency the currency of this entity the pricing item belongs to
* @param $cost the cost
* @param $entityId the id of the entity this pricingitem belongs to (i.e. the quotation id)
* @param $defaultCurrency the users default currency
--%>

<c:set var="name" value="${cost.costType.name.toLowerCase()}"/>
<c:set var="costid" value="${cost.costType.typeid}"/>
<c:set var="costname" value="${cost.costType.name}"/>

<script type="module"
        src="${pageContext.request.contextPath}/script/components/cwms-warning-onchange/cwms-warning-onchange.js"></script>
<fieldset id="housecostscal">
    <legend><spring:message code="vm_costs.inhousecharges"/> [<spring:message code="${cost.costType.messageCode}"/>]
    </legend>
    <ol>
        <li>
            <label><spring:message code="vm_costs.houseprice"/> [<spring:message
                    code="${cost.costType.messageCode}"/>]:</label>
            <div class="symbolbox">${currency.currencyERSymbol}</div>
            <form:input id="cost${name}" path="quotationitem.${name}Cost.houseCost" type="number" step="0.01"
                        placeholder="0.00" min="0" class="right" onclick="updateCostSource('${name}');"/>
            <span class="attention">${status.errorMessage}</span>
            <cwms-warning-onchange></cwms-warning-onchange>
        </li>
			<c:if test="${name == 'calibration'}">
				<li>
					<label><spring:message code="costsource" /> :</label>
					<div>
						<div><img src="img/icons/search_costs.png" class="img_marg_bot" alt="Cost Source" width="20" height="16">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<span id ="spanSource${name}">${pricingitem.calibrationCost.costSrc}</span>
						<form:hidden id="costSource${name}" path="quotationitem.calibrationCost.costSrc" value="${pricingitem.calibrationCost.costSrc}" />
						(<a href="#" onclick="switchSourceCost('${entityId}', '${pricingitem.inst.plantid}', '${pricingitem.model.modelid}', '${pricingitem.serviceType.serviceTypeId}', '${name}')">
						 <spring:message code="managecalconditions.switch"/></a>)</div>
					</div>
				</li>
			</c:if>
		</ol>
</fieldset>

<div class="clear"></div>

<c:set var="showTPCosts" value="false"/>
<c:if test="${cost.thirdCostTotal > 0}">
	<c:set var="showTPCosts" value="true"/>
</c:if>

<fieldset id="thirdcosts">

	<legend>
		<spring:message code="vm_costs.thirdpartycharges" />
		<c:if test="${showTPCosts == false}">
			(<a href="#" class="mainlink" onclick=" $j(this).parent().next().removeClass('hid').addClass('vis'); return false; "><spring:message code="vm_costs.viewtpcosts" /></a>)
		</c:if>
	</legend>
		<ol class='<c:if test="${showTPCosts == false}">hid</c:if>'>
			<li>
				<label class="labelhead bold">${costname}</label>
				<span>&nbsp;</span>
			</li>
			<li>
				<label><spring:message code="vm_costs.thirdpartycostsource" arguments="${costname}" />:</label>
				<div class="symbolbox">&nbsp;</div>
					<form:select path="quotationitem.${name}Cost.thirdCostSrc" onchange=" toggleManualCost($j(this).parent().next(), $j(this).parent().next().next(), this.value); return false; ">
						<c:forEach var="tpSrc" items="${tpcostsrc}">
							<option value="${tpSrc}" ${cost.thirdCostSrc == tpSrc ? "selected" : ""}>${tpSrc.name}</option>
						</c:forEach>
					</form:select>
				<form:errors class="attention" path="quotationitem.${name}Cost.thirdCostSrc" />
			</li>
			<c:choose>
				<c:when test="${(cost.thirdCostSrc == 'MANUAL') || (cost.thirdManualPrice > 0) && (cost.thirdCostSrc != 'THIRD_PARTY_QUOTATION')}">
					<c:set var="displayClass" value="vis"/>
				</c:when>
				<c:otherwise>
					<c:set var="displayClass" value="hid"/>
				</c:otherwise>
			</c:choose>
			<li class="${displayClass}">
				<label><spring:message code="vm_costs.manualthirdpartycosts" arguments="${costname}" />:</label>
				<div class="symbolbox">${currency.currencyERSymbol}</div>
				<form:input path="quotationitem.${name}Cost.thirdManualPrice" class="right" />
				<form:errors class="attention" path="quotationitem.${name}Cost.thirdManualPrice" />
			</li>
			<c:choose>
				<c:when test="${(cost.thirdCostSrc == 'THIRD_PARTY_QUOTATION') || (not empty cost.linkedCostSrc.finalCost && (cost.linkedCostSrc.finalCost > 0)) && (cost.thirdCostSrc != 'MANUAL')}">
					<c:set var="displayClass" value="vis"/>
				</c:when>
				<c:otherwise>
					<c:set var="displayClass" value="hid"/>
				</c:otherwise>
			</c:choose>
			<li class="${displayClass}">
				<label><spring:message code="vm_costs.thirdpartyquotcosts" arguments="${costname}" />:</label>
				<div class="float-left" style=" padding-right: 10px; ">
					<div class="symbolbox">${currency.currencyERSymbol}</div>
					<c:set var="differentCurrencies" value="false"/>
					<c:choose>
						<c:when test="${not empty cost.linkedCostSrc.finalCost}">
							<c:set var="linkedCostFinalCost" value="${cost.linkedCostSrc.finalCost}"/>
						</c:when>
						<c:otherwise>
							<c:set var="linkedCostFinalCost" value="0.00"/>
						</c:otherwise>
					</c:choose>
					<c:if test="${cost.linkedCostSrc.tpQuoteItem.tpquotation.currency.currencyId != cost.quoteItem.quotation.currency.currencyId}">
						<c:set var="differentCurrencies" value="true"/>
						<c:choose>
							<c:when test="${costname == 'CALIBRATION' && not empty cost.calibrationFinalCostInConvertedCurrency}">
								<c:set var="linkedCostFinalCost" value="<fmt:formatNumber value='${cost.calibrationFinalCostInConvertedCurrency}' type='number' pattern='0.00' />"/>
							</c:when>
							<c:when test="${costname == 'PURCHASE' && not empty cost.purchaseFinalCostInConvertedCurrency}">
								<c:set var="linkedCostFinalCost" value="<fmt:formatNumber value='${cost.purchaseFinalCostInConvertedCurrency}' type='number' pattern='0.00' />"/>
							</c:when>
						</c:choose>
					</c:if>
					<input type="text" class="text-right" readonly="readonly" value="${linkedCostFinalCost}" />
				</div>
				<div id="linked${name}cost" class="float-left padtop" style=" line-height: 1.4; ">
					<span style=" line-height: 1.0; ">
						<c:choose>
							<c:when test="${not empty cost.linkedCostSrc}">
								<spring:message code="vm_costs.tpquotation"/> <a href="viewtpquote.htm?id=${cost.linkedCostSrc.tpQuoteItem.tpquotation.id}" class="mainlink">${cost.linkedCostSrc.tpQuoteItem.tpquotation.qno}</a>
								<spring:message code="item" /> ${cost.linkedCostSrc.tpQuoteItem.itemno}<br />
								<c:if test="${differentCurrencies}">
									<b>(<spring:message code="vm_costs.convertedcurrency" /> ${cost.linkedCostSrc.tpQuoteItem.tpquotation.currency.currencyERSymbol}${cost.linkedCostSrc.finalCost})</b><br/>
								</c:if>
								${cost.linkedCostSrc.tpQuoteItem.tpquotation.contact.sub.comp.coname} -
								<a href="#" onclick=" removeTPCost('${costname}'); return false; " class="mainlink"><spring:message code="remove" /></a><br />
							</c:when>
							<c:otherwise>
								<spring:message code="vm_costs.notpcosts" /> -
							</c:otherwise>
						</c:choose>
					</span>
					<c:choose>
						<c:when test="${not empty pricingitem.inst}">
							<a href="#" onclick=" displayTPCostSearch(${pricingitem.id}, '${costname}', ${pricingitem.inst.model.modelid}); return false; " class="mainlink">
								<spring:message code="vm_costs.findtpcosts"/>
							</a>
						</c:when>
						<c:otherwise>
							<a href="#" onclick=" displayTPCostSearch(${pricingitem.id}, '${costname}', ${pricingitem.model.modelid}); return false; " class="mainlink">
								<spring:message code="vm_costs.findtpcosts"/>
							</a>
						</c:otherwise>
					</c:choose>
					<c:set var="cid" value=""/>
					<c:if test="${not empty cost.linkedCostSrc.tpQuoteItem}">
						<c:forEach var="c" items="${cost.linkedCostSrc.tpQuoteItem.costs}">
							<c:if test="${costid == c.costType.typeid}">
								<c:set var="cid" value="${c.costid}"/>
							</c:if>
						</c:forEach>
					</c:if>
					<input type="hidden" name="${name}CostId" value="${cid}" />
					<c:if test="${not empty status.errorMessage}">
						<span class="attention" style=" line-height: 1.0; ">${status.errorMessage}</span>
					</c:if>
				</div>
				<!-- clear floats and restore page flow -->
				<div class="clear-0"></div>					
			</li>
			<li>
				<label><spring:message code="vm_costs.thirdpartymarkupratesource" arguments="${costname}" />:</label>
				<div class="symbolbox">&nbsp;</div>
					<form:select path="quotationitem.${name}Cost.thirdMarkupSrc">
						<c:forEach var="tpSrc" items="${tpcostmarkupsrc}">
							<option value="${tpSrc}" ${cost.thirdMarkupSrc == tpSrc ? "selected" : ""}>${tpSrc.name}</option>
						</c:forEach>
					</form:select>
					<form:errors class="attention" path="quotationitem.${name}Cost.thirdMarkupSrc" />
			</li>
			<li>
				<label><spring:message code="vm_costs.thirdpartymarkuprate" arguments="${costname}" />:</label>
				<div class="symbolbox">&nbsp;</div><form:input path="quotationitem.${name}Cost.thirdMarkupRate" class="right"/> %
				<span class="attention">${status.errorMessage}</span>
			</li>
			<li>
				<label class="labelhead bold"><spring:message code="vm_costs.carriage" /></label>
				<span>&nbsp;</span>
			</li>
			<li>
				<label><spring:message code="vm_costs.tpcarriagemarkupratesource" />:</label>
				<div class="symbolbox">&nbsp;</div>
					<form:select path="quotationitem.${name}Cost.tpCarriageMarkupSrc" onchange="if(this.value == 'MANUAL'){ $j('#carMarkRate').css({display:'block'})} else {$j('#carMarkRate').css({display:'none'})} return false;">
						<c:forEach var="tpSrc" items="${tpcostmarkupsrc}">
							<option value="${tpSrc}" ${cost.tpCarriageMarkupSrc == tpSrc ? "selected" : ""}>${tpSrc.name}</option>
						</c:forEach>
					</form:select>
				<span class="attention">${status.errorMessage}</span>
			</li>
			<!-- Only display this if using a manual markup source -->
			<c:set var="displayCarMark" value="display"/>
			<c:if test="${cost.tpCarriageMarkupSrc != 'MANUAL'}">
				<c:set var="displayCarMark" value="none"/>
			</c:if>
			<li id="carMarkRate" style="display: ${displayCarMark};">	
				<label><spring:message code="vm_costs.carriagemarkuprate" /></label>
				<div class="symbolbox">&nbsp;</div>
				<form:input path="quotationitem.${name}Cost.tpCarriageMarkupRate" class="right" /> 
				<span class="attention">${status.errorMessage}</span>
			</li>
			<li>	
				<label><spring:message code="tpcosts.carriageout" /></label>
				<div class="symbolbox">${currency.currencyERSymbol}</div>
				<form:input path="quotationitem.${name}Cost.tpCarriageOut" class="right"/>
				<span class="attention">${status.errorMessage}</span>
			</li>
			<li>	
				<label><spring:message code="vm_costs.carriageoutmarkedup" />:</label>
				<div class="symbolbox">${currency.currencyERSymbol}</div>
				<div class="valuebox">
					<span class="valuetext">
						${cost.tpCarriageOutMarkupValue}
					</span>
		
				</div>
				<div class="clear"></div>
			</li>
			<li>
				<label><spring:message code="tpcosts.carriagein" /></label>
				<div class="symbolbox">${currency.currencyERSymbol}</div>
				<form:input path="quotationitem.${name}Cost.tpCarriageIn" class="right"/>
			<span class="attention">${status.errorMessage}</span>
			</li>
			<li>
				<label><spring:message code="vm_costs.carriageinmarkedup" />:</label>
				<div class="symbolbox">${currency.currencyERSymbol}</div>
				<div class="valuebox">
					<span class="valuetext">
						${cost.tpCarriageInMarkupValue}
					</span>
				
				</div>
				<div class="clear"></div>									
			</li>
			<li>
				<label><spring:message code="vm_costs.carriagetotal"/>:</label>
				<div class="symbolbox">${currency.currencyERSymbol}</div>
				<div class="valuebox">
					<span class="valuetext">
						${cost.tpCarriageTotal}
					</span>
				
				</div>
				<div class="clear"></div>
			</li>
			<li>
				<label><strong><spring:message code="vm_costs.thirdpartytotal" arguments="${costname}" />:</strong></label>
				<div class="symbolbox">${currency.currencyERSymbol}</div>
				<div class="valuebox">
					<span class="valuetext">
					${cost.thirdCostTotal}
				</span>
				</div>
				<div class="clear"></div>
			</li>
		</ol>
</fieldset>
<div class="clear"></div>
<fieldset id="totalcosts">
	<legend><spring:message code="editquot.totalcharges"/></legend>
		<ol>
			<li>
				<label><spring:message code="tpcosts.beforediscount"/>:</label>
				<div class="symbolbox">${currency.currencyERSymbol}</div>
				<div class="valuebox" >
					<span class="valuetext" style="width: 200px; left: 0px;" >
						<fmt:formatNumber value="${cost.totalCost}" minFractionDigits="2" maxFractionDigits="2"/>
					</span>
				</div>
				<div class="clear"></div>
			</li>
			<li>
				<label><spring:message code="discount" />:</label>
				<div class="symbolbox">%</div>
				<form:input path="quotationitem.${name}Cost.discountRate" type="number" step="0.01" class="right" />
					<c:if test="${not empty cost.discountValue}">
						[<fmt:formatNumber type="currency" value="${cost.discountValue}" currencyCode="${currency.currencyCode}"/>]
					</c:if>
				<span class="attention">${status.errorMessage}</span>
			</li>
			<li>
				<label><spring:message code="total" />:</label>
				<div class="symbolbox">${currency.currencyERSymbol}</div>
				<div class="valuebox">
					<span class="valuetext" style="width: 200px; left: 0px;">
						<fmt:formatNumber value="${cost.finalCost}" minFractionDigits="2" maxFractionDigits="2"/>
					</span>
				</div>
				<div class="clear"></div>
			</li>
		</ol>
</fieldset>