
<%@ tag language="java" display-name="showInstrumentProjection" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%@ attribute name="instrument" required="true" type="java.lang.Object" %>
<%@ attribute name="customerDescription" required="false" type="java.lang.Object" %>

${instrument.model}
<c:if test="${not empty instrument.customerDescription}">
	<c:out value=" / ${instrument.customerDescription}" />
</c:if>