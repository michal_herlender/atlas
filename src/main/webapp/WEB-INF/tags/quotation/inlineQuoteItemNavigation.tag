<%@ tag language="java" display-name="inlineQuoteItemNavigation" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="quotationitem" required="true" type="java.lang.Object" %>
<%@ attribute name="nearbyItems" required="true" type="java.lang.Object" %>

<div class="text-center">
	<c:set var="baseUrl" value="editquotationitem.htm?" />
	<span id="prevItem">									
		<c:set var="prevItemId" value="0"/>
		<c:forEach var="qItem" items="${nearbyItems}">
			<c:if test="${not qItem.partOfBaseUnit}">
				<c:choose>
					<c:when test="${qItem.id == quotationitem.id}">
						<input type="button" value="<spring:message code='editquot.prev'/>" onclick=" window.location.href = '${baseUrl}id=${prevItemId}'; " <c:if test="${prevItemId == 0}"> disabled="disabled" </c:if> />
					</c:when>
					<c:otherwise>
						<c:set var="prevItemId" value="${qItem.id}"/>
					</c:otherwise>
				</c:choose>
				
				<c:forEach var="modItem" items="${qItem.modules}">
					<c:choose>
						<c:when test="${modItem.id == quotationitem.id}">
							<input type="button" value="<spring:message code='editquot.prev'/>" onclick=" window.location.href = '${baseUrl}id=${prevItemId}'; " <c:if test="${prevItemId == 0}"> disabled="disabled" </c:if> />
						</c:when>
						<c:otherwise>
							<c:set var="prevItemId" value="${modItem.id}"/>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</c:if>
		</c:forEach>
	</span>
	<span id="itemnavlist">
		<select onchange="if(this.value != ''){window.location='editquotationitem.htm?'+'id='+this.value;}">
			<c:forEach var="qItem" items="${nearbyItems}">
				<c:if test="${qItem.partOfBaseUnit == false}">
					<option value="${qItem.id}" ${qItem.id == quotationitem.id ? "selected" : ""}>
						${qItem.itemno} - 
						<c:choose>
							<c:when test="${not empty qItem.inst}"><cwms:showmodel instrumentmodel="${qItem.inst.model}"/></c:when>
							<c:otherwise><cwms:showmodel instrumentmodel="${qItem.model}"/></c:otherwise>
						</c:choose>
					</option>
					<c:forEach var="modItem" items="${qItem.modules}">
						<option value="${modItem.id}" ${modItem.id == quotationitem.id ? "selected" : ""}>
							&nbsp;&nbsp;&nbsp;&nbsp; - 
							<c:choose>
								<c:when test="${not empty qItem.inst}"><cwms:showmodel instrumentmodel="${qItem.inst.model}"/></c:when>
								<c:otherwise><cwms:showmodel instrumentmodel="${qItem.model}"/></c:otherwise>
							</c:choose>
						</option>
					</c:forEach>
				</c:if>
			</c:forEach>
		</select>
	</span>
	<span id="nextItem">
		<c:set var="nextDisplayed" value="false"/>							
		<c:set var="nextItemId" value="0"/>
		<c:forEach var="qItem" items="${nearbyItems}">
			<c:if test="${qItem.partOfBaseUnit == false}">
				
				<%-- this iteration contains the item we want to view --%> 
				<c:if test="${nextItemId == quotationitem.id}">																
					<input type="button" value="<spring:message code='editquot.next'/>" onclick=" window.location.href = '${baseUrl}id=${qItem.id}'; " />																
					<c:set var="nextDisplayed" value="true"/>
				</c:if>
				
				<%-- we're now on the current item, so the next iteration should contain the item we want to link to --%>
				<c:set var="nextItemId" value="${qItem.id}"/>
				
				<c:forEach var="modItem" items="${qItem.modules}">
					
					<%-- this iteration contains the item we want to view --%>
					<c:if test="${nextItemId == quotationitem.id}">																	
						<input type="button" value="<spring:message code='editquot.next'/>" onclick=" window.location.href = '${baseUrl}id=${modItem.id}'; " />
						<c:set var="nextDisplayed" value="true"/>
					</c:if>
					
					<c:set var="nextItemId" value="${modItem.id}"/> 
					
				</c:forEach>
			</c:if>
		</c:forEach>
		<c:if test="${nextDisplayed == false}">
			<input type="button" value="<spring:message code='editquot.next'/>" disabled="disabled" />
		</c:if>
	</span>
	
</div>