<%@ tag language="java" display-name="showTPSupportedCosts velocityorder: pricingitem currency entityId defaultCurrency" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="quotation" tagdir="/WEB-INF/tags/quotation" %>

<%@ attribute name="pricingitem" required="true" type="java.lang.Object" %>
<%@ attribute name="currency" required="true" type="java.lang.Object" %>
<%@ attribute name="entityId" required="true" type="java.lang.Object" %>
<%@ attribute name="defaultCurrency" required="true" type="java.lang.Object" %>

<%--
Assumes usage within a bound form:form tag with path of root bean specified (e.g. command)

#macro(showTPSupportedCosts $pricingitem $currency $entityId $defaultCurrency)
* Loops through a price items's costs and displays them
* @param $pricingitem the pricing item to display
* @param $currency the currency of this entity the pricing item belongs to
* @param $entityId the id of the entity this pricingitem belongs to (i.e. the quotation id)
* @param $defaultCurrency the users default currency
 --%>

<c:forEach var="cost" items="${pricingitem.costs}">
	<c:set var="costname" value="${cost.costType.name}"/>
	<c:set var="name" value="${costname.toLowerCase()}"/>
	<c:set var="costid" value="${cost.costType.typeid}"/>
	<c:set var="includeCosts" value="${cost.active}"/>
	<div id="${costname}Costs" class="infobox">
		<h5>
			<spring:message code="vm_costs.peritemcharge" arguments="${costname}"/>
			<c:set var="divdisplay" value="none"/>
			<c:choose>
				<c:when test="${includeCosts}">
					- <a href="#" class="mainlink" onclick="toggleIncludeCost('${costname}', this, 'quotationitem\\.${name}Cost\\.active'); return false;"><spring:message code="remove" /></a>
					<c:set var="divdisplay" value="block"/>
				</c:when>
				<c:otherwise>
					<spring:message code="vm_costs.nocostset" arguments="${name}"/> - <a href="#" class="mainlink" onclick="toggleIncludeCost('${costname}', this, 'quotationitem\\.${name}Cost\\.active'); return false;"><spring:message code="add" /></a>)
				</c:otherwise>
			</c:choose>
			<form:hidden path="quotationitem.${name}Cost.active"/>
		</h5>
		<div id="${costname}Main" style="display: ${divdisplay};">
			<c:choose>
				<c:when test="${costname == 'CALIBRATION'}">
					<quotation:showCosts pricingitem="${pricingitem}" cost="${cost}" currency="${currency}" entityId="${entityId}" defaultCurrency="${defaultCurrency}" />
				</c:when>
				<c:when test="${costname == 'REPAIR'}">
					<quotation:showRepairCosts />
				</c:when>
				<c:when test="${costname == 'ADJUSTMENT'}">
					<quotation:showAdjustmentCosts />
				</c:when>
				<c:when test="${costname == 'PURCHASE'}">
					<quotation:showCosts pricingitem="${pricingitem}" cost="${cost}" currency="${currency}" entityId="${entityId}" defaultCurrency="${defaultCurrency}" />
				</c:when>
			</c:choose>
		</div>
	</div>
</c:forEach>