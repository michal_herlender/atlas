<%@ tag language="java" display-name="showRelatedContractInstructions" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image"%>

<%@ attribute name="job" required="true" type="java.lang.Object" %>
<%@ attribute name="highlightTypes" required="true" type="java.lang.Object" %>

<c:set var="highlightList" value='${highlightTypes.split(",")}'/>
<!-- nifty corners div -->
<div class="infobox">
	<h5><spring:message code="viewjob.jobcontractinstr"/></h5>
	<!-- clears floats and restores page flow -->
	<div class="clear"></div>
	<table class="default2" >
		<thead>
			<tr>
				<th><spring:message code="viewjob.contractinstrs.contractnumber"/></th>
				<th><spring:message code="items"/></th>
				<th><spring:message code="instructions"/></th>
			</tr>
		</thead>
		<tbody>
			<c:if test="${ empty job.contracts and job.contracts.size() == 0 }">
				<tr>
					<td colspan="3" class="center"><spring:message code="viewjob.noitems"/></td>
				</tr>
			</c:if>
			<c:forEach items="${ job.contracts }" var="contract">
				<tr>
					<td>${ contract.contractNumber }</td>
					<td class="center">
						${ job.getItemsCountByContract(contract) }
						<a href="" onclick="event.preventDefault(); displayContractInstructions(${contract.id}); return false;">
								<img id="contractInstruction_show_${ contract.id }" src="img/icons/items.png" width="16" height="16"
									alt=""
									title=""
									class="image_inline"/>
								<img id="contractInstruction_hid_${ contract.id }" src="img/icons/items_hide.png" width="16" height="16"
									alt=""
									title=""
									class="image_inline hid"/>
						</a>
					</td>
					<td class="center">${ empty contract.instructions ? 0:contract.instructions.size() }</td>
				</tr>
				<tr class="hid" id="contractInstruction_${ contract.id }">
				<td colspan="3">
				<fieldset>
<!-- 							<ol id="instructionList"> -->
<%-- 			<c:choose> --%>
<%-- 				<c:when test="${contract.instructions.size() < 1}"> --%>
<%-- 					<li><spring:message code="company.therearecurrentlynoinstructionstodisplay"/></li> --%>
<%-- 				</c:when> --%>
<%-- 				<c:otherwise> --%>
<%-- 					<c:forEach var="instruction" items="${contract.instructions}"> --%>
<%-- 						<c:set var="bgColor" value=""/> --%>
<%-- 						<c:forEach var="h" items="${highlightList}"> --%>
<%-- 							<c:if test="${h == instruction.instructiontype.type}"> --%>
<%-- 								<c:set var="bgColor" value="background-color: yellow;"/> --%>
<%-- 							</c:if> --%>
<%-- 						</c:forEach> --%>
<%-- 						<li id="instruction${instruction.instructionid}" style="${bgColor}"> --%>
<!-- 							<div class="notes_note instructions_note"> -->
<%-- 								<strong>${instruction.instructiontype.type}:</strong><br /> --%>
<%-- 								<c:choose> --%>
<%-- 									<c:when test='${datetools.isDateAfterXTimescale(-1, "m", instruction.lastModified)}'> --%>
<%-- 										<span class="red">${instruction.instruction}</span> --%>
<%-- 									</c:when> --%>
<%-- 									<c:when test='${datetools.isDateAfterXTimescale(-3, "m", instruction.lastModified)}'> --%>
<%-- 										<span class="amber">${instruction.instruction}</span> --%>
<%-- 									</c:when> --%>
<%-- 									<c:otherwise> --%>
<%-- 										<span class="brown">${instruction.instruction}</span> --%>
<%-- 									</c:otherwise> --%>
<%-- 								</c:choose> --%>
<!-- 							</div> -->
<%-- 							<div class="notes_editor"><t:showLastModified versionedEntity="${instruction}"/></div> --%>
<!-- 							<div class="notes_edit instructions_edit"> -->
<%-- 								<a href="editinstruction.htm?linkid=${instruction.instructionid}" class="imagelink"> --%>
<%-- 									<img src="img/icons/note_edit.png" width="16" height="16" alt="<spring:message code='company.editinstruction'/>" title="<spring:message code='company.editinstruction'/>" /> --%>
<!-- 								</a> -->
<%-- 								<c:if test="${i.instructionEntity.name() == 'JOB'}"> --%>
<%-- 									<a href="deleteinstruction.htm?linkid=${i.id}&entity=${i.instructionEntity}" class="imagelink"> --%>
<%-- 										<img src="img/icons/note_delete.png" width="16" height="16" alt="<spring:message code='company.deleteinstruction'/>" title="<spring:message code='company.deleteinstruction'/>" /> --%>
<!-- 									</a> -->
<%-- 								</c:if> --%>
<!-- 							</div> -->
<!-- 							<div class="clear-0"></div> -->
<!-- 						</li> -->
<%-- 					</c:forEach> --%>
<%-- 				</c:otherwise> --%>
<%-- 			</c:choose> --%>
<!-- 		</ol> -->
		<cwms-instructions link-contractid="${ contract.id }" show-mode="BASIC"></cwms-instructions>	
		</fieldset>	
		</td>			
				</tr>
			</c:forEach>
		</tbody>
	</table>

</div>