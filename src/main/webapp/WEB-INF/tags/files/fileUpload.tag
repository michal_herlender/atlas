<%@ tag language="java" display-name="fileUpload" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="systemComponentId" required="true" type="java.lang.Integer" %>
<%@ attribute name="thisEntityId1" required="true" type="java.lang.String" %>
<%@ attribute name="thisEntityId2" required="true" type="java.lang.Integer" %>
<%@ attribute name="rootFiles" required="true" type="java.lang.Object" %>
<%@ attribute name="count" required="true" type="java.lang.Integer" %>
<%@ attribute name="renameTo" required="true" type="java.lang.String" %>
<%@ attribute name="web" required="false" type="java.lang.Boolean" %>

<%--
* Macro that displays a file upload dialog with progress monitor for uploads.
* This should be extended to show an alert if moving away from the page whilst an upload is ongoing.
* 
* @param $systemComponentId the id of the system component that the file is being uploaded to.
* @param $thisEntityId1 the id of the actual system component entity the file is being uploaded to (i.e. jobno, qno etc)
* @param $thisEntityId2 the second part of the id of the actual system component entity the file is being uploaded to (i.e. version etc)
* @param $rootFiles
* @param $count integer value passed for id to make it distinct when more than one file upload plugin is required
* @param $renameTo 
Parameters in order of old velocity macros are as follows:   
<files:fileUpload systemComponentId="" thisEntityId1="" thisEntityId2="" rootFiles="" count="" renameTo="" />
--%>
	
<%-- force import of all required scripts --%>
<script type='text/javascript'>
	
	// load scripts needed for uploading files
	var fileoffset = "";
	if(${web == true}) {fileoffset = "../";}
	loadScript.loadScriptsDynamic( new Array (	fileoffset + 'script/thirdparty/fileupload/upload.js',
										'dwr/interface/UploadMonitor.js') );

</script>
	
<%-- IFRAME used as target to stop page redirect/refresh after submitting --%>
<iframe id="target_upload" name="target_upload" src="" class="hid"></iframe> 

<form:form method="post" class="fileuploadform" action="upload.htm" enctype="multipart/form-data" onsubmit=" startProgress(${count}); " target="target_upload" >    
	
	<c:set var="showSubFolders" value="false"/>
	<c:if test="${rootFiles != null}">
		<c:forEach var="fw" items="${rootFiles.getFiles()}">
			<c:if test="${fw.directory}">
				<c:set var="showSubFolders" value="true"/>
			</c:if>
		</c:forEach>
	</c:if>
	
	<c:set var="margclass" value="hid"/>
	<c:if test="${showSubFolders}">
		<c:set var="margclass" value="vis"/>
	</c:if>
	
	<div class="marg ${margclass}">
		Root Folder <input type="radio" name="directory" value="" onclick=" $j('input[name=\'subdirectory\']').val(this.value); " checked="checked" />
		<c:forEach var="fw" items="${rootFiles.getFiles()}">
			<c:if test='${fw.directory && fw.fileName != "Certificates"}'>
				${fw.fileName}
				<input type="radio" name="directory" value="${fw.fileName}" onclick=" $j('input[name=\'subdirectory\']').val(this.value); " />
			</c:if>
		</c:forEach>
	</div>
	<div> 

        <input type="file" name="file" size="36" value="${status.value}" id="file1${count}" />
        
        <input type="hidden" name="compid" value="${systemComponentId}"/>
        
        <input type="hidden" name="compEntityId" value="${thisEntityId1}"/>
        
        <input type="hidden" name="compEntityVersion" value="${thisEntityId2}"/>
        
        <input type="hidden" name="subdirectory" value=""/>
        
        <input type="hidden" name="renameTo" value="${renameTo}"/>
        
        <input type="submit" value="Upload file" id="uploadbutton${count}"/>

        <c:if test="${!web}">
			<input type="button" value="Cancel" onclick=" tb_remove(); "/>
        </c:if>
		
		<!-- upload progress bar -->
		<div id="progressBar${count}" class="hid">
			<!-- this span displays the upload progress text -->
        	<span></span>
			<!-- this div is the progress bar container -->
            <div>
				<!-- background of this div is updated to show upload progress -->
            	<div></div>
            </div>
        </div>
   </div>    
</form:form>