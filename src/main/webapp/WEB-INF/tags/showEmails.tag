<%@ tag language="java" display-name="showEmails" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ attribute name="entity" required="true" type="java.lang.Object" %>
<%@ attribute name="entityId" required="true" type="java.lang.Object" %>
<%@ attribute name="entityName" required="true" type="java.lang.Object" %>
<%@ attribute name="sc" required="true" type="java.lang.Object" %>
<%@ attribute name="coid" required="false" type="java.lang.Integer" %>

<%--
#*
* Displays an e-mail section for a given system component, with a table of previously sent e-mails
*	and a link to send a new e-mail.
* @param $entity the (component) entity (e.g. the Job, Quotation object, etc).
* @param $entityId the ID of the entity referenced above.
* @param $entityName the name of the type of entity referenced (e.g. "Job", "Quotation", etc).
* @param $sc the SystemComponent object related to the entity type.
*#
Parameters in order of old velocity macros are as follows:
 (showEmails $entity $entityId $entityName $sc)
--%>

<script type='text/javascript' src='script/trescal/core/system/ViewEmailDetails.js'></script>
	
			
<div id="entityEmailDisplay">
	<c:if test="${entity.sentEmails.size() > 0}">
		<a href="searchemail.htm" class="mainlink-float"><spring:message code="showemails.searchallemails" /></a>
		<div class="clear"></div>
	</c:if>
	
	<table class="default2" summary="This table displays all sent emails">
		<thead>
			<tr>
				<td colspan="7">
					<spring:message code="showemails.sentemails" /> (<span class="entityEmailDisplaySize">${entity.sentEmails.size()}</span>)&nbsp;
					<a href="#" onclick=" loadScript.createOverlay('external', '<spring:message code="showemails.createemail" />', 'createemail.htm?syscompid=${sc.componentId}&objid=${entityId}&coid=${coid}', null, null, 85, 860, null); return false; " title="<spring:message code="showemails.newemail" />">
						<img src="img/icons/email_add.png" width="16" height="16" alt="<spring:message code="showemails.newemail" />" title="<spring:message code="showemails.newemail" />" class="img_marg_bot" />
					</a>
				</td>
               </tr>
			<tr>
				<th class="tofield" scope="col"><spring:message code="system.to" /></th>
				<th class="ccfield" scope="col"><spring:message code="system.cc" /></th>
				<th class="subject" scope="col"><spring:message code="system.subject" /></th>
				<th class="senton" scope="col"><spring:message code="system.senton" /></th>
				<th class="sentby" scope="col"><spring:message code="system.sentby" /></th>
				<th class="view" scope="col"><spring:message code="system.view" /></th>
				<th class="resend" scope="col"><spring:message code="system.resend" /></th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="7">
					<a href="#" onclick=" loadScript.createOverlay('external', '<spring:message code="showemails.createemail" />', 'createemail.htm?syscompid=${sc.componentId}&objid=${entityId}&coid=${coid}', null, null, 85, 860, null); return false; " class="float-right padright20">
						<img src="img/icons/email_add.png" width="16" height="16" alt="<spring:message code="showemails.newemail" />" title="<spring:message code="showemails.newemail" />" class="img_marg_bot" />
					</a>
				</td>
			</tr>
		</tfoot>
		<tbody>
			<c:choose>
				<c:when test="${entity.sentEmails.size() < 1}">
					<tr>
						<td colspan="7" class="bold center"><spring:message code="showemails.noemailshavebeensent" /></td>
					</tr>
				</c:when>
				<c:otherwise>					
					<c:forEach var="e" items="${entity.sentEmails}">
						<tr id="email$e.id">
							<td class="tofield">
								<c:forEach var="r" items="${e.recipients}">
									<c:if test="${r.type == 'EMAIL_TO'}">
										${r.emailAddress}<br/>
									</c:if>
								</c:forEach>
							</td>
							<td class="ccfield">
								<c:forEach var="r" items="${e.recipients}">
									<c:if test="${r.type == 'EMAIL_CC'}">
										${r.emailAddress}<br/>
									</c:if>
								</c:forEach>
							</td>
							<td class="subject">
								${e.subject}
							</td>
							<td class="senton">
								<fmt:formatDate value="${e.sentOn}" dateStyle="medium" type="both" timeStyle="short" />
							</td>
							<td class="sentby">
								${e.sentBy.name}
							</td>
							<td class="view">
								<a href="#" id="email_anchor${e.id}" onclick=" getAuthentication(${currentContact.personid}, ${e.sentBy.personid}, ${e.publish}, ${e.id}, this.id); return false; ">
									<img src="img/icons/email_open.png" width="16" height="16" alt="<spring:message code="showemails.viewemaildetails" />" title="<spring:message code="showemails.viewemaildetails" />" />
								</a>								
							</td>
							<td class="resend">
								<c:if test="${e.sentBy.name.trim() != 'CWMS Auto User'}">
									<a href="#" onclick=" loadScript.createOverlay('external', '<spring:message code="showemails.resendemail" />', 'createemail.htm?syscompid=${sc.componentId}&objid=${entityId}&emailid=${e.id}', null, null, 85, 860, null); return false; " class="float-right padright20">
										<img src="img/icons/email_go.png" width="16" height="16" alt="<spring:message code="showemails.resendemail" />" title="<spring:message code="showemails.resendemail" />" class="img_marg_bot" />
									</a>
								</c:if>
							</td>
						</tr>				
					</c:forEach>
				</c:otherwise>
			</c:choose>
		</tbody>
	</table>
</div>