<%@ tag language="java" display-name="fill" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ attribute name="field" required="true" type="java.lang.Object" %>

<c:choose>
	<c:when test="${field != null && field.trim().length() > 0}">
		${field}
	</c:when>
	<c:otherwise>
		&nbsp;
	</c:otherwise>
</c:choose>