<%@ tag language="java" display-name="showViewModelLink" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<%@ attribute name="modelnametranslation" required="true" type="java.lang.Object" %>
<%@ attribute name="instrumentmodelname" required="true" type="java.lang.Object" %>
<%@ attribute name="instrumentsubfamilytypology" required="true" type="java.lang.Object" %>
<%@ attribute name="instrumentmodelmfrtype" required="true" type="java.lang.Object" %>
<%@ attribute name="instrumentmfrname" required="true" type="java.lang.Object" %>
<%@ attribute name="instrumentgenericmfr" required="true" type="java.lang.Object" %>
<%@ attribute name="hidetypology" required="false" type="java.lang.Object" %>
 
<c:choose>
	<c:when test="${ empty instrumentmodelmfrtype }">
		null&nbsp;
	</c:when>
	<c:otherwise>
		<t:jobCostingViewShowModel  modelnametranslation="${modelnametranslation}" 
			instrumentsubfamilytypology="${instrumentsubfamilytypology}"/>
		<c:if test="${instrumentmodelmfrtype eq 'MFR_GENERIC'}">
			<c:if test="${not empty instrumentgenericmfr and !instrumentgenericmfr}">
				&nbsp;${instrumentmfrname}
			</c:if>
			<c:if test="${not empty instrumentmodelname}">
				&nbsp;${instrumentmodelname}
			</c:if>
		</c:if>
	</c:otherwise>
</c:choose>
