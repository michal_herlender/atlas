<%@ tag language="java" display-name="accountsApproval" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%@ attribute name="purchaseOrder" required="true" type="java.lang.Object" %>

<form:form action="" method="post" >
	<input type="hidden" name="action" value="accountsApproval" />
	<table class="default4">
		<thead>
			<tr>
				<td colspan="8"><spring:message code="purchaseorder.purchaseorderitems" /> (<span class="poItemSize">${order.items.size()}</span>)</td>
			</tr>
			<tr>
				<th class="goodsitem" scope="col"><spring:message code="item"/></th>
				<th class="goodsdesc" scope="col"><spring:message code="description"/></th>
				<th class="goodsstatus" scope="col"><spring:message code="purchaseorder.receiptstatus"/></th>
				<th class="goodsapproved" scope="col"><spring:message code="purchaseorder.goodsapproved"/></th>
				<th class="goodscancelled" scope="col"><spring:message code="purchaseorder.cancelled"/></th>
				<th class="accountsapproved" scope="col"><spring:message code="purchaseorder.accountsapproved"/></th>
				<th class="accountsinvoice" scope="col"><spring:message code="supplierinvoice.linkedtoinvoice"/></th>
				<th class="goodscomment" scope="col"><spring:message code="comment"/></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="item" items="${purchaseOrder.items}" varStatus="loopTagStatus">
				<tr>
					<td>${item.itemno}</td>
					<td>${item.description}</td>
					<td>${item.receiptStatus.message}</td>
					<td><spring:message code="${item.goodsApproved}"/></td>
					<td><spring:message code="${item.cancelled}"/></td>
					<td><form:checkbox path="accountsApproval[${loopTagStatus.index}]" /></td>
					<td>
						<c:choose>
							<c:when test="${not empty item.internalInvoice}">
								${internalInvoice.invno}
							</c:when>
							<c:otherwise>
								<form:select path="supplierInvoiceIds[${loopTagStatus.index}]" items="${supplierInvoices}" itemLabel="value" itemValue="key" />
							</c:otherwise>
						</c:choose>
					</td>
					<td><form:input path="goodsComment[${loopTagStatus.index}]" /></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<div>
		<input type="submit" name="update" value="<spring:message code='update' />">
	</div>		
</form:form>