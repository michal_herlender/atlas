<%@ tag language="java" display-name="accountsApprovalOnlyRead" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="purchaseOrder" required="true" type="java.lang.Object" %>

	<table class="default4">
		<thead>
			<tr>
				<td colspan="8"><spring:message code="purchaseorder.purchaseorderitems" /> (<span class="poItemSize">${order.items.size()}</span>)</td>
			</tr>
			<tr>
				<th class="goodsitem" scope="col"><spring:message code="item"/></th>
				<th class="goodsdesc" scope="col"><spring:message code="description"/></th>
				<th class="goodsstatus" scope="col"><spring:message code="purchaseorder.receiptstatus"/></th>
				<th class="goodsapproved" scope="col"><spring:message code="purchaseorder.goodsapproved"/></th>
				<th class="goodscancelled" scope="col"><spring:message code="purchaseorder.cancelled"/></th>
				<th class="accountsapproved" scope="col"><spring:message code="purchaseorder.accountsapproved"/></th>
				<th class="accountsinvoice" scope="col"><spring:message code="supplierinvoice.linkedtoinvoice"/></th>
				<th class="goodscomment" scope="col"><spring:message code="comment"/></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="item" items="${purchaseOrder.items}" varStatus="loopTagStatus">
				<tr>
					<td>${item.itemno}</td>
					<td>${item.description}</td>
					<td>${item.receiptStatus.message}</td>
					<td><spring:message code="${item.goodsApproved}"/></td>
					<td><spring:message code="${item.cancelled}"/></td>
					<td>
						<c:choose>
							<c:when test="${item.accountsApproved}">
								<spring:message code="yes" />
							</c:when>
							<c:otherwise>
								<spring:message code="no" />
							</c:otherwise>
						</c:choose>
					</td>
					<td>
						<c:choose>
							<c:when test="${not empty item.internalInvoice}">
								${internalInvoice.invno}
							</c:when>
							<c:otherwise>
								<c:forEach var="invoice" items="${supplierInvoices}">
									<c:if test="${invoice.key != 0 }">
										${invoice.value} &nbsp;
									</c:if>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</td>
					<td>
						<c:forEach var="action" items="${item.progressActions}">
							<c:if test="${not empty action.description}">
								${action.description} &nbsp;
							</c:if>
						</c:forEach>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>	