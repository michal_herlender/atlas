<%@ tag language="java" display-name="showSupplierInvoices" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%@ attribute name="purchaseOrder" required="true" type="java.lang.Object" %>
<table class="default4">
	<thead>
		<tr>
			<td colspan="8">
				<spring:message code="supplierinvoice.supplierinvoices"/>: ${purchaseOrder.supplierInvoices.size()}
			</td>
		</tr>
		<tr>
			<th class=""><spring:message code="supplierinvoice.invoicenumber"/></th>
			<th class=""><spring:message code="supplierinvoice.invoicedate"/></th>
			<th class=""><spring:message code="supplierinvoice.paymentdate"/></th>
			<th class=""><spring:message code="supplierinvoice.paymentterms"/></th>
            <th class=""><spring:message code="paymentmode"/></th>
            <th class="cost"><spring:message code="supplierinvoice.totalwithouttax"/></th>
			<th class="cost"><spring:message code="supplierinvoice.totalwithtax"/></th>
			<th class=""></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="8">
				<c:if test="${!purchaseOrder.status.onComplete}">
					<a href="addsupplierinvoice.htm?orderid=${purchaseOrder.id}" class="mainlink-float" title="<spring:message code="supplierinvoice.createsupplierinvoiceforpurchaseorder" />">
						<spring:message code="supplierinvoice.createsupplierinvoice" />
					</a>
				</c:if>
			</td>
		</tr>
	</tfoot>
	<tbody>
		<c:forEach var="supplierInvoice" items="${purchaseOrder.supplierInvoices}">
		<tr>
			<td class=""><c:out value="${supplierInvoice.invoiceNumber}"/></td>
			<td class=""><fmt:formatDate pattern="dd.MM.yyyy" value="${supplierInvoice.invoiceDate}"/></td>
			<td class="">
				<c:if test="${supplierInvoice.paymentDate != null}">
					<fmt:formatDate pattern="dd.MM.yyyy" value="${supplierInvoice.paymentDate}"/>
				</c:if>
			</td>
			<td class="">
				<c:if test="${supplierInvoice.paymentTerm != null}">
					${supplierInvoice.paymentTerm.message}
				</c:if>
			</td>
			<td class="">
				<c:if test="${supplierInvoice.paymentMode != null}">
					<cwms:besttranslation translations="${supplierInvoice.paymentMode.translations}"/>
				</c:if>
			</td>
			<td class="pocost cost">
				<c:if test="${supplierInvoice.totalWithoutTax != null}">
					${supplierInvoice.currency.currencyERSymbol}${supplierInvoice.totalWithoutTax}
				</c:if>
			</td>
			<td class="pocost cost">
				<c:if test="${supplierInvoice.totalWithTax != null}">
                    ${supplierInvoice.currency.currencyERSymbol}${supplierInvoice.totalWithTax}
				</c:if>
			</td>
			<td class="">
				<div>
					<c:if test="${!purchaseOrder.status.onComplete}">
						<p>
							<a href="editsupplierinvoice.htm?id=${supplierInvoice.id}" class="mainlink"><spring:message code="edit"/></a>
							|
							<a href="deletesupplierinvoice.htm?id=${supplierInvoice.id}" class="mainlink"><spring:message code="delete"/></a>
						</p>
					</c:if>
				</div>
			</td>
		</tr>
		</c:forEach>
	</tbody>
</table>