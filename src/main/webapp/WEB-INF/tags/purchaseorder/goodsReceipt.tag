<%@ tag language="java" display-name="goodsReceipt" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%@ attribute name="purchaseOrder" required="true" type="java.lang.Object" %>

<form:form action="" method="post" >
	<input type="hidden" name="action" value="goodsReceipt" />

	<table class="default4">
		<thead>
			<tr>
				<td colspan="6"><spring:message code="purchaseorder.purchaseorderitems" /> (<span class="poItemSize">${order.items.size()}</span>)</td>
			</tr>
			<tr>
				<th class="goodsitem" scope="col"><spring:message code="item"/></th>
				<th class="goodsdesc" scope="col"><spring:message code="description"/></th>
				<th class="goodsstatus" scope="col"><spring:message code="purchaseorder.receiptstatus"/></th>
				<th class="goodsapproved" scope="col"><spring:message code="purchaseorder.goodsapproved"/></th>
				<th class="goodscancelled" scope="col"><spring:message code="purchaseorder.cancelled"/></th>
				<th class="goodscomment" scope="col"><spring:message code="comment"/></th>
			</tr>
		</thead>
		<tbody>
			<c:set var="itemsAllCancelled" value="${not empty purchaseOrder.items}" />
			<c:forEach var="item" items="${purchaseOrder.items}" varStatus="loopTagStatus">
				<c:if test="${not command.goodsCancelled[loopTagStatus.index]}">
					<c:set var="itemsAllCancelled" value="false" />
				</c:if>
				<tr>
					<td>${item.itemno}</td>
					<td>${item.description}</td>
					<td><form:select path="goodsItemStatus[${loopTagStatus.index}]" items="${receiptStatuses}" itemLabel="message" itemValue="name" /></td>
					<td><form:checkbox path="goodsApproval[${loopTagStatus.index}]" /></td>
					<td><form:checkbox id="goodsCancelled_${loopTagStatus.index}" path="goodsCancelled[${loopTagStatus.index}]" onClick=" showHideCancelledWarning() " /></td>
					<td><form:input size="50" path="goodsComment[${loopTagStatus.index}]" /></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	
	<c:choose>
		<c:when test="${itemsAllCancelled}">
			<c:set var="warningVisible" value="vis" />
		</c:when>
		<c:otherwise>
			<c:set var="warningVisible" value="hid" />
		</c:otherwise>
	</c:choose>
	<div id="poCancelledWarning" class="warningBox1 ${warningVisible}">
		<div class="warningBox2">
			<div class="warningBox3">
				<div class="attention">
					<spring:message code="purchaseorder.cancelledwarning" />
				</div>
			</div>
		</div>
	</div>
	
	<div>
		<input type="submit" name="update" value="<spring:message code='update' />">
	</div>		
</form:form>