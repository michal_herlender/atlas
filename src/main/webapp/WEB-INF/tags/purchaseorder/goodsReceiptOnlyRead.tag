<%@ tag language="java" display-name="goodsReceiptOnlyRead" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="purchaseOrder" required="true" type="java.lang.Object" %>

	<table class="default4">
		<thead>
			<tr>
				<td colspan="6"><spring:message code="purchaseorder.purchaseorderitems" /> (<span class="poItemSize">${order.items.size()}</span>)</td>
			</tr>
			<tr>
				<th class="goodsitem" scope="col"><spring:message code="item"/></th>
				<th class="goodsdesc" scope="col"><spring:message code="description"/></th>
				<th class="goodsstatus" scope="col"><spring:message code="purchaseorder.receiptstatus"/></th>
				<th class="goodsapproved" scope="col"><spring:message code="purchaseorder.goodsapproved"/></th>
				<th class="goodscancelled" scope="col"><spring:message code="purchaseorder.cancelled"/></th>
				<th class="goodscomment" scope="col"><spring:message code="comment"/></th>
			</tr>
		</thead>
		<tbody>
			<c:set var="itemsAllCancelled" value="${not empty purchaseOrder.items}" />
			<c:forEach var="item" items="${purchaseOrder.items}" varStatus="loopTagStatus">
				<c:if test="${not command.goodsCancelled[loopTagStatus.index]}">
					<c:set var="itemsAllCancelled" value="false" />
				</c:if>
				<tr>
					<td>${item.itemno}</td>
					<td>${item.description}</td>
					<td>${item.receiptStatus.message}</td>
					<td>
						<c:choose>
							<c:when test="${item.goodsApproved}">
								<spring:message code="yes" />
							</c:when>
							<c:otherwise>
								<spring:message code="no" />
							</c:otherwise>
						</c:choose>
					</td>
					<td>
						<c:choose>
							<c:when test="${item.cancelled}">
								<spring:message code="yes" />
							</c:when>
							<c:otherwise>
								<spring:message code="no" />
							</c:otherwise>
						</c:choose>
					</td>
					<td>
						<c:forEach var="action" items="${item.progressActions}">
							<c:if test="${not empty action.description}">${action.description} &nbsp;</c:if>
						</c:forEach>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	
	<c:choose>
		<c:when test="${itemsAllCancelled}">
			<c:set var="warningVisible" value="vis" />
		</c:when>
		<c:otherwise>
			<c:set var="warningVisible" value="hid" />
		</c:otherwise>
	</c:choose>
	<div id="poCancelledWarning" class="warningBox1 ${warningVisible}">
		<div class="warningBox2">
			<div class="warningBox3">
				<div class="attention">
					<spring:message code="purchaseorder.cancelledwarning" />
				</div>
			</div>
		</div>
	</div>
