<%@ tag language="java" display-name="linkInvoiceItemToPO" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%-- Tag used exclusively inside viewinvoice.jsp, created to prevent code duplication --%>

<%@ attribute name="invoiceitem" required="true" type="java.lang.Object" %>

<c:forEach var="invoicepolink" items="${invoiceitem.invPOsItem}">
	<c:set var="po" value="${invoicepolink.invPO}" />
		<div class="invpo${po.poId} padding2">
			<spring:message var="invoicePOText" code="invoice.invoicepo" />
			<c:out value="${po.poNumber} (${invoicePOText})" />
		</div>
</c:forEach>

<%-- Following code is now obsolete, invoice po links managed separately --%>
<%-- 
<c:forEach var="po" items="${invoiceitem.invoice.pos}">
	<c:set var="alreadyOnII" value="false" />
	<c:forEach var="iipo" items="${invoiceitem.invItemPOs}">
		<c:if test="${not empty iipo.invPO && (iipo.invPO.poId == po.poId)}">
			<c:set var="alreadyOnII" value="true" />
		</c:if>
	</c:forEach>
	
	<c:if test="${alreadyOnII == true}">
		<div class="invpo${po.poId} padding2">
			${po.poNumber} (<spring:message code="invoice.invoicepo" />)
		</div>
	</c:if>
</c:forEach>
--%>