<%@ tag language="java" display-name="invoiceGenerateOptions" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ attribute name="invoiceId" required="true" type="java.lang.Object" %>
<%@ attribute name="detailedInvoiceType" required="true" type="java.lang.String" %>
<%@ attribute name="printDiscount" required="true" type="java.lang.String" %>
<%@ attribute name="printUnitaryPrice" required="true" type="java.lang.String" %>
<%@ attribute name="printQuantity" required="true" type="java.lang.String" %>
<%@ attribute name="isCalibration" required="true" type="java.lang.Boolean" %>
<%@ attribute name="description" required="true" type="java.lang.String" %>
<%@ attribute name="languageTags" required="true" type="java.util.List" %>
<%@ attribute name="defaultLanguageTag" required="true" type="java.lang.String" %>
<%@ attribute name="showAppendix" required="true" type="java.lang.Boolean" %>

<input type="hidden" id="hidIsCalibration" name="isCalibration" value='${isCalibration}' />
<input type="hidden" id="hidDetailedInvoiceType" name="detailedInvoiceType" value='${detailedInvoiceType}' />
<input type="hidden" id="hidPrintDiscount" name="PrintDiscount" value='${printDiscount}' />
<input type="hidden" id="hidPrintUnitaryPrice" name="PrintUnitaryPrice" value='${printUnitaryPrice}' />
<input type="hidden" id="hidPrintQuantity" name="PrintQuantity" value='${printQuantity}' />
<input type="hidden" id="hidShowAppendix" name="ShowAppendix" value='${showAppendix}' />

<div id="invoiceOptions">
	<div id="invoiceTypeChoice">
		<input type="radio" name="rInvoiceType" id="rDetailedInvoice" onclick="manageInvoiceTypeFields(true);" /><spring:message code="invoice.detailed" />
		<input type="radio" name="rInvoiceType" id="rSummaryInvoice" onclick="manageInvoiceTypeFields(false);" /><spring:message code="invoice.summarized" />
	</div>
	
	<div id="language">
		<p>
			<label><spring:message code="company.documentlanguage" />:</label>
			<select id="languageTag" name="languageTag">
				<c:forEach var="languageOption" items="${languageTags}">
					<option ${languageOption.key eq defaultLanguageTag ? "selected='selected'" : "" } value="${languageOption.key}">
						<c:out value="${languageOption.value}" />
					</option>
				</c:forEach>
			</select>
		</p>
	</div>
	<div id="summarizedFields">
		<p>
			<label><spring:message code="invoice.description" />:</label>
			<textarea id="txtInvoiceDescription" name="txtInvoiceDescription" rows="5" cols="50" >${description}</textarea>
		</p>
	</div>
	<div id="detailedFields">
		<p>
			<input type="checkbox" id="chkDiscount" /><spring:message code="invoice.printDiscount" />
		</p>
		<p>
			<input type="checkbox" id="chkUnitaryPrice" /><spring:message code="invoice.printUnitaryPrice" />
		</p>
		<p>
			<input type="checkbox" id="chkQuantity" /><spring:message code="invoice.printQuantity" />
		</p>
		<p>
			<input type="checkbox" id="chkShowAppendix" /><spring:message code="invoice.includeAppendix" />
		</p>
	</div>
</div>
<p>
	<spring:message code='invoice.generateinvoicedocument' var="generateDoc"/>
	<spring:message code="generate" var="generate" />
	
	<spring:message code='pageversion.new' var="versionNew"/>

			<input name="submit" id="submit" type="submit" onclick=" event.preventDefault(); genInvoiceDocPopup(${invoiceId}, 'birtinvoicedocument.htm?id=', '${generateDoc}', 1, 1); " value="${generate}" />
		

</p>