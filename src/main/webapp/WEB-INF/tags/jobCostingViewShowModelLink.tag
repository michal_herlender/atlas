<%@ tag language="java" display-name="showViewModelLink" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<%@ attribute name="classValue" required="false" type="java.lang.Object" %>
<%@ attribute name="target" required="false" type="java.lang.Object" %>
<%@ attribute name="instrumentmodelid" required="true" type="java.lang.Object" %>
<%@ attribute name="modelnametranslation" required="true" type="java.lang.Object" %>
<%@ attribute name="instrumentmodelname" required="true" type="java.lang.Object" %>
<%@ attribute name="instrumentsubfamilytypology" required="true" type="java.lang.Object" %>
<%@ attribute name="instrumentmodelmfrtype" required="true" type="java.lang.Object" %>
<%@ attribute name="instrumentmfrname" required="true" type="java.lang.Object" %>
<%@ attribute name="instrumentgenericmfr" required="true" type="java.lang.Object" %>

<a href="<c:url value="/instrumentmodel.htm?modelid=${instrumentmodelid}"/>" 
	<c:choose>
		<c:when test="${not empty classValue}">
			class="${classValue}"
		</c:when>
		<c:otherwise>
			class="mainlink"		 
		</c:otherwise>
	</c:choose>
	<c:if test="${not empty target}">
		target="${target}"
	</c:if>
	>
	<c:choose>
		<c:when test="${not empty instrumentmodelnametranslation and not empty instrumentcustomerdescription}">
			<t:jobCostingViewShowInstrument instrumentmodelmfrtype="${instrumentmodelmfrtype}" instrumentgenericmfr="${instrumentgenericmfr}" 
			instrumentmfrname="${instrumentmfrname}" instrumentmodelname="${instrumentmodelname}" modelnametranslation="${modelnametranslation}" 
			instrumentsubfamilytypology="${instrumentsubfamilytypology}"/>
		</c:when>
		<c:otherwise>
			<t:jobCostingViewShowModel  modelnametranslation="${modelnametranslation}" 
			instrumentsubfamilytypology="${instrumentsubfamilytypology}"/>
		</c:otherwise>
	</c:choose>
</a>