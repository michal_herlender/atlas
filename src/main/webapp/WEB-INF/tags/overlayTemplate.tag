<!DOCTYPE HTML>
<%-- Basic template for use in overlay JSPs --%>
<%@ tag language="java" display-name="overlayTemplate" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>

<%@ attribute name="scriptPart" fragment="true" required="false" %>
<%@ attribute name="bodywidth" required="true" type="java.lang.Integer"
			  description="Should be 40px less than overlay width" %>

<c:set var="req" value="${pageContext.request}"/>

<html>
<head>
	<meta name="_serverTimeZone" content="${cwms:defaultTimeZone()}">
	<meta name="_contextPath" content="${pageContext.request.contextPath}">
	<meta name="_systemType" content="${systemType}">
	<meta name="_defaultCurrency" content="${defaultCurrency.currencyCode}">
	<link rel="stylesheet" href="styles/global.css" type="text/css" title="antech"/>
	<link rel="stylesheet" href="styles/print/print.css" type="text/css" media="print"/>
	<link rel="stylesheet" href="styles/jQuery/jquery-ui.css" type="text/css" media="screen"/>
	<jsp:invoke fragment="scriptPart"/>
	<script>
		// set variable to default context of project (used in javascript)
		var springUrl = '${req.contextPath}';
		// get allocated subdivision from session
		var allocatedSubdivId = ${sessionScope.allocatedSubdiv.key};
		// get user preferences
			var userPrefs = {
				/* tooltips turned on? */
				tooltips: ${currentContact.userPreferences.mouseoverActive},
				/* job item key nav on? */
				jikeynav: ${currentContact.userPreferences.jiKeyNavigation}
			};
			// set variable to i18n of javascript
			var i18locale = '${rc.locale.language}';
		</script>
		<script src='script/thirdparty/jQuery/jquery.js'></script>
		<script src='script/thirdparty/jQuery/jquery-migrate-1.2.1.js'></script>
		<script src='script/trescal/core/template/Cwms_Main.js'></script>
	</head>
	<body onload=" loadScript.init(true); " class="iframed" style=" width: ${bodywidth}px; min-width: ${bodywidth}px; ">
		<jsp:doBody/>
	</body>
</html>