<%@ tag language="java" display-name="showTPQuoteRequestJobForm velocityorder: job models" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="tpquoterequest" tagdir="/WEB-INF/tags/tpquoterequest" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ attribute name="job" required="true" type="java.lang.Object" %>
<%@ attribute name="models" required="true" type="java.lang.Object" %>

<div id="linktojob" class="infobox">
	
	<div id="linktype" class="float-right">
		<a href="#" onclick="$('modeltypes').style.display = 'none'; $('jobitems').style.display = 'block'; return false; " class="mainlink">
			Link specific Quotation items
		</a>
		|
		<a href="#" onclick="$('modeltypes').style.display = 'block'; $('jobitems').style.display = 'none'; return false; " class="mainlink">
			Link only Model Types
		</a>
	</div>
	
	<!-- this div clears any floats and provides space in between elements -->
	<div class="clear"></div>
	
	<fieldset id="jobitems">
	
		<legend>Available items from source job <a href="viewjob.htm?jobid=${job.jobid}" class="mainlink">${job.jobno}</a></legend>

		<!-- this list displays available items from source job -->
		<ol>
			<c:forEach var="item" items="${job.items}">
				<li>
					<div class="modulebox">
				
						<div class="modulebase">
								<c:set var="checkedText" value=""/>
								<c:if test="${item.jobItemId == tpQuoteRequestForm.itemid}">
									<c:set var="checkedText" value="checked"/>
								</c:if>
								<form:checkbox path="jobitemids" value="${item.jobItemId}" checked="${checkedText}" />
								
								${item.itemNo}. 
								<cwms:showmodel instrumentmodel="${item.inst.model}"/>
								<span class="baseCalType"><cwms:besttranslation translations="${item.caltype.serviceType.shortnameTranslation}"/></span>
								<span class="basePlant">${item.inst.plantno}</span>
						</div>
					</div>
				</li>
			</c:forEach>
		</ol>
	
	</fieldset>
	
	<tpquoterequest:showTPQuoteRequestModelTypes models="${models}" />
	
</div>