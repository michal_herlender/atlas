<%@ tag language="java" display-name="showTPQuoteRequestQuotationForm velocityorder: quotation models" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="tpquoterequest" tagdir="/WEB-INF/tags/tpquoterequest" %>

<%@ attribute name="quotation" required="true" type="java.lang.Object" %>
<%@ attribute name="models" required="true" type="java.lang.Object" %>

<div id="linktype" class="float-right">
	<a href="#" onclick=" $j(this).removeClass('vis').addClass('hid'); $j(this).next().removeClass('hid').addClass('vis'); $j('#tpq_addMT').removeClass('vis').addClass('hid'); $j('#tpq_addQI').removeClass('hid').addClass('vis');  return false; " class="mainlink hid">
		Link specific Quotation items
	</a>
	<a href="#" onclick=" $j(this).removeClass('vis').addClass('hid'); $j(this).prev().removeClass('hid').addClass('vis'); $j('#tpq_addMT').removeClass('hid').addClass('vis'); $j('#tpq_addQI').removeClass('vis').addClass('hid'); return false; " class="mainlink vis">
		Link only Model Types
	</a>
</div>

<!-- clear floats and restore page flow -->
<div class="clear"></div>

<!-- this table displays available items / modeltypes from source quotation -->
<table id="tpq_addQI" class="vis default2" summary="This table displays available items - modeltypes from source quotation">
	
	<thead>
		<tr>
			<td colspan="6">
				Available items / modeltypes from source quotation <a href="viewquotation.htm?id=${quotation.id}" class="mainlink">${quotation.qno} version ${quotation.ver}</a>
			</td>
		</tr>
		<tr>
			<th class="itemno" scope="col"><spring:message code="item"/></th>
			<th class="add" scope="col">
				<input type="checkbox" id="selectAll" onclick=" selectAllItems(this.checked, 'tpq_addQI'); " checked="true"/><spring:message code="all"/>
			</th>
			<th class="model" scope="col"><spring:message code="model"/></th>
			<th class="qty" scope="col"><spring:message code="quantity"/></th>
			<th class="caltype" scope="col"><spring:message code="caltype"/></th>
			<th class="plantno" scope="col"><spring:message code="plantno"/></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="6">&nbsp;</td>
		</tr>
	</tfoot>
	
	<tbody>
		<c:forEach var="heading" items="${quotation.quoteheadings}">
			<c:if test="${not heading.systemDefault}">
				<tr>
					<th colspan="6">${heading.headingName}</th>
				</tr>
			</c:if>
			<c:set var="caltype" value="0"/>
			<c:set var="itemno" value="0"/>
			<c:forEach var="quoteitem" items="${heading.quoteitems}">
				<c:if test="${not quoteitem.partOfBaseUnit}">
					<c:if test="${caltype != quoteitem.caltype.calTypeId}">
						<tr>
							<td colspan="6" class="bold"><cwms:besttranslation translations="${quoteitem.caltype.serviceType.longnameTranslation}"/>&nbsp; <spring:message code="items" /></td>
						</tr>
						<c:set var="itemno" value="1"/>
					</c:if>
				
					<c:set var="caltype" value="${quoteitem.caltype.calTypeId}"/>

					<tr>
						<td class="text-center">${itemno}.</td>	
						<td class="text-center">
								<c:set var="checkedText" value=""/>
								<c:if test="${quoteitem.id == tpQuoteRequestForm.itemid}">
									<c:set var="checkedText" value="checked"/>
								</c:if>
								<form:checkbox path="quoteids" value="${quoteitem.id}" checked="${checkedText}" />
						</td>								
						<td>
							<c:choose>
								<c:when test="${not empty quoteitem.inst}">
									<t:showViewModelLink instrumentmodel="${quoteitem.inst.model}" />
								</c:when>
								<c:otherwise>
									<t:showViewModelLink instrumentmodel="${quoteitem.model}" />
								</c:otherwise>
							</c:choose>
						</td>		 
						<td class="text-center">${quoteitem.quantity}</td>		
						<td class="text-center"><cwms:besttranslation translations="${quoteitem.caltype.serviceType.shortnameTranslation}"/></td>
						<td>
							<c:choose>
								<c:when test="${not empty quoteitem.inst}">
									${quoteitem.inst.plantno}
								</c:when>
								<c:otherwise>
									${quoteitem.plantno}
								</c:otherwise>
							</c:choose>
						</td>		
					</tr>
				
					<c:set var="thisBaseUnit" value="false" />
					<c:forEach var="module" items="${quoteitem.modules}">
						<c:set var="thisBaseUnit" value="true" />
						<tr id="modlistitem${module.id.itemno}">
							<td class="text-center"><img src="img/icons/arrow_merge.png" height="16" width="16" alt="Module of item ${itemno}" title="Module of item ${itemno}" /></td>
							<td class="text-center">
									<c:set var="checkedText" value=""/>
									<c:if test="${module.id == tpQuoteRequestForm.itemid}">
										<c:set var="checkedText" value="checked"/>
									</c:if>
									<form:checkbox path="quoteids" value="${module.id}" checked="${checkedText}"/>
							</td>
							<td>
								<c:choose>
									<c:when test="${not empty module.inst}">
										<cwms:showmodel instrumentmodel="${module.inst.model}"/>
									</c:when>
									<c:otherwise>
										<cwms:showmodel instrumentmodel="${module.model}"/>
									</c:otherwise>
								</c:choose>
							</td>									
							<td class="text-center">${module.quantity}</td>
							<td class="text-center"><cwms:besttranslation translations="${module.caltype.serviceType.shortnameTranslation}"/></td>
							<td>
								<c:choose>
									<c:when test="${not empty module.inst}">
										${module.inst.plantno}
									</c:when>
									<c:otherwise>
										${module.plantno}
									</c:otherwise>
								</c:choose>
							</td>									
						</tr>
					</c:forEach>
					
					<c:set var="itemno" value="${itemno + 1}" />
				</c:if>
				
			</c:forEach>
			
		</c:forEach>
		
	</tbody>

</table>

<tpquoterequest:showTPQuoteRequestModelTypes models="${models}"/>