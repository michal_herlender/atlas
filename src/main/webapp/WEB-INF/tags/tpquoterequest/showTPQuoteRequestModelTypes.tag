<%@ tag language="java" display-name="showTPQuoteRequestModelTypes velocityorder: models" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ attribute name="models" required="true" type="java.lang.Object" %>

<table id="tpq_addMT" class="hid default2" summary="This table shows only model types">
	
	<thead>
		<tr>
			<td colspan="4">Link only model types</td>
		</tr>
		<tr>
			<th class="add" scope="col">
				<input type="checkbox" id="selectAll" onclick=" selectAllItems(this.checked, 'tpq_addMT'); " /> All
			</th>
			<th class="model" scope="col">Model</th>
			<th class="qty" scope="col">Quantity</th>
			<th class="caltype" scope="col">Caltype</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="4">&nbsp;</td>
		</tr>
	</tfoot>
	
	<tbody>
		<c:forEach var="key" items="${models.keySet()}">
			<c:set var="model" value="${models.get(key)}" />
			<tr>
				<td class="text-center">
						<form:checkbox path="modelids" value="${key}"/>
				</td>
				<td><cwms:showmodel instrumentmodel="${model}"/></td>
				<td class="text-center">
						<form:input type="text" value="1" size="5" path="modelidQtys" />
				</td>
				<td class="text-center">
						<form:select path="modelCalTypeIds">
							<option value="">N/A</option>
							<c:forEach var="caltype" items="${caltypes}">
								<option value="${caltype.calTypeId}"><cwms:besttranslation translations="${caltype.serviceType.shortnameTranslation}"/></option>
							</c:forEach>
						</form:select>

				</td>
			</tr>
		</c:forEach>
	</tbody>
	
</table>