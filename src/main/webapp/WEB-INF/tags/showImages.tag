<%@ tag language="java" display-name="showImages" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="entity" required="true" type="java.lang.Object" %>
<%@ attribute name="imagetype" required="true" type="java.lang.Object" %>
<%@ attribute name="imagetypeid" required="true" type="java.lang.Integer" %>

<div class="infobox">	
			
	<div class="gallerifficContent">
							
		<div id="thumbs-adv" class="navigation">
			<ul class="thumbs noscript">
				<c:set var="count" value="${1}"></c:set>
				<c:choose>
					<c:when test="${entity.images.size() < 1}">
						<li class="thumbitem" id="emptyImage">
							<a class="thumb" href="img/noimage.png" title="">
								<img src="img/noimage.png" alt="" height="60" width="80" />
							</a>
							<div class="caption">
								<div class="download"></div>
								<div class="image-title">
									<spring:message code="viewmod.addnewimage"/>
									<a href="#" onclick=" addImageToEntity(${imagetypeid}, '${imagetype}'); return false; " title="">
										<img src="img/icons/picture_add.png" width="16" height="16" alt="<spring:message code='viewmod.addimages'/>" title="<spring:message code='viewmod.addimages'/>" />
									</a>
								</div>
								<div class="image-desc"></div>
								<div class="image-desc"></div>
							</div>
						</li>
					</c:when>
					<c:otherwise>
						<c:forEach var="image" items="${entity.images}">
							<li class="thumbitem" id="systemimg${image.id}">							
								<a class="thumb" href="displayimage?id=${image.id}" title="">
									<img src="displaythumbnail?id=${image.id}" alt="" />
								</a>
								<div class="caption">
									<div class="download">
										<a href="#" onclick=" addImageToEntity(${imagetypeid}, '${imagetype}'); return false; " title="">
											<img src="img/icons/picture_add.png" width="16" height="16" alt="" title="<spring:message code='viewmod.addimages'/>" />
										</a>
										&nbsp;&nbsp;&nbsp;
										<a href="displayimage?id=${image.id}"><spring:message code="viewmod.downloadoriginal"></spring:message></a>
										&nbsp;&nbsp;&nbsp;
										<a href="" onclick=" deleteGalleryImage(${image.id}, ${imagetypeid}, '${imagetype}'); return false; ">
											<img src="img/icons/redcross.png" width="16" height="16" alt="<spring:message code='viewmod.deletethisimage'/>" title="<spring:message code='viewmod.deletethisimage'/>" />
										</a>
									</div>
									<div class="image-title"><spring:message code="viewmod.image"/> ${count}</div>
	
									<div class="image-desc">
										<strong><spring:message code="description"/>: &nbsp;</strong>
										<c:choose>
											<c:when test='${image.description != "" }'>
												${image.description}
											</c:when>
											<c:otherwise>
												<spring:message code="nocomment"/>
											</c:otherwise>
										</c:choose>
									</div>
									<div class="image-desc">
										<strong><spring:message code="viewmod.taggedas"/>: &nbsp;</strong>
										<c:choose>
											<c:when test='${image.tags.size() > 0}'>
												<c:forEach var="tag" items="${image.tags}" varStatus="loop">
													<c:if test="${loop.index > 0 }"><c:out value=", "/></c:if>
													<c:out value="${tag.tag}"/>					
												</c:forEach>
											</c:when>
											<c:otherwise>
												<spring:message code="viewmod.notagsassigned"/>
											</c:otherwise>
										</c:choose>
									</div>
								</div>
							</li>
							<c:set var="count" value="${count + 1}"/>																
						</c:forEach>
					</c:otherwise>
				</c:choose>
				
			</ul>
		</div>
						
		<div id="gallery-adv" class="content">
			<div id="controls-adv" class="controls"></div>
			<div id="loading-adv" class="loader"></div>
			<div id="slideshow-adv" class="slideshow"></div>
			<div id="caption-adv" class="embox"></div>
		</div>
		
		<!-- clear floats and restore page flow -->
		<div class="clear"></div>
											
	</div>										
	
	
</div>