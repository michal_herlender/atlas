<%@ tag language="java" display-name="instructions" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>

<%@ attribute name="subdiv" required="true" type="java.lang.Object" %>
<%@ attribute name="procedureCategories" required="true" type="java.lang.Object" %>
<%@ attribute name="editable" required="true" type="java.lang.Boolean" %>

<table class="default2 subdivprocedurecategories">
    <thead>
    <tr>
        <td colspan="5">
            ${subdiv.subname}&nbsp;<spring:message
                code="company.procedurecategories"/>&nbsp;(${procedureCategories.size()})
        </td>
    </tr>
    <tr>
        <th class="department" scope="col"><spring:message code="company.department"/></th>
        <th class="depdefault" scope="col"><spring:message code="default"/></th>
        <th class="name" scope="col"><spring:message code="name"/></th>
        <th class="description" scope="col"><spring:message code="description"/></th>
        <th class="traceable" scope="col"><spring:message code="procedurecategory.traceable"/></th>

    </tr>
    </thead>
    <tfoot>
    <c:if test="${editable}">
        <tr>
            <td colspan="5"><a href="editcapabilitycategories.htm?subdivid=${subdiv.subdivid}"
                               class="mainlink"><spring:message code="company.editprocedurecategories"/></a></td>
        </tr>
    </c:if>
    </tfoot>
	<tbody>
    <c:forEach var="category" items="${procedureCategories}" varStatus="loopStatus">
        <c:choose>
            <c:when test="${loopStatus.count % 2 == 0}"><c:set var="rowClass" value="even"/></c:when>
            <c:otherwise><c:set var="rowClass" value="odd"/></c:otherwise>
        </c:choose>
        <tr class="${rowClass}">
            <td>${category.department.name}</td>
            <td><spring:message code="${category.departmentDefault}"/></td>
            <td>${category.name}</td>
            <td>${category.description}</td>
            <td><cwms:yesno flag="${category.splitTraceable}"/></td>
        </tr>
    </c:forEach>
	</tbody>
</table>