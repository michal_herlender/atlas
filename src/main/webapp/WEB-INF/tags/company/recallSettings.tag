<%@ tag language="java" display-name="recallSettings" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>

<%@ attribute name="recallConfig" required="true" type="java.lang.Object" %>
<%@ attribute name="company" required="true" type="java.lang.Object" %>

<table class="default2" id="recallconfig">
	<thead>
		<tr>
			<td colspan="4">
				<spring:message code="companyrecall.title"/>
			</td>
		</tr>
		<tr>
			<th class="name" scope="col"><spring:message code="name"/></th>
			<th class="value" scope="col"><spring:message code="company.value"/></th>
			<th class="source" scope="col"><spring:message code="systemdefault.source"/></th>
			<th class="descrip" scope="col"><spring:message code="edit"/></th>
		</tr>
	</thead>
	<tbody>
		<spring:message var="source" code="systemdefault.systemdefault"/>
		<spring:message var="valueActive" code="false"/>
		<c:if test="${not empty recallConfig}">
			<spring:message var="valueActive" code="${recallConfig.active}"/>
		</c:if>
		
		<tr>
			<td><image:globalSetting/><spring:message code="companyrecall.active"/></td>
			<td>${valueActive}</td>
			<td>${source}</td>
			<td class="center">
			<a >
			
			<a href="editcompanyrecall.htm?coid=${company.coid}" class="mainlink">
				<spring:message code="edit"/>
			</a>
			</td>
		</tr>
		<c:if test="${not empty recallConfig}">
			<c:set var="source"><spring:message code="systemdefault.overriden"/> COMPANY</c:set>
			<tr>
				<td><image:globalSetting/><spring:message code="companyrecall.recallperiodstarts"/></td>
				<td>${recallConfig.periodStart}</td>
				<td>${source}</td>
				<td></td>
			</tr>
			<tr>
				<td><image:globalSetting/><spring:message code="companyrecall.futuremonths"/></td>
				<td>${recallConfig.futureMonths}</td>
				<td>${source}</td>
				<td></td>
			</tr>
			<tr>
				<td><image:globalSetting/><spring:message code="companyrecall.attachmenttype"/></td>
				<td>${recallConfig.attachmentType}</td>
				<td>${source}</td>
				<td></td>
			</tr>
			<tr>
				<td><image:globalSetting/><spring:message code="companyrecall.templatetype"/></td>
				<td>${recallConfig.templateType}</td>
				<td>${source}</td>
				<td></td>
			</tr>
			<tr>
				<td><image:globalSetting/><spring:message code="companyrecall.emailsubject"/></td>
				<td>${recallConfig.subjectKey}</td>
				<td>${source}</td>
				<td></td>
			</tr>
			<tr>
				<td><image:globalSetting/><spring:message code="companyrecall.customtext"/></td>
				<td>${recallConfig.customText.replaceAll("\\n","<br/>")}</td>
				<td>${source}</td>
				<td></td>
			</tr>
		</c:if>
	</tbody>
</table>