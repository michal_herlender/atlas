<%@ tag language="java" display-name="systemDefaults" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image" %>

<%@ attribute name="parentEntity" required="true" type="java.lang.Object" %>
<%@ attribute name="entityKey" required="true" type="java.lang.Object" %>
<%@ attribute name="scope" required="true" type="java.lang.Object" %>
<%@ attribute name="systemdefaults" required="true" type="java.util.List" %>

<link rel="stylesheet" href="styles/jQuery/jquery.datetimepicker.css" type="text/css">

<table class="default2" id="settings" summary="This table lists company preferences">
	<thead>
		<tr>
			<td colspan="4">
				<spring:message code="systemdefault.defaultSettings"/>
			</td>
		</tr>
		<tr>
			<th class="name" scope="col"><spring:message code="name"/></th>
			<th class="value" scope="col"><spring:message code="company.value"/></th>
			<th class="source" scope="col"><spring:message code="systemdefault.source"/></th>
			<th class="descrip" scope="col"><spring:message code="systemdefault.editreset"/></th>
		</tr>
	</thead>
	<tbody>
		<c:set var="groupname" value=""/>
		<c:choose>
			<c:when test="${systemDefaultsTypes.size() == 0}">
				<tr>
					<td colspan="4" class="odd center"><strong><spring:message code="systemdefault.nosettings" /></strong></td>
				</tr>
			</c:when>
			<c:otherwise>
				<c:forEach var="sysDefaultDTO" items="${systemdefaults}">
					<c:set var="sysDefault" value="${sysDefaultDTO.systemDefault}"/>
					<c:set var="defId" value="${sysDefaultDTO.defaultName.ordinal()}"/>
					<c:if test="${groupname != sysDefault.group.name}">
						<tr>
							<td colspan="4"><strong><t:showTranslationOrDefault translations="${sysDefault.group.nameTranslation}" defaultLocale="${defaultlocale}" /></strong></td>
						</tr>
					</c:if>
					<c:set var="groupname" value="${sysDefault.group.name}"/>
					<c:set var="value" value="${sysDefaultDTO.value}"/>
					<c:set var="matchFound" value="${sysDefaultDTO.applicationId != null}"/>
					<c:set var="matchFoundAtScope" value="${matchFound && sysDefaultDTO.scope == scope}"/>
					<c:set var="applicationId" value="${sysDefaultDTO.applicationId}"/>
					<c:set var="applicationValue" value=""/>
					<tr class="odd">
						<td>
							<!-- hover over this anchor and a tooltip is displayed showing the contents of the div created below -->
							<a href="?id=#defnamedesc${defId}&amp;width=350" onclick="return false;" id="tip${defId}" title="<spring:message code='systemdefault.description' />" class="jTip comment">
								<c:choose>
									<c:when test="${sysDefault.getGroupwide()}"><image:globalSetting/></c:when>
									<c:otherwise><image:businessCompanySpecific/></c:otherwise>
								</c:choose>
								${sysDefaultDTO.localeName}
							</a>
							<div id="defnamedesc${defId}" class="hid">
								<cwms:besttranslation translations="${sysDefault.defaultDescriptionTranslation}"/>
							</div>
						</td>
						<td>
							<span id="value${defId}">${value}</span>
						</td>
						<td id="rule${defId}">
							<c:choose>
								<c:when test="${matchFound == true}">
									<span class="attention"><spring:message code="systemdefault.overriden" />&nbsp;${sysDefaultDTO.scope.getMessage()}</span>
								</c:when>
								<c:otherwise>
									<spring:message code="systemdefault.systemdefault" />
								</c:otherwise>
							</c:choose>
						</td>
						<td class="center">
							<script>
								var sdValues${defId} = <cwms:jsarray elements="${sysDefaultDTO.values}" />;
							</script>
							<span id="edit${defId}" class="vis">
								<cwms:securedJsLink permission="COMPANY_SETTINGS_EDIT" collapse="True" classAttr="mainlink" onClick="event.preventDefault(); editSystemDefault(${defId}, ${sysDefaultDTO.discreteType}, ${sysDefaultDTO.multiChoice}, sdValues${defId}, '${value}', '${ sysDefaultDTO.systemDefault.defaultDataType}');">
									<spring:message code="edit"/>
								</cwms:securedJsLink>
							</span>
							<span id="save${defId}" class="hid">
								<spring:message var="systemCode" code="systemdefault.overriden" />
								<spring:message var="editCode" code="edit" />
								<cwms:securedJsLink permission="COMPANY_SETTINGS_EDIT" collapse="True" classAttr="mainlink" onClick="event.preventDefault(); saveSystemDefault(${defId}, ${sysDefaultDTO.discreteType}, ${sysDefaultDTO.multiChoice}, sdValues${defId}, ${entityKey}, '${scope}', '${sytemCode}', '${editCode }','${ sysDefaultDTO.systemDefault.defaultDataType}');" >
									<spring:message code="save"/>
								</cwms:securedJsLink>
							</span>
							<c:choose>
								<c:when test="${matchFoundAtScope}">
									<span id="reset${defId}" class="vis"> |
										<cwms:securedJsLink permission="COMPANY_SETTINGS_EDIT" collapse="True" classAttr="mainlink" onClick="event.preventDefault(); resetSystemDefault(${applicationId}, ${sysDefaultDTO.discreteType}, ${sysDefaultDTO.multiChoice}, sdValues${defId}, '${value}');" >
											<spring:message code="systemdefault.reset"/>
										</cwms:securedJsLink>
									 </span>
								</c:when>
								<c:otherwise>
									<span id="reset${defId}" class="hid"> |
										<cwms:securedJsLink permission="COMPANY_SETTINGS_EDIT" collapse="True" classAttr="mainlink" onClick="event.preventDefault(); resetSystemDefault(${applicationId}, ${sysDefaultDTO.discreteType}, ${sysDefaultDTO.multiChoice}, sdValues${defId}, '${value}');" >
											<spring:message code="systemdefault.reset"/>
										</cwms:securedJsLink>
									</span>
								</c:otherwise>
							</c:choose>
						</td>
					</tr>
				</c:forEach>
			</c:otherwise>
		</c:choose>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="4">&nbsp;</td>
		</tr>
	</tfoot>
</table>

<script src="script/thirdparty/jQuery/jquery.datetimepicker.full.min.js"></script>

<script>

	/*
	 * Changes the value field into an editable select list with a set of 'allowed' values for the given field.
	 * The select list will default to the currently selected onscreen value as well as indicating the current system default.
	 * @param (defaultId) id of the selected default
	 * @param (defaultDataType) data type of the default (i.e. 'boolean')
	 * @param (value) value currently assigned to the default
	 * @param (min) some default types have a value range, this is the minimum value
	 * @param (max) some default types have a value range, this is the maximum value	
	*/
	function editSystemDefault(defaultId, discreteType, multiChoice, values, value, dataType)
	{
		if (discreteType) {
			if(multiChoice) {
				// empty the rule cell for this default
				$j('#rule' + defaultId).empty();
				// create loop and append inputs checkbox
				values.forEach(function(v) {
					console.log('v = '+v+', value = '+value+', value.includes(v) -> '+(value.includes(v)));
					$j('#rule' + defaultId).append('<input type="checkbox" id="newDefaultValue' + defaultId + '_' + v + '" value="' + v + '" ' + (value.includes(v) ? 'checked':'') + '/>' + v + '<br/>');
				});
				// hide the edit link and show the save link
				$j('#edit' + defaultId).removeClass().addClass('hid');
				$j('#save' + defaultId).removeClass().addClass('vis');
			}
			else {
					// empty the rule cell for this default and append a select list
					$j('#rule' + defaultId).empty().append('<select id="newDefaultValue' + defaultId + '">' + '</select>');
					// create loop and append options to the select list
					values.forEach(function(v) {
						$j('#rule' + defaultId + ' select').append('<option value="' + v + '">' + v + '</option>');
					});
					// set selected value to current value
					$j('#newDefaultValue' + defaultId).attr('value', value);
					// hide the edit link and show the save link
					$j('#edit' + defaultId).removeClass().addClass('hid');
					$j('#save' + defaultId).removeClass().addClass('vis');
				}
		}
		else {
			// empty the rule cell for this default and append a select list
			$j('#rule' + defaultId).empty().append('<input id="newDefaultValue' + defaultId + '" type="text" />');
			// set selected value to current value
			$j('#newDefaultValue' + defaultId).attr('value', value);
			// hide the edit link and show the save link
			$j('#edit' + defaultId).removeClass().addClass('hid');
			$j('#save' + defaultId).removeClass().addClass('vis');
			// if datatype is 'date' init datepicker
			if(dataType == 'date'){
				$j('#newDefaultValue' + defaultId).datetimepicker({
					timepicker:false,
					format:'d.m.Y',
					minTime:$j.now(),
					validateOnBlur:false,
					defaultSelect:false
				});
			}
		}
	}
	
	/*
	 * Saves a new overridden default value for the current company/subdiv/contact
	 * @param (defaultId) id of the selected default
	 * @param (value) value currently assigned to the default
	 * @param (id) id of the entity whose default setting is to be changed (i.e. company coid, subdivision subdivid)
	 * @param (type) type of entity whose default setting is to be changed (i.e. 'COMPANY', 'SUBDIVISION')
	*/
	function saveSystemDefault(defaultId, discreteType, multiChoice, values, scopeId, scope, dataType)
	{
		// prepare value to send
		var value = '';
		if(multiChoice) {
			values.forEach(function(v) {
				if($j('#newDefaultValue'+defaultId+'_'+v+':checkbox:checked').length > 0)
					value += v+',';
			});
			if(value != '')
				value = value.substr(0, value.length - 1);
		}
		else
			value = $j('#newDefaultValue'+defaultId).val();
		
		// save SystemDefaultApplication ajax call 
		$j.ajax({
			url: "saveSystemDefaultApplication.json",
			data: {
				defaultId: defaultId,
				newValue: value,
				scope: scope,
				scopeId: scopeId
				},
			async: true
		}).done(function(systemDefault) {
			$j('#value' + defaultId).empty().append(value);
			// empty the source cell and add 'Overriden Rule' text in red 
			$j('#rule' + defaultId).empty().append('<span class="attention"><spring:message code="systemdefault.overriden"/> ' + scope + '</span>');
			$j('#edit' + defaultId).removeClass().addClass('vis');
			$j('#edit' + defaultId + ' a').attr('onClick', 'event.preventDefault(); editSystemDefault(' + defaultId + ', ' + discreteType + ', ' + multiChoice + ', ' + JSON.stringify(values) + ', ' + JSON.stringify(value) + ','+dataType+');');
			$j('#save' + defaultId).removeClass().addClass('hid');
			$j('#reset' + defaultId).removeClass().addClass('vis');
			$j('#reset' + defaultId + ' a').attr('onClick', 'event.preventDefault(); resetSystemDefault(' + systemDefault.id + ', ' + discreteType + ', ' + multiChoice + ', ' + JSON.stringify(values) + ', ' + JSON.stringify(value) + ');');
		}).fail(function(error) {
			alert("Sorry, an error occurs while saving system default.");
		});
	}
	
	/*
	 * Resets the given default back to the system default value
	 * @param (applicationid) id of the system default to reset
	 */
	function resetSystemDefault(applicationid, discreteType, multiChoice, values, value)
	{
		// make sure that there is an overriden value to reset
		if (applicationid == null) {
			alert('System default value already present');
		}
		else {
			$j.ajax({
				url: "deleteSystemDefaultApplication.json",
				data: {applicationId: applicationid},
				async: true
			}).done(function(systemDefault) {
				$j('#value' + systemDefault.id).empty().append(systemDefault.defaultValue == "true" ? "Yes" : systemDefault.defaultValue == "false" ? "No" : systemDefault.defaultValue );
				// change the rule cell back to 'System Default' text
				$j('#rule' + systemDefault.id).empty().append('<spring:message code="systemdefault.systemdefault" />');
				// empty contents of link to be changed and add new content for the onclick event, make this link visible and hide the save and reset link
				$j('#edit' + systemDefault.id).removeClass().addClass('vis');
				$j('#edit' + systemDefault.id + ' a').attr('onclick', 'event.preventDefault(); editSystemDefault(' + systemDefault.id + ', ' + discreteType + ', ' + multiChoice + ', ' + JSON.stringify(values) + ', ' + JSON.stringify(systemDefault.defaultValue) + ');');
				$j('#save' + systemDefault.id).removeClass().addClass('hid');
				$j('#reset' + systemDefault.id).removeClass().addClass('hid');
			}).fail(function(error) {
				alert("Sorry, an error occurs while deleting system default.");
			});
		}
	}
	
</script>