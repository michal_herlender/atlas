<%@ tag language="java" display-name="bpos" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ taglib prefix="tags" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ attribute name="bpos" required="true" type="java.lang.Object" %>
<%@ attribute name="name" required="true" type="java.lang.Object" %>
<%@ attribute name="id" required="true" type="java.lang.Object" %>
<%@ attribute name="scope" required="true" type="java.lang.String" %>

<spring:message var="textEditBPO" code="editbpo.headtext"/>
<spring:message var="textCreateBPO" code="createbpo.linktext"/>
<spring:message var="textEdit" code="edit"/>
<spring:message var="textCreate" code="create"/>
<spring:message var="escapedName" javaScriptEscape="true" text="${name}" />

<script type="module" src="script/components/cwms-bpos/cwms-bpos.js"></script>
<cwms-bpos unitName="${name}" bPosList="${cwms:objectToJson(bpos)}" scope="${scope}" unitId="${id}" ></cwms-bpos>
