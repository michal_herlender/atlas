<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ attribute name="quotationRequest" required="true" type="org.trescal.cwms.core.quotation.entity.quotationrequest.QuotationRequest" %>
<%@ attribute name="coroles" required="true" type="java.util.Collection" %>
<%@ attribute name="countries" required="true" type="java.util.Collection" %>
<%@ attribute name="businessAreas" required="true" type="java.util.Collection" %>
<%@ attribute name="transportOPtions" required="true" type="java.util.Collection" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<div id="conversionutilitydialog" style="display: none; ">
    <div id="globalerrors" class="warningBox3" hidden>Please correct the errors below</div>
    <form:form>
        <fieldset>
            <ol>
                <li>
                    <label><spring:message code="company.company"/></label>
                    <input type="text" name="coname" size="40" value="${quotationRequest.company}" tabindex="1" />
                    <span class="error highlight" hidden></span>
                </li>
                <li>
                    <label><spring:message code="company.legalid"/></label>
                    <input type="text" name="legalIdentifier" value="" />
                    <span class="error highlight" hidden></span>
                </li>
                <li>
                    <label><spring:message code="company.subdivisionname"/></label>
                    <input type="text" name="subname" value=""/>
                    <span class="error highlight" hidden></span>
                </li>
                <li>
                    <label><spring:message code="company.role"/></label>
                    <select name="companyRole" tabindex="3">
                        <c:forEach var="corole" items="${coroles}">
                            <option value="${corole.key}" ${corole.key=="PROSPECT"?"selected" : ""}>${corole.value}</option>
                        </c:forEach>
                    </select>
                </li>
                <li>
                    <label><spring:message code="country"/></label>
                    <select name="countryId" tabindex="5">
                        <c:forEach var="country" items="${countries}">
                            <option value="${country.key}" ${country.key == userCountryId ? "SELECTED" : ""}>${country.value}</option>
                        </c:forEach>
                    </select>
                    <c:if test="${not empty quotationRequest.country}">
                        &nbsp;<span>[${quotationRequest.country}]</span>
                    </c:if>
                </li>
                <li>
                    <label><spring:message code="currency"/></label>
                    <select name="currencyId" tabindex="6">
                        <c:forEach var="currency" items="${currencies}">
                            <option value="${currency.key}" ${currency.key == userCurrencyId ? "SELECTED" : ""}>${currency.value}</option>
                        </c:forEach>
                    </select>
                </li>
                <li>
                    <label><spring:message code="company.sector"/></label>
                    <select name="businessAreaId" tabindex="6">
                        <c:forEach var="businessArea" items="${businessAreas}">
                            <option value="${businessArea.key}" ${businessArea.key == defaultBusinessAreaId ? "SELECTED" : ""}>${businessArea.value}</option>
                        </c:forEach>c
                    </select>
                </li>
                <li>
                    <label><spring:message code="address1"/></label>
                    <input type="text" name="addr1" size="40" value="${quotationRequest.addr1}" tabindex="7" />
                    <span class="error highlight" hidden></span>
                </li>
                <li>
                    <label><spring:message code="address2"/></label>
                    <input type="text" name="addr2" size="40" value="${quotationRequest.addr2}" tabindex="8" />
                </li>
                <li>
                    <label><spring:message code="address3"/></label>
                    <input type="text" name="addr3" size="40" value="" tabindex="8" />
                </li>
                <li>
                    <label><spring:message code="town"/></label>
                    <input type="text" name="town" size="40" value="${quotationRequest.town}" tabindex="9" />
                    <span class="error highlight" hidden></span>
                </li>
                <li>
                    <label><spring:message code="county"/></label>
                    <input type="text" name="county" value="${quotationRequest.county}" tabindex="10" />
                </li>
                <li>
                    <label><spring:message code="postcode"/></label>
                    <input type="text" name="postcode" value="${quotationRequest.postcode}" tabindex="11" />
                    <span class="error highlight" hidden></span>
                </li>
                <li>
                    <label><spring:message code="addjob.transportin"/></label>
                    <select name="transportinId" tabindex="13">
                        <option value="0">---</option>'
                        <c:forEach var="transportOption" items="${transportOptions}">
                            <option value="${transportOption.key}">${transportOption.value}</option>
                        </c:forEach>
                    </select>
                </li>
                <li>
                    <label><spring:message code="addjob.transportout"/></label>
                    <select name="transportoutId" tabindex="14">
                        <option value="0">---</option>'
                        <c:forEach var="transportOption" items="${transportOptions}">
                            <option value="${transportOption.key}">${transportOption.value}</option>
                        </c:forEach>
                    </select>
                </li>
                <c:set var="nameParts" value="${fn:split(quotationRequest.contact, ' ')}"/>
                <li>
                    <label><spring:message code="company.firstname"/></label>
                    <input type="text" name="firstname" value="${nameParts[0]}" tabindex="15" />
                    <span class="error highlight" hidden></span>
                </li>
                <li>
                    <label><spring:message code="company.lastname"/></label>
                    <input type="text" name="lastname" value="${nameParts[1]}" tabindex="16" />
                </li>
                <li>
                    <label><spring:message code="telephone"/></label>
                    <input type="text" name="telephone" value="${quotationRequest.phone}" tabindex="17" />
                    <span class="error highlight" hidden></span>
                </li>
                <li>
                    <label><spring:message code="mobile"/></label>
                    <input type="text" name="mobile" value="" tabindex="18" />
                </li>
                <li>
                    <label><spring:message code="email"/></label>
                    <input type="text" name="email" size="40" value="${quotationRequest.email}" tabindex="19" />
                    <span class="error highlight" hidden></span>
                </li>
                <li>
                    <label><spring:message code="company.fax"/></label>
                    <input type="text" name="fax" value="${quotationRequest.fax}" tabindex="20" />
                </li>
                <li>
                    <label>&nbsp;</label>
                    <button type="button" id="addfreehand"><spring:message code='submit'/></button>
                </li>
            </ol>
        </fieldset>
    </form:form>
</div>

