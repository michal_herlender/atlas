<%@ tag language="java" display-name="finance" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%@ attribute name="company" required="true" type="java.lang.Object" %>
<%@ attribute name="companysettings" required="true" type="java.lang.Object" %>
<%@ attribute name="updateFinance" required="true" type="java.lang.Object" %>
<%@ attribute name="accountsSoftwareSupported" required="true" type="java.lang.Object" %>
<%@ attribute name="defaultcurrency" required="true" type="java.lang.Object" %>

<table class="default2" id="finance">
	<thead>
		<tr>
			<td colspan="4">
				<spring:message code="company.financesettings"/>
				<c:if test="${updateFinance}">
					<c:set var="permission" value=""/>
					<c:choose>
						<c:when test="${company.companyRole.name() == 'BUSINESS'}">
							<c:set var="permission" value="COMPANY_FINANCE_EDIT_BUSINESS"/>
						</c:when>
						<c:otherwise>
							<c:set var="permission" value="COMPANY_FINANCE_EDIT_NON_BUSINESS"/>
						</c:otherwise>
					</c:choose>
					<c:if test="${not empty permission}">
						<cwms:securedLink permission="${permission}" collapse="True" classAttr="mainlink-float" parameter="?coid=${company.coid}">
							<spring:message code="company.editfinancesettings"/>
						</cwms:securedLink>
					</c:if>
				</c:if>
			</td>
		</tr>
		<tr>
			<th class="name" scope="col"><spring:message code="name"/></th>
			<th class="value" scope="col"><spring:message code="company.value"/></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td colspan="4">
				<strong><spring:message code="company.generalsettings" /></strong>
			</td>
		</tr>
		<tr class="odd">
			<td>
				<image:globalSetting/>
				<spring:message code="company.legalid" />
			</td>
			<td><c:out value="${company.legalIdentifier}"/></td>
		</tr>
		<tr class="odd">
			<td>
				<image:globalSetting/>
				<spring:message code="company.fiscalid" />
			</td>
			<td><c:out value="${company.fiscalIdentifier}"/></td>
		</tr>
		<tr class="odd">
			<td>
				<image:businessCompanySpecific/>
				<spring:message code="company.status" />
			</td>
			<td><c:out value="${companysettings.status.getMessage()}"/></td>
		</tr>
		<tr class="odd">
			<td>
				<image:businessCompanySpecific/>
				<spring:message code="company.active" />
			</td>
			<td>
				<spring:message code="${companysettings.active}"/>
			</td>
		</tr>
		<tr class="odd">
			<td>
				<image:businessCompanySpecific/>
				<spring:message code="company.onstop" />
			</td>
			<td>
				<spring:message code="${companysettings.onStop}"/>
			</td>
		</tr>
		<tr class="odd">
			<td>
				<image:businessCompanySpecific/>
				<spring:message code="invoicefrequency" />
			</td>
			<td>
				<c:if test="${not empty companysettings.invoiceFrequency}">
					<c:out value="${companysettings.invoiceFrequency.code}"/>
					-
					<cwms:besttranslation translations="${companysettings.invoiceFrequency.translations}"/>
				</c:if>
			</td>
		</tr>
		<tr class="odd">
			<td>
				<image:businessCompanySpecific/>
				<spring:message code="paymentmode" />
			</td>
			<td>
				<c:if test="${not empty companysettings.paymentMode}">
					<c:out value="${companysettings.paymentMode.code}"/>
					-
					<cwms:besttranslation translations="${companysettings.paymentMode.translations}"/>
				</c:if>
			</td>
		</tr>
		<tr class="odd">
			<td>
				<image:businessCompanySpecific/>
				<spring:message code="paymentterms" />
			</td>
			<td>
				<c:out value="${companysettings.paymentterm.getMessage()}"/>
			</td>
		</tr>
		<tr class="odd">
			<td>
				<image:businessCompanySpecific/>
				<spring:message code="factoring" />
			</td>
			<td>
				<c:choose>
					<c:when test="${companysettings.invoiceFactoring}">
						<spring:message code="yes"/>
					</c:when>
					<c:otherwise>
						<spring:message code="no"/>
					</c:otherwise>
				</c:choose>
			</td>
		</tr>
		<c:if test="${company.companyRole.name() == 'BUSINESS'}">
			<tr>
				<td colspan="4">
					<strong>
						<spring:message code="company.companysettings" /> - <c:out value="${company.companyRole.getMessage()}"/>
					</strong>
				</td>
			</tr>
			<tr class="odd">
				<td>
					<image:globalSetting/>
					<spring:message code="company.code" />
				</td>
				<td>
					<c:out value="${company.companyCode}"/>
				</td>
			</tr>		
			<tr class="odd">
				<td>
					<image:globalSetting/>
					<spring:message code="company.capital" />
				</td>
				<td>
					<c:out value="${company.businessSettings.capital}"/>
				</td>
			</tr>
			<tr class="odd">
				<td>
					<image:globalSetting/>
					<spring:message code="company.useTaxableOptionOnOurchaseOrders" />
				</td>
				<td>
					<spring:message code="${company.businessSettings.useTaxableOption}"/>
				</td>
			</tr>
			<tr class="odd">
				<td>
					<image:globalSetting/>
					<spring:message code="company.defaultquotationduration" />
				</td>
				<td>
					<c:out value="${company.businessSettings.defaultQuotationDuration}"/>
					<spring:message code="days" />
				</td>
			</tr>
			<tr class="odd">
				<td>
					<image:globalSetting/>
					<spring:message code="jobcost.hourlyrate" /> - <spring:message code="pricing" />
				</td>
				<td>
					<c:if test="${not empty company.businessSettings.hourlyRate}">
						<c:choose>
							<c:when test="${not empty company.currency}">
								<c:out value="${company.currency.currencySymbol}"/>
							</c:when>
							<c:otherwise>
								<c:out value="${defaultcurrency.currencySymbol}"/>
							</c:otherwise>
						</c:choose>
						<c:out value="${company.businessSettings.hourlyRate}"/>
					</c:if>
				</td>
			</tr>
			<tr class="odd">
				<td>
					<image:globalSetting/>
					<spring:message code="company.usepricecatalog" />
				</td>
				<td>
					<c:choose>
						<c:when test="${not empty company.businessSettings}">
							<spring:message code="${company.businessSettings.usePriceCatalog}" />
						</c:when>
						<c:otherwise>
							<c:out value="na"/>
						</c:otherwise>
					</c:choose>
				</td>
			</tr>			
			<tr class="odd">
				<td>
					<image:globalSetting/>
					<spring:message code="company.legalregistrationline1" />
				</td>
				<td>
					<c:out value="${company.businessSettings.legalRegistration1}"/>
				</td>
			</tr>		
			<tr class="odd">
				<td>
					<image:globalSetting/>
					<spring:message code="company.legalregistrationline2" />
				</td>
				<td>
					<c:out value="${company.businessSettings.legalRegistration2}"/>
				</td>
			</tr>		
			<tr class="odd">
				<td>
					<image:globalSetting/>
					<spring:message code="company.logo" />
				</td>
				<td>
					<c:out value="${company.businessSettings.logo}"/>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<strong>
						<spring:message code="company.companysettings" /> - <spring:message code="company.intersubdivisioncosting" />
					</strong>
				</td>
			</tr>
			<tr class="odd">
				<td>
					<image:globalSetting/>
					<spring:message code="company.interbranchsalesmode" />
				</td>
				<td>
					<c:out value="${company.businessSettings.interbranchSalesMode.message}"/>
				</td>
			</tr>
			<tr class="odd">
				<td>
					<image:globalSetting/>
					<spring:message code="company.interbranchdiscountrate" />
				</td>
				<td>
					<c:choose>
						<c:when test="${not empty company.businessSettings.interbranchDiscountRate}">
							<c:out value="${company.businessSettings.interbranchDiscountRate}"/> %
						</c:when>
						<c:otherwise>
							<c:out value="na"/>
						</c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr class="odd">
				<td>
					<image:globalSetting/>
					<spring:message code="company.interbranchmarkuprate" />
				</td>
				<td>
					<c:choose>
						<c:when test="${not empty company.businessSettings.interbranchMarkupRate}">
							<c:out value="${company.businessSettings.interbranchMarkupRate}"/> %
						</c:when>
						<c:otherwise>
							<spring:message code="notapplicable"/>
						</c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<strong>
						<spring:message code="company.subdivisionsettings" /> - <spring:message code="costing" />
					</strong>
				</td>
			</tr>
			<c:forEach var="subdiv" items="${company.subdivisions}">
				<tr class="odd">
					<c:if test="${subdiv.active eq true }">
						<td>
							<image:globalSetting/>
							<spring:message code="company.ebitdahourlycostrate" /> - <c:out value="${subdiv.subdivCode}"/>
						</td>
						<td>
						<c:choose>
							<c:when test="${not empty subdiv.businessSettings.ebitdaHourlyCostRate}">
								<c:choose>
									<c:when test="${not empty company.currency}">
										<c:out value="${company.currency.currencySymbol}"/>
									</c:when>
									<c:otherwise>
										<c:out value="${defaultcurrency.currencySymbol}"/>
									</c:otherwise>
								</c:choose>
								<c:out value="${subdiv.businessSettings.ebitdaHourlyCostRate}"/>
							</c:when>
							<c:otherwise>
								<spring:message code="notapplicable"/>
							</c:otherwise>
						</c:choose>
						</td>
					</c:if>
				</tr>
			</c:forEach>
			<tr>
				<td colspan="4">
					<strong>
						<spring:message code="company.subdivisionsettings" /> - <spring:message code="systemdefault.defaultSettings" />
					</strong>
				</td>
			</tr>
			<c:forEach var="subdiv" items="${company.subdivisions}">
				<c:if test="${subdiv.active eq true }">
					<tr class="odd">
						<td>
							<image:globalSetting/>
							<spring:message code="company.defaultturnaround" /> - <c:out value="${subdiv.subdivCode}"/>
						</td>
						<td>
						<c:choose>
							<c:when test="${not empty subdiv.businessSettings.defaultTurnaround}">
								<c:out value="${subdiv.businessSettings.defaultTurnaround}"/>
							</c:when>
							<c:otherwise>
								<spring:message code="notapplicable"/>
							</c:otherwise>
						</c:choose>
						</td>
					</tr>
				</c:if>
			</c:forEach>
		</c:if>
	</tbody>
</table>
<div class="clear">
</div>
<%-- 
     Important Note: The following link is ONLY for the single party Access Dimensions accounting system settings 
     which are NOT supported in the current build of Crocodile.  The functionality is preserved as-is for future use
     or evolution if the current code base if used for an Antech deployment.  accountsSoftwareSupported = false normally.     
--%>
<c:if test="${updateFinance && accountsSoftwareSupported}">
	<div class="float-left">
		<a href='<c:url value="/companyaccounts.htm?coid=${company.coid}"/>' class="mainlink-float"><spring:message code="company.editaccessdimensionsbrokering"/></a>
	</div>
</c:if>
<div class="clear">
</div>