<%@ tag language="java" display-name="usergroups" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ attribute name="groups" required="true" type="java.lang.Object" %>
<%@ attribute name="company" required="true" type="java.lang.Object" %>

<table class="default2 companyUserGroups">
	<thead>
		<tr>
			<td colspan="5">
				<spring:message code="company.usergroups"/>: ${groups.size()}
			</td>
		</tr>
		<tr>
			<th class=""><spring:message code="default"/></th>
			<th class=""><spring:message code="name"/></th>
			<th class=""><spring:message code="description"/></th>
			<th class=""></th>
			<th class=""></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="5">
				<cwms:securedLink permission="USER_GROUP_ADD_EDIT"  collapse="True"  classAttr="mainlink-float" parameter="?coid=${company.coid}">
					<spring:message code="company.newusergroup"/>
				</cwms:securedLink>
			</td>
		</tr>
	</tfoot>
	<tbody>
		<c:forEach var="usergroup" items="${groups}">
		<tr>
			<td class=""><spring:message code="${usergroup.systemDefault}"/></td>
			<c:choose>
				<%-- access groups associated with specific companies have been added by users in their own language there is no translation --%>
				<c:when test="${usergroup.company == null }">
					<td class=""><cwms:besttranslation translations="${usergroup.nameTranslation}"/></td>
					<td class=""><cwms:besttranslation translations="${usergroup.descriptionTranslation}"/></td>
				</c:when>
				<c:otherwise>
					<td class="">${usergroup.name}</td>
					<td class="">${usergroup.description}</td>
				</c:otherwise>
			</c:choose>
			<td class="">
					<table class="default2" style=" margin: 0; " summary="">
				
					<thead>
						<tr>
							<th width="50%" class=""><spring:message code="company.property"/></th>
							<th width="50%" class=""><spring:message code="company.value"/></th>
						</tr>
					</thead>
																		
					<tbody>
						<c:forEach var="property" items="${usergroup.properties}">
							<tr>
								<td>
									<!-- hover over this anchor and a tooltip is displayed showing the contents of the div created below -->
									<c:set var="uniqueid" value="${usergroup.groupid}_${property.userGroupProperty.ordinal()}"/>
									<a href="?id=#ugp${uniqueid}&amp;width=250" onclick=" return false; " id="tip${uniqueid}" class="jTip comment">${property.userGroupProperty.getName()}</a>
									<div id="ugp${uniqueid}" class="hid">
										${property.userGroupProperty.getDescription()}
									</div>
								</td>
								<td>
									${property.scope.getName()}
								</td>
							</tr>
						</c:forEach>
					</tbody>
					
					</table>
			</td>
			<td class="">
				<c:if test="${not usergroup.systemDefault && not empty usergroup.company}">
					<div>
						<p>
							<cwms:securedLink permission="USER_GROUP_ADD_EDIT"  collapse="True"  classAttr="mainlink" parameter="?groupid=${usergroup.groupid}">
								<spring:message code="edit"/>
							</cwms:securedLink>
							<cwms:securedLink permission="GROUP_DELETE"  collapse="True"  classAttr="mainlink" parameter="?groupid=${usergroup.groupid}">
								<spring:message code="delete"/>
							</cwms:securedLink>
						</p>
					</div>
				</c:if>
			</td>
		</tr>
		</c:forEach>
	</tbody>
</table>