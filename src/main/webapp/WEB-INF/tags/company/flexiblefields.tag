<%@ tag language="java" display-name="finance" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%@ attribute name="company" required="true" type="java.lang.Object" %>
<%@ attribute name="flexiblefields" required="true" type="java.lang.Object" %>

<table class="default2" id="flexiblefields">
	<thead>
		<tr>
			<td colspan="4">
				<spring:message code="company.flexiblefields"/>
				<cwms:securedLink permission="COMPANY_FLEXIBLE_FIELD_ADD_EDIT"  collapse="True"  classAttr="mainlink-float" parameter="?coid=${company.coid}">
					<spring:message code="company.addflexiblefield"/>
				</cwms:securedLink>
			</td>
		</tr>
		<tr>
			<th class="name" scope="col"><spring:message code="name"/></th>
			<th class="type" scope="col"><spring:message code="type"/></th>
			<th class="values" scope="col"><spring:message code="company.acceptablevalues"/></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<c:choose>
			<c:when test="${flexiblefields == null}">
				<tr><td colspan="4"><spring:message code="viewinstrument.noflexiblefields"/></td></tr>
			</c:when>
			<c:otherwise>
				<c:forEach var="flexiblefield" items="${flexiblefields}" varStatus="forEachStatus">
					<c:set var="rowClass" value="odd"/>
					<c:if test="${forEachStatus.index % 2 == 0}">
						<c:set var="rowClass" value="even"/>
					</c:if>
					<tr class="${rowClass}">
						<td>${flexiblefield.name}</td>
						<td>${flexiblefield.fieldType.getName()}</td>
						<c:choose>
							<c:when test="${flexiblefield.getInstrumentFieldLibraryValues() != null && flexiblefield.getInstrumentFieldLibraryValues().size() > 0}">
								<td>
									<c:forEach var="libraryValue" items="${flexiblefield.getInstrumentFieldLibraryValues()}">
										${libraryValue.name}<br>
									</c:forEach>
								</td>
							</c:when>
							<c:otherwise>
								<td>&nbsp;</td>
							</c:otherwise>
						</c:choose>
						<td>
							<cwms:securedLink permission="COMPANY_FLEXIBLE_FIELD_ADD_EDIT"  collapse="True"  classAttr="mainlink-float" parameter="?fielddefid=${flexiblefield.instrumentFieldDefinitionid}">
								<spring:message code="edit"/>
							</cwms:securedLink>
						</td>
					</tr>
				</c:forEach>
			</c:otherwise>
		</c:choose>
	</tbody>
</table>
<div class="clear">
</div>
