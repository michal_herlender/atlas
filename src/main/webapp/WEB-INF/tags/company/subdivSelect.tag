<%@ tag language="java" display-name="subdivSelect" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="availableSubdivs" required="true" type="java.util.Map" %>
<%@ attribute name="selectedSubdivId" required="true" type="java.lang.Integer" %>
<%@ attribute name="id" required="false" type="java.lang.String" %>
<%@ attribute name="name" required="true" type="java.lang.String" %>
<%@ attribute name="style" required="false" type="java.lang.String" %>
<%@ attribute name="onchange" required="false" type="java.lang.String" %>

<select style="${style}" id="${id}" name="${name}" onchange="${onchange}">
	<c:forEach var="company" items="${availableSubdivs.keySet()}">
		<optGroup style="color: black" label="${company.value}">
			<c:forEach var="subdiv" items="${availableSubdivs.get(company)}">
				<c:choose>
					<c:when test="${subdiv.key == selectedSubdivId}">
						<option style="color: black" value="${subdiv.key}" selected>${subdiv.value}</option>
					</c:when>
					<c:otherwise>
						<option style="color: black" value="${subdiv.key}">${subdiv.value}</option>
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</optGroup>
	</c:forEach>
</select>