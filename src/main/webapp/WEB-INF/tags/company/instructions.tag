<%@ tag language="java" display-name="instructions" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="command" required="true" type="java.lang.Object" %>

<div class="infobox" id="viewinstructions">
	<div class="float-left">
		<c:choose>
			<c:when test="${command.instructionEntity == 'SUBDIV'}">
				<spring:message code="company.companyinstructions"/>: <a href="viewinstructions.htm?coid=${command.coid}" class="mainlink">${command.companyInstructionCount}</a>
			</c:when>
			<c:when test="${command.instructionEntity == 'CONTACT'}">
				<spring:message code="company.subdivinstructions"/>: <a href="viewinstructions.htm?subdivid=${command.subdivid}" class="mainlink">${command.subdivInstructionCount}</a>
				<spring:message code="company.companyinstructions"/>: <a href="viewinstructions.htm?coid=${command.coid}" class="mainlink">${command.companyInstructionCount}</a>
			</c:when>
		</c:choose>
	</div>
						
	<c:choose>
		<c:when test="${command.instructionEntity == 'COMPANY'}">
		<cwms:securedLink permission="INSTRUCTION_ADD"  collapse="True"  classAttr="mainlink-float" parameter="?coid=${command.coid}">
			<spring:message code="company.newinstruction"/>
		</cwms:securedLink>
		</c:when>
		<c:when test="${command.instructionEntity == 'SUBDIV'}">
		<cwms:securedLink permission="INSTRUCTION_ADD" classAttr="mainlink-float"  collapse="True"  parameter="?subdivid=${command.subdivid}">
			<spring:message code="company.newinstruction"/>
		</cwms:securedLink>
		</c:when>
		<c:when test="${command.instructionEntity == 'CONTACT'}">
		<cwms:securedLink permission="INSTRUCTION_ADD" classAttr="mainlink-float"  collapse="True"  parameter="?personid=${command.personid}">
			<spring:message code="company.newinstruction"/>
		</cwms:securedLink>
		</c:when>
	</c:choose>

	<!--  clear floats and restore page flow  -->
	<div class="clear-0"></div>
	
	<div id="instructionHead">
		<h5>
			<c:choose>
				<c:when test="${command.dtos.isEmpty()}">
					<spring:message code="company.therearecurrentlynoinstructionstodisplay"/>
				</c:when>
				<c:otherwise>
					<c:choose>
						<c:when test="${command.dtos.size() == 1}">
							<spring:message code="company.thereiscurrently"/>
						</c:when>
						<c:otherwise>
							<spring:message code="company.therearecurrently"/>
						</c:otherwise>
					</c:choose>
					 &nbsp;${command.dtos.size()}&nbsp;
					 <spring:message code="company.instructiondefinedfor"/>&nbsp;
					<c:choose>
						<c:when test="${command.instructionEntity == 'COMPANY'}">
							<a href="viewcomp.htm?coid=${command.coid}" class="mainlink" title="${command.companyName}">${command.companyName}</a>
						</c:when>
						<c:when test="${command.instructionEntity == 'SUBDIV'}">
							<a href="viewsub.htm?subdivid=${command.subdivid}" class="mainlink" title="${command.subdivName}">${command.subdivName}</a>
						</c:when>
						<c:when test="${command.instructionEntity == 'CONTACT'}">
							<a href="viewperson.htm?personid=${command.personid}" class="mainlink" title="${command.contactName}">${command.contactName}</a>
						</c:when>
					</c:choose>						 
				</c:otherwise>
			</c:choose>
		</h5>
	</div>
	
	<fieldset>
	<legend>
		<spring:message code="company.instructionsfor"/>&nbsp;${command.entityName}
	</legend>
		
	<ol>
	
	<c:set var="displayClass" value="hid"></c:set>
	<c:if test="${command.dtos.isEmpty()}">
		<c:set var="displayClass" value="vis"></c:set>
	</c:if>
			 									
	<li id="instructionsEmpty" class="${displayClass}">
		<div class="center bold"><spring:message code="company.noinstructionstodisplay"/></div>
	</li>
	
	<c:set var="isCompanyOrSubdiv" value="${command.instructionEntity == 'COMPANY' || command.instructionEntity == 'SUBDIV'}" />
	<c:forEach var="dto" items="${command.dtos}">
		<c:set var="instruction" value="${dto.instruction}"/>
		<li id="instruction${instruction.instructionid}">
			<!-- this div is floated left and displays group name and description -->
			<div class="notes_note instructions_note">
				<c:choose>
					<c:when test="${dto.globalInstruction}">
						<image:globalSetting/>
					</c:when>
					<c:when test="${dto.subdivInstruction }">
						<image:businessSubdivisionSpecific/>
					</c:when>
					<c:otherwise>
						<image:businessCompanySpecific/>
					</c:otherwise>
				</c:choose>
				<strong>
					<c:if test="${dto.subdivInstruction }" >[<c:out value="${dto.businessSubdivName}"/>] </c:if>
					<spring:message code="${dto.instructionEntity.messageCode}"/> :
					(${dto.entityName}) -
					<spring:message code="${instruction.instructiontype.messageCode}"/>:
				</strong><br />
				<c:out value="${instruction.instruction}"/>
			</div>
			<!-- end float left div -->
			<div class="notes_editor">
				<c:if test="${not empty instruction.lastModifiedBy}">
					${instruction.lastModifiedBy.name}
				</c:if>
				<br/><fmt:formatDate value="${instruction.lastModified}" pattern="dd.MM.yyyy - HH:mm:ss"/>
			</div>
			<!-- this div is floated right and displays the edit and delete links -->
			<div class="notes_edit instructions_edit">
				<cwms:securedLink permission="INSTRUCTION_EDIT"  collapse="True"  classAttr="imagelink" parameter="?linkid=${dto.linkid}&entity=${dto.instructionEntity}">
							<img src="img/icons/note_edit.png" width="16" height="16" alt="<spring:message code="company.editinstruction"/>" title="<spring:message code="company.editinstruction"/>" />
				</cwms:securedLink>			
				<cwms:securedLink permission="INSTRUCTION_DELETE"  collapse="True"  classAttr="imagelink" parameter="?linkid=${dto.linkid}&entity=${dto.instructionEntity}">
						<img src="img/icons/note_delete.png" width="16" height="16" alt="<spring:message code="company.deleteinstruction"/>" title="<spring:message code="company.deleteinstruction"/>" />
				</cwms:securedLink>		
			
			</div>
			<!--  end of float right  div -->
			
			<!--  clear floats and restore page flow  -->
			<div class="clear"></div>
		</li>										
	</c:forEach>
</ol>
</fieldset>
</div>