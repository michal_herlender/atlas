<%@ tag language="java" display-name="bankaccounts" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ attribute name="bankaccounts" required="true" type="java.lang.Object" %>
<%@ attribute name="company" required="true" type="java.lang.Object" %>

<table class="default2 companyBankAccounts">
	<thead>
		<tr>
			<td colspan="7">
				<spring:message code="company.bankaccounts"/>: ${bankaccounts.size()}
			</td>
		</tr>
		<tr>
			<th class=""><spring:message code="bankaccount.iban"/></th>
			<th class=""><spring:message code="bankaccount.swiftbic"/></th>
			<th class=""><spring:message code="company.currency"/></th>
			<th class=""><spring:message code="bankaccount.bankname"/></th>
			<th class=""><spring:message code="bankaccount.localaccountnumber"/></th>
			<th><spring:message code="bankaccount.usage"/></th>
			<th class=""></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="7">
			<cwms:securedLink permission="BANK_ACCOUNT_ADD" collapse="True" classAttr="mainlink-float" parameter="?companyId=${company.coid}">
				<spring:message code="company.addbankaccount"/>
			</cwms:securedLink>

			</td>
		</tr>
	</tfoot>
	<tbody>
		<c:forEach var="bankaccount" items="${bankaccounts}">
		<tr>
			<td class=""><c:out value="${bankaccount.iban}"/></td>
			<td class=""><c:out value="${bankaccount.swiftBic}"/></td>
			<td class=""><c:out value="${bankaccount.currency.currencyCode}"/></td>
			<td class=""><c:out value="${bankaccount.bankName}"/></td>
			<td class=""><c:out value="${bankaccount.accountNo}"/></td>
			<td>
				<c:if test="${bankaccount.includeOnFactoredInvoices}"><spring:message code="bankaccount.factoredInvoices"/></c:if><br>
				<c:if test="${bankaccount.includeOnNonFactoredInvoices}"><spring:message code="bankaccount.nonFactoredInvoices"/></c:if>
			</td>
			<td class="">
					<div>
						<p>
							<cwms:securedLink permission="BANK_ACCOUNT_EDIT"  collapse="True"  classAttr="mainlink" parameter="?accountId=${bankaccount.id}&companyId=${company.coid}">
								<spring:message code="edit"/>
							</cwms:securedLink>
						</p>
						<p>
							<cwms:securedJsLink permission="BANK_ACCOUNT_DELETE"  collapse="True"  classAttr="mainlink" onClick=" event.preventDefault(); deleteBankAccount(${bankaccount.id}, ${company.coid})" >
								<spring:message code="delete"/>
							</cwms:securedJsLink>
				
						</p>
					</div>
			</td>
		</tr>
		</c:forEach>
	</tbody>
</table>