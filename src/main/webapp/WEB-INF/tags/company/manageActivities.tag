<%@tag import="java.util.Locale"%>
<%@ tag language="java" display-name="manageActivities" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ attribute name="activities" required="true" type="java.lang.Object"%>
<%@ attribute name="subdivid" required="true" type="java.lang.Integer"%>
<%@ attribute name="itemactivitytype" required="true" type="java.lang.Object"%>


<div id="subdivmanageactivities" class="infobox">
	<fieldset>
		<form:form method="post" id="subdiv_manage_act_form" action="subdivmanageactivities.htm">
				<input type="hidden" name="subdivid" id="subdivid" value="${ subdivid }"/>
					<ol>
						
						<li>
							<table id="table_manageactivities" class="default2"
									summary="This table lists all activities">	
								<thead>
									<tr>
										<th class="codefault"><spring:message code="manageactivities.table.englishname" /></th>
										<th class="codefault"><spring:message code="manageactivities.table.spanishname" /></th>
										<th class="codefault"><spring:message code="manageactivities.table.frenchname" /></th>
										<th class="codefault"><spring:message code="manageactivities.table.typeofactivity" /></th>
										<th class="codefault"><spring:message code="manageactivities.table.istransferredtoadveso" /></th>
										<th class="codefault"><spring:message code="manageactivities.table.isdisplayedonjoborderdetails" /></th>
										<th class="codefault"><spring:message code="manageactivities.table.action" /></th>
									</tr>
								</thead>
								<tbody>
									    <c:forEach items="${ activities }" var="act">
										    <c:if test="${ act.advesoTransferableActivity.itemActivity.active == true }">
										    	<c:set var="localeEnglish" value="<%=new Locale(\"en\", \"GB\")%>"/>
											    <c:set var="localeSpanish" value="<%=new Locale(\"es\", \"ES\")%>"/>
											    <c:set var="localeFrench" value="<%=new Locale(\"fr\", \"FR\")%>"/>
											    
										    	<c:set var="localeEnglishTrans" value="${ act.advesoTransferableActivity.itemActivity.translations.stream().filter(t -> t.getLocale().equals(localeEnglish)).findFirst().orElse(null) }"/>
											    <c:set var="localeSpanishTrans" value="${ act.advesoTransferableActivity.itemActivity.translations.stream().filter(t -> t.getLocale().equals(localeSpanish)).findFirst().orElse(null) }"/>
											    <c:set var="localeFrenchTrans" value="${ act.advesoTransferableActivity.itemActivity.translations.stream().filter(t -> t.getLocale().equals(localeFrench)).findFirst().orElse(null) }"/>
										    	
										    	<tr>
													<td>${ localeEnglishTrans.translation }</td>
													<td>${ localeSpanishTrans.translation }</td>
													<td>${ localeFrenchTrans.translation }</td>
													<td>${ act.advesoTransferableActivity.itemActivity.type }</td>
													<input type="hidden" name="idOverridden_${ act.advesoTransferableActivity.id }" value="${ act.id }"/>
													<input type="hidden" name="idTranferrable" value="${ act.advesoTransferableActivity.id }"/>
													<td align="center"><input type="checkbox" name="istrans_${ act.advesoTransferableActivity.id }" ${ act.isTransferredToAdveso ? 'checked':'' }/></td>
													<td align="center"><input type="checkbox" name="isdisp_${ act.advesoTransferableActivity.id }" ${ act.isDisplayedOnJobOrderDetails ? 'checked':'' }/></td>
													<td align="center">
														<a href="#" onclick=" event.preventDefault(); deleteOActivity(${act.id}, ${ subdivid });"><spring:message code="manageactivities.buttondelete" /></a>
													</td>
												</tr>
											</c:if>
									</c:forEach>
								</tbody>
								<tfoot>
									<tr>
										<td colspan="7">
											<a href="#" class="domthickbox mainlink-float" onclick=" event.preventDefault(); addNewActivity('<spring:message code="manageactivities.prompt.type" />','<spring:message code="manageactivities.prompt.activity" />','<spring:message code="manageactivities.prompt.istransferable" />','<spring:message code="manageactivities.prompt.isdisplayed" />');" title="">
												<spring:message code="manageactivities.buttonadd" /></a>
										</td>
									</tr>
								</tfoot>
							</table>
						</li>
						
						<li>
							<div class="displaycolumn-15">
								<button onclick="sendData()">
									<spring:message code='manageactivities.buttonsave'/> 
								</button>
							</div>
						</li>
					</ol>	
				</form:form>						
			</fieldset>
		</div>