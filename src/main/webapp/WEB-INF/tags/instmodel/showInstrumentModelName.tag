<%@ tag language="java" display-name="showInstrumentModelName" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%@ attribute name="instrument" required="true" type="java.lang.Object" %>
<%@ attribute name="caltypeid" required="true" type="java.lang.Object" %>
<%@ attribute name="relativePathToImages" required="true" type="java.lang.Object" %>

<c:if test="${instrument.calibrationStandard}">
	<img src="${relativePathToImages}img/icons/bullet-standard.png" width="12" height="12" class="image_inl" alt="Standard" title="Standard" />
</c:if>
<cwms:besttranslation translations="${instrument.model.nameTranslations}"/>