<%@ tag language="java" display-name="showInstrumentModelLink" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<%@ attribute name="instrumentcalibrationstandard" required="true" type="java.lang.Object" %>
<%@ attribute name="instrumentscrapped" required="true" type="java.lang.Object" %>
<%@ attribute name="instrumentmodelid" required="true" type="java.lang.Object" %>
<%@ attribute name="modelnametranslation" required="true" type="java.lang.Object" %>
<%@ attribute name="instrumentmodelname" required="true" type="java.lang.Object" %>
<%@ attribute name="instrumentsubfamilytypology" required="true" type="java.lang.Object" %>
<%@ attribute name="instrumentmodelmfrtype" required="true" type="java.lang.Object" %>
<%@ attribute name="instrumentmfrname" required="true" type="java.lang.Object" %>
<%@ attribute name="instrumentgenericmfr" required="true" type="java.lang.Object" %>

<c:if test="${instrumentcalibrationstandard}">
	<img src="img/icons/bullet-standard.png" width="12" height="12" class="image_inl" alt="Standard" title="Standard" />
</c:if>
<c:if test="${instrumentscrapped}">
	<img src="img/icons/bin_closed.png" width="16" height="16" class="image_inl" alt="Unit B.E.R." title="Unit B.E.R." />
</c:if>
<c:choose>
	<c:when test="${instrumentscrapped}">
		<t:jobCostingViewShowModelLink instrumentmodelid="${instrumentmodelid}" classValue="mainlink-strike"
		 modelnametranslation="${modelnametranslation}" instrumentmodelname="${instrumentmodelname}"
		 instrumentsubfamilytypology="${instrumentsubfamilytypology}" instrumentmodelmfrtype="${instrumentmodelmfrtype}"
		 instrumentmfrname="${instrumentgenericmfr}" instrumentgenericmfr="${instrumentgenericmfr}"/>
	</c:when>
	<c:otherwise>
		<t:jobCostingViewShowModelLink instrumentmodelid="${instrumentmodelid}"
		 modelnametranslation="${modelnametranslation}" instrumentmodelname="${instrumentmodelname}"
		 instrumentsubfamilytypology="${instrumentsubfamilytypology}" instrumentmodelmfrtype="${instrumentmodelmfrtype}"
		 instrumentmfrname="${instrumentgenericmfr}" instrumentgenericmfr="${instrumentgenericmfr}"/>
	</c:otherwise>
</c:choose>
&nbsp;