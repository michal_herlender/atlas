<%@ tag language="java" display-name="instrumentModelTable" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ attribute name="model" required="true" type="java.lang.Object" %>

<c:if test="${model.quarantined}">
	<div class="warningBox1">
		<div class="warningBox2">
			<div class="warningBox3">
				<span class="bold"><spring:message code="viewmod.attentionquarantined"/></span>
			</div>
		</div>
	</div>
</c:if>

<div class="infobox">
	<fieldset class="relative">
		<legend><t:showViewModelLink instrumentmodel="${model}"/></legend>
			<div class="displaycolumn">
				<div class="defaultEntityImage imgbox">
					
					<c:choose>
						<c:when test="${not empty model.images}">
							<c:forEach var="image" items="${model.images}" end="0">
								<img src="displaythumbnail?id=${image.id}" alt="" title="" />
							</c:forEach>
						</c:when>
						<c:otherwise>
							<img src="img/noimage.png" width="150" height="113" alt="<spring:message code="viewmod.noimageavailable"/>" title="<spring:message code="viewmod.noimageavailable"/>" class="noImage" />
						</c:otherwise>
					</c:choose>
					
				</div>
				<ol>	
					<li>
						<label><spring:message code="domain"/>:</label>
						<span><t:showTranslationOrDefault translations="${model.description.family.domain.translation}" defaultLocale="${defaultlocale}"/></span>
					</li>
					<li>
						<label><spring:message code="family"/>:</label>
						<span><t:showTranslationOrDefault translations="${model.description.family.translation}" defaultLocale="${defaultlocale}"/></span>
					</li>
					<li>
						<label><spring:message code="sub-family"/>:</label>
						<span><t:showTranslationOrDefault translations="${model.description.translations}" defaultLocale="${defaultlocale}"/></span>
					</li>											
					<li>
						<label><spring:message code="salescategory"/>:</label>
						<span><t:showTranslationOrDefault translations="${model.salesCategory.translations}" defaultLocale="${defaultlocale}"/></span>
					</li>
					<li>
						<label><spring:message code="instmod.manufacturer"/>:</label>
						<span>
<%-- 							<c:choose> --%>
<%-- 								<c:when test="${model.modelMfrType == 'MFR_GENERIC'}"><spring:message code="instmod.generic"/></c:when> --%>
<%-- 								<c:otherwise><c:out value="${model.mfr.name}"/></c:otherwise> --%>
<%-- 							</c:choose> --%>
							<c:out value="${model.mfr.name}"/>
						</span>
					</li>	
					<li>
						<label><spring:message code="instmod.model"/></label>
						<c:out value="${model.model}"/>
					</li>
					<li>
						<label><spring:message code="quarantined"/>:</label>
						<span>
							<c:choose>
								<c:when test="${model.quarantined}"><spring:message code="yes"/></c:when>
								<c:otherwise><spring:message code="nobool"/></c:otherwise>
							</c:choose>
						</span>
					</li>
																												
				</ol>
			</div>
			<div class="displaycolumn">
				<ol>
					<li>
						<label><spring:message code="instmod.addedby"/>:</label>
						<span>
							<links:contactLinkDWRInfo contact="${model.addedBy}" rowcount="1"/>
							<fmt:formatDate value="${model.addedOn}" pattern="hh:mm:ss - dd.MM.yyyy"/>
						</span>
					</li>
					<li>
						<label><spring:message code="instmod.lastmodified"/>:</label>
						<span>
							<links:lastModifiedContactLink entity="${model}" />
						</span>
					</li>
					<li>
						<label><spring:message code="instmod.modeltype"/>:</label>
						<span><t:showTranslationOrDefault translations="${model.modelType.modelTypeNameTranslation}" defaultLocale="${defaultlocale}"/></span>
					</li>
					
				</ol>
			</div>
	</fieldset>
</div>