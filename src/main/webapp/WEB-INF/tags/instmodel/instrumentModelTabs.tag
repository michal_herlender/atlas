<%@ tag language="java" display-name="instrumentModelTabs" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<%@ attribute name="model" required="true" type="java.lang.Object" %>
<%@ attribute name="selectedTab" required="true" type="java.lang.String" %>
<%@ attribute name="canEditInstrumentModel" required="true" type="java.lang.Boolean" %>
<%@ attribute name="canEditSalesCategory" required="true" type="java.lang.Boolean" %>

<spring:message var="notEditableText" code="company.noteditable" javaScriptEscape="true" />

<!-- sub navigation menu which displays and hides sections of page as well as linking to external pages -->
<div id="subnav">
	<dl>
		<c:choose>
			<c:when test="${canEditInstrumentModel}" >
				<t:subnavTabElement tab="edit" url="/editmodel.htm?modelid=${model.modelid}" 
							displayCode="edit" titleCode="viewmod.editdetailsofinstrumentmodel" selectedTab="${selectedTab }"/>
			</c:when>
			<c:otherwise>
				<dt>
					<a href="#" onclick=" event.preventDefault(); $j.prompt('${notEditableText}');" id="edit-link"
						class="disabled" title="<spring:message code='viewmod.editdetailsofinstrumentmodel' />">
						<spring:message code="edit"/>
					</a>
				</dt>
			</c:otherwise>
		</c:choose>

		<t:subnavTabElement tab="configuration" url="/instrumentmodelconfig.htm?modelid=${model.modelid}" 
			displayCode="viewmod.configuration" titleCode="viewmod.viewinstrumentmodelconfig" selectedTab="${selectedTab }"/>
			
		<t:subnavTabElement tab="prices" url="/instrumentmodelprices.htm?modelid=${model.modelid}" 
			displayCode="viewmod.costs" titleCode="viewmod.viewinstrumentmodelcosts" selectedTab="${selectedTab }"/>
			
		<t:subnavTabElement tab="services" url="/instrumentmodelservices.htm?modelid=${model.modelid}" 
			displayCode="services" titleCode="viewmod.viewservices" selectedTab="${selectedTab }"/>
			
		<c:choose>
			<c:when test="${canEditSalesCategory}" >
				<t:subnavTabElement tab="salescategory" url="/editmodelsalescategory.htm?modelid=${model.modelid}" 
					displayCode="salescategory" titleCode="viewmod.vieweditsalescategory" selectedTab="${selectedTab}"/>
			</c:when>
			<c:otherwise>
				<dt>
					<a href="#" onclick=" event.preventDefault(); $j.prompt('${notEditableText}');" id="salescategory-link"
						class="disabled" title="<spring:message code='viewmod.vieweditsalescategory' />">
						<spring:message code="salescategory"/>
					</a>
				</dt>
			</c:otherwise>
		</c:choose>
			
		<t:subnavTabElement tab="notes" url="/instrumentmodelnotes.htm?modelid=${model.modelid}" 
			displayCode="notes" titleCode="viewmod.viewnotesforinstrumentmodel" selectedTab="${selectedTab }"/>
			
		<%-- <t:subnavTabElement tab="files" url="/instrumentmodelfiles.htm?modelid=${model.modelid}" 
			displayCode="files" titleCode="viewmod.viewfileresourcesforinstrumentmodel" selectedTab="${selectedTab }"/> --%>
			
		<%-- <t:subnavTabElement tab="webresources" url="/instrumentmodelwebresources.htm?modelid=${model.modelid}" 
			displayCode="viewmod.webresources" titleCode="viewmod.viewwebresourcesforinstrumentmodel" selectedTab="${selectedTab }"/> --%>
		
		<c:if test="${model.modelType.baseUnits || model.modelType.modules}">
			<t:subnavTabElement tab="basemodel" url="/instrumentmodelbase.htm?modelid=${model.modelid}" 
				displayCode="viewmod.baseunitmodules" titleCode="viewmod.viewbaseunitsmodulesforinstrumentmodel" selectedTab="${selectedTab }"/>
		</c:if>
		
		<%-- <t:subnavTabElement tab="components" url="/instrumentmodelcomponents.htm?modelid=${model.modelid}" 
			displayCode="viewmod.components" titleCode="viewmod.viewcomponentsforinstrumentmodel" selectedTab="${selectedTab }"/> --%>
			
		<t:subnavTabElement tab="images" url="/instrumentmodelimages.htm?modelid=${model.modelid}" 
			displayCode="viewmod.images" titleCode="viewmod.viewinstrumentmodelimages" selectedTab="${selectedTab }"/>
		
		<t:subnavTabElement tab="hiremodel" url="/instrumentmodelhire.htm?modelid=${model.modelid}" 
			displayCode="viewmod.hireinformation" titleCode="viewmod.viewhireinformationformodel" selectedTab="${selectedTab }"/>
		
	</dl>
</div>
<!-- end of sub navigation menu -->