<%@ tag language="java" display-name="showInstrumentModelNameForHighlight" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>

<%@ attribute name="instrument" required="true" type="java.lang.Object" %>

<%--
	From velocity macro showInstrumentModelNameForHighlight (note, takes in instrument as parameter)
	Displays the model name of the given instrument on search results and adds hooks to mfr name, model name and
	description name so they can be highlighted. Instruments can override the mfr name of the model
	which this macro takes into consideration.
 --%>

	<c:if test="${instrument.calibrationStandard}">
		<img src="img/icons/bullet-standard.png" width="12" height="12" class="image_inl" alt="Standard" title="Standard" />
	</c:if>
	<c:if test="${instrument.scrapped}">
		<img src="img/icons/bin_closed.png" width="16" height="16" class="image_inl" alt="Unit B.E.R." title="Unit B.E.R." />
	</c:if>
	<c:set var="enclosingClass" value="" />
	<c:if test="${instrument.scrapped}">
		<c:set var="enclosingClass" value="strike" />
	</c:if>
	<span class="${enclosingClass}">
<%--	## DO NOT CHANGE - FORMATTED THIS WAY FOR RESULT HIGHLIGHTING!! -- Seems to pertain to span classes.  --%>	
		<c:choose>
			<c:when test='${instrument.model.modelMfrType  == "MFR_GENERIC"}'>
				<span class="mfrResult">${instrument.mfr.name}</span>
				<instmodel:showInstrumentRanges instrument="${instrument}"/>
				<span class="modelResult">${instrument.model.model}</span>
			</c:when>
			<c:otherwise>
				<span class="mfrResult">${instrument.model.mfr.name}</span>
				<instmodel:showInstrumentRanges instrument="${instrument}"/>
				<span class="modelResult">${instrument.model.model}</span>
			</c:otherwise>
		</c:choose>
		<span class="descResult"><cwms:besttranslation translations="${instrument.model.description.translations}" /></span>
	</span>
	
