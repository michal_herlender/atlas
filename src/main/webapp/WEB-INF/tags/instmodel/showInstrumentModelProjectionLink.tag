<%@ tag language="java" display-name="showInstrumentModelProjectionLink" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<%@ attribute name="instrument" required="true" type="java.lang.Object" %>

<c:if test="${instrument.calibrationStandard}">
	<img src="img/icons/bullet-standard.png" width="12" height="12" class="image_inl" alt="Standard" title="Standard" />
</c:if>
<c:if test="${instrument.scrapped}">
	<img src="img/icons/bin_closed.png" width="16" height="16" class="image_inl" alt="Unit B.E.R." title="Unit B.E.R." />
</c:if>
<c:choose>
	<c:when test="${instrument.scrapped}">
		<t:showViewModelProjectionLink classValue="mainlink-strike" instrument="${instrument}"/>
	</c:when>
	<c:otherwise>
		<t:showViewModelProjectionLink instrument="${instrument}"/>
	</c:otherwise>
</c:choose>
&nbsp;