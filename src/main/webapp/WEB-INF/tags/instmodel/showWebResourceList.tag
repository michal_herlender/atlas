<%@ tag language="java" display-name="showWebResourceList" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="webResources" required="true" type="java.util.Set" %>
<%@ attribute name="id" required="true" type="java.lang.Integer" %>
<%@ attribute name="entityName" required="true" type="java.lang.String" %>
<%@ attribute name="serviceName" required="true" type="java.lang.String" %>

<%-- #macro(showWebResourceList $webResources $id $entityName $serviceName) --%>

	<c:set var="dollarSym" value="$"/>
		
	<table class="default4 webResources" summary="This table displays all currently stored web resources for the chosen entity">
		<thead>
			<tr>
				<td colspan="3">Web Resources (<span class="webResCount">${webResources.size()}</span>)</td>
			</tr>
			<tr>
				<th class="desc" scope="col">Description</th>
				<th class="link" scope="col">Hyperlink</th>
				<th class="del" scope="col">Delete</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="3"><a href="#" class="mainlink" onclick=" createWebContent(); return false; " title="add new web resource to model">New Web Resource</a></td>
			</tr>
		</tfoot>
		<tbody>
			<c:set var="rowcount" value="0"/>
			<c:choose>
				<c:when test="${webResources.size() < 1}">
					<tr id="noWebResources" class="odd">
						<td colspan="3" class="center bold">No web Resources have been recorded</td>
					</tr>
				</c:when>
				<c:otherwise>
					<c:forEach var="wr" items="${webResources}">
						<tr 
						<c:choose>
							<c:when test="${rowcount % 2 == 0}"> class="even" </c:when>
							<c:otherwise>	class="odd" </c:otherwise>
						</c:choose> id="${wr.id}">
							<td class="desc">${wr.description}</td>
							<td class="link"><a href="${wr.url}" class="mainlink" target="_blank">${wr.url}</a></td>
							<td class="del"><a href="#" onclick=" deleteWebResource(${wr.id}); return false; "><img src="img/icons/delete.png" width="16" height="16" alt="Delete web resource" /></a></td>
						</tr>
						<c:set var="rowcount" value="${rowcount + 1}"/>
					</c:forEach>
				</c:otherwise>
			</c:choose>
		</tbody>
	</table>
	
	<script type='text/javascript'>
		
		// this variable holds the string value of the service to load
		var WebResourceService = '${serviceName}';
		// this variable holds the id of entity
		var WebResId = ${id};
		
		/**
		 * load service necessary for inserting web resource
		 */
		loadScript.loadScripts(new Array('dwr/interface/' + WebResourceService + '.js'));
						
		/**
		 * this function uses dwr to insert a new web resource into the database. If the insertion is successful
		 * then a message is displayed within the thickbox detailing the success. Else if the insertion is declined
		 * then appropriate error messages are displayed within the overlay.
		 */
		function insertWebResource()
		{
			// get value of the new url
			var newURL = ${dollarSym}j('div#addWebResourceOverlay #url').val();
			// get value of the new description
			var newDesc = ${dollarSym}j('div#addWebResourceOverlay #description').val();
			
			// call dwr method to insert the new web resource
			${serviceName}.insert(WebResId, newDesc, newURL,
			{
				callback:function(results)
				{
					// insertion failed
					if(results.success != true)
					{
						// warning box is not already present
						if (${dollarSym}j('div#addWebResourceOverlay .warningBox1').length == 0)
						{
							// remove successbox if present
							${dollarSym}j('div').remove('.successBox1');
							// append the new warningbox before fieldset
							${dollarSym}j('div#addWebResourceOverlay fieldset').before(	'<div class="warningBox1">' +
																							'<div class="warningBox2">' +
																								'<div class="warningBox3">' +
																									// new warning message goes here
																								'</div>' +
																							'</div>' +
																						'</div>');
						}
						// empty the current warningbox
						${dollarSym}j('.warningBox3').empty().append('<h5 class="center-0 attention">New Web Resource Failed</h5>');
						// loop through errorlist
						${dollarSym}j.each(results.errorList, function(i, n)
						{
							// error so display in main warningbox
							${dollarSym}j('.warningBox3').append('<div class="center attention">Field: ' + n.field + ' failed &nbsp;&nbsp;Message: ' + n.defaultMessage + '</div>');	
						});
					}
					// insertion was a success
					else
					{
						// remove warningbox if present
						${dollarSym}j('div').remove('.warningBox1');
						// append a successbox
						${dollarSym}j('div').remove('.successBox1');
						// empty the errormessage span
						${dollarSym}j('#errorMessage').empty();
						// append new successbox before fieldset
						${dollarSym}j('div#addWebResourceOverlay fieldset').before(	'<div class="successBox1">' +
																						'<div class="successBox2">' +
																							'<div class="successBox3">' +
																								'<h5 class="center-0">New web resource "' + results.results.description + '" added successfully</h5>' +
																							'</div>' +
																						'</div>' +
																					'</div>');
						// empty value in new url input field
						${dollarSym}j('div#addWebResourceOverlay #url').attr('value', '');
						// empty value in new description input field
						${dollarSym}j('div#addWebResourceOverlay #description').attr('value', '');
						// check for table row indicating that no web resources are available
						if (${dollarSym}j('tr#noWebResources').length > 0)
						{
							// remove if present in page
							${dollarSym}j('tr#noWebResources').remove();
						}
						// append new row for the web resource added
						${dollarSym}j('table.webResources tbody').append(	'<tr id="' + results.results.id + '">' +
																				'<td>' + results.results.description + '</td>' +
																				'<td><a href="' + results.results.url + '" class="mainlink" target="_blank">' + results.results.url + '</a></td>' +
																				'<td class="center"><a href="#" onclick=" deleteWebResource(' + results.results.id + '); return false; "><img src="img/icons/delete.png" width="16" height="16" alt="Delete web resource" /></a></td>' +
																			'</tr>');
						// recolour the rows in table
						${dollarSym}j('table.webResources tbody tr:odd').removeClass().addClass('odd');
						${dollarSym}j('table.webResources tbody tr:even').removeClass().addClass('even');
							
						// get the current count of web resources and add one then re-append
						${dollarSym}j('span.webResCount').text(parseInt(${dollarSym}j('span.webResCount:first').text()) + 1);
					}	
				}
			});
		}
		
		/**
		 * this function creates content for the overlay when adding a new web resource
		 */
		function createWebContent()
		{
			// create content to be displayed in overlay window using previously populated variables
			var content =	'<div id="addWebResourceOverlay">' +
								'<div class="warningBox1">' +
									'<div class="warningBox2">' +
										'<div class="warningBox3">' +
											'<div>Enter your new web resource and a description in the form below and then click the submit button.</div>' +
										'</div>' +
									'</div>' +
								'</div>' +
								'<fieldset id="addWebResource">' +
									'<ol>' +
										'<li>' +
											'<label for="url">Resource URL:</label>' +
											'<span>' +
												'<input type="text" name="url" id="url" />' +
												'<span id="errorMessage" class="attention"></span>' +
											'</span>' +
										'</li>' +
										'<li>' +
											'<label for="description">Resource Description:</label>' +
											'<span>' +
												'<input type="text" name="description" id="description" />' +
												'<span id="errorMessage" class="attention"></span>' +
											'</span>' +
										'</li>' +
										'<li>' +
											'<label>&nbsp;</label>' +
											'<input type="submit" name="submit" id="submit" onclick=" insertWebResource(); return false; " value="Submit" />' +
										'</li>' +
									'</ol>' +
								'</fieldset>' +
							'</div>';
			// create new overlay
			loadScript.createOverlay('dynamic', 'Add New Web Resource', null, content, null, 40, 680, 'url');
		}
		
		/**
		 * this function uses dwr to delete a web resource from the database. If the deletion is successful
		 * then the row containing the web resource is removed from the table. Else if the deletion is declined
		 * then appropriate error messages are displayed.
		 */
		function deleteWebResource(id)
		{
			// call dwr method to delete the new web resource
			${serviceName}.deleteWebResource(id,
			{
				callback:function(results)
				{
					// deletion failed
					if(results.success != true)
					{
						alert('error');
					}
					// deletion was a success
					else
					{
						// remove the deleted web resource table row from the table
						${dollarSym}j('table.webResources tbody tr').remove('#' + results.results.id);
						// get the current count of web resources and subtract one then re-append
						${dollarSym}j('span#webResCount').text(parseInt(${dollarSym}j('span#webResCount').text()) - 1);
						// recolour the rows in table
						${dollarSym}j('table.webResources tbody tr:odd').removeClass().addClass('odd');
						${dollarSym}j('table.webResources tbody tr:even').removeClass().addClass('even');
						// if all web resources have been deleted the append message
						if (parseInt(${dollarSym}j('span#webResCount').text()) == 0)
						{
							${dollarSym}j('table.webResources tbody').append(	'<tr class="odd" id="noWebResources">' +
																					'<td colspan="3" class="center bold">No web resources have been recorded</td>' +
																				'</tr>');
						}
						
					}	
				}
			});
		}
		
	</script>
	
<!-- #end -->