<%@ tag language="java" display-name="showInstrumentRanges" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel" %>

<%@ attribute name="instrument" required="true" type="java.lang.Object" %>

<c:choose>
	<c:when test="${not empty instrument.ranges}">
		<c:forEach var="range" items="${instrument.ranges}" varStatus="status">
			<c:if test="${status.count > 0}"> / </c:if>
			${range}
		</c:forEach>
	</c:when>
	<c:when test="${not empty instrument.model.ranges}">
		<c:forEach var="range" items="${instrument.model.ranges}" varStatus="status">
			<c:if test="${status.count > 0}"> / </c:if>
			${range}
		</c:forEach>
	</c:when>
</c:choose>
<%-- 
     Commented out original implementation below due to absence of inModelDescription tag, 
     to be confirmed/discussed in Madrid meeting - GB 2016-01-16 
--%>
<%-- <c:choose>
	<c:when test="${instrument.ranges != null && instrument.ranges.size() > 0}">
		<c:set var="rangeCount" value="0"/>
		<c:forEach var="range" items="${instrument.ranges}">
			<c:if test="${range.characteristicDescription.partofkey == true}">
				<c:if test="${rangeCount > 0}"> / </c:if>${range}
				<c:set var="rangeCount" value="${rangeCount + 1}"/>
			</c:if>
		</c:forEach>
	</c:when>
	<c:when test="${instrument.model.ranges != null && instrument.model.ranges.size() > 0}">
		<c:set var="rangeCount" value="0"/>
		<c:forEach var="range" items="${instrument.model.ranges}">
			<c:if test="${range.characteristicDescription.partofkey == true}">
				<c:if test="${rangeCount > 0}"> / </c:if>${range}
				<c:set var="rangeCount" value="${rangeCount + 1}"/>
			</c:if>
		</c:forEach>
	</c:when>
</c:choose> --%>