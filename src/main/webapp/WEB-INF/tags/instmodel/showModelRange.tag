<%@ tag language="java" display-name="showModelRange" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ attribute name="model" required="true" type="java.lang.Object" %>
<%@ attribute name="descid" required="false" type="java.lang.Integer" %>

<c:choose>
	<c:when test="${model.getRanges() != null && model.getRanges().size() > 0}">
		<table class="default2" >
			<thead>
				<tr>
					<th class="field" scope="col"><spring:message code="instmodeditmod.rangetype"/></th>
					<th class="name" scope="col"><spring:message code="instmodeditmod.name"/></th>
					<th class="dvalue" scope="col"><spring:message code="instmodeditmod.value"/></th>
					<th class="daction" scope="col"><spring:message code="instmodeditmod.action"/></th>																						
				</tr>
			</thead>
            <tfoot>
            	<tr>
                	<td colspan="7">&nbsp;</td>
                </tr>
            </tfoot>
            <tbody>
            	<c:set var="rangeCount" value="0"/>
				<c:forEach var="range" items="${model.ranges}">
            	<tr>
	            	<td class="field">${range.characteristicType}</td>
					<td class="name">
						<label id="${'lblName'}${range.characteristicDescription.getCharacteristicDescriptionId()}">
							<c:choose>
								<c:when test="${range.characteristicType == 'CHARACTERISTIC'}">
									<cwms:translationordefault translations="${range.characteristicDescription.translations}" name="${range.characteristicDescription.getName()}" />
								</c:when>
								<c:when test="${range.characteristicType == 'OPTION'}">
									${range.modelOption.getCode()}
								</c:when>
								<c:otherwise>
								</c:otherwise>
							</c:choose>
						</label>
					</td>
					<td class="dvalue">
						<label id="${'lblValue'}${range.characteristicDescription.getCharacteristicDescriptionId()}">
							${range.getValue()}
						</label>
					</td>
					<td class="daction">
	            		<div style="display:none">
	            			<input type="hidden" id="${'rid'}${range.id}" value="${range.id}"></input>
							<input type="hidden" id="${'rcid'}${range.id}" value="${range.characteristicDescription.getCharacteristicDescriptionId()}"></input>
							<input type="hidden" id="${'rctype'}${range.id}" value="${range.characteristicDescription.getCharacteristicType()}"></input>
							<input type="hidden" id="${'rtype'}${range.id}" value="${range.characteristicType}"></input>
							<input type="hidden" id="${'rstart'}${range.characteristicDescription.getCharacteristicDescriptionId()}" value="${range.getStart()}"></input>
							<input type="hidden" id="${'rend'}${range.characteristicDescription.getCharacteristicDescriptionId()}" value="${range.getEnd()}"></input>
							<input type="hidden" id="${'ruom'}${range.characteristicDescription.getCharacteristicDescriptionId()}" value="${range.getUomId()}"></input>
							<input type="hidden" id="${'rminvalue'}${range.characteristicDescription.getCharacteristicDescriptionId()}" value="${range.getMinvalue()}"></input>
							<input type="hidden" id="${'rmaxvalue'}${range.characteristicDescription.getCharacteristicDescriptionId()}" value="${range.getMaxvalue()}"></input>
							<input type="hidden" id="${'rlibraries'}${range.characteristicDescription.getCharacteristicDescriptionId()}"></input>
							<c:choose>
								<c:when test="${range.characteristicType == 'CHARACTERISTIC'}">
    								<input type="hidden" id="${'rname'}${range.id}" value="<cwms:translationordefault translations="${range.characteristicDescription.translations}" name="${range.characteristicDescription.getName()}" />"></input>
								</c:when>
								<c:when test="${range.characteristicType == 'OPTION'}">
									<input type="hidden" id="${'rname'}${range.id}" value="${range.modelOption.getCode()}"></input>
								</c:when>
								<c:otherwise>
									<input type="hidden" id="${'rname'}${range.id}" value=""></input>								
								</c:otherwise>
							</c:choose>
	            		</div>
	            		<c:choose>
							<c:when test="${range.characteristicType == 'CHARACTERISTIC'}">
    							<a href="#" onclick=" editRange('${range.characteristicType}',${range.id}, ${model.getModel().modelid},${range.characteristicDescription.getCharacteristicDescriptionId()}, null, $j(this).parent(), $j(this).parent().parent()); return false; ">
									<img src="img/icons/edit_wi.png" width="16" height="16" class="img_marg_bot" />
								</a>
							</c:when>
							<c:when test="${range.characteristicType == 'OPTION'}">
								<a href="#" onclick=" editRange('${range.characteristicType}',${range.id}, ${model.getModel().modelid},${range.characteristicDescription.getCharacteristicDescriptionId()}, null, $j(this).parent(), $j(this).parent().parent()); return false; ">
									<img src="img/icons/edit_wi.png" width="16" height="16" class="img_marg_bot" />
								</a>
							</c:when>
							<c:otherwise>
								<a href="#" onclick=" editRange('0',${range.id}, ${model.getModel().modelid},${range.characteristicDescription.getCharacteristicDescriptionId()}, null, $j(this).parent(), $j(this).parent().parent()); return false; ">
									<img src="img/icons/edit_wi.png" width="16" height="16" class="img_marg_bot" />
								</a>							
							</c:otherwise>
						</c:choose>
<%-- 	            		<c:choose> --%>
<%-- 							<c:when test="${range.id > 0}">							 --%>
<%-- 								<a href="#" onclick=" removeRange(${range.id}, ${range.characteristicDescription.getCharacteristicdescriptionid()}, null, $j(this).parent(), $j(this).parent().parent()); return false; "> --%>
<!-- 									<img src="img/icons/delete.png" width="16" height="16" class="img_marg_bot" /> -->
<!-- 								</a>	 -->
<%-- 							</c:when> --%>
<%-- 						</c:choose> --%>
					</td>
				</tr>
				<c:set var="rangeCount" value="${rangeCount + 1}"/>
				</c:forEach>
            </tbody>
		</table>
	</c:when>
</c:choose>