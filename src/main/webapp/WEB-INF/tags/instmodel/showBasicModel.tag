<%@ tag language="java" display-name="showBasicModel" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel" %>
<%@ attribute name="instrument" required="true" type="java.lang.Object" %>
		
<c:choose>
	<c:when test="${instrument.model.modelMfrType == 'MFR_GENERIC'}">
		${instrument.mfr.name}
		<instmodel:showInstrumentRanges instrument="${instrument}"/>
		${instrument.model.model} ${instrument.model.description.description}
	</c:when>
	<c:otherwise>
		<instmodel:showInstrumentRanges instrument="${instrument}"/>
		<cwms:showmodel instrumentmodel="${instrument.model}"/>
	</c:otherwise>
</c:choose>