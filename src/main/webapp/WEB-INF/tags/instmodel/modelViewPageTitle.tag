<%@ tag language="java" display-name="modelViewPageTitle" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ attribute name="selectedTab" required="true" type="java.lang.String" %>

<c:choose>
	<c:when test="${selectedTab=='configuration'}">
		<spring:message code="viewmod.viewinstrumentmodelconfig"/>
	</c:when>
	<c:when test="${selectedTab=='prices'}">
		<spring:message code="viewmod.viewinstrumentmodelcosts"/>
	</c:when>
	<c:when test="${selectedTab=='pricehistory'}">
		<spring:message code="viewmod.viewinstrumentmodelcostinghistory"/>
	</c:when>
	<c:when test="${selectedTab=='services'}">
		<spring:message code="viewmod.viewservices"/>
	</c:when>
	<c:when test="${selectedTab=='salescategory'}">
		<spring:message code="viewmod.vieweditsalescategory"/>
	</c:when>
	<c:when test="${selectedTab=='usage'}">
		<spring:message code="viewmod.viewusage"/>
	</c:when>
	<c:when test="${selectedTab=='notes'}">
		<spring:message code="viewmod.viewnotesforinstrumentmodel"/>
	</c:when>
	<c:when test="${selectedTab=='files'}">
		<spring:message code="viewmod.viewfileresourcesforinstrumentmodel"/>
	</c:when>
	<c:when test="${selectedTab=='webresources'}">
		<spring:message code="viewmod.viewwebresourcesforinstrumentmodel"/>
	</c:when>
	<c:when test="${selectedTab=='base'}">
		<spring:message code="viewmod.viewbaseunitsmodulesforinstrumentmodel"/>
	</c:when>
	<c:when test="${selectedTab=='components'}">
		<spring:message code="viewmod.viewcomponentsforinstrumentmodel"/>
	</c:when>
	<c:when test="${selectedTab=='images'}">
		<spring:message code="viewmod.viewinstrumentmodelimages"/>
	</c:when>
	<c:otherwise>
		<spring:message code="error"/>
	</c:otherwise>
</c:choose>