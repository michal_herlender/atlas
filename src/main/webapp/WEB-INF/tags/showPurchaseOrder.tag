<%@ tag language="java" display-name="showPurchaseOrder" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="job" required="true" type="java.lang.Object" %>

<c:set var="poCount" value="${0}"/>
<c:if test="${job.bpo != null}">
	<c:set var="poCount" value="${poCount + 1}"/>
</c:if>
<c:forEach var="po" items="${job.POs}">
	<c:if test="${po.active}">
		<c:set var="poCount" value="${poCount + 1}"/>
	</c:if>
</c:forEach>
<c:set var="expandPos" value="${poCount * 24}"/>
<div class="expandingPOs">
	<div>
		<c:if test="${not empty job.bpoLinks}">
			<c:forEach var="link" items="${job.bpoLinks}">
				<span>${link.bpo.poNumber}&nbsp;</span>
			</c:forEach> [BPO]
			<c:if test="${poCount > 1}">
				<a href="#" onclick=" $j(this).parent().parent().animate( { height: '${expandPos}' }, 1000 ); return false; ">
					<img src="img/icons/viewactions.png" width="16" height="16" class="img_inl_margleft" />
				</a>
			</c:if>
			&nbsp;<img src="img/icons/bullet_clipboard.png" width="10" height="10" onclick=" $j(this).prev().clipBoard(this, null, false, null); " alt="Add po number to clipboard" title="Add po number to clipboard" />&nbsp;
		</c:if>
		<c:forEach var="po" items="${job.POs}" varStatus="loopStatus">
			<c:if test="${po.active}">
				<div id="PO${po.poId}">
					<span>${po.poNumber}</span> [PO]
					<c:if test="${job.bpo == null && loopStatus.count < 2 && poCount > 1}">
						<a href="#" onclick=" $j(this).parent().parent().parent().animate( { height: '${expandPos}' }, 1000 ); return false; ">
							<img src="img/icons/viewactions.png" width="16" height="16" class="img_inl_margleft" />
						</a>
					</c:if>
					&nbsp;<img src="img/icons/bullet_clipboard.png" width="10" height="10" onclick=" $j(this).prev().clipBoard(this, null, false, null); " alt="Add po number to clipboard" title="Add po number to clipboard" />&nbsp;
					<c:if test="${job.defaultPO != null && job.defaultPO.poId == po.poId}">
						[default]
					</c:if>
				</div>
			</c:if>
		</c:forEach>
	</div>
</div>