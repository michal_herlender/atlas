<%@ tag language="java" display-name="showCapability" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>

<%@ attribute name="showHeader" required="true" type="java.lang.Boolean" %>
<%@ attribute name="capability" required="false" type="java.lang.Object" %>
<%@ attribute name="viewcapabilities" required="true" type="java.lang.Boolean" %>

<%-- 
	The parameter 'viewcapabilities' indicates whether viewing from 'viewcapabilities', 
	controls redirect from edit link, and whether overall capability description is shown 
	if true -> view capabilities (show description), false -> view capability (no need to show description)
	
--%>

<c:if test="${showHeader}">
	<table class="default4 capabilitytable">
		<thead>
			<tr>
				<td class="csrreference"><spring:message code="searchprocedureresults.reference"/></td>
				<td class="csrname"><spring:message code="name"/></td>
				<td class="csractive"><spring:message code="active"/></td>
				<td class="csredit"></td>
			</tr>
		</thead>
	</table>
</c:if>

<c:if test="${not empty capability}">
	<table class="default4 capabilitytable" id="proc-table-${capability.id}">
		<tbody>
			<tr>
				<td class="csrreference">
                	<cwms-capability-link-and-tooltip reference="${capability.reference}" id=${capability.id} ></cwms-capability-link-and-tooltip>
				</td>
				<td class="csrname">${capability.name}</td>
				<td class="csractive"><spring:message code="${capability.active}" /></td>
				<td class="csredit">
					<cwms:securedLink collapse="True" permission="CAPABILITY_FILTER_EDIT" classAttr="mainlink" parameter="?capabilityId=${capability.id}&viewcapabilities=${viewcapabilities}">
						<spring:message code="edit"/>
					</cwms:securedLink>
				</td>
			</tr>
			<%-- Show comments (stored in description) if present, but not needed on viewcapability page --%>
			<c:if test="${viewcapabilities && not empty capability.description}">
				<tr>
					<td colspan="4">${capability.description.replaceAll("\\n","<br/>")}</td>
				</tr>
			</c:if>
			<%-- Only show capability filters subtable if at least one is present --%>
			<c:if test="${not empty capability.capabilityFilters}">
				<tr>
					<td colspan="6" style=" padding: 0; ">
						<table class="default4 child-table filters" border="">
							<thead>
								<tr>
									<th class="filterServiceType" scope="col"><spring:message code="servicetype"/></th>
									<th class="filterServiceTypeLong" scope="col"><spring:message code="viewcapability.serviceTypeLong"/></th>
									<th class="filterCapability" scope="col"><spring:message code="viewcapability.capability"/></th>
									<th class="filterActive" scope="col"><spring:message code="viewcaltypes.active"/></th>
									<th class="filterComments" scope="col"><spring:message code="viewcapability.comments"/></th>
									<th class="filterWorkInstruction" scope="col"><spring:message code="editproc.defaultworkinstruction"/></th>
									<th class="filterEstimatedTime" scope="col"><spring:message code="editproc.estimatedtime"/></th>
									<th class="filterDescription" scope="col"><spring:message code="viewcapability.externalComments"/></th>
									<th class="filterBestLab" scope="col"><spring:message code="viewcapability.bestlab"/></th>
								</tr>
							</thead>
							<tbody>
							<c:set var="readonly" value="true"/>
							<sec:authorize access="hasRole('ROLE_MANAGER')">
								<c:set var="readonly" value="false"/>
							</sec:authorize>
								<c:forEach var="filter" items="${capability.capabilityFilters}">
									<tr>
										<td>
											<cwms:besttranslation translations="${filter.servicetype.shortnameTranslation}"/>
										</td>
										<td>
											<cwms:besttranslation translations="${filter.servicetype.longnameTranslation}"/>
										</td>
										<td>${filter.filterType.description}</td>
										<td>
										<cwms-capabilityfilter-active-switch value="${filter.active}" disabled="${readonly}"></cwms-capabilityfilter-active-switch>
										 </td>
										<td>${filter.internalComments.replaceAll("\\n","<br/>")}</td>
										<td>${filter.defaultWorkInstruction.name}</td>
										<td>${filter.standardTime}</td>
										<td>${filter.externalComments}</td>
										<td><c:if test="${filter.bestLab}"><spring:message code="yes" /></c:if></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</td>
				</tr>
			</c:if>
		</tbody>
	</table>
</c:if>