<%@ tag language="java" display-name="showAccreditations" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>

<%@ attribute name="capability" required="true" type="java.lang.Object" %>
<%@ attribute name="personid" required="true" type="java.lang.Object" %>
<%@ attribute name="caltype" required="true" type="java.lang.Object" %>

<c:set var="authorized" value="false"/>
<c:set var="authorization" value=""/>
<c:forEach var="authorizationItem" items="${capability.authorizations}">
	<c:if test="${!authorized && authorizationItem.authorizationFor.personid == personid}">
		<c:if test="${caltype == null || caltype.calTypeId == authorizationItem.caltype.calTypeId}">
			<c:set var="authorized" value="true"/>
			<c:set var="authorization" value="${authorizationItem}"/>
		</c:if>
	</c:if>
</c:forEach>
<c:choose>
	<c:when test="${authorized}">
		<spring:message code="accreditationlevel.useraccreditation"/>: ${authorization.accredLevel.message}
		<c:if test="${authorization.awardedBy != null || authorization.awardedOn != null}">
			[
			<c:if test="${authorization.awardedBy != null}">
				<span style="color:gray;">${authorization.awardedBy.name}</span>
			</c:if>
				<c:if test="${authorization.awardedOn != null}">
					<span style="color:gray;"><fmt:formatDate pattern="dd.MM.yyyy" type="date" value="${authorization.awardedOn}"/></span>
				</c:if>
			]
		</c:if>
	</c:when>
	<c:otherwise>
		<spring:message code="accreditationlevel.noaccreditation"/> 
	</c:otherwise>
</c:choose>
