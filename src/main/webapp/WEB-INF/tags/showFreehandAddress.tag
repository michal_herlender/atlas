<%@ tag language="java" display-name="showFreehandAddress" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="address" required="true" type="java.lang.Object" %>
<%@ attribute name="separator" required="true" %>

	<c:set var="addressText" value="${addressText}${address}${separator}"/>
	${addressText}
	