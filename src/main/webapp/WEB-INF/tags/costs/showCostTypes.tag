<%@ tag language="java" display-name="showCostTypes" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="costs" tagdir="/WEB-INF/tags/costs" %>
<%@ attribute name="costTypes" required="true" type="java.lang.Object" %>
<%@ attribute name="itemid" required="true" type="java.lang.Object" %>

<span id="costtypes${itemid}">
    [
    <span id="itemcosttypes${itemid}"> 
        <c:forEach var="costtype" items="${costTypes}" varStatus="costtpStatus">
            <span id="cost${costtype.typeid}item${itemid}">
                <c:choose>
                    <c:when test="${costTypes.size() == costtpStatus.index}">
                    <spring:message code="${costtype.messageCode}"/> <a href="#" onclick="event.preventDefault(); removeCostType(${costtype.typeid}, ${itemid}); "><img src="img/icons/bullet_delete.png" width="7" height="7" alt="Remove cost type" title="Remove cost type" /></a>
                    </c:when>
                    <c:otherwise>
                    <spring:message code="${costtype.messageCode}"/> <a href="#" onclick="event.preventDefault(); removeCostType(${costtype.typeid}, ${itemid}); "><img src="img/icons/bullet_delete.png" width="7" height="7" alt="Remove cost type" title="Remove cost type" /></a>
                    </c:otherwise>
                </c:choose>
            </span>
        </c:forEach>
        <c:set var="selectedLink" value=""/>
        <c:choose>
            <c:when test="${costTypes.size() < 1}">
                <c:set var="selectedLink" value='class="vis" ' />
            </c:when>
            <c:otherwise>
                <c:set var="selectedLink" value='class="hid" ' />
            </c:otherwise>
        </c:choose>
        
        <span id="nocosttype${itemid}" ${selectedLink}>No cost type selected</span>
    </span>
    ]
    <a href="#" <c:if test="${costTypes.size() > 3}"> class="hid" </c:if> id="showavailablecosttypes${itemid}" onclick="event.preventDefault(); showAvailableCostTypes(${itemid}); "><img src="img/icons/bullet_add.png" width="7" height="7" alt="Add cost type" title="Add cost type" /></a>
    
</span>