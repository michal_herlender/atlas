<%@ tag language="java" display-name="showJobCostingTPCost" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="costs" tagdir="/WEB-INF/tags/costs" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<%@ attribute name="item" required="true" type="java.lang.Object" %>
<%@ attribute name="costName" required="true" type="java.lang.Object" %>
<%@ attribute name="cost" required="true" type="java.lang.Object" %>

	<li>
		<strong><spring:message code="jobcost.thirdpartycosts"/></strong>
	</li>
	<c:set var="name" value="${costName}"/>
	<c:set var="costid" value="${cost.costType.typeid}"/>
	<li>
		<label><spring:message code="vm_costs.tpcostsource"/>:</label>
		<div class="symbolbox">&nbsp;</div>
		<form:select path="item.${name}Cost.thirdCostSrc" onchange=" toggleTPCostSrc(this.value, '${name}'); return false;">
            <c:forEach var="tpSrc" items="${tpcostsrc}">
                <option value="${tpSrc}" <c:if test="${cost.thirdCostSrc == tpSrc}"> selected="selected" </c:if>>${tpSrc.name}</option>
            </c:forEach>
        </form:select>
        <form:errors path="item.${name}Cost.thirdCostSrc" class="attention"/>
	</li>
	
	<c:set var="manualDisplay" value="vis"/>
	<c:set var="linkedDisplay" value="hid"/>
	<c:if test="${cost.thirdCostSrc == 'THIRD_PARTY_QUOTATION'}">
		<c:set var="manualDisplay" value="hid"/>
		<c:set var="linkedDisplay" value="vis"/>
	</c:if>
	<li id="manual${name}cost" class="${manualDisplay}">
		<label><spring:message code="vm_costs.manualtpcosts"/>:</label>
		<div class="symbolbox">${currency.currencyERSymbol}</div>
		<div class="valuebox">
		<form:input path="item.${name}Cost.thirdManualPrice" class="right" />
		<form:errors path="item.${name}Cost.thirdManualPrice" class="attention"/>
		</div>
	</li>
	
	<li id="linked${name}cost" class="${linkedDisplay}">
		<label><spring:message code="vm_costs.tpquotcosts"/>:</label>		
		<c:choose>
			<c:when test="${item.jobItem.linkedTPQuotes.size() > 0}">
			<div class="float-left">				
				<c:set var="costsFound" value=""/>
				<c:forEach var="linkedQuote" items="${item.jobItem.linkedTPQuotes}" varStatus="statuslinkedQuote">
					 <c:if test="${statuslinkedQuote.index > 1}"> <br /></c:if>
<!-- 					## iterate over all the costs on the tpquotationitem to make sure a cost for this costtype is present on the tpquotation  -->
					<c:set var="thirdCost" value=""/>
					<c:forEach var="tpcost" items="${linkedQuote.tpQuoteItem.costs}">
						<c:if test="${tpcost.costType.typeid == cost.costType.typeid}">
							<c:set var="thirdCost" value="${tpcost}"/>
						</c:if>
					</c:forEach>
					
					<c:set var="linkCostSrcId" value=""/>
					<c:if test="${not empty cost.linkedCostSrc}">
						<c:set var="linkCostSrcId" value="${cost.linkedCostSrc.costid}" />
					</c:if>
					
					<c:set var="disabled" value=""/>
                    <c:set var="checked" value=""/>
                    <c:if test="${thirdCost.active != true}">
                        <c:set var="disabled" value="disabled"/>
                    </c:if>
                    <c:if test="${linkCostSrcId == thirdCost.costid}">
                        <c:set var="checked" value="checked"/>
                    </c:if>
                    <form:radiobutton path="${name}CostId" class="widthAuto" value="${thirdCost.costid}" disabled="${disabled}" checked="${checked}"/>

					<a class="mainlink" href="viewtpquote.htm?id=${linkedQuote.tpQuoteItem.tpquotation.id}" target="_blank">${linkedQuote.tpQuoteItem.tpquotation.qno}</a>
										
					<c:choose>
						<c:when test="${thirdCost.active == true}">
							${linkedQuote.tpQuoteItem.tpquotation.currency.currencyERSymbol}
							&nbsp;${thirdCost.finalCost}
							<c:out value=" - "/>
							${thirdCost.discountRate}%
						</c:when>
						<c:otherwise>
							<spring:message code="vm_costs.nonamecosts" arguments="${name}"/>
						</c:otherwise>
					</c:choose>
					
					<t:showTranslationOrDefault translations="${linkedQuote.tpQuoteItem.caltype.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/>
				</c:forEach>

			</div>
			<!-- clear floats and restore page flow -->
			<div class="clear-0"></div>
			</c:when>
			<c:otherwise> 
			<span>
				<spring:message code="vm_costs.notpcostslinked"/>
				<!-- 
				<a href="#" class="mainlink domthickbox" onclick=" /* displayTPCostSearch($costid, $item.jobCosting.id, $item.jobItem.inst.model.modelid, '$esc.html($defaultCurrency.currencyERSymbol)', '$currency.currencyCode', '$defaultCurrency.currencyCode'); */ alert('functionality to be implemented'); return false; ">Find TP Costs</a>
				 -->
				<form:input path="${name}CostId" class="" />
				<form:errors path="${name}CostId" class="attention"/>
			</span>
			</c:otherwise>
		</c:choose>
	</li>
	
	<li>
		<label><spring:message code="vm_costs.tpmarkupratesource"/>:</label>
		<div class="symbolbox">&nbsp;</div>
		<form:select path="item.${name}Cost.thirdMarkupSrc" onchange=" toggleTPMarkupSrc(this.value, '${name}'); return false; ">
			<c:forEach var="tpSrc" items="${tpcostmarkupsrc}">
				<option value="${tpSrc}" <c:if test="${cost.thirdMarkupSrc == tpSrc}">selected="selected" </c:if>>${tpSrc.name}</option>
			</c:forEach>
		</form:select>
			<form:errors path="item.${name}Cost.thirdMarkupSrc" class="attention"/>
	</li>
	
	<c:set var="manualMarkupDisplay" value="vis"/>
	<c:set var="systemMarkupDisplay" value="hid"/>
	<c:if test="${cost.thirdMarkupSrc == 'SYSTEM_DEFAULT'}">
		<c:set var="manualMarkupDisplay" value="hid"/>
		<c:set var="systemMarkupDisplay" value="vis"/>
	</c:if>
	<li id="${name}manualmarkup" class="${manualMarkupDisplay}">
		<label><spring:message code="vm_costs.tpmarkuprate"/>:</label>
		<div class="symbolbox">&nbsp;</div>
		<div class="valuebox">
			<form:input path="item.${name}Cost.thirdMarkupRate" class="right" /> %
		</div>
			<form:errors path="item.${name}Cost.thirdMarkupRate" class="attention"/>
	</li>
	<li id="${name}systemmarkup" class="${systemMarkupDisplay}">
		<label><spring:message code="vm_costs.tpmarkuprate"/>:</label>
		<div class="symbolbox">&nbsp;</div>
		<div class="valuebox">
			<span class="valuetext">
				${cost.thirdMarkupRate}
			</span>
			<span class="percentbox">%</span>
		</div>		
		<!-- clear floats and restore page flow -->
		<div class="clear"></div>
	</li>
	<li>
		<label><spring:message code="vm_costs.tpmarkuptotal"/>:</label>
		<div class="symbolbox">${currency.currencyERSymbol}</div>
		<div class="valuebox">
			<span class="valuetext">
				${cost.thirdMarkupValue}
			</span>
			
		</div>
		<!-- clear floats and restore page flow -->
		<div class="clear"></div>
	</li>
	
	<li>
		<label><spring:message code="vm_costs.tpcarriagemarkupratesource"/>:</label>
		<div class="symbolbox">&nbsp;</div>
		<form:select path="item.${name}Cost.tpCarriageMarkupSrc" onchange="if(this.value == 'MANUAL'){ $j('#' + '${name}' + 'carMarkRate').css({display:'block'})} else {$j('#' + '${name}' + 'carMarkRate').css({display:'none'})} return false;">
			<c:forEach var="tpSrc" items="${tpcostmarkupsrc}">
				<option value="${tpSrc}" <c:if test="${cost.tpCarriageMarkupSrc == tpSrc}"> selected="selected" </c:if>>${tpSrc.name}</option>
			</c:forEach>
		</form:select>
	<form:errors path="item.${name}Cost.tpCarriageMarkupSrc" class="attention"/>
	</li>
	
	<!-- Only display this if using a manual markup source -->
	<c:set var="displayCarMark" value="display"/>
	<c:if test="${cost.tpCarriageMarkupSrc != 'MANUAL'}">
		<c:set var="displayCarMark" value="none"/>
	</c:if>
	<li id="${name}carMarkRate" style="display: ${displayCarMark};">	
		<label><spring:message code="vm_costs.carriagemarkuprate"/></label>
		<div class="symbolbox"></div>
		<form:input path="item.${name}Cost.tpCarriageMarkupRate" class="right" /> %
		<form:errors path="item.${name}Cost.tpCarriageMarkupRate" class="attention"/>
	</li>
	<li>	
		<label><spring:message code="tpcosts.carriageout"/></label>
		<div class="symbolbox">${currency.currencyERSymbol}</div>
		<form:input path="item.${name}Cost.tpCarriageOut" class="right" />
		<form:errors path="item.${name}Cost.tpCarriageOut" class="attention"/>
	</li>
	<li>	
		<label><spring:message code="vm_costs.carriageoutmarkedup"/>:</label>
		<div class="symbolbox">${currency.currencyERSymbol}</div>
		<div class="valuebox">
			<span class="valuetext">
				${cost.tpCarriageOutMarkupValue}
			</span>
		
		</div>
		<div class="clear"></div>
	</li>
	<li>
		<label><spring:message code="tpcosts.carriagein"/></label>
		<div class="symbolbox">${currency.currencyERSymbol}</div>
		<form:input path="item.${name}Cost.tpCarriageIn" class="right" />
		<form:errors path="item.${name}Cost.tpCarriageIn" class="attention"/>
	</li>
	<li>
		<label><spring:message code="vm_costs.carriageinmarkedup"/>:</label>
		<div class="symbolbox">${currency.currencyERSymbol}</div>
		<div class="valuebox">
			<span class="valuetext">
				${cost.tpCarriageInMarkupValue}
			</span>
			
		</div>
		<div class="clear"></div>									
	</li>
	<li>
		<label><spring:message code="vm_costs.carriagetotal"/>:</label>
		<div class="symbolbox">${currency.currencyERSymbol}</div>
		<div class="valuebox">
			<span class="valuetext">
				${cost.tpCarriageTotal}
			</span>
		
		</div>
		<div class="clear"></div>
	</li>
		
	<li>
		<label><spring:message code="vm_costs.tptotal"/>:</label>
		<div class="symbolbox">${currency.currencyERSymbol}</div>
		<div class="valuebox">
			<span class="valuetext">
				${cost.thirdCostTotal}
			</span>
		
		</div>
		<!-- clear floats and restore page flow -->
		<div class="clear"></div>
	</li>
	<li>
		&nbsp;
	</li>