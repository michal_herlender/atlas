<%@ tag language="java" pageEncoding="UTF-8" display-name="pInvoiceItemCostSource" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="item" required="true" type="java.lang.Object" %>

<span class="costsourcespan hid">
	<c:choose>
		<c:when test="${item.source.name() == 'CONTRACT_REVIEW'}">
			<%-- not empty itemcost.cost.linkedCost.quotationCalibrationCost --%>
			<%-- itemcost.cost.linkedCost.quotationCalibrationCost.quoteItem.quotation --%>
			<c:choose>
				<c:when test="${not empty item.quotation}">
					[<spring:message code="vm_costs.cr" /> - <a href="viewquotation.htm?id=${item.quotation.id}" class="mainlink">${item.quotation.qno} ver ${item.quotation.ver}</a>]
				</c:when>
				<c:otherwise>
					[<spring:message code="vm_costs.cr" />]
				</c:otherwise>
			</c:choose>
		</c:when>
		<c:when test="${item.source.name() == 'JOB_COSTING'}">
			[<spring:message code="vm_costs.jc" /> - <a href="viewjobcosting.htm?id=${item.jobCosting.id}" class="mainlink">${item.jobCosting.qno} ver ${item.jobCosting.ver}</a>]
		</c:when>
	</c:choose>
</span>