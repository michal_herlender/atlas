<%@ tag language="java" pageEncoding="UTF-8" display-name="pInvoiceItemCost" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="cost" required="true" type="java.lang.Object" %>
<%@ attribute name="source" required="true" type="java.lang.Object" %>

<c:if test="${not empty cost}">
	<!--  get the currency parent of the cost  -->
	<c:choose>
		<c:when test="${source == 'CONTRACT_REVIEW'}">
			<!-- this cost is a contract review cost -->
			${cost.jobitem.job.currency.currencyERSymbol}
		</c:when>
		<c:when test="${source == 'JOB_COSTING'}">
			<!-- this cost is a job costing cost -->
			${cost.jobCostingItem.jobCosting.currency.currencyERSymbol}
		</c:when>
	</c:choose>
	${itemcost.cost.finalCost}<br>
</c:if>