<%@ tag language="java" display-name="showActiveNotes velocityOrder: entity links noteType noteTypeId (calReqs) +privateOnlyNotes +contact" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<%@ attribute name="entity" required="true" type="java.lang.Object" %>
<%@ attribute name="links" required="true" type="java.lang.Object" %>
<%@ attribute name="noteType" required="true" type="java.lang.Object" %>
<%@ attribute name="noteTypeId" required="true" type="java.lang.Object" %>
<%@ attribute name="calReqs" required="false" type="java.lang.Object" %>
<%@ attribute name="privateOnlyNotes" required="true" type="java.lang.Object" %>
<%@ attribute name="contact" required="true" type="java.lang.Object" %>


<%-- 
#* showActiveNotes
* Macro loops through a list of Notes
* and displays only the active notes
*

* @param entity - entity that has one or more notes
* @param links - boolean value to indicate whether to show links
* @param noteType - type of note (ie quotation)
* @param noteTypeId - the id of entity the note should be attached to
* @param calReqs - calibration requirements if applicable here (actually optional - can be empty)
* privateOnlyNotes - boolean value indicating that only private notes should be shown (added to JSP tag)
* contact - current contact (added to JSP tag)
*#
--%>
<fieldset class="showActiveItemNotes">

	<c:if test="${not empty calReqs}">
		<script type='text/javascript'>
			// load note javascript file
			loadScript.loadScriptsDynamic(new Array('script/trescal/core/tools/CalibrationRequirements.js'), false);
		</script>
		<ol class="activeItemCalReqs">
			<t:showCalReqAsItemNote calReqs="${calReqs}" ji="0" page="quotation" />
		</ol>
	</c:if>

	<c:if test="${entity.notes.size() > 0}">
		<ol class="activeItemNotes">
			<c:forEach var="note" items="${entity.notes}">
				<c:choose>
					<c:when test="${note.publish}">
						<c:set var="getNoteText" value="pub"/>
					</c:when>
					<c:otherwise>
						<c:set var="getNoteText" value="priv"/>
					</c:otherwise>
				</c:choose>
				<c:if test="${note.active}">
						<li id="note_${noteType}$note.noteid">
							<div class="inlinenotes_note">
								<c:if test="${not empty note.label}">
									<strong><span class="notelabel_${getNoteText}${note.noteid}">${note.label}</span></strong><br />
								</c:if>
								<span class="note_${getNoteText}${note.noteid}">${note.note.replaceAll("\\n","<br/>")}
								</span>
							</div>
							<div class="inlinenotes_edit">
								<c:if test="${links}">
									<spring:message var="publishText" code="notes.makeprivate"/>
									<spring:message var="publishCurrent" code="public"/>
									<c:if test="${not note.publish}">
										<spring:message var="publishText" code="notes.makepublic"/>
										<spring:message var="publishCurrent" code="private"/>
									</c:if>
									<c:set var="showPublishLink" value="true"/>
									<c:if test="${privateOnlyNotes}">
										<c:set var="showPublishLink" value="false"/>
									</c:if>
									<c:if test="${showPublishLink}">
										(&nbsp;<a href="#" id="publishNote${note.noteid}" onclick=" event.preventDefault(); publish(${note.noteid}, ${not note.publish}, ${currentContact.personid}, '${noteType}', ${noteTypeId}, this.id, ''); " class="mainlink">${publishCurrent} - ${publishText}</a>&nbsp;)&nbsp;
									</c:if>
									<span><a href="#" onclick=" event.preventDefault(); addNoteBoxy('edit', true, '${noteType}', 0, ${currentContact.personid}, ${note.noteid}, 0); "><img src="img/icons/note_edit.png" width="16" height="16" alt="<spring:message code='notes.editnote' />" title="<spring:message code='notes.editnote' />" class="noteimg_padded" /></a></span>
									<a href="#" onclick=" event.preventDefault(); changeNoteStatus(${note.noteid}, ${not note.active}, ${currentContact.personid}, '${noteType}', '${noteTypeId}', ''); "><img src="img/icons/note_delete.png" width="16" height="16" alt="<spring:message code='notes.deletenote' />" title="<spring:message code='notes.deletenote' />" class="noteimg_padded" /></a><br />
								</c:if>
								${note.setBy.name} (<fmt:formatDate value="${note.setOn}" dateStyle="medium" type="both"  timeStyle="short"/>)
							</div>
							<div class="clear-0"></div>
						</li>
				</c:if>
			</c:forEach>
		</ol>
	</c:if>

</fieldset>
