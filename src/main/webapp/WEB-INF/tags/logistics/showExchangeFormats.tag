<%@ tag language="java" display-name="locationsTable"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<!-- one of this two attributes should be provided -->
<%@ attribute name="exchangeformats" required="true" type="java.lang.Object"%>
<!-- either Company or Subdiv (business or client) -->
<%@ attribute name="organisation" required="true" type="java.lang.Object"%>

<script type="text/javascript">

function deleteExchangeFormat(id){
	var yes = i18n.t("yes", "Yes");
	var no = i18n.t("no", "No");
	$j.prompt(i18n.t("delete", "delete")+" ?", {
		buttons: { [no]: false, [yes]: true },
		submit: function(e,v,m,f){
			
			if(v){
				// make the ajax call
				$j.ajax({
					  url: "deleteExchangeFormat.json?id="+id,
					  dataType:'json'
				}).done(function(res) {
					console.log(res);
					if(res.success===true){
						// refresh page
						window.location.reload();
					}else{
						$j.prompt.close();
						$j.prompt(res.message);
					}
				});
			}
		}
	});
}
</script>


<table class="default2" id="exchangeformats_table">
	<thead>
		<tr>
			<td colspan="8">
				<c:choose>
					<c:when	test="${ organisation['class'].simpleName eq 'Company' and organisation.companyRole eq 'BUSINESS' }">
						<spring:message code="exchangeformat.tabletitle.business.company" arguments="${organisation.coname}" />
						<span style="margin-left: 2px">(${exchangeformats.size()})</span>
						<cwms:securedLink permission="EXCHANGE_FORMAT" collapse="True" classAttr="mainlink-float" parameter="?action=create&businessCompany=${organisation.coid}">
							<spring:message code="exchangeformat.create"/>
						</cwms:securedLink>
					</c:when>
					<c:when	test="${ organisation['class'].simpleName eq 'Subdiv' and organisation.comp.companyRole eq 'BUSINESS' }">
						<spring:message code="exchangeformat.tabletitle.business.subdiv"  arguments="${organisation.comp.coname},${organisation.subname}" />
						<span style="margin-left: 2px">(${exchangeformats.size()})</span>
						<cwms:securedLink permission="EXCHANGE_FORMAT" collapse="True" classAttr="mainlink-float" parameter="?action=create&businessSubdiv=${organisation.subdivid}">
							<spring:message code="exchangeformat.create"/>
						</cwms:securedLink>
					</c:when>
					<c:when	test="${ organisation['class'].simpleName eq 'Company' and organisation.companyRole eq 'CLIENT' }">
						<spring:message code="exchangeformat.tabletitle.client" arguments="${organisation.coname}" />
						<span style="margin-left: 2px">(${exchangeformats.size()})</span>
						<cwms:securedLink permission="EXCHANGE_FORMAT" collapse="True" classAttr="mainlink-float" parameter="?action=create&clientComp=${organisation.coid}">
							<spring:message code="exchangeformat.create"/>
						</cwms:securedLink>

					</c:when>
					<c:when	test="${ organisation['class'].simpleName eq 'Subdiv' and organisation.comp.companyRole eq 'CLIENT' }">
						<spring:message code="exchangeformat.tabletitle.client" arguments="${organisation.comp.coname}" />
						<span style="margin-left: 2px">(${exchangeformats.size()})</span>
						<cwms:securedLink permission="EXCHANGE_FORMAT" collapse="True" classAttr="mainlink-float" parameter="?action=create&clientComp=${organisation.comp.coid}">
							<spring:message code="exchangeformat.create"/>
						</cwms:securedLink>
					</c:when>
				</c:choose>
			</td>
		</tr>
		<tr>
			<th class="organisation" scope="col"><spring:message code="exchangeformat.organization" /></th>
			<th class="name" scope="col" style="width:100px;" ><spring:message code="name" /></th>
			<th class="type" scope="col" style="width:80px;" ><spring:message code="type" /></th>
			<th class="description" scope="col" style="width:150px;" ><spring:message code="description" /></th>
			<th class="clientCompany" scope="col"><spring:message code="clientcompany" /></th>
			<th class="lastModified" scope="col"><spring:message code="viewworkinstruction.modifiedon"/></th>
			<th class="LastModifiedBy" scope="col"><spring:message code="viewworkinstruction.modifiedby"/></th>
			<th class="Actions" scope="col"><spring:message code="viewinstrument.actions"/></th>
		</tr>
	</thead>
	<tbody>
	<c:choose>
		<c:when test="${exchangeformats.size() eq 0}">
			<tr><td colspan="8"><spring:message code="exchangeformat.noexchangeformats"/></td></tr>
		</c:when>
		<c:otherwise>
		<c:forEach var="ef" items="${exchangeformats}">
			<tr>
				<td>
				  <c:choose>
				   <c:when test="${ef.exchangeFormatLevel ne 'GLOBAL' && ef.type ne 'PREBOOKING_FROM_MOBILEAPP'}">
					<c:choose>
						<c:when
							test="${ not empty ef.businessCompany }">
							<image:businessCompanySpecific />
							<links:companyLinkDWRInfo company="${ ef.businessCompany }"
								rowcount="0" copy="false" />
						</c:when>
						<c:otherwise>
							<image:businessSubdivisionSpecific />
							<links:subdivLinkDWRInfo rowcount="0"
								subdiv="${ ef.businessSubdiv }" />
						</c:otherwise>
					</c:choose>
				  </c:when>
				  <c:otherwise>
				  			<span>[GLOBAL]</span>
				  </c:otherwise>
				  </c:choose>
				</td>
				<td>${ef.name}</td>
				<td>${ef.type.value}</td>
				<td>${ef.description}</td>
				<td>
				  <c:choose>
				  	<c:when test="${ef.exchangeFormatLevel ne 'GLOBAL' && ef.type ne 'PREBOOKING_FROM_MOBILEAPP'}">
				  			<c:if test="${not empty ef.clientCompany}">
						      <links:companyLinkDWRInfo company="${ef.clientCompany}"	rowcount="0" copy="false" />
					        </c:if>
				  	</c:when>
				  	<c:otherwise>
				  	     [GLOBAL]
				  	</c:otherwise>
				  </c:choose>
				</td>
				<td><fmt:formatDate value="${ef.lastModified}" pattern="dd.MM.yyyy - HH:mm:ss"/></td>
				<td>${ef.lastModifiedBy.name}</td>
				<td>
					<center>
						<c:if test="${ef.exchangeFormatLevel ne 'GLOBAL' && ef.type ne 'PREBOOKING_FROM_MOBILEAPP'}">
		                	<a href="<c:url value='downloadExchangeFormatTemplate' />?id=${ef.id}" target="blank" class="mainlink">
								<spring:message code="exchangeformat.download" />
							</a>
						</c:if>
						
						<c:if test="${ ef.type ne 'INSTRUMENTS_IMPORTATION' }" >
							<c:choose>
								<c:when	test="${ organisation['class'].simpleName eq 'Company' and organisation.companyRole eq 'BUSINESS' }">
									<a href="exchangeformat.htm?action=edit&id=${ef.id}&businessCompany=${organisation.coid}">
										<spring:message code="edit"/>
									</a>
								</c:when>
								<c:when	test="${ organisation['class'].simpleName eq 'Subdiv' and organisation.comp.companyRole eq 'BUSINESS' }">
									<a href="exchangeformat.htm?action=edit&id=${ef.id}&businessSubdiv=${organisation.subdivid}">
										<spring:message code="edit"/>
									</a>
								</c:when>
								<c:when	test="${ organisation['class'].simpleName eq 'Company' and organisation.companyRole eq 'CLIENT' }">
									<a href="exchangeformat.htm?action=edit&id=${ef.id}&clientComp=${organisation.coid}">
										<spring:message code="edit"/>
									</a>
								</c:when>
								<c:when	test="${ organisation['class'].simpleName eq 'Subdiv' and organisation.comp.companyRole eq 'CLIENT' }">
									<a href="exchangeformat.htm?action=edit&id=${ef.id}&clientComp=${organisation.comp.coid}">
										<spring:message code="edit"/>
									</a>
								</c:when>
							</c:choose>
						</c:if>
						<c:if test="${ef.exchangeFormatLevel ne 'GLOBAL' && ef.type ne 'PREBOOKING_FROM_MOBILEAPP'}">
		                	<a onclick=" event.preventDefault(); deleteExchangeFormat(${ef.id});" href="">
		                		<spring:message code="delete"/>
		                	</a>
						</c:if>
						
					</center>
				</td>
			</tr>
		</c:forEach>
		</c:otherwise>
	</c:choose>
	</tbody>
</table>
<div class="clear"></div>


