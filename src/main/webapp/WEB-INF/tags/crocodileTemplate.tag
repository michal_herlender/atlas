<!DOCTYPE HTML>
<%@ tag language="java" display-name="crocodileTemplate" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="dsp" uri="http://www.ttddyy.net/dsproxy/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<c:set var="req" value="${pageContext.request}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="uri" value="${req.requestURI}" />
<%-- Allows use of loadtab variable if already specified in page context (e.g. reference data by controller) --%>
<c:if test="${empty loadtab}">
	<c:choose>
		<c:when test="${not empty req.getParameter('loadtab')}">
			<c:set var="loadtab" value="${req.getParameter('loadtab')}" />
		</c:when>
		<c:otherwise>
			<c:set var="loadtab" value="" />
		</c:otherwise>
	</c:choose>
</c:if>

<%@ attribute name="header" fragment="true" %>
<%@ attribute name="scriptPart" fragment="true" required="false" %>
<%@ attribute name="globalElements" fragment="true" required="false" %>

<html>
	<head>
		<link rel="SHORTCUT ICON" href="img/favicon.png" />
		<link rel="stylesheet" href="styles/global.css" type="text/css" title="antech" />
		<link rel="stylesheet" href="styles/print/print.css" type="text/css" media="print" />
		<link rel="stylesheet" href="styles/jQuery/jquery-datatables.css" type="text/css" />
		<link rel="stylesheet" href="styles/jQuery/buttons.dataTables.min.css" type="text/css" />
		<link rel="stylesheet" href="styles/jQuery/responsive.dataTables.min.css" type="text/css" />
		<link rel="stylesheet" href="styles/jQuery/jquery-impromptu.css" type="text/css" />
        <link rel="stylesheet" href="styles/jQuery/jquery-ui.css" type="text/css" media="screen" />
		<sec:csrfMetaTags />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<!-- Disable compatibility view in IE -->
		<meta http-equiv="X-UA-Compatible" content="IE=EDGE" />

		<meta name="_serverTimeZone" content="${cwms:defaultTimeZone()}">
    	<meta name="_contextPath" content="${pageContext.request.contextPath}">
		<meta name="_systemType" content="${systemType}">
		<meta name="_defaultCurrency" content="${defaultCurrency.currencyCode}">

		<meta name="_locale" content="${rc.locale.toLanguageTag()}"/>
		<c:if test="${systemType == 'development'}">
			<link rel="stylesheet" href="styles/theme/test-theme.css" type="text/css" />
		</c:if>
		<%--
			Note: Order of script loading is important for application initialization and should remain as follows:
			1) scriptPart typically loads page - specific javascript including pageImports.
			2) Global variables initialized from page context
			3) jQuery loaded
			4) jQuery migration library loaded
			5) Cwms_Main.js loaded, which then loads pageImports from step 1 
			Finally, when page fully loaded in DOM, loadScript.init - in Cwms_Main.js is called from body.onload(...)
		--%>
		<script type="module" src="script/tools/cwms-main-module.js"></script>
		<script type="module" src="script/components/cwms-datetime/cwms-datetime.js"></script>
		<jsp:invoke fragment="scriptPart"/>
		<script>
			// set variable to default context of project (used in javascript)
			var springUrl = '${req.contextPath}';
			// get value of loadtab parameter from querystring
			// used to show the correct tab when subnavigation bar is used
			var LOADTAB = '${loadtab}';
			// retrieves application session maximum inactive interval
			var sessionTimeout = ${request.session.getMaxInactiveInterval() / 60};
			// get allocated subdivision from session
			var allocatedSubdivId = ${sessionScope.allocatedSubdiv.key};
			// get user preferences
			var userPrefs = {
				/* tooltips turned on? */
				tooltips: ${currentContact.userPreferences.mouseoverActive},
				/* job item key nav on? */
				jikeynav: ${currentContact.userPreferences.jiKeyNavigation}
			};
			// set variable to i18n of javascript, used also by the cwms-translate custom element.
			var i18locale = '${rc.locale.language}';
			
			var cwmsVersion = '${sessionScope.version}';
		</script>
		<script src='script/thirdparty/jQuery/jquery.js'></script>
		<script type='text/javascript' src='script/thirdparty/jQuery/jquery.ui.js'></script>
		<script src='script/thirdparty/jQuery/jquery-migrate-1.2.1.js'></script>
		<script src='script/trescal/core/template/Cwms_Main.js'></script>		
		<title><spring:message code="include.title" /></title>
	</head>
	<body onload="loadScript.init(true);">
	<jsp:invoke fragment="globalElements"/>
		<div class="banner-column1"></div>

		<div class="banner-column3">
			<div>
				<div class="quickScan">
					<form:form id="quickScanForm" action="quickScan.htm">
						<span class="scantext"><spring:message code="include.quickscan"/></span>
						<span>'alt z'</span>
						<input type="text" name="scan" size="12" maxlength="10" tabindex="-1" accesskey="z" />
					</form:form>
				</div>
				<div id="loginBox">
					<spring:message code="include.loggedinas" />
					<strong>
						<a href="viewperson.htm?personid=${currentContact.personid}" style="color: white">${currentContact.name}</a>
					</strong>&nbsp;<a href="" onclick="event.preventDefault(); logout();"><img src="img/icons/logout.png" class="image_inline" width="16" height="16" alt="<spring:message code="include.logout"/>" title="<spring:message code="include.logout"/>" /></a>
					<form:form id="logout" action="logout" method="post" class="hid">
						<input type="submit" value="logout"/>
					</form:form>
				</div>
				<form:form action="changeallocatedsubdiv" method="POST" id="allocatedSubdivForm">
					<div id="subdivBox">
						<span class="headtextsmall">${allocatedCompany.value}</span>
						<br/>
						<div style="text-align:right">
							<select style="background-color: transparent; color: white" id="allocatedSubdivid" name="allocatedSubdivid">
								<c:forEach var="company" items="${availableSubdivs.keySet()}">
									<optGroup style="color: black" label="${company.value}">
										<c:forEach var="subdiv" items="${availableSubdivs.get(company)}">
											<c:choose>
												<c:when test="${subdiv.key == allocatedSubdiv.key}">
													<option style="color: black" value="${subdiv.key}" selected>${subdiv.value}</option>
												</c:when>
												<c:otherwise>
													<option style="color: black" value="${subdiv.key}">${subdiv.value}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</optGroup>
								</c:forEach>
							</select>
						</div>
					</div>
				</form:form>
			</div>
		</div>
		
		<div class="banner-column2"></div>
		
		<!--	Navigation Coding	-->
		<div id="navigation" class="navigation-holder">
			<div class="navtitle">
				<p><spring:message code='menu.admin'/></p>
			</div>
			<div class="menu">
				<ul>
					<!-- Removing for now the <span class="accesslink"></span> around special characters for localization -->
					<spring:message var="companiesManagementMessage" code="menu.company"/>
					<cwms:secNavParentLink permission="COMPANY_SEARCH" message="${companiesManagementMessage}">
						<cwms:secNavChildLink permission="COMPANY_SEARCH"><spring:message code='menu.company.search'/></cwms:secNavChildLink>	
						<cwms:secNavChildLink permission="COMPANY_LIST_SEARCH" classAttr="even"><spring:message code='menu.companylist.search'/></cwms:secNavChildLink>
					</cwms:secNavParentLink>
					
					<cwms:secNavMainLink permission="CONTACT_SEARCH"><spring:message code="menu.contact"></spring:message></cwms:secNavMainLink>
						
						<spring:message var="capabilitiesManagementMessage" code="menu.capabilities"/>
					<cwms:secNavParentLink permission="CAPABILITIES_SEARCH" message="${capabilitiesManagementMessage}">
						<cwms:secNavChildLink permission="CAPABILITIES_SEARCH" classAttr="even"><spring:message code='menu.capabilities.search' /></cwms:secNavChildLink>
						<cwms:secNavChildLink permission="CAPABILITIES_BY_DOMAIN_VIEW"><spring:message code='menu.capabilities.viewbydomain'/></cwms:secNavChildLink>
						<cwms:secNavChildLink permission="CAPABILITIES_BY_DEPARTMENT_VIEW" classAttr="even"><spring:message code='menu.capabilities.viewbydepartment'/></cwms:secNavChildLink>
						<cwms:secNavChildLink permission="CAPABILITY_ADD" ><spring:message code='menu.capabilities.add'/></cwms:secNavChildLink>
						<cwms:secNavChildLink permission="CAPABILITY_AUTHORIZATION" classAttr="even"><spring:message code='menu.capabilities.authorisations' /></cwms:secNavChildLink>
					</cwms:secNavParentLink>
	
					<spring:message var="modelManagementMessage" code="menu.models"/>
					<cwms:secNavParentLink permission="MODEL_SEARCH" message="${modelManagementMessage}">
						<cwms:secNavChildLink permission="MODEL_SEARCH"><spring:message code='menu.models'/></cwms:secNavChildLink>	
						<cwms:secNavChildLink permission="REQUIREMENTS_MODEL_ADD_SUBFAMILY" classAttr="even"><spring:message code='menu.models.addsubfamilyrequirements'/></cwms:secNavChildLink>	
						<cwms:secNavChildLink permission="REQUIREMENTS_MODEL_ADD"><spring:message code='menu.models.addrequirements'/></cwms:secNavChildLink>	
						<cwms:secNavChildLink permission="THREADS_MODEL_SEARCH" classAttr="even"><spring:message code='menu.models.searchthreads'/></cwms:secNavChildLink>	
						<cwms:secNavChildLink permission="INSTRUMENT_DOMAIN_MODEL"><spring:message code='menu.models.managedomain'/></cwms:secNavChildLink>	
						<cwms:secNavChildLink permission="INSTRUMENT_FAMILY_MODEL" classAttr="even"><spring:message code='menu.models.managefamily'/></cwms:secNavChildLink>	
						<cwms:secNavChildLink permission="DESCRIPTION_MODEL_SEARCH"><spring:message code='menu.models.searchdescription'/></cwms:secNavChildLink>	
						<cwms:secNavChildLink permission="SALES_CATEGORY_MODEL" classAttr="even"><spring:message code='menu.models.managesalescategory'/></cwms:secNavChildLink>	
							
					</cwms:secNavParentLink>
					
					<spring:message var="instrumentManagementMessage" code="menu.instruments"/>
					<cwms:secNavParentLink permission="INSTRUMENT_SEARCH" message="${instrumentManagementMessage}">
						<cwms:secNavChildLink permission="INSTRUMENT_SEARCH"><spring:message code='menu.instruments.search'/></cwms:secNavChildLink>	
						<cwms:secNavChildLink permission="INSTRUMENT_MODEL_ADD_AND_SEARCH" classAttr="even"><spring:message code='menu.instruments.add'/></cwms:secNavChildLink>
						<cwms:secNavChildLink permission="CHECK_OUT_MAIN"><spring:message code='menu.instruments.checkoutmain'/></cwms:secNavChildLink>
						<cwms:secNavChildLink permission="CHECK_OUT_SEARCH" classAttr="even"><spring:message code='menu.instruments.checkoutsearch'/></cwms:secNavChildLink>
					</cwms:secNavParentLink>
					
					<cwms:secNavMainLink permission="RECALL_VIEW"><spring:message code="menu.recall"></spring:message></cwms:secNavMainLink>

					<spring:message var="contractsManagementMessage" code="menu.contracts"/>
					<cwms:secNavParentLink permission="CONTRACT_SEARCH" message="${contractsManagementMessage}">
						<cwms:secNavChildLink permission="CONTRACT_SEARCH"><spring:message code="menu.contract.search"/></cwms:secNavChildLink>
						<cwms:secNavChildLink permission="CONTRACT_ADD" classAttr="even"><spring:message code="menu.contract.add"/></cwms:secNavChildLink>
					</cwms:secNavParentLink>

					<spring:message var="quoteManagementMessage" code="menu.quotations"/>
					<cwms:secNavParentLink permission="QUOTE_CREATE" message="${quoteManagementMessage}">
						<cwms:secNavChildLink permission="QUOTE_CREATE"><spring:message code='menu.quotations'/></cwms:secNavChildLink>	
						<cwms:secNavChildLink permission="QUOTE_SCHEDULED_REQUEST" classAttr="even"><spring:message code='menu.quotations.scheduled'/></cwms:secNavChildLink>	
					</cwms:secNavParentLink>
					
					<spring:message var="logisticInManagementMessage" code="menu.logisticsin"/>
					<cwms:secNavParentLink permission="JOB_ADD" message="${logisticInManagementMessage}">
						<cwms:secNavChildLink permission="JOB_ADD" id="Goods_In"><spring:message code='menu.goods.in'/></cwms:secNavChildLink>	
						<cwms:secNavChildLink permission="PREBOOKING_MANAGE" classAttr="even"><spring:message code='menu.prebooking'/></cwms:secNavChildLink>	
						<cwms:secNavChildLink permission="SCANIN_LOGISTIC" ><spring:message code='menu.logisticsin.scanin'/></cwms:secNavChildLink>	
					</cwms:secNavParentLink>
					
					<spring:message var="jobManagementMessage" code="menu.jobs"/>
					<cwms:secNavParentLink permission="JOB_SEARCH" message="${jobManagementMessage}">
						<cwms:secNavChildLink permission="JOB_SEARCH"><spring:message code='menu.jobs.search'/></cwms:secNavChildLink>
						<li><a href="home.htm" class="even"><spring:message code='menu.jobs.active'/></a></li>	
						<cwms:secNavChildLink permission="DASHBOARD_WORK_BY_DEPARTMENT" id="Job_item_dashboard"><spring:message code='menu.workflow.jobprogress'/></cwms:secNavChildLink>
						<cwms:secNavChildLink permission="ALLOCATE_WORK_ENGINEER" classAttr="even"><spring:message code='menu.workflow.allocate'/></cwms:secNavChildLink>	
						<cwms:secNavChildLink permission="USER_TASK_VIEW" ><spring:message code='menu.workflow.usertask'/></cwms:secNavChildLink>	
						<cwms:secNavChildLink permission="CLEANING_AND_INSPECTION" classAttr="even"><spring:message code='menu.workflow.cleaninginspection'/></cwms:secNavChildLink>	
						<cwms:secNavChildLink permission="CLEANING_LIST" ><spring:message code='menu.workflow.cleaninglist'/></cwms:secNavChildLink>	
						<cwms:secNavChildLink permission="SUSPENDED_JOB_ITEM" classAttr="even"><spring:message code='menu.jobs.calledoff'/></cwms:secNavChildLink>			
					</cwms:secNavParentLink>



					<spring:message var="purchasingManagementMessage" code="menu.purchasing"/>
					<cwms:secNavParentLink permission="SUPPLIER_PURCHASE_ORDER" message="${purchasingManagementMessage}">
						<cwms:secNavChildLink permission="SUPPLIER_PURCHASE_ORDER"><spring:message code='menu.purchasing.supplierpo'/></cwms:secNavChildLink>	
						<cwms:secNavChildLink permission="SUPPLIER_QUOTATION_REQUEST" classAttr="even"><spring:message code='menu.quotations.thirdpartyrequests'/></cwms:secNavChildLink>	
						<cwms:secNavChildLink permission="SUPPLIER_QUOTATION" ><spring:message code='menu.quotations.thirdparty'/></cwms:secNavChildLink>	
					</cwms:secNavParentLink>
					
					<spring:message var="scanOutManagementMessage" code="menu.logisticsout"/>
					<cwms:secNavParentLink permission="SCAN_OUT" message="${scanOutManagementMessage}">
						<cwms:secNavChildLink permission="SCAN_OUT" id ="Scan_Out"><spring:message code='menu.logisticsout.scanout'/></cwms:secNavChildLink>	
						<cwms:secNavChildLink permission="DELIVERY_NOTE_QUICK_CREATE" classAttr="even"><spring:message code='menu.logisticsout.quickdeliverynote'/></cwms:secNavChildLink>	
						<cwms:secNavChildLink permission="TRANSPORT_SCHEDULES_SEARCH"><spring:message code='menu.goods.schedule'/></cwms:secNavChildLink>	
						<cwms:secNavChildLink permission="DELIVERY_SEARCH" classAttr="even"><spring:message code='menu.goods.delivery'/></cwms:secNavChildLink>	
						<cwms:secNavChildLink permission="COURIER_DESPATCHES_SEARCH"><spring:message code='menu.goods.courier'/></cwms:secNavChildLink>	
						<cwms:secNavChildLink permission="CLIENT_GOOD_OUT_VIEW" classAttr="even"><spring:message code='menu.goods.awaitingclient'/></cwms:secNavChildLink>	
						<cwms:secNavChildLink permission="GOOD_OUT_VIEW"><spring:message code='menu.goods.awaitingthirdparty'/></cwms:secNavChildLink>	
						<cwms:secNavChildLink permission="GOOD_OUT_SCHEDULE_UPDATER"><spring:message code='menu.goods.scheduleupdater'/></cwms:secNavChildLink>		
					</cwms:secNavParentLink>

					<spring:message var="timeManagementMessage" code="menu.workingtime"/>
					<cwms:secNavParentLink permission="TIMESHEET" message="${timeManagementMessage}">
						<cwms:secNavChildLink permission="TIMESHEET"><spring:message code='menu.timesheet'/></cwms:secNavChildLink>
						<cwms:secNavChildLink permission="TIMESHEET_VALIDATION" classAttr="even"><spring:message code='menu.timesheetvalidation'/></cwms:secNavChildLink>
					</cwms:secNavParentLink>
					
					<spring:message var="invoiceManagementMessage" code="menu.invoicing"/>
					<cwms:secNavParentLink permission="INVOICE_HOME" message="${invoiceManagementMessage}">
						<cwms:secNavChildLink permission="INVOICE_HOME"><spring:message code='menu.invoicing.home'/></cwms:secNavChildLink>
						<cwms:secNavChildLink permission="CREDIT_NOTE_HOME" classAttr="even"><spring:message code='menu.invoicing.creditnotehome'/></cwms:secNavChildLink>
					</cwms:secNavParentLink>
				
					<spring:message var="certificatesMessage" code="menu.certificate"/>
					<cwms:secNavParentLink permission="CERTIFICATE_SEARCH" message="${certificatesMessage}">
						<cwms:secNavChildLink permission="CERTIFICATE_SEARCH"><spring:message code='menu.search.certificates'/></cwms:secNavChildLink>
						<cwms:secNavChildLink permission="REVERCE_TRACEABILITY_SEARCH"><spring:message code='menu.search.reversetraceability'/></cwms:secNavChildLink>
						<cwms:secNavChildLink permission="STANDARDS_USAGE_ANALYSIS_SEARCH"><spring:message code='menu.search.standardsusage'/></cwms:secNavChildLink>
					</cwms:secNavParentLink>
					
					<cwms:secNavMainLink permission="EMAIL_SEARCH" ><spring:message code="menu.email"></spring:message></cwms:secNavMainLink>
					<cwms:secNavMainLink permission="CALIBRATION_SEARCH"><spring:message code="menu.calibration"></spring:message></cwms:secNavMainLink>
	
					
					<!-- Hide for Madrid 
					<li><a href="<c:url value="/searchmodelcomponentform.htm"/>"><spring:message code="menu.stockitems"/><span class="menuoptions">&nbsp;</span></a>
						<ul>
							<li><a href="<c:url value="/searchmodelcomponentform.htm"/>"><spring:message code="menu.stockitems.search"/></a></li>
							<li class="even"><a href="<c:url value="/editmodelcomponent.htm"/>"><spring:message code="menu.stockitems.add"/></a></li>
						</ul>
					</li>
					 -->
					<%-- <li><a href="<c:url value="/importinstruments.htm"/>"><spring:message code="menu.instruments.import"/></a></li> --%>
					<!-- Hide for Madrid -->
					<!-- <li class="even"><a href="<c:url value="/searchwi.htm"/>"><spring:message code="menu.calibration.searchworkinstructions"/></a></li> -->
				</ul>
			</div>
			<div class="navtitle">
				<p><spring:message code="menu.tools"/></p>
			</div>
			<div class="menu">
				<ul>
					<!-- Hide for Madrid -->
					<%--<li><a href="<c:url value="/assetsearch.htm"/>"><spring:message code="menu.asset"/><span class="menuoptions">&nbsp;</span></a>
						<ul>
							<li><a href="<c:url value="/assetsearch.htm"/>"><spring:message code="menu.asset.search"/></a></li>
							<li class="even"><a href="<c:url value="/addeditasset.htm"/>"><spring:message code="menu.asset.add"/></a></li>
							<li><a href="<c:url value="/newassetfrominst.htm"/>"><spring:message code="menu.asset.addinstrument"/></a></li>
						</ul>
					</li>--%>
					<cwms:secNavMainLink permission="REPORTS"><spring:message code="menu.reports"></spring:message></cwms:secNavMainLink>
					
					<spring:message var="toolsMessage" code="menu.tools"/>
					<cwms:secNavParentLink permission="TOOLS" message="${toolsMessage}">
						<cwms:secNavChildLink permission="TOOLS"><spring:message code='tools.labelprinting'/></cwms:secNavChildLink>	
						<cwms:secNavChildLink permission="IMPORT_CALIBRATIONS"><spring:message code='menu.jobs.importoperations'/></cwms:secNavChildLink>
						<cwms:secNavChildLink permission="IMPORT_OPERATIONS_BATCH"><spring:message code='menu.jobs.importoperationsbatch'/></cwms:secNavChildLink>
						<cwms:secNavChildLink permission="IMPORT_VDI_CALIBRATIONS"><spring:message code='menu.jobs.importvdicalibrations'/></cwms:secNavChildLink>
						<cwms:secNavChildLink permission="IMPORT_INSTRUMENTS"><spring:message code='menu.instruments.import'/></cwms:secNavChildLink>
						<cwms:secNavChildLink permission="BG_TASKS_VIEW"><spring:message code='menu.jobs.bgtasks'/></cwms:secNavChildLink>
					</cwms:secNavParentLink>
					
					<cwms:secNavMainLink permission="ADMIN_AREA">
						<spring:message code="menu.adminarea"/>
					</cwms:secNavMainLink>
				</ul>
			</div>
			<div class="navtitle">
				<p><spring:message code="menu.hire"/></p>
			</div>
			<div class="menu">
				<ul>
					<cwms:secNavMainLink permission="HIRE_HOME"><spring:message code="menu.hire"></spring:message></cwms:secNavMainLink>
				</ul>
			</div>
		</div>
		
		<!--	Main Content Coding	-->	
		<div id="maincontent" class="maincontent">
			<div class="contenthead">
				<span class="headsearch">
					<a id="collapseLink" class="vis" onclick="loadScript.menuVisibility('collapse');">
						<img src="img/icons/contract.png" width="16" height="16" alt="<spring:message code="include.collapseandhidenavigationbar"/>" title="<spring:message code="include.collapseandhidenavigationbar"/>" />
					</a>
					<a id="expandLink" class="hid" onclick="loadScript.menuVisibility('expand');">
						<img src="img/icons/expand.png" width="16" height="16" alt="<spring:message code="include.expandanddisplaynavigationbar"/>" title="<spring:message code="include.expandanddisplaynavigationbar"/>" />
					</a>
				</span>
				<span class="headscan">
					<!-- this code displays the current user's locale --> 
					<a style="color:white; text-transform:capitalize;" href="changelocale.htm"> 
					    <%= response.getLocale().getDisplayLanguage(response.getLocale()) %>
				    	(<%= response.getLocale().getDisplayCountry(response.getLocale()) %>)
					</a>
					<a href="#" onclick=" event.preventDefault(); window.location='addfeedback.htm?url=' + window.location.href + ''; " >
						<img src="img/icons/feedback.png" width="16" height="16" alt="<spring:message code="include.providefeedback"/>" title="<spring:message code="include.providefeedback"/>" />
					</a>
				</span>
				<jsp:invoke fragment="header"></jsp:invoke>
			</div>
			<div class="contentframe">
				<div class="contentbody">
					<jsp:doBody/>
				</div>
			</div>
			<jsp:useBean id="date" class="java.util.Date" />
			<div class="footer">
				<div>
					<c:out value="Trescal Group ERP "/>
					&copy;
					<fmt:formatDate value="${date}" pattern="yyyy" />
				</div>
				<div>
					<c:out value="Version: ${sessionScope.version}"/>
				</div>
				<c:if test="${System.getProperty('environment') eq 'atlas-develop'}">
					<div>
						<c:out value="n(q) : "/>
						<span id="querycounttotal"><dsp:metrics metric="total" /></span>
						<c:out value=", t(q) : "/>
						<span id="querytime"><dsp:metrics metric="time" /></span>
						<c:out value=" ms"/>
					</div>
				</c:if>
			</div>
		</div>
	</body>
</html>