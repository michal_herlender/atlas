<%@ tag language="java" display-name="showCompanyInvoiceRequirements" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%-- Company is a Company object, the rest are boolean parameters (from system defaults) --%>
<%@ attribute name="company" required="false" type="java.lang.Object" %>
<%@ attribute name="companyname" required="false" type="java.lang.String" %>
<%@ attribute name="monthlyInvoice" required="true" type="java.lang.Object" %>
<%@ attribute name="poInvoice" required="true" type="java.lang.Object" %>
<%@ attribute name="separateCostsInvoice" required="true" type="java.lang.Object" %>

<c:set var="invreq" value="${false}"/>
<c:if test="${monthlyInvoice || separateCostsInvoice  || poInvoice}">
	<c:set var="invreq" value="${true}"/>
</c:if>
<c:if test="${companyname == null}">
	<c:set var="companyname" value="${company.coname}"/>
</c:if>
<table class="default4 <c:if test='${invreq}'>highlight_table</c:if>">
	<thead>
		<tr>
			<th>
				<spring:message code="invoice.thiscompanyhasthefollowinginvoicerequirements" />
			</th>
		</tr>
	</thead>
	<tbody>
		<c:if test="${monthlyInvoice}">
			<tr>													
				<td><spring:message code="invoice.requiresamonthlyinvoice" arguments="${companyname}"/></td>
			</tr>
		</c:if>
		<c:if test="${poInvoice}">
			<tr>													
				<td><spring:message code="invoice.requiresaseparateinvoiceperpurchaseorder" arguments="${companyname}"/></td>
			</tr>
		</c:if>
		<c:if test="${separateCostsInvoice}">
			<tr>													
				<td><spring:message code="invoice.requiresaseparateinvoiceforcalibrationandothercosts" arguments="${companyname}"/></td>
			</tr>
		</c:if>
		<c:if test="${!invreq}">
			<tr>
				<td><spring:message code="invoice.nospecialinvoicerequirements"/></td>
			</tr>
		</c:if>
	</tbody>
</table>