<%@ tag language="java" display-name="showTabbedNotes" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="entity" required="true" type="java.lang.Object" %>
<%@ attribute name="noteTypeId" required="true" type="java.lang.Object" %>
<%@ attribute name="noteType" required="true" type="java.lang.Object" %>
<%@ attribute name="privateOnlyNotes" required="true" type="java.lang.Object" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<script>
	if (!typeof privateOnlyNotes != 'undefined') {
		// assign global variable for private only notes
		var privateOnlyNotes = ${privateOnlyNotes};
	}
	// load note javascript file
	loadScript.loadScripts(new Array('script/trescal/core/utilities/Note.js') );
	<c:choose>
		<c:when test="${privateOnlyNotes}">
			var addCommandNotes = 'addPrivate';
			/* this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
			 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
			 */
			var menuElementsNotes = new Array(
					{ anchor: 'private-link', block: 'privateA-tab' },
					{ anchor: 'privatedel-link', block: 'privateD-tab' } );
		</c:when>
		<c:otherwise>
			/* this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
			 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
			 */
			 var addCommandNotes = 'addPublic';
			 var menuElementsNotes = new Array(
					 { anchor: 'public-link', block: 'publicA-tab' },
					 { anchor: 'publicdel-link', block: 'publicD-tab' },
					 { anchor: 'private-link', block: 'privateA-tab' },
					 { anchor: 'privatedel-link', block: 'privateD-tab' } );
		</c:otherwise>
	</c:choose>
</script>

<div class="infobox">
	<a href="#" id="addNoteModal" class="mainlink-float" onclick=" event.preventDefault(); addNoteBoxy(addCommandNotes, false,'${noteType}', ${noteTypeId}, ${currentContact.personid}, 0); " title="<spring:message code="notes.addnote" />"><spring:message code="notes.addnote" /></a>
	<!-- this div clears any floats and provides space in between elements -->
	<div class="clear"></div>
	<div id="tabmenu">		
		<ul class="subnavtab">
			<c:if test="${!privateOnlyNotes}">
				<li>
					<a href="#" id="public-link" onclick=" event.preventDefault(); addCommandNotes='addPublic'; switchMenuFocus(menuElementsNotes, 'publicA-tab', false); " class="selected">
						<spring:message code="notes.publicnotes"/>
						(<span id="publicA-count">${entity.publicActiveNoteCount}</span>)
					</a>
				</li>
				<li>
					<a href="#" id="publicdel-link" onclick=" event.preventDefault(); addCommandNotes='addPublic'; switchMenuFocus(menuElementsNotes, 'publicD-tab', false); ">
						<spring:message code="notes.publicnotesdeleted" />
						(<span id="publicD-count">${entity.publicDeactivatedNoteCount}</span>)
					</a>
				</li>
			</c:if>
			<li>
				<a href="#" id="private-link" onclick=" event.preventDefault(); addCommandNotes='addPrivate'; switchMenuFocus(menuElementsNotes, 'privateA-tab', false); " <c:if test="${privateOnlyNotes}">class="selected"</c:if>>
					<spring:message code="notes.privatenotes" />
					(<span id="privateA-count">${entity.privateActiveNoteCount}</span>)
				</a>
			</li>
			<li>
				<a href="#" id="privatedel-link" onclick=" event.preventDefault(); addCommandNotes='addPrivate'; switchMenuFocus(menuElementsNotes, 'privateD-tab', false); ">
					<spring:message code="notes.privatenotesdeleted" />
					(<span id="privateD-count">${entity.privateDeactivatedNoteCount}</span>)
				</a>
			</li>
		</ul>
		<div class="tab-box">
			<!--  display these notes if the user has passed either "public" or "both" -->
			<c:if test="${!privateOnlyNotes}">
				<!-- this fieldset displays any currently active public notes -->
				<fieldset id="publicA-tab">
					<legend><spring:message code="notes.publicnotesdesc" /></legend>
					<ol id="publicA-list">
						<c:if test="${entity.publicActiveNoteCount < 1}">
							<li><spring:message code="notes.privatenotesempty" /></li>
						</c:if>
						<c:forEach var="note" items="${entity.notes}">
							<c:if test="${note.publish && note.active}">
								<li id="note${note.noteid}">
									<div class="notes_note">
										<c:if test='${note.label != ""}'>
											<strong><span class="notelabel_pub${note.noteid}"><c:out value="${note.label}"/></span></strong><br />
										</c:if>
										<span class="note_pub${note.noteid}">${note.note.replaceAll("\\n","")}</span>
									</div>
									<div class="notes_editor">${note.setBy.name}<br /><fmt:formatDate value="${note.setOn}" dateStyle="medium" type="both" timeStyle="short" /></div>
									<div class="notes_edit">
										<!-- only show link if both public and private notes are displayed -->
										<span class="publishLink">
											(&nbsp;
											<a href="#" class="mainlink" onclick=" event.preventDefault(); publish(${note.noteid}, ${false}, ${currentContact.personid}, '${noteType}', 0, '', publicA); ">
												<spring:message code="notes.makeprivate" />
											</a>
											&nbsp;)
										</span>
										<a href="#" onclick=" event.preventDefault(); addNoteBoxy('edit', false, '${noteType}', 0, ${currentContact.personid}, ${note.noteid}, 0); ">
											<img src="img/icons/note_edit.png" width="16" height="16" alt="<spring:message code="notes.editnote" />" title="<spring:message code="notes.editnote" />" class="noteimg_padded" />
										</a>
										<a href="#" onclick=" event.preventDefault(); changeNoteStatus(${note.noteid}, false, ${currentContact.personid}, '${noteType}', '', publicA); ">
											<img src="img/icons/note_delete.png" width="16" height="16" alt="<spring:message code="notes.deletenote" />" title="<spring:message code="notes.deletenote" />" class="noteimg_padded" />
										</a>
									</div>
									<div class="clear-0"></div>
								</li>
							</c:if>
						</c:forEach>
					</ol>
				</fieldset>
				<!-- this div clears any floats and provides space in between elements -->
				<div class="clear"></div>
				<!-- this fieldset contains any deleted public notes -->
				<fieldset id="publicD-tab" class="hid">
					<legend><spring:message code="notes.publicnotesdeleted" /></legend>
					<ol id="publicD-list">
						<c:choose>
							<c:when test="${entity.publicDeactivatedNoteCount > 0}">
								<c:forEach var="note" items="${entity.notes}">
									<c:if test="${note.publish && !note.active}">
										<li id="note${note.noteid}">
											<div class="notes_note">
												<c:if test='${note.label != ""}'>
													<strong><c:out value="${note.label}"/>:</strong><br />
												</c:if>
												${note.note.replaceAll("\\n","")}
											</div>
											<div class="notes_editor">${note.setBy.name}<br /><fmt:formatDate value="${note.setOn}" dateStyle="medium" type="both" timeStyle="short" /></div>
											<div class="notes_edit">
												(&nbsp;
												<a href="#" class="mainlink" title="<spring:message code="notes.reactivate" />" onclick=" event.preventDefault(); changeNoteStatus(${note.noteid}, true, ${currentContact.personid}, '${noteType}', '', publicD); ">
													<spring:message code="notes.reactivate" />
												</a>
												&nbsp;)
											</div>
											<div class="clear-0"></div>
										</li>
									</c:if>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<li><spring:message code="notes.privatenotesempty" /></li>
							</c:otherwise>
						</c:choose>
					</ol>
				</fieldset>
				<!-- this div clears any floats and provides space in between elements -->
				<div class="clear"></div>
			</c:if>
			<!-- this fieldset displays any currently active private notes -->
			<c:set var="visPrivate" value="vis"/>
			<c:if test="${!privateOnlyNotes}">
				<c:set var="visPrivate" value="hid"/>
			</c:if>
			<fieldset id="privateA-tab" class="${visPrivate}">
				<legend><spring:message code="notes.privatenotesdesc" /></legend>
				<ol id="privateA-list">
					<c:if test="${entity.privateActiveNoteCount < 1}">
						<li><spring:message code="notes.privatenotesempty" /></li>
					</c:if>
					<c:forEach var="note" items="${entity.notes}">
						<c:if test="${!note.publish && note.active}">
							<li id="note${note.noteid}">
								<div class="notes_note">
									<c:if test='${note.label != ""}'>
										<strong><span class="notelabel_priv${note.noteid}">
										<c:out value="${note.label}"/>
										</span></strong><br />
									</c:if>
									<span class="note_priv${note.noteid}">${note.note.replaceAll("\\n","")}</span>
								</div>
								<div class="notes_editor">${note.setBy.name}<br /><fmt:formatDate value="${note.setOn}" dateStyle="medium" type="both" timeStyle="short" /></div>
								<div class="notes_edit">
									<!-- only show link if both public and private notes are displayed -->
									<c:if test="${!privateOnlyNotes}">
										<span class="publishLink">
											(&nbsp;
											<a href="#" class="mainlink" onclick=" event.preventDefault(); publish(${note.noteid}, true, ${currentContact.personid}, '${noteType}', 0, '', privateA); ">
												<spring:message code="notes.makepublic" />
											</a>
											&nbsp;)
										</span>
									</c:if>
									<a href="#" onclick=" event.preventDefault(); addNoteBoxy('edit', false, '${noteType}', 0, ${currentContact.personid}, ${note.noteid}, 0); ">
										<img src="img/icons/note_edit.png" width="16" height="16" alt="<spring:message code="newquotevers.editnote" />" title="<spring:message code="newquotevers.editnote" />" class="noteimg_padded" />
									</a>
									<a href="#" class="mainlink" onclick=" event.preventDefault(); changeNoteStatus(${note.noteid}, false, ${currentContact.personid}, '${noteType}', '', privateA); ">
										<img src="img/icons/note_delete.png" width="16" height="16" alt="<spring:message code="notes.deletenote" />" title="<spring:message code="notes.deletenote" />" class="noteimg_padded" />
									</a>
								</div>
								<div class="clear-0"></div>
							</li>
						</c:if>
					</c:forEach>
				</ol>
			</fieldset>
			<!-- this div clears any floats and provides space in between elements -->
			<div class="clear"></div>
			<!-- this fieldset contains any deleted private notes -->
			<fieldset id="privateD-tab" class="hid">
				<legend><spring:message code="notes.privatenotesdeleted" /></legend>
				<ol id="privateD-list">
					<c:choose>
						<c:when test="${entity.privateDeactivatedNoteCount > 0}">
							<c:forEach var="note" items="${entity.notes}">
								<c:if test="${!note.publish && !note.active}">
									<li id="note${note.noteid}">
										<div class="notes_note">
											<c:if test='${note.label != ""}'>
												<strong>
													<c:out value="${note.label}"/>
												</strong><br />
											</c:if>
												${note.note.replaceAll("\\n","")}
										</div>
										<div class="notes_editor">${note.setBy.name}<br /><fmt:formatDate value="${note.setOn}" dateStyle="medium" type="both" timeStyle="short" /></div>
										<div class="notes_edit">
											(&nbsp;
											<a href="#" class="mainlink" onclick=" event.preventDefault(); changeNoteStatus(${note.noteid}, true, ${currentContact.personid}, '${noteType}', '', privateD); ">
												<spring:message code="notes.reactivate" />
											</a>
											&nbsp;)
										</div>
										<div class="clear-0"></div>											
									</li>
								</c:if>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<li><spring:message code="notes.privatenotesempty" /></li>
						</c:otherwise>
					</c:choose>
				</ol>
			</fieldset>
		</div>
	</div>
	<!-- this div clears any floats and provides space in between elements -->
	<div class="clear"></div>
</div>