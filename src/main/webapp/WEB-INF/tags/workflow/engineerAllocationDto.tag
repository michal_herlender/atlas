<%@ tag language="java" display-name="engineerAllocation" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%@ attribute name="eaDto" required="true" type="java.lang.Object" %>
<%@ attribute name="canEdit" required="true" type="java.lang.Boolean" %>

<div class="relative" style="min-height: 20px;" id="allocated-${eaDto.jobItemId}">
	<div style="width: 70px; float: right; text-align: right;">
		<c:choose>
			<c:when test="${canEdit}">
				<a href="#" class="mainlink"
					onclick="event.preventDefault(); deallocateItem(${eaDto.jobItemId});">
					<c:out value="${eaDto.allocatedByInitials} " />
					<fmt:formatDate value="${eaDto.allocatedOn}" pattern="dd/MM" />
				</a>
			</c:when>
			<c:otherwise>
				<c:out value="${eaDto.allocatedByInitials} " />
				<fmt:formatDate value="${eaDto.allocatedOn}" pattern="dd/MM" />
			</c:otherwise>
		</c:choose>
	</div>
	<div style="position: absolute; left: 0; top: 0; width: 98px;">
		${eaDto.capabilityReference}
	</div>
	<div style="margin-left: 104px; margin-right: 80px;">
		<a href="viewworkforuser.htm?personid=${eaDto.allocatedToId}"
			class="mainlink"
			title="<spring:message code='workflow.viewallitemsallocatedto'/> ${eaDto.allocatedToName}">
			${eaDto.allocatedToShortenedName}
		</a>:
		<a href="viewjob.htm?jobid=${eaDto.jobId}" class="mainlink" target="_blank">
			${eaDto.jobNumber}
		</a> - ${eaDto.jobItemNumber}: ${eaDto.modelName}
		<c:forEach var="message" items="${eaDto.messages}">
			<div style="width: 80%; border-top: 1px dotted black;">
				${message.setByInitials}
				(<fmt:formatDate value="${message.setOn}" pattern="dd.MM" />):&nbsp;
				<em>${message.message}</em>
			</div>
		</c:forEach>
	</div>
	<div class="clear"></div>
</div>