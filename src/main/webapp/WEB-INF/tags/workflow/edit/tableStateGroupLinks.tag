<%@ tag language="java" display-name="tableItemStateTranslations" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ attribute name="itemState" required="true" type="java.lang.Object" %>

<%-- Displays a table of state group links for an item state (activity or status) --%>

<table class="default2">
	<thead>
		<tr>
			<td colspan="6">
				State Group Links:
			</td>
		</tr>
		<tr>
			<th>Link ID:</th>
			<th>Link Type:</th>
			<th>State Group ID:</th>
			<th>Key Name:</th>
			<th>Display Name:</th>
			<th>Type:</th>
		</tr>
	</thead>			
	<tbody>
		<c:forEach var="stateGroupLink" items="${itemState.groupLinks}">
		<tr>
			<td><c:out value="${stateGroupLink.id}"/></td>
			<td><c:out value="${stateGroupLink.type}"/></td>
			<td><c:out value="${stateGroupLink.group.id}"/></td>
			<td><c:out value="${stateGroupLink.group.keyName}"/></td>
			<td><c:out value="${stateGroupLink.group.displayName}"/></td>
			<td><c:out value="${stateGroupLink.group.type}"/></td>
		</tr>
		</c:forEach>
	</tbody>
</table>