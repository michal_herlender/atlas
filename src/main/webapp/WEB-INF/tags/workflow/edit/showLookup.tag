<%@ tag language="java" display-name="showLookup" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ attribute name="lookupInstance" required="true" type="java.lang.Object" %>

<span class="lookup">
	${lookupInstance.id} - ${lookupInstance.lookupFunction.getBeanName()}
</span>