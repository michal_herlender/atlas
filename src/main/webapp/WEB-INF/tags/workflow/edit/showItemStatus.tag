<%@ tag language="java" display-name="showItemStatus" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%@ attribute name="itemStatus" required="true" type="java.lang.Object" %>

<span class="status">
	${itemStatus.stateid} -
	
	<cwms:besttranslation translations="${itemStatus.translations}"/>
</span>