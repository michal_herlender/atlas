<%@ tag language="java" display-name="linkLookup" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="workflow" tagdir="/WEB-INF/tags/workflow/edit" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%@ attribute name="hook" required="true" type="java.lang.Object" %>
<%@ attribute name="lastItemActivity" required="false" type="java.lang.Object" %>
<%@ attribute name="lastItemStatus" required="false" type="java.lang.Object" %>
<%@ attribute name="lastLookupInstance" required="false" type="java.lang.Object" %>
<%@ attribute name="lastHook" required="false" type="java.lang.Object" %>

<%-- 
	Note: Optional attributes named differently from the lastActivity, lastStatus, lastLookup instances 
	to avoid undesired instances from scope rather than attributes
--%>

<c:set var="lastLink"><cwms:workflow.lastLink lastActivity="${lastItemActivity}" lastStatus="${lastItemStatus}" lastLookup="${lastLookupInstance}" lastHook="${lastHook}"/></c:set>

<a href="workfloweditor_hook.htm?hookId=${hook.id}${lastLink}" class="mainlink">
	<workflow:showHook hook="${hook}"/>
</a>
