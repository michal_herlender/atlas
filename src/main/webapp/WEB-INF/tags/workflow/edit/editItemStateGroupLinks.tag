<%@ tag language="java" display-name="tableItemStateTranslations" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%-- Displays a table to edit state group links on item status / item activity--%>

<table class="default2">
	<thead>
		<tr>
			<th colspan="3">State Group Links:</th>
		</tr>
		<tr>
			<td>State Group</td>
			<td>Link Type</td>
			<td>Delete</td>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="sglDto" items="${form.stateGroupLinks}" varStatus="sglStatus">
			<tr id="row-sgl-${sglStatus.index}">
				<td>
					<form:hidden path="stateGroupLinks[${sglStatus.index}].id" />
					<form:select path="stateGroupLinks[${sglStatus.index}].stateGroup" items="${stateGroups}" />
				</td>
				<td><form:select path="stateGroupLinks[${sglStatus.index}].linkType" items="${stateGroupLinkTypes}" /></td>
				<td><form:checkbox path="stateGroupLinks[${sglStatus.index}].delete" /></td>
			</tr>
		</c:forEach>
		<tr id="templaterow-sgl" class="hid">
			<td>
				<input type="hidden" id="template-sgl-id" value="0"/> 
				<form:select id="template-sgl-stateGroup" path="defaultStateGroup" items="${stateGroups}" />
			</td>
			<td><form:select id="template-sgl-linkType" path="defaultLinkType" items="${stateGroupLinkTypes}" /></td>
			<td>
				<input type="hidden" id="template-sgl-delete" value="false"/> 
				<img src="img/icons/delete.png" height="20" width="20" alt="Delete" onClick=" deleteInputRow(this); " />
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<div class="float-left"><img src="img/icons/add.png" height="20" width="20" alt="Add State Group Link" onClick=" addStateGroupRow(); " /></div>
				<div class="padtop"><span>&nbsp;Add State Group Link</span></div>
			</td>
		</tr>
	</tbody>
</table>
