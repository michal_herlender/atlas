<%@ tag language="java" display-name="showLookup" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ attribute name="hook" required="true" type="java.lang.Object" %>

<span class="hook">
	${hook.id} - ${hook.name}
</span>