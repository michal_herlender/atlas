<%@ tag language="java" display-name="showActivity" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%@ attribute name="itemActivity" required="true" type="java.lang.Object" %>

<span class="activity">
	${itemActivity.stateid} -

	<cwms:besttranslation translations="${itemActivity.translations}"/>
</span>