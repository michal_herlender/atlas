<%@ tag language="java" display-name="linkOutcomeStatus" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="workflow" tagdir="/WEB-INF/tags/workflow/edit" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%@ attribute name="outcomeStatus" required="true" type="java.lang.Object" %>
<%@ attribute name="lastItemActivity" required="false" type="java.lang.Object" %>
<%@ attribute name="lastItemStatus" required="false" type="java.lang.Object" %>
<%@ attribute name="lastLookupInstance" required="false" type="java.lang.Object" %>

<%-- 
	Note: Optional attributes named differently from the lastActivity, lastStatus, lastLookup instances 
	to avoid undesired instances from scope rather than attributes
--%>

<c:set var="lastLink"><cwms:workflow.lastLink lastActivity="${lastItemActivity}" lastStatus="${lastItemStatus}" lastLookup="${lastLookupInstance}"/></c:set>

<%-- Could have an itemstatus, could have an itemactivity, could have a lookup (or any of the above!) --%>

Outcome Status <c:out value="${outcomeStatus.id}"/> -->

<c:choose>
	<c:when test="${not empty outcomeStatus.lookup}">
		<workflow:linkLookup lookupInstance="${outcomeStatus.lookup}" lastItemActivity="${lastItemActivity}" lastItemStatus="${lastItemStatus}" lastLookupInstance="${lastLookupInstance}"/>
	</c:when>
	<c:when test="${not empty outcomeStatus.status}">
		<workflow:linkItemStatus itemStatus="${outcomeStatus.status}" lastItemActivity="${lastItemActivity}" lastItemStatus="${lastItemStatus}" lastLookupInstance="${lastLookupInstance}"/>
	</c:when>
	<c:when test="${not empty outcomeStatus.activity}">
		<workflow:linkActivity itemActivity="${outcomeStatus.activity}" lastItemActivity="${lastItemActivity}" lastItemStatus="${lastItemStatus}" lastLookupInstance="${lastLookupInstance}"/>
	</c:when>
	<c:otherwise>
		ERROR - Outcome Status Not Wired (ok if outcome not used)
	</c:otherwise>
</c:choose>