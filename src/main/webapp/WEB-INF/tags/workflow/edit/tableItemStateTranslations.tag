<%@ tag language="java" display-name="tableItemStateTranslations" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ attribute name="itemState" required="true" type="java.lang.Object" %>

<%-- Displays a table of item state (activity or status) translations --%>

<table class="default2">
	<thead>
		<tr>
			<th>Locale</th>
			<th>Translation</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="translation" items="${itemState.translations}">
			<tr>
				<td>${translation.locale}</td>
				<td>${translation.translation}</td>
			</tr>
		</c:forEach>
	</tbody>
</table>