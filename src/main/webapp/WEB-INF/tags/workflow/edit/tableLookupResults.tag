<%@ tag language="java" display-name="tableItemStateTranslations" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ attribute name="lookupFunction" required="true" type="java.lang.Object" %>

<%--
	Displays a table of lookup results for the selected lookup function to aid in wiring selection 
	Also provides a hidden input with the number of lookup messages, to be used by caller.
--%>

<c:set var="messageCount" value="${fn:length(lookupFunction.lookupResultMessages)}" />
<input type="hidden" id="lookup-message-count" value="${messageCount}" />
<table class="default2">
	<thead>
		<tr>
			<th colspan="3">Lookup Results:</th>
		</tr>
		<tr>
			<td>Index</td>
			<td>Message</td>
			<td>Description</td>
		</tr>
	</thead>
	<tbody>
		<fmt:setLocale value="en_GB" />
		<c:forEach begin="0" end="${messageCount-1}" varStatus="rowStatus">
			<c:set var="lookupMessage" value="${lookupFunction.lookupResultMessages[rowStatus.index]}" />
			<tr>
				<td>${rowStatus.index}</td>
				<td>${lookupMessage}</td>
				<td><fmt:message key="${lookupMessage.messageCode}" /></td>
			</tr>
		</c:forEach>
	</tbody>
</table>