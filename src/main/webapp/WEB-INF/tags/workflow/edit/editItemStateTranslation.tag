<%@ tag language="java" display-name="tableItemStateTranslations" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%-- Displays a table to edit item state (activity or status) translations --%>

<table class="default2">
	<thead>
		<tr>
			<td colspan="2">Description Translations:</td>
		</tr>
		<tr>
			<th>Locale</th>
			<th>Description Translation</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="locale" items="${supportedLocales}">
			<tr>
				<td>${locale.getDisplayLanguage(rc.locale)} (${locale.getDisplayCountry(rc.locale)})</td>
				<td><form:input path="translations['${locale.toString()}']" size="100"/></td>
			</tr>
		</c:forEach>
	</tbody>
</table>

