<%@ tag language="java" display-name="eaDetails" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%@ attribute name="ea" required="true" type="java.lang.Object" %>
<%@ attribute name="contact" required="true" type="java.lang.Object" %>
<%@ attribute name="canEdit" required="true" type="java.lang.Boolean" %>

<%-- 
	This tag shows a single engineer allocation.  The current contact is provided as well as a 'canEdit' authorisation.  
	The status of the allocation is indicated.  If complete, the allocation is greyed out.
    If the user has "edit" privileges for allocations, the allocation can be edited or completed.
    Otherwise, if the user is the one allocated to, the allocation can be completed here.
--%>

<span class="bold">
	<fmt:formatDate value='${ea.allocatedFor}' pattern='EEE dd.MM.yyyy' />

</span>
<c:out value=" : " />
<a href="viewworkforuser.htm?personid=${ea.allocatedTo.personid}"
	class="mainlink"
	title="<spring:message code='workflow.viewallitemsallocatedto'/><c:out value=' ${ea.allocatedTo.name}'/>">
<c:out value="${ea.allocatedTo.shortenedName}" />
</a>
<c:forEach var="message" items="${ea.messages}">
	<div style="width: 95%; border-top: 1px dotted black;">
		${message.setBy.initials}
		(<fmt:formatDate value="${message.setOn}" pattern="dd.MM" />):&nbsp;
		<em>${message.message}</em>
	</div>
</c:forEach>



