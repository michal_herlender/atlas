<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ attribute name="instrument" required="true" type="java.lang.Object" %>
<%@ attribute name="ownedByBusinessCompany" required="true" type="java.lang.Object" %>


<fieldset>
	<legend><spring:message code="viewinstrument.actions"/></legend>
	<c:choose>
		<c:when test="${instrument.asset != null}">
			<!-- displays content in column 50% wide (left) -->
			<div class="displaycolumn">
				<ol>
					<li>
						<label><spring:message code="viewinstrument.viewasset"/>:</label>
						<span>
							<a href="viewasset.htm?id=$!instrument.asset.assetId" class="mainlink">${instrument.asset.assetNo}</a>
						</span>
					</li>
					<li>
						<label><spring:message code="viewinstrument.printlabel"/>:</label>
						<span>
							<a href="#" class="mainlink" onclick=" printAssetLabel(${instrument.asset.assetId}, $j(this).next('input:checkbox').is(':checked')); return false; ">
                               	<span style="text-transform:uppercase"><spring:message code="viewinstrument.printlabel"/></span>
							</a>
							&nbsp;
							<input type="checkbox" class="checkbox" />&nbsp;<spring:message code="viewinstrument.includeipaddress"/>
						</span>
					</li>

				</ol>

			</div>

			<!-- displays content in column 50% wide (right) -->
			<div class="displaycolumn">

				<ol>

					<li>
						<label><spring:message code="viewinstrument.deleteasset"/>:</label>
						<span>
							<a href="#" class="mainlink" onclick=" deleteAsset(${instrument.asset.assetId}); return false; ">
								<img src="img/icons/delete.png" width="16" height="16" class="image_inline3" /> &nbsp;<spring:message code="viewinstrument.deleteasset"/>
							</a>
						</span>
					</li>

				</ol>

			</div>
		</c:when>
		<c:when test="${command.ownedByBusinessCompany == true}">
			<ol>
				<li>
					<label><spring:message code="viewinstrument.addasasset"/>:</label>
					<span><a href="addeditasset.htm?type=physical&plantid=$instrument.plantid" class="mainlink"><spring:message code="viewinstrument.linkinstrumentasasset"/></a></span>
				</li>
			</ol>
		</c:when>
	</c:choose>
</fieldset>