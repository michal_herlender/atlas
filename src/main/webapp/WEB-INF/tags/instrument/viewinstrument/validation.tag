<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%@ attribute name="instrument" required="true" type="java.lang.Object" %>
<spring:message code="viewinstrument.tableallvalidationrequests" var="tableSummary"/>
<table class="default2" id="validations" summary='${tableSummary}'>
	<thead>
		<tr>
			<td colspan="7"><spring:message code="viewinstrument.instrumentvalidationrequests"/> (${instrument.issues.size()})</td>
		</tr>
		<tr>
			<th class="issue" scope="col"><spring:message code="viewinstrument.issue"/></th>
			<th class="raiseon" scope="col"><spring:message code="viewinstrument.raisedon"/></th>
			<th class="raiseby" scope="col"><spring:message code="viewinstrument.raisedby"/></th>
			<th class="status" scope="col"><spring:message code="status"/></th>
			<th class="actions" scope="col"><spring:message code="viewinstrument.actions"/></th>
			<th class="resolveby" scope="col"><spring:message code="viewinstrument.resolvedby"/></th>
			<th class="resolveon" scope="col"><spring:message code="viewinstrument.resolvedon"/></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="7">&nbsp;</td>
		</tr>
	</tfoot>
	<tbody>
		<c:choose>
			<c:when test="${instrument.issues.size() < 1}">
				<tr>
					<td class="bold text-center" colspan="7"><spring:message code="viewinstrument.novalidationissuesreported"/></td>
				</tr>
			</c:when>
			<c:otherwise>
				<c:set var="rowcount" value="0"/>
				<c:forEach var="issue" items="${instrument.issues}">
					<c:set var="spanStyle" value=""/>
					<c:if test="${issue.status.requiringAttention == true}">
						<c:set var="spanStyle" value="class='attention'"/>
					</c:if>
					<tr id="issue${issue.id}" class='${$rowcount % 2 == 0 ? "even" : "odd" }'>
						<td><span ${spanStyle}>${issue.issue}</span></td>
						<td><fmt:formatDate value="${issue.raisedOn}" dateStyle="medium" type="date"/></td>
						<td>${issue.raisedBy.name}</td>
						<td class="stat"><cwms:besttranslation translations="${issue.status.nametranslations}"/></td>
						<td><span>${issue.actions.size()}</span> <a href="#" id="actionLink${rowcount}" onclick=" toggleActions(${rowcount}, this.id); return false; "><img src="img/icons/items.png" width="16" height="16" alt='<spring:message code="viewinstrument.viewactions"/>' title='<spring:message code="viewinstrument.viewactions"/>' class="image_inline" /></a></td>
						<c:choose>
							<c:when test="${issue.status.requiringAttention == false}">
								<td>${issue.resolvedBy.name}</td>
								<td><fmt:formatDate value="${issue.resolvedOn}" dateStyle="medium" type="date"/>}</td>
							</c:when>
							<c:otherwise>
								<td colspan="2">
									<a href="#" class="domthickbox mainlink" onclick=" tb_show('Add Validation Action', 'TB_dom?width=500&height=260', '', thickboxActionContent(${issue.id}, ${issue.status.statusid})); return false; " title=""><spring:message code="viewinstrument.addaction"/></a>
								</td>
							</c:otherwise>
						</c:choose>
					</tr>
					<tr id="validationchild$rowcount" class="hid">
						<td colspan="7">
							<div style=" display: none; ">
								<table class="child_table" id="actionlist${issue.id}" summary='<spring:message code="viewinstrument.tableallactionsvalidationissue"/>'>
									<thead>
										<tr>
											<th class="action" scope="col"><spring:message code="viewinstrument.actions"/></th>
											<th class="underby" scope="col"><spring:message code="viewinstrument.undertakenby"/></th>
											<th class="underon" scope="col"><spring:message code="viewinstrument.undertakenon"/></th>
										</tr>
									</thead>
									<tfoot>
										<tr>
											<td colspan="3">&nbsp;</td>
										</tr>
									</tfoot>
									<tbody>
										<c:choose>
											<c:when test="${issue.actions.size() < 1}">
												<tr>
													<td colspan="3" class="center bold"><spring:message code="viewinstrument.noactionscompleted"/></td>
												</tr>
											</c:when>
											<c:otherwise>
												<c:forEach var="action" items="${issue.actions}">
													<tr>
														<td>${action.action}</td>
														<td>${action.underTakenBy.name}</td>
														<td><fmt:formatDate value="${action.underTakenOn}" dateStyle="medium" type="date"/></td>
													</tr>
												</c:forEach>
											</c:otherwise>
										</c:choose>
									</tbody>
								</table>
							</div>
						</td>
					</tr>
					<c:set var="rowcount" value="${rowcount+1}"/>
				</c:forEach>
			</c:otherwise>
		</c:choose>
	</tbody>
</table>

