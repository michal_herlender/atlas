<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>

<%@ attribute name="instrument" required="true" type="java.lang.Object" %>

<table class="default2 instrumentHistory" summary='<spring:message code="viewinstrument.tableinstrumenthistory"/>'>
	<thead>
		<tr>
			<td colspan="5">
				<c:choose>
					<c:when test="${instrument.addedOn != null}">
						<spring:message code="viewinstrument.instrumentadded"/>&nbsp;
						<fmt:formatDate value="${instrument.addedOn}" type="date" dateStyle="short"/>&nbsp;
						<spring:message code="viewinstrument.by"/>&nbsp;<links:contactLinkDWRInfo contact="${instrument.addedBy}" rowcount="0"/>
					</c:when>
					<c:otherwise>
						<spring:message code="viewinstrument.nodateaddedinformationavailable"/>
					</c:otherwise>
				</c:choose>
			</td>
		</tr>
		<c:if test="${instrument.history.size() > 0}">
			<tr>
				<td colspan="5"><spring:message code="viewinstrument.followingchangeslogged"/> (${instrument.instrumentHistoryTotalCount})</td>
			</tr>
		</c:if>
		<tr>
			<th class="field" scope="col"><spring:message code="viewinstrument.field"/></th>
			<th class="changefrom" scope="col"><spring:message code="viewinstrument.changedfrom"/></th>
			<th class="changeto" scope="col"><spring:message code="viewinstrument.changedto"/></th>
			<th class="changeby" scope="col"><spring:message code="viewinstrument.changedby"/></th>
			<th class="changeon" scope="col"><spring:message code="viewinstrument.changedon"/></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
	</tfoot>
	<tbody>
		<c:choose>
			<c:when test="${instrument.history.size() > 0}">
				<c:forEach var="history" items="${instrument.history}">
					<c:if test="${history.companyUpdated}">
						<tr>
							<td><spring:message code="viewinstrument.instrumentcompany"/></td>
							<td>${history.oldCompany.coname}</td>
							<td>${history.newCompany.coname}</td>
							<td>${history.changeBy.name}</td>
							<td><fmt:formatDate value="${history.changeDate}" type="date" dateStyle="short"/></td>
						</tr>
					</c:if>
					<c:if test="${history.contactUpdated}">
						<tr>
							<td><spring:message code="viewinstrument.instrumentowner"/></td>
							<td>${history.oldContact.name}</td>
							<td>${history.newContact.name}</td>
							<td>${history.changeBy.name}</td>
							<td><fmt:formatDate value="${history.changeDate}" type="date" dateStyle="short"/></td>
						</tr>
					</c:if>
					<c:if test="${history.addressUpdated}">
						<tr>
							<td><spring:message code="viewinstrument.instrumentaddress"/></td>
							<td>${history.oldAddress.addr1}</td>
							<td>${history.newAddress.addr1}</td>
							<td>${history.changeBy.name}</td>
							<td><fmt:formatDate value="${history.changeDate}" type="date" dateStyle="short"/></td>
						</tr>
					</c:if>
					<c:if test="${history.serialNoUpdated}">
						<tr>
							<td><spring:message code="viewinstrument.instrumentserialno"/></td>
							<td>${history.oldSerialNo}</td>
							<td>${history.newSerialNo}</td>
							<td>${history.changeBy.name}</td>
							<td><fmt:formatDate value="${history.changeDate}" type="date" dateStyle="short"/></td>
						</tr>
					</c:if>
					<c:if test="${history.plantNoUpdated}">
						<tr>
							<td><spring:message code="viewinstrument.instrumentplantno"/></td>
							<td>${history.oldPlantNo}</td>
							<td>${history.newPlantNo}</td>
							<td>${history.changeBy.name}</td>
							<td><fmt:formatDate value="${history.changeDate}" type="date" dateStyle="short"/></td>
						</tr>
					</c:if>
					<c:if test="${history.statusUpdated}">
						<tr>
							<td><spring:message code="viewinstrument.instrumentstatus"/></td>
							<td>${history.oldStatus.status}</td>
							<td>${history.newStatus.status}</td>
							<td>${history.changeBy.name}</td>
							<td><fmt:formatDate value="${history.changeDate}" type="date" dateStyle="short"/></td>
						</tr>
					</c:if>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<tr>
					<td colspan="5" class="center bold"><spring:message code="viewinstrument.nohistoricalchanges"/></td>
				</tr>
			</c:otherwise>
		</c:choose>
	</tbody>
</table>
