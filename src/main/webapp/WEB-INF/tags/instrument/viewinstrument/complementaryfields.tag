<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%@ attribute name="instrument" required="true" type="org.trescal.cwms.core.instrument.entity.instrument.Instrument" %>
<%@ attribute name="hasdeviation" required="true" type="java.lang.Boolean" %>
<%@ attribute name="hasverificationstatus" required="true" type="java.lang.Boolean" %>

<fieldset>
	<legend><spring:message code="viewinstrument.complementary"/></legend>
	<cwms:securedLink collapse="True" permission="COMPLEMENTARY_FIELD_EDIT" classAttr="mainlink-float" parameter="?plantid=${instrument.plantid}">
		<spring:message code="viewinstrument.complementaryedit"/>
	</cwms:securedLink><br>

	<ol>
		<li>
			<label><spring:message code="viewinstrument.accreditation"/>:</label>
			<span>
				${instrument.instrumentComplementaryField.accreditation != null ? instrument.instrumentComplementaryField.accreditation : "&nbsp;" }
			</span>
		</li>
		<li>
			<label><spring:message code="viewinstrument.deliverystatus"/>:</label>
			<span>
				${instrument.instrumentComplementaryField.deliveryStatus != null ? instrument.instrumentComplementaryField.deliveryStatus : "&nbsp;" }
			</span>
		</li>
		<li>
			<label><spring:message code="viewinstrument.firstusedate"/>:</label>
			<span>
				<fmt:formatDate pattern="dd.MM.yyyy" type="date" value="${instrument.instrumentComplementaryField.firstUseDate}"/>
				<%--${instrument.instrumentComplementaryField.firstUseDate != null ? instrument.instrumentComplementaryField.firstUseDate : "&nbsp;" }--%>
			</span>
		</li>
		<li>
			<label><spring:message code="viewinstrument.purchasecost"/>:</label>
			<span>
				${instrument.instrumentComplementaryField.purchaseCost != null ? instrument.instrumentComplementaryField.purchaseCost : "&nbsp;" }
			</span>
		</li>
		<li>
			<label><spring:message code="viewinstrument.purchasedate"/>:</label>
			<span>
				<fmt:formatDate dateStyle="medium" type="date" value="${instrument.instrumentComplementaryField.purchaseDate}"/>
			</span>
		</li>
		<li>
			<label><spring:message code="viewinstrument.formerbarcode"/>:</label>
			<span>
				${instrument.instrumentComplementaryField.formerBarCode != null ? instrument.instrumentComplementaryField.formerBarCode : "&nbsp;"}
			</span>
		</li>
		<li>
			<label><spring:message code="viewinstrument.formerfamily"/>:</label>
			<span>
				${instrument.instrumentComplementaryField.formerFamily != null ? instrument.instrumentComplementaryField.formerFamily : "&nbsp;"}
			</span>
		</li>
		<li>
			<label><spring:message code="viewinstrument.formercharacteristics"/>:</label>
			<span>
				${instrument.instrumentComplementaryField.formerCharacteristics != null ? instrument.instrumentComplementaryField.formerCharacteristics : "&nbsp;"}
			</span>
		</li>
		<li>
			<label><spring:message code="viewinstrument.formerlocaldescription"/>:</label>
			<span>
				${instrument.instrumentComplementaryField.formerLocalDescription != null ? instrument.instrumentComplementaryField.formerLocalDescription : "&nbsp;"}
			</span>
		</li>
		<li>
			<label><spring:message code="viewinstrument.formermanufacturer"/>:</label>
			<span>
				${instrument.instrumentComplementaryField.formerManufacturer != null ? instrument.instrumentComplementaryField.formerManufacturer : "&nbsp;"}
			</span>
		</li>
		<li>
			<label><spring:message code="viewinstrument.formermodel"/>:</label>
			<span>
				${instrument.instrumentComplementaryField.formerModel != null ? instrument.instrumentComplementaryField.formerModel : "&nbsp;"}
			</span>
		</li>
		<li>
			<label><spring:message code="viewinstrument.formername"/>:</label>
			<span>
				${instrument.instrumentComplementaryField.formerName != null ? instrument.instrumentComplementaryField.formerName : "&nbsp;"}
			</span>
		</li>
		<li>
			<label><spring:message code="viewinstrument.provider"/>:</label>
			<span>
				${instrument.instrumentComplementaryField.provider != null ? instrument.instrumentComplementaryField.provider : "&nbsp;"}
			</span>
		</li>
		<li>
			<label><spring:message code="viewinstrument.remarks"/></label>
			<span>
				${instrument.instrumentComplementaryField.clientRemarks != null ? instrument.instrumentComplementaryField.clientRemarks : "&nbsp;"}
			</span>
		</li>
		<c:if test="${hasdeviation}">
			<li>
				<label><spring:message code="viewinstrument.deviationunit"/></label>
				<span>
					<c:if test="${instrument.instrumentComplementaryField.deviationUnits != null}">
						<cwms:besttranslation translations='${instrument.instrumentComplementaryField.deviationUnits.nameTranslation}'/>
					</c:if>
				</span>
			</li>
			<li>
				<label><spring:message code="viewinstrument.nominalvalue"/></label>
				<span>
						${instrument.instrumentComplementaryField.nominalValue != null ? instrument.instrumentComplementaryField.nominalValue : "&nbsp;"}
				</span>
			</li>
		</c:if>

		<c:if test="${hasCertClass}">
			<li>
				<label><spring:message code="certificate.class"/></label>
				<span>
					<c:if test="${instrument.instrumentComplementaryField.certificateClass != null}">
						<c:out value="${instrument.instrumentComplementaryField.certificateClass.description}"/>
					</c:if>
				</span>
			</li>
		</c:if>

        <c:if test="${hasverificationstatus}">
			<li>
				<label><spring:message code="instrument.customeracceptancecriteria"/></label>
				<span>
					<c:if test="${instrument.instrumentComplementaryField.customerAcceptanceCriteria != null}">
						<c:out value="${instrument.instrumentComplementaryField.customerAcceptanceCriteria}"/>
					</c:if>
				</span>
			</li>
            <li>
                <label><spring:message code="instrument.certvalidationstatusoverride"/></label>
                <span>
					<c:if test="${instrument.instrumentComplementaryField.calibrationStatusOverride != null}">
                        <c:out value="${instrument.instrumentComplementaryField.calibrationStatusOverride.getName()}"/>
                    </c:if>
				</span>
            </li>
        </c:if>
	</ol>
</fieldset>
