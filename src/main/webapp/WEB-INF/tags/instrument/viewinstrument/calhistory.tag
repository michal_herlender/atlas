<%@ tag language="java" display-name="calhistory" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>

<%@ attribute name="history" required="true" type="java.lang.Object" %>
<%@ attribute name="maxResults" required="true" type="java.lang.Object" %>
<%@ attribute name="historySize" required="true" type="java.lang.Object" %>

<table class="default2 instCalHistory" summary='<spring:message code="viewinstrument.tableallcalibrationhistory"/>'>
	<thead>
	<tr>
		<td colspan="6"><spring:message code="viewinstrument.calibrationhistory"/></td>
	</tr>
	<tr>
		<th class="caldate" scope="col"><spring:message code="caldate"/></th>
		<th class="type" scope="col"><spring:message code="servicetype"/></th>
		<th class="proc" scope="col"><spring:message code="capability"/></th>
		<th class="job" scope="col"><spring:message code="jobno"/></th>
		<th class="jobitem" scope="col"><spring:message code="itemno"/></th>
		<th class="outcome" scope="col"><spring:message code="viewinstrument.outcome"/></th>
	</tr>
	</thead>
	<tfoot>
	<tr>
		<td colspan="6">&nbsp;</td>
	</tr>
	</tfoot>
	<tbody>
	<c:choose>
		<c:when test="${not empty history}">
				<c:if test="${historySize > maxResults}">
					<tr>
						<td colspan="8" class="center bold">
							<spring:message code="pagination.displayingmostrecentresults" arguments="${maxResults},${historySize}"/>
						</td>
					</tr>
				</c:if>
			<c:forEach var="cal" items="${history}" varStatus="loopStatus" >
				
				<tr  style=" background-color: ${cal.calType.serviceType.displayColour}; ">
					<td class="caldate">
						<fmt:formatDate value="${cal.calDate}" type="date" dateStyle="SHORT" />
					</td>
					<td class="type">
						<cwms:besttranslation translations="${cal.calType.serviceType.shortnameTranslation}"/>
					</td>
					<td class="proc">
						<links:procedureLinkDWRInfo displayName="false" capability="${cal.capability}" rowcount="${loopStatus.index}"/>
					</td>
					<td class="job">
						<links:jobnoLinkDWRInfo rowcount="${loopStatus.index}" jobno="${cal.links.iterator().next().ji.job.jobno}" jobid="${cal.links.iterator().next().ji.job.jobno}" copy="false"/>
					</td>
					<td class="jobitem">
						<links:jobitemLinkDWRInfo jobitem="${cal.links.iterator().next().ji}" jicount="false" rowcount="${loopStatus.index}"/>
					</td>
					<td class="outcome">
						${cal.status.name}
					</td>
				</tr>
			</c:forEach>
		</c:when>
		<c:otherwise>
			<tr>
				<td colspan="6" class="bold text-center"><spring:message code="viewinstrument.nocalibrationsperformed"/></td>
			</tr>
		</c:otherwise>
	</c:choose>
	</tbody>
</table>
