<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ attribute name="instrument" required="true" type="java.lang.Object" %>
<%@ attribute name="currentInterimCalRecallRuleInterval" required="true" type="java.lang.Object" %>

<label><spring:message code="viewinstrument.nextinterimcaldate" /></label>
<c:choose>
	<c:when
		test="${instrument.instrumentComplementaryField.nextInterimCalibrationDate != null}">
		<span
			class="interimCalDate bold ${currentInterimCalRecallRuleInterval.overdue == true ? 'attention' : ''}">
			<fmt:formatDate
				value="${instrument.instrumentComplementaryField.nextInterimCalibrationDate}" />
		</span>&nbsp;
        <c:choose>
			<c:when
				test="${currentInterimCalRecallRuleInterval != null
                                && currentInterimCalRecallRuleInterval.interval > 0}">
                (<span class="interimCalInterval">${currentInterimCalRecallRuleInterval.interval}</span>
				<span class="interimCalUnit">${currentInterimCalRecallRuleInterval.translatedUnit}</span>)
            </c:when>
			<c:otherwise>
                (<span class="interimCalInterval"><spring:message
						code="viewinstrument.norecallruleset" /></span>
				<span class="interimCalUnit"></span>)
            </c:otherwise>
		</c:choose>
	</c:when>
	<c:otherwise>
		<span class="interimCalDate bold"></span>
		(<span class="interimCalInterval"><spring:message
				code="viewinstrument.norecallruleset" /></span>
		<span class="interimCalUnit"></span>)
    </c:otherwise>
</c:choose>
