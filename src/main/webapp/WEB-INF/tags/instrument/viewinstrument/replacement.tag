<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel" %>

<%@ attribute name="instrument" required="true" type="java.lang.Object" %>

<table class="default2" id="replacement" summary='<spring:message code="viewinstrument.tablereplacement"/>'>
	<c:choose>
		<c:when test="${instrument.replacement != null}">
			<thead>
			<tr>
				<td colspan="4"><spring:message code="viewinstrument.instrumentreplaced"/></td>
			</tr>
			<tr>
				<th class="barcode" scope="col"><spring:message code="barcode"/></th>
				<th class="instmod" scope="col"><spring:message code="viewinstrument.instrumentmodel"/></th>
				<th class="repby" scope="col"><spring:message code="viewinstrument.replacedby"/></th>
				<th class="repon" scope="col"><spring:message code="viewinstrument.replacedon"/></th>
			</tr>
			</thead>
			<tfoot>
			<tr>
				<td colspan="4">&nbsp;</td>
			</tr>
			</tfoot>
			<tbody>
			<tr>
				<td><links:showInstrumentLink instrument="${instrument.replacement}"/></td>
				<td><instmodel:showInstrumentModelLink instrument="${instrument.replacement}" caltypeid="0"/></td>
				<td>${instrument.replacedBy.name}</td>
				<td><fmt:formatDate value="${instrument.replacedOn}" type="date" dateStyle="short"/></td>
			</tr>
			</tbody>
		</c:when>
		<c:otherwise>
			<thead>
			<tr>
				<td class="center bold"><spring:message code="viewinstrument.notreplaced"/></td>
			</tr>
			</thead>
		</c:otherwise>
	</c:choose>
</table>

<table class="default2" id="replaced" summary='<spring:message code="viewinstrument.tableinstrumentsreplaced"/>'>
	<c:choose>	
		<c:when test="${instrument.replacementTo.size() > 0}">
			<thead>
			<tr>
				<td colspan="4"><spring:message code="viewinstrument.instrumentreplacedfollowing"/> ${instrument.replacementTo.size()} <spring:message code="viewinstrument.otherinstruments"/></td>
			</tr>
			<tr>
				<th class="barcode" scope="col"><spring:message code="barcode"/></th>
				<th class="instmod" scope="col"><spring:message code="viewinstrument.instrumentmodel"/></th>
				<th class="repby" scope="col"><spring:message code="viewinstrument.replacedby"/></th>
				<th class="repon" scope="col"><spring:message code="viewinstrument.replacedon"/></th>
			</tr>
			</thead>
			<tfoot>
			<tr>
				<td colspan="4">&nbsp;</td>
			</tr>
			</tfoot>
			<tbody>
				<c:forEach var="rep" items="${instrument.replacementTo}">
					<tr>
						<td><links:showInstrumentLink instrument="${rep}"/></td>
						<td><instmodel:showInstrumentModelLink instrument="${rep}" caltypeid="0"/></td>
						<td>${rep.replacedBy.name}</td>
						<td><fmt:formatDate value="${rep.replacedOn}" type="date" dateStyle="short"/></td>
					</tr>
				</c:forEach>
			</tbody>
		</c:when>
		<c:otherwise>
			<thead>
			<tr>
				<td class="center bold"><spring:message code="viewinstrument.instrumentnotreplaced"/></td>
			</tr>
			</thead>
		</c:otherwise>
	</c:choose>
</table>
