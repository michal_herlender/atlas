<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ attribute name="jobItemImages" required="true" type="java.lang.Object" %>

<div class="infobox">
	<div class="gallerifficContent">
		<div id="thumbs-adv" class="navigation">
			<ul class="thumbs noscript">
				<c:set var="count" value="1"/>
				<c:choose>
					<c:when test="${jobItemImages.size() < 1}">
						<li class="thumbitem" id="emptyImage">
							<a class="thumb" href="img/noimage.png" title="">
								<img src="img/noimage.png" alt="" height="60" width="80" />
							</a>
							<div class="caption">
								<div class="download"></div>
								<div class="image-title"><spring:message code="viewinstrument.noimagessaved"/></div>
								<div class="image-desc"></div>
								<div class="image-desc"></div>
							</div>
						</li>
					</c:when>
					<c:otherwise>
						<c:forEach var="image" items="${jobItemImages}">
							<li class="thumbitem" id="systemimg${image.id}">
								<a class="thumb" href="displayimage?id=${image.id}" title="">
									<img src="displaythumbnail?id=${image.id}" alt=""/>
								</a>
								<div class="caption">
									<div class="download">
										<a href="displayimage?id=${image.id}"><spring:message code="jiimages.downlorig"/></a>
									</div>
									<div class="image-title"><spring:message code="viewinstrument.image"/> ${count}</div>
									<div class="image-desc">
										<strong>Description: &nbsp;</strong>
										<c:choose>
											<c:when test='${image.description != ""}'>
												${image.description}
											</c:when>
											<c:otherwise>
												<spring:message code="nocomment"/>
											</c:otherwise>
										</c:choose>
									</div>
									<div class="image-desc">
										<strong><spring:message code="jiimages.taggedas"/>: &nbsp;</strong>
										<c:choose>
											<c:when test="${image.tags.size() > 0}">
												<c:forEach var="tag" items="${image.tags}" varStatus="loopStatus">
													<c:choose>
														<c:when test="${loopStatus.index==1}">
															${tag.tag}
														</c:when>
														<c:otherwise>
															, ${tag.tag}
														</c:otherwise>
													</c:choose>
												</c:forEach>
											</c:when>
											<c:otherwise>
												<spring:message code="jiimages.notagsassigned"/>
											</c:otherwise>
										</c:choose>
									</div>
								</div>
							</li>
							<c:set var="count" value="${count + 1}"/>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</ul>
		</div>

		<div id="gallery-adv" class="content">
			<div id="controls-adv" class="controls"></div>
			<div id="loading-adv" class="loader"></div>
			<div id="slideshow-adv" class="slideshow"></div>
			<div id="caption-adv" class="embox"></div>
		</div>

		<!-- clear floats and restore page flow -->
		<div class="clear"></div>

	</div>

</div>

