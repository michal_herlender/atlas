<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%@ attribute name="instrument" required="true" type="java.lang.Object" %>

<table class="default2 instMeasurementPossibilities" summary="This table lists all measurement possibilities for the instrument">
	<thead>
	<tr>
		<td colspan=7>
			<cwms:securedLink permission="INSTRUMENT_MEASURE_POSSIBILITY_EDIT" collapse="True" classAttr="mainlink-float" parameter="?plantid=${instrument.plantid}">
				<spring:message code="viewinstrument.measurementpossibilityadd"/>
			</cwms:securedLink><br>
		</td>
	</tr>
	<tr>
		<th class="name" scope="col"><spring:message code="viewinstrument.name"/></th>
		<th class="minimum" scope="col"><spring:message code="viewinstrument.minimum"/></th>
		<th class="maximum" scope="col"><spring:message code="viewinstrument.maximum"/></th>
		<th class="scalederiva" scope="col"><spring:message code="viewinstrument.quality"/></th>
		<th class="lmax" scope="col"><spring:message code="viewinstrument.lmax"/></th>
		<th class="fieldaction" scope="col"><spring:message code="viewinstrument.action"/></th>
	</tr>
	</thead>
	<tfoot>
	<tr>
		<td colspan="7">&nbsp;</td>
	</tr>
	</tfoot>
	<tbody>
	<c:if test="${instrument.measurementPossibilities.size() > 0}">
		<c:set var="rowcount" value="0"/>
		<c:forEach var="cf" items="${instrument.measurementPossibilities}">
			<tr>
				<td class="field">${cf.name}</td>
				<td class="minimum">${cf.minimum}</td>
				<td class="maximum">${cf.maximum}</td>
				<td class="scalederiva">${cf.scalederiva}</td>
				<td class="quality">${cf.quality}</td>
				<td class="lmax">${cf.lmax}</td>
				<td class="fieldaction">
					<a href='editmeasurementpossibility.htm?id=${cf.measurementpossibilityid}'>
						<img src="img/icons/edit_wi.png" width="16" height="16" alt='<spring:message code="viewinstrument.measurementpossibilityedit"/>' title='<spring:message code="viewinstrument.measurementpossibilityedit"/>' class="img_marg_bot" />
					</a>
				</td>
			</tr>
			<c:set var="rowcount" value="${rowcount + 1}"/>
		</c:forEach>
	</c:if>
	</tbody>
</table>
