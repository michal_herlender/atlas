<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ attribute name="instrument" required="true" type="java.lang.Object" %>
<%@ attribute name="currentMaintenanceRecallRuleInterval" required="true" type="java.lang.Object" %>

<label><spring:message code="viewintrument.nextmaintenancedate"/></label>
<c:choose>
	<c:when test="${instrument.instrumentComplementaryField.nextMaintenanceDate != null}">
	    <span class="maintenanceDate bold ${currentMaintenanceRecallRuleInterval.overdue == true ? 'attention' : ''}">
	        <fmt:formatDate value="${instrument.instrumentComplementaryField.nextMaintenanceDate}"/>
	    </span>&nbsp;
	
	    <c:choose>
	        <c:when test="${currentMaintenanceRecallRuleInterval != null
	                                && currentMaintenanceRecallRuleInterval.interval > 0}">
	            (<span class="maintenanceInterval">${currentMaintenanceRecallRuleInterval.interval}</span>
	            <span class="maintenanceUnit">${currentMaintenanceRecallRuleInterval.translatedUnit}</span>)
	        </c:when>
	        <c:otherwise>
	            (<span class="maintenanceInterval"><spring:message code="viewinstrument.norecallruleset"/></span>
	            <span class="maintenanceUnit"></span>)
	        </c:otherwise>
	    </c:choose>
	</c:when>
	<c:otherwise>
		<span class="maintenanceDate bold"></span>
		(<span class="maintenanceInterval"><spring:message code="viewinstrument.norecallruleset"/></span>
		<span class="maintenanceUnit"></span>)
     </c:otherwise>
</c:choose>
