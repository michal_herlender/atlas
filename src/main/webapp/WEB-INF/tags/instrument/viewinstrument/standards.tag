<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>

<%@ attribute name="instrument" required="true" type="java.lang.Object" %>
<%@ attribute name="procstandards" required="true" type="java.lang.Object" %>
<%@ attribute name="wistandards" required="true" type="java.lang.Object" %>

<div style="padding: 5px">
	<c:if test="${procstandards != null}">
		<c:choose>
			<c:when test="${procstandards.size() > 0}">
				<spring:message code="viewinstrument.instrumentstandardfor"/> ${procstandards.size()} <spring:message code="procedures"/>

				<c:forEach var="procstand" items="${procstandards}">
					<div>
						<spring:message code="viewinstrument.forprocedure"/> <links:procedureLinkDWRInfo capability="${procstand.capability}"  />

						&nbsp;<spring:message code="viewinstrument.usageenforced"/>:
						<a href="#" onclick=" updateProcStdEnforceUse(${procstand.id}, ${instrument.plantid}); return false; ">
							<c:choose>
								<c:when test="${procstand.enforceUse == true}">
									<img src="img/icons/bullet-tick.png" width="10" height="10" alt='<spring:message code="viewinstrument.enforceuse"/>' title='<spring:message code="viewinstrument.enforceuse"/>' />
								</c:when>
								<c:otherwise>
									<img src="img/icons/bullet-cross.png" width="10" height="10" alt='<spring:message code="viewinstrument.donotenforce"/>' title='<spring:message code="viewinstrument.donotenforce"/>' />
								</c:otherwise>
							</c:choose>
						</a>
						[<fmt:formatDate value="${procstamd.setOn}" type="date" dateStyle="short"/> ${procstand.setBy.name}]
						<a href="#" onclick=" deleteProcedureStd(${instrument.plantid}, ${procstand.id}); return false; ">
							<img src="img/icons/delete.png" width="16" height="16" alt='<spring:message code="viewinstrument.deleteprocedurestandard"/>' title='<spring:message code="viewinstrument.deleteprocedurestandard"/>' />
						</a>
					</div>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<spring:message code="viewinstrument.instrumentnotsetprocedure"/>
			</c:otherwise>
		</c:choose>
	</c:if>
</div>

<div style="padding: 5px">
	<c:if test="${wistandards != null}">
		<c:choose>
			<c:when test="${wistandards.size() > 0}">
				<spring:message code="viewinstrument.instrumentstandardfor"/> ${wistandards.size()} <span style="text-transform:lowercase"><spring:message code="viewinstrument.workinstruction"/></span>
				<c:forEach var="wistand" items="${wistandards}">
					<div>
						<spring:message code="viewinstrument.forworkinstruction"/>
						<a href="/viewworkinstruction.htm?wid=$wistand.workInstruction.id">${wistand.workInstruction.name}</a>
						${wistand.workInstruction.title}
						&nbsp;<spring:message code="viewinstrument.usageenforced"/>:
						<c:choose>
							<c:when test="${wistand.enforceUse == true}">
								<img src="img/icons/bullet-tick.png" width="10" height="10" alt='<spring:message code="viewinstrument.enforceuse"/>' title='<spring:message code="viewinstrument.enforceuse"/>' />
							</c:when>
							<c:otherwise>
								<img src="img/icons/bullet-cross.png" width="10" height="10" alt='<spring:message code="viewinstrument.donotenforce"/>' title='<spring:message code="viewinstrument.donotenforce"/>' />
							</c:otherwise>
						</c:choose>
						[<fmt:formatDate value="${wistand.setBy.name}" type="date" dateStyle="short"/> ${wistand.setBy.name}]
						<img src="img/icons/delete.png" width="16" height="16" alt='<spring:message code="viewinstrument.deleteprocedurestandard"/>' title='<spring:message code="viewinstrument.deleteprocedurestandard"/>' />
					</div>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<spring:message code="viewinstrument.instrumentnotsetasworkinstruction"/>
			</c:otherwise>
		</c:choose>
	</c:if>
</div>
