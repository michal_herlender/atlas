<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>

<%@ attribute name="resPerPage" required="true" type="java.lang.Object" %>
<%@ attribute name="instrument" required="true" type="java.lang.Object" %>

<table class="default4 instStandardCalibrations" summary="">
	<thead>
		<tr>
			<td colspan="8"><spring:message code="viewinstrument.showingthelast"/>&nbsp;<span class="instStandardCalSize">5</span>&nbsp;<spring:message code="viewinstrument.instrumentstandardcalibrations"/></td>
		</tr>
		<tr>
			<th class="job"><spring:message code="job"/></th>
			<th class="compby"><spring:message code="viewinstrument.completedbyon"/></th>
			<th class="caltype"><spring:message code="servicetype"/></th>
			<th class="process"><spring:message code="process"/></th>
			<th class="proc"><spring:message code="capability"/></th>
			<th class="stds"><spring:message code="viewinstrument.standards"/></th>
			<th class="certs"><spring:message code="certificates"/></th>
			<th class="status"><spring:message code="status"/></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="8"><a href="#" onclick=" event.preventDefault(); fetchMoreCalibrationsForInstrument(this, ${instrument.plantid}, ${resPerPage}, $j('input[name=\'instStandardCurrentRowcount\']').val()); " class="mainlink"><spring:message code="viewinstrument.loadmore"/></a></td>
		</tr>
	</tfoot>
	<tbody>
		<c:if test="${instrument.calibrations.size() > 0}">
			<c:set var="rowcount" value="1"/>
			<c:forEach var="stdUsed" items="${instrument.calibrations}" varStatus="loopStatus">
				<c:if test="${loopStatus.count < 6}">
					<c:set var="c" value="${stdUsed.calibration}"/>
					<tr>
						<td>
							<c:forEach var="cl" items="${c.links}">
								<spring:message code="item"/>&nbsp;
								<a href="jicalibration.htm?jobitemid=${cl.ji.jobItemId}&amp;ajax=jobitem&amp;width=580" class="jconTip mainlink" id="jobitem${cl.ji.jobItemId}$rowcount" tabindex="-1">${cl.ji.itemNo}</a>
								<links:showRolloverMouseIcon/>
								<c:set var="rowcount" value="${rowcount + 1}"/>
								<spring:message code="viewinstrument.onjob"/>&nbsp;
								<links:jobnoLinkDWRInfo rowcount="${rowcount}" jobno="${cl.ji.job.jobno}" jobid="${cl.ji.job.jobid}" copy="false"/>
								<c:set var="rowcount"  value="${rowcount + 1}"/>
								<br/>
							</c:forEach>
						</td>
						<td>${c.completedBy.name}<br /><fmt:formatDate value="${c.completeTime}" dateStyle="medium" type="both" timeStyle="short" /></td>
						<td><cwms:besttranslation translations="${c.calType.serviceType.shortnameTranslation}"/></td>
						<td>${c.calProcess.process}</td>
						<td>
							<links:procedureLinkDWRInfo displayName="${c.capability.reference}" capability="${c.capability}" rowcount="${loopStatus.count}" />
						</td>
						<td>
							<span class="calStandardsSize${c.id}">${c.standardsUsed.size()}</span>
							<!-- hover over this anchor and a tooltip is displayed showing the contents of the div created below -->
							<a href="?id=#calStandards${c.id}&amp;width=450" onclick=" return false; "  id="tip${c.id}" class="jTip">
								<img src="img/icons/standard.png" width="16" height="16" class="image_inl" alt="" />
							</a>
							<!-- contents of this div are displayed in tooltip created above -->
							<div id="calStandards${c.id}" class="hid">
								<table class="default2" summary='<spring:message code="viewinstrument.tableallstandardsusedcalibration"/>'>
									<thead>
										<tr>
											<th colspan="2">
												<div class="float-left"><spring:message code="viewinstrument.standardsused"/> (<span class="calStandardsSize${c.id}">${c.standardsUsed.size()}</span>)</div>
											</th>
										</tr>
									</thead>
									<tbody>
										<c:choose>
											<c:when test="${c.standardsUsed.size() < 1}">
												<tr>
													<td colspan="2" class="center bold"><spring:message code="viewinstrument.nostandardsforthiscalibration"/>></td>
												</tr>
											</c:when>
											<c:otherwise>
												<c:forEach var="s" items="${c.standardsUsed}">
													<tr>
														<td>
															<links:showInstrumentLink instrument="${s.inst}"/>
														</td>
														<td>
															${s.inst.definitiveInstrument}<br/>
															<spring:message code="serialno"/>: ${s.inst.serialno}<br />
															<spring:message code="plantno"/>: ${s.inst.plantno}
														</td>
													</tr>
												</c:forEach>
											</c:otherwise>
										</c:choose>
									</tbody>
								</table>
							</div>
						</td>
						<td>
							${c.certs.size()}
							<!-- hover over this anchor and a tooltip is displayed showing the contents of the div created below -->
							<a href="?id=#certificate${c.id}&amp;width=450" onclick=" return false; " id="certificatetip${c.id}" class="jTip"><img src="img/icons/certificate.png" width="16" height="16" class="image_inl" alt="" /></a>
							<!-- contents of this div are displayed in tooltip created above -->
							<div id="certificate${c.id}" class="hid">
								<table class="default2" id="jicalcertificates" summary='<spring:message code="viewinstrument.tableallcertificatesoncalibration"/>'>
									<thead>
										<tr>
											<th colspan="3">Certificates (${c.certs.size()})</th>
										</tr>
										<tr>
											<th id="certno" scope="col"><spring:message code="viewinstrument.certno"/></th>
											<th id="certstat" scope="col"><spring:message code="status"/></th>
											<th id="certdurtn" scope="col"><spring:message code="viewinstrument.certduration"/></th>
										</tr>
									</thead>
									<tbody>
										<c:choose>
											<c:when test="${c.certs.size() < 1}">
												<tr>
													<td colspan="3" class="center bold"><spring:message code="viewinstrument.nocertificatesforcalibration"/><br/>
														<c:if test='${c.status.name != "Complete" || c.status.name == "Unable to calibrate"}'>
															<a href="#" class="mainlink" onclick=" event.preventDefault(); findCalToIssueCert(${c.id}, ${instrument.plantid}, ${cl.id}); "><spring:message code="viewinstrument.clickissuebelatedcertificate"/></a>
														</c:if>
													</td>
												</tr>
											</c:when>
											<c:otherwise>
												<c:forEach var="cert" items="${c.certs}">
													<tr>
														<td>
															<c:choose>
																<c:when test="${cert.certFilePath != null}">
																	<a href="file:///${cert.certFilePath}" target="_blank" class="mainlink">${cert.certno}</a>
																</c:when>
																<c:otherwise>
																	${cert.certno}
																</c:otherwise>
															</c:choose>
														</td>
														<td><c:out value="${cert.status.message}"/></td>
														<td>${cert.duration} <spring:message code="months"/></td>
													</tr>
												</c:forEach>
											</c:otherwise>
										</c:choose>
									</tbody>
								</table>
							</div>
						</td>
						<td><cwms:besttranslation translations="${c.status.nametranslations}"/></td>
					</tr>
				</c:if>
				<c:set var="rowcount" value="${rowcount + 1}"/>
			</c:forEach>
		</c:if>
	</tbody>
</table>
<input type="hidden" name="instStandardCurrentRowcount" value="${rowcount}" />
