<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>

<%@ attribute name="instrument" required="true" type="java.lang.Object" %>
<%@ attribute name="certlinks" required="true" type="java.lang.Object" %>
<%@ attribute name="certlinkCount" required="true" type="java.lang.Object" %>
<%@ attribute name="instCertLinks" required="true" type="java.lang.Object" %>
<%@ attribute name="instCertLinkCount" required="true" type="java.lang.Object" %>
<%@ attribute name="fastTrackTurn" required="true" type="java.lang.Object" %>
<%@ attribute name="calibrationVerification" required="true" type="java.lang.Object" %>

<c:choose>
	<c:when test="${calibrationVerification}">
		<c:set var="colspan" value="7"/>
		<c:set var="colspan2" value="10"/>
	</c:when>
	<c:otherwise>
		<c:set var="colspan" value="6"/>
		<c:set var="colspan2" value="9"/>
	</c:otherwise>
</c:choose>

<table class="default2" id="instCertificates" summary='<spring:message code="viewinstrument.tablecertificates"/>'>
 	<thead>
		<tr>
			<td colspan="${colspan}"><spring:message code="certificates"/> (${certlinkCount})</td>
		</tr>
		<tr>
			<th class="jobno" scope="col"><spring:message code="jobno"/></th>
			<th class="itemno" scope="col"><spring:message code="itemno"/></th>
			<th class="certno" scope="col"><spring:message code="viewinstrument.certificateno"/></th>
			<th class="caltype" scope="col"><spring:message code="servicetype"/></th>
			<th class="caldate" scope="col"><spring:message code="caldate"/></th>
			<th class="certdate" scope="col"><spring:message code="viewinstrument.certdate"/></th>
			<c:if test="${calibrationVerification}">
				<th class="calver" scope="col"><spring:message code="viewinstrument.calverificationstatus"/></th>
			</c:if>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="${colspan}">&nbsp;</td>
		</tr>
	</tfoot>
	<tbody>
		<c:choose>
			<c:when test="${empty certlinks}">
				<tr>
					<td colspan="${colspan}" class="center bold">
						<spring:message code="viewinstrument.nocertificates"/>
					</td>
				</tr>
			</c:when>
			<c:otherwise>
				<c:set var="rowcount" value="0"/>
			
				<c:if test="${certlinks.size() < certlinkCount}">
					<tr>
						<td colspan="${colspan}" class="center bold">
							<spring:message code="pagination.displayingmostrecentresults" arguments="${certlinks.size()}, ${certlinkCount}"/>
						</td>
					</tr>
				</c:if>
				<c:forEach var="certlink" items="${certlinks}">
					<tr style=" background-color: ${certlink.jobItem.turn <= fastTrackTurn ? certlink.jobItem.calType.serviceType.displayColourFastTrack : certlink.jobItem.calType.serviceType.displayColour};">
						<td>
							<links:jobnoLinkDWRInfo rowcount="${rowcount}" jobno="${certlink.jobItem.job.jobno}" jobid="${certlink.jobItem.job.jobid}" copy="true"/>
						</td>
						<td class="center">
							<links:jobitemLinkDWRInfo jobitem="${certlink.jobItem}" jicount="false" rowcount="${rowcount}"/>
						</td>
						<td>
							<c:choose>
								<c:when test="${certlink.hasCertFile == true}">
									<c:url var="downloadURL" value="downloadfile.htm"><c:param name="file" value="${certlink.encryptedCertFilePath}" /></c:url>
									<a href='${downloadURL}' class="mainlink" target="_blank">${certlink.cert.certno}</a>
								</c:when>
								<c:otherwise>
									${certlink.cert.certno}
								</c:otherwise>
							</c:choose>
						</td>
						<td class="center">
							<cwms:besttranslation translations='${certlink.jobItem.calType.serviceType.shortnameTranslation}'/>
						</td>
						<td class="center"><fmt:formatDate value="${certlink.cert.calDate}" dateStyle="medium" type="date" /></td>
						<td class="center"><fmt:formatDate value="${certlink.cert.certDate}" dateStyle="medium" type="date" /></td>
						<c:if test="${calibrationVerification}">
							<td class="center">${certlink.cert.calibrationVerificationStatus.getName()}</td>
						</c:if>
					</tr>
					<c:if test="${not empty certlink.cert.remarks}">
						<tr>
							<td colspan="${colspan}" class="left bold">
								${certlink.cert.remarks}
							</td>
						</tr>
					</c:if>
					<c:set var="rowcount" value="${rowcount+1}"/>
				</c:forEach>
			</c:otherwise>
		</c:choose>
	</tbody>
</table>
	
<div class="clear">&nbsp;</div>
	
<table class="default2" id="clientCertificates" summary='<spring:message code="viewinstrument.tableclientcertificates"/>'>
	<thead>
		<tr>
		
			<td colspan="${colspan2}">
				<div>
					<spring:message code="viewinstrument.tableclientcertificates"/> (${instCertLinkCount})
				</div>
				<div class="float-right">
					<cwms:securedLink collapse="True" permission="CLIENT_CERTIFICATE_EDIT" classAttr="mainlink" parameter="?plantid=${instrument.plantid}">
						<spring:message code="viewinstrument.addclientcertificate"/>
					</cwms:securedLink>
				</div>
			</td>
		</tr>
		<tr>
			<th class="certno" scope="col"><spring:message code="viewinstrument.ourcertificateno"/></th>
			<th class="clicertno" scope="col"><spring:message code="viewinstrument.clientcertificateno"/></th>
			<th class="thirdparty" scope="col"><spring:message code="viewinstrument.thirdparty"/></th>
			<th class="caltype" scope="col"><spring:message code="servicetype"/></th>
			<th class="caldate" scope="col"><spring:message code="caldate"/></th>
			<th class="certdate" scope="col"><spring:message code="viewinstrument.certdate"/></th>
			<th class="regby" scope="col"><spring:message code="viewinstrument.registeredby"/></th>
			<th class="status" scope="col"><spring:message code="status"/></th>
			<c:if test="${calibrationVerification}">
				<th class="calver" scope="col"><spring:message code="viewinstrument.calverificationstatus"/></th>
			</c:if>
			<th class="edit" scope="col"><spring:message code="edit"/></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="${colspan2}">&nbsp;</td>
		</tr>
	</tfoot>
	<tbody>
		<c:choose>
			<c:when test="${empty instCertLinks}">
				<tr>
					<td colspan="${colspan2}" class="center bold">
						<spring:message code="viewinstrument.noclientcertificates"/>
					</td>
				</tr>
			</c:when>
			<c:otherwise>
				<c:if test="${instCertLinks.size() < instCertLinkCount}">
					<tr>
						<td colspan="${colspan2}" class="center bold">
							<spring:message code="pagination.displayingmostrecentresults" arguments="${instCertLinks.size()}, ${instCertLinkCount}"/>
						</td>
					</tr>
				</c:if>
				<c:set var="rowcount" value = "0"/>
				<c:forEach var="certlink" items="${instCertLinks}">
					<tr>
						<td>
							<c:choose>
								<c:when test="${certlink.cert.hasCertFile == true}">
									<c:url var="downloadURL" value="downloadfile.htm"><c:param name="file" value="${certlink.cert.encryptedCertFilePath}" /></c:url>
									<a href='${downloadURL}' class="mainlink" target="_blank">${certlink.cert.certno}</a>
								</c:when>
								<c:otherwise>
									${certlink.cert.certno}
								</c:otherwise>
							</c:choose>
						</td>
						<td>${certlink.cert.thirdCertNo}</td>
						<td>
						    <c:choose>
								<c:when test="${certlink.cert.thirdDiv != null}">
								    ${certlink.cert.thirdDiv.comp.coname} (${certlink.cert.thirdDiv.subname})
								</c:when>
							    <c:otherwise>
								&nbsp;
							    </c:otherwise>	
						    </c:choose>
						</td>
						<td class="center">
							<c:if test="${certlink.cert.calType != null}">
								<cwms:besttranslation translations="${certlink.cert.calType.serviceType.shortnameTranslation}"/>
							</c:if>
						</td>
						<td class="center"><fmt:formatDate value="${certlink.cert.calDate}" dateStyle="medium" type="date" /></td>
						<td class="center"><fmt:formatDate value="${certlink.cert.certDate}" dateStyle="medium" type="date" /></td>
						<td>${certlink.cert.registeredBy.name}</td>
						<td><c:out value="${certlink.cert.status.getMessage()}" /></td>
						<c:if test="${calibrationVerification}">
							<td class="center">${certlink.cert.calibrationVerificationStatus.getName()}</td>
						</c:if>
						<td>
						<cwms:securedLink permission="CLIENT_CERTIFICATE_EDIT" collapse="True" classAttr="mainlink" parameter="?plantid=${certlink.inst.plantid}&certid=${certlink.cert.certid}">
							<spring:message code="edit"/>
						</cwms:securedLink>
						</td>
					</tr>
					<c:if test="${not empty certlink.cert.remarks}">
						<tr>
							<td colspan="${colspan2}" class="left bold">
								${certlink.cert.remarks}
							</td>
						</tr>
					</c:if>
					<c:set var="rowcount" value="${rowcount + 1}"/>
				</c:forEach>
			</c:otherwise>
		</c:choose>
	</tbody>
</table>
