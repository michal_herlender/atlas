<%@ tag language="java" display-name="jobs" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ attribute name="instrument" required="true" type="java.lang.Object" %>
<%@ attribute name="fastTrackTurn" required="true" type="java.lang.Object" %>
<%@ attribute name="jobItems" required="true" type="java.lang.Object" %>
<%@ attribute name="jobItemCount" required="true" type="java.lang.Object" %>


<table class="default2 instJobHist" summary='<spring:message code="viewinstrument.tablejobinformation"/>'>
	<thead>
	<tr>
		<td colspan="10"><spring:message code="viewinstrument.jobhistory"/></td>
	</tr>
	<tr>
		<th class="jobno" scope="col"><spring:message code="jobno"/></th>
		<th class="itemno" scope="col"><spring:message code="itemno"/></th>
		<th class="caltype" scope="col"><spring:message code="servicetype"/></th>
		<th class="cont" scope="col"><spring:message code="contact"/></th>
		<th class="datein" scope="col"><spring:message code="viewinstrument.datein"/></th>
		<th class="datecomp" scope="col"><spring:message code="viewinstrument.datecomplete"/></th>
		<th class="caljobreviewcost" scope="col"><spring:message code="viewinstrument.caljobreviewcost"/></th>
		<th class="calcost" scope="col"><spring:message code="viewinstrument.calcost"/></th>
		<th class="calinvoicedcost" scope="col"><spring:message code="viewinstrument.calinvoicedcost"/></th>
		<th class="repcost" scope="col"><spring:message code="viewinstrument.repaircost"/></th>
	</tr>
	</thead>
	<tfoot>
	<tr>
		<td colspan="10">&nbsp;</td>
	</tr>
	</tfoot>
	<tbody>
		<c:choose>
			<c:when test="${empty jobItems}">
				<tr>
					<td colspan="10" class="center bold">
						<spring:message code="viewinstrument.notappearedonanyjobs"/>
					</td>
				</tr>
			</c:when>
			<c:otherwise>
				<c:set var="rowcount" value="0"/>
				<c:if test="${jobItems.size() < jobItemCount}">
					<tr>
						<td colspan="10" class="center bold">
							<spring:message code="pagination.displayingmostrecentresults" arguments="${jobItems.size()}, ${jobItemCount}"/>
						</td>
					</tr>
				</c:if>

				<c:forEach var="jobitem" items="${jobItems}" varStatus="loopStatus">
					<c:set var="jobCostId" value=""/>
					<c:set var="currencySym" value=""/>
					<c:set var="caljobreviewcost" value=""/>
					<c:set var="calcost" value=""/>
					<c:set var="calinvoicedcost" value=""/>
					<c:set var="repcost" value=""/>
					<c:if test="${not empty jobitem.jobCostingItems}">
						<c:set var="jobCostId" value="${jobitem.jobCostingItems.toArray()[0].jobCosting.id}"/>
						<c:set var="currencySym" value="${jobitem.jobCostingItems.toArray()[0].jobCosting.currency.currencyERSymbol}"/>
						<c:if test="${jobitem.jobCostingItems.toArray()[0].calibrationCost != null}">
							<c:set var="calcost" value="${jobitem.jobCostingItems.toArray()[0].calibrationCost.finalCost}"/>
						</c:if>
						<c:if test="${jobitem.jobCostingItems.toArray()[0].repairCost != null}">
							<c:set var="repcost" value="${jobitem.jobCostingItems.toArray()[0].repairCost.finalCost}"/>
						</c:if>
					</c:if>
                    <!-- display calibration job review cost and calibration invoiced cost
                     only for the last 2 jobs-->					
					<c:if test="${loopStatus.index < 2}">
						<c:set var="caljobreviewcost" value="${jobitem.calibrationCost.finalCost}"/>
						<c:set var="firsttime" value="false"/>
						<c:forEach var="invoiceitem" items="${jobitem.invoiceItems}">
						   <c:if test="${invoiceitem.invoice.type.name == 'Calibration' && !firsttime}">
						   		<c:set var="firsttime" value="true" />
						   		<c:if test="${invoiceitem.calibrationCost != null}">
									<c:set var="calinvoicedcost" value="${invoiceitem.calibrationCost.finalCost}"/>
								</c:if>		   		
						   </c:if>
						</c:forEach>
					</c:if>
					
					<tr style=" background-color: ${jobitem.turn <= fastTrackTurn ? jobitem.calType.serviceType.displayColourFastTrack : jobitem.calType.serviceType.displayColour} ">
						<td class="jobno"><links:jobnoLinkDWRInfo rowcount="${rowcount}" jobno="${jobitem.job.jobno}" jobid="${jobitem.job.jobid}" copy="true"/></td>
						<td class="itemno"><links:jobitemLinkDWRInfo jobitem="${jobitem}" jicount="false" rowcount="${rowcount}"/></td>
						<td class="caltype"><cwms:besttranslation translations="${jobitem.calType.serviceType.shortnameTranslation}"/></td>
						<td class="cont">${jobitem.job.con.name}</td>
						<td class="datein"><fmt:formatDate value="${jobitem.dateIn}" type="date" dateStyle="short"/></td>
						<td class="datecomp"><fmt:formatDate value="${jobitem.dateComplete}" type="date" dateStyle="short"/></td>
						<td class="caljobreviewcost">
							<c:if test='${caljobreviewcost != "" }'>
								<c:choose>
									<c:when test='${jobCostId != "" }'>
										<a href="viewjobcosting.htm?id=${jobCostId}" class="mainlink" title="View Job Costing">${currencySym}${caljobreviewcost}</a>
									</c:when>
									<c:otherwise>
									   ${jobitem.job.currency.currencyERSymbol}${caljobreviewcost}
									</c:otherwise>
								</c:choose>
							</c:if>
						</td>
						<td class="calcost">
							<c:if test='${calcost != "" }'>
								<a href="viewjobcosting.htm?id=${jobCostId}" class="mainlink" title="View Job Costing">${currencySym}${calcost}</a>
							</c:if>
						</td>
						<td class="calinvoicedcost">
							<c:if test='${calinvoicedcost != "" }'>
							  	<c:choose>
									<c:when test='${jobCostId != "" }'>
										<a href="viewjobcosting.htm?id=${jobCostId}" class="mainlink" title="View Job Costing">${currencySym}${calinvoicedcost}</a>
									</c:when>
									<c:otherwise>
									   ${jobitem.job.currency.currencyERSymbol}${calinvoicedcost}
									</c:otherwise>
								</c:choose>
							</c:if>
						</td>
						<td class="repcost">
							<c:if test='${repcost != ""}'>
								<a href="viewjobcosting.htm?id=${jobCostId}" class="mainlink" title="View Job Costing">${currencySym}${repcost}</a>
							</c:if>
						</td>
					</tr>
					<c:set var="rowcount" value="${rowcount + 1}"/>
				</c:forEach>
			</c:otherwise>
		</c:choose>
	</tbody>
</table>

