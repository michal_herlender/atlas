<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ attribute name="instrument" required="true" type="java.lang.Object" %>
<%@ attribute name="hireAccStatuss" required="true" type="java.lang.Object" %>

<c:if test="${not empty instrument.hireInstruments}">

	<c:set var="hireInst" value="${instrument.hireInstruments.iterator().next()}" />
	
	<fieldset>
		
		<legend><spring:message code="viewinstrument.hireinstrumentinformation"/></legend>
	
		<ol>
			<c:set var="onHire" value="false"/>
			<c:if test="${hireInst.hireItems.size() > 0}">
				<c:set var="firstNonEnquiry" value="true"/>
				<c:set var="noOfEnqs" value="0"/>
				<c:forEach var="hitem" items="${hireInst.hireItems}">
					<c:choose>
						<c:when test="${hitem.hire.enquiry == false}">
							<c:if test="${firstNonEnquiry == true && hitem.offHire == false}">
								<c:set var="onHire" value="true"/>
							</c:if>
							<c:set var="firstNonEnquiry" value="false"/>
						</c:when>
						<c:otherwise>
							<c:set var="noOfEnqs" value="noOfEnqs + 1"/>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</c:if>
	
			<c:set var="actives" value="false"/>
			<c:set var="inactives" value="false"/>
			<c:forEach var="acc" items="${hireInst.hireInstAccessories}">
				<c:choose>
					<c:when test="${acc.active == true}">
						<c:set var="actives" value="true"/>
					</c:when>
					<c:otherwise>
						<c:set var="inactives" value="true"/>
					</c:otherwise>
				</c:choose>
			</c:forEach>
	
			<li class="activeHireInstAccessories">
				<label><spring:message code="viewinstrument.activeaccessories"/>:</label>
				<div class="float-left padtop">
					<c:choose>
						<c:when test="${hireInst.hireInstAccessories.size() > 0 && actives == true}">
							<div class="bold hireAccItemHead"><spring:message code="viewinstrument.itemtableaccessories"/></div>
							<div class="bold hireAccStatusHead"><spring:message code="status"/></div>
							<div class="clear"></div>
							<c:forEach var="acc" items="${hireInst.hireInstAccessories}">
								<c:choose>
									<c:when test="${acc.active==true}">
										<div class="hireAccItem">
											<span id="hireAcc${acc.hireAccessoryId}">${acc.item}</span>
											<a href="#" class="edithireaccessory" onclick=" editHireAccessoryContent(${acc.hireAccessoryId}, $j(this).prev().text(), $j(this).parent()); return false; " title="Edit hire accessory description"></a>
										</div>
										<div class="hireAccStatus">
											<select id="hireAcc${ acc.hireAccessoryId}" onchange=" updateHireAccessoryStatus(this.id, ${acc.hireAccessoryId}, this.value); return false; " <c:if test="onHire == true"> disabled="disabled" </c:if> >
												<c:forEach var="hireAccState" items="${hireAccStatuss}">
													<option value="${hireAccState.statusid}" ${hireAccState.statusid == acc.status.statusid ? "selected" : ""}>${hireAccState.name}</option>
												</c:forEach>
											</select>
										</div>
										<div class="clear"></div>
									</c:when>
									<c:otherwise>
										<c:set var="inactives" value="true"/>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<spring:message code="viewinstrument.noactiveaccessories"/>
						</c:otherwise>
					</c:choose>
				</div>
				<div class="clear"></div>
			</li>
			<li class="inactiveHireInstAccessories">
				<label><spring:message code="viewinstrument.inactiveaccessories"/>:</label>
				<div class="float-left padtop">
					<c:choose>
						<c:when test="${hireInst.hireInstAccessories.size() > 0 && inactives == true}">
							<div class="bold hireAccItemHead"><spring:message code="viewinstrument.itemtableaccessories"/></div>
							<div class="bold hireAccStatusHead"><spring:message code="status"/></div>
							<div class="clear"></div>
							<c:forEach var="acc" items="${hireInst.hireInstAccessories}">
								<c:if test="${acc.active == false}">
									<div class="hireAccItem">${acc.item}</div>
									<div class="hireAccStatus">
										<select id="hireAcc$acc.hireAccessoryId" onchange=" updateHireAccessoryStatus(this.id, ${acc.hireAccessoryId}, this.value); return false; " <c:if test="onHire == true"> disabled="disabled" </c:if>>
											<c:forEach var="hireAccState" items="${hireAccStatuss}">
												<option value="${hireAccState.statusid}" ${hireAccState.statusid == acc.status.statusid ? "selected" : ""}>${hireAccState.name}</option>
											</c:forEach>
										</select>
									</div>
									<div class="clear"></div>
								</c:if>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<spring:message code="viewinstrument.noinactiveaccessories"/>
						</c:otherwise>
					</c:choose>
				</div>
				<div class="clear"></div>
			</li>
	
			<spring:message code="viewinstrument.addaccessories" var="accessPrefix"/>
			<c:if test="${hireInst.hireInstAccessories.size() > 0}">
				<c:set var="accessPrefix" value='<spring:message code="viewinstrument.changeaccessories"/>'/>
			</c:if>
	
			<li>
				<label>${accessPrefix}:</label>
				<span>
					<c:choose>
						<c:when test="${onHire == false}">
							<a href="#" class="mainlink" onclick=" updateAccessories(${hireInst.id}); return false; ">${accessPrefix}</a>
						</c:when>
						<c:otherwise>
							<spring:message code="viewinstrument.instrumentonhireaccessoriescannotupdated"/>
						</c:otherwise>
					</c:choose>
				</span>
			</li>
		</ol>
	
	</fieldset>
</c:if>