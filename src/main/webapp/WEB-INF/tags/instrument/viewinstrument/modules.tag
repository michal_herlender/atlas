<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%@ attribute name="instrument" required="true" type="java.lang.Object" %>


<c:choose>
	<c:when test="${instrument.model.modelType.baseUnits == true}">

		<h5><spring:message code="viewinstrument.instrumentmodeltypesupportsinstrumentbelongingtobaseunit"/></h5>

		<table class="default2" id="instrumentbaseunit" summary='<spring:message code="viewinstrument.tableallmodulescurrentlyattachedtobaseunit"/>'>
			<thead>
				<c:choose>
					<c:when test="${instrument.baseUnit != null}">
						<tr>
							<td colspan="3">
								<spring:message code="viewinstrument.instrumentcurrentlymoduleof"/>:
								<links:showInstrumentLink instrument="${instrument.baseUnit}"/>: <instmodel:showInstrumentModelLink instrument="${instrument.baseUnit}" caltypeid="0"/>
								- <spring:message code="viewinstrument.attachedmodulestobaseunit"/> (<span id="instModulesSize">${instrument.baseUnit.modules.size()}</span>)
							</td>
						</tr>
						<tr>
							<td colspan="3"><spring:message code="viewinstrument.completesystem"/>:</td>
						</tr>
					</c:when>
					<c:otherwise>
						<tr>
							<td colspan="3">
								<spring:message code="viewinstrument.instrumentnotmoduleofsystem"/>
							</td>
						</tr>
					</c:otherwise>
				</c:choose>
			</thead>
			<tfoot>
				<c:choose>
					<c:when test="${instrument.baseUnit}">
						<tr>
							<td colspan="3">&nbsp;</td>
						</tr>
					</c:when>
					<c:otherwise>
						<tr>
							<td colspan="3">
								<a href="#" class="domthickbox mainlink-float" onclick=" tb_show('Available Base Units', 'TB_dom?width=500&height=260', '', thickboxAvailableBaseUnitContent(${instrument.plantid}, ${instrument.comp.coid}, ${instrument.model.modelid})); return false; " title=""><spring:message code="viewinstrument.addbaseunits"/></a>
							</td>
						</tr>
					</c:otherwise>
				</c:choose>
			</tfoot>
			<tbody>
				<c:choose>
					<c:when test="${instrument.baseUnit}">
						<tr>
							<td class="center"><img src="img/icons/instrument.png" width="16" height="16" alt='<spring:message code="viewinstrument.baseunit"/>' title='<spring:message code="viewinstrument.baseunit"/>' /></td>
							<td colspan="2"><links:showInstrumentLink instrument="${instrument.baseUnit}"/>: <instmodel:showInstrumentModelLink instrument="${instrument.baseUnit}" caltypeid="0"/></td>
						</tr>
						<c:forEach var="module" items="${instrument.baseUnit.modules}">
							<tr id="m_${module.plantid}">
								<td class="modbaseinst"><img src="img/icons/arrow_merge.png" width="16" height="16" alt='<spring:message code="viewinstrument.module"/>' title='<spring:message code="viewinstrument.module"/>' /></td>
								<td class="modbasemodel">
									<c:choose>
										<c:when test="${module.plantid == $instrument.plantid}">
											${module.plantid} : <cwms:showmodel instrumentmodel="${module.model}"/>
										</c:when>
										<c:otherwise>
											<links:showInstrumentLink instrument="${module}"/>: <instmodel:showInstrumentModelLink instrument="${module}" caltypeid="0"></instmodel:showInstrumentModelLink>
										</c:otherwise>
									</c:choose>
								</td>
								<td class="modbaseremove">
									<a href="#" class="mainlink" onclick=" removeModuleFromBaseUnitList(${module.plantid}, ${instrument.plantid}, ${instrument.comp.coid}, ${instrument.model.modelid}, 'basetomod'); return false; ">
										<img src="img/icons/delete.png" width="16" height="16" alt='<spring:message code="viewinstrument.removemodule"/>' title='<spring:message code="viewinstrument.removemodule"/>' />
									</a>
								</td>
							</tr>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<tr>
							<td colspan="3" class="center bold"><spring:message code="viewinstrument.instrumentnotmoduleofsystem"/></td>
						</tr>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</c:when>
	<c:when test="${instrument.model.modelType.modules == true}">

		<h5><spring:message code="viewinstrument.instrumentmodeltypesupportshavingmultipleattachedmodules"/></h5>

		<table class="default2" id="instrumentmodules" summary='<spring:message code="viewinstrument.tableallmodulesattachedtobaseunit"/>'>
			<thead>
				<tr>
					<td colspan="3"><spring:message code="viewinstrument.attachedmodules"/> (<span id="instModulesSize">${instrument.modules.size()}</span>)</td>
				</tr>
				<tr>
					<td colspan="3"><spring:message code="viewinstrument.completesystem"/>:</td>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="3">
						<a href="#" class="domthickbox mainlink-float" onclick=" tb_show('Available Modules', 'TB_dom?width=500&height=260', '', thickboxAvailableModuleContent(${instrument.plantid}, ${instrument.comp.coid}, ${instrument.model.modelid}); return false; " title=""><spring:message code="viewinstrument.addmodules"/></a>
					</td>
				</tr>
			</tfoot>
			<tbody>
				<tr>
					<td class="center"><img src="img/icons/instrument.png" width="16" height="16" alt='<spring:message code="viewinstrument.baseunit"/>' title='<spring:message code="viewinstrument.baseunit"/>' /></td>
					<td colspan="2">${instrument.plantid} : <instmodel:showInstrumentModelName instrument="${instrument}" relativePathToImages="" caltypeid="0"/></td>
				</tr>
				<c:forEach var="module" items="${instrument.modules}">
					<tr id="m_${module.plantid}">
						<td class="modbaseinst"><img src="img/icons/arrow_merge.png" width="16" height="16" alt='<spring:message code="viewinstrument.module"/>' title='<spring:message code="viewinstrument.module"/>' /></td>
						<td class="modbasemodel"><links:showInstrumentLink instrument="${module}"/>: <instmodel:showInstrumentModelLink instrument="$module" caltypeid="0"/></td>
						<td class="modbaseremove">
							<a href="#" class="mainlink" onclick=" removeModuleFromBaseUnitList(${module.plantid}, '', '', '', 'modtobase'); return false; ">
								<img src="img/icons/delete.png" width="16" height="16" alt='<spring:message code="viewinstrument.removemodule"/>' title='<spring:message code="viewinstrument.removemodule"/>' />
							</a>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</c:when>
</c:choose>