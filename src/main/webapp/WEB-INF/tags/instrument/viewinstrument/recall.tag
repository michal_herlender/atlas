<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ attribute name="instrument" required="true" type="org.trescal.cwms.core.instrument.entity.instrument.Instrument" %>
<%@ attribute name="requirements" required="true"  type="java.lang.Object" %>
<%@ attribute name="units" required="true"  type="java.lang.Object" %>
<%@ attribute name="servicetypes" required="true" type="java.lang.Object" %>
<%@ attribute name="certlinkCount" required="true" type="java.lang.Object" %>
<%@ attribute name="instCertLinkCount" required="true" type="java.lang.Object" %>
<%@ attribute name="lastCalDate" required="true" type="java.lang.Object" %>
<%@ attribute name="recalldue" required="true" type="java.lang.Object" %>
<%@ attribute name="defaultrecall" required="true" type="java.lang.Object" %>
<%@ attribute name="activerecallrules" required="true" type="java.lang.Object" %>
<%@ attribute name="inactiverecallrules" required="true" type="java.lang.Object" %>

<%-- Check if item has any past certificates, if so then give the option to apply this new
rule starting from the last certificate date. --%>

<c:set var="applyFromCert" value="false"/>
<c:if test="${certlinkCount > 0 || instCertLinkCount > 0}">
	<c:set var="applyFromCert" value="true"/>
</c:if>
<c:set var="formattedLastCalDate" value=""/>
<c:if test="${not empty lastCalDate}">
	<fmt:formatDate value="${lastCalDate}" type="date" dateStyle="SHORT" />
</c:if>

<fieldset>

	<input type="hidden" value="${instrument.plantid}" id="plantid"/>
	<input type="hidden" value="${applyFromCert}" id="hascert"/>

	<legend><spring:message code="viewinstrument.currentrecallstatus"/></legend>

	<ol>
		<c:choose>
			<c:when test="${instrument.nextCalDueDate != null}">
				<li>
					<spring:message code="viewinstrument.nextberecalled"/>&nbsp;
					<span id="nextCalRecall" class="bold">${recalldue}</span> -
					<button type="button" class="newRecallRule">
						<spring:message code="viewinstrument.addrecallrule"/>
					</button>
				</li>
			</c:when>
			<c:otherwise>
				<li>
					<spring:message code="viewinstrument.nocalibrationduedateset"/> -
					<button type="button" class="newRecallRule">
						<spring:message code="viewinstrument.addrecallrule"/>
					</button>
				</li>
			</c:otherwise>
		</c:choose>
	</ol>

</fieldset>



<table class="default2 instrumentRecallHistory" summary='<spring:message code="viewinstrument.tableallrecallhistory"/>'>
	<thead>
		<tr>
			<td colspan="7"><spring:message code="viewinstrument.recallhistory"/> (${instrument.recalls.size()})</td>
		</tr>
		<tr>
			<th class="recallid" scope="col"><spring:message code="viewinstrument.recallid"/></th>
			<th class="recalldate" scope="col"><spring:message code="viewinstrument.recalldate"/></th>
			<th class="recalledto" scope="col"><spring:message code="viewinstrument.recalledto"/></th>
			<th class="recallnotes" scope="col"><spring:message code="viewinstrument.recallnotes"/></th>
			<th class="prevcert" scope="col"><spring:message code="viewinstrument.previouscertificate"/></th>
			<th class="prevcaldate" scope="col"><spring:message code="viewinstrument.previouscaldate"/></th>
			<th class="addrecallnote" scope="col"><spring:message code="addnote"/></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="7">&nbsp;</td>
		</tr>
	</tfoot>
	<tbody>
		
		<c:if test="${instrument.recalls.size() < 1}">
			<tr>
				<td colspan="7" class="text-center bold"><spring:message code="viewinstrument.neverrecalled"/></td>
			</tr>
		</c:if>
		<c:forEach var="recall" items="${instrument.recalls}" varStatus="loopStatus">
			<c:set var="rowcount" value="${loopStatus.index}"/>
			<c:choose>
				<c:when test="${recall.excludeFromNotification == false}">
					<tr id="parent${rowcount}">
						<td class="recallid">
						    ${recall.recall.recallNo}
                        </td>
						<td class="recalldate">
                            <fmt:formatDate value="${recall.recall.date}" type="date" pattern="dd-MM-yyyy"/>
                        </td>
						<td class="recalledto">
						    ${recall.contact.name}
                        </td>
						<td class="recallnotes">
							<span id="recallResponseNoteCount${rowcount}">${recall.recallResponseNotes.size()}</span>
							<a href="#" id="rrLink${rowcount}" onclick=" toggleRecallResponses(${rowcount}); return false; ">
								<c:choose>
									<c:when test="${loopStatus.index > 1}">
										<img src="img/icons/items.png" width="16" height="16"
											 alt='<spring:message code="viewinstrument.showrecallresponsenotes"/>'
											 title='<spring:message code="viewinstrument.showrecallresponsenotes"/>'
											 class="image_inline" />
									</c:when>
									<c:otherwise>
										<img src="img/icons/items_hide.png" width="16" height="16"
											 alt='<spring:message code="viewinstrument.hiderecallresponsenotes"/>'
											 title='<spring:message code="viewinstrument.hiderecallresponsenotes"/>'
											 class="image_inline" />
									</c:otherwise>
								</c:choose>
							</a>
						</td>
						<td class="prevcert">
							<c:forEach var="c" items="${instrument.lastCal.certs}">
								${c.certno}<br/>
							</c:forEach>
						</td>
						<td class="prevcaldate">
							<fmt:formatDate value="${instrument.lastCal.calDate}" type="date" pattern="dd-MM-yyyy"/>
                        </td>
						<td class="addrecallnote">
							<a href="#" class="addRecallNote"
                               onclick=" event.preventDefault(); generateRecallResponseContent(${recall.id}, ${rowcount}); "
                               title='<spring:message code="viewinstrument.addrecallresponsenote"/>'>
                                &nbsp;
                            </a>
						</td>
					</tr>
					<tr id="child${rowcount}" class = '${loopStatus.index > 1 ? "hid" : "vis"}'>
						<td colspan="7">
							<div style=' display: ${loopStatus.index > 1 ? "none" : "block"};'>
								<table class="child_table recallResponseNotes"
									   summary='<spring:message code="viewinstrument.tablerecallitemresponsenotes"/>'>
									<thead>
										<tr>
											<th class="status"><spring:message code="status"/></th>
											<th class="comments"><spring:message code="comment"/></th>
											<th class="setby"><spring:message code="viewinstrument.setby"/></th>
											<th class="seton"><spring:message code="viewinstrument.seton"/></th>
											<th class="del"><spring:message code="delete"/></th>
										</tr>
									</thead>
									<tbody>
										<c:choose>
											<c:when  test="${recall.recallResponseNotes.size() > 0}">
												<c:forEach var="note" items="${recall.recallResponseNotes }">
													<tr id="rrn${note.noteid}">
														<td class="status">
															<cwms:besttranslation translations="${note.recallResponseStatus.descriptiontranslation}"/>
														</td>
														<td class="comments">${note.comment}</td>
														<td class="setby">${note.setBy.name}</td>
														<td class="seton">
															<fmt:formatDate value="${note.setOn}" type="time" pattern="HH:mm" />
															&nbsp;
															<fmt:formatDate value="${note.setOn}" type="date" pattern="dd-MM-yyyy" />
														</td>
														<td class="del">
															<a href="#"
															   onclick=" event.preventDefault(); deleteRecallResponseNote(${note.noteid}, ${rowcount}); "
															   class="deleteAnchor"
															   title='<spring:message code="viewinstrument.deleterecallresponsenote"/>'>
																&nbsp;
															</a>
														</td>
													</tr>
												</c:forEach>
											</c:when>
											<c:otherwise>
												<tr>
													<td colspan="5" class="bold text-center">
                                                        <spring:message code="viewinstrument.norecallresponsenotes"/>
                                                    </td>
												</tr>
											</c:otherwise>
										</c:choose>
									</tbody>
								</table>
							</div>
						</td>
					</tr>
				</c:when>
				<c:otherwise>
					<tr class="highlight">
						<td class="recallid">${recall.recall.recallNo}</td>
						<td class="recalldate"><fmt:formatDate value="${recall.recall.date}" type="date" pattern="dd-MM-yyyy" /></td>
						<td colspan="5"><spring:message code="viewinstrument.instrumentexcludedfromrecall"/></td>
					</tr>
				</c:otherwise>
			</c:choose>
			<c:set var="rowcount" value="${rowcount + 1}"/>
		</c:forEach>
	</tbody>
</table>



<table class="default2 instrumentActiveRecallRules"
       summary='<spring:message code="viewinstrument.tableallactiverecallrules"/>'>
	<thead>
		<tr>
			<td colspan="6">
				<spring:message code="viewinstrument.activerecallrule"/> (<span id="activeRecallCount">${activerecallrules.size()}</span>)
				<c:if test="${activerecallrules.size() > 0}">
				- <spring:message code="viewinstrument.showninorderofprecedence"/>:
				</c:if>
			</td>
		</tr>
		<tr>
			<th class="interval" scope="col"><spring:message code="viewinstrument.interval"/></th>
			<th class="seton" scope="col"><spring:message code="viewinstrument.nextdate"/></th>
			<th class="type" scope="col"><spring:message code="type"/></th>
			<th class="scope" scope="col"><spring:message code="viewinstrument.scope"/></th>
			<th class="setby" scope="col"><spring:message code="viewinstrument.setby"/></th>
			<th class="actions" scope="col"><spring:message code="viewinstrument.actions"/></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="6">&nbsp;</td>
		</tr>
	</tfoot>
	<tbody>
		<c:if test="${activerecallrules.size() > 0}">
			<c:forEach var="rule" items="${activerecallrules}">
				<c:if test="${rule.active == true}">
					<c:set var="blanketRule" value=""/>
					<c:if test="${rule.con != null || rule.sub != null|| rule.comp !=null}">
						<c:set var="blanketRule" value="id='blanketRule'"/>
					</c:if>
					<tr ${blanketRule}>
						<td class="interval">
							<span>${rule.interval}</span> 
								${rule.intervalUnit.getName(rule.interval)}
						</td>
						<td>
							<fmt:formatDate value="${rule.calibrateOn}" type="date"/>
						</td>
						<td>
							<c:choose>
								<c:when test='${rule.recallRequirementType !=  "SERVICE_TYPE"}'>
									<c:out value="${rule.recallRequirementType.getName()}"/>
								</c:when>
								<c:otherwise>
									<cwms:besttranslation translations="${rule.serviceType.longnameTranslation }"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="scope">
							<c:choose>
								<c:when test="${rule.instrument != null}">
									<spring:message code="viewinstrument.coversjustthisinstrument"/>
								</c:when>
								<c:otherwise>
									<spring:message code="viewinstrument.coveringallinstrumentsbelongingto"/>
									<c:choose>
										<c:when test="${rule.con != null}">
											(<spring:message code="contact"/>) ${rule.con.name}
										</c:when>
										<c:when test="${rule.sub != null}">
											(<spring:message code="subdivision"/>) ${rule.sub.subname}
										</c:when>
										<c:when test="${rule.comp != null}">
											(<spring:message code="company"/>) ${rule.comp.coname}
										</c:when>
									</c:choose>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="setby">${rule.setBy.name} (<fmt:formatDate value="${rule.setOn}" type="date"/>)</td>
						<td class="actions">
                            <c:choose>
                                <c:when test="${rule.instrument != null}">
                                    <input type="hidden" name="activeRuleId" value="${rule.id}" class="activeRuleId"/>
                                    <a href="#"
                                       class="mainlink deactivate"
                                       title='<spring:message code="viewinstrument.deactivatesrecallrule"/>'>
                                            <spring:message code="viewinstrument.removerule"/>
                                    </a>
                                    <c:if test="${applyFromCert == true && rule.recallRequirementType.name() == 'FULL_CALIBRATION'}">
                                        | <a href="#"
                                           class="mainlink deactivateandreset"
                                           title='<spring:message code="viewinstrument.deactivatesrecallruleandresetrecalldate"/>'>
                                            <spring:message code="viewinstrument.removeruleandresetcaldate"/>
                                        </a>
                                    </c:if>
                                </c:when>
                                <c:otherwise>
                                    <spring:message code="viewinstrument.rulecanbeoverridden"/>
                                </c:otherwise>
                            </c:choose>
						</td>
					</tr>
				</c:if>
			</c:forEach>
		</c:if>
		<tr>
			<td colspan="6" class="text-center bold">
				<c:choose>
					<c:when test="${instrument.calFrequency != null}">
                        <spring:message code="viewinstrument.currentsystemdefaultrecall3"/>&nbsp;
                        ${instrument.calFrequency}&nbsp;${instrument.calFrequencyUnit.getName(instrument.calFrequency)}
					</c:when>
					<c:otherwise>
                        <spring:message code="viewinstrument.currentsystemdefaultrecall1"/>
                        <span>${defaultrecall}&nbsp;<spring:message code="months"/></span>
                    </c:otherwise>
				</c:choose>
                <spring:message code="viewinstrument.currentsystemdefaultrecall2"/>
			</td>
		</tr>
	</tbody>
</table>



<table class="default2 instrumentInactiveRecallRules" summary='<spring:message code="viewinstrument.tableallinactiverecall"/>'>
	<thead>
		<tr>
			<td colspan="6">
                <spring:message code="viewinstrument.inactiverecallrules"/> (<span id="inactiveRecallCount">${inactiverecallrules.size()}</span>)
            </td>
		</tr>
		<tr>
			<th class="interval" scope="col"><spring:message code="viewinstrument.interval"/></th>
			<th class="seton" scope="col"><spring:message code="viewinstrument.nextdate"/></th>
			<th class="type" scope="col"><spring:message code="type"/></th>
			<th class="scope" scope="col"><spring:message code="viewinstrument.scope"/></th>
			<th class="setby" scope="col"><spring:message code="viewinstrument.deactivatedby"/></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="6">&nbsp;</td>
		</tr>
	</tfoot>
	<tbody>
		<c:choose>
			<c:when test="${inactiverecallrules.size() < 1}">
				<tr>
					<td colspan="6" class="bold text-center"><spring:message code="viewinstrument.noinactiverecallrules"/></td>
				</tr>
			</c:when>
			<c:otherwise>
				<c:forEach var="rule" items="${inactiverecallrules}">
					<c:if test="${rule.active != true && rule.instrument != null}">
						<tr id="inactive$rule.id">
							<td class="interval"><span>${rule.interval}</span> 
								${rule.intervalUnit.getName(rule.interval)}
							</td>
							<td>
							<fmt:formatDate value="${rule.calibrateOn}" type="date"/>
						</td>
						<td>
							<c:choose>
								<c:when test='${rule.recallRequirementType !=  "SERVICE_TYPE"}'>
									<c:out value="${rule.recallRequirementType.getName()}"/>
								</c:when>
								<c:otherwise>
									<cwms:besttranslation translations="${rule.serviceType.longnameTranslation }"/>
								</c:otherwise>
							</c:choose>
						</td>
							<td class="scope">
								<c:choose>
									<c:when test="${rule.instrument != null}">
										<spring:message code="viewinstrument.coversjustthisinstrument"/>
									</c:when>
									<c:otherwise>
										<spring:message code="viewinstrument.coveringallinstrumentsbelongingto"/>
										<c:choose>
											<c:when test="${rule.con != null}">
												(<spring:message code="contact"/>) ${rule.con.name}
											</c:when>
											<c:when test="${rule.sub != null}">
												(<spring:message code="subdivision"/>) ${rule.sub.subname}
											</c:when>
											<c:when test="${rule.comp != null}">
												(<spring:message code="company"/>) ${rule.comp.coname}
											</c:when>
										</c:choose>
									</c:otherwise>
								</c:choose>
							</td>

							<td class="setby">${rule.deactivatedBy.name} (<fmt:formatDate value="${rule.deactivatedOn}" type="date"/>)</td>
						</tr>
					</c:if>
				</c:forEach>
			</c:otherwise>
		</c:choose>
	</tbody>
</table>

<div id="dialog" style="display: none;">
	<!-- html template used to create pop up form to add a new recall rule -->

	<div class="hid warningBox1">
		<div class="warningBox2">
			<div class="warningBox3">
			</div>
		</div>
	</div>
	<fieldset>
		<ol>
			<li class="relative">
				<label><spring:message code="viewinstrument.recallRequirementType"/> :</label>
				<div id="recallRequirementType" class="float-left vis">
					<select id="requirementTypeId">
						<c:forEach var="requirement" items="${requirements}">
							<option value="${requirement.key}">${requirement.value}</option>
						</c:forEach>
					</select>
				</div>
				<!--  clear floats and restore page flow -->
				<div class="clear"></div>
			</li>
			<li class="relative">
				<label><spring:message code="viewinstrument.nextdate"/> :</label>
				<div id="recallCalibrateDate" class="float-left ${(applyFromCert == true) ? 'hid' : 'vis'}">
					<input type="date" name="calibrateOn" id="calibrateOn" />
				</div>
				<div id="recallCalibrateCert" class="float-left ${(applyFromCert == true) ? 'vis' : 'hid'}">
					<span> <spring:message code="viewinstrument.recallCalibrateCert"/> </span>
				</div>
				<!--  clear floats and restore page flow -->
				<div class="clear"></div>
			</li>
			<li>
				<label><spring:message code="viewinstrument.recallIntervalLabel"/></label>
				<div id="recallInterval" class="float-left vis">
					<input type="text" name="interval" id="interval" />
					<select id="intervalUnitId">
						<c:forEach var="unit" items="${units}">
							<c:choose>
								<c:when test="${unit.key eq instrument.calFrequencyUnit}">
									<c:set var="selectedText" value="selected='selected'"/>
								</c:when>
								<c:otherwise>
									<c:set var="selectedText" value=""/>
								</c:otherwise>
							</c:choose>
							<option value="${unit.key}" ${selectedText}>${unit.value}</option>
						</c:forEach>
					</select>
				</div>
				<div id="recallNoInterval" class="float-left hid">
					<span><spring:message code="viewinstrument.recallNoInterval"/></span>
				</div>
				<!--  clear floats and restore page flow -->
				<div class="clear"></div>
			</li>
			<li id="updateInstrumentDiv">
					<fieldset>
						<legend>
							<spring:message code="viewinstrument.updateinstrumentinterval"/>
							${formattedLastCalDate}
							<input type="checkbox"
								   name="updateInstrument"
								   id="updateInstrument"
								   checked="checked"/>
						</legend>
					</fieldset>
			</li>
			<c:if test="${applyFromCert == true }">
				<li id="linkToCert">
					
						<fieldset>
							<legend>
								<spring:message code="viewinstrument.backDate"/>
								${formattedLastCalDate}
								<input type="checkbox"
									   name="applyfromcert"
									   id="applyfromcert"
									   checked="checked"/>
							</legend>
						</fieldset>
					
				</li>
			</c:if>
			<li id="serviceTypeSelector" hidden=true>
				<label><spring:message code="servicetype"/>:</label>
				<select id="serviceTypeId">
						<c:forEach var="service_type" items="${servicetypes}">
							<option value="${service_type.key}">${service_type.value}</option>
						</c:forEach>
					</select>
			</li>
			<li>
				<label>&nbsp;</label>
				<input type="button" value="<spring:message code='viewinstrument.createRecallRule'/>" name="save" id="createRecallRuleBtn" />
			</li>
		</ol>
	</fieldset>
</div>

