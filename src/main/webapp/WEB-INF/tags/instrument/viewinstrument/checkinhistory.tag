<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>

<%@ attribute name="checkin" required="true" type="java.lang.Object" %>

<table id="checkOutInstruments" class="default2">
			<thead>
				<tr>
					<th colspan="5">
						<div class="float-left">
							<spring:message code="instrument.checkedoutInstr"/> (${checkin.size()})
						</div>
								
						<a href="#" id="checkInLink" class="mainlink-float checkInLink hid " onclick="checkInInstruments();">Check In</a>
 					</th>
				</tr>
				 <tr>
					<th class="centre" scope="col"><spring:message code="barcode"/></th>
					<th class="centre" scope="col"><spring:message code="instmodelname"/></th>
					<th class="centre" scope="col"><spring:message code="instrument.checkoutdate"/></th>
					<th class="centre" scope="col"><spring:message code="instrument.checkindate"/></th>
					<th class="centre" scope="col"><spring:message code="jobno"/> </th>		  
				</tr>				
			</thead>
			<tbody>
				
				<c:set var="rowcount" value="0"/>
					<c:forEach var="element" items="${checkin}">
						<tr>
							<td>	
								<links:showInstrumentLink instrument="${element.instrument}"/>
							</td>
							 <td class="center">
								<cwms:showinstrument instrument="${element.instrument}" />
							 </td>
							 <td>	
								${element.checkOutDate}
							</td>
							<td>
								${element.checkInDate}
							 </td>
							<td>
								<c:if test="${element.job != null}">
									<links:jobnoLinkDWRInfo rowcount="${rowcount}" jobno="${element.job.jobno}" jobid="${element.job.jobid}" copy="${false}"/>
								 </c:if>
							</td>

							 <c:set var="rowcount" value="${rowcount + 1}"/>
						</tr>
				</c:forEach>
		 </tbody>
</table>