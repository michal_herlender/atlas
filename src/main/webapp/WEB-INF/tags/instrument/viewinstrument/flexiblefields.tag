<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ attribute name="instrument" required="true" type="java.lang.Object" %>
<%@ attribute name="flexiblefields" required="true" type="java.lang.Object" %>

<table class="default2 instFlexibleField" summary="This table lists all flexible fields for the customer">
	<thead>
	<tr>
		<th class="field" scope="col"><spring:message code="viewinstrument.field"/></th>
		<th class="fieldtype" scope="col"><spring:message code="type"/></th>
		<th class="fieldvalue" scope="col"><spring:message code="viewinstrument.fieldvalue"/></th>
		<th class="fieldvalue" scope="col"><spring:message code="viewinstrument.action"/></th>
	</tr>
	</thead>
	<tbody>
		<c:choose>
			<c:when test="${flexiblefields.size() > 0}">
				<c:forEach var="flexiblefield" items="${flexiblefields}">
					<tr>
						<td>${flexiblefield.fieldName}</td>
						<td>${flexiblefield.fieldType}</td>
						<td>
							${flexiblefield.value != null ? flexiblefield.value : "&nbsp;"}
						</td>
						<td>
							<c:choose>
								<c:when test="${flexiblefield.valueId > 0}">
									<a href="addeditflexiblefieldvalue.htm?plantid=${instrument.plantid}&valueid=${flexiblefield.valueId}"><spring:message code="edit"/></a>
								</c:when>
								<c:otherwise>
									<a href="addeditflexiblefieldvalue.htm?plantid=${instrument.plantid}&fielddefid=${flexiblefield.fieldDefinitionId}"><spring:message code="edit"/></a>
								</c:otherwise>
							</c:choose>
						</td>
					</tr>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<tr><td colspan=4><spring:message code="viewinstrument.noflexiblefields"/></td></tr>
			</c:otherwise>
		</c:choose>
	</tbody>
</table>
