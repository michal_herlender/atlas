<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%@ attribute name="instrument" required="true" type="java.lang.Object" %>

<table class="default2 instQuoteHistory" summary="This table lists all quotation history for the instrument">
	<thead>
		<tr>
			<td colspan="7"><spring:message code="viewinstrument.quotationhistory"/> (${instrument.quoteItems.size()})</td>
		</tr>
		<tr>
			<th class="qno" scope="col"><spring:message code="viewinstrument.quoteno"/></th>
			<th class="itemno" scope="col"><spring:message code="itemno"/></th>
			<th class="contact" scope="col"><spring:message code="contact"/></th>
			<th class="status" scope="col"><spring:message code="status"/></th>
			<th class="caltype" scope="col"><spring:message code="servicetype"/></th>
			<th class="date" scope="col"><spring:message code="viewinstrument.issuedate"/></th>
			<th class="cost" scope="col"><spring:message code="viewinstrument.finalcost"/></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="7">&nbsp;</td>
		</tr>
	</tfoot>
	<tbody>
		<c:choose>
			<c:when test="${instrument.quoteItems.size() > 0}">
				<c:set var="rowcount" value="0"/>
				<c:forEach var="qi" items="${instrument.quoteItems}">
					<tr  style=" background-color: ${qi.caltype.serviceType.displayColour}; ">
						<td class="qno"><links:quoteLinkInfo quoteId="${qi.quotation.id}" quoteNo="${qi.quotation.qno}" 
						quoteVer="${qi.quotation.ver}" showversion="true" rowcount="${rowcount}"/></td>
						<td class="itemno">${qi.itemno}</td>
						<td class="contact">${qi.quotation.contact.name}</td>
						<td class="status"><cwms:besttranslation translations="${qi.quotation.quotestatus.nametranslations}"/></td>
						<td class="caltype"><cwms:besttranslation translations="${qi.caltype.serviceType.longnameTranslation}"/></td>
						<td class="date"><fmt:formatDate value="${qi.quotation.issuedate}" type="date" dateStyle="short"/></td>
						<td class="cost">${qi.quotation.currency.currencyERSymbol}${qi.finalCost}</td>
					</tr>
					<c:set var="rowcount" value="${rowcount + 1}"/>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<tr>
					<td colspan="7" class="bold text-center"><spring:message code="viewinstrument.instrumentnotappearedquotations"/></td>
				</tr>
			</c:otherwise>
		</c:choose>
	</tbody>
</table>

