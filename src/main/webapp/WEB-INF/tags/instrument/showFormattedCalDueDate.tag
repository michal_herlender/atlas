<%@ tag language="java" display-name="showFormattedCalDueDate" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ attribute name="instrument" required="true" type="java.lang.Object" %>

<c:set var="spanStyle" value="" />
<c:if test="${instrument.outOfCalibration}">
	<c:set var="spanStyle" value="class='attention'" />
</c:if>

<span ${spanStyle}>
	<c:if test="${instrument.nextCalDueDate != null}">
		<fmt:formatDate value="${instrument.nextCalDueDate}"/>
	</c:if>
</span>
