<%@ tag language="java" display-name="subdivFilter" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ attribute name="subdiv" required="true" type="java.lang.Object" %>

<c:choose>
	<c:when test="${subdiv.subname == 'Un-named Subdivision'}">
		N/A
	</c:when>
	<c:otherwise>
		${subdiv.subname}
	</c:otherwise>
</c:choose>