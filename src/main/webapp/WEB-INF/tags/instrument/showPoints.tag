<%@ tag language="java" display-name="showPoints" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ attribute name="pointSet" required="true" type="java.lang.Object" %>

<span>
	<c:forEach var="point" items="${pointSet.points}" varStatus="forEachStatus">
		${point.getFormattedPoint()} ${point.uom.formattedSymbol}
		<c:if test="${point.getFormattedRelatedPoint() != ''}">
			@ ${point.getFormattedRelatedPoint()} ${point.relatedUom.formattedSymbol}
		</c:if>
		<c:if test="${not forEachStatus.last}">
		|
		</c:if>
	</c:forEach>
</span>