<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="searchdialog" style="display: none; overflow: auto">

    <fieldset>
        <ol>
            <li>
                <label><spring:message code="sub-family"/></label>
                <input type="text" id="desc" name="desc"/>
                <input type="hidden" id="descid" name="descid"/>
            </li>
            <li>
                <label><spring:message code="manufacturer"/></label>
                <input type="text" id="mfr" name="mfr"/>
                <input type="hidden" id="mfrid" name="mfrid"/>
            </li>
            <li>
                <label><spring:message code="model"/></label>
                <input type="text" id="model"/>
            </li>
            <li>
                <label><spring:message code="serialno"/></label>
                <input type="text" id="serialno">
            </li>
            <li>
                <label><spring:message code="plantno"/></label>
                <input type="text" id="plantno">
            </li>
            <li>
                <label><spring:message code="web.barcode"/></label>
                <input type="text" id="barcode"/>
            </li>
            <li>
                <label><spring:message code="web.incalibrationonly"/></label>
                <input type="checkbox" id="incal" checked="checked" value="true"/>
            </li>
            <li>
                <button id="startsearch"><spring:message code="search"/></button>
            </li>
        </ol>
    </fieldset>

</div>

<div id="searchresultsdialog" style="display: none;">
    <div id="loaderDiv" style="display: none; text-align: center">
        <spring:message code="searching"/><br/><br/>
        <img src="../img/icons/ajax-loader.gif" width="31" height="31" alt="">
    </div>
    <div id="searchagaindiv" style="display: none">
        <button id="searchagain"><spring:message code="viewjob.searchAgain"/></button>
    </div>
</div>