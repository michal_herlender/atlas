<%@ tag language="java" display-name="usages" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ attribute name="instrument" required="true" type="org.trescal.cwms.core.instrument.entity.instrument.Instrument" %>

<c:if test = "${instrument.calExpiresAfterNumberOfUses}">
    <div >
        <c:choose>
            <c:when test="${instrument.usesSinceLastCalibration >= instrument.permittedNumberOfUses}">
                <c:set var="usagesstyle" value="usagesbad"/>
            </c:when>
            <c:when test="${instrument.usesSinceLastCalibration == (instrument.permittedNumberOfUses - 1)}">
                <c:set var="usagesstyle" value="usagesdodgy"/>
            </c:when>
            <c:otherwise>
                <c:set var="usagesstyle" value="usagesgood"/>
            </c:otherwise>
        </c:choose>
        <spring:message code="viewinstrument.usagessincelastcal"/>&nbsp;
        <span class="${usagesstyle}" id="usageSpan">
            <span id="numberOfUses">
                <c:out value="${instrument.usesSinceLastCalibration}"/>
            </span>
            &nbsp;<spring:message code="pagination.of"/>&nbsp;
            <c:out value="${instrument.permittedNumberOfUses}"/>
        </span>
    </div>
</c:if>