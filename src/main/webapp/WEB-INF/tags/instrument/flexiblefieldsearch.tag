<%@ tag language="java" display-name="finance" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ attribute name="flexiblefields" required="true" type="java.lang.Object" %>

<script>
	//prevent user from entering anything other than numbers in numeric field
	$j(document).ready(function() {
			    $j(".numericField").keydown(function (e) {
			        // Allow: backspace, delete, tab, escape, enter and .
			        if ($j.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
			             // Allow: Ctrl+A
			            (e.keyCode == 65 && e.ctrlKey === true) ||
			             // Allow: Ctrl+C
			            (e.keyCode == 67 && e.ctrlKey === true) ||
			             // Allow: Ctrl+X
			            (e.keyCode == 88 && e.ctrlKey === true) ||
			             // Allow: home, end, left, right
			            (e.keyCode >= 35 && e.keyCode <= 39)) {
			                 // let it happen, don't do anything
			                 return;
			        }
			        // Ensure that it is a number and stop the keypress
			        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			            e.preventDefault();
			        }
			    });
			});
	
    
    
    var jqCalendar = jqCalendar || [];
    
    		<c:forEach var="field" items="${flexiblefields}" varStatus="loopStatus">
    			<c:if test="${field.fieldType == 'DATETIME'}">
    			jqCalendar.push(
			 		{ 
			 		inputField: 'dateField${loopStatus.index}',
			 		dateFormat: 'dd.mm.yy',
			 		displayDates: 'all',
			 		showWeekends: true
			 		});
			 	</c:if>
			</c:forEach>
	</script>

<ol>
	<c:forEach var="field" items="${flexiblefields}" varStatus="loopStatus">
		<li>
			<label>${field.name}</label>
			<form:hidden path="flexibleFieldSearches[${loopStatus.index}].fieldDefinitionId" value="${field.instrumentFieldDefinitionid}"/>
			<c:choose>
				<c:when test="${field.fieldType == 'STRING'}">
					<form:input path="flexibleFieldSearches[${loopStatus.index}].stringValue"/>
				</c:when>
				<c:when test="${field.fieldType == 'NUMERIC'}">
					<form:input path="flexibleFieldSearches[${loopStatus.index}].numericValue" class="numericField"/>
				</c:when>
				<c:when test="${field.fieldType == 'BOOLEAN'}">
					<spring:message code="yes"/>
					<form:radiobutton path="flexibleFieldSearches[${loopStatus.index}].booleanValue" value="true"/>
					<spring:message code="no"/>
					<form:radiobutton path="flexibleFieldSearches[${loopStatus.index}].booleanValue" value="false"/>
				</c:when>
				<c:when test="${field.fieldType == 'DATETIME'}">
					<form:input path="flexibleFieldSearches[${loopStatus.index}].dateValue" id="dateField${loopStatus.index}"/>
				</c:when>
				<c:when test="${field.fieldType == 'SELECTION'}">
					<form:select path="flexibleFieldSearches[${loopStatus.index}].selectionValueId">
					<form:option value="0" label=" "/>
					<form:options items="${field.instrumentFieldLibraryValues}" itemValue="instrumentFieldLibraryid" itemLabel="name"/>
					</form:select>
				</c:when> 
			</c:choose>
		</li>
	</c:forEach>
</ol>