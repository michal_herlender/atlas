<%@ tag language="java" display-name="showRegionalName" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ attribute name="defaultName" required="true" type="java.lang.Object" %>
<%@ attribute name="translations" required="true" type="java.lang.Object" %>
<%@ attribute name="locale" required="true" type="java.lang.Object" %>

<c:set var="localeData" value="" />
<c:forEach var="translation" items="${translations}">
	<c:if test="${translation.locale.language == locale.language}" >
		<c:set var="localeData" value="${translation.translation}" />
	</c:if>
</c:forEach>
<c:choose>
	<c:when test="${localeData != ''}">
		${localeData}
	</c:when>
	<c:otherwise>
		${defaultName}
	</c:otherwise>
</c:choose>
