<%@ tag language="java" display-name="jobnoLinkWeb" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ attribute name="job" required="true" type="java.lang.Object" %>


<a href="viewjob.htm?jobid=${job.jobid}" class="mainlink">${job.jobno}</a>
