<%@ tag language="java" display-name="showJobPo" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ attribute name="job" required="true" type="java.lang.Object" %>
<%@ attribute name="delimiter" required="true" type="java.lang.String" %>

<c:forEach var="jobPO" items="${job.POs}" varStatus="loopStatus">
	<c:if test="${!loopStatus.first}"><c:out value="${delimiter}"/></c:if>
	<c:choose>		
	
		<c:when test="${not empty jobPO  }">
			<c:out value="${jobPO.poNumber}"/>
		</c:when>
		
	</c:choose>
</c:forEach>