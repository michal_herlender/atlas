<%@ tag language="java" display-name="itemAccessory" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%@ attribute name="itemAccessory" required="true" type="java.lang.Object" %>

<tr>
	<td class="access"><cwms:besttranslation translations="${itemAccessory.accessory.translations}"/></td>
	<td class="qty">
		${itemAccessory.quantity}
	</td>
	<td class="del">
		<a class="edit" onclick="event.preventDefault(); editQuantity($j(this).parents('tr'));">
			<img src="img/icons/edit_details.png" width="16" height="16"/>
		</a>
		<a class="save hid" onclick="event.preventDefault(); saveQuantity($j(this).parents('tr'));">
			<img src="img/icons/success.png" width="16" height="16"/>
		</a>
		<a onclick="event.preventDefault(); removeItemAccessory(${itemAccessory.id}, $j(this).parents('tr'));">
			<img src="img/icons/delete.png" width="16" height="16"/>
		</a>
	</td>
	<td class="itemAccId hid">${itemAccessory.id}</td>
</tr>