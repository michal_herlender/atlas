<%@ tag language="java" display-name="printOrCreateSmallCertificateLabel" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="labelPrinter" required="true" type="java.lang.Object" %>
<%-- u is the labelwrapper, retaining same name as the JSP --%>
<%@ attribute name="u" required="true" type="java.lang.Object" %>
<%@ attribute name="useCertLinks" required="true" type="java.lang.Boolean" %>

<c:choose>
	<c:when test="${not empty labelPrinter}"> <!-- ${u.cl.linkId} or 11 for testing -->
		<c:set var="printInput" value="${useCertLinks ? u.cl.linkId : u.cl.cert.certno}" />
		<spring:message var="printSmallCalLabelText" code='jicertificates.prntsmallcallbl' />
<%--
		<a href="https://localhost:58159/service/label?type=SMALL&certificate=${u.cl.cert.certno}" ><img src="img/icons/printer.png" width="16" height="16" class="img_marg_bot" alt="${printSmallCalLabelText}" title="${printSmallCalLabelText}" /></a>
--%>
		<a href="#" onClick=" event.preventDefault(); printAlligatorLabel('SMALL', '${printInput}'); " >
			<img src="img/icons/printer.png" width="16" height="16" class="img_marg_bot" alt="${printSmallCalLabelText}" title="${printSmallCalLabelText}" />
		</a>
		
	</c:when>
	<c:otherwise>
		<spring:message var="textNoLabelPrinter" code="error.labelprinter.notfound" />
		<a href="#" onclick=" event.preventDefault(); alert('${textNoLabelPrinter}');" ><img src="img/icons/info-red.png" width="16" height="16" class="img_marg_bot" title="${textNoLabelPrinter}"></a>
	</c:otherwise>
</c:choose>
<spring:message var="createSmallCalLabelText" code='jicertificates.createsmallcallbl' />
<a href="#" onclick=" downloadCertificateLabel(this, 'birt.label_mc5is', ${u.cl.linkId}, ${u.cl.cert.certid}); return false; "><img src="img/icons/pdf.png" width="16" height="16" class="img_marg_bot" alt="${createSmallCalLabelText}" title="${createSmallCalLabelText}" /></a>
<c:set var="checkedText" value="" />
<c:if test="${u.labelSettings.labelRecallDate == 'Yes'}">
	<c:set var="checkedText" value=" checked='checked'" />
</c:if>
&nbsp;(<spring:message code="jicertificates.includerecalldates" /><input type="checkbox" name="includeRecallDate" ${checkedText} />)