<%@ tag language="java" display-name="scanItemsInOut" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="tags" tagdir="/WEB-INF/tags"%>

<%@ attribute name="defaultAddress" required="true" type="java.lang.Object" %>
<%@ attribute name="businessAddresses" required="true" type="java.lang.Object" %>

<script src='script/trescal/core/tools/ScanItemInOutPlugin.js'></script>

<!-- add warning box to display error messages if fails -->
<div class="hid warningBox1">
	<div class="warningBox2">
		<div class="warningBox3">
		</div>
	</div>
</div>
<div id="scanItemsInOut" class="infobox">
	<div class="displaycolumn">
		<fieldset>
			<legend><spring:message code="scanitemsinout.scanin"/></legend>
			<ol>
				<li>
					<label><spring:message code="scanitemsinout.confirmlocation"/></label>
					<select id="currentAddr">
						<c:forEach var="address" items="${businessAddresses}">
							<option value="${address.addrid}" ${address.addrid == defaultAddress.addrid ? "selected" : ""}>
								${address.addr1} - ${address.town}</option>
						</c:forEach>
					</select>
				</li>
				<li>
					<label><spring:message code="scanitemsinout.scanin"/></label>
					<input id="scanin" type="text" onkeypress="if(event.keyCode==13) {receive(this.value);}" />
				</li>
				<li>
					<label>
						<spring:message code="company.history"/>
						<a href="#" class="mainlink" onclick="event.preventDefault(); emptyScanIn();">&nbsp;(<spring:message code="clear"/>)</a>
					</label>
					<div id="scanInHistory" class="float-left"></div>
					<!-- clear floats and restore page flow -->
					<div class="clear"></div>
				</li>
			</ol>
		</fieldset>
	</div>
	<div class="displaycolumn">
		<fieldset>
			<legend><spring:message code="scanitemsinout.scanout"/></legend>
			<ol>
				<li>
					<label><spring:message code="scanitemsinout.scanout"/></label>
					<input type="text" id="scanout" onkeypress="if(event.keyCode==13) {despatch(this.value);}" />
				</li>
				<li>
					<label>
						<spring:message code="company.history"/>
						<a href="#" class="mainlink" onclick="event.preventDefault(); emptyScanOut();">&nbsp;(<spring:message code="clear"/>)</a>
					</label>
					<div id="scanOutHistory" class="float-left"></div>
					<!-- clear floats and restore page flow -->
					<div class="clear"></div>
				</li>
			</ol>
		</fieldset>
	</div>
	<div class="clear">&nbsp;</div>
</div>