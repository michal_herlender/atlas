<%@ tag language="java" display-name="showJobItemDeliveryNo" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ attribute name="jobDeliveries" required="true" type="java.lang.Object" %>
<%@ attribute name="jobDel" required="false" type="java.lang.Boolean" %>
<%@ attribute name="delimiter" required="true" type="java.lang.String" %>

<c:forEach var="delNo" items="${jobDeliveries}" varStatus="loopStatus">
	<c:if test="${!loopStatus.first}"><c:out value="${delimiter}"/></c:if>
	<c:if test="${not empty delNo}">
		<c:choose>
			<c:when test="${jobDel}">
				<c:out value="${delNo.deliveryno}"/>
			</c:when>
			<c:otherwise>
		    	<c:out value="${delNo.delivery.deliveryno}"/>
			</c:otherwise>
		</c:choose>
	</c:if>
</c:forEach>