<%@ tag language="java" display-name="showJobItemPosAndBPo" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ attribute name="jobitem" required="true" type="java.lang.Object" %>
<%@ attribute name="delimiter" required="true" type="java.lang.String" %>
<%@ attribute name="hidePO" required="false" type="java.lang.Boolean" %>
<%@ attribute name="hideBPO" required="false" type="java.lang.Boolean" %>

<c:forEach var="jobItemPO" items="${jobitem.jobItemPOs}" varStatus="loopStatus">
	
	<c:if test="${!loopStatus.first}"><c:out value="${delimiter}"/></c:if>
	
	<c:if test="${not empty jobItemPO.bpo && hidePO }">
		<c:out value="${jobItemPO.bpo.poNumber}"/> 
	</c:if>
		
	<c:if test="${not empty jobItemPO.po && hideBPO }">
		<c:out value="${jobItemPO.po.poNumber}"/>
	</c:if>
	
</c:forEach>