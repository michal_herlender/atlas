<%@ tag language="java" display-name="showExpenseItemPosAndBPo" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="expenseitem" required="true" type="java.lang.Object" %>
<%@ attribute name="delimiter" required="true" type="java.lang.String" %>
<%@ attribute name="hidePO" required="false" type="java.lang.Boolean" %>
<%@ attribute name="hideBPO" required="false" type="java.lang.Boolean" %>

<c:forEach var="expenseitemPO" items="${expenseitem.itemPOs}" varStatus="loopStatus">
	
	<c:if test="${!loopStatus.first}"><c:out value="${delimiter}"/></c:if>
	
	<c:if test="${not empty expenseitemPO.bpo && hidePO}">
	 	<c:out value="${expenseitemPO.bpo.poNumber}"/>
	</c:if>
	
	<c:if test ="${not empty expenseitemPO.po && hideBPO}">
		<c:out value="${expenseitemPO.po.poNumber}"/> 
	</c:if>

</c:forEach>
