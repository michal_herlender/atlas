<%@ tag language="java" display-name="printOrCreateRegularCertificateLabel" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="labelPrinter" required="true" type="java.lang.Object" %>
<%-- u is the labelwrapper, retaining same name as the JSP --%>
<%@ attribute name="u" required="true" type="java.lang.Object" %>
<%@ attribute name="useCertLinks" required="true" type="java.lang.Boolean" %>

<c:choose>
	<c:when test="${not empty labelPrinter}">  <!-- ${u.cl.cert.certid} or 87680 for testing -->
		<c:set var="printInput" value="${useCertLinks ? u.cl.linkId : u.cl.cert.certno}" />
	
		<spring:message var="printCalLabelText" code='jicertificates.prntcallbl' />
<%--
		<a href="https://localhost:58159/service/label?type=REGULAR&certificate=${u.cl.cert.certno}" ><img src="img/icons/printer.png" width="16" height="16" class="img_marg_bot" alt="${printCalLabelText}" title="${printCalLabelText}" /></a>
--%>
		<a href="#" onClick=" event.preventDefault(); printAlligatorLabel('REGULAR', '${printInput}'); " >
			<img src="img/icons/printer.png" width="16" height="16" class="img_marg_bot" alt="${printCalLabelText}" title="${printCalLabelText}" />
		</a>
	</c:when>
	<c:otherwise>
		<spring:message var="textNoLabelPrinter" code="error.labelprinter.notfound" />
		<a href="#" onclick=" event.preventDefault(); alert('${textNoLabelPrinter}');" ><img src="img/icons/info-red.png" width="16" height="16" class="img_marg_bot" title="${textNoLabelPrinter}"></a>
	</c:otherwise>
</c:choose>
<spring:message var="createCalLabelText" code='jicertificates.createcallbl' />
<a href="#" onclick=" event.preventDefault(); downloadCalSpecificLabel(this, ${u.cl.linkId}); "><img src="img/icons/pdf.png" width="16" height="16" class="img_marg_bot" alt="${createCalLabelText}" title="${createCalLabelText}" /></a>
<c:set var="checkedText" value="" />
<c:if test="${u.labelSettings.labelRecallDate == 'Yes'}">
	<c:set var="checkedText" value=" checked='checked'" />
</c:if>
&nbsp;(<spring:message code="jicertificates.includerecalldates" /><input type="checkbox" name="includeRecallDate" ${checkedText} />)
