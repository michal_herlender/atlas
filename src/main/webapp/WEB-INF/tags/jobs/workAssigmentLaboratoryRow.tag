<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@attribute name="countersRow" type="java.util.Map" required="true" %>
<%@attribute name="workDays" type="java.util.List" required="true" %>
<%@attribute name="label" type="java.lang.String" required="true" %>
<%@attribute name="labelClass" type="java.lang.String" required="true" %>
<%@attribute name="urlParameterPart" type="java.lang.String" required="true" %>
<%@attribute name="labCategorySplit" type="org.trescal.cwms.core.procedure.dto.LaboratoryCategorySplit"
             required="true" %>

<c:set var="dayWidths" value="${80 / model.workDays.size()}"/>

<c:choose>
    <c:when test="${labCategorySplit ne '' }">
        <c:set var="accreditedParam" value="&accredited=${labCategorySplit}"/>
    </c:when>
</c:choose>
<tr>
    <td class="${labelClass}">
        ${label} <c:choose>
        <c:when test="${labCategorySplit eq 'ACCREDITED' }">
            - <spring:message code="accredited"/>
        </c:when>
        <c:when test="${labCategorySplit eq 'TRACEABLE' }">
            - <spring:message code="traceable"/>
        </c:when>
    </c:choose>
    </td>
    <c:forEach var="day" items="${workDays}">
        <c:choose>
            <c:when test="${day == org.trescal.cwms.core.workflow.entity.workassignment.dto.WorkCountDay.DAY_BUSINESS}">
                <c:set var="dayParameter" value="&business=true"/>
            </c:when>
            <c:when test="${day.number == null}">
                <c:set var="dayParameter" value=""/>
            </c:when>
            <c:otherwise>
                <c:set var="dayParameter" value="&days=${day.number}&business=false"/>
            </c:otherwise>
        </c:choose>
        <td style=" width: ${dayWidths}%; " class="center">
            <a class="mainlink larger-text"
               href="viewselectedjobitems.htm?${urlParameterPart}${dayParameter}${accreditedParam}">${countersRow.get(day)}</a>
        </td>
    </c:forEach>
</tr>