<%@ tag language="java" display-name="jobItemTemplate" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="instrument" tagdir="/WEB-INF/tags/instrument"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="workflow" tagdir="/WEB-INF/tags/workflow"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%@ attribute name="subScript" fragment="true" %>
<%@ attribute name="warnings" fragment="true" %>

<c:set var="jobItem" value="${viewWrapper.jobItem}"/>
<c:set var="instrument" value="${jobItem.inst}"/>
<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<spring:message code="jitemplate.headtext" arguments="${jobItem.itemNo}, ${jobItem.job.jobno}" />
		</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<jsp:invoke fragment="subScript"/>
		<script type="text/javascript" src="script/trescal/core/tools/CalibrationRequirements.js"></script>
		<script type='module' src='script/components/cwms-translate/cwms-translate.js'></script>
		<script type='module' src='script/components/cwms-nooperation-performed/cwms-nooperation-performed.js'></script>
		<script type='module' src='script/components/cwms-upload-file/cwms-upload-file.js'></script>
		<script type="module" src="script/components/cwms-instructions/cwms-instructions.js"></script>
		<script type='module' src='script/components/search-plugins/cwms-modelcode-search/cwms-modelcode-search.js'></script>
		<script>
			var jobitems = new Array();
			var reqtypes = new Array();
			var shortreqtypes = new Array();
		</script>
		<c:forEach var="type" items="${requirementtypes}">
			<script>
				reqtypes.push('${type}');
				shortreqtypes.push('${type.shortname}');
			</script>
		</c:forEach>
		<c:forEach var="item" items="${viewWrapper.itemsOnJob}">
			<c:if test="${item.jobItemId != jobItem.jobItemId}">
				<script>
					jobitems.push({jiid: '${item.jobItemId}', jino: '${item.itemno}'});
				</script>
			</c:if>
		</c:forEach>
		<script type="text/vbscript" src='script/trescal/core/tools/VBScriptExecutor.vbs'></script>
	</jsp:attribute>
	<jsp:attribute name="globalElements">
		<c:if test="${ not empty noOperationPerformedActivityId }">
			<cwms-nooperation-performed activity-id="${ noOperationPerformedActivityId }" 
				jobitem-id="${jobItem.jobItemId}"></cwms-nooperation-performed>
		</c:if>
	     <cwms-upload-file system-component-id="${systemcomponent.componentId}" entity-id1="${instrument.plantid}" entity-id2=""
  			files-name="${filesName}" rename-to="" web="false"></cwms-upload-file>
	</jsp:attribute>
	<jsp:body>
		
		<jsp:invoke fragment="warnings"/>
		<!-- infobox is styled with nifty corners -->
		<div class="infobox" id="jiHeader">
			<!-- bit nasty but have had to put this here for calibration requirements to work nicely - please do not remove -->
			<input type="hidden" id="currentJobItemId" value="${jobItem.jobItemId}" />
			<input type="hidden" id="currentJobItemPlantId" value="${instrument.plantid}" />
			<!-- Begin job item select section -->
			<div class="text-center">
				<input type="button" value=" <spring:message code="pagination.prev"/> " id="jiPrevButton" onclick="changeItem(${viewWrapper.previousItemId});" <c:if test="${viewWrapper.previousItemId == null}">disabled</c:if>/>
				<select onchange="changeItem(this.value);">
					<c:forEach var="item" items="${viewWrapper.itemsOnJob}">
						<option value="${item.jobItemId}" ${item.jobItemId == jobItem.jobItemId ? "selected" : ""}>
							${item.itemno} - ${item.instrument.getInstrumentModelNameViaFields()}
						</option>
					</c:forEach>
				</select>
				<input type="button" value=" <spring:message code="pagination.next"/> " id="jiNextButton" onclick="changeItem(${viewWrapper.nextItemId});" <c:if test="${viewWrapper.nextItemId == null}">disabled</c:if>/>
			</div>
			<!-- End job item select section -->
			<c:choose>
				<c:when test="${jobItem.turn <= fastTrackTurn}">
					<c:set var="bgColor" value="${jobItem.calType.serviceType.displayColourFastTrack}"/>
				</c:when>
				<c:otherwise>
					<c:set var="bgColor" value="${jobItem.calType.serviceType.displayColour}"/>
				</c:otherwise>
			</c:choose>
			<fieldset class="<c:if test='${instrument.scrapped != null && instrument.scrapped}'>BER </c:if>relative" style="background-color: ${bgColor};">
				<c:if test="${instrument.scrapped != null && instrument.scrapped}">
					<div class="instScrappedOn">
						<spring:message code="vm_global.scrappedon"/>:
						<c:choose>
							<c:when test="${instrument.scrappedOn == null}">
								<spring:message code="unknown"/>
							</c:when>
							<c:otherwise>
								<fmt:formatDate value="${instrument.scrappedOn}" pattern="dd.MM.yyyy"/>
							</c:otherwise>
						</c:choose>
					</div>
					<div class="instScrappedJI">
						<spring:message code="jobquot.onjob"/>:
						<c:choose>
							<c:when test="${instrument.scrappedOnJI == null}">	
								<spring:message code="unknown"/>
							</c:when>
							<c:otherwise>
								${instrument.scrappedOnJI.job.jobno} - ${instrument.scrappedOnJI.itemNo}
							</c:otherwise>
						</c:choose>
					</div>
				</c:if>
				<legend><spring:message code="vm_global.jobitem"/><c:out value=" ${jobItem.itemNo} " /><spring:message code="viewquot.details"/></legend>
				<div class="defaultEntityImage imgbox">
					<c:choose>
						<c:when test="${jobItem.images.size() > 0}">
							<img src="displaythumbnail?id=${jobItem.images.first().id}" alt="" title="" /><br />
							<spring:message code="viewinstrument.sourcejobitem"/>
						</c:when>
						<c:when test="${jobItemImage != null}">
							<img src="displaythumbnail?id=${jobItemImage.id}" alt="" title="" /><br />
							<spring:message code="viewinstrument.sourceprvji"/>
						</c:when>
						<c:when test="${instModelImage != null}">
							<img src="displaythumbnail?id=${instModelImage.id}" alt="" title="" /><br />
							<spring:message code="viewinstrument.sourceinstmodel"/>
						</c:when>
						<c:otherwise>
							<img src="img/noimage.png" width="150" height="113" alt="<spring:message code='viewinstrument.noimage'/>" title="<spring:message code='viewinstrument.noimage'/>" class="noImage"/>
						</c:otherwise>
					</c:choose>											
					<div class="imgadd">
						<a href="#" onclick="addImageToEntity(${jobItem.jobItemId}, 'JOBITEM'); return false;" title="">
							<img src="img/icons/picture_add.png" width="16" height="16" alt="<spring:message code='viewmod.addimages'/>" title="<spring:message code='viewmod.addimages'/>"/>
						</a>
					</div>
				</div>
				<div class="quickLinkList">
					<c:choose>
						<c:when test="${instrument.scrapped != null && instrument.scrapped}">
							<a href="#" class="mainlink unscrapInstrument" onclick="callConfirmScrapInstrumentChange(false, ${instrument.plantid}, null); return false;"><spring:message code="viewinstrument.returninstrumenttoservice"/></a><br />
						</c:when>
						<c:otherwise>
							<a href="#" class="mainlink scrapInstrument" onclick="callConfirmScrapInstrumentChange(true, ${instrument.plantid}, ${jobItem.jobItemId}); return false;"><spring:message code="viewinstrument.scrapinstrument"/></a><br />
						</c:otherwise>
					</c:choose>
					<a href="" class="mainlink printJobItemLabel" onclick="event.preventDefault(); printDymoJobItemLabel(${jobItem.jobItemId}); " ><spring:message code="vm_global.printjobitemlabel"/></a><br />
					<c:if test="${allowedToChangeJobitemInstru eq true }">
						<a href="" class="mainlink changeJobItemInst" onclick="confirmNewBarcodeScan(${jobItem.jobItemId},${contact.personid}); return false;"><spring:message code="vm_global.changejobiteminstrument"/></a><br />
					</c:if>
					<a href="" class="mainlink exporttext" onclick="exportLabviewData(${jobItem.jobItemId}); return false;"><spring:message code="vm_global.exportlabviewdata"/></a>
				</div>
				<ol>
					<li>
						<label><spring:message code="jobno"/>:</label>
						<span class="largeJobNo">
							<links:jobnoLinkDWRInfo rowcount="0" jobno="${jobItem.job.jobno}" jobid="${jobItem.job.jobid}" copy="true"/>
						</span>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<span>
							<span>${jobItem.job.jobno}.${jobItem.itemNo}</span>
							<img src="img/icons/bullet_clipboard.png" width="10" height="10" onclick=" $j(this).prev().clipBoard(this, null, false, null); " alt="<spring:message code='vm_global.additemcodetoclipboard'/>" title="<spring:message code='vm_global.additemcodetoclipboard'/>"/>
						</span>
					</li>
					<li>
						<label>
							<spring:message code="vm_global.itemstatus"/>:
							<a href="img/workflow.pdf" target="_blank" class="mainlink">
								<img src="img/icons/help.png" width="16" height="16" title="view workflow pdf" class="img_marg_bot" />
							</a>
						</label>
						<span id="currentState">
							<c:choose>
								<c:when test="${jobItem.state != null}">
									<cwms:besttranslation translations="${jobItem.state.translations}"/>
								</c:when>
								<c:otherwise>
									<spring:message code="vm_global.nostatealert"/>!
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${viewWrapper.readyForClientDelivery}">
									(<a href="createdelnote.htm?destination=CLIENT&jobid=${jobItem.job.jobid}" class="mainlink"><spring:message code="vm_global.createdelnote"/></a>)
								</c:when>
								<c:when test="${viewWrapper.readyForInternalDelivery}">
									(<a href="createdelnote.htm?destination=INTERNAL&jobitemid=${jobItem.jobItemId}" class="mainlink"><spring:message code="vm_global.createdelnote"/></a>)
								</c:when>
								<c:when test="${viewWrapper.readyForInternalReturnDelivery}">
									(<a href="createdelnote.htm?destination=INTERNAL_RETURN&jobitemid=${jobItem.jobItemId}" class="mainlink"><spring:message code="vm_global.createdelnote"/></a>)
								</c:when>
								<c:when test="${viewWrapper.readyForThirdPartyDelivery}">
									(<a href="selecttpcontact.htm?jobid=${jobItem.job.jobid}" class="mainlink"><spring:message code="vm_global.createdelnote"/></a>)
								</c:when>
								<c:when test="${viewWrapper.readyForCalibration}">
									(
										<cwms:securedLink permission="CALIBRATION_NEW"  classAttr="mainlink"  parameter="?jobitemid=${jobItem.jobItemId}" collapse="True" >
											<spring:message code="vm_global.startcal"/>
										</cwms:securedLink>
									)
								</c:when>
								<c:when test="${viewWrapper.readyToResumeCalibration}">
									(<a href="resumecalibration.htm?jobitemid=${jobItem.jobItemId}" class="mainlink"><spring:message code="vm_global.resumecal"/></a>)
								</c:when>
								<c:when test="${viewWrapper.readyForGeneralServiceOperation}">
									(
										<cwms:securedLink permission="GENERALSERVICEOPERATION_NEW"  classAttr="mainlink"  parameter="?jobitemid=${jobItem.jobItemId}" collapse="True" >
											<spring:message code="vm_global.startgeneralserviceoperation"/>
										</cwms:securedLink>
									)
								</c:when>
								<c:when test="${viewWrapper.readyToResumeGeneralServiceOperation}">
									(<a href="resumegso.htm?jobitemid=${jobItem.jobItemId}" class="mainlink"><spring:message code="vm_global.resumegso"/></a>)
								</c:when>
								<c:when test="${viewWrapper.awaitingClientReceiptConfirmation && not empty jobItem.deliveryItems}">
									(<a href="viewdelnote.htm?delid=${jobItem.deliveryItems.toArray()[jobItem.deliveryItems.size() - 1].delivery.deliveryid}
									&destinationreceipt=true&loadtab=editdestinationreceiptdate-tab" class="mainlink">
										<spring:message code="vm_global.enterdestinationreceiptdate"/>
									</a>)
								</c:when>
							</c:choose>
						</span>
					</li>
					<li>
						<label><spring:message code="barcode"/>:</label>
						<span>
							<links:instrumentLinkDWRInfo displayCalTimescale="true" displayName="false" instrument="${instrument}" caltypeid="${jobItem.calType.calTypeId}" rowcount="0" displayBarcode="true"/>&nbsp;&nbsp;
							<img src="img/icons/bullet_clipboard.png" width="10" height="10" onclick="$j(this).prev().prev().clipBoard(this, null, false, null);" alt="<spring:message code='vm_global.addbarcodetoclipboard'/>" title="<spring:message code='vm_global.addbarcodetoclipboard'/>"/>&nbsp;&nbsp;
							<spring:message code="jobs"/> (${instrument.jobItems.size()})&nbsp;&nbsp; 
							
							<a href="editinstrument.htm?plantid=${instrument.plantid}" class="mainlink" target="_blank"><spring:message code="edit"/></a>
						</span>
					</li>
					<li>
						<label><spring:message code="instmodelname"/>:</label>
						<span id="jobItemInst">
							<cwms:securedLink permission="INSTRUMENT_MODEL"  classAttr="mainlink"  parameter="?modelid=${instrument.model.modelid}">
								<instrument:showExtendedModel instrument="${instrument}"/>
							</cwms:securedLink>
						</span>
					</li>
					<c:if test="${instrument.ranges != null && instrument.ranges.size() > 0}">
						<li>
							<label><spring:message code="viewinstrument.rangesize"/>:</label>
							<div class="float-left padtop">
								${instrument.ranges.toArray()[0]}
								<c:if test='${instrument.ranges.toArray()[0].uom.symbol != ""}'> 
										(<cwms:besttranslation translations="${instrument.ranges.toArray()[0].uom.nameTranslation}"/>)
								</c:if><br/>
							</div>
						</li>
					</c:if>
				</ol>		
				<!-- displays content in column 49% wide (left) -->
				<div class="displaycolumn">
					<ol id="jobitemInfo">
						<li>
							<label><spring:message code="serialno"/>:</label>
							<span>
								<span id="instSerialNo"><c:out value="${instrument.serialno}" /></span>
								<c:if test="${not empty instrument.serialno }">
									<img style="margin-left:5px" src="img/icons/bullet_clipboard.png" width="10" height="10" onclick=" $j(this).prev().clipBoard(this, null, false, null); " alt="<spring:message code='vm_global.addserialnumbertoclipboard'/>" title="<spring:message code='vm_global.addserialnumbertoclipboard'/>"/>
								</c:if>
							</span>
						</li>
						<li>
							<label><spring:message code="plantno"/>:</label>
							<span>
								<span>${instrument.plantno}</span>
								<c:if test="${not empty instrument.plantno }">
									<img src="img/icons/bullet_clipboard.png" width="10" height="10" onclick=" $j(this).prev().clipBoard(this, null, false, null); " alt="<spring:message code='vm_global.addplantnumbertoclipboard'/>" title="<spring:message code='vm_global.addplantnumbertoclipboard'/>"/>
								</c:if>
								&nbsp;
							</span>
						</li>
						<li>
							<label><spring:message code="jobresults.itempo"/>:</label>						
							<div class="float-left">
								<c:forEach var="item" items="${jobItem.jobItemPOs}" varStatus="loopStatus">
									<c:if test="${loopStatus.first}"><br/></c:if>
									<c:choose>
										<c:when test="${item.po == null}">
											<span>${item.bpo.poNumber}</span>
											<img src="img/icons/bullet_clipboard.png" width="10" height="10" onclick=" $j(this).prev().clipBoard(this, null, false, null); " alt="<spring:message code='viewjob.imgbulletclipb'/>" title="<spring:message code='viewjob.imgbulletclipb'/>"/>
										</c:when>
										<c:otherwise>
											<span>${item.po.poNumber}</span>
											<img src="img/icons/bullet_clipboard.png" width="10" height="10" onclick=" $j(this).prev().clipBoard(this, null, false, null); " alt="<spring:message code='viewjob.imgbulletclipb'/>" title="<spring:message code='viewjob.imgbulletclipb'/>"/>
										</c:otherwise>
									</c:choose>
								</c:forEach>
								&nbsp;
							</div>
							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
						</li>
						<c:if test="${jobItem.contract != null}">
							<li>
								<label><spring:message code="company.contract"/>:</label>
								<span><a href="contractview.htm?id=${jobItem.contract.id}"><c:out value="${jobItem.contract.contractNumber}" /></a><c:out value=" - ${jobItem.contract.description}" /></span>
								<br/>
								<cwms-instructions jobitem-ids="${jobItem.jobItemId}"  instruction-types="UNDEFINED,GENERAL,PACKAGING,CARRIAGE,DISCOUNT,CALIBRATION,REPAIRANDADJUSTMENT,COSTING,INVOICE,HIRE,PURCHASE,RECEIPT" show-mode="BASIC_IN_HEAD"></cwms-instructions>
							</li>
						</c:if>
						<c:if test="${not empty jobItem.clientRef}">
							<li>
								<label><spring:message code="jicontractreview.jobitemclientref"/>:</label>
								<span>${jobItem.clientRef}</span>
							</li>
						</c:if>
						<c:if test="${jobItem.group != null}">
							<li>
								<label><spring:message code="vm_global.group"/>:</label>
								<span>
									<a href="?group=${jobItem.group.id}&amp;ajax=jobitemgroup&amp;width=480" onclick="redirectToGroup(${jobItem.job.jobid});" class="jconTip mainlink" title="${jobItem.group.id} Group Information" id="jobitemgroup" tabindex="-1">
										${jobItem.group.id}
									</a>
									<links:showRolloverMouseIcon/>
								</span>
							</li>
						</c:if>
						<li>
							<label><spring:message code="caltype"/>:</label>
							<span class="bold">
								<t:showTranslationOrDefault translations="${jobItem.calType.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/>
							</span>
						</li>
						<li>
							<label><spring:message code="workinstruction"/>:</label>
							<span>
								<c:choose>
									<c:when test="${not empty jobItem.workInstruction}">
										${jobItem.workInstruction.name}
									</c:when>
									<c:otherwise>
										<c:if test="${instrument.workInstructions.size() >0}">
											${instrument.workInstructions[0].name}
										</c:if>
									</c:otherwise>
								</c:choose>
							</span>
						</li>
						<li id="workRequirementsList">
							<label class="bold"><spring:message code="vm_global.workrequirements"/>:</label>
							<div class="float-left padtop">
								<a href="#" onclick=" event.preventDefault(); addEditWorkRequirement('Add', ${jobItem.jobItemId}, null, ${instrument.model.modelid}); ">
									<img src="img/icons/add.png" width="16" height="16" alt="<spring:message code='vm_global.addnewworkrequirement'/>" title="<spring:message code='vm_global.addnewworkrequirement'/>"/>
								</a>
								<spring:message var='textcatalogsearch' javaScriptEscape="true" code='workrequirement.catalogsearch' />
									<a href="#" onclick=" event.preventDefault(); workRequirementCatalog(${jobItem.jobItemId}, '${textcatalogsearch}'); ">
										<img src="img/icons/search.png" width="16" height="16" alt="<spring:message code='search'/>" title="<spring:message code='search'/>"/>
									</a>
								</div>
								<div class="clear" style=" height: 3px; "></div>
								<div class="workReqHeadings">
									<div class="proc"><spring:message code="capability"/></div>
									<div class="servicetype"><spring:message code="abbreviation.servicetype"/></div>
									<div class="type"><spring:message code="wrtype"/></div>
									<div class="lab">Lab</div>
									<div class="actions">&nbsp;</div>
									<div class="clear"></div>
								</div>
								<c:forEach var="jiwr" items="${viewWrapper.workRequirements}">
									<div class="workRequirement" id="jiWorkReq${jiwr.id}">
										<div class="proc">
											<c:choose>
												<c:when test="${jobItem.nextWorkReq.id == jiwr.id}">
													<c:set var="reqClass" value="nextWorkReq"/>
													<spring:message var="reqTitle" code="vm_global.nextjobitemworkrequirement"/>
												</c:when>
											<c:otherwise>
												<c:set var="reqClass" value="makenextWorkReq"/>
												<spring:message var="reqTitle" code="vm_global.makenextjobitemworkrequirement"/>
											</c:otherwise>
											</c:choose>
											<a href="#" onclick="setJobItemNextWorkRequirement(${jobItem.jobItemId}, ${jiwr.id}); return false;" class="${reqClass}" title="${reqTitle}">&nbsp;</a>
											<c:choose>
												<c:when test="${not empty jiwr.workRequirement.capability}">
													<links:procedureLinkDWRInfo displayName="false" capability="${jiwr.workRequirement.capability}" rowcount="${jiwr.id}"/>
												</c:when>
												<c:otherwise>
													&nbsp;
												</c:otherwise>
											</c:choose>									
										</div>
										<div class="servicetype">
											<c:choose>
												<c:when test="${jiwr.workRequirement.serviceType == null}">
													&nbsp;
												</c:when>
											<c:otherwise>
												<cwms:besttranslation translations="${jiwr.workRequirement.serviceType.shortnameTranslation}"/>
											</c:otherwise>
										</c:choose>
									</div>
									<div class="type">
										<c:choose>
											<c:when test="${jiwr.workRequirement.type == null}">
												&nbsp;
											</c:when>
											<c:otherwise>
												${jiwr.workRequirement.type.shortname}
											</c:otherwise>
										</c:choose>
									</div>
									<div class="lab">
										<c:choose>
											<c:when test="${jiwr.workRequirement.department == null}">
												&nbsp;
											</c:when>
											<c:otherwise>
												${jiwr.workRequirement.department.shortName}
											</c:otherwise>
										</c:choose>
									</div>
									<div class="actions">
										<c:choose>
											<c:when test="${jiwr.workRequirement.status eq 'COMPLETE'}">
												<%-- <c:set var="reqClass" value="workReqCompleted"/> --%>												
												<spring:message var="reqTitle" code='vm_global.workrequirementcompleted'/>
												<a href="#" onclick="toggleCompleteWorkRequirement(${jiwr.id}); return false;" class="workReqCompleted" title="${reqTitle}">&nbsp;</a>
												&nbsp;
											</c:when>
											<c:when test="${jiwr.workRequirement.status eq 'CANCELLED'}">
													<spring:message var="reqTitle" code='vm_global.workrequirementcancelled'/>
													<a href="#" onclick="return false;" class="workReqCancelled" title="${reqTitle}">&nbsp;</a>
													&nbsp;
											</c:when>
													
											<c:otherwise>
												<spring:message var="reqTitle" code='vm_global.pendingworkrequirement'/>
												<a href="#" onclick="toggleCompleteWorkRequirement(${jiwr.id}); return false;" class="workReqNotCompleted" title="${reqTitle}">&nbsp;</a>
													&nbsp;
											</c:otherwise>
											</c:choose>
											<a href="#" class="workReqEdit" onclick=" addEditWorkRequirement('Edit', ${jobItem.jobItemId}, ${jiwr.workRequirement.id}, ${jobItem.inst.model.modelid}); return false;" title="<spring:message code='vm_global.editworkrequirement'/>">&nbsp;</a>
											<a href="#" class="workReqDelete <c:choose><c:when test='${jobItem.nextWorkReq.id == jiwr.id || jiwr.workRequirement.complete}'>hid</c:when><c:otherwise>vis</c:otherwise></c:choose>" onclick="deleteWorkRequirement(${jiwr.workRequirement.id}, ${jiwr.id}, ${jobItem.jobItemId}); return false;" title="<spring:message code='vm_global.deleteworkrequirement'/>">&nbsp;</a>
										</div>
										<c:if test="${not empty jiwr.workRequirement.requirement}">
											<span><c:out value="${jiwr.workRequirement.requirement}" /></span>
										</c:if>
										<div class="workReqSeparator"></div>
									</div>
								</c:forEach>
							</li>
						<c:set var="req" value="false"/>
						<c:forEach var="requirement" items="${jobItem.req}">
							<c:if test="${requirement != null && !requirement.deleted}">
								<c:set var="req" value="true"/>
							</c:if>
						</c:forEach>
						<c:if test="${req}">
							<li id="jobitemReqsLI">
								<label class="bold"><spring:message code="jobitem.requirements" />:</label>
								<div class="clear" style=" height: 1px; "></div>
								<div class="jobitemRequirements">
									<c:forEach var="requirement" items="${jobItem.req}">
										<c:if test="${!requirement.deleted}">
											<span id="req${requirement.id}">${requirement.label}: ${requirement.requirement}<br /></span>
										</c:if>
									</c:forEach>
								</div>
							</li>
						</c:if>
						<c:if test="${not empty jobItem.engineerAllocations}">
						<li>
							<label class="bold"><spring:message code="workflow.schedulefor"/>:</label>
							<div class="float-left">
								<c:forEach var="engineerAllocation" items="${jobItem.engineerAllocations}">
									<workflow:engineerAllocationDetails ea="${engineerAllocation}" contact="${contact}" canEdit="${canEditAllocation}"/>
								</c:forEach>
							</div>
							<div class="clear"></div>
						</li>
						</c:if>
					</ol>
				</div>
				<!-- end of left column -->
				<!-- displays content in column 49% wide (right) -->
				<div class="displaycolumn">
					<ol>
						<li>
							<label><spring:message code="vm_global.dateinturndue"/>:</label>
							<span class="bold"><fmt:formatDate value="${jobItem.dateIn}" type="date" dateStyle="SHORT"/></span>
							&nbsp;/&nbsp;
							<span class="bold"><c:out value=" ${jobItem.turn} "/><spring:message code="days"/></span>
							&nbsp;/&nbsp;
							<span class="bold"><fmt:formatDate value="${jobItem.dueDate}" type="date" dateStyle="SHORT"/></span>
							<c:choose>
								<c:when test="${ not empty jobItem.agreedDelDate }">
									(<spring:message code="jobitem.duedatesource.fromagreeddeldateofjobitem"/>)
								</c:when>
								<c:when test="${ not empty jobItem.job.agreedDelDate }">
									(<spring:message code="jobitem.duedatesource.fromagreeddeldateofjob"/>)
								</c:when>
								<c:otherwise></c:otherwise>
							</c:choose>
						</li>
						<li>
							<label><spring:message code="vm_global.currentlocation"/>:</label>
							<div>
								<c:if test="${jobItem.currentAddr != null}">
									<a class="mainlink" href="viewaddress.htm?subdivid=${jobItem.currentAddr.sub.subdivid}&addrid=${jobItem.currentAddr.addrid}">
										${jobItem.currentAddr.sub.comp.coname}<br>
										${jobItem.currentAddr.addr1}, ${jobItem.currentAddr.town} (${jobItem.currentAddr.country.countryCode})
									</a>
								</c:if>
								<c:if test="${jobItem.currentLoc != null}">
									&nbsp;-&nbsp;&nbsp;<a class="mainlink" href="location.htm?locid=${jobItem.currentLoc.locationid}">${jobItem.currentLoc.location}</a>
								</c:if>
								&nbsp;
							</div>
						</li>
						<li>
							<label><spring:message code="vm_global.calibrationlocation"/>:</label>
							<div>
								<c:if test="${jobItem.calAddr != null}">
									<a class="mainlink" href="viewaddress.htm?subdivid=${jobItem.calAddr.sub.subdivid}&addrid=${jobItem.calAddr.addrid}">
										${jobItem.calAddr.sub.comp.coname}<br>
										${jobItem.calAddr.addr1}, ${jobItem.calAddr.town} (${jobItem.calAddr.country.countryCode})
									</a>
								</c:if>
								<c:if test="${jobItem.calLoc}">
									&nbsp;-&nbsp;&nbsp;<a class="mainlink" href="location.htm?locid=${jobItem.calLoc.locationid}">${jobItem.calLoc.location}</a>
								</c:if>
								&nbsp;
							</div>
						</li>
						<li>
							<label><spring:message code="vm_global.transportout"/>:</label>
							<span>
								<c:choose>
									<c:when test="${not empty jobItem.returnOption.localizedName}">
										${jobItem.returnOption.localizedName }
									</c:when>
									<c:otherwise>
										<t:showTranslationOrDefault translations="${jobItem.returnOption.method.methodTranslation}" defaultLocale="${defaultlocale}"/>
									</c:otherwise>
								</c:choose>&nbsp;
							</span>
						</li>
						<t:showCalReqAsListItem inst="${jobItem.inst}" ji="${jobItem}" page="jiheader" calReqs="${calReqs}"/>
						<c:if test="${jobItem.onBehalf != null}">
							<li>
								<label><spring:message code="jicontractreview.onbehalf"/>:</label>
								<span>
									<links:companyLinkDWRInfo company="${jobItem.onBehalf.company}" rowcount="0" copy="true"/>
									[${jobItem.onBehalf.company.coid}-${jobItem.job.con.sub.comp.coid}]
								</span>
							</li>
							<li>
								<label><spring:message code="vm_global.onbehalfaddress"/>:</label>
								<div class="float-left padtop">
									<c:choose>
										<c:when test="${jobItem.onBehalf.address == null}">
											<spring:message code="vm_global.addressnotspecified"/>
										</c:when>
										<c:otherwise>
											<t:showFullAddress address="${jobItem.onBehalf.address}" separator="<br/>" copy="true"/>
										</c:otherwise>
									</c:choose>			
								</div>
								<!-- clear floats and restore page flow -->
								<div class="clear"></div>
							</li>
						</c:if>
					</ol>
				</div>
				<!-- end of right column -->
			</fieldset>
		</div>
		<!-- end of infobox div -->
		<!-- sub navigation menu which displays and hides sections of page as well as linking to external pages -->
		<div id="subnav">
			<dl>
				<dt><a href="jicontractreview.htm?jobitemid=${jobItem.jobItemId}" id="linkcontractrev"><spring:message code="viewjob.contrrev"/></a></dt>
				<dt><a href="jiactions.htm?jobitemid=${jobItem.jobItemId}" id="linkactivities"><spring:message code="viewquot.actions"/> (<span class="jiActionSize">${jobItem.activeActionsSize}</span>)</a></dt>
				<dt><a href="jifaultreport.htm?jobitemid=${jobItem.jobItemId}" id="linkfaultreport">
					<c:choose>
						<c:when test="${jobItem.failureReports.size() == 0}">
							<spring:message code="vm_global.faultreport"/>
						</c:when>
						<c:otherwise>
							<span class="attention"><spring:message code="vm_global.faultreport"/></span>
						</c:otherwise>
					</c:choose>
				</a></dt>
				<dt><a href="jithirdparty.htm?jobitemid=${jobItem.jobItemId}" id="linkthirdparty"><spring:message code="jicertificates.thirdparty"/></a></dt>
				<dt>
					<cwms:securedLink permission="ACCESSORY_AND_DEFECTS" collapse="True" id="linkaccessories-link" parameter="?jobid=${jobItem.job.jobid}&jobitemid=${jobItem.jobItemId}">
						<spring:message code="accessoriesanddefects.accessories"/>
					</cwms:securedLink>
				</dt>
				<dt><a href="jicertificates.htm?jobitemid=${jobItem.jobItemId}" id="linkcertificates"><spring:message code="certificates"/> (${jobItem.certLinks.size()})</a></dt>
				<dt><a href="jicalloffitem.htm?jobitemid=${jobItem.jobItemId}" id="linkcalloffitem"><spring:message code="vm_global.calloffitem"/></a></dt>
				<dt><a href="jicalibration.htm?jobitemid=${jobItem.jobItemId}" id="linkcalibration"><spring:message code="calibrations"/> (${jobItem.calLinks.size()})</a></dt>
			</dl>
		</div>
		<!-- end of sub navigation menu -->
		<!-- sub navigation menu which displays and hides sections of page as well as linking to external pages -->
		<div id="subnav2">
			<dl>
				<dt><a href="jidemands.htm?jobitemid=${jobItem.jobItemId}" id="linkdemands"><spring:message code="additionalDemand.additionalDemands"/></a></dt>
				<dt><a href="jipat.htm?jobitemid=${jobItem.jobItemId}" id="linkpat">PAT (${jobItem.pats.size()})</a></dt>
				<dt><a href="jiimages.htm?jobitemid=${jobItem.jobItemId}" id="linkimages"><spring:message code="viewinstrument.images"/> (<span class="entityImageSize">${jobItem.images.size()}</span>)</a></dt>
				<dt><a href="jiinstfiles.htm?jobitemid=${jobItem.jobItemId}" id="linkinstfiles"><spring:message code="instrumentfiles"/></a></dt>
				<dt><a href="jirepairs.htm?jobitemid=${jobItem.jobItemId}" id="linkrepairs"><spring:message code="repairs.headtext"/></a></dt>
				<dt><a href="jigeneralserviceoperations.htm?jobitemid=${jobItem.jobItemId}" id="linkgeneralserviceoperation"><spring:message code="generalserviceoperations.headtext"/> (${jobItem.gsoLinks.size()})</a></dt>
<!-- Repair information is now used in production, and should now be accessible regardless of subdivision -->
<!-- Confirming with AYB/MA/DBD whether we should have any check for location here (removed, for now) -->
<%-- 				<c:if test="${systemType == 'development' and itemAtCalibrationLocation}">   --%>
<%-- 				</c:if>   --%>
			</dl>
		</div>
		<!-- end of sub navigation menu -->
		<jsp:doBody/>
		<t:showTabbedNotes entity="${jobItem}" privateOnlyNotes="${privateOnlyNotes}" noteTypeId="${jobItem.jobItemId}" noteType="JOBITEMNOTE"/>
	</jsp:body>
</t:crocodileTemplate>