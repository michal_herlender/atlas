<%@ tag language="java" display-name="showJobPosProjection" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ attribute name="jb" required="true" type="java.lang.Object" %>

<%--
* Shows a neat view of a jobs purchase orders. If a job has a bpo this is displayed, if not the first purchase
* order and then a count of the other purchase orders is displayed.
* @param $jb the job entity
--%>

<c:choose>
	<c:when test="${not empty jb.bpoNumber}">
		${jb.bpoNumber}
		<c:set var="src" value="bpo" />
	</c:when>
	<c:when test="${not empty jb.defaultPo}">
		${jb.defaultPo.value}
		<c:set var="src" value="def" />
	</c:when>
	<c:otherwise>
		<c:forEach var="p" items="${jb.jobPos}" varStatus="statusCount">
			<c:if test="${statusCount.first}">
				${p.poNumber}
			</c:if>
		</c:forEach>
		<c:set var="src" value="nodef" />
	</c:otherwise>
</c:choose>

<c:set var="poCount" value="${jb.jobPos.size()}" />
<c:if test="${poCount > 1 && src == 'def'}">
	(+${poCount} more)
</c:if>