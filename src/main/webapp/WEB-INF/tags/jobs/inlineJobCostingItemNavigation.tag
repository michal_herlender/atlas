<%@ tag language="java" display-name="inlineJobCostingItemNavigation" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>		
	
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs"%>	

<%@ attribute name="item" required="true" type="java.lang.Object" %>			

							<div class="text-center">
						
								<c:set var="previd" value="0"/>
								<c:set var="nextid" value="0"/>
								<c:set var="upperFound" value="false"/>
								
								<c:forEach var="jitem" items="${item.jobCosting.items}">
									<c:if test="${jitem.id < item.id}">
										<c:set var="previd" value="${jitem.id}"/>
									</c:if>	
									<c:if test="${jitem.id > item.id && upperFound != true}">
										<c:set var="nextid" value="${jitem.id}"/>
										<c:set var="upperFound" value="true"/>
									</c:if>	
								</c:forEach>
								
								<c:choose>
									<c:when test="${previd > 0}">
									<input type="button" value=" <spring:message code='pagination.prev'/>" onclick=" window.location='<spring:url value="editjobcostingitem.htm?id=${previd}"/>';"/>

									</c:when>
									<c:otherwise>
									<input type="button" value=" <spring:message code='pagination.prev'/>" disabled="disabled" />
									</c:otherwise>
								</c:choose>									
									
								<select onchange=" window.location = '<spring:url value="editjobcostingitem.htm?id="/>' + this.value;" >
									<c:forEach var="jitem" items="${item.jobCosting.items}">
										<option value="${jitem.id}" <c:if test="${jitem.id == item.id}"> selected </c:if>>${jitem.jobItem.itemNo} - <cwms:showmodel instrumentmodel="${jitem.jobItem.inst.model}"/></option>
									</c:forEach>
								</select>
								
								<c:choose>
									<c:when test="${nextid > 0}">
									<input type="button" value=" <spring:message code='pagination.next'/> " onclick="  window.location='<spring:url value="editjobcostingitem.htm?id=${nextid}"/>';"/>
									</c:when>
									<c:otherwise>
									<input type="button" value=" <spring:message code='pagination.next'/> " disabled="disabled" />
									</c:otherwise>
								</c:choose>	
							
							</div>
							<!-- end of center aligned div -->