<%@ tag language="java" display-name="printCertificateLabelListItems" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs" %>

<%-- Used in two pop ups (link certificate and cert options), hence the tag to reuse --%>
<%@ attribute name="labelPrinter" required="true" type="java.lang.Object" %>
<%-- u is the labelwrapper, retaining same name as the JSP --%>
<%@ attribute name="u" required="true" type="java.lang.Object" %>
<%@ attribute name="serialno" required="true" type="java.lang.String" %>
<%@ attribute name="useCertLinks" required="true" type="java.lang.Boolean" %>

<li>
	<label class="labelhead bold"><spring:message code="viewjob.printing" /></label>
	<span>&nbsp;</span>
</li>
<c:if test="${empty serialno}">
	<li>
		<span class="attention bold"><spring:message code="jicertificates.string4" /></span>
	</li>
</c:if>
<li>
	<label><spring:message code="jicertificates.accreditedlabel"/>:</label>
	<span><spring:message code="${u.labelSettings.accredited}"/></span>
</li>
<c:if test="${u.labelSettings.accredited}">
	<li>
		<label><spring:message code="jicertificates.accreditationbody"/>:</label>
		<span>${u.labelSettings.accreditedLab.accreditationBody.shortName}</span>
	</li>
	<li>
		<label><spring:message code="jicertificates.accreditationnumber"/>:</label>
		<span>${u.labelSettings.accreditedLab.labNo}</span>
	</li>
</c:if>
<li>
	<label><spring:message code="jicertificates.labelcertificatenumber"/>:</label>
	<span><spring:message code="${u.labelSettings.labelCertificateNumber}"/></span>
</li>
<li>
	<label><spring:message code="jicertificates.labelplantnumber"/>:</label>
	<span><spring:message code="${u.labelSettings.labelPlantNumber}"/></span>
</li>
<li>
	<label><spring:message code="jicertificates.labelrecalldate"/>:</label>
	<span>${u.labelSettings.labelRecallDate}</span>
</li>
<li>
	<div class="displaycolumn">
		<label><spring:message code="jicertificates.calibrationlabel" />:</label>
		<span>
			<jobs:printOrCreateRegularCertificateLabel u="${u}" labelPrinter="${labelPrinter}" useCertLinks="${useCertLinks}" />
		</span>
		<br/><br/>
		<label><spring:message code="jicertificates.smallcallbl" />:</label>
		<span>
			<jobs:printOrCreateSmallCertificateLabel u="${u}" labelPrinter="${labelPrinter}" useCertLinks="${useCertLinks}" />
		</span>
		<br/>
		<c:if test="${u.cl.cert.links.size() > 1}">
			<br/>
			<label>&nbsp;</label>
			select: <a class="mainlink" href="#" onclick=" event.preventDefault(); $j('input:checkbox[id^=${u.cl.cert.certid}-labelCheck-]').prop('checked', true); ">
				<spring:message code="jicertificates.allitems" />
			</a>&nbsp;/&nbsp;
			<a class="mainlink" href="#" onclick=" event.preventDefault(); $j('input:checkbox[id^=${u.cl.cert.certid}-labelCheck-]').removeAttr('checked'); $j('input:checkbox[id^=${u.cl.cert.certid}-labelCheck-${u.cl.linkId}]').prop('checked', true); ">
				<spring:message code="jicertificates.justthisitem" />
			</a>
		</c:if>
	</div>
	<div class="displaycolumn">
	<c:if test="${u.cl.cert.links.size() > 1}" >
		<c:forEach var="x" items="${u.cl.cert.links}" >
			<span style=" font-size: 90%; ">
				<c:set var="xid" value="${x.cert.certid}-labelCheck-${x.linkId}" />
				<c:set var="checkedText" value="" />
				<c:if test="${u.cl.linkId == x.linkId}">
					<c:set var="checkedText" value=" checked='checked'" />
				</c:if>
				<input type="checkbox" id="${xid}" value="${x.linkId}" ${checkedText}/>
				&nbsp;&nbsp;
				${x.jobItem.itemNo}.&nbsp;&nbsp;${x.jobItem.inst.definitiveInstrument}
			</span>
			<br/>
		</c:forEach>
	</c:if>
	</div>
	<div class="clear"></div>
</li>
