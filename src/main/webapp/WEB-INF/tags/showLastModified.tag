<%@ tag language="java" display-name="lastModifiedContactLink" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ attribute name="versionedEntity" required="true" type="java.lang.Object" %>
<%@ attribute name="showRole" required="false" type="java.lang.Object" %>

<c:choose>
	<c:when test="${not empty versionedEntity.lastModifiedBy}">
		<c:out value="${versionedEntity.lastModifiedBy.name}" />
		<c:if test="${not empty showRole && showRole}">
			(<i>${versionedEntity.lastModifiedBy.sub.comp.companyRole.getMessage()}</i>)
		</c:if>
	</c:when>
	<c:otherwise>
		<spring:message code="unknown" />
	</c:otherwise>
</c:choose>
<c:if test="${not empty versionedEntity.lastModified}">
	<fmt:formatDate value="${versionedEntity.lastModified}" pattern="(dd.MM.yyyy - HH:mm:ss)"/>
</c:if>