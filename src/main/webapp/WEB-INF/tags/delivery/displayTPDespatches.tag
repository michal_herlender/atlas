<%@ tag language="java" display-name="displayTPDespatches" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%@ attribute name="sourceAddressDto" required="true" type="java.lang.Object"%>
<%@ attribute name="transportOptionDto" required="true" type="java.lang.Object"%>
<%@ attribute name="dataModel" required="true" type="java.lang.Object"%>
<%@ attribute name="stateGroups" required="true" type="java.lang.Object"%>

<table id="tableOpt-${sourceAddressDto.key}-${transportOptionDto.key}" class="default4 viewGoodsOut">
	<c:set var="itemCount"
		value="${dataModel.getItemCount(sourceAddressDto.key, transportOptionDto.key)}" />
	<c:if test="${itemCount > 0}">
			<tbody>
				<tr>
					<th colspan="5">${transportOptionDto.value}&nbsp;(<span>${itemCount}</span>)
					</th>
				</tr>
				<c:forEach var="companyDto"
					items="${dataModel.getDestCompanyDtos(sourceAddressDto.key, transportOptionDto.key)}"
					varStatus="compLoop">
					<c:forEach var="subdivDto"
						items="${dataModel.getDestSubdivDtos(sourceAddressDto.key, transportOptionDto.key, companyDto)}"
						varStatus="subLoop">
						<c:forEach var="addressDto"
							items="${dataModel.getDestAddressDtos(sourceAddressDto.key, transportOptionDto.key, companyDto, subdivDto)}"
							varStatus="addrLoop">
							<c:forEach var="jobDto"
								items="${dataModel.getJobDtos(sourceAddressDto.key, transportOptionDto.key, addressDto.key)}"
								varStatus="jobLoop">
								<c:set var="items" value="${dataModel.getDespatchItems(sourceAddressDto.key, transportOptionDto.key, addressDto.key, jobDto)}" /> 
								<c:forEach var="itemDto" 
									items="${items}" varStatus="itemLoop">
									<tr>
										<c:if
											test="${subLoop.first && addrLoop.first && jobLoop.first && itemLoop.first}">
											<td class="comp"
												rowspan="${dataModel.getItemCount(sourceAddressDto.key, transportOptionDto.key, companyDto)}">
												<c:set var="companyActive"
													value="${dataModel.isCompanyActive(companyDto.key)}" /> <c:set
													var="companyOnStop"
													value="${dataModel.isCompanyOnStop(companyDto.key)}" /> <links:companyLinkInfo
													active="${companyActive}" companyId="${companyDto.key}"
													companyName="${companyDto.value}" onStop="${companyOnStop}"
													copy="true" rowcount="${compLoop.count}" />
											</td>
										</c:if>
										<c:if test="${addrLoop.first && jobLoop.first && itemLoop.first}">
											<td class="subdiv"
												rowspan="${dataModel.getItemCount(sourceAddressDto.key, transportOptionDto.key, companyDto, subdivDto)}">
												<links:subdivLinkInfo companyActive="${companyActive}"
													companyOnStop="${companyOnStop}" subdivActive="true"
													subdivId="${subdivDto.key}" subdivName="${subdivDto.value}"
													rowcount="${subLoop.count}" />
											</td>
										</c:if>
										<c:if test="${jobLoop.first && itemLoop.first}">
											<td class="addr"
												rowspan="${dataModel.getItemCount(sourceAddressDto.key, transportOptionDto.key, addressDto.key)}">
												<c:out value="${addressDto.value}" />
											</td>
										</c:if>
										<c:if test="${itemLoop.first}">
											<td class="jobno" rowspan="${items.size()}">
												<links:jobnoLinkDWRInfo
													jobno="${itemDto.jobNumber}" jobid="${itemDto.jobId}"
													copy="true" rowcount="${jobLoop.count}" />
											</td>
										</c:if>
										<%-- Used for dynamic highlighting of rows; unexpected for states to be in multiple categories --%>
										<c:set var="category" value="" />
										
										<c:if test="${dataModel.isItemStateInTPDelivery(itemDto.itemStateId)}">
											<c:set var="category" value="tpdelivery" />
										</c:if>
										<c:if test="${dataModel.isItemStateInTPDespatch(itemDto.itemStateId)}">
											<c:set var="category" value="tpdespatch" />
										</c:if>
										<td class="items ${category} comp-${companyDto.key}">
											<div class="float-left" style="width: 56%;">
												<div style="float: left; width: 11%;">
													<links:jobitemLinkInfo defaultPage="jiactions.htm"
														jobItemId="${itemDto.jobItemId}"
														jobItemNo="${itemDto.jobItemNumber}" />
												</div>
												<div style="float: left; width: 87%;">
													<c:out value="${itemDto.instrumentModelName}" />
													<c:out value=" (${itemDto.subfamilyTypology})" />
													<c:if test="${itemDto.modelMfrType == 'MFR_GENERIC'}">
														<c:out value=" ${itemDto.brand} ${itemDto.modelName}" />
													</c:if>
													<c:if test="${itemDto.accessoriesCount >0}">
														<span class="attention">
															<c:out value="(+${itemDto.accessoriesCount} acc)" />
														</span>
													</c:if>
													
												</div>
												<div class="clear"></div>
											</div> <%-- Pending due date discussion - may need to query job for agreed overall due date --%>
											<div class="float-right text-center" style="width: 16%;">
												<fmt:formatDate value="${itemDto.dueDate}" type="date" dateStyle="SHORT"/>
											</div>
											<div class="float-right text-center" style="width: 16%;">
												<c:choose>
													<c:when test="${category == 'tpdelivery'}">
														<cwms:securedLink permission="DELIVERY_THIRD_PARTY_CREATE_FROM_JOB" classAttr="mainlink" parameter="?jobid=${itemDto.jobId}">
															<spring:message code="displaytpdespatches.deliver"/>
														</cwms:securedLink>
													</c:when>
													<c:when test="${category == 'tpdespatch'}">
														<spring:message code="displaytpdespatches.ondeliverynote" />
														<c:forEach var="di" items="${dataModel.getDeliveries(itemDto.jobItemId)}" >
																<cwms:securedLink permission="DELIVERY_NOTE_VIEW" classAttr="mainlink" parameter="?delid=${di.key}">
																	${di.value}
																</cwms:securedLink>
														</c:forEach>
													</c:when>
												</c:choose>
											</div>
											<div class="clear"></div>
										</td>
									</tr>
								</c:forEach>
							</c:forEach>
						</c:forEach>
					</c:forEach>
				</c:forEach>
			</tbody>
		</c:if>
</table>