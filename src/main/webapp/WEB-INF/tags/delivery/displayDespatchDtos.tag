<%@ tag language="java" display-name="displayDespatchDtos"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%@ attribute name="sourceAddressDto" required="true"
	type="java.lang.Object"%>
<%@ attribute name="transportOptionDto" required="true"
	type="java.lang.Object"%>
<%@ attribute name="dataModel" required="true" type="java.lang.Object"%>
<%@ attribute name="stateGroups" required="true" type="java.lang.Object"%>

<%-- Note : Formerly, showed today's daily transport option via vis/hid class, now user should select their own --%>
<c:set var="tableClass" value="hid" />
<table id="tableOpt-${sourceAddressDto.key}-${transportOptionDto.key}"
	class="default4 viewGoodsOut ${tableClass}">
	<c:set var="itemCount"
		value="${dataModel.getItemCount(sourceAddressDto.key, transportOptionDto.key)}" />
	<c:choose>
		<c:when test="${itemCount == 0}">
			<tbody>
				<tr>
					<th colspan="5">${transportOptionDto.value}&nbsp;(<span>${itemCount}</span>)
					</th>
				</tr>
				<tr>
					<td class="text-center" colspan="5">${transportOptionDto.value}
						 -&nbsp;<spring:message code="viewgoodsout.displayDespatchDtos"/>
					</td>
				</tr>
			</tbody>
		</c:when>
		<c:otherwise>
			<tbody>
				<tr>
					<th colspan="5">${transportOptionDto.value}&nbsp;(<span>${itemCount}</span>)
					</th>
				</tr>
				<c:forEach var="companyDto"
					items="${dataModel.getDestCompanyDtos(sourceAddressDto.key, transportOptionDto.key)}"
					varStatus="compLoop">
					<c:forEach var="subdivDto"
						items="${dataModel.getDestSubdivDtos(sourceAddressDto.key, transportOptionDto.key, companyDto)}"
						varStatus="subLoop">
						<c:forEach var="addressDto"
							items="${dataModel.getDestAddressDtos(sourceAddressDto.key, transportOptionDto.key, companyDto, subdivDto)}"
							varStatus="addrLoop">
							<c:forEach var="jobDto"
								items="${dataModel.getJobDtos(sourceAddressDto.key, transportOptionDto.key, addressDto.key)}"
								varStatus="jobLoop">
								<c:set var="items" value="${dataModel.getDespatchItems(sourceAddressDto.key, transportOptionDto.key, addressDto.key, jobDto)}" /> 
								<c:forEach var="itemDto" 
									items="${items}" varStatus="itemLoop">
									<tr>
										<c:if
											test="${subLoop.first && addrLoop.first && jobLoop.first && itemLoop.first}">
											<td class="comp"
												rowspan="${dataModel.getItemCount(sourceAddressDto.key, transportOptionDto.key, companyDto)}">
												<c:set var="companyActive"
													value="${dataModel.isCompanyActive(companyDto.key)}" /> <c:set
													var="companyOnStop"
													value="${dataModel.isCompanyOnStop(companyDto.key)}" /> <links:companyLinkInfo
													active="${companyActive}" companyId="${companyDto.key}"
													companyName="${companyDto.value}" onStop="${companyOnStop}"
													copy="true" rowcount="${compLoop.count}" />
											</td>
										</c:if>
										<c:if test="${addrLoop.first && jobLoop.first && itemLoop.first}">
											<td class="subdiv"
												rowspan="${dataModel.getItemCount(sourceAddressDto.key, transportOptionDto.key, companyDto, subdivDto)}">
												<links:subdivLinkInfo companyActive="${companyActive}"
													companyOnStop="${companyOnStop}" subdivActive="true"
													subdivId="${subdivDto.key}" subdivName="${subdivDto.value}"
													rowcount="${subLoop.count}" />
											</td>
										</c:if>
										<c:if test="${jobLoop.first && itemLoop.first}">
											<td class="addr"
												rowspan="${dataModel.getItemCount(sourceAddressDto.key, transportOptionDto.key, addressDto.key)}">
												<c:out value="${addressDto.value}" />
											</td>
										</c:if>
										<c:if test="${itemLoop.first}">
											<td class="jobno" rowspan="${items.size()}">
												<links:jobnoLinkDWRInfo
													jobno="${itemDto.jobNumber}" jobid="${itemDto.jobId}"
													copy="true" rowcount="${jobLoop.count}" />
											</td>
										</c:if>
										<%-- Used for dynamic highlighting of rows; unexpected for states to be in multiple categories --%>
										<c:set var="category" value="" />
										<c:if test="${dataModel.isItemStateInClientDelivery(itemDto.itemStateId)}">
											<c:set var="category" value="clientdelivery" />
										</c:if>
										<c:if test="${dataModel.isItemStateInClientDespatch(itemDto.itemStateId)}">
											<c:set var="category" value="clientdespatch" />
										</c:if>
										<c:if test="${dataModel.isItemStateInAwaitingDespatchApproval(itemDto.itemStateId)}">
											<c:set var="category" value="awaitingdespatchapproval" />
										</c:if>
										<c:set var="compName" value="${ji.currentAddr.sub.comp.coname}" />
										<td class="items ${category} comp-${companyDto.key}">
											<div class="float-left" style="width: 56%;">
												<div style="float: left; width: 11%;">
													<links:jobitemLinkInfo defaultPage="jiactions.htm"
														jobItemId="${itemDto.jobItemId}"
														jobItemNo="${itemDto.jobItemNumber}" />
												</div>
												<div style="float: left; width: 87%;">
													<%-- TODO tag to format instrument model name based on projection values? --%>
													<c:out value="${itemDto.instrumentModelName}" />
													<c:out value=" (${itemDto.subfamilyTypology})" />
													<c:if test="${itemDto.modelMfrType == 'MFR_GENERIC'}">
														<c:out value=" ${itemDto.brand} ${itemDto.modelName}" />
													</c:if>
												</div>
												<div class="clear"></div>
											</div> <%-- Pending due date discussion - may need to query job for agreed overall due date --%>
											<div class="float-right text-center" style="width: 6%;">
												<c:choose>
													<c:when test="${empty itemDto.dueDate}">
														&nbsp;
													</c:when>
													<c:otherwise>
														<c:set var="daysRemaining" value="${dataModel.compareTodayToDate(itemDto.dueDate)}" />
														<c:choose>
															<c:when test="${daysRemaining > 0}">	
																<spring:message var="altText" code="viewgoodsout.beforeduedate" arguments="${daysRemaining}" />
																<img src="img/icons/green_light.png" title="${altText}" />
															</c:when>
															<c:when test="${daysRemaining == 0}">
																<spring:message var="altText" code="viewgoodsout.onduedate" arguments="${daysRemaining}" />
																<img src="img/icons/amber_light.png" title="${altText}" />
															</c:when>
															<c:otherwise>
																<spring:message var="altText" code="viewgoodsout.pastduedate" arguments="${-1 * daysRemaining}" />
																<img src="img/icons/red_light.png" title="${altText}" />
															</c:otherwise>
														</c:choose>
													</c:otherwise>
												</c:choose>
											</div>
											<div class="float-right text-center" style="width: 16%;">
												<fmt:formatDate value="${itemDto.dueDate}" type="date" dateStyle="SHORT"/>
											</div>
											<div class="float-right text-center" style="width: 16%;">
												<c:if test="${dataModel.isItemStateInClientDelivery(itemDto.itemStateId)}">
													<div>
														<a href="createdelnote.htm?destination=CLIENT&jobid=${itemDto.jobId}" class="mainlink">
															<spring:message code="viewgoodsout.linkdeliver" />
														</a>
													</div>
												</c:if>
												<c:if test="${dataModel.isItemStateInClientDespatch(itemDto.itemStateId)}">
													<c:forEach var="diDto" items="${dataModel.getDeliveries(itemDto.jobItemId)}">
													<%-- Currently will display all deliveries, newest first, as difficult to query by type --%>
														<div>
															<a href="viewdelnote.htm?delid=${diDto.key}" class="mainlink">${diDto.value}</a>
														</div>
													</c:forEach>
												</c:if>
												<c:if test="${dataModel.isItemStateInAwaitingDespatchApproval(itemDto.itemStateId)}">
													<spring:message code="viewgoodsout.linkapproval" />
												</c:if>
											</div>
											<div class="clear"></div>
										</td>
									</tr>
								</c:forEach>
							</c:forEach>
						</c:forEach>
					</c:forEach>
				</c:forEach>
			</tbody>
		</c:otherwise>
	</c:choose>
</table>