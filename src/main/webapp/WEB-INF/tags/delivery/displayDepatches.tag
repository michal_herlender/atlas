<%@ tag language="java" display-name="displayDespatches" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%@ attribute name="optMap" required="true" type="java.lang.Object" %>
<%@ attribute name="model" required="true" type="java.lang.Object" %>
<%@ attribute name="stateGroups" required="true" type="java.lang.Object" %>
<%@ attribute name="subdivId" required="true" type="java.lang.Object" %>

<c:choose>
	<c:when test="${model.selected != null}">
		<c:set var="selectedId" value="${model.selected.id}"/>
	</c:when>
	<c:otherwise>
		<c:set var="selectedId" value="0"/>
	</c:otherwise>
</c:choose>
<c:forEach var="opt" items="${optMap.keySet()}">
	<table id="tableOpt-${subdivId}-${opt.id}" class=<c:choose><c:when test="${selectedId == opt.id}">"default4 viewGoodsOut vis"</c:when><c:otherwise>"default4 viewGoodsOut hid"</c:otherwise></c:choose>>
		<c:choose>
			<c:when test="${model.optSizes.get(opt) == 0}">
				<tbody>
					<tr>
						<th colspan="5">
							${opt}&nbsp;(<span>${model.optSizes.get(opt)}</span>)				
						</th>
					</tr>
					<tr>
						<td class="text-center" colspan="5">${opt} has no items awaiting delivery note or despatch</td>
					</tr>
				</tbody>
			</c:when>
			<c:otherwise>
				<tbody>
					<tr>
						<th colspan="5">
							${opt}&nbsp;(<span>${model.optSizes.get(opt)}</span>)
						</th>
					</tr>
					<c:set var="compMap" value="${optMap.get(opt)}"/>
					<c:forEach var="comp" items="${compMap.keySet()}" varStatus="compLoop">
						<c:set var="subMap" value="${compMap.get(comp).map}"/>
						<c:forEach var="sub" items="${subMap.keySet()}" varStatus="subLoop">
							<c:set var="addrMap" value="${subMap.get(sub).map}"/>
							<c:forEach var="addr" items="${addrMap.keySet()}" varStatus="addrLoop">
								<c:set var="jobMap" value="${addrMap.get(addr).map}"/>
								<c:forEach var="job" items="${jobMap.keySet()}" varStatus="jobLoop">
									<c:set var="items" value="${jobMap.get(job)}"/>
									<c:forEach var="ji" items="${items}" varStatus="itemLoop">
										<tr>
											<c:if test="${subLoop.first && addrLoop.first && jobLoop.first && itemLoop.first}">
												<td class="comp" rowspan="${compMap.get(comp).itemCount}">
													<links:companyLinkDWRInfo company="${comp}" rowcount="${opt.id}" copy="false"/>
												</td>
											</c:if>
											<c:if test="${addrLoop.first && jobLoop.first && itemLoop.first}">
												<td class="subdiv" rowspan="${subMap.get(sub).itemCount}">
													<links:subdivLinkDWRInfo rowcount="${opt.id}" subdiv="${sub}"/>
												</td>
											</c:if>
											<c:if test="${jobLoop.first && itemLoop.first}">
												<td class="addr" rowspan="${addrMap.get(addr).itemCount}">
													<address:showShortAddress address="${addr}"/>
													<c:if test="${model.otherJobs.get(addr.addrid) != null}">
														<a href="?addrid=${addr.addrid}&amp;ajax=addrjobs&amp;width=580" onclick="return(false);" class="jconTip mainlink" id="addr${addr.addrid}" tabindex="-1">${model.otherJobs.get(addr.addrid)} JOBS</a>
														<links:showRolloverMouseIcon/>
													</c:if>									
												</td>
											</c:if>
											<c:if test="${itemLoop.first}">
												<td class="jobno" rowspan="${items.size()}">
													<links:jobnoLinkDWRInfo rowcount="${opt.id}" jobno="${job}" jobid="${model.jobIds.get(job)}" copy="false"/>
												</td>
											</c:if>
											<c:set var="category" value=""/>
											<c:forEach var="sgl" items="${ji.state.groupLinks}">
												<c:if test="${stateGroups.contains(sgl.group)}">
													<c:set var="category" value="${sgl.group.keyName}"/>
												</c:if>
											</c:forEach>
											<c:set var="compName" value="${ji.currentAddr.sub.comp.coname}"/>
											<td class="items ${category} comp-${compName.trim().toLowerCase()}">
												<div class="float-left" style=" width: 56%; ">
													<div style=" float: left; width: 11%; ">
														<links:jobitemLinkDWRInfo jobitem="${ji}" jicount="false" rowcount="${opt.id}"/>
													</div>
													<div style=" float: left; width: 87%; ">
														${ji.inst.definitiveInstrument}
														<c:if test="${ji.accessories != null && ji.accessories.size() > 0}">
															<c:set var="accTitle" value=""/>
															<c:forEach var="itemAccessory" items="${ji.accessories}" varStatus="accLoop">
																<c:if test="${!accLoop.first}">
																	<c:set var="accTitle" value="${accTitle}, "/>
																</c:if>
																<c:set var="accTitle">${accTitle}${itemAccessory.quantity} x <cwms:besttranslation translations="${itemAccessory.accessory.translations}"/></c:set>
															</c:forEach>
															<span class="attention" title="${accTitle}">
															(+${ji.accessories.size()} acc)</span>
														</c:if>
													</div>
													<div class="clear"></div>
												</div>
												<div class="businessCompany">										
													<span title="Current Location: ${ji.currentAddr.sub.comp.coname}">${ji.currentAddr.sub.comp.coname.substring(0, 1)}</span>
												</div>
												<div class="float-right text-center" style=" width: 6%; ">
													<c:choose>
														<c:when test="${ji.effectiveAgreedDelDate == null}">
															&nbsp;
														</c:when>
														<c:otherwise>
															<c:choose>
																<c:when test="${!model.customDateTool.isToday(ji.effectiveAgreedDelDate) && datetool.getDate().after(ji.effectiveAgreedDelDate)}">
																	<c:set var="altText" value="Agreed delivery date (<fmt:formatDate pattern="dd.MM.yyyy" type="date" value="${ji.effectiveAgreedDelDate}"/> has now passed"/>
																	<img src="img/icons/red_light.png" alt="${altText}" title="${altText}" />
																</c:when>
																<c:otherwise>
																	<c:set var="altText" value="Agreed delivery date (<fmt:formatDate pattern="dd.MM.yyyy" type="date" value="${ji.effectiveAgreedDelDate}"/> has not yet passed"/>
																	<img src="img/icons/green_light.png" alt="${altText}" title="${altText}" />
																</c:otherwise>
															</c:choose>
														</c:otherwise>
													</c:choose>
												</div>
												<div class="float-right text-center" style=" width: 16%; ">
													${ji.formattedDueDate}
												</div>
												<div class="float-right text-center" style=" width: 16%; ">
													<c:choose>
														<c:when test="${category == 'clientdelivery'}">
															<a href="createdelnote.htm?destination=CLIENT&jobid=${ji.job.jobid}" class="mainlink">Deliver</a>
														</c:when>
														<c:when test="${category == 'clientdespatch'}">
															<c:forEach var="di" items="${ji.deliveryItems}">
																<c:if test="${di.delivery != null && di.delivery.type == 'CLIENT'}">
																	<a href="viewdelnote.htm?delid=${di.delivery.deliveryid}" class="mainlink">${di.delivery.deliveryno}</a>
																</c:if>
															</c:forEach>
														</c:when>
														<c:when test="${category == 'awaitingdespatchapproval'}">
															Awaiting Approval
														</c:when>
													</c:choose>
												</div>
												<div class="clear"></div>
											</td>
										</tr>
									</c:forEach>
								</c:forEach>
							</c:forEach>			
						</c:forEach>
					</c:forEach>
				</tbody>
			</c:otherwise>
		</c:choose>
	</table>
</c:forEach>