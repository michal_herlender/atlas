<%@ tag language="java" display-name="showTranslationOrDefault" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="translations" required="true" type="java.lang.Object" %>
<%@ attribute name="defaultLocale" required="true" type="java.lang.Object" %>
<%@ attribute name="specificLocale" required="false" type="java.lang.Object" %>
<%@ attribute name="looseMatching" required="false" type="java.lang.Boolean" %>

<%--
	Displays the best available translation:
	 (a) for the specific locale provided (optional parameter)  
	 (by or if not provided for the user's logged in locale (via request context)
	  
	The translation preference is for:
	 (a) an exact matching locale
	 (b) a matching locale by language (for example fr_CA if used would show fr_FR translations, if none for fr_CA)
	 (c) the system default language (expected to be en_GB)
	 
	If looseMatching (optional parameter) is provided and set to false,
	then only the exact matching locale is shown (needed for some admin features) 
	
	Updated 2021-07-22 to support multiple locales per language properly (so US English translations would appear!).
--%>

<c:set var="localeData" value=""/>
<c:set var="languageData" value=""/>
<c:set var="defaultLocaleData" value=""/>
	<c:choose>
		<c:when test="${empty specificLocale}">
			<c:set var="desiredLocale" value="${rc.locale}"/>
		</c:when>
		<c:otherwise>
			<c:set var="desiredLocale" value="${specificLocale}"/>
		</c:otherwise>
	</c:choose>
<c:forEach var="translation" items="${translations}">
	<c:if test="${translation.locale.equals(defaultLocale)}">
		<c:set var="defaultLocaleData" value="${translation.translation}"/>
	</c:if>
	<c:if test="${translation.locale.equals(desiredLocale)}">
		<c:set var="localeData" value="${translation.translation}"/>
	</c:if>
	<c:if test="${translation.locale.language.equals(desiredLocale.language)}">
		<c:set var="languageData" value="${translation.translation}"/>
	</c:if>
</c:forEach>
<c:choose>
	<c:when test='${not empty localeData}'>
		<c:out value="${localeData}" />
	</c:when>
	<c:when test='${(empty looseMatching or looseMatching) and not empty languageData}'>
		<c:out value="${languageData}" />
	</c:when>
	<c:when test='${(empty looseMatching or looseMatching) and not empty defaultLocaleData}'>
		<c:out value="${defaultLocaleData}" />
	</c:when>
</c:choose>