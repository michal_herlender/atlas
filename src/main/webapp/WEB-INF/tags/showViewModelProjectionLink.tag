<%@ tag language="java" display-name="showViewModelProjectionLink" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%@ attribute name="classValue" required="false" type="java.lang.String" %>
<%@ attribute name="target" required="false" type="java.lang.String" %>
<%@ attribute name="instrument" required="false" type="java.lang.Object" %>


<a href="<c:url value="/instrumentmodel.htm?modelid=${instrument.modelid}"/>" 
	<c:choose>
		<c:when test="${!(empty classValue)}">
			class="${classValue}"
		</c:when>
		<c:otherwise>
			class="mainlink"		 
		</c:otherwise>
	</c:choose>
	<c:if test="${!(empty target)}">
		target="${target}"
	</c:if>
	>
	<c:choose>
		<c:when test="${not empty instrument.plantid}">
			<cwms:showinstrumentprojection modelName="${instrument.instrumentModelTranslation}" subfamilyTypology="${instrument.subfamilyTypology}"
			 modelMfrType="${instrument.modelMfrType}" mfrName="${instrument.instrumentMfrName}" mfrGeneric="${instrument.instrumentMfrGeneric}"
			 instModelName="${instrument.instrumentModelName}"/>
		</c:when>
		<c:otherwise>
			<cwms:showmodelprojection modelName="${instrument.instrumentModelTranslation}" subfamilyTypology="${instrument.subfamilyTypology}"/>
		</c:otherwise>
	</c:choose>
</a>