<%@ tag language="java" display-name="paginationControl" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="resultset" required="true" type="java.lang.Object" %>
<%-- attribute can be set to true to hide the no results message. Useful to avoid the no results
message from appearing twice if the control is used twice on the page once above the results and once below--%>
<%@ attribute name="hideNoresultsMessage" required="false" type="java.lang.Boolean"%>

<div class="text-center">
	<c:choose>
		<c:when test="${resultset.resultsCount > 0 }">
		
			<c:if test="${resultset.previousPage}">
				<c:set var="prevPage" value="${resultset.currentPage - 1}"/>
			</c:if>
			
			<input type="submit" name="next" value="<spring:message code="pagination.prev"/>" class="float-left" onclick="$j('#pageNo').attr('value', ${prevPage});" <c:if test="${!resultset.previousPage}"> disabled="disabled" </c:if>/>
			
			<c:if test="${resultset.nextPage}">
				<c:set var="nextPage" value="${resultset.currentPage + 1}"/>
			</c:if>	
													
			<input type="submit" name="next" value="<spring:message code="pagination.next"/>" class="float-right" onclick="$j('#pageNo').attr('value', ${nextPage});" <c:if test="${!resultset.nextPage}"> disabled="disabled" </c:if> />
			
			<strong><spring:message code="pagination.displayingpage" /></strong>	
				
			<input type="submit" name="hiddenSelectSubmit" class="hid" onclick=" $j('#pageNo').attr('value', $j(this).next().val()); " />
					
			<select onchange=" $j(this).prev().trigger('click'); ">
			<c:set var="beginPage" value="${resultset.currentPage - 10 > 1 ? resultset.currentPage - 10 : 1}" />
			<c:set var="endPage" value="${resultset.pageCount - resultset.currentPage > 10 ? resultset.currentPage + 10 : resultset.pageCount}" />
			<c:forEach var="i" begin="${beginPage}" end="${endPage}">
				<c:choose>
					<c:when test="${i==resultset.currentPage}">
						<option value="${i}" selected="selected">${i}</option>
					</c:when>
					<c:otherwise>
						<option value="${i}">${i}</option>
					</c:otherwise>
				</c:choose>
			</c:forEach>
			</select>
					
			<strong>											
					<spring:message code="pagination.of"/><c:out value=" ${resultset.pageCount} "/><c:choose><c:when test="${resultset.pageCount > 1}"><spring:message code="pagination.pages"/></c:when><c:otherwise><spring:message code="pagination.page"/></c:otherwise></c:choose><spring:message code="pagination.withnbresults" arguments="${resultset.resultsCount}"/>
			</strong>
					
			<!-- clear floats and restore page flow -->
			<div class="clear"></div>																	
		</c:when>
		<c:otherwise>
			<c:if test="${!hideNoresultsMessage}">
				<strong>
					<spring:message code="pagination.noresults" />
				</strong>
			</c:if>
		</c:otherwise>
	</c:choose>
</div>
