<%@ tag language="java" display-name="formatPATResult" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="result" required="true" type="java.lang.Object" %>
<!-- #macro(formatPATResult $result) -->
			<c:choose>
				<c:when test="${empty result}">
					N/A
				</c:when>
				<c:otherwise>
					<c:choose>
						<c:when test="${result == true}">
							<spring:message code="jipat.pass"/>
						</c:when>
						<c:otherwise>
							<spring:message code="jipat.fail"/>
						</c:otherwise>
					</c:choose>
				</c:otherwise>
			</c:choose>
<!-- #end -->