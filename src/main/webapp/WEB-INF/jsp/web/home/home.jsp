<%-- File name: /WEB-Inf/jsp/web_/home_/home.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="quotation" tagdir="/WEB-INF/tags/quotation" %>
<%@ taglib prefix="instrument" tagdir="/WEB-INF/tags/instrument" %>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<t:webTemplate idheader="home">
  
 <jsp:attribute name="scriptPart">
        <script type='text/javascript'>
        var springUrl = "${'/web'}";
		</script>

		<script type='text/javascript' src='../script/web/home/Home.js'></script>
    </jsp:attribute>
    
    <jsp:body>
        <!-- main box body content div -->
				<div class="mainBoxBodyContent">
					
					<!-- InstanceBeginEditable name="content" -->
					
						<!-- div displaying search heading info -->
						<div>
							<!-- left floated column -->							
							<div class="columnLeft-1">
								<h3 class="text-center"><spring:message code="web.welcomeuser" arguments="${currentContact.firstName},${businessDetails.company}"/></h3>
								<div class="pad-left">
									<spring:message code="home.info" arguments="${businessDetails.company}"/>
								</div>
							</div>
							<!-- right floated column
							<div class="columnRight-1 text-right">
								<img src="../img/web/antechpremises.png" width="120" height="90" alt="" class="pad-5" />
							</div>
							clear floats and restore page flow -->
							<div class="clear"></div>
						</div>
						<!-- end of search heading info -->
						
						<!-- search form elements -->
						<fieldset>
																																							
							<!-- left floated column -->
							<div class="columnLeft-1">
								
								<ol>											
									<li>
										<label><spring:message code='home.jobs.personal'/></label>
										<span>
											<a class='mainlink' href="<spring:url value='equipmentstatus.htm?display=personal'/>">  <spring:message code="home.jobs.count" arguments="${jobStats.persItems},${jobStats.persJobs}"/></a>
										</span>
									</li>
									<c:if test="${userReadRule != 'personal'}">
										<li>
											<label><spring:message code='home.jobs.location'/></label>
											<span>
												<a class='mainlink'  href="<spring:url value='equipmentstatus.htm?display=location'/>"> <spring:message code="home.jobs.count" arguments="${jobStats.locItems},${jobStats.locJobs}"/></a>
											</span>
										</li>
									</c:if>
									<c:if test="${userReadRule == 'company' || userReadRule == 'subdiv'}">
										<li>
											<label><spring:message code='home.jobs.subdivision'/></label>
											<span>
												<a class='mainlink'  href="<spring:url value='equipmentstatus.htm?display=subdiv'/>"> <spring:message code="home.jobs.count" arguments="${jobStats.subItems},${jobStats.subJobs}"/></a>
											</span>				
										</li>
									</c:if>
									
								</ol>
							
							</div>
							<!-- end of left floated column -->
						
						</fieldset>
						<!-- end fieldset -->
						
						<!-- this section shows any items that require the contacts attention -->
						<table class="default tfirst itemsReqAction" summary="This table lists all job item action">
							<thead>
								<tr>
									<td colspan="6"><spring:message code="home.itemsrequireaction" arguments="${itemsReqActions.size()}"/></td>
								</tr>								
								<tr>
									<th class="jobno" scope="col"><spring:message code='web.jobno'/></th>
									<th class="itemno" scope="col"><spring:message code='web.barcode'/></th>
									<th class="inst" scope="col"><spring:message code='web.instrumentdesc'/></th>
									<th class="serial" scope="col"><spring:message code='web.serialno'/></th>
									<th class="plant" scope="col"><spring:message code='web.plantno'/></th>	 
									<th class="status" scope="col"><spring:message code='web.status'/></th>
								</tr>
							</thead>
						</table>
						
						<c:choose>
							<c:when test="${itemsReqActions.size() < 1}">
						
							<table class="default" summary="This table displays a message if no job items require action">
								<tbody>
									<tr>
										<td class="bold text-center"><spring:message code='home.noitemsrequireaction'/></td>
									</tr>
								</tbody>
							</table>
						
							</c:when>
							<c:otherwise>
												
							<c:forEach var="ji" items="${itemsReqActions}">
															
								<table class="default itemsReqAction" summary="This table displays job items requiring action">											
									<tbody>								
										<tr>
											<td class="jobno"><instrument:jobnoLinkWeb job="${ji.job}"/></td>
											<td class="itemno">${ji.itemNo}</td>									
											<td class="inst">
												<c:choose>
													<c:when test="${not empty ji.inst.customerDescription}">
														${ji.inst.customerDescription}
													</c:when>
													<c:otherwise>
														${ji.inst.definitiveInstrument}
													</c:otherwise>
												</c:choose>											
											</td>
											<td class="serial">${ji.inst.serialno}</td>
											<td class="plant">${ji.inst.plantno}</td>
											<td class="status">${ji.state.description}</td>
										</tr>																																																													
									</tbody>											
								</table>
								
							</c:forEach>
							
							</c:otherwise>
						</c:choose>
						<!-- end of items requiring attention section -->

						<!-- this section shows any items that are due to calibration -->
						<table class="default tfirst itemsDueToCalAction" summary="This table lists all items due for calibration">
							<thead>
								<tr>
									<td colspan="6"><spring:message code="home.itemsduetocalibration" arguments="${itemsDueToCalActions.size()}"/></td>
								</tr>								
								<tr>
									<th class="itemno" scope="col"><spring:message code='web.barcode'/></th>
									<th class="inst" scope="col"><spring:message code='web.instrumentdesc'/></th>
									<th class="serial" scope="col"><spring:message code='web.serialno'/></th>
									<th class="plant" scope="col"><spring:message code='web.plantno'/></th>	 
									<th class="status" scope="col"><spring:message code='web.status'/></th>
									<th class="lastcaldate" scope="col"><spring:message code='web.duedate'/></th>
								</tr>
							</thead>
						</table>
						
						<c:choose>
							<c:when test="${itemsDueToCalActions.size() < 1}">
							
							<table class="default" summary="This table displays a message if no item are due to calibration">
								<tbody>
									<tr>
										<td class="bold text-center"><spring:message code='home.noitemsduetocalibration'/></td>
									</tr>
								</tbody>
							</table>
						
							</c:when>
							<c:otherwise>						
							<c:forEach var="instrument" items="${itemsDueToCalActions}">
								<table class="default itemsDueToCalAction" summary="This table displays all requested collections">
									
									<tbody>	
        								<tr>
        									<td class="itemno">
        										<a href="viewinstrument.htm?plantid=${instrument.plantid}" class="
       											<c:choose>
       												<c:when test="${(instrument.scrapped == true)}">  mainlink-strike </c:when>
													<c:otherwise> mainlink </c:otherwise>
												</c:choose>">
                                            		${instrument.plantid}
                                            	</a>
        									</td>									
        									<td class="inst">
        										<c:choose>
        											<c:when test="${not empty instrument.customerDescription}">
        											<div title="${instrument.definitiveInstrument}">
														${instrument.customerDescription}
													</div>
													</c:when>
													<c:otherwise>
														${instrument.definitiveInstrument}
													</c:otherwise>
												</c:choose>	
        									<td class="serial">${instrument.serialno}</td>
        									<td class="plant">${instrument.plantno}</td>
        									<td class="status">${instrument.status.status}</td>
        									<td class="lastcaldate">${instrument.nextCalDueDate}</td>
        								</tr>	
        							</tbody>
        						</table>	
							</c:forEach>	
							</c:otherwise>
						</c:choose>
						<!-- end of items that are due to calibration section -->
						
						<!-- this section shows any quotations that the contact may have requested in the last month -->
						<table class="default tfirst viewQuotations" summary="This table displays headings for requested quotations">
							<thead>
								<tr>
									<td colspan="7"><spring:message code="quotation.requested.count" arguments="${requestedQuotations.size()}"/></td>	
								</tr>
								<tr>										
									<th class="qno" scope="col"><spring:message code='web.quoteno'/></th>
									<th class="ver" scope="col"><spring:message code='web.version'/></th>
									<th class="contact" scope="col"><spring:message code='web.contact'/></th>
									<th class="reqon" scope="col"><spring:message code='web.requestdate'/></th>
									<th class="issuedon" scope="col"><spring:message code='web.issuedate'/></th>
									<th class="items" scope="col"><spring:message code='web.items'/></th>
									<th class="status" scope="col"><spring:message code='web.status'/></th>  
								</tr>
							</thead>
						</table>
						
						
						<c:choose>
							<c:when test="${requestedQuotations.size() < 1}">
							
							<table class="default" summary="This table displays a message if no quotations have been requested">
								<tbody>
									<tr>
										<td class="bold text-center"><spring:message code='quotation.requested.empty'/></td>
									</tr>
								</tbody>
							</table>
							
							</c:when>
							<c:otherwise>
							
							<c:forEach var="q" items="${requestedQuotations}">
								<table class="default viewQuotations" summary="This table displays all requested collections">
									
									<tbody>
																							
										<tr>												
											<td class="qno">${q.qno}</td>								
											<td class="ver">${q.ver}</td>
											<td class="contact">${q.contact.name}</td>
											<td class="reqon"><fmt:formatDate value='${q.reqdate}' type="date" dateStyle="SHORT" /></td>
											<td class="issuedon"><fmt:formatDate value='${q.issuedate}' type="date" dateStyle="SHORT" /></td>
											<td class="items"><span>${q.quotationitems.size()}</span> <a href="#" id="quoteItemLink${q.id}" onclick=" loadScript.toggleItemDisplay(${q.id}, 'Quotation', this.id); return false; "><img src="../img/icons/items.png" width="16" height="16" alt="<spring:message code='quotation.viewitems'/>" title="<spring:message code='quotation.viewitems'/>" class="image_inline" /></a></td>
											<td class="status"><t:showTranslationOrDefault translations="${q.quotestatus.nametranslations}" defaultLocale="${defaultlocale}"/></td>
										</tr>
										
										<c:if test="${q.quotationitems.size() > 0}">
																					
											<tr id="child${q.id}" class="hid">
												<td colspan="7">
													
													<div style=" display: none; margin: 0 auto; ">
														
														<table class="child-table" summary="This table displays all items on the requested quotation">
															
															<thead>
																<tr>
																	<th class="barcode" scope="col"><spring:message code='web.barcode'/></th>
																	<th class="item" scope="col"><spring:message code='web.instrumentdesc'/></th>
																	<th class="qty" scope="col"><spring:message code='web.qty'/></th>
																	<th class="serial" scope="col"><spring:message code='web.serialno'/></th>
																	<th class="plant" scope="col"><spring:message code='web.plantno'/></th>												
																	<th class="caltype" scope="col"><spring:message code='web.caltype'/></th>																	
																	<th class="price" scope="col">&nbsp;</th>																
																</tr> 
															</thead>
															<tbody>																	
																<c:forEach var="item" items="${q.quotationitems}">
																																		
																	<tr>
																		<td class="barcode">
																			<c:choose>
       																			<c:when test="${not empty item.inst}">
																					${item.inst.plantid}
																				</c:when>
																				<c:otherwise>
																				-
																				</c:otherwise>
																			</c:choose>

																		</td>
																		<td class="item">
																			<c:choose>
																				<c:when test="${not empty item.inst}">
																				<quotation:showInstrument instrument="${item.inst}" /> 
																				</c:when>
																				<c:otherwise>
																				<cwms:showmodel instrumentmodel="${item.model}"/>
																				</c:otherwise>
																			</c:choose>
																		</td>
																		<td class="qty">
																			<c:choose>
																				<c:when test="${not empty item.inst}">
																				-
																				</c:when>
																				<c:otherwise>
																					${item.quantity}
																				</c:otherwise>
																			</c:choose>
																		</td>
																		<td class="serial">
																			<c:choose>
																				<c:when test="${not empty item.inst}">
																					${item.inst.serialno}
																				</c:when>
																				<c:otherwise>
																				-
																				</c:otherwise>
																			</c:choose>
																		</td>
																		<td class="plant">
																			<c:choose>
																				<c:when test="${not empty item.inst}"> 
																					${item.inst.plantno}
																				</c:when>
																				<c:otherwise>
																				-
																				</c:otherwise>
																			</c:choose>
																		</td>																																	
																		<td class="caltype"><t:showTranslationOrDefault translations="${item.caltype.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/></td>																			
																		<td class="price">&nbsp;</td>																	
																	</tr>																																
																	
																</c:forEach>
															</tbody>
															
														</table>
														
													</div>
												
												</td>
										
											</tr>
										
										</c:if>
																																
									</tbody>										
									
								</table>
							</c:forEach>
	
							</c:otherwise>
						</c:choose>
						
					
					<!-- InstanceEndEditable -->
				
			  	</div>
				<!-- end of main box body content div -->
    </jsp:body>
</t:webTemplate>