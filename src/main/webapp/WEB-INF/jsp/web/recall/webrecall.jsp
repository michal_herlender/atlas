<%-- File name: /trescal/core/web/recall/webrecall.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="instrument" tagdir="/WEB-INF/tags/instrument" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="web" tagdir="/WEB-INF/tags/web" %>

<t:webTemplate idheader="recall">
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='../script/web/recall/WebRecall.js'></script>
	</jsp:attribute>
	<jsp:body>
		<!-- main box body content div -->
		<div class="mainBoxBodyContent">
			
		<!-- InstanceBeginEditable name="content" -->
		<form:form action="" name="recallform" modelAttribute="form" id="recallform" method="post">
			
			<!-- div displaying search heading info -->
			<div>
				<!-- left floated column -->							
				<div class="columnLeft-1">
					<h3 class="text-center"><spring:message code="recall.title" /></h3>
					<div class="pad-left">
						<spring:message code="recall.info" />
						<c:choose>
							<c:when test="${userReadRule == 'COMPANY'}">
								<spring:message code="recall.info.company" />
							</c:when>
							<c:when test="${userReadRule == 'SUBDIV'}">
								<spring:message code="recall.info.subdivision" />
							</c:when>
							<c:when test="${userReadRule == 'ADDRESS'}">
								<spring:message code="recall.info.location" />
							</c:when>
							<c:otherwise>
								<spring:message code="recall.info.personal" />
							</c:otherwise>
						</c:choose>
						<br /><strong><spring:message code="recall.info.complement" /></strong>
					</div>
				</div>
				<!-- right floated column -->
				<div class="columnRight-1 text-right">
					<%-- <img src="../img/web/instrument.png" width="140" height="67" alt="" class="pad-5" /> --%>
				</div>
				<!-- clear floats and restore page flow -->
				<div class="clear"></div>
			</div>
			<!-- end of search heading info -->
		
			<!--  hidden input used to indicate if this is the first submission of the form -->
			<!--  blank means this is the first submission -->
										
			<div class="recall_timeline_container">
			
				<div class="recall_timeline_pad">&nbsp;</div>
				
				<div class="timeline_head_subdiv"><spring:message code="web.subdivision" /></div>
				<div class="timeline_head_addr"><spring:message code="web.address" /></div>
				<div class="timeline_head_contact"><spring:message code="web.contact" /></div>					
				
				<div class="clear"></div>
				
				<div class="recall_timeline_pad">&nbsp;</div>							
				<div class="timeline_select_subdiv">
					<c:set var="selectedSubdiv" value=""/>
						<c:choose>
							<c:when test="${userReadRule == 'COMPANY'}">
								<c:choose>
									<c:when test="${subdivs.size() > 1}">
										<form:select id="subdivid" path="subdivid" onchange=" getActiveSubdivAddrsAndConts(this.value, $j(this).parent().next().find('select'), $j(this).parent().next().next().find('select'), true, 0); return false; ">
											<c:choose>
												<c:when test="${form.subdivid == 0}">
													<c:set var="selectedText" value="selected='selected'"/>
												</c:when>
												<c:otherwise>
													<c:set var="selectedText" value=""/>
												</c:otherwise>
											</c:choose>
											<option value="0" ${selectedText}><spring:message code="web.search.allsubdivisions" /></option>
											<c:forEach var="sub" items="${subdivs}">
												<c:choose>
													<c:when test="${form.subdivid == sub.subdivid}">
														<c:set var="selectedText" value="selected='selected'"/>
														<c:set var="selectedSubdiv" value="${sub.subname}"/>
													</c:when>
													<c:otherwise>
														<c:set var="selectedText" value=""/>
													</c:otherwise>
												</c:choose>
												<option value="${sub.subdivid}" ${selectedText}>${sub.subname}</option>
											</c:forEach>
										</form:select>
									</c:when>
									<c:otherwise>
										<c:set var="selectedSubdiv" value="${userContact.sub.subname}"/>
										<div class="padtop"><instrument:subdivFilter subdiv="${userContact.sub}" /></div>
										<form:input type="hidden" path="subdivid" value="${userContact.sub.subdivid}" />
									</c:otherwise>
								</c:choose>
							</c:when>
							<c:otherwise>
								<c:set var="selectedSubdiv" value="${userContact.sub.subname}"/>
								<div class="padtop"><instrument:subdivFilter subdiv="${userContact.sub}" /></div>
								<form:input type="hidden" path="subdivid" value="${userContact.sub.subdivid}" />
							</c:otherwise>
						</c:choose>
					<form:errors  path="subdivid"><span class="error"><img src="../img/web/asterisk.png" width="16" height="16" /></span></form:errors>
				</div>
				<div class="timeline_select_addr">
					<c:set var="selectedAddress" value=""/>
						<c:choose>
							<c:when test="${userReadRule == 'COMPANY' || userReadRule == 'SUBDIV' || userReadRule == 'CONTACT'}">
								<form:select path="addrid" id="addrid">
									<c:choose>
										<c:when test="${form.addrid == 0}">
											<c:set var="selectedText" value="selected='selected'"/>
										</c:when>
										<c:otherwise>
											<c:set var="selectedText" value=""/>
										</c:otherwise>
									</c:choose>
									<option value="0" ${selectedText}><spring:message code="web.search.alladdresses" /></option>
									<c:forEach var="a" items="${addresses}">
										<c:if test="${not empty a.addr1}">
											<c:choose>
												<c:when test="${form.addrid == a.addrid}">
													<c:set var="selectedText" value="selected='selected'"/>
													<c:set var="selectedAddress" value="${a.addr1}"/>
												</c:when>
												<c:otherwise>
													<c:set var="selectedText" value=""/>
												</c:otherwise>
											</c:choose>
											<option value="${a.addrid}" ${selectedText}>${a.addr1}</option>
										</c:if>
									</c:forEach>
								</form:select>
							</c:when>
							<c:otherwise>
								<c:set var="selectedAddress" value="${userContact.defAddress.addr1}"/>
								<div class="padtop">${userContact.defAddress.addr1}, ${userContact.defAddress.addr2}</div>
								<form:input type="hidden" path="addrid" value="${userContact.defAddress.addrid}" />
							</c:otherwise>
						</c:choose>
					<form:errors  path="addrid"><span class="error"><img src="../img/web/asterisk.png" width="16" height="16" /></span></form:errors>
				</div>
				<div class="timeline_select_contact">
					<c:set var="selectedContact" value=""/>
						<c:choose>
							<c:when test="${userReadRule == 'COMPANY' || userReadRule == 'SUBDIV' || userReadRule == 'ADDRESS'}">
								<form:select id="personid" path="personid">
									<c:choose>
										<c:when test="${form.personid == 0}">
											<c:set var="selectedText" value="selected='selected'"/>
										</c:when>
										<c:otherwise>
											<c:set var="selectedText" value=""/>
										</c:otherwise>
									</c:choose>
									<option value="0" ${selectedText}><spring:message code="web.search.allcontacts" /></option>
									<c:forEach var="c" items="${contacts}">
										<c:choose>
											<c:when test="${form.personid == c.personid}">
												<c:set var="selectedText" value="selected='selected'"/>
												<c:set var="selectedContact" value="${c.name}"/>
											</c:when>
											<c:otherwise>
												<c:set var="selectedText" value=""/>
											</c:otherwise>
										</c:choose>
										<option value="${c.personid}" ${selectedText}>${c.reversedName}</option>
									</c:forEach>
								</form:select>
							</c:when>
							<c:otherwise>
								<c:set var="selectedContact" value="${userContact.name}"/>
								<div class="padtop">${userContact.name}</div>
								<form:input type="hidden" path="personid" value="${userContact.personid}" />
							</c:otherwise>
						</c:choose>
					<form:errors  path="addrid"><span class="error"><img src="../img/web/asterisk.png" width="16" height="16" /></span></form:errors>
				</div>

				<div class="clear">&nbsp;</div>
                <!-- Hidden fields used by js functions that handle clicks see WebRecall.js -->
				<form:hidden path="startDate" />
				<form:hidden path="finishDate" />
				
				<div class="recall_timeline_pad">&nbsp;</div>
				<c:forEach var="rdto" items="${recallPeriods}" varStatus="loopStatus">
					<c:choose>
						<c:when test="${rdto.selected}">
							<c:set var="timelineClass" value="recall_timeline_selected"/>
						</c:when>
						<c:otherwise>
							<c:set var="timelineClass" value="recall_timeline"/>
						</c:otherwise>
					</c:choose>
					
					<div class="float-left ${timelineClass}">
						<c:choose>
							<c:when test="${not empty rdto.startDate}">
								<fmt:formatDate var="startDate" value="${rdto.startDate}" type="date" dateStyle="SHORT" />
							</c:when>
							<c:otherwise>
								<c:set var="startDate" value=""/>
							</c:otherwise>
						</c:choose>
						<c:choose>
							<c:when test="${not empty rdto.finishDate}">
								<fmt:formatDate var="finishDate" value="${rdto.finishDate}" type="date" dateStyle="SHORT" />
							</c:when>
							<c:otherwise>
								<c:set var="finishDate" value=""/>
							</c:otherwise>
						</c:choose>
						<a href="#" class="mainlink timeline link1" onClick="event.preventDefault(); submitForm('${startDate}','${finishDate}');">
							<c:choose>
								<c:when test="${not empty rdto.startDate && not empty rdto.finishDate}">
									<fmt:formatDate value="${rdto.startDate}" type="date" dateStyle="SHORT" />
									<c:if test="${rdto.selected}">
										<fmt:formatDate var="recallDateText" value="${rdto.startDate}" type="date" dateStyle="SHORT" />
									</c:if>
								</c:when>
								<c:when test="${not empty rdto.startDate}">
									<spring:message var="fromText" code="system.from" />
									<fmt:formatDate var="startDateText" value="${rdto.startDate}" type="date" dateStyle="SHORT" />
									${fromText}<br/>${startDateText}
									<c:if test="${rdto.selected}">
										<c:set var="recallDateText" value="${fromText} ${startDateText}"/>
									</c:if>
								</c:when>
								<c:when test="${not empty rdto.finishDate}">
									<spring:message var="toText" code="system.to" />
									<fmt:formatDate var="finishDateText" value="${rdto.finishDate}" type="date" dateStyle="SHORT" />
									${toText}<br/>${finishDateText}
									<c:if test="${rdto.selected}">
										<c:set var="recallDateText" value="${toText} ${finishDateText}"/>
									</c:if>
								</c:when>
								<c:otherwise>
									<spring:message var="anyText" code="system.any" />
									${anyText}
									<c:if test="${rdto.selected}">
										<c:set var="recallDateText" value="${anyText}"/> 
									</c:if>
								</c:otherwise>
							</c:choose>
						</a>
					</div>	
				</c:forEach>
				<div class="clear"></div>
				<div class="recall_timeline_pad">&nbsp;</div>
				<c:forEach var="rdto" items="${recallPeriods}" varStatus="loopStatus">
					<!-- TODO: timeline_history, timeline_current, timeline_future usage? -->
					<c:choose>
						<c:when test="${rdto.past}">
							<c:set var="timelineClass" value="timeline_history"/>
						</c:when>
						<c:when test="${rdto.current}">
							<c:set var="timelineClass" value="timeline_current"/>
						</c:when>
						<c:when test="${rdto.future}">
							<c:set var="timelineClass" value="timeline_future"/>
						</c:when>
					</c:choose>
					<div class="float-left ${timelineClass}">&nbsp;</div>
				</c:forEach>
				<div class="clear"></div>
			</div>
				
			<div class="text-center">							
				<h3>
					<c:if test="${form.subdivid != 0}">
						${selectedSubdiv}<br/>
					</c:if>
					<c:if test="${form.addrid != 0}">
						${selectedAddress}<br/>
					</c:if>
					<c:if test="${form.personid != 0}">
						${selectedContact}<br/>
					</c:if>
					<spring:message code="instrument.recall.recalldate" /> : ${recallDateText}		
				</h3>						
			</div>						

			<table class="default recallResults" summary="This table lists all instrument search headings">
				<thead>
					<tr>
						<c:choose>
							<c:when test="${empty insts}">
								<c:set var="nbInstr" value="0"/>
							</c:when>
							<c:otherwise>
								<c:set var="nbInstr" value="${insts.size()}"/>
							</c:otherwise>
						</c:choose>
						<td colspan="7"><spring:message code="recall.search.count" arguments="${nbInstr}" /></td>
					</tr>
					<tr>
						<th class="barcode" scope="col"><spring:message code="web.barcode" /></th>  
						<th class="inst" scope="col"><spring:message code="web.instrumentdesc" /></th>
						<th class="serialno" scope="col"><spring:message code="web.serialno" /></th>
						<th class="plantno" scope="col"><spring:message code="web.plantno" /></th>
						<th class="owner" scope="col"><spring:message code="web.owner" /></th>
						<th class="addr" scope="col"><spring:message code="web.address" /></th>
						<th class="nextcaldate" scope="col"><spring:message code="web.nextcaldate" /></th>
					</tr>
				</thead>
			</table>
			
			<c:choose>
				<c:when test="${empty insts}">
					<table class="default">
						<tbody>
							<tr>
								<td class="bold text-center">
									<spring:message code="web.noresults" />
								</td>
							</tr>
						</tbody>
					</table>
				</c:when>
				<c:otherwise>
					<c:forEach var="instrument" items="${insts}">
						<table class="default recallResults">
							<tbody>
								<tr>										
									<td class="barcode">${instrument.plantid}</td>
									<td class="inst"><web:instrumentLinkWeb displayCalTimescale="true" instrument="${instrument}" /></td>								
									<td class="serialno">${instrument.serialno}</td>
									<td class="plantno">${instrument.plantno}</td>
									<td class="owner">${instrument.con.name}</td>												
									<td class="addr">
										${instrument.add.addr1}
									</td>
									<td class="nextcaldate">
										<c:if test="${not empty instrument.nextCalDueDate}">
											<fmt:formatDate value="${instrument.nextCalDueDate}" type="date" dateStyle="SHORT" />
										</c:if>
									</td>
								</tr>
							</tbody>
						</table>
					</c:forEach>
				</c:otherwise>
			</c:choose>
			
			<table class="default tlast">
				<tfoot>
					<tr>
						<td>&nbsp;</td>
					</tr>
				</tfoot>
			</table>					
			
		</form:form>				
		
		<!-- InstanceEndEditable -->
		
			</div>
		
		<!-- end of main box body content div -->

	</jsp:body>
</t:webTemplate>