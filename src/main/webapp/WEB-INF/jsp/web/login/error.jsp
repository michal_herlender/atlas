<%-- File name: /trescal/core/web/login/error.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:webTemplate idheader="">
	<jsp:body>
		<div class="warningBox1">
			<div class="warningBox2">
				<div class="warningBox3">
					<div class="attention">${error}</div>
					<h5 class="center-0 attention"><spring:message code="error"/>:</h5>
					<div class="center attention">
						<c:choose>
							<c:when test="${not empty error.localizedMessage}">
								<c:out value="${error.localizedMessage}"/>
							</c:when>
							<c:otherwise>
								<spring:message code="genericerrormessage"/>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
		</div>
		
		<c:if test="${(sessionScope.systemType eq 'development') or (sessionScope.systemType eq 'debug')}">
			<div style="text-align: center">
				<button onclick="$j('#stacktrace').removeClass('hid'); $j(this).addClass('hid');">Show stack trace</button>
			</div>
			<div class="warningBox1 hid" id="stacktrace">
				<div class="warningBox2">
					<div class="warningBox3">
						<c:forEach var="stackTraceElement" items="${error.stackTrace}">
							<div class="attention">${stackTraceElement}</div>
						</c:forEach>
					</div>
				</div>
			</div>
		</c:if>
	</jsp:body>
</t:webTemplate>