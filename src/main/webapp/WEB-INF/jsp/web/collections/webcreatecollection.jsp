./ <%-- File name: /trescal/core/web/collections/webcreatecollection.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="web" tagdir="/WEB-INF/tags/web" %>
<%@ taglib prefix="instrument" tagdir="/WEB-INF/tags/instrument" %>

<t:webTemplate idheader="collection">
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='../script/web/collection/WebCreateCollection.js'></script>
	</jsp:attribute>
	<jsp:body>
        <div class="mainBoxBodyContent">
    		<!-- error section displayed when form submission fails -->						
    		<div class="warningBox1 hid">
    			<div class="warningBox2">
    				<div class="warningBox3">
    					<!-- anchor here used for redirect only -->
    					<a href="../" id="errorMessage"></a>
    					<h5 class="center-0 attention"><spring:message code="collection.error" /></h5>
    					<div class="text-center attention"></div>
    				</div>
    			</div>
    		</div>						
    		<!-- end error section -->

       		<c:if test="${showSuccessMessage != null && showSuccessMessage}">
       			<!-- this success view is displayed when collection has been requested -->
       			<div class="successBox1">
       				<div class="successBox2">
       					<div class="successBox3">
       						<div class="text-center"><spring:message code="collection.successmessage" /></div>
       					</div>
       				</div>
       			</div>
       			<!-- end of the success view -->
       		</c:if>
        		
       		<div>
       			<div class="columnLeft-1">
       				<h3 class="text-center"><spring:message code="collection.title" /></h3>
       				<div class="pad-left; pad-top">
       					<spring:message code="collection.info" />
       				</div>
       				<div class="pad-left; pad-top">
       					<c:choose>
       						<c:when test="${userReadRule == 'company' || userReadRule == 'subdiv'}">
       							<spring:message code="collection.info.companyorsubdivision" />
       						</c:when>
       						<c:when test="${userReadRule == 'location'}">
       							<spring:message code="collection.info.location" />
       						</c:when>
       						<c:otherwise>
       							<spring:message code="collection.info.personal" />
       						</c:otherwise>
       					</c:choose>
       				</div>
       				<div class="pad-left; pad-top">
       					<spring:message code="collection.info.complement" />
       				</div>
       			</div>
       			<div class="columnRight-1 text-right">
       				<img src="../img/web/van.png" width="140" height="90" alt="" class="pad-5" />
       			</div>
       			<div class="clear"></div>
       		</div>
       			
       		<div class="pad-top">
       			<!-- sub navigation which changes between viewing requested collections and requesting a new collection -->						
       			<div id="subnav">
       				<dl>
       					<dt>
       						<a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'viewcollection-tab', false); " id="viewcollection-link" class="selected">
       							<spring:message code="collection.navig.requested" arguments="${requestedCollections.size()}" />
       						</a>
       					</dt>
       					<c:if test="${userUpdateRule != 'NONE'}">
       						<dt>
       							<a href="#" onclick="switchMenuFocus(menuElements, 'requestcollection-tab', false); return false;" id="requestcollection-link">
       								<spring:message code="collection.navig.newrequest" />
       							</a>
       						</dt>
       					</c:if>
       				</dl>
       			</div>
       		</div>
       		
       		<div id="viewcollection-tab">
       			<table class="default tfirst viewCollections" summary="This table displays headings for requested collections">
       				<thead>
       					<tr>
       						<td colspan="6"><spring:message code="collection.requested.count" arguments="${requestedCollections.size()}" /></td>
       					</tr>
       					<tr>										
       						<th class="subdiv" scope="col"><spring:message code="web.subdivision" /></th>
       						<th class="contact" scope="col"><spring:message code="web.contact" /></th>
       						<th class="address" scope="col"><spring:message code="web.address" /></th>
       						<th class="reqdate" scope="col"><spring:message code="web.collectiondate" /></th>
       						<th class="items" scope="col"><spring:message code="web.items" /></th>
       						<th class="status" scope="col"><spring:message code="web.status" /></th>  
       					</tr>
       				</thead>
       			</table>
				<c:choose>
					<c:when test="${requestedCollections.size() < 1}">
	       				<table class="default" summary="This table displays a message if no collections have been requested">
	       					<tbody>
								<tr>
	      							<td class="bold text-center" colspan="6">
	      								<spring:message code="collection.requested.empty" />
	      							</td>
	      						</tr>
	       					</tbody>
	       				</table>
					</c:when>
					<c:otherwise>
	       				<c:forEach var="col" items="${requestedCollections}">
	       					<table class="default viewCollections" summary="This table displays all requested collections">
	       						<tbody>
	       							<tr>												
	       								<td class="subdiv">
	       									<instrument:subdivFilter subdiv="${col.contact.sub}" />
	       								</td>								
	       								<td class="contact">${col.contact.name}</td>
	       								<td class="address">${col.address.addr1}, ${col.address.town}, ${col.address.county}, ${col.address.postcode}</td>
	       								<td class="reqdate"><fmt:formatDate value="${col.scheduleDate}" type="date" dateStyle="SHORT" /></td>
	       								<td class="items"><span>${col.scheduleequipment.size()}</span> <a href="#" id="schedItemLink${col.scheduleId}" onclick=" loadScript.toggleItemDisplay(${col.scheduleId}, 'Collection', this.id); return false; "><img src="../img/icons/items.png" width="16" height="16" alt="<spring:message code="collection.viewitems" />" title="<spring:message code="collection.viewitems" />" class="image_inline" /></a></td>
	       								<td class="status">${col.status.status}</td>
	       							</tr>
	       							
	       							<c:if test="${col.scheduleequipment.size() > 0}">
	       								<tr id="child${col.scheduleId}" class="hid">
	       									<td colspan="6">
	       										<div style=" display: none; width: 100%; ">
	       											<table class="child-table" summary="This table displays all items on the requested collection">
	       												<thead>
	       													<tr>
	       														<th class="item" scope="col"><spring:message code="web.mfr" /></th>
	       														<th class="serial" scope="col"><spring:message code="web.serialno" /></th>
	       														<th class="plant" scope="col"><spring:message code="web.plantno" /></th>
	       														<th class="qty" scope="col"><spring:message code="web.qty" /></th>
	       														<th class="itempo" scope="col"><spring:message code="web.itempo" /></th>
	       														<th class="caltype" scope="col"><spring:message code="web.caltype" /></th>
	       														<th class="turn" scope="col"><spring:message code="web.turn" /></th>
	       														<th class="fault" scope="col"><spring:message code="web.faulty" /></th>
	       														<th class="supported" scope="col"><spring:message code="web.supported" /></th>																
	       													</tr> 
	       												</thead>
	       												<tbody>																	
	       													<c:forEach var="equip" items="${col.scheduleequipment}">
	       														<tr>
	       															<c:choose>
	       																<c:when test="${equip.equipInst != null}">
		       																<td class="item">
		       																	<c:choose>
	       																			<c:when test="${equip.equipInst.mfr}">
		       																			${equip.equipInst.mfr.name} -
		       																		</c:when>
		       																		<c:otherwise>
		       																			${equip.equipInst.model.mfr.name} -
		       																		</c:otherwise>
		       																	</c:choose>
		       																	${equip.equipInst.model.model} -
		       																	${equip.equipInst.model.description.description}
		       																</td>
		       																<td class="serial">${equip.equipInst.serialno}</td>
		       																<td class="plant">${equip.equipInst.plantno}</td>
		       																<td class="qty">1</td>
	       																</c:when>
	       																<c:otherwise>
		       																<td class="item">
		       																	${equip.equipModel.mfr.name} -
		       																	${equip.equipModel.model} -
		       																	${equip.equipModel.description.description}
		       																</td>
		       																<td class="serial">-</td>
		       																<td class="plant">-</td>
		       																<td class="qty">${equip.qty}</td>
	       																</c:otherwise>
	       															</c:choose>
	       															<td class="itempo">${equip.ponumber}</td>
	       															<td class="caltype"><t:showTranslationOrDefault translations="${equip.caltype.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}" /></td>
	       															<td class="turn">${equip.turn.turn}</td>
	       															<td class="fault">
	       																<c:choose>
	       																	<c:when test="${equip.faulty}">
	       																		<img src="../img/web/tick.png" width="10" height="10" alt="<spring:message code="web.faulty" />" title="<spring:message code="web.faulty" />" />
	       																	</c:when>
	       																	<c:otherwise>
	       																		<img src="../img/web/cross.png" width="10" height="10" alt="<spring:message code="web.notfaulty" />" title="<spring:message code="web.notfaulty" />" />
	       																	</c:otherwise>
	       																</c:choose>
	       															</td>
	       															<td class="supported">
	       																<c:choose>
	       																	<c:when test="${equip.supported}">
	       																		<img src="../img/web/tick.png" width="10" height="10" alt="<spring:message code="web.supported" />" title="<spring:message code="web.supported" />" />
	       																	</c:when>
	       																	<c:otherwise>
	       																		<img src="../img/web/cross.png" width="10" height="10" alt="<spring:message code="web.notsupported" />" title="<spring:message code="web.notsupported" />" />
	       																	</c:otherwise>
	       																</c:choose>
	       															</td>																	
	       														</tr>
	       														
	       														<c:if test="${equip.faulty}">
	       															<tr>
	       																<td colspan="9">
	       																	<span class="bold"><spring:message code="web.faultydescription.label" />&nbsp;</span>
	       																	<c:choose>
	       																		<c:when test="${equip.faultDesc.length() > 0}">
	       																			${equip.faultDesc}
	       																		</c:when>
	       																		<c:otherwise>
	       																			<spring:message code="web.nofaultydescription" />
	       																		</c:otherwise>
	       																	</c:choose>
	       																</td>
	       															</tr>
	       														</c:if>
	       														
	       														<c:if test="${equip.commentDesc != null && equip.commentDesc != ''}">
	       															<tr>
	       																<td colspan="9">
	       																	<span class="bold"><spring:message code="web.itemcomment.label" /></span>
	       																	<c:choose>
	       																		<c:when test="${equip.commentDesc.length() > 0}">
	       																			${equip.commentDesc}
	       																		</c:when>
	       																		<c:otherwise>
	       																			<spring:message code="web.noitemcomment" />
	       																		</c:otherwise>
	       																	</c:choose>
	       																</td>
	       															</tr>
	       														</c:if>
	       													</c:forEach>
	       												</tbody>
	       											</table>
	       										</div>
	       									</td>
	       								</tr>
	       							</c:if>
	       						</tbody>										
	       					</table>
	       				</c:forEach>
       				</c:otherwise>
       			</c:choose>
       			
       			<table class="default tlast" summary="">
       				<tfoot>
       					<tr>
       						<td>&nbsp;</td>
       					</tr>
       				</tfoot>
       			</table>
       		</div>
       		
       		<div id="requestcollection-tab" class="hid">					
       			<!-- request collection form elements -->
       			<fieldset>
       				<form:form method="post" action="" name="collectionform" modelAttribute="form">
       					<ol>
       						<li>
       							<label><spring:message code="web.collectionperformedby.label" /></label>
       							<span>${businessSubdiv.getComp().getConame()} - ${businessSubdiv.getSubname()}</span>
       							<input type="hidden" id="businessSubdivId" value="${businessSubdiv.getSubdivid()}" />
       						</li>
       						<li>
       							<label><spring:message code="web.requestdate.label" /></label>
       							<span><fmt:formatDate value="${datetoday}" type="date" dateStyle="SHORT" /></span>
       						</li>
       						<li>
       							<label><spring:message code="web.name.label" /></label>
       							<span>${currentContact.name}</span>
       						</li>
       						<li>
       							<label><spring:message code="web.company.label" /></label>
       							<span>${currentContact.sub.comp.coname}</span>
       						</li>
       						<li>
       							<label><spring:message code="web.subdivision.label" /></label>
       							<span><instrument:subdivFilter subdiv="${currentContact.sub}" /></span>
       						</li>
       						<li>
       							<label for="${status.expression}"><spring:message code="web.collectionaddress.label" /></label>
       							<form:select path="addrId" onChange="setCollectionType();">
       								<form:option value="">--&nbsp;<spring:message code="web.selectaddress"/>&nbsp;--</form:option>
       								<c:forEach var="a" items="${addresses}">
       									<form:option value="${a.addrid}">
											${a.addr1}, ${a.addr2}, ${a.town}, ${a.county}, ${a.postcode}
										</form:option>
       								</c:forEach>
       							</form:select>
       						</li>
       						<li>
       							<label><spring:message code="web.collectiontype.label" /></label>
       							<span id="collmethod"></span>
       						</li>
       						<li>
       							<label><spring:message code="web.collectionday.label" /></label>
       							<span id="collday"></span>
       						</li>
       						<li>
       							<label><spring:message code="web.collectiondate.label" /></label>
       							<form:select path="rrDate">
       								<form:option value=""><spring:message code="web.empty" /></form:option>
       							</form:select>
       							<form:errors path="rrDate" class="attention"/>
       						</li>
       						<li>
       							<label><spring:message code="web.specifydate.label" /></label>
       							<form:input path="customDate" value="${form.customDate}" readonly="true" />
       							<form:errors path="customDate" class="attention"/>
       						</li>
       					</ol>								
       					
       					<div class="webSearchPlugin">						
       						<!-- Type of plugin required, either 'quotation' or 'collection' -->
       						<input type="hidden" name="pluginType" value="Collection" />
       						<!-- Turnaround options from enum -->
       						<c:set var="to" value="" />
       						<c:forEach var="t" items="${turnOpts}" varStatus="count">
       							<c:if test="${!count.first}">
       								<c:set var="to" value="${to}," />
       							</c:if>
       							<c:set var="to" value="${to} (${t}-${t.turn}${t.turnDays})" />
       						</c:forEach>
       						<input type="hidden" name="turnOpts" value="${to}" />
       						<!-- Company coid used to search instruments -->
       						<input type="hidden" name="pluginCoid" value="${currentContact.sub.comp.coid}" />
       						<!-- Business subdiv used for cal types -->
       						<input type="hidden" name="pluginBusinessSubdivid" value="${businessSubdiv.subdivid}" />
       						<c:if test="${form.rdetail}">
       							<!-- Populate from recall document? -->
       							<input type="hidden" name="populateFromRecall" value="${form.rdetail.recall.id},${form.rdetail.recallType},${form.rdetail.contact.personid},${form.rdetail.address.addrid}" />
       						</c:if>
       						<!-- Name of system company -->
       						<input type="hidden" name="pluginSysComp" value="${businessCompany}" />
       						<!-- Telephone number of system company -->
       						<input type="hidden" name="pluginSysTel" value="${businessTel}" />
       					</div>
       					
       					<ol>
       						<li>
       							<label><spring:message code="web.calibrationpono.label" /></label>
       							<form:input path="poNumberCal" value="${form.poNumberCal}" />
       						</li>
       						<li>
       							<label><spring:message code="web.repairpono.label" /></label>
       							<form:input path="poNumberRep" value="${form.poNumberCal}" />
       						</li>
       						<li>
       							<label><spring:message code="web.collectionfurtherinfo.label" /></label>
       							<form:textarea path="furtherInformation" value="${form.furtherInformation}" cols="60" rows="4"></form:textarea>
       						</li>
       					</ol>	
       					
       					<div class="text-center pad-top">
       						<input type="button" name="submitCollection" value="<spring:message code="collection.newrequest.submit" />" onclick=" checkBasketItemsSize('collection'); " />
       					</div>			
       				 </form:form>
       			</fieldset>
       		</div>
        </div>
	</jsp:body>
</t:webTemplate>