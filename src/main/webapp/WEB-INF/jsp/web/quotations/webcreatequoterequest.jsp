<%-- File name: /trescal/core/web/quotations/webcreatequoterequest.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="web" tagdir="/WEB-INF/tags/web" %>

<t:webTemplate idheader="quote">
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='../script/web/quotations/WebCreateQuoteRequest.js'></script>
	</jsp:attribute>
	<jsp:body>
		<div class="mainBoxBodyContent">
			<div class="warningBox1 hid">
				<div class="warningBox2">
					<div class="warningBox3">
						<a href="../" id="errorMessage"></a>
						<h5 class="center-0 attention"><spring:message code="quotation.error" /></h5>
						<div class="text-center attention"></div>
					</div>
				</div>
			</div>						

			<c:if test="${command.message != null && command.message != ''}">
				<div class="successBox1">
					<div class="successBox2">
						<div class="successBox3">
							<div class="text-center">${command.message}</div>
						</div>
					</div>
				</div>
			</c:if>
														
			<div>
				<div class="columnLeft-1">
					<h3 class="text-center"><spring:message code="quotation.title" /></h3>
					<div class="pad-left"><spring:message code="quotation.info" arguments="${businessDetails.company}" /></div>
				</div>
				<!-- <div class="columnRight-1 text-right">
					<img src="../img/web/instrument.png" width="140" height="67" alt="" class="pad-5" />
				</div> -->
				<div class="clear"></div>
			</div>
			
			<div class="pad-top">					
				<div id="subnav">
					<dl>
						<%-- <c:if test="${userUpdateRule = 'yes'}"> --%>
							<dt><a href="#" onclick="switchMenuFocus(menuElements, 'requestquotation-tab', false); return false;" id="requestquotation-link"><spring:message code="quotation.navig.newrequest" /></a></dt>
						<%-- </c:if> --%>
						<dt><a href="#" onclick="switchMenuFocus(menuElements, 'viewquotation-tab', false); return false;" id="viewquotation-link" class="selected"><spring:message code="quotation.navig.requested" arguments="${requestedQuotations.size()}" /></a></dt>								
						<dt><a href="#" onclick="switchMenuFocus(menuElements, 'issuedquotation-tab', false); return false;" id="issuedquotation-link"><spring:message code="quotation.navig.issued" arguments="${issuedQuotations.size()}" /></a></dt>
						<dt><a href="#" onclick="switchMenuFocus(menuElements, 'acceptedquotation-tab', false); return false;" id="acceptedquotation-link"><spring:message code="quotation.navig.accepted" arguments="${acceptedQuotations.size()}" /></a></dt>
					</dl>
				</div>
			</div>
			
			<div id="viewquotation-tab">
				<table class="default tfirst viewQuotations" summary="This table displays headings for requested quotations">
					<thead>
						<tr>
							<td colspan="7"><spring:message code="quotation.requested.count" arguments="${requestedQuotations.size()}" /></td>
						</tr>
						<tr>										
							<th class="qno" scope="col"><spring:message code="web.quoteno" /></th>
							<th class="ver" scope="col"><spring:message code="web.version" /></th>
							<th class="contact" scope="col"><spring:message code="web.contact" /></th>
							<th class="reqon" scope="col"><spring:message code="web.requestdate" /></th>
							<th class="issuedon" scope="col"><spring:message code="web.issuedate" /></th>
							<th class="items" scope="col"><spring:message code="web.items" /></th>
							<th class="status" scope="col"><spring:message code="web.status" /></th>  
						</tr>
					</thead>
				</table>
				
				<c:choose>
					<c:when test="${requestedQuotations.size() < 1}">
						<table class="default" summary="This table displays a message if no quotations have been requested">
							<tbody>
								<tr>
									<td class="bold text-center">
										<spring:message code="quotation.requested.empty" />
									</td>
								</tr>
							</tbody>
						</table>
					</c:when>
					<c:otherwise>		
						<c:forEach var="q" items="${requestedQuotations}">
								<table class="default viewQuotations" summary="This table displays all requested collections">
									<tbody>
										<tr>												
											<td class="qno">${q.qno}</td>								
											<td class="ver">${q.ver}</td>
											<td class="contact">${q.contact.name}</td>
											<td class="reqon"><fmt:formatDate value="${q.reqdate}" type="date" dateStyle="SHORT"/></td>
											<td class="issuedon"><fmt:formatDate value="${q.issuedate}" type="date" dateStyle="SHORT"/></td>
											<td class="items">
												<span>${q.quotationitems.size()}</span> 
												<a href="#"	id="quoteItemLink${q.id}" onclick=" event.preventDefault(); loadScript.toggleItemDisplay(${q.id}, 'Quotation', this.id); ">
													<img src="../img/icons/items.png" width="16" height="16" alt="<spring:message code="quotation.viewitems" />" title="<spring:message code="quotation.viewitems" />" class="image_inline" />
												</a>
											</td>
											<td class="status"><t:showTranslationOrDefault translations="${q.quotestatus.nametranslations}" defaultLocale="${defaultlocale}" /></td>
										</tr>
										
										<c:if test="${q.quotationitems.size() > 0}">
											<tr id="child${q.id}" class="hid">
												<td colspan="7">
													<div style=" display: none; margin: 0 auto; ">
														<table class="child-table" summary="This table displays all items on the requested quotation">
															<thead>
																<tr>
																	<th class="barcode" scope="col"><spring:message code="web.barcode" /></th>
																	<th class="item" scope="col"><spring:message code="web.instrumentdesc" /></th>
																	<th class="qty" scope="col"><spring:message code="web.qty" /></th>
																	<th class="serial" scope="col"><spring:message code="web.serialno" /></th>
																	<th class="plant" scope="col"><spring:message code="web.plantno" /></th>												
																	<th class="caltype" scope="col"><spring:message code="web.caltype" /></th>																	
																	<th class="price" scope="col">&nbsp;</th>																
																</tr> 
															</thead>
															<tbody>																	
																<c:forEach var="item" items="${q.quotationitems}">
																	<tr>
																		<td class="barcode">
																			<c:choose>
																				<c:when test="${item.inst == null}">
																					-
																				</c:when>
																				<c:otherwise>
																					${item.inst.plantid}
																				</c:otherwise>
																			</c:choose>
																		</td>
																		<td class="item">
																			<c:choose>
																				<c:when test="${item.inst == null}">
																					<cwms:showmodel instrumentmodel="${item.model}"/>
																				</c:when>
																				<c:otherwise>
																					<c:choose>
																						<c:when test="${empty item.inst.customerDescription}">
																							<cwms:showmodel instrumentmodel="${item.inst.model}"/>
																						</c:when>
																						<c:otherwise>
																							<div title="${item.inst.definitiveInstrument}">
																								${item.inst.customerDescription}
																							</div>
																						</c:otherwise>
																					</c:choose>
																				</c:otherwise>
																			</c:choose>
																		</td>
																		<td class="qty">
																			<c:choose>
																				<c:when test="${item.inst == null}">
																					${item.quantity}
																				</c:when>
																				<c:otherwise>
																					-
																				</c:otherwise>
																			</c:choose>
																		</td>
																		<td class="serial">
																			<c:choose>
																				<c:when test="${item.inst == null}">
																					-
																				</c:when>
																				<c:otherwise>
																					${item.inst.serialno}
																				</c:otherwise>
																			</c:choose>
																		</td>
																		<td class="plant">
																			<c:choose>
																				<c:when test="${item.inst == null}">
																					-
																				</c:when>
																				<c:otherwise>
																					${item.inst.plantno}
																				</c:otherwise>
																			</c:choose>
																		</td>																																	
																		<td class="caltype"><t:showTranslationOrDefault translations="${item.caltype.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}" /></td>
																		<td class="price">&nbsp;</td>																	
																	</tr>																																
																</c:forEach>
															</tbody>
														</table>
													</div>
												</td>
											</tr>
										</c:if>
									</tbody>										
								</table>
						</c:forEach>
					</c:otherwise>
				</c:choose>
				<table class="default tlast" summary="">
					<tfoot>
						<tr>
							<td>&nbsp;</td>
						</tr>
					</tfoot>
				</table>
			</div>

			<div id="issuedquotation-tab" class="hid">
				<table class="default tfirst viewQuotations" summary="This table displays headings for requested quotations">
					<thead>
						<tr>
							<td colspan="7"><spring:message code="quotation.issued.count" arguments="${issuedQuotations.size()}" /></td>
						</tr>
						<tr>										
							<th class="qno" scope="col"><spring:message code="web.quoteno" /></th>
							<th class="ver" scope="col"><spring:message code="web.version" /></th>
							<th class="contact" scope="col"><spring:message code="web.contact" /></th>
							<th class="reqon" scope="col"><spring:message code="web.requestdate" /></th>
							<th class="issuedon" scope="col"><spring:message code="web.issuedate" /></th>
							<th class="items" scope="col"><spring:message code="web.items" /></th>
							<th class="status" scope="col"><spring:message code="web.status" /></th>  										
						</tr>
					</thead>
				</table>							
				
				<c:choose>
					<c:when test="${issuedQuotations.size() < 1}">
						<table class="default" summary="This table displays a message if no quotations have been requested">
							<tbody>
								<tr>
									<td class="bold text-center">
										<spring:message code="quotation.issued.empty" />
									</td>
								</tr>
							</tbody>
						</table>
					</c:when>
					<c:otherwise>
						<c:forEach var="q" items="${issuedQuotations}">
							<c:if test="${q.issued && !q.accepted}">
								<table class="default viewQuotations" summary="This table displays all requested collections">
									<tbody>
										<tr>												
											<td class="qno">${q.qno}</td>								
											<td class="ver">${q.ver}</td>
											<td class="contact">${q.contact.name}</td>
											<td class="reqon"><fmt:formatDate value="${q.reqdate}" type="date" dateStyle="SHORT"/></td>
											<td class="issuedon"><fmt:formatDate value="${q.issuedate}" type="date" dateStyle="SHORT"/></td>
											<td class="items"><span>${q.quotationitems.size()}</span> <a href="#" id="quoteItemLink${q.id}" onclick=" event.preventDefault(); loadScript.toggleItemDisplay(${q.id}, 'Quotation', this.id); "><img src="../img/icons/items.png" width="16" height="16" alt="<spring:message code="quotation.viewitems" />" title="<spring:message code="quotation.viewitems" />" class="image_inline" /></a></td>
											<td class="status"><t:showTranslationOrDefault translations="${q.quotestatus.nametranslations}" defaultLocale="${defaultlocale}" /></td>
										</tr>
										
										<c:if test="${q.quotationitems.size() > 0}">
											<tr id="child${q.id}" class="hid">
												<td colspan="7">
													<div style=" display: none; margin: 0 auto; ">
														<table class="child-table" summary="This table displays all items on the requested quotation">
															<thead>
																<tr>
																	<th class="barcode" scope="col"><spring:message code="web.barcode" /></th>
																	<th class="item" scope="col"><spring:message code="web.instrumentdesc.label" /></th>
																	<th class="qty" scope="col"><spring:message code="web.qty" /></th>
																	<th class="serial" scope="col"><spring:message code="web.serialno" /></th>
																	<th class="plant" scope="col"><spring:message code="web.plantno" /></th>												
																	<th class="caltype" scope="col"><spring:message code="web.caltype" /></th>																	
																	<th class="price" scope="col">&nbsp;</th>	
																</tr> 
															</thead>
															<tbody>																	
																<c:forEach var="item" items="${q.quotationitems}">
																	<tr>
																		<td class="barcode">
																			<c:choose>
																				<c:when test="${item.inst == null}">
																					-
																				</c:when>
																				<c:otherwise>
																					${item.inst.plantid}
																				</c:otherwise>
																			</c:choose>
																		</td>
																		<td class="item">
																			<c:choose>
																				<c:when test="${item.inst == null}">
																					<cwms:showmodel instrumentmodel="${item.model}"/>
																				</c:when>
																				<c:otherwise>
																					${item.inst.definitiveInstrument}
																				</c:otherwise>
																			</c:choose>
																		</td>
																		<td class="qty">
																			<c:choose>
																				<c:when test="${item.inst == null}">
																					${item.quantity}
																				</c:when>
																				<c:otherwise>
																					-
																				</c:otherwise>
																			</c:choose>
																		</td>
																		<td class="serial">
																			<c:choose>
																				<c:when test="${item.inst == null}">
																					-
																				</c:when>
																				<c:otherwise>
																					${item.inst.serialno}
																				</c:otherwise>
																			</c:choose>
																		</td>
																		<td class="plant">
																			<c:choose>
																				<c:when test="${item.inst == null}">
																					-
																				</c:when>
																				<c:otherwise>
																					${item.inst.plantno}
																				</c:otherwise>
																			</c:choose>
																		</td>																																	
																		<td class="caltype"><t:showTranslationOrDefault translations="${item.caltype.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}" /></td>																				
																		<td class="price">&nbsp;</td>																	
																	</tr>																																
																</c:forEach>
															</tbody>
														</table>
													</div>
												</td>
											</tr>
										</c:if>
									</tbody>										
								</table>
							</c:if>
						</c:forEach>
					</c:otherwise>
				</c:choose>
				<table class="default tlast" summary="">
					<tfoot>
						<tr>
							<td>&nbsp;</td>
						</tr>
					</tfoot>
				</table>
				
			</div>

			<div id="acceptedquotation-tab" class="hid">
				<table class="default tfirst viewQuotations" summary="This table displays headings for requested quotations">
					<thead>
						<tr>
							<td colspan="7"><spring:message code="quotation.accepted.count" arguments="${acceptedQuotations.size()}" /></td>
						</tr>
						<tr>										
							<th class="qno" scope="col"><spring:message code="web.quoteno" /></th>
							<th class="ver" scope="col"><spring:message code="web.version" /></th>
							<th class="contact" scope="col"><spring:message code="web.contact" /></th>
							<th class="reqon" scope="col"><spring:message code="web.requestdate" /></th>
							<th class="issuedon" scope="col"><spring:message code="web.issuedate" /></th>
							<th class="items" scope="col"><spring:message code="web.items" /></th>
							<th class="status" scope="col"><spring:message code="web.status" /></th>  	
						</tr>
					</thead>
				</table>							
				
				<c:choose>
					<c:when test="${acceptedQuotations.size() < 1}">
						<table class="default" summary="This table displays a message if no quotations have been requested">
							<tbody>
								<tr>
									<td class="bold text-center">
										<spring:message code="quotation.accepted.empty" />
									</td>
								</tr>
							</tbody>
						</table>
					</c:when>					
					<c:otherwise>
						<c:forEach var="q" items="${acceptedQuotations}">
							<c:if test="${q.issued && q.accepted}">
								<table class="default viewQuotations" summary="This table displays all requested collections">
									<tbody>
										<tr>
											<td class="qno">${q.qno}</td>												
											<td class="ver">${q.ver}</td>
											<td class="contact">${q.contact.name}</td>
											<td class="reqon"><fmt:formatDate value="${q.reqdate}" type="date" dateStyle="SHORT"/></td>
											<td class="issuedon"><fmt:formatDate value="${q.issuedate}" type="date" dateStyle="SHORT"/></td>
											<td class="items"><span>${q.quotationitems.size()}</span> <a href="#" id="quoteItemLink${q.id}" onclick=" event.preventDefault(); loadScript.toggleItemDisplay(${q.id}, 'Quotation', this.id); "><img src="../img/icons/items.png" width="16" height="16" alt="<spring:message code="quotation.viewitems" />" title="<spring:message code="quotation.viewitems" />" class="image_inline" /></a></td>
											<td class="status"><t:showTranslationOrDefault translations="${q.quotestatus.nametranslations}" defaultLocale="${defaultlocale}" /></td>
										</tr>
										<c:if test="${q.quotationitems.size() > 0}">
											<tr id="child${q.id}" class="hid">
												<td colspan="7">
													<div style=" display: none; margin: 0 auto; ">
														<table class="child-table" summary="This table displays all items on the requested quotation">
															<thead>
																<tr>
																	<th class="barcode" scope="col"><spring:message code="web.barcode" /></th>
																	<th class="item" scope="col"><spring:message code="web.instrumentdesc.label" /></th>
																	<th class="qty" scope="col"><spring:message code="web.qty" /></th>
																	<th class="serial" scope="col"><spring:message code="web.serialno" /></th>
																	<th class="plant" scope="col"><spring:message code="web.plantno" /></th>												
																	<th class="caltype" scope="col"><spring:message code="web.caltype" /></th>																	
																	<th class="price" scope="col"><spring:message code="web.finalquost" /></th>	
																</tr> 
															</thead>
															<tbody>																	
																<c:forEach var="item" items="${q.quotationitems}">
																	<tr>
																		<td class="barcode">
																			<c:choose>
																				<c:when test="${item.inst == null}">
																					-
																				</c:when>
																				<c:otherwise>
																					${item.inst.plantid}
																				</c:otherwise>
																			</c:choose>
																		</td>
																		<td class="item">
																			<c:choose>
																				<c:when test="${item.inst == null}">
																					<cwms:showmodel instrumentmodel="${item.model}"/>
																				</c:when>
																				<c:otherwise>
																					${item.inst.definitiveInstrument}
																				</c:otherwise>
																			</c:choose>
																		</td>
																		<td class="qty">
																			<c:choose>
																				<c:when test="${item.inst == null}">
																					${item.quantity}
																				</c:when>
																				<c:otherwise>
																					-
																				</c:otherwise>
																			</c:choose>
																		</td>
																		<td class="serial">
																			<c:choose>
																				<c:when test="${item.inst == null}">
																					-
																				</c:when>
																				<c:otherwise>
																					${item.inst.serialno}
																				</c:otherwise>
																			</c:choose>
																		</td>
																		<td class="plant">
																			<c:choose>
																				<c:when test="${item.inst == null}">
																					-
																				</c:when>
																				<c:otherwise>
																					${item.inst.plantno}
																				</c:otherwise>
																			</c:choose>
																		</td>																																	
																		<td class="caltype"><t:showTranslationOrDefault translations="${item.caltype.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}" /></td>																				
																		<td class="price"><fmt:formatNumber value="${item.finalCost}" type="currency" currencySymbol="${q.currency.currencyERSymbol}"/></td>																	
																	</tr>																																
																</c:forEach>
																<tr>
																	<td colspan="5">&nbsp;</td>
																	<td class="caltypetotal"><spring:message code="web.total.label" /></td>																			
																	<td class="pricetotal"><fmt:formatNumber value="${q.finalCost}" type="currency" currencySymbol="${q.currency.currencyERSymbol}"/></td>																	
																</tr>
															</tbody>
														</table>
													</div>
												</td>
											</tr>
										</c:if>																						
									</tbody>
								</table>
							</c:if>
						</c:forEach>
					</c:otherwise>
				</c:choose>				
				<table class="default tlast" summary="">
					<tfoot>
						<tr>
							<td>&nbsp;</td>
						</tr>
					</tfoot>
				</table>
			</div>
			
			<div id="requestquotation-tab" class="hid">					
				<fieldset>
					<form:form method="post" action="" name="quotationform" >
						<div class="webSearchPlugin">						
							<!-- Type of plugin required, either 'quotation' or 'collection' -->
							<input type="hidden" name="pluginType" value="Quotation" />
							<!-- Company coid used to search instruments -->
							<input type="hidden" name="pluginCoid" value="${userContact.sub.comp.coid}" />
							<c:if test="${command.rdetail}">
								<!-- Populate from recall document? -->
								<input type="hidden" name="populateFromRecall" value="${command.rdetail.recall.id},${command.rdetail.recallType},${command.rdetail.contact.personid},${command.rdetail.address.addrid}" />
							</c:if>
							<!-- Name of system company -->
							<input type="hidden" name="pluginSysComp" value="${businessCompany}" />
							<!-- Telephone number of system company -->
							<input type="hidden" name="pluginSysTel" value="${businessTel}" />
							<!-- Business subdiv used for cal types -->
							<input type="hidden" name="pluginBusinessSubdivid" value="${businessSubdiv.subdivid}" />
							<!-- Locale Info -->
							<input type="hidden" name="pluginLanguage" value="${pageContext.response.locale.language}"/>
							<input type="hidden" name="pluginCountry" value="${pageContext.response.locale.country}"/>
						</div>
						<ol>
							<li>&nbsp;</li>
							<li>											
								<label for="poNumber"><spring:message code="web.purchaseorderno.label" /></label>
								<input type="text" id="poNumber" name="poNumber" value="" />
							</li>										
						</ol>	
						<div class="text-center pad-top">
							<input type="button" name="submitQuotation" value="<spring:message code="quotation.newrequest.submit" />" onclick=" checkBasketItemsSize('quotation'); " />
						</div>			
					 </form:form>
				</fieldset>
			</div>
	  	</div>
	</jsp:body>
</t:webTemplate>
