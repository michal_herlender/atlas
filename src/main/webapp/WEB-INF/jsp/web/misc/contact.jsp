<%-- File name: /trescal/core/web/misc/contact.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:webTemplate idheader="">
	<jsp:body>
		<div class="mainBoxBodyContent">
			<div>
				<!-- left floated column -->							
				<div class="columnLeft-1">
					<h3 class="text-center"><spring:message code="contact.title" arguments="${businessDetails.company}" /></h3>
					<div class="pad-left">
						<spring:message code="contact.info" arguments="${businessDetails.company}" />
					</div>
				</div>
				<!-- right floated column -->
				<div class="columnRight-1 text-right">
					<img src="../img/web/contactus.png" width="120" height="90" alt="" class="pad-5" />
				</div>
				<!-- clear floats and restore page flow -->
				<div class="clear"></div>
			</div>
			
			<fieldset>
				<!-- left floated column -->
				<div class="columnLeft-2">
					<ol>
						<li>
							<label><spring:message code="web.address.label" /></label>
							<div class="float-left padtop" style=" height: 110px; ">
								<strong>${businessDetails.company}</strong><br />
								<c:forEach var="a" items="${businessDetails.address}">
									${a}<br />
								</c:forEach>
							</div>
							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
						</li>
					</ol>
				</div>

				<div class="columnRight-2">
				</div>
				<div class="clear"></div>	
				
				<ol>
					<li>
						<label><spring:message code="web.telephone.label" /></label>
						<span>${businessDetails.tel}</span>
					</li>
					<li>
						<label><spring:message code="web.facsimile.label" /></label>
						<span>${businessDetails.fax}</span>
					</li>
					<li>
						<label><spring:message code="web.website.label" /></label>
						<span>
							<a href="http://${businessDetails.homepage}" class="mainlink">${businessDetails.homepage}</a>
						</span>
					</li>
					<li>
						<label><spring:message code="web.customerservices.label" /></label>
						<span>
							<a href="mailto:${businessDetails.salesEmail}" class="mainlink email" title="<spring:message code="web.emailcustomerservicesteam" />"><spring:message code="web.emailcustomerservicesteam" /></a>
						</span>
					</li>
					<li>
						<label><spring:message code="web.webteam.label" /></label>
						<span>
							<a href="mailto:${businessDetails.webEmail}" class="mainlink email" title="<spring:message code="web.emailwebteam" />"><spring:message code="web.emailwebteam" /></a>
						</span>
					</li>
				</ol>
			</fieldset>											
	  	</div>
	</jsp:body>
</t:webTemplate>
