<%-- File name: /web/misc/c.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:webTemplate idheader="">
    <jsp:body>
	<div class="mainBoxBodyContent">
	
	<!-- InstanceBeginEditable name="content" -->
	
		<c:if test="${command.feedbackLeft ==  true}">
			<div class="successBox1">
				<div class="successBox2">
					<div class="successBox3">
						<div class="text-center"><spring:message code='addfeedback.success' /></div>
						<div class="text-center padtop"><a href="${url}" class="mainlink"><spring:message code='addfeedback.goback' /></a></div>
					</div>
				</div>
			</div>
		    </c:if>
		
		<!-- div displaying search heading info -->
		<div>
			<!-- left floated column -->							
			<div class="columnLeft-1">
				<h3 class="text-center"><spring:message code='addfeedback.title' /></h3>
				<div class="pad-left">
					<spring:message code='addfeedback.info' arguments="${businessDetails.company}" />
				</div>
			</div>
			<!-- right floated column -->
			<div class="columnRight-1 text-right">
			</div>
			<!-- clear floats and restore page flow -->
			<div class="clear"></div>
		</div>
		<!-- end of search heading info -->

		<form:form action="" method="post" id="feedbackform" modelAttribute="command">

			<fieldset>
																						
			 	<ol>
					
					<li>
						<form:label path="title"><spring:message code='addfeedback.title.label' /></form:label>
						<form:input type="text" class="textlong" path="title" />
					</li>
					<li>
						<form:label path="url"><spring:message code='addfeedback.url.label' /></form:label>
						<form:input type="text" class="textlong" value="${command.url}" path="url" />
					</li>
					<li>
						<form:label path="description"><spring:message code='addfeedback.comment.label' /></form:label>
						<form:textarea path="description" cols="80" rows="8"></form:textarea>
					</li>
					<li>
						<label>&nbsp;</label>
						<input type="submit" name="submit" id="submit" value="<spring:message code='addfeedback.send' />"/>
					</li>
					
				</ol>
										
			</fieldset>

		</form:form>
	
	<!-- InstanceEndEditable -->

 	</div>
    </jsp:body>
</t:webTemplate>