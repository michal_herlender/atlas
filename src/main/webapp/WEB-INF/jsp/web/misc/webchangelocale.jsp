<%-- File name: /web/misc/webchangelocale.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:webTemplate idheader="">
    <jsp:body>
        <!-- main box body content div -->
		<div class="mainBoxBodyContent">					
		<div>
			<!-- left floated column -->							
			<div class="columnLeft-1">
				<h3 class="text-center"><spring:message code='webchangelocale.changelanguage'/></h3>
			</div>
			<!-- right floated column -->
			<div class="columnRight-1 text-right">
			</div>
			<!-- clear floats and restore page flow -->
			<div class="clear"></div>
		</div>
	
		<form:form action="" method="post" id="localeform" >
			<form:hidden path="refererUrl"/>
			<fieldset>
		 		<ol>
				
					<li>
						<label><spring:message code='webchangelocale.currentlanguage'/>:</label>
						${command.desiredLocale.getDisplayLanguage(rc.locale)}
						(${command.desiredLocale.getDisplayCountry(rc.locale)})
					</li>
					<li>
						<label><spring:message code='webchangelocale.desiredlanguage'/>:</label>
						<form:select path="desiredLocale">
							<c:forEach var="supportedLocale" items="${supportedLocales}">
								<option value="${supportedLocale}" <c:if test="${supportedLocale == command.desiredLocale}"> selected="selected" </c:if>>
								${supportedLocale.getDisplayLanguage(rc.locale)}
								(${supportedLocale.getDisplayCountry(rc.locale)})
								</option>
					  	</c:forEach>
						</form:select>
					</li>
				</ol>
									
		</fieldset>
		
		<div class="submit">
			<input type="submit" id="submit" value="<spring:message code='webchangelocale.changelanguage'/>"/>
		</div>
						
	</form:form>
	<!-- InstanceEndEditable -->
				
	</div>
	<!-- end of main box body content div -->
	</jsp:body>
</t:webTemplate>