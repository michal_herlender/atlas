<%-- File name: /trescal\web\misc\webuserprofile.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<html>

	<head>

		<link rel="SHORTCUT ICON" href="../img/company.ico" />
		
		<link rel="stylesheet" href="../styles/global_web.css" media="screen" />
		
		<c:if test="${systemType == 'development'}">
    		<link rel="stylesheet" href="../styles/theme/theme_web_test.css" type="text/css" title="antech" />
		</c:if>
		
		<script type='text/javascript'>

	 		// get value of loadtab parameter from querystring
	 		// used to show the correct tab when subnavigation bar is used
			var LOADTAB = '${loadtab}';
	
			// get string for certificate seal 
			var sslCertCode = '${sslCert}';
	
			// set variable to i18n of javascript
			var i18locale = '${rc.locale.language}';
			
			// set variable to default context of project
			var springUrl = '${req.contextPath}';
		
		</script>
		
		<title>	<spring:message code="termsandconditions.title" arguments="${businessDetails.company}"/></title>

		<script type='text/javascript' src='../script/web/misc/TermsAndConditions.js'></script>
						
		<script type='text/javascript' src='../script/thirdparty/jQuery/jquery.js'></script>
						
		<script type='text/javascript' src='../script/web/template/Web_Main.js'></script>
	
	</head>
		
	<body onload=" loadScript.init(true); ">
	
		<!-- this head links div contains all anchors that appear at the top of the page -->
		<div class="headLink">
			<div class="headLinkBox">
				<!-- logged in user div floated left (width 29%) -->
				<div class="loggedInUser">					
					<spring:message code="web.loggedinuser" arguments="${currentContact.name}"/>
					<a href="../logout"><img src="../img/icons/logout.png" width="16" height="16" alt="<spring:message code="web.logout" />" title="<spring:message code="web.logout" />" /></a>					
				</div>
				<!-- head links div floated right (width 69%) -->
				<div class="headLinkList">								
										
				</div>
				<!-- clear floats and restore page flow -->
				<div class="clear"></div>
			</div>
		</div>	
		<!-- end of div head links div -->
		
		<!-- main box div assigned background image -->
		<div class="mainBox">
			<!-- main box head div contains logos and links for common functionality -->
			<div class="mainBoxHead">
				<!-- Antech logo -->		
				<img src="../img/logo_colour_white.png"  alt="${businessDetails.company}" title="${businessDetails.company}" /><br />
				
				<!-- main box head links div contains images and links for email contacts -->
				<div class="mainBoxHeadLinks">
					<!-- email the web team link -->
					<div>
						<a href="mailto:${businessDetails.webEmail}" class="small_link email"><spring:message code="web.emailwebteam" /></a>
					</div>
					<!-- email the customer services team link -->
					<div>
						<a href="mailto:${businessDetails.salesEmail}" class="small_link email"><spring:message code="web.emailcustomerservicesteam" /></a>
					</div>
				</div>
				<!-- end of main box head links -->
			</div>
			<!-- end of main box div -->
			
			<!-- main box tabs contains all navigation tabs -->
			<div class="mainBoxTabs">
				<div class="mainBoxTabsContainer">
					
			  	</div>
				<div class="clear"></div>
			</div>
			<!-- end of navigation tabs div -->
			
			<!-- main box body div contains content for page -->
			<div class="mainBoxBody">
			
				<!-- main box body content div -->
				<div class="mainBoxBodyContent">
					
					<!-- error section displayed when form submission fails -->
					<form:errors path="command.*">
        				<div class="warningBox1">
                			 <div class="warningBox2">
                    	 	  <div class="warningBox3">
                    	   		<h4 class="attention text-center"><spring:message code="termsandconditions.error" /></h4>
                            	<c:forEach var="e" items="${messages}">
                               	<div class="center attention"><c:out value="${e}"/></div>
                            	</c:forEach>
                     	 	</div>
                 			</div>
           				</div>
       	   		    </form:errors>

					<!-- end error section -->
					
					<!-- div displaying search heading info -->
					<div>
						<!-- left floated column -->							
						<div class="columnLeft-1">
							<h3 class="text-center"><spring:message code="web.welcomeuser" arguments="${currentContact.firstName},${businessDetails.company}"/></h3>
							<div class="pad-left">
								<spring:message code="termsandconditions.info.general" arguments="${businessDetails.company}"/>
							</div>
						</div>
						<!-- right floated column -->
						<div class="columnRight-1 text-right">
							<img src="../img/web/terms.png" width="71" height="100" alt="" class="pad-10" />
						</div>
						<!-- clear floats and restore page flow -->
						<div class="clear"></div>
					</div>
					<!-- end of search heading info -->
					
					<!-- terms and conditions form elements -->
					<fieldset>
												
						<form:form method="post" action="" class="termsandconditionsform" modelAttribute="command">
					
							<ol>
								<li>
									<spring:message code="termsandconditions.info.password" />
								</li>
								<li>
									<form:label path="newpassword"><spring:message code="termsandconditions.newpassword.label" /></form:label>
									<form:input type="password" path="newpassword" value="" onkeypress=" if (event.keyCode==13){ event.preventDefault(); } " onkeyup=" passwordStrength(this.value, $j(this).next()); " />
									<div id="strengthMeter" class="strength0" style=" display: inline; padding: 4px 40px 4px; "><span class="passwordDescription">Password not entered</span></div>
									<form:errors path="newpassword" cssClass="errorabsolute">
									 	<span class="error"><img src="../img/web/asterisk.png" width="16" height="16" /></span>
									</form:errors>
<%-- 									<c:if test="${status.error}"><span class="error"><img src="../img/web/asterisk.png" width="16" height="16" /></span></c:if> --%>
								</li>
								<li>
									<form:label path="newpasswordConfirm"><spring:message code="termsandconditions.confirmpassword.label" /></form:label>
									<form:input type="password" path="newpasswordConfirm" value="" />
									<form:errors path="newpasswordConfirm" cssClass="errorabsolute">
									 	<span class="error"><img src="../img/web/asterisk.png" width="16" height="16" /></span>
									</form:errors>
								</li>
								<li>
									<spring:message code="termsandconditions.info.contact" arguments="${businessDetails.company}"/>
								</li>
								<li>
									<form:label path="contactPreference"><spring:message code="termsandconditions.contactpref.label" /></form:label>
									<form:select path="contactPreference">
										<option value="E" <c:if test="${command.contact.preference == 'E'}"> selected="selected" </c:if>><spring:message code="web.email" /></option>
										<option value="F" <c:if test="${command.contact.preference == 'F'}"> selected="selected" </c:if>><spring:message code="web.fax" /></option>
										<option value="L" <c:if test="${command.contact.preference == 'L'}"> selected="selected" </c:if>><spring:message code="web.letter" /></option>
									</form:select>
								</li>
								<li>
									<label><spring:message code="termsandconditions.label" /></label>
									<textarea rows="20" cols="80" readonly="readonly"><spring:message code="termsandconditions.text" arguments="${businessDetails.company}"/>
									</textarea>
								</li>
								<li>
									<label><spring:message code="termsandconditions.accept.label" /></label>
									<input type="hidden" name="_accept" />
									<form:checkbox path="accept" /> 
									<form:errors path="accept" cssClass="errorabsolute">
									 	<span class="error"><img src="../img/web/asterisk.png" width="16" height="16" /></span>
									</form:errors>
								</li>	
								<li>
									<label>&nbsp;</label>
									<input type="submit" name="submit" value="<spring:message code="web.continue" />"/>
								</li>
							</ol>
																					
						</form:form>
						
					</fieldset>
				
			  	</div>
				<!-- end of main box body content div -->
			
				<!-- main box body content footer contains copyright message -->
				<div class="mainBoxBodyContentFooter">
					<jsp:useBean id="date" class="java.util.Date" />
					&copy;&nbsp;<fmt:formatDate value='${date}' type="date" dateStyle="SHORT" />&nbsp;Trescal
				</div>
				<!-- end of main box body content footer div -->
				
			</div>
			<!-- end of main box body div -->
		
		</div>
		<!-- end of main box div -->
	
	</body>

</html>
