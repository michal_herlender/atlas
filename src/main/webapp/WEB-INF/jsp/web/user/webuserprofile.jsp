<%-- File name: /trescal\web\user/webuserprofile.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="instrument" tagdir="/WEB-INF/tags/instrument" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:webTemplate idheader="">
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='../script/web/user/WebUserProfile.js'></script>
    </jsp:attribute>
    <jsp:body>
        <!-- main box body content div -->
<div class="mainBoxBodyContent">

	<form:errors path="command.*">
		<div class="warningBox1">
			<div class="warningBox2">
				<div class="warningBox3">
					<h5 class="center-0 attention"><spring:message code="profile.error"/>:</h5>
					<c:forEach var="e" items="${messages}">
						<div class="center attention"><c:out value="${e}"/></div>
					</c:forEach>
				</div>
			</div>
		</div>
	</form:errors>
		<!-- end error section -->
		
		<!-- div displaying search heading info -->
		<div>
			<!-- left floated column -->							
			<div class="columnLeft-1">
				<h3 class="text-center"><spring:message code="profile.title" /></h3>
				<div class="pad-left"><spring:message code="profile.info" /></div>
			</div>

			<div class="clear"></div>
		</div>

		
		<c:if test="${not empty command.message}">
			<div class="successBox1">
				<div class="successBox2">
					<div class="successBox3">
						<div class="text-center">${command.message}</div>
					</div>
				</div>
			</div>
			<!-- end of the success view -->
		</c:if>
							
		<!-- search form elements -->
		<fieldset>
									
			<form:form method="post" action="">
				<!-- left floated column -->
				<div class="columnLeft-4">

					<ol>
						
						<li>
							<label><spring:message code="web.name.label" /></label>
							<span>${command.contact.name}</span>
						</li>
						<li>
							<label><spring:message code="web.company.label" /></label>
							<span>${command.contact.sub.comp.coname}</span>
						</li>
						<li>
							<label><spring:message code="web.subdivision.label" /></label>
							<span><instrument:subdivFilter subdiv="${command.contact.sub}" /></span>			
						</li>										
						<li>
							<label><spring:message code="web.usergroup.label" /></label>
							<span>

								<c:set var="groupid" value="${command.contact.usergroup.groupid}" />
								<c:choose>
									<c:when test="${groupid != 1}">
									${command.contact.usergroup.description}									
									</c:when>
		      					    <c:otherwise>
									<spring:message code="profile.nousergroup" />
									</c:otherwise>
               					</c:choose>
							</span>
						</li>																
						<li>
							<form:label path="addrid"><spring:message code="web.defaultaddress.label" /></form:label>
							<form:select path="addrid" >
								<form:options items="${addresses}" itemValue="addrid" itemLabel="addressLine"/>
							</form:select>
							<form:errors  path="addrid"><span class="error"><img src="../img/web/asterisk.png" width="16" height="16" /></span></form:errors>
						</li>			
						<li>
							<label><spring:message code="web.country.label" /></label>
							<span>${command.contact.defAddress.country.localizedName}</span>
						</li>
						<li>
							<label>&nbsp;</label>
							<input type="submit" name="submit" class="button" value="<spring:message code='profile.update' />"/>
						</li>													
																		
					</ol>
					
				</div>
				<!-- end of left column -->
				
				<!-- left floated column -->
				<div class="columnRight-4">

					<ol>
						
						<li>
							<form:label path="contact.telephone"><spring:message code="web.telephone.label" /></form:label>
							<form:input path="contact.telephone" type="tel"/>
							<form:errors  path="contact.telephone"><span class="error"><img src="../img/web/asterisk.png" width="16" height="16" /></span></form:errors>
						</li>
						
						<li>
							<form:label path="contact.telephoneExt"><spring:message code="web.ext.label" /></form:label>
							<form:input path="contact.telephoneExt" type="number"/>
							<form:errors  path="contact.telephoneExt"><span class="error"><img src="../img/web/asterisk.png" width="16" height="16" /></span></form:errors>
						</li>
						
						<li>
							<form:label path="contact.fax"><spring:message code="web.facsimile.label" /></form:label>
							<form:input path="contact.fax" type="tel"/>
							<form:errors  path="contact.fax"><span class="error"><img src="../img/web/asterisk.png" width="16" height="16" /></span></form:errors>
						</li>
						
						<li>
							<form:label path="contact.mobile"><spring:message code="web.mobile.label" /></form:label>
							<form:input path="contact.mobile" type="tel"/>
							<form:errors  path="contact.mobile"><span class="error"><img src="../img/web/asterisk.png" width="16" height="16" /></span></form:errors>
						</li>
						
						<li>
							<form:label path="contact.email"><spring:message code="web.email.label" /></form:label>
							<form:input path="contact.email" type="email"/>
							<form:errors  path="contact.email"><span class="error"><img src="../img/web/asterisk.png" width="16" height="16" /></span></form:errors>
						</li>
						
						<li>
							<form:label path="contact.position"><spring:message code="web.position.label" /></form:label>
							<form:input path="contact.position" type="text"/>
							<form:errors  path="contact.position"><span class="error"><img src="../img/web/asterisk.png" width="16" height="16" /></span></form:errors>
						</li>
						
						<li>
							<form:label path="contact.preference"><spring:message code="web.preference.label" /></form:label>
							<form:select path="contact.preference">
								<spring:message code="web.email" var="emailLabel"/>
								<spring:message code="web.fax" var="faxLabel"/>
								<spring:message code="web.letter" var="letterLabel"/>
								<form:option value="E" label="${emailLabel}"/>
								<form:option value="F" label="${faxLabel}"/>
								<form:option value="L" label="${letterLabel}"/>
							</form:select>
							<form:errors  path="contact.preference"><span class="error"><img src="../img/web/asterisk.png" width="16" height="16" /></span></form:errors>
						</li>
						
						<li>
							<label><spring:message code="web.password.label" /></label>
							<span>
								<a href="#" class="mainlink" onclick=" changePassword(${currentContact.personid}); return false; "><spring:message code="profile.changepassword" /></a>
							</span>
						</li>										
						
					</ol>
					
				</div>
				<!-- end of right column -->
				
			</form:form>
				
		</fieldset>																		
	
	<!-- InstanceEndEditable -->

</div>
<!-- end of main box body content div -->
    </jsp:body>
</t:webTemplate>