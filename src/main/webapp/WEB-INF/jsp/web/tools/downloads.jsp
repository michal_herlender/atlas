<%-- File name: /trescal/core/web/quotations/downloads.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="instrument" tagdir="/WEB-INF/tags/instrument" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="web" tagdir="/WEB-INF/tags/web" %>

<t:webTemplate idheader="">
	<jsp:attribute name="scriptPart">
	</jsp:attribute>
	<jsp:body>
		<!-- main box body content div -->
		<div class="mainBoxBodyContent">
			
			<!-- InstanceBeginEditable name="content" -->
			
				<!-- div displaying search heading info -->
				<div>
					<!-- left floated column -->							
					<div class="columnLeft-1">
						<h3 class="text-center"><spring:message code="tools.download.title" arguments="${userContact.sub.comp.coname}" /></h3>
						<div class="pad-left"><spring:message code="tools.download.info"/></div>
					</div>
					<!-- right floated column -->
					<div class="columnRight-1 text-right">
						<img src="../img/web/download.png" width="160" height="80" alt="" class="pad-5" />
					</div>
					<!-- clear floats and restore page flow -->
					<div class="clear"></div>
				</div>
				<!-- end of search heading info -->
				
				<div>
					<c:choose>
						<c:when test="${not empty files}">
							<c:forEach var="keyValue" items="${files.entrySet()}">
								<c:set var="icon" value="${icons.get(keyValue.key)}" />
								<div class="downloadFile">
									<a href="downloadfile.htm?file=${keyValue.value}" target="_blank">
										<img src="../${icon}" width="49" height="47" />
									</a>
									<a href="downloadfile.htm?file=${keyValue.value}" target="_blank" class="mainlink">${keyValue.key}</a>								
								</div>
							</c:forEach>
							<div class="clear"></div>
						</c:when>
						<c:otherwise>
							<div class="noFiles"><spring:message code="tools.download.nofiles" arguments="${userContact.sub.comp.coname}" /></div>
						</c:otherwise>
					</c:choose>				
				</div>
				
				<div class="tpDownload">
				
					<p class="text-center">
						<spring:message code="tools.download.acrobatinfo" />
						<a href="http://get.adobe.com/reader/" target="_blank">
							<img src="../img/web/adobereader.png"  height="31" alt="<spring:message code='tools.download.acrobatget' />" title="<spring:message code='tools.download.acrobatget' />" />
						</a>
					</p>
				</div>
			<!-- InstanceEndEditable -->
		 	</div>
		<!-- end of main box body content div -->
	</jsp:body>
</t:webTemplate>