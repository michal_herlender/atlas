<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="web" tagdir="/WEB-INF/tags/web" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="instrument" tagdir="/WEB-INF/tags/instrument"%>

<t:webTemplate idheader="cert">
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='../script/web/jobs/certificate/WebViewCert.js'></script>
	</jsp:attribute>
	<jsp:body>
		<div class="mainBoxBodyContent">
			<fieldset>	
				<!-- left floated column -->
				<div class="columnLeft-4">				
					<h3 class="text-center">
					<spring:message code="certificate.view.title" arguments="${model.cert.certno}"/></h3>
								<div class="pad-left">
									<spring:message code="certificate.view.info"/>		
								</div>
				
								<ol>									
									<li>
										<label><spring:message code="web.certificateno.label"/></label>
										<span>${model.cert.certno}</span>
									</li>
									<li>
										<label><spring:message code="web.caltype.label"/></label>
										<span><cwms:besttranslation translations="${model.cert.calType.serviceType.longnameTranslation}" /></span>
									</li>
									<li>
										<label><spring:message code="web.issuedate.label"/></label>
										<span><fmt:formatDate value="${model.cert.certDate}" type="date" dateStyle="SHORT" /></span>
									</li>
									<c:if test="${showCalVerificationStatus == true }">
										<li>
											<label><spring:message code="viewinstrument.calverificationstatus"/></label>
											<span>${model.cert.calibrationVerificationStatus.getName()}</span>
										</li>
									</c:if>
									<c:if test="${showDeviation == true }">
										<li>
											<label><spring:message code="certificate.deviation"/></label>
											<span>${model.cert.deviation.stripTrailingZeros()}</span>
										</li>
									</c:if>
									<c:if test="${showClass == true }">
										<li>
											<label><spring:message code="certificate.class"/></label>
											<span>${model.cert.certClass.getDescription()}</span>
										</li>
									</c:if>
									<li>
										<label>${businessDetails.docCompany} <spring:message code="web.jobno.label"/></label>
										<div class="float-left padtop">
											<c:if test="${model.cert.links.size() > 0}">
												<c:set var="currentJobNo" value=""/>
												<c:forEach var="cl" items="${model.cert.links}" varStatus="loop">
													<c:if test="${currentJobNo != cl.jobItem.job.jobno}">
														${(loop.count > 1) ? "," : ""} <instrument:jobnoLinkWeb job="${cl.jobItem.job}"/>
													</c:if>
													<c:set var="currentJobNo" value="${cl.jobItem.job.jobno}"/>								
												</c:forEach>
											</c:if>
										</div>
										<!-- clear floats and restore page flow -->
										<div class="clear"></div>
									</li>
									<li>
										<label><spring:message code="web.purchaseorderno.label"/></label>
										<c:choose>
											<c:when test="${model.cert.links.size() > 1 }">
												<c:forEach var="cl" items="${model.cert.links}" varStatus="loop">
													<c:if test="${loop.count < 2}">
														<c:choose>
															<c:when test="${cl.jobItem.jobItemPOs.size() > 0}">
																<div class="float-left padtop">
																	<c:forEach var="item" items="${cl.jobItem.jobItemPOs}" varStatus="loop1">
																		<c:choose>
																			<c:when test="${item.po != null}">
																				${item.po.poNumber} [PO] ${(loop1.count > 1) ? "<br />" : ""}
																			</c:when>
																			<c:otherwise>
																				${item.bpo.poNumber} [BPO] ${(loop1.count > 1) ? "<br />" : ""} 
																			</c:otherwise>
																		</c:choose>								
																	</c:forEach>
																	&nbsp;
																</div>
																<!-- clear floats and restore page flow -->
																<div class="clear"></div>
															</c:when>
															<c:otherwise>
																<span><spring:message code="web.nopurchaseorder"/></span>
															</c:otherwise>
														</c:choose>
													</c:if>
												</c:forEach>
											</c:when>
											<c:otherwise>
												<span><spring:message code="web.nopurchaseorder"/></span>
											</c:otherwise>
										</c:choose>										
									</li>
									<li>
										<label><spring:message code="web.clientref.label"/></label>
										<span>
											<c:choose>
												<c:when test="${cert != null && cert.links != null && cert.links.size() > 1}">							
													<c:forEach var="cl" items="${cert.links}" varStatus="loop">
														<c:if test="${loop.count < 2}">	
															<c:choose>													
																<c:when test="${cl.jobItem.job.clientRef.trim().length() > 0}">
																	<div class="float-left padtop">
																		${cl.jobItem.job.clientRef}
																		&nbsp;
																	</div>
																	<!-- clear floats and restore page flow -->
																	<div class="clear"></div>
																</c:when>
																<c:otherwise>
																	<span><spring:message code="web.noclientref"/></span>
																</c:otherwise>
															</c:choose>
														</c:if>
													</c:forEach>
												</c:when>
												<c:otherwise>
													<span><spring:message code="web.noclientref"/></span>
												</c:otherwise>	
											</c:choose>
										</span>
									</li>	
									<li>
										<label><spring:message code="viewcertificate.remarks" />:</label>
										<span>${model.cert.remarks}</span>
									</li>
									<c:if test="${obsoleteAllowed}">
										<li>
											<label><spring:message code="viewcertificate.obsolete"/>:</label>
											<span id="isitobsolete">
											<c:choose>
												<c:when test="${obsolete}">
													<spring:message code="yes"/>
												</c:when>
												<c:otherwise>
													<spring:message code="no"/>
												</c:otherwise>
											</c:choose>
										</span>
										</li>
									</c:if>
								</ol>
							</div>
							<!-- end of left column -->
							
							<!-- right floated column -->
							<div class="columnRight-4">						
								
								<div class="text-center">
									<div>
										<h3><spring:message code="certificate.view.download"/></h3>
									</div>
									<c:choose>
										<c:when test="${model.cert.hasCertFile}">
											<c:url var="downloadURL" value="downloadfile.htm"><c:param name="file" value="${model.cert.encryptedCertFilePath}" /></c:url>
	    									<a href="${downloadURL}" target="_blank">
	    										<img src="../img/doctypes/pdf-large.png" width="100" height="120" alt='<spring:message code="certificate.view.link"/>' title='<spring:message code="certificate.view.link"/>' />
	    									</a>
	    									<div>
	    										<spring:message code="certificate.view.download.info"/>
	    									</div>
										</c:when>
										<c:otherwise>
											<img src="../img/doctypes/pdf-large-unavailable.png" width="100" height="120" 
											alt='<spring:message code="viewcertificate.imgpdflargeunavailable"/>' 
											title='<spring:message code="viewcertificate.imgpdflargeunavailable"/>' />
										</c:otherwise>
									</c:choose>
								</div>	
								<div style="position: relative; top: 70px;">
								    <input type="hidden" value="${model.cert.certid}" id="certid">
                                    <c:if test="${obsoleteAllowed}">
                                        <button id="obsoletify" ${obsolete ? "style='display: none'" : ""}><spring:message code="viewcertificate.makeobsolete"/></button>
                                        <button id="unobsoletify" ${obsolete ? "" : "style='display: none'"}><spring:message code="viewcertificate.makenotobsolete"/></button>
                                    </c:if>
									<c:if test="${canPrintCertAccepptanceDoc}">
										<br><br>
									    <button id="certvalidation"><spring:message code="viewcertificate.certacceptancereport"/></button>
                                    </c:if>
								</div>			
							</div>
							<!-- end of right column -->
				
						</fieldset>
						<!-- end of instrument elements -->
						
						<table class="default certificateInstruments" summary="This table displays all certificate instruments">
							<thead>
								<tr>
									<td colspan="6">
										<c:set var="nbCert" value="0"/>
										<c:choose>
											<c:when test="${model.cert.links.size() > 0}">
												<c:set var="nbCert" value="${model.cert.links.size()}"/>
											</c:when>
											<c:otherwise>
												<c:set var="nbCert" value="${model.cert.instCertLinks.size()}"/>
											</c:otherwise>
										</c:choose>
										<spring:message code="certificate.view.items"  arguments="${nbCert}"/>
									</td>
								</tr>
								<tr>
									<th class="barcode" scope="col"><spring:message code="web.barcode"/></th>
									<th class="inst" scope="col"><spring:message code="web.instrument"/></th>
									<th class="serial" scope="col"><spring:message code="web.serialno"/></th>
									<th class="plant" scope="col"><spring:message code="web.plantno"/></th>
									<th class="jobno" scope="col"><spring:message code="web.jobno"/></th>
									<th class="itemno" scope="col"><spring:message code="web.itemno"/></th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<td colspan="6">&nbsp;</td>
								</tr>
							</tfoot>
							<tbody>
								<c:set var="rowcount" value="0"/>
								<c:choose>	
									<c:when test="${model.cert.links.size() > 0 }">				
										<c:forEach var="cl" items="${model.cert.links}">
											<tr style=" background-color: ${cert.calType.serviceType.displayColour}; ">
												<td class="barcode">${cl.jobItem.inst.plantid}</td>
												<td class="inst"><web:instrumentLinkWeb displayCalTimescale="${true}" instrument="${cl.jobItem.inst}" /></td>
												<td class="serial">${cl.jobItem.inst.serialno}</td>
												<td class="plant">${cl.jobItem.inst.plantno}</td>
												<td class="jobno"><instrument:jobnoLinkWeb job="${cl.jobItem.job}" /></td>
												<td class="itemno">${cl.jobItem.itemNo}</td>													
											</tr>
											<c:set var="rowcount"  value="${rowcount + 1}"/>										
										</c:forEach>
									</c:when>
									<c:otherwise>
										<c:forEach var="icl" items="${model.cert.instCertLinks}">
											<tr style=" background-color: ${cert.calType.serviceType.displayColour}; ">
												<td class="barcode">${icl.inst.plantid}</td>
												<td class="inst"><web:instrumentLinkWeb displayCalTimescale="${true}" instrument="${icl.inst}"/></td>
												<td class="serial">${icl.inst.serialno}</td>
												<td class="plant">${icl.inst.plantno}</td>
												<td class="jobno"></td>
												<td class="itemno"></td>													
											</tr>										
											<c:set var="rowcount" value="${rowcount + 1}"/>
										</c:forEach>											
									</c:otherwise>
								</c:choose>
							</tbody>
						</table>

				
						<table class="default certificateValidations" summary="This table displays all certificate validations">
							<thead>
								<tr>
									<td colspan="4">
										<spring:message code="certificate.view.validations"/>
									</td>
								</tr>
								<tr>
									<th scope="col"><spring:message code="viewquot.acceptance" /></th>
									<th scope="col"><spring:message code="certificate.view.acceptdate" /></th>
									<th scope="col"><spring:message code="certificate.view.acceptedby" /></th>
									<th scope="col"><spring:message code="certificationvalidation.note"/></th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<td colspan="4">&nbsp;</td>
								</tr>
							</tfoot>
							<tbody>
								<c:choose>
									<c:when test="${not empty model.cert.validations and model.cert.validations.size() > 0}">
										<c:forEach var="v" items="${model.cert.validations}">
											<tr>
												<td><span class="${ v.status eq 'ACCEPTED' ? 'accepteddate certaccepted': v.status eq 'REJECTED' ? 'accepteddate certrejected':'accepteddate certpending' }">${v.status.name}</span></td>
												<td><fmt:formatDate value="${v.datevalidated}" type="date" pattern="dd-MM-yyyy HH:mm"/></td>
												<td>${v.validatedBy.name}</td>	
												<td>${v.note}</td>													
											</tr>									
										</c:forEach>
									</c:when>
									<c:otherwise>
										<td colspan="4">
											<div style="text-align: center;"><spring:message code="viewjob.noitems"/></div>
										</td>
									</c:otherwise>
								</c:choose>
							</tbody>
						</table>
			  	</div>
				<!-- end of main box body content div -->
	
	
	</jsp:body>
</t:webTemplate>