<%-- File name: /jsp/web/jobs/certificate_/webcertsearch.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="instrument" tagdir="/WEB-INF/tags/instrument"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:webTemplate idheader="cert">
    <jsp:attribute name="scriptPart">
		<script type='text/javascript' src='../script/web/jobs/certificate/WebCertSearch.js'></script>
    </jsp:attribute>
    <jsp:body>
		<!-- main box body content div -->
		<div class="mainBoxBodyContent">
		
			<!-- InstanceBeginEditable name="content" -->
				 <form:errors path="form.*">
       				 <div class="warningBox1">
              		  <div class="warningBox2">
              	       <div class="warningBox3">
                    	<h4 class="center-0 attention"><spring:message code="web.search.error"/></h4>
                      <c:forEach var="e" items="${messages}">
                        <div class="center attention"><c:out value="${e}"/></div>
                      </c:forEach>
                     </div>
                   </div>
                  </div>
                 </form:errors>
				<!-- end error section -->
				
				<!-- div displaying search heading info -->
				<div>
					<!-- left floated column -->							
					<div class="columnLeft-1">
						<h3 class="text-center"><spring:message code="certificate.search.title"/></h3>
						<div class="pad-left">
							<spring:message code="certificate.search.info"/>
						</div>
					</div>
					<!-- right floated column
					<div class="columnRight-1 text-right">
						<img src="../img/web/certs.png" width="120" height="78" alt="" class="pad-5" />
					</div>
					clear floats and restore page flow -->
					<div class="clear"></div>
				</div>
				<!-- end of search heading info -->

				<!-- search form elements -->
				<fieldset>
					
					<form:form method="post" action="" id="certsearchform" onsubmit=" loadScript.applySearchingImage($j(this).find('input[type=\'submit\']').parent()); " modelAttribute="form" >
																																
					<!-- left floated column -->
					<div class="columnLeft-2">

						<ol>											
							<li>
								<form:label path="certNo"><spring:message code="web.certificateno.label"/></form:label>
								<form:input type="text" path="certNo" />
							</li>
							<li>
								<form:label path="jobNo"><c:out value="${businessDetails.docCompany}"/>&nbsp;<spring:message code="web.jobno.label"/></form:label>
								<form:input type="text" path="jobNo" />
							</li>
							<li>
								<form:label path="bc"><spring:message code="web.barcode.label"/></form:label>
								<form:input type="text" path="bc" />
							</li>
							<li>
								<form:label path="serialNo"><spring:message code="web.serialno.label"/></form:label>
								<form:input type="text" path="serialNo" />
							</li>
							<li>
								<form:label path="plantNo"><spring:message code="web.plantno.label"/></form:label>
								<form:input type="text" path="plantNo" />
							</li>										
							<li>
								<form:label path="calTypeId"><spring:message code="web.caltype.label"/></form:label>
								<form:select path="calTypeId" id="calTypeId">
									<option value="">--&nbsp;<spring:message code="web.any"/>&nbsp;--</option>
									<c:forEach var="ct" items="${caltypes}">
										<option value="${ct.calTypeId}"><t:showTranslationOrDefault translations="${ct.serviceType.longnameTranslation}" defaultLocale="${defaultlocale}"/>&nbsp;<t:showTranslationOrDefault translations="${ct.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/> </option>
									</c:forEach>
								</form:select>
							</li>
							<c:if test="${certvalidation}">
								<li>
									<label><spring:message code="web.certsearch.unacceptedonly"/></label>
									<form:checkbox path="unAcceptedCertsOnly"/>
								</li>
                                <li class="dateInputs" id="acceptdateinputs">
                                        <form:select path="acceptDateBetween" id="acceptDateBetween" onchange=" changeAcceptDateOption(this.value); return false; ">
                                            <option value="false" <c:if test="${form.acceptDateBetween == false }"> selected </c:if>><spring:message code="web.on"/></option>
                                            <option value="true" <c:if test="${form.acceptDateBetween == true }">  selected </c:if>><spring:message code="web.between"/></option>
                                        </form:select>

                                        <form:label path="acceptDate1"><spring:message code="web.acceptrejectdate.label"/></form:label>
                                        <form:input type="date" path="acceptDate1" id="acceptDate1" readonly="readonly" />

                                    <span id="acceptDateSpan" class="${form.acceptDateBetween ? 'vis' : 'hid'}">
                                        <spring:message code="web.and"/>&nbsp;
                                            <form:input type="date" path="acceptDate2" id="acceptDate2" readonly="readonly" />
                                    </span>
                                </li>
								<li>
                                    <label><spring:message code="viewinstrument.calverificationstatus"/></label>
                                        <form:select path="calibrationVerificationStatus">
                                            <option value="" selected><spring:message code="web.any"/></option>
                                            <c:forEach items="${calVerificationStatuses}" var="calStatus">
                                                <option value="${calStatus.key}"><c:out value="${calStatus.value}"/></option>
                                            </c:forEach>
                                        </form:select>
								</li>
                            </c:if>
                            <li class="dateInputs">
								<form:select path="jobItemCompletionDateBetween" id="jiCompletionDateBetween" onchange=" changeJiCompletionDateOption(this.value); return false; ">
									<option value="false" <c:if test="${form.jobItemCompletionDateBetween == false }"> selected </c:if>><spring:message code="web.on"/></option>
									<option value="true" <c:if test="${form.jobItemCompletionDateBetween == true }">  selected </c:if>><spring:message code="web.between"/></option>
								</form:select>

								<form:label path="${status.expression}"><spring:message code="web.jobitemcompletiondate.label"/></form:label>
								<form:input type="text" path="jobItemDateComplete1" id="jobItemDateComplete1" readonly="readonly" />											

								<span id="jobItemCompletionDateSpan" class="${form.jobItemCompletionDateBetween ? 'vis' : 'hid'}">
									<spring:message code="web.and"/>&nbsp;
									<form:input type="text" path="jobItemDateComplete2" id="jobItemDateComplete2" readonly="readonly" />
								</span>
							</li>
							<li>
								<label>&nbsp;</label>
								<input type="submit" name="submit" value="<spring:message code="web.search"/>" />
							</li>
						</ol>
					
					</div>
					<!-- end of left column -->
					
					<!-- right floated column -->
					<div class="columnRight-2">
					
						<ol>								
							<li>										
								<form:label path="subdivid"><spring:message code="web.subdivision.label"/></form:label>
								<c:choose>
									<c:when test="${userReadRule == 'COMPANY'}">
										<c:choose>
											<c:when test="${subdivs.size() > 1}">									
											<form:select id="subdivid" path="subdivid" onchange=" getActiveSubdivAddrsAndConts(this.value, $j(this).parent().next().find('select'), $j(this).parent().next().next().find('select'), true, ''); return false; ">											
											<option value="" selected="selected"><spring:message code="web.search.allsubdivisions"/></option>
											<c:forEach var="sub" items="${subdivs}">
												<option value="${sub.subdivid}"><c:out value="${sub.subname}"/></option>
											</c:forEach>
											</form:select>
											</c:when>
											<c:otherwise>
											<span><instrument:subdivFilter subdiv="${userContact.sub}" /></span>
											<form:hidden path="subdivid" value="${userContact.sub.subdivid}" />
											</c:otherwise>
										</c:choose>	
									</c:when>
									<c:otherwise>
									<span><instrument:subdivFilter subdiv="${userContact.sub}" /></span>
									<form:hidden path="subdivid" value="${userContact.sub.subdivid}" />
									</c:otherwise>
								</c:choose>	
								<form:errors  path="subdivid"><span class="error"><img src="../img/web/asterisk.png" width="16" height="16" /></span></form:errors>
							</li>
							<li>
								<form:label path="addrid"><spring:message code="web.address.label"/></form:label>
								<c:choose>
									<c:when test="${userReadRule == 'COMPANY' || userReadRule == 'SUBDIV'}">
									<form:select path="addrid" id="addrid">
										<option value="" selected="selected"><spring:message code="web.search.alladdresses"/></option>
										<c:forEach var="a" items="${addresses}">
											<option value="${a.addrid}">${a.addr1}, ${a.addr2}</option>
										</c:forEach>
									</form:select>
									</c:when>
									<c:otherwise>
									<span>${userContact.defAddress.addr1}, ${userContact.defAddress.addr2}</span>
									<form:hidden path="addrid" value="${userContact.defAddress.addrid}"/>
									</c:otherwise>
								</c:choose>
								<form:errors  path="addrid"><span class="error"><img src="../img/web/asterisk.png" width="16" height="16" /></span></form:errors>
							</li>
							<li>
								<form:label path="personid"><spring:message code="web.contactname.label"/></form:label>
									<c:choose>
										<c:when test="${userReadRule == 'COMPANY' || userReadRule == 'SUBDIV' || userReadRule == 'ADRESS'}"> 
										<form:select id="personid" path="personid">
										<option value="" selected="selected"><spring:message code="web.search.allcontacts"/></option>
										<c:forEach var="c" items="${contacts}">
											<option value="${c.personid}"><c:out value="${c.reversedName}"/></option>
										</c:forEach>
										</form:select>
										</c:when>
										<c:otherwise>
										<span><c:out value="${userContact.name}"/></span>
										<form:input type="hidden" path="personid" />
										</c:otherwise>
									</c:choose>
									<form:errors  path="personid"><span class="error"><img src="../img/web/asterisk.png" width="16" height="16" /></span></form:errors>
							</li>
							<li>
								<form:label path="mfr"><spring:message code="web.manufacturer.label"/></form:label>
								<div class="ui-widget">
									<input id="mfr" path="mfr" type="text" value="${form.mfr}"/>
								</div>
								<form:input type="hidden" id="mfrid" path="mfrid" />
							</li>
							<li>
								<form:label path="model"><spring:message code="web.model.label"/></form:label>
								<form:input type="text" path="model" />
							</li>
							<li>
								<form:label path="desc"><spring:message code="web.description.label"/></form:label>
								<div class="ui-widget">
									<form:input id="desc" path="desc" type="text" />
								</div>
								<form:input id="descid" path="descid" type="hidden" />
							</li>
							<li>
									<form:label path="tpCertNo"><spring:message code="certsearch.thirdpartcertno"/></form:label>
									<form:input type="text" path="tpCertNo" />
							</li>
							<li class="dateInputs">
								<form:select path="issueDateBetween" id="issueDateBetween" onchange=" changeIssueDateOption(this.value); return false; ">
									<option value="false" <c:if test="${form.issueDateBetween == false}"> selected </c:if>><spring:message code="web.on"/></option>
									<option value="true" <c:if test="${form.issueDateBetween == true}"> selected </c:if>><spring:message code="web.between"/></option>
								</form:select>
								
								<form:label path="issueDate1"><spring:message code="web.issuedate.label"/></form:label>
								<form:input type="text" path="issueDate1" id="issueDate1" readonly="readonly" />

								<span id="issueDateSpan" class="${form.issueDateBetween ? 'vis' : 'hid'}">	
									<spring:message code="web.and"/>&nbsp;
									<form:input type="text" path="issueDate2" id="issueDate2" readonly="readonly" />
								</span>
							</li>
							<li class="dateInputs">
								<form:select path="calDateBetween" id="calDateBetween" onchange=" changeCalDateOption(this.value); return false; ">
									<option value="false" <c:if test="${form.calDateBetween == false }"> selected </c:if>><spring:message code="web.on"/></option>
									<option value="true" <c:if test="${form.calDateBetween == true }">  selected </c:if>><spring:message code="web.between"/></option>
								</form:select>

								<form:label path="${status.expression}"><spring:message code="web.calibrationdate.label"/></form:label>
								<form:input type="text" path="calDate1" id="calDate1" readonly="readonly" />											

								<span id="calDateSpan" class="${form.calDateBetween ? 'vis' : 'hid'}">
									<spring:message code="web.and"/>&nbsp;
									<form:input type="text" path="calDate2" id="calDate2" readonly="readonly" />
								</span>
							</li>


						</ol>
					
					</div>
					<!-- end of right column -->
					
					</form:form>

				</fieldset>
				<!-- end of search form elements -->					
									
			<!-- InstanceEndEditable -->
		
	  	</div>
		<!-- end of main box body content div -->
    </jsp:body>
</t:webTemplate>