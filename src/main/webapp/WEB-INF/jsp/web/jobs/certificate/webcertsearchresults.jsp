<%-- File name: /trescal/core/web/jobs/certificate/webcertsearchresults.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="web" tagdir="/WEB-INF/tags/web" %>

<t:webTemplate idheader="cert">
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='../script/web/jobs/certificate/WebCertSearchResults.js'></script>
		<web:convertBusinessDetails businessDetails="${businessDetails}" />
		<web:convertContactDetails currentContact="${currentContact}" />
	</jsp:attribute>
	<jsp:body>
		<div class="mainBoxBodyContent">
			<c:set var="results" value="${form.rs.results}" />
			<div class="text-center" style="padding-bottom: 10px">
				<button class="float-left" id="searchagain"><spring:message code="web.search.again" /></button>
				<table class="float-right" style="font-weight: bold">
					<tr>
						<td><span id="totalselected">0</span></td>
						<td><spring:message code="web.certificatesselected"/></td>
						<c:if test="${certvalidation}">
							<td><button id="acceptcerts" class="selectedcertaction" disabled="true"><spring:message code="web.acceptreject.selectedcerts"/></button></td>
							<td><button id="downloadacceptdocs" class="selectedcertaction" disabled="true"><spring:message code="web.download.selectedacceptancedocs"/></button></td>
						</c:if>
						<td><button id="downloadcerts" class="selectedcertaction" disabled="true"><spring:message code="web.download.selectedcerts"/></button></td>
						<td><button id="clearselections" class="selectedcertaction" disabled="true"><spring:message code="web.accept.clearselections"/></button></td>
					</tr>
				</table>
				<div class="clear"></div>
			</div>
				
			<form:form id="form" action="" method="post" modelAttribute="form">
				<form:hidden path="resultsPerPage"/>
				<form:hidden path="pageNo"/>
				
				<form:hidden path="coid" />							
				<form:hidden path="subdivid" />
				<form:hidden path="addrid" />
				<form:hidden path="personid" />
				
				<form:hidden path="certNo" />
				<form:hidden path="status" />
				<form:hidden path="calTypeId" />
				<form:hidden path="mfr" />
				<form:hidden path="mfrid" />
				<form:hidden path="model" />
				<form:hidden path="modelid" />
				<form:hidden path="desc" />
				<form:hidden path="descid" />
				
				<form:hidden path="issueDate1" />
				<form:hidden path="issueDate2" />
				<form:hidden path="calDate1" />
				<form:hidden path="calDate2" />
				
				<form:hidden path="issueDateBetween" />
				<form:hidden path="calDateBetween" />
				
				<form:hidden path="tpId" />
				<form:hidden path="tpCertNo" />
				<form:hidden path="bc" />
				<form:hidden path="jobNo" />
				<form:hidden path="serialNo" />
				<form:hidden path="plantNo" />
				<form:hidden path="initialResultsPage"/>
				<form:hidden path="unAcceptedCertsOnly"/>

                <form:hidden path="acceptDateBetween"/>
                <form:hidden path="acceptDate1"/>
                <form:hidden path="acceptDate2"/>
				
				<t:showResultsPagination rs="${form.rs}" pageNoId="" />
											
				<table class="default tfirst certResults" summary="This table lists all certificate search headings">
					<thead>
						<tr>
							<td colspan="11"><spring:message code="certificate.result.count" arguments="${results.size()}" /></td>
						</tr>
						<tr>
							<th class="select" scope="col"><input type="checkbox" id="selectall"/></th>
							<th class="certno" scope="col"><spring:message code="web.certificateno" /></th>
							<th class="inst" scope="col"><spring:message code="web.instrumentdesc" /></th>
							<th class="plantno" scope="col"><spring:message code="web.plantno" /></th>
							<th class="serial" scope="col"><spring:message code="web.serialno" /></th>
							<th class="caldate" scope="col"><spring:message code="web.caldate" /></th>
							<th class="issue" scope="col"><spring:message code="web.issuedate" /></th>										
							<th class="jobno" scope="col">
                                <c:choose>
                                    <c:when test="${deviation}">
                                        <spring:message code="instrument.customeracceptancecriteria"/> / <spring:message code="certificate.deviation"/>
                                    </c:when>
                                    <c:otherwise>
                                        <spring:message code="web.jobandjobitemno" />
                                    </c:otherwise>
                                </c:choose>
                            </th>
							<c:if test="${calstatus}">
								<th class="status" scope="col"><spring:message code="viewinstrument.calverificationstatus"/></th>
							</c:if>
							<c:if test="${certvalidation}">
								<th class="validation" scope="col"><spring:message code="web.certificates.accepted" /></th>
							</c:if>
						</tr>
					</thead>
				</table>
				
				<c:if test="${results.size() < 1}">
					<table class="default" summary="This table displays a message if no certs are returned">
						<tbody>
							<tr>
								<td class="bold text-center">
									<spring:message code="web.noresults" />
								</td>
							</tr>
						</tbody>
					</table>
				</c:if>
				
				<c:if test="${results.size() >= 1}">
					<c:forEach var="certWrapper" items="${results}">
						<c:set var="cert" value="${certWrapper.cert}" />
						<c:set var="canCertVal" value="${certWrapper.createCertVal}" />
						
						<table class="default certResults" summary="This table displays certs returned">
							<tbody>
								<tr>
									<c:choose>
										<c:when test="${cert.status eq 'SIGNED' || cert.status eq 'SIGNING_NOT_SUPPORTED'}">
											<td class="select">
												<input type="checkbox" name="certSelector" id="C${cert.certid}" />
												<input type="hidden" name="nameSelector" value="${cert.certno}" />
											</td>
										</c:when>
										<c:otherwise>
											<td class="select">
												<input type="checkbox" name="" disabled="disabled" />
											</td>
										</c:otherwise>
										</c:choose>
                                    <td class="certno">
                                        <a href="viewcert.htm?cert=${cert.certid}" class="mainlink">${cert.certno}</a>&nbsp;
                                        <web:certificateClass certificateClass="${cert.certClass}"/>
										<span style="align-content: center"><c:out value="${cert.thirdCertNo}"/></span>
                                    </td>
									<td class="inst">
										<c:choose>
											<c:when test="${cert.links.size() > 0}">
												<c:forEach var="cl" items="${cert.links}">
													<div>
														<web:instrumentLinkWeb displayCalTimescale="${true}" instrument="${cl.jobItem.inst}" />
													</div>
												</c:forEach>
											</c:when>
											<c:when test="${cert.instCertLinks.size() > 0}">														
												<c:forEach var="cl" items="${cert.instCertLinks}">
													<div>
														<web:instrumentLinkWeb displayCalTimescale="${true}" instrument="${cl.inst}" />
													</div>
												</c:forEach>
											</c:when>
										</c:choose>	
									</td>
									<td class="plantno">
										<c:choose>
											<c:when test="${cert.links.size() > 0}">
												<c:forEach var="cl" items="${cert.links}">
													<div>
														<c:out value="${cl.jobItem.inst.plantno}"/>
													</div>
												</c:forEach>
											</c:when>
											<c:when test="${cert.instCertLinks.size() > 0}">														
												<c:forEach var="cl" items="${cert.instCertLinks}">
													<div>
														<c:out value="${cl.inst.plantno}"/>
													</div>
												</c:forEach>
											</c:when>
										</c:choose>	
									</td>
									<td class="serial">
										<c:choose>
											<c:when test="${cert.links.size() > 0}">
												<c:forEach var="cl" items="${cert.links}">
													<div>
														<c:out value="${cl.jobItem.inst.serialno}"/>
													</div>
												</c:forEach>
											</c:when>
											<c:when test="${cert.instCertLinks.size() > 0}">														
												<c:forEach var="cl" items="${cert.instCertLinks}">
													<div>
														<c:out value="${cl.inst.serialno}"/>
													</div>
												</c:forEach>
											</c:when>
										</c:choose>	
									</td>

									<td class="caldate"><fmt:formatDate value="${cert.calDate}" type="DATE" dateStyle="SHORT"/></td>
									<td class="issue"><fmt:formatDate value="${cert.certDate}" type="DATE" dateStyle="SHORT"/></td>
                                    <td class="jobno">
                                        <c:choose>
                                            <c:when test="${deviation}">
                                                <c:choose>
                                                    <c:when test="${cert.links.size() > 0}">
                                                        <c:forEach var="cl" items="${cert.links}">
                                                            <div>
                                                                <c:if test="${cl.jobItem.inst.instrumentComplementaryField != null}">
                                                                    <c:out value="${cl.jobItem.inst.instrumentComplementaryField.customerAcceptanceCriteria}"/> /
																	<fmt:formatNumber value="${cert.deviation}" maxFractionDigits="5"/>
                                                                </c:if>
                                                            </div>
                                                        </c:forEach>
                                                    </c:when>
                                                    <c:when test="${cert.instCertLinks.size() > 0}">
                                                        <c:forEach var="cl" items="${cert.instCertLinks}">
                                                            <div>
                                                                <c:if test="${cl.inst.instrumentComplementaryField != null}">
                                                                    <c:out value="${cl.inst.instrumentComplementaryField.customerAcceptanceCriteria}"/> /
                                                                    <fmt:formatNumber value="${cert.deviation}" maxFractionDigits="5"/>
                                                                </c:if>
                                                            </div>
                                                        </c:forEach>
                                                    </c:when>
                                                </c:choose>

                                            </c:when>
                                            <c:otherwise>
                                                <c:if test="${cert.links.size() > 0}">
                                                    <c:forEach var="cl" items="${cert.links}">
                                                        <div>
                                                            <a href="viewjob.htm?jobid=${cl.jobItem.job.jobid}" class="mainlink"><c:out value="${cl.jobItem.job.jobno}"/></a>
                                                            - <spring:message code="web.item" />
                                                                <c:out value="${cl.jobItem.itemNo}"/>
                                                        </div>
                                                    </c:forEach>
                                                </c:if>
                                            </c:otherwise>
                                        </c:choose>
									</td>
									<c:if test="${calstatus}">
										<td class="status">
											<c:choose>
												<c:when test="${cert.calibrationVerificationStatus != null}">
													<c:out value="${cert.calibrationVerificationStatus.getName()}"/>
												</c:when>
												<c:otherwise>
													<spring:message code="calibrationverification.notapplicable"/>
												</c:otherwise>
											</c:choose>
										</td>
									</c:if>
                                    <web:certificateValidationLinks cert="${cert}" validationrRequired="${certvalidation}" validationAllowed="${canCertVal}"/>
								</tr>
							</tbody>
						</table>
					</c:forEach>								
				</c:if>
				<table class="default tlast" summary="">
					<tfoot>
						<tr>
							<td>&nbsp;</td>
						</tr>
					</tfoot>
				</table>
				<t:showResultsPagination rs="${form.rs}" pageNoId="" />
			</form:form>								


	  	</div>

		<web:certificateValidationDialogs/>

	</jsp:body>
</t:webTemplate>
