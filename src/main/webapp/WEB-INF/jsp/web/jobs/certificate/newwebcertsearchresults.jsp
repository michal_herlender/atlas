<%-- File name: /trescal/core/web/jobs/certificate/webcertsearchresults.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="web" tagdir="/WEB-INF/tags/web" %>

<t:webTemplate idheader="cert">
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='../script/web/jobs/certificate/WebCertSearchResults.js'></script>
		<web:convertBusinessDetails businessDetails="${businessDetails}" />
		<web:convertContactDetails currentContact="${currentContact}" />
	</jsp:attribute>
	<jsp:body>
		<div class="mainBoxBodyContent">
			<c:set var="results" value="${form.rs.results}" />
			<div class="text-center" style="padding-bottom: 10px">
				<button class="float-left" id="searchagain"><spring:message code="web.search.again" /></button>
				<button class="float-left marg-left-md" id="validatestatuses" disabled onclick="validateCertificateStatuses(); return false;"><spring:message code="save" /></button>
				<table class="float-right" style="font-weight: bold">
					<tr>
						<td><span id="totalselected">0</span></td>
						<td><spring:message code="web.certificatesselected"/></td>
						<c:if test="${certvalidation}">
<%-- 							<td><button id="acceptcerts" class="selectedcertaction" disabled="true"><spring:message code="web.acceptreject.selectedcerts"/></button></td> --%>
							<td><button id="downloadacceptdocs" class="selectedcertaction" disabled="true"><spring:message code="web.download.selectedacceptancedocs"/></button></td>
						</c:if>
						<td><button id="downloadcerts" class="selectedcertaction" disabled="true"><spring:message code="web.download.selectedcerts"/></button></td>
						<td><button id="clearselections" class="selectedcertaction" disabled="true"><spring:message code="web.accept.clearselections"/></button></td>
					</tr>
				</table>
				<div class="clear"></div>
			</div>
				
			<form:form id="form" action="" method="post" modelAttribute="form">
				<form:hidden path="resultsPerPage"/>
				<form:hidden path="pageNo"/>
				
				<form:hidden path="coid" />							
				<form:hidden path="subdivid" />
				<form:hidden path="addrid" />
				<form:hidden path="personid" />
				
				<form:hidden path="certNo" />
				<form:hidden path="status" />
				<form:hidden path="calTypeId" />
				<form:hidden path="mfr" />
				<form:hidden path="mfrid" />
				<form:hidden path="model" />
				<form:hidden path="modelid" />
				<form:hidden path="desc" />
				<form:hidden path="descid" />
				
				<form:hidden path="issueDate1" />
				<form:hidden path="issueDate2" />
				<form:hidden path="calDate1" />
				<form:hidden path="calDate2" />
				
				<form:hidden path="issueDateBetween" />
				<form:hidden path="calDateBetween" />
				
				<form:hidden path="tpId" />
				<form:hidden path="tpCertNo" />
				<form:hidden path="bc" />
				<form:hidden path="jobNo" />
				<form:hidden path="serialNo" />
				<form:hidden path="plantNo" />
				<form:hidden path="initialResultsPage"/>
				<form:hidden path="unAcceptedCertsOnly"/>

                <form:hidden path="acceptDateBetween"/>
                <form:hidden path="acceptDate1"/>
                <form:hidden path="acceptDate2"/>
                
                <form:hidden path="jobItemCompletionDateBetween"/>
                <form:hidden path="jobItemDateComplete1"/>
                <form:hidden path="jobItemDateComplete2"/>
				
				<t:showResultsPagination rs="${form.rs}" pageNoId="" />
											
				<table class="default tfirst certResults" summary="This table lists all certificate search headings">
					<thead>
						<tr>
							<td colspan="12"><spring:message code="certificate.result.count" arguments="${results.size()}" /></td>
						</tr>
						<tr>
							<th class="validationbutton" scope="col">#</th>
							<th class="select" scope="col"><input type="checkbox" id="selectall"/></th>
							<th class="certfile" scope="col"><spring:message code="viewprebooking.downloadfile" /></th>
							<th class="certno" scope="col"><spring:message code="web.certificateno" /></th>
							<th class="inst" scope="col"><spring:message code="web.instrumentdesc" /></th>
							<th class="plantno" scope="col"><spring:message code="web.plantno" /></th>
							<th class="serial" scope="col"><spring:message code="web.serialno" /></th>
							<th class="caldate" scope="col"><spring:message code="web.caldate" /></th>
							<th class="issue" scope="col"><spring:message code="web.issuedate" /></th>										
							<th class="jobno" scope="col">
                                <c:choose>
                                    <c:when test="${deviation}">
                                        <spring:message code="instrument.customeracceptancecriteria"/> / <spring:message code="certificate.deviation"/>
                                    </c:when>
                                    <c:otherwise>
                                        <spring:message code="web.jobandjobitemno" />
                                    </c:otherwise>
                                </c:choose>
                            </th>
							<c:if test="${calstatus}">
								<th class="status" scope="col"><spring:message code="viewinstrument.calverificationstatus"/></th>
							</c:if>
							<c:if test="${certvalidation}">
								<th class="validation" scope="col"><spring:message code="web.certificates.accepted" /></th>
							</c:if>
						</tr>
					</thead>
				</table>
				
				<c:if test="${results.size() < 1}">
					<table class="default" summary="This table displays a message if no certs are returned">
						<tbody>
							<tr>
								<td class="bold text-center">
									<spring:message code="web.noresults" />
								</td>
							</tr>
						</tbody>
					</table>
				</c:if>
				
				<c:if test="${results.size() >= 1}">
					<c:forEach var="certWrapper" items="${results}">
						<c:set var="certDto" value="${certWrapper.certDto}" />
						<c:set var="canCertVal" value="${certWrapper.createCertVal}" />
						
						<table class="default certResults" summary="This table displays certs returned">
							<tbody>
								<tr>
									<c:choose>
										<c:when test="${certDto.certStatus eq 'SIGNED' || certDto.certStatus eq 'SIGNING_NOT_SUPPORTED'}">
											<td class="validationbutton">
												<c:if test="${certvalidation}">
													<img src="../img/icons/add.png" id="showButton_${certDto.certId}" 
													class="show" width="16" height="16" alt="" title="" 
													onclick=" showCertificateValidation(${certDto.certId}, this); return false; " />
												</c:if>
											</td>
											<td class="select">
												<input type="checkbox" name="certSelector" id="C${certDto.certId}" />
												<input type="hidden" name="nameSelector" value="${certDto.certno}" />
											</td>
										</c:when>
										<c:otherwise>
											<td class="validationbutton">
												<img src="../img/icons/add.png" id="showButton_${certDto.certId}" class="show" 
												width="16" height="16" alt="" title=""/>
											</td>
											<td class="select">
												<input type="checkbox" name="" disabled="disabled" />
											</td>
										</c:otherwise>
										</c:choose>
									<td class="certfile">
										<c:choose>
											<c:when test="${not empty certDto.certEncryptedAbsolutePath}">
												<c:url var="downloadURL" value="downloadfile.htm"><c:param name="file" value="${certDto.certEncryptedAbsolutePath}" /></c:url>
							       				<a href='${downloadURL}' class="mainlink" target="_blank">
													<img src="../img/doctypes/pdf.png" width="16" height="16" class="img_marg_bot" />
												</a><br/>
											</c:when>
											<c:otherwise>
												---
											</c:otherwise>
										</c:choose>
									</td>
                                    <td class="certno">
                                        <a href="viewcert.htm?cert=${certDto.certId}" class="mainlink">${certDto.certno}</a>&nbsp;
                                        <web:certificateClass certificateClass="${certDto.certClass}"/>
										<span style="align-content: center"><c:out value="${certDto.thirdCertNo}"/></span>
                                    </td>
									<td class="inst">
										<c:choose>
											<c:when test="${certDto.jobitemLinkDtos.size() > 0}">
												<c:forEach var="jil" items="${certDto.jobitemLinkDtos}">
													<div>
														<web:instrumentLinkProjectionWeb displayCalTimescale="${true}" instrumentDto="${jil.instDto}" />
													</div>
												</c:forEach>
											</c:when>
											<c:when test="${certDto.instrumentLinkDtos.size() > 0}">
												<c:forEach var="instDto" items="${certDto.instrumentLinkDtos}">
													<div>
														<web:instrumentLinkProjectionWeb displayCalTimescale="${true}" instrumentDto="${instDto}" />
													</div>
												</c:forEach>
											</c:when>
										</c:choose>	
									</td>
									<td class="plantno">
										<c:choose>
											<c:when test="${certDto.jobitemLinkDtos.size() > 0}">
												<c:forEach var="jil" items="${certDto.jobitemLinkDtos}">
													<div>
														<c:out value="${jil.instDto.plantno}"/>
													</div>
												</c:forEach>
											</c:when>
											<c:when test="${certDto.instrumentLinkDtos.size() > 0}">														
												<c:forEach var="instDto" items="${certDto.instrumentLinkDtos}">
													<div>
														<c:out value="${instDto.plantno}"/>
													</div>
												</c:forEach>
											</c:when>
										</c:choose>	
									</td>
									<td class="serial">
										<c:choose>
											<c:when test="${certDto.jobitemLinkDtos.size() > 0}">
												<c:forEach var="jil" items="${certDto.jobitemLinkDtos}">
													<div>
														<c:out value="${jil.instDto.serialno}"/>
													</div>
												</c:forEach>
											</c:when>
											<c:when test="${certDto.instrumentLinkDtos.size() > 0}">														
												<c:forEach var="instDto" items="${certDto.instrumentLinkDtos}">
													<div>
														<c:out value="${instDto.serialno}"/>
													</div>
												</c:forEach>
											</c:when>
										</c:choose>	
									</td>

									<td class="caldate"><fmt:formatDate value="${certDto.calDate}" type="DATE" dateStyle="SHORT"/></td>
									<td class="issue"><fmt:formatDate value="${certDto.certDate}" type="DATE" dateStyle="SHORT"/></td>
                                    <td class="jobno">
                                        <c:choose>
                                            <c:when test="${deviation}">
                                                <c:choose>
                                                    <c:when test="${certDto.jobitemLinkDtos.size() > 0}">
                                                        <c:forEach var="jil" items="${certDto.jobitemLinkDtos}">
                                                            <div>
                                                                <c:if test="${jil.instDto.complementaryFieldCustomerAcceptanceCriteria != null}">
                                                                    <c:out value="${jil.instDto.complementaryFieldCustomerAcceptanceCriteria}"/> /
																	<fmt:formatNumber value="${certDto.deviation}" maxFractionDigits="5"/>
                                                                </c:if>
                                                            </div>
                                                        </c:forEach>
                                                    </c:when>
                                                    <c:when test="${certDto.instrumentLinkDtos.size() > 0}">
                                                        <c:forEach var="instDto" items="${certDto.instrumentLinkDtos}">
                                                            <div>
                                                                <c:if test="${instDto.complementaryFieldCustomerAcceptanceCriteria != null}">
                                                                    <c:out value="${instDto.complementaryFieldCustomerAcceptanceCriteria}"/> /
                                                                    <fmt:formatNumber value="${certDto.deviation}" maxFractionDigits="5"/>
                                                                </c:if>
                                                            </div>
                                                        </c:forEach>
                                                    </c:when>
                                                </c:choose>

                                            </c:when>
                                            <c:otherwise>
                                                <c:if test="${certDto.jobitemLinkDtos.size() > 0}">
                                                    <c:forEach var="jil" items="${certDto.jobitemLinkDtos}">
                                                        <div>
                                                            <a href="viewjob.htm?jobid=${jil.jobId}" class="mainlink"><c:out value="${jil.jobNo}"/></a>
                                                            - <spring:message code="web.item" />
                                                                <c:out value="${jil.itemNo}"/>
                                                        </div>
                                                    </c:forEach>
                                                </c:if>
                                            </c:otherwise>
                                        </c:choose>
									</td>
									<c:if test="${calstatus}">
										<td class="status">
											<c:choose>
												<c:when test="${certDto.calibrationVerificationStatus != null}">
													<c:out value="${certDto.calibrationVerificationStatus.getName()}"/>
												</c:when>
												<c:otherwise>
													<spring:message code="calibrationverification.notapplicable"/>
												</c:otherwise>
											</c:choose>
										</td>
									</c:if>
                                    <web:certificateProjectionValidationLinks certStatus="${certDto.certStatus}" certValidationId="${certDto.validationId}"
                                    certValidationStatus="${certDto.validationStatus}" certValidationDateValidated="${certDto.validationDate}"
                                    certId="${certDto.certId}" certNo="${certDto.certno}" validationrRequired="${certvalidation}" 
                                    validationAllowed="${canCertVal}"/>
								</tr>
								<tr id="vaidationRow_${certDto.certId}" class="hid">
									<td colspan="${calstatus and certvalidation ? 12:calstatus or certvalidation ?11:10}">
									
										<table class="valid">
											<tr>
									            <td class="lab ">
									                <label class="float-right"><b><spring:message code="editinstr.calibrationinstructions"/> :</b></label>
									            </td>
									            <td class="val">
									                <span id="validationdialog-calinstructions_${ certDto.certId }" class="float-left"></span>
									            </td>
									            <td class="lab">
									                <label class="float-right"><b><spring:message code="status"/> :</b></label>
									            </td>
									            <td class="val">
									            	<select id="certValidationStatus_${ certDto.certId }" class="float-left"
									            	 onclick="$j('#certRequiredStatus_${ certDto.certId }').removeClass('vis').addClass('hid');">
			                                            <option value="" selected>---</option>
			                                            <c:forEach items="${certValidationStatuses}" var="certValidationStatus">
			                                                <option value="${certValidationStatus.key}" ${ not empty certDto.validationStatus and certDto.validationStatus.name() eq certValidationStatus.key ? 'selected':'' }><c:out value="${certValidationStatus.value}"/></option>
			                                            </c:forEach>
			                                        </select>
			                                        <span style="color:red;" class="hid " id="certRequiredStatus_${ certDto.certId }"><spring:message code="repairinspection.validator.mandatory" /></span>
									            </td>
								            </tr>
								            <tr>
									            <td class="lab">
								                	<label class="float-right"><b><spring:message code="instrument.customeracceptancecriteria"/> :</b></label>
								                </td>
								                <td class="val">
								                	<span id="validationdialog-customeracceptancecriteria_${ certDto.certId }" class="float-left"></span>
								                </td>
								                <td class="lab">
								                	<label for="certValidationDate_${certDto.certId}" class="float-right"><b><spring:message code="certificationvalidation.date"/> :</b></label>
									            </td>
								                <td class="val">
								                	<input type="text" style="width: 0; height: 0; top: -100px; position: absolute;"/>
									                <input type="date" id="certValidationDate_${certDto.certId}" size="12" style="width: 90px;" value="${certDto.validationDate}"
									                class="float-left" onclick="$j('#certRequiredDate_${ certDto.certId }').removeClass('vis').addClass('hid');"/>
									                <span style="color:red;" class="hid " id="certRequiredDate_${ certDto.certId }"><spring:message code="repairinspection.validator.mandatory" /></span>
								                </td>
								            </tr>
								            <tr>
								            	<td class="lab">
								                	<label class="float-right"><b><spring:message code="certificate.deviation"/> :</b></label>
								                </td>
								                <td class="val">
								                	<span id="validationdialog-deviation_${ certDto.certId }" class="float-left"></span>
								                </td>
								                <td class="lab">
								                	<label for="certValidationNote_${ certDto.certId }" class="float-right"><b><spring:message code="certificationvalidation.note"/> :</b></label>
								                </td>
								                <td class="val">
								                	<textarea id="certValidationNote_${ certDto.certId }" rows="2" cols="30" class="float-left">${ certDto.validationNote }</textarea>
								                </td>
								            </tr>
								        </table>
									
									</td>
								</tr>
							</tbody>
						</table>
					</c:forEach>								
				</c:if>
				<table class="default tlast" summary="">
					<tfoot>
						<tr>
							<td>&nbsp;</td>
						</tr>
					</tfoot>
				</table>
				<t:showResultsPagination rs="${form.rs}" pageNoId="" />
			</form:form>								


	  	</div>

		<web:certificateValidationDialogs/>

	</jsp:body>
</t:webTemplate>
