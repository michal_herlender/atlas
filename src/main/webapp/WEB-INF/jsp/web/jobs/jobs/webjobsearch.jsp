<%-- File name: /trescal\web\jobs\jobs\webjobsearch.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="instrument" tagdir="/WEB-INF/tags/instrument"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:webTemplate idheader="job">

    <jsp:attribute name="scriptPart">
		<script type='text/javascript' src='../script/web/jobs/job/WebJobSearch.js'></script>
    </jsp:attribute>
    <jsp:body>
        <!-- main box body content div -->
		<div class="mainBoxBodyContent">
	
		<!-- error section displayed when form submission fails -->
		<form:errors path="form.*">
        			<div class="warningBox1">
                			<div class="warningBox2">
                    	 	  <div class="warningBox3">
                    	   		<h4 class="center-0 attention"><spring:message code="web.search.error"/></h4>
                            	<c:forEach var="e" items="${messages}">
                               	<div class="center attention"><c:out value="${e}"/></div>
                            	</c:forEach>
                     	     </div>
                 			</div>
           			</div>
       	</form:errors>
       	   		    
		<!-- end error section -->
		
		<!-- div displaying search heading info -->
		<div>
			<!-- left floated column -->							
			<div class="columnLeft-1">
				<h3 class="text-center"><spring:message code="job.search.title"/></h3>
				<div class="pad-left">
					<spring:message code="job.search.info"/>
				</div>
			</div>

			<div class="clear"></div>
		</div>
		<!-- end of search heading info -->

		<!-- search form elements -->
		<fieldset>
																	
			<form:form method="post" action="" id="jobsearchform" onsubmit=" loadScript.applySearchingImage($j(this).find('input[type=\'submit\']').parent()); " modelAttribute="form">

				<!-- left floated column -->
				<div class="columnLeft-2">

					<ol>									
						<li>
							<form:label path="serialno"><spring:message code="web.serialno.label"/></form:label>											
							<form:input type="text" path="serialno" value="${form.serialno}" tabindex="" />
						</li>
						<li>
							<form:label path="plantno"><spring:message code="web.plantno.label"/></form:label>
							<form:input type="text"  path="plantno" name="plantno" value="${form.plantno}" tabindex="" />
						</li>
						<li>
							<form:label path="plantid"><c:out value="${businessDetails.docCompany}"/>&nbsp;<spring:message code="web.barcode.label"/></form:label>											
							<form:input type="text" path="plantid" name="plantid" value="${form.plantid}" tabindex="" />
						</li>
						<li>
							<form:label path="jobno"><c:out value="${businessDetails.docCompany}"/>&nbsp;<spring:message code="web.jobno.label"/></form:label>											
							<form:input type="text" path="jobno" name="jobno" value="${form.jobno}" tabindex="" />
						</li>
						<li>
							<form:label path="clientRef"><spring:message code="web.clientref.label"/></form:label>											
							<form:input type="text" path="clientRef" name="clientRef" value="${form.clientRef}" tabindex="" />
						</li>										
						<li>
							<form:label path="purOrder"><spring:message code="web.purchaseorderno.label"/></form:label>											
							<form:input type="text" path="purOrder" name="purOrder" value="${form.purOrder}" tabindex="" />
						</li>
						<li>
							<label for="submit">&nbsp;</label>
							<input type="submit" name="submit" value="<spring:message code="web.search"/>" tabindex="" />
						</li>							
					</ol>
			
				</div>
				<!-- end of left column -->
				
				<!-- right floated column -->
				<div class="columnRight-2">
				
					<ol>
						<li>											
							<form:label path="subdivid"><spring:message code="web.subdivision.label"/></form:label>
							<c:choose>
								<c:when test="${userReadRule == 'COMPANY'}">
									<c:choose>
										<c:when test="${subdivs.size() > 1}">									
										<form:select id="subdivid" path="subdivid" onchange=" getActiveSubdivAddrsAndConts(this.value, $j(this).parent().next().find('select'), $j(this).parent().next().next().find('select'), true, ''); return false; ">											
										<option value="" selected="selected"><spring:message code="web.search.allsubdivisions"/></option>
										<c:forEach var="sub" items="${subdivs}">
											<option value="${sub.subdivid}"><c:out value="${sub.subname}"/></option>
										</c:forEach>
										</form:select>
										</c:when>
										<c:otherwise>
										<span><instrument:subdivFilter subdiv="${userContact.sub}" /></span>
										<input type="hidden" name="subdivid" value="${userContact.sub.subdivid}" />
										</c:otherwise>
									</c:choose>
								</c:when>
								<c:otherwise>
								<span><instrument:subdivFilter subdiv="${userContact.sub}" /></span>
								<input type="hidden" name="subdivid" value="${userContact.sub.subdivid}" />
								</c:otherwise>
							</c:choose>
							<form:errors path="subdivid" cssClass="errorabsolute">
								<span class="error"><img src="../img/web/asterisk.png" width="16" height="16" /></span>
							</form:errors>	
						</li>
						<li>
							<form:label path="addrid"><spring:message code="web.address.label"/></form:label>
							<c:choose><c:when test="${userReadRule == 'COMPANY' || userReadRule == 'SUBDIV'}">
								<form:select path="addrid">
									<option value="" selected="selected"><spring:message code="web.search.alladdresses"/></option>
									<c:forEach var="a" items="${addresses}">
										<c:if test="${a.addr1 != null && a.addr2 != ''}">
											<option value="${a.addrid}">${a.addr1}, ${a.addr2}</option>
										</c:if>
									</c:forEach>
								</form:select>
							</c:when>
							<c:otherwise>
								<span>${userContact.defAddress.addr1}, ${userContact.defAddress.addr2}</span>
								<input type="hidden" name="addrid" value="${userContact.defAddress.addrid}" />
							</c:otherwise>
							</c:choose>
							<form:errors path="addrid" cssClass="errorabsolute">
								<span class="error"><img src="../img/web/asterisk.png" width="16" height="16" /></span>
							</form:errors>
						</li>
						<li>
							<form:label path="personid"><spring:message code="web.contactname.label"/></form:label>
							<c:choose><c:when test="${userReadRule == 'COMPANY' || userReadRule == 'SUBDIV' || userReadRule == 'ADDRESS'}">
								<form:select id="personid" path="personid">
									<option value="" selected="selected"><spring:message code="web.search.allcontacts"/></option>
									<c:forEach var="c" items="${contacts}">
										<form:option value="${c.personid}"><c:out value="${c.reversedName}"/></form:option>
									</c:forEach>
								</form:select>
							</c:when>
							<c:otherwise>
								<span><c:out value="${userContact.name}"/></span>
								<input type="hidden" name="personid" value="${userContact.personid}" />
							</c:otherwise>
							</c:choose>
							<form:errors path="personid" cssClass="errorabsolute">
								<span class="error"><img src="../iimg/web/asterisk.png" width="16" height="16" /></span>
							</form:errors>
						</li>
						<li>
							<form:label path="mfr"><spring:message code="web.manufacturer.label"/></form:label>
							<div class="ui-widget">
								<form:input id="mfr" path="mfr" type="text" value="${form.mfr}"/>
							</div>
							<input id="mfrid" name="mfrid" type="hidden" value="${form.mfrid}"/>
						</li>
						<li>
							<form:label path="model"><spring:message code="web.model.label"/></form:label>
							<form:input path= "model" type="text" name="model" value="${form.model}" />
						</li>
						<li>
							<form:label path="desc"><spring:message code="web.description.label"/></form:label>
							<div class="ui-widget">
								<form:input  path ="desc" id="desc" name="desc" type="text" value="${form.desc}"/>
							</div>
							<input id="descid" name="descid" type="hidden" value="${form.descid}"/>
						</li>
						<li class="dateInputs">
								<form:select path="bookedInDateBetween" id="bookedInDateBetween" tabindex="15" onchange=" changeDateOption(this.value, 'bookedInDateSpan'); return false; ">
									<option value="false" <c:if test="${form.bookedInDateBetween == false}"> selected </c:if>><spring:message code="web.on"/></option>
									<option value="true" <c:if test="${form.bookedInDateBetween == true}"> selected </c:if>><spring:message code="web.between"/></option>
								</form:select>
								<form:label path="bookedInDate1"><spring:message code="web.bookeddate.label"/></form:label>
								<form:input type="date" path="bookedInDate1" id="bookedInDate1"  value="${form.bookedInDate1}" tabindex="16" />
							<span id="bookedInDateSpan" class="${form.bookedInDateBetween ? 'vis' : 'hid'}">
								<spring:message code="web.and"/>&nbsp;						
									<form:input type="date" path="bookedInDate2" id="bookedInDate2" value="${form.bookedInDate2}"  tabindex="17" />
							</span>
						</li>
						<li class="dateInputs">
							<form:select name="completedDateBetween" path ="completedDateBetween" id="completedDateBetween" onchange=" changeDateOption(this.value, 'completedDateSpan'); return false; " tabindex="18">
								<option value="false" <c:if test="${form.completedDateBetween == false}"> selected </c:if>><spring:message code="web.on"/></option>
								<option value="true" <c:if test="${form.completedDateBetween == true}"> selected </c:if>><spring:message code="web.between"/></option>
							</form:select>
							<form:label path="completedDate1"><spring:message code="web.completeddate.label"/></form:label>
							<form:input path = "completedDate1" type="date" name="completedDate1" id="completedDate1"  value="${form.completedDate1}"  tabindex="19" />
							<span id="completedDateSpan" class="${form.completedDateBetween ? 'vis' : 'hid' }">
								<spring:message code="web.and"/>&nbsp;
								<form:input type="date" path="completedDate2" id="completedDate2"  value="${sform.completedDate2}"  tabindex="20" />
							</span>
						</li>																				
						<li>
							<form:label path="active"><spring:message code="job.search.activejobs.label"/></form:label>
							<span>
								<form:checkbox path="active" class="checkbox" value="true" tabindex="21" />
							</span>
						</li>
					</ol>
			
			</div>
			<!-- end of right column -->
			
			</form:form>

		</fieldset>
		<!-- end of search form elements -->
	
	<!-- InstanceEndEditable -->

	</div>
	<!-- end of main box body content div -->
    </jsp:body>
</t:webTemplate>