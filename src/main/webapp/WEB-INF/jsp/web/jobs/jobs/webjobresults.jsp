<%-- File name: /trescal/core/web/jobs/job/webjobresults.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="web" tagdir="/WEB-INF/tags/web" %>
<%@ taglib prefix="instrument" tagdir="/WEB-INF/tags/instrument" %>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs" %>

<t:webTemplate idheader="job">
	<jsp:body>
		<!-- main box body content div -->
		<div class="mainBoxBodyContent">
			<div class="text-center">
				<a href="jobs.htm" class="searchagain mainlink" title="<spring:message code="web.search.again" />" alt="<spring:message code="web.search.again" />"><spring:message code="web.search.again" /></a>
				<h3><spring:message code="job.result.title" /></h3>								
			</div>
			<form:form action="" name="searchform" id="searchform" method="post" modelAttribute="form">
				<form:hidden path="coid" />							
				<form:hidden path="subdivid" />
				<form:hidden path="addrid" />
				<form:hidden path="personid" />
				<form:hidden path="serialno" />
				<form:hidden path="plantno" />
				<form:hidden path="jobno" />							
				<form:hidden path="plantid" />
				<form:hidden path="clientRef" />
				<form:hidden path="purOrder" />
				<form:hidden path="mfr" />
				<form:hidden path="model" />
				<form:hidden path="desc" />
				<form:hidden path="modelid" />
				<form:hidden path="bookedInDateBetween" />
				<form:hidden path="completedDateBetween" />
				<form:hidden path="bookedInDate1" />
				<form:hidden path="bookedInDate2" />
				<form:hidden path="completedDate1" />
				<form:hidden path="completedDate2" />
				<form:hidden path="active" />
				<form:hidden path="pageNo" />
				<form:hidden path="resultsPerPage" />
				<form:hidden path="jobItemCount" />
				<form:hidden path="resultsTotal" />
				
				<t:showResultsPagination rs="${form.rs}" pageNoId="" />
				
				<table class="default tfirst jobResults" summary="This table lists all job search headings">
					<thead>
						<tr>
							<td colspan="6">
								<spring:message code="job.result.count" arguments="${form.jobs.size()}, ${form.jobItemCount}"/>
								<spring:message code="web.export.excel" var="submitText"/>
								<input type="submit" name="export" value="${submitText}" class="float-right">
							</td>
						</tr>									
						<tr>
							<th class="jobno" scope="col"><spring:message code="web.jobno" /></th>  
							<th class="address" scope="col"><spring:message code="web.returnaddress" /></th>  
							<th class="contact" scope="col"><spring:message code="web.contact" /></th>  
							<th	class="purchord" scope="col"><spring:message code="web.purchaseorder" /></th>
							<th class="clientref" scope="col"><spring:message code="web.clientref" /></th>
							<th class="datein" scope="col"><spring:message code="web.datein" /></th>
						</tr>
					</thead>
				</table>									
														
				<c:choose>
					<c:when test="${form.jobs.size() < 1}">
						<table class="default" summary="This table displays a message if no jobs are returned">
							<tbody>
								<tr>
									<td colspan="6" class="bold text-center">
										<spring:message code="web.noresults" />
									</td>
								</tr>
							</tbody>
						</table>
					</c:when>
					<c:otherwise>
						<c:forEach var="jb" items="${form.jobs}">	
							<table class="default jobResults" summary="This table displays jobs and items returned">											
								<tbody>								
									<tr>
										<td class="jobno"><instrument:jobnoLinkWeb job="${jb}" /></td>
										<td class="address"><c:out value="${jb.returnTo.addr1}"/> , <c:out value="${jb.returnTo.town}"/></td>
										<td class="contact"><c:out value="${jb.con.name}"/></td>
										<td	class="purchord"><jobs:showJobPos jb="${jb}" /></td>
										<td class="clientref"><c:out value="${jb.clientRef}"/></td>
										<td class="datein"><fmt:formatDate value="${jb.regDate}" type="date" dateStyle="medium"/></td>
									</tr>
									<c:if test="${jb.items.size() > 0}">
										<tr>
											<td colspan="6" style=" padding: 0; ">
												<table class="child-table" border="0" cellpadding="0" cellspacing="0" summary="This table displays any job items that appear on this job">
													<thead>
														<tr>
															<th class="item" scope="col"><spring:message code="web.item" /></th>
															<th class="instr" scope="col"><spring:message code="web.instrumentdesc" /></th>
															<th class="serial" scope="col"><spring:message code="web.serialno" /></th>
															<th class="plant" scope="col"><spring:message code="web.plantno" /></th>
															<th class="caltype" scope="col"><spring:message code="web.caltype" /></th>
															<th class="itempo" scope="col"><spring:message code="web.itempo" /></th>
															<th class="onbehalf" scope="col"><spring:message code="web.behalf" /></th>
															<th class="status" colspan="2" scope="col"><spring:message code="web.status" /></th>
														</tr>
													</thead>
													<tbody>
														<c:forEach var="i" items="${jb.items}">
															<tr style=" background-color: <c:out value="${ji.calType.serviceType.displayColour}"/>; ">
																<td class="text-center"><c:out value="${i.itemNo}"/></td>
																<td><web:instrumentLinkWeb displayCalTimescale="${true}" instrument="${i.inst}" /></td>
																<td><c:out value="${i.inst.serialno}"/></td>
																<td><c:out value="${i.inst.plantno}"/></td>
																<td class="text-center"><t:showTranslationOrDefault translations="${i.calType.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}" /></td>
																<td>
																	<c:forEach var="jipo" items="${i.jobItemPOs}">
																		<c:choose>
																			<c:when test="${not empty jipo.po}">
																				<c:out value="${ipo.po.poNumber}"/><br />
																			</c:when>
																			<c:when test="${not empty jipo.bpo}">
																				<c:out value="${jipo.bpo.poNumber}"/><br />
																			</c:when>
																			<c:otherwise>
																				<spring:message code="web.noitempo" />
																			</c:otherwise>
																		</c:choose>
																	</c:forEach>	
																</td>
																<td>
																	<c:choose>
																		<c:when test="${not empty i.onBehalf}">
																			<c:out value="${i.onBehalf.company.coname}"/><br />
																		</c:when>
																	</c:choose>
																</td>
																<td class="status">
																	<web:jiStatusHistoryIcon ji="${i}" />																																				
																	<t:showTranslationOrDefault translations="${i.state.translations}" defaultLocale="${defaultlocale}" />																		
																</td>																			
															</tr>
														</c:forEach>	
													</tbody>
												</table>
											</td>
										</tr>
									</c:if>
								</tbody>											
							</table>
						</c:forEach>
					</c:otherwise>
				</c:choose>
				<table class="default tlast" summary="">
					<tfoot>
						<tr>
							<td>
								<input type="submit" name="export" value="${submitText}" class="float-right">
							</td>
						</tr>
					</tfoot>
				</table>
				<t:showResultsPagination rs="${form.rs}" pageNoId="" />
			</form:form>
	  	</div>
	</jsp:body>
</t:webTemplate>