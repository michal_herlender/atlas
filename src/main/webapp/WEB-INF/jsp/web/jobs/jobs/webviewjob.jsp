<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="web" tagdir="/WEB-INF/tags/web" %>


<t:webTemplate idheader="cert">
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='../script/web/jobs/job/WebViewJob.js'></script>
	</jsp:attribute>
	<jsp:body>
			<div class="mainBoxBodyContent">
				<div>
					<!-- left floated column -->							
					<div class="columnLeft-1">
						<h3 class="text-center"><spring:message code="job.view.title" arguments="${job.jobno}"/></h3>
						<div class="pad-left">
							<spring:message code="job.view.info"/>
						</div>
					</div>
					<!-- right floated column -->
					<div class="columnRight-1 text-right">
								&nbsp;
					</div>
					<!-- clear floats and restore page flow -->
					<div class="clear"></div>
				</div>
				<!-- end of search heading info -->
					
				<!-- view job elements -->
				<fieldset>
					<!-- left floated column -->
					<div class="columnLeft-4">
						<ol>									
							<li>
								<label><spring:message code="web.company.label"/></label>
								<span>${job.con.sub.comp.coname}</span>
							</li>
							<li>
								<label><spring:message code="web.subdivision.label"/></label>
								
								<span>${job.con.sub}</span>
							</li>
							<li>
								<label><spring:message code="web.contactname.label"/></label>
								<span>${job.con.name}</span>
							</li>
							<li>
								<label><spring:message code="web.location.label"/></label> 			
								<span>${job.returnToLoc.location}</span>
							</li>
							<li>
								<label><spring:message code="web.returnto.label"/></label>												
								<div class="float-left padtop">
									<c:choose>
										<c:when test="${!(job.returnTo.addrid == null && job.returnTo.addrid == '') }" >
											<div><spring:message code="web.noreturnaddressset"/></div>
									  	</c:when>
									  	<c:otherwise>
									  		<t:showCompactFullAddress address="${job.returnTo}" edit="false" copy="false"/>												  		
									 	</c:otherwise>
								 	</c:choose>
								</div>												
								<!-- clear floats and restore page flow -->
								<div class="clear"></div>
							</li>									
							<li>
								<label><spring:message code="web.purchaseorderno.label"/></label>
								<div class="float-left padtop">
									<c:choose>
										<c:when test="${job.POs.size() < 1 && job.bpo == null}">
											<div><spring:message code="web.nopurchaseorder"/></div>
										</c:when>
										<c:otherwise>
											<t:showPurchaseOrder job="${job}"/>											  		
										</c:otherwise>
									</c:choose>
								</div>												
								<!-- clear floats and restore page flow -->
								<div class="clear"></div>
							</li>
							<li>
								<label><spring:message code="web.jobtype.label"/></label>
								<span>${job.type.description}</span>
							</li>							
						</ol>
					</div>
					<!-- end of left column -->
								
					<!-- right floated column -->
					<div class="columnRight-4">
						<ol>
							<li>
								<label><spring:message code="web.regdate.label"/></label>
								<span><fmt:formatDate value="${job.regDate}" dateStyle="medium" type="date" /></span>
							</li>
							<li>
								<label><spring:message code="web.clientref.label"/></label>
								<span>
									<c:choose>
										<c:when test="${job.clientRef.trim().length() > 0}">
											${job.clientRef}
										</c:when>
										<c:otherwise>
											<spring:message code="web.noclientref"/>
										</c:otherwise>
									</c:choose>
								</span>
							</li>
							<li>
								<label><spring:message code="web.status.label"/></label>
								<span>
									${job.js.name}
								</span>
							</li>
							<li>
								<label><spring:message code="web.bookedinat.label"/></label>
								<span>
									${job.bookedInAddr.addr1}, ${job.bookedInAddr.town}
									&nbsp;-&nbsp;&nbsp;${job.bookedInLoc.location}
								</span>
							</li>
							<li>
								<label><spring:message code="web.transportin.label"/></label>
								<span>${job.inOption}</span>
							</li>
							<li>
								<label><spring:message code="web.transportout.label"/></label>
								<span>
									<c:choose>
										<c:when test="${not empty job.returnOption.localizedName}">
											${job.returnOption.localizedName }
										</c:when>
										<c:otherwise>
											<t:showTranslationOrDefault translations="${job.returnOption.method.methodTranslation}" defaultLocale="${defaultlocale}"/>
										</c:otherwise>
									</c:choose>
								</span>
							</li>										
						</ol>
					</div>
					<!-- end of right column -->

				</fieldset>
				<!-- end of view job elements -->
						
				<table class="default tfirst viewJob" summary="This table lists all job items for the current job">
					<thead>
						<tr>
							<td colspan="8">
								<div class="float-left"><spring:message code="job.view.items" arguments="${job.items.size()}"/></div>
							</td>
						</tr>
								<tr>
									<th class="item" scope="col"><spring:message code="web.item"/></th>  
									<th class="instrument" scope="col"><spring:message code="web.instrument"/></th>  
									<th class="serial" scope="col"><spring:message code="web.serialno"/></th>  
									<th	class="plant" scope="col"><spring:message code="web.plantno"/></th> 
									<th class="caltype" scope="col"><spring:message code="web.caltype"/></th>
									<th class="status" scope="col"><spring:message code="web.status"/></th>	
									<th class="delivery" scope="col"><spring:message code="web.deliverymethod"/></th>
									<th class="actions" scope="col"></th>								
								</tr>
							</thead>
						</table>							
						<c:choose>	
							<c:when test="${jobitems.size() < 1 }">
						
								<table class="default" summary="This table displays a message if no job items are present">
									<tbody>
										<tr>
											<td class="bold text-center">
												<spring:message code="job.view.empty"/>
											</td>
										</tr>
									</tbody>
								</table>
							
							</c:when>
							<c:otherwise>	
								<c:forEach var="i" items="${jobitems}">
									
									<table class="default viewJob" summary="This table displays job items on the job">											
										<tbody>
											<tr style=" background-color: ${(fastTrackTurn && (i.jobItem.turn <= fastTrackTurn)) ? i.jobItem.calType.serviceType.displayColourFastTrack : i.jobItem.calType.serviceType.displayColour}; ">
												<td class="item">${i.jobItem.itemNo}</td>
												<td class="instrument">
													<web:instrumentLinkWeb displayCalTimescale="true" instrument="${i.jobItem.inst}"/>
												</td>
												<td class="serial">${i.jobItem.inst.serialno}</td>
												<td	class="plant">${i.jobItem.inst.plantno}</td>
												<td class="text-center caltype"><cwms:besttranslation translations="${i.jobItem.calType.serviceType.shortnameTranslation}"/></td>
												<td class="status">${i.jobItem.state.description}</td>	
												<td class="delivery">
													<c:if test="${i.jobItem.returnOption != null }">
														<cwms:besttranslation translations="${i.jobItem.returnOption.method.methodTranslation}"/>
													</c:if>
													<c:if test="${i.consignmentInfo != null }">
														<br>
														${i.consignmentInfo}													
													</c:if>
												</td>
												<td class="actions">
													<c:if test="${i.showStandardsUsedButton == true }">
														<input type="hidden" value="${i.jobItem.jobItemId}" class="jobItemId"/>
														<button class="showstandards" value="${i.jobItem.jobItemId}"><spring:message code="web.showstandards"/></button>
													</c:if>
												</td>									
											</tr>
										</tbody>
									</table>
								</c:forEach>
							</c:otherwise>
						</c:choose>							
						
						<table class="default tlast" summary="">	
							<tfoot>
								<tr>
									<td>&nbsp;</td>
								</tr>
							</tfoot>
						</table>
					
					<!-- InstanceEndEditable -->
				
			  	</div>
				<!-- end of main box body content div -->
				
				<%-- div to act as anchor for standards used dialog pop up
				dummy hidden input is just there to absorb focus as the dialog 
				looks a bit ugly with the close button highlighted --%>
				<div id="dialog" style="display: none;"><input type="hidden" autofocus="autofocus" /></div>
	</jsp:body>
</t:webTemplate>

