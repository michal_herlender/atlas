<%-- File name: /trescal/core/_PATH1_/_PATH2_/_FILE_.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="instrument" tagdir="/WEB-INF/tags/instrument"%>
<%@ taglib prefix="web" tagdir="/WEB-INF/tags/web" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs" %>

<t:webTemplate idheader="instrumentstatus">

    <jsp:attribute name="scriptPart">
<!--         Section between main_1 and main_2 -->
    </jsp:attribute>
    <jsp:body>
        				<!-- main box body content div -->
				<div class="mainBoxBodyContent">
					
					<!-- InstanceBeginEditable name="content" -->
					
						<c:set var="display" value="{request.getParameter('display')}" />
						<c:if test="${display == 'default'}">
							<c:set var="display" value="${userReadRule}" />
						</c:if>
						
						<!-- div displaying search heading info -->
						<div>
							<!-- left floated column -->							
							<div class="columnLeft-1">
								<h3 class="text-center"><spring:message code="equipementstatus.title" /></h3>
								<div class="pad-left">
									<spring:message code="equipementstatus.info" />
									<c:choose>
										<c:when test="${userReadRule == 'COMPANY'}">
										<spring:message code="equipementstatus.info.company" /></c:when>
										<c:when test="${userReadRule == 'SUBDIV'}">
										<spring:message code="equipementstatus.info.subdivision" /></c:when>
										<c:when test="${userReadRule == 'ADDRESS'}">
										<spring:message code="equipementstatus.info.location" /></c:when>
										<c:otherwise>
										<spring:message code="equipementstatus.info.personal" />
										</c:otherwise>
									</c:choose>
								</div>
							</div>
							<!-- right floated column -->
							<div class="columnRight-1 text-right">
<!-- ##								<img src="../img/web/job.png" width="120" height="94" alt="" class="pad-5" /> -->
							</div>
							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
						</div>
						<!-- end of search heading info -->
																		
						<div class="pad-top">						
							<!-- sub navigation -->						
							<div id="subnav">
								<dl>
									
									<dt><a href="../web/equipmentstatus.htm?display=ADDRESS" <c:if test="${display == 'ADDRESS'}"> class="selected" </c:if>> <spring:message code="equipementstatus.navig.location"/></a></dt>
									<dt><a href="../web/equipmentstatus.htm?display=CONTACT" <c:if test="${display == 'CONTACT'}"> class="selected" </c:if>> <spring:message code="equipementstatus.navig.personal"/></a></dt>
									<c:if test="${userReadRule == 'CONTACT'}">
									</c:if>
									<c:if test="${userReadRule == 'COMPANY' || userReadRule == 'SUBDIV'}">
										<dt><a href="../web/equipmentstatus.htm?display=SUBDIV" <c:if test="${display == 'SUBDIV'}"> class="selected" </c:if>> <spring:message code="equipementstatus.navig.subdivision"/></a></dt>
									</c:if>
									<c:if test="${userReadRule == 'company'}">
										<dt><a href="../web/equipmentstatus.htm?display=COMPANY" <c:if test="${display == 'COMPANY'}"> class="selected" </c:if>> <spring:message code="equipementstatus.navig.company"/></a></dt>
									</c:if>
								</dl>
							</div>
							<!-- end of sub navigation -->
						</div>					
						
						<form:form action="" name="searchform" id="searchform" method="post" modelAttribute="form">
								
							<form:hidden path="coid"/>							
							<form:hidden path="subdivid"/>
							<form:hidden path="addrid"/>
							<form:hidden path="personid"/>
							
							<form:hidden path="serialno"/>
							<form:hidden path="plantno"/>
							<form:hidden path="jobno"/>							
							<form:hidden path="plantid"/>
							<form:hidden path="clientRef"/>
							<form:hidden path="purOrder"/>
							<form:hidden path="mfr"/>
							<form:hidden path="model"/>
							<form:hidden path="desc"/>
							<form:hidden path="modelid"/>
							
							<form:hidden path="active"/>
							<form:hidden path="pageNo"/>
							<form:hidden path="resultsPerPage"/>
							
							<c:if test="${form.jobs.size() > 0}"> <t:showResultsPagination rs="${form.rs}" pageNoId="" /></c:if> 
							
							<table class="default tfirst jobResults" summary="This table lists all job search headings">
								<thead>
									<tr class="toprow">
										<td colspan="6">
											<spring:message code="equipementstatus.count" arguments="${form.jobs.size()},${form.jobItemCount}"/>
										</td>
									</tr>									
									<tr>
										<th class="jobno" scope="col"><spring:message code="web.jobno" /></th>  
										<th class="address" scope="col"><spring:message code="web.returnaddress" /></th>  
										<th class="contact" scope="col"><spring:message code="web.contact" /></th>  
										<th	class="purchord" scope="col"><spring:message code="web.purchaseorder" /></th>
										<th class="clientref" scope="col"><spring:message code="web.clientref" /></th>
										<th class="datein" scope="col"><spring:message code="web.datein" /></th>
									</tr>
								</thead>
							</table>									
																	
							<c:set var="rowcount" value="0"/>
							<c:choose>
								<c:when test="${form.jobs.size() < 1}">
							
								<table class="default" summary="This table displays a message if no jobs are returned">
									<tbody>
										<tr>
											<td colspan="6" class="bold text-center">
												<spring:message code="web.noresults" />
											</td>
										</tr>
									</tbody>
								</table>
								
								</c:when>
								<c:otherwise>
													
								<c:forEach var="jb" items="${form.jobs}">	
																
									<table class="default jobResults" summary="This table displays jobs and items returned">											
										<tbody>								
											<tr>
												<td class="jobno"><instrument:jobnoLinkWeb job="${jb}" /></td>
												<td class="address"><c:out value="${jb.returnTo.addr1}"/>,<c:out value="${jb.returnTo.town}"/></td>
												<td class="contact"><c:out value="${jb.con.name}"/></td>
												<td	class="purchord"><jobs:showJobPos jb="${jb}" />
														
												</td>
												<td class="clientref"><c:out value="${jb.clientRef}"/></td>
												<td class="datein"><fmt:formatDate value="${jb.regDate}" type="date" dateStyle="medium"/></td>
											</tr>
											
											<c:if test="${jb.items.size() > 0}">
											
												<tr>
												
													<td colspan="6" style=" padding: 0; ">
													
														<table class="child-table">
															<thead>
																<tr>
																	<th class="item" scope="col"><spring:message code="web.item"/></th>
																	<th class="instr" scope="col"><spring:message code="web.instrumentdesc"/></th>
																	<th class="serial" scope="col"><spring:message code="web.serialno"/></th>
																	<th class="plant" scope="col"><spring:message code="web.plantno"/></th>
																	<th class="caltype" scope="col"><spring:message code="web.caltype"/></th>
																	<th class="itempo" scope="col"><spring:message code="web.itempo"/></th>
																	<th class="status" colspan="2" scope="col"><spring:message code="web.status"/></th>
																</tr>
															</thead>
															<tbody>
																<c:set var="rowcount2" value="0"/>
																<c:forEach var="i" items="${jb.items}">
																	<tr class="${i.state.active ? "inprogress" : "withcustomer"}">
																		<td class="text-center"><c:out value="${jb.clientRef}"/>${i.itemNo}</td>
																		<td><web:instrumentLinkWeb displayCalTimescale="${true}" instrument="${i.inst}" /></td>
																		<td><c:out value="${i.inst.serialno}"/></td>
																		<td><c:out value="${i.inst.plantno}"/></td>
																		<td class="text-center"><t:showTranslationOrDefault translations="${i.calType.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/></td>
																		<td>
																			<c:forEach var="jipo" items="${i.jobItemPOs}">
																				<c:choose>
																					<c:when test="${not empty jipo.po}">
																						${jipo.po.poNumber}<br /></c:when>																					
																					<c:when test="${not empty jipo.bpo}">
																						${jipo.bpo.poNumber}<br /></c:when>
																					<c:otherwise>
																						<spring:message code="web.noitempo"/>
																					</c:otherwise>
																				</c:choose>
																			</c:forEach>	
																		</td>
																		<td class="status">
																			<web:jiStatusHistoryIcon ji="${i}" />																														
																			<t:showTranslationOrDefault translations="${i.state.translations}" defaultLocale="${defaultlocale}"/>
																		</td>																			
																	</tr>
																	<c:set var="rowcount2" value="${rowcount2 + 1}"/>
																</c:forEach>	
															</tbody>
														</table>
														
													</td>
													
												</tr>
												
											</c:if>
											
											<c:set var="rowcount" value="${rowcount + 1}"/>
											
										</tbody>											
									</table>
									
								</c:forEach>
								
								</c:otherwise>
							</c:choose>	
							
							<table class="default tlast" summary="">
								<tfoot>
									<tr>
										<td>&nbsp;</td>
									</tr>
								</tfoot>
							</table>
								
							<c:if test="${form.jobs.size() > 0}"> <t:showResultsPagination rs="${form.rs}" pageNoId="" /></c:if>
								
						</form:form>	
					
					<!-- InstanceEndEditable -->
				
			  	</div>
				<!-- end of main box body content div -->
    </jsp:body>
</t:webTemplate>