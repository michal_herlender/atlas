<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="instrument" tagdir="/WEB-INF/tags/instrument" %>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel" %>
<%@ taglib prefix="web" tagdir="/WEB-INF/tags/web" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script type='text/javascript' src='../script/trescal/core/instrument/EditComplementaryFields.js'></script>
<web:convertBusinessDetails businessDetails="${businessDetails}" />
<web:convertContactDetails currentContact="${currentContact}" />


<t:webTemplate idheader="instrument">
<jsp:attribute name="scriptPart">

</jsp:attribute>
    <jsp:body>
        <div class="mainBoxBodyContent">
            <div class="clear"></div>
            <fieldset>
                <form:form>
                    <h3 class="text-center"><spring:message code="viewinstrument.complementaryeditinst" arguments="${command.instrument.getPlantid().toString()}" /></h3>
                    <input type="hidden" id="plantid" value="${command.instrument.getPlantid().toString()}"/>
                    <ol>
                        <%-- Commented out these two inputs as they are not displayed in the view complimentary web page - PJW 01/03/17 --%>
                        <%--<li>
                            <label><spring:message code="viewinstrument.nextmaintenance"/>:</label>
                            <form:input path="nextMaintenanceDate" size="30" placeholder="DD.MM.YYYY"/>
                            <form:errors path="nextMaintenanceDate" class="error"/>
                        </li>
                        <li>
                            <label><spring:message code="viewinstrument.nextinterimcal"/>:</label>
                            <form:input path="nextInterimCalibrationDate" size="30" placeholder="DD.MM.YYYY"/>
                            <form:errors path="nextInterimCalibrationDate" class="error"/>
                        </li>--%>
                        <li>
                            <label><spring:message code="viewinstrument.accreditation"/>:</label>
                            <form:input path="accreditation" size="30" maxlength="255"/>
                            <form:errors path="accreditation" class="error"/>
                        </li>
                        <li>
                            <label><spring:message code="viewinstrument.deliverystatus"/>:</label>
                            <form:input path="deliveryStatus" size="30"  maxlength="255"/>
                            <form:errors path="deliveryStatus" class="error"/>
                        </li>
                        <li>
                            <label><spring:message code="viewinstrument.firstusedate"/>:</label>
                            <form:input path="firstUseDate" size="30" placeholder="DD.MM.YYYY"/>
                            <form:errors path="firstUseDate" class="error"/>
                        </li>
                        <li>
                            <label><spring:message code="viewinstrument.purchasecost"/>:</label>
                            <form:input path="purchaseCost" size="30"/>
                            <form:errors path="purchaseCost" class="error"/>
                        </li>
                        <li>
                            <label><spring:message code="viewinstrument.purchasedate" />:</label>
                            <form:input path="purchaseDate" size="30" placeholder="DD.MM.YYYY"/>
                            <form:errors path="purchaseDate" class="error"/>
                        </li>
                        <li>
                            <label><spring:message code="viewinstrument.formerbarcode"/>:</label>
                            <form:input path="formerBarCode" size="30" maxlength="255"/>
                            <form:errors path="formerBarCode" class="error"/>
                        </li>
                        <li>
                            <label><spring:message code="viewinstrument.formerfamily"/>:</label>
                            <form:input path="formerFamily" size="30" maxlength="255"/>
                            <form:errors path="formerFamily" class="error"/>
                        </li>
                        <li>
                            <label><spring:message code="viewinstrument.formercharacteristics"/>:</label>
                            <form:input path="formerCharacteristics" size="30" maxlength="255"/>
                            <form:errors path="formerCharacteristics" class="error"/>
                        </li>
                        <li>
                            <label><spring:message code="viewinstrument.formerlocaldescription"/>:</label>
                            <form:input path="formerLocalDescription" size="30" maxlength="255"/>
                            <form:errors path="formerLocalDescription" class="error"/>
                        </li>
                        <li>
                            <label><spring:message code="viewinstrument.formermanufacturer"/>:</label>
                            <form:input path="formerManufacturer" size="30" maxlength="255"/>
                            <form:errors path="formerManufacturer" class="error"/>
                        </li>
                        <li>
                            <label><spring:message code="viewinstrument.formermodel"/>:</label>
                            <form:input path="formerModel" size="30" maxlength="255"/>
                            <form:errors path="formerModel" class="error"/>
                        </li>
                        <li>
                            <label><spring:message code="viewinstrument.formername"/>:</label>
                            <form:input path="formerName" size="30" maxlength="255"/>
                            <form:errors path="formerName" class="error"/>
                        </li>
                        <li>
                            <label><spring:message code="viewinstrument.provider"/>:</label>
                            <form:input path="provider" size="30" maxlength="255"/>
                            <form:errors path="provider" class="error"/>
                        </li>
                        <%--<li>
                            <label><spring:message code="viewinstrument.remarks"/></label>
                            <form:input path="clientRemarks" size="30" maxlength="255"/>
                            <form:errors path="clientRemarks" class="error"/>
                        </li>--%>
                        <li>
                            <label>&nbsp;</label>
                            <input type="submit" name="save" id="submit" value="<spring:message code='update'/>" />
                        </li>
                    </ol>
                </form:form>
            </fieldset>
        </div>
    </jsp:body>
</t:webTemplate>
