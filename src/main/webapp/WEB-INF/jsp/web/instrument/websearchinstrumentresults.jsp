<%-- File name: /trescal/core/web/instrument/websearchinstrumentresults.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="instrument" tagdir="/WEB-INF/tags/instrument"%>
<%@ taglib prefix="web" tagdir="/WEB-INF/tags/web" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<t:webTemplate idheader="instrument">
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='../script/web/instrument/WebViewInstrument.js'></script>
		<web:convertBusinessDetails businessDetails="${businessDetails}" />
		<web:convertContactDetails currentContact="${currentContact}" />
	</jsp:attribute>

	<jsp:body>
		<!-- main box body content div -->
		<div class="mainBoxBodyContent">
			<div class="text-center">
				<a href="../web/searchinstrument.htm" class="searchagain mainlink" title="<spring:message code="web.search.again" />"><spring:message code="web.search.again" /></a>
				<h3><spring:message code="instrument.result.title" /></h3>
			</div>					
			<form:form action="" name="searchform" id="searchform" method="post" modelAttribute="command">
				<form:hidden path="newSearch" value="false"/>
				<form:hidden path="exactSearch" />
				<form:hidden path="coid" />						
				<form:hidden path="subdivid" />
				<form:hidden path="addressid" />
				<form:hidden path="personid" />
				<form:hidden path="location" />
				
				<form:hidden path="calDueDateBetween" />
				<form:hidden path="lastCalDateBetween" />
				<form:hidden path="calDueDate1" />
				<form:hidden path="calDueDate2" />
				<form:hidden path="lastCalDate1" />
				<form:hidden path="lastCalDate2" />
				
				<form:hidden path="mfr" />
				<form:hidden path="mfrid" />
				<form:hidden path="desc" />
				<form:hidden path="descid" />
				<form:hidden path="model" />
				<form:hidden path="customerDescription" />
				<form:hidden path="customerManaged" />
				
				<form:hidden path="serialno" />
				<form:hidden path="plantno" />
				<form:hidden path="plantid" />
				
				<form:hidden path="addedSince" />
				<form:hidden path="outOfCalibration" />
				<form:hidden path="standard" />
				<form:hidden path="status" />
				
				<form:hidden path="pageNo" />
				<form:hidden path="resultsPerPage" />
				<form:hidden path="instCount" />
				<form:hidden path="hasReplacement" />
				<form:hidden path="usageTypeId"/>
				
				<c:forEach var="field" items="${flexiblefields}" varStatus="loopStatus">
					<form:hidden path="flexibleFieldSearches[${loopStatus.index}].fieldDefinitionId" />
					<c:choose>
						<c:when test="${field.fieldType == 'STRING'}">
							<form:hidden path="flexibleFieldSearches[${loopStatus.index}].stringValue"/>
						</c:when>
						<c:when test="${field.fieldType == 'NUMERIC'}">
							<form:hidden path="flexibleFieldSearches[${loopStatus.index}].numericValue"/>
						</c:when>
						<c:when test="${field.fieldType == 'BOOLEAN'}">
							<form:hidden path="flexibleFieldSearches[${loopStatus.index}].booleanValue"/>
						</c:when>
						<c:when test="${field.fieldType == 'DATETIME'}">
							<form:hidden path="flexibleFieldSearches[${loopStatus.index}].dateValue"/>
						</c:when>
						<c:when test="${field.fieldType == 'SELECTION'}">
							<form:hidden path="flexibleFieldSearches[${loopStatus.index}].selectionValueId"/>
						</c:when> 
					</c:choose>
				</c:forEach>
				
				<t:showResultsPagination rs="${command.rs}" pageNoId=""/>
				
				<table class="default tfirst instResults" summary="This table lists all instrument search headings">
					<thead>
						<tr>
							<td colspan="11"><spring:message code="instrument.result.count" arguments="${command.instruments.size()}" />
								<spring:message code="web.export.excel" var="submitText"/>
								<input type="submit" name="export" value="${submitText}" class="float-right">
							</td>
						</tr>
						<tr>
							<th class="barcode" scope="col"><spring:message code="web.barcode" /></th>  
							<th class="inst" scope="col"><spring:message code="web.instrumentdesc" /></th>
							<th class="serialno" scope="col"><spring:message code="web.serialno" /></th>
							<th class="plantno" scope="col"><spring:message code="web.plantno" /></th>
							<th class="owner" scope="col"><spring:message code="web.owner" /></th>
							<th class="address" scope="col"><spring:message code="web.lastcalibrationdate" /></th>
							<th class="subdiv" scope="col"><spring:message code="web.nextcaldate" /></th>
							<th class="location" scope="col"><spring:message code="web.certificateno" /></th>
							<c:if test="${hasCalVerification}">
								<th class="location" scope="col"><spring:message code="web.status"/></th>
							</c:if>
							<th class="location" scope="col"><spring:message code="web.availability"/></th>
							<th class="location" scope="col"><spring:message code="web.location" /></th>
						</tr>
					</thead>
					<c:if test="${command.results.size() < 1}">
						<tbody>
							<tr>
								<td class="bold text-center" colspan="11">
									<spring:message code="web.noresults" />
								</td>
							</tr>
						</tbody>
                    </c:if>
                    <c:if test="${command.results.size() > 0}">
                        <c:forEach var="instrument" items="${command.results}">
                            <tbody>
                                <tr class="resultrow">
                                    <td class="barcode">${instrument.plantId}</td>
                                    <td class="inst">
                                        <web:WebInstrumentLink customerDescription="${instrument.customerDescription}" modelname="${instrument.modelName}"
                                                               plantid="${instrument.plantId}" scrapped="${instrument.scrapped}"
                                                               displayCalTimescale="${instrument.instrumentStatus != 'DO_NOT_RECALL'}"
                                                               calIntervalUnit="${instrument.calibrationIntervalUnit}"
                                                               calInterval="${instrument.calibrationInterval}"
                                                               calDueDate="${instrument.calDueDate}"/>
                                    </td>
                                    <td class="serialno">${instrument.serialNo}</td>
                                    <td class="plantno">${instrument.plantNo}</td>
                                    <td class="owner">${instrument.owner}</td>
                                    <td class="lastcaldate"><fmt:formatDate value="${instrument.lastcertdate}" type="date" dateStyle="SHORT"/></td>
                                    <td class="nextCalDate"><fmt:formatDate value="${instrument.calDueDate}" type="date" dateStyle="SHORT"/></td>
                                    <td class="certificate">
										<a href="viewcert.htm?cert=${instrument.certId}" class="mainlink">${instrument.certNo}</a>
									</td>
									<c:if test="${hasCalVerification}">
										<td class="certificate">${instrument.calStatusName}</td>
									</c:if>
									<td class="certificate">${instrument.usageTypeName}</td>
                                    <td class="location">${instrument.location}</td>
                                </tr>
                            </tbody>
                        </c:forEach>
                    </c:if>
					<tfoot>
						<tr>
							<td colspan="11"><input type="submit" name="export" value="${submitText}" class="float-right"></td>
						</tr>
					</tfoot>
				</table>
				<t:showResultsPagination rs="${command.rs}" pageNoId=""/>
			</form:form>									
	 	</div>
	</jsp:body>
</t:webTemplate>