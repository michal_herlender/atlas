<%-- File name: /trescal/core/web/instrument/webeditinstrument.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="instrument" tagdir="/WEB-INF/tags/instrument"%>
<%@ taglib prefix="web" tagdir="/WEB-INF/tags/web"%>

<%--@elvariable id="instrument" type="org.trescal.cwms.core.instrument.entity.instrument.Instrument"--%>
<%--@elvariable id="command" type="org.trescal.cwms.core.instrument.form.EditInstrumentForm"--%>
<%--@elvariable id="currentContact" type="org.trescal.cwms.core.instrument.form.EditInstrumentForm"--%>

<c:set var="xheader" value="instrument" />
<c:if test="${command.plantId == 0}">
	<c:set var="xheader" value="newinstrument" />
</c:if>

<t:webTemplate idheader="${xheader}">
	<jsp:attribute name="scriptPart">
		<script type='text/javascript'
			src='../script/web/instrument/WebEditInstrument.js'></script>
		<web:convertBusinessDetails businessDetails="${businessDetails}" />
		<web:convertContactDetails currentContact="${currentContact}" />

		<script>
			var addrArray = [];
			<c:forEach var="addr" items="${addresses}">
				var locArray = [];
				<c:forEach var="loc" items="${addr.locations}">
					locArray.push({locid: ${loc.locationid}, locname: '${loc.location}'});
				</c:forEach>
				addrArray.push({addrid: ${addr.addrid}, locArray: locArray});
			</c:forEach>

			var selectedLocId = 0;
			<c:if test="${not empty command.locid}">
				selectedLocId = ${command.locid};
			</c:if>
		
		</script>		
	</jsp:attribute>
	<jsp:body>
		<div class="mainBoxBodyContent">
			<t:showErrors path="command.*" showFieldErrors="true"/>

			<!-- set variables to show or hide areas of the page dependant on
				 whether the page is displayed for a new or edited instrument -->
			<c:set var="editSelVis" value="vis" />
			<c:set var="infoTextVis" value="hid" />
			<c:if test="${command.plantId > 0}">
				<c:set var="editSelVis" value="hid" />
				<c:set var="infoTextVis" value="vis" />
			</c:if>
				
			<!-- div displaying add/edit instrument heading info -->
			<div>
				<!-- left floated column -->							
				<div class="columnLeft-1">
					<c:choose>
						<c:when test="${command.plantId > 0}">
							<h3 class="text-center">
								<spring:message code="instrument.edit.title"
									arguments="${command.plantId.toString()}" /> - <a
									href="../web/viewinstrument.htm?plantid=${command.plantId}"
									class="mainlink"><spring:message
										code="instrument.edit.view" /></a>
							</h3>
							<div class="pad-left">
								<spring:message code="instrument.edit.info" />			
							</div>
						</c:when>
						<c:otherwise>
							<h3 class="text-center">
								<spring:message code="instrument.new.title" />
							</h3>
							<div class="pad-left">
								<spring:message code="instrument.new.info" />	 			
							</div>
						</c:otherwise>
					</c:choose>
				</div>
				
				<div class="clear"></div>
			</div>
									
			<fieldset>
				<form:form id="editinstrumentform">
					<div>
						<ol>
							<li>	
								<label><spring:message code="instmodelname" />:</label>

								<cwms:showmodel instrumentmodel="${instrument.model}" hideTypology="true" /> 
								<c:if test="${command.customerManaged}">
									<a
										href="<c:url value='/web/addinstrumentmodelsearch.htm?plantid=${command.plantId}'/>">Change</a>
								</c:if>
								<input type="hidden" name="modelid" value="${command.modelid}" />
							</li>								
							<c:if test="${command.modelMfrType != null && command.modelMfrType == 'MFR_GENERIC'}">
								<li>
			                        <label><spring:message code="manufacturer" />:</label>
			                        <!-- float div left -->
			                        <div class="float-left">
			                        	<c:choose>
				                        	<c:when test="${command.customerManaged}">
					                        	<form:input path="mfrname" id="mfrname" tabindex="3" />
		                             			<form:hidden path="mfrid" id="mfrid" />
               								</c:when>  
	                             			<c:otherwise>
	                              				<span>${command.mfrname}</span>
	                             				<form:hidden path="mfrid" id="mfrid"/>
	                             			</c:otherwise>
                              			</c:choose>           
			                        </div>                                                                                          
			                        <!-- clear floats and restore pageflow -->
			                        <div class="clear-0"></div>
		                    	</li>
		                    	<li>
		                    		<label><spring:message code='model' />:</label>
		                    		<c:choose>
				                    	<c:when test="${command.customerManaged}">
											<form:input path="modelName" />
											<form:errors path="modelName" />
										</c:when>
										<c:otherwise>
											<span>${command.modelName}</span>
										</c:otherwise>
									</c:choose>
		                    	</li>
							</c:if>
								
							<li>											
								<label for="subdivid"><spring:message
										code="web.subdivision.label" /></label>
								<c:choose>
									<c:when test="${(xheader == 'instrument' && userUpdateRule == 'COMPANY') || (xheader == 'newinstrument' && userAddInstRule == 'COMPANY')}">
										<c:choose>
											<c:when test="${subdivs.size() > 1}">
												<select id="subdivid" name="subdivid">
													<!-- onchange=" getActiveSubdivAddrsAndConts(this.value, $j(this).parent().next().find('select'), $j(this).parent().next().next().find('select'), false, ''); return false; "> -->											
													<c:forEach var="sub" items="${subdivs}">
														<c:set var="select" value="" />
														<c:choose>
															<c:when test="${command.plantId > 0}">
																<c:if
																	test="${sub.subdivid == instrument.con.sub.subdivid}">
																	<c:set var="select" value="selected='selected'" />
																</c:if>
															</c:when>
															<c:otherwise>
																<c:if
																	test="${sub.subdivid == currentContact.sub.subdivid}">
																	<c:set var="select" value="selected='selected'" />
																</c:if>
															</c:otherwise>
														</c:choose>
													
														<option value="${sub.subdivid}" ${select}>${sub.subname}</option>
													</c:forEach>
												</select>
											</c:when>
											<c:otherwise>
												<span>
													<instrument:subdivFilter subdiv="${currentContact.sub}" />
												</span>
												<input type="hidden" name="${status.expression}"
													value="${currentContact.sub.subdivid}" />
											</c:otherwise>
										</c:choose>
									</c:when>
									<c:otherwise>
										<span><instrument:subdivFilter
												subdiv="${currentContact.sub}" /></span>
										<input type="hidden" name="${status.expression}"
											value="${currentContact.sub.subdivid}" />
									</c:otherwise>
								</c:choose>																																																																			
							</li>
							<li>
								<spring:bind path="command.addrid">
									<label for="${status.expression}"><spring:message
											code="web.address.label" /></label>
									<c:choose>
										<c:when test="${(xheader == 'instrument' && (userUpdateRule == 'COMPANY' || userUpdateRule == 'SUBDIV')) || 
														(xheader == 'newinstrument' && (userAddInstRule == 'COMPANY' || userAddInstRule == 'SUBDIV')) }">
											<select id="${status.expression}" name="${status.expression}">
												<c:forEach var="add" items="${addresses}">
													<c:set var="select" value="" />
													<c:choose>
														<c:when test="${command.plantId > 0}">
															<c:if
																test="${add.addrid == instrument.add.addrid}">
																<c:set var="select" value="selected='selected'" />
															</c:if>
														</c:when>
														<c:otherwise>
															<c:if
																test="${add.addrid == currentContact.defAddress.addrid}">
																<c:set var="select" value="selected='selected'" />
															</c:if>
														</c:otherwise>
													</c:choose>
												
													<option value="${add.addrid}" ${select}>${add.addr1}, ${add.addr2}</option>
												</c:forEach>
											</select>
										</c:when>
										<c:otherwise>
											<span>${currentContact.defAddress.addr1}, ${currentContact.defAddress.addr2}</span>
											<input type="hidden" name="${status.expression}"
												value="${currentContact.defAddress.addrid}" />
										</c:otherwise>
									</c:choose>
									<c:if test="${status.error}">
										<span class="error"><img src="../img/web/asterisk.png"
											width="16" height="16" /></span>
									</c:if>
								</spring:bind>
							</li>
														
							<li>
								<spring:bind path="command.personid">
									<label for="${status.expression}"><spring:message
											code="web.contactname.label" /></label>
									<c:choose>
										<c:when test="${(xheader == 'instrument' && (userUpdateRule == 'COMPANY' || userUpdateRule == 'SUBDIV' || userUpdateRule == 'CONTACT')) ||
											        (xheader == 'newinstrument' && (userAddInstRule == 'COMPANY' || userAddInstRule == 'SUBDIV' || userAddInstRule == 'CONTACT'))}">
											<select id="${status.expression}" name="${status.expression}">
												<c:forEach var="c" items="${contacts}">
													<c:set var="select" value="" />
													<c:choose>
														<c:when test="${command.plantId > 0}">
															<c:if test="${c.personid == instrument.con.personid}">
																<c:set var="select" value="selected='selected'" />
															</c:if>
														</c:when>
														<c:otherwise>
															<c:if test="${c.personid == currentContact.personid}">
																<c:set var="select" value="selected='selected'" />
															</c:if>
														</c:otherwise>
													</c:choose>
												
													<option value="${c.personid}" ${select}>${c.reversedName}</option>
												</c:forEach>
											</select>
										</c:when>
										<c:otherwise>
											<span>${currentContact.name}</span>
											<input type="hidden" name="${status.expression}"
												value="${currentContact.personid}" />
										</c:otherwise>
									</c:choose>
									<c:if test="${status.error}">
										<span class="error"><img src="../img/web/asterisk.png"
											width="16" height="16" /></span>
									</c:if>
								</spring:bind>
							</li>	
							<li>
								<spring:bind path="command.locid">
									<label for="${status.expression}"><spring:message code="web.location.label" /></label>
									<select id="${status.expression}" name="${status.expression}">
										<option value="" ${select}><spring:message code="web.selectlocation" /></option>
										<c:forEach var="loc" items="${locations}">
											<c:set var="select" value="" />											
											<c:choose>
												<c:when test="${command.plantId > 0}">
													<c:if test="${loc.locationid == instrument.loc.locationid}">
														<c:set var="select" value="selected='selected'" />
													</c:if>
												</c:when>
											</c:choose>
											<option value="${loc.locationid}" ${select}>${loc.location}</option>
										</c:forEach>
									</select>
								</spring:bind>
								<span>
									<button type="button" id="addlocation" class="addlocation"><spring:message code="company.addnewlocation"/></button>
								</span>
							</li>																																		
							<li>
								<label for="instrument.freeTextLocation"><spring:message code="editinstr.freetextlocation" />:</label>
								<form:input path="freeTextLocation" size="64" />
								<span class="attention"><form:errors path="freeTextLocation" /> </span>
							</li>
							<li>
								<spring:bind path="command.serialNo">
									<label for="instrument.serialno"><spring:message
											code="web.serialno.label" /></label>
									<c:choose>
										<c:when
											test="${command.plantId == 0 || command.customerManaged}">
											<input type="text" id="${status.expression}"
												name="${status.expression}" value="${status.value}" />	
											<c:if test="${status.error}">
												<span class="error"><img
													src="../img/web/asterisk.png" width="16" height="16" /></span>
											</c:if>
										</c:when>
										<c:otherwise>
											<c:choose>
												<c:when test="${not empty command.serialNo}">
													<span>${command.serialNo}</span>
												</c:when>
												<c:otherwise>
													<input type="text" id="${status.expression}"
														name="${status.expression}" value="${status.value}" />
													<c:if test="${status.error}">
														<span class="error"><img
															src="../img/web/asterisk.png" width="16" height="16" /></span>
													</c:if>
												</c:otherwise>
											</c:choose>
										</c:otherwise>
									</c:choose>
								</spring:bind>
							</li>
							<li>
								<spring:bind path="command.plantNo">
									<label for="plantno"><spring:message code="web.plantno.label" /></label>
									<c:choose>
										<c:when test="${command.plantNo == null || command.plantNo.isEmpty() || command.customerManaged}">
											<input type="text" id="${status.expression}" name="${status.expression}" value="${status.value}" />	
											<c:if test="${status.error}">
												<span class="error"><img src="../img/web/asterisk.png" width="16" height="16" /></span>
											</c:if>
											<c:if test="${empty command.plantNo}">
												<span class="attention"> <spring:message code="instrument.new.plantno.warning" /></span>
											</c:if>
										</c:when>
										<c:otherwise>
											<span><c:out value="${command.plantNo}"/></span>
										</c:otherwise>
									</c:choose>
								</spring:bind>
							</li>
							<li>
								<label for="instrument.clientbarcode"><spring:message code="editinstr.clientbarcode" />:</label>
								<form:input path="clientBarcode" size="50" />
								<span class="attention"><form:errors path="clientBarcode" /> </span>
							</li>
							<li>
								<label><spring:message code="web.status.label"/></label>
								<form:select path="statusid" onchange="onStatusChange(this)">
									<c:forEach var="instrumentStatus" items="${statuslist}">
										<form:option value="${instrumentStatus.ordinal()}">
											<c:out value="${instrumentStatus.getStatus()}"/>
										</form:option>
									</c:forEach>
								</form:select>
								<form:errors path="statusid" />									
							</li>									
							<li>
									<label><spring:message code="web.storagetype.label" /></label>
									<form:select path="storageTypeId">
										<c:forEach var="storageType" items="${storage}">
											<form:option value="${storageType.id}">
												<cwms:besttranslation translations="${storageType.nametranslation}"/>
											</form:option>
										</c:forEach>
									</form:select>
									<form:errors path="storageTypeId"/>
							</li>
							<li>
									<label for="${status.expression}"><spring:message code="web.usagetype.label" /></label>
									<form:select path="usageTypeId">
										<form:option value=""></form:option>
										<c:forEach var="usageType" items="${usage}">
											<form:option value="${usageType.id}" data-linkedstatus="${usageType.instrumentStatus.ordinal()}">
												<cwms:besttranslation translations="${usageType.nametranslation}"/>
											</form:option>
										</c:forEach>
									</form:select>
									<form:errors path="usageTypeId"/>
							</li>
							<li>
								<label><spring:message code="nextcaldue"/></label>
								<c:choose>
									<c:when test="${command.customerManaged}">
										<form:input path="nextCalDueDate" class="date" type="date" />
									</c:when>
									<c:otherwise>
										<fmt:formatDate value="${command.nextCalDueDate}" type="date" dateStyle="SHORT" />
									</c:otherwise>
								</c:choose>
							</li>	
							<li>
								<label><spring:message code="editinstr.calibrateevery" /></label>
								<c:choose>
									<c:when test="${command.customerManaged}">
										<form:input path="calFrequency"
											cssStyle="width: 30px;" onkeypress="return event.charCode >= 46 && event.charCode <= 57"/>
										<form:select path="calFrequencyUnitId" items="${units}" itemLabel="value" itemValue="key"/>
										<form:errors path="calFrequency" class="error"/>
									</c:when>
									<c:otherwise>
										<span>${command.calFrequency}&nbsp;${command.translatedCalFrequencyUnit}</span>
									</c:otherwise>
								</c:choose>
							</li>
							<li>
								<form:label path="calExpiresAfterNumberOfUses">
									<spring:message code="editinstr.percalrecallinterval"/>:
								</form:label>
								<form:checkbox id="usages" path="calExpiresAfterNumberOfUses"/>
							</li>

							<li class="usageField" style="display: none">
								<form:label path="permittedNumberOfUses">
									<spring:message code="editinstr.permitednumberofuses"/>:
								</form:label>
								<form:input path="permittedNumberOfUses"/>
								<form:errors path="permittedNumberOfUses"/>
							</li>
							<li class="usageField" style="display: none">
								<form:label path="usesSinceLastCalibration">
									<spring:message code="editinstr.calssincelastcal"/>:
								</form:label>
								<form:input path="usesSinceLastCalibration"/>
								<form:errors path="usesSinceLastCalibration"/>
							</li>
							
                           <%--  Leaving this here for now in case the wind changes and someone decides 
                           		 customers do have to be able to edit this field after all
                           <c:if test="${hasCalVerification}">
                                <li>
                                    <form:label path="lastCertStatusOverride"><spring:message code="web.overridestatusoflastcert"/></form:label>
                                    <form:select path="lastCertStatusOverride">
                                            <spring:message code="no" var="labelforno"/>
                                            <form:option value="" label='${labelforno}'/>
                                            <form:options items="${verificationStatuses}" itemLabel="value" itemValue="key"/>
                                    </form:select>
                                    <br><i>Note: Override will be cleared when next certificate is issued</i>
                                </li>
                            </c:if> 
                            --%>
                            
                            
							<li>
								<label for="instrument.customerDescription"><spring:message code="editinstr.customerdescription" />:</label>
								<form:input path="customerDescription" size="100" />
								<span class="attention"><form:errors path="customerDescription" /> </span>
							</li>
							<li>
								<label for="instrument.customerSpecification"><spring:message code="editinstr.customerspecification" />:</label>
								<form:input path="customerSpecification" size="100" />
								<span class="attention"><form:errors path="customerSpecification" /> </span>
							</li>
							<li>
								<label for="instrument.customerRemarks"><spring:message code="editinstr.remarks" />:</label>
								<form:input path="customerRemarks" size="50" />
								<span class="attention"><form:errors path="customerRemarks" /> </span>
							</li>
							<li>
								<label for="instrument.calibrationInstructions"><spring:message code="editinstr.calibrationinstructions" />:</label>
								<form:textarea path="calibrationInstructions" cols="40" rows="3"/>
								<span class="attention"><form:errors path="calibrationInstructions" /> </span>
							</li>

							<li>
								<label>&nbsp;</label>
								<c:choose>
									<c:when test="${command.plantId == 0}">
										<input type="button"
											value="<spring:message code="instrument.new.submit" />"
											class="submitInstrument"
											onclick=" this.disabled = true; checkDuplicateInstruments(${command.instrument.comp.coid}, true); " />
									</c:when>
									<c:otherwise>
										<input type="button"
											value="<spring:message code="instrument.edit.submit" />"
											class="submitInstrument"
											onclick=" this.disabled = true; checkDuplicateInstruments(null, false); " />
									</c:otherwise>
								</c:choose>		
							</li>										
						</ol>
					</div>
				</form:form>
			</fieldset>																						
	  	</div>


		<div id="dialog" style="display: none;">
			<input type="hidden" autofocus="autofocus" />
			
			<fieldset>
                    <h3 class="text-center"><spring:message code="company.addnewlocation" /></h3>
                    <ol>
                        <li>
                            <label><spring:message code="address.address"/>:</label>
                            <span id="addr1"></span>
                        </li>
                        <li>
                            <label><spring:message code="company.location"/>:</label>
                            <input type="text" id="location" maxlength="100"/>
                        </li>
                        <li>
                            <label>&nbsp;</label>
                            <button id="savelocation"><spring:message code='save'/></button>
                        </li>
                    </ol>
            </fieldset>
			
			
		</div>
		
	</jsp:body>
</t:webTemplate>
