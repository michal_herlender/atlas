<%-- File name: /trescal/core/web/instrument/WebAddInstrumentModelSearchResults.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<c:set var="xheader" value="instrument" />
<c:if test="${empty addinstrumentmodelsearchform.plantId || addinstrumentmodelsearchform.plantId == '0'}">
	<c:set var="xheader" value="newinstrument" />
</c:if>

<t:webTemplate idheader="${xheader}">
	<jsp:body>
		<div class="mainBoxBodyContent">
			<form:form action="" name="searchform" id="searchform" method="post" modelAttribute="addinstrumentmodelsearchform">
				<form:hidden path="model"/>
				<form:hidden path="mfrNm"/>
				<form:hidden path="descNm"/>
				<form:hidden path="descId"/>
				<form:hidden path="mfrId"/>
				<form:hidden path="pageNo"/>
				<form:hidden path="resultsPerPage"/>
				<form:hidden path="salesCat"/>
				<form:hidden path="salesCatId"/>
				<form:hidden path="domainNm"/>
				<form:hidden path="domainId"/>
				<form:hidden path="familyNm"/>
				<form:hidden path="familyId"/>
				<form:hidden path="plantId"/>
				
				<t:paginationControl resultset="${addinstrumentmodelsearchform.rs}"/>
				<c:set var="totalCols" value="5"/>
																	
				<table id="modelsearchresults" class="default tfirst instResults" summary="<spring:message code="searchmodres.tablesearchresults"/>">
					<thead>
						<tr>
							<td colspan="${totalCols}">
								<c:choose>
									<c:when test="${addinstrumentmodelsearchform.rs.resultsCount == 1 }">
										<spring:message code="searchmodres.yourmodelsearchreturned"/> ${addinstrumentmodelsearchform.rs.resultsCount} <spring:message code="searchmodres.result"/>
									</c:when>
									<c:otherwise>
										<spring:message code="searchmodres.yourmodelsearchreturned"/> ${addinstrumentmodelsearchform.rs.resultsCount} <spring:message code="searchmodres.results"/>
									</c:otherwise>
								</c:choose>
							</td>
						</tr>
						</thead>
						</table>
						<table class="default instResults" summary="This table displays instruments returned">
						<thead>
						<tr>
							<th id="chars" scope="col"><spring:message code="instmodelname"/></th>
							<th id="mfr" scope="col"><spring:message code="manufacturer"/></th>
							<th id="mode" scope="col"><spring:message code="model"/></th>
							<th id="sales-category" scope="col"><spring:message code="salescategory"/></th>
							<th id="quarantined" scope="col"><spring:message code="quarantined"/></th>
						</tr>
					</thead>
				
					<tbody id="keynavResults">
						<c:set var="rowcount" value="0"/>
						<c:choose>
							<c:when test="${addinstrumentmodelsearchform.rs.resultsCount < 1}">
								<tr class="odd">
									<td colspan="${totalCols}" class="bold center"><spring:message code="noresults"/></td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:forEach var="model" items="${addinstrumentmodelsearchform.rs.results}">
									<c:choose>
										<c:when test="${rowcount % 2 == 0 }"><c:set var="rowclass" value="even"/></c:when>
										<c:otherwise><c:set var="rowclass" value="odd"/></c:otherwise>
									</c:choose>
									<tr class="${rowclass}" onclick="window.location.href = 'editinstrument.htm?modelid=${model.modelid}&plantid=${addinstrumentmodelsearchform.plantId}' " onmouseover=" $j(this).addClass('hoverrow'); " onmouseout=" if (this.className != 'hoverrow') $j(this).removeClass('hoverrow'); ">
										<td>
											<cwms:showmodel instrumentmodel="${model}"/>
										</td>
										<td>
											<c:out value="${model.mfr.name}"/>
										</td>
										<td>
											<a href="<c:url value="editinstrument.htm?modelid=${model.modelid}"/>" class="mainlink"> ${model.model} </a>
										</td>
										
										<td>
											<t:showTranslationOrDefault translations="${model.salesCategory.translations}" defaultLocale="${defaultlocale}"></t:showTranslationOrDefault>	
										</td>
										<td>
											<c:choose>
												<c:when test="${model.quarantined == true }"><spring:message code="yes"/></c:when>
												<c:otherwise><spring:message code="nobool"/></c:otherwise>
											</c:choose>
										</td>
									</tr>
									<c:set var="rowcount" value="${rowcount + 1}"/>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
				<table class="default tlast" summary="">
					<tfoot>
						<tr>
							<td colspan="${totalCols}">&nbsp;</td>
						</tr>
					</tfoot>
				</table>
					
				<t:paginationControl resultset="${addinstrumentmodelsearchform.rs}"/>
								
			 </form:form>
		</div>
			
	</jsp:body>
</t:webTemplate>