<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:webTemplate idheader="instrument">
    <jsp:body>
        <div class="mainBoxBodyContent">
            <t:showErrors path="command.*" showFieldErrors="true"/>
            <form:form>
                <form:hidden path="valueId"/>
                <form:hidden path="fieldDefinitionId"/>
                <form:hidden path="plantId"/>
                <div class="infobox">
                    <fieldset>
                        <ol>
                            <li>
                                <label>${command.fieldName}</label>
                                <form:input path="numericValue" id="numericValue"/>
                            </li>
                        </ol>
                        <input type="submit" name="update" value='<spring:message code="update"/>'/>
                        <c:if test="${command.valueId > 0}">
                            <input type="submit" name="delete" value='<spring:message code="delete"/>'/>
                        </c:if>
                    </fieldset>
                </div>
            </form:form>
        </div>
        <script>
            $j(document).ready(function() {
                $j("#numericValue").keydown(function (e) {
                    // Allow: backspace, delete, tab, escape, enter and .
                    if ($j.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                        // Allow: Ctrl+A
                        (e.keyCode == 65 && e.ctrlKey === true) ||
                        // Allow: Ctrl+C
                        (e.keyCode == 67 && e.ctrlKey === true) ||
                        // Allow: Ctrl+X
                        (e.keyCode == 88 && e.ctrlKey === true) ||
                        // Allow: home, end, left, right
                        (e.keyCode >= 35 && e.keyCode <= 39)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                        e.preventDefault();
                    }
                });
            });
        </script>
    </jsp:body>
</t:webTemplate>