<%-- File name: /trescal/core/web/instrument/websearchinstrument.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="instrument" tagdir="/WEB-INF/tags/instrument"%>

<t:webTemplate idheader="instrument">
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='../script/web/instrument/WebSearchInstrument.js'></script>
	</jsp:attribute>
	<jsp:body>
		<div class="mainBoxBodyContent">
			<!-- error section displayed when form submission fails -->
			<form:errors path="command.*">
       			 <div class="warningBox1">
              		 <div class="warningBox2">
              	       <div class="warningBox3">
                    	<h4 class="center-0 attention"><spring:message code="web.search.error"/>:</h4>
                     		 <c:forEach var="e" items="${messages}">
                        <div class="center attention"><c:out value="${e}"/></div>
                      </c:forEach>
                     </div>
                   </div>
                 </div>
             </form:errors>
				
			<!-- div displaying search heading info -->
			<div>
				<!-- left floated column -->							
				<div class="columnLeft-1">
					<h3 class="text-center"><spring:message code="instrument.search.title" /></h3>
					<div class="pad-left">
						<spring:message code="instrument.search.info" />
					</div>
				</div>
				<!-- right floated column
				<div class="columnRight-1 text-right">
					<img src="../img/web/instrument.png" width="140" height="67" alt="" class="pad-5" />
				</div>
				clear floats and restore page flow -->
				<div class="clear"></div>
			</div>
				
			<!-- search form elements -->
			<fieldset>
				<form:form method="post" action=""  id="searchinstrumentform" onsubmit=" loadScript.applySearchingImage($j(this).find('input[type=\'submit\']').parent()); ">
					<form:hidden path="newSearch" value="true"/>
					<input type="hidden" id="companyid" value="${companyId}"/>
					<!-- left floated column -->
					<div class="columnLeft-2">
						<ol>
							<%-- <li>
								<spring:bind path="command.exactSearch">
									<label for="${status.expression}"><spring:message code="web.exactresults.label" /></label>
									<input type="hidden" name="_${status.expression}" /><input type="checkbox" name="${status.expression}" />
								</spring:bind>
							</li> --%>
							<li>
									<form:label path="desc"><spring:message code="sub-family" /></form:label>
									<div class="ui-widget">
									<form:input id="desc" path="desc" type="text"/>
									</div>
									
									<form:hidden id="descid" path="descid" />
							</li>
							<li>
								<label for="mfr"><spring:message code="web.manufacturer.label" /></label>
								<!-- float div left -->
								<div class="float-left-margin">
									<div>
										<form:input path="mfr" name="mfr"/>
                                		<form:input path="mfrid" type="hidden" />
									</div>
								</div>																							
								<!-- clear floats and restore pageflow -->
								<span class="clear-0"></span>
							</li>
							<li>										
									<form:label path="model"><spring:message code="web.model.label" /></form:label>
									<form:input type="text" path="model" />
							</li>
							<li>										
									<form:label path="customerDescription"><spring:message code="viewinstrument.customerdescription" /></form:label>
									<form:input type="text" path="customerDescription" />
							</li>
							<li>
									<form:label path="plantid"><spring:message code="web.barcode.label" /></form:label>
									<form:input type="text" path="plantid" />
							</li>
							<li>
									<form:label path="serialno"><spring:message code="web.serialno.label" /></form:label>
									<form:input type="text" path="serialno" />
							</li>
							<li>
									<form:label path="plantno"><spring:message code="web.plantno.label" /></form:label>
									<form:input type="text" path="plantno" />
							</li>
							<li>
									<form:label path="outOfCalibration"><spring:message code="web.incalibration.label" /></form:label>
									<form:select path="outOfCalibration">
										<option value="" selected="selected"><spring:message code="web.either" /></option>
										<option value="false"><spring:message code="web.incalibrationonly" /></option>
										<option value="true"><spring:message code="web.outcalibrationonly" /></option>
									</form:select>
							</li>
							<li>
									<form:label path="status"><spring:message code="web.instrumentstatus.label" /></form:label>
									<form:select path="status">
										<option value="" selected="selected"><spring:message code="web.allstatus" /></option>
										<c:forEach var="s" items="${inststatuss}">
											<option value="${s}">${s.getStatus()}</option>
										</c:forEach>
									</form:select>
							</li>	
							<li>
								<label>&nbsp;</label>
								<input type="submit" name="submit" value="<spring:message code="web.search" />" />
							</li>
						</ol>
					</div>
						
					<!-- right floated column -->
					<div class="columnRight-2">
						<ol>											
							<li>											
									<form:label path="subdivid"><spring:message code="web.subdivision.label" /></form:label>
									<c:choose>
										<c:when test="${userReadRule == 'COMPANY' && subdivs.size() > 1}">
											<form:select id="subdivid" path="subdivid" style="max-width:70%;" onchange=" getActiveSubdivAddrsAndConts(this.value, $j(this).parent().next().find('select'), $j(this).parent().next().next().find('select'), true, ''); return false; ">											
												<option value="" selected="selected"><spring:message code="web.search.allsubdivisions" /></option>
												<c:forEach var="sub" items="${subdivs}">
													<option value="${sub.subdivid}">${sub.subname}</option>
												</c:forEach>
											</form:select>
										</c:when>
										<c:otherwise>
											<span><instrument:subdivFilter subdiv="${userContact.sub}" /></span>
											<form:hidden path="subdivid" value="${userContact.sub.subdivid}" />
										</c:otherwise>
									</c:choose>
									<form:errors  path="subdivid"><span class="error"><img src="../img/web/asterisk.png" width="16" height="16" /></span></form:errors>
							</li>
							<li>
									<form:label path="addressid"><spring:message code="web.address.label" /></form:label>
									<c:choose>
										<c:when test="${userReadRule == 'COMPANY' || userReadRule == 'SUBDIV'}">
											<form:select path="addressid" id="addressid">
												<option value="" selected="selected"><spring:message code="web.search.alladdresses" /></option>
												<c:forEach var="a" items="${addresses}">
													<c:if test="${a.addr1 != '' && a.addr2 != ''}">
														<option value="${a.addrid}">${a.addr1}, ${a.addr2}</option>
													</c:if>
												</c:forEach>
											</form:select>
										</c:when>
										<c:otherwise>
											<span>${userContact.defAddress.addr1}, ${userContact.defAddress.addr2}</span>
											<form:hidden path="addressid" value="${userContact.defAddress.addrid}" />
										</c:otherwise>
									</c:choose>
									<form:errors  path="addressid"><span class="error"><img src="../img/web/asterisk.png" width="16" height="16" /></span></form:errors>
							</li>

							<li>
									<form:label path="personid"><spring:message code="web.contactname.label" /></form:label>
									<c:choose>
										<c:when test="${userReadRule == 'COMPANY' || userReadRule == 'SUBDIV' || userReadRule == 'ADDRESS'}">
											<form:select id="personid" path="personid">
												<option value="" selected="selected"><spring:message code="web.search.allcontacts" /></option>
												<c:forEach var="c" items="${contacts}">
													<option value="${c.personid}">${c.reversedName}</option>
												</c:forEach>
											</form:select>
										</c:when>
										<c:otherwise>
											<span>${userContact.name}</span>
											<form:hidden path="personid" value="${userContact.personid}" />
										</c:otherwise>
									</c:choose>
									<form:errors  path="personid"><span class="error"><img src="../img/web/asterisk.png" width="16" height="16" /></span></form:errors>
							</li>
							<li>
								<label><spring:message code="web.location.label" /></label>
								<form:input path="location" id="location"/>
								<form:hidden path="locationId" id="locationid"/>
							</li>
							<li>
									<form:label path="customerManaged"><spring:message code="web.customermanaged.label" /></form:label>
									<form:select path="customerManaged">
										<option value="" selected="selected"><spring:message code="web.either" /></option>
										<option value="true"><spring:message code="web.yes" /></option>
										<option value="false"><spring:message code="web.no" /></option>
									</form:select>
							</li>
							<li class="dateInputs">
									<form:select path="calDueDateBetween" id="calDueDateBetween" onchange=" changeCalDueDateOption(this.value); return false; ">
										<form:option value="false"><spring:message code="web.on"/></form:option>
										<form:option value="true"><spring:message code="web.between"/></form:option>
									</form:select>
										
										<form:label path="calDueDate1"><spring:message code="web.calibrationduedate"/>:</form:label>
										<form:input type="date" path="calDueDate1" id="calDueDate1" />	
											
									<span id="calDueDateSpan" class=${(command.calDueDateBetween == true) ? "vis" : "hid"}>
											<spring:message code="web.and"/>&nbsp;
											<form:input type="date" path="calDueDate2" id="calDueDate2" />
									</span>
							</li>
								<li class="dateInputs">
									<form:select path="lastCalDateBetween" id="lastCalDateBetween" onchange=" changeLastCalDateOption(this.value); return false; ">
										<form:option value="false"><spring:message code="web.on"/></form:option>
										<form:option value="true"><spring:message code="web.between"/></form:option>
									</form:select>
										
										<form:label path="lastCalDate1"><spring:message code="web.lastcalibrationdate"/>:</form:label>
										<form:input type="date" path="lastCalDate1" id="lastCalDate1" />	
											
									<span id="lastCalDateSpan" class=${(command.lastCalDateBetween == true) ? "vis" : "hid"}>
											<spring:message code="web.and"/>&nbsp;
											<form:input type="date" path="lastCalDate2" id="lastCalDate2" />
									</span>
							</li>
							<li>
                                    <form:label path="remainingUses"><spring:message code="web.remaininguses"/></form:label>
                                    <form:input type="text" style="width: 40px" id="remaininguses" path="remainingUses" />
							</li>
                            <li>
								<label><spring:message code="web.usagetype.label"/></label>
								<form:select path="usageTypeId">
									<form:option value="0" label="any"/>
									<form:options items="${usageTypes}" itemLabel="value" itemValue="key"/>
								</form:select>
							</li>
							<li>
									<form:label path="hasReplacement"><spring:message code="web.hasreplacement.label" /></form:label>
									<form:select path="hasReplacement">
										<option value="" selected="selected"><spring:message code="web.either" /></option>
										<option value="true"><spring:message code="web.yes" /></option>
										<option value="false"><spring:message code="web.no" /></option>

									</form:select>
							</li>
							<c:if test="${flexiblefields.size() > 0}">
								<li>
									<b><spring:message code="company.flexiblefields"/></b>
									<instrument:flexiblefieldsearch flexiblefields="${flexiblefields}"/>
								</li>
							</c:if>
						</ol>							
					</div>
				</form:form>
			</fieldset>
		 </div>
	</jsp:body>
</t:webTemplate>