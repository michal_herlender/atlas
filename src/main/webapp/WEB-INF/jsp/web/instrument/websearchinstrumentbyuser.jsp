<%-- File name: /trescal/core/web/instrument/websearchinstrumentbyuser.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="instrument" tagdir="/WEB-INF/tags/instrument"%>
<%@ taglib prefix="web" tagdir="/WEB-INF/tags/web" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>

<t:webTemplate idheader="instrument">
	<jsp:attribute name="scriptPart">

	</jsp:attribute>

	<jsp:body>
		<!-- main box body content div -->
		<div class="mainBoxBodyContent">				
			<form:form action="" name="WebSearchInstrumentsByUserForm" id="WebSearchInstrumentsByUserForm" modelAttribute="command" method="post">
				
				<input id="pageNo" name="pageNo" type="hidden" value="${command.pageNo}">
				<t:showResultsPagination rs="${command.rs}" pageNoId=""/>
				<br>
				<table class="default">
					<thead>
						<tr>
							<th><spring:message code="web.barcode"/></th>
							<th><spring:message code="instmodelname"/></th>
							<th><spring:message code="web.serialno"/></th>
							<th><spring:message code="web.plantno"/></th>
							<th><spring:message code="web.status"/></th>
							<th><spring:message code="web.metrologicalstate"/></th>
							<th><spring:message code="web.lastdateofcalibration"/></th>
							<th><spring:message code="web.nextcaldate"/></th>
							<th><spring:message code="web.lastcalibrationcertificate"/></th>
							<th><spring:message code="web.owner"/></th>
							<th><spring:message code="web.subdivision"/></th>
							<th><spring:message code="web.behalf"/></th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test = "${command.rs.results.size() > 0}">
								<c:forEach items="${command.rs.results}" var="row">
									<tr>
										<td>
											<c:set var="linkclass" value="${row.scrapped == true ? 'mainlink-strike' : 'mainlink'}"/>
											<a href="viewinstrument.htm?plantid=${row.plantid}" class="${linkclass}">
												<c:out value="${row.plantid}"/>
											</a>
										</td>
										<td>
											<c:out value="${row.instrumentModelName}"/>
										</td>
										<td>
											<c:out value="${row.serialNo}"/>
										</td>
										<td>
											<c:out value="${row.plantNo}"/>
										</td>
										<td>
											<c:out value="${row.status}"/>
										</td>
										<td>
											<c:out value="${row.jobStatus}"/>
										</td>
										<td>
											<fmt:formatDate type="date" dateStyle="medium" value="${row.lastCalDate}"/>
										</td>
										<td>
											<fmt:formatDate type="date" dateStyle="medium" value="${row.nextCalDue}"/>
										</td>
										<td>
											<a href="viewcert.htm?cert=${row.lastCertId}">
												<c:out value="${row.lastCertificateNumber}"/>
											</a>
										</td>
										<td>
											<c:out value="${row.owner}"/>
										</td>
										<td>
											<c:out value="${row.subdivision}" />
										</td>
										<td>
											<c:out value="${row.onBehalfOf}" />
										</td>
									</tr>

								</c:forEach>
							</c:when>
							<c:otherwise>
								<tr>
									<td colspan="13">
										<spring:message code="searchinstrumentresults.therearenoresultstodisplay"/>
									</td>
								</tr>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
				<br>
				<t:showResultsPagination rs="${command.rs}" pageNoId=""/>
			</form:form>									
	 	</div>
	</jsp:body>
</t:webTemplate>