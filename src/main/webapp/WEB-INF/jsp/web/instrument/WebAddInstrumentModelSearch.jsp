<%-- File name: /trescal/core/web/instrument/WebAddInstrumentModelSearch.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="web" tagdir="/WEB-INF/tags/web"%>

<c:set var="xheader" value="instrument" />
<c:if test="${empty addinstrumentmodelsearchform.plantId || addinstrumentmodelsearchform.plantId == '0'}">
	<c:set var="xheader" value="newinstrument" />
</c:if>

<t:webTemplate idheader="${xheader}">
	<jsp:attribute name="scriptPart">
		<script type="text/javascript"
		src="../script/web/instrument/WebAddInstrumentModelSearch.js"></script>
	</jsp:attribute>
	
	<jsp:body>
        <div class="mainBoxBodyContent">
        	<c:if test="${empty addinstrumentmodelsearchform.plantId || addinstrumentmodelsearchform.plantId == '0'}">
		        <div>
		        	<div class="columnLeft-1">
						<h3 class="text-center">
							<spring:message code="instrument.new.title" />
						</h3>
						<div class="pad-left">
							<spring:message code="instrument.new.info" />	 			
						</div>
					</div>
					<!-- right floated column -->
		<!-- 				<div class="columnRight-1 text-right"> -->
		<!-- 					<img src="../img/web/instrument.png" width="140" height="67" alt="" -->
		<!-- 						class="pad-5" /> -->
		<!-- 				</div> -->
						<!-- clear floats and restore page flow -->
					<div class="clear"></div>
				</div>
			</c:if>	
            <form:form action="" method="POST" id="searchmodelform"
				modelAttribute="addinstrumentmodelsearchform">
                <fieldset>
                    <ol>
                        <li>
                            <label><spring:message code="domain" />:</label>
                            <!-- float div left -->
                            <div class="float-left">
                                <form:input path="domainNm"
									name="domainNm" tabindex="1" />
                                <form:input path="domainId"
									type="hidden" name="domainId" />                             
                            </div>
                            <div class="clear-0"></div> 
                        </li>       
                        <li>
                            <label><spring:message code="family" />:</label>
                            <!-- float div left -->
                            <div class="float-left">
                                <form:input path="familyNm"
									name="familyNm" tabindex="2" />
                                <form:input path="familyId"
									type="hidden" name="familyId" />                             
                            </div>
                            <div class="clear-0"></div> 
                        </li>   
                        <li>
                            <label><spring:message
									code="sub-family" />:</label>
                            <!-- float div left -->
                            <div class="ui-widget">
                                <form:input path="descNm" name="descNm"
									tabindex="3" />
                                <form:input path="descId" type="hidden"
									name="descId" />                       
                            </div>
                            <div class="clear-0"></div> 
                        </li>
                        
                        <li id="manufacturer">
                            <label><spring:message
									code="manufacturer" />:</label>
                            <!-- float div left -->
                            <div class="float-left">
                                <form:input path="mfrNm" name="mfrNm" tabindex="3" />
                                <form:input path="mfrId" type="hidden" />                       
                            </div>                                                                                          
                            <!-- clear floats and restore pageflow -->
                            <div class="clear-0"></div>
                        </li>
                        <li id="model">
                            <label><spring:message code="model" />:</label>                          
                            <form:input path="model" tabindex="5" />                                         
                        </li>
                        
                        <li>
                            <label><spring:message code="salescategory" />:</label>
                            <!-- float div left -->
                            <div class="float-left">
                                <form:input path="salesCat"
									name="salesCat" tabindex="6" />
                                <form:input path="salesCatId"
									type="hidden" name="salesCatId" />                             
                            </div>
                            <div class="clear-0"></div> 
                        </li>
                             
                        <li>
                            <label>&nbsp;</label>
                            <input type="submit" name="search" id="submit" value="<spring:message code="search"/>" tabindex="4" />
                        </li>                   
                    </ol>           
                </fieldset>
            </form:form>
        </div>
    </jsp:body>
</t:webTemplate>