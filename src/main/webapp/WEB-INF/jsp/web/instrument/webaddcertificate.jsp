<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<script type='text/javascript'
			src='../script/web/instrument/WebAddCertificate.js'></script>

<t:webTemplate idheader="cert">
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='../script/web/instrument/WebAddCertificate.js'></script>
	</jsp:attribute>
	<jsp:body>
		<t:showErrors path="command.*" showFieldErrors="true"/>
		<div class="mainBoxBodyContent">
			<h3 class="text-center">
				<spring:message code="instrument.performaction" />
			</h3>
			<fieldset>
				<form:form enctype="multipart/form-data" >
					<form:hidden path="instrumentId"/>
					<ol>
						<li>
							<label><spring:message code="servicetype"/></label>
							<form:select path="calTypeId" id="caltype">
								<c:forEach var="caltype" items="${services}">
									<option value="${caltype.calTypeId}" data-requirementtype="${caltype.recallRequirementType}">
										<cwms:besttranslation translations="${caltype.serviceType.longnameTranslation}"/>
									</option>
								</c:forEach>
							</form:select>
						</li>
						<li class="calonly">
                            <label><spring:message code="web.certdate" /></label>
                            <form:input path="certDate" cssStyle="width: 60px;" class="date"/>
                        </li>
						<li>
							<c:choose>
								<c:when test="${services[0].recallRequirementType=='FULL_CALIBRATION'}">
									<label id='datelabel'><spring:message code="editclientcert.caldate"/></label>
								</c:when>
								<c:when test="${services[0].recallRequirementType=='MAINTENANCE'}">
									<label id='datelabel'><spring:message code="editclientcert.maintenancedate"/></label>
								</c:when>
								<c:when test="${services[0].recallRequirementType=='INTERIM_CALIBRATION'}">
									<label id='datelabel'><spring:message code="editclientcert.interimcaldate"/></label>
								</c:when>
							</c:choose>
							
							
							<form:input path="calDate" cssStyle="width: 60px;" class="date"/>
						</li>
						<li>
							<label><spring:message code="editclientcert.duration"/></label>
							<form:input path="interval" cssStyle="width: 30px;" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
							&nbsp; <form:select path="intervalUnitId" items="${units}" itemLabel="value" itemValue="key"/>
						</li>
						<c:if test="${services[0].recallRequirementType=='FULL_CALIBRATION'}">
							<li class="calonly">
								<label><spring:message code="editclientcert.thirdpartcertno"/></label>
								<form:input path="certNumber"/>
							</li>
							<c:if test="${CalibrationVerificationStatusRequired == true }">
								<li class="calonly" >
									<label><spring:message code="viewinstrument.calverificationstatus"/></label>
									<form:select path="calibrationVerificationStatus" items="${verificationstatuses}" itemLabel="value" itemValue="key"/>
								</li>
							</c:if>
							<c:if test="${deviationRequired == true }">
								<li class="calonly" >
									<label><spring:message code="certificate.deviation"/></label>
									<form:input path="deviation" id="deviation" cssStyle="width: 60px;" />&nbsp;<c:out value="${deviationunitsymbol}"/>
								</li>
							</c:if>
							<li class="calonly">
								<label><spring:message code="viewcertificate.remarks"/></label>
								<form:textarea path="remarks" cols="40" rows="3"/>
							</li>
							<li class="calonly">
								<label><spring:message code="editclientcert.upload"/></label>
								<input type="file" name="file"/>
							</li>
						</c:if>
						<li>
							<form:button name="Submit" value="Submit"><spring:message code="submit"/></form:button>
						</li>
					</ol>
				</form:form>
			</fieldset>
		</div>
	</jsp:body>
</t:webTemplate>
