`<%-- File name: /trescal/core/web/instrument/webviewinstrument.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="instrument" tagdir="/WEB-INF/tags/instrument" %>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel" %>
<%@ taglib prefix="web" tagdir="/WEB-INF/tags/web" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="viewinstrument" tagdir="/WEB-INF/tags/instrument/viewinstrument" %>

<t:webTemplate idheader="instrument">
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='../script/web/instrument/WebViewInstrument.js'></script>
		<script type='text/javascript' src='../script/thirdparty/jQuery/jquery.js'></script>
		<script type='module' src='../script/components/cwms-upload-file/cwms-upload-file.js'></script>
		<script type='text/javascript'>
            // assign xindice database link to variable for use in createChart() method
            var xindicedb = '${command.xindicedb}';
		</script>
	</jsp:attribute>
	<jsp:attribute name="globalElements">
		<cwms-upload-file system-component-id="${command.sc.componentId}" entity-id1="${command.instrument.plantid}" entity-id2=""
 			files-name="${filesName}" rename-to="" web="${true}"></cwms-upload-file>
	</jsp:attribute>
	<jsp:body>
		<web:convertBusinessDetails businessDetails="${businessDetails}" />
		<web:convertContactDetails currentContact="${currentContact}" />
		<!-- main box body content div -->
		<div class="mainBoxBodyContent">
			
			<!-- InstanceBeginEditable name="content" -->
			
			<c:set var="instrument" value="${command.instrument}" />
			<c:set var="valStyle" value="" />
			<c:forEach var="issue" items="${instrument.issues}">
				<c:if test="${status.requiringAttention}">
					<c:set var="valStyle" value="class='attention'" />
				</c:if>
			</c:forEach>
			

			<!-- div displaying search heading info -->
			<div>
				<!-- left floated column -->							
				<div class="columnLeft-1">
					<%-- spring:message formats id with thousands separator if it's passed an integer hence use of toString method below --%>
					<h3 class="text-center"><spring:message code="instrument.title" arguments="${instrument.plantid.toString()}" /></h3>
					<div class="pad-left">
							<spring:message code="instrument.info" />
					</div>							
				</div>							
				<!-- right floated column -->
				<div class="columnRight-1">
					<h3><spring:message code="instrument.quickactions" /></h3>
					<c:if test="${userCanEdit}">
						<c:choose>
							<c:when test="${instrument.scrapped}">
								<a href="#" class="mainlink unscrapInstrument" onclick="event.preventDefault(); confirmScrapInstrumentChange(false, ${instrument.plantid}, null); "><spring:message code="instrument.unscrap" /></a>
							</c:when>
							<c:otherwise>
								<a href="#" class="mainlink scrapInstrument" onclick="event.preventDefault(); confirmScrapInstrumentChange(true, ${instrument.plantid}, null); "><spring:message code="instrument.scrap" /></a>
							</c:otherwise>
						</c:choose>
					</c:if>
				</div>						
				<!-- clear floats and restore page flow -->
				<div class="clear"></div>
			</div>
			<!-- end of search heading info -->
			
			<!-- instrument elements -->
			<fieldset <c:if test="${instrument.scrapped}">class="BER"</c:if> >					
				<ol>									
					<li>
						<label><spring:message code="web.barcode.label" /></label>
						<span id="plantid">${instrument.plantid}</span>									
					</li>
					<li>
						<label><spring:message code="web.instrumentmodel" /></label>
						<span>
							<cwms:showmodel instrumentmodel="${instrument.model}" hideTypology="true" />
							<c:if test="${instrument.thread != null}">
								<c:choose>
									<c:when test="${instrument.thread.size.length > 0}">
										${instrument.thread.size}
									</c:when>
									<c:otherwise>
										&nbsp;
									</c:otherwise>
								</c:choose>
								<c:if test="${instrument.thread.type != null}">
									(${instrument.thread.type.type})
								</c:if>							
							</c:if>
						</span>
					</li>
					<c:if test="${command.instrument.model.modelMfrType != null && command.instrument.model.modelMfrType == 'MFR_GENERIC'}">
						<li>
							<label><spring:message code="manufacturer"/></label>
							${instrument.mfr.name}
						</li>
						<li>
							<label><spring:message code='model'/></label>
							${instrument.modelname}
						</li>
					</c:if>
					<li>
						<label><spring:message code="web.serialno.label" /></label>
						<span>${instrument.serialno}</span>
					</li>
					<li>
						<label><spring:message code="web.plantno.label" /></label>
						<span>${instrument.plantno}</span>
					</li>
				</ol>
				
				<!-- left floated column -->
				<div class="columnLeft-4">

					<ol>
						<li>
							<label><spring:message code="web.subdivision.label" /></label>
							<span><instrument:subdivFilter subdiv="${instrument.con.sub}" /></span>
						</li>
						<li>
							<label><spring:message code="web.owner.label" /></label>
							<span>${instrument.con.name}</span>
						</li>
						<li>
							<label><spring:message code="web.address.label" /></label>
							<div class="float-left padtop">
								<t:showCompactFullAddress address="${instrument.add}" edit="false" copy="false" />
							</div>										
							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
						</li>
						<li>
							<label><spring:message code="web.location.label" /></label>
							<span>${instrument.loc.location}</span>
						</li>
						<li>
							<label><spring:message code="viewinstrument.customerdescription" /></label>
							<span>
								<c:if test="${instrument.customerDescription != null}">${instrument.customerDescription}</c:if>&nbsp;
							</span>
						</li>
						<li>
							<label><spring:message code="editinstr.calibrationinstructions" /></label>
							<span>
								<c:if test="${calibrationinstructions != null}">${calibrationinstructions}</c:if>&nbsp;
							</span>
						</li>
						<c:if test="${not instrument.scrapped}">
							<li class="inlineCalIndicator">
								<label><spring:message code="web.calduedate.label" /></label>
								<c:if test="${instrument.status!='DO_NOT_RECALL'}">
                                    <div class="float-left relative">
										<spring:message code="instrlink.calperiodpassed" var="msg" />
										<cwms:calTimescaleGraphic calIntervalUnit="${instrument.calFrequencyUnit}" 
										calInterval="${instrument.calFrequency}" calDueDate="${instrument.nextCalDueDate}" hoverMessageBegining="${msg}"/>
                                        <div class="nextCalDueDate">
                                            <span class="nextCalDueDate bold"><instrument:showFormattedCalDueDate instrument="${instrument}" /></span>
                                            (<span class="recallRuleInterval">
                                              ${command.CalRecallInterval.interval}
                                            </span> ${command.CalRecallInterval.translatedUnit})
                                        </div>
                                    </div>
								</c:if>
								<div class="clear"></div>
							</li>
							<c:if test="${calExtensionAllowed == true && command.calextension != null}">
								<li>
									<label><spring:message code="web.calextensionlabel"/></label>
                                    <span style="color: ${calExtensionDateColor}">
                                        <fmt:formatDate value="${command.calextension.extensionEndDate}" type="date" dateStyle="SHORT"/>
                                    </span>
								</li>
							</c:if>
                            <c:if test = "${instrument.calExpiresAfterNumberOfUses}">
                                <li>
                                    <instrument:usages instrument="${instrument}"/>
                                </li>
                            </c:if>
							<li>
								<viewinstrument:iterimcaldate currentInterimCalRecallRuleInterval="${command.currentInterimCalRecallRuleInterval}" instrument="${instrument}"/>
							</li>
							<li>
								<viewinstrument:maintenancedate currentMaintenanceRecallRuleInterval="${command.currentMaintenanceRecallRuleInterval}" instrument="${instrument}"/>
							</li>
						</c:if>

					</ol>
					
				</div>
				<!-- end of left column -->
				
				<!-- right floated column -->
				<div class="columnRight-4">
					<ol>
						<c:choose>
							<c:when test="${instrument.scrapped}">
							<li>
								<label><spring:message code="web.scrappedby.label" /></label>
								<span>
									<c:choose>
										<c:when test="${instrument.scrappedBy != null}">
											${instrument.scrappedBy}
										</c:when>
										<c:otherwise>
											<spring:message code="web.unknown" />
										</c:otherwise>
									</c:choose>
								</span>
							</li>
							<li>
								<label><spring:message code="web.scrappedon.label" /></label>
								<span>
									<c:choose>
										<c:when test="${instrument.scrappedOn != null}">
											<fmt:formatDate value="${instrument.scrappedOn}" type="date" dateStyle="SHORT"/>
										</c:when>
										<c:otherwise>
											<spring:message code="web.unknown" />
										</c:otherwise>
									</c:choose>
								</span>
							</li>
							<li>
								<label><spring:message code="web.scrappedonjob.label" /></label>
								<span>
									<c:choose>
										<c:when test="${instrument.scrappedOnJI != null}">
											<links:jobnoLinkDWRInfo rowcount="01" jobno="${instrument.scrappedOnJI.job.jobno}" jobid="${instrument.scrappedOnJI.job.jobid}" copy="false" />
											<spring:message code="web.item" /><links:jobitemLinkDWRInfo jobitem="${instrument.scrappedOnJI}" jicount="false" rowcount="01" />    
										</c:when>
										<c:otherwise>
											<spring:message code="web.unknown" />
										</c:otherwise>
									</c:choose>
								</span>
							</li>
							</c:when>
							<c:otherwise>
							<li>
								<label><spring:message code="web.addedby.label" /></label>
								<span>${instrument.addedBy.name}</span>
							</li>
							<li>
								<label><spring:message code="web.addedon.label" /></label>
								<span><fmt:formatDate value="${instrument.addedOn}" type="date" dateStyle="SHORT" /></span>
							</li>
							<li>
								<label><spring:message code="web.storagetype.label" /></label>
								<span>${instrument.storageType.name}</span>
							</li>
							<li>
								<label><spring:message code="web.usagetype.label" /></label>
								<span>${instrument.usageType.name}</span>
							</li>									
							<li>
								<label><spring:message code="web.status.label" /></label>
								<span>
									<c:if test="${instrument.status != null}">${instrument.status.getStatus()}</c:if>&nbsp;
								</span>
							</li>
							
							<li>
								<label><spring:message code="web.customermanaged"/></label>
								<span><c:out value="${instrument.customerManaged ? 'Yes' : 'No' }"></c:out></span>
							</li>
							<li>
								<label><spring:message code="viewinstrument.remarks"/></label>
								<span>
									<c:choose>
										<c:when test="${ not empty instrument.instrumentComplementaryField }">
											<c:choose>
												<c:when test="${ not empty instrument.instrumentComplementaryField.clientRemarks }">
													${ instrument.instrumentComplementaryField.clientRemarks }
												</c:when>
												<c:otherwise>
													&nbsp;
												</c:otherwise>
											</c:choose>
										</c:when>
										<c:otherwise>
											&nbsp;
										</c:otherwise>
									</c:choose>
								</span>
							</li>
							<li>
								<c:if test="${hasCalVerification}">
									<label><spring:message code="web.statusoflastcertificate"/></label>
									<span><c:out value="${command.calVerificationStatus}"/></span>
								</c:if>
                            </li>
								
							</c:otherwise>						
						</c:choose>
					</ol>
					
				</div>
				<!-- end of right column -->
				<table>

					<c:if test="${userCanCreateCert}">
						<c:url var="createUrl" value="addcertificate.htm?instid=${instrument.plantid}" />
						<tr>
							<td class="instrumentAction">
								<button type="button" id="performAction" onClick="event.preventDefault(); window.location.href='${createUrl}'; ">
									<spring:message code="instrument.performaction"/>
								</button>
							</td>
						</tr>
					</c:if>

					<c:if test="${not empty command.lastCert && not empty command.labeltemplate}">
						<c:url var="validityUrl" value="custcallabel?certId=${command.lastCert.certid}&instrumentId=${instrument.plantid}&templateKey=${command.labeltemplate.propertyName}" />
						<tr>
							<td  class="instrumentAction">
								<button type="button" id="validitylabel" onClick="event.preventDefault(); window.location.href='${validityUrl}'; ">
									<spring:message code="web.validitylabel"/>
								</button>
							</td>
						</tr>
					</c:if>

					<c:set var="displayRecordUse" value="${instrument.calExpiresAfterNumberOfUses
					&& !(instrument.usesSinceLastCalibration >= instrument.permittedNumberOfUses) ? '' : 'display: none'}"/>

						<tr id="recordUseTr" style="${displayRecordUse}">
							<td class="instrumentAction">
								<button type="button" id="recordUse">
									<spring:message code="web.recorduse"/>
								</button>
							</td>
						</tr>


					<c:url var="historyUrl" value="instrumenthistorydocument.htm?plantid=${instrument.plantid}" />
					<tr>
						<td  class="instrumentAction">
							<button type="button" id="historyreport" onClick="event.preventDefault(); window.location.href='${historyUrl}'; ">
								<spring:message code="web.instrumenthistory"/>
							</button>
						</td>
					</tr>

				</table>

			</fieldset>
			<!-- end of instrument elements -->
			<c:set var="certsize" value="${command.certlinks.size() + instrument.instCertLinks.size()}" />
			
			<!-- check recall count here as we need to check for items excluded from the recall -->
			<c:set var="recallCount" value="0" />
			<c:forEach var="recall" items="${instrument.recalls}">
				<c:if test="${not recall.excludeFromNotification}">
					<c:set var="recallCount" value="${recallCount + 1}" />
				</c:if>
			</c:forEach>
			
			<!-- sub navigation menu which displays and hides sections of page as well as linking to external pages -->
			<div id="subnav">
				<dl>
					<c:if test="${userCanEdit}">
						<dt><a href="editinstrument.htm?plantid=${instrument.plantid}" <c:if test="${instrument.scrapped}"> disabled="disabled" title="Instrument editing disabled as unit B.E.R." </c:if> >Edit</a></dt>
					</c:if>
					<dt><a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'jobs-tab', false); " id="jobs-link" class="selected"><spring:message code="instrument.navig.jobs" arguments="${instrument.jobItems.size()}" /></a></dt>
					<dt><a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'certs-tab', false); " id="certs-link" ><spring:message code="instrument.navig.certificates" arguments="${certsize}" /></a></dt>
<%-- 				<dt><a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'recall-tab', false); " id="recall-link" ><spring:message code="instrument.navig.recall" arguments="${recallCount}" /></a></dt> --%>
					<dt><a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'validation-tab', false); " id="validation-link" ><span ${valStyle}><spring:message code="instrument.navig.validation" arguments="${instrument.issues.size()}" /></span></a></dt>						
				    <dt><a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'rep-tab', false); " id="rep-link" ><spring:message code="instrument.navig.replacement" /></a></dt>
					<dt><a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'files-tab', false); " id="files-link"><spring:message code="instrument.navig.files"/></a></dt>
					<dt><a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'flexiblefields-tab', false); " id="flexiblefields-link" title="<spring:message code="viewinstrument.flexiblefields" />"><spring:message code="viewinstrument.flexiblefields" /></a></dt>
					<dt><a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'history-tab', false); " id="history-link"><spring:message code="instrument.navig.history" arguments="${instrument.instrumentHistoryTotalCount}" /></a></dt>
					<dt><a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'complementary-tab', false); " id="complementary-link" title="<spring:message code="viewinstrument.subnavtitlecomplementary" />"><spring:message code="viewinstrument.complementary" /></a></dt>
				</dl>
			</div>
			<!-- end of sub navigation menu -->
			
			<!-- div containing instrument job history -->
			<div id="jobs-tab">
				
				<table class="default instrumentJobHistory" summary="This table displays all instrument job history">
					<thead>
						<tr>
							<td colspan="6"><spring:message code="instrument.job.title" arguments="${instrument.jobItems.size()}" /></td>
						</tr>
						<tr>
							<th class="jobno" scope="col"><spring:message code="web.jobno" /></th>
							<th class="itemno" scope="col"><spring:message code="web.itemno" /></th>
							<th class="cont" scope="col"><spring:message code="web.contact" /></th>
							<th class="datein" scope="col"><spring:message code="web.datein" /></th>
							<th class="datecomp" scope="col"><spring:message code="web.datecomplete" /></th>
							<th class="caltype" scope="col"><spring:message code="web.caltype" /></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="6">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody>
						<c:choose>
							<c:when test="${instrument.jobItems.size() < 1}">
								<tr>
									<td colspan="6" class="text-center bold">
										<spring:message code="instrument.job.empty" />
									</td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:forEach var="jobitem" items="${instrument.jobItems}">
									<tr style=" background-color: ${jobitem.calType.serviceType.displayColour}; ">
										<td class="jobno">
											<instrument:jobnoLinkWeb job="${jobitem.job}" />											
										</td>
										<td class="itemno">
											${jobitem.itemNo}													
										</td>
										<td class="cont">${jobitem.job.con.name}</td>
										<td class="datein"><fmt:formatDate value="${jobitem.dateIn}" dateStyle="medium" type="date" /></td>
										<td class="datecomp"><fmt:formatDate value="${jobitem.dateComplete}" dateStyle="medium" type="date" /></td>
										<td class="caltype"><t:showTranslationOrDefault translations="${jobitem.calType.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/></td>
									</tr>
								</c:forEach>	
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
												
			</div>
			<!-- end of instrument job history -->
			
			<c:set var="certColSpan" value="${(hasCalVerification && hasCustomeCertificateValidation)?'9':
			                                    ((hasCalVerification != hasCustomeCertificateValidation)?'8':'7')}" />
			<!-- div containing certificate history for this instrument -->
			<div id="certs-tab" class="hid">
				<table class="default instrumentCertHistory" summary="This table displays all certificate history for instrument">
					<thead>
						<tr>
							<td colspan="${certColSpan}" >
								<spring:message code="instrument.certif.title" arguments="${certsize}"/>
                                <c:if test="${(!empty command.certlinks || !empty command.instCertLinks) && hasDeviation == true }">
                                    <button id="trendAnalysis" style="float: right"><spring:message code="viewinstrument.trendanalysis"/></button>
                                </c:if>

							</td>
						</tr>
						<tr>
							<th class="jobno" scope="col"><spring:message code="web.jobno" /></th>
							<th class="itemno" scope="col"><spring:message code="web.itemno" /></th>										
							<th class="caltype" scope="col"><spring:message code="web.caltype" /></th>
							<th class="caldate" scope="col"><spring:message code="web.caldate" /></th>
							<th class="certdate" scope="col"><spring:message code="web.certdate" /></th>
							<th class="duration" scope="col"><spring:message code="web.duration" /></th>
							<th class="certno" scope="col"><spring:message code="web.certificateno" /></th>
							<c:if test="${hasCalVerification}">
								<th class="verification"><spring:message code="viewinstrument.calverificationstatus"/></th>
							</c:if>
							<c:if test="${hasCustomeCertificateValidation}">
								<th class="validation" scope="col"><spring:message code="web.certificates.accepted" /></th>
							</c:if>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="${certColSpan}">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody>
						<c:choose>
							<c:when test="${empty command.certlinks && empty command.instCertLinks}">
								<tr>
									<td colspan="${certColSpan}" class="text-center bold">
										<spring:message code="instrument.certif.empty" />
									</td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:forEach var="certlink" items="${command.certlinks}">
									<tr style=" background-color: ${certlink.jobItem.calType.serviceType.displayColour}; ">
										<td class="jobno">
											<instrument:jobnoLinkWeb job="${certlink.jobItem.job}" />
										</td>
										<td class="itemno">
											${certlink.jobItem.itemNo}
										</td>												
										<td class="caltype"><t:showTranslationOrDefault translations="${certlink.jobItem.calType.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/></td>
										<td class="caldate"><fmt:formatDate value="${certlink.cert.calDate}" dateStyle="medium" type="date" /></td>
										<td class="certdate"><fmt:formatDate value="${certlink.cert.certDate}" dateStyle="medium" type="date" /></td>
										<td class="duration">
											<c:if test="${certlink.cert.duration != null}">
												${certlink.cert.duration}&nbsp;${certlink.cert.unit.getName(certlink.cert.duration)}
											</c:if>
										</td>
										<td class="certno">
											<a href="viewcert.htm?cert=${certlink.cert.certid}" class="mainlink">${certlink.cert.certno}</a><br>
                                            <c:if test="${!empty certlink.cert.certClass}">
                                                <span style="align-content: center">(<c:out value="${certlink.cert.certClass.description}"/>)</span>
                                            </c:if>
                                        </td>
										<c:if test="${hasCalVerification}">
											<td class="verification">${certlink.cert.calibrationVerificationStatus.getName()}</td>
										</c:if>

										<web:certificateValidationLinks cert="${certlink.cert}" validationrRequired="${hasCustomeCertificateValidation}" validationAllowed="${userCanValidateCert}"/>

									</tr>
									<c:if test="${not empty certlink.cert.remarks || not empty certlink.cert.validation.note}">
										<tr style="background-color: ${certlink.cert.calType.serviceType.displayColour};">
											<spring:message var="cert_remarks" code="instrument.certif.certificateremarks"/>
                                            <spring:message var="accept_remarks" code="instrument.certif.acceptanceremarks"/>
											<td colspan="${certColSpan}" class="left bold">
												${certlink.cert.remarks}
												${not empty certlink.cert.remarks ? cert_remarks += certlink.cert.remarks : ""}
                                                &nbsp;
                                                ${not empty certlink.cert.validation.note ? accept_remarks += certlink.cert.validation.note : ""}
											</td>
										</tr>
									</c:if>
								</c:forEach>
								<c:forEach var="icl" items="${command.instCertLinks}">
									<tr style=" background-color: ${icl.cert.calType.serviceType.displayColour}; ">
										<td class="jobno">N/A</td>
										<td class="itemno">N/A</td>												
										<td class="caltype"><t:showTranslationOrDefault translations="${icl.cert.calType.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/></td>
										<td class="caldate"><fmt:formatDate value="${icl.cert.calDate}" dateStyle="medium" type="date" /></td>
										<td class="certdate"><fmt:formatDate value="${icl.cert.certDate}" dateStyle="medium" type="date" /></td>
										<td class="duration">
											<c:if test="${icl.cert.duration != null}">
												${icl.cert.duration}&nbsp;${icl.cert.unit.getName(icl.cert.duration)}
											</c:if>
										</td>
										<td class="certno"><a href="viewcert.htm?cert=${icl.cert.certid}" class="mainlink">${icl.cert.certno}</a></td>
										<c:if test="${hasCalVerification}">
											<td>${icl.cert.calibrationVerificationStatus.getName()}</td>
										</c:if>
										<web:certificateValidationLinks cert="${icl.cert}"
																		validationrRequired="${hasCustomeCertificateValidation}"
																		validationAllowed="${userCanValidateCert}"/>
									</tr>
									<c:if test="${not empty icl.cert.remarks || not empty icl.cert.validation.note}">
										<tr style="background-color: ${icl.cert.calType.serviceType.displayColour};">
											<td colspan="${certColSpan}" class="left bold">
                                                <spring:message var="cert_remarks" code="instrument.certif.certificateremarks"/>
                                                <spring:message var="accept_remarks" code="instrument.certif.acceptanceremarks"/>

												${not empty icl.cert.remarks ? cert_remarks += icl.cert.remarks : ""}
                                                &nbsp;
                                                ${not empty icl.cert.validation.note ? accept_remarks += icl.cert.validation.note : ""}
											</td>
										</tr>
									</c:if>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>				
			</div>
			<!-- end of the instrument certificate history -->
				

																	
			<!-- this section displays all validation related information for current instrument -->
			<div id="validation-tab" class="hid">
			
				<table class="default instrumentValidationIssues" summary="This table displays all validation issues for instrument">																
					<thead>
						<tr>
							<td colspan="6">
								<spring:message code="instrument.validation.title" />
								<c:if test="${userCanEdit}">
									- <a href="#" class="mainlink" onclick="event.preventDefault(); createValidationIssue(${instrument.plantid}); " title=""><spring:message code="instrument.validation.title2" /></a>
								</c:if>
							</td>
						</tr>
						<tr>
							<th class="issue" scope="col"><spring:message code="web.issue" /></th>
							<th class="raisedon" scope="col"><spring:message code="web.raisedon" /></th>
							<th class="raisedby" scope="col"><spring:message code="web.raisedby" /></th>
							<th class="resolvedon" scope="col"><spring:message code="web.resolvedon" /></th>										
							<th class="resolvedby" scope="col"><spring:message code="web.resolvedby" /></th>
							<th class="status" scope="col"><spring:message code="web.status" /></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="6">
								<c:if test="${userCanEdit}">
									<a href="#" class="mainlink-float" onclick="event.preventDefault(); createValidationIssue(${instrument.plantid}); " title=""><spring:message code="instrument.validation.title2" /></a>
								</c:if>	&nbsp;
							</td>
						</tr>
					</tfoot>
					<tbody>
						<c:choose>
							<c:when test="${command.validation.size() < 1}">
								<tr>
									<td colspan="6" class="bold text-center">
										<spring:message code="instrument.validation.empty" />
										<c:if test="${userCanEdit}">
											- <a href="#" class="mainlink" onclick="event.preventDefault(); createValidationIssue(${instrument.plantid}); " title=""><spring:message code="instrument.validation.title2" /></a>
										</c:if>
									</td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:forEach var="v" items="${command.validation}">
									<tr>
										<td class="issue">${v.issue}</td>
										<td class="raisedon">
										<fmt:formatDate value="${v.raisedOn}" dateStyle="medium" type="date"/>}
										</td>
										<td class="raisedby">${v.raisedBy.name}</td>
										<td class="resolvedon">
											<c:choose>
												<c:when test="${v.status.requiringAttention}">
													- <spring:message code="web.awaitingaction" /> -
												</c:when>
												<c:otherwise>
													<fmt:formatDate value="${v.resolvedOn}" dateStyle="medium" type="date"/>}
												</c:otherwise>
											</c:choose>
										</td>											
										<td class="resolvedby">
											<c:choose>
												<c:when test="${v.status.requiringAttention}">
													- <spring:message code="web.awaitingaction" /> -
												</c:when>
												<c:otherwise>
													${v.resolvedBy.name}
												</c:otherwise>
											</c:choose>
										</td>
										<td class="status"><t:showTranslationOrDefault translations="${v.status.nametranslations}" defaultLocale="${defaultlocale}"/></td>
									</tr>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
				
			</div>
			<!-- end of the instrument validation section -->
			
			<!-- this section displays all replacement content this instrument -->
			<div id="rep-tab" class="hid">
				
				<table class="default instrumentReplacement" summary="This table displays the instrument replacement or message of no replacement">	
					<thead>																						
						<tr>
							<td colspan="4"><spring:message code="instrument.replacement.title" />
								<button id="linkreplacement"
										${instrument.replacement != null ?  "style = 'display: none; float: right'" : "style='float: right'"}>
									<spring:message code="viewinstrument.linkreplacementinstrument"/>
								</button>
                                <button id="unlinkreplacement"
										${instrument.replacement == null ?  "style = 'display: none; float: right'" : "style='float: right'"}>
									<spring:message code="viewinstrument.unlinkreplacementinstrument"/>
								</button>
							</td>
						</tr>
						<tr>
							<th class="barcode" scope="col"><spring:message code="web.barcode" /></th>
							<th class="instmod" scope="col"><spring:message code="instmodelname" /></th>
							<th class="repby" scope="col"><spring:message code="web.replacedby" /></th>
							<th class="repon" scope="col"><spring:message code="web.replacedon" /></th>
						</tr>												
					</thead>
					<tfoot>
						<tr>
							<td colspan="4">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody>
						<c:choose>
							<c:when test="${instrument.replacement != null}">
								<tr id="replacementInstrument">
									<td><links:showInstrumentLink instrument="${instrument.replacement}" /></td>
									<td><instmodel:showInstrumentModelLink instrument="${instrument.replacement}" caltypeid="0"/></td>
									<%--<td>
										<web:WebInstrumentLink customerDescription="${instrument.replacement.customerDescription}" modelname="${instrument.replacement.modelname}"
															   plantid="${instrument.replacement.plantid}" scrapped="${instrument.replacement.scrapped}"
															   displayCalTimescale="false"
															   calIntervalUnit="${instrument.replacement.calFrequencyUnit}"
															   calInterval="${instrument.replacement.calFrequency}"
															   calDueDate="${instrument.replacement.nextCalDueDate}"
															   instrumentmodel="${instrument.replacement.model}"/>
									</td>--%>
									<td>${instrument.replacedBy.name}</td>
									<td><fmt:formatDate value="${instrument.replacedOn}" type="date" dateStyle="SHORT" /></td>
								</tr>
							</c:when>
							<c:otherwise>	
								<tr id="replacementInstrument">
									<td colspan="4" class="text-center bold"><spring:message code="instrument.replacement.empty" /></td>
								</tr>											
							</c:otherwise>
						</c:choose>
					</tbody>								
				</table>
																						
				<table class="default instrumentReplace" summary="This table displays any instruments this one has replaced">	
					<thead>																						
						<tr>
							<td colspan="4"><spring:message code="instrument.replacement.title2" arguments="${instrument.replacementTo.size()}" /></td>
						</tr>
						<tr>
							<th class="barcode" scope="col"><spring:message code="web.barcode" /></th>
							<th class="instmod" scope="col"><spring:message code="web.instrumentmodel" /></th>
							<th class="repby" scope="col"><spring:message code="web.replacedby" /></th>
							<th class="repon" scope="col"><spring:message code="web.replacedon" /></th>
						</tr>												
					</thead>
					<tfoot>
						<tr>
							<td colspan="4">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody>
						<c:choose>
							<c:when test="${instrument.replacementTo.size() > 0}">
								<c:forEach var="rep" items="${instrument.replacementTo}">
									<tr>
										<td><links:showInstrumentLink instrument="${instrument.replacement}" /></td>
										<td><instmodel:showInstrumentModelLink instrument="${rep}" caltypeid="0"/></td>
										<%--<td>
											<web:WebInstrumentLink customerDescription="${rep.customerDescription}" modelname="${rep.modelname}"
																   plantid="${rep.plantid}" scrapped="${rep.scrapped}"
																   displayCalTimescale="false"
																   calIntervalUnit="${rep.calFrequencyUnit}"
																   calInterval="${rep.calFrequency}"
																   calDueDate="${rep.nextCalDueDate}"
																   instrumentmodel="${rep.model}"/>
										</td>--%>
										<td>${rep.replacedBy.name}</td>
										<td><fmt:formatDate value="${rep.replacedOn}" type="date" dateStyle="SHORT" /></td>
									</tr>
								</c:forEach>
							</c:when>
							<c:otherwise>									
								<tr>
									<td colspan="4" class="text-center bold"><spring:message code="instrument.replacement.empty2" /></td>
								</tr>											
							</c:otherwise>
						</c:choose>
					</tbody>								
				</table>
												
			</div>
			<!-- end of the instrument replacement content section -->
			
			<!-- this section displays all files for this instrument -->
			<div id="files-tab" class="hid">
				<cwms-upload-file system-component-id="${command.sc.componentId}" entity-id1="${command.instrument.plantid}" entity-id2=""
 			files-name="${filesName}" rename-to="" web="${true}"></cwms-upload-file>
				<web:showFiles rootFiles="${command.scRootFiles}" id="${instrument.plantid}" entity="${instrument}" identifier="${instrument.plantid}" deleteFiles="true" sc="${command.sc}" rootTitle="Files for instrument" />
			</div>
			<!-- end of the files for this instrument section -->
			
			<!-- this section displays flexible fields for this instrument -->
			<div id="flexiblefields-tab" class="hid">
				<table class="default instFlexibleField" summary="This table lists all flexible fields for the customer">
					<thead>
					<tr>
						<th class="field" scope="col"><spring:message code="viewinstrument.field"/></th>
						<th class="fieldtype" scope="col"><spring:message code="type"/></th>
						<th class="fieldvalue" scope="col"><spring:message code="viewinstrument.fieldvalue"/></th>
						<th class="fieldvalue" scope="col"><spring:message code="viewinstrument.action"/></th>
					</tr>
					</thead>
					<tbody>
                        <c:choose>
                            <c:when test="${flexiblefields.size() > 0}" >
                                <c:forEach var="flexiblefield" items="${flexiblefields}">
                                    <tr>
                                        <td>${flexiblefield.fieldName}</td>
                                        <td>${flexiblefield.fieldType}</td>
                                        <td>
                                            <c:choose>
                                                <c:when test="${flexiblefield.value != null}">
                                                    ${flexiblefield.value}
                                                </c:when>
                                                <c:otherwise>
                                                    &nbsp;
                                                </c:otherwise>
                                            </c:choose>
                                        </td>
                                        <td>
                                        	<c:if test="${flexiblefield.isEditable}">
                                            <c:choose>
                                                <c:when test="${flexiblefield.valueId != null}">
                                                    <a href="addeditflexiblefieldvalue.htm?plantid=${instrument.plantid}&valueid=${flexiblefield.valueId}" class="mainlink"><spring:message code="edit"/></a>
                                                </c:when>
                                                <c:otherwise>
                                                    <a href="addeditflexiblefieldvalue.htm?plantid=${instrument.plantid}&fielddefid=${flexiblefield.fieldDefinitionId}" class="mainlink"><spring:message code="edit"/></a>
                                                </c:otherwise>
                                            </c:choose>
                                            </c:if>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </c:when>
                            <c:otherwise>
                                <tr><td colspan=4><spring:message code="viewinstrument.noflexiblefields"/></td></tr>
                            </c:otherwise>
                        </c:choose>
					</tbody>
				</table>
			</div>		
			<!-- end of flexible fields info box -->
			
			<!-- begin instrument history section -->					
			<div id="history-tab" class="hid">
				
				<table class="default instrumentHistory" summary="This table displays any instrument history">										
					<thead>
						<tr>
							<td colspan="5">
								<c:choose>
									<c:when test="${instrument.addedOn != null}">
										<fmt:formatDate value='${instrument.addedOn}' type="date" dateStyle="SHORT" var="addedOn" />
										<spring:message code="instrument.history.title" arguments="${addedOn}, ${instrument.addedBy.name}" />
									</c:when>
									<c:otherwise>
										<spring:message code="instrument.history.empty" />
									</c:otherwise>
								</c:choose>
							</td>
						</tr>
						<c:if test="${instrument.history.size() > 0}">																					
							<tr>
								<td colspan="5"><spring:message code="instrument.history.count" arguments="${instrument.instrumentHistoryTotalCount}" /></td>
							</tr>
						</c:if>
						<tr>
							<th class="field" scope="col"><spring:message code="web.field" /></th>
							<th class="changefrom" scope="col"><spring:message code="web.changedfrom" /></th>
							<th class="changeto" scope="col"><spring:message code="web.changedto" /></th>
							<th class="changeby" scope="col"><spring:message code="web.changedby" /></th>
							<th class="changeon" scope="col"><spring:message code="web.changedon" /></th>
						</tr>												
					</thead>
					<tfoot>
						<tr>
							<td colspan="5">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody>
						<c:choose>
							<c:when test="${instrument.history.size() > 0}">
								<c:forEach var="history" items="${instrument.history}">
									<c:if test="${history.contactUpdated}">
										<tr>	
											<td><spring:message code="web.owner" /></td>
											<td>${history.oldContact.name}</td>
											<td>${history.newContact.name}</td>
											<td>${history.changeBy.name}</td>
											<td><fmt:formatDate value="${history.changeDate}" type="date" dateStyle="SHORT"/></td>
										</tr>
									</c:if>
									<c:if test="${history.addressUpdated}">
										<tr>
											<td><spring:message code="web.address" /></td>
											<td>${history.oldAddress.addr1}</td>
											<td>${history.newAddress.addr1}</td>
											<td>${history.changeBy.name}</td>
											<td><fmt:formatDate value="${history.changeDate}" type="date" dateStyle="SHORT"/></td>
										</tr>
									</c:if>
									<c:if test="${history.serialNoUpdated}">
										<tr>
											<td><spring:message code="web.serialno" /></td>
											<td>${history.oldSerialNo}</td>
											<td>${history.newSerialNo}</td>
											<td>${history.changeBy.name}</td>
											<td><fmt:formatDate value="${history.changeDate}" type="date" dateStyle="SHORT"/></td>
										</tr>
									</c:if>
									<c:if test="${history.plantNoUpdated}">
										<tr>
											<td><spring:message code="web.plantno" /></td>
											<td>${history.oldPlantNo}</td>
											<td>${history.newPlantNo}</td>
											<td>${history.changeBy.name}</td>
											<td><fmt:formatDate value="${history.changeDate}" type="date" dateStyle="SHORT"/></td>
										</tr>
									</c:if>
									<c:if test="${history.statusUpdated}">
										<tr>
											<td><spring:message code="web.status" /></td>
											<td>${history.oldStatus.getStatus()}</td>
											<td>${history.newStatus.getStatus()}</td>
											<td>${history.changeBy.name}</td>
											<td><fmt:formatDate value="${history.changeDate}" type="date" dateStyle="SHORT"/></td>
										</tr>
									</c:if>
								</c:forEach>
							</c:when>
							<c:otherwise>									
								<tr>
									<td colspan="5" class="text-center bold"><spring:message code="instrument.history.empty2" /></td>
								</tr>												
							</c:otherwise>
						</c:choose>
					</tbody>										
				</table>
			</div>
			<!-- end instrument history section -->
			
			<!-- begin instrument complementary section -->					
			<div id="complementary-tab" class="hid">
				<fieldset>	
					<c:choose>
						<c:when test="${ not empty instrument.instrumentComplementaryField }">
							<c:if test="${userCanEdit}">
								<a href='webeditcomplementaryfields.htm?plantid=${instrument.plantid}' class="mainlink-float"><spring:message code="viewinstrument.complementaryedit"/></a><br>
							</c:if>
							<ol>
								<li>
									<label><spring:message code="viewinstrument.accreditation" />:</label>
									<span>
										<c:choose>
											<c:when test="${ not empty instrument.instrumentComplementaryField.accreditation }">
												${ instrument.instrumentComplementaryField.accreditation }
											</c:when>
											<c:otherwise>
												&nbsp;
											</c:otherwise>
										</c:choose>
									</span>
								</li>
								<li>
									<label><spring:message code="viewinstrument.deliverystatus" />:</label>
									<span>
										<c:choose>
											<c:when test="${ not empty instrument.instrumentComplementaryField.deliveryStatus }">
												${ instrument.instrumentComplementaryField.deliveryStatus }
											</c:when>
											<c:otherwise>
												&nbsp;
											</c:otherwise>
										</c:choose>
									</span>
								</li>
								<li>
									<label><spring:message code="viewinstrument.firstusedate" />:</label>
									<span>
										<c:choose>
											<c:when test="${ not empty instrument.instrumentComplementaryField.firstUseDate }">
				<fmt:formatDate dateStyle="medium" type="date" value="${instrument.instrumentComplementaryField.firstUseDate}"/>
											</c:when>
											<c:otherwise>
												&nbsp;
											</c:otherwise>
										</c:choose>
									</span>
								</li>
								<li>
									<label><spring:message code="viewinstrument.purchasecost" />:</label>
									<span>
										<c:choose>
											<c:when test="${ not empty instrument.instrumentComplementaryField.purchaseCost }">
												${ instrument.instrumentComplementaryField.purchaseCost }
											</c:when>
											<c:otherwise>
												&nbsp;
											</c:otherwise>
										</c:choose>
									</span>
								</li>
								<li>
									<label><spring:message code="viewinstrument.purchasedate" />:</label>
									<span>
										<c:choose>
											<c:when test="${ not empty instrument.instrumentComplementaryField.purchaseDate }">
				<fmt:formatDate dateStyle="medium" type="date" value="${instrument.instrumentComplementaryField.purchaseDate}"/>
											</c:when>
											<c:otherwise>
												&nbsp;
											</c:otherwise>
										</c:choose>
									</span>
								</li>
								<li>
									<label><spring:message code="viewinstrument.formerbarcode" />:</label>
									<span>
										<c:choose>
											<c:when test="${ not empty instrument.instrumentComplementaryField.formerBarCode }">
												${ instrument.instrumentComplementaryField.formerBarCode }
											</c:when>
											<c:otherwise>
												&nbsp;
											</c:otherwise>
										</c:choose>
									</span>
								</li>
								<li>
									<label><spring:message code="viewinstrument.formerfamily" />:</label>
									<span>
										<c:choose>
											<c:when test="${ not empty instrument.instrumentComplementaryField.formerFamily }">
												${ instrument.instrumentComplementaryField.formerFamily }
											</c:when>
											<c:otherwise>
												&nbsp;
											</c:otherwise>
										</c:choose>
									</span>
								</li>
								<li>
									<label><spring:message code="viewinstrument.formercharacteristics" />:</label>
									<span>
										<c:choose>
											<c:when test="${ not empty instrument.instrumentComplementaryField.formerCharacteristics }">
												${ instrument.instrumentComplementaryField.formerCharacteristics }
											</c:when>
											<c:otherwise>
												&nbsp;
											</c:otherwise>
										</c:choose>
									</span>
								</li>
								<li>
									<label><spring:message code="viewinstrument.formerlocaldescription" />:</label>
									<span>
										<c:choose>
											<c:when test="${ not empty instrument.instrumentComplementaryField.formerLocalDescription }">
												${ instrument.instrumentComplementaryField.formerLocalDescription }
											</c:when>
											<c:otherwise>
												&nbsp;
											</c:otherwise>
										</c:choose>
									</span>
								</li>
								<li>
									<label><spring:message code="viewinstrument.formermanufacturer" />:</label>
									<span>
										<c:choose>
											<c:when test="${ not empty instrument.instrumentComplementaryField.formerManufacturer }">
												${ instrument.instrumentComplementaryField.formerManufacturer }
											</c:when>
											<c:otherwise>
												&nbsp;
											</c:otherwise>
										</c:choose>
									</span>
								</li>
								<li>
									<label><spring:message code="viewinstrument.formermodel" />:</label>
									<span>
										<c:choose>
											<c:when test="${ not empty instrument.instrumentComplementaryField.formerModel }">
												${ instrument.instrumentComplementaryField.formerModel }
											</c:when>
											<c:otherwise>
												&nbsp;
											</c:otherwise>
										</c:choose>
									</span>
								</li>
								<li>
									<label><spring:message code="viewinstrument.formername" />:</label>
									<span>
										<c:choose>
											<c:when test="${ not empty instrument.instrumentComplementaryField.formerName }">
												${ instrument.instrumentComplementaryField.formerName }
											</c:when>
											<c:otherwise>
												&nbsp;
											</c:otherwise>
										</c:choose>
									</span>
								</li>
								<li>
									<label><spring:message code="viewinstrument.provider" />:</label>
									<span>
										<c:choose>
											<c:when test="${ not empty instrument.instrumentComplementaryField.provider }">
												${ instrument.instrumentComplementaryField.provider }
											</c:when>
											<c:otherwise>
												&nbsp;
											</c:otherwise>
										</c:choose>
									</span>
								</li>
                                <c:if test="${hasDeviation}">
                                    <li>
                                        <label><spring:message code="viewinstrument.deviationunit"/></label>
                                        <span>
                                            <c:choose>
                                                <c:when test="${ not empty instrument.instrumentComplementaryField.deviationUnits}">
                                                    <cwms:besttranslation translations='${instrument.instrumentComplementaryField.deviationUnits.nameTranslation}'/>
                                                </c:when>
                                                <c:otherwise>
                                                    &nbsp;
                                                </c:otherwise>
                                            </c:choose>
                                        </span>
                                    </li>
									<li>
										<label><spring:message code="viewinstrument.nominalvalue" />:</label>
										<span>
										<c:choose>
											<c:when test="${ not empty instrument.instrumentComplementaryField.nominalValue }">
												${ instrument.instrumentComplementaryField.nominalValue }
											</c:when>
											<c:otherwise>
												&nbsp;
											</c:otherwise>
										</c:choose>
									</span>
									</li>
                                </c:if>
                                <c:if test="${hasCertClass}">
                                    <li>
                                        <label><spring:message code="certificate.class" />:</label>
                                        <span>
                                            <c:choose>
                                                <c:when test="${ not empty instrument.instrumentComplementaryField.certificateClass }">
                                                    ${ instrument.instrumentComplementaryField.certificateClass.description }
                                                </c:when>
                                                <c:otherwise>
                                                    &nbsp;
                                                </c:otherwise>
                                            </c:choose>
                                        </span>
                                    </li>
                                </c:if>
							</ol>
						</c:when>
						<c:otherwise>
							<h3><spring:message code="viewinstrument.nocomplimentary"/></h3>
							<c:if test="${userCanEdit}">
								<a href='webeditcomplementaryfields.htm?plantid=${instrument.plantid}' class="mainlink-float"><spring:message code="viewinstrument.complementaryadd"/></a><br>
							</c:if>
						</c:otherwise>
					</c:choose>
                </fieldset>

				<instrument:replacementInstrumentSearchDialogs/>

			</div>
			<!-- end instrument complementary section -->
		
			<!-- InstanceEndEditable -->
	  	</div>
		<!-- end of main box body content div -->

        <web:certificateValidationDialogs/>


	</jsp:body>
</t:webTemplate>
