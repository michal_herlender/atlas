<%-- File name: /trescal/core/admin/editdefaultnote.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="editdefaultnote.headertext" /></span>
    </jsp:attribute>
    <jsp:body>
		<div class="infobox">
			<form:form method="post" modelAttribute="form">
				<p>
					<a class="mainlink" href="<c:url value="/viewdefaultnotes.htm" />">
						<spring:message code="editdefaultnote.backtoviewnotes" />
					</a>				
				</p>
			
				<t:showErrors path="form.*" showFieldErrors="true" />
				<fieldset>
					<legend><spring:message code="editdefaultnote.headertext" /></legend>								
					<ol>
						<li>
							<label for="company"><spring:message code="company" /> :</label>
							${allocatedCompany.value}
						</li>
						<li>
							<label for="path"><spring:message code="system.component" /> :</label>
							<form:select path="component" items="${components}" />
						</li>
						<li>
							<label for="path"><spring:message code="note.publish" /> :</label>
							<form:checkbox path="publish" />
						</li>
						<li>
							<label for="language"><spring:message code="company.language" /> :</label>
							${primaryLocale.getDisplayLanguage(rc.locale)} (${primaryLocale.getDisplayCountry(rc.locale)})
						</li>
						<li>
							<label for="defaultLabel"><spring:message code="label" /> :</label>
							<form:input path="defaultLabel" size="25"/>
						</li>
						<li>
							<label for="defaultNote"><spring:message code="note.note" /> :</label>
							<form:textarea path="defaultNote" rows="10" cols="132"/>
						</li>
					</ol>
				</fieldset>
				<table class="default2">
					<thead>
						<tr>
							<th colspan="3"><spring:message code="editdefaultnote.defaultnote" /></th>
						</tr>
						<tr>
							<td><spring:message code="company.language" /></td>
							<td><spring:message code="label" /></td>
							<td><spring:message code="note.note" /></td>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="locale" items="${additionalLocales}">
							<tr>
								<td>${locale.getDisplayLanguage(rc.locale)} (${locale.getDisplayCountry(rc.locale)})</td>
								<td>
									<form:input path="labelTranslations[${locale}]" size="25"/>
								</td>
								<td>
									<form:textarea path="noteTranslations[${locale}]" rows="10" cols="132"/>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<input type="submit" value="<spring:message code='save' />" />
			</form:form>
		</div>
    </jsp:body>
</t:crocodileTemplate>