<%-- File name: /trescal/core/admin/viewprintfilerules.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="viewprintfilerules.headertext" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/admin/ViewPrintFileRules.js'></script>
    </jsp:attribute>
    <jsp:body>
        <div class="infobox">
			<table class="default4" id="printfilerules" summary="This table lists all printers">
				<thead>
					<tr>
						<td colspan="7">
							<div class="float-left"><spring:message code="viewprintfilerules.headertext" /> (${rules.size()})</div>
							<a href="editprintfilerule.htm" class="mainlink-float"><spring:message code="viewprintfilerules.addrule" /></a>
							<div class="clear"></div>
						</td>
					</tr>
					<tr>
						<th><spring:message code="viewprintfilerules.filenamelike" /></th>
						<th><spring:message code="viewprintfilerules.printer" /></th>
						<th><spring:message code="admin.firstpagepapertype" /></th>
						<th><spring:message code="admin.restpagepapertype" /></th>
						<th><spring:message code="admin.copies" /></th>
						<th><spring:message code="admin.edit" /></th>
						<th><spring:message code="admin.delete" /></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="7">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
					<c:forEach var="r" items="${rules}">
						<tr id="rule-${r.id}">
							<td>${r.fileNameLike}</td>
							<td>${r.printer.description}</td>
							<td>${r.firstPagePaperType.description}</td>
							<td>${r.restPagePaperType.description}</td>
							<td>${r.copies}</td>
							<td><a class="mainlink" href="editprintfilerule.htm?printfileruleid=${r.id}"><spring:message code="admin.edit" /></a></td>
							<td><a class="mainlink" onclick=" event.preventDefault(); deletePrintFileRule(${r.id}); " href="#"><spring:message code="admin.delete" /></a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
    </jsp:body>
</t:crocodileTemplate>