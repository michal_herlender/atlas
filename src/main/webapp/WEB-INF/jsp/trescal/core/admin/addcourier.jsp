<%-- File name: /trescal/core/admin/businessEmails.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message
				code='addcourier.headtext' /></span>
	</jsp:attribute>
	<jsp:body>
	<form:form modelAttribute="obj" id="addcourierform" action=""
			method="post">

		<t:showErrors path="obj.*" showFieldErrors="true" />
		<div class="infobox">
			<div class="float-right">
				<a href="viewcouriers.htm" class="mainlink"><spring:message
							code='addcourier.goback' /></a>
			</div>
			<div class="clear"></div>
			<fieldset>
								
									<legend>
						<spring:message code='addcourier.headtext' />
					</legend>
								
									<ol>
										<li>
											<form:label path="name">
								<spring:message code='addcourier.couriername' />
							</form:label>
											<form:input path="name" />
											<span class="attention"><form:errors path="name" /></span>
										</li>
										<li>
											<form:label path="webTrackURL">
								<spring:message code='addcourier.webtrackurl' />
							</form:label>
											<form:input path="webTrackURL" />
											<span class="attention"><form:errors
									path="webTrackURL" /></span>
										</li>
										<li>
											<label>&nbsp;</label>
											<input type="submit" id="submit"
							value="<spring:message code='addcourier.submit'/>" />
										</li>			
									</ol>
								
								</fieldset>							
							
							</div>
							<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
						</form:form>
	</jsp:body>
</t:crocodileTemplate>