<%-- File name: /trescal/core/admin/viewcompanygroups.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code='adminarea.managecompanygroups'/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <%-- No script file currently required --%>
    </jsp:attribute>
    <jsp:body>
		<div class="infobox">
			<table class="default4" id="companygroups" summary="<spring:message code='viewcompanygroups.tableallcompanygroups' />">
				<thead>
					<tr>
						<td colspan="4">
							<div class="float-left"><spring:message code='viewcompanygroups.companygroups'/> (${companyGroups.size()})</div>
							<a href="editcompanygroup.htm" class="mainlink-float"><spring:message code='viewcompanygroups.addcompanygroup'/></a>
							<div class="clear"></div>
						</td>
					</tr>
					<tr>
						<th><spring:message code='admin.companygroups.name'/></th>
						<th><spring:message code='active'/></th>
						<th><spring:message code='usage'/></th>
						<th><spring:message code='admin.edit'/></th>
						<%-- TODO - delete logic (allowed if unused) --%>
						<%-- <th><spring:message code='admin.delete'/></th>  --%>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="4">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
					<c:forEach var="c" items="${companyGroups}">
						<tr id="companygroup-${c.id}">
							<td><c:out value="${c.groupName}" /></td>
							<td><spring:message code="${c.active}" /></td>
							<td><c:out value="${c.companyCount}" /></td>
							<td>
								<a class="mainlink" href="editcompanygroup.htm?companygroupid=${c.id}">
									<spring:message code='admin.edit' ></spring:message>
								</a>
							</td>
						<%-- TODO - delete logic (could be allowed if unused, e.g. count = 0) --%>
						<%--
							<td>
								<a class="mainlink" onclick=" " href="#">
									<spring:message code='admin.delete' />
								</a>
							</td>
						 --%>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
    </jsp:body>
</t:crocodileTemplate>