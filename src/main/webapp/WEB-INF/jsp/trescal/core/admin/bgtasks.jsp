<%-- File name: /trescal/core/admin/businessEmails.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="bgtasks.title"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<style>
			vaadin-dialog-overlay	{
				width: 60%;
			}
			vaadin-grid-cell-content {
				cursor: pointer;
			}
		</style>
		<script type='module' src='script/components/cwms-translate/cwms-translate.js' ></script>
		<script type='module' src='script/components/cwms-bg-tasks/cwms-bg-tasks.js'></script>
	</jsp:attribute>
	<jsp:body>
		<cwms-bg-tasks person-id="${personid}" job-name="${jobname}" id="${id}"></cwms-bg-tasks>	
	</jsp:body>
</t:crocodileTemplate>