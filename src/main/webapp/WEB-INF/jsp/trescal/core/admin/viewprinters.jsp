<%-- File name: /trescal/core/admin/viewprinters.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code='viewprinters.headertext'/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/admin/ViewPrinters.js'></script>
    </jsp:attribute>
    <jsp:body>
 
        <c:set var="printers" value="${model.printers}" />
        <c:set var="lprinters" value="${model.labelPrinters}" />
						
		<div class="infobox">
		
			<table class="default4" id="printers" summary="<spring:message code='viewprinters.tableallprinters' />">
				<thead>
					<tr>
						<td colspan="6">
							<div class="float-left"><spring:message code='viewprinters.printers'/> (${printers.size()})</div>
							<a href="editprinter.htm" class="mainlink-float"><spring:message code='viewprinters.addprinter'/></a>
							<div class="clear"></div>
						</td>
					</tr>
					<tr>
				
						<th><spring:message code='viewprinters.path'/></th>
						<th><spring:message code='admin.description'/></th>
						<th><spring:message code='admin.address'/></th>
						<th><spring:message code='viewprinters.trays'/></th>
						<th><spring:message code='admin.edit'/></th>
						<th><spring:message code='admin.delete'/></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="6">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
					<c:forEach var="p" items="${printers}">
						<tr id="printer-${p.id}">
							<td>${p.path}</td>
							<td>${p.description}</td>
							<td><address:showShortAddress address="${p.addr}"/></td>
							<td>${p.trays.size()}</td>
							<td>
								<a class="mainlink" href="editprinter.htm?printerid=${p.id}">
									<spring:message code='admin.edit' ></spring:message>
								</a>
							</td>
							<td>
								<a class="mainlink" onclick=" deletePrinter(${p.id}); " href="#">
									<spring:message code='admin.delete' />
								</a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			
			<table class="default4" id="labelprinters" summary="<spring:message code='viewprinters.tablealllabelprinters' />">
				<thead>
					<tr>
						<td colspan="7">
							<div class="float-left"><spring:message code='viewprinters.labelprinters' /> (${lprinters.size()})</div>
						</td>
					</tr>
					<tr>
						<th><spring:message code='viewprinters.path'/></th>
						<th><spring:message code='admin.description'/></th>
						<th><spring:message code='asset.ipaddress'/></th>
						<th><spring:message code='viewprinters.language'/></th>
						<th><spring:message code='viewprinters.mediatype'/></th>
						<th><spring:message code='viewprinters.address'/></th>
						<th><spring:message code='admin.delete'/></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="7">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
				<c:forEach var="p" items="${lprinters}">
						<tr id="lprinter-${p.id}">
							<td>${p.path}</td>
							<td>${p.description}</td>
							<td>${p.ipv4}</td>
							<td>${p.language}</td>
							<td>${p.media.name}</td>
							<td><address:showShortAddress address="${p.addr}"/></td>
							<td><a class="mainlink" onclick=" deleteLabelPrinter(${p.id}); " href="#"><spring:message code='admin.delete' ></spring:message></a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
    </jsp:body>
</t:crocodileTemplate>