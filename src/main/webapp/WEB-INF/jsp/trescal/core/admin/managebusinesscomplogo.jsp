<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code='adminarea.managebusinesscompanylogo'/></span>
    </jsp:attribute>
 
	<jsp:attribute name="scriptPart">	
		<script src="script/trescal/core/admin/mangebusinesscomlogo.js"></script>
	</jsp:attribute>   

	<jsp:body>


<div id="imageUploader" class="UploadForm">
			<t:showErrors path="command.*" showFieldErrors="true"/>
			 <form:form action="" method="POST" enctype="multipart/form-data" modelAttribute="command">
				<fieldset>
					<form:hidden path="businessCompId"/>
					<ol>
						<li>
							<label><spring:message code="adminarea.managebusinesscompanylogo.description"/> : </label>
							<form:textarea path="description" rows="3" cols="80"/>
							</li>
						<li>
							<label><spring:message code="adminarea.managebusinesscompanylogo.logo"/> : </label>
							<input type = "file" name = "file" id="file" size = "50" accept="image/*" />
						</li>
						
						<li>
							 <input type = "submit" value = "<spring:message code="adminarea.managebusinesscompanylogo.uploadlogo"/>"/>
						</li>
					</ol>
				</fieldset>
			</form:form>
		</div>	
	
		<c:if test="${systemImages.size() > 0}">
			<div class="infobox box">
				<div class="gallerifficContent">
					<div id="thumbs-adv" class="navigation">
						<ul class="thumbs noscript">
							<c:forEach var="image" items="${systemImages}">
								<li class="thumbitem" id="systemimg${image.id}">
									<a class="thumb" href="displaybusinessCompanyLogo?id=${image.id}" title="">
										<img src="displaybusinessCompanyLogo?id=${image.id}" alt="" height="60" width="80" />
									</a>
									<div class="caption">
										<div class="download">
											<a href="" onclick="deleteSystemImage(${image.id}); return false; ">
												<img src="img/icons/redcross.png" width="16" height="16" alt="<spring:message code='viewmod.deletethisimage'/>" title="<spring:message code='viewmod.deletethisimage'/>" />
											</a>
										</div>
									</div>
								</li>
							</c:forEach>
						</ul>
					</div>
					<div id="gallery-adv" class="content">
							<div id="controls-adv" class="controls"></div>
							<div id="loading-adv" class="loader"></div>
							<div id="slideshow-adv" class="slideshow"></div>
							<div id="caption-adv" class="embox"></div>
					</div>
					<!-- clear floats and restore page flow -->
					<div class="clear"></div>
				</div>
			</div>	
			<div class="clear"></div>
		</c:if>
		
		

	</jsp:body>
    
</t:crocodileTemplate>