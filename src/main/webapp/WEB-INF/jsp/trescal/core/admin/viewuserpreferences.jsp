<%-- File name: /trescal/core/admin/viewuserpreferences.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="viewuserpreferences.headertext" /></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/admin/ViewUserPreferences.js'></script>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox">
		
			<table class="default4" id="userpreferences" >
				<thead>
					<tr>
						<th><spring:message code='viewuserpreferences.userid' /></th>
						<th><spring:message code='viewuserpreferences.language' /></th>
						<th><spring:message code='viewuserpreferences.labelprinter' /></th>
						<th><spring:message code='edituserpreferences.alligatorprofile' /></th>
						<th><spring:message code='edituserpreferences.production' /></th>
						<th><spring:message code='edituserpreferences.test' /></th>
						<th><spring:message code='viewuserpreferences.jikeynav' /></th>
						<th><spring:message code='viewuserpreferences.moveractive' /></th>
						<th><spring:message code='viewuserpreferences.moverdelay' /></th>
						<th><spring:message code='viewuserpreferences.navbarpos' /></th>
						<th><spring:message code='viewuserpreferences.servicetypeusage' /></th>
						<th><spring:message code='edituserpreferences.defaultjobtype' /></th>
						<th><spring:message code='viewjob.jobitemlabels' /></th>
						<th><spring:message code='admin.edit' /></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="14">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
					<c:forEach var="up" items="${userPreferences}">
						<tr id="preferences-${up.id}">
							<td>
								<c:out value="${up.contact.name}" />
							</td>
							<td>
								<c:choose>
									<c:when test="${not empty up.contact.locale}">
										<c:out value="${up.contact.locale}" />
									</c:when>
									<c:otherwise>
										<spring:message code="viewuserpreferences.nonespecified" />
									</c:otherwise>
								</c:choose>
							</td>
							<td>
								<c:choose>
									<c:when test="${not empty up.labelPrinter}">
										<c:out value="${up.labelPrinter.description}" />
									</c:when>
									<c:otherwise>
										<spring:message code="viewuserpreferences.nonespecified" />
									</c:otherwise>
								</c:choose>
							</td>
							<td>
								<c:choose>
									<c:when test="${alligatorContactSettings.get(up.contact.personid)}">
										<spring:message code="contact" />
									</c:when>
									<c:otherwise>
										<spring:message code="default" />
									</c:otherwise>
								</c:choose>
							</td>
							<td>
								<c:out value="${alligatorProductionSettings.get(up.contact.personid)}" />
							</td>
							<td>
								<c:out value="${alligatorTestSettings.get(up.contact.personid)}" />
							</td>
							<td>
								<c:choose>
									<c:when test="${up.jiKeyNavigation}">
										<spring:message code="viewuserpreferences.on" />
									</c:when>
									<c:otherwise>
										<spring:message code="viewuserpreferences.off" />
									</c:otherwise>
								</c:choose>
							</td>
							<td>
								<c:choose>
									<c:when test="${up.mouseoverActive}">
										<spring:message code="viewuserpreferences.on" />
									</c:when>
									<c:otherwise>
										<spring:message code="viewuserpreferences.off" />
									</c:otherwise>
								</c:choose>
							</td>
							<td>
								<c:out value="${up.mouseoverDelay} ms" />
							</td>
							<td>
								<c:choose>
									<c:when test="${up.navBarPosition}">
										<spring:message code="viewuserpreferences.top" />
									</c:when>
									<c:otherwise>
										<spring:message code="viewuserpreferences.side" />
									</c:otherwise>
								</c:choose>
							</td>
							<td>
								<c:choose>
									<c:when test="${up.defaultServiceTypeUsage}">
										<spring:message code="viewuserpreferences.on" />
									</c:when>
									<c:otherwise>
										<spring:message code="viewuserpreferences.off" />
									</c:otherwise>
								</c:choose>
							</td>
							<td>
								<c:out value="${up.defaultJobType}" />
							</td>
							<td>
								<c:choose>
									<c:when test="${up.autoPrintLabel}">
										<spring:message code="viewuserpreferences.automatic" />
									</c:when>
									<c:otherwise>
										<spring:message code="viewuserpreferences.nonautomatic" />
									</c:otherwise>
								</c:choose>
							</td>
							<td>
								<a class="mainlink" href="edituserpreferences.htm?preferenceid=${up.id}">
									<spring:message code="admin.edit" />
								</a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>		
	</jsp:body>
</t:crocodileTemplate>	