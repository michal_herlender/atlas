<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>

<script src='script/trescal/core/admin/SendNotifications.js'></script>


<t:crocodileTemplate>

	<jsp:attribute name="header">
		<span class="headtext"><spring:message
				code="sendnotification.pagetitle" /></span>
	</jsp:attribute>

	<jsp:body>
		<t:showErrors path="form.*" showFieldErrors="true" />
		<div id="sendnotifications" class="infobox">	
			<fieldset>
				<legend>
					<spring:message code="sendnotification.legendfieldset" />
				</legend>
					<ol>
					<form:form modelAttribute="form">

						<li>
							<span for="sendnotificationstatus"><spring:message
									code="sendnotification.form.status" /></span>
							<form:select path="status" onchange="hideForSuccesStatus();"> 
								<option value=""
									<c:if test="${empty form.status}">selected</c:if>></option>
								<option value="ERROR"
									<c:if test="${form.status eq 'ERROR'}">selected</c:if>>Error</option>						
								<option value="SUCCESS"
									<c:if test="${form.status eq 'SUCCESS'}">selected</c:if>>Success</option>	
								<option value="PENDING"
									<c:if test="${form.status eq 'PENDING'}">selected</c:if>>Pending</option>
							</form:select>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<span for="sendnotificationentityclass"><spring:message
									code="sendnotification.form.entityclass" /></span>
							<form:select path="entityClass">
								<form:option value="">&nbsp;</form:option>
     							<form:options items="${entitiesClass}" />
							</form:select>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<span for="sendnotificationopertype"><spring:message
									code="sendnotification.form.opertype" /></span>
							<form:select path="operationType">
								<form:option value="">&nbsp;</form:option>
     							<form:options items="${operationsType}" />
							</form:select>
								
						</li>
						
						<li>
							<input type="submit"
							value="<spring:message code="sendnotification.form.submit" />" />
						</li>
						
						<li>
							<input type="hidden" id="pageNo" name="pageNo" />
							<t:paginationControl resultset="${rs}" />
							
							<table id="table_sendnotifications" class="default2"
								summary="This table lists all notifications">	
								<thead>
									<tr>
										<th class="codefault">
											<spring:message code="sendnotification.table.sent" />
										</th>
										<th class="codefault"><spring:message
												code="sendnotification.table.entityclass" /></th>
										<th class="codefault"><spring:message
												code="workflow.jobitemno" /> | Instrument</th>
										<th class="codefault"><spring:message
												code="sendnotification.table.operationtype" /></th>
										<th class="codefault"><spring:message
												code="sendnotification.table.createdon" /></th>
										<th class="codefault"><spring:message
												code="sendevent.table.senton" /></th>
										<th class="codefault"><spring:message
												code="sendnotification.table.errormessage" /></th>
										<th class="codefault"><spring:message
												code="sendnotification.table.action" />
												<input onclick="CheckAll();" type="checkbox"
											id="MainResendNotification" class="MainResendNotification" />
										</th>
									</tr>
								</thead>
								<tbody>
							    	<c:forEach items="${ notifications }" var="notif"
										varStatus="status">
										<tr>
											<td><spring:message code="${ notif.sent }" /></td>
											<td>${ notif.entityclass }</td>
											<td>
												<c:if test="${notif.jobItemId ne null}">
													<a
														href="jiactions.htm?jobitemid=${notif.jobItemId}&amp;ajax=jobitem&amp;width=580"
														class="jconTip mainlink"
														title="Information sur le job item"
														id="jobitem${notif.jobItemId}" tabindex="-1">
													${notif.jobNo}.${notif.jobItemNo}</a>
												
												</c:if>
												<c:if test="${notif.jobItemId eq null}">
													--  
												</c:if>
												| 
												<c:if test="${notif.plantid ne null}">
													<cwms:securedLink permission="INSTRUMENT_VIEW"
														classAttr="mainlink" parameter="?plantid=${notif.plantid}">
														${notif.plantid}
													</cwms:securedLink>
												</c:if>
												<c:if test="${notif.plantid eq null}">
												  --
												</c:if>
											</td>
											<td>${ notif.operationtype }</td>
											<td><fmt:formatDate  value="${ notif.createdOn }" type="both" dateStyle="SHORT" /></td>
											<td><fmt:formatDate  value="${ notif.sentOn }" type="both" dateStyle="SHORT" /></td>
											<td>${ notif.lastErrorMessage }</td>
											<td>
												<center>
													<input type="checkbox" id="ResendNotification"
														Class="ResendNotification" value="${ notif.id }" />
												</center>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</li>
						</br>
							<center>
								<input type="button" name="save" class="button"
								onclick="resendNotification(); return false; "
								value="<spring:message code='submit' />" />
						   </center>
						</form:form>
					</ol>							
				</fieldset>
			</div>
		</jsp:body>
</t:crocodileTemplate>