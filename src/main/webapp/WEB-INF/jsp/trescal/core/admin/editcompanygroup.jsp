<%-- File name: /trescal/core/admin/editcompanygroup.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="editcompanygroup.headertext" /></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/admin/EditCompanyGroups.js'></script>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox">
			<form:form method="post" modelAttribute="form">
				<p>
					<a href="<c:url value="/viewcompanygroups.htm" />">
						<spring:message code="editcompanygroup.backtoviewcompanygroups" />
					</a>				
				</p>
			
				<t:showErrors path="form.*" showFieldErrors="true" />
				<fieldset>
					<legend><spring:message code="editcompanygroup.headertext" /></legend>								
					<ol>
						<li>
							<label for="active"><spring:message code="active" />:</label>
							<form:checkbox path="active" />
							<form:errors path="active" delimiter="; " cssClass="float-left; error"/>
						</li>
						<li>
							<label for="groupName"><spring:message code="admin.companygroups.name" />:</label>
							<form:input size="75" path="groupName" />
							<form:errors path="groupName" delimiter="; " cssClass="float-left; error"/>
						</li>
					</ol>
				</fieldset>									
				
				<div class="marg-top">
					<input type="submit" value="<spring:message code="save" />" />
				</div>								
			</form:form>
			
		</div>
	</jsp:body>
</t:crocodileTemplate>