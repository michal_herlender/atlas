<%-- File name: /trescal/core/admin/enablereversetraceabilityreport.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<form:form modelAttribute="form" action="enablereversetraceabilitysettings.htm" method="post" id="enablereversetraceabilitysettings">
	<fieldset> 
		<ol>
			<li>
				<label><spring:message code="businesscompany"/>:</label>  
				<span>${allocatedCompany.value}</span>
			</li>
			
			<li>
				<label><spring:message code="subdivision"/>:</label> 
				<form:select path="reverseTraceabilitySettingsDto.subdivid" style="width: 167px;">
					<c:forEach var="sub" items="${activeSubdivsNotAddedYet}">
							<option value="${sub.key}">${sub.value}</option>
					</c:forEach>
				</form:select>
			</li>
			
			<li>
				<label for="active"><spring:message code="mailgrouptype.reversetraceability"/>:</label>
				<form:select path="reverseTraceabilitySettingsDto.reverseTraceability" style="width: 167px;">
					<option value="true" selected><spring:message code="yes" /></option>
					<option value="false"><spring:message code="no" /></option>
				</form:select>
			</li>
			
			<li>
				<label><spring:message code="bpo.startdate"/>:</label>
				<form:input type="date" id="startDate" path="reverseTraceabilitySettingsDto.startDate" style="width: 165px;"/>
			</li>
		</ol>

	</fieldset>
</form:form>