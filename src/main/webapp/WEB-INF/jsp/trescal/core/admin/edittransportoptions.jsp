<%-- File name: /trescal/core/admin/edittransportoptions.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="adminarea.edittransportoptions" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
    	<script src='script/trescal/core/admin/EditTransportOptions.js'></script>		
	</jsp:attribute>
    <jsp:body>
    	<form:errors path="form.*">
       		<div class="warningBox1">
            	<div class="warningBox2">
                	<div class="warningBox3">
                    	<h5 class="center-0 attention"><spring:message code="error.save"/></h5>
                    </div>
                </div>
           </div>
        </form:errors>
        	<div class="infobox">
				<fieldset id="transportoptions">
					<legend><spring:message code="adminarea.edittransportoptions" /></legend>
					<ol>									
						<li>
							<label><spring:message code="businesscompany"/>:</label>
							<span>${subdiv.comp.coname}</span>
						</li>
						<li>
							<label><spring:message code="businesssubdivision"/>:</label>
							<span>${subdiv.subname}</span>
						</li>
					</ol>
				</fieldset>
				<form:form modelAttribute="form" action="" method="post">
					<table class="default4 editTransportOptionForm">
						<thead>
							<tr>
								<th class="transportmethod" scope="col"><spring:message code="jiactions.transmethod"/></th>
								<th class="active" scope="col" style="text-align: center;"><spring:message code="active"/></th>
								<th class="name" scope="col" style="text-align: center;"><spring:message code="transportoptions.localizedname"/></th>
								<th class="active" scope="col" style="text-align: center;"><spring:message code="transportoptions.externalid"/></th>
								<th class="dayofweek" scope="col" style="text-align: center;"><spring:message code="transportoptions.monday"/></th>
								<th class="dayofweek" scope="col" style="text-align: center;"><spring:message code="transportoptions.tuesday"/></th>
								<th class="dayofweek" scope="col" style="text-align: center;"><spring:message code="transportoptions.wednesday"/></th>
								<th class="dayofweek" scope="col" style="text-align: center;"><spring:message code="transportoptions.thursday"/></th>
								<th class="dayofweek" scope="col" style="text-align: center;"><spring:message code="transportoptions.friday"/></th>
								<th class="dayofweek" scope="col" style="text-align: center;"><spring:message code="transportoptions.saturday"/></th>
								<th class="dayofweek" scope="col" style="text-align: center;"><spring:message code="transportoptions.sunday"/></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="option" items="${transportOptionsWithActiveMethod}" varStatus="loopStatus">	
								<c:set var="monday" value=""/>
								<c:set var="mondayid" value=""/>
								<c:set var="timemonday" value=""/>
								
								<c:set var="tuesday" value=""/>
								<c:set var="tuesdayid" value=""/>
								<c:set var="timetuesday" value=""/>
								
								<c:set var="wednesday" value=""/>
								<c:set var="wednesdayid" value=""/>
								<c:set var="timewednesday" value=""/>
								
								<c:set var="thursday" value=""/>
								<c:set var="thursdayid" value=""/>
								<c:set var="timethursday" value=""/>
								
								<c:set var="friday" value=""/>
								<c:set var="fridayid" value=""/>
								<c:set var="timefriday" value=""/>
								
								<c:set var="saturday" value=""/>
								<c:set var="saturdayid" value=""/>
								<c:set var="timesaturday" value=""/>
								
								<c:set var="sunday" value=""/>
								<c:set var="sundayid" value=""/>
								<c:set var="timesunday" value=""/>
								<c:set var="i" value="${loopStatus.index}" />
								<c:forEach var="scRule" items="${option.scheduleRules}">
									<c:choose>
										<c:when test ="${scRule.dayOfWeek == 'MONDAY'}">
											<c:set var="monday" value="true"/>
											<c:set var="mondayid" value="${scRule.id}"/>
											<c:set var="timemonday" value="${scRule.cutoffTime}"/>
										</c:when>
										<c:when test ="${scRule.dayOfWeek == 'TUESDAY'}">
											<c:set var="tuesday" value="true"/>
											<c:set var="tuesdayid" value="${scRule.id}"/>
											<c:set var="timetuesday" value="${scRule.cutoffTime}"/>
										</c:when>
										<c:when test ="${scRule.dayOfWeek == 'WEDNESDAY'}">
											<c:set var="wednesday" value="true"/>
											<c:set var="wednesdayid" value="${scRule.id}"/>
											<c:set var="timewednesday" value="${scRule.cutoffTime}"/>
										</c:when>
										<c:when test ="${scRule.dayOfWeek == 'THURSDAY'}">
											<c:set var="thursday" value="true"/>
											<c:set var="thursdayid" value="${scRule.id}"/>
											<c:set var="timethursday" value="${scRule.cutoffTime}"/>
										</c:when>
										<c:when test ="${scRule.dayOfWeek == 'FRIDAY'}">
											<c:set var="friday" value="true"/>
											<c:set var="fridayid" value="${scRule.id}"/>
											<c:set var="timefriday" value="${scRule.cutoffTime}"/>
										</c:when>
										<c:when test ="${scRule.dayOfWeek == 'SATURDAY'}">
											<c:set var="saturday" value="true"/>
											<c:set var="saturdayid" value="${scRule.id}"/>
											<c:set var="timesaturday" value="${scRule.cutoffTime}"/>
										</c:when>
										<c:when test ="${scRule.dayOfWeek == 'SUNDAY'}">
											<c:set var="sunday" value="true"/>
											<c:set var="sundayid" value="${scRule.id}"/>
											<c:set var="timesunday" value="${scRule.cutoffTime}"/>
										</c:when>
									</c:choose>
								</c:forEach>
								<tr style="width: 300px;">
									
									<td >
										<span> <cwms:besttranslation translations="${option.method.methodTranslation}" /> </span>
										<form:hidden path="options[${i}].transportOptionId" value="${option.id}"/>
									</td>
									
									<td class="center">
										<form:select path="options[${i}].active">
											<option value="true" <c:if test ="${option.active}"> selected</c:if>><spring:message code="yes" /></option>
											<option value="false" <c:if test ="${ not option.active}"> selected</c:if>><spring:message code="no" /></option>
										</form:select>
									</td>
									
									<td class="center">
										<form:input path="options[${i}].localizedName" id="options${i}.localizedName" value="${option.localizedName}" size="30"/>
										<br><form:errors class="attention" path="options[${i}].localizedName"/>
									</td>
									
									<td class="center">
										<form:input path="options[${i}].externalRef" id="options${i}.externalRef" value="${option.externalRef}" size="10"/>
										<br><form:errors class="attention" path="options[${i}].externalRef"/>
									</td>
									
								<c:choose>
										<c:when test="${option.method.optionPerDayOfWeek}">
											<td>
												<form:checkbox path="options[${i}].scheduleRule[0].dayOfWeek" value="MONDAY" checked="${monday}"/>
												<form:input path="options[${i}].scheduleRule[0].cutoffTime" type="time" value="${timemonday}"/>
												<form:hidden path="options[${i}].scheduleRule[0].id" value="${mondayid}"/>
												<br><form:errors class="attention" path="options[${i}].scheduleRule[0].cutoffTime"/>
											</td>
											<td class="hid"></td>
									
											<td>
												<form:checkbox path="options[${i}].scheduleRule[1].dayOfWeek" value="TUESDAY" checked="${tuesday}"/>
												<form:input path="options[${i}].scheduleRule[1].cutoffTime" type="time" value="${timetuesday}"/>
												<form:hidden path="options[${i}].scheduleRule[1].id" value="${tuesdayid}"/>
												<br><form:errors class="attention" path="options[${i}].scheduleRule[1].cutoffTime"/>
											</td>
									
											<td>
												<form:checkbox path="options[${i}].scheduleRule[2].dayOfWeek" value="WEDNESDAY" checked="${wednesday}"/>
												<form:input path="options[${i}].scheduleRule[2].cutoffTime" type="time" value="${timewednesday}"/>
												<form:hidden path="options[${i}].scheduleRule[2].id" value="${wednesdayid}"/>
												<br><form:errors class="attention" path="options[${i}].scheduleRule[2].cutoffTime"/>
											</td>
									
											<td>
												<form:checkbox path="options[${i}].scheduleRule[3].dayOfWeek" value="THURSDAY" checked="${thursday}"/>
												<form:input path="options[${i}].scheduleRule[3].cutoffTime" type="time" value="${timethursday}"/>
												<form:hidden path="options[${i}].scheduleRule[3].id" value="${thursdayid}"/>
												<br><form:errors class="attention" path="options[${i}].scheduleRule[3].cutoffTime"/>
											</td>
									
											<td>
												<form:checkbox path="options[${i}].scheduleRule[4].dayOfWeek" value="FRIDAY" checked="${friday}"/>
												<form:input path="options[${i}].scheduleRule[4].cutoffTime" type="time" value="${timefriday}"/>
												<form:hidden path="options[${i}].scheduleRule[4].id" value="${fridayid}"/>
												<br><form:errors class="attention" path="options[${i}].scheduleRule[4].cutoffTime" />
											</td>
									
											<td>
												<form:checkbox path="options[${i}].scheduleRule[5].dayOfWeek" value="SATURDAY" checked="${saturday}"/>
												<form:input path="options[${i}].scheduleRule[5].cutoffTime" type="time" value="${timesaturday}"/>
												<form:hidden path="options[${i}].scheduleRule[5].id" value="${saturdayid}"/>
												<br><form:errors class="attention" path="options[${i}].scheduleRule[5].cutoffTime"/>
											</td>
									
											<td>
												<form:checkbox path="options[${i}].scheduleRule[6].dayOfWeek" value="SUNDAY" checked="${sunday}"/>
												<form:input path="options[${i}].scheduleRule[6].cutoffTime" type="time" value="${timesunday}"/>
												<form:hidden path="options[${i}].scheduleRule[6].id" value="${sundayid}"/>
												<br><form:errors class="attention" path="options[${i}].scheduleRule[6].cutoffTime"/>
											</td>
										</c:when>
										<c:otherwise>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</c:otherwise>
									</c:choose>
								</tr>
							</c:forEach>
							<tr>
								<td colspan="11">
									<div class="float-left"><a href="" onClick="event.preventDefault(); createTransportOption(); "><img src="img/icons/add.png" height="20" width="20" alt="<spring:message code="transportoptions.add"/>"/> </a></div>
									<div class="padtop"><span>&nbsp;<spring:message code="transportoptions.add"/></span></div>
								</td>
							</tr>
						</tbody>
					</table><br>
					<form:button name="submit" value="submit"><spring:message code="save"/></form:button>
				</form:form>
    		</div>
    </jsp:body>
</t:crocodileTemplate>  