<%-- File name: /trescal/core/admin/viewcaltypes.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="viewcaltypes.headertext" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/admin/ViewCalTypes.js'></script>
    </jsp:attribute>
    <jsp:body>
		<div class="infobox">
		
			<table id="caltypes" class="default4" summary="<spring:message code='viewcaltypes.thistable' />">
				<thead>
					
					<tr class="nodrag">
						<th id="qresshortname" scope="col"><spring:message code='admin.shortname' /></th>  
						<th id="qreslongname" scope="col"><spring:message code='admin.longname' /></th>
						<th id="qrescolour" scope="col"><spring:message code='admin.displaycolor' /></th> 
					</tr>
				</thead>
				<tfoot>
					<tr class="nodrag">
						<td colspan="3">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody id="typelist">
					<c:forEach var="t" items="${calTypes}">
						<tr class="nodrag">
							<td><cwms:besttranslation translations="${t.serviceType.shortnameTranslation}"/></td>
							<td><cwms:besttranslation translations="${t.serviceType.longnameTranslation}"/></td>
							<td>${t.serviceType.displayColour}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>						
		
		</div>
    </jsp:body>
</t:crocodileTemplate>