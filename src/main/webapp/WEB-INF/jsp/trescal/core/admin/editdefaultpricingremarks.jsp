<%-- File name: /trescal/core/admin/editdefaultnote.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="adminarea.edittransportoptions" /></span>
    </jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/admin/EditDefaultPricingRemarks.js'></script>
	</jsp:attribute>
    <jsp:body>
		<t:showErrors path="form.*" />
		<!-- sub navigation menu which displays and hides sections of page as well as linking to external pages -->
		<div id="subnav">
			<dl>
				<dt>
					<a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'calibration-tab', false); $j('input#openTab').val('calibration-tab'); " 
						id="calibration-link" class="selected" >
							<spring:message code="costtype.calibration"/>
					</a>
				</dt>
				<dt>
					<a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'repair-tab', false); $j('input#openTab').val('repair-tab'); " 
						id="repair-link" >
							<spring:message code="costtype.repair"/>
					</a>
				</dt>
				<dt>
					<a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'adjustment-tab', false); $j('input#openTab').val('adjustment-tab'); " 
						id="adjustment-link" >
							<spring:message code="costtype.adjustment"/>
					</a>
				</dt>
				<dt>
					<a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'purchase-tab', false); $j('input#openTab').val('purchase-tab'); " 
						id="purchase-link" >
							<spring:message code="costtype.purchase"/>
					</a>
				</dt>
			</dl>
		</div>
		<!-- end of sub navigation menu -->
		<div class="infobox">
			<form:form method="post" modelAttribute="form">
																		
				<div class="infobox" id="costs">
					<!-- div containing calibration related elements -->
					<div id="calibration-tab">
					</div>
					
					<!-- div containing repair related elements -->
					<div id="repair-tab">
					</div>
					
					<!-- div containing adjustment related elements -->
					<div id="adjustment-tab">
					</div>
					
					<!-- div containing purchase related elements -->
					<div id="purchase-tab">
					</div>
				</div>
				
			</form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>
