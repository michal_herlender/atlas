<%-- File name: /trescal/core/admin/viewlabelimages.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code='adminarea.managelabelimages'/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/documents/ImageGalleryFunctions.js'></script>
    </jsp:attribute>
    <jsp:body>
    	<t:showImages imagetype="${imageType}" entity="${entity}" imagetypeid="${entityId}" />
	</jsp:body>
</t:crocodileTemplate>