<%-- File name: /trescal/core/admin/dymosandbox.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<script type='text/javascript' src='script/trescal/core/admin/DymoSandbox.js'></script>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="adminarea.dymodemo" /></span>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox">
			<fieldset>
				<legend><spring:message code="adminarea.dymodemo" /></legend>
				<ol>
					<li>
						<label>Web Service Present:</label>
						<span id="webservicepresent"></span>
					</li>
					<li>
						<label>Printer Count:</label>
						<span id="printercount"></span>
					</li>
					<li>
						<label>Printer List:</label>
						<span id="printerlist"></span>
					</li>
					<li>
						<label>Hire Label:</label>
						<span id="testhirelabel">
							<a href="#" class="mainlink" onclick="event.preventDefault(); testHireLabel(); ">Print Test Label</a>
						</span>
					</li>
				</ol>
			</fieldset>
		</div>
	</jsp:body>
</t:crocodileTemplate>
