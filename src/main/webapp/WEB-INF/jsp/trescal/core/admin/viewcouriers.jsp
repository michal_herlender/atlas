<%-- File name: /trescal/core/admin/viewcouriers.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
    <span class="headtext">${couriers.size()} <spring:message
				code="viewcouriers.couriers" /></span>
    </jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript'
			src='script/trescal/core/admin/ViewCouriers.js'></script>
	</jsp:attribute>
	<jsp:body>
        <div class="infobox">
						
			<div class="float-right">
				<a href="addcourier.htm" class="mainlink">
					<spring:message code='viewcouriers.addcourier' />
				</a>
			</div>
			<div class="clear"></div>
										
			<c:forEach var="c" items="${couriers}">
				
				<fieldset>
				
					<legend>
						<spring:message code='admin.edit' /> ${c.name} <spring:message
							code='viewcouriers.courier' />
					</legend>
					
					<ol>										
						<li>
							<label><spring:message code='admin.name' /></label>
							<div class="float-left width80">
								<div class="float-left padding2 width30">
									${c.name}
								</div>
								<div class="float-left padding2">
									<a href="#" class="mainlink"
										onclick=" editCourierName(this, ${c.courierid}); return false; ">
										<img src="img/icons/note_edit.png" height="16" width="16"
										class="img_marg_bot"
										alt="<spring:message code='viewcouriers.editcourier' />"
										title="<spring:message code='viewcouriers.editcourier' />" />
									</a>
								</div>
								
								<c:if test="${c.defaultCourier}">
									<span style="font-weight: bold; margin-left: 150px;">
										<span class="attention">[</span>
										<spring:message code='viewcouriers.default' />
										<span class="attention">]</span>
									</span>
								</c:if>
								
								
								<div class="clear"></div>
							</div>
							<div class="clear"></div>					
							</li>
							<li>
							<label><spring:message code='company' /></label>
							<div class="float-left width80">
								<links:companyLinkDWRInfo company="${c.organisation}" rowcount="${0}" copy="${true}" />
								<div class="clear"></div>
							</div>
							<div class="clear"></div>					
							</li>
							<li>
								<label><spring:message code='viewcouriers.despatchtypes' /></label>
								<div class="float-left width80"> 	
																			
								<c:forEach var="cdt" items="${c.cdts}">
									<div class="float-left padding2 width30">
										${cdt.description}
									</div>
									<div class="float-left padding2">
										<a href="#"
											onclick=" editDespatchType(this, ${cdt.cdtid}); return false; ">
											<img src="img/icons/note_edit.png" height="16" width="16"
											class="img_marg_bot"
											alt="<spring:message code='viewcouriers.editdespatchtype' />"
											title="<spring:message code='viewcouriers.editdespatchtype' />" />
										</a>
										&nbsp;
										<a href="#"
											onclick=" removeDespatchType(this, ${cdt.cdtid}); return false; ">
											<img src="img/icons/delete.png" height="16" width="16"
											class="img_marg_bot"
											alt="<spring:message code='viewcouriers.deletedespatchtype' />"
											title="<spring:message code='viewcouriers.deletedespatchtype' />" />
										</a>
									</div>
									<c:if test="${cdt.defaultForCourier}">
										<span style="font-weight: bold; margin-left: 125px;">
											<span class="attention">[</span>
											<spring:message code='viewcouriers.defaultdespatch' /> ${c.name}
											<span class="attention">]</span>
										</span>
									</c:if>
									<div class="clear"></div>
								</c:forEach>
								
								<div class="float-left padding2">
									<a href="#" class="mainlink"
										onclick=" addDespatchType(this, ${c.courierid}); return false; "><spring:message
											code='viewcouriers.adddespatchtype' /></a>
								</div>
								</div>
								<div class="clear"></div>
							</li>
							<li>
								<label>
									
									<c:if test="${not empty c.webTrackURL}">
										<img src="img/icons/bullet_clipboard.png" width="10"
										height="10"
										onclick=" $j(this).parent().next().clipBoard(this, null, false, null); "
										alt="<spring:message code='viewcouriers.addtrackingurltoclipboard' />"
										title="<spring:message code='viewcouriers.addtrackingurltoclipboard' />" />
										</c:if>	
									</label>
								<c:choose>	
									<c:when test="${not empty c.webTrackURL}">
										<div class="float-left padtop width80 nowrap"
									title="${c.webTrackURL}">
											${c.webTrackURL}
										</div>	 											
										<div class="clear"></div>
									</c:when>	 	
																		
								<c:otherwise>
									<span>&nbsp;</span>
						        </c:otherwise>
						        
						 		</c:choose>	 
								</li>								
						        </ol>
						
					</fieldset>
			       	</c:forEach>
		        	</div>
			
                   </jsp:body>
</t:crocodileTemplate>