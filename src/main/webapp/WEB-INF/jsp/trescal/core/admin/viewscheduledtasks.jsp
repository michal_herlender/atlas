<%-- File name: /trescal/core/admin/viewscheduledtasks.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
        <span class="headtext"><spring:message
				code="viewscheduledtasks.header" /></span>
    </jsp:attribute>
	<jsp:attribute name="scriptPart">
        <script type='text/javascript'
			src='script/trescal/core/admin/ViewScheduledTasks.js'></script>
    </jsp:attribute>
	<jsp:body>
        <div class="infobox">
						
			<table class="default4 viewScheduledTasks" summary="">
				<thead>
					<tr>
						<td colspan="5">
							<spring:message code="viewscheduledtasks.header" /> (${scheduledTasks.size()})
						</td>
					</tr>
					<tr>
						<th class="name"><spring:message code="admin.name" /></th>
						<th class="desc"><spring:message code="admin.description" /></th>
						<th class="active"><spring:message code="viewscheduledtasks.active" /></th>
						<th class="run"><spring:message code="viewscheduledtasks.run" /></th>
						<th class="success"><spring:message code="viewscheduledtasks.successful" /></th>
					</tr>
				</thead>
			</table>							
				
			<c:forEach var="task" items="${scheduledTasks}">
				<table class="default4 viewScheduledTasks" summary="">
				<tbody>
					<tr>
				       <td class="name">${task.taskName}</td>
						<td class="desc">${task.description}</td>
						<td class="active">
							<c:choose>	
								<c:when test="${not task.active}">
									<a href="#"
										onclick=" event.preventDefault(); switchTaskOnOrOff(this, ${task.id}, true); "
										title="<spring:message code='viewscheduledtasks.clicktoactivate' />">
										<img src="img/icons/redcross.png" width="16" height="16">
									</a>
								</c:when>
						    	<c:otherwise>
								   <a href="#"
										onclick=" event.preventDefault(); switchTaskOnOrOff(this, ${task.id}, false); "
										title="<spring:message code='viewscheduledtasks.clicktodeactivate' />">
										<img src="img/icons/greentick.png" width="16" height="16" /> </a>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="run">
							<input type="button" value="<spring:message code='viewscheduledtasks.fire' />"
							onclick=" event.preventDefault(); runTask('${task.quartzJobName}'); " />
            			</td>
						<td class="success">
						<c:choose>	
						   <c:when test="${task.lastRunSuccess}">
								<img src="img/icons/greentick.png" class="tick" width="16"
									height="16"
									alt="<spring:message code='viewscheduledtasks.lastrunsuccess'/>"
									title="<spring:message code='viewscheduledtasks.lastrunsuccess' />" />
						   </c:when>
						   <c:otherwise>
								<img src="img/icons/redcross.png" class="cross" width="16"
									height="16"
									alt="<spring:message code='viewscheduledtasks.lastrunfail'/>"
									title="<spring:message code='viewscheduledtasks.lastrunfail'/>" />
						   </c:otherwise>
					    </c:choose>
						</td>
				   	</tr>
			    	<tr>
				    	<td class="bold"><spring:message code="viewscheduledtasks.class" /></td>
			    		<td colspan="4">${task.className} (task.methodName)</td>
			    	</tr>
			    	<tr>
				    	<td class="bold"><spring:message code="viewscheduledtasks.quartz" /></td>
					    <td colspan="4">${task.quartzJobName}</td>
				    </tr>
				    <tr>
					    <td class="bold"><spring:message code="viewscheduledtasks.lastrun" /></td>
					    <td colspan="4">
					    	<fmt:formatDate value="${task.lastRunTime}" type="both" dateStyle="SHORT" timeStyle="SHORT" /> (${task.lastRunMessage}) 
					    </td>
				    </tr>          
				</tbody>
				</table>
			</c:forEach>								
		</div>
		<c:choose>
			<c:when test="${globalActive}">
				<div class="infobox">
					<span class="allgood bold">Scheduled tasks globally active on this VM, you should be able to fire tasks.</span>
				</div>
			</c:when>
			<c:otherwise>
				<div class="warningBox1">
					<div class="warningBox2">
						<div class="warningBox3">
							<span class="attention">Scheduled tasks NOT globally active on this VM, you will NOT be able to fire tasks.</span>
						</div>
					</div>
				</div>
			</c:otherwise>
		</c:choose>
    </jsp:body>
</t:crocodileTemplate>