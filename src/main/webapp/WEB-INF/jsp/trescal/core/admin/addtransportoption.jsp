<%-- File name: /trescal/core/admin/addtransportoption.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>

<form:form modelAttribute="form" action="addtransportoption.htm" method="post" id="addtransportoption">
	<fieldset> 
		<ol>
			<li>
				<label><spring:message code="businesssubdivision"/>:</label>  
				<span>${allocatedSubdiv.value}</span>
			</li>
			
			<li>
				<label><spring:message code="jiactions.transmethod"/>*:</label> 
				<form:select id="methodId" path="methodId" style="width: 167px;" onchange="isOptionPerDayOfWeek(this.value, ${allocatedSubdiv.key}); return false;">
					<option value="0"></option>
					<c:forEach var="method" items="${activeMethods}">
							<option value="${method.id}">
								<cwms:besttranslation translations="${method.methodTranslation}" />
							</option>
					</c:forEach>
				</form:select>
			</li>
			
			<li>
				<label for="active"><spring:message code="active"/>:</label>
				<form:select path="active" style="width: 167px;">
					<option value="true" selected><spring:message code="yes" /></option>
					<option value="false"><spring:message code="no" /></option>
				</form:select>
			</li>
			<li>
				<label><spring:message code="transportoptions.localizedname"/>:</label>
				<form:input id="localizedName" path="localizedName" size="30"/>
			</li>
			<li>
				<label><spring:message code="transportoptions.externalid"/>:</label>
				<form:input path="externalRef" size="30"/>
			</li>
			<li class="dayOfweek">
				<label><spring:message code="transportoptions.monday"/>:</label>
				<form:checkbox class="isoptionPerDayOfWeek" path="scheduleRule[0].dayOfWeek" value="MONDAY"/>
				<form:input class="isoptionPerDayOfWeek" path="scheduleRule[0].cutoffTime" type="time"/>
			</li>
			<li class ="dayOfweek">
				<label><spring:message code="transportoptions.tuesday"/>:</label>
				<form:checkbox class="isoptionPerDayOfWeek" path="scheduleRule[1].dayOfWeek" value="TUESDAY"/>
				<form:input class="isoptionPerDayOfWeek" path="scheduleRule[1].cutoffTime" type="time"/>
			</li>
			<li class ="dayOfweek">
				<label><spring:message code="transportoptions.wednesday"/>:</label>
				<form:checkbox class="isoptionPerDayOfWeek" path="scheduleRule[2].dayOfWeek" value="WEDNESDAY"/>
				<form:input class="isoptionPerDayOfWeek" path="scheduleRule[2].cutoffTime" type="time"/>
			</li>
			<li class ="dayOfweek">
				<label><spring:message code="transportoptions.thursday"/>:</label>
				<form:checkbox class="isoptionPerDayOfWeek" path="scheduleRule[3].dayOfWeek" value="THURSDAY"/>
				<form:input class="isoptionPerDayOfWeek" path="scheduleRule[3].cutoffTime" type="time"/>
			</li>
			<li class ="dayOfweek">
				<label><spring:message code="transportoptions.friday"/>:</label>
				<form:checkbox class="isoptionPerDayOfWeek" path="scheduleRule[4].dayOfWeek" value="FRIDAY"/>
				<form:input class="isoptionPerDayOfWeek" path="scheduleRule[4].cutoffTime" type="time"/>
			</li>
			<li class ="dayOfweek">
				<label><spring:message code="transportoptions.saturday"/>:</label>
				<form:checkbox class="isoptionPerDayOfWeek" path="scheduleRule[5].dayOfWeek" value="SATURDAY"/>
				<form:input class="isoptionPerDayOfWeek" path="scheduleRule[5].cutoffTime" type="time"/>
			</li>
			<li class ="dayOfweek">
				<label><spring:message code="transportoptions.sunday"/>:</label>
				<form:checkbox class="isoptionPerDayOfWeek" path="scheduleRule[6].dayOfWeek" value="SUNDAY"/>
				<form:input class="isoptionPerDayOfWeek" path="scheduleRule[6].cutoffTime" type="time"/>
			</li>
		</ol>

	</fieldset>
</form:form>