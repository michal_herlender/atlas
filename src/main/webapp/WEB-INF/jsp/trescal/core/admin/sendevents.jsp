<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>


<t:crocodileTemplate>

	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="sendevent.pagetitle" /></span>
	</jsp:attribute>
	
	<jsp:attribute name="scriptPart">
		<script type="module" src="script/components/cwms-datetime/cwms-datetime.js"></script>
		<script src='script/trescal/core/admin/SendEvents.js'></script>
	</jsp:attribute>
	<jsp:body>
		<t:showErrors path="form.*" showFieldErrors="true"/>
		<form:form id="mainform" modelAttribute="form">
			<form:hidden path="action" />
			<div id="addexchangeformat" class="infobox">	
				<fieldset>
					<legend><spring:message code="sendevent.legendfieldset" /></legend>
					<div class="displaycolumn">
						<ol>
							<li>
								<label for="sendeventstatus"><spring:message code="sendevent.form.status" /></label>
								<form:select path="status" items="${statusOptions}" />
							</li>
							<li>
								<label for="activitydate"><spring:message code="sendevent.table.activitydate" /> :</label>
								<div class="form-item">
							<span ><spring:message code="sendevent.form.from" /></span>
							<form:input path="activityDateFrom" type="datetime-local" step="60"/>
								<span><spring:message code="sendevent.form.to" /></span>
							<form:input path="activityDateTo" type="datetime-local" step="60"/> 
							</div>
							</li>
							<li>
								<input type="button" onclick=" event.preventDefault(); this.disabled = true; submitForm('search'); " value="<spring:message code="sendevent.button.search" />"/>
							</li>
						</ol>
					</div>
					<div class="displaycolumn">
						<ol>
							<li>
								<label for="sendeventcompanysubdiv"><spring:message code="sendevent.form.companysubdiv" /> :</label>
								<div class="float-left extendPluginInput">
									<div class="compSearchJQPlugin">
										<input type="hidden" name="field" value="coid" />
										<input type="hidden" name="compCoroles" value="client" />
										<input type="hidden" name="tabIndex" value="1" />
										<input type="hidden" name="compIdPrefill" value="${form.coid}" />
										<input type="hidden" name="compNamePrefill" value="${form.comptext}" />
										<!-- company results listed here -->
									</div>
								</div>
							</li>
							<li>
								<label for="sendeventjob"><spring:message code="sendevent.form.job" /> :</label>
								<form:input path="jobno" length="30" />
							</li>
							<li>
								<label for="sendeventjobitem"><spring:message code="sendevent.form.jobitem" /> :</label>
								<form:input path="jobitemno" length="10" />
							</li>
						</ol>
					</div>				
					<form:hidden path="pageNo"/>
					<form:hidden id="pageNo" path="resultsPerPage"/>
					<t:paginationControl resultset="${rs}"/>
					
					<table id="table_sendevents" class="default2" summary="This table lists all events">	
						<thead>
							<tr>
								<th class="codefault"><spring:message code="sendevent.table.job" /></th>
								<th class="codefault"><spring:message code="sendevent.table.jobitem" /></th>
								<th class="codefault"><spring:message code="sendevent.table.companysubdiv" /></th>
								<th class="codefault"><spring:message code="sendevent.table.plantid" /></th>
								<th class="codefault"><spring:message code="sendevent.table.plantno" /></th>
								<th class="codefault"><spring:message code="sendevent.table.activitytype" /></th>
								<th class="codefault"><spring:message code="sendevent.table.senton" /></th>
								<th class="codefault"><spring:message code="sendevent.table.activitydate" /></th>
								<th class="codefault" rowspan="2">
									<center>
										<spring:message code="sendevent.table.selectall" /><br>
										<input onclick="CheckAll();"  type="checkbox" id="MainResendEvent" class="MainResendEvent"/>
									</center>
								</th>
							</tr>
							<tr>
								<th class="codefault" colspan="7"><spring:message code="sendevent.table.errormessage" /></th>
								<th class="codefault"><spring:message code="sendevent.table.errordate" /></th>
							</tr>
						</thead>
						<tbody>
					    	<c:forEach items="${ rs.results }" var="dto" varStatus="itemStatus">
					    		<c:set var="hasError" value="${ not empty dto.errorDate || not empty dto.lastErrorMessage }" />
					    		<c:set var="rowClass" value="${ itemStatus.index % 2 == 0 ? 'even' : 'odd' }" />
								<tr class="${rowClass}">
									<td>${ dto.jobno }</td>
									<td>${ dto.itemNo }</td>
									<td>${ dto.coname }</td>
									<td>${ dto.plantid }</td>
									<td>${ dto.plantno }</td>
									<td>${ dto.activityType }</td>
									<td><fmt:formatDate value="${ dto.sentOn }" type="both" dateStyle="SHORT" /></td>
									<td><fmt:formatDate  value="${ dto.activityDate }" type="both" dateStyle="SHORT" /></td>
									<td rowspan="${ hasError ? '2' : '1' }">
										<center>
											<form:checkbox path="resendIds[${dto.id}]" class="ResendEvent" value="true" />
										</center>
									</td>
								</tr>
								<c:if test="${ hasError }">
									<tr class="${rowClass}">
										<td colspan="7">${ dto.lastErrorMessage }</td>
										<td><fmt:formatDate value="${ dto.errorDate }" type="both" dateStyle="SHORT" /></td>
									</tr>
								</c:if>
							</c:forEach>
						</tbody>
					</table>
					<center>
						<input type="button" onclick=" event.preventDefault(); this.disabled = true; submitForm('resend'); " value="<spring:message code='sendevent.button.resendevents' />"/>
				   	</center>
				</fieldset>
			</div>
		</form:form>
	</jsp:body>
</t:crocodileTemplate>