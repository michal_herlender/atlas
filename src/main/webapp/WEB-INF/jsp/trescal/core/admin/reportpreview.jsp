<%-- File name: /trescal/core/admin/reportpreview.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">BIRT Report Preview</span>
	</jsp:attribute>
	<jsp:body>
		<t:showErrors path="form.*" showFieldErrors="true" />
		<form:form method="post" action="" modelAttribute="form">
			<div class="infobox">
				<fieldset>
					<legend>BIRT Report Preview</legend>
					<ol>
						<li>
							<form:label path="">Document Type:</form:label>
							<form:select path="documentType" items="${documentTypes}" itemLabel="value" itemValue="key" />
						</li>
						<li>
							<form:label path="">Entity ID:</form:label>
							<form:input path="entityId"/>
						</li>
						<li>
							<form:label path="">Document Locale:</form:label>
							<form:select path="documentLocale" items="${supportedLocales}" />
						</li>
					</ol>
				</fieldset>
				<form:button name="Submit" value="Submit">Submit</form:button>
			</div>
		</form:form>
	</jsp:body>
</t:crocodileTemplate>