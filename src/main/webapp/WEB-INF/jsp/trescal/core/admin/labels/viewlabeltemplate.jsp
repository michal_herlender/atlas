<%-- File name: /trescal/core/admin/labels/viewlabeltemplate.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code='viewlabeltemplate.details'/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        
    </jsp:attribute>
    <jsp:body>
		<div class="infobox">
			<fieldset>
				<legend><spring:message code="viewlabeltemplate.details" /></legend>
				<div class="displaycolumn">
					<ol>
						<li>
							<label><spring:message code="viewlabels.identifier" />:</label>
							<span><c:out value="${template.id}"/></span>
						</li>
						<li>
							<label><spring:message code="admin.description" />:</label>
							<span><c:out value="${template.description}"/></span>
						</li>
					</ol>
				</div>
				<div class="displaycolumn">
					<ol>
						<li>
							<label><spring:message code="viewlabels.labeltype" />:</label>
							<span><c:out value="${template.templateType}"/></span>
						</li>
						<li>
							<label><spring:message code="viewlabels.parametertype" />:</label>
							<span><c:out value="${template.parameterStyle}"/></span>
						</li>
					</ol>
				</div>
			</fieldset>
			<fieldset>
				<legend>XML</legend>
				<div>
					<pre><c:out value="${template.templateXml}"></c:out></pre>
				</div>
			</fieldset>
			<table class="default4" id="usagerules" >
				<thead>
					<tr>
						<td colspan="3">
							<div class="float-left"><spring:message code='viewlabeltemplate.usagerules' /> (${template.rules.size()})</div>
						</td>
					</tr>
					<tr>
						<th><spring:message code='viewlabels.identifier'/></th>
						<th><spring:message code='businesscompany'/></th>
						<th></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
					<c:forEach var="rule" items="${template.rules}">
						<tr id="rule-${rule.id}">
							<td><c:out value="${rule.id}" /></td>
							<td><c:out value="${rule.businessCompany.coname}" /></td>
							<td></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<table class="default4" id="parameters" >
				<thead>
					<tr>
						<td colspan="6">
							<div class="float-left"><spring:message code='viewlabeltemplate.parameters' /> (${template.parameters.size()})</div>
						</td>
					</tr>
					<tr>
						<th><spring:message code='viewlabels.identifier'/></th>
						<th><spring:message code='viewlabeltemplate.fieldname'/></th>
						<th><spring:message code='viewlabeltemplate.fieldvalue'/></th>
						<th><spring:message code='viewlabeltemplate.format'/></th>
						<th><spring:message code='viewlabeltemplate.text'/></th>
						<th></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="6">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
					<c:forEach var="parameter" items="${template.parameters}">
						<tr id="rule-${parameter.id}">
							<td><c:out value="${parameter.id}" /></td>
							<td><c:out value="${parameter.name}" /></td>
							<td><c:out value="${parameter.value}" /></td>
							<td><c:out value="${parameter.format}" /></td>
							<td><c:out value="${parameter.text}" /></td>
							<td></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</jsp:body>
</t:crocodileTemplate>
