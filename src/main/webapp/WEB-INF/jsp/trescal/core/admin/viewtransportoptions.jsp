<%-- File name: /trescal/core/admin/viewtransportoptions.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="adminarea.viewtransportoptions" /></span>
    </jsp:attribute>
    <jsp:body>
        <div class="infobox">
			<fieldset id="transportoptions">
				<legend><spring:message code="adminarea.viewtransportoptions" /></legend>
				<ol>									
					<li>
						<label><spring:message code="businesscompany"/>:</label>
						<span>${subdiv.comp.coname}</span>
					</li>
					<li>
						<label><spring:message code="businesssubdivision"/>:</label>
						<span>${subdiv.subname}</span>
					</li>
					<li>
						<div>
							<label><spring:message code="transportoptions.generaltransportoptions"/>:</label>
						</div>
						<div class="float-left">
							<c:forEach var="option" items="${transportOptionsWithActiveMethod}">
								<c:set var="displayClass" value=" ${option.active ? '' : 'strike' }"/>
								<span class="${displayClass} bold"> <cwms:besttranslation translations="${option.method.methodTranslation}" /></span>
								<span class="${displayClass}"> ${option.localizedName}</span><br>
							</c:forEach>
						</div>
						<div class="clear" />
					</li>
					<li>
						<label>&nbsp;</label>
						<c:choose>
							<c:when test="${editAuthorized}">
								<a class="mainlink" href="edittransportoptions.htm?subdivid=${subdiv.subdivid}"><spring:message code="adminarea.edittransportoptions"/></a>
							</c:when>
							<c:otherwise>
								<a class="mainlink" href="viewtransportoptionsdetails.htm?subdivid=${subdiv.subdivid}"><spring:message code="adminarea.viewtransportoptionsdetails"/></a>
							</c:otherwise>
						</c:choose>
					</li>
				</ol>
			</fieldset>
        </div>
    </jsp:body>
</t:crocodileTemplate>    