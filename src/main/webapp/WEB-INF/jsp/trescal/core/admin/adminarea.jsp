<%-- File name: /trescal/core/admin/adminarea.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<script type='text/javascript' src='script/trescal/core/admin/AdminArea.js'></script>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="adminarea.headtext" /></span>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox">
			<!-- Note, features are organized in groups -->
		
			<fieldset>
				<legend><spring:message code="adminarea.legend.global" /></legend>
				<ol>
					<li>
						<label><spring:message code="adminarea.userroles" /></label>
						<div class="float-left padtop">
							<c:forEach var="role" items="${userRoles}">
								${role.name}<br/>
							</c:forEach>
						</div>
						<div class="clear"></div>
					</li>
					<li>
						<label><spring:message code="adminarea.departmentroles" /></label>
						<div class="float-left padtop">
							<c:forEach var="role" items="${roles}">
								${role.role} <br />
		 	 				</c:forEach>
						</div>
						<div class="clear"></div>
					</li>									
					<li>
						<label>&nbsp;</label>
						<span><a href="viewcaltypes.htm" class="mainlink"><spring:message code="adminarea.managecaltypes" /></a></span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span><a href="viewcompanygroups.htm" class="mainlink"><spring:message code="adminarea.managecompanygroups" /></a></span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span><a href="supportedcurrency.htm" class="mainlink"><spring:message code="adminarea.managecurrencies" /></a></span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span><a href="viewlabelimages.htm" class="mainlink"><spring:message code="adminarea.managelabelimages" /></a></span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span><a href="viewlabeltemplates.htm" class="mainlink"><spring:message code="adminarea.managelabels" /></a></span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span><a href="viewmarkup.htm" class="mainlink"><spring:message code="adminarea.managemarkuprates" /></a></span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span><a href="viewprinters.htm" class="mainlink"><spring:message code="adminarea.manageprinters" /></a></span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span><a href="viewscheduledtasks.htm" class="mainlink"><spring:message code="adminarea.managescheduledtasks" /></a></span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span><a href="systemdefaultconfig.htm" class="mainlink"><spring:message code="adminarea.managesystemdefaults" /></a></span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span><a href="viewworkassignments.htm" class="mainlink"><spring:message code="adminarea.manageworkassignments" /></a></span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span><a href="workfloweditor.htm" class="mainlink"><spring:message code="adminarea.workfloweditor" /></a></span>
					</li>									
					<li>
						<label>&nbsp;</label>
						<span><a href="workflowbrowser.htm" class="mainlink"><spring:message code="adminarea.workflowbrowser" /></a></span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span><a href="managealiasgroups.htm" class="mainlink"><spring:message code="adminarea.managealiasgroups" /></a></span> 
					</li>
				</ol>
			</fieldset>
			<fieldset>
				<legend><spring:message code="adminarea.legend.global" /><c:out value=" - Adveso"/></legend>
				<ol>
					<li>
						<label>&nbsp;</label>
						<span><a href="manageactivities.htm" class="mainlink"><spring:message code="adminarea.manageactivities" /></a></span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span><a href="sendevents.htm" class="mainlink"><spring:message code="adminarea.sendevents" /></a></span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span><a href="sendnotifications.htm" class="mainlink"><spring:message code="adminarea.sendnotifications" /></a></span>
					</li>
				</ol>
			</fieldset>
			<fieldset>
				<legend><spring:message code="adminarea.legend.company" /></legend>
				<ol>
					<li>
						<label>&nbsp;</label>
						<span><a href="manageconditions.htm" class="mainlink"><spring:message code="adminarea.managequotationconditions" /></a></span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span><a href="viewcouriers.htm" class="mainlink"><spring:message code="adminarea.oldmanagecouriers" /></a></span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span><a href="managebusinesscompcouriers.htm" class="mainlink"><spring:message code="adminarea.managecouriers" /></a></span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span><a href="managecalconditions.htm" class="mainlink"><spring:message code="adminarea.managecalconditions" /></a></span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span><a href="viewdefaultnotes.htm" class="mainlink"><spring:message code="adminarea.managedefaultnotes" /></a></span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span><a href="editdefaultpricingremarks.htm" class="mainlink"><spring:message code="adminarea.managedefaultpricingremarks" /></a></span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span><a href="viewemailtemplates.htm" class="mainlink"><spring:message code="adminarea.manageemailtemplates" /></a></span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span><a href="managebusinesscomplogo.htm" class="mainlink"><spring:message code="adminarea.managebusinesscompanylogo" /></a></span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span><a href="managereversetraceabilityreport.htm" class="mainlink"><spring:message code="adminarea.managereversetraceabilityreport"/></a></span>
					</li>
				</ol>
			</fieldset>
			<fieldset>
				<legend><spring:message code="adminarea.legend.subdiv" /></legend>
				<ol>
					<li>
						<label>&nbsp;</label>
						<span><a href="businessEmails.htm" class="mainlink"><spring:message code="adminarea.managebusinessemails" /></a></span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span><a href="userpreferences.htm" class="mainlink"><spring:message code="adminarea.manageuserpreferences" /></a></span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span><a href="viewtransportoptions.htm?subdivid=${allocatedSubdiv.key}" class="mainlink"><spring:message code="adminarea.managetransportoptions" /></a></span>
					</li>
				</ol>
			</fieldset>
			<fieldset>
				<legend><spring:message code="adminarea.legend.userrights" /></legend>
				<ol>
					<li>
						<label>&nbsp;</label>
						<span><cwms:securedLink permission="PERMISSION_GROUPS" classAttr="mainlink"><spring:message code="adminarea.viewpermissiongroups"/></cwms:securedLink></span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span><cwms:securedLink permission="USER_ROLES" classAttr="mainlink"><spring:message code="adminarea.viewuserroles"/></cwms:securedLink></span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span><cwms:securedLink permission="MANAGE_PERMISSION_LIMIT" classAttr="mainlink"><spring:message code="adminarea.managepermissionlimit"/></cwms:securedLink></span>
					</li>
				</ol>
			</fieldset>
			<fieldset>
				<legend><spring:message code="adminarea.legend.hidden" /></legend>
				<ol>
					<li>
						<label>&nbsp;</label>
						<span><a href="searchprocedures.htm" class="mainlink"><spring:message code="adminarea.searchprocedures" /></a></span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span><a href="<c:url value="/searchwi.htm"/>" class="mainlink"><spring:message code="menu.calibration.searchworkinstructions"/></a></span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span><a href="searchmodelcomponentform.htm" class="mainlink"><spring:message code="modcompbrow.title" /></a></span>
					</li>
					
				</ol>
			</fieldset>
			<fieldset>
				<legend><spring:message code="adminarea.legend.unsupported" /></legend>
				<ol>
					<li>
						<label><spring:message code="adminarea.audittriggers" />:</label>
						<span>
							<c:choose>
								<c:when test="${auditTriggersEnabled}">
									<spring:message code="adminarea.enabled" />
								</c:when>
								<c:otherwise>
									<spring:message code="adminarea.disabled" />
								</c:otherwise>
							</c:choose>
							(Note: Legacy feature that is not supported or working, therefore remains disabled.)
						</span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span>
							<cwms:securedLink permission="SUPPLIER"  classAttr="mainlink"  collapse="True" >
								<spring:message code="company.supplierapprovaltypes"/>
							</cwms:securedLink>
						</span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span><a href="viewexceptions.htm" class="mainlink"><spring:message code="adminarea.viewexceptions" /></a></span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span><a href="viewproperties.htm" class="mainlink">Configuration Check</a></span>
					</li>
					<!-- Feature was hidden - possibly not fully working?  -->
					<!-- 
					<li>
						<label>&nbsp;</label>
						<span><a href="managepaymentterms.htm" class="mainlink"><spring:message code="adminarea.managepaymentterms" /></a></span>
					</li>
					-->
					<li>
						<label>&nbsp;</label>
						<span><a href="viewprintfilerules.htm" class="mainlink"><spring:message code="adminarea.manageprintfilerules" /></a></span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span><a href="viewqueuedcalibrations.htm" class="mainlink"><spring:message code="workflow.viewqueuedcalibrations" /></a></span>
					</li>
				</ol>
			</fieldset>
			<fieldset>
				<legend><spring:message code="menu.asset" /></legend>
				<ol>
					<li>
						<label>&nbsp;</label>
						<span><a href="<c:url value="/assetsearch.htm"/>" class="mainlink"><spring:message code="menu.asset.search"/></a></span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span><a href="<c:url value="/addeditasset.htm"/>" class="mainlink"><spring:message code="menu.asset.add"/></a></span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span><a href="<c:url value="/newassetfrominst.htm"/>" class="mainlink"><spring:message code="menu.asset.addinstrument"/></a></span>
					</li>
				</ol>
			</fieldset>
			<fieldset>
				<legend><spring:message code="adminarea.legend.development" /></legend>
				<ol>
					<li>
						<label>&nbsp;</label>
						<span><a href="dymosandbox.htm" class="mainlink"><spring:message code="adminarea.dymodemo" /></a></span>
					</li>									
					<li>
						<label>&nbsp;</label>
						<span><a href="upgrade.htm" class="mainlink">Perform Upgrade</a></span>
					</li>									
					<li>
						<label>&nbsp;</label>
						<span><a href="reportpreview.htm" class="mainlink"><spring:message code="adminarea.previewreports" /></a></span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span><a href="emailpreview.htm" class="mainlink"><spring:message code="adminarea.previewemails" /></a></span>
					</li>
					<li>
						<label>&nbsp;</label>
						<span><a href="bgtasks.htm" class="mainlink">All Background tasks</a></span>
					</li>
<!-- 					<li> -->
<!-- 						<label>&nbsp;</label> -->
<!-- 						<span><a href="serverinstancebgtasks.htm" class="mainlink">Instance background tasks</a></span> -->
<!-- 					</li> -->
				</ol>
			</fieldset>
			<fieldset>
				<legend><spring:message code="adminarea.legend.development" /> - <spring:message code="menu.reports" /></legend>
				<ol>
					<li>
						<label>Important Note</label>
						<span>2021-06-28 - Reports removed as summary tables are intentionally no longer created.</span>
					</li>
				</ol>
			</fieldset>
		</div>
	</jsp:body>
</t:crocodileTemplate>

    