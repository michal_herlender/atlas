<%-- File name: /trescal/core/admin/managereversetraceabilityreport.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="managereversetraceabilityreport.headertext"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/admin/ManageReverseTraceabilityReport.js'></script>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox">
		  <fieldset>
			<legend>${allocatedCompany.value}</legend>
			<form:form modelAttribute="form" action="" method="post">
				<table class="default4">
					<thead>
							<tr>
								<th scope="col"><spring:message code="subdivision"/></th>
								<th scope="col" style=" text-align: center;"><spring:message code="mailgrouptype.reversetraceability"/></th>
								<th scope="col" style=" text-align: center;"><spring:message code="bpo.startdate"/></th>
							</tr>
					</thead>
					<tbody>
						<c:forEach var="rt" items="${reverseTraceabilitySettingsList}" varStatus="loopStatus">
							<c:set var="i" value="${loopStatus.index}" />
							<form:hidden path="reverseTraceabilitySettingsDtos[${i}].id" value="${rt.id}"/>
							<tr>
								<td>
									<c:out value="${rt.subname}" />
									<form:hidden path="reverseTraceabilitySettingsDtos[${i}].subdivid" value="${rt.subdivid}"/>
								</td>
								<td class="center">
									<form:select path="reverseTraceabilitySettingsDtos[${i}].reverseTraceability">
											<option value="true" <c:if test ="${rt.reverseTraceability}"> selected</c:if>><spring:message code="yes" /></option>
											<option value="false" <c:if test ="${ not rt.reverseTraceability}"> selected</c:if>><spring:message code="no" /></option>
									</form:select>
								</td>
								<td class="center">
									<form:input path="reverseTraceabilitySettingsDtos[${i}].startDate" type="date" value="${cwms:isoDate(rt.startDate)}"/>
								</td>
							</tr>
						</c:forEach>
						<c:if test="${activeSubdivsNotAddedYet.size() != 0}">
							<tr>
								<td colspan="11">
									<div class="float-left"><a href="" onClick="event.preventDefault(); enableReverseTraceabilityForOtherSubdiv(); "><img src="img/icons/add.png" height="20" width="20" alt="<spring:message code="transportoptions.add"/>"/> </a></div>
									<div class="padtop"><span>&nbsp; <spring:message code="managereversetraceabilityreport.enablelink"/></span></div>
								</td>
							</tr>
						</c:if>
						
					</tbody>
				</table>
				<br><form:button name="submit" value="submit"><spring:message code="save"/></form:button>
			</form:form>
		  </fieldset>
		</div>
	</jsp:body>
</t:crocodileTemplate>