<%-- File name: /trescal/core/admin/viewtransportoptionsdetails.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="adminarea.viewtransportoptionsdetails" /></span>
    </jsp:attribute>
    <jsp:body>
        <div class="infobox">
			<fieldset id="transportoptions">
				<legend><spring:message code="adminarea.viewtransportoptionsdetails" /></legend>
				<ol>									
					<li>
						<label><spring:message code="businesscompany"/>:</label>
						<span>${subdiv.comp.coname}</span>
					</li>
					<li>
						<label><spring:message code="businesssubdivision"/>:</label>
						<span>${subdiv.subname}</span>
					</li>
				</ol>
				<div class="infobox">
					<table class="default4 editTransportOptionForm">
						<thead>
							<tr>
								<th style="text-align: center;" colspan="3"><spring:message code="transportoptions"/></th>
								<th style="text-align: center;" colspan="7"><spring:message code="transportoptions.cutofftimes"/></th>
							</tr>
							<tr>
								<th scope="col"><spring:message code="jiactions.transmethod"/></th>
								<th class="active" scope="col" style="text-align: center;"><spring:message code="active"/></th>
								<th class="name" scope="col" style="text-align: center;"><spring:message code="transportoptions.localizedname"/></th>
								<th class="dayofweek" scope="col" style="text-align: center;"><spring:message code="transportoptions.monday"/></th>
								<th class="dayofweek" scope="col" style="text-align: center;"><spring:message code="transportoptions.tuesday"/></th>
								<th class="dayofweek" scope="col" style="text-align: center;"><spring:message code="transportoptions.wednesday"/></th>
								<th class="dayofweek" scope="col" style="text-align: center;"><spring:message code="transportoptions.thursday"/></th>
								<th class="dayofweek" scope="col" style="text-align: center;"><spring:message code="transportoptions.friday"/></th>
								<th class="dayofweek" scope="col" style="text-align: center;"><spring:message code="transportoptions.saturday"/></th>
								<th class="dayofweek" scope="col" style="text-align: center;"><spring:message code="transportoptions.sunday"/></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="option" items="${transportOptionsWithActiveMethod}" varStatus="loopStatus">	
								<c:set var="i" value="${loopStatus.index}" />
								<tr style="width: 300px;">
									<td >
										<span> <cwms:besttranslation translations="${option.method.methodTranslation}" />
									</td>
	
									<td class="center">
										<c:set var="active" value="${option.active ? 'yes' : 'no'}"/>
										<spring:message code="${active}" />
									</td>
									
									<td>
										${option.localizedName}
									</td>
								<c:choose>
										<c:when test="${option.method.optionPerDayOfWeek}">
											<c:set var="timemonday" value=""/>
											<c:set var="timetuesday" value=""/>
											<c:set var="timewednesday" value=""/>
											<c:set var="timethursday" value=""/>
											<c:set var="timefriday" value=""/>
											<c:set var="timesaturday" value=""/>
											<c:set var="timesunday" value=""/>
											<c:forEach var="scRule" items="${option.scheduleRules}">
												<c:choose>
													<c:when test ="${scRule.dayOfWeek == 'MONDAY'}">
														<c:set var="timemonday" value="${scRule.cutoffTime}"/>
													</c:when>
													<c:when test ="${scRule.dayOfWeek == 'TUESDAY'}">
														<c:set var="timetuesday" value="${scRule.cutoffTime}"/>
													</c:when>
													<c:when test ="${scRule.dayOfWeek == 'WEDNESDAY'}">
														<c:set var="timewednesday" value="${scRule.cutoffTime}"/>
													</c:when>
													<c:when test ="${scRule.dayOfWeek == 'THURSDAY'}">
														<c:set var="timethursday" value="${scRule.cutoffTime}"/>
													</c:when>
													<c:when test ="${scRule.dayOfWeek == 'FRIDAY'}">
														<c:set var="timefriday" value="${scRule.cutoffTime}"/>
													</c:when>
													<c:when test ="${scRule.dayOfWeek == 'SATURDAY'}">
														<c:set var="timesaturday" value="${scRule.cutoffTime}"/>
													</c:when>
													<c:when test ="${scRule.dayOfWeek == 'SUNDAY'}">
														<c:set var="timesunday" value="${scRule.cutoffTime}"/>
													</c:when>
											</c:choose>
										</c:forEach>
											<td class="center">
												${timemonday}
											</td>
											<td class="center">
												${timetuesday}
											</td>
											<td class="center">
												${timewednesday}
											</td>
											<td class="center">
												${timethursday}
											</td>
											<td class="center">
												${timefriday}
											</td>
											<td class="center">
												${timesaturday}
											</td>
											<td class="center">
												${timesunday}
											</td>
										</c:when>
										<c:otherwise>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</c:otherwise>
									</c:choose>
								</tr>
							</c:forEach>
						
						</tbody>
					</table>
				</fieldset>
        	</div>
    	</jsp:body>
</t:crocodileTemplate>