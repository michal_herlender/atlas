<%-- File name: /trescal/core/admin/manageconditions.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="manageconditions.headertext"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/admin/ManageConditions.js'></script>
	</jsp:attribute>
	<jsp:body>
		<form:form method="post" action="" name="f1" modelAttribute="manageconditions">
			<div class="infobox">
				<fieldset>
					<legend><spring:message code='manageconditions.headertext'/></legend>
					<ol>
						<li>
							<form:label path="conditionText"><spring:message code='admin.conditions'/></form:label>
							<form:textarea path="conditionText" rows="10" id="conditionTextArea" class="width70" onkeyup="updatePreview($j(this).val());"/>
							<form:errors path="conditionText"/>
						</li>
						<li>
							<label>&nbsp;</label>
							<input name="submit" type="submit" value="<spring:message code='admin.submit'/>"/>
						</li>
						<li>
							<label for="preview">Preview</label>
							<div id="preview">
								${manageconditions.conditionText}
							</div>
							<div class="clear"></div>
						</li>
					</ol>
				</fieldset>
			</div>
		</form:form>
	</jsp:body>
</t:crocodileTemplate>