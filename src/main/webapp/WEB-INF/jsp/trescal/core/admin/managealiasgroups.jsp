<%@page import="java.util.Locale"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>

<style>
.addAliaseLink{
	float: right;
	margin-right: 10px;
	text-decoration: underline;
	cursor: pointer;
	color: #0063be;
}

.addAliaseLink:hover{
	color:  #990033;
}

#updateAliasGroups{
	cursor: pointer;
}

.details-control{
	cursor: pointer;
}

.defaultbusnicomp{
	text-decoration: underline;
	cursor: pointer;
	color: #0063be;
}
</style>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message	code="adminarea.managealiasgroups" /></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
	<script>
		var aliases = new Array();
			<c:forEach var="aliasgroup" items="${ aliasGroups }">
				<c:forEach var="alias" items="${ aliasgroup.aliasesForm }">
					var translation ='';
					
					if("${alias.effn}" == 'INTERVAL_UNIT'){
						translation = "${alias.intervalUnit.getSingularName(locale)}";
					}else if("${alias.effn}" == 'EXPECTED_SERVICE'){
						<c:forEach var="trans" items="${alias.serviceType.shortnameTranslation}">
							if(${trans.locale eq locale})
							{
								translation = "${trans.translation}";
							}
						</c:forEach>
					}
					aliases.push({
						'aliasgroupid' : "${aliasgroup.alaisGroupId}",
						'column' : "${alias.effn.getValue()}",
						'value'  :"${alias.value}",
						'translation' : translation,
						'alias'  :"${alias.alias}"
 					});
			
				</c:forEach>
			</c:forEach>
	</script>
		<script src="script/trescal/core/logistics/exchangeformat/aliasgroup/aliasGroup.js"></script>
		<script src="script/trescal/core/admin/ManageAliasGroups.js"></script>
	</jsp:attribute>
	
	<jsp:body>
	
	<fieldset>
		<legend><spring:message	code="adminarea.managealiasgroups" /></legend>	
			<div id="infobox">
				<div class="addAliaseLink" onclick="addEditAliasGroup('add',null)"> <span> <spring:message code="aliasgroup.addaliasgroup"/> </span> </div>
				<table id="aliasgroups" class="default2">
					<thead>
						  <tr>
						  		<th></th>
							    <th class="center"><spring:message code="aliasgroup.aliasgroupname"/></th>
							    <th class="center"><spring:message code="aliasgroup.aliasgroupdescription"/></th>
							    <th class="center"><spring:message code="aliasgroup.defaultbusinesscompany"/></th>
							    <th class="center"><spring:message code="aliasgroup.action"/></th>
						  </tr>				
					</thead>
					<tbody>
						  <c:forEach var="aliasGroup" items="${aliasGroups}">
						  	<tr>
						  		<td class="center details-control"> 
						  			<img id="open"  alt="" src="img/icons/details_open.png">
						  			<img id="close" class="hid"  alt="" src="img/icons/details_close.png">
						  			<input id="aliasgroupid" type="hidden" value="${aliasGroup.alaisGroupId}" /> 
						  		</td>
							    <td class="center">${aliasGroup.aliasGroupName}</td>
							    <td class="center">${aliasGroup.aliasGroupDesc}</td>
							    <td class="center">
							    	<c:if test="${aliasGroup.defaultForCurrentBusinessComp ne false}">
							    		<a  class="defaultbusnicomp" target="_blank" href="viewcomp.htm?coid=${aliasGroup.allocatedCompanyid}&ajax=company&width=400">(${aliasGroup.allocatedCompanyName})</a>
							    	</c:if>
							    	<c:if test="${aliasGroup.defaultForCurrentBusinessComp ne true}">
							    		--
							    	</c:if>
							    </td>
							    <td class="center"> 
							    	<img src="img/icons/form_edit.png" width="16" height="16" alt=""title="" onclick="addEditAliasGroup('edit',${aliasGroup.alaisGroupId})" id="updateAliasGroups" />
							    		&nbsp;
							    	<img src="img/icons/delete.png" width="16" height="16" alt=""title="" onclick="deleteAliasGroup(${aliasGroup.alaisGroupId});" id="updateAliasGroups" />
							    </td>	
						  	</tr>					  
						  </c:forEach>
					</tbody>
				</table>
	
			</div>
		</fieldset>
	</jsp:body>
</t:crocodileTemplate>