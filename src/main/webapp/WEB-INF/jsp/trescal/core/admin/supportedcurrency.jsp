<%-- File name: /trescal/core/admin/supportedcurrency.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code='supportedcurrency.headertext' /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/admin/SupportedCurrency.js'></script>
    </jsp:attribute>
    <jsp:body>
    
    <c:set var="defcur" value="${command.defaultcurrency}"/>
		<div class="infobox">
			
			<fieldset>
			
				<legend><spring:message code='supportedcurrency.defaultcurrency' /></legend>
				
				<ol>
					<li>
						<label><spring:message code='supportedcurrency.current' /></label>
						<span><spring:message code='supportedcurrency.currentdefaultis' /><c:out value=" ${defcur.currencyCode} ${defcur.currencyName} (${defcur.currencySymbol})" /></span>
					</li>
				</ol>
			
			</fieldset>	
			
			<table class="default4 supportedCurrencies" summary="">
				<thead>
					<tr>
						<td colspan="7"><span class="bold"><spring:message code='supportedcurrency.systemcurrencies' /></span> - <spring:message code='supportedcurrency.currentlyenabled' /></td>
					</tr>
					<tr>
						<th class="nameffff" scope="col"><spring:message code='admin.name' /></th>
						<th class="code" scope="col"><spring:message code='supportedcurrency.code' /></th>
						<th class="symbol" scope="col"><spring:message code='supportedcurrency.symbol' /></th>
						<th class="exrate" scope="col"><spring:message code='supportedcurrency.exchangerate' /></th>
						<th class="setby" scope="col"><spring:message code='supportedcurrency.setby' /></th>
						<th class="seton" scope="col"><spring:message code='supportedcurrency.seton' /></th>
						<th class="history" scope="col"><spring:message code='supportedcurrency.history' /></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="currency" items="${command.currencies}">
						<tr>
							<td class="name">${currency.currencyName}</td>
							<td class="code">${currency.currencyCode}</td>
							<td class="symbol">${currency.currencySymbol}</td>
							<td class="exrate">${defcur.currencySymbol}1 = ${currency.currencySymbol}
								<span id="${currency.currencyCode}notediting">
									<span id="${currency.currencyId}displayrate">
										${currency.defaultRate}
									</span>
									<c:choose>	
										 <c:when test="${currency.currencyId != defcur.currencyId}">
										<a href="#" class="mainlink" onclick="toggleEdit('${currency.currencyCode}', true); return false;" ><spring:message code='admin.edit' /></a>
										 </c:when>
									   <c:otherwise>
										(<spring:message code='supportedcurrency.ratefixed'/>)
									   </c:otherwise>
								    </c:choose>
								</span>
								<c:if test="${currency.currencyId != defcur.currencyId}">
									<span id="${currency.currencyCode}editing" class="hid">
										${defcur.currencySymbol}1 = ${currency.currencySymbol}
										<input id="${currency.currencyId}rate" value="${currency.defaultRate}"/>
										<input type="button" value="<spring:message code='admin.submit'/>" onclick="saveNewCurrencyRate(${currency.currencyId}, $j('#' + '${currency.currencyId}rate').val(),'${currency.currencyCode}');"/>
										<input type="button" value="<spring:message code='supportedcurrency.cancel' />" onclick="toggleEdit('${currency.currencyCode}', false);"/>
									</span>
								</c:if>
								
							</td>
							<td class="setby"><span id="${currency.currencyId}ratesetby">${currency.rateLastSetBy.name}</span></td>
							<td class="seton"><span id="${currency.currencyId}rateseton"><fmt:formatDate  type="date" dateStyle="SHORT" value="${currency.rateLastSet}" /></span></td>										
							<td class="history"><span id="${currency.currencyId}historysize">${currency.rateHistory.size()}</span> <a href="#" class="mainlink" onclick=" toggleHistoryDisplay(${currency.currencyId}); return false; "><spring:message code='supportedcurrency.view' /></a></td>
						</tr>
						<tr id="${currency.currencyId}history" class="hid">
							<td colspan="7">
								<table class="child_table" summary="">
									<thead>
										<tr id="${currency.currencyId}historyhead">
											<th scope="col"><spring:message code='supportedcurrency.rate' /></th>
											<th scope="col"><spring:message code='supportedcurrency.datefrom' /></th>
											<th scope="col"><spring:message code='supportedcurrency.dateto' /></th>
											<th scope="col"><spring:message code='supportedcurrency.originallysetby' /></th>
										</tr>
									</thead>
									<tbody>
								<c:choose>	
									<c:when test="${currency.rateHistory.size() > 0}" >
										<c:forEach var="hist" items="${currency.rateHistory}">
												<tr>
													<td>${hist.rate}</td>
													<td><fmt:formatDate type="date" dateStyle="SHORT" value="${hist.dateFrom}"/></td>
													<td><fmt:formatDate type="date" dateStyle="SHORT" value="${hist.dateTo}"/></td>
													<td>${hist.setBy.name}</td>
												</tr>
										</c:forEach>
								    </c:when>		
								 	  <c:otherwise>
											<tr id="${currency.currencyId}nohistory">
												<td colspan="4"><spring:message code='supportedcurrency.nohistory' /></td>
											</tr>
									  </c:otherwise>
							 </c:choose>
									</tbody>
								</table>
							</td>
						</tr>
				</c:forEach>
				</tbody>
			</table>
			
			<p class="bold">* <spring:message code='supportedcurrency.warning' /></p>
			
			<p><spring:message code='supportedcurrency.dailyexchangerate' /><a href="http://www.xe.com" class="mainlink" target="_blank"> www.xe.com</a></p>
		
		</div>
    </jsp:body>
</t:crocodileTemplate>