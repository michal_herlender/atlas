<%-- File name: /trescal/core/admin/viewworkassignments.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="viewworkassignments.headertext"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/admin/ViewWorkAssignments.js'></script>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox">
			<fieldset>
				<legend><spring:message code='viewworkassignments.headertext'/></legend>
				<ol>
					<li>
						<label><spring:message code='viewworkassignments.departmenttype'/></label>
						<select onchange=" showDepartment(this.value); " name="departmentType" id="departmentType">
							<c:forEach var="keyValue" items="${keyValues}" varStatus="itemStatus">
								<c:set var="selectedText" value=""/>
								<c:if test="${itemStatus.first}">
									<c:set var="selectedText" value="selected"/>
									<c:set var="firstKey" value="${keyValue.key}"/>
								</c:if>
								<option value="${keyValue.key}" ${selectedText}>${keyValue.value}</option>
							</c:forEach>
						</select>
					</li>
				</ol>
			</fieldset>
			<%-- Build map of department types --%>
			<c:forEach var="dt" items="${deptTypes}" varStatus="itemStatus">
				<c:choose>
					<c:when test="${dt == firstKey}">
						<c:set var="displayClass" value="vis" />
					</c:when>
					<c:otherwise>
						<c:set var="displayClass" value="hid" />
					</c:otherwise>
				</c:choose>
				<div id="depttype${dt}" class="${displayClass}">
					<c:choose>
						<c:when test="${deptTypeMap.get(dt).size() > 0}">
							<c:forEach var="wa" items="${deptTypeMap.get(dt)}">
								<table id="wa${wa.id}" class="default4 workassignmenttable">
									<thead>
										<tr>
											<th colspan="4"><span class="larger-text">${wa.stateGroup.displayName}</span></th>
										</tr>
										<tr>
											<td class="status"><spring:message code='viewworkassignments.status' /></td>
											<td class="active"><spring:message code='active' /></td>
											<td class="linktype"><spring:message code='linktype' /></td>
											<td class="type"><spring:message code='type' /></td>
										</tr>
									</thead>
									<tfoot>
										<tr>
											<td colspan="4">&nbsp;</td>
										</tr>
									</tfoot>
									<tbody>
										<c:set var="stateGroupLinks" value="${stateGroupMap.get(wa.stateGroup)}" />
										<c:forEach var="sgl" items="${stateGroupLinks}">
											<tr>
												<td>${sgl.state.stateid} - <cwms:besttranslation translations="${sgl.state.translations}"/></td>
												<td>${sgl.state.active}</td>
												<td>${sgl.type}</td>
												<td>
													<c:choose>
														<c:when test="${sgl.state.status}">Status</c:when>
														<c:otherwise>Activity</c:otherwise>
													</c:choose>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<table class="default4 workassignmenttable">
								<tr>
									<td colspan="4" align="center"><i>-- <spring:message code='viewworkassignments.noassignment'/> --</i></td>
								</tr>
							</table>								
						</c:otherwise>
					</c:choose>
				</div>
			</c:forEach>
			<table class="default4 workassignmenttable" summary="">
				<thead>
					<tr>
						<th colspan="4"><span class="larger-text"><spring:message code='viewworkassignments.nostatus'/></span></th>
					</tr>
					<tr>
						<td><spring:message code='viewworkassignments.status' /></td>
						<td><spring:message code='active' /></td>
						<td><spring:message code='linktype' /></td>
						<td><spring:message code='type' /></td>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="4">&nbsp;Note: Only Item Status values where "Active = No" should appear on this list</td>
					</tr>
				</tfoot>
				<tbody>
					<c:forEach var="itemState" items="${itemStateMap.values()}">
						<tr>
							<td class="status">${itemState.stateid} - <cwms:besttranslation translations="${itemState.translations}"/></td>
							<td class="active">${itemState.active}</td>
							<td class="linktype"><spring:message code='notapplicable' /></td>
							<td class="type">Status</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</jsp:body>
</t:crocodileTemplate>