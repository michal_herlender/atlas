<%-- File name: /trescal/core/admin/editprintfilerule.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="editprintfilerule.headertext" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/admin/EditPrintFileRule.js'></script>
    </jsp:attribute>
    <jsp:body>
        <div class="infobox">
			<form:form action="" method="post" modelAttribute="form">
				<form:errors path="*">
                    		<div class="warningBox1">
                        		<div class="warningBox2">
                            		<div class="warningBox3">
                                		<h5 class="center-0 attention">
                                    		<spring:message code='error.save' />
                                		</h5>
                                		<c:forEach var="e" items="${messages}">
                                    		<div class="center attention"><c:out value="${e}"/></div>
                                		</c:forEach>
                            		</div>
                        		</div>
                    		</div>
                 </form:errors>
				<fieldset>
					<legend><spring:message code="editprintfilerule.headertext" /></legend>
					<ol>
						<li>
							<form:label path="pfr.fileNameLike"><spring:message code="editprintfilerule.filenamelike" />*</form:label>
							<form:input size="60" id="pfr.fileNameLike" path="pfr.fileNameLike" value="${form.pfr.fileNameLike}" />
							<span class="attention">
								<form:errors path="pfr.fileNameLike"/>
							</span>
						</li>
						<li>
							<form:label path="printerId"><spring:message code="editprintfilerule.printer" /></form:label>
							<form:select path="printerId" id="printerId">	
								<option value="0">-- <spring:message code="editprintfilerule.unspecified" /> --</option>
								<c:forEach var="p" items="${printers}">
									<option value="${p.id}" 
									<c:if test="${p.id == form.pfr.printer.id}"> selected="selected" 
									</c:if>>${p.description}</option>
								</c:forEach>
							</form:select>
							<span class="attention">
								<form:errors path="printerId"/>
							</span>
						</li>
						<li>
							<form:label path="pfr.firstPagePaperType"><spring:message code="admin.firstpagepapertype" /></form:label>
							<form:select path="pfr.firstPagePaperType">
								<option value="">-- <spring:message code="editprintfilerule.unspecified" /> --</option>
								<c:forEach var="papertype" items="${papertypes}">
									<option value="${papertype}" <c:if test="${papertype == form.pfr.firstPagePaperType}"> selected="selected"</c:if>>${papertype.description}</option>
								</c:forEach>
							</form:select>
							<span class="attention">
								<form:errors path="pfr.firstPagePaperType"/>
							</span>
						</li>
						<li>
							<form:label path="pfr.restPagePaperType"><spring:message code="admin.restpagepapertype" /></form:label>
							<form:select path="pfr.restPagePaperType">
								<option value="">-- <spring:message code="editprintfilerule.unspecified" /> --</option>
								<c:forEach var="papertype" items="${papertypes}">
									<option value="${papertype}" <c:if test="${papertype == form.pfr.restPagePaperType}"> selected="selected" </c:if>>${papertype.description}</option>
								</c:forEach>
							</form:select>
							<span class="attention">
								<form:errors path="pfr.restPagePaperType"/>
							</span>
						</li>
						<li>
							<form:label path="pfr.copies"><spring:message code="admin.copies" /></form:label>
							<form:select path="pfr.copies">
								<option value="">-- <spring:message code="editprintfilerule.unspecified" /> --</option>
								<c:forEach var="n" begin="1" end="10">
									<option value="${n}" <c:if test="${form.pfr.copies == n}"> selected="selected" </c:if>>${n}</option>
								</c:forEach>
							</form:select>
							<span class="attention">
								<form:errors path="pfr.copies"/>
							</span>
						</li>
						<li>
							<label>&nbsp;</label>
							<input type="submit" value="<spring:message code='admin.submit' />" />
						</li>
					</ol>
				</fieldset>
			</form:form>
		</div>
    </jsp:body>
</t:crocodileTemplate>