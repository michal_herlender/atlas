<%-- File name: /trescal/core/admin/emailpreview.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">Email Preview</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/admin/EmailPreview.js'></script>
	</jsp:attribute>
	<jsp:body>
		<t:showErrors path="form.*" showFieldErrors="true" />
		<form:form method="post" action="" target="preview_frame" modelAttribute="form">
			<div class="infobox">
				<fieldset>
					<legend>Email Preview</legend>
					<ol>
						<li>
							<form:label path="">Email Type:</form:label>
							<form:select path="contentType" items="${contentTypes}" itemLabel="value" itemValue="key" />
						</li>
						<li>
							<form:label path="">Entity ID:</form:label>
							<form:input path="entityId" />
						</li>
						<li>
							<form:label path="">Document Locale:</form:label>
							<form:select path="locale" items="${supportedLocales}" />
						</li>
						<li>
							<form:label path="">Use Thymeleaf:</form:label>
							<form:checkbox path="thymeleaf" />
						</li>
						<li>
							<form:label path="">&nbsp;</form:label>
							<form:button name="Submit" value="Submit">Submit</form:button>
						</li>
					</ol>
				</fieldset>
				<c:if test="${not empty body && not empty subject}">
					<div id="preview_div" class="hid">${body}</div>
					<fieldset>
						<legend>Results</legend>
						<ol>
							<li>
								<form:label path="">Subject:</form:label>
								<span><c:out value="${subject}" /></span>							
							</li>
							<li>
								<form:label path="">Body:</form:label>
							</li>
							<li>
								<iframe width="100%" height="1000" id="preview_iframe"></iframe>
							</li>
						</ol>
					</fieldset>
				</c:if>
			</div>
		</form:form>
	</jsp:body>
</t:crocodileTemplate>