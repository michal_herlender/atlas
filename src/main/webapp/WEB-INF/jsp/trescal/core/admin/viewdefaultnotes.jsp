<%-- File name: /trescal/core/admin/viewdefaultnotes.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="viewdefaultnotes.headertext" arguments="${allocatedCompany.value}" /></span>
    </jsp:attribute>
    <jsp:body>
        <div class="infobox">
			<table class="default4" id="defaultnotes">
				<thead>
					<tr>
						<td colspan="7">
							<spring:message code="viewdefaultnotes.headertext" arguments="${allocatedCompany.value}" /> (${notes.size()})
						</td>
					</tr>
					<tr>
						<th><spring:message code="type" /></th>
						<th><spring:message code="company.language" /></th>
						<th><spring:message code="label" /></th>
						<th><spring:message code="note.note" /></th>
						<th><spring:message code="note.publish" /></th>
						<th><spring:message code="admin.edit" /></th>
						<th><spring:message code="admin.delete" /></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="7"><a href="editdefaultnote.htm" class="mainlink"><spring:message code="addnote" /></a></td>
					</tr>
				</tfoot>
				<tbody>
					<c:choose>
						<c:when test="${not empty notes}">
							<c:forEach var="note" items="${notes}" varStatus="loopStatus">
								<c:set var="rowclass" value="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}" /> 
								<tr id="note-${note.defaultNoteId}" class="${rowclass}">
									<td>${note.component.componentName}</td>
									<td><spring:message code="default" /></td>
									<td>${note.label}</td>
									<td>${note.note.replaceAll("\\n", "<br/>")}</td>
									<td><spring:message code="${note.publish}" /></td>
									<td><a class="mainlink" href="editdefaultnote.htm?id=${note.defaultNoteId}"><spring:message code="admin.edit" /></a></td>
									<td><a class="mainlink" href="deletedefaultnote.htm?id=${note.defaultNoteId}"><spring:message code="admin.delete" /></a></td>
								</tr>
								<c:forEach var="locale" items="${supportedLocales}">
								<c:if test="${locale !='en_GB' }">
										<c:set var="labelTranslation" value="${labelTranslations.get(note.defaultNoteId).get(locale)}" />
										<c:set var="noteTranslation" value="${noteTranslations.get(note.defaultNoteId).get(locale)}" />
										<c:if test="${not empty labelTranslation && not empty noteTranslation}">
											<tr id="note-${note.defaultNoteId}" class="${rowclass}">
												<td></td>
												<td>${locale.getDisplayLanguage(rc.locale)} (${locale.getDisplayCountry(rc.locale)})</td>
												<td>${labelTranslation}</td>
												<td>${noteTranslation.replaceAll("\\n", "<br/>")}</td>
												<td></td>
												<td></td>
												<td></td>
											</tr>
										</c:if>
									</c:if>
								</c:forEach>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<tr>
								<td colspan="7"><spring:message code="viewdefaultnotes.nonotesdefined"/></td>
							</tr>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
		</div>
    </jsp:body>
</t:crocodileTemplate>