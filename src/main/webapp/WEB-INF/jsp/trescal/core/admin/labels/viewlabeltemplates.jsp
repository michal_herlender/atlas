<%-- File name: /trescal/core/admin/labels/viewlabeltemplates.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code='adminarea.managelabels'/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        
    </jsp:attribute>
    <jsp:body>
		<div class="infobox">
			<table class="default4" id="labeltemplates" >
				<thead>
					<tr>
						<td colspan="5">
							<div class="float-left"><spring:message code='viewlabels.labeltemplates' /> (${templates.size()})</div>
						</td>
					</tr>
					<tr>
						<th><spring:message code='viewlabels.identifier'/></th>
						<th><spring:message code='admin.description'/></th>
						<th><spring:message code='viewlabels.labeltype'/></th>
						<th><spring:message code='viewlabels.parametertype'/></th>
						<th></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="5">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
					<c:forEach var="t" items="${templates}">
						<tr id="template-${t.id}">
							<td><c:out value="${t.id}" /></td>
							<td><c:out value="${t.description}" /></td>
							<td><c:out value="${t.templateType}" /></td>
							<td><c:out value="${t.parameterStyle}" /></td>
							<td><a class="mainlink" href="viewlabeltemplate.htm?id=${t.id}"><spring:message code='viewquot.details' /></a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>    	
	</jsp:body>
</t:crocodileTemplate>