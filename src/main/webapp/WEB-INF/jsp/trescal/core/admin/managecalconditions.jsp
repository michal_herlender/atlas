<%-- File name: /trescal/core/admin/managecalconditions.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<spring:message code='managecalconditions.manage'/>
			<c:out value=' - '/>
			<t:showTranslationOrDefault translations="${managecalconditions.condition.caltype.serviceType.longnameTranslation}" defaultLocale="${defaultlocale}"/>
			<c:out value=' - '/>
			<spring:message code='managecalconditions.termsandconditions'/>
		</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/admin/ManageConditions.js'></script>
	</jsp:attribute>
	<jsp:body>
		<form:form method="post" action="" name="f1" modelAttribute="managecalconditions">
			<div class="infobox">
				<fieldset>
					<legend>
						<spring:message code='managecalconditions.manage'/>
						<c:out value=' - '/>
						<t:showTranslationOrDefault translations="${managecalconditions.condition.caltype.serviceType.longnameTranslation}" defaultLocale="${defaultlocale}"/>
						<c:out value=' - '/>
						<spring:message code='managecalconditions.termsandconditions'/>
					</legend>
					<ol>
						<li>
							<label><spring:message code='businesscompany'/></label>
							<c:out value="${allocatedCompany.value}" />
						</li>
						<li>
							<label><spring:message code='company.language'/></label>
							<spring:message code='all' />
						</li>
						<li>
							<label><spring:message code='managecalconditions.viewing'/></label>
							<form:select path="calTypeId" onchange="window.location.href='managecalconditions.htm?id=' + $j(this).val();">
								<c:forEach var="caltype" items="${caltypes}">
									<form:option value="${caltype.calTypeId}">
										<t:showTranslationOrDefault translations="${caltype.serviceType.longnameTranslation}" defaultLocale="${defaultlocale}"/>
										<c:out value=' - '/>
										<spring:message code='managecalconditions.termsandconditions'/>
									</form:option>
								</c:forEach>
							</form:select>
						</li>
						<li>
							<form:label path="condition.conditionText"><spring:message code='admin.conditions'/></form:label>
							<form:textarea path="condition.conditionText" rows="10" id="conditionTextArea" class="width70" onkeyup="updatePreview($j(this).val());"/>
							<form:errors path="condition.conditionText"/>
						</li>
						<li>
							<label>&nbsp;</label>
							<input name="submit" type="submit" value="<spring:message code='admin.submit'/>"/>
						</li>
						<li>
							<label for="preview">Preview</label>
							<div id="preview">
								${managecalconditions.condition.conditionText}
							</div>
							<div class="clear"></div>
						</li>
					</ol>
				</fieldset>
			</div>
		</form:form>
	</jsp:body>
</t:crocodileTemplate>