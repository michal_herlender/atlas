<%-- File name: /trescal/core/admin/editprinter.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="editprinter.headertext" /></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/admin/EditPrinter.js'></script>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox">
			<form:form method="post" modelAttribute="form">
				<p>
					<a href="<c:url value="/viewprinters.htm" />">
						<spring:message code="editprinter.backtoviewprinters" />
					</a>				
				</p>
			
				<t:showErrors path="form.*" showFieldErrors="true" />
				<fieldset>
					<legend><spring:message code="editprinter.headertext" /></legend>								
					<ol>
						<li>
							<label for="path"><spring:message code="editprinter.path" /></label>
							<form:input size="60" path="path" />
						</li>
						<li>
							<label for="description"><spring:message code="admin.description" /></label>
							<form:input size="60" path="description" />
						</li>
						<li>
							<label><spring:message code="admin.address" /></label>
							<form:select path="addrId" items="${addresses}" itemLabel="value" itemValue="key" />
						</li>
					</ol>
				</fieldset>									
						
				<p><spring:message code="editprinter.warning" /></p>
				
				<table class="default4">
					<thead>
						<tr>
							<td colspan="5">
								<div class="float-left"><spring:message code="editprinter.printertrays" /></div>
								<div class="float-right">
									<a href="#" class="mainlink" onclick=" event.preventDefault(); addTrayRow(); "><spring:message code="editprinter.addtray" /></a>
								</div>
								<div class="clear"></div>
							</td>
						</tr>
						<tr>
							<th><spring:message code="editprinter.trayno" /></th>
							<th><spring:message code="editprinter.papertype" /></th>
							<th><spring:message code="editprinter.papersource" /></th>
							<th><spring:message code="editprinter.mediatray" /></th>
							<th><spring:message code="delete" /></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="5">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody id="printerTrays">
						<c:choose>
							<c:when test="${not empty form.trayIds}">
								<c:forEach var="trayId" items="${form.trayIds}" varStatus="loopStatus">
									<tr>
										<td>${loopStatus.count}</td>
										<td>
											<form:select path="paperTypes[${loopStatus.index}]" items="${papertypes}"></form:select>
										</td>
										<td>
											<form:select path="paperSources[${loopStatus.index}]" items="${papersources}"></form:select>
										</td>
										<td>
											<form:select path="mediaTrayRefs[${loopStatus.index}]" items="${mediatrayrefs}"></form:select>
										</td>
										<td>
											<c:choose>
												<c:when test="${loopStatus.last && (loopStatus.index > 0)}">
													<a href="<c:url value='deleteprintertray.htm?trayid=${trayId}' />">
														<spring:message code="delete" />
													</a>
												</c:when>
												<c:otherwise>
													<spring:message code="companyedit.na" />
												</c:otherwise>
											</c:choose>
											
										</td>
									</tr>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<tr id="notrays">
									<td colspan="5" align="center"><spring:message code="editprinter.notray" /></td>
								</tr>
							</c:otherwise>
						</c:choose>
					</tbody>
					
					<tbody id="rowPrototype" class="hid">
						<tr>
							<td id="tdTrayNo"></td>
							<td>
								<select id="idPaperTypes" name="namePaperTypes">
									<c:forEach var="pt" items="${papertypes}">
										<c:set var="selectedText" value="" />
										<c:if test="${pt == 'A4_PLAIN'}">
											<c:set var="selectedText" value="selected='selected'" />
										</c:if>
										<option value="${pt}" ${selectedText}>${pt}</option>
									</c:forEach>
								</select>
							</td>
							<td>
								<select id="idPaperSources" name="namePaperSources">
									<c:forEach var="ps" items="${papersources}">
										<option value="${ps}">${ps}</option>
									</c:forEach>
								</select>
							</td>
							<td>
								<select id="idMediaTrayRefs" name="nameMediaTrayRefs">
									<c:forEach var="mtr" items="${mediatrayrefs}">
										<option value="${mtr}">${mtr}</option>
									</c:forEach>
								</select>
							</td>
							<td>
								<spring:message code="companyedit.na" />
							</td>
						</tr>
					</tbody>
 
				</table>
					
				<div class="marg-top text-center">
					<input type="button" value="<spring:message code="systemdefault.reset" />" onclick="event.preventDefault(); window.location.href = '<c:url value="editprinter.htm?printerid=${form.printerId}"/>';" />
					<input type="submit" value="<spring:message code="save" />" />
				</div>								
			</form:form>
			
		</div>
	</jsp:body>
</t:crocodileTemplate>