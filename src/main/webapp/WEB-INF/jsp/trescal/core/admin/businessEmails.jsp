<%-- File name: /trescal/core/admin/businessEmails.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">Business emails</span>
	</jsp:attribute>
	<jsp:body>
		<form:form modelAttribute="businessEmails">
			<div class="infobox">
				<h4>Email addresses for ${allocatedCompany.value} - ${allocatedSubdiv.value}</h4>
				<fieldset>
					<ol>
						<li>
							<form:label path="account">Account:</form:label>
							<form:input type="email" path="account" style="width: 300px"/>
							<form:errors path="account"/>
						</li>
					</ol>
					<ol>
						<li>
							<form:label path="autoService">Auto Service:</form:label>
							<form:input type="email" path="autoService" style="width: 300px"/>
							<form:errors path="autoService"/>
						</li>
					</ol>
					<ol>
						<li>
							<form:label path="collections">Collections:</form:label>
							<form:input type="email" path="collections" style="width: 300px"/>
							<form:errors path="collections"/>
						</li>
					</ol>
					<ol>
						<li>
							<form:label path="goodsIn">Goods in:</form:label>
							<form:input type="email" path="goodsIn" style="width: 300px"/>
							<form:errors path="goodsIn"/>
						</li>
					</ol>
					<ol>
						<li>
							<form:label path="quality">Quality:</form:label>
							<form:input type="email" path="quality" style="width: 300px"/>
							<form:errors path="quality"/>
						</li>
					</ol>
					<ol>
						<li>
							<form:label path="quotations">Quotations:</form:label>
							<form:input type="email" path="quotations" style="width: 300px"/>
							<form:errors path="quotations"/>
						</li>
					</ol>
					<ol>
						<li>
							<form:label path="recall">Recall:</form:label>
							<form:input type="email" path="recall" style="width: 300px"/>
							<form:errors path="recall"/>
						</li>
					</ol>
					<ol>
						<li>
							<form:label path="sales">Sales:</form:label>
							<form:input type="email" path="sales" style="width: 300px"/>
							<form:errors path="sales"/>
						</li>
					</ol>
					<ol>
						<li>
							<form:label path="web">Web:</form:label>
							<form:input type="email" path="web" style="width: 300px"/>
							<form:errors path="web"/>
						</li>
					</ol>
				</fieldset>
				<form:button name="submit"><spring:message code="submit"/></form:button>
			</div>
		</form:form>
	</jsp:body>
</t:crocodileTemplate>