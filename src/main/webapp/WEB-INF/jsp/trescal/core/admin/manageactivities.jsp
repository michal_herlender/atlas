<%@page import="java.util.Locale"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>

<t:crocodileTemplate>

	
	<jsp:attribute name="header">
		<script>
			var itemactivitiestype = [];
			<c:forEach var="iat" items="${itemActivityType}">
				itemactivitiestype.push('${iat}');
			</c:forEach>
		</script>
		<script src='script/trescal/core/admin/ManageActivities.js'></script>
		<span class="headtext"><spring:message	code="manageactivities.pagetitle" />
		</span>
	</jsp:attribute>
	<jsp:body>

		<div id="adminmanageactivities" class="infobox">
			<form:form method="POST">
				<fieldset>
				<form:form method="post" id="admin_manage_act_form">
					<ol>
						
						<li>
							<table id="table_manageactivities" class="default2"
									summary="This table lists all activities">	
								<thead>
									<tr>
										<th class="codefault"><spring:message code="manageactivities.table.englishname" /></th>
										<th class="codefault"><spring:message code="manageactivities.table.spanishname" /></th>
										<th class="codefault"><spring:message code="manageactivities.table.frenchname" /></th>
										<th class="codefault"><spring:message code="manageactivities.table.typeofactivity" /></th>
										<th class="codefault"><spring:message code="manageactivities.table.istransferredtoadveso" /></th>
										<th class="codefault"><spring:message code="manageactivities.table.isdisplayedonjoborderdetails" /></th>
										<th class="codefault"><spring:message code="manageactivities.table.action" /></th>
									</tr>
								</thead>
								<tbody>
									    <c:forEach items="${ activities }" var="act">
										    <c:if test="${ act.itemActivity.active == true }">
											    <c:set var="localeEnglish" value="<%=new Locale(\"en\", \"GB\")%>"/>
												<c:set var="localeSpanish" value="<%=new Locale(\"es\", \"ES\")%>"/>
												<c:set var="localeFrench" value="<%=new Locale(\"fr\", \"FR\")%>"/>
												    
											    <c:set var="localeEnglishTrans" value="${ act.itemActivity.translations.stream().filter(t -> t.getLocale().equals(localeEnglish)).findFirst().orElse(null) }"/>
											    <c:set var="localeSpanishTrans" value="${ act.itemActivity.translations.stream().filter(t -> t.getLocale().equals(localeSpanish)).findFirst().orElse(null) }"/>
											    <c:set var="localeFrenchTrans" value="${ act.itemActivity.translations.stream().filter(t -> t.getLocale().equals(localeFrench)).findFirst().orElse(null) }"/>
										    	<tr>
													<td>${ localeEnglishTrans.translation }</td>
													<td>${ localeSpanishTrans.translation }</td>
													<td>${ localeFrenchTrans.translation }</td>
													<td>${ act.itemActivity.type }</td>
													<input type="hidden" name="id" value="${ act.id }"/>
													<td align="center"><input type="checkbox" name="istrans_${ act.id }" ${ act.isTransferredToAdveso ? 'checked':'' }/></td>
													<td align="center"><input type="checkbox" name="isdisp_${ act.id }" ${ act.isDisplayedOnJobOrderDetails ? 'checked':'' }/></td>
													<td align="center">
														<a href="#" onclick=" event.preventDefault(); deleteActivity(${act.id});"><spring:message code="manageactivities.buttondelete" /></a>
													</td>
												</tr>
											</c:if>
									</c:forEach>
								</tbody>
								<tfoot>
									<tr>
										<td colspan="7">
											<a href="#" class="domthickbox mainlink-float" onclick=" event.preventDefault(); addNewActivity('<spring:message code="manageactivities.prompt.type" />','<spring:message code="manageactivities.prompt.activity" />','<spring:message code="manageactivities.prompt.istransferable" />','<spring:message code="manageactivities.prompt.isdisplayed" />');" title="">
												<spring:message code="manageactivities.buttonadd" /></a>
										</td>
									</tr>
								</tfoot>
							</table>
						</li>
						
						<li>
							<div class="displaycolumn-15">
								<button onclick="sendData()">
									<spring:message code='manageactivities.buttonsave'/> 
								</button>
							</div>
						</li>
					</ol>	
					</form:form>						
				</fieldset>

   </form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>