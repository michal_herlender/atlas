<%-- File name: /trescal/core/admin/upgrade.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">Perform Upgrade</span>
	</jsp:attribute>
	<jsp:body>
		Upgrade Successfully Performed: <spring:message code="${form.upgradePerformed}"/><br/><br/>
	
		Upgrade Result: ${form.upgradeResult}<br/><br/>
	
		<form:form modelAttribute="form" method="post">
			Upgrade Type: <form:select path="action" items="${actions}"/><br/><br/>
			Offset: <form:input path="offset" /><br/><br/>
			Batch size: <form:input path="batchSize" /><br/><br/>
			<input type="submit" value="Perform Upgrade"/>
		</form:form>
	</jsp:body>
</t:crocodileTemplate>