<%-- File name: /trescal/core/admin/edituserpreferences.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<spring:message code="edituserpreferences.edituser" />
			<c:out value=" ${contact.name}" />
		</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/admin/EditUserPreferences.js'></script>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox">
			<c:choose>
				<c:when test="${editAuthorized}">
					<form:form action="" method="post" modelAttribute="form">
						<fieldset id="userPreferences">
							<ol>									
								<li>
									<label><spring:message code="edituserpreferences.user"/></label>
									<strong>${contact.name}</strong>
								</li>
								<li>
									<label><spring:message code="edituserpreferences.jobitemkey"/></label>
									<form:checkbox path="jiKeyNavigation"/>
								</li>
								<li>
									<label><spring:message code="edituserpreferences.mouseoveractive"/></label>
									<form:checkbox path="mouseoverActive"/>
								</li>
								<li>
									<label><spring:message code="edituserpreferences.mouseoverdelay"/></label>
									<form:input size="2" path="mouseoverDelay" /><c:out value=" ms" />
								</li>
								<li>
									<label><spring:message code="edituserpreferences.includeself"/></label>
									<form:checkbox path="includeSelfInEmailReplies"/>
								</li>
								<li>
									<label><spring:message code="edituserpreferences.navbar"/></label>
									<form:radiobutton value="true" path="navBarPosition"/>
									<spring:message code="edituserpreferences.top"/>
									<form:radiobutton value="false" path="navBarPosition"/>
									<spring:message code="edituserpreferences.side"/>
								</li>											
								<li>
									<label><spring:message code="edituserpreferences.labelprinter"/></label>
									<form:select path="labelPrinterId" itemLabel="value" itemValue="key" items="${labelPrinters}"/>
								</li>
								<li>
									<label><spring:message code="edituserpreferences.defaultjobtype"/></label>
									<form:select path="jobType" items="${jobTypes}"/>
								</li>
								<!-- -- #springMessage("edituserpreferences.nonespecified") -- -->
								<li>
									<label><spring:message code="viewuserpreferences.servicetypeusage"/></label>
									<form:radiobutton value="true" path="defaultServiceTypeUsage" />
									<spring:message code="viewuserpreferences.servicetypefromjobdefault"/>
									<form:radiobutton value="false" path="defaultServiceTypeUsage" />
									<spring:message code="viewuserpreferences.servicetypefromjobtype"/>
								</li>
								<li>
									<label><spring:message code="edituserpreferences.autoprintlabel"/></label>
									<form:checkbox path="autoPrintLabel"/>
								</li>
								<li>
									<label><spring:message code="edituserpreferences.alligatorprofile"/></label>
									<form:radiobutton value="false" path="alligatorSettingsOnContact" onclick=" hideAlligatorSettings(); " />
									<spring:message code="edituserpreferences.usesubdivisiondefault"/>
									<form:radiobutton value="true" path="alligatorSettingsOnContact" onclick=" showAlligatorSettings(); " />
									<spring:message code="edituserpreferences.overrideoncontact"/>
								</li>
								<c:choose>
									<c:when test="${form.alligatorSettingsOnContact}">
										<c:set var="ascVisibility" value="vis" />
									</c:when>
									<c:otherwise>
										<c:set var="ascVisibility" value="hid" />
									</c:otherwise>
								</c:choose>
								<li id="alligatorProduction" class="${ascVisibility}">
									<label><spring:message code="edituserpreferences.productionprofile"/></label>
									<form:select itemLabel="value" itemValue="key" items="${alligatorSettings}" path="alligatorSettingsProductionId"/>
								</li>
								<li id="alligatorTest" class="${ascVisibility}">
									<label><spring:message code="edituserpreferences.testprofile"/></label>
									<form:select itemLabel="value" itemValue="key" items="${alligatorSettings}" path="alligatorSettingsTestId"/>
								</li>
								<li>
									<label>&nbsp;</label>
									<input type="submit" value="<spring:message code='admin.submit'/> "/>
								</li>
							</ol>
						</fieldset>
					</form:form>
				</c:when>
				<c:otherwise>
					<spring:message code="edituserpreferences.nopermission" />.
				</c:otherwise>
			</c:choose>
		</div>
	</jsp:body>
</t:crocodileTemplate>