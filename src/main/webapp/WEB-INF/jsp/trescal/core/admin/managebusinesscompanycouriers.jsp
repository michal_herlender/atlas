<%@page import="java.util.Locale"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code='adminarea.managebusinesscompanycouriers'/></span>
    </jsp:attribute>
 
	<jsp:attribute name="scriptPart">	
		<script src="script/trescal/core/admin/Managebusinesscompanycouriers.js"></script>
		<script>
		var coid = ${allocatedCompany.key};
		var strings = new Array();
		strings['removecourier.confirmationmessage'] = "<spring:message code='removecourier.confirmationmessage' javaScriptEscape='true' />";
		strings['courierdespatchtype.remove.confirmationmessage'] = "<spring:message code='courierdespatchtype.remove.confirmationmessage' javaScriptEscape='true' />";
		strings['repairinspection.validator.mandatory'] = "<spring:message code='repairinspection.validator.mandatory' javaScriptEscape='true' />";
		</script>
	</jsp:attribute>   

	<jsp:body>
	<fieldset>
			<legend>
				<spring:message code="adminarea.managebusinesscompanycouriers"/>&nbsp;
				<a class="mainlink" href="viewcomp.htm?coid=${allocatedCompany.key}">${allocatedCompany.value}</a> 
			</legend>
			<div class="successBox1 hid" id="successMessage">
		<div class="successBox2">
			<div class="successBox3">
				<div class="center green bold">
					<spring:message code='repairinspection.success1' />
				</div>
			</div>
		</div>
	</div>
<table class="default2" id="courierTable">
	<thead>
		<tr>
			<td colspan="4">
				<spring:message code="cdsearch.lbl2" /> (<span id="courierCount">${couriers.size()}</span>)
				<a href="#" class="mainlink float-right" onclick="event.preventDefault(); addNewCourierContent();">
					<spring:message code='viewcouriers.addcourier' />
				</a>
			</td>
		</tr>
		<tr>
			<th><spring:message code="viewbatchcalibration.courdespattype" /></th>
			<th class="defaultCourier" scope="col"><spring:message code="viewcouriers.default" /></th>
			<th class="courierName" scope="col"><spring:message code="addcourier.couriername" /></th>
			<th class="action" scope="col"><spring:message code="viewquot.actions" /></th>
		</tr>
	</thead>
	<tbody>
		<c:choose>
			<c:when test="${empty couriers}">
				<tr class="center bold">
					<td colspan="4"><spring:message code="showfilesforsc.empty"/></td>
				</tr>
			</c:when>
			<c:otherwise>
				<c:forEach var="courier" items="${couriers}">
					<tr id="courier${courier.courierid}">
						<td>
							<a href="#"><img src="img/icons/add.png" id="showButton_${courier.courierid}" class="show" width="16" height="16" id="defaultCourier" alt="" title="" onclick=" showCourierDetails(${courier.courierid}, this); return false; " /></a>
							<a href="#"><img src="img/icons/delete.png" id="hidButton_${courier.courierid}" class="hid" width="16" height="16" id="defaultCourier" alt="" title="" onclick=" hidCourierDetails(${courier.courierid}, this); return false; " /></a>
						</td>
						<td>
							<a href="#" id="defaultButton_${courier.courierid}" class="${courier.defaultCourier?'show':'hid'}"><img src="img/icons/courier_default.png" width="16" height="16" id="defaultCourier" alt="" title="" /></a>
							<a href="#" id="nonDefaultButton_${courier.courierid}" class="${!courier.defaultCourier?'show':'hid'}"><img src="img/icons/courier.png" width="16" height="16" id="courier" alt="" title="" onclick=" updateCourierDefault(${allocatedCompany.key}, ${courier.courierid}, this); return false; " /></a>
						</td>
						<td>
							<span id="courierLabel_${courier.courierid}" class="show">${courier.name}</span>
							<input type="text" id="courierText_${courier.courierid}" class="hid" value="${courier.name}"/>
							<input type="hidden" id="courierWebTrackURL_${courier.courierid}" value="${courier.webTrackURL}"/>
						</td>
						<td class="center">
							<a href="#" id="editButton_${courier.courierid}" class="pad-left-small" onclick="editCourier(${courier.courierid}, this); return false; "><img src="img/icons/edit_details.png" width="16" height="16" alt="" title="" /></a>
							<a href="#" id="deleteButton_${courier.courierid}" class="pad-left-small" onclick="deleteCourier(${courier.courierid}, this); return false; "><img src="img/icons/delete.png" width="16" height="16" alt="" title="" /></a>
						</td>
					</tr>
					<tr id="courierDetails_${courier.courierid}" class="hid">
						<td colspan="4">
							<table class="default2" id="courierDespTable_${courier.courierid}">
								<thead>
									<tr>
										<td colspan="3">
											<span class="float-left"><spring:message code="viewbatchcalibration.courdespattype" />(<span id="courierDespCount_${courier.courierid}">${courier.cdts.size()}</span>)</span>
											<span class="float-right width40">
												
												<button id="newCourierDespCancelButton_${courier.courierid}" class="hid float-right" onclick="cancelNewCourierDesp(${courier.courierid});"><spring:message code='cancel' /></button>
												<button id="newCourierDespAddButton_${courier.courierid}" class="hid float-right" onclick="addNewCourierDesp($j('#newCourierDespDesc_${courier.courierid}').val(),${courier.courierid});"><spring:message code='add' /></button>
												<span id="newCourierDespDescErrorMsg_${courier.courierid}" class="error hid float-right"><spring:message code='repairinspection.validator.mandatory' javaScriptEscape='true' /></span>
												<input type="text" id="newCourierDespDesc_${courier.courierid}" class="hid width50 float-right" value=""/>
												<span id="newCourierDespDescLabel_${courier.courierid}"  class="hid float-right"><spring:message code="description" /> :</span>
												<a href="#" id="addNewCourierDespatchTypeContentLink_${courier.courierid}" class="mainlink float-right" onclick="event.preventDefault(); addNewCourierDespatchTypeContent(${courier.courierid});">
													<spring:message code='viewcouriers.adddespatchtype' />
												</a>
											</span>
										</td>
									</tr>
									<tr>
										<th class="defaultCourier" scope="col"><spring:message code="company.default" /></th>
										<th class="courierName" scope="col"><spring:message code="description" /></th>
										<th class="action" scope="col"><spring:message code="viewquot.actions" /></th>
									</tr>
								</thead>
								<tbody>
								<c:choose>
										<c:when test="${empty courier.cdts}">
											<tr class="center bold">
												<td colspan="3"><spring:message code="showfilesforsc.empty"/></td>
											</tr>
										</c:when>
										<c:otherwise>
											<c:forEach var="cdt" items="${courier.cdts}">
												<tr id="cdt${cdt.cdtid}">
													<td>
														<a href="#" id="defaultCDButton_${courier.courierid}_${cdt.cdtid}" class="${cdt.defaultForCourier?'show':'hid'}"><img src="img/icons/default_box.png" width="16" height="16" alt="" title="" /></a>
														<a href="#" id="nonDefaultCDButton_${courier.courierid}_${cdt.cdtid}" class="${!cdt.defaultForCourier?'show':'hid'}"><img src="img/icons/box.png" width="16" height="16" id="courier" alt="" title="" onclick=" updateCourierDespDefault(${courier.courierid}, ${cdt.cdtid}, this); return false; " /></a>
													</td>
													<td>
														<span id="courierDespLabel_${courier.courierid}_${cdt.cdtid}" class="show">${cdt.description}</span>
														<input type="text" id="courierDespText_${courier.courierid}_${cdt.cdtid}" class="hid width60" value="${cdt.description}"/>
													</td>
													<td class="center">
														<a href="#" id="editCDButton_${courier.courierid}_${cdt.cdtid}" class="pad-left-small" onclick="editCourierDesp(${courier.courierid}, ${cdt.cdtid}, this); return false; "><img src="img/icons/edit_details.png" width="16" height="16" alt="" title="" /></a>
														<a href="#" id="deleteCDButton_${courier.courierid}_${cdt.cdtid}" class="pad-left-small" onclick="deleteCourierDesp(${courier.courierid}, ${cdt.cdtid}, this); return false; "><img src="img/icons/delete.png" width="16" height="16" alt="" title="" /></a>
														<a href="#" id="saveCDButton_${courier.courierid}_${cdt.cdtid}" class="hid pad-left-small" onclick="saveCourierDesp(${courier.courierid}, ${cdt.cdtid}, this); return false; "><img src="img/icons/greentick.png" width="16" height="16" alt="" title="" /></a>
														<a href="#" id="cancelCDButton_${courier.courierid}_${cdt.cdtid}" class="hid pad-left-small" onclick="cancelCourierDesp(${courier.courierid}, ${cdt.cdtid}, this); return false; "><img src="img/icons/cancel.png" width="16" height="16" alt="" title="" /></a>
													</td>
												</tr>
												</c:forEach>
												</c:otherwise>
												</c:choose>
								</tbody>
							</table>
						</td>
					</tr>
				</c:forEach>
			</c:otherwise>
		</c:choose>
	</tbody>
</table>
</fieldset>
</jsp:body>
</t:crocodileTemplate>