 File name: /trescal/core/admin/viewmarkup.jsp
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message
				code='viewmarkup.headertext' /></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/admin/ViewMarkups.js'></script>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox">
			<c:forEach var="markupType" items="${markupTypes}">
				<fieldset>
				   <legend>${markupType.name}: ${markupType.description}</legend>
					<ol>
						<li>
					         <label>&nbsp;</label>
			                 <div class="float-left padtop width30">
								<span class="bold"><spring:message
									code='viewmarkup.range' /></span>
							</div>
							<div class="float-left padtop width30">
								<span class="bold"><spring:message
									code='viewmarkup.value' /></span> 
							</div>
							<div class="float-left padtop">
						    	<span class="bold"></span>
						    </div>
						    <div class="clear"></div>
					    </li>
				    	    <c:set var="count" value="1" />
				    	    <c:set var="rangeList"
						value="${markupRanges.get(markupType.id)}" />
							
						 <c:forEach var="range" varStatus="status"
						items="${markupRanges.get(markupType.id)}">
						     <li>
								<label><spring:message code='viewmarkup.range' /> ${status.count}:</label>
		       						<div class="float-left padtop width30">
									<c:out value="${currency.currencySymbol} ${range.rangeStart}"/>
									<c:choose>	
							            <c:when test="${status.count == rangeList.size()}"> + </c:when>
								            <c:otherwise> 
								            
							               <c:set var="count" value="${status.count}" />
							               <c:set var="nextStart"
											value="${rangeList.get(status.count)}" />							   
		 						               <c:set var="nextStart" value="${nextStart.rangeStart - 0.01}" />   
																		
		 										-  <c:out value="${currency.currencySymbol} ${nextStart}"/>  
		 									</c:otherwise>  
									    </c:choose> 
							   
								</div>
									<div class="float-left padtop width30"> 
										<c:choose>	 
		  									<c:when test="${range.action == 'PERCENTAGE'}">   
		  										<c:out value="${range.actionValue} %"/>  
		  								    </c:when>  
											<c:otherwise>   
		 										<c:out value="${currency.currencySymbol} ${range.actionValue}"/>   
		 									 </c:otherwise>  
		  								</c:choose>  
										</div>  
										<div class="float-left padtop">
										<!-- There's no way to add/edit, so removing deletion until multi-company refactoring is done - GB 2019-05-14 -->  
		  								<%--
		  									<a href="deletemarkuprange.htm?id=${range.id}"
											class="mainlink"><spring:message code='viewmarkup.remove' /></a>
										--%>   
										</div> 
									<div class="clear"></div> 
							</li>
							<c:set var="count" value="${count+1}"/>
					    </c:forEach>
					</ol>					
				</fieldset>			
			</c:forEach>			
		</div>   
    </jsp:body>
</t:crocodileTemplate>