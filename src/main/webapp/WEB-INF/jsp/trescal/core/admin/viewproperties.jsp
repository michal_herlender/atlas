<%-- File name: /trescal/core/admin/viewproperties.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">Configuration Check</span>
		<style>
			.word-wrap {
			  overflow-wrap: anywhere;
			  word-wrap: anywhere;
  			}
		</style>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox">
			<fieldset>
				<legend>Selected Configuration Properties</legend>
				<ol>
					<c:forEach var="entry" items="${atlasProperties.entrySet()}">
						<li>
							<label class="width30"><c:out value="${entry.key}"/></label>
							<span><c:out value="${entry.value}"/></span>
						</li>
					</c:forEach>
				</ol>
			</fieldset>
			
			<fieldset>
				<legend>Other Values</legend>
				<ol>
					<c:forEach var="entry" items="${otherValues.entrySet()}">
						<li>
							<label class="width30"><c:out value="${entry.key}"/></label>
							<div class="width70 float-left word-wrap"><c:out value="${entry.value}"/></div>
						</li>
					</c:forEach>
				</ol>
			</fieldset>
			
			<fieldset>
				<legend>File Server Tests</legend>
				<ol>
					<c:forEach var="entry" items="${fileServerTests.entrySet()}">
						<li>
							<label class="width30"><c:out value="${entry.key}"/></label>
							<span><c:out value="${entry.value}"/></span>
						</li>
					</c:forEach>
				</ol>
			</fieldset>
			<fieldset>
				<legend>Special Tests</legend>
				<ol>
					<li>
						<label class="width30">Test TML Sync Connection</label>
						<form:form>
							<input type="submit">Perform Test
						</form:form>
					</li>
					<li>
						<label class="width30">Test Result</label>
						<span><c:out value="${testResult}"/></span>
					</li>
				</ol>
			</fieldset>
		</div>
	</jsp:body>
</t:crocodileTemplate>