<%-- File name: /trescal/core/timekeeping/timesheet.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="menu.timesheet" /></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
<style>
table.default2 th.colTimeOnJobItems {
	text-align: right;
	border-right-style: hidden;
	border-left-style: hidden;
}

td.colJobItem, th.colJobItem {
	width: 15%;
}

td.colActivity, th.colActivity {
	width: 25%;
}

td.colRemark, th.colRemark {
	width: 40%;
}

td.colStartTime, th.colStartTime {
	width: 10%;
}

table.default2 td.colTimeSpent, table.default2 th.colTimeSpent {
	text-align: right;
	width: 10%;
}
</style>
		<script type="text/javascript">
			var menuElements = [];
			<c:forEach var="weekDay" items="${timeSheetMap.keySet()}">
				menuElements.push({ anchor: 'select-${weekDay}', block: 'timesheet-${weekDay}' });
			</c:forEach>
		</script>
		<script src='script/trescal/core/timekeeping/timesheet.js'></script>
	</jsp:attribute>
	<jsp:body>
	<div class="infobox">
		<div class="center">
		<form:form modelAttribute="searchForm">
			<form:input path="date" type="date" />
			<input type="submit" value="<spring:message code='submit'/>" />
		</form:form>
		</div>
	</div>
	<div class="infobox">
		<div id="tabmenu">
			<ul class="subnavtab">
				<c:forEach var="entry" items="${timeSheetMap.entrySet()}">
					<li>
						<a href="" id="select-${entry.key}"
							onclick="event.preventDefault(); switchMenuFocus(menuElements, 'timesheet-${entry.key}', false);"
							<c:if test="${entry.key == dayOfWeek}">class="selected"</c:if>>
							${entry.value.displayName}
						</a>
					</li>
				</c:forEach>
			</ul>
			<div class="tab-box">
				<c:forEach var="entry" items="${timeSheetMap.entrySet()}">
					<c:set var="timeSheet" value="${entry.value}" />
					<div id="timesheet-${entry.key}"
							class=<c:choose><c:when test="${timeSheet.date.getDayOfWeek() == dayOfWeek}">"vis"</c:when><c:otherwise>"hid"</c:otherwise></c:choose>>
						<table class="default2">
						<thead>
							<tr>
								<th colspan="2">
									<span class="larger-text"><spring:message
													code="timeactivity.timerelated" /></span>
									<img id="unhideTimeRelatedImg" alt="Show"
											src="img/icons/viewactions.png"
											onclick="unhideTimeRelated();">
									<img id="hideTimeRelatedImg" class="hid" alt="Show"
											src="img/icons/hideactions.png" onclick="hideTimeRelated();">
								</th>
								<th colspan="2" class="colTimeOnJobItems"><spring:message
												code="timeactivity.timespent" />:</th>
								<th class="colTimeSpent">
									<cwms:formatDuration
												value="${timeSheet.timeSpentOnTimeRelatedActions}" />
								</th>
							</tr>
							<c:if test="${not empty timeSheet.timeRelatedActions}">
							<tr id="timeRelatedHeadRow" class="hid">
								<th class="colJobItem"><spring:message
													code="workflow.jobitem" /></th>
								<th class="colActivity"><spring:message
													code="jiactions.activity" /></th>
								<th class="colRemark"><spring:message
													code="workflow.remark" /></th>
								<th class="colStartTime"><spring:message
													code="calsearchresults.starttime" /></th>
								<th class="colTimeSpent"><spring:message
													code="timeactivity.timespent" /></th>
							</tr>
							</c:if>
						</thead>
						<tbody id="timeRelatedBody" class="hid">
							<c:forEach var="jia" items="${timeSheet.timeRelatedActions}"
										varStatus="loopStatus">
							<tr
											class=<c:choose><c:when test="${loopStatus.index%2==0}">"even"</c:when><c:otherwise>"odd"</c:otherwise></c:choose>>
								<td>
									<a href="viewjob.htm?jobid=${jia.jobId}" class="mainlink"
												target="_blank">${jia.jobNo}</a> /
									<links:jobitemLinkInfo jobItemId="${jia.jobItemId}"
													defaultPage="jiactions.htm" jobItemNo="${jia.itemNo}" />
								</td>
								<td>${jia.activityDescription}</td>
								<td>${jia.remark}</td>
								<td><fmt:formatDate pattern="HH:mm"
													value="${jia.startStamp}" /></td>
								<td class="colTimeSpent"><cwms:formatDuration
													value="${jia.getDuration()}" /></td>
							</tr>
							</c:forEach>
						</tbody>
						</table>
						<table class="default2">
						<thead>
							<tr>
								<th colspan="2">
									<span class="larger-text"><spring:message
													code="timeactivity.nottimerelated" /></span>
									<img id="unhideNotTimeRelatedImg" alt="Show"
											src="img/icons/viewactions.png"
											onclick="unhideNotTimeRelated();">
									<img id="hideNotTimeRelatedImg" class="hid" alt="Show"
											src="img/icons/hideactions.png"
											onclick="hideNotTimeRelated();">
								</th>
								<th colspan="2" class="colTimeOnJobItems"><spring:message
												code="jiactions.timespent" />:</th>
								<th class="colTimeSpent">
									<cwms:formatDuration
												value="${timeSheet.timeSpentOnNotTimeRelatedActions}" />
								</th>
							</tr>
							<c:if test="${not empty timeSheet.notTimeRelatedActions}">
							<tr id="notTimeRelatedHeadRow" class="hid">
								<th class="colJobItem"><spring:message
													code="workflow.jobitem" /></th>
								<th class="colActivity"><spring:message
													code="jiactions.activity" /></th>
								<th class="colRemark"><spring:message
													code="workflow.remark" /></th>
								<th class="colStartTime"><spring:message
													code="calsearchresults.starttime" /></th>
								<th class="colTimeSpent"><spring:message
													code="jiactions.timespent" /></th>
							</tr>
							</c:if>
						</thead>
						<tbody id="notTimeRelatedBody" class="hid">
							<c:forEach var="jia" items="${timeSheet.notTimeRelatedActions}"
										varStatus="loopStatus">
							<tr
											class=<c:choose><c:when test="${loopStatus.index%2==0}">"even"</c:when><c:otherwise>"odd"</c:otherwise></c:choose>>
								<td>
									<a href="viewjob.htm?jobid=${jia.jobId}" class="mainlink"
												target="_blank">${jia.jobNo}</a> /
									<links:jobitemLinkInfo jobItemId="${jia.jobItemId}"
													defaultPage="jiactions.htm" jobItemNo="${jia.itemNo}" />
								</td>
								<td>${jia.activityDescription}</td>
								<td>${jia.remark}</td>
								<td><fmt:formatDate pattern="HH:mm"
													value="${jia.startStamp}" /></td>
								<td class="colTimeSpent"><cwms:formatDuration
													value="${jia.getDuration()}" /></td>
							</tr>
							</c:forEach>
						</tbody>
						</table>
						<table class="default2">
							<thead>
								<tr>
									<th colspan="7">
										<span class="larger-text"><spring:message
													code="timeactivity.recordedactivities" /></span>
									</th>
								</tr>
								<tr>
									<th><spring:message code="job" /></th>
									<th><spring:message code="subdivision" /></th>
									<th><spring:message code="timeactivity.timeactivity" /></th>
									<th><spring:message code="timesheetentry.expenses" /></th>
									<th><spring:message code="comment" /></th>
									<th class="colStartTime"><spring:message
												code="timeactivity.starttime" /></th>
									<th class="colTimeSpent"><spring:message
												code="timeactivity.timespent" /></th>
								</tr>
							</thead>
							<tbody>
								<c:choose>
									<c:when test="${not empty timeSheet.entries}">
										<c:forEach var="entry" items="${timeSheet.entries}">
											<tr
													onclick="window.location.href='timesheetentry.htm?entryid=${entry.id}'">
												<td><c:if test="${entry.job != null}">${entry.job.jobno}</c:if></td>
												<td><c:if test="${entry.subdiv != null}">${entry.subdiv.subname} (${entry.subdiv.comp.coname})</c:if></td>
												<td><spring:message
															code="${entry.timeActivity.messageCode}" /></td>
												<td><fmt:formatNumber type="currency"
															currencyCode="${entry.expenseCurrency.currencyCode}"
															value="${entry.expenseValue}" /></td>
												<td>${entry.comment}</td>
												<td><fmt:formatDate value="${entry.start}"
															pattern="HH:mm" /></td>
												<td class="colTimeSpent"><cwms:formatDuration
															value="${entry.duration}" /></td>
											</tr>
										</c:forEach>
									</c:when>
									<c:otherwise>
										<tr>
											<td colspan="7">
												<spring:message code="timeactivity.noworkingtime" />
											</td>
										</tr>
									</c:otherwise>
								</c:choose>
							</tbody>
							<tfoot>
								<c:if test="${!closed}">
								<tr class="createentry">
									<td colspan="7">
										<a href="timesheetentry.htm?date=${timeSheet.date}"
												class="mainlink">
											<spring:message code="timeactivity.createnewentry" />
										</a>
									</td>
								</tr>
								</c:if>
							</tfoot>
						</table>
						<table class="default2">
							<thead>
								<tr>
									<th colspan="2"><span class="larger-text"><spring:message
													code="timeactivity.dailystatistics" /></span></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><spring:message code="timesheet.workingtime" /></td>
									<td><cwms:formatDuration value="${timeSheet.workingTime}" /></td>
								</tr>
								<tr>
									<td><spring:message code="timeactivity.registeredtime" /></td>
									<td><cwms:formatDuration value="${timeSheet.timeSpent}" /></td>
								</tr>
								<tr>
									<td><spring:message code="timeactivity.overtime" /></td>
									<td><cwms:formatDuration
												value="${timeSheet.getTimeDifference()}" /></td>
								</tr>
							</tbody>
						</table>
					</div>
				</c:forEach>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="infobox">
		<table class="default2">
			<thead>
				<tr>
					<th><span class="larger-text">
						<spring:message code="timesheet.statistic" />
					</span></th>
					<th><spring:message code="timesheet.week"
								arguments="${searchForm.getWeek()},${searchForm.getYear()}" /></th>
					<th><spring:message code="timesheet.month" /></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><spring:message code="timesheet.workingtime" /></td>
					<td><cwms:formatDuration
								value="${weeklyStatistic.workingTime}" /></td>
					<td><cwms:formatDuration
								value="${monthlyStatistic.workingTime}" /></td>
				</tr>
				<tr>
					<td><spring:message code="timeactivity.registeredtime" /></td>
					<td><cwms:formatDuration
								value="${weeklyStatistic.registeredTime}" /></td>
					<td><cwms:formatDuration
								value="${monthlyStatistic.registeredTime}" /></td>
				</tr>
				<tr>
					<td><spring:message code="timeactivity.overtime" /></td>
					<td><cwms:formatDuration value="${weeklyStatistic.overtime}" /></td>
					<td><cwms:formatDuration value="${monthlyStatistic.overtime}" /></td>
				</tr>
			</tbody>
		</table>
		<c:if test="${!closed}">
			<button id="validateButton"
					onclick="event.preventDefault(); createValidation('${searchForm.date}');">
					<spring:message code="timeactivity.createTSforvalidation" />
				</button>
		</c:if>
	</div>
	</jsp:body>
</t:crocodileTemplate>