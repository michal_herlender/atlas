<%-- File name: /trescal/core/timekeeping/timesheetentry.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="menu.workingtime"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script>
		function init() {
			$j("#search_jobNo").autocomplete({
				source: function(request, response) {
					$j.ajax({
						url: "searchjobs.json",
						dataType: "json",
			            data: {
			            	partialJobNo: request.term
			            },
			            success: function(data) {
			            	response( data );
			            }
					})},
				minLength: 3,
				select: function(event, jkv) {
					$j("#hidden_jobId").val(jkv.item.key);
					$j("#search_jobNo").val(jkv.item.value);
				}
			})
			.autocomplete("instance")._renderItem = function(ul, jkv) {
				return $j("<li>").append(jkv.value).appendTo(ul);
			};
			$j("#search_subdiv").autocomplete({
				source: function(request, response) {
					$j.ajax({
						url: "searchBusinessSubdivs.json",
						dataType: "json",
			            data: {
			            	partialSubdivName: request.term
			            },
			            success: function(data) {
			            	response( data );
			            }
					})},
				minLength: 2,
				select: function(event, skv) {
					$j("#hidden_subdivId").val(skv.item.key);
					$j("#search_subdivName").val(skv.item.value);
				}
			})
			.autocomplete("instance")._renderItem = function(ul, skv) {
				return $j("<li>").append(skv.value).appendTo(ul);
			};
		}
		</script>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox">
			<form:form modelAttribute="entryForm">
				<fieldset>
					<form:hidden path="id"/>
					<ol>
						<li>
							<form:hidden path="startDate"/>
							<form:label path="startTime"><spring:message code="timesheetentry.starttime"/></form:label>
							<form:input path="startTime" type="time"/>
						</li>
						<li>
							<form:label path="hoursSpent"><spring:message code="timesheetentry.timespent"/></form:label>
							<form:input path="hoursSpent" type="number" min="0"/> hours
							<form:input path="minutesSpent" type="number" min="0" max="59"/> min
						</li>
						<li>
							<form:label path="timeActivity"><spring:message code="timeactivity.timeactivity"/></form:label>
							<form:select path="timeActivity">
								<c:forEach var="timeActivity" items="${timeActivities}">
									<spring:message var="timeActivityTranslation" code="${timeActivity.messageCode}"/>
									<form:option value="${timeActivity}" label="${timeActivityTranslation}"/>
								</c:forEach>
							</form:select>
						</li>
						<li>
							<form:label path="expenseValue"><spring:message code="timesheetentry.expenses" /></form:label>
							<form:input path="expenseValue" type="number" min="0" step="0.01"/>
							<form:select path="expenseCurrencyId" items="${currencies}" itemLabel="currencyCode" itemValue="currencyId"/>
						</li>
						<li>
							<form:label path="jobId"><spring:message code="job"/></form:label>
							<form:hidden id="hidden_jobId" path="jobId"/>
							<input id="search_jobNo" value="${entryForm.jobNo}">
						</li>
						<li>
							<form:label path="subdivId"><spring:message code="subdivision"/></form:label>
							<form:hidden id="hidden_subdivId" path="subdivId"/>
							<input id="search_subdiv" value="${entryForm.subdivName}">
						</li>
						<li>
							<form:label path="comment"><spring:message code="comment"/></form:label>
							<form:textarea path="comment"/>
						</li>
						<li>
							<input type="submit" value="<spring:message code='submit'/>"/>
						</li>
					</ol>
				</fieldset>
			</form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>