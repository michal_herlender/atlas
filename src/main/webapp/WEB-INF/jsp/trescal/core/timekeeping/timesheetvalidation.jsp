<%-- File name: /trescal/core/timekeeping/timesheetvalidation.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message
				code="menu.timesheetvalidation" /></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type="text/javascript">
			var menuElements = [];
			menuElements.push({
				anchor : 'awaitingvalidation-select',
				block : 'awaitingvalidation-tab'
			});
			menuElements.push({
				anchor : 'overviewbyweek-select',
				block : 'overviewbyweek-tab'
			});
		</script>
	</jsp:attribute>
	<jsp:body>
	<div class="infobox">
		<div class="tabmenu">
			<ul class="subnavtab">
				<li><a href="" id="awaitingvalidation-select"
						onclick="event.preventDefault(); switchMenuFocus(menuElements, 'awaitingvalidation-tab', false);"
						class="${validationForm.submitted ? '' : 'selected'}">
						<spring:message code="timesheetvalidation.awaitingvalidation" />
				</a></li>
				<li><a href="" id="overviewbyweek-select"
						onclick="event.preventDefault(); switchMenuFocus(menuElements, 'overviewbyweek-tab', false);"
						class="${validationForm.submitted ? 'selected' : ''}">
						<spring:message code="timesheetvalidation.overviewbyweek" />
				</a></li>
			</ul>
		</div>
		<div class="tab-box">
			<div id="awaitingvalidation-tab" class="${validationForm.submitted ? 'hid' : 'vis'}">
				<table class="default2">
					<tbody>
						<c:choose>
							<c:when test="${empty readyForValidation.keySet()}">
								<tbody>
									<tr><td><spring:message code="timesheetvalidation.notimesheetstovalidate" /></td></tr>
								</tbody>
							</c:when>
							<c:otherwise>
								<c:forEach var="year" items="${readyForValidation.keySet()}">
								<tr>
									<th colspan="4">${year}</th>
								</tr>
								<c:set var="weekMap" value="${readyForValidation.get(year)}" />
								<c:forEach var="week" items="${weekMap.keySet()}">
								<c:forEach var="timesheet" items="${weekMap.get(week)}"
													varStatus="tsLoop">
								<tr>
									<c:if test="${tsLoop.first}">
									<td rowspan="${weekMap.get(week).size()}">${week}</td>
									</c:if>
									<td>${timesheet.employee.name}</td>
									<td style="text-align: right"><cwms:formatDuration
																value="${timesheet.workingTime}" /></td>
									<td style="text-align: right"><cwms:formatDuration
																value="${timesheet.registeredTime}" /></td>
								</tr>
								</c:forEach>
								</c:forEach>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
			</div>
			<div id="overviewbyweek-tab" class="${validationForm.submitted ? 'vis' : 'hid'}">
				<form:form modelAttribute="validationForm">
					<fieldset>
						<ol>
							<li>
								<form:label path="week"><spring:message code="week" /></form:label>
								<form:input path="week" type="number" min="0" max="53" />
							</li>
							<li>
								<form:label path="year"><spring:message code="year" /></form:label>
								<form:input path="year" type="number" />
							</li>
							<li>
								<form:hidden path="submitted" value="true" /> 
								<input type="submit" value="<spring:message code='submit'/>" />
							</li>
						</ol>
					</fieldset>
				</form:form>
				<table class="default2">
					<c:choose>
						<c:when test="${empty validationList}">
							<tbody>
								<tr><td><spring:message code="timesheetvalidation.notimesheetstovalidate" /></td></tr>
							</tbody>
						</c:when>
						<c:otherwise>
							<thead>
								<tr>
									<th><spring:message code="timesheetvalidation.employee" /></th>
									<th><spring:message code="timesheetvalidation.workingtime" /></th>
									<th><spring:message code="timeactivity.registeredtime" /></th>
									<th><spring:message code="timesheetvalidation.validate" /></th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="validation" items="${validationList}">
									<tr>
										<td>${validation.employee.name}</td>
										<td><cwms:formatDuration value="${validation.workingTime}" /></td>
										<td><cwms:formatDuration
													value="${validation.registeredTime}" /></td>
										<td>
											<c:choose>
												<c:when test="${validation.status.name() != 'CREATED'}">
													 ${validation.status}
												</c:when>
												<c:otherwise>
													<cwms:securedLink permission="TIMESHEET_VALIDATE"  classAttr="mainlink"  parameter="?timesheetid=${validation.id}" collapse="True" >
														Validate
													</cwms:securedLink>	
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</c:otherwise>
					</c:choose>
				</table>
			</div>
		</div>
	</div>
	</jsp:body>
</t:crocodileTemplate>