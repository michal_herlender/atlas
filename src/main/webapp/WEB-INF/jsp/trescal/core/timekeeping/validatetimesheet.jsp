<%-- File name: /trescal/core/timekeeping/timesheet.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="menu.timesheet" /></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type="text/javascript">
			var menuElements = [];
			<c:forEach var="weekDay" items="${timeSheetMap.keySet()}">
				menuElements.push({ anchor: 'select-${weekDay}', block: 'timesheet-${weekDay}' });
			</c:forEach>
		</script>
		<script src='script/trescal/core/timekeeping/timesheet.js'></script>
	</jsp:attribute>
	<jsp:body>
	<t:showErrors path="timeSheet.*"/>
	<div class="infobox">
		<span class="larger-text">Validate Time Sheet for ${timeSheet.employee.name}</span>
	</div>
	<div class="infobox">
		<div id="tabmenu">
			<ul class="subnavtab">
				<c:forEach var="entry" items="${timeSheetMap.entrySet()}">
					<li>
						<a href="" id="select-${entry.key}"
							onclick="event.preventDefault(); switchMenuFocus(menuElements, 'timesheet-${entry.key}', false);"
							<c:if test="${entry.key != dayOfWeek}">class="selected"</c:if>>
							${entry.value.displayName}
						</a>
					</li>
				</c:forEach>
			</ul>
			<div class="tab-box">
				<c:forEach var="entry" items="${timeSheetMap.entrySet()}">
					<c:set var="timeSheetDay" value="${entry.value}" />
					<div id="timesheet-${entry.key}"
							class=<c:choose><c:when test="${timeSheetDay.date.getDayOfWeek() == dayOfWeek}">"vis"</c:when><c:otherwise>"hid"</c:otherwise></c:choose>>
						<table class="default2">
						<thead>
							<tr>
								<th colspan="5"><span class="larger-text">Job Item Activities</span></th>
							</tr>
							<tr>
								<th colspan="5">
									<spring:message code="timeactivity.timeonjobitems"
												arguments="${timeSheetDay.timeSpentOnTimeRelatedActions.toMinutes()}" />
								</th>
							</tr>
							<c:if test="${not empty timeSheetDay.timeRelatedActions}">
							<tr>
								<th><spring:message code="workflow.jobitem" /></th>
								<th><spring:message code="jiactions.activity" /></th>
								<th><spring:message code="workflow.remark" /></th>
								<th><spring:message code="calsearchresults.starttime" /></th>
								<th><spring:message code="jiactions.timespent" /></th>
							</tr>
							</c:if>
						</thead>
						<tbody>
							<c:forEach var="jia" items="${timeSheetDay.timeRelatedActions}"
										varStatus="loopStatus">
							<tr
											class=<c:choose><c:when test="${loopStatus.index%2==0}">"even"</c:when><c:otherwise>"odd"</c:otherwise></c:choose>>
								<td>
									<a href="viewjob.htm?jobid=${jia.jobId}" class="mainlink"
												target="_blank">${jia.jobNo}</a> /
									<links:jobitemLinkInfo jobItemId="${jia.jobItemId}"
													defaultPage="jiactions.htm" jobItemNo="${jia.itemNo}" />
								</td>
								<td>${jia.activityDescription}</td>
								<td>${jia.remark}</td>
								<td><fmt:formatDate pattern="HH:mm"
													value="${jia.startStamp}" /></td>
								<td><spring:message code="timeactivity.minutes"
													arguments="${jia.timeSpent}" /> </td>
							</tr>
							</c:forEach>
						</tbody>
						</table>
						<table class="default2">
							<thead>
								<tr>
									<th colspan="6">
										<span class="larger-text">Recorded Activities</span>
									</th>
								</tr>
								<tr>
									<th><spring:message code="timeactivity.starttime" /></th>
									<th><spring:message code="timeactivity.timespent" /></th>
									<th><spring:message code="job" /></th>
									<th><spring:message code="subdivision" /></th>
									<th><spring:message code="timeactivity.timeactivity" /></th>
									<th><spring:message code="comment" /></th>
								</tr>
							</thead>
							<tbody>
								<c:choose>
									<c:when test="${not empty timeSheetDay.entries}">
										<c:forEach var="entry" items="${timeSheetDay.entries}">
											<tr
													onclick="window.location.href='timesheetentry.htm?entryid=${entry.id}'">
												<td><fmt:formatDate value="${entry.start}"
															pattern="HH:mm" /></td>
												<td><spring:message code="timeactivity.minutes"
															arguments="${entry.duration.toMinutes()}" /></td>
												<td><c:if test="${entry.job != null}">${entry.job.jobno}</c:if></td>
												<td></td>
												<td><spring:message
															code="${entry.timeActivity.messageCode}" /></td>
												<td>${entry.comment}</td>
											</tr>
										</c:forEach>
									</c:when>
									<c:otherwise>
										<tr>
											<td colspan="6">
												<spring:message code="timeactivity.noworkingtime" />
											</td>
										</tr>
									</c:otherwise>
								</c:choose>
							</tbody>
						</table>
						<table class="default2">
							<thead>
								<tr>
									<th colspan="2"><span class="larger-text">Daily Statistics</span></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><spring:message code="timesheet.workingtime"/></td>
									<td><cwms:formatDuration value="${timeSheetDay.workingTime}"/></td>
								</tr>
								<tr>
									<td>Registered Time</td>
									<td><cwms:formatDuration value="${timeSheetDay.timeSpent}"/></td>
								</tr>
								<tr>
									<td>Overtime</td>
									<td><cwms:formatDuration value="${timeSheetDay.getTimeDifference()}"/></td>
								</tr>
							</tbody>
						</table>
					</div>
				</c:forEach>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="infobox">
		<table class="default2">
			<thead>
				<tr>
					<th><span class="larger-text">
						<spring:message code="timesheet.statistic"/>
					</span></th>
					<th><spring:message code="timesheet.week" arguments="${timeSheet.week},${timeSheet.year}"/></th>
					<th><spring:message code="timesheet.month"/></th>
					<th>Year</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><spring:message code="timesheet.workingtime"/></td>
					<td><cwms:formatDuration value="${weeklyStatistic.workingTime}"/></td>
					<td><cwms:formatDuration value="${monthlyStatistic.workingTime}"/></td>
				</tr>
				<tr>
					<td>Registered Time</td>
					<td><cwms:formatDuration value="${weeklyStatistic.registeredTime}"/></td>
					<td><cwms:formatDuration value="${monthlyStatistic.registeredTime}"/></td>
				</tr>
				<tr>
					<td>Overtime</td>
					<td><cwms:formatDuration value="${weeklyStatistic.overtime}"/></td>
					<td><cwms:formatDuration value="${monthlyStatistic.overtime}"/></td>
					<td><cwms:formatDuration value="${timeSheet.carryOver}"/></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="infobox">
		<div class="center">
			<form:form modelAttribute="timeSheet">
				<form:hidden path="id" value="${timeSheet.id}"/>
				<input type="hidden" name="action" value="VALIDATE"/>
				<input type="submit" value="Validate">
			</form:form>
		</div>
	</div>
	<div class="infobox">
		<form:form modelAttribute="timeSheet">
			<fieldset>
				<ul>
					<li>
						<form:hidden path="id" value="${timeSheet.id}"/>
						<form:label path="rejectionReason">Reason for rejection</form:label>
						<form:textarea path="rejectionReason" style="width: 100%"/>
					</li>
					<li>
						<div class="center">
							<input type="hidden" name="action" value="REJECT"/>
							<input type="submit" value="Reject">
						</div>
					</li>
				</ul>
			</fieldset>
		</form:form>
	</div>
	</jsp:body>
</t:crocodileTemplate>