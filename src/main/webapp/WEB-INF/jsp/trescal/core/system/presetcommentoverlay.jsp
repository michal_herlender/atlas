<%-- File name: /trescal/core/system/presetcommentoverlay.jsp --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div id="savedCommentOverlay">
	<t:showErrors path="comment.*"></t:showErrors>
	<form:form modelAttribute="comment">
		<fieldset>
			<form:hidden path="commentType"/>
			<form:hidden path="id"/>
			<ol>
				<li>
					<form:label path="categoryId"><spring:message code="system.category"/></form:label>
					<form:select path="categoryId">
						<form:option value="">-</form:option>
						<c:forEach var="category" items="${categories}">
							<form:option value="${category.id}"><cwms:besttranslation translations="${category.translations}"/></form:option>
						</c:forEach>
					</form:select>
				</li>
				<li>
					<form:label path="label"><spring:message code="label"/></form:label>
					<form:input path="label"/>
				</li>
				<li>
					<form:label path="comment"><spring:message code="comment"/></form:label>
					<form:textarea path="comment" class="width70" rows="8"/>
				</li>
				<li>
					<label>&nbsp;</label>
					<c:choose>
						<c:when test="${isNewComment}">
							<spring:message var="saveComment" code="system.savecomment"/>
							<input type="button" onclick="event.preventDefault(); addSavedComment(this);" value="${saveComment}"/>
						</c:when>
						<c:otherwise>
							<spring:message var="updateComment" code="system.updatecomment"/>
							<input type="button" onclick="event.preventDefault(); editSavedComment(this);" value="${updateComment}"/>
						</c:otherwise>
					</c:choose>
				</li>
			</ol>
		</fieldset>
	</form:form>
</div>