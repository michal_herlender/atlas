<%-- File name: /trescal/core/system/searchemail.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="system.searchemails" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/system/SearchEmail.js'></script>
    </jsp:attribute>
    <jsp:body>
       	<!-- infobox contains all search form elements and is styled with nifty corners -->
		<div class="infobox">
		
			<form:form method="post" action="" id="searchemail" modelAttribute="form" >
		
			<fieldset>
			
				<legend><spring:message code="system.searchemails" /></legend>
				
				<ol>
					<li>
						<form:label path="subject"><spring:message code="system.subject" />:</form:label>
						<form:input path="subject" type="text" tabindex="1" />
					</li>
					<li>
						<form:label path="date" ><spring:message code="system.senton" />:</form:label>
						<form:input  path="date" type="date" tabindex="2" />
					</li>
					<li>
						<form:label path="personid"><spring:message code="system.tocccontact" />:</form:label>
						<div id="contSearchPlugin">
							<input type="hidden" id="contCoroles" value="client,supplier,prospect,utility,business" />
							<input type="hidden" id="contindex" value="3" />
							<!-- contact results listed here -->
						</div>
						<div class="clear"></div>
					</li>
					<li>
						<form:label path="toAddress"><spring:message code="system.toccaddress" />:</form:label>
						<form:input path="toAddress" type="text" tabindex="5" />
					</li>
					<li>
						<form:label path="fromPersonId"><spring:message code="system.fromcontact" />:</form:label>
						<form:select path="fromPersonId" items="${fromContacts}" itemLabel="value" itemValue="key" />
					</li>
					<li>
						<form:label path="publish"><spring:message code="system.visibility" />:</form:label>
						<form:select path="publish" tabindex="7">
							<form:option value="">-- <spring:message code="system.any" /> --</form:option>
							<form:option value="true"><spring:message code="system.public" /></form:option>
							<form:option value="false"><spring:message code="system.private" /></form:option>
						</form:select>
					</li>
					<li>
						<label>&nbsp;</label>
						<input type="submit" value="<spring:message code="search" />" tabindex="8" />
					</li>						
				</ol>
			
			</fieldset>
			
			</form:form>
		
		</div>
		<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>