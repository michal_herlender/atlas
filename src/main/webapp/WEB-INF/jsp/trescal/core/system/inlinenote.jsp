<%-- File name: /trescal/core/system/inlinenote.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>

<li id="note_${note.noteType}${note.noteid}">
	<div class="inlinenotes_note">
		<c:choose>
			<c:when test="${note.publish}">
				<c:set var="scope" value="pub"/>
				<spring:message var="pub1" code="notes.makeprivate"/>
				<spring:message var="pub2" code="public"/>
			</c:when>
			<c:otherwise>
				<c:set var="scope" value="priv"/>
				<spring:message var="pub1" code="notes.makepublic"/>
				<spring:message var="pub2" code="private"/>
			</c:otherwise>
		</c:choose>
		<c:if test="${note.label != null && note.label != ''}">
			<strong><span class="notelabel_${scope}${note.noteid}">${note.label}</span></strong><br/>
		</c:if>
		<span class="note_${scope}${note.noteid}">${note.note}</span>
	</div>
	<div class="inlinenotes_edit">
		<c:if test="${!privateOnlyNotes}">
			(&nbsp;
			<a href="" id="publishNote${note.noteid}" onclick="event.preventDefault(); publish(${note.noteid}, ${!note.publish}, 0, '${note.noteType}', ${entityId}, this.id, '');" class="mainlink">
				${pub2} - ${pub1}
			</a>&nbsp;)&nbsp;
		</c:if>
		<a href="#" onclick="addNoteBoxy('edit', true, '${note.noteType}', ${entityId}, 0, ${note.noteid}, 0); return false; ">
			<img src="img/icons/note_edit.png" width="16" height="16" alt="<spring:message code="notes.editnote" />" title="<spring:message code="notes.editnote" />" class="noteimg_padded" />
		</a>
		<a href="#" onclick="changeNoteStatus(${note.noteid}, ${!note.active}, 0, '${note.noteType}', ${entityId}, ''); return false;">
			<img src="img/icons/note_delete.png" width="16" height="16" alt="<spring:message code="notes.deletenote" />" title="<spring:message code="notes.deletenote" />" class="noteimg_padded"/>
		</a><br />
		${note.setBy.name} (<fmt:formatDate value="${note.setOn}" type="both" dateStyle="SHORT" timeStyle="SHORT" />)
	</div>
	<div class="clear-0"></div>
</li>