<%-- File name: /trescal/core/system/instruction.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="company" tagdir="/WEB-INF/tags/company"%>

<script type='text/javascript' src='script/trescal/core/system/Instruction.js'></script>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="system.instruction"/></span>
	</jsp:attribute>
	<jsp:body>
													
	<!-- error section displayed when form submission fails -->
	<t:showErrors path="form.*" showFieldErrors="true"/>
	<!-- end error section -->		
	
	<div class="infobox" id="addinstruction">
		<c:if test="${form.instructionLink.instructionEntity.name() != 'JOB'}">
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3 center-0">
						<spring:message code="instructionform.warning" arguments="${form.instructionLink.instructionEntity.getMessage()}"/>
					</div>
				</div>
			</div>
		</c:if>	
	
		<form:form modelAttribute="form" method="post">
	
			<fieldset>
					<form:input path="refererUrl" type="hidden" />
				
				<legend>
					<spring:message code="system.addedit"/> 
					&nbsp;<spring:message code="${form.instructionLink.instructionEntity.messageCode}"/>
					&nbsp;<spring:message code="system.instruction"/> 
				</legend>
				
				<ol>
					<li>
						<label>
							<b><spring:message code="company.instructionsfor"/>:</b>
						</label>
					</li>
					<c:if test="${not empty instructionCompany}">
						<li>
							<label for="instructionCompany">
								<spring:message code="company"/>
							</label>
							<a href="viewcomp.htm?coid=${instructionCompany.coid}" class="mainlink">
								<c:out value="${instructionCompany.coname}"/>
							</a>
						</li>
					</c:if>
					<c:if test="${not empty instructionSubdiv}">
						<li>
							<label for="instructionSubdiv">
								<spring:message code="subdiv"/>
							</label>
							<a href="viewsub.htm?subdivid=${instructionSubdiv.subdivid}" class="mainlink">
								<c:out value="${instructionSubdiv.subname}"/>
							</a>
						</li>
					</c:if>
					<c:if test="${not empty instructionContact}">
						<li>
							<label for="instructionContact">
								<spring:message code="contact"/>
							</label>
							<a href="viewperson.htm?id=${instructionContact.personid}" class="mainlink">
								<c:out value="${instructionContact.name}"/>
							</a>
						</li>
					</c:if>
					<c:if test="${not empty instructionJob}">
						<li>
							<label for="instructionJob">
								<spring:message code="job"/>
							</label>
							<a href="viewjob.htm?jobid=${instructionJob.jobid}" class="mainlink">
								<c:out value="${instructionJob.jobno}"/>
							</a>
						</li>
					</c:if>
				
					<li>
						<form:label path="instructionType">
							<spring:message code="system.instructiontype"/>:
						</form:label>
						<form:select  
							path="instructionType"
							items="${instructionTypes}" itemLabel="value" itemValue="key" 
						style=" float: left; "  onchange=" additionalOptions(this); return false; "/>
						
						<c:set var="classText" value="hid"/>
						<c:if test="${form.instructionType == 'CARRIAGE'}">
							<c:set var="classText" value="vis"/>
						</c:if>
						<div id="additionalOptions" style=" float: left; margin-left: 20px; " class="${classText}">
						<form:checkbox path="includeOnDelNotes" />
								<spring:message code="system.includeonclientdeliverynotes" />
						<form:checkbox path="includeOnSupplierDelNotes" />
								<spring:message code="system.includeonsupplierdeliverynotes" />
						</div>
						
						<div class="clear"></div>
					</li>
					<c:set var="isCompanyOrSubdiv" value="${not empty param.coid || not empty param.subdivid || param.entity eq 'COMPANY' || param.entity eq 'SUBDIV' }" />
					<c:if test="${isCompanyOrSubdiv}">
						<li>
							<label for="subdivInstruction" class="jTip" 
								title="<spring:message code='instructionlink.subdivinstruction.tooltip' arguments='${sessionScope.allocatedSubdiv.value}' />"  >
								<spring:message  code="instructionlink.subdivinstruction"/><img src="img/icons/help.png" width="16" height="16" />
							</label>
							<form:checkbox id="subdivInstruction" path="subdivInstruction"/>
						</li>
					</c:if>
					<li>
						<label for="instructiontext"><spring:message code="system.instruction" />:</label>
						<form:textarea id="instructiontext" path="instructionLink.instruction.instruction" cssClass="width70 addSpellCheck" rows="8"/>
						<div class="clear"></div>
						<form:errors path="*" cssClass="errorabsolute"/>
					</li>
					<li>
						<label for="submit">&nbsp;</label>
						<c:set var="valueText"><spring:message code="system.submitinstruction"/></c:set>
						<input type="submit" id="submit" name="submit" value="${valueText}" />
					</li>
					
				</ol>
											
			</fieldset>
		
		</form:form>
	
	</div>

	</jsp:body>
</t:crocodileTemplate>