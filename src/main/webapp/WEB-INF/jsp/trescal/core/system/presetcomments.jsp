<%-- File name: /trescal/core/system/presetcomments.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="system.addeditdeletesavedcomments"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/system/PresetComments.js'></script>
	</jsp:attribute>
	<jsp:body>
		<c:choose>
			<c:when test="${model.status != null && model.status == 'failed'}">
				<div class="warningBox1">
					<div class="warningBox2">
						<div class="warningBox3">
							<spring:message code="system.warning1" />. 
							<br /><br />
							<spring:message code="system.pleasegobackandtryagain" />.
						</div>
					</div>
				</div>
			</c:when>
			<c:otherwise>
				<div class="infobox">
					<table class="default4" id="savedCommentTable" summary="<spring:message code="system.tablesavedpresetcomments" />">
						<thead>
							<tr>
								<td colspan="6">
									<div class="float-left">
										<spring:message code="system.savedcomments" /> (<span id="savedCommentSize">${model.comments.size()}</span>)
									</div>												
									<a href="" class="mainlink-float" onclick="event.preventDefault(); savedComment('Add', 0, '', '${model.type}', '${model.notetype}');">
										<spring:message code="system.newsavedcomment" />
									</a>												
									<!-- clear floats and restore page flow -->
									<div class="clear"></div>
								</td>
							</tr>
							<tr>
								<th class="cat"><spring:message code="system.category" /></th>
								<th class="lab"><spring:message code="note.notelabel"/></th>
								<th class="com"><spring:message code="system.comment" /></th>
								<th class="addby"><spring:message code="system.addedby" /></th>
								<th class="edit"><spring:message code="edit" /></th>
								<th class="del"><spring:message code="delete" /></th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="6">
									<a href="" class="mainlink-float" onclick="event.preventDefault(); savedComment('Add', 0, '', '${model.type}', '${model.notetype}');">
										<spring:message code="system.newsavedcomment" />
									</a>
								</td>
							</tr>
						</tfoot>
						<tbody>
							<c:choose>
								<c:when test="${model.comments.size() > 0}">
									<c:forEach var="c" items="${model.comments}">
										<tr id="row${c.id}">
											<td>
												<c:if test="${c.category != null}"><cwms:besttranslation translations="${c.category.translations}"/></c:if>
											</td>
											<td>${c.label}</td>
											<td>${c.comment}</td>
											<td>${c.setby.name} (<fmt:formatDate value="${c.seton}" type="date" dateStyle="SHORT" />)</td>
											<td class="center">
												<a href="" onclick="event.preventDefault(); savedComment('Edit', ${c.id}, $j(this).parent().parent().find('td:eq(1)').text(), '${model.type}', '${model.notetype}');">
													<img src="img/icons/note_edit.png" height="16" width="16" alt="<spring:message code='system.editsavedcomment'/>" title="<spring:message code='system.editsavedcomment'/>"/>
												</a>
											</td>
											<td class="center">
												<a href="" onclick="event.preventDefault(); deleteSavedComment(${c.id});">
													<img src="img/icons/delete.png" height="16" width="16" alt="<spring:message code='system.deletesavedcomment'/>" title="<spring:message code='system.deletesavedcomment'/>"/>
												</a>
											</td>
										</tr>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<tr>
										<td colspan="6" class="text-center bold"><spring:message code="system.nocommentssaved" /></td>
									</tr>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
				</div>
			</c:otherwise>
		</c:choose>
	</jsp:body>
</t:crocodileTemplate>