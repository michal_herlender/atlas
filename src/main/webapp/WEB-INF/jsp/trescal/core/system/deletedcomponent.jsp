<%-- File name: /trescal/core/_PATH1_/_PATH2_/_FILE_.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="quotation" tagdir="/WEB-INF/tags/quotation"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code='system.searchdeletedcomponents' /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type="text/javascript" src="script/trescal/core/system/DeletedComponent.js"></script>
    </jsp:attribute>
    <jsp:body>
        <!-- infobox div containing deleted invoice search form fields -->
		<div class="infobox">						
									
			<form:form modelAttribute="searchdeletedcomponentform" method="post">
				<fieldset> 
					<ol>
						<li>	
							<label for="component"><spring:message code='system.component' />:</label>
							<select name="component">
								<option value="">-- <spring:message code='all' /> --</option>
									<c:forEach var="comp" items="${searchdeletedcomponentform.deletableComponents}"> 
										<option 
											<c:if test="${not empty searchdeletedcomponentform.component and comp == searchdeletedcomponentform.component}"> 
												selected="selected" 
											</c:if>
										value="${comp}">${comp}</option>
									</c:forEach>
							
							</select>
						</li>
						<li> 
							<label for="refno"><spring:message code='system.referencenumber' />:</label>
							<form:input path="refNo"/>
						</li>
						<li> 
							<label for="dateFrom"><spring:message code='system.deletedsince' />:</label>
							<form:input path="from" type="date"/>
							
						</li>
						<li>
							<label>&nbsp;</label>
							<input type="submit" name="submit" value="<spring:message code='search' />" tabindex="3" />
						</li>
					</ol>
				</fieldset>
			
			
			</form:form>
			
		</div>
		<!-- end of infobox -->
		
		<!-- infobox div containing deleted invoice search results -->
		<div class="infobox">
		
			<table class="default2" id="delInvResults" summary="<spring:message code='system.tabledelinvoicesearchresults' />">
				<thead>
					<tr>
						<td colspan="7"><spring:message code="system.deletedcomponentresults" /> (${searchdeletedcomponentform.components.size()})</td>
					</tr>
					<tr>
						<th class="invid" scope="col"><spring:message code="system.delid" /></th>
						<th scope="col"><spring:message code="system.component" /></th>  
						<th class="invno" scope="col"><spring:message code="system.referencenumber" /></th>
						<th	class="contact" scope="col"><spring:message code="contact" /></th>
						<th class="date" scope="col"><spring:message code="date" /></th>
						<th class="reason" scope="col"><spring:message code="system.reason" /></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="7">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
				<c:choose>
					<c:when test="${searchdeletedcomponentform.components.size() > 0}">
						<c:forEach var="dc" items="${searchdeletedcomponentform.components}">
							<tr>
								<td class="center">${dc.id}</td>
								<td>${dc.component}</td>
								<td>${dc.refNo}</td>
								<td>${dc.contact.name}</td>
								<td><fmt:formatDate value="${dc.date}" type="date" dateStyle="SHORT"/></td>
								<td>${dc.reason}</td>										     
							</tr>
						</c:forEach>
				    </c:when>
				  	<c:otherwise>
						<tr>
							<td colspan="7" class="bold center"><spring:message code="system.nodeletedcomponentstodisplay" /></td>
						</tr>
				  	</c:otherwise>
				</c:choose>
				</tbody>
			</table>
		
		</div>
		<!-- end of infobox -->
    </jsp:body>
</t:crocodileTemplate>