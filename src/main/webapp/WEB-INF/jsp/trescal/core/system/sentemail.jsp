<%-- File name: /trescal/core/_PATH1_/_PATH2_/_FILE_.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<html>

<head>
		
	<link rel="stylesheet" href="styles/global.css" type="text/css" title="antech" />
	
	<link rel="stylesheet" href="styles/print.css" type="text/css" media="print" />
	
	<script type="text/javascript">
		
		// set variable to default context of project
		var springUrl = "${req.contextPath}";
		// set variable to i18n of javascript
		var i18locale = '${rc.locale.language}';
		
	</script>
	
	<script type='text/javascript' src='script/thirdparty/jQuery/jquery.js'></script>
	
	<script type='text/javascript' src='script/trescal/core/template/Cwms_Main.js'></script>
	
	<script type='text/javascript' src='script/trescal/core/system/SentEmail.js'></script>
		
</head>
	
<body onload=" loadScript.init(true); cleanUpAfterEmail(); " class="iframed" style=" width: 820px; min-width: 820px; ">
	<form:form action="">
		<input type="hidden" name="emailId" value="${form.email.id}" />
		<input type="hidden" name="currentContactId" value="${currentContact.personid}"/>
		<input type="hidden" name="sysCompId" value="${sysCompId}"/>
		<input type="hidden" name="entityId" value="${form.entityId}" />
		<c:if test="${not empty form.component }">
			<c:if test="${form.component  == 'JOB_COSTING'}">
				<input type="hidden" name="costingId" value="${form.entityId}" />
			</c:if>
		</c:if>
	</form:form>
	<c:set var="ccCount" value="0"/> 
	<c:forEach var="r" items="${form.email.recipients}">
		<c:if test="${r.type  == 'EMAIL_CC'}">
			<c:set var="ccCount" value="${ccCount + 1}"/>
		</c:if>
	</c:forEach>

	<fieldset id="emailbox">
		
		<ol>
			
			<li>
				<label class="bold"><spring:message code="system.confirmation" /></label>
				<span>&nbsp;</span>
			</li>
			<li>
				<label><spring:message code="system.to" />:</label>
				<div class="float-left padtop">
					<c:forEach var="r" items="${form.email.recipients}"> 
						<div><c:if test="${r.type  == 'EMAIL_TO'}"> ${r.recipientCon.name} (${r.emailAddress}) </c:if></div>
					</c:forEach>
				</div>
				<div class="clear-0"></div>
			</li>
			<c:if test="${ccCount > 0}">
				<li>
					<label><spring:message code="system.cc" />:</label>
					<div class="float-left padtop">
						<c:forEach var="r" items="${form.email.recipients}">
							<div><c:if test="${r.type  == 'EMAIL_CC'}"> ${r.recipientCon.name} (${r.emailAddress}) </c:if></div>
						</c:forEach>
					</div>
					<div class="clear-0"></div>
				</li>
			</c:if>
			<li>
				<label><spring:message code="system.subject" />:</label>
				<div class="float-left padtop"> 
					<div>${form.email.subject}</div>
				</div>
				<div class="clear-0"></div>
			</li>
			<li>
				<label><spring:message code="system.attachments" />:</label>
				<div class="float-left padtop">
				<c:choose>							
					<c:when test="${empty attachmentFileNames}">
						<div><spring:message code="system.noattachmentssent" /></div>
					</c:when>
					<c:otherwise>
						<c:forEach var="fileName" items="${attachmentFileNames}">
							<div>${fileName}</div>
						</c:forEach>
					</c:otherwise>
				</c:choose>
				</div>
				<div class="clear-0"></div>
			</li> 
			<li>
				<label><spring:message code="system.emailbody" />:</label>
				<div class="float-left padtop" style=" width: 70%; ">
					<div>${form.body}</div>
				</div>
				<div class="clear-0"></div>
			</li>
			
		</ol>
		
	</fieldset>
					
</body>
</html>