<%-- File name: /trescal/core/system/addemailtemplate.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<c:choose>
	<c:when test="${form.id == 0}">
		<spring:message var="pageTitle" code="system.addnewemailtemplate" />
	</c:when>
	<c:otherwise>
		<spring:message var="pageTitle" code="system.editemailtemplate" />
	</c:otherwise>
</c:choose>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">${pageTitle}</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/system/AddEmailTemplate.js'></script>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox">
			<form:form method="post" modelAttribute="form">
				<p>
					<a class="mainlink" href="<c:url value="/viewemailtemplates.htm" />">
						<spring:message code="system.backtoviewemailtemplates" />
					</a>				
				</p>
			
				<t:showErrors path="form.*" showFieldErrors="true" />
				<fieldset>
					<legend>${pageTitle}</legend>								
					<ol>
						<li>
							<label for="company"><spring:message code="company" />:</label>
							<a href="viewcomp.html?coid=${allocatedCompany.key}">${allocatedCompany.value}</a>
						</li>
						<li>
							<label for="locale"><spring:message code="company.language" />: *</label>
							<form:select path="languageTag" items="${locales}" itemLabel="value" itemValue="key" />
							<span class="attention"><form:errors path="languageTag"/></span>
						</li>
						<li>
							<label for="component"><spring:message code="system.component" />: *</label>
							<form:select path="component" items="${components}" />
							<span class="attention"><form:errors path="component"/></span>
						</li>
						<li>
							<label for="subdiv"><spring:message code="subdiv" />:</label>
							<form:select path="subdivid" items="${subdivs}" itemLabel="value" itemValue="key" />
							<span class="attention"><form:errors path="subdivid"/></span>
						</li>
						<li>
							<label for="subject"><spring:message code="system.subject" />: *</label>
							<form:input size="100" path="subject" spellcheck="false" />
							<span class="attention"><form:errors path="subject"/></span>
						</li>
						<li>
							<label for="template"><spring:message code="jicertificates.template" />: *</label>
							<form:textarea cols="120" rows="30" path="template" spellcheck="false" />
							<span class="attention"><form:errors path="template"/></span>
						</li>
					</ol>
				</fieldset>									

				<div class="marg-top text-left">
					<input type="submit" value="<spring:message code="save" />" />
					<c:if test="${form.id != 0}">
						<input type="submit" value="<spring:message code="delete" />" onClick="event.preventDefault(); $j('#deleteform').submit(); " />
					</c:if>
				</div>								
			</form:form>
			<c:if test="${form.id != 0}">
				<form:form id="deleteform" action="deleteemailtemplate.htm">
					<input type="hidden" name="id" value="${form.id} "/>
				</form:form>
			</c:if>			
		</div>
	</jsp:body>
</t:crocodileTemplate>
