<%-- File name: /trescal/core/system/viewemailtemplates.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="system.viewemailtemplates"/></span>
	</jsp:attribute>
	<jsp:body>
		<fieldset>
			<legend>
				<spring:message code="system.viewemailtemplatesfor"/>
				<a class="mainlink" href="viewcomp.htm?coid=${allocatedCompany.key}">${allocatedCompany.value}</a> 
			</legend>
			<div class="infobox">
				<c:forEach var="component" items="${supportedComponents}">
					<c:set var="templates" value="${templateMap.get(component)}"/>
					<table class="default4 emailtemplatestable" id="emailtemplatestable">
						<thead>
							<tr>
								<th class="component"><spring:message code="system.component"/></th>
								<th class="language"><spring:message code="company.language"/></th>
								<th class="subdiv"><spring:message code="subdivision"/></th>
								<th class="subject"><spring:message code="system.subject"/></th>
								<th class="edit"></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="template" items="${templates}">
								<tr>
									<td>${template.component}</td>
									<td>${template.locale.getDisplayName(rc.locale)}</td>
									<td><c:if test="${not empty template.subdiv}">${template.subdiv.subname}</c:if></td>
									<td>${template.subject}</td>
									<td><a class="mainlink" href="editemailtemplate.htm?id=${template.id}"><spring:message code="edit" /></a></td>
								</tr>
							</c:forEach>
							<tr>
								<td>${component}</td>
								<td colspan="4">
									<a class="mainlink" href="addemailtemplate.htm?component=${component}"><spring:message code="system.addnewemailtemplate"/></a>
									<c:if test="${empty templates}">(<spring:message code="company.nonespecified"/>)</c:if>
								</td>
							</tr>
						</tbody>
					</table>
				</c:forEach>
			</div>
		</fieldset>
	</jsp:body>
</t:crocodileTemplate>