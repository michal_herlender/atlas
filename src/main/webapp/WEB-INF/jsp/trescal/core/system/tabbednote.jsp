<%-- File name: /trescal/core/system/tabbednote.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>

<li id="note${note.noteid}">
	<!-- create a new div containing note -->
	<div class="notes_note">
		<c:choose>
			<c:when test="${note.publish}">
				<c:set var="noteScope" value="pub"/>
			</c:when>
			<c:otherwise>
				<c:set var="noteScope" value="priv"/>
			</c:otherwise>
		</c:choose>
		<strong><span class="notelabel_${noteScope}${note.noteid}">
			${note.label}
		</span>:</strong><br/>
		<span class="note_${noteScope}${note.noteid}">
			${note.note}
		</span>
	</div>
	<!-- create a new div containing editor details -->
	<div class="notes_editor">
		${note.setBy.name}<br/>
		<fmt:formatDate value="${note.setOn}" type="both" dateStyle="SHORT" timeStyle="SHORT"/>
	</div>
	<div class="notes_edit">
		<c:choose>
			<c:when test="${note.publish}">
				<c:set var="noteReference" value="publicA"/>
				<spring:message var="linkText" code="notes.makeprivate"/>
			</c:when>
			<c:otherwise>
				<c:set var="noteReference" value="privateA"/>
				<spring:message var="linkText" code="notes.makepublic"/>
			</c:otherwise>
		</c:choose>
		<!-- create first anchor which will be link to make note either public or private
			set onclick and use the location object to find the path to take which will 
			allow the user to keep swapping the note between public and private -->
		<span class="publishLink">
			(&nbsp;
			<a href="" class="mainlink" onclick="event.preventDefault(); publish(${note.noteid}, ${!note.publish}, ${note.setBy.personid}, '${note.noteType}', ${entityId}, '', ${noteReference});">
				${linkText}
			</a>
			&nbsp;)&nbsp;
		</span>
		<!-- the second anchor with image appended allows the user to edit the note -->
		<a href="" onclick="event.preventDefault(); addNoteBoxy('edit', ${false}, '${note.noteType}', 0, ${note.setBy.personid}, ${note.noteid}, 0);">
			<img src="img/icons/note_edit.png" width="16" height="16" alt="<spring:message code="notes.editnote" />" title="<spring:message code="notes.editnote" />" class="noteimg_padded"/>
		</a>
		&nbsp;
		<!-- the third anchor with image appended allows the user to delete the note -->
		<a href="" onclick="event.preventDefault(); changeNoteStatus(${note.noteid}, ${!note.active}, ${note.setBy.personid}, '${note.noteType}', '', ${noteReference});">
			<img src="img/icons/note_delete.png" class="noteimg_padded" width="16" height="16" alt="<spring:message code="notes.deletenote" />" title="<spring:message code="notes.deletenote" />"/>
		</a>
	</div>
	<!-- create a new div to clear floats -->
	<div class="clear-0"></div>
</li>