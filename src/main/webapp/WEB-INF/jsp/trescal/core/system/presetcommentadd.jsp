<%-- File name: /trescal/core/system/presetcommentadd.jsp --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>

<tr id="row${comment.id}">
	<td>
		<c:if test="${comment.category != null}"><cwms:besttranslation translations="${comment.category.translations}"/></c:if>
	</td>
	<td>${comment.label}</td>
	<td>${comment.comment}</td>
	<td>${comment.setby.name} (<fmt:formatDate value="${comment.seton}" type="date" dateStyle="SHORT" />)</td>
	<td class="center">
		<a href="" onclick="event.preventDefault(); savedComment('Edit', ${comment.id}, $j(this).parent().parent().find('td:eq(1)').text(), '${comment.type}', '');">
			<img src="img/icons/note_edit.png" height="16" width="16" alt="core.system:presetComment.editSavedComment" title="core.system:presetComment.editSavedComment"/>
		</a>
	</td>
	<td class="center">
		<a href="" onclick="event.preventDefault(); deleteSavedComment(${comment.id});">
			<img src="img/icons/delete.png" height="16" width="16" alt="core.system:presetComment.deleteSavedComment" title="core.system:presetComment.deleteSavedComment"/>
		</a>
	</td>
</tr>