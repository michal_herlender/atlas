<%-- File name: /trescal/core/system/deleteinstruction.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="system.deleteinstruction"/></span>
	</jsp:attribute>
	<jsp:body>
	<div class="infobox" id="addinstruction">
		<c:if test="${command.entity.name() != 'JOB' && not empty instructionLink}">
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3 center-0">
						<spring:message code="instructiondelete.warning" arguments="${command.entity.messageCode}"/>
					</div>
				</div>
			</div>
		</c:if>	
	
		<form:form method="post" >
	
			<fieldset>
					<form:input type="hidden" path="refererUrl" value="${command.refererUrl}"/>
				<legend>
					<spring:message code="delete"/> <spring:message code="${command.entity.messageCode}"/><spring:message code="system.instruction"/> 
				</legend>
				
				<ol>
					<%-- 
					SC - 03 Feb 2017
					Removed this section for now as currently all instructions are global
					<li>
						<label for="globalInstruction">
							<spring:message code="instruction.visibility"/>:
						</label>
						<c:choose>
							<c:when test="${empty command.instructionLink.organisation}">
								<spring:message code="instruction.visibility.global"/>
							</c:when>	
							<c:otherwise>	
								<spring:message code="instruction.visibility.notglobal"/>
							</c:otherwise>
						</c:choose>
					</li>
					--%>

					<li>
						<label for="instructiontypeid">
							<spring:message code="system.instructiontype"/>:
						</label>
						${instructionLink.instruction.instructiontype.type}			
					</li>
					<li>
						<label for="instructiontext"><spring:message code="system.instruction" />:</label>
						<c:out value="${instructionLink.instruction.instruction}"/>
					</li>
					<li>
						<label for="submit">&nbsp;</label>
						<c:set var="valueText"><spring:message code="system.deleteinstruction"/></c:set>
						<input type="submit" id="submit" name="submit" value="${valueText}" />
					</li>
				</ol>
											
			</fieldset>
		
		</form:form>
	
	</div>

	</jsp:body>
</t:crocodileTemplate>	