<%-- File name: /trescal/core/system/createemail.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<html>

<head>
		
		<sec:csrfMetaTags />
		<meta name="_serverTimeZone" value="${cwms:defaultTimeZone()}">
    	<meta name="_contextPath" content="${pageContext.request.contextPath}">
		<meta name="_systemType" content="${systemType}">
		<meta name="_defaultCurrency" content="${defaultCurrency.currencyCode}">

		<meta name="_locale" content="${rc.locale.toLanguageTag()}"/>
		
		<link rel="stylesheet" href="styles/global.css" type="text/css" title="antech" />
		
		<link rel="stylesheet" href="styles/print/print.css" type="text/css" media="print" />						
				
		<script type='text/javascript' src='script/thirdparty/jQuery/jquery.js'></script>
		
		<script type='text/javascript' src='script/thirdparty/jQuery/jquery-migrate-1.2.1.js'></script>
		
		<script type='text/javascript' src='script/trescal/core/template/Cwms_Main.js'></script>
		
		<script type='text/javascript' src='script/trescal/core/system/CreateEmail.js'></script>
		
		<script type='text/javascript' src='script/trescal/core/tools/FileBrowserPlugin.js'></script>
		
		<script type='module' src='script/components/cwms-upload-file/cwms-upload-file.js'></script>
		
		<script type="text/javascript">
			// set variable to default context of project
			const springUrl = "${pageContext.request.contextPath}";
			// get value of loadtab parameter from querystring
			// used to show the correct tab when subnavigation bar is used
			var LOADTAB = '${request.getParameter("loadtab")}';
			// set variable to i18n of javascript
			var i18locale = '${rc.locale.language}';
			
			var niftyElements = new Array({element: 'div.infobox', corner: 'normal'});
			
		</script>

		<script type="module">
			import "${pageContext.request.contextPath}/script/components/search-plugins/cwms-email-search/cwms-email-search.js";
		</script>
		
	</head>
	
	<body onload=" loadScript.init(true); " class="iframed" style=" width: 820px; min-width: 820px; ">

		<fieldset id="createEmail">						
			
			<form:form action="" method="post" enctype="multipart/form-data" modelAttribute="form" onsubmit=" $j('input#submit').attr('disabled', 'disabled'); ">
				
				<form:errors path="*">
                    <div class="warningBox1">
                        <div class="warningBox2">
                            <div class="warningBox3">
                                <h5 class="center-0 attention">
                                    <spring:message code='error.save' />
                                </h5>
                                <c:forEach var="e" items="${messages}">
                                	<div class="center attention"><c:out value="${e}"/></div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                </form:errors>
				<ol>						
					<li>
						<label path="from"><spring:message code="system.from" />:</label>
						<span>${form.from}</span>
						<span class="attention"><form:errors path="from" /></span>
						<c:set var="checked" value=""/>
						<c:if test="${form.includeSelfInCarbonCopy == true}">
							<c:set var="checked" value="checked"/>
						</c:if>
						<span class="float-right" style=" margin-right: 20px; ">
							<form:checkbox path="includeSelfInCarbonCopy" value="true" checked="${checked}"/>
							<input type="hidden" name="_includeSelfInCarbonCopy" value="false" />
							<spring:message code="system.includeSelfInCarbonCopy" /> (<em>${currentContact.email}</em>)
						</span>
					</li>
					<li>	
						<label><spring:message code="system.to" />:(<a href="#" onclick=" event.preventDefault(); $j(this).closest('li').next().removeClass(); " class="mainlink"><spring:message code="system.cc" /></a>)</label>
						<div class="float-left email-search-container">
							<!-- email search plugin -->
							<cwms-email-search coIds="<cwms:collectionToJson collection='${form.coIds}' />" namePrefix="to" 
							emailList="<cwms:collectionToJson collection='${form.toContacts}' />"></cwms-email-search>
						</div>
						<!-- clear floats and restore page flow -->
						<div class="clear"></div>
					</li>
					<li 
						<c:choose>
							<c:when test="${form.ccCons.size() > 0 || form.ccEmails.size() > 0}"> class="vis" </c:when>
							<c:otherwise> class="hid" </c:otherwise>
						</c:choose>>	
						<label><spring:message code="system.cc" />:</label>
						<div class="float-left email-search-container">
							<!-- email search plugin -->
							<cwms-email-search coIds="<cwms:collectionToJson collection='${form.coIds}' />" namePrefix="cc" emailList="<cwms:collectionToJson collection='${form.ccContacts}' />"></cwms-email-search>
						</div>
						<!-- clear floats and restore page flow -->
						<div class="clear"></div>
					</li>
					<li>
						<label for="form.subject"><spring:message code="system.subject" />:</label>
						<form:input path="subject" />
						<span class="attention"><form:errors path="subject" /></span>
					</li>
											
					<li>
						<label><spring:message code="system.attachments" />:</label>
						<div class="float-left" style=" width: 84%; ">
							<c:if test="${form.componentEmail == true}">
								<c:choose>
									<c:when test="${not empty form.componentAttachment}">
									<span id="attachments">
										<input type="checkbox" name="attachments" value="${form.componentAttachment}" checked="checked" />
										<c:out value="${componentAttachmentFileName}" />
									</span>
									</c:when>
									<c:when test="${not empty form.attachments}">
									<span id="attachments">
										<c:forEach var="a" items="${form.attachments}" varStatus="attachStatus">
											<input type="checkbox" name="attachments" value="${a}" checked="checked" />
											<c:if test="${not empty attachmentFileNames and attachmentFileNames.size() > attachStatus.index}"/>
											<c:out value="${attachmentFileNames.get(attachStatus.index)}" />
											<br/>
										</c:forEach>
									</span>
									</c:when>
									<c:otherwise>
									<span id="attachments" class="hid"></span>
									</c:otherwise>
								</c:choose>
								<div id="fileBrowser-sc" class="hid" style=" margin-top: 2px; ">
									<files:showFilesForSC entity="${entity}" sc="${sc}" id="${id}" identifier="${identifier}" ver="" rootFiles="${scRootFiles}" allowEmail="false" isEmailPlugin="true" rootTitle="Add from ${sc.componentName} folder" deleteFiles="true" />
								</div>
								<div id="fileLink-sc" style=" margin-top: 2px; ">
									<a href="#" class="mainlink" onclick=" event.preventDefault(); showFileBrowser($j(this).parent(), 'fileBrowser-sc'); "><spring:message code="system.browse" /><c:out value=" ${sc.componentName} " /><spring:message code="system.folder" /></a>
								</div>
							</c:if>								
							<div id="fileLink-dir" style=" margin-top: 2px; ">
								<a href="#" class="mainlink" onclick=" event.preventDefault(); showFileBrowser($j(this).parent(), 'fileBrowser-dir'); "><spring:message code="system.browsefilesystem" /></a>
							</div>
							<div id="fileBrowser-dir" class="hid" style=" margin-top: 2px; ">	
								<form:input type="file" path="file"/>
								<span class="attention"><form:errors path="file"/></span>
							</div>
							<div id="fileBrowser-reset" class="hid" style=" margin-top: 2px; ">
								<a href="#" class="mainlink" onclick=" event.preventDefault(); resetFileBrowsing($j(this).parent()); "><spring:message code="system.resetfilebrowsing" /></a>
							</div>
						</div>
						<!-- clear floats and restore page flow -->
						<div class="clear-0"></div>								
					</li>						
																	
					<li>
							<label class="ajaxlabel" for="body"><spring:message code="system.body" />:</label>
							<div class="float-left editor_box">
								<form:textarea path="body" style="height: 250px; width: 700px;" class="editor presetComments EMAIL"></form:textarea>
								<span class="attention"><form:errors path="body"/></span>
							</div>
							<!-- clear floats and restore page flow -->
							<div class="clear-0"></div>
					</li>
					<li>
						<label for="publish"><spring:message code="system.public" />:</label>
						<form:checkbox path="publish" value="true" />
						<form:errors path="publish" class="attention"/>
					</li>
					<li>
						<label for="">&nbsp;</label>
						<input type="submit" id="submit" value="<spring:message code='system.send' />" />
					</li>
					
				</ol>
			</form:form>
			
		</fieldset>
		
		<cwms-upload-file system-component-id="${sc.componentId}" entity-id1="${identifier}" entity-id2="" files-name="${filesName}" rename-to="" web="false"></cwms-upload-file>
	</body>
	
</html>