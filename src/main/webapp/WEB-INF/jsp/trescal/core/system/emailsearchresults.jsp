<%-- File name: /trescal/core/system/emailsearchresults.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="system.emailsearchresults" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/system/EmailSearchResults.js'></script>
    </jsp:attribute>
    <jsp:body>
        <!-- infobox contains all search form elements and is styled with nifty corners -->
		<div class="infobox">
			
			<form:form modelAttribute="form" action="" name="searchform" id="searchform" method="post">
			
				<form:hidden path="resultsPerPage" />
				<form:hidden path="pageNo" />
				<form:hidden path="subject" />
				<form:hidden path="personid" />
				<form:hidden path="toAddress" />
				<form:hidden path="fromPersonId" />
				<form:hidden path="publish" />
				<form:hidden path="date" />

				<t:showResultsPagination rs="${pagedResultSet}" pageNoId="" />
		
				<table class="default4" summary="<spring:message code='system.tableemailresults' />">
					
					<thead>
						<tr>
							<td colspan="7"><spring:message code="system.matchingemails" /> (${pagedResultSet.resultsCount})</td>
						</tr>
						<tr>
							<th scope="col"><spring:message code="system.senton" /></th>																					
							<th scope="col"><spring:message code="system.sentby" /></th>											
							<th scope="col"><spring:message code="system.to" /></th>
							<th scope="col"><spring:message code="system.cc" /></th>
							<th scope="col"><spring:message code="system.subject" /></th>
							<th scope="col"><spring:message code="system.publicprivate" /></th>
							<th scope="col"><spring:message code="system.view" /></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="7">&nbsp;</td>
						</tr>
					</tfoot>
					
					<tbody>
						<c:forEach var="emailDto" items="${pagedResultSet.results}">
							<tr id="email${emailDto.id}">
							    <td><fmt:formatDate value="${emailDto.sentOn}" type="both" dateStyle="SHORT" timeStyle="SHORT" /></td>
								<td><c:out value="${emailDto.sentByFirstName} ${emailDto.sentByLastName}" /></td>
								<td>
									<c:forEach var="recip" items="${emailDto.recipients}">
										<c:if test="${recip.type  == 'EMAIL_TO'}">
											${recip.emailAddress}<br />
										</c:if>
									</c:forEach>
								</td>
								<td>
									<c:forEach var="recip" items="${emailDto.recipients}">
										<c:if test="${recip.type  == 'EMAIL_CC'}"> 
											${recip.emailAddress}<br />
										</c:if>
									</c:forEach>
								</td>
								<td>${emailDto.subject}</td>
								<td>
									<c:choose>
										<c:when test="${emailDto.publish}">
											  <spring:message code="system.public" />&nbsp;
										</c:when>
										<c:otherwise>
											<spring:message code="system.private" />&nbsp; 
										</c:otherwise>
									</c:choose>
								</td>
								<td>
									<c:choose>
										<c:when test="${emailDto.publish}">
											 <a href="#" class="mainlink" onclick=" event.preventDefault(); viewEmailDetails(true, ${emailDto.id}, 'email${emailDto.id}'); ">
											 	<spring:message code="system.viewdetails" />
											 </a>
										</c:when>
										<c:otherwise>
											 <a href="#" class="mainlink" onclick=" event.preventDefault(); getAuthentication(${currentContact.personid}, ${emailDto.sentById}, ${emailDto.publish}, ${emailDto.id}, 'email${emailDto.id}'); ">
											 	<spring:message code="system.viewdetails" />*
											 </a>
										</c:otherwise>
									</c:choose>
								</td>
							</tr>		
						</c:forEach>
					</tbody>
					
				</table>
				
				<t:showResultsPagination rs="${pagedResultSet}" pageNoId="" />
			
			</form:form>							
			
		</div>
		<!-- end of infobox div -->

    </jsp:body>
</t:crocodileTemplate>