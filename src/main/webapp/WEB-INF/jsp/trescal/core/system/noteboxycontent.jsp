<%-- File name: /trescal/core/system/noteboxycontent.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>

<!-- create content for adding/editing a note -->
<div id="noteBoxy" class="overflow">
	<!-- add warning box to display error messages if the link fails -->
	<div class="hid warningBox1">
		<div class="warningBox2">
			<div class="warningBox3">
			</div>
		</div>
	</div>
	<fieldset>
		<legend><spring:message code="note.addoredit"/></legend>
		<ol>
			<!-- only add saved notes option if available for this note type -->
			<li>
				<label for="savedNotes"><spring:message code="note.savednotes"/>:</label>
				<c:choose>
					<c:when test="${supportPresetComments}">
						<select id="savedNotes" class="width70" onchange="presetCommentSelected($j(this));">
							<option value=""><spring:message code="note.selecttexttoadd"/></option>
							<c:forEach var="presetCommentCategory" items="${presetCommentMap.keySet()}">
								<c:choose>
									<c:when test="${presetCommentCategory == '' && presetCommentMap.get('').size() > 0}">
										<optgroup id="uncategorised" label="<spring:message code='note.uncategorised'/>">
											<c:forEach var="presetComment" items="${presetCommentMap.get(presetCommentCategory)}">
												<option value="<div>${fn:escapeXml(presetComment.comment)}</div>" data-label="${presetComment.label}">${empty presetComment.label ? fn:substring(fn:escapeXml(presetComment.comment),0,49):presetComment.label}</option>
											</c:forEach>
										</optgroup>
									</c:when>
									<c:otherwise>
										<optgroup label="${presetCommentCategory}">
											<c:forEach var="presetComment" items="${presetCommentMap.get(presetCommentCategory)}">
												<option value="<div>${fn:escapeXml(presetComment.comment)}</div>" data-label="${presetComment.label}">${empty presetComment.label ? fn:substring(fn:escapeXml(presetComment.comment),0,49):presetComment.label}</option>
											</c:forEach>
										</optgroup>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
						<a href="editpresetcomments.htm?notetype=${note.noteType}">
							<img src="img/icons/form_edit.png" width="16" height="16" alt="<spring:message code='note.editsavednotes'/>" title="<spring:message code='note.editsavednotes'/>"/>
						</a>
					</c:when>
					<c:otherwise>
						<span><spring:message code="note.savednotesnotsupported"/></span>
					</c:otherwise>
				</c:choose>
				<span class="relative">
					<a href="" class="initialiseSymbolBox" onclick="event.preventDefault(); drawSymbolBox(this);">
						<img src="img/icons/symbol_add.png" id="addSymbolLink" width="16" height="16" alt="<spring:message code='note.addsymbol'/>" title="<spring:message code='note.addsymbol'/>"/>
					</a>
				</span>
			</li>
			<!-- continue with note content -->
			<form:form method="post" action="" modelAttribute="note">
				<li>
					<form:label path="label"><spring:message code="note.notelabel"/>:</form:label>
					<form:input path="label" class="width60" tabindex="1"/>
					<a href="" onclick="event.preventDefault(); loadScript.spellCheck($j(this).prev(), $j(this).parent().next().find(\'textarea\'));">
						<img src="img/icons/spellcheck.png" width="16" height="16" alt="<spring:message code='note.checkspelling'/>" title="<spring:message code='note.checkspelling'/>"/>
					</a>
				</li>
				<li>
					<form:textarea path="note" id="noteBody" style="height: 300px; width: 700px;" tabindex="2"/>
				</li>
				<!-- <c:if test="${supportPresetComments}">
					<li>
						<label for="savecomment"><spring:message code="note.saveforfuture"/>:</label>
						<input type="checkbox" id="savecomment" name="savecomment" value="true"/>
						<select name="savecommentcategoryid" id="savecommentcategoryid">
							<option value="0" label="-- <spring:message code='note.uncategorised'/> --"/>
							<c:forEach var="category" items="${presetCommentCategories}">
								<option value="${category.key}">${category.value}</option>
							</c:forEach>
						</select>
					</li>
				</c:if> -->
				<c:choose>
					<c:when test="${privateOnlyNotes}">
						<li class="hid">
							<label>&nbsp;</label>
							<form:hidden path="publish" id="notePublish" value="false"/>
						</li>
					</c:when>
					<c:otherwise>
						<li>
							<spring:message var="notePrivate" code='system.private'/>
							<spring:message var="notePublic" code='system.public'/>
							<form:label path="publish"><spring:message code="note.publish"/>:</form:label>
							<form:select path="publish" id="notePublish">
								<form:option value="${false}" label="${notePrivate}"/>
								<form:option value="${true}" label="${notePublic}"/>
							</form:select>
						</li>
					</c:otherwise>
				</c:choose>
			</form:form>
			<li>
				<label for="submit"></label>
				<c:choose>
					<c:when test="${note.noteid == 0}">
						<input type="button" value="<spring:message code='save'/>" tabindex="3" onclick="insertNote(${inlineNote}, $j('#label').val(), $j('#noteBody').val(), $j('#notePublish').val() == 'true', 0, '${note.noteType}', ${entityId}, ${colspan}); return false; "/>
					</c:when>
					<c:otherwise>
						<input type="button" value="<spring:message code='update'/>" tabindex="3" onclick="updateNote(${inlineNote}, ${note.noteid}, ${note.publish}, $j('#label').val(), $j('#noteBody').val(), $j('#notePublish').val() == 'true', 0, '${note.noteType}', ${entityId}); return false;" />
					</c:otherwise>
				</c:choose>
			</li>
		</ol>
	</fieldset>
</div>