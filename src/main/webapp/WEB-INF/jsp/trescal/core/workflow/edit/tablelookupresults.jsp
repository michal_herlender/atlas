<%-- File name: /trescal/core/workflow/edit/lookupresultstable.jsp --%>
<%-- This is an HTML fragment only, intended for AJAX use --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="workflow" tagdir="/WEB-INF/tags/workflow/edit"%>

<workflow:tableLookupResults lookupFunction="${lookupFunction}"/>