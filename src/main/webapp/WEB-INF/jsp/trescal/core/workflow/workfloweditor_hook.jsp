<%-- File name: /trescal/core/workflow/workfloweditor_hook.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="workflow" tagdir="/WEB-INF/tags/workflow/edit" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%--
	Parameters returned in model
	
	d) if showing Hook (new, to be shown)
	
		@param Hook command.hookInstance
		*** In addition one of the three may be optionally present allowing navigation *** 
		@param ItemStatus command.lastStatus - optional
		@param ItemActivity command.lastActivity - optional
		@param LookupInstance command.lastLookup - optional
--%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="workflow.workfloweditor"/></span>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox workfloweditor">
			<%@include file="workfloweditor_header.jspf" %>
			<fieldset class="relative">
				<legend>Selected Hook: <workflow:showHook hook="${command.startHook}"/></legend>
				<div class="displaycolumn">
					<ol>												
						<li>
							<label>Hook ID:</label>
							<span>${command.startHook.id}</span>
							<a href="edithook.htm?id=${command.startHook.id}" class="mainlink">(Edit Hook)</a>
						</li>																								
						<li>
							<label>Hook Name:</label>
							<span>${command.startHook.name}</span>
						</li>
						<li>
							<label>Active:</label>
							<span><spring:message code="${command.startHook.active}" /></span>
						</li>
					</ol>
				</div>
				<div class="displaycolumn">
					<ol>												
						<li>
							<label>Always Possible:</label>
							<span><spring:message code="${command.startHook.alwaysPossible}" /></span>
						</li>
						<li>
							<label>Begin Activity:</label>
							<span><spring:message code="${command.startHook.beginActivity}" /></span>
						</li>
						<li>
							<label>Complete Activity:</label>
							<span><spring:message code="${command.startHook.completeActivity}" /></span>
						</li>
						<li>
							<label>Default Activity:</label>
							<span>
							<c:choose>
								<c:when test="${not empty command.startHook.activity}">
									<workflow:linkActivity itemActivity="${command.startHook.activity}" lastHook="${command.startHook}"></workflow:linkActivity>
								</c:when>
								<c:otherwise>None</c:otherwise>
							</c:choose>
							</span>
						</li>
					</ol>
				</div>
			</fieldset>
			<fieldset>
				<table class="default2">
					<thead>
						<tr>
							<td colspan="6">
								Hook Activities:
							</td>
						</tr>
						<tr>
							<th class="">Hook Activity ID:</th>
							<th class="">Begin Activity:</th>
							<th class="">Complete Activity:</th>
							<th class="">Start State (Status/Activity):</th>
							<th class="">Activity Performed:</th>
						</tr>
					</thead>			
					<tbody>
						<c:forEach var="hookActivity" items="${command.hookActivities}">
						<tr>
							<td class=""><c:out value="${hookActivity.id}"/></td>
							<td class=""><spring:message code="${hookActivity.beginActivity}"/></td>
							<td class=""><spring:message code="${hookActivity.completeActivity}"/></td>
							<td class="">
								<c:choose>
									<c:when test="${hookActivity.state.isStatus()}">
										<workflow:linkItemStatus itemStatus="${hookActivity.state}" lastHook="${command.startHook}"/>
									</c:when>
									<c:otherwise>
										<workflow:linkActivity itemActivity="${hookActivity.state}" lastHook="${command.startHook}"/>
									</c:otherwise>
								</c:choose>
							</td>
							<td class=""><workflow:linkActivity itemActivity="${hookActivity.activity}" lastHook="${command.startHook}"/></td>
						</tr>
						</c:forEach>
					</tbody>
				</table>
			</fieldset>
		</div>
	</jsp:body>
</t:crocodileTemplate>