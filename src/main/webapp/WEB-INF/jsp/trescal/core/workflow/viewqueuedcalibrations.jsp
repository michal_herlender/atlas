<%-- File name: /trescal/core/workflow/viewqueuedcalibrations.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<t:crocodileTemplate>
    <jsp:attribute name="header">
		<span class="headtext"><spring:message code="workflow.viewqueuedcalibrations"/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/workflow/ViewQueuedCalibrations.js'></script>
    </jsp:attribute>
    <jsp:body>
  			<!-- infobox contains all search results and is styled with nifty corners -->
		<div class="infobox">
		
			<form:form action="" method="post" modelAttribute="form">
				
				<div align="center">
					<input type="submit" value="<spring:message code='workflow.printcertificatesforselectedcalibrations'/>" />
				</div>
				<div class="clear">&nbsp;</div>
				
				<table class="default2">
					<thead>
						<tr>
							<th colspan="6">
								<spring:message code="workflow.queuedcalibrations"/> (${form.queuedCals.size()})
							</th>
							<th>
								<spring:message code="all"/> : <input type="checkbox" onclick=" checkAllOnPage(this); " />
							</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="7">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody>
						<c:set var="currentJobId" value="0"/>
						<c:forEach var="qc" items="${form.queuedCals}">
							<c:if test="${currentJobId != qc.queuedCal.batch.job.jobid}">
								<c:set var="currentJobId" value="${qc.queuedCal.batch.job.jobid}"/>
								<tr>
									<th colspan="7">Job No: ${qc.queuedCal.batch.job.jobno}</th>
								</tr>
								<tr>
									<th><spring:message code="itemno"/></th>
									<th><spring:message code="description"/></th>
									<th><spring:message code="caltype"/></th>
									<th><spring:message code="workflow.calibrationstarttime"/></th>
									<th><spring:message code="workflow.technician"/></th>
									<th><spring:message code="workflow.accredited"/></th>
									<th><spring:message code="all"/> : <input type="checkbox" onclick=" checkAllOnJob(${currentJobId}, this); " /></th>
								</tr>
							</c:if>
							<tr class="jobId-${currentJobId}">
								<c:set var="ji" value="${qc.queuedCal.links.iterator().next().ji}"/>
								<td>${ji.itemNo}</td>
								<td><cwms:showmodel instrumentmodel="${ji.inst.model}" /></td>
								<td><t:showTranslationOrDefault translations="${qc.queuedCal.calType.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/></td>
								<td><fmt:formatDate type="both" dateStyle="SHORT" timeStyle="SHORT" value="${qc.queuedCal.startTime}"/></td>
								<td>${qc.queuedCal.completedBy.name}</td>
								<td>
									<c:choose>
										<c:when test="${form.accreditedQueuedCals.contains(qc) == true}">
											<img src="img/icons/greentick.png" />
										</c:when>
										<c:otherwise>
											<img src="img/icons/redcross.png" />
										</c:otherwise>
									</c:choose>
								</td>
								<td>
									<c:set var="disabled" value="" />
									<c:if test="${form.accreditedQueuedCals.contains(qc) == false}">
										<c:set var="disabled" value="disabled='disabled'" />
									</c:if>
									<form:checkbox path="selectedCals" value="${qc.id}" ${disabled} />
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				
				<div class="clear">&nbsp;</div>
				<div align="center">
					<input type="submit" value="<spring:message code='workflow.printcertificatesforselectedcalibrations'/>" />
				</div>
				
			</form:form>
			
		</div>
		<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>