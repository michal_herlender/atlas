<%-- File name: /trescal/core/workflow/cleaning.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="workflow.cleaninginspection"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/workflow/Cleaning.js'></script>
		<script src='script/trescal/core/utilities/searchplugins/InstrumentSearchPlugin.js'></script>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox">
			<form:form id="frm" action="" method="post" onsubmit="$j('input#submitButton').attr('disabled', 'disabled');" modelAttribute="form">
			<form:errors path="cleaningJI.*">
        		<div class="warningBox1">
                	 <div class="warningBox2">
                    	   <div class="warningBox3">
                        	    <h5 class="center-0 attention">
                            	     <spring:message code='workflow.error' />!
                            	</h5>
                            	<c:forEach var="e" items="${messages}">
                               		<div class="center attention"><c:out value="${e}"/></div>
                            	</c:forEach>
                     	 </div>
                 	</div>
           		</div>
       	    </form:errors>
				<c:if test="${form.cleaned}">
					<div class="successBox1">
						<div class="successBox2">
							<c:forEach items="${cleaningJI.jis}" varStatus="item">
								<c:if test="${cleaningJI.cleaneds[item.index]}">
									<div class="successBox3 center-0">
										<spring:message code="workflow.cleaningcompleted" arguments="<cwms:showmodel instrumentmodel='${cleaningJI.jis[item.index].inst.model}'/>, ${cleaningJI.jis[item.index].itemNo}, ${cleaningJI.jis[item.index].job.jobno}"/>
									</div>
								</c:if>
							</c:forEach>
						</div>
					</div>
				</c:if>
				<fieldset>
					<legend><spring:message code="workflow.cleaninginspection"/></legend>
					<ol>
						<li>
							<label><spring:message code="workflow.inspected"/>:</label>
							<input type="checkbox" id="inspected" checked="checked" onclick="updateRemark();"/>
						</li>
						<li>
							<label>&nbsp;</label>
							<input type="radio" name="inspectstatus" value="no damage" checked="checked" onclick="updateRemark();"/>&nbsp;<spring:message code="workflow.nodamage"/>
							<br/>
							<input type="radio" name="inspectstatus" value="damaged" onclick="updateRemark();"/>&nbsp;<spring:message code="workflow.damaged"/>
						</li>
						<li>
							<label><spring:message code="workflow.cleaned"/>:</label>
							<input type="checkbox" id="cleaned" checked="checked" onclick="updateRemark();"/>
						</li>
						<li>
							<label><spring:message code="workflow.integrityseals"/>:</label>
							<input type="checkbox" id="integrity" onclick="updateRemark();"/>
						</li>
						<li>
							<label>&nbsp;</label>
							<input type="radio" name="integritystatus" value="intact" checked="checked" onclick="updateRemark();"/>&nbsp;<spring:message code="workflow.intact"/>
							<br/>
							<input type="radio" name="integritystatus" value="broken" onclick="updateRemark();"/>&nbsp;<spring:message code="workflow.broken"/>
						</li>
						<li>
							<form:label path="remark"><spring:message code="workflow.remark"/>:</form:label>
							<form:textarea id="remark" path="remark" rows="4" cols="45"></form:textarea>
						</li>
						<li>
								<form:label path="timeSpent"><spring:message code="workflow.time"/>:</form:label>
								<form:select path="timeSpent">
									<option ${(form.timeSpent == 5 || form.timeSpent == 0) ? "selected" : ""} value="5">5</option>
									<option ${form.timeSpent == 10 ? "selected" : ""} value="10">10</option>
									<option ${form.timeSpent == 15 ? "selected" : ""} value="15">15</option>
									<option ${form.timeSpent == 20 ? "selected" : ""} value="20">20</option>
									<option ${sform.timeSpent == 30 ? "selected" : ""} value="30">30</option>
									<option ${form.timeSpent == 45 ? "selected" : ""} value="45">45</option>
									<option ${form.timeSpent == 60 ? "selected" : ""} value="60">60</option>
								</form:select>&nbsp;<spring:message code="minutes"/>
						</li>
						<li>
								<form:label path="plantid"><spring:message code="barcode"/>:</form:label>
								<form:input type="text" path="plantid" value="${form.plantid}" onkeypress=" if (event.keyCode==13){ showInstruments (window.location.pathname, 'plantid', 'input#plantid', this, null, null, false, true); return false; }"/>
						</li>
						<li>
							<label>&nbsp;</label>
							<div id="itemsHistory" class="float-left">
								<c:forEach items="${form.plantids}" var="pi">
									<div><img src="img/icons/greentick.png" width="16" height="16"><input type="hidden" name="plantids" value="${pi}"/>${pi}</div>
								</c:forEach>
							</div>
							<!-- clear floats and restore page flow -->
							</div>
							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
						</li>
						<li>
							<label>&nbsp;</label>
							<input id="submitButton" type="submit" value="<spring:message code='submit'/>">
							&nbsp;&nbsp;
							<input type="button" value="<spring:message code='clear'/>" onclick="$j('input#plantid').val('').focus(); $j('#itemsHistory').html(''); return false;"/>
						</li>
					</ol>
				</fieldset>
			</form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>