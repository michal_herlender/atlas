<%-- File name: /trescal/core/workflow/workfloweditor_status.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="workflow" tagdir="/WEB-INF/tags/workflow/edit"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%--
	Parameters returned in model for ItemStatus
	
	@param ItemStatus command.startStatus - mandatory
	*** In addition one of the three may be optionally present allowing navigation *** 
	@param ItemStatus command.lastStatus - optional
	@param ItemActivity command.lastActivity - optional
	@param LookupInstance command.lastLookup - optional
--%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="workflow.workfloweditor"/></span>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox workfloweditor">
			<%@include file="workfloweditor_header.jspf" %>
			<fieldset class="relative">
				<legend>Selected Status: <workflow:showItemStatus itemStatus="${command.startStatus}"/></legend>
				<div class="displaycolumn">
					<ol>												
						<li>
							<label>State ID:</label>
							<span>${command.startStatus.stateid}</span>
							<a href="edititemstatus.htm?id=${command.startStatus.stateid}" class="mainlink">(Edit Status Definition)</a>
						</li>																								
						<li>
							<label>Description:</label>
							<span>${command.startStatus.description}</span>
						</li>
						<li>
							<label>Type:</label>
							<span>
								${command.startStatus.getClass().getSimpleName()}
							</span>
						</li>
						<li>
							<label>Legend:</label>
							<span class="activity">Activity - Green</span>
							<span class="status">Status - Blue</span>
							<span class="lookup">Lookup - Red</span>
						</li>
					</ol>
				</div>
				<div class="displaycolumn">
					<ol>												
						<li>
							<label>Retired:</label>
							<span><spring:message code="${command.startStatus.retired}"/></span>
						</li>																								
						<li>
							<label>Active:</label>
							<span><spring:message code="${command.startStatus.active}"/></span>
						</li>
						<li>
							<label># State Groups:</label>
							<span>${command.startStatus.groupLinks.size()}</span>
						</li>
						<li>
							<label># Next Activities:</label>
							<span>${command.startStatus.nextActivities.size()}</span>
						</li>
					</ol>
				</div>
			</fieldset>
			<fieldset>
				<workflow:tableItemStateTranslations itemState="${command.startStatus}" />
			</fieldset>
			<fieldset>
				<workflow:tableStateGroupLinks itemState="${command.startStatus}" />
			</fieldset>
			<fieldset>
				<table class="default2">
					<thead>
						<tr>
							<th colspan="3">Next Activities:</th>
						</tr>
						<tr>
							<td>Activity</td>
							<td>Manual Entry Allowed</td>
							<td>ID</td>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="nextact" items="${command.startStatus.nextActivities}">
							<tr>
								<td><workflow:linkActivity itemActivity="${nextact.activity}" lastItemStatus="${command.startStatus}"/></td>
								<td><spring:message code="${nextact.manualEntryAllowed}" /></td>
								<td><c:out value="${nextact.id}" /></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</fieldset>
		</div>
	</jsp:body>
</t:crocodileTemplate>