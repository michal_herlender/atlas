<%-- File name: /trescal/core/workflow/workflowbrowser.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<script type='text/javascript' src='script/trescal/core/workflow/WorkflowBrowser.js'></script>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="workflow.workfloweditor"/></span>
	</jsp:attribute>
	<jsp:body>

	<h1>Workflow - Tree View - Proof of Concept</h1>
	<div id="notes">
		<ul>
			<li>Currently displays from single entry point of collection for laboratory service</li>
			<li>Results are proxied to ItemActivity / ItemStatus and not to subtypes, consider using enums of ItemActivityType and ItemStatusType?
			<li>Consider adding functionality when clicking on a "See LookupInstance 62" that it highlights the actual Lookup</li>
			<li>Consider grouping workflow into multiple categories</li>
			<li>Show errors in workflow in red (e.g. lookups missing connections to results)</li>
		</ul>
	</div>
	
	<div id="ajax"></div>

	</jsp:body>
</t:crocodileTemplate>