<%-- File name: /trescal/core/workflow/workfloweditor_lookup.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="workflow" tagdir="/WEB-INF/tags/workflow/edit" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%--
	Parameters returned in model
	
	d) if showing LookupInstace (new, to be shown)
	
		@param LookupInstance command.lookupInstance
		*** In addition one of the three may be optionally present allowing navigation *** 
		@param ItemStatus command.lastStatus - optional
		@param ItemActivity command.lastActivity - optional
		@param LookupInstance command.lastLookup - optional
--%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="workflow.workfloweditor"/></span>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox workfloweditor">
			<%@include file="workfloweditor_header.jspf" %>
			<fieldset class="relative">
				<legend>Selected Lookup: <workflow:showLookup lookupInstance="${command.startLookupInstance}"/></legend>
				<div class="displaycolumn">
					<ol>												
						<li>
							<label>Lookup ID:</label>
							<span>${command.startLookupInstance.id}</span>
							<a href="editlookupinstance.htm?id=${command.startLookupInstance.id}" class="mainlink">(Edit Lookup Instance)</a>
						</li>																								
						<li>
							<label>Bean Name:</label>
							<span>${command.startLookupInstance.lookupFunction.getBeanName()}</span>
						</li>
						<li>
							<label>Description:</label>
							<span>${command.startLookupInstance.description}</span>
						</li>
						<li>
							<label>Legend:</label>
							<span class="activity">Activity - Green</span>
							<span class="status">Status - Blue</span>
							<span class="lookup">Lookup - Red</span>
						</li>
					</ol>
				</div>
			</fieldset>

			<table class="default2">
				<thead>
					<tr>
						<th colspan="7">Lookup Results:</th>
					</tr>
					<tr>
						<td>Index</td>
						<td>Message</td>
						<td>Description</td>
						<td colspan="2">Wired To</td>
						<td>LR ID</td>
						<td>OS ID</td>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="lookupMessage" items="${command.startLookupInstance.lookupFunction.getLookupResultMessages()}" varStatus="rowStatus">
						<tr>
							<td>${rowStatus.index}</td>
							<td>${lookupMessage}</td>
							<td><spring:message code="${lookupMessage.messageCode}" /></td>
							<c:set var="lookupResult" value="${mapLookupResults.get(lookupMessage)}" />
							<c:choose>
								<c:when test="${empty lookupResult}">
									<td colspan="4">Error - Not Wired</td>
								</c:when>
								<c:when test="${not empty lookupResult.outcomeStatus.lookup}">
									<td>Lookup</td>
									<td><workflow:linkLookup lookupInstance="${lookupResult.outcomeStatus.lookup}" lastLookupInstance="${command.startLookupInstance}" /></td>
									<td>${lookupResult.id}</td>
									<td>${lookupResult.outcomeStatus.id}</td>
								</c:when>
								<c:when test="${not empty lookupResult.outcomeStatus.status}">
									<td>Status</td>
									<td><workflow:linkItemStatus itemStatus="${lookupResult.outcomeStatus.status}" lastLookupInstance="${command.startLookupInstance}" /></td>
									<td>${lookupResult.id}</td>
									<td>${lookupResult.outcomeStatus.id}</td>
								</c:when>
							</c:choose>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</jsp:body>
</t:crocodileTemplate>