<%-- File name: /trescal/core\workflow/viewworkforuser.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="instrument" tagdir="/WEB-INF/tags/instrument" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="workflow.viewworkforuser"/> (${command.contact.name})</span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/workflow/ViewWorkForUser.js'></script>
    </jsp:attribute>
    <jsp:body>
        <!-- this infobox contains work for user tabs -->
	<div class="infobox" id="workForUser">
				
		<c:set var="globalRowcount" value="1"/> 
		
									
		<div class="float-right marg-right">
			<select onchange=" window.location.href = 'viewworkforuser.htm?personid=' + this.value; ">
				<option selected="selected"><spring:message code="workflow.viewworkforanotheruser"/>...</option>
				<c:forEach var="con" items="${command.businesscontacts}">
					<option value="${con.personid}">${con.name}</option>
				</c:forEach>
			</select>
		</div>
		<!-- clear floats and restore page flow -->
		<div class="clear"></div>
		
		<div class="padding2">
			<span>						
				<input type="checkbox" class="checkbox" name="all" onclick=" selectAllLabs(this.checked); " checked="checked" />
				<spring:message code="all"/> &nbsp;&nbsp;
				<c:forEach var="dp" items="${command.depts}">
					<c:choose>
						<c:when test="${dp.name.indexOf('Laboratory') != -1}">
							 <c:set var="deptName" value="${dp.name.replace('Laboratory','')}"/> 
						</c:when>
						<c:when test="${dp.name.indexOf('Laboratorio') != -1}">
							 <c:set var="deptName" value="${dp.name.replace('Laboratorio','')}"/> 
						</c:when>
				  	  	<c:otherwise>
							<c:set var="deptName" value="${dp.name}"/> 
						</c:otherwise>
					</c:choose>
					| &nbsp;&nbsp;
					<input type="checkbox" class="checkbox" name="filterlabs" value="${dp.id}" onclick=" filterLabView(); " checked="checked" /> ${deptName} &nbsp;&nbsp;													
				</c:forEach>
			</span>
		</div>
		
		<!-- div to hold all the tabbed submenu -->
		<div id="tabmenu">
			
			<!-- solves bug in IE where bar under subnav dissappears -->	
			<div>&nbsp;</div>
					
			<!-- tabbed submenu to change between user task lists -->	
			<ul class="subnavtab">
				<li><a href="" id="allocated-link" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'allocated-tab', false); " class="selected"><spring:message code="workflow.allocatedwork"/> (<span id="allocatedWorkCount1">${command.allocatedwork.size()}</span>)</a></li>
				<li><a href="" id="activities-link" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'activities-tab', false); "><spring:message code="workflow.ongoingactions"/> (${command.ongoingactivities.size()} / ${command.ongoingcals.size()})</a></li>
				<li><a href="" id="available-link" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'available-tab', false); "><spring:message code="workflow.availablecalibrations"/> (${command.available.size()})</a></li>
				<li><a href="" id="upcoming-link" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'upcoming-tab', false); "><spring:message code="workflow.upcomingwork"/> (${command.upcomingwork.size()})</a></li>
			</ul>
			<!-- end of tabbed submenu -->
			
			<!-- div to hold all content which is to be changed using the tabbed submenu -->
			<div class="tab-box">
				
				<!-- user's on-going activities tab -->
				<div id="allocated-tab">
					
					<table class="default4 allocatedwork tablesorter" id="tableallocated" summary="<spring:message code="workflow.tableitemsallocatedtouser"/>">
						<thead>
							<tr>
								<td colspan="8">
									<c:choose>
										<c:when test="${currentContact.personid == command.contact.personid}">
	                                    <spring:message code="workflow.youcurrentlyhave"/>&nbsp;<span id="allocatedWorkCount2">${command.allocatedwork.size()}&nbsp;</span> 
	                                    <c:choose>
	                                    	<c:when test="${command.allocatedwork.size() == 1}">
	                                   			 <spring:message code="item"/>
	                                    	</c:when>
											<c:otherwise> <spring:message code="items"/></c:otherwise>
										</c:choose>
	                                    <font style="text-transform:lowercase">&nbsp;<spring:message code="workflow.allocated"/></font>
										</c:when>
										<c:otherwise>
											${command.contact.firstName}&nbsp;<spring:message code="workflow.currentlyhas"/>&nbsp;<span id="allocatedWorkCount2">&nbsp;${command.allocatedwork.size()}&nbsp;</span> 
	                                    <c:choose>
	                                    	<c:when test="${command.allocatedwork.size() == 1}"> 
	                                    		<spring:message code="item"/>
	                                  	    </c:when>
											<c:otherwise> <spring:message code="items"/></c:otherwise>
										</c:choose> 
										<font style="text-transform:lowercase">&nbsp;<spring:message code="workflow.allocated"/></font>
										</c:otherwise>
									</c:choose>
								</td>
							</tr>
							<tr>
								<th class="comp" scope="col"><spring:message code="company"/></th>
								<th class="jobno" scope="col"><spring:message code="workflow.jobitemno"/></th>
								<th class="inst" scope="col"><spring:message code="instmodelname"/></th>
								<th class="allocator" scope="col"><spring:message code="workflow.allocator"/></th>
								<th class="time" scope="col"><spring:message code="workflow.schedulefor"/></th>
								<th class="duedate" scope="col"><spring:message code="duedate"/></th>
								<th class="message" scope="col"><spring:message code="workflow.messages"/></th>
								<th class="complete" scope="col"><spring:message code="workflow.done"/></th>
							</tr>
						</thead>
						<tbody>												
							<c:choose>
								<c:when test="${command.allocatedwork.size() > 0}">
								<c:forEach var="ea" items="${command.allocatedwork}">
									<tr>
										<td class="comp">${ea.itemAllocated.job.con.sub.comp.coname}</td>
										<td class="jobno">
											<links:jobnoLinkDWRInfo jobid="${ea.itemAllocated.job.jobid}" jobno="${ea.itemAllocated.job.jobno}" copy="true" rowcount="${globalRowcount}"/>&nbsp;
											<links:jobitemLinkDWRInfo jobitem="${ea.itemAllocated}" jicount="${true}" rowcount="${globalRowcount}"/>
										</td>
										<td class="inst"><cwms:showinstrument instrument="${ea.itemAllocated.inst}"/></td> 
										<td class="allocator">${ea.allocatedBy.shortenedName}</td> 
										<td class="time">
											<fmt:formatDate value="${ea.allocatedFor}" type="date" dateStyle="SHORT" /><c:out value="${ea.timeOfDay})"/>
										</td>
										<td class="duedate"><fmt:formatDate value="${ea.itemAllocated.dueDate}" type="date" dateStyle="SHORT"  /></td>
										<td class="message">
											<div id="awmessages${ea.id}">
												<a href="" class="addMessage" onclick=" event.preventDefault(); createAddEditMessageBox('add', ${ea.id}, 0, '', ${currentContact.personid}); ">
													<img src="img/icons/add.png" width="16" height="16" title="<spring:message code="workflow.addnewmessage"/>" />
												</a>
												<c:choose>
													<c:when test="${ea.messages.size() > 0}">
													<c:forEach var="message" items="${ea.messages}">
														<div id="awmessage${message.id}" class="width90 padding2 
															<c:if test="${velocityCount > 1}"> border-top </c:if>">
															<em>${message.message}</em>&nbsp;
															${message.setBy.initials}&nbsp;
															<fmt:formatDate value="${message.setOn}" type="date" dateStyle="SHORT" />																	
															<c:if test="${currentContact.personid == message.setBy.personid}">
																<c:set var="escMessage"><spring:escapeBody javaScriptEscape="true"><c:out value="${message.message}"/></spring:escapeBody></c:set>
																<a href="" class="editMessage" onclick=" event.preventDefault(); createAddEditMessageBox('edit', ${ea.id}, ${message.id}, '${escMessage}', ${currentContact.personid}); ">
																	<img src="img/icons/edit_small.png" width="12" height="12" title="<spring:message code='workflow.editmessage'/>" />
																</a>
																&nbsp;
																<a href="" class="deleteMessage" onclick=" event.preventDefault(); deleteAllocatedWorkMessage(this, ${message.id}); ">
																	<img src="img/icons/small_delete.png" width="12" height="12" title="<spring:message code='workflow.editmessage'/> <spring:message code='workflow.deletemessage'/>" />
																</a>
															</c:if>
														</div>
													</c:forEach>
													</c:when>
													<c:otherwise>
														&nbsp;
													</c:otherwise>
												</c:choose>
											</div>																
										</td> 
										<td class="complete">
											<c:choose>
												<c:when test="${currentContact.personid == command.contact.personid}">
												<input type="checkbox" onclick=" event.preventDefault(); completeAllocatedWork(this, ${ea.id}); " />
												</c:when>
												<c:otherwise>
													&nbsp;
												</c:otherwise>
											</c:choose>
										</td> 
									</tr>
								</c:forEach>
								</c:when>
								<c:otherwise>
								<tr>
									<td colspan="8" class="text-center bold">
										<c:choose>
											<c:when test="${currentContact.personid == command.contact.personid}">
												<spring:message code="workflow.youcurrentlyhavenoitemsallocated"/>
											</c:when>
											<c:otherwise>
												${command.contact.firstName}&nbsp;<spring:message code="workflow.currentlyhasnoitemsallocated"/>
											</c:otherwise>
										</c:choose>	
									</td>
								</tr>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
					
				</div>
				<!-- end of user's on-going activities tab -->
				
				<!-- user's on-going activities tab -->
				<div id="activities-tab" class="hid">
					
					<table class="default4 ongoingactivities" summary="<spring:message code="workflow.tableongoingactivities"/>">
						<thead>
							<tr>
								<td colspan="8">
									<c:choose>
										<c:when test="${currentContact.personid == command.contact.personid}">
											<spring:message code="workflow.youcurrentlyhave"/>&nbsp;${command.ongoingactivities.size()}&nbsp;
											<c:choose>
												<c:when test="${command.ongoingactivities.size() == 1}"> 
													<spring:message code="workflow.activity"/>
												</c:when>
												<c:otherwise>
													<spring:message code="workflow.activities"/>
												</c:otherwise>
											</c:choose>	
										</c:when>
										<c:otherwise>
										${command.contact.firstName}&nbsp;<spring:message code="workflow.currentlyhas"/>&nbsp;${command.ongoingactivities.size()}&nbsp;
										<c:choose>
											<c:when test="${command.ongoingactivities.size() <= 1}">  
												<spring:message code="workflow.activity"/>
											</c:when>
											<c:otherwise> 
												<spring:message code="workflow.activities"/>
											</c:otherwise>
										</c:choose>	
										</c:otherwise>
									</c:choose>	
								</td>
							</tr>
							<tr>
								<th class="comp" scope="col"><spring:message code="company"/></th>
								<th class="jobno" scope="col"><spring:message code="jobno"/></th>
								<th class="item" scope="col"><spring:message code="item"/></th>
								<th class="activity" scope="col"><spring:message code="workflow.bactivity"/></th>
								<th class="started" scope="col"><spring:message code="workflow.started"/></th>
								<th class="inst" scope="col"><spring:message code="instmodelname"/></th>
								<th class="serial" scope="col"><spring:message code="serialno"/></th>
								<th class="plant" scope="col"><spring:message code="plantno"/></th>
							</tr>
						</thead>
						<tbody>												
							<c:choose>
								<c:when test="${command.ongoingactivities.size() > 0}">
								<c:forEach var="activity" items="${command.ongoingactivities}">
									<tr id="${activity.jobItem.nextWorkReq.workRequirement.department.name}${globalRowcount}">
										<td class="comp"><links:companyLinkDWRInfo company="${activity.jobItem.job.con.sub.comp}" rowcount="${globalRowcount}" copy="${false}"/></td>
										<td class="jobno"><links:jobnoLinkDWRInfo jobid="${activity.jobItem.job.jobid}" jobno="${activity.jobItem.job.jobno}" copy="false" rowcount="${globalRowcount}"/></td>
										<td class="item"><links:jobitemLinkDWRInfo jobitem="${activity.jobItem}" jicount="${true}" rowcount="${globalRowcount}"/></td>
										<td class="activity">${activity.activityDesc}</td> 
										<td class="started"><fmt:formatDate value="${activity.startStamp}" type="time" dateStyle="SHORT" timeStyle="SHORT"/><br /><fmt:formatDate value="${activity.startStamp}" type="date" dateStyle="SHORT"/></td>
										<td class="inst"><links:instrumentLinkDWRInfo instrument="${activity.jobItem.inst}" rowcount="${globalRowcount}" displayBarcode="false" displayName="true" displayCalTimescale="false" /></td>
										<td class="serial">${activity.jobItem.inst.serialno}</td>
										<td class="plant">${activity.jobItem.inst.plantno}</td>														
									</tr>														
									<c:set var="globalRowcount" value="${globalRowcount + 1}"/>
								</c:forEach>
								</c:when>
								<c:otherwise> 
								<tr>
									<td colspan="8" class="text-center bold">
										<c:choose>
											<c:when test="${currentContact.personid == command.contact.personid}">
												<spring:message code="workflow.youcurrentlyhavenoongoingactivities"/>
											</c:when>
											<c:otherwise> 
												${command.contact.firstName}&nbsp;<spring:message code="workflow.currentlyhasnoongoingactivities"/>
											</c:otherwise>
										</c:choose>	
									</td>
								</tr>
								</c:otherwise>
							</c:choose>	
						</tbody>
					</table>
					
					<div class="clear">&nbsp;</div>
					
					<table class="default4 ongoingcals" summary="<spring:message code="workflow.tableongoingcalibrations"/>">
						<thead>
							<tr>
								<td colspan="7">
									<c:choose>
										<c:when test="${currentContact.personid == command.contact.personid}">
	                                   	 <spring:message code="workflow.youcurrentlyhave"/>&nbsp;${command.ongoingcals.size()}&nbsp;<spring:message code="calibrations"/>&nbsp;<spring:message code="workflow.youcurrentlyhave.ongoing"/>&nbsp;
	                                   	 <c:choose>
	                                   	 	<c:when test="${command.ongoingcals.size() == 1}"> <font style="text-transform:lowercase"></font>
	                                   	 	</c:when>
										 	<c:otherwise>  
	                                   	    </c:otherwise>
										 </c:choose>
										</c:when>
										<c:otherwise> 
											${command.contact.firstName}&nbsp;
											<spring:message code="workflow.currentlyhas"/>&nbsp;${command.ongoingcals.size()}&nbsp;<spring:message code="calibration"/>&nbsp;<spring:message code="workflow.youcurrentlyhave.ongoing"/>&nbsp;
											<c:choose>
												<c:when test="${command.ongoingcals.size() == 1}"> <font style="text-transform:lowercase"></font>
												</c:when>
											</c:choose>
										</c:otherwise>
									</c:choose>	
								</td>
							</tr>
							<tr>
								<th class="comp" scope="col"><spring:message code="company"/></th>
								<th class="jobno" scope="col"><spring:message code="jobno"/></th>
								<th class="item" scope="col"><spring:message code="item"/></th>
								<th class="started" scope="col"><spring:message code="workflow.started"/></th>
								<th class="inst" scope="col"><spring:message code="instmodelname"/></th>
								<th class="serial" scope="col"><spring:message code="serialno"/></th>
								<th class="plant" scope="col"><spring:message code="plantno"/></th>
							</tr>
						</thead>
						<tbody>
							<c:choose>
								<c:when test="${command.ongoingcals.size() > 0}">					
								<c:forEach var="calibration" items="${command.ongoingcals}">
									<tr>
										<th colspan="7">
											<c:choose>
												<c:when test="${calibration.links.size() > 1}">
													<spring:message code="workflow.groupcalibration"/>:
												</c:when>
					 							<c:otherwise>
													<spring:message code="workflow.singlecalibration"/>:
												</c:otherwise>
											</c:choose>		
										</th>
									</tr>													
									<c:forEach var="cl" items="${calibration.links}"> 
										<tr id="${cl.ji.nextWorkReq.workRequirement.department.name}${globalRowcount}">
											<td class="comp"><links:companyLinkDWRInfo company="${cl.ji.job.con.sub.comp}" rowcount="${globalRowcount}" copy="${false}"/></td>
											<td class="jobno"><links:jobnoLinkDWRInfo jobid="${cl.ji.job.jobid}" jobno="${cl.ji.job.jobno}" copy="false" rowcount="${globalRowcount}"/></td>
											<td class="item"><links:jobitemLinkDWRInfo jobitem="${cl.ji}" jicount="${true}" rowcount="${globalRowcount}"/></td>
											<td class="started"><fmt:formatDate value="${calibration.startTime}" type="time" timeStyle="SHORT"/><br /><fmt:formatDate value="${calibration.startTime}" type="date" dateStyle="SHORT"/></td> 
											<td class="inst"><links:instrumentLinkDWRInfo instrument="${cl.ji.inst}" rowcount="${globalRowcount}" displayBarcode="false" displayName="true" displayCalTimescale="false" /></td>
											<td class="serial">${cl.ji.inst.serialno}</td>
											<td class="plant">${cl.ji.inst.plantno}</td>
										</tr>
										<c:set var="globalRowcount" value="${globalRowcount + 1}"/>
									</c:forEach>														
								</c:forEach>
								</c:when>
					 			<c:otherwise>
								<tr>
									<td colspan="7" class="text-center bold">
										<c:choose>
											<c:when test="${currentContact.personid == command.contact.personid}">
												<spring:message code="workflow.youcurrentlyhavenoongoingcalibrations"/>
											</c:when>
			 								<c:otherwise>
												${command.contact.firstName}&nbsp;<spring:message code="workflow.currentlyhasnoongoingcalibrations"/>
											</c:otherwise>
										</c:choose>		
									</td>
								</tr>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
					
				</div>
				<!-- end of user's on-going activities tab -->
	
				<!-- user's items available for calibration tab -->
				<div id="available-tab" class="hid">
																									
					<table class="default4 availableforcal tablesorter" id ="tableavailable" summary="<spring:message code="workflow.tableitemsavailableforcalibration"/>">
						<thead>
							<tr>
								<td colspan="7">
									<c:choose>
										<c:when test="${currentContact.personid == command.contact.personid}"> 
											<spring:message code="workflow.therearecurrently"/>&nbsp;${command.available.size()}&nbsp;
											<spring:message code="item"/>(s)&nbsp;
											<c:choose>
												<c:when test="${command.available.size() == 1}"> 
													<spring:message code="workflow.active"/>
												</c:when>
			 									<c:otherwise> 
													<spring:message code="workflow.active"/>
												</c:otherwise>
											</c:choose>	
											&nbsp;<spring:message code="workflow.thatyouareaccreditedtocalibrate"/>
										</c:when>
					 					<c:otherwise>
											<spring:message code="workflow.therearecurrently"/>&nbsp;${command.available.size()} &nbsp;
											<spring:message code="workflow.active"/>&nbsp;
											<c:choose>
												<c:when test="${command.available.size() == 1}"> 
													<spring:message code="item"/>
												</c:when>
			 									<c:otherwise>
			 									<spring:message code="items"/>
			 									</c:otherwise>
											</c:choose>	
											&nbsp;<spring:message code="workflow.that"/>&nbsp;${command.contact.firstName} 
											&nbsp;<spring:message code="workflow.isaccreditedtocalibrate"/>
										</c:otherwise>
									</c:choose>	
								</td>
							</tr>
							<tr>
								<th class="comp" scope="col"><spring:message code="company"/></th>
								<th class="jobno" scope="col"><spring:message code="jobno"/></th>
								<th class="item" scope="col"><spring:message code="item"/></th>
								<th class="inst" scope="col"><spring:message code="instmodelname"/></th>
								<th class="serial" scope="col"><spring:message code="serialno"/></th>
								<th class="plant" scope="col"><spring:message code="plantno"/></th>
								<th class="duedate" scope="col"><spring:message code="duedate"/></th>
							</tr>
						</thead>
						<tbody>
							<c:choose>
								<c:when test="${command.available.size() > 0}">
								<c:forEach var="ji" items="${command.available}"> 	
									<c:if test="${ji.turn <= command.fastTrackTurn}">
										<tr id="dept${ji.nextWorkReq.workRequirement.department.id}-${globalRowcount}" style=" background-color: ${ji.calType.serviceType.displayColourFastTrack}; ">
											<td class="comp"><c:out value="${ji.job.con.sub.comp.coname}" /></td>
											<td class="jobno"><links:jobnoLinkDWRInfo jobid="${ji.job.jobid}" jobno="${ji.job.jobno}" copy="false" rowcount="${globalRowcount}"/></td>
											<td class="item"><links:jobitemLinkDWRInfo jobitem="${ji}" jicount="${true}" rowcount="${globalRowcount}"/></td>
											<td class="inst"><cwms:showinstrument instrument="${ji.inst}" /></td>
											<td class="serial">${ji.inst.serialno}</td>
											<td class="plant">${ji.inst.plantno}</td>														
											<td class="duedate"><fmt:formatDate value="${ji.dueDate}" type="date" dateStyle="SHORT" /></td>														
										</tr>
									</c:if>	
									<c:set var="globalRowcount" value="${globalRowcount + 1}"/>
								</c:forEach>
								<c:forEach var="ji" items="${command.available}">	
									<c:if test="${ji.turn > command.fastTrackTurn}">
										<tr id="dept${ji.nextWorkReq.workRequirement.department.id}-${globalRowcount}" style=" background-color: ${ji.calType.serviceType.displayColour}; ">
											<td class="comp"><c:out value="${ji.job.con.sub.comp.coname}" /></td>
											<td class="jobno"><links:jobnoLinkDWRInfo jobid="${ji.job.jobid}" jobno="${ji.job.jobno}" copy="false" rowcount="${globalRowcount}"/></td>
											<td class="item"><links:jobitemLinkDWRInfo jobitem="${ji}" jicount="${true}" rowcount="${globalRowcount}"/></td>
											<td class="inst"><cwms:showinstrument instrument="${ji.inst}" /></td>
											<td class="serial">${ji.inst.serialno}</td>
											<td class="plant">${ji.inst.plantno}</td>														
											<td class="duedate"><fmt:formatDate value="${ji.dueDate}" type="date" dateStyle="SHORT" /> </td>														
										</tr>
									</c:if>
									<c:set var="globalRowcount" value="${globalRowcount + 1}"/>
								</c:forEach>
								</c:when>
								<c:otherwise>
								<tr>
									<td colspan="7" class="text-center bold">
										<c:choose>
											<c:when test="${currentContact.personid == command.contact.personid}">
												<spring:message code="workflow.therearecurrentlynoactiveitemsthatyouareaccreditedtocalibrate"/>
											</c:when>
											<c:otherwise>
												<spring:message code="workflow.therearecurrentlynoactiveitemsthat"/>&nbsp;${command.contact.firstName}&nbsp;<spring:message code="workflow.isaccreditedtocalibrate"/>
											</c:otherwise>
										</c:choose>	
									</td>
								</tr>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>									
					
				</div>
				<!-- end of user's items available for calibration tab -->
				
				<!-- user upcoming work tab -->
				<div id="upcoming-tab" class="hid">
					
					<table class="default4 upcomingwork" summary="<spring:message code="workflow.tableallupcomingwork"/>">
						<thead>
							<tr>
								<td colspan="4">
									<c:choose>
										<c:when test="${currentContact.personid == command.contact.personid}">
											<spring:message code="workflow.youhave"/>&nbsp;${command.upcomingwork.size()}&nbsp;<spring:message code="workflow.upcomingworkof"/>&nbsp;
											<c:choose>
												<c:when test="${command.upcomingwork.size() == 1}">
													 <spring:message code="workflow.entry"/>
												</c:when>
												<c:otherwise>
													 <spring:message code="workflow.entries"/>
												</c:otherwise>
											</c:choose>
										</c:when>
										<c:otherwise>
											${command.contact.firstName} &nbsp;
											<spring:message code="workflow.has"/>&nbsp;${command.upcomingwork.size()}&nbsp;<spring:message code="workflow.upcomingworkof"/>&nbsp;
											<c:choose>
												<c:when test="${command.upcomingwork.size() == 1}"> 
													<spring:message code="workflow.entry"/>
												</c:when>
												<c:otherwise>
													<spring:message code="workflow.entries"/>
												</c:otherwise>
											</c:choose>
										</c:otherwise>
									</c:choose>
									
									<a href="viewupcomingwork.htm" class="mainlink-float"><spring:message code="workflow.viewupcomingworkcalendar"/></a>	
								</td>
							</tr>
							<tr>
								<th class="comp" scope="col"><spring:message code="company"/></th>
								<th class="desc" scope="col"><spring:message code="workflow.titledescription"/></th>
								<th class="start" scope="col"><spring:message code="workflow.startdate"/></th>
								<th class="due" scope="col"><spring:message code="duedate"/></th>
							</tr>
						</thead>
						<tbody>
							<c:choose>
								<c:when test="${command.upcomingwork.size() > 0}">
								<c:forEach var="uw" items="${command.upcomingwork}">
									<tr>
										<td class="comp"><links:companyLinkDWRInfo company="${uw.comp}" rowcount="${globalRowcount}" copy="${false}"/></td>
										<td class="desc"><strong>${uw.title}</strong><br />${uw.description}</td>
										<td class="start">
											<c:choose>
												<c:when test="${not empty uw.startDate}">
												<fmt:formatDate value="${uw.startDate}" type="date" dateStyle="SHORT"/> 		
												</c:when>
												<c:otherwise>
													-
												</c:otherwise>
											</c:choose>
										</td>
										<td class="due"><fmt:formatDate value="${uw.dueDate}" type="date" dateStyle="SHORT"/></td>
									</tr>
									<c:set var="globalRowcount" value="${globalRowcount + 1}"/>
								</c:forEach>
								</c:when>
								<c:otherwise>
								<tr>
									<td colspan="4" class="text-center bold">
										<c:choose>
											<c:when test="${currentContact.personid == command.contact.personid}">
												<spring:message code="workflow.youhavenoupcomingworkentries"/>
											</c:when>
											<c:otherwise>
												${command.contact.firstName}&nbsp;<spring:message code="workflow.hasnoupcomingworkentries"/>
											</c:otherwise>
										</c:choose>
									</td>
								</tr>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>										
					
				</div>
				<!-- end of user upcoming work tab -->
				
			</div>
			<!-- end of div to hold all tabbed submenu content -->
			
		</div>
		<!-- end of div to hold tabbed submenu -->
		
		<!-- this div clears floats and restores page flow -->
		<div class="clear"></div>
		
	</div>
	<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>