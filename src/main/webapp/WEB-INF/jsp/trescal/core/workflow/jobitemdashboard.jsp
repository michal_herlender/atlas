<%-- File name: /trescal/core/workflow/jobitemdashboard.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<spring:message code="workflow.viewworkfordepartmenttypes" />
		</span>
	</jsp:attribute>
	<jsp:body>
		<!-- this infobox contains all work progress for department types -->
		<div class="infobox" id="workForDeptType">
			<!-- div to hold all the tabbed submenu -->
			<div id="tabmenu">
				<!-- solves bug in IE where bar under subnav dissappears -->	
				<div>&nbsp;</div>
				<!-- tabbed submenu to change between departments -->	
				<ul class="subnavtab">
					<c:forEach var="department" items="${workByDepartment.entrySet()}">
						<li>
							<spring:message code="${department.key.messageCode}" />
						</li>
					</c:forEach>
				</ul>
			</div>
		</div>
	</jsp:body>
</t:crocodileTemplate>