<%-- File name: /trescal/core/workflow/allocateitemstoengineer.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel"%>
<%@ taglib prefix="workflow" tagdir="/WEB-INF/tags/workflow"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/workflow/AllocateItemsToEngineer.js'></script>
		<script>
			var menuElements2 = new Array();
			<c:forEach var="dept" items="${items.keySet()}">
				menuElements2.push({ anchor: '${dept.deptid}-link', block: '${dept.deptid}-tab'} );
			</c:forEach>
		</script>
	</jsp:attribute>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="workflow.allocateworktoengineers" /></span>
	</jsp:attribute>
	<jsp:body>
		<c:set var="showUnallocated" value="${items.keySet().size() > 0}" />
		<div id="subnav">
			<dl>
				<c:if test="${showUnallocated}">
					<dt>
						<a href="#"
							onclick="event.preventDefault(); switchMenuFocus(menuElements, 'unallocated-tab', false);"
							id="unallocated-link" class="selected"><spring:message
								code="workflow.unallocated" /></a>
					</dt>
				</c:if>
				<dt>
					<a href="#"
						onclick="event.preventDefault(); switchMenuFocus(menuElements, 'allocated-tab', true);"
						id="allocated-link"
						<c:if test="${!showUnallocated}">class="selected"</c:if>><spring:message
							code="workflow.allocated" /></a>
				</dt>
			</dl>
		</div>
		<div class="infobox" id="workallocation">
			<c:if test="${showUnallocated}">
				<div id="unallocated-tab">
					<div id="subnav">
						<dl>
							<c:forEach var="dept" items="${items.keySet()}" varStatus="loopStatus">
								<c:set var="selectedLink" value="" />
								<c:if test="${loopStatus.first}">
									<c:set var="selectedLink" value="class='selected'" />
								</c:if>
								<dt>
									<a href="#"
										onclick="event.preventDefault(); switchMenuFocus(menuElements2, '${dept.deptid}-tab', false);"
										id="${dept.deptid}-link"
										${selectedLink}>
									${dept.name} (${items.get(dept).size()} jobs)
								</a>
								</dt>
							</c:forEach>
						</dl>
					</div>
					<%-- We have an outer (dept) loop and inner (job) loop; this maintains a unique counter for each row (job) displayed --%>
					<c:set var="rowcount" value="0" />
					<c:forEach var="dept" items="${items.keySet()}" varStatus="deptLoopStatus">
						<c:set var="canAllocate" value="${authorisations.get(dept.deptid)}" />
						<c:set var="groupedJobItems" value="${items.get(dept)}" />
						<div id="${dept.deptid}-tab"
							<c:if test="${!deptLoopStatus.first}">class="hid"</c:if>>
							<c:choose>
								<c:when test="${groupedJobItems.keySet().size() == 0}">
									<table class="default4" summary='<spring:message code="workflow.tablenoongoingitems"/>'>
										<tbody>
											<tr>
												<td class="bold center">
													<spring:message code="noresults"/>
												</td>
											</tr>
										</tbody>
									</table>
								</c:when>
								<c:otherwise>
									<c:forEach var="job" items="${groupedJobItems.keySet()}">
										<table class="default4" summary="<spring:message code='workflow.tablejobwithongoingitems'/>">
											<tbody>
												<tr>
													<td class="wfjobno"><links:jobnoLinkDWRInfo rowcount="${rowcount}" jobno="${job.jobNo}" jobid="${job.jobId}" copy="${false}"/>
													<td class="wfconame"><links:companyLinkInfo companyId="${job.clientCompanyId}" onStop="${job.clientCompanyOnStop}" rowcount="${rowcount}" companyName="${job.clientCompanyName}" active="${job.clientCompanyActive}" copy="${false}"/></td>
													<td class="wfcontact"><links:contactLinkInfo contactName="${job.clientContactName}" personId="${job.clientContactId}" active="${job.clientContactActive}" rowcount="${rowcount}" /></td>
													<td class="wfstate">${job.jobStatus}</td>
													<td class="wfclientref">${job.transportOutName}</td>
													<td class="wfdatein"><fmt:formatDate value="${job.regDate}" type="date" dateStyle="SHORT" /></td>
												</tr>
												<tr>
													<td colspan="6" style="padding: 0;">
														<table class="default4 child-table" style="border: 0; cellpadding: 0; cellspacing: 0;" summary='<spring:message code="workflow.tableongoingitemswithinajob"/>'>
															<thead>
																<tr>
																	<th class="wfitem" scope="col"><spring:message code="item"/></th>
																	<th class="wfinstr" scope="col"><spring:message code="instmodelname"/></th>
																	<th class="wfserial" scope="col"><spring:message code="serialno"/></th>
																	<th class="wfplant" scope="col"><spring:message code="plantno"/></th>
																	<th class="wfcaltype" scope="col"><spring:message code="servicetype"/></th>
																	<th class="wfproc" scope="col"><spring:message code="capability"/></th>
																	<th class="wfstatus" scope="col"><spring:message code="status"/></th>
																	<th class="wfduedate" scope="col"><spring:message code="duedate"/></th>
																	<th class="wfallocate" scope="col"><spring:message code="workflow.allocated"/></th>
																</tr>
															</thead>
															<tbody>
																<c:forEach var="jobItem" items="${groupedJobItems.get(job)}">
																	<tr class="child-row state-${jobItem.stateId}- proc-${jobItem.procedureId}- csr-${jobItem.contractReviewerId}-" style="background-color: <c:choose><c:when test="${jobItem.turn <= fastTrackTurn}">${jobItem.displayColourFastTrack}</c:when><c:otherwise>${jobItem.displayColour}</c:otherwise></c:choose>">
																		<td><links:jobitemLinkInfo jobItemNo="${jobItem.jobItemNo}" jobItemCount="${jobItem.jobItemCount}" jobItemId="${jobItem.jobItemId}" defaultPage="jiactions.htm"/></td>
																		<td>${jobItem.modelName}</td>
																		<td>${jobItem.serialNo}</td>
																		<td>${jobItem.plantNo}</td>
																		<td class="center">${jobItem.serviceTypeName}</td>
																		<td class="center">${jobItem.procedureReference}</td>
																		<td>
																			<c:if test="${jobItem.lastActionRemark != null}">
																				<fmt:formatDate var="date" value="${jobItem.lastActionEndStamp}" type="both" dateStyle="SHORT" timeStyle="SHORT" />
																				<img src="img/icons/information.png" width="16" height="16" class="img_marg_bot" title="${date} - <c:out value='${jobItem.lastActionRemark}'/>"/>
																			</c:if>
																			${jobItem.stateName}
																		</td>
																		<td class="center"><fmt:formatDate value="${jobItem.dueDate}" type="date" dateStyle="SHORT"/></td>
																		<td class="center">
																			<c:if test="${canAllocate}">
																				<cwms:securedJsLink id="linkAlloc-${jobItem.jobItemId}" permission="ALLOCATE_ITEM_TO_DEPARTMENT" classAttr="mainlink" onClick="event.preventDefault(); displayAllocationOptions(${jobItem.jobItemId}, ${dept.deptid});">
																					<spring:message code="workflow.alloc"/>
																				</cwms:securedJsLink>
																			</c:if>
																		</td>
																	</tr>
																</c:forEach>
															</tbody>
														</table>
													</td>
												</tr>
											</tbody>
										</table>
										<c:set var="rowcount" value="${rowCount + 1}" />
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</div>
					</c:forEach>
				</div>	
			</c:if>
			<div id="allocated-tab" <c:if test="${showUnallocated}">class="hid"</c:if>>
				<table style="width: 100%;" class="default4">
					<c:forEach var="date" items="${allocatedDtos.keySet()}">
						<c:set var="allocatedForDate" value="${allocatedDtos.get(date)}" />
						<tr>
							<th width="50%" style="vertical-align: top;">
								<fmt:formatDate value='${date}' type="date" dateStyle="SHORT" /><c:out value=" AM" /></th>
							<th width="50%" style="vertical-align: top;">PM</th>
						</tr>
						<tr>
							<td>
								<c:forEach var="eaDto" items="${allocatedForDate.get(timeOfDayAM)}">
									<workflow:engineerAllocationDto canEdit="${authorisations.get(eaDto.deptId)}" eaDto="${eaDto}" />
								</c:forEach>
							</td>
							<td>
								<c:forEach var="eaDto" items="${allocatedForDate.get(timeOfDayPM)}">
									<workflow:engineerAllocationDto canEdit="${authorisations.get(eaDto.deptId)}" eaDto="${eaDto}" />
								</c:forEach>
							</td>
						</tr>
					</c:forEach>
				</table>
			</div>
			<div style="text-align: center;">
				<div class="clear">&nbsp;</div>
				<form:form method="post" action="">
					<div id="itemAllocations"></div>
					<input type="submit" value="<spring:message code='save'/>" />
				</form:form>
			</div>
		</div>
	</jsp:body>
</t:crocodileTemplate>