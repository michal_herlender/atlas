<%-- File name: /trescal/core/workflow/edit/edithook.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="workflow" tagdir="/WEB-INF/tags/workflow/edit"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<c:choose>
				<c:when test="${not empty lastHook}">Edit Hook</c:when>
				<c:otherwise>Create Hook</c:otherwise>
			</c:choose>
		</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script>
			// variables needed for tracking insertion points for new inputs
			var insertionHookActivity = ${form.hookActivityDtos.size()};
		</script>
		<script src='script/trescal/core/workflow/edit/EditHook.js'></script>
		<script src='script/trescal/core/workflow/edit/EditItemStateFunctions.js'></script>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox workfloweditor">
			<%@include file="../workfloweditor_header.jspf" %>
			<form:form modelAttribute="form" action="" method="post">
				<t:showErrors path="form.*" showFieldErrors="true" />
				<fieldset class="relative">
					<legend>
						<c:choose>
							<c:when test="${not empty lastHook}">Selected Hook: <workflow:showHook hook="${lastHook}"/></c:when>
							<c:otherwise>New Hook:</c:otherwise>
						</c:choose>
					</legend>
					<div>
						<ol>												
							<li>
								<label>Hook ID:</label>
								<span>
									<c:choose>
										<c:when test="${not empty lastHook}">${lastHook.id}</c:when>
										<c:otherwise>N/A - New Hook</c:otherwise>
									</c:choose>
								</span>
							</li>																								
							<li>
								<label>Active:</label>
								<span><form:checkbox path="active" />(Hook will only operate when checked)</span>
							</li>
							<li>
								<label>Always Possible:</label>
								<span><form:checkbox path="alwaysPossible" />(Hook will update active job items in any state - if unchecked, enter hook activities below)</span>
							</li>
							<li>
								<label>Begin Activity:</label>
								<span><form:checkbox path="beginActivity" />(Whether the Hook implementation begins an activity - normally true)</span>
							</li>
							<li>
								<label>Complete Activity:</label>
								<span><form:checkbox path="completeActivity" />(Whether the Hook should complete the activity - otherwise activity is left open)</span>
							</li>
							<li>
								<label>Default Activity Performed:</label>
								<span><form:select items="${itemActivities}" path="defaultActivityId" itemLabel="value" itemValue="key" />(Mandatory if 'Always Possible' is checked)</span>
							</li>
							<li>
								<label>Name:</label>
								<span><form:input path="name" size="50" />(Must match definition in codebase)</span>
							</li>
						</ol>
					</div>
				</fieldset>
				<!-- Hook Activities - can be added/editied for any hook -->
				<div id="section-hook-activity">
					<table class="default2">
						<thead>
							<tr>
								<th colspan="6">Hook Activities: (Optional if 'Always Possible' is checked)</th>
							</tr>
							<tr>
								<td>Begin Activity</td>
								<td>Complete Activity</td>
								<td>Start State</td>
								<td>Activity Performed</td>
								<td>Delete</td>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${form.hookActivityDtos}" varStatus="rowStatus">
								<tr>
									<td>
										<form:hidden path="hookActivityDtos[${rowStatus.index}].id" />
										<form:select path="hookActivityDtos[${rowStatus.index}].beginActivity" items="${selectBoolean}" itemLabel="value" itemValue="key" />
									</td>
									<td><form:select path="hookActivityDtos[${rowStatus.index}].completeActivity" items="${selectBoolean}" itemLabel="value" itemValue="key" /></td>
									<td>
										<form:select path="hookActivityDtos[${rowStatus.index}].statusOrActivity" items="${selectStatusActivity}" itemLabel="value" itemValue="key" onChange=" toggleHookActivity(this); " />
										<c:choose>
											<c:when test="${form.hookActivityDtos[rowStatus.index].statusOrActivity}">
												<c:set var="innerstatusVis" value="vis" />
												<c:set var="inneractivityVis" value="hid" />
											</c:when>
											<c:otherwise>
												<c:set var="innerstatusVis" value="hid" />
												<c:set var="inneractivityVis" value="vis" />
											</c:otherwise>
										</c:choose>
										<form:select path="hookActivityDtos[${rowStatus.index}].startStatusId" items="${itemStatuses}" itemLabel="value" itemValue="key" class="innerstatus ${innerstatusVis}" />
										<form:select path="hookActivityDtos[${rowStatus.index}].startActivityId" items="${itemActivities}" itemLabel="value" itemValue="key" class="inneractivity ${inneractivityVis}" />
									</td>
									<td>
										<form:select path="hookActivityDtos[${rowStatus.index}].finishActivityId" items="${itemActivities}" itemLabel="value" itemValue="key" />
									</td>
									<td><form:checkbox path="hookActivityDtos[${rowStatus.index}].delete" /></td>
								</tr>
							</c:forEach>
							<tr id="templaterow-ha" class="hid">
								<td>
									<form:hidden id="template-ha-id" path="templateDto.id" />
									<form:select id="template-ha-beginActivity" path="templateDto.beginActivity" items="${selectBoolean}" itemLabel="value" itemValue="key" />
								</td>
								<td><form:select id="template-ha-completeActivity" path="templateDto.completeActivity" items="${selectBoolean}" itemLabel="value" itemValue="key" /></td>
								<td>
									<form:select id="template-ha-statusOrActivity" path="templateDto.statusOrActivity" items="${selectStatusActivity}" itemLabel="value" itemValue="key" onChange=" toggleHookActivity(this); " />
									<c:choose>
										<c:when test="${form.templateDto.statusOrActivity}">
											<c:set var="innerstatusVis" value="vis" />
											<c:set var="inneractivityVis" value="hid" />
										</c:when>
										<c:otherwise>
											<c:set var="innerstatusVis" value="hid" />
											<c:set var="inneractivityVis" value="vis" />
										</c:otherwise>
									</c:choose>
									<form:select id="template-ha-startStatusId" path="templateDto.startStatusId" items="${itemStatuses}" itemLabel="value" itemValue="key" class="innerstatus ${innerstatusVis}" />
									<form:select id="template-ha-startActivityId" path="templateDto.startActivityId" items="${itemActivities}" itemLabel="value" itemValue="key" class="inneractivity ${inneractivityVis}" />
								</td>
								<td>
									<form:select id="template-ha-finishActivityId" path="templateDto.finishActivityId" items="${itemActivities}" itemLabel="value" itemValue="key" />
								</td>
								<td>
									<input type="hidden" id="template-ha-delete" value="false"/> 
									<img src="img/icons/delete.png" height="20" width="20" alt="Delete" onClick=" deleteInputRow(this); " />
								</td>
							</tr>
							<tr>
								<td colspan="5">
									<div class="float-left"><img src="img/icons/add.png" height="20" width="20" alt="Add Hook Activity" onClick=" addHookActivityRow(); " /></div>
									<div class="padtop"><span>&nbsp;Add Hook Activity</span></div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- END Hook Activities -->
				<input type="submit" value="<spring:message code='save' />" />
			</form:form>
		</div>		
	</jsp:body>
</t:crocodileTemplate>