<%-- File name: /trescal/core/workflow/workfloweditor_activity.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="workflow" tagdir="/WEB-INF/tags/workflow/edit"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%--
	Parameters returned in model for ItemActivity
	
		@param ItemActivity command.startActivity
		*** Will be present if the activity has a lookup associated ***
		@param LookupWrapper startSateLookup
		*** In addition one of the three may be optionally present allowing navigation *** 
		@param ItemStatus command.lastStatus - optional
		@param ItemActivity command.lastActivity - optional
		@param LookupInstance command.lastLookup - optional
--%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="workflow.workfloweditor"/></span>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox workfloweditor">
			<%@include file="workfloweditor_header.jspf" %>
			<fieldset class="relative">
				<legend>Selected Activity: <workflow:showActivity itemActivity="${command.startActivity}"/></legend>
				<div class="displaycolumn">
					<ol>												
						<li>
							<label>State ID:</label>
							<span>${command.startActivity.stateid}</span>
							<a href="edititemactivity.htm?id=${command.startActivity.stateid}" class="mainlink">(Edit Activity Definition)</a>
						</li>																								
						<li>
							<label>Description:</label>
							<span>${command.startActivity.description}</span>
						</li>
						<li>
							<label>Type:</label>
							<span>
								${command.startActivity.getClass().getSimpleName()}
							</span>
						</li>
						<li>
							<label>Characteristic(s):</label>
							<span>
								<c:if test="${command.startActivity.isOverride()}">Override<br/></c:if>
								<c:if test="${command.startActivity.isProgress()}">Progress<br/></c:if>
								<c:if test="${command.startActivity.isStatus()}">Status<br/></c:if>
							</span>
						</li>
						<li>
							<label>Legend:</label>
							<span class="activity">Activity - Green</span>
							<span class="status">Status - Blue</span>
							<span class="lookup">Lookup - Red</span>
						</li>
					</ol>
				</div>
				<div class="displaycolumn">
					<ol>												
						<li>
							<label>Retired:</label>
							<span><spring:message code="${command.startActivity.retired}"/></span>
						</li>																								
						<li>
							<label>Active:</label>
							<span><spring:message code="${command.startActivity.active}"/></span>
						</li>
						<li>
							<label># Action Outcomes:</label>
							<span>${command.startActivity.actionOutcomes.size()}</span>
						</li>
						<li>
							<label># Outcome Statuses:</label>
							<span>${command.startActivity.outcomeStatuses.size()}</span>
						</li>
						<li>
							<label>Action Outcome Type:</label>
							<span>
								<c:choose>
									<c:when test="${not empty command.startActivity.actionOutcomeType}">
										<c:out value="${command.startActivity.actionOutcomeType}"/>
									</c:when>
									<c:otherwise>
										Not Defined
									</c:otherwise>
								</c:choose>
							</span>
						</li>
					</ol>
				</div>
			</fieldset>
			<fieldset>
				<workflow:tableItemStateTranslations itemState="${command.startActivity}" />
			</fieldset>
			<fieldset>
				<workflow:tableStateGroupLinks itemState="${command.startActivity}" />
			</fieldset>
			<fieldset class="relative">
				<legend>Connected To</legend>
				<div>
					<ol>												
						<li>
							<label>Connection Type:</label>
							<c:choose>
								<c:when test="${command.startActivity.lookup != null}">Lookup</c:when>
								<c:otherwise>Status</c:otherwise>
							</c:choose>
						</li>
						<c:choose>
							<c:when test="${command.startActivity.lookup != null}">
								<li>
									<label>Connected Lookup:</label>
									<workflow:linkLookup lastItemActivity="${command.startActivity}" lookupInstance="${command.startActivity.lookup}"/>
								</li>
							</c:when>
							<c:otherwise>
								<li>
									<label>Connected Status:</label>
									<%-- Should be first status --%>
									<c:choose>
										<c:when test="${command.startActivity.outcomeStatuses.size() == 1}">
											<c:set var="outcomeStatus" value="${command.startActivity.outcomeStatuses.first()}" />
											<workflow:linkItemStatus lastItemActivity="${command.startActivity}" itemStatus="${outcomeStatus.status}"/>
										</c:when>
										<c:otherwise>
											<c:out value="Error - ${command.startActivity.outcomeStatuses} outcome statuses connected"/>
										</c:otherwise>
									</c:choose>
								</li>
							</c:otherwise>
						</c:choose>
					</ol>
				</div>
			</fieldset>

			<table class="default2">
				<thead>
					<tr>
						<th colspan="6">
							<c:out value="Action Outcomes: "/>
							<c:if test="${not empty command.startActivity.actionOutcomes}">
								<a href="editactionoutcomes.htm?id=${command.startActivity.stateid}">
									(Edit Translations)
								</a>
							</c:if>
						</th>
					</tr>
					<tr>
						<td>Active</td>
						<td>Default</td>
						<td>Positive</td>
						<td>Description</td>
						<td>ID</td>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="ao" items="${command.startActivity.actionOutcomes}">
						<tr>
							<td><spring:message code="${ao.active}" /></td>
							<td><spring:message code="${ao.defaultOutcome}" /></td>
							<td><spring:message code="${ao.positiveOutcome}" /></td>
							<td>${ao.description}</td>
							<td>${ao.id}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</jsp:body>
</t:crocodileTemplate>