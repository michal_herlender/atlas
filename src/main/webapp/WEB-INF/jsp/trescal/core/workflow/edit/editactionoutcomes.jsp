<%-- File name: /trescal/core/workflow/edit/editactionoutcomes.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="workflow" tagdir="/WEB-INF/tags/workflow/edit"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">Edit Action Outcome Translations</span>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox workfloweditor">
			<%@include file="../workfloweditor_header.jspf" %>
			<form:form modelAttribute="form" action="" method="post">
				<t:showErrors path="form.*" showFieldErrors="true" />
				<fieldset class="relative">
					<legend>
							Selected Activity: <workflow:showActivity itemActivity="${lastActivity}"/>
					</legend>
					<div>
						<ol>												
							<li>
								<label>State ID:</label>
								<span>${lastActivity.stateid}</span>
							</li>
						</ol>
					</div>
				</fieldset>
				<table class="default2">
					<thead>
						<tr>
							<th colspan="6">Action Outcomes:</th>
						</tr>
						<tr>
							<td>Description</td>
							<td>Translations</td>
							<td>ID</td>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="ao" items="${lastActivity.actionOutcomes}">
							<tr>
								<td>${ao.description}</td>
								<td>
									<c:forEach var="locale" items="${supportedLocales}">
										<label><c:out value="${locale.getDisplayLanguage(rc.locale)} (${locale.getDisplayCountry(rc.locale)}): "/></label>
										<form:input path="aoIdsToTranslations[${ao.id}][${locale}]" size="50"/>
										<br/>
										<div class="clear"></div>
									</c:forEach>
								</td>
								<td>${ao.id}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<!-- END Action Outcomes -->
				<input type="submit" value="<spring:message code='save' />" />
			</form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>