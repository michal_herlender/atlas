<%@ taglib prefix="jobs"
           tagdir="/WEB-INF/tags/jobs" %><%-- File name: /trescal/core/workflow/viewworkfordepttype.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>

<script type='text/javascript' src='script/trescal/core/workflow/ViewWorkForDeptType.js'></script>

<script type="text/javascript">
    var menuElements = [];
    <c:forEach var="deptType" items="${model.works.keySet()}">
    menuElements.push({anchor: 'dept${deptType}-link', block: 'dept${deptType}-tab'});
    </c:forEach>
</script>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="workflow.viewworkfordepartmenttypes"/></span>
	</jsp:attribute>
    <jsp:body>
        <c:set var="colspanVal" value="${model.workDays.size() + 1}"/>
        <c:set var="dayWidths" value="${80 / model.workDays.size()}"/>
        <!-- this infobox contains all work progress for department types -->
        <div class="infobox" id="workForDeptType">
            <!-- div to hold all the tabbed submenu -->
            <div id="tabmenu">
                <!-- solves bug in IE where bar under subnav dissappears -->
                <div>&nbsp;</div>
                <!-- tabbed submenu to change between departments -->
                <ul class="subnavtab">
                    <c:set var="first" value="true"/>
                    <c:forEach var="deptType" items="${model.works.keySet()}">
                        <li>
                            <a href="#" id="dept${deptType}-link"
                               onclick="event.preventDefault(); switchMenuFocus(menuElements, 'dept${deptType}-tab', false);"
                               <c:if test="${first}">class="selected"</c:if>>
                                <c:choose>
                                    <c:when test="${model.depttypes.get(deptType) == null}">
                                        <spring:message code='thirdparty'/>
                                    </c:when>
                                    <c:otherwise>
                                        <spring:message code="${model.depttypes.get(deptType).messageCode}"/>
                                    </c:otherwise>
                                </c:choose>
                            </a>
                        </li>
                        <c:set var="first" value="false"/>
                    </c:forEach>
                </ul>
                <!-- div to hold all content which is to be changed using the tabbed submenu -->
                <div class="tab-box">
                    <c:set var="first" value="vis"/>
                    <c:forEach var="deptType" items="${model.works.keySet()}">
                        <div id="dept${deptType}-tab" class="${first}">
                            <c:choose>
                                <%-- Display of "procedures" department type (i.e. Laboratory table) --%>
                                <c:when test="${model.depttypes.get(deptType).procedures}">
                                    <table class="default2">
                                        <thead>
                                        <tr>
                                            <th class="labstatus">
													<span class="larger-text">
														<spring:message code="workflow.workschedule"/>
													</span>
                                            </th>
                                            <c:forEach var="day" items="${model.days.keySet()}">
                                                <th style=" width: ${dayWidths}%; text-align: center; ">
                                                    <span class="larger-text"><spring:message code="${day}"/></span>
                                                </th>
                                            </c:forEach>
                                        </tr>
                                        </thead>
                                    </table>
                                    <c:forEach var="dept"
                                               items="${model.works.get(deptType).workByDepartmentMap.keySet()}">
                                        <table class="default4">
                                            <tbody>
                                            <tr>
                                                <th colspan="${colspanVal}">
                                                    <c:choose>
                                                        <c:when test="${model.depts.containsKey(dept)}">
                                                            <span class="larger-text"><em>${model.depts.get(dept).name}</em></span>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <em><spring:message code="workflow.uncategorised"/></em>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </th>
                                            </tr>
                                            <c:if test="${model.works.get(deptType).workByDepartmentMap.get(dept).total != null}">
                                                <spring:message code="workflow.laboratorytotal" var="label"/>
                                                <jobs:workAssigmentLaboratoryRow label="${label}" labelClass="bold"
                                                                                 countersRow="${model.works.get(deptType).workByDepartmentMap.get(dept).total}"
                                                                                 workDays="${model.workDays}"
                                                                                 labCategorySplit="NONE"
                                                                                 urlParameterPart="deptid=${dept}"/>
                                            </c:if>
                                            <c:forEach var="pc"
                                                       items="${model.works.get(deptType).workByDepartmentMap.get(dept).workByCategory}">
                                                <c:if test="${model.proccats.containsKey(pc.key._1())}">
                                                    <jobs:workAssigmentLaboratoryRow
                                                            label="${model.proccats.get(pc.key._1()).name}"
                                                            labelClass="italic"
                                                            countersRow="${pc.value}"
                                                            workDays="${model.workDays}"
                                                            urlParameterPart="proccatid=${pc.key._1()}"
                                                            labCategorySplit="${pc.key._2()}"/>
                                                </c:if>
                                            </c:forEach>
                                            </tbody>
                                        </table>
                                    </c:forEach>

                                    <c:if test="${model.works.get(deptType).uncategorised != null}">
                                        <table class="default4">
                                            <tbody>
                                            <tr>
                                                <th colspan="${colspanVal}">
                                                    <em><spring:message code="workflow.uncategorised"/></em>
                                                </th>
                                            </tr>
                                            <spring:message code="workflow.uncategorised" var="label"/>
                                            <jobs:workAssigmentLaboratoryRow label="${label}" labelClass="bold"
                                                                             countersRow="${model.works.get(deptType).uncategorised}"
                                                                             workDays="${model.workDays}"
                                                                             urlParameterPart="deptid=-1"
                                                                             labCategorySplit="NONE"/>
                                            </tbody>
                                        </table>
                                    </c:if>
                                    <%--                                    SUMMARY--%>
                                    <table class="default2">
                                        <thead>
                                        <c:set var="dayWidths" value="${80 / model.workDays.size()}"/>
                                        <tr>
                                            <th class="labstatus">
													<span class="larger-text">
														<spring:message code="workflow.summary"/>
													</span>
                                            </th>
                                            <c:forEach var="day" items="${model.workDays}">
                                                <th style=" width: ${dayWidths}%; text-align: center; ">
                                                    <span class="larger-text">${model.works.get(deptType).summary.get(day)}</span>
                                                </th>
                                            </c:forEach>
                                        </tr>
                                        </thead>
                                    </table>
                                    <%--                                    SUMMARY--%>
                                </c:when>
								<%-- Display of non-procedure department state groups --%>
								<c:otherwise>
                                    <c:choose>
                                        <c:when test="${model.works.get(deptType).stateGroups.keySet().size() == 0}">
                                            <table class="default2">
                                                <thead>
                                                <tr>
                                                    <th><span class="larger-text">
															<spring:message
                                                                    code="${model.depttypes.get(deptType).messageCode}"/>
														</span></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td class="bold center">
                                                        <spring:message
                                                                code="workflow.therearenostatussassignedtothisdepartmenttype"/>
                                                    </td>
                                                </tr>
                                                </tbody>
											</table>
										</c:when>
                                        <c:otherwise>
                                            <c:forEach var="group"
                                                       items="${model.works.get(deptType).stateGroups.keySet()}">
                                                <table class="default2">

                                                    <thead>
                                                    <tr>
                                                        <th class="status"><span class="larger-text">
                                                                ${model.stategroups.get(group)}
                                                        </span></th>
                                                        <th class="items">
                                                            <a class="mainlink larger-text"
                                                               href="viewselectedjobitems.htm?groupid=${group}">
                                                                    ${model.works.get(deptType).stateGroups.get(group).total}
                                                            </a>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <c:forEach var="status"
                                                               items="${model.works.get(deptType).stateGroups.get(group).counters.keySet()}">
                                                        <c:if test="${status != -1 && model.works.get(deptType).stateGroups.get(group).counters.get(status) != 0}">
                                                            <tr>
                                                                <td class="status">${model.statuses.get(status)}</td>
                                                                <td class="items">
                                                                    <a class="mainlink larger-text"
                                                                       href="viewselectedjobitems.htm?stateid=${status}&groupid=${group}">
                                                                            ${model.works.get(deptType).stateGroups.get(group).counters.get(status)}
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        </c:if>
                                                    </c:forEach>
                                                    </tbody>
                                                </table>
                                            </c:forEach>
                                        </c:otherwise>
									</c:choose>
								</c:otherwise>
							</c:choose>
						</div>
                        <c:set var="first" value="hid"/>
					</c:forEach>
				</div>
				<!-- end of div to hold all tabbed submenu content -->
			</div>
			<!-- end of div to hold tabbed submenu -->
			<!-- this div clears floats and restores page flow -->
			<div class="clear"></div>
		</div>
		<!-- end of infobox div -->
	</jsp:body>
</t:crocodileTemplate>