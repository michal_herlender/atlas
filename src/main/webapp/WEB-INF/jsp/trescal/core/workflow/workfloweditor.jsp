<%-- File name: /trescal/core/workflow/workfloweditor.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="workflow.workfloweditor"/></span>
	</jsp:attribute>
	<jsp:body>
<%--
	Parameters returned in model
	a) if showing activity/status selector only (starting point)
	
		@param List<ItemActivity> command.activities
		@param List<ItemStatus> command.statuses
--%>
		<fieldset>
			<legend><spring:message code="workflow.pleasechooseastatusoractivitytoviewedittheworkflowfor"/>:</legend>
			
			<ul>
				<li>
					<label class="usage bold">View/Edit Status:</label>
					<form:form id="statusform" method="get" action="workfloweditor_status.htm" modelAttribute="command">
						<select name="statusId" onchange=" if (this.value != '') $j('form#statusform').submit(); ">
							<option selected="selected" value="">-- <spring:message code="workflow.selectastatus"/> --</option>
							<optgroup label="-- HoldStatus --">
								<c:forEach var="status" items="${command.holdStatuses}">
									<option value="${status.stateid}">
										${status.stateid} - <cwms:besttranslation translations="${status.translations}"/>
									</option>
								</c:forEach>
							</optgroup>
							<optgroup label="-- WorkStatus --">
								<c:forEach var="status" items="${command.workStatuses}">
									<option value="${status.stateid}">
										${status.stateid} - <cwms:besttranslation translations="${status.translations}"/>
									</option>
								</c:forEach>
							</optgroup>
						</select>
					</form:form>
				</li>			
				<li>
					<label><a href="edititemstatus.htm" class="mainlink">Create New Status</a></label>
				</li>
				<li>
					<label class="usage bold">View/Edit Activity:</label>
					<form:form id="activityform" method="get" action="workfloweditor_activity.htm" modelAttribute="command">
						<select name="activityId" onchange=" if (this.value != '') $j('form#activityform').submit(); ">
							<option selected="selected" value="">-- <spring:message code="workflow.selectanactivity"/> --</option>
							<c:forEach var="activity" items="${command.activities}">
								<option value="${activity.stateid}">
									${activity.stateid} - <cwms:besttranslation translations="${activity.translations}"/>
								</option>
							</c:forEach>
						</select>
					</form:form>
				</li>			
				<li>
					<label><a href="edititemactivity.htm" class="mainlink">Create New Activity</a></label>
				</li>
				<li>
					<label class="usage bold">View/Edit Lookup:</label>
					<form:form id="lookupform" method="get" action="workfloweditor_lookup.htm" modelAttribute="command">
						<select name="lookupId" onchange=" if (this.value != '') $j('form#lookupform').submit(); ">
							<option selected="selected" value="">-- <spring:message code="workflow.selectalookup"/> --</option>
							<c:forEach var="lookup" items="${command.lookups}">
								<option value="${lookup.id}">
									${lookup.id} - ${lookup.description}
								</option>
							</c:forEach>
						</select>
					</form:form>
				</li>			
				<li>
					<label><a href="editlookupinstance.htm" class="mainlink">Create New Lookup</a></label>
				</li>
				<li>
					<label class="usage bold">View/Edit Hook:</label>
					<form:form id="hookform" method="get" action="workfloweditor_hook.htm" modelAttribute="command">
						<select name="hookId" onchange=" if (this.value != '') $j('form#hookform').submit(); ">
							<option selected="selected" value="">-- <spring:message code="workflow.selectahook"/> --</option>
							<c:forEach var="hook" items="${command.hooks}">
								<option value="${hook.id}">
									${hook.id} - ${hook.name}
								</option>
							</c:forEach>
						</select>
					</form:form>
				</li>			
				<li>
					<label><a href="edithook.htm" class="mainlink">Create New Hook</a></label>
				</li>
			</ul>
		</fieldset>
	</jsp:body>
</t:crocodileTemplate>