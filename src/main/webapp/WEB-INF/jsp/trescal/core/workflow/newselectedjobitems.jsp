<%-- File name: /trescal/core/workflow/viewselectedjobitems.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<script src="script/trescal/core/workflow/ViewSelectedJobItems.js"></script>
<style>
	.multiselect {
  		width: 200px;
	}

	.selectBox {
  		position: relative;
	}

	.selectBox select {
  		width: 100%;
  		font-weight: bold;
	}

	.overSelect {
  		position: absolute;
  		left: 0;
  		right: 0;
  		top: 0;
  		bottom: 0;
	}

	#checkboxes {
  		display : none;
  		border: 1px #dadada solid;
	}

	#checkboxes label {
  		display: block;
	}

	#checkboxes label:hover {
  		background-color: #1e90ff;
	}
</style>
<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="workflow.viewselectedjobitemsprogress"/></span>
	</jsp:attribute>
	<jsp:body>
		<!-- infobox contains all search results and is styled with nifty corners -->
		<div class="infobox" id="workflowresults">
			<div style=" width: 20.625em; float: left; ">
				<label style=" width: 120px; "><spring:message code="workflow.jobtype"/>:</label>
				<select id="jobTypeSelector" style="width: 235px;" onchange="filterJobs(); return false;">
					<option value="0"> -- <spring:message code="all"/> -- </option>
					<c:forEach var="jt" items="${jobTypes}">
						<c:set var="sel" value=""/>
						<c:if test="${currentContact.userPreferences.defaultJobType != null && currentContact.userPreferences.defaultJobType == jt}">
							<c:set var="sel" value="selected"/>
						</c:if>
						<option value="${jt}" <c:out value="${sel}"/>>${jt.description}</option>
					</c:forEach>
				</select>
				<div class="clear"></div>
				<label style=" width: 120px; "><spring:message code="workflow.transportout"/>:</label>
				<select id="transOptSelector" style="width: 235px;" onchange="filterJobs(); return false;">
					<option value="0"> -- <spring:message code="all"/> -- </option>
					<c:forEach var="transportOption" items="${model.transportOptions}">
						<option value="${transportOption.key}">${transportOption.value}</option>
					</c:forEach>
					<option value="-1"> -- <spring:message code="notapplicable"/> -- </option>
				</select>
				<div class="clear"></div>
				<label style=" width: 120px; "><spring:message code="company"/>:</label>
				<select id="companySelector" style="width: 235px;" onchange="filterJobs(); return false;">
					<option value="0"> -- <spring:message code="all"/> -- </option>
					<c:forEach var="clientCompany" items="${model.clientCompanies}">
						<option value="${clientCompany.key}">${clientCompany.value}</option>
					</c:forEach>
				</select>
				<div class="clear"></div>
				<label style=" width: 120px; "><spring:message code="subdiv"/>:</label>
				<select id="subdivSelector" style="width: 235px;" onchange="filterJobs(); return false;">
					<option value="0"> -- <spring:message code="all"/> -- </option>
					<c:forEach var="clientCompany" items="${model.clientCompanies}">
						<optgroup label="${clientCompany.value}" />
						<c:set var="clientSubdivs" value="${model.clientSubdivs.get(clientCompany.key)}" />
						<c:forEach var="clientSubdiv" items="${clientSubdivs}">
							<option value="${clientSubdiv.subdivid}">${clientSubdiv.subname}</option>
						</c:forEach>
					</c:forEach>
				</select>
			</div>
			<div style=" margin-left: 90px ; width: 20.625em; float: left;">
				<c:if test="${not empty model.itemStates}">
					<label style=" width: 140px; "><spring:message code="status"/>:</label>
					<select id="stateSelector" style="width: 235px;" onchange=" filterJobs(); return false; ">
						<option value="0"> -- <spring:message code="all"/> -- </option>
						<c:forEach var="state" items="${model.itemStates}">
							<option value="${state.key}">${state.value}</option>
						</c:forEach>
					</select>
					<div class="clear"></div>
				</c:if>
				<c:if test="${not empty model.capabilities}">
					<div>
						<label style=" width: 140px; "><spring:message code="capability"/>:</label>
						<select id="procSelector" style="width: 235px;" onchange="filterJobs(); return false;">
							<option value="0"> -- <spring:message code="all"/> -- &nbsp;&nbsp;&nbsp;</option>
							<c:forEach var="capability" items="${model.capabilities}">
								<option value="${capability.procId}">${capability.referenceAndName}</option>
							</c:forEach>
						</select>
					</div>
					<div class="clear"></div>
				</c:if>
				<div>
					<label style=" width: 140px; "><spring:message code="businesssubdivision"/>:</label>
					<select id="businessSubSelector" style="width: 235px;" onchange="filterJobs(); return false;">
						<option value="0"> -- <spring:message code="all"/> -- &nbsp;&nbsp;&nbsp;</option>
						<c:forEach var="subdiv" items="${model.businessSubdivs}">
							<option value="${subdiv.key}">${subdiv.value}</option>
						</c:forEach>
					</select>
				</div>
				<div class="clear"></div>
				<label style=" width: 140px; "><spring:message code="workflow.contractreviewby"/>:</label>
				<select id="csrSelector" style="width: 235px;" onchange="filterJobs(); return false;">
					<option value="0"> -- <spring:message code="all"/> -- </option>
					<c:forEach var="contractReviewer" items="${model.contractReviewers}">
						<option value="${contractReviewer.key}">${contractReviewer.value}</option>
					</c:forEach>
					<option value="-1"> -- <spring:message code="notapplicable"/> -- </option>
				</select>
				<div class="clear"></div>
				</div>
				<div class="multiselect" style="margin-top: -10px; margin-left: 678px;  width: 20%;">
					<label style=" width: 140px; "><spring:message code="servicetype"/>:</label>
    				<div class="selectBox" onclick="showCheckboxes()" style="width: 150px;">
      					<select style="width: 235px;">
      					</select>
      					<div class="overSelect"></div>
   					</div>
    				<div id="checkboxes" style="width:300px">
    					<label><input type="checkbox" checked value="${st.key}" id ="selectAll" onchange=" selectAll();  filterJobs(); return false;"/>
    						--<strong><spring:message code="certmanagement.selall"/></strong>--</label>
    					<c:forEach var="st" items="${model.serviceTypes}">
    						<label for="${st.key}"><input type="checkbox" checked name="serviceType" value="${st.key}" onchange="filterJobs(); return false;"/>${st.value}</label>
						</c:forEach>
    				</div>
  				</div>	
			<div class="clear"></div><br>
			<table class="default4" summary='<spring:message code="workflow.tableheadingsongoingjobitems"/>'>
				<thead>
					<tr>
						<td colspan="6">
							<spring:message code="jobitems"/> (<span id="itemCount"><c:out value="${model.jobItemCount}"/></span>)
							<div class="float-right">
								<form:form action="exportselectedjobitems.htm" name="exportform" id="exportform" method="post">
									<spring:message code="web.export.excel" var="submitText"/>
									<c:if test="${req.getParameter('deptid') != null}">
										<input type="hidden" name="deptid" value="${req.getParameter('deptid')}" />
									</c:if>  
									<c:if test="${req.getParameter('proccatid') != null}">
										<input type="hidden" name="proccatid" value="${req.getParameter('proccatid')}" />
									</c:if>  
									<c:if test="${req.getParameter('stateid') != null}">
										<input type="hidden" name="stateid" value="${req.getParameter('stateid')}" />
									</c:if>  
									<c:if test="${req.getParameter('groupid') != null}">
										<input type="hidden" name="groupid" value="${req.getParameter('groupid')}" />
									</c:if>
									<c:if test="${req.getParameter('days') != null}">
										<input type="hidden" name="days" value="${req.getParameter('days')}" />
									</c:if>  
									<c:if test="${req.getParameter('business') != null}">
										<input type="hidden" name="business" value="${req.getParameter('business')}" />
									</c:if> 
									<input type="submit" name="export" value="${submitText}">
								</form:form>
							</div>
						</td>
					</tr>											
					<tr>
						<th class="wfjobno" scope="col"><spring:message code="jobno"/></th>
						<th class="wfconame" scope="col"><spring:message code="company"/></th>
						<th class="wfsubdiv" scope="col"><spring:message code="subdiv"/></th>
						<th class="wfcontact" scope="col"><spring:message code="contact"/></th>
						<th class="wfdatein" scope="col"><spring:message code="workflow.datein"/></th>
						<th class="wftransout" scope="col"><spring:message code="workflow.transportout"/></th>
					</tr>
				</thead>
			</table>
			<c:choose>
				<c:when test="${empty model.jobDtos}">
					<table class="default4" summary='<spring:message code="workflow.tablenoongoingitems"/>'>
						<tbody>
							<tr>
								<td class="bold center">
									<spring:message code="noresults"/>
								</td>
							</tr>
						</tbody>
					</table>
				</c:when>
				<c:otherwise>
					<c:forEach var="jobDto" items="${model.jobDtos}" varStatus="jobLoopStatus">
						<table class="default4 parent-table company-${jobDto.contact.subdiv.company.coid}- subdiv-${jobDto.contact.subdiv.subdivid}- transOpt-${jobDto.transportOutId}- jobType-${jobDto.type}- businesssub-${jobDto.orgid}-">
							<tbody>
								<tr onclick="window.location.href='viewjob.htm?jobid=${jobDto.jobid}'" onmouseover="$j(this).addClass('hoverrow');" onmouseout="$j(this).removeClass('hoverrow');">
									<td class="wfjobno"><links:jobnoLinkDWRInfo rowcount="${jobLoopStatus.count}" jobno="${jobDto.jobno}" jobid="${jobDto.jobid}" copy="${false}"/>
									</br><spring:message code="jobexpenseitem.header"/><c:out value=": ${jobDto.jobExpenseItemCount}"/>
									<td class="wfconame"><links:companyLinkInfo companyId="${jobDto.contact.subdiv.company.coid}" onStop="${jobDto.contact.subdiv.company.onstop}" rowcount="${jobLoopStatus.count}" companyName="${jobDto.contact.subdiv.company.coname}" active="${jobDto.contact.subdiv.company.active}" copy="${false}"/></td>
									<td class="wfsubdiv"><c:out value="${jobDto.contact.subdiv.subname}"/></td>
									<td class="wfcontact"><links:contactLinkInfo contactName="${jobDto.contact.name}" personId="${jobDto.contact.personid}" active="${jobDto.contact.active}" rowcount="${jobLoopStatus.count}" /></td>
									<td class="wfdatein"><fmt:formatDate value="${jobDto.regDate}" type="date" dateStyle="SHORT" /></td>
									<td class="wftransout"><c:out value="${jobDto.transportOut.value}"/></td>
								</tr>
								
								<tr>
									<td colspan="6" style="padding: 0;">
										<table class="default4 child-table" style="border: 0; cellpadding: 0; cellspacing: 0;" summary='<spring:message code="workflow.tableongoingitemswithinajob"/>'>
											<thead>
												<tr>
													<th class="wfitem" scope="col"><spring:message code="item"/></th>
													<th class="wfinstr" scope="col"><spring:message code="instmodelname"/></th>
													<th class="wfserial" scope="col"><spring:message code="serialno"/></th>
													<th class="wfplant" scope="col"><spring:message code="plantno"/></th>
													<th class="wfcaltype" scope="col"><spring:message code="servicetype"/></th>
													<th class="wfproc" scope="col"><spring:message code="capability"/></th>
													<th class="wfstatus" scope="col"><spring:message code="status"/></th>
													<th class="wfduedate" scope="col"><spring:message code="duedate"/></th>
												</tr>
											</thead>
											<tbody>
											<%-- <tr>tesst</tr> --%>
												<c:forEach var="jiDto" items="${jobDto.jobitems}">
													<tr class="child-row state-${jiDto.stateId}- proc-${jiDto.capabilityId}- csr-${jiDto.contractReviewById}- st-${jiDto.serviceTypeId}-" style="background-color: <c:choose><c:when test="${jiDto.turn <= fastTrackTurn}">${jiDto.serviceType.displayColourFastTrack}</c:when><c:otherwise>${jiDto.serviceType.displayColour}</c:otherwise></c:choose>">
														<td><links:jobitemLinkInfo jobItemNo="${jiDto.itemno}" jobItemCount="${jobDto.jobItemCount}" jobItemId="${jiDto.jobItemId}" defaultPage="jiactions.htm"/></td>
														<td><c:out value="${jiDto.instrument.getInstrumentModelNameViaFields(false)}"/></td>
														<td><c:out value="${jiDto.instrument.serialno}"/></td>
														<td><c:out value="${jiDto.instrument.plantno}"/></td>
														<td class="center"><c:out value="${jiDto.serviceType.shortName}"/></td>
														<td class="center"><c:out value="${jiDto.capability.reference}"/></td>
														<td>
															<c:if test="${not empty jiDto.lastAction.remark}">
																<fmt:formatDate var="date" value="${jiDto.lastAction.startStamp}" type="both" dateStyle="SHORT" timeStyle="SHORT" />
																<img src="img/icons/information.png" width="16" height="16" class="img_marg_bot" title="${date} - <c:out value='${jiDto.lastAction.remark}'/>"/>
															</c:if>
															<c:out value="${jiDto.state.value}"/>
														</td>
														<td class="center"><fmt:formatDate value="${jiDto.dueDate}" type="date" dateStyle="SHORT" /></td>
													</tr>
													
														
												</c:forEach>
											</tbody>
										</table>
									</td>
								</tr>
							
							</tbody>
						</table>
					</c:forEach>
				</c:otherwise>
			</c:choose>
		</div>
		<!-- end of infobox div -->
	</jsp:body>
</t:crocodileTemplate>