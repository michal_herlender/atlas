<%-- File name: /trescal/core/workflow/edit/editlookupinstance.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="workflow" tagdir="/WEB-INF/tags/workflow/edit"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<c:choose>
				<c:when test="${not empty lastLookup}">Edit Lookup Instance</c:when>
				<c:otherwise>Create Lookup Instance</c:otherwise>
			</c:choose>
		</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script>
			var insertionOutcomeStatus = ${form.outcomeStatusDtos.size()};
			// variables needed for tracking number of lookup connections (current and maximum)
			var currentMessageCount = ${currentMessageCount};
			var maxMessageCount = ${maxMessageCount};
		</script>
		<script src='script/trescal/core/workflow/edit/EditLookupInstance.js'></script>
		<script src='script/trescal/core/workflow/edit/EditItemStateFunctions.js'></script>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox workfloweditor">
			<%@include file="../workfloweditor_header.jspf" %>
			<form:form modelAttribute="form" action="" method="post">
				<t:showErrors path="form.*" showFieldErrors="true" />
				<fieldset class="relative">
					<legend>
						<c:choose>
							<c:when test="${not empty lastActivity}">Selected Lookup: <workflow:showLookup lookupInstance="${lastLookup}"/></c:when>
							<c:otherwise>New Lookup Instance:</c:otherwise>
						</c:choose>
					</legend>
					<div>
						<ol>												
							<li>
								<label>Lookup ID:</label>
								<span>
									<c:choose>
										<c:when test="${not empty lastLookup}">${lastLookup.id}</c:when>
										<c:otherwise>N/A - New Lookup</c:otherwise>
									</c:choose>
								</span>
							</li>
							<li>
								<label>Lookup Function:</label>
								<form:select path="lookupFunction" items="${lookupFunctions}" onchange=" selectLookupFunction(this.value); "/>
							</li>
							<li>
								<label>Lookup Description:</label>
								<form:input path="description" size="50" />
							</li>
						</ol>
					</div>
				</fieldset>
				<!-- Lookup Information -->
				<div id="section-lookupresults">
					<c:if test="${not empty form.lookupFunction}">
						<workflow:tableLookupResults lookupFunction="${form.lookupFunction}"/>
					</c:if>
				</div>
				<!-- END Lookups -->
				<!-- Edit outcome statuses -->
				<div id="section-outcomestatus">
					<table class="default2">
						<thead>
							<tr>
								<th colspan="2">Outcome Statuses:</th>
								<th colspan="${maxMessageCount}">Wired To:</th>
								<th colspan="1"></th>
							</tr>
							<tr>
								<td>Connect To</td>
								<td>Lookup/Status</td>
								<c:forEach begin="0" end="${maxMessageCount-1}" varStatus="messageStatus">
									<td>${messageStatus.index}</td>
								</c:forEach>
								<td>Delete</td>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="dto" items="${form.outcomeStatusDtos}" varStatus="dtoStatus">
								<tr>
									<td>
										<form:hidden path="outcomeStatusDtos[${dtoStatus.index}].id" />
										<form:select path="outcomeStatusDtos[${dtoStatus.index}].lookupOrStatus" items="${selectLookupOrStatus}" itemLabel="value" itemValue="key" onChange=" toggleOutcomeStatus(this); " />
									</td>
									<td>
										<c:choose>
											<c:when test="${form.outcomeStatusDtos[dtoStatus.index].lookupOrStatus}">
												<c:set var="innerlookupVis" value="vis" />
												<c:set var="innerstatusVis" value="hid" />
											</c:when>
											<c:otherwise>
												<c:set var="innerlookupVis" value="hid" />
												<c:set var="innerstatusVis" value="vis" />
											</c:otherwise>
										</c:choose>
										<form:select path="outcomeStatusDtos[${dtoStatus.index}].lookupId" items="${lookups}" itemLabel="value" itemValue="key" class="innerlookup ${innerlookupVis}" />
										<form:select path="outcomeStatusDtos[${dtoStatus.index}].statusId" items="${itemStatuses}" itemLabel="value" itemValue="key" class="innerstatus ${innerstatusVis}" />
									</td>
									<c:forEach begin="0" end="${maxMessageCount-1}" varStatus="messageStatus">
										<c:choose>
											<c:when test="${messageStatus.index < currentMessageCount}">
												<c:set var="disabledText" value="false" />
											</c:when>
											<c:otherwise>
												<c:set var="disabledText" value="true" />
											</c:otherwise>
										</c:choose>
										<td><form:radiobutton class="os-selectedlookup" id="selectedlookup-${messageStatus.index}-${dtoStatus.index}" path="selectedLookups[${messageStatus.index}]" value="${dtoStatus.index}" disabled="${disabledText}"/></td>
									</c:forEach>
									<td><form:checkbox path="outcomeStatusDtos[${dtoStatus.index}].delete" /></td>
								</tr>
							</c:forEach>
							<tr id="templaterow-os" class="hid">
								<td>
									<form:hidden id="template-os-id" path="templateOutcomeStatus.id" />
									<form:select id="template-os-lookupOrStatus" path="templateOutcomeStatus.LookupOrStatus" items="${selectLookupOrStatus}" itemLabel="value" itemValue="key" onChange=" toggleOutcomeStatus(this); " />
								</td>
								<td>
									<c:choose>
										<c:when test="${form.templateOutcomeStatus.lookupOrStatus}">
											<c:set var="innerlookupVis" value="vis" />
											<c:set var="innerstatusVis" value="hid" />
										</c:when>
										<c:otherwise>
											<c:set var="innerlookupVis" value="hid" />
											<c:set var="innerstatusVis" value="vis" />
										</c:otherwise>
									</c:choose>
									<form:select id="template-os-lookupId" path="templateOutcomeStatus.lookupId" items="${lookups}" itemLabel="value" itemValue="key" class="innerlookup ${innerlookupVis}" />
									<form:select id="template-os-statusId" path="templateOutcomeStatus.statusId" items="${itemStatuses}" itemLabel="value" itemValue="key" class="innerstatus ${innerstatusVis}" />
								</td>
								<c:forEach begin="0" end="${maxMessageCount-1}" varStatus="messageStatus">
									<c:choose>
										<c:when test="${messageStatus.index < currentMessageCount}">
											<c:set var="disabledText" value="" />
										</c:when>
										<c:otherwise>
											<c:set var="disabledText" value="disabled='disabled'" />
										</c:otherwise>
									</c:choose>
									<td><input type="radio" class="os-selectedlookup" id="template-os-selectedlookup-${messageStatus.index}" ${disabledText}/></td>
								</c:forEach>
								<td>
									<input type="hidden" id="template-os-delete" value="false"/> 
									<img src="img/icons/delete.png" height="20" width="20" alt="Delete" onClick=" deleteInputRow(this); " />
								</td>
							</tr>
							<tr>
								<td colspan="${maxMessageCount+3}">
									<div class="float-left"><img src="img/icons/add.png" height="20" width="20" alt="Add Outcome Status" onClick=" addOutcomeStatusRow(); " /></div>
									<div class="padtop"><span>&nbsp;Add Outcome Status</span></div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- END Outcome Statuses -->
				<input type="submit" value="<spring:message code='save' />" />
			</form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>					