<%-- File name: /trescal/core/workflow/edit/edititemactivity.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="workflow" tagdir="/WEB-INF/tags/workflow/edit"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<c:choose>
				<c:when test="${not empty lastActivity}">Edit Item Activity</c:when>
				<c:otherwise>Create Item Activity</c:otherwise>
			</c:choose>
		</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script>
			// variables needed for tracking insertion points for new inputs
			var insertionStateGroupLinks = ${form.stateGroupLinks.size()};
			var insertionActionOutcome = ${form.actionOutcomeDtos.size()};
			// map of actionOutcomeType to values
			var actionOutcomeValues = new Map();
			<c:forEach var="aoType" items="${actionOutcomeValues.keySet()}">
			{
				var actionOutcomeList = [];
				actionOutcomeValues.set('${aoType}', actionOutcomeList);
				<c:forEach var="aoValue" items="${actionOutcomeValues.get(aoType)}">
					actionOutcomeList.push('${aoValue}');
				</c:forEach>
			}
			</c:forEach>
		</script>
		<script src='script/trescal/core/workflow/edit/EditItemActivity.js'></script>
		<script src='script/trescal/core/workflow/edit/EditItemStateFunctions.js'></script>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox workfloweditor">
			<%@include file="../workfloweditor_header.jspf" %>
			<form:form modelAttribute="form" action="" method="post">
				<t:showErrors path="form.*" showFieldErrors="true" />
				<fieldset class="relative">
					<legend>
						<c:choose>
							<c:when test="${not empty lastActivity}">Selected Activity: <workflow:showActivity itemActivity="${lastActivity}"/></c:when>
							<c:otherwise>New Item Activity:</c:otherwise>
						</c:choose>
					</legend>
					<div>
						<ol>												
							<li>
								<label>State ID:</label>
								<span>
									<c:choose>
										<c:when test="${not empty lastActivity}">${lastActivity.stateid}</c:when>
										<c:otherwise>N/A - New Activity</c:otherwise>
									</c:choose>
								</span>
							</li>																								
							<li>
								<label>
									Type:<c:if test="${not empty lastActivity}"><c:out value=" (non-modifiable)" /></c:if>
								</label>
								<span>
									<c:choose>
										<c:when test="${not empty lastActivity}">
											${lastActivity.type}
										</c:when>
										<c:otherwise>
											<form:select path="type" items="${types}"/>
										</c:otherwise>
									</c:choose>
								</span>
							</li>																								
							<li>
								<label>Active:</label>
								<span><form:checkbox path="active" />(Job Item treated as 'active' when checked)</span>
							</li>																								
							<li>
								<label>Retired:</label>
								<span><form:checkbox path="retired" />(Not available for use on new jobs when checked)</span>
							</li>																								
							<li>
								<label>Description:</label>
								<span><form:input path="description" size="100" /></span>
							</li>
						</ol>
					</div>
				</fieldset>
				<workflow:editItemStateTranslation />
				<workflow:editItemStateGroupLinks />
				<fieldset class="relative">
					<legend>Connected To</legend>
					<div>
						<ol>												
							<li>
								<label>Connection Type:</label>
								<div class="float-left">
									<form:radiobutton path="lookupOrStatus" value="true" onClick=" showLookup(); " />Lookup<br/>
									<form:radiobutton path="lookupOrStatus" value="false" onClick=" showStatus(); " />Status
								</div>
							</li>
							<c:choose>
								<c:when test="${empty form.lookupOrStatus}">
									<c:set var="lookupVis" value="hid" />
									<c:set var="statusVis" value="hid" />
									<c:set var="actionOutcomesVis" value="hid" />
								</c:when>
								<c:when test="${form.lookupOrStatus}">
									<c:set var="lookupVis" value="vis" />
									<c:set var="statusVis" value="hid" />
									<c:choose>
										<c:when test="${form.actionOutcomesEnabled}">
											<c:set var="actionOutcomesVis" value="vis" />
										</c:when>
										<c:otherwise>
											<c:set var="actionOutcomesVis" value="hid" />
										</c:otherwise>
									</c:choose>
								</c:when>
								<c:otherwise>
									<c:set var="lookupVis" value="hid" />
									<c:set var="statusVis" value="vis" />
									<c:set var="actionOutcomesVis" value="hid" />
								</c:otherwise>
							</c:choose>
							<li id="connected-lookup" class="${lookupVis}">
								<label>Connected Lookup:</label>
								<span>
									<form:select path="lookupId" items="${lookups}" itemLabel="value" itemValue="key" />
								</span>
							</li>
							<%-- Action outcomes are only available (optional) when connected to lookup, and action outcomes are enabled --%>
							<li id="action-outcome-enabled" class="${lookupVis}">
								<label>Action Outcomes Enabled:</label>
								<div class="float-left">
									<form:radiobutton id="radioAoEnabled" path="actionOutcomesEnabled" value="true" onClick=" showActionOutcomes(); " />Yes<br/>
									<form:radiobutton id="radioAoDisabled" path="actionOutcomesEnabled" value="false" onClick=" hideActionOutcomes(); " />No
								</div>
							</li>
							<li id="action-outcome-type" class="${actionOutcomesVis}">
								<label>Action Outcome Type:</label>
								<span>
									<form:select id="selectActionOutcomeType" path="actionOutcomeType" items="${actionOutcomeTypes}" onchange=" handleActionOutcomeTypeChange(); "/>
								</span>
							</li>
							<li id="connected-status" class="${statusVis}">
								<label>Connected Status:</label>
								<span>
									<form:select path="statusId" items="${itemStatuses}" itemLabel="value" itemValue="key" />
								</span>
							</li>
						</ol>
					</div>
				</fieldset>
				<!-- Action Outcomes - only visible/editable when connected to lookup, and action outcomes enabled -->
				<div id="section-actionoutcome" class="${actionOutcomesVis}">
					<table class="default2">
						<thead>
							<tr>
								<th colspan="6">Action Outcomes:</th>
							</tr>
							<tr>
								<td>Type</td>
								<td>Value</td>
								<td>Active</td>
								<td>Default</td>
								<td>Positive</td>
								<td>Delete</td>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="aoDto" items="${form.actionOutcomeDtos}" varStatus="rowStatus">
								<tr>
									<td>
										<form:hidden path="actionOutcomeDtos[${rowStatus.index}].type" />
										<c:out value="${aoDto.type}" />
									</td>
									<td>
										<form:hidden path="actionOutcomeDtos[${rowStatus.index}].value" />
										<c:out value="${aoDto.value}" />
									</td>
									<td>
										<form:hidden path="actionOutcomeDtos[${rowStatus.index}].id" />
										<form:select path="actionOutcomeDtos[${rowStatus.index}].active" items="${selectBoolean}" itemLabel="value" itemValue="key" />
									</td>
									<td><form:select path="actionOutcomeDtos[${rowStatus.index}].defaultOutcome" items="${selectBoolean}" itemLabel="value" itemValue="key" /></td>
									<td><form:select path="actionOutcomeDtos[${rowStatus.index}].positiveOutcome" items="${selectBoolean}" itemLabel="value" itemValue="key" /></td>
									<td><form:checkbox path="actionOutcomeDtos[${rowStatus.index}].delete" /></td>
								</tr>
							</c:forEach>
							<tr id="templaterow-ao" class="hid">
								<td>
									<form:hidden id="template-ao-type" path="templateActionOutcome.type" />
									<span id="template-ao-typename">${form.templateActionOutcome.type}</span>
								</td>
								<td>
									<form:select id="template-ao-value" path="templateActionOutcome.value" items="${actionOutcomeValues.get(form.getActionOutcomeType())}" />
								</td>
								<td>
									<form:hidden id="template-ao-id" path="templateActionOutcome.id" />
									<form:select id="template-ao-active" path="templateActionOutcome.active" items="${selectBoolean}" itemLabel="value" itemValue="key" />
								</td>
								<td><form:select id="template-ao-defaultOutcome" path="templateActionOutcome.defaultOutcome" items="${selectBoolean}" itemLabel="value" itemValue="key" /></td>
								<td><form:select id="template-ao-positiveOutcome" path="templateActionOutcome.positiveOutcome" items="${selectBoolean}" itemLabel="value" itemValue="key" /></td>
								<td>
									<input type="hidden" id="template-ao-delete" value="false"/> 
									<img src="img/icons/delete.png" height="20" width="20" alt="Delete" onClick=" deleteInputRow(this); " />
								</td>
							</tr>
							<tr>
								<td colspan="6">
									<div class="float-left"><img src="img/icons/add.png" height="20" width="20" alt="Add Action Outcome" onClick=" addActionOutcomeRow(); " /></div>
									<div class="padtop"><span>&nbsp;Add Action Outcome</span></div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- END Action Outcomes -->
				<input type="submit" value="<spring:message code='save' />" />
			</form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>