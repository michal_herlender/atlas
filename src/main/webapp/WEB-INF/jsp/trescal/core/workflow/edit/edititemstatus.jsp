<%-- File name: /trescal/core/workflow/edit/edititemstatus.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="workflow" tagdir="/WEB-INF/tags/workflow/edit"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<c:choose>
				<c:when test="${not empty lastStatus}">Edit Item Status</c:when>
				<c:otherwise>Create Item Status</c:otherwise>
			</c:choose>
		</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/workflow/edit/EditItemStatus.js'></script>
		<script src='script/trescal/core/workflow/edit/EditItemStateFunctions.js'></script>
		<script>
			// variables needed for tracking insertion points for new inputs
			var insertionStateGroupLinks = ${form.stateGroupLinks.size()};
			var insertionNextActivities = ${form.nextActivities.size()};
		</script>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox workfloweditor">
			<%@include file="../workfloweditor_header.jspf" %>
			<form:form modelAttribute="form" action="" method="post">
				<t:showErrors path="form.*" showFieldErrors="true" />
				<fieldset class="relative">
					<legend>
						<c:choose>
							<c:when test="${not empty lastStatus}">Selected Status: <workflow:showItemStatus itemStatus="${lastStatus}"/></c:when>
							<c:otherwise>New Item Status:</c:otherwise>
						</c:choose>
					</legend>
					<div>
						<ol>												
							<li>
								<label>State ID:</label>
								<span>
									<c:choose>
										<c:when test="${not empty lastStatus}">${lastStatus.stateid}</c:when>
										<c:otherwise>N/A - New Status</c:otherwise>
									</c:choose>
								</span>
							</li>																								
							<li>
								<label>
									Type:<c:if test="${not empty lastStatus}"><c:out value=" (non-modifiable)" /></c:if>
								</label>
								<span class="float-left">
									<form:radiobutton path="workStatus" value="true" disabled="${not empty lastStatus}" />Work Status<br/>
									<form:radiobutton path="workStatus" value="false" disabled="${not empty lastStatus}" />Hold Status
								</span>
								<div class="clear"></div>
							</li>																								
							<li>
								<label>Active:</label>
								<span><form:checkbox path="active" /></span>
							</li>																								
							<li>
								<label>Retired:</label>
								<span><form:checkbox path="retired" /></span>
							</li>																								
							<li>
								<label>Description:</label>
								<span><form:input path="description" size="100" /></span>
							</li>																								
						</ol>
					</div>
				</fieldset>
				<workflow:editItemStateTranslation />
				<workflow:editItemStateGroupLinks />
				<table class="default2">
					<thead>
						<tr>
							<th colspan="3">Next Activties:</th>
						</tr>
						<tr>
							<td>Activity</td>
							<td>Manual Entry Allowed</td>
							<td>Delete</td>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="nextActivityDto" items="${form.nextActivities}" varStatus="activityStatus">
							<tr>
								<td>
									<form:hidden path="nextActivities[${activityStatus.index}].id" />
									<form:select path="nextActivities[${activityStatus.index}].activityId" items="${itemActivities}" itemLabel="value" itemValue="key" />
								</td>
								<td><form:select path="nextActivities[${activityStatus.index}].manualEntryAllowed" items="${selectBoolean}" itemLabel="value" itemValue="key" /></td>
								<td><form:checkbox path="nextActivities[${activityStatus.index}].delete" /></td>
							</tr>
						</c:forEach>
						<tr id="templaterow-activity" class="hid">
							<td>
								<input type="hidden" id="template-activity-id" value="0"/> 
								<form:select id="template-activity-activityId" path="defaultActivityId" items="${itemActivities}" itemLabel="value" itemValue="key" />
							</td>
							<td><form:select id="template-activity-manualEntryAllowed" path="defaultManualEntryAllowed" items="${selectBoolean}" itemLabel="value" itemValue="key" /></td>
							<td>
								<input type="hidden" id="template-activity-delete" value="false"/> 
								<img src="img/icons/delete.png" height="20" width="20" alt="Delete" onClick=" deleteInputRow(this); " />
							</td>
						</tr>
						<tr>
							<td colspan="3">
								<div class="float-left"><img src="img/icons/add.png" height="20" width="20" alt="Add Next Activity" onClick=" addNextActivityRow(); " /></div>
								<div class="padtop"><span>&nbsp;Add Next Activity</span></div>
							</td>
						</tr>
					</tbody>
				</table>
				<input type="submit" value="<spring:message code='save' />" />
			</form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>