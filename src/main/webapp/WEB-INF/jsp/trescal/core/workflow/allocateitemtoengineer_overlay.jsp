<%-- File name : trescal/core/workflow/allocateitemtoengineer_overlay.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<div id="allocateItemOverlay">
	<fieldset>
		<ol>
			<li>
				<label><spring:message code="workflow.jobitem" /></label>
				<spring:message var="textItem" code="item"/>
				<c:out value="${dto.jobno} ${textItem} ${dto.itemno}" />
			</li>
			<li>
				<label><spring:message code="jobtype" /></label>
				${dto.job.type.getDescription()}
			</li>
			<li>
				<label><spring:message code="instmodelname" /></label>
				${dto.instrument.instrumentModelTranslation}
			</li>
			<li>
				<label><spring:message code="transportoptions.nextAvaildate" /></label>
				<fmt:formatDate value="${nextAvialableTransportDate}" type="date" dateStyle="SHORT" />
			</li>
			<li>
				<label><spring:message code="capability" /></label>
				${proc.reference}
			</li>
			<li>
				<label><spring:message code="workflow.engineer" /></label>
				<c:set var="options" value=""/>
				<c:forEach var="pa" items="${proc.authorizations}">
					<c:if test="${pa.authorizationFor.active&& pa.caltype.calTypeId == dto.calTypeId && pa.accredLevel.level > 0}">
						<c:set var="options" value="${options} <option value='${pa.authorizationFor.personid}'>${pa.authorizationFor.name}</option>"/>
						<%--	Making less restrictive for now 2016-06-30
						<c:forEach var="member" items="${dept.members}">
							<c:if test="${member.contact.personid == pa.accreditationFor.personid && (!member.departmentRole.manage || member.contact.personid == currentContact.personid)}">
								<c:set var="options" value="${options} <option value='${pa.accreditationFor.personid}'>${pa.accreditationFor.name}</option>"/>
							</c:if>
						</c:forEach> --%>
					</c:if>
				</c:forEach>
				<c:choose>
					<c:when test="${options == ''}">
						<span><em><spring:message code="workflow.noengineersaccreditedforthisprocedure" /></em></span>
						<li>
							<label>&nbsp;</label>
							<input type="button" onclick="event.preventDefault(); loadScript.closeOverlay();" value="<spring:message code='cancel'/>"/>
						</li>
					</c:when>
					<c:otherwise>
						<c:set var="options" value="<select id='person-${dto.jobItemId}'>${options}</select>" />
						${options}
						<li>
							<label><spring:message code="workflow.schedulefor" /></label>
							<select id="date-${dto.jobItemId}" onchange="event.preventDefault(); updateEndTime(${dto.jobItemId});">
								<c:forEach var="day" items="${availableDays}">
									<option value="${day}"><fmt:formatDate value="${day}" type="date" dateStyle="SHORT" /></option>
								</c:forEach>
							</select>
							<select id="time-${dto.jobItemId}" onchange="event.preventDefault(); updateEndTime(${dto.jobItemId});">
								<c:forEach var="time" items="${timesOfDay}">
									<option>${time}</option>
								</c:forEach>
							</select>
						</li>
						<li>
							<label><spring:message code="workflow.timerequired" /></label>
							<input type="checkbox" id="longcalcheck-${dto.jobItemId}" onclick="chooseLongCalForItem(this.checked, ${dto.jobItemId});"/>
							<spring:message code="workflow.morethanhalfaday"/>
							<div id="longcal-${dto.jobItemId}" class="hid">
								<select id="halfdays-${dto.jobItemId}" onchange="event.preventDefault(); updateEndTime(${dto.jobItemId});">
									<option value="2" selected="selected">
										1 <spring:message code="day" />
									</option>
									<option value="3">
										1.5 <spring:message code="days" />
									</option>
									<option value="4">
										2 <spring:message code="days" />
									</option>
									<option value="5">
										2.5 <spring:message code="days" />
									</option>
									<option value="6">
										3 <spring:message code="days" />
									</option>
									<option value="7">3.5 <spring:message
											code="days" /></option>
									<option value="8">4 <spring:message
											code="days" /></option>
									<option value="9">4.5 <spring:message
											code="days" /></option>
									<option value="10">5 <spring:message
											code="days" /></option>
								</select>
								<span id="calculatedFinish-${dto.jobItemId}"></span>
								<input type="hidden"
									id="dateUntil-${dto.jobItemId}" value="" />
								<input type="hidden"
									id="timeUntil-${dto.jobItemId}" value="" />
							</div>
						</li>
						<li>
							<label><spring:message code="workflow.message" /></label>
							<textarea id="message-${dto.jobItemId}" cols="50" rows="3"></textarea>
						</li>
						<li>
							<label>&nbsp;</label>
							<input type="hidden" id="dept-${dto.jobItemId}" value="${deptId}" />
							<input type="button" onclick="event.preventDefault(); allocateItem(${dto.jobItemId});" value="<spring:message code='workflow.alloc'/>" />
						</li>
					</c:otherwise>
				</c:choose>
			</li>
		</ol>
	</fieldset>
</div>