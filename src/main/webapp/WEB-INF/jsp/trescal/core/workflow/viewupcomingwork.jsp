<%-- File name: /trescal/core/workflow/viewupcomingwork.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>

<script src='script/trescal/core/workflow/ViewUpcomingWork.js'></script>

<script>
	var businessUsers = [];
	<c:forEach var="businessUser" items="${model.businessUsers}">
		businessUsers.push({id: ${businessUser.personid}, name: "${businessUser.name}"});
	</c:forEach>
	var currentContactId = ${currentContact.personid};
</script>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="workflow.viewupcomingwork"/></span>
		<script type="module" src="script/components/cwms-upcoming-work-job-link/cwms-upcoming-work-job-link.js"></script>
	</jsp:attribute>
	<jsp:body>
		<!-- infobox contains all upcoming work and is styled with nifty corners -->
		<div class="infobox upcomingwork">
			<div class="text-center">
				<a href="viewupcomingwork.htm?startdate=${cwms:isoDate(model.previous)}" class="mainlink float-left">
					&lt;&lt;&lt; <spring:message code="workflow.viewpreviousweeks" arguments="${model.numWeeks}" />
				</a>
				<a href="viewupcomingwork.htm?startdate=${cwms:isoDate(model.next)}" class="mainlink float-right">
					<spring:message code="workflow.viewnextweeks" arguments="${model.numWeeks}" />&gt;&gt;&gt;
				</a>
				<a href="#" onclick="event.preventDefault(); createUpcomingWork('Add', null, null, '', ${sessionScope.allocatedSubdiv.key}, 0, 0, '', '', 0, '', '');" class="mainlink">
					<spring:message code="workflow.addupcomingwork"/>
				</a>
			</div>
			<c:forEach var="dateKey" items="${model.datelist.keySet()}" varStatus="dateStatus">
				<c:set var="dates" value="${model.datelist.get(dateKey)}"/>
				<c:set var="upcWorkWraps" value="${model.worklist.get(dateKey)}"/>
				<c:set var="engAllocWraps" value="${model.allocationlist.get(dateKey)}"/>
				<c:if test="${dateStatus.first}">
					<div class="clear">&nbsp;</div>
					<div class="padding2" style=" float: left; width: 49%; ">
						<b><spring:message code="workflow.filterupcomingwork"/>:</b><br/>
						<div class="clear">&nbsp;</div>
						<c:forEach var="dp" items="${model.depts}" varStatus="deptStatus">
							<c:choose>
								<c:when test="${dp.name.indexOf('Laboratory') != -1}">
									<c:set var="deptName" value="${dp.name.replace('Laboratory', '')}"/>
								</c:when>
								<c:otherwise>
									<c:set var="deptName" value="${dp.name}"/>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${deptStatus.first}">
									<span>
										<input type="checkbox" class="checkbox" name="all" onclick="selectAllLabs(this.checked);" checked/>
										<spring:message code="all"/> &nbsp;&nbsp;
									</span>
								</c:when>
								<c:when test="${deptStatus.index % 5 == 0}">
									<br/>
								</c:when>
							</c:choose>
							<input type="checkbox" class="checkbox" name="filterlabs" value="${dp.name}" onclick="filterLabView(${dates.size()});" checked/>
							${deptName} &nbsp;&nbsp;													
						</c:forEach>
					</div>
					<div class="padding2" style=" float: right; width: 49%; ">
						<b><spring:message code="workflow.filterallocatedjobitems"/>:</b><br/>
						<div class="clear">&nbsp;</div>
						<c:forEach var="dp" items="${model.depts}" varStatus="deptStatus">
							<c:choose>
								<c:when test="${dp.name.indexOf('Laboratory') != -1}">
									<c:set var="deptName" value="${dp.name.replace('Laboratory', '')}"/>
								</c:when>
								<c:otherwise>
									<c:set var="deptName" value="${dp.name}"/>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${deptStatus.first}">
									<span>
										<input type="checkbox" class="checkbox" name="all" onclick="selectAllAllocations(this.checked);" checked/> <spring:message code="all"/> &nbsp;&nbsp;
									</span>
								</c:when>
								<c:when test="${deptStatus.index % 5 == 0}">
									<br/>
								</c:when>
							</c:choose>
							<input type="checkbox" class="checkbox" name="filterallocations" value="${dp.name}" onclick="filterAllocations(${dates.size()});" checked/> ${deptName} &nbsp;&nbsp;													
						</c:forEach>
					</div>
					<div class="clear">&nbsp;</div>
				</c:if>
				<table class="default2">
					<thead>
						<tr>
							<c:set var="colwidth" value="${100 / dates.size()}"/>
							<c:forEach var="date" items="${dates}">
								<td width="${colwidth}%"><fmt:formatDate value="${date}" type="date" dateStyle="SHORT" /></td>
							</c:forEach>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="${dates.size()}">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody id="upcomingBody-${dateStatus.index}">
						<c:choose>
							<c:when test="${upcWorkWraps.size() == 0}">
								<tr>
									<td class="text-center" colspan="${dates.size()}">
										-- <spring:message code="workflow.noupcomingworkduethisweek"/> --
									</td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:forEach var="w" items="${upcWorkWraps}">
									<tr class="labvisible">
										<c:set var="amountOfColumns" value="0"/>
										<c:forEach var="n" begin="1" end="${dates.size()}">
											<c:if test="${amountOfColumns + 1 == n}">
												<c:choose>
													<c:when test="${n == w.startBlock}">
														<c:set var="cspan" value="${w.endBlock - w.startBlock + 1}"/>
														<c:set var="contpid" value="0"/>
														<c:set var="subdivid" value="${sessionScope.allocatedSubdiv.key}"/>
														<c:if test="${w.upcomingWork.contact != null}">
															<c:set var="contpid" value="${w.upcomingWork.contact.personid}"/>
															<c:set var="subdivid" value="${w.upcomingWork.contact.sub.subdivid}"/>
														</c:if>
														<td colspan="${cspan}">
															<div class="calendar_div">
																	<div  id="${w.upcomingWork.department.name}${w.upcomingWork.id}-${n}"
																			<c:choose>
																				<c:when test='${currentContact.personid == w.upcomingWork.userResponsible.personid || currentContact.personid == w.upcomingWork.addedBy.personid}'>
																					onclick="createUpcomingWork('Edit', ${w.upcomingWork.id}, ${w.upcomingWork.comp.coid}, '${w.upcomingWork.comp.coname}',  ${sessionScope.allocatedSubdiv.key}, '${contpid}', ${w.upcomingWork.userResponsible.personid}, '${w.upcomingWork.title}', '${cwms:escapeJS( w.upcomingWork.description)}', ${w.upcomingWork.department.deptid}, '${cwms:isoDate(w.upcomingWork.startDate)}', '${cwms:isoDate(w.upcomingWork.dueDate)}');"
																				</c:when>
																				<c:otherwise>
																					onclick="$j.prompt('Only the creator of this upcoming work schedule, or the user that it affects, can edit it.');"
																				</c:otherwise>
																			</c:choose>>
																		<div class="calendar_lab">${w.upcomingWork.department.name}</div>
																		<span class="bold">
																		${w.upcomingWork.userResponsible.name}<br />
																		(<spring:message code="workflow.addedby"/>: ${w.upcomingWork.addedBy.name})<br />
																		<c:choose>
																			<c:when test="${w.upcomingWork.contact != null}">
																				${w.upcomingWork.contact.sub.comp.coname} (${w.upcomingWork.contact.name})<br/>
																			</c:when>
																			<c:otherwise>
																				${w.upcomingWork.comp.coname}<br/>
																			</c:otherwise>
																		</c:choose>
																		<c:if test="${w.upcomingWork.title != '- No title -'}">
																			${w.upcomingWork.title}<br/>
																		</c:if>
																	</span>
																		<c:if test="${w.upcomingWork.description != '- No description -'}">
																			<span>${w.upcomingWork.description}</span><br/>
																		</c:if>

																		<c:choose>
																			<c:when test="${currentContact.personid == w.upcomingWork.userResponsible.personid || currentContact.personid == w.upcomingWork.addedBy.personid}">
																				<c:set var="onClickEvent" value="deleteUpcomingWork(${w.upcomingWork.id}); return false;"/>
																			</c:when>
																			<c:otherwise>
																				<c:set var="onClickEvent" value="$j.prompt('Only the creator of this upcoming work schedule, or the user that it affects, can delete it.');"/>
																			</c:otherwise>
																		</c:choose>
																		<div class="calendar_delete" onclick="event.cancelBubble = true; ${onClickEvent}"></div>
																	</div>
																	<span>
																	<c:forEach var="upcomingWorkJob" items="${w.upcomingWorkJobLinkAjaxDtos}">
																		<links:jobnoLinkDWRInfo rowcount="0" jobno="${upcomingWorkJob.jobNo}" jobid="${upcomingWorkJob.jobId}" copy="true" />
																	</c:forEach>
																</span>
															</div>
                                                        </td>
														<c:set var="amountOfColumns" value="${amountOfColumns + cspan}"/>
													</c:when>
													<c:otherwise>
														<td>&nbsp;</td>
														<c:set var="amountOfColumns" value="${amountOfColumns + 1}"/>
													</c:otherwise>
												</c:choose>
											</c:if>
										</c:forEach>
									</tr>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
					<tbody id="allocationBody-${dateStatus.index}">
						<c:choose>
							<c:when test="${engAllocWraps.size() == 0}">
								<tr>
									<td class="text-center" colspan="${dates.size()}">
										-- <spring:message code="workflow.noitemsallocatedtoengineersthisweek"/> --
									</td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:forEach var="w" items="${engAllocWraps}">
									<tr class="labvisible">
										<c:set var="amountOfColumns" value="0"/>
										<c:forEach var="n" begin="1" end="${dates.size()}">
											<c:if test="${amountOfColumns + 1 == n}">
												<c:choose>
													<c:when test="${n == w.startBlock}">
														<c:set var="cspan" value="${w.endBlock - w.startBlock + 1}"/>
														<td colspan="${cspan}">
															<div class="allocation_div" id="allocation-${w.engineerAllocation.dept.name}${w.engineerAllocation.id}-${n}">
																<div class="allocation_lab">
																	${w.engineerAllocation.dept.name}
																</div>
																<span class="bold">
																	${w.engineerAllocation.allocatedTo.name}<br/>
																	(<spring:message code="workflow.allocatedby"/>: ${w.engineerAllocation.allocatedBy.name})<br/>
																	${w.engineerAllocation.itemAllocated.job.jobno} (${w.engineerAllocation.itemAllocated.job.con.sub.comp.coname})<br/>
																	${w.engineerAllocation.itemAllocated.itemNo}. ${w.engineerAllocation.itemAllocated.inst.definitiveInstrument}<br/>
																</span>
															</div>
														</td>
														<c:set var="amountOfColumns" value="${amountOfColumns + cspan}"/>
													</c:when>
													<c:otherwise>
														<td>&nbsp;</td>
														<c:set var="amountOfColumns" value="${amountOfColumns + 1}"/>
													</c:otherwise>
												</c:choose>
											</c:if>
										</c:forEach>
									</tr>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
			</c:forEach>
			<div class="text-center">
				<div class="float-left">
					<a href="viewupcomingwork.htm?startdate=${cwms:isoDate(model.previous)}" class="mainlink">
						&lt;&lt;&lt; <spring:message code="workflow.viewpreviousweeks" arguments="${model.numWeeks}" />
					</a>
				</div>
				<a href="#" onclick="event.preventDefault(); createUpcomingWork('Add', null, null, '', ${sessionScope.allocatedSubdiv.key}, 0, 0, '', '', 0, '', '');" class="mainlink">
					<spring:message code="workflow.addupcomingwork"/>
				</a>
				<div class="float-right">
					<a href="viewupcomingwork.htm?startdate=${cwms:isoDate(model.next)}" class="mainlink">
						<spring:message code="workflow.viewnextweeks" arguments="${model.numWeeks}" />&gt;&gt;&gt;
					</a>
				</div>									
			</div>
		</div>
		<!-- end of infobox div -->
	</jsp:body>
</t:crocodileTemplate>