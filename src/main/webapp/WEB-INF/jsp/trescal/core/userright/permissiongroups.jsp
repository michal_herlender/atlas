<%-- File name: /trescal/core/userright/permissiongroups.jsp--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext">Permission Groups</span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
    	    	<script src='script/trescal/core/userright/PermissionGroups.js'></script>
    </jsp:attribute>
    <jsp:body>
    	<div class="infobox">
    		<table id="groupPermissionTable" class="default4">
    			<thead>
    				<tr>
    					<th>Group Name</th>
    					<th>Description</th>
    					<th style="text-align: center;">
    					<img src="img/icons/add.png" height="20" width="20" alt="<spring:message code="add"/>" title="<spring:message code="add"/>"  style="cursor: pointer;" onclick="addPermissionGroup()"/>
    					</th>
    				</tr>
    			</thead>
    			<tbody>
    				<c:forEach var="group" items="${permissionGroups}">
    					<tr>
    						<td>${group.name}</td>
    						<td><cwms:besttranslation translations="${group.descriptionTranslations}"/></td>
    						<td style="text-align: center;"><a href="editpermissiongroup.htm?groupId=${group.id}">Edit</a></td>
    					</tr>
    				</c:forEach>
    			</tbody>
    		
    		</table>
    	</div>
    </jsp:body>
</t:crocodileTemplate>