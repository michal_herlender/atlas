<%-- File name: /trescal/core/userright/editpermissiongroup.jsp--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext">Edit User Right Role</span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
    	<script src='script/trescal/core/userright/EditRole.js'></script>
    </jsp:attribute>
    <jsp:body>
    	<h3>
    		<input id="nameInput" value="${role.name}" disabled class="width90"/>
   			<a href="userroles.htm"  >
   		   		<img src="img/icons/arrow_left.png">
   			</a>
    		<img id="enableEdit" src="img/icons/edit_small.png" onclick="enableNameEdit();">
   			<img id="disableEdit" src="img/icons/greentick.png" class="hid" onclick="changeName(${role.id});">
   			<a onClick="deleteOverlay(${role.id})"  title="<spring:message code='remove'/>">
   		   		<img src="img/icons/cross.png" style="height: 13px;padding-left: 2px;padding-bottom: 2px;" >
   			</a>
		</h3>
    	<div class="infobox">
    		<table id="groupTable" class="default4">
				<thead>
					<tr>
						<th>Group</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="group" items="${role.groups}">
						<tr>
							<td>${group.name}</td>
							<td style="text-align: center;">
								<img src="img/icons/delete.png" height="20" width="20" alt="<spring:message code='remove'/>" title="<spring:message code='remove'/>" onClick="removeGroupFromRole('${group.id}', ${role.id}, this)"/>
							</td>
						</tr>
					</c:forEach>
				</tbody>
				<tfoot>
					<tr>
						<td>
							<select id="newgroup">
								<c:forEach var="group" items="${groups}">
									<option value="${group.id}">
										${group.name}
									</option>
								</c:forEach>
							</select>
						</td>
						<td style="text-align: center;">
							<img src="img/icons/add.png" height="20" width="20" alt="<spring:message code="add"/>" title="<spring:message code="add"/>" onClick="addGroupToRole(${role.id})"/>
						</td>
					</tr>
				</tfoot>
			</table>
    	</div>
    </jsp:body>
</t:crocodileTemplate>