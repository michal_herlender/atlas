<%-- File name: /trescal/core/userright/editpermissionlimit.jsp--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="adminarea.managepermissionlimit"></spring:message></span>
    </jsp:attribute>
    
    <jsp:attribute name="scriptPart">
        	<script src='script/trescal/core/userright/PermissionLimit.js'></script>
    </jsp:attribute>
    
    <jsp:body>
       	<div class="infobox">
  			<h4>
       			<spring:message code="quotform.purchaseorder" />
    		</h4>
    		<table id="permissionTable" class="default4" style="table-layout: fixed;">
				<thead>
					<tr>
 						<th><spring:message code="adminarea.permission"/></th>
						<th><spring:message code="viewinstrument.maximum" />(<spring:message code="currency" /> : ${companyCurrency})</th>
						<th><spring:message code="edit" /></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="permissionLimit" items="${permissionLimitPos}">
						<tr>
							<td>${permissionLimit.getPermissionLimit().getPermission()}</td>
							<td>
							<span id="max${permissionLimit.getId()}" class="vis">
								${permissionLimit.getMaximum()}
							</span>
							<input type="text" id="inputmax${permissionLimit.getId()}" value="${permissionLimit.getMaximum()}" class="hid"/>
							
							</td>
							<td style="text-align: center;">
							<span id="edit${permissionLimit.getId()}" class="vis">
								<a href="#" class="mainlink" onClick="event.preventDefault();editValue(${permissionLimit.getId()});">
									<spring:message code="edit"/>
								</a>
							</span>
							<span id="save${permissionLimit.getId()}" class="hid">
								<spring:message var="editCode" code="edit" />
								<a href="#"  class="mainlink" onClick="event.preventDefault();saveValue(${permissionLimit.getId()});" >
									<spring:message code="save"/>
								</a>
							</span>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
    	</div>
    
     	<div class="infobox">
			<h4>
       			<spring:message code="quotations" />
			</h4>
    		<table id="permissionTable2" class="default4" style="table-layout: fixed;">
				<thead>
					<tr>
 						<th><spring:message code="adminarea.permission"/></th>
						<th><spring:message code="viewinstrument.maximum" />(<spring:message code="currency" /> : ${companyCurrency})</th>
						<th><spring:message code="edit" /></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="permissionLimit" items="${permissionLimitQuotes}">
						<tr>
							<td>${permissionLimit.getPermissionLimit().getPermission()}</td>
							<td>
							<span id="max${permissionLimit.getId()}" class="vis">
								${permissionLimit.getMaximum()}
							</span>
							<input type="text" id="inputmax${permissionLimit.getId()}" value="${permissionLimit.getMaximum()}" class="hid"/>
							
							</td>
							<td style="text-align: center;">
							<span id="edit${permissionLimit.getId()}" class="vis">
								<a href="#" class="mainlink" onClick="event.preventDefault();editValue(${permissionLimit.getId()});">
									<spring:message code="edit"/>
								</a>
							</span>
							<span id="save${permissionLimit.getId()}" class="hid">
								<spring:message var="editCode" code="edit" />
								<a href="#"  class="mainlink" onClick="event.preventDefault();saveValue(${permissionLimit.getId()});" >
									<spring:message code="save"/>
								</a>
							</span>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
    	</div>
    </jsp:body>
</t:crocodileTemplate>