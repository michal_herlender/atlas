<%-- File name: /trescal/core/userright/permissiongroupsline.jsp--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<tr>
	<td>${groupName}</td>
	<td>${translation}</td>
	<td style="text-align: center;"><a href="editpermissiongroup.htm?groupId=${groupId}">Edit</a></td>
</tr>