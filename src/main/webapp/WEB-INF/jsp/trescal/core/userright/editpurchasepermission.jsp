<%-- File name: /trescal/core/userright/editpermissiongroup.jsp--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="adminarea.manageprocedurepermissionlimit"></spring:message></span>
    </jsp:attribute>
    
    <jsp:attribute name="scriptPart">
        	<script src='script/trescal/core/userright/PurchasePermission.js'></script>
    </jsp:attribute>
    
    <jsp:body>
       	<div class="infobox">
    		<table id="permissionTable" class="default4" style="table-layout: fixed;">
				<thead>
					<tr>
						<th><spring:message code="adminarea.permission"/></th>
						<th><spring:message code="viewinstrument.maximum" />(<spring:message code="currency" /> : ${companyCurrency})</th>
						<th><spring:message code="edit" /></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="purchasePermission" items="${purchasePermissions}">
						<tr>
							<td>${purchasePermission.getPermission()}</td>
							<td>
							<span id="max${purchasePermission.getId()}" class="vis">
								${purchasePermission.getMaximum()}
							</span>
							<input type="text" id="inputmax${purchasePermission.getId()}" value="${purchasePermission.getMaximum()}" class="hid"/>
							
							</td>
							<td style="text-align: center;">
							<span id="edit${purchasePermission.getId()}" class="vis">
								<a href="#" class="mainlink" onClick="event.preventDefault();editValue(${purchasePermission.getId()});">
									<spring:message code="edit"/>
								</a>
							</span>
							<span id="save${purchasePermission.getId()}" class="hid">
								<spring:message var="editCode" code="edit" />
								<a href="#"  class="mainlink" onClick="event.preventDefault();saveValue(${purchasePermission.getId()});" >
									<spring:message code="save"/>
								</a>
							</span>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
    	</div>
    
    
    </jsp:body>
</t:crocodileTemplate>