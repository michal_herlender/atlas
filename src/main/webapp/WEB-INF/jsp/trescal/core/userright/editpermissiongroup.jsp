<%-- File name: /trescal/core/userright/editpermissiongroup.jsp--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext">Edit Permission Group</span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
		<script src='script/thirdparty/jQuery/jquery.js' ></script>
		<script src='script/thirdparty/jQuery/jquery.dataTables.min.js' ></script>
    	<script src='script/trescal/core/userright/EditPermissionGroup.js'></script>
    </jsp:attribute>
    <jsp:body>
   		<h3>
   			<input id="nameInput" value="${permissionGroup.name}" disabled class="width90"/>
			<a href="permissiongroups.htm"  >
   		   		<img src="img/icons/arrow_left.png">
   			</a>
   			<img id="enableEdit" src="img/icons/edit_small.png" style="cursor: pointer;" onclick="enableNameEdit();">
   			<img id="disableEdit" src="img/icons/greentick.png" style="cursor: pointer;" class="hid" onclick="changeName(${permissionGroup.id});">
   			<a onClick="deleteOverlay(${permissionGroup.id})"  title="<spring:message code='remove'/>">
   		   		<img src="img/icons/cross.png" style="height: 13px;padding-left: 2px;padding-bottom: 2px;" >
   			</a>
   		</h3>
    	<div class="infobox">
    		<table id="permissionTable" class="default4">
				<thead>
					<tr>
						<th>Permission</th>
						<th>Domain</th>
						<th>URL</th>
						<th>Description</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="permission" items="${permissionDto}" varStatus="loop">
						<tr>
							<td >${permission.getName()}</td>
							<td >${permission.getArea()}</td>
							<td >${permission.getUrl()}</td>		
							<td >${permission.getDescription()}</td>							
							<td style="text-align: center;">
								<c:choose>
								  <c:when test="${permission.getOwned()}">
								  <span class="hid" id="${loop.index}">enable</span>
									<img src="img/icons/delete.png" id="delete${loop.index}" class="vis"  height="20" width="20"  alt="<spring:message code='remove'/>" title="<spring:message code='remove'/>"  onClick="removePermission('${permission.getName()}', ${permissionGroup.id}, ${loop.index})"/>
									<img src="img/icons/add.png" id="add${loop.index}" class="hid"  height="20" width="20" alt="<spring:message code="add"/>" title="<spring:message code="add"/>" onClick="addPermission('${permission.getName()}',${permissionGroup.id},${loop.index})"/>
								  </c:when>
								  <c:otherwise>
								  <span class="hid" id="${loop.index}">unable</span>
									<img src="img/icons/delete.png" id="delete${loop.index}" class="hid"  height="20" width="20"  alt="<spring:message code='remove'/>" title="<spring:message code='remove'/>"  onClick="removePermission('${permission.getName()}', ${permissionGroup.id}, ${loop.index})"/>
									<img src="img/icons/add.png" id="add${loop.index}" class="vis"  height="20" width="20" alt="<spring:message code="add"/>" title="<spring:message code="add"/>" onClick="addPermission('${permission.getName()}',${permissionGroup.id},${loop.index})"/>
								  </c:otherwise>
								</c:choose>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
    	</div>
    </jsp:body>
</t:crocodileTemplate>