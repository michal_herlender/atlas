<tr>
	<td>${group.name}</td>
	<td style="text-align: center;"><img src="img/icons/delete.png"
		height="20" width="20" alt="<spring:message code='remove'/>"
		title="<spring:message code='remove'/>"
		onClick="removeGroupFromRole(${group.id}, ${roleId}, this)" />
	</td>
</tr>