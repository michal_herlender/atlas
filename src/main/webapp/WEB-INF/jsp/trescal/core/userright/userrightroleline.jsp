<%-- File name: /trescal/core/userright/userrightroleline.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<tr>
	<td>${roleName}</td>
	<td><cwms:besttranslation translations="${roleDesc}"/></td>
	<td><a href="editrole.htm?roleId=${roleId}">Edit</a></td>
</tr>