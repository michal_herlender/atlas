<%-- File name: /trescal/core/userright/permissionline.jsp--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<tr>
	<td>${permission}</td>
	<td>${permission.url}</td>
	<td style="text-align: center;">
		<c:set var="rmv"> 
			<spring:message code="remove"></spring:message>
		</c:set>
		<img src="img/icons/delete.png" height="20" width="20" alt="${rmv}" title="${rmv}" onClick="removePermission('${permission.name()}', ${groupId},this)"/>
	</td>
</tr>