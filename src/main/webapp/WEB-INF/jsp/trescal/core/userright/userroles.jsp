<%-- File name: /trescal/core/userright/permissiongroups.jsp--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext">User Roles</span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
    	<script src='script/trescal/core/userright/UserRoles.js'></script>
    </jsp:attribute>
    <jsp:body>
    	<div class="infobox">
    		<table class="default4" id="roletable">
    			<thead>
    				<tr>
    					<th>Role Name</th>
    					<th>Description</th>
    					<th>    		
    						<img src="img/icons/add.png" height="20" width="20" alt="<spring:message code="add"/>" title="<spring:message code="add"/>"  style="cursor: pointer;" onclick="addUserRoles()"/>
    					</th>
    				</tr>
    			</thead>
    			<tbody>
    				<c:forEach var="role" items="${roles}">
    					<tr>
    						<td>${role.name}</td>
    						<td><cwms:besttranslation translations="${role.descriptionTranslations}"/></td>
    						<td><a href="editrole.htm?roleId=${role.id}">Edit</a></td>
    					</tr>
    				</c:forEach>
    			</tbody>
    		</table>
    	</div>
    </jsp:body>
</t:crocodileTemplate>