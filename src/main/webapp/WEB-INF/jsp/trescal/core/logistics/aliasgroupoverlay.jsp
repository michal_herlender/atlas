<%-- File name: /trescal/core/admin/aliasgroupoverlay.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:overlayTemplate bodywidth="780">
	
	<jsp:attribute name="scriptPart">
		
		<script>

			var columnvalues = new Array();
			<c:forEach var="fieldName" items="${ intervalunits }">
				columnvalues.push({
							'name': "${fieldName}",
							'translation': "${fieldName.getSingularName(locale)}",
							'type' : 'INTERVAL_UNIT'
						});
			</c:forEach>
			
			<c:forEach var="fieldName" items="${ servicetypes }">
			var bestTransaltion = '';
			<c:forEach var="trans" items="${fieldName.shortnameTranslation}">
				if(${trans.locale eq locale})
					{
					bestTransaltion = "${trans.translation}";
					}
			</c:forEach>
				columnvalues.push({
							'name': "${fieldName.serviceTypeId}",
							'translation': bestTransaltion,
							'type' : 'EXPECTED_SERVICE'
						});
			</c:forEach>
			
		</script>
	
		<script src="script/trescal/core/logistics/exchangeformat/aliasgroup/aliasGroup.js"></script>
	</jsp:attribute>

	<jsp:body>
		<div id="AliasGroupOverlay">
			<t:showErrors path="aliasgroupform.*" showFieldErrors="true"/>
				<fieldset>
					<form:form  modelAttribute="aliasgroupform" action="" method="post" >
						<form:hidden path="action" />
						<form:hidden path="alaisGroupId" />
						<form:hidden path="allocatedCompanyid" />
						<form:hidden path="createdOn" />
						<form:hidden path="createdById" />
							<ol>
								<li>
									<label><spring:message code="aliasgroup.aliasgroupname"/> : </label>
									 <form:input path="aliasGroupName" />                      
									 <span class="attention"><form:errors path="aliasGroupName" /></span>
								</li>
								<li>
									<label><spring:message code="aliasgroup.aliasgroupdescription"/> : </label>
									<form:textarea path="aliasGroupDesc" cols="80" rows="2" />
									<span class="attention"><form:errors path="aliasGroupDesc" /></span>
								</li>
								<li>
									<label><spring:message code="aliasgroup.defaultforcurrentbuiscompany"/> : </label>
									<form:checkbox path="defaultForCurrentBusinessComp"/>
									<span class="attention"><form:errors path="defaultForCurrentBusinessComp" /></span>
								</li>
								<li> 
									<label><spring:message code="exchangeformat.FieldName"/> :</label>
									<select  id="aliasField">
										<c:forEach var="field" items="${exchangeformatfieldname}">
											<option value="${field}">${field.value}</option>
										</c:forEach>
									</select>
									<img src="img/icons/add.png" width="16" height="16" alt=""title="" onclick="addAliasField();initSelectValues($j('#aliasField').find(':selected').val());" id="addAliasFied" style="cursor: pointer; vertical-align: bottom;"/>
								</li>
								
								<li>
					
									<table id="aliasFieldsTable" class="default4 companySubdivs">
										<thead>
											<tr>
												<th class="codefault">#</th>
												<th class="codefault"><spring:message code="aliasgroup.column"/></th>
												<th class="codefault"><spring:message code="aliasgroup.value"/></th>
												<th class="codefault"><spring:message code="aliasgroup.alias"/></th>
												<th class="codefault"><spring:message code="aliasgroup.action"/></th>
											</tr>
										</thead>								
										<tbody>
										
											<c:forEach items="${aliasgroupform.aliasesForm}" varStatus="loop" var="aliases">
												<tr class="even">
													<td class="center"> <span>${ loop.index + 1 } </span>
														<input type="hidden" id="aliasesForm${ loop.index }.aliasId" name="aliasesForm[${ loop.index }].aliasId" value="${ aliases.aliasId }" />
													</td>
													<td class="center eff">
														${ aliases.effn.getValue() }
														<input type="hidden" id="aliasesForm${ loop.index }.effn" name="aliasesForm[${ loop.index }].effn" value="${ aliases.effn }" />
													</td>
													<td class="center">
														<select id="fieldvalues_${ loop.index }" name="aliasesForm[${ loop.index }].value">
															<c:if test="${ aliases.effn eq 'EXPECTED_SERVICE' }">
																<c:forEach var="st" items="${servicetypes}">
																		<option value="${st.serviceTypeId}" ${ st.serviceTypeId eq aliases.value ? 'selected="selected"' : '' }><cwms:besttranslation translations="${st.shortnameTranslation}" /></option>
																</c:forEach>
															</c:if>
															<c:if test="${ aliases.effn eq 'INTERVAL_UNIT' }">
																<c:forEach var="intervalunit" items="${intervalunits}">
																		<option value="${intervalunit}" ${ intervalunit eq aliases.value ? 'selected="selected"' : '' }><spring:message code="${intervalunit.singularNameCode}"/> </option>
																</c:forEach>
															</c:if>
														</select>
													</td>
													<td class="center">
														<input value="${ aliases.alias }" id="aliasesForm${ loop.index }.alias" name="aliasesForm[${ loop.index }].alias" />
													</td>
													<td class="center">
														<img src="img/icons/delete.png" height="16" width="16" alt="" title="" class="delimg" onclick="deleteAliasField(this)" style="cursor: pointer;"/>
													</td>
												
												</tr>
											
											</c:forEach>
													
										</tbody>									
									</table>
						
								</li>
								
								<li>
									<label>&nbsp;</label>
									<c:choose>
										<c:when test="${action == 'add'}">
											<spring:message var="buttonLabel" code="aliasgroup.add" />
										</c:when>
										<c:otherwise>
											<spring:message var="buttonLabel" code="aliasgroup.save" />
										</c:otherwise>
									</c:choose>
									<input type="submit" id="jiwr_submit" value="${buttonLabel}" />
									&nbsp;
									<input type="button" id="jiwr_close" value="<spring:message code='cancel' />" onclick="event.preventDefault(); window.parent.loadScript.closeOverlay(); " />
								</li>
							</ol>
					</form:form>
				</fieldset>
		</div>
	</jsp:body>
</t:overlayTemplate>