<%-- File name: /trescal/core/jobs/job/viewjob.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel"%>
<%@ taglib prefix="costs" tagdir="/WEB-INF/tags/costs"%>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image"%>

<t:crocodileTemplate>
	<jsp:attribute name="scriptPart">

	</jsp:attribute>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="viewprebooking.viewprebooking"/></span>
	</jsp:attribute>
	<jsp:body>
		<c:if test="${prebookingform.message != null}">
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						${prebookingform.message}
					</div>
				</div>
			</div>
		</c:if>
		<!-- main div which contains all prebooking job information -->
		<div id="viewprebooking">
			<!-- infobox contains all prebooking job information and is styled with nifty corners -->
			<div class="infobox">
				<fieldset>
					<legend><spring:message code="viewprebooking.legend"/> : ${asn.id}</legend>
					<!-- displays content in column 49% wide (left) -->
					<div class="displaycolumn">
						<ol>
							<li>
								<label><spring:message code="subdiv"/>:</label>
								<span>
									<links:subdivLinkDWRInfo rowcount="1" subdiv="${asn.prebookingJobDetails.con.sub}"/>
								</span>
							</li>
							<li>
								<label><spring:message code="contact"/>:</label>
								<span>
									<links:contactLinkDWRInfo contact="${asn.prebookingJobDetails.con}" rowcount="0"/>
								</span>
							</li>
							<li>
								<label><spring:message code="viewprebooking.sourceapi"/>:</label>
								<span>${asn.sourceApi}</span>
							</li>
							<li>
								<label><spring:message code="instmod.lastmodified"/>:</label>
								<links:lastModifiedContactLink entity="${asn}" />
							</li>
							<c:if test="${asn.prebookingJobDetails != null && not empty asn.prebookingJobDetails.returnToLoc}">
								<li>
									<label><spring:message code="viewprebooking.location"/>:</label> 			
									<span>${asn.prebookingJobDetails.returnToLoc.location}</span>
								</li>
							</c:if>
							
                         <c:if test="${asn.sourceApi eq 'CWMS'}">
							<li>
								<label><spring:message code="viewprebooking.downloadfile"/>:</label>
								<a href="<c:url value='downloadPrebookingJobFile' />?id=${asn.id}" target="blank" class="mainlink padleft10">
										<spring:message code="exchangeformat.download"/>
								</a>
							</li>
							</c:if>
						</ol>
					</div>
					<!-- end of left column -->
					<!-- displays content in column 49% wide (right) -->
					<div class="displaycolumn">
						<ol>	
							<li>
								<label><spring:message code="viewprebooking.clientref"/>:</label>
								<span>
									<c:choose>
										<c:when test="${asn.prebookingJobDetails != null && asn.prebookingJobDetails.clientRef.trim().length() > 0}">
											${asn.prebookingJobDetails.clientRef}
										</c:when>
										<c:otherwise>
											&nbsp;-- <spring:message code="viewprebooking.noclientref"/> --&nbsp;
										</c:otherwise>
									</c:choose>
								</span>
							</li>
							<li>
								<label><spring:message code="viewprebooking.currency"/>:</label>
								<c:set var="currency" value="${asn.prebookingJobDetails.currency}"/>
								<span>
									${currency.currencyCode}
									(${currency.currencySymbol} ${currency.currencyName}
									- ${defaultCurrency.currencyERSymbol}1 : ${currency.currencyERSymbol})
								</span>
							</li>
							<li>
								<label><spring:message code="viewprebooking.bookinaddr"/>:</label>
								<span>
									<c:if test="${asn.prebookingJobDetails != null && asn.prebookingJobDetails.bookedInAddr != null}">
										<a class="mainlink" href="viewaddress.htm?addrid=${asn.prebookingJobDetails.bookedInAddr.addrid}">
											${asn.prebookingJobDetails.bookedInAddr.addr1}, ${asn.prebookingJobDetails.bookedInAddr.town}
										</a>
									</c:if>
								</span>
							</li>
							<c:if test="${asn.prebookingJobDetails != null && asn.prebookingJobDetails.bookedInLoc != null}">
								<li>
									<label><spring:message code="viewprebooking.bookinloc"/>:</label>
									<span>
											<a class="mainlink" href="location.htm?locid=${job.bookedInLoc.locationid}">${job.bookedInLoc.location}</a>
									</span>
								</li>
							</c:if>
							<li>
								<label><spring:message code="createdby"/>:</label>
								<links:contactLinkDWRInfo contact="${asn.createdBy}" rowcount="1" />
								<fmt:formatDate value="${asn.createdOn}" type="both" dateStyle="SHORT" timeStyle="SHORT"/>
							</li>
							<c:if test="${asn.sourceApi eq 'CWMS'}"> 
							<li>
								<label><spring:message code="manageprebooking.table.exchangeformat"/>:</label>
								<span>${asn.exchangeFormat.name}</span>
							</li>
							</c:if>
						</ol>
					</div>
					<!-- end of right column -->
				</fieldset>
			</div>
			<!-- sub navigation menu which displays and hides sections of page as well as linking to external pages -->
			<div id="subnav">
				<dl>
					<dt>
						<a href="#" onclick="switchMenuFocus(menuElements, 'prebookingjobitem-tab', false); return false; " 
								id="prebookingjobitem-link" class="selected" 
								title='<spring:message code="viewprebooking.viewitemsofprebookingjob"/>'>
							<spring:message code="viewprebooking.jobitems"/> (<span class="jobItemsSize">${asn.asnItems.size()}</span>)
						</a>
					</dt>
				</dl>
			</div>
			<!-- end of sub navigation menu -->
			<!-- infobox contains all job functionality and is styled with nifty corners -->
 			<div class="infobox"> 
			<!-- this section displays all job items for the selected job --> 
 				<div id="prebookingjobitem-tab">
				<!-- div to hold all the tabbed submenu  -->
 					<div id="tabmenu">
					<!-- div to hold all content which is to be changed using the tabbed submenu  -->
 						<div class="tab-box"> 
						<!-- div displaying all job items  -->
 							<div id="jobitem_default-tab"> 
 								<div class="left bold"><spring:message code="viewprebooking.items.message"/></div>
 								<table id="jobitemlist" class="tablesorter" summary="This table lists all job items for the current job"> 
 									<thead> 
 										<tr ${ not empty asn.job ? 'style="display:none"' :''}  >
											<td id="newAnalysis" colspan="8">
												<a href="viewefsynthesis.htm?asnid=${asn.id}" class="mainlink-float" >
													<spring:message code="viewprebooking.items.newanalysis"/>
												</a>
											</td>
										</tr>
 										<tr> 
 											<th scope="col"><spring:message code="viewprebooking.items.index"/></th>   
 											<th scope="col"><spring:message code="viewprebooking.items.instrument"/></th> 
 											<th scope="col"><spring:message code="viewprebooking.items.status"/></th>   
 											<th scope="col"><spring:message code="viewprebooking.items.comment"/></th>   
 										</tr> 
 									</thead> 
 									<tfoot> 
 										<tr> 
 											<td colspan="8">&nbsp;</td> 
 										</tr> 
 									</tfoot> 
 									<tbody> 
										<c:if test="${empty asn.asnItems}">
											<tr>
												<td colspan="4" class="center bold"><spring:message code="viewprebooking.items.noitems"/></td>
											</tr>
										</c:if>
										<c:set var="rowcount1" value="0"/>
										<c:forEach items="${asn.asnItems}" var="item">
											<tr>
												<td class="bold center">${item.index}</td>
												<td>
													<links:showInstrumentLink instrument="${item.instrument}"/>
												</td>
												<td class="bold center">${fn:replace(item.status, '_', ' ')}</td>
												<td class="bold center">${item.comment}</td>
											</tr>
											<c:set var="rowcount1" value="${rowcount1 + 1}"/>
										</c:forEach>
 									</tbody> 
 								</table> 
 							</div> 
							<!-- end of div displaying all job items  -->
 						</div> 
						<!-- end of div to hold all tabbed submenu content  -->
 					</div> 
					<!-- end of div to hold tabbed submenu  -->
 				</div> 
				<!-- end job item  -->

 				<div class="clear"></div> 
 			</div> 
			<!-- end of infobox -->
			
		</div>
		<!-- end of main job div -->
	</jsp:body>
</t:crocodileTemplate>