<%-- File name: /trescal/core/logistics/scanitems.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<spring:message code="scanitemsinout.scanin" />
			&nbsp;/&nbsp;
			<spring:message code="scanitemsinout.scanout" />
		</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/tools/ScanItemInOutPlugin.js'></script>
		<script>
		var focusElement = '${focusElement}';
		</script>
	</jsp:attribute>
	<jsp:body>
		<jobs:scanItemsInOut defaultAddress="${defaultAddress}" businessAddresses="${businessAddresses}"/>
	</jsp:body>
</t:crocodileTemplate>