<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<script src='script/trescal/core/logistics/prebookingjob/prebooking.js'></script>

<t:crocodileTemplate>

	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="manageprebooking.pagetitle" /></span>
	</jsp:attribute>
	
	<jsp:body>
		<div id="manageprebooking" class="infobox">	
			<fieldset>
				<legend><spring:message code="manageprebooking.legendfieldset" /></legend>
					<ol>
					<form:form modelAttribute="form">
						
						<li>
							<span for="manageprebookingcompany" class="float-left"><spring:message code="manageprebooking.form.company" /> :</span>
							
							<div class="float-left extendPluginInput">
								<div class="compSearchJQPlugin">
									<input type="hidden" name="field" value="coid" />
									<input type="hidden" name="compCoroles" value="client" />
									<input type="hidden" name="tabIndex" value="1" />
									<input type="hidden" name="compIdPrefill" value="${form.coid}" />
									<input type="hidden" name="compNamePrefill" value="${form.coname}" />
									<!-- company results listed here -->
								</div>
							</div>
							
							<span for="manageprebookingsubdiv" class="float-left pad-left"><spring:message code="manageprebooking.form.businesssubdiv" /> :</span>
							
							<select id="manageprebookingsubdiv" name="bsubdivid" class="float-left">
								<c:forEach items="${businessSubdivs}" var="bs">
									<option value="${bs.subdivid}" ${selectedBusinessSubdivId==bs.subdivid?' selected':''}>${bs.subname}</option>
								</c:forEach>
							</select>
							
							<span for="manageprebookingassocjob" class="float-left pad-left"><spring:message code="manageprebooking.form.associatedjob" /> :</span>
							<form:checkbox id="manageprebookingassocjob" path="associatedWithJob"/>
							
						</li>
						<li>
							<input type="submit" value="<spring:message code="manageprebooking.form.submit" />"/>
						</li>
						
						<li>
						
						<spring:message code="manageprebooking.addnew" /> :
							<cwms:securedLink permission="PREBOOKING_JOB_ADD" collapse="True" >
								<img src="img/icons/add.png"
									width="16" height="16" alt="" title="" onclick=""
									id="newPrebooking" />
							</cwms:securedLink>
							<input type="hidden" id="pageNo" name="pageNo" />
 							<t:paginationControl resultset="${rs}"/>
							
							<table id="table_manageprebooking" class="default2" summary="This table lists all events">	
								<thead>
									<tr>
										<th class="codefault">N°</th>
										<th class="codefault"><spring:message code="company" /></th>
										<th class="codefault"><spring:message code="subdiv" /></th>
										<th class="codefault"><spring:message code="contact" /></th>
										<th class="codefault"><spring:message code="manageprebooking.table.clientref" /></th>
										<th class="codefault"><spring:message code="newcalibration.items" /></th>
										<th class="codefault"><spring:message code="manageprebooking.table.job" /></th>
										<th class="codefault"><spring:message code="manageprebooking.table.exchangeformat" /></th>
										<th class="codefault"><spring:message code="instmod.lastmodified" /></th>
										<th class="codefault"><spring:message code="manageprebooking.table.action" /></th>
									</tr>
								</thead>
								<tbody>
									<c:if test="${ asnList.size() eq 0 }">
										<td colspan="10" ><spring:message code="noresults" /></td>
									</c:if>
								
							    	<c:forEach items="${ asnList }" var="asn">
										<tr>
											<td>${ asn.id }</td>
											<td>
												<links:companyLinkDWRInfo company="${asn.prebookingJobDetails.con.sub.comp}" rowcount="${0}" copy="${true}" />
											</td>
											<td>
												<links:subdivLinkDWRInfo rowcount="1" subdiv="${asn.prebookingJobDetails.con.sub}"/>
											</td>
											<td>
												${asn.prebookingJobDetails.con.firstName}&nbsp;${asn.prebookingJobDetails.con.lastName}
											</td>
											<td>${ asn.prebookingJobDetails.clientRef }</td>
											<td>${ asn.asnItems.size() }</td>
											<td><c:if test="${asn.job != null}">
													<links:jobnoLinkDWRInfo rowcount="1"
														jobno="${asn.job.jobno}" jobid="${asn.job.jobid}"
														copy="false" />
												</c:if></td>
											<td>${ asn.exchangeFormat.name }</td>
											<td><links:lastModifiedContactLink entity="${asn}" /></td>
											<td style="width: 80px;">
												<a href="viewprebooking.htm?asnid=${ asn.id }"
												class="float-left padleft10">
													<img src="img/icons/showinfo.png" width="16" height="16"
													alt="<spring:message code='manageprebooking.table.action.details'/>" title="<spring:message code='manageprebooking.table.action.details'/>" class="img_marg_bot">
												</a>
												<a style="display: ${ (empty asn.job)? 'block' : ' none'}"
												href="deleteprebooking.htm?asnid=${ asn.id }"
												class="float-left padleft10">
													<img src="img/icons/delete.png" width="16" height="16"
													alt="<spring:message code='manageprebooking.table.action.delete'/>" title="<spring:message code='manageprebooking.table.action.delete'/>" class="img_marg_bot">
												</a>
												<a href="<c:url value='downloadPrebookingJobFile'/>?id=${asn.id}" target="blank" class="mainlink padleft10">
													<img src="img/icons/download.png" width="16" height="16"
													alt="<spring:message code='manageprebooking.table.action.download'/>" title="<spring:message code='manageprebooking.table.action.download'/>" class="img_marg_bot">
												</a>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</li>
						</form:form>
					</ol>							
				</fieldset>
			</div>
		</jsp:body>
</t:crocodileTemplate>