<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>



<t:crocodileTemplate>

	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="finalsynthesis.tabtitle" /></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/logistics/jobfinalsynthesis/jobfinalsynthesis.js'></script>
		<script type="module" src="script/components/cwms-job-final-synthesis/main.js"></script>
		<script type='module' src='script/components/cwms-overlay/cwms-overlay.js' ></script>
		<script type='module' src='script/components/cwms-translate/cwms-translate.js'></script>
    </jsp:attribute>
<jsp:body>

	<!-- error section displayed when form submission fails -->
	<t:showErrors path="jobFinalSynthForm.*" showFieldErrors="true" />

	<div id="jobfinalsyntesis" class="infobox">
		<form:form method="POST" modelAttribute="jobFinalSynthForm" onsubmit="document.getElementById('submitData').show();">
		   <input type="hidden" name="disbledCreateJob" id="disbledCreateJob" value="${disabled}"/>
		   <input type="hidden" name="addItemsToJobAlreadyCreated" id="addItemsToJobAlreadyCreated" value="${addItemsToJobAlreadyCreated}"/>
		   <input type="hidden" name="jobid" id="jobid" value="${jobid}"/>
			<fieldset>

				<legend>
					<spring:message code="finalsynthesis.title" />
					&nbsp;&nbsp;(<spring:message code="newcalibration.items" />: ${fn:length(jobFinalSynthForm.identifiedJobItemsFinalSynthDTO)+fn:length(jobFinalSynthForm.nonIdentifiedJobItemsFinalSynthDTO)})
				</legend>
				
				<ol>
					<li>
						<div class="displaycolumn-10">
							<label for="clientcompany">
								<spring:message code="formsynthesis.clientcompany" />:
							</label>
						</div>
						<div class="float-left">
							<links:companyLinkDWRInfo company="${clientcompany}" rowcount="${0}" copy="${true}" />
						</div>
					</li>
					
					<li>
					<cwms-final-synthesis-identified-items
					 tableHeader="${cwms:objectToJson(tableHeader)}"
					 items="${cwms:collectionToJson(jobFinalSynthForm.identifiedJobItemsFinalSynthDTO)}"
					 statuses="${cwms:objectToJson(status)}"
					 serviceTypes="${cwms:objectToJson(servicetypes)}"
					 >

					</cwms-final-synthesis-identified-items>
					</li>
					<li>
						<cwms-final-synthesis-nonidentified-items
						groupId="${clientcompany.companyGroup.id}"
						companyId="${clientcompany.coid}"
					 	statuses="${cwms:objectToJson(status)}"
					 	serviceTypes="${cwms:objectToJson(servicetypes)}"
							tableHeader="${cwms:objectToJson(tableHeader)}"
							items="${cwms:objectToJson(jobFinalSynthForm.nonIdentifiedJobItemsFinalSynthDTO)}"
						></cwms-final-synthesis-nonidentified-items>
					</li>
						
					
					<li>
						<div class="displaycolumn-25" >
							<label><spring:message code="finalsynthesis.barcode" />:</label>
							<input type="text" id="barcodeScanInput" onkeyup="checkItemByPlantid($j(this).val());"/>
						</div>
						<div class="displaycolumn-25" >
							<label><spring:message code="plantno" />:</label>
							<input type="text" id="plantnoScanInput" onkeyup="checkItemByPlantno($j(this).val());"/>
						</div>
						<div class="displaycolumn-30" >
							<label><spring:message code="finalsynthesis.generalcomment" />:</label>
							<form:textarea path="pbjf.comments"/>
						</div>
						
					</li>
					
					<li>
						<div class="displaycolumn-15">
							<label for="submit">&nbsp;</label>
						</div>
						<div class="float-left">
							<input type="submit" name="newanalysis" class="button"
									value="<spring:message code='filesynthesis.buttonnewanalysis'/>" />
							<button disabled>
								<spring:message code='filesynthesis.buttonprint' /> 
							</button>
							<input type="submit" name="save" id="save" class="button" 
									value="<spring:message code='addjob.headtext'/> " ${disabled}/>
							<button disabled>
								<spring:message code='finalsynthesis.boutonsave'  /> 
							</button>
							<c:if test="${disabled  == 'disabled'}">
							<input type="submit" name="additems" class="button" 
									value="<spring:message code='addnewitemstojob.additems'/> " />
						    </c:if>
						</div>
						<div class="clear"></div>
					</li>
				</ol>							
			</fieldset>
			
			<form:hidden path="pbjf.recordType" />
			<form:hidden path="pbjf.bpo" />
			<form:hidden path="pbjf.defaultPO" />
			<form:hidden path="pbjf.addrid" />
			<form:hidden path="pbjf.bookedInAddrId" />
			<form:hidden path="pbjf.bookedInLocId" />
			<form:hidden path="pbjf.clientref" />
			<form:hidden path="pbjf.coid" />
			<form:hidden path="pbjf.currencyCode" />
			<form:hidden path="pbjf.estCarriageOut" />
			<form:hidden path="pbjf.jobType" />
			<form:hidden path="pbjf.locid" />
			<form:hidden path="pbjf.personid" />
			<form:hidden path="pbjf.subdivid" />
			<form:hidden path="pbjf.recordType" />
			<form:hidden path="pbjf.exchangeFormat" />
			<form:hidden path="pbjf.asnId" />
			<form:hidden path="pbjf.receiptDate" />
			<form:hidden path="pbjf.pickupDate" />
			<form:hidden path="pbjf.overrideDateInOnJobItems" />
			<form:hidden path="pbjf.transportIn" />
			<form:hidden path="pbjf.carrier" />
			<form:hidden path="pbjf.trackingNumber" />
			<form:hidden path="pbjf.transportOut" />
			<form:hidden path="pbjf.numberOfPackages" />
			<form:hidden path="pbjf.packageType" />
			<form:hidden path="pbjf.storageArea" />
			<c:forEach items="${jobFinalSynthForm.pbjf.poCommentList}" varStatus="st">
				<form:hidden path="pbjf.poCommentList[${st.index}]"/>
			</c:forEach>
			
			<form:hidden path="pbjf.ponumber" />
			<c:forEach items="${jobFinalSynthForm.pbjf.poNumberList}" varStatus="st">
				<form:hidden path="pbjf.poNumberList[${st.index}]" />
			</c:forEach>
			<c:forEach items="${jobFinalSynthForm.pbjf.poIdList}" varStatus="st">
				<form:hidden path="pbjf.poIdList[${st.index}]" />
			</c:forEach>
			
		</form:form>
	</div>
</jsp:body>
</t:crocodileTemplate>

<cwms-overlay id="submitData">
	<cwms-translate slot="cwms-overlay-title" code="createdelnote.wait" >Loading...please wait</cwms-translate>
    <div slot="cwms-overlay-body">
    	<center>
            <img src="img/icons/ajax-loader.gif" width="31" height="31" alt="" />
        </center>
    </div>
</cwms-overlay>