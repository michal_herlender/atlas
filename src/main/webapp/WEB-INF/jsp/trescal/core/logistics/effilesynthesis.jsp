<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>

	<jsp:attribute name="header">
	<span class="headtext"><spring:message
				code="filesynthesis.tabtitle" /></span>
</jsp:attribute>
	<jsp:attribute name="scriptPart">
        <script type="module" src="script/components/cwms-effilesynthesis/cwms-effilesynthesis.js"></script>
	</jsp:attribute>
	<jsp:body>

	<t:showErrors path="addPrebookingJobForm.prebookingJobForm.*" showFieldErrors="false" />
	
	<div id="addexchangeformat" class="infobox">
		<form:form method="POST" modelAttribute="addPrebookingJobForm">
			<fieldset>
				<legend>
					<spring:message code="filesynthesis.title" />
				</legend>
				<ol>
					<li>
						<div class="displaycolumn-10">
							<label for="clientcompany">
								<spring:message code="formsynthesis.clientcompany" />:
							</label>
						</div>
						<div class="float-left">
							<links:companyLinkDWRInfo company="${clientcompany}"
									rowcount="${0}" copy="${true}" />
						</div>
					</li>
					<li>
                    <cwms-effilesynthesis tableHeader="${cwms:objectToJson(tableHeader)}"
                    servicesType ="${cwms:objectToJson(servicesType)}"
                    groupId="${clientcompany.companyGroup.id}"
                    items="${cwms:objectToJson(addPrebookingJobForm.asnItemsDTOList)}"></cwms-effilesynthesis>
					</li>
					<li>
						<div class="displaycolumn-15">
							<label for="submit">&nbsp;</label>
						</div>
						<div class="float-left">
							<input type="submit" name="newanalysis" class="button"
									value="<spring:message code='filesynthesis.buttonnewanalysis'/>" />
							<button disabled>
								<spring:message code='filesynthesis.buttonprint' /> 
							</button>
							<c:set var="submitButtonValue">
								<c:if test="${addPrebookingJobForm.prebookingJobForm.asnId == null}">
									<spring:message code='filesynthesis.buttoncreate'/>
								</c:if>
								<c:if test="${addPrebookingJobForm.prebookingJobForm.asnId != null}">
									<spring:message code='filesynthesis.buttonupdate'/>
								</c:if>
							</c:set>
							<input type="submit" name="save" class="button" value="${submitButtonValue}" />
						</div>
						<div class="clear"></div>
					</li>
				</ol>							
			</fieldset>
			
			<form:hidden path="prebookingJobForm.bpo" />
			<form:hidden path="prebookingJobForm.defaultPO" />
			<form:hidden path="prebookingJobForm.addrid" />
			<form:hidden path="prebookingJobForm.bookedInAddrId" />
			<form:hidden path="prebookingJobForm.bookedInLocId" />
			<form:hidden path="prebookingJobForm.clientref" />
			<form:hidden path="prebookingJobForm.coid" />
			<form:hidden path="prebookingJobForm.currencyCode" />
			<form:hidden path="prebookingJobForm.estCarriageOut" />
			<form:hidden path="prebookingJobForm.jobType" />
			<form:hidden path="prebookingJobForm.locid" />
			<form:hidden path="prebookingJobForm.personid" />
			<form:hidden path="prebookingJobForm.poCommentList" />
			<form:hidden path="prebookingJobForm.ponumber" />
			<form:hidden path="prebookingJobForm.poNumberList" />
			<form:hidden path="prebookingJobForm.subdivid" />
			<form:hidden path="prebookingJobForm.recordType" />
			<form:hidden path="prebookingJobForm.exchangeFormat" />
			<form:hidden path="prebookingJobForm.asnId" />
			<form:hidden path="prebookingJobForm.prebookingFileId" />
			
		</form:form>
	</div>
</jsp:body>
</t:crocodileTemplate>