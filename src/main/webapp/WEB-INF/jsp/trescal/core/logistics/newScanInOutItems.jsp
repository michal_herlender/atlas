<%-- File name: /trescal/core/logistics/scanitems.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<t:crocodileTemplate>

	<jsp:attribute name="header">
		<span class="headtext">
			<spring:message code="scanitemsinout.scanin" />
			&nbsp;/&nbsp;
			<spring:message code="scanitemsinout.scanout" />
		</span>
	</jsp:attribute>
	
	<jsp:attribute name="scriptPart">
		<script type="module" src="script/components/cwms-translate/cwms-translate.js"></script>
		<script type="module" src="script/components/cwms-scan-in-out-plugin/cwms-scan-in-out-plugin.js"></script>		
		
	</jsp:attribute>
	<jsp:body>
		<div id="scanItemsInOut" class="infobox">
			<cwms-scan-in-out-plugin defaultaddres-id="${defaultAddressId}"> </cwms-scan-in-out-plugin>
				<div class="clear">&nbsp;</div>
		</div>
	</jsp:body>

</t:crocodileTemplate>