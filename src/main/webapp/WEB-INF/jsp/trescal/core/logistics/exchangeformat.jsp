<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<style>
.myDragClass {
	border: solid 3px gray;
	border-radius: 3px
}

.delimg {
	cursor: pointer;
}

#newField, #addAliasGroups, #updateAliasGroups,#refreshAliasGroups {
	cursor: pointer;
	vertical-align: bottom;
}

.hidden {
	display: none;
}
</style>

<script src='script/trescal/core/logistics/exchangeformat/exchangeformat.js'></script>
<script src="script/trescal/core/logistics/exchangeformat/aliasgroup/aliasGroup.js"></script>
<script src='script/thirdparty/jQuery/jquery.js' ></script>
	
	<script>
	var columnsTypes = new Array();
	var type = '${ exchangeformatform.type }';

		<c:forEach var="fieldName" items="${ exchangeformatfieldname }">

			types = new Array();
			<c:forEach var="type" items="${ fieldName.types }">
				types.push('${ type }');
			</c:forEach>
			
			columnsTypes.push({
				'name': "${fieldName.name}",
				'translation': "${fieldName.value}",
				'types' : types
			});
			
		</c:forEach>
		
		$(document).ready(function() {
		    var val= $j('#aliasgroups').find(':selected').val();
		    if(val!=null){
				$j("#aliasGroupDetailTable tbody tr").addClass("hid");
				$j("tr#bodyTr_"+val+"").removeClass("hid");
			}
		});
		
	</script>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message
				code="exchangeformat.add" /></span>
	</jsp:attribute>
	<jsp:body>

		<t:showErrors path="exchangeformatform.*" showFieldErrors="true" />
		
		<div id="addexchangeformat" class="infobox">
		    <c:set var="index" value="0" />
			<form:form method="POST" modelAttribute="exchangeformatform">
				<fieldset>
					<legend>
						<spring:message code="exchangeformat.add" />
					</legend>
					
						<form:hidden path="refererUrl" value="${refererUrl}" />
					
						<form:hidden path="fromBusinessCompany"
							value="${fromBusinessCompany}" />
						<form:hidden path="fromBusinessSubdiv"
							value="${fromBusinessSubdiv}" />
						<form:hidden path="fromClient" value="${fromClient}" />
       				 
					<ol>
						<!-- EF name -->
						<li>
							<div class="displaycolumn-15">
								<label for="efname">
									<spring:message code="exchangeformat.name" />:
								</label>
							</div>
							<div class="float-left">
								<form:hidden path="id" value="${ id }" />
								<form:input path="name" id="efname" size="64" />
							</div>
							<div class="clear"></div>
						</li>
						
						<!-- EF description -->
						<li>
							<div class="displaycolumn-15">
								<label for="efdescription">
									<spring:message code="exchangeformat.description" />:
								</label>
							</div>
							<div class="float-left">
								<form:textarea maxLength="200" path="description" id="efdescription" cols="90"
									rows="5" />
							</div>
							<div class="clear"></div>
						</li>
						
						<c:choose>
							<c:when test="${ action eq 'create'}">
								<c:if test="${ exchangeformatform.fromBusinessCompany && exchangeformatform.level ne 'GLOBAL'}">
									<!-- business company fixed -->
									<li>
										<div class="displaycolumn-15">
											<label for="businesscompany">
												<spring:message code="exchangeformat.buscompanylabel" />:
											</label>
										</div>
										<div class="float-left">
											<links:companyLinkDWRInfo company="${businessCompany}"
												rowcount="0" copy="true" />
											<form:hidden path="businessCompanyId" id="businesscompany"
												value="${ businessCompany.coid }" /> 
										</div>
									</li>
									<!-- client company to be selected -->
									<li>
										<div class="displaycolumn-15">											
											<label><spring:message code="exchangeformat.clientcompanylabel" />:</label>
										</div>
										<div class="float-left">
											<spring:bind path="businessCompanyId">
												<div class="float-left extendPluginInput">
													<div class="compSearchJQPlugin">
														<input type="hidden" name="field"
																value="clientCompanyId" />
														<input type="hidden" name="compCoroles" value="client" />
														<input type="hidden" name="tabIndex" value="1" />
													</div>
												</div>							
											</spring:bind>
										</div>
										<!-- clear floats and restore page flow -->
										<div class="clear"/>
									</li>
								</c:if>
								
								<c:if test="${ exchangeformatform.fromBusinessSubdiv }">
									<!-- business subdiv fixed -->
									<li>
										<div class="displaycolumn-15">
											<label for="subdivcompany">
												<spring:message code="exchangeformat.bussubdivlabel" />:
											</label>
										</div>
										<div class="float-left">
											<links:subdivLinkDWRInfo rowcount="" subdiv="${businessSubdiv}" />
											<form:hidden path="businessSubdivId" id="businesssubdiv"
												value="${ businessSubdiv.subdivid }" /> 
										</div>
									</li>
									<!-- client company to be selected -->
									<li>
										<div class="displaycolumn-15">											
												<label><spring:message
													code="exchangeformat.clientcompanylabel" />:</label>
										</div>
										<div class="float-left">
										<spring:bind
												path="clientCompanyId">
											<div class="float-left extendPluginInput">
												<div class="compSearchJQPlugin">
													<input type="hidden" name="field" value="clientCompanyId" />
													<input type="hidden" name="compCoroles" value="client" />
													<input type="hidden" name="tabIndex" value="1" />
												</div>
											</div>							
										</spring:bind>
										</div>
										<!-- clear floats and restore page flow -->
										<div class="clear"></div>
									</li>
								</c:if>
								
								<c:if test="${ exchangeformatform.fromClient && exchangeformatform.level ne 'GLOBAL'}">
									<!-- choose with a radio button whether it's a business company or a business subdiv
										and search for them -->
									<li>
										<div class="displaycolumn-15">
											<label for="clientCompany">
												<spring:message code="exchangeformat.buscompanylabel" />/
												<spring:message code="exchangeformat.bussubdivlabel" />:
											</label>
										</div>
										
										<div class="float-left">
											<input type="radio" name="companySelected" value="true"
												${exchangeformatform.companySelected?'checked':''}
												onchange="chooseSubdivOrCompanyRadioChanged();">
												<spring:message code="exchangeformat.buscompanylabel" />
											</input>
											<input type="radio" name="companySelected" value="false"
												${exchangeformatform.companySelected?'':'checked'}
												onchange="chooseSubdivOrCompanyRadioChanged();">
												<spring:message code="exchangeformat.bussubdivlabel" />
											</input>
											<div id="businessCompanySelector" class="${exchangeformatform.companySelected?'':'hidden'}" >
												<spring:bind path="businessCompanyId">
													<div class="float-left extendPluginInput">
														<div class="compSearchJQPlugin">
															<input type="hidden" name="field" value="businessCompanyId" />
															<input type="hidden" name="compCoroles" value="business" />
															<input type="hidden" name="tabIndex" value="1" />
															<input type="hidden" name="compIdPrefill"
																value="${businessCompany.coid}" />
															<input type="hidden" name="compNamePrefill"
																value="${businessCompany.coname}" />
															<!-- company results listed here -->
														</div>
													</div>							
												</spring:bind>
											</div>
											<div id="businessSubdivSelector" class="${exchangeformatform.companySelected?'hidden':''}">
												<div id="cascadeSearchPlugin">
													<input type="hidden" id="cascadeRules" value="subdiv" />
													<input type="hidden" id="compCoroles" value="business" />
													<input type="hidden" id="compindex" value="1" />
													<input type="hidden" id="prefillIds"
														value="${businessCompany.coid},${businessSubdiv.subdivid}" />
												</div>
											</div>
										</div>
									</li>
									<!-- client company fixed -->
									<li>
										<div class="displaycolumn-15">
											<label for="clientCompany">
												<spring:message code="exchangeformat.clientcompanylabel" />:
											</label>
										</div>
										<div class="float-left">
											<links:companyLinkDWRInfo company="${clientCompany}" rowcount="0"
												copy="true" />
											<form:hidden path="clientCompanyId" value="${clientCompany.coid}" />
										</div>
									</li>
								</c:if>
							</c:when>				
									
							<c:when test="${ action eq 'edit'}">
							
								<c:if test="${ exchangeformatform.fromBusinessCompany && exchangeformatform.level ne 'GLOBAL'}">
									<!-- business company fixed -->
									<li>
										<div class="displaycolumn-15">
											<label for="businesscompany">
												<spring:message code="exchangeformat.buscompanylabel" />:
											</label>
										</div>
										<div class="float-left">
											<links:companyLinkDWRInfo company="${businessCompany}"
												rowcount="0" copy="true" />
											<form:hidden path="businessCompanyId" id="businesscompany"
												value="${ businessCompany.coid }" /> 
										</div>
									</li>
									<!-- client company to be selected -->
									<li>
										<div class="displaycolumn-15">											
												<label><spring:message code="exchangeformat.clientcompanylabel" />:</label>
										</div>
										<div class="float-left">
											<spring:bind path="clientCompanyId">
												<div class="float-left extendPluginInput">
													<div class="compSearchJQPlugin">
														<input type="hidden" name="field" value="clientCompanyId" />
														<input type="hidden" name="compCoroles" value="client" />
														<input type="hidden" name="tabIndex" value="1" />
														<input type="hidden" name="compIdPrefill" value="${clientCompany.coid}" />
														<input type="hidden" name="compNamePrefill"	value="${clientCompany.coname}" />
													</div>
												</div>							
											</spring:bind>
										</div>
										<!-- clear floats and restore page flow -->
										<div class="clear"></div>
									</li>
								</c:if>
								
								<c:if test="${ exchangeformatform.fromBusinessSubdiv && exchangeformatform.level ne 'GLOBAL'}">
									<!-- business subdiv fixed -->
									<li>
										<div class="displaycolumn-15">
											<label for="subdivcompany">
												<spring:message code="exchangeformat.bussubdivlabel" />:
											</label>
										</div>
										<div class="float-left">
											<links:subdivLinkDWRInfo rowcount="" subdiv="${businessSubdiv}" />
											<form:hidden path="businessSubdivId" id="businesssubdiv"
												value="${ businessSubdiv.subdivid }" /> 
										</div>
									</li>
									<!-- client company to be selected -->
									<li>
										<div class="displaycolumn-15">											
												<label><spring:message
													code="exchangeformat.clientcompanylabel" />:</label>
										</div>
										<div class="float-left">
										<spring:bind
												path="clientCompanyId">
											<div class="float-left extendPluginInput">
												<div class="compSearchJQPlugin">
													<input type="hidden" name="field" value="clientCompanyId" />
													<input type="hidden" name="compCoroles" value="client" />
													<input type="hidden" name="tabIndex" value="1" />
													<input type="hidden" name="compIdPrefill" value="${clientCompany.coid}" />
													<input type="hidden" name="compNamePrefill"	value="${clientCompany.coname}" />
													<!-- company results listed here -->
												</div>
											</div>							
										</spring:bind>
										</div>
										<!-- clear floats and restore page flow -->
										<div class="clear"></div>
									</li>
								</c:if>
								
								<c:if test="${ exchangeformatform.fromClient && exchangeformatform.level ne 'GLOBAL'}">
								
									<!-- choose with a radio button whether it's a business company or a business subdiv
										and search for them -->
									<li>
										<div class="displaycolumn-15">
											<label for="clientCompany">
												<spring:message code="exchangeformat.buscompanylabel" />/
												<spring:message code="exchangeformat.bussubdivlabel" />:
											</label>
										</div>
									
										<div class="float-left">
											<input type="radio" name="companySelected" value="true"
												${exchangeformatform.companySelected?'checked':''}
												onchange="chooseSubdivOrCompanyRadioChanged();" >
												<spring:message code="exchangeformat.buscompanylabel" />
											</input>
											<input type="radio" name="companySelected" value="false" 
												${exchangeformatform.companySelected?'':'checked'}
												onchange="chooseSubdivOrCompanyRadioChanged();">
												<spring:message code="exchangeformat.bussubdivlabel" />
											</input>
											<div id="businessCompanySelector" class="${exchangeformatform.companySelected?'':'hidden'}" >
												<spring:bind path="businessCompanyId">
													<div class="float-left extendPluginInput">
														<div class="compSearchJQPlugin">
															<input type="hidden" name="field"
																value="businessCompanyId" />
															<input type="hidden" name="compCoroles" value="business" />
															<input type="hidden" name="tabIndex" value="1" />
															<input type="hidden" name="compIdPrefill" value="${businessCompany.coid}" />
															<input type="hidden" name="compNamePrefill"	value="${businessCompany.coname}" />
															<!-- company results listed here -->
														</div>
													</div>							
												</spring:bind>
											</div>
											<div id="businessSubdivSelector" class="${exchangeformatform.companySelected?'hidden':''}">
												<div id="cascadeSearchPlugin">
													<input type="hidden" id="cascadeRules" value="subdiv" />
													<input type="hidden" id="compCoroles" value="business" />
													<input type="hidden" id="compindex" value="1" />
													<input type="hidden" id="prefillIds"
														value="${businessSubdiv.comp.coid},${businessSubdiv.subdivid}" />
												</div>
											</div>
										</div>
									</li>
								
									<!-- client company fixed -->
									<li>
										<div class="displaycolumn-15">
											<label for="clientCompany">
												<spring:message code="exchangeformat.clientcompanylabel" />:
											</label>
										</div>
										<div class="float-left">
											<links:companyLinkDWRInfo company="${clientCompany}" rowcount="0"
												copy="true" />
											<form:hidden path="clientCompanyId" value="${clientCompy.coid}" />
										</div>
									</li>

								</c:if>
							</c:when>		
						</c:choose>
						
						<!-- EF Type -->
						<c:choose >
						<c:when test="${ action eq 'edit'}">
						<li>
							<div class="displaycolumn-15">
								<label for="eftype">
									<spring:message code="exchangeformat.type" />:
								</label>
							</div>
							<div class="float-left">
							<span id="eftype">
									${ exchangeformatform.type.value }
						    </span>
						    <form:hidden path="type" value="${ exchangeformatform.type }"/>
							</div>
							<div class="clear"></div>
						</li>
						</c:when>
						<c:otherwise>
						<li>
							<div class="displaycolumn-15">
								<label for="eftype">
									<spring:message code="exchangeformat.type" />:
								</label>
							</div>
							<div class="float-left">
							<form:select id="eftype" path="type"
									items="${ exchangeformattypes }" itemLabel="value" itemValue="name" onchange="onFieldsNameChange($j(this).val());"></form:select>
							</div>
							<div class="clear"></div>
						</li>
						</c:otherwise>
						</c:choose>
						
						<!-- EF date format -->
						<li>
							<div class="displaycolumn-15">
								<label for="efdateformat">
									<spring:message code="exchangeformat.dateformat" />:
								</label>
							</div>
							<div class="float-left">
							<form:select id="df" path="dateFormat"
									items="${ exchangedateformatforselect }" itemLabel="value" itemValue="name"></form:select>
							</div>
							<div class="clear"></div>
						</li>
						
						<!-- lines to skip in excel file -->
					<c:if test="${exchangeformatform.level ne 'GLOBAL'}">
						<li>
							<div class="displaycolumn-15">
								<label for="efname">
									<spring:message code="exchangeformat.linestoskip" />:
								</label>
							</div>
							<div class="float-left">
								<form:input path="linesToSkip" id="eflinestoskip" />
							</div>
							<div class="clear"></div>
						</li>
						
						<!-- sheet name in excel file -->
						<li>
							<div class="displaycolumn-15">
								<label for="efname">
									<spring:message code="exchangeformat.sheetname" />:
								</label>
							</div>
							<div class="float-left">
								<form:input path="sheetName" id="efsheetname" />
							</div>
							<div class="clear"></div>
						</li>
					</c:if>
						<li>
							<div class="displaycolumn-15">
								<span class="mainlink"><spring:message code="exchangeformat.FieldName"/> :</span>
							</div>
							<div class="float-left">
									<select id="fieldNameItems">
									</select>
								<img src="img/icons/add.png" width="16" height="16" alt=""
										title="" onclick="addFieldItem()" id="newField" />
							</div>
						</li>
						
						<li>
							<table id="table_ef" class="default2 companySubdivs"
									summary="This table lists all subdivisions within the selected company">	
								<thead>
									<tr>
										<th class="codefault">#</th>
										<th class="coname"><spring:message code="exchangeformat.FieldName"/></th>
										<th class="dcreated"><spring:message code="exchangeformat.Mandatory"/></th>
										<th class="nocontacts"><spring:message code="exchangeformat.Exceltemplate"/></th>
										<th class="nocontacts"><spring:message code="viewinstrument.actions"/></th>
									</tr>
								</thead>
								<tbody>
									<c:forEach
											items="${exchangeformatform.exchangeFormatFieldNameDetailsForm}"
											varStatus="loop" var="effnd">
				                        <tr class="even">
											<td class="center">
												<span>${ effnd.position }</span> 
												<input type="hidden"
													id="exchangeFormatFieldNameDetailsForm${ loop.index }.id"
													name="exchangeFormatFieldNameDetailsForm[${ loop.index }].id"
													value="${ effnd.id }" />
												<input class="position" type="hidden"
													id="exchangeFormatFieldNameDetailsForm${ loop.index }.position"
													name="exchangeFormatFieldNameDetailsForm[${ loop.index }].position"
													value="${ effnd.position }" />
											</td>
											<td class="name">${ effnd.fieldName.value }<input
													type="hidden"
													id="exchangeFormatFieldNameDetailsForm${ loop.index }.fieldName"
													name="exchangeFormatFieldNameDetailsForm[${ loop.index }].fieldName"
													value="${ effnd.fieldName }" /></td>
											<td class="center"><input type="checkbox"
													${ effnd.mandatory ? 'checked':'' }
													id="exchangeFormatFieldNameDetailsForm${ loop.index }.mandatory"
													name="exchangeFormatFieldNameDetailsForm[${ loop.index }].mandatory" /></td>
											<td class="center"><input type="text"
													value="${ effnd.templateName }"
													id="exchangeFormatFieldNameDetailsForm${ loop.index }.templateName"
													name="exchangeFormatFieldNameDetailsForm[${ loop.index }].templateName" /></td>
											<td class="center">
											<img src="img/icons/delete.png" height="16" width="16"
													alt="<spring:message code='delete'/>"
													title="<spring:message code='delete'/>"
													class="delimg" onclick="deleteFieldItem(this)"
													id="delete_${ effnd.fieldName.fullName }"
													data-field-name="${ effnd.fieldName.fullName }"
													data-field-id="${ effnd.fieldName }" />
											<!-- span onMouseOver="this.style.color='#0F0'" onMouseOut="this.style.color='#00F'">delete</span-->
											</td>
										</tr>
                    				</c:forEach>
								</tbody>
							</table>
						</li>
						

						
						<li>
							
							<div class="displaycolumn-15">
								<span class="mainlink"><spring:message code="aliasgroup"/> :</span>
							</div>
							<div class="float-left">
								<select id="aliasgroups" name="aliasGroupid"   onchange="filterBySelectedValue($j('#aliasgroups').find(':selected').val());">
									<c:choose>
										<c:when test="${not empty aliasgroupdtos}">
											<option value="0"></option>
											<c:forEach var="aliasgroup" items="${aliasgroupdtos}">
												<option  value="${aliasgroup.alaisGroupId}" ${ (aliasgroup.alaisGroupId eq exchangeformatform.aliasGroupid) || (aliasgroup.defaultForCurrentBusinessComp eq true && action eq 'create')  ? 'selected="selected"' : '' }>${aliasgroup.aliasGroupName}</option>
											</c:forEach>
										</c:when>
										<c:otherwise>
											<option value="0">-- <spring:message code="aliasgroup.noaliases"/> --</option> 
										</c:otherwise>
									</c:choose>
								</select>
							</div>
							
						
						</li>
						<li>
							<table id="aliasGroupDetailTable" class="default2 companySubdivs">
								<thead>
									<tr>
										<th class="codefault">#</th>
										<th class="codefault"><spring:message code="aliasgroup.column"/></th>
										<th class="codefault"><spring:message code="aliasgroup.value"/></th>
										<th class="codefault"><spring:message code="aliasgroup.alias"/></th>
									</tr>
								</thead>								
								<tbody>
									<c:forEach var="aliasGroups" items="${aliasgroupdtos}">
											<c:forEach var="aliasFields" items="${aliasGroups.aliasesForm}" varStatus="loop">
												<tr class="even hid" id="bodyTr_${ aliasGroups.alaisGroupId }" data-group-id="${ aliasGroups.alaisGroupId }">
													<td class="center"> ${ loop.index + 1 } </td>
													<td class="center"> ${aliasFields.effn.getValue()} </td>
													<c:choose>
														<c:when test="${aliasFields.effn eq 'EXPECTED_SERVICE'}">
															<td class="center"> <cwms:besttranslation translations="${aliasFields.serviceType.shortnameTranslation}" /> </td>
														</c:when>
														<c:when test="${aliasFields.effn eq 'INTERVAL_UNIT'}">
															<td class="center"> <spring:message code="${aliasFields.intervalUnit.singularNameCode}"/> </td>
														</c:when>
													</c:choose>
													
													<td class="center"> ${aliasFields.alias} </td>
												</tr>
											</c:forEach>
									</c:forEach>
								</tbody>
							
							</table>
						
						</li>
						
						<li>
							<div class="displaycolumn-15">
								<label for="submit">&nbsp;</label>
							</div>
							<div class="float-left">
								<input type="submit" name="submit" id="submit" ${ exchangeformatform.level eq 'GLOBAL' ? 'disabled' : ''}
									value="<spring:message code='exchangeformat.add'/>" />
							</div>
							<div class="clear"></div>
						</li>
					</ol>							
				</fieldset>
   </form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>