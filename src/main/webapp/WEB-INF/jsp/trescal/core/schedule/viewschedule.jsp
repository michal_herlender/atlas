<%-- File name: /trescal/core/schedule/viewschedule.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="schedule.viewschedule"/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/schedule/ViewSchedule.js'></script>
    </jsp:attribute>
    <jsp:body>
			<!-- error section displayed when form submission fails -->
				<form:errors path="form.*">
        		<div class="warningBox1">
                	 <div class="warningBox2">
                    	   <div class="warningBox3">
                        	    <h5 class="center-0 attention">
                            	     <spring:message code='schedule.errorupdatingschedule' />
                            	</h5>
                            	<c:forEach var="e" items="${messages}">
                               		<div class="center attention"><c:out value="${e}"/></div>
                            	</c:forEach>
                     	 </div>
                 	</div>
           		</div>
       	   		</form:errors>
				<!-- end error section -->
			
				<!-- infobox contains schedule summary and is styled with nifty corners -->				
				<div class="infobox">
																		
					<fieldset>
					
						<legend><spring:message code="schedule.scheduleinformation"/></legend>
						
						<!-- displays content in column 49% wide (left) -->
						<div class="displaycolumn">
						
							<ol>
								<li>
									<label><spring:message code="company"/>:</label>
									<span>
										<c:choose>
											<c:when test="${not empty form.schedule.contact}" >
												<links:companyLinkDWRInfo company="${form.schedule.contact.sub.comp}" rowcount="0" copy="true" />
											</c:when><c:otherwise>
												${form.schedule.freehandContact.company}
											</c:otherwise>
										 </c:choose>
									</span>
								</li>
								<li>
									<label><spring:message code="subdivision"/>:</label>
									<span>
										<c:choose>
											<c:when test="${not empty form.schedule.contact}" > 
												<links:subdivLinkDWRInfo rowcount="0" subdiv="${form.schedule.contact.sub}"/> 
											</c:when><c:otherwise>
												${form.schedule.freehandContact.subdiv}
											</c:otherwise>
										</c:choose>
									</span>
								</li>
								<li>
									<label><spring:message code="contact"/>:</label>
									<span>
										<c:choose>
											<c:when test="${not empty form.schedule.contact}" >
												<links:contactLinkDWRInfo contact="${form.schedule.contact}" rowcount="0"/>
											</c:when><c:otherwise>
												${form.schedule.freehandContact.contact} (${form.schedule.freehandContact.telephone})
											</c:otherwise>
										</c:choose>
									</span>
								</li>
								<li>
									<label><spring:message code="address"/>:</label>
									
									<div class="float-left padtop">
										<c:choose>
											<c:when test="${not empty form.schedule.address}">
												<div>${form.schedule.address.addr1}</div>
												<div>${form.schedule.address.addr2}</div>
												<div>${form.schedule.address.addr3}</div>
												<div>${form.schedule.address.town}</div>
												<div>${form.schedule.address.county}</div>
												<div>${form.schedule.address.postcode}</div>
												<div>${form.schedule.address.country.localizedName}</div>
											</c:when>
											<c:otherwise>	
												<t:showFreehandAddress address="${form.schedule.freehandContact.address}" separator="<br/>" />
											</c:otherwise>
										</c:choose>
									</div>
									
									<!-- clear floats and restore page flow -->
									<div class="clear"></div>
								</li>
							</ol>
						
						</div>
						<!-- end of left column -->
						
						<!-- displays content in column 49% wide (right) -->
						<div class="displaycolumn">
						
							<ol>
								<li>
									<label><spring:message code="schedule.schedulefrom"/>:</label>
									<span>${form.schedule.organisation.subname}</span>
								</li>
								<li>
									<label><spring:message code="type"/>:</label>
									<span>${form.schedule.type}</span>
								</li>
								<li>
									<label><spring:message code="schedule.source"/>:</label>
									<span>${form.schedule.source.source}</span>
								</li>	
								<li>
									<label><spring:message code="company.transportoption"/>:</label>
									<c:choose>
										<c:when test="${not empty form.schedule.transportOption.localizedName}">
											<span>${form.schedule.transportOption.localizedName}</span>
										</c:when>
										<c:otherwise>
											<span><cwms:besttranslation translations="${form.schedule.transportOption.method.methodTranslation}"/></span>
										</c:otherwise>
									</c:choose>
								</li>										
								<li>
									<label><spring:message code="schedule.scheduledate"/>:</label>
									<span>
										<c:choose>
											<c:when test="${not empty form.schedule.scheduleDate}">
												<fmt:formatDate value="${form.schedule.scheduleDate}" var="scheduleDate" type="date" dateStyle="SHORT"/>
												<fmt:formatDate value="${form.schedule.scheduleDate}" var="scheduleDatelink" type="date" dateStyle="SHORT" />
												<a href="showschedules.htm?filter=date&date=${scheduleDatelink}" class="mainlink">${scheduleDate}</a>
													
											</c:when>
											<c:otherwise>
												-- <spring:message code="schedule.nodateset"/> --
											</c:otherwise>
										</c:choose>
									</span>
								</li>											
								<li>
									<label><spring:message code="createdby"/>:</label>
									<span>${form.schedule.createdBy.name}&nbsp;&nbsp;&nbsp;(<fmt:formatDate value="${form.schedule.createdOn}" type="date" dateStyle="SHORT" />)</span>
								</li>
							</ol>

						</div>
						<!-- end of right column -->
					
					</fieldset>
				
				</div>
				<!-- end of infobox div -->
				
				<!-- sub navigation menu which displays and hides sections of page as well as linking to external pages -->
				<div id="subnav">
					<dl>
						<dt><a href="#" onclick=" switchMenuFocus(menuElements, 'deliveries-tab', false); return false; " id="deliveries-link" title="<spring:message code='schedule.viewdeliveriesforschedule'/>" class="selected"><spring:message code="schedule.scheduledeliveries"/></a>
						<c:set var="selectedLink" value=""/>
						<c:if test="${form.schedule.scheduleequipment.size() < 1}">
							<c:set var="selectedLink" value='style=" display: none; "' />
						</c:if>
						<dt ${selectedLink}><a href="#" onclick=" switchMenuFocus(menuElements, 'schedequip-tab', false); return false; " id="schedequip-link" title="<spring:message code='schedule.viewequipmentonschedule'/>"><spring:message code="schedule.scheduleequipment"/> (${form.schedule.scheduleequipment.size()})</a>
						<dt><a href="#" onclick=" switchMenuFocus(menuElements, 'editschedule-tab', false); return false;" id="editschedule-link" title="<spring:message code='schedule.editscheduledetails'/>"><spring:message code="edit"/></a></dt>
						<dt><a href="#" onclick=" deleteSchedule(${form.schedule.scheduleId}); return false; " title="<spring:message code='schedule.deleteschedule'/>"><spring:message code="delete"/></a></dt>
					</dl>
				</div>
				<!-- end of sub navigation menu -->
				
				<!-- infobox div with nifty corners applied -->
				<div class="infobox">
				
					<!-- this section contains all deliveries currently added to this schedule -->
					<div id="deliveries-tab">							
													
						<table class="default2 schedDeliveries" summary="<spring:message code='schedule.tabledeliveries'/>">
							
							<thead>
								<tr>
									<td colspan="4"><spring:message code="schedule.deliveries"/> (<span id="delCount">${scheduleDeliveries.size()}</span>)</td>
								</tr>
								<tr>
									<th class="delno" scope="col"><spring:message code="schedule.deliveryno"/></th>
									<th class="deltype" scope="col"><spring:message code="schedule.deliverytype"/></th>
									<th class="delitems" scope="col"><spring:message code="schedule.noofitems"/></th>
									<th class="deletedel" scope="col"><spring:message code="delete"/></th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<td colspan="4">&nbsp;</td>
								</tr>
							</tfoot>
							<tbody>
								<c:choose>
									<c:when test="${scheduleDeliveries.size() < 1}">
									<tr class="odd">
										<td colspan="4" class="center bold"><spring:message code="schedule.therearenodeliveriestodisplayforthisschedule"/></td>
									</tr>
									</c:when>
									<c:otherwise>
									<c:set var="rowcount" value="0"/>
									<c:forEach var="sd" items="${scheduleDeliveries}">
										<tr id="parent${rowcount}" <c:choose>
																	<c:when test="${rowcount % 2 == 0}"> class="even" 
																    </c:when><c:otherwise> class="odd" 
																    </c:otherwise>
															     </c:choose> onclick=" window.location.href='viewdelnote.htm?delid=${sd.delivery.deliveryid}' " onmouseover=" $j(this).addClass('hoverrow'); " onmouseout=" $j(this).removeClass('hoverrow'); ">
											<td>${sd.delivery.deliveryno}</td>
											<td>${sd.delivery.type.getDescription()}</td>
											<td>${sd.delivery.items.size()}<a href="#" id="itemsLink${rowcount}" onclick=" getDeliveryItems(event, ${rowcount}, ${sd.delivery.deliveryid}, ${sd.delivery.generalDelivery}); return false; "><img src="img/icons/items.png" width="16" height="16" alt="<spring:message code='schedule.viewdeliveryitems'/>" title="<spring:message code='schedule.viewdeliveryitems'/>" class="image_inline" /></a></td>
											<td class="center"><a href="#" onclick=" removeDeliveryFromSchedule(${sd.delivery.deliveryid}, ${rowcount}); return false; "><img src="img/icons/delete.png" width="16px;" height="16px;" alt="<spring:message code='schedule.removethisdeliveryfromtheschedule'/>" title="<spring:message code='schedule.removethisdeliveryfromtheschedule'/>" /></a></td>
										</tr>
										<c:set var="rowcount" value="${rowcount + 1}"/>
									</c:forEach>
									</c:otherwise>
								</c:choose>
							</tbody>
							
						</table>
																							
					</div>
					<!-- end of deliveries section -->
					
					<!-- this section displays schedule equipment information -->
					<div id="schedequip-tab" class="hid">
														
						<c:choose>
							<c:when test="${form.schedule.scheduleequipment.size() > 0}">
																												
							<table class="default2 schedEquipList" summary="<spring:message code='schedule.tableallscheduleequipment'/>">
								
								<thead>
									<tr>
										<td colspan="10">${form.schedule.scheduleequipment.size()} 
										<c:choose>
											<c:when test="${form.schedule.scheduleequipment.size() != 1}" > Items 
											</c:when>
											<c:otherwise>
												 Item 
											</c:otherwise>
										</c:choose>	Added To Schedule</td>
									</tr>
									<tr>
										<th class="itemno" scope="col"><spring:message code="item"/></th>
										<th class="item" scope="col"><spring:message code="schedule.mfrmodeldescription"/></th>
										<th class="serial" scope="col"><spring:message code="serialno"/></th>
										<th class="plant" scope="col"><spring:message code="plantno"/></th>
										<th class="qty" scope="col"><spring:message code="schedule.qty"/></th>
										<th class="itempo" scope="col"><spring:message code="schedule.itempo"/></th>
										<th class="caltype" scope="col"><spring:message code="caltype"/></th>
										<th class="turn" scope="col"><spring:message code="schedule.turn"/></th>
										<th class="fault" scope="col"><spring:message code="schedule.faulty"/></th>
										<th class="supported" scope="col"><spring:message code="schedule.supported"/></th>																
									</tr> 
								</thead>
								
							</table>										
							
							<c:set var="itemno" value="1"/>
																							
							<c:forEach var="equip" items="${form.schedule.scheduleequipment}">
							
								<table class="default4 schedEquipList" summary="<spring:message code='schedule.tableallscheduleequipment'/>">
						
									<tbody>
																									
										<tr>
											<td class="itemno">${itemno}</td>
											<c:choose>
												<c:when test="${equip.equipInst}">
												<td class="item">
													<c:choose>
														<c:when test="${equip.equipInst.mfr}"> 
															${equip.equipInst.mfr.name} -
														</c:when>
														<c:otherwise>
															${equip.equipInst.model.mfr.name} -
														</c:otherwise>
													</c:choose>
													${equip.equipInst.model.model} -
													${equip.equipInst.model.description.description}
												</td>
												<td class="serial">${equip.equipInst.serialno}</td>
												<td class="plant">${equip.equipInst.plantno}</td>
												<td class="qty">1</td>
												</c:when>	
												<c:otherwise>																			
												<td class="item">
													${equip.equipModel.mfr.name} -
													${equip.equipModel.model} -
													${equip.equipModel.description.description}
												</td>
												<td class="serial">-</td>
												<td class="plant">-</td>
												<td class="qty">${equip.qty}</td>
												</c:otherwise>	
											</c:choose>																	
											<td class="itempo">${equip.ponumber}</td>
											<td class="caltype"><t:showTranslationOrDefault translations="${equip.caltype.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/></td>
											<td class="turn">${equip.turn.turn}</td>
											<td class="fault">
												<c:choose>
													<c:when test="${equip.faulty}"> 
														<img src="img/web/tick.png" width="10" height="10" alt="<spring:message code='schedule.faulty'/>" title="<spring:message code='schedule.faulty'/>" />
													</c:when>
													<c:otherwise>	
														<img src="img/web/cross.png" width="10" height="10" alt="<spring:message code='schedule.notfaulty'/>" title="<spring:message code='schedule.notfaulty'/>" />
													</c:otherwise>
												</c:choose>		
											</td>
											<td class="supported">
												<c:choose>
													<c:when test="${equip.supported}"> 
														<img src="img/web/tick.png" width="10" height="10" alt="<spring:message code='schedule.supported'/>" title="<spring:message code='schedule.supported'/>" />
													</c:when>
													<c:otherwise>
														<img src="img/web/cross.png" width="10" height="10" alt="<spring:message code='schedule.notsupported'/>" title="<spring:message code='schedule.notsupported'/>" />
													</c:otherwise>
												</c:choose>	
											</td>																	
										</tr>
										
										<c:if test="${equip.faulty}">
											<tr>
												<td colspan="10">
													<span class="bold"><spring:message code="schedule.faultdescription"/>: </span>
													<c:choose>
														<c:when test="${equip.faultDesc.length() > 0}"> 
															${equip.faultDesc}
														</c:when>
														<c:otherwise>
															<spring:message code="schedule.nofaultdescriptionsupplied"/>
														</c:otherwise>
													</c:choose>	
												</td>
											</tr>
										</c:if>
										<c:if test="${equip.commentDesc != null && equip.commentDesc != ''}">
											<tr>
												<td colspan="10">
													<span class="bold">Item Comment: </span>
													<c:choose>
														<c:when test="${equip.commentDesc.length() > 0}">
															${equip.commentDesc}
														</c:when>
														<c:otherwise>
															<spring:message code="schedule.noitemcommentsupplied"/>
														</c:otherwise>
													</c:choose>
												</td>
											</tr>
										</c:if>						
									
									</tbody>
						
								</table>
							
								<c:set var="itemno" value="${itemno + 1}" />
					
							</c:forEach>
							
							<table class="default2 schedEquipList" summary="<spring:message code='schedule.tableallscheduleequipment'/>">
						
								<tfoot>
									<tr>
										<td>&nbsp;</td>
									</tr>
								</tfoot>
								
							</table>
							
							</c:when>
							<c:otherwise>
						
							<table class="default4 schedEquipList" summary="<spring:message code='schedule.tablemessagenoscheduleequipment'/>">
						
								<tbody>
																								
									<tr>
										<td class="text-center bold"><spring:message code="schedule.noequipmenthasbeenaddedtothisschedule"/></td>																						
									</tr>										
								
								</tbody>
					
							</table>
													
							</c:otherwise>
						</c:choose>
						
					</div>
					<!-- end of schedule equipment section -->
					
					<!-- this section contains form elements for editing schedule info (contact, address, scheduledate) -->
					<div id="editschedule-tab" class="hid">
												
						<form:form action="" method="post" modelAttribute="form">
				
							<fieldset>
							
								<legend><spring:message code="schedule.editschedule"/></legend>
								
								<ol>
									<li>
										<label><spring:message code="type"/>:</label>
										<form:select path="type" style="width:400px;">
											<c:forEach var="st" items="${form.scheduleTypes}">
												<option value="${st}" <c:if test="${form.schedule.type == st}"> selected="selected" </c:if>>${st.type}</option>
											</c:forEach>
										</form:select>
									</li>
									<li>
										<label><spring:message code="contact"/>:</label>
										<form:select path="personid" style="width:400px;">
											<c:forEach var="c" items="${form.schedule.contact.sub.contacts}">
												<option value="${c.personid}" <c:if test="${form.schedule.contact.personid == c.personid}"> selected="selected" </c:if>>${c.firstName} ${c.lastName}</option>
											</c:forEach>
										</form:select>
									</li>
									<li>
										<label><spring:message code="address"/>:</label>
										<form:select path="addrid" style="width:400px;">
											<c:forEach var="a" items="${form.schedule.address.sub.addresses}">
												<option value="${a.addrid}" <c:if test="${form.schedule.address.addrid == a.addrid}"> selected="selected" </c:if>>
													${a.addr1},&nbsp;
													<c:if test="${a.addr2.trim().length() > 0}">${a.addr2},</c:if>
													<c:if test="${a.addr3.trim().length() > 0}">${a.addr3},</c:if>
													<c:if test="${a.town.trim().length() > 0}">${a.town},</c:if>
													<c:if test="${a.county.trim().length() > 0}">${a.county},</c:if> 
													<c:if test="${a.postcode.trim().length() > 0}">${a.postcode},</c:if> 
													<c:if test="${not empty a.country}">${a.country.localizedName}</c:if>
												</option>
											</c:forEach>
										</form:select>
									</li>
									<li>
										<label><spring:message code="company.transportoption"/>:</label>
										<form:select path="transportOptionid" style="width:400px;">
											<c:forEach var="option" items="${transportOptionsWithActiveMethod}">
												<option value="${option.id}" <c:if test="${form.schedule.transportOption.id == option.id}"> selected="selected" </c:if>>
													<cwms:besttranslation translations="${option.method.methodTranslation}" />
													<c:if test="${not empty option.localizedName}">&nbsp;${option.localizedName} </c:if>
												</option>
											</c:forEach>
										</form:select>
									</li>
									<li>
										<label><spring:message code="actiontype.status"/>:</label>
										<form:select path="status" style="width:400px;">
											<c:forEach var="ss" items="${form.scheduleStatuses}">
												<option value="${ss}" <c:if test="${form.schedule.status == ss}"> selected="selected" </c:if>>${ss.status}</option>
											</c:forEach>
										</form:select>
									</li>
									<li>
										<label><spring:message code="schedule.scheduledate"/>:</label>
										<form:input path="scheduleDate" id="scheduleDate" type="date" value="${cwms:isoDate(form.schedule.scheduleDate)}" 
										style="width:395px;"/>
										<form:errors path="scheduleDate" class="attention"/>
									</li>
									<li>
										<label>&nbsp;</label>
										<input type="submit" value="<spring:message code='submit'/>" name="submit" id="submit" />
									</li>
								</ol>
							
							</fieldset>
						
						</form:form>
															
					</div>
					<!-- end of edit schedule section -->
				
				</div>
				<!-- end of infobox div -->
										
				<!-- this section contains all quotation item notes -->
				<t:showTabbedNotes entity="${form.schedule}" privateOnlyNotes="${privateOnlyNotes}" noteTypeId="${form.schedule.scheduleId}" noteType="SCHEDULENOTE" />
				<!-- end of quotation item notes section -->
    </jsp:body>
</t:crocodileTemplate>