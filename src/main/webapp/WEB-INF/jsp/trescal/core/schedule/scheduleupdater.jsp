<%-- File name: /trescal/core/schedule/scheduleupdater.jsp--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="menu.goods.scheduleupdater"/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/schedule/ScheduleUpdater.js'></script>
    </jsp:attribute>
    <jsp:body>
		<div class="clear"></div>
			<!-- infobox contains all pending schedules -->
		  <div class="infobox">	
			<fieldset>
				<legend>${allocatedSubdiv.value}</legend>
				<form:form modelAttribute="form" method="post" id="scheduleUpdater">
					<table class="default4 ">
						<thead>
							<tr>
								<th scope="col"><spring:message code="select"/></th>
								<th scope="col" style="text-align: center;"><spring:message code="date"/></th>
								<th scope="col"><spring:message code="company.transportoption"/></th>
								<th scope="col"><spring:message code="schedule.pendingdeliveries"/></th>
							</tr>
						</thead>
						<tbody>
							<c:choose>
								<c:when test="${dtoMap.keySet().size()>0 }">
									<c:forEach var="scheduleDate" items="${dtoMap.keySet()}" varStatus="loop0">
										<c:forEach var="transportOption" items="${dtoMap.get(scheduleDate).keySet()}" varStatus="loop1">
											<tr>
												<td>
													<form:radiobutton path="transportOptionId" value="${transportOption}" onclick="checkDate('${loop0.index}${loop1.index}')"/>
												</td>
												<td class="center">
													<fmt:formatDate type="date" dateStyle="SHORT" value="${ scheduleDate }"/>
													<form:radiobutton class="hid" id="scheduleDate${loop0.index}${loop1.index}" path="scheduleDate" value="${scheduleDate}"/>
												</td>
												<td>
												<c:choose>
													<c:when test="${not empty dtoMap.get(scheduleDate).get(transportOption).toArray()[0].transportOptionLocalizedName}">
														${dtoMap.get(scheduleDate).get(transportOption).toArray()[0].transportOptionLocalizedName}
													</c:when>
													<c:otherwise>
														${dtoMap.get(scheduleDate).get(transportOption).toArray()[0].transportMethodName}
													</c:otherwise>
												</c:choose>
												</td>
												<td>${dtoMap.get(scheduleDate).get(transportOption).size()}</td>
											</tr>
										</c:forEach>
									</c:forEach>	
								</c:when>
								<c:otherwise>
									<tr class="nobord">											
										<td colspan="4" class="center bold"><spring:message code="schedule.noresulttodisplay" /></td>
									</tr>	
								</c:otherwise>
						</c:choose>	
					</tbody>															
				</table>
				<input type="button" value="<spring:message code="select"/>" onclick="event.preventDefault(); scheduledDeliverySubmit();" />
			</form:form>	
		</fieldset>											
		</div>
		<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>