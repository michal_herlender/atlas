<%-- File name: /trescal/core/schedule/createschedule.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
		<span class="headtext"><spring:message code="schedule.createschedule" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/schedule/CreateSchedule.js'></script>
    </jsp:attribute>
   		 <jsp:body>
			<!-- set loadtab if an error occurs as the account tab is
				 the only tab which can error -->
			
			<!-- infobox contains all search form elements and is styled with nifty corners -->
			<div class="infobox" id="createschedule">
				<c:if test="${noActiveDOW}">
					<div class="warningBox1">
						<div class="warningBox2">
								<div class="warningBox3">
									<h5 class="center-0 attention"><spring:message code="schedule.no.active.dayofweek"/></h5>
								</div>
							</div>
						</div>
					</c:if>
				<form:form method="post" action="" modelAttribute="form" id="createscheduleForm">
				  <!-- error section displayed when form submission fails -->
				  <form:errors path="*">
                    <div class="warningBox1">
                        <div class="warningBox2">
                            <div class="warningBox3">
                                <h5 class="center-0 attention">
                                    <spring:message code='schedule.errorcreateschedule' />
                                </h5>
                                <c:if test="${ not empty scheduleExists}">
                    				<div class="center attention">
                    					<spring:message code="schedule.error.duplicateschedule" arguments="${scheduleExists}"/>
                    				</div>
                    			</c:if>
                            </div>
                        </div>
                    </div>
                  </form:errors>
				  <!-- end error section -->
				<fieldset>
				
					<legend><spring:message code="schedule.newscheduleinformation" /></legend>
					
					<ol>
						<li>
							<label><spring:message code="schedule.schedulefrom" />:</label>
							<span><c:out value="${allocatedSubdiv.value}"/></span>
						</li>									
						<li>
							<label><spring:message code="type" />:</label>
							<form:select path="type">
								<c:forEach var="st" items="${form.scheduleTypes}">
									<option value="${st}">${st.type}</option>
								</c:forEach>
							</form:select>
						</li>
						<li>
							<label class="twolabel"><spring:message code="company" />:</label>
							<!-- this div creates a new cascading search plugin. The default search contains an input 
								 field and results box for companies, when results are returned this cascades down to
								 display the subdivisions within the selected company. This default behavoir can be obtained
								 by including the hidden input field with no value or removing the input field. You can also
								 add address and/or contact searches to the cascade by including the values 'contact' and/or 'address'
								 in the hidden inputs value attribute separated by a comma (e.g value="contact", value="address",
								 value="address,contact").  You can also swap these values around (e.g. value="contact,address").  
							 -->
							<label>
								<select id="coroleselect" name="coroleselect" class="marg-left" onchange=" changeCorole(this.value); ">
			    					<c:forEach var="companyRole" items="${form.coroles}">
			    						<option value="${companyRole}">${companyRole.getMessage()}</option>
			    					</c:forEach>
			    				</select>
								<div id="cascadeSearchPlugin" onClick="getScheduleDetails();" onkeyup="getScheduleDetails();">
									<input type="hidden" id="cascadeRules" value="subdiv,address,contact" />
									<input type="hidden" id="compCoroles" value="client" />
									<input type="hidden" id="addressType" value="Delivery"/>
									<c:if test="${(not empty form.coid) && (form.coid != 0)}">	
										<input type="hidden" id="prefillIds" value="${form.coid},${form.subdivid},${form.addrid},${form.personid}" 
										/>
									</c:if>
									<!-- Prefill IDs here if there was an error -->
								</div>
							</label>
							<form:errors path="addrid" class="attention"/>
							<form:errors path="coid" class="attention"/>
							<form:errors path="personid" class="attention"/>
							<div class="clear"></div>
						</li>
						<li class="hid" id="transportOptionIn">
							<label><spring:message code="viewjob.transpin"/>:</label>
							<span id="transportOptionInName"></span>
						</li>
						<li class="hid" id="transportOptionOut">
							<label><spring:message code="viewjob.transpout"/>:</label>
							<span id="transportOptionOutName"></span>
						</li>
						<li class="hid" id="schedulesCreated">
							<label><spring:message code="schedules.already.created"/>: </label>
							<table id="schedulesTable">
								<tbody>
								</tbody>
							</table>
							<form:hidden path="schedulesCreated"/>
						</li>
						<li>
							<label><spring:message code="company.transportoption"/>*:</label>
							<form:select path="transportOptionId">
								<option value=""><spring:message code="companyedit.na"/></option>
								<c:forEach var="option" items="${transportOptionsSchedulable}">
									<option value="${option.id}"> 
										<cwms:besttranslation translations="${option.method.methodTranslation}" />
										<c:if test="${not empty option.localizedName}">&nbsp;${option.localizedName} </c:if>
									
								</c:forEach>
							</form:select>
							<form:errors path="transportOptionId" class="attention"/>
						</li>
						<li>
							<label><spring:message code="status" />:</label>
							<form:select path="status">
								<c:forEach var="ss" items="${form.scheduleStatuses}">
									<option value="${ss}" <c:if test="${ss eq 'AGREED' }"> selected </c:if> > ${ss.status} </option>
								</c:forEach>
							</form:select>
						</li>
						<li>
							<label><spring:message code="date" />:</label>
							<form:radiobutton path="manualEntryDate" value="false" checked="true" onClick="hideDateEntry();"/>
								<spring:message code="schedule.nextavailable"/>
							<form:radiobutton path="manualEntryDate" id="entryDate" value="true" onClick="showDateEntry();"/>
								<spring:message code="schedule.manual"/>
							<br>
							<form:input class="hid" id="scheduleDate" path="scheduleDate" value="${form.currentDate}" type="date" autocomplete="off" />
							<form:errors path="scheduleDate" class="attention"/>
						</li>
						<li>
							<label><spring:message code="notes" />:</label>
							<div class="float-left editor_box">
								<form:textarea path="note" class="editor" style="height: 300px; width: 800px;"></form:textarea>
							</div>
							<!-- clear floats and restore page flow -->
							<div class="clear-0"></div>
							<form:errors path="note" class="attention"/>
						</li>
						<li>
							<label>&nbsp;</label>
							<input type="submit" name="submit" id="submit" value="<spring:message code='submit' />" <c:if test="${noActiveDOW}"> disabled</c:if>/>
						</li>
						
					</ol>
				
				</fieldset>
				
			 </form:form>
			
			</div>
			<!-- end of infobox div -->
		    </jsp:body>
</t:crocodileTemplate>