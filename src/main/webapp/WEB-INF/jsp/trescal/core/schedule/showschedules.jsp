<%-- File name: /trescal/core/schedule/showschedules.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="schedule.scheduleresults"/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/schedule/ShowSchedules.js'></script>
    </jsp:attribute>
    <jsp:body>
 			<!-- main div with id for applying table styles -->
			<div id="scheduleresults">
				<div class="float-left padding">
					<select onchange=" toggleDespatchedFromTransportOption(this.value); return false; ">
						<option value="0"><spring:message code="all"/></option>
						<c:forEach var="option" items="${transportOptionsSchedulable}">
							<c:choose>
								<c:when test="${not empty option.localizedName}">
									<option value="${option.id}"> ${option.localizedName} </option>
								</c:when>
								<c:otherwise>
									<option value="${option.id}"> 
										<cwms:besttranslation translations="${option.method.methodTranslation}"/>
									</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>									
				</div>
				<div class="float-right padding">
					<cwms:securedLink permission="SCHEDULE_CREATE"  classAttr="mainlink"   collapse="True" >
						<spring:message code="schedule.createanewschedule"/>
					</cwms:securedLink>
				</div>
				<div class="clear"></div>
			
				<!-- sub navigation menu which displays and hides sections of page as well as linking to external pages -->
				<div id="subnav">
					<dl>
						<dt><a href="#" onclick="switchMenuFocus(menuElements, 'agreedsched-tab', false); return false; " id="agreedsched-link" title="<spring:message code='schedule.viewagreedschedules'/>" class="selected"><spring:message code="schedule.agreedschedules"/> (<span class="sizeSpanA">${form.agreedSchedules.size()}</span>)</a>
						<dt><a href="#" onclick="switchMenuFocus(menuElements, 'provisionalsched-tab', false); return false;" id="provisionalsched-link" title="<spring:message code='schedule.viewprovisionalschedules'/>"><spring:message code="schedule.provisionalschedules"/> (<span class="sizeSpanP">${form.provisionalSchedules.size()}</span>)</a></dt>
					</dl>
				</div>
				<!-- end of sub navigation menu -->
				
				<!-- this section lists all agreed schedules in a table of results -->
				<div id="agreedsched-tab">
					<form:form action="schedulesearch.htm" method="post" modelAttribute="form">
					<!-- infobox contains all agreed schedule search results and is styled with nifty corners -->
					<div class="infobox">
						<!-- this div centers the main results text, then the relative class allows the previous and next dates to be
							 aligned to the left and right using absolute positioning -->
						<div class="center relative">
							
							<!-- align left using absolute positioning -->
							<c:if test="${not empty form.previous}">
								<div class="absoluteleft">
									<fmt:formatDate var="previousDate" value="${form.previous}" type="date" dateStyle="SHORT" />
									<%-- We must use ISO format date format for URLs, not localized formats! --%>
									<a href="showschedules.htm?filter=date&date=${form.previous}" class="mainlink">${previousDate}</a>
								</div>
							</c:if>
							
							<span class="bold">${form.description}</span><br>
							<c:if test="${form.filterToday}">
								<span class="bold">
									<spring:message code="schedule.workwave.export" var="submitText"/>
									<input type="submit" name="export" value="${submitText}">
								</span>
							</c:if>
							
															
							<!-- align right using absolute positioning -->
							<c:if test="${not empty form.next}">
								<div class="absoluteright">
									<fmt:formatDate var="nextDate" value="${form.next}" type="date" dateStyle="SHORT" />
									<%-- We must use ISO format date format for URLs, not localized formats! --%>
									<a href="showschedules.htm?filter=date&date=${form.next}" class="mainlink">${nextDate}</a>
								</div>
							</c:if>
						
						</div>
						
						<table class="default4" summary="<spring:message code='schedule.tableagreedschedules'/>">
							<thead>
								<tr>
									<td colspan="6"><spring:message code="schedule.agreedschedules"/> (<span class="sizeSpanA">${form.agreedSchedules.size()}</span>)</td>
								</tr>
								<tr>
									<th class="schedconinfo" scope="col"><spring:message code="schedule.companysubdivcontact"/></th>
									<th class="schedaddr" scope="col"><spring:message code="address"/></th>
									<th class="schedstatus" scope="col"><spring:message code="schedule.statusdate"/></th>
									<th class="schedassign" scope="col"><spring:message code="schedule.assignments"/></th>
									<th class="schednotes" scope="col"><spring:message code="notes"/></th>
									<th class="schedutils hideonprint" scope="col">&nbsp;</th>
								</tr>
							</thead>								
							<c:choose>
								<c:when test="${empty form.agreedSchedules}">
									<tbody>
										<tr class="odd">
											<td colspan="6" class="center bold"><spring:message code="schedule.therearenoagreedscheduleresultstodisplay"/></td>
										</tr>
									</tbody>
								</c:when>
								<c:otherwise>	
									<c:set var="rowcount" value="1"/>								
									<c:forEach var="s" items="${form.agreedSchedules}">
										<c:choose>
											<c:when test="${s.repeatSchedule == true}">
												<c:set var="repeatClass" value="repeatSched" />
											</c:when>
											<c:otherwise>
												<c:set var="repeatClass" value="even" />
											</c:otherwise>
										</c:choose>
										<tbody>
											<tr class="headrow rowClass transportoption${s.transportOptionId}" id="schedule${s.scheduleId}">
												<td colspan="3" class="bold">${s.source.source} - ${s.organisationName} - ${s.type.type}</td>
												<td colspan="2">&nbsp;</td>
												<td class="center hideonprint"><a href="#" onclick=" deleteScheduleConfirmation(${s.scheduleId}, '${s.status.status}', '${s.coname}'); "><img src="img/icons/delete.png" width="16px;" height="16px;" alt="<spring:message code='schedule.deletethisschedule'/>" title="<spring:message code='schedule.deletethisschedule'/>" /></a></td>
											</tr>
											<tr class="${repeatClass} rowClass transportoption${s.transportOptionId}" id="schedule1${s.scheduleId}">															
												<td class="schedconinfo">
													<div class="hideonprint">
														<div class="padding5">
															<links:companyLinkInfo companyId="${s.coid}" companyName="${s.coname}" active="${s.companyActive}" onStop="${s.companyOnStop}" rowcount="0" copy="true"/>			 		   
												    	</div>
														<div class="padding5">
															<links:subdivLinkInfo rowcount="0"
																subdivId="${s.subdivid}"
																subdivName="${s.subname}"
																companyActive="${s.companyActive}"
																companyOnStop="${s.companyOnStop}"
																subdivActive="${s.subdivActive}" />
												    	</div>
														<div class="padding5">
															<links:contactLinkInfo active="${s.contactActive}" rowcount="0"
																personId="${s.personid}" contactName="${s.contactName}" />		   		
												    	</div>
													</div>
												<!-- this div is displayed when printing -->
												<div class="hid" id="schedAgreedPrintables">															
													<div class="padding5">
														${s.coname}
													</div>
													<div class="padding5">
														${s.subname}
													</div>
													<div class="padding5">
														${s.contactName} 
													</div>
													<div class="padding5">
														<spring:message code="phone"/> : ${s.telephone}<br />
														<spring:message code="mobile"/> : ${s.mobile}
													</div>																	
												</div>
												<!-- end of printing div -->
											</td>
											<td class="schedaddr">
												<c:if test='${not empty s.addr1}'>${s.addr1} <br/></c:if>
												<c:if test='${not empty s.addr2}'>${s.addr2} <br/></c:if>
												<c:if test='${not empty s.addr3}'>${s.addr3} <br/></c:if>
												<c:if test='${not empty s.town}'>${s.town} <br/></c:if>
												<c:if test='${not empty s.postCode}'>${s.postCode} <br/></c:if>
												<c:if test='${not empty s.country}'>${s.country}</c:if>
											</td>
											<td class="schedstatus">
												${s.status.status}<br /> 
												<c:if test="${not empty s.scheduleDate}">
													(<fmt:formatDate value="${s.scheduleDate}" type="date" dateStyle="SHORT" />)
												</c:if>
											</td>
											<td class="schedassign">
												<c:if test="${s.scheduledDeliveryCount > 0}"><spring:message code="viewjob.delivs"/>(${s.scheduledDeliveryCount})<br /><br /></c:if>
												<c:if test="${s.scheduleEquipmentsCount > 0}"><spring:message code="schedule.scheduleequipment"/> (${s.scheduleEquipmentsCount}) </c:if>
											</td>
											<td class="schednotes">
												<c:if test="${s.notes.size() > 0}"> 
													<c:forEach var="n" items="${s.notes}">
														<c:if test="${not empty n.active}"> 
															<div class="bold">
																<c:out value="${n.setByName} - " />
																<em><fmt:formatDate value="${n.setOn}" type="date" dateStyle="SHORT" /></em>
															</div>
															${n.note}
														</c:if>
													</c:forEach>
												</c:if>
											</td>
											<td class="schedutils hideonprint">
												<c:if test="${s.email.trim().length() > 0}"> 
													<div class="hideonprint"><a href="#" onclick=" sendEmailConfirmation(${s.scheduleId}); return false; "><img src="img/icons/email.png" width="16px;" height="16px;" alt="<spring:message code='schedule.sendconfirmationemail'/>" title="<spring:message code='schedule.sendconfirmationemail'/>" /></a></div>
												</c:if>														
												<div class="hideonprint paddtop"><a href="viewschedule.htm?schid=${s.scheduleId}"><img src="img/icons/information.png" width="16px;" height="16px;" alt="<spring:message code='schedule.vieweditthisschedule'/>" title="<spring:message code='schedule.vieweditthisschedule'/>" /></a></div>
											</td>
										</tr>
									</tbody>
									
								<c:set var="rowcount" value="${rowcount + 1}"/>									
							</c:forEach>
						</c:otherwise>
					</c:choose>											
					</table>											
				</div>
					<!-- end of infobox div -->
					</form:form>
				</div>
				<!-- end of agreed schedule section -->
				
				<!-- this section lists all provisional schedules in a table of results -->
				<div id="provisionalsched-tab" class="hid">
				
					<!-- infobox contains all provisional schedule search results and is styled with nifty corners -->
					<div class="infobox">
					
						<!-- this div centers the main results text, then the relative class allows the previous and next dates to be
							 aligned to the left and right using absolute positioning -->
						<div class="center relative">
							
							<!-- align left using absolute positioning -->
							<c:if test="${not empty form.previous}">
								<div class="absoluteleft">
									<fmt:formatDate var="previousDate" value="${form.previous}" type="date" dateStyle="SHORT" />
									<a href="showschedules.htm?filter=date&date=${previousDate}" class="mainlink">${previousDate}</a>
								</div>
							</c:if>
							
							<span class="bold">${form.description}</span>
															
							<!-- align right using absolute positioning -->
							<c:if test="${not empty form.next}">
								<div class="absoluteright">
									<fmt:formatDate var="nextDate" value="${form.next}" type="date" dateStyle="SHORT" />
									<a href="showschedules.htm?filter=date&date=${nextDate}" class="mainlink">${nextDate}</a>
								</div>
							</c:if>
						
						</div>
					
						<table class="default4" summary="This table displays provisional schedule headings">
							<thead>
								<tr>
									<td colspan="6"><spring:message code="schedule.provisionalschedules"/> (<span class="sizeSpanP">${form.provisionalSchedules.size()}</span>)</td>
								</tr>
								<tr>
									<th class="schedconinfo" scope="col"><spring:message code="schedule.companysubdivcontact"/></th>
									<th class="schedaddr" scope="col"><spring:message code="address"/></th>
									<th class="schedstatus" scope="col"><spring:message code="schedule.statusdate"/></th>
									<th class="schedassign" scope="col"><spring:message code="schedule.assignments"/></th>
									<th class="schednotes" scope="col"><spring:message code="notes"/></th>
									<th class="schedutils" scope="col">&nbsp;</th>
								</tr>
							</thead>							
						<c:choose>
							<c:when test="${empty form.provisionalSchedules}">
								<tbody>
									<tr class="odd">
										<td colspan="6" class="center bold"><spring:message code='schedule.therearenoprovisionalscheduleresultstodisplay'/></td>
									</tr>
								</tbody>
							</c:when>
							<c:otherwise>
																		
							<c:forEach var="s" items="${form.provisionalSchedules}">
								<tbody>
									<tr class="headrow rowClass transportoption${s.transportOptionId}">
										<td colspan="3" class="bold">${s.source.source} - ${s.organisationName} - ${s.type.type}</td>
										<td colspan="2">&nbsp;</td>
										<td class="center"><a href="#" class="hideonprint" onclick=" deleteScheduleConfirmation(${s.scheduleId}, '${s.status.status}', '${s.coname}'); return false; "><img src="img/icons/delete.png" width="16px;" height="16px;" alt="<spring:message code='schedule.deletethisschedule'/>" title="<spring:message code='schedule.deletethisschedule'/>" /></a></td>
									</tr>
									<tr class="${repeatClass} rowClass transportoption${s.transportOptionId}">
										<td class="schedconinfo">
											<div class="hideonprint">
												<div class="padding5">
													<links:companyLinkInfo companyId="${s.coid}" companyName="${s.coname}" active="${s.companyActive}" onStop="${s.companyOnStop}" rowcount="0" copy="true"/>			 		   
												</div>
												<div class="padding5">
													<links:subdivLinkInfo rowcount="0"
														subdivId="${s.subdivid}"
														subdivName="${s.subname}"
														companyActive="${s.companyActive}"
														companyOnStop="${s.companyOnStop}"
														subdivActive="${s.subdivActive}" />
												</div>
												<div class="padding5">
													<links:contactLinkInfo active="${s.contactActive}" rowcount="0"
														personId="${s.personid}" contactName="${s.contactName}" />		   		
												</div>
											</div>
												<!-- this div is displayed when printing -->
												<div class="hid" id="schedProvPrintables">															
													<div class="padding5"> ${s.coname}</div>
													<div class="padding5"> ${s.subname}</div>
													<div class="padding5"> ${s.contactName} </div>
													<div class="padding5">
														<spring:message code="phone"/>: ${s.telephone}<br />
														<spring:message code="mobile"/>: ${s.mobile}
													</div>																	
												</div>
												<!-- end of printing div -->
											</td>
											<td class="schedaddr">
												<c:if test='${not empty s.addr1}'>${s.addr1} <br/></c:if>
												<c:if test='${not empty s.addr2}'>${s.addr2} <br/></c:if>
												<c:if test='${not empty s.addr3}'>${s.addr3} <br/></c:if>
												<c:if test='${not empty s.town}'>${s.town} <br/></c:if>
												<c:if test='${not empty s.postCode}'>${s.postCode} <br/></c:if>
												<c:if test='${not empty s.country}'>${s.country}</c:if>	
											</td>
											<td class="schedstatus">
												${s.status.status}<br />
												<c:if test="${not empty s.scheduleDate}">
													(<fmt:formatDate value='${s.scheduleDate}' type="date" dateStyle="SHORT" />)
												</c:if>
											</td>
											<td class="schedassign">
												<c:if test="${s.scheduledDeliveryCount > 0}"><spring:message code="viewjob.delivs"/>(${s.scheduledDeliveryCount})<br /><br /></c:if>
												<c:if test="${s.scheduleEquipmentsCount > 0}"><spring:message code="schedule.scheduleequipment"/> (${s.scheduleEquipmentsCount}) </c:if>
											</td>
											<td class="schednotes">
												<c:if test="${s.notes.size() > 0}">
													<c:forEach var="n" items="${s.notes}">
														<div class="bold">
															<c:out value="${n.setByName} - "/> 
															<em><fmt:formatDate value='${n.setOn}' type="date" dateStyle="SHORT" /></em>
														</div>
														${n.note}
													</c:forEach>	
												</c:if>
											</td>
											<td class="schedutils">
												<c:if test="${s.email.trim().length() > 0}">
													<div class="hideonprint"><a href="#" onclick=" sendEmailConfirmation(${s.scheduleId}); return false; "><img src="img/icons/email.png" width="16px;" height="16px;" alt="<spring:message code='schedule.sendconfirmationemail'/>" title="<spring:message code='schedule.sendconfirmationemail'/>"></a></div>
												</c:if>
												<div class="hideonprint"><a href="viewschedule.htm?schid=${s.scheduleId}"><img src="img/icons/information.png" width="16px;" height="16px;" alt="<spring:message code='schedule.vieweditthisschedule'/>" title="<spring:message code='schedule.vieweditthisschedule'/>"></a></div>
											</td>
										</tr>
									</tbody>								
							</c:forEach>
							</c:otherwise>
						</c:choose>
					</table>																													
					</div>
					<!-- end of infobox div -->
				
				</div>
				<!-- end of provisional schedule section -->
			
			</div>
			
			<!-- end of main div -->
    </jsp:body>
</t:crocodileTemplate>