<%-- File name: /trescal/core/schedule/schedulesearchresults.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/schedule/ShowSchedules.js'></script>
    </jsp:attribute>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="schedule.scheduleresults"/></span>
	</jsp:attribute>
	<jsp:body>
		<!-- infobox contains all search results and is styled with nifty corners -->
		<div class="infobox" id="searchschedules">
			<div class="float-right padding">
					<cwms:securedLink permission="SCHEDULE_CREATE"  classAttr="mainlink"   collapse="True" >
						<spring:message code="schedule.createanewschedule"/>
					</cwms:securedLink>	
			</div>
			<div class="clear">&nbsp;</div>
			<form:form action="" modelAttribute="form" name="searchform" id="searchform" method="post">
				<form:hidden path="coid"/>
				<form:hidden path="status"/>
				<form:hidden path="includeHistory"/>
				<form:hidden path="dateSearchType"/>
				<form:hidden path="pageNo"/>
				<form:hidden path="resultsPerPage"/>
			
			<t:showResultsPagination rs="${form.rs}" pageNoId=""/>
			<table class="default4" >
				<thead>
					<tr>
						<td colspan="6">
							<spring:message code="schedule.search.string1"/>&nbsp;${form.rs.results.size()}&nbsp;
						</td>
					</tr>
					<tr>
						<th class="schedconinfo" scope="col"><spring:message code="schedule.companysubdivcontact"/></th>
						<th class="schedaddr" scope="col"><spring:message code="address"/></th>
						<th class="schedstatus" scope="col"><spring:message code="schedule.statusdate"/></th>
						<th class="schedassign" scope="col"><spring:message code="schedule.assignments"/></th>
						<th class="schednotes" scope="col"><spring:message code="notes"/></th>
						<th class="schedutils hideonprint" scope="col">&nbsp;</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="6">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody id="keynavResults">
					<c:choose>
						<c:when test="${form.rs.results.size() == 0}">
							<tr class="odd">
								<td colspan="6" class="bold center">
									<spring:message code="schedule.search.string2"/>
								</td>
							</tr>
						</c:when>
						<c:otherwise>
							<c:forEach var="s" items="${form.rs.results}" varStatus="loop">
								<tr class="headrow" id="schedule${s.scheduleId}" >
									<td colspan="3" class="bold">${s.source.source} - ${s.organisationName} - ${s.type.type}</td>
									<td colspan="2">&nbsp;</td>
									<td class="center hideonprint"><a href="" onclick=" deleteScheduleConfirmation(${s.scheduleId}, '${s.status.status}', '${s.coname}'); event.preventDefault();"><img src="img/icons/delete.png" width="16px;" height="16px;" alt="<spring:message code='schedule.deletethisschedule'/>" title="<spring:message code='schedule.deletethisschedule'/>" /></a></td>
								</tr>
								<tr class="${repeatClass}" id="schedule1${s.scheduleId}">															
									<td class="schedconinfo">
										<div class="hideonprint">
											<div class="padding5">
												<links:companyLinkInfo companyId="${s.coid}" companyName="${s.coname}" active="${s.companyActive}" onStop="${s.companyOnStop}" rowcount="0" copy="true"/>
											</div>
											<div class="padding5">
												<links:subdivLinkInfo rowcount="0"
												subdivId="${s.subdivid}"
												subdivName="${s.subname}"
												companyActive="${s.companyActive}"
												companyOnStop="${s.companyOnStop}"
												subdivActive="${s.subdivActive}" />
											</div>
											<div class="padding5">
												<links:contactLinkInfo active="${s.contactActive}" rowcount="0"
													personId="${s.personid}" contactName="${s.contactName}" />
											</div>
										</div>
										<!-- this div is displayed when printing -->
										<div class="hid" id="schedAgreedPrintables">															
											<div class="padding5">
												${s.coname}
											</div>
											<div class="padding5">
													${s.subname}
											</div>
											<div class="padding5">
												 ${s.contactName}
											</div>
											<div class="padding5">
												<spring:message code="phone"/> : ${s.telephone}<br />
												<spring:message code="mobile"/> : ${s.mobile}
											</div>																	
										</div>
												<!-- end of printing div -->
											</td>
											<td class="schedaddr">
												<c:if test='${not empty s.addr1}'>${s.addr1} <br/></c:if>
												<c:if test='${not empty s.addr2}'>${s.addr2} <br/></c:if>
												<c:if test='${not empty s.addr3}'>${s.addr3} <br/></c:if>
												<c:if test='${not empty s.town}'>${s.town} <br/></c:if>
												<c:if test='${not empty s.postCode}'>${s.postCode} <br/></c:if>
												<c:if test='${not empty s.country}'>${s.country}</c:if>
											</td>
											<td class="schedstatus">
												${s.status.status}<br /> 
												<c:if test="${not empty s.scheduleDate}">
													(<fmt:formatDate value="${s.scheduleDate}" type="date" dateStyle="FULL"/>)
												</c:if>
											</td>
											<td class="schedassign">
												<c:if test="${s.scheduledDeliveryCount > 0}"><spring:message code="viewjob.delivs"/>(${s.scheduledDeliveryCount})<br /><br /></c:if>
												<c:if test="${s.scheduleEquipmentsCount > 0}"><spring:message code="schedule.scheduleequipment"/> (${s.scheduleEquipmentsCount}) </c:if>
											</td>
											<td class="schednotes">
												<c:if test="${s.notes.size() > 0}"> 
													<c:forEach var="n" items="${s.notes}">
														<c:if test="${not empty n.active}"> 
															<div class="bold">
																<c:out value="${n.setByName} - " />
																<em><fmt:formatDate value="${n.setOn}" type="date" dateStyle="FULL" /></em>
															</div>
															${n.note}
														</c:if>
													</c:forEach>
												</c:if>
											</td>
											<td class="schedutils hideonprint">
												<c:if test="${s.email.trim().length() > 0}"> 
													<div class="hideonprint"><a href="#" onclick=" sendEmailConfirmation(${s.scheduleId}); return false; "><img src="img/icons/email.png" width="16px;" height="16px;" alt="<spring:message code='schedule.sendconfirmationemail'/>" title="<spring:message code='schedule.sendconfirmationemail'/>" /></a></div>
												</c:if>															
												<div class="hideonprint paddtop"><a href="viewschedule.htm?schid=${s.scheduleId}"><img src="img/icons/information.png" width="16px;" height="16px;" alt="<spring:message code='schedule.vieweditthisschedule'/>" title="<spring:message code='schedule.vieweditthisschedule'/>" /></a></div>
											</td>
										</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
			<t:showResultsPagination rs="${form.rs}" pageNoId=""/>
		</form:form>
		</div>
		<!-- end of infobox div -->
	</jsp:body>
</t:crocodileTemplate>