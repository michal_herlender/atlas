<%-- File name: /trescal/core/schedule/scheduledeliveries.jsp--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="schedule.scheduledeliveries"/></span>
    </jsp:attribute>
     <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/schedule/ScheduledDeliveries.js'></script>
        <script type="module" src="script/components/cwms-schedule-rules/cwms-schedule-rules.js" ></script>
        <script>
			// variable needed for tracking scheduleDeliveries is case we have an validation error
			var delievriesCount = ${scheduleDeliveries.size()};
		</script>
    </jsp:attribute>   
    <jsp:body>
		<div class="clear"></div>
			<!-- infobox contains all pending schedules -->
		  <div class="infobox">	
			<fieldset>
				<legend>
					<a href="scheduleupdater.htm"  >
   		   				<img src="img/icons/arrow_left.png">
   					</a>
				</legend>
				<div class="clear">&nbsp;</div>
				<form:form modelAttribute="form" method="post">
					<form:hidden path="transportOptionId" value="${form.transportOptionId}"/>
					<form:hidden path="scheduleDate" value="${form.scheduleDate}"/>
					<form:hidden path="subdivid" value="${allocatedSubdiv.key}"/>
					<table class="default4 scheduledDeliveries">
						<thead>
							<tr>
								<th colspan="2" ><spring:message code="schedule.scheduledeliveries.showen"/></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="bold showen" scope="col"><spring:message code="businesssubdivision"/></td>
								<td scope="col">${allocatedSubdiv.value}</td>
							<tr>
							<tr>
								<td class="bold showen" scope="col"><spring:message code="date"/></td>
								<td scope="col">
									<fmt:formatDate type="date" dateStyle="SHORT" value="${ form.scheduleDate }"/>
								</td>
							<tr>
							<tr>
								<td class="bold showen" scope="col"><spring:message code="company.transportoption"/></td>
								<td scope="col">${transportOptionName}</td>
							<tr>
							<tr>
								<td class="bold showen" scope="col"><spring:message code="actiontype.status"/></td>
								<td scope="col">PENDING</td>
							<tr>
							<tr>
								<td class="bold showen" scope="col"><spring:message code="viewinstrument.nextdate"/></td>
								<td scope="col">
									<fmt:formatDate type="date" dateStyle="SHORT" value="${ nextDate }"/>
								</td>
							<tr>
						</tbody>														
				</table>
				<div class="clear">&nbsp;</div>
				<table class="default4 scheduledDeliveries" id="scheduledeliveries">
					<thead>
						<th class="status" style="text-align: center;">PENDING</th>
						<th class="status" style="text-align: center;">COMPLETE</th>
						<th class="status" style="text-align: center;">RESCHEDULE (<spring:message code="viewuserpreferences.automatic"/>)</th>
						<th class="status" style="text-align: center;">RESCHEDULE (<spring:message code="schedule.manual"/>)</th>
						<th style="text-align: center;"><spring:message code="invoiceexport.deliverynumber"/></th>
						<th class="delItemCount" style="text-align: center;"><spring:message code="schedule.itemcount"/></th>
						<th><spring:message code="company"/></th>
						<th><spring:message code="address.address"/></th>
					</thead>
					<tbody>
						<c:forEach var="scheduledDelivery" items="${scheduleDeliveries}" varStatus="loopStatus">
							<c:set var="i" value="${loopStatus.index}" />
							<form:hidden path="dto[${i}].scheduledDeliveryId" value="${scheduledDelivery.scheduledDeliveryId}"/>
							<tr>
								<td class="center">
									<form:radiobutton path="dto[${i}].scheduledDeliveryStatus" value="PENDING" onclick="hidDateEntry('dto[${i}].clientReceiptDate'); hidScheduleRulesData('scheduledDelivery${i}')" checked="checked"/>
								</td>
								<td class="center">
									<form:radiobutton path="dto[${i}].scheduledDeliveryStatus" value="COMPLETE" onclick="showDateEntry('dto[${i}].clientReceiptDate'); hidScheduleRulesData('scheduledDelivery${i}')"/>
									<form:input placeholder="Receipt Date" class="hid" id="clientReceiptDate" path="dto[${i}].clientReceiptDate" type="date"/>
									<form:errors class="attention" path="dto[${i}].clientReceiptDate"/>
									<form:hidden path="dto[${i}].deliveryId" value="${scheduledDelivery.deliveryId}"/>
								</td>
								<td class="center">
									<form:radiobutton path="dto[${i}].scheduledDeliveryStatus" value="RESCHEDULED_AUTO" onclick="hidDateEntry('dto[${i}].clientReceiptDate'); hidScheduleRulesData('scheduledDelivery${i}')"/>
								</td>
								<td class="center">
									<form:radiobutton path="dto[${i}].scheduledDeliveryStatus" value="RESCHEDULED_MANUAL" onclick="hidDateEntry('dto[${i}].clientReceiptDate'); showScheduleRulesData('scheduledDelivery${i}')"/>
									<form:errors class="attention" path="dto[${i}].scheduledDeliveryStatus"/>
								</td>
								<td class="center">
									<a href="viewdelnote.htm?delid=${scheduledDelivery.deliveryId}">${scheduledDelivery.deliveryNo}</a>
								</td>
								<td class="center">
									${scheduledDelivery.deliveryItemCount}
								</td>
								<td>
									${scheduledDelivery.deliveryCompany}
								</td>
								<td>
									${scheduledDelivery.deliveryAddress}
								</td>
								<form:hidden id="dtoNewTransportOptionId${i}" path="dto[${i}].newTransportOptionId" value=""/>
								<form:hidden id="dtoNewdate${i}" path="dto[${i}].newDate" value=""/>
							</tr>
							<tr id="scheduledDelivery${i}" class="hid">
								<td colspan="8">
									<cwms-schedule-rules subdiv-id="${allocatedSubdiv.key}" index="${i}"></cwms-schedule-rules>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<div class="text-center">
					<input type="submit" class="center" value="<spring:message code="submit"/>"/>
				</div>
			</form:form>	
		</fieldset>											
		</div>
		<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>