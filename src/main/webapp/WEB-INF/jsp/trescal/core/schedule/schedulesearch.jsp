<%-- File name: /trescal/core/schedule/schedulesearch.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<t:crocodileTemplate>
    <jsp:attribute name="header">
		<span class="headtext"><spring:message code="schedule.schedulebrowser" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/schedule/ScheduleSearch.js'></script>
    </jsp:attribute>
    <jsp:body>
				<!-- infobox contains all search form elements and is styled with nifty corners -->
				<div class="infobox" id="searchschedules">
				
					<div class="float-right">
						<cwms:securedLink permission="SCHEDULE_CREATE"  classAttr="mainlink"   collapse="True" >
							<spring:message code="schedule.createanewschedule"/>
						</cwms:securedLink>
					</div>
					
					<!-- this div clears floats and restores page flow -->
					<div class="clear"></div>
					
					<!-- display if there are any outstanding web schedules -->
					<c:if test="${form.numOfNewWebSchedules > 0}">
					
						<form:form method="post" action="">
						
							<fieldset>
								
								<legend><spring:message code="schedule.pendingwebschedules" /></legend>
								
								<ol>
									<li>
										<label><spring:message code="schedule.pendingschedules" />:</label>
										<input type="hidden" name="searchformsubmitted" value="pendingweb" />
										(${form.numOfNewWebSchedules})&nbsp;&nbsp;<input type="submit" name="submit" id="submit" value="<spring:message code='schedule.viewschedules' />" />
									</li>
								</ol>
								
							</fieldset>
						
						</form:form>
					
					</c:if>
					<!-- end of web schedule section -->
					
					<!-- displays content in column 39% wide (left) -->
					<div class="displaycolumn-40">
					
						<form:form method="post" action="" id="schedulesearchquickform">
					
						<fieldset>
					
							<legend><spring:message code="schedule.schedulequicksearch" /></legend>
							
							<ol>
								<li>
									<label><spring:message code="schedule.showtoday" />:</label>
									<span>
										<input id="today" name="quicksearch" type="radio" value="today" checked="checked" />
									</span>
								</li>
								<li>
									<label><spring:message code="schedule.showtomorrow" />:</label>
									<span>
										<input id="tomorrow" name="quicksearch" type="radio" value="tomorrow" />
									</span>
								</li>
								<li>
									<label><spring:message code="schedule.showallfuture" />:</label>
									<span>
										<input id="future" name="quicksearch" type="radio" value="future" />
									</span>
								</li>
								<li>
									<label>&nbsp;</label>
									<input type="hidden" name="searchformsubmitted" value="quicksearch" />
									<input type="submit" value="<spring:message code='submit' />" name="submit" id="submit" />
								</li>
							</ol>
						
						</fieldset>
						
						</form:form>
					
					</div>
					<!-- end of left column -->
					
					<!-- displays content in column 59% wide (right) -->
					<div class="displaycolumn-60">
					
						<form:form method="post" action="" id="schedulesearchadvancedform" modelAttribute="form" onsubmit=" return checkDateFields(); ">
						<form:errors path="*">
                    		<div class="warningBox1">
                        		<div class="warningBox2">
                            		<div class="warningBox3">
                                		<h5 class="center-0 attention">
                                    		<spring:message code='schedule.errorsearchschedule' />
                                		</h5>
                                		<c:forEach var="e" items="${messages}">
                                    		<div class="center attention"><c:out value="${e}"/></div>
                                		</c:forEach>
                            		</div>
                        		</div>
                    		</div>
                 		</form:errors>
						<fieldset>
						
							<legend><spring:message code="schedule.scheduleadvancedsearch" /></legend>
							
							<ol>
								<li>
									<label><spring:message code="schedule.dateselector" />:</label>
									<form:select path="dateSearchType" id="datetype" width="30px;" onchange=" toggleDateFields(this.value); return false; ">
										<option value="0" selected="selected"><spring:message code="schedule.alldates" /></option>
										<option value="1"><spring:message code="schedule.singledate" /></option>
										<option value="2"><spring:message code="schedule.daterange" /></option>
									</form:select>
									<form:errors path="dateSearchType" class="attention"/>
								</li>
								<li class="vis">
									<label><spring:message code="status"/>:</label>
									<span>
										<form:select path="status" width="30px;">
											<option value=""> <spring:message code="createquot.any"/></option>
											<c:forEach var="ss" items="${form.scheduleStatuses}">
												<option value="${ss}"> ${ss.status} </option>
											</c:forEach>
										</form:select>
									</span>
								</li>
								<li id="datetype0" class="vis">
									<label><spring:message code="schedule.includehistory" />:</label>
									<span>
										<form:checkbox path="includeHistory" value="true" /> 
										<spring:message code="schedule.includehistory" />
									</span>
								</li>
								<li id="datetype1" class="hid">
									<label><spring:message code="schedule.choosedate" />:</label>
									<span>
										<form:input path="dateSingle" type="date"/>
									</span>
								</li>
								<li id="datetype2" class="hid">
									<label><spring:message code="schedule.inrangefrom" />:</label>
									<span>
									<form:input path="dateBegin" type="date" />
									</span>
									<div class="clear-0"></div>
									<label><spring:message code="schedule.inrangeto" />:</label>
									<span>
									<form:input path="dateEnd"  type="date" />
									</span>
								</li>
								<li>
									<label><spring:message code="company" />:</label>
									<select id="coroleselect" name="coroleselect" onchange=" changeCorole(this.value); " tabindex="5">
										<c:forEach var="companyRole" items="${form.coroles}">
											<option value="${companyRole}">${companyRole.getMessage()}</option>
										</c:forEach>
									</select>										
									<div class="float-left extendPluginInput">
										<div class="compSearchJQPlugin">
											<input type="hidden" name="field" value="coid" />
											<input type="hidden" name="compCoroles" value="client" />
											<input type="hidden" name="tabIndex" value="6" />
											<!-- company results listed here -->
										</div>
									</div>										
									<!-- div clears the page flow -->
									<div class="clear"></div>
								</li>
								<li>
									<label>&nbsp;</label>
									<input type="hidden" name="searchformsubmitted" value="advancedsearch" />
									<input type="submit" value="<spring:message code='submit' />" name="submit" id="submit" />
								</li>
							</ol>
						
						</fieldset>
						
						</form:form>
					
					</div>
					<!-- end of right column -->
					
					<!-- this div clears floats and restores page flow -->
					<div class="clear">&nbsp;</div>
				
				</div>
				<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>