<%-- File name: /trescal/core/recall/viewrecall.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="recall.recall"/>&nbsp;${command.recall.recallNo}</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/recall/ViewRecall.js'></script>
		<script type='text/javascript' src='script/thirdparty/jQuery/jquery.js'></script>
		<script type='module' src='script/components/cwms-upload-file/cwms-upload-file.js'></script>
	</jsp:attribute>
	<jsp:attribute name="globalElements">
		<cwms-upload-file system-component-id="${sc.componentId}" entity-id1="${command.recall.identifier}" entity-id2=""
 			files-name="${filesName}" rename-to="" web="false"></cwms-upload-file>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox">
			<fieldset>
				<legend><spring:message code="recall.recall" />&nbsp;${command.recall.recallNo}</legend>
				<ol>
					<li>
						<label><spring:message code="fromcompany" />:</label>
						<span><links:companyLinkDWRInfo company="${command.recall.organisation}" rowcount="0" copy="false" /></span>
					</li>
					<li>
						<label><spring:message code="tocompany" />:</label>
						<span><links:companyLinkDWRInfo company="${command.recall.company}" rowcount="0" copy="false" /></span>
					</li>
					<li>
						<label><spring:message code="recall.recallexecutedon" />:</label>
						<span><fmt:formatDate value="${command.recall.date}" type="date" dateStyle="SHORT" /></span>
					</li>
					<li>
						<label><spring:message code="recall.recallcoveringdates" />:</label>
						<span><fmt:formatDate value="${command.recall.dateFrom}" type="date" dateStyle="SHORT" />&nbsp;<spring:message code="to" />&nbsp;<fmt:formatDate value="${command.recall.dateTo}" type="date" dateStyle="SHORT" /></span>
					</li>
					<li>
						<label><spring:message code="recall.contactsrecalled" />:</label>
						<span>${command.recall.rdetails.size()}</span>
					</li>
					<li>
						<label><spring:message code="recall.instrumentsrecalled" />:</label>
						<span>${command.recall.items.size()}</span>
					</li>
					<li>
						<label><spring:message code="recall.recallfilebrowser" />:</label>
						<div class="float-left overflow" style=" height: 140px; border: 1px solid #999; background-color: #fff; ">
							<files:showFilesForSC rootFiles="${command.scRootFiles}" id="${command.recall.id}" allowEmail="false" entity="${command.recall}" isEmailPlugin="false" ver="" identifier="${command.recall.identifier}" deleteFiles="false" sc="${command.sc}" rootTitle="<spring:message code='recall.filesforrecall' /> ${command.recall.recallNo}"/>
						</div>
						<!-- clear floats and restore page flow -->
						<div class="clear"></div>
					</li>
				</ol>
			</fieldset>
		</div>
	
		<div class="infobox">
			<table class="default4 viewrecall" summary="<spring:message code="recall.tableitemsrecalled" />">
				<thead>									
					<tr>
						<th scope="col" class="comp"><spring:message code="company" /></th>
						<th scope="col" class="cont"><spring:message code="contact" /></th>
						<th scope="col" class="type"><spring:message code="type" /></th>
						<th scope="col" class="items"><spring:message code="items" /></th>
						<th scope="col" class="files"><spring:message code="file" /></th>
						<th scope="col" class="pref"><spring:message code="recall.sendtype" /></th>
						<th scope="col" class="status"><spring:message code="sendnotification.table.sent" /></th>
						<th scope="col" class="quote"><spring:message code="recall.quote" /></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="8">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
					<c:set var="company" value="" />
					<c:set var="sub" value="" />
					<c:set var="rowcount" value="1" />
					<c:if test="${empty command.recall.rdetails}">
						<tr class="highlit">
							<td colspan="8">
								<spring:message code="recall.nocontactsconfigured" />
							</td>
						</tr>
					</c:if>
					<c:forEach var="rd" items="${command.recall.rdetails}">
						<c:choose>
							<c:when test="${rd.recallType == 'CONTACT'}">
								<c:set var="company" value="${rd.contact.sub.comp}" />
								<c:set var="sub" value="${rd.contact.sub.subname}" />
							</c:when>
							<c:otherwise>
								<c:set var="company" value="${rd.address.sub.comp}" />
								<c:set var="sub" value="${rd.address.sub.subname}" />
							</c:otherwise>
						</c:choose>			
					
						<tr id="parent${rowcount}" <c:if test="${rd.ignoredOnSetting == true || rd.ignoredAllOnJob == true}"> class="highlight" </c:if>>
							<td class="comp">
								<links:companyLinkDWRInfo company="${company}" rowcount="${rowcount}" copy="false" />
								<c:if test="${sub != null}">
								 - ${sub}
								</c:if>
								<c:choose>
									<c:when test="${rd.ignoredOnSetting == true}">
										&nbsp;<img src="img/icons/information.png" width="16" height="16" class="img_marg_bot" title="<spring:message code="recall.recallignored" />" /> <spring:message code="recall.recallignoredbysetting" />
									</c:when>
									<c:when test="${rd.ignoredAllOnJob == true}">
										&nbsp;<img src="img/icons/information.png" width="16" height="16" class="img_marg_bot" title="<spring:message code="recall.recallignored" />" /> <spring:message code="recall.recallignoredonjob" />
									</c:when>
								</c:choose>
							</td>
							<td class="cont">												
								<c:choose>
									<c:when test="${not empty rd.contact}">
										<links:contactLinkDWRInfo contact="${rd.contact}" rowcount="${rowcount}" />
									</c:when>
									<c:otherwise>
										<spring:message code="recall.calibratedequipmentmanager" />
									</c:otherwise>
								</c:choose>
								<c:if test="${rd.recallType == 'ADDRESS'}">
									- ${rd.address.addr1}
								</c:if>
							</td>
							<td class="type">${rd.recallType}</td>
							<td class="items">
								${rd.numberOfItems}								
								<a href="#" id="itemsLink${rowcount}" onclick=" getRecallItems(event, ${rowcount}, '${rd.recallType}', ${rd.recall.id}, '${rd.contact.personid}', '${rd.address.addrid}', ${rd.ignoredOnSetting}); return false; ">
									<img src="img/icons/items.png" width="16" height="16" alt="<spring:message code="recall.viewrecallitems" />" title="<spring:message code="recall.viewrecallitems" />" class="image_inline" />
								</a>
							</td>
							<td class="files">
								<c:choose>
									<c:when test="${not empty rd.pathToFile && rd.pathToFile != '' && rd.ignoredOnSetting == false && rd.ignoredAllOnJob == false}">
										<a href="downloadfile.htm?file=${rd.filePathEncrypted}" target="_blank" class="mainlink pdfdocument" title="<spring:message code="recall.viewpdfdocumentat" />">&nbsp;</a>
									</c:when>
									<c:otherwise>
										<spring:message code="recall.nofile" />
									</c:otherwise>
								</c:choose>
							</td>
							<td class="pref">${rd.recallSendType}</td>
							<td class="status">${rd.recallSendStatus}</td>
							<td class="quote">
								<c:if test="${rd.recallType == 'CONTACT' && rd.ignoredOnSetting == false && rd.ignoredAllOnJob == false}">
									<a href="recalltoquotation.htm?rdid=${rd.id}" class="mainlink"><spring:message code="create" /></a>
								</c:if>
							</td>
						</tr>
						<c:set var="rowcount" value="${rowcount + 1}" />
					</c:forEach>
				</tbody>
			</table>
		</div>
	</jsp:body>
</t:crocodileTemplate>