<%-- File name: /trescal/core/recall/viewrecalls.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="recall.recallbrowsershowingrecallsforlastyear" arguments="${recallform.months}"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/recall/ViewRecall.js'></script>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox">
		<form:form action="" name="recallform" id="recallform" method="post" modelAttribute="recallform">
			<form:hidden path="pageNo"/>
			<form:hidden path="resultsPerPage"/>
			<form:hidden path="months"/>
			
			<t:showResultsPagination rs="${recallform.rs}" pageNoId=""/>
			<table class="default4 viewrecalls" summary="<spring:message code='recall.tablerecallmonths' />">
				<thead>
					<tr>
						<td colspan="6">
							<span class="recallHistorySize">${recallform.resultsTotal}</span>
							<c:choose>
								<c:when test="${recallform.resultsTotal == 1}"><spring:message code="recall.recall"/></c:when>
								<c:otherwise><spring:message code="recall.recalls"/></c:otherwise>
							</c:choose>							
						</td>
					</tr>
					<tr>
						<td colspan="6">
								<label style=" width: 120px; "><spring:message code="company"/>:</label>
								<select id="clientSelector" onchange="filterByCompany()">
									<option value="0"> -- <spring:message code="all"/> -- </option>
									<c:forEach var="client" items="${clients}">
										<option value="${client.key}" <c:if test="${client.key == recallform.coid}"> selected</c:if>><c:out value="${client.value}"/></option>
									</c:forEach>
								</select>
						</td>
					</tr>
					<tr>
						<th class="recallid" scope="col"><spring:message code="recall.recallid"/></th>  
						<th class="recallcompany" scope="col"><spring:message code="tocompany"/></th>  
						<th class="recalldate" scope="col"><spring:message code="recall.dateofrecall"/></th>  
						<th class="covermonth" scope="col"><spring:message code="recall.coveringmonth"/></th>  
						<th class="contactsrecall" scope="col"><spring:message code="recall.contactsrecalled"/></th>
						<th class="unitsrecall" scope="col"><spring:message code="recall.unitsrecalled"/></th>
					</tr>
				</thead>								
				<tbody>
					<c:set var="lastRowcount" value="1" />
					<c:set var="lastRecallLoaded" value="" />
					<c:forEach var="r" items="${recallform.rs.getResults()}" varStatus="loopStatus">
						<c:choose>
							<c:when test="${loopStatus.count % 2 == 0}">
								<c:set var="rowClass" value="even"/>
							</c:when>
							<c:otherwise>
								<c:set var="rowClass" value="odd"/>
							</c:otherwise>
						</c:choose>
						<tr class="${rowClass}" id="${rowClass}" onclick="window.location.href='viewrecall.htm?id=${r.recallid}';" onmouseover="this.className = 'hoverrow'" onmouseout="this.className = this.id">
							<td>
								<a href="viewrecall.htm?id=${r.recallid}" class="mainlink">${r.recallNo}</a>
							</td>
							<td>${r.coname}</td>
							<td><fmt:formatDate value="${r.date}" type="date" dateStyle="SHORT" /></td>
							<td>
								<fmt:formatDate value="${r.dateFrom}" type="date" dateStyle="SHORT" />
								(<fmt:formatDate value="${r.dateTo}" type="date" dateStyle="SHORT" />)
							</td>
							<td>${r.rdetailsSize}</td>
							<td>${r.count}</td>
						</tr>
						<c:set var="lastRowcount" value="${loopStatus.count}"/>
						<c:set var="lastRecallLoaded"><fmt:formatDate value="${r.date}" type="date" dateStyle="SHORT" /></c:set>
					</c:forEach>
				</tbody>
			</table>
			<t:showResultsPagination rs="${recallform.rs}" pageNoId=""/>
			</form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>