<%-- File name: /trescal/core/account/creditcard.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="creditcard.headtext" /></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/admin/CreditCard.js'></script>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox">
			<c:if test="${creditcardform.creditCard != null && creditcardform.creditCard.ccid != 0 && !accountsAuthenticated}">
				<div class="warningBox1">
					<div class="warningBox2">
						<div class="warningBox3">
							<h5 class="center-0 attention"><spring:message code="creditcard.note1"/></h5>
						</div>
					</div>
				</div>
			</c:if>
			<form:form modelAttribute="creditcardform" action="" method="post">
				<fieldset>
					<legend><spring:message code="creditcard.addeditcard"/></legend>
					<ol>
						<li>
							<label><spring:message code="creditcard.cardno"/></label>
							<form:input path="cardno1" size="2" maxlength="4"/>-
							<form:input path="cardno2" size="2" maxlength="4"/>-
							<form:input path="cardno3" size="2" maxlength="4"/>-
							<form:input path="cardno4" size="2" maxlength="4"/>
							<span class="error">${status.errorMessage}</span>
						</li>
						<li>
							<form:label path="creditCard.cardname"><spring:message code="creditcard.cardholdername"/></form:label>
							<form:input path="creditCard.cardname"/>
							<span class="error"><form:errors path="creditCard.cardname"/></span>
						</li>
						<li>
							<form:label path="typeid"><spring:message code="creditcard.cardtype"/></form:label>
							<form:select path="typeid" items="${types}" itemValue="typeid" itemLabel="name"/>
						</li>
						<li>
							<form:label path="creditCard.issueno"><spring:message code="creditcard.issueno"/></form:label>
							<form:input path="creditCard.issueno"/>
							<span class="error"><form:errors path="creditCard.issueno"/></span>
						</li>
						<li>
							<form:label path="expDate1"><spring:message code="creditcard.expirydate"/></form:label>
							<form:select path="expDate1">
								<form:option value="01" label="01"/>
								<form:option value="02" label="02"/>
								<form:option value="03" label="03"/>
								<form:option value="04" label="04"/>
								<form:option value="05" label="05"/>
								<form:option value="06" label="06"/>
								<form:option value="07" label="07"/>
								<form:option value="08" label="08"/>
								<form:option value="09" label="09"/>
								<form:option value="10" label="10"/>
								<form:option value="11" label="11"/>
								<form:option value="12" label="12"/>
							</form:select>
							/
							<form:select path="expDate2">
								<form:option value="2017" label="2017"/>
								<form:option value="2018" label="2018"/>
								<form:option value="2019" label="2019"/>
								<form:option value="2020" label="2020"/>
								<form:option value="2021" label="2021"/>
								<form:option value="2022" label="2022"/>
								<form:option value="2023" label="2023"/>
							</form:select>
						</li>
						<li>
							<form:label path="creditCard.houseno"><spring:message code="creditcard.houseno"/></form:label>
							<form:input path="creditCard.houseno"/>
							<span class="error"><form:errors path="creditCard.houseno"/></span>
						</li>
						<li>
							<form:label path="creditCard.postcode"><spring:message code="creditcard.postcode"/></form:label>
							<form:input path="creditCard.postcode"/>
							<span class="error"><form:errors path="creditCard.postcode"/></span>
						</li>
						<li>
							<label for="submit">&nbsp;</label>
							<input type="submit" name="submit" id="submit" value="<spring:message code='creditcard.submit'/>"/>
						</li>
					</ol>									
				</fieldset>
			</form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>