<%-- File name: /trescal/core/jobs/jobitem/jiactions.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs"%>
<%@ taglib prefix="addr" tagdir="/WEB-INF/tags/address"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<jobs:jobItemTemplate>
	<jsp:attribute name="subScript">
		<script src='script/trescal/core/jobs/jobitem/JIActions.js'></script>
		<script src='script/trescal/core/documents/ImageGalleryFunctions.js'></script>
		<script>
			// grab scroll to parameter passed when using job item key navigation
			var scrollto = '${request.getParameter("scrollto")}';
		</script>
	</jsp:attribute>
	<jsp:attribute name="warnings">
		<!-- error section displayed when form submission fails -->				
		<form:errors path="form.*">
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<h5 class="center-0 attention"><spring:message code="jiactions.errorsocc"/>:</h5>
						<c:forEach var="e" items="${messages}">
							<div class="center attention"><c:out value="${e}"/></div>
						</c:forEach>
					</div>
				</div>
			</div>
		</form:errors>

		<c:if test="${form.message != null && form.message != ''}">
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						${form.message}
					</div>
				</div>
			</div>
		</c:if>
		<!-- end error section -->
	</jsp:attribute>
	<jsp:body>
		<!-- Page specific code -->
		<!-- infobox is styled with nifty corners -->
		<div class="infobox">
			<table class="default2" id="tableofactions" summary="<spring:message code='jiactions.string6'/>">
				<thead>
					<tr>
						<td colspan="7">
							<c:if test="${form.ji.state.status && !form.ji.state.hold && canOverride}">
								<a href="" onclick="event.preventDefault(); loadOverrideOverlay(${form.ji.jobItemId});" class="mainlink<c:if test='${incompleteAct}'> hid</c:if>"><spring:message code="jiactions.overrstat"/></a>
							</c:if>
							<c:if test="${holdstatuses.size() > 0}">
								<c:if test="${form.ji.state.status && !form.ji.state.hold && canOverride}">&nbsp;&nbsp;|&nbsp;&nbsp;</c:if>
								<a href="" onclick="event.preventDefault(); loadHoldOverlay(${form.ji.jobItemId});" class="mainlink<c:if test='${incompleteAct}'> hid</c:if>">
									<c:choose>
										<c:when test="${form.ji.state.status && form.ji.state.hold}"><spring:message code="jiactions.takeitemoffhold"/></c:when>
										<c:otherwise><spring:message code="jiactions.placeitemonhold"/></c:otherwise>
									</c:choose>
								</a>
							</c:if>
							<c:if test="${incompleteAct}">
								<spring:message code="jiactions.activitytocomplete"/>
							</c:if>
							<c:if test="${viewWrapper.readyForCalibration and not empty form.ji.nextWorkReq}">
								&nbsp;|&nbsp;
								<a href="#"
									onclick="event.preventDefault(); document.querySelector('cwms-nooperation-performed').show()"
									id="uploadLink" class="mainlink"
									title="<spring:message code="vm_global.noactionrequired"/>">
									<spring:message code="vm_global.noactionrequired" />
								</a>
						 	</c:if>
						</td>
					</tr>
					<tr>
						<td colspan="7">
							<c:set var="deletedActions" value="${form.ji.actions.size() - form.ji.activeActionsSize}"/>
							<spring:message code="jiactions.actactions"/> (<span class="jiActionSize">${form.ji.activeActionsSize}</span>) -
							<spring:message code="jiactions.delactions"/> (${deletedActions})
						</td>
					</tr>
					<tr>
						<td colspan="7">
							<spring:message code="jiactions.viewdel"/>: <input id="deletedCheck" type="checkbox" onclick="toggleActions('deleted', this);"/>
							&nbsp;&nbsp;&nbsp;
							<spring:message code="jiactions.viewact"/>: <input type="checkbox" id="activitiesCheck" onclick="toggleActions('activity', this);" checked="checked"/>
							&nbsp;&nbsp;&nbsp;
							<spring:message code="jiactions.viewtrans"/>: <input type="checkbox" id="transitsCheck" checked="checked" onclick="toggleActions('transit', this);"/>
							&nbsp;&nbsp;&nbsp;
							<spring:message code="jiactions.viewoverr"/>: <input type="checkbox" id="overridesCheck" checked="checked" onclick="toggleActions('override', this);"/>
							<div class="float-right padtop">
								<a href="#" id="toggleActionColumns" class="mainlink" onclick="toggleActionColumns(this, true); return false;"><spring:message code="jiactions.showstartstat"/></a>
							</div>
						</td>
					</tr>
					<tr>
						<th class="step" scope="col"><spring:message code="jiactions.step"/></th>
						<th class="bstatus" scope="col"><spring:message code="jiactions.begstatus"/></th>
						<th class="activity" scope="col"><spring:message code="jiactions.activity"/></th>
						<th class="startby" scope="col"><spring:message code="jiactions.startedby"/></th>
						<th class="completeby" scope="col"><spring:message code="jiactions.completedby"/></th>
						<th class="estatus" scope="col"><spring:message code="jiactions.endstatus"/></th>
						<th class="time" scope="col"><spring:message code="jiactions.time"/></th>
						<th class="remark" scope="col"><spring:message code="jiactions.remark"/></th>
						<th class="edit" scope="col"><spring:message code="viewjob.edit"/></th>
					</tr>
				</thead>
				<tbody id="actions">
					<c:set var="incompleteActivities" value="false"/>
					<c:set var="stepNo" value="1"/>
					<c:choose>
						<c:when test="${form.ji.activeActionsSize == 0}">
							<tr>
								<td colspan="7" class="center bold">
									<spring:message code="jiactions.string13"/>
								</td>
							</tr>
						</c:when>
						<c:otherwise>
							<c:forEach var="a" items="${form.ji.actions}">
								<c:set var="lastAct" value="${a.id}"/>
								<c:choose>
									<c:when test="${!a.deleted && a.transit}">
										<tr class="transit" id="transit${a.id}">
											<td class="center">${stepNo}</td>
											<td class="bstatus"><t:showTranslationOrDefault translations="${a.startStatus.translations}" defaultLocale="${defaultlocale}"/></td>
											<td>
												<t:showTranslationOrDefault translations="${a.activity.translations}" defaultLocale="${defaultlocale}"/>
												(<c:if test="${a.fromAddr != null}">
													<a class="mainlink" href="viewaddress.htm?addrid=${a.fromAddr.addrid}">
														${a.fromAddr.addr1}, ${a.fromAddr.town}
													</a>
													<c:if test="${a.fromLoc != null}">
														- <a class="mainlink" href="location.htm?locid=${a.fromLoc.locationid}">${a.fromLoc.location}</a>
													</c:if>
												</c:if>
												&nbsp;~~>>&nbsp;
												<c:if test="${a.toAddr != null}">
													<a class="mainlink" href="viewcomp.htm?coid=${a.toAddr.sub.comp.coid}">${a.toAddr.sub.comp.coname}</a> ,
													<a class="mainlink" href="viewaddress.htm?addrid=${a.toAddr.addrid}">${a.toAddr.addr1}, ${a.toAddr.town}</a>
													<c:if test="${a.toLoc != null}">
														- <a class="mainlink" href="location.htm?locid=${a.toLoc.locationid}">${a.toLoc.location}</a>
													</c:if>
												</c:if>)
											</td>
											<td class="startby">
												${a.startedBy.name}<br />
												(<fmt:formatDate value="${a.startStamp}" type="both" dateStyle="SHORT" timeStyle="SHORT"/>)
											</td>
											<td>
												${a.startedBy.name}<br />
												(<fmt:formatDate value="${a.endStamp}" type="both" dateStyle="SHORT" timeStyle="SHORT"/>)
											</td>
											<td colspan="2">
												<t:showTranslationOrDefault translations="${a.endStatus.translations}" defaultLocale="${defaultlocale}"/>
											</td>
											<td>${a.remark}</td>
											<td>&nbsp;</td>
										</tr>
										<c:set var="stepNo" value="${stepNo + 1}"/>
									</c:when>
									<c:when test="${!a.deleted && !a.transit}">
										<tr <c:choose><c:when test="${a.activity.override}">class="override" id="override${a.id}"</c:when><c:otherwise>class="activity" id="activity${a.id}"</c:otherwise></c:choose>>
											<td class="center">${stepNo}</td>
											<td class="bstatus"><t:showTranslationOrDefault translations="${a.startStatus.translations}" defaultLocale="${defaultlocale}"/></td>
											<td>
												<t:showTranslationOrDefault translations="${a.activity.translations}" defaultLocale="${defaultlocale}"/>
												<c:if test="${not empty a.outcome}">
 													<br />(<c:out value="${a.outcome.genericValue.translation}" />)
												</c:if>
											</td>
											<td class="startby">
												${a.startedBy.name}<br />
												(<fmt:formatDate value="${a.startStamp}" type="both" dateStyle="SHORT" timeStyle="SHORT"/>)
											</td>
											<c:choose>
												<c:when test="${a.complete}">
													<td>
														${a.completedBy.name}<br />
														(<fmt:formatDate value="${a.endStamp}" type="both" dateStyle="SHORT" timeStyle="SHORT"/>)
													</td>
													<td><t:showTranslationOrDefault translations="${a.endStatus.translations}" defaultLocale="${defaultlocale}"/></td>
													<td class="time center"><c:out value="${a.timeSpent} " /><spring:message code="jobcost.mins"/></td>
													<td class="remark">${a.remark}</td>
													<td class="edit">
														<c:if test="${currentContact.personid == a.completedBy.personid && not rrtItemStates.contains(a.activity.stateid)}">
															<a href="#" onclick="editAction($j(this).closest('tr'), ${a.id}); return false;"><img src="img/icons/note_edit.png" height="16" width="16" alt="<spring:message code='jiactions.editaction'/>" title="<spring:message code='jiactions.editaction'/>"/></a>
														</c:if>
													</td>
												</c:when>
												<c:otherwise>
													<td class="notcomplete" colspan="5">
														<c:set var="isCalibration" value="${false}"/>
														<c:set var="isGeneralServiceOperation" value="${false}"/>
														<c:forEach var="gl" items="${a.activity.groupLinks}">
															<c:if test="${gl.group.keyName == 'calibration'}">
																<c:set var="isCalibration" value="${true}"/>
															</c:if>
															<c:if test="${gl.group.keyName == 'generalserviceoperation'}">
																<c:set var="isGeneralServiceOperation" value="${true}"/>
															</c:if>
														</c:forEach>
														<input type="button" value="<spring:message code='jiactions.complact'/>" 
															<c:choose>
																<c:when test="${isCalibration}">
																	onclick="window.location.href='jicalibration.htm?jobitemid=${form.ji.jobItemId}';"
																</c:when>
																<c:when test="${isGeneralServiceOperation}">
																	onclick="window.location.href='completegso.htm?jobitemid=${form.ji.jobItemId}';"
																</c:when>
																<c:otherwise>
																	onclick="completePendingActivity(${form.ji.jobItemId}, 'work'); return false;"
																</c:otherwise>
															</c:choose>
														/>
														<%-- Access to multi-complete feature only enabled if 'cwms.config.calibrations.completemultiple' property set to true --%>
														<c:if test="${isCalibration && completeMultipleCalibrations && not empty optionalMultiCompleteItems}">
															<input type="button" class="marg-left" value="<spring:message code='jiactions.complmultcals'/>" onclick="this.disabled = true; window.location.href = 'multicomplete.htm?jobid=${form.ji.job.jobid}&jiid=${form.ji.jobItemId}';"/>																			
														</c:if>
													</td>
													<c:set var="incompleteActivities" value="true"/>
													<c:set var="incompleteActId" value="${a.id}"/>
												</c:otherwise>
											</c:choose>
										</tr>
										<c:set var="stepNo" value="${stepNo + 1}"/>
									</c:when>
									<c:when test="${a.deleted && a.transit}">
										<tr class="deleted hid" id="transit${a.id}">
											<td class="center">(Deleted by)
											<br />
											 <!-- Delete Time stamp -->
											<fmt:formatDate value="${a.deleteStamp}" type="both" dateStyle="MEDIUM" timeStyle="SHORT"/>
											<br />
											 <!-- Delete by Username  -->
											<c:out value="${a.deletedBy.name}"/>
											
											</td>
											<td class="bstatus"><t:showTranslationOrDefault translations="${a.startStatus.translations}" defaultLocale="${defaultlocale}"/></td>
											<td><t:showTranslationOrDefault translations="${a.activity.translations}" defaultLocale="${defaultlocale}"/>
													(<c:if test="${a.fromAddr != null}">
														<a class="mainlink" href="viewaddress.htm?addrid=${a.fromAddr.addrid}&subdivid=${a.fromAddr.sub.subdivid}">${a.fromAddr.addr1}, ${a.fromAddr.town}</a>
														<c:if test="${a.fromLoc != null}">
															- <a class="mainlink" href="location.htm?locid=${a.fromLoc.locationid}">${a.fromLoc.location}</a>
														</c:if>
													</c:if>
													<c:if test="${a.toAddr != null}">
														<a class="mainlink" href="viewaddress.htm?addrid=${a.toAddr.addrid}&subdivid=${a.toAddr.sub.subdivid}">${a.toAddr.addr1}, ${a.toAddr.town}</a>
														<c:if test="${a.toLoc != null}">
															- <a class="mainlink" href="location.htm?locid=${a.toLoc.locationid}">${a.toLoc.location}</a>
														</c:if>
													</c:if>)
												</td>
											<td class="startby">
												${a.startedBy.name}<br />
												(<fmt:formatDate value="${a.startStamp}" type="both" dateStyle="SHORT" timeStyle="SHORT"/>)
											</td>
											<td>
												${a.startedBy.name}<br />
												(<fmt:formatDate value="${a.endStamp}" type="both" dateStyle="SHORT" timeStyle="SHORT"/>)
											</td>
											<td colspan="2">
												<t:showTranslationOrDefault translations="${a.endStatus.translations}" defaultLocale="${defaultlocale}"/><br />
											</td>
											<td>${a.remark}</td>
											<td>&nbsp;</td>
										</tr>
									</c:when>
									<c:otherwise>
										<tr <c:choose><c:when test="${a.activity.override}">class="override deleted hid" id="override${a.id}"</c:when><c:otherwise>class="activity deleted hid" id="activity${a.id}"</c:otherwise></c:choose>>
											<td class="center">(<spring:message code="jiactions.deleted"/>)</td>
											<td class="bstatus"><t:showTranslationOrDefault translations="${a.startStatus.translations}" defaultLocale="${defaultlocale}"/></td>
											<td>
												<t:showTranslationOrDefault translations="${a.activity.translations}" defaultLocale="${defaultlocale}"/>
												<c:if test="${a.outcome != null}">
													<br />(<t:showTranslationOrDefault translations="${a.outcome.translations}" defaultLocale="${defaultlocale}"/>)
												</c:if>
											</td>
											<td class="startby">
												${a.startedBy.name}<br />
												(<fmt:formatDate value="${a.startStamp}" type="both" dateStyle="SHORT" timeStyle="SHORT"/>)
											</td>
											<c:choose>
												<c:when test="${a.complete}">
													<td>
														${a.completedBy.name}<br />
														(<fmt:formatDate value="${a.endStamp}" type="both" dateStyle="SHORT" timeStyle="SHORT"/>)
													</td>
													<td><t:showTranslationOrDefault translations="${a.endStatus.translations}" defaultLocale="${defaultlocale}"/></td>
													<td class="time center"><c:out value="${a.timeSpent} "/><spring:message code="jobcost.mins"/></td>
													<td class="remark">${a.remark}</td>
													<td class="edit">
														<c:if test="${currentContact.personid == a.completedBy.personid}">
															<a href="#" onclick="editAction($j(this).closest('tr'), ${a.id}); return false;"><img src="img/icons/note_edit.png" height="16" width="16" alt="<spring:message code='jiactions.editaction'/>" title="<spring:message code='jiactions.editaction'/>"/></a>
														</c:if>
													</td>
												</c:when>
												<c:otherwise>
													<td class="notcomplete" colspan="5">
														<input type="button" value="<spring:message code='jiactions.complact'/>" onclick="completePendingActivity(${form.ji.jobItemId}, 'work'); return false;"/>
													</td>
													<c:set var="incompleteActivities" value="true"/>
													<c:set var="incompleteActId" value="${a.id}"/>
												</c:otherwise>
											</c:choose>
										</tr>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="7" align="right">
							<a href="img/workflow.pdf" target="_blank">
								<img src="img/icons/help.png" width="16" height="16" title="view workflow pdf" class="img_marg_bot" />
							</a>
							&nbsp;&nbsp;
							<c:choose>
								<c:when test="${isintransit}">
									<spring:message code="jiactions.string7"/>:
									<form:form id="form3" action="" method="post" class="inline">
										<input type="hidden" name="submitAction" value="receive" />
										<a href="#" class="mainlink" onclick="$j('#form3').submit(); return false;"><spring:message code="jiactions.receiveitem"/></a>
									</form:form>
								</c:when>
								<c:otherwise>
									<c:set var="viewClass" value="vis"/>
									<c:if test="${incompleteActivities}"><c:set var="viewClass" value="hid"/></c:if>
									<span id="newActivity" class="${viewClass}"><a href="#" class="mainlink" onclick="beginActivity('work', ${form.ji.jobItemId}); return false;"><spring:message code="jiactions.newactivity"/></a>&nbsp;&nbsp;|&nbsp;&nbsp;</span>
									<span id="newProgActivity" class="${viewClass}"><a href="#" class="mainlink" onclick="beginActivity('progress', ${form.ji.jobItemId}); return false;"><spring:message code="jiactions.newprogact"/></a>&nbsp;&nbsp;|&nbsp;&nbsp;</span>
									<%--<span id="newTransit" class="${viewClass}"><a href="#" class="mainlink" onclick="beginActivity('transit', ${form.ji.jobItemId}); return false;"><spring:message code="jiactions.newtransit"/></a>&nbsp;&nbsp;|&nbsp;&nbsp;</span>  --%>
									<c:if test="${form.canDeleteLastActivity}">
										<form:form id="form2" method="post" action="" class="inline">
											<input type="hidden" name="submitAction" value="delete" />
											<a href="#" class="mainlink" onclick="$j('#form2').submit(); return false;"><spring:message code="jiactions.dellastact"/></a>
										</form:form>
									</c:if>
								</c:otherwise>
							</c:choose>
							<span>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="#" class="mainlink" onclick="createChaseContent(${form.ji.jobItemId}); return false;"><spring:message code="jiactions.chaseoptions"/></a></span>
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
		<!-- end of infobox div -->
		<!-- new work activity div -->
		<div class="hid" id="newactivitydiv">
			<!-- infobox is styled with nifty corners -->
			<div class="infobox">
				<fieldset>
					<legend><spring:message code="jiactions.newactivity"/></legend>
					<ol>
						<c:choose>
							<c:when test="${allowedActivities == null || allowedActivities.size() == 0}">
								<li><spring:message code="jiactions.string8"/>.</li>
							</c:when>
							<c:otherwise>
								<li class="startfields vis">
								<label><spring:message code="jiactions.selonact"/>:</label>
								<select id="workactivity" onchange="changeOutcomeStatuses(this.value, this.options[this.selectedIndex].text, $j(this.options[this.selectedIndex]).hasClass('lookup')); return false;">
									<c:forEach var="na" items="${allowedActivities}">
										<c:set var="a" value="${na.activity}"/>
										<c:set var="lookup" value=""/>
										<c:if test="${a.lookup != null}">
											<c:set var="lookup" value="lookup"/>
										</c:if>
										<option class="${lookup}" value="${a.stateid}"><t:showTranslationOrDefault translations="${a.translations}" defaultLocale="${defaultlocale}"/></option>
									</c:forEach>
								</select>
							</li>
							<li class="startfields vis">
								<label>&nbsp;</label>
								<input type="button" value="<spring:message code='jiactions.startact'/>" onclick="insertIncompleteActivity(${form.ji.jobItemId}, 'work'); return false;"/>
								<input type="button" value="<spring:message code='jiactions.complact'/>" onclick="completeNow(); return false;"/>
							</li>
							</c:otherwise>
						</c:choose>
						<li class="endfields hid">
							<label class="twolabel"><spring:message code="jiactions.remark"/>:</label>
							<div class="float-left width80">
								<textarea id="workremark" class="width96 presetComments ACTIVITY"></textarea>
							</div>
							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
						</li>
						<li class="endfields hid">
							<label><spring:message code="jiactions.timespent"/>:</label>
							<input id="worktimespent" type="text" value="${form.timeSpent}"/>&nbsp;<spring:message code="jobcost.mins"/>
							<c:if test="${incompleteActId != null}">
								<a href="#" onclick="getMinutesSinceStart(${incompleteActId}); return false;"><img src="img/icons/refresh.png" width="16" height="16" alt="<spring:message code='jiactions.imgrefreshmin'/>" title="<spring:message code='jiactions.imgrefreshmin'/>"/></a>
							</c:if>
						</li>
						<li class="endfields hid">
							<label><spring:message code="jiactions.actoutcome"/>:</label>
							<select id="workactionoutcome">
								<c:choose>
									<c:when test="${actionOutcomes != null && actionOutcomes.size() > 0}">
										<c:forEach var="ao" items="${actionOutcomes}">
											<c:if test="${ao.active}">
												<option value="${ao.id}"><t:showTranslationOrDefault translations="${ao.translations}" defaultLocale="${defaultlocale}"/></option>
											</c:if>
										</c:forEach>
									</c:when>
									<c:otherwise>
										<option value="">-- N/A --</option>
									</c:otherwise>
								</c:choose>
							</select>
						</li>
						<li class="endfields hid">
							<label><spring:message code="jiactions.seloutcomestat"/>:</label>
							<div id="workendstatusfields">
								<c:set var="hasLookup"
									value="${incompleteAct && !form.ji.state.isStatus() && form.ji.state.lookup != null ||
										!incompleteAct && allowedActivities.size() > 0 && allowedActivities.get(0).activity.lookup != null}"/>
								<span class="${hasLookup ? 'vis' : 'hid'}">
									<spring:message code="jiactions.string9"/>
								</span>
								<span class="${hasLookup ? 'hid' : 'vis'}">
									<select id="workendstatus">
										<c:forEach var="os" items="${allowedOutcomes}">
											<c:set var="s" value="${os.status}"/>
											<option value="${s.stateid}"><cwms:besttranslation translations="${s.translations}"/></option>
										</c:forEach>
									</select>
								</span>
								<!-- clear floats and restore page flow -->
								<div class="clear"></div>
							</div>
						</li>
						<li class="endfields hid">
							<label><spring:message code="jiactions.samestat"/>:</label>
							<div id="workSSItems" class="float-left padtop"></div>
							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
						</li>
						<li class="endfields hid">
							<label>&nbsp;</label>
							<input type="button" id="backButton" <c:choose><c:when test="${incompleteActivities}">class="hid"</c:when><c:otherwise>class="vis"</c:otherwise></c:choose> value="<spring:message code='jiactions.canact'/>" onclick="cancelActivity(); return false;"/>
							<input type="button" id="noIncompleteButton" <c:choose><c:when test="${incompleteActivities}">class="hid"</c:when><c:otherwise>class="vis"</c:otherwise></c:choose> value="<spring:message code='jiactions.complact'/>" onclick="insertCompletedActivity(${form.ji.jobItemId}, 'work'); return false;"/>
							<input type="button" id="incompleteButton" <c:choose><c:when test="${incompleteActivities}">class="vis"</c:when><c:otherwise>class="hid"</c:otherwise></c:choose> value="<spring:message code='jiactions.complpendact'/>" onclick="updatePendingActivity(${form.ji.jobItemId}, 'work'); return false;"/>
						</li>
					</ol>					
				</fieldset>
			</div>
			<!-- end of infobox div -->
		</div>
		<!-- end of work activity div -->
		<!-- new progress activity div -->
		<div class="hid" id="newprogactivitydiv">
			<!-- infobox is styled with nifty corners -->
			<div class="infobox">
				<fieldset>
					<legend><spring:message code="jiactions.newprogact"/></legend>
					<ol>
						<li>
							<label><spring:message code="jiactions.progact"/>:</label>
							<select id="progressactivity">
								<c:forEach var="a" items="${allProgressActivities}">
									<option value="${a.stateid}"><t:showTranslationOrDefault translations="${a.translations}" defaultLocale="${defaultlocale}"/></option>
								</c:forEach>
							</select>
						</li>
						<li>
							<label class="twolabel"><spring:message code="jiactions.remark"/>:</label>
							<div class="float-left width80">
								<textarea id="progressremark" class="width96 presetComments ACTIVITY" rows="6"></textarea>
								<br/>
								<spring:message code="jiactions.string10"/> <input type="checkbox" id="progressjobnote" checked="checked"/>
							</div>
							<div class="clear"></div>
						</li>
						<li>
							<label><spring:message code="jiactions.timespent"/>:</label>
							<input type="text" id="progresstimespent" value="${form.timeSpent}"/>&nbsp;<spring:message code="jobcost.mins"/>
						</li>
						<li>
							<label><spring:message code="jiactions.samestat"/>:</label>
							<div id="progressSSItems" class="float-left padtop">
							</div>
							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
						</li>
						<li>
							<label>&nbsp;</label>
							<input type="button" value="<spring:message code='jiactions.string11'/>" onclick="insertCompletedActivity(${form.ji.jobItemId}, 'progress'); return false;"/>
						</li>
					</ol>
				</fieldset>
			</div>
			<!-- end of infobox div -->
		</div>
		<!-- end of progress activity div -->
		<!-- End of page specific code -->
	</jsp:body>
</jobs:jobItemTemplate>