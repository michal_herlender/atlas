<%-- File name: /trescal/core/jobs/jobitem/jiactions.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs"%>
<%@ taglib prefix="addr" tagdir="/WEB-INF/tags/address"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<jobs:jobItemTemplate>
<jsp:attribute name="subScript">
	<script type='text/javascript' src='script/trescal/core/jobs/jobitem/JobItemFunctions.js'></script>
</jsp:attribute>

<jsp:attribute name="warnings">
	<!-- error section displayed when form submission fails -->
	<form:errors path="form.*">
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<h5 class="center-0 attention"><spring:message code="jiactions.errorsocc"/>:</h5>
						<c:forEach var="e" items="${messages}">
							<div class="center attention"><c:out value="${e}"/></div>
						</c:forEach>
					</div>
				</div>
			</div>
		</form:errors>
	<!-- end error section -->
</jsp:attribute>
<jsp:body>

<div class="infobox">

	<c:if test="${ allowRIRCreationForCurrentSubdiv }">
		<a href="repairreports.htm?jobitemid=${form.ji.jobItemId}" target="blank" class="mainlink-float" >
	    	<spring:message code="tools.createreport" />
	   	</a>
    <div class="clear"></div>
	</c:if>
    
<table class="default2" >
	<thead>
		<tr>
			<th class="status" scope="col"><spring:message code="repairinspection.headtext" /></th>
			<th class="status" scope="col"><spring:message code="viewworkinstructionversion.creationdate" /></th>
			<th class="status" scope="col"><spring:message code="repairinspection.technicalvalidation" /></th>
			<th class="status" scope="col"><spring:message code="repairinspection.managervalidation" /></th>
			<th class="status" scope="col"><spring:message code="repaircompletionreport.operations" /></th>
			<th class="status" scope="col"><spring:message code="repaircompletionreport.headtext" /></th>
			<th class="status" scope="col"><spring:message code="repaircompletionreport.repairtime" /></th>
			<th class="status" scope="col"><spring:message code="subdiv" /></th>
		</tr>
	</thead>
	<tbody>
	
	<c:if test="${ form.ji.repairInspectionReports.size() eq 0 }" >
		<tr>
			<td colspan="9" class="center bold"><spring:message code="repairinspection.emptylist"/></td>
		</tr>
	</c:if>
	<c:forEach var="rir" items="${ form.ji.repairInspectionReports }" varStatus="varStatus" >
		<tr>
			<td><a class="mainlink" href="repairreports.htm?rirId=${ rir.rirId }&jobitemid=${form.ji.jobItemId}">${ rir.rirNumber }</a></td>
			<td><fmt:formatDate type="both" dateStyle="SHORT" timeStyle="SHORT" value="${ rir.creationDate }"/></td>
			<td><c:if test="${ not empty rir.technician }"><links:contactLinkDWRInfo contact="${ rir.technician }" rowcount="1"/>&nbsp;<fmt:formatDate type="both" dateStyle="SHORT" timeStyle="SHORT" value="${ rir.completedByTechnicianOn }"/></c:if></td>
			<td><c:if test="${ not empty rir.manager }"><links:contactLinkDWRInfo contact="${ rir.manager }" rowcount="1"/>&nbsp;<fmt:formatDate type="both" dateStyle="SHORT" timeStyle="SHORT" value="${ rir.validatedByManagerOn }"/></c:if></td>
			<td>${ rir.freeRepairOperations.size() }</td>
			<c:if test="${ varStatus.index eq 0 }">
				<td rowspan="${ form.ji.repairInspectionReports.size() }" >
					<c:choose>
						<c:when test="${ not empty rir.repairCompletionReport }">
							<a class="mainlink" href="repairreports.htm?rirId=${ rir.rirId }&jobitemid=${form.ji.jobItemId}&showRcr=true">
								<img src="img/icons/link.png" width="16" height="16" alt="<spring:message code='viewjob.link'/>" title="<spring:message code='viewjob.link'/>" />
							</a>
						</c:when>
						<c:otherwise>
							<spring:message code="company.none" />
						</c:otherwise>
					</c:choose>
				</td>
			</c:if>
			<td>
				<c:choose>
					<c:when test="${ allowRepairTimeUpdate }">
						<a class="mainlink" href="#" onclick="manageRepairTime(${ rir.repairCompletionReport.rcrId },${form.ji.jobItemId}); return false;">
							<img src="img/icons/link.png" width="16" height="16" alt="<spring:message code='repaircompletionreport.repairtime'/>" title="<spring:message code='repaircompletionreport.repairtime'/>" />
						</a>
					</c:when>
					<c:otherwise>
						<spring:message code="company.none" />
					</c:otherwise>
				</c:choose>
			</td>
			<td><links:subdivLinkDWRInfo subdiv="${ rir.organisation }" rowcount="1" /></td>
		</tr>
	</c:forEach>
	</tbody>
</table>
</div>

</jsp:body>
</jobs:jobItemTemplate>