<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <link rel="stylesheet" href="styles/global.css" type="text/css" title="antech" />

    <link rel="stylesheet" href="styles/print/print.css" type="text/css" media="print" />

    <script type='text/javascript'>

        // get value of loadtab parameter from querystring
        // used to show the correct tab when subnavigation bar is used
        var LOADTAB = '${request.getParameter("loadtab")}';
        // set variable to i18n of javascript
        var i18locale = '${rc.locale.language}';


    </script>

    <script type='text/javascript' src='script/thirdparty/jQuery/jquery.js'></script>

    <script type='text/javascript' src='script/trescal/core/template/Cwms_Main.js'></script>

    <script type='text/javascript' src='script/trescal/core/jobs/calibration/CalibrationPoints.js'></script>
</head>

<body onload=" loadScript.init(true); " class="iframed" style=" width: 860px; min-width: 860px; ">

<form:form name="" action="" id="calibrationpointsform"  style=" width: 100%; " method="post">

<%--     <t:showErrors path="command.*" showFieldErrors="true" showFieldName="false"/> --%>
	<form:errors path="*">
  		<div class="warningBox1">
   			 <div class="warningBox2">
     		 <div class="warningBox3">
      		   <h5 class="center-0 attention">
        	   <spring:message code="error.save"/>
        	  </h5>
     		 </div>
  		 </div>
 	 </div>
	</form:errors>

    <div class="publishCalReqs">
        <a href="#" class="${command.cr.publish == true ? 'publishCalreq' : 'publishCalreq_inactive'}" onclick=" publishCalReq(this, true); return false; " title="<spring:message code='calibrationpoints.title1'/>"><spring:message code="calibrationpoints.published"/></a>
        <a href="#" class="${command.cr.publish == true ? 'publishCalreq_inactive' : 'publishCalreq'}" onclick=" publishCalReq(this, false); return false; " title="<spring:message code='calibrationpoints.title2'/>"><spring:message code="calibrationpoints.notpublished"/></a>
        <form:hidden path="cr.publish"/>
    </div>

    <div>&nbsp;</div>

    <div id="subnav">
        <dl>
            <dt><a href="#" onclick="switchMenuFocus(menuElements, 'points-tab', false); $j('input#action').val('pointset'); return false; " id="points-link" ${command.action == "pointset" ? 'class="selected"' : ''} title="<spring:message code='calibrationpoints.title3'/>"><spring:message code="calibrationpoints.points"/></a></dt>
            <dt><a href="#" onclick="switchMenuFocus(menuElements, 'range-tab', true); $j('input#action').val('range'); return false; " id="range-link" ${command.action == "range" ? 'class="selected"' : ''} title="<spring:message code='calibrationpoints.title4'/>"><spring:message code="calibrationpoints.range"/></a></dt>
            <dt><a href="#" onclick="switchMenuFocus(menuElements, 'textonly-tab', true); $j('input#action').val('textonly'); return false; " id="textonly-link" ${command.action == "textonly" ? 'class="selected"' : ""} title="<spring:message code='calibrationpoints.title5'/>"><spring:message code="calibrationpoints.textonly"/></a></dt>
        </dl>
    </div>

    <div id="points-tab" ${command.action != "pointset" ? 'class="hid"' : ''}>

        <fieldset>

            <ol id="pointslist">

                <li>
                    <label><spring:message code="calibrationpoints.calpoints"/>:</label>

                    <c:choose>
                       <c:when test="${templates.size() > 0}">
                           <select name="templatelist" class="width50" onchange=" loadPointsTemplate(this.value); return false; " style=" visibility: hidden; ">
                               <option value=""><spring:message code="calibrationpoints.option1"/></option>
                               <c:forEach var="temp" items="${templates}">
                                   <option value="${temp.id}">${temp.title}</option>
                               </c:forEach>
                           </select>
                        </c:when>
                        <c:otherwise>
                            <span><spring:message code="calibrationpoints.string1"/></span>
                        </c:otherwise>
                    </c:choose>

                    <form:checkbox path="relational" id="currentlyRelational" onclick="toggleRelational(this, ${command.relational});"/>
                    &nbsp;<spring:message code="calibrationpoints.relational"/>

                </li>

                <c:set var="count" value="0"/>
                <c:set var="tabindex1" value="1"/>
                <c:set var="tabindex2" value="10"/>
                <c:set var="tabindex3" value="19"/>
                <c:set var="tabindex4" value="28"/>
                <c:forEach var="point" items="${command.points}" varStatus="pointStatus">
                  <c:set var="count" value="${pointStatus.index}"/>
                <li id="point${pointStatus.index}">
                    <label>
                        <spring:message code="calibrationpoints.point" /> ${pointStatus.count}:
                        <c:set var="errorOnThisPoint" value="${false}"/>
                        <form:errors path="points[${count}]">
                        	<c:set var="errorOnThisPoint" value="${true}"/>	
                        </form:errors>
						<form:errors path="relativePoints[${count}]">
                        	<c:set var="errorOnThisPoint" value="${true}"/>	
                        </form:errors>
                        <c:if test="${errorOnThisPoint}">
    	                    <span class="error" style=" display: inline-block; float: right; margin-right: 10px; "><img src="img/web/asterisk.png" width="16" height="16" /></span>
                        </c:if>
                    </label>
        	           	 <form:input type="text" size="4" path="points[${count}]" tabindex="${tabindex1}" onkeyup=" assignPointsFalling(); return false; " />
                    &nbsp;

	                    <form:select path="metrics[${count}]" tabindex="${tabindex2}">
	                        <c:forEach var="u" items="${uom}" varStatus="uomStatus">
	                        <option value="${u.id}" <c:if test="${u.id == command.metrics[count]}"> selected="selected" </c:if>
	                     						  >${u.formattedSymbol}
						    </option>
	                       	</c:forEach>
	                    </form:select>
                    <c:choose>
                   	 	<c:when test="${count == 0}">
	                    	<a href="#" onclick=" cascadeUOM($j(this).closest('li'), true); return false; " style=" text-decoration: none; ">
		                        <img src="img/icons/arrow_down.png" width="10" height="16" alt="<spring:message code='calibrationpoints.imgarrowdown' />" title="<spring:message code='calibrationpoints.imgarrowdown' />" />
	                  	 	</a>
                   		</c:when>
                   		<c:otherwise>
    	                	<img src="img/icons/arrow_down.png" style=" visibility: hidden; " width="10" height="16" alt="" title="" />
                    	</c:otherwise>
                    </c:choose>

                    <span class="<c:choose><c:when test="${command.relational == true}"> vis </c:when>
                    						<c:otherwise> hid </c:otherwise>
                    			</c:choose>">&nbsp;@&nbsp;</span>

                    	<c:choose>
                    		<c:when test="${command.relational and not empty command.relativePoints[count]}">
                    			<c:set var="hasRelationalPoint" value="true" />
                    			<c:set var="classRelationalPoint" value="vis" />
                    		</c:when>
                    		<c:otherwise>
                    			<c:set var="hasRelationalPoint" value="false" />
                    			<c:set var="classRelationalPoint" value="hid" />
                    		</c:otherwise>
                    	</c:choose> 
	                    <input type="checkbox" <c:choose>
	                    							<c:when test="${not empty command.relativePoints[count]}"> class="vis" checked="checked" </c:when>
	                    						 	<c:when test="${command.relational}"> class="vis" </c:when>
	                    							<c:otherwise> class="hid" </c:otherwise>
	                   							 </c:choose> onclick=" clearRelationalPoint(this); " />
	                    
                    <span class="relationalPoint ${classRelationalPoint}">
		                    <form:input type="text" size="4" path="relativePoints[${count}]" tabindex="${tabindex3}" />
	                    &nbsp;

		                    <form:select path="relativeMetrics[${count}]" tabindex="${tabindex4}">
		                        <c:forEach var="u" items="${uom}" varStatus="calibStatus">
		                        <option value="${u.id}" <c:if test="${u.id == command.relativeMetrics[count]}"> selected="selected" </c:if>>${u.formattedSymbol}</option>
		                        </c:forEach>
		                    </form:select>
	                    <c:if test="${count == 0}">
		                    <a href="#" class="mainlink" onclick=" cascadeUOM($j(this).closest('li'), false); return false; ">
		                        <img src="img/icons/arrow_down.png" width="10" height="16" alt="<spring:message code='calibrationpoints.imgarrowdown' />" title="<spring:message code='calibrationpoints.imgarrowdown' />" />
		                    </a>
	                    </c:if>
                    </span>
                </li>
                <c:set var="tabindex1" value="${tabindex1 + 1}"/>
                <c:set var="tabindex2" value="${tabindex2 + 1}"/>
                <c:set var="tabindex3" value="${tabindex3 + 1}"/>
                <c:set var="tabindex4" value="${tabindex4 + 1}"/>
                </c:forEach>

                <li>
                    <input type="hidden" name="pointsize" value="${count}" id="pointsize"/>
                    <span>
							<a href="#" onclick=" addPoint(1); return false; "><img src="img/icons/add.png" width="16" height="16" class="img_marg_bot" alt="Add Point To List" title="Add Point To List" /></a>
								&nbsp;&nbsp;(<spring:message code="calibrationpoints.note1" />)
							</span>
                </li>
                <!-- #*
						<li id="savetemplate">
							#springFormHiddenInput("command.saveAsTemplate" "")
							Save as points template?
							<input type="checkbox" name="savet" value="$status.value" onclick=" toggleSaveTemplate(this); " #if($command.saveAsTemplate == true) checked="checked" #end/>
						</li>

						#if($command.saveAsTemplate == true)
							<li>
								<label>Template Name:</label>
								#springBind("command.template.title")
								<input name="$status.expression" id="$status.expression" value="$!status.value" type="text"/>
								<span class="attention">${status.errorMessage}</span>
							</li>

							<li>
								<label>Template Description:</label>
								#springBind("command.template.description")
								<textarea name="$status.expression" id="$status.expression">$!status.value</textarea>
								<span class="attention">${status.errorMessage}</span>
							</li>
						#end
						*# -->

            </ol>

        </fieldset>

    </div>

    <div id="range-tab" <c:if test="${command.action != 'range'}"> class="hid" </c:if>>

        <fieldset>

            <ol>

                <li>
                    <label>
                        <spring:message code="calibrationpoints.from" />

                        <c:set var="errorOnRange" value="${false}"/>
                        <form:errors path="rangeFrom">
                        	<c:set var="errorOnRange" value="${true}"/>	
                        </form:errors>
						<form:errors path="rangeTo">
                        	<c:set var="errorOnRange" value="${true}"/>	
                        </form:errors>
						<form:errors path="rangeRelatedPoint">
                        	<c:set var="errorOnRange" value="${true}"/>	
                        </form:errors>
						
                         <c:if test="${errorOnRange}">
                        <span class="error" style=" display: inline-block; float: right; margin-right: 10px; "><img src="img/web/asterisk.png" width="16" height="16" /></span>
                       	</c:if>
                    </label>

	                    <form:input type="text" size="4" path="rangeFrom" />
                    		&nbsp; <spring:message code="calibrationpoints.to" />&nbsp;
    	                <form:input type="text" size="4" path="rangeTo" />

	                    <form:select path="rangeUom">
	                        <c:forEach var="u" items="${uom}">
	        	                <option value="${u.id}" <c:if test="${u.id == command.rangeUom}"> selected="selected" </c:if>>${u.formattedSymbol}</option>
	                        </c:forEach>
	                    </form:select>
	                    <form:checkbox path="relationalRange" value="true" onclick=" toggleRelationalRange(this); " id="currentlyRelationalRange" />&nbsp;<spring:message code="calibrationpoints.relational" />

                    		<span class="relationalRange <c:choose>
                    									<c:when test="${command.relationalRange}"> vis </c:when>
                    									<c:otherwise> hid </c:otherwise>
                  								 </c:choose>">
								&nbsp;@&nbsp;
									<form:input type="text" size="4" path="rangeRelatedPoint" />
								<form:select path="rangeRelatedUom">
									<c:forEach var="u" items="${uom}">
										<option value="${u.id}" <c:if test="${u.id == command.rangeRelatedUom}"> selected="selected" </c:if>>${u.formattedSymbol}</option>
									</c:forEach>
								</form:select>
					</span>
                </li>

            </ol>

        </fieldset>

    </div>

    <fieldset>

        <ol>

            <li>
                <label class="ajaxlabel" style=" width: 140px; "><spring:message code="calibrationpoints.string2"/>:<br />(<spring:message code="calibrationpoints.string3"/>)</label>
					<form:textarea id="privatecalreqinstructions" path="cr.privateInstructions" rows="5" class="width80 symbolBox presetComments CALIBRATION_REQUIREMENT" value="${command.cr.privateInstructions}" />
      		 </li>
            <li>
                <label class="ajaxlabel" style=" width: 140px; "><spring:message code="calibrationpoints.string4"/>:<br />(<spring:message code="calibrationpoints.string5"/>)</label>
					<form:textarea id="publiccalreqinstructions" path="cr.publicInstructions" rows="5" class="width80 symbolBox presetComments CALIBRATION_REQUIREMENT" value="${command.cr.publicInstructions}" />
            </li>

        </ol>

    </fieldset>

    <div class="text-center marg-bot">
       		<form:hidden id="action" path="action" />
       		<input type="submit" value="<spring:message code='save'/>" name="Submit" />

    </div>

</form:form>

</body>

</html>