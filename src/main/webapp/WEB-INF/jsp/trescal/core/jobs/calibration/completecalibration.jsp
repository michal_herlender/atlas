<%-- File name: /trescal/core/jobs/calibration/completecalibration.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<jsp:useBean id="now" class="java.util.Date" />

<jobs:jobItemTemplate>
	<jsp:attribute name="subScript">
		<script src='script/trescal/core/jobs/calibration/CompleteCalibration.js'></script>
	</jsp:attribute>
	<jsp:body>
		<!-- Page specific code -->
		<!-- infobox is styled with nifty corners -->
		<div class="infobox">
			<c:if test="${form.openProc}">
				<script>window.open('runproc.htm?runfile=${cal.xindiceKey}&calid=${cal.id}');</script>
			</c:if>
			<c:if test="${displayUncertainties}">
				<div class="warningBox1">
					<div class="warningBox2">
						<div class="warningBox3 center-0"><spring:message code="completecalibration.note1"/></div>
					</div>
				</div>
			</c:if>
			<form:form modelAttribute="form" id="completecalibrationform" method="post" action="" enctype="multipart/form-data" onsubmit="$j(this).find('input:submit').attr('disabled', 'disabled'); return true;">
				<fieldset id="completecalibration">
					<!-- calls html file that refreshes the iframe every 15 minutes, preventing the session from expiring on this page -->
					<iframe src="html/refresh.html" class="hid"></iframe>
					<legend><spring:message code="completecalibration.complcal"/></legend>
					<ol>
						<li>
							<label><spring:message code="completecalibration.calfor"/>:</label>
							<div class="float-left">
								<c:forEach var="cl" items="${cal.links}">
									Item ${cl.ji.itemNo} on Job ${cl.ji.job.jobno} :
									<links:showInstrumentLink instrument="${cl.ji.inst}"/><br/>
								</c:forEach>
							</div>
							<div class="clear"></div>
						</li>
						<li>
							<label><spring:message code="calsearch.startby"/>:</label>
							<span>
								${cal.startedBy.name} at <fmt:formatDate value="${cal.startTime}" dateStyle="medium" type="both" timeStyle="short"/>
							</span>
						</li>
						<li>
							<label><spring:message code="completecalibration.procselect"/>:</label>
							<span>
								${cal.capability.reference} - ${cal.capability.name}
							</span>
						</li>
						<li>
							<label><spring:message code="completecalibration.calprocselect"/>:</label>
							<span>
								${cal.calProcess.process}
							</span>
						</li>
						<li>
							<label><spring:message code="completecalibration.caltypeselect"/>:</label>
							<span>
								<t:showTranslationOrDefault translations="${cal.calType.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/>
							</span>
						</li>
						<li>
							<label><spring:message code="completecalibration.standuse"/>:</label>
							<span class="calStandardsSize${cal.id}">${cal.standardsUsed.size()}</span>
							<!-- hover over this anchor and a tooltip is displayed showing the contents of the div created below -->
							<a href="?id=#calStandards${cal.id}&amp;width=450" onclick="return false;" title="Standards Used" id="tip" class="jTip"><img src="img/icons/standard.png" width="16" height="16" class="image_inl" alt="" /></a>
							<!-- provide clipboard icon to copy tabbed list of standards -->
							<img class="marg-left" src="img/icons/bullet_clipboard.png" width="10" height="10" onclick=" copyListOfStandardsToClipboard(this, ${cal.id}); return false; " alt="<spring:message code='completecalibration.bulletclipboard'/>" title="<spring:message code='completecalibration.bulletclipboard'/>"/>
							<!-- contents of this div are displayed in tooltip created above -->
							<div id="calStandards${cal.id}" class="hid">
								<table class="default2" summary="<spring:message code='completecalibration.tablecalstand'/>">
									<thead>
										<tr>
											<th colspan="2">
												<div class="float-left">
													<spring:message code="completecalibration.standuse"/> (<span class="calStandardsSize${cal.id}">${cal.standardsUsed.size()}</span>)
												</div>
												<c:forEach var="cl" items="${cal.links}">
													<c:if test="${cl.ji.jobItemId == form.ji.jobItemId}">
														<div class="float-right">
															<cwms:securedJsLink classAttr="mainlink" onClick="loadAddRemoveStandards(${cal.id}); return false;" permission="ROLE_INTERNAL">
																		<spring:message code="completecalibration.addrem"/>
															</cwms:securedJsLink>
														</div>
													</c:if>
												</c:forEach>
											</th>
										</tr>
									</thead>
									<tbody>
										<c:choose>
											<c:when test="${cal.standardsUsed.size() == 0}">
												<tr>
													<td colspan="2" class="center bold"><spring:message code="completecalibration.nostandforcal"/></td>
												</tr>
											</c:when>
											<c:otherwise>
												<c:forEach var="su" items="${cal.standardsUsed}">
													<tr>
														<td>
															<links:showInstrumentLink instrument="${su.inst}"/>
														</td>
														<td>
															<cwms:showmodel instrumentmodel="${su.inst.model}"/>
															<spring:message code="serialno"/>: ${su.inst.serialno}<br/>
															<spring:message code="plantno"/>: ${su.inst.plantno}
														</td>
													</tr>
												</c:forEach>
											</c:otherwise>
										</c:choose>
									</tbody>
								</table>
							</div>			
						</li>
						<li>
							<label><spring:message code="calsearch.headtext"/>:</label>
							<span>
								<t:showTranslationOrDefault translations="${cal.status.nametranslations}" defaultLocale="${defaultlocale}"/>
							</span>
						</li>
						<c:choose>
							<c:when test="${form.accredited}">
								<li>
									<form:label path="calDate"><spring:message code="newbatchcalibration.caldate"/>:</form:label>
									<form:input path="calDate" type="date" max="${cwms:isoDate(now)}"/> 
									<span class="attention"><form:errors path="calDate"/></span>
								</li>
								<li>
									<form:label path="calClass"><spring:message code="completecalibration.lblfoundleft"/>:</form:label>
									<span>
										<form:select path="calClass" items="${classes}" itemLabel="desc"/>
										<form:errors path="calClass"/>
									</span>
								</li>
								<li>
									<form:label path="duration"><spring:message code="completecalibration.calinterval"/>:</form:label>
									<form:input path="duration" id="durationCWMS"/>
									<form:select path="intervalUnitId" items="${units}" itemLabel="value" itemValue="key"/>
									<%--<select id="intervalUnitId">
										<c:forEach var="unit" items="${units}">
											<option value="${unit.key}">${unit.value}</option>
										</c:forEach>
									</select>--%>
									<form:errors path="duration"/>
								</li>
								<li>
									<form:label path="remark"><spring:message code="completecalibration.remarks"/>:</form:label>
									<form:textarea path="remark" rows="6" class="width60 presetComments COMPLETE_CALIBRATION"/>
									<form:errors path="remark"/>
								</li>
								<li>
									<form:label path="includeRemarksOnCert"><spring:message code="completecalibration.includeremarks"/></form:label>
									<form:checkbox path="includeRemarksOnCert"/>
								</li>
								<c:set var="showIncludeOnCertBoxes" value="${form.linkedOutcomes.size() > 1 && form.certAllowed}"/>
								<li style=" border-bottom: 0; ">
									<label>&nbsp;</label>
									<div class="float-left padtop" style=" width: 400px; ">&nbsp;<b><spring:message code="completecalibration.calout"/></b></div>
									<div class="float-left padtop" style=" width: 140px; text-align: center; "><b><spring:message code="completecalibration.timetake"/></b></div>
									<c:if test="${showIncludeOnCertBoxes}">
										<div class="float-left padtop" style=" width: 100px; text-align: center; "><b><spring:message code="completecalibration.inclconcert"/></b></div>
									</c:if>
									<div class="clear"></div>
								</li>
								<c:forEach var="key" items="${form.linkedOutcomes.keySet()}">
									<li>
										<label>
											<c:forEach var="calLink" items="${cal.links}">
												<c:if test="${calLink.id == key}">
													<spring:message code="item"/> ${calLink.ji.itemNo}:
												</c:if>
											</c:forEach>
										</label>
										<div class="float-left" style="width: 400px;">									
											<select name="linkedOutcomes[${key}]" onchange="changeOutcome(this); return false;">
												
<%-- 												<c:forEach var="ao" items="${actionoutcomes}"> --%>
<%-- 														<c:if test="${ao.active}"> --%>
<%-- 															<option value="${ao.id}" ${ao.defaultOutcome ? "selected" : ""}> --%>
<%-- 																<t:showTranslationOrDefault translations="${ao.translations}" defaultLocale="${defaultlocale}"/> --%>
<!-- 															</option> -->
<%-- 														</c:if> --%>
<%-- 												</c:forEach> --%>
											
												<optgroup label="<spring:message code='completecalibration.success'/>" class="Success">
													<c:forEach var="ao" items="${actionoutcomes}">
														<c:if test="${ao.active && ao.positiveOutcome}">
															<option value="${ao.id}" ${ao.defaultOutcome ? "selected" : ""}><t:showTranslationOrDefault translations="${ao.translations}" defaultLocale="${defaultlocale}"/></option>
														</c:if>
													</c:forEach>
												</optgroup>
												<optgroup label="<spring:message code='completecalibration.failure'/>" class="Failure">
													<c:forEach var="ao" items="${actionoutcomes}">
														<c:if test="${ao.active && !ao.positiveOutcome && !ao.description.contains('on hold')}">
															<option value="${ao.id}" ${ao.defaultOutcome ? "selected" : ""}><t:showTranslationOrDefault translations="${ao.translations}" defaultLocale="${defaultlocale}"/></option>
														</c:if>
													</c:forEach>
												</optgroup>
												<optgroup label="<spring:message code='completecalibration.incomplete'/>" class="Incomplete">
													<c:forEach var="ao" items="${actionoutcomes}">
														<c:if test="${ao.active && !ao.positiveOutcome && ao.description.contains('on hold')}">
															<option value="${ao.id}" ${ao.defaultOutcome ? "selected" : ""}><t:showTranslationOrDefault translations="${ao.translations}" defaultLocale="${defaultlocale}"/></option>
														</c:if>
													</c:forEach>
												</optgroup>



											</select>
										</div>
										<div class="float-left" style=" width: 140px; text-align: center; ">
											<form:input path="timeSpents[${key}]" size="3"/>
											<a href="#" onclick="getMinutesSinceStart('timeSpents-${key}', ${cal.id}); return false;">
												<img src="img/icons/refresh.png" width="16" height="16" alt="<spring:message code='completecalibration.refmin'/>" title="<spring:message code='completecalibration.refmin'/>"/>
											</a>
											<form:errors path="timeSpents[${key}]"/>
										</div>
										<c:if test="${showIncludeOnCertBoxes}">
											<div class="float-left" style=" width: 100px; text-align: center; ">
												&nbsp;&nbsp;&nbsp;
												<input type="checkbox" name="includeOnCert" value="${key}" <c:if test="${form.includeOnCert.contains(key)}">checked="checked"</c:if>/>
											</div>
										</c:if>
										<div class="clear"></div>													
									</li>
								</c:forEach>
								<!-- if include on cert checkboxes are shown, this should also be here as a fallback for if no boxes are checked -->
								<c:if test="${showIncludeOnCertBoxes}">
									<input type="hidden" name="_includeOnCert" value="0" />
								</c:if>
								<c:if test="${form.certAllowed}">
								<li id="signing1">
									<form:label path="issueNow"><spring:message code="completecalibration.issnewcertnum"/>:</form:label>
									<form:checkbox path="issueNow" id="issueNewCertCheckbox" onclick="if($j(this).is(':checked')){$j('input:checkbox#existingCertCheckbox').removeAttr('checked');}"/>
									<form:errors path="issueNow"/>
									<c:if test="${form.ji.certLinks.size() > 0}">
										<!-- hover over this anchor and a tooltip is displayed showing the contents of the div created below -->
										&nbsp;
										<a href="?id=#previousCerts&amp;width=440" onclick="return false;" title="Previous certificates for Item ${form.ji.itemNo} on Job ${form.ji.job.jobno}" id="tip1" class="jTip mainlink">
											<img src="img/icons/help.png" width="16" height="16" alt="" />
										</a>
										&nbsp;
										<spring:message code="completecalibration.note"/>: ${form.ji.certLinks.size()}
										<spring:message code="completecalibration.certnum"/>
										<c:if test="${form.ji.certLinks.size() > 1}">s</c:if> <spring:message code="completecalibration.alreadyiss"/>!
										<!-- contents of this div are displayed in tooltip created above -->
										<div id="previousCerts" class="hid">
											<div class="padding">
												<c:forEach var="cl" items="${form.ji.certLinks}">
													${cl.cert.certno} initially issued for ${cl.cert.cal.calClass.desc} calibration on <fmt:formatDate value="${cl.cert.certDate}" type="date" dateStyle="SHORT"/> by ${cl.cert.registeredBy.shortenedName}<br/>
												</c:forEach>
											</div>
										</div>
									</c:if>
								</li>
								</c:if>
								<c:if test="${certsForGroupItems.size() > 0 || form.ji.certLinks.size() > 0 && !cal.calProcess.applicationComponent}">
									<li id="signing3">
										<form:label path="addToExistingCert">
											<b>OR&nbsp;&nbsp;</b>
											<c:choose>
												<c:when test="${certsForGroupItems.size() > 0}">
													<spring:message code="completecalibration.addexistgroup"/> ${form.ji.group.id} Cert:
												</c:when>
												<c:otherwise>
													<spring:message code="completecalibration.addexistcert"/>:
												</c:otherwise>
											</c:choose>
										</form:label>
										<form:checkbox id="existingCertCheckbox" path="addToExistingCert" onclick="if($j(this).is(':checked')){ $j('input:checkbox#issueNewCertCheckbox').removeAttr('checked'); }"/>
										<c:choose>
											<c:when test="${certsForGroupItems.size() > 0}">
												<form:select path="existingCertId">
													<c:forEach var="cert" items="${certsForGroupItems}">
														<form:option value="${cert.certid}">
															${cert.certno}&nbsp;&nbsp;&nbsp;&nbsp;
															(${cert.registeredBy.name} - <fmt:formatDate value="${cl.cert.certDate}" type="date" dateStyle="SHORT"/> -
															${cl.cert.status.message}")
														</form:option>
													</c:forEach>
												</form:select>
											</c:when>
											<c:otherwise>
												<form:select path="existingCertId">
													<c:forEach var="cl" items="${form.ji.certLinks}">
														<form:option value="${cl.cert.certid}">
															${cl.cert.certno}&nbsp;&nbsp;&nbsp;&nbsp;
															(${cl.cert.registeredBy.name} - <fmt:formatDate value="${cl.cert.certDate}" type="date" dateStyle="SHORT"/> -
															${cl.cert.status.message})
														</form:option>
													</c:forEach>
												</form:select>
											</c:otherwise>
										</c:choose>
									</li>
								</c:if>
								<c:if test="${cal.calProcess.signOnIssue}">
									<!-- TEMPORARILY DISABLED SIGNING ON THIS PAGE TO AVOID CONFUSION AMONG NEW USERS
									  -- TO ENABLE AGAIN, SIMPLY REMOVE CLASS="HID" FROM LINE BELOW AND CHANGE ID BACK
									  -- TO "signing2" -->
									<li id="hiddenfield" class="hid">
										<form:label path="signNow"><spring:message code="completecalibration.signcert"/>:</form:label>
										<c:choose>
											<c:when test="${form.accreditedToSign}">
												<form:checkbox path="signNow" onclick="changeSignNow(this);" disabled="disabled"/>
											</c:when>
											<c:otherwise>
												<form:checkbox path="signNow" onclick="changeSignNow(this);"/>
											</c:otherwise>
										</c:choose>
									</li>
									<c:if test="${cal.calProcess.uploadBeforeIssue}">
										<li id="password6" <c:if test="${!form.signNow}">class="hid"</c:if>>
											<form:label path="certFilePath"><spring:message code="completecalibration.selcertdoc"/>:</form:label>
											<form:select path="certFilePath" onchange="$j('li#file input[type=file]').val(''); return false;">
												<form:option value=""> -- <spring:message code="completecalibration.certdoc"/> -- </form:option>
												<form:options items="${jobfiles}" itemValue="path" itemLabel="name"/>
											</form:select>
											&nbsp;<spring:message code="completecalibration.uploaddoc"/>&nbsp;
											<form:input path="file" type="file" onclick="$j('li#password6 select').val(0);"/>
											<form:errors path="file"/>									 
										</li>
									</c:if>
								</c:if>
							</c:when>
							<c:otherwise>
								<li>
									<label><spring:message code="completecalibration.lblfoundleft"/>:</label>
									<span>${cal.calClass.desc}</span>
								</li>
							</c:otherwise>
						</c:choose>
						<c:if test="${form.passwordReq}">
							<c:choose>
								<c:when test="${form.pwExists}">
									<li id="password5" <c:if test="${!form.signNow}">class="hid"</c:if>>
										<form:label path="encPassword"><spring:message code="completecalibration.passw"/>:</form:label>
										<form:password path="encPassword" onkeypress="if(event.keyCode==13){event.preventDefault();}else{$j('input#generatecertbutton').attr('disabled','');}"/>
										<form:errors path="encPassword"/>
									</li>
								</c:when>
								<c:otherwise>
									<li id="password1" <c:if test="${!form.signNow}">class="hid"</c:if>>
										<label class="labelhead">
											<span class="attention">${currentContact.name}, <spring:message code="completecalibration.nopasswset"/>:</span>
										</label>
										<span>&nbsp;</span>
									</li>
									<li id="password2" <c:if test="${!form.signNow}">class="hid"</c:if>>
										<form:label path="encPassword"><spring:message code="completecalibration.passw"/>:</form:label>
										<form:password path="encPassword" onkeyup="if(event.keyCode==13){event.preventDefault();}else{passwordStrength(this.value);}"/>
										<form:errors path="encPassword"/>
									</li>
									<li id="password3" <c:if test="${!form.signNow}">class="hid"</c:if>>
										<form:label path="confirmPassword"><spring:message code="completecalibration.confpassw"/>:</form:label>
										<form:password path="confirmPassword" onkeypress="if(event.keyCode==13){ event.preventDefault();}"/>
										<form:errors path="confirmPassword"/>
									</li>
									<li id="password4" <c:if test="${!form.signNow}">class="hid"</c:if>>
										<label><spring:message code="completecalibration.strength"/>:</label>
										<div id="passwordDescription"><spring:message code="completecalibration.passwnotent"/></div>
			                      			<div id="passwordStrength" class="strength0"></div>
										<!-- clear floats and restore page flow -->
										<div class="clear"></div>
									</li>
								</c:otherwise>
							</c:choose>
						</c:if>
						<c:choose>
							<c:when test="${form.accredited}">
								<c:if test="${cal.calProcess.applicationComponent}">
									<li>
										<form:label path="template"><spring:message code="completecalibration.certtemp"/>:</form:label>
										<form:select path="template" items="${certtemplates}"/>
										<a href="#" class="mainlink previewCertificate" onclick="createCertificatePreview(this, null, ${cal.id}, $j(this).prev().val()); return false;">Certificate Preview</a>
									</li>
									<li>
										<form:label path="certPreviewed"><spring:message code="completecalibration.certprev"/>:</form:label>
										<form:checkbox path="certPreviewed" class="widthAuto"/>
										<form:errors path="certPreviewed"/>
									</li>
								</c:if>
								<li>
									<label>&nbsp;</label><br/>
									<span>
										<input type="submit" onclick="$j('#submitAction').val($j(this).attr('id'));" name="sAction" value="<spring:message code='completecalibration.lblcomplcal'/>" id="completeButton" />
										<c:if test="${not empty cal.calProcess.toolContinueLink}">
											<input type="submit" onclick="event.preventDefault(); continueWithCalTool(${form.ji.jobItemId},'${cal.calProcess.id}');" value="<spring:message code='completecalibration.lblcontinuecal'/>" />
										</c:if>
										<input type="submit" onclick="$j('#submitAction').val($j(this).attr('id'));" name="sAction" value="<spring:message code='completecalibration.cancelcal'/>" id="cancelButton" />
										<form:hidden path="submitAction" id="submitAction"/>
									</span>
								</li>
							</c:when>
							<c:otherwise>
								<li>
									<label>&nbsp;</label>
									<span><spring:message code="completecalibration.noaccr"/></span>
								</li>
							</c:otherwise>
						</c:choose>
					</ol>
				</fieldset>
			</form:form>
		</div>
		<!-- end of infobox div -->
		<!-- End of page specific code -->
	</jsp:body>
</jobs:jobItemTemplate>