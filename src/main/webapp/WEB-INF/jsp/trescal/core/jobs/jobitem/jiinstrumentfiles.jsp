<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>

<jobs:jobItemTemplate>
	<jsp:attribute name="subScript">
        <script type='text/javascript' src='script/trescal/core/jobs/jobitem/JIInstrumentFiles.js'></script>
    </jsp:attribute>
	<jsp:body>
		<spring:message code="instrument.filesfor" var="filesilesForTranslated"/>
		<files:showFilesForSC entity="${instrument}" sc="${systemcomponent}" id="${instrument.plantid}" identifier="${instrument.plantid}" ver="" rootFiles="${rootfiles}" allowEmail="false" isEmailPlugin="false" rootTitle="${filesilesForTranslated} ${instrument.plantid}" deleteFiles="false" />
	</jsp:body>
</jobs:jobItemTemplate>
	