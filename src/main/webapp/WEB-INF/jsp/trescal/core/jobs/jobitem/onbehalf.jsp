<%-- File name: /trescal/core/jobs/jobitem/onbehalf.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="onbehalf.headtext" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/jobs/jobitem/OnBehalf.js'></script>
    </jsp:attribute>
    <jsp:body>
    	<t:showErrors path="command.*" showFieldErrors="true" />
		<div class="infobox">	
			<form:form modelAttribute="command" action="" id="onbehalfform" method="post" onsubmit="$j(this).find(':input[type=submit]').prop('disabled', true);">
				<fieldset>
				    <legend>
					    <span> <spring:message code="onbehalf.legend"/> </span>
					    <links:jobnoLinkDWRInfo rowcount="0" jobno="${command.job.jobno}" jobid="${command.job.jobid}" copy="true"/>
				    </legend>
					<ol>
						<li>
							<label>
								<spring:message code="company"/>:<br />								
								<a href="#" class="mainlink" onclick=" CascadeSearchPlugin.toggleAddressSearch($j(this)); return false; ">
							    <c:choose>
									<c:when test="${not empty command.srcJobitem.onBehalf}">
										<c:choose>
			                            	<c:when test="${not empty command.srcJobitem.onBehalf.address}">
												<spring:message code="onbehalf.remaddress"/>
											</c:when>
			      					    	<c:otherwise>
												<spring:message code="onbehalf.seladdress"/>
											</c:otherwise>
										</c:choose>
									</c:when>
		      						<c:otherwise>
										<spring:message code="onbehalf.remaddress"/>
							   		</c:otherwise>
								</c:choose>
								</a>
							</label>
							<div id="cascadeSearchPlugin">
								<input type="hidden" id="compCoroles" value="client,business" />
								  <c:choose>
			                         <c:when test="${not empty command.srcJobitem.onBehalf}">
								       <c:choose>
			                              <c:when test="${not empty command.srcJobitem.onBehalf.address}"> 
										     <input type="hidden" id="cascadeRules" value="subdiv,address" />
								          </c:when>
			      					      <c:otherwise>
										    <input type="hidden" id="cascadeRules" value="" />
									      </c:otherwise>
			                           </c:choose>
								     </c:when>
			      					 <c:otherwise>
									   <input type="hidden" id="cascadeRules" value="subdiv,address" />
								     </c:otherwise>
			                      </c:choose>
	                          
								<c:if test="${not empty command.srcJobitem.onBehalf}">
									<c:set var="ob" value="${command.srcJobitem.onBehalf}"/>
									<c:choose>
			                          <c:when test="${not empty command.srcJobitem.onBehalf.address}">
										<input type="hidden" id="prefillIds" value="${ob.company.coid},${ob.address.sub.subdivid},${ob.address.addrid}"/>
									  </c:when>
			   					      <c:otherwise>
										<input type="hidden" id="prefillIds" value="${ob.company.coid}"/>
									  </c:otherwise>
			                        </c:choose>
							    </c:if>
							</div>
							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
						</li>
						<li>
							<label><spring:message code="jobitems"/>:</label>
							<div class="float-left padtop" id="items">											
								<div class="marg-bot">													
									<input type="checkbox" onclick=" selectAllItems(this.checked, 'items'); "/>
									<spring:message code="all"/>
							    </div>
								<c:forEach var="jobitem" items="${command.job.items}">
								 <div id="item${jobitem.jobItemId}" style=" height: 26px; ">												
									<form:checkbox path="jobitemids[${jobitem.jobItemId}]" />
									<links:jobitemLinkDWRInfo jobitem="${jobitem}" jicount="false" rowcount="0" />
									<cwms:showmodel instrumentmodel="${jobitem.inst.model}"/>
									 <c:if test="${not empty jobitem.onBehalf}">
			    					  <span id="behalfinfo${jobitem.jobItemId}">
			    				    - <links:companyLinkDWRInfo company="${jobitem.onBehalf.company}" rowcount="0" copy="true"/> [${jobitem.onBehalf.setBy.name}]
								      <a href="#" onclick="removeItemOnBehalf (${jobitem.jobItemId}, ${jobitem.onBehalf.id}); return false;">
								      <img src="img/icons/delete.png" width="16" height="16" class="img_inl_margleft" alt=<spring:message code="onbehalf.remonbehalfcomp"/> title ="<spring:message code="onbehalf.remonbehalfcomp"/>"/>
								      </a>
								      </span>
								     </c:if>
								 </div>
								</c:forEach>
							</div>
							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
						</li>
						<li>
							<label>&nbsp;</label>
							<input type="submit" name="save" value="save"/>
						</li>									
					</ol>
				</fieldset>
			</form:form>
		</div>
    </jsp:body>
</t:crocodileTemplate>