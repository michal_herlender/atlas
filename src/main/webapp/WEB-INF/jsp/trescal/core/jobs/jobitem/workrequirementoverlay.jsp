<%-- File name: /trescal/core/jobs/jobitem/workrequirementoverlay.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="company" tagdir="/WEB-INF/tags/company" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:overlayTemplate bodywidth="780">
	<jsp:attribute name="scriptPart">
		<script>
			var procedureSubdivId = '${form.subdivId}';
			var modelId = '${form.modelId}';
			var type ='${form.type}';
		</script>
		<script src="script/trescal/core/jobs/jobitem/WorkRequirementOverlay.js"></script>
	</jsp:attribute>
	<jsp:body>
		<div id="newWorkRequirementOverlay">
			<t:showErrors path="form.*" showFieldErrors="true"></t:showErrors>
			<fieldset>
				<form:form modelAttribute="form" method="post" >
					<form:hidden path="action" />
					<form:hidden path="type"/>
					<ol>								
						<li>
							<label><spring:message code="businesssubdivision"/>:</label>
							<company:subdivSelect name="subdivId" availableSubdivs="${subdivsWithProceduresMap}" 
								selectedSubdivId="${form.subdivId}" onchange="subdivisionSelected(this.value);" style="width: 505px;"/>
							<span class="attention"><form:errors path="subdivId" /></span>
							<div class="clear-0"></div>
						</li>							
						<li>
							<label><spring:message code="servicetype"/>:</label>
							<form:select path="serviceTypeId" items="${serviceTypes}" itemLabel="value" itemValue="key" style="width: 505px;" />
							<span class="attention"><form:errors path="serviceTypeId" /></span>
							<div class="clear-0"></div>							
						</li>
						<li>
							<label><spring:message code="capability"/>:</label>
							<%-- JQueryUI Autocomplete field --%>
	                        <form:input path="procedureReferenceAndName" />
	                        <form:input path="procedureId" type="hidden" />                       
							<span class="attention"><form:errors path="procedureId" /></span>
							<div class="clear-0"></div>
						</li>
						<li>
							<label><spring:message code="workrequirement.searchallprocs"/>:</label>
							<input type="checkbox" id="searchAllProcs" onchange="searchAllProcedures();"/>
							<div class="clear-0"></div>
						</li>
						<li>
							<label><spring:message code="workrequirement.type"/>:</label>
							<div class="clear"></div>
						</li>
						<li>
							<label><spring:message code="tools.department" />:</label>
							<div class="clear"></div>
						</li>
						<li>
							<label><spring:message code="workrequirement.text" />:</label>
							<form:textarea path="requirementText" cols="80" rows="2" />
							<span class="attention"><form:errors path="requirementText" /></span>
							<div class="clear"></div>
						</li>
						<li>
							<label>&nbsp;</label>
							<c:choose>
								<c:when test="${action == 'add'}">
									<spring:message var="buttonLabel" code="workrequirement.add" />
								</c:when>
								<c:otherwise>
									<spring:message var="buttonLabel" code="workrequirement.save" />
								</c:otherwise>
							</c:choose>
							<input type="submit" id="jiwr_submit" value="${buttonLabel}" />
							&nbsp;
							<input type="button" id="jiwr_close" value="<spring:message code='cancel' />" onclick="event.preventDefault(); window.parent.loadScript.closeOverlay(); " />
						</li>
					</ol>
				</form:form>
			</fieldset>					
		</div>
	</jsp:body>
</t:overlayTemplate>