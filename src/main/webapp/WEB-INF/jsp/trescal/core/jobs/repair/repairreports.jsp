<%-- File name: /trescal/core/failurereport/viewfailurereport.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="costs" tagdir="/WEB-INF/tags/costs" %>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>
<%@ taglib prefix="purchaseorder" tagdir="/WEB-INF/tags/purchaseorder" %>

<style>
.pointer-events-none {
	color: grey;
	pointer-events: none;
	
}

.wrapper {
	cursor: not-allowed;

}
</style>
<t:crocodileTemplate>
	<jsp:attribute name="header">
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/jobs/jobitem/JobItemFunctions.js'></script>
        <script type='text/javascript' src='script/trescal/core/jobs/repair/RepairReports.js'></script>
		<script type='text/javascript'>
			var pageImports = new Array('script/thirdparty/jQuery/jquery.tablednd.js',
					'script/trescal/core/utilities/DocGenPopup.js',
					'script/thirdparty/jQuery/jquery.boxy.js');
			// grab scroll to parameter passed when using job item key navigation
			var scrollto = '${request.getParameter("scrollto")}';
		</script>
	</jsp:attribute>
<jsp:body>

<div class="infobox" >
	<fieldset>
		<legend>Repair Report</legend>
		<!-- displays content in column 49% wide (left) -->
		<div class="displaycolumn">
			<ol>
				<li>
					<label><spring:message code="job"/>:</label>
					<span><links:jobnoLinkDWRInfo rowcount="1" jobno="${ji.job.jobno}" jobid="${ji.job.jobid}" copy="true"/> </span>
					<span><links:jobitemLinkDWRInfo jobitem="${ ji }" jicount="true" rowcount="1"></links:jobitemLinkDWRInfo> </span>
				</li>
				<li>
					<label><spring:message code="clientcompany"/>:</label>
					<span><links:companyLinkDWRInfo copy="false" rowcount="1" company="${ ji.job.con.sub.comp }"/></span>
				</li>
				<c:if test="${ not empty rirForm }">
					<br/>
					<strong><span><spring:message code="repairinspection.headtext" />:</span></strong>
					<li>
						<label>N°:</label>
						<span>${ rirForm.rirNumber }</span>
					</li>
					<c:if test="${ rirForm.completedByTechnician }">
						<li>
							<label><spring:message code='repairinspection.technicalvalidation' />:</label>
							<span>${ rirForm.technician } - <fmt:formatDate type="both" dateStyle="SHORT" timeStyle="SHORT" value="${ rirForm.completedByTechnicianOn }"/></span>
						</li>
					</c:if>
				</c:if>
				<c:if test="${ not empty rcrForm and rirForm.reviewedByCsr }">
					<br/>
					<strong><span><spring:message code="repaircompletionreport.headtext" />:</span></strong>
					<li>
						<label><spring:message code="instmod.lastmodified" />:</label>
						<span>${ rcrForm.lastModifiedBy } - <fmt:formatDate type="both" dateStyle="SHORT" timeStyle="SHORT" value="${ rcrForm.lastModified }"/></span>
					</li>
				</c:if>
			</ol>
		</div>
		
		<!-- displays content in column 49% wide (right) -->
		<div class="displaycolumn">
			<ol>
				<li>
					<label><spring:message code="businesscompany"/>:</label>
					<span><links:companyLinkDWRInfo company="${ businessSubdiv.comp }" rowcount="1" copy="false"/></span>
				</li>
				<li>
					<label><spring:message code="businesscompany"/>:</label>
					<span><links:subdivLinkDWRInfo rowcount="1" subdiv="${ businessSubdiv }"/></span>
				</li>
				<c:if test="${ not empty rirForm }">
					<br/><br/>
					<li>
						<label><spring:message code="instmod.lastmodified" />:</label>
						<span><fmt:formatDate type="both" dateStyle="SHORT" timeStyle="SHORT" value="${ rirForm.lastModified }"/></span>
					</li>
					<c:if test="${ rirForm.completedByTechnician }">
						<li>
							<label><spring:message code='repairinspection.managervalidation' />:</label>
							<span>${ rirForm.manager } - <fmt:formatDate type="both" dateStyle="SHORT" timeStyle="SHORT" value="${ rirForm.validatedByManagerOn }"/></span>
						</li>
					</c:if>
				</c:if>
				<c:if test="${ not empty rcrForm and not empty rcrForm.validated }">
					<br/><br/>
					<li>
						<label><spring:message code="failurereport.validated" /></label>
						<span>${ rcrForm.validatedBy } - <fmt:formatDate type="both" dateStyle="SHORT" timeStyle="SHORT" value="${ rcrForm.validatedOn }"/></span>
					</li>
				</c:if>
			</ol>
		</div>	
	</fieldset>
</div>
	
<div class="infobox default2" >

	<c:set var="showRcrTab"  value="${ not empty param['saveRcr'] or not empty param['showRcr']  }"  />
	
	<!-- div to hold all the tabbed submenu -->
	<div>
		<!-- tabbed submenu to change between contract review options -->
		<ul class="subnavtab">
			<li>
				<a href="" id="repairinspectionreport-link" ${ showRcrTab ? '' : 'class="selected"' }
					onclick="event.preventDefault(); switchMenuFocus(menuElements, 'repairinspectionreport-tab', false);">
					<spring:message code="repairinspection.headtext" />
				</a>
			</li>
			<li id="rcrTab" class="${ disableRcrTab ? 'wrapper':'' }">
				<a  id="repaircompletionreport-link" class="${ disableRcrTab ? 'pointer-events-none':'' } ${ !showRcrTab ? '' : 'selected' }" href="" 
					onclick="event.preventDefault(); switchMenuFocus(menuElements, 'repaircompletionreport-tab', false);" >
					<spring:message code="repaircompletionreport.headtext" />
				</a>
			</li>
		</ul>
		<!-- div to hold all content which is to be changed using the tabbed submenu -->
		<div class="tab-box">
			<!-- TODO: lazy loading... -->
			<div id="repairinspectionreport-tab" ${ showRcrTab ? 'class="hid"' : '' } >
				<c:import  url="/repairinspectionreport.htm"/>
			</div>
			<div id="repaircompletionreport-tab" ${ !showRcrTab ? 'class="hid"' : '' } >
		        <c:import url="/repaircompletionreport.htm"/>
			</div>
		</div>
	</div>

</div>

</jsp:body>
</t:crocodileTemplate>