<%-- File name: /trescal/core/jobs/job/deliveryItemAccessories.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<c:forEach var="deliveryItemAccessory" items="${deliveryItem.accessories}" varStatus="loopStatus"><cwms:besttranslation translations="${deliveryItemAccessory.accessory.translations}"/> x ${deliveryItemAccessory.qty}<c:if test="${!loopStatus.last}">, </c:if></c:forEach><c:if test="${deliveryItem.accessoryFreeText != null}"><c:if test="${deliveryItem.accessories.size() > 0}">, </c:if>${deliveryItem.accessoryFreeText}</c:if>