<%-- File name: /trescal/core/jobs/job/addjob.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel"%>
<%@ taglib prefix="costs" tagdir="/WEB-INF/tags/costs"%>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files"%>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<style>
	.rtradio {
		width: 20px !important;
	}
	
	.hidden {
	display: none;
	}

</style>



<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="addprebookingjob.headtext"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/jobs/job/AddJob.js'></script>
	</jsp:attribute>
	<jsp:body>
		<!-- error section displayed when form submission fails -->
		<form:errors path="pbjf.*">
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<h5 class="center-0 attention"><spring:message code="addjob.attention"/>:</h5>
						<c:forEach var="e" items="${messages}">
							<div class="center attention"><c:out value="${e}"/></div>
						</c:forEach>
					</div>
				</div>
			</div>
		</form:errors>
		<!-- end error section -->
		<!-- infobox div containing form elements for creating a new job -->
		<div class="infobox" id="createprebookingjob">
			
		<div class="clear"></div>
		<form:form action="" method="post" id="addprebookingjobform" modelAttribute="pbjf" enctype="multipart/form-data">
			<input type="hidden" name="defaultpo" id="defaultpo"/>
				<fieldset>
					<legend>
						<spring:message code="addprebookingjob.legend"/>
					</legend>
					<ol>
						<li>
							<label for="jobType"><spring:message code="addjob.jobtype"/>:</label>
							<form:select path="jobType" items="${jobTypes}" itemValue="name" itemLabel="description" />
						</li>
						<li>
							<label><spring:message code="addjob.contaddr"/>:</label>		
							<!-- this div creates a new cascading search plugin. The default search contains an input 
								 field and results box for companies, when results are returned this cascades down to
								 display the subdivisions within the selected company. This default behavoir can be obtained
								 by including the hidden input field with no value or removing the input field. You can also
								 add address and/or contact searches to the cascade by including the values 'contact' and/or 'address'
								 in the hidden inputs value attribute separated by a comma (e.g value="contact", value="address",
								 value="address,contact").  You can also swap these values around (e.g. value="contact,address").  
							 -->									
							<div id="cascadeSearchPlugin">
								<input type="hidden" id="cascadeRules" value="subdiv,contact,address" />
								<input type="hidden" id="compCoroles" value="client,business" />
								<input type="hidden" id="addressType" value="Delivery" />
								<input type="hidden" id="searchItems" value="true" />
								<input type="hidden" id="searchBPO" value="true" />
								<input type="hidden" id="searchLocations" value="true" />
								<input type="hidden" id="loadCurrency" value="true" />
								<input type="hidden" id="compindex" value="1" />
								<input type="hidden" id="entityType" value="prebooking" />
								<input type="hidden" id="addressCallBackFunction" value="getExchangesFormatForSelectedCompany" />
							</div>
							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
						</li>
						
						<li>
							<label for="recordtype"><spring:message code="addjob.recordtype.file"/> :</label>
							
							<table>
								<tr>
									<td>
										<label><spring:message code="addjob.recordtype.file.exchangeformat"/> :</label>
									</td>
									<td>
										<form:select path="exchangeFormat" type="text" id="exchangeformat" class="maxwidth-240"></form:select>
										<span id="ef_error_msg" class="error hidden"></span>
									</td>
									<td></td>
								</tr>
								<tr>
									<td>
										<label><spring:message code="addjob.recordtype.file.filetoupload"/> :</label>
									</td>
									<td colspan="2">
										<form:input path="uploadFile" type="file" id="uploadfile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
									</td>
								</tr>
							</table>
							
						</li>
						
						<li>
							<label for="clientref"><spring:message code="addjob.clientref"/>:</label>
							<form:input path="clientref" name="clientref" id="clientref" maxlength="30" tabindex="2"/>
						</li>
						<li>
							<label for="currencyCode"><spring:message code="addjob.currency"/>:</label>
							<form:select path="currencyCode">
								<c:forEach var="currency" items="${currencyList}">
									<form:option value="${currency.currencyCode}">
										${currency.currencyCode} ${currency.currencyERSymbol}
										[1:${currency.defaultRate}]
										<c:if test="${pbjf.currencyCode != null && currency.currencyCode == pbjf.currencyCode}">
											- default
										</c:if>
									</form:option>
								</c:forEach>
							</form:select>
							<form:errors path="currencyCode"/>
						</li>
						<li id="PO">
							<label><spring:message code="addjob.po"/>:</label>
							<span>
								<a href="#" class="mainlink" onclick="event.preventDefault(); purchaseOrdContent();" title="">
									<spring:message code="addjob.createpo"/>
								</a>
							</span>
						</li>
						<li id="Carriage In">
							<label><spring:message code="addjob.estcaroutcost"/>:</label>
							<form:input path="estCarriageOut" tabindex="4"/>
							<form:errors path="estCarriageOut"/>
						</li>
						<c:if test="${not empty businessAddresses}">
							<c:choose>
								<c:when test="${pbjf.jobType == 'SITE'}">
									<c:set var="visibilitySite" value="vis" />
									<c:set var="visibilityStandard" value="hid" />
								</c:when>
								<c:otherwise>
									<c:set var="visibilitySite" value="hid" />
									<c:set var="visibilityStandard" value="vis" />
								</c:otherwise>
							</c:choose>
							<c:set var="defLoc" value="0"/>
							<c:if test="${not empty pbjf.bookedInLocId}">
								<c:set var="defLoc" value="${pbjf.bookedInLocId}"/>
							</c:if>
							<c:set var="defAddr" value="${pbjf.bookedInAddrId}"/>
							<spring:message var="emptyText" code="addjob.nolocationsforaddress"/>
							<li>
								<label><spring:message code="addjob.bookinaddr"/>:</label>
								<span id="bookedInAddrStandard" class="${visibilityStandard}">
									<form:select path="bookedInAddrId" id="bookedInAddrId" tabindex="5" onchange="bookingInAddrChange(${useLocations}, this.value, ${defLoc}, '${emptyText}'); return true;">
										<c:forEach var="address" items="${businessAddresses}" varStatus="status">
											<%-- This defAddr is set here if there is none in the form, e.g. no default address for subdivision--%>
											<c:if test="${status.first && (empty defAddr)}">
												<c:set var="defAddr" value="${address.addrid}"/>
											</c:if>
											<form:option value="${address.addrid}" >
												${address.sub.comp.coname} - ${address.sub.subname} - ${address.addr1}
											</form:option>
										</c:forEach>
									</form:select>
								</span>
								<span id="bookedInAddrSite" class="${visibilitySite}">
									<spring:message code="addjob.sitejobaddress" /> 
								</span>
							</li>
							<c:if test="${useLocations}">
								<li id="bookingInLoc">
									<label><spring:message code="addjob.bookinloc"/>:</label>
									<span id="bookedInLocStandard" class="${visibilityStandard}">
										<form:select id="bookedInLocId" path="bookedInLocId" tabindex="6">
											<c:set var="locationCount" value="0" />
											<c:forEach var="location" items="${businessLocations}">
												<c:if test="${location.add.addrid == defAddr}">
													<form:option value="${location.locationid}" >
														${location.location}
													</form:option>
													<c:set var="locationCount" value="${locationCount + 1}" />
												</c:if>
											</c:forEach>
											<c:if test="${locationCount == 0}">
												<form:option value="0" >
													<spring:message code="addjob.nolocationsforaddress"/>
												</form:option>
											</c:if>
										</form:select>
									</span>
									<span id="bookedInLocSite" class="${visibilitySite}">
										<spring:message code="addjob.sitejoblocation" /> 
									</span>
								</li>
							</c:if>
						</c:if>
						
						<li>
							<label for="submit">&nbsp;</label>
							<input type="submit" id="submit" value="<spring:message code="addprebookingjob.headtext" />" tabindex="8" />
						</li>
					</ol>
				</fieldset>
			</form:form>
		</div>
		
	</jsp:body>
</t:crocodileTemplate>