<%-- File name: /trescal/core/jobs/certificate/certmanagement.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="certmanagement.headtext" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript'>
			var allowCertCreation = false;
			<c:if test="${allowCertCreation}">
				allowCertCreation = true;
			</c:if>
		</script>

		<script type='text/javascript' src='script/trescal/core/jobs/certificate/CertManagement.js'></script>
    </jsp:attribute>
    <jsp:body>
		<!-- infobox div with nifty corners applied -->											
			<div class="infobox">
				
				<div class="marg-bot">
					<!-- link to send user back to job -->
					<a class="mainlink" href="viewjob.htm?jobid=${form.job.jobid}">
						<spring:message code="certmanagement.backtojob" /><c:out value=" ${form.job.jobno}" />
					</a>
				</div>
				
				<!-- div to hold all the tabbed submenu -->
				<div id="tabmenu">

					<!-- tabbed submenu to change between active/inactive contacts/addresses -->
					<ul class="subnavtab">
						<li><a href="#" id="jicerts-link" onclick="switchMenuFocus(menuElements, 'jicerts-tab', false); return false; " class="selected"><spring:message code="certmanagement.jobitemcert" /></a></li>
						<c:if test="${allowCertCreation}">
							<li><a href="#" id="issuejicerts-link" onclick="switchMenuFocus(menuElements, 'issuejicerts-tab', false); placeDateField('jicertDate', 'groupcertDate'); <c:if test="${isAdmin}"> placeIssuerField('jiIssuer', 'groupIssuer'); </c:if> return false; "><spring:message code="certmanagement.issitemcert" /></a></li>
							<li><a href="#" id="issuegroupcerts-link" onclick="switchMenuFocus(menuElements, 'issuegroupcerts-tab', false); placeDateField('groupcertDate', 'jicertDate'); <c:if test="${isAdmin}"> placeIssuerField('groupIssuer', 'jiIssuer'); </c:if> return false; "><spring:message code="certmanagement.issgroupcert" /></a></li>
						</c:if>
					</ul>
					
					<!-- div to hold all content which is to be changed using the tabbed submenu -->
					<div class="tab-box">
						
						<!-- div displays all job items and the number of certificates allocated to each -->
						<div id="jicerts-tab">
							
							<table class="default4 jicerts" summary="<spring:message code='certmanagement.default4jicerts' />">
								<thead>
									<tr>
										<td colspan="5">
											<spring:message code="certmanagement.certforjob" /><c:out value=" ${form.job.jobno}"/>
										</td>
									</tr>
									<tr>
										<th class="itemno" scope="col"><spring:message code="certmanagement.itemno" /></th>
										<th class="desc" scope="col"><spring:message code="description" /></th>
										<th class="serial" scope="col"><spring:message code="serialno" /></th>
										<th class="numcerts" scope="col"><spring:message code="certmanagement.nocerts" /></th>
										<th class="certnums" scope="col"><spring:message code="certmanagement.certnum" /></th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<td colspan="5">&nbsp;</td>
									</tr>
								</tfoot>
								<tbody>
									<c:forEach var="ji" items="${form.job.items}">
										<tr>
											<td class="itemno">${ji.itemNo}</td>
											<td class="desc"><cwms:showmodel instrumentmodel="${ji.inst.model}"/></td>
											<td class="serial">${ji.inst.serialno}</td>
											<td class="numcerts">${ji.certLinks.size()}</td>
											<td class="certnums">
												<c:forEach var="cl" items="${ji.certLinks}" varStatus="clLoop">
													<c:choose>
														<c:when test="${clLoop.index > 1}">
															<br/> <a href="jicertificates.htm?jobitemid=${ji.jobItemId}" class="mainlink">${cl.cert.certno}</a>
														</c:when>
														<c:otherwise>
													 		<a href="jicertificates.htm?jobitemid=${ji.jobItemId}" class="mainlink">${cl.cert.certno}</a>
														</c:otherwise>	
													</c:choose>															
													<c:if test="${cl.cert.links.size() > 1}">
														(<spring:message code="certmanagement.group" />)
													</c:if>
													
													<div class="float-right">
														<c:choose>
															<c:when test="${cl.isHasCertFile()}">
																<img alt="<spring:message code='certmanagement.pdfexists' />" title="<spring:message code='certmanagement.pdfexists' />" height="16" width="16" src="img/doctypes/pdf-results.png" />
															</c:when>
															<c:otherwise>
																<img alt="<spring:message code='certmanagement.pdfnotfound' />" title="PDF not found!" height="16" width="16" src="img/doctypes/pdf-unavailable.png" />																
															</c:otherwise>
														</c:choose>
														
														<c:choose>
															<c:when test="${cl.cert.status eq 'UNSIGNED_AWAITING_SIGNATURE' 
																|| cl.cert.status eq 'SIGNED_AWAITING_APPROVAL' 
																|| cl.cert.status eq 'TO_BE_REPLACED'}">
																<img title="${cl.cert.status.getMessage()}" height="16" width="16" src="img/icons/info-red.png" />
															</c:when>
															<c:otherwise>
																<img title="${cl.cert.status.getMessage()}" height="16" width="16" src="img/icons/information.png" />																
															</c:otherwise>
														</c:choose>
														
														<c:choose>
															<c:when test="${cl.cert.type == 'CERT_INHOUSE'}">
																<input type="checkbox" name="labelCheckbox-${cl.cert.calType.serviceType.shortName}" value="printCalSpecificLabel(${cl.cert.certid}, $j('input[name=inclRecallDate]:checked').val());" />
															</c:when>
															<c:when test="${cl.cert.type == 'CERT_THIRDPARTY'}">
																<input type="checkbox" name="labelCheckbox-TP" value="printCertificateLabel('zeb.label_tp', ${cl.linkId}, $j('input[name=inclRecallDate]:checked').val());" />																		
															</c:when>
														</c:choose>
													</div>
													<div class="clear"></div>
												</c:forEach>
												
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>									
							
							<b><spring:message code="certmanagement.callbl" />:</b><br/><br/>
							
							<a class="mainlink" href="#" onclick=" $j('input:checkbox[name^=\'labelCheckbox\']').attr('checked', ''); return false; "><spring:message code="certmanagement.deselall" /></a><br/>
							<a class="mainlink" href="#" onclick=" $j('input:checkbox[name^=\'labelCheckbox\']').attr('checked', 'checked'); return false; "><spring:message code="certmanagement.selall" /></a><br/>
							<a class="mainlink" href="#" onclick=" $j('input:checkbox[name=\'labelCheckbox-UKAS\']').attr('checked', 'checked'); return false; "><spring:message code="certmanagement.selukas" /></a><br/>
							<a class="mainlink" href="#" onclick=" $j('input:checkbox[name=\'labelCheckbox-STD\']').attr('checked', 'checked'); return false; "><spring:message code="certmanagement.selstd" /></a><br/>
							<a class="mainlink" href="#" onclick=" $j('input:checkbox[name=\'labelCheckbox-TP\']').attr('checked', 'checked'); return false; "><spring:message code="certmanagement.selthipar" /></a><br/><br/>
							
							<input type="radio" id="opt1" name="inclRecallDate" value="<spring:message code='certmanagement.exclude' />" checked="checked" />&nbsp;<label style=" width: auto; display: inline; float: none; " for="opt1"><spring:message code="certmanagement.inputopt1"/></label><br/>
							<input type="radio" id="opt2" name="inclRecallDate" value="<spring:message code='certmanagement.include'/>" />&nbsp;<label style=" width: auto; display: inline; float: none; " for="opt2"><spring:message code="certmanagement.inputopt2"/></label><br/>
							<input type="radio" id="opt3" name="inclRecallDate" value="PREDICT" />&nbsp;<label style=" width: auto; display: inline; float: none; " for="opt3"><spring:message code="certmanagement.inputopt3"/></label><br/><br/>
							
							<a class="mainlink" href="#" onclick=" printAllSelectedLabels(); return false; "><spring:message code="certmanagement.printsel"/></a>
							
						</div>
						<!-- end of job item certs div -->
						
						<c:if test="${allowCertCreation}">
				
							<form:form action="" method="post" modelAttribute="form">
																		
								<!-- div displays all job items and allows certs to be issued for one or more items (site jobs only) -->
								<div id="issuejicerts-tab" class="hid">
							
									<table class="default4 issuejicerts" summary="<spring:message code='certmanagement.def4issjicerts'/>">
										<thead>
											<tr>
												<td <c:choose><c:when test="${isAdmin}"> colspan="3" </c:when><c:otherwise> colspan="6" </c:otherwise></c:choose>>
													<spring:message code="certmanagement.isssitecertswithcaldate"/>:																
													<form:input type="date" path="calDate" id="jicertDate"  />
													<form:errors path="calDate" class="attention" />																
												</td>
												
												<c:if test="${isAdmin}">
													<td colspan="3">
														<spring:message code="certmanagement.issby"/>:
														<form:select path="issuerId" id="jiIssuer">
															<c:forEach var="con" items="${businessContacts}">
																<option value="${con.key}" <c:if test="${not empty form.issuerId}"></c:if><c:if test="${form.issuerId == con.key}"> selected="selected" </c:if>>${con.value}</option>
															</c:forEach>
														</form:select>
														<form:errors path="issuerId" class="attention" />
													</td>
												</c:if>
											</tr>
											<tr>
												<th class="all" scope="col">
													<spring:message code="certmanagement.all"/> <input type="checkbox" onclick=" selectAllItems(this.checked, 'issuejicerts', this.name); " />
												</th>
												<th class="itemno" scope="col"><spring:message code="certmanagement.item"/></th>
												<th class="desc" scope="col"><spring:message code="certmanagement.description"/></th>
												<th class="caltype" scope="col"><spring:message code="certmanagement.caltype"/></th>
												<th class="certduration" scope="col"><spring:message code="certmanagement.certdur"/></th>
												<th class="calibrations" scope="col"><spring:message code="certmanagement.calibrations"/></th>
											</tr>
										</thead>
										<tfoot>
											<tr>
												<td colspan="6">&nbsp;</td>
											</tr>
										</tfoot>
										<tbody>
											<c:forEach var="ji" items="${form.job.items}">
												<c:set var="ccw" value="${form.customCerts.get(ji.jobItemId)}" />
												<tr>
													<td class="all">
														<c:set var="checked" value=""/>
														<c:if test="${ccw.selected == true}"><c:set var="checked" value="checked"/> </c:if>
														<form:checkbox path="customCerts[${ji.jobItemId}].selected" value="true" checked="${checked}" />
														<form:hidden path="customCerts[${ji.jobItemId}].selected" value="false" />
													</td>
													<td class="itemno">${ji.itemNo}</td>
													<td class="desc"><cwms:showmodel instrumentmodel="${ji.inst.model}"/></td>
													<td class="caltype">
														<form:select path="customCerts[${ji.jobItemId}].calTypeId">
															<c:forEach var="ct" items="${calTypes}">
																<option value="${ct.calTypeId}" <c:if test="${ccw.calTypeId == ct.calTypeId}"> selected="selected" </c:if>>
																<t:showTranslationOrDefault translations="${ct.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/></option>
															</c:forEach>
														</form:select>
														<a href="#" class="imagelink" onclick=" copySelectedOption('down', 'select', $j(this).siblings('select')); return false; ">
															<img src="img/icons/arrow_down.png" width="10" height="16" alt="<spring:message code='certmanagement.imgarrowdown'/>" title="<spring:message code='certmanagement.imgarrowdown'/>" />
														</a>
														<a href="#" class="imagelink" onclick="  copySelectedOption('up', 'select', $j(this).siblings('select')); return false; ">
															<img src="img/icons/arrow_up.png" width="10" height="16" alt="<spring:message code='certmanagement.imgarrowup'/>" title="<spring:message code='certmanagement.imgarrowup'/>" />
														</a>
													</td>
													<td class="certduration">
														<form:input type="text" path="customCerts[${ji.jobItemId}].duration" value="${form.customCerts[ji.jobItemId].duration}" size="2" />
														<a href="#" class="imagelink" onclick=" copySelectedOption('down', 'input', $j(this).siblings('input')); return false; ">
															<img src="img/icons/arrow_down.png" width="10" height="16" alt="<spring:message code='certmanagement.imgarrowdown'/>" title="<spring:message code='certmanagement.imgarrowdown'/>" />
														</a>
														<a href="#" class="imagelink" onclick="  copySelectedOption('up', 'input', $j(this).siblings('input')); return false; ">
															<img src="img/icons/arrow_up.png" width="10" height="16" alt="<spring:message code='certmanagement.imgarrowup'/>" title="<spring:message code='certmanagement.imgarrowup'/>" />
														</a>
													</td>
													<td class="calibrations">
														<c:choose>
															<c:when test="${ji.calLinks.size() > 0}">
																<form:select path="customCerts[${ji.jobItemId}].calId" class="width86">
																	<option value=""> -- <spring:message code="certmanagement.optdonotlink"/> -- </option>
																	<c:forEach var="cl" items="${ji.calLinks}">
																		<option value="${cl.cal.id}" <c:if test="${not empty ccw.calId}"></c:if> <c:if test="${ccw.calId == cl.cal.id}"> selected="selected" </c:if>>${cl.cal.startedBy.name} - <fmt:formatDate value="${cl.cal.startTime}" type="both" dateStyle="SHORT" timeStyle="SHORT" /> - <t:showTranslationOrDefault translations="${cl.cal.calType.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/>) - ${cl.cal.capability.reference} - ${cl.cal.calProcess.process} - ${cl.cal.calClass.desc}</option>
																	</c:forEach>
																</form:select>
																<a href="#" class="imagelink" onclick=" copySelectedOption('down', 'select', $j(this).siblings('select')); return false; ">
																	<img src="img/icons/arrow_down.png" width="10" height="16" alt="<spring:message code='certmanagement.imgarrowdown'/>" title="<spring:message code='certmanagement.imgarrowdown'/>" />
																</a>
																<a href="#" class="imagelink" onclick="  copySelectedOption('up', 'select', $j(this).siblings('select')); return false; ">
																	<img src="img/icons/arrow_up.png" width="10" height="16" alt="<spring:message code='certmanagement.imgarrowup'/>" title="<spring:message code='certmanagement.imgarrowup'/>" />
																</a>
															</c:when>
															<c:otherwise>
																<spring:message code="certmanagement.noexistcalforitem"/>
															</c:otherwise>
														</c:choose>
													</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>											
									
									<!-- center align item cert submit div -->										
									<div class="text-center marg-top">
										<input type="submit" name="" value="<spring:message code='certmanagement.isssitecerts'/>" />
									</div>
									<!-- end of item cert submit div -->
								
								</div>
								<!-- end of job item cert issue div -->
								
								<!-- div displays all groups on job and allows certs to be issued for one or more groups (site jobs only) -->
								<div id="issuegroupcerts-tab" class="hid">
							
									<table class="default4 issuegroupcerts" summary="<spring:message code='certmanagement.def4issgroupcerts'/>">
										<thead>
											<tr>
												<td <c:choose><c:when test="${isAdmin}"> colspan="2" </c:when><c:otherwise> colspan="4" </c:otherwise></c:choose>>
													<spring:message code="certmanagement.string1"/>:
													<input type="date" name="" id="groupcertDate" value="" />
													<span class="attention">${status.errorMessage}</span>
												</td>
												
												<c:if test="${isAdmin}">
													<td colspan="2">
														<spring:message code="certmanagement.issby"/>:
														<select name="" id="groupIssuer">
															<c:forEach var="con" items="${businessContacts}">
																<option value="${con.key}" <c:if test="${form.issuerId}"></c:if> <c:if test="${form.issuerId == con.key}"> selected="selected" </c:if>>${con.value}</option>
															</c:forEach>
														</select>
														<span class="attention">${status.errorMessage}</span>
													</td>
												</c:if>
											</tr>
											<tr>
												<th class="all" scope="col">
													<spring:message code="certmanagement.all"/> <input type="checkbox" onclick=" selectAllItems(this.checked, 'issuegroupcerts', this.name); " />
												</th>
												<th class="group" scope="col"><spring:message code="certmanagement.groupid"/></th>
												<th class="caltype" scope="col"><spring:message code="certmanagement.caltype"/></th>
												<th class="certduration" scope="col"><spring:message code="certmanagement.certdur"/></th>
											</tr>
										</thead>
										<tfoot>
											<tr>
												<td colspan="4">&nbsp;</td>
											</tr>
										</tfoot>
										<tbody>
										<c:choose>
											<c:when test="${form.job.groups.size() > 0}">
												<c:forEach var="group" items="${form.job.groups}">
													 <c:set var="form.customGroupCerts" value="${calReqsText.get(group.id)}" />
													<tr>																	
														<td class="all">
															<c:set var="checked" value=""/>
															<c:if test="${gw.selected == true}"><c:set var="checked" value="checked"/> </c:if>
															<form:checkbox path="customGroupCerts[${group.id}].selected" value="true"  onclick=" enableGroupItems(this.checked, 'gitems${group.id}'); " checked="${checked}"/>
															<form:hidden path="_customGroupCerts[${group.id}].selected" value="false" />																		
														</td>
														<td class="group">
															Group ${group.id}
														</td>
														<td class="caltype">
															<form:select path="customGroupCerts[${group.id}].calTypeId">
																<c:forEach var="ct" items="${calTypes}">
																	<option value="${ct.calTypeId}" <c:if test="${gw.calTypeId == ct.calTypeId}"> selected="selected" </c:if>><t:showTranslationOrDefault translations="${ct.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/></option>
																</c:forEach>
															</form:select>
														</td>
														<td class="certduration">
															<form:input type="text" path="customGroupCerts[${group.id}].duration" value="${form.customGroupCerts[group.id].duration}" size="2" />
														</td>
													</tr>															
										
													<tr>
														<td>&nbsp;</td>
														<td colspan="3" style=" padding: 0; ">
														
															<table class="child-table groupitems gitems${group.id}" border="0" cellpadding="0" cellspacing="0" summary="<spring:message code='certmanagement.childtablegroupitems'/>">
																<thead>
																	<tr>																						
																		<th class="all" scope="col">
																			<spring:message code="certmanagement.all"/> <input type="checkbox" onclick=" selectAllItems(this.checked, 'gitems${group.id}', this.name); " disabled="disabled" />
																		</th>
																		<th class="itemno" scope="col"><spring:message code="certmanagement.item"/></th>
																		<th class="desc" scope="col"><spring:message code="description"/></th>
																		<th class="serial" scope="col"><spring:message code="serialno"/></th>
																		<th class="plant" scope="col"><spring:message code="plantno"/></th>
																	</tr>
																</thead>
																<tbody>
																	<c:choose>
																		<c:when test="${group.items.size() > 0}">
																			<c:forEach var="gi" items="${group.items}">
																				<tr>																							
																					<td class="all">
																						<form:checkbox path="customGroupCerts[${group.id}].jobItemIds" class="noboundary" value="${gi.jobItemId}" disabled="disabled" />
																					</td>
																					<td class="itemno">${gi.itemNo}</td>
																					<td class="desc"><cwms:showmodel instrumentmodel="${gi.inst.model}"/></td>
																					<td class="serial">${gi.inst.serialno}</td>
																					<td class="plant">${gi.inst.plantno}</td>
																				</tr>
																			</c:forEach>
																		</c:when>
																		<c:otherwise>
																			<tr>
																				<td colspan="5"><spring:message code="certmanagement.noitemsthisgroup"/></td>
																			</tr>
																		</c:otherwise>
																	</c:choose>	
																</tbody>
															</table>
															
														</td>
														
													</tr>
																													
												</c:forEach>
												
											</c:when>
											<c:otherwise>
												<tr>
													<td class="text-center bold" colspan="4">
														<spring:message code="certmanagement.nogroupsthisjob"/>
													</td>
												</tr>
											</c:otherwise>
										</c:choose>
										</tbody>
									</table>
							
									<!-- center align group cert submit button -->
									<div class="text-center marg-top">													
										<input type="submit" name="" value="<spring:message code='certmanagement.issgroupsitecerts'/>" />
									</div>
									<!-- end of group cert submit button div -->
								
								</div>
								<!-- end of job group cert issue div -->
							
							</form:form>
							
						</c:if>								
						
					</div>
					<!-- end of div to hold all tabbed submenu content -->
					
				</div>
				<!-- end of div to hold tabbed submenu -->
				
				<!-- this div clears floats and restores page flow -->
				<div class="clear"></div>
					
			</div>
		<!-- end of certificate management infobox -->
    </jsp:body>
</t:crocodileTemplate>