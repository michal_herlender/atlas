<%-- File name: /trescal/core/jobs/certificate/certsearch.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code='certsearch.headtext'/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
       <script type='text/javascript' src='script/trescal/core/jobs/certificate/CertSearch.js'></script>
    </jsp:attribute>
    <jsp:body>
        <!-- infobox contains all form elements relating to searching for a job and is styled with nifty corners -->				
<div class="infobox">

	<form:form method="post" action="" id="certsearchform" modelAttribute="certsearchform">

		<t:showErrors path="certsearchform.*" showFieldErrors="true" />
		
		<fieldset>

			<legend><spring:message code='certsearch.headtext'/></legend>
		
			<!-- displays content in column 69% wide (left) -->
			<div style=" float:left; padding:0.4%; width:49%; ">

				<ol>
					<li>
						<form:label path="certNo"><spring:message code='certsearch.certno'/>:</form:label>
						<form:input path="certNo" type="text" id="certNo" tabindex="1" />
					</li>
					<li>
						<form:label path="status"><spring:message code='certsearch.status'/>:</form:label>
						<form:select path="status" id="status" tabindex="2">
							<option value="">-- <spring:message code='certsearch.any'/> --</option>
							<c:forEach var="cs" items="${certstatuses}">
								<option value="${cs.name}">${cs.message}</option>
							</c:forEach>
						</form:select>
					</li>
					<li>
						<form:label path="calTypeId"><spring:message code='certmanagement.caltype'/>:</form:label>
						<form:select path="calTypeId" id="calTypeId" tabindex="3">
							<option value="">-- <spring:message code='certsearch.any'/> --</option>
							<c:forEach var="ct" items="${caltypes}">
								<option value="${ct.calTypeId}"><t:showTranslationOrDefault translations="${ct.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/></option>
							</c:forEach>
						</form:select>
					</li>
					<li id="descrip">
                            <label for="desc"><spring:message code='sub-family'/>:</label>
                            <!-- float div left -->
                            <div class="float-left">
                                <form:input type="text" path="desc" id="desc" tabindex="4" />
                                <form:input type="hidden" path="descid" id="descid" />
                            </div>                                                                                          
                            <!-- clear floats and restore pageflow -->
                            <div class="clear-0"></div>
                        </li>
					<li id="manufacturer">
						<label for="mfr"><spring:message code='certsearch.manufacturer'/>:</label>
						<!-- float div left -->
						<div class="float-left">
                            <form:input type="text" path="mfr" id="mfr" tabindex="5" />
                            <form:input type="hidden" path="mfrid" id="mfrid" />
						</div>																							
						<!-- clear floats and restore pageflow -->
						<div class="clear-0"></div>
					</li>
					<li>
						<form:label path="model"><spring:message code='certsearch.model'/>:</form:label>
						<form:input type="text" path="model" id="model" tabindex="6" />		
					</li>
					<li>
						<label for="issueDate1"><spring:message code='certsearch.issdate'/>:</label>
						
						<form:select path="issueDateBetween" id="issueDateBetween" tabindex="7" onchange=" changeDateOption(this.value, 'issueDateSpan'); return false; ">
							<option value="false" <c:if test="${certsearchform.issueDateBetween == false}"> selected </c:if>><spring:message code='certsearch.on'/></option>
							<option value="true" <c:if test="${certsearchform.issueDateBetween == true}"> selected </c:if>><spring:message code='certsearch.between'/></option>
						</form:select>
						
						<form:input  path="issueDate1" id="issueDate1" type="date" tabindex="8" />
						
						<span id="issueDateSpan" <c:choose><c:when test="${certsearchform.issueDateBetween == true}"> class="vis" </c:when><c:otherwise> class="hid" </c:otherwise></c:choose>><spring:message code='certsearch.and'/> &nbsp;
							<form:input  path="issueDate2" id="issueDate2" type="date" tabindex="9" />
						</span>
					</li>
					<li>
						<label for="calDate1"><spring:message code='certsearch.caldate'/>:</label>
						
						<form:select path="calDateBetween" id="calDateBetween" onchange=" changeDateOption(this.value, 'calDateSpan'); return false; " tabindex="10">
							<option value="false" <c:if test="${certsearchform.calDateBetween == false}"> selected </c:if>><spring:message code='certsearch.on'/></option>
							<option value="true" <c:if test="${certsearchform.calDateBetween == true}"> selected </c:if>><spring:message code='certsearch.between'/></option>
						</form:select>
						
						<form:input  path="calDate1" id="calDate1" type="date" tabindex="11" />
						
						<span id="calDateSpan" <c:choose><c:when test="${certsearchform.calDateBetween == true}"> class="vis" </c:when><c:otherwise> class="hid" </c:otherwise></c:choose>><spring:message code='certsearch.and'/>&nbsp;
							<form:input path="calDate2" id="calDate2" type="date" tabindex="12" />
						</span>
					</li>
					<li>
						<label for="jobItemDateComplete1"><spring:message code='web.jobitemcompletiondate.label'/>:</label>
						
						<form:select path="jobItemCompletionDateBetween" id="jiCompletionDateBetween" onchange=" changeDateOption(this.value, 'jobItemCompletionDateSpan'); return false; " tabindex="13">
							<option value="false" <c:if test="${certsearchform.jobItemCompletionDateBetween == false}"> selected </c:if>><spring:message code='certsearch.on'/></option>
							<option value="true" <c:if test="${certsearchform.jobItemCompletionDateBetween == true}"> selected </c:if>><spring:message code='certsearch.between'/></option>
						</form:select>
						
						<form:input  path="jobItemDateComplete1" id="jobItemDateComplete1" type="date" tabindex="14" />
						
						<span id="jobItemCompletionDateSpan" <c:choose><c:when test="${certsearchform.jobItemCompletionDateBetween == true}"> class="vis" </c:when><c:otherwise> class="hid" </c:otherwise></c:choose>><spring:message code='certsearch.and'/>&nbsp;
							<form:input path="jobItemDateComplete2" id="jobItemDateComplete2" type="date" tabindex="15" />
						</span>
					</li>
					<li>
						<label>&nbsp;</label>
						<input type="submit" value="<spring:message code='search'/>" tabindex="22" />
					</li>
				</ol>
			
			</div>
			<!-- end of left column -->
			
			<!-- displays content in column 49% wide (right) -->
			<div style=" float:right; padding: 0.4%; width:49%; ">
			
				<ol>
					<li>
						<form:label path="coid"><spring:message code='company'/>:</form:label>								
						<div class="float-left extendPluginInput">
							<div class="compSearchJQPlugin">
								<input type="hidden" name="compCoroles" value="CLIENT,BUSINESS" />
								<input type="hidden" name="field" value="<spring:message code='certsearch.coid'/>" />
								<input type="hidden" name="copyOption" value="<spring:message code='certsearch.false'/>" />
							</div>
						</div>
						
						<!-- clear floats and restore page flow -->
						<div class="clear"></div>
					</li>
					<li>
						<form:label path="personid"><spring:message code='certsearch.contact'/>:</form:label>
						<div id="contSearchPlugin">
							<input type="hidden" id="contCoroles" value="CLIENT,BUSINESS" />
							<input type="hidden" id="contindex" value="14" />
							<!-- contact results listed here -->
						</div>
						
						<!-- clear floats and restore page flow -->
						<div class="clear"></div>
					</li>
					<li>
						<form:label path="tpId"><spring:message code='certsearch.thirdpart'/>:</form:label>								
						<div class="float-left extendPluginInput">
							<div class="compSearchJQPlugin">
								<input type="hidden" name="compCoroles" value="CLIENT,SUPPLIER,BUSINESS" />
								<input type="hidden" name="field" value="tpId" />
								<input type="hidden" name="copyOption" value="false" />
							</div>
						</div>
						
						<!-- clear floats and restore page flow -->
						<div class="clear"></div>
					</li>
					<li>
						<form:label path="tpCertNo"><spring:message code='certsearch.thirdpartcertno'/>:</form:label>
						<form:input path="tpCertNo" type="text" id="tpCertNo" tabindex="17" />
					</li>
					<li>
						<form:label path="jobNo"><spring:message code='jobno'/>:</form:label>
						<form:input path="jobNo" type="text" id="jobNo" tabindex="18" />
					</li>
					<li>
						<form:label path="bc"><spring:message code='barcode'/>:</form:label>
						<form:input path="bc" type="text" id="bc" tabindex="19" />
					</li>
					<li>
						<form:label path="serialNo"><spring:message code='serialno'/>:</form:label>
						<form:input path="serialNo" type="text" id="serialNo" tabindex="20" />
					</li>
					<li>
						<form:label path="plantNo"><spring:message code='plantno'/>:</form:label>
						<form:input path="plantNo" type="text" id="plantNo" tabindex="21" />
					</li>					
				</ol>
			
			</div>
			<!-- end of right column -->

		</fieldset>
		
	</form:form>

</div>
<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>