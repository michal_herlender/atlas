<%-- File name: /trescal/core/jobs/job/viewjob.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="scriptPart">
		<script type="module" src="script/components/cwms-datetime/cwms-datetime.js"></script>
		<script src="script/trescal/core/jobs/job/EditJob.js"></script>
	</jsp:attribute>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="viewjob.editjob" /> - ${job.jobno}</span>
	</jsp:attribute>
	<jsp:body>
		<!-- main div which contains all job information -->
		<div id="editjob">
			<!-- infobox contains all job information and is styled with nifty corners -->
			<div class="infobox">
				<fieldset>
					<legend>
						<spring:message code="joblink.information" />: 
					</legend>
					<ol>
						<li>
							<label class="width30"><spring:message code="croverview.jobno" />:</label>
							<span>
								<cwms:securedLink permission="JOB_VIEW" parameter="?jobid=${job.jobid}" classAttr="mainlink">
									 ${job.jobno}
								</cwms:securedLink>
							</span>
						</li>
						<li>
							<label class="width30"><spring:message code="company" />:</label>
							<span><c:out value="${job.con.sub.comp.coname}" /></span>
						</li>
						<li>
							<label class="width30"><spring:message code="subdivision" />:</label>
							<span><c:out value="${job.con.sub.subname}" /></span>
						</li>
						<li>
							<label class="width30"><spring:message code="changeclient.contact" />:</label>
							<span>
								<c:out value="${job.con.name}" />
							</span>
						</li>
					</ol>
				</fieldset>
				<form:form method="post" action="" modelAttribute="form">
				<fieldset>
					<legend>
						<spring:message code="viewjob.string3" />
					</legend>
					<ol>
						<li>
							<label class="width30" for="jobType">
								<spring:message code="addjob.jobtype" />
								<c:out value=": *"/>
							</label>
							<c:choose>
								<c:when test="${jobItemCount eq 0}">
									<form:select path="jobType" items="${jobTypes}"
										itemLabel="description" itemValue="name"
										onChange=" changeJobType(this.value); " />
								</c:when>
								<c:otherwise>
									<form:hidden path="jobType" />
									<c:out value="${form.jobType.getDescription()}" />
									<c:out value=" - "/>
									<spring:message code="viewjob.jobtypefixed" />
								</c:otherwise>
							</c:choose>									
						</li>
						<!--
							The allowed default booking in addresses for the job are at
							- client addresses if a site job, and at 
							- business addresses if otherwise (inhouse/laboratory job)
							One or the other selections appears dependent on job type selected
							with cascading to location selection, when present
						 -->
						<c:choose>
							<c:when test="${form.jobType == 'SITE'}">
								<c:set var="cbiAddrVis" value="vis" />
								<c:set var="cbiLocVis" value="${not empty clientInLocations ? 'vis' : 'hid'}" />
								<c:set var="bbiAddrVis" value="hid" />
								<c:set var="bbiLocVis" value="hid" />
							</c:when>
							<c:otherwise>
								<c:set var="cbiAddrVis" value="hid" />
								<c:set var="cbiLocVis" value="hid" />
								<c:set var="bbiAddrVis" value="vis" />
								<c:set var="bbiLocVis" value="${not empty businessInLocations ? 'vis' : 'hid'}" />
							</c:otherwise>
						</c:choose>
						<li id="editCbiAddr" class="${cbiAddrVis}">
							<label class="width30">
								<spring:message code="addjob.bookinaddr" />
								&nbsp;(<spring:message code="companyrole.client" />
								<c:out value="): *"/>
							</label>
							<form:select path="bookedInClientAddressId"
								items="${clientAddresses}" itemValue="key"
								itemLabel="value"
								onchange="addrChange(this.value, ${form.bookedInClientLocationId}, 'bookedInClientLocationId', 'editCbiLoc'); " />
							<form:errors path="bookedInClientAddressId" class="attention" />
						</li>
						<li id="editCbiLoc" class="${cbiLocVis}">
							<label class="width30">
								<spring:message code="addjob.bookinloc" />
								&nbsp;(<spring:message code="companyrole.client" />):
							</label>
							<form:select path="bookedInClientLocationId"
							    id="${form.bookedInClientLocationId}"
								items="${clientInLocations}" itemValue="locationid"
								itemLabel="location" />
						</li>
						<li id="editBbiAddr" class="${bbiAddrVis}">
							<label class="width30">
								<spring:message code="addjob.bookinaddr" />
								&nbsp;(<spring:message code="companyrole.business" />
								<c:out value="): *"/>
							</label>
							<form:select path="bookedInBusinessAddressId"
								items="${businessAddresses}" itemValue="key"
								itemLabel="value"
								onchange="addrChange(this.value, ${form.bookedInBusinessLocationId}, 'bookedInBusinessLocationId', 'editBbiLoc'); " />
							<form:errors path="bookedInBusinessAddressId" class="attention" />
						</li>
						<li id="editBbiLoc" class="${bbiLocVis}">
							<label class="width30">
								<spring:message code="addjob.bookinloc" />
								&nbsp;(<spring:message code="companyrole.business" />):
							</label>
							<form:select path="bookedInBusinessLocationId"
								items="${businessInLocations}" itemValue="locationid"
								itemLabel="location" />
						</li>
<!-- 						///// -->
						<li id="" class="">
							<label class="width30">
							<spring:message code="viewjob.trescalcontactname" />:
								<c:out value=": *"/>
							</label>
							<form:select path="businessSubdivContactId" items="${businessSubdivContacts}" itemLabel="value" itemValue="key" />
							<form:errors path="businessSubdivContactId" class="attention" />

						</li>
						<li>
							<label class="width30" for="jobContact">
								<spring:message code="viewjob.jobcontact" />
								<c:out value=": *"/>
							</label>
							<form:select path="jobContactId" items="${clientContacts}" itemLabel="value" itemValue="key" />
							<form:errors path="jobContactId" class="attention" />
						</li>
						<li id="editReturnAddr">
							<label class="width30">
								<spring:message code="viewjob.returntoaddr" />
								&nbsp;(<spring:message code="companyrole.client" />
								<c:out value="): *"/>
							</label>
							<form:select path="returnToAddressId"
								items="${clientAddresses}" itemValue="key"
								itemLabel="value"
								onchange="addrChange(this.value, ${form.returnToLocationId}, 'returnToLocationId', 'editDelLoc');" />
							<form:errors path="returnToAddressId" class="attention" />
						</li>
						<c:set var="delLocVis" value="${not empty returnToLocations ? 'vis' : 'hid'}" />
						<li id="editDelLoc" class="${delLocVis}">
							<label class="width30"><spring:message code="viewjob.returntoloc" />:</label>
							<form:select path="returnToLocationId"
								items="${returnToLocations}" itemValue="locationid"
								itemLabel="location" />
							<form:errors path="returnToLocationId" class="attention" />
						</li>
						
						<li>
							<label class="width30" for="receiptDate">
								<spring:message code="addjob.receiptdate" />
								<c:out value=": *"/>
							</label>
							<form:input type="datetime-local" path="receiptDate"
									step="60" max="${cwms:isoDatetime(form.currentDate)}"/>
							<form:errors path="receiptDate" class="attention" />
						</li>
						
						<li>
							<label class="width30" for="pickupDate"><spring:message code="addjob.pickupdate"/>:</label>
							<form:input type="datetime-local" path="pickupDate"
									step="60" max="${cwms:isoDatetime(form.currentDate)}"/>
							<form:errors path="pickupDate" class="attention" />
						</li>
						
						<li>
							<label class="width30" ><spring:message code="addjob.overrideDateInOnJobItems"/>:</label>
							<form:checkbox path="overrideDateInOnJobItems" id="overrideDateInOnJobItems" onClick="checkOverrideDateInOnJobItems(this);"/>
							<div id="overrideDateInOnJobItemsDialog" class="hid" title="<spring:message code='viewjob.overrideDateInOnJobItemsDialogTitle'/>">
								<spring:message code="viewjob.overrideDateInOnJobItemsDialogText"/>
							</div>
						</li>
						
						<li>
							<label class="width30" for="agreedDelDate">
								<spring:message code="viewjob.agrdeldate" />:
							</label>
							<!-- jqCalendar input -->
							<form:input type="date" id="agreedDeliveryDate" name="agreedDeliveryDate" 
								path="agreedDeliveryDate" />
							<a href="" class="mainlink" onclick="event.preventDefault(); $j('#agreedDeliveryDate').val('');">
								<spring:message code="clear" />
							</a>
							<form:errors path="agreedDeliveryDate" class="attention" />
						</li>								
						<li>
							<label class="width30" for="clientref">
								<spring:message code="addjob.clientref" />:
							</label>
							<!-- jqCalendar input -->
							<form:input type="text" path="clientReference" size="30" />
							<form:errors path="clientReference" class="attention" />
						</li>
							<li>
								<label class="width30" for="carriageOut">
									<spring:message code="viewjob.estcarrout" />:
								</label>
								<c:choose>
									<c:when test="${estCarriageOutEditable}">
										<form:input path="estCarriageOut" />
									</c:when>
									<c:otherwise>
										<form:hidden path="estCarriageOut" />
										<c:out value="${estCarriageOut}"/>
										&nbsp;(<spring:message code="company.noteditable" />)
									</c:otherwise>
								</c:choose>
								<form:errors path="estCarriageOut" class="attention" />
							</li>
						<li>
							<label class="width30" for="currencyCode">
								<spring:message code="addjob.currency" />
								<c:out value=": *"/>
							</label>
							<form:select path="currencyCode" items="${supportedCurrencies}" itemLabel="value" itemValue="key" />
							<form:errors path="currencyCode" class="attention" />
						</li>
						<li>
							<label class="width30" for="transportInId">
								<spring:message code="viewjob.transpin" />:
							</label>
							<form:select path="transportInId">
								<c:forEach var="t" items="${transportOptions}">
									<form:option value="${t.id}">
										<c:choose>
											<c:when test="${not empty t.localizedName}">
												${t.localizedName }
											</c:when>
											<c:otherwise>
												<t:showTranslationOrDefault translations="${t.method.methodTranslation}" defaultLocale="${defaultlocale}"/>
											</c:otherwise>
											</c:choose>
											<c:if test="${t.sub != null}">
												- ${t.sub.subname}
											</c:if>
									</form:option>
								</c:forEach>
							</form:select>
							<form:errors path="transportInId" class="attention" />
							<br />
							<form:checkbox path="copyTransportIn" />
							<span style="font-size: 94%;">
								<spring:message code="viewjob.string17" />
							</span>
						</li>
						<li>
							<label class="width30" for="transportOutId">
								<spring:message code="viewjob.transout" />:
							</label>
							<form:select path="transportOutId">
								<c:forEach var="t" items="${transportOptions}">
									<form:option value="${t.id}">
										<c:choose>
											<c:when test="${not empty t.localizedName}">
												${t.localizedName }
											</c:when>
											<c:otherwise>
												<t:showTranslationOrDefault translations="${t.method.methodTranslation}" defaultLocale="${defaultlocale}"/>
											</c:otherwise>
											</c:choose>
											<c:if test="${t.sub != null}">
												- ${t.sub.subname}
											</c:if>
									</form:option>
								</c:forEach>
							</form:select>
							<form:errors path="transportOutId" class="attention" />
							<br />
							<form:checkbox path="copyTransportOut" />
							<span style="font-size: 94%;">
								<spring:message code="viewjob.string18" />
							</span>
						</li>
						<li>
							<label class="width30">&nbsp;</label>
							<input type="submit" name="update" value='<spring:message code="update"/>'/>
						</li>
					</ol>
					</fieldset>
				</form:form>
			</div>
		</div>
	</jsp:body>	
</t:crocodileTemplate>