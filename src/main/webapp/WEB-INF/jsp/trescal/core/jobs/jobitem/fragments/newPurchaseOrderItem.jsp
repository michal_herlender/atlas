<%-- File name: /trescal/core/jobs/jobitem/fragments/newPurchaseOrderItem.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="order" value="${poItem.order}"/>
<tr>
	<td>
		<a href="viewpurchaseorder.htm?id=${order.id}" class="mainlink">${order.pono}</a>
	</td>
	<td class="center">${poItem.itemno}</td>
	<td>${order.contact.sub.comp.coname}</td>
	<td>${order.contact.name}</td>
	<td><fmt:formatDate value="${order.regdate}" type="date" dateStyle="SHORT"/></td>
	<td>${order.createdBy.name}</td>
	<td class="cost2pc">
		<fmt:formatNumber type="CURRENCY" value="${poItem.finalCost}" currencySymbol="${order.currency.currencyERSymbol}"/>
	</td>
</tr>