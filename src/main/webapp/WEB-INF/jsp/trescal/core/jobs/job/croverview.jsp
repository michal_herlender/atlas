<%-- File name: /trescal/core/jobs/jobs/croverview.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="costs" tagdir="/WEB-INF/tags/costs"%>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
        <span class="headtext"><spring:message
				code="croverview.headtext" />&nbsp;${job.jobno}</span>
    </jsp:attribute>
	<jsp:attribute name="scriptPart">
<!--         Section between main_1 and main_2 -->
    </jsp:attribute>
	<jsp:body>
    <!-- main div which contains all job information -->
	<div id="viewjob">
	
		<!-- infobox contains all job information and is styled with nifty corners -->
		<div class="infobox">
		 	<c:set var="sym" value="${job.currency.currencyERSymbol}" />
			<h2>
				<spring:message code="croverview.jobno" />: 
				<span class="largeJobNo">
					<links:jobnoLinkDWRInfo rowcount="0" jobno="${job.jobno}" jobid="${job.jobid}"
							copy="true" />
				</span>
			</h2>
	
			<table class="default4">
				<thead>
					<tr>
						<th><spring:message code="item" /></th>
						<th><spring:message code="instmodelname" /></th>
						<th><spring:message code="croverview.crprice" /><br />(${sym})</th>
						<th><spring:message code="croverview.crpricesour" /></th>
						<!-- <th>Last CR<br/>Price</th> -->
						<th><spring:message code="croverview.lastinv" /><br />
								<spring:message code="croverview.price" /></th>
						<th><spring:message code="croverview.requirem" /></th>
						<th><spring:message code="croverview.workreq" /></th>
					</tr>
				</thead>
				<c:forEach var="ji" items="${job.items}" varStatus="jiStatus">
					<c:set var="penultimateJi" value="" />
					<c:set var="penultimateInvItem" value="" /> 
					<c:set var="penultimateNo" value="${ji.inst.jobItems.size() - 1}" />
					<c:forEach var="oldJi" items="${ji.inst.jobItems}">
						<c:if test="${jiStatus.index == penultimateNo}">
							<c:set var="penultimateJi" value="${oldJi}" />
							<c:if test="${oldJi.invoiceItems.size() > 0}">
								<c:set var="penultimateInvItem"
										value="${oldJi.invoiceItems.iterator().next()}" />
							</c:if>
						</c:if>
		    		</c:forEach> 
				    
						<tr
							style=" background-color: 
		                  	<c:choose>
		                        <c:when test="${ji.turn <= fastTrackTurn}">
		                           ${ji.calType.serviceType.displayColourFastTrack}; </c:when>
			                    <c:otherwise> ${ji.calType.serviceType.displayColour};</c:otherwise>
                            </c:choose>">
			                               
							<td>
								<links:jobitemLinkDWRInfo jobitem="${ji}" jicount="false"
									rowcount="${jiStatus.index}" />
							</td>

							<td>
								<instmodel:showInstrumentModelLink instrument="${ji.inst}"
									caltypeid="${ji.calType.calTypeId}" />
							</td>
									
							<td style="text-align: right;">
								<c:set var="style" value="" />
								<c:if
									test="${empty penultimateInvItem || penultimateInvItem.calibrationCost.finalCost != ji.calibrationCost.totalCost}">
									<c:set var="style" value="{color: red; font-weight: bold;}" />
								</c:if>
								<span style="">${ji.calibrationCost.totalCost}</span>
							</td>
							
							<td>
								<c:set var="cost" value="${ji.calibrationCost}" />
								<c:choose>							
								   <c:when test="${cost.costSrc == 'QUOTATION'}">
								   		<c:set var="qi"
											value="${cost.linkedCost.quotationCalibrationCost.quoteItem}" />
									  		QUOTE
									 	<links:showQuotationLink quotation="${qi.quotation}" />
									 	
						 				<c:choose>							
											<c:when test="${qi.quotation.issued != true}">
												[<span style="color: red;"
													title="<spring:message code="croverview.string1" />">
													<spring:message code="croverview.unissued" />
												</span>]
											</c:when>
											
											<c:when test="${qi.quotation.expired == true}">
												[<span style="color: red;"
													title="<spring:message code="croverview.string2" />">e</span>]
											</c:when>
										</c:choose>
		
										<br />
										Item <a
											href="<c:url value="/editquotationitem.htm?id=${qi.id}" />"
											class="mainlink">${qi.itemno}</a>	
								 	</c:when>
								 	<c:when test="${cost.costSrc == 'JOB_COSTING'}">
									    <c:set var="jci"
											value="${cost.linkedCost.jobCostingCalibrationCost.jobCostingItem}" />
									     <spring:message code="croverview.jobcost" />
									    <br />
									    <a href="viewjobcosting.htm?id=${jci.jobCosting.id}"
											class="mainlink">${jci.jobCosting.identifier}
									    </a>&nbsp;Item&nbsp;
									    <a href="editjobcostingitem.htm?id=${jci.id}"
											class="mainlink">${jci.itemno}
									    </a>
								   </c:when>
								   <c:when test="${cost.costSrc == 'INSTRUMENT'}"> 
								     	<c:set var="jci"
											value="${cost.linkedCost.jobCostingCalibrationCost.jobCostingItem}" />
								    	<spring:message code="croverview.jobcostinst" />
									    <br />
								     	<a href="viewjobcosting.htm?id=${jci.jobCosting.id}"
											class="mainlink">${jci.jobCosting.identifier}</a>&nbsp;Item&nbsp;<a
											href="editjobcostingitem.htm?id=${jci.id}" class="mainlink">${jci.itemno}</a>
							       </c:when>
								   <c:otherwise>
									     ${cost.costSrc}
								   </c:otherwise>
							    </c:choose>
							</td>
							<!-- <td>$penultimateJi.calibrationCost.totalCost</td> -->
							<td style="text-align: right;">
								<c:choose>		
							 		<c:when test="${not empty penultimateInvItem}">
								  		${penultimateInvItem.calibrationCost.totalCost} 
								  	</c:when>
							   		<c:otherwise> N/A </c:otherwise>
								</c:choose>
							</td>
							<td>
							    <c:set var="calReq" value="${calReqs.get(ji.jobItemId)}" />
							    <c:set var="calReqText" value="${calReqsText.get(ji.jobItemId)}" />
								<c:if test="${not empty calReq}"> 
							 		${calReqText}<br />
		 					    	(<spring:message code="${calReq.source}"
									arguments="${calReq.sourceParameter}" />)
 								</c:if>
							</td>
							<td>
								<c:forEach var="jiwr" items="${ji.workRequirements}">
									<links:procedureLinkDWRInfo displayName="false" capability="${jiwr.workRequirement.capability}" rowcount="0"/>
									<br>
					    		</c:forEach>
							</td>
						</tr>
				</c:forEach>
			</table>
			<br /><br />
			
		</div>
	</div>
</jsp:body>
</t:crocodileTemplate>