<%-- File name: /trescal/core/jobs/jobitem/jicalibration.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs"%>
<%@ taglib prefix="addr" tagdir="/WEB-INF/tags/address"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<jobs:jobItemTemplate>
	<jsp:attribute name="subScript">
		<script src='script/trescal/core/jobs/jobitem/JIGeneralServiceOperation.js'></script>
		<script>
			//grab scroll to parameter passed when using job item key navigation
			var scrollto = '${request.getParameter("scrollto")}';
		</script>
	</jsp:attribute>
	<jsp:body>
		<!-- infobox is styled with nifty corners -->
		<div class="infobox">
			<table class="default2 jigeneralserviceoperationtable" summary="This table contains details of any general service operations for this item">
				<thead>
					<tr>
						<td colspan="7">
							<spring:message code="generalserviceoperations.headtext"/>
							(${gsoliste.size()})
							<c:if test="${viewWrapper.readyForGeneralServiceOperation}">
								-
								<cwms:securedLink permission="GENERALSERVICEOPERATION_NEW"  classAttr="mainlink"  parameter="?jobitemid=${jobItem.jobItemId}" collapse="True" >
									<spring:message code="jigeneralserviceoperation.newgso"/>
								</cwms:securedLink>
							</c:if>
						</td>
					</tr>
					<tr>
						<th class="gsodocument" scope="col"><spring:message code="documents"/></th>
						<th class="gsostartedon" scope="col"><spring:message code="jigeneralserviceoperation.startedon"/>
						 / <spring:message code="jigeneralserviceoperation.startedby"/></th>
						<th class="gsocompletedon" scope="col"><spring:message code="jigeneralserviceoperation.completedon"/>
						 / <spring:message code="jigeneralserviceoperation.completedby"/></th> 
						<th class="gsoclientdecision" scope="col"><spring:message code="generalserviceoperation.clientdecisionrequired"/></th>
						<th class="gsooutcome" scope="col"><spring:message code="generalserviceoperation.outcome"/></th>
						<th class="gsostatus" scope="col"><spring:message code="jobresults.stat"/></th>
						<th class="gsoaction" scope="col"><spring:message code="jifaultreport.actions"/></th>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${gsoliste.size() == 0}">
							<tr>
								<td colspan="7" class="center bold"><spring:message code="jigeneralserviceoperation.string1"/></td>
							</tr>
						</c:when>
						<c:otherwise>
							<c:set var="size" value="${ gsoliste.size() }"/>
							<c:forEach var="gso" items="${gsoliste}" varStatus="rowStatus">
								<tr id="gso${gso.id}">
									<td class="gsodocument">${gso.getJobItemGsoDocument(jobItem) ne null ? gso.getJobItemGsoDocument(jobItem).documentNumber:''}</td>
									<td class="gsostartedon">
										<fmt:formatDate value="${gso.startedOn}" type="both" dateStyle="SHORT" timeStyle="SHORT"/>
										&nbsp;
										(${gso.startedBy.shortenedName})
									</td>
									<td class="gsocompletedon">
										<c:if test="${gso.status.name eq 'Complete'}">
											<fmt:formatDate value="${gso.completedOn}" type="both" dateStyle="SHORT" timeStyle="SHORT"/>
											&nbsp;
											(${gso.completedBy.shortenedName})
										</c:if>
									</td>
									<td>
										<c:choose>
											<c:when test="${gso.clientDecisionNeeded != null and gso.clientDecisionNeeded}">
												<spring:message code="yes"/>
											</c:when>
											<c:otherwise>
												<spring:message code="no"/>
											</c:otherwise>
										</c:choose>
									</td>
									<td>
										${ gso.getJobItemOutcome(jobItem) ne null ? gso.getJobItemOutcome(jobItem).getGenericValue().getTranslation():'' }
									</td>
									<td class="gsostatus">
										<cwms:besttranslation translations="${gso.status.nametranslations}"/>
									</td>
								    <td>
								    	<c:set var="jioutcome" value="${ gso.getJobItemOutcome(jobItem) }"></c:set>
										<c:if test="${gso.status.name eq 'On-going' and empty jioutcome}">
											<a href="completegso.htm?jobitemid=${ jobItem.jobItemId }&gsoid=${ gso.id }" class="mainlink">
												<spring:message code="poitemreceiptstatus.complete"/>
											</a>
										</c:if>
										<c:if test="${gso.status.name eq 'Complete' and jioutcome ne null and jioutcome.genericValue ne 'FAILED_REDO' and viewWrapper.readyForGeneralServiceOperationDocumentUpload and rowStatus.index eq (size-1)}">
											<a href="uploadgsodocument.htm?jobitemid=${ jobItem.jobItemId }&gsoid=${ gso.id }" class="mainlink">
												<spring:message code="jicertificates.uplfile"/>
											</a>
										</c:if>
										<c:if test="${not empty gsodocliste.get(gso.id)}">
											<a href="editgsodocument.htm?jobitemid=${ jobItem.jobItemId }&gsodocid=${ gsodocliste.get(gso.id) }" class="mainlink">
												Edit Document
											</a>
										</c:if>
									</td>
								</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="7">&nbsp;</td>
					</tr>
				</tfoot>
			</table>
		</div>
		<!-- end of infobox div -->
	</jsp:body>
</jobs:jobItemTemplate>