<%-- File name: /trescal/core/jobs/jobitem/jipat.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs"%>

<jobs:jobItemTemplate>
    <jsp:attribute name="subScript">
        <script type='text/javascript' src='script/trescal/core/jobs/jobitem/JIPAT.js'></script>
		<script type='text/javascript' src='script/trescal/core/documents/ImageGalleryFunctions.js'></script>
		<script>
			// grab scroll to parameter passed when using job item key navigation
			var scrollto = '${request.getParameter("scrollto")}';
		</script>
	</jsp:attribute>
	<jsp:body>
		<form:form modelAttribute="form" id="patform" method="post">
			<fieldset>
				<ol>
					<li><h5><spring:message code="jipat.recnewpatres"/></h5></li>
					<li>
						<form:label path="pat.plugOk"><spring:message code="jipat.plugok"/></form:label>
						<form:select path="pat.plugOk">
							<option value="true" <c:if test="${!empty status.value && status.value == true}"> selected="selected" </c:if>><spring:message code="jipat.pass"/></option>
							<option value="false" <c:if test="${!empty status.value && status.value == false}"> selected="selected" </c:if>><spring:message code="jipat.fail"/></option>
							<option value="" <c:if test="${!empty status.value}"> selected="selected" </c:if>>N/A</option>
						</form:select>
					</li>
					<li>
						<form:label path="pat.fuseOk"><spring:message code="jipat.fuseok"/></form:label>
						<form:select path="pat.fuseOk">
							<option value="true" <c:if test="${!empty status.value && status.value == true}"> selected="selected" </c:if>><spring:message code="jipat.pass"/></option>
							<option value="false" <c:if test="${!empty status.value && status.value == false}"> selected="selected" </c:if>><spring:message code="jipat.fail"/></option>
							<option value="" <c:if test="${!empty status.value}"> selected="selected" </c:if>>N/A</option>
						</form:select>
					</li>
					<li>
						<form:label path="pat.cableOk"><spring:message code="jipat.cableok"/></form:label>
						<form:select path="pat.cableOk">
							<option value="true" <c:if test="${!empty status.value && status.value == true}"> selected="selected" </c:if>><spring:message code="jipat.pass"/></option>
							<option value="false" <c:if test="${!empty status.value && status.value == false}"> selected="selected" </c:if>><spring:message code="jipat.fail"/></option>
							<option value="" <c:if test="${!empty status.value}"> selected="selected" </c:if>>N/A</option>
						</form:select>
					</li>
					<li>
						<form:label path="pat.caseOk"><spring:message code="jipat.cableok"/></form:label>
						<form:select path="pat.caseOk">
							<option value="true" <c:if test="${!empty status.value && status.value == true}"> selected="selected" </c:if>><spring:message code="jipat.pass"/></option>
							<option value="false" <c:if test="${!empty status.value && status.value == false}"> selected="selected" </c:if>><spring:message code="jipat.fail"/></option>
						</form:select>
					</li>
					<li>
						<form:label path="pat.safe"><spring:message code="jipat.springtouse"/></form:label>
						<form:select path="pat.safe">
							<option value="true" <c:if test="${!empty status.value && status.value == true}"> selected="selected" </c:if>><spring:message code="jipat.pass"/></option>
							<option value="false" <c:if test="${!empty status.value && status.value == false}"> selected="selected" </c:if>><spring:message code="jipat.fail"/></option>
						</form:select>
					</li>
					<li>
						<form:label path="pat.bondingOk"><spring:message code="jipat.bonding"/></form:label>
						<form:select id="pat.bondingOk" path="pat.bondingOk">
							<option value="true" <c:if test="${!empty status.value && status.value == true}"> selected="selected" </c:if>><spring:message code="jipat.pass"/></option>
							<option value="false" <c:if test="${!empty status.value && status.value == false}"> selected="selected" </c:if>><spring:message code="jipat.fail"/></option>
							<option value="" <c:if test="${!empty status.value}"> selected="selected" </c:if>>N/A</option>
						</form:select>
						&nbsp;&nbsp;
						<form:input type="text" id="pat.bondingReading" path="pat.bondingReading" size="5" value="${form.pat.bondingReading}"/>
					</li>
					<li>
						<form:label path="pat.insulationOk"><spring:message code="jipat.insulation"/></form:label>
						<form:select path="pat.insulationOk">
							<option value="true" <c:if test="${!empty status.value && status.value == true}"> selected="selected" </c:if>><spring:message code="jipat.pass"/></option>
							<option value="false" <c:if test="${!empty status.value && status.value == false}"> selected="selected" </c:if>><spring:message code="jipat.fail"/></option>
						</form:select>
						&nbsp;&nbsp;
						<form:input type="text" id="pat.insulationReading" path="pat.insulationReading" size="5" value="${form.pat.insulationReading}"/>
					</li>
					<li>
						<form:label path="pat.testEquipment"><spring:message code="jipat.tester"/></form:label>
						<form:input type="text" id="pat.testEquipment" path="pat.testEquipment" value="${form.pat.testEquipment}"/>
					</li>
					<li>
						<form:label path="${status.expression}"><spring:message code="comments"/></form:label>
						<form:textarea id="pat.comment" path="pat.comment" rows="4" cols="50" value="${form.pat.comment}" />
					</li>
					<li>
						<label>&nbsp;</label>
						<input type="submit" value="<spring:message code='submit'/>">
					</li>
				</ol>
			</fieldset>
		</form:form>
		<!-- infobox div contains previous pats and is styled with nifty corners -->
		<div class="infobox" id="jiPAT">
			<c:choose>
				<c:when test="${form.ji.pats.size() < 1}">
				<h5><spring:message code="jipat.norecpats"/></h5>
				</c:when>
				<c:otherwise>
				<h5><spring:message code="jipat.patresults"/> (${form.ji.pats.size()})</h5>
				<div class="clear">&nbsp;</div>
				<c:forEach var="pat" items="${form.ji.pats}">
					<table class="default2">
						<tbody>
							<tr>
								<td colspan="2" scope="col"><b><spring:message code="jipat.testedby"/>:</b> ${pat.testedBy.name}</td>
								<td colspan="2" scope="col"><b><spring:message code="jipat.testedon"/>:</b> <fmt:formatDate type="date" dateStyle="SHORT" value="${pat.testDate}"/></td>
								<td colspan="2" scope="col"><b><spring:message code="jipat.testequip"/>:</b> ${pat.testEquipment}</td>
								<!--<td colspan="1" scope="col"><b>#springMessage("jipat.label"):</b> <a href="#" class="mainlink" onclick=" downloadPATLabel($form.ji.inst.plantid, '<fmt:formatDate pattern="dd.MM.yyyy" value="${pat.testDate}"/>'); return false; ">#springMessage("print")</a></td>-->
								<td colspan="1" scope="col"><b><spring:message code="jipat.label"/>:</b> 
									<a href="#" onclick=" printPATLabel(this, 'birt.label_pattest',${form.ji.inst.plantid}, '${form.ji.inst.serialno}', '<fmt:formatDate type="date" dateStyle="SHORT" value="${pat.testDate}"/>'); return false; ">
										<img src="img/icons/printer.png" width="16" height="16" class="img_marg_bot" />
									</a>
									<a href="#" class="mainlink" onclick=" downloadPATLabel(this, 'birt.label_pattest',${form.ji.inst.plantid}, '${form.ji.inst.serialno}', '<fmt:formatDate type="date" dateStyle="SHORT" value="${pat.testDate}"/>'); return false; ">
										<img src="img/icons/pdf.png" width="16" height="16" class="img_marg_bot" />
									</a>
								</td>
							</tr>
							<tr>
								<td colspan="7" scope="col"><b><spring:message code="comment"/>:</b> ${pat.comment}</td>
							</tr>
							<tr>
								<th scope="col"><spring:message code="jipat.plug"/></th>
								<th scope="col"><spring:message code="jipat.fuse"/></th>
								<th scope="col"><spring:message code="jipat.cable"/></th>
								<th scope="col"><spring:message code="jipat.case"/></th>
								<th scope="col"><spring:message code="jipat.safe"/></th>										
								<th scope="col"><spring:message code="jipat.bonding"/></th>										
								<th scope="col"><spring:message code="jipat.insulation"/></th>
							</tr>
							<tr>
								<td><t:formatPATResult result="${pat.plugOk}"/></td>
								<td><t:formatPATResult result="${pat.fuseOk}"/></td>
								<td><t:formatPATResult result="${pat.cableOk}"/></td>
								<td><t:formatPATResult result="${pat.caseOk}"/></td>
								<td><t:formatPATResult result="${pat.safe}"/></td>
								<td><t:formatPATResult result="${pat.bondingOk}"/>
				 				<c:if test="${!empty pat.bondingOk}"> - ${pat.bondingReading} </c:if></td>
								<td><t:formatPATResult result="${pat.insulationOk}"/> - ${pat.insulationReading}</td>
							</tr>
						</tbody>
					</table>
					<div class="clear">&nbsp;</div>
				</c:forEach>
				</c:otherwise>
			</c:choose>
		</div>
		<!-- end of pat infobox div -->
	</jsp:body>
</jobs:jobItemTemplate>