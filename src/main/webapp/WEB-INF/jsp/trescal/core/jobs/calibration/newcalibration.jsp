<%-- File name: /trescal/core/jobs/calibration/newcalibration.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs"%>
<%@ taglib prefix="addr" tagdir="/WEB-INF/tags/address"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image"%>
<jsp:useBean id="now" class="java.util.Date" />

<jobs:jobItemTemplate>
	<jsp:attribute name="subScript">
		<script src='script/trescal/core/jobs/calibration/NewCalibration.js'></script>
		<script>
			// populate javascript variable with current contact personid for use in onload function
			var currentPersonid = ${currentContact.personid};
			var allowuncalibratedstds = ${form.allowUncalibratedStds};
			// populate information about the calibration processes for use 
			// when switching between them and dynamically displaying options
			var calProcesses = {};
			<c:forEach var="p" items="${processes}">
				calProcesses.p${p.id} = new calProcess(${p.uploadBeforeIssue}, ${p.issueImmediately}, ${p.quickSign}, '${p.toolLink}');
			</c:forEach>
			var readyForCal = ${viewWrapper.readyForCalibration};
		</script>
	</jsp:attribute>
	<jsp:attribute name="warnings">
		<!-- error section displayed when form submission fails -->
		<c:set var="formHasError" value="${false}"/>
		<c:set var="formIssueNow" value="${false}"/>
		<spring:bind path="form.*">
			<c:if test="${status.error}">
				<c:set var="formHasError" value="${true}"/>
				<c:set var="formIssueNow" value="${form.issueNow}"/>
				<div class="warningBox1">
					<div class="warningBox2">
						<div class="warningBox3">
							<h5 class="center-0 attention"><spring:message code="newcalibration.errors"/>:</h5>
							<c:forEach var="error" items="${status.errorMessages}">
								<div class="center attention">${error}</div>
							</c:forEach>
						</div>
					</div>
				</div>
			</c:if>
		</spring:bind>
	</jsp:attribute>
	<jsp:body>
		<!-- infobox is styled with nifty corners -->
		<div class="infobox">
			<c:choose>
				<c:when test="${viewWrapper.readyForCalibration}">
					<div class="warningBox1 hid">
						<div class="warningBox2">
							<div class="warningBox3 center-0"><spring:message code="newcalibration.warning3"/></div>
						</div>
					</div>
					<div class="successBox1 hid">
						<div class="successBox2">
							<div class="successBox3 center-0">
								<spring:message code="newcalibration.successbox3"/>
								&nbsp;<span class="bold" id="levelText"></span>&nbsp;
								<spring:message code="newcalibration.successbox4"/>
							</div>
						</div>
					</div>
					<span id="jobItemId" class="hid">${form.ji.jobItemId}</span>
					<form:form modelAttribute="form" id="newcalibrationform" method="post" action="" enctype="multipart/form-data" onsubmit="return checkOnSubmit(this);">
						<c:set var="showInstructions" value="${calReqs != null}"/>
						<c:forEach var="i" items="${form.ji.job.relatedInstructionLinks}">
							<c:if test="${i.instruction.instructiontype == 'CALIBRATION' or i.instruction.instructiontype == 'REPAIRANDADJUSTMENT'}">
								<c:set var="showInstructions" value="${true}"/>
							</c:if>
						</c:forEach>
						<c:forEach var="i" items="${form.contractInstructions}">
							<c:if test="${i.type == 'CALIBRATION' or i.type == 'REPAIRANDADJUSTMENT'}">
								<c:set var="showInstructions" value="${true}"/>
							</c:if>
						</c:forEach>
						<c:if test="${not empty form.ji.nextWorkReq.workRequirement.requirement}">
							<c:set var="showInstructions" value="${true}"/>
						</c:if>
						<c:choose>
							<c:when test="${showInstructions}">
								<div class="messageBox1">
									<div class="messageBox2">
										<div class="messageBox3">
											<c:forEach var="i" items="${form.ji.job.relatedInstructionLinks}">
												<c:if test="${i.instruction.instructiontype == 'CALIBRATION' or i.instruction.instructiontype == 'REPAIRANDADJUSTMENT'}">
													<c:set var="isCompanyOrSubdiv" value="${i.instructionEntity == 'COMPANY' || i.instructionEntity == 'SUBDIV'}" />
													<c:choose>
														<c:when test="${isCompanyOrSubdiv && i.subdivInstruction }">
															<image:businessSubdivisionSpecific/>
														</c:when>
														<c:otherwise>
															<image:businessCompanySpecific/>
														</c:otherwise>
													</c:choose>
													<c:if test="${isCompanyOrSubdiv && i.subdivInstruction }">
														<strong>[${i.businessSubdiv.subname}]</strong> 
													</c:if>
													<c:choose>
														<c:when test="${i.instructionEntity == 'CONTACT'}">
															<b>${i.entity.name}: </b>
														</c:when>
														<c:when test="${i.instructionEntity == 'SUBDIV'}">
															<b>${i.entity.subname}: </b>
														</c:when>
														<c:when test="${i.instructionEntity == 'COMPANY'}">
															<b>${i.entity.coname}: </b>
														</c:when>
													</c:choose>
													${i.instruction.instruction}<br/><br/>
												</c:if>
											</c:forEach>
											<c:forEach var="i" items="${form.contractInstructions}">
												<c:if test="${i.type == 'CALIBRATION' or i.type == 'REPAIRANDADJUSTMENT'}">
													
													<strong>${i.linkedToType.defaultName}</strong> 
													
													<b> (${i.linkedToName}): </b>
										
													${i.instruction}<br/><br/>
												</c:if>
											</c:forEach>
											<c:if test="${calReqs != null}">
												<span style="color: red;"><c:out value="${calReqString}" /></span>
												<br/><br/>
											</c:if>
											<c:if test="${not empty form.ji.nextWorkReq.workRequirement.requirement}">
												<spring:message code="workrequirement.text" /><c:out value=" : "/>
												<span style="color: red;">${form.ji.nextWorkReq.workRequirement.requirement}</span>
												<br/><br/>
											</c:if>
											<span class="float-right"><i><spring:message code="newcalibration.confirmation"/></i>&nbsp;&nbsp;
												<form:checkbox path="infoAcknowledged"/>
											</span>
											<div class="clear"></div>
										</div>
									</div>
								</div>
							</c:when>
							<c:otherwise>
								<form:hidden path="infoAcknowledged" value="${true}"/>
							</c:otherwise>
						</c:choose>
						<!-- hidden input used to indicate that the form has been submitted with an error -->
						<input type="hidden" id="formHasError" value="${formHasError}"/>
						<input type="hidden" id="formIssueNow" value="${formIssueNow}"/>
						<!-- only allow starting multiple calibrations if system property is set to true (cwms.config.calibrations.startmultiple) -->
						<c:if test="${startMultipleCalibrations && optionalMultiStartItems != null && optionalMultiStartItems.size() > 0}">
							<div class="successBox1">
								<div class="successBox2">
									<div class="successBox3">													
										<h5 class="float-left width50">
											<c:out value="${optionalMultiStartItems.size()} "/>
											<c:choose>
												<c:when test="${optionalMultiStartItems.size() == 1}">
													<spring:message code="multistart.item"/>
												</c:when>
												<c:otherwise>
													<spring:message code="newcalibration.items"/>
												</c:otherwise>
											</c:choose>
											(<a href="" class="mainlink" onclick="event.preventDefault(); $j('div.optionalMultiStart').slideDown('slow');">
												&nbsp;<spring:message code="newcalibration.view"/>&nbsp;
											</a>)
											<spring:message code="newcalibration.sameproc"/>
										</h5>
										<input type="button" id="optionalMultiStartButton" value="<spring:message code='newcalibration.startmultical'/>" onclick="event.preventDefault(); this.disabled = true; redirectToMultiStart(${form.ji.jobItemId}, ${form.ji.job.jobid});"/>
										<div class="optionalMultiStart marg-top hid">
											<table class="default4" summary="">
												<thead>
													<tr>
														<th class=""><spring:message code="multistart.item"/></th>
														<th class=""><spring:message code="calsearchresults.instrument"/></th>
														<th class=""><spring:message code="calsearch.capability"/></th>
														<th class=""><spring:message code="calsearch.status"/></th>
													</tr>
												</thead>
												<tbody>
													<c:forEach var="item" items="${optionalMultiStartItems}">
														<tr>
															<td>${item.itemNo}</td>
															<td>${item.inst.definitiveInstrument}</td>
															<td>
																<c:forEach var="p" items="${procs}">
																	<c:if test="${form.procId == p.id}">
																		${p.reference}
																	</c:if>
																</c:forEach>
															</td>
															<td>${item.state.description}</td>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</c:if>
						<fieldset id="newcalibration">
							<legend><spring:message code="newcalibration.newcal"/></legend>
							<ol>
								<li>
									<label><spring:message code="completecalibration.calfor"/>:</label>
									<span>
										<input type="hidden" name="itemsToCalibrate" value="${form.ji.jobItemId}" />
										<!-- marker used to scroll to when page loads -->													
										Item ${form.ji.itemNo} on Job ${form.ji.job.jobno}
									</span>
								</li>
								<c:choose>
									<c:when test="${form.ji.job.items.size() == 1}">
										<form:hidden path="groupCal" value="false" />
									</c:when>
									<c:otherwise>
										<li>
											<label><spring:message code="newcalibration.groupcal"/>:</label>
											<div>
												<spring:message code="newcalibration.groupcalwithitems"/>:
												<form:checkbox path="groupCal" onclick="toggleGroupCal(this);"/>
												<c:choose>
													<c:when test="${form.ji.group != null && form.ji.group.calibrationGroup != null}">
														<c:set var="gciView" value="vis"/>
													</c:when>
													<c:otherwise>
														<c:set var="gciView" value="hid"/>
													</c:otherwise>
												</c:choose>
												<div id="groupCalItems" class="${gciView}">
													<div class="clear"></div>
													<c:forEach var="item" items="${form.ji.job.items}">
														<c:choose>
															<c:when test="${item.itemNo == form.ji.itemNo}">
																<!-- also show the instrument being calibrated in the list to save confusion -->
																<input type="checkbox" checked="checked" disabled="disabled" />
																${item.itemNo} - <cwms:showmodel instrumentmodel="${item.inst.model}"/>
																&nbsp;&nbsp;&nbsp;
																<c:set var="statusClass" value="color: red;"/>
																<c:forEach var="sgl" items="${item.state.groupLinks}">
																	<c:if test="${sgl.group.keyName == 'awaitingcalibration'}">
																		<c:set var="statusClass" value="color: green;"/>
																	</c:if>
																</c:forEach>
																(<span style="${statusClass}">${item.state.description}</span>)
																<br/>
															</c:when>
															<c:otherwise>
																<c:set var="itemChecked" value="${false}"/>
																<c:forEach var="link" items="${form.cal.links}">
																	<c:if test="${link.ji.jobItemId == item.jobItemId}">
																		<c:set var="itemChecked" value="${true}"/>
																	</c:if>
																</c:forEach>
																<c:set var="selectable" value="${true}"/>
																<c:set var="itemReadyForCal" value="${false}"/>
																<c:set var="statusClass" value="color: red;"/>
																<c:set var="sameProcedure" value="${false}"/>
																<c:set var="wrProcId" value="0" />
																<c:forEach var="sgl" items="${item.state.groupLinks}">
																	<c:if test="${sgl.group == 'AWAITINGCALIBRATION'}">
																		<c:set var="itemReadyForCal" value="${true}"/>
																	</c:if>
																</c:forEach>
																<c:choose>
																	<c:when test="${not empty item.nextWorkReq.workRequirement.capability}">
																		<!-- Expected that there will be at most one work requirement procedure in actual use -->
																		<c:set var="wrProcId" value="${item.nextWorkReq.workRequirement.capability.id}" />
																		<c:if test="${form.procId == wrProcId}">
																			<c:set var="sameProcedure" value="${true}" />
																		</c:if>
																	</c:when>
																	<c:when test="${not empty item.capability}">
																		<c:if test="${form.procId == item.capability.id}">
																			<c:set var="sameProcedure" value="${true}" />
																		</c:if>
																	</c:when>
																</c:choose>
																
																<c:if test="${itemReadyForCal || item.inst.model.modelType.modules && item.state.isStatus()}">
																	<c:if test="${sameProcedure}">
																		<c:set var="statusClass" value="color: green;"/>
																		<c:set var="selectable" value="${true}"/>
																	</c:if>
																</c:if>
																<input name="itemsToCalibrate" value="${item.jobItemId}" type="checkbox" <c:if test="${itemChecked}">checked="checked"</c:if> <c:if test="${not selectable}">disabled</c:if>/>
																${item.itemNo} - <cwms:showmodel instrumentmodel="${item.inst.model}"/>
																&nbsp;&nbsp;&nbsp;
																(<span style="${statusClass}">
																	${item.state.description}
																	Test A${form.procId}A --- A${item.capability.id}A --- ${form.procId == item.capability.id}
																	
																	<c:if test="${not sameProcedure}">
																		- <spring:message code="newcalibration.otherprocedure"/>(${form.procId} - ${item.capability.id} - ${wrProc.id})
																	</c:if>
																</span>)
																<br/>
															</c:otherwise>
														</c:choose>
													</c:forEach>
												</div>
											</div>
										</li>
									</c:otherwise>
								</c:choose>
								<li>
									<form:label path="calDate"><spring:message code="newbatchcalibration.caldate"/>:</form:label>
									<form:input path="calDate" type="date" max="${cwms:isoDate(now)}"/> 
									<c:if test="${not empty form.calDateFromReservedCertificate}">
										<span class="attention">(<spring:message code="newcalibration.reservedcertificate" arguments="${form.calDateFromReservedCertificate}"/>)</span>
									</c:if>
									<form:hidden path="calDateFromReservedCertificate" />
									<span class="attention"><form:errors path="calDate"/></span>
								</li>
								<li>
									<form:label path="cal.calClass"><spring:message code="completecalibration.lblfoundleft"/>:</form:label>
									<form:select path="cal.calClass" items="${classes}" itemLabel="desc"/>
									<span class="attention"><form:errors path="cal.calClass"/></span>
								</li>
								<li>
									<form:label path="procId"><spring:message code="multistart.procstands"/>:</form:label>
										<c:set var="defProcId" value=""/>
										<c:forEach var="p" items="${procs}">
											<c:if test="${form.procId == p.id}">
												${p.reference}
												<c:set var="defProcId" value="${p.id}"/>
											</c:if>
										</c:forEach>
										<c:if test="${defProcId == ''}">
											<span class="attention"><spring:message code="newcalibration.attention"/></span>
										</c:if>
										<input type="hidden" name="defaultProcId" value="${defProcId}"/>
									<span class="attention"><form:errors path="procId"/></span>
									<!-- clear floats and restore page flow -->
									<div class="clear"></div>
								</li>
								<li>
									<form:label path="calProcessId"><spring:message code="newcalibration.selcalproc"/>:</form:label>
									<form:select path="calProcessId" id="processSelect">
										<form:option value="0"> -- <spring:message code="newcalibration.plselproc"/> -- </form:option>
										<form:options items="${processes}" itemValue="id" itemLabel="process"/>
									</form:select>
									<span class="attention"><form:errors path="calProcessId"/></span>
								</li>
								<li id="workInstructionListItem" class="hid">
									<form:label path="workInstructionId"><spring:message code="newcalibration.selwrokinstr"/>:</form:label>
									<form:select path="workInstructionId" id="workInstructionSelect"></form:select>
									<span class="attention"><form:errors path="workInstructionId"/></span>
									<!-- clear floats and restore page flow -->
									<div class="clear"></div>
								</li>
								<li>
									<label><spring:message code="newcalibration.caltype"/>:</label>
									<span>
										<cwms:besttranslation translations="${form.ji.calType.serviceType.shortnameTranslation}"/>
										<form:hidden path="ji.calType.calTypeId" id="calTypeId"/>
									</span>
								</li>
								<li>
									<label class="labelhead"><spring:message code="newcalibration.plselcalstands"/>:</label>
									<span>&nbsp;</span>
								</li>
							</ol>
						</fieldset>
						<!-- this table displays all default procedure standards for the selected procedure -->
						<table class="procedureStds default4" summary="<spring:message code='newcalibration.tabledisdefstands'/>">
							<thead>
								<tr>
									<td colspan="6"><spring:message code="calsearchresults.standards"/></td>
								</tr>
								<tr>
									<th class="stduse" scope="col"><spring:message code="multistart.use"/></th>
									<th class="stdbar" scope="col"><spring:message code="barcode"/></th>
									<th class="stdinst" scope="col"><spring:message code="calsearchresults.instrument"/></th>
									<th class="stdserial" scope="col"><spring:message code="serialno"/></th>
									<th class="stdplant" scope="col"><spring:message code="plantno"/></th>
									<th class="stdenf" scope="col"><spring:message code="multistart.enforce"/></th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<td colspan="6">
										<a href="#" class="domthickbox mainlink" onclick="tb_show('Add Standards', 'TB_dom?width=800&height=400', '', thickboxAdditionalStandardsContent()); return false; " title="">
											<spring:message code="multistart.addaddstand"/>
										</a>
									</td>
								</tr>
							</tfoot>
							<tbody id="pStds">
								<tr>
									<th colspan="6"><spring:message code="multistart.procstands"/> (<span id="pStdsSize">${procstandards.size()}</span>)</th>
								</tr>
								<c:choose>
									<c:when test="${procstandards.size() == 0}">
										<tr>
											<td colspan="6" class="bold center">
												<spring:message code="multistart.nodefstands"/>
											</td>
										</tr>
									</c:when>
									<c:otherwise>
										<c:forEach var="std" items="${procstandards}">
											<c:set var="instrument" value="${std.instrument}"/>
											<c:set var="checked" value=""/>
											<c:choose>
												<c:when test="${instrument.isOutOfCalibration()}">
													<spring:message var="outOfCal" code="multistart.outofcal"/>
													<c:set var="rowcolor" value='highlight'/>
												</c:when>
												<c:otherwise>
													<c:set var="outOfCal" value=""/>
													<c:set var="rowcolor" value=""/>
												</c:otherwise>
											</c:choose>
											<c:if test="${form.stdIds != null && form.stdIds.contains(instrument.plantid)}">
												<c:set var="checked" value='checked="checked"'/>
											</c:if>
											<tr id="std${instrument.plantid}" class="${rowcolor}">
												<td class="stduse">
													<input type="checkbox" class="standardId" name="instrums" value="${instrument.plantid}" onclick="toggleStd(this);" ${checked}/>
													<input type="hidden" name="procStdsOnScreen" value="${std.id}"/>
												</td>
												<td class="stdbar">
													<links:instrumentLinkDWRInfo instrument="${instrument}" rowcount="0" displayBarcode="true" displayName="false" displayCalTimescale="true" caltypeid="${form.ji.calType.calTypeId}"/>
												</td>
												<td class="stdinst">
													<links:showInstrumentLink instrument="${instrument}"/>
													<c:if test="${outOfCal != ''}">
														<br/><span class="attention">${outOfCal}</span>
													</c:if>
												</td>
												<td class="stdserial">${instrument.serialno}</td>
												<td class="stdplant">${instrument.plantno}</td>
												<td class="stdenf">
													<c:choose>
														<c:when test="">
															<img src="img/icons/bullet-tick.png" width="10" height="10" alt="<spring:message code='multistart.enforceuse'/>" title="<spring:message code='multistart.enforceuse'/>"/>
														</c:when>
														<c:otherwise>
															<img src="img/icons/bullet-cross.png" width="10" height="10" alt="<spring:message code='multistart.donotenforce'/>" title="<spring:message code='multistart.donotenforce'/>"/>
														</c:otherwise>
													</c:choose>
												</td>
											</tr>
										</c:forEach>
									</c:otherwise>
								</c:choose>
							</tbody>
							<tbody id="wiStds" class="hid">
								<tr>
									<th colspan="6"><spring:message code="multistart.workinststand"/> (<span id="wiStdsSize">${wistandards.size()}</span>)</th>
								</tr>
								<c:choose>
									<c:when test="${wistandards.size() == 0}">
										<tr>
											<td colspan="6" class="bold center">
												<spring:message code="multistart.nodefstandforinstr"/>
											</td>
										</tr>
									</c:when>
									<c:otherwise>
										<c:forEach var="std" items="${wistandards}">
											<c:set var="instrument" value="${std.instrument}"/>
											<c:set var="checked" value=""/>
											<c:choose>
												<c:when test="${instrument.isOutOfCalibration()}">
													<spring:message var="outOfCal" code="multistart.outofcal"/>
													<c:set var="rowcolor" value='highlight'/>
												</c:when>
												<c:otherwise>
													<c:set var="outOfCal" value=""/>
													<c:set var="rowcolor" value=""/>
												</c:otherwise>
											</c:choose>
											<c:if test="${form.stdIds != null && form.stdIds.contains(instrument.plantid)}">
												<c:set var="checked" value='checked="checked"'/>
											</c:if>
											<tr id="std${instrument.plantid}" class="${rowcolor}">
												<td class="center">
													<input type="checkbox" class="standardId" name="instrums" value="${instrument.plantid}" onclick="toggleStd(this);" ${checked}/>
													<input type="hidden" name="procStdsOnScreen" value="${std.id}"/>
												</td>
												<td class="stdbar">
													<links:instrumentLinkDWRInfo instrument="${instrument}" rowcount="0" displayBarcode="true" displayName="false" displayCalTimescale="true" caltypeid="${form.ji.calType.calTypeId}"/>
												</td>
												<td>
													<links:showInstrumentLink instrument="${instrument}"/>
													<c:if test="${outOfCal != ''}">
														<br/><span class="attention">${outOfCal}</span>
													</c:if>
												</td>
												<td>${instrument.serialno}</td>
												<td>${instrument.plantno}</td>
												<td class="center">
													<c:choose>
														<c:when test="${std.enforceUse}">
															<img src="img/icons/bullet-tick.png" width="10" height="10" alt="<spring:message code='multistart.enforceuse'/>" title="<spring:message code='multistart.enforceuse'/>"/>
														</c:when>
														<c:otherwise>
															<img src="img/icons/bullet-cross.png" width="10" height="10" alt="<spring:message code='multistart.donotenforce'/>" title="<spring:message code='multistart.donotenforce'/>"/>
														</c:otherwise>
													</c:choose>
												</td>
											</tr>
										</c:forEach>
									</c:otherwise>
								</c:choose>
							</tbody>
						</table>
						<!-- end of table to display all default procedure standards -->
						<c:choose>
							<c:when test="${formHasError}">
								<!-- this table displays additional standards that have been added by the user in the event that an error
								occurs when the form has been submitted. Only shown if there are additional standards to display -->														
								<input type="hidden" name="instrums" value="0"/>
								<table class="default2 #if ($form.errorAdditionalStds.size() < 1) hid #end" id="additionalstds" summary="<spring:message code='multistart.alladdstand'/>">
									<thead>
										<tr>
											<td colspan="5"><spring:message code="multistart.addstand"/> (<span class="addstdsSizeSpan">$form.errorAdditionalStds.size()</span>)</td>
										</tr>
										<tr>
											<th class="addstduse" scope="col"><spring:message code="multistart.use"/></th>
											<th class="addstdbar" scope="col"><spring:message code="barcode"/></th>
											<th class="addstdinst" scope="col"><spring:message code="calsearchresults.instrument"/></th>
											<th class="addstdserial" scope="col"><spring:message code="serialno"/></th>
											<th class="addstdplant" scope="col"><spring:message code="plantno"/></th>
										</tr>
									</thead>
									<tfoot>
										<tr>
											<td colspan="5">
												&nbsp;
											</td>
										</tr>
									</tfoot>
									<tbody>
										<c:if test="${form.errorAdditionalStds.size() > 0}">
											<c:forEach var="instrument" items="${form.errorAdditionalStds}">
												<c:set var="alreadyOnScreen" value="${false}"/>
												<c:forEach var="test" items="${form.errorProcStds}">
													<c:if test="${test.instrument.plantid == instrument.plantid}">
														<c:set var="alreadyOnScreen" value="${true}"/>
													</c:if>
												</c:forEach>
												<c:if test="${!alreadyOnScreen}">
													<c:set var="checked" value=""/>
													<c:choose>
														<c:when test="${instrument.isOutOfCalibration()}">
															<spring:message var="outOfCal" code="multistart.outofcal"/>
															<c:set var="rowcolor" value='highlight'/>
														</c:when>
														<c:otherwise>
															<c:set var="outOfCal" value=""/>
															<c:set var="rowcolor" value=""/>
														</c:otherwise>
													</c:choose>
													<c:forEach var="i" items="${form.instrums}">
														<c:if test="${i == instrument.plantid}">
															<c:set var="checked" value='checked="checked"'/>
														</c:if>
													</c:forEach>
													<tr id="addstd${instrument.plantid}" class="${rowcolor}">
														<td class="center">
															<input type="checkbox" class="standardId" name="instrums" value="${instrument.plantid}" ${checked}/>
															<input type="hidden" name="addStdsOnScreen" value="${instrument.plantid}"/>
														</td>
														<td class="addstdbar">
															<links:instrumentLinkDWRInfo instrument="${instrument}" rowcount="0" displayBarcode="true" displayName="false" displayCalTimescale="true" caltypeid="${form.ji.calType.calTypeId}"/>
														</td>
														<td>
															<links:showInstrumentLink instrument="${instrument}"/>
															<c:if test="${outOfCal != ''}">
																<br/><span class="attention">${outOfCal}</span>
															</c:if>
														</td>
														<td>${instrument.serialno}</td>
														<td>${instrument.plantno}</td>
													</tr>
												</c:if>
											</c:forEach>
										</c:if>
									</tbody>
								</table>
								<!-- end of table to display additional standards in the event of an error -->
								</c:when>
								<c:otherwise>
									<!-- this table displays any additional standards that the user may wish to add to this procedure,
										only displayed when the user chooses to add an additional standard -->							
								<table class="default2 hid" id="additionalstds" summary="This table displays all additional standards">
									<thead>
										<tr>
											<td colspan="5"><spring:message code="multistart.addstand"/> (<span class="addstdsSizeSpan">0</span>)</td>
										</tr>
										<tr>
											<th class="addstduse" scope="col"><spring:message code="multistart.use"/></th>
											<th class="addstdbar" scope="col"><spring:message code="barcode"/></th>
											<th class="addstdinst" scope="col"><spring:message code="calsearchresults.instrument"/></th>
											<th class="addstdserial" scope="col"><spring:message code="serialno"/></th>
											<th class="addstdplant" scope="col"><spring:message code="plantno"/></th>
										</tr>
									</thead>
									<tfoot>
										<tr>
											<td colspan="5">
												&nbsp;
											</td>
										</tr>
									</tfoot>
									<tbody>
										
									</tbody>
								</table>
								<!-- end of table created for additional procedure standards -->
							</c:otherwise>
						</c:choose>
						<form:hidden path="startFromTool" id="startFromTool"/>
					</form:form>
					<!-- submit button div -->	
					<div class="center">
						<input type="button" id="submit" name="submit" value="<spring:message code='newcalibration.continue'/>" onclick=" $j(this).attr('disabled', 'disabled'); submitForm(); "/>
					</div>
					<!-- end of submit button div -->
				</c:when>
				<c:otherwise>
					<div class="warningBox1">
						<div class="warningBox2">
							<div class="warningBox3">
								<h5 class="center-0 attention"><spring:message code="newcalibration.itemreadytocal"/></h5>
							</div>
						</div>
					</div>
				</c:otherwise>
			</c:choose>
		</div>
		<!-- end of infobox div -->
	</jsp:body>
</jobs:jobItemTemplate>