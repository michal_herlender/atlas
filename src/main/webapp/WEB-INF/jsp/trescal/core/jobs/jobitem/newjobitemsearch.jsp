<%-- File name: /trescal/core/jobs/jobitem/newjobitemsearch.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="newjobitemsearch.newjobitemsearch" /></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
        <script type="module" src="script/components/cwms-jobitem-search-form/main.js"></script>
        <script type="module" src="script/components/cwms-jobitem-templating-properties/cwms-jobitem-templating-properties.js"></script>
		<script src="script/trescal/core/jobs/jobitem/NewJobItemSearch.js"></script>
	</jsp:attribute>
	<jsp:body>
		<form:form action="" modelAttribute="form" id="newjobitemsearchform" onsubmit="showUnLoadWarning = false; maintainBasket(this.id);">
			<!-- infobox div containing form elements for searching new job items subdivid-->
			<div class="infobox">
				<!-- add new model link -->
				<a href="" onclick="event.preventDefault(); window.open('editmodel.htm');" class="mainlink-float">
					<spring:message code="newjobitemsearch.addmodel"/>
				</a>
<!-- 				add new instrument link -->				
				<a href="#" onclick="event.preventDefault(); addItemsFromPrebooking('Add',${job.jobid});" class="mainlink-float">
				<spring:message code="newjobitemsearch.additemsfromPrebooking"/></a>
<!-- 				clear floats and restore page flow -->
				<div class="clear"></div>
				<fieldset id="searchfields">
					<legend><spring:message code="newjobitemsearch.legend"/>
						(<a class="mainlink" href="viewjob.htm?jobid=${job.jobid}">
							<spring:message code="newjobitemsearch.backtojob" arguments="${job.jobno}"/>
						</a>)
					</legend>
					
                    <cwms-jobitem-search-form jobId="${job.jobid}" coid="${job.con.sub.comp.coid}" groupId="${job.con.sub.comp.companyGroup.id}"></cwms-jobitem-search-form>
					
				</fieldset>
				
					<cwms-jobitem-templating-properties seletedTurnaround="${job.defaultTurn}"
					jobId="${job.jobid}"
					 jobType="${job.type.description}" locations="${cwms:collectionToJson(bookingInAddresses)}" 
					 selectedCalTypeId="${job.defaultCalType.calTypeId}"
					 calTypes="${cwms:collectionToJson(activeCalTypes)}"
					 bookingAddressId="${form.bookingInAddrId}"
					 defaultServiceTypeUsage="${defaultServiceTypeUsage}"
					 ></cwms-jobitem-templating-properties>
				<cwms-linked-quotation-search 
				companyId="${job.con.sub.comp.coid}"
				jobId="${job.jobid}" linkedQuotes="${cwms:collectionToJson(linkedQuotes)}"></cwms-linked-quotation-search>
				
			</div>
			<!-- end of infobox div -->
		</form:form>
		<!-- wrapper div to grab all tables for column sizing -->
		<div id="newjobitemsearch">
				<cwms-results-and-basket></cwms-results-and-basket>
			
		</div>
		<!-- end of wrapper div to grab tables -->
	</jsp:body>
</t:crocodileTemplate>