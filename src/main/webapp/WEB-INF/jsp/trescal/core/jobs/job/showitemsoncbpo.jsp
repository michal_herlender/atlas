<%-- File name: /trescal/core/jobs/job/showitemsoncbpo.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div id="createNewBPOBoxy" class="overflow">
	<fieldset>
		<legend>${titleAction}</legend>
		<ol>
			<li>
				<label><spring:message code="viewjob.bponum"/>:</label>
				<input type="text" id="bponumber" name="bponumber" value="${bpoNo}" tabindex="100"/>
			</li>
			<li>
				<label><spring:message code="tools.datefrom"/>:</label>
				<input type="text" id="bpofrom" name="bpofrom" value="${bpoFrom}" tabindex="100"/>
			</li>
			<li>
				<label><spring:message code="tools.dateto"/>:</label>
				<input type="text" id="bpoto" name="bpoto" value="${bpoTo}" tabindex="100"/>
			</li>
			<li>
				<label><spring:message code="bpo.limitamount"/>:</label>
				<input type="text" name="bpolimit" value="${bpoLimit}" tabindex="100"/>
			</li>
			<li>
				<label><spring:message code="comment"/>:</label>
				<textarea name="bpocomment" cols="50" rows="4" tabindex="101">${bpoComment}</textarea>
			</li>
			<li>
				<label><spring:message code="jobitems"/>:</label>
				<div id="bpoItemsToAdd" class="float-left">
					<c:choose>
						<c:when test="${jobItems != null && jobItems.size() > 0}">
							<div>
								<input type="checkbox" onclick="selectAll(this.checked, 'bpoItemsToAdd');"/>
								<spring:message code="all"/>
							</div>
							<c:forEach var="jobItem" items="${jobItems}">
								<div>
									<input type="checkbox" value="${jobItem.id}" name="items" <c:if test="${jobItem.onClientPO}">checked</c:if>/>
									${jobItem.itemNo} -
									${jobItem.modelName}
									<c:if test="${jobItem.onAnyClientPO}">
										<div class="inline attention">- <spring:message code="showitemsoncpo.onpo"></spring:message></div>
									</c:if>
								</div>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<div class="padtop"><spring:message code="showitemsoncpo.noitemsonjob"/></div>
						</c:otherwise>
					</c:choose>
				</div>
				<div class="clear"></div>
			</li>
			<li id="bpoSubmit">
				<label>&nbsp;</label>
				<c:choose>
					<c:when test="${bpoId == null || bpoId == 0}">
						<spring:message code="showitemsoncbpo.createbpo" var="createBPO"/>
						<input type="button" onclick="this.disabled = true; createBlanketPurchOrd(${jobId}); return false;" value="${createBPO}" tabindex="102"/>
					</c:when>
					<c:otherwise>
						<spring:message code="showitemsoncbpo.savebpo" var="saveBPO"/>
						<input type="button" onclick="this.disabled = true; editBlanketPurchOrd(${bpoId}, ${jobId}, '${jobNo}'); return false;" value="${saveBPO}" tabindex="102"/>
					</c:otherwise>
				</c:choose>
			</li>
		</ol>
	</fieldset>
</div>