<%-- File name: /trescal/core/jobs/job/accessoriesanddefects.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<style>
	.curhand { cursor: hand; }
</style>
	
<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code='accessoriesanddefects.headtext'/>&nbsp;${model.job.jobno}</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script>
			<c:choose>
				<c:when test="${model.selectedItem != null}">
					var selectedItemId = ${model.selectedItem.jobItemId};
					var selectedGroupId = 0;
					var selectedItemNo = ${model.selectedItem.itemNo};
				</c:when>
				<c:when test="${model.selectedGroup != null}">
					var selectedItemId = 0;
					var selectedGroupId = ${model.selectedGroup.id};
				</c:when>
				<c:otherwise>
					var selectedItemId = 0;
					var selectedGroupId = 0;
				</c:otherwise>
			</c:choose>
			var accessoryArray = new Array();
			<c:forEach var="accessory" items="${accessories}">
				accessoryArray.push({value:'${accessory.desc}', name:'${accessory.desc}'});
			</c:forEach>
		</script>
		<script src='script/trescal/core/jobs/job/AccessoriesAndDefects.js'></script>
	</jsp:attribute>
	<jsp:body>
		<c:set var="job" value="${model.job}"/>
		<div class="infobox">
			<a href="viewjob.htm?jobid=${job.jobid}" class="mainlink">
				<spring:message code="accessoriesanddefects.backjob"/>&nbsp;${job.jobno}
			</a>
			<!-- clear floats and restore page flow -->
			<div class="clear">&nbsp;</div>
			<div class="displaycolumn-60">
				<table class="default4 accessoryJobItems" summary="<spring:message code='accessoriesanddefects.summary'/>">
					<thead>
						<tr>
							<td colspan="5">
								<spring:message code="accessoriesanddefects.jobitems"/>
								(${job.items.size()})
							</td>
						</tr>
						<tr>
							<td colspan="5">
								<spring:message code="accessoriesanddefects.groups"/>
								(<span id="groupSize">${job.groups.size()}</span>)&nbsp;&nbsp;
								<a href="#" class="mainlink" onclick="createNewGroup(${job.jobid}); return false;">
									<spring:message code="accessoriesanddefects.creategroup"/>
								</a>
							</td>
						</tr>
						<tr>
							<td colspan="5">
								<span id="grouplink$0"></span>
								<c:forEach var="group" items="${job.groups}">
									<span id="grouplink${group.id}">
										&nbsp;&nbsp;
										<a href="#" class="mainlink" onclick="changeToGroup(${group.id}); return false;">
											${group.id}
										</a>
										(<span id="accSize${group.id}">${group.accessories.size()}</span>)
									</span>
								</c:forEach>
							</td>
						</tr>
						<tr>
							<th class="item"><spring:message code="accessoriesanddefects.item"/></th>
							<th class="inst"><spring:message code="accessoriesanddefects.instrument"/></th>
							<th class="plantno"><spring:message code="plantno"/></th>
							<th class="serial"><spring:message code="serialno"/></th>
							<th class="group"><spring:message code="accessoriesanddefects.group"/></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="5">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody>
						<c:forEach var="ji" items="${job.items}">
							<tr>
								<td>
									${ji.itemNo}
									(<span id="accSize${ji.jobItemId}">${ji.accessories.size()}</span>)
								</td>
								<td>
									<a class="mainlink" href="" onclick="event.preventDefault(); changeToItem(${ji.jobItemId}, ${ji.itemNo});">
										<cwms:showmodel instrumentmodel="${ji.inst.model}"/>
									</a>
								</td>
								<td>${ji.inst.plantno}</td>
								<td>${ji.inst.serialno}</td>
								<td>
									<select name="group" onchange="updateJobItemGroup(${ji.jobItemId}, this.value); return false;" id="selectGroup_${ji.jobItemId}">
										<option value="0"><spring:message code="accessoriesanddefects.nogroup"/></option>
										<c:forEach var="group" items="${job.groups}">
											<option value="${group.id}" ${ji.group.id == group.id ? "selected" : ""}>${group.id}</option>
										</c:forEach>
									</select>
									
									
					                <img class="curhand hid" onclick=" cascadeGroup(${ji.jobItemId}); return false; " id="cascadeGroup_${ji.jobItemId}" src="img/icons/arrow_down.png" width="10" height="16" alt="<spring:message code='calibrationpoints.imgarrowdown' />" title="<spring:message code='calibrationpoints.imgarrowdown' />" />
					              
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<div class="displaycolumn-40">
				<fieldset>
					<legend>
						<span id="accHeader"></span>
					</legend>
					<div class="marg-small infobox">
						<div id="accSpace">
							<spring:message code="accessoriesanddefects.string1"/>
						</div>
						<!-- add new accessory -->
						<table class="default4 addAccessory">
							<thead>
								<tr>
									<td colspan="3"><spring:message code="accessoriesanddefects.addaccessory"/></td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="accessory">
										<select>
											<c:forEach var="accessory" items="${accessories}">
												<option value="${accessory.id}"><cwms:besttranslation translations="${accessory.translations}"/></option>
											</c:forEach>
										</select>
									</td>
									<td class="quantity">
										<input type="number" value="1" min="1"/>
									</td>
									<td class="add">
										<a onclick="
											event.preventDefault();
											addAccessory(
												$j(this).parents('tr').find('td.accessory > select').val(),
												$j(this).parents('tr').find('td.quantity > input').val()
											);
										">
											<img src="img/icons/add.png" width="16" height="16"/>
										</a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="marg-small infobox">
						<div id="accFreeText"></div>
					</div>
				</fieldset>
			</div>
			<!-- clear floats and restore page flow -->
			<div class="clear"></div>
		</div>
	</jsp:body>
</t:crocodileTemplate>