<%-- File name: /trescal/core/jobs/jobitem/jicalloffitem.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="costs" tagdir="/WEB-INF/tags/costs"%>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs"%>
<%@ taglib prefix="addr" tagdir="/WEB-INF/tags/address"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<jobs:jobItemTemplate>
	<jsp:attribute name="subScript">
        <script type='text/javascript'
			src='script/trescal/core/jobs/jobitem/JICallOffItem.js'></script>
		<script type='text/javascript'
					src='script/trescal/core/documents/ImageGalleryFunctions.js'></script>
		<script type='text/javascript'>
			// grab scroll to parameter passed when using job item key navigation
			var scrollto = '${request.getParameter("scrollto")}';
		</script>
    </jsp:attribute>
	<jsp:body>
		<t:showErrors path="form.*" showFieldErrors="true" />
																			
		<!-- infobox is styled with nifty corners -->
		<div class="infobox">
			<c:choose>
				<c:when test="${not empty form.callOffItem || form.incompleteActivity}">
					<table class="default2" id="jobitemcalledoff" summary="<spring:message code="jicalloffitem.string1"/>">
						<thead>
							<tr>
								<c:choose>
									<c:when test="${not empty form.callOffItem}">
										<td colspan="5"><spring:message code="jicalloffitem.string2" /></td>
									</c:when>
								 	<c:when test="${form.incompleteActivity}">
										<td colspan="5"><spring:message code="jicalloffitem.string3" /></td>
									</c:when>
								</c:choose>
							</tr>
							<tr>
								<th class="all" scope="col">
									<c:choose>
										<c:when test="${not empty form.callOffItem}">
											<spring:message code="status" />
										</c:when>
										<c:when test="${form.incompleteActivity}">
											<spring:message code="jicalloffitem.currstatus" />
										</c:when>
									</c:choose>
								</th>
								<th class="itemno" scope="col">
									<spring:message code="itemno" />
								</th>
								<th class="inst" scope="col">
									<spring:message code="instrument" />
								</th>
								<th class="status" scope="col">
									<spring:message code="status" />
								</th>
								<th class="reason" scope="col">
									<c:choose>
										<c:when test="${not empty form.callOffItem}">
											<spring:message code="jicalloffitem.reason" />
										</c:when>
										<c:when test="${form.incompleteActivity}">
											<spring:message code="jiactions.viewact" />
										</c:when>
						    		</c:choose>
								</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="5">
									<c:choose>
									    <c:when test="${not empty form.callOffItem}">
									    	<cwms:securedLink permission="SUSPENDED_JOB_ITEM"  classAttr="mainlink-float" collapse="True" >
												<spring:message code="jicalloffitem.string4"/>
											</cwms:securedLink>
										</c:when>
										<c:when test="${form.incompleteActivity}">
											<a href="jiactivities.htm?jobitemid=${form.ji.jobItemId}"
												class="mainlink-float">
												<spring:message code="jicalloffitem.string5" />
											</a>
										</c:when>
							        </c:choose>													
								</td>
							</tr>
						</tfoot>
						<tbody>												
							<tr>
								<td>
									<t:showTranslationOrDefault translations="${form.ji.state.translations}" defaultLocale="${defaultlocale}" />
								</td>
								<td>${form.ji.itemNo}</td>
								<td> <cwms:showmodel instrumentmodel="${form.ji.inst.model}" /> </td>
								<td></td>
								<td>
									<c:choose>
								    <c:when test="${not empty form.callOffItem}">
										${form.callOffItem.reason}
									</c:when>
									<c:when test="${form.incompleteActivity}">
										<a href="jiactivities.htm?jobitemid=${form.ji.jobItemId}" class="mainlink">
											<spring:message code="jicalloffitem.string5" />
										</a>
								    </c:when>
							        </c:choose>
								</td>
							</tr>													
						</tbody>
					</table>
				</c:when>	
				<c:otherwise>
					<form:form method="post" modelAttribute="form">
						<table class="default2" id="jobitemcalloff" summary="<spring:message code="jicalloffitem.string6"/>">
							<thead>
								<tr>
									<td colspan="5">
										<spring:message code="calloffitems.jobcalloffcount" arguments="${form.ji.job.items.size()}" />
									</td>
								</tr>
								<tr>
									<th class="all" scope="col">
										<input type="checkbox" onclick=" selectCallOffItems(this.checked, 'items'); " />
										<spring:message code="jiactions.all" />
									</th>
									<th class="itemno" scope="col">
										<spring:message code="itemno" />
									</th>
									<th class="inst" scope="col">
										<spring:message code="instrument" />
									</th>
									<th class="status" scope="col">
										<spring:message code="status" />
									</th>
									<th class="reason" scope="col">
										<spring:message code="jicalloffitem.reason" />
									</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<td colspan="5">&nbsp;</td>
								</tr>
							</tfoot>
							<tbody id="items">
								<c:forEach var="item" items="${form.ji.job.items}" varStatus="loopStatus">
									<%-- TODO - Not happy with manual IDs in UI, would be better as enums or stategroup? --%>
									<c:choose>
									<c:when test="${selectableItems[loopStatus.index]}">
										<tr>
											<td>
												<c:set var="checked" value=""/>
												<c:if test="${item.jobItemId == form.ji.jobItemId}"> 
													<c:set var="checked" value="checked"/>
												</c:if>
												<input type="hidden" name="_${status.expression}" />
												<form:checkbox path="jobItemIds[${item.jobItemId}]" onclick=" enableCheckedTextAreas(); "
													value="${item.jobItemId}" checked="${checked}" />
											</td>
											<td>${item.itemNo}</td>
											<td>
												<cwms:showmodel instrumentmodel="${item.inst.model}" />
											</td>
											<td>
												<cwms:besttranslation translations="${item.state.translations}" />
											</td>
											<td>
												<c:set var="hide" value="vis"/>
												<c:if test="${item.jobItemId != form.ji.jobItemId}"> 
													<c:set var="hide" value="hid"/>
												</c:if>
													<form:textarea path="reasons[${item.jobItemId}]" style="width: 90%; height: 26px;"
														class="${hide}"></form:textarea>
													<a href="#" onclick=" copyReason(this); return false; "
														<c:choose>
															<c:when test="${item.jobItemId != form.ji.jobItemId}"> class="mainlink hid" </c:when>
															<c:otherwise> class="mainlink vis" </c:otherwise>
														</c:choose>>
											
														<spring:message code="jicalloffitem.copy" />
													</a>
													<form:errors path="reasons[${item.jobItemId}]" class="attention"/>
											</td>
										</tr>
									</c:when>
									<c:otherwise>
										<tr>
											<td></td>
											<td>${item.itemNo}</td>
											<td><cwms:showmodel instrumentmodel="${item.inst.model}" /></td>
											<td><cwms:besttranslation translations="${item.state.translations}" /></td>
											<td></td>
										</tr>
									</c:otherwise>
									</c:choose>
								</c:forEach>
							</tbody>
						</table>
							
						<div class="center">
							<input type="submit" value="<spring:message code="jicalloffitem.string7"/>" />
						</div>
					</form:form>
				</c:otherwise>
			</c:choose>
		</div>
		<!-- end of infobox div -->
    </jsp:body>
</jobs:jobItemTemplate>