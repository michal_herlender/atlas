<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<style>
#rcrForm label {
	width: 210px;
}

#rcrForm h5 {
	text-decoration-line: underline;
	margin-bottom: 0px;
}

.compSearchJQPlugin {
	display: inline-block;
}

.right {
	text-align: right !important;
}

#totalsTable {
	float: right;
	margin: 50px;
	margin-bottom: 10px;
	font-size: small;
}

#saveBtn {
	margin: 10px;
}

.pointer {
	cursor: pointer;
}

.row-resize {
	cursor: row-resize;
}

#operationsTable input, #componentsTable input {
	width: 65px;
}

.operationtime {
	text-align: center;
	width: 90px;
}

.operationtime input, .operationcost input, #componentsTable input {
	text-align: right;
}

.operationcost, .operationprice {
	text-align: center;
	width: 90px;
}

.dragNdropIcon {
	width: 20px;
	height: 15px;
	float: left;
	filter: invert(40%);
}

.infobox h5 {
	text-align: left;
}
#lastTpCertDateDiv {
	display : inline;
	color : darkorange;
	text-decoration: underline;
}
.hidden {
	visibility : hidden;
}


</style>
<script type="module" src="script/components/cwms-datetime/cwms-datetime.js"></script>
<script>
	/* Translations */
	 // strings var already defined in repairinspection report
	 strings['repaircompletionreport.validate'] = "<spring:message code='repaircompletionreport.validate' javaScriptEscape='true' />";
	 strings['jiactions.timespent'] = "<spring:message code='jiactions.timespent' javaScriptEscape='true' />";
	 strings['repaircompletionreport.confirmation'] = "<spring:message code='repaircompletionreport.confirmation' javaScriptEscape='true' />";

</script>
<script>
	/* global vars */
	var currencySymbol = "${not empty currencySymbol ? currencySymbol:''}";
	var actionRcrPerformed = ${actionPerformed};
	var rcrOperationStatusOptions = '<option value="">&nbsp;</option>';
	<c:forEach items="${operationStatus}" var="o">
	rcrOperationStatusOptions += "<option value='${o}'>${o.value}</option>";
	</c:forEach>

	var rcrComponentStatusOptions = '<option value="">&nbsp;</option>';
	<c:forEach items="${componentStatus}" var="c">
	rcrComponentStatusOptions += "<option value='${c}'>${c.value}</option>";
	</c:forEach>
	
</script>
<link rel="stylesheet" href="styles/jQuery/jquery.datetimepicker.css"></link>
<script src="script/thirdparty/jQuery/jquery.datetimepicker.full.min.js"></script>
<script src="script/trescal/core/jobs/repair/repaircompletionreport.js"></script>
<div class="clear">&nbsp;</div>
<!-- error section displayed when form submission fails -->
<form:errors path="rcrForm.*">
	<div class="warningBox1">
		<div class="warningBox2">
			<div class="warningBox3">
				<h5 class="center-0 attention"><spring:message code="repairinspection.validator.generalmessage"/>:</h5>
				<c:forEach var="e" items="${messages}">
					<div class="center attention"><c:out value="${e}"/></div>
				</c:forEach>
			</div>
		</div>
	</div>
	<div class="successBox1 hid" id="successMessageRcr">
		<div class="successBox2">
			<div class="successBox3">
				<div class="center green bold">
					<spring:message code='repairinspection.success1' />
				</div>
			</div>
		</div>
	</div>
</form:errors>
<!-- end error section -->
<form:form modelAttribute="rcrForm" method="POST" id="rcrForm"
	enctype="multipart/form-data">
	<form:hidden path="jobItemId" />
	<form:hidden path="rcrId" />
	<form:hidden path="rirId" />
	<form:hidden path="validated" />
	<form:hidden path="validatedOn" />
	<form:hidden path="validatedBy" />

	<fieldset>
		<legend><spring:message
				code='instructiontype.repairsandadjustment' /></legend>
	<cwms-instructions link-coid="${ rirForm.ji.job.con.sub.comp.coid }" link-subdivid="${ rirForm.ji.job.con.sub.subdivid }" link-contactid="${ rirForm.ji.job.con.personid }" instruction-types="REPAIRANDADJUSTMENT" show-mode="TABLE"></cwms-instructions>
	</fieldset>
	<!-- Completion comments START-->
	<fieldset>
		<legend>
			<span><spring:message
					code='repaircompletionreport.fieldset.completioncomments.title' /></span>
		</legend>
		<ol>
			<li><label><spring:message
						code='repaircompletionreport.internalrepaircompletionreport' />:</label>
				<form:textarea path="internalCompletionComments" rows="2" cols="130"
					disabled="${disabledModification}" /> <form:errors
					class="error max-wrap" path="internalCompletionComments" /></li>
			<li><label><spring:message
						code='repaircompletionreport.externalsupplierrepaircomments' />:</label>
				<form:textarea path="externalCompletionComments" rows="2" cols="130"
					disabled="${disabledModification}" /> <form:errors
					class="error max-wrap" path="externalCompletionComments" /></li>
		</ol>
	</fieldset>
	<!-- Completion comments END-->

	<!-- Internal Informations START-->
	<fieldset>
		<legend>
			<span id="accHeader"><spring:message
					code='repaircompletionreport.operationsandcomments' /></span>
		</legend>
		<h5>
			<spring:message code="repaircompletionreport.operations" />
			:
		</h5>
		<ol>
			<!-- add operation -->
			<li><label><spring:message
						code='repairinspection.internalestimation.estimation.addoperation' />:</label>
				<!-- add operation -->
				<div class="${ disabledModification ? 'hid':'' }">
					<div style="display: inline-block;">
						<textarea rows="2" cols="112" id="operationName"></textarea>
						<!-- internal external radio buttons and dependant inputs -->
						<div>
							<input type="radio" name="optype" checked="checked"
								value="INTERNAL"
								onchange="showRcrAdditionalFieldsForOperationType($j(this).is(':checked'), 'internal'); return false;" />
							<span id="internalOp"> <spring:message
									code='repairinspection.internalestimation.estimation.addoperation.radio.internal' />
							</span> <input type="radio" name="optype" value="EXTERNAL"
								onchange="showRcrAdditionalFieldsForOperationType($j(this).is(':checked'), 'external'); return false;" />
							<span id="externalOp"> <spring:message
									code='repairinspection.internalestimation.estimation.addoperation.radio.external' />
							</span>
							<!-- minutes (internal) -->
							<div style="display: inline;" class="internalFields">
								<span style="margin-left: 15px;"> <spring:message
										code='workflow.time' />
									:
								</span> 
								<input type="number" style="width: 35px;" min="0" max="99" id="internalTimeHours" value="0"/>
								<spring:message code='hours' />
								&nbsp;
								<input type="number" style="width: 35px;" min="0" max="59" id="internalTimeMinutes" value="0"/>
								<spring:message code='minutes' />
							</div>
							<!-- Company (External) -->
							<div style="display: none;" class="externalFields">
								<span style="margin-left: 15px;"> <spring:message
										code='company' /> :
								</span>
								<div class="extendPluginInput" style="display: inline;">
									<div class="compSearchJQPlugin">
										<input type="hidden" name="field" value="coid" /> <input
											type="hidden" name="compCoroles"
											value="supplier,utility,business" /> <input type="hidden"
											name="tabIndex" value="1" />
										<!-- company results listed here -->
									</div>
								</div>
							</div>
							<!-- delay (External) -->
							<div style="display: none;" class="externalFields">
								<span style="margin-left: 8px"> <spring:message
										code='repairinspection.internalestimation.estimation.addoperation.radio.externaldelay' />
									:
								</span> <input type="number" style="width: 40px;" id="externalTime" />
							</div>
							<!-- delay (External) -->
							<div style="display: none;" class="externalFields">
								<span> <spring:message
										code='repairinspection.internalestimation.estimation.addoperation.radio.externalwithcal' />
									:
								</span> <input type="checkbox" id="externalWithCal"
									disabled="${disabledModification}" />
							</div>
						</div>

					</div>
					<div
						style="display: inline-block; height: 50px; vertical-align: top;">
						<button style="padding: 7px 0px 7px 0px; width: 70px;"
							onclick="addRcrOperation(); return false;">
							<spring:message
								code="repairinspection.internalestimation.estimation.addcomponent.addbutton" />
						</button>
					</div>
				</div></li>
			<!-- operations table -->
			<li><label>&nbsp;<form:errors class="error max-wrap"
						path="rcrOperationsForm" /></label>
				<div style="display: flex;">
					<table class="default4" id="operationsTable">
						<thead>
							<tr>
								<th><spring:message
										code="repairinspection.internalestimation.estimation.operationstable.order" /></th>
								<th style="width: 400px"><spring:message
										code="repairinspection.internalestimation.estimation.table.operation" /></th>
								<th><spring:message
										code="repairinspection.internalestimation.estimation.operationstable.type" /></th>
								<th><spring:message
										code="repairinspection.internalestimation.estimation.operationstable.time" /></th>
								<th><spring:message
										code="repairinspection.internalestimation.cost" /></th>
								<th><spring:message
										code="repairinspection.internalestimation.rirstatus" /></th>
								<th><spring:message
										code="repairinspection.internalestimation.status" /></th>
								<th><spring:message
										code="repairinspection.internalestimation.estimation.table.actions" /></th>
							</tr>
						</thead>
						<tfoot>
							<tr class="nodrag nodrop">
								<td colspan="3" class="text-left bold right"><spring:message
										code="repairinspection.internalestimation.estimation.operationstable.totalinternal" />
								</td>
								<td style="text-align: center;">
									<span id="opTIntTime"></span>
									<span class="hid" id="opTIntTimeInMinutes"></span>
								</td>
								<td style="text-align: center;"><span id="opTIntCost"></span></td>
								<td rowspan="3" colspan="3"></td>
							</tr>
							<tr class="nodrag nodrop">
								<td colspan="3" class="text-left bold right"><spring:message
										code="repairinspection.internalestimation.estimation.operationstable.totalexternal" />
								</td>
								<td style="text-align: center;"><span id="opTExtTime"></span></td>
								<td style="text-align: center;"><span id="opTExtCost"></span></td>
							</tr>
							<tr class="nodrag nodrop">
								<td colspan="3" class="text-left bold right"><spring:message
										code="repairinspection.internalestimation.estimation.table.total" />
								</td>
								<td style="text-align: center;"><span id="opTTime"></span></td>
								<td style="text-align: center;"><span id="opTCost"></span></td>
							</tr>
						</tfoot>
						<tbody>
							<c:if test="${ empty rcrForm.rcrOperationsForm }">
								<tr class="emptyRow">
									<!-- show this if table is empty -->
									<td colspan="8"></td>
								</tr>
							</c:if>
							<c:forEach items="${ rcrForm.rcrOperationsForm }" var="op"
								varStatus="i">
								<tr class="${ i.index % 2 == 0 ? 'even':'odd' }"
									addedafterrirvalidation=" ${ op.addedAfterRiRValidation }">
									<td class="center"><c:if
											test="${ op.addedAfterRiRValidation && op.status != 'EXECUTED' }">
											<img class="dragNdropIcon" src="img/icons/scroller.png">
										</c:if> <span>${i.index+1}</span> <form:hidden
											cssClass="frOperationId"
											path="rcrOperationsForm[${ i.index }].frOperationId" /> <form:hidden
											path="rcrOperationsForm[${ i.index }].addedAfterRiRValidation" />
										<form:hidden cssClass="position"
											path="rcrOperationsForm[${ i.index }].position" /></td>
									<td class="operationname"><form:textarea class="autoresizing"
											path="rcrOperationsForm[${ i.index }].name" style="width:100%"
											disabled="${disabledModification}" /> <form:errors
											class="error max-wrap"
											path="rcrOperationsForm[${ i.index }].name" />
									</td>
									<td class="operationtype">${ op.type.value }<c:if
											test="${ op.type ne 'INTERNAL' }"> (${ op.coName })</c:if> <form:hidden
											path="rcrOperationsForm[${ i.index }].type" /> <form:hidden
											path="rcrOperationsForm[${ i.index }].coid" /> <form:errors
											class="error max-wrap"
											path="rcrOperationsForm[${ i.index }].type" /> <form:errors
											class="error max-wrap"
											path="rcrOperationsForm[${ i.index }].coid" />
									</td>
									<td class="operationtime"
										isinternal="${ op.type eq 'INTERNAL' }"><form:input
											path="rcrOperationsForm[${ i.index }].labourTimeHours"
											onchange="updateRcrOperationsTotals();"
											disabled="${disabledModification}"
											readonly="${not op.addedAfterRiRValidation}" style="width: 25px;" />
											${(op.type eq 'EXTERNAL' || op.type eq 'EXTERNAL_WITH_CALIBRATION') ? 'D(days)'
														 : 'H&nbsp;'}
											<form:input
											path="rcrOperationsForm[${ i.index }].labourTimeMinutes"
											onchange="updateRcrOperationsTotals();"
											disabled="${disabledModification}"
											readonly="${not op.addedAfterRiRValidation}" style="width: 25px;" />
											${(op.type eq 'EXTERNAL' || op.type eq 'EXTERNAL_WITH_CALIBRATION') ? ''
														 : 'M'}
											<form:errors class="error max-wrap"
											path="rcrOperationsForm[${ i.index }].labourTimeHours" />
											<form:errors class="error max-wrap"
											path="rcrOperationsForm[${ i.index }].labourTimeMinutes" />
									</td>
									<td class="operationcost"
										isinternal="${ op.type eq 'INTERNAL' }"><form:input
											path="rcrOperationsForm[${ i.index }].cost"
											onchange="updateRcrOperationsTotals();"
											disabled="${disabledModification}" /> <form:errors
											class="error max-wrap"
											path="rcrOperationsForm[${ i.index }].cost" /></td>
									<td>
										${ op.rirStatus.value }
										<form:hidden path="rcrOperationsForm[${ i.index }].rirStatus"/>
									</td>
									<td><form:select
											path="rcrOperationsForm[${ i.index }].status"
											disabled="${disabledModification}" >
												<form:option value="">&nbsp;</form:option>
     											<form:options items="${ operationStatus }" itemLabel="value" />														
											</form:select> 
											<form:errors class="error max-wrap"
 												path="rcrOperationsForm[${ i.index }].status" />
									</td>
									<td class="center"><c:if
											test="${ op.addedAfterRiRValidation && (op.status == 'AWAITING_EXECUTION') }">
											<img src="img/icons/delete.png" height="16" width="16"
												class="pointer delimg" onclick="deleteOperation(this)">
										</c:if></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div></li>
		</ol>


		<h5>
			<spring:message code="viewmod.components" />
			:
		</h5>
		<ol>
			<!-- add components -->
			<li><label><spring:message
						code='repairinspection.internalestimation.estimation.addcomponent' />:</label>
				<!-- add component -->
				<div class="${ disabledModification ? 'hid':'' }">
					<div style="display: inline-block;">
						<input style="width: 550px;" type="text" id="componentName" /> <img
							src="img/icons/searchplugin.png" width="16" height="16">

						<div style="display: block;">
							<span> <spring:message
									code='repairinspection.internalestimation.estimation.addcomponent.quantity' />
								:
							</span> <input type="number" style="width: 40px" id="componentQuantity" />

							<span> <spring:message
									code='repairinspection.internalestimation.cost' /> :
							</span> <input type="text" style="width: 40px" id="componentCost" />

						</div>

					</div>
					<div
						style="display: inline-block; height: 50px; vertical-align: top;">
						<button style="padding: 7px 0px 7px 0px; width: 70px;"
							onclick="addRcrComponent(); return false;">
							<spring:message
								code="repairinspection.internalestimation.estimation.addcomponent.addbutton" />
						</button>
					</div>
				</div></li>
			<!-- components table -->
			<li><label>&nbsp;<form:errors class="error max-wrap"
						path="rcrComponentsForm" /></label>
				<div style="display: flex;">
					<form:errors class="error max-wrap" path="rcrComponentsForm" />
					<table class="default4" id="componentsTable">
						<thead>
							<tr>
								<th><spring:message
										code="repairinspection.internalestimation.estimation.componentstable.num" /></th>
								<th style="width: 400px;"><spring:message
										code="repairinspection.internalestimation.estimation.componentstable.component" /></th>
								<th><spring:message
										code="repairinspection.internalestimation.estimation.table.operation" /></th>
								<th><spring:message
										code="repairinspection.internalestimation.estimation.componentstable.source" /></th>
								<th><spring:message
										code="repairinspection.internalestimation.estimation.addcomponent.quantity" /></th>
								<th><spring:message code="repairinspection.component.cost" /></th>
								<th><spring:message
										code="repairinspection.internalestimation.rirstatus" /></th>
								<th><spring:message
										code="repairinspection.internalestimation.status" /></th>
								<th><spring:message
										code="repairinspection.internalestimation.estimation.table.actions" /></th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="4" class="text-left bold right"><spring:message
										code="repairinspection.internalestimation.estimation.table.total" />
								</td>
								<td><span id="compTQuantity"></span></td>
								<td><span id="compTCost"></span></td>
								<td colspan="3"></td>
							</tr>
						</tfoot>
						<tbody>
							<c:if test="${ empty rcrForm.rcrComponentsForm }">
								<tr class="emptyRow">
									<!-- show this if table is empty -->
									<td colspan="9"></td>
								</tr>
							</c:if>
							<c:forEach items="${ rcrForm.rcrComponentsForm }" var="comp"
								varStatus="i">
								<tr class="${ i.index % 2 == 0 ? 'even':'odd' }"
									addedafterrirvalidation="${ comp.addedAfterRiRValidation }">
									<td class="center"><span>${i.index+1}</span> <form:hidden
											cssClass="frComponentId"
											path="rcrComponentsForm[${ i.index }].frComponentId" /> <form:hidden
											path="rcrComponentsForm[${ i.index }].addedAfterRiRValidation" />
									</td>
									<td class="componentname"><form:textarea class="autoresizing"
											path="rcrComponentsForm[${ i.index }].name" style="width:100%;text-align:left"
											disabled="${disabledModification}" /> <form:errors
											class="error max-wrap"
											path="rcrComponentsForm[${ i.index }].name" />
									</td>
									<td class="componentOperation"><c:choose>
											<c:when test="${ ! comp.addedAfterRiRValidation}">
												<spring:bind
													path="rcrComponentsForm[${ i.index }].freeRepairOperationName">
											${ status.value }
										</spring:bind>
												<form:hidden cssClass="freeRepairOperationId"
													path="rcrComponentsForm[${ i.index }].freeRepairOperationId" />
											</c:when>
											<c:otherwise>
												<form:select
													path="rcrComponentsForm[${ i.index }].freeRepairOperationId"
													onchange="componentOperationChange(this);">
													<form:options items="${ rcrForm.rcrOperationsForm }"
														itemLabel="name" itemValue="frOperationId" disabled="${disabledModification}" />
												</form:select>
											</c:otherwise>
										</c:choose> <form:errors class="error max-wrap"
											path="rcrComponentsForm[${ i.index }].freeRepairOperationId" />
										<form:hidden cssClass="freeRepairOperationName"
											path="rcrComponentsForm[${ i.index }].freeRepairOperationName" />
									</td>
									<td class="componentsource">${ comp.source.message } <form:hidden
											path="rcrComponentsForm[${ i.index }].source" /> <form:errors
											class="error max-wrap"
											path="rcrComponentsForm[${ i.index }].source" />
									</td>
									<td class="componentquantity"><form:input
											path="rcrComponentsForm[${ i.index }].quantity"
											onchange="updateRcrComponentsTotals();"
											disabled="${disabledModification}" /> <form:errors
											class="error max-wrap"
											path="rcrComponentsForm[${ i.index }].quantity" /></td>
									<td class="componentcost"><form:input
											path="rcrComponentsForm[${ i.index }].cost"
											onchange="updateRcrComponentsTotals();"
											disabled="${disabledModification}" /> <form:errors
											class="error max-wrap"
											path="rcrComponentsForm[${ i.index }].cost" /></td>
									<td>
										${ comp.rirStatus.value }
										<form:hidden path="rcrComponentsForm[${ i.index }].rirStatus"/>
									</td>
									<td><form:select
											path="rcrComponentsForm[${ i.index }].status"
											disabled="${disabledModification}" >
											<form:option value="">&nbsp;</form:option>
     										<form:options items="${ componentStatus }" itemLabel="value" />
										</form:select>
										 	<form:errors
											class="error max-wrap"
											path="rcrComponentsForm[${ i.index }].status" /></td>
									<td class="center"><c:if
											test="${ comp.addedAfterRiRValidation and (comp.status == 'TO_ORDER' or comp.status == 'IN_STOCK') }">
											<img src="img/icons/delete.png" height="16" width="16"
												class="delimg" onclick="deleteRcrComponent(this)">
										</c:if></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div></li>
			<li><label>&nbsp;</label>
				<div>
					<table id="totalsTable">
						<tr>
							<td><label><spring:message
										code='repairinspection.totalcost' />:</label></td>
							<td><span id="totalCost"></span></td>
						</tr>
					</table>
				</div></li>
		</ol>

	</fieldset>
	
	<!-- upload other START-->
		<fieldset>
			<legend>
				<span id="accHeader"><spring:message
						code='repairinspection.otherinformations' /></span>
			</legend>
			<ol>
				<li>
					<label>
						<spring:message code='repaircompletionreport.repaircompletiondate' />:
					</label>
					<form:input type="datetime-local" path="repairCompletionDate"
										step="60" max="${cwms:isoDatetime(rcrForm.currentDate)}"
										/>
					<form:errors path="repairCompletionDate"/>
					<div id="lastTpCertDateDiv" class="hidden">
						<c:if test="${ not empty lastTpCertDate}">
							<spring:message code='repairinspection.lasttpcertmessage' />:
							<fmt:formatDate type="both" dateStyle="SHORT" timeStyle="SHORT" value="${ lastTpCertDate }" />
						</c:if>
					</div>
				</li>
				
				<li>
					<label>
						<spring:message code='repairinspection.internalestimation.misuse' />:
					</label>
					<form:radiobutton path="misuse" id="misuseCheckBoxRcr" value="YES" disabled="${disabledModification}" onclick="showMisuseCommentRcr(true)"/>
					<span class="padding10"><spring:message code='yes' /></span>
					<form:radiobutton path="misuse" id="misuseCheckBoxRcr" value="NO" disabled="${disabledModification}" onclick="showMisuseCommentRcr(false)"/>
					<span><spring:message code='no' /></span>
					</li>
					<li id="misuseCommentTextRcr" class="${ rcrForm.misuse ? 'show':'hid' }">
						<label>
							<spring:message code='repairinspection.internalestimation.misusecomment' />:
						</label>
						<form:textarea path="misuseComment" rows="2" cols="130" disabled="${disabledModification}" />
					</li>
				<li>
					<label>
						<spring:message code='repaircompletionreport.adjustmentperformed' /> * :
					</label>
					<c:forEach items="${ adjustmentPerformedValues }" var="ap">
						<form:radiobutton path="adjustmentPerformed" value="${ ap }" disabled="${disabledModification}" />
						<span  class="padding10">${ ap.value }</span>
					</c:forEach>
					<form:errors class="error max-wrap" path="adjustmentPerformed" />
				</li>
				<c:if test="${ empty needTPRcrAppendixDocument or !needTPRcrAppendixDocument }">
				<div align="center">
					<input type="submit" name="saveRcr" value="Save" id="saveBtn"
						${disabledModification ? 'disabled':''} />
					<button type="button" name="saveAndValidateRcr"
						${ canValidate ? '' : 'disabled' } onclick="validateRcr()">
						<spring:message code='failurereport.validate' />
					</button>
					<button type="button"
						${ not empty rcrForm.validated && rcrForm.validated ? '' : 'disabled' }
						onclick=" event.preventDefault(); genDocPopup(${rirForm.rirId}, 'rirdocument.htm?completionReport=true&ririd=', 'Generating Document'); "
						title="<spring:message code='generate' />">
						<spring:message code="generate" />
					</button>
				</div>
			</c:if>
			</ol>
		</fieldset>
	<!-- upload other END-->

	<c:if test="${ not empty needTPRcrAppendixDocument and needTPRcrAppendixDocument }">
		<!-- upload tp rir document START-->
		<fieldset>
			<legend>
				<span id="accHeader"><spring:message
						code="repairinspection.uploadtprcrappendix" /></span>
			</legend>
			<ol>

				<li><form:label path="rcrAppendixFile">
						<spring:message code="jicertificates.uplfile" />:</form:label> <form:input
						path="rcrAppendixFile" type="file" id="uploadfile"
						accept="application/pdf" disabled="${disabledModification}" /> <span
					class="attention"><form:errors path="rcrAppendixFile" /></span> <c:if
						test="${ not empty encryptedRcrAppendixFile }">
				&nbsp;&nbsp;
				<span><spring:message code="repairinspection.existingtprcrappendixfile" />:</span>
						<a href="downloadfile.htm?file=${encryptedRcrAppendixFile}"
							target="_blank"
							title="(<spring:message code="showfilesforsc.lastmodified"/>: ${rcrForm.lastModified})"
							disabled="${disabledModification}">&nbsp; <img
							src="img/icons/certificate.png" width="20" height="16"
							class="img_inl_margleft" alt="" title="" /> <span
							style="color: black">${rcrAppendixFileName}</span>
						</a>
					</c:if></li>
				<div align="center">
					<input type="submit" name="saveRcr" value="Save" id="saveBtn"
						${disabledModification ? 'disabled':''} />
					<button type="button" name="saveAndValidateRcr"
						${ canValidate ? '' : 'disabled' } onclick="validateRcr()">
						<spring:message code='failurereport.validate' />
					</button>
					<button type="button"
						${ not empty rcrForm.validated && rcrForm.validated ? '' : 'disabled' }
						onclick=" event.preventDefault(); genDocPopup(${rirForm.rirId}, 'rirdocument.htm?completionReport=true&ririd=', 'Generating Document'); "
						title="<spring:message code='generate' />">
						<spring:message code="generate" />
					</button>
				</div>
			</ol>
		</fieldset>
		<!-- upload tp rir document END-->
	</c:if>


	
	
</form:form>