<%-- File name: /trescal/core/jobs/calibration/newbatchcalibration.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext">
		<c:choose>
			<c:when test="${not empty form.existingBatch}">
			<spring:message code="newbatchcalibration.addcaltobatch"/>
			</c:when>
			<c:otherwise>
			<spring:message code="newbatchcalibration.newbatchcal"/> 
			</c:otherwise>
		</c:choose>
		</span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
		<script type='module' src='script/components/search-plugins/cwms-capability-search/cwms-capability-search.js'></script>
        <script type='text/javascript' src='script/trescal/core/jobs/calibration/NewBatchCalibration.js'></script>
    </jsp:attribute>
    <jsp:body>
	<!-- error section displayed when form submission fails -->
				<form:errors path="form.*">
       				 <div class="warningBox1">
              		  <div class="warningBox2">
              	       <div class="warningBox3">
                    	<h5 class="center-0 attention"><spring:message code="newbatchcalibration.attention"/>:</h5>
                      <c:forEach var="e" items="${messages}">
                        <div class="center attention"><c:out value="${e}"/></div>
                      </c:forEach>
                     </div>
                   </div>
                  </div>
                 </form:errors>
			
			<form:form action="" method="post" id="newbatchcalibration" modelAttribute="form">
			<c:choose>
				<c:when test="${not empty form.existingBatch}">
					<input type="hidden" name="batchid" value="${form.existingBatch.id}">
				</c:when>
				<c:otherwise>
					<input type="hidden" name="jobid" value="${form.job.jobid}">
				</c:otherwise>
			</c:choose>
				<!-- div styled with nifty corners -->
				<div class="infobox">
																		
			<fieldset>
				
				<legend><spring:message code="newbatchcalibration.legend"/></legend>									
			
				<ol>
					<li>
						<label><spring:message code="newbatchcalibration.lbltechcian"/>:</label>
						<span>
							<c:choose>
								<c:when test="${not empty form.existingBatch}">
									${form.existingBatch.owner.name}
								</c:when>
								<c:otherwise>
									${currentContact.name}
								</c:otherwise>
							</c:choose>
						</span>
					</li>
					<li>
						<label><spring:message code="company"/>:</label>
						<span><links:companyLinkDWRInfo company="${form.job.con.sub.comp}" rowcount="0" copy="${true}"/></span>
					</li>
					<li>
						<label><spring:message code="calsearch.jobno"/>:</label>
						<span>
							<links:jobnoLinkDWRInfo jobno="${form.job.jobno}" jobid="${form.job.jobid}" copy="${true}" rowcount="${rowcount}"/>
						</span>
					</li>
					<li>
						<label><spring:message code="newbatchcalibration.caldate"/>:</label>
						<span>
							<c:choose>
								<c:when test="${not empty form.existingBatch}">
								<fmt:formatDate value='${form.existingBatch.calDate}' type="date" dateStyle="SHORT" />
								</c:when>
								<c:otherwise>
                                <form:input path="calDate" type="date" />
								</c:otherwise>
						</c:choose>
						</span>
					</li>
					<li>
						<label><spring:message code="multistart.calprocess"/>:</label>
						<span>
						<c:choose>
							<c:when test="${not empty form.existingBatch}">
								${form.existingBatch.process.process}
							</c:when>
							<c:otherwise>
							<form:select path="calProcessId" onchange=" changeProcess($j(this).find('option:selected').text()); return false; ">
								<c:forEach var="cp" items="${calprocesses}">
									<option value="${cp.id}" <c:if test="${cp.id == form.calProcessId}"> selected="selected" </c:if>>${cp.process}</option>
								</c:forEach>
							</form:select>
							</c:otherwise>
						</c:choose>
						</span>
					</li>
				</ol>
				
			<c:set var="calInstructions" value="0"/>
				<c:forEach var="i" items="${form.job.relatedInstructionLinks}">
					<c:if test="${i.instruction.instructiontype == 'CALIBRATION'}">
						<c:set var="calInstructions" value="${calInstructions + 1}"/>
					</c:if>
				</c:forEach>
				<c:set var="itemsWithReqs" value="0"/>
				<c:forEach var="ji" items="${form.job.items}">
					<c:if test="${ji.req.size() > 0}">
						<c:set var="activeReq" value="${false}"/>
						<c:forEach var="r" items="${ji.req}">
							<c:if test="${r.deleted == false}">
								<c:set var="activeReq" value="${true}"/>
							</c:if>
						</c:forEach>
						
						<c:if test="${not empty activeReq}">
							<c:set var="itemsWithReqs" value="${itemsWithReqs + 1}"/>
						</c:if>
					</c:if>
				</c:forEach>
				<c:choose>
					<c:when test="${itemsWithReqs < 1 && calInstructions < 1}">
						<input type="hidden" name="infoAcknowledged" value="true" />
					</c:when>
					<c:otherwise>
					<div class="clear">&nbsp;</div>
					<ol>
						<li>
							<span class="float-left">
								<c:if test="${calInstructions > 0}"><span class="attention bold"><spring:message code="newbatchcalibration.thereare"/><c:out value=" ${calInstructions} " /><spring:message code="newbatchcalibration.thereare1"/></span><br/></c:if>
								<c:if test="${itemsWithReqs > 0}"><span class="attention bold"><spring:message code="newbatchcalibration.thereare"/><c:out value=" ${itemsWithReqs} " /><spring:message code="newbatchcalibration.thereare2"/></span><br/></c:if>
							</span>
							<span class="float-right"><i><spring:message code="newbatchcalibration.understcalinstr"/></i>&nbsp;&nbsp;
								<c:set var="checked" value=""/>
                                <c:if test="${form.infoAcknowledged == true}">
                                    <c:set var="checked" value="checked"/>
                                </c:if>
                                <form:checkbox path="infoAcknowledged" />
							</span>
							<div class="clear"></div>
						</li>
					</ol>
					</c:otherwise>
				</c:choose>
			
			</fieldset>
						
				</div>
				<!-- end of div -->
						
				<!-- div containing batch calibration items table styled with nifty corners -->
				<div class="infobox">		
													
					<table class="default2" id="newbatchcaltable" summary="<spring:message code="newbatchcalibration.newbatchcaltable"/>">
						
						<thead>
							<tr>
								<td colspan="8"><spring:message code="newbatchcalibration.batcalitems"/></td>
							</tr>
							<tr>
								<th class="item" scope="col"><spring:message code="multistart.item"/></th>
								<th class="itemdesc" scope="col"><spring:message code="newbatchcalibration.itemdescr"/></th>
								<th class="serialno" scope="col"><spring:message code="serialno"/></th>
								<th colspan="2" scope="col"><spring:message code="calsearch.capability"/></th>
								<th class="caltype" scope="col"><spring:message code="calsearch.casltype"/></th>
								<th class="accred" scope="col"><spring:message code="newbatchcalibration.accred"/></th>
								<th class="add" scope="col">
									<spring:message code="multistart.all"/>
									<input type="checkbox" id="selectAll" onclick=" selectAllItems(this.checked, 'newbatchcaltable'); " checked="checked" />
								</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="8">&nbsp;</td>
							</tr>
						</tfoot>
						<tbody>
							<c:forEach var="itemCal" items="${form.itemCals}" varStatus="loop">
								<c:set var="count" value="${loop.index}"/>
								<tr id="${count}" <c:choose>
													<c:when test="${count % 2 == 0}"> class="even" </c:when>
													<c:otherwise> class="odd" </c:otherwise>
												</c:choose>>
									<td>${itemCal.ji.itemNo}</td>
									<td><cwms:showmodel instrumentmodel="${itemCal.ji.inst.model}"/></td>
									<td>${itemCal.ji.inst.serialno}</td>
									<td colspan="2">
										<div class="float-left">
											<cwms-capability-search-form 
											name="itemCals[${count}].procId}"
											procid="${itemCal.procId}" 
											defaultvalue="${procedures.get(itemCal.procId).reference}">
											</cwms-capability-search-form>
										</div>
										<form:errors path="itemCals[${count}].procId" class="attention"/>
										<!-- clear floats and restore page flow -->
										<div class="clear"></div>
									</td>
									<td class="center">
										<t:showTranslationOrDefault translations="${itemCal.ji.calType.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/>
									</td>												
									<td class="accredited center">
										<c:if test="${not empty itemCal.accredited}">
											<c:choose>
												<c:when test="${itemCal.accredited == true}">
												<input type="hidden" name="itemCals[${count}].accredited" value="true" />
												<img src="img/icons/greentick.png" alt="<spring:message code="newbatchcalibration.accredited"/>" title="<spring:message code='newbatchcalibration.accredited'/>" width="16" height="16" />
												</c:when>
												<c:otherwise>
												<input type="hidden" name="itemCals[${count}].accredited" value="false" />
												<img src="img/icons/redcross.png" alt="<spring:message code="newbatchcalibration.notaccredited"/>" title="<spring:message code='newbatchcalibration.notaccredited'/>" width="16" height="16" />
												</c:otherwise>
											</c:choose>
										</c:if>	
									</td>
									<td class="addtobatch center">
										<input type="hidden" path="_itemCals[${count}].toBatch" value="false" />
										<c:choose>
											<c:when test="${not empty itemCal.accredited}">
                                                    <c:set var="checked" value="" />
                                                    <c:set var="disabled" value="" />
                                                    <c:choose>
                                                        <c:when test="${form.itemCals.toArray()[count].toBatch == true}">
                                                            <c:set var="checked" value="checked" />
                                                        </c:when>
                                                        <c:when test="${itemCal.accredited == false}">
                                                            <c:set var="disabled" value="disabled" />
                                                        </c:when>
                                                    </c:choose>
                                             </c:when>
                                             <c:otherwise>
                                                 <c:set var="disabled" value="disabled" />
                                             </c:otherwise>
                                        </c:choose>
                                        <form:checkbox class="calcheck" path="itemCals[${count}].toBatch" disabled="${disabled}" />
									</td>
								</tr>
								<c:set var="showCwmsOnly" value="${true}"/>
								<c:if test="${not empty form.existingBatch}">
									<c:set var="showCwmsOnly" value="${false}"/>
									<c:if test="${form.existingBatch.process.applicationComponent}">
										<c:set var="showCwmsOnly" value="${true}"/>
									</c:if>
								</c:if>
								<tr <c:choose><c:when test="${count % 2 == 0}"> class="even" </c:when><c:otherwise> class="odd" </c:otherwise></c:choose>>
									<td colspan="8">
										<span class="bold"><spring:message code="newbatchcalibration.standsel"/>: <span id="stdCount-${count}">${itemCal.standardIds.size()}</span></span>
										<span class="vis">
											<a href="#" onclick=" showStandardSelect(${count}, this); return false; ">
												<img src="img/icons/showinfo.png" class="img_marg_bot" width="16" height="16" alt="<spring:message code="newbatchcalibration.imgshowinfo"/>" title="<spring:message code="newbatchcalibration.imgshowinfo"/>" />
											</a>
										</span>
										<span class="hid">
											<a href="#" onclick=" hideStandardSelect(${count}, this); return false; ">
												<img src="img/icons/hideinfo.png" class="img_marg_bot" width="16" height="16" alt="<spring:message code="newbatchcalibration.hidestand"/>" title="<spring:message code="newbatchcalibration.hidestand"/>" />
											</a>
										</span>
										
										<div id="stdSelect-${count}" class="hid">
											<br/>
										
											<span style="float: right;">
												<span style=" text-decoration: underline; "><spring:message code="newbatchcalibration.key"/></span><br/>
												<span><spring:message code="newbatchcalibration.optional"/></span><br/>
												<span style=" font-weight: bold; "><spring:message code="newbatchcalibration.enforced"/></span><br/>
												<span style=" color: red; "><spring:message code="multistart.outofcal"/></span><br/>
											</span>
										
											<br/>
											<spring:message code="newbatchcalibration.procdef"/>:
											<br/>
											<div id="procStds-${count}">
												<c:if test="${not empty itemCal.procId}">
													<c:forEach var="std" items="${procedures.get(itemCal.procId).standards}">
														<input name="itemCals[${count}].standardIds" <c:if test="${not empty itemCal.standardIds.contains(std.instrument.plantid)}"> checked="checked" </c:if>  onclick=" toggleDuplicates(this, ${count}); " type="checkbox" value="${std.instrument.plantid}" />
														<span style=" <c:if test="${not empty std.instrument.outOfCalibration}"> color: red; </c:if> 
																	<c:if test="${not empty std.enforceUse}"> font-weight: bold; </c:if> ">${std.instrument.plantid} : <cwms:showmodel instrumentmodel="${std.instrument}"/>
														</span><br/>
													</c:forEach>
												</c:if> 
											</div>
											
											<br/>
											<spring:message code="newbatchcalibration.workinstrdef"/>:
											<br/>
											<div id="wiStds-${count}">
												<c:if test="${not empty itemCal.workInstructionId}">
													<c:forEach var="std" items="${workinstructions.get(itemCal.workInstructionId).standards}">
														<input name="itemCals[${count}].standardIds" <c:if test="${not empty itemCal.standardIds.contains(std.instrument.plantid)}"> checked="checked" </c:if> onclick=" toggleDuplicates(this, ${count}); " type="checkbox" value="$std.instrument.plantid" /> 
														<span style=" <c:if test="${not empty std.instrument.outOfCalibration}"> color: red; </c:if> 
														 <c:if test="${not empty std.enforceUse}"> font-weight: bold; </c:if> ">${std.instrument.plantid} : <cwms:showmodel instrumentmodel="${std.instrument}"/>
														 </span><br/>
													</c:forEach>
												</c:if> 
											</div>
											
											<!-- This fallback is here because Spring would otherwise 
												pass the previous selections if no standards were selected -->
											<input type="hidden" name="_itemCals[${count}].standardIds" value="0" />
										</div>
									</td>
								</tr>											
							</c:forEach>										
						</tbody>
						
					</table>
					
				</div>
				<!-- end of batch calibration items infobox div -->
				
				<!-- div containing password form elements and styled with nifty corners -->
				<c:choose>
					<c:when test="${not empty form.existingBatch}">
					<div class="center">								
						<input type="submit" value="<spring:message code="newbatchcalibration.addcaltobatch"/>" />
					</div>
					<div class="clear">&nbsp;</div>
					</c:when>
					<c:otherwise>
					<div class="infobox">
						
						<fieldset>
							
							<legend><spring:message code="newbatchcalibration.batcalconf"/></legend>
							
							<ol>
								
								<li>
									<label class="width80 bold"><spring:message code="newbatchcalibration.label1"/></label>
									<span>&nbsp;</span>
								</li>
								<li class="cwmsOnly">
									<fieldset>
										<legend>
											<form:checkbox path="manualSelect" checked="checked" />
											<spring:message code="newbatchcalibration.string1"/>
										</legend>
									</fieldset>
								</li>
								<c:if test="${form.autoPrinting == true}">
									<c:choose>
										<c:when test="${not empty currentContact.encryptedPassword}">
										<li>
											<form:label path="password"><spring:message code="completecalibration.passw"/>:</form:label>
											<form:input type="password" path="password" />
										</li>
										</c:when>
										<c:otherwise>
										<li>
											<fieldset>
												<legend><spring:message code="newbatchcalibration.legend1"/>:</legend>
											</fieldset>
										</li>
										<li>
											<form:label path="password"><spring:message code="completecalibration.passw"/>:</form:label>
											<form:input type="password" path="password" onkeyup=" passwordStrength(this.value); " />
										</li>
										<li>
											<form:label path="confirmPassword"><spring:message code="completecalibration.confpassw"/>:</form:label>
											<form:input type="password" path="confirmPassword" />
										</li>
										<li>
											<label><spring:message code="completecalibration.strength"/>:</label>
											<div id="passwordDescription"><spring:message code="completecalibration.passwnotent"/></div>
						                    <div id="passwordStrength" class="strength0"></div>
											<!-- clear floats and restore page flow -->
											<div class="clear"></div>
										</li>										
										</c:otherwise>
									</c:choose>
								</c:if>
							</ol>
							
						</fieldset>									
													
						<div class="center">								
							<input type="submit" value="<spring:message code="newbatchcalibration.beginbatch"/>" />
						</div>						
				
					</div>
					</c:otherwise>
				</c:choose>
				<!-- end of password infobox div -->
			
			</form:form>
    </jsp:body>
</t:crocodileTemplate>