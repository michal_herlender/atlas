<%-- File name: /trescal/core/jobs/job/activejobs.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code='activejobs.headtext'/></span>
	</jsp:attribute>
	<jsp:body>
		<p><span class="bold">${currentContact.name}</span>: <spring:message code='activejobs.welcomemessage'/></p>
		<div>
			<span class="bold">
				<spring:message code='activejobs.upcomingwork'/> - 
				<a href="viewupcomingwork.htm" class="mainlink">
					<spring:message code='activejobs.viewcalendar'/>
					<c:out value=" (${futureTotal}) - "/> 
					<spring:message code='activejobs.thisweek'/> (${weeklyTotal})
				</a>
			</span>
		<p>
			<span class="bold"><spring:message code='activejobs.activeitems'/> - </span>
			<a href="viewselectedjobitems.htm?all=true" class="mainlink">
				<spring:message code='activejobs.viewall'/> (${pagemap.total})
			</a>
		</p>
		<div class="center">
			<div class="small-list">
				<h4><spring:message code='activejobs.activeitems'/></h4>
				<ul class="small-left">
					<li>5+ <spring:message code='activejobs.days'/></li>
					<li>4 <spring:message code='activejobs.days'/></li>
					<li>3 <spring:message code='activejobs.days'/></li>
					<li>2 <spring:message code='activejobs.days'/></li>
					<li>1 <spring:message code='activejobs.day'/></li>
					<li class="attention"><spring:message code='activejobs.OVERDUE'/></li>
				</ul>
				<ul class="small-right">
					<li><a href="viewselectedjobitems.htm?days=5" class="mainlink">${pagemap.five}</a></li>
					<li><a href="viewselectedjobitems.htm?days=4" class="mainlink">${pagemap.four}</a></li>
					<li><a href="viewselectedjobitems.htm?days=3" class="mainlink">${pagemap.three}</a></li>
					<li><a href="viewselectedjobitems.htm?days=2" class="mainlink">${pagemap.two}</a></li>
					<li><a href="viewselectedjobitems.htm?days=1" class="mainlink">${pagemap.one}</a></li>
					<li class="attention"><a href="viewselectedjobitems.htm?days=0" class="mainlink">${pagemap.zero}</a></li>
				</ul>
				<hr class="clear" />
				<ul class="small-left">
					<li class="bold"><spring:message code='activejobs.total'/></li>
				</ul>
				<ul class="small-right">
					<li><a href="viewselectedjobitems.htm?all=true" class="mainlink">${pagemap.total}</a></li>
				</ul>	
				<div class="clear"></div>
			</div>						
		</div>
	</jsp:body>
</t:crocodileTemplate>