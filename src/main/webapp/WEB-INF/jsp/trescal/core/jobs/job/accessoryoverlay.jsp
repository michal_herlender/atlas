<%-- File name: /trescal/core/jobs/job/accessoryoverlay.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<div id="addAccessoriesOverlay">
	<table class="default4 hireAccessories" summary="display any job item accessories">
		<thead>
			<tr>
				<td colspan="2"><spring:message code="accessoryoverlay.jobItemAccessories" arguments="${jobItem.accessories.size()}"/></td>
			</tr>
			<tr>
				<td colspan="2"><spring:message code="accessoryoverlay.selectAccessories"/></td>
			</tr>
			<tr>
				<th class="item"><spring:message code="item"/></th>
				<th class="add">
					<input type="checkbox" id="selectAll" onclick="$j('input:checkbox').prop('checked', $j(this).prop('checked'));"/><spring:message code="all"/><br />
				</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="itemAccessory" items="${jobItem.accessories}">
				<tr>
					<td class="item">
						<cwms:besttranslation translations="${itemAccessory.accessory.translations}"/>
						x ${itemAccessory.quantity}
					</td>
					<td class="add">
						<input type="checkbox" name="accSelector" value="${itemAccessory.id}"/>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<div class="text-center">
		<input type="button" value="<spring:message code='save'/>" onclick="event.preventDefault(); this.disabled=true; updateDeliveryItemAccessories(${deliveryItemId}, ${jobItem.jobItemId});"/>
	</div>
</div>