<%-- File name: /trescal/core/jobs/jobitem/jicertificates.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="org.trescal.cwms.core.tools.EncryptionTools" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs" %>
<%@ taglib prefix="addr" tagdir="/WEB-INF/tags/address" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<jobs:jobItemTemplate>
	<jsp:attribute name="subScript">
		<script type='text/javascript' src='script/trescal/core/jobs/jobitem/JICertificates.js'></script>
		<script type='text/javascript' src='script/trescal/core/tools/labelprinting/dymo/DymoLabelFunctions.js'></script>
		<script type='text/javascript' src='script/trescal/core/documents/ImageGalleryFunctions.js'></script>
		<script>
			// grab scroll to parameter passed when using job item key navigation
			var scrollto = '${request.getParameter("scrollto")}';
			var strings = new Array();
			strings['certificatereplacement.selectwr.title'] = "<spring:message code='certificatereplacement.selectwr.title' javaScriptEscape='true' />";
			strings['certificatereplacement.confirmation'] = "<spring:message code='certificatereplacement.confirmation' javaScriptEscape='true' />";
		</script>
	</jsp:attribute>
	<jsp:attribute name="warnings">
	     <form:errors path="*">
       		   <div class="warningBox1">
                  <div class="warningBox2">
                       <div class="warningBox3">
                             <h5 class="center-0 attention">
                                 <spring:message code="error.save"/>
                              </h5>
                         </div>
                   </div>
               </div>
         </form:errors>
		<!-- end error section -->
	</jsp:attribute>
	<jsp:body>
		<!-- Page specific code -->
		<!-- this infobox contains all certificate options -->
		<div class="infobox">
			<!-- div to hold all the tabbed submenu -->
			<div id="tabmenu">
				<!-- tabbed submenu to change between contract review options -->
				<ul class="subnavtab">
					<li><a href="#" id="existingcerts-link" onclick="switchMenuFocus(menuElements, 'existingcerts-tab', false); return false; " class="selected"><spring:message code="jicertificates.existcerts" /></a></li>
					<li><a href="#" id="newcert-link" onclick="switchMenuFocus(menuElements, 'newcert-tab', false); return false; "><spring:message code="jicertificates.thirdpartycert" /></a></li>
				</ul>
				<!-- div to hold all content which is to be changed using the tabbed submenu -->
				<div class="tab-box">
				<c:set var="hasErrors" value="false"/>
						<form:errors path="hasErrors" class="attention"/>

					<!-- div displaying all existing certificates -->
					<div id="existingcerts-tab" <c:if test="${hasErrors}">class="hid"</c:if>>
						
						<c:set var="normalCount" value="0"/>
		      			<c:forEach var="u" items="${form.userCertWraps}">
		      				<c:if test="${u.cl.cert.type == 'CERT_INHOUSE'}">
				  				<c:set var="normalCount" value="${normalCount + 1}"/>
				  			</c:if>
				  		</c:forEach>
				  		
						<c:choose>
							<c:when test="${calibrationVerification}">
								<c:set var="colspan" value="17"/>
								<c:set var="colspan2" value="16"/>
							</c:when>
							<c:otherwise>
								<c:set var="colspan" value="16"/>
								<c:set var="colspan2" value="15"/>
							</c:otherwise>
						</c:choose>
				  		
						<!-- table displaying all job item certificates -->
						<spring:message var="certForJobItemText" code='jicertificates.string1' />
			        	<table class="default2" id="jicerttable" summary="${certForJobItemText}">
			        		<thead>
			        			<tr>
			        				<td colspan="${colspan}"><spring:message code="jicertificates.inhousecert" /> (${normalCount})</td>
			        			</tr>
			        			<tr>
				            		<th class="jic_certno"><spring:message code="jicertificates.certno" /></th>
				            		<th class="jic_externalno"><spring:message code="jicertificates.externalno" /></th>
				            		<th class="jic_certnote"><spring:message code="jicertificates.notes" /></th>
									<th class="jic_caldate"><spring:message code="jicertificates.caldate" /></th>
							        <th class="jic_issuedate"><spring:message code="jicertificates.issdate" /></th>
							        <th class="jic_duration"><spring:message code="jicertificates.duration" /></th>
							        <th class="jic_files"><spring:message code="jicertificates.files" /></th>
							        <th class="jic_caltype"><spring:message code="type" /></th>
							        <th class="jic_regby"><spring:message code="jicertificates.regby" /></th>
							        <th class="jic_process"><spring:message code="process" /></th>
									<c:if test="${calibrationVerification}">
								        <th class="jic_calver"><spring:message code="viewinstrument.calverificationstatus" /></th>
									</c:if>
									<th class="jic_status"><spring:message code="status" /></th>
									<th class="jic_properties"><spring:message code="exchangeformat.fieldname.restriction" /></th>
									<th class="jic_properties"><spring:message code="exchangeformat.fieldname.adjustment" /></th>
									<th class="jic_properties"><spring:message code="exchangeformat.fieldname.optimization" /></th>
									<th class="jic_properties"><spring:message code="exchangeformat.fieldname.repair" /></th>
									<th class="jic_properties"><spring:message code="jicertificates.cancelandreplace" /></th>
				      			</tr>
			        		</thead>
			        		<tfoot>
			        			<tr>
			        				<td colspan="${colspan}">&nbsp;</td>
			        			</tr>
			        		</tfoot>
			        		<tbody>
								<c:choose>
									<c:when test="${normalCount < 1}">
										<tr>
								        	<td colspan="${colspan}" class="center bold"><spring:message code="jicertificates.string2" /></td>
								      	</tr>
									</c:when>
									<c:otherwise>
							  			<c:forEach var="u" items="${form.userCertWraps}">
							  				<c:if test="${u.cl.cert.type == 'CERT_INHOUSE'}">
							      				<tr id="certificate${u.cl.cert.certid}">
							        				<td class="jic_certno">
							        					<c:if test="${u.cl.cert.supplementaryFor != null}">
															<spring:message code="jicertificates.supplemfor" />: ${u.cl.cert.supplementaryFor.certno}
														</c:if>
														<c:url var="viewCertUrl" value='viewcertificate.htm?certid=${u.cl.cert.certid}' />
							        					<a href="${viewCertUrl}" class="mainlink" title="Edit Certificate">
							        						${u.cl.cert.certno}
							        					</a>
							        					<spring:message var="addCertText" code="jicertificates.addcerttoclipb" />
							        					<img src="img/icons/bullet_clipboard.png" width="10" height="10" onclick=" $j(this).prev().clipBoard(this, null, false, null); " alt="${addCertText}" title="${addCertText}" >
																
														<!-- certificate options div available if certificate is linked to a calibration -->
														<c:choose>
															<c:when test="${u.cl.cert.cal != null}">
																<c:set var="className" value="inline" />
															</c:when>
															<c:otherwise>
																<c:set var="className" value="hid" />
															</c:otherwise>
														</c:choose>
														<div class="${className}">
													
															<!-- link which creates thickbox to show certificate options below -->
															<a href="#" onclick=" displayCertOptions('cert${u.cl.cert.certid}'); return false; ">
																<img src="img/icons/cert_edit.png" width="20" height="16" class="img_inl_margleft" alt="<spring:message code='jicertificates.string3' /> ${u.cl.cert.certno}" title="<spring:message code='jicertificates.string3' /> ${u.cl.cert.certno}" />
															</a>
															
															<!-- contents of this div are displayed in thickbox link above -->
															<div id="cert${u.cl.cert.certid}" class="hid">
																<fieldset class="marg-bot">
																	<ol>
																		<jobs:printCertificateLabelListItems serialno="${form.ji.inst.serialno}" u="${u}" labelPrinter="${labelPrinter}" useCertLinks="${useCertLinks}"/>
																		<li>
																			<div class="displaycolumn-30">
																				<label><spring:message code="jicertificates.envelopelbl" />:</label>
																				<span>
																					<spring:message var="printCertEnvelopeLabelText" code="jicertificates.prntcertenvlbl" />
																					<a href="#" onclick=" event.preventDefault(); printDymoCertLabel(${u.cl.linkId}); ">
																						<img src="img/icons/printer.png" width="16" height="16" class="img_marg_bot" alt="${printCertEnvelopeLabelText}" title="${printCertEnvelopeLabelText}" />
																					</a>
																				</span>
																			</div>
																			<div class="displaycolumn-70">
																				<span style="vertical-align: top;"><spring:message code="jicertificates.optcomment" />:&nbsp;&nbsp;&nbsp;</span>
																				<textarea name="freetext" style=" width: 225px; height: 35px; " id="envelopeComment-${u.cl.linkId}"></textarea>
																			</div>
																			<div class="clear"></div>
																		</li>
																			
																		<c:choose>
																			<c:when test="${u.cl.cert.cal.calProcess.applicationComponent}">
																			<li>
																				<label class="labelhead bold"><spring:message code="jicertificates.template" /></label>
																				<span>&nbsp;</span>
																			</li>
																			<c:choose>
																				<c:when test="${not empty u.cl.cert.docTemplateKey}">
																				<li>
																					<label><spring:message code="jicertificates.currtempl" />:</label>
																					<span>
																						${u.cl.cert.docTemplateKey}
																					</span>
																					<a href="#" class="mainlink previewCertificate marg-left" onclick=" createCertificatePreview(this, ${u.cl.cert.certid}, ${u.cl.cert.cal.id}, $j(this).prev().text().trim()); return false; ">
																						<spring:message code="jicertificates.certprev" />
																					</a>
																				</li>
																				<c:if test="${!u.cl.cert.certPreviewed && (currentContact.personid == u.cl.cert.cal.completedBy.personid)}">
																					<li>
																						<label><spring:message code="jicertificates.certpreview" />:</label>
																						<span>
																							<spring:message var="previewedText" code='jicertificates.string5' />
																							<a href="#" class="cross" onclick=" markCertificateAsPreviewed(this, ${u.cl.cert.certid}); return false; " title="${previewedText}">&nbsp;</a>
																						</span>
																					</li>
																				</c:if>
																			</c:when>
																			<c:when test="${not empty u.cl.cert.cal.certTemplate}">
																				<li>
																					<label><spring:message code="jicertificates.usetempl" />:</label>
																					<span>
																						${u.cl.cert.cal.certTemplate}
																					</span>
																					<a href="#" class="mainlink previewCertificate marg-left" onclick=" createCertificatePreview(this, ${u.cl.cert.certid}, ${u.cl.cert.cal.id}, $j(this).prev().text().trim()); return false; ">
																						<spring:message code="jicertificates.certprev" />
																					</a>
																				</li>
																				<c:if test="${!u.cl.cert.certPreviewed && (currentContact.personid == u.cl.cert.cal.completedBy.personid)}">
																					<li>
																						<label><spring:message code="jicertificates.certificatepreviewed" />:</label>
																						<span>
																							<spring:message var="previewedText" code='jicertificates.string5' />
																							<a href="#" class="cross" onclick=" markCertificateAsPreviewed(this, ${u.cl.cert.certid}); return false; " title="${previewedText}">&nbsp;</a>
																						</span>
																					</li>
																				</c:if>
																				</c:when>
																			</c:choose>
																			<li>
																				<label class="labelhead bold"><spring:message code="capability" /></label>
																				<span>&nbsp;</span>	
																			</li>
																			<li>
																				<label><spring:message code="jicertificates.prevproc" />:</label>
																				<span>
																					<a href="#" onclick=" window.open('runproc.htm?runfile=${u.cl.cert.cal.xindiceKey}&calid=${u.cl.cert.cal.id}'); return false; " class="mainlink">
																						<spring:message code="jicertificates.viewproc" />
																					</a>
																				</span>
																			</li>
																			<li>
																				<label><spring:message code="jicertificates.unlockproc" />:</label>
																				<span>
																					<a href="#" onclick=" unlockProc('${u.cl.cert.cal.xindiceKey}', ${form.ji.jobItemId}); return false; ">
																						<img src="img/icons/lock_open.png" width="16" height="16" class="img_marg_bot" alt="Unlock Procedure" title="Unlock Procedure" />
																					</a>
																				</span>														
																			</li>																																																														
																			</c:when>
																			<c:when test="${u.cl.cert.cal.calProcess.process == 'Metcal'}" >
																				<li>
																					<label class="labelhead bold"><spring:message code="jicertificates.template" /></label>
																					<span>&nbsp;</span>
																				</li>
																				<li>
																					<label><spring:message code="jicertificates.metcaltempl" />:</label>
																					<span>
																						${u.cl.cert.docTemplateKey} &nbsp;
																					</span>
																				</li>
																			</c:when>
																		</c:choose>																																																									
																		
																		<c:if test="${(u.cl.cert.cal == null) || u.cl.cert.cal.calProcess.actionAfterIssue}" >
																			<li>
																				<label class="labelhead bold"><spring:message code="jicertificates.signopt" /></label>
																				<span>&nbsp;</span>
																			</li>
																			<c:if test="${u.cl.cert.status eq 'UNSIGNED_AWAITING_SIGNATURE' && u.accredited && (u.level == 'SIGN' || u.level == 'APPROVE')}">
																  				<li>
																  					<label><spring:message code="jicertificates.signcert"/>:</label>
																  					<span>
																  						<a href="#" class="mainlink" onclick=" $j(this).closest('ol').find('li[class^=\'sign\'], li[class^=\'password\'], li[class^=\'training\'], li[class^=\'template\']').removeClass('hid').addClass('vis'); $j('input#encpassword').focus(); return false; " >
																  							<spring:message code="jicertificates.sign" />...
																  						</a>
																  						<c:if test="${u.cl.cert.cal.calProcess.applicationComponent}">
																  							<span class="previewCertificateStatus">
																  								<c:choose>
																	  								<c:when test="${u.cl.cert.certPreviewed}" >
																	  									<spring:message var="previewedText" code="jicertificates.certprevbyengineer" />
																		  								<img src="img/icons/greentick.png" width="16" height="16" class="marg-left img_marg_bot" title="${previewedText}" />
																		  								${previewedText}
																	  								</c:when>
																	  								<c:otherwise>
																	  									<spring:message var="notPreviewedText" code="jicertificates.string7" />
																		  								<img src="img/icons/redcross.png" width="16" height="16" class="marg-left img_marg_bot" title="${notPreviewedText}" />
																		  								${notPreviewedText}
																	  								</c:otherwise>		
																  								</c:choose>
																  							</span>
																  						</c:if>
																  					</span>
																  				</li>
																  				
																  				<form:form action="" method="post" modelAttribute="form" enctype="multipart/form-data" onsubmit=" $j(this).find('input:submit').attr('disabled','disabled'); return true; ">
																  					<form:hidden path="currentContactId" />
																  					<c:if test="${(u.cl.cert.cal == null) || u.cl.cert.cal.calProcess.uploadAfterIssue}">
																		  				<li class="sign1 hid">
																							<label><spring:message code="jicertificates.certdoc" />:</label>
																							<div class="float-left">
																								<c:choose>
																									<c:when test="${u.cl.hasWordFile}">
																										${u.cl.cert.certno}.doc&nbsp;&nbsp;&nbsp;found in directory&nbsp;&nbsp;&nbsp;
																										<spring:message var="foundText" code="jicertificates.string8" />
																										<img src="img/icons/greentick.png" alt="${foundText}" title="${foundText}" height="16" width="16" />
																									</c:when>
																									<c:otherwise>
																										${u.cl.cert.certno}.doc&nbsp;&nbsp;&nbsp;not found in directory&nbsp;&nbsp;&nbsp;
																										<spring:message var="notFoundText" code="jicertificates.docnotfound" />
																										<img src="img/icons/redcross.png" alt="${notFoundText}" title="${notFoundText}" height="16" width="16" />
																										<br/><br/>Upload document:&nbsp;<br/>
																											<form:input type="file" path="file" />
																									</c:otherwise>
																								</c:choose>
																							</div>
																							<!-- clear floats and restore page flow -->
																							<div class="clear"></div>
																						</li>
																					</c:if>
																					<c:if test="${u.cl.cert.cal.calProcess.process == 'Metcal'}">
																						<li class="template1 hid">
																							<label><spring:message code="jicertificates.metcaltempl" />:</label>
																							<span>
																								<select name="template">
																									<c:forEach var="t" items="${metcaltemplates}">
																										<c:set var="selectedText" value="" />
																										<c:if test="${t == u.cl.cert.docTemplateKey}">
																											<c:set var="selectedText" value="selected='selected'" />
																										</c:if>
																										<option value="${t}" ${selectedText}>${t}</option>
																									</c:forEach>
																								</select>
																							</span>
																						</li>
																					</c:if>
																					<c:if test="${u.cl.cert.cal.calProcess.applicationComponent}">
																						<li class="template2 hid">
																							<label>CWMS <spring:message code="jicertificates.template" />:</label>
																							<div class="float-left width80 padtop">
																								<select name="template" class="width70">
																									<c:forEach var="t" items="${certtemplates}">
																										<c:set var="selectedText" value="" />
																										<c:if test="${t == u.cl.cert.docTemplateKey}">
																											<c:set var="selectedText" value="selected='selected'" />
																										</c:if>
																										<option value="${t}" ${selectedText}>${t}</option>
																									</c:forEach>
																								</select>
																								<p>
																									<a href="#" class="mainlink previewCertificate" onclick=" createCertificatePreview(this, ${u.cl.cert.certid}, ${u.cl.cert.cal.id}, $j(this).parent().prev().val()); return false; ">
																										<spring:message code="jicertificates.certprev" />
																									</a>
																								</p>
																							</div>
																							<div class="clear"></div>
																						</li>
																					</c:if>
																					<c:choose>
																						<c:when test="${form.pwExists == false}">
																							<li class="password1 hid">
																								<label class="labelhead width96"><span class="attention">${currentContact.name}, <spring:message code="jicertificates.string8" />:</span></label>
																								<span>&nbsp;</span>
																							</li>
																							<li class="password2 hid">
																									<form:label path="encPassword"><spring:message code="jicertificates.passw" />:</form:label>
																									<form:input type="password" path="encPassword" id="encpassword" onkeypress=" if (event.keyCode==13){ event.preventDefault(); } " onkeyup=" passwordStrength(this.value, $j(this).parent()); " />
																							</li>
																							<li class="password3 hid">
																									<form:label path="confirmPassword"><spring:message code="jicertificates.confpassw" />:</form:label>
																									<form:input type="password" path="confirmPassword" id="confirmpassword" onkeypress=" if (event.keyCode==13){ event.preventDefault(); } " />
																							</li>
																							<li class="password4 hid">
																								<label><spring:message code="jicertificates.strength" />:</label>
																								<div class="float-left padtop">
																									<div class="passwordDescription"><spring:message code="jicertificates.passwnotenter" /></div>
																                       				<div class="strength0">&nbsp;</div>
																									<!-- clear floats and restore page flow -->
																									<div class="clear"></div>
																								</div>
																								<!-- clear floats and restore page flow -->
																								<div class="clear"></div>
																							</li>											
																						</c:when>
																						<c:otherwise>
																							<li class="password5 hid">
																									<form:label path="encPassword"><spring:message code="jicertificates.passw" />:</form:label>
																									<form:input type="password" path="encPassword" id="encpassword" onkeypress="if (event.keyCode==13){event.preventDefault(); $j(this).closest('form').submit(); }" />
																							</li>
																						</c:otherwise>
																					</c:choose>
																					<c:choose>
																						<c:when test="${(u.level == 'APPROVE') && (isUserManager || isAdminUser) && (u.cl.cert.procTrainingRec == null)}">
																							<li class="training1 hid">
																								<label><spring:message code="jicertificates.traingcert" />:</label>																										
																								<input type="checkbox" name="procTrainingRecord" value="false" onclick=" if(this.checked){this.value = 'true'}else{this.value = 'false'} " />																										
																							</li>
																						</c:when>
																						<c:otherwise>
																							<input type="hidden" name="procTrainingRecord" value="false" />
																						</c:otherwise>
																					</c:choose>
																					<%-- Eclipse JSP syntax checker balks at form within fieldset; enclosing in <c:if> as workaround --%>
																					<c:if test="true">
	 																					<li class="sign2 hid">
																							<label>&nbsp;</label>
																							<input type="hidden" name="action" value="sign" />
																							<input type="hidden" name="actionForCertId" value="${u.cl.cert.certid}" />	
																							<input type="submit" value="Sign Certificate" />
																						</li>
																					</c:if>
																					
																				</form:form>
																			</c:if>	
																			
																			<%-- ONLY IF PDF EXISTS? (migrated from .vm comment) --%> 
																			<c:if test="${(u.cl.cert.status eq 'SIGNED_AWAITING_APPROVAL') && u.accredited && u.level == 'APPROVE'}">
																  				<li>
																  					<label><spring:message code="jicertificates.apprcert" />:</label>
																  					<span>
																  						<a href="#" class="mainlink" onclick=" $j(this).closest('ol').find('li[class^=\'sign\'], li[class^=\'password\'], li[class^=\'training\']').removeClass('hid').addClass('vis'); $j('input#approvePassword').focus(); return false; " >Approve...</a>
																  						<c:if test="${u.cl.cert.cal.calProcess.applicationComponent}">
																  							<span class="previewCertificateStatus">
																								<c:choose>
																									<c:when test="${u.cl.cert.certPreviewed}">
																										<spring:message var="textPreviewed" code="jicertificates.certprevbyengineer" />
																		  								<img src="img/icons/greentick.png" width="16" height="16" class="marg-left img_marg_bot" title="${textPreviewed}" />
																									</c:when>
																									<c:otherwise>
																		  								<spring:message var="textNotPreviewed" code="jicertificates.string7" />
																		  								<img src="img/icons/redcross.png" width="16" height="16" class="marg-left img_marg_bot" title="${textNotPreviewed}" />
																									</c:otherwise>
																								</c:choose>
																			  				</span>
																  						</c:if>
																  					</span>
																  				</li>
																  				<li class="password6 hid">
																					<label><spring:message code="jicertificates.passw" />:</label>
																					<input type="password" name="approvePassword" id="approvePassword" onkeypress="if (event.keyCode==13){$j('div#visibleBox input[type=\'button\'][id=\'approveButton\']').trigger('onclick'); return false;}" />
																				</li>
																				<c:choose>
																					<c:when test="${(isUserManager || isAdminUser) && (u.cl.cert.procTrainingRec == null)}">
																						<li class="training2 hid">
																							<label><spring:message code="jicertificates.traingcert" />:</label>																										
																							<input type="checkbox" name="approveTrainingCert" id="approveTrainingCert" />
																						</li>
																					</c:when>
																					<c:otherwise>
																						<input type="checkbox" name="approveTrainingCert" id="approveTrainingCert" class="hid" />
																					</c:otherwise>
																				</c:choose>
																				<li class="sign3 hid">
																					<label>&nbsp;</label>
																					<spring:message var="textApproveCert" code="jicertificates.apprcert" />
																					<input type="button" id="approveButton" onclick=" this.disabled = true; approveCert(${u.cl.cert.certid}, ${u.cl.jobItem.capability.id}, ${u.cl.jobItem.capability.reference}, $j('div#visibleBox input#approvePassword').val(), $j('div#visibleBox input#approveTrainingCert').is(':checked'), this); return false; " value="${textApproveCert}" />
																				</li>
																			</c:if>
																			<li>
																				<label><spring:message code="jicertificates.reset" />:</label>
																				<div class="float-left padtop">
																					<form:form action="" method="post">
																						<a href="#" class="mainlink" onclick=" event.preventDefault(); $j(this).parent('form').submit(); " ><spring:message code="jicertificates.resetcert" /></a>
																						<input type="hidden" name="action" value="reset" />
																						<input type="hidden" name="actionForCertId" value="${u.cl.cert.certid}" />																								
																					</form:form>
																				</div>
																				<!-- clear floats and restore page flow -->
																				<div class="clear"></div>
																			</li>
																			
																		</c:if>
																																																												
																		<li>
																			<label class="labelhead bold"><spring:message code="jicertificates.supplementary" /></label>
																			<span>&nbsp;</span>
																		</li>
																		<li>
																			<label><spring:message code="jicertificates.reissuecert" />:</label>
																			<div class="float-left padtop">
																				<c:choose>
																					<c:when test="${not hasSupplementaryCertPermission}" >
																						<spring:message code="notapplicable" /><c:out value=" : " />
																						<spring:message code="jicertificates.nopermission" />
																					</c:when>
																					<c:when test="${empty u.cl.cert.cal}">
																						<spring:message code="notapplicable" /><c:out value=" : " />
																						<spring:message code="jicertificates.nocalibrationlinked" />
																					</c:when>
																					<c:when test="${u.cl.cert.cal.organisation.subdivid ne allocatedSubdiv.key}">
																						<spring:message code="notapplicable" /><c:out value=" : " />
																						<spring:message code="jicertificates.nosubdivmatch" />
																					</c:when>
																					<c:otherwise>
																						<form:form action="createsupplementarycertificate.htm" method="post">
																							<a href="#" class="mainlink" onclick=" event.preventDefault(); $j(this).parent('form').submit(); " ><spring:message code="jicertificates.creatsupplcert" /></a>
																							<input type="hidden" name="jobitemid" value="${form.getJi().getJobItemId()}" />
																							<input type="hidden" name="certid" value="${u.cl.cert.certid}" />																								
																						</form:form>
																					</c:otherwise>
																				</c:choose>
																			</div>
																			<!-- clear floats and restore page flow -->
																			<div class="clear"></div>
																		</li>
																		
																		<c:if test="${(isUserManager || isAdminUser) && ((u.cl.cert.status == 'UNSIGNED_AWAITING_SIGNATURE') || (u.cl.cert.status eq 'SIGNED') && (u.cl.cert.procTrainingRec == null))}">
																			<li>
																				<label class="labelhead bold"><spring:message code="jicertificates.managefunc" /></label>
																				<span>&nbsp;</span>
																			</li>
																			<c:if test="${u.cl.cert.status eq 'UNSIGNED_AWAITING_SIGNATURE'}">
																				<li>
																					<label><spring:message code="jicertificates.delaycert" />:</label>
																					<div class="float-left padtop">
																						<form:form action="" method="post">
																							<a href="#" class="mainlink" onclick=" event.preventDefault(); $j(this).parent('form').submit(); $j(this).text('Delaying...Please Wait....'); " ><spring:message code="jicertificates.delaycert" /></a>
																							<input type="hidden" name="action" value="delay" />
																							<input type="hidden" name="actionForCertId" value="${u.cl.cert.certid}" />																							
																						</form:form>
																					</div>
																					<!-- clear floats and restore page flow -->
																					<div class="clear"></div>
																				</li>
																			</c:if>
																			<c:if test="${(u.cl.cert.status eq 'SIGNED') && (u.cl.cert.procTrainingRec == null)}">
																				<li>
																					<label><spring:message code="jicertificates.traincert" />:</label>
																					<div class="float-left padtop">
																						<a href="" class="mainlink" onclick="event.preventDefault(); markAsTrainingCertificate(${u.cl.cert.certid}, ${u.cl.jobItem.capability.id}, '${u.cl.jobItem.capability.reference}', this);" >
																							<spring:message code="jicertificates.marktraincert" />
																						</a>
																					</div>
																					<div class="clear"></div>
																				</li>
																			</c:if>
																		</c:if>
																																							
																		<c:if test='${isAdminUser and u.cl.cert.status ne "DELETED"}'>
																			<li>
																				<label class="labelhead bold"><spring:message code="jicertificates.adminfunct" /></label>
																				<span>&nbsp;</span>
																			</li>
																			<li>
																				<label><spring:message code="jicertificates.unisscert" />:</label>
																				<span>
																					<a href="#" class="mainlink" onclick=" unIssueCertificate(${u.cl.cert.certid}); return false; " ><spring:message code="jicertificates.delcert" /></a>
																				</span>
																			</li>
																		</c:if>
																	</ol>
																</fieldset>
																
																<div>&nbsp;</div>
																																		
								        					</div>
															<!-- end of thickbox content -->
														
														</div>
														<!-- end of certificate options div available if certificate is linked to a calibration -->																	
														
														<!-- certificate linking div displayed when certificate requires linking to a calibration -->
														<c:set var="hidOrInline" value="hid" />
														<c:if test="${u.cl.cert.cal == null}">
															<c:set var="hidOrInline" value="inline" />
														</c:if>
														
														<div class="${hidOrInline}">
														
															<!-- link which creates thickbox to show certificate options below -->
															<spring:message var="certRequiresLinkingText" code="jicertificates.string9" />
															<a href="#" onclick=" displayCertLinking('linkcert${u.cl.cert.certid}'); return false; ">
																<img src="img/icons/cert_link.png" width="20" height="16" class="img_inl_margleft" alt="${certRequiresLinkingText}" title="${certRequiresLinkingText}" />
															</a>
														
															<!-- contents of this div are displayed using link above -->
															<div id="linkcert${u.cl.cert.certid}" class="hid">
															
																<fieldset>
																	<ol>
																		<jobs:printCertificateLabelListItems serialno="${form.ji.inst.serialno}" u="${u}" labelPrinter="${labelPrinter}" useCertLinks="${useCertLinks}" />
																		<c:if test="${isAdminUser}">
																			<li>
																				<label class="labelhead bold"><spring:message code="jicertificates.adminfunct" /></label>
																				<span>&nbsp;</span>
																			</li>
																			<li>
																				<label><spring:message code="jicertificates.unisscert" />:</label>
																				<span>
																					<a href="#" class="mainlink" onclick=" unIssueCertificate(${u.cl.cert.certid}); return false; " >
																						<spring:message code="jicertificates.delcert" />
																					</a>
																				</span>
																			</li>
																		</c:if>
																	</ol>
																</fieldset>
															
																<table class="default4 jicalibrationtable" summary="">
																	<thead>
																		<tr>
																			<th class="calstart" scope="col"><spring:message code="jicalibration.startdatetime" /></th>
																			<th class="caltype" scope="col"><spring:message code="type" /></th>
																			<th class="calprocess" scope="col"><spring:message code="process" /></th>
																			<th class="calstds" scope="col"><spring:message code="jicalibration.stds" /></th>
																			<th class="calcerts" scope="col"><spring:message code="jicalibration.certs" /></th>
																			<th class="calproc" scope="col"><spring:message code="capability" /></th>
																			<th class="calclass" scope="col"><spring:message code="jicertificates.class" /></th>
																			<th class="calstatus" scope="col"><spring:message code="status" /></th>
																			<th class="caltolink" scope="col"><spring:message code="jicertificates.link" /></th>
																		</tr>
																	</thead>
																	<tbody>
																		<c:forEach var="cl" items="${form.ji.calLinks}">
																			<c:set var="c" value="${cl.cal}"/>
																			<c:if test="${empty c.certs}">
																				<tr>
																					<td class="calstart">
																						 <fmt:formatDate value="${c.startTime}" dateStyle="medium" type="both" timeStyle="short"/>
																						<c:if test="${c.startedBy != null}">
																							${c.startedBy.shortenedName}
																						</c:if>																							
																					</td>
																					<td class="caltype"><cwms:besttranslation translations="${c.calType.serviceType.shortnameTranslation}" /></td>
																					<td class="calprocess">${c.calProcess.process}</td>
																					<td class="calstds">${c.standardsUsed.size()}</td>
																					<td class="calcerts">${c.certs.size()}</td>
																					<td class="calproc">${c.capability.reference} - ${c.capability.name}</td>
																					<td class="calclass"><t:fill field="${c.calClass.desc}" /></td>
																					<td class="calstatus"><cwms:besttranslation translations="${c.status.nametranslations}" /></td>
																					<td class="caltolink">
																						<a href="#" class="mainlink" onclick=" linkCertificateToCalibration(${u.cl.cert.certid}, ${c.id}); return false; ">
																							<img src="img/icons/link.png" width="16" height="16" class="img_marg_bot" /> <spring:message code="jicertificates.link" />
																						</a>
																					</td>
																				</tr>
																			</c:if>
																		</c:forEach>
																	</tbody>
																</table>
																
															</div>
															<!-- end of div in which calibrations to be linked are displayed -->
															
														</div>
														<!-- end of certificate linking div displayed when certificate requires linking to a calibration -->
																									       					
								       					<!-- hover over this anchor and a tooltip is displayed showing the contents of the div created below -->
								       					<spring:message var="linktext" code="jicertificates.string10" arguments="${u.cl.cert.certno}" />
														&nbsp;
														<a href="?id=#certActions${u.cl.cert.certid}&amp;width=440" onclick=" return false; " title="${linkText}" id="tip${u.cl.cert.certid}" class="jTip mainlink">
															<img src="img/icons/help.png" width="16" height="16" alt="" />
														</a>
														&nbsp;
														<!-- contents of this div are displayed in tooltip created above -->
														<div id="certActions${u.cl.cert.certid}" class="hid">
															<div class="padding">
																<table class="default4">
										       						<c:forEach var="action" items="${u.cl.cert.actions}">
										       							<tr>
											       							<td>${action.action.description}</td>
											       							<td>${action.actionBy.name}</td>
											       							<td>
											       								<fmt:formatDate value="${action.actionOn}" type="both" dateStyle="SHORT" timeStyle="SHORT" />
											       							</td>
										       							</tr>
										       						</c:forEach>
										       					</table>
															</div>
														</div>
													</td>
													<td class="jic_externalno">
														${u.cl.cert.thirdCertNo }
													</td>
													<td class="jic_certnote">
														<links:showNotesLinks noteTypeId="${u.cl.cert.certid}" noteType="CERTNOTE" colspan="10" notecounter="${u.cl.cert.privateActiveNoteCount}" />
													</td>
													<td class="jic_caldate"><fmt:formatDate value="${u.cl.cert.calDate}" dateStyle="medium" type="date" /></td>
							        				<td class="jic_issuedate"><fmt:formatDate value="${u.cl.cert.certDate}" dateStyle="medium" type="date" /></td>
							        				<td class="jic_duration">
														<c:choose>
															<c:when test="${u.cl.cert.duration != null}">
																${u.cl.cert.duration}&nbsp;
<%-- 																<c:choose> --%>
<%--                                                                     <c:when test="${u.cl.cert.unit != null}"> --%>
<%--                                                                         ${u.cl.cert.unit.getName(u.cl.cert.duration)} --%>
<%--                                                                     </c:when> --%>
<%--                                                                     <c:otherwise> --%>
<%--                                                                         <spring:message code="months" /> --%>
<%--                                                                     </c:otherwise> --%>
<%--                                                                 </c:choose> --%>
															</c:when>
															<c:otherwise>
																<spring:message code="companyedit.na" />
															</c:otherwise>
														</c:choose>
							        				</td>
							       					<td class="jic_files">
														<c:choose>
															<c:when test="${not empty u.cl.certFiles}">
							       								<c:forEach var="f" items="${u.cl.certFiles}">
							       									<c:url var="downloadURL" value="downloadfile.htm"><c:param name="file" value="${EncryptionTools.encrypt(f.absolutePath)}" /></c:url>
							       									<a href='${downloadURL}' class="mainlink" target="_blank">${f.name}</a><br/>
							       								</c:forEach>
															</c:when>
															<c:otherwise>
																<spring:message code="jicertificates.string16" />
															</c:otherwise>
														</c:choose>
							          				</td>
							       					<td class="jic_caltype">
							       						<cwms:besttranslation translations="${u.cl.cert.calType.serviceType.shortnameTranslation}" />
							       					</td>
													<td class="jic_regby">${u.cl.cert.registeredBy.name}</td>
													<td class="jic_process">
														<c:choose>
															<c:when test="${u.cl.cert.cal.calProcess != null}">
																${u.cl.cert.cal.calProcess.process}
															</c:when>
															<c:otherwise>
																<spring:message code="jicertificates.string11" />
															</c:otherwise>
														</c:choose>
													</td>
													<c:if test="${calibrationVerification}">
														<td class="jic_calver">
															${u.cl.cert.calibrationVerificationStatus.getName()}
														</td>
													</c:if>
													<td class="jic_status">
														<c:out value="${u.cl.cert.status.getMessage()}" />
														<c:if test="${u.cl.cert.procTrainingRec != null}">
															(PTR for <a href="viewcapability.htm?capabilityId=${u.cl.jobItem.capability.id}&loadtab=proctraining-tab" class="mainlink" target="_blank">${u.cl.jobItem.capability.reference}</a>)
														</c:if>
													</td>
													<td class="jic_properties">
														<cwms:yesno flag="${u.cl.cert.restriction}"/>
													</td>
													<td class="jic_properties">
														<cwms:yesno flag="${u.cl.cert.adjustment}"/>
													</td>
													<td class="jic_properties">
														<cwms:yesno flag="${u.cl.cert.optimization}"/>
													</td>
													<td class="jic_properties">
														<cwms:yesno flag="${u.cl.cert.repair}"/>
													</td>
													<td class="jic_properties">
													${ u.cl.cert.status.fullName }
														<c:if test="${ u.cl.cert.getHasCertFile() and  (u.cl.cert.status eq 'SIGNED' or u.cl.cert.status eq 'SIGNED_AWAITING_APPROVAL' or u.cl.cert.status eq 'SIGNING_NOT_SUPPORTED' or u.cl.cert.status eq 'TO_BE_REPLACED') }">
															<a href="#" onclick=" certificateReplacement(${form.ji.jobItemId}, ${u.cl.cert.certid}, '${ certType }'); return false; " title="" id="" class="jTip mainlink">
																<img src="img/icons/cert_move.png" width="16" height="16" alt="" />
															</a>
														</c:if>
													</td>
											  	</tr>															  	
											  	
												<tr id="CERTNOTE${u.cl.cert.certid}" class="hid">
													<td colspan="${colspan}" class="nopadding">
														<c:if test="${not empty u.cl.cert.notes}" >
															<t:showActiveNotes entity="${u.cl.cert}" links="true"  noteType="CERTNOTE" noteTypeId="${u.cl.cert.certid}" privateOnlyNotes="${privateOnlyNotes}" contact="contact" />
														</c:if>
													</td>
												</tr>
											</c:if>
										</c:forEach>
									</c:otherwise>
								</c:choose>			        			
							</tbody>
						</table>
						<!-- end of table displaying all job item certificates -->
																										
						<c:set var="tpCount" value="0"/>
		      			<c:forEach var="u" items="${form.userCertWraps}">
				  			<c:if test="${u.cl.cert.type == 'CERT_THIRDPARTY'}">
				  				<c:set var="tpCount" value="${tpCount + 1}" />
				  			</c:if>
				  		</c:forEach>
						
						<!-- table displaying all third party job item certificates -->
						<spring:message var="textSummary" code="jicertificates.string12" />
						<table class="default2" id="jithirdcerttable" summary="${textSummary}">
							<thead>
								<tr>
									<td colspan="${colspan2}"><spring:message code="jicertificates.thirdpartycerts" /> (${tpCount})</td>
								</tr>
								<tr>
				            		<th class="jitc_certno"><spring:message code="jicalibration.certno" /></th>
							        <th class="jitc_caldate"><spring:message code="jicertificates.caldate" /></th>
							        <th class="jitc_issuedate"><spring:message code="jicertificates.issdate" /></th>
							        <th class="jitc_duration"><spring:message code="jicertificates.duration" /></th>
									<th class="jitc_files"><spring:message code="jicertificates.files" /></th>
							        <th class="jitc_caltype"><spring:message code="type" /></th>
							        <th class="jitc_regby"><spring:message code="jicertificates.tpcertno" /></th>
							        <c:if test="${calibrationVerification}">
								        <th class="jic_calver"><spring:message code="viewinstrument.calverificationstatus" /></th>
									</c:if>
							        <th class="jitc_status"><spring:message code="status" /></th>
									<th class="jic_properties"><spring:message code="exchangeformat.fieldname.restriction" /></th>
									<th class="jic_properties"><spring:message code="exchangeformat.fieldname.adjustment" /></th>
									<th class="jic_properties"><spring:message code="exchangeformat.fieldname.optimization" /></th>
									<th class="jic_properties"><spring:message code="exchangeformat.fieldname.repair" /></th>
									<th class="jic_properties"><spring:message code="jicertificates.cancelandreplace" /></th>
								</tr>
							</thead>
			        		<tfoot>
			        			<tr>
			        				<td colspan="${colspan2}">&nbsp;</td>
			        			</tr>
			        		</tfoot>
			        		<tbody>
								<c:choose>
									<c:when test="${tpCount < 1}">
										<tr>
								        	<td colspan="${colspan2}" class="center bold"><spring:message code="jicertificates.string13" /></td>
								      	</tr>
									</c:when>
									<c:otherwise>
							  			<c:forEach var="u" items="${form.userCertWraps}">
							  				<c:if test="${u.cl.cert.type == 'CERT_THIRDPARTY'}">
							      				<tr>
							        				<td>
							        					<spring:message var="addToClipboardText" code="jicertificates.addcerttoclipb" />
							        					<a href="viewcertificate.htm?certid=${u.cl.cert.certid}" class="mainlink">${u.cl.cert.certno}</a> <img src="img/icons/bullet_clipboard.png" width="10" height="10" onclick=" $j(this).prev().clipBoard(this, null, false, null); " alt="${addToClipboardText}" title="${addToClipboardText}" />
							        					
														<!-- link which creates thickbox to show certificate options below -->
														<spring:message var="certOptionsForText" code="jicertificates.string3" />
														<a href="#TB_inline?height=550&width=780&inlineId=cert${u.cl.cert.certid}" class="thickbox"><img src="img/icons/cert_edit.png" width="20" height="16" class="img_inl_margleft" alt="${certOptionsForText} ${u.cl.cert.certno}" title="${certOptionsForText} ${u.cl.cert.certno}" /></a>
			
														<!-- contents of this div are displayed in thickbox link above -->
														<div id="cert${u.cl.cert.certid}" class="hid">
															
															<fieldset>
																<legend><spring:message code="jicertificates.string14"/></legend>
																<ol>
																	<li>
																		<label class="labelhead bold"><spring:message code="jicertificates.thirdparty"/></label>
																		<span>&nbsp;</span>
																	</li>
																	<li>
																		<label><spring:message code="jicertificates.thirdpartycomp"/>:</label>
																		<span>
																			<t:fill field="${u.cl.cert.thirdDiv.comp.coname}"/>
																		</span>
																	</li>
																	<li>
																		<label><spring:message code="jicertificates.thirdpartysubdiv"/>:</label>
																		<span>
																			<t:fill field="${u.cl.cert.thirdDiv.subname}" />
																		</span>
																	</li>
																	<li>
																		<label><spring:message code="jicertificates.thirdpartycertno"/>:</label>
																		<span>
																			<t:fill field="${u.cl.cert.thirdCertNo}" />
																		</span>
																	</li>
																	<li>
																		<label class="labelhead bold"><spring:message code="jicertificates.printing"/></label>
																		<span>&nbsp;</span>
																	</li>
																	<c:if test="${empty form.ji.inst.serialno}">
																		<li>
																			<span class="attention bold"><spring:message code="jicertificates.string4"/></span>
																		</li>
																	</c:if>
																	<li>
																		<label><spring:message code="jicertificates.accreditedlabel"/>:</label>
																		<span><spring:message code="${u.labelSettings.accredited}"/></span>
																	</li>
																	<c:if test="${u.labelSettings.accredited}">
																		<li>
																			<label><spring:message code="jicertificates.accreditationbody"/>:</label>
																			<span>${u.labelSettings.accreditedLab.accreditationBody.shortName}</span>
																		</li>
																		<li>
																			<label><spring:message code="jicertificates.accreditationnumber"/>:</label>
																			<span>${u.labelSettings.accreditedLab.labNo}</span>
																		</li>
																	</c:if>
																	<li>
																		<label><spring:message code="jicertificates.labelcertificatenumber"/>:</label>
																		<span><spring:message code="${u.labelSettings.labelCertificateNumber}"/></span>
																	</li>
																	<li>
																		<label><spring:message code="jicertificates.labelplantnumber"/>:</label>
																		<span><spring:message code="${u.labelSettings.labelPlantNumber}"/></span>
																	</li>
																	<li>
																		<label><spring:message code="jicertificates.labelrecalldate"/>:</label>
																		<span>${u.labelSettings.labelRecallDate}</span>
																	</li>
																	<li>
																		<label><spring:message code="jicertificates.thirdpartylbl"/>:</label>
																		<span>
																			<jobs:printOrCreateRegularCertificateLabel u="${u}" labelPrinter="${labelPrinter}" useCertLinks="${useCertLinks}" />
																		</span>
																	</li>
																	<li>
																		<label><spring:message code="jicertificates.smallthirdpartylbl"/>:</label>
																		<span>
																			<jobs:printOrCreateSmallCertificateLabel u="${u}" labelPrinter="${labelPrinter}" useCertLinks="${useCertLinks}" />
																		</span>
																	</li>
																	<li>
																		<label class="labelhead bold"><spring:message code="jicertificates.uplcert"/></label>
																		<span>&nbsp;</span>
																	</li>
																	<li>
																		<label><spring:message code="jicertificates.uplcert"/>:</label>
																		<div class="float-left width80">
																			<c:if test="${u.cl.hasCertFile}">
																				<span class="attention"><spring:message code="jicertificates.string15"/></span>
																				<br/>
																			</c:if>
																			<files:fileUpload systemComponentId="${form.component.componentId}" thisEntityId1="${form.ji.job.jobno}" thisEntityId2="" rootFiles="${scRootFiles}" count="1" renameTo="${u.cl.cert.certno}" />
																		</div>
																		<!-- clear floats and restore page flow -->
																		<div class="clear"></div>
																	</li>
																</ol>
															</fieldset>
																																	
							        					</div>
														<!-- end of thickbox content -->
														
														<!-- contents of this div are displayed in tooltip created below -->
														<div id="certnote${u.cl.cert.certid}" class="hid">
															<fieldset>
																<legend><spring:message code="jicertificates.notesfor"/>${u.cl.cert.certno}</legend>
																<ol>
																	<c:set var="noteCount" value="0"/>
										       						<c:forEach var="n" items="${u.cl.cert.notes}">
										       							<li>
																			<div class="inlinenotes_note">
																				${n.note} 
																				<br />
																				(${n.setBy.name} - <fmt:formatDate value="${n.setOn}" dateStyle="medium" type="date"/>)
																			</div>
																			<div class="inlinenotes_edit">
																				&nbsp;
																			</div>
																			<!-- clear floats and restore page flow -->
																			<div class="clear-0" ></div>
																		</li>
																		<c:set var="noteCount" value="${noteCount + 1}"/>
										       						</c:forEach>
									       						</ol>
															</fieldset>
														</div>
														<!-- end of tooltip content -->
														<c:if test="${noteCount > 0}">
															<!-- hover over this anchor and a tooltip is displayed showing the contents of the div created below -->
															<spring:message var="textNotesFor" code="jicertificates.notesfor" />
															<a href="?id=#certnote${u.cl.cert.certid}&amp;width=500" onclick=" return false; " title="${textNotesFor} ${u.cl.cert.certno}" id="tipcertnote${u.cl.cert.certid}" class="jTip">
																<img src="img/icons/note.png" width="16" height="16" class="img_inl_margleft" alt="${textNotesFor} ${u.cl.cert.certno}" title="${textNotesFor} ${u.cl.cert.certno}" >
															</a>
								       					</c:if>
								       					
								       					<!-- hover over this anchor and a tooltip is displayed showing the contents of the div created below -->
								       					<spring:message var="textCertificateHistory" code="jicertificates.string10" arguments="${u.cl.cert.certno}"/>
														&nbsp;
														<a href="?id=#certActions${u.cl.cert.certid}&amp;width=440" onclick=" return false; " title="${textCertificateHistory}" id="tip${u.cl.cert.certid}" class="jTip mainlink">
															<img src="img/icons/help.png" width="16" height="16" alt=""${textCertificateHistory} />
														</a>
														&nbsp;
														<!-- contents of this div are displayed in tooltip created above -->
														<div id="certActions${u.cl.cert.certid}" class="hid">
															<div class="padding">
																<table class="default4">
										       						<c:forEach var="action" items="${u.cl.cert.actions}">
										       							<tr>
											       							<td>${action.action.description}</td>
											       							<td>${action.actionBy.name}</td>
											       							<td>
											       								<fmt:formatDate value="${action.actionOn}" type="both" dateStyle="SHORT" timeStyle="SHORT" />
											       							</td>
										       							</tr>
										       						</c:forEach>
										       					</table>
															</div>
														</div>
													</td>
							        				<td class="center"><fmt:formatDate value="${u.cl.cert.calDate}" dateStyle="medium" type="date" /></td>
							        				<td class="center"><fmt:formatDate value="${u.cl.cert.certDate}" dateStyle="medium" type="date" /></td>
							        				<td class="center">
							        					${u.cl.cert.duration} 	
                                                    </td>
													<td>
														<c:choose>
															<c:when test="${u.cl.certFiles.size() > 0}">
							       								<c:forEach var="f" items="${u.cl.certFiles}">
							       									<c:url var="downloadURL" value="downloadfile.htm"><c:param name="file" value="${EncryptionTools.encrypt(f.absolutePath)}" /></c:url>
							       									<a href='${downloadURL}' class="mainlink" target="_blank">${f.name}</a><br/>
							       								</c:forEach>
															</c:when>
															<c:otherwise>
							       								<spring:message code="jicertificates.string16" />
															</c:otherwise>
														</c:choose>
							          				</td>
							       					<td class="center"><cwms:besttranslation translations="${u.cl.cert.calType.serviceType.shortnameTranslation}"/></td>
													<td><t:fill field="${u.cl.cert.thirdCertNo}" /></td>
													<c:if test="${calibrationVerification}">
														<td class="jic_calver">
															${u.cl.cert.calibrationVerificationStatus.getName()}
														</td>
													</c:if>
													<td><c:out value="${u.cl.cert.status.getMessage()}" /></td>
													<td class="jic_properties">
														<cwms:yesno flag="${u.cl.cert.restriction}"/>
													</td>
													<td class="jic_properties">
														<cwms:yesno flag="${u.cl.cert.adjustment}"/>
													</td>
													<td class="jic_properties">
														<cwms:yesno flag="${u.cl.cert.optimization}"/>
													</td>
													<td class="jic_properties">
														<cwms:yesno flag="${u.cl.cert.repair}"/>
													</td>
													<td class="jic_properties">
														<c:if test="${ u.cl.cert.getHasCertFile() and (u.cl.cert.status eq 'SIGNED' or u.cl.cert.status eq 'SIGNED_AWAITING_APPROVAL' or u.cl.cert.status eq 'SIGNING_NOT_SUPPORTED' or u.cl.cert.status eq 'TO_BE_REPLACED') }">
															<a href="#" onclick=" certificateReplacement(${form.ji.jobItemId}, ${u.cl.cert.certid}, 'THIRDPARTY_CERT'); return false; " title="" id="" class="jTip mainlink">
																<img src="img/icons/cert_move.png" width="16" height="16" alt="" />
															</a>
														</c:if>
													</td>
										      	</tr>
									  		</c:if>
								  		</c:forEach>
									</c:otherwise>
								</c:choose>
							</tbody>
						</table>
						<!-- end of table displaying all third party job item certificates -->
						
					</div>
					<!-- end of div containing all existing certificates -->
					
					<!-- div containing form elements for adding new certificate -->
					<div id="newcert-tab" <c:if test="${!hasErrors}">class="hid"</c:if>>
						<form:form id="newcertform" enctype="multipart/form-data" name="newcertform" method="post" action="" modelAttribute="form">
						
							<fieldset>
								
								<legend><spring:message code="jicertificates.string17" /></legend>
								
								<ol>
									<li>
				        				<label><spring:message code="jicertificates.thirdpartycertno" />:</label>
					        				<form:input path="c.thirdCertNo" type="text" id="c.thirdCertNo" />
					        				<form:errors path="c.thirdCertNo" cssClass="attention"/>
				        			</li>
				        			<li>
										<label><spring:message code="jicertificates.thirdparty" />:</label>
										<div class="float-left">													
											<!-- this div creates a new cascading search plugin. The default search contains an input 
												 field and results box for companies, when results are returned this cascades down to
												 display the subdivisions within the selected company. This default behavoir can be obtained
												 by including the hidden input field with no value or removing the input field. You can also
												 add address and/or contact searches to the cascade by including the values 'contact' and/or 'address'
												 in the hidden inputs value attribute separated by a comma (e.g value="contact", value="address",
												 value="address,contact").  You can also swap these values around (e.g. value="contact,address").  
											 -->									
											<div id="cascadeSearchPlugin">
												<input type="hidden" id="cascadeRules" value="subdiv" />
												<input type="hidden" id="compCoroles" value="supplier,business" />
												<c:if test="${not empty mostRecentTpSubdiv}">
													<input type="hidden" id="prefillIds" value="${mostRecentTpSubdiv.comp.coid},${mostRecentTpSubdiv.subdivid}" />
												</c:if>
											</div>
										</div>
										<!-- clear floats and restore page flow -->
										<div class="clear-0">&nbsp;</div>
									</li>
					        		<li>
											<form:label path="c.calDate"><spring:message code="caldate" />:</form:label>
											<form:input type="date" id="calDate" path="c.calDate"  />
											<form:errors path="c.calDate" cssClass="attention"/>
									</li>
									<li>	
											<form:label path="c.certDate"><spring:message code="jicertificates.issdate" />:</form:label>
											<form:input type="date" id="certDate" path="c.certDate"  />
											<form:errors path="c.certDate" cssClass="attention"/>
									</li>
									<li>
										<label><spring:message code="caltype" />:</label>
											<form:select path="calTypeId">
												<c:forEach var="ct" items="${form.calTypes}">
													<c:set var="selectedText" value="" />
													<c:if test="${ct.calTypeId == form.calTypeId}">
														<c:set var="selectedText" value=" selected='selected'" />
													</c:if>
													<option value="${ct.calTypeId}" ${selectedText}>
														<cwms:besttranslation translations="${ct.serviceType.shortnameTranslation}"/>
													</option>
												</c:forEach>
											</form:select>
												<form:errors path="calTypeId" cssClass="attention"/>
									</li>
									<li>
											<form:label path="c.duration"><spring:message code="jicertificates.duration" />:</form:label>
											<form:input path="c.duration" type="text" size="4" maxlength="4" /> 
											<form:errors path="c.duration" cssClass="attention"/>
										<form:select path="durationUnitId" items="${units}" itemLabel="value" itemValue="key"/>
									</li>
									<li>
										<label><spring:message code="editclientcert.remarks"/>:</label>
										<form:textarea path="c.remarks" rows="5" cols="80"/>
									</li>
									<li>
										<label><spring:message code="jicertificates.uplfile" />:</label>
											<form:input type="file" path="file" />
									</li>
									<li>
										<label><spring:message code="jicertificates.supplementaryfor" />:</label>
										<form:select path="supplementaryForId" items="${ tpCertificates }" onchange="supplementaryForChange($j(this).val());"></form:select>
									</li>
									<li id="supplementaryForCommentInput" class="hid">
										<label><spring:message code="system.comment" />:</label>
										<form:textarea path="supplementaryForComment" rows="3" cols="45" value="" />
									</li>
									<c:if test="${actionoutcomes != null}">
										<li>
											<label><spring:message code="jicertificates.furthwork" />:</label>
												<form:select path="actionOutcomeId">
													<c:forEach var="ao" items="${actionoutcomes}">
													<c:set var="selectedText" value="" />
														<c:if test="${ao.defaultOutcome}">
															<c:set var="selectedText" value=" selected='selected'" />
														</c:if>
														<option value="${ao.id}" ${selectedText}>
															<cwms:besttranslation translations="${ao.translations}"/>
														</option> 
													</c:forEach>
												</form:select>
										</li>
									</c:if>
									<c:if test="${calibrationVerification}">
										<li>
											<label><spring:message code="viewinstrument.calverificationstatus" /></label>
											<form:select path="calibrationVerificationStatus"
														 items="${verificationstatuses}"
														 itemLabel="value" itemValue="key"
														 disabled="${(caneditverificationstatus != true) ? 'true' : 'false'}"/>
										</li>
									</c:if>
									<c:if test="${deviationAllowed}">
										<li>
											<label><spring:message code="certificate.deviation"/></label>
											<form:input path="deviation" id="deviation"/>
										</li>
									</c:if>
									<li>
											<form:label path="note"><spring:message code="jicertificates.note"/>:</form:label>
											<form:textarea id="note" path="note" rows="3" cols="45" value="${form.note}" />
											<form:errors path="note" class="attention"/>
									</li>
									<li>
										<label><spring:message code="jicertificates.othitems" />:</label>
										<div class="float-left padtop" id="items">
																		
											<div class="marg-bot">													
												<input type="checkbox" onclick=" selectAllItemBoxes(this.checked); " />
												<spring:message code="jiactions.all" />
											</div>
											<table class="default2">
												<thead>
													<tr>
														<th width="5%">&nbsp;</th>
														<th width="10%"><spring:message code="itemno" /></th>
														<th width="40%"><spring:message code="description" /></th>
														<th width="15%"><spring:message code="serialno" /></th>
														<th width="15%"><spring:message code="plantno" /></th>
														<th width="15%"><spring:message code="group" /></th>
													</tr>
												</thead>
												<tbody>
													<c:forEach var="ji" items="${form.ji.job.items}">
														<tr>
															<td>
																<c:choose>
																	<c:when test="${ji.jobItemId != form.ji.jobItemId}">
																		<input type="checkbox" name="otherJobItems" value="${ji.jobItemId}" />																	
																	</c:when>
																	<c:otherwise>
																		<input type="checkbox" style=" visibility: hidden; " />
																	</c:otherwise>
																</c:choose>
															</td>
															<td>
																<links:jobitemLinkDWRInfo jobitem="${ji}" jicount="false" rowcount="0" />
															</td>
															<td>${ji.inst.definitiveInstrument}</td>
															<td><t:fill field="${ji.inst.serialno}"/></td>
															<td><t:fill field="${ji.inst.plantno}"/></td>
															<td>
																<c:if test="${not empty ji.group}">
																	${ji.group.id}
																</c:if>
															</td>
														</tr>
													</c:forEach>
												</tbody>
											</table>
											
										</div>
										<!-- clear floats and restore page flow -->
										<div class="clear"></div>
																								
									</li>
									<li>
							     		<label>&nbsp;</label>
							     		<spring:message var="addCertText" code="jicertificates.addcert" />
							     		<input type="submit" name="certadd" value="${addCertText}" />
							     		<input type="hidden" name="action" value="new" />
									</li>
								</ol>
								
							</fieldset>
										
						</form:form>
																						
					</div>
					<!-- end of div containing new cert form elements -->
					
				</div>
				<!-- end of div to hold all tabbed submenu content -->
					
			</div>
			<!-- end of div to hold tabbed submenu -->
				
			<!-- this div clears floats and restores page flow -->
			<div class="clear"></div>
		
		</div>
		<!-- end of infobox -->
							
		<!-- End of page specific code -->
	</jsp:body>
</jobs:jobItemTemplate>