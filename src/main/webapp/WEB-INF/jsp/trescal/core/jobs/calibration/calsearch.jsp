<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="calsearch.headtext"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script>
			// Used in procedure JQUI autocomplete
			var procedureSubdivId = ${allocatedSubdiv.key};		
		</script>
		<script src='script/trescal/core/jobs/calibration/CalSearch.js'></script>
	</jsp:attribute>
	<jsp:body>
		<t:showErrors path="form.*" showFieldErrors="true"/>
		<spring:message code="calsearch.any" var="translatedAny"/>
		
		<div class="infobox">
			<form:form modelAttribute="form" id="certsearchform">
				<fieldset>
					<legend><spring:message code="calsearch.headtext"/></legend>
					<!-- displays content in left column -->
					<div class="displaycolumn-60">
						<ol>											
							<li>
								<label><spring:message code="calsearch.jobno"/>:</label>
								<form:input path="jobNo" tabindex="1" />
							</li>
							<li>
								<label><spring:message code="calsearch.capability"/>:</label>
								<%-- JQueryUI Autocomplete field --%>
		                        <form:input path="procedureReferenceAndName" tabindex="2" />
		                        <form:input path="procedureId" type="hidden" />                       
								<span class="attention"><form:errors path="procedureId" /></span>
							</li>
							<li>
								<label><spring:message code="calsearch.startby"/>:</label>
								<div id="contSearchPlugin">
									<input type="hidden" id="contCoroles" value="business" />
									<input type="hidden" id="contindex" value="3" />
								</div>
								<!--  clear floats and restore page flow  -->
								<div class="clear"></div>
							</li>
							<li>
								<label><spring:message code="calsearch.startdate"/>:</label>												
								<form:select path="startDateBetween" onchange=" changeStartDateOption(this.value); return false; ">
									<option value="false"><spring:message code="calsearch.on"/></option>
									<option value="true"><spring:message code="calsearch.between"/></option>
								</form:select>
							
								<form:input path="startDate1" type="date" id="startDate1" tabindex="5" />
								<span id="startDateSpan" ${(form.startDateBetween == true) ? 'class="vis"' : 'class="hid"'}>
									&nbsp;<spring:message code="and" />&nbsp;
									<form:input path="startDate2" type="date" id="startDate2" tabindex="6" />
								</span>
							</li>
							<li>
								<label><spring:message code="calsearch.depart"/>:</label>
								<form:select path="deptId" id="deptId" tabindex="7" >
									<form:option value="" label="${translatedAny}"/>
									<%-- Currently just allocated subdiv's depts shown --%>
									<optgroup label="${allocatedCompany.value} - ${allocatedSubdiv.value}">
										<c:forEach var="d" items="${depts}">
											<form:option value="${d.deptid}" label="${d.name}"/>
										</c:forEach>
									</optgroup>
								</form:select>
							</li>
							<li>
								<label>&nbsp;</label>	
								<input type="submit" value='<spring:message code="search"/>' tabindex="13" />
							</li>
						</ol>
					</div>
					<!-- end of left column -->
										
					<!-- displays content in right column -->
					<div class="displaycolumn-40">
					
						<ol>
							<li>
								<label><spring:message code="calsearch.barcode"/>:</label>
								<form:input path="barcode" tabindex="8" />
							</li>
							<li>
								<label><spring:message code="calsearch.standbarcode"/>:</label>
								<form:input path="standardBarcode" tabindex="9" />
							</li>
							<li>
								<label><spring:message code="calsearch.status"/>:</label>
								<form:select path="statusId" tabindex="10" >
									<form:option value="" label="${translatedAny}"/>
									<c:forEach var="cs" items="${calstatuses}">
										<option value="${cs.statusid}"><cwms:besttranslation translations="${cs.nametranslations}"/></option>
									</c:forEach>
								</form:select>
							</li>
							<li>
								<label><spring:message code="calsearch.casltype"/>:</label>
								<form:select path="calTypeId" id="calTypeId" tabindex="11" >
									<form:option value="" label="${translatedAny}"/>
									<c:forEach var="ct" items="${caltypes}">
										<option value="${ct.calTypeId}"><cwms:besttranslation translations="${ct.serviceType.shortnameTranslation}"/></option>
									</c:forEach>
								</form:select>
							</li>
							<li>
								<label><spring:message code="calsearch.calproc"/>:</label>
								<form:select path="calProId" tabindex="12" >
									<form:option value="" label="${translatedAny}"/>
									<form:options items="${calprocesses}" itemValue="id" itemLabel="process"/>
								</form:select>
							</li>
						</ol>
					</div>
					<!-- end of right column -->
				</fieldset>
		   </form:form>
		</div>
	    
	</jsp:body>
	    
</t:crocodileTemplate>
	    