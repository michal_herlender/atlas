<%-- File name: /trescal/core/jobs/jobitem/jicalibration.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="org.trescal.cwms.core.tools.EncryptionTools" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs"%>
<%@ taglib prefix="addr" tagdir="/WEB-INF/tags/address"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<jobs:jobItemTemplate>
	<jsp:attribute name="subScript">
		<script src='script/trescal/core/jobs/jobitem/JICalibration.js'></script>
		<script src='script/trescal/core/documents/ImageGalleryFunctions.js'></script>
		<script>
			//grab scroll to parameter passed when using job item key navigation
			var scrollto = '${request.getParameter("scrollto")}';
			
			var intervalUnits = [];
			<c:forEach var="unit" items="${units}">
				intervalUnits.push({id: '${unit.key}', name: '${unit.value}'})
			</c:forEach>
			
		</script>
	</jsp:attribute>
	<jsp:body>
		<!-- infobox is styled with nifty corners -->
		<div class="infobox">
			<table class="default2 jicalibrationtable" summary="This table contains details of any calibrations for this item">
				<thead>
					<tr>
						<td colspan="9">
							<spring:message code="viewjob.calibrations"/>
							(${form.ji.calLinks.size()})
							<c:if test="${viewWrapper.readyForCalibration}">
								-
								<cwms:securedLink permission="CALIBRATION_NEW"  classAttr="mainlink"  parameter="?jobitemid=${form.ji.jobItemId}" collapse="True" >
									<spring:message code="jicalibration.newcalibration"/>
								</cwms:securedLink>
							</c:if>
						</td>
					</tr>
					<tr>
						<th class="calstart" scope="col"><spring:message code="jicalibration.startdatetime"/></th>
						<th class="caltype" scope="col"><spring:message code="jobresults.type"/></th>
						<th class="calprocess" scope="col"><spring:message code="viewjob.process"/></th>
						<th class="calstds" scope="col"><spring:message code="jicalibration.stds"/></th>
						<th class="calcerts" scope="col"><spring:message code="jicalibration.certs"/></th>
						<th class="calproc" scope="col"><spring:message code="jobresults.capability"/></th>
						<th class="calclass" scope="col"><spring:message code="jicalibration.class"/></th>
						<th class="calstatus" scope="col"><spring:message code="jobresults.stat"/></th>
						<th class="calchecksheet" scope="col"><spring:message code="jicalibration.checksheet"/></th>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${form.ji.calLinks.size() == 0}">
							<tr>
								<td colspan="9" class="center bold"><spring:message code="jicalibration.string1"/></td>
							</tr>
						</c:when>
						<c:otherwise>
							<c:forEach var="cl" items="${form.ji.calLinks}">
								<c:set var="c" value="${cl.cal}"/>
								<tr id="cal${c.id}">
									<td class="calstart">
										<fmt:formatDate value="${c.startTime}" type="both" dateStyle="SHORT" timeStyle="SHORT"/>
										<c:if test="${c.startedBy != null}">
											(${c.startedBy.shortenedName})
										</c:if>
										<c:if test="${c.calAddress != null}">
											<br/>
											<a class="mainlink" href="viewaddress.htm?subdivid=${form.ji.calAddr.sub.subdivid}&addrid=${form.ji.calAddr.addrid}">
												${c.calAddress.addr1}, ${c.calAddress.town}
											</a>
										</c:if>
									</td>
									<td class="caltype"><cwms:besttranslation translations="${c.calType.serviceType.shortnameTranslation}"/></td>
									<td class="calprocess">${c.calProcess.process}</td>
									<td class="calstds">
										<span class="calStandardsSize${c.id}">${c.standardsUsed.size()}</span>
										<!-- hover over this anchor and a tooltip is displayed showing the contents of the div created below -->
										<a href="?id=#calStandards${c.id}&amp;width=450" onclick="event.preventDefault();" id="tip${c.id}" class="jTip">
											<img src="img/icons/standard.png" width="16" height="16" class="image_inl" alt="" />
										</a>
										<!-- provide clipboard icon to copy tabbed list of standards -->
										<img class="marg-left" src="img/icons/bullet_clipboard.png" width="10" height="10" onclick="copyListOfStandardsToClipboard(this, ${c.id}); return false;" alt="<spring:message code='jicalibration.string2'/>" title="<spring:message code='jicalibration.string2'/>"/>
										<!-- contents of this div are displayed in tooltip created above -->
										<div id="calStandards${c.id}" class="hid">
											<table class="default2" summary="<spring:message code='jicalibration.string3'/>">
												<thead>
													<tr>
														<th colspan="2">
															<div class="float-left">
																<spring:message code="jicalibration.standused"/>
																(<span class="calStandardsSize${c.id}">${c.standardsUsed.size()}</span>)
															</div>
																<div class="float-right">
																	<cwms:securedJsLink classAttr="mainlink" onClick="event.preventDefault(); loadAddRemoveStandards(${c.id});" permission="ROLE_INTERNAL">
																		<spring:message code="jicalibration.addrem"/>
																	</cwms:securedJsLink>
																</div>
														</th>
													</tr>
												</thead>
												<tbody>
													<c:choose>
														<c:when test="${c.standardsUsed.size() == 0}">
															<tr>
																<td colspan="2" class="center bold">
																	<spring:message code="jicalibration.string4"/>
																</td>
															</tr>
														</c:when>
														<c:otherwise>
															<c:forEach var="s" items="${c.standardsUsed}">
																<tr>
																	<td>
																		<links:showInstrumentLink instrument="${s.inst}"/>
																	</td>
																	<td>
																		${s.inst.definitiveInstrument}<br />
																		<spring:message code="serialno"/>: ${s.inst.serialno}<br/>
																		<spring:message code="plantno"/>: ${s.inst.plantno}
																	</td>
																</tr>
															</c:forEach>
														</c:otherwise>
													</c:choose>
												</tbody>
											</table>
										</div>													
									</td>
									<td class="calcerts">
										${c.certs.size()}
										<!-- hover over this anchor and a tooltip is displayed showing the contents of the div created below -->
										<a href="?id=#certificate${c.id}&amp;width=450" onclick="event.preventDefault();" id="certificatetip${c.id}" class="jTip">
											<img src="img/icons/certificate.png" width="16" height="16" class="image_inl" alt="" />
										</a>
										<!-- contents of this div are displayed in tooltip created above -->
										<div id="certificate${c.id}" class="hid">
											<table class="default2" id="jicalcertificates" summary="<spring:message code='jicalibration.string6'/>">
												<thead>
													<tr>
														<th colspan="3"><spring:message code="certificates"/> (${c.certs.size()})</th>
													</tr>
													<tr>
														<th id="certno" scope="col"><spring:message code="jicalibration.certno"/></th>
														<th id="certstat" scope="col"><spring:message code="status"/></th>
														<th id="certdurtn" scope="col"><spring:message code="jicalibration.certduration"/></th>
													</tr>
												</thead>
												<tbody>
													<c:choose>
														<c:when test="${c.certs.size() == 0}">
															<tr>
																<td colspan="3" class="center bold"><spring:message code="jicalibration.string5"/><br/>
																	<c:if test="${c.status.name == 'Complete' || c.status.name == 'Unable to calibrate'}">
																		<a href="" class="mainlink" onclick="event.preventDefault(); findCalToIssueCert(${c.id}, ${form.ji.inst.plantid}, ${cl.id});">
																			<spring:message code="jicalibration.issuebelatedcert"/>
																		</a>
																	</c:if>
																</td>
															</tr>
														</c:when>
														<c:otherwise>
															<c:forEach var="cert" items="${c.certs}">
																<tr>
																	<td>
																		<c:choose>
																			<c:when test="${cert.certFiles.size() == 0}">
																				${cert.certno}
																			</c:when>
																			<c:otherwise>
																				<c:forEach var="f" items="${cert.certFiles}">
																					<c:url var="downloadURL" value="downloadfile.htm"><c:param name="file" value="${EncryptionTools.encrypt(f.absolutePath)}" /></c:url>
																					<a href="${downloadURL}" target="_blank" class="mainlink">${cert.certno}</a>
																				</c:forEach>
																			</c:otherwise>
																		</c:choose>
																	</td>
																	<td>${cert.status.message}</td>
																	<td>${cert.duration} ${cert.unit.getName(cert.duration)}</td>
																</tr>
															</c:forEach>
														</c:otherwise>
													</c:choose>
												</tbody>
											</table>
										</div>
									</td>
									<td class="calproc">${c.capability.reference} - ${c.capability.name}</td>
									<td class="calclass">${c.calClass.desc}</td>
									<td class="calstatus">
										<c:set var="link"  value="completecalibration.htm?calid=${c.id}&jobitemid=${form.ji.jobItemId}"/>
										<c:if test="${c.batch != null}">
											<c:set var="link" value="viewbatchcalibration.htm?batchid=${c.batch.id}"/>
										</c:if>
										<c:choose>
											<c:when test="${(c.status.name == 'On-going' || c.status.name == 'Awaiting work')}">
												<a href="${link}" class="mainlink"><cwms:besttranslation translations="${c.status.nametranslations}"/></a>
											</c:when>
											<c:when test="${c.status.name == 'On hold' && form.ji.state.isStatus()}">
												<a href="resumecalibration.htm?jobitemid=${form.ji.jobItemId}" class="mainlink" title="Click to resume this calibration">
													<cwms:besttranslation translations="${c.status.nametranslations}"/>
												</a>
											</c:when>
											<c:otherwise>
												<cwms:besttranslation translations="${c.status.nametranslations}"/>
											</c:otherwise>
										</c:choose>
									</td>
								</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="9">&nbsp;</td>
					</tr>
				</tfoot>
			</table>
		</div>
		<!-- end of infobox div -->
	</jsp:body>
</jobs:jobItemTemplate>