<%-- File name: /trescal/core/jobs/jobitem/fragments/linkedPurchaseOrders.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="displayPOsToLinkOverlay" class="center-pad">
	<c:choose>
		<c:when test="${linkedOrders.size() == 0}">
			<table class="default4" summary="This table displays message of no linked purchase orders">
				<thead>
					<tr>
						<td><spring:message code="jithirdparty.noPOAlreadyLinked"/></td>
					</tr>
				</thead>
			</table>
		</c:when>
		<c:otherwise>
			<!-- add warning box to display error messages if the link fails -->
			<div class="hid warningBox1">
				<div class="warningBox2">
					<div class="warningBox3"></div>
				</div>
			</div>
			<table class="default4 tbxPOs" summary="This table displays all linked purchase orders and most recent purchase orders">
				<thead>
					<tr>
						<td colspan="5"><spring:message code="jithirdparty.displayPOLinked" arguments="${linkedOrders.size()}"/></td>
					</tr>
					<tr>
						<th scope="col" class="tbxpono"><spring:message code="viewjob.ponumber"/></th>
						<th scope="col" class="tbxcomp"><spring:message code="company"/></th>
						<th scope="col" class="tbxdate"><spring:message code="jithirdparty.datecreated"/></th>
						<th scope="col" class="tbxcost cost2pc"><spring:message code="cost"/></th>
						<th scope="col" class="tbxitem"><spring:message code="items"/></th>
					</tr>
				</thead>
				<tbody>
					<!-- loop through linked purchase orders -->
					<c:forEach var="order" items="${linkedOrders}">
						<tr id="linkedPO${order.id}">
							<td>
								<a href="viewpurchaseorder.htm?id=${order.id}" class="mainlink">
									${order.poNo}
								</a>
							</td>
							<td>${order.companyName}</td>
							<td><fmt:formatDate value="${order.regDate}" type="date" dateStyle="SHORT"/></td>
							<td class="cost2pc"><fmt:formatNumber type="CURRENCY" value="${order.finalCost}" currencySymbol="${order.currencyERSymbol}"/></td>
							<td class="center">
								<a href="" class="mainlink" onclick="event.preventDefault(); displayLinkedPurchaseOrderItems($j(this).parent().parent(), ${order.id}, 'linkedPO${order.id}', ${jobItemId}); return false; ">
									<img src="img/icons/items.png" width="16" height="16" alt="<spring:message code='showItems'/>"
										title="<spring:message code='showItems'/>" class="image_inline"/>
								</a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:otherwise>
	</c:choose>
	<c:if test="${mostRecentOrders.size() > 0}">
		<table class="default4 tbxPOs" summary="This table displays all linked purchase orders and most recent purchase orders">
			<thead>
				<tr>
					<td colspan="5"><spring:message code="jithirdparty.displayMostRecentPO" arguments="${mostRecentOrders.size()}"/></td>
				</tr>
				<tr>
					<th scope="col" class="tbxpono"><spring:message code="viewjob.ponumber"/></th>
					<th scope="col" class="tbxcomp"><spring:message code="company"/></th>
					<th scope="col" class="tbxdate"><spring:message code="jithirdparty.datecreated"/></th>
					<th scope="col" class="tbxcost cost2pc"><spring:message code="cost"/></th>
					<th scope="col" class="tbxitem"><spring:message code="items"/></th>
				</tr>
			</thead>
			<tbody>
				<!-- loop through most recent orders raised -->
				<c:forEach var="order" items="${mostRecentOrders}">
					<tr id="search${order.id}">
						<td>
							<a href="viewpurchaseorder.htm?id=${order.id}" class="mainlink">
								${order.poNo}
							</a>
						</td>
						<td>${order.companyName}</td>
						<td><fmt:formatDate value="${order.regDate}" type="date" dateStyle="SHORT"/></td>
						<td class="cost2pc"><fmt:formatNumber type="CURRENCY" value="${order.finalCost}" currencySymbol="${order.currencyERSymbol}"/></td>
						<td class="center">
							<a href="" class="mainlink" onclick="event.preventDefault(); displayLinkedPurchaseOrderItems($j(this).parent().parent(), ${order.id}, 'search${order.id}', ${jobItemId}); return false; ">
								<img src="img/icons/items.png" width="16" height="16" alt="<spring:message code='showItems'/>"
									title="<spring:message code='showItems'/>" class="image_inline"/>
							</a>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</c:if>
</div>