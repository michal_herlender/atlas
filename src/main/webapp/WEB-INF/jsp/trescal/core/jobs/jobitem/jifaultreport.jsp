<%-- File name: /trescal/core/jobs/jobitem/jifaultreport.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="instrument" tagdir="/WEB-INF/tags/instrument"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<jobs:jobItemTemplate>
	<jsp:attribute name="subScript">
        <script type='text/javascript'
			src='script/trescal/core/jobs/jobitem/JIFaultReport.js'></script>
        <script type='text/javascript'
			src='script/trescal/core/documents/ImageGalleryFunctions.js'></script>
        <script>
            //grab scroll to parameter passed when using job item key navigation
            var scrollto = '${request.getParameter("scrollto")}';

            var intervalUnits = [];
            <c:forEach var="unit" items="${units}">
            	intervalUnits.push({id: '${unit.key}', name: '${unit.value}'})
            </c:forEach>
            
        </script>
    </jsp:attribute>
	<jsp:body>
        <div class="infobox">
	        
	        <a href="editfailurereport.htm?jobitemid=${form.ji.jobItemId}" target="blank" class="mainlink-float" >
	        	<spring:message code="failurereport.newfailurereport" />
        	</a>
	        <div class="clear"></div>
	        
        	<table class="default2" >
				<thead>
					<tr>
						<th class="status" scope="col"><spring:message code="jifaultreport.faultreport" /></th>
						<th class="status" scope="col"><spring:message code="jicertificates.issdate" /></th>
						<th class="status" scope="col"><spring:message code="certmanagement.issby" /></th>
						<th class="status" scope="col"><spring:message code="failurereport.validatedon" /></th>
						<th class="status" scope="col"><spring:message code="failurereport.validatedby" /></th>
						<th class="status" scope="col"><spring:message code="failurereport.clientresponseneeded" /></th>
						<th class="status" scope="col"><spring:message code="failurereport.clientresponsereceivedon" /></th>
						<th class="status" scope="col"><spring:message code="failurereport.outcome" /></th>
					</tr>
				</thead>
				<tbody>
					<c:if test="${ form.failurereports.size() eq 0 }" >
						<tr>
							<td colspan="9" class="center bold"><spring:message code="failurereport.emptylist"/></td>
						</tr>
					</c:if>
					<c:forEach var="fr" items="${ form.failurereports }" >
						<tr>
							<td>
								<a href="viewfailurereport.htm?failurereportid=${ fr.id }" class="mainlink" >${ fr.number }</a>
							</td>
							<td><fmt:formatDate type="both" dateStyle="SHORT" timeStyle="SHORT" value="${ fr.issueDate }"/></td>
							<td>${ fr.issuedBy }</td>
							<td><fmt:formatDate type="both" dateStyle="SHORT" timeStyle="SHORT" value="${ fr.validatedOn }"/></td>
							<td>${ fr.validatedBy }</td>
							<td>
								<c:choose>
									<%-- When value not selected, it should appear as blank --%>
									<c:when test="${ fr.clientReponseNeeded eq true}"><spring:message code="yes" /></c:when>
									<c:when test="${ fr.clientReponseNeeded eq false}"><spring:message code="no" /></c:when>
								</c:choose>
							</td>
							<td>
								<c:choose>
									<c:when test="${ fr.clientReponseNeeded eq false or  fr.clientReponseNeeded eq 'NOT_NECESSARY' }">
										<spring:message code="failurereport.notnecessary" />
									</c:when>
									<c:otherwise>
										<fmt:formatDate type="both" dateStyle="SHORT" timeStyle="SHORT" value="${ fr.clientApprovedon }"/>
									</c:otherwise>
								</c:choose>
							</td>
							<td>
								<c:choose>
									<c:when test="${ empty fr.finalOutcome }">
										<spring:message code="tools.none" />
									</c:when>
									<c:otherwise>
										${ fr.finalOutcome.translation }
									</c:otherwise>
								</c:choose>
							</td>
						<tr>
					</c:forEach>
				</tbody>
			</table>

        </div>
    </jsp:body>
</jobs:jobItemTemplate>