<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>


<form:form action="addjobfp.htm" id="additemtojob" method="post" >
	
	<input type='hidden' id='jobid' name='jobid' value='${job.jobid}'/>
	<input type='hidden' id='addrid' name='addrid' value='${job.bookedInAddr.addrid}'/>
	<input type='hidden' id='personid' name='personid' value='${job.con.personid}'/>
	<input type='hidden' id='coid' name='coid' value='${job.con.sub.comp.coid}'/>
	<input type='hidden' id='receiptDate' name='receiptDate'   
	value="${cwms:isoDatetime(job.receiptDate)}" />

	<fieldset>  
    	<li>
        	<td><spring:message code="addjob.recordtype.prebooking.jobsavailable" />(${listasn.size()}):</td>
    	</li>
 
		<c:if test="${ listasn.size() > 0}"> 
 		<table class="default2">
			<thead>
				<tr>
		    		<th class="codefault"></th>
					<th class="codefault">N°</th>
					<th class="codefault"><spring:message code="contact" /></th>
					<th class="codefault"><spring:message code="manageprebooking.table.clientref" /></th>
					<th class="codefault"><spring:message code="viewjob.createdon" /></th>
					<th class="codefault"><spring:message code="newcalibration.items" /></th>
					<th class="codefault"><spring:message code="instmod.lastmodified" /></th>
				</tr>
			</thead>
			<tbody>
	  			<c:forEach var="asn" items="${listasn}">
	  				<tr>
	   					<td>
	     					<input type="radio" name="asnId" value="${asn.id}" />
	   					</td> 
	   					<td>${asn.id}</td>
	   					<td>${asn.createdByFirstName} &nbsp;${asn.createdByLastName}</td>
	   					<td>${asn.clientRef} &nbsp;</td>
	   					<td>${asn.createdOn}</td>
	   					<td>${asn.itemsSize}</td>
	   					<td>${asn.lastModifiedByFirstName} &nbsp; ${asn.lastModifiedByLastName}</td>
	  				</tr>
	  			</c:forEach>
			</tbody>
		 </table>
		</c:if>  
	</fieldset>
</form:form>
