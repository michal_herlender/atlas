<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<script src='script/trescal/core/jobs/repair/RepairTime.js'></script>
<script type="module" src="${pageContext.request.contextPath}/script/components/cwms-datetime/cwms-datetime.js"></script>
<script>
	var actionPerformed = ${actionPerformed};
</script>

<t:overlayTemplate bodywidth="780">
	<!-- error section displayed when form submission fails -->
	<form:errors path="form.*">
		<div class="warningBox1">
			<div class="warningBox2">
				<div class="warningBox3">
					<h5 class="center-0 attention"><spring:message code="error.withinput"/>:</h5>
					<c:forEach var="e" items="${messages}">
						<div class="center attention"><c:out value="${e}"/></div>
					</c:forEach>
				</div>
			</div>
		</div>
		<div class="successBox1 hid" id="successMessage">
		<div class="successBox2">
			<div class="successBox3">
				<div class="center green bold">
					<spring:message code='repairinspection.success1' />
				</div>
			</div>
		</div>
	</form:errors>
	<!-- end error section -->
	<div id="manageRepairTime">
		<form:form method="POST" modelAttribute="form" id="repairTimeForm">

			<form:hidden path="action" />
			<form:hidden path="itemToDeleteId" />
			<form:hidden path="jobItemId" />
			<form:hidden path="technicianId" />
			<form:hidden path="repairCompletionReportId" />
			<input type="hidden" name="saveRepairTime" value="true" />
			<fieldset>
				<ol>
					<li>
						<label><spring:message code="workflow.technician" />:</label>
						<span> ${form.technicianName} </span>
						<div class="clear-0"></div>
					</li>
					<li>
						<label><spring:message code="repaircompletionreport.headtext" />:</label> 
						<span>${form.repairCompletionReportIdentifier} </span>
						<div class="clear-0"></div>
					</li>
				</ol>
			</fieldset>
			<fieldset>
				<legend>
					<span id="accHeader">
						<spring:message code="repairtime.history.technician" />
					</span>
				</legend>
				<ol>				
					<li>
						<table class="default2">
							<tr>
								<th><spring:message code="repairinspection.riroperation.name" /></th>
								<th><spring:message code="repairinspection.riroperation.time" /></th>
								<th><spring:message code="jigeneralserviceoperation.startedon" /></th>
								<th><spring:message code="timeactivity.timespent" /></th>
								<th><spring:message code="comment" /></th>
								<th><spring:message code="manageprebooking.table.action" /></th>
							</tr>

							<c:if
								test="${empty form.repairTimes or form.repairTimes.size() == 0}">
								<tr>
									<td colspan="6" class="center"><spring:message
											code="repairtime.emptylist" /></td>
								</tr>
							</c:if>

							<c:forEach items="${form.repairTimes}" var="rt"
								varStatus="status">
								<c:if test="${rt.technicianId eq form.technicianId or empty rt.technicianId}">
								<tr>
									<td>${rt.operationName}</td>
									<td>${rt.operationRirTimespentHour} &nbsp; <spring:message
											code="hours" /> &nbsp;
										${rt.operationRirTimespentMinute} &nbsp; <spring:message
											code="minutes" />
									</td>
									<td><form:hidden
											path="repairTimes[${status.index}].jobItemActionId" />  
											<span id="startDate-${rt.jobItemActionId}"> <fmt:formatDate
												value="${rt.startDate}" type="both" dateStyle="SHORT" timeStyle="SHORT" />
									</span> 
									
									<form:input type="datetime-local" path="repairTimes[${status.index}].startDate"
						id="startDate_${ rt.jobItemActionId }" class="hid"
										step="60" max="${cwms:isoDatetime(form.currentDate)}"
										value="${rt.startDate.toString()}"
										/>
										<form:errors class="error max-wrap"
											path="repairTimes[${status.index}].startDate" /></td>
									<td><span id="timeSpentH_${ rt.jobItemActionId }">${rt.timespentHour}</span>
										<form:input class="hid"
											path="repairTimes[${status.index}].timespentHour"
											style="width: 20px;text-align:right;"
											id="timeSpentHour_${ rt.jobItemActionId }" /> <spring:message
											code="hours" /> &nbsp; <span
										id="timeSpentM_${ rt.jobItemActionId }">${rt.timespentMinute}</span>
										<form:input class="hid"
											path="repairTimes[${status.index}].timespentMinute"
											style="width: 20px;text-align:right;"
											id="timeSpentMinute_${ rt.jobItemActionId }" /> <spring:message
											code="minutes" /> <form:errors class="error max-wrap"
											path="repairTimes[${status.index}].timespentHour" /></td>
									<td><span id="technicianComment-${rt.jobItemActionId}">${rt.technicianComment}</span>
										<form:textarea class="hid"
											path="repairTimes[${status.index}].technicianComment"
											id="technicianComment_${ rt.jobItemActionId }" rows="2"
											cols="25" /></td>
									<td><a href="#"
										onclick="preparEditRepairTime(this,${ rt.jobItemActionId }); "
										title=""> <img src="img/icons/edit_details.png" width="16"
											height="16" alt="<spring:message code='edit'/>"
											title="<spring:message code='edit'/>" />
									</a> <a href="#"
										onclick="deleteRepairTime(${ rt.jobItemActionId }); "
										class="padleft10"> <img src="img/icons/delete.png"
											width="16" height="16" alt="<spring:message code='delete'/>"
											title="<spring:message code='delete'/>" class="img_marg_bot">
									</a></td>
								</tr>
								</c:if>
							</c:forEach>

						</table>
					</li>

					<li id="validationController" class="hid"><label>&nbsp;</label>
						<input type="submit" value="<spring:message code='save' />"
						onclick="editRepairTime();" /> &nbsp; <input type="button"
						value="<spring:message code='cancel' />"
						onclick=" cancelRepairTime(); " /></li>
				</ol>
			</fieldset>
			<fieldset>
				<legend>
					<span id="accHeader">
						<spring:message code="repairtime.history.othertechnicians" />
					</span>
				</legend>
				<ol>
					<li>
						<table class="default2">
							<tr>
								<th><spring:message code="repairinspection.riroperation.name" /></th>
								<th><spring:message code="repairinspection.riroperation.time" /></th>
								<th><spring:message code="workflow.technician" /></th>
								<th><spring:message code="jigeneralserviceoperation.startedon" /></th>
								<th><spring:message code="timeactivity.timespent" /></th>
								<th><spring:message code="comment" /></th>
								
							</tr>

							<c:if
								test="${empty form.repairTimes or form.repairTimes.size() == 0}">
								<tr>
									<td colspan="6" class="center"><spring:message
											code="repairtime.emptylist" /></td>
								</tr>
							</c:if>
							<c:forEach items="${form.repairTimes}" var="rt"
								varStatus="status">
								<c:if test="${rt.technicianId ne form.technicianId and not empty rt.technicianId}">
									<tr>
										<td>${rt.operationName}</td>
										<td>${rt.operationRirTimespentHour} &nbsp; <spring:message
												code="hours" /> &nbsp;
											${rt.operationRirTimespentMinute} &nbsp; <spring:message
												code="minutes" />
										</td>
										<td>
											${ rt.technicianName }
										</td>
										<td> 
											<fmt:formatDate
													value="${rt.startDate}" type="both" dateStyle="SHORT" timeStyle="SHORT" /> 
										</td>
										<td>
											${rt.timespentHour}&nbsp;
											<spring:message code="hours" /> &nbsp; 
											${rt.timespentMinute}&nbsp;
											<spring:message code="minutes" /> 
										</td>
										<td>
											${rt.technicianComment}
										</td>
										
									</tr>
									<c:set var="tableNotEmpty" value="true"></c:set>
								</c:if>
							</c:forEach>
							<c:if test="${ empty tableNotEmpty }">
								<tr>
									<td colspan="6" class="center"><spring:message
											code="repairtime.emptylist" /></td>
								</tr>
							</c:if>
						</table>
					</li>
				</ol>
			</fieldset>	
		</form:form>
	</div>
</t:overlayTemplate>