<%-- File name: /trescal/core/jobs/job/jobresults.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="web" tagdir="/WEB-INF/tags/web" %>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="instrument" tagdir="/WEB-INF/tags/instrument" %>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel" %>

<t:crocodileTemplate>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/jobs/job/JobResults.js'></script>
		<script type="text/javascript">
		// does the user want to highlight search criteria?
		var highlight = ${jobsearchform.searchCriteria.highlight};
		// does the user want to highlight search criteria?
		if (highlight)
		{
			var searchCrit = new Array (	{ name: 'jsrserial', value: '${jobsearchform.searchCriteria.serialno}', classname: 'highlightSerial' },
											{ name: 'jsrplant', value: '${jobsearchform.searchCriteria.plantno}', classname: 'highlightPlant' }, 
											{ name: 'mfrResult', value: '${jobsearchform.searchCriteria.mfr}', classname: 'highlightMfr' },
											{ name: 'modelResult', value: '${jobsearchform.searchCriteria.model}', classname: 'highlightModel' },
											{ name: 'descResult', value: '${jobsearchform.searchCriteria.desc}', classname: 'highlightDesc' },
											{ name: 'jsrclientref', value: '${jobsearchform.searchCriteria.clientref}', classname: 'highlightClientref' },
											{ name: 'jsrpurchaseord', value: '${jobsearchform.searchCriteria.purchaseorder}', classname: 'highlightPurchaseorder' },
											{ name: 'jsrproc', value: '${jobsearchform.searchCriteria.procno}', classname: 'highlightProc' });
		
		}
		</script>
	</jsp:attribute>

	<jsp:body>
		<!-- main box body content div -->
		<div class="infobox" id="jobsearchresults">				
			<form:form modelAttribute="jobsearchform" action="" name="searchform" id="searchform" method="post">
				<form:hidden path="coid" />
				<form:hidden path="subdivid" />
				<form:hidden path="jobType" />
				<form:hidden path="personid" />
				<form:hidden path="serialno" />
				<form:hidden path="plantno" />
				<form:hidden path="jobno" />
				<form:hidden path="plantid" />
				<form:hidden path="clientRef" />
				<form:hidden path="purOrder" />
				<form:hidden path="mfr" />
				<form:hidden path="model" />
				<form:hidden path="desc" />
				<form:hidden path="modelid" />
				<form:hidden path="mfrid" />
				<form:hidden path="descid" />
				<form:hidden path="procId" />
				<form:hidden path="bookedInDateBetween" />
				<form:hidden path="completedDateBetween" />
				<form:hidden path="highlight" />
				
				<form:hidden path="bookedInDate1" />
				<form:hidden path="bookedInDate2" />
				<form:hidden path="completedDate1" />
				<form:hidden path="completedDate2" />
				
				<form:hidden path="active" />
				<form:hidden path="pageNo" />
				<form:hidden path="resultsPerPage" />
				<form:hidden path="resultsTotal" />
				<form:hidden path="jobItemCount" />
				
				<t:showResultsPagination rs="${jobsearchform.rs}" pageNoId=""/>
				
				<table class="default4" summary="${jobresults.def4}">
					<thead>
						<tr>
							<td colspan="7">
								<spring:message code="jobresults.jobres"/>
								(${jobsearchform.jobs.size()} jobs with ${jobsearchform.jobItemCount} items)
								<spring:message code="web.export.excel" var="submitText"/>  
								<input type="submit" name="export" value="${submitText}" class="float-right">
							</td>
						</tr>
						<tr>
							<td><spring:message code="jobresults.searchcrit"/>:</td>
							<td colspan="6">
								<c:if test="${not empty jobsearchform.searchCriteria.jobno}">
									<spring:message code="jobno"/>: ${jobsearchform.searchCriteria.jobno} &nbsp;&nbsp;&nbsp;
								</c:if>
								<c:if test="${not empty jobsearchform.searchCriteria.company}">
									<spring:message code="company"/>: ${jobsearchform.searchCriteria.company} &nbsp;&nbsp;&nbsp;
								</c:if>
								<c:if test="${not empty jobsearchform.searchCriteria.subdiv}">
									<spring:message code="subdiv"/>: ${jobsearchform.searchCriteria.subdiv} &nbsp;&nbsp;&nbsp;
								</c:if>
								<c:if test="${not empty jobsearchform.searchCriteria.contact}">
									<spring:message code="contact"/>: ${jobsearchform.searchCriteria.contact} &nbsp;&nbsp;&nbsp;
								</c:if>
								<c:if test="${not empty jobsearchform.searchCriteria.plantid}">
									<spring:message code="barcode"/>: ${jobsearchform.searchCriteria.plantid} &nbsp;&nbsp;&nbsp;
								</c:if>
								<c:if test="${not empty jobsearchform.searchCriteria.serialno}">
									<spring:message code="serialno"/>: ${jobsearchform.searchCriteria.serialno} &nbsp;&nbsp;&nbsp;
								</c:if>
								<c:if test="${not empty jobsearchform.searchCriteria.plantno}">
									<spring:message code="plantno"/>: ${jobsearchform.searchCriteria.plantno} &nbsp;&nbsp;&nbsp;
								</c:if>
								<c:if test="${not empty jobsearchform.searchCriteria.mfr}">
									<spring:message code="mfr"/>: ${jobsearchform.searchCriteria.mfr} &nbsp;&nbsp;&nbsp;
								</c:if>
								<c:if test="${not empty jobsearchform.searchCriteria.model}">
									<spring:message code="model"/>: ${jobsearchform.searchCriteria.model} &nbsp;&nbsp;&nbsp;
								</c:if>
								<c:if test="${not empty jobsearchform.searchCriteria.desc}">
									<spring:message code="jobresults.desc"/>: ${jobsearchform.searchCriteria.desc} &nbsp;&nbsp;&nbsp;
								</c:if>
								<c:if test="${not empty jobsearchform.searchCriteria.purchaseorder}">
									<spring:message code="jobresults.purchord"/>: ${jobsearchform.searchCriteria.purchaseorder} &nbsp;&nbsp;&nbsp;
								</c:if>
								<c:if test="${not empty jobsearchform.searchCriteria.clientref}">
									<spring:message code="addjob.clientref"/>: ${jobsearchform.searchCriteria.clientref} &nbsp;&nbsp;&nbsp;
								</c:if>
								<c:if test="${not empty jobsearchform.searchCriteria.procno}">
									<spring:message code="jobresults.capability"/>: ${jobsearchform.searchCriteria.procno} &nbsp;&nbsp;&nbsp;
								</c:if>
								<c:if test="${not empty jobsearchform.searchCriteria.jobType}">
									<spring:message code="jobresults.type"/>: ${jobsearchform.searchCriteria.jobType} &nbsp;&nbsp;&nbsp;
								</c:if>
								<c:if test="${not empty jobsearchform.searchCriteria.active}">
									<spring:message code="jobresults.actonly"/>: ${jobsearchform.searchCriteria.active} &nbsp;&nbsp;&nbsp;
								</c:if>
							</td>
						</tr>
						<tr>
							<th class="jsrjobno" scope="col"><spring:message code="jobno" /></th>  
							<th class="jsrconame" scope="col"><spring:message code="company" /></th>  
							<th class="jsrsubdiv" scope="col"><spring:message code="subdiv" /></th>  
							<th class="jsrcontact" scope="col"><spring:message code="contact" /></th>  
							<th	class="jsrpurchaseord" scope="col"><spring:message code="jobresults.purchord" /></th>
							<th class="jsrclientref" scope="col"><spring:message code="addjob.clientref" /></th>
							<th class="jsrdatein" scope="col"><spring:message code="jobresults.datein" /></th>
						</tr>									
					</thead>
				</table>
				
				<c:choose>
					<c:when test="${jobsearchform.jobs.size() < 1}">
						<table class="default" summary="This table displays a message if no jobs are returned">
							<tbody>
								<tr>
									<td colspan="7" class="bold center">
										<spring:message code="jobresults.string1" />
									</td>
								</tr>
							</tbody>
						</table>
					</c:when>
					<c:otherwise>
						<c:forEach var="jb" items="${jobsearchform.jobs}">	
							<table class="default4" summary="<spring:message code="jobresults.string3" />">											
								<tbody>								
									<tr onclick=" window.location.href='viewjob.htm?jobid=${jb.jobid}' " onmouseover=" $j(this).addClass('hoverrow'); " onmouseout=" $j(this).removeClass('hoverrow'); ">
										<td class="jsrjobno"><instrument:jobnoLinkWeb job="${jb}" /></td>
										<td class="jsrconame"><links:companyLinkDWRInfo company="${jb.con.sub.comp}" copy="false" rowcount=""/></td>
										<td class="jsrsubdiv"><links:subdivLinkDWRInfo subdiv="${jb.con.sub}" rowcount=""/></td>
										<td class="jsrcontact"><links:contactLinkDWRInfo contact="${jb.con}" rowcount=""/></td>
										<td	class="jsrpurchaseord"><jobs:showJobPos jb="${jb}" /></td>
										<td class="jsrclientref">${jb.clientRef}</td>
										<td class="jsrdatein"><fmt:formatDate value="${jb.regDate}" type="date" dateStyle="medium"/></td>
									</tr>
									<c:if test="${jb.items.size() > 0}">
										<tr>
											<td colspan="7" style=" padding: 0; ">
												<table class="child-table" border="" summary="<spring:message code="jobresults.string4"/>">
													<thead>
														<tr>
															<th class="jsritem" scope="col"><spring:message code="accessoriesanddefects.item"/></th>
															<th class="jsrinstr" scope="col"><spring:message code="accessoriesanddefects.instrument"/></th>
															<th class="jsrserial" scope="col"><spring:message code="serialno"/></th>
															<th class="jsrplant" scope="col"><spring:message code="plantno"/></th>
															<th class="jsrcaltype" scope="col"><spring:message code="jobresults.caltype"/></th>
															<th class="jsrproc" scope="col"><spring:message code="jobresults.capability"/></th>
															<th class="jsritempo" scope="col"><spring:message code="jobresults.itempo"/></th>
															<th class="jsrstatus" scope="col"><spring:message code="jobresults.stat"/></th>
														</tr>
													</thead>
													<tbody>
														<c:forEach var="i" items="${jb.items}">
															<tr style="background-color: <c:choose><c:when test="${i.turn} <= ${fastTrackTurn}"> ${i.calType.serviceType.displayColourFastTrack}; </c:when><c:otherwise> ${i.calType.serviceType.displayColour};</c:otherwise></c:choose>">
																<td class="center"><links:jobitemLinkDWRInfo jobitem="${i}" jicount="false" rowcount=""/></td>
																<td><instmodel:showInstrumentModelLink caltypeid="" instrument="${i.inst}" /></td>
																<td class="jsrserial">${i.inst.serialno}</td>
																<td class="jsrplant">${i.inst.plantno}</td>
																<td class="center"><t:showTranslationOrDefault translations="${i.calType.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}" /></td>
																<td class="jsrproc center">${i.capability.reference}</td>
																<td>
																	<c:forEach var="jipo" items="${i.jobItemPOs}">
																		<c:choose>
																			<c:when test="${not empty jipo.po}">
																				${jipo.po.poNumber}<br />
																			</c:when>
																			<c:when test="${not empty jipo.bpo}">
																				${jipo.bpo.poNumber}<br />
																			</c:when>
																			<c:otherwise>
																				<spring:message code="jobresults.noitempo" />
																			</c:otherwise>
																		</c:choose>
																	</c:forEach>	
																</td>
																<td class="status">																																		
																	<t:showTranslationOrDefault translations="${i.state.translations}" defaultLocale="${defaultlocale}" />																		
																</td>																			
															</tr>
														</c:forEach>	
													</tbody>
												</table>
											</td>
										</tr>
									</c:if>
								</tbody>											
							</table>
						</c:forEach>
					</c:otherwise>
				</c:choose>
				<table class="default tlast" summary="">
					<tfoot>
						<tr>
							<td>
								<input type="submit" name="export" value="${submitText}" class="float-right">
							</td>
							
						</tr>
					</tfoot>
				</table>
				<t:showResultsPagination rs="${jobsearchform.rs}" pageNoId=""/>
				
			</form:form>									
	 	</div>
	</jsp:body>
</t:crocodileTemplate>