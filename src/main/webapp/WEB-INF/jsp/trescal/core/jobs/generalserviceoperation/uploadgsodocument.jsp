<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs"%>
<%@ taglib prefix="addr" tagdir="/WEB-INF/tags/address"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image"%>
<jsp:useBean id="now" class="java.util.Date" />

<jobs:jobItemTemplate>
	<jsp:attribute name="subScript">
		<script type="module" src="${pageContext.request.contextPath}/script/components/cwms-datetime/cwms-datetime.js"></script>
		<script
			src='script/trescal/core/jobs/generalserviceoperation/GeneralServiceOperation.js'></script>
		<script>
			//grab scroll to parameter passed when using job item key navigation
			var scrollto = '${request.getParameter("scrollto")}';
			var startedOn = '${form.startedOn}';
			var completedOn = '${form.completedOn}';
		</script>
	</jsp:attribute>
	<jsp:attribute name="warnings">
		<!-- error section displayed when form submission fails -->
		<spring:bind path="form.*">
			<c:if test="${status.error}">
				<c:set var="formHasError" value="${true}" />
				<div class="warningBox1">
					<div class="warningBox2">
						<div class="warningBox3">
							<h5 class="center-0 attention">
								<spring:message code="generalserviceoperation.errors" />:</h5>
							<c:forEach var="error" items="${status.errorMessages}">
								<div class="center attention">${error}</div>
							</c:forEach>
						</div>
					</div>
				</div>
			</c:if>
		</spring:bind>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox">
			    <span id="jobItemId" class="hid">${form.ji.jobItemId}</span>
					<form:form modelAttribute="form"
				id="completegeneralserviceoperationform" method="post" action=""
				enctype="multipart/form-data">
						<fieldset id="completegeneralserviceoperation">
							<legend>
								<c:choose>
									<c:when test="${ form.actionPerformed eq 'EDIT_GSODOCUMENT' }">
										<spring:message code="generalserviceoperation.editgsodocument" />
									</c:when>
									<c:otherwise>
										<spring:message code="generalserviceoperation.uploadgsodocument" />
									</c:otherwise>
								</c:choose>
							</legend>
							<form:hidden path="gso.id" />
							<form:hidden path="actionPerformed" />
							<form:hidden path="documentId" />
							<ol>
								<li>
									<label><spring:message code="newcalibration.items" />:</label>
									<div class="float-left">										
												Item <links:jobitemLinkInfo jobItemNo="${form.ji.itemNo}"
									jobItemId="${form.ji.jobItemId}" defaultPage="" /> on Job ${form.ji.job.jobno}<br />
									</div>
								</li>
								<li>
									<label for="startedOn"><spring:message
									code="generalserviceoperation.startedon" />:</label>
									<fmt:formatDate value="${form.startedOn}" type="both" dateStyle="SHORT" timeStyle="SHORT" />
									&nbsp;(${form.startedByName})
								</li>
								<li>
									<label for="serviceType"><spring:message
									code="servicetype" />:</label>
									<cwms:besttranslation
								translations='${form.gso.calType.serviceType.shortnameTranslation}' /> - 
									<cwms:besttranslation
								translations='${form.gso.calType.serviceType.longnameTranslation}' />
								</li>
								
								<li>
									<label for="completedOn"><spring:message
									code="generalserviceoperation.completedon" />:</label>
									<fmt:formatDate value="${form.completedOn}" type="both" dateStyle="SHORT" timeStyle="SHORT" />
									&nbsp;(${form.completedByName})
								</li>
								
								<li>
									<label for="status"><spring:message code="state" />:</label>
									<cwms:besttranslation
								translations='${form.gso.status.nametranslations}' />
								</li>
								
								
								<li>
									<form:label path="documentDate">
								<spring:message code="generalserviceoperation.documentdate" />:</form:label>
									<form:input type="datetime-local" path="documentDate"
									step="60" max="${cwms:isoDatetime(now)}"/>
									<span class="attention"><form:errors path="documentDate" /></span>
								</li>
								<c:if test="${ form.actionPerformed eq 'EDIT_GSODOCUMENT' }">
								<li>
									<form:label path="file">
								<spring:message code="jicertificates.uplfile" />:</form:label>
								<form:input path="file" type="file" id="uploadfile"
									accept="application/pdf" /> 
									<span class="attention"><form:errors path="file" /></span>
									&nbsp;&nbsp;
									<a
								href="downloadfile.htm?file=${form.gsoDocument.encryptedGsoDocumentFilePath}"
								target="_blank"
								title="(<spring:message code="showfilesforsc.lastmodified"/>: ${form.gsoDocument.lastModified})">&nbsp;
										<img src="img/icons/certificate.png" width="20" height="16"
									class="img_inl_margleft" alt="" title="" />
										<span style="color: black">${form.gsoDocument.gsoDocumentFileName}</span>
									</a>
								</li>
								</c:if>
								<c:if test="${ form.actionPerformed ne 'EDIT_GSODOCUMENT' }">
								<li>
									<form:label path="file">
								<spring:message code="jicertificates.uplfile" />:</form:label>
									<form:input path="file" type="file" id="uploadfile"
									accept="application/pdf" /> 
									<span class="attention"><form:errors path="file" /></span>
								</li>	
								<c:if test="${ otherjobitems.size() > 0 }">
									<li>
										<form:label path="otherJobItemsId">
											<spring:message code="jicertificates.othitems" />:</form:label>
										<table class="default2">
											<thead>
												<tr>
													<th><input type="checkbox"
													onclick="checkAllItems($j(this).prop('checked'));" /></th>
													<th><spring:message code="viewjob.itemno" /></th>
													<th><spring:message code="description" /></th>
													<th><spring:message
														code="completecalibration.serialno" /></th>
													<th><spring:message code="completecalibration.plantno" /></th>
												</tr>
											</thead>			
											<tbody>
											<c:set var="j" value="0"></c:set>
												<c:forEach items="${ otherjobitems }" var="ji">
												
												<tr>
												<c:if test="${ form.ji.jobItemId ne ji.jobItemId }">
													<td>
														<input type="checkbox" name="otherJobItemsId[${ j }]"
															value="${ ji.jobItemId }" />
																						<c:set var="j" value="${ j+1 }"></c:set>
													</td>
													</c:if>
													<c:if test="${ form.ji.jobItemId eq ji.jobItemId }">
													<td></td>
													</c:if>
													<td><links:jobitemLinkInfo jobItemNo="${ji.itemNo}"
															jobItemId="${ji.jobItemId}" defaultPage="" /></td>
													<td><c:out value="${ji.inst.customerDescription}" /></td>
													<td><c:out value="${ji.inst.serialno}" /></td>
													<td><c:out value="${ji.inst.plantno}" /></td>
												</tr>
												</c:forEach>
											</tbody>
										</table>
									</li>
								</c:if>
								</c:if>
							</ol>
						</fieldset>
						<!-- submit button div -->	
						<div class="center">
							<input type="submit" value="<spring:message code='save'/>" />
<%-- 							<input type="button" id="submit" name="submit" value="<spring:message code='newcalibration.continue'/>" onclick=" $j(this).attr('disabled', 'disabled'); submitForm(); "/> --%>
						</div>
						<!-- end of submit button div -->
					</form:form>
		</div>
		<!-- end of infobox div -->
	</jsp:body>
</jobs:jobItemTemplate>