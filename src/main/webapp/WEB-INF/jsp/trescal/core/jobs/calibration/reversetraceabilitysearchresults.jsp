<%-- File name: /trescal/core/jobs/calibration/reversetraceabilitysearchresults.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="reversetraceabilitysearchresult.headtext"/></span>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox">
			<form:form method="post" action="" modelAttribute="form">
				<form:hidden path="plantId" />
				<form:hidden path="startCalDate" />
				<form:hidden path="endCalDate" />
				<form:hidden path="pageNo" />
				<form:hidden path="resultsPerPage" />
				<form:hidden path="resultsTotal" />
				<t:showResultsPagination rs="${form.rs}" pageNoId="" />
				<table class="default2" summary="This table lists all calibrations performed">
					<thead>
						<tr>
							<td colspan="10"><spring:message code="tpcosts.searchresults"/> (${form.rs.results.size()})
								<spring:message code="web.export.excel" var="submitText"/>  
								<input type="submit" name="export" value="${submitText}" class="float-right">
							</td>
						</tr>
						<tr>
							<td colspan="10">
								<spring:message code="searchmodres.usingcriteria"/>:&nbsp;&nbsp;
								<span class="bold"> <spring:message code="barcode"/>: </span> '${form.plantId}' &nbsp;
								<span class="bold"> <spring:message code="jicalibration.startdatetime"/>: </span> '${form.startCalDate}'&nbsp;
								<span class="bold"> <spring:message code="bpo.enddate"/>: </span> '${form.endCalDate}' </span>
							</td>
						</tr>
						<tr>
							<th scope="col"><spring:message code="newbatchcalibration.caldate"/></th>
							<th	scope="col"><spring:message code="searchprocedureform.procedurereference"/></th>
							<th	scope="col"><spring:message code="certnumber"/></th>    
							<th	scope="col"><spring:message code="jobno"/></th>
							<th	scope="col" style="text-align: center;"><spring:message code="invoiceexport.jobitemno"/></th>
							<th scope="col"><spring:message code="company"/></th>
							<th scope="col"><spring:message code="subdiv"/></th>
							<th scope="col"><spring:message code="barcode"/></th>
							<th scope="col"><spring:message code="serialno"/></th>
							<th scope="col"><spring:message code="plantno"/></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="10">&nbsp;</td>
						</tr>
					</tfoot>
					<body>
						<c:forEach var="dto" items="${form.rs.results}" varStatus="loop">
							<tr>
								<td><fmt:formatDate value="${dto.calDate}" type="date" dateStyle="SHORT"/></td>
								<td>${dto.capabilityReference}</td>
								<td><a href="viewcertificate.htm?certid=${dto.certid}" class="mainlink">${dto.certifNo}</a></td>
								<td><a href="viewjob.htm?jobid=${dto.jobId}" class="mainlink">${dto.jobNo}</a></td>
								<td class="center">${dto.jobItemNo}</td>
								<td>
									<links:companyLinkInfo companyId="${dto.coid}" onStop="${dto.onStop}" rowcount="0" companyName="${dto.coname}" active="${dto.companyActive}" copy="true" />
								</td>
								<td>
									<links:subdivLinkInfo rowcount="0"
										subdivId="${dto.subdivid}"
										subdivName="${dto.subname}"
										companyActive="${dto.companyActive}"
										companyOnStop="${dto.onStop}"
										subdivActive="${dto.subActive}" />
								</td>
								<td><a href="viewinstrument.htm?plantid=${dto.plantId}" class="mainlink">${dto.plantId}</a></td>
								<td>${dto.plantNo}</td>
								<td>${dto.serialNo}</td>
							</tr>
						</c:forEach>
					
					</body>
				
				</table>
				<table class="default tlast" summary="">
					<tfoot>
						<tr>
							<td>
								<input type="submit" name="export" value="${submitText}" class="float-right">
							</td>
							
						</tr>
					</tfoot>
				</table>
				<t:showResultsPagination rs="${form.rs}" pageNoId="" />
			</form:form>
		</div>						
	</jsp:body>
</t:crocodileTemplate>