<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<c:choose>
				<c:when test="${form.persisted == true}">
					<spring:message code="editclientcert.editclientcert"/>
				</c:when>
				<c:otherwise>
					<spring:message code="editclientcert.addclientcert"/>
				</c:otherwise>
			</c:choose>
		</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/jobs/certificate/EditClientCert.js'></script>
	</jsp:attribute>
	<jsp:body>
		<t:showErrors path="form.*" showFieldErrors="true"/>
		<div class="infobox">
                <%--@elvariable id="form" type="org.trescal.cwms.core.jobs.certificate.form.EditClientCertificateForm"--%>
			<form:form modelAttribute="form" enctype="multipart/form-data">
				<fieldset>
					<legend>
						<c:choose>
							<c:when test="${form.persisted == true}">
								<spring:message code="editclientcert.editclientcert"/>
							</c:when>
							<c:otherwise>
								<spring:message code="editclientcert.addclientcert"/>
							</c:otherwise>
						</c:choose>
					</legend>
					<ol>
						<li>
							<label class="width30"><spring:message code="editclientcert.thirdpartcertno"/>:</label>
							<form:input path="cert.thirdCertNo"/>
							<form:errors path="cert.thirdCertNo"/>
						</li>
						<li>
							<label class="width30"><spring:message code="editclientcert.thirdpartcomp"/>:</label>
							<div class="float-left">													
								<div id="cascadeSearchPlugin">
									<input type="hidden" id="cascadeRules" value="subdiv" />
									<input type="hidden" id="compCoroles" value="supplier" />
									<c:if test="${form.cert.thirdDiv != null}">
										<input type="hidden" id="prefillIds" value="${form.cert.thirdDiv.comp.coid},${form.cert.thirdDiv.subdivid}" />
									</c:if>
								</div>
							</div>
							<!-- clear floats and restore page flow -->
							<div class="clear-0"></div>
						</li>
						<li>
							<label class="width30"><spring:message code="editclientcert.caldate"/>:</label>
							<form:input path="cert.calDate" id="calDate" type="date"/>
							<form:errors path="cert.calDate"/>
						</li>
						<li>
							<label class="width30"><spring:message code="issuedate"/>:</label>
							<form:input path="cert.certDate" id="certDate" type="date"/>
							<form:errors path="cert.certDate"/>
						</li>
						<li>
							<label class="width30"><spring:message code="editclientcert.duration"/>:</label>
							<form:input path="cert.duration"/>
							<form:select path="durationUnitId" items="${units}" itemLabel="value" itemValue="key"/>
							<form:errors path="cert.duration"/>
						</li>
						<li>
							<label class="width30"><spring:message code="editclientcert.updatecaldate"/>:</label>
							<form:checkbox path="updateNextCalDueDate" />
						</li>
						<li>
							<label class="width30"><spring:message code="editclientcert.caltype"/>:</label>
							<form:select path="serviceTypeId" items="${serviceTypes}" itemValue="key" itemLabel="value" />
						</li>
						<c:if test="${calVerificationStatusRequired == true }">
							<li>
								<label class="width30"><spring:message code="viewinstrument.calverificationstatus"/>:</label>
								<form:select path="calVerificationStatus" items="${calVerificationStatuses}" itemLabel="value" itemValue="key"/>&nbsp;
                                <spring:message code="instrument.customeracceptancecriteria"/>:&nbsp;
								<c:out value="${form.inst.instrumentComplementaryField.customerAcceptanceCriteria}"/>
							</li>
						</c:if>
                        <c:if test="${deviationRequired == true }">
                            <li>
                                <label class="width30"><spring:message code="certificate.deviation"/>:</label>
                                <form:input path="deviation" id="deviation"/>&nbsp;
								<c:out value="${form.inst.instrumentComplementaryField.deviationUnits != null
														? form.inst.instrumentComplementaryField.deviationUnits.formattedSymbol
														: ''}"/>
                            </li>
                        </c:if>
						
						<li>
							<label class="width30"><spring:message code="editclientcert.remarks"/>:</label>
							<form:textarea path="cert.remarks" rows="5" cols="80"/>
						</li>			
						<li>
							<label class="width30"><spring:message code="editclientcert.upload"/>:</label>
							<input type="file" name="file" />
						</li>			
						<li>
							<label class="width30">&nbsp;</label>
							<form:button name="Submit" value="Submit"><spring:message code="submit"/></form:button>
						</li>				
					</ol>
				</fieldset>
			</form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>