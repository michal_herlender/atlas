<%-- File name: /trescal/core/jobs/job/jobresults.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="web" tagdir="/WEB-INF/tags/web" %>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="instrument" tagdir="/WEB-INF/tags/instrument" %>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel" %>

<t:crocodileTemplate>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/jobs/job/JobResults.js'></script>
		<script type="text/javascript">
		// does the user want to highlight search criteria?
		var highlight = ${searchCriteria.highlight};
		// does the user want to highlight search criteria?
		if (highlight)
		{
			var searchCrit = new Array (	{ name: 'jsrserial', value: '${searchCriteria.serialno}', classname: 'highlightSerial' },
											{ name: 'jsrplant', value: '${searchCriteria.plantno}', classname: 'highlightPlant' }, 
											{ name: 'mfrResult', value: '${searchCriteria.mfr}', classname: 'highlightMfr' },
											{ name: 'modelResult', value: '${searchCriteria.model}', classname: 'highlightModel' },
											{ name: 'descResult', value: '${searchCriteria.desc}', classname: 'highlightDesc' },
											{ name: 'jsrclientref', value: '${searchCriteria.clientref}', classname: 'highlightClientref' },
											{ name: 'jsrpurchaseord', value: '${searchCriteria.purchaseorder}', classname: 'highlightPurchaseorder' },
											{ name: 'jsrproc', value: '${searchCriteria.procno}', classname: 'highlightProc' });
		
		}
		</script>
	</jsp:attribute>

	<jsp:body>
		<!-- main box body content div -->
		<div class="infobox" id="jobsearchresults">				
			<form:form modelAttribute="jobsearchform" action="" name="searchform" id="searchform" method="post">
				<form:hidden path="coid" />
				<form:hidden path="subdivid" />
				<form:hidden path="jobType" />
				<form:hidden path="personid" />
				<form:hidden path="serialno" />
				<form:hidden path="plantno" />
				<form:hidden path="jobno" />
				<form:hidden path="plantid" />
				<form:hidden path="clientRef" />
				<form:hidden path="purOrder" />
				<form:hidden path="mfr" />
				<form:hidden path="model" />
				<form:hidden path="desc" />
				<form:hidden path="modelid" />
				<form:hidden path="mfrid" />
				<form:hidden path="descid" />
				<form:hidden path="procId" />
				<form:hidden path="bookedInDateBetween" />
				<form:hidden path="completedDateBetween" />
				<form:hidden path="highlight" />
				
				<form:hidden path="bookedInDate1" />
				<form:hidden path="bookedInDate2" />
				<form:hidden path="completedDate1" />
				<form:hidden path="completedDate2" />
				
				<form:hidden path="active" />
				<form:hidden path="pageNo" />
				<form:hidden path="resultsPerPage" />
				<form:hidden path="resultsTotal" />
				<form:hidden path="jobItemCount" />
				
				<t:showResultsPagination rs="${jobsearchform.rs}" pageNoId=""/>
				
				<table class="default4" summary="${jobresults.def4}">
					<thead>
						<tr>
							<td colspan="8">
								<spring:message code="jobresults.jobres"/>
								(${jobsearchform.jobs.size()} jobs with ${jobsearchform.jobItemCount} items)
								<spring:message code="web.export.excel" var="submitText"/>  
								<input type="submit" name="export" value="${submitText}" class="float-right">
							</td>
						</tr>
						<tr>
							<td><spring:message code="jobresults.searchcrit"/>:</td>
							<td colspan="7">
								<c:if test="${not empty searchCriteria.jobno}">
									<spring:message code="jobno"/>: ${searchCriteria.jobno} &nbsp;&nbsp;&nbsp; 
								</c:if>
								<c:if test="${not empty searchCriteria.company}">
									<spring:message code="company"/>: ${searchCriteria.company} &nbsp;&nbsp;&nbsp;
								</c:if>
								<c:if test="${not empty searchCriteria.subdiv}">
									<spring:message code="subdiv"/>: ${searchCriteria.subdiv} &nbsp;&nbsp;&nbsp;
								</c:if>
								<c:if test="${not empty searchCriteria.contact}">
									<spring:message code="contact"/>: ${searchCriteria.contact} &nbsp;&nbsp;&nbsp;
								</c:if>
								<c:if test="${not empty searchCriteria.plantid}">
									<spring:message code="barcode"/>: ${searchCriteria.plantid} &nbsp;&nbsp;&nbsp;
								</c:if>
								<c:if test="${not empty searchCriteria.serialno}">
									<spring:message code="serialno"/>: ${searchCriteria.serialno} &nbsp;&nbsp;&nbsp;
								</c:if>
								<c:if test="${not empty searchCriteria.plantno}">
									<spring:message code="plantno"/>: ${searchCriteria.plantno} &nbsp;&nbsp;&nbsp;
								</c:if>
								<c:if test="${not empty searchCriteria.mfr}">
									<spring:message code="mfr"/>: ${searchCriteria.mfr} &nbsp;&nbsp;&nbsp;
								</c:if>
								<c:if test="${not empty searchCriteria.model}">
									<spring:message code="model"/>: ${searchCriteria.model} &nbsp;&nbsp;&nbsp;
								</c:if>
								<c:if test="${not empty searchCriteria.desc}">
									<spring:message code="jobresults.desc"/>: ${searchCriteria.desc} &nbsp;&nbsp;&nbsp;
								</c:if>
								<c:if test="${not empty searchCriteria.purchaseorder}">
									<spring:message code="jobresults.purchord"/>: ${searchCriteria.purchaseorder} &nbsp;&nbsp;&nbsp;
								</c:if>
								<c:if test="${not empty searchCriteria.clientref}">
									<spring:message code="addjob.clientref"/>: ${searchCriteria.clientref} &nbsp;&nbsp;&nbsp;
								</c:if>
								<c:if test="${not empty searchCriteria.procno}">
									<spring:message code="jobresults.capability"/>: ${searchCriteria.procno} &nbsp;&nbsp;&nbsp;
								</c:if>
								<c:if test="${not empty searchCriteria.jobType}">
									<spring:message code="jobresults.type"/>: ${searchCriteria.jobType} &nbsp;&nbsp;&nbsp;
								</c:if>
								<c:if test="${not empty searchCriteria.active}">
									<spring:message code="jobresults.actonly"/>: ${searchCriteria.active} &nbsp;&nbsp;&nbsp;
								</c:if>
								<c:if test="${not empty bookedInDate1 || not empty jobsearchform.bookedInDate2}">
									<spring:message code="jobsearch.bookindate"/>:
									<c:choose>
										<c:when test="${jobsearchform.bookedInDateBetween}">
											<spring:message code="jobsearch.between"/>&nbsp;
											<fmt:formatDate type="date" dateStyle="SHORT" value="${bookedInDate1}"/>&nbsp;
											<spring:message code="and"/>&nbsp;
											<fmt:formatDate type="date" dateStyle="SHORT" value="${jobsearchform.bookedInDate2}"/>
										</c:when>
										<c:otherwise>
											<spring:message code="jobsearch.on"/>&nbsp;
											<fmt:formatDate type="date" dateStyle="SHORT" value="${bookedInDate1}"/>
										</c:otherwise>
									</c:choose> 
									&nbsp;&nbsp;&nbsp;
								</c:if>
								<c:if
									test="${not empty jobsearchform.completedDate1 || not empty jobsearchform.completedDate2}">
									<spring:message code="jobsearch.completedjob" />:
									<c:choose>
										<c:when test="${jobsearchform.completedDateBetween}">
											<spring:message code="jobsearch.between" />&nbsp;
											<fmt:formatDate type="date" dateStyle="SHORT"
												value="${jobsearchform.completedDate1}" />&nbsp;
											<spring:message code="and" />&nbsp;
											<fmt:formatDate type="date" dateStyle="SHORT"
												value="${jobsearchform.completedDate2}" />
										</c:when>
										<c:otherwise>
											<spring:message code="jobsearch.on" />&nbsp;
											<fmt:formatDate type="date" dateStyle="SHORT"
												value="${jobsearchform.completedDate1}" />
										</c:otherwise>
									</c:choose> 
									&nbsp;&nbsp;&nbsp;
								</c:if>
							</td>
						</tr>
						<tr>
							<th class="jsrjobno" scope="col"><spring:message code="jobno" /></th>  
							<th class="jsrconame" scope="col"><spring:message code="company" /></th>  
							<th class="jsrsubdiv" scope="col"><spring:message code="subdiv" /></th>  
							<th class="jsrcontact" scope="col"><spring:message code="contact" /></th>  
							<th	class="jsrpurchaseord" scope="col"><spring:message code="jobresults.purchord" /></th>
							<th class="jsrjobclientref" scope="col"><spring:message code="addjob.clientref" /></th>
							<th class="jsrdatein" scope="col"><spring:message code="jobresults.datein" /></th>
							<th class="jsrdatecomplete" scope="col"><spring:message code="datecomplete" /></th>
						</tr>									
					</thead>
				</table>
				
				<c:choose>
					<c:when test="${empty jobsearchform.jobs}">
						<table class="default" summary="This table displays a message if no jobs are returned">
							<tbody>
								<tr>
									<td colspan="7" class="bold center">
										<spring:message code="jobresults.string1" />
									</td>
								</tr>
							</tbody>
						</table>
					</c:when>
					<c:otherwise>
						<c:forEach var="jb" items="${jobsearchform.jobs}">	
							<table class="default4" summary="<spring:message code="jobresults.string3" />">											
								<tbody>								
									<tr onclick=" window.location.href='viewjob.htm?jobid=${jb.jobid}' " onmouseover=" $j(this).addClass('hoverrow'); " onmouseout=" $j(this).removeClass('hoverrow'); ">
										<td class="jsrjobno">
										<instrument:jobnoProjectionLinkWeb jobid="${jb.jobid}" jobno="${jb.jobno}" /> 
											</br><spring:message code="jobexpenseitem.header"/><c:out value=": ${jb.jobExpenseItemCount}"/>
										</td>
										
										
										<td class="jsrconame"><links:companyProjectionLinkInfo company="${jb.contact.subdiv.company}" copy="false" rowcount=""/></td>
										<td class="jsrsubdiv"><links:subdivProjectionLinkInfo subdiv="${jb.contact.subdiv}" rowcount=""/></td>
										<td class="jsrcontact"><links:contactProjectionLinkInfo contact="${jb.contact}" rowcount=""/></td>
										<td	class="jsrpurchaseord"><jobs:showJobPosProjection jb="${jb}" /></td>
										<td class="jsrjobclientref">${jb.clientRef}</td>
										<td class="jsrdatein"><fmt:formatDate type="date" dateStyle="SHORT" value="${jb.regDate}"/></td>
										<td class="jsrdatecomplete"><fmt:formatDate type="date" dateStyle="SHORT" value="${jb.dateComplete}"/></td>
									</tr>
									<c:if test="${jb.jobitems.size() > 0}">
										<tr>
											<td colspan="8" style=" padding: 0; ">
												<table class="child-table" border="" summary="<spring:message code="jobresults.string4"/>">
													<thead>
														<tr>
															<th class="jsritem" scope="col"><spring:message code="accessoriesanddefects.item"/></th>
															<th class="jsrinstr" scope="col"><spring:message code="accessoriesanddefects.instrument"/></th>
															<th class="jsrserial" scope="col"><spring:message code="serialno"/></th>
															<th class="jsrplant" scope="col"><spring:message code="plantno"/></th>
															<th class="jsrcaltype" scope="col"><spring:message code="jobresults.caltype"/></th>
															<th class="jsrproc" scope="col"><spring:message code="jobresults.capability"/></th>
															<th class="jsritempo" scope="col"><spring:message code="jobresults.itempo"/></th>
															<th class="jsritemclientref" scope="col"><spring:message code="addjob.clientref"/></td>
															<th class="jsrstatus" scope="col"><spring:message code="jobresults.stat"/></th>
														</tr>
													</thead>
													<tbody>
														<c:forEach var="i" items="${jb.jobitems}">
															<tr style="background-color: <c:choose><c:when test="${i.turn} <= ${fastTrackTurn}"> ${i.serviceType.displayColourFastTrack}; </c:when><c:otherwise> ${i.serviceType.displayColour};</c:otherwise></c:choose>">
																<td class="center"><links:jobitemProjectionLinkInfo jobitem="${i}" jicount="false" jobno="${jb.jobno}" itemssize="${jb.jobitems.size()}" rowcount=""/></td>
																<td>
																<instmodel:showInstrumentModelProjectionLink instrument="${i.instrument}" />
																</td>
																<td class="jsrserial">${i.instrument.serialno}</td>
																<td class="jsrplant">${i.instrument.plantno}</td>
																<td class="jsrcaltype">${i.serviceType.shortName}</td>
																<td class="jsrproc">${i.capability.reference}</td>
																<td>
																	<c:forEach var="jipo" items="${i.jobItemPos}">
																		<c:choose>
																			<c:when test="${not empty jipo.poId}">
																				${jipo.poNumber}<br />
																			</c:when>
																			<c:when test="${not empty jipo.bpoId}">
																				${jipo.bpoNumber}<br />
																			</c:when>
																			<c:otherwise>
																				<spring:message code="jobresults.noitempo" />
																			</c:otherwise>
																		</c:choose>
																	</c:forEach>	
																</td>
																<td class="jsritemclientref">${i.clientRef}</td>
																<td class="status">																																		
																		${ i.state.value }
																</td>																			
															</tr>
														</c:forEach>	
													</tbody>
												</table>
											</td>
										</tr>
									</c:if>
								</tbody>											
							</table>
						</c:forEach>
					</c:otherwise>
				</c:choose>
				<table class="default tlast" summary="">
					<tfoot>
						<tr>
							<td>
								<input type="submit" name="export" value="${submitText}" class="float-right">
							</td>
							
						</tr>
					</tfoot>
				</table>
				<t:showResultsPagination rs="${jobsearchform.rs}" pageNoId=""/>
				
			</form:form>									
	 	</div>
	</jsp:body>
</t:crocodileTemplate>