<%-- File name: /trescal/core/jobs/job/addjob.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel"%>
<%@ taglib prefix="costs" tagdir="/WEB-INF/tags/costs"%>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files"%>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="addjob.headtext"/></span>
		<style>
			.rtradio {
				width: 20px !important;
			}
		</style>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script>

			function updateInstructionSize(instructionSize) {
			}
		</script>
		<script type="module" src="script/components/cwms-translate/cwms-translate.js"></script>
		<script type="module" src="script/components/cwms-datetime/cwms-datetime.js"></script>
		<script src='script/trescal/core/jobs/job/AddJob.js'></script>
		<script type="module" src="script/components/cwms-instructions/cwms-instructions.js"></script>
		<script type="module"
				src="${pageContext.request.contextPath}/script/components/cwms-cascade-search/index.js"></script>
		<script type="module"
				src="${pageContext.request.contextPath}/script/components/cwms-addjob-calloff-items/cwms-addjob-calloff-items.js"></script>
		<script type="module"
				src="${pageContext.request.contextPath}/script/components/cwms-addjob-collected-instruments/cwms-addjob-collected-instruments.js"></script>
		<script type="module"
				src="${pageContext.request.contextPath}/script/components/cwms-addjob-bpo/cwms-addjob-bpo.js"></script>
	</jsp:attribute>
	<jsp:body>
		<!-- error section displayed when form submission fails -->
		<form:errors path="ajf.*">
       				 <div class="warningBox1">
              		  <div class="warningBox2">
              	       <div class="warningBox3">
                    	<h4 class="center-0 attention"><spring:message code="addjob.attention"/></h4>
                      <c:forEach var="e" items="${messages}">
                        <div class="center attention"><c:out value="${e}"/></div>
                      </c:forEach>
                     </div>
                   </div>
                  </div>
         </form:errors>
		<!-- end error section -->
		<!-- infobox div containing form elements for creating a new job -->
		<div class="infobox" id="createjob">
			<div class="float-right">
				<!-- <form action="addjob.htm" method="get">
					<spring:message code="addjob.string1"/>: <input type="text" name="plantid" onkeypress=" if (event.keyCode==13){ $j(this).prev().submit(); }" /> -->
					<spring:message code="addjob.string1"/>:
					<input type="text" name="plantid" onkeypress="if(event.keyCode==13) {showInstruments(window.location.pathname, 'plantid', 'div#createjob input[name=&quot;plantid&quot;]', null, $j(this).parent(), $j(this).parent().parent(), true, true); }" />
					<input type="hidden" id="plantid" value="${scannedBarcode}"/>
				<!-- </form> -->
			</div>
		<div class="clear"></div>
		<form:form action="" method="post" id="addjobform" enctype="multipart/form-data" modelAttribute="ajf" onsubmit="$j(this).find(':input[type=submit]').prop('disabled', true);">
			<input type="hidden" name="defaultpo" id="defaultpo"/>
			<input type="hidden" name="defaultbpo" id="defaultbpo"/>
				<fieldset>
					<legend>
						<spring:message code="addjob.legend"/>
						<c:choose>
							<c:when test="${ajf.createJobFrom == 'job'}">
								(<spring:message code="addjob.string2"/> <a href="viewjob.htm?jobid=${ajf.basedOnJob.jobid}" class="mainlink">${ajf.basedOnJob.jobno}</a>)
							</c:when>
							<c:when test="${ajf.createJobFrom == 'calloffitems'}">
								(<spring:message code="addjob.string3"/>)
							</c:when>
						</c:choose>
					</legend>
					<ol>
						<li>
							<label for="jobType"><spring:message code="addjob.jobtype"/>:</label>
							<form:select path="jobType" items="${jobTypes}" itemValue="name" itemLabel="description"
										 onchange=" jobTypeChange(this.value); "/>
						</li>
						<li>
							<label><spring:message code="addjob.contaddr"/>:</label>
							<cwms-cascade-search-parent id="cascade-search" companyRoles='["client","business"]'
														cascadeRules='["subdiv","address","location","contact"]'
														addressType="Delivery"
														prefills="${cwms:objectToJson(prefills)}"
							>

							</cwms-cascade-search-parent>
						</li>
						<li>
							<label><spring:message code="calloffitems.headtext"/>:</label>
							<cwms-addjob-calloff-items name="selectedCallOffItems"></cwms-addjob-calloff-items>
						</li>
						<li>
							<label><spring:message code="collectedinstruments.label"/>:</label>
							<cwms-addjob-collected-instruments
									name="selectedCollectedInsts"></cwms-addjob-collected-instruments>
						</li>
						<li>
							<label><spring:message code="jicontractreview.blankpo"/>:</label>
							<cwms-addjob-bpo name="bpo"></cwms-addjob-bpo>
						</li>


						<li>
							<label for="recordtype"><spring:message code="addjob.recordtype"/>:</label>
							<div class="float-left">
								<table>
									<tr>
										<td colspan="2" rowspan="3"><form:radiobutton path="recordType"
																					  id="standardradio" name="rt"
																					  class="rtradio" value="STANDARD"
																					  onchange="recordTypeChange($j(this)); updateFormAction($j(this), 'addjob');"/><spring:message
												code="addjob.recordtype.standard"/></td>
										<td></td>
									</tr>
								</table>
							</div>
							<div class="float-left pad-left-small" >
								<table>
									<tr>
										<td colspan="2">
											<form:radiobutton path="recordType" id="fileradio" name="rt" class="rtradio" 
												onchange="recordTypeChange($j(this)); getExchangesFormatForSelectedCompany(); updateFormAction($j(this), 'addjobff');"
												value="FILE"/>
											<spring:message code="addjob.recordtype.file"/>
										</td>
										<td></td>
									</tr>
									<tr>
										<td><label><spring:message code="addjob.recordtype.file.exchangeformat"/> :</label></td>
										<td><form:select path="exchangeFormat" type="text" id="exchangeformat"></form:select></td>
										<td></td>
									</tr>
									<tr>
										<td><label><spring:message code="addjob.recordtype.file.filetoupload"/> :</label></td>
										<td colspan="2"><form:input path="uploadFile" type="file" id="uploadfile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" /></td>
									</tr>
								</table>
							</div>
							<div class="float-left pad-left">
								<table>
									<tr>
										<td colspan="2">
											<form:radiobutton path="recordType" id="prebookingradio" name="rt" 
												class="rtradio" value="PREBOOKING" onchange="recordTypeChange($j(this)); getPrebookingListForSelectedCompany(); updateFormAction($j(this), 'addjobfp');"/>
												<spring:message code="addjob.recordtype.prebooking"/>
										</td>
										<td></td>
									</tr>
									<tr>
										<td>
											<label><spring:message code="addjob.recordtype.prebooking.instclientid"/>:</label>
										</td>
										<td>
											<input type="text" id="plantno"/>
										<td>
											<button id="search" onclick="getPrebookingListForSelectedCompany(); return false;">
												<spring:message code="addjob.recordtype.prebooking.search"/>
											</button>
										</td>
									</tr>
									<tr>
										<td>
											<label><spring:message code="addjob.recordtype.prebooking.jobsavailable"/> :</label>
										</td>
										<td>
											<select id="availableprebookings" name="asnId" onchange="deleteAllPurchOrd(); createPurchOrdFromPrebooking($j(this).val()); selecteBPOFromPrebooking($j(this).find(':selected').data('bpoid')); return false;"></select>
										</td>
										<td></td>
									</tr>
								</table>
							</div>
						</li>
						
						<li>
							<label for="clientref"><spring:message code="addjob.clientref"/>:</label>
							<form:input path="clientref" name="clientref" id="clientref" maxlength="30" tabindex="2"/>
						</li>
						<li>
							<label for="currencyCode"><spring:message code="addjob.currency"/>:</label>
							<form:select path="currencyCode">
								<c:forEach var="currency" items="${currencyList}">
									<form:option value="${currency.currencyCode}">
										<c:out value="${currency.currencyCode} "/>
										${currency.currencyERSymbol}
										<c:out value=" [1:${currency.defaultRate}]" />
										<c:if test="${ajf.currencyCode != null && currency.currencyCode == ajf.currencyCode}">
											<c:out value=" - default" />
										</c:if>
									</form:option>
								</c:forEach>
							</form:select>
							<form:errors path="currencyCode"/>
						</li>
						<li id="PO">
							<label><spring:message code="addjob.po"/>:</label>
							<span>
								<a href="#" class="mainlink" onclick="event.preventDefault(); purchaseOrdContent();" title="">
									<spring:message code="addjob.createpo"/>
								</a>
							</span>
						</li>
						<c:if test="${not empty businessAddresses}">
							<c:choose>
								<c:when test="${ajf.jobType == 'SITE'}">
									<c:set var="visibilitySite" value="vis" />
									<c:set var="visibilityStandard" value="hid" />
								</c:when>
								<c:otherwise>
									<c:set var="visibilitySite" value="hid" />
									<c:set var="visibilityStandard" value="vis" />
								</c:otherwise>
							</c:choose>
							<c:set var="defLoc" value="0"/>
							<c:if test="${not empty ajf.bookedInLocId}">
								<c:set var="defLoc" value="${ajf.bookedInLocId}"/>
							</c:if>
							<c:set var="defAddr" value="${ajf.bookedInAddrId}"/>
							<spring:message var="emptyText" code="addjob.nolocationsforaddress"/>
							<li>
								<label><spring:message code="addjob.bookinaddr"/>:</label>
								<span id="bookedInAddrStandard" class="${visibilityStandard}">
									<form:select path="bookedInAddrId" id="bookedInAddrId" tabindex="5" onchange="bookingInAddrChange(${useLocations}, this.value, ${defLoc}, '${emptyText}'); return true;">
										<c:forEach var="address" items="${businessAddresses}" varStatus="status">
											<%-- This defAddr is set here if there is none in the form, e.g. no default address for subdivision--%>
											<c:if test="${status.first && (empty defAddr)}">
												<c:set var="defAddr" value="${address.addrid}"/>
											</c:if>
											<form:option value="${address.addrid}" >
												${address.sub.comp.coname} - ${address.sub.subname} - ${address.addr1}
											</form:option>
										</c:forEach>
									</form:select>
								</span>
								<span id="bookedInAddrSite" class="${visibilitySite}">
									<spring:message code="addjob.sitejobaddress" /> 
								</span>
							</li>
							<c:if test="${useLocations}">
								<li id="bookingInLoc">
									<label><spring:message code="addjob.bookinloc"/>:</label>
									<span id="bookedInLocStandard" class="${visibilityStandard}">
										<form:select id="bookedInLocId" path="bookedInLocId" tabindex="6">
											<c:set var="locationCount" value="0" />
											<c:forEach var="location" items="${businessLocations}">
												<c:if test="${location.add.addrid == defAddr}">
													<form:option value="${location.locationid}" >
														${location.location}
													</form:option>
													<c:set var="locationCount" value="${locationCount + 1}" />
												</c:if>
											</c:forEach>
											<c:if test="${locationCount == 0}">
												<form:option value="0" >
													<spring:message code="addjob.nolocationsforaddress"/>
												</form:option>
											</c:if>
										</form:select>
									</span>
									<span id="bookedInLocSite" class="${visibilitySite}">
										<spring:message code="addjob.sitejoblocation" /> 
									</span>
								</li>
							</c:if>
						</c:if>
						<c:if test="${ajf.createJobFrom == 'job'}">
							<li>
									<form:label path="oldJobItemIds"><spring:message code="addjob.includeitems"/>:</form:label>
									<div class="float-left">
										<c:forEach var="oldJobItem" items="${ajf.basedOnJob.items}">
											<c:set var="instrument" value="${oldJobItem.inst}"/>
											<c:set var="onAnotherJob" value="false"/>
											<c:forEach var="jobItem" items="${instrument.jobItems}">
												<c:if test="${jobItem.state.active}">
													<c:set var="onAnotherJob" value="true"/>
												</c:if>
											</c:forEach>
											<input type="checkbox"
												name="oldJobItemIds"
												class="checkbox"
												<c:choose>
													<c:when test="${onAnotherJob}">disabled</c:when>
													<c:otherwise>checked</c:otherwise>
												</c:choose>
												value="${oldJobItem.jobItemId}"
											/>
											${oldJobItem.itemNo} - <cwms:showmodel instrumentmodel="${oldJobItem.inst.model}"/>
											<c:if test="${onAnotherJob}">
												<span class="attention">(<spring:message code="addjob.onanotherjob"/>)</span>
											</c:if>
											<br/>
										</c:forEach>
									</div>
								<div class="clear"></div>
							</li>
							<li>
									<form:label path="oldJobServiceIds"><spring:message code="addjob.includeservices"/>:</form:label>
									<div class="float-left">
										<c:forEach var="oldJobService" items="${ajf.basedOnJob.expenseItems}">
											<c:set var="model" value="${oldJobService.model}"/>
											<input type="checkbox" name="oldJobServiceIds" class="checkbox" checked
												value="${oldJobService.id}"/>
											${oldJobService.itemNo} - <cwms:showmodel instrumentmodel="${model}"/>, ${oldJobService.comment}
											<br/>
										</c:forEach>
									</div>
								<div class="clear"></div>
							</li>
						</c:if>
						
						<li>
							<label><spring:message code="instructiontype.receipt"/>:</label>
							<cwms-instructions instruction-types="RECEIPT" show-mode="BASIC"></cwms-instructions>
						</li>
							
						<li id="receiptDateLI">
							<label><spring:message code="addjob.receiptdate"/>:<span class="color:red">*</span></label>
							<form:input type="datetime-local" path="receiptDate"
									step="60" max="${cwms:isoDatetime(currentDate)}"/>
						</li>
						
						<li id="pickupDateLI">
							<label><spring:message code="addjob.pickupdate"/>:</label>
							<form:input type="datetime-local" path="pickupDate"
									step="60" max="${cwms:isoDatetime(currentDate)}"/>
						</li>
						
						<li>
							<label><spring:message code="addjob.overrideDateInOnJobItems"/>:</label>
							<form:checkbox path="overrideDateInOnJobItems" id="overrideDateInOnJobItems" onClick="checkOverrideDateInOnJobItems(this);"/>
							<div id="overrideDateInOnJobItemsDialog" class="hid" title="<spring:message code='viewjob.overrideDateInOnJobItemsDialogTitle'/>">
								<spring:message code="viewjob.overrideDateInOnJobItemsDialogText"/>
							</div>
						</li>
						
						<li id="transportInLI">
							<div class="float-left">
								<label><spring:message code="addjob.transportin" />:</label>
								<form:select tabindex="9" path="transportIn" onchange="showCarrier()">
									<c:forEach var="t" items="${transportOptions}">
										<option value="${t.id}">
											<c:choose>
												<c:when test="${not empty t.localizedName}">
													${t.localizedName }
												</c:when>
												<c:otherwise>
													<t:showTranslationOrDefault translations="${t.method.methodTranslation}" defaultLocale="${defaultlocale}"/>
												</c:otherwise>
											</c:choose>
										</option>
									</c:forEach>
								</form:select>
							</div>
							<div class="float-left pad-left hid" id="carrier_div">
								<label><spring:message code="addjob.carrier" />:</label>
								<form:select path="carrier" tabindex="10" id="carrier" items="${couriers}" itemLabel="name" itemValue="courierid"></form:select>
							</div>
							<div class="float-left pad-left hid" id="trackingnumber_div">
								<label><spring:message code="addjob.trackingnumber" />:</label>
								<form:input path="trackingNumber" tabindex="11" type="text" id="trackingnumber"/>
							</div>
						</li>
						<li id="transportOutLI">
							<label><spring:message code="addjob.transportout" />:</label>
							<form:select tabindex="12" path="transportOut">
								<c:forEach var="t" items="${transportOptions}">
									<option value="${t.id}">
										<c:choose>
											<c:when test="${not empty t.localizedName}">
												${t.localizedName }
											</c:when>
											<c:otherwise>
												<t:showTranslationOrDefault translations="${t.method.methodTranslation}" defaultLocale="${defaultlocale}"/>
											</c:otherwise>
										</c:choose>
									</option>
								</c:forEach>
							</form:select>
						</li>
						<li id="packageLI">
							<div class="float-left">
								<label><spring:message code="addjob.numberofpackages" />:</label>
								<form:input path="numberOfPackages" tabindex="13" id="numberofpackages"/>
							</div>
							<div class="float-left pad-left">
								<label><spring:message code="addjob.packagetype" />:</label>
								<form:input path="packageType" tabindex="14" id="packagetype"></form:input>
							</div>
						</li>
						<li id="storageAreaLI">
							<label><spring:message code="addjob.storagearea" />:</label>
							<form:input path="storageArea" tabindex="15" id="storagearea"/>
						</li>
						
						<li>
							<label for="submit">&nbsp;</label>
							<input type="submit" id="submit" value="<spring:message code="addjob.headtext" />" tabindex="8" />
						</li>
					</ol>
				</fieldset>
			</form:form>
		</div>
		<!-- end of infobox div -->
		<div class="clear">&nbsp;</div>
	</jsp:body>
</t:crocodileTemplate>