<%-- File name: /trescal/core/jobs/certificate/viewcertificate.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="viewcertificate.headtext" /> (${cert.certno})</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/jobs/certificate/ViewCertificate.js'></script>
	</jsp:attribute>
	<jsp:body>
		<c:set var="editCertVisibility" value="hid" />
		<c:set var="certInstsVisibility" value="vis" />
		<form:errors path="form.*">
			<c:set var="editCertVisibility" value="vis" />
			<c:set var="certInstsVisibility" value="hid" />
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<h5 class="center-0 attention">
							<spring:message code="error.save"/>
						</h5>
					</div>
				</div>
			</div>
		</form:errors>
		<!-- this infobox contains certificate info -->											
		<div class="infobox">
			<fieldset>
				<legend><spring:message code="viewcertificate.headtext" /> (${cert.certno})</legend>
				<!-- left floated column -->
				<div class="displaycolumn">
					<ol>																		
						<li>
							<label><spring:message code="certsearchresults.certno" />:</label>
							<span>${cert.certno}</span>
						</li>
						<c:if test="${not empty cert.cal}">
							<li>
								<label><spring:message code="viewcertificate.calprocess" />:</label>
								<span>${cert.cal.calProcess.process}</span>
							</li>
						</c:if>
						<li>
							<label><spring:message code="editclientcert.caltype" />:</label>
							<span><cwms:besttranslation translations="${cert.calType.serviceType.longnameTranslation}"/></span>
						</li>
						<li>
							<label><spring:message code="certsearch.caldate" />:</label>
							<span><fmt:formatDate value="${cert.calDate}" dateStyle="medium" type="date" /></span>
						</li>
						<li>
							<label><spring:message code="certsearch.issdate" />:</label>
							<span><fmt:formatDate value="${cert.certDate}" type="date" dateStyle="SHORT"/></span>
						</li>
						<li>
							<label><spring:message code="viewcertificate.calduration" />:</label>
							<span><c:if test="${not empty cert.duration}"> ${cert.duration}&nbsp;
								<c:choose><c:when test="${cert.unit != null}">${cert.unit.getName(cert.duration)}</c:when>
									<c:otherwise><spring:message code="months"/></c:otherwise></c:choose>
							</c:if></span>
						</li>
						<li>
							<label><spring:message code="viewcertificate.antjobno" />:</label>
							<div class="float-left padtop">
								<c:if test="${cert.links.size() > 0}">
									<c:set var="currentJobNo" value="" />
									<c:forEach var="cl" items="${cert.links}" varStatus="feCount">
										<c:if test="${currentJobNo != cl.jobItem.job.jobno}">
											<c:choose>
												<c:when test="!${feCount.first}">
													, <links:jobnoLinkDWRInfo rowcount="1001" jobno="${cl.jobItem.job.jobno}" jobid="${cl.jobItem.job.jobid}" copy="true" />
												</c:when>
												<c:otherwise>
													<links:jobnoLinkDWRInfo rowcount="1002" jobno="${cl.jobItem.job.jobno}" jobid="${cl.jobItem.job.jobid}" copy="true" />
												</c:otherwise>
											</c:choose>
										</c:if>
										<c:set var="currentJobNo" value="${cl.jobItem.job.jobno}" />								
									</c:forEach>
								</c:if>
							</div>
							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
						</li>
						<li>
							<label><spring:message code="viewcertificate.purchordno" />:</label>
							<c:choose>
								<c:when test="${cert.links.size() > 1}">
									<c:forEach var="cl" items="${cert.links}" varStatus="feCount">
										<c:if test="${feCount.first}">
											<c:choose>
												<c:when test="${cl.jobItem.jobItemPOs.size() > 0}">
													<div class="float-left padtop">
														<c:forEach var="item" items="${cl.jobItem.jobItemPOs}" varStatus="itemCount">
															<c:choose>
																<c:when test="${not empty item.po}">
																	${item.po.poNumber} [PO]<c:if test="${!itemCount.first}"><br /></c:if>
																</c:when>
																<c:otherwise>									
																	${item.bpo.poNumber} [BPO]<c:if test="${!itemCount.first}"><br /></c:if>
																</c:otherwise>
															</c:choose>						
														</c:forEach>
														&nbsp;
													</div>
													<!-- clear floats and restore page flow -->
													<div class="clear"></div>
												</c:when>
												<c:otherwise>
													<span>&nbsp;-- <spring:message code="viewcertificate.nopurchord" /> --&nbsp;</span>
												</c:otherwise>
											</c:choose>
										</c:if>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<span>&nbsp;-- <spring:message code="viewcertificate.nopurchord" /> --&nbsp;</span>
								</c:otherwise>
							</c:choose>										
						</li>
						<li>
							<label><spring:message code="viewcertificate.certref" />:</label>
							<c:choose>
								<c:when test="${cert.links.size() > 1}">																		
									<c:forEach var="cl" items="${cert.links}" varStatus="feCount">
										<c:if test="${feCount.first}">
											<c:choose>	
												<c:when test="${cl.jobItem.job.clientRef.trim().length() > 0}">
													<div class="float-left padtop">
														${cl.jobItem.job.clientRef}
														&nbsp;
													</div>
													<!-- clear floats and restore page flow -->
													<div class="clear"></div>
												</c:when>
												<c:otherwise>
													<span>&nbsp;-- <spring:message code="viewcertificate.noclientref" /> --&nbsp;</span>
												</c:otherwise>
											</c:choose>
										</c:if>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<span>&nbsp;-- <spring:message code="viewcertificate.noclientref" /> --&nbsp;</span>
								</c:otherwise>
							</c:choose>
							</li>
							<c:if test="${hasverificationstatus == true}">
								<li>
									<label><spring:message code="viewinstrument.calverificationstatus"/>:</label>
									<span><c:out value="${cert.calibrationVerificationStatus != null ?
														cert.calibrationVerificationStatus.getName() : ''}"/></span>
								</li>
							</c:if>
							<c:if test="${hasdeviation == true}">
								<li>
									<label><spring:message code="certificate.deviation"/>:</label>
									<span><c:out value="${form.deviation}"/></span>
								</li>
							</c:if>
						<c:if test="${hasclass == true}">
							<li>
								<label><spring:message code="certificate.class"/>:</label>
								<span><c:out value="${cert.certClass != null ? cert.certClass.getDescription() : ''}"/></span>
							</li>
						</c:if>
						<c:if test="${not empty cert.remarks}">
							<li>
								<label><spring:message code="viewcertificate.remarks" />:</label>
								<span>${cert.remarks}</span>
							</li>
						</c:if>
						<c:if test="${not empty cert.thirdCertNo}">
							<li>
								<label><spring:message code="jicertificates.externalno" />:</label>
								<span>${cert.thirdCertNo}</span>
							</li>
						</c:if>
					</ol>
					
				</div>
				<!-- end of left column -->
				
				<!-- right floated column -->
				<div class="displaycolumn">
					<ol>
						<li>
							<label><spring:message code="viewcertificate.jobcomp" />:</label>
							<span>
								<c:if test="${cert.links.size() > 0}">
									<c:forEach var="cl" items="${cert.links}" varStatus="feCount">
										<c:if test="${feCount.first}">
											${cl.jobItem.job.con.sub.comp.coname}
										</c:if>
									</c:forEach>
								</c:if>
							</span>
						</li>
						<c:set var="itemonbehalfcomp" value="" />
						<c:if test="${cert.links.size() > 0}">
							<c:forEach var="cl" items="${cert.links}" varStatus="feCount">
								<c:if test="${feCount.first && not empty cl.jobItem.onBehalf}">
									<c:set var="itemonbehalfcomp" value="${cl.jobItem.onBehalf.company.coname}" />
								</c:if>
							</c:forEach>
						</c:if>
						<c:if test="${itemonbehalfcomp != ''}">
							<li>
								<label><spring:message code="viewcertificate.onbehcomp" />:</label>
								<span>${itemonbehalfcomp}</span>
							</li>
						</c:if>
						<li>
							<label>
								<spring:message code="exchangeformat.fieldname.restriction"/>:
							</label>
							<span><cwms:yesno flag="${cert.restriction}"/></span>
						</li>
						<li>
							<label>
								<spring:message code="exchangeformat.fieldname.adjustment"/>:
							</label>
							<span><cwms:yesno flag="${cert.adjustment}"/></span>
						</li>
						<li>
							<label>
								<spring:message code="exchangeformat.fieldname.optimization"/>:
							</label>
							<span><cwms:yesno flag="${cert.optimization}"/></span>
						</li>
						<li>
							<label>
								<spring:message code="exchangeformat.fieldname.repair"/>:
							</label>
							<span><cwms:yesno flag="${cert.repair}"/></span>
						</li>
					</ol>					
					
					<div class="text-center">
						<div>
							<h4><spring:message code="viewcertificate.viewcertbel" /></h4>
						</div>
						<c:set var="filepath" value="" />
						<c:if test="${cert.hasCertFile}">
							<c:set var="filepath" value="${cert.certFilePath}" />
						</c:if>
						<c:choose>
							<c:when test="${filepath != ''}">
								<c:url var="downloadURL" value="downloadfile.htm"><c:param name="file" value="${cert.encryptedCertFilePath}" /></c:url>
								<a href="${downloadURL}" target="_blank">
									<img src="img/doctypes/pdf-large.png" width="100" height="120" alt="<spring:message code="viewcertificate.imgpdflarge" />" title="<spring:message code="viewcertificate.imgpdflarge" />" />
								</a><br>
								[<spring:message code="viewcertificate.string1" />]
							</c:when>
							<c:otherwise>
								<img src="img/doctypes/pdf-large-unavailable.png" width="100" height="120" alt="<spring:message code="viewcertificate.imgpdflargeunavailable" />" title="<spring:message code="viewcertificate.imgpdflargeunavailable" />" />
							</c:otherwise>
						</c:choose>
					</div>					
				</div>
			</fieldset>
		</div>
		
		<!-- sub navigation menu which displays and hides sections of page as well as linking to external pages -->
		<div id="subnav">
			<dl>						
				<dt><a href="#" onclick="switchMenuFocus(menuElements, 'certinsts-tab', false); return false; " id="certinsts-link" class="selected" title="<spring:message code="viewcertificate.viewlinkinstr" />"><spring:message code="viewcertificate.certitems" /> 
						(
						<c:choose>
							<c:when test="${cert.links.size() > 0}">${cert.links.size()}</c:when>
							<c:otherwise>${cert.instCertLinks.size()}</c:otherwise>
						</c:choose>)
					</a></dt>
				<dt><a href="#" onclick="switchMenuFocus(menuElements, 'editcert-tab', false); return false; " id="editcert-link" title="<spring:message code="viewcertificate.editcert" />"><spring:message code="viewcertificate.editcert" /></a></dt>
			</dl>
		</div>
		
		<!-- this infobox contains all edit certificate options -->											
		<div class="infobox">
			<div id="certinsts-tab" class="${certInstsVisibility}">
				<table class="default4 certInsts" summary="This table displays all certificate instruments">
					<thead>
						<tr>
							<td colspan="6">
								<spring:message code="viewcertificate.certitems" /> (
								<c:choose>
									<c:when test="${cert.links.size() > 0}">${cert.links.size()}</c:when>
									<c:otherwise>${cert.instCertLinks.size()}</c:otherwise>
								</c:choose>)
							</td>
						</tr>
						<tr>
							<th class="barcode" scope="col"><spring:message code="barcode" /></th>
							<th class="inst" scope="col"><spring:message code="viewcertificate.instrument" /></th>
							<th class="serial" scope="col"><spring:message code="serialno" /></th>
							<th class="plant" scope="col"><spring:message code="plantno" /></th>
							<th class="jobno" scope="col"><spring:message code="jobno" /></th>
							<th class="itemno" scope="col"><spring:message code="certmanagement.itemno" /></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="6">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody>
						<c:choose>
							<c:when test="${cert.links.size() > 0}">
								<c:forEach var="cl" items="${cert.links}" varStatus="feCount">
									<tr style=" background-color: ${cert.calType.serviceType.displayColour}; ">
										<td class="barcode"><links:instrumentLinkDWRInfo displayCalTimescale="true" displayName="false" instrument="${cl.jobItem.inst}" caltypeid="${cl.jobItem.calType.calTypeId}" rowcount="${feCount.index}" displayBarcode="true" /></td>
										<td class="inst"><instmodel:showInstrumentModelLink instrument="${cl.jobItem.inst}" caltypeid="${cl.jobItem.calType.calTypeId}" /></td>
										<td class="serial">${cl.jobItem.inst.serialno}</td>
										<td class="plant">${cl.jobItem.inst.plantno}</td>
										<td class="jobno"><links:jobnoLinkDWRInfo jobid="${cl.jobItem.job.jobid}" jobno="${cl.jobItem.job.jobno}" copy="false" rowcount="${feCount.index}" />
										<td class="itemno"><links:jobitemLinkDWRInfo jobitem="${cl.jobItem}" jicount="false" rowcount="${feCount.index}" /></td>
									</tr>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<c:forEach var="icl" items="${cert.instCertLinks}" varStatus="feCount">
									<tr style=" background-color: ${cert.calType.serviceType.displayColour}; ">
										<td class="barcode">${icl.inst.plantid}</td>
										<td class="inst"><links:instrumentLinkDWRInfo displayCalTimescale="false" displayName="true" instrument="${icl.inst}" caltypeid="0" rowcount="${feCount.index}" displayBarcode="false" /></td>
										<td class="serial">${icl.inst.serialno}</td>
										<td class="plant">${icl.inst.plantno}</td>
										<td class="jobno"></td>
										<td class="itemno"></td>													
									</tr>										
								</c:forEach>											
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
			</div>
			
			<div id="editcert-tab" class="${editCertVisibility}">		
				<form:form action="" method="post" id="editcertform" modelAttribute="form">
					<fieldset>
						<legend><spring:message code="viewcertificate.editcert" /> (${cert.certno})</legend>
						<ol>
							<c:if test="${cert.type == 'CERT_THIRDPARTY'}">
								<li>
									<label><spring:message code="certsearch.thirdpartcertno" />:</label>
									<form:input path="thirdCertNo" />
									<form:errors path="thirdCertNo" class="attention"/>
								</li>
								<li>
									<label><spring:message code="editclientcert.thirdpartcomp" />:</label>
									<div class="float-left">													
										<!-- this div creates a new cascading search plugin. The default search contains an input 
											 field and results box for companies, when results are returned this cascades down to
											 display the subdivisions within the selected company. This default behavoir can be obtained
											 by including the hidden input field with no value or removing the input field. You can also
											 add address and/or contact searches to the cascade by including the values 'contact' and/or 'address'
											 in the hidden inputs value attribute separated by a comma (e.g value="contact", value="address",
											 value="address,contact").  You can also swap these values around (e.g. value="contact,address").  
										 -->									
										<div id="cascadeSearchPlugin">
											<input type="hidden" id="cascadeRules" value="subdiv" />
											<input type="hidden" id="compCoroles" value="supplier,business" />
											<input type="hidden" id="prefillIds" value="${form.coid},${form.subdivid},," />
										</div>
									</div>
									<!-- clear floats and restore page flow -->
									<div class="clear-0"></div>
									<form:errors path="subdivid" class="attention"/>

								</li>
							</c:if>
							<c:if test="${not empty cert.cal.calProcess}">
								<li>
									<label><spring:message code="viewcertificate.calprocess" />:</label>
									<form:select path="calProcessId">
										<form:options itemLabel="process" items="${calprocesses}" itemValue="id" />
									</form:select>
								</li>
							</c:if>
							<li>
								<label><spring:message code="certsearch.caldate" />:</label>
								<form:input path="calDate" type="date" autocomplete="off" />
								<form:errors path="calDate" class="attention"/>
							</li>
							<li>
								<label><spring:message code="certsearch.issdate" />:</label>
								<form:input path="certDate" type="date" autocomplete="off" />
								<form:errors path="certDate" class="attention"/>
							</li>
							<li>
								<label><spring:message code="certsearchresults.duration" />:</label>
								<form:input path="duration" /> 
								<form:select path="durationUnit" items="${units}" itemLabel="value" itemValue="key"/>
								<form:errors path="duration" class="attention"/>
								<form:errors path="durationUnit" class="attention"/>
							</li>
                            <c:if test="${hasverificationstatus == true}">
		                        <li>
		                            <label><spring:message code="viewinstrument.calverificationstatus"/>:</label>

                                    <form:select path="calibrationVerificationStatus"
                                                 items="${verificationStatuses}"
                                                 itemValue="key" itemLabel="value"
                                                 disabled="${(caneditverificationstatus != true) ? 'true' : 'false'}"/>
		                            <form:errors path="calibrationVerificationStatus" class="attention"/>
									&nbsp;
									<spring:message code="instrument.customeracceptancecriteria"/>
									&nbsp;
									<c:out value="${form.acceptanceCriteria}"/>
		                        </li>
                    		</c:if>
                            <c:if test="${hasdeviation == true}">
                                <li>
                                    <label><spring:message code="certificate.deviation"/>:</label>
                                    <form:input path="deviation" id="deviation"/>&nbsp;<c:out value="${deviationunitsymbol}"/>
                                    <form:errors path="deviation" class="attention"/>
                                </li>
                            </c:if>
							<c:if test="${hasclass == true}">
								<li>
									<label><spring:message code="certificate.class"/>:</label>
									<form:select path="certificateClass"
												 items="${certificateclasses}"
												 itemValue="key" itemLabel="value"/>
								</li>
							</c:if>
                    		<li>
                    			<label><spring:message code="viewcertificate.remarks"/></label>
                    			<form:textarea path="remarks" rows="5" cols="40"/>
                    		</li>
							<li>
								<label>&nbsp;</label>
								<input type="submit" value="Submit" />
							</li>				
						</ol>
					</fieldset>
				</form:form>
			</div>
		</div>
	
		<!-- this section contains all certificate notes -->
		<t:showTabbedNotes entity="${cert}" privateOnlyNotes="${privateOnlyNotes}" noteTypeId="${cert.certid}" noteType="CERTNOTE" />
	</jsp:body>		
</t:crocodileTemplate>