<%-- File name: /trescal/core/jobs/jobitem/addnewitemstojob.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel"%>
<%@ taglib prefix="inst" tagdir="/WEB-INF/tags/instrument"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

			<spring:message var="${copySelDownText}"
				code="updateallitems.copyseldown" />
			<spring:message var="${copySelUpText}"
				code="updateallitems.copyselup" />
<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message
				code="addnewitemstojob.headtext" /></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type="text/javascript" src="script/trescal/core/jobs/jobitem/AddNewItemsToJob.js"></script>
    	<script type="module" src="script/components/cwms-duplicates/main.js"></script>
		<script type="module" src="script/components/cwms-spinner/cwms-spinner.js"></script>
		<script type='module'
				src='script/components/search-plugins/cwms-modelcode-search/cwms-modelcode-search.js'></script>
		<script type="text/javascript">
			var threadArray = new Array();
			<c:forEach var="thread" items="${threadTypes}">
				threadArray.push({id: ${thread.id}, type: '${thread.type}'});
			</c:forEach>
			var jobId = '${pageContext.request.getParameter("jobid")}';
			var bookingInAddrId = '${pageContext.request.getParameter("bookingInAddrId")}';
		</script>
	</jsp:attribute>
	<jsp:body>
		<cwms-spinner class="hid"></cwms-spinner>
		<form:form modelAttribute="form" action="" id="addnewitemstojobform" method="post"
		    data-coid="${job.con.sub.comp.coid}"
		    data-group-id="${job.con.sub.comp.companyGroup.id}"
		    data-confirm-on-submit="${confirmOnSubmit}"
		    data-allow-contract-review="${allowContractReview}"
		    data-disallow-same-plant-number="${disallowSamePlantNumber}" >
			<c:set var="modelsInBasket" value="false" />
			<c:set var="modCount" value="0" />
			<c:forEach var="bi" items="${basketItems}">
				<c:if test="${not empty bi.model}">
					<c:set var="modelsInBasket" value="true" />
					<c:set var="modCount" value="${modCount + 1}" />
				</c:if>
			</c:forEach>

			<t:showErrors path="form.*" showFieldErrors="true" />
			<cwms-newjobitem-errors></cwms-newjobitem-errors>
			<c:if test="${modelsInBasket}">
										
            <cwms-newjobitem-models 
					coid="${job.con.sub.comp.coid}"
					subdivId="${sessionScope.allocatedSubdiv.key}"
					serviceTypes="${cwms:objectToJson(serviceTypes)}"
					addresses="${cwms:objectToJson(addresses)}"
					contacts="${cwms:objectToJson(contacts)}"
					rangeUnits="${cwms:objectToJson(rangeUnits)}"
					calFrequencyUnits="${cwms:objectToJson(units)}"
					jobId="${job.jobid}"
					jobNo="${job.jobno}"
					autoPrintLabels="${currentContact.userPreferences.autoPrintLabel}"
			></cwms-newjobitem-models>
			</c:if>
			

			<c:set var="instsInBasket" value="false" />
			<c:set var="instsCount" value="0" />
			<c:forEach var="bi" items="${basketItems}">
				<c:if test="${not empty bi.inst}">
					<c:set var="instsInBasket" value="true" />
					<c:set var="instsCount" value="${instsCount + 1}" />
				</c:if>
			</c:forEach>
		
            <cwms-newjobitem-basket basketItems="${cwms:objectToJson(newBasketItems)}"
					subdivId="${sessionScope.allocatedSubdiv.key}"
					serviceTypes="${cwms:objectToJson(serviceTypes)}"
					jobId="${job.jobid}""
					jobNo="${job.jobno}"
			></cwms-newjobitem-basket>
			
			<!-- Placeholders for deleted items -->
			<c:forEach var="bi" items="${basketItems}" varStatus="loopStatus">
				<c:if test="${empty bi.inst && empty bi.model}">
					<input type="hidden" name="instDeletes[${loopStatus.index}]"
						value="true" />
				</c:if>
			</c:forEach>
			
			<div class="center">
			
				<h2></h2>
				<form:hidden path="contractReviewItems"/>
				<input type="submit"
					value="<spring:message code="addnewitemstojob.additems"/>"
					/>
			
			</div>
			
		</form:form>
	</jsp:body>
</t:crocodileTemplate>