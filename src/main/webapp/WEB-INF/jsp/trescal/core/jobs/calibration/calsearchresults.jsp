<%-- File name: /trescal/core/jobs/calibration/calsearchresults.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="calsearchresults.headtext"/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/jobs/calibration/CalSearchResults.js'></script>
    </jsp:attribute>
    <jsp:body>
        <!-- infobox contains all search form elements and is styled with nifty corners -->
		<div class="infobox">						
			<c:set var="results" value="${form.rs.results}"/>	
			
			<form:form action="" modelAttribute="form" name="searchform" id="searchform" method="post">
		
				<form:hidden path="resultsPerPage"/>
				<form:hidden path="pageNo"/>
				<form:hidden path="statusId"/>
				<form:hidden path="calTypeId"/>
				<form:hidden path="procedureId"/>
				<form:hidden path="calProId"/>
				<form:hidden path="deptId"/>
				<form:hidden path="startDate1"/>
				<form:hidden path="startDate2"/>
				<form:hidden path="startDateBetween"/>
				<form:hidden path="personid"/>
				<form:hidden path="barcode"/>
				<form:hidden path="standardBarcode"/>
				<form:hidden path="jobNo"/>
				
				<t:showResultsPagination rs="${form.rs}" pageNoId="" />	
												
				<table class="default4">
					<thead>
						<tr>
							<td colspan="9"><spring:message code="calsearchresults.tablematchcal"/> (${results.size()})</td>
						</tr>
						<tr>
							<th><spring:message code="calsearchresults.starttime"/></th>
							<th><spring:message code="calsearch.startby"/></th>
							<th><spring:message code="calsearch.casltype"/></th>
							<th><spring:message code="calsearchresults.process"/></th>
							<th><spring:message code="calsearchresults.standards"/></th>
							<th><spring:message code="calsearch.capability"/></th>
							<th><spring:message code="instmodelname"/></th>
							<th><spring:message code="calsearchresults.jobitem"/></th>
							<th><spring:message code="calsearch.status"/></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="9">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody>
						<c:choose>
                            <c:when test="${results.size() > 0}">                                     
		                        <c:forEach var="cal" items="${results}">
									<tr>
										<td><fmt:formatDate value="${cal.startTime}" dateStyle="medium" type="both" timeStyle="short" /></td>
										<td>${cal.contactName}</td>
										<td>${cal.serviceType}</td>
										<td>${cal.process}</td>
										<td>${cal.standardsUsageSize}</td>
										<td>${cal.capabilityReference}</td>
										<td>${cal.instModel}</td>
										<td>
											<a class="mainlink" href="viewjob.htm?jobid=${cal.jobId}">${cal.jobNo}</a> - <a class="mainlink" href="jicalibration.htm?jobitemid=${cal.jobItemId}">Item ${cal.jobItemNo}</a><br/>
										</td>
										<td>${cal.status}</td>
									</tr>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<tr>
									<td colspan="9" class="text-center bold"><spring:message code="calsearchresults.nocal"/></td>
								</tr>
							</c:otherwise>
						</c:choose> 
					</tbody>
				</table>
			 	<t:showResultsPagination rs="${form.rs}" pageNoId="" />	
			</form:form>

		</div>
		<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>