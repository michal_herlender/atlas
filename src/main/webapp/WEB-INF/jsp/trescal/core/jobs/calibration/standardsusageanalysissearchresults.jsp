<%-- File name: /trescal/core/jobs/calibration/standardsusageanalysissearchresults.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="standardsusage.results.search"/></span>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox">
			<form:form method="post" action="" modelAttribute="form">
				<form:hidden path="instId" />
				<form:hidden path="certno" />
				<form:hidden path="status" />
				<form:hidden path="startDate" />
				<form:hidden path="finishDate" />
				<form:hidden path="pageNo" />
				<form:hidden path="resultsPerPage" />
				<t:showResultsPagination rs="${form.rs}" pageNoId="" />
				<table class="default2" summary="This table lists all calibrations performed">
					<thead>
						<tr>
							<td colspan="10"><spring:message code="tpcosts.searchresults"/> (${form.rs.results.size()})
								<spring:message code="web.export.excel" var="submitText"/>  
								<input type="submit" name="export" value="${submitText}" class="float-right">
							</td>
						</tr>
						<tr>
							<th scope="col"><spring:message code="barcode"/></th>
							<th scope="col"><spring:message code="certnumber"/></th>
							<th	scope="col" style="text-align: center"><spring:message code="status"/></th>
							<th	scope="col" style="text-align: center"><spring:message code="calsearch.startdate"/></th>    
							<th	scope="col" style="text-align: center"><spring:message code="bpo.enddate"/></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="10">&nbsp;</td>
						</tr>
					</tfoot>
					<body>
						<c:forEach var="dto" items="${form.rs.results}" varStatus="loop">
							<spring:url value="viewstandardsusageanalysis.htm" var="standardsusageanalysisUrl" >
								<spring:param name="id" value="${dto.standardsUsageAnalysisId}"/>
							</spring:url>
							<tr onclick="window.location.href='${standardsusageanalysisUrl}'" onmouseover=" $j(this).addClass('hoverrow'); " onmouseout=" if (this.className = 'hoverrow') $j(this).removeClass('hoverrow');">
								<td><a href="viewinstrument.htm?plantid=${dto.instId}" class="mainlink">${dto.instId}</a></td>
								<td><a href="viewcertificate.htm?certid=${dto.certid}" class="mainlink">${dto.certno}</a></td>
								<td class="center">${dto.status.getStatus()}</td>
								<td class="center"><fmt:formatDate value="${dto.startDate}" type="date" dateStyle="SHORT"/></td>
								<td class="center"><fmt:formatDate value="${dto.finishDate}" type="date" dateStyle="SHORT"/></td>
							</tr>
						</c:forEach>
					
					</body>
				
				</table>
				<table class="default tlast" summary="">
					<tfoot>
						<tr>
							<td>
								<input type="submit" name="export" value="${submitText}" class="float-right">
							</td>
							
						</tr>
					</tfoot>
				</table>
				<t:showResultsPagination rs="${form.rs}" pageNoId="" />
			</form:form>
		</div>						
	</jsp:body>
</t:crocodileTemplate>