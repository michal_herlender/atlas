<%-- File name: /trescal/core/jobs/job/jobsearch.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<t:crocodileTemplate>
	<jsp:attribute name="scriptPart">
	<script type='module'
			src='script/trescal/core/jobs/calibration/VdiCalibrationsImport.js'></script>
	</jsp:attribute>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message
				code="importcalibrations.title" /></span>
	</jsp:attribute>
	<jsp:body>
	<h3>Import calibration from vdi</h3>
	<cwms-vdi-calibrations-import caption="Import vdi calibrations"></cwms-vdi-calibrations-import>
	</jsp:body>
</t:crocodileTemplate>