<%-- File name: /trescal/core/jobs/job/updateallitems.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="updateallitems.updallitems" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
		<script type='module' src='script/components/search-plugins/cwms-capability-search/cwms-capability-search.js'></script>
        <script type='text/javascript' src='script/trescal/core/jobs/job/UpdateAllItems.js'></script>
        <script type='module' src='script/components/search-plugins/cwms-modelcode-search/cwms-modelcode-search.js'></script>

<script type="text/javascript">
	
	var threadArray = new Array();
	<c:forEach var="thread" items="${threadtypes}">
		threadArray.push({id: ${thread.id}, type: '${thread.type}'});
		</c:forEach>
	
	function toggleCalTypeSelect(checkbox)
	{
		if($j(checkbox).is(':checked'))
		{
			$j('#calTypeSelect').attr('disabled', '');
		}
		else
		{
			$j('#calTypeSelect').attr('disabled', 'disabled');
		}
	}
	
</script>
    </jsp:attribute>
    <jsp:body>
        <form:form action="" method="post" modelAttribute="form">
		
		<!-- infobox div containing form elements for adding model job items -->
		<div class="infobox" id="updateallitems">
		
			<div class="float-right">
				<spring:message code="updateallitems.string1" />
				
				<input type="checkbox" path="updateCalType" value="true" onclick=" toggleCalTypeSelect(this); " />
				<input type="hidden" path="_updateCalType" value="false" />
				
				<form:select id="calTypeSelect" path="calTypeId" disabled="disabled">
					<c:forEach var="ct" items="${caltypes}">
						<option value="${ct.calTypeId}" <c:if test="${form.job.defaultCalType.calTypeId == ct.calTypeId}"> selected="selected" </c:if>>
				 			<t:showTranslationOrDefault translations="${ct.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/>
				 		</option>
					</c:forEach>
				</form:select>
				
			</div>
			<div class="clear"></div>
			
			<c:set var="rangetabcount" value="1" />
			
			<table class="default4 updateAllItems" summary="<spring:message code="updateallitems.string2" />">
				<thead>
					<tr>
						<th><spring:message code="updateallitems.updalljobitems" /></th>
					</tr>
				</thead>									
			</table>
		
			<c:forEach var="ji" items="${form.job.items}">
											
				<table class="default4 updateAllItems" summary="<spring:message code="updateallitems.string2" />">
				
					<tbody>										
						
						<tr>
							<td colspan="2" class="larger-text">
								${ji.itemNo}. ${ji.inst.definitiveInstrument}
							</td>
							<td>
								<span class="bold"><spring:message code="serialno" />:</span> <input type="text" name="instSerialNos" value="${ji.inst.serialno}" />
							</td>
							<td>
								<span class="bold"><spring:message code="plantno" />:</span> <input type="text" name="instPlantNos" value="${ji.inst.plantno}" />
							</td>
						</tr>
						<tr>
							<td class="proc">
								<span class="bold"><spring:message code="jobresults.capability" />:</span><br/>
								<div class="float-left">
								<cwms-capability-search-form 
									name="itemProcIds"
									procid="${ji.capability.id}" 
									defaultvalue="${ji.capability.reference}">
								</cwms-capability-search-form>
								</div>
							</td>
							<td class="workinst">
								<span class="bold"><spring:message code="updateallitems.workinstr" />:</span><br/>
								<div class="float-left">
									<cwms-modelcode-search modelid="${ji.inst.model.modelid}"  name="itemWorkInstIds" defaultvalue="${ji.workInstruction.title}" value="${ji.workInstruction.id}" ></cwms-modelcode-search>
								</div>
							</td>
							<td class="thread">
								<span class="bold"><spring:message code="updateallitems.threadsize" />:</span><br/>
								<div class="float-left">														
									<div class="threadSearchJQPlugin">
										<input type="hidden" name="field" value="instThreadIds" />
										<input type="hidden" name="selectfield" value="threadTypeForPlugin" />
										<input type="hidden" name="copyOption" value="true" />
										<input type="hidden" name="threadTypeId" value="${ji.inst.thread.type.id}" />
										<c:if test="${not empty ji.inst.thread}">
											<input type="hidden" name="defaultThreadId" value="${ji.inst.thread.id}" />
											<input type="hidden" name="defaultThreadText" value="${ji.inst.thread.size}" />
										</c:if>	
									</div>
								</div>
							</td>
							<td class="range">
								<span class="bold"><spring:message code="updateallitems.rangesize" />:</span><br/>
								<div class="float-left">
									<c:choose>
										<c:when test="${ji.inst.ranges.size() > 0}">
											Instrument already has size/range:<br/>
											<c:forEach var="range" items="${ji.inst.ranges}">
												${range}<br/>
											</c:forEach>
										</c:when>
										<c:when test="${ji.inst.model.ranges.size() > 0}">
											Model already has size/range:<br/>
											<c:forEach var="range" items="${ji.inst.model.ranges}">
												${range}<br/>
											</c:forEach>
										</c:when>
										<c:otherwise>
											<input type="text" size="4" name="instStartRanges" tabindex="${rangetabcount}" />
											<c:set var="rangetabcount" value="${rangetabcount + 1}" />
											-&nbsp;
											<input type="text" size="4" name="instEndRanges" tabindex="${rangetabcount}" />
											<c:set var="rangetabcount" value="${rangetabcount + 1}" />
											<select name="instUomIds" tabindex="${rangetabcount}">
												<c:forEach var="uom" items="${uoms}">
													<option value="${uom.id}">${uom.formattedSymbol} (<t:showTranslationOrDefault translations="${uom.nameTranslation}" defaultLocale="${defaultlocale}"/>)</option>
												</c:forEach>
											</select>
											<c:set var="rangetabcount" value="${rangetabcount + 1}" />
											<a href="#" class="imagelink" onclick=" copyRangeSelection('down', $j(this).siblings('input:first'), $j(this).siblings('input:last'), $j(this).siblings('select')); return false; ">
												<img src="img/icons/arrow_down.png" width="10" height="16" alt="<spring:message code="updateallitems.copyseldown" />" title="<spring:message code="updateallitems.copyseldown" />" />
											</a>
											<a href="#" class="imagelink" onclick=" copyRangeSelection('up', $j(this).siblings('input:first'), $j(this).siblings('input:last'), $j(this).siblings('select')); return false; ">
												<img src="img/icons/arrow_up.png" width="10" height="16" alt="<spring:message code="updateallitems.copyselup" />" title="<spring:message code="updateallitems.copyselup" />" />
											</a>
										</c:otherwise>
									</c:choose>	
								</div>
							</td>
						</tr>
				
					</tbody>
				
				</table>
				
			</c:forEach>
			
			<div class="text-center marg-top">
				<input type="submit" value="<spring:message code="addjobviawebservice.submit" />" />
				<input type="button" value="<spring:message code="updateallitems.cancel" />" onclick=" window.location.href = 'viewjob.htm?jobid=${form.job.jobid}' "/>
			</div>
											
		</div>
		<!-- end of infobox div -->
	
		</form:form>
	</jsp:body>
</t:crocodileTemplate>