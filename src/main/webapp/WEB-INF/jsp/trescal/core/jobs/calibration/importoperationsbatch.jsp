<%-- File name: /trescal/core/jobs/calibration/importoperationsbatch.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<t:crocodileTemplate>
	<jsp:attribute name="scriptPart">
		<script type='module' src='script/thirdparty/@polymer/iron-form/iron-form.js'></script>
		<script type='module' src='script/thirdparty/@vaadin/vaadin-combo-box/vaadin-combo-box.js'></script>
		<script type='module' src='script/thirdparty/@vaadin/vaadin-progress-bar/vaadin-progress-bar.js'></script>
		<script type='module' src='script/thirdparty/@vaadin/vaadin-button/vaadin-button.js'></script>
		<script type='module' src='script/thirdparty/@vaadin/vaadin-checkbox/vaadin-checkbox.js'></script>
		<script type='module' src='script/thirdparty/@vaadin/vaadin-dialog/vaadin-dialog.js'></script>
		<script type='module' src='script/thirdparty/@vaadin/vaadin-select/vaadin-select.js'></script>
		<script type='module' src='script/components/cwms-translate/cwms-translate.js'></script>
		<script type='text/javascript' src='script/trescal/core/jobs/calibration/ImportOperationsbatch.js'></script>
	</jsp:attribute>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message
				code="menu.jobs.importoperationsbatch" /></span>
	</jsp:attribute>
	<jsp:body>
	
		<vaadin-dialog id="loadingDialog" no-close-on-esc no-close-on-outside-click>
			<template>
				<vaadin-progress-bar id="rowsProgressBar" value="0.1"></vaadin-progress-bar>
			</template>
		</vaadin-dialog>
		<vaadin-dialog id="submitDialog" no-close-on-esc no-close-on-outside-click></vaadin-dialog>

		<div class="infobox">
			<dom-module id="short-field" theme-for="vaadin-text-field" >
			  <template>
			    <style>
			      [part="value"] {
			        min-height : 25px;
			      }
			    </style>
			  </template>
			</dom-module>
			<dom-module id="short-field-select" theme-for="vaadin-combo-box-item" >
			  <template>
			    <style>
			      [part="content"] {
			        font-size : 11px;
			      }
			    </style>
			  </template>
			</dom-module>
			<iron-form id="ironFormId">
			<form:form modelAttribute="form" action="importoperationsbatch.json">
				<fieldset>
					<legend>
						<spring:message code="menu.jobs.importoperationsbatch" />
					</legend>
					<!-- displays content in left column -->
					<div class="displaycolumn-60">
						<ol>											
							<li>
								<label><spring:message code="subdiv" />:</label>
								<div id="cascadeSearchPlugin" style="z-index:50">
									<input type="hidden" id="cascadeRules" value="subdiv" />
									<input type="hidden" id="compCoroles" value="client" />
									<input type="hidden" id="subdivCallBackFunction"
										value="companySelected" />
								</div>
								<span class="attention" style="margin-left: 50px;"><form:errors
										path="subdivid" /></span>
								<!-- clear floats and restore page flow -->
								<div class="clear"></div>
							</li>
							<li>
								<label><spring:message code="job" /> :</label>
								<vaadin-combo-box id="jobComboBox" name="jobId" clear-button-visible required="true"
									theme="small" class="combo-size" ></vaadin-combo-box>
							</li>
							<li>
								<label><spring:message code="addjob.recordtype.file.exchangeformat" /> :</label>
								<vaadin-combo-box id="exchangeformat" name="exchangeFormatId" clear-button-visible required="true"
									theme="small" class="combo-size" ></vaadin-combo-box>
							</li>
							<li>
								<label><spring:message code="importedcalibrations.automaticcontractreview" /> :</label>
								<vaadin-checkbox name="automaticContractReview" checked></vaadin-checkbox>
							</li>
							<li>
								<label><spring:message code="importedcalibrations.autocreatejobitems" /> :</label>
								<vaadin-checkbox name="autoCreateJobitems" checked></vaadin-checkbox>
							</li>
							<li>
								<label><spring:message
										code="importcalibrations.autosubmitpolicy" /> :</label>						
								<vaadin-select name="autoSubmitPolicy" required="true" theme="small"
									 class="combo-size" value="DO_NOT_SUBMIT" >
									<template>
										<vaadin-list-box>
											<c:forEach var="i" items="${ autoSubmitPolicies }">
												<vaadin-item style="font-size : 11px;" value="${ i.key }" >${ i.value }</vaadin-item>
											</c:forEach>
										</vaadin-list-box>
									</template>
								</vaadin-select>
							</li>
							<li>
								<label><spring:message code="file" />:</label>	
								<form:input path="file" type="file" id="fileInput" required="true"
									accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel, zip, application/zip" />									
								<span class="attention"><form:errors path="file" /></span>
								<!-- clear floats and restore page flow -->
								<div class="clear"></div>
							</li>
							
							<li>
								<label>&nbsp;</label>
								<input type="button" onclick="submitIronForm();return false;" value="<spring:message code='submit' />" />
							</li>
						</ol>
					</div>
				</fieldset>
			</form:form>
			</iron-form>
		</div>
	</jsp:body>
</t:crocodileTemplate>