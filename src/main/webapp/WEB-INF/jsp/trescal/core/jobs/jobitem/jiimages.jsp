<%-- File name: /trescal/core/jobs/jobitem/jiimages.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="costs" tagdir="/WEB-INF/tags/costs"%>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs"%>
<%@ taglib prefix="addr" tagdir="/WEB-INF/tags/address"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<jobs:jobItemTemplate>
	<jsp:attribute name="subScript">
        <script type='text/javascript' src='script/trescal/core/jobs/jobitem/JIImages.js'></script>
		<script type='text/javascript'>
			// grab scroll to parameter passed when using job item key navigation
			var scrollto = '${request.getParameter("scrollto")}';
		</script>
   	</jsp:attribute>
	<jsp:body>
 		<div class="infobox">
		
 		<div class="gallerifficContent">
		
			<div id="thumbs-adv" class="navigation">
				<ul class="thumbs noscript">
					<c:choose>
						<c:when test="${form.ji.images.size() < 1}">
							<li class="thumbitem" id="emptyImage">
								<a class="thumb" href="img/noimage.png" title="">
										<img src="img/noimage.png" alt="" height="60" width="80" />
								</a>
							    <div class="caption">
									<div class="download"></div>
									<div class="image-title">
										<spring:message code="jiimages.addnewimage" />
										<a href="#"
											onclick=" addImageToEntity(${form.ji.jobItemId}, 'JOBITEM'); return false; "
											title="">
											<img src="img/icons/picture_add.png" width="16" height="16"
												alt="<spring:message code="jiimages.addimgs"/>"
												title="<spring:message code="jiimages.addimgs"/>" />
										</a>
									</div>
									<div class="image-desc"></div>
									<div class="image-desc"></div>
								</div>
							</li>
						</c:when>
						<c:otherwise>
							<c:forEach var="image" items="${form.ji.images}" varStatus="imageStatus">
							   <li class="thumbitem" id="systemimg${image.id}">
								<a class="thumb" href="displayimage?id=${image.id}" title="">
									<img src="displaythumbnail?id=${image.id}" alt="" />
									<!-- TODO convert to macro when we convert this page to JSP -->
								    <c:choose>
								       <c:when test="${image.tags.size() > 0}">
									   <c:forEach var="tag" items="${image.tags}">
										<c:choose>
								           <c:when test="${imageStatus.count == 1}">
											  ${tag.tag}
											</c:when>
								            <c:otherwise>
											  , ${tag.tag}
										    </c:otherwise>
										</c:choose>
									   </c:forEach>
									   </c:when>
								       <c:otherwise>
										<spring:message code="jiimages.notagsassigned" />
									   </c:otherwise>
								    </c:choose>
								</a>
								<div class="caption">
								<div class="download">
									<a href="#" onclick=" addImageToEntity(${form.ji.jobItemId}, 'JOBITEM'); return false; " title="">
										<img src="img/icons/picture_add.png" width="16" height="16"
										alt="<spring:message code='jiimages.addimgs'/>"
										title="<spring:message code='jiimages.addimgs'/>" />
									</a>
									&nbsp;&nbsp;&nbsp;
									<a href="displayimage?id=${image.id}"><spring:message code="jiimages.downlorig" /></a>
									&nbsp;&nbsp;&nbsp;
									<a href="#" onclick=" deleteGalleryImage(${image.id}, ${form.ji.jobItemId}, 'JOBITEM'); return false; ">
										<img src="img/icons/redcross.png" width="16" height="16"
										alt="<spring:message code='jiimages.delthisimage'/>"
										title="<spring:message code='jiimages.delthisimage'/>" />
									</a>
								</div> 
								<div class="image-title"><spring:message code="jiimages.image" />&nbsp;${imageStatus.count}</div>
								<div class="image-desc">
									<strong>Description: &nbsp;</strong>
									<c:choose>
										<c:when test="${not empty image.description}">
											${image.description}
										</c:when>
										<c:otherwise>
											<spring:message code="nocomment" />
										</c:otherwise>
							        </c:choose>
								</div>
								 </br>
								 <div class="image-tmupld">
							        <strong><spring:message code="jiimages.timeuploaded" /> :</strong>
							        <fmt:formatDate value="${ image.addedOn }" type="both" dateStyle="MEDIUM" timeStyle="SHORT"/>
								</div>
								<div class="image-desc">
									<strong><spring:message code="jiimages.taggedas" />: &nbsp;</strong>
									<c:choose>
										<c:when test="${image.tags.size() > 0}"> 
											<c:forEach var="tag" items="${image.tags}">
												<c:choose>
													<c:when test="${imageStatus.count == 1}">
													${tag.tag}
													</c:when>
												<c:otherwise>, ${tag.tag}</c:otherwise>
												</c:choose>
											</c:forEach>
										</c:when>
										<c:otherwise>
											<spring:message code="jiimages.notagsassigned" />
										</c:otherwise>
									</c:choose>
								</div>
								</div>
								</li>
						    </c:forEach>
						</c:otherwise>
					</c:choose>
					</ul>
					</div>
							
					<div id="gallery-adv" class="content">
					<div id="controls-adv" class="controls"></div>
					<div id="loading-adv" class="loader"></div>
					<div id="slideshow-adv" class="slideshow"></div>
					<div id="caption-adv" class="embox"></div>
				</div>
		
					<!-- clear floats and restore page flow -->
				<div class="clear"></div>
			</div>
		</div>
	<!-- end of infobox div -->

    </jsp:body>
</jobs:jobItemTemplate>