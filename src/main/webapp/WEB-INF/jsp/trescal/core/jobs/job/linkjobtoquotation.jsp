<%-- File name: /trescal/core/jobs/job/linkjobtoquotation.jsp --%>
<tr id="quote${jobQuoteLink.quoteLinkId}">
	<td>
		<a href="viewquotation.htm?id=${jobQuoteLink.quoteId}" title="core.jobs:job.viewThisQuote" class="mainlink">
			${jobQuoteLink.quoteQno} version ${jobQuoteLink.quoteVersion}
		</a>
	</td>
	<td>${jobQuoteLink.quoteCompany}</td>
	<td>${jobQuoteLink.quoteContact}</td>
	<td><fmt:formatDate value="${jobQuoteLink.quoteLinkedOn}" dateStyle="medium" type="date" /></td>
	<td>${jobQuoteLink.quoteLinkedBy}</td>
	<td class="center">
		<a href="" onclick="event.preventDefault(); removeJobQuoteLink(${jobQuoteLink.quoteLinkId});">
			<img src="img/icons/delete.png" width="16" height="16" alt="core.jobs:job.deleteLinkToQuotation" title="core.jobs:job.deleteLinkToQuotation"/>
		</a>
	</td>
</tr>