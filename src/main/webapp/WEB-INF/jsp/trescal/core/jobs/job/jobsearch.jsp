<%-- File name: /trescal/core/jobs/job/jobsearch.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:crocodileTemplate>
	<jsp:attribute name="scriptPart">
		<script type='module' src='script/components/search-plugins/cwms-capability-search/cwms-capability-search.js'></script>
		<script type='text/javascript' src='script/trescal/core/jobs/job/JobSearch.js'></script>
	</jsp:attribute>
	<jsp:attribute name="header"><span class="headtext"><spring:message code="jobsearch.headtext" /></span></jsp:attribute>
	<jsp:body>
		<!-- infobox contains all form elements relating to searching for a job and is styled with nifty corners -->				
		<div class="infobox">
			<form:form method="post" action="" id="jobsearchform" modelAttribute="jobsearchform">
				<!-- error section displayed when form submission fails -->
				<t:showErrors path="jobsearchform.*" showFieldErrors="true" />
				<!-- end error section -->
		
				<fieldset>
					<legend><spring:message code="jobsearch.headtext" /></legend>									
					<div class="displaycolumn-100">
						<ol>
							<li>
								<label><spring:message code="company" />:</label>
								<div id="cascadeSearchPlugin">
									<input type="hidden" id="cascadeRules" value="subdiv,contact" />
									<input type="hidden" id="compCoroles" value="client,business" />
									<input type="hidden" id="autoselect" value="false" />
									<input type="hidden" id="compindex" value="1" />
								</div>
								<!-- clear floats and restore page flow -->
								<div class="clear"></div>
							</li>
						</ol>
					</div>
					<!-- displays content in column 69% wide (left) -->
					<div class="displaycolumn-60">
						<ol>
							<li>
								<label><spring:message code="serialno" />:</label>			
								<form:input path="serialno" tabindex="5" />
							</li>
							<li>
								<label><spring:message code="plantno" />:</label>			
								<form:input path="plantno" tabindex="6" />
							</li>
							<li id="descrip">
		                        <label><spring:message code="sub-family" />:</label>
		                        <!-- float div left -->
		                        <div class="float-left">
		                            <form:input path="desc" tabindex="7" id="desc" />
		                            <form:hidden path="descid" i="descid" />
		                        </div>
		                        <!-- clear floats and restore pageflow -->
		                        <div class="clear-0"></div>
		                    </li>
							<li id="manufacturer">
								<label><spring:message code="manufacturer" />:</label>
								<!-- float div left -->
								<div class="float-left">
									<form:input path="mfr" id="mfr" tabindex="8" />
		                            <form:hidden path="mfrid" id="mfrid" />
								</div>
								<!-- clear floats and restore pageflow -->
								<div class="clear-0"></div>
							</li>
							<li>
								<label><spring:message code="model" />:</label>
								<form:input path="model" tabindex="9" />											
							</li>
							<li>
								<label for="bookedInDate1"><spring:message code="jobsearch.bookindate"  />:</label>
								
								<form:select path="bookedInDateBetween" id="bookedInDateBetween" tabindex="10" onchange="event.preventDefault(); changeDateOption(this.value, 'bookedInDateSpan'); ">
									<form:option value="false"><spring:message code="jobsearch.on" /></form:option>
									<form:option value="true"><spring:message code="jobsearch.between" /></form:option>
								</form:select>
								
								<form:input path="bookedInDate1" id="bookedInDate1"   tabindex="11" type="date" />
								
								<c:set var="visual" value="hid" />
								<c:if test="${jobsearchform.bookedInDateBetween}">
									<c:set var="visual" value="vis" />
								</c:if>
								
								<span id="bookedInDateSpan" class="${visual}">
									<spring:message code="and" />&nbsp;
									<form:input path="bookedInDate2"   tabindex="12"  type="date"/>
								</span>
							</li>
							<li>
								<label><spring:message code="jobsearch.completedjob" />:</label>
								
								<form:select path="completedDateBetween" id="completedDateBetween" tabindex="13" onchange="event.preventDefault(); changeDateOption(this.value, 'completedDateSpan'); ">
									<form:option value="false"><spring:message code="jobsearch.on" /></form:option>
									<form:option value="true"><spring:message code="jobsearch.between" /></form:option>
								</form:select>
								
								<form:input path="completedDate1" id="completedDate1"   tabindex="14" type="date" />
								
								<c:set var="visual" value="hid" />
								<c:if test="${jobsearchform.completedDateBetween}">
									<c:set var="visual" value="vis" />
								</c:if>
								
								<span id="completedDateSpan" class="${visual}">
									<spring:message code="and" />&nbsp;
									<form:input path="completedDate2"  tabindex="15"  type="date"/>
								</span>
							</li>
							<li>
								<label for="submit">&nbsp;</label>
								<input type="submit" name="Submit" id="submit" class="float-left" value="<spring:message code='addjobviawebservice.submit' />" tabindex="24" />
								<input type="button" value="<spring:message code='jobsearch.clrform' />" id="clear" class="float-right" onclick=" clearForm(); " tabindex="0" />
								<!-- clear floats and restore page flow -->
								<div class="clear-0"></div>
							</li>
						</ol>
					</div>
					<!-- end of left column -->
					
					<!-- displays content in column 39% wide (right) -->
					<div class="displaycolumn-40">
						<ol>
							<li>
								<label><spring:message code="croverview.jobno" />:</label>
								<form:input path="jobno" tabindex="16" />
							</li>					
							<li>
								<label><spring:message code="jobresults.barcode" />:</label>
								<form:input path="plantid" tabindex="17" />
							</li>
							<li>
								<label><spring:message code="addjob.clientref" />:</label>
								<form:input path="clientRef" tabindex="18" />
							</li>
							<li>
								<label><spring:message code="jobresults.purchord" />:</label>
								<form:input path="purOrder" tabindex="19" />
							</li>
							<li>
								<label for="procId"><spring:message code="jobresults.capability" />:</label>
								<cwms-capability-search-form 
								tabIndex="20"
									name="procId"
									procid="${jobsearchform.procId}" 
									defaultvalue="${procText}">
								</cwms-capability-search-form>
							</li>
							<li>
								<label><spring:message code="addjob.jobtype" />:</label>	
								<form:select path="jobType" tabindex="21" items="${jobTypes}" itemLabel="value" itemValue="key" />
							</li>											
							<li>
								<label><spring:message code="jobsearch.alljobonly" />:</label>
								<span>
									<form:checkbox cssClass="checkbox" path="active" tabindex="22" />
								</span>
							</li>
							<li>
								<label><spring:message code="jobsearch.highlres" />:</label>
								<span>
									<form:checkbox path="highlight" cssClass="checkbox" checked="true" tabindex="23" />
								</span>
							</li>
							<li>
								<label><spring:message code="jobsearch.deljobs" />:</label>
								<span>
									<a href="deletedcomponent.htm" class="mainlink"><spring:message code="jobsearch.searchdeljobs" /></a>
								</span>
							</li>
						</ol>
					</div>
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
					<!-- end of right column -->
				</fieldset>
			</form:form>
		</div>
		<!-- end of infobox div -->
	</jsp:body>
</t:crocodileTemplate>