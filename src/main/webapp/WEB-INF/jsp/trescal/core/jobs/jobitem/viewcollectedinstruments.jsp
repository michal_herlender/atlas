<%-- File name: /trescal/core/jobs/jobitem/ViewCollectedInstruments.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>


<t:crocodileTemplate>
	<jsp:attribute name="header">
        <span class="headtext"><spring:message code='viewcollectedinstruments.headtext' /></span>
    </jsp:attribute>
	<jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/jobs/jobitem/ViewCollectedInstruments.js'></script>
    </jsp:attribute>
	<jsp:body>
        <div id="collectedinstruments" class="infobox">
        <c:choose>
		  <c:when test="${not empty form.collectedInsts}">
		     <form:form method="post" action="" modelAttribute="form">
		  		<fieldset>
					<ol>
						<%-- Starts with ID of 0, so first element is treated as a new company --%>
						<c:forEach var="ci" items="${form.collectedInsts}" >
					    	<c:if test="${ci.inst.con.sub.comp.coid != currentCompId}" >
						    	<c:set var="currentCompId" value="${ci.inst.con.sub.comp.coid}" />
						    	<li>
								 	${ci.inst.con.sub.comp.coname} --
									<input type="button" class="newjob" onclick="event.preventDefault(); newJob(this, ${currentCompId});" value="<spring:message code='viewcollectedinstruments.string2'/>" /> 
									<input type="button" class="cancel hid" onclick="event.preventDefault(); cancel(this);" value="<spring:message code='cancel'/>" /> 
								</li>
							 </c:if>
							<li id="${ci.id}">
								${ci.inst.plantid}<c:out value=" - "/><cwms:showmodel instrumentmodel="${ci.inst.model}"/>								
							</li>
						</c:forEach>
					</ol>
				</fieldset>
				<div class="clear">&nbsp;</div>
				<input type="hidden" id="coid" name="coid" value="" />
				<input type="submit" value="<spring:message code='submit'/>" />
				</form:form>
			</c:when>
			<c:otherwise>
				<p><spring:message code='viewcollectedinstruments.string1'/>.</p>
			</c:otherwise>
		</c:choose>
		</div>
	</jsp:body>
</t:crocodileTemplate>