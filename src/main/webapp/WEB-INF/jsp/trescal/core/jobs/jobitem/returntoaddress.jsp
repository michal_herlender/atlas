<%-- File name: /trescal/core/jobs/jobitem/returntoaddress.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="returntoaddress.headtext" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/jobs/jobitem/ReturnToAddress.js'></script>
    </jsp:attribute>
    <jsp:body>
    	<t:showErrors path="command.*" showFieldErrors="true" />
		<div class="infobox">	
			<form:form modelAttribute="command" action="" method="post" onsubmit="$j(this).find(':input[type=submit]').prop('disabled', true);">
				<fieldset>
				    <legend>
					    <links:jobnoLinkDWRInfo rowcount="0" jobno="${command.job.jobno}" jobid="${command.job.jobid}" copy="true"/>
				    </legend>
					<ol>
						<li>
							<label>
								<spring:message code="address"/>:<br />								
							</label>
							<div id="cascadeSearchPlugin">
								<input type="hidden" id="compCoroles" value="client" />
								<input type="hidden" id="cascadeRules" value="subdiv,address,contact" />
								<input type="hidden" id="addressType" value="Delivery" />
	                          
								<c:if test="${not empty command.srcJobitem.returnToAddress}">
									<c:set var="a" value="${command.srcJobitem.returnToAddress}"/>
									<c:set var="c" value="${command.srcJobitem.returnToContact}"/>
									<input type="hidden" id="prefillIds" value="${a.sub.comp.coid},${a.sub.subdivid},${a.addrid},${c.personid}"/>
							    </c:if>
							</div>
							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
						</li>
						<li>
							<label><spring:message code="jobitems"/>:</label>
							<div class="float-left padtop" id="items">											
								<div class="marg-bot">													
									<input type="checkbox" onclick=" selectAllItems(this.checked, 'items'); "/>
									<spring:message code="all"/>
							    </div>
								<c:forEach var="jobitem" items="${command.job.items}">
									<div id="item${jobitem.jobItemId}" style=" height: 26px; ">												
										<form:checkbox path="jobitemids[${jobitem.jobItemId}]" />
											<links:jobitemLinkDWRInfo jobitem="${jobitem}" jicount="false" rowcount="0" />
											<cwms:showmodel instrumentmodel="${jobitem.inst.model}"/>
								 	</div>
								</c:forEach>
							</div>
							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
						</li>
						<li>
							<label>&nbsp;</label>
							<input type="submit" name="save" value="save"/>
						</li>									
					</ol>
				</fieldset>
			</form:form>
		</div>
    </jsp:body>
</t:crocodileTemplate>