<%-- File name: /trescal/core/jobs/calibration/standardsusageanalysissearch.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="standardsusage.headtext"/></span>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox">
			<form:form method="post" action="" modelAttribute="form">
				<form:errors path="*">
       				<div class="warningBox1">
              		  <div class="warningBox2">
              	       		<div class="warningBox3">
                    			<h5 class="center-0 attention"><spring:message code="error.save"/>:</h5>
                    		</div>
                   		</div>
                  	</div>
                </form:errors>
				<fieldset>
						<legend><spring:message code="standardsusage.search"/></legend>
						<div>

							<ol>	
								<li>
									<label><spring:message code="barcode"/></label>
									<form:input path="instId" tabindex="2" size="22"/>
								</li>	
								<li>
									<label><spring:message code="certnumber"/></label>
									<form:input path="certno" tabindex="2" size="22"/>
								</li>
								<li>
									<form:label path="status"><spring:message code="status" />:</form:label>
									<form:select path="status" style=" width: 130px; ">
											<option value="" selected><spring:message code="all"/></option>
											<c:forEach var="ss" items="${form.standardsUsageAnalysisStatus}">
												<option value="${ss}"> ${ss.status} </option>
											</c:forEach>
									</form:select>
								</li>								
								<li>
									<form:label path="startDate"><spring:message code="calsearch.startdate"/>:</form:label>
									<form:input type ="date" path="startDate" tabindex="2" />
									<form:errors path="startDate" class="attention"/>
								</li>	
								<li>
									<form:label path="finishDate"><spring:message code="bpo.enddate"/>:</form:label>
									<form:input type="date" path="finishDate" tabindex="2" />
								</li>	
								<li>
									<label>&nbsp;</label>
									<span>
										<input type="submit" name="search" value="Search" tabindex="6" />
									</span>
								</li>								
							</ol>
						
						</div>
				</fieldset>
			</form:form>
		</div>						
	</jsp:body>
</t:crocodileTemplate>