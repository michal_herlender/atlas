<%-- File name: /trescal/core/jobs/job/editjobinvoicing.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="viewjob.editjobiteminvoicing"/> - ${job.jobno}</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src="script/trescal/core/jobs/job/EditJobInvoicing.js"></script>
	</jsp:attribute>
	<jsp:body>
		<t:showErrors path="command.*" showFieldErrors="true"/>
		<!-- main div which contains all information -->
		<div id="editjobinvoicing">
			<!-- infobox contains all information -->
			<div class="infobox">
				<form:form modelAttribute="command" method="post">
					<fieldset>
						<legend><spring:message code="viewjob.editjobiteminvoicing"/>: <a href="viewjob.htm?jobid=${job.jobid}&loadtab=invoice-tab">${job.jobno}</a></legend>
						<table class="default4" id="editjobiteminvoicingtable">
							<thead>
								<tr>
									<td colspan="7"><spring:message code="jobcost.jobitems"/> (${job.items.size()})</td>
								</tr>
								<tr>
									<td><spring:message code="viewjob.itemno"/></td>
									<td><spring:message code="status"/></td>
									<td><spring:message code="invoice.invoices"/></td>
									<td><spring:message code="invoice.notinvoiced"/></td>
									<td><spring:message code="invoice.notinvoicedreason"/></td>
									<td><spring:message code="invoice.periodicinvoice"/></td>
									<td><spring:message code="comments"/></td>
								</tr>
							</thead>
							<tfoot>
								<tr><td colspan="7"></td></tr>
							</tfoot>
							<tbody>
								<c:forEach var="jobitem" items="${job.items}" varStatus="loopJobItem">
									<c:set var="valueDisabled" value="false" />
									<c:if test="${!command.notInvoiced[jobitem.jobItemId].selected}">
										<c:set var="valueDisabled" value="true" />
									</c:if>
									<tr>
										<td>
											<links:jobitemLinkDWRInfo jobitem="${jobitem}" jicount="true" rowcount="" />
										</td>
										<td>
											<cwms:besttranslation translations="${jobitem.state.translations}" />
										</td>
										<td>
											<c:forEach var="invoiceItem" items="${jobitem.invoiceItems}" varStatus="loopInvoiceItem" >
												<c:if test="${!loopInvoiceItem.first}">,</c:if>
												<a href="viewinvoice.htm?id=${invoiceItem.invoice.id}" class="mainlink">${invoiceItem.invoice.invno}</a>
											</c:forEach>
										</td>
										<td>
											<form:checkbox path="notInvoiced[${jobitem.jobItemId}].selected" value="true" onClick=" enableDisableNotInvoiced(this.checked, '${jobitem.jobItemId}'); " />
										</td>
										<td>
											<form:select id="reason${jobitem.jobItemId}" path="notInvoiced[${jobitem.jobItemId}].reason" itemLabel="message" items="${notInvoicedReasons}" disabled="${valueDisabled}" />
										</td>
										<td>
											<form:select id="periodicInvoiceId${jobitem.jobItemId}" path="notInvoiced[${jobitem.jobItemId}].periodicInvoiceId" itemLabel="value" itemValue="key" items="${periodicInvoices}" disabled="${valueDisabled}" />
										</td>
										<td>
											<form:input id="comments${jobitem.jobItemId}" path="notInvoiced[${jobitem.jobItemId}].comments" disabled="${valueDisabled}" size="50" />
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<table class="default4" id="editjobiteminvoicingtable">
							<thead>
								<tr>
									<td colspan="7"><spring:message code="viewjob.jobservices"/> (${job.expenseItems.size()})</td>
								</tr>
								<tr>
									<td><spring:message code="viewjob.itemno"/></td>
									<td><spring:message code="viewjob.service"/></td>
									<td><spring:message code="invoice.invoices"/></td>
									<td><spring:message code="invoice.notinvoiced"/></td>
									<td><spring:message code="invoice.notinvoicedreason"/></td>
									<td><spring:message code="invoice.periodicinvoice"/></td>
									<td><spring:message code="comments"/></td>
								</tr>
							</thead>
							<tfoot>
								<tr><td colspan="7"></td></tr>
							</tfoot>
							<tbody>
								<c:forEach var="expenseItem" items="${job.expenseItems}" varStatus="loopJobItem">
									<c:if test="${expenseItem.invoiceable}">
										<c:set var="valueDisabled" value="false" />
										<c:if test="${!command.notInvoiced[expenseItem.id].selected}">
											<c:set var="valueDisabled" value="true" />
										</c:if>
										<tr>
											<td>
												${expenseItem.itemNo}
											</td>
											<td>
												<cwms:besttranslation translations="${expenseItem.model.nameTranslations}"/>
											</td>
											<td>
												<c:forEach var="invoiceItem" items="${expenseItem.invoiceItems}" varStatus="loopInvoiceItem" >
													<c:if test="${!loopInvoiceItem.first}">,</c:if>
													<a href="viewinvoice.htm?id=${invoiceItem.invoice.id}" class="mainlink">${invoiceItem.invoice.invno}</a>
												</c:forEach>
											</td>
											<td>
												<form:checkbox path="expensesNotInvoiced[${expenseItem.id}].selected" value="true" onClick=" enableDisableNotInvoiced(this.checked, '${expenseItem.id}'); " />
											</td>
											<td>
												<form:select id="reason${expenseItem.id}" path="expensesNotInvoiced[${expenseItem.id}].reason" itemLabel="message" items="${notInvoicedReasons}" disabled="${valueDisabled}" />
											</td>
											<td>
												<form:select id="periodicInvoiceId${expenseItem.id}" path="expensesNotInvoiced[${expenseItem.id}].periodicInvoiceId" itemLabel="value" itemValue="key" items="${periodicInvoices}" disabled="${valueDisabled}" />
											</td>
											<td>
												<form:input id="comments${expenseItem.id}" path="expensesNotInvoiced[${expenseItem.id}].comments" disabled="${valueDisabled}" size="50" />
											</td>
										</tr>
									</c:if>
								</c:forEach>
							</tbody>
						</table>
						<input type="submit" name="<spring:message code="submit"/>"/>
					</fieldset>	
				</form:form>
			</div>
		</div>
	</jsp:body>
</t:crocodileTemplate>