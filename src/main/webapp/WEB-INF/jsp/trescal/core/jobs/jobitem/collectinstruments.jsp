<%-- File name: /trescal/core/jobs/jobitem/collectinstruments.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code= "collectinstruments.headtext"/> </span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/jobs/jobitem/CollectInstruments.js'></script>
    </jsp:attribute>
    <jsp:body>
        <div id="collectinstruments">

					    <t:showErrors path="form.*" showFieldErrors="true"/>
							
						  <input type="text" name="bc" id="bc" onkeypress=" if (event.keyCode==13){ checkBarcode(this.value); }" /><br/><br/>
								
							<form:form id="collectinstrumentsform" action="" method="post">	
								
							<ol id="instruments"></ol>
							
							<input type="submit" value="<spring:message code ='collectinstruments.markascoll'/>" />
							
							</form:form>
						
		</div>
    </jsp:body>
</t:crocodileTemplate>