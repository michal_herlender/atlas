<%-- File name: /trescal/core/jobs/jobitem/overrideoverlay.jsp--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<div id="changeItemStatusBoxy" class="overflow marg-bot">
	<div id="overrideStatusContent">
		<form:form modelAttribute="form" action="" method="post" class="inline">
			<fieldset>
				<legend><spring:message code="jiactions.overrstatthisitem"/></legend>
				<ol>
					<li>
						<label><spring:message code="jicalloffitem.currstatus"/>:</label>
						<cwms:besttranslation translations="${jobItem.state.translations}"/>
					</li>
					<li>
						<form:label path="overrideStatusId"><spring:message code="jiactions.setstat"/>:</form:label>
						<form:select path="overrideStatusId">
							<c:forEach var="status" items="${allTranslatedStatuses}">
								<option value="${status.key}">${status.value}</option>
							</c:forEach>
						</form:select>
					</li>
					<li>
						<form:label path="overrideRemark" class="twolabel"><spring:message code="jiactions.remark"/>:</form:label>
						<form:textarea path="overrideRemark" class="OVERRIDE" rows="4" cols="80"/>
					</li>
					<li>
						<label><spring:message code="jiactions.foritems"/>:</label>
						<div class="float-left" id="overrideItems">
							<input type="checkbox" onclick="selectAllItems(this.checked, 'overrideItemIds', 'overrideItems');"/>
							<spring:message code="jiactions.all"/><br />
							<c:forEach var="item" items="${jobItem.job.items}">
								<c:choose>
									<c:when test="${item.state.status && !item.state.hold}">
										<c:set var="checked" value=""/>
										<c:choose>
											<c:when test="${item.jobItemId == jobItem.jobItemId || form.overrideItemIds != null && form.overrideItemIds.contains(item.jobItemId)}">
												<form:checkbox path="overrideItemIds"  value="${item.jobItemId}" checked="checked"/>
											</c:when>
											<c:otherwise>
												<form:checkbox path="overrideItemIds"  value="${item.jobItemId}"/>
											</c:otherwise>
										</c:choose>
										${item.itemNo} - <cwms:showmodel instrumentmodel="${item.inst.model}"/> &nbsp;&nbsp;&nbsp;
										(<span style="color: blue;"><cwms:besttranslation translations="${item.state.translations}"/></span>)<br/>
									</c:when>
									<c:when test="${item.state.status && item.state.hold }">
										<span>${item.itemNo} - <spring:message code="jiactions.string1"/></span><br/>
									</c:when>
									<c:otherwise>
										<span>
											${item.itemNo} - <spring:message code="jiactions.theactivity"/> '<cwms:besttranslation translations="${item.state.translations}"/>'
											<spring:message code="jiactions.string2"/>
										</span><br/>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</div>
						<!-- clear floats and restore page flow -->
						<div class="clear"></div>
					</li>
					<li>
						<label>&nbsp;</label>
						<input type="submit" value="<spring:message code='jiactions.overrstat'/>"/>
					</li>
				</ol>
			</fieldset>
		</form:form>
	</div>
</div>