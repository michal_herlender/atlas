<%-- File name: /trescal/core/jobs/calibration/viewstandardsusageanalysis.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">View Standards Usage Analysis</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/jobs/calibration/ViewStandardsUsageAnalysis.js'></script>
        <script type='text/javascript' src='script/thirdparty/jQuery/jquery.js' ></script>
        <script type="module" src="script/components/cwms-upload-file/cwms-upload-file.js" ></script>
    </jsp:attribute>
    <jsp:attribute name="globalElements">
    	<cwms-upload-file system-component-id="${sc.componentId}" entity-id1="${standardsUsageAnalysis.instrument.plantid}" entity-id2=""
  			files-name="${filesName}" rename-to="" web="false">
		</cwms-upload-file>
    </jsp:attribute>
	<jsp:body>
		<div class="infobox" >
			<fieldset>
				<legend>Standards Usage Analysis<span class="bold"></span> </legend>
				<div class="displaycolumn">
					<ol>
						<li>
							<label><spring:message code="barcode" />:</label>
							<span><a href="viewinstrument.htm?plantid=${standardsUsageAnalysis.instrument.plantid}" class="mainlink"><c:out value="${standardsUsageAnalysis.instrument.plantid}"/></a></span>
						</li>
						<li>
							<label><spring:message code="certnumber" />:</label>
							<span>
								<a href="viewcertificate.htm?certid=${standardsUsageAnalysis.certificate.certid}" class="mainlink">
								${standardsUsageAnalysis.certificate.certno}</a>
							</span>
						</li>
						<li>
							<label><spring:message code="status" />:</label>
							<span><c:out value="${standardsUsageAnalysis.status.getStatus()}" /></span>
						</li>
					</ol>
				</div>
				<div class="displaycolumn">
					<ol>
						<li>
							<label><spring:message code="calsearch.startdate" />:</label>
							<span><fmt:formatDate type="date" dateStyle="SHORT" value="${standardsUsageAnalysis.startDate}"/></span>
						</li>
						<li>
							<label><spring:message code="bpo.enddate" />:</label>
							<span><fmt:formatDate type="date" dateStyle="SHORT" value="${standardsUsageAnalysis.finishDate}"/></span>
						</li>
					</ol>
				</div>
			
				<div class="clear-height"></div>
			</fieldset>
		</div>
		<div id="subnav">
			<dl>
				<dt><a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'files-tab', false);" id="files-link" title="<spring:message code="files" />" class="selected"><spring:message code="files"/></a></dt> 
				<dt><a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'update-tab', false);" title="<spring:message code="update" />" id="update-link"><spring:message code="update"/></a></dt>
				<dt><a href="#" onclick=" event.preventDefault(); genDocPopup(${standardsUsageAnalysis.id}, 'generatereversetraceabilityreport.htm?id=', 'Generating Reverse Traceability Report'); " title=""><spring:message code='generate' /></a></dt>
			</dl>
		</div>
		<div class="infobox">
			<div id="files-tab">
				<files:showFilesForSC entity="${standardsUsageAnalysis}" sc="${sc}" id="${standardsUsageAnalysis.id}"
						identifier="${standardsUsageAnalysis.id}" ver="" rootFiles="${scRootFiles}"
						allowEmail="${false}" isEmailPlugin="${false}"
						rootTitle="Files for Standards Usage Analysis" deleteFiles="${true}"/>
			</div>	
			<div id="update-tab" class="hid">
				<div>
					<form:form method="post" action="updatestandardsusage.htm" modelAttribute="form">
						<form:hidden path="standardsUsageAnalysisId" value="${standardsUsageAnalysis.id}"/>
						<fieldset>
							<legend>Update Standards Usage</legend>
							<ol>
								<li>
									<label><spring:message code="calsearch.startdate" />:</label>
									<form:select path="status" style=" width: 130px; ">
										<c:forEach var="ss" items="${form.standardsUsageAnalysisStatus}">
											<option value="${ss}" <c:if test="${ss.status eq standardsUsageAnalysis.status.getStatus()}">selected</c:if> > ${ss.status} </option>
										</c:forEach>
									</form:select>
								</li>
								<li>
									<label><spring:message code="calsearch.startdate" />:</label>
									<form:input type="date" path="startDate" value="${cwms:isoDate(standardsUsageAnalysis.startDate)}"/>
								</li>
								<li>
									<label><spring:message code="bpo.enddate" />:</label>
									<form:input type="date" path="finishDate" value="${cwms:isoDate(standardsUsageAnalysis.finishDate)}"/>
								</li>
								<li>
									<label>&nbsp;</label>
									<span>
										<input type="submit" value="<spring:message code='update' />" tabindex="6" />
									</span>
								</li>
							</ol>
						</fieldset>
					</form:form>
				</div>
			</div>
		</div>			
	</jsp:body>
</t:crocodileTemplate>
