<%-- File name: /trescal/core/jobs/job/viewjob.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel"%>
<%@ taglib prefix="costs" tagdir="/WEB-INF/tags/costs"%>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>

<t:crocodileTemplate>
	<jsp:attribute name="scriptPart">
		<script>
			function updateInstructionSize(instructionSize) {}
		</script>
		<script type="module"
			src="script/components/cwms-translate/cwms-translate.js"></script>
		<script type="module"
			src="script/components/cwms-upload-certificates/cwms-upload-certificates.js"></script>
		<script type="module"
			src="script/components/cwms-upload-file/cwms-upload-file.js"></script>
		<script type="module"
			src="script/components/cwms-jcosting-items/cwms-jcosting-items.js"></script>
		<script type="module"
			src="script/components/cwms-add-quick-items/cwms-add-quick-items.js"></script>
		<script type="module"
			src="script/components/cwms-instructions/cwms-instructions.js"></script>
		<script type="module"
			src="script/components/cwms-deliveries-items/cwms-deliveries-items.js"></script>
		<script type="module"
			src="script/components/cwms-job-po-table/cwms-job-po-table.js"></script>
		<script type="module"
			src="script/components/cwms-contract-reviews-table/cwms-contract-reviews-table.js"></script>
		<script type="module">
		    import {JobItemFeedback} from "./script/trescal/core/jobs/job/JobItemFeedback/JobItemFeedback.js";
		    window.JobItemFeedback = JobItemFeedback;
		</script>
		<script type="module"
			src="script/components/cwms-po-table/cwms-po-table.js"></script>
		<script src="script/trescal/core/jobs/job/ViewJob.js"></script>
		<script src='script/thirdparty/jQuery/jquery.js'></script>
		<script src="script/thirdparty/ckeditor4/ckeditor.js"></script>    
		<script
			src="script/trescal/core/utilities/searchplugins/jquery/ProcedureSearchJQPlugin.js"></script>
		<script
			src="script/trescal/core/utilities/searchplugins/jquery/WorkInstSearchJQPlugin.js"></script>
		<script
			src="script/trescal/core/tools/labelprinting/dymo/DymoLabelFunctions.js"></script>
		
		<link rel="stylesheet"
			href="script/thirdparty/jQuery/jHtmlArea/jHtmlArea.css">
		<script>
			var groups = [];
			<c:forEach var="group" items="${job.groups}">
				groups.push(${group.id});
			</c:forEach>
			var bookingInAddrs = [];
			<c:forEach items="${bookingInAddresses}" var="addressDto">
				bookingInAddrs.push({id: ${addressDto.key}, address: '<spring:message javaScriptEscape="true" text="${addressDto.value}"/>'});
			</c:forEach>
			var quickitembc = 0;
			<c:if test="${quickitembc != null}">quickitembc = ${quickitembc}</c:if>
			var jobId = ${job.jobid};
		</script>
		<style>
.greenText {
	color: green !important;
	font-weight: bold;
}

#jobiteminvoicetable #itemno {
	width: 7% !important;
}

#jobiteminvoicetable #status {
	width: 18% !important;
}

#jobiteminvoicetable #comments {
	width: 18% !important;
}
</style>
	</jsp:attribute>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="viewjob.viewjob" /> - ${job.jobno}</span>
	</jsp:attribute>
	<jsp:body>
	
		<c:if test="${not empty message}">
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						${message}
					</div>
				</div>
			</div>
		</c:if>
		<!-- main div which contains all job information -->
		<div id="viewjob">
			<!-- infobox contains all job information and is styled with nifty corners -->
			<div class="infobox">
				<fieldset>
					<legend>
						<spring:message code="croverview.jobno" />: ${job.jobno}</legend>
					<!-- displays content in column 49% wide (left) -->
					<div class="displaycolumn">
						<ol>
							<li>
								<label><spring:message code="company" />:</label>
								<span>
									<links:companyLinkInfo active="${companyActive}"
										companyId="${job.con.sub.comp.coid}" onStop="${companyOnStop}"
										companyName="${job.con.sub.comp.coname}" copy="true"
										rowcount="0" />
								</span>
							</li>
							<li>
								<label><spring:message code="subdivision" />:</label>
								<span>
									<links:subdivLinkInfo rowcount="0"
										subdivId="${job.con.sub.subdivid}"
										subdivName="${job.con.sub.subname}"
										companyActive="${companyActive}"
										companyOnStop="${companyOnStop}"
										subdivActive="${job.con.sub.active}" />
								</span>
							</li>
							<li>
								<label><spring:message code="changeclient.contact" />:</label>
								<span>
									<links:contactLinkInfo active="${job.con.active}" rowcount="1"
										personId="${job.con.personid}" contactName="${job.con.name}" />
									<c:choose>
										<c:when test='${not empty job.con.telephone}'>
											${job.con.telephone}
										</c:when>
										<c:when test='${not empty job.con.mobile}'>
											${job.con.mobile}
										</c:when>
									</c:choose>
								</span>
							</li>
							<c:if test="${not empty job.returnToLoc}">
								<li>
									<label><spring:message code="viewjob.location" />:</label> 			
									<span>${job.returnToLoc.location}</span>
								</li>
							</c:if>
							<li>
								<label><spring:message code="web.returnto.label" /></label>												
								<div class="float-left padtop">
									<c:choose>
										<c:when
											test='${job.returnTo.addrid == null || job.returnTo.addrid == ""}'>
											<div>
												<spring:message code="viewjob.noretaddrset" />
											</div>
										</c:when>
										<c:otherwise>
											<t:showCompactFullAddress address="${job.returnTo}"
												edit="${true}" copy="${true}"></t:showCompactFullAddress>
										</c:otherwise>
									</c:choose>
								</div>
								<!-- clear floats and restore page flow -->
								<div class="clear"></div>
							</li>
							<li>
								<label><spring:message code="jobresults.purchord" />:</label>
								<div class="float-left">
									<c:choose>
										<c:when test='${job.POs.size() < 1 && job.bpo == null}'>
											<div class="padtop">
												<spring:message code="viewjob.nopurchorder" />
											</div>
										</c:when>
										<c:otherwise>
											<t:showPurchaseOrder job="${job}" />
										</c:otherwise>
									</c:choose>
								</div>
								<!-- clear floats and restore page flow -->
								<div class="clear"></div>
							</li>
							<li>
								<label class="bold"><spring:message
										code="viewjob.jobinstr" />:</label>
								<cwms-instructions link-jobid="${ job.jobid }"
									show-mode="IN_HEAD" instruction-types="${ instructionTypes }"></cwms-instructions>
								<c:if test="${ job.contractInstructionsSize > 0 }">
									<br />
									<span class="bold error accesslink"><spring:message
											code="warning" /> : </span>
									<span class="error"><spring:message
											code="viewjob.contractinstrs.warning" /></span>
								</c:if>
								
							</li>
							<li>
								<label><spring:message code="addjob.receiptdate" />:</label>
								<span id="receiptDateSpan"><fmt:formatDate type="date"
										dateStyle="SHORT" timeStyle="SHORT" value="${job.receiptDate}" /></span>
							</li>
							
								<li>
                                    <label><spring:message
										code="purchaseorder.businesscontact" />:</label>            
                                    <span>${job.createdBy.getName()}</span>
                                </li>
							<li>
								<label><spring:message code="addjob.pickupdate" />:</label>
								<span id="pickupDate"><fmt:formatDate type="date"
										dateStyle="SHORT" value="${job.pickupDate}" /></span>
							</li>
							<c:if test="${ job.type eq 'SITE' }">
								<li>
									<label><spring:message code="viewjob.certfile" />:</label>
									<a href="#" class="mainlink"
									onclick="event.preventDefault();document.querySelector('cwms-upload-certificates').show();">
											<spring:message code="viewjob.uploadcertfile" />
									</a>
								</li>
							</c:if>
						</ol>
					</div>
					<!-- end of left column -->
					<!-- displays content in column 49% wide (right) -->
					<div class="displaycolumn">
						<ol>
							<li>
								<label><spring:message code="jobresults.stat" />
									<c:if test="${job.dateComplete != null}">/Date:</c:if>
								</label>
								<span id="jobStatusContainer">
									<cwms:besttranslation translations="${job.js.nametranslations}" />

									<c:if test="${not empty job.dateComplete}">
										&nbsp;(<fmt:formatDate value="${job.dateComplete}" type="date"
											dateStyle="SHORT" />)
									</c:if>
									<c:if test="${job.js.readyToInvoice}">
										<a href="#" class="mainlink"
											onclick=" invoiceNotRequired(${job.jobid}); return false; ">
											<spring:message code="viewjob.invoinotrequir" />
										</a>
									</c:if>
								</span>
							</li>
							<li>
								<label><spring:message code="viewjob.jobtypeindate" />:</label>
								<span>${job.type.getDescription()} (<strong><fmt:formatDate
											value="${job.regDate}" type="date" dateStyle="SHORT" /></strong>)</span>
							</li>	
							<li>
								<label><spring:message code="viewjob.agrdeldate" />:</label>
								<span>
									<c:choose>
										<c:when test="${not empty job.agreedDelDate}">
											<fmt:formatDate value="${job.agreedDelDate}" type="date"
												dateStyle="SHORT" />
										</c:when>
										<c:otherwise>
											N/A
										</c:otherwise>
									</c:choose>
								</span>
							</li>
							<li>
								<label><spring:message code="addjob.clientref" />:</label>
								<span>
									<c:choose>
										<c:when test="${job.clientRef.trim().length() > 0}">
											${job.clientRef}
										</c:when>
										<c:otherwise>
											&nbsp;-- <spring:message code="viewjob.noclientref" /> --&nbsp;
										</c:otherwise>
									</c:choose>
								</span>
							</li>
							<li>
								<label><spring:message code="addjob.currency" />:</label>
								<c:set var="currency" value="${job.currency}" />
								<span>
									${currency.currencyCode}
									(${currency.currencySymbol} ${currency.currencyName}
									- ${defaultCurrency.currencyERSymbol}1 : ${currency.currencyERSymbol}${job.rate})
								</span>
							</li>
							<li>
								<label><spring:message code="addjob.bookinaddr" />:</label>
								<span>
									<c:if test="${job.bookedInAddr != null}">
										<cwms:securedLink permission="ADDRESS_VIEW"
											classAttr="mainlink"
											parameter="?addrid=${job.bookedInAddr.addrid}">
											${job.bookedInAddr.addr1}, ${job.bookedInAddr.town}
										</cwms:securedLink>
									</c:if>
								</span>
							</li>
							<c:if test="${job.bookedInLoc != null}">
								<li>
									<label><spring:message code="addjob.bookinloc" />:</label>
									<span>
											<cwms:securedLink permission="LOCATION_EDIT"
											classAttr="mainlink"
											parameter="?locid=${job.bookedInLoc.locationid}">
												${job.bookedInLoc.location}
											</cwms:securedLink>
									</span>
								</li>
							</c:if>
							<li>
								<label><spring:message code="viewjob.returntoaddr" />:</label>
								<span>
									<c:if test="${job.returnTo != null}">
										<cwms:securedLink permission="ADDRESS_VIEW"
											classAttr="mainlink"
											parameter="?addrid=${job.returnTo.addrid}">
											${job.returnTo.addr1}, ${job.returnTo.town}
										</cwms:securedLink>
									</c:if>
								</span>
							</li>
							<c:if test="${job.returnToLoc != null}">
								<li>
									<label><spring:message code="viewjob.returntoloc" />:</label>
									<span>
										<cwms:securedLink permission="LOCATION_EDIT"
											classAttr="mainlink"
											parameter="?locid=${job.returnToLoc.locationid}">
											${job.returnToLoc.location}
										</cwms:securedLink>
									</span>
								</li>
							</c:if>
							<li>
								<label><spring:message code="viewjob.transpout" />:</label>
								<span>
									<c:choose>
										<c:when test="${not empty job.returnOption.localizedName}">
											${job.returnOption.localizedName }
										</c:when>
										<c:otherwise>
											<t:showTranslationOrDefault
												translations="${job.returnOption.method.methodTranslation}"
												defaultLocale="${defaultlocale}" />
										</c:otherwise>
									</c:choose>
								</span>
							</li>
							<li>
								<label><spring:message code="viewjob.reqonitems" />:</label>
								<div class="float-left padtop attention bold">
									<c:set var="itemsWithReqCount" value="${null}" />
									<c:forEach var="i" items="${job.items}">
										<c:set var="crForJI" value="${calReqs.get(i.jobItemId)}" />
										<c:if test="${crForJI != null}">
											<c:if test="${itemsWithReqCount > 0}">, </c:if>${i.itemNo}
											<c:set var="itemsWithReqCount"
												value="${itemsWithReqCount + 1}" />
										</c:if>
									</c:forEach>
									<c:if test="${itemsWithReqCount == 0}">
										<spring:message code="viewjob.noitemswithrequirements" />
									</c:if>
								</div>
								<!-- clear floats and restore page flow -->
								<div class="clear"></div>
							</li>
							<li>
								<label><spring:message code="viewjob.contractsonitems" />:</label>
								<div class="float-left">
									<c:forEach items="${ job.contractsItems }" var="ji">												
										<links:jobitemLinkInfo jobItemNo="${ji.itemNo}"
											jobItemId="${ji.jobItemId}" defaultPage="jiactions.htm" />
									</c:forEach>
								</div>
							</li>
						</ol>
					</div>
					<!-- end of right column -->
				</fieldset>
			</div>
			<!-- sub navigation menu which displays and hides sections of page as well as linking to external pages -->
			<div id="subnav">
				<dl>
					<c:set var="poSize" value="${job.POs.size() + job.bpoLinks.size()}" />
					<dt>
						<a href="#"
							onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'jobitem-tab', false); "
							id="jobitem-link" class="selected"
							title='<spring:message code="viewjob.viewitemsforjob"/>'><spring:message
								code="accessoriesanddefects.jobitems" /> (<span
							class="jobItemsSize">${job.items.size()}</span>)</a>
					</dt>
					<dt>
					<c:choose>
						<c:when test="${ job.js.complete}">
					<a href="#"
							onclick="confirm('Please Note: The job is marked completed - Click Ok to continue or Cancel.' ); event.preventDefault(); switchMenuFocus(menuElements, 'quickitems-tab', true); "
							id="quickitems-link"
							title='<spring:message code="viewjob.additemswithbarcode"/>'><spring:message
								code="viewjob.quickitems" /></a>
											</c:when>
				 	<c:otherwise>	
				 	<a href="#"
							onclick="event.preventDefault(); switchMenuFocus(menuElements, 'quickitems-tab', true); "
							id="quickitems-link"
							title='<spring:message code="viewjob.additemswithbarcode"/>'><spring:message
								code="viewjob.quickitems" /></a>
				 	</c:otherwise> 
						</c:choose>
				 
					</dt>

					<dt>	
					<c:choose>

						<c:when test="${ job.js.complete}">
					&nbsp;<a href="newjobitemsearch.htm?jobid=${job.jobid}" onclick="return confirm('Please Note: The job is marked completed - Click Ok to continue or Cancel.' );"
							id="newitem-link"
							title='<spring:message code="viewjob.additemstojob"/>'><spring:message
								code="viewjob.newitem" /></a>
								
								
								</c:when>
				 	<c:otherwise>
					&nbsp;<a href="newjobitemsearch.htm?jobid=${job.jobid}"
							id="newitem-link"
							title='<spring:message code="viewjob.additemstojob"/>'><spring:message
								code="viewjob.newitem" /></a>
					</c:otherwise> 
						</c:choose>
					</dt>

					<dt>
								<c:choose>
									<c:when test="${ job.js.complete}">
						<a href="#"
							onClick=" confirm('Please Note: The job is marked completed - Click Ok to continue or Cancel.' );  event.preventDefault(); switchMenuFocus(menuElements, 'expenses-tab', false); changeServiceModelSearch(-1,${job.jobid});"
							id="expenses-link"
							title='<spring:message code="viewjob.jobservices"/>'><spring:message
								code="viewjob.jobservices" /> (${job.expenseItems.size()})</a>
								</c:when>
								<c:otherwise>
									<a href="#"
							onClick="event.preventDefault(); switchMenuFocus(menuElements, 'expenses-tab', false); changeServiceModelSearch(-1,${job.jobid});"
							id="expenses-link"
							title='<spring:message code="viewjob.jobservices"/>'><spring:message
								code="viewjob.jobservices" /> (${job.expenseItems.size()})</a>
								</c:otherwise>
								</c:choose>
								
					</dt>
					<dt>
						<c:set var="ACC_DEF">
							<spring:message code="viewjob.string2" />
						</c:set>
												
						<a href="accessoriesanddefects.htm?jobid=${job.jobid}"
							id="accessories-link"
							title="<spring:message code="accessoriesanddefects.accessories"/>">
							<spring:message code="accessoriesanddefects.accessories" /> (${countItemsJobAccessory})</a>
					</dt>
					<dt>
						<spring:message var="titleEditJob" code="viewjob.string3" />
						<cwms:securedLink permission="JOB_EDIT" id="editjob-link"
							collapse="true" title="${titleEditJob}"
							parameter="?jobid=${job.jobid}">
							<spring:message code="viewjob.editjob" />
						</cwms:securedLink>
					</dt>
					<dt>
						<a href="#"
							onclick="switchMenuFocus(menuElements, 'contractinstructions-tab', false); return false; "
							id="contractinstructions-link"
							title='<spring:message code="viewjob.string56"/>'><spring:message
								code="viewjob.contractinstrs" /> (<span class="instructionSize">${job.contracts.size()}</span>)</a>
					</dt>
					<dt>
						<a href="#"
							onclick="switchMenuFocus(menuElements, 'instructions-tab', false); return false; "
							id="instructions-link"
							title='<spring:message code="viewjob.string4"/>'><spring:message
								code="viewjob.instrs" /> (<span class="instructionSize">${job.relatedInstructionLinks.size()}</span>)</a>
					</dt>
					<dt>
						<a href="#"
							onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'newdeliveries-tab', false); document.querySelector('cwms-deliveries-items').refresh()"
							id="newdeliveries-link"
							title='<spring:message code="viewjob.string5"/>'> <spring:message
								code="viewjob.delivs" /> (${delivriesSize})</a>
					</dt>
					<dt>
						<a href="#"
							onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'clientpurchord-tab', false); "
							id="clientpurchord-link"
							title='<spring:message code="viewjob.string6"/>'><spring:message
								code="viewjob.clientpos" /> (<span class="cpoSizeSpan">${poSize}</span>)</a>
					</dt>
					<dt>
						<a href="#"
							onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'printing-tab', false); "
							id="printing-link"
							title='<spring:message code="viewjob.printlbljob"/>'><spring:message
								code="viewjob.printing" /></a>
					</dt>
				</dl>
			</div>
			<!-- end of sub navigation menu -->
			<!-- sub navigation menu which displays and hides sections of page as well as linking to external pages -->
			<div id="subnav2">
				<dl>
					<dt>
						<a href="#"
							onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'jobfiles-tab', false); "
							id="jobfiles-link"
							title='<spring:message code="viewjob.string7"/>'><spring:message
								code="viewjob.jobfiles" /> (<span class="fileResourceCount">${scRootFiles.numberOfFiles}</span>)</a>
					</dt>
					<dt>
						<a href="#"
							onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'invoices-tab', false); "
							id="invoices-link"
							title='<spring:message code="viewjob.string8"/>'><spring:message
								code="viewjob.invoice" /> (${job.invoiceLinks.size()})</a>
					</dt>
					<dt>
						<a href="#"
							onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'purchord-tab', false); document.querySelector('cwms-job-po-table').refresh() "
							id="purchord-link"
							title='<spring:message code="viewjob.string9"/>'><spring:message
								code="viewjob.purchord" /> (${PoSize})</a>
					</dt>
					<dt>
						<a href="#"
							onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'contractrev-tab', false); document.querySelector('cwms-contract-reviews-table').refresh() "
							id="contractrev-link"
							title='<spring:message code="viewjob.string10"/>'><spring:message
								code="viewjob.contrrev" /> (${ContractReviewSize})</a>
					</dt>
					<dt>
						<a href="#"
							onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'costs-tab', false);document.querySelector('cwms-jcosting-items').refresh() "
							id="costs-link" title='<spring:message code="viewjob.string11"/>'><spring:message
								code="viewjob.pricesquotes"
								arguments="${job.jobCostings.size()},${job.linkedQuotes.size()}" /></a>
					</dt>
					<dt>
						<a href="#"
							onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'email-tab', false); "
							id="email-link" title='<spring:message code="viewjob.string12"/>'><spring:message
								code="viewjob.emails" /> (<span class="entityEmailDisplaySize">${job.sentEmails.size()}</span>)</a>
					</dt>
					<dt>
						<a href="#"
							onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'batchcal-tab', false); "
							id="batchcal-link"
							title='<spring:message code="viewjob.string13"/>'><spring:message
								code="viewjob.batchcals" /> (${job.batchCals.size()})</a>
					</dt>
					<dt>
						<a href="#"
							onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'jobsheet-tab', false); "
							id="jobsheet-link"
							title='<spring:message code="viewjob.string14"/>'><spring:message
								code="viewjob.jobsheet" /></a>
					</dt>
					<dt>
						<a href="#"
							onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'advanced-tab', false); "
							id="advanced-link"><spring:message code="viewjob.advancfeat" /></a>
					</dt>
					<c:if test="${ !empty job.jobCourier }">
						<dt>
							<a href="#"
								onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'jobcourier-tab', false); "
								id="jobcourier-link"
								title='<spring:message code="viewjob.string54"/>'><spring:message
									code="viewjob.jobcourier" /></a>
						</dt>
					</c:if>
					<!-- #springUrl('/printjobdocument.htm?jobid=')${job.jobid} -->
				</dl>
			</div>
			<!-- end of sub navigation menu -->
			<!-- infobox contains all job functionality and is styled with nifty corners -->
			<div class="infobox">
				<!-- this section displays all job items for the selected job -->
				<div id="jobitem-tab">
					<!-- div to hold all the tabbed submenu -->
					<div id="tabmenu">
						<!-- tabbed submenu to change between contract review options -->
						<ul class="subnavtab">
							<li><a href="#" id="jobitem_default-link"
								onclick=" event.preventDefault(); switchMenuFocus(menuElements2, 'jobitem_default-tab', false); "
								class="selected"><spring:message code="viewjob.jobitemdef" /></a></li>
							<li><a href="#" id="jobitem_group-link"
								onclick=" event.preventDefault(); switchMenuFocus(menuElements2, 'jobitem_group-tab', false); "><spring:message
										code="viewjob.jobitemgroups" /></a></li>
						</ul>
						<!-- div to hold all content which is to be changed using the tabbed submenu -->
						<div class="tab-box">
							<!-- div displaying all job items -->
							<div id="jobitem_default-tab">
								<table id="jobitemlist" class="tablesorter"
									summary="This table lists all job items for the current job">
									<thead>
										<tr>
											<td id="itemQuantityRow" colspan="10">
												<div class="float-left">
													<spring:message code="accessoriesanddefects.jobitems" /> (<span
														class="jobItemsSize">${job.items.size()}</span>)</div>
												<a href="croverview.htm?jobid=${job.jobid}" target="_blank"
												class="mainlink-float"><spring:message
														code="viewjob.contrrevoverv" /></a>
												<a href="#" class="mainlink-float"
												onclick="JobItemFeedback.createFeedbackRequestContent(${job.jobid}, ${job.con.sub.comp.coid}, ${contact.personid}, ${sc.componentId}); return false;"
												title=""><spring:message code="viewjob.reqjobitfeed" /></a>
											</td>
										</tr>
										<tr>
											<th class="item" scope="col"><spring:message
													code="accessoriesanddefects.item" /></th>  
											<th class="instrument" scope="col"><spring:message
													code="instmodelname" /></th>
											<th class="serial" scope="col"><spring:message
													code="serialno" /></th>  
											<th class="plant" scope="col"><spring:message
													code="plantno" /></th>
											<th class="plant" scope="col"><spring:message
													code="frequency" /></th>
											<th class="plant" scope="col"><spring:message
													code="barcode" /></th> 
											<th class="group" scope="col"><spring:message
													code="accessoriesanddefects.group" /></th>
											<th class="caltype" scope="col"><spring:message
													code="jobresults.caltype" /></th>
											<th class="status" scope="col"><spring:message
													code="jobresults.stat" /></th>
											<th class="delete" scope="col"><spring:message
													code="accessoriesanddefects.delete" /></th>
										</tr>
									</thead>
									<tfoot>
										<tr>
											<td colspan="10">&nbsp;</td>
										</tr>
									</tfoot>
									<tbody>
										<c:choose>
											<c:when test="${job.items.size() < 1}">
												<tr>
													<td colspan="10" class="bold center">
														<spring:message code="viewjob.string15" />
													</td>
												</tr>
											</c:when>
											<c:otherwise>
												<c:set var="rowcount1" value="0" />
												<c:forEach var="i" items="${job.items}">
														<c:set var="bgcolor"
														value="${i.calType.serviceType.displayColour}" />
													<c:if test="${i.turn <= fastTrackTurn}">
														<c:set var="bgcolor"
															value="${i.calType.serviceType.displayColourFastTrack}" />
													</c:if>
													<c:set var="defaultURI"
														value="${contractReviewedJobItemIds.contains(i.jobItemId) ? 'jiactions.htm' : 'jicontractreview.htm'}" />
													<tr id="ji${i.jobItemId}"
														style=" background-color: ${bgcolor}"
														onclick=" window.location.href='${defaultURI}?jobitemid=${i.jobItemId}';"
														onmouseover="$j(this).addClass('hoverrow');"
														onmouseout="$j(this).removeClass('hoverrow');">
														<td><links:jobitemLinkInfo jobItemNo="${i.itemNo}"
																jobItemId="${i.jobItemId}" defaultPage="${defaultURI}" />
														</td>
														<td>
															<cwms:showinstrument instrument="${i.inst}" />
															<c:set var="crForJI" value="${calReqs.get(i.jobItemId)}" />
															<c:if test="${crForJI != null}">
																<img src="img/icons/info-red.png" width="12" height="12"
																	class="image_inl"
																	alt='<spring:message code="viewjob.imginfored"/>'
																	title='<spring:message code="viewjob.imginfored"/>' />
															</c:if>
															<div style="float: right">
																<span style="vertical-align: top;">(${i.images.size()})</span>
																<a href="#"
																	onclick="openImageUploadPopUp(event,${i.jobItemId}, '${imagetype}'); return false;"
																	title="">
																	<img src="img/icons/picture_add.png" width="16"
																	height="16"
																	alt="<spring:message code='jiimages.addimgs'/>"
																	title="<spring:message code='jiimages.addimgs'/>" />
																</a>
															</div>
														</td>
														<td>${i.inst.serialno}</td>
														<td>${i.inst.plantno}</td>
														<td>${i.inst.calFrequency} (${i.inst.calFrequencyUnit.getName(i.inst.calFrequency)})</td>
														<td>
															<cwms:securedLink permission="INSTRUMENT_VIEW"
																classAttr="mainlink"
																parameter="?plantid=${i.inst.plantid}">
														      ${i.inst.plantid}
											                </cwms:securedLink>
														</td>
														<td class="group center">${i.group.id}</td>
														<td class="center"><t:showTranslationOrDefault
																translations="${i.calType.serviceType.shortnameTranslation}"
																defaultLocale="${defaultlocale}" /></td>
														<td><t:showTranslationOrDefault
																translations="${i.state.translations}"
																defaultLocale="${defaultlocale}" /></td>
														<td class="center">
															<form:form action="deletejobitem.htm" method="post">
																<cwms:securedJsLink collapse="True"
																	permission="JOB_ITEM_DELETE" classAttr="imagelink"
																	onClick=" event.cancelBubble = true; if(confirm(i18n.t('core.jobs:job.confirmDeleteJobItem', 'Are you sure you wish to delete this job item?')) == true){$j(this).parent().submit();} return false; ">
																	<img src="img/icons/delete.png" width="16" height="16"
																		alt='<spring:message code="viewjob.imgdelete"/>'
																		title='<spring:message code="viewjob.imgdelete"/>' />
																</cwms:securedJsLink>
																<input type="hidden" name="deleteItemId"
																	value="${i.jobItemId}" />
															</form:form>
														</td>
													</tr>
													<c:set var="rowcount1" value="${rowcount1 + 1}" />
												</c:forEach>
											</c:otherwise>
										</c:choose>
									</tbody>
								</table>
							</div>
							<!-- end of div displaying all job items -->
							<!-- div displaying all grouped job items -->
							<div id="jobitem_group-tab" class="hid">
								<table class="default4 groupedItems">
									<thead>
										<tr>
											<td colspan="8">
												<div class="float-left">
													<spring:message code="viewjob.jobitemsbygroup" /> (<span
														class="jobItemsSize">${job.items.size()}</span>)</div>
												<cwms:securedJsLink collapse="True"
													permission="JOB_ITEM_NEW_GROUP_ADD"
													classAttr="mainlink-float"
													onClick="createNewGroup(${job.jobid}, ${fastTrackTurn}); return false; ">
													<spring:message code="viewjob.createnewgroup" />
												</cwms:securedJsLink>
												<div class="clear"></div>
											</td>
										</tr>
										<tr>
											<th class="type" scope="col"><spring:message
													code="jobresults.type" /></th>
											<th class="item" scope="col"><spring:message
													code="accessoriesanddefects.item" /></th>  
											<th class="instrument" scope="col"><spring:message
													code="instmodelname" /></th>  
											<th class="serial" scope="col"><spring:message
													code="serialno" /></th>  
											<th class="plant" scope="col"><spring:message
													code="plantno" /></th>  
											<th class="caltype" scope="col"><spring:message
													code="jobresults.caltype" /></th>
											<th class="status" scope="col"><spring:message
													code="jobresults.stat" /></th>
											<th class="group action" scope="col"><spring:message
													code="viewjob.move" /></th>
										</tr>
									</thead>													
								</table>
								<c:forEach var="group" items="${job.groups}">
									<table class="default4 groupedItems" summary="">
										<tbody id="group${group.id}">
											<tr class="groupHead">
												<th class="type">
													<a href="#"
													onclick="deleteGroup(${group.id}, ${job.jobid}, ${fastTrackTurn}); return false;">
														<img src="img/icons/delete.png" width="16" height="16"
														alt='<spring:message code="viewjob.imgdelgroup"/>'
														title='<spring:message code="viewjob.imgdelgroup"/>'
														class="img_marg_bot" />
													</a>
													Group ${group.id}
												</th>
												<th class="rest" colspan="7">&nbsp;</th>
											</tr>
											<c:choose>
												<c:when test="${group.items.size() < 1}">
													<tr class="groupMessage">
														<td id="noelement" colspan="8" class="bold center">
															<spring:message code="viewjob.string16" />
														</td>
													</tr>
												</c:when>
												<c:otherwise>
													<c:forEach var="i" items="${group.items}">
														<tr id="gji${i.itemNo}"
															style=' background-color: <c:choose><c:when test="${i.turn <= fastTrackTurn}">${i.calType.serviceType.displayColourFastTrack}; </c:when><c:otherwise>${i.calType.serviceType.displayColour}; </c:otherwise></c:choose>'>
															<td class="type"><t:showTranslationOrDefault
																	translations="${i.inst.model.modelType.modelTypeNameTranslation}"
																	defaultLocale="${defaultlocale}" /></td>
															<td class="item"><links:jobitemLinkInfo
																	jobItemNo="${i.itemNo}" jobItemId="${i.jobItemId}"
																	contractReviewed="${contractReviewedJobItemIds.contains(i.jobItemId)}" />
															</td>
															<td class="instrument"><links:instrumentLinkDWRInfo
																	displayCalTimescale="${false}" displayName="${true}"
																	instrument="${i.inst}"
																	caltypeid="${i.calType.calTypeId}"
																	rowcount="${rowcount1}" displayBarcode="${false}" /></td>
															<td class="serial">${i.inst.serialno}</td>
															<td class="plant">${i.inst.plantno}</td>
															<td class="caltype"><t:showTranslationOrDefault
																	translations="${i.calType.serviceType.shortnameTranslation}"
																	defaultLocale="${defaultlocale}" /></td>
															<td class="status"><t:showTranslationOrDefault
																	translations="${i.state.translations}"
																	defaultLocale="${defaultlocale}" /></td>
															<td class="action">																			
																<select name="groupselect" id="groupselect${i.itemNo}"
																onchange="if (this.value != ${i.group.id}){ moveGroupItem(${i.jobItemId},${i.itemNo},this.value,${i.group.id}); return false; } ">
																	<option value="0"><spring:message
																			code="viewjob.none" /></option>
																	<c:forEach var="group" items="${job.groups}">
																		<option value="${group.id}"
																			${i.group.id == group.id ? "selected" : ""}>${group.id}</option>
																	</c:forEach>
																</select>
															</td>
														</tr>
														<c:set var="rowcount1" value="${rowcount1 + 1}" />
													</c:forEach>
												</c:otherwise>
											</c:choose>
										</tbody>
									</table>
								</c:forEach>
								
								<table class="default4 groupedItems" id="ungroupedItems"
									summary="">
									<tbody id="group0">
										<tr class="groupHead">
											<th colspan="8"><spring:message code="viewjob.ungritems" /></th>
										</tr>
										<c:set var="gi" value="0" />
										<c:forEach var="i" items="${job.items}">
											<c:if test="${i.group == null}">
												<tr id="gji${i.itemNo}"
													style='background-color: <c:choose><c:when test="${i.turn <= $fastTrackTurn}">${i.calType.serviceType.displayColourFastTrack};</c:when><c:otherwise>${i.calType.serviceType.displayColour};</c:otherwise></c:choose>'>
													<td class="type"><t:showTranslationOrDefault
															translations="${i.inst.model.modelType.modelTypeNameTranslation}"
															defaultLocale="${defaultlocale}" /></td>
													
													<td class="item"><links:jobitemLinkInfo
															jobItemNo="${i.itemNo}" jobItemId="${i.jobItemId}"
															contractReviewed="${contractReviewedJobItemIds.contains(i.jobItemId)}" />
													</td>
													<td class="instrument"><links:instrumentLinkDWRInfo
															displayCalTimescale="${false}" displayName="${true}"
															instrument="${i.inst}" caltypeid="${i.calType.calTypeId}"
															rowcount="${rowcount1}" displayBarcode="${false}" /></td>
													<td class="serial">${i.inst.serialno}</td>
													<td class="plant">${i.inst.plantno}</td>
													<td class="caltype"><t:showTranslationOrDefault
															translations="${i.calType.serviceType.shortnameTranslation}"
															defaultLocale="${defaultlocale}" /></td>
													<td class="status"><t:showTranslationOrDefault
															translations="${i.state.translations}"
															defaultLocale="${defaultlocale}" /></td>
													<td class="action">																		
														<select name="groupselect" id="groupselect${i.itemNo}"
														onchange=" if (this.value != 0){ moveGroupItem(${i.jobItemId},${i.itemNo},this.value,0); return false; } ">
															<option value="0"><spring:message
																	code="viewjob.none" /></option>
															<c:forEach var="group" items="${job.groups}">
																<option value="${group.id}">${group.id}</option>
															</c:forEach>
														</select>
													</td>
												</tr>
												<c:set var="rowcount1" value="${rowcount1 + 1}" />
												<c:set var="gi" value="${gi + 1}" />
											</c:if>
										</c:forEach>
										<c:if test="${gi < 1}">
											<tr id="noelement" class="groupMessage">
												<td colspan="8" class="bold center">
													<spring:message code="viewjob.string16" />
												</td>
											</tr>
										</c:if>													
									</tbody>	
								</table>
							<!-- end of div displaying all grouped job items -->
							</div>
						</div>
						<!-- end of div to hold all tabbed submenu content -->
					</div>
					<!-- end of div to hold tabbed submenu -->
				</div>
				<!-- end job item -->
				<!-- this section displays form elements for adding job items quickly -->
				<div id="quickitems-tab" class="hid">
					<div id="quickItemContainer">
						<cwms-add-quick-items job-id="${job.jobid}"
							company-id="${job.con.sub.comp.coid}"></cwms-add-quick-items>																						
					</div>
				</div>
				<!-- end of quick job items section -->
				<div id="expenses-tab" class="hid">
					<div id="jobServiceQuotations">
						<label>Search new services in</label>
						<select id="serviceQuotationSelect"
							onchange="changeServiceModelSearch();">
							<option value="-1" selected>all service models</option>
							<option value="0">all linked quotations</option>
							<c:forEach var="quote" items="${linkedQuotesWithServiceModels}">
								<option value="${quote.key}">${quote.value}</option>
							</c:forEach>
						</select>
					</div>
					<div id="jobServices">
						<jsp:include page="expenseitems.jsp" />
					</div>
				</div>
				<!-- this section displays form elements for "advanced features" -->
				<div id="advanced-tab" class="hid">
					<fieldset>
						<legend>
							<spring:message code="viewjob.advancfeat" />
						</legend>
						<ol>
							<li>
								<label><spring:message code="changeclient.headtext" />:</label>
								<span>
									<cwms:securedLink permission="CLIENT_CHANGE"
										classAttr="mainlink" parameter="?jobid=${job.jobid}">
										<spring:message code="changeclient.headtext" />
									</cwms:securedLink>
								</span>
							</li>
							<c:if test="${allowUpdateAll}">
								<li>
									<label><spring:message code="viewjob.updateitems" />:</label>
									<span>
										<cwms:securedLink permission="JOB_ITEM_UPDATE_ALL"
											classAttr="mainlink" parameter="?jobid=${job.jobid}">
											<spring:message code="viewjob.string19" />
										</cwms:securedLink>
									</span>
								</li>
							</c:if>
							<li>
								<label><spring:message code="viewjob.deljob" />:</label>
								<span>
									<cwms:securedJsLink permission="JOB_DELETE"
										classAttr="mainlink"
										onClick="showDeleteJobForm(${job.jobid}); return false;">
										<spring:message code="viewjob.deljob" />
									</cwms:securedJsLink>
								</span>
							</li>											
							<li>
								<label><spring:message code="addjob.headtext" />:</label>
								<span>
									<cwms:securedLink permission="JOB_ADD" classAttr="mainlink"
										parameter="?basedonjobid=${job.jobid}">
										<spring:message code="viewjob.string20" />
									</cwms:securedLink>
								</span>
							</li>
							<li>
								<label><spring:message code="viewjob.certs" />:</label>
								<!-- 
								<span>
									<a href="certmanagement.htm?jobid=${job.jobid}" class="mainlink"><spring:message code="viewjob.certsmanage"/></a>
								</span>
								 -->
								 Not Available - Future Evolution - Assembla Ticket #591
							</li>
						</ol>
					</fieldset>
				</div>
				<!-- this section used to display form elements for editing job information -->
				<!-- 
					can delete the following js:
						addrChange();
				 -->
				<!-- end of job editing section -->

				<!-- this section displays any contract instructions for the job -->
				<div id="contractinstructions-tab" class="hid">
					<t:showRelatedContractInstructions highlightTypes="" job="${job}" />
				</div>
				<!-- end of contract instruction section -->
				
				<!-- this section displays any instructions for the job -->
				<div id="instructions-tab" class="hid">
					<t:showRelatedInstructions highlightTypes="" job="${job}" />	
				</div>
				<!-- end of instruction section -->
				<!-- this section displays delivery information for the selected job -->
				
				<div id="newdeliveries-tab" class="hid">
					
					<form:form method="POST" action="updateJobDelivery.json"
						modelAttribute="jobDeliveryForm">
					<fieldset>
						<legend>Package Information</legend>
						<ol>
							<li>
							<form:hidden path="jobId" />
								<label><spring:message code="addjob.numberofpackages" />:</label>
								<span>
									<form:input type="number" path="numberOfPackages" />
								</span>
							</li>
							<li>
								<label><spring:message code="addjob.packagetype" />:</label>
								<span>
									<form:input path="packageType" />
								</span>
							</li>
							<li>
								<label><spring:message code="addjob.storagearea" />:</label>
								<span>
									<form:input path="storageArea" />
								</span>
						
							
							
							
							
							
							</ol>
					</fieldset>
					</form:form>
					
					<cwms-deliveries-items job-id="${job.jobid}"></cwms-deliveries-items>	
				</div>
				
				<!-- end of delivery information section -->
				<!-- this section displays purchase order information for the selected job -->
				<div id="clientpurchord-tab" class="hid">
					<cwms-po-table job-id="${job.jobid}" job-no="${job.jobno}"
						person-id="${job.con.personid}"
						currency="${job.currency.currencyCode}"></cwms-po-table>
				</div>
				<!-- end of purchase order information section -->
				<!-- this section contains links for viewing and uploading job files for the selected job -->
				<div id="jobfiles-tab" class="hid">
					<files:showFilesForSC entity="${job}" sc="${sc}" id="${job.jobid}"
						identifier="${job.jobno}" ver="" rootFiles="${scRootFiles}"
						allowEmail="${true}" isEmailPlugin="${false}"
						rootTitle="Files for Job ${job.jobno}" deleteFiles="${true}" />
				</div>
				<!-- end of job file section -->
				<!-- this section contains links for printing tasks for the selected job -->
				<div id="printing-tab" class="hid">
					<fieldset>
						<legend>
							<spring:message code="viewjob.printingtasks" />
						</legend>
						<ol>
							<li>
								<label><spring:message code="viewjob.jobfilelbl" />:</label>
								<span>
									<a href="#" class="mainlink"
									onclick=" event.preventDefault(); printDymoJobLabel(${job.jobid}); "><spring:message
											code="viewjob.printjobfilelabel" /></a>
								</span>
							</li>
							<li>
								<label><spring:message code="viewjob.jobitemlabels" />:</label>
								<span>
									<a href="#" class="mainlink"
									onclick=" event.preventDefault(); printDymoAllJobItemLabels(${job.jobid}); "><spring:message
											code="viewjob.printjobitemlabels" /></a>
								</span>
							</li>
							<li>
								<label><spring:message code="viewjob.multjobitemlbl" />:</label>
								<span>
									<a href="#" class="mainlink"
									onclick=" event.preventDefault(); printMultipleJobItemLabelsContent('dymojobitemlabels.json?jobid=${job.jobid}'); "><spring:message
											code="viewjob.printmultjobitemlbl" /></a>
								</span>
							</li>
							<li>
								<label><spring:message code="viewjob.joblbl" />:</label>
								<span>
									<a href="#" class="mainlink"
									onclick=" event.preventDefault(); printDymoAllJobLabels(${job.jobid}); "><spring:message
											code="viewjob.prntjobfilejobitemslbl" /></a>
								</span>
							</li>
						</ol>
					</fieldset>
				</div>
				<!-- end of printing section -->
				<c:set var="showInstructions" value="${false}" />
				<c:forEach var="link" items="${job.relatedInstructionLinks}">
					<c:forEach var="uit" items="${userInstructionTypes}">
						<c:if
							test="${uit.instructionType == link.instruction.instructiontype}">
							<c:if test='${link.instruction.instructiontype == "INVOICE"}'>
								<c:set var="showInstructions" value="${true}" />
							</c:if>
						</c:if>
					</c:forEach>
				</c:forEach>
				<!-- this section contains all invoice information -->
				<div id="invoices-tab" class="hid">
					<t:showCompanyInvoiceRequirements company="${job.con.sub.comp}"
						monthlyInvoice="${monthlyinvoicesystemdefault}"
						separateCostsInvoice="${separatecostsinvoicesystemdefault}"
						poInvoice="${poinvoicesystemdefault}" />
					<c:if test="${showInstructions}">
						<t:showTableOfInstructionTypesToUser
							userInstructionTypes="${userInstructionTypes}"
							instructionLinks="${job.relatedInstructionLinks}"
							stringType1="Invoice" stringType2=""></t:showTableOfInstructionTypesToUser>
					</c:if>
					<c:set var="confirmInstructions" value="${false}" />
					<c:if
						test="${monthlyinvoicesystemdefault || poinvoicesystemdefault || separatecostsinvoicesystemdefault || showInstructions}">
						<c:set var="confirmInstructions" value="${true}" />
						<span class="float-left" style="margin-top: 5px;">
							<b><spring:message code="viewjob.string27" /></b>
						</span>
						<span class="float-right"><i><spring:message
									code="viewjob.string28" /></i>&nbsp;&nbsp;
							<input type="checkbox"
							onclick="toggleConfirmInvoiceInstructions(this);" />
						</span>
						<div class="clear">&nbsp;</div>
					</c:if>
					<table class="default4" id="invoicetable"
						summary="This table displays all invoices for the selected job">
						<thead>
							<tr>
								<td colspan="6"><spring:message code="viewjob.invoiforjob" /> (${job.invoiceLinks.size()})</td>
							</tr>
							<tr>
								<th id="invno" scope="col"><spring:message
										code="viewjob.invoiceno" /></th>
								<th id="type" scope="col"><spring:message
										code="viewjob.invoicetype" /></th>
								<th id="company" scope="col"><spring:message code="company" /></th>
								<th id="createby" scope="col"><spring:message
										code="viewjob.createdby" /></th>
								<th id="createon" scope="col"><spring:message
										code="viewjob.createdon" /></th>
								<th id="status" scope="col"><spring:message
										code="jobresults.stat" /></th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="6">
									<div class="float-right">
										&nbsp;
										<span <c:if test="${confirmInstructions}">class="hid"</c:if>
											id="invoicingLinks">
											<c:if test="${companyOnStop}">
												<span class="attention"><spring:message
														code="viewjob.string29" />.</span>
												<cwms:securedLink permission="INVOICE_CREATE"
													classAttr="mainlink" parameter="?jobid=${job.jobid}&type=p">
													<spring:message code="viewjob.previewproformainvoice" />
												</cwms:securedLink>
												&nbsp;Other invoicing options:&nbsp;
											</c:if>
											<c:if test="${monthlyinvoicesystemdefault}">
												<cwms:securedLink permission="INVOICE_MONTHLY_PREPARE"
													classAttr="mainlink"
													parameter="?coid=${job.con.sub.comp.coid}&jobid=${job.jobid}">
													<spring:message code="viewjob.string32" />
												</cwms:securedLink>
											</c:if>
											<%-- By default, the system will pick the correct type of invoice, no type parameter needed --%>
											<c:set var="invoiceTypeOverride" value="" />
											<c:if test='${job.type == "VALVE"}'>
												<c:set var="invoiceTypeOverride" value="v" />
											</c:if>
												<cwms:securedLink permission="INVOICE_CREATE"
												classAttr="mainlink"
												parameter="?jobid=${job.jobid}&type=${invoiceTypeOverride}&coid=${job.con.sub.comp.coid}">
													<spring:message code="viewjob.previnvoi" />
												</cwms:securedLink>
										</span>
									</div>
									<!-- clear floats and restore page flow -->
									<div class="clear"></div>
								</td>
							</tr>
						</tfoot>
						<c:choose>
							<c:when test="${job.invoiceLinks.size() < 1}">
								<tbody>
									<tr>
										<td colspan="6" class="bold center"><spring:message
												code="viewjob.noinvforjob" /></td>
									</tr>
								</tbody>
							</c:when>
							<c:otherwise>
								<c:set var="rowcount" value="0" />
								<tbody>
									<c:forEach var="invref" items="${job.invoiceLinks}">
										<tr>
											<td>
												<cwms:securedLink permission="INVOICE_VIEW"
													classAttr="mainlink" parameter="?id=${invref.invoice.id}">
													${invref.invoice.invno}
												</cwms:securedLink>
											</td>
											<td><cwms:besttranslation
													translations="${invref.invoice.type.nametranslation}" /></td>
											<td>${invref.invoice.comp.coname}</td>
											<td>${invref.invoice.createdBy.name}</td>
											<td><fmt:formatDate value="${invref.invoice.regdate}"
													type="date" dateStyle="short" />
											
											
											
											
											
											
											
											<td><cwms:besttranslation
													translations="${invref.invoice.status.descriptiontranslations}" /></td>
										</tr>
										<c:set var="rowcount" value="${rowcount + 1}" />
									</c:forEach>
								</tbody>
							</c:otherwise>
						</c:choose>
					</table>
					<table class="default4" id="jobiteminvoicetable">
						<thead>
							<tr>
								<td colspan="9"><spring:message code="jobcost.jobitems" /> (${job.items.size()})</td>
							</tr>
							<tr>
								<th id="itemno" scope="col"><spring:message
										code="viewjob.itemno" /></th>
								<th id="status" scope="col"><spring:message code="status" /></th>
								<th id="customerinvoices" scope="col"><spring:message
										code="invoice.customerinvoices" /></th>
								<th id="internalinvoices" scope="col"><spring:message
										code="invoice.internalinvoices" /></th>
								<th id="reason" scope="col"><spring:message
										code="invoice.notinvoicedreason" /></th>
								<th id="periodicinvoice" scope="col"><spring:message
										code="invoice.periodicinvoice" /></th>
								<th id="reviewby" scope="col"><spring:message
										code="viewjob.reviewby" /></th>
								<th id="reviewdate" scope="col"><spring:message
										code="viewjob.reviewdate" /></th>
								<th id="comments" scope="col"><spring:message
										code="comments" /></th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="9">
							<cwms:securedLink collapse="" permission="JOB_INVOICING_EDIT"
										classAttr="mainlink" parameter="?jobid=${job.jobid}">
								<spring:message code="viewjob.editjobiteminvoicing" />
							</cwms:securedLink>							
							</td>
							</tr>
						</tfoot>
						<tbody>
							<c:forEach var="jobitem" items="${job.items}">
								<tr>
									<td><links:jobitemLinkInfo jobItemNo="${jobitem.itemNo}"
											jobItemId="${jobitem.jobItemId}"
											jobItemCount="${job.items.size()}"
											contractReviewed="${contractReviewedJobItemIds.contains(jobitem.jobItemId)} " />
									</td>
									<td>
										<t:showTranslationOrDefault
											translations="${jobitem.state.translations}"
											defaultLocale="${defaultlocale}" />
									</td>
									<td>
										<c:forEach var="invoiceItem" items="${jobitem.invoiceItems}"
											varStatus="loopInvoiceItem">
											<c:if test="${invoiceItem.invoice.isCustomerInvoice()}">
												<c:if test="${!loopInvoiceItem.first}">,</c:if>
												<cwms:securedLink permission="INVOICE_VIEW"
													classAttr="mainlink"
													parameter="?id=${invoiceItem.invoice.id}">
													${invoiceItem.invoice.invno}
												</cwms:securedLink>
											</c:if>
										</c:forEach>
									</td>
									<td>
										<c:forEach var="invoiceItem" items="${jobitem.invoiceItems}"
											varStatus="loopInvoiceItem">
											<c:if test="${invoiceItem.invoice.isInternalInvoice()}">
												<c:if test="${!loopInvoiceItem.first}">,</c:if>
												<cwms:securedLink permission="INVOICE_VIEW"
													classAttr="mainlink"
													parameter="?id=${invoiceItem.invoice.id}">
													${invoiceItem.invoice.invno}
												</cwms:securedLink>
											</c:if>
										</c:forEach>
									</td>
									<c:choose>
										<c:when test="${not empty jobitem.notInvoiced}">
											<td>${jobitem.notInvoiced.reason.getMessage()}</td>
											<td>
												<c:if
													test="${not empty jobitem.notInvoiced.periodicInvoice}">
													<cwms:securedLink permission="INVOICE_VIEW"
														classAttr="mainlink"
														parameter="?id=${jobitem.notInvoiced.periodicInvoice.id}">
														${jobitem.notInvoiced.periodicInvoice.invno}
													</cwms:securedLink>
												</c:if>
											</td>
											<td>${jobitem.notInvoiced.contact.name}</td>
											<td><fmt:formatDate value="${jobitem.notInvoiced.date}"
													type="date" dateStyle="short" /> </td>
											<td>${jobitem.notInvoiced.comments}</td>
										</c:when>
										<c:otherwise>
											<td colspan="5"></td>
										</c:otherwise>
									</c:choose>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<table class="default4" id="expenseiteminvoicetable">
						<thead>
							<tr>
								<td colspan="8"><spring:message code="viewjob.jobservices" /> (${job.expenseItems.size()})</td>
							</tr>
							<tr>
								<th id="expenseitemno" scope="col"><spring:message
										code="viewjob.itemno" /></th>
								<th id="expensemodel" scope="col"><spring:message
										code="viewjob.service" /></th>
								<th id="expenseinvoices" scope="col"><spring:message
										code="invoice.invoices" /></th>
								<th id="reason" scope="col"><spring:message
										code="invoice.notinvoicedreason" /></th>
								<th id="periodicinvoice" scope="col"><spring:message
										code="invoice.periodicinvoice" /></th>
								<th id="reviewby" scope="col"><spring:message
										code="viewjob.reviewby" /></th>
								<th id="reviewdate" scope="col"><spring:message
										code="viewjob.reviewdate" /></th>
								<th id="comments" scope="col"><spring:message
										code="comments" /></th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="8">
									<cwms:securedLink collapse="" permission="JOB_INVOICING_EDIT"
										classAttr="mainlink" parameter="?jobid=${job.jobid}">
										<spring:message code="viewjob.editjobiteminvoicing" />
									</cwms:securedLink>
								</td>
							</tr>
						</tfoot>
						<tbody>
							<c:forEach var="expenseItem" items="${job.expenseItems}">
								<tr>
									<td>
										<a href=""
										onclick="event.preventDefault(); switchMenuFocus(menuElements, 'expenses-tab', false); changeServiceModelSearch(-1,${job.jobid});">
											${expenseItem.itemNo}
										</a>
									</td>
									<td>
										<cwms:besttranslation
											translations="${expenseItem.model.nameTranslations}" />
									</td>
									<td>
										<c:forEach var="invoiceItem"
											items="${expenseItem.invoiceItems}"
											varStatus="loopInvoiceItem">
											<c:if test="${!loopInvoiceItem.first}">,</c:if>
											<cwms:securedLink permission="INVOICE_VIEW"
												classAttr="mainlink"
												parameter="?id=${invoiceItem.invoice.id}">
												${invoiceItem.invoice.invno}
											</cwms:securedLink>
										</c:forEach>
									</td>
									<c:choose>
										<c:when test="${not empty expenseItem.notInvoiced}">
											<td>${expenseItem.notInvoiced.reason.getMessage()}</td>
											<td>
												<c:if
													test="${not empty expenseItem.notInvoiced.periodicInvoice}">
													<cwms:securedLink permission="INVOICE_VIEW"
														classAttr="mainlink"
														parameter="?id=${expenseItem.notInvoiced.periodicInvoice.id}">
														${expenseItem.notInvoiced.periodicInvoice.invno}
													</cwms:securedLink>
												</c:if>
											</td>
											<td>${expenseItem.notInvoiced.contact.name}</td>
											<td><fmt:formatDate
													value="${expenseItem.notInvoiced.date}" type="date"
													dateStyle="short" /> </td>
											<td>${expenseItem.notInvoiced.comments}</td>
										</c:when>
										<c:otherwise>
											<td colspan="5"></td>
										</c:otherwise>
									</c:choose>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<!-- end of invoice section -->
				<!-- this section contains all invoice information -->
				<div id="purchord-tab" class="hid">
					<cwms-job-po-table job-id="${job.jobid}"> </cwms-job-po-table>
				</div>
				<!-- end of purchase order section -->
				<!-- this section contains all contract review information -->
				<div id="contractrev-tab" class="hid">
						<cwms-contract-reviews-table job-id="${job.jobid}"> <cwms-contract-reviews-table>								
 				
				
				
				
				
				
				</div>
				<!-- end of contract review section -->
				<!-- this section contains all quotation costs information -->
				<div id="costs-tab" class="hid">
					<!-- tabbed submenu to change between active/inactive contacts/addresses -->
					<ul class="subnavtab">
						<li>
							<a href="#" id="jcgeneral-link"
							onclick="switchMenuFocus(menuElements3, 'jcgeneral-tab', false); return false; "
							class="selected">
								<spring:message code="viewjob.costingsgeneral" />
							</a>
						</li>
						<li>
							<a href="#" id="jcbreakdown-link"
							onclick="switchMenuFocus(menuElements3, 'jcbreakdown-tab', false); return false; ">
								<spring:message code="viewjob.costingsbreakd" />
							</a>
						</li>
					</ul>
					<!-- div to hold all content which is to be changed using the tabbed submenu -->
					<div class="tab-box">
						<!-- div displays all general costing info -->
						<div id="jcgeneral-tab">
							<div style="border: 1px solid gray; padding: 5px;">
								<cwms:securedLink permission="JOB_QUOTATION"
									classAttr="mainlink" parameter="?jobid=${job.jobid}">
									<spring:message code="viewjob.string37" />
								</cwms:securedLink>
							</div>
							<!-- this component 'cwms-jcosting-items' displays all job costings with the possibility to display job costing items -->
							<cwms-jcosting-items job-id="${job.jobid}"></cwms-jcosting-items>
							<cwms:securedLink permission="JOB_COSTING_CREATE"
								classAttr="mainlink-float" parameter="?jobid=${job.jobid}">
								             <spring:message code="viewjob.createcost" />
							</cwms:securedLink>
							<br>
							<!-- this table displays all items that have no costs -->
							<table id="uncosteditemlist" class="tablesorter"
								summary='<spring:message code="viewjob.string40"/>'>
								<thead>
									<tr>
										<td id="itemQuantityRow" colspan="7"><spring:message
												code="viewjob.string41" /> (${nocostjobitems.size()})</td>
									</tr>
									<tr>
										<th id="item" scope="col"><spring:message
												code="accessoriesanddefects.item" /></th>
										<th id="instrument" scope="col"><spring:message
												code="accessoriesanddefects.instrument" /></th>
										<th id="serial" scope="col"><spring:message
												code="serialno" /></th>
										<th id="plant" scope="col"><spring:message code="plantno" /></th>
										<th id="caltype" scope="col"><spring:message
												code="jobresults.caltype" /></th>
										<th id="status" scope="col"><spring:message
												code="jobresults.stat" /></th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<td colspan="6">&nbsp;</td>
									</tr>
								</tfoot>
								<tbody>
									<c:choose>
										<c:when test="${nocostjobitems.size() < 1}">
											<tr>
												<td colspan="6" class="bold center">
													<spring:message code="viewjob.string15" />
												</td>
											</tr>
										</c:when>
										<c:otherwise>
											<c:set var="rowcount1" value="0" />
											<c:forEach var="i" items="${nocostjobitems}">
												<c:set var="bgcolor"
													value="${i.calType.serviceType.displayColour}" />
												<c:if test="${i.turn <= fastTrackTurn}">
													<c:set var="bgcolor"
														value="${i.calType.serviceType.displayColourFastTrack}" />
												</c:if>
												<tr style=" background-color: ${bgcolor}"
													onclick="window.location.href='${defaultURI}?jobitemid=${i.jobItemId}';"
													onmouseover=" $j(this).addClass('hoverrow'); "
													onmouseout=" $j(this).removeClass('hoverrow'); ">
													<td><links:jobitemLinkInfo jobItemNo="${i.itemNo}"
															jobItemId="${i.jobItemId}"
															contractReviewed="${contractReviewedJobItemIds.contains(i.jobItemId)}" />
													</td>
													<td><links:instrumentLinkDWRInfo
															displayCalTimescale="${false}" displayName="${true}"
															instrument="${i.inst}" caltypeid="${i.calType.calTypeId}"
															rowcount="${rowcount1}" displayBarcode="${false}" /></td>
													<td>${i.inst.serialno}</td>
													<td>${i.inst.plantno}</td>
													<td class="text-center">
														<t:showTranslationOrDefault
															translations="${i.calType.serviceType.shortnameTranslation}"
															defaultLocale="${defaultlocale}" />
													</td>
													<td><t:showTranslationOrDefault
															translations="${i.state.translations}"
															defaultLocale="${defaultlocale}" /></td>													
												</tr>
												<c:set var="rowcount1" value="${rowcount1 + 1}" />
											</c:forEach>
										</c:otherwise>
									</c:choose>
								</tbody>
							</table>
							<!-- end table displays all items that have no costs -->
							<!-- this table displays all linked quotes -->																		
							<table class="default2" id="linkedquotes"
								summary="This table displays all linked quotes">
								<thead>
									<tr>
										<td colspan="6"><spring:message
												code="viewjob.linkedquotes" /> (<span id="linkedCount">${job.linkedQuotes.size()}</span>)</td>
									</tr>
									<tr>
										<th class="qno" scope="col"><spring:message
												code="viewjob.quoteno" /></th>
										<th class="comp" scope="col"><spring:message
												code="company" /></th>
										<th class="cont" scope="col"><spring:message
												code="changeclient.contact" /></th>
										<th class="linkon" scope="col"><spring:message
												code="viewjob.linkedon" /></th>
										<th class="linkby" scope="col"><spring:message
												code="viewjob.linkedby" /></th>
										<th class="rem" scope="col"><spring:message
												code="accessoriesanddefects.delete" /></th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<td colspan="6">
											<c:set var="linkQuot"> event.preventDefault(); linkQuotation(${job.jobid}, ${job.con.sub.comp.coid}, ${resultsYearFilter}, false, false); </c:set>
											<cwms:securedJsLink permission="QUOTATION_LINK"
												classAttr="mainlink-float domthickbox" onClick="${linkQuot}">
												<spring:message code="viewjob.linkquotat" />
											</cwms:securedJsLink>
										</td>
									</tr>
								</tfoot>
								<tbody>
									<c:choose>
										<c:when test="${job.linkedQuotes.size() < 1}">
											<tr>
												<td colspan="6" class="bold center"><spring:message
														code="viewjob.string42" /></td>
											</tr>
										</c:when>
										<c:otherwise>
											<c:forEach var="lq" items="${job.linkedQuotes}">
												<c:set var="quote" value="${lq.quotation}" />
												<tr id="quote${lq.id}">
													<td><links:showQuotationLink quotation="${quote}" /></td>
													<td>${quote.contact.sub.comp.coname}</td>
													<td>${quote.contact.name}</td>
													<td><fmt:formatDate type="date" dateStyle="SHORT"
															value="${lq.linkedOn}" /></td>
													<td>${lq.linkedBy.name}</td>
													<td class="center">
														
														<cwms:securedJsLink permission="QUOTE_LINK_REMOVE_JOB"
															classAttr="mainlink-float"
															onClick="removeJobQuoteLink(${lq.id}); return false;">
															<img src="img/icons/delete.png" width="16" height="16"
																alt='<spring:message code="viewjob.string42"/>'
																title='<spring:message code="viewjob.string42"/>' />
														</cwms:securedJsLink>
													</td>
												</tr>
											</c:forEach>
										</c:otherwise>
									</c:choose>
								</tbody>
							</table>
							<!-- end of table which displays all linked quotes -->
						</div>
						<!-- end of div which displays all general costing info -->
						<!-- div displays job costing breakdown -->
						<div id="jcbreakdown-tab" class="hid">
							<!-- this table displays a breakdown of all job items and the most recent costing they appear on -->
							<table class="default4 jobItemCostSummary"
								summary='<spring:message code="viewjob.string43"/>'>
								<thead>
									<tr>
										<td colspan="5"><spring:message code="viewjob.string44" /> (${job.items.size()})</td>
									</tr>
									<tr>
										<th class="itemno" scope="col"><spring:message
												code="viewjob.itemno" /></th>
										<th class="inst" scope="col"><spring:message
												code="accessoriesanddefects.instrument" /></th>
										<th class="caltype" scope="col"><spring:message
												code="jobresults.caltype" /></th>
										<th class="costing" scope="col"><spring:message
												code="viewjob.recentcosting" /></th>
										<th class="cost" scope="col"><spring:message
												code="viewjob.cost" /></th>
									</tr>
								</thead>
								<tbody>
									<c:choose>
										<c:when test="${job.items.size() < 1}">
											<tr>
												<td colspan="5" class="bold center"><spring:message
														code="viewjob.string45" /></td>
											</tr>
										</c:when>
										<c:otherwise>
											<c:set var="rowcount" value="0" />
											<c:forEach var="ji" items="${job.items}">
												<c:set var="latestCost" value="0.00" />
												<c:set var="itemCosted" value="${false}" />
												<c:set var="bgcolor"
													value="${ji.calType.serviceType.displayColour}" />
												<c:if test="${ji.turn <= fastTrackTurn}">
													<c:set var="bgcolor"
														value="${ji.calType.serviceType.displayColourFastTrack}" />
												</c:if>
												<tr style=" background-color: ${bgcolor}"
													onclick="window.location.href='${defaultURI}?jobitemid=${ji.jobItemId}';">
													<td class="itemno">
														<links:jobitemLinkInfo jobItemNo="${ji.itemNo}"
															jobItemId="${ji.jobItemId}"
															contractReviewed="${contractReviewedJobItemIds.contains(ji.jobItemId)}" />
													</td>
													<td class="inst">
													    <cwms:showmodel instrumentmodel="${ji.inst.model}" />
													</td>
													<td class="caltype">
														<t:showTranslationOrDefault
															translations="${ji.calType.serviceType.shortnameTranslation}"
															defaultLocale="${defaultlocale}" />
													</td>
													<c:choose>
														<c:when test="${not empty ji.jobCostingItems}">
															<td class="costing">
																<c:set var="jci"
																	value="${ji.jobCostingItems.iterator().next()}" />
																<links:jobcostingLinkDWRInfo
																	jobcosting="${jci.jobCosting}" rowcount="${rowcount}"
																	showno="${true}" />
															</td>
															<td class="cost">
																${currency.currencySymbol}${jci.finalCost}
															</td>
														</c:when>
														<c:otherwise>
															<td class="costing">
																<span class="attention">
																	<spring:message code="viewjob.notyetcosted" />
																</span>
															</td>
															<td class="cost">
															</td>
														</c:otherwise>
													</c:choose>
												</tr>
												<c:set var="rowcount" value="rowcount + 1" />
											</c:forEach>
										</c:otherwise>
									</c:choose>
								</tbody>
							</table>
							<!-- end of table which displays all job costings -->
							<table class="default4 jobItemCostSummary"
								summary='<spring:message code="viewjob.string46"/>'>
								<tbody>
									<tr class="nobord">
										<td class="totalsum cost2pcTotal bold"><spring:message
												code="viewjob.netttotal" />:</td>
										<td class="totalamount cost2pcTotal bold">
											${currency.currencySymbol}${jcTotalCost}
										</td>
									</tr>
								</tbody>
							</table>
							<table class="default4 jobItemCostSummary"
								summary='<spring:message code="viewjob.string47"/>'>
								<tbody>
									<tr class="nobord">
										<td class="totalsum cost2pcTotal bold"><spring:message
												code="viewjob.vat" />:</td>
										<td class="totalamount cost2pcTotal bold">
											${currency.currencySymbol}${jcVatValue}
										</td>
									</tr>
								</tbody>
							</table>
							<table class="default4 jobItemCostSummary"
								summary="This table displays the costing net total value">
								<tbody>
									<tr class="nobord">											
										<td class="totalsum cost2pcTotal bold"><spring:message
												code="viewjob.total" />:</td>
										<td class="totalamount cost2pcTotal bold">
											${currency.currencySymbol}${jcFinalCost}
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<!-- end of div which displays job costing breakdown -->
					</div>
					<!-- end of div to hold all tabbed submenu content -->												
				</div>
				<!-- end of job costing section -->
				<!-- this section displays all emails for the selected job -->
				<div id="email-tab" class="hid">
					<t:showEmails entity="${job}" entityId="${job.jobid}"
						entityName="Job" sc="${sc}" />
				</div>
				<!-- end of email section -->
				<!-- this section displays all batch calibrations for the selected job -->
				<div id="batchcal-tab" class="hid">
					<table class="default2" id="batchcalibrations"
						summary='<spring:message code="viewjob.string48"/>'>
						<thead>
							<tr>
								<td colspan="6"><spring:message code="viewjob.batchcals" /> (${job.batchCals.size()})</td>
							</tr>
							<tr>
								<th class="batchid" scope="col"><spring:message
										code="viewjob.batchid" /></th>
								<th class="starton" scope="col"><spring:message
										code="viewjob.startedon" /></th>
								<th class="tech" scope="col"><spring:message
										code="viewjob.technician" /></th>
								<th class="process" scope="col"><spring:message
										code="viewjob.process" /></th>
								<th class="cals" scope="col"><spring:message
										code="viewjob.calibrations" /></th>
								<th class="view" scope="col"><spring:message
										code="viewjob.view" /></th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="6">
									<cwms:securedLink permission="BATCH_CALIBRATION_NEW"
										classAttr="mainlink-float" parameter="?jobid=${job.jobid}">
										<spring:message code="viewjob.creatnewbatchcal" />
									</cwms:securedLink>
								</td>
							</tr>
						</tfoot>
						<tbody>
							<c:choose>
								<c:when test="${job.batchCals.size() > 0}">
									<c:set var="rowcount" value="0" />
									<c:forEach var="batch" items="${job.batchCals}">
										<c:set var="rowClass" value="even" />
										<c:if test="${rowcount%2 == 1}">
											<c:set var="rowClass" value="odd" />
										</c:if>
										<tr class="${rowClass}">
											<td>${batch.id}</td>
											<td><fmt:formatDate type="both" dateStyle="SHORT"
													timeStyle="SHORT" value="${batch.beginTime}" /></td>
											<td>${batch.owner.name}</td>
											<td>${batch.process.process}</td>
											<td>
												${batch.cals.size()}
												<a href="#" id="calLink${rowcount}"
												onclick="toggleCalibrations(${rowcount}, this.id); return false; ">
													<img src="img/icons/items.png" width="16" height="16"
													alt='<spring:message code="viewjob.imgviewcals"/>'
													title='<spring:message code="viewjob.imgviewcals"/>'
													class="image_inline" />
												</a>
											</td>
											<td>
												<cwms:securedLink permission="BATCH_CALIBRATION_VIEW"
													classAttr="mainlink" parameter="?batchid=${batch.id}">
													<spring:message code="viewjob.view" />
												</cwms:securedLink>
											</td>
										</tr>
										<tr id="batchchild${rowcount}" class="${rowClass} hid">
											<td colspan="6">
												<div style="display: none;">
													<table class="child_table">
														<thead>
															<tr>
																<td colspan="7"><spring:message
																		code="viewjob.calibrations" /> (${batch.cals.size()})</td>
															</tr>
															<tr>
																<th scope="col" class="pos"><spring:message
																		code="viewjob.pos" /></th>
																<th scope="col" class="proc"><spring:message
																		code="jobresults.capability" /></th>
																<th scope="col" class="caltype"><spring:message
																		code="jobresults.caltype" /></th>
																<th scope="col" class="item"><spring:message
																		code="viewjob.itemno" /></th>
																<th scope="col" class="inst"><spring:message
																		code="accessoriesanddefects.instrument" /></th>
																<th scope="col" class="serial"><spring:message
																		code="serialno" /></th>
																<th scope="col" class="plant"><spring:message
																		code="plantno" /></th>
															</tr>
														</thead>
														<tfoot>
															<tr>
																<td colspan="7">&nbsp;</td>
															</tr>
														</tfoot>
														<tbody>
															<c:set var="calIndicator" value="" />
															<c:forEach var="cal" items="${batch.cals}">
																<c:forEach var="link" items="${cal.links}">
																	<c:set var="bgcolor"
																		value="${link.ji.calType.serviceType.displayColour}" />
																	<c:if test="${link.ji.turn <= fastTrackTurn}">
																		<c:set var="bgcolor"
																			value="${link.ji.calType.serviceType.displayColourFastTrack}" />
																	</c:if>
																	<tr style=" background-color: ${bgcolor}">
																		<c:if test="${calIndicator != cal.id}">
																			<c:set var="calIndicator" value="${cal.id}" />
																			<td class="center" rowspan="${cal.links.size()}">
																				${cal.batchPosition}
																			</td>																					
																			<td class="center" rowspan="${cal.links.size()}">
																				<links:procedureLinkDWRInfo displayName="${false}"
																					capability="${cal.capability}" rowcount="0" />
																			</td>
																			<td class="center" rowspan="${cal.links.size()}">
																				<t:showTranslationOrDefault
																					translations="${cal.calType.serviceType.shortnameTranslation}"
																					defaultLocale="${defaultlocale}" />
																			</td>
																		</c:if>
																		<td class="center">${link.ji.itemNo}</td>
																		<td><cwms:showmodel
																				instrumentmodel="${link.ji.inst.model}" /></td>
																		<td>${link.ji.inst.serialno}</td>
																		<td>${link.ji.inst.plantno}</td>
																	</tr>
																</c:forEach>
															</c:forEach>
														</tbody>
													</table>
												</div>
											</td>
										</tr>
										<c:set var="rowcount" value="${rowcount + 1}" />
									</c:forEach>
								</c:when>
								<c:otherwise>
									<tr>
										<td colspan="6" class="center bold"><spring:message
												code="viewjob.string49" /></td>
									</tr>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
				</div>
				<!-- end of batch calibration section -->
				<!-- this div clears floats and restores page flow -->
				<div class="clear"></div>
				<!-- this section displays job sheet form for the selected job -->
				<div id="jobsheet-tab" class="hid">
				<form:form action="birtjobsheet.htm" target="_blank" method="get">
					<input type="hidden" name="jobid" value="${job.jobid}" /> 
					<fieldset>
						<legend>
							<spring:message code="viewjob.string50" />
						</legend>
								<li>
									<label><spring:message code="options" />:</label>
									<input type="radio" name="pagegroup" value="NONE" />
									<spring:message code="viewjob.nopagebreaks" />
									<input type="checkbox" name="subdiv"
								value="${allocatedSubdiv.key}" checked="checked" />
									<spring:message code="viewjob.currentsubdivisiononly" />
									<c:out
									value=" (${allocatedCompany.value} - ${allocatedSubdiv.value}) " />
									</select>
								</li>
								<li>
									<label><spring:message code="viewjob.groupby" />
										<c:out value=" (1):" /></label>
									<select name="group1">
										<c:forEach var="group" items="${jobItemSheetGroupValues}">
											<option ${group == 'DEPARTMENT' ? 'selected' : ''}>${group}</option>
										</c:forEach>
									</select>
									<input type="radio" name="pagegroup" value="GROUP_1"
								checked="checked" />
									<spring:message code="viewjob.pagebreakongroup" arguments="1" />
								</li>
								<li>
									<label><spring:message code="viewjob.groupby" />
										<c:out value=" (2):" /></label>
									<select name="group2">
										<c:forEach var="group" items="${jobItemSheetGroupValues}">
											<option ${group == 'CAPABILITY_CATEGORY' ? 'selected' : ''}>${group}</option>
										</c:forEach>
									</select>
									<input type="radio" name="pagegroup" value="GROUP_2" />
									<spring:message code="viewjob.pagebreakongroups"
									arguments="1+2" />
								</li>
								<li>
									<label><spring:message code="viewjob.groupby" />
										<c:out value=" (3):" /></label>
									<select name="group3">
										<c:forEach var="group" items="${jobItemSheetGroupValues}">
											<option ${group == 'SUB_FAMILY' ? 'selected' : ''}>${group}</option>
										</c:forEach>
									</select>
									<input type="radio" name="pagegroup" value="GROUP_3" />
									<spring:message code="viewjob.pagebreakongroups"
									arguments="1+2+3" />
								</li>
								<li>
									<label><spring:message code="viewjob.orderby" />:</label>
									<select name="order">
										<c:forEach var="order" items="${jobItemSheetOrderValues}">
											<option>${order}</option>
										</c:forEach>
									</select>
								</li>
						<ol>
							<li>
								<label><spring:message code="viewjob.generate" />:</label>
								<input type="submit" name="js_submit" id="js_submit"
									value='<spring:message code="viewjob.generate"/> - <spring:message code="pageversion.new"/>' />
							</li>
							<li>
								<label><spring:message code="viewjob.generate" />:</label>
								<input type="button" name="js_submit" id="js_submit"
									value='<spring:message code="viewjob.generate"/> - <spring:message code="pageversion.old"/>'
									onclick=" event.preventDefault(); generateOldJobSheet(${job.jobid}); " />
							</li>
						</ol>	
					</fieldset>
					</form:form>
					<form:form action="generatejobitemlist.htm" target="_blank"
						method="get">
						<input type="hidden" name="jobid" value="${job.jobid}" /> 
						<fieldset>
							<legend>
								<spring:message code="viewjob.generatejobitemlist" />
							</legend>
							<ol>											
								<li>
									<label><spring:message code="options" />:</label>
									<input type="radio" name="pagegroup" value="NONE" />
									<spring:message code="viewjob.nopagebreaks" />
									<input type="checkbox" name="subdiv"
									value="${allocatedSubdiv.key}" checked="checked" />
									<spring:message code="viewjob.currentsubdivisiononly" />
									<c:out
										value=" (${allocatedCompany.value} - ${allocatedSubdiv.value}) " />
									</select>
								</li>
								<li>
									<label><spring:message code="jicalloffitem.currstatus" />:</label>
									<input type="radio" name="dept" value="0" checked="checked" />
									<spring:message code="all" />
									<c:forEach var="dept" items="${activeDeptDto}">
										<input type="radio" name="dept" value="${dept.key}" />
										<c:out value="${dept.value}" />
									</c:forEach>
								</li>
								<li>
									<label><spring:message code="viewjob.groupby" />
										<c:out value=" (1):" /></label>
									<select name="group1">
										<c:forEach var="group" items="${jobItemSheetGroupValues}">
											<option ${group == 'DEPARTMENT' ? 'selected' : ''}>${group}</option>
										</c:forEach>
									</select>
									<input type="radio" name="pagegroup" value="GROUP_1"
									checked="checked" />
									<spring:message code="viewjob.pagebreakongroup" arguments="1" />
								</li>
								<li>
									<label><spring:message code="viewjob.groupby" />
										<c:out value=" (2):" /></label>
									<select name="group2">
										<c:forEach var="group" items="${jobItemSheetGroupValues}">
											<option ${group == 'CAPABILITY_CATEGORY' ? 'selected' : ''}>${group}</option>
										</c:forEach>
									</select>
									<input type="radio" name="pagegroup" value="GROUP_2" />
									<spring:message code="viewjob.pagebreakongroups"
										arguments="1+2" />
								</li>
								<li>
									<label><spring:message code="viewjob.groupby" />
										<c:out value=" (3):" /></label>
									<select name="group3">
										<c:forEach var="group" items="${jobItemSheetGroupValues}">
											<option ${group == 'SUB_FAMILY' ? 'selected' : ''}>${group}</option>
										</c:forEach>
									</select>
									<input type="radio" name="pagegroup" value="GROUP_3" />
									<spring:message code="viewjob.pagebreakongroups"
										arguments="1+2+3" />
								</li>
								<li>
									<label><spring:message code="viewjob.orderby" />:</label>
									<select name="order">
										<c:forEach var="order" items="${jobItemSheetOrderValues}">
											<option>${order}</option>
										</c:forEach>
									</select>
								</li>
								<li>
									<label><spring:message code="viewjob.generate" />:</label>
									<input type="submit" name="js_submit_l" id="js_submit_l"
									value='<spring:message code="viewjob.generate"/>' />
								</li>
							</ol>
						</fieldset>
					</form:form>
					<security:authorize
						access="hasAuthority('EXPORT_VDI_CALIBRATIONS')">
                    <spring:message code="viewjob.vdiExport"
							text="VDI export" var="legend" />
                    <spring:message code="viewjob.generate"
							text="Generate" var="generate" />
                     <fieldset>
                        <legend>${legend}</legend>
                        <ol>
                            <li>
                                <label>${generate}</label>
                                <a download="VDI_${job.jobno}.zip"
									href="vdi/downloadJob.zip?jobId=${job.jobid}">
                                    <button>${generate}</button>
                                 <a />
                            
								
								
								
								
								
								</li>
                        </ol>
                    </fieldset>
                    </security:authorize>
				</div>
				<!-- end of job sheet section -->
				<!-- this div clears floats and restores page flow -->
				<div class="clear"></div>
				
				<c:if test="${ !empty job.jobCourier }">
					<!-- this section displays job courier details for the selected job -->
					<div id="jobcourier-tab" class="hid">
						<fieldset>
							<legend>
								<spring:message code="viewjob.string53" />
							</legend>
							<input type="hidden" id="jobCourierId"
								value="${job.jobCourier.id}" />
							<input type="hidden" id="jobid" value="${job.jobid}" />
							<div class="displaycolumn">
							<ol>
								<li>
										<label><spring:message code="addjob.carrier" />:</label>
										<select id="jobCourierName" disabled="true">
											<c:forEach items="${couriers}" var="courier">
												<option value="${courier.courierid}"
													${courier.name eq job.jobCourier.courier.name ? 'selected':''}>${courier.name}</option>
											</c:forEach>
										</select>
									</li>
									<li>
										<label><spring:message code="addjob.trackingnumber" />:</label>
										<input type="text" id="jobCourierTrackingNumber"
										value="${job.jobCourier.trackingNumber}" disabled="true" />
									
								</li>
							
								</ol>
								<button onclick="updateJobCourier($j(this))">Update</button>
								</div>
								<div class="displaycolumn">
							</div>				
						</fieldset>
					</div>
					<!-- end of job courier details section -->
					<!-- this div clears floats and restores page flow -->
					<div class="clear"></div>
				</c:if>
			</div>
			<!-- end of infobox -->
			<!-- this section contains all job notes -->
			<t:showTabbedNotes entity="${job}"
				privateOnlyNotes="${privateOnlyNotes}" noteTypeId="${job.jobid}"
				noteType="JOBNOTE" />
			<!-- end of job notes section -->
		</div>
		<!-- end of main job div -->
	</jsp:body>

</t:crocodileTemplate>
<cwms-upload-certificates job-id="${job.jobid}"></cwms-upload-certificates>
<cwms-jobitem-feedback-overlay coid="${job.con.sub.comp.coid}"
	jobId="${job.jobid}" currentContactEmail="${contact.email}">
</cwms-jobitem-feedback-overlay>
<cwms-upload-file system-component-id="${sc.componentId}"
	entity-id1="${job.jobno}" entity-id2="" files-name="${filesName}"
	rename-to="" web="false"> </cwms-upload-file>
<cwms-upload-certificates job-id="${job.jobid}"></cwms-upload-certificates>
<cwms-overlay id="addQuickItem"> <cwms-translate
	slot="cwms-overlay-title" code="error">Error</cwms-translate>
<div slot="cwms-overlay-body">
	<center>
		<span> <cwms-translate slot="cwms-overlay-title"
				code="viewjob.quickitems.already.on.list"></cwms-translate>
		</span><br> <br>
	</center>
</div>
</cwms-overlay>
<cwms-overlay id="printProgress"> <cwms-translate
	slot="cwms-overlay-title" code="tools.labelprinting">
Labels printing</cwms-translate>
<div slot="cwms-overlay-body">
	<center>
		<img src="img/icons/ajax-loader.gif" width="31" height="31" alt="" />
	</center>
</div>
</cwms-overlay>
<cwms-preview-email-details>
<div slot="rich-text">
	<textarea></textarea>
</div>
</cwms-preview-email-details>


<cwms-overlay id="submitData">
	<cwms-translate slot="cwms-overlay-title" code="createdelnote.wait" >Loading...please wait</cwms-translate>
    <div slot="cwms-overlay-body">
    	<center>
            <img src="img/icons/ajax-loader.gif" width="31" height="31" alt="" />
        </center>
    </div>
 </cwms-overlay>
