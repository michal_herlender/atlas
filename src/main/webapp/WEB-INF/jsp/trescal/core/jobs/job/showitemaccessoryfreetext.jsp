<%-- File name: /trescal/core/jobs/job/showitemaccessoryfreetext.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs"%>

<c:choose>
	<c:when test="${jobItem.accessoryFreeText == null}">
		<spring:message code="accessoriesanddefects.string2"/>
		<div id="accFreeButtons">
			<a id="itemAccFTAdd" href="" onclick="event.preventDefault(); updateFreeText('');" class="mainlink">
				<spring:message code="accessoriesanddefects.add"/>
			</a>
		</div>
	</c:when>
	<c:otherwise>
		<spring:message code="accessoriesanddefects.furtheraccessoryinformation"/>:
		(<span id="furtherInfoChanges" class="allgood"><spring:message code="accessoriesanddefects.saved"/></span>)<br/>
		<textarea id="itemAccDesc" cols="40" rows="5" maxlength="500" onkeydown="$j('span#furtherInfoChanges').addClass('attention').text('Unsaved Changes');" >${jobItem.accessoryFreeText}</textarea>
		<div id="accFreeButtons" style="width: 100%">
			<a id="itemAccFTSave" href="" onclick="event.preventDefault(); updateFreeText($j('textarea#itemAccDesc').val());" class="mainlink">
				<spring:message code="accessoriesanddefects.save"/>
			</a>
			<a id="itemAccFTDel" href="" onclick="event.preventDefault(); removeFreeText();" class="mainlink">
				<spring:message code="accessoriesanddefects.delete"/>
			</a>
		</div>
	</c:otherwise>
</c:choose>