<%-- File name: /trescal/core/jobs/job/companyquotations.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<table id="jobquotelinktable" class="default2" style="border: 0" summary="">
	<thead>
		<tr>
			<td colspan="4" style="font-weight: normal;">
				<spring:message code="viewjob.showingAllQuotations" arguments="${coName}"/>&nbsp;
				<select id="lqyears">
					<option value=""><spring:message code="all"/></option>
					<c:forEach var="year" begin="1" end="6">
						<option value="${year}">${year}</option>
					</c:forEach>
				</select>
				<spring:message code="viewjob.years"/><br/>
				<select id="lqexpired">
					<option value="false"><spring:message code="viewjob.hiding"/></option>
					<option value="true"><spring:message code="viewjob.showing"/></option>
				</select>
				<spring:message code="viewjob.expiredAnd"/>&nbsp;
				<select id="lqissued">
					<option value="false"><spring:message code="viewjob.hiding"/></option>
					<option value="true"><spring:message code="viewjob.showing"/></option>
				</select>
				<spring:message code="viewjob.unIssued"/>&nbsp;
				-&nbsp;
				<a href="" class="mainlink"
					onclick="event.preventDefault(); searchQuotationsToLink(${jobId != null ? jobId : 'null'}, ${coId}, $j('#lqyears').val(), $j('#lqexpired').val(), $j('#lqissued').val(), '${linkTo}');">
					<spring:message code="viewjob.searchAgain"/>
				</a>
			</td>
			<td colspan="4" style="align: center">
				<spring:message code="viewjob.changeQuotationStatus"/>
				<img src="img/icons/comment.png" width="16" height="16" alt="" title="<spring:message code='viewjob.onlyForIssued'/>"/>
				<select id="acceptStatusSelect" onchange="filterQuotationStatus(); return false;">
					<option value="0">--<spring:message code="all"/>--</option>
					<c:forEach var="as" items="${acceptStatus}">
						<option value="${as.key}">${as.value}</option>
					</c:forEach>
				</select>
			</td>
		</tr>
		<tr>
			
		</tr>
		<tr>
			<th class="jqquoteno" scope="col">
				<spring:message code="viewjob.quoteNoVersion"/>
			</th>
			<th class="jqitem" scope="col">
				<spring:message code="items"/>
			</th>
			<th class="jqcomp" scope="col">
				<spring:message code="company"/>
			</th>
			<th class="jqcont" scope="col">
				<spring:message code="contact"/>
			</th>
			<th class="jqissued" scope="col">
				<spring:message code="viewjob.issued"/>
			</th>
			<th class="jqaccept" scope="col">
				<spring:message code="viewjob.accepted"/>
			</th>
			<th class="jqexp" scope="col">
				<spring:message code="viewjob.expired"/>
			</th>
			<th class="jqlink" scope="col">
				<spring:message code="viewjob.link"/>
			</th>
		</tr>
	</thead>
	<tbody>
		<c:choose>
			<c:when test="${quotations.size() == 0}">
				<tr>
					<td colspan="8" class="bold center">
						<spring:message code="viewjob.noResultsFound"/>
					</td>
				</tr>
			</c:when>
			<c:otherwise>
				<c:forEach var="quote" items="${quotations}" varStatus="quoteLoop">
					<c:set var="i" value="${quoteLoop.index}"/>
					<tr id="qparent${i}" status="${quote.accepted}">
						<td>
							<a href="viewquotation.htm?id=${quote.qid}">
								${quote.qno} version ${quote.ver}
							</a>
							<c:choose>
								<c:when test="${!quote.issued}">
									[<span style="color:red;" title="<spring:message code='viewjob.quotationUnissued'/>">
										<spring:message code="viewjob.unIssued"/>
									</span>]
								</c:when>
								<c:when test="${quote.expired}">
									[<span style="color:red;" title="<spring:message code='viewjob.quotationExpired'/>">
										<spring:message code="viewjob.expired"/>
									</span>]
								</c:when>
							</c:choose>
						</td>
						<td class="center">
							${quote.itemCount}
							<a href="" id="qitemsLink${i}" onclick="event.preventDefault(); getQuoteItems(${i}, ${quote.qid}, '${quote.qno}');">
								<img src="img/icons/items.png" width="16" height="16"
									alt="<spring:message code='viewjob.viewQuotationItems'/>"
									title="<spring:message code='viewjob.viewQuotationItems'/>"
									class="image_inline"/>
							</a>
						</td>
						<td>${quote.coname}</td>
						<td>${quote.conname}</td>
						<td class="center">
							<c:choose>
								<c:when test="${quote.issued}">
									<spring:message code="true"/>
								</c:when>
								<c:otherwise>
									<spring:message code="false"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="center">
							<c:choose>
								<c:when test="${quote.accepted}">
									<spring:message code="true"/>
								</c:when>
								<c:otherwise>
									<spring:message code="false"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td class="center">
							<c:choose>
								<c:when test="${quote.expired}">
									<spring:message code="true"/>
								</c:when>
								<c:otherwise>
									<spring:message code="false"/>
								</c:otherwise>
							</c:choose>
						</td>
						<td>
							<c:choose>
								<c:when test='${linkTo == "job"}'>
									<a href="" id="availquotelink${quote.qid}" class="mainlink" onclick=" event.preventDefault(); insertJobQuoteLink(${jobId}, ${quote.qid}); ">
										<spring:message code="viewjob.linkToJob"/>
									</a>
								</c:when>
								<c:when test='${linkTo == "contract"}'>
									<c:set var="quotenoandversion" value='${quote.qno.concat(" version ").concat(quote.ver)}'/>
									<a href="" id="availquotelink${quote.qid}" class="mainlink" onclick=' event.preventDefault(); linkQuotetoContract(${quote.qid}, "${quotenoandversion}"); self.parent.tb_remove()'>
										<spring:message code="contract.linktocontract"/>
									</a>
								</c:when>
							</c:choose>
							
						</td>
					</tr>
				</c:forEach>
			</c:otherwise>
		</c:choose>
		<tr>
			<td colspan="8" class="bold center">
				<spring:message code="viewjob.useFilters"/>
			</td>
		</tr>
	</tbody>
</table>