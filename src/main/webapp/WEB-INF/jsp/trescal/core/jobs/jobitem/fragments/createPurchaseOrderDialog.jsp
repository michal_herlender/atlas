<%-- File name: /trescal/core/jobs/jobitem/fragments/createPurchaseOrderDialog.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div id="createPurchaseOrderDialog">

    <div id="warningBox1" class="warningBox1" style="display: none">
        <div id="warningBox2" class="warningBox2">
            <div id="warningBox3" class="warningBox3"></div>
        </div>
    </div>
    <div id="successBox1" class="successBox1" style="display: none">
        <div id="successBox2" class="successBox2">
            <div id="successBox3" class="successBox3 text-center">
                <spring:message code="jithirdparty.pocreatedsuccessfully"/> - <span id="po_link"></span>
            </div>
        </div>
    </div>
    <div id="po_items">
        <fieldset>
            <input type="hidden" id="po_jobid" value="${purchaseOrderForm.fromJobItem.job.jobid}" />
            <input type="hidden" id="po_jobitemid" value="${purchaseOrderForm.fromJobItem.jobItemId}" />
            <ol>
                <li>
                    <label><spring:message code="company"/></label>
                    <div id="cascadeSearchPlugin">
                        <input type="hidden" id="compCoroles" value="supplier,business" />
                        <input type="hidden" id="cascadeRules" value="subdiv,contact,address" />
                        <input type="hidden" id="loadCurrency" value="true" />
                        <input type="hidden" id="sourceDiscount" value="true" />
                        <input type="hidden" id="sourceNominal" value="true" />
                        <input type="hidden" id="customHeight" value="250" />
                    </div>
                    <div class="clear"></div>
                </li>
                <li>
                    <label><spring:message code="createpurchaseorder.returnaddress"/></label>
                    <select name="po_retaddressid" id="po_retaddressid" style="width: 370px;">
                        <c:forEach var="address" items="${purchaseOrderForm.returnAddress}">
                            <option value="${address.addrid}"><c:out value="${address.addr1} - ${address.town}"/></option>
                        </c:forEach>
                    </select>
                </li>
                <li>
                    <label><spring:message code="currency"/></label>
                    <select name="currencyCode" id="currencyCode" style="width: 370px;">
                        <c:forEach var="currency" items="${purchaseOrderForm.currencies}">
                            <option value="${currency.currencyId}" ${purchaseOrderForm.defaultCurrency.currencyId == currency.currencyId ? 'selected="selected"' : ''}>
                                <c:out value="${currency.currencyCode}"/>&nbsp;-&nbsp;${currency.currencyERSymbol}
                            </option>
                        </c:forEach>
                    </select>
                </li>
                <li>
                    <label><spring:message code="deliverydate"/></label>
                    <input type="text" name="po_deldate" id="po_deldate" readonly="readonly" size="50"/>
                </li>
                <li>
                    <label><spring:message code="createpurchaseorder.supplierref"/></label>
                    <input type="text" name="po_clientref" id="po_clientref" size="50" />
                </li>
                <li>
                    <table class="default4">
                        <thead>
                        <tr>
                            <th><div class="float-left"><spring:message code="purchaseorder.purchaseorderitems"/></div>
                                <a href="#" class="mainlink-float" onclick=" addNewPoItemToForm(); return false; ">
                                    <img src="img/icons/add.png" width="16" height="16" alt="<spring:message code="jicontractreview.additemtopo"/>" />
                                </a>
                                <div class="clear-0"></div>
                            </th>
                        </tr>
                        </thead>
                    </table>
                    <c:set var="poitems" value="0"/>
                    
                    
                    
                    <div id="items">
                    <c:forEach var="jicost" items="${purchaseOrderForm.jiContractReviewCosts}">
                    <c:if test="${jicost.value.size() >0}">
                    	<c:forEach var="cost" items="${jicost.value}">
                    	
                    		<c:set var="poitems" value="${poitems + 1}" scope="page"/>
	                        <table id="po_item${poitems}" class="default4 po_item">
	                            <thead>
	                            <tr>
	                                <td width="4%" class="text-center">${poitems}</td>
	                                <td width="30%" class="text-center" rowspan="3">
	                                    <textarea id="po_item_desc${poitems}" style=" width: 170px; height: 80px; "><c:out value="${jicost.key.shortnameTranslation}" /> - <c:out value="${jicost.key.longnameTranslation}" /></textarea>
	                                </td>
	                                <td width="8%" class="text-right"><spring:message code="costtype"/>:</td>
	                                <td width="10%">
	                                    <select name="po_item_costtype${poitems}" id="po_item_costtype${poitems}">
	                                        <option value=""><spring:message code="notapplicable"/></option>
	                                        <c:forEach var="costtype" items="${purchaseOrderForm.costtypes}">
	                                            <option value="${costtype.key}" ${cost.key == costtype.key ? 'selected="selected"' : ''}><c:out value="${costtype.value}"/></option>
	                                        </c:forEach>
	                                    </select>
	                                </td>
	                                <td width="8%" class="text-right"><spring:message code="qty"/>:</td>
	                                <td width="14%"><input type="text" id="po_item_qty${poitems}" value="1" class="text-right"/></td>
	                                <td width="4%" class="text-center" rowspan="3">
	                                    <a href="#" class="removelink" ${fn:length(jicost.value) > 1 ? '' : 'style="display: none;"'} id="po_item_remove${poitems}" onclick=" removePoItem(${poitems}); return false; ">
	                                        <img src="img/icons/delete.png" width="16" height="16" alt="Remove item" />
	                                    </a>
	                                </td>
	                            </tr>
	                            <tr>
	                                <td rowspan="2">&nbsp;</td>
	                                <td class="text-right"><spring:message code="calsearchresults.jobitem"/>:</td>
	                                <td>
	                                    <select name="po_item_jobitemid${poitems}" id="po_item_jobitemid${poitems}" onchange="updatePoItemDesc(${item.jobItemId})">
	                                        <c:forEach var="item" items="${purchaseOrderForm.jobitems}">
	                                            <option value="${item.jobItemId}" ${item.jobItemId == jicost.key.jobItemId ? "selected" : ""}>
	                                                <c:out value="${item.itemNo}"/> - <c:out value="${item.inst.plantno}"/> - <cwms:showmodel instrumentmodel="${item.inst.model}"/>
	                                            </option>
	                                        </c:forEach>
	                                    </select>
	                                </td>
	                                <td class="text-right"><spring:message code="discount"/><c:out value=" (%):"/></td>
	                                <td><input type="text" id="po_item_disc${poitems}" value="0.00" class="text-right"/></td>
	                            </tr>
	                            <tr>
	                                <td class="text-right"><spring:message code="purchaseorder.nominalcode"/>:</td>
	                                <td>
	                                    <select name="po_item_nominal${poitems}" id="po_item_nominal${poitems}">
	                                        <option value=""><spring:message code="unknown"/></option>
	                                        <c:forEach var="nominal" items="${purchaseOrderForm.nominalcodes}">
	                                            <option value="${nominal.id}">
	                                                <c:out value="${nominal.code}"/>&nbsp;-&nbsp;<t:showTranslationOrDefault translations="${nominal.titleTranslations}" defaultLocale="${defaultlocale}"/>
	                                            </option>
	                                        </c:forEach>
	                                    </select>
	                                </td>
	                                <td class="text-right"><spring:message code="price"/>:</td>
	                                <td><input type="text" id="po_item_value${poitems}" value="${cost.value}" class="text-right"/></td>
	                            </tr>
	                            </thead>
	                        </table>
	                    </c:forEach> 
	                    </c:if>
	                    <c:if test="${jicost.value.size() == 0}">
	                    <c:set var="poitems" value="${poitems + 1}" scope="page"/>
	                        <table id="po_item${poitems}" class="default4 po_item">
	                            <thead>
	                            <tr>
	                                <td width="4%" class="text-center">${poitems}</td>
	                                <td width="30%" class="text-center" rowspan="3">
	                                    <textarea id="po_item_desc${poitems}" style=" width: 170px; height: 80px; "><c:out value="${jicost.key.shortnameTranslation}" /> - <c:out value="${jicost.key.longnameTranslation}" /></textarea>
	                                </td>
	                                <td width="8%" class="text-right"><spring:message code="costtype"/>:</td>
	                                <td width="10%">
	                                    <select name="po_item_costtype${poitems}" id="po_item_costtype${poitems}">
	                                        <option value=""><spring:message code="notapplicable"/></option>
	                                        <c:forEach var="costtype" items="${purchaseOrderForm.costtypes}">
	                                            <option value="${costtype.key}" ><c:out value="${costtype.value}"/></option>
	                                        </c:forEach>
	                                    </select>
	                                </td>
	                                <td width="8%" class="text-right"><spring:message code="qty"/>:</td>
	                                <td width="14%"><input type="text" id="po_item_qty${poitems}" value="1" class="text-right"/></td>
	                                <td width="4%" class="text-center" rowspan="3">
	                                    <a href="#" class="removelink" style="display: none;" id="po_item_remove${poitems}" onclick=" removePoItem(${poitems}); return false; ">
	                                        <img src="img/icons/delete.png" width="16" height="16" alt="Remove item" />
	                                    </a>
	                                </td>
	                            </tr>
	                            <tr>
	                                <td rowspan="2">&nbsp;</td>
	                                <td class="text-right"><spring:message code="calsearchresults.jobitem"/>:</td>
	                                <td>
	                                    <select name="po_item_jobitemid${poitems}" id="po_item_jobitemid${poitems}" onchange="updatePoItemDesc(${item.jobItemId})">
	                                        <c:forEach var="item" items="${purchaseOrderForm.jobitems}">
	                                            <option value="${item.jobItemId}" ${item.jobItemId == jicost.key.jobItemId ? "selected" : ""}>
	                                                <c:out value="${item.itemNo}"/> - <c:out value="${item.inst.plantno}"/> - <cwms:showmodel instrumentmodel="${item.inst.model}"/>
	                                            </option>
	                                        </c:forEach>
	                                    </select>
	                                </td>
	                                <td class="text-right"><spring:message code="discount"/><c:out value=" (%):"/></td>
	                                <td><input type="text" id="po_item_disc${poitems}" value="0.00" class="text-right"/></td>
	                            </tr>
	                            <tr>
	                                <td class="text-right"><spring:message code="purchaseorder.nominalcode"/>:</td>
	                                <td>
	                                    <select name="po_item_nominal${poitems}" id="po_item_nominal${poitems}">
	                                        <option value=""><spring:message code="unknown"/></option>
	                                        <c:forEach var="nominal" items="${purchaseOrderForm.nominalcodes}">
	                                            <option value="${nominal.id}">
	                                                <c:out value="${nominal.code}"/>&nbsp;-&nbsp;<t:showTranslationOrDefault translations="${nominal.titleTranslations}" defaultLocale="${defaultlocale}"/>
	                                            </option>
	                                        </c:forEach>
	                                    </select>
	                                </td>
	                                <td class="text-right"><spring:message code="price"/>:</td>
	                                <td><input type="text" id="po_item_value${poitems}" value="0.00" class="text-right"/></td>
	                            </tr>
	                            </thead>
	                        </table>
	                    </c:if> 
	                    </c:forEach>   
                    </div>
                </li>
                <li>
                    <div id="po_button" class="text-center marg-top">
                        <button id="createpurchaseorder"><spring:message code="addjob.createpo"/></button>
                    </div>
                </li>
            </ol>
        </fieldset>
    </div>
</div>
