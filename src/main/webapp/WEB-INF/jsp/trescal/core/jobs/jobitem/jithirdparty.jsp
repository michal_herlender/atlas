<%-- File name: /trescal/core/jobs/jobitem/jithirdparty.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<jobs:jobItemTemplate>
    <jsp:attribute name="subScript">
        <script type='text/javascript' src='script/trescal/core/jobs/jobitem/JIThirdParty.js'></script>
		<script src='script/trescal/core/documents/ImageGalleryFunctions.js'></script>
		<script>
			<!-- // 	grab scroll to parameter passed when using job item key navigation -->
		 	var scrollto = '${request.getParameter("scrollto")}';	
			// 	 populate ledgers for use in ajax calls when creating POs 
		  	var ledgers = new Array(); 
	 	</script>
		<c:forEach var="ledgerItem" items="${ledgers}">
			<script>
				ledgers.push('${ledgerItem}');
		 	</script>
	 	</c:forEach>
    </jsp:attribute>
    <jsp:body>

	<!-- Page specific code -->
	<!-- infobox div contains third party quote requests and deliveries and is styled with nifty corners -->
	<div class="infobox" id="jiThirdParty">
	
		<c:if test="${readyForRequirement == true}">
		
			<form:form action="" method="post" modelAttribute="form">
				
				<fieldset>									
					
					<legend><spring:message code="jithirdparty.selectworkrequired"/></legend>
			
					<ol>
						<li>
							<form:label path="investigation"><spring:message code="jithirdparty.investigation"/>:</form:label>
							<form:checkbox id="investigation" path="investigation" />
						</li>
						<li>
							<form:label path="calibration"><spring:message code="calibration"/>:</form:label>
							<form:checkbox id="calibration" path="calibration" />
						</li>
						<li>
							<form:label path="repair"><spring:message code="repair"/>:</form:label>
							<form:checkbox id="repair" path="repair" />
						</li>
						<li>
							<form:label path="adjustment"><spring:message code="adjust"/>:</form:label>
							<form:checkbox  id="adjustment" path="adjustment" />
						</li>
						<li>
							<label class="labelhead width80">
							<spring:message code="jithirdparty.string1"/>:
							</label>
							<span>&nbsp;</span>
						</li>
						<li>
							<label><spring:message code="jicontractreview.outcome"/>:</label>
							<div class="float-left">
							<c:set var="checked" value=""/>
							<c:forEach var="ao" items="${actionoutcomes}">
								 <c:choose>
		 							<c:when test="${ao.defaultOutcome == true}">
		    						  <c:set var="checked" value="checked"/>
			  						</c:when>
		 							<c:otherwise>
			   					 	  <c:set var="checked" value=""/>
			  						</c:otherwise>
								 </c:choose>
								<form:radiobutton path="actionOutcomeId" value="${ao.id}" checked="${checked}"/>
								<t:showTranslationOrDefault translations="${ao.translations}" defaultLocale="${defaultlocale}"/>
								<br />
							</c:forEach>
							</div>
							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
						</li>
						<c:if test="${form.ji.tpRequirements.size() < 1 && (form.ji.tpInstructions.size() < 1)}">
							<li>
								<label class="instruction pad-top"><spring:message code="jithirdparty.string2"/>:<br /></label>
								<form:textarea path="tpInstruction" rows="5" class="width70 symbolBox presetComments THIRD_PARTY_INSTRUCTION"></form:textarea>
							</li>
						</c:if>
						<c:if test="${others.size() > 0}">
							<li>
								<label class="labelhead width80">
									<spring:message code="jithirdparty.string3"/>:
								</label>
								<span>&nbsp;</span>
							</li>
							<li id="itemsWithTPRequirements">
								<input type="checkbox" onclick=" selectAllItems(this.checked, 'otherItemsForTPReq', 'itemsWithTPRequirements'); " /> <spring:message code="jiactions.all"/><br />
								<c:forEach var="item" items="${others}">
									<form:checkbox id="otherItemsForTPReq" path="otherItemsForTPReq" value="${item.jobItemId}" />
									<span>${item.itemNo} - <links:instrumentLinkDWRInfo instrument="${item.inst}" rowcount="0" displayBarcode="false" displayName="true" displayCalTimescale="false"  caltypeid="0" displayInstClientId="true"/></span>	
									<br/>
								</c:forEach>
							</li>
						</c:if>
						<li>
							<label>&nbsp;</label>
							<input type="submit" value="<spring:message code="submit"/>" />
						</li>
						
					</ol>								
				
				</fieldset>
				
			</form:form>
			
		</c:if>
																		
		<table class="default4" id="TPRequirements" summary="This table displays any third party requirements for job item">
			<thead>
				<tr>
					<td colspan="9" class="bold"><spring:message code="jithirdparty.string4"/> (${form.ji.tpRequirements.size()})</td>
				</tr>
				<tr>
					<th scope="col"><spring:message code="jithirdparty.recorded"/></th>
					<th scope="col" class="center"><spring:message code="jithirdparty.investigation"/></th>
					<th scope="col" class="center"><spring:message code="calibration"/></th>
					<th scope="col" class="center"><spring:message code="jifaultreport.repair"/></th>
					<th scope="col" class="center"><spring:message code="jifaultreport.adjust"/></th>
					<th scope="col" class="center"><spring:message code="jicontractreview.quote"/></th>
					<th scope="col" class="center"><spring:message code="delivery"/></th>
					<th scope="col"><spring:message code="jithirdparty.lastupdated"/></th>
					<th scope="col" class="center"><spring:message code="edit"/></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="9">
						&nbsp;
					</td>
				</tr>
			</tfoot>
			<tbody>
				<c:choose>
					<c:when test="${form.ji.tpRequirements.size() < 1}">
					<tr>
						<td colspan="9" class="center bold"><spring:message code="jithirdparty.string5"/></td>
					</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="tpr" items="${form.ji.tpRequirements}">
						<tr id="tpr${tpr.id}">
							<td width="15%">${tpr.recordedBy.name} <br/><fmt:formatDate type="both" dateStyle="SHORT" timeStyle="SHORT" value="${tpr.created}"/></td>
							<td class="booleanfield center" width="8%">
								<c:choose>
									<c:when test="${tpr.investigation}">
									<img src="img/icons/greentick.png" class="t" width="16" height="16" />
									</c:when>
									<c:otherwise>
									<img src="img/icons/redcross.png" class="c" width="16" height="16" />
									</c:otherwise>
								</c:choose>	
							</td>
							<td class="booleanfield center" width="8%">
								<c:choose>
									<c:when test="${tpr.calibration}">
									<img src="img/icons/greentick.png" class="t" width="16" height="16" />
									</c:when>
									<c:otherwise>
									<img src="img/icons/redcross.png" class="c" width="16" height="16" />
									</c:otherwise>
								</c:choose>															
							</td>
							<td class="booleanfield center" width="8%">
								<c:choose>
									<c:when test="${tpr.repair}">
									<img src="img/icons/greentick.png" class="t" width="16" height="16" />
									</c:when>
									<c:otherwise>
									<img src="img/icons/redcross.png" class="c" width="16" height="16" />
									</c:otherwise>
								</c:choose>															
							</td>
							<td class="booleanfield center" width="8%">
								<c:choose>
									<c:when test="${tpr.adjustment}">
									<img src="img/icons/greentick.png" class="t" width="16" height="16" />
									</c:when>
									<c:otherwise>
									<img src="img/icons/redcross.png" class="c" width="16" height="16" />
									</c:otherwise>
								</c:choose>															
							</td>
							<td width="15%" class="center"><c:choose><c:when test="${not empty tpr.quote}"> ${tpr.quote.qno} </c:when><c:otherwise> N/A </c:otherwise></c:choose></td>
							<td width="15%" class="center"><c:choose><c:when test="${not empty tpr.deliveryItem}"> ${tpr.deliveryItem.delivery.deliveryno} </c:when><c:otherwise> N/A </c:otherwise></c:choose></td>
							<td width="15%" id="tpr${tpr.id}-updated">${tpr.updatedBy.name} <c:if test="${not empty tpr.lastUpdated}"><br/><fmt:formatDate type="both" dateStyle="SHORT" timeStyle="SHORT" value="${tpr.lastUpdated}"/></c:if></td>
							<td width="8%" class="center">
								<a href="#" id="edittpreqlink${tpr.id}" onclick=" editTPRequirement(${tpr.id}); return false; " class="mainlink vis"><spring:message code="edit"/></a>
								<a href="#" id="savetpreqlink${tpr.id}" onclick=" saveTPRequirement(${tpr.id}); return false; " class="mainlink hid"><spring:message code="save"/></a>
							</td>
						</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>	
			</tbody>
		</table>
		
		<c:if test="${form.ji.tpRequirements.size() > 0}">
		
			<fieldset class="tpInstructions">
			
				<legend><spring:message code="jithirdparty.thipartcalinstr"/></legend>
			
				<ol>
					<li>
						<label class="labelhead">
							<spring:message code="jithirdparty.string6"/>:
						</label>
						<span>&nbsp;</span>
					</li>
					<li>
						<table class="default4 jitpInstList" summary="This table contains tp instructions">
							<thead>
								<tr>
									<th class="faultdetail" scope="col">
										<spring:message code="save"/>&nbsp;<spring:message code="instruction"/>
										<a href="#" class="domthickbox" onclick=" createAddOrEditJITPInstruction('add', ${form.ji.jobItemId}, 0, ''); return false; ">
											<img src="img/icons/add.png" width="16" height="16" class="img_inl_margleft" alt="<spring:message code="jifaultreport.addinstr"/>" title="<spring:message code="jifaultreport.addinstr"/>" />
										</a>
									</th>
									<th class="showontpdel" scope="col"><spring:message code="jithirdparty.show"/></th>
									<th class="modifiedby" scope="col">
										<spring:message code="jithirdparty.setbyandn"/>
									</th>
									<th class="editremove" scope="col">
										Actions
									</th>
								</tr>
							</thead>
							<tbody>
								<c:set var="instCount" value="0"/>
								<c:forEach var="inst" items="${form.ji.tpInstructions}">
									<tr>
										<td class="jitpinstruction" id="Instruction-${inst.id}">
											<c:choose>
												<c:when test="${not inst.active}">
													<del>${inst.instruction}</del>
												</c:when>
												<c:otherwise>
													${inst.instruction}
												</c:otherwise>
											</c:choose>
										</td>
										<td class="showontpdel" id="showontpdn${inst.id}">																	
											<a href="#" onclick=" updateShowOnTPDelNote(${inst.id}, ${inst.isShowOnTPDelNote()}); return false; ">
												<c:choose>
													<c:when test="${inst.isShowOnTPDelNote() == true}">
													<img src="img/icons/bullet-tick.png" width="10" height="10" alt="Show On TP Del Note" title="Show On TP Del Note" />
													</c:when>
													<c:otherwise>
													<img src="img/icons/bullet-cross.png" width="10" height="10" alt="Hide On TP Del Note" title="Hide On TP Del Note" />
													</c:otherwise>
												</c:choose>	
											</a>
										</td>
										<td class="modifiedby">[${inst.recordedBy.name}&nbsp;<fmt:formatDate value="${inst.lastModified}" dateStyle="medium" type="date" />]</td>
										<td>
											<c:if test="${inst.active == true}">
												<a href="#" onclick=" createAddOrEditJITPInstruction('edit', ${form.ji.jobItemId}, ${inst.id}, `${inst.instruction}`); return false; " >
													<img width="16" height="16" title="<spring:message code="jifaultreport.editinstr"/>" class="noteimg_padded" alt="<spring:message code="jifaultreport.editinstr"/>" src="img/icons/note_edit.png" />
												</a>
												&nbsp;
												<a href="#" onclick=" removeTPInstruction(this, ${inst.id}); return false; " >
													<img width="16" height="16" title="<spring:message code="jifaultreport.deleteinstr"/>" class="noteimg_padded" alt="<spring:message code="jifaultreport.deleteinstr"/>" src="img/icons/note_delete.png" />
												</a>
											</c:if>
										</td>																
									</tr>
									<c:set var="instCount" value="${instCount + 1}"/>
								</c:forEach>
								<c:if test="${instCount == 0}">
									<tr>
										<td colspan="4" class="bold"><spring:message code="jithirdparty.string7"/></td>
									</tr>
								</c:if>
							</tbody>
						</table>												
					</li>									
				</ol>
			
			</fieldset>
		
		</c:if>
		
		<table class="default4" id="TPQuoteReqs" summary="This table displays any third party quotation requests for job item">
			<thead>
				<tr>
					<td colspan="6" class="bold"><spring:message code="jithirdparty.string16"/> (<span class="jiTPQuoteReqSize">${form.ji.tpQuoteItemRequests.size()}</span>)</td>
				</tr>
				<tr>
					<th class="qqno" scope="col"><spring:message code="quoteno"/></th>
					<th class="qcomp" scope="col"><spring:message code="company"/></th>
					<th class="qcont" scope="col"><spring:message code="contact"/></th>
					<th class="qreqdate" scope="col"><spring:message code="requestdate"/></th>
					<th class="qreqduedate" scope="col"><spring:message code="duedate"/></th>
					<th class="qstatus" scope="col"><spring:message code="status"/></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="6">
						<a href="#" class="mainlink" onclick=" requestThirdPartyQuoteContent(${form.ji.jobItemId}); return false; " title=""><spring:message code="jithirdparty.string17"/></a>
					</td>
				</tr>
			</tfoot>
			<tbody>
				<c:choose>
					<c:when test="${form.ji.tpQuoteItemRequests.size() < 1}">
					<tr>
						<td colspan="6" class="text-center bold"><spring:message code="jithirdparty.string18"/></td>
					</tr>
					</c:when>
					<c:otherwise>
					<c:forEach var="tpQRI" items="${form.ji.tpQuoteItemRequests}">
						<c:set var="req" value="${tpQRI.tpQuoteRequest}"/>
						<tr>
							<td class="qqno"><a href="<spring:url value='viewtpquoterequest.htm?id=${req.id}'/>" class="mainlink">${req.requestNo}</a></td>
							<td class="qcomp">${req.contact.sub.comp.coname}</td>
							<td class="qcont">${req.contact.name}</td>
							<td class="qreqdate"><fmt:formatDate value="${req.reqdate}" dateStyle="medium" type="date"/></td>
							<td class="qreqduedate"><fmt:formatDate value="${req.dueDate}" dateStyle="medium" type="date"/></td>
							<td class="qstatus"><t:showTranslationOrDefault translations="${req.status.nametranslations}" defaultLocale="${defaultlocale}" /></td>
						</tr>
					</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
		
		<table class="default4" id="TPQuotes" summary="This table displays any third party linked quotations for job item">
			<thead>
				<tr>
					<td colspan="7" class="bold"><spring:message code="jithirdparty.string11"/> (<span id="jiTPQuoteSize">${form.ji.linkedTPQuotes.size()}</span>)</td>
				</tr>
				<tr>
					<th class="qqno" scope="col"><spring:message code="quoteno"/></th>
					<th class="qitem" scope="col"><spring:message code="item"/></th>
					<th class="qcomp" scope="col"><spring:message code="company"/></th>
					<th class="qcont" scope="col"><spring:message code="contact"/></th>
					<th class="qlinkby" scope="col"><spring:message code="linkedby"/></th>
					<th class="qlinkon" scope="col"><spring:message code="linkedon"/></th>
					<th class="qdel" scope="col"><spring:message code="delete"/></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="7">
						<a href="#" class="domthickbox mainlink" onclick=" linkTPQContent(${form.ji.inst.model.modelid}, ${resultsYearFilter}, ${form.ji.jobItemId}, 1);  return false; " title=""><spring:message code="jithirdparty.string12"/></a> | 
						<a href="#" class="domthickbox mainlink" onclick=" recordTPQContent('${form.ji.jobItemId}'); return false; " title=""><spring:message code="jithirdparty.string13"/></a>
					</td>
				</tr>
			</tfoot>
			<tbody>
				<c:choose>
					<c:when test="${form.ji.linkedTPQuotes.size() < 1}">
					<tr>
						<td colspan="7" class="text-center bold"><spring:message code="jithirdparty.string14"/></td>
					</tr>
					</c:when>
					<c:otherwise>
					<c:set var="rowcount" value="0"/>
					<c:forEach var="tpQRI" items="${form.ji.linkedTPQuotes}"> 
						<c:set var="req" value="${tpQRI.tpQuoteItem}"/>
						<tr id="tpquotelink${tpQRI.id}">
							<td>
 								<links:tpquoteLinkDWRInfo tpquote="${req.tpquotation}" rowcount="${rowcount}"/>
							</td>
							<td class="center">${req.itemno}</td>
							<td>${req.tpquotation.contact.sub.comp.coname}</td>
							<td>${req.tpquotation.contact.name}</td>
							<td>${tpQRI.contact.name}</td>
							<td><fmt:formatDate type="date" dateStyle="MEDIUM" value="${tpQRI.date}"/></td>
							<td class="center">
								<a href="#" onclick=" deleteTPQuoteLink(${tpQRI.id}); return false; ">
									<img src="img/icons/delete.png" width="16" height="16" alt="<spring:message code="jithirdparty.string15"/>" title="<spring:message code="jithirdparty.string15"/>" />		
								</a>
							</td>
						</tr>
						<c:set var="rowcount" value="${rowcount + 1}"/>
					</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
		
		<table class="default4" id="jiPurchaseOrders" summary="This table displays linked purchase orders to this job item">
			<thead>
				<tr>
					<td colspan="7" class="bold"><spring:message code="jithirdparty.string8"/> (<span id="posLinkedSize">${form.ji.purchaseOrderItems.size()}</span>)</td>
				</tr>
				<tr>
					<th class="pono" scope="col"><spring:message code="jicontractreview.ponumber"/></th>
					<th class="itemno" scope="col"><spring:message code="itemno"/></th>
					<th class="coname" scope="col"><spring:message code="company"/></th>
					<th class="contact" scope="col"><spring:message code="contact"/></th>
					<th class="createdate" scope="col"><spring:message code="jithirdparty.datecreated"/></th>
					<th class="createby" scope="col"><spring:message code="createdby"/></th>								
					<th class="cost" scope="col"><spring:message code="cost"/></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="7">
						<a href="#" class="mainlink-float" onclick=" displayPurchaseOrdersToLink('${form.ji.job.jobid}', ${form.ji.jobItemId}); return false; "><spring:message code="jithirdparty.linktoexistingpo"/></a>
                        <a href="#" class="mainlink-float" onclick=" showCreatePurchaseOrderForm(); return false; "><spring:message code="jithirdparty.createpo"/></a>
					</td>
				</tr>
			</tfoot>
			<tbody>
				<c:choose>
					<c:when test="${form.ji.purchaseOrderItems.size() == 0}">
					<tr>
						<td class="bold text-center" colspan="7"><spring:message code="jithirdparty.nopolinked"/></td>
					</tr>
					</c:when>
					<c:otherwise>											
						<c:forEach var="pol" items="${form.ji.purchaseOrderItems}">
						<c:set var="poitem" value="${pol.poitem}"/>
						<c:set var="po" value="${pol.poitem.order}"/>
						<tr>
							<td><a href="<spring:url value='viewpurchaseorder.htm?id=${po.id}'/>" class="mainlink">${po.pono}</a></td>
							<td class="center">${poitem.itemno}</td>
							<td>${po.contact.sub.comp.coname}</td>
							<td>${po.contact.name}</td>
							<td><fmt:formatDate type="date" dateStyle="SHORT" value="${po.regdate}"/></td>
							<td>${po.createdBy.name}</td>												
							<td class="cost2pc">${po.currency.currencyERSymbol}${poitem.finalCost}</td>
						</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>	
			</tbody>
		</table>
		
		<table class="default4" id="TPDels" summary="This table displays any third party deliveries for job item">
			<thead>
				<tr>
					<td colspan="5" class="bold"><spring:message code="jithirdparty.thirdpartydeliverys"/> (<span class="jiTPDels">${deliveries.size()}</span>)</td>
				</tr>
				<tr>
					<th class="ddelno" scope="col"><spring:message code="deliveryno"/></th>
					<th class="dcomp" scope="col"><spring:message code="company"/></th>
					<th class="dcont" scope="col"><spring:message code="contact"/></th>
					<th class="ditem" scope="col"><spring:message code="items"/></th>
					<th class="ddeldate" scope="col"><spring:message code="deliverydate"/></th>										
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="5">
						<a href="selecttpcontact.htm?jobid=${form.ji.job.jobid}" class="mainlink-float"><spring:message code="jithirdparty.thirdpartydelivery"/></a>
					</td>
				</tr>
			</tfoot>
			<tbody>
				<c:choose>
					<c:when test="${deliveries.size() < 1}">
					<tr>
						<td colspan="5" class="center bold"><spring:message code="jithirdparty.nothirdpartydeliverys"/></td>
					</tr>
					</c:when>
					<c:otherwise>
					<c:forEach var="d" items="${deliveries}">
						<tr>
							<td><a href="viewdelnote.htm?delid=${d.deliveryid}" class="mainlink">${d.deliveryno}</a></td>
							<td>${d.contact.sub.comp.coname}
								<c:if test="${not empty d.contact.sub.subname}">
									(${d.contact.sub.subname})
								</c:if>	
							</td>
							<td>${d.contact.name}</td>
							<td>${d.items.size()}</td>
							<td> <fmt:formatDate value="${d.deliverydate}" type="date" dateStyle="medium" /></td>
						</tr>
					</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>									
	</div>

	<div id="createPurchaseOrderDialog">

	</div>

	<!-- end of third party infobox div -->

    </jsp:body>
</jobs:jobItemTemplate>