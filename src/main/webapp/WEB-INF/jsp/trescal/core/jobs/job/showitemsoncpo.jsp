<%-- File name: /trescal/core/jobs/job/showitemsoncpo.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div id="createNewPOBoxy" class="overflow">
	<fieldset>
		<legend>${titleAction}</legend>
		<ol>
			<li>
				<label><spring:message code="viewjob.ponumber"/>:</label>
				<input type="text" name="ponumber" value="<c:out value='${poNo}'></c:out>" tabindex="100"/>
			</li>
			<li>
				<label><spring:message code="comment"/>:</label>
				<textarea name="pocomment" cols="50" rows="4" tabindex="101"><c:out value="${poComment}"></c:out></textarea>
			</li>
			<li id="poSubmit">
				<label>&nbsp;</label>
				<c:choose>
					<c:when test="${poId == null || poId == 0}">
						<spring:message code="showitemsoncpo.createpo" var="createPO"/>
						<input type="button" onclick="this.disabled = true; createPurchOrd(${jobId}); return false;" value="${createPO}" tabindex="102"/>
					</c:when>
					<c:otherwise>
						<spring:message code="showitemsoncpo.savepo" var="savePO"/>
						<input type="button" onclick="this.disabled = true; editPurchOrd(${poId}, ${jobId}, '${jobNo}'); return false;" value="${savePO}" tabindex="102"/>
					</c:otherwise>
				</c:choose>
			</li>
		</ol>
	</fieldset>
</div>