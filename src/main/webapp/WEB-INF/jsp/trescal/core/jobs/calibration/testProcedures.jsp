<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<t:crocodileTemplate>
    <jsp:attribute name="header">
<!--         Section between main_2 and main_3 -->
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <!-- DWR -->
<script type='text/javascript' src='dwr/engine.js'></script>							
<script type='text/javascript' src='dwr/util.js'></script>

<script type='text/javascript' src='dwr/interface/proceditservice.js'></script>

<script type = "text/javascript">
			function link(root,job_collection,procedure_collection,key) {					
				// collect the data needed to pass in 
				//TODO
				// create a new job form using DWR							
				proceditservice.runProcedure(key, root,job_collection,procedure_collection,null, function(str) {window.location.href="#springUrl('/runproc.htm?runfile=" + str + "')"});													
			}
</script>

<script type='text/javascript' src='script/trescal/calibration/Procedures.js'></script>
    </jsp:attribute>
    <jsp:body>
 						<div class="infobox">
			<spring:bind path="testProcedures.procedures">
			<table id="proceduresearchresults" class="tablesorter" summary="This table lists all procedure search results">
				<thead>
					<tr>
						<td colspan="4">
							<c:choose>
								<c:when test="${command.procedures.size() > 1}">
									Your procedures search returned ${testProcedures.procedures.size()} results
								</c:when>
								<c:otherwise>
									Your procedures search returned ${testProcedures.procedures.size()} result
								</c:otherwise>
							</c:choose>
						</td>
					</tr>
					<tr>																		
						<th id="number" scope="col">Title</th>  
						<th id="qresconame" scope="col">department</th>  
						<th id="qrescontact" scope="col">heading</th>  
						<th	id="qresclientref" scope="col">equipment</th>																			
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="4">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
					<c:set var="rowcount" value="1"/>
					<c:forEach var="p" items="${testProcedures.procedures}">
						##<tr id="" <c:choose><c:when test="${rowcount % 2 == 0}"> class="even" </c:when><c:otherwise> class="odd" </c:otherwise></c:choose> onclick="link('${testProcedures.xindiceRoot}','${testProcedures.xindiceJobs}','${testProcedures.xindiceProcedures}','${p.xindiceKey}')" onmouseover="this.id = this.className; this.className = 'hoverrow';" onmouseout="if (this.id == 'even') {this.className = 'even'; this.id = '';} else {this.className = 'odd'; this.id = ''}">
						##set($uri = '<c:url value="/runproc.htm"/>'?runfile=${p.xindiceKey}&preview=false")
##						<tr id="" <c:choose><c:when test="${rowcount % 2 == 0}"> class="even" </c:when><c:otherwise> class="odd" </c:otherwise></c:choose> onclick="window.open('${uri}')" onmouseover="this.id = this.className; this.className = 'hoverrow';" onmouseout="if (this.id == 'even') {this.className = 'even'; this.id = '';} else {this.className = 'odd'; this.id = ''}">
						<tr id="" <c:choose><c:when test="${rowcount % 2 == 0}"> class="even" </c:when><c:otherwise> class="odd" </c:otherwise></c:choose> onclick="link('${testProcedures.xindiceRoot}','${testProcedures.xindiceJobs}','${testProcedures.xindiceProcedures}','${p.xindiceKey}')" onmouseover="this.id = this.className; this.className = 'hoverrow';" onmouseout="if (this.id == 'even') {this.className = 'even'; this.id = '';} else {this.className = 'odd'; this.id = ''}">								
							<td scope="row">${p.title}</td>
							<td>${p.heading}</td>		
							<td>${p.department}</td>
							<td>${p.equipment}</td>																														
						</tr>
						<c:set var="rowcount" value="${rowcount + 1}"/>
					</c:forEach>
				</tbody>
			</table>
			</spring:bind>
			
		</div>
    </jsp:body>
</t:crocodileTemplate>