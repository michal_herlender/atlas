<%-- File name: /trescal/core/jobs/calibration/viewbatchcalibration.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="viewbatchcalibration.headtext"/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/jobs/calibration/ViewBatchCalibration.js'></script>
    </jsp:attribute>
    <jsp:body>
	<!-- error section displayed when form submission fails -->
			<form:errors path="form.*">
       				 <div class="warningBox1">
              		  <div class="warningBox2">
              	       <div class="warningBox3">
                    	<h5 class="center-0 attention"><spring:message code="viewbatchcalibration.errors"/>:</h5>
                      <c:forEach var="e" items="${messages}">
                        <div class="center attention"><c:out value="${e}"/></div>
                      </c:forEach>
                     </div>
                   </div>
                  </div>
             </form:errors>
			<!-- end error section -->
			
			<!-- div styled with nifty corners -->
			<div class="infobox">
			
				<fieldset>
					
					<legend><spring:message code="viewbatchcalibration.batchid"/>: ${form.batchCal.id}</legend>
					
					<ol>
						<li>
							<label><spring:message code="viewbatchcalibration.technician"/>:</label>
							<span>
								${form.batchCal.owner.name}
							</span>
						</li>
						<li>
							<label><spring:message code="newbatchcalibration.company"/>:</label>
							<span>
								<links:companyLinkDWRInfo company="${form.batchCal.job.con.sub.comp}" rowcount="0" copy="${true}"/>											
							</span>
						</li>
						<li>
							<label>Job No:</label>
							<span><links:jobnoLinkDWRInfo jobno="${form.batchCal.job.jobno}" jobid="${form.batchCal.job.jobid}" copy="${true}" rowcount="${rowcount}"/></span>
						</li>
						<li>
							<label><spring:message code="completecalibration.calprocselect"/>:</label>
							<span>
								${form.batchCal.process.process}
							</span>
						</li>
						<li>
							<label><spring:message code="newbatchcalibration.caldate"/>:</label>
							<span>
								<fmt:formatDate value='${form.batchCal.calDate}' type="date" dateStyle="SHORT" />
							</span>
						</li>
						<c:if test="${form.batchCal.process.applicationComponent}">
							<li>
								<fieldset>
									<legend>
										<input type="checkbox" onchange=" changeChooseNext(${form.batchCal.id}, this); " 
										<c:if test="${form.batchCal.chooseNext == true}"> checked="checked" </c:if>/> 
										<spring:message code="viewbatchcalibration.inputText1"/>
									</legend>
								</fieldset>
							</li>
						</c:if>
					</ol>
					
				</fieldset>
			
			</div>
			<!-- end of infobox div -->
			
			<!-- div containing batch calibration items styled with nifty corners -->
			<div class="infobox" id="viewbatchcalibration">
			
				<c:if test="${form.batchCal.process.applicationComponent == false}">
					<div class="float-right">
						<spring:message code="viewbatchcalibration.certsave"/> 
						<a href="file:///${form.jobCertDirectory}" target="_blank" class="mainlink">&nbsp;<spring:message code="viewbatchcalibration.thisfolder"/></a>
					</div>
					<div class="clear"></div>	
				</c:if>		
											
				<table id="batchtable" class="default2">
					<thead>
						<tr class="nodrag">
							<td colspan="8"><spring:message code="viewbatchcalibration.calibrations"/> (${form.batchCal.cals.size()})</td>
						</tr>
						<tr class="nodrag">
							<th class="item" scope="col">
								<a href="#" class="mainlink" onclick=" updateBatchCalibrationSortOrder(false, ${form.batchCal.id}); return false; "><spring:message code="multistart.item"/></a>
							</th>
							<th class="desc" scope="col"><spring:message code="viewbatchcalibration.description"/></th>
							<th class="serial" scope="col">
								<a href="#" class="mainlink" onclick=" updateBatchCalibrationSortOrder(true, ${form.batchCal.id}); return false; "><spring:message code="serialno"/></a>
							</th>
							<th class="proc" scope="col"><spring:message code="calsearch.capability"/></th>
							<th class="caltype" scope="col"><spring:message code="calsearch.casltype"/></th>
							<th class="status" scope="col"><spring:message code="calsearch.status"/></th>
							<th class="utc" scope="col"><spring:message code="jicalibration.unabltocali"/></th>
							<th class="delete" scope="col"><spring:message code="viewbatchcalibration.del"/></th>
						</tr>
					</thead>
					<tfoot>
						<tr class="nodrag">
							<td colspan="8">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody id="batch${form.batchCal.id}">
						<c:forEach var="calWrap" items="${form.calWrappers}">
							<c:set var="cal" value="${calWrap.cal}" />
							<c:set var="jobItem" value="${cal.links.iterator().next().ji}" />
								<tr id="${cal.batchPosition}">
								<!-- the class of dataHook is used to retrieve all cal data for reprinting checksheets -->
								<td class="center dataHook">
									<!-- this hidden input is used when selecting checksheets to be printed -->
									<input type="hidden" name="calIds" value="${cal.id}" />
									${cal.links.iterator().next().ji.itemNo}
								</td>
								<td><instmodel:showBasicModel instrument = "${jobItem.inst}"/></td>
								<td>${jobItem.inst.serialno}</td>
								<td class="center">${cal.capability.reference}</td>
								<td class="center">	<t:showTranslationOrDefault translations="${cal.calType.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/></td>
								<td id="statusForCal${cal.id}">
								
								<c:choose>
									<c:when test="${(cal.status.name == 'Awaiting work' || cal.status.name == 'On-going')}">
									<c:choose>
										<c:when test="${calWrap.accredited == true}">
											<c:choose>
												<c:when test="${cal.calProcess.applicationComponent}">
												<a href="#" onclick=" startCalibration(${cal.id}, ${currentContact.personid}, 'runproc.htm?calid=${cal.id}&batchid=${form.batchCal.id}&runfile=${cal.xindiceKey}', '${cal.status.name}'); return false; " class="mainlink">${cal.status.name}</a>
												</c:when>
												<c:otherwise>
													<c:choose>
														<c:when test="${cal.certs.size() > 0}">
															<spring:message code="viewbatchcalibration.isscertnum"/>:
															<c:forEach var="cert" items="${cal.certs}">
																${cert.certno}<br/>
															</c:forEach>
															<a href="" class="mainlink" onclick=" event.preventDefault(); completeCal(${cal.id}); " >
																<spring:message code="completecalibration.lblcomplcal"/>
															</a>
														</c:when>
														<c:otherwise>
															<a href="" class="mainlink" onclick=" event.preventDefault(); issueCert(${cal.id}); " >
																<spring:message code="viewbatchcalibration.isscertnum"/>
															</a>
														</c:otherwise>
													</c:choose>
												</c:otherwise>
											</c:choose>
										</c:when>
										<c:otherwise>
											<t:showTranslationOrDefault translations="${cal.status.nametranslations}" defaultLocale="${defaultlocale}"/> (<spring:message code="viewbatchcalibration.notaccrtocali"/>)
										</c:otherwise>
									</c:choose>
									</c:when>
									<c:otherwise>
										<t:showTranslationOrDefault translations="${cal.status.nametranslations}" defaultLocale="${defaultlocale}"/>
									</c:otherwise>
								</c:choose>
									
									<c:if test="${form.batchCal.process.applicationComponent == false && cal.certs.size() > 0}">

										<div class="float-right">
											<c:choose>
												<c:when test="${not empty calWrap.wordFileExists}">
													<img src="img/icons/word_tick.png" alt="<spring:message code="viewbatchcalibration.imgwordtick"/>" title="<spring:message code="viewbatchcalibration.imgwordtick"/>" />
												</c:when>
												<c:otherwise>
													<a href="#" onclick=" checkForWordFile(this, ${cal.certs.get(0).certid}); return false; "><img src="img/icons/word_cross.png" alt="<spring:message code="viewbatchcalibration.imgwordcross"/>" title="<spring:message code="viewbatchcalibration.imgwordcross"/>" /></a>
												</c:otherwise>
											</c:choose>
										</div>
									</c:if>
								</td>
								<td id="utcForCal${cal.id}">
									<c:choose>
										<c:when test="${(cal.status.name == 'Awaiting work' || cal.status.name == 'On-going')}">
											<a href="#" class="mainlink-float" onclick=" createUnableToCalContent(${cal.id}, ${currentContact.personid}, ${jobItem.state.stateid}); return false; " title="">
												<img src="img/icons/cancel.png" width="16" height="16" title="<spring:message code="viewbatchcalibration.imgcancel"/>" alt="<spring:message code="viewbatchcalibration.imgcancel"/>" />
											</a>			
										</c:when>
										<c:otherwise>
											&nbsp;
										</c:otherwise>
									</c:choose>
								</td>
								<td id="delForCal${cal.id}" class="center">
									<c:choose>
										<c:when test="${(cal.status.name == 'Awaiting work' || cal.status.name == 'On-going')}">
											<a href="#" class="mainlink" onclick=" removeCalibrationFromBatch(${cal.id}); "><spring:message code="viewbatchcalibration.del"/></a>
										</c:when>
										<c:otherwise>
											##&nbsp;
										</c:otherwise>
									</c:choose>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				
				<div class="center">
					<spring:message code="viewbatchcalibration.string2"/>
				</div>
				
			</div>
			<!-- end of batch calibration items infobox div -->					
			
			<!-- continue batch calibration button -->
			<div id="batchButtons" class="infobox center">							
				
				<c:if test="${form.batchCal.process.applicationComponent}">
					<input type="button" onclick=" window.location.href = 'continuebatchcalibration.htm?batchid=${form.batchCal.id}'; " value="<spring:message code="viewbatchcalibration.contbatch"/>" />
				</c:if>
			
				<c:if test="${form.canBeDeleted}">
					<input type="button" onclick=" deleteBatchCalibration( ${form.batchCal.id}, 'viewjob.htm?jobid=${form.batchCal.job.jobid}'); " value="<spring:message code="viewbatchcalibration.delbatch"/>" />
				</c:if>
				
				<input type="button" onclick=" window.location.href = 'addcalibrationstobatch.htm?batchid=${form.batchCal.id}'; " value="<spring:message code="viewbatchcalibration.addcalis"/>" />
			
				<c:if test="${form.batchCal.lastCertify && form.batchCal.process.applicationComponent}">
					
					<input type="button" onclick=" reprintCertsBoxy(); return false; " value="<spring:message code="viewbatchcalibration.reprintcerts"/>" />
					
					<!-- content of this div is used in reprint certs javascript pop up @see reprintCertsBoxy() -->
					<div id="certsThatCanBeReprinted" class="hid">										
						<table class="boxytable" id="reprintCertsTable" summary="<spring:message code='viewbatchcalibration.certsThatCanBeReprinted'/>">
							<thead>
								<tr>
									<td colspan="3"><spring:message code="viewbatchcalibration.reprintcertlist"/></td>
								</tr>
								<tr>
									<th class="select" scope="col">
										<input type="checkbox" onclick=" selectAllItems(this.checked, 'reprintCertsBoxy table#reprintCertsTable'); " /> <spring:message code="multistart.all"/>
									</th>
									<th class="itemno" scope="col"><spring:message code="viewbatchcalibration.itemno"/></th>
									<th class="certno" scope="col"><spring:message code="viewbatchcalibration.certno"/></th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<td colspan="3">&nbsp;</td>
								</tr>
							</tfoot>
							<tbody>
							
								<c:forEach var="calWrap" items="${form.calWrappers}">
									<c:set var="cal" value="${calWrap.cal}"/>
									<c:if test="${cal.certs.size() > 0}">
										<c:forEach var="cert" items="${cal.certs}">
											<tr>
												<td><input type="checkbox" name="selectedCertIds" value="${cert.certid}" /></td>
												<td>Item ${cal.links.iterator().next().ji.itemNo}</td>
												<td>${cert.certno}</td>
											</tr> 
										</c:forEach>											
									</c:if>
								</c:forEach>
							</tbody>
						</table>									
					</div>
					
				</c:if>							
				
				<form:form name="batchActionForm" id="batchActionForm" modelAttribute="form" action="" method="post" onsubmit=" checkItemsToBeInvoiced(${form.batchCal.id}); return false; ">
					<fieldset>
						<legend><spring:message code="viewbatchcalibration.actions"/>:</legend>
						
						<ol>
							<c:if test="${otherCompleteBatches.size() > 0}">
								<li>
									<label><spring:message code="viewbatchcalibration.complbatches"/></label>
									<div class="float-left">
										<c:forEach var="completeBatchWrapper" items="${otherCompleteBatches}">
											<c:set var="completeBatch" value="${completeBatchWrapper.batchCal}"/>
											<input type="checkbox" name="selectedBatchIds" value="${completeBatch.id}" /> 
												<spring:message code="viewbatchcalibration.batch"/> ${completeBatch.id}: ${completeBatch.owner.name} - ${completeBatch.process.process}  (${completeBatch.cals.size()} <spring:message code="viewbatchcalibration.calibrations"/>):
											<br/>
											<c:choose>
												<c:when test="${completeBatchWrapper.missingFiles < 1}">
													<span>&nbsp;&nbsp;&nbsp;&nbsp;(<spring:message code="viewbatchcalibration.allfilespres"/>)</span>
												</c:when>
												<c:otherwise>														
													<span style="color: red; font-weight: bold;">&nbsp;&nbsp;&nbsp;&nbsp;(<spring:message code="viewbatchcalibration.warning"/>: ${completeBatchWrapper.missingFiles} <spring:message code="viewbatchcalibration.filearemiss"/> 
													<a href="file:///${form.jobCertDirectory}" target="_blank" class="mainlink"><spring:message code="viewbatchcalibration.certdirect"/></a>)</span>
												</c:otherwise>
											</c:choose>
											<br/>
										</c:forEach>
									</div>
									<div class="clear"></div>
								</li>
							</c:if>
							<li>
								<form:label path="certify"><spring:message code="viewbatchcalibration.certify"/></form:label>
								<c:set var="disabled" value=""/>
								<c:set var="onClick" value=""/>
								<c:choose>
                                    <c:when test="${form.canCertify.success == true}">
                                        <c:set var="checked" value="checked"/>
                                    </c:when>
                                    <c:otherwise>
                                        <c:set var="disabled" value="disabled"/>
                                    </c:otherwise>
                                </c:choose>
								
								<c:if test="${form.autoPrinting == false}"> 
									<c:set var="onClick" value="toggleFields(this, 'passwordFields');"/>
								</c:if>
								
								<form:checkbox path="certify" disabled="${disabled}" onClick="${onClick}"/>
								<c:choose>
									<c:when test="${form.batchCal.lastCertify}">
									[<spring:message code="viewbatchcalibration.lastcert"/>: <fmt:formatDate value="${form.batchCal.lastCertify}" type="both" dateStyle="SHORT" timeStyle="SHORT" />]
									</c:when>
									<c:otherwise>
									[<spring:message code="viewbatchcalibration.notyetcert"/>]
									</c:otherwise>
								</c:choose>
								[${form.canCertify.message}]
								
								<br/>
								<c:choose>
									<c:when test="${missingFiles < 1}"> 
										<span>&nbsp;&nbsp;&nbsp;&nbsp;(<spring:message code="viewbatchcalibration.allfilespres"/>)</span>
									</c:when>
									<c:otherwise> 														
										<span style="color: red; font-weight: bold;">&nbsp;&nbsp;&nbsp;&nbsp;(<spring:message code="viewbatchcalibration.warning"/>: ${missingFiles} <spring:message code="viewbatchcalibration.filearemiss"/> 
										<a href="file:///${form.jobCertDirectory}" target="_blank" class="mainlink"><spring:message code="viewbatchcalibration.certdirect"/></a>)</span>
									</c:otherwise>
								</c:choose>
								
								<c:if test="${form.autoPrinting == false}">
									<ol id="passwordFields" <c:if test="${form.canCertify.success == false}"> class="hid" </c:if>>
										<c:choose>
											<c:when test="${not empty currentContact.encryptedPassword}">
											<li>
												<label><spring:message code="completecalibration.passw"/>:</label>
												<form:input type="password" path="certify" />
											</li>
											</c:when>
											<c:otherwise>
											<li>
												<fieldset>
													<legend><spring:message code="newbatchcalibration.legend1"/>:</legend>
												</fieldset>
											</li>
											<li>
												<label><spring:message code="completecalibration.passw"/>:</label>
												<form:input type="password" path="certify" onkeyup=" passwordStrength(this.value); " />
											</li>
											<li>
												<label><spring:message code="completecalibration.confpassw"/>:</label>
												<input type="password" path="certify" />
											</li>
											<li>
												<label><spring:message code="completecalibration.strength"/>:</label>
												<div id="passwordDescription"><spring:message code="completecalibration.passwnotent"/></div>
							                    <div id="passwordStrength" class="strength0"></div>
												<!-- clear floats and restore page flow -->
												<div class="clear"></div>
											</li>										
											</c:otherwise>
										</c:choose>
									</ol>
								</c:if>
							</li>
							<li>
								<label path="despatch"><spring:message code="viewbatchcalibration.despatch"/></label>
								<form:checkbox id="despatch" path="despatch" onclick=" toggleFields(this, 'courierFields'); " checked="${checked}" />
								<c:choose>
									<c:when test="${not empty form.batchCal.lastDespatch}">
										[<spring:message code="viewbatchcalibration.lastdespat"/>:<fmt:formatDate type="both" dateStyle="SHORT" timeStyle="short" value="${form.batchCal.lastDespatch}"/>]
									</c:when>
									<c:otherwise>
										[<spring:message code="viewbatchcalibration.notyetdespat"/>]
									</c:otherwise>
								</c:choose>
							
								<c:if test="${form.batchRequiresCourier == true}">
									<ol id="courierFields">
										<li>
											<span><spring:message code="viewbatchcalibration.courierfields"/>:</span>
										</li>
											
										<li>
												<form:label path="cdtId"><spring:message code="viewbatchcalibration.courdespattype"/>:</form:label>
												<form:select path="cdtId" id="cdtId">
													<%-- Formerly had option group with courier names - map in controller if needed in future --%>
													<c:forEach var="cdt" items="${courierDespatchTypes}">
														<option value="${cdt.id}">${cdt.courierName} - ${cdt.name}</option>
													</c:forEach>
												</form:select>
										</li>
										<li>
											<form:label path="consignmentNo"><spring:message code="viewbatchcalibration.consno"/>:</form:label>
											<form:input type="text" id="consignmentNo" path="consignmentNo" size="30" />
										</li>
									</ol>
								</c:if>	
							</li>
							<li>
								<form:label path="invoice"><spring:message code="viewbatchcalibration.invoice"/></form:label>
								<c:set var="checked" value=""/>
								<c:set var="disabled" value=""/>
								<c:choose>
									<c:when test="${not empty allowInvoicing}">
										<c:set var="checked" value="checked"/>
									</c:when>
									<c:otherwise> 
									 <c:set var="disabled" value="disabled"/>
									</c:otherwise>
								</c:choose>
								<form:checkbox id="invoice" path="invoice" disabled="${disabled}"/>
								 
								<c:choose>
									<c:when test="${not empty form.batchCal.lastInvoice}">
										[<spring:message code="viewbatchcalibration.lastinvoi"/>: <fmt:formatDate type="both" dateStyle="SHORT" timeStyle="SHORT" value="${form.batchCal.lastInvoice}"/>]
									</c:when>
									<c:otherwise>
										[<spring:message code="viewbatchcalibration.notyetinvoi"/>]
									</c:otherwise>
								</c:choose>
							</li>
							<li>
								<form:label path="invType"><spring:message code="viewbatchcalibration.invoiscope"/>:</form:label>
								<form:select path="invType" id="invType">
									<c:forEach var="invt" items="${form.invTypes}">
										<option value="${invt}" <c:if test="${invt == 'ALL_BATCH_ITEMS'}"> selected="selected" </c:if>>${invt.desc}</option>											
									</c:forEach>
								</form:select>
							</li>
							
							<li>
								<form:label path="labels"><spring:message code="viewbatchcalibration.printlab"/></form:label>
								<c:set var="checked" value=""/>
								<c:set var="disabled" value=""/>
								<c:choose>
									<c:when test="${not empty allowLabelPrinting}"> 
									<c:set var="checked" value="checked"/>
									 </c:when>
									<c:otherwise>
										<c:set var="disabled" value="disabled"/>
									 </c:otherwise>
								</c:choose>
								<form:checkbox id="labels" path="labels" onclick=" toggleFields(this, 'labelFields'); " disabled="${disabled}"
								 />
								
								<c:choose>
									<c:when test="${not empty form.batchCal.lastLabels}">
										[<spring:message code="viewbatchcalibration.lastlabprint"/>: <fmt:formatDate type="both" dateStyle="SHORT" timeStyle="SHORT" value="${form.batchCal.lastLabels}"/>]
									</c:when>
									<c:otherwise>
										[<spring:message code="viewbatchcalibration.notyetprint"/>]
									</c:otherwise>
								</c:choose>
							
								<ol id="labelFields" <c:choose>
														<c:when test="${not empty allowLabelPrinting}"> class="vis" </c:when>
														<c:otherwise> class="hid" </c:otherwise>
													</c:choose>>
									<li>
										<label><spring:message code="viewbatchcalibration.inclrecdate" />:</label>
										<c:set var="checked" value=""/>
										<c:if test="${form.includeRecallDateOnLabels == true}"> 
											<c:set var="checked" value="checked"/>
										</c:if>
										<form:checkbox path="includeRecallDateOnLabels" />
									</li>								
								</ol>
							</li>
							<li>
								<label>&nbsp;</label>
								<input type="submit" value="save"/>
							</li>
						</ol>
					</fieldset>
				</form:form>
			</div>
    </jsp:body>
</t:crocodileTemplate>