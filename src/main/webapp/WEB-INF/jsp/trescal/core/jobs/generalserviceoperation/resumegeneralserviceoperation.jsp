<%-- File name: /trescal/core/jobs/calibration/resumecalibration.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="generalserviceoperation.resume.headtext"/></span>
	</jsp:attribute>
	<jsp:body>
		<t:showErrors path="form.*" showFieldErrors="true"/>
		<div class="infobox">
			<p>
				<a href="jiactions.htm?jobitemid=${form.ji.jobItemId}" class="mainlink">
					<spring:message code="resumecalibration.backtojob" />
				</a>
			</p>
		</div>						
	</jsp:body>
</t:crocodileTemplate>