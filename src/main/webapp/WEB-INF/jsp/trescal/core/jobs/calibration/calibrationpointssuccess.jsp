<%-- File name: /trescal/core/jobs/calibration/calibrationpointssuccess.jsp --%><%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
		
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<html>

	<head>
		
		<link rel="stylesheet" href="styles/global.css" type="text/css" title="antech" />
		
		<link rel="stylesheet" href="styles/print/print.css" type="text/css" media="print" />
		
		<script type="text/javascript">
			
			// set variable to default context of project
			var springUrl = "${req.contextPath}";
			// set variable to i18n of javascript
			var i18locale = '${rc.locale.language}';
			// end js hide -->
			
		</script>
		
		<script type='text/javascript' src='script/thirdparty/jQuery/jquery.js'></script>
		
		<script type='text/javascript' src='script/trescal/core/template/Cwms_Main.js'></script>
		
		<script type='text/javascript' src='script/trescal/core/jobs/calibration/CalibrationPointsSuccess.js'></script>
		
	</head>
	
	<body onload=" cleanUpAfterPoints('${form.cr.classKey}', ${form.cr.id}, '${form.page}', ${form.cr.genericId}); ">
						
	</body>
	
</html>