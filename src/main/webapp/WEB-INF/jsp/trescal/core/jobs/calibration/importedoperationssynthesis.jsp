<%-- File name: /trescal/core/jobs/job/jobsearch.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="link" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<t:crocodileTemplate>
	<jsp:attribute name="scriptPart">
	
	
<style>
.thickTopBorder {
	border-top: 2px solid #aaa;
}

/* column visibility */ 
.dt-button-collection {
    width: unset !important;
}

.dt-button-collection div {
	column-count: 6 !important;
}
.buttons-columnVisibility {
	display: inline-block !important;
    min-width: 150px !important;
}
.text-center {
	text-align : center !important;
}
</style>
	
		<c:set var="pageErrors">
			<form:errors path="*" />
		</c:set>
		
		<script type='text/javascript'>
			var selectedJobId = null;
			<c:if test="${not empty form.jobId}">
				selectedJobId = ${form.jobId};
			</c:if>
			var pageHasErrors = ${not empty pageErrors};
		</script>
		
		<script type='text/javascript'>
			
			/* get data: jobs, jobitems and workrequirements*/
		
			var joJiWr = new Array ();
			var jiWr = new Array ();
			var wrs = new Array();
			<c:forEach var="jj" items="${joJiWr}">
				<c:set var="job" value="${jj.key}" />
				<c:set var="jiWr" value="${jj.value}" />
				jiWr = new Array();
			
				<c:forEach var="jw" items="${jiWr}">
					<c:set var="jobitem" value="${jw.key}" />
					<c:set var="workrequirments" value="${jw.value}" />
					wrs = new Array();
					
					<c:forEach var="jiwr" items="${workrequirments}" varStatus="wrLoopStatus" >
						wrs.push({
							wrId : ${jiwr.id},
							wrText : "${wrLoopStatus.index+1}/${workrequirments.size()} - ${ empty jiwr.workRequirement.procedure.reference ? 'No procedure' : jiwr.workRequirement.procedure.reference } - <cwms:besttranslation translations='${jiwr.workRequirement.serviceType.shortnameTranslation}'/>",
							nextWr : ${ jobitem.nextWorkReq.id eq jiwr.id},
							serviceTypeId : ${ jiwr.workRequirement.serviceType.serviceTypeId }
						});
					</c:forEach>
					jiWr.push({
						jobitemId : ${jobitem.jobItemId},
						itemNo : ${jobitem.itemNo},
						state : "<cwms:besttranslation escapeJavaScript='true' translations='${jobitem.state.translations}'/>",
						instPlantidText : "<spring:message code='trescalid' />:  ${ jobitem.inst.plantid }",
						instPlantnoText : "<spring:message code='plantno' />:  ${ jobitem.inst.plantno }",
						instPlantid : ${ jobitem.inst.plantid },
						instPlantno : "${ jobitem.inst.plantno }",
						contractReviewed : ${ jobitemContractReviewed.get(jobitem.jobItemId) },
						workrequirements : wrs
					});
				</c:forEach>
				joJiWr.push({
					jobId : ${job.jobid},
					jobno : "${job.jobno}",
					jiWrs : jiWr
				});
			</c:forEach>
		</script>
		
		<script type="module" src="script/components/cwms-overlay/cwms-overlay.js" ></script>
		<script type="module" src="script/components/cwms-translate/cwms-translate.js" ></script>
		<script src='script/trescal/core/jobs/calibration/ImportedCalibrationsSynthesis.js'></script>
		<script type="module" src='script/trescal/core/jobs/calibration/ImportedOperationsSynthesisModule.js'></script>
			
		
	</jsp:attribute>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message
				code="importcalibrations.title" /></span>
	</jsp:attribute>

		
	<jsp:body>
			
		<div class="warningBox1 hid" id="warningBox">
			<div class="warningBox2">
				<div class="warningBox3 center-0">
					<spring:message code="calibrationimportation.norecodselected"/>
				</div>
			</div>
		</div>
		
		<spring:bind path="form.*">
			<c:if test="${status.error}">
				<div class="warningBox1">
					<div class="warningBox2">
						<div class="warningBox3">
							<div class="center attention">
								<spring:message code='repairinspection.validator.generalmessage' />
								<%-- For now, display the first error message (and some counts) just to help the user know --%> 
								<%--  what sort of error they would be searching for, which can be in a very large table --%>
								<div><c:out value="First Error: ${status.errorMessage}" /></div>
								<div><c:out value="Field Error Count : ${status.errors.fieldErrorCount}" /></div>
								<div><c:out value="Global Error Count : ${status.errors.globalErrorCount}" /></div>
<%-- 								<c:forEach var="errorMessage" items="${status.errorMessages}">  --%>
<%--  									<div class="center attention">${errorMessage}</div>  --%>
<%-- 								</c:forEach>  --%>
							</div>
						</div>
					</div>
				</div>
			</c:if>
		</spring:bind>
	
		<form:form modelAttribute="form" id="importedCalibrationsForm">
		
			<div class="infobox">
				<fieldset>
					<legend>
						<spring:message code="job" />
					</legend>
					<!-- displays content in left column -->
					<div class="displaycolumn-60">
						<ol>											
							<li>
								<label><spring:message code="company" />:</label>
								<link:companyLinkDWRInfo company="${subdiv.comp}" rowcount="1"
									copy="true" />
							</li>
							<li>
								<label><spring:message code="subdiv" />:</label>
								<link:subdivLinkDWRInfo rowcount="1" subdiv="${subdiv}" />
								<form:hidden path="subdivid" />
							</li>
							<li>
								<label><spring:message code="jobtype.site" />:</label>
								<form:select path="jobId" id="jobSelector"
									onchange="updateJobItemSelectors();">
								
								</form:select>
							</li>
							<form:hidden path="exchangeFormatId" />
						</ol>
					</div>
				</fieldset>
			</div>
			<div class="infobox">
				<a href="importoperations.htm" class="mainlink-float">
					<spring:message code="importcalibrations.importagain" />
				</a>
				<a href="" id="missingJobitems"
					class="mainlink-float">
					<spring:message code="importcalibrations.createmissingjobitems" />
				</a>
				<a href="<c:url value='/addinstrumentmodelsearch.htm' />"
					class="mainlink-float" target="_blank">
						<spring:message code="instrument.new.submit" />
				</a>
				
				<table id="importedCalibration" class="default2">
					<c:set var="numberOfFixedColumns" value="4" />									
					<c:set var="colspan"
						value="${ numberOfFixedColumns + tableHeader.keySet().size() }" />
					<thead>
						<tr>
							<th class="text-center" ><spring:message code="calibration" /> #	
								<input type="checkbox" id="selectRecordsToImport" onclick="selectedRecordsToImport();">
							</th>
							<c:forEach items="${ tableHeader.keySet() }" var="c" varStatus="vs">
								<!-- showing the first 9 columns -->
								<th class="codefault erp_data ${ vs.index > 9 ? 'hiddenByDefault':'' }" data-type="${ tableHeader[c] }">${c}</th>
							</c:forEach>
							<th><spring:message code="vm_global.jobitem" />/<spring:message
									code="vm_global.workrequirements" /></th>
							<th class="text-center" ><spring:message code="jicontractreview.contrrew" />
								<input type="checkbox" id="auto-contractReview"
								onclick="CheckautomaticContractReviewCheckBox();">
							</th>
						</tr>
					</thead>												
					
					<tbody>
						<c:choose>
							<c:when test="${empty form.rows}">
								<tr class="odd">
									<td colspan="${ colspan }" class="center bold">
										<spring:message code="noresults" />
									</td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:forEach var="row" items="${form.rows}" varStatus="status">
								
									<c:choose>
										<c:when test="${status.index % 2 == 0}">
											<c:set var="rowClass" value="odd" />
										</c:when>
										<c:otherwise>
											<c:set var="rowClass" value="even" />
										</c:otherwise>
									</c:choose>
									
									<c:set var="instrumentPlantIdErrors">
										<form:errors class="error" path="rows[${ status.index }].plantid" />
									</c:set>
									
									<tr class="calFirstRow thickTopBorder ${rowClass}" 
										${ (empty instrumentPlantIdErrors and not empty row.plantid) ? 'instrumentIdentified' : '' }
										plantid="${ row.plantid }" plantno="${ row.plantno }">
										
										<td class="text-center">
											<c:if test="${row.rowspan > 0 }">
												<form:checkbox class="selectCheckbox" id="select-RecordsToImport-${ status.index }" path="rows[${ status.index }].checked"/>
											</c:if>
										</td>
											
										<c:forEach items="${ tableHeader.keySet() }" var="headVal">
											<c:choose>
												<c:when test="${tableHeader[headVal] eq 'JOBITEM_EQUIVALENT_ID'  }">
													<td index="" ${row.rowspan > 0 ? 'grouped':'' } data-type="${tableHeader[headVal]}">${ row.jobitemEquivalentId}<br/>
															<form:errors class="error" path="rows[${status.index}].jobitemEquivalentId" />
													</td>
													<form:hidden path="rows[${status.index}].jobitemEquivalentId" />
													<form:hidden path="rows[${status.index}].rowspan"/>
												</c:when>
											
												<c:when test="${tableHeader[headVal] eq 'ID_TRESCAL'}">
													<td ${row.rowspan > 0 ? 'grouped':'' } data-type="${tableHeader[headVal]}">${ row.plantid }  <br/>
													    <c:if
															test="${ empty instrumentPlantIdErrors and not empty row.plantid }">&nbsp;
															<a target="_blank" class="viewinstrumentsLink"
																href="viewinstrument.htm?plantid=${ row.plantid }&ajax=instrument&width=500">(${ row.plantid })</a>
														</c:if>
														<form:errors class="error max-wrap" path="rows[${ status.index }].plantid" />
													</td>
													<form:hidden path="rows[${ status.index }].plantid" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'PLANT_NO'}">
													<td ${row.rowspan > 0 ? 'grouped':'' } data-type="${tableHeader[headVal]}">${ row.plantno }<br/>
													<form:errors class="error" path="rows[${ status.index }].plantno" /></td>
													<form:hidden path="rows[${ status.index }].plantno" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'CUSTOMER_DESCRIPTION'}">
													<td data-type="${tableHeader[headVal]}">${ row.customerDescription }</td>
													<form:hidden path="rows[${ status.index }].customerDescription" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'BRAND'}">
													<td ${row.rowspan > 0 ? 'grouped':'' } data-type="${tableHeader[headVal]}">${ row.brand }</td>
													<form:hidden path="rows[${ status.index }].brand" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'MODEL'}">
													<td ${row.rowspan > 0 ? 'grouped':'' } data-type="${tableHeader[headVal]}">${ row.model }</td>
													<form:hidden path="rows[${ status.index }].model" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'SERIAL_NUMBER'}">
													<td ${row.rowspan > 0 ? 'grouped':'' } data-type="${tableHeader[headVal]}">
														${ row.serialNo } <br/>
														<form:errors class="error max-wrap" path="rows[${ status.index }].serialNo" />
													</td>
													<form:hidden path="rows[${ status.index }].serialNo" />
													
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'EXPECTED_SERVICE'}">
													<td data-type="${tableHeader[headVal]}"
														data-servicetypeid="${ row.serviceTypeId }" >${ row.service }<br/>
														<form:errors class="error" path="rows[${ status.index }].service" />
													</td>
													<form:hidden path="rows[${ status.index }].service" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'SERVICETYPE_ID'}">
													<td ${row.rowspan > 0 ? 'grouped':'' } 
														data-servicetypeid="${ row.serviceTypeId }"
														data-type="${tableHeader[headVal]}" >${ row.serviceTypeId } (
														<c:forEach items="${serviceTypes}" var="st"> 
															<c:if test="${st.key eq row.serviceTypeId }">
	                                                               ${st.value} 
	                                                         </c:if>		             	
	                                                    </c:forEach>)
                                                    </td>
													<input type="hidden" name="rows[${status.index}].serviceTypeId" value="${ row.serviceTypeId }" />
												</c:when>
												
												<c:when
													test="${tableHeader[headVal] eq 'TECHNICAL_REQUIREMENTS'}">
													<td data-type="${tableHeader[headVal]}">${ row.technicalRequirements }</td>
													<form:hidden path="rows[${ status.index }].technicalRequirements" />
												</c:when>
												
												<c:when
													test="${tableHeader[headVal] eq 'CUSTOMER_COMMENTS'}">
													<td data-type="${tableHeader[headVal]}">${ row.customerComments }</td>
													<form:hidden path="rows[${ status.index }].customerComments" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'OPERATION_BY_HRID'}">
													<td data-type="${tableHeader[headVal]}">
														${ row.operationByHrId }
														<c:if test="${ not empty row.operationBy }">
															(<link:contactLinkDWRInfo contact="${row.operationBy}"
																rowcount="1"></link:contactLinkDWRInfo>)
														</c:if>
														<br/>
														<form:errors class="error" path="rows[${ status.index }].operationByHrId" />
													</td>
													<form:hidden path="rows[${ status.index }].operationByHrId" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'OPERATION_DATE'}">
													<td data-type="${tableHeader[headVal]}">
														<fmt:formatDate type="both" dateStyle="SHORT" timeStyle ="short" value="${row.operationDate}" />
														<br/>
														<form:errors class="error" path="rows[${ status.index }].operationDate" />
													</td>
													<form:hidden path="rows[${ status.index }].operationDate" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'OPERATION_START_DATE'}">
													<td data-type="${tableHeader[headVal]}">
														<fmt:formatDate type="both" dateStyle="SHORT" timeStyle ="short"
															value="${row.operationStartDate}" />
														<br/>
														<form:errors class="error" path="rows[${ status.index }].operationStartDate" />
													</td>
													<form:hidden path="rows[${ status.index }].operationStartDate" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'OPERATION_DURATION'}">
													<td data-type="${tableHeader[headVal]}" class="text-center">
														${ row.operationDuration }
														<br/>
														<form:errors class="error" path="rows[${ status.index }].operationDuration" />
													</td>
													<form:hidden path="rows[${ status.index }].operationDuration" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'OPERATION_TYPE'}">
													<td data-type="${tableHeader[headVal]}">${ row.operationType }<br/>
														<form:errors class="error" path="rows[${ status.index }].operationType" />
													</td>
													<form:hidden path="rows[${ status.index }].operationType" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'DOCUMENT_NO'}">
													<td data-type="${tableHeader[headVal]}">${ row.documentNumber }<br/>
														<form:errors class="error" path="rows[${ status.index }].documentNumber" />
													</td>
													<form:hidden path="rows[${ status.index }].documentNumber" />
												</c:when>
												
												<c:when
													test="${tableHeader[headVal] eq 'OPERATION_VALIDATED_ON'}">
													<td data-type="${tableHeader[headVal]}">
														<fmt:formatDate type="date" dateStyle="SHORT"
															value="${row.operationValidatedOn}" />
														<br/>
														<form:errors class="error" path="rows[${ status.index }].operationValidatedOn" />
													</td>
													<form:hidden path="rows[${ status.index }].operationValidatedOn" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'OPERATION_VALIDATED_BY_HRID'}">
													<td data-type="${tableHeader[headVal]}">
														${ row.operationValidatedByHrId }
														<c:if test="${ not empty row.operationValidatedBy }">
															(<link:contactLinkDWRInfo
																contact="${row.operationValidatedBy}" rowcount="1"></link:contactLinkDWRInfo>)
														</c:if>
														<br/>
														<form:errors class="error" path="rows[${ status.index }].operationValidatedByHrId" />
													</td>
													<form:hidden path="rows[${ status.index }].operationValidatedByHrId" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'CALIBRATION_OUTCOME'}">
													<td data-type="${tableHeader[headVal]}">
														${ row.calibrationOutcome }
														<br/>
														<form:errors class="error" path="rows[${ status.index }].calibrationOutcome" />
													</td>
													<form:hidden path="rows[${ status.index }].calibrationOutcome" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'CAL_FREQUENCY'}">
													<td data-type="${tableHeader[headVal]}">
														${ row.calibrationFrequency }
														<br/>
														<form:errors class="error" path="rows[${ status.index }].calibrationFrequency" />
													</td>
													<form:hidden path="rows[${ status.index }].calibrationFrequency" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'INTERVAL_UNIT'}">
													<td data-type="${tableHeader[headVal]}">
														${ row.intervalUnit }
														<br/>
														<form:errors class="error" path="rows[${ status.index }].intervalUnit" />
													</td>
													<form:hidden path="rows[${ status.index }].intervalUnit" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'NEXT_CAL_DUEDATE'}">
													<td data-type="${tableHeader[headVal]}">
														<fmt:formatDate type="date" dateStyle="SHORT" 
															value="${row.nextCalDueDate}" />
														<br/>
														<form:errors class="error" path="rows[${ status.index }].nextCalDueDate" />
													</td>
													<form:hidden path="rows[${ status.index }].nextCalDueDate" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'CALIBRATION_CLASS'}">
													<td data-type="${tableHeader[headVal]}">
														${ row.calibrationClass }
														<br/>
														<form:errors class="error" path="rows[${ status.index }].calibrationClass" />
													</td>
													<form:hidden path="rows[${ status.index }].calibrationClass" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'CALIBRATION_VERIFICATION_STATUS'}">
													<td data-type="${tableHeader[headVal]}">
														${ row.calibrationVerificationStatus }
														<br/>
														<form:errors class="error" path="rows[${ status.index }].calibrationVerificationStatus" />
													</td>
													<form:hidden path="rows[${ status.index }].calibrationVerificationStatus" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'ADJUSTMENT'}">
													<td data-type="${tableHeader[headVal]}">
														${ row.adjustment }
														<br/>
														<form:errors class="error" path="rows[${ status.index }].adjustment" />
													</td>
													<form:hidden path="rows[${ status.index }].adjustment" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'OPTIMIZATION'}">
													<td data-type="${tableHeader[headVal]}">
														${ row.optimization }
														<br/>
														<form:errors class="error" path="rows[${ status.index }].optimization" />
													</td>
													<form:hidden path="rows[${ status.index }].optimization" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'RESTRICTION'}">
													<td data-type="${tableHeader[headVal]}">
														${ row.restiction }
														<br/>
														<form:errors class="error" path="rows[${ status.index }].restiction" />
													</td>
													<form:hidden path="rows[${ status.index }].restiction" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'REPAIR'}">
													<td data-type="${tableHeader[headVal]}">
														${ row.repair }
														<br/>
														<form:errors class="error" path="rows[${ status.index }].repair" />
													</td>
													<form:hidden path="rows[${ status.index }].repair" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'FR_STATES'}">
													<td data-type="${tableHeader[headVal]}">
														${ row.frStates }
														<br/>
														<form:errors class="error" path="rows[${ status.index }].frStates" />
													</td>
													<form:hidden path="rows[${ status.index }].frStates" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'FR_STATE_OTHER'}">
													<td data-type="${tableHeader[headVal]}">
														${ row.frOtherState }
														<br/>
														<form:errors class="error" path="rows[${ status.index }].frOtherState" />
													</td>
													<form:hidden path="rows[${ status.index }].frOtherState" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'FR_TECHNICIAN_COMMENTS'}">
													<td data-type="${tableHeader[headVal]}">
														${ row.frTechnicianComment }
														<br/>
														<form:errors class="error" path="rows[${ status.index }].frTechnicianComment" />
													</td>
													<form:hidden path="rows[${ status.index }].frTechnicianComment" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'FR_RECOMMENDATIONS'}">
													<td data-type="${tableHeader[headVal]}">
														${ row.frRecommendations }
														<br/>
														<form:errors class="error" path="rows[${ status.index }].frRecommendations" />
													</td>
													<form:hidden path="rows[${ status.index }].frRecommendations" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'FR_DISPENSATION_COMMENTS'}">
													<td data-type="${tableHeader[headVal]}">
														${ row.frDispensationComment }
														<br/>
														<form:errors class="error" path="rows[${ status.index }].frDispensationComment" />
													</td>
													<form:hidden path="rows[${ status.index }].frDispensationComment" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'FR_VALIDATION_COMMENTS'}">
													<td data-type="${tableHeader[headVal]}">
														${ row.frValidationComment }
														<br/>
														<form:errors class="error" path="rows[${ status.index }].frValidationComment" />
													</td>
													<form:hidden path="rows[${ status.index }].frValidationComment" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'FR_SEND_TO_CLIENT'}">
													<td data-type="${tableHeader[headVal]}">
														${ row.frSendToClient }
														<br/>
														<form:errors class="error" path="rows[${ status.index }].frSendToClient" />
													</td>
													<form:hidden path="rows[${ status.index }].frSendToClient" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'FR_CLIENT_RESPONSE_NEEDED'}">
													<td data-type="${tableHeader[headVal]}">
														${ row.frClientResponseNeeded }
														<br/>
														<form:errors class="error" path="rows[${ status.index }].frClientResponseNeeded" />
													</td>
													<form:hidden path="rows[${ status.index }].frClientResponseNeeded" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'FR_FINAL_OUTCOME'}">
													<td data-type="${tableHeader[headVal]}">
														${ row.frFinalOutcome }
														<br/>
														<form:errors class="error" path="rows[${ status.index }].frFinalOutcome" />
													</td>
													<form:hidden path="rows[${ status.index }].frFinalOutcome" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'FR_CLIENT_APPROVAL_ON'}">
													<td data-type="${tableHeader[headVal]}">
														<fmt:formatDate type="both" dateStyle="SHORT" timeStyle ="short"
															value="${ row.frClientApprovalOn }" />
														<br/>
														<form:errors class="error" path="rows[${ status.index }].frClientApprovalOn" />
													</td>
													<form:hidden path="rows[${ status.index }].frClientApprovalOn" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'FR_CLIENT_DECISION'}">
													<td data-type="${tableHeader[headVal]}">
														${ row.frClientDecision }
														<br/>
														<form:errors class="error" path="rows[${ status.index }].frClientDecision" />
													</td>
													<form:hidden path="rows[${ status.index }].frClientDecision" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'FR_CLIENT_COMMENTS'}">
													<td data-type="${tableHeader[headVal]}">
														${ row.frClientComments }
														<br/>
														<form:errors class="error" path="rows[${ status.index }].frClientComments" />
													</td>
													<form:hidden path="rows[${ status.index }].frClientComments" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'FR_OPERATION_BY_TRESCAL'}">
													<td data-type="${tableHeader[headVal]}">
														${ row.frOperationByTrescal }
														<br/>
														<form:errors class="error" path="rows[${ status.index }].frOperationByTrescal" />
													</td>
													<form:hidden path="rows[${ status.index }].frOperationByTrescal" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'FR_ESTIMATED_DELIVERY_TIME'}">
													<td data-type="${tableHeader[headVal]}">
														${ row.frEstimatedDeliveryTime }
														<br/>
														<form:errors class="error" path="rows[${ status.index }].frEstimatedDeliveryTime" />
													</td>
													<form:hidden path="rows[${ status.index }].frEstimatedDeliveryTime" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'FR_SUBDIV_ID'}">
													<td data-type="${tableHeader[headVal]}">
														${ row.frOperationByTrescal }
														<br/>
														<form:errors class="error" path="rows[${ status.index }].frSubdivisionId" />
													</td>
													<form:hidden path="rows[${ status.index }].frSubdivisionId" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'FR_CLIENT_ALIAS'}">
													<td data-type="${tableHeader[headVal]}">
														${ row.frClientAlias }
														<br/>
														<form:errors class="error" path="rows[${ status.index }].frClientAlias" />
													</td>
													<form:hidden path="rows[${ status.index }].frClientAlias" />
												</c:when>
												
												<c:when test="${tableHeader[headVal] eq 'UNDEFINED'}">
													<td>
														${ row.undefineds[headVal] }
														<br/>
														<form:errors class="error" path="rows[${ status.index }].undefineds[${headVal}]" />
													</td>
													<form:hidden path="rows[${ status.index }].undefineds[${headVal}]" />
												</c:when>
												
											</c:choose>
		 								</c:forEach>
		 								
		 								<c:if test="${row.rowspan > 0 }">
											<td rowspan="${row.rowspan}">
												<!-- jobitem / wrq -->
												<form:select class="jobitemSelector"
													path="rows[${ status.index }].jobitemId"
													onchange="jobitemSelectorOnChange(this);"
													preselectedvalue="${ row.jobitemId }">
													
												</form:select>
												<br/>
												<a target="_blank" class="mainlink linktojobitem" href="">
													<spring:message code="purchaseorder.linktojobitem" />
												</a>
												<br/>
												<form:errors class="error" path="rows[${ status.index  + loop.index }].jobitemId" />
												<br/>
												<form:select class="workRequirementSelector"
													path="rows[${ status.index }].jiWrId"
													preselectedvalue="${ row.jiWrId }">
												</form:select>
												<br/>
												<a href="#" class="mainlink" 
													onclick=" event.preventDefault(); workRequirementCatalog(this); ">
													<spring:message code='vm_global.addnewworkrequirement'/>
												</a>
												<br/>
												<form:errors class="error" path="rows[${ status.index + loop.index }].jiWrId" />
											</td>
										 </c:if>
										
										<c:if test="${row.rowspan > 0 }">
											<td rowspan="${row.rowspan}" class="text-center">
												<!-- Contract review -->
												<div class="yes">
													<b><spring:message code="yes" /></b>
												</div>
												<div class="no">
													<b><spring:message code="no" /></b>
													<hr>
													<label><spring:message code="importedcalibrations.automaticcontractreview" /></label>
													<form:checkbox path="rows[${ status.index }].automaticContractReview"
														class="automaticContractReview" />
												</div>
												<br/>
												<form:errors class="error"
													path="rows[${ status.index }].contractReviewed" />
											</td>
										</c:if>
										<form:hidden class="contractReviewedHiddenInput" path="rows[${ status.index }].contractReviewed" />
									</tr>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>	
				
				<br>
				
				<center>
					<input type="submit" id="newAnalysisButton" name="newanalysis" class="button"
						value="<spring:message code='filesynthesis.buttonnewanalysis'/>" />
						
					<input type="submit" name="save" class="button" onclick="preventRedirectionsifNoRecordsSelected();"
						value="<spring:message code='submit' />" />
						
				</center>
			</div>
		</form:form>
	</jsp:body>
</t:crocodileTemplate>
	
	<cwms-overlay id="missingJobItemsValidation" >
		<cwms-translate slot="cwms-overlay-title" code="error" >Error</cwms-translate>
       <div slot="cwms-overlay-body">
        </div>
	</cwms-overlay>