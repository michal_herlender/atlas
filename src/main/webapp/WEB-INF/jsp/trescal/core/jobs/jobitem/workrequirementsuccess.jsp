<%-- File name: /trescal/core/jobs/jobitem/workrequirementsuccess.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:overlayTemplate bodywidth="780">
	<div id="newWorkRequirementOverlay">
		<fieldset>
				<ol>								
					<li>
						<label><spring:message code="businesscompany"/>:</label>
						<span>
							<%-- May be replaced with subdivision allocation of work requirement - TBD --%>
							<spring:message var="wrSubdiv" code="notapplicable" />
							<c:if test="${not empty wr.workInstruction}">
								<c:set var="wrCompany" value="${wr.workInstruction.organisation.coname}" />
							</c:if>
							${wrCompany}
						</span>
						<div class="clear-0"></div>
					</li>							
					<li>
						<label><spring:message code="servicetype"/>:</label>
						<span>
							<c:if test="${not empty wr.serviceType}">
								<cwms:besttranslation translations="${wr.serviceType.shortnameTranslation}"/>
								<c:out value=" - " />
								<cwms:besttranslation translations="${wr.serviceType.longnameTranslation}"/>
							</c:if>
						</span>
						<div class="clear-0"></div>
					</li>							
					<li>
						<label><spring:message code="capability"/>:</label>
						<span>
							<c:if test="${not empty wr.capability}">
								${wr.capability.reference}
								<c:out value=" - " />
								${wr.capability.name} 
							</c:if>
						</span>
						<div class="clear-0"></div>
					</li>
					<li>
						<label><spring:message code="workrequirement.type"/>:</label>
						<span>${wr.type}</span>
						<div class="clear"></div>
					</li>
					<li>
						<label><spring:message code="tools.department" />:</label>
						${wr.department.shortName} - ${wr.department.name}
						<div class="clear"></div>
					</li>
						<li>
							<label><spring:message code="workrequirement.text" />:</label>
							<span><c:out value="${wr.requirement}" /></span>
							<div class="clear"></div>
						</li>
					<li>
						<label>&nbsp;</label>
						<div>
							<input type="button" id="jiwr_close" value="<spring:message code='close' />" onclick="event.preventDefault(); window.parent.loadScript.closeOverlay(); " />
							<c:choose>
								<c:when test="${form.action == 'add'}">
									- <spring:message code="workrequirement.created" />
								</c:when>
								<c:when test="${form.action == 'edit'}">
									- <spring:message code="workrequirement.updated" />
								</c:when>
								<c:when test="${form.action == 'editNotAllowed'}">
									- <spring:message code="error.workrequirement.noteditable" />
								</c:when>
							</c:choose>
						</div>
						<div class="clear"></div>
					</li>
				</ol>
		</fieldset>					
	</div>
</t:overlayTemplate>