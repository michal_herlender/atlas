<%-- File name: /trescal/core/jobs/bposelection.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<table class="default2" id="bpotable" summary="This table displays the assigned blanket purchase order">
	<thead>
		<tr>
			<th id="bponum" scope="col"><spring:message code="bpo.bpono"/></th>
			<th id="bpocom" scope="col"><spring:message code="comment"/></th>
			<th id="bpovalfrom" scope="col"><spring:message code="bpo.validfrom"/></th>
			<th id="bpovalto" scope="col"><spring:message code="bpo.validto"/></th>
			<th id="bpolimit" scope="col"><spring:message code="bpo.limitamount"/></th>
			<th id="bpoassign" scope="col"><spring:message code="bpo.assignbpo"/></th>
		</tr>
	</thead>
	<tbody>
		<c:if test="${not empty contactBPOs}">
			<tr><td colspan="6" class="highlight-gold">Contact BPOs</td></tr>
			<c:forEach var="bpo" items="${contactBPOs}">
				<tr <c:if test="${bpo.defaultForJob}">class="highlight"</c:if>>
					<td>${bpo.poNumber}</td>
					<td>${bpo.comment}</td>
					<td class="center">${bpo.durationFrom}</td>
					<td class="center">${bpo.durationTo}</td>
					<td class="center">${bpo.limitAmount}</td>
					<td>
						<a href="" class="mainlink" onclick="event.preventDefault(); assignItemToBPO(${jobId}, ${jobItemId}, ${bpo.poId})">
							<spring:message code="bpo.assignitem"/>
						</a>
					</td>
				</tr>
			</c:forEach>
		</c:if>
		<c:if test="${not empty subdivBPOs}">
			<tr><td colspan="6" class="highlight-gold">Subdiv BPOs</td></tr>
			<c:forEach var="bpo" items="${subdivBPOs}">
				<tr <c:if test="${bpo.defaultForJob}">class="highlight-green"</c:if>>
					<td>${bpo.poNumber}</td>
					<td>${bpo.comment}</td>
					<td class="center">${bpo.durationFrom}</td>
					<td class="center">${bpo.durationTo}</td>
					<td class="center">${bpo.limitAmount}</td>
					<td>
						<a href="" class="mainlink" onclick="event.preventDefault(); assignItemToBPO(${jobId}, ${jobItemId}, ${bpo.poId})">
							<spring:message code="bpo.assignitem"/>
						</a>
					</td>
				</tr>
			</c:forEach>
		</c:if>
		<c:if test="${not empty companyBPOs}">
			<tr><td colspan="6" class="highlight-gold">Company BPOs</td></tr>
			<c:forEach var="bpo" items="${companyBPOs}">
				<tr <c:if test="${bpo.defaultForJob}">class="highlight"</c:if>>
					<td>${bpo.poNumber}</td>
					<td>${bpo.comment}</td>
					<td class="center">${bpo.durationFrom}</td>
					<td class="center">${bpo.durationTo}</td>
					<td class="center">${bpo.limitAmount}</td>
					<td>
						<a href="" class="mainlink" onclick="event.preventDefault(); assignItemToBPO(${jobId}, ${jobItemId}, ${bpo.poId})">
							<spring:message code="bpo.assignitem"/>
						</a>
					</td>
				</tr>
			</c:forEach>
		</c:if>
	</tbody>
</table>