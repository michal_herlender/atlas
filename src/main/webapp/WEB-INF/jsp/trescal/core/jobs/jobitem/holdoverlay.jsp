<%-- File name: /trescal/core/jobs/jobitem/onholdoverlay.jsp--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<div id="changeItemStatusBoxy" class="overflow marg-bot">
	<div id="onholdStatusContent">
		<form:form modelAttribute="form" action="" method="post" class="inline">
			<fieldset>
				<legend><spring:message code="jiactions.string3"/></legend>
				<ol>
					<li>
						<label><spring:message code="jicalloffitem.currstatus"/>:</label>
						<cwms:besttranslation translations="${jobItem.state.translations}"/>
					</li>
					<li>
						<c:choose>
							<c:when test="${jobItem.state.status && jobItem.state.hold}">
								<label>&nbsp;</label>
								<span><spring:message code="jiactions.string4"/></span>
							</c:when>
							<c:otherwise>
								<form:label path="holdStatusId"><spring:message code="jiactions.setstat"/>:</form:label>
								<form:select path="holdStatusId">
									<c:forEach var="status" items="${translatedholdstatuses}">
										<option value="${status.key}">${status.value}</option>
							    	</c:forEach>
							    </form:select>
							</c:otherwise>
						</c:choose>
					</li>
					<li>
						<form:label path="holdRemark" class="twolabel"><spring:message code="jiactions.remark"/>:</form:label>
						<form:textarea path="holdRemark" class="ON_HOLD" rows="4" cols="80"/>
					</li>
					<li>
						<label><spring:message code="jiactions.foritems"/>:</label>
						<div class="float-left" id="onholdItems">
							<input type="checkbox" onclick="selectAllItems(this.checked, 'holdItemIds', 'onholdItems'); " />
							<spring:message code="jiactions.all"/><br />
							<c:forEach var="item" items="${jobItem.job.items}">
								<c:choose>
									<c:when test="${jobItem.state.status && jobItem.state.hold && jobItem.state.hold || item.state.status && !item.state.hold}">
											<c:set var="checked" value=""/>
											<c:if test="${item.jobItemId == jobItem.jobItemId || form.holdItemIds != null && form.holdItemIds.contains(item.jobItemId)}"><c:set var="checked" value="checked"/></c:if>
											<form:checkbox path="holdItemIds" value="${item.jobItemId}"/>
											${item.itemNo} - <cwms:showmodel instrumentmodel="${item.inst.model}"/><br/>
									</c:when>
									<c:when test="${item.state.status && item.state.hold}">
										<span>${item.itemNo} - <spring:message code="jiactions.alreadyonhold"/></span><br/>
									</c:when>
									<c:otherwise>
										<span>${item.itemNo} - <spring:message code="jiactions.theact"/> '<cwms:besttranslation translations="${item.state.translations}"/>' <spring:message code="jiactions.string5"/></span><br/>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</div>
						<!-- clear floats and restore page flow -->
						<div class="clear"></div>
					</li>
					<li>
						<c:choose>
							<c:when test="${jobItem.state.status && jobItem.state.hold}">
								<label>&nbsp;</label>
								<input type="submit" value="<spring:message code='jiactions.takeoffhold'/>"/>
								<input type="hidden" name="submitAction" value="offhold" />
							</c:when>
							<c:otherwise>
								<label>&nbsp;</label>
								<input type="submit" value="<spring:message code='jiactions.placeonhold'/>"/>
								<input type="hidden" name="submitAction" value="onhold" />
							</c:otherwise>
						</c:choose>
					</li>
				</ol>
			</fieldset>
		</form:form>
	</div>
</div>