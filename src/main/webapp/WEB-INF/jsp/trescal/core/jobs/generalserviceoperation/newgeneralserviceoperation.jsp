<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs"%>
<%@ taglib prefix="addr" tagdir="/WEB-INF/tags/address"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image"%>

<jsp:useBean id="now" class="java.util.Date" />
<jobs:jobItemTemplate>
	<jsp:attribute name="subScript">
		<script type="module" src="${pageContext.request.contextPath}/script/components/cwms-datetime/cwms-datetime.js"></script>
		<script
			src='script/trescal/core/jobs/generalserviceoperation/GeneralServiceOperation.js'></script>
		<script>
			// grab scroll to parameter passed when using job item key navigation
			var scrollto = '${request.getParameter("scrollto")}';
			var startedOn = '${form.startedOn}';
			var completedOn = '${form.completedOn}';
		</script>
	</jsp:attribute>
	<jsp:attribute name="warnings">
		<!-- error section displayed when form submission fails -->
		<spring:bind path="form.*">
			<c:if test="${status.error}">
				<c:set var="formHasError" value="${true}" />
				<div class="warningBox1">
					<div class="warningBox2">
						<div class="warningBox3">
							<h5 class="center-0 attention">
								<spring:message code="generalserviceoperation.errors" />:</h5>
							<c:forEach var="error" items="${status.errorMessages}">
								<div class="center attention">${error}</div>
							</c:forEach>
						</div>
					</div>
				</div>
			</c:if>
		</spring:bind>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox">
			<c:choose>
				<c:when test="${ viewWrapper.readyForGeneralServiceOperation }">
					<span id="jobItemId" class="hid">${form.ji.jobItemId}</span>
					<form:form modelAttribute="form"
						id="newgeneralserviceoperationform" method="post" action=""
						enctype="multipart/form-data">
						<fieldset id="newgeneralserviceoperation">
							<legend>
								<spring:message code="generalserviceoperation.newgso" />
							</legend>
							<form:hidden path="gso.id" />
							<form:hidden path="actionPerformed" />
							<ol>
								<li>
									<label><spring:message
											code="generalserviceoperation.jobitem" />:</label>
									<span>
										<input type="hidden" name="itemsToCheck"
										value="${form.ji.jobItemId}" />
										Item ${form.ji.itemNo} on Job ${form.ji.job.jobno}
									</span>
								</li>
								<li>
									<form:label path="startedOn">
										<spring:message code="generalserviceoperation.startedon" />:</form:label>
									<form:input type="datetime-local" path="startedOn"
									step="60" max="${cwms:isoDatetime(now)}"/>
								    <span class="attention"><form:errors path="startedOn" /></span>
								</li>
								<li>
									<label for="serviceType"><spring:message
											code="servicetype" />:</label>
									<cwms:besttranslation translations='${serviceType.shortnameTranslation}'/> - 
									<cwms:besttranslation translations='${serviceType.longnameTranslation}'/>
								</li>
								<c:if test="${ otherjobitems.size() > 0 }">
									<li>
										<form:label path="otherJobItemsId">
											<spring:message code="jicertificates.othitems" />:</form:label>
										<table class="default2">
											<thead>
												<tr>
													<th><input type="checkbox" onclick="checkAllItems($j(this).prop('checked'));" /></th>
													<th><spring:message code="viewjob.itemno" /></th>
													<th><spring:message code="description" /></th>
													<th><spring:message code="completecalibration.serialno" /></th>
													<th><spring:message code="completecalibration.plantno" /></th>
												</tr>
											</thead>			
											<tbody>
											<c:set var="j" value="0"></c:set>
												<c:forEach items="${ otherjobitems }" var="ji">
												
												<tr>
												<c:if test="${ form.ji.jobItemId ne ji.jobItemId }">
													<td>
														<input type="checkbox" name="otherJobItemsId[${ j }]"
																										value="${ ji.jobItemId }" />
																						<c:set var="j" value="${ j+1 }"></c:set>
													</td>
													</c:if>
													<c:if test="${ form.ji.jobItemId eq ji.jobItemId }">
													<td></td>
													</c:if>
													<td><links:jobitemLinkInfo jobItemNo="${ji.itemNo}"
																										jobItemId="${ji.jobItemId}" defaultPage="" /></td>
													<td><c:out value="${ji.inst.customerDescription}" /></td>
													<td><c:out value="${ji.inst.serialno}" /></td>
													<td><c:out value="${ji.inst.plantno}" /></td>
												</tr>
												</c:forEach>
											</tbody>
										</table>
									</li>
								</c:if>
							</ol>
						</fieldset>
						<!-- submit button div -->	
						<div class="center">
							<input type="submit" value="<spring:message code='save'/>" />
						</div>
						<!-- end of submit button div -->
					</form:form>
				</c:when>
				<c:otherwise>
					<div class="warningBox1">
						<div class="warningBox2">
							<div class="warningBox3">
								<h5 class="center-0 attention">
									<spring:message
										code="generalserviceoperation.itemnotreadyforgso" />
								</h5>
							</div>
						</div>
					</div>
				</c:otherwise>
			</c:choose>
		</div>
		<!-- end of infobox div -->
	</jsp:body>
</jobs:jobItemTemplate>