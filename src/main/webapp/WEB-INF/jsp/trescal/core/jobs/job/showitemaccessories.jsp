<%-- File name: /trescal/core/jobs/job/showitemaccessories.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs"%>

<table class="default4 savedAccessoryList">
	<thead>
		<tr>
			<td colspan="3">
				<spring:message code="showitemaccessories.savedaccessories"/>
				<span id="printlabel" class="${not empty jobItem.accessories ? 'vis' : 'hid'}">
					&nbsp;<a href="" class="mainlink" onClick="event.preventDefault(); printDymoJobItemAccessoryLabel('${jobItem.jobItemId}'); "><spring:message code="showitemaccessories.printaccessorylabel"/></a>
				</span>
			</td>
		</tr>
	</thead>
	<tbody>
		<c:choose>
			<c:when test="${jobItem.accessories.size() == 0}">
				<tr>
					<td colspan="3"><spring:message code="showitemaccessories.noaccessories"/></td>
				</tr>
			</c:when>
			<c:otherwise>
				<c:forEach var="itemAccessory" items="${jobItem.accessories}">
					<jobs:itemAccessory itemAccessory="${itemAccessory}"/>
				</c:forEach>
			</c:otherwise>
		</c:choose>
	</tbody>
</table>