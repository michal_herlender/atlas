<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%-- Note - if this page ever gets used in the future (currently it seems to be only available to 2 contacts, presumably Antech ones)
then it will need to be adapted to handle selection of different interval units for the duration around line 119 --%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="multicomplete.headtext"/><c:out value=" ${form.job.jobno}" /></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/jobs/calibration/MultiComplete.js'></script>
	</jsp:attribute>
	<jsp:body>
		<t:showErrors path="form.*" showFieldErrors="true"/>
	
	    <form:form modelAttribute="form">
			<form:hidden path="action"/>
		    <div class="infobox">
			    <div class="marg-left">
                    <input type="checkbox" onclick=" selectAllItems(this.checked, 'multiCompleteCalibrations'); " />
                    <span class="bold">All</span>
                </div>
				<div>
                    <c:set var="cals" value="${form.onGoingCals}"/>
					<c:forEach var="mCal" items="${cals}" varStatus="calStatus">
						<c:set var="cal" value="${mCal.cal}"/>
						<table class="default4 multiCompleteCalibrations" summary="">
							<tbody>
								<tr>
									<th class="group">
										<c:choose>
											<c:when test="${cal.links.size() > 1}">
												<spring:message code="multicomplete.groupcal"/>
											</c:when>
											<c:otherwise>
												<spring:message code="multicomplete.singlecal"/>
											</c:otherwise>
										</c:choose>
									</th>
									<th class="results"><spring:message code="multicomplete.results"/>: ${cal.calClass.desc}</th>
									<th class="process"><spring:message code="completecalibration.calprocselect"/>: ${cal.calProcess.process}</th>
									<th class="proc"><spring:message code="calsearch.capability"/>: ${cal.capability.reference}</th>
									<th class="started"><spring:message code="calsearch.startby"/>: 
										${cal.startedBy.shortenedName}&nbsp;&nbsp;<fmt:formatDate value="${cal.startTime}" type="date" dateStyle="SHORT" />
									</th>
								</tr>
								<tr>
									<td style=" padding: 0; " colspan="5">
										<table class="child-table"  >
											<tbody>
												<c:forEach var="mCalItem" items="${mCal.items}" varStatus="itemStatus">
													<c:set var="cl" value="${mCalItem.calLink}"/>
													<tr>
														<td class="select">
															<c:if test="${itemStatus.isFirst()}">
																<form:checkbox path="onGoingCals[${calStatus.index}].selected"/>
															</c:if>
														</td>																	
														<td class="itemno">${cl.ji.itemNo}</td>
														<td class="inst">${cl.ji.inst.definitiveInstrument}</td>
														<td class="outcome">
															<spring:message code="multicomplete.outcome"/>:
                                                            <spring:bind path="form.onGoingCals[${calStatus.index}].items[${itemStatus.index}].outcomeId">
                                                                <select name="${status.expression}">
                                                                    <optgroup label="Success (allows certification)">
                                                                    <c:forEach var="ao" items="${actionoutcomes}">
                                                                        <c:if test="${ao.positiveOutcome == true}">
                                                                            <option value="${ao.id}" ${mCalItem.outcomeId != null && mCalItem.outcomeId == ao.id ? "selected" : ""}>
                                                                                    ${ao.description}
                                                                            </option>
                                                                        </c:if>
                                                                    </c:forEach>
                                                                </optgroup>
                                                                    <optgroup label="Failure">
                                                                        <c:forEach var="ao" items="${actionoutcomes}">
                                                                            <c:if test='${ao.positiveOutcome == false && !ao.description.contains("on hold") }'>
                                                                                <option value="${ao.id}" ${mCalItem.outcomeId != null && mCalItem.outcomeId == ao.id ? "selected" : ""}>
                                                                                        ${ao.description}
                                                                                </option>
                                                                            </c:if>
                                                                        </c:forEach>
                                                                    </optgroup>
                                                                    <optgroup label="Incomplete">
                                                                        <c:forEach  var="ao" items="${actionoutcomes}">
                                                                            <c:if test='${ao.positiveOutcome == false && ao.description.contains("on hold")}'>
                                                                                <option value="${ao.id}" ${mCalItem.outcomeId != null && mCalItem.outcomeId == ao.id ? "selected" : ""}>
                                                                                        ${ao.description}
                                                                                </option>
                                                                            </c:if>
                                                                        </c:forEach>
                                                                    </optgroup>
                                                                </select>
                                                            </spring:bind>
                                                            <a href="#" class="imagelink" onclick="  copySelectedOption('down', 'select', $j(this).siblings('select')); return false; ">
                                                                <img src="img/icons/arrow_down.png" width="10" height="16" alt='<spring:message code="multicomplete.imgcopysel"/>' title='<spring:message code="multicomplete.imgcopysel"/>' />
                                                            </a>
                                                        </td>
                                                        <td ${itemStatus.isFirst() ? 'class="time"' : 'colspan="3"'}>
                                                            <spring:message code="multicomplete.timespent"/>:
                                                            <form:input path="onGoingCals[${calStatus.index}].items[${itemStatus.index}].timeSpent" size="2"/>
                                                            <a href="#" class="imagelink" onclick="  copySelectedOption('down', 'input', $j(this).siblings('input')); return false; ">
                                                                <img src="img/icons/arrow_down.png" width="10" height="16" alt='<spring:message code="multicomplete.imgcopysel"/>' title='<spring:message code="multicomplete.imgcopysel"/>' />
                                                            </a>
                                                        </td>
                                                        <c:if test="${itemStatus.isFirst()}">
                                                            <td class="duration">
                                                                <spring:message code="multicomplete.duration"/>:
                                                                <form:input path="onGoingCals[${calStatus.index}].duration.interval" size="2"/>
                                                                <a href="#" class="imagelink" onclick="  copySelectedOption('down', 'input', $j(this).siblings('input')); return false; ">
                                                                    <img src="img/icons/arrow_down.png" width="10" height="16" alt='<spring:message code="multicomplete.imgcopysel"/>' title='<spring:message code="multicomplete.imgcopysel"/>' />
                                                                </a>
                                                            </td>
                                                        </c:if>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</td>
								</tr>
							</tbody>
						</table>
                    </c:forEach>
                    <fieldset>
						<legend><spring:message code="multicomplete.caldet"/></legend>
                        <ol>
                            <li>
                                <label><spring:message code="multicomplete.isscertnos"/>:</label>
                                <form:checkbox path="issueNow" id="issueNewCertCheckbox"/>
                            </li>
                            <li>
                                <label><spring:message code="multicomplete.remappl"/>:</label>
                                <form:textarea path="remark" class="width60 presetComments COMPLETE_CALIBRATION"/>
                            </li>
                        </ol>
					</fieldset>
                    <div class="text-center marg-top marg-bottom">
                        <input type="submit" value='<spring:message code="multicomplete.complselcal"/>' />
                    </div>
				</div>
			</div>
		</form:form>
	</jsp:body>
</t:crocodileTemplate>