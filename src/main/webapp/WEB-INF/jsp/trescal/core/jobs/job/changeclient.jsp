<%-- File name: /trescal/core/jobs/job/changeclient.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="changeclient.headtext" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/jobs/job/ChangeClient.js'></script>
    </jsp:attribute>
    <jsp:body>
		<!-- error section displayed when form submission fails -->
		<t:showErrors path="fbo.*" showFieldErrors="true" />
		<!-- end error section -->
		
		<!-- main div which holds all form elements for changing the client on a job -->
		<div class="infobox">
		
			<form:form action="" method="post" id="changejobclientform" modelAttribute="fbo" onsubmit=" return checkForm(); ">
			
			<!-- fieldset containing details of the current client on job -->
			<fieldset>
				
				<legend><spring:message code="changeclient.currclientjob" />&nbsp;<a href="viewjob.htm?jobid=${fbo.job.jobid}" class="mainlink">${fbo.job.jobno}</a></legend>
				
				<ol>
						
					<li>
						<label><spring:message code="company" />:</label>
						<span>
							${fbo.job.con.sub.comp.coname}
						</span>
					</li>
					<li>
						<label><spring:message code="subdivision" />:</label>
						<span>
							${fbo.job.con.sub.subname}
						</span>	
					</li>
					<li>
						<label><spring:message code="contact" />:</label>
						<span>
							${fbo.job.con.name}
						</span>					
					</li>
					<li>
						<label><spring:message code="address" />:</label>
						<div class="float-left padtop">
						<c:if test="${fbo.job.returnTo.addr1.trim().length() > 0}"> ${fbo.job.returnTo.addr1}<br/></c:if> 
						<c:if test="${fbo.job.returnTo.addr2.trim().length() > 0}"> ${fbo.job.returnTo.addr2}<br/></c:if> 
						<c:if test="${fbo.job.returnTo.addr3.trim().length() > 0}"> ${fbo.job.returnTo.addr3}<br/></c:if> 
					    <c:if test="${fbo.job.returnTo.town.trim().length() > 0}"> ${fbo.job.returnTo.town}<br/></c:if>
						<c:if test="${fbo.job.returnTo.county.trim().length() > 0}"> ${fbo.job.returnTo.county}<br/></c:if>	
						<c:if test="${fbo.job.returnTo.county.trim().length() > 0}"> ${fbo.job.returnTo.postcode}<br/></c:if>
					    <c:if test="${fbo.job.returnTo.postcode.trim().length() > 0}"> ${fbo.job.returnTo.country.localizedName}<br/></c:if>
						</div>
						
						<!-- clear floats and restore page flow -->
						<div class="clear"></div>
					</li>
					
				</ol>
				
			</fieldset>
			<!-- end of current client on job fieldset -->
			
			<!-- fieldset containing the cascading search plugin for choosing a new client and address -->
			<fieldset>
				
				<legend><spring:message code="changeclient.legend" />&nbsp;<a href="viewjob.htm?jobid=${fbo.job.jobid}" class="mainlink">${fbo.job.jobno}</a></legend>
												
				<ol>
				
					<li>
						<label><spring:message code="addjob.contaddr" />:</label>
																													
						<!-- this div creates a new cascading search plugin. The default search contains an input 
							 field and results box for companies, when results are returned this cascades down to
							 display the subdivisions within the selected company. This default behavoir can be obtained
							 by including the hidden input field with no value or removing the input field. You can also
							 add address and/or contact searches to the cascade by including the values 'contact' and/or 'address'
							 in the hidden inputs value attribute separated by a comma (e.g value="contact", value="address",
							 value="address,contact").  You can also swap these values around (e.g. value="contact,address").  
						 -->									
						<div id="cascadeSearchPlugin">
							<input type="hidden" id="cascadeRules" value="subdiv,contact,address" />
							<input type="hidden" id="compCoroles" value="client,business" />
							<input type="hidden" id="addressType" value="Delivery" />
						</div>
						
						<!-- clear floats and restore page flow -->									
						<div style="clear:both;"></div>							
					
					</li>
					<li>
						<label class="labelhead bold"><spring:message code="changeclient.addopt" /></label>
						<span>&nbsp;</span>
					</li>
					<li>
						<form:checkbox path="incInstruments" />
						<spring:message code="changeclient.inclallinstr" />
					</li>
					<li>
						<form:checkbox path="incDeliveries" />
						<spring:message code="changeclient.inclallclientdel" />
					</li>
					<li>
						<label>&nbsp;</label>
						<input type="submit" name="submit" id="submit" value="<spring:message code='changeclient.headtext'/>"/>
					</li>
					
				</ol>
					
			</fieldset>
			<!-- end of fieldset containing cascading search plugin -->
				
			</form:form>
			
		</div>
		<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>