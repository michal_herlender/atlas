<%-- File name: /trescal/core/jobs/jobitem/jidemands.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs"%>

<jobs:jobItemTemplate>
    <jsp:attribute name="subScript">
        <script type='text/javascript' src='script/trescal/core/jobs/jobitem/JIDemands.js'></script>
		<script type='text/javascript' src='script/trescal/core/documents/ImageGalleryFunctions.js'></script>
		<script>
			// grab scroll to parameter passed when using job item key navigation
			var scrollto = '${request.getParameter("scrollto")}';
		</script>
	</jsp:attribute>
	<jsp:body>
		<form:form modelAttribute="form" id="patform" method="post">
			<fieldset>
				<ol>
					<li><h5><spring:message code="additionalDemand.additionalDemands"/></h5></li>								
					<c:forEach var="demand" items="${additionalDemands}">
						<li>
							<div>
								<spring:message var="demandLabel" code="${demand.messageCode}"/>
								<form:label path="demandStatus[${demand.ordinal()}]">${demandLabel}</form:label>
								<form:select path="demandStatus[${demand.ordinal()}]" items="${additionalDemandStatus}"/>
							</div>
							<div>
								<form:textarea cols="120" rows="5" path="demandComment[${demand.ordinal()}]"/>
							</div>
						</li>
					</c:forEach>
					<li>
						<form:label path="">&nbsp;</form:label>
						<form:button name="submit"><spring:message code="submit"/></form:button>
					</li>
				</ol>
			</fieldset>
		</form:form>
	</jsp:body>
</jobs:jobItemTemplate>