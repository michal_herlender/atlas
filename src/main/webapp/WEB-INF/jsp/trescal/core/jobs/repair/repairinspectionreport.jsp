<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<style>
#rcrForm label {
	width: 210px;
}

#rcrForm h5 {
	text-decoration-line: underline;
	margin-bottom: 0px;
}

.compSearchJQPlugin {
	display: inline-block;
}

.right {
	text-align: right !important;
}

#totalsTable {
	float: right;
	margin: 50px;
	margin-bottom: 10px;
	font-size: small;
}

#saveBtn {
	margin: 10px;
}

.pointer {
	cursor: pointer;
}

.row-resize {
	cursor: row-resize;
}

#operationsTable input {
	width: 65px;
}

.operationtime {
	text-align: center;
	width: 90px;
}

.operationtime input {
	text-align: right;
}

.operationcost {
	text-align: center;
	width: 90px;
}

.operationcost input {
	text-align: right;
}

.dragNdropIcon {
	width: 20px;
	height: 15px;
	float: left;
	filter: invert(40%);
}
.display-block {
	display: block;
}

.autoresizing {
            display: block;
            overflow: hidden;
            resize: none;
            margin: 0px;
        }
</style>
<script>
	/* Translations */
	 var strings = new Array();
	 strings['repairinpectionreport.complete'] = "<spring:message code='repairinpectionreport.complete' javaScriptEscape='true' />";
	 strings['repairinpectionreport.validate'] = "<spring:message code='repairinpectionreport.validate' javaScriptEscape='true' />";
	 strings['repairinpectionreport.savewarning'] = "<spring:message code='repairinpectionreport.savewarning' javaScriptEscape='true' />";
	 
	 function updateInstructionSize(instructionSize) {}
	 
	 function autoResize() {
		    this.style.height = 'auto';
		    this.style.height = this.scrollHeight + 'px';
	}
	 
	 function autoresizing() {
			textarea = document.querySelectorAll(".autoresizing");
			textarea.forEach(element => {
				element.addEventListener('input', autoResize, false);
				element.addEventListener('focus', autoResize, false);
	 		});
		}
	
</script>
<script src="script/trescal/core/jobs/repair/repairinspectionreport.js"></script>
<script type="module" src="script/components/cwms-instructions/cwms-instructions.js"></script>
<script type="text/javascript">
var enableRcr = ${disableRcrTab};
if(enableRcr != undefined && !enableRcr)
	enableRcrTab();
var actionRirPerformed = ${actionPerformed};

var rirOperationStatusOptions = '<option value="">&nbsp;</option>';
<c:forEach items="${operationStatus}" var="o">
rirOperationStatusOptions += "<option value='${o}'>${o.value}</option>";
</c:forEach>

var rirComponentStatusOptions = '<option value="">&nbsp;</option>';
<c:forEach items="${componentStatus}" var="c">
rirComponentStatusOptions += "<option value='${c}'>${c.value}</option>";
</c:forEach>
</script>
<div class="clear">&nbsp;</div>
<form:form modelAttribute="rirForm" method="POST" id="rirForm">
<!-- error section displayed when form submission fails -->
<spring:bind path="rirForm.*">
	<c:if test="${status.error}">
		<div class="warningBox1">
			<div class="warningBox2">
				<div class="warningBox3">
					<div class="center attention">
						<spring:message code='repairinspection.validator.generalmessage' /><br/>
						<form:errors class="error max-wrap" path="isAccredited" />
					</div>
				</div>
			</div>
		</div>
	</c:if>
	<div class="successBox1 hid" id="successMessageRir">
		<div class="successBox2">
			<div class="successBox3">
				<div class="center green bold">
					<spring:message code='repairinspection.success1' />
				</div>
			</div>
		</div>
	</div>
</spring:bind>

<!-- end error section -->

	<form:hidden path="rirId" />
	<form:hidden path="jobItemId" />
	<form:hidden path="completedByTechnician" />
	<form:hidden path="validatedByManager" />
	<form:hidden path="creationDate" />
	<form:hidden path="createdBy" />
	
	
	
	<fieldset>
		<legend>
			<spring:message
				code='instructiontype.repairsandadjustment' />
		</legend>
		<cwms-instructions link-coid="${ rirForm.ji.job.con.sub.comp.coid }" link-subdivid="${ rirForm.ji.job.con.sub.subdivid }" link-contactid="${ rirForm.ji.job.con.personid }" instruction-types="REPAIRANDADJUSTMENT" show-mode="TABLE"></cwms-instructions>
	</fieldset>	
	<!-- Customer Informations START-->
	<fieldset>
		<legend>
			<span id="accHeader"><spring:message
					code='repairinspection.customer.legend' /></span>
		</legend>
		<ol>
			<li><label><spring:message
						code='repairinspection.customer.description' />* :</label> <form:textarea
					path="clientDescription" rows="2" cols="130" disabled="${disabled}" />
				<form:errors class="error max-wrap" path="clientDescription" /></li>
			<li><label><spring:message
						code='repairinspection.customer.instructions' />:</label> <form:textarea
					path="clientInstructions" rows="2" cols="130"
					disabled="${disabled}" /> <form:errors class="error max-wrap"
					path="clientInstructions" /></li>
		</ol>
	</fieldset>
	<!-- Customer Informations END-->

	<!-- Internal Informations START-->
	<fieldset>
		<legend>
			<span id="accHeader"><spring:message
					code='repairinspection.internalestimation.legend' /></span>
		</legend>
		<ol>
			<li><label for="internalDescription"><spring:message
						code='repairinspection.internalestimation.description' />:</label> <form:textarea
					path="internalDescription" rows="2" cols="130"
					disabled="${disabled}" /> <form:errors class="error max-wrap"
					path="internalDescription" /></li>
			<li><label for="internalInspection"><spring:message
						code='repairinspection.internalestimation.inspection' />* :</label> <form:textarea
					path="internalInspection" rows="2" cols="130"
					disabled="${disabled}" /> <form:errors class="error max-wrap"
					path="internalInspection" /></li>
<%-- 			<li><label for="internalOperations"><spring:message --%>
<%-- 						code='repairinspection.internalestimation.operations' />:</label> <form:textarea --%>
<%-- 					path="internalOperations" rows="2" cols="130" --%>
<%-- 					disabled="${disabled}" /> <form:errors class="error max-wrap" --%>
<%-- 					path="internalOperations" /></li> --%>
			<li><label><spring:message
						code='repairinspection.internalestimation.warranty' />:</label>
				<table>
					<tr>
						<td>
							<div class="padding5">
								<label><spring:message
										code='repairinspection.internalestimation.warranty.manufacturer' /></label>
								<form:checkbox path="manufacturerWarranty"
									disabled="${disabled}" />
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="padding5">
								<label><spring:message
										code='repairinspection.internalestimation.warranty.trescal' /></label>
								<form:checkbox path="trescalWarranty" id="trescalWarrantyCheckBox" disabled="${disabled}" />
							</div>
						</td>
					</tr>
				</table></li>
			<li>
				<label>
					<spring:message code='repairinspection.internalestimation.misuse' /> * :
				</label>
				<form:radiobutton path="misuse" id="misuseCheckBox" value="YES" disabled="${disabled}" onclick="showMisuseComment(true)"/>
				<span class="padding5"><spring:message code='yes' /></span>
				<form:radiobutton path="misuse" id="misuseCheckBox" value="NO" disabled="${disabled}" onclick="showMisuseComment(false)"/>
				<span class="padding5"><spring:message code='no' /></span>
				<form:errors class="error max-wrap" path="misuse" />
			</li>
			<li id="misuseCommentText" class="${ rirForm.misuse ? 'show':'hid' }">
				<label>
					<spring:message code='repairinspection.internalestimation.misusecomment' />:
				</label>
				<form:textarea path="misuseComment" rows="2" cols="130" disabled="${disabled}" />
			</li>
			<li><label><spring:message
						code='repairinspection.internalestimation.instructions' />:</label>
				<table>
					<tr>
						<td><label for="internalInstructions"><spring:message
									code='repairinspection.internalestimation.instructions.internalinst' />:</label>
							<form:textarea path="internalInstructions" rows="2" cols="130"
								disabled="${disabled}" /> <form:errors class="error max-wrap"
								path="internalInstructions" /></td>
					<tr />
					<tr>
						<td><label for="externalInstructions"><spring:message
									code='repairinspection.internalestimation.instructions.externalinst' />:</label>
							<form:textarea path="externalInstructions" rows="2" cols="130"
								disabled="${disabled}" /> <form:errors class="error max-wrap"
								path="externalInstructions" /></td>
					</tr>
				</table></li>
			<li>
				<div class="displaycolumn-100">
					<label><spring:message
							code='repairinspection.internalestimation.estimation' />:</label>
				</div>
				<div class="displaycolumn-100">
					<table>
						<tr>
							<td class="width15">
								<h5>
									<spring:message
										code='repairinspection.internalestimation.estimation.addoperation' />
									:
								</h5>
							</td>
							<td class="width80">
								<div class="displaycolumn-100">
									<div class="warningBox1 hid" id="operationErrors">
										<div class="warningBox2">
											<div class="warningBox3"></div>
										</div>
									</div>
								</div>
								<div class="displaycolumn-100 ${ disabled ? 'hid':'' }">
									<div style="display: inline-block;">
										<textarea rows="2" cols="112" id="operationName"></textarea>
										<!-- internal external radio buttons and dependant inputs -->
										<div>
											<input type="radio" name="optype" checked="checked"
												value="INTERNAL"
												onchange="showAdditionalFieldsForRirOperationType($j(this).is(':checked'), 'internal'); return false;" />
											<span id="internalOp"> <spring:message
													code='repairinspection.internalestimation.estimation.addoperation.radio.internal' />
											</span> <input type="radio" name="optype" value="EXTERNAL"
												onchange="showAdditionalFieldsForRirOperationType($j(this).is(':checked'), 'external'); return false;" />
											<span id="externalOp"> <spring:message
													code='repairinspection.internalestimation.estimation.addoperation.radio.external' />
											</span>
											<!-- minutes (internal) -->
											<div style="display: inline;" class="internalFields">
												<span style="margin-left: 15px;">
													<spring:message code='workflow.time' />* :
												</span>
												<input type="number" style="width: 35px;" min="0" max="99" id="internalTimeHours" value="0"/>
												<spring:message code='hours' />
												&nbsp;
												<input type="number" style="width: 35px;" min="0" max="59" id="internalTimeMinutes" value="0"/>
												<spring:message code='minutes' />
											</div>
											<!-- Company (External) -->
											<div style="display: none;" class="externalFields">
												<span style="margin-left: 15px;"> <spring:message
														code='company' /> :
												</span>
												<div class="extendPluginInput" style="display: inline;">
													<div class="compSearchJQPlugin">
														<input type="hidden" name="field" value="coid" /> <input
															type="hidden" name="compCoroles"
															value="supplier,utility,business" /> <input
															type="hidden" name="tabIndex" value="1" />
														<!-- company results listed here -->
													</div>
												</div>
											</div>
											<!-- delay (External) -->
											<div style="display: none;" class="externalFields">
												<span style="margin-left: 8px"> <spring:message
														code='repairinspection.internalestimation.estimation.addoperation.radio.externaldelay' />
													:
												</span> <input type="number" style="width: 40px;" id="externalTime" />
											</div>
											<!-- delay (External) -->
											<div style="display: none;" class="externalFields">
												<span> <spring:message
														code='repairinspection.internalestimation.estimation.addoperation.radio.externalwithcal' />
													:
												</span> <input type="checkbox" id="externalWithCal" />
											</div>
										</div>
									</div>
									<div
										style="display: inline-block; height: 50px; vertical-align: top;">
										<button style="padding: 7px 0px 7px 0px; width: 70px;"
											onclick="addRirOperation(); return false;">
											<spring:message
												code="repairinspection.internalestimation.estimation.addcomponent.addbutton" />
										</button>
									</div>
								</div>


								<div class="displaycolumn-100">
									<br />
									<form:errors class="error max-wrap" path="operationsForm" />
									<table class="default4" id="operationsTable">
										<thead>
											<tr>
												<th><spring:message
														code="repairinspection.internalestimation.estimation.operationstable.order" /></th>
												<th style="width: 400px"><spring:message
														code="repairinspection.internalestimation.estimation.table.operation" /></th>
												<th><spring:message
														code="repairinspection.internalestimation.estimation.operationstable.type" /></th>
												<th><spring:message
														code="repairinspection.internalestimation.estimation.operationstable.time" /></th>
												<th><spring:message
														code="repairinspection.internalestimation.cost" /></th>
												<th><spring:message
														code="repairinspection.internalestimation.status" /></th>
												<th><spring:message
														code="repairinspection.internalestimation.estimation.table.actions" /></th>
											</tr>
										</thead>
										<tfoot>
											<tr>
												<td colspan="3" class="text-left bold right"><spring:message
														code="repairinspection.internalestimation.estimation.operationstable.totalinternal" />
												</td>
												<td style="text-align: center;">
													<span id="opTIntTime"></span>
													<span class="hid" id="opTIntTimeInMinutes"></span>
												</td>
												<td style="text-align: center;"><span id="opTIntCost"></span></td>
												<td rowspan="3" colspan="2"></td>
											</tr>
											<tr>
												<td colspan="3" class="text-left bold right"><spring:message
														code="repairinspection.internalestimation.estimation.operationstable.totalexternal" />
												</td>
												<td style="text-align: center;"><span id="opTExtTime"></span></td>
												<td style="text-align: center;"><span id="opTExtCost"></span></td>
											</tr>
											<tr>
												<td colspan="3" class="text-left bold right"><spring:message
														code="repairinspection.internalestimation.estimation.table.total" />
												</td>
												<td style="text-align: center;"><span id="opTTime"></span></td>
												<td style="text-align: center;"><span id="opTCost"></span></td>
											</tr>
										</tfoot>
										<tbody>
											<c:if test="${ empty rirForm.operationsForm }">
												<tr class="emptyRow">
													<!-- show this if table is empty -->
													<td colspan="7"></td>
												</tr>
											</c:if>
											<c:forEach items="${ rirForm.operationsForm }" var="op"
												varStatus="i">
												<tr class="${ i.index % 2 == 0 ? 'even':'odd' }"
													addedafterrirvalidation="${ op.addedAfterRiRValidation }">
													<td class="center"><img class="dragNdropIcon"
														src="img/icons/scroller.png" /><span>${i.index+1}</span>
														<input type="hidden"
															id="operationsForm${i.index}.frOperationId"
															name="operationsForm[${i.index}].frOperationId"
															value="${ op.frOperationId }" /> <input type="hidden"
															id="operationsForm${i.index}.addedAfterRiRValidation"
															name="operationsForm[${i.index}].addedAfterRiRValidation"
															value="${ op.addedAfterRiRValidation }" /> <input
															type="hidden" id="operationsForm${i.index}.coid"
															name="operationsForm[${i.index}].coid" value="${op.coid}" />
														<input class="order" type="hidden"
															id="operationsForm${i.index}.position"
															name="operationsForm[${i.index}].position"
															value="${i.index+1}" />
													</td>
													<td class="operationname" opId="${ op.frOperationId }">
														<textarea class="autoresizing"
															id="operationsForm${i.index}.name"
															name="operationsForm[${i.index}].name"
															style="width:100%" ${disabled ? 'disabled':''}
															>${ op.name }</textarea>
														<form:errors
															class="error max-wrap"
															path="operationsForm[${i.index}].name" />
													</td>

													<td class="operationtype">${ op.type.value }${ not empty op.coName ? ' (':''}
														${not empty op.coName ? op.coName:''} ${not empty op.coName ? ')':'' }<input
														type="hidden" id="operationsForm${i.index}.type"
														name="operationsForm[${i.index}].type"
														value="${ op.type }" /> <form:errors
															class="error max-wrap"
															path="operationsForm[${i.index}].type" />
													</td>

													<td class="operationtime"
														isinternal="${ op.type eq 'INTERNAL' }"><input
														type="text" onchange="updateRirOperationsTotals();"
														id="operationsForm${i.index}labourTimeHours"
														name="operationsForm[${i.index}].labourTimeHours"
														value="${op.labourTimeHours}" ${disabled ? 'disabled':''}
														 style="width: 25px;" />
														 ${(op.type eq 'EXTERNAL' || op.type eq 'EXTERNAL_WITH_CALIBRATION') ? 'D(days)'
														 : 'H&nbsp;'}
														<input
														type="text" onchange="updateRirOperationsTotals();"
														id="operationsForm${i.index}labourTimeMinutes"
														name="operationsForm[${i.index}].labourTimeMinutes"
														value="${op.labourTimeMinutes}" ${disabled ? 'disabled':''} style="width: 25px;" /> 
														${(op.type eq 'EXTERNAL' || op.type eq 'EXTERNAL_WITH_CALIBRATION') ? ''
														 : 'M'}
														<form:errors
															class="error max-wrap"
															path="operationsForm[${i.index}].labourTimeHours" /><br/>
														<form:errors
															class="error max-wrap"
															path="operationsForm[${i.index}].labourTimeMinutes" /></td>

													<td class="operationcost"
														isinternal="${ op.type eq 'INTERNAL' }"><input
														type="text" id="operationsForm${i.index}.cost"
														name="operationsForm[${i.index}].cost" value="${op.cost}"
														onchange="updateRirOperationsTotals(); return false;"
														${disabled ? 'disabled':''} /> <form:errors
															class="error max-wrap"
															path="operationsForm[${i.index}].cost" /></td>

													<td><form:select
														path="operationsForm[${ i.index }].status"
														disabled="${disabled}" >
															<form:option value="">&nbsp;</form:option>
     														<form:options items="${ operationStatus }" itemLabel="value" />
														</form:select> <form:errors
														class="error max-wrap"
														path="operationsForm[${ i.index }].status" /></td>
											
													<td class="center">
														<c:if test="${not disabled and not rirForm.validatedByManager}">
															<img src="img/icons/delete.png" 
															height="16" width="16" alt="" title=""
															class="pointer delimg" onclick="deleteRirOperation(this)"
															id="${op.name}" data-type="${op.type}"
															data-timeHours="${op.labourTimeHours}"
															data-timeMinutes="${op.labourTimeMinutes}" data-cost="${op.cost}" />
														</c:if>
													</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</td>
						</tr>

						<tr>
							<td class="width20">
								<h5>
									<spring:message
										code='repairinspection.internalestimation.estimation.addcomponent' />
									:
								</h5>
							</td>
							<td class="width80">
								<!-- add component -->
								<div class="${ disabled ? 'hid':'' }">
									<div style="display: inline-block;">
										<input style="width: 550px;" type="text" id="componentName" />
										<input type="hidden" id="compid" />
										<div style="display: block;">
											<span> 
												<spring:message code='repairinspection.internalestimation.estimation.addcomponent.quantity' />* 												:
											</span> 
											<input type="number" style="width: 40px" id="componentQuantity" />
											<span>
												<spring:message code='repairinspection.internalestimation.cost' /> :
											</span>
											<input type="text" style="width: 40px" id="componentCost" />
										</div>

									</div>
									<div
										style="display: inline-block; height: 50px; vertical-align: top;">
										<button style="padding: 7px 0px 7px 0px; width: 70px;"
											onclick="addRirComponent(); return false;">
											<spring:message
												code="repairinspection.internalestimation.estimation.addcomponent.addbutton" />
										</button>
									</div>
								</div> <br />
								<div class="displaycolumn-100">
									<table class="default4" id="componentsTable">
										<thead>
											<tr>
												<th><spring:message
														code="repairinspection.internalestimation.estimation.componentstable.num" /></th>
												<th><spring:message
														code="repairinspection.internalestimation.estimation.componentstable.component" /></th>
												<th><spring:message
														code="repairinspection.internalestimation.estimation.table.operation" /></th>
												<th><spring:message
														code="repairinspection.internalestimation.estimation.componentstable.source" /></th>
												<th><spring:message
														code="repairinspection.internalestimation.estimation.addcomponent.quantity" /></th>
												<th><spring:message
														code="repairinspection.component.cost" /></th>
												<th><spring:message
														code="repairinspection.internalestimation.status" /></th>
												<th><spring:message
														code="repairinspection.internalestimation.estimation.table.actions" /></th>
											</tr>
										</thead>
										<tfoot>
											<tr>
												<td colspan="4" class="text-left bold right"><spring:message
														code="repairinspection.internalestimation.estimation.table.total" />
												</td>
												<td><span id="compTQuantity"></span></td>
												<td><span id="compTCost"></span></td>
												<td colspan="2"></td>
											</tr>
										</tfoot>
										<tbody>
											<c:if test="${ empty rirForm.componentsForm }">
												<tr class="emptyRow">
													<!-- show this if table is empty -->
													<td colspan="8"></td>
												</tr>
											</c:if>
											<c:forEach items="${ rirForm.componentsForm }" var="comp"
												varStatus="i">
												<tr class="${ i.index % 2 == 0 ? 'even':'odd' }"
													addedafterrirvalidation="${ comp.addedAfterRiRValidation }">
													<td class="center"><span>${i.index+1}</span> <input
														type="hidden" id="componentsForm${i.index}.frComponentId"
														name="componentsForm[${i.index}].frComponentId"
														value="${comp.frComponentId}" /> <input type="hidden"
														id="componentsForm${i.index}.addedAfterRiRValidation"
														name="componentsForm[${i.index}].addedAfterRiRValidation"
														value="${ comp.addedAfterRiRValidation }" /></td>
														
													<td class="componentname">
													<textarea id="componentsForm${i.index}.name" class="autoresizing"
														name="componentsForm[${i.index}].name" 
														style="width:100%;text-align:left;" ${disabled ? 'disabled':''} 
														cols="90" rows="1">${comp.name}</textarea>
														<form:errors 
															class="error max-wrap display-block"
															path="componentsForm[${i.index}].name" />
													</td>
													
													<td class="componentOperation"><select
														id="opSelect${i.index}"
														onchange="$j('#rirForm #currentSelectedIdValue${i.index}').val($j(this).val()); $j('#rirForm #currentSelectedNameValue${i.index}').val($j(this).find('option:selected').text());"
														${disabled ? 'disabled':''}>
															<c:forEach items="${ rirForm.operationsForm }" var="op">
																<option value="${ op.frOperationId }"
																	${ (comp.freeRepairOperationId eq op.frOperationId || (comp.freeRepairOperationId eq 0 && comp.freeRepairOperationName eq op.name)) ? 'selected':'' }>${ op.name }</option>
															</c:forEach>
													</select> <input type="hidden" id="currentSelectedIdValue${i.index}"
														name="componentsForm[${i.index}].freeRepairOperationId"
														value="${comp.freeRepairOperationId}" /> <input
														type="hidden" id="currentSelectedNameValue${i.index}"
														name="componentsForm[${i.index}].freeRepairOperationName"
														value="${comp.freeRepairOperationName}" /></td>
													<td class="componentsource">${comp.source.value}<input
														type="hidden" id="componentsForm${i.index}source"
														name="componentsForm[${i.index}].source"
														value="${comp.source}" />
													</td>
													<td class="componentquantity"><input type="text"
														id="componentsForm${i.index}quantity"
														name="componentsForm[${i.index}].quantity"
														value="${comp.quantity}"
														onchange="updateRirComponentsTotals();"
														${disabled ? 'disabled':''} /></td>
													<td class="componentcost"><input type="text"
														id="componentsForm${i.index}cost"
														name="componentsForm[${i.index}].cost"
														value="${comp.cost}"
														onchange="updateRirComponentsTotals();"
														${disabled ? 'disabled':''} />
														<form:errors
															class="error max-wrap"
															path="componentsForm[${i.index}].cost" />
															</td>
													
													<td><form:select
														path="componentsForm[${ i.index }].status"
														disabled="${disabled}" >
															<form:option value="">&nbsp;</form:option>
     														<form:options items="${ componentStatus }" itemLabel="value" />
														</form:select>
														<form:errors
														class="error max-wrap"
														path="componentsForm[${ i.index }].status" /></td>
													
													<td class="center">
														<c:if test="${not disabled and not rirForm.validatedByManager}">
															<img src="img/icons/delete.png"
	
															height="16" width="16" alt="" title="" class="delimg"
															onclick="deleteRirComponent(this)"
															id="delete_${comp.name}" data-quantity="${comp.quantity}"
	
															data-cost="${comp.cost}" />
														</c:if>
													</td>
													
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</td>
						</tr>

						<tr>
							<td class="width20"><label><spring:message
										code='repairinspection.internalestimation.leadtime' />* :</label></td>
							<td class="width80">
								<div class="displaycolumn-100">


									<form:input path="leadTime" style="width: 40px" onchange="onChangeLeadTime()"
										disabled="${disabled}" />
									<form:errors class="error max-wrap" path="leadTime" />
								</div>
							</td>
						</tr>
						<tr>
							<td class="width20"><label><spring:message
										code='repairinspection.internalestimation.estimatedreturndate' />* :</label>
							</td>
							<td class="width80">
								<div class="displaycolumn-100">
									<form:input path="estimatedReturnDate" disabled="${disabled}" type="date" />
									<form:errors class="error max-wrap" path="estimatedReturnDate" />
								</div>
							</td>
						</tr>
					</table>
				</div>
			</li>
			<li><label>&nbsp;</label>
				<div>
					<table id="totalsTable">
						<tr>
							<td><label><spring:message
										code='repairinspection.totalcost' />:</label></td>
							<td><span id="totalCost"></span></td>
						</tr>
					</table>
				</div></li>
		</ol>
		<div class="padding10" style="text-align:center;">
					<button type="button" name="saveRir" onclick="saveRirFrom()" ${disabled or rirForm.validatedByManager ? 'disabled':''}> 
						<spring:message code="repairinspection.savebutton" />
					</button>
 					<c:if test="${ empty rirForm.validatedByManager or not rirForm.validatedByManager }">
 					    <c:if test="${empty rirForm.rirId}" >
 					        <c:set var="rirEmpty" value="true"/>
 					     </c:if>
						<button type="button" ${rirEmpty ? 'disabled' : ''} name="saveAndValidateRir"  onclick="validateRir()"> 
							<spring:message code="${not rirForm.completedByTechnician ? 'failurereport.complete':'failurereport.validate'}" />	
						</button>
 					</c:if>
 					<c:if test="${ not empty rirForm.validatedByManager and rirForm.validatedByManager and 
 					(empty rirForm.reviewedByCsr or not rirForm.reviewedByCsr) }">
						<button type="button" ${rirEmpty ? 'disabled' : ''} name="reviewAndSaveRir" onclick="reviewRir()"> 
							<spring:message code='repairinspection.button.csrreview' />
						</button>
 					</c:if>
					<button type="button"  ${ rirForm.completedByTechnician ? '' : 'disabled' }
							onclick=" event.preventDefault(); genDocPopup(${rirForm.rirId}, 'rirdocument.htm?ririd=', 'Generating Document'); "  title="<spring:message code='generate' />" >
						<spring:message code="generate"/>
					</button>
			</div>
	</fieldset>
	<!-- Internal Informations END-->
	
</form:form>
<div class="clear"></div>
