<%-- File name: /trescal/core/jobs/jobitem/fragments/linkedPurchaseOrderItems.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:if test="${not empty poItems}">
	<tr id="${appendTo}-child">
		<td colspan="6">
			<div style="display: none;">
				<table class="child_table" summary="This table lists all delivery items for a particular delivery">
					<thead>
						<tr>
							<td colspan="6">
								<c:choose>
									<c:when test="${poItems.size() == 1}">
										<spring:message code="purchaseorder.onepurchaseorderitem"/>
									</c:when>
									<c:otherwise>
										<spring:message code="purchaseorder.somepurchaseorderitems" arguments="${poItems.size()}"/>
									</c:otherwise>
								</c:choose>
							</td>
						</tr>
						<tr>
							<th scope="col" class="item"><spring:message code="itemno"/></th>
							<th scope="col" class="desc"><spring:message code="description"/></th>
							<th scope="col" class="disc"><spring:message code="discount"/></th>
							<th scope="col" class="cost"><spring:message code="cost"/></th>
							<th scope="col" class="jitem"><spring:message code="jobitems"/></th>
							<th scope="col" class="link"><spring:message code="viewjob.link"/></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="poItem" items="${poItems}" varStatus="poItemIndex">
							<tr>
								<td>${poItem.itemNo}</td>
								<td>${poItem.description}</td>
								<td><fmt:formatNumber value="${poItem.generalDiscountRate/100}" type="PERCENT" maxFractionDigits="2"/></td>
								<td><fmt:formatNumber value="${poItem.finalCost}" type="CURRENCY" currencySymbol="${poItem.currencyERSymbol}"/></td>
								<!-- item already linked to job item? -->
								<c:choose>
									<c:when test="${poItem.jobItemId == null}">
										<td>
											<select>
												<c:forEach var="ji" items="${jobItems}">
													<option value="${ji.key}" <c:if test="${ji.key == jobItemId}">selected</c:if>>${ji.value}</option>
												</c:forEach>
											</select>
										</td>
										<td>
											<a href="" class="mainlink" id="linkPOI${poItem.itemId}"
												onclick="event.preventDefault(); linkJobItemToPoItem($j(this).parent().prev().find('select').val(), '${poItem.itemId}');">
												<spring:message code="purchaseorder.linkItem"/>
											</a>
										</td>
									</c:when>
									<c:otherwise>
										<td colspan=2>
											<c:choose>
												<c:when test="${poItem.jobItemId == jobItemId}">
													<spring:message code="purchaseorder.linkjobitemtopoitem"/>
												</c:when>
												<c:otherwise>
													<spring:message code="purchaseorder.linkedto" arguments="${poItem.jobNo}.${poItem.jobItemNo}"/>
												</c:otherwise>
											</c:choose>
										</td>
									</c:otherwise>
								</c:choose>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</td>
	</tr>
</c:if>