<%-- File name: /trescal/core/jobs/bpousage.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="bpodetails.headtext"/></span>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox">
			<fieldset>
				<legend><spring:message code="bpodetails.headtext"/></legend>
				<div class="displaycolumn">
					<ol>
						<li>
							<label class="width40"><spring:message code="bpo.ponum"/>:</label>
							<span>${bpo.poNumber}</span>
						</li>
						<li>
							<label class="width40"><spring:message code="linkedby"/>:</label>
							<span>${bpo.scope}</span>
						</li>
						<c:set var="bpoCompany" value=""/>
						<c:set var="bpoSubdiv" value=""/>
						<c:set var="bpoContact" value=""/>
						<c:set var="bpoCurrencySymbol" value=""/>
						
						<c:choose>
							<c:when test="${not empty bpo.company}">
								<c:set var="bpoCompany" value="${bpo.company}"/>
							</c:when>
							<c:when test="${not empty bpo.subdiv}">
								<c:set var="bpoCompany" value="${bpo.subdiv.comp}"/>
								<c:set var="bpoSubdiv" value="${bpo.subdiv}"/>
							</c:when>
							<c:when test="${not empty bpo.contact}">
								<c:set var="bpoCompany" value="${bpo.contact.sub.comp}"/>
								<c:set var="bpoSubdiv" value="${bpo.contact.sub}"/>
								<c:set var="bpoContact" value="${bpo.contact}"/>
							</c:when>
						</c:choose>
						<c:if test="${not empty bpoCompany}">
							<c:set var="bpoCurrencySymbol" value="${bpoCompany.currency.currencySymbol}"/>
							<li>
								<label class="width40"><spring:message code="company"/>:</label>
								<span><links:companyLinkDWRInfo company="${bpoCompany}" rowcount="1" copy="true"/>:</span>
							</li>
						</c:if>
						<c:if test="${not empty bpoSubdiv}">
							<li>
								<label class="width40"><spring:message code="subdiv"/>:</label>
								<span><links:subdivLinkDWRInfo subdiv="${bpoSubdiv}" rowcount="1"/>:</span>
							</li>
						</c:if>
						<c:if test="${not empty bpoContact}">
							<li>
								<label class="width40"><spring:message code="contact"/>:</label>
								<span><links:contactLinkDWRInfo contact="${bpoContact}" rowcount="1"/>:</span>
							</li>
						</c:if>
					</ol>
				</div>
				<div class="displaycolumn">
					<ol>
						<li>
							<label class="width40"><spring:message code="bpo.active"/>:</label>
							<span><spring:message code="${bpo.active}"/></span>
						</li>
						<li>
							<label class="width40"><spring:message code="bpo.startdate"/>:</label>
							<span><fmt:formatDate value="${bpo.durationFrom}" dateStyle="medium" type="date" /></span>
						</li>
						<li>
							<label class="width40"><spring:message code="bpo.enddate"/>:</label>
							<span><fmt:formatDate value="${bpo.durationTo}" dateStyle="medium" type="date" /></span>
						</li>
						<li>
							<label class="width40"><spring:message code="bpo.limitamount"/>:</label>
							<span>${bpoCurrencySymbol}&nbsp;${bpo.limitAmount}</span>
						</li>
						<li>
							<label class="width40"><spring:message code="bpo.comment"/>:</label>
							<span>${bpo.comment}</span>
						</li>
					</ol>
				</div>
			</fieldset>
		</div>
		<div class="infobox" id="usagetable">
			<table class="default2">
				<tbody>
					<tr>
						<th colspan="9"><spring:message code="bpousage.jobitemsonbpo"/></th>
					</tr>
					<tr>
						<th><spring:message code="jobno"/></th>
						<th><spring:message code="itemno"/></th>
						<th><spring:message code="instmodeditmod.modelname"/></th>
						<th><spring:message code="servicetype"/></th>
						<th><spring:message code="datein"/></th>
						<th><spring:message code="datecomplete"/></th>
						<th><spring:message code="jobstatus"/></th>
						<th colspan="2" style="text-align: right;"><spring:message code="cost"/></th>
					</tr>
					<c:if test="${empty bpoUsage}">
						<tr>
							<td class="bold" colspan="9"><spring:message code="jobresults.string1"/></td>
						</tr>
					</c:if>
					<c:forEach var="item" items="${bpoUsage}" varStatus="loopStatus">
						<tr>
							<td><links:jobnoLinkDWRInfo jobno="${item.jobNo}" jobid="${item.jobId}" copy="false" rowcount="${loopStatus.count}"/></td>
							<td><links:jobitemLinkInfo jobItemId="${item.jobItemId}" jobItemNo="${item.itemNo}" jobItemCount="${item.itemCount}" defaultPage="jiactions.htm"/></td>
							<td>${item.modelName}</td>
							<td>${item.serviceType}</td>
							<td><fmt:formatDate value="${item.regDate}" type="date" dateStyle="SHORT"/></td>
							<td><fmt:formatDate value="${item.dateComplete}" type="date" dateStyle="SHORT"/></td>
							<td>${item.jobStatus}</td>
							<td colspan="2" class="text-right">
								<fmt:formatNumber value="${item.amount}" type="currency" currencySymbol="${bpoCurrencySymbol}"/>
							</td>
						</tr>
					</c:forEach>
					<c:if test="${not empty expenseItems}">
					<tr>
						<th colspan="9"><spring:message code="viewjob.jobservices"/></td>
					</tr>
					<tr>
						<th><spring:message code="jobno"/></th>
						<th><spring:message code="itemno"/></th>
						<th><spring:message code="instmodeditmod.modelname"/></th>
						<th colspan="2"><spring:message code="comment"/></th>
						<th><spring:message code="date"/></th>
						<th><spring:message code="jobstatus"/></th>
						<th colspan="2" style="text-align: right;"><spring:message code="cost"/></th>
					</tr>
					<c:forEach var="item" items="${expenseItems}">
						<tr>
							<td><links:jobnoLinkDWRInfo jobno="${item.jobNo}" jobid="${item.jobId}" copy="false" rowcount="${loopStatus.count}"/></td>
							<td>${item.itemNo} / ${item.itemCount}</td>
							<td>${item.modelName}</td>
							<td colspan="2">${item.comment}</td>
							<td><fmt:formatDate value="${item.dateComplete}" type="date" dateStyle="SHORT"/></td>
							<td>${item.jobStatus}</td>
							<td colspan="2" class="text-right">
								<fmt:formatNumber value="${item.amount}" type="currency" currencySymbol="${bpoCurrencySymbol}"/>
							</td>
						</tr>
					</c:forEach>
					</c:if>
					<tr class="highlight-green">
						<td colspan="7" class="bold"><spring:message code="bpo.spentamount"/></td>
						<td colspan="2" style="text-align: right;" class="bold">
							<fmt:formatNumber value="${spentTotal}" type="currency" currencySymbol="${bpoCurrencySymbol}"/>
						</td>
					</tr>
				</tbody>
				<c:if test="${not empty periodicInvoices}">
					<tbody>
						<tr>
							<th colspan="9"><spring:message code="bpousage.periodicinvoicesonbpo"/></th>
						</tr>
						<tr>
							<th><spring:message code="jobno"/></th>
							<th><spring:message code="itemno"/></th>
							<th><spring:message code="instmodeditmod.modelname"/></th>
							<th><spring:message code="servicetype"/></th>
							<th><spring:message code="jobstatus"/></th>
							<th><spring:message code="datein"/></th>
							<th><spring:message code="datecomplete"/></th>
							<th><spring:message code="bpousage.onbpo" />
							<th style="text-align: right;"><spring:message code="bpousage.amountprice"/></th>
						</tr>
						<c:forEach var="periodicInvoice" items="${periodicInvoices}">
							<tr class="highlight-green">
								<td colspan="7">
									<a href="viewinvoice.htm?id=${periodicInvoice.periodicInvoiceId}" class="mainlink">
										${periodicInvoice.periodicInvoiceNumber}
									</a>
									<c:if test="${periodicInvoice.jobItems.size() == jobItemsOnPeriodicInvoiceLimit}">
										<img src="img/icons/info-red.png" title='<spring:message code="bpousage.limitjobitems" arguments="${jobItemsOnPeriodicInvoiceLimit}"/>'/>
									</c:if>
								</td>
								<td colspan="2" class="text-right bold"><fmt:formatNumber value="${periodicInvoice.periodicInvoiceAmount}" type="currency" currencySymbol="${bpoCurrencySymbol}"/></td>
							</tr>
							<c:forEach var="jobItem" items="${periodicInvoice.jobItems}">
								<tr>
									<td><links:jobnoLinkDWRInfo jobno="${jobItem.jobNo}" jobid="${jobItem.jobId}" copy="false" rowcount=""/></td>
									<td><links:jobitemLinkInfo jobItemId="${jobItem.jobItemId}" jobItemNo="${jobItem.itemNo}" jobItemCount="${jobItem.itemCount}" defaultPage="jiactions.htm"/></td>
									<td>${jobItem.modelName}</td>
									<td>${jobItem.serviceType}</td>
									<td>${jobItem.jobStatus}</td>
									<td><fmt:formatDate value="${jobItem.regDate}" type="date" dateStyle="SHORT"/></td>
									<td><fmt:formatDate value="${jobItem.dateComplete}" type="date" dateStyle="SHORT"/></td>
									<td class="center">
										<c:choose>
											<c:when test="${jobItem.itemOnBPO}">
												<img src="img/icons/greentick.png"/>
											</c:when>
											<c:otherwise>
												<img src="img/icons/redcross.png"/>
											</c:otherwise>
										</c:choose>
									</td>
									<td class="text-right"><fmt:formatNumber value="${jobItem.amount}" type="currency" currencySymbol="${bpoCurrencySymbol}"/></td>
								</tr>
							</c:forEach>
						</c:forEach>
					</tbody>
				</c:if>
				<tbody>
					<tr class="highlight-gold">
						<td colspan="7" class="bold"><spring:message code="bpo.remainingamount"/></td>
						<td colspan="2" style="text-align: right;" class="bold">
							<c:choose>
								<c:when test="${remainingTotal != null}">
									<fmt:formatNumber value="${remainingTotal}" type="currency" currencySymbol="${bpoCurrencySymbol}"/>
								</c:when>
								<c:otherwise>
									<spring:message code="notapplicable"/>								
								</c:otherwise>
							</c:choose>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</jsp:body>
</t:crocodileTemplate>
