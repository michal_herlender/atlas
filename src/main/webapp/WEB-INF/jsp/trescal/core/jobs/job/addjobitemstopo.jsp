<%-- File name: /trescal/core/jobs/job/addjobitemstopo.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<div id="addJobItemsToPOBoxy" class="overflow marg-bot">
	<table class="default2" id="jobitemtopotable" summary="This table displays all available job items">
		<thead>
			<tr>
				<td colspan="6">
					<c:choose>
						<c:when test="${jobItems.size() < 2}">
							<spring:message code="addjobitemstopo.jobItemCanBeAddOrRemove" arguments="${jobItems.size()},${poNo}"/>
						</c:when>
						<c:otherwise>
							<spring:message code="addjobitemstopo.jobItemsCanBeAddOrRemove" arguments="${jobItems.size()},${poNo}"/>
						</c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr>
				<th class="add" scope="col">
					<spring:message code="all"/><br/>
					<input type="checkbox" id="selectAll" onclick="selectAll(this.checked, 'jobitemtopotable');"/>
				</th>
				<th class="item" scope="col"><spring:message code="item"/></th>
				<th class="inst" scope="col"><spring:message code="instrument"/></th>
				<th class="serial" scope="col"><spring:message code="serialno"/></th>
				<th class="plant" scope="col"><spring:message code="plantno"/></th>
				<th class="caltype" scope="col"><spring:message code="caltype"/></th>
			</tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when test="${jobItems.size() > 0}">
					<c:forEach var="jobItem" items="${jobItems}">
						<tr style="background: ${jobItem.calTypeColour};">
							<td class="center">
								<input type="checkbox" name="jobitemforpo" value="${jobItem.id}" onclick="unassignejobitemAndJobServicesFromBPO($(this),${jobItem.id},null,${poId}, ${jobId});"
									<c:if test="${jobItem.onClientPO}">
										checked
									</c:if>
								>
							</td>
							<td>${jobItem.itemNo}</td>
							<td>${jobItem.modelName}</td>
							<td>${jobItem.serialNo}</td>
							<td>${jobItem.plantNo}</td>
							<td>${jobItem.calType} - ${jobItem.calTypeLong}</td>
						</tr>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<tr class="bold center">
						<td colspan="6" class="text-center bold"><spring:message code="showitemsoncpo.noitemsonjob"/></td>																								
					</tr>
				</c:otherwise>
			</c:choose>
		</tbody>
	</table>
	<table class="default2" id="expenseitemtopotable" summary="This table displays all available job services">
		<thead>
			<tr>
				<td colspan="6">
					<c:choose>
						<c:when test="${jobServices.size() < 2}">
							<spring:message code="addjobitemstopo.expenseItemCanBeAddOrRemove" arguments="${jobServices.size()},${poNo}"/>
						</c:when>
						<c:otherwise>
							<spring:message code="addjobitemstopo.expenseItemsCanBeAddOrRemove" arguments="${jobServices.size()},${poNo}"/>
						</c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr>
				<th class="add" scope="col">
					<spring:message code="all"/><br/>
					<input type="checkbox" id="selectAll" onclick="selectAll(this.checked, 'expenseitemtopotable');"/>
				</th>
				<th class="item" scope="col"><spring:message code="item"/></th>
				<th class="inst" scope="col"><spring:message code="instrument"/></th>
				<th class="comment" scope="col"><spring:message code="comment"/></th>
			</tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when test="${jobServices.size() > 0}">
					<c:forEach var="jobService" items="${jobServices}">
						<tr>
							<td class="center">
								<spring:message var="onAnotherPoOrBpo" code="addjobitemstopo.onanotherpoOrbpo"/>
								<input type="checkbox" name="jobserviceforpo" value="${jobService.id}" onclick="unassignejobitemAndJobServicesFromBPO($(this),null,${jobService.id},${poId}, ${jobId});"
									<c:choose>
										<c:when test="${jobService.onClientPO}">
											checked
										</c:when>
										<c:when test="${jobService.onAnyClientPO}">
											disabled title="${onAnotherPoOrBpo}"
										</c:when>
									</c:choose>
								>
							</td>
							<td>${jobService.itemNo}</td>
							<td>${jobService.modelName}</td>
							<td>${jobService.comment}</td>
						</tr>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<tr class="bold center">
						<td colspan="6" class="text-center bold"><spring:message code="showitemsoncpo.noitemsonjob"/></td>																								
					</tr>
				</c:otherwise>
			</c:choose>
		</tbody>
	</table>
	<div class="center-pad">
	<c:choose>
		<c:when test="${ actionOnPO }">
			<input type="button" value="<spring:message code="submit"/>" onclick="this.disabled = true; submitPurchaseOrderJobItems(${poId}, ${jobId});"/>
		</c:when>
		<c:otherwise>
			<input type="button" value="<spring:message code="submit"/>" onclick="this.disabled = true; submitBlanketPurchaseOrderJobItems(${poId}, '${poNo}', ${jobId});"/>
		</c:otherwise>
	</c:choose>
	</div>
</div>

