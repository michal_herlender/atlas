<%-- File name: /trescal/core/jobs/jobitem/costsourceoverlay.jsp--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<div class="text-center marg-top" id="switchCostOverlay">
	<div class="warningBox1 hid">
		<div class="warningBox2">
			<div class="warningBox3">
				&nbsp;
			</div>
		</div>
	</div>
	<div>&nbsp;</div>
	<%-- sub navigation menu which displays and hides sections of page --%> 
	<div id="subnav">
		<dl>
			<dt>
				<a href="#" onclick=" event.preventDefault(); switchMenuFocus(newCostElements, 'ncs_summary-tab', false); " id="ncs_summary-link" class="selected">
					<spring:message code="summary" />
				</a>
			</dt>
			<dt>
				<a href="#" onclick=" event.preventDefault(); switchMenuFocus(newCostElements, 'ncs_qitem-tab', false); " id="ncs_qitem-link">
				 	<spring:message code="docs.quotationitems" />
				 	<c:out value=" ( ${quotationItemSelectableOutputs.size() + quotationItemOtherOutputs.size()} )"/>
				 </a>
			</dt>
			<dt>
				<a href="#" onclick=" event.preventDefault(); switchMenuFocus(newCostElements, 'ncs_model-tab', false); " id="ncs_model-link">
					<spring:message code="instmod.instrumentmodelprices" />
					<c:out value=" ( ${catalogPriceOutputs.size()} )"/>
				</a>
			</dt>
			<dt>
				<a href="#" onclick=" event.preventDefault(); switchMenuFocus(newCostElements, 'ncs_jobpricing-tab', false); " id="ncs_jobpricing-link">
					<spring:message code="viewinstrument.jobhistory" />
					<c:out value=" ( ${jobCostingOutputs.size()} )"/>
				</a>
			</dt>
		</dl>
	</div>
	<%-- end of sub navigation menu --%>						
	<%-- div containing summary table --%>
	<div id="ncs_summary-tab">
		<table class="default4">
			<thead>
				<tr>
					<td colspan="2">
						<spring:message code="jobitemlink.information" />
					</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<th class="sfield" scope="col">
						<spring:message code="clientcompany" />
					</th>
					<td class="svalue" scope="col">
						<c:out value="${jobitem.job.con.sub.comp.coname}" />
					</td>
				</tr>
				<tr>
					<th>
						<spring:message code="barcode" />
					</th>
					<td>
						<c:out value="${jobitem.inst.plantid}" />
					</td>
				</tr>
				<tr>
					<th>
						<spring:message code="servicetype" />
					</th>
					<td>
						<cwms:besttranslation translations="${jobitem.calType.serviceType.shortnameTranslation}" />
						<c:out value=" - " />
						<cwms:besttranslation translations="${jobitem.calType.serviceType.longnameTranslation}" />
					</td>
				</tr>
				<tr>
					<th>
						<spring:message code="instmodelname" />
					</th>
					<td>
						<c:set var="instrumentModelUrl" value="instrumentmodelprices.htm?modelid=${jobitem.inst.model.modelid}" />
						<a href="#" onclick=" event.preventDefault(); window.open('${instrumentModelUrl}'); " class="mainlink">
							<cwms:showinstrument instrument="${jobitem.inst}" />
						</a>
					</td>
				</tr>
				<tr>
					<th>
						<spring:message code="viewjob.linkedquotes" />
					</th>
					<td>
						<c:choose>
							<c:when test="${empty jobitem.job.linkedQuotes}">
								<spring:message code="viewjob.none" />
							</c:when>
							<c:otherwise>
								<c:forEach var="jobQuoteLink" items="${jobitem.job.linkedQuotes}">
									<c:set var="quotationUrl" value="viewquotation.htm?id=${jobQuoteLink.quotation.id}" />
									<a href="#" onclick=" event.preventDefault(); window.open('${quotationUrl}'); " class="mainlink">
										<c:out value="${jobQuoteLink.quotation.qno} ver ${jobQuoteLink.quotation.ver}"/><br/>
									</a>
								</c:forEach>
							</c:otherwise>
						</c:choose> 
					</td>
				</tr>
				<tr>
					<th>
						<spring:message code="company.contract" />
					</th>
					<td>
						<c:choose>
							<c:when test="${empty jobitem.contract}">
								<spring:message code="viewjob.none" />
							</c:when>
							<c:otherwise>
								<c:out value="${jobitem.contract.contractNumber}" /><br/>
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
				<tr>
					<th>
						<spring:message code="company.contract" />
						<c:out value=" - "/>
						<spring:message code="quotation" />
					</th>
					<td>
						<c:choose>
							<c:when test="${empty jobitem.contract || empty jobitem.contract.quotation}">
								<spring:message code="viewjob.none" />
							</c:when>
							<c:otherwise>
								<c:set var="quotationUrl" value="viewquotation.htm?id=${jobitem.contract.quotation.id}" />
								<a href="#" onclick=" event.preventDefault(); window.open('${quotationUrl}'); " class="mainlink">
									<c:out value="${jobitem.contract.quotation.qno} ver ${jobitem.contract.quotation.ver}" /><br/>
								</a>
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	
	<%-- div containing quotation item tables --%>
	<div id="ncs_qitem-tab" class="hid">
		<fmt:formatDate var="formattedRegDateAfter" type="date" dateStyle="SHORT" value="${quotationRegDateAfter}"/>
		<div class="center">
			<spring:message code="viewjob.quotationsRegisteredAfter" arguments="${formattedRegDateAfter}" />
			<br/><br/>
		</div>
		<table class="default4">
			<thead>
				<tr>
					<td colspan="11">
						<spring:message code="costoverlay.activequotationitems" />
						<c:out value=" ( ${quotationItemSelectableOutputs.size()} )"/>
					</td>
				</tr>
				<tr>
					<th class="qsource" scope="col">
						<spring:message code="source" />
					</th>
					<th class="qlinked" scope="col">
						<spring:message code="linked" />
					</th>
					<th class="qexpiry" scope="col">
						<spring:message code="createquot.expirydate" />
					</th>
					<th class="qqno" scope="col">
						<spring:message code="quoteno" />
					</th>
					<th class="qitemno" scope="col">
						<spring:message code="itemno" />
					</th>
					<th class="qcost cost2pc" scope="col">
						<spring:message code="price" />
					</th>
					<th class="qdiscp cost2pc" scope="col">
						<spring:message code="discount" />
						<c:out value=" (%)"/>
					</th>
					<th class="qdiscv cost2pc" scope="col">
						<spring:message code="discount" />
					</th>
					<th class="qfinalc cost2pc" scope="col">
						<spring:message code="finalcost" />
					</th>
					<th class="qcurr cost2pc" scope="col">
						<spring:message code="currency" />
					</th>
					<th class="qupd" scope="col">
						<spring:message code="update" />
					</th>
				</tr>
			</thead>
			<tbody>
				<c:if test="${empty quotationItemSelectableOutputs}">
					<tr>
						<td colspan="11" class="bold center"><spring:message code="viewcapabilities.noresults" /></td>
					</tr>
				</c:if>
				<%-- quotationItemOutputs is a map of PriceLookupOutput to Quotationitem--%>
				<c:forEach var="priceLookupOutput" items="${quotationItemSelectableOutputs.keySet()}">
					<c:set var="quoteItem" value="${quotationItemSelectableOutputs.get(priceLookupOutput)}" />
					<tr>
						<td>
							<c:out value="${priceLookupOutput.queryType.getMessage()}" />
						</td>
						<td>
							<c:if test="${priceLookupOutput.preferredQuotation}">
								<spring:message code="yes" />
							</c:if>
						</td>
						<td>
							<fmt:formatDate value="${priceLookupOutput.quotationExpiryDate}" type="date" dateStyle="SHORT" />
						</td>
						<td>
							<c:set var="quotationUrl" value="viewquotation.htm?id=${quoteItem.quotation.id}" />
							<a href="#" onclick=" event.preventDefault(); window.open('${quotationUrl}'); " class="mainlink">
								<c:out value="${quoteItem.quotation.qno} ver ${quoteItem.quotation.ver}" />
							</a>
						</td>
						<td>
							<c:set var="quotationItemUrl" value="editquotationitem.htm?id=${quoteItem.id}" />
							<a href="#" onclick=" event.preventDefault(); window.open('${quotationItemUrl}'); " class="mainlink">
								<c:out value="${quoteItem.itemno}" />
							</a>
						</td>
						<td class="cost2pc">
							<c:out value="${quoteItem.quotation.currency.currencySymbol} ${priceLookupOutput.quotationCalCostTotalCost}" /> 
						</td>
						<td class="cost2pc">
							<c:out value="${priceLookupOutput.quotationCalCostDiscountRate} %" />
						</td>
						<td class="cost2pc">
							<c:out value="${quoteItem.quotation.currency.currencySymbol} ${quoteItem.calibrationCost.discountValue}" />
						</td>
						<td class="cost2pc">
							<c:out value="${quoteItem.quotation.currency.currencySymbol} ${quoteItem.calibrationCost.finalCost}" />
						</td>
						<td class="cost2pc">
							<c:out value="${quoteItem.quotation.currency.currencyCode}" /> 
						</td>
						<td>
							<a href="#" class="mainlink" onclick=" event.preventDefault(); updateCalibrationCostSource( ${jobitem.jobItemId}, ${priceLookupOutput.quotationCalCostId}, 'QUOTATION'); ">
								<spring:message code="select" />
							</a>
						</td>
					</tr>
				</c:forEach>
			</tbody>													
		</table>
		<br/>
		<table class="default4">
			<thead>
				<tr>
					<td colspan="11">
						<spring:message code="costoverlay.inactivequotationitems" />
						<c:out value=" ( ${quotationItemOtherOutputs.size()} )"/>
					</td>
				</tr>
				<tr>
					<th class="qsource" scope="col">
						<spring:message code="source" />
					</th>
					<th class="qstatus" scope="col">
						<spring:message code="status" />
					</th>
					<th class="qexpiry" scope="col">
						<spring:message code="createquot.expirydate" />
					</th>
					<th class="qqno" scope="col">
						<spring:message code="quoteno" />
					</th>
					<th class="qitemno" scope="col">
						<spring:message code="itemno" />
					</th>
					<th class="qcost cost2pc" scope="col">
						<spring:message code="price" />
					</th>
					<th class="qdiscp cost2pc" scope="col">
						<spring:message code="discount" />
						<c:out value=" (%)"/>
					</th>
					<th class="qdiscv cost2pc" scope="col">
						<spring:message code="discount" />
					</th>
					<th class="qfinalc cost2pc" scope="col">
						<spring:message code="finalcost" />
					</th>
					<th class="qcurr cost2pc" scope="col">
						<spring:message code="currency" />
					</th>
					<th class="qupd" scope="col">
						<spring:message code="update" />
					</th>
				</tr>
			</thead>
			<tbody>
				<c:if test="${empty quotationItemOtherOutputs}">
					<tr>
						<td colspan="11" class="bold center"><spring:message code="viewcapabilities.noresults" /></td>
					</tr>
				</c:if>
				<%-- quotationItemOutputs is a map of PriceLookupOutput to Quotationitem--%>
				<c:forEach var="priceLookupOutput" items="${quotationItemOtherOutputs.keySet()}">
					<c:set var="quoteItem" value="${quotationItemOtherOutputs.get(priceLookupOutput)}" />
					<tr>
						<td>
							<c:out value="${priceLookupOutput.queryType.getMessage()}" />
						</td>
						<td>
							<c:choose>
								<c:when test="${not priceLookupOutput.quotationAccepted || 
									not priceLookupOutput.quotationIssued}">
									<c:set var="statusStyle" value="color:red;" />
								</c:when>
								<c:otherwise>
									<c:set var="statusStyle" value=""/>
								</c:otherwise>
							</c:choose>
							<span style="${statusStyle}">
								<cwms:besttranslation translations="${quoteItem.quotation.quotestatus.nametranslations}" />
							</span>
						</td>
						<td>
							<fmt:formatDate value="${priceLookupOutput.quotationExpiryDate}" type="date" dateStyle="SHORT" />
							<c:if test="${quoteItem.quotation.expired}">
								<span style="color:red;">
									[<spring:message code="viewquot.expired" />]
								</span>
							</c:if>
						</td>
						<td>
							<c:set var="quotationUrl" value="viewquotation.htm?id=${quoteItem.quotation.id}" />
							<a href="#" onclick=" event.preventDefault(); window.open('${quotationUrl}'); " class="mainlink">
								<c:out value="${quoteItem.quotation.qno} ver ${quoteItem.quotation.ver}" />
							</a>
						</td>
						<td>
							<c:set var="quotationItemUrl" value="editquotationitem.htm?id=${quoteItem.id}" />
							<a href="#" onclick=" event.preventDefault(); window.open('${quotationItemUrl}'); " class="mainlink">
								<c:out value="${quoteItem.itemno}" />
							</a>
						</td>
						<td class="cost2pc">
							<c:out value="${quoteItem.quotation.currency.currencySymbol} ${priceLookupOutput.quotationCalCostTotalCost}" /> 
						</td>
						<td class="cost2pc">
							<c:out value="${priceLookupOutput.quotationCalCostDiscountRate} %" />
						</td>
						<td class="cost2pc">
							<c:out value="${quoteItem.quotation.currency.currencySymbol} ${quoteItem.calibrationCost.discountValue}" />
						</td>
						<td class="cost2pc">
							<c:out value="${quoteItem.quotation.currency.currencySymbol} ${quoteItem.calibrationCost.finalCost}" />
						</td>
						<td class="cost2pc">
							<c:out value="${quoteItem.quotation.currency.currencyCode}" /> 
						</td>
						<td>
							<spring:message code="company.inactive" />
						</td>
					</tr>
				</c:forEach>
			</tbody>													
		</table>
	</div>
	<%-- div containing model default costs table  --%>
	<div id="ncs_model-tab" class="hid">
		<table class="default4" >
			<thead>
				<tr>
					<td colspan="7">
						<spring:message code="instmod.instrumentmodelprices" />
						<c:out value=" ( ${catalogPriceOutputs.size()} )"/>
					</td>
				</tr>
				<tr>
					<th class="msource" scope="col">
						<spring:message code="source" />
					</th>
					<th class="mtype" scope="col">
						<spring:message code="instmod.modeltype" />
					</th>
					<th class="mname" scope="col">
						<spring:message code="instmodelname" />
					</th>
                    <th class="mservice" scope="col">
                    	<spring:message code="servicetype" />
                    </th>
					<th class="mprice cost2pc" scope="col">
						<spring:message code="price" />
					</th>
					<th class="mcurrency cost2pc" scope="col">
						<spring:message code="currency" />
					</th>
                    <th class="mupdate" scope="col">
                    	<spring:message code="update" />
                    </th>
				</tr>
			</thead>
			<tbody>
				<c:if test="${empty catalogPriceOutputs}">
					<tr>
						<td colspan="7" class="bold center"><spring:message code="viewcapabilities.noresults" /></td>
					</tr>
				</c:if>
				<%-- catalogPriceOutputs is a map of PriceLookupOutput to CatalogPrice --%>
				<c:forEach var="priceLookupOutput" items="${catalogPriceOutputs.keySet()}">
					<c:set var="catalogPrice" value="${catalogPriceOutputs.get(priceLookupOutput)}" />
					<tr>
						<td>
							<c:out value="${priceLookupOutput.queryType.getMessage()}" />
						</td>
						<td>
							<cwms:besttranslation translations="${catalogPrice.instrumentModel.modelType.modelTypeNameTranslation}" />
						</td>
						<td>
							<c:set var="instrumentModelUrl" value="instrumentmodelprices.htm?modelid=${catalogPrice.instrumentModel.modelid}" />
							<a href="#" onclick=" event.preventDefault(); window.open('${instrumentModelUrl}'); " class="mainlink">
								<cwms:showmodel instrumentmodel="${catalogPrice.instrumentModel}" />
							</a>
						</td>
						<td>
							<cwms:besttranslation translations="${catalogPrice.serviceType.shortnameTranslation}" />
						</td>
						<td class="cost2pc">
							<c:out value="${businessCurrency.currencySymbol} ${catalogPrice.fixedPrice}"/>
						</td>
						<td class="cost2pc">
							<c:out value="${businessCurrency.currencyCode}"/>
						</td>
						<td>
							<a href="#" class="mainlink" onclick=" event.preventDefault(); updateCalibrationCostSource(${jobitem.jobItemId}, ${catalogPrice.id}, 'MODEL'); ">
								<spring:message code="select" /> 
							</a>
						</td>
					</tr>
				</c:forEach>
			</tbody>													
		</table>
	</div>
	
	<%-- div containing quotation item tables --%>
	<div id="ncs_jobpricing-tab" class="hid">
		<fmt:formatDate var="formattedRegDateAfter" type="date" dateStyle="SHORT" value="${quotationRegDateAfter}"/>
		<div class="center">
			<spring:message code="viewjob.costingsIssuedAfter" arguments="${formattedRegDateAfter}" />
			<br/><br/>
		</div>
		<table class="default4" >
			<thead>
				<tr>
					<td colspan="9">
						<spring:message code="viewinstrument.jobhistory" />
						<c:out value=" ( ${jobCostingOutputs.size()} )"/>
					</td>
				</tr>
				<tr>
					<th class="jno" scope="col">
						<spring:message code="docs.jobcosting" />
					</th>
					<th class="jitemno" scope="col">
						<spring:message code="itemno" />
					</th>
					<th class="jissued" scope="col">
						<spring:message code="issuedate" />
					</th>
					<th class="jcost" scope="col">
						<spring:message code="price" />
					</th>
                    <th class="jdiscp" scope="col">
                    	<spring:message code="discount" />
                    	<c:out value=" (%)"/>
                    </th>
					<th class="jdiscv cost2pc" scope="col">
						<spring:message code="discount" />
					</th>
					<th class="jfinalc cost2pc" scope="col">
						<spring:message code="finalcost" />
					</th>
					<th class="jcurrency cost2pc" scope="col">
						<spring:message code="currency" />
					</th>
                    <th class="jupdate" scope="col">
                    	<spring:message code="update" />
                    </th>
				</tr>
			</thead>
			<tbody>
				<c:if test="${empty jobCostingOutputs}">
					<tr>
						<td colspan="9" class="bold center"><spring:message code="viewcapabilities.noresults" /></td>
					</tr>
				</c:if>
				<c:forEach var="jcCost" items="${jobCostingOutputs}">
					<c:set var="currencyName" value="${jcCost.jobCostingItem.jobCosting.currency.currencyName}" />
					<c:set var="currencySymbol" value="${jcCost.jobCostingItem.jobCosting.currency.currencySymbol}" />
					<tr>
						<td>
							<a href="#" onclick=" event.preventDefault(); window.open('viewjobcosting.htm?id=${jcCost.jobCostingItem.jobCosting.id}'); " class="mainlink">
								<c:out value="${jcCost.jobCostingItem.jobCosting.identifier}" />
							</a>
						</td>
						<td>
							<a href="#" onclick=" event.preventDefault(); window.open('editjobcostingitem.htm?id=${jcCost.jobCostingItem.id}'); " class="mainlink">
								<c:out value="${jcCost.jobCostingItem.itemno}" />
							</a>
						</td>
						<td>
							<fmt:formatDate type="date" dateStyle="SHORT" value="${jcCost.jobCostingItem.jobCosting.issuedate}" />
						</td>
						<td>
							<c:out value="${currencySymbol} ${jcCost.totalCost}" />
						</td>
						<td>
							<c:out value="${jcCost.discountRate} %" />
						</td>
						<td>
							<c:out value="${currencySymbol} ${jcCost.discountValue}" />
						</td>
						<td>
							<c:out value="${currencySymbol} ${jcCost.finalCost}" />
						</td>
						<td>
							<c:out value="${currencyName}" />
						</td>
						<td>
							<a href="#" class="mainlink" onclick=" event.preventDefault(); updateCalibrationCostSource(${jobitem.jobItemId}, ${jcCost.costid}, 'JOB_COSTING'); ">
								<spring:message code="select" /> 
							</a>
						</td>
					</tr>
				</c:forEach>
			</tbody>													
		</table>
	</div>
</div>
