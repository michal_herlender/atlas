<%-- File name: /trescal/core/jobs/jobitem/jobexpenseitem.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code='jobexpenseitem.header'/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
    </jsp:attribute>
    <jsp:body>
    	<form:form modelAttribute="expenseItem">
    		<fieldset>
    			<form:hidden path="id"/>
    			<ol>
    				<li>
    					<form:label path="model">Service</form:label>
    					<form:select path="model" items="${models}" itemLabel="name"/>
    				</li>
    				<li>
    					<form:label path="comment">Comment</form:label>
    					<form:input path="comment" type="textarea"/>
    				</li>
    				<li>
    					<form:label path="singlePrice">Single Price</form:label>
    					<form:input path="singlePrice"/>
    				</li>
    				<li>
    					<form:label path="quantity">Quantity</form:label>
    					<form:input path="quantity" type="number" min="1"/>
    				</li>
    			</ol>
    		</fieldset>
    	</form:form>
    </jsp:body>
</t:crocodileTemplate>