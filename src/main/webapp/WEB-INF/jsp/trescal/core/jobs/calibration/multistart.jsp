<%-- File name: /trescal/core/jobs/calibration/multistart.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="multistart.headtext"/>&nbsp;<c:out value="${form.job.jobno}"/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
		<script type='module' src='script/components/search-plugins/cwms-capability-search/cwms-capability-search.js'></script>
        <script type='text/javascript' src='script/trescal/core/jobs/calibration/MultiStart.js'></script>
    </jsp:attribute>
    <jsp:body>
     		<form:form action="" method="post" modelAttribute="form">
		
			<div class="infobox">
					<form:errors path="form.*">
					<c:set var="formHasError" value="${false}"/>
       				 <div class="warningBox1">
              		  <div class="warningBox2">
              	       <div class="warningBox3">
                    	<h5 class="center-0 attention"><spring:message code="multistart.errors"/>:</h5>
                      <c:forEach var="e" items="${messages}">
                        <div class="center attention"><c:out value="${e}"/></div>
                      </c:forEach>
                     </div>
                   </div>
                  </div>
            	 </form:errors>
									
				<table class="default4 multiCalibrationStart" summary="">
					<thead>
						<tr>
							<td colspan="7"><c:out value="${form.cals.size()} "/><spring:message code="multistart.itemsready"/></td>
						</tr>
						<tr>
							<th class="add" scope="col"><spring:message code="multistart.all"/> <input type="checkbox" onclick=" selectAllItems(this.checked, 'multiCalibrationStart'); " <c:if test="${formHasError == false}"> checked="checked" </c:if> /></th>
							<th class="itemno" scope="col"><spring:message code="multistart.item"/></th>
							<th class="inst" scope="col"><spring:message code="calsearchresults.instrument"/></th>
							<th class="proc" scope="col"><spring:message code="multistart.itemsready"/></th>
							<th class="class" scope="col"><spring:message code="completecalibration.lblfoundleft"/></th>
							<th class="process" scope="col"><spring:message code="multistart.calprocess"/></th>
							<th class="caltype" scope="col"><spring:message code="calsearch.casltype"/></th>
						</tr>
					</thead>
				</table>
											
				<c:set var="cals" value="${form.cals}"/>
				<c:set var="count" value="0" />
				<c:forEach var="cal" items="${cals}">
					<table class="default4 multiCalibrationStart" summary="">								
						
						<tbody>
							<c:forEach var="ci" items="${cal.items}" varStatus="calStatus">
								<tr>
									<td class="add">
									<c:set var="checked" value=""/>
										<c:choose>
											<c:when test="${(calStatus.index+1) == 1}" >
													<c:set var="formHasError" value="${true}"/>
													<c:choose>
														<c:when test="${form.cals[count].selected == true}"><c:set var="checked" value="checked"/></c:when>
														<c:otherwise><c:set var="checked" value=""/></c:otherwise>
													</c:choose>
													<form:checkbox path="cals[${count}].selected" value="true" checked="${checked}" onclick=" (this.checked) ? this.value = true : this.value = false; " />
											</c:when>
											<c:otherwise>
												<spring:message code="multistart.grouped"/>
											</c:otherwise>
										</c:choose>
									</td>
									<td class="itemno">${ci.ji.itemNo}</td>
									<td <c:choose><c:when test="${(calStatus.index+1) > 1}" > colspan="5" </c:when>
												  <c:otherwise> class="inst" </c:otherwise>
										</c:choose>>
											${ci.ji.inst.definitiveInstrument}
									</td>
									<c:if test="${(calStatus.index+1) == 1}">
										<td class="proc">
											<cwms-capability-search-form 
											name="cals[${count}].procId"
											procid="${cal.capability.id}" defaultvalue="${cal.capability.reference}"></cwms-capability-search-form>
										</td>
										<td class="class">

										<form:select path="cals[${count}].calClass">
											<c:forEach var="cla" items="${classes}">
												<option value="${cla}" <c:if test="${cal.calClass != null && (cal.calClass == cla)}"> selected="selected" </c:if>>${cla.desc}</option>
											</c:forEach>
										</form:select>
										</td>
										<td class="process">
											<form:select path="cals[${count}].calProcessId">
												<c:forEach var="p" items="${processes}">
													<c:if test="${not empty p.applicationComponent and !p.applicationComponent}">
														<option value="${p.id}" 
															<c:if test="${cal.calProcessId != null && (cal.calProcessId == p.id)}"> selected="selected" </c:if>>
																${p.process}
														</option>
													</c:if>
												</c:forEach>
											</form:select>
										</td>
										<td class="caltype">
											<t:showTranslationOrDefault translations="${cal.calType.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/>
										</td>
									</c:if>
								</tr>
							</c:forEach>	
						</tbody>												
						
					</table>
					<c:set var="count" value="${count + 1}"/>
				</c:forEach>
				
				<div class="clear-height">&nbsp;</div>
				
				<c:if test="${not empty form.ji}">
					<div class="warningBox1">
						<div class="warningBox2">
							<div class="warningBox3 center-0"><spring:message code="multistart.warning3"/></div>
						</div>
					</div>
				</c:if>
				
				<!-- this table displays all default procedure standards for the selected procedure -->
				<table class="procedureStds default4" summary="This table displays all default standards">
					<thead>
						<tr>
							<td colspan="6"><spring:message code="calsearchresults.standards"/></td>
						</tr>
						<tr>
							<th class="stduse" scope="col"><spring:message code="multistart.use"/></th>
							<th class="stdbar" scope="col"><spring:message code="barcode"/></th>
							<th class="stdinst" scope="col"><spring:message code="calsearchresults.instrument"/></th>
							<th class="stdserial" scope="col"><spring:message code="serialno"/></th>
							<th class="stdplant" scope="col"><spring:message code="plantno"/></th>
							<th class="stdenf" scope="col"><spring:message code="multistart.enforce"/></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="6">
								<a href="#" class="domthickbox mainlink" onclick=" tb_show('Add Standards', 'TB_dom?width=800&height=400', '', thickboxAdditionalStandardsContent()); return false; " title=""><spring:message code="multistart.addaddstand"/></a>
							</td>
						</tr>
					</tfoot>
					<tbody id="pStds">
						<tr>
							<th colspan="6"><spring:message code="multistart.procstands"/> (<span id="pStdsSize">${procstandards.size()}</span>)</th>
						</tr>
						<c:choose>
							<c:when test="${procstandards.size() < 1}">
							<tr>
								<td colspan="6" class="bold center">
									<spring:message code="multistart.nodefstands"/>
								</td>
							</tr>
							</c:when>
							<c:otherwise>
							<c:forEach var="std" items="${procstandards}">
								<c:set var="instrument" value="${std.instrument}"/>
								<c:set var="checked" value=""/>
								<c:choose>
									<c:when test="${instrument.isOutOfCalibration() == true}">
										<c:set var="outOfCal" value=""/><br/><span class="attention">OUT OF CALIBRATION</span>
										<c:set var="rowcolor" value='highlight'/>
									</c:when>
									<c:otherwise>
										<c:set var="outOfCal" value=""/>
										<c:set var="rowcolor" value=""/>
										<c:set var="checked" value=" checked='checked'" />
									</c:otherwise>
								</c:choose>
								<c:if test="${not empty form.stdIds}">
									<c:set var="checked" value=""/>
									<c:if test="${form.stdIds.contains(std.instrument.plantid)}">
										<c:set var="checked" value=" checked='checked'" />
									</c:if>	
								</c:if>			
								<tr id="std${instrument.plantid}" class="${rowcolor}">
									<td class="stduse">
										<input type="checkbox" class="standardId" name="instrums" value="${instrument.plantid}" onclick=" toggleStd(this); " ${checked} />
										<input type="hidden" name="procStdsOnScreen" value="${std.id}" />
									</td>
									<td class="stdbar"><links:instrumentLinkDWRInfo instrument="${instrument}" rowcount="0" displayBarcode="true" displayName="false" displayCalTimescale="true" caltypeid="${form.ji.calType.calTypeId}"/></td>
									<td class="stdinst"><instmodel:showInstrumentModelLink instrument="${instrument}" caltypeid="0"/> ${outOfCal}</td>
									<td class="stdserial">${instrument.serialno}</td>
									<td class="stdplant">${instrument.plantno}</td>
									<td class="stdenf">
										<c:choose>
											<c:when test="${std.enforceUse == true}">
											<img src="img/icons/bullet-tick.png" width="10" height="10" alt="<spring:message code="multistart.enforceuse"/>" title="<spring:message code="multistart.enforceuse"/>" />
											</c:when>
											<c:otherwise>
											<img src="img/icons/bullet-cross.png" width="10" height="10" alt="<spring:message code="multistart.donotenforce"/>" title="<spring:message code="multistart.donotenforce"/>" />
											</c:otherwise>
										</c:choose>
									</td>
								</tr>
							</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
					<tbody id="wiStds" class="hid">
						<tr>
							<th colspan="6"><spring:message code="multistart.workinststand"/> (<span id="wiStdsSize">${wistandards.size()}</span>)</th>
						</tr>
						<c:choose>
							<c:when test="${wistandards.size() < 1}">
							<tr>
								<td colspan="6" class="bold center">
									<spring:message code="multistart.nodefstandforinstr"/>
								</td>
							</tr>
							</c:when>
							<c:otherwise>
							<c:forEach var="std" items="${wistandards}">
								<c:set var="instrument" value="${std.instrument}"/>
								<c:set var="checked" value=""/>
								<c:choose>
									<c:when test="${instrument.isOutOfCalibration() == true}">
									<c:set var="outOfCal" value=""/><br/><span class="attention">OUT OF CALIBRATION</span>
									<c:set var="rowcolor" value='highlight'/>
									</c:when>
									<c:otherwise>
									<c:set var="outOfCal" value=""/>
									<c:set var="rowcolor" value=""/>
									<c:set var="checked" value=" checked='checked'" />
									</c:otherwise>
								</c:choose>
								<c:if test="${not empty form.stdIds}">
									<c:set var="checked" value=""/>
									<c:if test="${form.stdIds.contains(std.instrument.plantid)}">
										<c:set var="checked" value=" checked='checked'" />
									</c:if>	
								</c:if>
								<tr id="std${instrument.plantid}" class="${rowcolor}">
									<td class="center">
										<input type="checkbox" class="standardId" name="instrums" value="${instrument.plantid}" onclick=" toggleStd(this); " ${checked} />
										<input type="hidden" name="procStdsOnScreen" value="${std.id}" />
									</td>
									<td class="stdbar"><links:instrumentLinkDWRInfo instrument="${instrument}" rowcount="0" displayBarcode="true" displayName="false" displayCalTimescale="true" caltypeid="${form.ji.calType.calTypeId}"/></td>
									<td><instmodel:showInstrumentModelLink instrument="${instrument}" caltypeid="0"/> ${outOfCal}</td>
									<td>${instrument.serialno}</td>
									<td>${instrument.plantno}</td>
									<td class="center">
										<c:choose>
											<c:when test="${std.enforceUse == true}">
												<img src="img/icons/bullet-tick.png" width="10" height="10" alt="<spring:message code="multistart.enforceuse"/>" title="<spring:message code="multistart.enforceuse"/>" />
											</c:when>
											<c:otherwise>
												<img src="img/icons/bullet-cross.png" width="10" height="10" alt="<spring:message code="multistart.donotenforce"/>" title="<spring:message code="multistart.donotenforce"/>" />
											</c:otherwise>
										</c:choose>
									</td>
								</tr>
							</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
				<!-- end of table to display all default procedure standards -->
														
				<c:choose>
					<c:when test="${(formHasError == true) && form.errorAdditionalStds.size() > 0}">
					
					<input type="hidden" name="instrums" value="0" />
					<!-- this table displays additional standards that have been added by the user in the event that an error
						occurs when the form has been submitted. Only shown if there are additional standards to display -->														
					<table class="default2 <c:if test="${form.errorAdditionalStds.size() < 1}"> hid </c:if> " id="additionalstds" summary="<spring:message code="multistart.alladdstand"/>">
						<thead>
							<tr>
								<td colspan="5"><spring:message code="multistart.addstand"/> (<span class="addstdsSizeSpan">${form.errorAdditionalStds.size()}</span>)</td>
							</tr>
							<tr>
								<th class="addstduse" scope="col"><spring:message code="multistart.use"/></th>
								<th class="addstdbar" scope="col"><spring:message code="barcode"/></th>
								<th class="addstdinst" scope="col"><spring:message code="calsearchresults.instrument"/></th>
								<th class="addstdserial" scope="col"><spring:message code="serialno"/></th>
								<th class="addstdplant" scope="col"><spring:message code="plantno"/></th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="5">
									&nbsp;
								</td>
							</tr>
						</tfoot>
						<tbody>
							<c:if test="${form.errorAdditionalStds.size() > 0}">
								<c:forEach var="instrument" items="${form.errorAdditionalStds}">
									<c:set var="alreadyOnScreen" value="${false}"/>
								<c:forEach var="test" items="${form.errorProcStds}">
										<c:if test="${test.instrument.plantid == instrument.plantid}">
											<c:set var="alreadyOnScreen" value="${true}"/>
										</c:if>
								</c:forEach>
									
									<c:if test="${alreadyOnScreen == false}">
										<c:set var="outOfCal" value=""/>
										<c:set var="checked" value=""/>
										<c:set var="rowcolor" value=""/>
										<c:if test="${instrument.outOfCalibration == true}">
											<c:set var="outOfCal" value=""/><br/><span class="attention"><spring:message code="multistart.outofcal"/></span>
											<c:set var="rowcolor" value='highlight'/>
										</c:if>
										<c:forEach var="i" items="${form.instrums}">
											<c:if test="${i == instrument.plantid}">
												<c:set var="checked" value=" checked='checked'" />
											</c:if>
										</c:forEach>
								
									<tr id="addstd${instrument.plantid}" class="${rowcolor}">
										<td class="center">
											<input type="checkbox" class="standardId" name="instrums" value="${instrument.plantid}" ${checked} />
											<input type="hidden" name="addStdsOnScreen" value="${instrument.plantid}" />
										</td>
										<td class="addstdbar"><links:instrumentLinkDWRInfo instrument="${instrument}" rowcount="0" displayBarcode="true" displayName="false" displayCalTimescale="true" caltypeid="${form.ji.calType.calTypeId}"/></td>
										<td><instmodel:showInstrumentModelLink instrument="${instrument}" caltypeid="0"/> ${outOfCal}</td>
										<td>${instrument.serialno}</td>
										<td>${instrument.plantno}</td>
									</tr>
									</c:if>
								</c:forEach>
							</c:if>
						</tbody>
					</table>
					<!-- end of table to display additional standards in the event of an error -->
														
					</c:when>
					<c:otherwise>
					
					<!-- this table displays any additional standards that the user may wish to add to this procedure,
							only displayed when the user chooses to add an additional standard -->							
					<table class="default2 hid" id="additionalstds" summary="This table displays all additional standards">
						<thead>
							<tr>
								<td colspan="5"><spring:message code="multistart.addstand"/> (<span class="addstdsSizeSpan">0</span>)</td>
							</tr>
							<tr>
								<th class="addstduse" scope="col"><spring:message code="multistart.use"/></th>
								<th class="addstdbar" scope="col"><spring:message code="barcode"/></th>
								<th class="addstdinst" scope="col"><spring:message code="calsearchresults.instrument"/></th>
								<th class="addstdserial" scope="col"><spring:message code="serialno"/></th>
								<th class="addstdplant" scope="col"><spring:message code="plantno"/></th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="5">
									&nbsp;
								</td>
							</tr>
						</tfoot>
						<tbody>
							
						</tbody>
					</table>
					<!-- end of table created for additional procedure standards -->
					
					</c:otherwise>	
				</c:choose>					
				
				<div class="text-center marg-top">
					<input type="submit" value="<spring:message code="multistart.startselcal"/>" />
				</div>
				
				
			</div>
		
			</form:form>
    </jsp:body>
</t:crocodileTemplate>