<%-- File name: /trescal/core/jobs/jobitem/calloffitems.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code='calloffitems.headtext'/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/jobs/jobitem/CallOffItems.js'></script>
    </jsp:attribute>
    <jsp:body>
        <div id="calloffitems" class="infobox">
			<t:showErrors path="form.*" showFieldErrors="true"/>
			<c:choose>
				<c:when test="${not empty itemsMap}">
					<p>
						<spring:message code="calloffitems.companycalloffcount" arguments="${itemsCount}, ${companyCount}" />
					</p>
				</c:when>
				<c:otherwise>
					<p><spring:message code='calloffitems.noitems' /></p>
				</c:otherwise>
			</c:choose>
			<c:forEach var="coid" items="${itemsMap.keySet()}">
				<form:form modelAttribute="form">
					<!--  Needed for validator -->
					<form:hidden path="allocatedSubdivId" value="${allocatedSubdiv.key}" />
					<fieldset>
						<c:set var ="alreadySet" value="true" />
						<c:forEach var="subdiv" items="${itemsMap.get(coid).keySet()}">
							<c:if test="${alreadySet}">
								<c:set var="coname" value="${itemsMap.get(coid).get(subdiv).toArray()[0].coname}" />
								<c:set var="alreadySet" value="false"/>
							</c:if>
						</c:forEach>
						<ol>
			             	<li>
								<label class="bold"><spring:message code="company" />: </label>
								<links:companyLinkInfo companyId="${coid}" onStop="${companyOnStop.get(coid)}" rowcount="0" companyName="${coname}" active="${companyActive.get(coid)}" copy="true" />
								<form:hidden path="coid" value="${coid}" />
								<input type="button" value="<spring:message code='viewjob.updateitems'/>" onClick="" class="hid" />
							</li>
							<li>
								<label class="bold"><spring:message code="actiontype.action" />:</label>
								<div class="float-left">
									<form:radiobuttons path="action" items="${actions}" itemLabel="name" element="div" cssClass="float-left"/>
								</div>
								<form:errors path="action" delimiter="; " cssClass="float-left; error"/>
								<div class="clear"></div>
							</li>
							<li> 
								<c:set var="subdivMap" value="${itemsMap.get(coid)}"/>
								<c:forEach var="subdivid" items="${subdivMap.keySet()}">
																
									<table class="default2 calloffitems">
										<thead>                                                                       
											<tr>
												<th colspan="8" ><links:subdivLinkInfo rowcount="0"
													subdivId="${subdivid}"
													subdivName="${itemsMap.get(coid).get(subdivid).toArray()[0].subname}"
													companyActive="${companyActive.get(coid)}"
													companyOnStop="${companyOnStop.get(coid)}"
													subdivActive="${itemsMap.get(coid).get(subdivid).toArray()[0].subActive}" />
												</th>
											</tr>
											<tr>
												<th class="select" scope="col"><input type="checkbox" onclick=" toggleAll(${subdivid}, this.checked); "></th>
												<th class="barcode" scope="col" style="text-align: center" ><spring:message code="barcode" /></th>
												<th class="instrument" scope="col" style="text-align: center"><spring:message code="instmodelname" /></th>
												<th class="job" scope="col" style="text-align: center" ><spring:message code="jobno" /></th>
												<th class="jobitem" scope="col" style="text-align: center" ><spring:message code="vm_global.jobitem" /></th>
												<th class="currentloc" scope="col" style="text-align: center" ><spring:message code="vm_global.currentlocation" /></th>
												<th class="calledoffby" scope="col"style="text-align: center" ><spring:message code="calloffitems.calledoffby" /></th>
												<th class="calledoffon" scope="col" style="text-align: center" ><spring:message code="calloffitems.calledoffon" /></th>
											</tr>
										</thead>
										<tbody>
											<c:forEach var="callOffItem" items="${subdivMap.get(subdivid)}" varStatus="itemStatus">
												<tr>
													<td rowspan="2" style="text-align: center"><form:checkbox path="itemIds" cssClass="checkbox_${subdivid}" value="${callOffItem.calloffId}"/></td>
													<td style="text-align: center">
														<cwms:securedLink permission="INSTRUMENT_VIEW" classAttr="mainlink" parameter="?plantid=${callOffItem.plantId}">
																${callOffItem.plantId}
														</cwms:securedLink>
													</td>
													<td style="text-align: center">${callOffItem.instModel}</td>
													<td style="text-align: center">
														<links:jobnoLinkDWRInfo jobno="${callOffItem.jobno}" jobid="${callOffItem.jobId}" copy="true" rowcount="${itemStatus.count}"/>
													</td>
													<td style="text-align: center">
														<c:choose>
															<c:when test="${callOffItem.contractReviewItemExist}">
																<links:jobitemLinkInfo defaultPage="jiactions.htm" jobItemNo="${callOffItem.itemNo}" jobItemCount="${callOffItem.itemsCount}" jobItemId="${callOffItem.jobItemId}"/>
															</c:when>
															<c:otherwise>
																<links:jobitemLinkInfo defaultPage="jicontractreview.htm" jobItemNo="${callOffItem.itemNo}" jobItemCount="${callOffItem.itemsCount}" jobItemId="${callOffItem.jobItemId}"/>
															</c:otherwise>
														</c:choose>
													</td>
													<td style="text-align: center">
														<c:if test="${ not empty callOffItem.town }" >
															<c:out value="${callOffItem.companyCode}"/>
															<c:out value=" - "/>
															<c:out value="${callOffItem.subdivCode}"/>
															<c:out value=" - "/>
															<c:out value="${callOffItem.town}"/>
														</c:if>
													</td>
													<td style="text-align: center"><c:out value="${callOffItem.calledOffByFirstName} ${callOffItem.calledOffByLastName}"/></td>
													<td style="text-align: center"><fmt:formatDate type="both" dateStyle="SHORT" timeStyle="SHORT" value="${callOffItem.offDate}"/></td>
												</tr>
												<tr>
													<td colspan="7" style="text-align: center"><c:out value="${callOffItem.reason}" /></td>
												</tr>
											</c:forEach>
										</tbody>
									</table>						
								</c:forEach>
							</li>
							<li>
								<input type="submit" value="<spring:message code='addjobviawebservice.submit'/>"/>
							</li>
						</ol>
					</fieldset>
				</form:form>
			</c:forEach>
		</div>
    </jsp:body>
</t:crocodileTemplate>