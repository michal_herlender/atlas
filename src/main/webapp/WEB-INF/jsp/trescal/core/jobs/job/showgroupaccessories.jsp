<%-- File name: /trescal/core/jobs/job/showgroupaccessories.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs"%>

<table class="default4 groupOptions">
	<tbody>
		<tr>
			<td class="groupOptText"><spring:message code="showitemaccessories.deletegroup"/>: </td>
			<td class="groupOptVal">
				<a href="" class="mainlink" onclick="event.preventDefault(); deleteGroup(${group.id}, ${group.job.jobid});">
					<img src="img/icons/delete.png" width="16" height="16" alt="<spring:message code='showitemaccessories.deletegroup'/>" title="<spring:message code='showitemaccessories.deletegroup'/>" class="img_marg_bot"/>
				</a>
			</td>
		</tr>
	</tbody>
</table>
<table class="default4 groupOptions">
	<tbody>
		<tr>
			<td class="groupOptText"><spring:message code="showitemaccessories.calgroup"/>: </td>
			<td class="groupOptVal">
				<input type="checkbox" id="calGroup" <c:if test="${group.calibrationGroup}">checked</c:if> onclick="changeGroupField();" />
			</td>
		</tr>
		<tr>
			<td class="groupOptText"><spring:message code="showitemaccessories.delgroup"/>: </td>
			<td class="groupOptVal">
				<input type="checkbox" id="delGroup" <c:if test="${group.deliveryGroup}">checked</c:if> onclick="changeGroupField();" />
			</td>
		</tr>
	</tbody>
</table>
<table class="default4 savedAccessoryList">
	<thead>
		<tr>
			<td colspan="3"><spring:message code="showitemaccessories.savedaccessories"/></td>
		</tr>
	</thead>
	<tbody>
		<c:choose>
			<c:when test="${group.accessories.size() == 0}">
				<tr>
					<td colspan="3"><spring:message code="showitemaccessories.noaccessories"/></td>
				</tr>
			</c:when>
			<c:otherwise>
				<c:forEach var="groupAccessory" items="${group.accessories}">
					<jobs:groupAccessory groupAccessory="${groupAccessory}"/>
				</c:forEach>
			</c:otherwise>
		</c:choose>
	</tbody>
</table>