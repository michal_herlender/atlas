<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs"%>
<%@ taglib prefix="addr" tagdir="/WEB-INF/tags/address"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image"%>

<jsp:useBean id="now" class="java.util.Date" />
<jobs:jobItemTemplate>
	<jsp:attribute name="subScript">
		<script type="module" src="${pageContext.request.contextPath}/script/components/cwms-datetime/cwms-datetime.js"></script>
		<script src='script/trescal/core/jobs/generalserviceoperation/GeneralServiceOperation.js'></script>
		<script>
			//grab scroll to parameter passed when using job item key navigation
			var scrollto = '${request.getParameter("scrollto")}';
			var startedOn = '${form.startedOn}';
			var completedOn = '${form.completedOn}';
		</script>
	</jsp:attribute>
	<jsp:attribute name="warnings">
		<!-- error section displayed when form submission fails -->
		<spring:bind path="form.*">
			<c:if test="${status.error}">
				<c:set var="formHasError" value="${true}"/>
				<div class="warningBox1">
					<div class="warningBox2">
						<div class="warningBox3">
							<h5 class="center-0 attention"><spring:message code="generalserviceoperation.errors"/>:</h5>
							<c:forEach var="error" items="${status.errorMessages}">
								<div class="center attention">${error}</div>
							</c:forEach>
						</div>
					</div>
				</div>
			</c:if>
		</spring:bind>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox">
			    <span id="jobItemId" class="hid">${form.ji.jobItemId}</span>
					<form:form modelAttribute="form" id="completegeneralserviceoperationform" method="post" action="" enctype="multipart/form-data" >
						<fieldset id="completegeneralserviceoperation">
							<legend><spring:message code="generalserviceoperation.completegso"/></legend>
							<form:hidden path="gso.id"/>
							<form:hidden path="actionPerformed" />
							<ol>
								<li>
									<label><spring:message code="newcalibration.items"/>:</label>
									<div class="float-left">
										<c:forEach items="${ form.gso.links }" var="link">												
										Item <links:jobitemLinkInfo jobItemNo="${link.ji.itemNo}" jobItemId="${link.ji.jobItemId}" defaultPage="" /> on Job ${link.ji.job.jobno}<br/>
										</c:forEach>
									</div>
								</li>
								<li>
									<form:label path="startedOn"><spring:message code="generalserviceoperation.startedon"/>:</form:label>
									<fmt:formatDate value="${form.startedOn}" type="both" dateStyle="SHORT" timeStyle="SHORT"/>
									&nbsp;(${form.startedByName})
								</li>
								<li>
									<label for="serviceType"><spring:message code="servicetype"/>:</label>
									<cwms:besttranslation translations='${form.gso.calType.serviceType.shortnameTranslation}'/> - 
									<cwms:besttranslation translations='${form.gso.calType.serviceType.longnameTranslation}'/>
								</li>
								
								<li>
									<form:label path="duration"><spring:message code="generalserviceoperation.interval"/>:</form:label>
									<form:input path="duration" id="durationCWMS"/>
									<form:select path="intervalUnitId" items="${units}" itemLabel="value" itemValue="key"/>
									<form:errors path="duration"/>
								</li>
								
								<li>
									<form:label path="completedOn"><spring:message code="generalserviceoperation.completedon"/>:</form:label>
									<form:input type="datetime-local" path="completedOn"
									step="60" max="${cwms:isoDatetime(now)}"/>
									<span class="attention"><form:errors path="completedOn"/></span>
								</li>
								
								<li>
									<form:label path="remark"><spring:message code="jiactions.remark"/>:</form:label>
									<form:textarea path="remark" rows="6" class="width60"/> 
									<span class="attention"><form:errors path="remark"/></span>
								</li>
								
								<li style=" border-bottom: 0; ">
									<label>&nbsp;</label>
									<div class="float-left padtop" style=" width: 400px; ">&nbsp;<b><spring:message code="generalserviceoperation.outcome"/></b></div>
									<div class="float-left padtop" style=" width: 140px; text-align: center; "><b><spring:message code="completecalibration.timetake"/></b></div>
									<div class="clear"></div>
								</li>
								<c:forEach var="key" items="${form.linkedOutcomes.keySet()}">
									<li>
										<label>
											<c:forEach var="gsoLink" items="${form.gso.links}">
												<c:if test="${gsoLink.id == key}">
													<spring:message code="item"/> ${gsoLink.ji.itemNo}:
												</c:if>
											</c:forEach>
										</label>
										<div class="float-left" style="width: 400px;">									
											<select name="linkedOutcomes[${key}]" onchange="changeOutcome(this); return false;">
												
											<c:forEach items="${ outcomes }" var="ao">
											<option value="${ao.id}" class="${ao.genericValue}">
												<cwms:besttranslation translations='${ao.translations}'/>
											</option>
										</c:forEach>

											</select>
										</div>
										<div class="float-left" style=" width: 140px; text-align: center; ">
											<form:input path="linkedTimesSpent[${key}]" size="3" value="5"/>
											<span class="attention"><form:errors path="linkedTimesSpent[${key}]"/></span>
										</div>
										<div class="clear"></div>													
									</li>
								</c:forEach>
								
								<li class="show" id="clientDecisionRequired">
									<form:label path="clientDecisionNeeded"><spring:message code="generalserviceoperation.clientdecisionrequired"/>:</form:label>
									<form:radiobutton path="clientDecisionNeeded" value="true"/> 
									<spring:message code="yes"/>
									&nbsp;&nbsp;
									<form:radiobutton path="clientDecisionNeeded" value="false"/> 
									<spring:message code="no"/>
									<span class="attention"><form:errors path="clientDecisionNeeded"/></span>
								</li>
						<!-- submit button div -->	
						<li>
									<label>&nbsp;</label><br/>
									<span>
										<input type="submit" onclick="$j('#submitAction').val($j(this).attr('id'));" value="<spring:message code='generalserviceoperation.action.complete'/>" id="complete" />
										<input type="submit" onclick="$j('#submitAction').val($j(this).attr('id'));" value="<spring:message code='generalserviceoperation.action.cancel'/>" id="cancel" />
										<form:hidden path="submitAction" id="submitAction"/>
									</span>
								</li>
						<!-- end of submit button div -->
						</ol>
						</fieldset>
					</form:form>
		</div>
		<!-- end of infobox div -->
	</jsp:body>
</jobs:jobItemTemplate>