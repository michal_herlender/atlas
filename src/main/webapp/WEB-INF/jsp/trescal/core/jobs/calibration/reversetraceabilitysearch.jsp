<%-- File name: /trescal/core/jobs/calibration/reversetraceabilitysearch.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="reversetraceabilitysearch.headtext"/></span>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox">
			<form:form method="post" action="" modelAttribute="form">
				<form:errors path="*">
       				<div class="warningBox1">
              		  <div class="warningBox2">
              	       		<div class="warningBox3">
                    			<h5 class="center-0 attention"><spring:message code="error.save"/>:</h5>
                    		</div>
                   		</div>
                  	</div>
                </form:errors>
				<fieldset>
						<legend><spring:message code="reversetraceabilitysearch.calibrations.performed"/></legend>
						<div>

							<ol>	
								<li>
									<label><spring:message code="barcode"/>*:</label>
									<form:input path="plantId" tabindex="2" size="22"/>
									<form:errors path="plantId" class="attention"/>
								</li>									
								<li>
									<form:label path="startCalDate"><spring:message code="jicalibration.startdatetime"/>:</form:label>
									<form:input type ="date" path="startCalDate" tabindex="2" />
									<form:errors path="startCalDate" class="attention"/>
								</li>	
								<li>
									<form:label path="endCalDate"><spring:message code="bpo.enddate"/>:</form:label>
									<form:input type="date" path="endCalDate" tabindex="2" />
								</li>	
								<li>
									<label>&nbsp;</label>
									<span>
										<input type="submit" name="search" value="Search" tabindex="6" />
									</span>
								</li>								
							</ol>
						
						</div>
				</fieldset>
			</form:form>
		</div>						
	</jsp:body>
</t:crocodileTemplate>