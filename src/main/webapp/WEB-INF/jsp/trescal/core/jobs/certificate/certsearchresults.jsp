<%-- File name: /trescal/core/jobs/certificate/certsearchresults.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="certsearchresults.headtext"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/jobs/certificate/CertSearchResults.js'></script>
	</jsp:attribute>
	<jsp:body>
		<!-- infobox contains all search form elements and is styled with nifty corners -->
		<div class="infobox" id="certsearchresults">
			<c:set var="results" value="${certsearchform.rs.results}"/>
			<form:form modelAttribute="certsearchform" id="searchform" method="post">
				<form:hidden path="resultsPerPage"/>
				<form:hidden path="pageNo"/>
				<form:hidden path="certNo"/>
				<form:hidden path="status"/>
				<form:hidden path="calTypeId"/>
				<form:hidden path="mfr"/>
				<form:hidden path="mfrid"/>
				<form:hidden path="model"/>
				<form:hidden path="modelid"/>
				<form:hidden path="desc"/>
				<form:hidden path="descid"/>
				<form:hidden path="issueDate1"/>
				<form:hidden path="issueDate2"/>
				<form:hidden path="calDate1"/>
				<form:hidden path="calDate2"/>
				<form:hidden path="issueDateBetween"/>
				<form:hidden path="calDateBetween"/>
				<form:hidden path="coid"/>
				<form:hidden path="personid"/>
				<form:hidden path="tpId"/>
				<form:hidden path="tpCertNo"/>
				<form:hidden path="bc"/>
				<form:hidden path="jobNo"/>
				<form:hidden path="serialNo"/>
				<form:hidden path="plantNo"/>
				<form:hidden path="jobItemDateComplete1"/>
				<form:hidden path="jobItemDateComplete2"/>
				<form:hidden path="jobItemCompletionDateBetween"/>
				<t:showResultsPagination rs="${certsearchform.rs}" pageNoId=""/>
				<table class="default4" summary="This table lists all certificate search results">
					<thead>
						<tr>
							<td colspan="10"><spring:message code="certsearchresults.matchcert"/> (${results.size()})</td>
						</tr>
						<tr>
							<th class="certno" scope="col"><spring:message code="certsearchresults.certno"/></th>
							<th class="caldate" scope="col"><spring:message code="certsearchresults.caldate"/></th>
							<th class="issuedate" scope="col"><spring:message code="certsearch.issdate"/></th>
							<th class="calpro" scope="col"><spring:message code="certsearchresults.process"/></th>
							<th class="duration" scope="col"><spring:message code="certsearchresults.duration"/></th>
							<th class="caltype" scope="col"><spring:message code="certmanagement.caltype"/></th>
							<th class="regby" scope="col"><spring:message code="certsearchresults.registerby"/></th>
							<th class="status" scope="col"><spring:message code="certsearch.status"/></th>
							<th class="type" scope="col"><spring:message code="certsearchresults.certtype"/></th>
							<th class="file" scope="col"><spring:message code="certsearchresults.file"/></th>
						</tr>
					</thead>
				</table>
				<c:set var="rowcount" value="0"/>
				<c:choose>
					<c:when test="${result.size() == 0}">
						<table class="default4" summary="<spring:message code='certsearchresults.string1'/>">
							<tbody>
								<tr>
									<td colspan="10" class="bold center">
										<spring:message code="certsearchresults.string2"/>
									</td>
								</tr>
							</tbody>
						</table>
					</c:when>
					<c:otherwise>
						<c:forEach var="certWrapper" items="${results}">
							<c:set var="cert" value="${certWrapper.cert}"/>
							<table class="default4" summary="<spring:message code='certsearchresults.string3'/>">
								<tbody>
									<tr>
										<td class="certno">
											<a href="viewcertificate.htm?certid=${cert.certid}" class="mainlink">${cert.certno}</a>
										</td>
										<td class="caldate"><fmt:formatDate value="${cert.calDate}" dateStyle="medium" type="date" /></td>
										<td class="issuedate"><fmt:formatDate value="${cert.certDate}" dateStyle="medium" type="date" /></td>
										<td class="calpro">${cert.cal.calProcess.process}</td>
										<td class="duration">${cert.duration}</td>
										<td class="caltype"><cwms:besttranslation translations="${cert.calType.serviceType.shortnameTranslation}"/></td>
										<td class="regby">${cert.registeredBy.name}</td>														
										<td class="status">${cert.status.message}</td>
										<td class="type">
											<c:choose>
												<c:when test="${cert.type == 'CERT_INHOUSE'}">
													<spring:message code="certsearchresults.inhouse"/>
												</c:when>
												<c:when test="${cert.type == 'CERT_THIRDPARTY'}">
													<spring:message code="certsearchresults.thirdparty"/>
												</c:when>
												<c:when test="${cert.type == 'CERT_CLIENT'}">
													<spring:message code="client"/>
												</c:when>
												<c:otherwise>
													${cert.type}
												</c:otherwise>
											</c:choose>
										</td>
										<td class="file center">
											<c:if test="${cert.hasCertFile}">
												<c:url var="downloadURL" value="downloadfile.htm"><c:param name="file" value="${cert.encryptedCertFilePath}" /></c:url>
												<a href="${downloadURL}">
													<img src="img/doctypes/pdf.gif" width="16" height="16" title="Open Certificate PDF" alt="Open Certificate PDF" />
												</a>
											</c:if>
										</td>
									</tr>
									<tr>
										<td colspan="2" class="bold"><spring:message code="company"/>:</td>
										<td colspan="4">
											<c:choose>
												<c:when test="${cert.links.size() > 0}">
													${cert.links.iterator().next().jobItem.inst.comp.coname}
												</c:when>
												<c:when test="${cert.instCertLinks.size() > 0}">
													${cert.instCertLinks.iterator().next().inst.comp.coname}
												</c:when>
											</c:choose>
										</td>
										<td class="bold"><spring:message code="certsearch.jobno"/> / <spring:message code="certmanagement.itemno"/>:</td>
										<td colspan="3">
											<c:choose>
												<c:when test="${cert.links.size() > 0}">
													<c:forEach var="cl" items="${cert.links}">
														<a class="mainlink" href="viewjob.htm?jobid=${cl.jobItem.job.jobid}">
															${cl.jobItem.job.jobno}
														</a>
														-
														<a class="mainlink" href="jicertificates.htm?jobitemid=${cl.jobItem.jobItemId}">
															<spring:message code="certmanagement.item"/>&nbsp;${cl.jobItem.itemNo}
														</a>
														<br/>
													</c:forEach>
												</c:when>
												<c:when test="${cert.instCertLinks.size() > 0}">
													<c:forEach var="cl" items="${cert.instCertLinks}">
														<a class="mainlink" href="viewinstrument.htm?loadtab=certs-tab&plantid=${cl.inst.plantid}">
															${cl.inst.plantid}
														</a>
														<br/>
													</c:forEach>
												</c:when>
											</c:choose>
										</td>
									</tr>
								</tbody>
							</table>
						</c:forEach>
					</c:otherwise>
				</c:choose>
				<t:showResultsPagination rs="${certsearchform.rs}" pageNoId=""/>
			</form:form>
		</div>
		<!-- end of infobox div -->
	</jsp:body>
</t:crocodileTemplate>