<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>

<script src='script/trescal/core/jobs/jobitem/SelectWorkRequirement.js'></script>

<t:overlayTemplate bodywidth="780">
	<div id="selectWorkRequirement">
		<form:form method="POST" id="selectWorkRequirementForm">
			<input type="hidden" id="jobitemid" value="${ jobitemid }" />
			<input type="hidden" id="certificateid" value="${ certificateid }" />
			<fieldset>
				<ol>
					<li>
						<label><spring:message code="addresstype.certificate" />:</label> 
						<span>${ certificateno } </span>
						<div class="clear-0"></div>
					</li>
				</ol>
			</fieldset>
			<fieldset>
				<legend>
					<span id="accHeader">
						Workrequirement :
					</span>
				</legend>
				<ol>				
					<li>
						<table class="default2">
							<tr>
								<th></th>
								<th><spring:message code="capability"/></th>
								<th><spring:message code="abbreviation.servicetype"/></th>
								<th><spring:message code="wrtype"/></th>
								<th>Lab</th>
								<th><spring:message code="instmod.lastmodified"/></th>
							</tr>

							

							<c:forEach items="${workrequirements}" var="wr"
								varStatus="status">
								<tr>
									<td><input type="radio" name="wrid" value="${ wr.id }"/></td>
									<td>
										<c:choose>
											<c:when test="${not empty wr.capability}">
												<links:procedureLinkDWRInfo displayName="true" capability="${wr.capability}" rowcount="${wr.id}"/>
											</c:when>
											<c:otherwise>
												&nbsp;
											</c:otherwise>
										</c:choose>		
									</td>
									<td>
										<c:choose>
											<c:when test="${wr.serviceType == null}">
												&nbsp;
											</c:when>
											<c:otherwise>
												<cwms:besttranslation translations="${wr.serviceType.shortnameTranslation}"/>
											</c:otherwise>
										</c:choose>
									</td>
									<td>
										<c:choose>
											<c:when test="${wr.type == null}">
												&nbsp;
											</c:when>
											<c:otherwise>
												${wr.type.shortname}
											</c:otherwise>
										</c:choose>
									</td>
									<td>
										<c:choose>
											<c:when test="${wr.department == null}">
												&nbsp;
											</c:when>
											<c:otherwise>
												${wr.department.shortName}
											</c:otherwise>
										</c:choose>
									</td>
									<td>
										<fmt:formatDate value="${wr.lastModified}" type="both" dateStyle="SHORT" timeStyle="SHORT" />
									</td>
								</tr>
							</c:forEach>

						</table>
					</li>
					<li class="center">
						<button onclick="saveSeletedWR(); return false;">
							<spring:message code='save' />
						</button>
						 &nbsp; 
						 <button onclick="event.preventDefault(); window.parent.loadScript.closeOverlay();">
							<spring:message code='cancel' />
						 </button>
					</li>
				</ol>
			</fieldset>
		</form:form>
	</div>
</t:overlayTemplate>