<%-- File name: /trescal/core/jobs/job/expenseitemrow.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<table id="expenselist" class="tablesorter">
	<thead>
		<tr>
			<th id="expenseQuantityRow" colspan="11">
				<spring:message code="viewjob.jobservices"/> (${job.expenseItems.size()})
			</th>
		</tr>
		<tr>
			<th id="expenseItemNoCol" scope="col"><spring:message code="item"/></th>
			<th id="expenseDateCol" scope="col"><spring:message code="date"/></th>
			<th id="expenseModelCol" scope="col"><spring:message code="viewjob.service"/></th>
			<th id="expenseTypeCol" scope="col" <c:if test="${expenseServiceTypes.size() <= 1}">class="hid"</c:if>>
				<spring:message code="type"/>
			</th>
			<th id="expenseCommentCol" scope="col"><spring:message code="comment"/></th>
			<th id="expenseClientRefCol" scope="col"><spring:message code="clientref"/></th>
			<th id="expenseSinglePriceCol" scope="col"><spring:message code="singleprice"/></th>
			<th id="expenseQuantityCol" scope="col"><spring:message code="quantity"/></th>
			<th id="expenseTotalPriceCol" scope="col"><spring:message code="totalcost"/></th>
			<th class="" id="expenseInvoiceableCol" scope="col"><spring:message code="viewjob.invoiceable"/></th>
			<th class="expenseEditCol" scope="col"><spring:message code="system.addedit"/></th>
			<th class="expenseDeleteCol" scope="col"><spring:message code="delete"/></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="expenseItem" items="${job.expenseItems}">
			<c:set var="invoiced" value="${!expenseItem.invoiceItems.isEmpty()}"/>
			<tr class="expenseItemRow">
				<td>${expenseItem.itemNo}<input class="expenseItemId" type="hidden" value="${expenseItem.id}"/></td>
				<td><input class="expenseItemDate" value="${expenseItem.date}" type="date" lang="${rc.locale}" min="1900-01-01" readonly/></td>
				<td>
					<input class="expenseItemModelInput" value="<cwms:besttranslation translations='${expenseItem.model.nameTranslations}'/>"/>
					<select class="expenseItemModelSelect hid">
						<c:forEach var="model" items="${expenseModels}">
							<option value="${model.id}"
								<c:if test="${expenseItem.model.modelid == model.id}">selected</c:if>>
								${model.name}
							</option>
						</c:forEach>
					</select>
				</td>
				<td <c:if test="${expenseServiceTypes.size() <= 1}">class="hid"</c:if>>
					<input class="expenseItemTypeInput" value="<cwms:besttranslation translations='${expenseItem.serviceType.shortnameTranslation}'/>"/>
					<select class="expenseItemTypeSelect hid">
						<c:forEach var="serviceType" items="${expenseServiceTypes}">
							<option value="${serviceType.serviceTypeId}"
								<c:if test="${expenseItem.serviceType.serviceTypeId == serviceType.serviceTypeId}">selected</c:if>>
								<cwms:besttranslation translations='${serviceType.shortnameTranslation}'/>
							</option>
						</c:forEach>
					</select>
				</td>
				<td><input class="expenseItemComment" value="${expenseItem.comment}" readonly/></td>
				<td><input class="expenseItemClientRef" value="${expenseItem.clientRef}" readonly/></td>
				<td>
					<span class="expenseItemPriceFmt">
						<fmt:formatNumber value="${expenseItem.singlePrice}" type="currency" currencySymbol="${job.currency.currencySymbol}" currencyCode="${job.currency.currencyCode}"/>
					</span>
					<input class="expenseItemPrice hid" value="${expenseItem.singlePrice}" type="number" step="0.01"/>
				</td>
				<td><input class="expenseItemQuantity" type="number" value="${expenseItem.quantity}" min="1" readonly/></td>
				<td>
					<span class="expenseItemPriceFmt expenseItemTotalPrice">
						<fmt:formatNumber value="${expenseItem.totalPrice}" type="currency" currencySymbol="${job.currency.currencySymbol}" currencyCode="${job.currency.currencyCode}"/>
					</span>
				</td>
				<td>
					<c:choose>
						<c:when test="${invoiced}">
							<c:set var="invoice" value="${expenseItem.invoiceItems.iterator().next().invoice}"/>
							<spring:message code="viewjob.oninvoice" arguments="<a href='viewinvoice.htm?id=${invoice.id}' class='mainlink'>${invoice.invno}</a>"/>
						</c:when>
						<c:otherwise>
							<input class="expenseItemInvoiceable" type="checkbox" <c:if test='${expenseItem.invoiceable}'>checked</c:if> disabled/>
						</c:otherwise>
					</c:choose>
				</td>
				<td class="expenseEditCol">
					<c:if test="${!invoiced}">
						<img src="img/icons/note_edit.png" width="16" height="16"
							alt='<spring:message code="edit"/>' title='<spring:message code="edit"/>'
							onclick="event.preventDefault(); editExpenseItem($j(this), ${job.jobid});"/>
					</c:if>
				</td>
				<td class="expenseDeleteCol">
					<c:if test="${!invoiced}">
						<img src="img/icons/delete.png" width="16" height="16"
							alt='<spring:message code="viewjob.imgdelete"/>' title='<spring:message code="viewjob.imgdelete"/>'
							onclick="deleteExpenseItem(${expenseItem.id}, ${job.jobid});"/>
					</c:if>
				</td>
			</tr>
		</c:forEach>
		<tr id="newExpenseItemRow">
			<td></td>
			<td><input type="date" id="newExpenseItemDate" type="date" locale="${rc.locale}" min="1900-01-01"/></td>
			<td><select id="newExpenseModel" onchange="setExpenseDefaultPrice();"></select></td>
			<td <c:if test="${expenseServiceTypes.size() <= 1}">class="hid"</c:if>>
				<select id="newExpenseServiceType">
					<c:forEach var="serviceType" items="${expenseServiceTypes}" varStatus="stLoop">
						<option value="${serviceType.serviceTypeId}" <c:if test="${stLoop.first}">selected</c:if>><cwms:besttranslation translations="${serviceType.shortnameTranslation}"/></option>
					</c:forEach>
				</select>
			</td>
			<td><input id="newExpenseComment"/></td>
			<td><input id="newExpenseClientRef"/></td>
			<td><input id="newExpensePrice" type="number" min="0" step="0.01"/></td>
			<td><input id="newExpenseQuantity" type="number" min="0" step="1"/></td>
			<td></td>
			<td class=""><input id="newExpenseInvoiceable" type="checkbox" value="${expenseItem.invoiceable}" checked/></td>
			<td class="expenseEditCol">
				<img src="img/icons/add.png" width="16" height="16"
					alt='<spring:message code="add"/>' title='<spring:message code="add"/>'
					onclick="addExpenseItem(${job.jobid});"/>
			</td>
			<td></td>
		</tr>
	</tbody>
</table>
<table id="timesheetentries" class="tablesorter">
	<thead>
		<tr>
			<td id="timeSheetQuantityRow" colspan="7">
				<spring:message code="menu.timesheet" /> (${job.timeSheetEntries.size()})
			</td>
		</tr>
		<tr>
			<th><spring:message code="timesheetvalidation.employee" /></th>
			<th><spring:message code="timesheetvalidation.start" /></th>
			<th><spring:message code="jicertificates.duration" /></th>
			<th><spring:message code="jiactions.activity" /></th>
			<th><spring:message code="email.comment" /></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="entry" items="${job.timeSheetEntries}">
			<tr class="timeSheetEntryRow">
				<td>${entry.employee.name}</td>
				<td><fmt:formatDate value="${entry.start}" dateStyle="medium" type="both" timeStyle="short" /></td>
				<td><cwms:formatDuration value="${entry.duration}"/></td>
				<td><spring:message code="${entry.timeActivity.messageCode}"/></td>
				<td>${entry.comment}</td>
			</tr>
		</c:forEach>
	</tbody>
</table>