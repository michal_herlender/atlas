<%-- File name: /trescal/core/jobs/job/jobsearch.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript'
			src='script/trescal/core/jobs/calibration/ImportOperations.js'></script>
	</jsp:attribute>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message
				code="importcalibrations.title" /></span>
	</jsp:attribute>
	<jsp:body>
		<t:showErrors path="form.*" showFieldErrors="true"
			showFieldName="false" />

		<c:if test="${not empty job }">
			<div class="successBox1">
				<div class="successBox2">
					<div class="successBox3 center-0">
						<spring:message code="calibrationimportation.successful" /> 
						&nbsp;<a href="viewjob.htm?jobid=${job.jobid }">${job.jobno }</a>	
					</div>
				</div>
			</div>
		</c:if>
		
		<div class="infobox">
			<div>
				<cwms:securedLink permission="IMPORT_OPERATIONS_BATCH"  classAttr="mainlink" >
					<spring:message code='menu.jobs.importoperationsbatch'/>
				</cwms:securedLink>
			</div>
			<form:form modelAttribute="form" enctype="multipart/form-data">
				<fieldset>
					<legend>
						<spring:message code="importcalibrations.title" />
					</legend>
					<!-- displays content in left column -->
					<div class="displaycolumn-40">
						<ol>											
							<li>
								<label><spring:message code="subdiv" />:</label>
								<div id="cascadeSearchPlugin">
									<input type="hidden" id="cascadeRules" value="subdiv" />
									<input type="hidden" id="compCoroles" value="client" />
									<input type="hidden" id="subdivCallBackFunction"
										value="getExchangeFormatForSelectedCompany" />
								</div>
								<span class="attention" style="margin-left: 50px;"><form:errors
										path="subdivid" /></span>
								<!-- clear floats and restore page flow -->
								<div class="clear"></div>
							</li>
							<li>
								<label><spring:message
										code="addjob.recordtype.file.exchangeformat" /> :</label>
								<select id="exchangeformat" name="exchangeFormatId"></select>
							</li>
							<li>
								<label><spring:message code="file" />:</label>	
								<form:input path="file" type="file"
									accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />									
								<span class="attention"><form:errors path="file" /></span>
								<!-- clear floats and restore page flow -->
								<div class="clear"></div>
							</li>
							
							<li>
								<label>&nbsp;</label>
								<input type="submit" value="<spring:message code='submit' />" />
							</li>
						</ol>
					</div>
				</fieldset>
			</form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>