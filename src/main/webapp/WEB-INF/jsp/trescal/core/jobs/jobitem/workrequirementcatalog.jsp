<%-- File name: /trescal/core/jobs/jobitem/workrequirementoverlay.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="instrument" tagdir="/WEB-INF/tags/instrument" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="company" tagdir="/WEB-INF/tags/company" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:overlayTemplate bodywidth="960">
	<jsp:attribute name="scriptPart">
		<script src="script/trescal/core/jobs/jobitem/WorkRequirementCatalog.js"></script>
	</jsp:attribute>
	<jsp:body>
		<t:showErrors path="form.*" showFieldErrors="true"></t:showErrors>
		<form:form modelAttribute="form" method="post" >
			<form:hidden id="action" path="action" />
			<form:hidden path="jobItemId" />
			<div id="workRequirementCatalogSearch">
				<fieldset>
					<ol>								
						<li>
							<c:choose>
								<c:when test="${selectSubdivision}">
									<c:set var="classSubdivDisplay" value="hid"/>
									<c:set var="classSubdivSelect" value="vis"/>
								</c:when>
								<c:otherwise>
									<c:set var="classSubdivDisplay" value="vis"/>
									<c:set var="classSubdivSelect" value="hid"/>
								</c:otherwise>
							</c:choose>
							<label class="width30">
								<spring:message code="subdivision"/>:
								<span id="spanSubdivChangeLink" class="${classSubdivDisplay}">
									<c:out value=" ( "/>
									<a class="mainlink" href="" onclick=" event.preventDefault(); showSubdivisionSelect(); " >
										<spring:message code="editinstr.change" />
									</a>
									<c:out value=" )"/>
								</span>
								<span id="spanSubdivResetLink" class="${classSubdivSelect}">
									<c:out value=" ( "/>
									<a class="mainlink" href="" onClick=" event.preventDefault(); hideSubdivisionSelect(); ">
										<spring:message code="searchcompinst.Reset" />
									</a>
									<c:out value=" )"/>
								</span>
							
							</label>
							<form:hidden path="selectSubdivision"/>
							<span id="spanSubdivDisplay" class="${classSubdivDisplay}">
								${selectedSubdiv.getComp().getCompanyRole().getMessage()}
								<c:out value=" : "/>
								<c:choose>
									<c:when test="${selectedSubdiv.comp.companyRole == 'BUSINESS'}">
										${selectedSubdiv.getComp().getCountry().getCountryCode()}
										<c:out value=" - "/>
										${selectedSubdiv.getComp().getCompanyCode()}
										<c:out value=" - "/>
										${selectedSubdiv.getSubdivCode()}
									</c:when>
									<c:otherwise>
										${selectedSubdiv.getComp().getConame()}
										<c:out value=" - "/>
										${selectedSubdiv.getSubname()}
									</c:otherwise>
								</c:choose>
							</span>
							<span id="spanSubdivSelect" class="${classSubdivSelect}">
								<div id="cascadeSearchPlugin">
									<input type="hidden" id="compCoroles" value="supplier,business" />
									<input type="hidden" id="cascadeRules" value="subdiv" />
									<input type="hidden" id="prefillIds" value="${form.coid},${form.subdivid}" />
								</div>
							</span>
							<div class="clear-0"></div>
						</li>							
						<li>
							<label class="width30"><spring:message code="instmodelname"/>:</label>
							<span><instrument:showExtendedModel instrument="${instrument}" /></span>
							<div class="clear-0"></div>
						</li>							
						<li>
							<label class="width30">
								<spring:message code="sub-family"/>:
							</label>
							<c:choose>
								<c:when test="${form.getIncludeGenericCapabilities()}">
									<c:set var="classVisibleForGeneric" value="vis"/>
									<c:set var="classVisibleForSubfamily" value="hid"/>
								</c:when>
								<c:otherwise>
									<c:set var="classVisibleForGeneric" value="hid"/>
									<c:set var="classVisibleForSubfamily" value="vis"/>
								</c:otherwise>
							</c:choose>
							
							<%-- The following is visible when restricting search to capabilities without subfamily --%>
							<span id="subfamilyGeneric" class="${classVisibleForGeneric}"><spring:message code="company.none"/></span>
							
							<%-- The following is visible when restricting search to a specific subfamily --%>
							<form:input id="subfamilyName" path="subfamilyName" class="${classVisibleForSubfamily} width300"/>

							<form:hidden id="subfamilyId" path="subfamilyId" />
							<span class="attention"><form:errors path="subfamilyId" /></span>
							<div class="clear-0"></div>
						</li>							
						<li>
							<label class="width30">
								<spring:message code="jobresults.searchcrit"/>:
							</label>
							<div class="float-left">
								<form:checkbox path="includeGenericCapabilities" onclick=" toggleGenericCapabilities(this.checked); "/>
								<span><spring:message code="viewcapability.showgenericcapabilities" /></span>
								<span class="attention"><form:errors path="includeGenericCapabilities" /></span>
								<br/>
								<form:checkbox path="includeInstrumentModel" />
								<span><spring:message code="viewcapability.showinstrumentmodel" /></span>
								<span class="attention"><form:errors path="includeGenericCapabilities" /></span>
							</div>
							
							<div class="clear-0"></div>
						</li>
						<li>
							<label class="width30"><spring:message code="servicetype"/>:</label>
							<form:select path="serviceTypeId" items="${serviceTypes}" itemLabel="value" itemValue="key" />
							<span class="attention"><form:errors path="serviceTypeId" /></span>
							<div class="clear-0"></div>							
						</li>
						<li>
							<label class="width30"><spring:message code="jobresults.searchcrit"/>:</label>

							<%-- The following is visible when restricting search to capabilities without subfamily --%>
							<span id="includeAllGeneric" class="${classVisibleForGeneric}"><spring:message code="notapplicable"/></span>

							<%-- The following is visible when restricting search to a specific subfamily --%>
							<form:checkbox id="includeAllCheckbox" path="includeAllResults" class="${classVisibleForSubfamily}" />
							<span id="includeAllSpan" class="${classVisibleForSubfamily}"><spring:message code="viewcapability.showotherservicetypes" /></span>
							
							<span class="attention"><form:errors path="includeAllResults" /></span>
							<div class="clear-0"></div>
						</li>
						<li>
							<label class="width30">&nbsp;</label>
							<spring:message var="buttonLabel" code="search" />
							<input type="submit" id="wr_search" value="${buttonLabel}" onclick=" $j('#action').attr('value','search') " />
							&nbsp;
							<input type="button" id="wr_close_1" value="<spring:message code='cancel' />" onclick="event.preventDefault(); window.parent.loadScript.closeOverlay(); " />
						</li>
					</ol>
				</fieldset>					
			</div>
			<div id="workRequirementCatalogResults">
				<table class="default2">
					<thead>
						<tr>
							<th><spring:message code="select" /></th>
							<th><spring:message code="servicetype" /></th>
							<th><spring:message code="viewcapability.capability" /></th>
							<th><spring:message code="viewcapability.bestlab" /></th>
							<th><spring:message code="subdiv" /></th>
							<th><spring:message code="country" /></th>
							<th><spring:message code="company" /></th>
							<th><spring:message code="reference" /></th>
							<th><spring:message code="source" /></th>
						</tr>
					</thead>
					<tbody>
						<c:set var="rowcount" value="0" />
						<c:forEach var="matchType" items="${matchTypes}">
							<c:if test="${not empty resultMap.get(matchType)}">
                                <tr>
                                    <th colspan="9">${matchType.getDescription()}</th>
                                </tr>
                                <c:forEach var="procedureMatch" items="${resultMap.get(matchType)}">
                                    <c:set var="rowcount" value="${rowcount + 1}"/>
                                    <c:set var="rowclass" value="${rowcount % 2 == 0 ? 'even' : 'odd'}"/>
                                    <c:choose>
                                        <c:when test="${not empty procedureMatch.bestFilter && not empty procedureMatch.bestFilter.internalComments}">
                                            <c:set var="comments"
                                                   value="${procedureMatch.bestFilter.internalComments}"/>
                                        </c:when>
                                        <c:when test="${procedureMatch.matchSource == 'GENERIC_PROCEDURE' }">
                                            <%-- No filter on generic results; display capability description to aid in usage selection --%>
											<c:set var="comments" value="${procedureMatch.capability.description}" />
										</c:when>
										<c:otherwise>
											<c:set var="comments" value="" />
										</c:otherwise>
									</c:choose>
									<tr class="${rowclass}">
										<td rowspan="${not empty comments ? 2 : 1}">
											<c:if test="${procedureMatch.indicateMatch}">
												<form:radiobutton path="procId" value="${procedureMatch.capability.id}"/>
											</c:if>
										</td>
										<td>
											<c:set var="calTypeClass" value="${procedureMatch.indicateMatch ? '' : 'strike'}"/>
											<span class="${calTypeClass}">
												<c:choose>
													<c:when test="${procedureMatch.matchSource == 'INSTRUMENTMODEL_CAPABILITY'}">
														<cwms:besttranslation translations="${procedureMatch.serviceCapability.calibrationType.serviceType.shortnameTranslation}" />
													</c:when>
													<c:otherwise>
														<cwms:besttranslation translations="${serviceType.shortnameTranslation}" />
													</c:otherwise>
												</c:choose>
											</span>
										</td>
										<td>
											<c:if test="${procedureMatch.matchSource == 'SUBFAMILY_PROCEDURE'}">
												<c:out value="${procedureMatch.filterType.getDescription()}" />
											</c:if>
										</td>
										<td>
											<c:if test="${procedureMatch.matchSource == 'INSTRUMENTMODEL_CAPABILITY'}">
												<c:if test="${not empty procedureMatch.bestFilter && procedureMatch.bestFilter.bestLab}" >
													<c:out value="*" />
												</c:if>
											</c:if>
										</td>
										<td>${procedureMatch.capability.organisation.subname}</td>
										<td>${procedureMatch.capability.organisation.comp.country.countryCode}</td>
										<td>${procedureMatch.capability.organisation.comp.companyCode}</td>
										<td><links:procedureLinkDWRInfo  capability="${procedureMatch.capability}" rowcount="0"/></td>
										<td>${procedureMatch.matchSource.getDescription()}</td>
									</tr>
									<c:if test="${not empty comments}">
										<tr class="${rowclass}">
											<td colspan="8">
												<span><c:out value="${comments}"/></span>
											</td>
										</tr>
									</c:if>
								</c:forEach>
							</c:if>
						</c:forEach>					
					</tbody>
				</table>
			</div>
			<div id="workRequirementCatalogResults">
				<fieldset>
					<ol>
						<li>
							<label class="width30"><spring:message code="workrequirement.createnew" />:</label>
							<form:input path="addCount" type="number" size="5" min="1" max="99" />
							<span class="attention"><form:errors path="addCount" /></span>
						</li>
					</ol>
					<ol>
						<li>
							<label class="width30"><spring:message code="workrequirement.text" />:</label>
							<form:textarea path="requirementText" cols="80" rows="2" />
							<span class="attention"><form:errors path="requirementText" /></span>
						</li>
					</ol>
					<ol>
						<li>
							<label class="width30">&nbsp;</label>
							<spring:message var="buttonLabel" code="create" />
							<input type="submit" id="wr_create" value="${buttonLabel}" onclick=" $j('#action').attr('value','create') " />
							&nbsp;
							<input type="button" id="wr_close_2" value="<spring:message code='cancel' />" onclick="event.preventDefault(); window.parent.loadScript.closeOverlay(); " />
						</li>
					</ol>
				</fieldset>
			</div>
		</form:form>
	</jsp:body>
</t:overlayTemplate>