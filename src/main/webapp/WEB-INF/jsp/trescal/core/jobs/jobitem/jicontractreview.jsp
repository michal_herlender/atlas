<%-- File name: /trescal/core/jobs/jobitem/jicontractreview.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="costs" tagdir="/WEB-INF/tags/costs"%>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs"%>
<%@ taglib prefix="addr" tagdir="/WEB-INF/tags/address"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jobs:jobItemTemplate>
	<jsp:attribute name="subScript">
	<link rel="stylesheet" href="styles/trescal/core/jobs/jobitem/jicontractreview.css" type="text/css">
		<script type="module" src="script/components/cwms-datetime/cwms-datetime.js"></script>
    	<script type="module" src="script/components/cwms-contractreview/index.js"></script>
		<script src='script/trescal/core/jobs/jobitem/JIContractReview.js'></script>
		<script src='script/trescal/core/documents/ImageGalleryFunctions.js'></script>
		<script>
			// grab scroll to parameter passed when using job item key navigation
			var scrollto = '${request.getParameter("scrollto")}';
		</script>
		<script type="module" src="script/components/cwms-instructions/cwms-instructions.js"></script>
	</jsp:attribute>
	<jsp:attribute name="warnings">
		<t:showErrors path="form.*" showFieldErrors="true" />
	</jsp:attribute>
	<jsp:body >
		<!-- this infobox contains all contract review options -->
		<div class="infobox">
			<form:form modelAttribute="form" id="contractreviewform" method="post">
				<!-- div to hold all the tabbed submenu -->
				<div id="tabmenu">
					<!-- tabbed submenu to change between contract review options -->
					<ul class="subnavtab">
						<li><a href="" id="contractreview-link"
							onclick="event.preventDefault(); switchMenuFocus(menuElements, 'contractreview-tab', false);"
							class="selected"><spring:message
									code="jicontractreview.contrrew" /></a></li>
						<li><a href="" id="PO-link"
							onclick="event.preventDefault(); switchMenuFocus(menuElements, 'PO-tab', false);"><spring:message
									code="jicontractreview.purchord" /></a></li>
						<li><a href="" id="revhistory-link"
							onclick="event.preventDefault(); switchMenuFocus(menuElements, 'revhistory-tab', false);"><spring:message
									code="viewinstrument.history" /></a></li>
					</ul>
					<!-- div to hold all content which is to be changed using the tabbed submenu -->
					<div class="tab-box">
						<!-- div displaying all contract review form elements -->
						<div id="contractreview-tab">
							<fieldset>
								<legend>
									<spring:message code="jicontractreview.contrrevopt" />
								</legend>
								<form:hidden path="ji.jobItemId" id="jobItemId"/>
								<ol>
									<li>
										<form:label path="ji.calType.calTypeId">
											<spring:message code="caltype" />:</form:label>
										<form:hidden id="originalcaltype" path="ji.calType.calTypeId" />
										<form:select id="calTypeSelect" path="calType"
											onchange="if(this.value != $j('#originalcaltype').val()){$j('#contractreviewcostlink').css({display:'inline'})};  refreshContracts(${form.ji.jobItemId}, $j(this).val()); return false; ">
											<c:forEach var="ct" items="${caltypes}">
												<form:option value="${ct.id}">
													<c:out value="${ct.label}"/>
												</form:option>
											</c:forEach>
										</form:select>
										<span id="contractreviewcostlink" style="display: none;">
											<spring:message code="jicontractreview.string2" />
										</span>
									</li>
									<li id="contractchoice">
										<c:choose>
											<c:when test="${fn:length(contracts) gt 0}">
												<form:label path="contractId"><spring:message code="company.contract"/>:</form:label>
												<span>
													<form:select id="contractSelect" path="contractId" onchange="changeContract(${jobItem.jobItemId});">
														<form:option value="0">N/A</form:option>
														<form:options items="${contracts}" itemLabel="contractDescription" itemValue="contractId"/>
													</form:select>
												</span>
												<!-- displaying selected contract instructions -->
												<cwms-instructions show-mode="RELATED" link-contractid="${not empty jobItem.contract ? jobItem.contract.id:0}"></cwms-instructions>
											</c:when>
											<c:otherwise>
												<label><spring:message code="company.contract"/>:</label>
												<span><spring:message code="contract.nocontract"/></span>
											</c:otherwise>
										</c:choose>
									</li>
									<li id="warrantyTerms"> 
										<label><spring:message code="jicontractreview.warrantyTerms"/>:</label>
										<div id="warrantyTermsLabel">
											<div id="ContractSelectedNotSelected">
												<c:forEach items="${calWarterms}" var="entry">
													<span>
														${entry.key} <spring:message code="jicontractreview.warrantyCalibrationdays"/> (${entry.value})
													</span>
												</c:forEach>
											,
												<c:forEach items="${repWarterms}" var="entry">
													<span>
														${entry.key} <spring:message code="jicontractreview.warrantyRepairdays"/> (${entry.value})
													</span>
												</c:forEach>
											</div>
											<div id="ContractSelected" class="hid">
											
											</div>
										</div>
									</li>
									
										<li id="warrantyEvaluation">
											<label><spring:message code="jicontractreview.warrantyEvaluation"/>:</label>
									
												<c:if test="${CalibrationWarrantyApplicable eq false && RepairWarrantyApplicable eq false }">
													<span> 
														<spring:message code="jicontractreview.warrantyTermsnotapplicable"/> 
													</span>
												</c:if>
													
												<c:if test="${CalibrationWarrantyApplicable eq true && RepairWarrantyApplicable eq true }">
													<span> 
														<spring:message code="jicontractreview.warrantyCalibrationmaybeapplicable"/>
														   , <spring:message code="jicontractreview.warrantyRepairmaybeapplicable"/>
													</span>
												</c:if>
												
												<c:if test="${CalibrationWarrantyApplicable eq false && RepairWarrantyApplicable eq true }">
													<span> 
														<spring:message code="jicontractreview.warrantyCalibrationnotapplicable"/> 
														   , <spring:message code="jicontractreview.warrantyRepairmaybeapplicable"/>
													</span>
												</c:if>
												
												<c:if test="${CalibrationWarrantyApplicable eq true && RepairWarrantyApplicable eq false }">
													<span> 
														<spring:message code="jicontractreview.warrantyCalibrationmaybeapplicable"/>
														   , <spring:message code="jicontractreview.warrantyRepairnnotapplicable"/> 
													</span>
												</c:if>
							
										</li>
									<li>
										<form:label path="ji.turn">
											<spring:message code="jicontractreview.turnaround" />:</form:label>
										<form:input id="jiturn" path="turnaround" type="number" min="1" max="365" onchange="turnChange($j(this).val())"/>
										<span class="attention"><form:errors path="turnaround" /></span>
									</li>
									<li class="hid" id="dueDateComments">
										<label for="dueDateComment">
											<spring:message code="comment" />:</label>
										<input type="hidden" id="oldTurn" value="${ form.ji.turn }"/>
										<textarea name="dueDateComment" id="dueDateComment" cols="80" rows="3"></textarea>
									</li>
									
									<li>
										<form:label path="agreedDelDate"><spring:message code="jicontractreview.agrdeldate" />:</form:label>
										<c:if test="${form.ji.job.agreedDelDate != null}">
											<spring:message code="jicontractreview.string4" />
											(<b>
												<fmt:formatDate value="${form.ji.job.agreedDelDate}" type="date" dateStyle="SHORT" />
											</b>)
											<spring:message code="jicontractreview.string5" />:
										</c:if>
										<form:input path="agreedDelDate" id="agreedDelDateDP" type="date"
											autocomplete="off"  onchange="toggleComment(event.target.value,'oldAgreedDelDate','agreedDelDateComments')"/>
										<a role="button" class="mainlink" onclick=" {document.getElementById('agreedDelDateDP').value = ''}">
											<spring:message code="clear" />
										</a>
									</li>
									<c:if test="${ not empty form.ji.history }">										
										<li id="lastDateComment">
											<label for="lastAgreedDelDateComment">
												<spring:message code="jicontractreview.agreeddeliverycomment" />:</label>
											<span id="lastDateComment">
												${ lastDateComment }</span>
										</li>
									</c:if>
									<li class="hid" id="agreedDelDateComments">
										<label for="agreedDelDateComments">
											<spring:message code="jicontractreview.agreeddeliverycomment" />:</label>
										<input type="hidden" id="oldAgreedDelDate"
											value="${cwms:isoDate(form.ji.agreedDelDate != null ? form.ji.agreedDelDate:form.ji.job.agreedDelDate)}"
											/>
										<textarea name="agreedDelDateComment" id="agreedDelDateComment" cols="80" rows="3"></textarea>
									</li>
									
									<c:if test="${form.ji.clientReceiptDate != null}">
										<li >
											<form:label path="clientReceiptDate">
												<spring:message code="delnotesummary.clientreceiptdate" />:</form:label>
										<form:input type="datetime-local" path="clientReceiptDate"
											step="60" max="${cwms:isoDatetime(form.currentDate)}"/>
										</li>
									</c:if>
									<li>
										<form:label path="inMethodId">
											<spring:message code="jicontractreview.transpin" />:</form:label>
										<form:select path="inMethodId" items="${transportInOptions}" itemValue="key" itemLabel="value" />
										<span class="attention"><form:errors path="inMethodId" /></span>
									</li>
									<li>
										<form:label path="returnMethodId">
											<spring:message code="jicontractreview.transpout" />:</form:label>
										<form:select path="returnMethodId" items="${transportOutOptions}" itemValue="key" itemLabel="value" />
										<span class="attention"><form:errors path="returnMethodId" /></span>
									</li>
									<li>
										<form:label path="clientReference">
											<spring:message code="jicontractreview.jobitemclientref" />:</form:label>
										<form:input path="clientReference"></form:input>
									<li>
										<form:label path="ji.approvedToBypassCosting">
											<spring:message code="jicontractreview.bypasscosting" />:</form:label>
										<div style="float: left;">
											<form:checkbox path="ji.approvedToBypassCosting"
												onclick="toggleBypassWarning(this);" />
										</div>
										<div id="bypass-warning"
											style="float: left; margin-left: 10px;"
											class=<c:choose><c:when test="${form.ji.approvedToBypassCosting}">"vis"</c:when><c:otherwise>"hid"</c:otherwise></c:choose>>
											[&nbsp;<b><spring:message code="jicontractreview.warning" />:</b> <spring:message
												code="jicontractreview.string6" /> (<spring:message
												code="jicontractreview.string7" />)&nbsp;]
											<br />
											[&nbsp;<b><spring:message code="jicertificates.note" />:</b> <spring:message
												code="jicontractreview.string8" />]</div>
										<div class="clear"></div>
									</li>
									
									<li>
										<label>
											<spring:message code="viewjob.returntoaddr" />:
										</label>
										<div class="float-left">
											<c:choose>
												<c:when test="${ not empty form.ji.returnToAddress }">
													<b><c:out value="${ form.ji.returnToContact.name }"/></b>
													<br/>
													<b><c:out value="${ form.ji.returnToAddress.addressLine }"/></b>
													<br/>
													<b><c:out value="${ form.ji.returnToAddress.sub.subname },${ form.ji.returnToAddress.sub.comp.coname }"/></b>
												</c:when>
												<c:otherwise>
													<span><c:out value="${ form.ji.job.returnTo.addressLine }"/></span>
													<b>(<spring:message code="jicontractreview.string5" />)</b>
												</c:otherwise>
											</c:choose>
											<a href="returntoaddress.htm?jobitemid=${form.ji.jobItemId}" target="blank">
												<img src="img/icons/form_edit.png" width="16" height="16" class="img_inl_margleft" />
											</a>
										</div>
									</li>
									
									<li>
										<label><spring:message
												code="jicontractreview.onbehalfitem" />:</label>
										<div class="float-left" id="behalfdiv">
											<c:choose>
												<c:when test="${form.ji.onBehalf == null}">
													<span>
														<spring:message code="jicontractreview.string9" />
													</span>
												</c:when>
												<c:otherwise>
													<span id="onbehalf">
														<links:companyLinkDWRInfo
															company="${form.ji.onBehalf.company}" rowcount="1"
															copy="true" />
														&nbsp;[${form.ji.onBehalf.setBy.name}]
														<a href=""
														onclick="event.preventDefault(); removeItemOnBehalf(${form.ji.jobItemId}, ${form.ji.onBehalf.id});">
															<img src="img/icons/delete.png" width="16" height="16"
															class="img_inl_margleft"
															alt="<spring:message code='jicontractreview.remonbehalfitem'/>"
															title="<spring:message code='jicontractreview.remonbehalfitem'/>" />
														</a>
													</span>
												</c:otherwise>
											</c:choose>
											<cwms:securedLink permission="ON_BEHALF"  classAttr="mainlink"  parameter="?jobitemid=${form.ji.jobItemId}" collapse="True" >
												<img src="img/icons/form_edit.png" width="16" height="16"
												class="img_inl_margleft"
												alt="<spring:message code='jicontractreview.string10'/>"
												title="<spring:message code='jicontractreview.string10'/>" />
											</cwms:securedLink>
										</div>
										<!-- clear floats and restore page flow -->
										<div class="clear-0"></div>
									</li>
									 <li>
										<form:label path="ji.reqCleaning">
											<spring:message code="jicontractreview.reqclean" />:</form:label>
										<span>
											<form:radiobutton path="ji.reqCleaning" class="radio_inl"
												value="true" />
											<spring:message code="jicontractreview.clean" />
											&nbsp;&nbsp;
											<form:radiobutton path="ji.reqCleaning" class="radio_inl"
												value="false" />
											<spring:message code="jicontractreview.dontclean" />
										</span>
									</li>
									<li>
										<form:label path="demands"><spring:message code="additionalDemand.additionalDemands" /></form:label>
										<div class="clear"></div>
										<c:forEach var="demand" items="${additionalDemands}">
											<spring:message var="demandLabel" code="${demand.messageCode}"/>
											<form:label path="demands[${demand.ordinal()}]">${demandLabel}</form:label>
											<form:checkbox id="demands${demand.ordinal()}" path="demands[${demand.ordinal()}]"/>
											<div class="clear"></div>
										</c:forEach>
									</li>
									<c:choose>
										<c:when test="${form.ji.job.type eq 'SITE'}">
											<li>
												<form:label path="reserveCertificate">
													<spring:message code="jicontractreview.reservecertificate" />:
												</form:label>
												<div class="float-left">
													<form:checkbox path="reserveCertificate"
														onclick="toggleReserveCertificate(this);" />
												</div>
												<div id="reserve-certificate" class="float-left">
													<spring:message code="jicontractreview.reservecertificateinfo" />
												</div>
											</li>
											
											<li id="certificate-date">
												<form:label path="certDate">
													<spring:message code="caldate" />:
												</form:label>
												<div class="float-left">
													<form:input path="certDate" type="date"
														autocomplete="off"  />
												</div>
												<span class="attention"><form:errors path="certDate" /></span>
											</li>	
										</c:when>
										<c:otherwise>
											<li class="hid">
												<form:hidden path="reserveCertificate" value="false" />  
												<form:hidden path="certDate" value=""/>  
											</li>
										</c:otherwise>
									</c:choose>		
									<li>
										<form:label path="bulkUpdate">
											<spring:message code="jicontractreview.bulkupdrev" />:</form:label>
										<span>
											<form:radiobutton path="bulkUpdate" class="radio_inl"
												value="false" onclick="updateBulk(false);" />
											<spring:message code="jicontractreview.single" />
											&nbsp;&nbsp;
											<form:radiobutton path="bulkUpdate" class="radio_inl"
												value="true" onclick="updateBulk(true);" />
											<spring:message code="jicontractreview.multiple" />
										</span>
									</li>
									<c:choose>
										<c:when test="${form.bulkUpdate}">
											<c:set var="displayClass" value="vis"/>
										</c:when>
										<c:otherwise>
											<c:set var="displayClass" value="hid"/>
										</c:otherwise>
									</c:choose>
									<li id="copyoptions" class="${displayClass}">
										<label><spring:message code="jicalloffitem.copy" />:</label>
										<div class="float-left" style="margin-top: 4px;"
											id="copyparams">
											<div class="marg-bot">
												<strong><spring:message
														code="jicontractreview.options" /></strong>
											</div>
											<div class="marg-bot">													
												<input type="checkbox"
													onclick="selectAllItemsWithSimiliar(this.checked, 'copyparams', false);" />
												<spring:message code="jiactions.all" />
											</div>
											<div>
												<form:checkbox path="copyCalType" />
												<spring:message code="calibrationtype" /> (<cwms:besttranslation
													translations="${form.ji.calType.serviceType.shortnameTranslation}" />)
											</div>
											<div>
												<form:checkbox path="copyContract" />
												<spring:message code="company.contract" />
												<span id="contractnotocopy"></span>
											</div>
											<div>
												<form:checkbox path="copyTurnaround" />
												<spring:message code="jicontractreview.turnaround" /> (${form.ji.turn} <spring:message
													code="days" />)
											</div>
											<div>
												<form:checkbox path="copyTransportIn" />
												<spring:message code="jicontractreview.transpin" />
											</div>
											<div>
												<form:checkbox path="copyTransportOut" />
												<spring:message code="jicontractreview.transpout" />
											</div>
											<div>
												<form:checkbox path="copyBypassCosting" />
												<spring:message code="jicontractreview.bypasscosting" /> (${form.ji.approvedToBypassCosting})
											</div>
											<div>
												<form:checkbox path="copyOnBehalf" />
												<spring:message code="jicontractreview.onbehalf" />
											</div>
											<div>
												<form:checkbox path="copyCleaning" />
												<spring:message code="jicontractreview.clean" /> (${form.ji.reqCleaning})
											</div>
											<div>
												<form:checkbox path="copyReserveCertificate" />
												<spring:message code="jicontractreview.reservecertificate" />
											</div>
											<div>
												<form:checkbox path="copyClientReference" />
												<spring:message code="jicontractreview.jobitemclientref" />
											</div>
											<div>
												<form:checkbox path="copyWorkRequirements" />
												<spring:message code="vm_global.workrequirements" />
											</div>
											<%--
												Note, this copies the Requirement entities 
												(only used for quotation linkage), so is  
												deactivated to prevent user confusion. 
											--%>
											<%--
											<div>
												<form:checkbox path="copyRequirements" />
												<spring:message code="jicontractreview.requirements" />
											</div>
											--%>
											<div>
												<form:checkbox path="copyCosts"
													onclick="if(this.checked == true){$j('#costwarning').removeClass().addClass('vis');}else{$j('#costwarning').removeClass().addClass('hid');}" />
												<spring:message code="jicontractreview.copycosts" />
												<span id="costwarning" class="hid">
													<span class="attention">
														<spring:message code="jicontractreview.string11" />
													</span>
												</span>
											</div>
											<div>
												<form:checkbox path="copyOutcome" />
												<spring:message code="jicontractreview.copyoutcome" />
											</div>
										</div>
										<div class="float-left marginleft60" style="margin-top: 4px;"
											id="copyitems">
											<div class="marg-bot">
												<strong><spring:message code="items" /></strong>
											</div>
											<div class="marg-bot">													
												<input type="checkbox" id="selectAll"
													onclick="selectAllItemsWithSimiliar(this.checked, 'copyitems', false);" />
												<spring:message code="jiactions.all" />
												&nbsp;-&nbsp;
												<input type="checkbox" id="selectSimiliar"
													onclick="selectAllItemsWithSimiliar(this.checked, 'copyitems', true);" />
												<spring:message code="jicontractreview.similar" />
											</div>
											<c:forEach var="item" items="${viewWrapper.itemsOnJob}">
												<c:if test="${item.jobItemId != form.ji.jobItemId}">
													<c:set var="style" value="" />
													<c:set var="similiar" value="" />
													<c:if
														test="${item.instrument.modelid == form.ji.inst.model.modelid}">
														<c:set var="style" value="attention" />
														<c:set var="similiar" value="similiar" />
													</c:if>
													<div class="${style}">
														<form:checkbox path="jobItemIds" value="${item.jobItemId}"
															class="${similiar}" />
														${item.itemno}
														<instmodel:showInstrumentModelProjectionLink instrument="${item.instrument}" />
														<c:if test="${not empty item.instrument.plantno}">
															<span style="color: black;"> ( <spring:message  code="plantno"/> :  ${item.instrument.plantno} ) </span>	
														</c:if>
													</div>
												</c:if>
											</c:forEach>
										</div>
										<!-- clear floats and restore page flow -->
										<div class="clear-0"></div>
									</li>
									<li>
										<label><spring:message
												code="jicontractreview.contrrevnot" />:</label>
										<div class="float-left width80">
											<!-- this div creates a new preset comments plugin. -->		
											<form:textarea path="reviewComments" value="${form.reviewComments}"
												class="width96 presetComments CONTRACT_REVIEW" cols=""
												rows="5" />
											<span class="attention"><form:errors
													path="reviewComments" /></span>
											<br />
											<spring:message code="jicontractreview.string12" />
											<form:checkbox path="saveComments" />
											<span class="attention"><form:errors
													path="saveComments" /></span>
										</div>
										<!-- clear floats and restore page flow -->
										<div class="clear-0"></div>
									</li>
									<li>
                          				<cwms-contractreview-cost-table currencyCode="${form.ji.job.currency.currencyCode}"
										  costs="${cwms:objectToJson(costs)}"
										  jobId="${form.ji.job.jobid}"
										  jobItemId="${form.ji.jobItemId}"
										  ></cwms-contractreview-cost-table>
										
										<c:if test="${ empty form.ji.costs or form.ji.costs.size() eq 0 }">
											<div class="messageBox1">
												<div class="messageBox2">
													<div class="messageBox3 text-center">
														<spring:message code="jicontractreview.costlookupsstillrunning" />
													</div>
												</div>
											</div>
										</c:if>
									</li>
									<c:if test="${actionoutcomes != null}">
										<li>
											<form:label path="actionOutcomeId">
												<spring:message code="jicontractreview.outcome" />:
											</form:label>
											<form:select path="actionOutcomeId">
												<c:forEach var="ao" items="${actionoutcomes}">
													<form:option value="${ao.id}">
														<cwms:besttranslation translations="${ao.translations}" />
													</form:option>
												</c:forEach>
											</form:select>
										</li>
									</c:if>
									<li>
										<label>&nbsp;</label>
										<form:button name="submit" value="update">
											<spring:message code='update' />
										</form:button>
										<form:button name="submit" class="contract_review" value="contract review">
											<spring:message code='jicontractreview.contrrew' />
										</form:button>
									</li>
								</ol>
							</fieldset>
						</div>
						<!-- end of contract review form elements div -->
						<!-- div displaying all purchase orders for job item -->
						<div id="PO-tab" class="hid">
                    <cwms-contractreview-purchordtable jobId="${form.ji.job.jobid}" jobItemId="${form.ji.jobItemId}"
						jobItemBPOs="${cwms:collectionToJson(jobItemBPOs)}" 
						jobItemPOs="${cwms:collectionToJson(jobItemPOs)}"
					></cwms-contractreview-purchordtable>
						</div>
						<!-- end of purchase order div -->
						<!-- div displaying all contract review history -->
						<div id="revhistory-tab" class="hid">
						<fieldset>
						<ol>
									<li>
							<table class="default2" id="contrevhistory"
								summary="This table contains contract review history and comments">
								<thead>
									<tr>
										<th colspan="3"><spring:message
												code="jicontractreview.contrrevhist" /></th>
									</tr>
									<tr>
										<th class="reviewer" scope="col"><spring:message
												code="jicontractreview.reviewer" /></th>
										<th class="reviewed" scope="col"><spring:message
												code="jicontractreview.datereved" /></th>
										<th class="notesleft" scope="col"><spring:message
												code="jicontractreview.notesleft" /></th>
									</tr>
								</thead>
								<tbody>
									<c:choose>
										<c:when test="${form.ji.contactReviewItems.size() > 0}">
											<c:forEach var="reviewItem"
												items="${form.ji.contactReviewItems}">
												<tr class="odd">
													<td>${reviewItem.review.reviewBy.name}</td>
													<td><fmt:formatDate
															value="${reviewItem.review.reviewDate}"
															type="both" dateStyle="SHORT" timeStyle="SHORT" /></td>
													<td>${reviewItem.review.comments}</td>
												</tr>
											</c:forEach>
										</c:when>
										<c:otherwise>
											<tr class="odd">
												<td colspan="3"><spring:message
														code="jicontractreview.string18" /></td>
											</tr>
										</c:otherwise>
									</c:choose>
								</tbody>
							</table>
							</li>
							
							<li>
							<table class="default2" id="contrevotherhistory"
								summary="This table contains other contract review history and comments">
								<thead>
									<tr>
										<th colspan="6"><spring:message
												code="jicontractreview.contrrevotherhist" /></th>
									</tr>
									<tr>
										<th class="reviewer" scope="col"><spring:message
												code="viewinstrument.changedby" /></th>
										<th class="reviewed" scope="col"><spring:message
												code="viewinstrument.changedon" /></th>
										<th class="reviewed" scope="col"><spring:message
												code="jicontractreview.fieldname" /></th>
										<th class="reviewed" scope="col"><spring:message
												code="jicontractreview.oldvalue" /></th>
										<th class="reviewed" scope="col"><spring:message
												code="jicontractreview.newvalue" /></th>
										<th class="notesleft" scope="col"><spring:message
												code="comment" /></th>
									</tr>
								</thead>
								<tbody>
									<c:choose>
										<c:when test="${form.ji.history.size() > 0}">
											<c:forEach var="historyItem"
												items="${form.ji.history}">
												<tr class="odd">
													<td>${historyItem.changeBy.name}</td>
													<td><fmt:formatDate
															value="${historyItem.changeDate}"
															type="both" dateStyle="SHORT" timeStyle="SHORT" /></td>
													<td>
														<c:if test="${ not empty historyItem.fieldname }">
															<spring:message code="jicontractreview.history.${fn:toLowerCase(historyItem.fieldname)}"/>
														</c:if>
													</td>
													<td>${historyItem.oldValue}</td>
													<td>${historyItem.newValue}</td>
													<td>${historyItem.comment}</td>
												</tr>
											</c:forEach>
										</c:when>
										<c:otherwise>
											<tr class="odd">
												<td colspan="6"><spring:message
														code="jicontractreview.string19" /></td>
											</tr>
										</c:otherwise>
									</c:choose>
								</tbody>
							</table>
							</li>
							</ol>
							</fieldset>
						</div>
						<!-- end of contract review history div -->
					</div>
					<!-- end of div to hold all tabbed submenu content -->
				</div>
				<!-- end of div to hold tabbed submenu -->
			</form:form>
			<!-- this div clears floats and restores page flow -->
			<div class="clear"></div>
		</div>
		<!-- end of infobox -->
	</jsp:body>
</jobs:jobItemTemplate>