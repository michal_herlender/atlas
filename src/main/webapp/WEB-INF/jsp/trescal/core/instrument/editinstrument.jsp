<%-- File name: /trescal/core/instrument/editinstrument.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="inst" tagdir="/WEB-INF/tags/instrument"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
        <span class="headtext"><spring:message
				code="editinstr.title" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
    	<script>
			var threadArray = new Array();
			<c:forEach items="${threadtypes}" var="thread">
				threadArray.push({id: ${thread.id}, type: '${thread.type}'});
			</c:forEach>
		</script> 
		<script	src="script/trescal/core/instrument/EditInstrument.js"></script>
		<script type='module' src='script/components/search-plugins/cwms-modelcode-search/cwms-modelcode-search.js'></script>
		<script type='module' src='script/components/search-plugins/cwms-capability-search/cwms-capability-search.js'></script>
    	<script type="module" src="script/components/cwms-duplicates/cwms-edit-instrument/cwms-edit-instrument-duplicates/cwms-edit-instrument-duplicates.js"></script>
    </jsp:attribute>
	<jsp:body>
	
	<t:showErrors path="command.*" showFieldErrors="true" />
	
	<!-- set variables to show or hide areas of the page dependant on whether the page is displayed for a new or edited instrument -->
	<c:choose>
		<c:when test ="${command.plantId == null}">
			<c:set var="editSelVis" value="vis"/>
			<c:set var="infoTextVis" value="hid"/>
		</c:when>
		<c:otherwise>
			<c:set var="editSelVis" value="hid"/>
			<c:set var="infoTextVis" value="vis"/>
		</c:otherwise>
	</c:choose>	
	<div class="infobox" id="editinstrument">
		<%--@elvariable id="command" type="org.trescal.cwms.core.instrument.form.EditInstrumentForm"--%>
		<form:form id="mainForm" modelAttribute="command" data-current-plant-id="${command.plantId}"
		data-disallow-same-plant-number="${disallowSamePlantNumber}" >
			<fieldset>
				<form:hidden path="lastModified"/>
				<legend><spring:message code="editinstr.title"/></legend>
				<ol>
					<li>	
						<label><spring:message code="instmodelname"/>:</label>
						<t:showViewModelLink instrumentmodel="${model}" target="_blank"/> <a href="<c:url value='/addinstrumentmodelsearch.htm?plantid=${command.plantId}'/>"><spring:message code="editinstr.change"/></a>
						<form:hidden path="modelid"/>
					</li>
					<c:set var="instModel" value="${model}"/>
					<c:if test="${instModel.ranges != null && instModel.ranges.size() > 0 && instModel.modelMfrType != 'MFR_GENERIC'}">
						<li>
							<label><spring:message code="editinstr.modelrangesize"/>:</label>
							<div id="modelRanges" class="float-left padtop">
								<table>
									<c:forEach items="${sortedCharacteristicDescriptions}" var="characteristicDescription">
										<tr>
											<td>
												<cwms:besttranslation translations="${characteristicDescription.translations}"/>
											</td>
											<td>
												<c:choose>
													<c:when test="${characteristicDescription.getCharacteristicType() == 'LIBRARY' }">
														<c:forEach var="libValue" items="${model.libraryCharacteristics.get(characteristicDescription)}" varStatus="libLoop">
															<c:if test="${!libLoop.first}">, </c:if>
															<cwms:besttranslation translations="${libValue.translation}"/> 
														</c:forEach>
													</c:when>
													<c:otherwise>
														<c:forEach var="rangeKeyValue" items="${rangeKeyValues}">
															<c:if test="${rangeKeyValue.key == characteristicDescription.characteristicDescriptionId }">
																${rangeKeyValue.value}
															</c:if>
														</c:forEach>
													</c:otherwise>
												</c:choose>
											</td>
										</tr>
									</c:forEach>
								</table>
							</div>
							<!-- clear floats and restore page flow -->
							<div class="clear-0"></div>
						</li>
					</c:if>
					<c:if test="${instModel.modelMfrType == 'MFR_GENERIC'}">
						<li>
							<label><spring:message code="characteristics"/></label>
							<!-- clear floats and restore page flow -->
							<div class="clear-0"></div>
							<div>
								<table class="default4">
									<thead>
										<tr>
											<td rowspan="2"><spring:message code="characteristics"/></td>
											<td colspan="3"><spring:message code="Instrumentmodelrange"/></td>
											<td colspan="2"><spring:message code="Instrumentproperty"/></td>
										</tr>
										<tr>
											<td><spring:message code="min"/></td>
											<td><spring:message code="max"/></td>
											<td><spring:message code="unit"/></td>
											<td><spring:message code="value"/></td>
											<td><spring:message code="unit"/></td>
										</tr>
									</thead>
									<tbody>
									<c:forEach var="charId" items="${command.characteristicMap.keySet()}">
										<c:set var="characteristic" value="${command.characteristicMap.get(charId)}"/>
										<tr>
											<td><cwms:besttranslation translations="${characteristic.characteristic.translations}"/></td>
											<c:choose>
												<c:when test="${characteristic.modelRange != null}">
													<td>${characteristic.modelRange.minvalue == null ? characteristic.modelRange.start : characteristic.modelRange.minvalue}</td>
													<td>${characteristic.modelRange.maxvalue == null ? characteristic.modelRange.end : characteristic.modelRange.maxvalue}</td>
													<td>${characteristic.modelRange.uom == null ? '' : characteristic.modelRange.uom.symbol}</td>
												</c:when>
												<c:otherwise>
													<td colspan="3" style="background-color: antiquewhite"></td>
												</c:otherwise>
											</c:choose>
											<c:choose>
												<c:when test="${characteristic.characteristic.characteristicType == 'LIBRARY'}">
													<td colspan="2">
														<form:select path="characteristicMap[${charId}].libraryValueId">
															<form:option value="0">&nbsp;</form:option>
															<c:forEach var="libVal" items="${characteristic.libraryValues}">
																<form:option value="${libVal.characteristiclibraryid}"><cwms:besttranslation translations="${libVal.translation}"/></form:option>
															</c:forEach>
														</form:select>
													</td>
												</c:when>
												<c:when test="${characteristic.characteristic.characteristicType == 'FREE_FIELD'}">
													<td colspan="2">
														<form:input path="characteristicMap[${charId}].freeTextValue"/>
													</td>
												</c:when>
												<c:when test="${characteristic.characteristic.characteristicType == 'VALUE_WITH_UNIT'}">
													<td>
														<input type="text" pattern="[0-9]*(,|.)[0-9]*" lang="${locale}" name="characteristicMap[${charId}].value" value="${characteristic.value}"/>
													</td>
													<td>
														<form:select path="characteristicMap[${charId}].uomid" items="${uomlist}" itemLabel="symbol" itemValue="id" htmlEscape="false"/>
													</td>
												</c:when>
												<c:otherwise>
													<td colspan="2"/>
												</c:otherwise>
											</c:choose>
										</tr>
									</c:forEach>
									</tbody>
								</table>
							</div>
							<!-- clear floats and restore page flow -->
							<div class="clear-0"></div>
						</li>
					</c:if>
					<c:if test="${command.modelMfrType != null && command.modelMfrType == 'MFR_GENERIC'}">
						<li>
	                        <label><spring:message code="manufacturer"/>:</label>
	                        <!-- float div left -->
	                        <div class="float-left">
	                            <!-- this div creates a new manufacturer jquery search plugin -->   
	                            <div class="mfrSearchJQPlugin">
	                                <input type="hidden" name="fieldId" value="mfrid"/>
	                                <input type="hidden" name="fieldName" value="mfrname"/>
	                                <input type="hidden" name="mfrIdPrefill" value="${command.mfrid}"/>
	                                <input type="hidden" name="mfrNamePrefill" value="${command.mfrname}"/>
	                                <input type="hidden" name="textOnlyOption" value="false"/>
	                                <input type="hidden" name="tabIndex" value="4"/>
	                                <!-- manufacturer results appear here -->
	                            </div>
	                        </div>                                                                                          
	                        <!-- clear floats and restore pageflow -->
	                        <div class="clear-0"></div>
                    	</li>
                    	<li>
                    		<form:label path="modelName"><spring:message code='model'/>:</form:label>
							<form:input path="modelName"/>
							<form:errors path="modelName"/>
                    	</li>
					</c:if>
					<li class="${infoTextVis}">
						<label><spring:message code="editinstr.contactcompany"/>:</label>
						<span>${instrument.comp.coname}</span>
					</li>
					<li class="${infoTextVis}">
						<label><spring:message code="editinstr.contactsubdivision"/>:</label>
						<span>${instrument.con.sub.subname}</span>
					</li>
					<li class="${infoTextVis}">
						<label><spring:message code="editinstr.contactaddress"/>:</label>
						<span>${instrument.add.addr1}</span>
					</li>										
					<li class="${infoTextVis}">
						<label><spring:message code="editinstr.contactname"/>:</label>
						<span>
							${instrument.con.name} 
							(<a href="<c:url value='/viewcomp.htm?coid=${instrument.con.sub.comp.coid}'/>" class="mainlink">
								${instrument.con.sub.comp.coname}
							</a>)
							 - 
							(<a href="" class="mainlink" onclick="event.preventDefault(); createCascadingSearchPlugin('${command.coid}', '${instrument.add.sub.subdivid}', '${command.personid}', '${command.addrid}', '${command.locid}');">
								<spring:message code="editinstr.change"/>
							</a>)
						</span>
					</li>
					<li class="${infoTextVis}">
						<label><spring:message code="editinstr.instrumentlocation"/>:</label>
						<span>
							<c:choose>
								<c:when test="${instrument.loc != null}">
									${instrument.loc.location} - (<a href="" class="mainlink" onclick="event.preventDefault(); createCascadingSearchPlugin('${command.coid}', '${instrument.add.sub.subdivid}', '${command.personid}', '${command.addrid}', '${command.locid}');"><spring:message code="editinstr.change"/></a>)
								</c:when>
								<c:otherwise>
									<a href="" class="mainlink" onclick="event.preventDefault(); createCascadingSearchPlugin('${command.coid}', '${instrument.con.sub.subdivid}', '${command.personid}', '${command.addrid}', '${command.locid}');"><spring:message code="editinstr.updatelocation"/></a>
								</c:otherwise>
							</c:choose>
						</span>
					</li>
					<li id="contactSelector" class="${editSelVis}">
						<c:if test="${not empty instrument.flexibleFields}">
							<label><spring:message code="warning"/>:</label>
							<span class="attention"><spring:message code="editinstr.flexiblefieldwarning" arguments="${instrument.flexibleFields.size()}" /></span>
	                        <div class="clear"></div>
						</c:if>
						<label><spring:message code="editinstr.contactselector"/>:</label>
						<div class="float-left">
							<!-- these hidden input fields hold the values of the current company, subdivision, contact, address
								 ids when the user is editing the instrument but has not edited these specific fields.  When the
								 user decides to edit any of the fields above these hidden inputs are removed and new ones are added
								 when the cascading search is created -->
							<c:if test="${infoTextVis == 'vis'}">
								<div id="tempIds">
									<form:hidden path="coid"/>
									<input type="hidden" name="groupId" value="${instrument.comp.companyGroup.id}"/>
									<input type="hidden" name="subdivid" value="${instrument.add.sub.subdivid} "/>
									<input type="hidden" name="personid" value="${instrument.con.personid}" />
									<input type="hidden" name="addrid" value="${instrument.add.addrid}" />
									<input type="hidden" name="locationid" value="${instrument.loc.locationid}" />
								</div>
							</c:if>
							
							<c:if test="${editSelVis == 'vis'}">
								<div class="attention"><form:errors path="coid"/></div>	
							   	<div class="attention"><form:errors path="personid"/></div>	
								<div class="attention"><form:errors path="addrid"/></div> 		
								<div class="attention"><form:errors path="locid"/></div>   
								<div id="cascadeSearchPlugin">
									<input type="hidden" id="compCoroles" value="client,business" />
									<input type="hidden" id="cascadeRules" value="subdiv,contact,address" />
									<input type="hidden" id="searchLocations" value="true" />
									<input type="hidden" id="noLocation" value="true" />
									<c:if test="${command.coid != 0}">
										<input type="hidden" id="prefillIds" value="${command.coid},${instrument.con.sub.subdivid},${command.personid},${command.addrid},${command.locid}" />
									</c:if>
								</div>
							</c:if>
						</div>
					</li>
					<li>
						<label for="instStartRange"><spring:message code='updateallitems.rangesize'/>:</label>
						<c:if test="${not empty instrument}">
							<c:if test="${instrument.ranges != null && instrument.ranges.size() > 0}">
								<c:set var="start" value="${instrument.ranges.toArray()[0].start}"/>
								<c:set var="end" value="${instrument.ranges.toArray()[0].end}"/>
								<c:set var="uomid" value="${instrument.ranges.toArray()[0].uom.id}"/>
							</c:if>
						</c:if>
						<form:input type="text" size="4" path="instStartRange" value="${start}"/> -&nbsp;
						<form:input type="text" size="4" path="instEndRange" value="${end}"/>
						<form:select path="instUomId" style=" width: 157px">
							<c:forEach var="uom" items="${uomlist}">
									<option value="${uom.id}" <c:if test="${ uom.id == uomid}">selected</c:if>>${uom.formattedSymbol}</option>
							</c:forEach>
						</form:select>
						<span class="attention"><form:errors path="instStartRange"/></span>
					</li>
     	           <cwms-edit-instrument-duplicates  currentPlantId="${instrument.plantid}" modelId="${model.modelid}" coid=${instrument.con.sub.comp.coid} >      
						<li>
							<label for="serialNo"><spring:message code='serialno'/>:</label>
							<form:input id="serialNo" path="serialNo"/>
							<span class="attention"><form:errors path="serialNo"/></span>
						</li>
						<li>
							<label for="plantNo"><spring:message code="plantno"/>:</label>
							<form:input id="plantNo" path="plantNo"/>
							<span class="attention"><form:errors path="plantNo"/></span>
						</li>
     	           </cwms-edit-instrument-duplicates>      
				
					<li>
						<label for="plantNo"><spring:message code="editinstr.clientbarcode"/>:</label>
						<form:input id="clientBarcode" path="clientBarcode"/>
						<span class="attention"><form:errors path="clientBarcode"/></span>
					</li>
					<li>
						<label for="freeTextLocation"><spring:message code="editinstr.freetextlocation"/>:</label>
						<form:input id="freeTextLocation" path="freeTextLocation"/>
						<span class="attention"><form:errors path="freeTextLocation"/></span>
					</li>
					<c:choose>
						<c:when test="${command.plantId == null}">
							<li>
								<form:label path="nextCalDueDate"><spring:message code="editinstr.nextcaldue"/>:</form:label>
								<form:input path="nextCalDueDate" type="date" style=" width: 260px"/>
								<span class="attention"><form:errors path="nextCalDueDate"/></span>
							</li>
							<li>
								<form:label path="calFrequency"><spring:message code="editinstr.calfrequency"/>:</form:label>
								<form:input path="calFrequency"/> 
								<form:select path="calFrequencyUnitId" items="${units}" itemLabel="value" itemValue="key" style=" width: 110px"/>
								<form:errors path="calFrequency"/>
							</li>
						</c:when>
						<c:otherwise>
							<c:if test="${!calExist}">
								<li>
									<form:label path="calFrequency"><spring:message code="editinstr.calfrequency"/>:</form:label>
									<form:input path="calFrequency"/> 
									<form:select path="calFrequencyUnitId" items="${units}" itemLabel="value" itemValue="key" style=" width: 110px"/>
									<form:errors path="calFrequency"/>
								</li>
							</c:if>
							<li>
								<label><spring:message code="editinstr.nextcaldue"/>:</label>
								<span><spring:message code="editinstr.see"/> '<a href="viewinstrument.htm?plantid=${command.plantId}&loadtab=recall-tab" class="mainlink"><spring:message code='editinstr.recall'/></a> <spring:message code="editinstr.viewrecallrule"/></span>
							</li>
						</c:otherwise>
					</c:choose>
					<li>
						<form:label path="status"><spring:message code="status"/>:</form:label>
						<form:select path="status" items="${statuslist}" itemLabel="status" style=" width: 260px" onchange="onStatusChange(this)"/>
						<span class="attention"><form:errors path="status"/></span>
					</li>
					<li>
						<form:label path="storageTypeId"><spring:message code="editinstr.storagetype"/>:</form:label>
						<form:select path="storageTypeId" style=" width: 260px">
							<c:forEach var="type" items="${storage}">
								<form:option value="${type.id}">
									<cwms:besttranslation translations="${type.nametranslation}"/>
								</form:option>
							</c:forEach>
						</form:select>
						<span class="attention"><form:errors path="storageTypeId"/></span>
					</li>
					<li>
						<form:label path="usageTypeId"><spring:message code="editinstr.usagetype"/>:</form:label>
						<form:select path="usageTypeId" style=" width: 260px">
							<form:option value=""></form:option>
							<c:forEach var="type" items="${usageTypes}">
								<form:option value="${type.id}" data-linkedstatus="${type.instrumentStatus.name()}">
									<cwms:besttranslation translations="${type.nametranslation}"/>
								</form:option>
							</c:forEach>
						</form:select>
					<span class="attention"><form:errors path="usageTypeId"/></span>
					</li>
                    
					<li>
						<form:label path="firmwareAccessCode"><spring:message code="editinstr.firmwareaccesscode"/>:</form:label>
						<form:input id="firmwareAccessCode" path="firmwareAccessCode"/>
						<form:errors path="firmwareAccessCode"/>
					</li>
					<c:if test="${command.companyRole == 'BUSINESS'}">
						<li>
							<spring:message var="textInstrument" code='instrument' />
							<spring:message var="textStandard" code='editinstr.calibrationstandard' />
							<label for="calibrationStandard">
								<spring:message code="editinstr.instrumenttype"/>:
							</label>
							<form:select path="calibrationStandard" style=" width: 260px">
								<form:option value="false" label="${textInstrument}" />
								<form:option value="true" label="${textStandard}"/>
							</form:select>
							<form:errors path="calibrationStandard"/>
						</li>
					</c:if>
						<li>
							<form:label path="calExpiresAfterNumberOfUses">
                                <spring:message code="editinstr.percalrecallinterval"/>:
                            </form:label>
							<form:checkbox id="usages" path="calExpiresAfterNumberOfUses"/>
                        </li>

                        <li class="usageField" style="display: none">
							<form:label path="permittedNumberOfUses">
								<spring:message code="editinstr.permitednumberofuses"/>:
							</form:label>
							<form:input path="permittedNumberOfUses"/>
							<form:errors path="permittedNumberOfUses"/>
						</li>
						<li class="usageField" style="display: none">
							<form:label path="usesSinceLastCalibration">
                                <spring:message code="editinstr.calssincelastcal"/>:
                            </form:label>
							<form:input path="usesSinceLastCalibration"/>
							<form:errors path="usesSinceLastCalibration"/>
						</li>

					<li>
						<form:label path="procid"><spring:message code="editinstr.defaultcapability"/>:</form:label>
						<cwms-capability-search-form 
						name="procid"
						procid="${command.procid}" defaultvalue="${command.procedureReference}"></cwms-capability-search-form>
						<span class="attention"><form:errors path="procid"/></span>
						<!-- clear floats and restore page flow -->
						<div class="clear"></div>
					</li>
					<li>
						<form:label path="wiid"><spring:message code="editinstr.defaultworkinstruction"/>:</form:label>
						<c:if test="${instrument.workInstructions.size() >0}">
							<c:set var="workInstructionId" value="${instrument.workInstructions[0].id}"/>
							<c:set var="workInstructionTitle" value="${instrument.workInstructions[0].title}"/>
						</c:if>
						
						<div>
							<cwms-modelcode-search modelid="${model.modelid}" defaultvalue="${workInstructionTitle}" name="wiid" value="${workInstructionId}" ></cwms-modelcode-search>
							&nbsp;&nbsp;<a onclick="selectAllWI(${model.modelid});"><img src="img/icons/searchplugin_all.png" alt="View All" title="View All" width="16" height="16"></a>
						</div>
						<span class="attention"><form:errors path="wiid"/></span>
						<!-- clear floats and restore page flow -->
						<div class="clear"></div>
					</li>
					<li>
						<label><spring:message code="editinstr.customermanaged"/></label>
						<form:checkbox path="customerManaged"/>
					</li>
					<li>
							<label><spring:message code="editinstr.hireinstrument"/>:</label>
							<span>
								<input type="hidden" path="hireInstrumentReq" id="hireInstrumentReq" value="${command.hireInstrumentReq}" />

								<c:set var="checked" value=""/>
								<c:if test="${command.hireInstrumentReq == true}">
									<c:set var="checked" value="checked"/>
								</c:if>
								<form:checkbox path="hireInstrumentReq" checked="${checked}" value="true" />
							</span>
					</li>
					<li class="hireInst ${command.hireInstrumentReq ? 'vis' : 'hid'}">
						<label><spring:message code="editinstr.plantnoreview"/>:</label>
						<span class="hirePlantReview"></span>
					</li>
					<li>
						<label for="defaultClientRef"><spring:message code="instr.defaultClientRef"/>:</label>
						<form:input id="defaultClientRef" path="defaultClientRef"/>
						<span class="attention"><form:errors path="defaultClientRef"/></span>
					</li>
					<li>
						<label id="customerDescription"><spring:message code="editinstr.customerdescription"/>:</label>
						<form:input id="customerDescription" path="customerDescription"/>
						<form:errors path="customerDescription"/>
					</li>
					<li>
						<label id="customerSpecification"><spring:message code="editinstr.customerspecification"/>:</label>
						<form:input id="customerSpecification" path="customerSpecification"/>
						<form:errors path="customerSpecification"/>
					</li>
					<li>
						<label id="defaultServiceType"><spring:message code="editinstr.defaultservicetype"/>:</label>
						<form:select id="defaultServiceType" path="defaultServiceTypeId" style=" width: 262px" >
							<form:option value="">&nbsp;</form:option>
    						<form:options items="${serviceTypes}" itemValue="key" itemLabel="value"  />
						</form:select>
						<form:errors path="defaultServiceTypeId"/>
					</li>
					<li>
						<label>&nbsp;</label>
						<c:choose>
							<c:when test="${command.plantId != null}">	
								<button type="submit" click="this.disabled = true;  checkChangeCompany();"><spring:message code="update" /></button>
							</c:when>
							<c:otherwise>
								<button type="submit" id="addInstButton" ><spring:message code="create" /></button>
							</c:otherwise>
						</c:choose>									
					</li>
				</ol>
				<input id="onActiveJob" type="hidden" value="${onActiveJob}"/>
				<input id="oldCoid" type="hidden" value="${command.coid}"/>
				<input id="updateOnBehalf" name="updateOnBehalf" type="hidden" value="false"/>
			</fieldset>
		</form:form>
	</div>
	</jsp:body>
</t:crocodileTemplate>