<%-- File name: /trescal/core/instrument/viewcompanyinstrument.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address" %>
<%@ taglib prefix="instrument" tagdir="/WEB-INF/tags/instrument" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="searchcompinst.title"/> (${form.company.coname})</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/instrument/ViewCompanyInstrument.js'></script>
		<script type='text/javascript' src='script/thirdparty/jQuery/jquery.thickbox.js'></script>
	</jsp:attribute>
	<jsp:body>
		<form:form action="" id="viewCompanyInstrumentsForm" method="post" onsubmit=" showUnLoadWarning = false; " modelAttribute="form">
			<form:errors path="*">
            	<div class="warningBox1">
                     <div class="warningBox2">
                          <div class="warningBox3">
                                <h5 class="center-0 attention">
                                    <spring:message code="error.save" />
                                </h5>
                                <c:forEach var="e" items="${messages}">
                                    <div class="center attention"><c:out value="${e}"/></div>
                                </c:forEach>
                           </div>
                       </div>
                 </div>
            </form:errors>
			<form:hidden path="search.compId" />
			<form:hidden path="search.pageNo" id="pageNo" />
		
			<!-- infobox contains company instrument search form elements and is styled with nifty corners -->	
			<div class="infobox" id="search">
				
				<fieldset class="searchCompanyInstruments">
					
					<legend><spring:message code="searchcompinst.title"/></legend>
					
					<!-- column with width 50% (left) -->
					<div class="displaycolumn">
					
						<ol class="instSearchColumn">
							
							<li>
								<fieldset>
									<legend>
										<c:out value="${form.company.coname} " />
										<spring:message code="searchcompinst.instcount"/>
										<c:out value=" ${form.totalinstrumentcount}" />
									</legend>
								</fieldset>
							</li>
							<li>
								<label><spring:message code="barcode"/>:</label>
								<form:input type="text" path="search.barcode" value="${form.search.barcode}" tabindex="1" />
							</li>																																		
							<li>												
								<label><spring:message code="sub-family"/>:</label>
								<!-- float div left -->
								<div class="float-left">
									<form:input path="search.descNm" id="descNm" value="${form.search.descNm}" tabindex="4" />
									<form:input type="hidden" path="search.descId" id="descId" />
								</div>																							
								<!-- clear floats and restore pageflow -->
								<div class="clear-0"></div>											
							</li>
							<li>													
								<label><spring:message code="manufacturer"/>:</label>
								<!-- float div left -->
								<div class="float-left">
									<!-- this div creates a new manufacturer jquery search plugin -->	
									<div class="mfrSearchJQPlugin">
										<input type="hidden" name="fieldId" value="search.mfrId" />
										<input type="hidden" name="fieldName" value="search.mfrNm" />
										<input type="hidden" name="textOnlyOption" value="true" />
										<input type="hidden" name="tabIndex" value="2" />
										<input type="hidden" id="mfrId" name="mfrNamePrefill" value="${form.search.mfrNm}" />
										<input type="hidden" name="mfrIdPrefill" value="${form.search.mfrId}" />
										<!-- manufacturer results appear here -->
									</div>
								</div>																							
								<!-- clear floats and restore pageflow -->
								<div class="clear-0"></div>
							</li>											
							<li>
								<label><spring:message code="model"/>:</label>
								<form:input path="search.model" value="${form.search.model}" tabindex="3" />
							</li>
							<li>
								<label><spring:message code="searchcompinst.orderby"/>:</label>
								<form:select path="sortType">
									<c:forEach var="st" items="${form.sortTypes}">
										<c:choose>
											<c:when test="${form.sortType.displayName == st.displayName}">
												<c:set var="selectedText" value="selected='selected'"/>
											</c:when>
											<c:otherwise>
												<c:set var="selectedText" value=""/>
											</c:otherwise>
										</c:choose>
										<option value="${st}" ${selectedText}>${st.displayName}</option>
									</c:forEach>
								</form:select>
								<form:select path="ascOrder">
									<c:choose>
										<c:when test="${form.ascOrder}">
											<c:set var="selectedText" value="selected='selected'"/>
										</c:when>
										<c:otherwise>
											<c:set var="selectedText" value=""/>
										</c:otherwise>
									</c:choose>
									<option value="true" ${selectedText}><spring:message code="searchcompinst.ascending"/></option>
									<c:choose>
										<c:when test="${!form.ascOrder}">
											<c:set var="selectedText" value="selected='selected'"/>
										</c:when>
										<c:otherwise>
											<c:set var="selectedText" value=""/>
										</c:otherwise>
									</c:choose>
									<option value="false" ${selectedText}><spring:message code="searchcompinst.descending"/></option>
								</form:select> 
							</li>									
							<li>
								<label><input type="button" value="<spring:message code="searchcompinst.Reset"/>" onclick=" event.preventDefault(); window.location.href = 'viewcompanyinstrument.htm?coid=${form.company.coid}'; " /></label>
								<input type="submit" name="submit" value="<spring:message code="search"/>" tabindex="11" />
								<!-- onclick=" $j(this).closest('form').submit(); " -->
							</li>
							
						</ol>
					
					</div>
					<!-- end of left column -->
					
					<!-- column with width 50% (left) -->
					<div class="displaycolumn">
					
						<ol>
						
							<li>
								<label><spring:message code="serialno"/>:</label>
								<form:input path="search.serialNo" value="${form.search.serialNo}" tabindex="5" />
							</li>
							<li>
								<label><spring:message code="plantno"/>:</label>
								<form:input path="search.plantNo" value="${form.search.plantNo}" tabindex="6" />
							</li>	
							<li>
								<label><spring:message code="subdivision"/>:</label>
								<form:select path="selectedSubdivid" id="selectedSubdiv" class="width70" onchange=" event.preventDefault(); getActiveSubdivAddrsAndConts(this.value, $j(this).parent().next().find('select'), $j(this).parent().next().next().find('select'), true, ''); " tabindex="7">
									<option value="" selected="selected"><spring:message code="updatecompinst.searchallsubdiv"/></option>
									<c:forEach var="subdiv" items="${form.sublist}">
										<c:choose>
											<c:when test="${form.subdivid == subdiv.subdivid}">
												<c:set var="selectedText" value="selected='selected'"/>
											</c:when>
											<c:otherwise>
												<c:set var="selectedText" value=""/>
											</c:otherwise>
										</c:choose>
										<option value="${subdiv.subdivid}" ${selectedText} title="${subdiv.subname}">${subdiv.subname}</option>
									</c:forEach>
								</form:select>
							</li>
							<li>
								<label><spring:message code="address"/>:</label>
								<form:select path="addressid" class="width70" tabindex="8">
									<c:choose>
										<c:when test="${not empty form.addlist}">
											<option value="" selected="selected"><spring:message code="updatecompinst.searchalladdresses"/></option>
											<c:forEach var="address" items="${form.addlist}">
												<c:choose>
													<c:when test="${form.addressid == address.addrid}">
														<c:set var="selectedText" value="selected='selected'"/>
													</c:when>
													<c:otherwise>
														<c:set var="selectedText" value=""/>
													</c:otherwise>
												</c:choose>
												<option value="${address.addrid}" ${selectedText}><address:showShortAddress address="${address}" /></option>
											</c:forEach>
										</c:when>
										<c:otherwise>
											<option value="" selected="selected"><spring:message code="updatecompinst.selectsubdivision"/></option>
										</c:otherwise>
									</c:choose>
								</form:select>
							</li>
							<li>
								<label><spring:message code="contact"/>:</label>
								<form:select path="personid" class="width70" tabindex="9">
									<c:choose>
										<c:when test="${not empty form.conlist}">
											<option value="" selected="selected"><spring:message code="updatecompinst.searchallcontacts"/></option>
											<c:forEach var="contact" items="${form.conlist}">
												<c:choose>
													<c:when test="${form.personid == contact.personid}">
														<c:set var="selectedText" value="selected='selected'"/>
													</c:when>
													<c:otherwise>
														<c:set var="selectedText" value=""/>
													</c:otherwise>
												</c:choose>
												<option value="${contact.personid}" ${selectedText} title="${contact.reversedName}">${contact.reversedName}</option>
											</c:forEach>
										</c:when>
										<c:otherwise>
											<option value="" selected="selected"><spring:message code="updatecompinst.selectsubdivision"/></option>
										</c:otherwise>
									</c:choose>									
								</form:select>
							</li>												
							<li>
								<label><spring:message code="searchinstrument.incalibration"/>:</label>
								<form:select path="outOfCal" tabindex="10">
									<option value="" selected="selected"><spring:message code="searchinstrument.either"/></option>
									<c:choose>
										<c:when test="${(not empty form.outOfCal) && !form.outOfCal}">
											<c:set var="selectedText" value="selected='selected'"/>
										</c:when>
										<c:otherwise>
											<c:set var="selectedText" value=""/>
										</c:otherwise>
									</c:choose>										
									<option value="false" ${selectedText}><spring:message code="searchinstrument.incalibrationonly"/></option>
									<c:choose>
										<c:when test="${(not empty form.outOfCal) && form.outOfCal}">
											<c:set var="selectedText" value="selected='selected'"/>
										</c:when>
										<c:otherwise>
											<c:set var="selectedText" value=""/>
										</c:otherwise>
									</c:choose>	
									<option value="true" ${selectedText}><spring:message code="searchinstrument.outofcalibrationonly"/></option>
								</form:select>
							</li>
							
							<li>
								<label><spring:message code="searchcompinst.exportresults"/>:</label>
								<span>
									<a href="instrumentexport.xlsx?coid=${form.company.coid}
										<c:if test='${not empty form.search.plantNo}'>
											&plantNo=${form.search.plantNo}
										</c:if>
										<c:if test='${not empty form.search.serialNo}'>
											&serialNo=${form.search.serialNo}
										</c:if>
										<c:if test='${form.search.descId != null && form.search.descId != 0}'>
											&descId=${form.search.descId}
										</c:if>
										<c:if test='${form.search.mfrId != null && form.search.mfrId != 0}'>
											&mfrId=${form.search.mfrId}
										</c:if>
										<c:if test='${form.personid != null && form.personid != 0}'>
											&personId=${form.personid}
										</c:if>
										<c:if test='${form.subdivid != null && form.subdivid != 0}'>
											&subdivId=${form.subdivid}
										</c:if>
										<c:if test='${form.addressid != null && form.addressid != 0}'>
											&addressId=${form.addressid}
										</c:if>
										<c:if test='${form.outOfCal != null}'>
											&outOfCal=${form.outOfCal}
										</c:if>
										<c:if test='${form.search.barcode != null && form.search.barcode != 0}'>
											&plantId=${form.search.barcode}
										</c:if>
										<c:if test='${not empty form.search.model}'>
											&modelText=${form.search.model}
										</c:if>"
										target="_newtab" class="mainlink" title="<spring:message code="searchcompinst.exportspreadsheet"/>">
										<img src="img/doctypes/excel_small.gif" width="16" height="16" alt="" style=" margin-bottom: -2px; " />&nbsp;&nbsp;<spring:message code="searchcompinst.exportallspreadsheet"/>
									</a>
								</span>
							</li>
						</ol>
					
					</div>
					<!-- end of left column -->
					
				</fieldset>
				
			</div>
			<!-- end of infobox div -->
			
			<c:choose>
				<c:when test="${not empty form.basketIds}">
					<c:set var="basketCount" value="${form.basketIds.size()}"/>
				</c:when>
				<c:otherwise>
					<c:set var="basketCount" value="0"/>
				</c:otherwise>
			</c:choose>
			<!-- sub navigation menu which displays and hides sections of page as well as linking to external pages -->
			<div id="subnav">
				<dl>
					<dt><a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'searchresults-tab', false); " id="searchresults-link" title="<spring:message code="instr.viewsearchresults"/>" class="selected"><spring:message code="instr.searchresults"/></a></dt>
					<dt><a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'searchbasket-tab', false); " id="searchbasket-link" title="<spring:message code="searchcompinst.viewitembasket"/>"><spring:message code="searchcompinst.instrumentbasket"/> (<span class="basketCount">${basketCount}</span>)</a></dt>
				</dl>
			</div>
			<!-- end of sub navigation menu -->
			
			<!-- displays instrument search results -->								
			<div id="searchresults-tab">
			
				<!-- infobox contains company instrument with nifty corners -->	
				<div class="infobox">
											
					<t:paginationControl resultset="${form.search.rs}"/>
					<table class="default4 companyInstResults" summary="<spring:message code="searchcompinst.tablecompanyinstruments"/>">
						<thead>
							<c:if test="${not empty form.search.rs.results}">
								<tr>
									<td colspan="6"><spring:message code="searchinstrumentresults.title"/> (<span class="compInstResultSize">${form.search.rs.results.size()}</span>)</td>
								</tr>
							</c:if>
							<tr>
								<th class="add" scope="col">
									<a href="#" class="addAnchor" onclick=" event.preventDefault(); toggleInstrumentInBasket(null, true, true); " title="<spring:message code="searchcompinst.addallinstrbasket"/>">&nbsp;</a>
									<spring:message code="instr.all"/>
									<a href="#" class="removeBasketAnchor" onclick=" event.preventDefault(); toggleInstrumentInBasket(null, false, true); " title="<spring:message code="searchcompinst.removeallinstrbasket"/>">&nbsp;</a>
								</th>
								<th class="barcode" scope="col"><spring:message code="barcode"/></th>
								<th class="desc" scope="col"><spring:message code="description"/></th>
								<th class="serialno" scope="col"><spring:message code="serialno"/></th>
								<th class="plantno" scope="col"><spring:message code="plantno"/></th>
								<th class="caldue" scope="col"><spring:message code="caldue"/></th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="6">&nbsp;</td>
							</tr>
						</tfoot>
						<tbody>
							<c:choose>
								<c:when test="${not empty form.search.rs.results}">
								<c:forEach var="inst" items="${form.search.rs.results}" varStatus="loopStatus">
									<tr>
										<td class="add">
											<c:set var="inBasket" value="false"/>
											<c:if test="${(not empty form.basketIds) && form.basketIds.contains(inst.plantid)}">
												<c:set var="inBasket" value="true"/>
											</c:if>
											<c:choose>
												<c:when test="${!inBasket}">
													<a href="#" class="addAnchor" id="instAdd${inst.plantid}" onclick=" event.preventDefault(); toggleInstrumentInBasket(this, true, false); " title="<spring:message code="searchcompinst.addinstrbasket"/>">&nbsp;</a>
												</c:when>
												<c:otherwise>
													<a href="#" class="removeBasketAnchor" id="instAdd${inst.plantid}" onclick=" event.preventDefault(); toggleInstrumentInBasket(this, false, false); " title="<spring:message code="searchcompinst.removeinstrbasket"/>">&nbsp;</a>
												</c:otherwise>
											</c:choose>
										</td>
										<td class="barcode"><links:instrumentLinkDWRInfo instrument="${inst}" rowcount="${loopStatus.count}" displayBarcode="true" displayName="false" displayCalTimescale="true" caltypeid="0" /></td>
										<td class="desc"><links:instrumentLinkDWRInfo instrument="${inst}" rowcount="${loopStatus.count}" displayBarcode="false" displayName="true" displayCalTimescale="false" caltypeid="0" /></td>
										<td class="serialno">${inst.serialno}</td>
										<td class="plantno">${inst.plantno}</td>
										<td class="caldue"><instrument:showFormattedCalDueDate instrument="${inst}" /></td>
									</tr>
								</c:forEach>
								</c:when>
								<c:otherwise>
								<tr>
									<td colspan="6" class="bold center">
										<spring:message code="searchinstrumentresults.therearenoresultstodisplay"/>
									</td>
								</tr>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
					<t:paginationControl resultset="${form.search.rs}"/>
					
				</div>
				<!-- end of infobox div -->
			
			</div>
			
			<div id="searchbasket-tab" class="hid">
			
				<div class="infobox">
				
					<!-- link to delete all items in the quote basket -->
					<a href="#" class="mainlink-float" onclick=" event.preventDefault(); deleteFromBasket(null); " id="removelink" title="<spring:message code='searchcompinst.removeallitemsbasket'/>"><spring:message code='searchcompinst.removeallitems' /></a>
															
					<!-- clear floats and restore page flow -->
					<div class="clear"></div>
						
					<table class="default4 companyInstBasket" summary="<spring:message code="searchcompinst.tablebasket"/>">
						<thead>
							<tr>
								<td colspan="6"><spring:message code="searchcompinst.instrumentbasket"/> (<span class="basketCount">${basketCount}</span>)</td>
							</tr>
							<tr>
								<th class="item" scope="col"><spring:message code="item"/></th>
								<th class="barcode" scope="col"><spring:message code="barcode"/></th>
								<th class="inst" scope="col"><spring:message code="instrument"/></th>
								<th class="serial" scope="col"><spring:message code="serialno"/></th>
								<th class="plant" scope="col"><spring:message code="plantno"/></th>
								<th class="remove" scope="col"><spring:message code="instr.remove"/></th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="6">&nbsp;</td>
							</tr>
						</tfoot>
						<c:choose>
							<c:when test="${not empty form.basketIds}">
								<c:forEach var="plantid" items="${form.basketIds}" varStatus="loopStatus">
									<tbody id="basketitem${plantid}">
										<tr>
											<td class="item">${loopStatus.count}</td>
											<td class="barcode">
												<form:input type="hidden" path="basketIds" value="${plantid}"/>${plantid}
											</td>
											<td class="inst">
												<form:input type="hidden" path="basketDefInsts" value="${fn:escapeXml(form.basketDefInsts.get(loopStatus.index))}" />
												${form.basketDefInsts.get(loopStatus.index)}
											</td>						
											<td class="serial">
												<form:input type="hidden" path="basketSerials" value="${fn:escapeXml(form.basketSerials.get(loopStatus.index))}" />
												${form.basketSerials.get(loopStatus.index)}
											</td>													
											<td class="plant">
												<form:input type="hidden" path="basketPlants" value="${fn:escapeXml(form.basketPlants.get(loopStatus.index))}" />
												${form.basketPlants.get(loopStatus.index)}
											</td>															
											<td class="remove">
												<a href="#" onclick=" event.preventDefault(); deleteFromBasket('${plantid}'); " class="deleteAnchor">
													&nbsp;
												</a>
											</td>
										</tr>									
									</tbody>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<tbody>
									<tr>
										<td colspan="8" class="center bold"><spring:message code="searchcompinst.basketempty"/></td>
									</tr>
								</tbody>
							</c:otherwise>
						</c:choose>
					</table>
					
					<input type="hidden" name="submitType" id="submitType" value="" />										
					
					<div class="text-center padtop">
						<div class="displaycolumn text-center">
							<div class="bold marg-bot"><spring:message code="searchcompinst.updateclickbelow"/></div>
							<input type="button" value="<spring:message code="updatecompinst.updateinstruments"/>" onclick=" checkBasketContents(this, 'updatecompinsts'); " />
						</div>
						<div class="displaycolumn text-center">										
							<div class="bold marg-bot"><spring:message code="searchcompinst.createquotation1"/>&nbsp;${form.company.coname}&nbsp;<spring:message code="searchcompinst.createquotation2"/></div>
							<form:select path="quoteContact" tabindex="">													
								<option value="" selected="selected"><spring:message code="searchcompinst.selectcontact"/></option>
								<%-- TODO - used to be displayed in option group by subdiv, refactor in controller to achieve in JSP if needed--%>
								<c:forEach var="contact" items="${form.compconlist}">
									<option value="${contact.personid}" title="${contact.name}">${contact.name}</option>														
								</c:forEach>
							</form:select>
							<input type="button" value="<spring:message code="searchcompinst.createquotation"/>" onclick=" checkBasketContents(this, 'createquoteinsts'); " />
						</div>
						<div class="clear"></div>
					</div>
				
				</div>
																							
			</div>
		
		</form:form>		
		
	</jsp:body>
</t:crocodileTemplate>
