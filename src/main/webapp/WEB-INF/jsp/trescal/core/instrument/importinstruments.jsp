<%-- File name: /trescal/core/instrument/searchinstrument.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>



<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="importinstruments.title" /></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/instrument/ImportInstruments.js'></script>
	</jsp:attribute>
<jsp:body>
	<t:showErrors path="form.*" showFieldErrors="true" showFieldName="false" />
	<div class="infobox">
		<div>
			<a class="mainlink-float" href="importinstrumentsbatch.htm"><spring:message code="importbatch.link" /></a>
			<br/>
		</div>
		<form:form modelAttribute="form" enctype="multipart/form-data" action="importinstruments.htm">
				<fieldset>
					<legend><spring:message code="importinstruments.title" /></legend>
					<!-- displays content in left column -->
					<div class="displaycolumn-40">
						<ol>											
							<li>
								<label><spring:message code="importinstruments.defaultaddress" /> / 
								       <spring:message code="importinstruments.defaultcontact" /> :</label>
								<div id="cascadeSearchPlugin">
									<input type="hidden" id="cascadeRules" value="subdiv,contact,address" />
									<input type="hidden" id="compCoroles" value="client,business" />
									<input type="hidden" id="addressCallBackFunction" value="getExchangeFormatForSelectedCompany" />
								</div>
								<span class="attention" style="margin-left: 50px;" >
								<form:errors path="addrid" /></span>
								<form:errors path="personid" /></span>
								<!-- clear floats and restore page flow -->
								<div class="clear"></div>
							</li>
							<li>
								<label><spring:message code="servicetype" /> :</label>
								<select name="defaultServiceTypeId"
													id="servicetype">
									<c:forEach var="st" items="${ servicesType }">														
										<option value="${st.serviceTypeId}">
											<cwms:besttranslation translations="${st.shortnameTranslation}" />
										</option>
									</c:forEach>
								</select>
							</li>
							<li>
								<label><spring:message code="addjob.recordtype.file.exchangeformat" /> :</label>
								<select id="exchangeformat" name="exchangeFormatId"></select>
							</li>
							<li>
								<label><spring:message code="file" />:</label>	
								<form:input path="file" type="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />									
								<span class="attention"><form:errors path="file" /></span>
								<!-- clear floats and restore page flow -->
								<div class="clear"></div>
							</li>
							<li>
								<label>&nbsp;</label>
								<input type="submit" value="<spring:message code='submit' />" />
							</li>
						</ol>
					</div>
				</fieldset>
			</form:form>
	</div>
	<!-- end of infobox div -->
</jsp:body>
</t:crocodileTemplate>