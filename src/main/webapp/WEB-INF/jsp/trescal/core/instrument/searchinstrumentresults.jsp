<%-- File name: /trescal/core/instrument/searchinstrumentresults.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>

<script type='text/javascript' src='script/trescal/core/instrument/SearchInstrumentResults.js'></script>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="searchinstrumentresults.title"/></span>
	</jsp:attribute>
	<jsp:body>
		<!-- infobox contains all search results and is styled with nifty corners -->
		<div class="infobox">
			<form:form action="" name="searchform" id="searchform" method="post" modelAttribute="command">
				<form:hidden path="coid"/>
				<form:hidden path="personid"/>
				<form:hidden path="subdivid"/>
				<form:hidden path="family"/>
				<form:hidden path="familyid"/>
				<form:hidden path="mfr"/>
				<form:hidden path="mfrid"/>
				<form:hidden path="desc"/>
				<form:hidden path="descid"/>
				<form:hidden path="model"/>
				<form:hidden path="modelid"/>
				<form:hidden path="serialno"/>
				<form:hidden path="plantno"/>
				<form:hidden path="plantid"/>
				<form:hidden path="addedSince"/>
				<form:hidden path="outOfCalibration"/>
				<form:hidden path="standard"/>
				<form:hidden path="hireOnlyInsts"/>
				<form:hidden path="pageNo"/>
				<form:hidden path="resultsPerPage"/>
				<t:paginationControl resultset="${command.rs}"/>
				<table class="default4 instrumentsearchresults" summary="<spring:message code='searchinstrumentresults.tableallinstrumentsearchresults'/>">
					<thead>
						<tr>
							<td colspan="4">
								<spring:message code="searchinstrumentresults.title"/>
								(${command.instruments.size()})
							</td>
						</tr>
						<tr>
							<th class="barcode" scope="col"><spring:message code="barcode"/></th>
							<th class="model" scope="col"><spring:message code="searchinstrumentresults.modelname"/></th>
							<th class="serialno" scope="col"><spring:message code="serialno"/></th>
							<th class="plantno" scope="col"><spring:message code="plantno"/></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="4">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody>
						<c:set var="rowcount" value="0"/>
						<c:choose>
							<c:when test="${command.instruments.size() == 0}">
								<tr>
									<td colspan="4" class="bold center">
										<spring:message code="searchinstrumentresults.therearenoresultstodisplay"/>
									</td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:forEach var="instrument" items="${command.instruments}">
									<tr>
										<td class="barcode">
											<links:InstrumentLink rowcount="${rowcount}" displayBarcode="true" displayName="false" displayCalTimescale="true"
                                                                  calDueDate="${instrument.calDueDate}" calInterval="${instrument.calibrationInterval}"
                                                                  calIntervalUnit="${instrument.calibrationIntervalUnit}"
                                                                  isCalStandard="${instrument.isCalStandard}" isScrapped="${instrument.scrapped}"
                                                                  plantId="${instrument.plantId}" modelName="${instrument.modelName}"
																  manufacturerFromInstrument="${instrument.instMfr}" modelFromInstrument="${instrument.instModel}"
                                                                  modelIsGeneric="${instrument.modelMfrType == 'MFR_GENERIC'}"/>
										</td>
										<c:set var="rowcount" value="${rowcount + 1}"/>
										<td class="model">
                                            <links:InstrumentLink rowcount="${rowcount}" displayBarcode="false" displayName="true" displayCalTimescale="false"
                                                                  calDueDate="${instrument.calDueDate}" calInterval="${instrument.calibrationInterval}"
                                                                  calIntervalUnit="${instrument.calibrationIntervalUnit}"
                                                                  isCalStandard="${instrument.isCalStandard}" isScrapped="${instrument.scrapped}"
                                                                  plantId="${instrument.plantId}" modelName="${instrument.modelName}"
                                                                  manufacturerFromInstrument="${instrument.instMfr}" modelFromInstrument="${instrument.instModel}"
                                                                  modelIsGeneric="${instrument.modelMfrType == 'MFR_GENERIC'}"/>
                                             </br>
												<span style="font-style: italic; font-size: 11px;">  ${instrument.customerDescription}  </span>
										</td>
										<c:set var="rowcount" value="${rowcount + 1}"/>
										<td class="serialno">${instrument.serialNo}</td>
										<td class="plantno">${instrument.plantNo}</td>
									</tr>			
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
				<t:paginationControl resultset="${command.rs}"/>
			</form:form>
		</div>
		<!-- end of infobox div -->
	</jsp:body>
</t:crocodileTemplate>