<%-- File name: /trescal/core/instrument/editcomplementaryfields.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<t:crocodileTemplate>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='../script/trescal/core/instrument/EditComplementaryFields.js'></script>
	</jsp:attribute>
    <jsp:attribute name="header">
		<span class="headtext">
			<spring:message code="viewinstrument.complementaryedit"/>
		</span>
	</jsp:attribute>
    <jsp:body>
		<t:showErrors path="command.*" showFieldErrors="true" />

        <div class="infobox">

        	<form:form modelAttribute="command">
				<fieldset>
					<legend><spring:message code="viewinstrument.complementaryedit"/></legend>
					<ol>
						<li>
							<label><spring:message code="viewinstrument.accreditation"/>:</label>
							<form:input path="complementaryField.accreditation" size="30" maxlength="255"/>
							<form:errors path="complementaryField.accreditation" class="error"/>
						</li>
						<li>
							<label><spring:message code="viewinstrument.deliverystatus"/>:</label>
							<form:input path="complementaryField.deliveryStatus" size="30"  maxlength="255"/>
							<form:errors path="complementaryField.deliveryStatus" class="error"/>
						</li>
						<li>
							<label><spring:message code="viewinstrument.firstusedate"/>:</label>
							<form:input path="complementaryField.firstUseDate" type="date" size="30" placeholder="DD.MM.YYYY"/>
							<form:errors path="complementaryField.firstUseDate" class="error"/>
						</li>
						<li>
							<label><spring:message code="viewinstrument.purchasecost"/>:</label>
							<form:input path="complementaryField.purchaseCost" size="30"/>
							<form:errors path="complementaryField.purchaseCost" class="error"/>
						</li>
						<li>
							<label><spring:message code="viewinstrument.purchasedate" />:</label>
							<form:input path="complementaryField.purchaseDate" type="date" size="30" placeholder="DD.MM.YYYY"/>
							<form:errors path="complementaryField.purchaseDate" class="error"/>
						</li>
						<li>
							<label><spring:message code="viewinstrument.formerbarcode"/>:</label>
							<form:input path="complementaryField.formerBarCode" size="30" maxlength="255"/>
							<form:errors path="complementaryField.formerBarCode" class="error"/>
						</li>
						<li>
							<label><spring:message code="viewinstrument.formerfamily"/>:</label>
							<form:input path="complementaryField.formerFamily" size="30" maxlength="255"/>
							<form:errors path="complementaryField.formerFamily" class="error"/>
						</li>
						<li>
							<label><spring:message code="viewinstrument.formercharacteristics"/>:</label>
							<form:input path="complementaryField.formerCharacteristics" size="30" maxlength="255"/>
							<form:errors path="complementaryField.formerCharacteristics" class="error"/>
						</li>
						<li>
							<label><spring:message code="viewinstrument.formerlocaldescription"/>:</label>
							<form:input path="complementaryField.formerLocalDescription" size="30" maxlength="255"/>
							<form:errors path="complementaryField.formerLocalDescription" class="error"/>
						</li>
						<li>
							<label><spring:message code="viewinstrument.formermanufacturer"/>:</label>
							<form:input path="complementaryField.formerManufacturer" size="30" maxlength="255"/>
							<form:errors path="complementaryField.formerManufacturer" class="error"/>
						</li>
						<li>
							<label><spring:message code="viewinstrument.formermodel"/>:</label>
							<form:input path="complementaryField.formerModel" size="30" maxlength="255"/>
							<form:errors path="complementaryField.formerModel" class="error"/>
						</li>
						<li>
							<label><spring:message code="viewinstrument.formername"/>:</label>
							<form:input path="complementaryField.formerName" size="30" maxlength="255"/>
							<form:errors path="complementaryField.formerName" class="error"/>
						</li>
						<li>
							<label><spring:message code="viewinstrument.provider"/>:</label>
							<form:input path="complementaryField.provider" size="30" maxlength="255"/>
							<form:errors path="complementaryField.provider" class="error"/>
						</li>
						<li>
							<label><spring:message code="viewinstrument.remarks"/></label>
							<form:input path="complementaryField.clientRemarks" size="30" maxlength="255"/>
							<form:errors path="complementaryField.clientRemarks" class="error"/>
						</li>
						<c:if test="${hasdeviation}">
							<li>
								<label><spring:message code="viewinstrument.deviationunit"/></label>
								<form:select path="unitId" items="${units}" itemLabel="value" itemValue="key"/>
							</li>
							<li>
								<label><spring:message code="viewinstrument.nominalvalue"/></label>
								<form:input path="complementaryField.nominalValue" size="30" />
								<form:errors path="complementaryField.nominalValue" class="error"/>
							</li>
						</c:if>
						<c:if test="${hascertclass}">
							<li>
								<label><spring:message code="certificate.class"/></label>
                                <form:select path="certClass" items="${certclasses}" itemLabel="value" itemValue="key"/>
							</li>
						</c:if>
						<c:if test="${hasverificationstatus}">
							<li>
								<label><spring:message code="instrument.customeracceptancecriteria"/></label>
								<form:input path="complementaryField.customerAcceptanceCriteria"/>
							</li>
							<li>
                                <label><spring:message code="instrument.certvalidationstatusoverride"/></label>
                                <form:select path="complementaryField.calibrationStatusOverride">
                                    <form:option value="" label="None"/>
                                    <form:options items="${verificationStatuses}" itemValue="key" itemLabel="value" />
                                </form:select>
							</li>
						</c:if>
						<li>
							<label>&nbsp;</label>
							<input type="submit" name="save" id="submit" value="<spring:message code='update'/>" />
						</li>
					</ol>
	            </fieldset>
            </form:form>
        </div>
		<script>

		</script>
    </jsp:body>
</t:crocodileTemplate>