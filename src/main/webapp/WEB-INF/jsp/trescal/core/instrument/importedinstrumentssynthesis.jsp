<%-- File name: /trescal/core/jobs/job/jobsearch.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="link" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="importinstruments.title" /></span>
		<style>
			.max-wrap {
				max-width : 120px;
				display : block;
			}
		</style>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='module' src='script/components/search-plugins/cwms-capability-search/cwms-capability-search.js'></script>
		<script type='text/javascript' src='script/trescal/core/instrument/ImportedInstrumentsSynthesis.js'></script>
	</jsp:attribute>
<jsp:body>
	
<form:form modelAttribute="form" id="importedIntrumentsForm" >
	<div class="infobox">
		<fieldset>
			<legend><spring:message code="defaultvalue"/></legend>
			<!-- displays content in left column -->
			<div class="displaycolumn">
				<ol>											
					<li>
						<label><spring:message code="company" />:</label>
						<link:companyLinkDWRInfo company="${ company }" rowcount="1" copy="true"/>
					</li>
					<li>
						<label><spring:message code="subdiv" />:</label>
						<link:subdivLinkDWRInfo rowcount="1" subdiv="${ subdiv }"/>
						<form:hidden path="subdivId" />
					</li>
					<li>
						<label><spring:message code="importinstruments.defaultcontact" />:</label>
						<link:contactLinkDWRInfo contact="${ contact }" rowcount="0" />
						<form:hidden path="defaultContactId" />
					</li>
				</ol>
			</div>
			<div class="displaycolumn">
				<ol>	
					<li>
						<label><spring:message code="importinstruments.defaultaddress" />:</label>
						${ address.addressLine }
					</li>
					<form:hidden path="defaultAddressId" />
					
					<li>
						<label><spring:message code="editinstr.defaultservicetype" />:</label>
                        <cwms:besttranslation translations="${ serviceType.shortnameTranslation }"/>
					</li>
					<form:hidden path="defaultServiceTypeId" />
				</ol>
			</div>
		</fieldset>
	</div>
	
	<div class="infobox">
		<a href="importinstruments.htm" class="mainlink-float">
			<spring:message code="importcalibrations.importagain" />
		</a>
		<table class="default2">										
			<thead>
				<tr>
					<td colspan="10">${ form.rows.size() }</td>
				</tr>
				<tr>
					<th rowspan="2" >#</th>
					<th><spring:message code="plantno" /></th>
					<th><spring:message code="manufacturer" /></th>
					<th><spring:message code="family" /></th>
					<th><spring:message code="instrumentmodel" /></th>
					<th><spring:message code="editinstr.nextcaldue" /></th>
					<th rowspan="2"><spring:message code="exchangeformat.fieldname.instrumentusagetype" /></th>
					<th rowspan="2"><spring:message code="editinstr.defaultcapability" /></th>
					<th><spring:message code="contact" /></th>
					<th><spring:message code="editinstr.defaultservicetype" /></th>
				</tr>
				<tr>
					<th><spring:message code="serialno" /></th>
					<th><spring:message code="model" /></th>
					<th><spring:message code="sub-family" /></th>
					<th><spring:message code="description" /></th>
					<th><spring:message code="editinstr.calfrequency" /></th>
					<th><spring:message code="address" /></th>
					<th><spring:message code="instr.defaultClientRef" /></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${ form.rows }" var="inst" varStatus="status">
					<tr>
						<td rowspan="2" >
							${ status.index + 1 }</br>
							<form:errors class="error max-wrap" path="rows[${ status.index }]"/>
						</td>
						
						<td>
							${ inst.plantno }
							<div style="color:darkorange;">
								<c:forEach var="warning" items="${ inst.warningMessages }" >
									<c:out value="${ warning }"/>
								</c:forEach>
							</div>
							<form:errors class="error max-wrap" path="rows[${ status.index }].plantno"/>
						</td>
						<form:hidden path="rows[${ status.index }].plantno"/>
						
						<td>${ inst.brand }<form:errors class="error max-wrap" path="rows[${ status.index }].brand"/></td>
						<form:hidden path="rows[${ status.index }].brand"/>
						
						<td><span style="text-decoration: underline;">(${ inst.familyId })</span> ${inst.familyName }
						<form:errors class="error max-wrap" path="rows[${ status.index }].familyName"/>
						<form:errors class="error max-wrap" path="rows[${ status.index }].familyId"/></td>
						<form:hidden path="rows[${ status.index }].familyName"/>
						<form:hidden path="rows[${ status.index }].familyId"/>
						<td>${ inst.instrumentModelId }
							<c:set var="instrumentModelIdErrors"><form:errors class="error max-wrap" path="rows[${ status.index }].instrumentModelId"/></c:set>
							<c:if test="${ empty instrumentModelIdErrors and not empty inst.instrumentModelId }">
								&nbsp;<a target="_blank" href="instrumentmodel.htm?modelid=${ inst.instrumentModelId }">(${ inst.instrumentModelId })</a>
							</c:if>
							<form:errors class="error max-wrap" path="rows[${ status.index }].instrumentModelId"/>
						</td>
						<form:hidden path="rows[${ status.index }].instrumentModelId"/>
						
						<td>
							<fmt:formatDate value="${ inst.nextCalDueDate }" type="date" dateStyle="SHORT"/>
							<form:errors class="error max-wrap" path="rows[${ status.index }].nextCalDueDate"/>
						</td>
						<form:hidden path="rows[${ status.index }].nextCalDueDate"/>
						
						<td rowspan="2">
							<c:out value="${ inst.instrumentUsageType }"/>
							<form:errors class="error max-wrap" path="rows[${ status.index }].instrumentUsageTypeId"/>
						</td>
						<form:hidden path="rows[${ status.index }].instrumentUsageType"/>
						<form:hidden path="rows[${ status.index }].instrumentUsageTypeId"/>
						
						<td rowspan="2">
						<cwms-capability-search-form 
						name="rows[${ status.index }].defaultProcedureId"
						procid="${inst.defaultProcedureId}" defaultvalue="${inst.defaultProcedureReference}"></cwms-capability-search-form>
							<form:errors class="error max-wrap" path="rows[${ status.index }].defaultProcedureReference"/>
						</td>
						<form:hidden path="rows[${ status.index }].defaultProcedureReference"/>
						
						<td>${ inst.contact }<form:errors class="error max-wrap" path="rows[${ status.index }].contact"/></td>
						<form:hidden path="rows[${ status.index }].contact"/>
						<form:hidden path="rows[${ status.index }].contactId"/>
						
						<td>
							<form:select path="rows[${ status.index }].defaultServiceTypeId"  >
								<form:option value="">&nbsp;</form:option>
	    						<form:options items="${serviceTypes}" itemValue="key" itemLabel="value"  />
							</form:select>
							<form:errors class="error max-wrap" path="rows[${ status.index }].serviceType"/>
						</td>
						<form:hidden path="rows[${ status.index }].serviceType"/>
						
					</tr>
					<tr>
						<td>${ inst.serialno }<form:errors class="error max-wrap" path="rows[${ status.index }].serialno"/></td>
						<form:hidden path="rows[${ status.index }].serialno"/>
						
						<td>${ inst.model }<form:errors class="error max-wrap" path="rows[${ status.index }].model"/></td>
						<form:hidden path="rows[${ status.index }].model"/>
						
						<td><span style="text-decoration: underline;">(${ inst.subFamilyId })</span> ${inst.subFamilyName }
						<form:errors class="error max-wrap" path="rows[${ status.index }].subFamilyName"/>
						<form:errors class="error max-wrap" path="rows[${ status.index }].subFamilyId"/></td>
						<form:hidden path="rows[${ status.index }].subFamilyName"/>
						<form:hidden path="rows[${ status.index }].subFamilyId"/>
						
						<td>${ inst.description }<form:errors class="error max-wrap" path="rows[${ status.index }].description"/></td>
						<form:hidden path="rows[${ status.index }].description"/>
						
						<td>
							${ inst.calFrequency }&nbsp;${ inst.intervalUnit }
							<form:errors class="error max-wrap" path="rows[${ status.index }].calFrequency"/>
							<form:errors class="error max-wrap" path="rows[${ status.index }].intervalUnit"/>
						</td>
						<form:hidden path="rows[${ status.index }].calFrequency"/>
						<form:hidden path="rows[${ status.index }].intervalUnit"/>

						<td>${ inst.address }<form:errors class="error max-wrap" path="rows[${ status.index }].address"/></td>
						<form:hidden path="rows[${ status.index }].address"/>
						<form:hidden path="rows[${ status.index }].addressId"/>
						
						<td> ${inst.clientRef} <form:errors class="error max-wrap" path="rows[${ status.index }].clientRef"/> </td>
						<form:hidden path="rows[${ status.index }].clientRef"/>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<center>
			<input type="submit" name="newanalysis" class="button"
									value="<spring:message code='filesynthesis.buttonnewanalysis'/>" />
			<input type="submit" name="save" class="button" value="<spring:message code='submit' />" />
		</center>
		<form:hidden path="exchangeFormatId"/>
	</div>
</form:form>
</jsp:body>
</t:crocodileTemplate>
