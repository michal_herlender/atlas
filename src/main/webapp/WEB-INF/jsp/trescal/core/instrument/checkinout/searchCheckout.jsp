<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<spring:message code="instrument.checkoutbrower" />     
		</span>
	</jsp:attribute>

	<jsp:attribute name="scriptPart">
		<script
			src='script/trescal/core/instrument/checkinout/searchCheckout.js'></script>
	</jsp:attribute>

	<jsp:body>	
		<div id="searchCheckout" class="infobox">
				<fieldset>
					<legend> <spring:message code="instrument.checkoutbrower" />    </legend>
					<form:form method="post" modelAttribute="command">
						<ol>
							<li>
								<label><spring:message code="barcode" /> : </label>
								<form:input path="barcode" />
							</li>
							<li>
								<label> <spring:message code="jobno"/>  : </label>
								<form:input path="jobno" />
							</li>
							<li>
								<label><spring:message code="instrument.checkoutdate"/> : </label>
								<select id="checkoutdateselector" onchange="hidselector(this.value,'checkoutdateSpan');">
									<option value="on"> <spring:message code="instrument.checkouton"/></option>
									<option value="between"> <spring:message code="instrument.checkoutbetween"/></option>   
								</select>
								<form:input path="checkoutdate1" type="date" class="date"  />
								<span id="checkoutdateSpan" class="hid" >
									<form:input path="checkoutdate2" type="date" class="date"  />   
								</span>
							</li>
							<li>
								<label><spring:message code="instrument.checkindate"/> : </label>
								<select id="checkindateselector"  onchange="hidselector(this.value,'checkindateSpan');">
									<option value="on"> <spring:message code="instrument.checkouton"/></option>
									<option value="between"> <spring:message code="instrument.checkoutbetween"/></option>
								</select>
								
								<form:input path="checkindate1" type="date" class="date" />
								<span id="checkindateSpan" class="hid">
									<form:input path="checkindate2" type="date" class="date"  />
								</span>
							</li>
							<li>
								<label><spring:message code="instrument.checkoutcomplet"/> : </label>   
								<select name="checkoutcomplet" id="checkoutcomplet">
									<option value="true"> <spring:message code="instrument.checkoutyes"/> </option>   
									<option value="false">  <spring:message code="instrument.checkoutno"/> </option>   
								</select>
							</li>
							<li>
								<label for="submit">&nbsp;</label>
								<input type="submit" value="<spring:message code='submit'/>" name="submit" id="submit" tabindex="20" />
							</li>
						</ol>
					</form:form>
				</fieldset>
			<div class="clear">&nbsp;</div>
		</div>
		
		
		
	</jsp:body>


</t:crocodileTemplate>

