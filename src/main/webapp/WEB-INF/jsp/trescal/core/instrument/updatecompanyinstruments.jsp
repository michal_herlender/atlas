<%-- File name: /trescal/core/instrument/updatecompanyinstruments.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="instrument" tagdir="/WEB-INF/tags/instrument" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
       <span class="headtext"><spring:message code="updatecompinst.title"/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/instrument/UpdateCompanyInstruments.js'></script>
    </jsp:attribute>
    <jsp:body>
    	<t:showErrors path="form.*" showFieldErrors="false" />
		
		<c:if test="${not empty message}">
			<div class="successBox1">
				<div class="successBox2">
					<div class="successBox3">
						<div class="text-center">${message}</div>
					</div>
				</div>
			</div>
		</c:if>
														
		<!-- infobox contains company instrument search form elements and is styled with nifty corners -->	
		<div class="infobox" id="search">
		
			<form:form modelAttribute="form" method="post" action="" onsubmit=" $j(this).find('input:last').attr('disabled', 'disabled'); ">
			
				<fieldset class="updateCompanyInstruments">
					
					<legend><spring:message code="updatecompinst.title"/></legend>
																							
					<ol class="instSearchColumn">
						
						<li>
							<fieldset>
								<legend>
                                 <spring:message code="updatecompinst.selected" arguments="${insts.size()}"/>
                                 <c:out value="     ( " />
                                 <a href="viewcompanyinstrument.htm?coid=${instrumentCompany.coid}" class="mainlink">
	                                 <spring:message code="updatecompinst.backtoinstrumentsearch"/>
                                 </a>
                                 <c:out value=" ) " />
								</legend>
							</fieldset>
						</li>
						<li>
							<label><spring:message code="updatecompinst.currentcompany"/>:</label>
							<cwms:securedLink permission="COMPANY_VIEW" classAttr="mainlink" parameter="?coid=${instrumentCompany.coid}">
								<c:out value="${instrumentCompany.coname}"/>
							</cwms:securedLink>
						</li>
						<li class="bold">
							<spring:message code="updatecompinst.selectcontact"/>
						</li>										
						<li>
							<label><spring:message code="company"/>:</label>
							<div id="changecompany" class="float-left widePluginInput hid">
								<!-- this div creates a new company search plugin  -->
								<div class="compSearchJQPlugin">
									<input type="hidden" name="compNamePrefill" value="${comp.coname}" />
									<input type="hidden" name="compIdPrefill" value="${comp.coid}" />
									<input type="hidden" name="field" value="companyid" />
									<input type="hidden" name="compCoroles" value="client,prospect,business" />
									<input type="hidden" name="tabIndex" value="1" />
									<input type="hidden" name="triggerMethod" value="true" />
								</div>
							</div>
							<div id="showcompany" class="vis">
								<c:out value="${comp.coname} "/>
								(<a href="" class="mainlink" onclick=" event.preventDefault(); $j('#showcompany').addClass('hid').removeClass('vis'); $j('#changecompany').addClass('vis').removeClass('hid'); ">
	                        		<spring:message code="editinstr.change"/>
                            	</a>)
							</div>
							<form:errors path="companyid" class="attention" />
							<!--  clear floats and restore page flow  -->
							<div class="clear"></div>
						</li>
						<li>
							<label><spring:message code="subdivision"/>:</label>
							<form:select id="subdivid" path="subdivid" items="${subdivList}" itemLabel="value" itemValue="key" 
								onchange=" getActiveSubdivAddrsAndConts(this.value, $j('#addressid'), $j('#personid'), true, ''); " />
							<form:errors path="subdivid" class="attention" />
						</li>
						<li>
							<label><spring:message code="address"/>:</label>
							<form:select id="addressid" path="addressid" items="${addressList}" itemLabel="value" itemValue="key" />
							<form:errors path="addressid" class="attention" />
						</li>
						<li>
							<label><spring:message code="contact"/>:</label>
							<form:select id="personid" path="personid" items="${contactList}" itemLabel="value" itemValue="key" />
							<form:errors path="personid" class="attention" />
						</li>
						<li>
							<label>&nbsp;</label>
							<input type="submit" name="submit" value="<spring:message code="updatecompinst.updateinstruments"/>" />
						</li>							
					
					</ol>
					
				</fieldset>
				
			</form:form>
			
		</div>
		<!-- end of infobox div -->
		
		<!-- infobox contains company instrument with nifty corners -->	
		<div class="infobox">
									
			<table class="default4 updateCompanyInsts" summary="<spring:message code="updatecompinst.tableinstupdate"/>">
				<thead>
					<c:if test="${not empty insts}">
						<tr>
							<td colspan="7"><spring:message code="updatecompinst.selected" arguments="${insts.size()}" /></td>
						</tr>
					</c:if>
					<tr>											
						<th class="barcode" scope="col"><spring:message code="barcode"/></th>
						<th class="desc" scope="col"><spring:message code="description"/></th>
						<th class="serialno" scope="col"><spring:message code="serialno"/></th>
						<th class="plantno" scope="col"><spring:message code="plantno"/></th>
						<th class="contact" scope="col"><spring:message code="contact"/></th>
						<th class="address" scope="col"><spring:message code="address"/></th>
						<th class="subdiv" scope="col"><spring:message code="subdivision"/></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="7">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
					<c:set var="rowcount" value="1"/>
					<c:choose>
						<c:when test="${insts != null && insts.size() > 0}">
						 <c:forEach var="inst" items="${insts}">
							<tr>													
								<td class="barcode"><a href="viewinstrument.htm?plantid=${inst.plantid}" class="mainlink" >${inst.plantid}</a></td>
								<td class="desc">${inst.definitiveInstrument}</td>
								<td class="serialno">${inst.serialno}</td>
								<td class="plantno">${inst.plantno}</td>
								<td class="contact">${inst.con.name}</td>
								<td class="address">${inst.add.addr1}, ${inst.add.town}</td>
								<td class="subdiv">${inst.con.sub.subname}</td>
							</tr>
							<c:set var="rowcount" value="${rowcount + 1}"/>
						 </c:forEach>
					    </c:when>
						<c:otherwise>												
						<tr>
							<td colspan="7" class="bold center">
								<spring:message code="updatecompinst.noinstrumentsupdated"/>
							</td>
						</tr>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
			
		</div>
		<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>