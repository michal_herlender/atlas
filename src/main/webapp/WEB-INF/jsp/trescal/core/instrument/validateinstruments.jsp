<%-- File name: /trescal/core/instrument/searchinstrument.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">Validate Imported Instruments</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script >
			var jqCalendar = new Array ();
			<c:forEach var="inst" items="${form.importedInstrumentsList}" varStatus="st" >
				jqCalendar.push({ inputField: 'importedInstrumentsList${st.index}\\.importedNextcaldate',
					 dateFormat: 'dd/mm/yy',
					  displayDates: 'all'
				   });
			</c:forEach>
		</script>
		<style>
			fieldset {
				background:none;
			}
			label {
				margin:0px !important;
				min-width:95px;
			}
			.minWidth110 {
				min-width:120px;
			}
			.centerText {
				text-align: center;
			}
			.spacer {
				 height:2px;
			}
			.attention {
				 display: grid;
			}
		</style>
	</jsp:attribute>
<jsp:body>
	<div class="infobox">
		<div class="clear-0"></div>
		<form:form method="post" action="validateinstruments.htm" modelAttribute="form" >
			<fieldset>
				<table class="default2" >
					<thead>
						<tr>
							<td colspan="7">
								size: X
							</td>
						</tr>
						<tr>
							<td colspan="7">
								Company: Y
							</td>
						</tr>
						<tr>
							<th style="width:280px;" >Instrument</th>
							<th style="width:260px;" >Family</th>
							<th style="width:290px;" >Calibration Frequency</th>
							<th style="width:290px;">Service type</th>
							<th style="width:260px;">Contact</th>
							<th class="">Match Level</th>
							<th class=""></th>
						</tr>
					</thead>
					<tbody>
					
						<c:forEach items="${form.importedInstrumentsList}" var="inst" varStatus="st" >
							<spring:bind path="form.importedInstrumentsList[${st.index}]" >
								<tr instrumentIndex="${st.index}" >
									<td>
										
										<fieldset>
											<li>
												<label>Serial No:</label>
												<form:input path="${status.expression}.importedSerialNo" />
												<form:errors delimiter=", " path="${status.expression}.importedSerialNo" cssClass="attention" />
											</li>								
											<li>
												<label>Plant No:</label>
												<form:input path="${status.expression}.importedPlantNo" />
												<form:errors delimiter=", " path="${status.expression}.importedPlantNo" cssClass="attention" />
											</li>								
											<li>
												<label>Model:</label>
												<form:input path="${status.expression}.importedModel" />
											</li>								
											<li>
												<label>Brand:</label>
												<form:input path="${status.expression}.importedManufacturer" />
											</li>								
										</fieldset>
									</td>
									<td>
										<fieldset>
											<li><label>Family:</label><input value="" ></li>								
											<li><label>Sub Family:</label><input value="" ></li>								
											<li><label>Domain:</label><input value="" ></li>								
										</fieldset>
									</td>
									<td>
										<fieldset>
											<li>
												<label class="minWidth110" >Frequency:</label>
												<form:input path="${status.expression}.frequency" />
											</li>								
											<li>
												<label class="minWidth110" >Frequency Unit:</label>
												<form:select path="${status.expression}.calFrequencyUnit" items="${units}" itemLabel="value" itemValue="key"/>
											</li>								
											<li>
												<label class="minWidth110" >Next Calibration date:</label>
												<fmt:formatDate value="${status.value.importedNextcaldate}" type="date" dateStyle="SHORT" />
												<form:input cssStyle="width:145px;" cssClass="date" readonly="true"
													 path="${status.expression}.importedNextcaldate" value="${dateString}" />
											</li>								
										</fieldset>
									</td>
									<td>
										<fieldset>
											<li>
												<label class="minWidth110" >Service Type:*</label>
												<form:select style="max-width:145px;" id="defaultServiceType" path="${status.expression}.defaultServiceTypeId"  >
					    							<form:options items="${serviceTypes}" itemValue="key" itemLabel="value"  />
												</form:select>
												<form:errors path="${status.expression}.defaultServiceTypeId"/>
											</li>								
											<li>
												<label class="minWidth110" >Usage Type:</label>
												<form:select style="max-width:145px;" path="${status.expression}.usageTypeId">
													<form:option value="">&nbsp;</form:option>
													<c:forEach var="type" items="${usageTypes}">
														<form:option value="${type.id}">
															<cwms:besttranslation translations="${type.nametranslation}"/>
														</form:option>
													</c:forEach>
												</form:select>
												<form:errors path="${status.expression}.usageTypeId"/>
											</li>								
											<li>
												<label class="minWidth110" >Customer Managed:</label>
												<form:checkbox path="${status.expression}.customerManaged"/>
											</li>								
										</fieldset>
									</td>
									<td>
										<fieldset>
											<li>
												<label>Contact:*</label>
												<form:select path="${status.expression}.contactId" style="width:145px;" >
													<form:options items="${contacts}" itemValue="key" itemLabel="value" />	
												</form:select>
											</li>								
											<li>
												<label>Address:*</label>
												<form:select path="${status.expression}.addressId" style="width:145px;" >
													<form:options items="${addresses}" itemValue="key" itemLabel="value" />	
												</form:select>
											</li>	
											<li><label>Description:</label>
												<form:textarea path="${status.expression}.importedDescription" rows="4" />
											</li>								
										</fieldset>
									</td>
									<td class="centerText"> High </td>
									<td class="centerText"> 
										<button>Validate</button>
										<button>Delete</button>
									</td>
								</tr>
								<tr class="spacer"></tr>
							</spring:bind>
						</c:forEach>
					</tbody>
				</table>
			</fieldset>
			<input type="submit" value="Submit"/>
		</form:form>
	</div>
	<!-- end of infobox div -->
</jsp:body>
</t:crocodileTemplate>