<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="importinstruments.title" /></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/instrument/BatchImportInstrument.js'></script>
	</jsp:attribute>
	<jsp:body>
		<c:if test="${not empty jobId }">
			<div class="successBox1">
				<div class="successBox2">
					<div class="successBox3 center-0">
						<spring:message code="calibrationimportation.job.submitted.successfuly" />
							&nbsp;<a style="float: none;padding: 0px;" class="mainlink-float" target="_blank" 
									href="bgtasks.htm?personid=${personid}&jobname=INSTRUMENT_IMPORT_JOB">${ jobId }</a>	
					</div>
				</div>
			</div>
		</c:if>
		<t:showErrors path="form.*" showFieldErrors="true" showFieldName="false" />
	<div class="infobox">
		<div class="clear-0"></div>
		<form:form modelAttribute="form" enctype="multipart/form-data" action="importinstrumentsbatch.htm">
				<fieldset>
					<legend><spring:message code="importinstruments.title" /></legend>
					<!-- displays content in left column -->
					<div class="displaycolumn-50">
						<ol>											
							<li>
								<label><spring:message code="importinstruments.defaultaddress" /> / 
								       <spring:message code="importinstruments.defaultcontact" /> :</label>
								<div id="cascadeSearchPlugin">
									<input type="hidden" id="cascadeRules" value="subdiv,contact,address" />
									<input type="hidden" id="compCoroles" value="client,business" />
									<input type="hidden" id="addressCallBackFunction" value="companySelected" />
								</div>
								<span class="attention" style="margin-left: 50px;" >
								<form:errors path="addrid" /></span>
								<form:errors path="personid" /></span>
								<!-- clear floats and restore page flow -->
								<div class="clear"></div>
							</li>
							<li>
								<label><spring:message code="servicetype" /> :</label>
								<form:select path="defaultServiceTypeId" id="servicetype">
									<form:options items="${serviceTypes}" itemValue="key" itemLabel="value"  />
								</form:select>
							</li>
							<li>
								<label><spring:message code="addjob.recordtype.file.exchangeformat" /> :</label>
								<select id="exchangeformat" name="exchangeFormatId"></select>
							</li>
							<li>
								<label><spring:message code="importcalibrations.autosubmitpolicy" /> :</label>			
								<select name="autoSubmitPolicy" id="autosubmitpolicy">
									<c:forEach var="i" items="${ autoSubmitPolicies }">														
										<option value="${i.key}"> ${i.value} </option>
									</c:forEach>
								</select>		
							</li>
							<li>
								<label><spring:message code="file" />:</label>	
								<form:input path="file" type="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />									
								<span class="attention"><form:errors path="file" /></span>
								<!-- clear floats and restore page flow -->
								<div class="clear"></div>
							</li>
							<li>
								<label>&nbsp;</label>
								<input type="submit" value="<spring:message code='submit' />" />
							</li>
						</ol>
					</div>
				</fieldset>
			</form:form>
	</div>
	<!-- end of infobox div -->
	</jsp:body>
		
	
</t:crocodileTemplate>