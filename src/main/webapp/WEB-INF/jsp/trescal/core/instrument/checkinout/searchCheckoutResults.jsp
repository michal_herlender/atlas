<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<spring:message code="instrument.checkoutbrower" />     
		</span>
	</jsp:attribute>

	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/instrument/checkinout/searchCheckout.js'></script>
		<script src="script/trescal/core/instrument/confidenceCheck/confidenceCheckOverlay.js"></script>
	</jsp:attribute>
	
	
	<jsp:body>
		<div id="ListsOfCheckedOutInstruments" class="infobox">
			<table id="checkOutInstruments" class="default2">
					<thead>
						<tr>
							<th colspan="6">
								<div class="float-left">
									<spring:message code="instrument.checkedoutInstr"/> (${checkoutInsts.size()})
								</div>
								<a href="exportcheckout.htm?barcode=${command.barcode}&jobno=${command.jobno}&checkoutdate1=${command.checkoutdate1}&checkoutdate2=${command.checkoutdate2}&checkindate1=${command.checkindate1}&checkindate2=${command.checkindate2}&checkoutcomplet=${command.checkoutcomplet}" id="checkInLink" class="mainlink-float checkInLink ">Export results</a>
 							</th>
						</tr>
						  <tr>
							  <th class="centre" scope="col"><spring:message code="barcode"/></th>
							  <th class="centre" scope="col"><spring:message code="instmodelname"/></th>
							  <th class="centre" scope="col"><spring:message code="instrument.checkoutdate"/></th>
							  <th class="centre" scope="col"><spring:message code="instrument.checkindate"/></th>
							  <th class="centre" scope="col"><spring:message code="jobno"/> </th>
							  <th class="centre" scope="col"><spring:message code="instrument.confidencecheck"/></th>
							  
						  </tr>				
					</thead>
					<tbody>
						<c:set var="rowcount" value="0"/>
							<c:forEach var="element" items="${checkoutInsts}">
								<tr>
									<td>	
										<links:showInstrumentLink instrument="${element.instrument}"/>
									</td>
									 <td class="center">
									 	 <cwms:showinstrument instrument="${element.instrument}" />
									 </td>
									 <td>	
									 		${element.checkOutDate}
									 </td>
									 <td>
									  		${element.checkInDate}
									 </td>
									 <td>
										 <c:if test="${element.job != null}">
										 	<links:jobnoLinkDWRInfo rowcount="${rowcount}" jobno="${element.job.jobno}" jobid="${element.job.jobid}" copy="${false}"/>
										 </c:if>
									 </td>
									 <td>
									 	<c:if test="${element.confidenceChecks.size() > 0}">
									 		<a href="#" id="checkInLink" class="mainlink" onclick="displayConfidenceCheckOverlay(${element.id},'view');"><spring:message code="instrument.confidencecheck"/></a>
									 	</c:if>
									 </td>

									 <c:set var="rowcount" value="${rowcount + 1}"/>
								</tr>
							</c:forEach>
					</tbody>
				</table>
			<div class="clear">&nbsp;</div>
		</div>
	</jsp:body>
	
</t:crocodileTemplate>