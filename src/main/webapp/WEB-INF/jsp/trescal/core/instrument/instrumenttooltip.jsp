<%-- File name: /trescal/core/instrument/instrumenttooltip.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>

<div id="instrumentTT">
	<!-- main navigation bar -->
	<div class="slatenav-main">
		<ul>
			<li>
				<a id="tti_instrum-link" class="selected"
					onmouseover="event.preventDefault(); switchMenuFocus(Elements1, 'tti_instrum-tab', false);">
					<spring:message code="instrumenttooltip.instrumentinfo" text="Instrument Info"/>
				</a>
			</li>
		</ul>
	</div>
	<div id="tti_instrum-tab" class="vis clearL">
		<!-- instrument sub navigation bar -->
		<div class="slatenav-sub">
			<ul>
				<li>
					<a id="tti_summary-link" class="selected"
						onmouseover="event.preventDefault(); switchMenuFocus(Elements2, 'tti_summary-tab', false);">
						<spring:message code="summary"/>
					</a>
				</li>
				<c:if test="${useThreads}">
					<li>
						<a id="tti_thread-link"
							onmouseover="event.preventDefault(); switchMenuFocus(Elements2, 'tti_thread-tab', false);">
							<spring:message code="editinstr.thread"/>
						</a>
					</li>
				</c:if>						
				<li>
					<a id="tti_jobs-link"
						onmouseover="event.preventDefault(); switchMenuFocus(Elements2, 'tti_jobs-tab', false);">
						<spring:message code="jobs"/>
						(${instrument.jobItems.size()})
					</a>
				</li>
				<li>
					<a id="tti_certs-link"
						onmouseover="event.preventDefault(); switchMenuFocus(Elements2, 'tti_certs-tab', false);">
						<spring:message code="jicalibration.certs"/>
						(${certLength})
					</a>
				</li>
				<li>
					<a id="tti_notes-link"
						onmouseover="event.preventDefault(); switchMenuFocus(Elements2, 'tti_notes-tab', false);">
						<spring:message code="notes"/>
						(${instrument.notes.size()})
					</a>
				</li>
			</ul>
		</div>
		<!-- instrument summary for sub tab index -->
		<div id="tti_summary-tab">
			<fieldset class="newtooltip">					
				<ol id="instrumentTooltip">
					<li>
						<label><spring:message code="barcode"/>:</label>
						<span>${instrument.plantid}</span>
					</li>
					<li>
						<label><spring:message code="instmodelname"/>:</label>
						<div class="fleftTooltip"> 
							<cwms:showmodel instrumentmodel="${instrument.model}"/>
						</div>
						<div class="clear-0"></div>
					</li>
					<li> 
						<label><spring:message code="viewinstrument.customerdescription"/></label>
						<span>${instrument.customerDescription}</span>
					</li>
					<c:if test="${instrument.model.modelMfrType == 'MFR_GENERIC'}">
						<li>
							<label><spring:message code="mfr"/>:</label>
							<span>${instrument.mfr.name}</span>
						</li>
						<li>
							<label><spring:message code="model"/>:</label>
							<span>${instrument.modelname}</span>
						</li>
						<li>
							<label><spring:message code="editinstr.defaultservicetype"/>:</label>
							<span>
								<cwms:besttranslation translations="${instrument.defaultServiceType.shortnameTranslation}"/>
								-
								<cwms:besttranslation translations="${instrument.defaultServiceType.longnameTranslation}"/>
							</span>
						</li>
					</c:if>
					<li>
						<label><spring:message code="company"/></label>
						<div class="fleftTooltip">
							${instrument.comp.coname} 
						</div>
						<div class="clear-0"></div>
					</li>
					<li>
						<label><spring:message code="owner"/></label>
						<span>${instrument.con.name}</span>
					</li>
					<li>
						<label><spring:message code="viewinstrument.location"/></label>
						<span>${instrument.add.addr1}, ${instrument.add.town}</span>
					</li>
					<li>
						<label><spring:message code="plantno"/></label>
						<span>${instrument.plantno}</span>
					</li>
					<li>
						<label><spring:message code="serialno"/></label>
						<span>${instrument.serialno}</span>
					</li>
					<li>
						<label><spring:message code="instrumenttooltip.calfreq" text="Cal Freq"/></label>
						<span>${instrument.calFrequency}&nbsp;${instrument.calFrequencyUnit.getName(instrument.calFrequency)}</span>
					</li>
					<li>
						<label><spring:message code="instrumenttooltip.nextcal" text="Next Cal"/></label>
						<span>
							<c:choose>
								<c:when test="${instrument.nextCalDueDate == null}">
									-- <spring:message code="instrumenttooltip.notset"/> --
								</c:when>
								<c:otherwise>
									<fmt:formatDate value="${instrument.nextCalDueDate}" type="date" dateStyle="SHORT"/>
								</c:otherwise>
							</c:choose>
						</span>
					</li>
				</ol>
			</fieldset>
		</div>
		<!-- end of instrument summary -->
		<c:if test="${useThreads}">
			<!-- instrument thread summary for sub tab index -->
			<div id="tti_thread-tab" class="hid">
				<fieldset class="newtooltip">
					<ol>
						<li>
							<label><spring:message code="instrumenttooltip.size"/></label>
							<span>
								<c:choose>
									<c:when test="${instrument.thread == null}">
										-
									</c:when>
									<c:otherwise>
										${instrument.thread.size}
									</c:otherwise>
								</c:choose>
							</span>
						</li>
						<li>
							<label><spring:message code="type"/></label>
							<span>
								<c:choose>
									<c:when test="${instrument.thread == null}">
										-
									</c:when>
									<c:otherwise>
										${instrument.thread.type.type}
									</c:otherwise>
								</c:choose>
							</span>
						</li>
						<li>
							<label><spring:message code="thread.wire"/></label>
							<span>
								<c:choose>
									<c:when test="${instrument.thread == null}">
										-
									</c:when>
									<c:otherwise>
										${instrument.thread.wire.designation}
									</c:otherwise>
								</c:choose>
							</span>
						</li>
						<li>
							<label><spring:message code="cylindricalstandard.cylindricalstandard"/></label>
							<span>
								<c:choose>
									<c:when test="${instrument.thread == null}">
										-
									</c:when>
									<c:otherwise>
										${instrument.thread.cylindricalStandard.id}
									</c:otherwise>
								</c:choose>
							</span>
						</li>
						<li>
							<label><spring:message code="thread.threaduom"/></label>
							<span>
								<c:choose>
									<c:when test="${instrument.thread == null}">
										-
									</c:when>
									<c:otherwise>
										${instrument.thread.uom.description}
									</c:otherwise>
								</c:choose>
							</span>
						</li>
					</ol>
				</fieldset>
			</div>
			<!-- end of instrument thread summary -->
		</c:if>
		<!-- instrument jobs tab -->
		<div id="tti_jobs-tab" class="hid">
			<table class="default2 instTooltipJob" summary="This table displays job information for this instrument">
				<thead>
					<tr>
						<td colspan="10">
							<spring:message code="jobs"/> (${instrument.jobItems.size()})
						</td>
					</tr>
					<tr>
						<th class="tti_jobno" scope="col">
							<spring:message code="jobno"/>
						</th>
						<th class="tti_item" scope="col">
							<spring:message code="item"/>
						</th>
						<th class="tti_caltype" scope="col">
							<spring:message code="caltype"/>
						</th>
						<th class="tti_turn" scope="col">
							<spring:message code="instrumenttooltip.turn"/>
						</th>
						<th class="tti_datein" scope="col">
							<spring:message code="instrumenttooltip.datein"/>
						</th>
						<th class="tti_datecomp" scope="col">
							<spring:message code="instrumenttooltip.datecomp"/>
						</th>
						<th class="tti_caljobreviewcost" scope="col">
							<spring:message code="viewinstrument.caljobreviewcost"/>
						</th>
						<th class="tti_calcost" scope="col">
							<spring:message code="viewinstrument.calcost"/>
						</th>
						<th class="tti_calinvoicedcost" scope="col">
							<spring:message code="viewinstrument.calinvoicedcost"/>
						</th>
						<th class="tti_repcost" scope="col">
							<spring:message code="instrumenttooltip.repcost"/>
						</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="10">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
					<c:choose>
						<c:when test="${empty instrument.jobItems}">
							<tr>
								<td colspan="10" class="bold center">
									<spring:message code="instrumenttooltip.instrhasnotappeared"/>
								</td>
							</tr>
						</c:when>
						<c:otherwise>
							<c:forEach var="ji" items="${instrument.jobItems}" varStatus="loopStatus">
								<tr style="background-color: ${ji.calType.serviceType.displayColour}">
									<td class="tti_jobno">
										<a href="viewjob.htm?jobid=${ji.job.jobid}" target="_blank" class="mainlink">
											${ji.job.jobno}
										</a>
									</td>
									<td class="tti_item">
										<a href="${ji.defaultPage}?jobitemid=${ji.jobItemId}" target="_blank" class="mainlink">
											${ji.itemNo}
										</a>
									</td>
									<td class="tti_caltype">
										${ji.calType.serviceType.shortName}
									</td>
									<td class="tti_turn">
										${ji.turn}
									</td>
									<td class="tti_datein">
										<c:choose>
											<c:when test="${ji.dateIn == null}">
												-- <spring:message code="instrumenttooltip.novalue"/> --
											</c:when>
											<c:otherwise>
												<fmt:formatDate value="${ji.dateIn}" type="date" dateStyle="SHORT"/>
											</c:otherwise>
										</c:choose>
									</td>
									<td class="tti_datecomp">
										<c:choose>
											<c:when test="${ji.dateComplete == null}">
												-- <spring:message code="instrumenttooltip.novalue"/> --
											</c:when>
											<c:otherwise>
												<fmt:formatDate value="${ji.dateComplete}" type="date" dateStyle="SHORT"/>
											</c:otherwise>
										</c:choose>
									</td>
									<c:set var="caljobreviewcost" value=""/>
									<c:set var="calcost" value=""/>
									<c:set var="calinvoicedcost" value=""/>
									<c:set var="repcost" value=""/>
									<c:set var="jobCostId" value=""/>
									<c:set var="currencySym" value=""/>
									<c:forEach var="jobCostingItem" items="${ji.jobCostingItems}" varStatus="jcLoop">
										<c:if test="${jcLoop.first}">
											<c:set var="jobCostId" value="${jobCostingItem.jobCosting.id}"/>
											<c:set var="currencySym" value="${jobCostingItem.jobCosting.currency.currencyERSymbol}"/>
											<c:if test="${jobCostingItem.calibrationCost != null}">
												<c:set var="calcost" value="${jobCostingItem.calibrationCost.finalCost}"/>
											</c:if>
											<c:if test="${jobCostingItem.repairCost != null}">
												<c:set var="repcost" value="${jobCostingItem.repairCost.finalCost}"/>
											</c:if>
										</c:if>
									</c:forEach>
									<!-- display calibration job review cost and calibration invoiced cost
                    				 only for the last 2 jobs-->
                    				 <c:set var="last2jobs" value="0"/>
                    				 <c:if test="${instrument.jobItems.size() >	2}">
                    				 	<c:set var="last2jobs" value="${instrument.jobItems.size() - 2}"/>
                    				 </c:if>
									<c:if test="${loopStatus.index >= last2jobs }">
										<c:set var="caljobreviewcost" value="${ji.calibrationCost.finalCost}"/>
										<c:set var="firsttime" value="false"/>
										<c:forEach var="invoiceitem" items="${ji.invoiceItems}">
						   					<c:if test="${invoiceitem.invoice.type.name == 'Calibration' && !firsttime}">
						   						<c:set var="firsttime" value="true" />
						   						<c:if test="${invoiceitem.calibrationCost != null}">
													<c:set var="calinvoicedcost" value="${invoiceitem.calibrationCost.finalCost}"/>
												</c:if>		   		
						   					</c:if>
										</c:forEach>
									</c:if>
									<td class="tti_caljobreviewcost">
										<c:if test="${caljobreviewcost != ''}">
											<c:choose>
												<c:when test='${jobCostId != "" }'>
													<a href="viewjobcosting.htm?id=${jobCostId}" class="mainlink" title="View Job Costing">${currencySym}${caljobreviewcost}</a>
												</c:when>
												<c:otherwise>
									   				${ji.job.currency.currencyERSymbol}${caljobreviewcost}
												</c:otherwise>
											</c:choose>
										</c:if>
									</td>
									<td class="tti_calcost">
										<c:if test="${calcost != ''}">
											<a href="viewjobcosting.htm?id=${jobCostId}" class="mainlink" title="View Job Costing">
												${currencySym} ${calcost}
											</a>
										</c:if>
									</td>
									<td class="tti_calinvoicedcost">
										<c:if test="${calinvoicedcost != ''}">
											<c:choose>
												<c:when test='${jobCostId != "" }'>
													<a href="viewjobcosting.htm?id=${jobCostId}" class="mainlink" title="View Job Costing">${currencySym}${calinvoicedcost}</a>
												</c:when>
												<c:otherwise>
									   				${ji.job.currency.currencyERSymbol}${calinvoicedcost}
												</c:otherwise>
											</c:choose>
										</c:if>
									</td>
									<td class="tti_repcost">
										<c:if test="${repcost != ''}">
											<a href="viewjobcosting.htm?id=${jobCostId}" class="mainlink" title="View Job Costing">
												${currencySym} ${repcost}
											</a>
										</c:if>
									</td>
								</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
		</div>
		<!-- end of instrument jobs tab -->
		<!-- instrument certs tab -->
		<div id="tti_certs-tab" class="hid">
			<table class="default2" id="instTooltipCert" summary="This table displays certificate information for this instrument">
				<thead>
					<tr>
						<td colspan="5">
							<spring:message code="instrumenttooltip.certs"/>
						</td>
					</tr>
					<tr>
						<th class="tti_cert" scope="col">
							<spring:message code="viewinstrument.certno"/>
						</th>
						<th class="tti_job" scope="col">
							<spring:message code="jobno"/>
						</th>
						<th class="tti_item" scope="col">
							<spring:message code="itemno"/>
						</th>
						<th class="tti_caldate" scope="col">
							<spring:message code="caldate"/>
						</th>
						<th class="tti_certdate" scope="col">
							<spring:message code="viewinstrument.certdate"/>
						</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="5">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
					<c:forEach var="jobItem" items="${instrument.jobItems}">
						<c:forEach var="certLink" items="${jobItem.certLinks}">
							<tr>
								<td>
									<c:choose>
										<c:when test="${certLink.certFilePath == null}">
											${certLink.cert.certno}
										</c:when>
										<c:otherwise>
											<img src="img/doctypes/pdf-small.gif" width="10" height="10" class="image_inl" alt=""/>
											<a href="file://${certLink.certFilePath}" target="_blank" class="mainlink">
												${certLink.cert.certno}
											</a>
										</c:otherwise>
									</c:choose>
								</td>
								<td>
									<a href="viewjob.htm?jobid=${jobItem.job.jobid}" target="_blank" class="mainlink">
										${jobItem.job.jobno}
									</a>
								</td>
								<td class="center">
									<a href="jicertificates.htm?jobitemid=${jobItem.jobItemId}" target="_blank" class="mainlink">
										${jobItem.itemNo}
									</a>
								</td>
								<td class="center">
									<fmt:formatDate value="${certLink.cert.calDate}" type="date" dateStyle="SHORT"/>
								</td>
								<td class="center">
									<fmt:formatDate value="${certLink.cert.certDate}" type="date" dateStyle="SHORT"/>
								</td>
							</tr>
						</c:forEach>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<!-- end of instrument certs tab -->
		<!-- instrument notes tab -->
		<div id="tti_notes-tab" class="hid">
			<table class="default2" id="instTooltipNotes" summary="This table displays all notes for this instrument">
				<thead>
					<tr>
						<td>
							<spring:message code="notes"/> (${instrument.notes.size()})
						</td>
					</tr>
					<tr>
						<th>
							<spring:message code="instrumenttooltip.labelnote"/>
						</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
					<c:choose>
						<c:when test="${empty instrument.notes}">
							<tr>
								<td class="bold center">
									<spring:message code="instrumenttooltip.nonotes"/>
								</td>
							</tr>
						</c:when>
						<c:otherwise>
							<c:forEach var="note" items="${instrument.notes}">
								<tr>
									<td>
										<span class="bold">${note.label}</span><br />
										${note.note}
									</td>
								</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
		</div>
		<!-- end of instrument notes tab -->
	</div>
	<!-- end of instrument tab div -->
</div>
<!-- end of main div -->