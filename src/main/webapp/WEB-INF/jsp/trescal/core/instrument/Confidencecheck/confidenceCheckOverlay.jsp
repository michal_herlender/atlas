<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>


<t:overlayTemplate bodywidth="780">
	<jsp:attribute name="scriptPart">
		<script src="script/trescal/core/instrument/confidenceCheck/confidenceCheckOverlay.js"></script>
	</jsp:attribute>

	<jsp:body>
		<div id="confidenceCheckOverlay">
		<c:if test="${ not empty confCheck}">
			<div class="successBox1">
				<div class="successBox2">
					<div class="successBox3 center-0">
						<spring:message code="instrument.confidencechecksuccessful" /> 
					</div>
				</div>
			</div>
		</c:if>
		
		<form:form method="post" modelAttribute="command">
			<fieldset>
				<input type="hidden" name="checkoutId"  id="checkoutId" value="${checkoutid }" />
				<ol>
					<li>
						<label><spring:message code="barcode"/> : </label>
						<label> <links:showInstrumentLink instrument="${checkOut.instrument}"/>  </label>
					</li>
					
					<li>
						<label><spring:message code="instmodelname"/>  : </label>
						<label> <cwms:showinstrument instrument="${checkOut.instrument}" />  </label>
					</li>
					
					<c:if test="${checkOut.job != null}">
						<li>
							<label><spring:message code="jobno"/> :</label>
							<label> <links:jobnoLinkDWRInfo rowcount="0" jobno="${checkOut.job.jobno}" jobid="${checkOut.job.jobid}" copy="${true}"/> </label>
						</li>
					</c:if>
					
					<li>
						<label><spring:message code="instrument.checkoutdate"/>  : </label>
						<label> ${checkOut.checkOutDate}  </label>
					</li>
					
					
					<c:if test="${checkOut.checkOutComplet == true}">
						<li>
							<label><spring:message code="instrument.checkindate"/>  : </label> 
							<label> ${checkOut.checkInDate}  </label>
						</li>
					</c:if>
					
					<li> 
						<label><spring:message code="instrument.checkedoutby"/>  : </label>
						<label>${checkOut.technicien.getReversedName()}  </label>
					</li>
					
					<li>
						<label><spring:message code="instrument.confidencecheckresult"/>  : </label>
						<c:set var="size" value="${checkOut.confidenceChecks.size()}"/>
						<select name="result">
							<c:forEach items="${confidenceCheckResults }" var="rs">
								<option value="${rs}" ${ size>0 && checkOut.confidenceChecks.get(size-1).result eq rs ? 'selected="selected"' : ''  }> ${rs.description}  </option>
							</c:forEach>
						</select>
					</li>
					
					<li>
						<label><spring:message code="instrument.confidencecheckdescription"/>  : </label>   
						<form:textarea path="description" cols="80" rows="2" />
					</li>
					
					<li>
						<c:if test="${action == 'add' }">
							<input type="submit" id="jiwr_submit" value="<spring:message code='submit'/>" name="submit" id="submit" />
							&nbsp;
						</c:if>
						
						
						<input type="button" id="jiwr_close" value="<spring:message code='cancel' />" onclick="event.preventDefault(); window.parent.loadScript.closeOverlay(); " />
					</li>
				</ol>
			</fieldset>
		</form:form>
		</div>
	</jsp:body>
</t:overlayTemplate>
