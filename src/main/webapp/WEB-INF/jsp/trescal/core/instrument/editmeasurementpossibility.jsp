<%-- File name: /trescal/core/instrument/editmeasurementpossibility.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<t:crocodileTemplate>
    <jsp:attribute name="header">
		<span class="headtext">
			<spring:message code="viewinstrument.measurementpossibility.instrument"/>&nbsp;${measurementpossibility.instrument.getPlantid()}
		</span>
	</jsp:attribute>
    <jsp:body>
        <div class="infobox">
        	<form:form method="post">
        		<form:errors path="*">
            		<div class="warningBox1">
                     		<div class="warningBox2">
                          		<div class="warningBox3">
                                	<h5 class="center-0 attention">
                                    	<spring:message code="error.characteristic" />
                                	</h5>
                                	<c:forEach var="e" items="${messages}">
                                	    <div class="center attention"><c:out value="${e}"/></div>
                            	    </c:forEach>
                        	   </div>
                    	   </div>
                	 </div>
            	</form:errors>
				<fieldset>
	            	<ol>
	                	<li>
	                		<label><spring:message code="viewinstrument.name"/></label>
	                		<input name="name" value="${measurementpossibility.name}" size="50"/>
	                	</li>
	                	<li>
	                		<label><spring:message code="viewinstrument.minimum"/></label>
	                		<input name="minimum" value="${measurementpossibility.minimum}" size="50"/>
	                	</li>
	                	<li>
	                		<label><spring:message code="viewinstrument.maximum"/></label>
	                		<input name="maximum" value="${measurementpossibility.maximum}" size="50"/>
	                	</li>
	                	<li>
	                		<label><spring:message code="viewinstrument.scalederiva"/></label>
	                		<input name="scalederiva" value="${measurementpossibility.scalederiva}" size="50"/>
	                	</li>
	                	<li>
	                		<label><spring:message code="viewinstrument.quality"/></label>
	                		<input name="quality" value="${measurementpossibility.quality}" size="50"/>
	                	</li>
	                	<li>
	                		<label><spring:message code="viewinstrument.lmax"/></label>
	                		<input name="lmax" value="${measurementpossibility.lmax}" size="50"/>
	                	</li>
	                	<li>
                            <input type="submit" name="save" id="submit" value="<spring:message code='save'/>" />
                            <c:if test="${measurementpossibility.measurementpossibilityid > 0}">
                            	<input type="submit" name="delete" id="delete" value="<spring:message code='viewinstrument.deletemeasurementpossibility'/>" /> 
                            </c:if>
                       	</li>
	                </ol>
	            </fieldset>
            </form:form>
        </div>
    </jsp:body>
</t:crocodileTemplate>