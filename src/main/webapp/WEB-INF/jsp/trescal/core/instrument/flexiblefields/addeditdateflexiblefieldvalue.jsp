<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="instrument.editflexiblefield"/></span>
	</jsp:attribute>
	<jsp:body>
		<t:showErrors path="command.*" showFieldErrors="true"/>
		<form:form>
			<form:hidden path="valueId"/>
			<form:hidden path="fieldDefinitionId"/>
			<form:hidden path="plantId"/>
			<div class="infobox">
				<fieldset>
					<ol>
						<li>
							<label>${command.fieldName}</label>
							<form:input path="dateValue" id="dateValue"/>
						</li>
					</ol>
					<input type="submit" name="update" value='<spring:message code="update"/>'/>
					<c:if test="${command.valueId > 0}">
						<input type="submit" name="delete" value='<spring:message code="delete"/>'/>
					</c:if>
				</fieldset>
			</div>
		</form:form>
		<script>
									
		var jqCalendar = new Array ( { 
										 inputField: 'dateValue',
										 dateFormat: 'dd.mm.yy',
										 displayDates: 'all',
										 showWeekends: true
									 } );
		
		
		</script>
	</jsp:body>
</t:crocodileTemplate>