<%-- File name: /trescal/core/instrument/AddInstrumentModelSearch.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<script type="text/javascript"
	src="script/trescal/core/instrument/AddInstrumentSearch.js"></script>

<t:crocodileTemplate>
	<jsp:attribute name="header">
        <span class="headtext"><spring:message
				code="addinstrument.modelsearch.title" /></span>
    </jsp:attribute>
	<jsp:body>
        
        <div class="infobox">
            <a href='editmodel.htm' class="mainlink-float"><spring:message
					code="searchmod.addnewmodel" /></a><br>
            <form:form action="" method="POST" id="searchmodelform"
				modelAttribute="addinstrumentmodelsearchform">
                <fieldset>
                    <legend>
						<spring:message code="addinstrument.modelsearch.findmodel" />
					</legend>               
                    <ol>
                        <li>
                            <label><spring:message code="domain" />:</label>
                            <!-- float div left -->
                            <div class="float-left">
                                <form:input path="domainNm"
									name="domainNm" tabindex="1" />
                                <form:input path="domainId"
									type="hidden" name="domainId" />                             
                            </div>
                            <div class="clear-0"></div> 
                        </li>       
                        <li>
                            <label><spring:message code="family" />:</label>
                            <!-- float div left -->
                            <div class="float-left">
                                <form:input path="familyNm"
									name="familyNm" tabindex="2" />
                                <form:input path="familyId"
									type="hidden" name="familyId" />                             
                            </div>
                            <div class="clear-0"></div> 
                        </li>   
                        <li>
                            <label><spring:message
									code="sub-family" />:</label>
                            <!-- float div left -->
                            <div class="float-left">
                                <form:input path="descNm" name="descNm"
									tabindex="3" />
                                <form:input path="descId" type="hidden" />                       
                            </div>
                            <div class="clear-0"></div> 
                        </li>
                        
                        <li id="manufacturer">
                            <label><spring:message
									code="manufacturer" />:</label>
                            <!-- float div left -->
                            <div class="float-left">
                                <form:input path="mfrNm" name="mfrNm"
									tabindex="3" />
                                <form:input path="mfrId" type="hidden" />                       
                            </div>                                                                                          
                            <!-- clear floats and restore pageflow -->
                            <div class="clear-0"></div>
                        </li>
                        <li id="model">
                            <label><spring:message code="model" />:</label>                          
                            <form:input path="model" tabindex="5" />                                         
                        </li>
                        
                        <li>
                            <label><spring:message
									code="salescategory" />:</label>
                            <!-- float div left -->
                            <div class="float-left">
                                <form:input path="salesCat"
									name="salesCat" tabindex="6" />
                                <form:input path="salesCatId"
									type="hidden" name="salesCatId" />                             
                            </div>
                            <div class="clear-0"></div> 
                        </li>  
                        
                        <li>
                            <label><spring:message code="quotsort.sortby" />:</label>

                            <form:radiobutton path="sortBy" style="width:20px;" value="" checked="checked"/><spring:message code="instmodelname"/>
                            <form:radiobutton path="sortBy" style="width:20px;" value="Brand"/><spring:message code="manufacturer"/>
                            <form:radiobutton path="sortBy" style="width:20px;" value="model"/><spring:message code="model"/>
                            <div class="clear-0"></div> 
                        </li> 
                                  
                        <li>
                            <label>&nbsp;</label>
                            <input type="submit" name="search"
							id="submit" value="<spring:message code="search"/>" tabindex="4" />
                        </li>                   
                    </ol>           
                </fieldset>
            </form:form>
    
        </div>
            
    </jsp:body>
</t:crocodileTemplate>