<%-- File name: /trescal/core/instrument/searchinstrument.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:crocodileTemplate>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/instrument/SearchInstrument.js'></script>
	</jsp:attribute>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="searchinstrument.title"/></span>
	</jsp:attribute>
	<jsp:body>
		<!-- infobox contains all form elements relating to searching for an instrument and is styled with nifty corners -->				
		<div class="infobox">
			<form:form  method="post" action="" id="searchinstrumentform" modelAttribute="command"
				onsubmit="$j('input#submit').attr('disabled', 'disabled');" autocomplete="off">
				<form:errors path="*">
       				<div class="warningBox1">
              		 <div class="warningBox2">
              	       <div class="warningBox3">
                    	<h5 class="center-0 attention"><spring:message code="searchinstrument.errorwhilesearch"/>:</h5>
                      	<c:forEach var="e" items="${messages}">
                        	<div class="center attention"><c:out value="${e}"/></div>
                      	</c:forEach>
                     </div>
                  </div>
                 </div>
              </form:errors>
			  <fieldset id="instrumentSearch">
					<legend><spring:message code="searchinstrument.title"/></legend>
					<!-- this is where the cascading search is displayed should the user wish to use it -->
					<ol id="cascadingSearch" class="hid">
						<li>
							<label><spring:message code="searchinstrument.cascadeselector"/>:</label>
							<!-- cascading search is created here -->
							&nbsp;&nbsp;&nbsp;
							<!-- clear floats and restore pageflow -->
							<div class="clear-0"></div>
						</li>
					</ol>
					<!-- clear floats and restore pageflow -->
					<div class="clear-0"></div>
					<!-- displays content in column 69% wide (left) -->
					<div class="displaycolumn-65">
						<ol>
							<li id="defaultCompany">
								<label>
									<spring:message code="company"/>:
									<a href="#" class="mainlink" onclick="event.preventDefault(); toggleCascade(true);">
										<spring:message code="searchinstrument.casc"/>
									</a>
								</label>
								<div class="float-left extendPluginInput">
									<div class="compSearchJQPlugin">
										<!-- company search results displayed here -->
										<input type="hidden" name="field" value="coid" />
										<input type="hidden" name="compCoroles" value="client,business" />
										<input type="hidden" name="tabIndex" value="1" />
									</div>
								</div>
								<div class="float-left padtop marg-left">
									<a href="#" class="mainlink"
										onclick="
											if($j('input[name=coid]').val() != ''){
												window.location.href = 'viewcompanyinstrument.htm?coid=' + $j('input[name=coid]').val()
											} else {
												alert('Please choose a company before clicking this link')
											};
											return false;">
										<spring:message code="searchinstrument.companyinstrumentsearch"/>
									</a>
	                            </div>
								<!-- clear floats and restore pageflow -->
								<div class="clear-0"></div>
							</li>	
							<li id="defaultContact">
								<label><spring:message code="contact"/>:</label>
								<!-- this div creates a new contact search plugin and the hidden input passes
									 a comma separated list of coroles to search over, no input field causes
									 searches over client corole as default -->
								<div id="contSearchPlugin">
									<input type="hidden" id="contCoroles" value="client,business" />
									<input type="hidden" id="contindex" value="2" />
								</div>&nbsp;
								<!-- clear floats and restore pageflow -->
								<div class="clear-0"></div>
							</li>
							
							<li>
								<label><spring:message code="family"/>:</label>
								<!-- float div left -->
								<div class="float-left">
									<form:input path="family" id="family" tabindex="5"/>
									<form:hidden path="familyid" />
								</div>																							
								<!-- clear floats and restore pageflow -->
								<div class="clear-0"></div>											
							</li>
							<li>
								<label><spring:message code="sub-family"/>:</label>
								<!-- float div left -->
								<div class="float-left">
									<form:input path="desc" id="desc" tabindex="6"/>
									<form:hidden path="descid" />
								</div>																							
								<!-- clear floats and restore pageflow -->
								<div class="clear-0"></div>											
							</li>
							<li>
                                <label><spring:message code="manufacturer"/>:</label>
                                <!-- float div left -->
                                <div class="float-left">
									<form:input path="mfr" id="mfr" tabindex="6"/>
									<form:hidden path="mfrid" />
								</div>		                                                                                      
                                <!-- clear floats and restore pageflow -->
                                <div class="clear-0"></div>
                            </li>
                            <li>
                                <label><spring:message code="model"/>:</label>
                                <span>
                                    <form:input path="model" tabindex="8"/>
                                </span>
                            </li>
                            <li>
								<label><spring:message code="editinstr.customerdescription"/>:</label>
								<span>
									<form:input path="customerDescription" tabindex="9"/>
								</span>
							</li>
							<li>
								<label>&nbsp;</label>
								<input type="submit" name="submit" id="submit" value="<spring:message code='search'/>" tabindex="12" />
							</li>							
						</ol>
					</div>
					<!-- end of left column -->
					<!-- displays content in column 39% wide (right) -->
					<div class="displaycolumn-35">
						<ol>							
							<li>
								<label><spring:message code="barcode"/>:</label>
								<span>
									<form:input path="plantid" tabindex="9"/>
								</span>
							</li>
							<li>
								<label><spring:message code="serialno"/>:</label>
								<span>
									<form:input path="serialno" tabindex="10"/>
								</span>
							</li>
							<li>
								<label><spring:message code="plantno"/>:</label>
								<span>
									<form:input path="plantno" tabindex="11"/>
								</span>
							</li>
						
							<li>
                                <label><spring:message code="searchinstrument.standard"/>:</label>
                                <form:select path="standard" tabindex="7">
                                    <option value="" selected="selected"><spring:message code="searchinstrument.either"/></option>
                                   	<option value="true"><spring:message code="searchinstrument.standardsonly"/></option>
                                    <option value="false"><spring:message code="searchinstrument.nonstandardsonly"/></option>
                                </form:select>
                            </li>
                            
							<li>
								<label><spring:message code="searchinstrument.incalibration"/>:</label>
								<form:select path="outOfCalibration" tabindex="12">
									<option value="" selected="selected"><spring:message code="searchinstrument.either"/></option>
									<option value="false"><spring:message code="searchinstrument.incalibrationonly"/></option>
									<option value="true"><spring:message code="searchinstrument.outofcalibrationonly"/></option>
								</form:select>
							</li>
						</ol>			
					</div>
					<!-- end of right column -->
					<form:hidden path="isExport" value="false"/>
				</fieldset>
			</form:form>
		</div>
		<!-- end of infobox div -->
	</jsp:body>
</t:crocodileTemplate>