<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<spring:message code="instrument.checkin" />
			&nbsp;/&nbsp;
			<spring:message code="instrument.checkout" />
		</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/instrument/checkinout/CheckInOut.js'></script>
		<script src="script/trescal/core/instrument/confidenceCheck/confidenceCheckOverlay.js"></script>
	</jsp:attribute>
	<jsp:body>		

		<div id="scanItemsInOut" class="infobox">
				<fieldset>
					<legend><spring:message code="instrument.checkout"/></legend>
					<ol>
						<li>
							<label><spring:message code="barcode"/> : </label>
							<input id="barcode" type="text" onkeypress="if(event.keyCode==13) {performCheckOut(this.value,$j('#jobNo').val());}" />					
						</li>
						
						<li>
							<label><spring:message code="jobno"/> : </label>
							<input id="jobNo" type="text" name="jobNo" onkeypress="if(event.keyCode==13) {performCheckOut($j('#barcode').val(),this.value);}"  />					
						</li>
					</ol>
				</fieldset>
				
			<div class="clear">&nbsp;</div>
		</div>
		
		<div id="ListsOfCheckedOutInstruments" class="infobox">
			<div class="displaycolumn">
				<table id="checkOutInstruments" class="default2">
					<thead>
						<tr>
							<th colspan="6">
								<div class="float-left">
									<spring:message code="instrument.checkedoutInstr"/>  (${checkoutInsts.size()})
								</div>
								
								<a href="#" id="checkInLink" class="mainlink-float checkInLink hid " onclick="checkInInstruments();">Check In</a>
 							</th>
						</tr>
						  <tr>
							  <th class="centre" scope="col"><spring:message code="barcode"/></th>
							  <th class="centre" scope="col"><spring:message code="instmodelname"/></th>
							  <th class="centre" scope="col"><spring:message code="instrument.checkoutdate"/></th>
							  <th class="centre" scope="col"><spring:message code="jobno"/> </th>
							  <th class="centre" scope="col"><spring:message code="instrument.checkin"/></th>
							  
						  </tr>				
					</thead>
					<tbody>
						<c:set var="rowcount" value="0"/>
							<c:forEach var="element" items="${checkoutInsts}">
								<tr>
									<td>	
										<links:showInstrumentLink instrument="${element.instrument}"/>
									</td>
									 <td class="center">
									 	 <cwms:showinstrument instrument="${element.instrument}" />
									 </td>
									 <td>	
									 	${element.checkOutDate}
									 </td>
									 <td>
									 <c:if test="${element.job != null}">
									 	<links:jobnoLinkDWRInfo rowcount="${rowcount}" jobno="${element.job.jobno}" jobid="${element.job.jobid}" copy="${false}"/>
									 </c:if>
									 	
									 </td>
									  <td>
									 	<input type="checkbox" id="checkInInstru" name="checkInInstru" onclick="displayMassCheckInLink();" value="${element.instrument.plantid}">
									 </td>
									 
									 <c:set var="rowcount" value="${rowcount + 1}"/>
								</tr>
							</c:forEach>
					</tbody>
				</table>
			</div>
			
			<div class="displaycolumn">
				<table id="checkOutInstrumentsWithnoConfidenceCheck" class="default2">
					<thead>
						<tr>
							<th colspan="5">
								<spring:message code="instrument.checkedoutInstrnoconfcheck"/> (${checkedInInstWithNoConfidenceCheck.size()})
 							</th>
						</tr>
						 <tr>
							  <th class="centre" scope="col"><spring:message code="barcode"/></th>
							   <th class="centre" scope="col"><spring:message code="instmodelname"/></th>
							  <th class="centre" scope="col"><spring:message code="instrument.checkindate"/></th>
							  <th class="centre" scope="col"><spring:message code="jobno"/></th>  
							  <th class="centre" scope="col"><spring:message code="instrument.confidencecheck"/></th>
						  </tr>	
					</thead>
					<tbody>
					<c:set var="rowcount" value="0"/>
							<c:forEach var="elem" items="${checkedInInstWithNoConfidenceCheck}">
								<tr>
									<td>	
										<links:showInstrumentLink instrument="${elem.instrument}"/>
									</td>
									 <td class="center">
									 	 <cwms:showinstrument instrument="${elem.instrument}" />
									 </td>
									 <td>	
									 	${elem.checkInDate}
									 </td>
									 <td>
									 	<c:if test="${elem.job != null}">
									 		<links:jobnoLinkDWRInfo rowcount="${rowcount}" jobno="${elem.job.jobno}" jobid="${elem.job.jobid}" copy="${false}"/>
									 	</c:if>
									 </td>
									  <td>
									 	<a href="#" id="checkInLink" class="mainlink-float " onclick="displayConfidenceCheckOverlay(${elem.id},'add');"><spring:message code="instrument.confidencecheck"/></a>
									 </td>
									 <c:set var="rowcount" value="${rowcount + 1}"/>
								</tr>
							</c:forEach>
						
					</tbody>
				</table>
			</div>
			<div class="clear">&nbsp;</div>
		</div>
	</jsp:body>
</t:crocodileTemplate>