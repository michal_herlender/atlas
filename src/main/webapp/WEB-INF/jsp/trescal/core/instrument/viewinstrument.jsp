
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address" %>
<%@ taglib prefix="instrument" tagdir="/WEB-INF/tags/instrument" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel" %>
<%@ taglib prefix="viewinstrument" tagdir="/WEB-INF/tags/instrument/viewinstrument" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>



<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="viewinstrument.title"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/instrument/ViewInstrument.js'></script>
		<script type='text/javascript' src='script/trescal/core/documents/ImageGalleryFunctions.js'></script>
		<script type='text/javascript' src='script/thirdparty/jQuery/jquery.js' ></script>
		<script type='module' src='script/components/cwms-upload-file/cwms-upload-file.js'></script>

		<c:if test="${instrument.asset != null}">
			<script type='text/javascript' src='script/trescal/core/asset/ViewAsset.js'></script>
		</c:if>
		<script type='text/javascript'>
			<c:if test="${instrument.model.modelType.baseUnits == true || instrument.model.modelType.modules == true}">
				menuElements.push({ anchor: 'mods-link', block: 'mods-tab' });
			</c:if>
			<c:if test="${instrument.calibrationStandard == true}">
				menuElements.push({ anchor: 'cals-link', block: 'cals-tab' });
			</c:if>
			<c:if test="${instrument.asset != null || ownedByBusinessCompany == true}">
				menuElements.push({ anchor: 'asset-link', block: 'asset-tab' });
			</c:if>
			<c:if test="${not empty instrument.hireInstruments}">
				menuElements.push({ anchor: 'hireinst-link', block: 'hireinst-tab' });
			</c:if>
	
			var hireAccStateOptions = [];
			<c:forEach var="hireAccState" items="${hireAccStatuss}">
				hireAccStateOptions.push({statusid:	${hireAccState.statusid}, statusname: '${hireAccState.name}'});
			</c:forEach>
			
			var intervalUnits = [];
			<c:forEach var="unit" items="${units}">
				intervalUnits.push({id: '${unit.key}', name: '${unit.value}'})
			</c:forEach>
				
			var recallRequirementTypes = [];
			<c:forEach var="requirement" items="${requirements}">
				recallRequirementTypes.push({id: '${requirement.key}', name: '${requirement.value}'})
			</c:forEach>

            var plantid = '${instrument.plantid}';

		</script>
				
	</jsp:attribute>
	<jsp:attribute name="globalElements">
		<cwms-upload-file system-component-id="${sc.componentId}" entity-id1="${instrument.plantid}" entity-id2=""
 			files-name="${filesName}" rename-to="" web="false"></cwms-upload-file>
	</jsp:attribute>
	<jsp:body>
        <c:set var="valStyle" value=""/>
        <c:forEach var="issue" items="${instrument.issues}">
            <c:if test="${issue.status.requiringAttention == true}">
                <c:set var="valStyle" value="class='attention'"/>
            </c:if>
        </c:forEach>

		<!-- Display notification if user cannot edit this item -->
        <c:if test="${usercanupdate != true}">
            <div class="warningBox1">
                <div class="warningBox2">
                    <div class="warningBox3">
                        <span class="bold"><spring:message code="viewinstrument.cannotedit"/>></span>
                    </div>
                </div>
            </div>
        </c:if>

		<!-- Display notification if instrument is a hire item -->
        <c:if test="${not empty instrument.hireInstruments}">
            <div class="warningBox1">
                <div class="warningBox2">
                    <div class="warningBox3">
                        <span class="bold"><spring:message code="viewinstrument.attentionhireinstr"/></span>
                    </div>
                </div>
            </div>
        </c:if>

		<!-- infobox contains instrument summary and is styled with nifty corners -->
		<div class="infobox">

			<fieldset id="viewinstrument" class='<c:if test="${instrument.scrapped == true}">BER</c:if> relative'>

				<legend><spring:message code="viewinstrument.title"/></legend>

                <c:choose>
                    <c:when test="${jobItemImages.size() > 0}">
                        <c:forEach var="image" items="${jobItemImages}" varStatus="loopStatus">
							<c:if test="${loopStatus.index == 1}">
								<c:set var="imagePath" value="${image.id}"/>
							</c:if>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                    	<c:if test="${instModelImage != null}">
                    		<c:set var="imagePath" value="${instModelImage.id}"/>
                    	</c:if>
                    </c:otherwise>
                </c:choose>

				<div class="defaultEntityImage imgbox">
				<c:choose>
					<c:when test="${imagePath != null}">
						<img src="displaythumbnail?id=${imagePath}" alt="" title="" />
						<br/>
						<c:choose>
							<c:when test="${jobItemImages.size() == 0 && not empty instModelImage}">
								<spring:message code="viewinstrument.sourceinstmodel"/>
							</c:when>
							<c:otherwise>
								<spring:message code="viewinstrument.sourcejobitem"/>
							</c:otherwise>
						</c:choose>
					</c:when>
					<c:otherwise>
						<img src="img/noimage.png" width="150" height="113" alt='<spring:message code="viewinstrument.noimage"/>' title='<spring:message code="viewinstrument.noimage"/>' class="noImage" />
					</c:otherwise>
				</c:choose>
				</div>

				<div class="quickLinkList">
					<c:choose>
						<c:when test="${instrument.scrapped == true}">
							<a href="#" class="mainlink unscrapInstrument" onclick=" confirmScrapInstrumentChange(false, ${instrument.plantid}, null); return false; "><spring:message code="viewinstrument.returninstrumenttoservice"/></a>
						</c:when>
						<c:otherwise>
							<a href="#" class="mainlink scrapInstrument" onclick=" confirmScrapInstrumentChange(true, ${instrument.plantid}, null); return false; "><spring:message code="viewinstrument.scrapinstrument"/></a>
					 	</c:otherwise>
					 </c:choose>
				</div>

				<ol>
					<li>
						<label><spring:message code="viewinstrument.barcodecaldue"/>:</label>
						<div class="float-left">
							<span><links:instrumentLinkDWRInfo displayCalTimescale="true" displayName="false" instrument="${instrument}" rowcount="0" displayBarcode="true"/>&nbsp;&nbsp;</span>
							&nbsp;&nbsp;
							<span class="nextCalDueDate bold"><instrument:showFormattedCalDueDate instrument="${instrument}" /></span>
	                        (<span class="recallRuleInterval">${currentCalRecallRuleInterval.interval}</span>&nbsp;
	                         <span class="recallRuleIntervalUnit">${currentCalRecallRuleInterval.translatedUnit}</span> )
							<c:if test="${extensionallowed}">
								<span id="extendedDateMessage" style="display: none">With permited extension to </span>
								<span id="extendedDate"><!-- populated by initializeCalExtensionDetails javascript function --></span>
	                            <div id="calExtensionDiv" style="padding-top: 10px">
	                                <span id="calExtensionDetails"><!-- populated by initializeCalExtensionDetails javascript function --></span>
									<button id="addOrEditCalExtensionButton" style="display: none"><spring:message code="viewinstrument.addcalextension"/></button>
	                            </div>
	                        </c:if>
							<instrument:usages instrument="${instrument}"/>
						</div>
					</li>
					<li>
						<label><spring:message code="instmodelname"/>: </label>
						<span><cwms:showinstrument instrument="${instrument}"/></span>
					</li>
					<c:if test="${instrument.ranges != null}">
						<c:if test="${instrument.ranges.size() > 0}">
							<li>
								<label><spring:message code="viewinstrument.rangesize"/>:</label>
								<div class="float-left padtop">
									${instrument.ranges.toArray()[0]}
									<c:if test='${instrument.ranges.toArray()[0].uom.symbol != ""}'> 
												(<cwms:besttranslation translations="${instrument.ranges.toArray()[0].uom.nameTranslation}"/>)
									</c:if><br/>
								</div>
								<div class="clear"></div>
							</li>
						</c:if>
					</c:if>
					<li>
						<label><spring:message code="description.description"/>: </label>
						<span><c:out value="${instrument.model.description.description}" /></span>
					</li>
					<li>
						<label><spring:message code="company"/>:</label>
						<span>
							<links:companyLinkDWRInfo company="${instrument.comp}" rowcount="0" copy="true"/>
								-
							<a href='<c:url value="/viewcompanyinstrument.htm?coid=${instrument.comp.coid}"/>' class="mainlink"><spring:message code="viewinstrument.viewallinstruments"/></a>
						</span>
					</li>
				</ol>
				<!-- displays content in column 49% wide (left) -->
				<div class="displaycolumn">

					<ol>

						<li>
							<label><spring:message code="subdivision"/>:</label>
							<span><links:subdivLinkDWRInfo rowcount="0" subdiv="${instrument.con.sub}"/></span>
						</li>
						<li>
							<label><spring:message code="viewinstrument.owner"/>:</label>
							<span><links:contactLinkDWRInfo contact="${instrument.con}" rowcount="0"/></span>
						</li>
						<li>
							<label><spring:message code="address"/>:</label>
							<div class="float-left padtop">
								<c:choose>
									<c:when test='${instrument.add == null || instrument.add.addrid == 0 }'>
										<div><spring:message code="viewinstrument.noinstrumentaddressset"/></div>
									</c:when>
									<c:otherwise>
										<t:showCompactFullAddress address="${instrument.add}" edit="false" copy="true"/>
									</c:otherwise>
								</c:choose>
							</div>
							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
						</li>
						
						<c:if test="${not empty instrument.freeTextLocation }" >
							<li>
								<label><spring:message code="editinstr.freetextlocation"/>:</label>
								<span>${instrument.freeTextLocation}</span>
							</li>
						</c:if>
						
						<c:if test="${instrument.scrapped == false}" >
							<c:if test="${instrument.loc != null}">
								<li>
									<label><spring:message code="viewinstrument.location"/>:</label>
									<span>${instrument.loc.location}</span>
								</li>
							</c:if>
							<li>
								<label><spring:message code="status"/>:</label>
								<span>${instrument.status.status}</span>
							</li>
							<li>
								<label><spring:message code="viewinstrument.usagestoragetype"/>:</label>
								<span>${instrument.usageType.name} (${instrument.storageType.name})</span>
							</li>
							<li>
								<label><spring:message code="viewinstrument.instcalinterval"/>:</label>
								<span class="instCalFrequency">${instrument.calFrequency}</span>
								<span class="instCalFrequencyUnits"> (${instrument.calFrequencyUnit.getName(instrument.calFrequency)})</span>
							</li>

							<li>
								<label><spring:message code="editinstr.customermanaged"/>:</label>
								<span>
									<c:choose>
										<c:when test="${instrument.customerManaged == true}">
											<spring:message code="yes" />
										</c:when>
										<c:otherwise>
											<spring:message code="nobool" />
										</c:otherwise>
									</c:choose>
								</span>
							</li>
							<li>
								<label><spring:message code="viewinstrument.recallstatus"/>:</label>
								<div class="float-left padtop width60">
									<c:set var="neverRecalled" value="true"/>
									<c:if test="${instrument.recalls.size() > 0}">
										<c:forEach var="recall" items="${instrument.recalls}">
											<c:if test="${recall.excludeFromNotification == false}">
												<c:set var="neverRecalled" value="false"/>
											</c:if>
										</c:forEach>
									</c:if>
									<c:choose>
										<c:when test="${neverRecalled == false }">
										<c:choose>
											<c:when test="${instrument.recalledNotReceived == true}">
												<spring:message code="viewinstrument.recalled"/>
											</c:when>
											<c:otherwise>
												<spring:message code="viewinstrument.recieved"/>
											</c:otherwise>
										</c:choose>
										</c:when>
										<c:otherwise>
											<spring:message code="viewinstrument.neverrecalled"/>
										</c:otherwise>
									</c:choose>
								</div>
								<!-- clear floats and restore page flow -->
								<div class="clear"></div>
							</li>
							<li>
								<label><spring:message code="viewinstrument.addedbyon"/>:</label>
								<span>
									<c:if test="${instrument.addedBy != null}">
										<links:contactLinkDWRInfo contact="${instrument.addedBy}" rowcount="0"/>&nbsp;
									</c:if>
									<c:if test="${instrument.addedOn != null}">
										<fmt:formatDate value="${instrument.addedOn}" type="date" dateStyle="short"/>
									</c:if>
								</span>
							</li>
							<c:if test='${instrument.calibrationStandard}'>
								<li>
									<label><spring:message code="searchinstrument.standard"/>:</label>
									<div class="float-left">
										<spring:message code="viewinstrument.standard"/><br/>
										<c:if test="${instrument.calExpiresAfterNumberOfUses == true}">
											<spring:message code="viewinstrument.calibrationintervalstandard1"/>
											<c:out value=" ${instrument.permittedNumberOfUses} " />
											<spring:message code="viewinstrument.calibrationintervalstandard2"/>
											<br/>
											<spring:message code="viewinstrument.calibrationusedstandard1"/>
											<c:out value=" ${instrument.usesSinceLastCalibration} "/>
											<spring:message code="viewinstrument.calibrationusedstandard2"/>
											<br/>
										</c:if>
									</div>
								</li>
							</c:if>
						</c:if>

					</ol>

				</div>
				<!-- end of left column -->

				<!-- displays content in column 49% wide (right) -->
				<div class="displaycolumn">

					<ol class="viewInstrumentTechnicalList">
						<c:if test='${instrument.model.modelMfrType  == "MFR_GENERIC"}'>
							<li>
								<label><spring:message code="manufacturer"/>:</label>
								<span>${instrument.mfr.name}</span>
							</li>
							<li>
								<label><spring:message code='model'/>:</label>
								<span>${instrument.modelname}</span>
							</li>
						</c:if>
						<li>
							<label><spring:message code="serialno"/>:</label>
							<span>${instrument.serialno}</span>
						</li>
						<li>
							<label><spring:message code="plantno"/>:</label>
							<span>${instrument.plantno}</span>
						</li>
						<c:if test="${not empty instrument.clientBarcode }" >
							<li>
								<label><spring:message code="editinstr.clientbarcode"/>:</label>
								<span>${instrument.clientBarcode}</span>
							</li>
						</c:if>
						<c:choose>
							<c:when test="${instrument.scrapped == true}">
								<li>
									<label><spring:message code="viewinstrument.scrappedbyon"/>:</label>
									<span>
										<c:choose>
											<c:when test="${instrument.scrappedBy != null}">
												${instrument.scrappedBy.name}
											</c:when>
											<c:otherwise>
												<spring:message code="viewinstrument.unknown"/>
											</c:otherwise>
										</c:choose>
										<c:choose>
											<c:when test="${instrument.scrappedOn != null}">
												<fmt:formatDate value="${instrument.scrappedOn}" type="date" dateStyle="short"/>
											</c:when>
											<c:otherwise>
												<spring:message code="viewinstrument.unknown"/>
											</c:otherwise>
										</c:choose>
									</span>
								</li>
								<li>
									<label><spring:message code="viewinstrument.scrappedonjob"/>:</label>
									<span>
										<c:choose>
											<c:when test="${instrument.scrappedOnJI != null}">
												<links:jobnoLinkDWRInfo rowcount="01" jobno="${instrument.scrappedOnJI.job.jobno}" jobid="${instrument.scrappedOnJI.job.jobid}" copy="false"/>
												item <links:jobitemLinkDWRInfo jobitem="${instrument.scrappedOnJI}" jicount="false" rowcount="01"/>
											</c:when>
											<c:otherwise>
												<spring:message code="viewinstrument.unknown"/>
											</c:otherwise>
										</c:choose>
									</span>
								</li>
							</c:when>
							<c:otherwise>
								<c:if test='${instrument.firmwareAccessCode != null && instrument.firmwareAccessCode != ""}'>
									<li>
										<label><spring:message code="viewinstrument.firmwarecode"/>:</label>
										<span>${instrument.firmwareAccessCode}</span>
									</li>
								</c:if>
							</c:otherwise>
						</c:choose>
						<c:if test="${instrument.thread != null}">
							<li>
								<label><spring:message code="viewinstrument.threadsize"/>:</label>
								<span>
									<c:if test="${instrument.thread != null}">
										<c:choose>
											<c:when test="${instrument.thread.size != null}">
												${instrument.thread.size}
											</c:when>
											<c:otherwise>
												&nbsp;
											</c:otherwise>
										</c:choose>
										<c:if test="${instrument.thread.type != null}">
											(${instrument.thread.type.type})
										</c:if>
										<a href='<c:url value="/thread.htm?id=${instrument.thread.id}"/>' class="mainlink"><spring:message code="viewinstrument.editthread"/></a>]
									</c:if>
								</span>
							</li>
						</c:if>
						<li>
							<label><spring:message code="capability"/>:</label>
							<div class="float-left padtop width60">
								<c:choose>
									<c:when test="${instrument.capability != null}">
										<links:procedureLinkDWRInfo displayName="true" capability="${instrument.capability}" rowcount="0"/>
									</c:when>
									<c:otherwise>
										<spring:message code="viewinstrument.nodefaultcapability"/>
									</c:otherwise>
								</c:choose>
							</div>

							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
						</li>
						<li>
							<label><spring:message code="viewinstrument.defaultservicetype"/>:</label>
							<div class="float-left padtop width60">
								<c:choose>
									<c:when test="${instrument.defaultServiceType != null}">
										<cwms:besttranslation translations="${instrument.defaultServiceType.shortnameTranslation}" /> - 
										<cwms:besttranslation translations="${instrument.defaultServiceType.longnameTranslation}" />
									</c:when>
									<c:otherwise>
										<spring:message code="viewinstrument.nodefaultservicetype"/>
									</c:otherwise>
								</c:choose>
							</div>

							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
						</li>
						<li>
							<label><spring:message code="viewinstrument.workinstruction"/>:</label>
							<div class="float-left padtop width60">
								<c:choose>
									<c:when test="${instrument.workInstructions.size() >0 }">
										<a href='<c:url value="/viewworkinstruction?wid=${instrument.workInstructions[0].id}"/>' class="mainlink">
										<c:out value="${instrument.workInstructions[0].title}" /></a>
									</c:when>
									<c:otherwise>
										<spring:message code="viewinstrument.nodefaultworkinstruction"/>
									</c:otherwise>
								</c:choose>
							</div>
							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
						</li>
						<li>
							<label><spring:message code="instr.defaultClientRef"/>:</label>
							<div class="float-left padtop width60">
								<c:choose>
									<c:when test="${instrument.defaultClientRef != null}">
										 ${instrument.defaultClientRef}
									</c:when>
									<c:otherwise>
											N/A
									</c:otherwise>
								</c:choose>		
							</div>
							<div class="clear"></div>
						</li>

						<li>
							<label><spring:message code="viewinstrument.customerdescription"/></label>
							<span>
								<c:choose>
									<c:when test="${instrument.customerDescription != null}">
										${instrument.customerDescription}
									</c:when>
									<c:otherwise>
										&nbsp;
									</c:otherwise>				
								</c:choose>
							</span>
						</li>
						<c:if test="${not empty instrument.customerSpecification }" >
							<li>
								<label><spring:message code="editinstr.customerspecification"/>:</label>
								<span>${instrument.customerSpecification}</span>
							</li>
						</c:if>
						<li>
							<viewinstrument:iterimcaldate currentInterimCalRecallRuleInterval="${currentInterimCalRecallRuleInterval}" instrument="${instrument}"/>
						</li>
						<li>
							<viewinstrument:maintenancedate currentMaintenanceRecallRuleInterval="${currentMaintenanceRecallRuleInterval}" instrument="${instrument}"/>
						</li>
						
						<t:showCalReqAsListItem inst="${instrument}" ji="0" page="instrument" calReqs="${calReqs}"/>

					</ol>

				</div>
				<!-- end of right column -->

			</fieldset>

		</div>
		<!-- end of infobox div -->

		<c:set var="recallCount" value="0"/>
		<c:forEach var="recall" items="${instrument.recalls}">
			<c:if test="${recall.excludeFromNotification == false}">
				<c:set var="recallCount" value="${recallCount + 1}"/>
			</c:if>
		</c:forEach>

		<!-- sub navigation menu which displays and hides sections of page as well as linking to external pages -->
		<div id="subnav">
			<dl>
				<dt>
					<a href='<c:url value="/editinstrument.htm?plantid=${instrument.plantid}"/>'
					<c:choose>
						<c:when test="${instrument.scrapped == true}"> 
							title='<spring:message code="viewinstrument.subnavtitleedit1"/>' disabled="disabled" 
						</c:when>
						<c:otherwise> 
							title='<spring:message code="viewinstrument.subnavtitleedit2"/>' 
						</c:otherwise>
					</c:choose>>
					<spring:message code="edit"/>
					</a>
				</dt>
				<dt>
					<a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'calhistory-tab', false); " 
							id="calhistory-link" title='<spring:message code="viewinstrument.subnavtitlecalhistory"/>'>
							<spring:message code="viewinstrument.calhistory"/> (${historySize})
					</a>
				</dt>
				<dt>
					<a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'jobs-tab', false); " 
						id="jobs-link" class="selected" title='<spring:message code="viewinstrument.subnavtitlejobhistory"/>'>
							<spring:message code="viewinstrument.jobs"/> (${instrument.jobItems.size()})
					</a>
				</dt>
				<dt>
					<a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'certs-tab', false); " 
						id="certs-link" title='<spring:message code="viewinstrument.subnavtitleinstcert"/>'>
							<spring:message code="certificates"/> (${certlinkCount + instCertLinkCount})
					</a>
				</dt>
				<dt>
					<a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'files-tab', false); " 
						id="files-link" title='<spring:message code="viewinstrument.subnavtitlefiles"/>'>
							<spring:message code="viewinstrument.files"/> (${scRootFiles.numberOfFiles})
					</a>
				</dt>
				<dt>
					<a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'recall-tab', false); " 
						id="recall-link" title='<spring:message code="viewinstrument.subnavtitlerecall"/>'>
							<spring:message code="viewinstrument.recall"/> (${recallCount})
					</a>
				</dt>
				<dt>
					<a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'validation-tab', false); " 
						id="validation-link" title='<spring:message code="viewinstrument.subnavtitlevalidation"/>'>
							<span ${valStyle}><spring:message code="viewinstrument.validation"/> (${instrument.issues.size()})</span></a>
				</dt>
				<dt><a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'rep-tab', false); " 
					id="rep-link" title='<spring:message code="viewinstrument.subnavtitlereplacement"/>'>
						<spring:message code="viewinstrument.replacement"/>
					</a>
				</dt>
				<c:if test="${instrument.calibrationStandard}">
					<dt>
						<a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'cals-tab', false); " 
							id="cals-link" title='<spring:message code="viewinstrument.subnavtitleinstcalibration"/>'>
								<spring:message code="viewinstrument.calibrations"/> (${instrument.calibrations.size()})
						</a>
					</dt>
				</c:if>
				
			</dl>
		</div>
		<!-- end of sub navigation menu -->

		<!-- sub navigation menu which displays and hides sections of page as well as linking to external pages -->
		<div id="subnav2">
			<dl>
				<dt>
					<a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'history-tab', false); " 
					id="history-link" title='<spring:message code="viewinstrument.subnavtitlechangehist"/>'>
						<spring:message code="viewinstrument.history"/> (${instrument.instrumentHistoryTotalCount})
					</a>
				</dt>
				<dt>
					<a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'images-tab', false); " 
					id="images-link" title='<spring:message code="viewinstrument.subnavtitleimg"/>'>
						<spring:message code="viewinstrument.images"/> (${jobItemImages.size()})
					</a>
				</dt>
				<dt>
					<a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'standards-tab', false); " 
					id="standards-link" title='<spring:message code="viewinstrument.subnavtitlestandards"/>'>
						<spring:message code="searchinstrument.standard"/> (${instrument.standards.size()})
					</a>
				</dt>
				<dt>
					<a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'flexiblefields-tab', false); " 
					id="formerfields-link" title='<spring:message code="viewinstrument.formerfields"/>'>
						<spring:message code="viewinstrument.formerfields"/>
					</a>
				</dt>
				<dt>
					<a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'measurementpossibilities-tab', false); " 
					id="measurementpossibilities-link" title='<spring:message code="viewinstrument.measurementpossibilities"/>'>
						<spring:message code="viewinstrument.measurementpossibilities"/>
					</a>
				</dt>
				<c:if test="${instrument.model.modelType.baseUnits == true || instrument.model.modelType.modules == true }">
					<dt>
						<a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'mods-tab', false); " 
						id="mods-link" title='<spring:message code="viewinstrument.subnavtitlebasemods"/>'>
							<spring:message code="viewinstrument.baseunitmodules"/>
						</a>
					</dt>
				</c:if>
				<%-- 
				## Currently disabled, see Assembla ticket 670
				###if (($instrument.asset) || ($ownedByBusinessCompany == true))
				##	<dt><a href="#" onclick="switchMenuFocus(menuElements, 'asset-tab', false); return false; " id="asset-link" title="#springMessage("viewinstrument.subnavtitleasset")">$businessDetails.docCompany #springMessage("viewinstrument.asset")</a></dt>
				###end
				--%>
				
				<dt>
					<a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'quotes-tab', false); " 
					id="quotes-link" title='<spring:message code="viewinstrument.subnavtitlequotes"/>'>
						<spring:message code="viewinstrument.quotations"/> (${instrument.quoteItems.size()})
					</a>
				</dt>
				<c:if test="${not empty instrument.hireInstruments}">
					<dt>
						<a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'hireinst-tab', false); " 
						id="hireinst-link" title='<spring:message code="viewinstrument.subnavtitlehire"/>'>
							<spring:message code="viewinstrument.hireinformation"/>
						</a>
					</dt>
				</c:if>
				<dt>
					<a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'complementary-tab', false); " 
					id="complementary-link" title='<spring:message code="viewinstrument.subnavtitlecomplementary"/>'>
						<spring:message code="viewinstrument.complementary"/>
					</a>
				</dt>
				
				<dt>
					<a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'Checkinout-tab', false); " 
							id="Checkinout-link" title='Check In-Out History'>
						<spring:message code="instrument.checkouthistory"/>  
					</a>
				</dt>
			</dl>
		</div>
		<!-- end of sub navigation menu -->

		<!-- div containing information tabs -->
		<div class="infobox" id="viewinstrumentinfo">
			
			 <div id="calhistory-tab" class="hid">
				<viewinstrument:calhistory history="${maxCalHistory}" maxResults="${maxResultsCalHistory}" historySize="${historySize}"/>
			</div>

			<div id="jobs-tab">
				<viewinstrument:jobs instrument="${instrument}" fastTrackTurn="${fastTrackTurn}" jobItems="${jobItems}" jobItemCount="${jobItemCount}" />
			</div>

			<div id="certs-tab" class="hid">
				<viewinstrument:certificates instrument="${instrument}"
				certlinks="${certlinks}" certlinkCount="${certlinkCount}"
				instCertLinks="${instCertLinks}" instCertLinkCount="${instCertLinkCount}"
				fastTrackTurn="${fastTrackTurn}" calibrationVerification="${calibrationVerification}" />
			</div>

 			<div id="files-tab" class="hid">
 				<fmt:formatNumber var="formattedPlantId" value="${instrument.plantid}" pattern="0" />
 				<spring:message var="rootTitle" code='filebrowser.title.instrument' arguments="${formattedPlantId}"/>
 				<files:showFilesForSC entity="${instrument}" sc="${sc}" id="${instrument.plantid}"
 				identifier="${instrument.plantid}" ver="" rootFiles="${scRootFiles}" allowEmail="false"
 				isEmailPlugin="false" rootTitle="${rootTitle}"
 				deleteFiles="${true}"/>			
			</div>

			<div id="recall-tab" class="hid">
				<viewinstrument:recall instrument="${instrument}" requirements="${requirements}" units="${units}" servicetypes="${servicetypes}" 
					activerecallrules="${activerecallrules}" certlinkCount="${certlinkCount}" defaultrecall="${defaultrecall}" 
					inactiverecallrules="${inactiverecallrules}" instCertLinkCount="${instCertLinkCount}" lastCalDate="${lastCalDate}" 
					recalldue="${recalldue}" />
			</div>

			<div id="validation-tab" class="hid">
				<viewinstrument:validation instrument="${instrument}"/>
			</div>

			<div id="rep-tab" class="hid">
				<viewinstrument:replacement instrument="${instrument}"/>
			</div>

			<div id="cals-tab" class="hid">
				<viewinstrument:calibrations instrument="${instrument}" resPerPage="${maxResultsCalHistory}" />
			</div>

			<div id="history-tab" class="hid">
				<viewinstrument:history instrument="${instrument}" />
			</div>

			<div id="images-tab" class="hid">
				<viewinstrument:images jobItemImages="${jobItemImages}" />
			</div>

			<div id="standards-tab" class="hid">
				<viewinstrument:standards instrument="${instrument}" procstandards="${procstandards}" wistandards="${wistandards}" />
			</div>

			<div id="mods-tab" class="hid">
				<viewinstrument:modules instrument="${instrument}"/>
			</div>

			<div id="asset-tab" class="hid">
				<viewinstrument:assets instrument="${instrument}" ownedByBusinessCompany="${ownedByBusinessCompany}"/>
			</div>

			<div id="quotes-tab" class="hid">
				<viewinstrument:quotation instrument="${instrument}"/>
			</div>

			<div id="flexiblefields-tab" class="hid">
				<viewinstrument:flexiblefields instrument="${instrument}" flexiblefields="${flexiblefields}"/>
			</div>

			<div id="measurementpossibilities-tab" class="hid">
				<viewinstrument:measurementpossibilities instrument="${instrument}"/>
			</div>

			<c:if test="${not empty instrument.hireInstruments}">
				<div id="hireinst-tab" class="hid">
					<viewinstrument:hire instrument="${instrument}" hireAccStatuss="${hireAccStatuss}" />
				</div>
			</c:if>

			<div id="complementary-tab" class="hid">
				<viewinstrument:complementaryfields instrument="${instrument}" hasdeviation="${hasdeviation}" hasverificationstatus="${hasverificationstatus}"/>
			</div> 
			
			<div id="Checkinout-tab" class="hid">
				<viewinstrument:checkinhistory checkin="${checkouts}"/>
			</div> 
		</div>

		<t:showTabbedNotes entity="${instrument}" privateOnlyNotes="${privateOnlyNotes}" noteTypeId="${instrument.plantid}" noteType="INSTRUMENTNOTE"/>

        <div id="calExtensionDialog" style="display: none;">
            <!-- html template used to create pop up form to add/edit cal extension -->
            <fieldset>
                <ol>
                    <li>
                        <select name="calextensiontype" id="calextensiontype">
                            <c:forEach var="type" items="${extensiontypes}">
                                <option value="${type.key}"><c:out value="${type.value}"/></option>
                            </c:forEach>
                         </select>
                    </li>
                    <li>
                        <label><spring:message code="viewinstrument.calextension"/></label>
                        <input name="extensionvalue" id="extensionvalue">&nbsp;
                        <select name="extensiontimeunits" id="extensiontimeunits">
                            <c:forEach var="unit" items="${units}">
                                <option value="${unit.key}"><c:out value="${unit.value}"/></option>
                            </c:forEach>
                        </select>
                        <span id="percentagesymbol" style="display: none">%</span>
                    </li>
                    <li>
                        <button id="addoreditcalextension"><spring:message code="add"/></button>
						<button id="deletecalextension" style="display: none"><spring:message code="delete"/></button>
                    </li>
                </ol>
            </fieldset>
        </div>

	</jsp:body>
</t:crocodileTemplate>
