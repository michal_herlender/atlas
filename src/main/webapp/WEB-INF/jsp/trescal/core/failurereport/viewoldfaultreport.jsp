<%-- File name: /trescal/core/failurereport/viewoldfaultreport.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="failurereport.viewfailurereport" /></span>
	</jsp:attribute>
	<jsp:body>
		<%-- This contains the now READ ONLY fault report data --%>
		<div id="faultreportolddata-tab" >
			<fieldset>
                   <legend>
					<spring:message code="jifaultreport.olddata" />
				</legend>
                   <ol>
                   	<li>
                           <label class="width30"><spring:message code="jifaultreport.string3"/></label>
                           <input onclick="return false" type="checkbox" ${ faultReport.preRecord ? 'checked' : '' } disabled="disabled" />
                       </li>
                       <li>
                           <label class="width30"><spring:message code="jifaultreport.string4"/></label>
                           <input onclick="return false" type="checkbox" ${ faultReport.preCert ? 'checked' : '' } disabled="disabled" />
                       </li>
					<!-- description -->
                       <li>
                              <span>
                                  <spring:message code="jifaultreport.string5"/>:
                              </span>
                              <span>&nbsp;</span>
                          </li>
                          <li>
                           <table class="default2" id="frDescList" summary="This table contains contract review job item requirements that are active">
                               <thead>
                                   <tr>
                                       <th class="faultdetail" scope="col">
                                          <spring:message code = "description"/>
                                       </th>
                                       <th class="modifiedby" scope="col">
                                           <spring:message code="jifaultreport.setbyseton"/>
                                       </th>
                                   </tr>
                               </thead>
                               <tbody>
                               	<c:if test="${faultReport.descriptions.size() == 0}">
                                       <tr><td colspan="2" class="bold"><spring:message code="jifaultreport.string6"/></td></tr>
                                   </c:if>
                                   <c:forEach var="desc" items="${faultReport.descriptions}">
                                       <tr>
                                           <td id="Description-${desc.id}">
                                               ${desc.active == false ? "<del>" : ""} ${desc.description} ${desc.active == false ? "</del>" : ""}
                                           </td>
                                           <td><c:out value="[${desc.recordedBy.name}]"/> <fmt:formatDate value="${desc.lastModified}" dateStyle="medium" type="date" /></td>
                                       </tr>
                                   </c:forEach>
                               </tbody>
                           </table>
                      	</li>
                      	<li>
                              <span>
                                  <spring:message code="jifaultreport.string6"/>
                              </span>
                          </li>
                       <li>
                           <table class="default2" id="frActList" summary="<spring:message code='jifaultreport.string7'/>">
                               <thead>
                                   <tr>
                                       <th class="faultdetail" scope="col">
                                           <spring:message code="jifaultreport.action"/>
                                       </th>
                                       <th class="modifiedby" scope="col">
                                           <spring:message code="jifaultreport.setbyseton"/>
                                       </th>
                                   </tr>
                               </thead>
                               <tbody>
                              		 <c:if test="${faultReport.actions.size() == 0}">
                                       <tr><td colspan="2" class="bold"><spring:message code="jifaultreport.string8"/></td></tr>
                                   </c:if>
                                   <c:set var="actCount" value="0"/>
                                   <c:forEach var="act" items="${faultReport.actions}">
                                       <tr>
                                           <td class="require" id="Action-$act.id">
                                               ${act.active == false ? "<del>" : ""} ${act.description} ${act.active == false ? "</del>" : ""}
                                           </td>
                                           <td class="addby"><c:out value="[${act.recordedBy.name}]"/> <fmt:formatDate value="${act.lastModified}" dateStyle="medium" type="date" /></td>
                                       </tr>
                                   </c:forEach>
                           </tbody>
                           </table>
                       </li>
                       <li>
                              <span>
                                  <spring:message code="jifaultreport.string9"/>:
                              </span>
                          </li>
                       <li>
                           <span class="bold">
                               <spring:message code="jifaultreport.string10"/>&nbsp;<a href="jithirdparty.htm?jobitemid=${faultReport.jobItem.jobItemId}" class="mainlink"><spring:message code="jifaultreport.thirdpartytab"/></a>
                           </span>
                       </li>
                       <li>
                           <table class="default2" id="frInstList" summary="<spring:message code='jifaultreport.string11'/>">
                               <thead>
                                   <tr>
                                       <th class="faultdetail" scope="col">
                                           <spring:message code="instruction"/>
                                       </th>
                                       <th class="showontpdel" scope="col"><spring:message code="jifaultreport.show"/></th>
                                       <th class="modifiedby" scope="col"><spring:message code="jifaultreport.setbyseton"/></th>
                                   </tr>
                               </thead>
                               <tbody>
                               	<c:if test="${faultReport.instructions.size() == 0}">
                                       <tr>
                                           <td colspan="4" class="bold"><spring:message code="jifaultreport.string12"/></td>
                                       </tr>
                                   </c:if>
                                   <c:forEach var="inst" items="${faultReport.instructions}">
                                       <tr>
                                           <td class="faultdetail" id="Instruction-$inst.id">
                                               ${inst.active == false ? "<del>" : ""} ${inst.description} ${inst.active == false ? "</del>" : ""}
                                           </td>
                                           <td class="showontpdel" id="showontpdn$inst.id">
                                               <a href="#" onclick=" event.preventDefault(); ">
                                                   <c:choose>
                                                       <c:when test="${inst.isShowOnTPDelNote() == true}">
                                                           <img src="img/icons/bullet-tick.png" width="10" height="10" alt="<spring:message code='jifaultreport.showontpdelnote'/>" title="<spring:message code='jifaultreport.showontpdelnote'/>" />
                                                       </c:when>
                                                       <c:otherwise>
                                                           <img src="img/icons/bullet-cross.png" width="10" height="10" alt="<spring:message code='jifaultreport.hideontpdelnote'/>" title="<spring:message code='jifaultreport.hideontpdelnote'/>" />
                                                       </c:otherwise>
                                                   </c:choose>
                                               </a>
                                           </td>
                                           <td class="modifiedby"><c:out value="[${inst.recordedBy.name}]"/> <fmt:formatDate value="${inst.lastModified}" dateStyle="medium" type="date" /></td>
                                       </tr>
                                   </c:forEach>
                               </tbody>
                           </table>
                       </li>
                  	</ol>
               </fieldset>
		</div>
	</jsp:body>
</t:crocodileTemplate>
