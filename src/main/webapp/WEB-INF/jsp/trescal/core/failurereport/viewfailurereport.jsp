<%-- File name: /trescal/core/failurereport/viewfailurereport.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="costs" tagdir="/WEB-INF/tags/costs" %>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>
<%@ taglib prefix="purchaseorder" tagdir="/WEB-INF/tags/purchaseorder" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="failurereport.viewfailurereport" /></span>
		<style>
			#subnav dt input {
		        color: #000;
   				background-color: #fff;
   				border: 1px solid #999;
			}
			#subnav dt input:hover {
				border: 1px solid #666;
				border-top: 2px solid #666;
				border-bottom: 2px solid #666;
			}
			#subnav dt input.disabled {
				color: #999;
				background-color: #DEDEDE;
				border: 1px solid #999;
			}
		
		</style>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/failurereport/viewfailurereport.js'></script>
	</jsp:attribute>
	<jsp:body>
	
		<form:form modelAttribute="form" action="">
		<%-- View only; not an actual working form; editfailurereport.jsp used for editing/creating --%>
	
		<!-- infobox contains all failure report information and is styled with nifty corners -->
		<div class="infobox" >
		
			<fieldset>
			
				<legend><spring:message code="jifaultreport.faultreport"/>: ${failureReport.faultReportNumber}</legend>
				
				<!-- displays content in column 49% wide (left) -->
				<div class="displaycolumn">
					<span style="font-weight: bold; " ><spring:message code="failurereport.sender"/>:</span>
					<ol>
						<li>
							<label><spring:message code="job"/>:</label>
							<span><links:jobnoLinkDWRInfo rowcount="1" jobno="${failureReport.jobItem.job.jobno}" jobid="${failureReport.jobItem.job.jobid}" copy="true"/> </span>
							<span><links:jobitemLinkDWRInfo jobitem="${ failureReport.jobItem }" jicount="true" rowcount="1"></links:jobitemLinkDWRInfo> </span>
						</li>
						<li>
							<label><spring:message code="viewjob.technician"/>:</label>
							<span><links:contactLinkDWRInfo contact="${ failureReport.technician }" rowcount="1"/></span>
						</li>
						<li>
							<label><spring:message code="issuedate"/>:</label>
							<span><fmt:formatDate type="date" dateStyle="SHORT" value="${ failureReport.issueDate }"/></span>
						</li>
						<li>
							<label><spring:message code="telephone"/>:</label>
							<span>${ failureReport.technician.telephone }</span>
						</li>
						<li>
							<label><spring:message code="company.fax"/>:</label>
							<span>${ failureReport.technician.fax }</span>
						</li>
						<li>
							<label><spring:message code="subdiv"/>:</label>
							<span><links:subdivLinkDWRInfo subdiv="${ failureReport.organisation }" rowcount="1" /></span>
						</li>
						<li>
							<label><spring:message code="failurereport.validated" />:</label>
							<span>
								<c:if test="${ not empty failureReport.managerValidation }">
									<spring:message code="${failureReport.managerValidation}" />
								</c:if>
							</span>
						</li>
						<li>
							<label><spring:message code="failurereport.validatedby"/>:</label>
							<span>
								<c:if test="${not empty failureReport.managerValidationBy }">
									<links:contactLinkDWRInfo contact="${ failureReport.managerValidationBy }" rowcount="1" />
								</c:if>								
							</span>
						</li>
						<li>
							<label><spring:message code="failurereport.validatedon"/>:</label>
							<span><fmt:formatDate type="date" dateStyle="SHORT" value="${ failureReport.managerValidationOn }"/></span>
						</li>
					</ol>
																																						
				</div>
				<!-- end of left column -->
				
				<!-- displays content in column 49% wide (right) -->
				<div class="displaycolumn">
					<span style="font-weight: bold;" ><spring:message code="client"/>:</span>
					<ol>
						<li>
							<label><spring:message code="contact"/>:</label>
							<span><links:contactLinkDWRInfo contact="${ failureReport.jobItem.job.con }" rowcount="1"/></span>
						</li>
						<li>
							<label><spring:message code="telephone"/>:</label>
							<span>${ failureReport.jobItem.job.con.telephone }</span>
						</li>
						<li>
							<label><spring:message code="company.fax"/>:</label>
							<span>${ failureReport.jobItem.job.con.fax }</span>
						</li>
						<li>
							<label><spring:message code="company"/>:</label>
							<span><links:companyLinkDWRInfo company="${ failureReport.jobItem.job.con.sub.comp }" copy="true" rowcount="1"/></span>
						</li>
						<li>
							<label><spring:message code="subdiv"/>:</label>
							<span><links:subdivLinkDWRInfo subdiv="${ failureReport.jobItem.job.con.sub }" rowcount="1" /></span>
						</li>
						<li>
							<label><spring:message code="failurereport.clientapproval" />:</label>
							<span>
								<c:if test="${ not empty failureReport.clientApproval }">
									<spring:message code="${failureReport.clientApproval}" />
								</c:if>
							</span>
						</li>
						<li>
							<label><spring:message code="failurereport.clientresponsefrom"/>:</label>
							<span>
								<c:if test="${ not empty failureReport.clientApprovalBy }">
									<links:contactLinkDWRInfo contact="${ failureReport.clientApprovalBy }" rowcount="1" />
								</c:if>
								<c:if test="${ not empty failureReport.clientAlias }">
									<c:out value="${ failureReport.clientAlias }"/>
								</c:if>
							</span>
						</li>
						<li>
							<label><spring:message code="failurereport.clientresponsereceivedon"/>:</label>
							<span><fmt:formatDate type="date" dateStyle="SHORT" value="${ failureReport.clientApprovalOn }"/></span>
						</li>
					</ol>														
				</div>
				<!-- end of right column -->
																
			</fieldset>
			
			
		</div>
		<!-- end of failure report information div -->
		
		<div id="subnav">
			<dl>
				<dt>
					<a href="viewfailurereport.htm?failurereportid=${ form.faultRepId }" class="selected"	>
						<spring:message code="system.view" />
					</a>
				</dt>
				<dt>
					<a href="editfailurereport.htm?failurereportid=${form.faultRepId}" ${ allowEdit ? '' : 'class="disabled"' } 
							title="<spring:message code='edit' />">
						<spring:message code="edit"/>
					</a>
				</dt>
				<dt>
					<%-- The manager performing validation should enter the regular edit screen, to perform the validation --%>
					<c:set var="validateButton" ><spring:message code="failurereport.validate"/></c:set>
                   	<c:if test="${ not empty form.managerValidation && form.managerValidation }">
                   		<c:set var="validateButton" ><spring:message code="failurereport.validated"/></c:set>
                   	</c:if>
					<a href="editfailurereport.htm?failurereportid=${form.faultRepId}" ${ allowValidation ? '' : 'class="disabled"' } 
							title="${validateButton}">
						${validateButton}
					</a>
				</dt>
				<dt>
					<a ${ allowGeneration ? '' : 'class="disabled"' }
						 onclick=" event.preventDefault(); genDocPopup(${form.faultRepId}, 'birtfailurereport.htm?faultreportid=', 'Generating Failure report Document'); "  title="<spring:message code='generate' />" >
						<spring:message code="generate"/>
					</a>
				</dt>
			</dl>
		</div>
		
		<!-- infobox contains all failure report information-->
		<div class="infobox">
		
			<div id="tabmenu">
					<!-- tabbed submenu to change between contract review options -->
					<ul class="subnavtab">
						<li>
							<a href="#" id="technicianpart-link" 
								onclick="event.preventDefault(); switchMenuFocus(menuElements, 'technicianpart-tab', false); " class="selected">
								<spring:message code="jifaultreport.string2" />
							</a>
						</li>
						<li>
							<a href="#" id="managerpart-link" 
									onclick="event.preventDefault(); switchMenuFocus(menuElements, 'managerpart-tab', false); ">
								<spring:message code="failurereport.situationintheprocess" />
							</a>
						</li>
						<li>
							<c:set var="clientTabOnClickFunction" >return false;</c:set>
							<c:if test="${ showClientPart eq true }">
								<c:set var="clientTabOnClickFunction" >event.preventDefault(); switchMenuFocus(menuElements, 'clientpart-tab', false); </c:set>
							</c:if>
							<a href="#" id="clientpart-link" onclick="${clientTabOnClickFunction}" 
									${ showClientPart eq true ? '':'style="color:gray;cursor: not-allowed;"' } >
								<spring:message code="stategroup.displayname.decision" />
							</a>
						</li>
						<li>
							<c:set var="finalOutcomeTabOnClickFunction" >return false;</c:set>
							<c:if test="${ showFinalOutcomePart eq true }">
								<c:set var="finalOutcomeTabOnClickFunction" >event.preventDefault(); switchMenuFocus(menuElements, 'froutcome-tab', false); </c:set>
							</c:if>
							<a href="#" id="froutcome-link" ${ showFinalOutcomePart eq true ? '':'style="color:gray;cursor: not-allowed;"' }
								onclick="${ finalOutcomeTabOnClickFunction }">
								<spring:message code="failurereport.outcome" />
							</a>
						</li>
					</ul>
					<!-- div to hold all content which is to be changed using the tabbed submenu -->
					<div class="tab-box">
						
						<!-- technician part -->
						<div id="technicianpart-tab">
							<fieldset>
				            	<legend>
									<spring:message code="jifaultreport.string2" />
								</legend>
				                <ol>
				                 	<li>
				               			<label class="frdetails"><spring:message code="state" />:</label>
										<div class="float-left">
											<table>
												<c:forEach items="${failureReportStates}" var="state" varStatus="loopStatus" >	
													
													<c:if test="${ loopStatus.index % 2 eq 0 }">
														<tr>
													</c:if>
										
													<td>
														<input type="checkbox" name="states" value="${state.key}" class="pad-left" 
																 onclick="return false" ${form.states.contains(state.key) ? ' checked' : ''}  />
														${state.value}
													</td>
												
													<c:if test="${ loopStatus.count % 2 eq 0 }">
														</tr>
													</c:if>
													
												</c:forEach>
											</table>
										</div>
								
									</li>
									<li id="otherStateLi" class="${ hasStateOther ? 'vis' : 'hid' }">
				                   		<label class="frdetails"><spring:message code="jifaultreport.otherstate" />:</label>
				                   		<form:textarea path="otherState" rows="2" cols="100" readonly="true" />
				                    </li>
				                    <li>
				               			<label class="frdetails"><spring:message code="failurereport.technicianComments" />:</label>
										<form:textarea path="technicianComments" rows="3" cols="100" readonly="true" />
				                    </li>
				                </ol>
							</fieldset>
						</div>					
					
						<!-- manager part -->
						<div id="managerpart-tab" class="hid" >
							<fieldset>
			            	<legend>
								<spring:message code="failurereport.situationintheprocess" />
							</legend>
			                <ol>
			                	<li>
			                 		<label class="frdetails"><spring:message code="systemdefault.sendfaultreporttoclient" />:</label>
			                 		
			                 		<input name="sendToClient" value="true" onclick="return false" type="checkbox" ${ form.sendToClient eq true ? 'checked' : '' } />
		                 			<spring:message code="yes" />
			
									<input name="sendToClient" value="false" onclick="return false" type="checkbox" ${ form.sendToClient eq false ? 'checked' : '' } />
		                 			<spring:message code="no" />
			
								</li>
			                    <li>
			                    	<label class="frdetails"><spring:message code="failurereport.clientresponseneeded" />:</label>
			                    	
			                    	<input name="clientResponseNeeded" value="true" onclick="return false" type="checkbox" ${ form.clientResponseNeeded eq true ? 'checked' : '' } />
		                 			<spring:message code="yes" />
			
									<input name="clientResponseNeeded" value="false" onclick="return false" type="checkbox" ${ form.clientResponseNeeded eq false ? 'checked' : '' } />
		                 			<spring:message code="no" />
			                    		
								</li>
			                    <li>
			                    	<label class="frdetails"><spring:message code="failurereport.recommendation" />:</label>
			                    	<div class="float-left">
			                    	
										<table>
											<c:forEach items="${failureReportRecommendations}" var="recom" varStatus="loopStatus" >		
												<c:if test="${ loopStatus.index % 2 eq 0 }">
													<tr>
												</c:if>
													
												<td>
													<input onclick="return false" type="checkbox" name="recommendations" value="${recom.key}" class="pad-left"
																	${form.recommendations.contains(recom.key) ? ' checked' : ''}  />
													${recom.value}
												</td>
															
												<c:if test="${ loopStatus.count % 2 eq 0 }">
													</tr>
												</c:if>
											</c:forEach>
												
										</table>
									</div>
								</li>
								<li id="dispensationCommentsLi" class="${hasDispensation ? 'vis' : 'hid'}">
		                       		<label class="frdetails"><spring:message code="jifaultreport.dispensationcomments" />:</label>
		                       		<form:textarea path="dispensationComments" readonly="true" rows="2" cols="100" />
		                        </li>
			
		                        <li>
		                    		<label class="frdetails"><spring:message code="failurereport.operationcarriedby" />:</label>
		                    		<input onclick="return false" type="radio" value="true" name="operationByTrescal" ${ form.operationByTrescal eq true ? 'checked' : '' } >
									<spring:message code="trescal" />
									<input onclick="return false" type="radio" value="false" name="operationByTrescal" ${ form.operationByTrescal eq false ? 'checked' : '' } >
									<spring:message code="failurereport.qualifiedsupplier" />
		                        </li>
		                        <li>
		                    		<label class="frdetails"><spring:message code="failurereport.operationcarriedon" />:</label>
		                    		<input onclick="return false" type="radio" value="true" name="operationInLaboratory" ${ form.operationInLaboratory eq true ? 'checked' : '' }  >
									<spring:message code="laboratory" />
									<input onclick="return false" type="radio" value="false" name="operationInLaboratory" ${ form.operationInLaboratory eq false ? 'checked' : '' } >
									<spring:message code="failurereport.atcustomersite" />
		                        </li>
		                        <li>
		                       		<label class="frdetails"><spring:message code="comments" />:</label>
		                       		<form:textarea path="managerComments" readonly="true" rows="3" cols="100" />
		                        </li>
		                        <li>
		                            <label class="frdetails"><spring:message code="failurereport.deliverytime" />:</label>
		                            <input readonly name="estDeliveryTime" type="number" value="${ form.estDeliveryTime }" >&nbsp;<spring:message code="days" />
		                        </li>
			                    
		                     	<li>
					               	<div class="displaycolumn">
					                   <fieldset>
					                       <p style="font-size: 105%; text-align: center;">
											<b><span style="color: red;"><spring:message code="jifaultreport.repairestimate" /></span></b>
										</p>
					                       <ol>
					                           <li>
					                               <label style="width: 250px;"><spring:message code="jifaultreport.string17" />:</label>
					                               <input readonly name="estRepairTime" type="number"  value="${ form.estRepairTime }" />
					                           </li>
					                           <li>
					                               <label style="width: 250px;">
					                               		<spring:message code="jifaultreport.string18" /> 
					                               		(${businessCompanyCurrency.currencyERSymbol}):</label>
					                               <input readonly name="valueInBusinessCompanyCurrency" value="${form.valueInBusinessCompanyCurrency}" size="10" />
					                           </li>
					                       </ol>
					                   </fieldset>
					               	</div>
			
					               	<div class="displaycolumn">
					                	<fieldset>
					                    	<p style="font-size: 105%; text-align: center;">
												<b><span style="color: red;"><spring:message code="jifaultreport.adjustestimate" /></span></b>
											</p>
					                       	<ol>
					                           <li>
					                               <label style="width: 250px;"><spring:message
														code="jifaultreport.string19" />:</label>
													<input readonly name="estAdjustTime" type="number" value="${ form.estAdjustTime }" />
					                           </li>
					                           <li>
					                               <label>&nbsp;</label>
					                               <span>&nbsp;</span>
					                           </li>
					                       	</ol>
					                	</fieldset>
					               	</div>
			                	</li>
		               		</ol>
			                    
		                </fieldset>
					</div>
					
					<div id="clientpart-tab" class="hid" >
						<c:if test="${ showClientPart }" >
							<fieldset>
			                   	<legend>
									<spring:message code="stategroup.displayname.decision" />
								</legend>
			
			                	<ol>
			                  		<li>
			                     		<label class="frdetails"><spring:message code="failurereport.approvaltype" />:</label>
			                            <form:select path="clientApprovalType" disabled="true" >
			                            	<form:option value=""></form:option>
											<form:option value="NOT_NECESSARY"><spring:message code="failurereport.notnecessary" /></form:option>
											<form:option value="SIGNATURE"><spring:message code="docs.signature" /></form:option>
											<form:option value="EMAIL"><spring:message code="email" /></form:option>
											<form:option value="ADVESO">Adveso</form:option>
										</form:select>
			                   		</li>
			                   		
			                   		<li>
			                       		<label class="frdetails"><spring:message code="failurereport.clientAlias" />:</label>
										<form:input readonly="true" path="clientAlias"/>
			                       </li>
			                   		
			                   		<li>
			                    		<label class="frdetails"><spring:message code="stategroup.displayname.decision" />:</label>
			                    		
			               				<form:select id="clientApprovalSelect" path="clientApproval" disabled="true" >
				                           	<option value=""></option>
											<form:option value="true"><spring:message code="yes" /></form:option>
											<form:option value="false"><spring:message code="no" /></form:option>
										</form:select>
			                        </li>
			                        
			                        <li>
			                     		<label class="frdetails"><spring:message code="contact" />:</label>
			                     		<links:contactLinkDWRInfo contact="${ failureReport.clientApprovalBy }" rowcount="1"/>
			                     		<c:if test="${ empty failureReport.clientApprovalBy }">
			                     			<spring:message code="jiactions.notspecified" />
			                     		</c:if>
			                   		</li>
			                   		
			                   		<li>
			                     		<label class="frdetails"><spring:message code="date" />:</label>
			                   				<form:input path="clientApprovalOn" id="clientApprovalOn" 
			                   					cssClass="date" readonly="true" />
			                   		</li>
			                   		
			                   		 <li>
			                       		<label class="frdetails"><spring:message code="comments" />:</label>
			                            <form:textarea  path="clientComments" readonly="true" rows="3" cols="100" />
			                        </li>
			                   		
			                   	</ol>
			           		</fieldset>
														
						</c:if>
					</div>
						
					<div id="froutcome-tab" class="hid" >
						<fieldset>
		                	<ol>
		                  		<li>
		                     		<label class="frdetails"><spring:message code="failurereport.outcome" />:</label>
		                            <form:select path="finalOutcome" disabled="true" >
		                            	<form:option value=""></form:option>
		                            	<form:options items="${finaloutcomes}" itemLabel="translation" />
									</form:select>
									<form:errors class="attention" path="finalOutcome"/>
		                   		</li>
	                   		</ol>
	                   	</fieldset>
					</div>
				
				</div>
			</div>
			
			<!-- this div clears floats and restores page flow -->
			<div class="clear"></div>
		
		</div>
		<!-- end of infobox -->
		
		</form:form>
		
	</jsp:body>
</t:crocodileTemplate>	