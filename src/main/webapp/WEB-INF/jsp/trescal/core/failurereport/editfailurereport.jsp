<%-- File name: /trescal/core/pricing/purchaseorder/viewpurchaseorder.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="org.trescal.cwms.core.account.entity.AccountStatus"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="costs" tagdir="/WEB-INF/tags/costs"%>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files"%>
<%@ taglib prefix="purchaseorder" tagdir="/WEB-INF/tags/purchaseorder"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message
				code="jifaultreport.faultreport" /></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type="module" src="script/components/cwms-datetime/cwms-datetime.js"></script>
		<script type='text/javascript'
			src='script/trescal/core/failurereport/editfailurereport.js'></script>
	</jsp:attribute>
	<jsp:body>
	
	<t:showErrors path="form.*" showFieldErrors="true" />

	<form:form modelAttribute="form">
       	<form:hidden id="action" path="action" />
		<form:hidden path="faultRepId" />
		<form:hidden path="jobItemId" />
		<form:hidden path="awaitingFinalisation" />
		<form:hidden path="managerValidation" />
	
		<!-- infobox contains all failure report information and is styled with nifty corners -->
		<div class="infobox">
			<fieldset>
			
				<legend>
					<c:choose>
						<c:when test="${not empty failureReport}">
							<spring:message code="jifaultreport.faultreport" />: <c:out value="${failureReport.faultReportNumber}" />
						</c:when>
						<c:otherwise>
							<spring:message code="failurereport.newfailurereport" />
						</c:otherwise>
					</c:choose>
				</legend>
				<!-- displays content in column 49% wide (left) -->
				<div class="displaycolumn">
					<ol>
						<li>
							<label><spring:message code="jobno" />:</label>
							<span>${ ji.job.jobno }.<links:jobitemLinkDWRInfo jobitem="${ ji }" jicount="false" rowcount="1"></links:jobitemLinkDWRInfo> </span>
						</li>
						<li>
							<label><spring:message code="contact" />:</label>
							<span><links:contactLinkDWRInfo contact="${ ji.job.con }" rowcount="1" /></span>
						</li>
						<li>
							<label><spring:message code="telephone" />:</label>
							<span>${ ji.job.con.telephone }</span>
						</li>
						<li>
							<label><spring:message code="failurereport.validated" />:</label>
							<span>
								<c:if test="${ not empty failureReport && not empty failureReport.managerValidation }">
									<spring:message code="${failureReport.managerValidation}" />
								</c:if>
							</span>
						</li>
						<li>
							<label><spring:message code="failurereport.validatedby" />:</label>
							<span>
								<c:if test="${not empty failureReport && not empty failureReport.managerValidationBy }">
									<links:contactLinkDWRInfo contact="${ failureReport.managerValidationBy }" rowcount="1" />
								</c:if>
							</span>
						</li>
						<li>
							<label><spring:message code="failurereport.validatedon" />:</label>
							<span>
								<c:if test="${not empty failureReport && not empty failureReport.managerValidationOn }">
									<fmt:formatDate type="date" dateStyle="SHORT" value="${ failureReport.managerValidationOn }" />
								</c:if>
							</span>
						</li>
					</ol>														
				</div>
				
				<!-- displays content in column 49% wide (right) -->
				<div class="displaycolumn">
					<ol>
						<li>
							<label><spring:message code="company" />:</label>
							<span><links:companyLinkDWRInfo company="${ ji.job.con.sub.comp }" copy="true" rowcount="1" /></span>
						</li>
						<li>
							<label><spring:message code="subdiv" />:</label>
							<span><links:subdivLinkDWRInfo subdiv="${ ji.job.con.sub }" rowcount="1" /></span>
						</li>
						<%-- Always display source of failure report, for clarity to user / support --%>
						<li>
							<label> <spring:message code="viewprebooking.sourceapi" /> </label>
							<span>${ failureReport.source }</span>
						</li>
						<li>
							<label><spring:message code="failurereport.clientapproval" />:</label>
							<span>
								<c:if test="${ not empty failureReport && not empty failureReport.clientApproval }">
									<spring:message code="${failureReport.clientApproval}" />
								</c:if>
							</span>
						</li>
						<li>
							<label><spring:message code="failurereport.clientresponsefrom" />:</label>
							<span>
								<c:if test="${ not empty failureReport && not empty failureReport.clientApprovalBy }">
									<links:contactLinkDWRInfo contact="${ failureReport.clientApprovalBy }" rowcount="1" />
								</c:if>
								<c:if test="${ not empty failureReport && not empty failureReport.clientAlias }">
									<c:out value="${ failureReport.clientAlias }"/>
								</c:if>
							</span>
						</li>
						<li>
							<label><spring:message code="failurereport.clientresponsereceivedon" />:</label>
							<span>
								<c:if test="${ not empty failureReport && not empty failureReport.clientApprovalOn }">
									<fmt:formatDate type="date" dateStyle="SHORT" value="${ failureReport.clientApprovalOn }" />
								</c:if>
							</span>
						</li>
					</ol>
				</div>
																
			</fieldset>
		</div>
		
		<c:set var="disabledAttribute" value=""/>	<%-- For use in HTML attributes --%>
		<c:set var="disabledValue" value="false"/>	<%-- For use in Spring form tags --%>
		<c:if test="${ form.action == 'EDIT_CLIENT_RESPONSE' }">
			<c:set var='disabledAttribute' value='disabled="disabled"' />
			<c:set var="disabledValue" value="true"/>
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<h5 class="center-0 attention"><spring:message code="failureport.cannotbemodified.sourcetlm" /></h5>
					</div>
				</div>
			</div>
		</c:if>
			
		<!-- infobox contains all failure report information-->
		<div class="infobox">
		
			<fieldset>
	           	<legend>
					<spring:message code="jifaultreport.string2" />
				</legend>
                <ol>
                 	<li>
            			<label class="frdetails"><spring:message
									code="state" />:</label>
						<div class="float-left">
							<table >
								<c:forEach items="${failureReportStates}" var="state"
										varStatus="loopStatus">	
									
									<c:if test="${ loopStatus.index % 2 eq 0 }">
										<tr>
									</c:if>
						
									<td >
										<input type="checkbox" name="states" value="${state.key}" ${disabledAttribute}
											${state.key eq 'OTHER' ? 'onchange="showHideOther($j(this)); return false;"':''}
											class="pad-left" ${form.states.contains(state.key) ? ' checked' : ''} 
											/>
										${state.value}
									</td>
								
									<c:if test="${ loopStatus.count % 2 eq 0 }">
										</tr>
									</c:if>
									
								</c:forEach>
							</table>
							<form:errors class="attention" path="states" />
						</div>
					</li>
					<li id="otherStateLi" class="${ hasStateOther ? 'vis' : 'hid' }">
                   		<label class="frdetails"><spring:message code="jifaultreport.otherstate" />:</label>
                   		<spring:message var="placeholderComments" code='comments'/>
                   		<form:textarea placeholder="${placeholderComments}" path="otherState" id="otherStateTextArea" 
                   		rows="2" cols="100" readonly="${disabledValue}" />				
                   		<form:errors class="attention" path="otherState" />
                    </li>
                    <li>
               			<label class="frdetails"><spring:message code="failurereport.technicianComments" />:</label>
						<form:textarea path="technicianComments" rows="3" cols="100" readonly="${disabledValue}" />
						<form:errors class="attention" path="technicianComments" />
                    </li>
                </ol>
			</fieldset>
					
			<fieldset>
	           	<legend>
					<spring:message code="failurereport.situationintheprocess" />
				</legend>
	            <ol>
	              	<li>
                   		<label class="frdetails"><spring:message
									code="systemdefault.sendfaultreporttoclient" />:</label>
                   		
						<form:radiobutton path="sendToClient" value="true" disabled="${disabledValue}" />
						<spring:message code="yes" />

						<form:radiobutton path="sendToClient" value="false" disabled="${disabledValue}" />
						<spring:message code="no" />

						<form:errors class="attention" path="sendToClient" />
                       </li>
                   	<li>
                   		<label class="frdetails"><spring:message
									code="failurereport.clientresponseneeded" />:</label>
                   		
						<form:radiobutton path="clientResponseNeeded" value="true" disabled="${disabledValue}" />
						<spring:message code="yes" />

						<form:radiobutton path="clientResponseNeeded" value="false" disabled="${disabledValue}" />
						<spring:message code="no" />

						<form:errors class="attention" path="clientResponseNeeded" />
                    </li>
				    <li>
	                   	<label class="frdetails"><spring:message
									code="failurereport.recommendation" />:</label>
	                   	<div class="float-left">
				                    	
							<table>
								<c:forEach items="${failureReportRecommendations}" var="recom"
										varStatus="loopStatus">		
									<c:if test="${ loopStatus.index % 2 eq 0 }">
										<tr>
									</c:if>
										
									<td>
										<input type="checkbox" name="recommendations" value="${recom.key}" ${disabledAttribute} 
											class="pad-left" ${form.recommendations.contains(recom.key) ? ' checked' : ''}
											 />
										${recom.value}
									</td>
												
									<c:if test="${ loopStatus.count % 2 eq 0 }">
										</tr>
									</c:if>
								</c:forEach>
							</table>
							<form:errors class="attention" path="recommendations" />
						</div>
					</li>
					
					<li>
	            		<label class="frdetails"><spring:message code="failurereport.recom.dispensation" />:</label>
	            		<form:checkbox onclick="onClickDispensation()" id="dispensation" path="dispensation"
	            			 disabled="${disabledValue}"/>
						<form:errors class="attention" path="dispensation" />
						<br/>
						<c:set var="dispensationPlaceholder"><spring:message code='jifaultreport.dispensationcomments'/></c:set>
						<form:textarea placeholder="${ dispensationPlaceholder }"
							class="${hasDispensation ? 'vis' : 'hid'}"
							path="dispensationComments" id="dispensationCommentsTextArea"
							rows="2" cols="100" disabled="${disabledValue}" />
						<form:errors class="attention" path="dispensationComments" />
					</li>
					
	                <li>
	             		<label class="frdetails"><spring:message
									code="failurereport.operationcarriedby" />:</label>
                   		<form:radiobutton path="operationByTrescal" value="true" disabled="${disabledValue}" />
						<spring:message code="trescal" />
						<form:radiobutton path="operationByTrescal" value="false" disabled="${disabledValue}" />
						<spring:message code="failurereport.qualifiedsupplier" />
						<form:errors class="attention" path="operationByTrescal" />
					</li>
					
				    <li>
				    	<label class="frdetails"><spring:message
									code="failurereport.operationcarriedon" />:</label>
                   		<form:radiobutton path="operationInLaboratory" value="true" disabled="${disabledValue}" />
						<spring:message code="laboratory" />
						<form:radiobutton path="operationInLaboratory" value="false" disabled="${disabledValue}" />
						<spring:message code="failurereport.atcustomersite" />
						<form:errors class="attention" path="operationInLaboratory" />
					</li>
					
					<li>
				    	<label class="frdetails"><spring:message code="comments" />:</label>
				    	<form:textarea path="managerComments" id="managerCommentsTextArea" rows="3" cols="100" 
				    	 disabled="${disabledValue}" />
						<form:errors class="attention" path="managerComments" />
				    </li>
				    
				    <li>
		                <label class="frdetails"><spring:message
									code="failurereport.deliverytime" />:</label>
		                <form:input path="estDeliveryTime" disabled="${disabledValue}" />
		                &nbsp;<spring:message code="days" />
						<form:errors class="attention" path="estDeliveryTime" />
		            </li>
				                    
	               	<li>
		               	<div class="displaycolumn">
		                   <fieldset>
								<p style="font-size: 105%; text-align: center;">
									<b><span style="color: red;"><spring:message
													code="jifaultreport.repairestimate" /></span></b>
								</p>
							    <ol>
							    	<li>
	                               		<label style="width: 250px;"><spring:message
													code="jifaultreport.string17" />:</label>
	                               		<form:select path="estRepairTime" items="${selectTimes}" itemLabel="value" itemValue="key" 
	                               			disabled="${disabledValue}" />
										<form:errors class="estRepairTime" path="" />
									</li>
							        
									<li>
							        	<label style="width: 250px;">
							            	<spring:message code="jifaultreport.string18" /> 
							                (${businessCompanyCurrency.currencyERSymbol}):</label>
							            <form:input path="valueInBusinessCompanyCurrency" size="10" disabled="${disabledValue}" />
										<form:errors class="valueInBusinessCompanyCurrency" path="" />
								    </li>
								</ol>
							</fieldset>
						</div>
					
						<div class="displaycolumn">
						  	<fieldset>
						       	<p style="font-size: 105%; text-align: center;">
									<b><span style="color: red;"><spring:message
													code="jifaultreport.adjustestimate" /></span></b>
								</p>
							   	<ol>
								    <li>
							    	    <label style="width: 250px;"><spring:message
													code="jifaultreport.string19" />:</label>
										<form:select path="estAdjustTime" items="${selectTimes}" 
											itemLabel="value" itemValue="key" disabled="${disabledValue}" />
										<form:errors class="attention" path="estAdjustTime" />
							        </li>
							        <li>
							        	<label>&nbsp;</label>
							            <span>&nbsp;</span>
							        </li>
							    </ol>
							</fieldset>
						</div>
					 </li>
		     	</ol>
			</fieldset>
			
			<c:if test="${ showClientPart }" >
				<fieldset>

                   	<legend>
						<spring:message code="stategroup.displayname.decision" />
					</legend>

                	<ol>
                  		<li>
                     		<label class="frdetails"><spring:message code="failurereport.approvaltype" />:</label>
                            <form:select path="clientApprovalType" >
                            	<form:option value=""></form:option>
								<form:option value="NOT_NECESSARY"><spring:message code="failurereport.notnecessary" /></form:option>
								<form:option value="SIGNATURE"><spring:message code="docs.signature" /></form:option>
								<form:option value="EMAIL"><spring:message code="email" /></form:option>
								<form:option value="ADVESO">Adveso</form:option>
							</form:select>
							<form:errors class="attention" path="clientApprovalType"/>
                   		</li>
                   		
                   		<li>
                    		<label class="frdetails"><spring:message code="stategroup.displayname.decision" />:</label>
                    		
               				<form:select id="clientApprovalSelect" path="clientApproval"
               						onchange="onChangeClientApprovalSelect()" >
	                           		<option value=""></option>
									<form:option value="true"><spring:message code="yes" /></form:option>
									<form:option value="false"><spring:message code="no" /></form:option>
							</form:select>
							<form:errors class="attention" path="clientApproval"/>
							<br/>
							<form:select id="clientOutcomeSelect" path="clientOutcome" cssClass="hid" >
								<option value=""></option>
								<form:options items="${failureReportRecommendations}" itemLabel="value" itemValue="key"></form:options>
							</form:select>
							<span id="clientOutcomeText" class="hid" ><spring:message code="failurereport.clientrecommendeddiffrenetoutcome" /></span>
							<form:errors class="attention" path="clientOutcome"/>

                       </li>
                       
                   		<%-- Moved client approval date to before contact plug-in, to prevent overlay issues when date is at end of scrolled browser window --%>
                   		<li>
                     		<label class="frdetails"><spring:message code="date" />:</label>
                     		<div>
								<form:input  path="clientApprovalOn" type="datetime-local" step="60"/>
								<form:errors class="attention" path="clientApprovalOn"/>
                     		</div>
                   		</li>
                   		
                        <li>
                     		<label class="frdetails"><spring:message code="contact" />:</label>
                     		
               				<div id="cascadeSearchPlugin">
								<input type="hidden" id="cascadeRules" value="subdiv,contact" />
								<input type="hidden" id="compCoroles" value="client,business" />
								<input type="hidden" id="prefillIds"
									value="${form.coid},${form.subdivid},${form.personid}" />
							</div>
							<form:errors class="attention" path="personid"/>
                   		</li>
                   		
                   		<li>
                       		<label class="frdetails"><spring:message code="failurereport.clientAlias" />:</label>
							<form:input path="clientAlias" />
							<form:errors class="attention" path="clientAlias"/>
                       </li>
                   		
                   		 <li>
                       		<label class="frdetails"><spring:message code="comments" />:</label>
                            <form:textarea path="clientComments" rows="3" cols="100" />
							<form:errors class="attention" path="clientComments"/>
                        </li>
                   		
                   	</ol>
           		</fieldset>
       		</c:if>
       		<c:if test="${ showFinalOutcomePart eq true }" >
				<fieldset>

                   	<legend>
						<spring:message code="failurereport.outcome" />
					</legend>

                	<ol>
                  		<li>
                     		<label class="frdetails"><spring:message code="failurereport.outcome" />:</label>
                            <form:select path="finalOutcome" disabled="${ canEditFinalOutcomePart eq true ? 'false' : 'true' }" >
                            	<form:option value=""></form:option>
                            	<form:options items="${finaloutcomes}" itemLabel="translation" />
							</form:select>
							<form:errors class="attention" path="finalOutcome"/>
                   		</li>
              		</ol>
              	</fieldset>
           	</c:if>
			
			<div class="text-center">
			 	<input type="submit" value="<spring:message code='save'/>" />
			 	
			 	<c:set var="validateButton" ><spring:message code="failurereport.saveandvalidate"/></c:set>
               	<c:if test="${ not empty form.managerValidation && form.managerValidation }">
               		<c:set var="validateButton" ><spring:message code="failurereport.validated"/></c:set>
               	</c:if>
				<input type="submit" name="validate" value="${validateButton}" onClick=" $j('#action').attr('value','VALIDATE');" 
						${ not allowValidation ? 'disabled': '' } />
			</div>
			
		</div>
		
	</form:form>
	</jsp:body>
</t:crocodileTemplate>
