<%-- File name: /trescal/core/procedure/viewworkinstruction/standards.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<jsp:useBean id="date" class="java.util.Date" />


<t:crocodileTemplate>
	<jsp:attribute name="header">
		<script type='text/javascript' src='script/trescal/core/procedure/DefaultStandards1.js'></script>
		<span class="headtext">
			<spring:message code="viewworkinstruction.viewworkinstruction" />
		</span>
	</jsp:attribute>
	<jsp:body>
	<%@include file="header.jsp"%>
	<div id="subnav">
		<dl>			
			<dt><a href="<c:url value="/editworkinstruction.htm?wid=${wi.id}"/>" title="<spring:message code="viewworkinstruction.editworkinstructiondetails"/>"><spring:message code="edit"/></a></dt>
			<dt><a href="<c:url value="/viewworkinstruction?wid=${wi.id}"/>" class="selected"><spring:message code="viewworkinstruction.standards"/> (<span class="stdsSizeSpan">${wi.standards.size()}</span>)</a></dt>
		</dl>
	</div>
	
	<div id="standards-tab">
		<table class="default2" id="wiviewstds" summary="This table displays all standards added to the current work instruction">
			<thead>
				<tr>
					<td colspan="8"><spring:message code="viewworkinstruction.standards"/> (<span class="stdsSizeSpan">${wi.standards.size()}</span>)</td>
				</tr>
				<tr>
					<th class="wistdbar" scope="col"><spring:message code="barcode"/></th>
					<th class="wistdinst" scope="col"><spring:message code="instrument"/></th>
					<th class="wistdserial" scope="col"><spring:message code="serialno"/></th>
					<th class="wistdplant" scope="col"><spring:message code="plantno"/></th>
					<th class="wistdcaldate" scope="col"><spring:message code="caldate"/></th>
					<th class="wistdenf" scope="col"><spring:message code="enforce"/></th>
					<th class="wistdaddby" scope="col"><spring:message code="addedby"/></th>
					<th class="wistddel" scope="col"><spring:message code="delete"/></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="8">
						<a href="#" class="mainlink-float" onclick=" addStandardsContent('wiviewstds', ${wi.id}, '${wi.title}', '', 'WORKINSTRUCTION'); return false; " title=""><spring:message code="viewprocedure.addstandards"/></a>
					</td>
				</tr>
			</tfoot>
			<tbody>
			<c:choose>
				<c:when test="${empty wi.standards}">
					<tr>
						<td colspan="8" class="bold center">
							<spring:message code="viewprocedure.therearenostandardstodisplay"/>
						</td>
					</tr>
				</c:when>
				<c:otherwise>
					<c:forEach var="stand" items="${wi.standards}">
						<c:choose>
							<c:when test="${stand.instrument.outOfCalibration == true}">
								<%-- <c:set var="outOfCal" value="<br/><span class="attention"><spring:message code="viewworkinstruction.outofcalibration"/></span>"/> --%>
								<c:set var="rowcolor" value="highlight"/>
							</c:when>
							<c:otherwise>
								<c:set var="outOfCal" value=""/>
								<c:set var="rowcolor" value=""/>
							</c:otherwise>
						</c:choose>
						<tr id="std${instrument.plantid}" class="${rowcolor}">
							<td class="wistdbar">
								<links:instrumentLinkDWRInfo instrument="${stand.instrument}" rowcount="0" displayBarcode="true" displayName="false" displayCalTimescale="true" />
							</td>
							<td>
								<cwms:showinstrument instrument="${stand.instrument}"/>
							</td>
							<td>${stand.instrument.serialno}</td>
							<td>${stand.instrument.plantno}</td>
							<td><fmt:formatDate value="${stand.instrument.nextCalDueDate}" type="date" dateStyle="SHORT" /></td>
							<td class="center">
								<a href="#" onclick=" updateDefStdEnforceUse('wiviewstds', ${stand.id}, ${stand.instrument.plantid}; return false; ">
									<c:choose>
										<c:when test="${stand.enforceUse == true}">
											<img src="img/icons/bullet-tick.png" width="10" height="10" alt="<spring:message code="viewworkinstruction.enforceuse"/>" title="<spring:message code="viewworkinstruction.enforceuse"/>" />
										</c:when>
										<c:otherwise>
											<img src="img/icons/bullet-cross.png" width="10" height="10" alt="<spring:message code="viewworkinstruction.donotenforce"/>" title="<spring:message code="viewworkinstruction.donotenforce"/>" />
										</c:otherwise>
									</c:choose>
								</a>
							</td>
							<td>
								<fmt:formatDate value="${stand.setOn}" type="date" dateStyle="SHORT" /><br/>
														${stand.setBy.name}
							</td>
							<td class="center">
								<a href="#" onclick=" deleteDefStd('wiviewstds', ${stand.instrument.plantid}, ${stand.id}); return false; ">
									<img src="img/icons/delete.png" width="16" height="16" alt="<spring:message code="viewworkinstruction.deleteprocedurestandard"/>" title="<spring:message code="viewworkinstruction.deleteprocedurestandard"/>" />
								</a>
							</td>	
						</tr>	
					</c:forEach>
				</c:otherwise>			
			</c:choose>
		</tbody>
	</table>
</div>
</jsp:body>
</t:crocodileTemplate>