<%-- File name: /trescal/core/procedure/searchworkinstructions.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<spring:message code="searchworkinstructions.workinstructionbrowser" />
		</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='module' src='script/components/search-plugins/cwms-description-search/cwms-description-search.js'></script>
	</jsp:attribute>
	<jsp:body>
	
	<!-- infobox contains all form elements for procedure searching and is styled with nifty corners -->				
	<div class="infobox">
		<a href="<c:url value="/editworkinstruction.htm"/>" class="mainlink-float"><spring:message code="searchworkinstructions.newworkinstruction" /></a>
		<!-- clear floats and restore page flow -->
		<div class="clear"></div>
		<form:form modelAttribute="command" method="POST">
			<form:errors path="*">
                    		<div class="warningBox1">
                        		<div class="warningBox2">
                            		<div class="warningBox3">
                                		<h5 class="center-0 attention">
                                    		<spring:message code="searchprocedureform.oneormoreerrorsoccuredsearchingforprocedures" />
                                		</h5>
                                		<c:forEach var="e" items="${messages}">
                                    		<div class="center attention"><c:out value="${e}"/></div>
                                		</c:forEach>
                            		</div>
                        		</div>
                    		</div>
            </form:errors>
			<fieldset id="work_instruction_search">
				<legend><spring:message code="menu.calibration.searchworkinstructions" /></legend>
					<ol>
						<li>
							<label><spring:message code="businesscompany" />:</label>
							<span><c:out value="${allocatedCompany.value}"/></span>
						</li>					
						<li>
							<label><spring:message code="businesssubdivision" />:</label>
							<span><c:out value="${allocatedSubdiv.value}"/></span> 
						</li>					
						<li>
							<label><spring:message code="title" />:</label>
							<form:input path="title" size="80" tabindex="3" /><br>
							<font color="red"><form:errors path="title"/></font>
						</li>											
						<li>
							<label><spring:message code="name" />:</label>
							<form:input path="name" size="80" tabindex="4" /><br>
							<font color="red"><form:errors path="name"/></font>
						</li>
						<li>
							<label><spring:message code="description.description"/>:</label>
							<cwms-description-search-generic name="descid" value=""></cwms-description-search-generic>
							<font color="red"><form:errors path="descid"/></font>
						</li>
						<li>
							<label>&nbsp;</label>
							<span><input type="submit" name="search" value="<spring:message code="search"/>" tabindex="5" /></span>
						</li>
					</ol>
				</fieldset>
			</form:form>
		</div>
		<!-- end of infobox div -->
	</jsp:body>
</t:crocodileTemplate>