<%-- File name: /trescal/core/procedure/editcapabilityfilters.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="procedure" tagdir="/WEB-INF/tags/procedure" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="editcapability.editcapabilityfilters"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script>
			var insertionFilter = ${form.filters.size()};
		</script>
		<script src='script/trescal/core/procedure/EditCapabilityFilters.js'></script>
		<script type="module" src="script/components/cwms-tooltips/cwms-capability-link-and-tooltip/cwms-capability-link-and-tooltip.js"></script>
    	<script type="module" src="script/components/capability/cwms-capabilityfilters-edit/cwms-capabilityfilters-edit.js"></script>
	</jsp:attribute>
	<jsp:body>
		<t:showErrors path="form.*" showFieldErrors="true" />
		<div class="infobox" id="editcapabilityfilters">
			<fieldset>
				<legend><spring:message code="editcapability.editcapabilityfilters"/></legend>
				<div class="displaycolumn">
					<ol>
						<li>
							<label><spring:message code="editproc.refno"/>:</label>
							<span>
							<cwms-capability-link-and-tooltip reference="${procedure.reference}" id=${procedure.id} ></cwms-capability-link-and-tooltip>
							</span>
						</li>
						<li>
							<label><spring:message code="editproc.capabilityname"/>:</label>
							<span>${procedure.name}</span>
						</li>
						<li>
							<label><spring:message code="sub-family"/>:</label>
							<span>
								<c:choose>
									<c:when test="${not empty procedure.subfamily}">
										<cwms:besttranslation translations="${procedure.subfamily.translations}"/>
									</c:when>
									<c:otherwise><spring:message code="notapplicable"/></c:otherwise>
								</c:choose>
							</span>
						</li>
					</ol>
				</div>
				<div class="displaycolumn">
					<ol>
						<li>
							<label><spring:message code="company"/>:</label>
							<span>${procedure.organisation.comp.coname}</span>
						</li>
						<li>
							<label><spring:message code="company.role"/>:</label>
							<span>${procedure.organisation.comp.companyRole.getMessage()}</span>
						</li>
						<li>
							<label><spring:message code="subdivision"/>:</label>
							<span>${procedure.organisation.subname}</span>
						</li>
					</ol>
				</div>
				<!-- clear floats and restore page flow -->
				<div class="clear"></div>
				<div class="displaycolumn-100">
					<ol>				
						<li>
							<div class="float-left">
								<label><spring:message code="description"/>:</label>
								${procedure.description.replaceAll("\\n","<br/>")}
							</div>
							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
						</li>
					</ol>
				</div>
			</fieldset>
			
			<form:form modelAttribute="form">
                <cwms-capabilityfilters-edit 
					capabilities="${cwms:objectToJson(form.filters)}"
					serviceTypes="${cwms:objectToJson(serviceTypes)}"
					filterTypes="${cwms:objectToJson(filterTypes)}"
					calibrationProcesess="${cwms:objectToJson(calibrationProcesess)}"
					workInstruktions="${cwms:objectToJson(workInstruktions)}"
					accreditedLabs="${cwms:objectToJson(accreditedLabs)}"
					requirementTypes="${cwms:objectToJson(requirementTypes)}"
					categories="${cwms:objectToJson(categories)}"
				></cwms-capabilityfilters-edit>
			<br>
			<form:button name="submit" value="submit"><spring:message code="save"/></form:button>
		</form:form>
		</div>		
	</jsp:body>
</t:crocodileTemplate>