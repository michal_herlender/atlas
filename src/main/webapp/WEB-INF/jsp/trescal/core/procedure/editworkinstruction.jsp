<%-- File name: /trescal/core/procedure/editworkinstruction.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<c:choose>
				<c:when test="${newWorkInstruction}">
					<spring:message code="editworkinstruction.newworkinstruction" />
				</c:when>
				<c:otherwise>
					<spring:message code="editworkinstruction.editworkinstruction" />
				</c:otherwise>
			</c:choose>
		</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='module' src='script/components/search-plugins/cwms-description-search/cwms-description-search.js'></script>
	</jsp:attribute>
	<jsp:body>
		<t:showErrors path="form.*" showFieldErrors="true" />
		<div class="infobox" id="editprocedureform">
			<form:form modelAttribute="form">
				<fieldset>
					<legend>
						<c:choose>
							<c:when test="${newWorkInstruction}">
								<spring:message code="editworkinstruction.createnewworkinstruction"/>
							</c:when>
							<c:otherwise>
								<spring:message code="editworkinstruction.editworkinstruction"/> (<a href="<c:url value="/viewworkinstruction?wid=${form.id}"/>" class="mainlink">${form.name}</a>)
							</c:otherwise>
						</c:choose>
					</legend>
					<ol>
						<li>
							<label><spring:message code="businesscompany" />:</label>
							<span><c:out value="${allocatedCompanyName}"/></span>
						</li>					
						<li>
							<label><spring:message code="businesssubdivision" />:</label>
							<span><c:out value="${allocatedSubdivName}"/></span>
						</li>					
						<li>
							<label><spring:message code="name"/><c:out value=": *"/></label>
							<form:input path="name" size="100"/>
							<span class="attention"><form:errors path="name" /></span>
						</li>
						<li>
							<label><spring:message code="title"/><c:out value=": *"/></label>
							<form:input path="title" size="100"/>
							<span class="attention"><form:errors path="title" /></span>
						</li>
						<li>
							<label><spring:message code="description.description"/>:</label>
							<cwms-description-search-generic defaultvalue="${form.description}" name="descid" value="${form.descid}"></cwms-description-search-generic>
							<form:errors path="descid" class="attention"/>
						</li>							
						<li>
							<form:hidden path="id"/>
							<label>&nbsp;</label>
							<input type="submit" name="submit" id="submit" value="Save"/>
						</li>
					</ol>
				</fieldset>
			</form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>