<%-- File name: /trescal/core/procedure/editprocedurecategories.jsp --%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<spring:message code="company.editprocedurecategories" />
		</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/procedure/EditProcedureCategories.js'></script>
		<script>
			// variable needed for tracking insertion point for new inputs
			var insertionIndex = ${command.inputDTOs.size()};
		</script>
	</jsp:attribute>
	<jsp:body>
		<t:showErrors path="command.*" showFieldErrors="true" />
		<!-- this div contains all form elements and has nifty corners applied -->
		<div class="infobox" id="editprocedureform">
			<form:form modelAttribute="command" method="post">
				<table class="default2 editprocedurecategories">
					<thead>
                    <tr>
                        <td colspan="7">
                            <a class="mainlink"
                               href="viewsub.htm?load-tab=categories&subdivid=${subdiv.id}">${subdiv.subname}</a>&nbsp;<spring:message
                                code="company.procedurecategories"/>
                        </td>
                    </tr>
                    <tr>
                        <th class="department" scope="col"><spring:message code="company.department"/></th>
                        <th class="depdefault" scope="col"><spring:message code="default"/></th>
                        <th class="name" scope="col"><spring:message code="name"/></th>
                        <th class="description" scope="col"><spring:message code="description"/></th>
                        <th class="traceable" scope="col"><spring:message code="procedurecategory.traceable"/></th>
                        <th class="procedures" scope="col"><spring:message code="capabilities"/></th>
                        <th class="add" scope="col"><spring:message code="add"/></th>
                        <th class="delete" scope="col"><spring:message code="delete"/></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="dept" items="${deptDTOs}" varStatus="deptStatus">
                        <c:forEach var="inputDTO" items="${command.inputDTOs}" varStatus="inputDTOStatus">
                            <c:if test="${inputDTO.deptId == dept.value}">
                                <c:set var="procedureCount" value="0"/>
									<c:if test="${not empty inputDTO.id && procedureCounts.containsKey(inputDTO.id)}">
										<c:set var="procedureCount" value="${procedureCounts.get(inputDTO.id)}" />
									</c:if>
									<c:set var="rowId" value="row-${inputDTOStatus.index}" />
											<tr id="${rowId}">
												<td>
													<c:choose>
														<c:when test="${inputDTO.departmentDefault}">
															<c:out value="${dept.key}" />
														</c:when>
														<c:otherwise>
															<form:select path="inputDTOs[${inputDTOStatus.index}].deptId" items="${deptDTOs}" itemLabel="key" itemValue="value" />
														</c:otherwise>
													</c:choose>
													<form:hidden path="inputDTOs[${inputDTOStatus.index}].id" />
                                                </td>
                                                <td><spring:message code="${inputDTO.departmentDefault}"/></td>
                                                <td>
                                                    <form:input path="inputDTOs[${inputDTOStatus.index}].name"
                                                                size="25"/>
                                                    <form:errors path="inputDTOs[${inputDTOStatus.index}].name"
                                                                 class="attention"/>
                                                </td>
                                                <td>
                                                    <form:input path="inputDTOs[${inputDTOStatus.index}].description"
                                                                size="50"/>
                                                    <form:errors path="inputDTOs[${inputDTOStatus.index}].description"
                                                                 class="attention"/>
                                                </td>
                                                <td>
                                                    <form:checkbox
                                                            path="inputDTOs[${inputDTOStatus.index}].splitTraceable"/>
                                                </td>
                                                <td>${procedureCount}</td>
                                                <td>
                                                    <c:choose>
                                                        <c:when test="${inputDTO.departmentDefault}">
                                                            <img src="img/icons/add.png" height="20" width="20"
                                                                 alt="<spring:message code="company.addprocedurecategory"/>"
                                                                 onClick="addInputRow('${dept.value}'); "/>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <form:hidden path="inputDTOs[${inputDTOStatus.index}].add"/>
                                                        </c:otherwise>
                                                    </c:choose>
												</td>
												<td>
													<c:if test="${not inputDTO.departmentDefault}">
														<c:choose>
															<c:when test="${inputDTO.add}">
																<img src="img/icons/delete.png" height="20" width="20" alt="<spring:message code="delete"/>" onClick=" deleteInputRow(this); " />
															</c:when>
															<c:otherwise>
																<form:checkbox path="inputDTOs[${inputDTOStatus.index}].delete" />
															</c:otherwise>
														</c:choose>
														<c:if test="${procedureCount > 0}">
															<spring:message var="warningText" code="procedurecategory.deletewarning"/>
															<img src="img/icons/info-red.png" width="12" height="12" alt="${warningText}" title="${warningText}"/>
														</c:if>
													</c:if>
												</td>
											</tr>
								</c:if>
							</c:forEach>
							<tr id="templaterow-${dept.value}" class="hid">
								<td>
									${dept.key}
									<input type="hidden" id="template_${dept.value}_deptId" value="${dept.value}" />
								</td>
								<td><spring:message code="no" /></td>
								<td>
									<input type="text" id="template_${dept.value}_name" size="25" />
								</td>
								<td>
									<input type="text" id="template_${dept.value}_description" size="50" />
								</td>
								<td>0</td>
								<td>
									<input type="hidden" id="template_${dept.value}_add" value="true" />
								</td>
								<td><img src="img/icons/delete.png" height="20" width="20" alt="<spring:message code="delete"/>" onClick=" deleteInputRow(this); " /></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<input type="submit" id="submit" value="<spring:message code='update'/>" /> 
			</form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>