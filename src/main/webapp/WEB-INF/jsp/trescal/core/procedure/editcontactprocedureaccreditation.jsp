<%-- File name: /trescal/core/procedure/editcontactprocedureaccreditation.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="editcontactprocedureauthorisation.editauthorisation"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/procedure/EditContactProcedureAccreditation.js'></script>
	</jsp:attribute>
	<jsp:body>
		<form:errors path="command.*">
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<h5 class="center-0 attention"><spring:message code="error.save"/>:</h5>
						<c:forEach var="e" items="${messages}">
							<div class="center attention"><c:out value="${e}"/></div>
						</c:forEach>
					</div>
				</div>
			</div>
		</form:errors>
		<!-- infobox div with nifty corners applied -->
		<div class="infobox" id="editcontprocaccredform">
			<a href='viewcontactprocedureaccreditations.htm?personid=${command.contact.personid}' class="mainlink">
				<spring:message code="editcontactprocedureauthorisation.backtoauthorisationlist"/>
			</a>
			<form:form modelAttribute="command" method="post">
				<fieldset>
					<legend>
						<spring:message code="editcontactprocedureauthorisation.editauthorisationfor"/>
						${command.contact.name}
						<spring:message code="editcontactprocedureauthorisation.forprocedure"/> 
						(<links:procedureLinkDWRInfo displayName="true" capability="${command.capability}" rowcount="0"/>)
					</legend>
					<ol>
						<c:forEach var="caltype" items="${command.calTypes}" varStatus="loopStatus">
							<li>
								<label><cwms:besttranslation translations="${caltype.serviceType.longnameTranslation}"/>:</label>
								<c:set var="currentAcc" value="${null}"/>
								<c:forEach var="ca" items="${command.currentCapabilityAuthorizations}">
									<c:if test="${ca.caltype.calTypeId == caltype.calTypeId}">
										<c:set var="currentAcc" value="${ca}"/>
									</c:if>
								</c:forEach>
								<form:select path="accredLevels[${loopStatus.index}]">
									<form:option value=""><spring:message code="editcontactprocedureauthorisation.pleaseselect"/></form:option>
									<form:options items="${caltype.accreditationLevels}" itemValue="level" itemLabel="level.message"/>
								</form:select>
								<form:input type="date" path="accredOns[${loopStatus.index}]" id="${caltype.serviceType.shortName.toLowerCase()}"/>
								<span class="attention"><form:errors path="accredOns[${loopStatus.index}]"/></span>
								<c:if test="${currentAcc != null && currentAcc.awardedBy != null && currentAcc.awardedBy.name != ''}">
									[<span style="color:gray;">${currentAcc.awardedBy.name}</span>]
								</c:if>
							</li>
						</c:forEach>
						<li>
							<span class="attention">
								<spring:message code="editcontactprocedureauthorisation.string1"/>
							</span>
						</li>
						<li>
							<label>&nbsp;</label>
							<input type="submit" name="submit" id="submit" value="<spring:message code='save'/>"/>
						</li>
					</ol>
				</fieldset>
			</form:form>
		</div>
		<!-- end of infobox div -->
	</jsp:body>
</t:crocodileTemplate>