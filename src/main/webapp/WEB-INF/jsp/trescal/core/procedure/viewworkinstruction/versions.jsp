<%-- File name: /trescal/core/procedure/viewworkinstruction/versions.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<jsp:useBean id="date" class="java.util.Date" />

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<spring:message code="viewworkinstruction.viewworkinstruction" />
		</span>
	</jsp:attribute>
	<jsp:body>
	<%@include file="header.jsp"%>
	<div id="subnav">
		<dl>			
			<dt><a href="<c:url value="/viewworkinstruction/versions?wid=${form.wi.id}" />" class="selected"><spring:message code="viewworkinstruction.versions"/></a></dt>						
			<dt><a href="<c:url value="/editworkinstruction.htm?wid=${form.wi.id}"/>" title="<spring:message code="viewworkinstruction.editworkinstructiondetails"/>"><spring:message code="edit"/></a></dt>
			<dt><a href="<c:url value="/editworkinstructionversion.htm?wid=${form.wi.id}"/>" title="<spring:message code="viewworkinstruction.createnewversionofworkinstruction"/>"><spring:message code="viewworkinstruction.newversion"/></a></dt>
			<dt><a href="<c:url value="/viewworkinstruction/standards?wid=${form.wi.id}"/>"><spring:message code="viewworkinstruction.standards"/> (<span class="stdsSizeSpan">${form.wi.standards.size()}</span>)</a></dt>
		</dl>
	</div>

	<div class="infobox">			
		<div id="versions-tab">		
			<table class="default2" id="wiversionstable" summary="<spring:message code="viewworkinstruction.thistablelistsallversionsofthecurrentworkinstruction"/>">
				<thead>
					<tr>
						<td colspan="4"><spring:message code="viewworkinstruction.versions"/> ${form.wi.versions.size()}</td>
					</tr>
					<tr>
						<th class="version" scope="col"><spring:message code="viewworkinstruction.version"/></th>
						<th class="state" scope="col"><spring:message code="state"/></th>
						<th class="modon" scope="col"><spring:message code="viewworkinstruction.modifiedon"/></th>
						<th class="modby" scope="col"><spring:message code="viewworkinstruction.modifiedby"/></th>
						<%-- <th class="edit" scope="col"><spring:message code="viewworkinstruction.editwi"/></th>
						<th class="run" scope="col"><spring:message code="viewworkinstruction.run(adhocnojob)"/></th> --%>											
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="6">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
					<c:choose>
						<c:when test="${form.wi.versions.size() < 1}">
							<tr>
								<td colspan="6" class="bold center">
									<spring:message code="searchprocedureresults.therearenoresultstodisplay"/>
								</td>
							</tr>
						</c:when>
						<c:otherwise>
							<c:forEach var="WorkInstructionVersion" items="${form.wi.versions}">
								<tr>
									<td>
										<a href="<c:url value = "/viewworkinstructionversion.htm?wivid=${WorkInstructionVersion.id}"/>" class="mainlink"><spring:message code="viewworkinstruction.version"/> ${WorkInstructionVersion.versionNumber}</a>
									</td>
									<td>${WorkInstructionVersion.state}</td>
									<td><fmt:formatDate value="${WorkInstructionVersion.lastEditDate}" type="both" dateStyle="SHORT" timeStyle="SHORT" /></td>
									<td>${WorkInstructionVersion.lastEditedBy.name}</td>
									<%-- <td class="center">
										<a href="#springUrl('/editproc.htm')?file=${WorkInstructionVersion.contentPath}" onclick=" event.preventDefault(); window.open('' + this.href + ''); ">
											<img src="img/icons/edit_wi.png" width="16" height="16" alt="<spring:message code="editworkinstruction.editworkinstruction"/>" title="<spring:message code="editworkinstruction.editworkinstruction"/>" />
										</a>
									</td>
									<td class="center">
										<a href="#" onclick=" runWorkInstruction('${WorkInstructionVersion.contentPath}','$form.xindiceRoot','$form.xindiceJobs','$form.xindiceProcedures'); ">
											<img src="img/icons/cali_next.png" width="16" height="16" alt="<spring:message code="viewworkinstruction.runadhoc"/>" title="<spring:message code="viewworkinstruction.runadhoc"/>"/>
										</a>
									</td> --%>
								</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</tbody>				
			</table>				
		</div>
	</div>					
	</jsp:body>
</t:crocodileTemplate>