<%-- File name: /trescal/core/procedure/viewprocedures.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address" %>
<%@ taglib prefix="company" tagdir="/WEB-INF/tags/company" %>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="viewprocedures.viewprocedures"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/procedure/ViewProcedures.js'></script>
	</jsp:attribute>
	<jsp:body>
		<!-- sub navigation menu which displays and hides sections of page as well as linking to external pages -->
		<c:set var="numOfDepts" value="${departments.size()}"/>
		<div id="subnav">
			<dl>
				<c:set var="max" value="6"/>
				<c:if test="${numOfDepts < 6}"><c:set var="max" value="${numOfDepts}"/></c:if>
				<c:forEach var="department" items="${departments.subList(0, max)}">
					<c:set var="selectedClass" value="${department.key == selectedKey ? 'selected' : '' }" />
					<dt><a href="viewprocedures.htm?deptid=${department.key}" id="dept${department.key}-link" class="${selectedClass}">${department.value}</a></dt>
				</c:forEach>
			</dl>
		</div>
		<c:if test="${numOfDepts > 6}">
			<div id="subnav2">
				<dl>
					<c:set var="max" value="12"/>
					<c:if test="${numOfDepts < 12}"><c:set var="max" value="${numOfDepts}"/></c:if>
					<c:forEach var="department" items="${departments.subList(6, max)}">
						<c:set var="selectedClass" value="${department.key == selectedKey ? 'selected' : '' }" />
						<dt><a href="viewprocedures.htm?deptid=${department.key}" id="dept${department.key}-link" class="${selectedClass}">${department.value}</a></dt>
					</c:forEach>
				</dl>
			</div>
		</c:if>
		<c:if test="${numOfDepts > 12}">
			<div id="subnav3">
				<dl>
					<c:forEach var="department" items="${departments.subList(12, numOfDepts)}">
						<c:set var="selectedClass" value="${department.key == selectedKey ? 'selected' : '' }" />
						<dt><a href="viewprocedures.htm?deptid=${department.key}" id="dept${department.key}-link" class="${selectedClass}">${department.value}</a></dt>
					</c:forEach>
				</dl>
			</div>
		</c:if>
		<!-- end of sub navigation menu -->
		<!-- infobox div with nifty corners applied -->
		<div class="infobox">
			<c:choose>
				<c:when test="${not empty department}">
					<!-- specific department selected -->
					<div class="center"><h4>${department.name} [${department.subdiv.subname}]</h4></div>
					<c:forEach var="category" items="${categoryMap.keySet()}">
						<div class="left">
							<strong>${category.name}</strong>
							<c:if test="${canReallocate}">
								<c:out value=" - "/>
								<a class="mainlink" href="recategoriseprocedures.htm?categoryid=${category.id}">
									<spring:message code="capability.recategoriseprocedures" />
								</a>
							</c:if>
						</div>
						<c:choose>
							<c:when test="${empty categoryMap.get(category)}">
								<div class="padding5 padding20 bold">
									<spring:message code="viewprocedure.noprocedureforcategory"/>
								</div>		
							</c:when>
							<c:otherwise>
								<ul>
									<c:forEach var="procedure" items="${categoryMap.get(category)}">
										<div class="padding5 padding20">
											<links:procedureLinkDWRInfo displayName="false" capability="${procedure}" rowcount="0" />
											<c:set var="inactiveProcStyle" value=""/>
											<c:if test="${!procedure.active}">
												<c:set var="inactiveProcStyle" value="color:grey"/>
											</c:if>
											<span style="${inactiveProcStyle}">
												- ${procedure.name}
												<c:if test="${!procedure.active}">
													[${procedure.deactivatedBy.name} - <fmt:formatDate type="date" dateStyle="SHORT" value="${procedure.deactivatedOn}"/>]
												</c:if>
											</span>
										</div>
									</c:forEach>
								</ul>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<!-- show procedures that don't belong to a department -->
					<h4><spring:message code="viewprocedures.proceduresnodepartment"/></h4>
					<ul>
						<c:forEach var="procedure" items="${nodeptprocs}">
							<div class="padding5 padding20">
								<links:procedureLinkDWRInfo displayName="false" capability="${procedure}" rowcount="0"></links:procedureLinkDWRInfo>
								<c:set var="inactiveProcStyle" value=""/>
								<c:if test="${!procedure.active}">
									<c:set var="inactiveProcStyle" value="color:grey"/>
								</c:if>
								<span style="${inactiveProcStyle}">
									- ${procedure.name}
									<c:if test="${!procedure.active}">
										[${procedure.deactivatedBy.name} - <fmt:formatDate type="date" dateStyle="SHORT" value="${procedure.deactivatedOn}"/>]
									</c:if>
								</span>
							</div>
						</c:forEach>
					</ul>
				</c:otherwise>
			</c:choose>
		<!-- end of infobox div -->
		</div>
	</jsp:body>
</t:crocodileTemplate>