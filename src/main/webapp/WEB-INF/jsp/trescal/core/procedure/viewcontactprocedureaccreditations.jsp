<%-- File name: /trescal/core/procedure/viewcontactprocedureaccreditations.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address" %>
<%@ taglib prefix="company" tagdir="/WEB-INF/tags/company" %>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="procs" tagdir="/WEB-INF/tags/procedure" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="viewcontactprocedureauthorisations.contactprocedureauthorisations"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/procedure/ViewContactProcedureAccreditations.js'></script>
		<script>
		<c:forEach var="category" items="${department.categories}">
			menuElements.push({ anchor: '${category.id}-link', block: '${category.id}-tab'} );
		</c:forEach>
		</script>
	</jsp:attribute>
	<jsp:body>
		<div style="padding: 5px;">
			<spring:message code="viewcontactprocedureauthorisations.currentlyviewingtheprocedureauthorisationsof"/>:
			<strong>${contact.value}</strong>.
			<spring:message code="viewcontactprocedureauthorisations.switchto"/>
			<select id="newcontact" onchange="if($j('#newcontact').val() != 0){window.location='viewcontactprocedureaccreditations.htm?deptid=${deptid}&personid='+$j('#newcontact').val();}">
			<option value="0"><spring:message code="viewcontactprocedureauthorisations.selectanotheruser"/></option>
				<c:forEach var="con" items="${contacts}">
					<option value="${con.key}">${con.value}</option>
				</c:forEach>
			</select>
		</div>
		<br /><br />
		<!-- sub navigation menu which displays and hides sections of page as well as linking to external pages -->
		<c:set var="numOfDepts" value="${departments.size()}"/>
		<div id="subnav">
			<dl>
				<c:set var="max" value="6"/>
				<c:if test="${numOfDepts < 6}"><c:set var="max" value="${numOfDepts}"/></c:if>
				<c:forEach var="deptOption" items="${departments.subList(0, max)}">
					<dt><a href="viewcontactprocedureaccreditations.htm?personid=${contact.key}&deptid=${deptOption.deptid}" <c:if test="${department.deptid == deptOption.deptid}">class="selected"</c:if>>${deptOption.name}</a></dt>
				</c:forEach>
			</dl>
		</div>
		<c:if test="${numOfDepts > 6}">
			<div id="subnav2">
				<dl>
					<c:set var="max" value="12"/>
					<c:if test="${numOfDepts < 12}"><c:set var="max" value="${numOfDepts}"/></c:if>
					<c:forEach var="deptOption" items="${departments.subList(6, max)}">
						<dt><a href="viewcontactprocedureaccreditations.htm?personid=${contact.key}&deptid=${deptOption.deptid}" <c:if test="${department.deptid == deptOption.deptid}">class="selected"</c:if>>${deptOption.name}</a></dt>
					</c:forEach>
				</dl>
			</div>
		</c:if>
		<c:if test="${numOfDepts > 12}">
			<div id="subnav3">
				<dl>
					<c:forEach var="deptOption" items="${departments.subList(12, numOfDepts)}">
						<dt><a href="viewcontactprocedureaccreditations.htm?personid=${contact.key}&deptid=${deptOption.deptid}" <c:if test="${department.deptid == deptOption.deptid}">class="selected"</c:if>>${deptOption.name}</a></dt>
					</c:forEach>
				</dl>
			</div>
		</c:if>
		<!-- end of sub navigation menu -->
		<!-- infobox div with nifty corners applied -->
		<div class="infobox">
			<div class="center"><h4>${department.name}</h4></div>
				<!-- div to hold all the tabbed submenu -->
				<div id="tabmenu">
					<!-- tabbed submenu to change between active/inactive contacts/addresses -->
					<ul class="subnavtab">
						<c:set var="count2" value="1"/>
						<c:forEach var="category" items="${department.categories}">
							<li><a href="#" id="${category.id}-link" onclick="switchMenuFocus(menuElements, '${category.id}-tab', false); return false; " <c:if test="${count2 == 1}">selected</c:if>>${category.name}</a></li>
							<c:set var="count2" value="${count2 + 1}"/>
						</c:forEach>
					</ul>	
					<!-- div to hold all content which is to be changed using the tabbed submenu -->
					<div class="tab-box">
						<c:set var="count3" value="0"/>
						<c:forEach var="category" items="${department.categories}">
							<!-- div which contains accreditations for each category name -->
							<div id="${category.id}-tab" <c:if test="${count3 > 0}">class="hid"</c:if>>
							<c:set var="hid" value="0"/>
								<c:choose>
									<c:when test="${category.procedures.size() == 0}">
										<div class="padding5 bold">
											<spring:message code="viewcontactprocedureauthorisations.noproceduresforthiscategory"/>
										</div>
									</c:when>
									<c:otherwise>
										<ul>
											<c:set var="count4" value="0"/>
											<c:forEach var="proccat" items="${category.procedures}">
												<c:if test="${proccat.capability.heading != null && proccat.capability.heading.id != hid}">
													<li><strong>${proccat.capability.heading.heading}</strong></li>
													<c:set var="hid" value="${proccat.capability.heading.id}"/>
												</c:if>
												<div class="padding5 padding20">
													<c:set var="identifier" value="${count3 + count4}"/>
													<links:procedureLinkDWRInfo displayName="false" capability="${proccat.capability}" rowcount="${identifier}"/>
													<c:set var="inactiveProcStyle" value=""/>
													<c:if test="${!proccat.capability.active}">
														<c:set var="inactiveProcStyle" value="inactive"/>
													</c:if>
													<span class="${inactiveProcStyle}">
														<c:out value=" - ${proccat.capability.name} - " />
														<cwms:securedLink permission="ACCREDITATION_CONTACT_PROCEDURE_EDIT"  collapse="True" classAttr="mainlink" parameter="?personid=${contact.key}&procid=${proccat.capability.id}&deptid=${deptid}">
															<spring:message code="viewcontactprocedureauthorisations.setauthorisation"/>
														</cwms:securedLink>
														<c:if test="${!proccat.capability.active}">
															[${proccat.capability.deactivatedBy.name} - <fmt:formatDate type="date" dateStyle="SHORT" value="${proccat.capability.deactivatedOn}"/>]
														</c:if>
													</span>
													<div id="accred" style="padding-left: 20px; padding-top: 3px;">
														<c:forEach var="serviceType"
																   items="${proccat.capability.serviceTypes}">
															<div>
																<t:showTranslationOrDefault
																		translations="${serviceType.shortnameTranslation}"
																		defaultLocale="${defaultlocale}"/>
																<c:out value=" - "/>
																<procs:showAccreditations
																		caltype="${serviceType.calibrationType}"
																		personid="${contact.key}"
																		capability="${proccat.capability}"/>
															</div>
														</c:forEach>
													</div>
												</div>
												<c:set var="count4" value="${count4 + 1}"/>
											</c:forEach>
										</ul>
									</c:otherwise>
								</c:choose>
							<c:set var="count3"  value="${count3 + 1}"/>
						</div>
						<!-- end of category name div -->
					</c:forEach>
				</div>
				<!-- end of tab-box div -->	
			</div>
			<!-- end of tabmenu div -->
			<!-- clear floats and restore page flow -->
			<div class="clear"></div>
		</div>
		<!-- end of infobox div -->
	</jsp:body>
</t:crocodileTemplate>