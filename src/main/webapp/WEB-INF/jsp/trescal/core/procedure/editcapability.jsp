<%-- File name: /trescal/core/procedure/editproc.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<script src='script/trescal/core/procedure/EditProc.js'></script>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<c:choose>
				<c:when test="${form.capabilityId == 0}">
					<spring:message code="editproc.newcapability" />
				</c:when>
				<c:otherwise>
					<spring:message code="editproc.editcapability" />
				</c:otherwise>
			</c:choose>
		</span>
	</jsp:attribute>
	<jsp:body>
		<t:showErrors path="form.*" showFieldErrors="true" />
		<!-- this div contains all form elements and has nifty corners applied -->
		<div class="infobox" id="editprocedureform">
			<form:form modelAttribute="form" method="post">
				<fieldset>
					<legend>
						<c:choose>
							<c:when test="${form.capabilityId == 0}">
								<spring:message code="editproc.createnewcapability"/>
							</c:when>
							<c:otherwise>
								<spring:message code="editproc.editcapability"/>&nbsp;
								<a href="<c:url value="/viewcapability.htm?capabilityId=${form.capabilityId}" />" >${form.reference}</a>
							</c:otherwise>
						</c:choose>
					</legend>
					<c:choose>
						<c:when test="${form.supplierCapability}">
							<c:set var="supplierVisibility" value="vis" />
							<c:set var="businessVisibility" value="hid" />
						</c:when>
						<c:otherwise>
							<c:set var="supplierVisibility" value="hid" />
							<c:set var="businessVisibility" value="vis" />
						</c:otherwise>
					</c:choose>
					<ol>
						<li>
							<label><spring:message code="company.role"/>:</label>
							<span>
								<form:radiobutton path="supplierCapability" value="false" onclick=" showBusinessCapability(); " />
								<spring:message code="companyrole.business"/>
								<form:radiobutton path="supplierCapability" value="true" onclick=" showSupplierCapability(); " />
								<spring:message code="companyrole.supplier"/>
							</span>
						</li>
						<li id="liBusinessCompany" class="${businessVisibility} businessVisibility">
							<label><spring:message code="company"/>:</label>
							<span>
								<c:out value= "${businessSubdiv.comp.coname}"/>
							</span>
						</li>
						<li id="liBusinessSubdiv" class="${businessVisibility} businessVisibility">
							<label><spring:message code="subdivision"/>:</label>
							<span>
								<c:out value= "${businessSubdiv.subname}"/>
							</span>
						</li>
						<li id="liBusinessAddress" class="${businessVisibility} businessVisibility">
							<label><spring:message code="company.defaultaddress"/>:</label>
							<form:select path="defAddressId" items="${addresses}" itemValue="key" itemLabel="value"/>
							<span class="error">
								<form:errors path="defAddressId"/>
							</span>
						</li>
						<li id="liSupplierCascade" class="${supplierVisibility} supplierVisibility">
							<label><spring:message code="companyrole.supplier"/>:</label>
							<div id="cascadeSearchPlugin">
								<input type="hidden" id="compCoroles" value="supplier" />
								<input type="hidden" id="cascadeRules" value="subdiv,address" />
								<input type="hidden" id="prefillIds" value="${form.coid},${form.subdivid},${form.addrid}" />
							</div>
							<span class="error">
								<form:errors path="coid"/>
								<form:errors path="subdivid"/>
								<form:errors path="addrid"/>
							</span>
						</li>
						<li>
							<label><spring:message code="editproc.active"/>:</label>
									<spring:message code="editproc.active"/>
							<form:radiobutton path="active" value="true" />
									<spring:message code="editproc.inactive"/>
							<form:radiobutton path="active" value="false" />
							<form:errors path="active" class="attention" />
						</li>
						<li>
							<label><spring:message code="editproc.refno"/>:</label>
							<form:input size="20" path="reference"/>
							<span class="error">
								<form:errors path="reference"/>
							</span>
						</li>
						<li>
							<label><spring:message code="editproc.capabilityname"/>:</label>
							<form:input size="80" path="name"/>
							<span class="error">
								<form:errors path="name"/>
							</span>
						</li>
						<li>
                            <label><spring:message code="sub-family" />:</label>
                            <!-- float div left -->
                            <div class="float-left">
                                <form:input size="40" path="subfamilyName" name="subfamilyName" tabindex="3"/>
								<form:hidden path="subfamilyId" name="subfamilyId" />
                            </div>

                            <!-- clear floats and restore pageflow -->
                            <div class="clear-0"></div>
                        </li>
						<li>
							<label><spring:message code='description'/>:</label>
							<form:textarea cols="120" rows="10" path="description"/>
							<span class="error">
								<form:errors path="description"/>
							</span>
						</li>
						<li class="${businessVisibility} businessVisibility">
							<label><spring:message code="editproc.businesscapabilities"/>:</label>
						</li>
						<li class="${businessVisibility} businessVisibility">
							<form:label path="reqType"><spring:message code="workrequirement.type"/>:</form:label>
							<form:select path="reqType" items="${reqtypes}" />
							<span class="error">
								<form:errors path="reqType"/>
							</span>
						</li>
						<li class="${businessVisibility} businessVisibility">
							<label><spring:message code="editproc.departmentcategory"/>:</label>
							<div class="float-left">
								<c:forEach var="categoryDto" items="${categories}">
									<form:checkbox path="categoryids" value="${categoryDto.key}" />
									<c:out value="${categoryDto.value}"/>
									<br/>
								</c:forEach>
								<span class="error">
									<form:errors path="categoryids"/>
								</span>
							</div>
							<div class="clear"></div>
							<%--	<spring:message code='editproc.nodepartment' var="noDepartmentLabel"/>	 --%>
						</li>
						<li class="${businessVisibility} businessVisibility">
							<form:label path="accreditedLabId"><spring:message code="editproc.accreditedlab"/>:</form:label>
							<form:select path="accreditedLabId" items="${accreditedlabs}" itemValue="key" itemLabel="value"/>
							<span class="error">
								<form:errors path="accreditedLabId"/>
							</span>
						</li>
						<li class="${businessVisibility} businessVisibility">
							<label><spring:message code="editproc.defaultcalprocess"/>:</label>
							<form:select path="calProcessId">
								<spring:message code='editproc.nodefaultcalibrationprocess' var="noDefaultCalProcessLabel"/>
								<form:option value="" label="${noDefaultCalProcessLabel}"/>
								<form:options items="${calprocess}" itemValue="id" itemLabel="process"/>
							</form:select>
							<span class="error">
								<form:errors path="calProcessId"/>
							</span>
						</li>
						<li class="${businessVisibility} businessVisibility">
							<label><spring:message code="editproc.defaultworkinstruction"/>: </label>
							<form:select path="workInstructionId">
								<spring:message code="editproc.nodefaultworkinstruction" var="noDefaultWorkInstrLabel"/>
								<form:option value="" label="${noDefaultWorkInstrLabel}"/>
								<form:options items="${workinstructions}" itemValue="id" itemLabel="extendedName"/>
							</form:select>
							<span class="error">
								<form:errors path="workInstructionId"/>
							</span>
						</li>
						<li class="${businessVisibility} businessVisibility">
							<label><spring:message code="editproc.estimatedtime"/>: </label>
							<form:input path="estimatedTime" type="number" min="0" max="9999" cssStyle="width: 70px"/>
							<span class="error">
								<form:errors path="estimatedTime"/>
							</span>
						</li>
						<li>
							<label>&nbsp;</label>
							<input type="submit" name="submit" id="submit" value="<spring:message code='save'/>" />
						</li>
					</ol>
				</fieldset>
			</form:form>
		</div>
		<!-- end of infobox div containing all form elements -->
	</jsp:body>
</t:crocodileTemplate>