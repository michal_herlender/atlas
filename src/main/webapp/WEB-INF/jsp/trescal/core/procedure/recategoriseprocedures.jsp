<%-- File name: /trescal/core/procedure/editprocedurecategories.jsp --%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<spring:message code="capability.recategoriseprocedures" />
		</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src="script/trescal/core/procedure/RecategoriseProcedures.js"></script>
	</jsp:attribute>
	<jsp:body>
		<t:showErrors path="command.*" showFieldErrors="true" />
		<!-- this div contains all form elements and has nifty corners applied -->
		<div class="infobox" id="recategoriseprocedureform">
			<form:form modelAttribute="command" method="post">
				<fieldset id="recategoriseprocedure">
					<legend><spring:message code="capability.recategoriseprocedures" /></legend>
					<ol>
						<li>
							<label class="usage"><spring:message code="subdivision"/>:</label>
							<a class="mainlink" href="viewsub.htm?loadtab=categories-tab&subdivid=${category.department.subdiv.subdivid}">
								${category.department.subdiv.subname}
							</a>
						</li>
						<li>
							<label class="usage"><spring:message code="company.department"/>:</label>
							<c:out value="${category.department.name} ("/>
							<a class="mainlink" href="viewprocedures.htm?loadtab=dept${category.department.id}-tab" >
								<spring:message code="viewprocedures.viewprocedures" />
							</a>
							<c:out value=")" />
						</li>
						<li>
							<label class="usage"><spring:message code="capability.currentcategory"/>:</label>
							${category.department.name} - ${category.name}
						</li>
						<li>
							<label class="usage"><spring:message code="capability.newcategory"/>:</label>
							<form:select path="procedureCategoryId" items="${categoryDtos}" itemValue="value" itemLabel="key" />
							<form:errors path="procedureCategoryId" cssClass="attention" />
						</li>
					</ol>
				</fieldset>
				<table id="recategoriseprocedurestable" class="default2 tablesorter">
					<thead>
						<tr>
							<td colspan="7">
								<spring:message code="capability.selectprocedurestoupdate"/>
							</td>
						</tr>
						<tr>
							<th class="select" scope="col"><input type="checkbox" onClick=" $j('input.procedureid').toggleCheck(); " /></th>
							<th class="reference" scope="col"><spring:message code="searchprocedureresults.reference"/></th>
							<th class="name" scope="col"><spring:message code="name"/></th>
							<th class="active" scope="col"><spring:message code="active"/></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="procedure" items="${procedures}">
							<tr>
								<td><form:checkbox class="procedureid" path="procedureIds[${procedure.id}]" /></td>
								<td>${procedure.name}</td>
								<td>${procedure.reference}</td>
								<td><spring:message code="${procedure.active}" /></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<input type="submit" id="submit" value="<spring:message code='update'/>" /> 
			</form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>
	
