<%-- File name: /trescal/core/procedure/searchworkinstructionresults.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<spring:message code="searchworkinstructionsresults.workinstructionresults" />
		</span>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox">								
			<form:form modelAttribute="command" method="POST">
			<form:hidden path="pageNo"/>
			<form:hidden path="resultsPerPage"/>
			<form:hidden path="coid"/>
			<form:hidden path="name"/>
			<form:hidden path="title"/>
			<t:paginationControl resultset="${pagedResults}"/>
			<table id="wisearchresults" class="tablesorter" />
				<thead>
					<tr>
						<td colspan="4">
							<spring:message code="searchworkinstructionsresults.workinstructionresults"/>
						</td>
					</tr>
					<tr>
						<th class="title" scope="col"><spring:message code="title"/></th>
						<th class="name" scope="col"><spring:message code="name"/></th>	
						<th class="lab" scope="col"><spring:message code="description.description"/></th>							
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="4">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody id="keynavResults">
					<c:choose>
						<c:when test="${pagedResults.results.size() < 1}">
							<tr>
								<td colspan="4" class="bold center">
									<spring:message code="searchprocedureresults.therearenoresultstodisplay"/>
								</td>
							</tr>
						</c:when>
						<c:otherwise>
							<c:forEach var="workInstruction" items="${pagedResults.results}" varStatus="loopStatus" >
								<tr onclick="window.location.href='<c:url value="/viewworkinstruction?wid=${workInstruction.id}"/>'" onmouseover=" $j(this).addClass('hoverrow'); " onmouseout=" if (this.className = 'hoverrow') $j(this).removeClass('hoverrow'); " >
									<td>${workInstruction.title}</td>
									<td>${workInstruction.name}</td>
									<td>${workInstruction.description.description}</td>
								</tr>
							</c:forEach>
						</c:otherwise>												
					</c:choose>
				</tbody>
			</table>
			<t:paginationControl resultset="${command.rs}"/>
		</form:form>
	</div>
</jsp:body>
</t:crocodileTemplate>