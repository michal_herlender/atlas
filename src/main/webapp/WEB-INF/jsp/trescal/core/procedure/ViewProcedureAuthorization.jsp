<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<t:overlayTemplate bodywidth="780"> 


	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/procedure/ViewProcedureAuthorization.js'></script>
		<script src='script/thirdparty/jQuery/jquery.js' ></script>
		<script type='text/javascript' src='script/thirdparty/jQuery/jquery.ui.js'></script>
		<script src='script/thirdparty/jQuery/jquery-migrate-1.2.1.js'></script>
		
		<script>
// 			$(function(){
// 				var today = new Date();
// 				$('.date').datepicker({
// 					dateFormat : 'yy-mm-dd',
// 					beforeShowDay: $.datepicker.noWeekends,
// 					maxDate: new Date(today.getFullYear(), today.getMonth(), today.getDate())
// 				});
// 			})
			
			
		</script>
	</jsp:attribute>

	<jsp:body>
		<div id="changeUserAccreditationOverlay">
			<t:showErrors path="form.*" showFieldErrors="true"/>
		
			<div class="attention text-center marg-top marg-bot">
					<spring:message code="viewprocedure.logSupportingForAllAccred"/>				
			</div>
			
			<div id="tabmenu">
				<ul class="subnavtab">
					<li>
						<a href="#" id="accred-link" onclick="switchMenuFocus(menuElements3, 'accred-tab', false); return false; " class="selected"><spring:message code="viewprocedure.accreditedUsers"/></a>
					</li>
					<li>
						<a href="#" id="nonaccred-link" onclick="switchMenuFocus(menuElements3, 'nonaccred-tab', false); return false; "><spring:message code="viewprocedure.nonAccreditedUsers"/></a>
					</li>
				</ul>				
			
			<div class="tab-box">
				<div id="accred-tab">
					<fieldset>
						<ol>
							<c:forEach items="${authorizedContacts}" var="entry">
							<form:form modelAttribute="form" method="POST" id="form_${entry.key.personid}">
								<li>
									<div class="float-left width60"> ${entry.key.getName()} </div>
									<div class="float-left">
										<a href="#" onclick=" toggleAccreditations(this); return false; ">
											<img src="img/icons/showinfo.png" width="16" height="16" class="img_marg_bot" alt="Show Accreditations" title="Show Accreditations" />
										</a>
									</div>
									<div class="clear-0"></div>
									<div class="accreditations padding10" id="accreditations_${entry.key.personid}" style=" display: none; ">
										<c:forEach items="${caltypes}" var="caltype" varStatus="loop">
											<div class="padding5">
												<input type="hidden" id="personId_${entry.key.personid}" name="personId" value="${entry.key.personid}" />
												<input type="hidden" id="procsId_${procid}" name="procsId"  value="${procid}" />
												<input type="hidden" id="caltypsIds_${caltype.calTypeId}_${entry.key.personid}" name="items[${loop.index}].caltypsId"  value="${caltype.calTypeId}" />
													<div class="accredCaltype">
														<cwms:besttranslation translations="${caltype.serviceType.shortnameTranslation}" /> : 
													</div>
													<div class="accredLevel">
														<select class="accredLevelSelect" name="items[${loop.index}].level" id="levels[${loop.index}]_${entry.key.personid}">
															<c:set var="levelfound" value="false"/>
															<c:forEach items="${entry.value}" var="value">
																<c:if test="${caltype.calTypeId eq value.caltype.calTypeId}">
																	<option value="${value.accredLevel }" selected="selected">${value.accredLevel.getMessage()}</option>
																	<c:set var="tempLevelVar" value="${value.accredLevel}" />
																	<c:set var="levelfound" value="true"/>
																</c:if>
															</c:forEach>
															<c:forEach items="${accedLevels}" var="Acclevel">
																<c:choose>
																	<c:when test="${Acclevel eq tempLevelVar }">
																		<option value="${Acclevel}" style="display: none;" > ${Acclevel.getMessage()}</option>
																		<c:set var="tempLevelVar" value="" />
																	</c:when>
																	<c:otherwise>
																		<option value="${Acclevel}" ${ levelfound==false && Acclevel == 'NONE' ? 'selected="selected"' : ''}> ${Acclevel.getMessage()}</option>
																	</c:otherwise>
																</c:choose>
 															 </c:forEach>
														</select>
														<a href="#" onclick="CopySelectedOptions('accredLevelSelect',$j(this).siblings('select'));">
															<img src="img/icons/arrow_down.png" width="10" height="16" alt="Copy selection to all" title="Copy selection to all" />
														</a>
													</div>
													<div class="accredDate">
														<c:set var="found" value="false"/>
														<c:forEach items="${entry.value}" var="value">
															<c:if test="${caltype.calTypeId eq value.caltype.calTypeId}">
																<form:input type="date" path="items[${loop.index}].date" id="dates[${loop.index}]_${entry.key.personid}" class="date" readonly="readonly" autocomplete="off" value="${ value.awardedOn }" />
																<c:set var="found" value="true"/>
															</c:if>
														</c:forEach>
															<c:if test="${found eq false }">
																<form:input type="date" path="items[${loop.index}].date" id="dates[${loop.index}]_${entry.key.personid}" class="date" readonly="readonly" autocomplete="off"/>
															</c:if>
															
														 	<a href="#" onclick="CopySelectedOptions('date',$j(this).siblings('input')); return false;">
														 		<img src="img/icons/arrow_down.png" width="10" height="16" alt="Copy selection to all" title="Copy selection to all" />
														 	</a>
													</div>	
												<div class="clear"></div>
											</div>	
										</c:forEach>
										<div class="center">
											<input type="submit" id="button_${entry.key.personid}" value="Save" />
										</div>
									</div>
								</li>
								</form:form>
							</c:forEach>
							
						</ol>
						
					</fieldset>
				</div>
					
				<div id="nonaccred-tab" class="hid">
					<fieldset>
						<ol>
							<c:forEach items="${ nonauthorizedContacts }" var="nonAccredContact">
									<li>
										<form:form modelAttribute="form" method="POST" action="" id="form_${nonAccredContact.personid}">

											<input type="hidden" id="personId_${nonAccredContact.personid}" name="personId" value="${nonAccredContact.personid}" />
											<input type="hidden" id="procsId_${procid}" name="procsId"  value="${procid}" />

											<div class="float-left width60"> ${nonAccredContact.getName()} </div>
											<div class="float-left">
												<a href="#" onclick=" toggleAccreditations(this); return false; ">
													<img src="img/icons/showinfo.png" width="16" height="16" class="img_marg_bot" alt="Show Accreditations" title="Show Accreditations" />
												</a>
											</div>
											<div class="clear-0"></div>
											<div class="accreditations padding10" id="accreditations_${nonAccredContact.personid}" style=" display: none; ">
												<c:forEach items="${caltypes}" var="caltype" varStatus="loop">
													<input type="hidden" id="caltypsIds_${caltype.calTypeId}_${nonAccredContact.personid}" name="items[${loop.index}].caltypsId"  value="${caltype.calTypeId}" />
													<div class="padding5">
														<div class="accredCaltype">
															<cwms:besttranslation translations="${caltype.serviceType.shortnameTranslation}" /> :
														 </div>
														 <div class="accredLevel">
														 	<select class="accredLevelSelect" name="items[${loop.index}].level" id="levels[${loop.index}]_${nonAccredContact.personid}">
														 		<c:forEach items="${accedLevels}" var="Acclevel">
														 			<option value="${Acclevel}" ${Acclevel == 'NONE' ? 'selected="selected"' : ''}  > ${Acclevel.getMessage()}</option>
														 		</c:forEach>
														 	</select>
														 	<a href="#" onclick="CopySelectedOptions('accredLevelSelect',$j(this).siblings('select'));">
														 		<img src="img/icons/arrow_down.png" width="10" height="16" alt="Copy selection to all" title="Copy selection to all" />
														 	</a>
														 </div>
														 <div class="accredDate">
														 	<form:input  type="date" path="items[${loop.index}].date" id="dates[${loop.index}]_${nonAccredContact.personid}" class="date" readonly="readonly" autocomplete="off"/>
														 	<a href="#" onclick=" CopySelectedOptions('date',$j(this).siblings('input')); return false; ">
														 		<img src="img/icons/arrow_down.png" width="10" height="16" alt="Copy selection to all" title="Copy selection to all" />
														 	</a>
														 </div>
														 <div class="clear"></div>
													</div>
												</c:forEach>
												<div class="center">
													<input type="submit" id="button_${nonAccredContact.personid}" value="Save" />
												</div>
											</div>
										</form:form>
									</li>
							</c:forEach>
						</ol>
					</fieldset>
				</div>
			</div>
				
		</div>
			<div class="clear"></div>
		</div>
	</jsp:body>
</t:overlayTemplate>