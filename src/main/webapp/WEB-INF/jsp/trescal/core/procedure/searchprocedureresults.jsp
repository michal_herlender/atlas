<%-- File name: /trescal/core/procedure/searchprocedureresults.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address" %>
<%@ taglib prefix="company" tagdir="/WEB-INF/tags/company" %>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>

<script src='script/trescal/core/procedure/SearchProcedureResults.js'></script>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="searchprocedureresults.procedureresults"/></span>
	</jsp:attribute>
	<jsp:body>
		<!-- infobox contains all search results and is styled with nifty corners -->
		<div class="infobox">
			<form:form action="" modelAttribute="command" name="searchform" id="searchform" method="post">
				<form:hidden path="name"/>
				<form:hidden path="reference"/>
				<form:hidden path="description"/>
				<form:hidden path="active"/>
				<form:hidden path="subfamilyId"/>
				<form:hidden path="subfamilyName"/>
				<form:hidden path="subdivid"/>
				<form:hidden path="coid"/>
				<form:hidden path="pageNo"/>
				<form:hidden path="resultsPerPage"/>
				<t:showResultsPagination rs="${command.rs}" pageNoId=""/>
				<table id="proceduresearchresults" class="tablesorter" summary="This table lists all procedure search results">
					<thead>
						<tr>
							<td colspan="6"><spring:message code="searchprocedureresults.procedureresults"/> (${command.rs.results.size()})</td>
						</tr>
						<tr>
							<th id="company" scope="col"><spring:message code="company"/></th>  
							<th id="subdiv" scope="col"><spring:message code="subdivision"/></th>  
							<th id="subfamily" scope="col"><spring:message code="sub-family"/></th>  
							<th id="reference" scope="col"><spring:message code="searchprocedureresults.reference"/></th>  
							<th id="name" scope="col"><spring:message code='name'/></th>
							<th id="active" scope="col"><spring:message code="searchprocedureresults.active"/></th>											
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="6">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody id="keynavResults">
						<c:set var="rowcount" value="0"/>
						<c:choose>
							<c:when test="${command.rs.results.size() < 1}">
								<tr>
									<td colspan="6" class="bold center">
										<spring:message code="searchprocedureresults.therearenoresultstodisplay"/>
									</td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:forEach var="capability" items="${command.rs.results}">
									<tr onclick="window.location.href='viewcapability.htm?capabilityId=${capability.id}'" onmouseover="$j(this).addClass('hoverrow');" onmouseout="if (this.className != 'hoverrow') $j(this).removeClass('hoverrow');">
										<td>${capability.organisation.comp.coname}</td>
										<td>${capability.organisation.subname}</td>
										<td>
											<c:if test="${not empty capability.subfamily}">
												<cwms:besttranslation translations="${capability.subfamily.translations}"/>
											</c:if>
										</td>
										<td class="center">
											<links:procedureLinkDWRInfo displayName="false" capability="${capability}" rowcount="${rowcount}"></links:procedureLinkDWRInfo>
										</td>
										<td>${capability.name}</td>
										<td class="center">
											<c:choose>
												<c:when test="${capability.active}">
													<img src="img/icons/greentick.png" width="16" height="16" alt="<spring:message code='searchprocedureresults.procedureactive'/>" title="<spring:message code='searchprocedureresults.procedureactive'/>" />
												</c:when>
												<c:otherwise>
													<img src="img/icons/redcross.png" width="16" height="16" alt="<spring:message code='searchprocedureresults.procedureinactive'/>" title="<spring:message code='searchprocedureresults.procedureinactive'/>" />
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
									<c:set var="rowcount" value="${rowcount + 1}"/>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
				<t:showResultsPagination rs="${command.rs}" pageNoId=""/>
			</form:form>
		</div>
		<!-- end of infobox div -->
	</jsp:body>
</t:crocodileTemplate>