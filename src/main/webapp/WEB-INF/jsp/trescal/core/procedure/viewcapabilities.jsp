<%-- File name: /trescal/core/procedure/viewcapabilities.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="procedure" tagdir="/WEB-INF/tags/procedure" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="viewcapabilities.forsubdivision"/></span>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox" id="viewcapabilities">
			<fieldset>
				<legend><spring:message code="viewcapabilities.viewcapabilities"/></legend>
				<div class="displaycolumn">
					<ol>
						<li>
							<label><spring:message code="company"/>:</label>
							<span><links:companyLinkDWRInfo company="${selectedSubdiv.comp}" rowcount="0" copy="true"/></span>
						</li>
						<li>
							<label><spring:message code="subdivision"/>:</label>
							<span><links:subdivLinkDWRInfo subdiv="${selectedSubdiv}" rowcount="0" /></span>
						</li>
						<li>
							<label><spring:message code="company.role"/>:</label>
							<span>${selectedSubdiv.comp.companyRole.getMessage()}</span>
						</li>
					</ol>
				</div>
				<div class="displaycolumn">
					<ol>
						<li>
							<label><spring:message code="domain"/>:</label>
							<c:choose>
								<c:when test="${selectedDomain.active == 'Yes'}"><spring:message var="activeText" code="company.active"/></c:when>
								<c:otherwise><spring:message var="activeText" code="company.inactive"/></c:otherwise>
							</c:choose>
							<span>${selectedDomain.name}<c:out value=" (${activeText})" /></span>
						</li>
						<c:if test="${not empty subfamilyCountWithCapability && not empty subfamilyCountWithoutCapability}">
							<li>
								<label><spring:message code="viewcapabilities.subfamilies"/>:</label>
								<div class="inline">
									<spring:message code="viewcapabilities.withactivecapability" arguments="${subfamilyCountWithCapability}" />
									<br/>
									<spring:message code="viewcapabilities.withoutactivecapability" arguments="${subfamilyCountWithoutCapability}" />
								</div>
							</li>
						</c:if>
					</ol>
				</div>
			</fieldset>
		</div>
		
		<!-- sub navigation menu which links by domain to external pages -->
		<c:forEach var="subnavIndex" begin="0" end="${domainList.size() / 6}">
			<div id="subnav${subnavIndex == 0 ? '' : subnavIndex + 1}">
				<dl>
					<c:set var="min" value="${subnavIndex * 6}"/>
					<c:set var="max" value="${subnavIndex * 6 + 6}"/>
					<c:if test="${max > domainList.size()}"><c:set var="max" value="${domainList.size()}"/></c:if>
					<c:forEach var="domain" items="${domainList.subList(min, max)}">
						<dt>
							<c:set var="selectedClass" value="${domain.domainid == selectedDomain.domainid ? 'selected' : ''}" />
							<c:set var="disabledClass" value="${domain.active == 'Yes' || selectedClass == 'selected' ? '' : 'disabled'}" />
							<a href="viewcapabilities.htm?subdivid=${selectedSubdiv.subdivid}&domainid=${domain.domainid}" id="domain${domain.domainid}-link" class="${selectedClass} ${disabledClass}">
								${domain.name}
							</a>
						</dt>
					</c:forEach>
				</dl>			
			</div>
		</c:forEach>
		
		<div class="infobox" id="capabilitysearchresults">
			<c:choose>
				<c:when test="${selectedDomain.domainid == 0}">
					<c:choose>
						<c:when test="${not empty proceduresWithoutSubfamily}">
							<c:forEach var="capability" items="${proceduresWithoutSubfamily}" varStatus="procStatus">
								<procedure:showCapability showHeader="${procStatus.first}" capability="${capability}" viewcapabilities="true" />
							</c:forEach>
						</c:when>
						<c:otherwise>
							<span><spring:message code="viewcapabilities.noresults" /></span>
						</c:otherwise>
					</c:choose>
				</c:when>
				<c:otherwise>
					<c:choose>
						<c:when test="${not empty familyList}">
							<!-- display the procedures sorted by family and then subfamily -->
							<c:set var="showSubfamilyHeader" value="true" />
							<c:forEach var="familyDto" items="${familyList}" varStatus="familyStatus">
								<c:forEach var="subfamilyDto" items="${subfamilyMap.get(familyDto.familyid).entrySet()}" varStatus="subfamilyStatus">				
									<%-- Always display new header when subfamily has capability entries --%>
									<table class="default4 subfamilytable">
										<c:if test="${capabilityMap.containsKey(subfamilyDto.key) || showSubfamilyHeader}">
											<thead>
												<tr>
													<td class="headerfamily"><spring:message code="family"/></td>
													<td class="headersubfamily"><spring:message code="sub-family"/></td>
													<td class="headeractive"><spring:message code="active"/></td>
													<td class="headerinactive"><spring:message code="editproc.inactive"/></td>
													<td class="headerlinks"></td>
												</tr>
											</thead>
											<c:set var="showSubfamilyHeader" value="false" />
										</c:if>
										<tbody>
											<tr>
												<td class="headerfamily">${familyDto.name}</td>
												<td class="headersubfamily">${subfamilyDto.value}</td>
												<td class="headeractive">${capabilityCountActive.containsKey(subfamilyDto.key) ? capabilityCountActive.get(subfamilyDto.key) :0}</td>
												<td class="headerinactive">${capabilityCountInactive.containsKey(subfamilyDto.key) ? capabilityCountInactive.get(subfamilyDto.key) :0}</td>
												<td class="headerlinks"></td>
											</tr>
											<c:if test="${capabilityMap.containsKey(subfamilyDto.key)}">
												<tr>
													<td colspan="5">
														<c:forEach var="capability" items="${capabilityMap.get(subfamilyDto.key)}" varStatus="procStatus">
															<procedure:showCapability showHeader="${procStatus.first}" capability="${capability}" viewcapabilities="true" />
															<c:set var="showSubfamilyHeader" value="true" />
														</c:forEach>
													</td>
												</tr>
											</c:if>
										</tbody>
									</table>
								</c:forEach>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<span><spring:message code="viewcapabilities.noresults" /></span>
						</c:otherwise>
					</c:choose>
				</c:otherwise>
			</c:choose>
		</div>
	</jsp:body>
</t:crocodileTemplate>