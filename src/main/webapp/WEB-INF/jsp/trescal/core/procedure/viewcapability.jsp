<%-- File name: /trescal/core/procedure/viewcapability.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address" %>
<%@ taglib prefix="company" tagdir="/WEB-INF/tags/company" %>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="procedure" tagdir="/WEB-INF/tags/procedure" %>

<t:crocodileTemplate>
<jsp:attribute name="header">
	<span class="headtext"><spring:message code="viewprocedure.viewprocedure"/></span>
</jsp:attribute>
    <jsp:attribute name="globalElements">
	<cwms-upload-file system-component-id="${sc.componentId}" entity-id1="${procedure.reference}" entity-id2=""
                      files-name="${filesName}" rename-to="" web="false"></cwms-upload-file>
</jsp:attribute>
    <jsp:attribute name="scriptPart">
	<style>
        .addAliaseLink {
            float: right;
            margin-right: 10px;
            text-decoration: underline;
            cursor: pointer;
            color: #0063be;
        }

        .addAliaseLink:hover {
            color: #990033;
        }
    </style>
    <link rel="stylesheet" href="styles/trescal/core/capability/capability.css">

    <script type='text/javascript' src='script/trescal/core/procedure/ViewProcedure.js'></script>
    <script type='text/javascript' src='script/trescal/core/procedure/ViewProcedureAuthorization.js'></script>

    <script type='module' src='script/components/cwms-upload-file/cwms-upload-file.js'></script>
    <script type="module"
            src="script/components/cwms-tooltips/cwms-capability-link-and-tooltip/cwms-capability-link-and-tooltip.js"></script>
    <script type="module"
            src="script/components/capability/cwms-capabilityfilter-active-switch/cwms-capabilityfilter-active-switch.js"></script>

    <!-- load the different calibration levels into a javascript array -->
    <script type='text/javascript'>
        <c:set var="cals" value=""/>
        <c:forEach var="cal" items="${caltypes}" varStatus="loopStatus">
        <c:if test="${!loopStatus.first}">
        <c:set var="cals" value="${cals}, "/>
        </c:if>
        <c:set var="cals" value="${cals} ${cal.calTypeId}"/>
        menuElements2.push({anchor: 'caltype${cal.calTypeId}-link', block: 'caltype${cal.calTypeId}-tab'});
        </c:forEach>
        var cals = [${cals}];
    </script>
    </jsp:attribute>
    <jsp:body>
        <!-- infobox containing procedure details with nifty corners applied -->
        <div class="infobox" id="viewprocedure">
            <fieldset>
                <legend><spring:message code="viewprocedure.viewprocedure"/></legend>
                <div class="displaycolumn">
                    <ol>
                        <li>
                            <label><spring:message code="editproc.refno"/>:</label>
                            <span>${procedure.reference}</span>
                        </li>
                        <li>
                            <label><spring:message code="editproc.capabilityname"/>:</label>
                            <span>${procedure.name}</span>
                        </li>
                        <li>
                            <label><spring:message code="sub-family"/>:</label>
                            <span>
							<c:choose>
                                <c:when test="${not empty procedure.subfamily}">
                                    <cwms:besttranslation translations="${procedure.subfamily.translations}"/>
                                </c:when>
                                <c:otherwise><spring:message code="notapplicable"/></c:otherwise>
                            </c:choose>
						</span>
                        </li>
                        <li>
                            <label><spring:message code="workrequirement.type"/>:</label>
                            <span>${procedure.reqType}</span>
                        </li>
                        <li>
                            <label><spring:message code="viewprocedure.defaultcalprocess"/>:</label>
                            <span>
							<c:choose>
                                <c:when test="${procedure != null}">
                                    ${procedure.calibrationProcess.process}
                                </c:when>
                                <c:otherwise>
                                    <spring:message code="viewprocedure.nodefaultprocedureset"/>.
                                </c:otherwise>
                            </c:choose>
						</span>
                        </li>
                        <li>
                            <label><spring:message code="viewprocedure.defaultworkinstruction"/>:</label>
                            <span>
							<c:choose>
                                <c:when test="${not empty procedure.defWorkInstruction}">
                                    <c:out value="${procedure.defWorkInstruction.title} : ${procedure.defWorkInstruction.name}"/>
                                </c:when>
                                <c:otherwise>
                                    <spring:message code="viewprocedure.noworkinstructionset"/>.
                                </c:otherwise>
                            </c:choose>
						</span>
                        </li>
                        <li>
                            <label><spring:message code="editproc.estimatedtime"/>:</label>
                            <span>
                                    ${procedure.estimatedTime}
                            </span>
                        </li>
                    </ol>
                </div>

                <div class="displaycolumn">
                    <ol>
                        <li>
                            <label><spring:message code="company"/>:</label>
                            <span>${procedure.organisation.comp.coname}</span>
                        </li>
                        <li>
                            <label><spring:message code="subdivision"/>:</label>
                            <span>${procedure.organisation.subname}</span>
                        </li>
                        <li>
                            <label><spring:message code="company.defaultaddress"/>:</label>
                            <div class="float-left">
                                <c:choose>
                                    <c:when test="${not empty procedure.defAddress}">
                                        <address:showAddress address="${procedure.defAddress}" vis="true"
                                                             separator="<br/>" copy="false"/>
                                    </c:when>
                                    <c:otherwise>
                                        <spring:message code="notapplicable"/>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </li>
                        <li>
                            <label><spring:message code="viewprocedure.accreditedlab"/>:</label>
                            <span>
							<c:choose>
                                <c:when test="${not empty procedure.labAccreditation}">
                                    <c:out value="${procedure.labAccreditation.labNo} : ${procedure.labAccreditation.description}"/>
                                </c:when>
                                <c:otherwise>
                                    <spring:message code="notaccredited"/>
                                </c:otherwise>
                            </c:choose>
						</span>
                        </li>
                        <li>
                            <label><spring:message code="viewprocedure.categorydepartment"/>:</label>
                            <div class="float-left">
                                <c:choose>
                                    <c:when test="${procedure.categories.size() > 0}">
                                        <c:forEach var="proccat" items="${procedure.categories}" varStatus="loopStatus">
                                            <c:if test="${!loopStatus.first}"><br/></c:if>
                                            ${proccat.category.name} - ${proccat.category.department.name}
                                        </c:forEach>
                                    </c:when>
                                    <c:otherwise>
                                        <spring:message code="viewprocedure.nodepartmentassigned"/>.
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </li>
                        <li>
                            <label><spring:message code="active"/>:</label>
                            <span>
								<c:choose>
                                    <c:when test="${procedure.active == true}">
                                        <spring:message code="active"/>
                                    </c:when>
                                    <c:otherwise>
                                        <spring:message code="editproc.inactive"/>
                                    </c:otherwise>
                                </c:choose>
							</span>
						</li>
					</ol>
				</div>
				<div class="displaycolumn-100">
					<ol>
						<li>
							<div class="float-left">
								<label><spring:message code="description"/>:</label>
								${procedure.description.replaceAll("\\n","<br/>")}
							</div>
							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
                        </li>
                    </ol>
                </div>
            </fieldset>
        </div>
        <!-- end of infobox containing procedure details -->
        <!-- sub navigation menu which displays and hides sections of page as well as linking to external pages -->
        <div id="subnav">
            <dl>
                <dt>
                    <a role="button"
                       onclick="event.preventDefault(); switchMenuFocus(menuElements, 'viewfilters-tab', false);"
                       id="viewfilters-link" title="<spring:message code="viewprocedure.filters"/>" class="selected">
                        <spring:message code="viewprocedure.filters"/>
                    </a>
                </dt>
                <dt>
                    <a role="button"
                       onclick="event.preventDefault(); switchMenuFocus(menuElements, 'viewaccredusers-tab', false);"
                       id="viewaccredusers-link" title="<spring:message code="viewprocedure.viewusersaccredited"/>">
                        <spring:message code="viewprocedure.accreditedusers"/>
                    </a>
                </dt>
                    <%-- TODO - control procedure edit privileges - logged in subdiv, etc... --%>
                <dt>
                    <a href="editcapability.htm?procid=${procedure.id}"
                       title="<spring:message code="viewprocedure.editdetails"/>">
                        <spring:message code="edit"/>
                    </a>
                </dt>
                <dt>
                    <a role="button"
                       onclick="event.preventDefault(); switchMenuFocus(menuElements, 'standards-tab', false);"
                       id="standards-link" title="<spring:message code="viewprocedure.viewstandards"/>">
                        <spring:message code="viewprocedure.standards"/> (<span
                            class="stdsSizeSpan">${procedure.standards.size()}</span>)
                    </a>
                </dt>
                <dt>
                    <a role="button"
                       onclick="event.preventDefault(); switchMenuFocus(menuElements, 'resources-tab', false);"
                       id="resources-link" title="<spring:message code="viewprocedure.viewfiles"/>">
                        <spring:message code="viewprocedure.files"/> (${scRootFiles.numberOfFiles})
                    </a>
                </dt>
                <dt>
                    <a role="button"
                       onclick="event.preventDefault(); switchMenuFocus(menuElements, 'proctraining-tab', false);"
                       id="proctraining-link" title="<spring:message code="viewprocedure.viewtrainingrecords"/>">
                        <spring:message code="viewprocedure.trainingrecords"/>
                        (${procedure.capabilityTrainingRecords.size()})
                    </a>
                </dt>
            </dl>
        </div>
        <!-- end of sub navigation menu -->
        <!-- infobox div with nifty corners applied -->
        <div class="infobox" id="viewprocedure">
            <!-- this section displays all accredited operators of this procedure -->
            <div id="viewfilters-tab" class="vis">
				<procedure:showCapability showHeader="true" capability="${procedure}" viewcapabilities="false" />
			</div>
			<!-- this section displays all accredited operators of this procedure -->
			<div id="viewaccredusers-tab" class="hid">
				<h5><spring:message code="viewprocedure.calibrationtypes"/></h5>
				<!-- div to hold all the tabbed submenu -->
				<div id="tabmenu">
					<div class="float-right">
						<cwms:securedJsLink permission="CALIBRATION_ACCREDITATION_EDIT" collapse="True" classAttr="mainlink" onClick="event.preventDefault(); procedureAuthorizationOverlay(${procedure.id})" >
							<spring:message code="viewprocedure.editauthorization"/>
						</cwms:securedJsLink>
					</div>
					<!-- tabbed submenu to change between active/inactive contacts/addresses -->
					<ul class="subnavtab">
                        <c:forEach var="caltype" items="${caltypes}" varStatus="loopStatus">
                            <li><a role="button" id="caltype${caltype.calTypeId}-link"
                                   onclick="event.preventDefault(); switchMenuFocus(menuElements2, 'caltype${caltype.calTypeId}-tab', false);"
                                   <c:if test="${loopStatus.first}">class="selected"</c:if>>
                                <t:showTranslationOrDefault translations="${caltype.serviceType.shortnameTranslation}"
                                                            defaultLocale="${defaultlocale}"/>
                            </a></li>
                        </c:forEach>
					</ul>
					<!-- div to hold all content which is to be changed using the tabbed submenu -->
					<div class="tab-box">
						<c:forEach var="caltype" items="${caltypes}" varStatus="loopStatus">
							<div id="caltype${caltype.calTypeId}-tab" <c:if test="${!loopStatus.first}">class="hid"</c:if>>
								<c:choose>
									<c:when test="${caltype.accreditationLevels == null || caltype.accreditationLevels.size() == 0}">
										<table class="default4" summary="">
											<thead>
												<tr>
													<td colspan="3"><strong><spring:message code="viewprocedure.none"/></strong></td>
												</tr>
												<tr>
													<th class="accredcont" scope="col"><spring:message code="viewprocedure.accreditedcontact"/></th>
													<th class="awardedon" scope="col"><spring:message code="viewprocedure.awardedon"/></th>
													<th class="awardedby" scope="col"><spring:message code="viewprocedure.awardedby"/></th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td colspan="3" class="bold center"><spring:message code="viewprocedure.noaccreditedusers"/></td>
												</tr>
											</tbody>
										</table>
									</c:when>
									<c:otherwise>
										<c:forEach var="level" items="${caltype.accreditationLevels}">
											${level.level.getMessage()}
											<c:if test="${level.level != 'NONE'}">
												<table class="default4" id="caltype${caltype.calTypeId}${level.level}" summary="">
													<thead>
														<tr>
															<td colspan="3"><strong><spring:message code="viewprocedure.accreditationlevel.${level.level.defaultName}.description"/> </strong></td>
														</tr>
														<tr>
															<th class="accredcont" scope="col"><spring:message code="viewprocedure.accrediteduser"/></th>
															<th class="awardedon" scope="col"><spring:message code="viewprocedure.awardedon"/></th>
															<th class="awardedby" scope="col"><spring:message code="viewprocedure.awardedby"/></th>
														</tr>
													</thead>
													<tbody>
														<c:set var="hasAccreditedUsers" value="false"/>
														<c:forEach var="accred" items="${procedure.authorizations}">
															<c:if test="${accred.caltype.calTypeId == caltype.calTypeId && accred.accredLevel == level.level}">
																<c:set var="hasAccreditedUsers" value="true"/>
																<tr id="${level.level.toString().toLowerCase().trim()}${accred.caltype.calTypeId}${accred.authorizationFor.personid}">
																	<td>${accred.authorizationFor.name}</td>
																	<td><fmt:formatDate value="${accred.awardedOn}" type="date" dateStyle="SHORT" /></td>
																	<td>${accred.awardedBy.name}</td>
																</tr>
															</c:if>
														</c:forEach>
														<c:if test="${!hasAccreditedUsers}">
															<tr>
																<td colspan="3" class="bold center"><spring:message code="viewprocedure.noaccreditedusers"/></td>
															</tr>
														</c:if>
													</tbody>
												</table>
											</c:if>
										</c:forEach>
									</c:otherwise>
								</c:choose>
							</div>
						</c:forEach>
					</div>
				<!-- end of div to hold all tabbed submenu content -->
				</div>
					<!-- end of div to hold tabbed submenu -->
					<!-- this div clears floats and restores page flow -->
					<div class="clear"></div>
				</div>
				<!-- end of accredited operators section -->
				<!-- this section displays all standards that can/should be used for procedure -->
				<div id="standards-tab" class="hid">
					<table class="default2" id="procedurestds" summary="This table displays all standards added to this procedure">
						<thead>
							<tr>
								<td colspan="8">
									<spring:message code="viewprocedure.standards"/>
									(<span class="stdsSizeSpan">${procedure.standards.size()}</span>)
								</td>
							</tr>
							<tr>
								<th class="procstdbar" scope="col"><spring:message code="barcode"/></th>
								<th class="procstdinst" scope="col"><spring:message code="instmodelname"/></th>
								<th class="procstdserial" scope="col"><spring:message code="serialno"/></th>
								<th class="procstdplant" scope="col"><spring:message code="plantno"/></th>
								<th class="procstdcaldate" scope="col"><spring:message code="caldate"/></th>
								<th class="procstdenf" scope="col"><spring:message code="viewprocedure.enforce"/></th>
								<th class="procstdaddby" scope="col"><spring:message code="viewprocedure.addedby"/></th>
								<th class="procstddel" scope="col"><spring:message code="delete"/></th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="8">
									<cwms:securedJsLink permission="PROCEDURE_STANDARD_ADD" collapse="True" classAttr="mainlink-float" onClick="event.preventDefault(); addStandardsContent('procedurestds', ${procedure.id}, '${procedure.reference}', '${procedure.name}', 'PROCEDURE');">
										<spring:message code="viewprocedure.addstandards"/>
									</cwms:securedJsLink>
								</td>
							</tr>
						</tfoot>
						<tbody>
							<c:choose>
								<c:when test="${procedure.standards.size() == 0}">
									<tr>
										<td colspan="8" class="bold center">
											<spring:message code="viewprocedure.therearenostandardstodisplay"/>
										</td>
									</tr>
								</c:when>
								<c:otherwise>
									<c:forEach var="procstand" items="${procedure.standards}">
										<c:set var="instrument" value="${procstand.instrument}"/>
										<c:choose>
											<c:when test="${instrument.outOfCalibration == true}">
												<c:set var="rowColor" value="highlight"/>
											</c:when>
											<c:otherwise>
												<c:set var="rowColor" value=""/>
											</c:otherwise>
										</c:choose>
										<tr id="std${instrument.plantid}" class="${rowcolor}">
											<td><links:instrumentLinkDWRInfo displayCalTimescale="true" displayName="false" instrument="${instrument}" caltypeid="0" rowcount="0" displayBarcode="true"/></td>
                                            <td>
                                                <cwms:showmodel instrumentmodel="${instrument.model}"/>
                                                <c:if test="${instrument.outOfCalibration == true}">
                                                    <br/><span class="attention"><spring:message
                                                        code="viewworkinstruction.outofcalibration"/></span>
                                                </c:if>
                                            </td>
                                            <td>${instrument.serialno}</td>
                                            <td>${instrument.plantno}</td>
                                            <td><fmt:formatDate value="${instrument.nextCalDueDate}" type="date"
                                                                dateStyle="SHORT"/></td>
                                            <td class="center">
                                                <a role="button"
                                                   onclick="event.preventDefault(); updateDefStdEnforceUse('procedurestds', ${procstand.id}, ${instrument.plantid});">
                                                    <c:choose>
                                                        <c:when test="${procstand.enforceUse == true}">
                                                            <img src="img/icons/bullet-tick.png" width="10" height="10"
                                                                 alt="<spring:message code='viewprocedure.enforceuse'/>"
                                                                 title="<spring:message code='viewprocedure.enforceuse'/>"/>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <img src="img/icons/bullet-cross.png" width="10" height="10"
                                                                 alt="<spring:message code='viewprocedure.donotenforce'/>"
                                                                 title="<spring:message code='viewprocedure.donotenforce'/>"/>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </a>
                                            </td>
                                            <td>
                                                <fmt:formatDate value="${procstand.setOn}" type="date"
                                                                dateStyle="SHORT"/> <br/>${procstand.setBy.name}
                                            </td>
                                            <td class="center">
                                                <a role="button"
                                                   onclick="event.preventDefault(); deleteDefStd('procedurestds', ${instrument.plantid}, ${procstand.id});">
                                                    <img src="img/icons/delete.png" width="16" height="16"
                                                         alt="<spring:message code='viewprocedure.deleteprocedurestandard'/>"
                                                         title="<spring:message code='viewprocedure.deleteprocedurestandard'/>"/>
                                                </a>
                                            </td>
                                        </tr>
                                    </c:forEach>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
				</div>
				<!-- end of standards section -->
				<!-- this section displays file resources for procedure -->
				<div id="resources-tab" class="hid">
					<files:showFilesForSC rootFiles="${scRootFiles}" id="${procedure.id}" allowEmail="false" entity="${procedure}" isEmailPlugin="false" ver="" identifier="${procedure.reference}" deleteFiles="true" sc="${sc}" rootTitle="${procedure.reference}"/>
				</div>
				<!-- end of resources section -->
				<!--  this section displays all procedure training records -->
				<div id="proctraining-tab" class="hid">
				<table class="default4 proctrainingrecords" summary="This table displays all procedure training records">
					<thead>
						<tr>
							<td colspan="8">
								<spring:message code="viewprocedure.proceduretrainingrecords"/>
								(${procedure.capabilityTrainingRecords.size()})
							</td>
						</tr>
						<tr>
							<th class="jobno"><spring:message code="viewprocedure.deleteprocedurestandard"/></th>
							<th class="certno"><spring:message code="viewprocedure.certno"/></th>
		   					<th class="caldate"><spring:message code="caldate"/></th>
					        <th class="issuedate"><spring:message code="issuedate"/></th>
					        <th class="caltype"><spring:message code="caltype"/></th>
					        <th class="trainee"><spring:message code="viewprocedure.trainee"/></th>
					        <th class="supervisor"><spring:message code="viewprocedure.supervisor"/></th>
							<th class="files"><spring:message code="files"/></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="8">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody>
						<c:choose>
							<c:when test="${procedure.capabilityTrainingRecords.size() == 0}">
								<tr>
									<td colspan="8" class="bold text-center">
										<spring:message code="viewprocedure.therearenoproceduretrainingrecordstodisplay"/>
									</td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:forEach var="ptr" items="${procedure.capabilityTrainingRecords}">
									<tr>
										<td class="jobno">
											<c:set var="rowcount" value="0"/>
											<c:forEach var="link" items="${ptr.cert.links}">
												<links:jobnoLinkDWRInfo rowcount="${rowcount}" jobno="${link.jobItem.job.jobno}" jobid="${link.jobItem.job}" copy="false"/>
												<links:jobitemLinkDWRInfo jobitem="${link.jobItem}" jicount="false" rowcount="${rowcount}"/>
												<c:set var="rowcount" value="${rowcount + 1}"/>
												<br />
											</c:forEach>
										</td>
										<td class="certno">${ptr.cert.certno}</td>
										<td class="caldate"><fmt:formatDate value="${ptr.cert.calDate}" dateStyle="medium" type="date" /></td>
				        				<td class="issuedate"><fmt:formatDate value="${ptr.cert.certDate}" dateStyle="medium" type="date" /></td>
				       					<td class="caltype"><t:showTranslationOrDefault translations="${ptr.cert.calType.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/></td>
										<td class="trainee"><a href="viewperson.htm?personid=${ptr.trainee.personid}&loadtab=proctraining-tab" class="mainlink" target="_blank">${ptr.trainee.name}</a></td>
										<td class="supervisor">${ptr.labManager.name}</td>
										<td class="files">
											<c:forEach var="link" items="${ptr.cert.links}">
												<c:forEach var="f" items="${link.certFiles}">
													<a href='file:///${f.absolutePath}' class="mainlink" target="_blank">${f.name}</a><br/>
												</c:forEach>
											</c:forEach>
			       						</td>
									</tr>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
			</div>
			<!-- end of procedure training records -->
		</div>
		<!-- end of infobox div -->
	</jsp:body>
</t:crocodileTemplate>