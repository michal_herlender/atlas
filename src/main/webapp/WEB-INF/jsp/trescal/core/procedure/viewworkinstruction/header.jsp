<%-- File name: /trescal/core/procedure/viewworkinstruction/header.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div class="infobox">								
		<fieldset>
			<legend><spring:message code="viewworkinstruction.viewworkinstruction"/></legend>
			<ol>
				<li>
					<label><spring:message code="businesscompany" />:</label>
					<span><c:out value="${wi.organisation.coname}"/></span>
				</li>									
				<li>
					<label><spring:message code="title"/>:</label>
					<span>${wi.title}</span>
				</li>										
				<li>
					<label><spring:message code="name"/>:</label>
					<span>${wi.name}</span>
				</li>	
				<li>
					<label><spring:message code="description.description"/>:</label>
					<span>${wi.description.description}</span>
				</li>							
			</ol>
		</fieldset>
	</div>