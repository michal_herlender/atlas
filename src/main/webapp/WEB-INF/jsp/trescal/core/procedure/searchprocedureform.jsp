<%-- File name: /trescal/core/procedure/searchprocedureform.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="searchprocedureform.procedurebrowser"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/procedure/SearchProcedureForm.js'></script>
	</jsp:attribute>
	<jsp:body>
		<!-- error section displayed when form submission fails -->
		<form:errors path="searchprocedureform.*">
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<h5 class="center-0 attention"><spring:message code="searchprocedureform.oneormoreerrorsoccuredsearchingforprocedures"/>:</h5>
						<c:forEach var="e" items="${messages}">
							<div class="center attention"><c:out value="${e}"/></div>
						</c:forEach>
					</div>
				</div>
			</div>
		</form:errors>
		<!-- end error section -->
		<!-- infobox contains all form elements for procedure searching and is styled with nifty corners -->				
		<div class="infobox">
			<form:form method="post" action="" id="searchinstrumentform" modelAttribute="searchprocedureform">
				<fieldset id="procedureSearch">
					<legend><spring:message code="searchprocedureform.searchprocedure"/></legend>
					<ol>
						<li>
							<label><spring:message code="subdivision"/>:</label>
							<div id="cascadeSearchPlugin">
								<input type="hidden" id="compCoroles" value="supplier,business" />
								<input type="hidden" id="cascadeRules" value="subdiv,address" />
								<input type="hidden" id="prefillIds" value="${searchprocedureform.coid},${searchprocedureform.subdivid}" />
							</div>
						</li>
						<li>
                            <label><spring:message code="sub-family" />:</label>                                               
                            <!-- float div left -->
                            <div class="float-left">
                                <form:input size="40" path="subfamilyName" name="subfamilyName" tabindex="3"/>
								<form:hidden path="subfamilyId" name="subfamilyId" />			
                            </div>
                                                                                                                      
                            <!-- clear floats and restore pageflow -->
                            <div class="clear-0"></div>
                        </li>
						<li>
							<label><spring:message code="searchprocedureform.procedurereference"/>:</label>
							<form:input size="40" path="reference" tabindex="4"/>
						</li>
						<li>
							<label><spring:message code='name'/>:</label>
							<form:input size="80" path="name" tabindex="5"/>
						</li>
						<li>
							<label><spring:message code='description'/>:</label>
							<form:input size="80" path="description" tabindex="6"/>
						</li>
						<li>
							<label><spring:message code="searchprocedureform.activeonly"/>:</label>
							<form:checkbox path="active" value="true" tabindex="7"/>
						</li>
						<li>
							<label>&nbsp;</label>
							<input type="submit" name="search" value=<spring:message code="search"/> tabindex="6" />
						</li>
					</ol>
				</fieldset>
			</form:form>
		</div>
		<!-- end of infobox div -->
	</jsp:body>
</t:crocodileTemplate>