<%-- File name: /trescal/core/asset/assetsearchresults.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="assetsearchres.headtext"/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/asset/AssetSearchResults.js'></script>
    </jsp:attribute>
    <jsp:body>
			<!-- infobox contains all search form elements and is styled with nifty corners -->
			<div class="infobox" id="assetsearchresults">
																				
				<c:set var="results" value="${form.rs.results}"/>
				
				<form action="" name="searchform" id="searchform" method="post">
			
					<form:hidden path="form.resultsPerPage" />
					<form:hidden path="form.pageNo" />
					
					<form:hidden path="form.assetNo" />
					<form:hidden path="form.assettypeId" />
					<form:hidden path="form.departmentId" />
					<form:hidden path="form.supplierId" />
					<form:hidden path="form.physLocId" />
					<form:hidden path="form.name" />
					<form:hidden path="form.personid" />
					<form:hidden path="form.plantNo" />
					<form:hidden path="form.serialNo" />
					<form:hidden path="form.statusId" />							
																							
					<t:showResultsPagination rs="${form.rs}" pageNoId=""/>
												
					<table class="default4 assetSearchResults" summary="<spring:message code='assetsearchres.tblsummary'/>">
						<thead>
							<tr>
								<td colspan="10"><spring:message code="assetsearchres.resultcount"/> (${results.size()})</td>
							</tr>
							<tr>
								<th class="number" scope="col"><spring:message code="asset.assetno"/></th>
								<th class="type" scope="col"><spring:message code="asset.assettype"/></th>
								<th class="name" scope="col"><spring:message code="asset.assetname"/></th>
								<th class="qty" scope="col"><spring:message code="assetsearchres.qty"/></th>
								<th class="department" scope="col"><spring:message code="asset.department"/></th>
								<th class="owner" scope="col"><spring:message code="asset.owner"/></th>
								<th class="serialno" scope="col"><spring:message code="serialno"/></th>
								<th class="plantno" scope="col"><spring:message code="plantno"/></th>
							</tr>
						</thead>
						<tbody>
					
							<c:set var="rowcount" value="0"/>
								<c:choose>
									<c:when test="${results.size() < 1}">
									<tr>
										<td colspan="10" class="bold center">
											<spring:message code="assetsearchres.nores"/>
										</td>
									</tr>
									
									</c:when>
									<c:otherwise>
							
									<c:forEach var="asset" items="${results}">
									
									<tr>
										<td class="number"><a href="viewasset.htm?id=${asset.assetId}" class="mainlink">${asset.assetNo}</a></td>
										<td class="type"><t:showTranslationOrDefault translations="${asset.type.translations}" defaultLocale="${defaultlocale}"/></td>
										<td class="name">
											<c:choose>
												<c:when test="${asset.assettype == 'PHYSICAL' && not empty asset.inst}">
													<links:instrumentLinkDWRInfo instrument="${asset.inst}" rowcount="0" displayBarcode="false" displayName="true" displayCalTimescale="false" caltypeid="0" />
												</c:when>
												<c:otherwise>
													${asset.name}
												</c:otherwise>
											</c:choose>	
										</td>
										<td class="qty">${asset.quantity}</td>
										<td class="department">${asset.dept.name}</td>
										<td class="owner">${asset.owner.name}</td>
										<c:if test="${asset.assettype == 'PHYSICAL'}">
											<c:choose>
												<c:when test="${not empty asset.inst}">
													<td class="serialno">${asset.inst.serialno}</td>														
													<td class="plantno">${asset.inst.plantno}</td>
												</c:when>
												<c:otherwise>
													<td class="serialno">${asset.serialNo}</td>														
													<td class="plantno">${asset.plantNo}</td>
												</c:otherwise>
											</c:choose>																								
										</c:if>
									</tr>													
															
								</c:forEach>								

								</c:otherwise>
							</c:choose>	
						
						</tbody>
					</table>
					
						<t:showResultsPagination rs="${form.rs}" pageNoId=""/>
				
				</form>

			</div>
			<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>