<%-- File name: /trescal/core/asset/newassetfrominstrument.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="newassetinst.headtext" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/asset/NewAssetFromInstrument.js'></script>
    </jsp:attribute>
    <jsp:body>
			<!-- infobox contains all edit instrument form elements and is styled with nifty corners -->
			<div class="infobox">
			
				<form:form action="" method="post" modelAttribute="form">
					<form:errors path="*">
        					<div class="warningBox1">
                	 			<div class="warningBox2">
                    	   			<div class="warningBox3">
                        	    		<h5 class="center-0 attention">
                            	    		 <spring:message code='error.save' />
                            			</h5>
                            			<c:forEach var="e" items="${messages}">
                               				<div class="center attention"><c:out value="${e}"/></div>
                            			</c:forEach>
                     	 			</div>
                 				</div>
           					</div>
       	    			</form:errors>
				
					<fieldset>
					
						<legend><spring:message code="newassetinst.search" /></legend>
						
						<!-- displays content in column 50% wide (left) -->
						<div class="displaycolumn">
						
							<ol>
							
								<li>
									<label><spring:message code="company" />:</label>
									<form:select path="coid" name="businesscompId" onchange=" searchBusinessInstruments(); return false; ">
										<c:forEach var="comp" items="${businesscomps}">
											<option value="${comp.coid}" <c:if test="${comp.coid == form.coid }"> selected</c:if>>${comp.coname}</option>
										</c:forEach>
									</form:select>
								</li>
								<li>
									<label><spring:message code="barcode" />:</label>
									<form:input path="plantId" name="searchplantid" id="searchplantid" size="10" onkeyup=" searchBusinessInstruments(); return false; " />
									<form:errors path="plantId" class="attention"/>
									<input type="checkbox" name="standards" checked="checked" class="marginleft60 checkbox" onclick=" searchBusinessInstruments(); " /><spring:message code="newassetinst.standardsonly" />
								</li>
								<li>
									<label><spring:message code="serialno" />:</label>
									<input type="text" name="serialno" onkeyup=" searchBusinessInstruments(); return false; " />
								</li>
								<li>
									<label><spring:message code="plantno" />:</label>
									<input type="text" name="plantno" onkeyup=" searchBusinessInstruments(); return false; " />
								</li>
								
							</ol>										
							
						</div>
						
						<!-- displays content in column 50% wide (right) -->
						<div class="displaycolumn">
						
							<ol>
							
								<li style=" height: 20px; ">
									<label>&nbsp;</label>
									<span id="loading" class="hid">
										<img src="img/web/searching.gif" width="128" height="15" alt="<spring:message code='newassetinst.loadingresults' />" title="<spring:message code='newassetinst.loadingresults' />" />
									</span>
									<span>&nbsp;</span>
								</li>
								<li>
									<label><spring:message code="manufacturer" />:</label>
									<input type="text" name="mfr" onkeyup=" searchBusinessInstruments(); return false; " />
								</li>
								<li>
									<label><spring:message code="model" />:</label>
									<input type="text" name="model" onkeyup=" searchBusinessInstruments(); return false; " />
								</li>
								<li>
									<label><spring:message code="sub-family" />:</label>
									<input type="text" name="desc" onkeyup=" searchBusinessInstruments(); return false; " />
								</li>
<!-- 								<li> -->
									
<!-- 									<br/><br/> -->
<%-- 									<input type="submit" value="<spring:message code='asset.submit' />" /> --%>
<!-- 								</li> -->
							</ol>
						
						</div>
						
					</fieldset>										
				
				</form:form>
											
			</div>
			<!-- end of infobox div -->
			
			<!-- infobox contains all edit instrument form elements and is styled with nifty corners -->
			<div class="infobox">
			
				<table class="default4 newInstAsset" summary="<spring:message code='newassetinst.resultstblsummary' />">
					<thead>
						<tr>
							<td colspan="5"><spring:message code="newassetinst.resultstblheader" />(<span class="businessCompanyInstSize">0</span>)</td>
						</tr>
						<tr>
							<th class="add" scope="col"><spring:message code="newassetinst.addasset" /></th>
							<th class="barcode" scope="col"><spring:message code="barcode" /></th>
							<th class="desc" scope="col"><spring:message code="sub-family" /></th>
							<th class="serialno" scope="col"><spring:message code="serialno" /></th>
							<th class="plantno" scope="col"><spring:message code="plantno" /></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="5">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody>
						<tr>
							<td colspan="5" class="bold center"><spring:message code="newassetinst.searchmessage" /></td>
						</tr>
					</tbody>								
				</table>							
			
			</div>
			<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>