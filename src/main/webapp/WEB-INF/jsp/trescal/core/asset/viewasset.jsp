<%-- File name: /trescal/core/_PATH1_/_PATH2_/_FILE_.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="viewasset.view" />&nbsp;${command.asset.assettype.type}
        					   &nbsp;<spring:message code="asset.assetno" />&nbsp;(<c:out value="${command.asset.assetNo}"/>)</span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/asset/ViewAsset.js'></script>
    </jsp:attribute>
    <jsp:body>
        		<!-- this infobox holds general info about the asset -->
			<div class="infobox">
			
				<fieldset>

					<legend><c:out value="${command.asset.assettype.type} "/><spring:message code="asset.assetno" /><c:out value=" - ${command.asset.assetNo}"/></legend>

					<!-- displays content in column 50% wide (left) -->
					<div class="displaycolumn">
					
						<ol>
						
							<li>
								<label><spring:message code="asset.assetno" /> / <spring:message code="asset.type" />:</label>
								<span class="bold">${command.asset.assetNo} / ${command.asset.assettype.type}</span>
							</li>
							<li>
								<label><spring:message code="asset.assetname" />:</label>
								<span>
									<c:choose>
										<c:when test="${command.asset.assettype == 'PHYSICAL' && not empty command.asset.inst}">
											<links:instrumentLinkDWRInfo displayCalTimescale="true" displayName="true" instrument="${command.asset.inst}" caltypeid="0" rowcount="0" displayBarcode="true" />
										</c:when>
										<c:otherwise>
											${command.asset.name}
										</c:otherwise>
									</c:choose>
								</span>
							</li>
							<li>
								<label><spring:message code="asset.assettype" />:</label>
								<span><t:showTranslationOrDefault translations="${command.asset.type.translations}" defaultLocale="${defaultlocale}"/></span>
							</li>
							<li>
								<label><spring:message code="asset.owner" />:</label>
								<span>${command.asset.owner.name}</span>
							</li>
							<c:if test="${command.asset.custodian}">
								<li>
									<label><spring:message code="asset.custodian" />:</label>
									<span>${command.asset.custodian.name}</span>
								</li>
							</c:if>
							<c:if test="${command.asset.primaryUser}">
								<li>
									<label><spring:message code="asset.primaryuser" /></label>
									<span>${command.asset.primaryUser.name}</span>
								</li>
							</c:if>
							<li>
								<label><spring:message code="asset.department" />:</label>
								<span>${command.asset.dept.name}</span>
							</li>
							<c:if test="${command.asset.assettype == 'PHYSICAL'}">
								<li>
									<label><spring:message code="asset.location" />:</label>
									<span>
										${command.asset.physicalLocation.location}
										<c:if test="${command.asset.physicalLocation.add.addr1 != null && command.asset.physicalLocation.add.addr1 != ''}">
											(${command.asset.physicalLocation.add.addr1}, ${command.asset.physicalLocation.add.town})
										</c:if>
									</span>
								</li>
								<li>
									<label><spring:message code="asset.ipaddress" />:</label>
									<span>${command.asset.ipAddress}</span>
								</li>
								<li>
									<label><spring:message code="asset.supplier" />:</label>
									<span><links:companyLinkDWRInfo copy="true" company="${command.asset.supplier}" rowcount="0" /></span>
								</li>
							</c:if>
							<c:if test="${command.asset.assettype == NETWORK}">
								<li>
									<label><spring:message code="asset.networklocation" />:</label>
									<span>
										<a href="file://${command.asset.networkLocation}" class="mainlink" target="_blank">${command.asset.networkLocation}</a>
									</span>
								</li>
							</c:if>
																										
						</ol>

					</div>

					<!-- displays content in column 50% wide (left) -->
					<div class="displaycolumn">
					
						<ol>
							
							<li>
								<label><spring:message code="asset.status" />:</label>
								<span><t:showTranslationOrDefault translations="${command.asset.status.translations}" defaultLocale="${defaultlocale}"/></span>
							</li>
							<li>
								<label><spring:message code="asset.quantity" />:</label>
								<span>${command.asset.quantity}</span>
							</li>
							<c:if test="${command.asset.assettype == 'PHYSICAL'}">
								<c:choose>
									<c:when test="${not empty command.asset.inst}">
										<li>
											<label><spring:message code="serialno" />:</label>
											<span>${command.asset.inst.serialno}</span>
										</li>
										<li>
											<label><spring:message code="plantno" />:</label>
											<span>${command.asset.inst.plantno}</span>
										</li>
									</c:when>
									<c:otherwise>
										<li>
											<label><spring:message code="serialno" />:</label>
											<span>${command.asset.serialNo}</span>
										</li>
										<li>
											<label><spring:message code="plantno" />:</label>
											<span>${command.asset.plantNo}</span>
										</li>
									</c:otherwise>
								</c:choose>				
							</c:if>
							<li>
								<label><spring:message code="viewasset.addedon" />:</label>
								<span><fmt:formatDate value="${command.asset.editableAddedOn}" type="date" dateStyle="SHORT"/></span>
							</li>
							<li>
								<label><spring:message code="viewasset.addedby" />:</label>
								<span>${command.asset.systemAddedBy.name}</span>
							</li>
							<c:if test="${command.asset.purchasedOn != null && command.asset.purchasedOn != ''}">
								<li>
									<label><spring:message code="viewasset.purchasedon" />:</label>
									<span><fmt:formatDate value="${command.asset.purchasedOn}" type="date" dateStyle="SHORT"/></span>
								</li>
							</c:if>
							<c:if test="${command.asset.price != null && command.asset.price != '0.00'}">
								<li>
									<label><spring:message code="asset.price" />:</label>
									<span>&pound;${command.asset.price}</span>
								</li>
							</c:if>
																	
						</ol>

					</div>
					
				</fieldset>
				
			</div>
			<!-- end of general info box -->
			
			<!-- sub navigation menu which displays and hides sections of page as well as linking to external pages -->
			<div id="subnav">
				<dl>
					<dt><a href="#" class="selected" title="<spring:message code='viewasset.actionsavailabletoperformonasset' />"><spring:message code="viewasset.actions" /></a></dt>
					<dt><a href="<spring:url value ='addeditasset.htm?assetid=${command.asset.assetId}'/>" title="<spring:message code='viewasset.actionsavailabletoperformonasset' />"><spring:message code="viewasset.edit" /></a></dt>
				</dl>
			</div>
			<!-- end of sub navigation menu -->
			
			<!-- this infobox holds general info about the asset -->
			<div class="infobox">
			
				<fieldset>

					<legend><spring:message code="viewasset.actions" /></legend>		
					
					<!-- displays content in column 50% wide (left) -->
					<div class="displaycolumn">
					
						<ol>
						
							<li>
								<label><spring:message code="viewasset.printlabel" />:</label>
								<span>											
									<a href="#" class="mainlink" onclick=" printAssetLabel(${command.asset.assetId}, $j(this).next('input:checkbox').is(':checked')); return false; ">
										<spring:message code="viewasset.printlabel" />
									</a>
									&nbsp;
									<input type="checkbox" class="checkbox" />&nbsp;<spring:message code="viewasset.includeip" />
								</span>
							</li>
							
						</ol>
					
					</div>
					
					<!-- displays content in column 50% wide (right) -->
					<div class="displaycolumn">
					
						<ol>
					
							<li>
								<label><spring:message code="viewasset.deleteasset" />:</label>
								<span>											
									<a href="#" class="mainlink" onclick=" deleteAsset(${command.asset.assetId}); return false; ">
										<img src="img/icons/delete.png" width="16" height="16" class="image_inline3" /> &nbsp;<spring:message code="viewasset.deleteasset" />
									</a>											
								</span>
							</li>
						
						</ol>
																																			
					</div>							
					
				</fieldset>
				
			</div>
			<!-- end of general info box -->
			
			<!-- this section contains all quote notes -->
			<t:showTabbedNotes entity="${command.asset}" privateOnlyNotes="${command.privateOnlyNotes}" noteTypeId="${command.asset.assetId}" noteType="ASSETNOTE"/>
			<!-- end of quote notes section -->
    </jsp:body>
</t:crocodileTemplate>