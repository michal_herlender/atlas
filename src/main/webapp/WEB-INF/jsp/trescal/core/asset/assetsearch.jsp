<%-- File name: /trescal/core/asset/assetsearch.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="assetsearch.headtext"/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/jobs/certificate/CertSearch.js'></script>
    </jsp:attribute>
    <jsp:body>
   			<!-- infobox contains all form elements relating to searching for a job and is styled with nifty corners -->				
			<div class="infobox">
			
				<form:form method="post" action="" id="assetsearchform" modelAttribute="form">
				
					<!-- error section displayed when form submission fails -->
					<form:errors path="*">
                    <div class="warningBox1">
                        <div class="warningBox2">
                            <div class="warningBox3">
                                <h5 class="center-0 attention">
                                    <spring:message code='asset.error' />
                                </h5>
                                <c:forEach var="e" items="${messages}">
                                    <div class="center attention"><c:out value="${e}"/></div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                   </form:errors>
				<!-- end error section -->

					<fieldset>

						<legend><spring:message code="assetsearch.headtext"/></legend>
					
						<!-- displays content in column 69% wide (left) -->
						<div style=" float:left; padding:0.4%; width:49%; ">

							<ol>	
								<li>
									<label><spring:message code="asset.assettype"/></label>
									<form:select path="assettypeId" tabindex="1">
										<option value=""><spring:message code="assetsearch.optionall"/></option>
										<c:forEach var="at" items="${assettypes}">
											<option value="${at.id}"><t:showTranslationOrDefault translations="${at.translations}" defaultLocale="${defaultlocale}"/></option>
										</c:forEach>
									</form:select>
								</li>									
								<li>
									<form:label path="assetNo"><spring:message code="asset.assetno"/>:</form:label>
									<form:input path="assetNo" tabindex="2" />
								</li>
								<li>
									<form:label path="name"><spring:message code="asset.assetname"/>:</form:label>
									<form:input path="name" tabindex="3" />
								</li>											
								<li>												
									<label><spring:message code="asset.owner"/>:</label>
									<div id="contSearchPlugin">
										<input type="hidden" id="contCoroles" value="business" />
										<input type="hidden" id="contindex" value="4" />
										<!-- contact results listed here -->
									</div>											
									<!-- clear floats and restore page flow -->
									<div class="clear"></div>
								</li>
								<li>
									<form:label path="plantId"><spring:message code="barcode"/>:</form:label>
									<form:input path="plantId" tabindex="5" />
								</li>
								<li>
									<label>&nbsp;</label>
									<input type="submit" name="submit" value="<spring:message code='submit'/>" tabindex="11" />
								</li>											
							</ol>
						
						</div>
						<!-- end of left column -->
						
						<!-- displays content in column 49% wide (right) -->
						<div style=" float:right; padding: 0.4%; width:49%; ">
						
							<ol>
								
								<li>
									<form:label path="physLocId"><spring:message code="asset.location"/>:</form:label>												
									<form:select path="physLocId">
										<option value=""> <spring:message code="assetsearch.optionall"/> </option>
										<c:if test="${businesscompanylocations.size() > 0}">
											<c:set var="currentAddr" value="${businesscompanylocations.get(0).add.addrid}"/>
											<optgroup label="${businesscompanylocations.get(0).add.addr1}, ${businesscompanylocations.get(0).add.town}">
											<c:forEach var="loc" items="${businesscompanylocations}">
												<c:if test="${currentAddr != loc.add.addrid}">
													<c:set var="currentAddr" value="${loc.add.addrid}"/>
													</optgroup>
													<optgroup label="${loc.add.addr1}, ${loc.add.town}">
												</c:if>
												<option value="${loc.locationid}">${loc.location}</option>
											</c:forEach>
											</optgroup>
										</c:if>
									</form:select>
								</li>
								<li>
									<label><spring:message code="asset.department"/>:</label>
									<form:select path="departmentId" tabindex="6">
										<option value=""> <spring:message code="assetsearch.optionall"/> </option>
										<c:forEach var="dept" items="${departments}">
											<option value="${dept.deptid}">${dept.name}</option>
										</c:forEach>
									</form:select>
									&nbsp;
								</li>
								<li>
									<label><spring:message code="status"/>:</label>
									<form:select path="statusId" tabindex="7">
										<option value=""> <spring:message code="assetsearch.optionall"/> </option>
										<c:forEach var="st" items="${assetstatus}">
											<option value="${st.id}"><t:showTranslationOrDefault translations="${st.translations}" defaultLocale="${defaultlocale}"/></option>
										</c:forEach>
									</form:select>
									&nbsp;
								</li>
								<li>
									<form:label path="serialNo"><spring:message code="serialno"/>:</form:label>
									<form:input path="serialNo" tabindex="8" />
								</li>
								<li>
									<form:label path="plantNo"><spring:message code="plantno"/>:</form:label>
									<form:input path="plantNo" tabindex="9" />
								</li>
								<li>
									<label><spring:message code="asset.supplier"/>:</label>
									<div class="float-left">
										<div class="compSearchJQPlugin">
											<input type="hidden" name="field" value="supplierId" />
											<input type="hidden" name="compCoroles" value="supplier" />
											<input type="hidden" name="tabIndex" value="10" />
											<!-- company results listed here -->
										</div>
									</div>									
									<!-- div clears the page flow -->
									<div class="clear"></div>										
								</li>														
							</ol>
						
						</div>
						<!-- end of right column -->

					</fieldset>
					
				</form:form>

			</div>
			<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>