<%-- File name: /trescal/core/asset/addeditasset.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext">	<c:choose>
       								<c:when test="${form.persisted}"> 
        								<spring:message code="addeditasset.edit" /> 
        							</c:when> 
        							<c:otherwise>	
        								<spring:message code="addeditasset.add" /> 
       								</c:otherwise> 
       							</c:choose>
       								&nbsp;${form.asset.assettype.type}&nbsp; 
        						<spring:message code="addeditasset.asset" />
        </span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/asset/AddEditAsset.js'></script>
    </jsp:attribute>
    <jsp:body>
	<!-- infobox contains all edit instrument form elements and is styled with nifty corners -->
	<div class="infobox">

	<form:form action="" method="post" modelAttribute="form" class="addEditAssetForm">
			<!-- error section displayed when form submission fails -->
			<form:errors path="*">
                 <div class="warningBox1">
                   <div class="warningBox2">
                      <div class="warningBox3">
                         <h5 class="center-0 attention">
                             <spring:message code='asset.error' />
                          </h5>
                          <c:forEach var="e" items="${messages}">
                              <div class="center attention"><c:out value="${e}"/></div>
                           </c:forEach>
                        </div>
                     </div>
                  </div>
             </form:errors>
             <!-- end error section -->
             
		<fieldset>
			<legend>
				<c:choose>
					<c:when test="${form.persisted}">
						<spring:message code="addeditasset.editasset" /> (<a href="<spring:url value='viewasset.htm?id=${form.asset.assetId}'/>" class="mainlink"><spring:message code="addeditasset.back" />&nbsp;<c:out value="${form.asset.assetNo}"/></a>)
			    	</c:when> 															
			   		<c:otherwise>
						<spring:message code="addeditasset.add" />${form.asset.assettype.type} <spring:message code="addeditasset.asset" />
					</c:otherwise>
				</c:choose>
			</legend>
	
			<ol>
			<c:choose>
				<c:when test="${form.persisted}">
					<li>
						<label><spring:message code="asset.assetno" />:</label>
						<span>${form.asset.assetNo}</span>
					</li>
				</c:when>
				<c:otherwise>
					<li>
						<label><spring:message code="asset.type" /></label>
						<span>${form.asset.assettype.type} <a href="<spring:url value='addeditasset.htm?type=${form.asset.oppassettype}'/>" class="marginleft60 mainlink"><spring:message code="addeditasset.add" />&nbsp;<c:out value="${form.asset.oppassettype}"/>&nbsp;<spring:message code="addeditasset.asset" /></a></span>
					</li>
				</c:otherwise>
			</c:choose>											
				<li>
					<label><spring:message code="asset.assettype" />:</label>
					<c:choose>
						<c:when test="${form.asset.assettype == 'PHYSICAL' && not empty form.asset.inst}">
							<span><t:showTranslationOrDefault translations="${form.asset.type.translations}" defaultLocale="${defaultlocale}"/></span>
						</c:when>
						<c:otherwise>
							<form:select path="typeId">
								<option value=""><spring:message code="addeditasset.optionpleaseselect" /></option>
								<c:forEach var="type" items="${types}">
									<option <c:if test="${form.asset.type.id == type.id}"> selected </c:if> value="${type.id}"><t:showTranslationOrDefault translations="${type.translations}" defaultLocale="${defaultlocale}"/></option>
								</c:forEach>
							</form:select>
							<form:errors path="typeId" cssClass="attention"/>
						</c:otherwise>
					</c:choose>													
				</li>										
				<li>
					<label><spring:message code="asset.quantity" />:</label>
					<c:choose>
						<c:when test="${form.asset.assettype == 'PHYSICAL' && not empty form.asset.inst}">
							<span>${form.asset.quantity}</span>
						</c:when>
						<c:otherwise>
								<form:select path="asset.quantity">													
									<c:forEach var="qty" begin="1" end="50">
										<option value="${qty}"<c:if test="${form.asset.quantity == qty}"> selected </c:if> >${qty}</option>
									</c:forEach>
								</form:select>
						</c:otherwise>
					</c:choose>	
				</li>											
				<li>
					<label><spring:message code="asset.assetname" />:</label>
					<c:choose>
						<c:when test="${form.asset.assettype == 'PHYSICAL' && not empty form.asset.inst}">
							<links:instrumentLinkDWRInfo displayCalTimescale="true" displayName="true" instrument="${form.asset.inst}" caltypeid="0" rowcount="0" displayBarcode="true" />
						</c:when>
						<c:otherwise>
							<form:input path="asset.name"/>
						</c:otherwise>
					</c:choose>
				</li>											
				<li>
					<label><spring:message code="asset.status" />:</label>
					<form:select path="statusId">
						<option value=""><spring:message code="addeditasset.optionpleaseselect" /></option>
						<c:forEach var="st" items="${statuses}">
							<option <c:if test="${form.asset.status.id == st.id}"> selected </c:if> value="${st.id}"><t:showTranslationOrDefault translations="${st.translations}" defaultLocale="${defaultlocale}"/></option>
						</c:forEach>
					</form:select>
				</li>											
				<li>
					<label><spring:message code="asset.department" />:</label>
					<form:select path="deptId">
						<option value=""><spring:message code="addeditasset.optionpleaseselect" /></option>
						<c:forEach var="dept" items="${depts}">
							<option <c:if test="${form.asset.dept.deptid == dept.deptid}"> selected </c:if> value="${dept.deptid}">${dept.name}</option>
						</c:forEach>
					</form:select>
				</li>											
				<li>
					<label><spring:message code="asset.owner" />:</label>
					<form:select path="ownerId">
						<c:forEach var="con" items="${companycontacts}">
							<option <c:if test="${form.asset.owner.personid == con.key}"> selected </c:if> value="${con.key}">${con.value}</option>
						</c:forEach>
					</form:select>
					<input type="checkbox" class="marginleft60 checkbox widthAuto" onclick=" toggleUserOptions($j(this).parent(), this.checked); " <c:if test="${form.persisted && form.custodianId || form.primaryUserId}"> checked="checked" </c:if>/> <spring:message code="addeditasset.ownercheckbox" />
				</li>											
				<li <c:choose>
						<c:when test="${form.persisted && form.custodianId || form.primaryUserId}"> class="vis" </c:when>
						<c:otherwise> class="hid" </c:otherwise>
					</c:choose>>
					<label><spring:message code="asset.custodian" />:</label>
					<form:select path="custodianId">
						<option value=""><spring:message code="addeditasset.optionpleaseselect" /></option>
						<c:forEach var="con" items="${businesscontacts}">
							<option <c:if test="${form.asset.custodian.personid == con.personid}"> selected </c:if> value="${con.personid}">${con.name}</option>
						</c:forEach>
					</form:select>
				</li>											
				<li <c:choose>
						<c:when test="${form.persisted && form.custodianId || form.primaryUserId}"> class="vis" </c:when>
						<c:otherwise> class="hid" </c:otherwise>
					</c:choose>>
					<label><spring:message code="asset.primaryuser" />:</label>
					<form:select path="primaryUserId">
						<option value=""><spring:message code="addeditasset.optionpleaseselect" /></option>
						<c:forEach var="con" items="${businesscontacts}">
							<option <c:if test="${form.asset.primaryUser.personid == con.personid}"> selected </c:if> value="${con.personid}">${con.name}</option>
						</c:forEach>
					</form:select>
				</li>
															
				<c:choose>
					<c:when test="${form.asset.assettype == 'PHYSICAL'}">
					 <li>
						<label><spring:message code="asset.location" />:</label>
						<form:select path="physLocId">  
							<option value=""><spring:message code="addeditasset.optionpleaseselect" /></option>
							<c:if test="${companylocations.size() > 0}">
								<c:set var="currentAddr" value="${companylocations.get(0).add.addrid}"/>
								<optgroup label="${companylocations.get(0).add.addr1}, ${companylocations.get(0).add.town}">
								<c:forEach var="loc" items="${companylocations}">
									<c:if test="${currentAddr != loc.add.addrid}">
									<c:set var="currentAddr" value="${loc.add.addrid}"/>
									</optgroup>
									<optgroup label="${loc.add.addr1}, ${loc.add.town}">
									</c:if>
									<option value="${loc.locationid}">${loc.location}</option>
								</c:forEach>
								</optgroup>
							</c:if>
					   	 </form:select>
					 </li>												
					<li>
						<label><spring:message code="serialno" />:</label>
						<c:choose>
						<c:when test="${not empty form.asset.inst}">
								<span>${form.asset.inst.serialno}</span>
							</c:when>
							<c:otherwise>
								<form:input path="asset.serialNo"/>
							</c:otherwise>
						</c:choose>
					</li>												
					<li>
						<label><spring:message code="asset.plant" />:</label>
						<c:choose>
						<c:when test="${not empty form.asset.inst}">
								<span>${form.asset.inst.plantno}</span>
							</c:when>
							<c:otherwise>
								<form:input path="asset.plantNo"/>
							</c:otherwise>
						</c:choose>
					</li>												
					<li>
						<label><spring:message code="asset.ipaddress" />:</label>
						<form:input path="asset.ipAddress"/>					
					</li>												
					 <li>
						<label><spring:message code="asset.supplier" />:</label>
							<div class="float-left">
								<div class="compSearchJQPlugin">
									<input type="hidden" name="field" value="supplierId" />
									<input type="hidden" name="compIdPrefill" value="${form.asset.supplier.coid}" />
									<input type="hidden" name="compNamePrefill" value="${form.asset.supplier.coname}" />
									<input type="hidden" name="compCoroles" value="supplier" />
									<input type="hidden" name="tabIndex" value="6" />
									<!-- company results listed here -->
								</div>
							</div>					
						<!-- div clears the page flow -->
						<div class="clear"></div>										
					 </li>
					</c:when>
					<c:otherwise>
					 <li>
						<label><spring:message code="asset.networklocation" />:</label>
						<form:input path="asset.networkLocation"/>
					 </li>
					</c:otherwise>
				</c:choose>
				
				<li>
					<label><spring:message code="addeditasset.adddate" />:</label>
					<form:input path="asset.editableAddedOn" type="date" value="${form.asset.editableAddedOn}"  autocomplete="off" />
				</li>											
				<li>
					<label><spring:message code="addeditasset.purchasedate" />:</label>
					<form:input path="asset.purchasedOn" type="date" value="${form.asset.purchasedOn}"  autocomplete="off" />													
				</li>											
				<li>
					<label><spring:message code="asset.price" />:</label>
					<form:input path="asset.price"/>				
				</li>
				<li>
					<label>&nbsp;</label>
					<input type="submit" value="<spring:message code='submit' />" class="button" />
				</li>
		
			</ol>									
		
		</fieldset>
		
	</form:form>
								
	</div>
	<!-- end of infobox div -->
	</jsp:body>
</t:crocodileTemplate>