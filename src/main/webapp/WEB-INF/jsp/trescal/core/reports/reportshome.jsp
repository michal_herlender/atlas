<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="tools.reports"/></span>
	</jsp:attribute>
    <jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/tools/reports/ReportsHome.js'></script>
	</jsp:attribute>
    <jsp:body>
        <div class="infobox">

            <fieldset>

                <legend><spring:message code="tools.workrequiringattention"/></legend>
                <ol>
                    <li><span>
                    	<cwms:securedLink permission="ITEM_SATTP"  classAttr="mainlink" >
							<spring:message code="tools.subcontractedjobitems"/>
						</cwms:securedLink>
                    </span></li>
                </ol>

             </fieldset>

            <fieldset>

                <legend><spring:message code="tools.financial"/></legend>

                <ol>
                    <li><span><a href="viewhireinstrumentrevenues.htm" class="mainlink"><spring:message code="tools.hireinstrumentrevenues"/></a></span></li>
                </ol>

             </fieldset>

            <fieldset>

                <legend><spring:message code="tools.companyuserloginandcollectionreports"/></legend>

                <ol>
                    <li><span><a href="companycollections.xlsx" class="mainlink"><spring:message code="tools.createcollectionreport"/></a></span></li>
                    <li><span><a href="companyuserlogin.xlsx" class="mainlink"><spring:message code="tools.createuserloginreport"/></a></span></li>
                </ol>
            </fieldset> 

        </div>
    </jsp:body>
</t:crocodileTemplate>