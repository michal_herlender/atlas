<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="tools.reports"/></span>
	</jsp:attribute>
    <jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/tools/reports/CompanyReports.js'></script>
	</jsp:attribute>
    <jsp:body>
    	<t:showErrors path="form.*" />
        <div class="infobox">
			<form:form modelAttribute="form">
	            <fieldset>
	                <legend><c:out value="${reportTitle}"/></legend>
	                <ol>
	                    <li>
	                        <label><spring:message code="company"/>:</label>
	                        <div class="float-left">
	                            <div class="compSearchJQPlugin">
	                                <input type="hidden" name="field" value="coid" />
	                                <input type="hidden" name="compCoroles" value="client,business" />
	                                <input type="hidden" name="compNamePrefill" value="${companyName}" />
	                                <input type="hidden" name="compIdPrefill" value="${form.getCoid()}" />
	                            </div>
	                        </div>
	                        <form:errors path="coid" cssClass="attention" />
	                        <div class="clear"></div>
	                    </li>
	                    <li>
	                        <label><spring:message code="tools.datefrom"/>:</label>
	                        <form:input path="dateFrom" type="date" id="datefrom" autocomplete="off" />
	                        <form:errors path="dateFrom" cssClass="attention" />
	                    </li>
	                    <li>
	                        <label><spring:message code="tools.dateto"/>:</label>
	                        <form:input path="dateTo" type="date" id="dateto" autocomplete="off" />
	                        <form:errors path="dateTo" cssClass="attention" />
	                    </li>
	                    <li>
	                        <label></label>
	                        <spring:message code="generate" var="generateText" />
	                        <input type="submit" id="submit" value="${generateText}"/>
	                    </li>
	                </ol>
	            </fieldset>
            </form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>