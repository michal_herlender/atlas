<%-- File name: /trescal/core/instrumentmodel/searchmodelform.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="searchmod.title"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src="script/trescal/core/instrumentmodel/SearchModelForm.js"></script>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox">
			<cwms:securedLink permission="MODEL_EDIT" collapse="true" classAttr="mainlink-float"><spring:message code="searchmod.addnewmodel"/></cwms:securedLink><br>
			<form:form action="" method="POST" id="searchmodelform">							
				<fieldset>
					<legend><spring:message code="searchmod.title"/></legend>				
					<ol>
						<li>
							<label><spring:message code="domain"/>:</label>
							<!-- float div left -->
							<div class="float-left">
								<form:input path="domainNm" name="domainNm" tabindex="1"/>
								<form:input path="domainId" type="hidden" name="domainId"/>								
							</div>
							<div class="clear-0"></div>	
						</li>		
						<li>
							<label><spring:message code="family"/>:</label>
							<!-- float div left -->
							<div class="float-left">
								<form:input path="familyNm" name="familyNm" tabindex="2"/>
								<form:input path="familyId" type="hidden" name="familyId"/>								
							</div>
							<div class="clear-0"></div>	
						</li>	
						<li id="subfamilyLi">
							<label><spring:message code="sub-family"/>:</label>
							<!-- float div left -->
							<div class="float-left">
								<form:input path="desc" name="desc" tabindex="3"/>
								<form:input path="descId" type="hidden"/>						
							</div>
							<div class="clear-0"></div>	
						</li>
						<li>
							<label><spring:message code="instmod.mfrtype"/>:</label>
								<form:radiobutton path="modelMfrType" style="width:20px;"  value="" checked="checked"/><spring:message code="showall"/>
								<form:radiobutton path="modelMfrType" style="width:20px;"  value="MFR_SPECIFIC"/><spring:message code="instmod.mfrspecific"/>
								<form:radiobutton path="modelMfrType" style="width:20px;" value="MFR_GENERIC"/><spring:message code="instmod.nonmfrspecific"/>
						</li>	
						<li id="manufacturer">
							<label><spring:message code="manufacturer"/>:</label>
							<!-- float div left -->
							<div class="float-left">
								<form:input path="mfrtext" name="mfrtext" tabindex="1"/>
								<form:input path="mfrId" type="hidden"/>						
							</div>
							<!-- clear floats and restore pageflow -->
							<div class="clear-0"></div>
						</li>
						<li id="model">
							<label><spring:message code="model"/>:</label>							
							<form:input path="model" tabindex="5"/>											
						</li>
						<li>
							<label><spring:message code="instmod.modeltype"/>:</label>
								<form:select path="modelTypeId">
									<option value="0">ALL</option>
									<c:forEach items="${modeltypes}" var="type">
										<form:option value="${type.instModelTypeId}">
											<t:showTranslationOrDefault translations='${type.modelTypeNameTranslation}' defaultLocale='${defaultlocale}'/>
										</form:option>
									</c:forEach>
								</form:select>
								<form:errors path="modelTypeId" class="attention"/>
								<span class="attention">${status.errorMessage}</span>
						</li>
						<li>
							<label><spring:message code="salescategory"/>:</label>
							<!-- float div left -->
							<div class="float-left">
								<form:input path="salesCat" name="salesCat" tabindex="6"/>
								<form:input path="salesCatId" type="hidden" name="salesCatId"/>								
							</div>
							<div class="clear-0"></div>	
						</li>	
						<li>
							<label><spring:message code="instmod.exclude_quarantined"/></label>
							<form:checkbox path="excludeQuarantined"/>
						</li>			
						<li>
							<label>&nbsp;</label>
							<input type="submit" name="search" id="submit" value="<spring:message code="search"/>" tabindex="4" />
						</li>					
					</ol>			
				</fieldset>
			</form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>