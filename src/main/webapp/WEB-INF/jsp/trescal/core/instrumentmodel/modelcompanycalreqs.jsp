<%-- File name: /trescal/core/instrumentmodel/modelcompanycalreqs.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>


<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="instmodcompvalreq.title" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
    	<script type="text/javascript" src="script/trescal/core/tools/CalibrationRequirements.js"></script>
        <script type='text/javascript' src='script/trescal/core/instrumentmodel/ModelCompanyCalReqs.js'></script>
        <style>
	        label {
			    width: 170px;
			}
        </style>
    </jsp:attribute>
    <jsp:body>
    
    	<form:form modelAttribute="form">
    	
        <!-- infobox contains all form elements for adding a new model component and is styled with nifty corners -->													
		<div class="infobox">
			
			<form:hidden path="pageNo"/>
			
			<fieldset>
				<legend><spring:message code="instmodcompvalreq.title" /></legend>
					<ol>
						<li>
							<label><spring:message code="company" />:</label>
							<div class="float-left extendPluginInput">
								<div class="compSearchJQPlugin">
									<input type="hidden" name="field" value="coid" />
									<input type="hidden" name="compCoroles" value="client,business" />
									<input type="hidden" name="tabIndex" value="1" />
									<input type="hidden" name="triggerMethod" value="true" />
									<input type="hidden" name="compIdPrefill" value="${form.coid}" />
									<input type="hidden" name="compNamePrefill" value="${form.comptext}" />
									<!-- company results listed here -->
								</div>
							</div>												
							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
							<form:errors path="coid" cssClass="error" />
						</li>
						<li>
							<label><spring:message code="sub-family"/>:</label>
							<form:input type="text" path="desc" tabindex="4" size="49"/>
							<form:input path="descId" type="hidden"/>
						</li>
						<li>
							<label><spring:message code="manufacturer" />:</label>
							<form:input type="text" path="mfr" tabindex="2" size="49"/>
							<form:input path="mfrId" type="hidden"/>
						</li>
						<li>
							<label><spring:message code="model" />:</label>
							<form:input type="text" path="model" tabindex="3" size="49"/>
						</li>
						<li>
							<label><spring:message code="instmodcompvalreq.onlymodelswithcalreqs" />:</label>
							<form:checkbox path="onlyWithCalReqs"/>
						</li>
						
						<li style=" height: 20px; ">
							<label>&nbsp;</label>
							<input type="reset" onclick="resetForm()" value="<spring:message code='instmodcompvalreq.clearsearch' />" />
							<input type="submit" value="<spring:message code='search' />" />
						</li>
						
					</ol>								
			
			</fieldset>						
			
		</div>
		<!-- end of infobox div -->
							
							
		<!-- infobox contains all edit instrument form elements and is styled with nifty corners -->
		<div class="infobox">
		
			<t:paginationControl resultset="${pagedResults}"/>
			<c:set var="totalCols" value="6"/>
		
			<table class="default4 viewCompanyModelCalRequirements" summary="<spring:message code="instmodcompvalreq.tablesearchresults" />">
				<thead>
					<tr>
						<td colspan="${ totalCols }"><spring:message code="instmodcompvalreq.title" />
							(<span class="instModCompCalReqSize">
								<c:choose>
									<c:when test="${not empty pagedResults }">
										<c:out value="${ pagedResults.resultsCount } "/>
									</c:when>
									<c:otherwise>0 </c:otherwise>
								</c:choose>
							</span>)
						</td>
					</tr>
					<tr>
						<th scope="col"><spring:message code="instmodelname" /></th>
						<th scope="col"><spring:message code="model"/></th>
						<th scope="col"><spring:message code="sub-family"/></th>
						<th scope="col"><spring:message code="manufacturer"/></th>
						<th scope="col"><spring:message code="instmod.modeltype" /></th>
						<th scope="col"><spring:message code="instmodcompvalreq.calrequirements" /></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="${ totalCols }">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
					<c:if test="${empty pagedResults.results}">
						<tr>
							<td colspan="${ totalCols }" class="bold center"><spring:message code="instmodcompvalreq.string2" /></td>
						</tr>
					</c:if>
					<c:forEach var="item" items="${pagedResults.results}" >
						<tr>
							<td>
								<a href="<c:url value='instrumentmodel.htm?modelid=${item.modelid}' />" target="blank" >
									<c:out value="${ item.instrumentModelName }"/>
								</a>
							</td>
							<td><c:out value="${ item.model }"/> </td>
							<td><c:out value="${ item.description }"/> </td>
							<td><c:out value="${ item.mfr }"/> </td>
							<td><c:out value="${ item.modelType }"/></td>
							<td>
								<c:choose>
									<c:when test="${empty item.crid }">
										<a href="#" class="addPoints" onclick="addCompanyCalReq(${form.coid},${item.modelid})">&nbsp;</a>
									</c:when>
									<c:otherwise>
										<a href="#" class="editPoints" onclick="modifyCompanyCalReq(${item.crid})" >&nbsp;</a>
										<a href="#" class="deletePoints" onclick="deactivateCalReq(${item.crid},${item.modelid})">&nbsp;</a>
									</c:otherwise>
								</c:choose>
							</td>
						</tr>
					</c:forEach>
				</tbody>								
			</table>					
			<t:paginationControl resultset="${pagedResults}"/>		
		
		</div>
		<!-- end of infobox div -->
		
		</form:form>
		
    </jsp:body>
</t:crocodileTemplate>