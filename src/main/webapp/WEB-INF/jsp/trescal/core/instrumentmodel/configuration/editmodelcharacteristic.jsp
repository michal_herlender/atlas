<%-- File name: /trescal/core/instrumentmodel/configuration/editmodelcharacteristic.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<%-- <instmodel:modelViewPageTitle selectedTab="${selectedTab}" /> --%>
			<spring:message code="editrange.title" />
		</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<%-- <script src='script/trescal/core/instrumentmodel/EditModelCharacteristic.js'></script>  --%>
	</jsp:attribute>
	<jsp:body>

		<instmodel:instrumentModelTable model="${model}" />
		<instmodel:instrumentModelTabs model="${model}" selectedTab="${selectedTab}" canEditInstrumentModel="${canEditInstrumentModel}" canEditSalesCategory="${canEditSalesCategory}" />

		<t:showErrors path="form.*" showFieldErrors="true" />
        <div class="infobox">
        	<form:form modelAttribute="form">
        		<form:hidden path="modelId" />
				<table class="default2" id="modeleditconfigurationcharacteristics">
					<thead>
						<tr>
							<td colspan="8"><spring:message code="characteristics" /></td>
						</tr>
						<tr>
							<th id="orderby" rowspan="2" scope="col" ><spring:message code="viewjob.orderby" /></th>
							<th id="description" rowspan="2" scope="col" ><spring:message code="description" /></th>
							<th id="type" rowspan="2" scope="col" ><spring:message code="type" /></th>
							<th id="key" rowspan="2" scope="col" ><spring:message code="key" /></th>
							<th id="include" rowspan="2" scope="col" ><spring:message code="active" /></th>
							<th id="value" colspan="3" ><spring:message code="value" /></th>
						</tr>
						<tr>
							<th id="min" scope="col" ><spring:message code="min" /></th>
							<th id="max" scope="col" ><spring:message code="max" /></th>
							<th id="unit" scope="col" ><spring:message code="unit" /></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="dto" items="${form.characteristics}" varStatus="loopStatus">
							<c:set var="charDesc" value="${charDescMap.get(dto.getCharacteristicDescriptionId())}" />
							<tr>
								<td>
									<c:out value="${charDesc.getOrderno()}" />
								</td>
								<td>
									<form:hidden path="characteristics[${loopStatus.index}].characteristicDescriptionId" />
									<c:choose>
										<c:when test="${charDesc.translations.size() > 0}">
											<cwms:besttranslation translations="${charDesc.translations}"/>
										</c:when>
										<c:otherwise>
											<c:out value="${charDesc.name}" />
										</c:otherwise>
									</c:choose>
								</td>
								<td>
									<c:out value="${charDesc.getCharacteristicType().getMessage()}" />
								</td>
								<td>
									<c:if test="${not empty charDesc.partOfKey}" >
										<spring:message code="${charDesc.partOfKey}" />
									</c:if>
								</td>
								<td>
									<form:checkbox path="characteristics[${loopStatus.index}].included" />
								</td>
									<c:choose>
										<c:when test="${charDesc.characteristicType eq 'LIBRARY'}">
											<td colspan="3">
												<form:select path="characteristics[${loopStatus.index}].characteristicLibraryId" 
													items="${charDesc.library}" itemLabel="name" itemValue="characteristiclibraryid" />
											</td>
										</c:when>
										<c:when test="${charDesc.characteristicType eq 'FREE_FIELD'}">
											<td>
												<form:input path="characteristics[${loopStatus.index}].textStart" />
											</td>
											<td>
												<form:input path="characteristics[${loopStatus.index}].textEnd" />
											</td>
										</c:when>
										<c:when test="${charDesc.characteristicType eq 'VALUE_WITH_UNIT'}">
											<td>
												<form:input path="characteristics[${loopStatus.index}].decimalStart" />
											</td>
											<td>
												<form:input path="characteristics[${loopStatus.index}].decimalEnd" />
											</td>
											<td>
												<form:select path="characteristics[${loopStatus.index}].uomId" 
													items="${units}" itemLabel="symbol" itemValue="id" />
											</td>
										</c:when>
									</c:choose>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<div>
					<input type="submit" name="save" id="submit" value="<spring:message code='update'/>" />
				</div>
            </form:form>
        </div>
    </jsp:body>
</t:crocodileTemplate>