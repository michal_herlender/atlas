<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>


<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="description.compcalreqs" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
		<script src="script/trescal/core/instrumentmodel/description/descriptioncompanycalreqs.js"></script>
	</jsp:attribute>
    <jsp:body>
        <div class="infobox">
            <fieldset>

                <legend><spring:message code="description.compcalreqs" /></legend>

                <ol>

                    <li>
                        <label><spring:message code="company" />:</label>
                        <div class="float-left extendPluginInput">
                            <div class="compSearchJQPlugin">
                                <input type="hidden" name="field" value="coid" />
                                <input type="hidden" name="compCoroles" value="client,business" />
                                <input type="hidden" name="tabIndex" value="1" />
                                <input type="hidden" name="triggerMethod" value="true" />
                                <!-- company results listed here -->
                            </div>
                        </div>
                        <!-- clear floats and restore page flow -->
                        <div class="clear"></div>
                    </li>
                    <li>
                        <label><spring:message code="sub-family"/>:</label>
						<!-- float div left -->
						<div class="float-left">
							<input id ="desc" name="desc" type="text"/>
							<input type="hidden" id="descId" />
						</div>
						<div class="clear-0"></div>	
                    </li>
                    <li>
						<label>&nbsp;</label>
						<button id="edit"><spring:message code="addedit"/></button>
					</li>	
                </ol>
            </fieldset>
        </div>
    </jsp:body>
</t:crocodileTemplate>
