<%-- File name: /trescal/core/instrumentmodel/hire/modelviewhire.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<!-- TODO: Below taglibs are not used yet but are expected to be as content built out, remove if unused -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<cwms:showmodel instrumentmodel="${command.model}"/>
		</span>
	</jsp:attribute>
	<jsp:body>
		<instmodel:instrumentModelTable model="${command.model}" />
		<instmodel:instrumentModelTabs model="${command.model}" selectedTab="${command.selectedTab}" canEditInstrumentModel="${command.canEditInstrumentModel}" canEditSalesCategory="${command.canEditSalesCategory}" />
		<div class="infobox">
			<form:form modelAttribute="hireModel">
				<fieldset>
					<ol>
						<li>
							<form:label path="hireModelDetail"><spring:message code="instmodeditmod.hiremodeldetail"/></form:label>
							<form:textarea path="hireModelDetail"/>
						</li>
						<li>
							<form:label path=""><spring:message code="instmodeditmod.hirecost"/></form:label>
							<form:input path="hireCost" pattern="([0-9]*([.][0-9][0-9])?)" style="text-align: right"/> ${hireModel.currency.currencySymbol}
						</li>
						<li>
							<form:label path="purchaseCost"><spring:message code="instmodeditmod.purchasecost"/></form:label>
							<form:input path="purchaseCost" pattern="([0-9]*([.][0-9][0-9])?)" style="text-align: right"/> ${hireModel.currency.currencySymbol}
						</li>
						<li>
							<form:button><spring:message code="submit"/></form:button>
						</li>
					</ol>
				</fieldset>
			</form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>