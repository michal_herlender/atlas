<%-- File name: /trescal/core/instrumentmodel/family/editfamily.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="editfamily.edit"/></span>
	</jsp:attribute>
	<jsp:body>

	<!-- error section displayed when form submission fails -->
		<form:errors path="editfamilyform.*">
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<h5 class="center-0 attention"><spring:message code="editfamily.error"/>:</h5>
						<c:forEach var="e" items="${messages}">
							<div class="center attention"><c:out value="${e}"/></div>
						</c:forEach>
					</div>
				</div>
			</div>
		</form:errors>
	
	<div class="infobox">   
		<form:form modelAttribute="editfamilyform" method="POST">
			<fieldset>
		   		<legend><spring:message code="editfamily.addfamily"/></legend>
		   		<ol>
			    	<cwms:translations translations="${editfamilyform.translations}"/>
			    	<form:hidden path="family.domain.domainid"/>
			    	<li>
			     		<label><spring:message code="editfamily.active"/></label>
			     		<form:checkbox path="family.active"/><br>
			     		<font color="red"><form:errors path="family.active"/></font>
			    	</li>
			    	<li>
					     <label><spring:message code="editfamily.tmlid"/>:</label>
					     <form:input path="family.tmlid" /><br>
					     <font color="red"><form:errors path="family.tmlid"/></font>
				    </li>
				    <li>
					     <form:hidden path="family.familyid"/><br>
					     <label>&nbsp;</label>
					     <input type="submit" name="save" id="save" value="Save" />
					     <c:if test="${editfamilyform.family.familyid > 0}">
					      	<input type="submit" name="delete" id="delete" value="Delete" />
					     </c:if>
				    </li>
		   		</ol>
		 	</fieldset>       
		</form:form>
	</div>
	</jsp:body>
</t:crocodileTemplate>