<%-- File name: /trescal/core/instrumentmodel/newsearchmodelresults.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<script type="text/javascript" src="script/trescal/core/instrumentmodel/SearchModelResults.js"></script>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="searchmodres.title"/></span>
	</jsp:attribute>
	<jsp:body>
		
		<div class="infobox">
			
			<form:form action="" name="searchform" id="searchform" method="post">
				<form:hidden path="model"/>
				<form:hidden path="mfrtext"/>
				<form:hidden path="desc"/>
				<form:hidden path="descId"/>
				<form:hidden path="mfrId"/>
				<form:hidden path="hireOnlyModels"/>
				<form:hidden path="pageNo"/>
				<form:hidden path="resultsPerPage"/>
				<form:hidden path="modelMfrType"/>
				<form:hidden path="salesCat"/>
				<form:hidden path="salesCatId"/>
				<form:hidden path="domainNm"/>
				<form:hidden path="domainId"/>
				<form:hidden path="familyNm"/>
				<form:hidden path="familyId"/>
				<form:hidden path="excludeQuarantined"/>
				<form:hidden path="modelTypeId"/>
				
				<t:paginationControl resultset="${command.rs}"/>
				<c:set var="totalCols" value="9"/>
																	
				<table id="modelsearchresults" class="default2" summary="<spring:message code="searchmodres.tablesearchresults"/>">
					<thead>
						<tr>
							<td colspan="${totalCols}">
								<c:choose>
									<c:when test="${command.rs.resultsCount == 1 }">
										<spring:message code="searchmodres.yourmodelsearchreturned"/><c:out value=" ${command.rs.resultsCount} " /><spring:message code="searchmodres.result"/>
									</c:when>
									<c:otherwise>
										<spring:message code="searchmodres.yourmodelsearchreturned"/>
										<c:out value=" ${command.rs.resultsCount} " />&nbsp;<spring:message code="searchmodres.results"/>
									</c:otherwise>
								</c:choose>
							</td>
						</tr>
						<tr>
							<td colspan="${totalCols}">
								<spring:message code="searchmodres.usingcriteria"/>:&nbsp;&nbsp;
								<c:forEach var="criteria" items="${command.searchedfor}">
									<span>${criteria} &nbsp;</span>
								</c:forEach>								
							</td>
						</tr>
						<tr>
							<th id="chars" scope="col"><spring:message code="instmodelname"/></th>
							<th id="domain" scope="col"><spring:message code="domain"/></th>
							<th id="family" scope="col"><spring:message code="family"/></th>
							<th id="sub-family" scope="col"><spring:message code="sub-family"/></th>
							<th id="mfr" scope="col"><spring:message code="manufacturer"/></th>
							<th id="mode" scope="col"><spring:message code="model"/></th>
							<th id="sales-category" scope="col"><spring:message code="salescategory"/></th>
							<th><spring:message code="instmod.modeltype"/></th>
							<th id="quarantined" scope="col"><spring:message code="quarantined"/></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="${totalCols}">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody id="keynavResults">
						<c:set var="rowcount" value="0"/>
						<c:choose>
							<c:when test="${command.rs.resultsCount < 1}">
								<tr class="odd">
									<td colspan="${totalCols}" class="bold center"><spring:message code="noresults"/></td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:forEach var="model" items="${command.rs.results}">
									<c:choose>
										<c:when test="${rowcount == 0 }"><c:set var="rowclass" value="hoverrow"/></c:when>
										<c:when test="${rowcount % 2 == 0 }"><c:set var="rowclass" value="even"/></c:when>
										<c:otherwise><c:set var="rowclass" value="odd"/></c:otherwise>
									</c:choose>
									<c:url var="modelUrl" value="instrumentmodel.htm?modelid=${model.modelid}" />
									<tr class="${rowclass}" onclick="window.location.href = '${modelUrl}' " onmouseover=" $j(this).addClass('hoverrow'); " onmouseout=" if (this.className != 'hoverrow') $j(this).removeClass('hoverrow'); ">
										<td>
											<a href="${modelUrl}"><cwms:showmodel instrumentmodel="${model}"/></a>
										</td>
										<td>
											<t:showTranslationOrDefault translations="${model.description.family.domain.translation}" defaultLocale="${defaultlocale}"></t:showTranslationOrDefault>
										</td>
										<td>
											<t:showTranslationOrDefault translations="${model.description.family.translation}" defaultLocale="${defaultlocale}"></t:showTranslationOrDefault>		
										</td>
										<td>
											<t:showTranslationOrDefault translations="${model.description.translations}" defaultLocale="${defaultlocale}"></t:showTranslationOrDefault>	
										</td>
										<td>
											<c:out value="${model.mfr.name}"/>
										</td>
										<td>
											<a href="<c:url value="/instrumentmodel.htm?modelid=${model.modelid}"/>" class="mainlink"> ${model.model} </a>
										</td>
										
										<td>
											<t:showTranslationOrDefault translations="${model.salesCategory.translations }" defaultLocale="${defaultlocale}"/>	
										</td>
										<td>
										    <t:showTranslationOrDefault translations="${model.modelType.modelTypeNameTranslation }" defaultLocale="${defaultlocale}"/>
										</td>
										<td>
											<c:choose>
												<c:when test="${model.quarantined == true }"><spring:message code="yes"/></c:when>
												<c:otherwise><spring:message code="nobool"/></c:otherwise>
											</c:choose>
										</td>
									</tr>
									<c:set var="rowcount" value="${rowcount + 1}"/>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
				<t:paginationControl resultset="${command.rs}"/>
								
			 </form:form>
		</div>
			
	</jsp:body>
</t:crocodileTemplate>