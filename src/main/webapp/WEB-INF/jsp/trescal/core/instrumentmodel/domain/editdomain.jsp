<%-- File name: /trescal/core/instrumentmodel/domain/editdomain.jsp --%>
<%-- @elvariable id="editdomainform" type="org.trescal.cwms.core.instrumentmodel.form.EditDomainForm" --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="editdomain.edit"/></span>
	</jsp:attribute>
	<jsp:body>
		<form:errors path="editdomainform.*">
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<h5 class="center-0 attention"><spring:message code="editdomain.error"/>:</h5>
						<c:forEach var="e" items="${messages}">
							<div class="center attention"><c:out value="${e}"/></div>
						</c:forEach>
					</div>
				</div>
			</div>
		</form:errors>
	
	<div class="infobox">
								
		<form:form modelAttribute="editdomainform" method="POST">
			<fieldset>
				<legend><spring:message code="editdomain.edit"/></legend>
				<ol>
					<cwms:translations translations="${editdomainform.translations}"/>
		
<!-- 					<li> -->
<%-- 						<label><spring:message code="editdomain.active"/></label> --%>
<%-- 						<form:checkbox path="domain.active"/><br> --%>
<%-- 						<font color="red"><form:errors path="domain.active"/></font> --%>
<!-- 					</li> -->
					<li>
						<label><spring:message code="editdomain.tmlid"/>:</label>
						<form:input path="domain.tmlid" /><br>
						<font color="red"><form:errors path="domain.tmlid"/></font>
	
					</li>
					<li>
						<form:hidden path="domain.domainid"/><br>
						<label>&nbsp;</label>
						<input type="submit" name="save" id="save" value="Save" />
						<c:if test="${editdomainform.domain.domainid > 0}">
							<input type="submit" name="delete" id="delete" value="Delete" />
						</c:if>
					</li>
				</ol>
			</fieldset>							
		</form:form>
	</div>
							
	</jsp:body>
</t:crocodileTemplate>