<%-- File name: /trescal/core/instrumentmodel/services/modelviewservices.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<instmodel:modelViewPageTitle selectedTab="${selectedTab}"/>
		</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/instrumentmodel/services/ModelViewServices.js'></script>
		<script>
			var editString = '<spring:message code="edit"/>';
		</script>
	</jsp:attribute>
	<jsp:body>
		<div id="dialog_oes" class="hid" title="">
  			<p><spring:message code="viewmod.replaceservices"/></p>
		</div>
		
		<instmodel:instrumentModelTable model="${model}" />
		<instmodel:instrumentModelTabs model="${model}" selectedTab="${selectedTab}" canEditInstrumentModel="${canEditInstrumentModel}" canEditSalesCategory="${canEditSalesCategory}" />
		
		<div class="infobox">	
			<c:set var="colspan" value="8"></c:set>
			<c:if test="${showInitializeServices}">
				<a href="" id="initServicesLink" class="mainlink" onclick="event.preventDefault(); initializeServices(${allocatedCompany.key}, ${model.modelid});">
					<spring:message code="viewmod.initializeServices"/>
				</a>
			</c:if>
			<c:if test="${model.modelType.capability}">
				<a href="" id="copyServicesLink" class="mainlink" onclick="event.preventDefault(); copyServices(${model.modelid});">
					<spring:message code="viewmod.copyServices"/>
				</a>
			</c:if>
			<a href='editmodelservices.htm?modelid=${model.modelid}' class="mainlink-float"><spring:message
					code="viewmod.editservices" /></a><br>
			
			<c:forEach var="company" items="${availableSubdivs.keySet()}">
			<table class="default2" id="modelviewservices${company.key}">
				<thead>
					<tr>
						<td colspan="${colspan}">${company.value}</td>
					</tr>
					<tr>
						<th id="subdivision" scope="col"><spring:message
									code="subdivision" /></th>
						<th id="costtype" scope="col"><spring:message code="costtype" /></th>
						<th id="servicetype" scope="col"><spring:message
									code="servicetype" /></th>
						<th id="procedure" scope="col"><spring:message
									code="capability" /></th>
						<th id="processtype" scope="col"><spring:message
									code="processtype" /></th>
						<th id="checksheet" scope="col"><spring:message
									code="checksheet" /></th>
						<th></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="${colspan}">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
					<c:forEach var="subdiv" items="${availableSubdivs.get(company)}">
						<c:forEach var="service" items="${services}">
							<c:if test="${service.organisation.subdivid == subdiv.key}">
								<tr>
									<td><c:out value="${subdiv.value}" /></td>
									<td><spring:message code="${service.costType.messageCode}" /></td>
									<td><cwms:besttranslation translations="${service.calibrationType.serviceType.longnameTranslation}"/></td>
									<td>
										<c:out value="${service.capability.reference}"/> -
										<c:out value="${service.capability.name}"/>
									</td>
									<td><c:out value="${service.calibrationProcess.process}"/></td>
									<td><c:out value="${service.checksheet }"/></td>
									<td>
										<c:if test="${subdiv.key == allocatedSubdiv.key }">
											<a href='editmodelservices.htm?id=${service.id}'><spring:message code="edit"/></a>
										</c:if>
									</td>
								</tr>	
							</c:if>
						</c:forEach>
					</c:forEach>
				</tbody>											
			</table>
			</c:forEach>
		</div>

	</jsp:body>
</t:crocodileTemplate>