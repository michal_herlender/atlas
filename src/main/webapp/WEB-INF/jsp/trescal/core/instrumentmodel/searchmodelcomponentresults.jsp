<%-- File name: /trescal/core/instrumentmodel/searchmodelcomponentresults.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="modcompres.modelcomponentresults" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/instrumentmodel/SearchModelComponentResults.js'></script>
    </jsp:attribute>
    <jsp:body>
			<!-- infobox contains all search results and is styled with nifty corners -->
				<div class="infobox">
				
					<a href="<spring:url value='/editmodelcomponent.htm'/>" class="mainlink-float">
						<spring:message code="modcompbrow.addnewcomponent"/></a>
					
					<!-- clear floats and restore pageflow -->
					<div class="clear-0"></div>
				
					<table id="componentsearchresults" class="tablesorter" summary="This table displays all model component results">
						<thead>
							<tr>
								<td colspan="6"><spring:message code="modcompres.modelcomponentresults"/> (${modelcomponents.size()})</td>
							</tr>
							<tr>										
								<th id="compbrand" scope="col"><spring:message code="manufacturer"/></th>
								<th id="compdesc" scope="col"><spring:message code="modcompres.componentdescription"/></th>
								<th id="compstock" scope="col"><spring:message code="modcompres.stockchecked"/></th>
								<th id="compcheck" scope="col"><spring:message code="modcompres.checkedby"/></th>
								<th id="complevel" scope="col"><spring:message code="modcompres.stocklevel"/></th>
								<th id="compcost" scope="col"><spring:message code="instmodeditmodcomp.salescost"/></th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="6">
									<a class="mainlink" href='<spring:url value="/editmodelcomponent.htm"/>'>
										<spring:message code="modcompbrow.addnewcomponent"/>
									</a> 
								</td>
							</tr>
						</tfoot>
						<tbody id="keynavResults">
							<c:choose> 
								<c:when test="${modelcomponents.size() < 1}">
									<tr>
										<td colspan="6" class="bold center">
											<spring:message code="noresults"/>
										</td>
									</tr>
								</c:when>
								<c:otherwise>		
									<c:forEach var="model" items="${modelcomponents}">
										<tr onclick="window.location.href='<spring:url value="/editmodelcomponent.htm?cid=${model.id}"/>'/>" onmouseover=" $j(this).addClass('hoverrow'); " onmouseout=" if (this.className != 'hoverrow') $j(this).removeClass('hoverrow'); " >
											<td>${model.mfr.name}</td>
											<td>${model.description.description}</td>
											<td><fmt:formatDate value="${model.stockLastCheckedOn}" type="date" dateStyle="SHORT"/></td>
											<td>${model.stockLastCheckedBy.name}</td>
											<td>${model.inStock}</td>
											<td class="right">&pound;${model.salesCost}</td>
										</tr>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
					
				</div>
			<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>