<%-- File name: /trescal/core/instrumentmodel/editmodelsalescategory.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<spring:message code="viewmod.vieweditsalescategory"/>
		</span>
	</jsp:attribute>
	<jsp:body>
	
	<t:showErrors path="form.*" showFieldErrors="true" />
	
	<!-- display clearly the unit is quarantined -->
	<c:if test="${quarantined}">
		<div class="warningBox1">
			<div class="warningBox2">
				<div class="warningBox3">
					<h5 class="center-0 attention">
						<spring:message code="instmodeditmod.quarantinedwarning"/>
					</h5>
				</div>
			</div>
		</div>
	</c:if>
							
	<form:form method="post" modelAttribute="form">
	<form:hidden path="modelid" />
		
	<!-- infobox contains all form elements for adding a new instrument model and is styled with nifty corners -->
	<div class="infobox" id="editmodelsalescategory">
		<fieldset>
			<legend>
				<spring:message code="instmodeditmod.coremodeldefinition"/>
				(<spring:message code="instmodeditmod.backto"/>
				 <t:showViewModelLink instrumentmodel="${instModel}"/>)
			</legend>
			<ol>
				<li id="mfrarea">
					<label>
						<spring:message code="manufacturer"/>
						<c:out value=" * : "/>
					</label>
					<c:out value="${mfrName}" />
				</li>
				<li id="model">
					<label>
						<spring:message code="instmodeditmod.modelname"/>
						<c:out value=" : "/>						
					</label>
					<c:out value="${modelName}" />
				</li>
				<li id="descrip">
					<label>
						<spring:message code="sub-family"/>
						<c:out value=" * : "/>
					</label>
					<!-- float div left -->
					<div class="float-left">
						<c:out value="${subFamilyName}" />
					</div>
					<!-- clear floats and restore pageflow -->
					<div class="clear-0"></div>
				</li>
				<li id="salescat">
					<label>
						<spring:message code="salescategory"/>
						<c:out value=" : "/>
					</label>
					<!-- float div left -->
					<div class="float-left">
						<form:select path="salesCategoryId" itemLabel="value" itemValue="key" items="${salesCategories}" />
					</div>
					<form:errors path="salesCategoryId" cssClass="attention-pad" />
					<!-- clear floats and restore pageflow -->
					<div class="clear-0"></div>
				</li>
				<li>
					<label>
						<spring:message code="quarantined"/>
						<c:out value=" * : "/>
					</label>
					<c:choose>
						<c:when test="${quarantined}">
							<spring:message code="yes"/>
						</c:when> 
						<c:otherwise>
							<spring:message code="no"/>
						</c:otherwise>
					</c:choose>
						 
				</li>
				<li>
					<label>
						<spring:message code="instmod.modeltype"/>
						<c:out value=" * : "/>
					</label>
                    <span>
                   	  <c:out value="${modelType}" />
                    </span>
				</li>
				<li>
					<input name="submit" id="submit" type="submit" value="<spring:message code="submit"/>" />
				</li>
			</ol>
		
		</fieldset>
									
	</div>
	<!-- end of infobox div -->
	</form:form>
	
	</jsp:body>
</t:crocodileTemplate>