<%-- File name: /trescal/core/instrumentmodel/dimensional/threadsearch.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
		<span class="headtext"><spring:message code="threads.searchthreads" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/instrumentmodel/dimensional/ThreadSearch.js'></script>
    </jsp:attribute>
    <jsp:body>
			<!-- infobox contains all description form elements and is styled with nifty corners -->
	
			<!-- infobox div containing all thread search form elements -->					
			<div class="infobox">
				<!-- add new thread link -->
				<a class="mainlink-float" href="<spring:url value='/thread.htm'/>">
					<spring:message code="threadsearch.addnewthread" /></a>
				<!-- clear floats and restore page flow -->
				<div class="clear"></div>
			
				<form:form method="post" action="" >
			
					<fieldset>
						
						<legend><spring:message code="threads.searchthreads" /></legend>
						
						<ol>
							<li>
								<label><spring:message code="thread.size" />:</label>
								<form:input path="size" tabindex="" />
								<form:errors path="size" class="attention-pad" />
							</li>
							<li>
								<label><spring:message code="thread.type" />:</label>
								<form:select path="typeId" id="typeid" onchange="showHideEditLink('type', this.value); return false;">
									<option value="" selected="selected"><spring:message code="thread.select" /></option>
									<c:forEach var="type" items="${threadtypes}">
										<option value="${type.id}">${type.type}</option>
									</c:forEach>
								</form:select>
								
								<span id="edittype" class="hid">
									<c:set var="editUrlId" value="$j('#typeid').val()" />
									<a href="#" onclick=" event.preventDefault(); window.location.href='threadtype.htm?id='+${editUrlId}; " class="imagelink">
										<img src="img/icons/form_edit.png" width="16" height="16" alt="<spring:message code='threadsearch.editthreadtype' />" title="<spring:message code='threadsearch.editthreadtype' />" />										
									</a>
								</span> 
								<a href="<spring:url value='/threadtype.htm'/>" class="imagelink">
									<img src="img/icons/add.png" width="16" height="16" alt="<spring:message code='threadsearch.addthreadtype' />" title="<spring:message code='threadsearch.addthreadtype' />" />
								</a>
								<form:errors path="typeId" class="attention-pad" />
							</li>
							<li>
								<label><spring:message code="thread.wire" />:</label>
								<form:select path="wireId" id="wireid" onchange="showHideEditLink('wire', this.value); return false;">
									<option value="" selected="selected"><spring:message code="thread.select" /></option>
									<c:forEach var="wire" items="${wires}">
										<option value="${wire.id}">${wire.designation}</option>
									</c:forEach>
								</form:select>
								
								<span id="editwire" class="hid">
									<c:set var="editUrlId" value="$j('#wireid').val()" />
									<a href="#" onclick=" event.preventDefault(); window.location.href='wire.htm?id='+${editUrlId}; " class="imagelink">
										<img src="img/icons/form_edit.png" width="16" height="16" alt="<spring:message code='threadsearch.editwire' />" title="<spring:message code='threadsearch.editwire' />" />																
									</a>
								</span> 
								<a href="<spring:url value='/wire.htm'/>" class="imagelink">
									<img src="img/icons/add.png" width="16" height="16" alt="<spring:message code='threadsearch.addwire' />" title="<spring:message code='threadsearch.addwire' />" />
								</a>
								<form:errors path="wireId" class="attention-pad" />
							</li>
							<li>
								<label><spring:message code="cylindricalstandard.cylindricalstandard" />:</label>
								<form:select path="cylindricalStandardId" id="csid" onchange="showHideEditLink('cs', this.value); return false;">
									<option value="" selected="selected"><spring:message code="thread.select" /></option>
									<c:forEach var="cs" items="${cylindricalstandards}">
										<option value="${cs.id}">${cs.id}</option>
									</c:forEach>
								</form:select>
								
								<span id="editcs" class="hid">
									<c:set var="editUrlId" value="$j('#csid').val()" />
									<a href="#" onclick=" event.preventDefault(); window.location.href='cylindricalstandard.htm?id='+${editUrlId}; " class="imagelink">
										<img src="img/icons/form_edit.png" width="16" height="16" alt="<spring:message code='threadsearch.editcylindricalstandard' />" title="<spring:message code='threadsearch.editcylindricalstandard' />" />															
									</a>
								</span> 
								<a href="<spring:url value="/cylindricalstandard.htm"/>" class="imagelink">
									<img src="img/icons/add.png" width="16" height="16" alt="<spring:message code='threadsearch.addcylindricalstandard' />" title="<spring:message code='threadsearch.addcylindricalstandard' />" />
								</a>
								<form:errors path="cylindricalStandardId" class="attention-pad" />
							</li>
							<li>
								<label><spring:message code="thread.threaduom" />:</label>
								<form:select path="uomId" id="threaduomid" onchange="showHideEditLink('threaduom', this.value); return false;">
									<option value="" selected="selected"><spring:message code="thread.select" /></option>
									<c:forEach var="uom" items="${threaduoms}">
										<option value="${uom.id}">${uom.description}</option>
									</c:forEach>
								</form:select>
								
								<span id="editthreaduom" class="hid">
									<c:set var="editUrlId" value="$j('#threaduomid').val()" />
									<a href="#" onclick=" event.preventDefault(); window.location.href='threaduom.htm?id='+${editUrlId}; " class="imagelink">
										<img src="img/icons/form_edit.png" width="16" height="16" alt="<spring:message code='threadsearch.editthreaduom' />" title="<spring:message code='threadsearch.editthreaduom' />" />
									</a>
								</span> 
								<a href="<spring:url value='/threaduom.htm'/>" class="imagelink">
									<img src="img/icons/add.png" width="16" height="16" alt="<spring:message code='threadsearch.addthreaduom' />" title="<spring:message code='threadsearch.addthreaduom' />" />
								</a>
								<form:errors path="uomId" class="attention-pad" />
							</li>
							
							<li>
								<label>&nbsp;</label>
								<input type="submit" name="submit" id="submit" value="<spring:message code='search' />" />
							</li>
						</ol>
					
					</fieldset>
				
				</form:form>
			
			</div>
			<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>