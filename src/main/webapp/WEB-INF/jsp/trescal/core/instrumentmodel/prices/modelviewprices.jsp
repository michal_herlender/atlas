<%-- File name: /trescal/core/instrumentmodel/prices/modelviewprices.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<instmodel:modelViewPageTitle selectedTab="${selectedTab}" />
		</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/instrumentmodel/prices/ModelViewPrices.js'></script>
		<script>
			var editPrice = '<spring:message code="viewmod.editprice"/>';
		</script>
	</jsp:attribute>
	<jsp:body>
		<instmodel:instrumentModelTable model="${model}" />
		<instmodel:instrumentModelTabs model="${model}" selectedTab="${selectedTab}" canEditInstrumentModel="${canEditInstrumentModel}" canEditSalesCategory="${canEditSalesCategory}" />
		<div class="infobox">	
			<c:set var="colspan" value="7"></c:set>
			<a href='editmodelprice.htm?modelid=${model.modelid}'
				class="mainlink-float"><spring:message
					code="viewmod.createprice" /></a><br>
			<c:forEach var="coid" items="${prices.keySet()}">
			<table class="default2" id="modelviewprices${coid}">
				<thead>
					<tr>
						<td colspan="${colspan}">${prices.get(coid).toArray()[0].catalogPrice.coname}</td>
					</tr>
					<tr>
						<th id="costtype" scope="col"><spring:message code="costtype" /></th>
						<th id="servicetype" scope="col"><spring:message
									code="servicetype" /></th>
						<th id="fixedprice" scope="col"><spring:message
									code="fixedprice" /></th>
						<th id="variableprice" scope="col"><spring:message
									code="variableprice" /></th>
						<th id="per" scope="col"><spring:message code="per" /></th>
						<th id="comments" scope="col"><spring:message code="comments" /></th>
                        <th></th>
					</tr>
				</thead>
				<tbody>
				<c:forEach var="price" items="${prices.get(coid)}">
					<tr>
						<td><spring:message code="${price.catalogPrice.costType}" /></td>
						<td>${price.catalogPrice.serviceType}</td>
						<td>
							${price.currencyPrice}&nbsp;${price.currency.currencySymbol}
							<c:if test="${price.catalogPrice.isSalesCategory and !model.modelType.salescategory}">
								<img src="img/icons/salescategory.png" width="30" height="30" alt="" title="" style="float: right;"/>
							</c:if>
						</td>
						<td>${price.currencyUnitPrice}</td>
						<td>${price.catalogPrice.variablePriceUnit}</td>
						<td>${price.catalogPrice.comments}</td>
                        <td>
                        	<c:if test="${coid == allocatedCompany.key}">
                        		<c:choose>
                        			<c:when test="${!model.modelType.salescategory}">
                        				<c:if test="${!price.catalogPrice.isSalesCategory}">
                        					<a href="editmodelprice.htm?id=${price.catalogPrice.id}">
                        						 <spring:message code="viewmod.editprice" />
                        					</a>
                        				</c:if>
                        			</c:when>
                        			<c:otherwise>
                        				<a href="editmodelprice.htm?id=${price.catalogPrice.id}">
                        					 <spring:message code="viewmod.editprice" />
                        				</a>
                        			</c:otherwise>
                        		</c:choose>
							</c:if>
						</td>
					</tr>
				</c:forEach>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="${colspan}">&nbsp;</td>
					</tr>
				</tfoot>
			</table>
			</c:forEach>
		</div>
	</jsp:body>
</t:crocodileTemplate>