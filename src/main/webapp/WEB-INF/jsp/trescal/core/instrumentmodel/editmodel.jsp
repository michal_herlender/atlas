<%-- File name: /trescal/core/instrumentmodel/editmodel.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
		<c:choose>
			<c:when test="${command.modelid == 0}">
				<spring:message code="add"/>
			</c:when>
			<c:otherwise>
				<spring:message code="edit"/>
			</c:otherwise>
		</c:choose>
		<c:out value=" " /><spring:message code="instmodeditdesc.instmod"/>
		</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/instrumentmodel/EditModel.js'></script>
		<script type='text/javascript' src='script/trescal/core/utilities/NewManufacturer.js'></script>
	</jsp:attribute>
	<jsp:body>
	
	<t:showErrors path="command.*"/>
	
	<!-- display clearly the unit is quarantined -->
		<form:errors path="command.quarantined">
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<h5 class="center-0 attention">
							<spring:message code="instmodeditmod.quarantinedwarning"/>
						</h5>
					</div>
				</div>
			</div>
		</form:errors>
							
	<form:form method="post" action="" id="editmodelform">
	<form:hidden path="modelid" />
		
	<!-- infobox contains all form elements for adding a new instrument model and is styled with nifty corners -->
	<div class="infobox" id="editmodel">
		<fieldset>
			<legend>
				<spring:message code="instmodeditmod.coremodeldefinition"/>
				<c:if test="${not empty model}">
					(<spring:message code="instmodeditmod.backto"/>
					 <t:showViewModelLink instrumentmodel="${model}"></t:showViewModelLink>)
				</c:if>
			</legend>
			<ol>
				<li>
					<%-- MFR_SPECIFIC MFR_GENERIC --%>
						<c:forEach var="type" items="${modelmfrtypes}">
							<c:set var="classname" value="hid"/>
							<c:if test="${type == 'MFR_SPECIFIC'}">
								<c:set var="classname" value="vis"/>
							</c:if>
							<c:set var="checkedText" value=""/>
							<c:if test="${type == command.modelMfrType}">
								<c:set var="checkedText" value="checked=\"checked\""/>
							</c:if>
							<form:radiobutton path="modelMfrType"
												value="${type}"
												onclick="$j('#mfrarea').removeClass().addClass('${classname}');"/>
							<c:choose>
								<c:when test="${type == 'MFR_SPECIFIC'}">
									<spring:message code="instmodeditmod.mfrspecificmodel"/>
								</c:when>
								<c:otherwise>
									<spring:message code="instmodeditmod.nonmfrspecificmodel"/>
								</c:otherwise>
							</c:choose>
						</c:forEach>
				</li>
				<%-- 
					If the modelMfrType states that this model does not have a
					mfr type then hide the mfr area.
				--%>									
				<c:set var="mfrAreaVis" value="vis"/>
				<c:if test="${command.modelMfrType == 'MFR_GENERIC'}">
					<c:set var="mfrAreaVis" value="hid"/>
				</c:if>
				<li class="${mfrAreaVis}" id="mfrarea">
					<label>
						<spring:message code="manufacturer"/>
						<c:out value=" * : "/>
					</label>
						<!-- float div left -->
						<div class="float-left">
							<!-- this div creates a new manufacturer jquery search plugin -->	
							<div class="mfrSearchJQPlugin">
								<input type="hidden" name="fieldId" value="mfrid" />
								<input type="hidden" name="tabIndex" value="1" />		
								<c:if test="${command.modelMfrType == 'MFR_SPECIFIC'}">
									<input type="hidden" name="mfrNamePrefill" value="${command.mfrname}" />
									<input type="hidden" name="mfrIdPrefill" value="${command.mfrid}" />
								</c:if>																									
								<!-- manufacturer results appear here -->
							</div>
						</div>
						<div class="float-left">
							<cwms:securedJsLink permission="MANUFACTURER_EDIT" 
								classAttr="domthickbox padtop mainlink-float"
								onClick=" tb_show('New Manufacturer', 'TB_dom?width=640&height=360', '', createManufacturerContent(true)); ">  
									<spring:message code="instmodeditmod.newmanufacturer" />
							</cwms:securedJsLink>
						</div>
					<form:errors path="modelMfrType" class="attention-pad" />
						<div class="clear"></div>
					<!-- clear floats and restore pageflow -->
					<div class="clear-0"></div>
				</li>
				<li id="model">
					<label>
						<spring:message code="instmodeditmod.modelname"/>
						<c:out value=" : "/>						
					</label>
						<form:input path="modelName" id="modelname" type="text" tabindex="2" />
						<form:errors path="modelName" class="attention-pad" />
				</li>
				<li id="descrip">
					<label>
						<spring:message code="sub-family"/>
						<c:out value=" * : "/>
					</label>
					<!-- float div left -->
					<div class="float-left">
						<form:input path="descname" tabindex="3" />
						<form:hidden path="descid" />
						<form:errors path="descid" cssClass="attention-pad" />
					</div>
					<!-- clear floats and restore pageflow -->
					<div class="clear-0"></div>
				</li>
				<li id="salescat">
					<label>
						<spring:message code="salescategory"/>
						<c:out value=" : "/>
					</label>
					<!-- float div left -->
					<div class="float-left">
						<c:choose>
							<c:when test="${isSalesCategoryModelType}">
								Sales category definition will be automatically updated
							</c:when>
							<c:otherwise>
								<form:input path="salesCategoryText" tabindex="4" />
								<form:input path="salesCategoryId" type="hidden" />
								<form:errors path="salesCategoryId" cssClass="attention-pad" />
							</c:otherwise>
						</c:choose>
					</div>
					<!-- clear floats and restore pageflow -->
					<div class="clear-0"></div>
				</li>
				<li>
					<label>
						<spring:message code="quarantined"/>
						<c:out value=" * : "/>
					</label>
					<form:checkbox path="quarantined" />
					<form:errors path="quarantined" cssClass="attention-pad" />
				</li>
				<li>
					<label>
						<spring:message code="instmod.modeltype"/>
						<c:out value=" * : "/>
					</label>
					<form:select path="instModelTypeId" items="${modelTypes}" itemLabel="value" itemValue="key" />
					<form:errors path="instModelTypeId" cssClass="attention-pad" />
				</li>
				<c:if test="${not empty listDuplicates}">
					<%-- Only appears for duplicate confirmation after first submission of new model, or when duplicates exist on edit model --%>
					<li>
						<label>
							<spring:message code="instmod.duplicates"/>
							<c:out value=" * : "/>
						</label>
						<div class="float-left">
							<div>
								<form:checkbox path="confirmDuplicate" />
								<spring:message code="instmod.duplicatestext" arguments="${listDuplicates.size()}" />
								<form:errors path="confirmDuplicate" cssClass="attention-pad" />
							</div>
							<c:forEach var="duplicateModel" items="${listDuplicates}" varStatus="loopStatus">
								<div>
									<a href="instrumentmodel.htm?modelid=${duplicateModel.modelid}" class="mainlink">
										<cwms:showmodel instrumentmodel="${duplicateModel}"/>
									</a>
								</div>
							</c:forEach>
						</div>
					</li>
				</c:if>
				<li>
					<input name="submit" id="submit" type="submit" value="<spring:message code="submit"/>" />
				</li>
			</ol>
		
		</fieldset>
									
	</div>
	<!-- end of infobox div -->
	</form:form>
	
	</jsp:body>
</t:crocodileTemplate>