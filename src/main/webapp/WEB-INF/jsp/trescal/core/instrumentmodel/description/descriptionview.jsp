<%-- File name: /trescal/core/_PATH1_/_PATH2_/_FILE_.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext">${designationdefault}</span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
    </jsp:attribute>
    <jsp:body>
        							
<!-- infobox contains all description form elements and is styled with nifty corners -->
<div class="infobox">
	<fieldset>
		<legend>${designationdefault}</legend>
		
		<ol>
			<li>
				<label><spring:message code="descview.descriptionusedfor"/>:</label>
				<a href="<spring:url value='/searchmodel.htm?forced=forced&descid=${description.id}'/>" class="mainlink">${modelscount}</a>
				<c:choose> 
					<c:when test="${modelscount == 1}">
						<spring:message code="model"/>
					</c:when>
					<c:otherwise>	
						<spring:message code="models"/>
					</c:otherwise>
				</c:choose>
			</li>
			<li>
				<label><spring:message code="instmod.lastmodified"/>:</label>
				<span>
					<c:if test="${description.lastModifiedBy != null}">
						${description.lastModifiedBy.name}
					</c:if>
					<c:if test="${description.lastModified != null}">
                    	(<fmt:formatDate value="${description.lastModified}" type="BOTH" dateStyle="SHORT" timeStyle="SHORT"/>)
					</c:if>
				</span>
			</li>
			<li>
				<label>
				<spring:message code="description.family"/>:</label>
				<c:choose>
					<c:when test="${description.family != null}">
						<span><t:showTranslationOrDefault translations="${description.family.translation}" defaultLocale="${locale}"/></span>
<%-- 						<t:showTranslation translations="${description.family.translation}" desiredLocale="${locale}"/> --%>
					</c:when>
					<c:otherwise>
                   	  <span> &nbsp </span>
					</c:otherwise>
				</c:choose>
			</li>
		</ol>
	</fieldset>
</div>
<!-- end of infobox div -->

<!-- begin infobox div with nifty corners applied -->
<div class="infobox">
	
	<!-- this section displays translations -->
	<div id="costs-tab">
		
		<table class="default2" id="descriptiontranslations" summary="<spring:message code='descview.tabletranslations'/>">
			<thead>
				<tr>
					<td colspan="2"><spring:message code="descview.translations"/></td>
				</tr>
				<tr>
					<th id="language" scope="col"><spring:message code="descview.language"/></th>
					<th id="description" scope="col"><spring:message code="description"/></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
			</tfoot>
			<tbody>
				<c:forEach var="loc" items="${supportedLocales}">
					<tr>
						<td>${loc.getDisplayLanguage(rc.locale)} (${loc.getDisplayCountry(rc.locale)})</td>
						<td><c:forEach var="trans" items="${translation}">
								<c:if test="${trans.locale.equals(loc)}">
									${trans.translation}
<!-- 									#break  -->
 								</c:if> 
						</c:forEach></td>
					</tr>
				</c:forEach>
			</tbody>
        </table>
    </div>
	<!-- end of translations-->
	
</div>
<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>