<%-- File name: /trescal/core/instrumentmodel/dimensional/threadresults.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
		<span class="headtext"><spring:message code="threadresult.threadresults"/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
       <script type='text/javascript' src='script/trescal/core/instrumentmodel/dimensional/ThreadResults.js'></script>
    </jsp:attribute>
    <jsp:body>
 				<!-- infobox contains all search results and is styled with nifty corners -->
			<div class="infobox">
				
				<form:form action="" name="searchform" id="searchform" method="post" >
					
					<form:hidden path="size" />
					<form:hidden path="wireId" />
					<form:hidden path="typeId" />
					<form:hidden path="cylindricalStandardId" />
					<form:hidden path="uomId" />
					
					<form:hidden path="pageNo" />
					<form:hidden path="resultsPerPage" />
					
					<t:paginationControl resultset="${command.rs}"/>
					
				</form:form>
										
				<table id="threadsearchresults" class="tablesorter" summary="<spring:message code='threadresult.tablesearchresults'/>">
					
					<thead>
						<tr>
							<td colspan="4">
								<spring:message code="threadresult.threadresults"/>
							</td>
						</tr>
						<tr>
							<th class="size" scope="col"><spring:message code="thread.size"/></th>  
							<th class="type" scope="col"><spring:message code="thread.type"/></th>
							<th class="wire" scope="col"><spring:message code="thread.wire"/></th>
							<th class="cylstand" scope="col"><spring:message code="cylindricalstandard.cylindricalstandard"/></th>
						</tr>
					</thead>
					
					<tfoot>
						<tr>
							<td colspan="4">&nbsp;</td>
						</tr>
					</tfoot>
					
					<tbody id="keynavResults">										
						<c:choose>
							<c:when test="${command.threads.size() < 1}">
								<tr>
									<td colspan="4" class="bold center">
										<spring:message code="noresults"/>
									</td>
								</tr>
							</c:when>
							<c:otherwise>											
							<c:forEach var="thread" items="${command.threads}">
								<tr onclick="window.location.href='<spring:url value="/thread.htm?id=${thread.id}"/>' " onmouseover=" $j(this).addClass('hoverrow'); " onmouseout=" if (this.className != 'hoverrow') $j(this).removeClass('hoverrow'); " >
									<td>
										<!-- this anchor is included to stop key navigation breaking -->
										<!-- key navigation uses an anchor to apply focus to row -->
										<a href="#"></a>
										${thread.size}
									</td>
									<td>${thread.type.type}</td>
									<td>${thread.wire.designation}</td>
									<td>${thread.cylindricalStandard.id}</td>
								</tr>												
							</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
					
				</table>
				
			</div>
			<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>