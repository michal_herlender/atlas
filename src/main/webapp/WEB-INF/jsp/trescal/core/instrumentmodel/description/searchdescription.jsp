<%-- File name: /trescal/core/instrumentmodel/description/searchdescription.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="description.searchdescription"/></span>
    </jsp:attribute>
    <jsp:body>
  <!-- infobox div containing all description form elements -->					
  <div class="infobox" id="searchdescription">
	<!-- clear floats and restore page flow -->
	<div class="clear"></div>

	<form:form method="post" action="" id="searchdescriptionform" modelAttribute="command">

		<fieldset id="descriptionbrowser">
			
			<legend><spring:message code="description.descriptionbrowser"/></legend>
			
			<ol>
				<li>
					<label><spring:message code="description.description"/>:</label>
					<form:input path="description" tabindex="" />
					<form:errors path="description" class="attention-pad" />
				</li>
				<li>
					<label><spring:message code="description.searchlanguage"/>:</label>
					<form:select path="searchLanguage">
						<c:forEach var="supportedLocale" items="${supportedLocales}">
							<option value="${supportedLocale}" 
							<c:if test="${supportedLocale == command.searchLanguage}"> selected="selected" </c:if>>
							${supportedLocale.getDisplayLanguage(rc.locale)}
							(${supportedLocale.getDisplayCountry(rc.locale)})
							</option>
						</c:forEach>
					</form:select>
					<form:errors path="searchLanguage" class="attention-pad" />
				</li>
				<li>
					<label><spring:message code="description.displaylanguage"/>:</label>
					<form:select path="displayLanguage">
						<c:forEach var="supportedLocale" items="${supportedLocales}">
							<option value="${supportedLocale}" 
							<c:if test="${supportedLocale == command.displayLanguage}"> selected="selected" </c:if>>
							${supportedLocale.getDisplayLanguage(rc.locale)}
							(${supportedLocale.getDisplayCountry(rc.locale)})
							</option>
						</c:forEach>
					</form:select>
					<form:errors path="displayLanguage" class="attention-pad" />
				</li>
				
				<li>
					<label>&nbsp;</label>
					<input type="submit" name="submit" id="submit" value="<spring:message code='search'/>" />
				</li>
			</ol>
		
		</fieldset>
	
	</form:form>

</div>
<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>