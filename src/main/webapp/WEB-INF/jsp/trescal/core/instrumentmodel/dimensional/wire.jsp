<%-- File name: /trescal/core/instrumentmodel/dimensional/wire.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
		<span class="headtext"><spring:message code="threadwire.addeditwire" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/instrumentmodel/dimensional/Wire.js'></script>
    </jsp:attribute>
    <jsp:body>
				<!-- infobox contains all thread wire form elements and is styled with nifty corners -->
				<div class="infobox">
					
					<!-- link to thread search -->
					<a href="<spring:url value='/threadsearch.htm'/>" class="mainlink-float"><spring:message code="threads.searchthreads" /></a>
					<!-- clear floats and restore page flow -->
					<div class="clear"></div>
				
					<form:form method="post" action="" >
				
						<form:errors path="*">
                     		<div class="warningBox1">
                        		<div class="warningBox2">
                            		<div class="warningBox3">
                                		<h5 class="center-0 attention">
                                    		<spring:message code='cylindricalstandard.errorsaved' />
                                		</h5>
                                		<c:forEach var="e" items="${messages}">
                                    		<div class="center attention"><c:out value="${e}"/></div>
                                		</c:forEach>
                            		</div>
                        		</div>
                     		</div>
                    	</form:errors>
						<fieldset>
							
							<legend><spring:message code="threadwire.addeditwire" /></legend>
							
							<ol>
								<li>
									<label><spring:message code="threads.designation" />:</label>
									<form:input path="designation" tabindex="" />
									<form:errors path="designation" class="attention-pad" />
								</li>
								<li>
									<label><spring:message code="threadwire.metricpitch" />:</label>
									<form:input path="metricPitch" tabindex="" />
									<form:errors path="metricPitch" class="attention-pad" />
								</li>
								<li>
									<label><spring:message code="threadwire.unifiedtpi" />:</label>
									<form:input path="unifiedTPI" tabindex="" />
									<form:errors path="unifiedTPI" class="attention-pad" />
								</li>
								<li>
									<label><spring:message code="threadwire.whitworthtpi" />:</label>
									<form:input path="whitworthTPI" tabindex="" />
									<form:errors path="whitworthTPI" class="attention-pad" />
								</li>
								<li>
									<label><spring:message code="threadwire.ba" />:</label>
									<form:input path="ba" tabindex="" />
									<form:errors path="ba" class="attention-pad" />
								</li>
								<li>
									<label><spring:message code="threadwire.bapitch" />:</label>
									<form:input path="baPitch" tabindex="" />
									<form:errors path="baPitch" class="attention-pad" />
								</li>
								<li>
									<label><spring:message code="threadwire.ballno" />:</label>
									<form:input path="ballNo" tabindex="" />
									<form:errors path="ballNo" class="attention-pad" />
								</li>
								<li>
									<label><spring:message code="threadwire.rubyball" />:</label>
									<form:input path="rubyBall" tabindex="" />
									<form:errors path="rubyBall" class="attention-pad" />
								</li>
								<li>
									<label><spring:message code="threadwire.wiremean" />:</label>
									<form:input path="wireMean" tabindex="" />
									<form:errors path="wireMean" class="attention-pad" />
								</li>
								
								<li>
									<label><spring:message code="threadwire.eowidth" />:</label>
									<form:input path="eoWidth" tabindex="" />
									<form:errors path="eoWidth" class="attention-pad" />
								</li>
								<li>
									<label><spring:message code="threadwire.eounified" />:</label>
									<form:input path="eoUnified" tabindex="" />
									<form:errors path="eoUnified" class="attention-pad" />
								</li>
								<li>
									<label><spring:message code="threadwire.eoiso" />:</label>
									<form:input path="eoISO" tabindex="" />
									<form:errors path="eoISO" class="attention-pad" />
								</li>
								<li>
									<label><spring:message code="thread.wire" /> 1:</label>
									<form:input path="wire1" tabindex="" />
									<form:errors path="wire1" class="attention-pad" />
								</li>
								<li>
									<label><spring:message code="thread.wire" /> 2:</label>
									<form:input path="wire2" tabindex="" />
									<form:errors path="wire2" class="attention-pad" />
								</li>
								
								<li>
									<label>&nbsp;</label>
									<input type="submit" name="submit" id="submit" value="<spring:message code='submit' />" />
								</li>
							</ol>									
						
						</fieldset>
					
					</form:form>
				
				</div>
				<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>