<%-- File name: /trescal/core/instrumentmodel/description/searchdescriptionresults.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
        <span class="headtext"><spring:message
				code="description.descriptionresults" /></span>
    </jsp:attribute>
	<jsp:attribute name="scriptPart">
        <script type='text/javascript'>
									/**
									 * array of objects containing the element name and corner type which should be applied.
									 * applied in init() function.
									 */
									var niftyElements = new Array({
										element : 'div.infobox',
										corner : 'normal'
									});
								</script>
    </jsp:attribute>
	<jsp:body>
        <!-- infobox contains all search results and is styled with nifty corners -->
<div class="infobox">

	<form:form action="" name="searchform" id="searchform" method="post"
				modelAttribute="command">

		<form:hidden path="description" />
		<form:hidden path="searchLanguage" />
		<form:hidden path="displayLanguage" />

		<form:hidden path="pageNo" />
		<form:hidden path="resultsPerPage" />

		<t:showResultsPagination rs="${results}" pageNoId="" />

	<table id="descriptionsearchresults" class="tablesorter"
					summary="<spring:message code='description.descriptionresults'/>">

		<thead>
			<tr>
				<td colspan="3">
					<spring:message code="description.descriptionresults" />
				</td>
			</tr>
			<tr>
				<th class="size" scope="col">${searchLocale.getDisplayName(rc.locale)}</th>
				<th class="type" scope="col">${displayLocale.getDisplayName(rc.locale)}</th>
				<th class="type" scope="col"><spring:message
									code="description.family" /></th>
			</tr>
		</thead>

		<tfoot>
			<tr>
				<td colspan="4">&nbsp;</td>
			</tr>
		</tfoot>

		<tbody id="keynavResults">
			<c:set var="rowcount" value="0" />
			<c:choose>
				<c:when test="${empty results.resultsCount}">
					<tr>
						<td colspan="3" class="bold center">
							<spring:message code="noresults" />
						</td>
					</tr>
				</c:when>
			<c:otherwise>
				<c:forEach var="description" items="${results.results}">
					<c:choose>
						<c:when test="${rowcount == 0 }">
											<c:set var="rowclass" value="hoverrow" />
										</c:when>
						<c:when test="${rowcount % 2 == 0 }">
											<c:set var="rowclass" value="even" />
										</c:when>
						<c:otherwise>
											<c:set var="rowclass" value="odd" />
										</c:otherwise>
					</c:choose>
					<tr class="${rowclass}"
										onclick="window.location.href='<spring:url value="/descriptionview.htm?id=${description.id}"/>'"
										onmouseover=" $j(this).addClass('hoverrow'); "
										onmouseout=" if (this.className != 'hoverrow') $j(this).removeClass('hoverrow'); ">
						<td>
							<!-- this anchor is included to stop key navigation breaking -->
							<!-- key navigation uses an anchor to apply focus to row -->
							<a href=""></a>
							
							<t:showTranslationOrDefault
												translations="${description.translations}"
												specificLocale="${searchLocale}"
												defaultLocale="${defaultLocale}" looseMatching="false" />
						</td>
						<td><t:showTranslationOrDefault
												translations="${description.translations}"
												specificLocale="${displayLocale}"
												defaultLocale="${defaultLocale}" looseMatching="false" /></td>
						<td><t:showTranslationOrDefault
												translations="${description.family.translation}"
												specificLocale="${searchLocale}"
												defaultLocale="${defaultLocale}" /></td>
					</tr>
					<c:set var="rowcount" value="${rowcount + 1}" />
				</c:forEach>
				</c:otherwise>
			</c:choose>
		</tbody>

	</table>

	<t:showResultsPagination rs="${command.rs}" pageNoId="" />

	</form:form>

</div>
<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>