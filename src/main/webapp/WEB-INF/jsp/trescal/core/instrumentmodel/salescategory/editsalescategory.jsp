<%-- File name: /trescal/core/instrumentmodel/salescategory/editsalescategory.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="editsalescategory.edit"/></span>
	</jsp:attribute>
	<jsp:body>

	<!-- error section displayed when form submission fails -->
		<form:errors path="editsalescategoryform.*">
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<h5 class="center-0 attention"><spring:message code="editsalescategory.error"/>:</h5>
						<c:forEach var="e" items="${messages}">
							<div class="center attention"><c:out value="${e}"/></div>
						</c:forEach>
					</div>
				</div>
			</div>
		</form:errors>
	
	<div class="infobox">   
		<form:form modelAttribute="editsalescategoryform" method="POST">
			<fieldset>
		   		<legend><spring:message code="editsalescategory.addsalescategory"/></legend>
		   		<ol>
			    	<cwms:translations translations="${editsalescategoryform.translations}"/>
			    	
			    	<li>
			     		<label><spring:message code="editsalescategory.active"/></label>
			     		<form:checkbox path="salesCategory.active"/><br>
			     		<font color="red"><form:errors path="salesCategory.active"/></font>
			    	</li>
				    <li>
					     <form:hidden path="salesCategory.id"/><br>
					     <label>&nbsp;</label>
					     <input type="submit" name="save" id="save" value="Save" />
					     <c:if test="${editsalescategoryform.salesCategory.id > 0}">
					      	<input type="submit" name="delete" id="delete" value="Delete" />
					     </c:if>
				    </li>
		   		</ol>
		 	</fieldset>       
		</form:form>
	</div>
	</jsp:body>
</t:crocodileTemplate>