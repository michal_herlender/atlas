<%-- File name: /trescal/core/instrumentmodel/salescategory/viewsalescategory.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<script src='script/trescal/core/instrumentmodel/salescategory/viewsalescategory.js'></script>
<script>
	var locale = '${locale}';
</script>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="editsalescategory.title"/></span>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox">
			<fieldset>		
				<legend><spring:message code="editsalescategory.title"/></legend>
				<br>
				<table id="salescategories" class="display" style="cellspacing: 2">
			        <thead>
			            <tr>
			            	<th align='left'><spring:message code="editsalescategory.id"/></th>
			                <th align='left'><spring:message code="editsalescategory.name"/></th>
			                <th align='center' width='10%'><spring:message code="editsalescategory.active"/></th>
			            </tr>
			        </thead>
			    </table>
			</fieldset>
		</div>
	</jsp:body>
</t:crocodileTemplate>