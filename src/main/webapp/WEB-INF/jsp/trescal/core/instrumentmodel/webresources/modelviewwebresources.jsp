<%-- File name: /trescal/core/instrumentmodel/webresources/modelwebresources.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<!-- TODO: Below taglibs are not used yet but are expected to be as content built out, remove if unused -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!--  TODO: Include page specific javascript file below once created -->
<!-- <script type='text/javascript' src='script/trescal/core/instrumentmodel/webresources/ModelViewWebResources.js'></script> -->

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<instmodel:modelViewPageTitle selectedTab="${selectedTab}"/>
		</span>
	</jsp:attribute>
	<jsp:body>

	<instmodel:instrumentModelTable model="${command.model}" />
	<instmodel:instrumentModelTabs model="${command.model}" selectedTab="${command.selectedTab}" canEditInstrumentModel="${command.canEditInstrumentModel}" canEditSalesCategory="${command.canEditSalesCategory}" />
	
	<!-- TODO: Insert implementation specific content here -->
	
	TODO: Content goes here

	</jsp:body>
</t:crocodileTemplate>