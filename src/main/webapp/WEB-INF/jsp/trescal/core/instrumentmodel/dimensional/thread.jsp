<%-- File name: /trescal/core/instrumentmodel/dimensional/thread.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
		<span class="headtext"><spring:message code="thread.addeditthread" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/instrumentmodel/dimensional/Thread.js'></script>
    </jsp:attribute>
    <jsp:body>			
			<c:set var="thread" value="${command.thread}"/>
		
			<!-- infobox contains all description form elements and is styled with nifty corners -->
			<div class="infobox">
				
				<!-- link to thread search -->
				<a class="mainlink-float" href="<spring:url value='/threadsearch.htm'/>"><spring:message code="threads.searchthreads" /></a>
				<!-- clear floats and restore page flow -->
				<div class="clear"></div>
			
				<form:form method="post" action="" >
			
					<form:errors path="*">
                     <div class="warningBox1">
                        <div class="warningBox2">
                            <div class="warningBox3">
                                <h5 class="center-0 attention">
                                    <spring:message code='cylindricalstandard.errorsaved' />
                                </h5>
                                <c:forEach var="e" items="${messages}">
                                    <div class="center attention"><c:out value="${e}"/></div>
                                </c:forEach>
                            </div>
                        </div>
                     </div>
                    </form:errors>
                  
					<fieldset id="threadform">
						
						<legend><spring:message code="thread.addeditthread" /></legend>
						
						<ol>
							<li>
								<label><spring:message code="thread.size" />:</label>
								<form:input path="thread.size" tabindex="" />
								<form:errors path="thread.size" class="attention-pad" />
							</li>
							<li>
								<label><spring:message code="thread.type" />:</label>
								<form:select path="typeId">
									<option value=""><spring:message code="thread.select" /></option>
									<c:forEach var="type" items="${threadtypes}">
										<option value="${type.id}" <c:if test="${type.id == command.typeId}"> selected="selected" </c:if>>${type.type}</option>
									</c:forEach>
								</form:select>
								<form:errors path="typeId" class="attention-pad" />
							</li>
							<li>
								<label><spring:message code="thread.wire" />:</label>
								<form:select path="wireId">
									<option value=""><spring:message code="thread.select" /></option>
									<c:forEach var="wire" items="${wires}">
										<option value="${wire.id}" <c:if test="${wire.id == command.wireId}"> selected="selected" </c:if>>${wire.designation}</option>
									</c:forEach>
								</form:select>
								<form:errors path="wireId" class="attention-pad" />
							</li>
							<li>
								<label><spring:message code="cylindricalstandard.cylindricalstandard" />:</label>
								<form:select path="cylindricalStandardId">
									<option value=""><spring:message code="thread.select" /></option>
									<c:forEach var="cs" items="${cylindricalstandards}">
										<option value="${cs.id}" <c:if test="${cs.id == command.cylindricalStandardId}"> selected="selected" </c:if>>${cs.id}</option>
									</c:forEach>
								</form:select>
								<form:errors path="cylindricalStandardId" class="attention-pad" />
							</li>
							<li>
								<label><spring:message code="thread.threaduom" />:</label>
								<form:select path="uomId">
									<option value=""><spring:message code="thread.select" /></option>
									<c:forEach var="uom" items="${threaduoms}">
										<option value="${uom.id}" <c:if test="${uom.id == command.uomId}"> selected="selected" </c:if>>${uom.description}</option>
									</c:forEach>
								</form:select>
								<form:errors path="uomId" class="attention-pad" />
							</li>
							<li>
								&nbsp;
							</li>
							<li>
								<label><spring:message code="thread.goeffectivediameter" />:</label>
								<form:input path="thread.goEffectiveDiameter" tabindex="" />
								<form:errors path="thread.goEffectiveDiameter" class="attention-pad" />
							</li>
							<li>
								<label><spring:message code="thread.goupperlimit" />:</label>
								<form:input path="thread.goUpperLimit" tabindex="" />
								<form:errors path="thread.goUpperLimit" class="attention-pad" />
							</li>
							<li>
								<label><spring:message code="thread.golowerlimit" />:</label>
								<form:input path="thread.goLowerLimit" tabindex="" />
								<form:errors path="thread.goLowerLimit" class="attention-pad" />
							</li>
							<li>
								<label><spring:message code="thread.gowornlimit" />:</label>
								<form:input path="thread.goWornLimit" tabindex="" />
								<form:errors path="thread.goWornLimit" class="attention-pad" />
							</li>											
							<li>
								&nbsp;
							</li>											
							<li>
								<label><spring:message code="thread.notgoeffective" />:</label>
								<form:input path="thread.notGoEffective" tabindex="" />
								<form:errors path="thread.notGoEffective" class="attention-pad" />
							</li>
							<li>
								<label><spring:message code="thread.notgoupperlimit" />:</label>
								<form:input path="thread.notGoUpperLimit" tabindex="" />
								<form:errors path="thread.notGoUpperLimit" class="attention-pad" />
							</li>
							<li>
								<label><spring:message code="thread.notgolowerlimit" />:</label>
								<form:input path="thread.notGoLowerLimit" tabindex="" />
								<form:errors path="thread.notGoLowerLimit" class="attention-pad" />
							</li>
							<li>
								<label><spring:message code="thread.notgowornlimit" />:</label>
								<form:input path="thread.notGoWornLimit" tabindex="" />
								<form:errors path="thread.notGoWornLimit" class="attention-pad" />
							</li>										
							<li>
								<label><spring:message code="thread.pitch" />:</label>
								<form:input path="thread.pitch" tabindex="" />
								<form:errors path="thread.pitch" class="attention-pad" />
							</li>
							<li>
								<label><spring:message code="thread.nostarts" />:</label>
								<form:input path="thread.noStarts" tabindex="" />
								<form:errors path="thread.noStarts" class="attention-pad" />
							</li>
							<li>
								<label><spring:message code="thread.lead" />:</label>
								<form:input path="thread.lead" tabindex="" />
								<form:errors path="thread.lead" class="attention-pad" />
							</li>
							<li>
								<label><spring:message code="thread.ball" />:</label>
								<form:input path="thread.ball" tabindex="" />
								<form:errors path="thread.ball" class="attention-pad" />
							</li>											
							<li>
								&nbsp;
							</li>											
							<li>
								<label><spring:message code="thread.taperlimit" />:</label>
								<form:input path="thread.taperLimit" tabindex="" />
								<form:errors path="thread.taperLimit" class="attention-pad" />
							</li>
							<li>
								<label><spring:message code="thread.overalllength" />:</label>
								<form:input path="thread.overallLength" tabindex="" />
								<form:errors path="thread.overallLength" class="attention-pad" />
							</li>
							<li>
								<label><spring:message code="thread.overalllengthb" />:</label>
								<form:input path="thread.overallLengthB" tabindex="" />
								<form:errors path="thread.overallLengthB" class="attention-pad" />
							</li>
							<li>
								<label><spring:message code="thread.overalllengthupper" />:</label>
								<form:input path="thread.overallLengthUpper" tabindex="" />
								<form:errors path="thread.overallLengthUpper" class="attention-pad" />
							</li>
							<li>
								<label><spring:message code="thread.overalllengthlower" />:</label>
								<form:input path="thread.overallLengthLower" tabindex="" />
								<form:errors path="thread.overallLengthLower" class="attention-pad" />
							</li>											
							<li>
								<label><spring:message code="thread.lengthgaugeplane" />:</label>
								<form:input path="thread.lengthToGaugePlane" tabindex="" />
								<form:errors path="thread.lengthToGaugePlane" class="attention-pad" />
							</li>
							<li>
								<label><spring:message code="thread.lengthgaugeplaneu" />:</label>
								<form:input path="thread.lengthToGaugePlaneUpper" tabindex="" />
								<form:errors path="thread.lengthToGaugePlaneUpper" class="attention-pad" />
							</li>
							<li>
								<label><spring:message code="thread.lengthgaugeplanel" />:</label>
								<form:input path="thread.lengthToGaugePlaneLower" tabindex="" />
								<form:errors path="thread.lengthToGaugePlaneLower" class="attention-pad" />
							</li>
							<li>
								<label><spring:message code="thread.sdatum" />:</label>
								<form:input path="thread.SDatum" tabindex="" />
								<form:errors path="thread.SDatum" class="attention-pad" />
							</li>										
							<li>
								<label><spring:message code="thread.depthstep" />:</label>
								<form:input path="thread.depthStep" tabindex="" />
								<form:errors path="thread.depthStep" class="attention-pad" />
							</li>
							<li>
								<label><spring:message code="thread.depthstepupper" />:</label>
								<form:input path="thread.depthStepUpper" tabindex="" />
								<form:errors path="thread.depthStepUpper" class="attention-pad" />
							</li>
							<li>
								<label><spring:message code="thread.depthsteplower" />:</label>
								<form:input path="thread.depthStepLower" tabindex="" />
								<form:errors path="thread.depthStepLower" class="attention-pad" />
							</li>
							<li>
								<label><spring:message code="thread.maxstep" />:</label>
								<form:input path="thread.maxStep" tabindex="" />
								<form:errors path="thread.maxStep" class="attention-pad" />
							</li>
							<li>
								<label><spring:message code="thread.minstep" />:</label>
								<form:input path="thread.minStep" tabindex="" />
								<form:errors path="thread.minStep" class="attention-pad" />
							</li>										
							<li>
								<label>&nbsp;</label>
								<input type="submit" name="submit" id="submit" value="<spring:message code='submit' />" />
							</li>
						</ol>
					
					</fieldset>
				
				</form:form>
			
			</div>
			<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>