<%-- File name: /trescal/core/instrumentmodel/configuration/modelviewconfiguration.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<instmodel:modelViewPageTitle selectedTab="${selectedTab}" />
		</span>
	</jsp:attribute>
	<jsp:body>

	<instmodel:instrumentModelTable model="${model}" />
	<instmodel:instrumentModelTabs model="${model}" selectedTab="${selectedTab}" canEditInstrumentModel="${canEditInstrumentModel}" canEditSalesCategory="${canEditSalesCategory}" />

	<div class="infobox">	
		
		<c:set var="colspan" value="9" />
		<c:set var="libraryMap" value="${model.getLibraryCharacteristics()}" /> 
		<c:if test="${model.tmlid == null || model.tmlid == 0}">
			<cwms:securedLink permission="MODEL_EDIT_CHARACTERISTICS" parameter="?modelid=${model.modelid}" title="${linkTitle}" classAttr="mainlink-float">
				<spring:message code="viewmod.editconfig" />
			</cwms:securedLink>
            <br/>
        </c:if>
		<table class="default2" id="modelviewconfigurationcharacteristics">
			<thead>
				<tr>
					<td colspan="${colspan}">
						<spring:message code="characteristics" />
					</td>
				</tr>
				<tr>
					<th id="orderby" scope="col" rowspan="2"><spring:message code="viewjob.orderby" /></th>
					<th id="shortname" scope="col" rowspan="2"><spring:message code="shortname" /></th>
					<th id="formatted" scope="col" rowspan="2"><spring:message code="description" /></th>
					<th id="seton" scope="col" rowspan="2"><spring:message code="viewinstrument.seton" /></th>
					<th id="seton" scope="col" rowspan="2"><spring:message code="type" /></th>
					<th id="key" scope="col" rowspan="2"><spring:message code="key" /></th>
					<th colspan="3"><spring:message code="values" /></th>
				</tr>
				<tr>
					<th id="min" scope="col"><spring:message code="min" /></th>
					<th id="max" scope="col"><spring:message code="max" /></th>
					<th id="unit" scope="col"><spring:message code="unit" /></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="${colspan}">&nbsp;</td>
				</tr>
			</tfoot>
			<tbody>
				<c:forEach items="${sortedCharacteristics}" var="charDesc" >
					<%-- ModelRange will only be set if characteristic is set on instrument model --%>
					<c:set var="rangeList" value="${rangeMap.get(charDesc)}" />
					<c:set var="libraryValues" value="${libraryMap.get(charDesc)}" />
					<c:choose>
						<c:when test="${not empty rangeList || not empty libraryValues}">
							<spring:message var="setonText" code="viewinstrument.instrumentmodel"/>
							<c:set var="classText" value="bold" />
						</c:when>
						<c:otherwise>
							<spring:message var="setonText" code="instrument" />
							<c:set var="classText" value="" />
						</c:otherwise>
					</c:choose>
					<tr>
						<td>
							<span class="${classText}">
								<c:out value="${charDesc.getOrderno()}" />
							</span>
						</td>
						<td>
							<span class="${classText}">
								<c:choose>
									<c:when test="${not empty charDesc.shortNameTranslations}">
										<cwms:besttranslation translations="${charDesc.shortNameTranslations}"/>
									</c:when>
									<c:otherwise>
										${charDesc.shortName}
									</c:otherwise>
								</c:choose>
							</span>
						</td>
						<td>
							<span class="${classText}">
								<c:choose>
									<c:when test="${charDesc.translations.size() > 0}">
										<cwms:besttranslation translations="${charDesc.translations}"/>
									</c:when>
									<c:otherwise>
										${charDesc.name}
									</c:otherwise>
								</c:choose>
							</span>
						</td>
						<td>
							<span class="${classText}">
								<c:out value="${setonText}" />
							</span>
						</td>
						<td>
							<span class="${classText}">
								<c:out value="${charDesc.getCharacteristicType().getMessage()}" />
							</span>
						</td>
						<td>
							<span class="${classText}">
								<%-- Note, some values are null --%>
								<spring:message code="${charDesc.partOfKey ? 'yes' : 'no'}" />
							</span>
						</td>
						<c:choose>
							<c:when test="${charDesc.getCharacteristicType() == 'LIBRARY' }">
								<td colspan="3">
									<span class="${classText}">
										<c:forEach var="libValue" items="${libraryMap.get(charDesc)}" varStatus="loopStatus">
											<c:if test="${!loopStatus.first}">
												<c:out value=" - " />
											</c:if>
											<t:showTranslationOrDefault translations="${libValue.translation}" defaultLocale="${defaultlocale}"/>
										</c:forEach>
									</span>
								</td>
							</c:when>
							<c:otherwise>
								<c:choose>
									<c:when test="${not empty rangeList and rangeList.size() eq 1}">
										<c:set var="modelRange" value="${rangeList.get(0)}" />
										<c:choose>
											<c:when test="${modelRange.characteristicDescription.characteristicType == 'VALUE_WITH_UNIT'}">
												<td><span class="${classText}">${modelRange.start}</span></td>
												<td><span class="${classText}">${modelRange.end}</span></td>
											</c:when>
											<c:otherwise>
												<td><span class="${classText}">${modelRange.minvalue}</span></td>
												<td><span class="${classText}">${modelRange.maxvalue}</span></td>
											</c:otherwise>
										</c:choose>
										<td>
											<span class="${classText}">
												${modelRange.uom.symbol}
											</span>
										</td>
									</c:when>
									<c:otherwise>
										<td colspan="3">
											<c:if test="${not empty rangeList}">
												<span class="error">
													<c:out value="${rangeList.size()} ranges defined" />
												</span>
											</c:if>
										</td>
									</c:otherwise>
								</c:choose>
							</c:otherwise>
						</c:choose>
					</tr>
				</c:forEach>
			</tbody>											
		</table>
		
		<c:set var="colspan" value="4" />
		<table class="default2" id="modelviewconfigurationoptions">
			<thead>
				<tr>
					<td colspan="${colspan}"><spring:message code="options" /></td>
				</tr>
				<tr>
					<th>
						<spring:message code="shortname" />
					</th>
					<th id="formatted" scope="col">
						<spring:message code="description" />
					</th>
					<th id="formatted" scope="col">
						Translation
					</th>
					<th id="formatted" scope="col">
						TML Option ID
					</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="${colspan}">&nbsp;</td>
				</tr>
			</tfoot>
			<tbody>
				<c:forEach items="${model.ranges}" var="range">
					<c:if test="${range.characteristicType == 'OPTION'}">
						<tr>
							<td>${range.modelOption.code}</td>
							<td>${range.modelOption.name}</td>
							<td>
								<t:showTranslationOrDefault
										translations="${range.modelOption.translation}"
										defaultLocale="${defaultlocale}" />
							</td>
							<td>${range.modelOption.tmlid}</td>
						</tr>
					</c:if>
				</c:forEach>	
			</tbody>											
		</table>

		<table class="default2" id="modelviewconfigurationtranslations">
			<thead>
				<tr>
					<td colspan="3"><spring:message code="descview.translations" /></td>
				</tr>
				<tr>
					<th id="language" scope="col"><spring:message code="descview.language" /></th>
					<th id="locale" scope="col"><spring:message code="reference" /></th>
					<th id="translation" scope="col"><spring:message code="value" /></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
			</tfoot>
			<tbody>
				<c:forEach var="locale" items="${supportedLocales}">
					<tr>
						<td><c:out value="${locale.getDisplayLanguage()}"/></td>
						<td><c:out value="${locale.toString()}"/></td>
						<td>
							
							<c:choose>
								<c:when test="${translationMap.containsKey(locale)}">
									<c:out value="${translationMap.get(locale)}"/>
								</c:when>
								<c:otherwise>
									<c:out value="--- " />
									<spring:message code="tools.none" />
									<c:out value="--- " />
								</c:otherwise>
							</c:choose>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
				
	</div>

	</jsp:body>
</t:crocodileTemplate>