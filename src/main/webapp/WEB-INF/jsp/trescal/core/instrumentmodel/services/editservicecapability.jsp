<%-- File name: /trescal/core/instrumentmodel/services/editservicecapability.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<cwms:showmodel instrumentmodel="${instrumentModel}" />
		</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='module' src='script/components/search-plugins/cwms-capability-search/cwms-capability-search.js'></script>
	</jsp:attribute>
	<jsp:body>
		<form:errors path="form.*">
            	<div class="warningBox1">
                     <div class="warningBox2">
                          <div class="warningBox3">
                                <h5 class="center-0 attention">
                                    <spring:message code="editservicecapability.error" />
                                </h5>
                                <c:forEach var="e" items="${messages}">
                                    <div class="center attention"><c:out value="${e}"/></div>
                                </c:forEach>
                           </div>
                       </div>
                 </div>
         </form:errors>
        
        <c:choose>
        <c:when test="${caltypes.size() == 0}">
       		<div class="warningBox1">
            	<div class="warningBox2">
                	<div class="warningBox3">
                    	<h5 class="center-0 attention">
							<spring:message code="editservicecapability.noservicetypes" />
						</h5>
                    </div>
                </div>
           </div>
        </c:when>
        <c:otherwise>

        <div class="infobox">

            <form:form modelAttribute="form" method="post" >
                <fieldset>
                        <ol>
                            <li>
                                <label><spring:message
									code="model" />:</label>
                                <cwms:showmodel instrumentmodel="${instrumentModel}" />
                            </li>
                            <li>
                                <label> <spring:message code="costtype" />:</label>
									${costType }
                            </li>
                            <li>
                                <label><spring:message
									code="servicetype" />:</label>
                                <c:choose>
                                    <c:when test="${form.id == 0}">
                                    	<form:select path="serviceTypeId" items="${serviceTypes}" itemLabel="value" itemValue="key"/>
                                    </c:when>
                                    <c:otherwise>
                                        ${shortServiceType} - ${longServiceType} 
                                    </c:otherwise>
                                </c:choose>

                            </li>
                            <li>
                                <label><spring:message code="capability" /> :</label>
								<div class="float-left">
                                    <cwms-capability-search-form 
                                    name="procedureId" procid="${form.procedureId}" defaultvalue="${form.procedureRef}"></cwms-capability-search-form>
								</div>
                            </li>
                            <li>
                                <label><spring:message code="editservicecapability.calibrationprocess" /> :</label>
                               	<select name="processId" style="width: 145px;">
                               		<option value="0">None</option>
                                    <c:forEach items="${processes}" var="process">
                                       <option value="${process.id}" ${process.id == form.processId ? 'selected="selected"' : ''}>${process.process}</option>
                                     </c:forEach>
                                 </select>
                            </li>
                            <li>
                                <label><spring:message
									code="checksheet" />:</label>
                                <input name="checkSheet" Style="width: 250px" value="${form.checkSheet}"/>
                            </li>
                            <li>
                                <label>&nbsp;</label>
                                <form:hidden path="id" />
                                <form:hidden path="modelId" />
                                <form:hidden path="subdivId" />
                                <input type="submit" name="save" id="submit" value="<spring:message code="save" />" />
                                <c:if test="${form.id != 0}">
                                    <input type="submit" name="delete" id="delete" value="Delete" />
                                </c:if>
                            </li>
                        </ol>
                </fieldset>
            </form:form>
        </div>
        </c:otherwise>
        </c:choose>
    </jsp:body>
</t:crocodileTemplate>