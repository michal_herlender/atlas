<%-- File name: /trescal/core/instrumentmodel/editmodelcomponent.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="instmodeditmodcomp.title"/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/instrumentmodel/EditModelComponent.js'></script>
    </jsp:attribute>
    <jsp:body>			
		<!-- infobox contains all form elements for adding a new model component and is styled with nifty corners -->													
		<div class="infobox">
		
		<form:form method="post" action="" >
			<form:errors path="*">
            	<div class="warningBox1">
                     	<div class="warningBox2">
                          	<div class="warningBox3">
                            <h5 class="center-0 attention">
                                  <spring:message code="error.save" />
                            </h5>
                            <c:forEach var="e" items="${messages}">
                                <div class="center attention"><c:out value="${e}"/></div>
                            </c:forEach>
                        </div>
                    </div>
                </div>
            </form:errors>
		
			<fieldset>
			
				<legend><spring:message code="instmodeditmodcomp.coremodelcomponentdefinition" /></legend>
				
				<ol>
					<li>
						<label><spring:message code="manufacturer" />:</label>
						<!-- float div left -->
						<div class="float-left">
							<!-- this div creates a new manufacturer jquery search plugin -->	
							<div class="mfrSearchJQPlugin">
								<input type="hidden" name="fieldId" value="mfrid" />
								<input type="hidden" name="tabIndex" value="1" />
								<input type="hidden" name="mfrNamePrefill" value="${command.mfrName}" />
								<input type="hidden" name="mfrIdPrefill" value="${command.mfrid}" />
								<!-- manufacturer results appear here -->
							</div>
						</div>																					 
						<a href="#" class="domthickbox padtop mainlink-float" onclick=" tb_show('New Manufacturer', 'TB_dom?width=640&height=360', '', createManufacturerContent(true)); " title=""><spring:message code="instmodeditmod.newmanufacturer" /></a>
						<form:errors class="attention-pad" path="mfrid"/>
						<!-- clear floats and restore pageflow -->
						<div class="clear-0"></div>
						
					</li>
					<li>
						<label><spring:message code="instmodeditmodcomp.componentdesc" />:</label>
						<!-- float div left -->
						<div class="float-left">
							<!-- this div creates a new component description search plugin -->
							<div class="compDescSearchJQPlugin">
								<input type="hidden" name="fieldId" value="descid" />
								<input type="hidden" name="compDescNamePrefill" value="${command.descName}" />
								<input type="hidden" name="compDescIdPrefill" value="${command.descid}" />													
							</div>
						</div>
						<a href="#" class="domthickbox padtop mainlink-float" onclick=" event.preventDefault(); tb_show('New Component Description', 'TB_dom?width=640&height=360', '', createCompDescriptionContent()); " title=""><spring:message code="instmodeditmodcomp.newcomponentdescription"/></a>
						<form:errors class="attention-pad" path="descid"/>										
						<!-- clear floats and restore page flow -->
						<div class="clear"></div>
					</li>
					<li>
						<label><spring:message code="instmodeditmodcomp.stockcount"/>:</label>
						<form:input path="inStock" value="${command.inStock}"/>
						<form:errors class="attention-pad" path="inStock"/>
					</li>
					<li>
						<label><spring:message code="instmodeditmodcomp.reorderlevel"/>:</label>
						<form:input path="minimumStockLevel" value="${command.minimumStockLevel}"/>
						<form:errors class="attention-pad" path="minimumStockLevel"/>
					</li>
					<li>
						<label><spring:message code="instmodeditmodcomp.lastchecked"/>:</label>
						<span>
							<c:choose> 
								<c:when test="${not empty command.stockLastCheckedOn}">	
									<fmt:formatDate value="${command.stockLastCheckedOn}" type="date" dateStyle="SHORT"/>
								</c:when>
								<c:otherwise>
									&nbsp;
								</c:otherwise>
							</c:choose>
						</span>
					</li>
					<li>
						<label><spring:message code="instmodeditmodcomp.checkedby"/>:</label>
						<span>
							<c:choose> 
								<c:when test="${not empty command.stockLastCheckedBy}">
									${command.stockLastCheckedBy}
								</c:when>
								<c:otherwise>
									&nbsp;
								</c:otherwise>
								</c:choose>
						</span>
					</li>
					<li>
						<label><spring:message code="instmodeditmodcomp.salescost"/>:</label>
						<form:input path="salesCost" value="${command.salesCost}"/>
						<form:errors class="attention-pad" path="salesCost"/>
					</li>
					<li>
						<label>&nbsp;</label>
						<input name="submit" id="submit" type="submit" value="<spring:message code='submit'/>" />
					</li>
				</ol>
			
			</fieldset>
		
		</form:form>
		
		</div>
		<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>