<%-- File name: /trescal/core/instrumentmodel/dimensional/cylindricalstandard.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
       <span class="headtext"><spring:message code="cylindricalstandard.cylindricalstandard" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/instrumentmodel/dimensional/CylindricalStandard.js'></script>
    </jsp:attribute>
    <jsp:body>
		<!-- infobox contains all cylindrical standard form elements and is styled with nifty corners -->
		<div class="infobox">
			
			<!-- link to thread search -->
			<a href="<spring:url value='/threadsearch.htm'/>" class="mainlink-float">
				<spring:message code="threads.searchthreads" /></a>
			<!-- clear floats and restore page flow -->
			<div class="clear"></div>
		
			<form:form method="post" action="" >
				<form:errors path="*">
            		<div class="warningBox1">
                     		<div class="warningBox2">
                          		<div class="warningBox3">
                                	<h5 class="center-0 attention">
                                    	<spring:message code="error.save" />
                                	</h5>
                                	<c:forEach var="e" items="${messages}">
                                	    <div class="center attention"><c:out value="${e}"/></div>
                            	    </c:forEach>
                        	   </div>
                    	   </div>
                	 </div>
            	</form:errors>
		
				<fieldset>
					
					<legend><spring:message code="cylindricalstandard.addeditcylindricalstandard" /></legend>
					
					<ol>
						<li>
							<label><spring:message code="cylindricalstandard.id" />:</label>
							<%-- The ID can't be edited for existing values, since it's actually a database id. :-(  --%>
							<form:input path="cs.id" tabindex="" />
							<span class="attention-pad">${status.errorMessage}</span>
						</li>
						<li>
							<label><spring:message code="cylindricalstandard.imperial" />:</label>
							<form:input path="cs.sizeImperial" tabindex="" />
							<span class="attention-pad">${status.errorMessage}</span>
						</li>
						<li>
							<label><spring:message code="cylindricalstandard.metric" />:</label>
							<form:input path="cs.sizeMetric" tabindex="" />
							<span class="attention-pad">${status.errorMessage}</span>
						</li>											
						<li>
							<label>&nbsp;</label>
							<input type="submit" name="submit" id="submit" value="<spring:message code='submit' />" />
						</li>
					</ol>
				
				</fieldset>
			
			</form:form>
		
		</div>
		<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>