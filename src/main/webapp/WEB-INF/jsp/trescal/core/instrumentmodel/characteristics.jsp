<%-- File name: /trescal/core/instrumentmodel/characteristics.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<c:if test="${characteristics != null && characteristics.size() > 0}">
	<li id="characteristicLi">
		<label>Characteristics</label>
		<ul>
			<li></li>
			<c:forEach var="characteristic" items="${characteristics}">
				<li>
					<label><cwms:besttranslation translations="${characteristic.translations}"/></label>
					<c:choose>
						<c:when test="${characteristic.characteristicTypeInt == 6}">
							<select name="characteristics[${characteristic.characteristicDescriptionId}]">
								<option value=""><spring:message code="all"/></option>
								<c:forEach var="libraryVal" items="${characteristic.library}">
									<option value="${libraryVal.characteristiclibraryid}"><cwms:besttranslation translations="${libraryVal.translation}"/></option>
								</c:forEach>
							</select>
						</c:when>
					</c:choose>
				</li>
			</c:forEach>
		</ul>
	</li>
</c:if>