<%-- File name: /trescal/core/instrumentmodel/dimensional/threadtype.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
		<span class="headtext"><spring:message code="threadtype.addeditthreadtype" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/instrumentmodel/dimensional/ThreadType.js'></script>
    </jsp:attribute>
    <jsp:body>		
		<!-- infobox contains all thread form elements and is styled with nifty corners -->
		<div class="infobox">
			
			<!-- link to thread search -->
			<a href="<spring:url value='/threadsearch.htm'/>" class="mainlink-float"><spring:message code="threads.searchthreads" /></a>
			<!-- clear floats and restore page flow -->
			<div class="clear"></div>
			
			<form:form method="post" action="" >
				 <form:errors path="*">
                     <div class="warningBox1">
                        <div class="warningBox2">
                            <div class="warningBox3">
                                <h5 class="center-0 attention">
                                    <spring:message code='cylindricalstandard.errorsaved' />
                                </h5>
                                <c:forEach var="e" items="${messages}">
                                    <div class="center attention"><c:out value="${e}"/></div>
                                </c:forEach>
                            </div>
                        </div>
                     </div>
                    </form:errors>
				   <fieldset>
					
					 <legend><spring:message code="threadtype.addeditthreadtype" /></legend>
					
					 <ol>
						<li>
							<label><spring:message code="thread.type" />:</label>
							<form:input path="type" tabindex="" />
							<form:errors path="type" class="attention-pad" />
						</li>
						<li>
							<label>&nbsp;</label>
							<input type="submit" name="submit" id="submit" value="<spring:message code='submit' />" />
						</li>
					</ol>
				
				</fieldset>
			
			</form:form>
		</div>
		<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>