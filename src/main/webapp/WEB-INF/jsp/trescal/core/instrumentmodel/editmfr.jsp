<%-- File name: /trescal/core/instrumentmodel/editmfr.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
 		<span class="headtext"><spring:message code="instmodeditmfr.title" />: ${command.name}</span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/instrumentmodel/EditMfr.js'></script>
        <script type='text/javascript' src='script/thirdparty/jQuery/jquery.js'></script>
        <script type='module' src='script/components/cwms-upload-file/cwms-upload-file.js'></script>
    </jsp:attribute>
    <jsp:attribute name="globalElements">
    	<cwms-upload-file system-component-id="${sc.componentId}" entity-id1="${command.mfrid}" entity-id2=""
 			files-name="${filesName}" rename-to="" web="false"></cwms-upload-file>
    </jsp:attribute>
    <jsp:body>
        <!-- infobox contains all description form elements and is styled with nifty corners -->
		<div class="infobox">
		
			<form:form method="post" action="" >
		
			<fieldset>
			
				<legend><spring:message code="edit" /> (${command.name})</legend>
				
				<ol>
					<li>
						<label><spring:message code="instmodeditmfr.mfrname" />:</label>
						<form:input path="name" tabindex="" />
						<form:errors path="name" class="attention-pad" />
					</li>
					<li>
						<label><spring:message code="instmodeditmfr.active" /> :</label>
						<form:input type="hidden" path="active"/>
						
						<spring:message code="instmodeditmfr.active" />
						<input type="radio" name="activem" value="true" onclick="$j('#active').attr('value', 'true');" <c:if test="${command.active == true }"> checked="checked" </c:if>/>
						
						<spring:message code="instmodeditmfr.deactivated" />
						<input type="radio" name="activem" value="true" onclick="$j('#active').attr('value', 'false');" <c:if test="${command.active == false }"> checked="checked"  </c:if>/>
					</li>
					<li>
						<label>&nbsp;</label>
						<input type="submit" name="submit" id="submit" value="Submit" />
					</li>
				</ol>
			
			</fieldset>
			
			</form:form>
		
		</div>
		<!-- end of infobox div -->
		
		<!-- sub navigation menu which displays and hides sections of page as well as linking to external pages -->							
		<div id="subnav">
			<dl>
				<dt><a href="#" onclick="switchMenuFocus(menuElements, 'resources-tab', false); return false;" id="resources-link" class="selected" title="<spring:message code='instmodeditmfr.viewresourcesmanufacturer' />"><spring:message code="instmodeditmfr.resources" /></a></dt>
				<dt><a href="#" onclick="switchMenuFocus(menuElements, 'models-tab', false); return false; " id="models-link" title="<spring:message code='instmodeditmfr.viewmodels' />"><spring:message code="models" /> (${command.models.size()})</a></dt>
				<dt><a href="#" onclick="switchMenuFocus(menuElements, 'instruments-tab', false); return false;" id="instruments-link" title="<spring:message code='instmodeditmfr.viewinstr' />"><spring:message code="instruments" /></a></dt>
			</dl>
		</div>
		<!-- end of sub navigation menu -->
		
		<!-- this section displays all file and web resources for this manufacturer -->
		<div id="resources-tab">
		
			<div class="infobox">
				<files:showFilesForSC entity="${command}" sc="${sc}" id="${command.mfrid}" identifier="${identifier}" ver="" rootFiles="${scRootFiles}" allowEmail="false" isEmailPlugin="false" rootTitle="Files for ${command.name} folder" deleteFiles="true" />
			</div>
			
			<div class="infobox">
				<instmodel:showWebResourceList webResources="${command.webResources}" entityName="Mfr" serviceName="mfrwebresourceservice" id="${command.mfrid}" />
			</div>
			
		</div>
		<!-- end of file resources section -->
									
		<!-- this section displays all models which use this description -->
		<div id="models-tab" class="hid">
			
			<div class="infobox">
			
				<table class="default2" summary="">
					<thead>
						<tr>
							<td><spring:message code="instmodeditdesc.instmods" /> (${command.models.size()})</td>
						</tr>
						<tr>
							<th><spring:message code="instmodeditdesc.instmod" /></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td>&nbsp;</td>
						</tr>
					</tfoot>
					<tbody id="keynavResults">
						<c:choose>
							<c:when test="${empty command.models}">
								<tr class="odd">
									<td class="bold center">
										<spring:message code="noresults" />
									</td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:forEach var="model" items="${command.models}" varStatus="loopStatus">
									<c:set var="rowClass" value="${loopStatus.first ? 'hoverrow' : loopStatus.index % 2 == 0 ? 'even' : 'odd'}" />
 									<tr class="${rowClass}"
 										onclick="window.location.href='<c:url value="/instrumentmodel.htm?modelid=${model.modelid}"/>'" 
 										onmouseover=" $j(this).addClass('hoverrow'); " 
 										onmouseout=" if (this.className != 'hoverrow') $j(this).removeClass('hoverrow'); ">
										<td><t:showViewModelLink instrumentmodel="${model}" /></td>
 									</tr>
								</c:forEach>
							</c:otherwise>
							</c:choose>
					</tbody>
				</table>
			
			</div>
	
		</div>
		<!-- end of model section -->
		
		<!-- this section displays all instruments -->
		<div id="instruments-tab" class="hid">

		</div>
		<!-- end of instrument section -->
    </jsp:body>
</t:crocodileTemplate>