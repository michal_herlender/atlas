<%-- File name: /trescal/core/instrumentmodel/searchmodelcomponentform.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="modcompbrow.title" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/instrumentmodel/SearchModelComponentForm.js'></script>
    </jsp:attribute>
    <jsp:body>
		
		<!-- infobox contains all search model component form elements and is styled with nifty corners -->
		<div class="infobox" id="searchmodelcomponent">
		
			<a class="mainlink-float" href="<spring:url value='/editmodelcomponent.htm'/>">
				<spring:message code="modcompbrow.addnewcomponent" /></a>
			
			<!-- this div clears floats and restores page flow -->
			<div class="clear"></div>
			
			<form:form method="post" action="" id="searchmodelcomponentform" modelAttribute="command">
				<form:errors path="*">
            		<div class="warningBox1">
                     <div class="warningBox2">
                          <div class="warningBox3">
                                <h5 class="center-0 attention">
                                    <spring:message code="modcompbrow.errorsoccured" />
                                </h5>
                                <c:forEach var="e" items="${messages}">
                                    <div class="center attention"><c:out value="${e}"/></div>
                                </c:forEach>
                           </div>
                       </div>
                 	</div>
            	</form:errors>
		
			<fieldset id="componentbrowser">
			
				<legend><spring:message code="modcompbrow.title" /></legend>
				
				<ol>
					<li>
						<label><spring:message code="manufacturer" />:</label>
						<!-- float div left -->
						<div class="float-left">
							<!-- this div creates a new manufacturer jquery search plugin -->	
							<div class="mfrSearchJQPlugin">
								<input type="hidden" name="fieldId" value="mfrid" />
						<!--	<input type="hidden" name="fieldName" value="mfr" /> -->
								<input type="hidden" name="tabIndex" value="1" />
						<!--	<input type="hidden" name="textOnlyOption" value="true" /> -->
								<!-- manufacturer results appear here -->
							</div>
						</div>																							
						<!-- clear floats and restore pageflow -->
						<div class="clear-0"></div>
					</li>
					<li>
						<label><spring:message code="description" />:</label>
						<!-- float div left -->
						<div class="float-left">
							<!-- this div creates a new component description search plugin -->
							<div class="compDescSearchJQPlugin">
								<input type="hidden" name="fieldId" value="descid" />
						<!--	<input type="hidden" name="fieldName" value="desc" /> -->
								<input type="hidden" name="tabIndex" value="2" />
						<!--	<input type="hidden" name="textOnlyOption" value="true" /> -->
								<!-- description results appear here -->
							</div>
						</div>	
						<span class="attention-pad">${status.errorMessage}</span>
						<!-- clear floats and restore pageflow -->
						<div class="clear-0"></div>
					</li>
					<li>
						<fieldset>
							<legend>
								<spring:message code="modcompbrow.showcomponentsstockonly" />
								<form:checkbox id="stock" path="inStock" value="false" tabindex="3" onclick=" if(this.checked == true) { this.value = true } else { this.value = false; };"/>
							</legend>
						</fieldset>
					</li>
					<li>
						<label>&nbsp;</label>
						<input type="submit" id="submit" name="submit" value="<spring:message code='submit' />" tabindex="4" />
					</li>
				</ol>
			
			</fieldset>
			
		</form:form>
		
	</div>
		<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>