<%-- File name: /trescal/core/instrumentmodel/notes/modelviewnotes.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<!-- TODO: Below taglibs are not used yet but are expected to be as content built out, remove if unused -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:crocodileTemplate>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/instrumentmodel/notes/ModelViewNotes.js'></script>
		<script type='text/javascript'>var privateOnlyNotes = ${command.privateOnlyNotes};</script>
	</jsp:attribute>
	<jsp:attribute name="header">
		<span class="headtext">
			<instmodel:modelViewPageTitle selectedTab="${selectedTab}"/>
		</span>
	</jsp:attribute>
	<jsp:body>
		<instmodel:instrumentModelTable model="${command.model}" />
		<instmodel:instrumentModelTabs model="${command.model}" selectedTab="${command.selectedTab}" canEditInstrumentModel="${command.canEditInstrumentModel}" canEditSalesCategory="${command.canEditSalesCategory}" />
		<t:showTabbedNotes entity="${command.model}" privateOnlyNotes="${command.privateOnlyNotes}" noteTypeId="${command.model.modelid}" noteType="INSTRUMENTMODELNOTE"/>
		<!-- previous attribute: url="${requestScope['javax.servlet.forward.request_uri']}?modelid=${model.modelid}" -->
	</jsp:body>
</t:crocodileTemplate>