<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="company.addeditusergroup"/></span>
	</jsp:attribute>
	<jsp:body>
		<t:showErrors path="command.*" showFieldErrors="true"/>
		<div class="infobox" id="addusergroup">
			<fieldset>
				<legend><spring:message code="company.addeditusergroup" /></legend>
				<form:form>
					<ol>
						<li>
							<form:hidden path="userGroupId"/>
							<form:hidden path="coid"/>
							<label><spring:message code="name"/></label>
							<form:input path="name"/>
						</li>
						<li>
							<label><spring:message code="description"/></label>
							<form:input path="description"/>
						</li>
					</ol>
					
					<table class="default2 companyUserGroups">
						<thead>
							<tr>
								<td colspan=3><spring:message code="company.Properties"/></td>
							</tr>
							<tr>
								<td><spring:message code="name"/></td>
								<td><spring:message code="description"/></td>
								<td><spring:message code="value"/></td>
							</tr>
						</thead>
						<tbody>
						<c:forEach var="property" items="${allproperties}">
							<tr>
								<td>${property.getName()}</td><td>${property.getDescription()}</td>
								<td>
								<form:select path="selectedValues[${property.ordinal()}]">
									<c:forEach var="scope" items="${allscopes}">
											<option value="${scope.ordinal()}" ${scope.ordinal() == command.selectedValues.get(property.ordinal()) ? 'selected' : 'toss'}>
												${scope.getName()}
											</option> 
									</c:forEach>
								</form:select>
								</td>
							</tr>
						</c:forEach>
						<tbody>
					</table>

					<input type="submit"  value="<spring:message code='company.Submit'/>"/>
						
				</form:form>
			</fieldset>
		</div>
	</jsp:body>
</t:crocodileTemplate>