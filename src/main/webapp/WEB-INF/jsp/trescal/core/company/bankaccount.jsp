<%-- File name: /trescal/core/company/bankaccountadd.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image" %>


<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<c:choose>
				<c:when test="${form.isEdit}">
					<spring:message code="company.editbankaccount"/>
				</c:when>
				<c:otherwise>
					<spring:message code="company.addbankaccount"/>
				</c:otherwise>
			</c:choose>
		</span>
	</jsp:attribute>
	<jsp:body>
		<script>
			var focusElement = "iban";
		</script>
		<t:showErrors path="form.*" showFieldErrors="false"/>
		
		<div class="listbox">
			<form:form modelAttribute="form" method="post">
				<form:hidden id="entityId" path="entityId"/>
				<fieldset>
					<legend>
						<c:choose>
							<c:when test="${form.isEdit}">
								<spring:message code="company.editbankaccountforcompany"/>
							</c:when>
							<c:otherwise>
								<spring:message code="company.addbankaccountforcompany"/>
							</c:otherwise>
						</c:choose>
						<a href="viewcomp.htm?loadtab=bankaccounts-tab&coid=${reference.company.coid}" class="mainlink" >${reference.company.coname}</a>
					</legend>
					
					<ol>
						<li>
							<label for="iban" class="usage">
								<spring:message code="bankaccount.iban"/> *:
							</label>
							<div class="float-left">
								<form:input id="iban" path="iban" size="50"/>
							</div>
							<form:errors path="iban" element="div" delimiter="; " cssClass="float-left; error"/>
							<div class="clear"></div>															
						</li>
						<li>
							<label for="swiftBic" class="usage">
								<spring:message code="bankaccount.swiftbic"/> *:
							</label>
							<div class="float-left">
								<form:input id="swiftBic" path="swiftBic" size="20"/>
							</div>
							<form:errors path="swiftBic" element="div" delimiter="; " cssClass="float-left; error"/>
							<div class="clear"></div>															
						</li>
						<li>
							<form:label path="currencyId" class="usage">
								<spring:message code="company.currency"/> *:
							</form:label>
							<div class="float-left">
								<form:select id="currencyId" path="currencyId">
									<form:options items="${reference.currencies}" itemValue="currencyId" itemLabel="currencyName" />
								</form:select>
							</div>
							<form:errors path="currencyId" element="div" delimiter="; " cssClass="float-left; error"/>
							<div class="clear"></div>															
						</li>
						<li>
							<label for="bankName" class="usage">
								<spring:message code="bankaccount.bankname"/> *:
							</label>
							<div class="float-left">
								<form:input id="bankName" path="bankName" size="125"/>
							</div>
							<form:errors path="bankName" element="div" delimiter="; " cssClass="float-left; error"/>
							<div class="clear"></div>															
						</li>
						<li>
							<label for="accountNo" class="usage">
								<spring:message code="bankaccount.localaccountnumber"/>:
							</label>
							<div class="float-left">
								<form:input id="accountNo" path="accountNo" size="75"/>
							</div>
							<form:errors path="accountNo" element="div" delimiter="; " cssClass="float-left; error"/>
							<div class="clear"></div>															
						</li>
						<li>
							<label class="usage">
								<spring:message code="bankaccount.bankAccountUsage"/>
							</label>
							<div class="float-left">
								<form:checkbox path="includeOnFactoredInvoices"/>
								<spring:message code="bankaccount.factoredInvoices" />
								<form:errors path="includeOnFactoredInvoices"/>
								<br>
								<form:checkbox path="includeOnNonFactoredInvoices"/>
								<spring:message code="bankaccount.nonFactoredInvoices"/>
								<form:errors path="includeOnNonFactoredInvoices"/>
							</div>
							<div class="clear"></div>
						</li>
						<li>
							<label for="submit" class="usage">&nbsp;</label>
							<c:choose>
								<c:when test="${form.isEdit}">
									<input type="submit" id="submit" value="<spring:message code="company.editbankaccount"/>" />
								</c:when>
								<c:otherwise>
									<input type="submit" id="submit" value="<spring:message code='company.addbankaccount'/>" />
								</c:otherwise>
							</c:choose>
							<div class="clear"></div>															
						</li>
					</ol>				
			
				</fieldset>
			</form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>