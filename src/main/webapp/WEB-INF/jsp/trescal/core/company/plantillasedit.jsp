<%-- File name: /trescal/core/company/plantillasedit.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="address.editplantillassettings"/></span>
	</jsp:attribute>
	<jsp:body>
		<t:showErrors path="form.*" showFieldErrors="false" />
		<div class="listbox">
			<form:form modelAttribute="form" method="post">
				<fieldset>
					<legend>
						<spring:message code="address.plantillassettings"/>
						<a href="viewaddress.htm?addrid=${address.addrid}" class="mainlink" >${address.addr1}, ${address.town}</a>
					</legend>
					<ol>
						<li>
							<label class="width30">
								<spring:message code="plantillas.formercustomerid"/> :
							</label>
							<div class="float-left">
								<form:input id="formerCustomerId" path="formerCustomerId" size="20"/>
							</div>
							<form:errors path="formerCustomerId" element="div" delimiter="; " cssClass="float-left; error"/>
							<div class="clear"></div>															
						</li>
						<li>
							<label class="width30">
								<spring:message code="plantillas.putnextdatecerts"/> :
							</label>
							<div class="float-left">
								<form:radiobutton path="nextDateOnCertificate" value="true"/>
								<spring:message code="true"/>
								<br/>
								<form:radiobutton path="nextDateOnCertificate" value="false"/>
								<spring:message code="false"/>
								<br/>
								<form:errors path="nextDateOnCertificate" delimiter="; " cssClass="error"/>
							</div>
						</li>
						<li>
							<label class="width30">
								<spring:message code="plantillas.textuncertaintyfailure"/> :
							</label>
							<div class="float-left">
								<form:input id="textUncertaintyFailure" path="textUncertaintyFailure" size="60"/>
							</div>
							<form:errors path="textUncertaintyFailure" element="div" delimiter="; " cssClass="float-left; error"/>
							<div class="clear"></div>															
						</li>
						<li>
							<label class="width30">
								<spring:message code="plantillas.texttolerancefailure"/> :
							</label>
							<div class="float-left">
								<form:input id="textToleranceFailure" path="textToleranceFailure" size="60"/>
							</div>
							<form:errors path="textToleranceFailure" element="div" delimiter="; " cssClass="float-left; error"/>
							<div class="clear"></div>															
						</li>
						<li>
							<label class="width30">
								<spring:message code="plantillas.sendcertsbyftp"/> :
							</label>
							<div class="float-left">
								<form:radiobutton path="sendByFtp" value="true"/>
								<spring:message code="true"/>
								<br/>
								<form:radiobutton path="sendByFtp" value="false"/>
								<spring:message code="false"/>
								<br/>
								<form:errors path="sendByFtp" delimiter="; " cssClass="error"/>
							</div>
						</li>
						<li>
							<label class="width30">
								<spring:message code="plantillas.ftpserver"/> :
							</label>
							<div class="float-left">
								<form:input id="ftpServer" path="ftpServer" size="60"/>
							</div>
							<form:errors path="ftpServer" element="div" delimiter="; " cssClass="float-left; error"/>
							<div class="clear"></div>															
						</li>
						<li>
							<label class="width30">
								<spring:message code="plantillas.ftpuser"/> :
							</label>
							<div class="float-left">
								<form:input id="ftpUser" path="ftpUser" size="60"/>
							</div>
							<form:errors path="ftpUser" element="div" delimiter="; " cssClass="float-left; error"/>
							<div class="clear"></div>															
						</li>
						<li>
							<label class="width30">
								<spring:message code="plantillas.ftppassword"/> :
							</label>
							<div class="float-left">
								<form:input id="ftpPassword" path="ftpPassword" size="60"/>
							</div>
							<form:errors path="ftpPassword" element="div" delimiter="; " cssClass="float-left; error"/>
							<div class="clear"></div>															
						</li>
						<li>
							<label class="width30">
								<spring:message code="plantillas.metraclient"/> :
							</label>
							<div class="float-left">
								<form:radiobutton path="metraClient" value="true"/>
								<spring:message code="true"/>
								<br/>
								<form:radiobutton path="metraClient" value="false"/>
								<spring:message code="false"/>
								<br/>
								<form:errors path="metraClient" delimiter="; " cssClass="error"/>
							</div>
						</li>
						<li>
							<label class="width30">
								<spring:message code="company.contract"/> :
							</label>
							<div class="float-left">
								<form:radiobutton path="contract" value="true"/>
								<spring:message code="true"/>
								<br/>
								<form:radiobutton path="contract" value="false"/>
								<spring:message code="false"/>
								<br/>
								<form:errors path="contract" delimiter="; " cssClass="error"/>
							</div>
						</li>
						<li>
							<label for="submit" class="usage">&nbsp;</label>
							<div class="float-left">
								<input type="submit" id="submit" value="<spring:message code="save"/>" />
							</div>
							<div class="clear"></div>															
						</li>
					</ol>
				</fieldset>
			</form:form>
		</div>
		
	</jsp:body>
</t:crocodileTemplate>