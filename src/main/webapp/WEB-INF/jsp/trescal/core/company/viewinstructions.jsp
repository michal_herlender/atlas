<%-- File name: /trescal/core/company/viewinstructions.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="company" tagdir="/WEB-INF/tags/company"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script src='script/trescal/core/company/ViewInstructions.js'></script>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">${form.entityName} <spring:message code="company.instructions"/></span>
	</jsp:attribute>
	<jsp:body>
		<company:instructions command="${form}"/>
	</jsp:body>
</t:crocodileTemplate>