<%-- File name: /trescal/core/company/systemdefaultconfig.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address" %>
<%@ taglib prefix="company" tagdir="/WEB-INF/tags/company" %>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>

<t:crocodileTemplate>
	<jsp:attribute name="scriptPart">
	<link rel="stylesheet" href="styles/trescal/core/system/systemdefaultconfig.css" type="text/css" >
	</jsp:attribute>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="adminarea.managesystemdefaults" /></span>
	</jsp:attribute>
	<jsp:body>
		<table class="default4">
			<thead>
				<tr>
					<td><spring:message code="name" /></td>
					<td><spring:message code="values" /></td>
					<td><spring:message code="description" /></td>
					<td><spring:message code="defaultvalue" /></td>
				</tr>
			</thead>
			<c:forEach var="d" items="${systemDefaultTypes}">
				<tr>
					<td>
						<cwms:besttranslation translations="${d.getSystemDefault().defaultNameTranslation}"/>
					</td>
					<td>
						<div class="values">
						<c:forEach var="value" items="${d.getValues()}">
							<span>${value}</span>
						</c:forEach>
						</div>
					</td>
					<td>
						<cwms:besttranslation translations="${d.getSystemDefault().defaultDescriptionTranslation}"/>
					</td>
					<td>
						${d.getSystemDefault().getDefaultValue()}
				</tr>
			</c:forEach>
		</table>
	</jsp:body>
</t:crocodileTemplate>