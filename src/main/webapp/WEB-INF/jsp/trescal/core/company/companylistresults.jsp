<%-- File name: /trescal/core/company/companylistresults.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>

	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="companylistsearch.results.headtext"/></span>
	</jsp:attribute>
	
	 <jsp:body>
        <!-- infobox div containing invoice results -->						
		<div class="infobox" id="companyListSearchResults">
			<form:form action="" method="post" modelAttribute="companylistform">
				<form:hidden path="name" />
				<form:hidden path="activestatus" />
				
				<t:showResultsPagination rs="${companylistform.rs}" pageNoId="" />
				<table class="default2" summary="This table lists all company list search results">
					<thead>
						<tr>
							<td colspan="7">
								<spring:message code="companylist.results"/>  
								(${companylistform.rs.results.size()}) 
								<span class="float-right" > ${allocatedCompany.value} </span>
							</td>
						</tr>
						<tr>
							<th class="companylistname" scope="col"> <spring:message code="companylist.name" /> </th>
							<th  class="active" scope="col"> <spring:message code="companylistsearch.activestatus" /> </th>
						</tr>
					</thead>
					<tbody>
						<c:set var="rowcount" value="0"/>
						<c:choose>
						 	<c:when test="${companylistform.rs.results.size() < 1}">
							<tr>
								<td colspan="7" class="bold center">
									<spring:message code="noresults"/>
								</td>
							</tr>
							</c:when> 
							<c:otherwise> 												
							 <c:forEach var="companylist" items="${companylistform.rs.results}"> 
							 	<tr>
							 		<td class="companylistname"> 
							 			<a href="<spring:url value='viewcompanylist.htm?id=${companylist.id}'/>" class="mainlink">${companylist.name}</a>
							 		</td>
							 		<td class="active">
							 			<c:choose>
											<c:when test="${companylist.active}"><spring:message code="yes"/></c:when>
											<c:otherwise><spring:message code="no" /></c:otherwise>
										</c:choose>
							 		</td>
							 	</tr>
							 	<c:set var="rowcount" value="${rowcount + 1}"/>
							 </c:forEach>
							 </c:otherwise>
						</c:choose>
					</tbody>
				</table>
				<t:showResultsPagination rs="${companylistform.rs}" pageNoId="" />
			</form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>
