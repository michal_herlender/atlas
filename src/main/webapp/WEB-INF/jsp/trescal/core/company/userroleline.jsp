<%-- File name: /trescal/core/company/contactedit.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<tr id="userRoleRow${userRole.userRoleId}">
	<td>${userRole.companyName}</td>
	<td>${userRole.subdivName}</td>
	<td>${userRole.roleName}</td>
	<td style="text-align: center;">
		<img src="img/icons/delete.png" height="20" width="20" alt="<spring:message code='contactedit.deleteuserrole'/>" title="<spring:message code='contactedit.deleteuserrole'/>" onClick="deleteNewUserRight(${userRole.userRoleId})"/>
	</td>
</tr>