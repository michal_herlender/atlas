<%-- File name: /trescal/core/company/companylistsearch.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="companylist.browser"/></span>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox" >
			<div class="float-right"><a href="addcompanylist.htm" class="mainlink"><spring:message code="companylist.add"/></a></div>
			<br>
			<form:form action="" method="post" modelAttribute="companylistform" id="searchcompanylistform">
				<fieldset>
					<legend><spring:message code="companylistsearch.headtext" /></legend>
					<li>
						<label for="orgid"><spring:message code="businesscompany" />:</label> 
						<span>${allocatedCompany.value}</span>
					</li>
					<li>
						<label for="name"><spring:message code="companylistsearch.listname" />:</label> 
						<form:input path="name"/>
					</li>
					<li>
						<label for="activestatus"><spring:message code="companylistsearch.activestatus" />:</label>
						<form:select path="activestatus">
							<option value=""><spring:message code='certsearch.any'/></option>
							<option value="true" selected><spring:message code="yes" /></option>
							<option value="false"><spring:message code="no" /></option>
						</form:select>
					</li>
					<li>
						<label>&nbsp;</label>
						<input type="submit" name="submit" value="<spring:message code='search' />" tabindex="6" />
					</li>
				</fieldset>
			</form:form>
			<div class="clear-height"></div>
		</div>
	</jsp:body>
</t:crocodileTemplate>