<%-- File name: /trescal/core/company/addcompany.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image" %>

<script src='script/trescal/core/company/AddCompany.js'></script>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="company.add"/></span>
	</jsp:attribute>
	<jsp:body>
		<style>
			 label {width:auto;}
		</style>
		<t:showErrors path="addcompanyform.*" showFieldErrors="false"/>
		<div id="addcompany" class="infobox">
			<form:form method="post" action="" modelAttribute="addcompanyform">
				<fieldset>
					<legend><spring:message code="company.add"/></legend>
					<ol>
						<li>
							<div class="displaycolumn-25">
								<label for="coname">
									<image:globalSetting/>
									<spring:message code="company.name"/>:
								</label>
							</div>
							<div class="float-left">
								<form:input path="coname" id="coname" size="45"/>
							</div>
							<form:errors path="coname" delimiter="; " cssClass="attention"/>
							<div class="clear"></div>
						</li>
						<li>
							<div class="displaycolumn-25">
								<label for="coname">
									<image:globalSetting/>
									<spring:message code="company.legalid"/>:
								</label>
							</div>
							<div class="float-left">
								<form:input path="legalIdentifier" id="coname" size="45"/>
							</div>
							<form:errors path="legalIdentifier" delimiter="; " cssClass="attention"/>
							<div class="clear"></div>
						</li>
						<li>
							<div class="displaycolumn-25">
								<label for="subdivName">
									<image:globalSetting/>
									<spring:message code="subdivision"/>:
								</label>
							</div>
							<div class="float-left">
								<form:input path="subdivName"/>
							</div>
							<form:errors path="subdivName" delimiter="; " cssClass="attention"/>
							<div class="clear"></div>
						</li>
						<li>
							<div class="displaycolumn-25">
								<label for="siretNumber">
									<image:globalSetting/>
									<spring:message code="subdiv.siretnumber"/>:
								</label>
							</div>
							<div class="float-left">
								<form:input path="siretNumber"/>
							</div>
							<form:errors path="siretNumber" delimiter="; " cssClass="attention"/>
							<div class="clear"></div>
						</li>
						<li>
							<div class="displaycolumn-25">
								<label for="corole">
									<image:globalSetting/>
									<spring:message code="company.role"/>:
								</label>
							</div>
							<div class="float-left">
								<form:select path="companyRole" items="${coroles}" itemLabel="message" onchange="event.preventDefault(); changeRole($j(this));"/>
							</div>
							<form:errors path="companyRole" delimiter="; " cssClass="attention"/>
							<div class="clear"></div>
						</li>
						<li>
							<div class="displaycolumn-25">
								<label for="country">
									<image:globalSetting/>
									<spring:message code="company.country"/>:
								</label>
							</div>
							<div class="float-left">
								<form:select path="countryId" items="${countries}" itemValue="key" itemLabel="value"/>
							</div>
							<form:errors path="countryId" delimiter="; " cssClass="attention"/>
							<div class="clear"></div>
						</li>
						<li>
							<div class="displaycolumn-25">
								<label for="currency">
									<image:businessCompanySpecific/>
									<spring:message code="currency"/>:
								</label>
							</div>
							<div class="float-left">
								<form:select path="currencyId" items="${currencies}" itemValue="key" itemLabel="value"/>
							</div>
							<form:errors path="currencyId" delimiter="; " cssClass="attention"/>
							<div class="clear"></div>
						</li>
						<c:set var="visForClient" value="hid"/>
						<c:if test="${addcompanyform.companyRole.name() == 'CLIENT'}">
							<c:set var="visForClient" value="vis"/>
						</c:if>
						<li id="client" class="${visForClient}">
							<div class="displaycolumn-25">
								<label for="busArea">
									<image:globalSetting/>
									<spring:message code="company.sector"/>:
								</label>
							</div>
							<div class="float-left">
								<form:select path="businessAreaId">
									<form:option value=""><spring:message code="addcompany.defaultsector" /></form:option>
									<c:forEach var="ba" items="${businessAreas}">
										<form:option value="${ba.key}">
											${ba.value}
										</form:option>
									</c:forEach>
								</form:select>
							</div>
							<form:errors path="businessAreaId" delimiter="; " cssClass="attention"/>
							<div class="clear"></div>
						</li>
						<c:set var="visForBusiness" value="hid"/>
						<c:if test="${addcompanyform.companyRole.name() == 'BUSINESS'}">
							<c:set var="visForBusiness" value="vis"/>
						</c:if>
						<li id="business" class="${visForBusiness}">
							<div class="displaycolumn-25">
								<label for="companyCode">
									<image:globalSetting/>
									<spring:message code="company.code"/>:
								</label>
							</div>
							<div class="float-left">
								<form:input path="companyCode"/>
							</div>
							<form:errors path="companyCode" delimiter="; " cssClass="attention"/>
							<div class="clear"></div>
						</li>
						<li>
							<div class="displaycolumn-25">
								<label for="submit">&nbsp;</label>
							</div>
							<div class="float-left">
								<input type="submit" name="submit" id="submit" value="<spring:message code='company.add'/>" />
							</div>
							<div class="clear"></div>
						</li>
					</ol>							
				</fieldset>
			</form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>