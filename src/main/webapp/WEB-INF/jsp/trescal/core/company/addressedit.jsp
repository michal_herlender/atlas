<%-- File name: /trescal/core/company/addressedit.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="address.addedit"/> (${command.address.sub.subname})</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/company/AddressEdit.js'></script>
	</jsp:attribute>
	<jsp:body>
		<style>
			 label {width:auto;}
		</style>
		<c:set var="escapedName"><spring:escapeBody javaScriptEscape="true">${command.address.addr1}</spring:escapeBody></c:set>
		<c:if test="${!command.address.active}">
			<div class="warningBox1" id="deactivewarning">
				<div class="warningBox2">
					<div class="warningBox3">
						<div class="center-0">
							<spring:message code="address.deactivatemessage1"/>
							<fmt:formatDate type="date" dateStyle="SHORT" value="${command.address.deactTime}"/>.
							<spring:message code="address.deactivatemessage2"/>
							<c:choose>
								<c:when test='${command.address.deactReason.trim() == ""}'>
									<spring:message code="address.deactivatemessage3"/>
								</c:when>
								<c:otherwise>
									${command.address.deactReason}
								</c:otherwise>
							</c:choose>
						</div>
					</div>
				</div>
			</div>
		</c:if>
		<c:if test="${command.address.accountAddress != null}">
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<div class="center-0">
							<spring:message code="address.note1"/> ${command.address.sub.comp.coname} <spring:message code="address.note2"/>
						</div>
					</div>
				</div>
			</div>
		</c:if>
		<t:showErrors path="command.*" showFieldErrors="false"/>
		<div class="listbox">
			<form:form modelAttribute="command" method="post">
				<fieldset>
					<form:input class="hid" path="address.addrid" value="${command.address.addrid}"/>
					<form:input class="hid" path="addressSettingsCompany.id" value="${command.addressSettingsCompany.id}"/>
					<form:input class="hid" id="validator" path="address.validator"/>
					<form:input class="hid" id ="validatedon" path="address.validatedOn"/>
					<legend>
						<spring:message code="address.addeditfor"/>&nbsp;
						<a href="viewsub.htm?subdivid=${command.address.sub.subdivid}">${command.address.sub.subname}</a>
					</legend>
					<ol>
						<li>
							<div class="displaycolumn-25">
								<label><image:globalSetting/><spring:message code="company.active"/></label>
							</div>
							<c:choose>
								<c:when test="${command.address.active}">
									<c:set var="activeClass" value="vis"/>
									<c:set var="inactiveClass" value="hid"/>
								</c:when>
								<c:otherwise>
									<c:set var="activeClass" value="hid"/>
									<c:set var="inactiveClass" value="vis"/>
								</c:otherwise>
							</c:choose>
							<div class="float-left">
								<!-- display active text and link to deactivate an address -->
								<span id="active" class="${activeClass}">
									<spring:message code="company.active"/>
									(<a href="#" class="mainlink" onclick="event.preventDefault(); toggleThickboxContentForActivation('Address', ${command.address.addrid}, '${escapedName}', true, createWarningMessage('deactivate', '${escapedName}'), ${command.address.sub.comp.coid});" title=""><spring:message code="company.deactivate"/></a>)
								</span>
								<!-- display inactive text and link to activate an address -->
								<span id="inactive" class="${inactiveClass}">
									<spring:message code="address.inactive"/>
									(<a href="#" class="mainlink" onclick="event.preventDefault(); toggleThickboxContentForActivation('Address', ${command.address.addrid}, '${escapedName}', false, createWarningMessage('activate', '${escapedName}'), ${command.address.sub.comp.coid});" title=""><spring:message code="company.activate"/></a>)												
								</span>
							</div>
							<div class="clear"></div>	
						</li>
						<li>
							<div class="displaycolumn-25">
								<label for="addr1">
									<image:globalSetting/>
									<spring:message code="address.address"/> 1:
								</label>
							</div>
							<div class="float-left">
								<form:input id="addr1" size="100" path="address.addr1" onchange="negateValidation();"/><br>
								<form:errors path="address.addr1" delimiter="; " cssClass="float-left; error"/>
							</div>
							<div class="clear"></div>	
						</li>
						<li>
							<div class="displaycolumn-25">
								<label for="addr2">
									<image:globalSetting/>
									<spring:message code="address.address"/> 2:
								</label>
							</div>
							<div class="float-left">
								<form:input id="addr2" size="100" path="address.addr2" onchange="negateValidation();"/>
								<form:errors path="address.addr2" delimiter="; " cssClass="float-left; error"/>
							</div>
							<div class="clear"></div>	
						</li>
						<li>
							<div class="displaycolumn-25">
								<label for="addr3">
									<image:globalSetting/>
									<spring:message code="address.address"/> 3:
								</label>
							</div>
							<div class="float-left">
								<form:input id="addr3" size="100" path="address.addr3" onchange="negateValidation();"/>
								<form:errors path="address.addr3" delimiter="; " cssClass="float-left; error"/>
							</div>
							<div class="clear"></div>	
						</li>
						<li>
							<div class="displaycolumn-25">
								<label for="town">
									<image:globalSetting/>
									<spring:message code="address.town"/>:
								</label>
							</div>
							<div class="float-left">
								<form:input id="town" path="address.town" size="30" onchange="negateValidation();"/>
								<form:errors path="address.town" delimiter="; " cssClass="float-left; error"/>
							</div>
							<div class="clear"></div>	
						</li>
						<li>
							<div class="displaycolumn-25">
								<label for="county">
									<image:globalSetting/>
									<spring:message code="address.stateprovinceregion"/>:
								</label>
							</div>
							<div class="float-left">
								<form:input id="county" path="address.county" size="30" onchange="negateValidation();"/>
								<form:errors path="address.county" delimiter="; " cssClass="float-left; error"/>
							</div>
							<div class="clear"></div>	
						</li>
						<li>
							<div class="displaycolumn-25">
								<label for="postcode">
									<image:globalSetting/>
									<spring:message code="address.postcode"/>:
								</label>
							</div>
							<div class="float-left">
								<form:input id="postcode" path="address.postcode" size="10" onchange="negateValidation();"/>
								<form:errors path="address.postcode" delimiter="; " cssClass="float-left; error"/>
							</div>	
							<div class="clear"></div>	
						</li>
						<li>												
							<div class="displaycolumn-25">
								<label for="country">
									<image:globalSetting/>
									<spring:message code="address.country"/>:
								</label>
							</div>
							<div class="float-left">
								<form:select path="countryId" items="${countrylist}" itemValue="key" itemLabel="value" onchange="negateValidation();"/>
								<form:errors path="countryId" delimiter="; " cssClass="float-left; error"/>
							</div>
							<div class="clear"></div>
						</li>
						<li>
							<div class="displaycolumn-25">
								<form:label path="address.latitude">
									<image:globalSetting/>
									<spring:message code="address.latitude"/>:
								</form:label>
							</div>
							<div class="float-left">
								<form:input id="latitude" path="address.latitude" pattern="[\-]?[0-9]{0,3}([.][0-9]{0,6})?"/>
							</div>
							<div class="clear"></div>
						</li>
						<li>
							<div class="displaycolumn-25">
								<form:label path="address.longitude">
									<image:globalSetting/>
									<spring:message code="address.longitude"/>:
								</form:label>
							</div>
							<div class="float-left">
								<form:input id="longitude" path="address.longitude" pattern="[\-]?[0-9]{0,3}([.][0-9]{0,6})?"/>
							</div>
							<div class="clear"></div>
						</li>
						<li>												
							<div class="displaycolumn-25">
								<label for="phone">
									<image:globalSetting/>
									<spring:message code="company.tel"/>:
								</label>
							</div>
							<div class="float-left">
								<form:input path="address.telephone" />
								<form:errors path="address.telephone" delimiter="; " cssClass="float-left; error"/>
							</div>
							<div class="clear"></div>	
						</li>
						<li>												
							<div class="displaycolumn-25">
								<label for="fax">
									<image:globalSetting/>
									<spring:message code="company.fax"/>:
								</label>
							</div>
							<div class="float-left">
								<form:input path="address.fax" />
								<form:errors path="address.fax" delimiter="; " cssClass="float-left; error"/>
							</div>
							<div class="clear"></div>	
						</li>
						<li>
							<div class="displaycolumn-25">
								<label>
									<image:globalSetting/>
									<spring:message code="company.addresstypes"/>:
								</label>
							</div>
							<div class="float-left">
								<form:checkboxes cssClass="float-left" path="addressTypes" items="${addresstypes}" itemLabel="type" delimiter="<br/>"/>
								<form:errors path="address.addressType" delimiter="; " cssClass="float-left; error"/>
							</div>
							<div class="clear"></div>
						</li>
						<li>
							<div class="displaycolumn-25">
								<label for="transportin">
									<image:businessCompanySpecific/>
									<spring:message code="address.transportin"/>:
								</label>
							</div>
							<div class="float-left">
								<spring:bind path="transportInId">
									<select id="${status.expression}" name="${status.expression}">
										<c:set var="selectedText" value="" />
										<c:if test="${command.transportInId == null}">
											<c:set var="selectedText" value="selected" />
										</c:if>
										<option value="0" ${selectedText}>
											<spring:message code="company.nonespecified"/>
										</option>
										<c:forEach var="t" items="${transportoptions}">
											<c:set var="selectedText" value="" />
											<c:if test="${(command.transportInId != null) && (command.transportInId == t.id)}">
												<c:set var="selectedText" value="selected" />
											</c:if>
											<option value="${t.id}" ${selectedText}>
												<c:choose>
													<c:when test="${not empty t.localizedName}">
														${t.localizedName}
													</c:when>
													<c:otherwise>
														<t:showTranslationOrDefault translations="${t.method.methodTranslation}" defaultLocale="${defaultlocale}"/>
													</c:otherwise>
												</c:choose>
												<c:if test="${t.sub != null}"> - ${t.sub.subname}</c:if>
											</option>
										</c:forEach>
									</select>
								</spring:bind>
							</div>
							<div class="clear"></div>	
						</li>
						<li>
							<div class="displaycolumn-25">
								<label for="transportout">
									<image:businessCompanySpecific/>
									<spring:message code="address.transportout"/>:
								</label>
							</div>
							<div class="float-left">
								<spring:bind path="transportOutId">
									<select id="${status.expression}" name="${status.expression}">
										<c:set var="selectedText" value="" />
										<c:if test="${command.transportOutId == null}">
											<c:set var="selectedText" value="selected" />
										</c:if>
										<option value="0" ${selectedText}>
											<spring:message code="company.nonespecified"/>
										</option>
										
										<c:forEach var="t" items="${transportoptions}">
											<c:set var="selectedText" value="" />
											<c:if test="${(command.transportOutId != null) && (command.transportOutId == t.id)}">
												<c:set var="selectedText" value="selected" />
											</c:if>
											<option value="${t.id}" ${selectedText}>
												<c:choose>
													<c:when test="${not empty t.localizedName}">
														${t.localizedName}
													</c:when>
													<c:otherwise>
														<t:showTranslationOrDefault translations="${t.method.methodTranslation}" defaultLocale="${defaultlocale}"/>
													</c:otherwise>
												</c:choose>
												<c:if test="${t.sub != null}"> - ${t.sub.subname}</c:if>
											</option>
										</c:forEach>
									</select>
								</spring:bind>
							</div>	
							<div class="clear"></div>	
						</li>
						<li>
							<div class="displaycolumn-25">
								<label for="vatOverride">
									<image:businessCompanySpecific/>
									<spring:message code="address.vatratefrom"/>:
								</label>
							</div>
							<div class="float-left">
								<spring:message var="companyLabel" code="company"/>
								<form:radiobutton
									label="${companyLabel}" 
									cssClass="float-left" 
									path="overrideCompanyVatRate" 
									name="${status.expression}" 
									value="false" 
									onchange="event.preventDefault(); changeVatOverride($j(this));"/>
								<br/>
								<spring:message var="addressLabel" code="address"/>
								<form:radiobutton 
									label="${addressLabel}"
									cssClass="float-left" 
									path="overrideCompanyVatRate" 
									name="${status.expression}" 
									value="true" 
									onchange="event.preventDefault(); changeVatOverride($j(this));"/>
							</div>
							<div class="float-left">
								<form:errors path="overrideCompanyVatRate" delimiter="; " cssClass="float-left; error"/>
							</div>
							<div class="clear"></div>	
						</li>
						<c:set var="visVatCode" value="hid"/>
						<c:if test="${command.overrideCompanyVatRate}">
							<c:set var="visVatCode" value="vis"/>
						</c:if>
						<li id="vatcode" class="${visVatCode}">
							<div class="displaycolumn-25">
								<label for="vatcode">
									<image:businessCompanySpecific/>
									<spring:message code="company.vatrate"/>:
								</label>
							</div>
							<div class="float-left">
								<form:select path="overrideVatCode" id="overrideVatCode">
									<c:forEach var="vatGroup" items="${vatRates.keySet()}">
										<optgroup label="${vatGroup}">
											<form:options items="${vatRates.get(vatGroup)}" itemLabel="value" itemValue="key" /> 
										</optgroup>
									</c:forEach>
								</form:select>
								<form:errors path="overrideVatCode" delimiter="; " cssClass="float-left; error"/>
							</div>
							<div class="clear"></div>
						</li>
						<li>
							<div class="displaycolumn-25">
								<label for="costCentre">
									<image:globalSetting/>
									<spring:message code="address.costcentre"/>:
								</label>
							</div>
							<div class="float-left">
								<form:input path="address.costCentre" size="20"/>
								<form:errors path="address.costCentre" delimiter="; " cssClass="float-left; error"/>
							</div>
							<div class="clear"></div>	
						</li>
						<c:if test="${addressprinters.size() > 0}">
							<li>
								<spring:bind path="printerId">
									<div class="displaycolumn-25">
										<label for="${status.expression}">
											<image:globalSetting/>
											<spring:message code="address.defaultprinter"/>:
										</label>
									</div>
									<div class="float-left">
										<select id="${status.expression}" name="${status.expression}">
											<option value="0"><spring:message code="address.defaultnone"/></option>
											<c:forEach var="printer" items="${addressprinters}">
												<option value="${printer.id}" ${command.address.defaultPrinter != null && command.address.defaultPrinter.id == printer.id ? "selected" : ""}>
													${printer.description}
												</option>
											</c:forEach>
										</select>
										<div class="clear"></div>
									</div>
								</spring:bind>
								<div class="clear"></div>
							</li>
						</c:if>
						<c:if test="${labelprinters.size() > 0}">
							<li>
								<div class="displaycolumn-25">
									<label for="${status.expression}">
										<image:globalSetting/>
										<spring:message code="address.defaultlabelprinter"/>:
									</label>
								</div>
								<div class="float-left">
									<spring:bind path="labelPrinterId">
										<select id="${status.expression}" name="${status.expression}">
											<option value="0"><spring:message code="address.defaultnone"/></option>
											<c:forEach var="printer" items="${labelprinters}">
												<option value="${printer.id}" ${command.address.defaultLabelPrinter != null && command.address.defaultLabelPrinter.id == printer.id ? "selected" : ""}>
													${printer.description}
												</option>
											</c:forEach>
										</select>
									</spring:bind>
								</div>
								<div class="clear"></div>
							</li>
						</c:if>
						<li>
							<div class="displaycolumn-25">
								<label for="submit">&nbsp;</label>
							</div>
							<c:if test="${avalaraAware}">
								<div class="float-left">
									<input type="submit" name="validate" value="<spring:message code='failurereport.validate'/>" id="validate" onclick="event.preventDefault(); validateAddress();" />
								</div>
							</c:if>
							<div class="float-left">
								<input type="submit" name="submit" value="<spring:message code='company.submit'/>" id="submit" />
							</div>
							<div class="clear"></div>
						</li>
					</ol>
				</fieldset>
			</form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>