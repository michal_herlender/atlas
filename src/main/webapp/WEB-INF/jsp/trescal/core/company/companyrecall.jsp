<%-- File name: /trescal/core/company/companyrecall.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>


<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="companyrecall.title"/></span>
	</jsp:attribute>
	<jsp:body>
		<t:showErrors path="command.*" showFieldErrors="false"/>
		<fieldset>
			<legend><spring:message code="companyrecall.title"/></legend>
			<form:form modelAttribute="command" action="" method="post">
				<ol>
					<li>
						<label class="width30"><spring:message code="company"/>:</label>
						<span><links:companyLinkDWRInfo company="${company}" rowcount="0" copy="true"/></span>
					</li>
					<li>
						<label class="width30"><spring:message code="companyrecall.active"/>:</label>
						<form:radiobutton path="active" value="true" /> <spring:message code="true"/> 
						<form:radiobutton path="active" value="false" /> <spring:message code="false"/> 
						<span class="attention"><form:errors path="active" cssStyle="error"/></span>
					</li>
					<li>
						<label class="width30"><spring:message code="companyrecall.recallperiodstarts"/>:</label>
						<form:select path="periodStart" items="${periodStartTypes}" /> 
						<span class="attention"><form:errors path="periodStart" cssStyle="error"/></span>
					</li>
					<li>
						<label class="width30"><spring:message code="companyrecall.futuremonths"/>:</label>
						<form:input path="futureMonths" /> <spring:message code="month"/> 
						<span class="attention"><form:errors path="futureMonths" cssStyle="error"/></span>
					</li>
					<li>
						<label class="width30"><spring:message code="companyrecall.customtext"/>:</label>
						<form:textarea path="customText" cols="80" rows="15" /> 
						<span class="attention"><form:errors path="customText" cssStyle="error"/></span>
					</li>
					<li>
						<label>&nbsp;</label>
						<input type="submit" name="submit" class="submit" value="<spring:message code="company.save"/>" />
					</li>
				</ol>
			</form:form>
		</fieldset>
	</jsp:body>
</t:crocodileTemplate>