<%-- File name: /trescal/core/company/fragments/addresseelect.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<select name="${htmlSelectName}" id="${htmlSelectName}">
	<c:forEach var="a" items="${options}">
		<option value="${a.key}" ${selectedOptionId == a.key ? 'selected' : ''}>
			<c:out value="${a.value}" />
		</option>
	</c:forEach>
</select>