<%-- File name: /trescal/core/company/addcompanylistusage.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<form:form action="addcompanylistusage.htm" id="addnewcompanylistusage" method="post" modelAttribute="editcompanylistform">
	<form:input type='hidden' path='companylistId' value='${companylist.id}'/>
	<fieldset> 
		<ol>
			<li>
				<center> <strong><c:out value="${companylist.name}" /></strong></center>
			</li>
			
			<li>
				<label><spring:message code="companysearch.companyroles"/>:</label>  
				<span><spring:message code="companyrole.client"/>,
					<spring:message code="companyrole.business"/>
				</span>
			</li>
			
			<li>
				<label for="userCoid"><spring:message code="companylist.select.user"/>:</label> 
				<br><form:select path="userCoid" items="${possibleusers}" itemValue="key" itemLabel="value"/>
			</li>

		</ol>

	</fieldset>
</form:form>