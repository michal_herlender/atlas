<%-- File name: /trescal/core/company/companyeditfinance.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="company.editfinancesettings"/></span>
	</jsp:attribute>
	<jsp:body>
		<script>
			var focusElement = "legalIdentifier";
		</script>
		<style>
			 label {width:auto;}
		</style>
		<t:showErrors path="form.*" showFieldErrors="false"/>
		
		<div class="listbox">
			<form:form modelAttribute="form" method="post">
				<fieldset>
					<legend>
						<spring:message code="company.editfinancesettingsforcompany"/>
						<a href="viewcomp.htm?coid=${reference.company.coid}" class="mainlink" >&nbsp;<c:out value="${reference.company.coname}"/></a>
					</legend>
					
					<ol>
						<li>
							<strong>
								<spring:message code="company.generalsettings" />
							</strong>
						</li>
						<li>
							<div class="displaycolumn-25">
								<form:label path="legalIdentifier">
									<image:globalSetting/>
									<spring:message code="company.legalid"/>:
								</form:label>
							</div>
							<div class="float-left">
								<form:input path="legalIdentifier"/>
							</div>
							<form:errors path="legalIdentifier" element="div" delimiter="; " cssClass="float-left; error"/>
							<div class="clear"></div>															
						</li>
						<li>
							<div class="displaycolumn-25">
								<form:label path="fiscalIdentifier">
									<image:globalSetting/>
									<spring:message code="company.fiscalid"/>:
								</form:label>
							</div>
							<div class="float-left">
								<form:input path="fiscalIdentifier"/>
							</div>
							<form:errors path="fiscalIdentifier" element="div" delimiter="; " cssClass="float-left; error"/>
							<div class="clear"></div>															
						</li>
						<li>
							<div class="displaycolumn-25">
								<form:label path="status">
									<image:businessCompanySpecific/>
									<spring:message code="company.status"/>:
								</form:label>
							</div>
							<div class="float-left">
								<form:radiobuttons path="status" cssClass="float-left" element="div" items="${reference.status}"/>
								<form:errors path="status" delimiter="; " cssClass="error"/>
							</div>
							<div class="clear"></div>															
						</li>
						<li>
							<div class="displaycolumn-25">
								<form:label path="active">
									<image:businessCompanySpecific/>
									<spring:message code="company.active"/>:
								</form:label>
							</div>
							<div class="float-left">
								<form:radiobutton path="active" value="true"/>
								<spring:message code="company.active"/>
								<br/>
								<form:radiobutton path="active" value="false"/>
								<spring:message code="company.inactive"/>
								<br/>
								<form:errors path="active" delimiter="; " cssClass="error"/>
							</div>
							<div class="clear"></div>															
						</li>
						<li>
							<div class="displaycolumn-25">
								<form:label path="onStop">
									<image:businessCompanySpecific/>
									<spring:message code="company.onstop"/>:
								</form:label>
							</div>
							<div class="float-left">
								<form:radiobutton path="onStop" value="true"/>
								<spring:message code="company.onstop"/>
								<br/>
								<form:radiobutton path="onStop" value="false"/>
								<spring:message code="company.trusted"/>
								<br/>
								<form:errors path="onStop" delimiter="; " cssClass="error"/>
							</div>
							<div class="clear"></div>															
						</li>
						<li>
							<div class="displaycolumn-25">
								<form:label path="invoiceFrequencyId">
									<image:businessCompanySpecific/>
									<spring:message code="invoicefrequency"/>:
								</form:label>
							</div>
							<div class="float-left">
								<form:select 
										cssClass="float-left"
										id="invoiceFrequencyId" 
										items="${reference.invoicefrequencies}" 
										itemValue="key" 
										itemLabel="value"
										path="invoiceFrequencyId"/>
								<form:errors path="invoiceFrequencyId" delimiter="; " cssClass="error"/>
							</div>
							<div class="clear"></div>															
						</li>
						<li>
							<div class="displaycolumn-25">
								<form:label path="paymentModeId">
									<image:businessCompanySpecific/>
									<spring:message code="paymentmode"/>:
								</form:label>
							</div>
							<div class="float-left">
								<form:select 
										cssClass="float-left"
										id="paymentModeId" 
										items="${reference.paymentmodes}" 
										itemValue="key" 
										itemLabel="value"
										path="paymentModeId"/>
								<form:errors path="paymentModeId" delimiter="; " cssClass="error"/>
							</div>
							<div class="clear"></div>															
						</li>
						<li>
							<div class="displaycolumn-25">
								<form:label path="paymentTerm">
									<image:businessCompanySpecific/>
									<spring:message code="paymentterms"/>:
								</form:label>
							</div>
							<div class="float-left">
								<form:select 
										cssClass="float-left" 
										id="paymentTerm"  
 										items="${reference.paymentterms}"     
 										path="paymentTerm"/> 
								<form:errors path="paymentTerm" delimiter="; " cssClass="error"/> 
							</div>
							<div class="clear"></div>															
						</li>
						<li>
							<div class="displaycolumn-25">
								<form:label path="invoiceFactoring">
									<image:businessCompanySpecific/>
									<spring:message code="factoring"/>:
								</form:label>
							</div>
							<div class="float-left">
								<form:checkbox path="invoiceFactoring"/>
								<form:errors path="invoiceFactoring" delimiter="; " cssClass="error"/>
							</div>
							<div class="clear"></div>
						</li>
						<%-- **** Start of settings specific by CompanyRole settings **** --%>
						<li>
							<div class="displaycolumn-25">
								<strong>
									<spring:message code="company.companysettings" /> - ${reference.company.companyRole.getMessage()}
								</strong>
							</div>
							<div class="clear"></div>														
						</li>
						<c:if test="${reference.company.companyRole.name() == 'BUSINESS'}">
							<%-- CompanyRole.BUSINESS only --%>
							<li>
								<div class="displaycolumn-25">
									<form:label path="companyCode">
										<image:globalSetting/>
										<spring:message code="company.code"/>:
									</form:label>
								</div>
								<div class="float-left">
									<form:input path="companyCode" size="10"/>
									<form:errors path="companyCode" delimiter="; " cssClass="error"/>
								</div>
								<div class="clear"></div>															
							</li>
							<li>
								<div class="displaycolumn-25">
									<form:label path="companyCapital">
										<image:globalSetting/>
										<spring:message code="company.capital"/>:
									</form:label>
								</div>
								<div class="float-left">
									<form:input path="companyCapital" size="40"/>
									<form:errors path="companyCapital" delimiter="; " cssClass="error"/>
								</div>
								<div class="clear"></div>															
							</li>
							<li>
								<div class="displaycolumn-25">
									<form:label path="useTaxableOptionOnPurchaseOrders">
										<image:globalSetting/>
										<spring:message code="company.useTaxableOptionOnOurchaseOrders"/>
									</form:label>
								</div>
								<div class="float-left">
									<form:checkbox path="useTaxableOptionOnPurchaseOrders"/>
								</div>
								<div class="clear"></div>
							</li>
							<li>
								<div class="displaycolumn-25">
									<form:label path="defaultQuotationDuration">
										<image:globalSetting/>
										<spring:message code="company.defaultquotationduration"/>:
									</form:label>
								</div>
								<div class="float-left">
									<form:input path="defaultQuotationDuration" size="10"/>
									<spring:message code="days"/>
									<form:errors path="defaultQuotationDuration" delimiter="; " cssClass="error"/>
								</div>
								<div class="clear"></div>															
							</li>
							<li>
								<div class="displaycolumn-25">
									<form:label path="hourlyRate">
										<image:globalSetting/>
										<spring:message code="jobcost.hourlyrate"/>:
									</form:label>
								</div>
								<div class="float-left">
									<c:choose>
										<c:when test="${not empty reference.company.currency}">
											<c:out value="${reference.company.currency.currencySymbol}"/>
										</c:when>
										<c:otherwise>
											<c:out value="${reference.defaultcurrency.currencySymbol}"/>
										</c:otherwise>
									</c:choose>
									<form:input path="hourlyRate" size="10"/>
									<form:errors path="hourlyRate" delimiter="; " cssClass="error"/>
								</div>
								<div class="clear"></div>															
							</li>
							<li>
								<div class="displaycolumn-25">
									<form:label path="legalRegistration1">
										<image:globalSetting/>
										<spring:message code="company.legalregistrationline1"/>:
									</form:label>
								</div>
								<div class="float-left">
									<form:input path="legalRegistration1" size="100"/>
									<form:errors path="legalRegistration1" delimiter="; " cssClass="error"/>
								</div>
								<div class="clear"></div>															
							</li>
							<li>
								<div class="displaycolumn-25">
									<form:label path="legalRegistration2">
										<image:globalSetting/>
										<spring:message code="company.legalregistrationline2"/>:
									</form:label>
								</div>
								<div class="float-left">
									<form:input path="legalRegistration2" size="100"/>
									<form:errors path="legalRegistration2" delimiter="; " cssClass="error"/>
								</div>
								<div class="clear"></div>															
							</li>
							<li>
								<div class="displaycolumn-25">
									<form:label path="logo">
										<image:globalSetting/>
										<spring:message code="company.logo"/>:
									</form:label>
								</div>
								<div class="float-left">
									<form:radiobuttons path="logo" cssClass="float-left" delimiter="</br>" options="${reference.logo}"/>
									<form:errors path="logo" delimiter="; " cssClass="error"/>
								</div>
								<div class="clear"></div>															
							</li>
							<li>
								<div class="displaycolumn-25">
									<form:label path="warrantyCalibration">
										<spring:message code="company.warrantyCalibration"/>:
									</form:label>
								</div>
								<div class="float-left">
									<form:input path="warrantyCalibration" size="10"  cssClass="float-left"/><spring:message code="days"/>
									<form:errors path="warrantyCalibration" delimiter="; " cssClass="error"/>
								</div>
							</li>
							<li>
								<div class="displaycolumn-25">
									<form:label path="warrantyRepaire">
										<spring:message code="company.warrantyRepaire"/>:
									</form:label>
								</div>
								<div class="float-left">
									<form:input path="warrantyRepaire"  size="10" cssClass="float-left"/><spring:message code="days"/>
									<form:errors path="warrantyRepaire" delimiter="; " cssClass="error"/>
								</div>
							</li>
							<li>
								<div class="displaycolumn-25">
									<strong>
										<spring:message code="company.bankaccount" /> - <c:out value="${reference.company.companyRole.getMessage()}"/>
									</strong>
								</div>
								<div class="clear"></div>														
							</li>
							<li>
								<div class="displaycolumn-25">
									<form:label path="bankAccountId">
										<image:businessCompanySpecific/>
										<spring:message code="company.bankaccount"/>:
									</form:label>
								</div>
								<div class="float-left">
									<form:radiobuttons 
										cssClass="float-left"
										delimiter="</br>"
										id="bankAccountId" 
										items="${reference.bankaccounts}" 
										itemValue="key" 
										itemLabel="value"
										path="bankAccountId"/>
								</div>
								<div class="float-left">
									<form:errors path="bankAccountId" delimiter="; " cssClass="error"/>
								</div>
								<div class="clear"></div>															
							</li>
							<li>
								<div class="displaycolumn-25">
									<strong>
										<spring:message code="company.companysettings" /> - <spring:message code="company.intersubdivisioncosting" />
									</strong>
								</div>
							</li>
							<li>
								<div class="displaycolumn-25">
									<form:label path="interbranchSalesMode">
										<image:globalSetting/>
										<spring:message code="company.interbranchsalesmode" />:
									</form:label>
								</div>
								<div class="float-left">
									<form:radiobuttons path="interbranchSalesMode" cssClass="float-left" items="${reference.interbranchSalesModes}" />
									<form:errors path="interbranchSalesMode" delimiter="; " cssClass="error"/>
								</div>
								<div class="clear"></div>
							</li>
							<li>
								<div class="displaycolumn-25">
									<form:label path="interbranchDiscountRate">
										<image:globalSetting/>
										<spring:message code="company.interbranchdiscountrate" />:
									</form:label>
								</div>
								<div class="float-left">
									<form:input path="interbranchDiscountRate" size="10"/>
									<c:out value=" %"/>
								</div>
								<div class="float-left">
									<form:errors path="interbranchDiscountRate" delimiter="; " cssClass="error"/>
								</div>
								<div class="clear"></div>
							</li>
							<li>
								<div class="displaycolumn-25">
									<form:label path="interbranchMarkupRate">
										<image:globalSetting/>
										<spring:message code="company.interbranchmarkuprate" />:
									</form:label>
								</div>
								<div class="float-left">
									<form:input path="interbranchMarkupRate" size="10"/>
									<c:out value=" %"/>
								</div>
								<div class="float-left">
									<form:errors path="interbranchMarkupRate" delimiter="; " cssClass="error"/>
								</div>
								<div class="clear"></div>
							</li>
							<li>
								<div class="displaycolumn-25">
									<form:label path="usePriceCatalog">
										<image:globalSetting/>
										<spring:message code="company.usepricecatalog" />:
									</form:label>
								</div>
								<div class="float-left">
									<form:radiobuttons path="usePriceCatalog" items="${reference.booleanDtos}" 
										itemLabel="value" itemValue="key" cssClass="float-left" />
								</div>
								<div class="float-left">
									<form:errors path="usePriceCatalog" delimiter="; " cssClass="error"/>
								</div>
								<div class="clear"></div>
							</li>
						</c:if>
						<%-- **** End of CompanyRole.BUSINESS settings **** --%>
						<li>
							<label for="submit">&nbsp;</label>
							<input type="submit" id="submit" value="<spring:message code='companyedit.updatecompany'/>" />
							<div class="clear"></div>															
						</li>
					</ol>				
			
				</fieldset>
			</form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>