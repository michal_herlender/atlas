<%-- File name: /trescal/core/company/companyedit.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="company" tagdir="/WEB-INF/tags/company"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>



<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<spring:message code="company.edit"/>
		</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/company/CompanyEdit.js'></script>	
		<!-- create a lookup array for the available system currencies -->
		<script type='text/javascript'>
			<c:forEach var="cur" items="${currencies}" varStatus="curStatus">
				currencies[${curStatus.index}] = new currency(${cur.currencyId}, '${cur.currencySymbol}', '${cur.currencyName}', ${cur.defaultRate});
			</c:forEach>
		</script>
	</jsp:attribute>
	<jsp:body>
		<div id="editcompany">
			<t:showErrors path="companyform.*" showFieldErrors="true"/>
			<div class="infobox">
				<form:form modelAttribute="companyform" action="" method="post">
					<fieldset>
						<legend><spring:message code="company.edit"/> <a href="viewcomp.htm?coid=${companyform.coid}" class="mainlink" >&nbsp;<c:out value="${companyform.coname}"/></a></legend>
						<spring:bind path="companyform.lastModified">
							<input type="hidden" name="${status.expression}" value="${status.value}" />
						</spring:bind>
						<ol>
							<li>
								<label for="coname">
									<image:globalSetting/>
									<spring:message code="company.name"/>:
								</label>
								<spring:bind path="companyform.coname">
									<input type="text" onkeyup=" checkForNameChange('${companyform.coname}', this.value); " size="45" name="${status.expression}" value="${status.value}" />
								</spring:bind>
								<form:errors path="coname" cssClass="attention"/>
								&nbsp;
								<c:set var="spanClass" value="vis"/>
								<c:if test="${companyform.oldConame == companyform.coname}">
									<c:set var="spanClass" value="hid"/>
								</c:if>
								<span class="${spanClass}" id="nameChangeFields">
									<spring:bind path="companyform.saveNameChange">
										<c:set var="checkedText" value="vis"/>
										<c:if test="${((companyform.saveNameChange == true) && (companyform.oldConame != companyform.coname))}">
											<c:set var="checkedText" value=" checked='checked'"/>
										</c:if>
										<input type="checkbox" class="checkbox" name="${status.expression}" value="true" ${checkedText} /> 
										<input type="hidden" name="_${status.expression}" value="false" />
									</spring:bind>
									<spring:message code="company.savecurrentname"/> (${companyform.coname}) <spring:message code="company.tohistory"/>?
								</span>
							</li>
							<li>
								<label>
									<image:globalSetting/>
									<spring:message code="company.groupname"/>:
								</label>
								<form:input path="groupName" />
								<form:errors path="groupName" cssClass="attention"/>
							</li>
							<li>
								<label>
									<image:globalSetting/>
									<spring:message code="company.companygroup"/>:
								</label>
								<form:select path="companyGroupId" items="${companyGroups}" itemLabel="value" itemValue="key" />
								<form:errors path="companyGroupId" cssClass="attention"/>
							</li>
							<li>
								<label>
									<image:globalSetting/>
									<spring:message code="company.legacynumber"/>:
								</label>
								<form:input path="formerId" />
								<form:errors path="formerId" cssClass="attention"/>
							</li>
							
							<c:if test="${companyform.companyRole.name() == 'BUSINESS'}">
								<li>
									<label for="companyCode">
										<image:globalSetting/>
										<spring:message code="company.code"/>:
									</label>
									<form:input path="companyCode" />
									<form:errors path="companyCode" cssClass="attention"/>
								</li>
							</c:if>
							<li>
								<label for="corole">
									<image:globalSetting/>
									<spring:message code="company.role"/>:								
								</label>
								<c:choose>
									<c:when test="${companyform.companyRole.isEditable()}">
										<spring:bind path="companyform.companyRole">
											<select name="${status.expression}" id="corole">
												<option value="${companyform.companyRole}">${companyform.companyRole.getMessage()}</option>
												<c:forEach var="ecorole" items="${companyform.companyRole.editableTo()}">
													<c:if test="${ecorole != companyform.companyRole}">
														<option value="${ecorole}">${ecorole.getMessage()}</option>
													</c:if>
												</c:forEach>
											</select>
										</spring:bind>
										${status.errorMessage}
									</c:when>
									<c:otherwise> 
										<span>${companyform.companyRole.getMessage()} [<spring:message code="company.noteditable"/>]</span>													
									</c:otherwise>
								</c:choose>
							</li>											
							<c:if test="${companyform.companyRole.name() == 'CLIENT'}">
								<li>
									<label for="businessareaid">
										<image:globalSetting/>
										<spring:message code="company.sector"/>:
									</label>
									<spring:bind path="companyform.businessareaid">
										<select name="${status.expression}" id="${status.expression}">
											<c:forEach var="businessarea" items="${businessAreas}">
												<c:set var="selectedText" value=""/>
												<c:if test="${companyform.businessareaid == businessarea.key}">
													<c:set var="selectedText" value="selected='selected'"/>
												</c:if>
												<option value="${businessarea.key}" ${selectedText}>
													${businessarea.value}
												</option>
											</c:forEach>
										 </select>
									 </spring:bind>
								</li>
							</c:if>
							<li>
								<label for="countryid">
									<image:globalSetting/>
									<spring:message code="company.country"/>:
								</label>
								<spring:bind path="companyform.countryid">
									<select name="${status.expression}" id="${status.expression}">
										<c:forEach var="country" items="${countries}">
											<c:set var="selectedText" value=""/>
											<c:if test="${companyform.countryid == country.key}">
												<c:set var="selectedText" value="selected='selected'"/>
											</c:if>
											<option value="${country.key}" ${selectedText}>${country.value}</option>
										</c:forEach>
									 </select>
								 </spring:bind>
							</li>
							<li>
								<label>
									<image:globalSetting/>
									<spring:message code="company.primarybusinesscompany"/>:
								</label>
								<div class="float-left">
									<div>
										<c:out value="${defaultBusinessCompanyName}" />
									</div>
									<c:choose>
										<c:when test="${requirePrimaryBusinessChange}" >
											<div>
												<form:checkbox path="changeBusinessContact" onchange=" toggleBusinessContactSelect(this.checked); " />
												<spring:message code="company.changeprimarybusinesscompany" arguments="${allocatedCompanyName}" />
											</div>
										</c:when>
										<c:otherwise>
											<span>
												<form:hidden path="changeBusinessContact" />
											</span>
										</c:otherwise>
									</c:choose>
								</div>
								<div class="clear" />
							</li>
							<li>
								<label>
									<image:globalSetting/>
									<spring:message code="company.primarybusinesscontact"/>:
								</label>
								<c:choose>
									<c:when test="${companyform.changeBusinessContact}">
										<c:set var="selectVisibility" value="vis" />
										<c:set var="displayVisibility" value="hid" />
									</c:when>
									<c:otherwise>
										<c:set var="selectVisibility" value="hid" />
										<c:set var="displayVisibility" value="vis" />
									</c:otherwise>
								</c:choose>
								<div id="businesscontact-select" class="${selectVisibility}">
									<form:select 
											cssClass="float-left"
											id="primaryBusinessContactId" 
											items="${primaryBusinessContacts}" 
											itemValue="key" 
											itemLabel="value"
											path="primaryBusinessContactId"/>
									<form:errors path="primaryBusinessContactId" cssClass="attention"/>
								</div>
								<div id="businesscontact-display" class="${displayVisibility}">
									<c:out value="${defaultBusinessContactName}" />
								</div>
							</li>
							<li>
								<label>
									<image:globalSetting/>
									<spring:message code="company.documentlanguage"/>
                                </label>
                                <form:select path="documentLanguage" items="${locales}" itemValue="key" itemLabel="value"/>
							</li>
							<c:if test="${companyform.companyRole.name() == 'CLIENT' || 
								companyform.companyRole.name() == 'BUSINESS' }">
								<li>
									<label>
										<image:businessCompanySpecific/>
										<spring:message code="company.requiresuppliercompanylist"/>
                                	</label>
                                	<form:radiobutton path="requireSupplierCompanyList" value="true" />
									<spring:message code="true"/>
									<form:radiobutton path="requireSupplierCompanyList" value="false" />
									<spring:message code="false"/>
									<form:errors path="requireSupplierCompanyList" cssClass="attention"/>
							   </li>
							</c:if>
							<li>
								<label class="labelhead bold">
									<spring:message code="company.currency"/>
								</label>
								<span>&nbsp;</span>
							</li>
							<li>
								<label><image:globalSetting/><spring:message code="company.defaultcurrency"/>:</label>
								<spring:bind path="companyform.specificCurrency">
									<c:set var="checkedText" value=""/>
									<c:if test="${not status.value}">
										<c:set var="checkedText" value=" checked='checked'"/>
									</c:if>
									<c:set var="checkedText" value=""/>
									<c:if test="${status.value}">
										<c:set var="checkedText" value=" checked='checked'"/>
									</c:if>
									<input type="radio" name="${status.expression}" value="true" ${checkedText} onclick="toggleCurrencySource(this);"/>
									<spring:message code="companyedit.specifycurrency"/>
								</spring:bind>
								<c:set var="displayCurrency" value="hid"/>
								<c:if test="${companyform.specificCurrency}">
									<c:set var="displayCurrency" value="vis"/>
								</c:if>
							
								<span id="specificcurrency" class="${displayCurrency}">
									<c:set var="match" value="${defaultcurrency.currencyId}"/>
									<c:if test="${not empty companyform.specificCurrencyId}">
										<c:set var="match" value="${companyform.specificCurrencyId}"/>
									</c:if>
									-
									<spring:bind path="companyform.specificCurrencyId">
										<select id="currencyList" name="${status.expression}" onchange="updateRateSymbol(this.value);">
										
										<c:forEach var="cur" items="${currencies}">
											<c:set var="selectedText" value=""/>
											<c:if test="${match == cur.currencyId}">
												<c:set var="selectedText" value=" selected='selected'"/>
											</c:if>
											<option value="${cur.currencyId}" ${selectedText}>
												${cur.currencyCode} - ${cur.currencyName}
												<c:if test="${cur.currencyId == defaultcurrency.currencyId}">
													(System Default)
												</c:if>
											</option>
										</c:forEach>
										</select>
									</spring:bind>
								</span>
							</li>
							<li id="rateli" class="${displayCurrency}">
								<label><image:globalSetting/><spring:message code="companyedit.exchangerate"/></label>
								
								<spring:bind path="companyform.specificRate">
									<c:set var="checkedText" value=""/>
									<c:if test="${!status.value}">
										<c:set var="checkedText" value=" checked='checked'"/>
									</c:if>
									<input type="radio" name="${status.expression}" value="false" ${checkedText}/>
									<spring:message code="companyedit.defaultexchange"/>
								
									<c:set var="checkedText" value=""/>
									<c:if test="${status.value}">
										<c:set var="checkedText" value=" checked='checked'"/>
									</c:if>
									<input type="radio" name="${status.expression}" value="true" ${checkedText}/>
								</spring:bind>
								<spring:message code="companyedit.specifyexchange"/>
								
								<span id="specificrate" class="${displayRate}">
									- ${defaultcurrency.currencySymbol}1 = 
									<span id="currentCurSym"></span>
									<spring:bind path="companyform.specificRateValue">
										<input type="text" name="${status.expression}" value="${status.value}"/>
									</spring:bind>
									<form:errors path="specificRateValue" cssClass="attention"/>
								</span>
							</li>
							<li>
								<label for="vatcode">
									<image:businessCompanySpecific/>
									<spring:message code="company.vatrate"/>:
								</label>
								<form:select path="vatCode" id="vatCode">
									<c:forEach var="vatGroup" items="${vatRates.keySet()}">
										<optgroup label="${vatGroup}">
											<form:options items="${vatRates.get(vatGroup)}" itemLabel="value" itemValue="key" /> 
										</optgroup>
									</c:forEach>
								</form:select>
								 <form:errors path="vatCode" cssClass="attention"/>
							</li>
							<li>
								<label for="taxable">
									<image:businessCompanySpecific/>
									<spring:message code="company.taxable"/>:
								</label>
								<form:radiobutton path="taxable" value="true" />
								<spring:message code="true"/>
								<form:radiobutton path="taxable" value="false" />
								<spring:message code="false"/>
								<form:errors path="taxable" cssClass="attention"/>
							</li>
							<li>
								<label for="company.companyRef">
									<image:businessCompanySpecific/>
									<spring:message code="company.reference"/>:
								</label>
								<form:input path="companyRef"/>
								<form:errors path="companyRef" cssClass="attention"/>
							</li>
							<li>
								<label for="company.contract">
									<image:businessCompanySpecific/>
									<spring:message code="company.contract"/>:
								</label>
								<form:radiobutton path="contract" value="true"/>
								<spring:message code="true"/>
								<form:radiobutton path="contract" value="false"/>
								<spring:message code="false"/>
								<form:errors path="contract" cssClass="attention"/>
							</li>
							<c:choose>
								<c:when test="${usesPlantillas}">
									<li>
										<label for="company.synctoplantillas">
											<image:businessCompanySpecific/>
											<spring:message code="company.synctoplantillas"/>:
										</label>
										<form:radiobutton path="syncToPlantillas" value="true" />
										<spring:message code="true"/>
										<form:radiobutton path="syncToPlantillas" value="false" />
										<spring:message code="false"/>
									</li>
								</c:when>
								<c:otherwise>
									<form:hidden path="syncToPlantillas"/>
								</c:otherwise>
							</c:choose>
							<li>
								<label for="submit">&nbsp;</label>
								<input type="submit" id="submit" value="<spring:message code="companyedit.updatecompany"/>" />
							</li>
						</ol>
					</fieldset>
				</form:form>
			</div>
		</div>
	</jsp:body>
</t:crocodileTemplate>