<%-- File name: /trescal/core/company/contactadd.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="company" tagdir="/WEB-INF/tags/company"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="contactadd.addcontactfor"/>&nbsp;<c:out value="${subdiv.subname}"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/company/ContactAdd.js'></script>
	</jsp:attribute>
	<jsp:body>
		<c:if test="${empty subdiv.addresses}">
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<spring:message code="contactadd.erraddaddress"/>
					</div>
				</div>
			</div>
		</c:if>
		<t:showErrors path="command.*" showFieldErrors="true"></t:showErrors>
		<div class="infobox" id="contactadd">
			<form:form method="post" action="" modelAttribute="command">
			<fieldset>
				<legend><spring:message code="contactadd.addcontactfor"/>&nbsp;<c:out value="${subdiv.subname}"/></legend>
				<ol>
					<li>
						<label><spring:message code="company"/>:</label>
						<span><a href="viewcomp.htm?coid=${subdiv.comp.coid}" class="mainlink">${subdiv.comp.coname}</a></span>
					</li>
					<li>
						<label><spring:message code="subdivision"/>:</label>
						<span><a href="viewsub.htm?subdivid=${subdiv.subdivid}" class="mainlink">${subdiv.subname}</a></span>
					</li>
					<li>
						<label for="title"><spring:message code="company.title"/></label>
						<input list="title-list" name="title" id="title">
						<datalist id="title-list" >
							<c:forEach var="title" items="${titles}">
								<option value="${title.value}" title="${title.value}">${title.value}</option>
							</c:forEach>
						</datalist>
						<form:errors path="title" delimiter="; " cssClass="error" />
					</li>
					<li>
						<label for="firstName"><spring:message code="company.firstname"/>: *</label>
						<form:input path="firstName"/>
						<form:errors path="firstName" delimiter="; " cssClass="error" />
					</li>
					<li>
						<label for="lastName"><spring:message code="company.lastname"/>:</label>
						<form:input path="lastName"/>
						<form:errors path="lastName" delimiter="; " cssClass="error" />
					</li>
					<li>
						<label for="position"><spring:message code="company.position"/>:</label>
						<form:input path="position"/>
						<form:errors path="position" delimiter="; " cssClass="error" />
					</li>
					<li>
						<label for="telephone"><spring:message code="company.tel"/>: *</label>
						<form:input path="telephone"/>
						<form:errors path="telephone" delimiter="; " cssClass="error" />
						
						<spring:message code="company.ext"/>:
						<form:input path="telephoneExt"/>
						<form:errors path="telephoneExt" delimiter="; " cssClass="error" />
					</li>
					<li>
						<label for="mobile"><spring:message code="company.mobile"/>:</label>
						<form:input path="mobile"/>
						<form:errors path="mobile" delimiter="; " cssClass="error" />
					</li>
					<li>
						<label for="fax"><spring:message code="company.fax"/>:</label>
						<form:input path="fax"/>
						<form:errors path="fax" delimiter="; " cssClass="error" />
					</li>
					<li>
						<label for="email"><spring:message code="company.email"/>: *</label>
						<form:input path="email"/>
						<form:errors path="email" delimiter="; " cssClass="error" />
					</li>
					<li>
						<label for="preference"><spring:message code="company.prefs"/>:</label>
						<select name="preference" id="preference">
							<option value="E" ${command.preference == 'E' ? "selected" : ""}><spring:message code="company.email"/></option>
							<option value="F" ${command.preference == 'F' ? "selected" : ""}><spring:message code="company.fax"/></option>
							<option value="L" ${command.preference == 'L' ? "selected" : ""}><spring:message code="company.letter"/></option>
						</select>
						<form:errors path="preference" delimiter="; " cssClass="error" />
					</li>
					<li>
						<label><spring:message code="company.language"/>:</label>
						<form:select path="languageTag" items="${supportedLocales}" itemLabel="value" itemValue="key"/>
						<form:errors path="languageTag" delimiter="; " cssClass="error" />
					</li>
					<c:if test="${subdiv.comp.companyRole == 'BUSINESS'}">
						<li>
							<label for="contacthrid"><spring:message code="company.hrid"/>:</label>
							<form:input id="contacthrid" onkeydown="keepPrefix('${hridcountryprefix}');" path="hrid"  />
							<form:errors path="hrid" delimiter="; " cssClass="error" />
						</li>
					</c:if>
					<li>
						<label class="labelhead bold"><c:out value="${subdiv.subname}"/>&nbsp;<spring:message code="company.addresses"/></label>
						<span>&nbsp;</span>
					</li>
					<c:choose>
						<c:when test="${not empty subdiv.defaultAddress}">
							<c:set var="defaultAddrId" value="${subdiv.defaultAddress.addrid}" />
						</c:when>
						<c:otherwise>
							<c:set var="defaultAddrId" value="0" />
						</c:otherwise>
					</c:choose>
					<c:forEach var="address" items="${addresses}" varStatus="loopStatus">
						<li>
							<label>
								<spring:message code="contactadd.address"/><c:out value=" ${loopStatus.count} " />
								<c:if test="${address.addrid eq defaultAddrId}">(<spring:message code="default"/>)</c:if>
							</label>
							<input type="radio" class="radiobutton" name="addressid" value="${address.addrid}" <c:if test="${address.addrid eq command.addressid}"> checked='checked' </c:if> />
							<c:if test="${not empty address.addr1}">${address.addr1}, </c:if>
							<c:if test="${not empty address.addr2}">${address.addr2}, </c:if>
							<c:if test="${not empty address.addr3}">${address.addr3}, </c:if>
							<c:if test="${not empty address.postcode}">${address.postcode}, </c:if>
							<c:if test="${not empty address.town}">${address.town}, </c:if>
							<c:if test="${not empty address.county}">${address.county}, </c:if>
							<c:if test="${not empty address.country}">${address.country.localizedName}</c:if>
							<div class="clear"></div>
						</li>
					</c:forEach>
					<li>
						<label class="labelhead bold"><spring:message code="company.weblogin"/></label>
					</li>
					<li>
						<label for="addweblogin"><spring:message code="company.addweblogin"/></label>						
						<div class="float-left">
							<form:radiobutton path="createAccount" value="false" onClick=" hideWebLogin(); " /><spring:message code="false"/>
							<form:radiobutton path="createAccount" value="true" onClick=" showWebLogin(); " /><spring:message code="true"/>
						</div>
						<form:errors path="createAccount" delimiter="; " cssClass="error" />
						<div class="clear"></div>
					</li>
					<c:choose>
						<c:when test="${command.createAccount}">
							<c:set var="visibility" value="vis" />
						</c:when>
						<c:otherwise>
							<c:set var="visibility" value="hid" />
						</c:otherwise>
					</c:choose>
					<li class="weblogin ${visibility}">
						<label for="note"><spring:message code="company.note"/>:</label>
						<span><spring:message code="contactadd.auto.assigned"/></span>
					</li>
					<li class="weblogin ${visibility}">
						<label for="username"><spring:message code="contactadd.userloginid"/>:</label>
						<form:input path="username"/>
						<form:errors path="username" delimiter="; " cssClass="error" />
					</li>
					<li class="weblogin ${visibility}">
						<label for="password"><spring:message code="contactadd.password"/>:</label>
						<form:password path="password"/>
						<form:errors path="password" delimiter="; " cssClass="error" />
					</li>
					<li class="weblogin ${visibility}">
						<label for="password2"><spring:message code="contactadd.reenterpassword"/>:</label>
						<form:password path="password2"/>
						<form:errors path="password2" delimiter="; " cssClass="error" />
					</li>
					<li class="weblogin ${visibility}">
						<label for="role"><spring:message code="contactadd.userrole"/>:</label>
						<form:select path="role" items="${roles}" />
					</li>
					<li class="weblogin ${visibility}">
						<label for="groupid"><spring:message code="company.usergroups"/>:</label>
						<form:select path="groupid">
							<c:choose>
								<c:when test="${empty usergroups}">
									<option value=""><spring:message code="company.nousrgrp"/></option>
								</c:when>
								<c:otherwise>
									<c:forEach var="usergroup" items="${usergroups}">
										<c:choose>
											<c:when test="${command.groupid == usergroup.groupid}">
												<c:set var="selectedAttr" value=" selected='selected' " />
											</c:when>
											<c:when test="${(command.groupid == 0) && usergroup.compDefault}">
												<c:set var="selectedAttr" value=" selected='selected' " />
											</c:when>
											<c:when test="${(command.groupid == 0) && usergroup.systemDefault}}">
												<c:set var="selectedAttr" value=" selected='selected' " />
											</c:when>
											<c:otherwise>
												<c:set var="selectedAttr" value="" />
											</c:otherwise>
										</c:choose>
										<option ${selectedAttr} value="${usergroup.groupid}">${usergroup.name}</option>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</form:select>
						<form:errors path="groupid" delimiter="; " cssClass="error" />
					</li>
					<li class="weblogin ${visibility}">
						<label for="loginEmail"><spring:message code="contactadd.sendloginemail"/>:</label>
						<div class="float-left">
							<form:radiobutton path="loginEmail" value="true"/><spring:message code="true"/>
							<form:radiobutton path="loginEmail" value="false"/><spring:message code="false"/>
						</div>
						<form:errors path="loginEmail" delimiter="; " cssClass="error" />
						<div class="clear"></div>
					</li>
					<c:if test="${subdiv.comp.companyRole == 'BUSINESS'}">
						<li class="weblogin ${visibility}">
							<label class="labelhead bold"><spring:message code="contactadd.businessusersettings"/></label>
							<span>&nbsp;</span>
						</li>
						<li class="weblogin ${visibility}">
							<label for="defaultprinter"><spring:message code="company.defaultprinter"/>:</label>
							<form:select path="printerid" items="${printers}" itemLabel="value" itemValue="key" />
							<form:errors path="printerid" delimiter="; " cssClass="error" />
						</li>
					</c:if>
					<li>
						<label for="submit">&nbsp;</label>
						<input type="submit" name="submit" id="submit" value="<spring:message code='contactadd.addcontact' />" />
					</li>
					
				</ol>
		
			</fieldset>
			
			</form:form>
		
		</div>		
	</jsp:body>
</t:crocodileTemplate>