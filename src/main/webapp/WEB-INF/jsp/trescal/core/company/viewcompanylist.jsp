<%-- File name: /trescal/core/company/viewcompanylist.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="companylist.view"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/company/ViewCompanyList.js'></script>
	</jsp:attribute>
	<jsp:body>
		<t:showErrors path="editcompanylistform" showFieldErrors="true" />
		<div class="infobox" >
			<fieldset>
				<legend><spring:message code="companylist"/> : <span class="bold"><c:out value="${companylist.name}" /></span> </legend>
				<div class="displaycolumn">
					<ol>
						<li>
							<label><spring:message code="businesscompany" />:</label>
							<span><c:out value="${companylistBusinessCompany}" /></span>
						</li>
						<li>
							<label><spring:message code="companylist" />:</label>
							<span><c:out value="${companylist.name}" /></span>
						</li>
					</ol>
				</div>
				<div class="displaycolumn">
					<ol>
						<li>
							<label><spring:message code="lastModifiedBy" />:</label>
							<span><c:out value="${companylist.lastModifiedBy.name}" /></span>
						</li>
						<li>
							<label><spring:message code="instmod.lastmodified" />:</label>
							<span><fmt:formatDate type="date" dateStyle="SHORT" value="${ companylist.lastModified }"/></span>
						</li>
					</ol>
				</div>
			
				<div class="clear-height"></div>
			</fieldset>
		</div>
		<div id="subnav">
			<dl>
				<dt><a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'members-tab', false);" id="members-link" title="<spring:message code="companylist.members" />" class="selected"><spring:message code="companylist.members"/></a></dt> 
				<dt><a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'edit-tab', false);" title="<spring:message code="edit" />" id="edit-link"><spring:message code="edit" /></a></dt>
				<dt><a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'usage-tab', false); " id="usage-link" title="<spring:message code="companylist.usage.tab" />" ><spring:message code="companylist.usage.tab" /></a></dt>
			</dl>
		</div>
		
		<div class="infobox">
			<div id="members-tab">
				<div class="float-right">
					<a href="#" class="mainlink" id="showall" onclick=" event.preventDefault(); addNewMember(${companylist.id});"><spring:message code="companylist.member.add" /></a>
				</div>
				<div class="clear"></div>
				<div id="companylistmembers">
					<table class="default4 companylistmembers" summary="Table contains all members of the company list">
						<thead>
							<tr>
								<td colspan="8">
									<spring:message code="companylist.members" /> ( ${members.size()} )
								</td>
							</tr>
							<tr>
								<th class ="member" scope="col"><spring:message code="companylist.member"/></th>
								<th class ="companyname" scope="col"><spring:message code="company.name"/></th>
								<th class ="active" scope="col"><spring:message code="active"/></th>
								<th class ="active" scope="col"><spring:message code="lastModifiedBy"/></th>
								<th class ="delete" scope="col"><spring:message code="delete"/></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="member" items="${members}" varStatus="loop">
								<tr>
									<td class ="member">${loop.count}</td>
									<td class ="companyname" > 
										<links:companyLinkDWRInfo company="${member.company}" rowcount="0" copy="true" />
									</td>
									<td class ="active" >
										<c:choose>
											<c:when test="${member.active}"><spring:message code="yes"/></c:when>
											<c:otherwise><spring:message code="no" /></c:otherwise>
										</c:choose>
									</td>
									<td class ="active" >${member.lastModifiedBy.name}</td>
									<td class ="delete" >
										<cwms:securedJsLink permission="COMPANY_LIST_MEMBER_DELETE" classAttr="mainlink" collapse="True" onClick="event.preventDefault(); deleteCompanyListMember(${member.id})">
												<img src="img/icons/delete.png" width="16" height="16" alt="<spring:message code="delete"/>" title="<spring:message code="delete"/>" />
										</cwms:securedJsLink>
									</td>
								</tr>
							</c:forEach>
							
						</tbody>
					</table>
				</div>
			</div>
			
			<div id="edit-tab" class="hid">
				<form:form action="editcompanylist.htm" class="editCompanyListForm" method="post" modelAttribute="editcompanylistform">
					<input type="hidden" name="compnaylistid" value="${companylist.id}"/>
					<fieldset>
						<legend><spring:message code="companylist.edit" /></legend>
						<ol>
							<li>
								<label for="name"><spring:message code="companylistsearch.listname" />:</label>
								<c:choose>
									<c:when test="${not empty errorMessage}">
										<form:input path="name" value="${editcompanylistform.name}" size="50"/>
										<span class="error">${errorMessage}</span>
									</c:when>
									<c:otherwise>
										<form:input path="name" value="${companylist.name}" size="50"/>
									</c:otherwise>
								</c:choose>
							</li>
							<li>
								<label for="activestatus"><spring:message code="companylistsearch.activestatus" />:</label>
								<form:select path="activestatus">
									<option value="true" <c:if test="${companylist.active}"> selected </c:if>><spring:message code="yes"/></option>
									<option value="false" <c:if test="${!companylist.active}"> selected </c:if>><spring:message code="no"/></option>
								</form:select>
							</li>
							<li>
								<label>&nbsp;</label>
								<input type="submit" id="submit" value="<spring:message code='delnotesummary.inputupdate' />" />
							</li>
						</ol>
					</fieldset>
				</form:form>
			
			</div>
			
			<div id="usage-tab" class="hid">
				<div class="float-right">
					<a href="#" class="mainlink" id="showall" onclick=" event.preventDefault(); addNewCompanyListUsage(${companylist.id});"><spring:message code="companylist.usage.add" /></a>
				</div>
				<div class="clear"></div>
				<div id="companylistusage">
					<table class="default4 companylistusage" summary="Table contains all companies using the list">
						<thead>
							<tr>
								<td colspan="8">
									<spring:message code="companylist.usage" /> ( ${users.size()} )
								</td>
							</tr>
							<tr>
								<th class ="user" scope="col"><spring:message code="companylist.user"/></th>
								<th class ="companyname" scope="col"><spring:message code="company.name"/></th>
								<th class ="active" scope="col"><spring:message code="lastModifiedBy"/></th>
								<th class ="delete" scope="col"><spring:message code="delete"/></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="user" items="${users}" varStatus="loop">
								<tr>
									<td class ="user">${loop.count}</td>
									<td class ="companyname" > 
										<links:companyLinkDWRInfo company="${user.company}" rowcount="0" copy="true" />
									</td>
									<td class ="active" >${user.lastModifiedBy.name}</td>
									<td class ="delete" >
										<cwms:securedJsLink permission="COMPANY_LIST_USAGE_DELETE" classAttr="mainlink" collapse="True" onClick="event.preventDefault(); deleteCompanyListUsage(${user.id}, ${companylist.id})">
												<img src="img/icons/delete.png" width="16" height="16" alt="<spring:message code="delete"/>" title="<spring:message code="delete"/>" />
										</cwms:securedJsLink>
									</td>
								</tr>
							
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</jsp:body>
</t:crocodileTemplate>