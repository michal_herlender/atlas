<%-- File name: /trescal/core/company/supplier.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script type='text/javascript' src='script/trescal/core/company/Supplier.js'></script>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="company.supplierapprovaltypes"/></span>
	</jsp:attribute>
	<jsp:body>
	<form:form method="post" modelAttribute="form">

	   <div class="infobox companySupplierForm">
	
		<fieldset>
		
			<legend><spring:message code="company.supplierapprovaltypes"/></legend>
			
			<ol>
				<li>
					<label><spring:message code="company.supplierapprovaltype"/></label>
					<spring:message code="active"/>
				</li>
				<c:forEach var="supplier" items="${suppliers}">
					<li>
						<label>${supplier.approvalType.getName()}:</label>				
						<form:checkbox path="activeSupplierIds[${supplier.supplierid}]" value="true"/>
					</li>
				</c:forEach>
				<li>
					<label>&nbsp;</label>
					<input type="submit" name="submit" value="<spring:message code="submit"/>"/>
				</li>
			</ol>
		
		</fieldset>								
	
	</div>
	
</form:form>
	</jsp:body>
</t:crocodileTemplate>