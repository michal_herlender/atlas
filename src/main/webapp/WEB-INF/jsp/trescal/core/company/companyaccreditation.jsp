<%-- File name: /trescal/core/company/companyaccreditation.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="company.editsupplierapproval"/></span>
	</jsp:attribute>
	<jsp:body>
		<script type='text/javascript'>
// 			var jqCalendar = new Array (); 
// 			<c:forEach var="supplier" items="${command.suppliers}">
// 			jqCalendar.push({inputField: 'companyAccreditationDate${supplier.supplierid}', dateFormat: 'dd.mm.yy', displayDates: 'future' });
// 			</c:forEach>
		</script>
		
	 	<div class="infobox">
			<form:form action="" method="post" modelAttribute="form">
			
				<form:errors path="*">
                     		<div class="warningBox1">
                        		<div class="warningBox2">
                            		<div class="warningBox3">
                                		<h5 class="center-0 attention">
                                    		<spring:message code='error.save' />
                                		</h5>
                                		<c:forEach var="e" items="${messages}">
                                    		<div class="center attention"><c:out value="${e}"/></div>
                                		</c:forEach>
                            		</div>
                        		</div>
                     		</div>
                 </form:errors>
                 
				<input type="hidden" name="coid" value="${command.company.coid}">
				<fieldset>
					<legend><spring:message code="company.editsupplierapproval"/></legend>
					<label>
						<spring:message code="company.name"/>
					</label>
					<span>
						<links:companyLinkDWRInfo company="${command.company}" rowcount="0" copy="0"></links:companyLinkDWRInfo>
					</span>
					<table class="default2">
						<thead>
							<tr>
								<td colspan="4"><spring:message code="company.supplierapprovals"/></td>
							</tr>
							<tr>
								<th><spring:message code="company.approvaltype"/></th>
								<th><spring:message code="active"/></th>
								<th><spring:message code="company.expiryreviewdate"/></th>
								<th><spring:message code="company.reason"/></th>
							</tr>
						</thead>
						
						<tfoot>
							<tr>
								<td colspan="4">
									<input type="submit" name="submit" class="submit" value="<spring:message code="company.save"/>" />
								</td>
							</tr>
						</tfoot>
						
						<tbody>
							<c:forEach var="supplier" items="${command.suppliers}" varStatus="forEachStatus">
								<c:set var="rowClass" value="odd"/>
								<c:if test="${forEachStatus.index % 2 == 0}">
									<c:set var="rowClass" value="even"/>
								</c:if>
								<tr class="${rowClass}">
									<td>
										<image:businessCompanySpecific/>
										<spring:message code="${supplier.approvalType.messageCode}"/>
									</td>
									<td>
										<c:set var="checkedText" value=""/>
										<c:if test="${form.supplierIds.contains(supplier.supplierid)}">
											<c:set var="checkedText" value="checked"/>
										</c:if>
										<form:checkbox path="supplierIds" class="checkbox" value="${supplier.supplierid}" checked="${checkedText}"/>
									</td>
									<td>
										
										<c:choose>
											<c:when test="${supplier.doesNotExpire}">
												<spring:message code="companyedit.na"/>	
											</c:when>
											<c:otherwise>
												<form:input path="companyAccreditationDate[${supplier.supplierid}]" id="companyAccreditationDate${supplier.supplierid}" type="date" />
											</c:otherwise>
										</c:choose>
									</td>
									<td>
										<c:if test="${not supplier.allowUse}">
											<form:input path="companyAccreditationReason[${supplier.supplierid}]" size="50" />
										</c:if>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</fieldset>	
			</form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>