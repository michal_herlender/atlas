<%-- File name: /trescal/core/company/company.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address" %>
<%@ taglib prefix="company" tagdir="/WEB-INF/tags/company" %>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="logistics" tagdir="/WEB-INF/tags/logistics" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="company.companyinformation"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script>
			var coid = ${company.coid};
		</script>
		<script src='script/thirdparty/jQuery/jquery.js' ></script>
		<script src='script/thirdparty/jQuery/jquery.dataTables.min.js' ></script>
		<script src='script/thirdparty/jQuery/diacritics-sort.js' ></script>
		<script type='text/javascript' src='script/trescal/core/company/Company.js'></script>
		<script type='module' src='script/components/cwms-translate/cwms-translate.js'></script>
		<script type='module' src='script/components/cwms-ef/cwms-ef.js'></script>
		<script type='module' src='script/components/cwms-upload-file/cwms-upload-file.js'></script>
	</jsp:attribute>
	<jsp:attribute name="globalElements">
		<cwms-upload-file system-component-id="${sc.componentId}" entity-id1="${company.coid}" entity-id2=""
 			files-name="${filesName}" rename-to="" web="false"></cwms-upload-file>
	</jsp:attribute>
	<jsp:body>
		
		<form:form id="subdivform" action="" method="post">
			<div class="hid">
				<input type="hidden" id="subdivid" name="subdivid" value="" />
			</div>
		</form:form>

		<c:choose>
			<c:when test="${settings.active}">
				<c:set var="warningBoxClass" value="hid"/>
			</c:when>
			<c:otherwise>
				<c:set var="warningBoxClass" value="vis"/>
			</c:otherwise>
		</c:choose>
		
		<div id="warningBoxInactive" class="${warningBoxClass}">
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<div class="center-0">
							${company.coname}
							<spring:message code="company.deactivatewarning1"/>
							<span id="deactDateSpan">
								<c:if test="${not empty settings.deactDate}">
									<fmt:formatDate type="date" dateStyle="medium" value="${settings.deactDate}"/>
								</c:if>
							</span>
							<spring:message code="company.deactivatewarning2"/>
							<c:choose>
								<c:when test="${empty settings.deactReason}">
									<c:set var="deactReasonSpan" value="hid"/>
									<c:set var="noDeactReasonSpan" value="vis"/>
								</c:when>
								<c:otherwise>
									<c:set var="deactReasonSpan" value="vis"/>
									<c:set var="noDeactReasonSpan" value="hid"/>
								</c:otherwise>
							</c:choose>
							<span id="noDeactReason" class="${noDeactReasonSpan}">
								<spring:message code="company.deactivatewarning3"/>
							</span>
							<span id="deactReason" class="${deactReasonSpan}">
								${settings.deactReason}
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<c:set var="escapedName"><spring:escapeBody javaScriptEscape="true">${company.coname}</spring:escapeBody></c:set>
		
		<c:if test="${settings.onStop}">
			<div id="onstop" class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<div class="center-0">
							${company.coname}
							<spring:message code="company.onstopwarning1"/>
							<c:if test="${not empty settings.onStopBy}">
								&nbsp;&nbsp;(<spring:message code="company.onstopwarning2"/>
								${settings.onStopBy.name}
								<spring:message code="company.onstopwarning3"/>
								<fmt:formatDate type="date" dateStyle="SHORT" value="${settings.onStopSince}" />
							</c:if>
							<c:if test="${updateOnstopActive}">
								&nbsp;&nbsp;
								<spring:message code="company.revert"/>
								<a href="#" class="mainlink" onclick="toggleContentForOnstop('Set Company To Trusted', '${escapedName}', ${company.coid}, ${currentContact.personid}, true, createOnstopWarningMessage('trusted', '${escapedName}')); return false;" title="">
									<spring:message code="company.trusted"/>
								</a>
							</c:if>
						 	<br/>
							<span class="bold">${settings.onStopReason}</span>										 	
						 </div>
					</div>
				</div>
			</div>
		</c:if>

		<!-- infobox div with nifty corners applied -->
		<div class="infobox">
			<fieldset>

				<legend>${company.coname}</legend>
				
				<!-- displays content in column 49% wide (left) -->
				<div class="displaycolumn">
				
					<ol>
						<li>
							<label class="width40"><spring:message code="company.legalid"/>:</label>
							<span>${company.legalIdentifier}</span>
						</li>
						<li>
							<label class="width40"><spring:message code="company.fiscalid"/>:</label>
							<span>${company.fiscalIdentifier}</span>
						</li>
						<li>
							<label class="width40"><spring:message code="company.legacynumber"/>:</label>
							<span>${company.formerID}</span>
						</li>
						<li>
							<label class="width40"><spring:message code="company.legaladdress"/>:</label>
							<span>
								<c:if test="${legaladdresses.size() != 1}">
									<spring:message code="company.haslegaladdresses" arguments="${legaladdresses.size()}"/><br/>
								</c:if>
								<c:forEach var="a" items="${legaladdresses}">
											<cwms:securedLink permission="ADDRESS_VIEW" classAttr="mainlink" parameter="?addrid=${a.addrid}">
										<c:out value="${a.addr1}" />
										<c:if test="${not empty a.addr2}"><c:out value=", ${a.addr2}" /></c:if>
										<c:if test="${not empty a.addr3}"><c:out value=", ${a.addr3}" /></c:if>
										<c:if test="${not empty a.town}"><c:out value=", ${a.town}" /></c:if>
										<c:if test="${not empty a.county}"><c:out value=", ${a.county}" /></c:if>
									</cwms:securedLink>
								</c:forEach>
							</span>
						</li>
						<li>
							<label class="width40">
								<image:businessCompanySpecific/>
								<spring:message code="company.status"/>:
							</label>
							<span>${settings.status.getMessage()}</span>
						</li>
						<li>
							<label class="width40">
								<image:businessCompanySpecific/>
								<spring:message code="company.active"/>:
							</label>

							<c:set var="classActive" value="hid"/>
							<c:set var="classInactive" value="vis"/>
							<c:if test="${settings.active}">
								<c:set var="classActive" value="vis"/>
								<c:set var="classInactive" value="hid"/>
							</c:if>

							<!-- display active text and link to deactivate a company -->
							<span id="active" class="${classActive}">
								<spring:message code="company.active"/>
								<c:if test="${updateOnstopActive}">
									(<cwms:securedJsLink permission="COMPANY_CHANGE_ACTIVE" classAttr="mainlink" onClick="event.preventDefault(); toggleThickboxContentForActivation('Company', ${company.coid}, '${escapedName}', true, createActivationWarningMessage('deactivate', '${escapedName}'), null); return false; " >
										<spring:message code="company.deactivate"/>
									</cwms:securedJsLink>)
								</c:if>
							</span>
							
							<!-- display inactive text and link to activate a company -->
							<span id="inactive" class="${classInactive}">
								<spring:message code="company.inactive"/>
								<c:if test="${updateOnstopActive}">
									(<cwms:securedJsLink permission="COMPANY_CHANGE_ACTIVE" classAttr="mainlink"  onClick="toggleThickboxContentForActivation('Company', ${company.coid}, '${escapedName}', false, createActivationWarningMessage('activate', '${escapedName}'), null); return false;"  >
										<spring:message code="company.activate"/>
									</cwms:securedJsLink>)
								</c:if>
							</span>
						</li>
						<li>
							<label class="width40">
								<image:businessCompanySpecific/>
								<spring:message code="company.onstop"/>:
							</label>

							<c:set var="classTrusted" value="vis"/>
							<c:set var="classOnstop" value="hid"/>
							<c:if test="${settings.onStop}">
								<c:set var="classTrusted" value="hid"/>
								<c:set var="classOnstop" value="vis"/>
							</c:if>

							<!-- display active text and link to deactivate a company -->
							<span id="trusted" class="${classTrusted}">
								<spring:message code="company.trusted"/>
								<c:if test="${updateOnstopActive}">
									(<cwms:securedJsLink permission="COMPANY_ON_STOP_TRUSTED" classAttr="mainlink"  onClick="toggleContentForOnstop('Set Company To On-stop', '${escapedName}', ${company.coid}, ${currentContact.personid}, false, createOnstopWarningMessage('onstop', '${escapedName}')); return false;"   >
										<spring:message code="company.onstop"/>
									</cwms:securedJsLink>)
								</c:if>
							</span>

							<!-- display inactive text and link to activate a company -->
							<span id="on-stop" class="${classOnstop}">
								<spring:message code="company.onstop"/>
								<c:if test="${updateOnstopActive}">
									(<cwms:securedJsLink permission="COMPANY_ON_STOP_TRUSTED" classAttr="mainlink"  onClick=" toggleContentForOnstop('Set Company To Trusted', '${escapedName}', ${company.coid}, ${currentContact.personid}, true, createOnstopWarningMessage('trusted', '${escapedName}')); return false;"    >
										<spring:message code="company.trusted"/>
									</cwms:securedJsLink>)
								</c:if>
							</span>
						</li>
						<li>
							<label class="width40">
								<image:businessCompanySpecific/>
								<spring:message code="company.contract"/>:
							</label>
							<span>
								<spring:message code="${settings.contract}"/>
							</span>
						</li>
						<li>
							<label class="width40">
								<image:businessCompanySpecific/>
								<spring:message code="company.vatrate"/>:
							</label>
							
							<!-- display VAT information -->
							<span>
								<c:out value="${vatRate}" />
							</span>
						
						</li>
						<li>
							<label class="width40">
								<image:businessCompanySpecific/>
								<spring:message code="company.taxable"/>:
							</label>
							<span>
								<spring:message code="${settings.taxable}"/>
							</span>
						</li>
					</ol>

				</div>
				<!-- end of left column -->

				<!-- displays content in column 49% wide (right) -->
				<div class="displaycolumn">
					<ol>
						<li>
							<label class="width40"><spring:message code="company.role"/>:</label>
							<span>${company.companyRole.getMessage()}</span>
							<span class="float-right">
								<cwms:securedLink permission="COMPANY_EDIT"  collapse="True"  classAttr="mainlink" parameter="?coid=${company.coid}">
									<spring:message code="company.edit"/>
								</cwms:securedLink>
							</span>
							<div class="clear"></div>							
						</li>
						<li>
							<label class="width40"><spring:message code="company.country"/>:</label>
							<span>${company.country.localizedName}</span>
						</li>
						<li>
							<label class="width40"><spring:message code="company.currency"/>:</label>
							
							<span>
								<c:choose>
									<c:when test="${company.currency != null}">
										${company.currency.currencyCode}
										(${company.currency.currencySymbol})
										<c:choose>
											<c:when test="${company.rate != null}">
												- <spring:message code="company.customrate"/>: ${defaultcurrency.currencySymbol}1 
												= ${company.currency.currencySymbol}${company.rate}
											</c:when>
											<c:otherwise>
												- <spring:message code="company.defaultrate"/>: ${defaultcurrency.currencySymbol}1 
												= ${company.currency.currencySymbol}${company.currency.defaultRate}
											</c:otherwise>
										</c:choose>
									</c:when>
									<c:otherwise>
										<spring:message code="company.systemdefault"/> - ${defaultcurrency.currencyCode} (${defaultcurrency.currencySymbol})
									</c:otherwise>
								</c:choose>
							</span>
						</li>
						<li>
							<label class="width40"><spring:message code="company.created"/>:</label>
							<span>
								<fmt:formatDate value="${company.createDate}" type="date" dateStyle="medium"/>
							</span>
						</li>
						<c:if test="${company.companyRole.name() == 'CLIENT'}">
							<li>
								<label class="width40"><spring:message code="company.sector"/>:</label>
								<span>
									<cwms:besttranslation translations="${company.businessarea.nametranslation}"/>
								</span>
							</li>
						</c:if>
						<c:if test="${company.companyRole.name() == 'BUSINESS'}">
							<li>
								<label class="width40"><spring:message code="company.code"/>:</label>
								<span>
									<c:out value="${company.companyCode}"/>
								</span>
							</li>
						</c:if>
						<li>
							<label class="width40"><spring:message code="company.groupname"/>:</label>
							<span><c:out value="${company.groupName}"/></span>
						</li>
						<li>
							<label class="width40"><spring:message code="company.companygroup"/>:</label>
							<span>
								<c:choose>
									<c:when test="${not empty company.companyGroup}">
										<c:out value="${company.companyGroup.groupName}"/>
									</c:when>
									<c:otherwise>
										<spring:message code="company.nonespecified"/>
									</c:otherwise>
								</c:choose>
							</span>
						</li>
						<li>
							<label class="width40">
								<image:businessCompanySpecific/>
								<spring:message code="company.reference"/>:
							</label>
							<span><c:if test="${not empty settings.companyReference}">${settings.companyReference}</c:if></span>
						</li>
						<c:if test="${usesPlantillas}">
							<li>
								<label class="width40">
									<image:businessCompanySpecific/>
									<spring:message code="company.synctoplantillas"/>:
								</label>
								<span><c:if test="${not empty settings.syncToPlantillas}"><spring:message code="${settings.syncToPlantillas}" /></c:if></span>
							</li>
						</c:if>
						<li>
							<label class="width40"><spring:message code="company.primarybusinesscontact"/>:</label>
							<span>
								<c:if test="${not empty company.defaultBusinessContact}">
									<c:out default="" value="${company.defaultBusinessContact.sub.comp.country.countryCode}"/>
									-
									<c:out default="" value="${company.defaultBusinessContact.sub.comp.companyCode}"/>
									-
									<c:out default="" value="${company.defaultBusinessContact.sub.subdivCode}"/>
									-
									<c:out value="${company.defaultBusinessContact.name}"/>
								</c:if>
							</span>
						</li>
						<li>
							<label class="width40"><spring:message code="company.documentlanguage"/>:</label>
                            <span>
                            	<c:out value="${company.documentLanguage.getDisplayName(rc.locale)}"/>
                            </span>
						</li>
					</ol>
				</div>
				<!-- end of right column -->
				<div class="clear"></div>
				<div class="center">
					<spring:message code="company.instruments"/>: <cwms:securedLink permission="COMPANY_INSTRUMENT_VIEW" classAttr="mainlink" parameter="?coid=${company.coid}">
						${instcount}
					</cwms:securedLink>
					&nbsp;&nbsp;&nbsp;
					<spring:message code="company.activejobs"/>&nbsp;${allocatedSubdiv.value}: <cwms:securedLink permission="JOB_SEARCH" 
					classAttr="mainlink" parameter="?forced=forced&compid=${company.coid}&activeonly=true">
						${activejobcount}
					</cwms:securedLink>
					&nbsp;&nbsp;&nbsp;
					<spring:message code="company.totaljobsin"/>&nbsp;${allocatedSubdiv.value}: <cwms:securedLink permission="JOB_SEARCH" 
					classAttr="mainlink" parameter="?forced=forced&compid=${company.coid}&activeonly=false">
						${totaljobcountbysubdiv}
					</cwms:securedLink>
					&nbsp;&nbsp;&nbsp;
					<spring:message code="company.totaljobs"/>: ${totaljobcount}
					&nbsp;&nbsp;&nbsp;
					<spring:message code="company.quotations"/>: <cwms:securedLink permission="QUOTE_CREATE" 
					classAttr="mainlink" parameter="?forced=forced&compid=${company.coid}&alltime=true">
						${quotecount}
					</cwms:securedLink>
					&nbsp;&nbsp;
					(<spring:message code="company.last2years"/>: <cwms:securedLink permission="QUOTE_CREATE" 
					classAttr="mainlink" parameter="?forced=forced&compid=${company.coid}&yearfilter=2">
						${recentquotecount}
					</cwms:securedLink>)
				</div>
			</fieldset>
		</div>
		
		<!-- infobox div with nifty corners applied -->
		<div class="infobox">
			<!-- div to hold all the tabbed submenu -->
			<div id="tabmenu">

				<!-- tabbed submenu to change between subdivisions and accreditations -->
				<ul class="subnavtab">
					<li><a href="#" id="subdivs-link" onclick="switchMenuFocus(menuElements, 'subdivs-tab', false); return false; " class="selected"><spring:message code="company.subdivs"/> (${company.subdivisions.size()})</a></li>
					<c:set var="accredClass" value="hid"/>
					<c:if test="${company.companyRole.name() == 'SUPPLIER'}">
						<c:set var="accredClass" value="show"/>
					</c:if>
					<li class="${accredClass}"><a href="#" id="accred-link" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'accred-tab', false); "><spring:message code="company.supplierapprovals"/> (${company.companyAccreditations.size()})</a></li>
					<li><a href="#" id="bankaccounts-link" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'bankaccounts-tab', false); "><spring:message code="company.bankaccounts"/> (<span id="mainBankAccountsCount">${bankaccounts.size()}</span>)</a></li>
					<li><a href="#" id="bpo-link" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'bpo-tab', false); "><spring:message code="company.bpos"/> (<span id="mainBPOCount">${bPOsList.size()}</span>)</a></li>
					<li><a href="#" id="files-link" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'files-tab', false); "><spring:message code="company.files"/></a></li>
					<li><a href="#" id="history-link" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'history-tab', false); "><spring:message code="company.history"/></a></li>
					<li><a href="#" id="instructions-link" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'instructions-tab', false); "><spring:message code="company.instructions"/> (${instructions.totalCount})</a></li>
					<li><a href="#" id="settings-link" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'settings-tab', false); "><spring:message code="company.settings"/></a></li>
					<li><a href="#" id="usergroups-link"
						onclick="event.preventDefault(); switchMenuFocus(menuElements, 'usergroups-tab', false); "><spring:message
								code="company.usergroups" /> (${groups.size()})</a></li>
					<li><a href="#" id="finance-link" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'finance-tab', false); "><spring:message code="company.finance"/></a></li>
					<li><a href="#" id="flexiblefields-link" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'flexiblefields-tab', false); "><spring:message code="company.flexiblefields"/></a></li>
					
					<c:set var="env" ><spring:eval expression="@props.getProperty('cwms.config.systemtype')" /></c:set>
<%-- 				<c:if test='${(not empty env) and (env eq "development") }'> --%>
						<c:if test="${company.companyRole == 'CLIENT' or company.companyRole == 'BUSINESS'}">
							<li><a href="#" id="exchangeformat-link" 
								onclick="event.preventDefault(); switchMenuFocus(menuElements, 'exchangeformat-tab', false);document.querySelector('cwms-ef').refresh()">
									<spring:message code="exchangeformat.tabtitle"/> (${countExchangeFormats})
								</a>
							</li>
						</c:if>
<%-- 				</c:if> --%>
				</ul>
				<!-- div to hold all content which is to be changed using the tabbed submenu -->
				<div class="tab-box">
					<!-- div displays all instructions for this company -->
					<div id="instructions-tab" class="hid">
						<company:instructions command="${instructions}"/>
					</div>					
					<!-- end of div to display all instructions -->
					
					<!-- div displays all user groups for this company -->
					<div id="usergroups-tab" class="hid">
						<company:usergroups groups="${groups}" company="${company}" />
					</div>					
					<!-- end of div to display all user groups -->
					
					<!-- div displays all bank accounts for this company -->
					<div id="bankaccounts-tab" class="hid">
						<company:bankaccounts bankaccounts="${bankaccounts}" company="${company}" />
					</div>					
					<!-- end of div to display all user groups -->
					
					<!-- div displays all finance settings for this company -->
					<div id="finance-tab" class="hid">
						<company:finance company="${company}" companysettings="${settings}" updateFinance="${updateFinance}" accountsSoftwareSupported="${accountsSoftwareSupported}" defaultcurrency="${defaultcurrency}"/>
					</div>
					<!-- end of div to display all finance settings -->
					
					<!-- div to display flexible fields -->
					<div id="flexiblefields-tab" class="hid">
						<company:flexiblefields company="${company}" flexiblefields="${flexiblefields}"/>
					</div>
					<!-- div to display flexible fields -->
					
					<c:if test="${company.companyRole == 'CLIENT' or company.companyRole == 'BUSINESS'}">
						<div id="exchangeformat-tab" class="hid">
							<%-- <logistics:showExchangeFormats exchangeformats="${exchangeformatsList}" organisation="${company}" /> --%>
							<cwms-ef company-id="${company.coid}" company-role="${company.companyRole}" ${hasPermisionOnEF ? 'permission' : ''} >
									<h3 style="	color: red;text-align: center"> Please use newer versions of chrome or firefox  </h3>
							</cwms-ef>
						</div>
					</c:if>
					
					<!-- div displays all subdivisions for this company -->
					<div id="subdivs-tab">
						<table class="default2 companySubdivs" summary="<spring:message code="company.messagelistsubdivs"/>">	
							<thead>
								<tr>
									<td colspan="5">
										<spring:message code="company.subdivs"/>: ${company.subdivisions.size()}
									</td>
								</tr>
								<tr>
									<th class="codefault"><spring:message code="company.default"/></th>
									<th class="coname"><spring:message code="subdivision"/></th>
									<th class="dcreated"><spring:message code="company.created"/></th>
									<th class="nocontacts"><spring:message code="company.contacts"/></th>
									<th class="nocontacts"><spring:message code="company.addresses"/></th>
								</tr>
							</thead>
							<tfoot>
								<tr>
								<td colspan="5">
									<cwms:securedLink permission="SUBDIV_ADD" collapse="True" classAttr="mainlink" parameter="?coid=${company.coid}">
										<spring:message code="company.newsubdivision"/>
									</cwms:securedLink>
								</td>
								</tr>
							</tfoot>
							<tbody>
								<c:forEach items="${company.subdivisions}" var="s" varStatus="forEachStatus">
									<c:set var="rowClass" value="odd"/>
									<c:if test="${forEachStatus.index % 2 == 0}">
										<c:set var="rowClass" value="even"/>
									</c:if>
									<tr class="${rowClass}">
										<td class="center">
											<c:choose>
												<c:when test="${company.defaultSubdiv != null && (company.defaultSubdiv.subdivid == s.subdivid)}">
													<a href="#"><img src="img/icons/house_default.png" width="24" height="16" id="defaultSubDiv" alt="<spring:message code="company.defaultsubdiv"/>" title="<spring:message code="company.defaultsubdiv"/>" onclick=" updateCompanySubdivDefault(${company.coid}, ${s.subdivid}, this); return false; " /></a>
												</c:when>
												<c:otherwise>
													<a href="#"><img src="img/icons/house.png" width="24" height="16" alt="<spring:message code="company.makedefaultsubdiv"/>" title="<spring:message code="company.makedefaultsubdiv"/>" onclick=" updateCompanySubdivDefault(${company.coid}, ${s.subdivid}, this); return false; " /></a>
												</c:otherwise>
											</c:choose>
										</td>
										<td class="name">
											<links:subdivLinkDWRInfo subdiv="${s}" rowcount="${forEachStatus.index}"/> 
										</td>
										<td class="center"><fmt:formatDate dateStyle="medium" type="date" value="${s.createDate}"/></td>
										<td class="center">${s.contacts.size()}</td>
										<td class="center">${s.addresses.size()}</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
					<!-- end of div to display all subdivisions -->
					
					<!-- div displays all accreditations for this company -->
					<div id="accred-tab" class="hid">
									
						<table class="default2">
							
							<thead>
								<tr>
									<td colspan="4"><spring:message code="company.supplierapprovals"/> (${accreditations.size()})</td>
								</tr>
								<tr>
									<th><spring:message code="company.approvaltype"/></th>
									<th><spring:message code="company.expiryreviewdate"/></th>
									<th><spring:message code="company.reason"/></th>
									<th><spring:message code="company.emailed"/></th>
								</tr>
							</thead>
							
							<tfoot>
								<tr>
									<td colspan="4">
										<a href="<c:url value="/editcompanyaccreditation.htm?coid=${company.coid}"/>" class="mainlink">
											<spring:message code="company.editsupplierapproval"/>
										</a>
									</td>
								</tr>
							</tfoot>
							
							<tbody>
								<c:choose>
									<c:when test="${accreditations.size() < 1}">
										<tr class="odd">
											<td colspan="4" class="center bold"><spring:message code="company.noaccred"/></td>
										</tr> 
									</c:when>
									<c:otherwise>
										<c:forEach items="${accreditations}" var="accred" varStatus="forEachStatus">
											<c:set var="rowClass" value="odd"/>
											<c:if test="${forEachStatus.index % 2 == 0}">
												<c:set var="rowClass" value="even"/>
											</c:if>
											<tr class="${rowClass}">
												<td>
													<image:businessCompanySpecific/>
													<spring:message code="${accred.supplier.approvalType.messageCode}"/>
												</td>
												<td>
													<c:set var="span" value=""/>
													<c:if test="${accred.expired}">
														<c:set var="span" value="color:red"/>
													</c:if>
													<span style="${span}">
														<c:if test="${not empty accred.expiryDate}">
															<fmt:formatDate type="date" dateStyle="SHORT" value="${accred.expiryDate}"/>
														</c:if>
													</span>
												</td>
												<td><c:out value="${accred.reason}" default=""/></td>
												<td>
													<c:if test="${accred.emailAlert && accred.expired}">
														<spring:message code="company.mailsent"/>
														<fmt:formatDate type="date" dateStyle="SHORT" value="${accred.reminderMailDate}"/>
													</c:if>
												</td>
											</tr>
										</c:forEach>
									</c:otherwise>
								</c:choose>
							</tbody>
						</table>
					</div>
					<!-- end of div to display all accreditations -->
					
					<!-- div displays all default settings for this company -->
					<div id="settings-tab" class="hid">
						<company:recallSettings recallConfig="${recallConfig}" company="${company}" />
						<company:systemDefaults parentEntity="${company}" entityKey="${company.coid}" scope="${defaultrole}" systemdefaults="${systemdefaulttypes}"/> 
					</div>
					<!-- end of div to display all default settings -->
					
					<!-- div displays all blanket purchase orders for this company -->
					<div id="bpo-tab" class="hid">
						<company:bpos  bpos="${bPOsList}" name="${company.coname}"  scope="COMPANY" id="${company.coid}"/>
					</div>
					<!-- end of div to display all blanket purchase orders -->
					
					<!-- div displays all files for this company -->
					<div id="files-tab" class="hid">
						<spring:message code="company.filesfor" var="textFilesFor"/>
						<c:set var="rootTitle" value="${textFilesFor} ${company.coname}"/> 
						<files:showFilesForSC entity="${company}" sc="${sc}" id="${company.coid}" identifier="${company.coid}" ver="" rootFiles="${scRootFiles}" allowEmail="false" isEmailPlugin="false" rootTitle="${rootTitle}" deleteFiles="true" /> 
					</div>
					<!-- end of div to display files -->

					<!-- div displays all name history for this company -->
					<div id="history-tab" class="hid">														
						
						<h5><spring:message code="company.namehistory"/></h5>
						
						<table class="default4 companyPrevNames" summary="<spring:message code="company.historymessage"/>">
							<thead>
								<tr>
									<td colspan="3"><spring:message code="company.previous"/> (${company.histories.size()})</td>
								</tr>
								<tr>
									<th scope="col" class="named"><spring:message code="company.named"/></th>
									<th scope="col" class="until"><spring:message code="company.until"/></th>
									<th scope="col" class="changed"><spring:message code="company.changedby"/></th>
								</tr>
							</thead>
							<tbody>
								<c:choose>
									<c:when test="${company.histories.size() > 0}">
										<c:forEach var="ch" items="${company.histories}">
											<tr>
												<td>${ch.coname}</td>
												<td><fmt:formatDate type="date" dateStyle="SHORT" value="${ch.validTo}"/>
												<td>${ch.changedBy.name}</td>
											</tr>
										</c:forEach>
									</c:when>
									<c:otherwise>
										<tr>
											<td colspan="3">${company.coname}&nbsp;<spring:message code="company.messagenochange"/></td>
										</tr>
									</c:otherwise>
								</c:choose>
							</tbody>
						</table>							
					
					</div>
					<!-- end of div to display name history -->
					
				</div>
				<!-- end of div to hold all tabbed submenu content -->
					
			</div>
			<!-- end of div to hold tabbed submenu -->
			
			<!-- this div clears floats and restores page flow -->
			<div class="clear"></div>
		
		</div>
		<!-- end of tabbed menu -->
	</jsp:body>
</t:crocodileTemplate>