<%-- File name: /trescal/core/company/subdivadd.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<script src='script/trescal/core/company/SubdivAdd.js'></script>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="company.addnewsubdivision"/></span>
	</jsp:attribute>
	<jsp:body>
		<style>
			 label {width:auto;}
		</style>
		<t:showErrors path="command.*" showFieldErrors="false"/>
		<div class="listbox" id="addsubdivision">
			<form:form modelAttribute="command" method="post">
				<fieldset>
					<legend>
						<spring:message code="company.addingsubdivisionfor"/>
						<a href="viewcomp.htm?coid=${command.subdiv.comp.coid}" class="mainlink" title="${command.subdiv.comp.coname}">&nbsp;<c:out value="${command.subdiv.comp.coname} "/></a>
					</legend>
					<ol>
						<li>
							<div class="displaycolumn-25">
								<label><spring:message code="company"/>:</label>
							</div>
							<div class="float-left">
								<span>${command.subdiv.comp.coname}</span>
							</div>
							<div class="clear"></div>	
						</li>
						<li>
							<div class="displaycolumn-25">
								<label><spring:message code="company.role"/>:</label>
							</div>
							<div class="float-left">
								<span>${command.subdiv.comp.companyRole.getMessage()}</span>
							</div>
							<div class="clear"></div>	
						</li>
						<li>
							<div class="displaycolumn-25">
								<label for="subname">
									<image:globalSetting/>
									<spring:message code="company.subdivisionname"/>:
								</label>
							</div>
							<div class="float-left">
								<form:input id="subname" size="75" path="subdiv.subname"/>
								<form:errors path="subdiv.subname" delimiter="; " cssClass="float-left; error"/>
							</div>
							<div class="clear"></div>	
						</li>
						<li>
							<div class="displaycolumn-25">
								<label for="addr1">
									<image:globalSetting/>
									<spring:message code="company.addressline"/> 1:
								</label>
							</div>
							<div class="float-left">
								<form:input id="addr1" size="100" path="address.addr1"/>
								<form:errors path="address.addr1" delimiter="; " cssClass="float-left; error"/>
							</div>
							<div class="clear"></div>	
						</li>
						<li>
							<div class="displaycolumn-25">
								<label for="addr2">
									<image:globalSetting/>
									<spring:message code="company.addressline"/> 2:
								</label>
							</div>
							<div class="float-left">
								<form:input id="addr2" size="100" path="address.addr2"/>
								<form:errors path="address.addr2" delimiter="; " cssClass="float-left; error"/>
							</div>
							<div class="clear"></div>	
						</li>
						<li>
							<div class="displaycolumn-25">
								<label for="addr3">
									<image:globalSetting/>
									<spring:message code="company.addressline"/> 3:
								</label>
							</div>
							<div class="float-left">
								<form:input id="addr3" size="100" path="address.addr3"/>
								<form:errors path="address.addr3" delimiter="; " cssClass="float-left; error"/>
							</div>
							<div class="clear"></div>	
						</li>
						<li>
							<div class="displaycolumn-25">
								<label for="town">
									<image:globalSetting/>
									<spring:message code="town"/>:
								</label>
							</div>
							<div class="float-left">
								<form:input id="town" path="address.town" size="30" />
								<form:errors path="address.town" delimiter="; " cssClass="float-left; error"/>
							</div>
							<div class="clear"></div>	
						</li>
						<li>
							<div class="displaycolumn-25">
								<label for="county">
									<image:globalSetting/>
									<spring:message code="address.stateprovinceregion"/>:
								</label>
							</div>
							<div class="float-left">
								<form:input id="county" path="address.county" size="30" />
								<form:errors path="address.county" delimiter="; " cssClass="float-left; error"/>
							</div>
							<div class="clear"></div>	
						</li>
						<li>
							<div class="displaycolumn-25">
								<label for="postcode">
									<image:globalSetting/>
									<spring:message code="postcode"/>:
								</label>
							</div>
							<div class="float-left">
								<form:input id="postcode" path="address.postcode" size="10" />
								<form:errors path="address.postcode" delimiter="; " cssClass="float-left; error"/>
							</div>
							<div class="clear"></div>	
						</li>
						<li>
							<div class="displaycolumn-25">
								<label for="country">
									<image:globalSetting/>
									<spring:message code="country"/>:
								</label>
							</div>
							<div class="float-left">
								<form:select path="countryId" items="${countrylist}" itemValue="key" itemLabel="value"/>
							</div>
							<div class="clear"></div>	
						</li>										
 						<li>
							<div class="displaycolumn-25">
								<label>
									<image:globalSetting/>
									<spring:message code="company.addresstypes"/>:
								</label>
							</div>
							<div class="float-left">
								<form:checkboxes cssClass="float-left" path="address.addressType" items="${addressTypes}" itemLabel="type" delimiter="<br/>"/>
								<form:errors path="address.addressType" delimiter="; " cssClass="float-left; error"/>
							</div>
							<div class="clear"></div>
						</li>
						<li>
							<div class="displaycolumn-25">
								<label>
									<image:globalSetting/>
									<spring:message code="subdiv.siretnumber"/>:
								</label>
							</div>
							<div class="float-left">
								<form:input path="subdiv.siretNumber"/>
								<form:errors path="subdiv.siretNumber" delimiter="; " cssClass="float-left; error"/>
							</div>
							<div class="clear"></div>
						</li>
						<c:set var="visibleForBusinessCompany" value="hid" />
						<c:if test="${command.subdiv.comp.companyRole == 'BUSINESS'}">
							<c:set var="visibleForBusinessCompany" value="vis" />
						</c:if>
 						<li id="analyticalcenter" class="${visibleForBusinessCompany}">
							<div class="displaycolumn-25">
								<label>
									<image:globalSetting/>
									<spring:message code="subdiv.analyticalcenter"/>:
								</label>
							</div>
							<div class="float-left">
								<form:input path="subdiv.analyticalCenter" size="10" />
								<form:errors path="subdiv.analyticalCenter" delimiter="; " cssClass="float-left; error"/>
							</div>
							<div class="clear"></div>
						</li>
 						<li id="subdivcode" class="${visibleForBusinessCompany}">
							<div class="displaycolumn-25">
								<label>
									<image:globalSetting/>
									<spring:message code="subdiv.code"/>:
								</label>
							</div>
							<div class="float-left">
								<form:input path="subdiv.subdivCode" size="10" />
								<form:errors path="subdiv.subdivCode" delimiter="; " cssClass="float-left; error"/>
							</div>
							<div class="clear"></div>
						</li>
						<li>
							<div class="displaycolumn-25">
								<label for="transportIn">
									<image:businessCompanySpecific/>
									<spring:message code="address.transportin"/>:
								</label>
							</div>
							<div class="float-left">
								<spring:bind path="command.transportInId">
									<select id="${status.expression}" name="${status.expression}">
										<option value="0" selected>
											<spring:message code="company.nonespecified"/>
										</option>
										<c:forEach var="t" items="${transportoptions}">
											<option value="${t.id}">
												<c:choose>
													<c:when test="${not empty t.localizedName}">
														${t.localizedName }
													</c:when>
													<c:otherwise>
														<t:showTranslationOrDefault translations="${t.method.methodTranslation}" defaultLocale="${defaultlocale}"/>
													</c:otherwise>
												</c:choose>
												<c:if test="${t.sub != null}">
													 - ${t.sub.subname}
												</c:if>
											</option>
										</c:forEach>
									</select>
								</spring:bind>
							</div>
							<div class="clear"></div>	
						</li>
						<li>
							<div class="displaycolumn-25">
								<label for="transportOut">
									<image:businessCompanySpecific/>
									<spring:message code="address.transportout"/>:
								</label>
							</div>
							<div class="float-left">
								<spring:bind path="command.transportOutId">
									<select id="${status.expression}" name="${status.expression}">
										<option value="0" selected>
											<spring:message code="company.nonespecified"/>
										</option>
										<c:forEach var="t" items="${transportoptions}">
											<option value="${t.id}">
												<c:choose>
													<c:when test="${not empty t.localizedName}">
														${t.localizedName}
													</c:when>
													<c:otherwise>
														<t:showTranslationOrDefault translations="${t.method.methodTranslation}" defaultLocale="${defaultlocale}"/>
													</c:otherwise>
												</c:choose> 
												<c:if test="${t.sub != null}">
													 - ${t.sub.subname}
												</c:if>
											</option>
										</c:forEach>
									</select>
								</spring:bind>
							</div>
							<div class="clear"></div>	
						</li>
						<li>
							<div class="displaycolumn-25">
								<label for="submit">&nbsp;</label>
							</div>
							<div class="float-left">
								<input type="submit" name="submit" id="submit" value="<spring:message code='company.addsubdivision'/>" />
							</div>
							<div class="clear"></div>	
						</li>
					</ol>
				</fieldset>
			</form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>