<%-- File name: /trescal/core/company/contactedit.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="company" tagdir="/WEB-INF/tags/company"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="company.profileof"/><c:out value=" ${contacteditform.contact.name}" /></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/company/ContactEdit.js'></script>
		<script>
			var contAddressOptions = '';
			<c:forEach var="addr" items="${activeaddresses}">
				contAddressOptions += '<option value="${addr.addrid}"><c:out value="${addr.addr1}"/>, <c:if test="${addr.addr2 != null}"><c:out value="${addr.addr2}"/>,</c:if> <c:out value="${addr.town}"/>, <c:out value="${addr.county}"/></option>';
			</c:forEach>
			var transOptions = '';
			<c:forEach var="transOpt" items="${dailytransport}">
				transOptions += '<option value="${transOpt.id}" <c:if test="${transOpt.id == defaultTransportOptionInId && transOpt.id == defaultTransportOptionOutId}"> selected </c:if>>'+
					'<c:choose><c:when test="${not empty transOpt.localizedName}"><c:out value="${transOpt.localizedName}"/></option></c:when><c:otherwise><cwms:besttranslation translations="${transOpt.method.methodTranslation}" /></c:otherwise></c:choose>';
			</c:forEach>
			<!-- only the allocated subdivision is included -->
			var businessSubdivs = '<option value="${allocatedSubdiv.key}"><c:out value="${allocatedSubdiv.value}"/></option>';
			<!-- prepare subdivision selection on user rights tab -->
			var subdivsOf = {
				<c:forEach var="compDTO" items="${businessSubs.keySet()}" varStatus="loop">
					${compDTO.key}: '<c:forEach var="subDTO" items="${businessSubs.get(compDTO)}"><option value="${subDTO.key}"><c:out value="${subDTO.value}"/></option></c:forEach>'${loop.last ? '' : ','}
				</c:forEach>
			}
		</script>
	</jsp:attribute>
	<jsp:body>
        <c:set var="disableEdit" value=""/>
        <c:if test="${!allowEditUser}">
            <c:set var="disableEdit" value="disabled"/>
        </c:if>
        <t:showErrors path="contacteditform.*" showFieldErrors="true"/>
        <!-- sub navigation -->
        <div id="subnav">
            <dl>
                <dt><a href="" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'profile-tab', true); "
                       id="profile-link" title="<spring:message code='company.viewcontactprofile'/>"
                       class="selected"><spring:message code="company.profile"/></a></dt>
                <dt><a href="" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'accounts-tab', false); "
                       id="accounts-link"
                       title="<spring:message code='company.addacreditcardforcontact'/>"><spring:message
                        code="company.accounts"/></a></dt>
                <c:if test="${allowPasswordEdit}">
                    <dt><a href=""
                           onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'weblogin-tab', false); "
                           id="weblogin-link"
                           title="<spring:message code='company.contactswebsitelogindetails'/>"><spring:message
                            code="company.weblogin"/></a></dt>
                </c:if>
                <dt><a href=""
                       onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'mailgroups-tab', false); "
                       id="mailgroups-link"
                       title="<spring:message code='company.assigncontacttoemailinggroups'/>"><spring:message
                        code="company.mailgroups"/></a></dt>
				<dt>
				<cwms:securedJsLink permission="DEPARTMENT_ADD_EDIT" collapse='true' onClick="event.preventDefault(); switchMenuFocus(menuElements, 'departments-tab', false);" isDisplayNone="true" id="departments-link" >
					<spring:message code="company.departments"/>
				</cwms:securedJsLink>
				</dt>
				<dt><a href="" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'settings-tab', false); " id="settings-link" title="<spring:message code='company.editcontactsettings'/>"><spring:message code="company.settings"/></a></dt>
				<dt><a href="" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'bpo-tab', false); " id="bpo-link" title="<spring:message code='company.addbpoforthiscontact'/>"><spring:message code="company.bpos"/> (${bPOsList.size()})</a></dt>
				<dt><a href="" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'repeatschedules-tab', false); " id="repeatschedules-link" title="<spring:message code='company.assignrepeatscheduleforthiscontact'/>"><spring:message code="company.repeatschedules"/> (<span class="repeatSchedSize">${contacteditform.contact.repeatSchedules.size()}</span>)</a></dt>
				<c:if test="${contacteditform.contact.sub.comp.companyRole == 'BUSINESS'}">
					<dt><a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'userprefs-tab', false);  " id="userprefs-link" title="<spring:message code='company.viewuserpreferences'/>"><spring:message code="company.userprefs"/></a></dt>
				</c:if>
				<c:if test="${contacteditform.contact.capabilityTrainingRecords.size() > 0}">
					<dt><a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'proctraining-tab', false); " id="proctraining-link" title="<spring:message code='company.viewproceduretrainingrecordsforcontact'/>"><spring:message code="company.proceduretraining"/> (${contacteditform.contact.capabilityTrainingRecords.size()})</a></dt>
				</c:if>
				<c:if test="${contacteditform.contact.sub.comp.companyRole == 'BUSINESS' && allowChangeUserRights}">
					<dt><a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'userrights-tab', false); " id="userrights-link" title="<spring:message code='company.viewuserrights'/>"><spring:message code="company.userrights"/></a></dt>
				</c:if>
			</dl>
		</div>
		
		<c:if test="${contacteditform.message}">
			<!-- this success view is displayed when the contact profile has been updated -->
			<div class="successBox1">
				<div class="successBox2">
					<div class="successBox3">
						<div class="text-center">${contacteditform.message}</div>
					</div>
				</div>
			</div>
			<!-- end of the success view -->
		</c:if>
		
		<c:if test="${!contacteditform.contact.active}">
			<div class="warningBox1" id="warningBoxInactive">
				<div class="warningBox2">
					<div class="warningBox3">
						<div class="center-0">
							<c:out value="${contacteditform.contact.name} " />
							<spring:message code="company.hasbeendeactivatedsince"/>
							<c:out value=" " />
							<fmt:formatDate type="date" dateStyle="SHORT" value="${contacteditform.contact.deactTime}"/>.
							<spring:message code="company.reason"/>:
							<c:choose>
								<c:when test="${!contacteditform.contact.deactReason.trim() == \"\"}">
									<i><spring:message code="company.unspecified"/></i>
								</c:when>
								<c:otherwise>
									${contacteditform.contact.deactReason}
								</c:otherwise>	
							</c:choose>
						</div>
					</div>
				</div>
			</div>
		</c:if>
		<c:if test="${companysettings.onStop}">
			<div id="stop" class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<div class="center-0">
							<c:out value="${contacteditform.contact.sub.comp.coname} "/> 
							<spring:message code="company.iscurrentlyonstoptorevertthiscompanytotrustedgoto"/> 
							<a href="viewcomp.htm?coid=${contacteditform.contact.sub.comp.coid}" class="mainlink" >
								${contacteditform.contact.sub.comp.coname}
							</a> 
							<spring:message code="company.profilepage"/>
						</div>
					</div>
				</div>
			</div>
		</c:if>
		
		<div class="infobox" id="contactedit">
            <form:form modelAttribute="contacteditform" method="post" action="">
                <!-- main profile section with contact information -->
                <div id="profile-tab">
                    <fieldset ${disableEdit}>
                        <legend><spring:message code="company.editprofilefor"/>&nbsp;<c:out
                                value="${contacteditform.contact.name}"/></legend>
                        <c:if test="${contacteditform.contact.accountContact != null}">
                            <div id="stop" class="warningBox1">
                                <div class="warningBox2">
                                    <div class="warningBox3">
                                        <div class="center-0">
                                            <strong><spring:message code="company.note"/>:</strong>
                                            <spring:message code="company.thiscontacthasbeensetas"/>
                                                ${contacteditform.contact.sub.comp.coname}<spring:message
                                                    code="company.saccountscontact.changesappliedlinkedtpaccountssoftware"/>.
                                        </div>
									</div>
								</div>
							</div>
						</c:if>
						<ol>
							<li>
								<label><spring:message code="company"/>:</label>
								<span>
									<links:companyLinkDWRInfo company="${contacteditform.contact.sub.comp}" rowcount="${0}" copy="${true}"/>
								</span>
							</li>
						</ol>
						<!-- displays content in column 49% wide (left) -->
						<table style="width:100%">
							<tr>
								<td style="width:40%">
							<label><spring:message code="company.active"/>:</label>
							<spring:message var="jsContactName" javaScriptEscape="true" text="${contacteditform.contact.name}" />
							
							<%-- Regular user can activate/deactivate non-business contacts; must be admin for business contacts --%>
							<c:choose>
								<c:when test="${contacteditform.contact.sub.comp.companyRole != 'BUSINESS' || allowChangeUserRights}">
									<c:choose>
										<c:when test="${contacteditform.contact.active}">
											<c:set var="activeClass" value="vis" />
											<c:set var="inactiveClass" value="hid" />
										</c:when>
										<c:otherwise>
											<c:set var="activeClass" value="hid" />
											<c:set var="inactiveClass" value="vis" />
										</c:otherwise>
									</c:choose>
									
									<!-- display active text and link to deactivate a contact -->
									<span id="active" class="${activeClass}">
										<spring:message code="company.active"/> 
										(<a href="#" class="mainlink" onclick=" event.preventDefault(); toggleThickboxContentForActivation('Contact', ${contacteditform.contact.personid}, '${jsContactName}', true, createWarningMessage('deactivate', '${jsContactName}'), ${contacteditform.contact.sub.comp.coid}); " title="">
											<spring:message code="company.deactivate"/>
										</a>)
									</span>
									<!-- display inactive text and link to activate a contact -->
									<span id="inactive" class="${inactiveClass}">
										<spring:message code="company.inactive"/> 
										(<a href="#" class="mainlink" onclick=" event.preventDefault(); toggleThickboxContentForActivation('Contact', ${contacteditform.contact.personid}, '${jsContactName}', false, createWarningMessage('activate', '${jsContactName}'), ${contacteditform.contact.sub.comp.coid}); " title="">
											<spring:message code="company.activate"/>
										</a>)
									</span>
								</c:when>
								<c:otherwise>
									<c:choose>
										<c:when test="${contacteditform.contact.active}">
											<spring:message code="company.active"/> 
										</c:when>
										<c:otherwise>
											<spring:message code="company.inactive"/> 
										</c:otherwise>
									</c:choose>
								</c:otherwise>
							</c:choose>
						</td>
						<td style="width:60%">
							<label><spring:message code="subdivision"/>:</label>
							<span>
								<c:if test="${companySettings.onStop}">
									<img src="img/icons/flag_red.png" width="12" height="12" alt="Subdivision Company Onstop" title="Subdivision Company Onstop" />
								</c:if>
								<links:subdivLinkDWRInfo rowcount="0" subdiv="${contacteditform.contact.sub}"/>
								(<a href="movecontact.htm?personid=${contacteditform.contact.personid}" class="mainlink">
									<spring:message code="company.changesubdivision"/>
								</a>)
							</span>
						</td>
					</tr>
					<tr>
						<td>
							<label for="title"><spring:message code="title"/>:</label>
							<input list="title-list" name="title" id="title" value="${contacteditform.title}">
							<datalist name="title" id="title-list">
								<c:forEach var="title" items="${titles}">
									<c:choose>
										<c:when test="${title.value == contacteditform.title}">
											<option value="${title.value}" title="${title.value}">${title.value}</option>
										</c:when>
										<c:otherwise>
											<option value="${title.value}" title="${title.value}">${title.value}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</datalist>
							<form:errors path="title" delimiter="; " cssClass="error" />
						</td>
						<td>
							<label><spring:message code="company.instructions"/>:</label>
							<span><a href="viewinstructions.htm?personid=${contacteditform.contact.personid}" class="mainlink">
								<spring:message code="company.contactinstructions"/> (${instructioncount})
							</a></span>
						</td>
					</tr>
					<tr>
						<td>
							<form:label path="contact.firstName"><spring:message code="company.firstname"/>:</form:label>
							<form:input path="contact.firstName"/>
							<form:errors path="contact.firstName"/>
						</td>
						<td>
							<form:label path="contact.mobile"><spring:message code="company.mobile"/>:</form:label>
							<form:input path="contact.mobile"/>
							<form:errors path="contact.mobile"/>
						</td>
					</tr>
					<tr>
						<td>
							<form:label path="contact.lastName"><spring:message code="company.lastname"/>:</form:label>
							<form:input path="contact.lastName"/>
							<form:errors path="contact.lastName"/>
						</td>
						<td>
							<form:label path="contact.fax"><spring:message code="company.fax"/>:</form:label>
							<form:input path="contact.fax"/>
							<form:errors path="contact.fax"/>
						</td>
					</tr>
					<tr>
						<td>
							<form:label path="contact.position"><spring:message code="company.position"/>:</form:label>
							<form:input path="contact.position"/>
							<form:errors path="contact.position" cssClass="error"/>
						</td>
						<td>
							<form:label path="email">
								<c:choose>
									<c:when test="${status.value == \"\"}">
										<spring:message code="email"/>
									</c:when>
									<c:otherwise>
										<a href="mailto:${status.value}" class="mainlink"><spring:message code="email"/></a>:
									</c:otherwise>
								</c:choose>
							</form:label>
							<form:input id="email" path="email"/>
							<form:errors path="email"/>
						</td>
					</tr>
					<tr>
						<td>
							<form:label path="contact.telephone"><spring:message code="company.tel"/>:</form:label>
							<form:input path="contact.telephone"/>
							<form:errors path="contact.telephone"/>
						</td>
						<td>
							<form:label path="contact.preference"><spring:message code="company.preference"/>:</form:label>
							<form:select path="contact.preference">
								<form:option value="E"><spring:message code="email"/></form:option>
								<form:option value="F"><spring:message code="company.fax"/></form:option>
								<form:option value="L"><spring:message code="company.letter"/></form:option>
							</form:select>
						</td>
					</tr>
					<tr>
						<td>
							<form:label path="contact.telephoneExt"><spring:message code="company.ext"/>:</form:label>
							<form:input path="contact.telephoneExt"/>
							<form:errors path="contact.telephoneExt" cssClass="error"/>
						</td>
							<td>
							<label><spring:message code="company.homeaddress"/>:</label>
							<spring:bind path="contacteditform.addressid">
								<select name="${status.expression}" id="${status.expression}" class="width70">
									<c:forEach var="address" items="${addresses}">
										<c:set var="addressText" value=""/>
										<c:if test="${address.addr1 != \"\"}">
											<c:set var="addressText" value="${address.addr1 }, "/>
										</c:if>
										<c:if test="${address.addr2 != \"\"}">
											<c:set var="addressText" value="${addressText} ${address.addr2}, "/>
										</c:if>
										<c:if test="${address.town != \"\"}">
											<c:set var="addressText" value="${addressText} ${address.town}, "/>
										</c:if>
										<c:if test="${address.county != \"\"}">
											<c:set var="addressText" value="${addressText} ${address.county}"/>
										</c:if>
										<c:choose>
											<c:when test="${contacteditform.contact.defAddress.addrid == address.addrid}">
												<option value="${address.addrid}" title="${addressText}" selected>${addressText}</option>
											</c:when>
											<c:otherwise>
												<option value="${address.addrid}" title="${addressText}">${addressText}</option>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</select>
							</spring:bind>
						</td>
					</tr>
					<tr>
						<td>
							<label><spring:message code="contactadd.lastactivity"/>:</label>
							<span>${contacteditform.contact.lastActivity}</span>
							<span class="error">${status.errorMessage}</span>
						</td>
						<td>
							<spring:bind path="contacteditform.locale">
								<label for="${status.expression}"><spring:message code="company.language"/>:</label>
								<select name="${status.expression}" id="${status.expression}" class="width70">
									<option value="">-- <spring:message code="edituserpreferences.nonespecified" /> --</option>
									<c:forEach var="localeOption" items="${locales}">
										<option value="${localeOption}" ${contacteditform.contact.locale != null && localeOption == contacteditform.contact.locale ? "selected" : ""} >
										${localeOption.getDisplayLanguage(rc.locale)}
										(${localeOption.getDisplayCountry(rc.locale)})
										</option>
									</c:forEach>
								</select>
							</spring:bind>
						</td>
					</tr>
					<c:if test="${contacteditform.contact.sub.comp.companyRole == 'BUSINESS'}">
						<tr>
							<td>
								<label><spring:message code="company.userpreferences"/>:</label>
								<span><a href="edituserpreferences.htm?preferenceid=${userpreference.id}" class="mainlink">
										<spring:message code="company.editpreferences"/>
								</a></span>
							</td>
							<td>
								<form:label path="printerid"><spring:message code="company.defaultprinter"/>:</form:label>
								<form:select path="printerid">
									<form:option value="0">-- <spring:message code="company.none"/> --</form:option>
									<form:options items="${printers}" itemValue="key" itemLabel="value"/>
								</form:select>
							</td>
						</tr>
						
						<tr>
							<td>
								<label><spring:message code="company.hrid"/>:</label>
								<form:input id="contacthrid" onkeydown="keepPrefix('${hridcountryprefix}');" path="contact.hrid"  />
								<form:errors path="contact.hrid" delimiter="; " cssClass="error" />
							</td>
						</tr>
						
					</c:if>
				</table>
				<!--  clear floats and restore page flow  -->
				<div class="clear"></div>
				<div class="text-center marg-top">
					<spring:message code="instruments"/>: <a class="mainlink" href="searchinstrument.htm?contactid=${contacteditform.contact.personid}&forced=forced">${instcount}</a>
					&nbsp;&nbsp;&nbsp;<spring:message code="company.activejobs"/> ${allocatedSubdiv.value}: <a class="mainlink" href="jobsearch.htm?forced=forced&contactid=${contacteditform.contact.personid}&activeonly=true">${activejobcount}</a>
					&nbsp;&nbsp;&nbsp;<spring:message code="company.totaljobsin"/>&nbsp;${allocatedSubdiv.value}: <a class="mainlink" href="jobsearch.htm?forced=forced&contactid=${contacteditform.contact.personid}&activeonly=false">${totaljobcountbysubdiv}</a>
                    &nbsp;&nbsp;&nbsp;<spring:message code="company.totaljobs"/>: ${totaljobcount}
                    &nbsp;&nbsp;&nbsp;<spring:message code="quotations"/>: <a class="mainlink"
                                                                              href="createquote.htm?forced=forced&contactid=${contacteditform.contact.personid}&alltime=true">${quotecount}</a>
                    &nbsp;&nbsp;(<spring:message code="company.last2years"/>: <a class="mainlink"
                                                                                 href="createquote.htm?forced=forced&contactid=${contacteditform.contact.personid}&yearfilter=2">${recentquotecount}</a>)
                </div>
                    </fieldset>
                </div>
                <!-- end of main profile section -->

                <!-- contacts account information section -->
                <div id="accounts-tab" class="hid">
                    <fieldset ${disableEdit}>
                        <legend><spring:message code="company.accountsinfo"/></legend>
                        <ol>
                            <c:choose>
                                <c:when test="${contacteditform.contact.creditCard != null}">
                                    <li>
                                        <label><spring:message code="company.creditcard"/>:</label>
                                        <span><a
                                                href="editcreditcard.htm?ccid=${contacteditform.contact.creditCard.ccid}"
                                                class="mainlink">${contacteditform.contact.creditCard.type.name}</a></span>
                                    </li>
                                </c:when>
                                <c:otherwise>
                                    <li>
                                        <label><spring:message code="company.creditcard"/>:</label>
                                        <span aria-labelledby="creditCard"><a
                                                href="addcreditcard.htm?personid=${contacteditform.contact.personid}"
                                                class="mainlink"><spring:message
                                                code="company.addcreditcard"/></a></span>
                                    </li>
                                </c:otherwise>
					</c:choose>
				</ol>
			</fieldset>
		</div>
		<!-- end of contacts account section -->
		
		<!-- contacts weblogin details section -->
                <c:if test="${allowPasswordEdit}">
                    <div id="weblogin-tab" class="hid">
                        <fieldset ${disableEdit}>
                            <legend><spring:message code="company.weblogininformationfor"/><c:out
                                    value=" ${contacteditform.contact.name}"/></legend>
                            <c:choose>
                                <c:when test="${contacteditform.contact.user != null}">
                                    <c:set var="u" value="${contacteditform.contact.user}"/>
                                    <div class="displaycolumn">
                                        <ol>
                                            <li>
                                                <label><spring:message code="company.username"/>:</label>
                                                <span>${u.username}</span>
                                            </li>
									<li>
										<form:label path="groupid"><spring:message code="company.usergroup"/>:</form:label>
										<form:select path="groupid">
											<c:forEach var="usergroup" items="${usergroups}">
												<form:option value="${usergroup.groupid}">
													<c:choose>
														<c:when test="${usergroup.company != null}">
															${usergroup.name}
														</c:when>
														<c:otherwise>
															<cwms:besttranslation translations="${usergroup.nameTranslation}"/>
														</c:otherwise>
													</c:choose>
												</form:option>
											</c:forEach>
										</form:select>
									</li>
									<li>
										<form:label path="contact.user.webAware"><spring:message code="contactedit.accesstocustomerportal"/></form:label>
										<c:choose>
											<c:when test="${allowChangeUserRights}">
												<form:checkbox path="contact.user.webAware"/>
											</c:when>
											<c:otherwise>
												<span><spring:message code="${contacteditform.contact.user.webAware}"/></span>
											</c:otherwise>
										</c:choose>
									</li>
									<c:if test="${isOwnAccount}">
									  	<li>
											<label class="labelhead bold"><spring:message code="profile.changepassword"/>:</label>
										</li>
										<li>
											<form:label path="password"><spring:message code="contactedit.newpassword"/>:</form:label>
											<form:password path="password" showPassword="true"/>
											<form:errors path="password" delimiter="; " cssClass="error" />
										</li>
										<li>
											<form:label path="password2"><spring:message code="contactadd.reenterpassword"/>:</form:label>
											<form:password path="password2" showPassword="true"/>
											<form:errors path="password2" delimiter="; " cssClass="error" />
										</li>
									</c:if>
									<li>
										<form:label path="sendLoginEmail"><spring:message code="contactadd.sendloginemail"/>:</form:label>
										<div class="float-left">
											<form:radiobutton path="sendLoginEmail" value="true"/><spring:message code="true"/>
											<form:radiobutton path="sendLoginEmail" value="false"/><spring:message code="false"/>
										</div>
										<form:errors path="sendLoginEmail" delimiter="; " cssClass="error" />
										<div class="clear"></div>
									</li>
								</ol>
							</div>
							<div class="displaycolumn">
								<ol>
									<li>
										<label><spring:message code="company.registered"/>:</label>
										<span><fmt:formatDate value="${u.registeredDate}" dateStyle="MEDIUM" type="date"/></span>
									</li>
									<li>
										<label><spring:message code="company.lastlogin"/>:</label>
										<span>
											<c:choose>
												<c:when test="${u.lastLogin != null && u.lastLogin == \"\"}">
													<spring:message code="company.nologinsstored"/> 
												</c:when>
												<c:otherwise>
													<fmt:formatDate value="${u.lastLogin}" dateStyle="MEDIUM" type="date"/> (${u.loginCount} <spring:message code="company.times"/>)
												</c:otherwise>
											</c:choose>
										</span>
									</li>
									<li>
										<label><spring:message code="company.email"/>:</label>
										<span>${contacteditform.contact.email}</span>
									</li>
									<li>
										<label><spring:message code="company.passwordreset"/>:</label>
										<span>
											<c:choose>
												<c:when test="${contacteditform.contact.email == \"\"}">
													<spring:message code="company.emailaddressisnotavailableforthiscontact"/>
												</c:when>
												<c:otherwise>
													<a href="#" onclick="event.preventDefault(); resetPassword(${contacteditform.contact.personid});" class="mainlink"><spring:message code="company.emailpasswordreminder"/></a>
												</c:otherwise>
											</c:choose>
										</span>
									</li>
								</ol>
							</div>
							<div class="clear"></div>
						</c:when>
						<c:otherwise>
							<ol>
								<li>
									<label><spring:message code="company.weblogin"/>:</label>
									<span><a href="<c:url value='addlogin.htm?personid=${contacteditform.contact.personid}'/>" class="mainlink"><spring:message code="company.addweblogin"/></a></span>
								</li>
                            </ol>
                        </c:otherwise>
                            </c:choose>
                        </fieldset>
                    </div>
                </c:if>
                <!-- end of contacts weblogin section -->

                <!-- contacts mailgroups section -->
                <div id="mailgroups-tab" class="hid">
                    <fieldset ${disableEdit}>
                        <legend><spring:message code="company.mailgroups"/></legend>
                        <div class="">
                            <ol>
                                <c:choose>
                                    <c:when test="${mailGroupTypes.size() < 1}">
                                        <li>
                                            <div class="center bold"><spring:message
                                                    code="company.nomailgroupstodisplay"/></div>
                                        </li>
                                    </c:when>
                                    <c:otherwise>
								<c:forEach var="mailGroupType" items="${mailGroupTypes}">
									<li>
										<label>${mailGroupType.name}</label>
										<form:checkbox path="mailgroups[${mailGroupType}]" />
										(${mailGroupType.description})
									</li>
								</c:forEach>
                                    </c:otherwise>
                                </c:choose>
                            </ol>
                        </div>
                    </fieldset>
                </div>
                <!-- end of contacts mailgroups section -->

                <!-- contacts department section -->
                <div id="departments-tab" class="hid">
                    <fieldset ${disableEdit}>
                        <legend><spring:message code="company.departments"/></legend>
                        <ol>
                            <c:choose>
                                <c:when test="${departments.size() < 1}">
                                    <li>
                                        <div class="center bold"><spring:message
                                                code="company.nodepartmentstodisplay"/></div>
                                    </li>
                                </c:when>
                                <c:otherwise>
                                    <c:forEach var="editabledep" items="${departments}">
								<c:set var="department" value="${editabledep.department}"/>
								<li>
									<label><a href="department.htm?deptid=${department.deptid}">${department.name}</a></label>
									<spring:bind path="contacteditform.departments['${department.deptid}']">
										<input type="hidden" name="_${status.expression}"/>
										<input type="checkbox" class="checkbox" name="${status.expression}" <c:if test="${contacteditform.departments.get(department.deptid) == \"on\"}">checked="checked"</c:if>/>
									</spring:bind>
									<spring:bind path="contacteditform.departmentroles['${department.deptid}']">
										<select name="${status.expression}">
											<c:set var="contactRole" value="0"/>
											<c:forEach var="role" items="${deparmentroles}">
												<c:forEach var="m" items="${contacteditform.contact.member}">
													<c:if test="${m.department.deptid == department.deptid}">
														<c:set var="contactRole" value="${m.departmentRole.id}"/>
													</c:if>
												</c:forEach>
												<option value="${role.id}" ${contactRole == role.id ? "selected" : ""}>${role.role}</option>
											</c:forEach>
										</select>
										<c:if test="${!editabledep.editable}">
											<span style="color: red;">[<spring:message code="company.noteditable"/>]</span>
										</c:if>
									</spring:bind>
								</li>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</ol>
			</fieldset>
		</div>
		<!-- end of contacts department section -->
		
		<!-- default settings section -->
		<div id="settings-tab" class="hid">
			<company:systemDefaults parentEntity="${contacteditform.contact}" entityKey="${contacteditform.contact.personid}" scope="${defaultrole}" systemdefaults="${systemdefaulttypes}"/>
		</div>
		<!-- end of default settings section -->
		
		<!-- div displays all blanket purchase orders for this contact -->
		<div id="bpo-tab" class="hid">
			<company:bpos  bpos="${bPOsList}" name="${contacteditform.contact.name}"  scope="CONTACT" id="${contacteditform.contact.personid}"/>
		</div>
		<!-- end of div to display all blanket purchase orders -->
		
		<!-- contact's repeat schedule details section -->
		<div id="repeatschedules-tab" class="hid">
			<div id="contactRepeatSchedules">
				<table class="default4" id="repeatScheduleHead" summary="<spring:message code='company.tablerepeatscheduleheadings'/>">
					<thead>
						<tr>
							<td colspan="5"><spring:message code="company.repeatschedulesfor"/>&nbsp;<c:out value="${contacteditform.contact.name}" /> (<span class="repeatSchedSize">${contacteditform.contact.repeatSchedules.size()}</span>)</td>
						</tr>
						<tr>
							<th scope="col" class="transOpt"><spring:message code="company.transportoption"/></th>
							<th scope="col" class="addr"><spring:message code="address"/></th>
							<th scope="col" class="notes"><spring:message code="notes"/></th>
							<th scope="col" class="createby"><spring:message code="company.createdby"/></th>
							<th scope="col" class="active"><spring:message code="company.active"/></th>
						</tr>
					</thead>
				</table>
				<c:choose>
					<c:when test="${contacteditform.contact.repeatSchedules.size() < 1}">
						<table class="default4" summary="<spring:message code='company.tablenorepeatscheduleresults'/>">
							<tbody>
								<tr class="odd">
									<td colspan="5" class="center bold"><spring:message code="company.therearenorepeatschedulesfor"/>&nbsp;<c:out value="${contacteditform.contact.name} " /></td>
								</tr>
							</tbody>
						</table>
					</c:when>
					<c:otherwise>
						<c:set var="rowcount" value="1"/>
						<c:forEach var="rs" items="${contacteditform.contact.repeatSchedules}">
							<table class="default4" summary="<spring:message code='company.tablerepeatschedule'/>">
								<tbody>
									<tr class="headrow">
										<td colspan="5" class="bold">${rs.type.type} - ${rs.organisation.subname}</td>
									</tr>
									<tr class="repeatSched">
										<td class="transOpt">
											<div class="padding5">
												<c:choose>
													<c:when test="${ not empty rs.transportOption.localizedName}">
														${rs.transportOption.localizedName}
													</c:when>
													<c:otherwise>
														<cwms:besttranslation translations="${rs.transportOption.method.methodTranslation}" />
													</c:otherwise>
												</c:choose></div>
										</td>
										<td class="addr">
											<t:showFullAddress address="${rs.addr}" separator="<br/>" copy="${false}"/>
										</td>
										<td class="notes">
											${rs.notes}
										</td>
										<td class="createby">
											${rs.createdBy.name}<br/> 
											(<fmt:formatDate type="date" dateStyle="SHORT" value="${rs.createdOn}"/>)
										</td>
										<td class="active">
											<c:choose>
												<c:when test="${rs.active}">
													<spring:message code="company.active"/><br/>(<a href="#" class="mainlink" onclick=" deactivateRepeatSchedule(${rs.id}, this); return false; "><spring:message code="company.deactivate"/></a>)
												</c:when>
												<c:otherwise>
													<spring:message code="address.deactivated"/><br/>${rs.deactivatedBy.name}<br/>(<fmt:formatDate type="date" dateStyle="SHORT" value="${rs.deactivatedOn}"/>)
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
								</tbody>
							</table>
							<c:set var="rowcount" value="${rowcount + 1}"/>
						</c:forEach>
					</c:otherwise>
				</c:choose>
				<table class="default4" summary="<springMessage code='company.tablefooterrepeatschedules'/>">
					<tfoot>
						<tr>
							<td colspan="6">
								<a href="#" class="domthickbox mainlink-float" onclick="event.preventDefault(); tb_show(i18n.t('core.company:contact.addRepeatSchedule', 'Add Repeat Schedule') , 'TB_dom?width=680&height=350', '', createRepeatScheduleContent('${contacteditform.contact.name}', ${contacteditform.contact.personid})); " title=""><spring:message code="company.addrepeatschedule"/></a>
							</td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
		<!-- end of contact's repeat schedules section -->
		
		<!-- user preferences section -->
		<c:if test="${contacteditform.contact.sub.comp.companyRole == 'BUSINESS'}">
			<div id="userprefs-tab" class="hid">
				<div class="displaycolumn">
					<fieldset class="contactUserPrefs">
						<legend>${contacteditform.contact.name}<spring:message code="company.suserpreferences"/></legend>
						<ol>
							<li>
								<label>&nbsp;</label>
								<span><a href="edituserpreferences.htm?preferenceid=${userpreference.id}" class="mainlink">Edit Preferences</a></span>
							</li>							
							<li>
								<label><spring:message code="company.jobitemkeynavigation"/>:</label>
								<span>
									<c:choose>
										<c:when test="${contacteditform.contact.userPreferences.jiKeyNavigation}">
											<spring:message code="company.on"/>
										</c:when>
										<c:otherwise>
											<spring:message code="company.off"/>
										</c:otherwise>
									</c:choose>
								</span>
							</li>
							<li>												
								<label><spring:message code="company.mouseoveractive"/>:</label>
								<span>
									<c:choose>
										<c:when test="${contacteditform.contact.userPreferences.mouseoverActive}">
											<spring:message code="company.on"/>
										</c:when>
										<c:otherwise>
											<spring:message code="company.off"/>
										</c:otherwise>
									</c:choose>
								</span>
							</li>
							<li>
								<label><spring:message code="company.mouseoverdelay"/>:</label>
								<span><c:out value="${contacteditform.contact.userPreferences.mouseoverDelay} ms"/></span>
							</li>
							<li>												
								<label><spring:message code="company.includeselfinemailreplies"/>:</label>
								<span>
									<c:choose>
										<c:when test="${contacteditform.contact.userPreferences.includeSelfInEmailReplies}">
											<spring:message code="company.on"/>
										</c:when>
										<c:otherwise>
											<spring:message code="company.off"/>
										</c:otherwise>
									</c:choose>
								</span>
							</li>
							<li>												
								<label><spring:message code="company.navbarposition"/>:</label>
								<span>
									<c:choose>
										<c:when test="${contacteditform.contact.userPreferences.navBarPosition}">
											<spring:message code="company.top"/>
										</c:when>
										<c:otherwise>
											<spring:message code="company.side"/>
										</c:otherwise>
									</c:choose>
								</span>
							</li>											
							<li>												
								<label><spring:message code="company.defaultjobtype"/>:</label>
								<span>
									<c:choose>
										<c:when test="${contacteditform.contact.userPreferences.defaultJobType != null}">
											${contacteditform.contact.userPreferences.defaultJobType.description}
										</c:when>
										<c:otherwise>
											<spring:message code="company.nonespecified"/>
										</c:otherwise>
									</c:choose>
								</span>
							</li>
							<li>												
								<label><spring:message code="viewuserpreferences.servicetypeusage"/>:</label>
								<span>
									<c:choose>
										<c:when test="${contacteditform.contact.userPreferences.defaultServiceTypeUsage}">
											<spring:message code="viewuserpreferences.on" />
											<c:out value=" - "/>
											<spring:message code="viewuserpreferences.servicetypefromjobdefault"/>
										</c:when>
										<c:otherwise>
											<spring:message code="viewuserpreferences.off" />
											<c:out value=" - "/>
											<spring:message code="viewuserpreferences.servicetypefromjobtype"/>
										</c:otherwise>
									</c:choose>
								</span>
							</li>
							<li>												
								<label><spring:message code="company.autoprintlabel"/>:</label>
								<span>
									<c:choose>
										<c:when test="${contacteditform.contact.userPreferences.autoPrintLabel}">
											<spring:message code="yes"/>
										</c:when>
										<c:otherwise>
											<spring:message code="no"/>
										</c:otherwise>
									</c:choose>
								</span>
							</li>
						</ol>
					</fieldset>
				</div>
				<div class="displaycolumn">
					<fieldset class="contactUserPrefs">
						<legend><spring:message code="company.instructiontypes"/></legend>
						<ol>
							<c:forEach var="it" items="${instructionTypes}">
								<li>
									<label>${it.type}:</label>
									<c:set var="signedUpForIT" value="${false}"/>
									<c:set var="currentUserInstructionTypeId" value="${0}"/>
									<c:forEach var="uit" items="${contacteditform.contact.userPreferences.userInstructionTypes}">
										<c:if test="${uit.instructionType == it}">
											<c:set var="signedUpForIT" value="${true}"/>
											<c:set var="currentUserInstructionTypeId" value="${uit.id}"/>
										</c:if>
									</c:forEach>
									<span>
										<c:choose>
											<c:when test="${signedUpForIT}">
												<div class="tick" title="<spring:message code='company.viewingthisinstructiontype'/>" onclick=" deleteUserInstructionType(this, ${currentUserInstructionTypeId}, ${userpreference.id}, '${it}'); return false; ">&nbsp;</div>
											</c:when>
											<c:otherwise>
												<div class="cross" title="<spring:message code='company.notviewingthisinstructiontype'/>" onclick=" insertUserInstructionType(this, ${userpreference.id}, '${it}'); return false; ">&nbsp;</div>
											</c:otherwise>
										</c:choose>
									</span>
								</li>
							</c:forEach>
						</ol>
					</fieldset>
				</div>
				<!-- clear floats and restore page flow -->
				<div class="clear"></div>
				<div class="displaycolumn">
					<fieldset class="alligatorPrefs">
						<legend><spring:message code="viewuserpreferences.alligatorpreferences"/></legend>
						<ol>
							<li>												
								<label><spring:message code="company.labelprinter"/>:</label>
								<span>
									<c:choose>
										<c:when test="${contacteditform.contact.userPreferences.labelPrinter != null}">
											${contacteditform.contact.userPreferences.labelPrinter.description}
										</c:when>
										<c:otherwise>
											<spring:message code="company.nonespecified"/>
										</c:otherwise>
									</c:choose>
								</span>
							</li>
							<li>
								<label><spring:message code="edituserpreferences.alligatorprofile"/>:</label>
								<span>
									<c:choose>
										<c:when test="${not empty alligatorSettingsContact}" >
											<spring:message code="edituserpreferences.overrideoncontact"/>
										</c:when>
										<c:otherwise>
											<spring:message code="edituserpreferences.usesubdivisiondefault" />
										</c:otherwise>
									</c:choose>
								</span>							
							</li>
						</ol>
					</fieldset>
				</div>
				<div class="displaycolumn">
					<fieldset class="alligatorPrefs">
						<legend><spring:message code="edituserpreferences.productionprofile"/></legend>
						<ol>
							<li>
								<label><spring:message code="description"/>:</label>
								<span>
									<c:out value="${alligatorSettingsProduction.getDescription()}" />
								</span>							
							</li>
						</ol>
					</fieldset>
					<fieldset class="alligatorPrefs">
						<legend><spring:message code="edituserpreferences.testprofile"/></legend>
						<ol>
							<li>
								<label><spring:message code="description"/>:</label>
								<span>
									<c:out value="${alligatorSettingsTest.getDescription()}" />								
								</span>							
							</li>
						</ol>
					</fieldset>
				</div>
			</div>				
		</c:if>
		<!-- end of user preferences section -->
		
		<!--  this section displays all procedure training records -->
		<c:if test="${contacteditform.contact.capabilityTrainingRecords.size() > 0}">
			<div id="proctraining-tab" class="hid">
				<table class="default4 proctrainingrecords" summary="<spring:message code='company.tableproceduretrainingrecords'/>">
					<thead>
						<tr>
							<td colspan="9"><spring:message code="company.proceduretrainingrecords"/> (${contacteditform.contact.capabilityTrainingRecords.size()})</td>
						</tr>
						<tr>
							<th class="jobno"><spring:message code="company.jobnoitemno"/></th>
							<th class="certno"><spring:message code="company.certno"/></th>
							<th class="proc"><spring:message code="capability"/></th>
							<th class="caldate"><spring:message code="caldate"/></th>
							<th class="issuedate"><spring:message code="issuedate"/></th>
							<th class="caltype"><spring:message code="caltype"/></th>
							<th class="trainee"><spring:message code="company.trainee"/></th>
							<th class="supervisor"><spring:message code="company.supervisor"/></th>
							<th class="files"><spring:message code="files"/></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="9">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody>
						<c:choose>
							<c:when test="${contacteditform.contact.capabilityTrainingRecords.size() < 1}">
								<tr>
									<td colspan="9" class="bold text-center">
										<spring:message code="company.therearenoproceduretrainingrecordstodisplay"/>
									</td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:forEach var="ptr" items="${contacteditform.contact.capabilityTrainingRecords}">
									<tr>
										<td class="jobno">
											<c:set var="rowcount" value="${0}"/>
											<c:forEach var="link" items="${ptr.cert.links}">
												<links:jobnoLinkDWRInfo jobno="${link.jobItem.job.jobno}" jobid="${link.jobItem.job.jobid}" copy="${false}" rowcount="${rowcount}"/>
												<c:set var="rowcount" value="${rowcount + 1}"/>
												<br/>
											</c:forEach>
										</td>
										<td class="certno">${ptr.cert.certno}</td>
										<td class="proc"><a href="viewcapability.htm?capabilityId=${ptr.procedure.id}&loadtab=proctraining-tab" class="mainlink" target="_blank">${ptr.procedure.reference}</a></td>
										<td class="caldate"><fmt:formatDate value="${ptr.cert.calDate}" dateStyle="medium" type="date" /></td>
										<td class="issuedate"><fmt:formatDate value="${ptr.cert.certDate}" dateStyle="medium" type="date" /></td>
										<td class="caltype">${ptr.cert.calType.serviceType.shortName}</td>
										<td class="trainee">${ptr.trainee.name}</td>
										<td class="supervisor">${ptr.labManager.name}</td>
										<td class="files">
											<c:forEach var="link" items="${ptr.cert.links}">
												<c:forEach var="f" items="${link.certFiles}">
													<a href='file:///${f.absolutePath}' class="mainlink" target="_blank">${f.name}</a><br/>
												</c:forEach>
											</c:forEach>
								    	</td>
									</tr>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
			</div>			
		</c:if>
		<!-- end of procedure training records -->
		
		<!-- user rights section -->
		<c:if test="${contacteditform.contact.sub.comp.companyRole == 'BUSINESS' && allowChangeUserRights}">
			<div id="userrights-tab" class="hid">
				<c:choose>
					<c:when test="${not empty contacteditform.contact.user}">
						<div id="newUserRights">
						<h4>User Rights</h4>
						<c:set var="userRoles" value="${contacteditform.userRoles}"/>
						<table class="default4" id="userRoleTable">
							<thead>
								<tr>
									<th scope="col" width="30%"><spring:message code="company"/></th>
									<th scope="col" width="30%"><spring:message code="subdivision"/></th>
									<th scope="col" width="30%"><spring:message code="contactadd.role"/></th>
									<th scope="col" width="10%"><spring:message code="contactedit.addordelete"/></th>
								</tr>
							</thead>
							<tbody id="userRoleTableBody">
								<c:forEach var="userRole" items="${userRoles}">
									<tr id="userRoleRow${userRole.userRoleId}">
										<td>${userRole.companyName}</td>
										<td>${userRole.subdivName}</td>
										<td>${userRole.roleName}</td>
										<td style="text-align: center;">
											<img src="img/icons/delete.png" height="20" width="20" alt="<spring:message code="contactedit.deleteuserrole"/>" title="<spring:message code="contactedit.deleteuserrole"/>" onClick="deleteNewUserRight(${userRole.userRoleId})"/>
										</td>
									</tr>
								</c:forEach>
								<tr id="newUserRole" style="background-color: #EEDDDD">
									<td>
										<select style="background-color: transparent" id="urCompanies" onchange="changeUserRightSubdivs(this)" onload="changeUserRightSubdivs(this)">
											<c:forEach var="compDTO" items="${businessSubs.keySet()}" varStatus="loop">
												<option value="${compDTO.key}" ${loop.first ? 'selected' : ''}>${compDTO.value}</option>
											</c:forEach>
										</select>
									</td>
									<td>
										<select style="background-color: transparent" id="selectUserRightSubdiv">
											<c:forEach var="compDTO" items="${businessSubs.keySet()}" varStatus="loop">
												<c:if test="${loop.first}">
													<c:forEach var="subDTO" items="${businessSubs.get(compDTO)}">
														<option value="${subDTO.key}">${subDTO.value}</option>
													</c:forEach>'
												</c:if>
											</c:forEach>
										</select>
									</td>
									<td>
										<select style="background-color: transparent" id="selectUserRole">
											<c:forEach var="role" items="${userRightRoles}">
												<option value="${role.id}">${role.name}</option>
											</c:forEach>
										</select>
									</td>
									<td style="text-align: center;">
										<img src="img/icons/add.png" height="20" width="20" alt="<spring:message code="contactedit.adduserrole"/>" title="<spring:message code="contactedit.adduserrole"/>" onClick="addNewUserRight(${contacteditform.contact.personid},'<spring:message code='contactedit.deleteuserrole'/>')"/>
									</td>
								</tr>
							</tbody>
						</table>
						</div>
					</c:when>
					<c:otherwise>
						<fieldset>
							<legend><spring:message code="company.weblogininformationfor"/><c:out value=" ${contacteditform.contact.name}"/></legend>										
							<ol>
								<li>
                                    <label><spring:message code="company.weblogin"/>:</label>
                                    <span><a
                                            href="<c:url value='addlogin.htm?personid=${contacteditform.contact.personid}'/>"
                                            class="mainlink"><spring:message code="company.addweblogin"/></a></span>
                                </li>
                            </ol>
                        </fieldset>
                    </c:otherwise>
                </c:choose>
            </div>
        </c:if>
                <!-- end of user rights section -->
                <c:if test="${allowEditUser}">
                    <div class="center">
                        <spring:message code="company.updatecontact" var="updateContactMsg"/>
                        <input type="submit" name="submit" id="submit" value="${updateContactMsg}"/>
                    </div>
                </c:if>
            </form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>