<%-- File name: /trescal/core/company/movecontact.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
        <span class="headtext"><spring:message
				code='company.changecontactsubdivision' /></span>
    </jsp:attribute>
	<jsp:attribute name="scriptPart">
        <script type='text/javascript'
			src='script/trescal/core/company/MoveContact.js'></script>
    </jsp:attribute>
	<jsp:body>           
        <div class="infobox">
		    <t:showErrors path="command.*" showFieldErrors="true" />
			
			<!-- this blank div clears floats and restores page flow -->
			<div class="clear"></div>
			
			<form:form modelAttribute="command" method="post" action="">
				<fieldset>
					<legend><spring:message code='company.changecontactsubdivision' /></legend>
					<ol>
						<li>
						    <a href="<spring:url value='viewperson.htm?personid=${command.contactId}'/>" class="mainlink">
						    	<spring:message code='company.backtocontact' />
						    </a>
						</li>
						<li>
							<spring:message code='company.youhavechosentomove' />
							<c:out value=" ${contact.name} " />
							<spring:message code='company.toadifferentdivisionwithin' />
							<c:out value=" ${contact.sub.comp.coname}." />
						</li>
						<li>
							<spring:message code='company.selectadivisionandnewdefaultaddressfor' />
							<span> ${contact.sub.comp.coname}</span>
						</li>
						<li>
							<label><spring:message code='subdiv' />:</label>
							<div class="float-left">
								<form:select path="newSubdivId" items="${subdivList}" itemLabel="value" itemValue="key" 
									onChange=" getActiveSubdivAddrs(this.value, 'newAddressId'); "/>
							</div>
							<form:errors path="newSubdivId" cssClass="error" />
							<div class="clear"></div>
						</li>
						<li>
							<label><spring:message code='address' />:</label>
							<div class="float-left">
								<form:select path="newAddressId" items="${addressList}" itemLabel="value" itemValue="key"/>
							</div>
							<form:errors path="newAddressId" cssClass="error" />
							<div class="clear"></div>
						</li>
						<li>
							 <spring:message code="company.instrumentsthatheistheownerof" arguments="${contact.name},${contact.instrum.size()}" />.
						</li>
						<li>
							<spring:message code='company.ifyoucontinuetomove' /><c:out value=" ${contact.name} " /><spring:message code='company.moveinstrumentorreassign' />.
						</li>
						<li>
							<form:radiobutton path="changeInstrumOwner" value="false" />
							<spring:message code='company.copyinstrumentsto' arguments="${contact.name}" />
						</li>
						<li>
							<form:radiobutton path="changeInstrumOwner" value="true" />
							<spring:message code='company.reassigninstruments' arguments="${contact.name}"/>
							
							<spring:bind path="command.newOwnerId">
								<select name="${status.expression}">
								<option value="0"><spring:message code="company.none"/></option>
								<c:forEach var="con" items="${contactlist}">
							       <c:if test="${con.personid != contact.personid}">
										<option value="${con.personid}">${con.name}</option>
								   </c:if>
								</c:forEach>
							</select>
							</spring:bind>
							<form:errors path="newOwnerId" cssClass="error" />
							<c:if test="${contactlist.size() == 0}"> 
								<div>
									<spring:message code='company.therearenoothercontactsin' /> ${contact.name}
									<spring:message code='company.labellong2' />.
								</div>
							</c:if>
						</li>
					</ol>
				</fieldset>
			
				<div>
					<ul>
					<c:set var="first" value="true" /> 
						<c:forEach var="subdiv" items="${sublist}">
							<li>
								${subdiv.subname}
								<spring:bind path="command.newSubdivId">
								<input type="radio" value="${subdiv.subdivid}" name="${status.expression}"
									<c:if test="${first == true}"> checked="checked" </c:if>>
								</spring:bind>
								<br />
								<br />
								
								<span>
									<spring:message code='company.availableaddressesfor' /> ${subdiv.subname} (${subdiv.addresses.size()})
								</span>
								<ul>
								<c:forEach var="address" items="${subdiv.addresses}">
									<li>
										<spring:bind path="command.newAddressId">
										<input type="radio" value="${address.addrid}" name="${status.expression}"
											<c:if test="${first == true}"> checked="checked" </c:if>>
										</spring:bind>
										<address:showShortAddress address="${address}" />
									</li>
									<c:set var="first" value="false" />
								</c:forEach>
								</ul>
							</li>
						</c:forEach>
					</ul>
				</div>
				
				<input type="submit" value="<spring:message code='company.movecontact' />" name="save" />
			</form:form>
	
		</div>
    </jsp:body>
</t:crocodileTemplate>