<%-- File name: /trescal/core/company/subdivedit.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<script type='text/javascript' src='script/trescal/core/company/SubdivEdit.js'></script>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="company.editsubdivision"/></span>
	</jsp:attribute>
	<jsp:body>
		<t:showErrors path="form.*" showFieldErrors="false"/>
		<div class="infobox">
			<form:form modelAttribute="form" method="post">
				<fieldset>
					<legend><spring:message code="edit"/>&nbsp;${subdiv.subname}</legend>
					<ol>
						<li>
							<div class="displaycolumn-25">
								<label><spring:message code="company"/>:</label>
							</div>
							<div class="float-left">
								<span>${subdiv.comp.coname}</span>
							</div>
							<div class="clear"></div>	
						</li>
						<li>
							<div class="displaycolumn-25">
								<label><spring:message code="company.role"/>:</label>
							</div>
							<div class="float-left">
								<span>${subdiv.comp.companyRole.getMessage()}</span>
							</div>
							<div class="clear"></div>	
						</li>
						<li>
							<div class="displaycolumn-25">
								<form:label path="subdivName">
									<image:globalSetting/>
									<spring:message code="company.subdivisionname"/>:
								</form:label>
							</div>
							<div class="float-left">
								<form:input path="subdivName" size="75"/>
								<form:errors path="subdivName" delimiter="; " cssClass="float-left; error"/>
							</div>
							<div class="clear"></div>	
						</li>
						<li>
							<div class="displaycolumn-25">
								<form:label path="formerId">
									<image:globalSetting/>
									<spring:message code="company.legacynumber"/>:
								</form:label>
							</div>
							<div class="float-left">
								<form:input path="formerId" size="50"/>
								<form:errors path="formerId" delimiter="; " cssClass="float-left; error"/>
							</div>
							<div class="clear"></div>	
						</li>
						<c:if test="${subdiv.comp.companyRole == 'BUSINESS'}">
							<li id="analyticalcenter">
								<div class="displaycolumn-25">
									<label>
										<image:globalSetting/>
										<spring:message code="subdiv.analyticalcenter"/>:
									</label>
								</div>
								<div class="float-left">
									<form:input path="analyticalCenter" size="10" />
									<form:errors path="analyticalCenter" delimiter="; " cssClass="float-left; error"/>
								</div>
								<div class="clear"></div>
							</li>
	 						<li id="subdivcode">
								<div class="displaycolumn-25">
									<label>
										<image:globalSetting/>
										<spring:message code="subdiv.code"/>:
									</label>
								</div>
								<div class="float-left">
									<form:input path="subdivCode" size="10" />
									<form:errors path="subdivCode" delimiter="; " cssClass="float-left; error"/>
								</div>
								<div class="clear"></div>
							</li>
							<c:if test="${isAdmin}">
								<li>
									<div class="displaycolumn-25">
										<label>
											<image:globalSetting/>
											<spring:message code="company.defaultturnaround"/>:
										</label>
									</div>
									<div class="float-left">
										<form:input path="defaultTurnaround" type="number"/>
										<form:errors path="defaultTurnaround" delimiter="; " cssClass="float-left; error"/>
									</div>
									<div class="clear"></div>
								</li>
							</c:if>
							<li>
								<div class="displaycolumn-25">
									<label>
										<image:businessSubdivisionSpecific/>
										<spring:message code="company.defaultbusinesscontact"/>:
									</label>
								</div>
								<div class="float-left">
									<form:select path="defaultBusinessContact" items="${businessContacts}" itemValue="key" itemLabel="value" />
									<form:errors path="defaultBusinessContact" delimiter="; " cssClass="float-left; error"/>
								</div>
								<div class="clear"></div>
							</li>
						</c:if>
						<c:if test="${subdiv.comp.companyRole == 'CLIENT'}">
							<li>
								<div class="displaycolumn-25">
									<label>
										<image:businessSubdivisionSpecific/>
										<spring:message code="subdiv.settingsforallocatedsubdiv.localbusinesscontact" />:
									</label>
								</div>
								<div class="float-left">
									<form:select path="defaultBusinessContactForAllocatedSubdiv" items="${businessContactsForAllocatedSubdiv}" itemValue="key" itemLabel="value" /><strong>[${allocatedSubdiv.value}]</strong> 
									<form:errors path="defaultBusinessContactForAllocatedSubdiv" delimiter="; " cssClass="float-left; error"/>
								</div>
								<div class="clear"></div>
							</li>
						</c:if>
						<li>
							<div class="displaycolumn-25">
								<label>
									<image:globalSetting/>
									<spring:message code="subdiv.siretnumber"/>:
								</label>
							</div>
							<div class="float-left">
								<form:input path="siretNumber"/>
								<form:errors path="siretNumber" delimiter="; " cssClass="float-left; error"/>
							</div>
							<div class="clear"></div>
						</li>
						<li>
							<div class="displaycolumn-25">
								<label for="submit">&nbsp;</label>
							</div>
							<div class="float-left">
<%-- 								<input type="submit" name="submit" id="submit" value="<spring:message code='submit'/>" /> --%>
								<input type="submit" name="submit" id="submit" value="<spring:message code='submit' />" />
							</div>
							<div class="clear"></div>	
						</li>
					</ol>
				</fieldset>
			</form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>