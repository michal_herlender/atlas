<%-- File name: /trescal/core/company/department.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address" %>
<%@ taglib prefix="company" tagdir="/WEB-INF/tags/company" %>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="company.addeditdepartmentdetails"/> (${command.department.subdiv.subname})</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/company/Department.js'></script>
		<style>div#addresses-tab label {width: auto}</style>
	</jsp:attribute>
	<jsp:body>
		<t:showErrors path="command.*" showFieldErrors="true"/>
		<form:form modelAttribute="command" method="post" action="" id="departmentform">
			<div class="infobox">
				<fieldset>
					<legend>
						<spring:message code="company.addeditdepartmentdetailsfor"/>
						<a href="viewsub.htm?subdivid=${command.subdivid}" class="mainlink">
							${command.department.subdiv.subname}
						</a>
					</legend>
					<c:if test="${command.userCanEdit != true}">
						<div class="warningBox1">
							<div class="warningBox2">
								<div class="warningBox3">
									<spring:message code="company.nopriviledgesforeditdepartment"/>.
								</div>
							</div>
						</div>
					</c:if>
					<ol>											
						<li>
							<label><spring:message code="name"/>:</label>
							<form:input path="departmentName" class="width300" />
							<form:errors path="departmentName"/>
						</li>
						<li>
							<label><spring:message code="company.shortname"/>:</label>
							<form:input path="shortName" maxlength="4" size="4"/>
							<span>(<spring:message code="company.maximum4charactersonlynecessaryforlabs"/>)</span>
							<form:errors path="shortName"/>
						</li>
						<li>
							<label><spring:message code="company.departmenttype"/>:</label>
							<form:select path="departmentType" items="${departmenttypes}" itemValue="key" itemLabel="value"/>
							<form:errors path="departmentType"/>
						</li>
					</ol>
				</fieldset>
			</div>
			<!-- this infobox contains all active and inactive contacts/addresses for the subdivision -->
			<div class="infobox">
				<h5><spring:message code="company.departmentmembers"/></h5>
				<!-- div to hold all the tabbed submenu -->
				<div id="tabmenu">
					<!-- tabbed submenu to change between active/inactive contacts/addresses -->
					<ul class="subnavtab">
						<li><a href="#" id="addresses-link" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'addresses-tab', false); " class="selected"><spring:message code="company.addresses"/></a></li>
						<li><a href="#" id="members-link" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'members-tab', false); "><spring:message code="company.currentmembers"/></a></li>
						<li><a href="#" id="add-link" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'add-tab', false); "><spring:message code="company.addmembers"/></a></li>
						<li><a href="#" id="categories-link" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'categories-tab', false); "><spring:message code="company.procedurecategories"/></a></li>
					</ul>
					<!-- div to hold all content which is to be changed using the tabbed submenu -->
					<div class="tab-box">
						<!-- div to choose an address -->
						<div id="addresses-tab">
							<fieldset>
								<legend><spring:message code="company.chooseaddress"/></legend>
								<ol>
									<li>
										<form:radiobuttons path="addrid" items="${addresses}" itemValue="key" itemLabel="value"  delimiter="</li><li>" cssClass="float-left"/>
										<div class='clear'></div>
									</li>
								</ol>
							</fieldset>
						</div>
						<div class="clear"></div>
						<!-- end of div to choose an address -->
						<!-- div displays all active/inactive contacts for this subdivision -->
						<div id="members-tab" class="hid">
							<fieldset>
								<legend><spring:message code="company.currentmembers"/></legend>
								<ol>
									<c:set var="count" value="0"/>
									<c:forEach var="contact" items="${cons}">
										<c:if test="${command.contacts.get(contact.personid) == 'on'}">
											<li>
												<spring:bind path="command.contacts['${contact.personid}']">
													<input type="hidden" name="_${status.expression}"/>
													<label class="alpha">${contact.name}</label>
													<input type="checkbox" name="${status.expression}" checked/>
												</spring:bind>
												<spring:bind path="command.deptRoles['${contact.personid}']">
													<select name="${status.expression}" style=" margin-left: 20px; ">
														<c:forEach var="role" items="${roles}">
															<option value="${role.id}" ${role.id == command.deptRoles.get(contact.personid) ? "selected" : ''}>${role.role}</option>
														</c:forEach>
													</select>
												</spring:bind>
											</li>
											<c:set var="count" value="${count + 1}"/>
										</c:if>
									</c:forEach>
									<c:if test="${count == 0}">
										<li>
											<spring:message code="company.nomembersassigned"/>
										</li>
									</c:if>
								</ol>
							</fieldset>
						</div>
						<!-- end of div which displays all contacts -->
						<!-- div displays all active/inactive addresses for the chosen subdivision -->
						<div id="add-tab" class="hid">
							<fieldset>
								<legend><spring:message code="company.addmembers"/></legend>
								<ol>
									<c:set var="count" value="0"/>
									<c:forEach var="contact" items="${cons}">
										<c:if test="${command.contacts.get(contact.personid) != 'on' && contact.active}">
											<li>
												<spring:bind path="command.contacts['${contact.personid}']">
													<input type="hidden" name="_${status.expression}"/>
													<label>${contact.name}</label>
													<input type="checkbox" name="${status.expression}"/>
												</spring:bind>
												<spring:bind path="command.deptRoles['${contact.personid}']">
													<select name="${status.expression}" style=" margin-left: 20px; ">
														<c:forEach var="role" items="${roles}">
															<option value="${role.id}">${role.role}</option>
														</c:forEach>
													</select>
												</spring:bind>
											</li>
											<c:set var="count" value="${count + 1}"/>
										</c:if>
									</c:forEach>
									<c:if test="${count == 0}">
										<li>
											<spring:message code="company.nocontactstobeassigned"/>
										</li>
									</c:if>
								</ol>
							</fieldset>
						</div>
						<!-- end of div to display all addresses -->
						<!-- div displays all procedure categories of the department -->
						<div id="categories-tab" class="hid">
							<fieldset>
								<legend><spring:message code="company.procedurecategories"/></legend>
								<table class="default4">
									<thead>
										<tr>
											<th><spring:message code="name"/></th>
											<th><spring:message code="description"/></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="procCat" items="${procedurecategories}">
											<tr>
												<td width="20%">${procCat.name}</td>
												<td>${procCat.description}</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</fieldset>
						</div>
						<!--  end of div to display all procedure categories -->
					</div>
					<!-- end of div to hold all tabbed submenu content -->
				</div>
				<!-- end of div to hold tabbed submenu -->
				<!-- this div clears floats and restores page flow -->
				<div class="clear"></div>
			</div>
			<!-- end of active and inactive contacts/addresses infobox -->
			<div class="center-padbot">
				<input type="submit" name="submit" id="submit" value="<spring:message code='company.submitdepartment'/>"/>
			</div>
		</form:form>
	</jsp:body>
</t:crocodileTemplate>