<%-- File name: /trescal/core/company/companyaccounts.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address" %>
<%@ taglib prefix="company" tagdir="/WEB-INF/tags/company" %>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="company.companyinformation"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/company/CompanyAccounts.js'></script>
	</jsp:attribute>
	<jsp:body>
	
		<fieldset>
			
			<legend><spring:message code="company.accountinfo"/></legend>
																											
			<form:form action="" class="companyAccountForm" method="post" modelAttribute="command">
			
				<form:errors path="*">
                     		<div class="warningBox1">
                        		<div class="warningBox2">
                            		<div class="warningBox3">
                                		<h5 class="center-0 attention">
                                    		<spring:message code='cylindricalstandard.errorsaved' />
                                		</h5>
                                		<c:forEach var="e" items="${messages}">
                                    		<div class="center attention"><c:out value="${e}"/></div>
                                		</c:forEach>
                            		</div>
                        		</div>
                     		</div>
                  </form:errors>							
				<ol>
					<li>
						<label><spring:message code="company"/>:</label>
						<span><links:companyLinkDWRInfo company="${command.company}" rowcount="0" copy="true"/></span>
					</li>
					
					<li>
						<c:choose>
							<c:when test="${command.newAccount}">
								<label><spring:message code="company.accountstatus"/>:</label>
								<span><spring:message code="company.companyaccountisunsaved"/></span>
							</c:when>
							<c:otherwise>
								<label><spring:message code="company.accountstatus"/>:</label>
								<div class="float-left padtop">
									<c:choose>
										<c:when test="${command.account.accountsStatus == 'P'}">
											<img src="img/icons/sync_pending.png" width="14" height="14" alt="<spring:message code="company.pendingsync"/>" title="<spring:message code="company.pendingsync"/>" class="image_inl" />
											<spring:message code="company.pendingsyncmessage"/>
										</c:when>
										<c:when test="${command.account.accountsStatus == 'E'}">
											<img src="img/icons/sync_error.png" width="14" height="14" alt="<spring:message code="company.errorsync"/>" title="<spring:message code="company.errorsync"/>" class="image_inl" />
											<spring:message code="company.errorsyncmessage"/>
										</c:when>
										<c:otherwise>
											<img src="img/icons/sync.png" width="14" height="14" alt="<spring:message code="company.sync"/>" title="<spring:message code="company.sync"/>" class="image_inl" /> 
											<spring:message code="company.syncmessage"/>
										</c:otherwise>
									</c:choose>
									<br />															
									<c:if test="${not empty accountssoftware}">
										( ${accountssoftware} <img src="img/accountsthumb.jpg" width="18" height="19" class="img_marg_bot" /> )
									</c:if>
								</div>
								<!-- clear floats and restore page flow -->
								<div class="clear"></div>															
							</c:otherwise>
						</c:choose>
					</li>
					<li>
						<label><spring:message code="company.contactsource"/>:</label>
						<div class="float-left">
							<c:set var="contactSource" value=""/>
								<c:set var="checkedTextCont" value=""/>
								<c:if test="${command.contactSource == 'CUSTOM'}">
									<c:set var="checkedTextCont" value="checked"/>
								</c:if>
								<form:radiobutton path="contactSource" class="widthAuto" value="CUSTOM" checked="${checkedTextCont}" onclick=" if (this.checked){$j('li.cont-custom').removeClass().addClass('cont-custom vis');$j('li.cont-exist').removeClass().addClass('cont-exist hid');} " />
								<spring:message code="company.customaccsource"/>
								<br />
								<c:set var="checkedTextCont" value=""/>
								<c:if test="${command.contactSource == 'EXISTING'}">
									<c:set var="checkedTextCont" value="checked"/>
								</c:if>
								<form:radiobutton path="contactSource" class="widthAuto" value="EXISTING" checked="${checkedTextCont}" onclick=" if (this.checked){$j('li.cont-exist').removeClass().addClass('cont-exist vis');$j('li.cont-custom').removeClass().addClass('cont-custom hid');} " />
								<spring:message code="company.existingsubdiv"/>
						</div>
						<!-- clear floats and restore page flow -->
						<div class="clear"></div>
					</li>
					<c:set var="hideForExisting" value="vis"/>
					<c:set var="hideForCustom" value="hid"/>
					<c:if test="${contactSource == 'EXISTING'}">
						<c:set var="hideForExisting" value="hid"/>
						<c:set var="hideForCustom" value="vis"/>
					</c:if>
					<li class="cont-custom ${hideForExisting}">
						<label><spring:message code="company.title"/></label>
						<input id="compid" type="hidden" value="${command.company.coid}">
						<form:input path="title" id="titles" onclick="getTitlesAutoComplet()"/>
						<form:errors path="title" class="attention"/>
					</li>
					<li class="cont-custom ${hideForExisting}">
						<label><spring:message code="company.firstname"/>:</label>
						<form:input path="accountContact.firstName"/>
						<form:errors path="accountContact.firstName" class="attention"/>
					</li>
					<li class="cont-custom ${hideForExisting}">
						<label><spring:message code="company.lastname"/>:</label>
						<form:input path="accountContact.lastName"/>
						<form:errors path="accountContact.lastName" class="attention"/>
					</li>
					<li class="cont-custom ${hideForExisting}">
						<label><spring:message code="company.position"/>:</label>
						<form:input path="accountContact.position"/>
						<form:errors path="accountContact.position" class="attention"/>
					</li>
					<li class="cont-custom ${hideForExisting}">
						<label><spring:message code="company.tel"/>:</label>
						<form:input path="accountContact.telephone"/>
						<form:errors path="accountContact.telephone" class="attention"/>
						<spring:message code="company.ext"/>
						<form:input path="accountContact.telephoneExt" cssClass="small"/>
						<form:errors path="accountContact.telephoneExt" class="attention"/>
					</li>
					<li class="cont-custom ${hideForExisting}">
						<label><spring:message code="company.mobile"/>:</label>
						<form:input path="accountContact.mobile"/>
						<form:errors path="accountContact.mobile" class="attention"/>
					</li>
					<li class="cont-custom ${hideForExisting}">
						<label><spring:message code="company.fax"/>:</label>
						<form:input path="accountContact.fax"/>
						<form:errors path="accountContact.fax" class="attention"/>
					</li>
					<li class="cont-custom ${hideForExisting}">
						<label><spring:message code="company.email"/>:</label>
						<form:input path="accountContact.email"/>
						<form:errors path="accountContact.email" class="attention"/>
					</li>
					<li class="cont-custom ${hideForExisting}">
						<label><spring:message code="company.prefs"/>:</label>
						<form:select path="accountContact.preference">
							<form:option value="E"><spring:message code="company.email"/></form:option>
							<form:option value="F"><spring:message code="company.fax"/></form:option>
							<form:option value="L"><spring:message code="company.letter"/></form:option>
						</form:select>
						<form:errors path="accountContact.preference" class="attention"/>
					</li>
					<li class="cont-exist ${hideForCustom}">
						<label><spring:message code="company.existingcontact"/>:</label>
						 <div id="existingContact" class="accountbox width80">
							<c:set var="currentdiv" value=""/>
							<c:forEach var="contact" items="${contacts}" varStatus="forEachStatus">
								<c:set var="checked" value=""/>
								<c:if test="${(contact.personid == command.personid) || ((empty command.personid) && forEachStatus.first)}">
									<c:set var="checked" value="checked"/>
								</c:if>
								<c:if test="${contact.sub.subname != currentdiv}">
									<div class="padding5 bold">${contact.sub.subname}</div>
									<c:set var="currentdiv" value="${contact.sub.subname}"/>
								</c:if>
								<div class="padleft10">
									<form:radiobutton path="personid" checked="${checked}" value="${contact.personid}" />
									${contact.name}
									<c:if test="${not empty contact.position}">- ${contact.position}</c:if>
								</div>
							</c:forEach>
						</div>
						<!-- clear floats and restore page flow -->
						<div class="clear"></div>
					</li>
					<li>
						   <label><spring:message code="company.addresssource"/>:</label>
						    <c:set var="addressSource" value=""/>
							<c:set var="checkedTextAddr" value=""/>
							<c:if test="${command.addressSource == 'CUSTOM'}">
								<c:set var="checkedTextAddr" value="checked"/>
							</c:if>
							<form:radiobutton path="addressSource" class="widthAuto" value="CUSTOM" checked="${checkedTextAddr}" onclick=" if (this.checked){$j('li.addr-custom').removeClass().addClass('addr-custom vis');$j('li.addr-exist').removeClass().addClass('addr-exist hid');} " />
							<spring:message code="company.custaccaddress"/>
							<br />
							<c:set var="checkedTextAddr" value=""/>
							<c:if test="${command.addressSource == 'EXISTING'}">
								<c:set var="checkedTextAddr" value="checked"/>
							</c:if>
							<form:radiobutton path="addressSource" class="widthAuto" value="EXISTING" checked="${checkedTextAddr}" onclick=" if (this.checked){$j('li.addr-exist').removeClass().addClass('addr-exist vis');$j('li.addr-custom').removeClass().addClass('addr-custom hid');} " />
							<spring:message code="company.existdivaddress"/>
						<!-- clear floats and restore page flow -->
						<div class="clear"></div>
					</li>
					
					<c:set var="hideForExisting" value="vis"/>
					<c:set var="hideForCustom" value="hid"/>
					<c:if test="${addressSource == 'EXISTING'}">
						<c:set var="hideForExisting" value="hid"/>
						<c:set var="hideForCustom" value="vis"/>
					</c:if>
					
					<li class="addr-custom ${hideForExisting}">
						
						<label for="addr1"><spring:message code="address.address"/> 1:</label>
						<form:input path="accountAddress.addr1"/>
						<form:errors path="accountAddress.addr1" class="attention"/>
					</li>
					<li class="addr-custom ${hideForExisting}">
						
						<label for="addr2"><spring:message code="address.address"/> 2:</label>
						<form:input path="accountAddress.addr2"/>
						<form:errors path="accountAddress.addr2" class="attention"/>
					</li>
					<li class="addr-custom ${hideForExisting}">
						
						<label for="addr3"><spring:message code="address.address"/> 3:</label>
						<form:input path="accountAddress.addr3"/>
						<form:errors path="accountAddress.addr3" class="attention"/>
					</li>
					<li class="addr-custom ${hideForExisting}">
						
						<label for="town"><spring:message code="address.town"/>:</label>
						<form:input path="accountAddress.town"/>
						<form:errors path="accountAddress.town" class="attention"/>
					</li>
					<li class="addr-custom ${hideForExisting}">
						
						<label for="county"><spring:message code="address.county"/>:</label>
						<form:input path="accountAddress.county"/>
						<form:errors path="accountAddress.county" class="attention"/>
					</li>
					<li class="addr-custom ${hideForExisting}">

						<label for="postcode"><spring:message code="address.postcode"/>:</label>
						<form:input path="accountAddress.postcode"/>
						<form:errors path="accountAddress.postcode" class="attention"/>
					</li>
					<li class="addr-custom ${hideForExisting}">
						<label for="country"><spring:message code="address.country"/>:</label>
						<div class="float-left">
							<form:select path="countryId" items="${countrylist}" itemValue="countryid" itemLabel="localizedName"/>
							<form:errors path="countryId" class="attention"/>
						</div>
						<div class="clear"></div>															
					</li>
					<li class="addr-exist ${hideForCustom}">
						<label><spring:message code="company.existingaddress"/>:</label>
						<div id="existingAddress" class="accountbox width80">
							<c:set var="currentdiv" value=""/>
							<c:forEach var="address" items="${addresses}" varStatus="forEachStatus">
								<c:if test="${address.sub.subname != currentdiv}">
									<div class="padding5 bold">${address.sub.subname}</div>
									<c:set var="currentdiv" value="${address.sub.subname}"/>
								</c:if>
								<c:set var="checked" value=""/>
								<c:if test="${(address.addrid == command.addressid) || (empty command.addressid && forEachStatus.index == 0)}">
									<c:set var="checked" value="checked"/>
								</c:if>
								
								<div class="padleft10">
									<form:radiobutton path="addressid" checked="${checked}" value="${address.addrid}"/>
									<address:showShortAddress address="${address}"/>
									<c:if test="${address.addressType.size() > 0}">
										[<c:forEach var="at" items="${address.addressType}" varStatus="forEachStatus">
											<c:if test="${not forEachStatus.first}">,</c:if>
											${at.getType()}
										</c:forEach>]
									</c:if>
								</div>
							</c:forEach>
						</div>
						<!-- clear floats and restore page flow -->
						<div class="clear"></div>
					</li>
					<li>
						<c:set var="bankDetails" value="false"/>
						<label><spring:message code="company.incbankdetails"/>:</label>
							<c:set var="checked" value=""/>
							<c:if test="${includeBankDetails}">
								<c:set var="checked" value="checked"/>
							</c:if>
							<form:checkbox path="includeBankDetails" class="widthAuto" value="true" checked="${checked}" onclick=" if (this.checked){$j('li.bank-det').removeClass().addClass('bank-det vis');}else{$j('li.bank-det').removeClass().addClass('bank-det hid');} " />
							<input type="hidden" name="_includeBankDetails" value="false"/>
							<c:set var="bankDetails" value="${includeBankDetails}"/>
					</li>
					
					<c:set var="hideBankDetails" value="hid"/>
					<c:if test="${bankDetails == true}">
						<c:set var="hideBankDetails" value="vis"/>
					</c:if>
					
					<li class="bank-det ${hideBankDetails}">
						<label ><spring:message code="company.accountnumber"/>:</label>
						
						<form:input path="bankDetails.accountNo"/>
						<form:errors path="bankDetails.accountNo" class="attention"/>
					</li>
					<li class="bank-det ${hideBankDetails}">
						<label ><spring:message code="company.sortcode"/>:</label>
						
						<form:input path="bankDetails.sortCode"/>
						<form:errors path="bankDetails.sortCode" class="attention"/>
					</li>
					<li class="bank-det ${hideBankDetails}">
						<label ><spring:message code="company.accountname"/>:</label>
						
						<form:input path="bankDetails.accountName"/>
						<form:errors path="bankDetails.accountName" class="attention"/>
					</li>
					<li class="bank-det ${hideBankDetails}">
						<label ><spring:message code="company.bacsref"/>:</label>
						
						<form:input path="bankDetails.bacsRef"/>
						<form:errors path="bankDetails.bacsRef" class="attention"/>
					</li>
					<li class="bank-det ${hideBankDetails}">
						<label ><spring:message code="company.banknumber"/>:</label>
						
						<form:input path="bankDetails.bankName"/>
						<form:errors path="bankDetails.bankName" class="attention"/>
					</li>
					<li>
						<label ><spring:message code="company.accountnote"/>:</label>
						<form:textarea path="account.accountNote" cols="" rows="4" class="width80"/>
						<form:errors path="account.accountNote" class="attention"/>
					</li>
					<%--
					Field no longer in use
					<li>
						<label >Line Discount:</label>
						<form:input path=""/>account.lineDiscount" "")%
						${status.errorMessage}
					</li>
					--%>										
					<li>
						<label ><spring:message code="company.settlediscount"/>:</label>
						
						<form:input path="account.settlementDiscount" cssClass="small"/>%
						<form:errors path="account.settlementDiscount" class="attention"/>
					</li>
					<li>
						<label ><spring:message code="company.settledays"/>:</label>
						<form:input path="account.settlementDays" cssClass="small"/> days
						<form:errors path="account.settlementDays" class="attention"/>
					</li>
					<!-- Custom warning for Dimensions, rubbish I know but that's Dimensions -->
					
					<c:if test="${((command.company.corole.corole == 'CLIENT') || (command.company.corole.corole == 'BUSINESS')) && (accountssoftware == 'Access Dimensions')}">
						<li>
							<spring:message code="company.settlemessage1"/> ${accountssoftware} ,<spring:message code="company.settlemessage2"/> 
						</li>
					</c:if>
					
					<li>
						<label>&nbsp;</label>
						<input type="submit" name="submit" class="submit" value="<spring:message code="company.save"/>" />
					</li>
					
				</ol>
													
			</form:form>
			
		</fieldset>

	
	</jsp:body>
</t:crocodileTemplate>