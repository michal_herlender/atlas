<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="company.addflexiblefield"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script	src="script/trescal/core/company/addeditflexiblefielddefinition.js"></script>
    </jsp:attribute>
	<jsp:body>
		<t:showErrors path="command.*" showFieldErrors="true"/>
		<form:form>
			<form:hidden path="coid"/>
			<form:hidden path="fieldDefinitionId"/>
			<div class="infobox flexibleFieldDefinition">
				<fieldset>
					<ol>
						<li>
							<label><spring:message code="name"/></label>
							<form:input path="name"/>
						</li>
						<li>
							<label><spring:message code="type"/></label>
							<form:select path="typeId" items="${fieldtypes}" itemLabel="value" itemValue="key" id="FieldType" disabled="${(command.fieldDefinitionId > 0)?'true':'false'}"/>
						</li>
						<li>
							<label><spring:message code="company.updateablebycustomer"/></label>
							<form:checkbox path="updatable"/>
						</li>
						
						<li>
						<div id="Values" ${(command.typeId == 4)?'':'hidden'}>
							<h3><spring:message code="company.acceptablevalues"/></h3>
							
							<ul id="valuelist">
							<c:forEach var="value" items="${command.values}" varStatus="loopStatus">
								<li>
									<form:input path="values[${loopStatus.index}].libraryValueName" class="libraryValue" />
									<form:hidden path="values[${loopStatus.index}].libraryValueId" />
								</li>
							</c:forEach>
							</ul>
							<ul><li>
							<input type="text" name="newValue" id="newValue" class="libraryValue" /> <button type="button" id="addbtn"><spring:message code="add"/></button>
							</li></ul>
							
						</div>
						</li>
						
					</ol>
					<input type="submit" value="<spring:message code="company.submit"/>"/>
				</fieldset>
			</div>
		</form:form>
		
	</jsp:body>
</t:crocodileTemplate>