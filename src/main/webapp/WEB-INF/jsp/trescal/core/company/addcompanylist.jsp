<%-- File name: /trescal/core/company/addcompanylist.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="companylist.add"/></span>
	</jsp:attribute>
	
	<jsp:body>
		<div class="infobox" id="searchcompanylistform">
		<form:form action="" id="addcompanylist" method="post" modelAttribute="addcompanylistform">
			<fieldset>
				<legend><spring:message code="companylist.add" /></legend>
				<li>
					<label for="orgid"><spring:message code="businesscompany" />:</label> 
					<span>${allocatedCompany.value}</span>
				</li>
				
				<li>
					<label for="nameList"><spring:message code="companylistsearch.listname" />*:</label> 
					<form:input path="nameList"/>
					<form:errors class="error" path="nameList"/>
				</li>
				
				<li>
					<label>&nbsp;</label>
					<input type="submit" name="submit" value="<spring:message code="create" />" tabindex="9" />
				</li>
					
			</fieldset>
		</form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>