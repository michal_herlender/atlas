<%-- File name: /trescal/core/company/usergroup.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="company.addeditusergroup"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/company/Usergroup.js'></script>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox" id="addusergroup">
			<form:form method="post">
				<fieldset>
					<legend><spring:message code="company.addeditusergroup" /></legend>
					<ol>
						<li>
							<a href="viewcomp.htm?coid=${model.ug.company.coid}&loadtab=usergroups-tab"><spring:message code="company.backtoviewgroups" /></a>
						</li>
						<li>
							<label for="ug.name"><spring:message code="company.groupname" />:</label>
							<form:input path="model.ug.name"/>
							<span class="error"><form:errors path="model.ug.name"/></span>
						</li>
						<li>
							<label for="ug.description"><spring:message code="company.groupdescription" />:</label>
							<div class="float-left editor_box">
								<form:textarea path="model.ug.description" rows="10" cols="" style=" width: 100%; "/>
							</div>
							<!-- clear floats and restore page flow -->
							<div class="clear-0"></div>
							<form:errors path="model.ug.description" class="attention"/>
						</li>
						
						<c:forEach var="property" items="${model.properties}" varStatus="curStatus">
							<li>
								${property.name} | ${property.description} <br/>
								<form:select path="model.valueid[${curStatus.index}]">
									<c:forEach var="value" items="${property.values}">
										<c:choose>
											<c:when test="${model.add}">
												<option value="${value.valueid}">${value.value}</option>
											</c:when>
											<c:otherwise>
												<!-- set the selected value -->
												<c:forEach var="ugv" items="${model.ug.values}">
													<c:if test="${value.ugp.propertyid == ugv.value.ugp.propertyid}">	<!-- for the selected property -->
														<c:choose>
															<c:when test="${ugv.value.valueid == value.valueid}">
																<option value="${value.valueid}" selected="selected">${value.value}</option>
															</c:when>
															<c:otherwise>
																<option value="${value.valueid}">${value.value}</option>
															</c:otherwise>
														</c:choose>
													</c:if>
												</c:forEach>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</form:select>
							</li>
						</c:forEach>
						<li>
							<label for="submit">&nbsp;</label>
							<input type="submit" id="submit" name="submit" value="<spring:message code='company.submitusergroup' />" />
						</li>
					</ol>							
				</fieldset>
			</form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>