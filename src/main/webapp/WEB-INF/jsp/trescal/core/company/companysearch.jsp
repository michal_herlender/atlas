<%-- File name: /trescal/core/company/companysearch.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="companysearch.headtext"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/company/CompanySearch.js'></script>
		<script>var ALLCOROLES = [];</script>
		<c:forEach var="companyRole" items="${companyRoles}">
			<c:if test="${companyRole.getKey() > 0}">
				<script>ALLCOROLES.push('${companyRole}');</script>
			</c:if>
		</c:forEach>
	</jsp:attribute>
	<jsp:body>
		<div id="compsearch" class="infobox">
			<form:form action="">
			<fieldset>
				<legend><spring:message code="company.companybrowser"/></legend>
				<ol>
					<li id="compCoroles">
						<h5><spring:message code="companysearch.companyroles"/></h5>
						<c:forEach var="companyRole" items="${companyRoles}">
							<c:if test="${companyRole.getKey() > 0}">
								<input type="checkbox" class="checkbox cbrole" id="${companyRole}" name="${companyRole}" value="${companyRole}" onclick="onClickCompanyRole(); runCompanyAjax($j('#coname').val());" <c:if test="${companyRole.getKey() == 1}">checked</c:if> />
								<a href="#" title="${companyRole}" class="thinlink" onclick="event.preventDefault(); selectOnly('${companyRole}'); runCompanyAjax($j('#coname').val());">${companyRole.getMessage()}</a>
							</c:if>
						</c:forEach>
						&nbsp;&nbsp;&nbsp;
						<input id="searchAll" type="button" style="width: 100px" onclick="event.preventDefault(); selectAll(); runCompanyAjax($j('#coname').val());" value="<spring:message code='companysearch.searchall'/>"/>
						<input id="searchDefault" type="hidden" style="width: 100px" onclick="event.preventDefault(); selectDefault(); runCompanyAjax($j('#coname').val());" value="<spring:message code='companysearch.searchdefault'/>"/>
					</li>
					<li>
						<h5><spring:message code="companysearch.searchoptions"/></h5>
						<div>
							<input type="radio" class="checkbox" name="activeradio" value="active" checked onclick="runCompanyAjax($j('#coname').val());" />
							<spring:message code="companysearch.active"/>
							<input type="radio" class="checkbox" name="activeradio" value="activeanywhere" onclick="runCompanyAjax($j('#coname').val());" />
							<spring:message code="companysearch.activeanywhere"/>
							<input type="radio" class="checkbox" name="activeradio" value="inactive" onclick="runCompanyAjax($j('#coname').val());" />
							<spring:message code="companysearch.inactive"/>
							&nbsp; | &nbsp;
							<input type="radio" class="checkbox" name="historyradio" value="CURRENT_NAME" checked onclick="runCompanyAjax($j('#coname').val());" />
							<spring:message code="companysearch.currentname"/>
							<input type="radio" class="checkbox" name="historyradio" value="HISTORIC_NAME" onclick="runCompanyAjax($j('#coname').val());" />
							<spring:message code="companysearch.historicname"/>
							<input type="radio" class="checkbox" name="historyradio" value="LEGAL_IDENTIFIER" onclick="runCompanyAjax($j('#coname').val());" />
							<spring:message code="company.legalid"/>
						</div>
					</li>
					<li>
						<div>
							<div>
								<a href="#" class="mainlink" onclick="event.preventDefault(); $j('#coname').val(''); runCompanyAjax('%');"><spring:message code="companysearch.listall"/></a>
								<input name="coname" type="text" id="coname" onkeyup="runCompanyAjax(this.value); return false; " autocomplete="off" />
								<!-- this div and spans create the headings for the company list -->
								<div id="companyDivHead">
									<span class="headcomp"><spring:message code="company.company"/></span>
									<span class="headcorole"><spring:message code="company.role"/></span>
									<span class="headlegal"><spring:message code="company.legalid"/></span>
								</div>
																	
								<!--  this div displays ajax results for the company search -->
								<div id="companyDiv"></div>
							</div>
						</div>
					</li>
					<li>
						<input type="button" id="newcompany" value="<spring:message code='companysearch.addnew'/>" onclick="window.location.href='addcompany.htm'" />
					</li>
				</ol>										
			</fieldset>
			</form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>