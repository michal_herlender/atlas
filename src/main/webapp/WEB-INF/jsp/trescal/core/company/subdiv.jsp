<%-- File name: /trescal/core/company/subdiv.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address"%>
<%@ taglib prefix="company" tagdir="/WEB-INF/tags/company"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image"%>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="logistics" tagdir="/WEB-INF/tags/logistics"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<script>
	// if departments are enabled for this company then set department variable to true
	// this is used in the subdiv.js file
	var departments = false;
	<c:if test="${subdiv.comp.companyRole.departmentEnabled}">
		departments = true;
	</c:if>
	
	var itemactivitiestype = [];
	<c:forEach var="iat" items="${itemActivityType}">
		itemactivitiestype.push('${iat}');
	</c:forEach>
</script>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message
				code="company.subdivisioninformation" /> (${subdiv.subname})</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src='script/thirdparty/jQuery/jquery.js' ></script>
		<script src='script/thirdparty/jQuery/jquery.dataTables.min.js' ></script>
		<script src='script/thirdparty/jQuery/diacritics-sort.js' ></script>
		<script src='script/trescal/core/company/SubDiv.js'></script>
		<script type='module' src='script/components/cwms-translate/cwms-translate.js'></script>
		<script type='module' src='script/components/cwms-ef/cwms-ef.js'></script>
	</jsp:attribute>
	<jsp:body>
		<c:if test="${!subdiv.active}">
			<div class="warningBox1" id="warningBoxInactive">
				<div class="warningBox2">
					<div class="warningBox3">
						<div class="center-0">
							${subdiv.subname}
							<spring:message code="company.hasbeendeactivatedsince" />
							<fmt:formatDate type="date" dateStyle="SHORT"
								value="${subdiv.deactTime}" />.
							<spring:message code="company.deactivatewarning2" />: 
							<c:choose>
								<c:when
									test='${subdiv.deactReason == null|| subdiv.deactReason.trim() == ""}'>
									<spring:message code="company.deactivatewarning3" />
								</c:when>
								<c:otherwise>
									${subdiv.deactReason}
								</c:otherwise>
							</c:choose>
						</div>
					</div>
				</div>
			</div>
		</c:if>
		<c:if test="${companysettings.onStop}">
			<div id="stop" class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<div class="center-0">
							<c:out value="${subdiv.comp.coname}"/>&nbsp;
							<spring:message
								code="company.iscurrentlyonstoptorevertthiscompanytotrustedgoto" />
							<cwms:securedLink permission="COMPANY_VIEW" classAttr="mainlink" parameter="?coid=${subdiv.comp.coid}">
								&nbsp;<c:out value="${subdiv.comp.coname}"/>&nbsp;
							</cwms:securedLink>
							<spring:message code="company.profilepage" />
						</div>
					</div>
				</div>
			</div>
		</c:if>
		
		<c:set var="escapedName"><spring:escapeBody javaScriptEscape="true">${subdiv.subname}</spring:escapeBody></c:set>
		
		<!-- this infobox holds general info about the subdivision -->
		<div class="infobox" id="subdivInfoPage">
			<fieldset>
				<legend>${subdiv.comp.coname}<c:out value=" - " />${subdiv.subname}</legend>
				<table style="width: 100%">
					<tr>
						<td style="width: 50%">
							<label><spring:message code="company" />:</label>
							<span><links:companyLinkDWRInfo company="${subdiv.comp}"
									rowcount="0" copy="true" /></span>
						</td>
						<td style="width: 50%">
							<label><spring:message code="company.role" />:</label>
							<span>${subdiv.comp.companyRole.getMessage()}</span>
						</td>
					</tr>
					<tr>
						<td>
							<label><spring:message code="subdivision" />:</label>
							<span>
								<c:if test="${companysettings.onStop}">
									<img src="img/icons/flag_red.png" width="12" height="12"
										alt='<spring:message code="company.subdivisioncompanyonstop"/>'
										title='<spring:message code="company.subdivisioncompanyonstop"/>' />
								</c:if>
								${subdiv.subname} (<cwms:securedLink permission="SUBDIV_EDIT"  classAttr="mainlink"  parameter="?subdivid=${subdiv.subdivid}" collapse="True" >
										<spring:message code="edit"/>
									</cwms:securedLink>)
							</span>
						</td>
						<td>
							<label><spring:message code="company.created" />:</label>
							<span>
								<fmt:formatDate value="${subdiv.createDate}" type="date" dateStyle="medium"/>
</span>
						</td>
					</tr>
					<tr>
						<td>
							<label><spring:message code="company.legacynumber" />:</label>
							<span>${subdiv.formerId}</span>
						</td>
						<td>
							<c:if test="${subdiv.comp.companyRole == 'CLIENT'}">
								<label>
								<image:businessSubdivisionSpecific /><spring:message code="subdiv.settingsforallocatedsubdiv.localbusinesscontact" />:</label>
								<c:if test="${not empty subdivsettings.defaultBusinessContact}">
									<span>
										<links:contactLinkDWRInfo contact="${subdivsettings.defaultBusinessContact}" rowcount="0" />
																	&nbsp;<strong>[${allocatedSubdiv.value}]</strong> 
									</span>
								</c:if>
							</c:if>
							<c:if test="${subdiv.comp.companyRole == 'BUSINESS' and not empty subdiv.businessSettings}">
								<label>
								<image:businessSubdivisionSpecific /><spring:message code="company.defaultbusinesscontact" />:</label>
								<c:if test="${not empty subdiv.businessSettings.defaultBusinessContact}">
									<span>
										<links:contactLinkDWRInfo contact="${subdiv.businessSettings.defaultBusinessContact}" rowcount="0" />
									</span>
								</c:if>
							</c:if>
						</td>
					</tr>
					<tr>
						<td>
							<label><spring:message code="company.active" />:</label>
							<!-- display active text and link to deactivate a subdivision -->
							<c:choose>
								<c:when test="${subdiv.active}">
									<c:set var="activeClass" value="vis" />
									<c:set var="inactiveClass" value="hid" />
								</c:when>
								<c:otherwise>
									<c:set var="activeClass" value="hid" />
									<c:set var="inactiveClass" value="vis" />
								</c:otherwise>
							</c:choose>
							<span id="active" class="${activeClass}">
								<spring:message code="company.active" />
								(<a href="#" class="mainlink"
								onclick=" event.preventDefault(); toggleThickboxContentForActivation('Subdivision', ${subdiv.subdivid}, '${escapedName}', true, createWarningMessage('deactivate', '${escapedName}'), null); "
								title="">
									<spring:message code="company.deactivate" />
								</a>)
							</span>
							<!-- display inactive text and link to activate a subdivision -->
							<span id="inactive" class="${inactiveClass}">
								<spring:message code="company.inactive" />
								(<a href="#" class="mainlink"
								onclick=" event.preventDefault(); toggleThickboxContentForActivation('Subdivision', ${subdiv.subdivid}, '${escapedName}', false, createWarningMessage('activate', '${escapedName}'), null); "
								title="">
									<spring:message code="company.activate" />
								</a>)
							</span>
						</td>
						<td>

						</td>
					</tr>
					<c:if test="${subdiv.comp.companyRole == 'BUSINESS'}">
						<tr>
							<td>
								<label><spring:message code="company.transitcalendar" />:</label>
								<span>
									<cwms:securedLink permission="TRANSIT_CALENDAR_VIEW"  classAttr="mainlink"  parameter="?subdivid=${subdiv.subdivid}&weeks=4" collapse="False" >
										<spring:message code="company.viewtransitcalendar"/>
									</cwms:securedLink>
									 (<spring:message code="company.next4weeks" />)
									 </span>
							</td>
							<td>
								<label><spring:message code="subdiv.analyticalcenter" />:</label>
								<span>${subdiv.analyticalCenter}</span>
							</td>
						</tr>
						<tr>
							<td>
								<label><spring:message code="company.defaultturnaround" />:</label>
								<span>
									<c:if
										test="${not empty subdiv.businessSettings.defaultTurnaround}">
										${subdiv.businessSettings.defaultTurnaround}&nbsp;<spring:message
											code="days" />
									</c:if>
								</span>
							</td>
							<td colspan="2">
								<label><spring:message code="subdiv.code" />:</label>
								<span>${subdiv.subdivCode}</span>
							</td>
						</tr>
					</c:if>
					<tr>
						<%-- Display SIRET number field, even if blank for France, as a prompt to the user that it is blank --%>
						<c:if test="${not empty subdiv.siretNumber || subdiv.comp.country.countryCode == 'FR'}">
							<td colspan="2">
								<label><spring:message code="subdiv.siretnumber" />:</label>
								<span>${subdiv.siretNumber}</span>
							</td>
						</c:if>
					</tr>
				</table>
				<div class="center">
					<spring:message code="company.instruments" />: <a class="mainlink"
						href="viewcompanyinstrument.htm?coid=${subdiv.comp.coid}&subdivid=${subdiv.subdivid}">${instcount}</a>
					&nbsp;&nbsp;&nbsp;
					<spring:message code="company.activejobs" />&nbsp;${allocatedSubdiv.value}: <a
						class="mainlink"
						href="jobsearch.htm?forced=forced&subdivid=${subdiv.subdivid}&activeonly=true">${activejobcount}</a>
					&nbsp;&nbsp;&nbsp;
					<spring:message code="company.totaljobsin" />&nbsp;${allocatedSubdiv.value}: <a
						class="mainlink"
						href="jobsearch.htm?forced=forced&subdivid=${subdiv.subdivid}&activeonly=false">${totaljobcountbysubdiv}</a>
					&nbsp;&nbsp;&nbsp;
					<spring:message code="company.totaljobs" />: ${totaljobcount}
					&nbsp;&nbsp;&nbsp;
					<spring:message code="company.quotations" />: <cwms:securedLink permission="QUOTE_CREATE" classAttr="mainlink" parameter="?forced=forced&subdivid=${subdiv.subdivid}&alltime=true">
																	${quotecount}
																</cwms:securedLink>
					&nbsp;&nbsp;
					(<spring:message code="company.last2years" />: <cwms:securedLink permission="QUOTE_CREATE" classAttr="mainlink" parameter="?forced=forced&subdivid=${subdiv.subdivid}&yearfilter=2">
																	${recentquotecount}
																</cwms:securedLink>)
				</div>
			</fieldset>
		</div>
		<!-- end of general info box -->
		<!-- this infobox contains all active and inactive contacts/addresses for the subdivision -->
		<div class="infobox" id="subdivConAdd">
			<c:set var="activeContactsCount" value="${activecontacts.size()}" />
			<c:set var="inactiveContactsCount" value="${inactivecontacts.size()}" />
			<c:set var="activeAddressesCount" value="${activeaddresses.size()}" />
			<c:set var="inactiveAddressesCount"
				value="${inactiveaddresses.size()}" />
			<c:set var="contactcount" value="${subdiv.contacts.size()}" />
			<c:set var="addresscount" value="${subdiv.addresses.size()}" />
			<!-- div to hold all the tabbed submenu -->
			<div id="tabmenu">
				<!-- tabbed submenu to change between active/inactive contacts/addresses -->
				<ul class="subnavtab">
					<li><a href="#" id="contact-link"
						onclick="event.preventDefault(); switchMenuFocus(menuElements, 'contact-tab', false);"
						<c:if test="${addresscount > 0}">class="selected"</c:if>>
						<spring:message code="contacts" /> (${contactcount})
					</a></li>
					<li><a href="#" id="address-link"
						onclick="event.preventDefault(); switchMenuFocus(menuElements, 'address-tab', false);"
						<c:if test="${addresscount == 0}">class="selected"</c:if>>
						<spring:message code="company.addresses" /> (${addresscount})
					</a></li>
					<li><a href="#" id="bpo-link"
						onclick="event.preventDefault(); switchMenuFocus(menuElements, 'bpo-tab', false);">
						<spring:message code="company.bpos" /> (${bPOsList.size()})
					</a></li>
					<li><a href="#" id="instructions-link"
						onclick="event.preventDefault(); switchMenuFocus(menuElements, 'instructions-tab', false);">
						<spring:message code="company.instructions" /> (${instructions.totalCount})
					</a></li>
					<c:if test="${subdiv.comp.companyRole.departmentEnabled}">
						<li><a href="#" id="department-link"
							onclick="event.preventDefault(); switchMenuFocus(menuElements, 'department-tab', false);">
							<spring:message code="company.departments" />
						</a></li>
						<li><a href="#" id="categories-link"
							onclick="event.preventDefault(); switchMenuFocus(menuElements, 'categories-tab', false);">
							<spring:message code="company.categories" />
						</a></li>
					</c:if>
					<li><a href="#" id="settings-link"
						onclick="event.preventDefault(); switchMenuFocus(menuElements, 'settings-tab', false);">
						<spring:message code="company.settings" />
					</a></li>
					
					<c:set var="env">
						<spring:eval
							expression="@props.getProperty('cwms.config.systemtype')" />
					</c:set>
<%-- 				<c:if test='${(not empty env) and (env eq "development") }'> --%>
						<c:if test="${subdiv.comp.companyRole == 'CLIENT' or subdiv.comp.companyRole == 'BUSINESS'}">
							<li><a href="#" id="exchangeformat-link"
								onclick="event.preventDefault(); switchMenuFocus(menuElements, 'exchangeformat-tab', false); document.querySelector('cwms-ef').refresh()"><spring:message
										code="exchangeformat.tabtitle" /></a></li>
						</c:if>
<%-- 				</c:if> --%>
					
					<c:if
						test='${ subdiv.comp.companyRole eq "CLIENT" and islinktoadveso }'>
						<li><a href="#" id="advesoactivities-link"
							onclick="event.preventDefault(); switchMenuFocus(menuElements, 'advesoactivities-tab', false);">
							<spring:message code="company.advesoactivities" />
						</a></li>
					</c:if>
					<li>
					<a role="button" id="subdivs-link" onclick="event.preventDefault();switchMenuFocus(menuElements, 'subdivs-tab', false);">
                                 <spring:message code="company.subdivs" />
                                                                      						</a>
					</li>
				</ul>
				<!-- div to hold all content which is to be changed using the tabbed submenu -->
				<div class="tab-box">
					<!-- div displays all active/inactive contacts for this subdivision -->
					<div id="contact-tab"
						<c:if test="${addresscount == 0}">class="hid"</c:if>>
						<div id="subnav">
							<dl>
								<dt>
									<a href="#" id="contactActive-link"
										onclick="switchMenuFocus(menuElements2, 'contactActive-tab', false); return false; "
										class="selected">
									<spring:message code="company.activecontacts" /> (${activeContactsCount})
								</a>
								</dt>
								<dt>
									<a href="#" id="contactInactive-link"
										onclick="switchMenuFocus(menuElements2, 'contactInactive-tab', false); return false; ">
									<spring:message code="company.inactivecontacts" /> (${inactiveContactsCount})
								</a>
								</dt>
							</dl>
						</div>
						<div id="contactActive-tab">
							<table class="default2 subdivcon"
								summary="<spring:message code='company.tableactivecontactschosensubdiv'/>">
								<thead>
									<tr>
										<td colspan="8">
											<c:choose>
												<c:when test="${activeContactsCount == 1}">
													<spring:message code="company.hasactivecontact"
														arguments="${subdiv.subname}, ${activeContactsCount}" />
												</c:when>
												<c:otherwise>
													<spring:message code="company.hasactivecontacts"
														arguments="${subdiv.subname}, ${activeContactsCount}" />
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
									<tr>
										<th class="defaultcon" scope="col"><spring:message
												code="company.def" /></th>
										<th class="contact" scope="col"><spring:message
												code="contact" /></th>
										<th class="tel" scope="col"><spring:message
												code="company.tel" /></th>
										<th class="email" scope="col"><spring:message
												code="email" /></th>
										<th class="defadd" scope="col"><spring:message
												code="company.defaddress" /></th>
										<th class="position" scope="col"><spring:message
												code="company.position" /></th>													
										<th class="jobs" scope="col"><spring:message code="jobs" /></th>
										<th class="quotes" scope="col"><spring:message
												code="company.quotes" /></th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<c:choose>
											<c:when test="${subdiv.addresses.size() > 0}">
												<td colspan="8"><a
													href="addperson.htm?subdivid=${subdiv.subdivid}"
													class="mainlink">
													<spring:message code="company.addnewcontact" />
												</a></td>
											</c:when>
											<c:otherwise>
												<td colspan="8"><a href="#" class="mainlink-strike"
													onclick="event.preventDefault();">
													<spring:message code="company.addnewcontact" />
												</a></td>
											</c:otherwise>
										</c:choose>
									</tr>
								</tfoot>
								<tbody>
									<c:choose>
										<c:when test="${activeContactsCount == 0}">
											<tr class="odd">
												<td colspan="8" class="odd center">
													<spring:message
														code="company.thissubdivisionhasnoactivecontacts" />
												</td>
											</tr>
										</c:when>
										<c:otherwise>
											<c:set var="rowcount1" value="1" />
											<c:forEach var="c" items="${activecontacts}">
												<tr
													class=<c:choose><c:when test="${rowcount1 % 2 == 0}">"even"</c:when><c:otherwise>"odd"</c:otherwise></c:choose>>
													<td class="center">
														<c:choose>
															<c:when
																test="${subdiv.defaultContact != null && subdiv.defaultContact.personid == c.personid}">
																<a href="#"><img src="img/icons/user_default.png"
																	width="24" height="16" id="defaultUser"
																	alt="<spring:message code='company.defaultuser'/>"
																	title="<spring:message code='company.defaultuser'/>"
																	onclick="updateSubdivContactDefault(${subdiv.subdivid}, ${c.personid}, this); return false;" /></a>
															</c:when>
															<c:otherwise>
																<a href="#"><img src="img/icons/user.png" width="24"
																	height="16"
																	alt="<spring:message code='company.makeuserdefault'/>"
																	title="<spring:message code='company.makeuserdefault'/>"
																	onclick="updateSubdivContactDefault(${subdiv.subdivid}, ${c.personid}, this); return false;" /></a>
															</c:otherwise>
														</c:choose>
													</td>
													<td class="name"><links:contactLinkDWRInfo
															contact="${c}" rowcount="${rowcount1}" /></td>
													<td class="tel">${c.telephone}</td>
													<td class="email"><a href="mailto:${c.email}"
														class="mainlink">${c.email}</a></td>
													<td class="defadd">${c.defAddress.addr1}<c:if
															test="${c.defAddress.addr1 != null && c.defLocation.location != null}">, ${c.defLocation.location}</c:if></td>
													<td>${c.position}</td>																
													<td class="jobs"><a
														href="jobsearch.htm?forced=forced&contactid=${c.personid}"
														class="mainlink">${c.jobs.size()}</a></td>
													<td class="quotes"><a
														href="createquote.htm?forced=forced&contactid=${c.personid}&alltime=true"
														class="mainlink">${c.quotations.size()}</a></td>
												</tr>
												<c:set var="rowcount1" value="${rowcount1 + 1}" />
											</c:forEach>
										</c:otherwise>
									</c:choose>
								</tbody>
							</table>
						</div>
						<div id="contactInactive-tab" class="hid">
							<table class="default2"
								summary="#springMessage('company.tablelistallinsctivecontacsforsubdiv').">
								<thead class="subdivconinact">
									<tr>
										<td colspan="6">
											<c:choose>
												<c:when test="${inactiveContactsCount == 1}">&nbsp;
													${subdiv.subname} <spring:message code="company.has" />&nbsp;
													${inactiveContactsCount}&nbsp;
													<font style="text-transform: lowercase"><spring:message
															code="company.inactivecontact" /></font>
												</c:when>
												<c:otherwise>
													${subdiv.subname}&nbsp;
													<spring:message code="company.has" />&nbsp;
													${inactiveContactsCount}&nbsp;
													<font style="text-transform: lowercase"><spring:message
															code="company.inactivecontacts" /></font>
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
									<tr>
										<th class="contact" scope="col"><spring:message
												code="contact" /></th>
										<th class="position" scope="col"><spring:message
												code="company.position" /></th>
										<th class="lastact" scope="col"><spring:message
												code="contactadd.lastactivity" /></th>
										<th class="email" scope="col"><spring:message
												code="email" /></th>
										<th class="jobs" scope="col"><spring:message code="jobs" /></th>
										<th class="quotes" scope="col"><spring:message
												code="company.quotes" /></th>
									</tr>
								</thead>
								<tfoot>
									<c:choose>
										<c:when test="${subdiv.addresses.size() > 0}">
											<td colspan="8"><a
												href="addperson.htm?subdivid=${subdiv.subdivid}"
												class="mainlink">
												<spring:message code="company.addnewcontact" />
											</a></td>
										</c:when>
										<c:otherwise>
											<td colspan="8"><a href="#" class="mainlink-strike"
												onclick="event.preventDefault();">
												<spring:message code="company.addnewcontact" />
											</a></td>
										</c:otherwise>
									</c:choose>
								</tfoot>
								<tbody>
									<c:choose>
										<c:when test="${inactiveContactsCount == 0}">
											<tr class="odd">
												<td colspan="6" class="odd center">
													<spring:message
														code="company.thissubdivisionhasnoinactivecontacts" />
												</td>
											</tr>
										</c:when>
										<c:otherwise>
											<c:set var="rowcount2" value="1" />
											<c:forEach var="c" items="${inactivecontacts}">
												<tr
													class=<c:choose><c:when test="${rowcount2%2 == 0}">"even"</c:when><c:otherwise>"odd"</c:otherwise></c:choose>>
													<td class="name"><links:contactLinkDWRInfo
															contact="${c}" rowcount="${rowcount2}" /></td>
													<td>${c.position}</td>
													<td class="center">
								<fmt:formatDate value="${c.lastActivity}" type="date" dateStyle="medium"/>
													</td>
													<td><a href="mailto:${c.email}" class="mainlink">${c.email}</a></td>
													<td class="jobs"><a
														href="jobsearch.htm?forced=forced&contactid=${c.personid}"
														class="mainlink">${c.jobs.size()}</a></td>
													<td class="quotes"><a
														href="createquote.htm?forced=forced&contactid=${c.personid}&alltime=true"
														class="mainlink">${c.quotations.size()}</a></td>
												</tr>
												<c:set var="rowcount2" value="${rowcount2 + 1}" />
											</c:forEach>
										</c:otherwise>
									</c:choose>
								</tbody>
							</table>
						</div>
					</div>
					<!-- end of div which displays all contacts -->
					<!-- div displays all active/inactive addresses for the chosen subdivision -->
					<div id="address-tab"
						<c:if test="${addresscount > 0}">class="hid"</c:if>>
						<div id="subnav">
							<dl>
								<dt>
									<a href="#" id="addressActive-link"
										onclick="switchMenuFocus(menuElements3, 'addressActive-tab', false); return false;"
										class="selected">
									<spring:message code="company.activeaddresses" /> (${activeAddressesCount})
								</a>
								</dt>
								<dt>
									<a href="#" id="addressInactive-link"
										onclick="switchMenuFocus(menuElements3, 'addressInactive-tab', false); return false;">
									<spring:message code="company.inactiveaddresses" /> (${inactiveAddressesCount})
								</a>
								</dt>
							</dl>
						</div>
						<div id="addressActive-tab">
							<table class="default2"
								summary="<spring:message code='company.thistablelistsallactiveaddressesforthechosensubdivision'/>.">
								<thead class="subdivadd">
									<tr>
										<td colspan="6">
											<c:choose>
												<c:when test="${activeAddressesCount == 1}">
													${subdiv.subname}&nbsp;<spring:message code="company.has" />&nbsp;${activeAddressesCount}&nbsp;<font
														style="text-transform: lowercase"><spring:message
															code="company.activeaddress" /></font>
												</c:when>
												<c:otherwise>
													${subdiv.subname}&nbsp;<spring:message code="company.has" />&nbsp;${activeAddressesCount}&nbsp;<font
														style="text-transform: lowercase"><spring:message
															code="company.activeaddresses" /></font>
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
									<tr>
										<th class="defaultadd" scope="col"><spring:message
												code="company.default" /></th>
										<th class="address" scope="col"><spring:message
												code="changesubdiv.headtext" /></th>
										<th class="town" scope="col"><spring:message
												code="address.town" /></th>
										<th class="county" scope="col"><spring:message
												code="address.stateprovinceregion" /></th>
										<th class="postcode" scope="col"><spring:message
												code="address.postcode" /></th>
										<th class="locations" scope="col"><spring:message
												code="company.locations" /></th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<td colspan="6">
											<cwms:securedLink permission="ADDRESS_ADD" collapse="True"  classAttr="mainlink" parameter="?subdivid=${subdiv.subdivid}">
												<spring:message code="company.addnewaddress"/>
											</cwms:securedLink>
										</td>
									</tr>
								</tfoot>
								<tbody>
									<c:choose>
										<c:when test="${activeAddressesCount == 0}">
											<tr>
												<td colspan="6" class="odd center">
													<spring:message
														code="company.thissubdivisionhasnoactiveaddresses" />
												</td>
											</tr>
										</c:when>
										<c:otherwise>
											<c:set var="rowcount3" value="1" />
											<c:forEach var="a" items="${activeaddresses}">
												<tr
													class=<c:choose><c:when test="${rowcount3%2 == 0}">"even"</c:when><c:otherwise>"odd"</c:otherwise></c:choose>>
													<td class="center">
														<c:choose>
															<c:when
																test="${subdiv.defaultAddress != null && subdiv.defaultAddress.addrid == a.addrid}">
																<a href="#"><img src="img/icons/house_default.png"
																	width="24" height="16" id="defaultAddr"
																	alt="<spring:message code='company.defaultaddress'/>"
																	title="<spring:message code='company.defaultaddress'/>"
																	onclick="updateSubdivAddressDefault(${subdiv.subdivid}, ${a.addrid}, this); return false;" /></a>
															</c:when>
															<c:otherwise>
																<a href="#"><img src="img/icons/house.png"
																	width="24" height="16"
																	alt="<spring:message code='company.makeaddressdefault'/>"
																	title="<spring:message code='company.makeaddressdefault'/>"
																	onclick="updateSubdivAddressDefault(${subdiv.subdivid}, ${a.addrid}, this); return false;" /></a>
															</c:otherwise>
														</c:choose>
													</td>
													<td>
														<c:choose>
															<c:when test="${companysettings.active && subdiv.active}">
																<c:set var="linkClass" value="mainlink" />
															</c:when>
	
															<c:otherwise>
																<c:set var="linkClass" value="mainlink-strike" />
															</c:otherwise>
														</c:choose>
														<cwms:securedLink permission="ADDRESS_VIEW" classAttr="${linkClass}" parameter="?addrid=${a.addrid}">
															 <c:set var="ListeAddress" value=" ${a.addr1},${a.addr2},${a.addr3}"/>
															 <c:set var="ListeAddressSplit" value="${fn:split(ListeAddress, ',')}" />
															 <c:out value="${fn:join(ListeAddressSplit, ', ')}" />

														</cwms:securedLink>
														<address:showAddress address="${a}" vis="false"
															separator="<br/>" copy="true" />
													</td>
													<td>${a.town}</td>
													<td>${a.county}</td>
													<td class="center">${a.postcode}</td>
													<td class="center">
														<!-- hover over this anchor and a tooltip is displayed showing the contents of the div created below -->
														<a href="?id=#locations${a.addrid}&amp;width=250"
														title="Locations for ${a.addr1}" id="tip${a.addrid}"
														class="jTip" onclick="return false;">
															<img src="img/icons/location.png" width="16" height="16"
															alt="" />
														</a>
														<!-- contents of this div are displayed in tooltip created above -->
														<div id="locations${a.addrid}" class="hid">
															<c:choose>
																<c:when test="${a.locations.size() > 0}">
																	<ol>
																		<c:forEach var="location" items="${a.locations}">
																			<li>${location.location}</li>
																		</c:forEach>
																	</ol>
																</c:when>
																<c:otherwise>
																	<div class="center">
																		<spring:message code="company.nolocationssaved" />
																	</div>
																</c:otherwise>
															</c:choose>
														</div>
														<!-- end of tooltip content -->
													</td>
												</tr>
												<c:set var="rowcount3" value="${rowcount3 + 1}" />
											</c:forEach>
										</c:otherwise>
									</c:choose>
								</tbody>
							</table>
						</div>
						<div id="addressInactive-tab" class="hid">
							<table class="default2"
								summary="#springMessage('company.tableaddressesforsubdiv').">
								<thead class="subdivaddinact">
									<tr>
										<td colspan="5">
											<c:choose>
												<c:when test="${inactiveAddressesCount == 1}">
													${subdiv.subname} <spring:message code="company.has" /> ${inactiveAddressesCount} <font
														style="text-transform: lowercase"><spring:message
															code="company.inactiveaddress" /></font>
												</c:when>
												<c:otherwise>
													${subdiv.subname} <spring:message code="company.has" /> ${inactiveAddressesCount} <font
														style="text-transform: lowercase"><spring:message
															code="company.inactiveaddresses" /></font>
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
									<tr>
										<th class="address" scope="col"><spring:message
												code="address" /></th>
										<th class="town" scope="col"><spring:message
												code="address.town" /></th>
										<th class="county" scope="col"><spring:message
												code="address.county" /></th>
										<th class="postcode" scope="col"><spring:message
												code="address.postcode" /></th>
										<th class="locations" scope="col"><spring:message
												code="company.locations" /></th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<td colspan="5">
											<cwms:securedLink permission="ADDRESS_ADD" collapse="True" classAttr="mainlink" parameter="?subdivid=${subdiv.subdivid}">
												<spring:message code="company.addnewaddress"/>
											</cwms:securedLink>
										</td>
									</tr>
								</tfoot>
								<tbody>
									<c:choose>
										<c:when test="${inactiveAddressesCount == 0}">
											<tr>
												<td colspan="5" class="odd center">
													<spring:message
														code="company.thissubdivisionhasnoinactiveaddresses" />
												</td>
											</tr>
										</c:when>
										<c:otherwise>
											<c:set var="rowcount4" value="1" />
											<c:forEach var="a" items="${inactiveaddresses}">
												<tr
													class=<c:choose><c:when test="${rowcount4%2 == 0}">"even"</c:when><c:otherwise>"odd"</c:otherwise></c:choose>>
													<c:choose>
														<c:when
															test="${companysettings.active && settings.active && a.active}">
															<c:set var="linkClass" value="mainlink" />
														</c:when>
														<c:otherwise>
															<c:set var="linkClass" value="mainlink-strike" />
														</c:otherwise>
													</c:choose>
													<td>
													<cwms:securedLink permission="ADDRESS_VIEW" classAttr="${linkClass}" parameter="?addrid=${a.addrid}">
															 <c:set var="ListeAddress" value=" ${a.addr1},${a.addr2},${a.addr3}"/>
															 <c:set var="ListeAddressSplit" value="${fn:split(ListeAddress, ',')}" />
															 <c:out value="${fn:join(ListeAddressSplit, ', ')}" />
													</cwms:securedLink>
													</td>
													<td>${a.town}</td>
													<td>${a.county}</td>
													<td class="center">${a.postcode}</td>
													<td class="center">
														<!-- hover over this anchor and a tooltip is displayed showing the contents of the div created below -->
														<a href="?id=#locations${a.addrid}&amp;width=250"
														title="Locations for ${a.addr1}" id="tip${a.addrid}"
														class="jTip" onclick="return false;">
															<img src="img/icons/location.png" width="16" height="16"
															alt="" />
														</a>
														<!-- contents of this div are displayed in tooltip created above -->
														<div id="locations${a.addrid}" class="hid">
															<c:choose>
																<c:when test="${a.locations.size() > 0}">
																	<ol>
																		<c:forEach var="location" items="${a.locations}">
																			<li>${location.location}</li>
																		</c:forEach>
																	</ol>
																</c:when>
																<c:otherwise>
																	<div class="center">
																		<spring:message code="company.nolocationssaved" />
																	</div>
																</c:otherwise>
															</c:choose>
														</div>
													</td>
												</tr>
												<c:set var="rowcount4" value="${rowcount4 + 1}" />
											</c:forEach>
										</c:otherwise>
									</c:choose>
								</tbody>
							</table>
						</div>
					</div>
					<!-- end of div to display all addresses -->
					<!-- div displays all blanket purchase orders for this subdivision -->
					<div id="bpo-tab" class="hid">
						<company:bpos  bpos="${bPOsList}" name="${subdiv.subname}"  scope="SUBDIV" id="${subdiv.subdivid}"/>
					</div>
					<!-- end of div to display all blanket purchase orders -->
					<c:if test="${subdiv.comp.companyRole.departmentEnabled}">
						<!-- this section displays all departments for the company if departments are enabled -->
						<div id="department-tab" class="hid">
							<table class="default2"
								summary="This table lists all departments within the chosen subdivision.">
								<thead class="subdivdep">
									<tr>
										<td colspan="4">
											${subdiv.subname}&nbsp;<spring:message
												code="company.departments" /> (<span id="depCount">${subdiv.departments.size()}</span>)
										</td>
									</tr>
									<tr>
										<th class="department" scope="col"><spring:message
												code="company.department" /></th>
										<th class="deptype" scope="col"><spring:message
												code="company.type" /></th>
										<th class="depcontacts" scope="col"><spring:message
												code="company.noofcontacts" /></th>
										<th class="depdelete" scope="col"><spring:message
												code="delete" /></th>
									</tr>
								</thead>
								<tfoot>
									<c:if test="${allowAddDepts}">
										<tr>
											<td colspan="4"><a
												href="department.htm?subdivid=${subdiv.subdivid}"
												class="mainlink"><spring:message
														code="company.adddepartment" /></a></td>
										</tr>
									</c:if>
								</tfoot>
								<tbody id="deptbody">
									<c:choose>
										<c:when test="${subdiv.departments.size() == 0}">
											<tr>
												<td colspan="4" class="odd center">
													<spring:message
														code="company.thissubdivisionhasbeenassignednodepartments" />
												</td>
											</tr>
										</c:when>
										<c:otherwise>
											<c:set var="rowcount4" value="1" />
											<c:forEach var="department" items="${subdiv.departments}">
												<tr
													class=<c:choose><c:when test="${rowcount4%2 == 0}">"even"</c:when><c:otherwise>"odd"</c:otherwise></c:choose>
													id="department${department.deptid}">
													<td>
														<a href="department.htm?deptid=${department.deptid}"
														class="mainlink">${department.name}</a>
													</td>
													<td><spring:message
															code="${department.type.messageCode}" /></td>
													<td class="center">
														<c:set var="activeMembers" value="0" />
														<c:forEach var="dm" items="${department.members}">
															<c:if test="${dm.contact.active}">
																<c:set var="activeMembers" value="${activeMembers + 1}" />
															</c:if>
														</c:forEach>
														${activeMembers}
													</td>
													<td class="center">
															<c:if test="${allowAddDepts}">
														<a href="#"
														onclick="deleteDep(${department.deptid}); return false;">
															<img src="img/icons/delete.png" height="16" width="16"
															alt="<spring:message code='company.deletedepartment'/>"
															title="<spring:message code='company.deletedepartment'/>"
															class="delimg" />
														</a>
														</c:if>
													</td>
												</tr>
												<c:set var="rowcount4" value="${rowcount4 + 1}" />
											</c:forEach>
										</c:otherwise>
									</c:choose>
								</tbody>
							</table>
						</div>
						<!-- end of departments section -->
						<!-- div displays all procedure categories for this subdivision (business company only) -->
						<div id="categories-tab" class="hid">
							<company:procedureCategories subdiv="${subdiv}"
								procedureCategories="${procedureCategories}"
								editable="${allowEditCategories}" />
						</div>
					</c:if>
					<!-- div displays all instructions for this company -->
					<div id="instructions-tab" class="hid">
						<company:instructions command="${instructions}" />
					</div>					
					<!-- div displays all default settings for this subdivision -->
					<div id="settings-tab" class="hid">
						<company:systemDefaults parentEntity="${subdiv}"
							entityKey="${subdiv.subdivid}" scope="${defaultrole}"
							systemdefaults="${systemdefaulttypes}" />
					</div>
					
					<!-- div displays all exchange formats for this subdivision -->
					<c:if test="${subdiv.comp.companyRole == 'CLIENT' or subdiv.comp.companyRole == 'BUSINESS'}">
						<div id="exchangeformat-tab" class="hid">
							<%-- <logistics:showExchangeFormats --%>
							<%-- exchangeformats="${exchangeformatsList}" organisation="${subdiv}" /> --%>
							<cwms-ef subdiv-id="${subdiv.subdivid}" company-id="${subdiv.comp.coid}" company-role="${subdiv.comp.companyRole}" ${hasPermisionOnEF ? 'permission' : ''} >
								<h3 style="	color: red;text-align: center"> Please use newer versions of chrome or firefox  </h3>
							</cwms-ef>
						</div>
					</c:if>
					
					<!-- div displays all adveso activities for this subdivision -->
					<c:if
						test='${ subdiv.comp.companyRole eq "CLIENT" and islinktoadveso }'>
						<div id="advesoactivities-tab" class="hid">
							<company:manageActivities subdivid="${subdiv.id}"
								activities="${activities}"
								itemactivitytype="${itemActivityType}" />
						</div>
					</c:if>

					<div id="subdivs-tab" class="hid">
					    <table class="default2">
					    <thead>
					    <tr>
					    <th colspan="4">
					        Default business contacts for subdivision ${subdiv.subname}
					    </th>
					    </tr>
					    <tr>
					        <th><spring:message code="subdiv.allocatedFor" text="Allocated"/></th>
					        <th><spring:message code="company.defaultbusinesscontact" /></th>
					        <th><spring:message code="telephone" /></th>
					        <th><spring:message code="email" />
					    </tr>
					    </thead>
					    <tbody>
					        <c:forEach var="item" items="${subdivs}">
					        <tr>
					        <td>${item.allocatedForName}</td>
					        <td>
	<a href="viewperson.htm?personid=${item.contactId}&amp;ajax=contact&amp;width=380" class="jconTip mainlink" title="<spring:message code='company.contactinformation' />" id="contact${item.contactId}${rowcount}" tabindex="-1">${item.contactName}</a>
	<links:showRolloverMouseIcon/>
					        </td>
					        <td><a href="tel:${item.contactPhone}">${item.contactPhone}</a></td>
					        <td><a href="mailto:${item.contactEmail}">${item.contactEmail}</a></td>
					        </tr>
					        </c:forEach>
					    </tbody>
					    </table>
					</div>
				</div>
				<!-- end of div to hold all tabbed submenu content -->
			</div>
			<!-- end of div to hold tabbed submenu -->
			<!-- this div clears floats and restores page flow -->
			<div class="clear"></div>
		</div>
		<!-- end of active and inactive contacts/addresses infobox -->
	</jsp:body>
</t:crocodileTemplate>