<%-- File name: /trescal/core/company/addcompanylistmember.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<form:form action="addcompanylistmember.htm" id="addmembertocompanylist" method="post" modelAttribute="editcompanylistform">
	<form:input type='hidden' path='companylistId' value='${companylist.id}'/>
	<fieldset> 
		<ol>
			<li>
				<center><strong><c:out value="${companylist.name}"/></strong></center>
			</li>
			
			<li>
				<label><spring:message code="company.role"/>:</label>  
				<span><spring:message code="companyrole.supplier"/>, 
					<spring:message code="companyrole.business"/>
				</span>
			</li>
			
			<li>
				<label for="memberCoid"><spring:message code="companylist.member"/>:</label> 
				<br><form:select path="memberCoid" items="${possiblemembers}" itemValue="key" itemLabel="value"/>
			</li>
			
			<li>
				<label for="activestatus"><spring:message code="companylistsearch.activestatus" />:</label>
				<br><form:select path="activestatus">
					<option value="true" selected><spring:message code="yes" /></option>
					<option value="false"><spring:message code="no" /></option>
				</form:select>
			</li>
		</ol>

	</fieldset>
</form:form>