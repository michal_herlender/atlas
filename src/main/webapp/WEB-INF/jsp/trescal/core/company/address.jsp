<%-- File name: /trescal/core/company/address.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>

<script src='script/trescal/core/company/Address.js'></script>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="address.viewdetails"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type="module" src="script/components/cwms-address-transportoptions/cwms-address-transportoptions.js" ></script>
	</jsp:attribute>
	<jsp:body>
		<!-- infobox div with nifty corners applied -->
		<div class="infobox">
			<fieldset>

				<legend><spring:message code="address.viewdetails"/></legend>
				
				<!-- displays content in column 49% wide (left) -->
				<div class="displaycolumn">
				
					<ol>
						<li>
							<label class="width40"><image:globalSetting/><spring:message code="company"/>:</label>
							<span><links:companyLinkDWRInfo company="${address.sub.comp}" rowcount="0" copy="true"/></span>
						</li>
						<li>
							<label class="width40"><image:globalSetting/><spring:message code="subdiv"/>:</label>
							<span><links:subdivLinkDWRInfo subdiv="${address.sub}" rowcount="0"/></span>
						</li>
						<li>
							<label class="width40"><image:globalSetting/><spring:message code="address.formattedaddress"/>:</label>
							<div class="float-left">
								<c:forEach var="addressLine" items="${formattedAddress}">
									<c:out value="${addressLine}"/><br/>
								</c:forEach>
							</div>
							&nbsp;&nbsp;
							<img src="img/icons/bullet_clipboard.png" width="10" height="10" onclick=" $j(this).parent().find('div').clipBoard(this, null, false, null); " alt="<spring:message code='addresstag.toclipboard'/>" title="<spring:message code='addresstag.toclipboard'/>" />
							<div class="clear"></div>
						</li>
						<li>
							<label class="width40"><image:globalSetting/><spring:message code="address.latitude"/>:</label>
							<span><c:out value="${address.latitude}" /></span>
						</li>
						<li>
							<label class="width40"><image:globalSetting/><spring:message code="address.longitude"/>:</label>
							<span><c:out value="${address.longitude}" /></span>
						</li>
						<li>
							<label class="width40"><image:businessCompanySpecific/><spring:message code="address.vatratefrom"/>:</label>
							<span>
								<c:choose>
									<c:when test="${vatRateOverride}">
										<spring:message code="address"/>
									</c:when>
									<c:otherwise>
										<spring:message code="company"/>
									</c:otherwise>
								</c:choose>
							</span>
						</li>
						<li>
							<label class="width40"><image:businessCompanySpecific/><spring:message code="company.vatrate"/>:</label>
							<span>
								<c:choose>
									<c:when test="${not empty vatRate}">
										${vatRate}
									</c:when>
									<c:otherwise>
										<spring:message code="company.nonespecified"/>
									</c:otherwise>
								</c:choose>
							
							</span>
						</li>
					</ol>

				</div>
				<!-- end of left column -->

				<!-- displays content in column 49% wide (right) -->
				<div class="displaycolumn">
					<ol>
						<li>
							<label class="width40"><image:globalSetting/><spring:message code="company.active"/>:</label>
							<span>
								<c:choose>
									<c:when test="${address.active}">
										<spring:message code="company.active"/>
									</c:when>
									<c:otherwise>
										<spring:message code="company.inactive"/>
									</c:otherwise>
								</c:choose>
							</span>
							<span class="float-right">

							<cwms:securedLink permission="ADDRESS_EDIT"  classAttr="mainlink"  parameter="?addrid=${address.addrid}" collapse="True">
								<spring:message code="address.edit"/>
							</cwms:securedLink>
							</span>
						</li>
						<li>
							<label class="width40"><image:globalSetting/><spring:message code="company.addresstypes"/>:</label>
							<div class="float-left">
								<c:forEach var="addressType" items="${address.addressType}">
									${addressType.getType()}<br/>
								</c:forEach>
							</div>
							<div class="clear"></div>							
						</li>
						<li>
							<label class="width40"><image:globalSetting/><spring:message code="address.costcentre"/>:</label>
							<span>${address.costCentre}</span>
						</li>
						<li>
							<label class="width40"><image:globalSetting/><spring:message code="company.tel"/>:</label>
							<span>${address.telephone}</span>
						</li>
						<li>
							<label class="width40"><image:globalSetting/><spring:message code="company.fax"/>:</label>
							<span>${address.fax}</span>
						</li>
						<%-- Printers - for business companies only --%>
						<c:if test="${address.sub.comp.companyRole == 'BUSINESS'}">
						<li>
							<label class="width40"><image:globalSetting/><spring:message code="address.defaultprinter"/>:</label>
							<span>
								<c:choose>
									<c:when test="${not empty address.defaultPrinter}">
										${address.defaultPrinter.description}
									</c:when>
									<c:otherwise>
										<spring:message code="company.nonespecified"/>
									</c:otherwise>
								</c:choose>
							</span>
						</li>
						<li>
							<label class="width40"><image:globalSetting/><spring:message code="address.defaultlabelprinter"/>:</label>
							<span>
								<c:choose>
									<c:when test="${not empty address.defaultLabelPrinter}">
										${address.defaultLabelPrinter.description}
									</c:when>
									<c:otherwise>
										<spring:message code="company.nonespecified"/>
									</c:otherwise>
								</c:choose>
							</span>
							<span></span>
						</li>
						</c:if>
					</ol>
				</div>
				<!-- end of right column -->
			</fieldset>
		</div>

		<!-- div to hold all the tabbed submenu -->
		<div class="infobox">
			<div id="tabmenu">
				<!-- tabbed submenu to change between address details -->
				<ul class="subnavtab">
					<li><a href="#" id="transport-link" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'transport-tab', false); " class="selected">
						<spring:message code="transport"/>
					</a></li>
					<li><a href="#" id="locations-link" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'locations-tab', false); ">
						<spring:message code="company.locations"/> (${address.locations.size()})
					</a></li>
					<c:if test="${usesPlantillas}">
						<li><a href="#" id="plantillas-link" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'plantillas-tab', false); ">
							<%-- Intentionally not translated - it is the name of the system! --%>
							Plantillas
						</a></li>
					</c:if>
				</ul>
			</div>
			<!-- div to hold all content which is to be changed using the tabbed submenu -->
			<div class="tab-box">
				<!-- div displays all transport options for this address (edit the options for my own business company) -->
				<div id="transport-tab">
					<cwms-address-transportoptions addressid="${address.addrid}"></cwms-address-transportoptions>
				</div>
				<!-- div displays all locations for this address -->
				<div id="locations-tab" class="hid">
					<address:locationsTable address="${address}" instrumentCountMap="${instrumentCountMap}" instrumentCount="${instrumentCount}"/>
				</div>
				<c:if test="${usesPlantillas}">
					<!-- div displays plantillas for this address -->
					<div id="plantillas-tab" class="hid">
						<address:plantillasTable address="${address}" plantillasSettings="${plantillasSettings}" syncToPlantillas="${syncToPlantillas}" usesPlantillas="${usesPlantillas}" />
					</div>
				</c:if>	
				<!-- end of div to hold all tabbed submenu content -->
			</div>
			<!-- end of div to hold tabbed submenu -->
			<!-- this div clears floats and restores page flow -->
			<div class="clear"></div>
		</div>
		<!-- end of active and inactive contacts/addresses infobox -->
	</jsp:body>
</t:crocodileTemplate>