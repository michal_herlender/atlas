<%-- File name: /trescal/core/company/locationadd.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="company.addeditlocation"/></span>
        
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script>
	    var focusElement = "locationname"
        </script>
    </jsp:attribute>
    <jsp:body>
    <div class="infobox" id="locationadd">
	<form:form method="post" modelAttribute="command">
		<fieldset>
			<legend><spring:message code="company.addeditlocation"/></legend>
			<ol>
				<li>
					<label><spring:message code="address"/>:</label>  
					<span>
						<a href="viewaddress.htm?loadtab=locations-tab&addrid=${command.add.addrid}">${command.add.addr1}</a>
					</span>
				</li>
				<li>
					<label><spring:message code="company.location"/>:</label>  
					<span>
						<form:input path="location" size="100" id="locationname"/>
						<form:errors path="location" class="attention"/>
					</span>
				</li>
				<li>
					<label><spring:message code="company.active"/>:</label>
					<span>
						<c:set var="checked" value=""/>
						<c:if test="${command.active}">
							<c:set var="checked" value="checked"/>
						</c:if>
						<form:checkbox path="active" value="true" checked="${checked}"/>
						<input type="hidden" name="_active" value="false" />
					</span>
				</li>
				<li>
					<label>&nbsp;</label>
					<span><input type="submit" value="<spring:message code='submit'/>"></span>
				</li>
			</ol>
		</fieldset>
	</form:form>
</div>
<p/>

  </jsp:body>
</t:crocodileTemplate>