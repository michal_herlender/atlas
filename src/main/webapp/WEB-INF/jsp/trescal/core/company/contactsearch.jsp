<%-- File name: /trescal/core/company/contactsearch.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="company.contactbrowser"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/company/ContactSearch.js'></script>
		<script>
			var ALLCOROLES = [];
			<c:forEach var="companyRole" items="${companyRoles}">
				ALLCOROLES.push('${companyRole}');
			</c:forEach>
		</script>
	</jsp:attribute>
	<jsp:body>
		<div id="contactsearch" class="infobox">
			<fieldset>
				<legend><spring:message code="company.contactbrowser"/></legend>
				<ol>
					<li id="contCoroles">
						<h5><spring:message code="companysearch.companyroles"/></h5>
						<c:forEach var="companyRole" items="${companyRoles}">
							<c:if test="${companyRole.getKey() > 0}">
								<input type="checkbox" class="checkbox cbrole" id="${companyRole}" name="${companyRole}" value="${companyRole}" onclick="onClickCompanyRole(); runContactAjax($j('#firstName').val(), $j('#lastName').val());" <c:if test="${companyRole.getKey() == 1}">checked</c:if> />
								<a href="#" title="${companyRole}" class="thinlink" onclick="event.preventDefault(); selectOnly('${companyRole}'); runContactAjax($j('#firstName').val(), $j('#lastName').val());">${companyRole.getMessage()}</a>
							</c:if>
						</c:forEach>
						&nbsp;&nbsp;&nbsp;
						<input id="searchAll" type="button" style="width: 100px" onclick="event.preventDefault(); selectAll(); runContactAjax($j('#firstName').val(), $j('#lastName').val());" value="<spring:message code='companysearch.searchall'/>"/>
						<input id="searchDefault" type="hidden" style="width: 100px" onclick="event.preventDefault(); selectDefault(); runContactAjax($j('#firstName').val(), $j('#lastName').val());" value="<spring:message code='companysearch.searchdefault'/>"/>
					</li>
					<li>
						<div class="center">
							<spring:message code="company.searchdeactivatedcontacts"/>?
							<input type="checkbox" class="checkbox" name="active" id="active" onclick="runContactAjax($j('#firstName').val(), $j('#lastName').val());"/>
						</div>
					</li>
					<li>
						<div>
							<div>
								<!-- first and last name search fields call dwr function to retrieve all matches -->
								<div class="searchfields">
									<spring:message code="company.firstname"/>: <input name="firstName" type="text" id="firstName" value="" onkeyup="runContactAjax(this.value, $j('#lastName').val()); return false;" autocomplete="off" /> <spring:message code="company.surname"/>: <input name="lastName" type="text" id="lastName" value="" onkeyup="runContactAjax($j('#firstName').val(), this.value); return false;" autocomplete="off" />
								</div>
								<!-- this div and spans create the headings for the contact list -->						
								<div id="contactDivHead">
									<span class="headname"><spring:message code="company.contactname"/></span>
									<span class="headcomp"><spring:message code="company.companysubdivisionrole"/></span>
								</div>
								<!--  this div displays ajax results for the contact search -->
								<div id="contactDiv"></div>
							</div>
							<!-- this span displays the loading image and text when a search has been activated  -->
							<span id="loading" class="hid">
								<img src="img/icons/loading-anim.gif" class="img_marg_bot" height="18" width="80" alt="" />
							</span>
						</div>
					</li>
				</ol>
			</fieldset>
		</div>
	</jsp:body>
</t:crocodileTemplate>