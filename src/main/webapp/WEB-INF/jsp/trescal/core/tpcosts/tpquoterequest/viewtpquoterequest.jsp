<%-- File name: /trescal/core/tpcosts/tpquoterequest/viewtpquoterequest.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="costs" tagdir="/WEB-INF/tags/costs" %>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="tpcosts.thirdpartyquotationrequest" /></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/tpcosts/tpquoterequest/ViewTPQuoteRequest.js'></script>
		<script type='text/javascript' src='script/thirdparty/jQuery/jquery.js' ></script>
		<script type='module' src='script/components/cwms-upload-file/cwms-upload-file.js'></script>
		<script type="text/javascript">
		// initialise cost type array
			var costtypes = [];
			<c:forEach var="ct" items="${costtypes}">
			// add cost types to array
				costtypes.push({typeid: ${ct.typeid},	name: '<spring:message code="${ct.messageCode}"/>'});
			</c:forEach>
			
			function updateInstructionSize(instructionSize) {
				$j("span.instructionSize").text(instructionSize);
			}
		</script>
		<script type="module" src="script/components/cwms-instructions/cwms-instructions.js"></script>
	</jsp:attribute>
	<jsp:attribute name="globalElements">
		<cwms-upload-file system-component-id="${sc.componentId}" entity-id1="${tpquoterequest.requestNo}" entity-id2=""
 			files-name="${filesName}" rename-to="" web="false"></cwms-upload-file>
	</jsp:attribute>
	<jsp:body>
			<c:if test="${tpQuotes.size() > 0}">
				<div class="successBox1">
					<div class="successBox2">
						<div class="successBox3">
							<spring:message code="tpcosts.thefollowingthirdpartyquotationshavebeencreatedfromthisrequest" />
							<c:forEach var="tpq" items="${tpQuotes}" varStatus="tpqStatus">
								<c:choose>
									<c:when test="${tpqStatus.index == 1}">
										<a href="viewtpquote.htm?id=${tpq.id}" class="mainlink" target="_blank"><c:out value="${pq.qno}"/></a>
									</c:when>
									<c:otherwise>
									, <a href="viewtpquote.htm?id=${tpq.id}" class="mainlink" target="_blank"><c:out value="${tpq.qno}"/></a>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</div>
					</div>
				</div>
			 </c:if>

		<form:errors path="tpquoterequest.*">
            <div class="warningBox1">
                <div class="warningBox2">
                     <div class="warningBox3">
                    	<spring:message code="tpcosts.warningyourchangeswerenotsavedduetooneormoreproblems" />
                      <c:forEach var="e" items="${messages}">
                        <div class="center attention"><c:out value="${e}"/></div>
                      </c:forEach>
                     </div>
                </div>
           </div>
         </form:errors>
							
		<!-- this div displays all information available for the third party quotation -->				
		<div class="infobox" id="viewtpquoterequest">
		
			<fieldset>
			
				<legend><spring:message code="tpcosts.thirdpartyquotationrequest" /></legend>
			
				<!-- displays content in column 49% wide (left) -->
				<div class="displaycolumn">
				
					<ol>
					
						<li>
							<label><spring:message code="company" />:</label>
							<span><links:companyLinkDWRInfo copy="true" company="${tpquoterequest.contact.sub.comp}" rowcount="0" /></span>
						</li>
						<li>
							<label><spring:message code="subdiv" />:</label>
							<span><c:out value="${tpquoterequest.contact.sub.subname}"/></span>
						</li>
						<li>
							<label><spring:message code="contact" />:</label>
							<span><links:contactLinkDWRInfo contact="${tpquoterequest.contact}" rowcount="0" /></span>
						</li>
						<li>
							<label><spring:message code="currency" />:</label>
							<c:set var="currency" value="${tpquoterequest.currency}" />
							<span>
								<c:out value="${currency.currencyCode} (${currency.currencySymbol} ${currency.currencyName})"/>
							</span>
						</li>
						<li>
							<label><spring:message code="tpcosts.requestfrom" />:</label>
							<span>
								<c:choose>
									<c:when test="${!empty tpquoterequest.requestFromQuote}">
										<spring:message code="quotation" /> 
										<a href="<spring:url value='viewquotation.htm?id=${tpquoterequest.requestFromQuote.id}'/>" class="mainlink"><c:out value="${tpquoterequest.requestFromQuote.qno}"/>
											<spring:message code="tpcosts.version" /> <c:out value="${tpquoterequest.requestFromQuote.ver}"/>
										</a>
									</c:when>
									<c:when test="${!empty tpquoterequest.requestFromJob}">
										<spring:message code="job" />&nbsp;
										<links:jobnoLinkDWRInfo rowcount="0" jobno="${tpquoterequest.requestFromJob.jobno}" jobid="${tpquoterequest.requestFromJob.jobid}" copy="true" />
									</c:when>
									<c:otherwise>
										<spring:message code="tpcosts.nosourcemanuallyentere" />.
									</c:otherwise>
								</c:choose>
							</span>
						</li>
						<li>
							<label><spring:message code="tpcosts.requestno" />:</label>
							<span><c:out value="${tpquoterequest.requestNo}"/></span>
						</li>											
																	
					</ol>
				
				</div>
				
				<!-- displays content in column 49% wide (right) -->
				<div class="displaycolumn">
				
					<ol>
					
						<li>
							<label><spring:message code="status" />:</label>
							<span class="bold"><t:showTranslationOrDefault translations="${tpquoterequest.status.nametranslations}" defaultLocale="${defaultlocale}"/></span>
						</li>
						<li>
							<label><spring:message code="tpcosts.registeredby" />:</label>
							<span><c:out value="${tpquoterequest.createdBy.name}"/></span>
						</li>
						<li>
							<label><spring:message code="tpcosts.registeredon" />:</label>
							<span>
								<fmt:formatDate type="date" dateStyle="medium" value="${tpquoterequest.regdate}"/>
							</span>
						</li>
						<li>
							<label><spring:message code="issuedate" />:</label>
							<span><fmt:formatDate value="${tpquoterequest.issuedate}" dateStyle="medium" type="date"/></span>
						</li>
						<li>
							<label><spring:message code="tpcosts.issuedby" />:</label>
							<span>
								<c:out value="${tpquoterequest.issueby.name}"/> 
								<c:if test="${tpquoterequest.issued != true}">
									<a href="<spring:url value='viewtpquoterequest.htm?submission=submit&amp;id=${tpquoterequest.id}&amp;issuing=true'/>" class="mainlink">Issue Request</a>
								</c:if>
							</span>
						</li>
						<li>
							<label><spring:message code="duedate" />:</label>
							<span><fmt:formatDate value="${tpquoterequest.dueDate}" dateStyle="medium" type="date"/></span>
						</li>
						
					</ol>
					
				</div>
			
			</fieldset>
			
		</div>
		<!-- end of quote info div -->
		
		<!-- this div contains all subnavigation links -->
		<div id="subnav">
			<dl>
				<dt><a href="#" onclick="event.preventDefault(); $j('#newitemoptions').removeClass().addClass('hid'); switchMenuFocus(menuElements, 'tpquoteitems-tab', false); " id="tpquoteitems-link" title="<spring:message code='tpcosts.viewthirdpartyquoterequestitems' />" class="selected"><spring:message code="tpcosts.quoteitems" /> (${tpquoterequest.items.size()})</a></dt>
				<c:choose>
					<c:when test="${empty tpquoterequest.requestFromQuote || tpquoterequest.requestFromJob}">
					<dt><a href="#" onclick="$j('#newitemoptions').removeClass().addClass('vis'); return false;" title="<spring:message code='tpcosts.additemtothirdpartyquoterequest' />"><spring:message code="tpcosts.newitem" /></a></dt>
					</c:when>
					<c:otherwise>
					<dt><a href="<spring:url value='tpquotereqadditemmodel.htm?id=${tpquoterequest.id}'/>" title="<spring:message code='tpcosts.additemtothirdpartyquoterequest' />"><spring:message code="tpcosts.newitem" /></a></dt>
					</c:otherwise>
				</c:choose>
					<dt><a href="#" onclick="event.preventDefault(); $j('#newitemoptions').removeClass().addClass('hid'); switchMenuFocus(menuElements, 'quotedocs-tab', false); " id="quotedocs-link" title="<spring:message code='tpcosts.viewdocumentsinthirdpartyquoterequestdirectory' />"><spring:message code="documents" /> (${scRootFiles.numberOfFiles})</a></dt>
					<dt><a href="#" onclick="event.preventDefault(); $j('#newitemoptions').removeClass().addClass('hid'); switchMenuFocus(menuElements, 'quoteemails-tab', false); " id="quoteemails-link" title="<spring:message code='tpcosts.viewsendemailsforthirdpartyquoterequest' />"><spring:message code="emails" /> (<span class="entityEmailDisplaySize">${tpquoterequest.sentEmails.size()}</span>)</a></dt>
					<dt><a href="#" onclick="event.preventDefault(); $j('#newitemoptions').removeClass().addClass('hid'); switchMenuFocus(menuElements, 'editquote-tab', false); " id="editquote-link" title="<spring:message code='tpcosts.editthirdpartyquoterequestdetails' />"><spring:message code="edit" /></a></dt>
			</dl>
		</div>
		<div id="subnav2">
			<dl>		
					<dt><a href="<spring:url value='deletetpquoterequest.htm?id=${tpquoterequest.id}'/>" title="<spring:message code='tpcosts.deletethirdpartyquoterequest' />"><spring:message code="delete" /></a></dt>
					<dt><a href="<spring:url value='tpquotefromrequest.htm?tpqrid=${tpquoterequest.id}'/>" title="<spring:message code='tpcosts.recordreceiptofthirdpartyquoterequest'/>"><spring:message code="tpcosts.recordreceipt"/></a></dt>
					<dt><a href="#" onclick="event.preventDefault(); genDocPopup(${tpquoterequest.id}, 'tpqrbirtdocs.htm?id=', '<spring:message code="tpcosts.generatethirdpartyquoterequestdocument"/>'); " title="<spring:message code='tpcosts.generatethirdpartyquoterequestdocument'/>"><spring:message code="generate"/></a></dt>
					<dt>
						<a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'instructions-tab', false); " id="instructions-link" title="<spring:message code="instructions" />">
							<spring:message code="instructions" />(<span class="instructionSize">0</span>)
						</a>
					</dt>
			</dl>
		</div>
		<!-- end subnavigation div -->
		
		<!-- begin infobox div with nifty corners applied -->
		<div class="infobox">
			
			<!-- this div displays all items on the third party quotation request -->
			<div id="tpquoteitems-tab">
																																										
				<c:set var="itemCount" value="0" />
				<c:set var="rowcount" value="0" />
				<c:set var="notecount" value="0" />
				<c:set var="mnotecount" value="0" />
				
				<!-- table in which all items on the quote are displayed -->
				<table class="default4 viewtpquoterequestitems" summary="This table contains all the third party quote items">
								
					<thead>
						<tr>
							<td colspan="9">
								<c:choose>
									<c:when test="${tpquoterequest.items.size() == 1}">
										<spring:message code="tpcosts.thereis" /><c:out value=" ${tpquoterequest.items.size()} "/><spring:message code="tpcosts.itemonthisthirdpartyquotationrequest" />
									</c:when>
									<c:otherwise>
										<spring:message code="tpcosts.thereare" /><c:out value="  ${tpquoterequest.items.size()} "/><spring:message code="tpcosts.itemsonthisthirdpartyquotationrequest" />
									</c:otherwise>
								</c:choose>
							</td>
						</tr>
						<tr>
							<th class="unit" scope="col" colspan="2"><spring:message code="tpcosts.itemunit" /></th>
							<th scope="col">														
								<a href="#" onclick=" toggleNoteDisplay(this, 'TPQUOTEREQUESTITEMNOTE', 'vis', null); return false;" class="toggleNoteDisplay">
									<img src="img/icons/note_expand.png" width="20" height="16" alt="<spring:message code='tpcosts.expandallnotes' />" title="<spring:message code='tpcosts.expandallnotes'/>" />
								</a>													
							</th>
							<th class="ref" scope="col"><spring:message code="tpcosts.referenceno" /></th> 
							<th class="costtype" scope="col"><spring:message code="costtype" /></th> 
							<th class="caltype" scope="col"><spring:message code="servicetype" /></th>
							<th class="from" scope="col"><spring:message code="from" /></th>
							<th class="qty" scope="col"><spring:message code="qty" /></th>
							<th class="del" scope="col"><spring:message code="delete" /></th>
						</tr>
					</thead>
								
					<tfoot>
						<tr>
							<td colspan="9">&nbsp;</td>
						</tr>
					</tfoot>
					
					<tbody id="tpquotereqrows">
					
					<c:if test="${tpquoterequest.items.size() == 0}">
						<tr>
							<td class="text-center bold" colspan="9"><spring:message code="tpcosts.therearecurrentlynoitemsonthisthirdpartyquotationrequest" /></td>
						</tr>
					</c:if>
								
					<c:forEach var="item" items="${tpquoterequest.items}">
						
						<c:if test="${not item.partOfBaseUnit}">
							
							<c:set var="itemCount" value="${itemCount + 1}" />
							<c:set var="rowcount" value="${rowcount + 1}" />
							<c:set var="notecount" value="${item.publicActiveNoteCount + item.privateActiveNoteCount}" />
							
							<tr id="item${item.id}">
								<td class="itemno"><c:out value="${itemCount}"/>.</td>
								<td>
									<cwms:showmodel instrumentmodel="${item.model}"/>																						
								</td>
								<td class="notes">
									<links:showNotesLinks noteTypeId="${item.id}" noteType="TPQUOTEREQUESTITEMNOTE" colspan="9" notecounter="${notecount}"/>
								</td>
								<td>
									<c:if test="${!empty item.referenceNo}">
										<c:out value="${item.referenceNo}"/>
									</c:if>
								</td>
								<td class="costtype"><costs:showCostTypes itemid="${item.id}" costTypes="${item.costTypes}"/></td>
								<td class="caltype">
									<c:choose>
										<c:when test="${!empty item.caltype}">
										<t:showTranslationOrDefault translations="${item.caltype.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/>
										</c:when>
										<c:otherwise>
											N/A
										</c:otherwise>
									</c:choose>
								</td>
								<td class="from">
									<c:choose>
										<c:when test="${!empty item.requestFromQuoteItem}">
											<spring:message code="quotation" /> 
											<a href="<spring:url value='viewquotation.htm?id=${tpquoterequest.requestFromQuote.id}'/>" class="mainlink">${tpquoterequest.requestFromQuote.qno}<spring:message code="tpcosts.version" /><c:out value="${tpquoterequest.requestFromQuote.ver}"/></a>
											<spring:message code="item" />
											<a href="<spring:url value='editquotationitem.htm?id=${item.requestFromQuoteItem.id}'/>" class="mainlink"><c:out value="${item.requestFromQuoteItem.itemno}"/></a>
										</c:when>
										<c:when test="${!empty item.requestFromJobItem}">
											<spring:message code="job" />&nbsp;<links:jobnoLinkDWRInfo rowcount="0" jobno="${tpquoterequest.requestFromJob.jobno}" jobid="${tpquoterequest.requestFromJob.jobid}" copy="true" />
											<spring:message code="item" />
											<a href="<spring:url value='jithirdparty.htm?jobitemid=${item.requestFromJobItem.jobItemId}'/>" class="mainlink"><c:out value="${item.requestFromJobItem.itemNo}"/></a>
										</c:when>
									</c:choose>
								</td>								
								<td class="qty"><c:out value="${item.qty}"/></td>
									<c:set var="delText" value="Delete Item" />
								<c:if test="${item.modules.size() > 0}">
									<c:set var="delText" value="Delete Item And Modules" />
								</c:if>
								<td class="del">
									<a href="#" onclick="deleteTPQRItem(${item.id}); return false;">
										<img src="img/icons/delete.png" height="16" width="16" alt="${delText}" title="${delText}" class="delimg" />
									</a>
								</td>
							</tr>
							
							<c:if test="${item.modules.size() > 0}">
							
								<c:forEach var="module" items="${item.modules}">
								
									<c:set var="mnotecount" value="${module.publicActiveNoteCount + module.privateActiveNoteCount}"/>
									
									<tr id="item${module.id}">
										<td class="itemno"><img src="img/icons/arrow_merge.png" height="16" width="16" alt="<spring:message code='tpcosts.partofbaseunit' /> - <cwms:showmodel instrumentmodel="${quoteitem.model}"/>" title="<spring:message code='tpcosts.partofbaseunit'/> - <cwms:showmodel instrumentmodel="${quoteitem.model}"/>" /></td>
										<td>
											<cwms:showmodel instrumentmodel="${module.model}"/>																																							
										</td>
										<td colspan="2" class="notes"><links:showNotesLinks noteTypeId="${module.id}" noteType="TPQUOTEREQUESTITEMNOTE" colspan="7" notecounter="${mnotecount}"/></td>
																		
										<td class="costtype"><costs:showCostTypes itemid="${module.id}" costTypes="${module.costTypes}"/></td>
															
										<td class="caltype">
											<c:choose>
												<c:when test="${not empty item.caltype}">
												<t:showTranslationOrDefault translations="${item.caltype.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/>
												</c:when>
												<c:otherwise>
													N/A
												</c:otherwise>
										</c:choose>
										<td class="from">&nbsp;</td>															
										<td class="qty"><c:out value="${module.qty}"/></td>
										<td class="del">
											<a href="#" onclick=" deleteTPQRItem(${module.id}); return false; ">
												<img src="img/icons/delete.png" height="16" width="16" alt="<spring:message code='tpcosts.deletemodule'/>" title="<spring:message code='tpcosts.deletemodule'/>" />
											</a>
										</td>
									</tr>
									
									<c:if test="${module.publicActiveNoteCount + module.privateActiveNoteCount > 0}">
										
										<tr id="TPQUOTEREQUESTITEMNOTE${module.id}" class="hid">
											<td></td>
											<td colspan="9" class="nopadding">										
											<t:showActiveNotes contact="${currentContact}" links="true" entity="${module}" privateOnlyNotes="false" noteTypeId="${module.id}" noteType="TPQUOTEREQUESTITEMNOTE"/>
											</td>
										</tr>
									</c:if>
										
								</c:forEach>
								
							</c:if>																
																	
							<c:if test="${item.publicActiveNoteCount + item.privateActiveNoteCount > 0}">
								
								<tr id="TPQUOTEREQUESTITEMNOTE${item.id}" class="hid">
									<td colspan="9" class="nopadding">
										<t:showActiveNotes entity="${item}" links="true" noteType="TPQUOTEREQUESTITEMNOTE" noteTypeId="${item.id}" privateOnlyNotes="false" contact="${currentContact}"/>
									</td>
								</tr>
							
							</c:if>
																											
						</c:if>
					
					</c:forEach>
						
					</tbody>
						
				</table>
														
			</div>
			<!-- end of quotation items div -->
		
			<!-- this div contains options available when adding items to the quotation -->
			<div id="newitemoptions" class="hid">
								
				<fieldset>
					
					<legend><spring:message code="tpcosts.newitemoptions"/></legend>
					
					<ol>
											
						<c:choose>	 
							<c:when test="${!empty tpquoterequest.requestFromQuote}">
							<li>
								<a href="<spring:url value='tpquotereqadditemexternal.htm?id=${tpquoterequest.id}'/>" class="mainlink"><spring:message code="tpcosts.additemsfromquotation"/><c:out value="${tpquoterequest.requestFromQuote.qno}"/> <spring:message code="tpcosts.version"/> <c:out value="${tpquoterequest.requestFromQuote.ver}"/></a>
							</li>
							</c:when>
							<c:when test="${!empty tpquoterequest.requestFromJob}">
							<li>	
								<a href="<spring:url value='tpquotereqadditemexternal.htm?id=${tpquoterequest.id}'/>" class="mainlink"><spring:message code="tpcosts.additemsfromjob"/></a>
							</li>
							</c:when>
						</c:choose>
						<li>
							<a href="<spring:url value='tpquotereqadditemmodel.htm?id=${tpquoterequest.id}'/>" class="mainlink"><spring:message code="tpcosts.searchformodelstoaddtothisrequest"/></a>
						</li>
						
					</ol>
					
				</fieldset>
										
			</div>
			<!-- end options div -->
		
		
			<!-- this section view these documents -->
			<div id="quotedocs-tab" class="hid">
				<files:showFilesForSC entity="${tpquoterequest}" sc="${sc}" id="${tpquoterequest.id}" identifier="${tpquoterequest.requestNo}" ver="" rootFiles="${scRootFiles}" allowEmail="true" isEmailPlugin="false" rootTitle="<spring:message code='tpcosts.filesforthirdpartyquotationrequest'/> ${tpquoterequest.requestNo}" deleteFiles="true" />
			</div>
			<!-- end of quote documents section -->
		
		
			<!-- this section contains quote emails -->
			<div id="quoteemails-tab" class="hid">
				<t:showEmails entity="${tpquoterequest}" entityId="${tpquoterequest.id}" entityName="<spring:message code='tpcosts.thirdpartyquotationrequest' />" sc="${sc}" />
			</div>
			<!-- end of quote emails section -->

			<!-- this div contains all items on the quotation that can be edited -->
			<div id="editquote-tab" class="hid">
				<form:form modelAttribute="form">
					<fieldset>
						<legend><spring:message code="edit" /></legend>
						<div class="displaycolumn">
							<ol>
								<li>
									<label><spring:message code="company" />:</label>
									<span><links:companyLinkDWRInfo copy="false" company="${tpquoterequest.contact.sub.comp}" rowcount="10000" /></span>
								</li>
								<li>
									<label><spring:message code="subdivision" />:</label>
									<span><c:out value="${tpquoterequest.contact.sub.subname}"/></span>
								</li>
								<li>
									<label><spring:message code="contact" />:</label>
									<span>
										<links:contactLinkDWRInfo contact="${tpquoterequest.contact}" rowcount="10000" />
										(<a href="#" class="mainlink limitAccess" onclick=" createCascadingSearchPlugin('${tpquoterequest.contact.sub.comp.coid}', '${tpquoterequest.contact.sub.subdivid}', '${tpquoterequest.contact.personid}'); return false; ">Change</a>)
									</span>
								</li>
								<li id="contactSelector" class="hid">
									<form:label path="contactId"><spring:message code="tpcosts.contactselector" />:</form:label>
									<!-- these hidden input fields hold the values of the current company, subdivision, contact, address
										 ids when the user is editing the instrument but has not edited these specific fields.  When the
										 user decides to edit any of the fields above these hidden inputs are removed and new ones are added
										 when the cascading search is created -->
									<form:input path="contactId" type="hidden" id="tempId"/>
									&nbsp;<span class="attention"><form:errors path="contactId"/></span>
								</li>
								<li>
									<form:label path="statusId"><spring:message code="status" />:</form:label>
									<form:select path="statusId">
										<c:forEach var="status" items="${statusList}">
											<form:option value="${status.statusid}"><t:showTranslationOrDefault translations="${status.nametranslations}" defaultLocale="${defaultlocale}" /></form:option>
										</c:forEach>
									</form:select>
									<span class="error"><form:errors path="statusId"/></span>
								</li>													
								<li>
									<form:label path="dueDate"><spring:message code="tpcosts.requestduedate" />:</form:label>
									<form:input path="dueDate" type="date" value="${form.dueDate}"/>
									<span class="error"><form:errors path="dueDate"/></span>
								</li>
								<li>
									<label for="submit">&nbsp;</label>
									<input type="submit" value="<spring:message code='save' />" name="submit" id="submit" />
								</li>
							</ol>
						</div>
						<div class="displaycolumn">
							<ol>
								<li>
									<label><spring:message code="tpcosts.createtpqrequest" />:</label>
									<span>
										<a href="<spring:url value='tpquoterequestform.htm?basedontpqrid='/>${tpquoterequest.id}" class="mainlink"><spring:message code="tpcosts.createtpquoterequestfromthisrequest" /></a>
									</span>
								</li>
							</ol>
						</div>
						<!-- clear floats and restore page flow -->
						<div class="clear"></div>
					</fieldset>
				</form:form>
			</div>
			<!-- end edit quotation info div -->
			
			<!-- begin the tp quotation request instructions -->
			<div id="instructions-tab" class="hid">
				<cwms-instructions link-coid="${instructionLinkCoids}" link-subdivid="${instructionLinkSubdivids}" 
				link-contactid="${instructionLinkContactids}" link-contractid="${instructionLinkContractid}" 
				link-jobid="${ not empty tpquoterequest.requestFromJob ? tpquoterequest.requestFromJob.jobid:'' }"
				instruction-types="CALIBRATION,REPAIRANDADJUSTMENT,PURCHASE" show-mode="RELATED"></cwms-instructions>
			</div>
			<!-- end the tp quotation request instructions -->
			
		</div>
		<!-- end of infobox div with nifty corners applied -->
																
		<!-- this section contains all third party quotation request notes -->
		<t:showTabbedNotes entity="${tpquoterequest}" noteTypeId="${tpquoterequest.id}" noteType="TPQUOTEREQUESTNOTE" privateOnlyNotes="${privateOnlyNotes}"/>
		<!-- end of third party quotation request notes section -->
	</jsp:body>
</t:crocodileTemplate>