<%-- File name: /trescal/core/tpcosts/tpquoterequest/tpquoterequestsearch.jsp--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="tpcosts.thirdpartyquotationrequests" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/tpcosts/tpquoterequest/TPQuoteRequestSearch.js'></script>
    </jsp:attribute>
    <jsp:body>
    		<form:errors path="tpQuoteRequestSearchForm.*">
        		<div class="warningBox1">
                	 <div class="warningBox2">
                    	   <div class="warningBox3">
                            	<c:forEach var="e" items="${messages}">
                               		<div class="center attention"><c:out value="${e}"/></div>
                            	</c:forEach>
                     	 </div>
                 	</div>
           		</div>
       	    </form:errors>
				<!-- sub navigation which changes between search quotations and create quotations -->
				<div id="subnav" class="tpquotereqnav">
					<dl>
						<dt><a href="<spring:url value='tpquoterequestsearch.htm'/>" title="<spring:message code='tpcosts.searchthirdpartyquotationrequests' />" class="selected"><spring:message code="tpcosts.searchthirdpartyquotationrequests"/></a></dt>
						<dt><a href="<spring:url value='tpquoterequestform.htm'/>" title="<spring:message code='tpcosts.createthirdpartyquotationrequest' />"><spring:message code="tpcosts.recordthirdpartyquotationrequest"/></a></dt>								
					</dl>
				</div>
						
				<div id="tpquotesearch" class="infobox">
				
					<form:form action="" method="post" id="tpquotesearchform" modelAttribute="tpQuoteRequestSearchForm">
				
						<fieldset>
						
						<legend><spring:message code="tpcosts.searchthirdpartyquotationrequests" /></legend>
						
							<ol>
								
								<!-- this list item contains the company search plugin -->
								<li>
									<label><spring:message code="tpcosts.companysearch" />:</label>
									<div class="float-left extendPluginInput">
										<div class="compSearchJQPlugin">
											<input type="hidden" name="field" value="coid" />
											<input type="hidden" name="compCoroles" value="supplier,business" />
											<input type="hidden" name="tabIndex" value="1" />
											<!-- company results listed here -->
										</div>
									</div>										
									<!-- div clears the page flow -->
									<div class="clear"></div>																			
								</li>										
								<!-- this list item contains the contact search plugin -->
								<li>
									<label><spring:message code="tpcosts.contactsearch" />:</label>											
									<!-- this div creates a new contact search plugin and the hidden input passes
										 a comma separated list of coroles to search over, no input field causes
										 searches over client corole as default -->		
									<div id="contSearchPlugin">
										<input type="hidden" id="contCoroles" value="supplier" />
										<input type="hidden" id="contindex" value="2" />
									</div>											
									<!--  clear floats and restore page flow  -->
									<div class="clear"></div>											
								</li>																																									
								<li>
									<label for="qno"><spring:message code="tpcosts.requestnumber" />:</label>
									<form:input path="requestno" type="text" id="qno" tabindex="4" />
								</li>										
								<li>
									<label for="statusid"><spring:message code="status" /></label>
									<form:select path="statusid" id="statusid" tabindex="5">
										<option selected="selected" value=""><spring:message code="tpcosts.pleaseselect" />...</option>
										<c:forEach var="status" items="${statusList}" >
											<option value="${status.statusid}"><t:showTranslationOrDefault translations="${status.nametranslations}" defaultLocale="${defaultlocale}"/></option>
										</c:forEach>
									</form:select>
								</li>										
								<li>
									<label for="submit">&nbsp;</label>
									<input type="submit" value="<spring:message code='submit'/>" name="submit" id="submit" tabindex="6" />
								</li>								
							
							</ol>
							
						</fieldset>
					
					</form:form>
					
				</div>
    </jsp:body>
</t:crocodileTemplate>