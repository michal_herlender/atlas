<%-- File name: /trescal/core/tpcosts/tpquote/editbulktpquotationitem.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="tpcosts.editthirdpartyquotationitem"/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/tpcosts/tpquote/EditBulkTPQuotationItem.js'></script>
    </jsp:attribute>
    <jsp:body>
			<!-- error section displayed when form submission fails -->
		<form:errors path="command.*">
       		 <div class="warningBox1">
                <div class="warningBox2">
                     <div class="warningBox3">
                    	<h5 class="center-0 attention"><spring:message code="tpcosts.errorsavethirdpartyquoteitem"/> </h5>
                      <c:forEach var="e" items="${messages}">
                        <div class="center attention"><c:out value="${e}"/></div>
                      </c:forEach>
                     </div>
                </div>
        	</div>
 		</form:errors>
				<!-- end error section -->
						
				<!-- edit bulk third party quote items before adding to quote form -->
				<form:form action="" method="post" id="editbulktpquoteitems" modelAttribute="command">
					
					<!-- div containing bulk third party quote items -->
					<div class="infobox">
						
						<h4 class="center">		
							<spring:message code="tpcosts.bulkediting"/>&nbsp;<c:out value="${command.tpQitems.size()}" />&nbsp;<spring:message code="tpcosts.itemsonthirdpartyquotation"/> 
							<a href="<spring:url value='viewtpquote.htm?id=${command.tpQuote.id}'/>" class="mainlink">&nbsp;<c:out value="${command.tpQuote.qno}" /></a>		
						</h4>
						
						<c:forEach var="tpQitem" items="${command.tpQitems}" varStatus="statusQitem">
																				
							<fieldset id="costs${tpQitem.id}">
								
								<legend><spring:message code="edit"/>  <cwms:showmodel instrumentmodel="${tpQitem.model}" /></legend>
								
								<ol>
									<li>
										<label><spring:message code="tpcosts.modelname"/>:</label>
										<span><cwms:showmodel instrumentmodel="${tpQitem.model}" /></span>
									</li>
									<li> 
										<form:label path="tpQitems[${statusQitem.index}].quantity"><spring:message code="tpcosts.quantity"/>:</form:label>
										<form:input type="text" path="tpQitems[${statusQitem.index}].quantity" value=" " id="tpQitems[${statusQitem.index}].quantity" />
<%-- 										<form:errors path="" /> --%>
									</li>
									<li>
										<form:label path="calTypeIds"><spring:message code="calibrationtype"/>:</form:label>
										<form:select path="calTypeIds" id="calTypeIds" multiple="false" >
											<option value="0" <c:if test="${not empty tpQitem.caltype}"> selected="selected" </c:if>>N/A</option>
											<c:forEach var="caltype" items="${caltypes}">
												<option value="${caltype.calTypeId}" <c:if test="${tpQitem.caltype.calTypeId == caltype.calTypeId}"> selected="selected"</c:if>> 
													<t:showTranslationOrDefault translations="${caltype.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/>
												</option>
											</c:forEach>
										</form:select>
									</li>
									<li>
										<label>&nbsp;</label> 
											<div class="cost"><spring:message code="cost"/> (${command.tpQuote.currency.currencyERSymbol})</div>
											<div class="disc"><spring:message code="discount"/> (&#37;)</div>
										<div class="clear"></div>
									</li>
									<c:set var="costCount" value="0" />
									<c:forEach var="cost" items="${tpQitem.costs}">
											<c:set var="display" value="vis" />
										<c:choose>
											<c:when test="${cost.active == false}">
												<c:set var="display" value="hid" />
											</c:when>
											<c:otherwise>
												<c:set var="costCount" value="${costCount + 1}" />
											</c:otherwise>	
										</c:choose>
										<li id="cost${tpQitem.id}${cost.costType.typeid}" class="${display}">
											<label><spring:message code="${cost.costType.messageCode}"/> <spring:message code="costs"/>:</label>
																					
											<c:set var="name" value="${cost.costType.name.toLowerCase()}"/>
											<c:set var="fieldName" value="${name}Cost"/>
											
											${command.tpQuote.currency.currencyERSymbol}
											<form:input type="text" path="tpQitems[${statusQitem.index}].${fieldName}.totalCost" value=" " id="tpQitems[${statusQitem.index}].${fieldName}.totalCost" />
											<t:showErrors path="command" showFieldErrors="true" />
											
											<form:input type="text" path="tpQitems[${statusQitem.index}].${fieldName}.discountRate" value=" " id="tpQitems[${statusQitem.index}].${fieldName}.discountRate" /> %
											<t:showErrors path="command" showFieldErrors="true" />
											
											<form:hidden path="tpQitems[${statusQitem.index}].${fieldName}.active" id="tpQitems[${statusQitem.index}].${fieldName}.active" value=" "/>
											
											<a href="#" onclick=" addRemoveCostType(${cost.costType.typeid}, '${name}Cost', ${tpQitem.id}, ${statusQitem.index}); return false; ">
												<img src="img/icons/delete.png" width="16" height="16" class="padleft40" alt="<spring:message code='tpcosts.removecosttype'/>" title="<spring:message code='tpcosts.removecosttype'/>" />
											</a>
										</li>
									</c:forEach>
									
									<li <c:choose>
											<c:when test="${costCount > 1}">  class="hid" </c:when>
											<c:otherwise> class="vis" 
											</c:otherwise>
										</c:choose> id="noCosts${tpQitem.id}">
										<label>&nbsp;</label>
										<span> <spring:message code="tpcosts.nocoststypeshavebeenaddedtotheitem"/></span> </li>
									<li id="availableCosts${tpQitem.id}" <c:choose><c:when test="${not empty costtypes}"> class="vis" </c:when><c:otherwise> class="hid" </c:otherwise></c:choose>>
									
										<label><spring:message code="tpcosts.addcosttype"/>:</label>
										<select name="remCosts" id="remCosts${tpQitem.id}">													
											<c:forEach var="rem" items="${costtypes}">
												<option value="${rem.typeid}">${rem.name.toLowerCase()}</option>
											</c:forEach>													
										</select>
										<a href="#" onclick=" addRemoveCostType($j(this).siblings('select').val(), $j(this).siblings('select').find('option:selected').text() + 'Cost', ${tpQitem.id}, ${statusQitem.index}); return false; ">
											<img src="img/icons/add.png" width="16" height="16" class="img_marg_bot" alt="<spring:message code='tpcosts.addcosttype'/>" title="<spring:message code='tpcosts.addcosttype'/>" />
										</a>
									</li>
									
									<li>
										<form:label path="tpQitems[${statusQitem.index}].carriageIn"><spring:message code="tpcosts.carriagein"/>:</form:label>
										${command.tpQuote.currency.currencyERSymbol}&nbsp;
										<form:input type="text" path="tpQitems[${statusQitem.index}].carriageIn" value=" " id="tpQitems[${statusQitem.index}].carriageIn" />
									</li>
									<li>
										<form:label path="tpQitems[${statusQitem.index}].carriageOut"><spring:message code="tpcosts.carriageout"/>:</form:label>
										${command.tpQuote.currency.currencyERSymbol}&nbsp;
										<form:input type="text" path="tpQitems[${statusQitem.index}].carriageOut" value=" " id="tpQitems[${statusQitem.index}].carriageOut" />
									</li>
									
									<li>
										<label><spring:message code="tpcosts.beforediscount"/>:</label>
										<span>${command.tpQuote.currency.currencyERSymbol}&nbsp;${tpQitem.totalCost}</span>
									</li>
									<li>
										<form:label path="tpQitems[${statusQitem.index}].generalDiscountRate"><spring:message code="tpcosts.itemdiscount"/>:</form:label>
										<form:input type="text" path="tpQitems[${statusQitem.index}].generalDiscountRate" value=" " id="tpQitems[${statusQitem.index}].generalDiscountRate" />%
										<form:errors path="" />
									</li>
									<li>
										<label><spring:message code="tpcosts.discountvalue"/>:</label>
										<span>${command.tpQuote.currency.currencyERSymbol}&nbsp;${tpQitem.generalDiscountValue}</span>
									</li>
									<li>
										<label><spring:message code="totalcost"/>:</label>
										<span>${command.tpQuote.currency.currencyERSymbol}&nbsp;${tpQitem.finalCost}</span>
									</li>
								</ol>										
																										
							</fieldset>
							
						</c:forEach>
						
					</div>
					<!-- end of div containing bulk third party quote items -->
					
					<!-- div holding submit button -->
					<div class="center">
						<input type="submit" value="<spring:message code='save'/>" name="submit" />
					</div>
					<!-- end of submit button div -->
				
				</form:form>
				<!-- end of bulk third party quote items form -->	
    </jsp:body>
</t:crocodileTemplate>