<%-- File name: /trescal/core/tpcosts/tpquoterequest/tpquoterequestadditemfromquote.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tpquoterequest" tagdir="/WEB-INF/tags/tpquoterequest" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
		<span class="headtext">
			<spring:message code="tpcosts.additemstothirdpartyquotationrequest" /> 
		</span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/tpcosts/tpquoterequest/TPQuoteRequestAddItemFromQuote.js'></script>
    </jsp:attribute>
    <jsp:body>
		<form:form modelAttribute="tpQuoteRequestForm" action="" method="post">

			<!-- infobox div with nifty corners applied -->
			<div class="infobox width60">							
				
				<fieldset>
					
					<legend><spring:message code="tpcosts.addingfor"/> <a href="/viewtpquoterequest.htm?&id=${req.id}">${req.requestNo}</a></legend>
					
					<ol>
						<li>
							<label><spring:message code="tpcosts.costtypes"/>:</label>
							<div class="float-left">
								<form:checkboxes path="defaultCostTypes" items="${costTypes}" itemLabel="message" itemValue="name" delimiter="<br/>"/>
							</div>
							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
						</li>
					</ol>
					
				</fieldset>
			
			</div>
			<!-- end of infobox div -->
			
			<!-- infobox div with nifty corners applied -->
			<div class="infobox">
				
				<c:choose>
					<c:when test="${not empty req.requestFromQuote}">
						<tpquoterequest:showTPQuoteRequestQuotationForm quotation="${req.requestFromQuote}" models="${models}" />
					</c:when>
					<c:when test="${not empty req.requestFromJob}">
						<c:set var="job" value="${req.requestFromJob}"/>
						<!-- this table displays available items / modeltypes from source quotation -->
						<table id="tpq_addQI" class="default2" summary="<spring:message code='tpcosts.tableitemsfromjob'/>">
							
							<thead>
								<tr>
									<td colspan="6">
										<spring:message code="tpcosts.availableitemsfromsourcejob"/> <a href="viewjob.htm?jobid=${job.jobid}" class="mainlink">${job.jobno}</a>
									</td>
								</tr>
								<tr>
									<th class="itemno" scope="col"><spring:message code="item"/></th>
									<th class="add" scope="col">
										<input type="checkbox" id="selectAll" onclick=" selectAllItems(this.checked, 'tpq_addQI'); "/> <spring:message code="all"/>
									</th>
									<th class="model" scope="col"><spring:message code="model"/></th>
									<th class="caltype" scope="col"><spring:message code="caltype"/></th>
									<th class="plantno" scope="col"><spring:message code="plantno"/></th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<td colspan="6">&nbsp;</td>
								</tr>
							</tfoot>
							
							<tbody>												
								<c:forEach var="item" items="${job.items}">
									<tr>
										<td class="center">${item.itemNo}.</td>
										<td class="center">
												<c:set var="checkedText" value=""/>
												<c:if test="${tpQuoteRequestForm.jobitemids.contains(item.jobItemId)}">
													<c:set var="checkedText" value="checked"/>
												</c:if>
												<form:checkbox path="jobitemids" value="${item.jobItemId}" checked="${checkedText}" />
										</td>
										<td><cwms:showmodel instrumentmodel="${item.inst.model}"/></td>	
										<td class="center"><cwms:besttranslation translations="${item.calType.serviceType.shortnameTranslation}"/></td>
										<td>${item.inst.plantno}</td>
									</tr>
								</c:forEach>
							</tbody>
					
						</table>
					</c:when>
				</c:choose>
			</div>
			<!-- end of infobox div -->
			
			<div class="text-center">
				<input type="submit" name="submit" value="<spring:message code='tpcosts.additems' />">
			</div>
			
		</form:form>						
    </jsp:body>
</t:crocodileTemplate>