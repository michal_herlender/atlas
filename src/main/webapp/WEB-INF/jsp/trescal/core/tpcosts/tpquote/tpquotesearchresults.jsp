<%-- File name: /trescal/core\tpcosts\tpquote/tpquotesearchresults.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="quotation" tagdir="/WEB-INF/tags/quotation"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="tpcosts.thirdpartyquotationresults" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type="text/javascript" src="script/trescal/core/tpcosts/tpquote/TPQuoteSearchResults.js"></script>
    </jsp:attribute>
    <jsp:body>
        <!-- infobox contains all search results and is styled with nifty corners -->
							<div class="infobox">
							
								<form:form action="" name="searchform" id="searchform" method="post" >
								
									<form:hidden path="coid" />
									<form:hidden path="personid" />
									<form:hidden path="coname" />
									<form:hidden path="contact" />
									
									<form:hidden path="tpqno" />
									<form:hidden path="qno" />
									<form:hidden path="clientref" />
									<form:hidden path="statusid" />
									
									<form:hidden path="pageNo" />
									<form:hidden path="resultsPerPage" />
									
									<form:hidden path="descid" />
									<form:hidden path="description" />
									<form:hidden path="mfr" />
									<form:hidden path="mfrid" />
									<form:hidden path="model" />
									<form:hidden path="modelid" />
									
									<t:showResultsPagination rs="${command.rs}" pageNoId="" />
							
									<c:set var="tpquotations" value="${command.rs.results}" />
									<table id="tpquotesearchresults" class="tablesorter" summary="<spring:message code="tpcosts.tablequotsearchresults" />">
										<thead>
											<tr>
												<td colspan="6">
													<spring:message code="tpcosts.thirdpartyquotationresults" /><c:out value="${tpquotations.size()}" />
											</tr>
											<tr>
												<th id="tpqquoteno" scope="col"><spring:message code="quoteno" /></th>  
												<th id="tpqconame" scope="col"><spring:message code="company" /></th>  
												<th id="tpqcontact" scope="col"><spring:message code="contact" /></th> 
												<th id="tpqstatus" scope="col"><spring:message code="status" /></th>
												<th	id="tpqissuedate" scope="col"><spring:message code="issuedate" /></th>
												<th	id="tpqregdate" scope="col"><spring:message code="regdate" /></th>
											</tr>
										</thead>
										<tfoot>
											<tr>
												<td colspan="6">&nbsp;</td>
											</tr>
										</tfoot>
										<tbody id="keynavResults">
											 <c:choose>
											 	<c:when test="${tpquotations.size() < 1 }">
												<tr>
													<td colspan="6" class="bold center">
														<spring:message code="noresults" />
													</td>
												</tr>
											    </c:when>
				  							    <c:otherwise>												
												<c:forEach var="tpquoterequest" items="${tpquotations}">
													<tr onclick="window.location.href='<spring:url value="/viewtpquote.htm?id=${tpquoterequest.id}"/>'" onmouseover=" $j(this).addClass('hoverrow'); " onmouseout=" if (this.className != 'hoverrow') $j(this).removeClass('hoverrow'); ">
														<td><c:out value="${tpquoterequest.qno}" /></td> 
														<td><links:companyLinkDWRInfo company="${tpquoterequest.contact.sub.comp}" rowcount="${rowcount}" copy="false"/></td>
														<td><links:contactLinkDWRInfo contact="${tpquoterequest.contact}" rowcount="${rowcount}"/></td>
														<td><t:showTranslationOrDefault translations="${tpquoterequest.status.nametranslations}" defaultLocale="${defaultlocale}"/></td>
														<td><fmt:formatDate value="${tpquoterequest.issuedate}" dateStyle="medium" type="date"/></td>
														<td><fmt:formatDate value="${tpquoterequest.regdate}" type="date" dateStyle="SHORT" /></td>
													</tr> 
												</c:forEach>
											    </c:otherwise>
											</c:choose>
										</tbody>
									</table>
									
									<t:showResultsPagination rs="${command.rs}" pageNoId="" />
									
								</form:form>
								
							</div>
							<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>