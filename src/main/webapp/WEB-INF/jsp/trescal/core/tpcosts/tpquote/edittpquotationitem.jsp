<%-- File name: /trescal/core/tpcosts/tpquote/edittpquotationitem.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="tpcosts.editthirdpartyquotationitem"/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/tpcosts/tpquote/EditTPQuotationItem.js'></script>
    </jsp:attribute>
    <jsp:body>
        <!-- error section displayed when form submission fails -->
          <form:errors path="command.*">
        	<div class="warningBox1">
                <div class="warningBox2">
                     <div class="warningBox3">
                    	<h5 class="center-0 attention"><spring:message code="tpcosts.errorsavethirdpartyquoteitem"/></h5>
                      <c:forEach var="e" items="${messages}">
                        <div class="center attention"><c:out value="${e}"/></div>
                      </c:forEach>
                     </div>
                </div>
       		</div>
  		</form:errors>
		<!-- end error section -->
						
		<!-- edit third party quote item form -->
		<form:form action="" method="post" id="editTPQuoteItem" >
								
			<!-- div containing general quote item information -->
			<div class="infobox">
																		
				<!-- displays main quote information -->
				<fieldset id="quoteiteminfo">
		
					<legend>
						<spring:message code="tpcosts.editingitemonthirdpartyquotation"/> 
						<a href="<spring:url value='viewtpquote.htm?id=${command.tpQitem.tpquotation.id}'/>" class="mainlink">&nbsp;<c:out value="${command.tpQitem.tpquotation.qno}"/></a>
					</legend>
		
					<ol>
						<li>
							<label><spring:message code="tpcosts.modelname"/>:</label>
							<span><cwms:showmodel instrumentmodel="${command.tpQitem.model}"/></span>
						</li>
						<li>
							<form:label path="tpQitem.quantity"><spring:message code="tpcosts.quantity"/>:</form:label>
							<form:input type="text" path="tpQitem.quantity" value="${command.tpQitem.quantity}" id="tpQitem.quantity" />
						</li>
						<li>
							<form:label path="caltypeid"><spring:message code="servicetype"/>:</form:label>
							<form:select path="caltypeid" id="caltypeid">
								<c:forEach var="caltype" items="${caltypes}">
									<option value="${caltype.calTypeId}" <c:if test="${command.tpQitem.caltype.calTypeId == caltype.calTypeId}"> selected="selected" </c:if>>
										<t:showTranslationOrDefault translations="${caltype.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/>
									</option>
								</c:forEach>
							</form:select>
						</li>
					</ol>
				
				</fieldset>
				
			</div>
				<!-- end of div containing third party quote item info -->
								
				<!-- div containing cost types for quote item -->
			<div class="infobox">
																		
				<!-- displays main quote information -->
				<fieldset id="activecosts">
		
					<legend>
						<spring:message code="tpcosts.activecostsforitem"/>
					</legend>
		
					<ol>
						<li>
							<label>&nbsp;</label>
							<div class="cost"><spring:message code="cost"/></div>
							<div class="disc"><spring:message code="discount"/> (&#37;)</div>												
							<div class="clear"></div>
						</li>
						<c:set var="costCount" value="0"/>
						<c:forEach var="cost" items="${command.tpQitem.costs}">
							<c:set var="display" value="vis" />
							<c:choose>
								<c:when test="${cost.active == false}">
									<c:set var="display" value="hid" />
								</c:when>
								<c:otherwise>
									<c:set var="costCount" value="${costCount + 1}" />
								</c:otherwise>
							</c:choose>
							<li id="cost${cost.costType.typeid}" class="${display}">
								<label>
						    	<spring:message code="${cost.costType.messageCode}"/>&nbsp;<spring:message code="tpcosts.costs"/>:
						    	</label>
																		
								<c:set var="name" value="${cost.costType.name.toLowerCase()}"/>
								<c:set var="fieldName" value="${name}Cost"/>
								
									${command.tpQitem.tpquotation.currency.currencyERSymbol}
									<form:input type="text" path="tpQitem.${fieldName}.totalCost" value="" id="tpQitem.${fieldName}.totalCost" />
									<form:errors path="tpQitem.${fieldName}.totalCost"/>
								
									<form:input type="text" path="tpQitem.${fieldName}.discountRate" value="" id="tpQitem.${fieldName}.discountRate" /> %
									<form:hidden path="tpQitem.${fieldName}.active" />
								
								<a href="" onclick=" event.preventDefault(); addRemoveCostType(${cost.costType.typeid}, '${name}Cost'); ">
									<img src="img/icons/delete.png" width="16" height="16" class="padleft40" alt="<spring:message code="tpcosts.removecosttype"/>" title="<spring:message code="tpcosts.removecosttype"/>" />
								</a>
							</li>
						</c:forEach>
						<li <c:choose>
								<c:when test="${costCount > 1}"> class="hid" </c:when>
								<c:otherwise> class="vis" </c:otherwise>
							</c:choose> id="noCosts">
							<label>&nbsp;</label>
							<span><spring:message code="tpcosts.nocoststypeshavebeenaddedtotheitem"/></span>
						</li>
						<li>
							<label><spring:message code="tpcosts.addcosttype"/>:</label>
							<select name="remCosts" id="remCosts">
								<c:forEach var="rem" items="${remCostTypes}">
									<option value="${rem.typeid}">${rem.name.toLowerCase()}</option>
								</c:forEach>
							</select>
							<a href="" onclick=" event.preventDefault(); addRemoveCostType($j(this).siblings('select').val(), $j(this).siblings('select').find('option:selected').text() + 'Cost'); ">
								<img src="img/icons/add.png" width="16" height="16" class="img_marg_bot" alt="<spring:message code="tpcosts.addcosttype"/>" title="<spring:message code="tpcosts.addcosttype"/>" />
							</a>
						</li>
					</ol>
				
				</fieldset>
				
			</div>
			<!-- end of div containing quote item cost types -->
								
			<!-- div containing total costs for quote item -->
			<div class="infobox">
				
				<fieldset>
		
					<legend>
						<spring:message code="tpcosts.othercosts"/>
					</legend>
		
					<ol>								
						<li>
							<form:label path="tpQitem.inspection"><spring:message code="tpcosts.inspection"/>:</form:label>
								${command.tpQitem.tpquotation.currency.currencyERSymbol}
							<form:input type="text" path="tpQitem.inspection" value="${command.tpQitem.inspection}" id="tpQitem.inspection" />
						</li>
						<li>
							<form:label path="tpQitem.carriageIn"><spring:message code="tpcosts.carriagein"/>:</form:label>
								${command.tpQitem.tpquotation.currency.currencyERSymbol}
							<form:input type="text" path="tpQitem.carriageIn" value="${command.tpQitem.carriageIn}" id="tpQitem.carriageIn" />
						</li>
						<li>
							<form:label path="tpQitem.carriageOut"><spring:message code="tpcosts.carriageout"/>:</form:label>
								${command.tpQitem.tpquotation.currency.currencyERSymbol}
							<form:input type="text" path="tpQitem.carriageOut" value="${command.tpQitem.carriageOut}" id="tpQitem.carriageOut" />
						</li>
					</ol>
				</fieldset>
																		
				<!-- displays main quote information -->
				<fieldset>
		
					<legend>
						<spring:message code="tpcosts.totalcostsforitem"/>
					</legend>
		
					<ol>								
						<li>
							<label><spring:message code="tpcosts.beforediscount"/>:</label>
							<span>${command.tpQitem.tpquotation.currency.currencyERSymbol}${command.tpQitem.totalCost}</span>
						</li>
						<li>
							<form:label path="tpQitem.generalDiscountRate"><spring:message code="tpcosts.globaldiscount"/>:</form:label>
							<form:input type="text" path="tpQitem.generalDiscountRate" value="${command.tpQitem.generalDiscountRate}" id="tpQitem.generalDiscountRate" />%
						</li>
						<li>
							<label><spring:message code="tpcosts.discountvalue"/>:</label>
							<span>${command.tpQitem.tpquotation.currency.currencyERSymbol}${command.tpQitem.generalDiscountValue}</span>
						</li>
						<li>
							<label><spring:message code="totalcost"/>:</label>
							<span>${command.tpQitem.tpquotation.currency.currencyERSymbol}${command.tpQitem.finalCost}</span>
						</li>
						<li>
							<label>&nbsp;</label>
							<input type="submit" value="<spring:message code="save"/>" name="submit" />
						</li>
					</ol>
				
				</fieldset>
				
			</div>
			<!-- end of div containing total costs for quote item -->
												
		</form:form>
		<!-- end of third party quote item form -->
    </jsp:body>
</t:crocodileTemplate>