<%-- File name: /trescal/core/tpcosts/tpquoterequest/tpquoterequestaddmodel.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	    <jsp:attribute name="header">
			<span class="headtext"><spring:message code="tpcosts.additemstothirdpartyquotationrequest" /></span>    
		</jsp:attribute>
	    <jsp:attribute name="scriptPart">
	        <script type='text/javascript' src='script/trescal/core/tpcosts/tpquoterequest/TPQuoteRequestAddModel.js'></script>
	    </jsp:attribute>
	    <jsp:body>
	        <form:form id="testid" method="post" action="" modelAttribute="tpQuoteRequestItemForm">
		
		<form:errors path="tpQuoteRequestItemForm.*">
       		 <div class="warningBox1">
                <div class="warningBox2">
                     <div class="warningBox3">
                    	<h5 class="center-0 attention"><spring:message code="tpcosts.thereareerrors"/></h5>
                      <c:forEach var="e" items="${messages}">
                        <div class="center attention"><c:out value="${e}"/></div>
                      </c:forEach>
                     </div>
                </div>
        	</div>
 		</form:errors>
															
	<!-- div to contain all search choices -->									
	<div class="infobox">
	
		<form:hidden path="submitted" />
	
		<h5><spring:message code="tpcosts.additemstothirdpartyquotationrequest"/> <a class="mainlink" href="<spring:url value='viewtpquoterequest.htm?id=${tpQuoteRequestItemForm.tpQuoteRequest.id}'/>">&nbsp;<c:out value="${tpQuoteRequestItemForm.tpQuoteRequest.requestNo}"/></a></h5>

		<!-- displays form for searching equipment to add to quotation --> 
		<fieldset>

			<legend><spring:message code="tpcosts.equipmentsearch"/></legend>
			
			<ol>
				<li>
					<label><spring:message code="domain" />:</label>
					<!-- float div left -->
					<div class="float-left">
						<form:input path="searchForm.domainNm" id="domainNm" value="${tpQuoteRequestItemForm.searchForm.domainNm}" tabindex="1"/>
						<input type="hidden" path="searchForm.domainId" id="domainId" value="${tpQuoteRequestItemForm.searchForm.domainId}"/>
					</div>
					<div class="clear-0"></div>	
				</li>		
				<li>
					<label><spring:message code="family" />:</label>
					<!-- float div left -->
					<div class="float-left">
						<form:input path="searchForm.familyNm" id="familyNm" value="${tpQuoteRequestItemForm.searchForm.familyNm}" tabindex="2"/>
						<input type="hidden" path="searchForm.familyId" id="familyId" value="${tpQuoteRequestItemForm.searchForm.familyId}"/>
					</div>
					<div class="clear-0"></div>	
				</li>	
				<li>
					<label for="description"><spring:message code="description.description" />:</label>
					<!-- float div left -->
					<div class="float-left">
						<!-- inst description results appear here -->
						<form:input path="searchForm.descNm" id="descNm" value="${tpQuoteRequestItemForm.searchForm.descNm}" tabindex="3" />
						<spring:bind path="tpQuoteRequestItemForm.searchForm.descId">
						<input type="hidden" path="searchForm.descId" id="descId" value="${tpQuoteRequestItemForm.searchForm.descId}" />
						</spring:bind>
					</div>																							
					<!-- clear floats and restore pageflow -->
					<div class="clear-0"></div>
				</li>
				
				<li>
					<label for="mfr"><spring:message code="manufacturer" />:</label>
					<!-- float div left -->
					<div class="float-left">
						<!-- this div creates a new manufacturer jquery search plugin -->	
						<div class="mfrSearchJQPlugin">
							<input type="hidden" name="fieldId" value="searchForm.mfrId" />
							<input type="hidden" name="fieldName" value="searchForm.mfrNm" />
							<input type="hidden" name="textOnlyOption" value="true" />
							<input type="hidden" name="tabIndex" value="1" />
							<!-- manufacturer results appear here -->
						</div>
					</div>																							
					<!-- clear floats and restore pageflow -->
					<div class="clear-0"></div>
				</li>
				<li>
					<label for="instmodel"><spring:message code="model" />:</label>
					<form:input path="searchForm.model" tabindex="" />
				</li>
				
				<li>
					<label><spring:message code="salescategory" />:</label>
					<!-- float div left -->
                    <div class="float-left">
						<form:input path="searchForm.salesCat" id="salesCat" value="${tpQuoteRequestItemForm.searchForm.salesCat}" tabindex="6"/>
						<input type="hidden" path="searchForm.salesCatId" id="salesCatId" value="${tpQuoteRequestItemForm.searchForm.salesCatId}"/>
					</div>                           
					<div class="clear-0"></div>	
				</li>   
				
				<li>
						<label><spring:message code="instmod.modeltype" /></label>
						<form:select path="modelTypeSelectorChoice">
						<c:forEach var="option" items="${modelTypeSelections}">
							<option value="${option.key}"><c:out value="${option.value}"/></option>
						</c:forEach>
						</form:select>
				</li>
				<li>
						<label><spring:message code="instmod.exclude_quarantined" /></label>
						<c:set var="checked" value=""/>
						<c:if test="${status.value == true}"> 
							<c:set var="checked" value="checked"/>
						</c:if>
						<spring:bind path="tpQuoteRequestItemForm.searchForm.excludeQuarantined">
						<form:checkbox path="searchForm.excludeQuarantined" value="true"  checked="${checked}"/>
						<input type="hidden" path="_searchForm.excludeQuarantined" value="false" />
						</spring:bind>
				</li>
				
				<li>
					<label><spring:message code="tpcosts.costtypes" />:</label>
					<c:forEach var="costtype" items="${costTypes}">
						<c:set var="checked" value=""/>
						 <c:if test="${costtype.typeid == '1'}"> 
							<c:set var="checked" value="checked" />
						</c:if>
						<form:checkbox path="defaultCostTypes" value="${costtype.name}" checked="${checked}" /> ${costtype.getMessage()}
					</c:forEach>
				</li>
				<li>
					<label for="submit">&nbsp;</label>
					<input type="submit" id="submit" name="search" value="<spring:message code="search" />" />
				</li>
				
			</ol>
		
		</fieldset>
		
	</div>
	<!-- end of search div -->
	
	<!-- div to contain submit button which adds items to the third party quote request -->
	<div class="padtop">
		<input type="submit" name="additems" value="<spring:message code="tpcosts.additems" />" onclick="$j('#submitted').attr('value', 'add');" />
	</div>
	<!-- end of submit div -->
	
	<!-- div to display instrument search results -->								
	<div class="results" id="tpquoteitemresults">
	
		<h5>
			<c:choose>
				<c:when test="${not empty tpQuoteRequestItemForm.searchModels}">
					<c:choose>
						<c:when test="${tpQuoteRequestItemForm.searchModels.size() > 1}">
							<c:out value="${tpQuoteRequestItemForm.searchModels.size()}"/><spring:message code="tpcosts.modelsfound" />
						</c:when>
						<c:otherwise>
							<c:out value="${tpQuoteRequestItemForm.searchModels.size()}"/><spring:message code="tpcosts.modelfound" />
						</c:otherwise>
					</c:choose>
				</c:when>
				<c:otherwise>
					<spring:message code="noresults" />
				</c:otherwise>
			</c:choose>
		</h5>
							
		<!-- this list displays each instrument returned in the search -->
		<ul>
		
			<li style=" display: none; "></li>
						
			<c:forEach var="key" items="${tpQuoteRequestItemForm.searchModels.keySet()}">
				<c:set var="model" value="${tpQuoteRequestItemForm.searchModels.get(key)}"/>
				<li>
					<div class="headingtext">
						<span>
							<form:checkbox path="selectedIds" value="${key}"/>
						</span>
						<cwms:showmodel instrumentmodel="${model}"/> -- <t:showTranslationOrDefault translations="${model.modelType.modelTypeNameTranslation}" defaultLocale="${defaultlocale}"/>
						
<%-- 						<input type="hidden" path="modelids" value="${model.modelid}"/> --%>

						<form:hidden path="modelids" value="${model.modelid}"/>
												
					</div>
					
					<div class="headingoptions">
						<div>
							<form:select path="calTypeIds" multiple="false">
								<option value="0" selected>N/A</option>
								<c:forEach var="cal" items="${tpQuoteRequestItemForm.calTypes}">
									<option value="${cal.calTypeId}"><t:showTranslationOrDefault translations="${cal.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/></option>
								</c:forEach>
							</form:select>
							<form:input path="modelQtys" value="1" class="tpitemqty" name="${tpQuoteRequestItemForm.modelQtys}" />
						</div>
					</div>
																
					<!--  clear floats and restore page flow  -->
					<div class="clear"></div>
					
				</li>
			</c:forEach>
			
		</ul>
		<!-- end of list to display instruments -->
		
		<!-- this blank div clears floats, restores page flow and applies border to last list item -->
		<div class="clear_border">&nbsp;</div>
		
	</div>
	<!-- end of div to display instrument search results -->
	
	<!-- div to contain submit button which adds items to the third party quote request -->
	<div class="padtop">
		<input type="submit" name="additems2" value="<spring:message code="tpcosts.additems" />" onclick="$j('#submitted').attr('value', 'add');" />
	</div>	
	<!-- end of submit div -->
											
</form:form>
    </jsp:body>
</t:crocodileTemplate>