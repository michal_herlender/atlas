<%-- File name: /trescal/core/tpcosts/tpquoterequest/tpquoterequestsearchresults.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="tpcosts.thirdpartyquotationrequestresults"/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/tpcosts/tpquoterequest/TPQuoteRequestSearchResults.js'></script>
    </jsp:attribute>
    <jsp:body>
        <!-- infobox contains all search results and is styled with nifty corners -->
		<div class="infobox">
		<form:form action="" modelAttribute="tpQuoteRequestSearchForm" name="searchform" id="searchform" method="post">
			<form:hidden path="personid"/>
			<form:hidden path="contact"/>
			<form:hidden path="coid"/>
			<form:hidden path="coname"/>
			<form:hidden path="requestno"/>
			<form:hidden path="statusid"/>
			<form:hidden path="issued"/>
			<form:hidden path="pageNo"/>
			<form:hidden path="resultsPerPage"/>
			
			<t:showResultsPagination rs="${tpQuoteRequestSearchForm.rs}" pageNoId=""/>
			<table id="tpquoterequestsearchresults" class="tablesorter" summary="<spring:message code='tpcosts.tabletpqrequestsearchresults' />" >
				<thead>
					<tr>
						<td colspan="5">
							<spring:message code='tpcosts.thirdpartyquotationrequestresults' /> ${tpQuoteRequestSearchForm.rs.results.size()}
						</td>
					</tr>
					<tr>
						<th id="tpqresquoteno" scope="col"><spring:message code='quoteno' /></th>  
						<th id="tpqresconame" scope="col"><spring:message code='company' /></th>  
						<th id="tpqrescontact" scope="col"><spring:message code='contact' /></th>  
						<th	id="tpqresstatus" scope="col"><spring:message code='status' /></th>
						<th id="tpqresissuedate" scope="col"><spring:message code='issuedate' /></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="5">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody id="keynavResults">
					<c:choose>
						<c:when test="${empty tpQuoteRequestSearchForm.rs.results}">
							<tr>
								<td class="bold center" colspan="5">
								<spring:message code='noresults' />
								</td>
							</tr>
						</c:when>
						<c:otherwise>
							<c:forEach var="tpquoterequest" items="${tpQuoteRequestSearchForm.rs.results}" varStatus="loopStatus">
								<spring:url var="viewURL" value="/viewtpquoterequest.htm?id=${tpquoterequest.tpquoterequestId}" />
								<tr onclick="window.location.href='${viewURL}'; " onmouseover=" $j(this).addClass('hoverrow'); " onmouseout=" if (this.className != 'hoverrow') $j(this).removeClass('hoverrow'); ">
									<td>
										${tpquoterequest.requestNo}
									</td>
									<td>
										<links:companyLinkInfo companyId="${tpquoterequest.coid}" companyName="${tpquoterequest.coname}" active="${tpquoterequest.companyActive}" onStop="${tpquoterequest.companyOnstop}" rowcount="0" copy="true"/>
									</td>
									<td>
										<links:contactLinkInfo contactName="${tpquoterequest.contactName}" personId="${tpquoterequest.personId}" active="${tpquoterequest.contactActive}" rowcount="${loopStatus.count}" />
									</td>
									<td>
										${tpquoterequest.status}
									</td>   
									<td>
										<fmt:formatDate value="${tpquoterequest.issueDate}" dateStyle="medium" type="date"/>
									</td>
								</tr>
							</c:forEach>
						</c:otherwise>
				    </c:choose> 
				</tbody>
			</table>
			<t:showResultsPagination rs="${tpQuoteRequestSearchForm.rs}" pageNoId=""/>
		</form:form>
		</div>
		<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>