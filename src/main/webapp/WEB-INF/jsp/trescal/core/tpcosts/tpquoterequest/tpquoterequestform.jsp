<%-- File name: /trescal/core/tpcosts/tpquoterequest/tpquoterequestform.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tpquoterequest" tagdir="/WEB-INF/tags/tpquoterequest" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
		<span class="headtext">
			<spring:message code="tpcosts.recordthirdpartyquotationrequest" /> 
			<c:if test="${not empty basedOnTPQuoteRequest}">
				<c:out value="(From ${basedOnTPQuoteRequest.requestNo})" />
			</c:if>
		</span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/tpcosts/tpquoterequest/TPQuoteRequestForm.js'></script>
    </jsp:attribute>
    <jsp:body>
    	<t:showErrors path="tpQuoteRequestForm.*" showFieldErrors="true" />
		
		<!-- sub navigation which changes between search quotations and create quotations -->
		<div id="subnav" class="tpquotereqnav">
			<dl>
				<dt><a href="tpquoterequestsearch.htm" title="<spring:message code='tpcosts.searchthirdpartyquotationrequests'/>"><spring:message code="tpcosts.searchthirdpartyquotationrequests" /></a></dt>
				<dt><a href="tpquoterequestform.htm" title="<spring:message code='tpcosts.createthirdpartyquotationrequest'/>" class="selected"><spring:message code="tpcosts.recordthirdpartyquotationrequest" /></a></dt>
			</dl>
		</div>
		
		<form:form modelAttribute="tpQuoteRequestForm" id="tpquoterequestform" name="theForm" action="" method="post">
									
			<div class="infobox">
				
				<!-- this div creates a new multi contact search plugin and the hidden input 'multiContCoroles'
				 	 passes a comma separated list of coroles to search over, no input field causes searches over 
				 	 client corole as default. The 'anchorRedirect' input field should contain the page name to which
				 	 you want the personid to be passed.  -->
				<div id="multiContSearchPlugin">
					<input type="hidden" id="multiContCoroles" value="supplier,business" />
					<input type="hidden" id="customFunction" value="true" />
					<input type="hidden" id="businessCoId" value="${allocatedCompany.key}" />
				</div>
			
			</div>
			
			<c:if test="${not empty requestFromQuote}">
				<div class="infobox">
					<tpquoterequest:showTPQuoteRequestQuotationForm quotation="${requestFromQuote}" models="${models}" />
				</div>
			</c:if>
			<c:if test="${not empty requestFromJob}">
				<div class="infobox">
					<tpquoterequest:showTPQuoteRequestJobForm job="${requestFromJob}" models="${models}" />
				</div>
			</c:if>
			
			<div id="contactinfo" class="infobox">
			
				<fieldset>
				
					<legend><spring:message code="tpcosts.thirdpartyquotedetails" /></legend>
					
					<ol>							
						<c:if test="${not empty basedOnTPQuoteRequest}">
							<li>
								<span class="bold"><spring:message code="tpcosts.creatingnewthirdpartyquotationrequestfromexistingrequest" />&nbsp;${basedOnTPQuoteRequest.requestNo}</span>
							</li>
						</c:if>
						<li>
								<form:label path="dueDate"><spring:message code="tpcosts.requestduedate" />:</form:label>
								<form:input type="date" path="dueDate" />
								<input type="button" value="Clear" onclick=" document.getElementById('dueDate').value=''; return false; " />
								<form:errors  path="dueDate" class="attention"/>
						</li>
						<c:choose>
							<c:when test="${not empty basedOnTPQuoteRequest}">
								<li>
									<label><spring:message code="tpcosts.costtypes" />:</label>
									<span><spring:message code="tpcosts.costtypeswillbethesameasthirdpartyquoterequestitemson" /> ${basedOnTPQuoteRequest.requestNo}</span>
								</li>
							</c:when>
							<c:otherwise>
								<li>
									<label for="costTypeIds" class="ajaxlabel"><spring:message code="tpcosts.defaultcosttypes" />:</label>
									<form:checkboxes path="defaultCostTypes" items="${costTypes}" itemLabel="message" itemValue="name" delimiter="<br/>"/>
								</li>
							</c:otherwise>
						</c:choose>
						<c:if test="${not empty basedOnTPQuoteRequest}">
							<li>
								    <form:label path=""><spring:message code="tpcosts.includeitems" />:</form:label>
									<div class="float-left">
										<c:forEach var="oldQi" items="${basedOnTPQuoteRequest.items}" varStatus="loopStatus">
											<form:checkbox path="oldTPQRItemIds" id="oldTPQRItemIds" class="checkbox" checked="checked" value="${oldQi.id}" />
											${loopStatus.count}. - <cwms:showmodel instrumentmodel="${oldQi.model}"/><br/>
										</c:forEach>
									</div>
									<div class="clear"></div>
							</li>
						</c:if>
						<li>
							<label><spring:message code="contact" />:</label>
							<span id="contactname"><spring:message code="tpcosts.youmustselectacontactabove" /></span>
						</li>
						
					</ol>
					
				</fieldset>
			
			</div>						
			
		</form:form>
    </jsp:body>
</t:crocodileTemplate>