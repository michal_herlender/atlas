<%-- File name: /trescal/core/tpcosts/tpquote/tpquotationaddmodel.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="tpcosts.equipmentsearch"/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/tpcosts/tpquote/TPQuotationAddModel.js'></script>
    </jsp:attribute>
    <jsp:body>
        <!-- error section displayed when form submission fails -->
  <form:errors path="form.*">
        <div class="warningBox1">
                <div class="warningBox2">
                     <div class="warningBox3">
                    	<h5 class="center-0 attention"><spring:message code="tpcosts.errorthirdpartyitems"/></h5>
                      <c:forEach var="e" items="${messages}">
                        <div class="center attention"><c:out value="${e}"/></div>
                      </c:forEach>
                     </div>
                </div>
        </div>
  </form:errors>
<!-- end error section -->

<form:form action="" id="searchtpquoteitemform" method="post" onsubmit=" showUnLoadWarning = false; " modelAttribute="form">
	
	<form:hidden path="submitted" />
	
	<div>
				
		<!-- infobox div containing search elements -->									
		<div class="infobox">
			
			<div><spring:message code="tpcosts.addingitemsfor"/> &nbsp;<a href="<spring:url value='viewtpquote.htm?&id=${form.tpQuote.id}'/>" class="mainlink"><spring:message code="tpcosts.thirdpartyquotation"/>&nbsp;<c:out value="${form.tpQuote.qno}"/></a></div>
			<!-- displays content in column 60% wide (left) -->
			<div class="displaycolumn-60">
															
				<!-- displays form for searching equipment to add to quotation -->
				<fieldset>
	
					<legend><spring:message code="tpcosts.equipmentsearch"/></legend>
																	
					<ol>
						<li>
							<label><spring:message code="domain"/>:</label>
							<!-- float div left -->
							<div class="float-left">
								<form:input type="text" path="search.domainNm" id="domainNm" value="${form.search.domainNm}" tabindex="1"/>
								<input type="hidden" path="search.domainId" id="domainId" value="${form.search.domainId}"/>
							</div>
							<div class="clear-0"></div>	
						</li>		
						<li>
							<label><spring:message code="family"/>:</label>
							<!-- float div left -->
							<div class="float-left">
								<input type="text" path="search.familyNm" id="familyNm" value="${form.search.familyNm}" tabindex="2"/>
								<input type="hidden" path="search.familyId" id="familyId" value="${form.search.familyId}"/>
							</div>
							<div class="clear-0"></div>	
						</li>	
					    <li id="descrip">
                            <label for="description"><spring:message code="description.description"/>:</label>
                            <!-- float div left -->
                            <div class="float-left">
								<form:input type="text" path="search.descNm" id="descNm" value="${form.search.descNm}" tabindex="3" />
								<input type="hidden" path="search.descId" id="descId" value="${form.search.descId}" />
                            </div>
                            <!-- clear floats and restore pageflow -->
                            <div class="clear-0"></div>
                            <form:errors path="search.descId" cssClass="errorabsolute"/>
                        </li>

						<li id="manufacturer">
							<label for="mfr"><spring:message code="manufacturer"/>:</label>
							<!-- float div left -->
							<div class="float-left">
								<!-- this div creates a new manufacturer jquery search plugin -->	
								<div class="mfrSearchJQPlugin">
									<input type="hidden" name="fieldId" value="search.mfrId" />
									<input type="hidden" name="fieldName" value="search.mfrNm" />
									<input type="hidden" name="textOnlyOption" value="true" />
									<input type="hidden" name="tabIndex" value="1" />
									<!-- manufacturer results appear here -->
								</div>
							</div>																							
							<!-- clear floats and restore pageflow -->
							<div class="clear-0"></div>
						</li>
						<li>
							<label for="instmodel"><spring:message code="model"/>:</label>
							<form:input path="search.model" tabindex="2" />
							<form:errors path="search.model" cssClass="errorabsolute"/>
						</li>
						<li>
							<label><spring:message code="salescategory"/>:</label>
							
								
							<!-- float div left -->
                            <div class="float-left">
								<form:input path="search.salesCat" id="salesCat" value="${form.search.salesCat}" tabindex="6"/>
								<input type="hidden" name="search.salesCatId" id="salesCatId" value="${form.search.salesCatId}"/>
							</div>                           
							<div class="clear-0"></div>	
						</li>   
						
						
						<li>
								<label><spring:message code="instmod.modeltype"/></label>
								<form:select path="modelTypeSelectorChoice" items="${modelTypeSelections}" itemLabel="value" itemValue="key"/>
						</li>

						<li>
							<label for="submit">&nbsp;</label>
							<input type="submit" value="<spring:message code='search'/>" name="submit" class="float-left" id="submit" tabindex="0" />
							<input type="button" value="<spring:message code='tpcosts.clearform'/>" id="clear" class="float-right" onclick=" clearForm(); " tabindex="0" />
							<!-- clear floats and restore page flow -->
							<div class="clear-0"></div>
						</li>
					</ol>
					
				</fieldset>											
			
			</div>
											
			<!-- displays content in column 40% wide (right) -->
			<div id="searchdefaults" class="displaycolumn-40">
				
				<!-- displays form for entering default settings when adding equipment to quote -->
				<fieldset>

					<legend>Choose defaults</legend>
					
					<ol>
						<li>																								
							<label for="defaultCaltypes">Cal Type:</label>
							<c:set var="count" value="1" />
							<c:forEach var="ct" items="${form.caltypes}">
								<c:if test="${count > 1}">
									<label>&nbsp;</label>
								</c:if>
								
								<c:set var="checked" value="" />
								<c:forEach var="tpct" items="${form.tpQuote.defaultCalTypes}">
									<c:if test="${tpct.caltype.calTypeId == ct.key}">
										<c:set var="checked" value="checked='checked'" />
									</c:if>
								</c:forEach>
								
								<form:checkbox path="defcaltypeids" value="${ct.key}" checked="${checked}" />
								<t:showTranslationOrDefault translations="${ct.value.serviceType.longnameTranslation}" defaultLocale="${defaultlocale}"/><br />
								<!-- this blank div clears floats and restores page flow -->
								<div class="clear"></div>
								<c:set var="count" value="${count + 1}" />
							</c:forEach>
						</li>																							
						<li>
							<form:label path="tpQuote.defaultQty">Quantity:</form:label>
							<form:select path="tpQuote.defaultQty" id="defaultQty">
								<c:forEach var="q" begin="1" end="${form.maxQuantity}">
								<option value="${q}"<c:if test="${form.tpQuote.defaultQty == q}"> selected="selected" </c:if>><c:out value="${q}"/></option>
								</c:forEach>
							</form:select>
						</li>
					</ol>
				</fieldset>					
			
			</div>
			
			<!-- this blank div clears floats and restores page flow -->
			<div class="clear"></div>
			
		</div>
		<!-- end of infobox div containing search elements -->
		
		<!-- sub navigation menu which displays and hides sections of page as well as linking to external pages -->
		<div id="subnav">
			<dl>
				<dt><a href="#" onclick="switchMenuFocus(menuElements, 'searchresults-tab', false); return false;" id="searchresults-link" title="<spring:message code='tpcosts.viewnewthirdpartyquoteitemsearchresults'/>" class="selected"><spring:message code="tpcosts.searchresults"/></a></dt>
				<dt><a href="#" onclick="switchMenuFocus(menuElements, 'searchbasket-tab', false); return false;" id="searchbasket-link" title="<spring:message code='tpcosts.viewnewthirdpartyquoteitembasket'/>"><spring:message code="tpcosts.quoteitembasket"/>(<span class="basketCount"><c:choose><c:when test="${empty form.modelIds}">0</c:when>
																																																																										<c:otherwise>${form.modelIds.size()}</c:otherwise>	
																																																																							  </c:choose>
																																																																	</span>)
				</a></dt>
			</dl>
		</div>
		<!-- end of sub navigation menu -->
										
		<!-- displays instrument search results -->								
		<div id="searchresults-tab">
		
			<div class="results">
												
				<h5>
					<c:choose> 
						<c:when test="${not empty form.searchModels}">
							<c:choose> 
								<c:when test="${form.searchModels.size() > 1}">
									<c:out value="${form.searchModels.size()}"/> <spring:message code="tpcosts.modelsfound" />
								</c:when>
								<c:otherwise>
									<c:out value="${form.searchModels.size()}"/><spring:message code="tpcosts.modelfound" />
								</c:otherwise>
							</c:choose>
						</c:when>
						<c:otherwise>
							<spring:message code="noresults" />
						</c:otherwise>
					</c:choose>
				</h5>
				
				<!-- this list displays each instrument returned in the search -->
				<ul>
				
					<li style=" display: none; "></li>
				
					<c:forEach var="key" items="${form.searchModels.keySet()}">
						 <c:set var="model" value="${form.searchModels.get(key)}"/>
						<li id="model${model.modelid}">
							<!-- this div is floated left and displays the instrument name -->
							<div class="result_text">
								<span>
									<!-- this link is used to create the block hover effect -->
									<a id="getmodelname${model.modelid}" href="#" 
										onclick=' loadAddItemSection(${model.modelid}, "<cwms:showmodel instrumentmodel="${model}"/>"); return false; '
									>
										<cwms:showmodel instrumentmodel="${model}"/> -- <t:showTranslationOrDefault translations="${model.modelType.modelTypeNameTranslation}" defaultLocale="${defaultlocale}"/>
									</a>															
								</span>
							</div>
							<!-- this div is floated right and displays the add default link -->
							<div class="add_default">
								<span>
									<c:set var="escapedModelName"><spring:escapeBody javaScriptEscape="true"><cwms:showmodel instrumentmodel="${model}"/></spring:escapeBody></c:set>
									<a href="" class="mainlink" id="default${model.modelid}" 
										onclick="event.preventDefault(); addDefaults(event, ${model.modelid}, '${escapedModelName}');" >
										<spring:message code="tpcosts.adddefaults" />
									</a>
								</span>
							</div>
																	
						</li>
						
					</c:forEach>
				
				</ul>
				
				<!-- this blank div clears floats, restores page flow and applies border to last list item  -->
				<div class="clear_border">&nbsp;</div>
			
			</div>
		
		</div>
		<!-- end of instrument search results section -->
		
		<!-- this section displays all instruments that have been added to the third party quote items basket -->																
		<div id="searchbasket-tab" class="hid">
		
			<div class="infobox">
											
				<h5>
					<spring:message code="tpcosts.quotebasket" />
				</h5>
				
				<!-- link to delete all items in the quote basket -->
				<a href="#" class="mainlink-float" onclick=" deleteFromBasket('all', ''); return false; " id="removelink" title="<spring:message code='tpcosts.removeallitemsfromthebasket'/>"><spring:message code="tpcosts.removeallitems" /></a>
														
				<!-- clear floats and restore page flow -->
				<div class="clear"></div>
					
				<table class="default2" id="basketitemstable" summary="<spring:message code='tpcosts.tableitemsbasket'/>">
					<thead>
						<tr>
							<td colspan="8"><spring:message code="tpcosts.quoteitembasket"/>(<span class="basketCount"><c:choose>
																															<c:when test="${empty form.modelIds}">0</c:when>
																															<c:otherwise>
																																<c:out value="${form.modelIds.size()}"/>
																															</c:otherwise>
																														</c:choose>
																					</span>)</td>
						</tr>
						<tr>
							<th class="addtpqitem" scope="col"><spring:message code="item"/></th>
							<th class="addtpqinst" scope="col"><spring:message code="instmodelname"/></th>
							<th class="addtpqqty" scope="col"><spring:message code="qty"/></th>
							<th class="addtpqcaltype" scope="col"><spring:message code="caltype"/></th>
							<th class="addtpqdel" scope="col"><spring:message code="delete"/></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="8">&nbsp;</td>
						</tr>
					</tfoot>																			
					<tbody>
						<c:set var="count" value="0"/>
						<c:set var="itemcount" value="1"/>												
						<c:choose>
							<c:when test="${form.modelIds.size() > 0}">
							<c:forEach var="modelId" items="${form.modelIds}">
								
								<tr id="basketitem${modelId}" <c:choose>
																		<c:when test="${count % 2 == 0}"> class="even" </c:when>
																		<c:otherwise> class="odd" </c:otherwise>
															  </c:choose>>
									<td class="center"><c:out value="${itemcount}"/></td>
									<td>
										<c:out value="${form.modelNames.get(count)}"/>
										
										<input path="modelIds" value="${form.modelIds}" type="hidden" />
										
										<input path="modelNames" value="${form.modelNames.get(count)}" type="hidden" />
										
										<input path="itemNos" value="${form.itemNos.get(count)}" type="hidden" />
									</td>
									<td class="center">
										<form:select path="qtys">
											<c:forEach var="q" begin="1" end="${form.maxQuantity}">
												<option value="${q}" <c:if test="${form.qtys.get(count) == q}"> selected="selected" </c:if>><c:out value="${q}"/></option>
											</c:forEach>
										</form:select>
									</td>						
									<td class="center">
										<form:select path="calTypeIds">
											<c:forEach var="cal" items="${form.caltypes}">
												<option value="${cal.calTypeId}" 
													<c:if test="${form.calTypeIds.get(count) == cal.calTypeId}"> selected="selected" </c:if>>
													<t:showTranslationOrDefault translations="${cal.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/>
												</option>
											</c:forEach>
										</form:select>
									</td>													
									<td class="center">
										<a href="#" onclick=" deleteFromBasket('item', 'basketitem${modelId}'); return false;" class="mainlink"><img src="img/icons/delete.png" width="16" height="16" title="<spring:message code='tpcosts.deletefrombasket' />" alt="<spring:message code='tpcosts.deletefrombasket' />"/></a>
									</td>
								</tr>
																						
								<c:set var="count" value="${count + 1}" />
								<c:set var="itemcount" value="${itemcount + 1}" />											
				
							</c:forEach>
							
							</c:when>
							<c:otherwise>										
							
							<tr>
								<td colspan="8" class="center bold"><spring:message code="tpcosts.yourthirdpartyquotationitembasketisempty" /></td>
							</tr>												
								
							</c:otherwise>
						</c:choose>
					</tbody>								
				</table>
				
			</div>
			<div class="center">
				<input type="submit" name="additems3" value="<spring:message code='add' />" onclick="$('submitted').value = 'add';"/>
				&nbsp;
				<input type="submit" name="additems4" value="<spring:message code='tpcosts.addandedit' />" onclick="$('submitted').value = 'bulkedit';"/>
			</div>
		</div>
		<!-- end of third party quote items basket section -->
										
		<script type='text/javascript'>
																			
			/** List of all available CostTypes	*/
			var costTypes = new Array(
										<c:forEach var="ct" items="${form.availableCostTypes}" varStatus="statuscosttype">
											<c:choose>
												<c:when test="${statuscosttype.index == form.availableCostTypes.size()}">
													new CostType(${ct.key}, '${ct.value.name}')
												</c:when>
												<c:otherwise>
													new CostType(${ct.key}, '${ct.value.name}'),
												</c:otherwise>
											</c:choose>	
										</c:forEach>
									);
			
			/** Simple class to represent a CostType */
			function CostType(id, name)
			{
				this.id = id;
				this.name = name;
			}
			
			/** List of all available CalTypes **/
			var calTypes = new Array(new CalType(0, 'N/A', 'N/A'));
			<c:forEach var="cal" items="${form.caltypes}">
				calTypes.push(new CalType(${cal.key}, '<t:showTranslationOrDefault translations="${cal.value.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/>', 
													  '<t:showTranslationOrDefault translations="${cal.value.serviceType.longnameTranslation}" defaultLocale="${defaultlocale}"/>'));
			</c:forEach>
			
			/** Simple helper class for a calibratio type **/
			function CalType(id, sname, lname)
			{
				this.id = id;
				this.sname = sname;
				this.lname = lname;
			}
			
			/** maximum qty field **/
			var maxQuantity = ${form.maxQuantity};
											
		</script>
	</div>
	
</form:form>
    </jsp:body>
</t:crocodileTemplate>