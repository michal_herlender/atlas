<%-- File name: /trescal/core\tpcosts\tpquote\tpquoteform.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tpquoterequest" tagdir="/WEB-INF/tags/tpquoterequest" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="tpcosts.createquotation" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/tpcosts/tpquote/TPQuoteForm.js'></script>
    </jsp:attribute>
    <jsp:body>
        <!-- error section displayed when form submission fails -->
       	
				<div class="infobox">
				
					<form:form method="post" id="tpquoteform" action="" modelAttribute="tpquoteform">
					<form:errors path="*">
        				<div class="warningBox1">
                			 <div class="warningBox2">
                    	 	  <div class="warningBox3">
                    	   <h5 class="center-0 attention"><spring:message code="tpcosts.errorthirdpartyquoteitems" /></h5>
                            	<c:forEach var="e" items="${messages}">
                               	<div class="center attention"><c:out value="${e}"/></div>
                            	</c:forEach>
                     	 	</div>
                 			</div>
           			</div>
       	   		    </form:errors>
						<fieldset>
						
							<legend><spring:message code="tpcosts.confirmthirdpartyquotationdetails" /></legend>
							
							<ol>
								<li>
									<label><spring:message code="contact" />:</label>
									<span>${tpquoteform.tpQuotation.contact.name}</span>
								</li>
								<li>
									<label><spring:message code="subdivision" />:</label>
									<span>
									<c:choose>
										<c:when test="${tpquoteform.tpQuotation.contact.sub.subname == ''}">
											<spring:message code="tpcosts.defaultsubdivision" />
										</c:when>
			      					    <c:otherwise>
											${tpquoteform.tpQuotation.contact.sub.subname}
										</c:otherwise>
                   					</c:choose>
									</span>
								</li>
								<li>
									<label><spring:message code="company" />:</label>
									<span>${tpquoteform.tpQuotation.contact.sub.comp.coname}</span>
								</li>
								<li>
									<form:label path="tpQuotation.tpqno"><spring:message code="tpcosts.tpquotenumber" />:</form:label>
									<form:input type="text" path="tpQuotation.tpqno" />
									<form:errors path="tpQuotation.tpqno" cssClass="errorabsolute"/>
								</li>
								<li>
									<form:label path="tpQuotation.regdate"><spring:message code="tpcosts.receiptdate" />:</form:label>
									<form:input type="date" path="tpQuotation.regdate"  id="tpQuotation.regdate"  />
									<form:errors path="tpQuotation.regdate" cssClass="errorabsolute"/>
								</li>
								<li>
									<form:label path="tpQuotation.issuedate"><spring:message code="tpcosts.issuedate" />:</form:label>
									<form:input type="date" path="tpQuotation.issuedate"  id="tpQuotation.issuedate"  />
									<form:errors path="tpQuotation.issuedate" cssClass="errorabsolute"/>
								</li>
								<li>
									<form:label path="tpQuotation.duration"><spring:message code="tpcosts.validity" />:</form:label>
									<form:input type="text" path="tpQuotation.duration" value="${status.value}" /> Days
									<form:errors path="tpQuotation.duration" cssClass="errorabsolute"/>
								</li>
								<li>
									<form:label path="tpQuotation.status.statusid"><spring:message code="status" />:</form:label>
									<form:select path="tpQuotation.status.statusid">
										<c:forEach var="s" items="${statusList}">
											<option value="${s.statusid}">
											<t:showTranslationOrDefault translations="${s.nametranslations}" defaultLocale="${defaultlocale}" /></option>
										</c:forEach>
									</form:select>
									<form:errors path="tpQuotation.status.statusid" cssClass="errorabsolute"/>
								</li>
								<li>
									<form:label path="currencyCode"><spring:message code="currency" />:</form:label>
									<form:select path="currencyCode">
										<c:forEach var="curopt" items="${currencyopts}">
										
											<option value="${curopt.optionValue}"
											<c:if test="${curopt.selected == true}">
												selected="selected"
											</c:if>
											>
											${curopt.currency.currencyCode}
											<c:choose>
												<c:when test="${curopt.companyDefault == true}">
												[<spring:message code="tpcosts.companydefaultrate" />]</c:when>
												<c:when test="${curopt.systemDefault == true}">
												[<spring:message code="tpcosts.systemdefaultrate" />]</c:when>
											</c:choose>
											</option>
										
										</c:forEach>
									</form:select>
									<form:errors path="currencyCode" cssClass="errorabsolute"/>
								</li>
								<li>
									<label for="submit">&nbsp;</label>
									<input type="submit" value="<spring:message code="submit" />" name="submit" id="submit" />
								</li>
							
							</ol>
						
						</fieldset>
					
					</form:form>
				
				</div>
    </jsp:body>
</t:crocodileTemplate>