<%-- File name: /trescal/core/tools/tooltip/tooltip_job.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div id="jobTT">
	<!--  main navigation bar -->
	<div class="slatenav-main">
		<ul>
			<li>
				<a id="ttj_job-link" onmouseover=" event.preventDefault(); switchMenuFocus(Elements1, 'ttj_job-tab', false); " class="selected">
					<spring:message code="job"/> 
				</a>
			</li>
		</ul>
	</div>
	<div id="ttj_job-tab" class="vis clearL">												
		<!-- job sub navigation bar -->
		<div class="slatenav-sub">
			<ul>
				<li>
					<a onmouseover=" event.preventDefault(); switchMenuFocus(Elements2, 'ttj_summary-tab', false); " id="ttj_summary-link" class="selected">
						<spring:message code="summary"/>
					 </a>
				</li>
				<li>
					<a onmouseover=" event.preventDefault(); switchMenuFocus(Elements2, 'ttj_items-tab', false); " id="ttj_items-link">
						<spring:message code="items"/>
					</a>
				</li>
			</ul>
		</div>
		<!-- job summary tab  -->
		<div id="ttj_summary-tab">
			<fieldset class="newtooltip">
				<ol>
					<li>
						<label><spring:message code="company"/><c:out value=" :"/></label>
						<span><c:out value="${jobDto.contact.subdiv.company.coname}"/></span>
					</li>
					<li>
						<label><spring:message code="subdivision"/><c:out value=" :"/></label>
						<span><c:out value="${jobDto.contact.subdiv.subname}"/></span>
					</li>
					<li>
						<label><spring:message code="contact"/><c:out value=" :"/></label>
						<span><c:out value="${jobDto.contact.name}"/></span>
					</li>
					<li>
						<label><spring:message code="telephone"/><c:out value=" :"/></label>
						<span><c:out value="${jobDto.contact.telephone}"/></span>
					</li>
					<li>
						<label><spring:message code="company.email"/><c:out value=" :"/></label>
						<span>
							<c:if test="${not empty jobDto.contact.email}">
								<a href="mailto:${jobDto.contact.email}" class="mainlink">
									<c:out value="${jobDto.contact.email}"/>
								</a>
							</c:if>
						</span>
					</li>
					<li>
						<label><spring:message code="status"/><c:out value=" :"/></label>
						<span><c:out value="${jobDto.jobStatus.value}"/></span>
					</li>
					<li>
						<label><spring:message code="clientref"/><c:out value=" :"/></label>
						<span><c:out value="${jobDto.clientRef}"/></span>
					</li>
					<li>
						<label><spring:message code="regdate"/><c:out value=" :"/></label>
						<span>
							<c:choose>
								<c:when test="${not empty jobDto.regDate}">
									<fmt:formatDate value="${jobDto.regDate}" type="date" dateStyle="SHORT" />
								</c:when>
								<c:otherwise>
									<spring:message code="instrumenttooltip.notset" />
								</c:otherwise>
							</c:choose>
						</span>
					</li>
					<li>
						<label><spring:message code="datein"/><c:out value=" :"/></label>
						<span>
							<c:choose>
								<c:when test="${not empty jobDto.receiptDate}">
									<fmt:formatDate value="${jobDto.receiptDate}" type="date" dateStyle="SHORT" />
								</c:when>
								<c:otherwise>
									<spring:message code="instrumenttooltip.notset" />
								</c:otherwise>
							</c:choose>
						</span>
					</li>
					<li>
						<label><spring:message code="datecomplete"/><c:out value=" :"/></label>
						<span>
							<c:choose>
								<c:when test="${not empty jobDto.dateComplete}">
									<fmt:formatDate value="${jobDto.dateComplete}" type="date" dateStyle="SHORT" />
								</c:when>
								<c:otherwise>
									<spring:message code="instrumenttooltip.notset" />
								</c:otherwise>
							</c:choose>
						</span>
					</li>
				</ol>
			</fieldset>
		</div>
		<!-- end of job summary tab -->							
		<!-- job items tab -->
		<div id="ttj_items-tab" class="hid">
			<table class="default2 jitems" summary="This table shows all job items for the current job">
				<thead>
					<tr>
						<td colspan="6">
							<spring:message code="items"/>
							<c:out value=" ( ${jobDto.jobitems.size()} ) "/>
						</td>
					</tr>
					<tr>
						<th class="item" scope="col"> <spring:message code="item"/></th>
						<th class="inst" scope="col"> <spring:message code="instmodelname"/></th>
						<th class="serial" scope="col"> <spring:message code="serialno"/></th>
						<th class="plant" scope="col"> <spring:message code="plantno"/></th>
						<th class="caltype" scope="col"> <spring:message code="servicetype"/></th>
						<th class="status" scope="col"> <spring:message code="status"/></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="6">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
					<c:forEach var="jiDto" items="${jobDto.jobitems}">
						<tr>
							<td class="item"><c:out value="${jiDto.itemno}" /></td>
							<td class="inst"><c:out value="${jiDto.instrument.getInstrumentModelNameViaFields(false)}" /></td>
							<td class="serial"><c:out value="${jiDto.instrument.serialno}" /></td>
							<td class="plant"><c:out value="${jiDto.instrument.plantno}" /></td>
							<td class="caltype"><c:out value="${jiDto.serviceType.shortName}" /></td>
							<td class="status"><c:out value="${jiDto.state.value}" /></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<!-- end of job items tab -->
	</div>
	<!-- end of job tab -->
</div>
<!-- end of main div -->