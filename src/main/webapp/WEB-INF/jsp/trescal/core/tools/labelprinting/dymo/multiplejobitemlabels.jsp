<%-- File name: /trescal/core/tools/labelprinting/dymo/multiplejobitemlabels.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:showErrors path="form.*" showFieldErrors="true" />

<div id="printJobItemLabelsOverlay">
	<form:form id="jobItemLabelsForm" modelAttribute="form">
		<form:hidden path="jobId"/>
		<table class="default4 jobItemsToPrint">
			<thead>
				<tr>
					<td colspan="4"><spring:message code="jobitems" /> (${job.items.size()})</td>
				</tr>
				<tr>
					<th class="add">
						<input type="checkbox" onclick=" selectAllNamedInputs(this.checked, 'printJobItemLabel'); " />
						<spring:message code="jobitems" /> 
					</th>
					<th class="acc">
						<input type="checkbox" onclick=" selectAllNamedInputs(this.checked, 'printAccessoryLabel'); " />
						<spring:message code="accessoriesanddefects.accessories" /> 
					</th>
					<th class="itemno"><spring:message code="itemno" /></th>
					<th class="itemdesc"><spring:message code="createdelnote.coldesc" /></th>
			</tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when test="${not empty job.items}">
					<c:forEach var="item" items="${job.items}">
						<tr>
							<td class="add">
								<form:checkbox path="printJobItemLabel[${item.jobItemId}]"/>
								<form:input type="number" min="0" max="9" path="copiesJobItemLabel[${item.jobItemId}]" />
							</td>
							<td class="acc">
								<form:checkbox path="printAccessoryLabel[${item.jobItemId}]"/>
								<form:input type="number" min="0" max="9" path="copiesAccessoryLabel[${item.jobItemId}]" />
							</td>
							<td class="itemno">${item.itemNo}</td>
							<td class="itemdesc">
								<cwms:showmodel instrumentmodel="${item.inst.model}" />
								<pre>${accessories[item.jobItemId]}</pre>
							</td>
						</tr>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<tr class="text-center bold">
						<td colspan="4"><spring:message code="viewjob.string15" /><%-- No Items on job --%></td>
					</tr>
				</c:otherwise>
			</c:choose>
			</tbody>
		</table>
		<div class="text-center marg-top marg-bot">
			<input type="button" onclick=" event.preventDefault(); printDymoJobItemLabels(); " value="<spring:message code='viewbatchcalibration.printlab' />" />
		</div>
	</form:form>
</div>
