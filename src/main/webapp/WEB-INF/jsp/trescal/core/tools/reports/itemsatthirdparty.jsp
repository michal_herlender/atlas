<%-- File name: /trescal/core/tools/reports/itemsatthirdparty.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>


<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code='tools.subcontractedjobitems' /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
    	<script type="module" src="script/components/cwms-items-at-tp/cwms-items-at-tp.js" ></script>
    	<script src='script/thirdparty/jQuery/jquery.js' ></script>
        <script type='text/javascript' src='script/trescal/core/tools/reports/ItemsAtThirdParty.js'></script>
    </jsp:attribute>
    <jsp:body>
      <!-- this web component display third party items -->
		<div class="infobox"><a href="subcontractedJobItemsexport.xlsx" class="mainlink"><spring:message code="web.export.excel"/></a> 
    		<cwms-items-at-tp></cwms-items-at-tp>
    	</div>
    </jsp:body>
</t:crocodileTemplate>