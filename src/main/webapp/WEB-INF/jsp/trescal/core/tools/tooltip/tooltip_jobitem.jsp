<%-- File name: /trescal/core/tools/tooltip/tooltip_jobitem.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%-- 
	Only the job item components are included 
	(job costing and quotation item used less, to be done later)
	Images also not included (not really necessary!)) 
--%>

<div id="jobItemCostingTT">	
<%-- main navigation bar --%>
	<div class="slatenav-main">
		<ul>
			<li>
				<a id="ttjc_jobitem-link" onmouseover=" event.preventDefault(); switchMenuFocus(Elements1, 'ttjc_jobitem-tab', false); " class="selected">
					<spring:message code="vm_global.jobitem"/> 
				</a>
			</li>
		</ul>
	</div>
	<div id="ttjc_jobitem-tab" class="vis clearL">												
		<!-- job sub navigation bar -->
		<div class="slatenav-sub">
			<ul>
				<li>
					<a onmouseover=" event.preventDefault(); switchMenuFocus(Elements2, 'ttjc_jisummary-tab', false); " id="ttjc_jisummary-link" class="selected">
						<spring:message code="summary" /> 
					</a>
				</li>
				<li>
					<a onmouseover=" event.preventDefault(); switchMenuFocus(Elements2, 'ttjc_jiactions-tab', false); " id="ttjc_jiactions-link">
						 <spring:message code="viewinstrument.actions" /> 
					</a>
				</li>
				<li>
					<a onmouseover=" event.preventDefault(); switchMenuFocus(Elements2, 'ttjc_jicontrev-tab', false); " id="ttjc_jicontrev-link">
						 <spring:message code="jicontractreview.contrrew" /> 
					</a>
				</li>
			</ul>
		</div>
		<%-- job item summary for sub tab index --%>
		<div id="ttjc_jisummary-tab">
			<fieldset class="newtooltip">
				<ol style=" background: url( 'img/noimage.jpg') top right no-repeat; ">
					<li style=" width: 70%; ">
						<label> <spring:message code="instmodelname"/><c:out value=" :"/> </label>
						<div class="fleftTooltip" style=" width: 60%; ">
							<c:out value="${jiDto.instrument.getInstrumentModelNameViaFields(false)}" />
						</div>
						<div style=" clear: left;  "></div>
					</li>
					<li>
						<label><spring:message code="viewinstrument.customerdescription"/></label>
						<span>${jiDto.instrument.customerDescription}</span>
					</li>
					<li style=" width: 70%; ">
						<label> <spring:message code="serialno"/><c:out value=" :"/> </label>
						<span><c:out value="${jiDto.instrument.serialno}" /></span>
					</li>
					<li style=" width: 70%; ">
						<label> <spring:message code="plantno"/><c:out value=" :"/> </label>
						<span><c:out value="${jiDto.instrument.plantno}" /></span>
					</li>
					<li style=" width: 70%; ">
						<label> <spring:message code="servicetype"/><c:out value=" :"/> </label>
						<span><c:out value="${jiDto.serviceType.bothNames}" /></span>
					</li>
					<li style=" width: 70%; ">
						<label> <spring:message code="datein"/><c:out value=" :"/> </label>
						<span><fmt:formatDate value="${jiDto.dateIn}" type="date" dateStyle="SHORT" /></span>
					</li>
					<li style=" width: 70%; ">
						<label> <spring:message code="duedate"/><c:out value=" :"/> </label>
						<span><fmt:formatDate value="${jiDto.dueDate}" type="date" dateStyle="SHORT" /></span>
					</li>
					<li style=" width: 70%; ">
						<label> <spring:message code="datecomplete"/><c:out value=" :"/> </label>
						<span><fmt:formatDate value="${jiDto.dateComplete}" type="date" dateStyle="SHORT" /></span>
					</li>
					<%-- Laboratory / Departments (from work requiremnts) removed for now --%>
					<li>
						<label> <spring:message code="status"/><c:out value=" :"/> </label>																	
						<span><c:out value="${jiDto.state.value}" /></span>
					</li>
				</ol>
			</fieldset>
		</div>
		<%-- end of job item summary tab for sub index --%>
								
		<%-- job item actions tab --%>
		<div id="ttjc_jiactions-tab" class="hid">
			<table class="default2">
				<thead>
					<tr>
						<td colspan="6"> <spring:message code="jiactions.actactions"/></td>
					</tr>
					<tr>															
						<th class="activity" scope="col"> <spring:message code="jiactions.activity"/></th>
						<th class="compby" scope="col"> <spring:message code="jiactions.startedby"/></th>
						<th class="compon" scope="col"> <spring:message code="viewjob.startedon"/></th>
						<th class="endstatus" scope="col"> <spring:message code="jiactions.endstatus"/></th>
						<th class="remarks" scope="col"> <spring:message code="jiactions.remark"/></th>
						<th class="time" scope="col"> <spring:message code="jiactions.timespent"/></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="action" items="${jiDto.actions}">
							<tr class="odd">																						
								<td><c:out value="${action.activity.value}"/></td>
								<td><c:out value="${action.startedBy.value}" /></td>
								<td><fmt:formatDate type="both" dateStyle="SHORT" timeStyle="SHORT" value="${action.startStamp}" /></td>
								<td><c:out value="${action.endStatus.value}"/></td>
								<td><c:out value="${action.remark}" /></td>
								<td><c:out value="${action.timeSpent} " /><spring:message code="jobcost.mins"/></td>
							</tr>
					</c:forEach>
					<c:if test="${empty jiDto.actions}">
						<tr class="odd center bold">
							<td colspan="6"> <spring:message code="viewjob.none"/></td>
						</tr>
					</c:if>
				</tbody>
			</table>
		</div>
		<%-- end of job item actions tab --%>													 			
						
		<%-- job item contract review tab --%>
		<div id="ttjc_jicontrev-tab" class="hid">
			<fieldset class="newtooltip">
				<ol>
					<li>
						<label> <spring:message code="jicontractreview.turnaround"/><c:out value=" :"/> </label>
						<span> <c:out value="${jiDto.turn}"/> </span>
					</li>
					<li>
						<label> <spring:message code="duedate"/><c:out value=" :"/> </label>
						<span> <fmt:formatDate value="${jiDto.dueDate}" type="date" dateStyle="SHORT" /> </span>
					</li>
				</ol>
			</fieldset>
			<table class="default2">
				<thead>
					<tr>
						<td colspan="6"> <spring:message code="jicontractreview.contrrew"/></td>
					</tr>
					<tr>
						<th class="costtype" scope="col"> <spring:message code="jicontractreview.costtype"/> </th>
						<th class="costs" scope="col"> <spring:message code="jicontractreview.costs"/> </th>
						<th class="disc" scope="col"> <spring:message code="jicontractreview.discount"/> </th>
						<th class="discval" scope="col"> <spring:message code="jicontractreview.discval"/> </th>
						<th class="finalcost" scope="col"> <spring:message code="jicontractreview.finalcost"/> </th>
						<th class="costsource" scope="col"> <spring:message code="jicontractreview.costsource"/> </th>
					</tr>
				</thead>
				<tbody>
					<%-- TODO exclude inactive costs --%>
					<c:forEach var="priceDto" items="${jiDto.priceContractReview.prices}">
						<c:if test="${priceDto.active}">
							<tr class="odd">
								<td><c:out value="${priceDto.costType}"/></td>
								<td><c:out value="${priceDto.totalCost} ${jiDto.job.currency.value}"/></td>
								<td><c:out value="${priceDto.discountRate} %"/></td>
								<td><c:out value="${priceDto.discountValue} ${jiDto.job.currency.value}"/></td>
								<td><c:out value="${priceDto.finalCost} ${jiDto.job.currency.value}"/></td>
								<td><c:out value="${priceDto.costSource}"/></td>
							</tr>
						</c:if>
					</c:forEach>
					<c:if test="${empty jiDto.priceContractReview.prices}">
						<tr class="odd center bold">
							<td colspan="6"> <spring:message code="jicontractreview.string18"/> </td>
						</tr>
					</c:if>
				</tbody>
			</table>
			<table class="default2">
				<thead>
					<tr>
						<td colspan="3"> <spring:message code="jicontractreview.contrrevhist"/> </td>
					</tr>
					<tr>
						<th class="reviewer" scope="col"> <spring:message code="jicontractreview.reviewer"/> </th>
						<th class="daterev" scope="col"> <spring:message code="jicontractreview.datereved"/> </th>
						<th class="notes" scope="col"> <spring:message code="jicontractreview.notesleft"/> </th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="crDto" items="${jiDto.contractReviews}">
						<tr class="odd">
							<td><c:out value="${crDto.reviewBy.value}" /></td>
							<td><fmt:formatDate type="date" dateStyle="SHORT" value="${crDto.reviewDate}" /></td>
							<td><c:out value="${crDto.comments}" /></td>
						</tr>
					</c:forEach>
					<c:if test="${empty jiDto.contractReviews}" >
						<tr class="odd center bold">
							<td colspan="3"> <spring:message code="jicontractreview.string18"/> /td>
						</tr>
					</c:if>
				</tbody>
			</table>
		</div>
		<%-- end of job item contract review tab --%>
	</div>
	<%-- end of job item tab --%>
</div>
<%--  end of main div--%>