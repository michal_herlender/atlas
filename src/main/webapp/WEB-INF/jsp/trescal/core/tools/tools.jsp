<%-- File name: /trescal/core/tools/tools.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="tools.tools"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/tools/Tools.js'></script>
	</jsp:attribute>
	<jsp:body>

		<fieldset>
			
			<legend><spring:message code="tools.labelprinting" /></legend>
			
				<spring:message var="textNoLabelPrinter" code="error.labelprinter.notfound" />
 				<ul>
				<li>
					<label style="width: 300px;"><spring:message code="tools.onsitestdcalibrationlabels" /></label>
					<select>
						<c:forEach var="i" items="${labelCounts}">
							<option value="${i}">${i}</option>
						</c:forEach>
					</select>
					<c:choose>
						<c:when test="${not empty labelPrinter}">
							<a href="#" onclick="event.preventDefault(); test_Alligator_PrintNonDataBasedLabel(this,'birt.label_stdonsite', '${labelPrinter.path}');">
								<img src="img/icons/printer.png" width="16" height="16" class="img_marg_bot" />
							</a>
						</c:when>
						<c:otherwise>
							<a href="#" onclick=" event.preventDefault(); alert('${textNoLabelPrinter}');" ><img src="img/icons/info-red.png" width="16" height="16" class="img_marg_bot" title="${textNoLabelPrinter}"></a>
						</c:otherwise>
					</c:choose>
					<a href="#" class="mainlink" onclick="event.preventDefault(); testDownloadNonDataBasedLabel(this,'birt.label_stdonsite');">
						<img src="img/icons/pdf.png" width="16" height="16" class="img_marg_bot" />
					</a>
				</li>
				<li>
					<label style="width: 300px;"><spring:message code="tools.restrictedcalibrationlabels" /></label>
					<select>
						<c:forEach var="i" items="${labelCounts}">
							<option value="${i}">${i}</option>
						</c:forEach>
					</select>
					<c:choose>
						<c:when test="${not empty labelPrinter}">
							<a href="#" onclick="event.preventDefault(); test_Alligator_PrintNonDataBasedLabel(this,'birt.label_restricted', '${labelPrinter.path}');">
								<img src="img/icons/printer.png" width="16" height="16" class="img_marg_bot" />
							</a>
						</c:when>
						<c:otherwise>
							<a href="#" onclick=" event.preventDefault(); alert('${textNoLabelPrinter}');" ><img src="img/icons/info-red.png" width="16" height="16" class="img_marg_bot" title="${textNoLabelPrinter}"></a>
						</c:otherwise>
					</c:choose>
					<a href="#" class="mainlink" onclick="event.preventDefault(); testDownloadNonDataBasedLabel(this,'birt.label_restricted');">
						<img src="img/icons/pdf.png" width="16" height="16" class="img_marg_bot" />
					</a>
				</li>
				<li>
					<label style="width: 300px;"><spring:message code="tools.nocertificateukascalibrationlabels" /></label>
					<select>
						<c:forEach var="i" items="${labelCounts}">
							<option value="${i}">${i}</option>
						</c:forEach>
					</select>
					<c:choose>
						<c:when test="${not empty labelPrinter}">
							<a href="#" onclick="event.preventDefault(); test_Alligator_PrintNonDataBasedLabel(this,'birt.label_ukasnocert', '${labelPrinter.path}');">
								<img src="img/icons/printer.png" width="16" height="16" class="img_marg_bot" />
							</a>
						</c:when>
						<c:otherwise>
							<a href="#" onclick=" event.preventDefault(); alert('${textNoLabelPrinter}');" ><img src="img/icons/info-red.png" width="16" height="16" class="img_marg_bot" title="${textNoLabelPrinter}"></a>
						</c:otherwise>
					</c:choose>
					<a href="#" class="mainlink" onclick="event.preventDefault();  testDownloadNonDataBasedLabel(this,'birt.label_ukasnocert');">
						<img src="img/icons/pdf.png" width="16" height="16" class="img_marg_bot" />
					</a>
				</li>
				<li>
					<label style="width: 300px;"><spring:message code="tools.donotuseforcalibrationlabels" /></label>
					<select>
						<c:forEach var="i" items="${labelCounts}">
							<option value="${i}">${i}</option>
						</c:forEach>
					</select>
					<c:choose>
						<c:when test="${not empty labelPrinter}">
							<a href="#" onclick="event.preventDefault(); test_Alligator_PrintNonDataBasedLabel(this,'birt.label_donotuse', '${labelPrinter.path}');">
								<img src="img/icons/printer.png" width="16" height="16" class="img_marg_bot" />
							</a>
						</c:when>
						<c:otherwise>
							<a href="#" onclick=" event.preventDefault(); alert('${textNoLabelPrinter}');" ><img src="img/icons/info-red.png" width="16" height="16" class="img_marg_bot" title="${textNoLabelPrinter}"></a>
						</c:otherwise>
					</c:choose>
					<a href="#" class="mainlink" onclick="event.preventDefault(); testDownloadNonDataBasedLabel(this,'birt.label_donotuse');">
						<img src="img/icons/pdf.png" width="16" height="16" class="img_marg_bot" />
					</a>
				</li>
				<li>
					<label style="width: 300px;"><spring:message code="tools.hireplantstdcalibrationlabels" /></label>
					<spring:message code="date" />: <input type="text" name="hiredate" id="hiredate" />
					<spring:message code="tools.certno" />: <input type="text" name="hirecertno" id="hirecertno" />
					<c:choose>
						<c:when test="${not empty labelPrinter}">
							<a href="#" onclick="event.preventDefault(); test_Alligator_PrintStringDateLabel(this,'birt.label_stdhire', $j('#hirecertno').val(), $j('#hiredate').val(), '${labelPrinter.path}');">
								<img src="img/icons/printer.png" width="16" height="16" class="img_marg_bot" />
							</a>
						</c:when>
						<c:otherwise>
							<a href="#" onclick=" event.preventDefault(); alert('${textNoLabelPrinter}');" ><img src="img/icons/info-red.png" width="16" height="16" class="img_marg_bot" title="${textNoLabelPrinter}"></a>
						</c:otherwise>
					</c:choose>
					<a href="#" class="mainlink" onclick="event.preventDefault(); testDownloadStringDateLabel(this,'birt.label_stdhire', $j('#hirecertno').val(), $j('#hiredate').val());">
						<img src="img/icons/pdf.png" width="16" height="16" class="img_marg_bot" />
					</a>
				</li>
				<li>
					<label style="width: 300px;"><spring:message code="tools.electricalsafetytestlabels" /></label>
					<spring:message code="date" />: <input type="text" name="patdate" id="patdate" />
					<spring:message code="tools.serial" />: <input type="text" name="patserial" id="patserial" />
					<spring:message code="tools.barcodeopt" />: <input type="text" name="patplantid" id="patplantid" />
					<c:choose>
						<c:when test="${not empty labelPrinter}">
							<a href="#" onclick="event.preventDefault(); test_Alligator_PrintInstDateLabel(this, 'birt.label_pattest',$j('#patplantid').val(),$j('#patserial').val(), $j('#patdate').val(), '${labelPrinter.path}');">
								<img src="img/icons/printer.png" width="16" height="16" class="img_marg_bot" />
							</a>
						</c:when>
						<c:otherwise>
							<a href="#" onclick=" event.preventDefault(); alert('${textNoLabelPrinter}');" ><img src="img/icons/info-red.png" width="16" height="16" class="img_marg_bot" title="${textNoLabelPrinter}"></a>
						</c:otherwise>
					</c:choose>
					<a href="#" class="mainlink" onclick="event.preventDefault(); testDownloadPATLabel(this, 'birt.label_pattest',$j('#patplantid').val(),$j('#patserial').val(), $j('#patdate').val());">
						<img src="img/icons/pdf.png" width="16" height="16" class="img_marg_bot" />
					</a>
					<!--<input type="submit" name="PrintBtn" value="Print" onclick="print_label(); return true;" id="PrintBtn" />-->
				</li>
				<%-- <li class="hid">
					<label style="width: 300px;"><spring:message code="tools.nationalgridassetlabels")</label>
					<spring:message code="barcode"): <input type="text" name="ngplantid" id="ngplantid" />
					<input type="button" id="zeb.label_ngasset" onclick="testPrintInstDateLabel(this, $j('#ngplantid').val(), null, ''); return false;" value="<spring:message code='print')" />
				</li> --%>
			</ul>
		</fieldset>
	</jsp:body>
</t:crocodileTemplate>