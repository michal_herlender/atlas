<%-- File name: /trescal/core/quotation/quotationrequest.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address" %>
<%@ taglib prefix="company" tagdir="/WEB-INF/tags/company" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
<html>

<head>
	<link rel="stylesheet" href="styles/global.css" type="text/css" title="antech" />
	
	<link rel="stylesheet" href="styles/print/print.css" type="text/css" media="print" />
	
	<script type='text/javascript'>
		// set variable to default context of project
		 var springUrl = "${req.contextPath}";
		// get value of loadtab parameter from querystring
		// used to show the correct tab when subnavigation bar is used
		 var LOADTAB = '${request.getParameter("loadtab")}';
		// retrieves application session maximum inactive interval
		 var sessionTimeout = ${pageContext.session.maxInactiveInterval} / 60;
		// set variable to i18n of javascript
		 var i18locale = '${rc.locale.language}';
	</script>
	
	<!-- makes all spring generated html elements xhtml compliant -->
	<c:set var="springXhtmlCompliant" value="true"/>
	
	<script type='text/javascript' src='script/trescal/core/quotation/QuotationRequest.js'></script>
	
	<script type='text/javascript' src='script/thirdparty/jQuery/jquery.js'></script>	
	<script type='text/javascript' src='script/trescal/core/template/Cwms_Main.js'></script>
</head>

<body onload=" loadScript.init(true); " class="iframed" style=" width: 560px; min-width: 560px; ">

	<form:form action="" method="post" class="editquotationrequest">
		<form:errors path="*">
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<h5 class="center-0 attention"><spring:message code="contact" /><spring:message code="quotrequest.oneormoreerrorsoccuredsavingthequotationrequest" /></h5>
						<div class="center attention">${fn:join(messages.toArray(),"<br/>")}</div>
					</div>
				</div>
			</div>
		</form:errors>
			<fieldset>
				<ol>
					<li>
						<label>&nbsp;</label>
						<span>
							<a href="#" onclick=" event.preventDefault(); document.getElementById('request.detailsSource').value= 'EXISTING'; $j('#existingcompany').removeClass().addClass('vis'); $j('#newcompany').removeClass().addClass('hid'); " class="mainlink"><spring:message code="quotrequest.existingcompanycontact" /></a>
							&nbsp;&nbsp;|&nbsp;&nbsp;
							<a href="#" onclick=" event.preventDefault(); document.getElementById('request.detailsSource').value= 'FREEHAND'; $j('#existingcompany').removeClass().addClass('hid'); $j('#newcompany').removeClass().addClass('vis'); " class="mainlink"><spring:message code="quotrequest.enterfreehandcompanycontact" /></a>		
						</span>
					</li>
					<li>
						<form:label path="request.quoteType"><spring:message code="quotrequest.quotetype" /></form:label>
						<form:select path="request.quoteType" >
							<form:options items="${quotereqtypes}" itemLabel="displayName"/>
						</form:select>
						<form:errors  path="request.quoteType"><span class="error"><img src="../img/web/asterisk.png" width="16" height="16" /></span></form:errors>
					</li>
				</ol>
				<ol id="existingcompany" 
				<c:choose>
					<c:when test="${command.request.detailsSource == 'FREEHAND'}"> class="hid" </c:when>
					<c:otherwise> class="vis" </c:otherwise>
				</c:choose>>
					<li>
						<label><spring:message code="quotrequest.companycontact" />:</label>
						<div id="cascadeSearchPlugin">
							<input type="hidden" id="cascadeRules" value="subdiv,contact" />
							<input type="hidden" id="compCoroles" value="client,prospect,business" />
							<input type="hidden" id="customHeight" value="340" />
							<c:if test="${not empty command.request.comp.coid}">
								<input type="hidden" id="prefillIds" value="${command.request.comp.coid},${command.request.con.sub.subdivid},${command.request.con.personid}" />
							</c:if>
						</div>
						<!-- clear floats and restore page flow -->
						<div class="clear"></div>
					</li>
				</ol>
				<ol id="newcompany" 
				<c:choose>
				<c:when test="${command.request.detailsSource == 'EXISTING'}">  class="hid" </c:when><c:otherwise>class="vis" </c:otherwise>
				</c:choose>>
					<li>
						<form:label path="request.company"><spring:message code="company" /></form:label>
						<form:input path="request.company" type="text"/>
						<form:errors  path="request.company" cssClass="error"></form:errors>
					</li>
					<li>
						<form:label path="request.contact"><spring:message code="contact" /></form:label>
						<form:input path="request.contact" type="text"/>
						<form:errors  path="request.contact" cssClass="error"></form:errors>
					</li>
			
					<li>
						<form:label path="request.phone"><spring:message code="quotrequest.phone" /></form:label>
						<form:input path="request.phone" type="tel"/>
						<form:errors  path="request.phone" cssClass="error"/>
					</li>
					<li>
						<form:label path="request.fax"><spring:message code="quotrequest.fax" /></form:label>
						<form:input path="request.fax" type="tel"/>
						<form:errors  path="request.fax" cssClass="error"/>
					</li>
					<li>
						<form:label path="request.email"><spring:message code="quotrequest.email" /></form:label>
						<form:input path="request.email" type="email"/>
						<form:errors  path="request.email" cssClass="error"/>
					</li>
					<li>
						<form:label path="request.addr1"><spring:message code="address" /></form:label>
						<form:input path="request.addr1" type="text"/>
						<form:errors  path="request.addr1" cssClass="error"/>
					</li>
					<li>
						<form:label path="request.addr2"><spring:message code="address" /></form:label>
						<form:input path="request.addr2" type="text"/>
						<form:errors  path="request.addr2" cssClass="error"/>
					</li>
					<li>
						<form:label path="request.town"><spring:message code="town" /></form:label>
						<form:input path="request.town" type="text"/>
						<form:errors  path="request.town" cssClass="error"/>
					</li>
					<li>
						<form:label path="request.county"><spring:message code="county" /></form:label>
						<form:input path="request.county" type="text"/>
						<form:errors  path="request.county" cssClass="error"/>
					</li>
					<li>
						<form:label path="request.postcode"><spring:message code="postcode" /></form:label>
						<form:input path="request.postcode" type="text"/>
						<form:errors  path="request.postcode" cssClass="error"/>
					</li>
					<li>
						<form:label path="request.country"><spring:message code="country" /></form:label>
						<form:input path="request.country" type="text"/>
						<form:errors  path="request.country" cssClass="error"/>
					</li>
				</ol>
			
				<ol>				
					<li>
						<form:label path="request.requestInfo"><spring:message code="quotrequest.requestinformation" /></form:label>
						<form:textarea path="request.requestInfo" rows="5" cols="70"/>
						<form:errors  path="request.requestInfo" cssClass="error"/>
					</li>
					<li>
						<form:label path="request.reqdate"><spring:message code="requestdate" /></form:label>
						<form:input path="request.reqdate" type="date"/>
						<form:errors  path="request.reqdate" cssClass="error" />
					</li>
					<li>
						<form:label path="request.duedate"><spring:message code="quotrequest.duedates" /></form:label>
						<form:input path="request.duedate" type="date"/>
						<form:errors  path="request.duedate" cssClass="error" />
					</li>
					<li>
						<form:label path="request.clientref"><spring:message code="clientref" /></form:label>
						<form:input path="request.clientref" type="text"/>
						<form:errors  path="request.clientref" cssClass="error"/>
					</li>
					<li>
						<form:label path="request.clientOrderNo"><spring:message code="quotrequest.clientorderno" /></form:label>
						<form:input path="request.clientOrderNo" type="text"/>
						<form:errors  path="request.clientOrderNo" cssClass="error"/>
					</li>					
					<li>
						<form:hidden path="request.detailsSource"/>
						<label>&nbsp;</label>
						<input type="submit" name="submit" value="<spring:message code="save" />"/>
					</li>
				</ol>
					
			</fieldset>
		
		</form:form>
		
	</body>

</html>				


