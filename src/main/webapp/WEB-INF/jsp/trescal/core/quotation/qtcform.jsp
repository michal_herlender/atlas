<%-- File name: /trescal/core/_PATH1_/_PATH2_/_FILE_.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="quotation" tagdir="/WEB-INF/tags/quotation"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
        <span class="headtext"><spring:message
				code="qtcform.editquotationtermsandconditions" /></span>
    </jsp:attribute>
	<jsp:attribute name="scriptPart">
        <script type='text/javascript'
			src='script/trescal/core/quotation/QtcForm.js'></script>
    </jsp:attribute>
	<jsp:body>
        <div class="infobox">

	<div id="subnav">
		<dl>
			<dt>
						<a href="#"
							onclick="switchMenuFocus(menuElements, 'systemconditions-tab', false); return false;"
							id="systemconditions-link"
							<c:choose>
				<c:when test="${qtcform.quotation.usingDefaultTermsAndConditions == true}">  class="selected" </c:when>
				<c:otherwise> 
				</c:otherwise>
			</c:choose>
							title="<spring:message code="qtcform.viewsystemtermsandconditions" />">
			<spring:message code="qtcform.systemconditions" />
						</a>
					</dt>
			
			<dt>
						<a href="#"
							onclick="switchMenuFocus(menuElements, 'customconditions-tab', false); return false;"
							id="customconditions-link"
							<c:choose>
				<c:when test="${qtcform.quotation.usingDefaultTermsAndConditions == true}">  class="selected" </c:when>  
				<c:otherwise> class="selected" </c:otherwise>
			</c:choose>
							title="<spring:message code="qtcform.viewcustomtermsandconditions" />">
			<spring:message code="qtcform.customconditions" />
						</a>
					</dt>
		</dl>
	</div>

	<form:form method="post" modelAttribute="qtcform">
		<fieldset id="systemconditions-tab"
					<c:choose>
				<c:when test="${qtcform.quotation.usingDefaultTermsAndConditions == true}"> class="vis" </c:when>
				<c:otherwise> class="hid" </c:otherwise>
			</c:choose>>
			<legend>
				<spring:message code="qtcform.editquotationtermsandconditions" />
				<a href="viewquotation.htm?id=${qtcform.quotation.id}"
							class="mainlink">
					<c:out value="${qtcform.quotation.qno} " /> 
					<spring:message code="quotaddinstmod.version" />
					<c:out value=" ${qtcform.quotation.ver}" />
				</a>
			</legend>

			<div class="center bold">
							<form:radiobutton  path="usingDefaultTermsAndConditions"
								 value="true"
								 /> 
			<spring:message code="qtcform.usesystemdefaulttermsandconditions" />
						</div>
			<div id="defaultgeneral">
				<c:out value="${general.conditionText}"  escapeXml="false"/>
			</div>
			
			<c:if test="${qtcform.conditions.calCon.size() > 0}">
			   <div>
				   <c:forEach var="con" items="${qtcform.conditions.calCon}">
						<c:if test="${not empty con.conditionText}">
							<h5 class="text-left">
										<t:showTranslationOrDefault
											translations="${con.caltype.serviceType.longnameTranslation}"
											defaultLocale="${defaultlocale}" /> Terms &amp; 
							<spring:message code="qtcform.conditions" />
									</h5>
							<span><c:out value="${con.conditionText}" escapeXml="false"/></span>
						</c:if>
				   </c:forEach>
			   </div>
			</c:if>									
															
		</fieldset>
		
		<fieldset id="customconditions-tab"
					<c:choose>
			<c:when test="${qtcform.quotation.usingDefaultTermsAndConditions == true}"> class="hid" </c:when> 
			<c:otherwise> class="vis" </c:otherwise>
		</c:choose>>
		
			<legend>
				<spring:message code="qtcform.editquotationtermsandconditions" />
				<a href="viewquotation.htm?id=${qtcform.quotation.id}"
							class="mainlink">
					<c:out value="${qtcform.quotation.qno} " /> 
					<spring:message code="quotaddinstmod.version" />
					<c:out value=" ${qtcform.quotation.ver}" />
				</a>
			</legend>							
	
			<div class="center bold">
							<form:radiobutton path="usingDefaultTermsAndConditions" value="false" />
			<spring:message code="qtcform.usecustomtermsandconditions" />
						</div>
								
			<ol id="custom">
			
				<li id="customgeneral">
					<form:label path="generalConditions"><spring:message
									code="qtcform.generaltermsandconditions" /></form:label>
					
					<div class="float-left editor_box">													
						<form:textarea cssClass="editor" cssStyle="height: 300px; width: 800px;" path="generalConditions"
										 rows="8" />
					</div>
					<!-- clear floats and restore page flow -->
					<div class="clear-0"></div>
					<form:errors cssClass="errorabsolute"
								path="generalConditions" showFieldErrors="false" />
				</li>
				
<!-- 				#* -->
<!-- 				 Iterate over the calibration types. If there is a custom calibration -->
<!-- 				 term/condition for the caltype then display that, otherwise -->
<!-- 				 display the current system default term or condition. -->
<!-- 				*# -->
				<c:forEach var="calCondition" items="${qtcform.calConditions}" varStatus="vs">
					<li> 
	 						<label><t:showTranslationOrDefault
										translations='${calCondition.serviceType.longnameTranslation}'
										defaultLocale='${defaultlocale}' /><br /></label>

							<form:input type="hidden" path="calConditions[${vs.index}].calTypeId"
										 />
							<form:errors cssClass="errorabsolute"
										path="calConditions[${vs.index}].calTypeId" showFieldErrors="false" /> 
						
							<div class="float-left editor_box">
							<form:textarea class="editor" style="height: 300px; width: 800px;"
											path="calConditions[${vs.index}].condition"
											rows="8"/>
							</div>
						
							<!-- clear floats and restore page flow -->
							<div class="clear-0"></div>
							<form:errors cssClass="errorabsolute"
									path="calConditions[${vs.index}].condition" showFieldErrors="false" /> 					
							</li>
					
				</c:forEach>
				
			</ol>
	
		</fieldset>
		
		<div class="center">
					<input name="submit" type="submit"
						value="<spring:message code="qtcform.saveconditions" />" />
				</div>
	</form:form>
		
</div>
</jsp:body>
</t:crocodileTemplate>