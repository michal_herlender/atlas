<%-- File name: /trescal/core/quotation/scheduledquotationrequest.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="quotation" tagdir="/WEB-INF/tags/quotation"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="createquot.createquotation"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/quotation/scheduledquotationrequest.js'></script>
	</jsp:attribute>
	<jsp:body>
		<t:showErrors path="form.*" showFieldErrors="true" />

		<c:set var="outstandingClass" value=""/>
		<c:set var="outstandingVisibility" value="hid"/>
		<c:set var="newClass" value="selected"/>
		<c:set var="newVisibility" value="vis"/>
		<form:errors path="form.*">
			<c:set var="outstandingClass" value="selected"/>
			<c:set var="outstandingVisibility" value="vis"/>
			<c:set var="newClass" value=""/>
			<c:set var="newVisibility" value="hid"/>
		<div class="warningBox1">
			<div class="warningBox2">
				<div class="warningBox3">
					<h5 class="center-0 attention">
						<spring:message code="error.save"/>
					</h5>
				</div>
			</div>
		</div>
	</form:errors>
		
		<!-- div to hold all the tabbed submenu -->
		<div id="tabmenu">
		
			<!-- tabbed submenu to change between subdivisions and accreditations -->
			<ul class="subnavtab">
				<li>
					<a href="#" id="outstanding-link" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'outstanding-tab', false);" class="${outstandingClass}">
						<spring:message code="schedquotrequ.pendingcreation"/> (<span id="outstandingcount">${outstanding.size()}</span>)
					</a>
				</li>
				<li>
					<a href="#" id="new-link" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'new-tab', false); " class="${newClass}">
						<spring:message code="schedquotrequ.createnewrequest"/>
					</a>
				</li>
				<li>
					<a href="#" id="old-link" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'old-tab', false); ">
						<spring:message code="schedquotrequ.previousrequests"/>
					</a>
				</li>
			</ul>
			
			<!-- div to hold all content which is to be changed using the tabbed submenu -->
			<div class="tab-box">
			
				<div id="outstanding-tab" class="${outstandingVisibility}">															
					<table id="outstandinglist" class="default4">
						<thead>
							<tr>
								<th><spring:message code="schedquotrequ.requestby"/></th>
								<th><spring:message code="schedquotrequ.requeston"/></th>
								<th><spring:message code="schedquotrequ.requestfor"/></th>
								<th><spring:message code="schedquotrequ.requesttype"/></th>
								<th><spring:message code="parameters"/></th>
								<th><spring:message code="schedquotrequ.requestscope"/></th>
								<th><spring:message code="delete"/></th>
							</tr>
						</thead>
						<tbody id="outstandinglistbody">
							<c:choose>
								<c:when test="${empty outstanding}">
								<tr id="nooutstandingrequests">
									<td colspan="7">
										<spring:message code="schedquotrequ.nooutstandingrequests"/>
									</td>
								</tr>
								</c:when>
								<c:otherwise>
								<c:forEach var="sreq" items="${outstanding}">
									<tr id="scheduledrequest${sreq.id}">
										<td align="left">${sreq.requestBy.name}</td>
										<td align="left"><fmt:formatDate type="both" dateStyle="SHORT" timeStyle="SHORT" value="${sreq.requestOn}" /></td>
										<td align="left">${sreq.quoteContact.name} [${sreq.quoteContact.sub.comp.coname}]</td>
										<td align="left">
											${sreq.requestType}
										</td>
										<td align="left">
											<quotation:showScheduledDateRange dateRange="${sreq.dateRange}" />
										</td>
										<td align="left">
											${sreq.scope}
										</td>
										<td align="left">
											<a href="<c:url value='deletescheduledquoterequest.htm?id=${sreq.id}' />"><spring:message code="delete"/></a>
										</td>
									</tr>	
								</c:forEach>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
				</div>
				
				<div id="new-tab" class="${newVisibility}">		
					<form:form modelAttribute="form">
						<fieldset>
							<ol>
								<li>
									<label><spring:message code="contact"/>:</label>
									<div id="cascadeSearchPlugin">
										<input type="hidden" id="cascadeRules" value="subdiv,contact" />
										<input type="hidden" id="compCoroles" value="client,prospect,business" />
										<c:if test="${not empty form.contactid}">
											<input type="hidden" id="prefillIds" value="${form.coid},${form.subdivid},${form.contactid}," />
										</c:if>
									</div>
									<div class="clear"></div>
								</li>
								
								<li>
									<spring:message code="company.language"/>:
									<div><form:select path="languageTag" items="${supportedLocales}" itemLabel="value" itemValue="key" /></div>
								</li>
								
								<li>
									<spring:message code="schedquotrequ.addinstrumentsbelongingto"/>:
									<div><form:radiobutton path="scope" value="CONTACT" checked="checked"/> <spring:message code="schedquotrequ.justthiscontact"/></div>
									<div><form:radiobutton path="scope" value="SUBDIV"/> <spring:message code="schedquotrequ.thiscontactsdivision"/></div>
									<div><form:radiobutton path="scope" value="COMPANY" /> <spring:message code="schedquotrequ.thiscontactscompany"/></div>
								</li>
								
								<li>
									<spring:message code="schedquotrequ.instrumentstoadd"/>:
									<div>
										<form:radiobutton path="type" value="ALL_INSTRUMENTS" checked="checked" onclick=" toggleDateVisibility(false);" />
										<spring:message code="schedquotrequ.all"/>
									</div>
									<div>
										<form:radiobutton path="type" value="ALL_OUT_OF_CAL_INSTRUMENTS" onclick=" toggleDateVisibility(false);" />
										<spring:message code="schedquotrequ.justinstrumentsoutofcalibration"/>
									</div>
									<div>
										<form:radiobutton path="type" value="DATE_RANGE_INSTRUMENTS" onclick=" toggleDateVisibility(true); " />
										<spring:message code="schedquotrequ.instrumentdaterange"/>
									</div>
								</li>
								
								<c:choose>
									<c:when test="${form.type == 'DATE_RANGE_INSTRUMENTS'}">
										<c:set var="dateVisibility" value="vis"/>
									</c:when>
									<c:otherwise>
										<c:set var="dateVisibility" value="hid"/>
									</c:otherwise>
								</c:choose>
								
								<li id="daterange" class="${dateVisibility}">
									<spring:message code="schedule.daterange"/>:
									<div>
										<form:input type="date" path="startDate" />
										<spring:message code="to"/>
										<form:input type="date" path="finishDate" />
									</div>
									<div>
										<form:radiobutton path="includeEmptyDates" value="false" />
										<spring:message code="schedquotrequ.excludeblankdates"/>
									</div>
									<div>
										<form:radiobutton path="includeEmptyDates" value="true" />
										<spring:message code="schedquotrequ.includeblankdates"/>
									</div>
								</li>
								
								<li>
									<div><spring:message code="schedquotrequ.quotationitemstrategy"/>:</div>
									<c:forEach var="strategy" items="${quotegenerationstrategies}">
										<div>
											<c:set var="checkedText" value=""/>
											<c:if test="${form.strategy == strategy}">
												<c:set var="checkedText" value="checked='checked'"/>
											</c:if>
											<input type="radio" name="strategy" value="${strategy}" ${checkedText}/>
											<spring:message code="quotation.generation.strategy.${strategy}" arguments="${strategy}"/>
										</div>
									</c:forEach>
								</li>
								
								<li>
									<div><spring:message code="schedquotrequ.defaultcalibrationtypewherenoinstrumenthistoryexists"/></div>
									<c:forEach var="caltype" items="${allcaltypes}">
										<div>
											<c:set var="checkedText" value=""/>
											<c:if test="${caltype.calTypeId == form.calTypeId}">
												<c:set var="checkedText" value="checked='checked'"/>
											</c:if>
											<input type="radio" name="caltypeid" value="${caltype.calTypeId}" ${checkedText}/>
											<cwms:besttranslation translations="${caltype.serviceType.shortnameTranslation}"/> - 
											<cwms:besttranslation translations="${caltype.serviceType.longnameTranslation}"/>
										</div>
									</c:forEach>
								</li>
								
								<li>
									<label>&nbsp;</label>
									<input type="submit" value="<spring:message code='submit' />" name="submit" id="submit" />
								</li>
								
							</ol>
						
						</fieldset>
					</form:form>										
				</div>
				
				<div id="old-tab" class="hid">															
					
					<table id="oldlist" class="default4">
						<thead>
							<tr>
								<th><spring:message code="schedquotrequ.requestby"/></th>
								<th><spring:message code="schedquotrequ.requeston"/></th>
								<th><spring:message code="schedquotrequ.requestfor"/></th>
								<th><spring:message code="schedquotrequ.requesttype"/></th>
								<th><spring:message code="parameters"/></th>
								<th><spring:message code="schedquotrequ.requestscope"/></th>
								<th><spring:message code="schedquotrequ.dateprocessed"/></th>
								<th><spring:message code="quotation"/></th>
							</tr>
						</thead>
						<tbody id="oldllst">
							<c:choose>
								<c:when test="${empty mostrecent}">
									<tr>
										<td colspan="8">
											<spring:message code="schedquotrequ.nooldrequests"/>
										</td>
									</tr>
								</c:when>
								<c:otherwise>
									<c:forEach var="sreq" items="${mostrecent}">
										<tr id="scheduledrequest${sreq.id}">
											<td align="left">${sreq.requestBy.name}</td>
											<td align="left"><fmt:formatDate type="date" dateStyle="SHORT" value="${sreq.requestOn}"/></td>
											<td align="left">${sreq.quoteContact.name} [${sreq.quoteContact.sub.comp.coname}]</td>
											<td align="left">
												${sreq.requestType}
											</td>
											<td align="left">
												<quotation:showScheduledDateRange dateRange="${sreq.dateRange}" />
											</td>
											<td align="left">${sreq.scope}</td>
											<td align="left"><fmt:formatDate type="date" dateStyle="SHORT" value="${sreq.processedOn}"/></td>
											<td align="left"><a href='<spring:url value="viewquotation.htm?id=${sreq.quotation.id}"/>'>${sreq.quotation.qno}</a></td>
										</tr>	
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
					
				</div>
				
			</div>
		</div>
		<!-- end of tabmenu div -->		
	</jsp:body>
</t:crocodileTemplate>