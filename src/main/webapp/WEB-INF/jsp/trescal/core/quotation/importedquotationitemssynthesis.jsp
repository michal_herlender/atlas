<%-- File name: /trescal/core/jobs/job/jobsearch.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="link" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="importquotationItems.title" /></span>
		<style>
			.max-wrap {
				max-width : 120px;
				display : block;
			}
			.disabled {
                backround-color:grey; 
                color:darkgrey; 
                cursor: not-allowed;
   				pointer-events: none;
        }
		</style>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/quotation/ImportedQuotationItemsSynthesis.js'></script>
    </jsp:attribute>
<jsp:body>
	
<form:form modelAttribute="form" id="importedQuotationItemsForm" >
	<form:hidden path="coid" value="${ quotation.contact.sub.comp.coid}"/>
	<div class="infobox">
		<fieldset>
		<legend>
				<a href="viewquotation.htm?id=${quotation.id}" class="mainlink-float">
					<spring:message code="quotation" />
					<c:out value=" ${quotation.qno} " />
					<spring:message code="quotaddinstmod.version" />
					<c:out value=" ${quotation.ver}" />
				<form:hidden path="quotationId" />
				</a>
			</legend>
			<!-- displays content in left column -->
			<div class="displaycolumn">
				<ol>										
					<li>
						<label><spring:message code="company" />:</label>
						<link:companyLinkDWRInfo company="${ company }" rowcount="1" copy="true"/>
					</li>
					<li>
						<label><spring:message code="subdiv" />:</label>
						<link:subdivLinkDWRInfo rowcount="1" subdiv="${ subdiv }"/>
						<form:hidden path="subdivId" />
					</li>
					<li>
						<label><spring:message code="requestdate" />:</label>
							<span><fmt:formatDate value="${quotation.reqdate}" dateStyle="medium" type="date"/></span>
					</li>
				</ol>
			</div>
			<div class="displaycolumn">
				<ol>	
				    <li>
						<label><spring:message code="contact" />:</label>
						<link:contactLinkDWRInfo contact="${ contact }" rowcount="0" />
						<form:hidden path="contactId" />
					</li>
					<c:if test="${not empty quotation.duedate}">
								<li>
									<label><spring:message code="quotrequest.duedates" />:</label>
									<span>
										<fmt:formatDate type="date" dateStyle="SHORT"
											value="${quotation.duedate}" />
									</span>
								</li>
					</c:if>
					<form:hidden path="addressId" />
				</ol>
			</div>
		</fieldset>
	</div>
	
	<div class="infobox">
	    		<table class="default2">										
			<thead>
				<tr>
					<td colspan="10"><spring:message code="finalsynthesis.table.nonidentitle"/> (${ form.nonIdentifiedItems.size() }) </td>
				</tr>
				<tr>
					<th><spring:message code="trescalid" /></th>
					<th><spring:message code="plantno" /></th>
					<th><spring:message code="servicetype" /></th>
					<th><spring:message code="exchangeformat.fieldname.catalogprice" /></th>
					<th><spring:message code="exchangeformat.fieldname.discountrate" /></th>
					<th><spring:message code="exchangeformat.fieldname.finalprice" /></th>
					<th><spring:message code="exchangeformat.fieldname.publicnote" /></th>
					<th><spring:message code="exchangeformat.fieldname.privatenote" /></th>
				</tr>
				
			</thead>
			<tbody>
				<c:forEach items="${ form.nonIdentifiedItems }" var="inst" varStatus="status">
					<tr>						
						<td>
							${ inst.plantId }
							
						</td>
						<form:hidden path="nonIdentifiedItems[${ status.index }].plantId"/>
						
						<td>
							${ inst.plantNo }
							<div style="color:darkorange;">
								<c:forEach var="warning" items="${ inst.errorMessages }" >
									<c:out value="${ warning }"/> <br>
								</c:forEach>
							   <form:hidden path="nonIdentifiedItems[${ status.index }].errorMessages"/>
							</div>
							<span class="attention"><form:errors path="nonIdentifiedItems[${ status.index }].plantNo" /></span>
						</td>
						<form:hidden path="nonIdentifiedItems[${ status.index }].plantNo"/>
						
						<td>
							${ inst.serviceType }
						</td>
						<form:hidden path="nonIdentifiedItems[${ status.index }].serviceType"/>
						<form:hidden path="nonIdentifiedItems[${ status.index }].serviceTypeId"/>
						
						<td>
							${ inst.catalogPrice }
						</td>
						<form:hidden path="nonIdentifiedItems[${ status.index }].catalogPrice"/>
						
						<td>
							${ inst.discountRate }
						</td>
						<form:hidden path="nonIdentifiedItems[${ status.index }].discountRate"/>
						
						<td>
							${ inst.finalPrice }
						</td>
						<form:hidden path="nonIdentifiedItems[${ status.index }].finalPrice"/>

						<td>
							${ inst.publicNote }
						</td>
						<form:hidden path="nonIdentifiedItems[${ status.index }].publicNote"/>
						
						<td>
							${ inst.privateNote }
						</td>
						<form:hidden path="nonIdentifiedItems[${ status.index }].privateNote"/>
	
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<table class="default2">										
			<thead>
				<tr>
					<td colspan="10"><spring:message code="finalsynthesis.table.identitle"/> (${ form.identifiedItems.size() }) </td>
				</tr>
				<tr>
					<th><spring:message code="trescalid" /></th>
					<th><spring:message code="plantno" /></th>
					<th><spring:message code="servicetype" /></th>
					<th><spring:message code="exchangeformat.fieldname.catalogprice" /></th>
					<th><spring:message code="exchangeformat.fieldname.discountrate" /></th>
					<th><spring:message code="exchangeformat.fieldname.finalprice" /></th>
					<th><spring:message code="exchangeformat.fieldname.publicnote" /></th>
					<th><spring:message code="exchangeformat.fieldname.privatenote" /></th>
				</tr>
				
			</thead>
			<tbody>
				<c:forEach items="${ form.identifiedItems }" var="inst" varStatus="status">
					<tr>						
						<td>
							${ inst.plantId }
						</td>
						<form:hidden path="identifiedItems[${ status.index }].plantId"/>
						
						<td>
							${ inst.plantNo }
							<div style="color:darkorange;">
							</div>
							<span class="attention"><form:errors path="identifiedItems[${ status.index }].plantNo" /></span>
						</td>
						<form:hidden path="identifiedItems[${ status.index }].plantNo"/>
						
						<td>
							${ inst.serviceType }
						</td>
						<form:hidden path="identifiedItems[${ status.index }].serviceType"/>
						<form:hidden path="identifiedItems[${ status.index }].serviceTypeId"/>
						
						<td>
							${ inst.catalogPrice }
						</td>
						<form:hidden path="identifiedItems[${ status.index }].catalogPrice"/>
						
						<td>
							${ inst.discountRate }
						</td>
						<form:hidden path="identifiedItems[${ status.index }].discountRate"/>
						
						<td>
							${ inst.finalPrice }
						</td>
						<form:hidden path="identifiedItems[${ status.index }].finalPrice"/>

						<td>
							${ inst.publicNote }
						</td>
						<form:hidden path="identifiedItems[${ status.index }].publicNote"/>
						
						<td>
							${ inst.privateNote }
						</td>
						<form:hidden path="identifiedItems[${ status.index }].privateNote"/>
	
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<c:set var="disable" value="disabled"></c:set>
		<c:if test="${form.identifiedItemsExist}">
				<c:set var="disable" value=""></c:set>
		</c:if>
		<center>
			<input type="submit" name="newanalysis" class="button"
									value="<spring:message code='filesynthesis.buttonnewanalysis'/>" />
			<input type="submit" name="save" class="button" value="<spring:message code='submit' />" ${disable}
			onclick="disableMeAfterFirstClick(this)"/>
		</center>
		<form:hidden path="exchangeFormatId"/>
	</div>
</form:form>
</jsp:body>
</t:crocodileTemplate>
