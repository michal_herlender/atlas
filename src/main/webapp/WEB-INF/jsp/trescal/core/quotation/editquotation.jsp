<%-- File name: /trescal/core/quotation/viewquotation.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="costs" tagdir="/WEB-INF/tags/costs" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>
<%@ taglib prefix="quotation" tagdir="/WEB-INF/tags/quotation" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="viewquot.viewquotation"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/quotation/EditQuotation.js'></script>
	</jsp:attribute>
	<jsp:body>
		<%-- Note, errors were bound but not displayed in VM file, not showing field errors for now. --%>
		<t:showErrors path="quotationform.*" showFieldErrors="true"/>
		<form:form modelAttribute="quotationform" id="viewquotationeditform" action="" method="post">
		<fieldset>
			<legend><spring:message code="edit"/></legend>
			<ol>
				<li>
					<label><spring:message code="viewquot.quotationcompany"/>:</label>
					<span><links:companyLinkDWRInfo company="${quotationform.quotation.contact.sub.comp}" rowcount="10000" copy="false"/></span>
				</li>
				<li>
					<label><spring:message code="viewquot.quotationsubdivision"/>:</label>
					<span>${quotationform.quotation.contact.sub.subname}</span>
				</li>										
				<li>
					<label><spring:message code="viewquot.quotationcontact"/>:</label>
					<span>
						<links:contactLinkDWRInfo contact="${quotationform.quotation.contact}" rowcount="10000" />
						(<a href="#" class="mainlink limitAccess <c:if test='${quotationform.quotation.quotestatus.issued}'> notvis </c:if>" onclick=" event.preventDefault(); createCascadingSearchPlugin('${quotationform.quotation.contact.sub.comp.coid}', '${quotationform.quotation.contact.sub.subdivid}', '${quotationform.quotation.contact.personid}'); ">Change</a>)
					</span>
				</li>											
				<li id="contactSelector" class="hid">
					<label><spring:message code="viewquot.contactselector"/>:</label>
					<!-- these hidden input fields hold the values of the current company, subdivision, contact, address
						 ids when the user is editing the instrument but has not edited these specific fields.  When the
						 user decides to edit any of the fields above these hidden inputs are removed and new ones are added
						 when the cascading search is created -->
						<form:input type="hidden" id="tempId" path="personid" value="${quotationform.quotation.contact.personid}" />
						&nbsp;<form:errors path="personid" class="attention" />
				</li>
			</ol>
			<div class="displaycolumn">
				<ol>												
					<li>
						<label for="duration"><spring:message code="newquotevers.duration"/>:</label>
						<span>
							<form:input path="quotation.duration"/>
							<span class="attention"><form:errors path="quotation.duration" /></span>
						</span>
					</li>
					<li>
						<label for="clientref"><spring:message code="newquotevers.clientref"/>:</label>
						<span>
							<form:input path="quotation.clientref" type="text" id="clientref" value="${quotationform.quotation.clientref}" />
							<span class="attention"><form:errors path="quotation.clientref" /></span>
						</span>
					</li>
					<li>
						<label for="statusid"><spring:message code="status"/>:</label>
						<span>
							<!-- Was broken (causing hiberate exception) as it was trying to set entity id via quotationform.quotation.quotestatus.statusid - GB 2016-04-07 -->
								<form:select path="statusid" id="statusid" onchange="showClientAcceptanceDateInput($j('#statusid option:selected').data('accepted')); return false;">
									<c:forEach var="qstatus" items="${quoteStatusList}">
										<option value="${qstatus.statusid}" ${status.value == qstatus.statusid ? "selected" : ""}  ${qstatus.accepted?' data-accepted="yes"':' data-accepted="no"'}><cwms:besttranslation translations="${qstatus.nametranslations}"/></option>
									</c:forEach>
								</form:select>
								<span class="attention"><form:errors path="statusid" /></span>
						</span>
					</li>
					<li id="clientacceptedon" class="${ not empty quotationform.clientAcceptanceOn ? 'show':'hid' }">
						<label><spring:message code="viewquot.clientacceptedon" javaScriptEscape="true" />: </label>
						<span><form:input path="clientAcceptanceOn" type="date"/></span>
					</li>
					<li>
						<label for="reqdate"><spring:message code="requestdate"/>:</label>
						<span>
								<form:input path="quotation.reqdate" type="date" id="reqDate" value="${quotation.reqdate}" autocomplete="off" />
								<span class="attention"><form:errors path="quotation.reqdate" /></span>

						</span>
					</li>
					<li>
						<label for="duedate"><spring:message code="quotrequest.duedates"/>:</label>
						<span>
								<form:input type="date" path="quotation.duedate" id="duedate" value="${quotation.duedate}" autocomplete="off" />
								<span class="attention"><form:errors path="quotation.duedate" /></span>
						</span>
					</li>	
					<li>
						<label for="sourceAddressid"><spring:message code="company.addresssource"/>:</label>
						<span>
								<form:select path="sourceAddressid" id="sourceAddressid">
									<option value="${quotationform.quotation.sourceAddress.addrid}" selected>${quotationform.quotation.sourceAddress.addr1},
										${quotationform.quotation.sourceAddress.addr2}</option>
									<c:forEach var="qAdresses" items="${quoteAdresseList}">
										<c:if test="${qAdresses.addrid != quotationform.quotation.sourceAddress.addrid}">
											<option value="${qAdresses.addrid}">${qAdresses.addr1}, ${qAdresses.addr2}</option>
										</c:if>
									</c:forEach>
								</form:select>
								<span class="attention"><form:errors path="sourceAddressid" /></span>
						</span>
					</li>
					
				</ol>
			
			</div>
			
			<div class="displaycolumn">
			
				<ol>
					
					<li>
						<label for="sourcedByPersonid"><spring:message code="createquot.sourcedby"/>:</label>
						<form:select path="sourcedByPersonid" items="${businessContactList}" itemLabel="value" itemValue="key" />
						<span class="attention"><form:errors path="sourcedByPersonid" /></span>
					</li>
					<li>
						<label for="currencyCode"><spring:message code="currency"/>:</label>
						<span>
							
							<%-- if company has a default rate, try and work out if that's been set on the quote or not --%>
						
							<c:set var="quote" value="${quotationform.quotation}"/>
								<form:select path="currencyCode" id="currencyCode">
								<c:forEach var="curopt" items="${currencyopts}">
									<option value="${curopt.optionValue}" ${curopt.currency.currencyCode == quote.currency.currencyCode && curopt.rate == quote.rate ? "selected" : ""} >
									${curopt.currency.currencyCode}
									@ ${curopt.defaultCurrencySymbol}1 = ${curopt.currency.currencySymbol}${curopt.rate}
									<c:choose>
										<c:when test="${curopt.companyDefault}">
											[Company Default Rate]
										</c:when>
										<c:when test="${curopt.systemDefault}">
											[System Default Rate]
										</c:when>
									</c:choose>
									</option>
								</c:forEach>
							</form:select>
							<span class="attention"><form:errors path="currencyCode" /></span>
						</span>
					</li>
					<li>
						<label><spring:message code="company.vatrate"/>:</label>
						<form:input id="vatrate" path="quotation.vatRate"/>
						<c:out value=" %"/>
					</li>
					<li>
						<label class="bold"><spring:message code="viewquot.quotetotaldisplay"/>:</label>
						<div class="float-left padtop">
								<form:checkbox path="quotation.showDiscounts" class="checkbox" /><spring:message code="viewquot.showdiscounts" /><br />
 						</div>
						<!-- clear floats and restore page flow -->
						<div class="clear"></div>
					</li>
					<li>
						<label for="contractStartDate"><spring:message code="viewquot.contractstartdate"/> :</label>
						<span>
								<form:input type="date" path="quotation.contractStartDate" id="contractStartDate" value="${quotationform.quotation.contractStartDate}" autocomplete="off" />
								<span class="attention"><form:errors path="quotation.contractStartDate" /></span>
						</span>
					</li>
					<li>
						<label for="contractInvoiceThreshold"><spring:message code="viewquot.contractmininvoice"/> :</label>
						<span>
							<form:input path="quotation.contractInvoiceThreshold"/>
							<span class="attention"><form:errors path="quotation.contractInvoiceThreshold" /></span>
						</span>
					</li>
					
				</ol>
			
			</div>
			
			<div class="clear"></div>
			
			<ol>
			
				<li>
					<div class="center">
						<input type="submit" value="<spring:message code='save'/>" id="submit" name="submit" />
					</div>
				</li>
				
			</ol>
			
		</fieldset>
		
		</form:form>		
	</jsp:body>
</t:crocodileTemplate>
