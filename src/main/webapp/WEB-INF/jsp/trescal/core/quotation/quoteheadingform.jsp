<%-- File name: /trescal/core\quotation/quoteheadingform.jsp --%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="quotation" tagdir="/WEB-INF/tags/quotation"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
        <span class="headtext"><spring:message code="quotheadform.quotationheadings" /></span>
    </jsp:attribute>
	<jsp:attribute name="scriptPart">
        <script type='text/javascript'
			src='script/trescal/core/quotation/QuoteHeadingForm.js'></script>

<!-- InstanceParam name="onload" type="text" value="anyError(); return false;" -->
<!-- InstanceParam name="runinit" type="text" value="true" -->
		<script type='text/javascript'
			src='script/thirdparty/jQuery/jquery.js'></script>
		<script type='text/javascript'
			src='script/trescal/core/template/Cwms_Main.js'></script>
		<style>
.radioOptionInput {
	float: left;
}

#radioOptionInputDiv span {
	display: flex;
}

.radioOptionInput+label {
	width: 100%;
}
</style> 
    </jsp:attribute>
	<jsp:body>
        <!-- error section displayed when form submission fails -->
		<form:errors path="quoteheadingform.*">
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<h5 class="center-0 attention"><spring:message code="quotheadform.oneormoreerrorsoccuredcreatingyournewquoteheading"/></h5>
					</div>
				</div>
			</div>
		</form:errors>
<!-- end error section -->	

<div id="subnav">
	<dl>
		<dt>
					<a href="#"
						onclick="switchMenuFocus(menuElements, 'currentheadingsarea', false); return false;"
						id="currentheadingslink"
						title="<spring:message code='quotheadform.viewcurrentheadingsonquote'/>"
						class="selected"><spring:message
							code="quotheadform.currentquoteheadings" /></a>
				</dt>
		<dt>
					<a href="#"
						onclick="switchMenuFocus(menuElements, 'newheadingarea', true); return false;"
						id="newheadinglink"
						title="<spring:message code='quotheadform.createnewquoteheading'/>"><spring:message
							code="quotheadform.newquoteheading" /></a>
				</dt>
	</dl>
</div>					

<form action="" method="post" id="quoteheadingform">
						
	<!-- begin the current headings area -->
	<div id="currentheadingsarea">
			<form:errors path="quoteheadingform.*">
				<div class="warningBox1">
					<div class="warningBox2">
						<div class="warningBox3">
							<h5 class="center-0 attention">
								<spring:message code="error.save"/>
							</h5>
						</div>
					</div>
				</div>
			</form:errors>
		<form:hidden id="formfunction" path="quoteheadingform.formfunction" />
		
		<!-- infobox with nifty corners applied for sorting headings -->
		<div class="infobox">
	
			<fieldset>
			
				<legend>
					<spring:message code="quotheadform.sortheadings" />
				</legend>
			
				<ol>										
					<c:if test="${not empty prevSortTypeQH}">
						<li>
							<label><spring:message code="quotsort.previoussorttype" />:</label>
							<span>${prevSortTypeQH.displayName}</span>
						</li>
					</c:if>												
					<li>
						<label><spring:message code="quotsort.sortby"/>:</label>
						<form:select path="quoteheadingform.sortType" items="${sorttypes}" itemLabel="displayName" /> 
						<c:if test="${quoteheadingform.sortType.name() == 'CUSTOM'}">
							<span class="attention"><spring:message code="quotsort.notechangeslost" /></span>
						</c:if>
					</li>
					<li>
						<label>&nbsp;</label>
						<input type="submit" name="submit"
								value="<spring:message code="update"/>"
								onclick="$j('#formfunction').attr('value', 'sort'); " />
					</li>
				</ol>
			
			</fieldset>	
		
		</div>
		<!-- end of heading sort section -->
		
		<!-- main div to hold all headings -->
		<div class="results">
			
			<!-- title to show amount of headings -->
			<h5>
				<c:choose>
					<c:when test="${quoteheadingform.quotation.quoteheadings.size() == 1}">
						<spring:message code="viewquot.thereis" />&nbsp;<c:out
									value="${quoteheadingform.quotation.quoteheadings.size()}" />&nbsp;
						<spring:message code="quotaddinstmod.heading" />
					</c:when>
					<c:otherwise>
						<spring:message code="viewquot.thereare" />&nbsp;<c:out
									value="${quoteheadingform.quotation.quoteheadings.size()}" />&nbsp;
						<spring:message code="viewquot.headings" />
					</c:otherwise>
				</c:choose>
				&nbsp;<spring:message code="quotheadform.forquotation" />
				<a class="mainlink"
							href="<spring:url value='viewquotation.htm?id=${quoteheadingform.quotation.id}'/>"
							<spring:message code="quotheadform.viewthisquote"/>>
					${quoteheadingform.quotation.qno} version ${quoteheadingform.quotation.ver}
				</a>
			</h5>
			
			<!-- begin heading list -->
			<ul id="headinglist">
			
				<!--  loop through each heading assigned to this quotation   -->
				<c:set var="count" value="0" />
				<c:forEach var="heading" items="${quoteheadingform.quotation.quoteheadings}">
					
					<!-- start of heading list item -->
					<li id="header${heading.headingId}">
							
							<!-- this div is floated left and displays heading name -->
							<div class="headingtext">
								<div>
									${heading.headingNo}.&nbsp;
									<a class="mainlink"
											href="<spring:url value='editquoteheadingform.htm?headingId=${heading.headingId}&amp;submit=false'/>"
											title="<spring:message code='quotheadform.editthisheading'/>">
										${heading.headingName}
									</a>
									<c:out value=" - ${heading.quoteitems.size()} "/>
									<c:choose>
										<c:when test="${heading.quoteitems.size() == 1}">
											<spring:message code="item" />
										</c:when>
										<c:otherwise>
											<spring:message code="items" />
										</c:otherwise>
									</c:choose>
									<c:if test="${heading.systemDefault}">
										[<spring:message
												code="quotheadform.defaultheadingforthisquotation" />]
									</c:if>	
								</div>
							</div>
							<!-- end float left div -->
							
							<!-- this div is floated right and displays the view items, edit and delete links -->
							<div class="headingoptions">
								<div>
									<a href="#" class="mainlink" id="viewitems"
											onclick=" $j('#itemlist${heading.headingId}').slideToggle(1000); return false; "
											title="<spring:message code='quotheadform.viewitemsunderthisheading'/>"><spring:message
												code="quotheadform.viewitems" /></a>
									|
									<a class="mainlink"
											href="<spring:url value='editquoteheadingform.htm?headingId=${heading.headingId}&amp;submit=false'/>"><spring:message
												code="edit" /></a> 
									|
									<a class="mainlink"
											href="<spring:url value='deletequoteheading.htm?headingId=${heading.headingId}'/>"><spring:message
												code="delete" /></a>
								</div>
							</div>
							<!--  end of float right  div -->
							
							<!--  clear floats and restore page flow  -->
							<div class="clear"></div>
							
							<!-- begin items attached to heading list div -->
							<div id="itemlist${heading.headingId}" style="display: none;">
							
								<div class="headingitems">
								
									<c:choose>
										<c:when test="${heading.quoteitems.size() > 0}">	
																			
										<c:set var="caltype" value="0" />											
										<c:set var="qinotecount" value="0" />
										<c:set var="mnotecount" value="0" />	
																				
									<c:forEach var="quoteitem" items="${heading.quoteitems}">
											<c:if test="${quoteitem.partOfBaseUnit != true}"> 
													<c:if test="${caltype != quoteitem.caltype.calTypeId}">
														<c:set var="rowcount" value="1" />
																																													
														<table class="default4 quoteheadingitems"
																summary="This table contains quote items that have been sorted">
															
															<thead>
																<tr>
																	<td colspan="5">
																		<t:showTranslationOrDefault
																				translations="${quoteitem.caltype.serviceType.longnameTranslation}"
																				defaultLocale="${defaultlocale}" />
																		&nbsp;<spring:message code="items" />
																	</td>
																</tr>
																<tr>
																	<th class="qid" scope="col"><spring:message
																				code="quotsort.id" /></th>
																	<th class="qitemno" scope="col"><spring:message
																				code="itemno" /></th>
																	<th class="qimodel" scope="col"><spring:message
																				code="editquothead.instrumentmodel" /></th>
																	<th class="qiplantno" scope="col"><spring:message
																				code="editquot.referenceno" /></th>
																	<th class="qicaltype" scope="col"><spring:message
																				code="caltype" /></th>  																															
																</tr>
															</thead>
															<tfoot>
																<tr>
																	<td colspan="5">&nbsp;</td>
																</tr>
															</tfoot>
																												
															<tbody>
	
															<c:set var="itemno" value="1" />
													</c:if>
												<c:set var="caltype" value="${quoteitem.caltype.calTypeId}" />
												<c:set var="qinotecount"
																		value="${quoteitem.publicActiveNoteCount + quoteitem.privateActiveNoteCount}" />
													<tr
																		style=" background-color: ${quoteitem.caltype.serviceType.displayColour}; ">
														<td class="qid">${quoteitem.id}</td>
														<td class="qitemno">${quoteitem.itemno}</td>
														<td class="qimodel">
															<c:choose>
																<c:when test="${not empty quoteitem.inst}">
																	<quotation:showInstrument instrument="${quoteitem.inst}" />					
																</c:when>
																<c:otherwise>
																	<cwms:showmodel instrumentmodel="${quoteitem.model}" />
																</c:otherwise>	
															</c:choose>
														</td>
														<td class="qiplantno">${quoteitem.referenceNo}</td>
														<td class="qicaltype"><t:showTranslationOrDefault
																				translations="${quoteitem.caltype.serviceType.shortnameTranslation}"
																				defaultLocale="${defaultlocale}" /></td>
													</tr>
													
													<c:set var="thisBaseUnit" value="false" />
													<c:if test="${quoteitem.modules.size() > 0}">
													
														<tbody>
															
															<tr style="display: none;">
																<td colspan="5"></td>
															</tr>
														
															<c:forEach var="module" items="${quoteitem.modules}">
																<c:set var="thisBaseUnit" value="true" />
																<c:set var="mnotecount"
																					value="${module.publicActiveNoteCount + module.privateActiveNoteCount}" />												
																
																<tr
																					style=" background-color: ${module.caltype.serviceType.displayColour}; ">
																	<td class="qid">${module.id}</td>
																	<td class="qitemno"><img
																						src="img/icons/arrow_merge.png" height="16"
																						width="16"
																						alt="Part of base unit - <cwms:showmodel instrumentmodel="${quoteitem.model}"/> "
																						title="<spring:message code="editquothead.partofbaseunit"/> - <cwms:showmodel instrumentmodel="${quoteitem.model}"/> " /></td>
																	<td class="qimodel">
																		<c:choose>
																			<c:when test="${not empty module.inst}">
																				<quotation:showInstrument
																									instrument="${module.inst}" />
																			</c:when>
																			<c:otherwise>
																				<cwms:showmodel instrumentmodel="${module.model}" /> 
																			</c:otherwise>
																		</c:choose>
																	</td>
																	<td class="qiplantno">
																		<c:choose>
																			<c:when test="${not empty module.inst}">
																				<c:choose>
																					<c:when test="${module.inst.plantno != null && module.inst.plantno != ''}">
																						(<span class="bold">P</span>) ${module.inst.plantno}
																					</c:when>
																					<c:otherwise>
																						(<span class="bold">S</span>) ${module.inst.serialno}
																					</c:otherwise>
																				</c:choose>
																			</c:when>
																			<c:otherwise>
																				<c:if
																									test="${module.plantno != null && module.plantno != ''}">
																					(<span class="bold">P</span>) ${module.plantno}
																				</c:if>	
																			</c:otherwise>
																		</c:choose>
																	</td>
																	<td class="qicaltype"><t:showTranslationOrDefault
																							translations="${module.caltype.serviceType.shortnameTranslation}"
																							defaultLocale="${defaultlocale}" /></td>
																</tr>																	
															</c:forEach>
														
													
																	
																	</c:if>																
													
													</tbody>
													
													<c:set var="rowcount" value="${rowcount + 1}" />														
											</c:if>	
											
									</c:forEach>
												
										</table>
																	
										
											
											</c:when>
										<c:otherwise>
									
										<table class="default4"
													summary="<spring:message code='editquothead.tablenoquotation'/>">
											<tbody>
												<tr class="nobord">												
													<td class="totalsum text-center bold"><spring:message
																	code='editquothead.noitemsquoteheading' /></td>
												</tr>
											</tbody>
										</table>
									
										</c:otherwise>
									</c:choose>									
								</div>
								
							</div>
							<!-- end items attached to heading list div -->
															
					</li>
					<!-- end of heading list item -->
					
				</c:forEach>
				
				<c:if test="${quoteheadingform.quotation.headingSortType == 'CUSTOM'}">
					<c:set var="optionList" value="" />
					<c:set var="optionCount" value="0" />
					<c:forEach var="qh" items="${quoteheadingform.quotation.quoteheadings}">
						<c:if test="${not qh.systemDefault}">										
							<c:set var="optionList">${optionList}<option value='${qh.headingId}'>${qh.headingNo}</option></c:set>
							<c:set var="optionCount" value="${optionCount + 1}" />
						</c:if>
					</c:forEach>
					<c:if test="${optionCount > 1}">
						<li class="padding">
							<div class="headingtext">
								<div>
									<spring:message code="quotheadform.moveheading" />
									<select style="width: 40px;">
										${optionList}
									</select>
									<spring:message code="quotheadform.toheading" />
									<select style="width: 40px;">
										${optionList}"
									</select>
									<input type="button"
												value="<spring:message code='quotheadform.moveheading'/>"
												class="moveQHButton"
												onclick=" moveQuoteHeading($j(this).prev().prev().val(), $j(this).prev().val(), ${quoteheadingform.quotation.id}); return false; " />
								</div>
							</div>													
						</li>
					</c:if>
				</c:if>
					
			</ul>
			<!-- end heading list -->
		
			<!-- this blank div clears floats, restores page flow and applies border to last list item -->
			<div class="clear_border">&nbsp;</div>
											
		</div>
		<!-- end of div to hold all headings -->
	
	</div>
	<!-- end the current headings area -->
	
	<!-- begin the new heading area -->
	<div id="newheadingarea" class="hid">
	
		<div class="infobox">
		
			<fieldset>
			
				<legend>
							<spring:message code="quotheadform.createnewheadingfor" />&nbsp;<c:out value="${quoteheadingform.quotation.qno}"/>&nbsp;<spring:message
								code="quotaddinstmod.version" />&nbsp;<c:out value="${quoteheadingform.quotation.ver}"/></legend>
				
				<p>
							<spring:message
								code="quotheadform.enteranewheadinganddescriptionbelow" />:</p>
				
				<ol>
				
					<li>
						<label path="quoteheadingform.header.headingName"><spring:message
										code="quotaddinstmod.heading" />:</label>
						<form:input path="quoteheadingform.header.headingName" tabindex="" />
						<form:errors path="quoteheadingform.header.headingName" cssClass="error" />
					</li>
					<li>
						<label path="quoteheadingform.header.headingDescription"><spring:message
										code="description" />:</label>
						<form:textarea path="quoteheadingform.header.headingDescription"
									id="disable" rows="4" cols="50" />
						<form:errors path="quoteheadingform.header.headingDescription" cssClass="error"/>
					</li>
					<li>
						<label path="quoteheadingform.itemInsertOption" id="itemoptions"><spring:message
										code="quotheadform.options" />:</label>
								<div id="radioOptionInputDiv">
									<form:radiobuttons path="quoteheadingform.itemInsertOption"
										items="${newHeaderItemOptions}" cssClass="radioOptionInput" />
								</div>
  
						<form:errors path="quoteheadingform.itemInsertOption" cssClass="error"/>
					</li>
					<li>
						<label path="quoteheadingform.quoteDefault"><spring:message
										code="quotheadform.quotationdefault" />:</label>
						<form:checkbox value="true" path="quoteheadingform.quoteDefault"
										 />
					</li>
					<li>
						<label for="submit">&nbsp;</label>
						<input type="submit" name="submit" id="submit"
								value="<spring:message code='quotheadform.saveheading'/>"
								onclick="$j('#formfunction').attr('value', 'newheading'); " />
					</li>
					
				</ol>	
											
			</fieldset>
			
		</div>
		
	</div>
	<!-- end the new heading area -->

</form>
    </jsp:body>
</t:crocodileTemplate>