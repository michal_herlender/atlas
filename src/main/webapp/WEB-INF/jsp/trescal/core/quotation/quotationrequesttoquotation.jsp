<%-- File name: /trescal/core/quotation/quotationrequesttoquotation.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="quotation" tagdir="/WEB-INF/tags/quotation" %>
<%@ taglib prefix="company" tagdir="/WEB-INF/tags/company" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<spring:message code="quotrequesttoquot.title" /></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/quotation/QuotationRequestToQuotation.js'></script>
	</jsp:attribute>
	<jsp:body>
		<form:errors path="command.*">
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<h5 class="center-0 attention"><spring:message code="quotrequest.oneormoreerrorsoccuredsavingthequotationrequest"/>:</h5>
						<c:forEach var="e" items="${messages}">
							<div class="center attention"><c:out value="${e}"/></div>
						</c:forEach>
					</div>
				</div>
			</div>
		</form:errors>

		<div class="infobox">

			<form:form method="post" action="" modelAttribute="command">

				<fieldset class="quoteReqToQuote">

					<legend>
						<spring:message code="quotrequesttoquot.confirmcompanycontactdetails" />
					</legend>

					<ol>
						<c:if test="${command.quotationRequest.detailsSource  == 'FREEHAND'}">
							<li>
								<strong>
									<spring:message code="quotrequesttoquot.freehanddetails" /></strong>
							</li>
							<li>
								<label>
									<spring:message code="quotrequesttoquot.quickaddtosystem" />:</label>
								<button id="convert" type="button">
									<spring:message code="company.addfreehandcontacttosystem" /></button>
							</li>
							<li>
								<label>
									<spring:message code="company" />:</label>
								<span>${command.quotationRequest.company}</span>
								<c:if test="${not empty command.quotationRequest.company }">
									<img src="img/icons/bullet_clipboard.png" width="10" height="10"
										onclick=" $j(this).prev().clipBoard(this, null, false, null); "
										alt="Copy company to clipboard" title="Copy company to clipboard" />
								</c:if>
							</li>
							<li>
								<label>
									<spring:message code="contact" />:</label>
								<span>${command.quotationRequest.contact}</span>
								<c:if test="${not empty command.quotationRequest.contact }">
									<img src="img/icons/bullet_clipboard.png" width="10" height="10"
										onclick=" $j(this).prev().clipBoard(this, null, false, null); "
										alt="Copy contact to clipboard" title="Copy contact to clipboard" />
								</c:if>
							</li>
							<li>
								<label>
									<spring:message code="quotrequest.phone" />:</label>
								<span>${command.quotationRequest.phone}</span>
								<c:if test="${not empty command.quotationRequest.phone }">
									<img src="img/icons/bullet_clipboard.png" width="10" height="10"
										onclick=" $j(this).prev().clipBoard(this, null, false, null); "
										alt="Copy telephone number to clipboard"
										title="Copy telephone number to clipboard" />
								</c:if>
							</li>
							<li>
								<label>
									<spring:message code="quotrequest.fax" />:</label>
								<span>${command.quotationRequest.fax}</span>
								<c:if test="${not empty command.quotationRequest.fax }">
									<img src="img/icons/bullet_clipboard.png" width="10" height="10"
										onclick=" $j(this).prev().clipBoard(this, null, false, null); "
										alt="Copy fax number to clipboard" title="Copy fax number to clipboard" />
								</c:if>
							</li>
							<li>
								<label>
									<spring:message code="quotrequest.email" />:</label>
								<span>${command.quotationRequest.email}</span>
								<c:if test="${not empty command.quotationRequest.email }">
									<img src="img/icons/bullet_clipboard.png" width="10" height="10"
										onclick=" $j(this).prev().clipBoard(this, null, false, null); "
										alt="Copy email to clipboard" title="Copy email to clipboard" />
								</c:if>
							</li>
							<li>
								<label>
									<spring:message code="address" />:</label>
								<div class="float-left padtop width70">
									${command.quotationRequest.addr1}<br />
									<c:if test="${not empty command.quotationRequest.addr2 }">
										${command.quotationRequest.addr2}<br />
									</c:if>
									${command.quotationRequest.town}<br />
									<c:if test="${not empty command.quotationRequest.county  }">
										${command.quotationRequest.county}<br />
									</c:if>
									${command.quotationRequest.postcode}<br />
									${command.quotationRequest.country}
								</div>
								<div class="clear"></div>
							</li>

						</c:if>

						<li>
							<span><strong>
									<spring:message code="quotrequesttoquot.confirmcontact" /></strong></span>
						</li>
						<li>
							<label class="contact">
								<spring:message code="quotrequest.companycontact" />:</label>
							<div id="cascadeSearchPlugin">
								<input type="hidden" id="cascadeRules" value="subdiv,contact" />
								<input type="hidden" id="compCoroles" value="client,prospect,business" />
								<c:if test="${not empty command.coid }">
									<input type="hidden" id="prefillIds" value="${command.coid},${command.subdivid},${command.personid}," />
								</c:if>
							</div>
							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
							${status.errorMessage}
						</li>

						<c:if test="${not empty command.quotationRequest.requestInfo }">
							<li>
								<label>
									<spring:message code="quotrequesttoquot.requestdetails" />:</label>
								<div class="float-left padtop width70">
									${command.quotationRequest.requestInfo}
								</div>
								<!-- clear floats and restore page flow -->
								<div class="clear"></div>
							</li>
							<li>
								<form:label path="addRequestInfoAsNote">
									<spring:message code="quotrequesttoquot.addrequestdetailsasanote" />
								</form:label>
								<form:checkbox path="addRequestInfoAsNote" />
							</li>
						</c:if>

						<li>
							<label>&nbsp;</label>
							<input type="submit" name="submit"
								value="<spring:message code='quotrequesttoquot.converttoquotation'/>" />
						</li>

					</ol>

				</fieldset>

			</form:form>

		</div>

		<company:freehandcontactconversionutility quotationRequest="${command.quotationRequest}"
			businessAreas="${businessAreas}" coroles="${coroles}" countries="${countries}"
			transportOPtions="${transportOptions}" />

	</jsp:body>
</t:crocodileTemplate>