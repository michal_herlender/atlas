<%-- File name: /trescal/core/quotation/importquotationitemsbatch.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="quotation" tagdir="/WEB-INF/tags/quotation"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="link" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="importquotationItems.title" />Batch</span>
		<style>
			.max-wrap {
				max-width : 120px;
				display : block;
			}
			.labelStyle {
				font-weight: bold;
			}
			
			#ef, #autoSubmitPolicy{
			    width: 350px;
			}
			
		</style>
		<script>
			if ( window.history.replaceState ) {
  				window.history.replaceState( null, null, window.location.href );
			}
		</script>
	</jsp:attribute>
	<jsp:body>
		<t:showErrors path="importQuotationItemsBatch.*" showFieldErrors="false" />		
		<c:if test="${errors.size() > 0 }">
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3 center-0">
						<spring:message code="viewquot.attention" />
						<c:forEach var="errorMessage" items="${errors}">
							<div class="center attention">${errorMessage}</div>
						</c:forEach>
					</div>
				</div>
			</div>
		</c:if>
		
		<c:if test="${importQuotationItemsBatch.bgTaskId != null }">
			<div class="successBox1">
				<div class="successBox2">
					<div class="successBox3 center-0">
						<spring:message code="calibrationimportation.job.submitted.successfuly" />
							&nbsp;<a style="float: none;padding: 0px;" class="mainlink-float" target="_blank" 
							href='bgtasks.htm?personid=${personid}&jobname=QUOTATION_IMPORT_JOB'>${ importQuotationItemsBatch.bgTaskId }</a>	
					</div>
				</div>
			</div>
		</c:if>
      
      	<form:form modelAttribute="importQuotationItemsBatch" method="post" enctype="multipart/form-data">
      		<div class="infobox">
			<fieldset>
				<legend>
					<a href="viewquotation.htm?id=${quotation.id}" class="mainlink-float">
					<spring:message code="quotation" />
					<c:out value=" ${quotation.qno} " />
					<spring:message code="quotaddinstmod.version" />
					<c:out value=" ${quotation.ver}" />
					<form:hidden path="quotationId" />
					</a>
				</legend>
			<!-- displays content in left column -->
				<div class="displaycolumn">
					<ol>										
						<li>
							<label><spring:message code="company" />:</label>
							<link:companyLinkDWRInfo company="${quotation.contact.sub.comp}" rowcount="1" copy="true"/>
						</li>
						<li>
							<label><spring:message code="subdiv" />:</label>
							<link:subdivLinkDWRInfo rowcount="1" subdiv="${ quotation.contact.sub }"/>
							<form:hidden path="subdivid" />
						</li>
						<li>
							<label><spring:message code="requestdate" />:</label>
							<span><fmt:formatDate value="${quotation.reqdate}" dateStyle="medium" type="date"/></span>
						</li>
					</ol>
				</div>
				<div class="displaycolumn">
					<ol>	
				    	<li>
							<label><spring:message code="contact" />:</label>
							<link:contactLinkDWRInfo contact="${ quotation.contact }" rowcount="0" />
							<form:hidden path="personid" />
						</li>
						<li>
							<label><spring:message code="viewquot.sourcedbyregdate" />:</label>
							<span>${quotation.sourcedBy.name} (<fmt:formatDate value="${quotation.regdate}" dateStyle="medium" type="date"/>)</span>
						</li>
						<li>
							<label><spring:message code="status" />:</label>
							<span><cwms:besttranslation
									translations="${quotation.quotestatus.nametranslations}" /></span>
						</li>
					</ol>
				</div>
		</fieldset>
	</div>
	<div class="infobox">
		<form:hidden path="quotationId" value="${ quotation.id }"/>
		<form:hidden path="coid" value="${ quotation.contact.sub.comp.coid }"/>
		<table align="center">
			<tr>
				<td><label class="labelStyle">Instrument Options</td>
				<td>
					: <form:radiobutton path="priceAndServiceTypeFromFile" value="true" checked="checked"/>
					   	<spring:message code="batch.price.service.type.from.file"/>
					   	
					  <form:radiobutton path="priceAndServiceTypeFromFile" value="false"/>
					  	<spring:message code="batch.price.service.type.from.latest.job.item"/>
					  	
				</td>
			</tr>
			<tr>
				<td><label class="labelStyle"><spring:message code="addjob.recordtype.file.exchangeformat"/></label></td>
				<td>
					: <form:select path="exchangeFormatId" id="ef">
						<c:forEach var="ef" items="${efImportQuotationItems}">
							<option value="${ef.id}">${ef.name}</option>
						</c:forEach>
					</form:select>
				</td>
			</tr>
			<tr>
				<td>
					<label class="labelStyle"><spring:message code="importcalibrations.autosubmitpolicy" />*</label>	
				</td>
				<td>
					:
						<form:select path="autoSubmitPolicy" id="autoSubmitPolicy" type="text">
							<option value=""></option>
							<c:forEach var="i" items="${autoSubmitPolicies}">
								<option value="${i.key}">${i.value}</option>
							</c:forEach>
						</form:select>
				</td>
			</tr>
			<tr>
				<td><label class="labelStyle"><spring:message code="addjob.recordtype.file.filetoupload"/></label></td>
				<td colspan="2">
					: <form:input path="file" type="file" id="uploadFile"  enctype="multipart/form-data" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
					<span class="attention"></span>
					<div class="clear"></div>
				</td>
			</tr><br>
			<tr>
				<td></td>
				<td colspan="2">&nbsp;&nbsp;<input class="labelStyle center" type="submit" id="submit" value="<spring:message code="submit" />" tabindex="8" /></td>
			</tr>
		</table>
	</div>
</form:form>
</jsp:body>
</t:crocodileTemplate>