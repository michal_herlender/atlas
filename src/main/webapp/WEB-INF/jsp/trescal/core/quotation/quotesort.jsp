<%-- File name: /trescal/core/quotation/quotesort.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="quotation" tagdir="/WEB-INF/tags/quotation" %>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="link" tagdir="/WEB-INF/tags/links" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="quotsort.sortingquotation" />&nbsp;<c:out value="${command.quotation.qno}" /></span>
        
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/quotation/QuoteSort.js'></script>
    </jsp:attribute>
     <jsp:body>
	<form:form action="" method="post">
	
		<div class="infobox">
		
			<fieldset>
			
				<legend><spring:message code="quotsort.sortingquoteitemson" /> (<link:showQuotationLink quotation="${command.quotation}" /></legend>
				
				<ol>
					
					<c:set var="prevSortType" value="${session.getAttribute({command.quotation.id}+'_prevSortTypeQuotation')}"	/>
					<c:if test="${not empty prevSortType}">
						<li>
							<label><spring:message code="quotsort.previoussorttype" />:</label>
							<span>${prevSortType.displayName}</span>
						</li>
					</c:if>
					<li>
						<label path="sortType"><spring:message code="quotsort.sortby" />:</label>
						<form:select path="sortType" items="${sorttypes}" itemLabel="displayName"/>
						<c:if test="${command.quotation.sortType == 'CUSTOM'}">
							<span class="attention"><spring:message code="quotsort.notechangeslost" /></span>
						</c:if>
					</li>
					<li>
						<label>&nbsp;</label>
						<input type="submit" name="submit" value="<spring:message code="update" />"/>
					</li>
					
				</ol>
			
			</fieldset>								
			
			<c:set var="rowcount" value="0" />
			<c:forEach var="heading" items="${command.quotation.quoteheadings}">
			<c:choose>
				<c:when test="${command.quotation.quoteheadings.size() < 2 && heading.quoteitems.size() < 1} " >
				
					<table class="default4" summary="<spring:message code="editquothead.tablenoquotation" />">
						<tbody>
							<tr class="nobord">												
								<td class="text-center bold"><spring:message code="quotsort.noitemshavebeenaddedtothisquotation" /></td>
							</tr>
						</tbody>
					</table>
				
				</c:when>
				<c:otherwise> 
					<h5 class="center">
						<spring:message code="quotaddinstmod.heading" /> - ${heading.headingName}
					</h5>
					
					<c:choose>
						<c:when test="${not empty heading.quoteitems}" >
																						
							<c:set var="caltype" value="0"/>											
							<c:set var="qinotecount" value="0"/>
							<c:set var="mnotecount" value="0" />
																																								
							<table class="default4 sortingquotationitems" summary="<spring:message code='quotsort.tableitemssort'/>">
							<tbody>
							<c:forEach var="quoteitem" items="${heading.quoteitems}">
									
								<c:if test='${not quoteitem.partOfBaseUnit}'>
										
									<c:if test="${caltype != quoteitem.caltype.calTypeId}">
										<%-- Display cal type header on first cal type, and on cal type change --%>
										<c:set var="rowcount" value="1" />
										
										<c:if test="${caltype != 0}">
											<tr>
												<th colspan="5">&nbsp;</th>
											</tr>
										</c:if>
	
										<tr>
											<th colspan="5"><t:showTranslationOrDefault translations="${quoteitem.caltype.serviceType.longnameTranslation}" defaultLocale="${defaultlocale}"/>&nbsp;<span><spring:message code="items" /></span></th>
										</tr>
										<tr>
											<th class="qid" scope="col"><spring:message code="quotsort.id" /></th>
											<th class="qitemno" scope="col"><spring:message code="itemno" /></th>
											<th class="qimodel" scope="col"><spring:message code="editquothead.instrumentmodel" /></th>
											<th class="qiplantno" scope="col"><spring:message code="editquot.referenceno" /></th>
											<th class="qicaltype" scope="col"><spring:message code="caltype" /></th>  																															
										</tr>
										
										<c:set var="itemno" value="1" />
								 	</c:if>
								 	
									<c:set var="caltype" value="${quoteitem.caltype.calTypeId}"/>
									<c:set var="qinotecount" value="${quoteitem.publicActiveNoteCount + quoteitem.privateActiveNoteCount}"/>
									<tr style=" background-color: ${quoteitem.caltype.serviceType.displayColour}; ">
										<td class="qid">${quoteitem.id}</td>
										<td class="qitemno">${quoteitem.itemno}</td>
										<td class="qimodel">
											<c:choose> 
												<c:when test="${not empty quoteitem.inst}">
													<quotation:showInstrument instrument="${quoteitem.inst}"/>
												</c:when>
												<c:otherwise>
													<cwms:showmodel instrumentmodel="${quoteitem.model}"/>
												</c:otherwise>
											</c:choose>		
										</td>
										<td class="qiplantno">${quoteitem.referenceNo}</td>
										<td class="qicaltype"><t:showTranslationOrDefault translations="${quoteitem.caltype.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/></td>
									</tr>										
										
									<c:set var="thisBaseUnit" value="false"/>
									<c:if test="${quoteitem.modules.size() > 0}">
									
										<tr style=" display: none; ">
											<td colspan="5"></td>
										</tr>
									
										<c:forEach var="module" items="${quoteitem.modules}">
											<c:set var="thisBaseUnit" value="true"/>
											<c:set var="mnotecount" value="${module.publicActiveNoteCount + module.privateActiveNoteCount}"/>											
											
											<tr style=" background-color: ${module.caltype.serviceType.displayColour}">
												<td class="qid">${module.id}</td>
												<td class="qitemno"><img src="img/icons/arrow_merge.png" height="16" width="16" alt="<spring:message code="editquothead.partofbaseunit" /> - <cwms:showmodel instrumentmodel="${quoteitem.model}"/>" title="<spring:message code="editquothead.partofbaseunit" /> - <cwms:showmodel instrumentmodel="${quoteitem.model}"/>"/></td>
												<td class="qimodel">
													<c:choose> 
														<c:when test="${not empty module.inst}">
															<quotation:showInstrument instrument="${module.inst}"/>
														</c:when>
														<c:otherwise>
															<cwms:showmodel instrumentmodel="${module.model}"/>
														</c:otherwise>
													</c:choose>
												</td>
												<td class="qiplantno">
													<c:choose> 
														<c:when test="${not empty module.inst}">
															<c:choose> 
																<c:when test="${module.inst.plantno && (not empty module.inst.plantno)}" >
																(<span class="bold">P</span>)${module.inst.plantno}</c:when>
																<c:otherwise>
																(<span class="bold">S</span>) ${module.inst.serialno}
																</c:otherwise>
															</c:choose>
															
														</c:when>
														<c:otherwise>
															<c:if test="${module.plantno && (not empty module.plantno)}">
																(<span class="bold">P</span>) ${module.plantno}
															</c:if>	
														</c:otherwise>
														</c:choose>
												</td>
												<td class="qicaltype"><t:showTranslationOrDefault translations="${module.caltype.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/></td>
											</tr>																	
										</c:forEach>
									</c:if>															
									<c:set var="rowcount" value="${rowcount + 1}" />														
								</c:if>
							</c:forEach>	
						</tbody>
						</table>						
						</c:when>
						<c:otherwise>
							<table class="default4" summary="<spring:message code="editquothead.tablenoquotation" />">
								<tbody>
									<tr class="nobord">												
										<td class="totalsum text-center bold"><spring:message code="editquothead.noitemsquoteheading" /></td>
									</tr>
								</tbody>
							</table>
						</c:otherwise>
					</c:choose>	
				</c:otherwise>
			</c:choose>								
			</c:forEach>		
		</div>
	</form:form>
</jsp:body>
</t:crocodileTemplate>