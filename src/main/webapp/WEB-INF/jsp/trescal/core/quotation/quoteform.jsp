<%-- File name: /trescal/core\quotation/quoteform.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="instrument" tagdir="/WEB-INF/tags/instrument" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs" %>
<%@ taglib prefix = "fn" uri = "http://java.sun.com/jsp/jstl/functions" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<spring:message code="createquot.createquotation" /></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/quotation/QuoteForm.js'></script>
		<script type="text/javascript">
			
			function updateInstructionSize(instructionSize) {}
		</script>
		<script type="module" src="script/components/cwms-instructions/cwms-instructions.js"></script>
	</jsp:attribute>
	<jsp:body>
		<!-- error section displayed when form submission fails -->
		<!-- end error section -->

		<!-- begin the tp quotation request instructions -->
		<div class="infobox">
			<cwms-instructions link-coid="${addquoteform.contact.sub.comp.coid}" link-subdivid="${addquoteform.contact.sub.subdivid}" link-contactid="${addquoteform.contact.personid}" instruction-types="DISCOUNT,COSTING"  show-mode="TABLE"></cwms-instructions>
		</div>
		<!-- end the tp quotation request instructions -->
			
		<div id="confirmcreate" class="infobox">

			<form:form modelAttribute="addquoteform" method="post" action="">

				<fieldset>

					<legend>
						<spring:message code="quotform.confirmquotedetails" />
					</legend>

					<form:errors path="*">
						<div class="warningBox1">
							<div class="warningBox2">
								<div class="warningBox3">
									<h5 class="center-0 attention">
										<spring:message code="quotform.errorcreatingquote" />
									</h5>
									<div class="center attention">${fn:join(messages.toArray(),"<br />")}</div>
								</div>
							</div>
						</div>
					</form:errors>
					<ol>

						<li>
							<label>
								<spring:message code="contact" />:</label>
							<span>${addquoteform.contact.name}</span>
						</li>
						<li>
							<label>
								<spring:message code="subdivision" />:</label>
							<span>${addquoteform.contact.sub.subname}</span>
						</li>
						<li>
							<label>
								<spring:message code="company" />:</label>
							<span>
								<links:companyLinkDWRInfo company="${addquoteform.contact.sub.comp}" rowcount="0"
									copy="true" /></span>
						</li>
						<li>
							<form:label path="clientRef">
								<spring:message code="jobquot.clientsreference" />
							</form:label>
							<form:input path="clientRef" type="text" maxLength="100" />
							<form:errors path="clientRef" cssClass="attention" element="span"></form:errors>
						</li>
						<li>
							<form:label path="reqDate">
								<spring:message code="requestdate" />
							</form:label>
							<form:input path="reqDate" type="date" max="${today}" />
							<form:errors path="reqDate" cssClass="attention" element="span"></form:errors>
						</li>
						<li>
							<label for="sourcedBy">
								<spring:message code="createquot.sourcedby" />:</label>
							<form:select path="sourcedBy" items="${businessContactList}" itemLabel="value"
								itemValue="key" />
							<span class="attention">
								<form:errors path="sourcedBy" /></span>
						</li>
						<li>
							<c:set var="company" value="${addquoteform.contact.sub.comp}" />

								<form:label path="currencyCode">
									<spring:message code="currency" />:</form:label>
								<form:select path="currencyCode" id="currencyCode">
									<c:forEach var="curopt" items="${currencyopts}">

										<option value="${curopt.optionValue}" <c:if test="${curopt.selected == true}">
											selected="selected"
											</c:if>
											>
											${curopt.currency.currencyCode}
											@ ${curopt.defaultCurrencySymbol}1 =
											${curopt.currency.currencySymbol}${curopt.rate}
											<c:choose>
												<c:when test="${curopt.companyDefault == true}">
													[Company Default Rate] </c:when>
												<c:when test="${curopt.systemDefault == true}">
													[System Default Rate] </c:when>
											</c:choose>
										</option>
									</c:forEach>
								</form:select>
								<form:errors path="currencyCode" /></span>
						</li>

						<c:choose>
							<c:when test="${addquoteform.creationType != 'FROM_COMPINSTLIST'}">
								<li>
									<spring:message code="quotform.createthefollowingquotationtype" /> :
								</li>
								<li>
									<!-- this infobox contains quote source -->
									<div class="infobox">

										<!-- div to hold all the tabbed submenu -->
										<div id="tabmenu">

											<!-- tabbed submenu to change between subdivisions and accreditations -->
											<ul class="subnavtab">
												<li><a href="#" id="empty-link"
														onclick="switchMenuFocus(menuElements, 'empty-tab', false); switchCreationType('creationType', 'empty-tab'); return false; "
														class="selected">
														<spring:message code="quotform.blankdefault" /></a></li>
												<li><a href="#" id="jobs-link"
														onclick="switchMenuFocus(menuElements, 'jobs-tab', false); switchCreationType('creationType', 'jobs-tab'); return false; ">
														<spring:message code="quotform.previousjobs" />
														(${jobs.resultsCount})</a></li>
												<li><a href="#" id="recalls-link"
														onclick="switchMenuFocus(menuElements, 'recalls-tab', false); switchCreationType('creationType', 'recalls-tab'); return false; ">
														<spring:message code="quotform.recalls" />
														(${rdetails.size()})</a></li>
												<li><a href="#" id="instruments-link"
														onclick="switchMenuFocus(menuElements, 'instruments-tab', false); switchCreationType('creationType', 'instruments-tab'); return false; ">
														<spring:message code="instruments" /></a></li>
											</ul>

											<!-- div to hold all content which is to be changed using the tabbed submenu -->
											<div class="tab-box">

												<!-- div displays empty -->
												<div id="empty-tab">

													<div class="padding">
														<span>
															<spring:message code="quotform.createablankquotation" />:
														</span>
														<input type="radio" name="creationType" value="EMPTY" <c:if
															test="${addquoteform.creationType == 'EMPTY'}">
														checked="checked" </c:if>/>
													</div>

												</div>

												<!-- div displays jobs -->
												<div id="jobs-tab" class="hid">

													<div class="padding">
														<span>
															<spring:message
																code="quotform.createquotationfromapreviousjobfor" />
															<c:out value=" ${addquoteform.contact.sub.comp.coname}" />:
														</span>
														<input type="radio" name="creationType" value="FROM_JOB" <c:if
															test="${addquoteform.creationType == 'FROM_JOB'}">
														checked="checked" </c:if>/>
													</div>

													<table class="default4 createQuoteFromJob" id="createQuoteFromJob"
														summary="<spring:message code='quotform.tablejobreturned'/>">

														<thead>
															<tr>
																<td colspan="6">${jobs.resultsCount}&nbsp;<c:choose>
																		<c:when test="${jobs.resultsCount != 1}">
																			<spring:message code="jobs" />
																		</c:when>
																		<c:otherwise>
																			<spring:message code="job" />
																		</c:otherwise>
																	</c:choose>
																	&nbsp;
																	<spring:message code="quotform.availablefor" />
																	<c:out
																		value=" ${addquoteform.contact.sub.comp.coname}" />
																</td>
															</tr>
															<tr>
																<th class="source" scope="col">
																	<spring:message code="createquot.source" />
																</th>
																<th class="jobno" scope="col">
																	<spring:message code="jobno" />
																</th>
																<th class="contact" scope="col">
																	<spring:message code="contact" />
																</th>
																<th class="purchaseord" scope="col">
																	<spring:message code="quotform.purchaseorder" />
																</th>
																<th class="clientref" scope="col">
																	<spring:message code="clientref" />
																</th>
																<th class="datein" scope="col">
																	<spring:message code="quotform.datein" />
																</th>
															</tr>
														</thead>
														<tfoot>
															<tr>
																<td colspan="6">
																	<c:choose>
																		<c:when test="${not empty jobs.isMorePages()}">
																			<a href="#"
																				onclick=" loadJobsForCreation($j(this).closest('table').attr('id'), ${addquoteform.contact.sub.comp.coid}, ${resultsperpage }, 2); return false; "
																				class="mainlink">
																				<spring:message
																					code="quotform.fetchnext" />
																				${resultsperpage}
																				<spring:message
																					code="quotform.jobresults" /></a>
																		</c:when>
																		<c:otherwise>
																			&nbsp;
																		</c:otherwise>
																	</c:choose>
																</td>
															</tr>
														</tfoot>
														<tbody>
															<c:set var="rowcount" value="0" />
															<c:choose>
																<c:when test="${jobs.results.size() < 1}">
																	<tr>
																		<td colspan="6" class="bold center">
																			<spring:message code="noresults" />
																		</td>
																	</tr>
																</c:when>
																<c:otherwise>
																	<c:forEach var="jb" items="${jobs.results}">
																		<tr>
																			<td class="source">
																				<form:radiobutton path="quoteFromJobId"
																					value="${jb.jobid}"
																					onclick="sourceClicked();" />
																			</td>
																			<td class="jobno">
																				<links:jobnoLinkDWRInfo
																					rowcount="${rowcount}"
																					jobno="${jb.jobno}"
																					jobid="${jb.jobid}" copy="false" />
																			</td>
																			<td class="contact">
																				<links:contactLinkDWRInfo
																					contact="${jb.contact}"
																					rowcount="${rowcount}" />
																			</td>
																			<td class="purchaseord">
																				<jobs:showJobPosProjection jb="${jb}" />
																			</td>
																			<td class="clientref">${jb.clientRef}</td>
																			<td class="datein"><fmt:formatDate value="${jb.regDate}" type="date" dateStyle="medium"/></td>
																		</tr>
																		<c:set var="rowcount" value="${rowcount + 1}" />
																	</c:forEach>
																</c:otherwise>
															</c:choose>
														</tbody>

													</table>

												</div>

												<div id="recalls-tab" class="hid">

													<div class="padding">
														<span>
															<spring:message
																code="quotform.createquotationfromarecallfor" />
															<c:out value=" ${addquoteform.contact.name}" />: </span>
														<input type="radio" name="creationType" value="FROM_RECALL"
															<c:if test="${addquoteform.creationType == 'FROM_RECALL'}">
														checked="checked" </c:if>/>
													</div>

													<table class="default4 createQuoteFromRecall"
														summary="#springMessage('quotform.tablerecalls')">

														<thead>
															<tr>
																<td colspan="5">${rdetails.size()}&nbsp;<c:choose>
																		<c:when test="${rdetails.size() != 1}">
																			<spring:message code="quotform.recalls" />
																		</c:when>
																		<c:otherwise>
																			<spring:message code="quotform.recall" />
																		</c:otherwise>
																	</c:choose>&nbsp;
																	<spring:message code="quotform.availablefor" />
																	<c:out value=" ${addquoteform.contact.name}" />
																</td>
															</tr>
															<tr>
																<th class="source" scope="col">
																	<spring:message code="createquot.source" />
																</th>
																<th class="recallno" scope="col">
																	<spring:message code="quotform.recallno" />
																</th>
																<th class="for" scope="col">
																	<spring:message code="quotform.for" />
																</th>
																<th class="date" scope="col">
																	<spring:message code="date" />
																</th>
																<th class="items" scope="col">
																	<spring:message code="items" />
																</th>
															</tr>
														</thead>
														<tbody>
															<c:set var="rowcount" value="10000" />
															<c:choose>
																<c:when test="${rdetails.size() < 1}">
																	<tr>
																		<td colspan="5" class="bold center">
																			<spring:message
																				code="quotform.therearenorecallstodisplay" />
																		</td>
																	</tr>
																</c:when>
																<c:otherwise>
																	<c:forEach var="rd" items="${rdetails}">
																		<tr>
																			<td class="source">
																				<c:choose>
																					<c:when
																						test="${(rd.ignoredOnSetting == false) && (rd.ignoredAllOnJob == false)}">
																						<form:radiobutton
																							path="quoteFromRecallId"
																							value="${rd.id}"
																							onclick="sourceClicked();" />
																					</c:when>
																					<c:otherwise>
																						<spring:message
																							code="quotform.recallignored" />
																					</c:otherwise>
																				</c:choose>
																			</td>
																			<td class="plno"><a
																					href="<spring:url value='viewrecall.htm?id=${rd.recall.id}'/>"
																					class="mainlink">${rd.recall.recallNo}</a>
																			</td>
																			<td class="plfor">
																				<links:contactLinkDWRInfo
																					contact="${rd.contact}"
																					rowcount="${rowcount}" />
																				[${rd.contact.sub.comp.coname}]
																			</td>
																			<td class="plexp">
																				<fmt:formatDate
																					value="${rd.recall.date}"
																					type="date" dateStyle="SHORT" />
																			</td>
																			<td class="plitems">${rd.numberOfItems}</td>
																		</tr>
																		<c:set var="rowcount" value="${rowcount + 1}" />
																	</c:forEach>
																</c:otherwise>
															</c:choose>
														</tbody>

													</table>

												</div>

												<div id="instruments-tab" class="hid">

													<a
														href="<spring:url value='scheduledquoterequest.htm?loadtab=new-tab&personid=${addquoteform.contact.personid}'/>">
														<spring:message
															code="quotform.scheduleaquotationgenerationforthiscontact" />
														</a>

												</div>

											</div>

										</div>
										<!-- end of tabmenu div -->

										<!-- this div clears floats and restores page flow -->
										<div class="clear"></div>

									</div>
									<!-- end of infobox div -->

								</li>
							</c:when>
							<c:otherwise>
								<li class="bold">
									<spring:message code="quotform.creatingaquotefor" />
									&nbsp;${addquoteform.qcif.plantids.size()}
									<c:choose>
										<c:when test="${addquoteform.qcif.plantids.size() != 1}">
											<spring:message code="instruments" />
										</c:when>
										<c:otherwise>
											<spring:message code="instrument" />
										</c:otherwise>
									</c:choose>
									<spring:message code="quotform.selectedfromthecompanyinstrumentsearch" />
								</li>
							</c:otherwise>
						</c:choose>
						<li>
							<label>&nbsp;</label>
							<input type="submit" value="<spring:message code='submit'/>" name="submit" id="submit" />
						</li>

					</ol>

				</fieldset>

			</form:form>

		</div>
	</jsp:body>
</t:crocodileTemplate>