<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>


<form:form action="addjobfp.htm" id="additemtojob" method="post" >
	<fieldset>  
 		<div id="subnav">
			<dl>
				<dt><a href="#" onclick="event.preventDefault(); switchMenuFocus(subMenuElements, 'invoiceprice-tab', false); " id="invoiceprice-link" class="selected <c:if test="${wrap.instrumentModelCase}">disabled</c:if>" ><spring:message code="invoices"/>(${wrap.invoiceItemOuputs.size()})</a></dt>
				<dt><a href="#" onclick="event.preventDefault(); switchMenuFocus(subMenuElements, 'jobcosting-tab', false); " id="jobcosting-link" <c:if test="${wrap.instrumentModelCase}">class="disabled"</c:if>><spring:message code="docs.jobcosting"/>(${wrap.jobCostings.size()})</a></dt>
				<dt><a href="#" onclick="event.preventDefault(); switchMenuFocus(subMenuElements, 'contractreview-tab', false); " id="contractreview-link" <c:if test="${wrap.instrumentModelCase}">class="disabled"</c:if>><spring:message code="jicontractreview.contrrew"/>(${not empty wrap.contractReviewCost ? 1 : 0})</a></dt>
				<dt><a href="#" onclick="event.preventDefault(); switchMenuFocus(subMenuElements, 'quotationitem-tab', false); " id="quotationitem-link" <c:if test="${wrap.instrumentModelCase}">class="selected"</c:if>><spring:message code="company.quotations"/>(${wrap.quotationItemOuputs.size()})</a></dt>
				<dt><a href="#" onclick="event.preventDefault(); switchMenuFocus(subMenuElements, 'catalogprice-tab', false); " id="catalogprice-link" ><spring:message code="instrumentmodel"/>(${wrap.catalogPrices.size()})</a></dt>
				<dt><a href="#" onclick="event.preventDefault(); switchMenuFocus(subMenuElements, 'manual-tab', false); " id="manual-link"><spring:message code="schedule.manual"/></a></dt>
			</dl>
		</div>
		
		<div id="invoiceprice-tab" <c:if test="${wrap.instrumentModelCase}">class="hid"</c:if>>
			<table class="default2">
				<thead>
					<tr>
		    			<th class="codefault"><spring:message code="select"/></th>
		    			<th class="codefault"><spring:message code="supplierinvoice.invoicenumber"/></th>
		    			<th class="codefault"><spring:message code="itemno"/></th>
		    			<th class="codefault"><spring:message code="issuedate"/></th>
		    			<th class="codefault"><spring:message code="invoice.discountvalue"/></th>
		    			<th class="codefault"><spring:message code="currency"/></th>
						<th class="codefault"><spring:message code="price"/></th>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${wrap.invoiceItemOuputs.size() == 0}">
							<tr>
								<td colspan="7" class="bold center">
								<spring:message code="viewcapabilities.noresults"/>
								</td>
							</tr>
						</c:when>
						<c:otherwise>
							<c:forEach var="invitem" items="${wrap.invoiceItemOuputs}">
								<tr>
									<td><input id="INVOICE" type="radio" name="priceSelected" value="${invitem.finalPriceForInvoiceItem}"/></td>
									<td><a href="viewinvoice.htm?id=${invitem.invoiceId}">${invitem.invoiceNo}</a></td>
									<td>${invitem.itemno}</td>
									<td>${invitem.issueDate}</td>
									<td>${invitem.generalDiscountRate}</td>
									<td>${invitem.currencyInvoice}</td>
									<td>${invitem.finalPriceForInvoiceItem}</td>
								</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
		</div>
		
		<div id="jobcosting-tab" class="hid">
			<table class="default2">
				<thead>
					<tr>
		    			<th class="codefault"><spring:message code="select"/></th>
		    			<th class="codefault"><spring:message code="docs.jobcosting"/></th>
		    			<th class="codefault"><spring:message code="issuedate"/></th>
		    			<th class="codefault"><spring:message code="invoice.discountvalue"/></th>
		    			<th class="codefault"><spring:message code="currency"/></th>
						<th class="codefault"><spring:message code="price"/></th>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${wrap.jobCostings.size() == 0}">
							<tr>
								<td colspan="6" class="bold center">
								<spring:message code="viewcapabilities.noresults"/>
								</td>
							</tr>
						</c:when>
						<c:otherwise>
							<c:forEach var="jc" items="${wrap.jobCostings}">
								<tr>
									<td><input id="JOB_COSTING" type="radio" name="priceSelected" value="${jc.finalCost}"/></td>
									<td><a href="viewjobcosting.htm?id=${jc.elementId}" class="mainlink" title="View Job Costing">${jc.elementLabel}</a></td>
									<td>${jc.issueDate}</td>
									<td>${jc.discountRate}</td>
									<td>${jc.currencyCode}</td>
									<td>${jc.finalCost}</td>
								</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
		</div>
		
		<div id="contractreview-tab" class="hid">
			 <table class="default2">
				<thead>
					<tr>
		    			<th class="codefault"><spring:message code="select"/></th>
						<th class="codefault"><spring:message code="itemno"/></th>
						<th class="codefault"><spring:message code="servicetype"/></th>
						<th class="codefault"><spring:message code="currency"/></th>
						<th class="codefault"><spring:message code="price"/></th>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${empty wrap.contractReviewCost}">
							<tr>
								<td colspan="6" class="bold center">
								<spring:message code="viewcapabilities.noresults"/>
								</td>
							</tr>
						</c:when>
						<c:otherwise>
							<tr>
								<td><input id="CONTRACT_REVIEW" type="radio" name="priceSelected" value="${wrap.contractReviewCost.finalCost}"/></td>
								<td><a href="jicontractreview.htm?jobitemid=${wrap.contractReviewCost.jobItemId}">${wrap.contractReviewCost.itmeno}</a></td>
								<td>${wrap.contractReviewCost.serviceType}</td>
								<td>${wrap.contractReviewCost.currencyERSymbol}</td>
								<td>${wrap.contractReviewCost.finalCost}</td>
							</tr>
						</c:otherwise>
					</c:choose>
					
				</tbody>
			</table>
		</div>
		
		<div id="quotationitem-tab" <c:if test="${!wrap.instrumentModelCase}">class="hid"</c:if>>
			<table class="default2">
				<thead>
					<tr>
		    			<th class="codefault"><spring:message code="select"/></th>
						<th class="codefault"><spring:message code="tpcosts.quotationno"/></th>
						<th class="codefault"><spring:message code="itemno"/></th>
						<th class="codefault"><spring:message code="createquot.expirydate"/></th>
						<th class="codefault"><spring:message code="currency"/></th>
						<th class="codefault"><spring:message code="price"/></th>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${wrap.quotationItemOuputs.size() == 0}">
							<tr>
								<td colspan="6" class="bold center">
									<spring:message code="viewcapabilities.noresults"/>
								</td>
							</tr>
						</c:when>
						<c:otherwise>
							<c:forEach var="qi" items="${wrap.quotationItemOuputs}">
								<tr>
									<td><input id="QUOTATION" type="radio" name="priceSelected" value="${qi.finalPrice}"/></td>
									<td><a href="viewquotation.htm?id=${qi.quoteId}" class="mainlink">${qi.quoteNo} version ${qi.quotationVer}</a></td>
									<td>${qi.itemNo}</td>
									<td>${qi.exiryDate}</td>
									<td>${qi.currencyCode}</td>
									<td>${qi.finalPrice}</td>
								</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
		</div>
		
		<div id="catalogprice-tab" class="hid">
			<table class="default2">
				<thead>
					<tr>
		    			<th class="codefault"><spring:message code="select"/></th>
						<th class="codefault"><spring:message code="source"/></th>
						<th class="codefault"><spring:message code="instmod.modeltype"/></th>
						<th class="codefault"><spring:message code="instmodelname"/></th>
						<th class="codefault"><spring:message code="servicetype"/></th>
						<th class="codefault"><spring:message code="currency"/></th>
						<th class="codefault"><spring:message code="price"/></th>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${wrap.catalogPrices.size() == 0}">
							<tr>
								<td colspan="7" class="bold center">
								<spring:message code="viewcapabilities.noresults"/>
								</td>
							</tr>
						</c:when>
						<c:otherwise>
							<c:forEach var="ct" items="${wrap.catalogPrices}">
								<tr>
									<td><input id="MODEL" type="radio" name="priceSelected" value="${ct.price}"/></td>
									<td>${ct.source}</td>
									<td>${ct.modelTypeName}</td>
									<td><a href="instrumentmodel.htm?modelid=${ct.modelId}">${ct.modelName}</a></td>
									<td>${ct.serviceType}</td>
									<td>${wrap.currency}</td>
									<td>${ct.price}</td>
								</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
		</div>
		
		<div id="manual-tab" class="hid">
			<table class="default2">
				<thead>
					<tr>
		    			<th class="codefault"><spring:message code="select"/></th>
		    			<th class="codefault"><spring:message code="currency"/></th>
						<th class="codefault"><spring:message code="price"/></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><input id="MANUAL" type="radio" name="priceSelected" value="Manual"/></td>
						<td>${wrap.currency}</td>
						<td><input id="manualPrice" type="text" value=""/></td>
					</tr>
				</tbody>
			</table>
		</div>
	</fieldset>
</form:form>
