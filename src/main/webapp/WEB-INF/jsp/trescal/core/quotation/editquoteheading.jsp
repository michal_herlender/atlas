<%-- File name: /trescal/core/quotation/editquoteheading.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="quotation" tagdir="/WEB-INF/tags/quotation" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/quotation/EditQuoteHeading.js'></script>
    </jsp:attribute>
    <jsp:body>
			<form:form modelAttribute="quoteheadingform" name="editquoteheadingform"  method="post">
			
				<form:hidden path="formfunction" />
				
				<div class="infobox">
					
					<div class="float-right">
						<a class="mainlink" href="<spring:url value='quoteheadingform.htm?id=${quoteheadingform.header.quotation.id}'/>">
							<spring:message code="editquothead.backtoheadingssummary"/>
						</a>
					</div>
					
					<div class="clear"></div>
				
					<fieldset>
					
						<legend>
							<spring:message code="editquothead.quotationheadingfor"/>
							<a href="<spring:url value='viewquotation.htm?id=${quoteheadingform.header.quotation.id}'/>">
								${quoteheadingform.header.quotation.qno} version ${quoteheadingform.header.quotation.ver}
							</a>
						</legend>
						
						<ol>
															
							<c:if test="${quoteheadingform.header.systemDefault == true}">
								<li class="infotext">
									<spring:message code="editquothead.systemdefaultheadingquotation"/>	
								</li>
							</c:if>	
							
							<li>
								<form:label path="header.headingName"><spring:message code="quotaddinstmod.heading"/>:</form:label>
								<c:set var="disable" value=""/>
								<c:if test="${quoteheadingform.header.systemDefault == true}">
									<c:set var="disable" value="disabled='disabled'"/>
								</c:if>	
								<form:input path="header.headingName" tabindex="disable" />
 								<form:errors path="header.headingName"/>
							</li>
							<li>
								<form:label path="header.headingDescription"><spring:message code="editquothead.headingdescription"/>:</form:label>
								<form:textarea path="header.headingDescription" id="disable" rows="4" cols="50"/>
								<form:errors path="header.headingDescription"/>
							</li>
							<li>
								<form:label path="quoteDefault"><spring:message code="editquothead.setasquotationdefault"/>:</form:label>
								<form:checkbox path="quoteDefault"/>	
								<form:errors path="quoteDefault"/>
							</li>
							
						</ol>						
					
					</fieldset>
				
				</div>
				
				<div class="infobox">
				
					<div class="headingitems">
											
						<c:choose>
							<c:when test="${quoteheadingform.header.quoteitems.size() > 0}">
																							
							<c:set var="caltype" value="0"/>											
							<c:set var="qinotecount" value="0"/>
							<c:set var="mnotecount" value="0"/>												

							<table class="default4 quoteheadingitems" summary="This table contains quote items that have been sorted">
								<tbody>
									<c:forEach var="quoteitem" items="${quoteheadingform.header.quoteitems}">
										<c:if test="${quoteitem.partOfBaseUnit != true}">
											<c:if test="${caltype != quoteitem.caltype.calTypeId}">
												<tr>
													<td colspan="5"><t:showTranslationOrDefault translations="${quoteitem.caltype.serviceType.longnameTranslation}" defaultLocale="${defaultlocale}"/>
																	<spring:message code="items"/>
													</td>
												</tr>
												<tr>
													<th class="qid" scope="col">																	
														<spring:message code="quotaddinst.all"/>
														<input type="checkbox" onclick=" toggleQIDeleteItems(this.checked, ${quoteitem.caltype.calTypeId}); " />
													</th>
													<th class="qitemno" scope="col"><spring:message code="itemno"/></th>
													<th class="qimodel" scope="col"><spring:message code="editquothead.instrumentmodel"/></th>
													<th class="qiplantno" scope="col"><spring:message code="editquot.referenceno"/></th>
													<th class="qicaltype" scope="col"><spring:message code="caltype"/></th>  																															
												</tr>
												<tr>
													<td colspan="5">&nbsp;</td>
												</tr>
											</c:if>
											<c:set var="caltype" value="${quoteitem.caltype.calTypeId}"/>
											<c:set var="qinotecount" value="${quoteitem.publicActiveNoteCount + quoteitem.privateActiveNoteCount}"/>
											<tr style=" background-color: ${quoteitem.caltype.serviceType.displayColour}; ">
												<td class="qid">
													<form:checkbox value="${quoteitem.id}" path="items" id="items" class="headItem_${quoteitem.caltype.calTypeId}" />
												</td>
												<td class="qitemno">${quoteitem.itemno}</td>
												<td class="qimodel">
													<c:choose>
														<c:when test="${not empty quoteitem.inst}">
															<quotation:showInstrument instrument="${quoteitem.inst}" />
														</c:when>
														<c:otherwise>
															<cwms:showmodel instrumentmodel="${quoteitem.model}"/>
														</c:otherwise>
													</c:choose>		
												</td>
												<td class="qiplantno">${quoteitem.referenceNo}</td>
												<td class="qicaltype"><t:showTranslationOrDefault translations="${quoteitem.caltype.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/></td>
											</tr>										
											
											<c:set var="thisBaseUnit" value="false"/>
											<c:if test="${quoteitem.modules.size() > 0}">
														
												<tr style=" display: none; ">
													<td colspan="5"></td>
												</tr>
											
												<c:forEach var="module" items="${quoteitem.modules}">
													<c:set var="thisBaseUnit" value="true"/> 
													<c:set var="mnotecount" value="${module.publicActiveNoteCount + module.privateActiveNoteCount}"/>												
													
													<tr style=" background-color: ${module.caltype.serviceType.displayColour}; ">
														<td class="qid">
															<form:checkbox value="${module.id}" path="items" id="items" />
														</td>
														<td class="qitemno"><img src="img/icons/arrow_merge.png" height="16" width="16" alt="Part of base unit - <cwms:showmodel instrumentmodel="${quoteitem.model}"/>" title="<spring:message code="editquothead.partofbaseunit"/> - <cwms:showmodel instrumentmodel="${quoteitem.model}"/>" /></td>
														<td class="qimodel">
															<c:choose> 
																<c:when test="${not empty module.inst}">
																	<quotation:showInstrument instrument="${module.inst}" />
																</c:when>
																<c:otherwise>
																	<cwms:showmodel instrumentmodel="${module.model}"/>
																</c:otherwise>
															</c:choose>	
														</td>
														<td class="qiplantno">
															<c:choose> 
																<c:when test="${module.inst != null}">
																<c:choose> 
																	<c:when test="${module.inst.plantno != null && module.inst.plantno != ''}">
																		(<span class="bold">P</span>) <c:out value="${module.inst.plantno}"/>
																	</c:when>
																	<c:otherwise>
																		(<span class="bold">S</span>) <c:out value="${module.inst.serialno}"/>
																	</c:otherwise>
																</c:choose>	
																</c:when>
																<c:otherwise>
																<c:if test="${module.plantno != null && module.plantno != ''}">
																	(<span class="bold">P</span>) <c:out value="${module.plantno}"/>
																</c:if>
																</c:otherwise>
															</c:choose>
														</td>
														<td class="qicaltype"><t:showTranslationOrDefault translations="${module.caltype.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/></td>
													</tr>																	
												</c:forEach>
													
											</c:if>															
										</c:if>	
									</c:forEach>
								</tbody>
							</table>
							
							<c:if test="${headings.size() > 0}">
							
								<p id="assignoptions">
								
									<spring:message code="editquothead.assigntheselecteditemstoanewheading"/>
									
									<form:select path="assignToHeading" items="${headings}" itemValue="headingId" itemLabel="headingName"/>
									
								</p>
							
							</c:if>	
														
							</c:when>
							<c:otherwise>
						
							<table class="default4" summary="<spring:message code="editquothead.tablenoquotation"/>">
								<tbody>
									<tr class="nobord">												
										<td class="totalsum text-center bold"><spring:message code="editquothead.noitemsquoteheading"/></td>
									</tr>
								</tbody>
							</table>
						
							 </c:otherwise>
					    </c:choose>							
					
					</div>
					
				</div>
				
				<div class="text-center">
					<input type="submit" value="<spring:message code="save"/>" name="save" />
				</div>
			
			</form:form>
    </jsp:body>
</t:crocodileTemplate>