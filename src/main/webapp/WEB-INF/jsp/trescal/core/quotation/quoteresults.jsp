<%-- File name: /trescal/core/quotation/quoteresults.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="quotation" tagdir="/WEB-INF/tags/quotation"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="quotresult.quotationresults" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/quotation/QuoteResults.js'></script>
    </jsp:attribute>
    <jsp:body>
       <!-- infobox contains all search results and is styled with nifty corners -->
		<div class="infobox">
		
			<c:set var="quotes" value="${command.quotes}" />
		
			<form:form action="" name="searchform" id="searchform" method="post" modelAttribute="command" >
		
			  <form:hidden path="coid" />
			  <form:hidden path="personid" />
			  <form:hidden path="coname" />
			  <form:hidden path="contact" />
			  <form:hidden path="qno" />
			  <form:hidden path="quotestatus" />
			  <form:hidden path="clientref" />
			  <form:hidden path="resultsYearFilter" />
			  <form:hidden path="resultsPerPage" />
			  <form:hidden path="pageNo" />
											
			  <form:hidden path="mfr" />
			  <form:hidden path="mfrid" />
			  <form:hidden path="model" />
			  <form:hidden path="description" />
			  <form:hidden path="descid" />
			  <form:hidden path="modelid" />
		
			  <form:hidden path="plantid" />
			  <form:hidden path="plantno" />
			  <form:hidden path="serialno" />
		
			  <form:hidden path="issueDateBetween" />
			  <form:hidden path="issueDate1" />
			  <form:hidden path="issueDate2" />
			  
			  <form:hidden path="expiryDateBetween" />
			  <form:hidden path="expiryDate1" />
			  <form:hidden path="expiryDate2" />
			
											
				<t:showResultsPagination rs="${command.rs}" pageNoId="" />
																	
				<table id="quotesearchresults" class="default2" summary="<spring:message code="quotresult.tablesearchresults" />">
					<thead>
						<tr>
							<td colspan="7">
								<spring:message code="quotresult.quotationresults" /> (${quotes.size()})
							</td>
						</tr>
						<tr>
							<th class="qresquoteno" scope="col"><spring:message code="quoteno" /></th>  
							<th class="qresconame" scope="col"><spring:message code="company" /></th> 
							<th class="qressubdiv" scope="col"><spring:message code="subdiv" /></th> 
							<th class="qrescontact" scope="col"><spring:message code="contact" /></th>  
							<th	class="qresstatus" scope="col"><spring:message code="status" /></th>
							<th class="qresclientref" scope="col"><spring:message code="clientref" /></th>
							<th	class="qresregdate" scope="col"><spring:message code="quotresult.regdate" /></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="7">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody id="keynavResults">
						<c:set var="rowcount" value="0"/> 
				    <c:choose>
	         		   <c:when test="${quotes.size() < 1 }">
							<tr>
								<td colspan="7" class="bold center">
									<spring:message code="noresults" />
							</tr>
					   </c:when>
					   <c:otherwise>												
							<c:forEach var="q" items="${quotes}">
								<tr onclick="window.location.href='<spring:url value="viewquotation.htm?id=${q.id}"/>'>" onmouseover=" $j(this).addClass('hoverrow'); " onmouseout=" if (this.className != 'hoverrow') $j(this).removeClass('hoverrow'); ">
									<td class="qresquoteno">
										<links:quoteLinkInfo quoteId="${q.id}" quoteNo="${q.qno}" quoteVer="${q.ver}" showversion="true" rowcount="${rowcount}"/>
									</td>
									<td class="qresconame"><links:companyLinkDWRInfo company="${q.contact.sub.comp}" rowcount="${rowcount}" copy="false"/></td>
									<td class="qressubdiv"><links:subdivLinkDWRInfo subdiv="${q.contact.sub}" rowcount="${rowcount}"/></td>
									<td class="qrescontact"><links:contactLinkDWRInfo contact="${q.contact}" rowcount="${rowcount}"/></td>
									<td	class="qresstatus"><t:showTranslationOrDefault translations="${q.quotestatus.nametranslations}" defaultLocale="${defaultlocale}"/></td>
									<td class="qresclientref">${q.clientref}</td>
									<td	class="qresregdate"><fmt:formatDate value="${q.regdate}" type="date" dateStyle="SHORT"/></td>
								</tr>
								<c:set var="rowcount" value="${rowcount + 1}"/>
							</c:forEach>
					   </c:otherwise>
					</c:choose>
					</tbody>
				</table>
			
				<t:showResultsPagination rs="${command.rs}" pageNoId="" />
				
			</form:form>
			
		</div>
		<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>