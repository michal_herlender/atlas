<%-- File name: /trescal/core/quotation/recalltoquotation.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="recalltoquot.createquotationfromrecallfor"/> 
  									<c:choose>
  										<c:when test="${form.rdetail.recallType == 'CONTACT'}">
  											${form.rdetail.contact.name}, ${form.rdetail.contact.sub.comp.coname} 
  										</c:when>
  										<c:otherwise>
  											${form.rdetail.address.addr1}, ${form.rdetail.address.sub.comp.coname} 
  									</c:otherwise>
  									</c:choose>
		</span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/quotation/RecallToQuotation.js'></script>
    </jsp:attribute>
    <jsp:body>
     		<!-- error section displayed when form submission fails -->
		<form:errors path="form.*">
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<h5 class="center-0 attention"><spring:message code="recalltoquot.errorcreatingfromrecall"/>:</h5>
						<c:forEach var="e" items="${messages}">
							<div class="center attention"><c:out value="${e}"/></div>
						</c:forEach>
					</div>
				</div>
			</div>
		</form:errors>
			<!-- end error section -->
			
			<!-- set recall detail -->
			<c:set var="rd" value="${form.rdetail}" />
			
			<c:set var="recallTypeId" value="0" />
			<c:choose>
				<c:when test="${rd.recallType == 'CONTACT'}">
					<c:set var="recallTypeId" value="${rd.contact.personid}" />
				</c:when>
				<c:otherwise>
					<c:set var="recallTypeId" value="${rd.address.addrid}" />
				</c:otherwise>
			</c:choose>						
			
			<div class="recallToQuotation">
			
				<form:form modelAttribute="form" method="post" action="" >
			
					<!-- infobox div with nifty corners applied -->
					<div class="infobox">
						
						<fieldset>
						
							<legend><spring:message code="recalltoquot.createquotationfromrecallfor"/></legend>
							
							<div class="displaycolumn">
							
								<ol>									
									
									<li>
										<label><spring:message code="contact"/>:</label>
										<span>
											<c:choose>
												<c:when test="${not empty form.contact}">
													${form.contact.name}
												</c:when>
												<c:when test="${rd.recallType == 'CONTACT'}">
													${rd.contact.name}
												</c:when>
											</c:choose>	
										</span>
									</li>	
									<li>
										<label><spring:message code="subdiv"/>:</label>
										<span>
											<c:choose>
												<c:when test="${not empty form.contact}">
													${form.contact.sub.subname}
												</c:when>
												<c:otherwise>
													<c:choose>
														<c:when test="${rd.recallType == 'CONTACT'}">
															${rd.contact.sub.subname}
														</c:when>
														<c:otherwise>
															${rd.address.sub.subname}
														</c:otherwise>
													</c:choose>
												</c:otherwise>
											</c:choose>
										</span>
									</li>
									<li>
										<label><spring:message code="company"/>:</label>
										<span>
											<c:choose>
												<c:when test="${not empty form.contact}">
													${form.contact.sub.comp.coname}
												</c:when>
												<c:otherwise>
													<c:choose>
														<c:when test="${rd.recallType == 'CONTACT'}">
															${rd.contact.sub.comp.coname}
														</c:when>
														<c:otherwise>
															${rd.address.sub.comp.coname}
														</c:otherwise>
													</c:choose>
												</c:otherwise>
											</c:choose>
										</span>
									</li>																				
									<li>
										<label path="clientRef"><spring:message code="jobquot.clientsreference"/>:</label>
										<c:choose>
											<c:when test="${not empty form.fromRecall}">
												<form:input path="clientRef" />
											</c:when>
											<c:otherwise>
												<span>${form.clientRef}</span>
											</c:otherwise>
										</c:choose>
									</li>										
									<li>
										<label path="reqDate"><spring:message code="requestdate"/>:</label>
										<c:choose>
											<c:when test="${not empty form.fromRecall}">
												<form:input  path="reqDate" type="date"  max="${today}" readonly="readonly" autocomplete="off" />
											</c:when>
											<c:otherwise>
												<span>${form.reqDate}</span>
											</c:otherwise>
										</c:choose>
									</li>
																			
								</ol>
							
							</div>
							
							<div class="displaycolumn">
							
								<ol>
								
									<li>
										<label><spring:message code="quotform.recallno"/>:</label>
										<span>${rd.recall.recallNo}</span>
									</li>
									<li>
										<label><spring:message code="recalltoquot.recalldate"/>:</label>
										<span><fmt:formatDate value="${rd.recall.date}" type="date" dateStyle="SHORT"/></span>
									</li>
									<li>
										<label><spring:message code="recalltoquot.recallperiod"/>:</label>
										<span><fmt:formatDate value="${rd.recall.dateFrom}" type="date" dateStyle="SHORT"/> to <fmt:formatDate value="${rd.recall.dateTo}" type="date" dateStyle="SHORT"/></span>
									</li>
									<li>
										<label><spring:message code="currency"/>:</label>
										<c:choose>
											<c:when test="${not empty form.contact}">
												<c:set var="company" value="${form.contact.sub.comp}" />
											</c:when>
											<c:otherwise>
											<c:choose>
												<c:when test="${rd.recallType == 'CONTACT'}">
													<c:set var="company" value="${rd.contact.sub.comp}" />
												</c:when>
												<c:otherwise>
													<c:set var="company" value="${rd.address.sub.comp}" />
												</c:otherwise>
											</c:choose>		
											</c:otherwise>	
										</c:choose>						
										<c:choose>
											<c:when test="${not empty form.fromRecall}">										
												<form:select path="currencyCode" items="${currencyopts}" itemLabel="label" itemValue="optionValue"/>												
												<form:errors cssClass="attention" path="currencyCode"/>
											</c:when>
											<c:otherwise>
											<span>
													<c:forEach var="curopt" items="${currencyopts}">												
														<c:if test="${curopt.currency.currencyCode == form.currencyCode}">
															${curopt.currency.currencyCode}
															@ ${curopt.defaultCurrencySymbol}1 = ${curopt.currency.currencySymbol}${curopt.rate}
															<c:choose>
																<c:when test="${curopt.companyDefault == true}">
																	[Company Default Rate] </c:when>
																<c:when test="${curopt.systemDefault == true}">
																	[System Default Rate] </c:when>
															</c:choose>	
														</c:if>																			
													</c:forEach>
												</span>
											</c:otherwise>
										</c:choose>	
									</li>											
								
								</ol>
							
							</div>
						
						</fieldset>
						
					</div>
					<!-- end of infobox div with nifty corners applied -->
					
					<!-- infobox div with nifty corners applied -->
					<div class="infobox">
						
						<h5 class="text-center">Prohibition Items (<a href="#" class="mainlink" onclick=" loadProhibitionInstruments(this, ${rd.recall.id}, '${rd.recallType}', ${recallTypeId}); return false; ">Load Prohibition Instruments</a>)</h5>
													
						<table class="default4 prohibitionItems hid" summary="<spring:message code='recalltoquot.tableprohibitionitems'/>">
							<thead>																
								<tr>
									<th scope="col" class="addtoquote"><spring:message code="add"/><input type="checkbox" onclick=" selectAllItems(this.checked, 'prohibitionItems'); " /></th>
									<th scope="col" class="barcode"><spring:message code="barcode"/></th> 
									<th scope="col" class="inst"><spring:message code="instrument"/></th>
									<th scope="col" class="serial"><spring:message code="serialno"/></th>
									<th scope="col" class="plant"><spring:message code="plantno"/></th>
									<th scope="col" class="duedate"><spring:message code="duedate"/></th>											
								</tr>
							</thead>
							<tfoot>
								<tr>
									<td colspan="6">&nbsp;</td>
								</tr>
							</tfoot>
							<tbody>
								<tr>
									<td colspan="6" class="bold text-center"><spring:message code="recalltoquot.noprohibitionitemsfound"/></td>
								</tr>
							</tbody>
						</table>							
						
					</div>
					<!-- end of infobox div with nifty corners applied -->
					
					<!-- infobox div with nifty corners applied -->
					<div class="infobox">
					
						<h5 class="text-center"><spring:message code="recalltoquot.itemsonrecallforperiod"/> <fmt:formatDate value="${rd.recall.dateFrom}" type="date" dateStyle="SHORT"/> to <fmt:formatDate value="${rd.recall.dateTo}" type="date" dateStyle="SHORT"/></h5>
						
						<table class="default4 recallItems" summary="This table lists all items recalled in the given month">
							<thead>
								<tr>
									<td colspan="6">${form.recallitems.size()} 
													<c:choose>
														<c:when test="${form.recallitems.size() != 1}"> items </c:when>
														<c:otherwise> item </c:otherwise>
													</c:choose> on recall</td>
								</tr>								
								<tr>
									<th scope="col" class="addtoquote"><spring:message code="add"/> <input type="checkbox" onclick=" selectAllItems(this.checked, 'recallItems'); " /></th>
									<th scope="col" class="barcode"><spring:message code="barcode"/></th> 
									<th scope="col" class="inst"><spring:message code="instrument"/></th>
									<th scope="col" class="serial"><spring:message code="serialno"/></th>
									<th scope="col" class="plant"><spring:message code="plantno"/></th>
									<th scope="col" class="duedate"><spring:message code="duedate"/></th>											
								</tr>
							</thead>
							<tfoot>
								<tr>
									<td colspan="6">&nbsp;</td>
								</tr>
							</tfoot>
							<tbody>
								<c:set var="rowcount" value="1" />
								<c:forEach var="ri" items="${form.recallitems}">										
									<tr>
										<td class="addtoquote">												
											<c:if test="${ri.excludeFromNotification != true}">
												<input type="checkbox" name="toQuotePlantIds" value="${ri.instrument.plantid}" />
											</c:if>
										</td>
										<td class="barcode"><links:instrumentLinkDWRInfo instrument="${ri.instrument}" rowcount="0" displayBarcode="true" displayName="false" displayCalTimescale="true" caltypeid="${jobItem.calType.calTypeId}" /></td>
<!-- 										#instrumentLinkDWRInfo($ri.instrument $rowcount true false true 0) -->
										<td class="inst">${ri.instrument.definitiveInstrument} <c:if test="${ri.excludeFromNotification == true}"> <spring:message code="recalltoquot.itemexcludedfromrecall"/> </c:if></td>
										<td class="serial">${ri.instrument.serialno}</td>
										<td class="plant">${ri.instrument.plantno}</td>
										<td class="duedate"><fmt:formatDate value="${ri.instrument.nextCalDueDate}" type="date" dateStyle="SHORT"/></td>												
									</tr>
									<c:set var="rowcount" value="${rowcount + 1}"/>									
								</c:forEach>
							</tbody>
						</table>
						
					</div>
					<!-- end of infobox div with nifty corners applied -->
					
					<!-- infobox div with nifty corners applied -->
					<div class="infobox">
					
						<h5 class="text-center"><spring:message code="recalltoquot.futurerecallitems"/> (<a href="#" class="mainlink" onclick=" loadFutureInstruments(this, ${rd.recall.id}, '${rd.recallType}', ${recallTypeId}, 2); return false; ">Load Future Recall Instruments</a>)</h5>
													
						<table class="default4 futureItems hid" summary="This table lists future recall items">
							<thead>																
								<tr>
									<th scope="col" class="addtoquote"><spring:message code="add"/> <input type="checkbox" onclick=" selectAllItems(this.checked, 'futureItems'); " /></th>
									<th scope="col" class="barcode"><spring:message code="barcode"/></th> 
									<th scope="col" class="inst"><spring:message code="instrument"/></th>
									<th scope="col" class="serial"><spring:message code="serialno"/></th>
									<th scope="col" class="plant"><spring:message code="plantno"/></th>
									<th scope="col" class="duedate"><spring:message code="duedate"/></th>											
								</tr>
							</thead>
							<tfoot>
								<tr>
									<td colspan="6">&nbsp;</td>
								</tr>
							</tfoot>
							<tbody>
								<tr>
									<td colspan="6" class="bold text-center"><spring:message code="recalltoquot.nofuturerecallitemsfound"/></td>
								</tr>
							</tbody>
						</table>
						
					</div>
					<!-- end of infobox div with nifty corners applied -->
					
					<div class="text-center">
						<input type="submit" name="submit" value="<spring:message code='recalltoquot.createquotefromrecall'/>" />
					</div>
				
				</form:form>
				
			</div>
    </jsp:body>
</t:crocodileTemplate>