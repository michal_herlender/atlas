<%-- File name: /trescal/core/quotation/createquote.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="quotations"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script>
		
		    var sourcedByContBySubdivRequest = ${sourcedByContBySubdivRequest} 
			var sourcedByContBySubdivAC =  ${sourcedByContBySubdivAC};
			var sourcedByContBySubdivIssued =  ${sourcedByContBySubdivIssued};
			var quoteRequesteSubdivId =  ${command.quoteRequesteSubdivId};
			var quoteRequesteContactId =  ${command.quoteRequesteContactId};
			var quoteSubdivId =  ${command.quoteSubdivId};
			var quoteContactId =  ${command.quoteContactId};
		</script>
		<script src='script/trescal/core/quotation/NewCreateQuote.js'></script>
	</jsp:attribute>
	<jsp:body>
		<!-- error section displayed when form submission fails -->
		<form:errors path="command.*">
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<h5 class="center-0 attention"><spring:message code="error"/>:</h5>
						<c:forEach var="e" items="${messages}">
							<div class="center attention"><c:out value="${e}"/></div>
						</c:forEach>
					</div>
				</div>
			</div>
		</form:errors>
		<!-- end error section -->
		<!-- sub navigation which changes between search quotations and create quotations -->
		<div id="subnav">
			<dl>
				<dt><a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'homequote-tab', true); " id="homequote-link" title="<spring:message code='createquot.viewquoteregistercategories'/>" class="selected"><spring:message code="createquot.quoteregister"/></a></dt>
				<dt><a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'searchquote-tab', true); " id="searchquote-link" title="<spring:message code='createquot.searchquotations'/>"><spring:message code="createquot.searchquotations"/></a></dt>
				<dt><a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'createquote-tab', true); " id="createquote-link" title="<spring:message code='createquot.createquotation'/>"><spring:message code="createquot.createquotation"/></a></dt>
			</dl>
		</div>
		<div class="infobox">
			<div id="homequote-tab">
				<h5>
					<spring:message code="createquot.quotationregister"/> -
					<a href="#" onclick=' event.preventDefault(); loadScript.createOverlay("external", "<spring:message code='createquot.logaquotationrequest' />", "quotationrequest.htm", null, null, 70, 640, null); ' class="mainlink" title="">
						<spring:message code="createquot.logaquotationrequest"/>
					</a>
				</h5>
				
				<!-- div to hold all the tabbed submenu -->
				<div id="tabmenu" class="createquotetabs">
					<!-- tabbed submenu to change between subdivisions and accreditations -->
					<ul class="subnavtab">
						<li><a href="#" id="quoterequest-link" onclick="event.preventDefault(); switchMenuFocus(menuElements1, 'quoterequest-tab', false); " class="${ command.tabIndex == 0 ? 'selected':'' }">
							<spring:message code="createquot.outstandingquoterequests"/> (${command.quoteRequestedList.resultsCount})
						</a></li>
						<li><a href="#" id="awaitingcompletion-link" onclick="event.preventDefault(); switchMenuFocus(menuElements1, 'awaitingcompletion-tab', false); " class="${ command.tabIndex == 1 ? 'selected':'' }">
							<spring:message code="createquot.awaitingcompletionissue"/> (${command.quotationsRequiringAction.resultsCount})
						</a></li>
						<li><a href="#" id="recentlyissued-link" onclick="event.preventDefault(); switchMenuFocus(menuElements1, 'recentlyissued-tab', false); ">
							<spring:message code="createquot.issued"/> (${command.quotationsIssued.size()})
						</a></li>
					</ul>
					<!-- div to hold all content which is to be changed using the tabbed submenu -->
					<div class="tab-box">
						<div id="quoterequest-tab" class="${ command.tabIndex == 0 ? '':'hid' }">
						<form:form name="command" method="post" id="quoterequestform">
							<div>
							
								<label style=" width: 120px; "><spring:message code="subdiv"/>:</label>
								<select class="subDivSelector" name="quoteRequesteSubdivId" style="width: 235px;" onchange="submitSearchForm('quoterequestform');" >
								<!-- subDivSelectorOnChange('#quoterequest-tab', sourcedByContBySubdivRequest); -->
									<option value="0"> -- <spring:message code="all"/> -- </option>
								</select>
								<div class="clear"></div>
								
								<label style=" width: 120px; "><spring:message code="createquot.assignedto"/>:</label>
								<select class="sourcedBySelector" name="quoteRequesteContactId" style="width: 235px;" onchange="submitSearchForm('quoterequestform');">
								<!--  filterQuotations('#quoterequest-tab'); -->
									<option value="0"> -- <spring:message code="all"/> -- </option>
								</select>
								<div class="clear"></div>
							</div>

							<t:showResultsPagination rs="${command.quoteRequestedList}" pageNoId="quoteRequestedListCurrentPage" />
							<input type="hidden" name="quoteRequestedList.currentPage" id="quoteRequestedListCurrentPage" value="${ command.quoteRequestedList.currentPage }"/>
							<input type="hidden" name="tabIndex" value="0"/>
							<input type="hidden" name="pagination" value="true"/>
							</form:form>
							<table id="table1" class="default2 outstandingQuoteRequests">
								<thead>
									<tr>
										<td colspan="12">
											<spring:message code="createquot.outstandingquotationrequests"/> (${command.quoteRequestedList.resultsCount})
										</td>
									</tr>
									<tr>
										<th	class="webtype" scope="col"><spring:message code="type"/></th>
										<th class="webqno" scope="col"><spring:message code="createquot.requestno"/></th>  
										<th class="webcomp" scope="col"><spring:message code="company"/></th>
										<th class="websubdiv" scope="col"><spring:message code="subdiv"/></th>   
										<th class="webcontact" scope="col"><spring:message code="contact"/></th>  
										<th	class="websource" scope="col"><spring:message code="createquot.source"/></th>
										<th	class="webregdate" scope="col"><spring:message code="createquot.reqdate"/></th>
										<th	class="webduedate" scope="col"><spring:message code="quotrequest.duedates"/></th>
										<th	class="webview" scope="col"><spring:message code="createquot.view"/></th>
										<th	class="webedit" scope="col"><spring:message code="edit"/></th>
										<th	class="webaction" scope="col"><spring:message code="createquot.action"/></th>
										<th	class="webdel" scope="col"><spring:message code="createquot.del"/></th>														
									</tr>
								</thead>
								<tfoot>
									<tr>
										<td colspan="12">&nbsp;</td>
									</tr>
								</tfoot>
								<tbody>
									<c:set var="rowcount" value="3000"/>
									<c:choose>
										<c:when test="${command.quoteRequestedList.results.size() > 0}">
											<c:forEach var="qr" items="${command.quoteRequestedList.results}">
												<!-- entries in this list can be quotes or quote request -->
												<c:choose>
													<c:when test="${qr.isQuote()}">
														this is a quotation
														<tr id="wq${qr.id}"  sourcedBy="${qr.sourcedBy.personid}" subdiv="${qr.sourcedBy.subdiv.subdivid}" onclick="window.location.href='<c:url value="/viewquotation.htm?id=${qr.id}" />'" onmouseover=" $j(this).addClass('hoverrow'); " onmouseout=" $j(this).removeClass('hoverrow'); ">
															<td class="webtype bold">${qr.quoteType.displayName}</td>
															<td class="webqno">
																<links:quoteLinkInfo quoteId="${qr.id}" quoteNo="${qr.qno}" quoteVer="${qr.ver}" showversion="true" rowcount="${rowcount}"/>
															</td>
															<td class="webcomp">
																<links:companyProjectionLinkInfo company="${qr.contactDto.subdiv.company}" rowcount="${rowcount}" copy="false"/></td>
															<td class="websubdiv">
																<links:subdivProjectionLinkInfo subdiv="${qr.contactDto.subdiv}" rowcount="${rowcount}" /></td>
															<td class="webcontact">
																<links:contactProjectionLinkInfo contact="${qr.contactDto}" rowcount="${rowcount}"/></td>
															<td class="websource">
																${qr.quotestatus}
															</td>
															<td class="webregdate"><fmt:formatDate value="${qr.reqdate}" type="date" dateStyle="SHORT"/></td>
															<td class="webduedate"><fmt:formatDate value="${qr.duedate}" type="date" dateStyle="SHORT"/></td>
															<td class="webview">N/A</td>
															<td class="webedit">N/A</td>
															<td class="webaction">N/A</td>
															<td class="webdel">N/A</td>
														</tr>
													</c:when>
													<c:otherwise>
														<!-- this is a quote request -->
														<tr id="wr${qr.id}" sourcedBy="${qr.loggedBy.personid}" subdiv="${qr.loggedBy.subdiv.subdivid}">
															<td class="webtype bold">${qr.quoteType.displayName}</td>
															<td class="webqno">${qr.requestNo}</td>
															<td class="webcomp">
																<c:choose>
																	<c:when test="${qr.detailsSource == 'EXISTING'}">
																		<links:companyProjectionLinkInfo company="${qr.comp}" rowcount="${rowcount}" copy="false"/>
																		<c:set var="delCompName" value="${qr.comp.coname}"/>
																	</c:when>
																	<c:otherwise>
																		${qr.company}
																		<c:set var="delCompName" value="${qr.company}"/>
																	</c:otherwise>
																</c:choose>
															</td>
															<td class="websubdiv">
																<c:choose>
																	<c:when test="${qr.detailsSource == 'EXISTING'}">
																		<links:subdivProjectionLinkInfo subdiv="${qr.con.subdiv}" rowcount="${rowcount}"/>
																	</c:when>
																	<c:otherwise>
																		${qr.con.subdiv.subname}
																	</c:otherwise>
																</c:choose>
															</td>
															<td class="webcontact">
																<c:choose>
																	<c:when test="${qr.detailsSource == 'EXISTING'}">
																		<links:contactProjectionLinkInfo contact="${qr.con}" rowcount="${rowcount}"/>
																	</c:when>
																	<c:otherwise>
																		${qr.contact}
																	</c:otherwise>
																</c:choose>
															</td>
															<td class="websource">${qr.logSource.viewName}</td>
															<td class="webregdate"><fmt:formatDate value="${qr.reqdate}" type="date" dateStyle="SHORT"/></td>
															<td class="webduedate"><fmt:formatDate value="${qr.duedate}" type="date" dateStyle="SHORT"/></td>
															<td class="webview">
																<a href="#" onclick=" event.preventDefault(); loadScript.createOverlay('external', 'View Quotation Request - ${qr.requestNo}', 'viewquotationrequest.htm?id=${qr.id}', null, null, 64, 600, null); " title="">
																	<img src="img/icons/view_details.png" width="16" height="16" alt="View Quotation Request Details" title="View Quotation Request Details" />
																</a>
															</td>
															<td class="webedit">
																<a href="#" onclick=" event.preventDefault(); loadScript.createOverlay('external', 'Edit Quotation Request - ${qr.requestNo}', 'quotationrequest.htm?id=${qr.id}', null, null, 76, 600, null); " title="">
																	<img src="img/icons/edit_details.png" width="16" height="16" alt="Edit Quotation Request Details" title="Edit Quotation Request Details" />
																</a>
															</td>
															<td class="webaction">
																<a href="<c:url value="/quotationrequesttoquotation.htm?id=${qr.id}"/>" class="mainlink"><spring:message code="createquot.action"/></a>
															</td>
															<td class="webdel">
																<a href="#" onclick=" event.preventDefault(); deleteQuoteRequest('wr', ${qr.id}, '${delCompName}'); ">
																	<img src="img/icons/delete.png" width="16" height="16" alt="Delete Quote Request" title="Delete Quote Request" />
																</a>
															</td>
														</tr>
													</c:otherwise>
												</c:choose>
												<c:set var="rowcount" value="${rowcount + 1}"/>
											</c:forEach>
										</c:when>
										<c:otherwise>
											<tr>
												<td colspan="12" class="bold text-center">
													<spring:message code="createquot.noquotationrequeststodisplay"/>
												</td>
											</tr>
										</c:otherwise>
									</c:choose>
								</tbody>
							</table>
						</div>
						<div id="awaitingcompletion-tab" class="${ command.tabIndex == 1 ? '':'hid' }">
							<form:form name="command" method="post" id="awaitingcompletionform">
							<div>
								<label style=" width: 120px; "><spring:message code="subdiv"/>:</label>
								<select class="subDivSelector" name="quoteSubdivId" style="width: 235px;" onchange="submitSearchForm('awaitingcompletionform');" >
									<option value="0"> -- <spring:message code="all"/> -- </option>
								</select>
<!-- 								subDivSelectorOnChange('#awaitingcompletion-tab',sourcedByContBySubdivAC); -->
								<div class="clear"></div>
								
								<label style=" width: 120px; "><spring:message code="createquot.sourcedby"/>:</label>
								<select class="sourcedBySelector" name="quoteContactId" style="width: 235px;" onchange="submitSearchForm('awaitingcompletionform');">
									<option value="0"> -- <spring:message code="all"/> -- </option>
								</select>
<!-- 								filterQuotations('#awaitingcompletion-tab'); -->
								<div class="clear"></div>
							</div>
							
							<t:showResultsPagination rs="${command.quotationsRequiringAction}" pageNoId="quotationsRequiringActionCurrentPage" />
							<input type="hidden" name="quotationsRequiringAction.currentPage" id="quotationsRequiringActionCurrentPage" value="${ command.quotationsRequiringAction.currentPage }"/>
							<input type="hidden" name="tabIndex" value="1"/>
							<input type="hidden" name="pagination" value="true"/>
							</form:form>
							<table class="default2 awaitingCompAndIssue">
								<thead>
									<tr>
										<td colspan="8">
											<spring:message code="createquot.quotationsawaitingcompletionandissue"/><span class="quotationCount" >(${command.quotationsRequiringAction.resultsCount})</span>
										</td>
									</tr>
									<tr>
										<th class="caiqno" scope="col"><spring:message code="quoteno"/></th>  
										<th class="caicomp" scope="col"><spring:message code="createquot.companyclientref"/></th>
										<th class="caisubdiv" scope="col"><spring:message code="subdiv"/></th>   
										<th class="caicontact" scope="col"><spring:message code="contact"/></th>  
										<th	class="caistatus" scope="col"><spring:message code="status"/></th>
										<th class="caisourcedby" scope="col"><spring:message code="createquot.sourcedby"/></th>
										<th	class="cairegdate" scope="col"><spring:message code="createquot.reqdate"/></th>
										<th	class="caiduedate" scope="col"><spring:message code="quotrequest.duedates"/></th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<td colspan="8">&nbsp;</td>
									</tr>
								</tfoot>
								<tbody>
									<c:set var="rowcount" value="1000"/>
									<c:choose>
										<c:when test="${command.quotationsRequiringAction.results.size() > 0}">
											<c:forEach var="quotation" items="${command.quotationsRequiringAction.results}">
												<c:url var="quoteUrl" value="/viewquotation.htm?id=${quotation.id}"/>
													<c:choose>
														<c:when test="${quotesAllowIssue.contains(quotation.id)}" >
															<c:set var="isGreen" value="style=\"background-color: #99ff99;\""></c:set>
														</c:when>
														<c:otherwise>
															<c:set var="isGreen" value=""></c:set>
														</c:otherwise>
													</c:choose>
												<tr sourcedBy="${quotation.sourcedBy.personid}" subdiv="${quotation.sourcedBy.subdiv.subdivid}" onclick="window.location.href='${quoteUrl}'" ${isGreen} onmouseover=" $j(this).addClass('hoverrow'); " onmouseout=" $j(this).removeClass('hoverrow'); ">
													<td class="caiqno">
														<links:quoteLinkInfo quoteId="${quotation.id}" quoteNo="${quotation.qno}" 
														quoteVer="${quotation.ver}" showversion="true" rowcount="${rowcount}"/>
													</td>
													<td class="caicomp">
														<links:companyProjectionLinkInfo company="${quotation.contactDto.subdiv.company}" rowcount="${rowcount}" copy="false"></links:companyProjectionLinkInfo>
														<c:set var="linkClass" value="jconTip mainlink-strike" />

														<c:if test="${quotation.clientref != null && quotation.clientref != ''}">
															(${quotation.clientref})
														</c:if>
													</td>
													<td class="caisubdiv">
														<links:subdivProjectionLinkInfo subdiv="${quotation.contactDto.subdiv}" rowcount="${rowcount}" />
													</td>
													<td class="caicontact">
														<links:contactProjectionLinkInfo contact="${quotation.contactDto}" rowcount="${rowcount}"/>
													</td>
													<td	class="caistatus">
														${quotation.quotestatus}
													</td>
													<td class="caisourcedby">
														${quotation.sourcedSubCode} -
														<links:contactProjectionLinkInfo contact="${quotation.sourcedBy}" rowcount="0"/>
													</td>
													<td	class="cairegdate"><fmt:formatDate value="${quotation.regdate}" type="date" dateStyle="SHORT"/></td>
													<td	class="caiduedate"><fmt:formatDate value="${quotation.duedate}" type="date" dateStyle="SHORT"/></td>
												</tr>
												<c:set var="rowcount" value="${rowcount + 1}"/>
											</c:forEach>
										</c:when>
										<c:otherwise>
											<tr>
												<td colspan="8" class="bold text-center">
													<spring:message code="createquot.noquotationsawaitingcompletionandissue"/>
												</td>
											</tr>
										</c:otherwise>
									</c:choose>
								</tbody>
							</table>
						</div>
						<div id="recentlyissued-tab" class="hid">
						
							<div>
								<label style=" width: 120px; "><spring:message code="subdiv"/>:</label>
								<select class="subDivSelector" style="width: 235px;" onchange="subDivSelectorOnChange('#recentlyissued-tab',sourcedByContBySubdivIssued);" >
									<option value="0"> -- <spring:message code="all"/> -- </option>
								</select>
								<div class="clear"></div>
								
								<label style=" width: 120px; "><spring:message code="createquot.sourcedby"/>:</label>
								<select class="sourcedBySelector" style="width: 235px;" onchange="filterQuotations('#recentlyissued-tab');">
									<option value="0"> -- <spring:message code="all"/> -- </option>
								</select>
								<div class="clear"></div>
							</div>
						
							<table class="default2 recentlyIssued">
								<thead>
									<tr>
										<td colspan="7">
											<spring:message code="createquot.recentlyissued"/><span class="quotationCount">(${command.quotationsIssued.size()})</span>
										</td>
									</tr>
									<tr>
										<th class="riqno" scope="col"><spring:message code="quoteno"/></th>  
										<th class="ricomp" scope="col"><spring:message code="createquot.companyclientref"/></th>
										<th class="risubdiv" scope="col"><spring:message code="subdiv"/></th>   
										<th class="ricontact" scope="col"><spring:message code="contact"/></th>  
										<th	class="ristatus" scope="col"><spring:message code="status"/></th>
										<th	class="riissuedby" scope="col"><spring:message code="createquot.issuedby"/></th>
										<th	class="riissuedate" scope="col"><spring:message code="createquot.issuedate"/></th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<td colspan="7">&nbsp;</td>
									</tr>
								</tfoot>
								<tbody>
									<c:set var="rowcount" value="4000"/>
									<c:choose>
										<c:when test="${command.quotationsIssued.size() > 0}">
											<c:forEach var="quotation" items="${command.quotationsIssued}">
												<tr sourcedBy="${quotation.sourcedBy.personid}" subdiv="${quotation.sourcedBy.subdiv.subdivid}" onclick="window.location.href='<c:url value="/viewquotation.htm?id=${quotation.id}" />'" onmouseover=" $j(this).addClass('hoverrow'); " onmouseout=" $j(this).removeClass('hoverrow'); ">
													<td class="riqno"><links:quoteLinkInfo quoteId="${quotation.id}" quoteNo="${quotation.qno}" 
													quoteVer="${quotation.ver}" showversion="true" rowcount="${rowcount}"/></td>
													<td class="ricomp">
														<links:companyProjectionLinkInfo company="${quotation.contactDto.subdiv.company}" rowcount="${rowcount}" copy="false"/>
														<c:if test="${quotation.clientref != null && quotation.clientref != ''}">
															(${quotation.clientref})
														</c:if>
													</td>
													<td class="risubdiv">
														<links:subdivProjectionLinkInfo subdiv="${quotation.contactDto.subdiv}" rowcount="${rowcount}" />
													</td>
													<td class="ricontact">
														<links:contactProjectionLinkInfo contact="${quotation.contactDto}" rowcount="${rowcount}"/>
													</td>
													<td	class="ristatus">
														${quotation.quotestatus}
													</td>
													<td	class="riissuedby">
														${quotation.issuebySubCode} -
														<links:contactProjectionLinkInfo contact="${quotation.issueby}" rowcount="${rowcount}"/>
													</td>
													<td	class="riissuedate"><fmt:formatDate value="${quotation.issuedate}" type="date" dateStyle="SHORT"/></td>
												</tr>
												<c:set var="rowcount" value="${rowcount + 1}"/>
											</c:forEach>
										</c:when>
										<c:otherwise>
											<tr>
												<td colspan="7" class="bold text-center">
													<spring:message code="createquot.noquotationsissued"/> 
												</td>
											</tr>
										</c:otherwise>
									</c:choose>
								</tbody>
							</table>
						</div>
					</div>
					<!-- end of div to hold all tabbed submenu content -->
				</div>
				<!-- end of div to hold tabbed submenu -->
				<!-- this div clears floats and restores page flow -->
				<div class="clear"></div>
			</div>
			<div id="searchquote-tab" class="hid">
				<cwms:securedLink permission="QUOTATION_METRIC"  classAttr="mainlink-float" collapse="True" >
					<spring:message code="createquot.metrics"/>
				</cwms:securedLink>
				<!-- this div clears floats and restores page flow -->
				<div class="clear"></div>
				<form:form path="command" method="post" action="" id="searchquoteform">
					<fieldset>
						<legend><spring:message code="createquot.searchforquotation"/></legend>
						<ol>
							<!-- this list item contains the company search plugin -->
							<li>
								<label><spring:message code="createquot.companysearch"/>:</label>
								<div class="float-left">
									<!-- this div creates a new company search plugin  -->
									<div class="compSearchJQPlugin">
										<input type="hidden" name="field" value="coid" />
										<input type="hidden" name="compCoroles" value="client,prospect,business" />
										<input type="hidden" name="tabIndex" value="1" />
									</div>
								</div>
								<a href="" class="mainlink-float" onclick="event.preventDefault(); $j('li.advancedOptions').removeClass('hid'); "><spring:message code="createquot.showadvancedoptions"/></a>
								<!--  clear floats and restore page flow  -->
								<div class="clear"></div>
							</li>
							<!-- this list item contains the contact search plugin -->
							<li>
								<label><spring:message code="createquot.contactsearch"/>:</label>
								<!-- this div creates a new contact search plugin and the hidden input passes
									 a comma separated list of coroles to search over, no input field causes
									 searches over client corole as default -->
								<div id="contSearchPlugin">
									<input type="hidden" id="contCoroles" value="client,prospect" />
									<input type="hidden" id="contindex" value="2" />
								</div>
								<!--  clear floats and restore page flow  -->
								<div class="clear"></div>
							</li>	
							
							<li> 
							 	<form:label path="sourcedBySubdivId"> <spring:message code="quotaddinst.selectsubdivision"/>:</form:label>
							 	<form:select path="sourcedBySubdivId">
							 		<form:option value=""><spring:message code="createquot.any"/></form:option>
<%-- 							 		<form:options items="${subdivsOfLoggedinCompany }" itemLabel="subname" itemValue="subdivid"  /> --%>
							 		<c:forEach var="subdiv" items="${subdivsOfLoggedinCompany}">
											<option value="${subdiv.subdivid}" <c:if test="${subdiv.subdivid == currentSubdiv.key }"> selected</c:if>>${subdiv.subname}</option>
									</c:forEach>
							 	</form:select>
							 </li>
																	
							<li>
								<form:label path="clientref"><spring:message code="clientref"/>:</form:label>
								<form:input path="clientref" tabindex="4"/>
							</li>
							<li>
								<form:label path="qno"><spring:message code="createquot.quotationno"/>:</form:label>
								<form:input path="qno" tabindex="5"/> 
							</li>
							<!-- mfrid descid -->
							<li>
									<form:label path="mfr"><spring:message code="createquot.mfr"/>:</form:label>
									<!-- float div left -->
									<div class="float-left">
										<!-- this div creates a new manufacturer jquery search plugin -->	
										<div class="mfrSearchJQPlugin">
											<input type="hidden" name="fieldId" value="mfrid" />
											<input type="hidden" name="fieldName" value="mfr" />
											<input type="hidden" name="textOnlyOption" value="true" />
											<input type="hidden" name="tabIndex" value="6" />
											<input type="hidden" name="mfrNamePrefill" value="${command.mfr}" />
											<input type="hidden" name="mfrIdPrefill" value="${command.mfrid}" />
											<!-- manufacturer results appear here -->
										</div>
									</div>
								<!-- clear floats and restore pageflow -->
								<div class="clear-0"></div>
							</li>
							<li>
									<label path="model"><spring:message code="model"/>:</label>
									<input path="model" type="text" tabindex="7" />
							</li>
							<li>
								<label for="${status.expression}"><spring:message code="sub-family"/>:</label>												
								<!-- float div left -->
								<div class="float-left">
									<form:input path="description" name="description" tabindex="8"/>
									<form:input path="descid" type="hidden"/>						
								</div>
								<!-- clear floats and restore pageflow -->
								<div class="clear-0"></div>
							</li>
							<li>
									<form:label path="plantid"><spring:message code="barcode"/>:</form:label>
									<form:input type="text" path="plantid" tabindex="9" />
							</li>
							<li>
									<form:label path="serialno"><spring:message code="serialnumber"/>:</form:label>
									<form:input path="serialno" type="text" tabindex="5" />
							</li>
							<li>
									<form:label path="plantno"><spring:message code="plantno"/>:</form:label>
									<form:input path="plantno" type="text" tabindex="6" />
							</li>
							<li>
									<form:label path="quotestatus"><spring:message code="status"/>:</form:label>
									<spring:message var="anyStatus" code="createquot.any"/>
									<form:select path="quotestatus" tabindex="9">
										<form:option value="">${anyStatus}</form:option>
										<form:options items="${quotestatus}" itemLabel="name" itemValue="id"/>
									</form:select>
							</li>
							<li>
									<form:label path="resultsYearFilter"><spring:message code="createquot.resultsfromlast"/>:</form:label>
									<form:select path="resultsYearFilter"  tabindex="10">
										<form:option value=""><spring:message code="quotaddinst.all"/></form:option>
										<form:options items="${resultsyears}"/>
									</form:select>
								<spring:message code="createquot.years"/>
							</li>
							<li>
								<form:label path="issueDateBetween"><spring:message code="createquot.issuedate"/>:</form:label>
									<form:select path="issueDateBetween" tabindex="11" onchange="event.preventDefault(); changeDateOption(this.value, 'issueDateSpan'); ">
										<option value="false" ${!status.value ? "selected" : ""}><spring:message code="createquot.on"/></option>
										<option value="true" ${status.value ? "selected" : ""}><spring:message code="createquot.between"/></option>
									</form:select>
									<form:input type="date" path="issueDate1" class="date" tabindex="12" />
								<span id="issueDateSpan" <c:choose><c:when test="${command.issueDateBetween}">class="vis"</c:when><c:otherwise>class="hid"</c:otherwise></c:choose>>
									<spring:message code="createquot.and"/>&nbsp;
										<form:input type="date" path="issueDate2" class="date" tabindex="13" />
								</span>
							</li>
							<li>
								<form:label path="expiryDate1"><spring:message code="createquot.expirydate"/>:</form:label>
									<form:select path="expiryDateBetween" tabindex="14" onchange="event.preventDefault(); changeDateOption(this.value, 'expiryDateSpan'); ">
										<option value="false" ${!status.value ? "selected" : ""}><spring:message code="createquot.on"/></option>
										<option value="true" ${status.value ? "selected" : ""}><spring:message code="createquot.between"/></option>
									</form:select>
									<form:input type="date" path="expiryDate1" class="date" tabindex="15" />
								<span id="expiryDateSpan" <c:choose><c:when test="${command.expiryDateBetween}">class="vis"</c:when><c:otherwise>class="hid"</c:otherwise></c:choose>>
									<spring:message code="createquot.and"/>&nbsp;
										<form:input type="date" path="expiryDate2" class="date" tabindex="16" />
								</span>
							</li>											
							<li class="advancedOptions hid">
								<label for="advancedOptions"><spring:message code="createquot.advancedoptions"/>: </label>
								<div class="float-left">
									<div class="float-left">
											<form:label path="createdByIds"><spring:message code="createdby"/>: </form:label><br/>
											<form:select path="createdByIds"  multiple="multiple" tabindex="17" size="7">
												<form:option value=""><spring:message code="createquot.any"/></form:option>
												<form:options itemLabel="name" itemValue="personid" items="${businesscons}"/>
											</form:select>
									</div>
									<div class="float-left">
											<form:label path="issuedByIds"><spring:message code="createquot.issuedby"/>: </form:label><br/>
											<form:select path="issuedByIds" multiple="multiple" tabindex="18" size="7">
												<form:option value=""><spring:message code="createquot.any"/></form:option>
												<form:options itemLabel="name" itemValue="personid" items="${businesscons}"/>
											</form:select>
									</div>
									<div class="float-left">
											<form:label path="sourcedByIds"><spring:message code="createquot.sourcedby"/>: </form:label><br/>
											<form:select path="sourcedByIds"  multiple="multiple" tabindex="19" size="7">
												<form:option value=""><spring:message code="createquot.any"/></form:option>
												<form:options itemLabel="name" itemValue="personid" items="${businesscons}"/>
											</form:select>
									</div>
									<div class="clear-0"></div>
									(<spring:message code="createquot.holdcontroltoselectmultiple"/>)	
								</div>
								<!-- clear floats and restore pageflow -->
							</li>
							<li>
								<label for="submit">&nbsp;</label>
								<input type="submit" value="<spring:message code='submit'/>" name="submit" id="submit" tabindex="20" />
							</li>
						</ol>
					</fieldset>
				</form:form>			
			</div>
			<div id="createquote-tab" class="hid">
				<!-- this div creates a new multi contact search plugin and the hidden input 'multiContCoroles'
				 	 passes a comma separated list of coroles to search over, no input field causes searches over 
				 	 client corole as default. The 'anchorRedirect' input field should contain the page name to which
				 	 you want the personid to be passed.  -->
				<div id="multiContSearchPlugin">
					<input type="hidden" id="multiContCoroles" value="client,prospect,business" />
					<input type="hidden" id="anchorRedirect" value="quoteform" />
					<input type="hidden" id="businessCoId" value="${allocatedCompany.key}" />
				</div>
			</div>
		</div>
	</jsp:body>
</t:crocodileTemplate>