<%-- File name: /trescal/core/_PATH1_/_PATH2_/_FILE_.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
		<span class="headtext"><spring:message code="quotmetric.quotationmetrics" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
		<link rel="stylesheet" href="styles/quotationmetrics.css" type="text/css" />
    </jsp:attribute>
    <jsp:body>
			<div id="search" class="infobox">
				<form:form action="" method="post">
					<fieldset>

					<legend><spring:message code="quotmetric.yoursearchcomprisesquotes"/>:</legend>
					
					<div class="datefilter">
						<form:label path="regFrom"><spring:message code="quotmetric.createdbetween"/></form:label>
						<form:input type="date" path="regFrom"/>
						<span><spring:message code="createquot.and"/></span>
						<form:input type="date" path="regTo"/>
					</div>
					
					<div id="contactfilter">
						<div>
							<form:label path="sourcedByIds"><spring:message code="createquot.sourcedby"/>: </form:label>
	 						<form:select path="sourcedByIds" multiple="multiple" size="6" items="${command.businessContacts}" itemValue="key" itemLabel="value"/>
							(<spring:message code="createquot.holdcontroltoselectmultiple"/>)
						</div>
				
					<div>
							<form:label path="createdByIds"><spring:message code="createdby"/>: </form:label>
	 						<form:select path="createdByIds"  multiple="multiple" size="6" items="${command.businessContacts}" itemValue="key" itemLabel="value"/>
	 						(<spring:message code="createquot.holdcontroltoselectmultiple"/>)
							
						</div>
						
						<div>
							<form:label path="issuedByIds"><spring:message code="createquot.issuedby"/>: </form:label>
							<form:select path="issuedByIds" multiple="multiple" size="6"  items="${command.businessContacts}" itemValue="key" itemLabel="value"/>
							(<spring:message code="createquot.holdcontroltoselectmultiple"/>)
						</div>
					</div>
					
					<input type="submit" name="submit" value="<spring:message code='search'/>"/>
					</fieldset>
				</form:form>
			</div>
			
			<hr />
			
			<c:set var="m" value="${command.metrics}"/>
			<div id="metrics">
				<strong>${m.quotationCount}&nbsp;</strong><spring:message code="quotmetric.quotationsmatchyoursearch"/>.
				<br /><br />
				<spring:message code="quotmetric.ofthese"/>, ${m.issued} (${m.percentIssued}%) <spring:message code="quotmetric.havebeenissued"/>, ${m.notIssued} (${m.percentNotIssued}%) <spring:message code="quotmetric.havenot"/>.
				<br /><br />
				<spring:message code="quotmetric.ofthoseissued"/>, ${m.accepted} (${m.percentAccepted}%) <spring:message code="quotmetric.havebeenaccepted"/>, ${m.notAccepted} (${m.percentNotAccepted}%) <spring:message code="quotmetric.havenot"/>. 
			</div>

			<div id="quotes">
			
			</div>
    </jsp:body>
</t:crocodileTemplate>