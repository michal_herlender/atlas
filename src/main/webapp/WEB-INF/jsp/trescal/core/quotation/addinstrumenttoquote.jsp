<%-- File name: /trescal/core/quotation/addinstrumenttoquote.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="instrument" tagdir="/WEB-INF/tags/instrument" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel" %>
<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="quotaddinst.addinstrumentstoquotation"/>&nbsp;<c:out value="(${quotation.qno})"/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/quotation/AddInstrumentToQuote.js'></script>

		<script type='text/javascript'>
		
		var headings = [];
		<c:forEach var="qh" items="${headings}">
			headings.push({headingId: ${qh.headingId}, headingName: '${qh.headingName}'});
		</c:forEach>
		
		var caltypes = [];
		<c:forEach var="ct" items="${caltypes}">
			caltypes.push({calTypeId: ${ct.key}, calTypeName: '${ct.value}'});
		</c:forEach>
		
		</script>
    </jsp:attribute>
    <jsp:body>

	<!-- error section displayed when form submission fails -->
	<t:showErrors path="form.*" />
	<!-- end error section -->
	
	<form:form id="viewCompanyInstrumentsForm" method="post" modelAttribute="form" onsubmit=" showUnLoadWarning = false; " >
		<form:hidden path="quotationId" />
		<form:hidden path="search.compId" />
		<form:hidden path="search.pageNo" id="pageNo" />
		<form:hidden path="search.resultsPerPage" />
	
	<!-- infobox contains company instrument search form elements and is styled with nifty corners -->	
	<div class="infobox" id="search">
	
		<!-- this shows any quotation request information that the user has provided on the website -->
		<c:if test="${quotation.quotationRequests.size() > 0}">
			<fieldset>
				<legend><spring:message code="quotaddinst.quotationrequestslinkedtothisquotation"/> (${quotation.quotationRequests.size()})</legend>
				<ol>
					<li>
						<c:forEach var="qrequest" items="${quotation.quotationRequests}">
							${qrequest.requestInfo.replace(",", "<br />")}																							
						</c:forEach>
					</li>
				</ol>
			</fieldset>
		</c:if>
		<!-- end of quotation request info -->					
		
		<fieldset class="searchCompanyInstruments">
			
			<legend><spring:message code="quotaddinst.quotationinstrumentbrowser"/> (<a href="<spring:url value='viewquotation.htm?id=${quotation.id}'/>" class="mainlink">${quotation.qno} version ${quotation.ver}</a>)</legend>
			
			<!-- column with width 50% (left) -->
			<div class="displaycolumn">
			
				<ol class="instSearchColumn">
					
					<li>
						<fieldset id="companySearchText">
							<legend <c:if test="${form.search.searchAllCompanies}"> class="hid" </c:if>>
									<c:out value="${totalinstrumentcount} ${quotation.contact.sub.comp.coname} " /> 
								<c:choose>
									<c:when test="${totalinstrumentcount != 1}">
										<spring:message code="quotaddinst.availableinstruments"/> 
									</c:when>
									<c:otherwise>
									 	<spring:message code="quotaddinst.availableinstrument"/>
									</c:otherwise>
								</c:choose>
							</legend>
							<c:if test="${form.search.searchAllCompanies}">
								<legend class="attention"><spring:message code="quotaddinst.searchinginstrumentsforallcompanies"/></legend>
							</c:if>
						</fieldset>
					</li>
					<li>
						<label><spring:message code="barcode"/>:</label>
						<form:input path="search.barcode" value="${form.search.barcode}" tabindex="1" />
						<form:errors cssClass="attention" path="search.barcode"/>
					</li>
						<li>												
						<label><spring:message code="sub-family"/>:</label>
						<!-- float div left -->
						<div class="float-left">
							<form:input path="search.descNm" id="descNm" value="${form.search.descNm}" tabindex="4" />
							<form:hidden path="search.descId" id="descId" value="${form.search.descId}" />
						</div>																							
						<!-- clear floats and restore pageflow -->
						<div class="clear-0"></div>											
					</li>																																		
					<li>													
						<label><spring:message code="manufacturer"/>:</label>
						<!-- float div left -->
						<div class="float-left">
							<form:input path="search.mfrNm" id="mfrNm" value="${form.search.mfrNm}" tabindex="4" />
							<form:hidden path="search.mfrId" id="mfrId" value="${form.search.mfrId}" />
						</div>																							
						<!-- clear floats and restore pageflow -->
						<div class="clear-0"></div>
					</li>											
					<li>
						<label><spring:message code="model"/>:</label>
						<form:input path="search.model" value="${form.search.model}" tabindex="3" />
					</li>
					<li> 
						<label><spring:message code="viewinstrument.customerdescription"/></label>
						<form:input path="search.customerDescription" value="${form.search.customerDescription}" tabindex="3" />
					</li>
					
					<li>
						<label><spring:message code="quotaddinst.orderresultsby"/>:</label>
						<form:select path="sortType" items="${sortTypeList}" itemLabel="value" itemValue="key" />
						<form:select path="ascOrder" items="${ascOrderList}" itemLabel="value" itemValue="key" />
					</li>												
											
					<li>
						<input type="hidden" name="submitType" id="submitType" value="" />
						<label><input type="button" value="<spring:message code='quotaddinst.reset'/>" onclick=" event.preventDefault(); window.location.href = 'addinstrumenttoquote.htm?id=${quotation.id}'; " /></label>
						<!-- Note, only one submit is provided on the form (immediately below), the buttonAdd triggers this single submit -->
						<input type="submit" name="submitSearch" id="submitSearch" value="<spring:message code='search'/>" tabindex="11" />
					</li>
					
				</ol>
			
			</div>
			<!-- end of left column -->
			
			<!-- column with width 50% (left) -->
			<div class="displaycolumn">
			
				<ol>
				
					<li>
						<label><spring:message code="serialno"/>:</label>
						<form:input path="search.serialNo" value="${form.search.serialNo}" tabindex="5" />
					</li>
					<li>
						<label><spring:message code="plantno"/>:</label>
						<form:input path="search.plantNo" value="${form.search.plantNo}" tabindex="6" />
					</li>	
					<li>
						<label><spring:message code="quotaddinst.searchallcompanies"/>:</label>
						<div class="float-left">
							<form:checkbox path="search.searchAllCompanies" tabindex="11" onclick=" searchAllCompanyInstruments(this, this.checked); " />
 						</div>
						<div class="clear"></div>
					</li>
					<li>
						<c:set var="disabled" value="false"/>
						<c:if test="${form.search.searchAllCompanies == true}"> <c:set var="disabled" value="true"/> </c:if>
						<label><spring:message code="subdivision"/>:</label>
						<form:select path="subdivid" class="width70" onchange=" getActiveSubdivAddrsAndConts(this.value, $j(this).parent().next().find('select'), $j(this).parent().next().next().find('select'), true, ''); return false; " tabindex="7" 
						 disabled ="${disabled}">
							<option value="" selected="selected"><spring:message code="quotaddinst.searchallsubdivisions"/></option>
							<c:forEach var="subdiv" items="${sublist}">
								<c:set var="selectedText" value="" />
								<c:if test="${form.search.searchAllCompanies == false}"> 
									<c:choose>
										<c:when test="${not empty form.subdivid}">
											<c:if test="${form.subdivid == subdiv.key}"> selected="selected" </c:if>
									 	</c:when>
									 	<c:otherwise>
											<c:if test="${quotation.contact.sub.subdivid == subdiv.key}"> selected="selected" </c:if>
								   	 	</c:otherwise>
								   	 </c:choose>
							   	</c:if>
								<option value="${subdiv.key}"
							   	  title="${subdiv.value}">${subdiv.value}
							   	 </option>
							</c:forEach>
						</form:select>
					</li>
					<li>
						<label><spring:message code="address"/>:</label>
						<form:select path="addressid" class="width70" tabindex="8" disabled="${disabled}">
							<c:choose>
							   <c:when test="${not empty addlist}">
								<option value="" selected="selected"><spring:message code="quotaddinst.searchalladdresses"/></option>
								<c:forEach var="address" items="${addlist}">
									<option value="${address.key}" <c:if test="${form.search.searchAllCompanies == false}"></c:if>
									 <c:if test="${not empty form.addressid}"></c:if>
										  <c:if test="${form.addressid == address.key}"> selected="selected" </c:if>
									  	title="${address.value}"> ${address.value}
								   </option>
								</c:forEach>
							   </c:when>
							   <c:otherwise>
								<option value="" selected="selected"><spring:message code="quotaddinst.selectsubdivision"/></option>
							   </c:otherwise>
							</c:choose>
						</form:select>
					</li>
					<li>
						<label><spring:message code="contact"/>:</label>
						<form:select path="personid" class="width70" tabindex="9" disabled ="${disabled}">
							<c:choose>
							   <c:when test="${not empty conlist}">
								<option value="" selected="selected"><spring:message code="quotaddinst.searchallcontacts"/></option>
								<c:forEach var="contact" items="${conlist}">
									<option value="${contact.key}" <c:if test="${form.search.searchAllCompanies == false}"> </c:if>
									 <c:if test="${not empty form.personid}"> </c:if>
									   <c:if test="${form.personid == contact.key}"> selected="selected" </c:if> title="${contact.value}">${contact.value}</option>
								</c:forEach>
							   </c:when>
							   <c:otherwise>
								<option value="" selected="selected"><spring:message code="quotaddinst.selectsubdivision"/></option>
							   </c:otherwise>
							 </c:choose>
						</form:select>
					</li>												
				
					<li>
						<label><spring:message code="searchinstrument.incalibration"/>:</label>
						<form:select path="outOfCal" items="${outOfCalList}" itemValue="key" itemLabel="value" />
					</li>
				
				</ol>
			
			</div>
			<!-- end of left column -->
			
		</fieldset>
		
	</div>
	<!-- end of infobox div -->
	
	<!-- sub navigation menu which displays and hides sections of page as well as linking to external pages -->
	<div id="subnav">
		<dl>
			<dt><a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'searchresults-tab', false); " id="searchresults-link" title="<spring:message code='quotaddinst.viewsearchresults'/>" <c:if test="${not searchbasketTabSelected}">class="selected"</c:if>><spring:message code="quotaddinstmod.searchresults"/></a></dt>
			<dt><a href="#" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'searchbasket-tab', false); " id="searchbasket-link" title="<spring:message code='quotaddinst.viewitembasket'/>"<c:if test="${searchbasketTabSelected}">class="selected"</c:if>><spring:message code="quotaddinst.instrumentbasket"/>
			 (<span class="basketCount"><c:out value=" ${basketCount} " /></span>)</a></dt>
		</dl>
	</div>
	<!-- end of sub navigation menu -->
	
	<!-- displays instrument search results -->								
	<div id="searchresults-tab" <c:if test="${searchbasketTabSelected}">class="hid"</c:if>>
	
		<!-- infobox contains company instrument with nifty corners -->	
		<div class="infobox">
			<t:paginationControl resultset="${form.search.rs}"/>
			<table class="default4 quoteInstResults" id="quoteInstResultsSortId" summary="<spring:message code='quotaddinst.thistabledisplaysallcompanyinstrumentsreturnedbysearch'/>">
				<thead>
					<c:if test="${not empty form.search.rs.results}">
						<tr>
							<td colspan="6"><spring:message code="quotaddinst.yoursearchproduced"/>&nbsp;<span class="compInstResultSize">${form.search.rs.results.size()}</span> 
								<c:choose>
									<c:when test="${form.search.rs.results.size() != 1}">  <spring:message code="quotaddinst.results"/>
									</c:when><c:otherwise>
										 <spring:message code="quotaddinst.result"/> 
							    	</c:otherwise>
						    	</c:choose>
					    	</td>
						</tr>
					</c:if>
					<tr>
						<th class="add" scope="col">
							<a href="#" class="addAnchor" onclick=" toggleInstrumentInBasket(null, true, null, ${quotation.id}, '${currencyERSymbol}', ${allocatedSubdiv.key}); return false; " title="<spring:message code='quotaddinst.addinstrumentsbelowtobasket'/>">&nbsp;</a>
							<spring:message code="quotaddinst.all"/>
							<a href="#" class="removeBasketAnchor" onclick=" toggleInstrumentInBasket(null, false, null, ${quotation.id}, '${currencyERSymbol}', ${allocatedSubdiv.key}); return false; " title="<spring:message code='quotaddinst.removeanyinstrumentsbelowfrombasket'/>">&nbsp;</a>
						</th>
						<th class="barcode" scope="col"><spring:message code="barcode"/></th>
						<th class="desc" scope="col"><spring:message code="editquot.modelname"/></th>
						<th class="serialno" scope="col"><spring:message code="serialno"/></th>
						<th class="plantno" scope="col"><spring:message code="plantno"/></th>
						<th class="caldue" scope="col"><spring:message code="caldue"/></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="6">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
					<c:set var="rowcount" value="1" />
					<c:choose>
						<c:when test="${not empty form.search.rs.results}">
						<c:forEach var="inst" items="${form.search.rs.results}">
							<tr>
								<td class="add">
									<c:set var="inBasket" value="false"/>
									<c:if test="${not empty form.basketIds}">
										<c:if test="${form.basketIds.contains(inst.plantid) == true}">
											<c:set var="inBasket" value="true"/>
										</c:if>	
									</c:if>	
									<c:choose>
										<c:when test="${inBasket == false}">
											<a href="#" class="addAnchor" id="instAdd${inst.plantid}" onclick=" toggleInstrumentInBasket(this, true, ${inst.plantid}, ${quotation.id}, '${currencyERSymbol}', ${allocatedSubdiv.key}); return false; " title="<spring:message code='quotaddinst.addinstrumenttobasket'/>">&nbsp;</a>
										</c:when>
										<c:otherwise>
											<a href="#" class="removeBasketAnchor" id="instAdd${inst.plantid}" onclick=" toggleInstrumentInBasket(this, false, ${inst.plantid}, ${quotation.id}, '', ${allocatedSubdiv.key}); return false; " title="<spring:message code='quotaddinst.removeinstrumentfrombasket'/>">&nbsp;</a>
										</c:otherwise>
									</c:choose>
								</td>
								<td class="barcode"> <links:instrumentLinkDWRInfo instrument="${inst}" rowcount="0" displayBarcode="true" displayName="false" displayCalTimescale="true" /></td>
								<td class="desc">
									<instmodel:showInstrumentModelLink instrument="${inst}" caltypeid="0"/>
									</br>
									${inst.customerDescription}
								</td>
								<td class="serialno">${inst.serialno}</td>
								<td class="plantno">${inst.plantno}</td>
								<td class="caldue"><instrument:showFormattedCalDueDate instrument="${inst}" /></td>
							</tr>
							<c:set var="rowcount" value="${rowcount + 1}"/>
						</c:forEach>
						</c:when>
						<c:otherwise>												
						<tr>
							<td colspan="6" class="bold center">
								<spring:message code="noresults"/>
							</td>
						</tr>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
			<t:paginationControl resultset="${form.search.rs}"/>
			
		</div>
		<!-- end of infobox div -->
	
	</div>
	
	<div id="searchbasket-tab"  <c:if test="${not searchbasketTabSelected}">class="hid"</c:if>>
	
		<div class="infobox">
		
			<h5>
				<spring:message code="quotaddinst.instrumentbasket"/> 
			</h5>
			
			<!-- link to delete all items in the quote basket -->
			<a href="#" class="mainlink-float" onclick=" deleteFromBasket(null); return false; " id="removelink" title="<spring:message code='quotaddinstmod.removeallitemsfromthebasket'/>"><spring:message code="quotaddinstmod.removeallitems"/></a>
													
			<!-- clear floats and restore page flow -->
			<div class="clear"></div>
				
			<table class="default4 quoteInstBasket" summary="<spring:message code='quotaddinst.thistabledisplaysallinstrumentsinbasket'/>">
				<thead>
					<tr>
						<td colspan="10"><spring:message code="quotaddinst.instrumentbasket"/>  (<span class="basketCount"><c:out value=" ${basketCount} "/></span>)</td>
					</tr>
					<tr>
						<th class="item" scope="col"><spring:message code="item"/></th>
						<th class="barcode" scope="col"><spring:message code="barcode"/></th>
						<th class="inst" scope="col"><spring:message code="instmodelname"/></th>
						<th class="serial" scope="col"><spring:message code="serialno"/></th>
						<th class="plant" scope="col"><spring:message code="plantno"/></th>
						<th class="heading" scope="col"><spring:message code="quotaddinst.heading"/></th>
						<th class="source" scope="col"><spring:message code="quotaddinst.costsrc"/></th>
						<th class="caltype" scope="col"><spring:message code="servicetype"/></th>													
						<th class="cost" scope="col"><spring:message code="price"/></th>
						<th class="remove" scope="col"><spring:message code="delete"/></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="10">&nbsp;</td>
					</tr>
				</tfoot>
				<c:choose>
					<c:when test="${not empty form.basketIds}">
						<c:set var="index" value="0"/>
						<c:forEach var="plantid" items="${form.basketIds}" varStatus="statusIds">
							<%-- 
								When an instrument is deleted from the middle of the basket, 
								and then form is submitted, the list will have a null element.
								We suppress display of these null elements, as well as adding
								to the quotation (skipped in controller/service logic)
							--%>
							<c:if test="${not empty plantid}">
								<tbody id="basketitem${plantid}">
									<tr>
										<td class="item">${index + 1}</td>
										<td class="barcode">
											<form:hidden path="basketIds[${statusIds.index}]" value="${plantid}" />
												${plantid}
											<br><form:errors cssClass="attention" path="basketIds[${statusIds.index}]"/>
										</td>
										<td class="inst">	
												<c:set var="escapedBasketDefInsts"><spring:escapeBody htmlEscape="true">${form.basketDefInsts.get(statusIds.index)}</spring:escapeBody></c:set>
												<form:hidden path="basketDefInsts[${statusIds.index}]" value="${escapedBasketDefInsts}" />
												${escapedBasketDefInsts}
										</td>					
										<td class="serial">
												<form:hidden path="basketSerials[${statusIds.index}]" value="${form.basketSerials.get(statusIds.index)}" />
												${form.basketSerials.get(statusIds.index)}
										</td>													
										<td class="plant">
												<form:hidden path="basketPlants[${statusIds.index}]" value="${form.basketPlants.get(statusIds.index)}" />
												${form.basketPlants.get(statusIds.index)}
										</td>
										<td class="heading">
											<form:select path="basketHeadings[${statusIds.index}]">
												<c:forEach var="qh" items="${headings}">
													<option value="${qh.headingId}" <c:if test="${qh.headingId == form.basketHeadings.get(statusIds.index)}"> selected="selected" </c:if>>${qh.headingName}</option>
												</c:forEach>
											</form:select>
											<a href="#" class="copyDownLink" onclick="  copySelectedOption('down', 'select', $j(this).siblings('select')); return false; " title="<spring:message code='quotaddinst.copyselectiondown'/>">&nbsp;</a>
											<a href="#" class="copyUpLink" onclick="  copySelectedOption('up', 'select', $j(this).siblings('select')); return false; " title="<spring:message code='quotaddinst.copyselectiondown'/>">&nbsp;</a>
										</td>
										<td class="source">
												<form:hidden id="basketSources${plantid}" path="basketSources[${statusIds.index}]" value="${form.basketSources.get(statusIds.index)}" />
												<span id="spanSource${plantid}">${form.basketSources.get(statusIds.index)}</span><br>
												<a href="#" onclick="switchSourceCost('${plantid}','${quotation.id}')"/><spring:message code='managecalconditions.switch'/></a>
										</td>
										<td class="caltype">
												<form:select id="caltype${plantid}" path="basketCaltypes[${statusIds.index}]" onchange="reevaluatePrice('${plantid}','${quotation.id}');">
													<c:forEach var="caltype" items="${caltypes}">
														<option value="${caltype.key}" <c:if test="${caltype.key == form.basketCaltypes.get(statusIds.index)}"> selected="selected" </c:if>>
															${caltype.value}
														</option>  
													</c:forEach>
												</form:select>
										</td>																
										<td class="cost">
											${quotation.currency.currencyERSymbol}
												<form:hidden id="basketCosts${plantid}" path="basketCosts[${statusIds.index}]" value="${form.basketCosts.get(statusIds.index)}" />
												<span id="spanCosts${plantid}">${form.basketCosts.get(statusIds.index)}</span>
										</td>											
										<td class="remove">
											<a href="#" onclick=" deleteFromBasket('${plantid}'); return false;" class="<spring:message code='quotaddinst.deleteanchor'/>">
												&nbsp;
											</a>
										</td>
									</tr>									
								</tbody>
								<c:set var="index" value="${index + 1}"/>
							</c:if>
						</c:forEach>
					
					</c:when>
					<c:otherwise>
					
						<tbody>
							<tr>
								<td colspan="10" class="center bold"><spring:message code="quotaddinst.yourinstrumentbasketisempty"/></td>
							</tr>
						</tbody>
					
					</c:otherwise>
				</c:choose>
															
			</table>
			
			<div class="text-center padtop">
				<input type="button" name="buttonAdd" id="buttonAdd" value="<spring:message code='quotaddinst.addtoquote'/>" 
					onclick=" submitBasketContents(this, 'addToQuote'); " />
			</div>
		
		</div>
																					
	</div>

</form:form>
</jsp:body>
</t:crocodileTemplate>