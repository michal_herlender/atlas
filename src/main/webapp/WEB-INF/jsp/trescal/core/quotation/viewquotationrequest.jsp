<%-- File name: /trescal/core/quotation/viewquotationrequest.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address" %>
<%@ taglib prefix="company" tagdir="/WEB-INF/tags/company" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<html>
 <head>
 	
	<sec:csrfMetaTags />
	<link rel="stylesheet" href="styles/global.css" type="text/css" title="antech" />
	
	<link rel="stylesheet" href="styles/print/print.css" type="text/css" media="print" />
	
	
	<script type='text/javascript'>
		// set variable to default context of project
		var springUrl = "${req.contextPath}";
		// get value of loadtab parameter from querystring
		// used to show the correct tab when subnavigation bar is used
		var LOADTAB = '${request.getParameter("loadtab")}';
		// retrieves application session maximum inactive interval
		var sessionTimeout = ${pageContext.session.maxInactiveInterval} / 60;
		// get user preferences
		var userPrefs =
		{
			/* tooltips turned on? */
			tooltips: 		${currentContact.userPreferences.mouseoverActive},
			/* job item key nav on? */
			jikeynav:		${currentContact.userPreferences.jiKeyNavigation}
		};
		// set variable to i18n of javascript
		var i18locale = '${rc.locale.language}';
	</script>
	
	<!-- makes all spring generated html elements xhtml compliant -->
	<c:set var="springXhtmlCompliant" value="true"/>
	
	<script type='text/javascript' src='script/trescal/core/quotation/ViewQuotationRequest.js'></script>
	
	<script type='text/javascript' src='script/thirdparty/jQuery/jquery.js'></script>	
	
	<script type='text/javascript' src='script/trescal/core/template/Cwms_Main.js'></script>
	
	<script type='module' src='script/components/cwms-upload-file/cwms-upload-file.js'></script>

	<cwms-upload-file system-component-id="${command.sc.componentId}" entity-id1="${command.request.requestNo}" entity-id2=""
 			files-name="${command.filesName}" rename-to="" web="false"></cwms-upload-file>
</head>
<body onload=" loadScript.init(true); " class="iframed" style=" width: 560px; min-width: 560px; ">
	<fieldset>
		<c:if test="${command.request.status == 'REQUESTED'}">
			<ol>
				<li>
					<a href="quotationrequest.htm?id=${command.request.id}" class="mainlink" title="<spring:message code="viewquotrequ.editquotationrequest" />"><spring:message code="viewquotrequ.editrequest" /></a>
					|
					<a href="#" onclick="parent.window.location='<spring:url value="quotationrequesttoquotation.htm?id=${command.request.id}"/>'" class="mainlink" title="<spring:message code="viewquotrequ.convertrequesttoquotation" />"><spring:message code="viewquotrequ.converttoquotation" /></a>
				</li>
			</ol>
		</c:if>
		<ol id="existingcompany">
			<li>
				<label><spring:message code="viewquotrequ.quotetypestatus" />:</label>
				<span class="bold">${command.request.quoteType.displayName}</span> (${command.request.status})
			</li>
			<li>
				<label><spring:message code="company" />:</label>
				<span>
					<c:choose>
						<c:when test="${command.request.detailsSource == 'EXISTING'}">
							<links:companyLinkDWRInfo company="${command.request.comp}" rowcount="${rowcount}" copy="false"></links:companyLinkDWRInfo>
						</c:when>
						<c:otherwise> 
							${command.request.company}
						</c:otherwise>
					</c:choose>	
				</span>
			</li>			
			<li>
				<label><spring:message code="contact" />:</label>
				<span>
					<c:choose>
						<c:when test="${command.request.detailsSource == 'EXISTING'}">
							<links:contactLinkDWRInfo contact="${command.request.con}" rowcount="${rowcount}"/>
						</c:when>
						<c:otherwise>
							${command.request.contact}
						</c:otherwise>
					</c:choose>
				</span>
			</li>			
			<li>
				<label><spring:message code="quotrequest.phone" />:</label>
				<span>
					<c:choose>
						<c:when test="${command.request.detailsSource == 'EXISTING'}">
							${command.request.con.telephone}
						</c:when>
						<c:otherwise>
							${command.request.phone}
						</c:otherwise>
					</c:choose>
				</span>
			</li>
			<li>
				<label><spring:message code="quotrequest.fax" />:</label>
				<span>
					<c:choose>
						<c:when test="${command.request.detailsSource == 'EXISTING'}">
							${command.request.con.fax}
						</c:when>
						<c:otherwise>
							${command.request.fax}
						</c:otherwise>
					</c:choose>
				</span>
			</li>
			<li>
				<label><spring:message code="quotrequest.email" />:</label>
				<span>
					<c:choose>
						<c:when test="${command.request.detailsSource == 'EXISTING'}">
						<a href="mailto:${command.request.con.email}" class="mainlink">${command.request.con.email}</a>
						</c:when>
						<c:otherwise>
						<a href="mailto:${command.request.email}" class="mainlink">${command.request.email}</a>
						</c:otherwise>
					</c:choose>
				</span>
			</li>
			<li>
				<label><spring:message code="address" />:</label>
				<div class="float-left padtop width70">
					<c:choose>
						<c:when test="${command.request.detailsSource == 'EXISTING'}">
							<c:choose>
								<c:when test="${not empty command.request.con.defAddress}">
									<address:showAddress address="${command.request.con.defAddress}" vis="false" separator="<br/>" copy="true"/>
								</c:when>
								<c:otherwise>
									<spring:message code="viewquotrequ.nodefaultaddressforcontact" />
								</c:otherwise>
							</c:choose>
					</c:when>
					<c:otherwise>
							${command.request.addr1}<br />
							<c:if test="${not empty command.request.addr2}">
								${command.request.addr2}<br />
							</c:if>
							${command.request.town}<br />
							<c:if test="${not empty command.request.county}">
								${command.request.county}<br />
							</c:if>
							${command.request.postcode}<br />
							${command.request.country}
					</c:otherwise>
					</c:choose>
				</div>
				<div class="clear"></div>
			</li>
			<li>
				<label><spring:message code="quotrequest.requestinformation" />:</label>
				<div class="float-left padtop width70">${command.request.requestInfo}</div>
				<div class="clear"></div>
			</li>
			<c:if test="${not empty command.request.clientref}">
				<li>
					<label><spring:message code="clientref" />:</label>
					<span>${command.request.clientref}</span>
				</li>
			</c:if>
			<c:if test="${not empty command.request.clientOrderNo}">
				<li>
					<label><spring:message code="quotrequest.clientorderno" />:</label>
					<span>${command.request.clientOrderNo}</span>
				</li>
			</c:if>
			<li>
				<label><spring:message code="requestdate" />:</label>
				<span><fmt:formatDate value="${command.request.reqdate}" type="date" dateStyle="SHORT"/></span>
			</li>
			<c:if test="${not empty command.request.duedate}">
				<li>
					<label><spring:message code="quotrequest.duedates" />:</label>
					<span><fmt:formatDate value="${command.request.duedate}" type="date" dateStyle="SHORT"/></span>
				</li> <td></td>
			</c:if>
			<li>
				<label><spring:message code="viewquotrequ.loggedby" />:</label>
				<span>${command.request.loggedBy.name} (${command.request.loggedBy.sub.comp.coname})</span>
			</li>
			<li>
				<label><spring:message code="viewquotrequ.loggedtype"/>:</label>
				<span>
					<c:choose>
						<c:when test="${command.request.logSource == 'MEMBERS_WEB' || command.request.logSource == 'PUBLIC_WEB'}">
						<strong>${command.request.logSource.viewName}</strong>
						</c:when>
						<c:otherwise>
							${command.request.logSource.viewName}
						</c:otherwise>
					</c:choose>
				</span>
			</li>			
			<li>
				<files:showFilesForSC entity="${command.request}" sc="${command.sc}" id="${command.request.id}" identifier="${command.request.requestNo}" ver="" rootFiles="${command.scRootFiles}" allowEmail="false" isEmailPlugin="false" rootTitle="<spring:message code='viewquotrequ.filesforquoterequest' /> ${command.request.requestNo}" deleteFiles="true" 
				importJQuery="${true}"/>
				<spring:message code='viewquotrequ.filesforquoterequest' /> <c:out value=" ${command.request.requestNo}" />
			</li>
		</ol>
	</fieldset>
 </body>
</html>