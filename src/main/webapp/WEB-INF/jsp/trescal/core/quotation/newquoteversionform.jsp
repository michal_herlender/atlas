<%-- File name: /trescal/core/quotation/newquoteversionform.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="quotation" tagdir="/WEB-INF/tags/quotation" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="newquotevers.newquotationversion" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/quotation/NewQuoteVersionForm.js'></script>
    </jsp:attribute>
    <jsp:body>
    <form:form method="post" id="newquoteversionform" action="" modelAttribute="newquoteversion">
	<form:hidden path="businessCompanyId" />
	
	<!-- div contains fieldsets for main quote editable info -->
	<div class="infobox">
	
		<t:showErrors path="newquoteversion.*" showFieldErrors="true" />
	
		<!-- displays main quote information -->
		<fieldset id="quoteiteminfo">

			<legend>
				<spring:message code="newquotevers.creatinganewversionofquotation" />  
				<a href="<spring:url value='viewquotation.htm?id=${oldQuotation.id}'/>">
					${oldQuotation.qno} version ${oldQuotation.ver}
				</a>
			</legend>
			
			<!-- displays content in column 49% wide (left), id used to style form elements -->
			<div class="displaycolumn" id="newversioninfo">

				<ol>
					<li>
						<label for="personid"><spring:message code="contact" />:</label>
						<form:select path="personId" items="${clientContactList}" itemLabel="value" itemValue="key" />
						<form:errors path="personId" class="attention" />
					</li>
					<li>
						<label for="clientRef"><spring:message code="newquotevers.clientref" />:</label>
						<form:input path="newQuotation.clientref" type="text" id="newQuotation.clientref" value="${newquoteversion.newQuotation.clientref}" />
						<form:errors path="newQuotation.clientref" class="attention" />
					</li>
					<li>
						<label for="reqdate"><spring:message code="requestdate" />:</label>
						<form:input path="newQuotation.reqdate" type="date" id="newQuotation.reqdate" value="${newquoteversion.newQuotation.reqdate}"  autocomplete="off" />
						<form:errors path="newQuotation.reqdate" class="attention" />
					</li>
					<li>
						<label for="statusid"><spring:message code="status" />:</label>
						<form:select path="newQuotation.quotestatus.statusid" id="newQuotation.quotestatus.statusid">
							<c:forEach var="qstatus" items="${quoteStatusList}">
								<option value="${qstatus.statusid}" <c:if test="${qstatus.statusid == newquoteversion.newQuotation.quotestatus.statusid}"> selected="selected" </c:if>><t:showTranslationOrDefault translations="${qstatus.nametranslations}" defaultLocale="${defaultlocale}"/></option>
							</c:forEach>
						</form:select>
						<form:errors path="newQuotation.quotestatus.statusid" class="attention" />
					</li>
					<li>
						<label for="sourcedBy"><spring:message code="createquot.sourcedby" />:</label>
						<form:select path="sourcedById" items="${businessContactList}" itemLabel="value" itemValue="key" />
						<form:errors path="sourcedById" class="attention" />
					</li>
					<li>
						<label class="threelabel"><spring:message code="newquotevers.quotationoptions" />:</label>
						<span>
							<a href="#" id="headinganchor" class="mainlink" onclick="SetAllElementCheckBoxes('quotationitems', 'headingids', false, this, 'Headings'); return false;"><spring:message code="newquotevers.deselectallheadings" /></a>
							<br  />
							<a href="#" id="itemsanchor" class="mainlink" onclick="SetAllElementCheckBoxes('quotationitems', '', false, this, 'Headings and Items'); return false;"><spring:message code="newquotevers.deselectallheadingsanditems" /></a>
							<br />
							<a href="#" id="notesanchor" class="mainlink" onclick="SetAllElementCheckBoxes('quotenotes', 'noteids', false, this, 'Notes'); return false;"><spring:message code="newquotevers.deselectallnotes" /></a>
						</span>
					</li>
				</ol>
			
			</div>
			
			<!-- displays content in column 49% wide (right), id used to style form elements -->
			<div class="displaycolumn" id="questions">
			
				<ol>
					<li>
						<form:label path="newQuotation.duration"><spring:message code="newquotevers.duration" />:</form:label>
						<form:input type="text" path="newQuotation.duration" id="newQuotation.duration" value="${newquoteversion.newQuotation.duration}"/>
						<form:errors path="newQuotation.duration" cssClass="errorabsolute"/>
					</li>
					<li>
						<form:label path="noteThis"><spring:message code="newquotevers.recordnoteofthisaction" />?</form:label>
							<c:set var="checked" value=""/>
							<c:if test="${status.value == true}"><c:set var="checked" value="checked"/> </c:if>
						<form:checkbox path="noteThis" id="noteThis" value="true"/>
					</li>
					<li>
						<form:label path="preserveModules"><spring:message code="newquotevers.preservemodulerrelationships" />?</form:label>
							<c:set var="checked" value=""/>
							<c:if test="${status.value == true}"><c:set var="checked" value="checked"/></c:if>
						<form:checkbox path="preserveModules" id="preserveModules" value="true"/>
					</li>
					<c:if test="${not empty oldQuotation.usingDefaultTermsAndConditions}"> 
						<li>
							<form:label path="includeCustomConditions"><spring:message code="newquotevers.includecustomtermsandconditions" />?</form:label>
								<c:set var="checked" value=""/>
								<c:if test="${status.value == true}"><c:set var="checked" value="checked"/></c:if>
							<form:checkbox path="includeCustomConditions" id="includeCustomConditions" value="true"/>

							<c:forEach var="con" items="${oldQuotation.calConditions}">
								<form:hidden path="cusCalConIds" value="${con.defCalConId}" />
							</c:forEach>
							<form:hidden path="cusGenConIds" value="${oldQuotation.generalConditions.genConId}" />
						</li>
					</c:if>
					<li>
						<form:label path="includeDefaultQuoteNotes"><spring:message code="newquotevers.includedefaultquotenotes" />?</form:label>
							<c:set var="checked" value=""/>
							<c:if test="${status.value == true}"><c:set var="checked" value="checked"/></c:if>
						<form:checkbox path="includeDefaultQuoteNotes" id="includeDefaultQuoteNotes" value="true"/>
					</li>
					<li>
						<label for="createAsNewVersion" class="threelabel"><spring:message code="newquotevers.newquotetype" />:</label>
							<c:set var="checked" value=""/>
							<c:if test="${status.value == true}"><c:set var="checked" value="checked"/></c:if>
						<form:radiobutton path="createAsNewVersion" id="createAsNewVersion" class="radiobutton" value="true" /> <spring:message code="newquotevers.newversion" /><br />
							<c:if test="${status.value == false}"><c:set var="checked" value="checked"/></c:if>
							<form:radiobutton path="createAsNewVersion" class="radiobutton" value="false"/> <spring:message code="newquotevers.newquotation" />
					</li>
				</ol>
				
			</div>
			
		</fieldset>
								
	</div>
	<!-- close div for main quote editable info -->
							
	<!-- this displays all quotation items under their corresponding heading and calibration type -->
	<div id="quotationitems">
																					
	<c:forEach var="heading" items="${oldQuotation.quoteheadings}">
			
		<c:if test="${heading.quoteitems.size() > 0}">
		
			<!-- this div contains all items which appear under this heading, 
					id used for select/deselect all javascript function -->
			<div class="infobox" id="heading${heading.headingId}items">
				
				<!-- anchor contains onclick event which uses scriptaculous slide effect to hide and show heading items -->
				<h5 class="center">
					<form:checkbox value="${heading.headingId}" path="headingids" checked="checked" />
					<spring:message code="quotaddinstmod.heading" /> - ${heading.headingName} - <a href="#" onclick=" $j('#slide${heading.headingId}').slideToggle(1000); return false; " class="mainlink"><spring:message code="newquotevers.hideitems" /></a>
				</h5>
																				
				<c:set var="caltype" value="0"/>
				<c:set var="itemno" value="0"/> 
				
				<div id="slide${heading.headingId}">
				
				<div class="float-right">
					<a href="#" class="mainlink" id="anchor${heading.headingId}" onclick="SetAllElementCheckBoxes('heading${heading.headingId}items', 'itemnos', false, this, 'Items'); return false;"><spring:message code="newquotevers.deselectallitems" /></a>
				</div>
				
				<!-- this blank div clears floats and restores page flow -->
				<div class="clear"></div>
				
					<table class="default2" summary="<spring:message code="newquotevers.tablequoteitemsspeccal" />">
						<c:forEach var="quoteitem" items="${heading.quoteitems}">
							<c:if test="${quoteitem.partOfBaseUnit != true}">
								<c:if test="${caltype != quoteitem.caltype.calTypeId}">
									
									<!-- if the table has already been created and modules where not the last items
										to be added under this calibration type then close tags -->
									<c:choose>
										<c:when test="${caltype != 0 && thisBaseUnit == false}"></c:when>
										<c:when test="${caltype != 0}"></c:when>
									</c:choose>
								
									<c:set var="rowcount" value="1"/>
									<c:set var="itemno" value="1"/>
									<c:set var="caltype" value="${quoteitem.caltype.calTypeId}"/>
										
									<tbody>													
										<c:if test="${caltype != 0}">
											<tr>
												<td colspan="7">&nbsp;</td>
											</tr>
										</c:if>
										<tr>
											<td colspan="7">
												<t:showTranslationOrDefault translations="${quoteitem.caltype.serviceType.longnameTranslation}" defaultLocale="${defaultlocale}"/>&nbsp;<spring:message code="items" />
											</td>
										</tr>
										<tr>
											<th class="unit" scope="col" colspan="2"><spring:message code="newquotevers.itemunit" /></th>  
											<th class="caltype" scope="col"><spring:message code="caltype" /></th>  
											<th class="plantno" scope="col"><spring:message code="editquot.referenceno" /></th>  
											<th	class="discount" scope="col"><spring:message code="newquotevers.discount" /></th>
											<th class="qty" scope="col"><spring:message code="quotaddinstmod.qty" /></th>
											<th class="unitcost" scope="col"><spring:message code="newquotevers.unitcost" /></th>
										</tr>
									</tbody>
								</c:if>
								<tbody>	
									
									<!-- set the checkCalType variable to the current calibration id ready for 
									testing above on the next iteration -->
									<c:set var="checkCalType" value="${quoteitem.caltype.calTypeId}"/>
																	
									<tr <c:choose><c:when test="${rowcount % 2 == 0}"> class="even" </c:when><c:otherwise> class="odd" </c:otherwise></c:choose>>
										<td class="itemno" scope="col">
											${itemno}
										</td>
										<td>
											<form:checkbox value="${quoteitem.id}" path="itemnos" checked="checked" />
											<c:choose>
												<c:when test="${not empty quoteitem.inst}">
													<quotation:showInstrument instrument="${quoteitem.inst}"/>
												</c:when>
												<c:otherwise>
													<cwms:showmodel instrumentmodel="${quoteitem.model}"/>
												</c:otherwise>
											</c:choose>
										</td>
										<td class="center"><t:showTranslationOrDefault translations="${quoteitem.caltype.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/></td>
										<td>${quoteitem.referenceNo}</td>
										<td class="center">${quoteitem.generalDiscountRate}%</td>
										<td class="center">${quoteitem.quantity}</td>
										<td class="right">														
										<a class="mainlink" href="<spring:url value='editquotationitem.htm?id=${quoteitem.id}'/>">
											${quoteitem.unitCost} ${quoteitem.quotation.currency.currencyERSymbol}
										</a>																
										</td>
									</tr>
								</tbody>	
									
								<c:set var="thisBaseUnit" value="${false}"/>
								
								<c:if test="${quoteitem.modules.size() > 0}">
									
									<tbody class="modulerows">
									
										<c:forEach var="module" items="${quoteitem.modules}">
											<c:set var="thisBaseUnit" value="${true}"/>
																								
											<tr <c:choose><c:when test="${rowcount % 2 == 0}"> class="even" </c:when><c:otherwise> class="odd" </c:otherwise></c:choose>>
												<td><img src="img/icons/arrow_merge.png" height="16" width="16" alt="<spring:message code='editquothead.partofbaseunit' /> - <cwms:showmodel instrumentmodel="${quoteitem.model}"/>" title="<spring:message code='editquothead.partofbaseunit' /> - <cwms:showmodel instrumentmodel="${quoteitem.model}"/>" /></td>
												<td>
													<form:checkbox value="${module.id}" path="itemnos" checked="checked" />
													<c:choose>
														<c:when test="${not empty module.inst}">
															<quotation:showInstrument instrument="${module.inst}"/>
														</c:when>
														<c:otherwise>
															<cwms:showmodel instrumentmodel="${module.model}"/>
														</c:otherwise>	
													</c:choose>																								
												</td>
												<td class="center"><t:showTranslationOrDefault translations="${module.caltype.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/></td>
												<td>
													<c:choose>
														<c:when test="${not empty module.inst}">
															<c:choose>
																<c:when test="${module.inst.plantno != null && module.inst.plantno != ''}">
																	(<span class="bold">P</span>) ${module.inst.plantno}
																</c:when>
																<c:otherwise>
																	(<span class="bold">S</span>) ${module.inst.serialno}
																</c:otherwise>
															</c:choose>	
														</c:when>
														<c:otherwise>
															<c:if test="${module.plantno != null && module.plantno != ''}">
																(<span class="bold">P</span>) ${module.plantno}
															</c:if>
														</c:otherwise>
													</c:choose>	
												</td>
												<td class="center">${module.generalDiscountRate}%</td>
												<td class="center">${module.quantity}</td>
												<td class="right">														
													<a class="mainlink" href="<spring:url value='editquotationitem.htm?id=${module.id}'/>">
														Â£${module.finalCost}
													</a>																	
												</td>
											</tr>
											
										</c:forEach>
										
									</tbody>
										
								</c:if>
									
								<c:set var="rowcount" value="${rowcount + 1}" />
								<c:set var="itemno" value="${itemno + 1}" />
					
							</c:if>
							
						</c:forEach>
					</table>
				
				</div>
				<!-- close the slide div -->
			
			</div>
			<!-- close the infobox div -->

		</c:if>
			
	</c:forEach>
	
	</div>
	<!-- close the quotation items div -->
	
	<!-- this div contains all active notes that are currently on this quotation, 
			id used for select/deselect all javascript function -->
	<div class="infobox" id="quotenotes">
	
		<h5><spring:message code="newquotevers.quotationnotes" /></h5>
		
		<!-- this fieldset displays any currently active public notes -->
		<fieldset>
		
			<legend><spring:message code="newquotevers.publicnotes" /></legend>
			
			<ol id="public-list">
				
				<c:if test="${oldQuotation.publicActiveNoteCount < 1}">
					<li><spring:message code="newquotevers.nonotes" /></li>
				</c:if>
			
				<c:forEach var="note" items="${oldQuotation.notes}">
					<c:if test="${note.publish == true && note.active == true}">
						<li id="note${note.noteid}">
							
							<div class="notes_note_edit">
								<form:checkbox value="${note.noteid}" path="noteids" checked="checked" />
								<c:if test="${note.label != ''}">
									<strong>${note.label}:</strong>&nbsp;
								</c:if>
									${note.note}
							</div>
							<div class="notes_editor_edit">${note.setBy.name}<br /><fmt:formatDate value="${note.setOn}" dateStyle="medium" type="both" timeStyle="short" /></div>
							<div class="clear-0"></div>
							
						</li>
					</c:if>
				</c:forEach>
			
			</ol>
		
		</fieldset>
		
		<!-- this fieldset displays any currently active private notes -->
		<fieldset>
		
			<legend><spring:message code="newquotevers.privatenotes" /></legend>
			
			<ol id="private-list">
				
				<c:if test="${oldQuotation.privateActiveNoteCount < 1}">
					<li><spring:message code="newquotevers.nonotes" /></li>
				</c:if>
			
				<c:forEach var="note" items="${oldQuotation.notes}">
					<c:if test="${note.publish == false && note.active == true}">
						<li id="note${note.noteid}">
							
							<div class="notes_note_edit">
								<form:checkbox value="${note.noteid}" path="noteids" checked="checked" />
								<c:if test="${note.label != ''}">
									<strong>${note.label}:</strong>&nbsp;
								</c:if>
									${note.note}
							</div>
							<div class="notes_editor_edit">${note.setBy.name}<br /><fmt:formatDate value="${note.setOn}" dateStyle="medium" type="both" timeStyle="short"/></div>
							<div class="clear-0"></div>
						</li>
					</c:if>
				</c:forEach>
			
			</ol>
		
		</fieldset>
	
	</div>
	<!-- close the quote notes div -->
	
	<div class="center-padbot">
		<input type="submit" value="<spring:message code='create' />" />
	</div>

	</form:form>
    </jsp:body>
</t:crocodileTemplate>