<%-- File name: /trescal/core/quotation/viewquotation.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="costs" tagdir="/WEB-INF/tags/costs"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files"%>
<%@ taglib prefix="quotation" tagdir="/WEB-INF/tags/quotation"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message
				code="viewquot.viewquotation" /></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type="module">
			import "${pageContext.request.contextPath}/script/trescal/core/quotation/cwms-client-accept-date-on.js";
		</script>
		<script type='text/javascript' src='script/thirdparty/jQuery/jquery.js'></script>
		<script type="module" src="script/components/cwms-upload-file/cwms-upload-file.js" ></script>
		<script type='text/javascript'
			src='script/trescal/core/quotation/ViewQuotation.js'></script>
		<script type='text/javascript'>
			var quoteIssued = false;
			/* Mark if quotation is locked for editing */
			<c:if test="${quotation.quotestatus.issued}">
				quoteIssued = true;
			</c:if>
			// get scroll top marker populated on form backing object from session
			var scrollTopMarker = '${sessSetScrollTopMarker}';
			
			// create array of quotation headings
			var quoteHeadArray = new Array();
			// add quote headings to array
			<c:forEach var="hd" items="${quotation.quoteheadings}">
				quoteHeadArray.push({headingId: ${hd.headingId}, headingName: '${hd.headingName}'});
			</c:forEach>
			// create array of calibration types
			var caltypeArray = new Array();
			// add calibration type to array
			<c:forEach var="ct" items="${caltypes}">
				caltypeArray.push({calTypeId: ${ct.calTypeId}, calTypeName: '<cwms:besttranslation translations="${ct.serviceType.shortnameTranslation}"/>'});
			</c:forEach>
			
			function updateInstructionSize(instructionSize) {
				$j("span.instructionSize").text(instructionSize);
			}
		</script>
		<script type="module" src="script/components/cwms-instructions/cwms-instructions.js"></script>
	</jsp:attribute>
	<jsp:attribute name="globalElements">
		<cwms-upload-file system-component-id="${sc.componentId}" entity-id1="${quotation.qno}" entity-id2="${quotation.ver}"
 			files-name="${filesName}" rename-to="" web="false"></cwms-upload-file>
	</jsp:attribute>
	<jsp:body>
		<c:if test="${not empty quotation.expiryDate}">
			<c:if test="${not quotation.valid}">								
				<div class="warningBox1">
					<div class="warningBox2">
						<div class="warningBox3">
							<spring:message code="viewquot.quotvalidityexpired" />!
							<a href="newquoteversionform.htm?id=${quotation.id}"
								title="<spring:message code='viewquot.createnewversion' />"
								class="mainlink">
								<spring:message code="viewquot.createnewrevision" />.
							</a>	
						</div>
					</div>
				</div>								
			</c:if>
		</c:if>
		
		<!-- Mark if quotation is locked for editing -->
		<c:if test="${quotation.quotestatus.issued}">
			<div id="lockedmessage" class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<spring:message code="viewquot.noteissuedlocked" />.									
						<a href="#" class="mainlink"
							onclick=" event.preventDefault(); unlockQuotation(); "><spring:message
								code="viewquot.unlockforedit" /></a> 
					</div>
				</div>
			</div>	
		</c:if>
		
		<c:set var="quotationCoid" value="${quotation.contact.sub.comp.coid}" />
		
		<!-- this div wraps the whole quotation page -->
		<div id="viewquotationpage">
			<div class="infobox">
				<fieldset>
					<legend>
						<spring:message code="quotation" />
						<c:out value=" ${quotation.qno} " />
						<spring:message code="quotaddinstmod.version" />
						<c:out value=" ${quotation.ver}" />
					</legend>
					<div class="displaycolumn">
						<ol>
							<li>
								<label><spring:message code="company" />:</label>
								<span><links:companyLinkDWRInfo copy="true"
										company="${quotation.contact.sub.comp}" rowcount="0" /></span>
							</li>
							<li>
								<label><spring:message code="subdivision" />:</label>
								<span>${quotation.contact.sub.subname}</span>
							</li>
							<li>
								<label><spring:message code="contact" />:</label>
								<span><links:contactLinkDWRInfo
										contact="${quotation.contact}" rowcount="0" /></span>
							</li>
							<c:if test="${not empty quotation.clientref}">
								<li>
									<label><spring:message code="clientref" />:</label>
									<span>${quotation.clientref}</span>
								</li>
							</c:if>
							<li>
								<label><spring:message code="requestdate" />:</label>
								<span><fmt:formatDate value="${quotation.reqdate}" dateStyle="medium" type="date"/></span>
							</li>
							<li>
								<label><spring:message code="viewquot.durationcurrency" />:</label>
								<span>
									${quotation.duration}&nbsp;<spring:message code="days" />
									<c:if test="${not empty quotation.expiryDate}">
										<c:choose>
											<c:when test="${quotation.valid}">
												(<spring:message code="viewquot.expires" /> <fmt:formatDate
													type="date" dateStyle="SHORT" value="${quotation.expiryDate}" />)
											</c:when>
											<c:otherwise>
												<span style="color: red;">(<spring:message
														code="viewquot.expired" /> <fmt:formatDate
														type="date" dateStyle="SHORT" value="${quotation.expiryDate}" />)</span>
											</c:otherwise>
										</c:choose>
									</c:if>
									&nbsp;/&nbsp;
									<c:set var="currency" value="${quotation.currency}" />
									${currency.currencyCode}
									(${currency.currencySymbol} ${currency.currencyName}
									- ${defaultCurrency.currencyERSymbol}1 : ${currency.currencyERSymbol}${quotation.rate})
								</span>
							</li>
							<li>
								<label><spring:message
										code="viewquot.contractmininvoice" /> :</label>
								<span>${quotation.contractInvoiceThreshold}</span>
							</li>
						</ol>
					</div>
					<div class="displaycolumn">
						<ol>
							<li>
								<label><spring:message code="viewquot.version" />:</label>
								<span>
									${quotation.ver} of ${quotelist.size()}
									<c:if test="${not empty quotelist}">
										[<c:forEach var="ver" items="${quotelist}">
											<c:choose>
												<c:when test="${ver.ver == quotation.ver}">
													${ver.ver}
												</c:when>
												<c:otherwise>
													<a href="<c:url value='/viewquotation.htm?id=${ver.id}'/>"
														class="mainlink">${ver.ver}</a>
												</c:otherwise>
											</c:choose>
										</c:forEach>]
									</c:if>
								</span>
							</li>
							<li>
								<label><spring:message code="status" />:</label>
								<span><cwms:besttranslation
										translations="${quotation.quotestatus.nametranslations}" /></span>
							</li>
							<li>
								<label><spring:message code="viewquot.sourcedbyregdate" />:</label>
								<span>${quotation.sourcedBy.name} (<fmt:formatDate value="${quotation.regdate}" dateStyle="medium" type="date"/>)</span>
							</li>
							<li>
								<label><spring:message code="createquot.issuedate" />:</label>
								<div class="fleftTooltip width60">
									<span>
										<fmt:formatDate value="${quotation.issuedate}" dateStyle="medium" type="date"/>
										
										<c:if test="${quotation.issueby.name}">
											- ${quotation.issueby.name}
										</c:if> 
										<c:if test="${not quotation.issued}">
											<c:choose>
												<c:when test="${ not isWellAllocated}">
													<strong>
													<spring:message code="purchaseorder.statuserrorcomp" />
													${subdivSrcBy}
													</strong>
												</c:when>
												<c:when test="${not isUnderLimit}">
													<strong>
													<spring:message code="viewquot.isunderlimitorhasnopermission" />
													</strong>
												</c:when>
												<c:otherwise>
													<cwms:securedLink permission="QUOTATION_STATUS_UPDATE"  classAttr="mainlink"  parameter="?id=${quotation.id}&amp;issuing=true" collapse="True" >
															<spring:message code="viewquot.issuequotation"/>
														</cwms:securedLink>
												</c:otherwise>
											</c:choose>
										</c:if>
									</span>
								</div>
							</li>
							<c:if test="${not empty quotation.duedate}">
								<li>
									<label><spring:message code="quotrequest.duedates" />:</label>
									<span>
										<fmt:formatDate type="date" dateStyle="SHORT"
											value="${quotation.duedate}" />
									</span>
								</li>
							</c:if>
							<li>
								<label><spring:message code="viewquot.acceptance" />:</label>
								<span>
									<c:choose>
										<c:when test="${quotation.quotestatus.name eq 'Lost'}">
											${quotation.clientAcceptedOn}
										</c:when>
										<c:otherwise>
											<cwms-client-accept-date-on quotationId="${quotation.id}" acceptedBy="${quotation.acceptedBy.name}"
											acceptedOn="${quotation.acceptedOn}"
											></cwms-client-accept-date-on>
										</c:otherwise>
									</c:choose>
								</span>
							</li>
							<c:if test="${ quotation.accepted }">
								<li>
									<label><spring:message code="viewquot.clientacceptedon" />:</label>
									<span>
										<fmt:formatDate type="date" dateStyle="SHORT"
											value="${quotation.clientAcceptedOn}" />	
									</span>
								</li>
							</c:if>
							<li>
								<label><spring:message code="viewquot.contractstartdate" /> :</label>
								<span>
									<fmt:formatDate type="date" dateStyle="SHORT"
										value="${quotation.contractStartDate}" />
								</span>
							</li>	
							<c:if test="${ isWellAllocated}">									
								<li>
									<label><spring:message code="viewquot.limit" /> :</label>
								<span>
								<c:choose>
									<c:when test="${limit.getKey() == true }">
										<spring:message code="purchaseorder.limitless" />
									</c:when>
									<c:otherwise>
										${limit.getValue()}
									</c:otherwise>
								</c:choose>
								</span>
							</li>	
							</c:if>
						</ol>
					
					</div>
				
				</fieldset>
				
			</div>
						
			<c:if test="${quotation.quotestatus.issued}">
				<c:set var="notvisIfIssued" value="notvis" />
			</c:if>
																	
			<!-- sub navigation menu which displays and hides sections of page as well as linking to external pages -->
			<div id="subnav">
				<dl>
					<dt>
						<a href="#"
							onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'viewquotationitems-tab', false); "
							id="viewquotationitems-link"
							title="<spring:message code='viewquot.viewquotationitems'/>"
							class="selected"><spring:message code="items" /> (${totalItemCount})</a>
					</dt>
					<dt>
						<a href="#"
							onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'summary-tab', false); "
							id="summary-link"
							title="<spring:message code='viewquot.viewsummary'/>"><spring:message
								code="summary" /></a>
					</dt>
					<dt>
						<a href="addinstrumenttoquote.htm?id=${quotation.id}"
							title="<spring:message code='viewquot.addnewitemtoquotation'/>"
							class="limitAccess ${notvisIfIssued}"><spring:message
								code="viewquot.addinstrument" /></a>
					</dt>
					<dt>
						<c:set var="code_addnewitemtoquotation">
							<spring:message code='viewquot.addnewitemtoquotation'/>
						</c:set>
						<cwms:securedLink permission="INSTRUMENT_MODEL_ADD"  classAttr="limitAccess ${notvisIfIssued}"  parameter="?id=${quotation.id}" collapse="True" title="${code_addnewitemtoquotation}">
							<spring:message code="viewquot.addmodel"/>
						</cwms:securedLink>
					</dt>
					<dt>
						<a href="quoteheadingform.htm?id=${quotation.id}"
							title="<spring:message code='viewquot.viewheadingsforquotation'/>"
							class="limitAccess ${notvisIfIssued}"><spring:message
								code="viewquot.headings" /> (${quotation.quoteheadings.size()}) - <span
							class="bold">${quotation.headingSortType.displayName}</span></a>
					</dt>
					<dt>
						<a href="quotesort.htm?quoteid=${quotation.id}" onclick=""
							title=""><spring:message code="viewquot.sort" /> (<span
							class="bold">${quotation.sortType.displayName}</span>)</a>
					</dt>
				</dl>
			</div>
			<!-- end of sub navigation menu -->
			
			<div id="subnav2">
				<dl>
					<dt>
						<a href="editquotation.htm?id=${quotation.id}"
							id="editquotation-link"
							title="<spring:message code='viewquot.editquotationdetails'/>"
							class="limitAccess ${notvisIfIssued}"><spring:message
								code="edit" /></a>
					</dt>
					<dt>
						<a href="newquoteversionform.htm?id=${quotation.id}"
							title="<spring:message code='viewquot.createnewversion'/>"><spring:message
								code="newquotevers.newversion" /></a>
					</dt>
					<dt>
						<a href="#"
							onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'tpquoterequests-tab', false); "
							id="tpquoterequests-link"
							title="<spring:message code='viewquot.viewthirdpartyquotations'/>"><spring:message
								code="viewquot.supplierquotes" /> (${quotation.tpQuoteRequests.size()})</a>
					</dt>
					<dt>
						<a href="#"
							onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'priorstatushistory-tab', false); "
							id="priorstatushistory-link"
							title="<spring:message code='viewquot.viewallstatushistoryforquotation'/>"><spring:message
								code="viewquot.actions" /> (${quotation.priorStatusList.size()})</a>
					</dt>
					<dt>
						<a href="#"
							onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'termsandcond-tab', false); "
							id="termsandcond-link"
							title="<spring:message code='viewquot.viewtermsandconditionsforquotation'/>"><spring:message
								code="viewquot.termsnconditions" /></a>
					</dt>
					<dt>
						<a href="#"
							onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'email-tab', false); "
							id="email-link"
							title="<spring:message code='viewquot.viewsendemailsforquotation'/>"><spring:message
								code="quotrequest.email" /> (<span
							class="entityEmailDisplaySize">${quotation.sentEmails.size()}</span>)</a>
					</dt>
				</dl>
			</div>

			<div id="subnav3">
				<dl>
					<dt>
						<a href="#"
							onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'jobs-tab', false); "
							id="jobs-link"
							title="<spring:message code='viewquot.viewjobslinkedtothisquotation'/>"><spring:message
								code="jobs" /> (${jobQuoteLinkCount})</a>
					</dt>
					<dt>
						<a href="#"
							onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'quotedocs-tab', false); "
							id="quotedocs-link"
							title="<spring:message code='viewquot.viewdocumentsinquotationdirectory'/>"><spring:message
								code="viewquot.documents" /> (${scRootFiles.numberOfFiles})</a>
					</dt>
					<dt>
						<c:set var="code_viewcurrentquotationdocumenttext">
							<spring:message code="viewquot.viewcurrentquotationdocumenttext"/>
						</c:set>
						<cwms:securedLink permission="QUOTE_DOCUMENT_TEXT"  classAttr="limitAccess ${notvisIfIssued} "  parameter="?id=${quotation.id}" collapse="True" title="${code_viewcurrentquotationdocumenttext}">
							<spring:message code="viewquot.documenttext"/>
						</cwms:securedLink>
					</dt>
					<dt>
						<a href="#"
							onclick=" event.preventDefault(); genDocPopup(${quotation.id}, 'quotebirtdocs.htm?id=', '<spring:message code='viewquot.generatequotationdocument'/>'); "
							title="<spring:message code='viewquot.generatequotationdocument'/>">
								<spring:message code="generate" />
						</a>
					</dt>
					<dt>
						<a href="#"
							onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'quoterequests-tab', false); "
							id="quoterequests-link"
							title="<spring:message code='viewquot.viewquotationrequestslinkedtoquotation'/>"><spring:message
								code="viewquot.quotationrequests" /> (${quotation.quotationRequests.size()})</a>
					</dt>
					<dt>
						<a href="#"
							onclick=" event.preventDefault(); deleteQuote(${quotation.id}); "
							title="<spring:message code='viewquot.deletethequotationdocument'/>"
							class="limitAccess ${notvisIfIssued}"><span class="attention"><spring:message
									code="delete" /></span></a>
					</dt>
				</dl>
			</div>
			<div id="subnav4">
				<dl>
					<dt>
						<cwms:securedLink permission="IMPORT_QUOTATION_ITEMS"  classAttr="mainlink, limitAccess ${notvisIfIssued}"  parameter="?id=${quotation.id}" collapse="True" >
							<spring:message code='viewquot.importquotationitemsfromexchangeformat'/>
						</cwms:securedLink>
					</dt>
				   	<dt>
						<cwms:securedLink permission="IMPORT_QUOTATION_ITEMS_BATCH"  classAttr="mainlink, limitAccess ${notvisIfIssued}"  parameter="?id=${quotation.id}" collapse="True" >
							<spring:message code='viewquot.importquotationitemsbatch'/>
						</cwms:securedLink>
					</dt>
					<dt>
						<a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'instructions-tab', false); " id="instructions-link" title="<spring:message code="instructions" />">
							<spring:message code="instructions" />(<span class="instructionSize">${instructionsCount}</span>)
						</a>
					</dt>
				</dl>
			</div>
			<!-- end of sub navigation menu -->

			<!-- begin infobox div with nifty corners applied -->
			<div class="infobox">

				<!-- this section displays the selected page of items for the quotation along with their modules if necessary -->
				<div id="viewquotationitems-tab">
					<form:form path="command" name="searchform" id="searchform"
						method="post">
						<form:hidden path="pageNo" />
						<form:hidden path="resultsPerPage" />
						<form:hidden path="quotationid" />
						<t:paginationControl resultset="${prs}" />
						<c:set var="rowcount" value="0" />
						<c:forEach var="heading" items="${quotationItemMap.keySet()}">
							<h5 class="text-center">
								<spring:message code="quotaddinstmod.heading" /> - ${heading.headingName}
							</h5>
							<c:forEach var="serviceType"
								items="${quotationItemMap.get(heading).keySet()}">
								<c:set var="caltypename">
									<cwms:besttranslation
										translations="${serviceType.shortnameTranslation}" />
								</c:set>
								<c:set var="caltype" value="${serviceType.calibrationType}" />
								<c:set var="qinotecount" value="0" />
								<c:set var="mnotecount" value="0" />												
								<c:set var="rowcount" value="1" />
								<table class="default4 quotationitems"
									summary="<spring:message code='viewquot.tableitemsspeccaltypespechead'/>">
									<thead>
										<tr>
											<td colspan="11">
												<cwms:besttranslation
													translations="${serviceType.longnameTranslation}" />
												<a href="#" class="QIDel float-right pad-right hid"
												onclick=" event.preventDefault(); compileAndSubmitQisToDelete(${quotation.id},${currentContact.personid},${allocatedSubdiv.key}); ">
													<img src="img/icons/delete.png" height="16" width="16"
													alt="<spring:message code='viewquot.deleteselectedqis'/>"
													title="<spring:message code='viewquot.deleteselectedqis'/>" />
												</a>
												<a href="#"
												class="QICopy mainlink-float pad-right marg-top-small hid"
												onclick=" event.preventDefault(); compileAndSubmitQisToCopy(${quotation.id}); "><spring:message
														code="viewquot.copyitemsmodelsonly" /></a>
												<!-- calibration requirements key -->
												<div class="instcalreq_highlight_key marg-top-small">
													<spring:message
														code="viewquot.instrumentcalibrationrequirement" />
												</div>
												<div class="compmodelcalreq_highlight_key marg-top-small">
													<spring:message
														code="viewquot.companymodelcalibrationrequirement" />
												</div>
											</td>
										</tr>
										<tr>
											<th class="unit" scope="col" colspan="3"><spring:message
													code="viewquot.itemunit" /></th>
											<th scope="col">
												<c:choose>
													<c:when test="${sessQuoteItemNoteVisibility}">
														<a href="#"
															onclick=" event.preventDefault(); toggleNoteDisplay(this, 'QUOTEITEMNOTE', 'hid', ${quotation.id}); "
															class="toggleNoteDisplay">
															<img src="img/icons/note_collapse.png" width="20"
															height="16"
															alt="<spring:message code='viewquot.collapseallnotes'/>"
															title="<spring:message code='viewquot.collapseallnotes'/>" />
														</a>
													</c:when>
													<c:otherwise>
														<a href="#"
															onclick=" event.preventDefault(); toggleNoteDisplay(this, 'QUOTEITEMNOTE', 'vis', ${quotation.id}); "
															class="toggleNoteDisplay">
															<img src="img/icons/note_expand.png" width="20"
															height="16"
															alt="<spring:message code='viewquot.expandallnotes'/>"
															title="<spring:message code='viewquot.expandallnotes'/>" />
														</a>
													</c:otherwise>
												</c:choose>
											</th> 
											<th class="caltype" scope="col"><spring:message
													code="servicetype" /></th>  
											<th class="plantno" scope="col"><spring:message
													code="editquot.referenceno" /></th>  
											<th class="discount" scope="col"><spring:message
													code="viewquot.disc" /></th>
											<th class="qty" scope="col"><spring:message
													code="quotaddinstmod.qty" /></th>
											<th class="unitcost" scope="col"><spring:message
													code="newquotevers.unitcost" /></th>
											<th> <spring:message code="editquot.totalcharges" /> </th>  
											<th class="del" scope="col">
												<input type="checkbox" name="qiDeleteAll"
												onclick=" toggleQIDeleteItems(this.checked, ${heading.headingId}, ${caltype.calTypeId}); "
												title="<spring:message code='viewquot.selectallquoteitems'/>"
												<c:if test="${quotation.quotestatus.issued}"> disabled="disabled" </c:if> />
											</th>
										</tr>
									</thead>
									<tfoot>
										<tr class="hid QIDel">
											<td colspan="11">
												<a href="#" class="float-right pad-right"
												onclick=" event.preventDefault(); compileAndSubmitQisToDelete(${quotation.id},${currentContact.personid},${allocatedSubdiv.key}); ">
													<img src="img/icons/delete.png" height="16" width="16"
													alt="<spring:message code='viewquot.deleteselectedqis'/>"
													title="<spring:message code='viewquot.deleteselectedqis'/>" />
												</a>
												<a href="#"
												class="QICopy mainlink-float pad-right marg-top-small hid"
												onclick=" event.preventDefault(); modifyQuotationItemsOverlay(${quotation.id}); "><spring:message
														code="viewquot.modifyitems" /></a>
											</td>
										</tr>
										<c:set var="moveCalType" value="${caltype.calTypeId}" />
										<c:if test="${quotation.sortType == 'CUSTOM'}">
											<c:set var="optionList" value="" />
											<c:set var="optionCount" value="0" />
											<c:forEach var="quoteitem" items="${heading.quoteitems}">
												<c:if test="${not quoteitem.partOfBaseUnit}">
													<c:if test="${moveCalType == quoteitem.caltype.calTypeId}">
														<c:set var="optionList">${optionList}<option
																value='${quoteitem.id}'>${quoteitem.itemno}</option>
														</c:set>
														<c:set var="optionCount" value="${optionCount + 1}" />
													</c:if>
												</c:if>
											</c:forEach>
											<c:if test="${optionCount > 1}">
												<tr>
													<td colspan="10">
														<spring:message code="viewquot.moveitem" />
														<select style="width: 40px;">
															<c:out value="${optionList}" />
														</select>
														<spring:message code="viewquot.toitem" />
														<select style="width: 40px;">
															<c:out value="${optionList}" />
														</select>
														<input type="button"
														value="<spring:message code='viewquot.moveitem'/>"
														class="moveQIButton"
														onclick=" event.preventDefault(); moveQuotationItem($j(this).prev().prev().val(), $j(this).prev().val(), $heading.headingId, $moveCalType); " />
													</td>
												</tr>
											</c:if>
										</c:if>
									</tfoot>
									<c:set var="itemno" value="1" />
									<c:forEach var="quoteitem"
										items="${quotationItemMap.get(heading).get(serviceType)}">
										<c:if test="${not quoteitem.partOfBaseUnit}">
											<tbody>
												<c:set var="comp"
													value="${quoteitem.quotation.contact.sub.comp}" />
												<c:set var="qinotecount"
													value="${quoteitem.publicActiveNoteCount + quoteitem.privateActiveNoteCount}" />
												<%-- In velocity, was set to 0 if empty or object if present, replaced with not empty test in JSP --%>
												
												<c:set var="crForQi" value="" />
												<c:if test="${calReqs.containsKey(quoteitem.id)}">
													<c:set var="crForQi" value="${calReqs.get(quoteitem.id)}" />
													<c:set var="qinotecount" value="${qinotecount + 1}" />
												</c:if>
												<c:set var="rowid">item${quoteitem.id}</c:set>
												<c:choose>
													<c:when test="${sessSetScrollTopMarker == rowid}">
														<c:set var="backgroundColor" value="#B9DCFF" />
													</c:when>
													<c:otherwise>
														<c:set var="backgroundColor"
															value="${quoteitem.serviceType.displayColour}" />
													</c:otherwise>
												</c:choose>
												<tr id="${rowid}"
													style=" background-color: ${backgroundColor}; ">
													<td class="marker" scope="col">
														<c:choose>
															<c:when test="${sessSetScrollTopMarker == rowid}">
																<c:set var="markerClass" value="quotationMarked" />
															</c:when>
															<c:otherwise>
																<c:set var="markerClass" value="quotationMarker" />
															</c:otherwise>
														</c:choose>
														<a href="#" class="${markerClass}"
														onclick=" event.preventDefault(); setScrollTopAttribute(this, ${quotation.id}, ${quoteitem.id}); ">&nbsp;</a>
														<input type="hidden"
														value="${quoteitem.serviceType.displayColour}" />
													</td>
													<td class="itemno" scope="col">
														${quoteitem.itemno}
													</td>
													<td
														<c:if test="${not empty quoteitem.inst}"> class="qiinst" </c:if>>
														<c:choose>
														    <c:when test="${not empty quoteitem.inst}">
															    <links:instrumentLinkDWRInfo
																	instrument="${quoteitem.inst}" rowcount="0"
																	displayBarcode="true" displayName="false"
																	displayCalTimescale="true" />
		                                                        <a
																	href="viewinstrument.htm?plantid=${quoteitem.inst.plantid}"><cwms:showinstrument
																		instrument="${quoteitem.inst}" /></a>
		                                                        <c:if
																	test="${not empty quoteitem.inst.customerDescription}">
																	<c:out value=" / ${quoteitem.inst.customerDescription}" />
																</c:if>
														    </c:when>
														    <c:otherwise>
																<a
																	href="instrumentmodel.htm?modelid=${quoteitem.model.modelid}"><cwms:showmodel
																		instrumentmodel="${quoteitem.model}" /></a>
															</c:otherwise>
														</c:choose>
													</td>
													
													<c:if test="${not empty crForQi}">
														<c:choose>
															<c:when test="${crForQi.classKey == 'instrument'}">
																<c:choose>
																	<c:when test="${crForQi.publish}">
																		<c:set var="notesClass"
																			value="instcalreq_highlight_doc" />
																	</c:when>
																	<c:otherwise>
																		<c:set var="notesClass" value="instcalreq_highlight" />
																	</c:otherwise>
																</c:choose>
															</c:when>
															<c:otherwise>
																<c:choose>
																	<c:when test="${crForQi.publish}">
																		<c:set var="notesClass"
																			value="compmodelcalreq_highlight_doc" />
																	</c:when>
																	<c:otherwise>
																		<c:set var="notesClass"
																			value="compmodelcalreq_highlight" />
																	</c:otherwise>
																</c:choose>
															</c:otherwise>
														</c:choose>
													</c:if>
													
													<td class="notes ${notesClass}" scope="col">
														<links:showNotesLinks noteTypeId="${quoteitem.id}"
															noteType="QUOTEITEMNOTE" colspan="10"
															notecounter="${qinotecount}" />
														<c:choose>
															<c:when test="${not empty crForQi}">
																<c:choose>
																	<c:when test="${crForQi.classKey == 'instrument'}">
																		<a href="#"
																			class="calReqButton-${crForQi.inst.plantid} editPoints"
																			onclick=" event.preventDefault(); loadScript.createOverlay('external', 'Edit Cal Points &amp; Requirements', 'calibrationpoints.htm?type=instrument&page=quotation&calreqid=${crForQi.id}', null, null, 80, 900, null); "
																			title="<spring:message code='viewquot.editpointsrequirements'/>">&nbsp;</a>
																	</c:when>
																	<c:otherwise>
																		<a href="#"
																			class="calReqButton-${crForQi.model.modelid} editPoints"
																			onclick=" event.preventDefault(); loadScript.createOverlay('external', 'Edit Cal Points &amp; Requirements', 'calibrationpoints.htm?type=companymodel&page=quotation&calreqid=${crForQi.id}', null, null, 80, 900, null); "
																			title="<spring:message code='viewquot.editpointsrequirements'/>">&nbsp;</a>
																		<c:if test="${not empty quoteitem.inst}">
																			<a href="#"
																				class="calReqButton-${quoteitem.inst.plantid} addPoints"
																				onclick=" event.preventDefault(); loadScript.createOverlay('external', 'Add Cal Points &amp; Requirements', 'calibrationpoints.htm?type=instrument&page=quotation&plantid=${quoteitem.inst.plantid}', null, null, 80, 900, null); "
																				title="<spring:message code='viewquot.addpointsrequirements'/>">&nbsp;</a>
																		</c:if>
																	</c:otherwise>
																</c:choose>
															</c:when>
															<c:otherwise>
																<c:choose>
																	<c:when test="${not empty quoteitem.inst}">
																		<a href="#"
																			class="calReqButton-${quoteitem.inst.plantid} addPoints"
																			onclick=" event.preventDefault(); loadScript.createOverlay('external', 'Add Cal Points &amp; Requirements', 'calibrationpoints.htm?type=instrument&page=quotation&plantid=${quoteitem.inst.plantid}', null, null, 80, 900, null); "
																			title="<spring:message code='viewquot.addpointsrequirements'/>">&nbsp;</a>
																	</c:when>
																	<c:otherwise>
																		<a href="#"
																			class="calReqButton-${quoteitem.model.modelid} addPoints"
																			onclick=" event.preventDefault(); loadScript.createOverlay('external', 'Add Cal Points &amp; Requirements', 'calibrationpoints.htm?type=companymodel&page=quotation&compid=${comp.coid}&modelid=${quoteitem.model.modelid}', null, null, 80, 900, null); "
																			title="<spring:message code='viewquot.addpointsrequirements'/>">&nbsp;</a>
																	</c:otherwise>
																</c:choose>
															</c:otherwise>
														</c:choose>														
														<!-- these values provided for calibration requirements - do not remove or move -->
														<input type="hidden" class="itemCalReqNoteType"
														value="QUOTEITEMNOTE" />
														<input type="hidden" class="itemCalReqQuoteitemId"
														value="${quoteitem.id}" />
														<c:choose>
															<c:when test="${not empty quoteitem.inst}">
																<input type="hidden" class="itemCalReqPlantId"
																	value="${quoteitem.inst.plantid}" />
																<input type="hidden" class="itemCalReqModelId"
																	value="${quoteitem.inst.model.modelid}" />
															</c:when>
															<c:otherwise>
																<input type="hidden" class="itemCalReqModelId"
																	value="${quoteitem.model.modelid}" />
															</c:otherwise>
														</c:choose>
														<input type="hidden" class="itemCalReqCompId"
														value="${comp.coid}" />															
													</td>
													<td class="center"><cwms:besttranslation
															translations="${quoteitem.serviceType.shortnameTranslation}" /></td>
													<td>${quoteitem.referenceNo}</td>
													<td class="cost2pc">
														<c:choose>
															<c:when
																test="${quoteitem.calibrationCost.active && quoteitem.purchaseCost.active}">
																<c:set var="totalDiscount"
																	value="${quoteitem.calibrationCost.discountValue + quoteitem.purchaseCost.discountValue}" /> 
															</c:when>
															<c:when test="${quoteitem.calibrationCost.active}">
																<c:set var="totalDiscount"
																	value="${quoteitem.calibrationCost.discountValue}" />
															</c:when>
															<c:otherwise>
																<c:set var="totalDiscount"
																	value="${quoteitem.purchaseCost.discountValue}" />
															</c:otherwise>
														</c:choose>
														${quotation.currency.currencyERSymbol} ${totalDiscount}
													</td>
													<td class="center">${quoteitem.quantity}</td>
													<td
														class="cost2pc <c:if test="${(not empty quoteitem.calibrationCost.linkedCostSrc.tpQuoteItem) || (not empty quoteitem.calibrationCost.thirdCostSrc) && (quoteitem.calibrationCost.thirdCostTotal > 0)}"> tpcost_highlight </c:if>">														
														<a href="editquotationitem.htm?id=${quoteitem.id}"
														class="mainlink">
															${quotation.currency.currencyERSymbol} ${quoteitem.unitCost}
														</a>
											
													</td>
													<td class="center"> ${quotation.currency.currencyERSymbol} ${quoteitem.finalCost}  </td>
													<td class="center">
														<input type="checkbox"
														name="qiDelete_${heading.headingId}_${quoteitem.caltype.calTypeId}"
														value="${quoteitem.id}" onclick=" monitorCheckedItems(); "
														title="<spring:message code='viewquot.deletequoteitem'/>"
														<c:if test="${quotation.quotestatus.issued}"> disabled="disabled" </c:if> />
													</td>
												</tr>
												
												<%-- Used in 2 places below --%>
												<c:choose>
													<c:when test="${sessQuoteItemNoteVisibility}">
														<c:set var="noteVisibility" value="vis" />
													</c:when>
													<c:otherwise>
														<c:set var="noteVisibility" value="hid" />
													</c:otherwise>
												</c:choose>
												<c:if test="${qinotecount > 0}">
													<tr id="QUOTEITEMNOTE${quoteitem.id}"
														class="${noteVisibility}">
														<td colspan="10" class="nopadding">
															<t:showActiveNotes entity="${quoteitem}" links="true"
																noteType="QUOTEITEMNOTE" noteTypeId="${quoteitem.id}"
																calReqs="${crForQi}" privateOnlyNotes="false"
																contact="${currentContact}" />
														</td>
													</tr>
												</c:if>
											</tbody>
												
											<c:set var="thisBaseUnit" value="false" />
											<c:if test="${not empty quoteitem.modules}">
												<tbody class="modulerows">
													<tr style="display: none;">
														<td colspan="10"></td>
													</tr>
													<c:forEach var="module" items="${quoteitem.modules}">
														<c:set var="thisBaseUnit" value="${true}" />
														<c:set var="mnotecount"
															value="${module.publicActiveNoteCount + module.privateActiveNoteCount}" />												
														<%-- In velocity, was set to 0 if empty or object if present, replaced with not empty test in JSP --%>
														<c:set var="crForQi" value="" />
														<c:if test="${calReqs.containsKey(module.model.modelid)}">
															<c:set var="crForQi"
																value="${calReqs.get(module.model.modelid)}" />
															<c:set var="mnotecount" value="${mnotecount + 1}" />
														</c:if>
														<tr id="item${module.id}"
															style=" background-color: ${module.caltype.serviceType.displayColour}; ">
															<td colspan="2"><img src="img/icons/arrow_merge.png"
																height="16" width="16"
																alt="<spring:message code='editquothead.partofbaseunit'/>"
																title="<spring:message code='editquothead.partofbaseunit'/>" /></td>
															<td>
																<c:choose>
																	<c:when test="${not empty module.inst}">
																		<links:instrumentLinkDWRInfo
																			instrument="${module.inst}"
																			rowcount="${module.itemno}" displayBarcode="true"
																			displayName="true" displayCalTimescale="true"
																			caltypeid="${module.caltype.calTypeId}" />
																	</c:when>
																	<c:otherwise>
																		<t:showViewModelLink instrumentmodel="${module.model}" />
																	</c:otherwise>
																</c:choose>																																									
															</td>
															<td>
																<links:showNotesLinks notecounter="${mnotecount}"
																	noteType="QUOTEITEMNOTE" noteTypeId="${module.id}"
																	colspan="10"></links:showNotesLinks>
																<c:choose>
																	<c:when test="${not empty crForQi}">
																		<a href="#"
																			class="calReqButton-${module.model.modelid} editPoints"
																			onclick=" event.preventDefault(); loadScript.createOverlay('external', 'Edit Cal Points &amp; Requirements', 'calibrationpoints.htm?type=companymodel&page=quotation&calreqid=${crForQi.id}', null, null, 80, 900, null); "
																			title="<spring:message code='viewquot.editpointsrequirements'/>">&nbsp;</a>
																	</c:when>
																	<c:otherwise>
																		<a href="#"
																			class="calReqButton-${module.model.modelid} addPoints"
																			onclick=" event.preventDefault(); loadScript.createOverlay('external', 'Add Cal Points &amp; Requirements', 'calibrationpoints.htm?type=companymodel&page=quotation&compid=${comp.coid}&modelid=${module.model.modelid}', null, null, 80, 900, null); "
																			title="<spring:message code='viewquot.addpointsrequirements'/>">&nbsp;</a>
																	</c:otherwise>
																</c:choose>
																<!-- these values provided for calibration requirements - do not remove or move -->
																<input type="hidden" class="itemCalReqNoteType"
																value="QUOTEITEMNOTE" />
																<input type="hidden" class="itemCalReqQuoteitemId"
																value="${module.id}" />
																<input type="hidden" class="itemCalReqModelId"
																value="${module.model.modelid}" />
																<input type="hidden" class="itemCalReqCompId"
																value="${comp.coid}" />
															</td>
															<td class="center"><cwms:besttranslation
																	translations="${module.caltype.serviceType.shortnameTranslation}" /></td>
															<td>${module.referenceNo}</td>
															<td class="cost">
																<c:choose>
																	<c:when
																		test="${module.calibrationCost.active && module.purchaseCost.active}">
																		<c:set var="totalDiscount"
																			value="${module.calibrationCost.discountValue + module.purchaseCost.discountValue}" />
																	</c:when>
																	<c:when test="${module.calibrationCost.active}">
																		<c:set var="totalDiscount"
																			value="${module.calibrationCost.discountValue}" />
																	</c:when>
																	<c:otherwise>
																		<c:set var="totalDiscount"
																			value="${module.purchaseCost.discountValue}" />
																	</c:otherwise>
																</c:choose>
																${quotation.currency.currencyERSymbol} ${totalDiscount}
															</td>
															<td class="center">${module.quantity}</td>
															<td class="cost">
																<c:if
																	test="${module.calibrationCost.thirdCostSrc && (module.calibrationCost.thirdCostTotal > 0)}">
																	<img src="img/icons/linked_tpc.png" class="float-left"
																		width="26" height="12" />
																</c:if>
																<a href="editquotationitem.htm?id=${module.id}"
																class="mainlink">
																	${quotation.currency.currencyERSymbol} ${module.unitCost}
																</a>
															
															</td>
															<td class="center">
																<input type="checkbox"
																name="qiDelete_${heading.headingId}_${quoteitem.caltype.calTypeId}"
																value="${module.id}" onclick=" monitorCheckedItems(); "
																title="<spring:message code='viewquot.deletequoteitem'/>"
																<c:if test="${quotation.quotestatus.issued}">disabled="disabled" </c:if> />
															</td>
														</tr>
														<c:if test="${mnotecount > 0}">
															<tr id="QUOTEITEMNOTE${module.id}"
																class="${noteVisibility}">
																<td colspan="10" class="nopadding">
																	<t:showActiveNotes entity="${module}" links="true"
																		privateOnlyNotes="QUOTEITEMNOTE"
																		noteType="${module.id}" noteTypeId="${crForQi}"
																		contact="${currentContact}" />
																</td>
															</tr>
														</c:if>
														
													</c:forEach>
													<c:if
														test="${thisBaseUnit || (quoteitem.model && not empty quoteitem.model.thisBaseUnitsModules)}">
														<tr id="basetotal">
															<td colspan="4">
															<a href="#"
																onclick=" event.preventDefault(); addModulesToBaseUnit(${quoteitem.id}); "
																class="mainlink"><spring:message
																		code="viewquot.addmodules" /></a>
															</td>
															<td colspan="4" class="right bold">
																<spring:message
																	code="viewquot.totalforbaseunitandmodules" />:
															</td>
															<td class="cost bold">
																${quotation.currency.currencyERSymbol} ${quoteitem.getUnitsTotalCost()}
													
															</td>
															<td></td>
														</tr>
													</c:if>
												</tbody>
											</c:if>
											<c:set var="rowcount" value="${rowcount + 1}" />
										</c:if>															
									</c:forEach>
								</table>																					
							</c:forEach>
						</c:forEach>
						<t:paginationControl resultset="${prs}"
							hideNoresultsMessage="true" />
					</form:form>
				</div>
				
				<!-- tthis section displays a summary of the quotation -->
				<div id="summary-tab" class="hid">
					<a href="quoteitemdownload.xlsx?quoteid=${quotation.id}"><img
						src="img/doctypes/excel_small.gif" width="20" height="20"
						alt="Export To Excel" class="img-right"></a>
					<c:if test="${(quotationItemCount == 0)}">
						<table class="default4"
							summary="<spring:message code='editquothead.tablenoquotation'/>">
							<tbody>
								<tr class="nobord">
									<td class="totalsum text-center bold"><spring:message
											code="viewquot.noitemsquot" /></td>
								</tr>
							</tbody>
						</table>
					</c:if>
					<c:set var="rowcount" value="0" />
					<c:if
						test="${quotation.headingCaltypeTotalVisible || quotation.headingTotalVisible}">
					<c:forEach var="heading" items="${quotation.quoteheadings}">
						<h5 class="text-center">
							<spring:message code="quotaddinstmod.heading" /> - ${heading.headingName}
						</h5>
						<c:choose>
							<c:when test="${empty heading.quoteitems}">
								<table class="default4"
										summary="<spring:message code='editquothead.tablenoquotation'/>">
									<tbody>
										<tr class="nobord">												
											<td class="totalsum text-center bold"><spring:message
														code="editquothead.noitemsquoteheading" /></td>
										</tr>
									</tbody>
								</table>
							</c:when>
						</c:choose>
						<%-- this displays the quote heading caltype totals if set to true in edit quotation section --%>
						<c:if test="${quotation.headingCaltypeTotalVisible}">
							<c:forEach var="serviceType" items="${serviceTypes}">
								<c:if
										test="${pricingtool.isServiceTypeInQuoteHeading(heading.headingId, serviceType.serviceTypeId)}">
									<table class="default4 quotationitems_subtotal"
											summary="<spring:message code='viewquot.tabletotalcost'/>">
										<tbody>
											<tr class="nobord">
												<td class="firstfill">&nbsp;</td>											
												<td class="totalsum cost2pcTotal bold">
													<spring:message code="viewquot.total" />&nbsp;
													${heading.headingName}&nbsp;
													<cwms:besttranslation
															translations="${serviceType.shortnameTranslation}" />&nbsp;
													<spring:message code="cost" />:
												</td>
												<td class="totalamount cost2pcTotal bold">																															
													${currency.currencyERSymbol}${pricingtool.calculateQuoteHeadingServiceTypeTotal(heading, serviceType.serviceTypeId)}
												</td>
												<td class="filler">
													<c:choose>
														<c:when test="${quotation.showHeadingCaltypeTotal}">
															<a href="#"
																	class="showTotalHeadingCaltypeValue${quotation.id}"
																	onclick=" event.preventDefault(); updateHeadingCaltypeTotalVisibility(this, false, ${quotation.id}); ">
																<img src="img/icons/page_tick.png" width="21"
																	height="19" class="img_marg_bot"
																	alt="<spring:message code='viewquot.totaldisplayedondocumentscreated'/>"
																	title="<spring:message code='viewquot.totaldisplayedondocumentscreated'/>" />
															</a>
														</c:when>
														<c:otherwise>
															<a href="#"
																	class="showTotalHeadingCaltypeValue${quotation.id}"
																	onclick=" event.preventDefault(); updateHeadingCaltypeTotalVisibility(this, true, ${quotation.id}); ">
																<img src="img/icons/page_cross.png" width="21"
																	height="19" class="img_marg_bot"
																	alt="<spring:message code='viewquot.totalnotdisplayedondocumentscreated'/>"
																	title="<spring:message code='viewquot.totalnotdisplayedondocumentscreated'/>" />
															</a>
														</c:otherwise>
													</c:choose>
												</td>
											</tr>
										</tbody>
									</table>
								</c:if>
							</c:forEach>
						</c:if>
						<%-- this displays the quote heading totals if set to true in edit quotation section --%>
						<c:if test="${quotation.headingTotalVisible}">									
							<table class="default4 quotationitems_subtotal"
									summary="<spring:message code='viewquot.tabletotalcostcurhead'/>">
								<tbody>
									<tr class="nobord">
										<td class="firstfill">&nbsp;</td>												
										<td class="totalsum cost2pcTotal bold">
											<spring:message code="viewquot.total" />&nbsp;
											${heading.headingName}&nbsp;
											<spring:message code="viewquot.headingcost" />:
										</td>
										<td class="totalamount cost2pcTotal bold">																														
											${currency.currencyERSymbol}${pricingtool.calculateQuoteHeadingTotal(heading)}
										</td>
										<td class="filler">
											<c:choose>
												<c:when test="${quotation.showHeadingTotal}">
													<a href="#" class="showTotalHeadingValue${quotation.id}"
															onclick=" event.preventDefault(); updateHeadingTotalVisibility(this, false, ${quotation.id}); ">
														<img src="img/icons/page_tick.png" width="21" height="19"
															class="img_marg_bot"
															alt="<spring:message code='viewquot.totaldisplayedondocumentscreated'/>"
															title="<spring:message code='viewquot.totaldisplayedondocumentscreated'/>" />
													</a>
												</c:when>
												<c:otherwise>
													<a href="#" class="showTotalHeadingValue${quotation.id}"
															onclick=" event.preventDefault(); updateHeadingTotalVisibility(this, true, ${quotation.id}); ">
														<img src="img/icons/page_cross.png" width="21" height="19"
															class="img_marg_bot"
															alt="<spring:message code='viewquot.totalnotdisplayedondocumentscreated'/>"
															title="<spring:message code='viewquot.totalnotdisplayedondocumentscreated'/>" />
													</a>
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
								</tbody>
							</table>
						</c:if>
					</c:forEach>
					</c:if>
					<%-- this displays the quote calibration type totals if set to true in edit quotation section  --%>
					<h5>
						<spring:message code="quotation" />
					</h5>
					<c:if test="${quotation.quoteCaltypeTotalVisible}">
						<c:forEach var="serviceType" items="${serviceTypes}">
							<c:if
								test="${pricingtool.isServiceTypeInQuotation(serviceType.serviceTypeId)}">
								<table class="default4 quotationitems_subtotal"
									summary="<spring:message code='viewquot.tabletotalcostcaltype'/>">
									<tbody>
										<tr class="nobord">
											<td class="firstfill">&nbsp;</td>												
											<td class="totalsum cost2pcTotal bold">
												<spring:message code="quotation" />&nbsp;
												<cwms:besttranslation
													translations="${serviceType.shortnameTranslation}" />&nbsp;
												<spring:message code="cost" />:
											</td>
											<td class="totalamount cost2pcTotal bold">																														
												${currency.currencyERSymbol}${pricingtool.calculateQuoteServiceTypeTotal(quotation, serviceType.serviceTypeId)}												
											</td>
											<td class="filler">
												<c:choose>
													<c:when test="${quotation.showQuoteCaltypeTotal}">
														<a href="#"
															class="showTotalQuoteCaltypeValue${quotation.id}"
															onclick=" event.preventDefault(); updateQuotationCaltypeTotalVisibility(this, false, ${quotation.id}); ">
															<img src="img/icons/page_tick.png" width="21" height="19"
															class="img_marg_bot"
															alt="<spring:message code='viewquot.totaldisplayedondocumentscreated'/>"
															title="<spring:message code='viewquot.totaldisplayedondocumentscreated'/>" />
														</a>
													</c:when>
													<c:otherwise>
														<a href="#"
															class="showTotalQuoteCaltypeValue${quotation.id}"
															onclick=" event.preventDefault(); updateQuotationCaltypeTotalVisibility(this, true, ${quotation.id}); ">
															<img src="img/icons/page_cross.png" width="21"
															height="19" class="img_marg_bot"
															alt="<spring:message code='viewquot.totalnotdisplayedondocumentscreated'/>"
															title="<spring:message code='viewquot.totalnotdisplayedondocumentscreated'/>" />
														</a>
													</c:otherwise>
												</c:choose>
											</td>
										</tr>
									</tbody>
								</table>
							</c:if>
						</c:forEach>
					</c:if>
					
					<table class="default4 quotationitems_total"
						summary="<spring:message code='viewquot.tablealltotalcost'/>">
						<tbody>
							<tr class="nobord">
								<td class="firstfill">&nbsp;</td>										
								<td class="totalsum cost2pcTotal bold"><spring:message
										code="viewquot.quotationtotalprice" />:</td>
								<td class="totalamount cost2pcTotal bold">														
									<c:out value="${quotation.totalCost} " />${currency.currencyERSymbol}															
								</td>
								<td class="filler">
									<c:choose>
										<c:when test="${quotation.showTotalQuoteValue}">
											<a href="#"
												onclick=" event.preventDefault(); updateTotalVisibility(this, false, ${quotation.id}); ">
												<img src="img/icons/page_tick.png" width="21" height="19"
												class="img_marg_bot"
												alt="<spring:message code='viewquot.totaldisplayedondocumentscreated'/>"
												title="<spring:message code='viewquot.totaldisplayedondocumentscreated'/>" />
											</a>
										</c:when>
										<c:otherwise>
											<a href="#"
												onclick=" event.preventDefault(); updateTotalVisibility(this, true, ${quotation.id}); ">
												<img src="img/icons/page_cross.png" width="21" height="19"
												class="img_marg_bot"
												alt="<spring:message code='viewquot.totalnotdisplayedondocumentscreated'/>"
												title="<spring:message code='viewquot.totalnotdisplayedondocumentscreated'/>" />
											</a>
										</c:otherwise>
									</c:choose>
								</td>
							</tr>
							<c:if test="${showVAT}">
								<tr class="nobord">
									<td class="firstfill">&nbsp;</td>										
									<td class="totalsum cost2pcTotal bold">
										<spring:message code="viewjob.vat" />
										<c:out value=" ( ${quotation.vatRate} % ) :" />
									</td>
									<td class="totalamount cost2pcTotal bold">
										<c:out value="${quotation.vatValue} " />${currency.currencyERSymbol}
									</td>
									<td class="filler">
									</td>
								</tr>
								<tr class="nobord">
									<td class="firstfill">&nbsp;</td>										
									<td class="totalsum cost2pcTotal bold"><spring:message
											code="viewquot.quotationfinalprice" />:</td>
									<td class="totalamount cost2pcTotal bold">
										<c:out value="${quotation.finalCost} " />${currency.currencyERSymbol}
									</td>
									<td class="filler">
									</td>
								</tr>
							</c:if>
						</tbody>
					</table>
					
					<c:if test="${discountCalculator.discounts > 0}">
						<table class="default4 quotationitems_total"
							summary="<spring:message code='viewquot.tablediscount'/>">
							<tbody>											
								<tr class="nobord">	
									<td class="firstfill">&nbsp;</td>											
									<td class="totalsum cost2pcTotal bold"><spring:message
											code="viewquot.discountsapplied" /> (${discountCalculator.discounts}):</td>
									<td class="totalamount cost2pcTotal bold">
										${currency.currencyERSymbol}
										${discountCalculator.discountValue}
									</td>
									<td class="filler">&nbsp;</td>
								</tr>											
							</tbody>
						</table>
					</c:if>
				</div>
				<!-- end of quote summary section -->
				
				<!-- this section contains links to create a new quote document and view these documents -->
				<div id="quotedocs-tab" class="hid">
					<files:showFilesForSC entity="${quotation}" sc="${sc}"
						id="${quotation.id}" identifier="${quotation.qno}"
						ver="${quotation.ver}" rootFiles="${scRootFiles}"
						allowEmail="true" isEmailPlugin="false"
						rootTitle="<spring:message code='viewquot.filesforquotation'/> ${quotation.qno} <spring:message code='quotaddinstmod.version'/> ${quotation.ver}"
						deleteFiles="true" />
				</div>
				<!-- end of quote documents section -->
				
				<!-- this section contains third party quotes for this quotation -->
				<div id="tpquoterequests-tab" class="hid">
																							
					<h5>
						<spring:message code="viewquot.supplierquotes" />
					</h5>
				
					<!-- table requires any ID for the jQuery tablesorter to work! -->
					<table id="supplierquotetable" class="default2 tablesorter"
						summary="<spring:message code='viewquot.tablethirdparty'/>">
						<thead>
							<tr>
								<td colspan="5">
									<c:set var="countTpQuoteRequests"
										value="${quotation.tpQuoteRequests.size()}" />
									<c:choose>
										<c:when test="${countTpQuoteRequests == 1}">
											<spring:message code="viewquot.thereis" />
											<c:out value=" ${quotation.tpQuoteRequests.size()} " />
											<spring:message
												code="viewquot.thirdpartyquotationrequestthatorginatedfromthisquotation" />
										</c:when>
										<c:otherwise>
											<spring:message code="viewquot.thereare" />
											<c:out value=" ${quotation.tpQuoteRequests.size()} " />
											<spring:message
												code="viewquot.thirdpartyquotationrequeststhatorginatedfromthisquotation" />
										</c:otherwise>
									</c:choose>
								</td>
							</tr>
							<tr>
								<th scope="col"><spring:message code="createquot.requestno" /></th>  
								<th scope="col"><spring:message code="company" /></th>  
								<th scope="col"><spring:message code="contact" /></th>  
								<th scope="col"><spring:message code="status" /></th>
								<th scope="col"><spring:message code="createquot.issuedate" /></th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="5"><a
									href="tpquoterequestform.htm?quoteid=${quotation.id}"
									class="mainlink-float"><spring:message
											code="viewquot.requestathirdpartyquotation" /></a></td>
							</tr>
						</tfoot>
						<tbody class="keynavResults">
							<c:choose>
								<c:when test="${empty quotation.tpQuoteRequests}">
									<tr class="odd">
										<td colspan="5" class="center bold"><spring:message
												code="viewquot.nothirdpartyrequests" /></td>
									</tr>
								</c:when>
								<c:otherwise>
									<c:forEach var="tpQuoteRequest"
										items="${quotation.tpQuoteRequests}">
										<c:choose>
											<c:when test="${rowcount % 2 == 0}">
												<c:set var="rowClass" value="even" />
											</c:when>
											<c:otherwise>
												<c:set var="rowClass" value="odd" />
											</c:otherwise>
										</c:choose>
										<tr class="rowClass}"
											onclick="window.location.href='viewtpquoterequest.htm?id=${tpQuoteRequest.id}'"
											onmouseover=" $j(this).addClass('hoverrow'); "
											onmouseout=" if (this.className != 'hoverrow') $j(this).removeClass('hoverrow'); ">
											<td>${tpQuoteRequest.requestNo}</td>
											<td><links:companyLinkDWRInfo
													company="${tpQuoteRequest.contact.sub.comp}" copy="true"
													rowcount="${rowcount}" />
											
											<td><links:contactLinkDWRInfo
													contact="${tpQuoteRequest.contact}" rowcount="${rowcount}" /></td>
											<td>
												<cwms:besttranslation
													translations="${tpQuoteRequest.status.nametranslations}" />
												<div class="float-right"
													style="margin-left: 10px; margin-right: 10px;">
													<c:choose>
														<c:when test="${tpQuoteRequest.status.followingReceipt}">
															<img src="img/icons/green_light.png"
																alt="${tpQuoteRequest.status.name}"
																title="${tpQuoteRequest.status.name}" />
														</c:when>
														<c:when test="${tpQuoteRequest.status.followingIssue}">
															<img src="img/icons/amber_light.png"
																alt="${tpQuoteRequest.status.name}"
																title="${tpQuoteRequest.status.name}" />
														</c:when>
														<c:otherwise>
															<img src="img/icons/red_light.png"
																alt="${tpQuoteRequest.status.name}"
																title="${tpQuoteRequest.status.name}" />
														</c:otherwise>
													</c:choose>
												</div>
											</td>
											<td><fmt:formatDate value="${tpQuoteRequest.issuedate}" dateStyle="medium" type="date"/></td>
										</tr>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
													
				</div>
				<!-- end of third party quotation section -->
				
				<!-- this section contains prior status history information -->
				<div id="priorstatushistory-tab" class="hid">
					
					<spring:message
						code="viewquot.allstatuschangehistoryforthisquotation" />:
					<!-- <a href="#" onclick="$('priorstatushistory').style.display = 'none';">Close</a> -->
					<c:forEach var="priorstatus" items="${quotation.priorStatusList}">
						<div id="$priorstatus.pqsId" style="margin: 5px; padding: 2px;">
							
							<spring:message code="viewquot.quotationstatussetto" /> '${priorstatus.revokedTo.name}' <spring:message
								code="viewquot.on" /> <fmt:formatDate value="${priorstatus.revokedOn}" dateStyle="medium" type="both" timeStyle="short" /> by ${priorstatus.revokedBy.name}

						</div>
					</c:forEach>
					
				</div>
				<!-- end of status history section -->		
				
				<!-- this section displays the current terms and conditions -->
				<div id="termsandcond-tab" class="hid">
				
					<ul id="termsandconditions">
						<li id="one">
							<h3>
								<spring:message code="viewquot.termsnconditions" />
							</h3>							
							<div id="termscontainer">
							
								<p>
									<span>
									<c:choose>
										<c:when test="${quotation.usingDefaultTermsAndConditions}">
											(<spring:message
													code="viewquot.usingsystemdefaulttermsandconditions" />) 
										</c:when>
										<c:otherwise>
											(<spring:message
													code="viewquot.usingcustomtermsandconditions" />)
										</c:otherwise>
									</c:choose>
									</span>
									<a href="#"
										onclick=" event.preventDefault(); slideTermsAndCond('conditiontext', this); "
										class="mainlink"><spring:message code="viewquot.view" /></a>&nbsp;&nbsp;
									<cwms:securedLink permission="QTC_FORM"  classAttr="mainlink"  parameter="?id=${quotation.id}" collapse="True" >
										<spring:message code="edit"/>
									</cwms:securedLink>
								</p>
																						
								<div id="conditiontext" style="display: none;">
									<div id="generalconditions">
										<h4>
											<spring:message code="viewquot.generaltermsconditions" />
										</h4>
										<span>${conditions.generalCon.conditionText}</span>
									</div>
									<div id="calibrationconditions">
										<c:forEach var="con" items="${conditions.calCon}">
											<c:if test="${not empty con.conditionText}">
												<h4>
													<cwms:besttranslation
														translations="${con.caltype.serviceType.longnameTranslation}" /> <spring:message
														code="viewquot.termsnconditions" />
												</h4>
												<span>${con.conditionText}</span>
											</c:if>
										</c:forEach>
									</div>
								</div>
							</div>
						</li>
					</ul>
				
				</div>
				<!-- end of current terms and conditions section -->
				
				<!-- this section contains emails for this quotation -->
				<div id="email-tab" class="hid">
					<t:showEmails entity="${quotation}" entityId="${quotation.id}"
						entityName="Quotation" sc="${sc}" />
				</div>
				<!-- end of emails section -->
				
				<!-- this section contains linked jobs for this quotation -->
				<div id="jobs-tab" class="hid">
					
					<table class="default2 quoteJobQuoteLinks"
						summary="<spring:message code='viewquot.tablelinkedjobs'/>">
						
						<thead>
							<tr>
								<th colspan="5">
									<c:choose>
										<c:when test="${jobQuoteLinkCount != 1}">
											<spring:message code="viewquot.thereare" /> ${jobQuoteLinkCount} <spring:message
												code="viewquot.jobslinkedtothisquoteshowing" /> <span
												class="jqLinkSize">${resPerPage}</span>
										</c:when>
										<c:otherwise>
											<spring:message code="viewquot.thereis" /> ${jobQuoteLinkCount} <spring:message
												code="viewquot.joblinkedtothisquoteshowing" /> <span
												class="jqLinkSize">${resPerPage}</span>
										</c:otherwise>
									</c:choose>
								</th>
							</tr>
							<tr>
								<th class="lj_jobno" scope="col"><spring:message
										code="jobno" /></th>
								<th class="lj_comp" scope="col"><spring:message
										code="company" /></th>
								<th class="lj_cont" scope="col"><spring:message
										code="contact" /></th>
								<th class="lj_linkon" scope="col"><spring:message
										code="viewquot.linkedon" /></th>
								<th class="lj_linkby" scope="col"><spring:message
										code="viewquot.linkedby" /></th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="5"><a href="#"
									onclick=" event.preventDefault(); fetchMoreJQLinks(this, ${quotation.id}, ${resPerPage}); "
									class="mainlink"><spring:message code="viewquot.loadmore" />...</a></td>
							</tr>
						</tfoot>
																
						<tbody>											
							<c:set var="rowcount" value="1" />
							<c:choose>
								<c:when test="${not empty jobQuoteLinks}">
									<c:forEach var="lq" items="${jobQuoteLinks}">
										<c:set var="job" value="${lq.job}" />
										<c:choose>
											<c:when test="${rowcount % 2 == 0}">
												<c:set var="rowClass" value="even" />
											</c:when>
											<c:otherwise>
												<c:set var="rowClass" value="odd" />
											</c:otherwise>
										</c:choose>
										<tr class="${rowClass}">
											<td class="lj_jobno"><links:jobnoLinkDWRInfo
													jobid="${job.jobid}" jobno="${job.jobno}" copy="false"
													rowcount="${rowcount}" /></td>
											<td class="lj_comp"><links:companyLinkDWRInfo
													company="${job.con.sub.comp}" copy="false"
													rowcount="${rowcount}" /></td>
											<td class="lj_cont"><links:contactLinkDWRInfo
													contact="${job.con}" rowcount="${rowcount}" /></td>
											<td class="lj_linkon">
												<fmt:formatDate type="date" dateStyle="SHORT" value="${lq.linkedOn}" />
											</td>
											<td class="lj_linkby">${lq.linkedBy.name}</td>
										</tr>
										<c:set var="rowcount" value="${rowcount + 1}" />
									</c:forEach>
								</c:when>
								<c:otherwise>
									<tr>
										<td colspan="5" class="center bold">
											<spring:message
												code="viewquot.nojobshavebeenlinkedtothisquotation" />
										</td>													
									</tr>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>									
					
				</div>
				<!-- end of linked jobs section -->
				
				<!-- this section contains linked jobs for this quotation -->
				<div id="quoterequests-tab" class="hid">
				
					<table class="default2 quoteRequestForQuote">
						<thead>
							<tr>
								<td colspan="7">
									<spring:message
										code="viewquot.quotationrequestslinkedtothisquotation" /> (${quotation.quotationRequests.size()})
								</td>
							</tr>
							<tr>
								<th class="qrqno" scope="col"><spring:message
										code="createquot.requestno" /></th>  
								<th class="qrcomp" scope="col"><spring:message
										code="company" /></th>  
								<th class="qrcont" scope="col"><spring:message
										code="contact" /></th>  
								<th class="qrsource" scope="col"><spring:message
										code="createquot.source" /></th>
								<th class="qrregdate" scope="col"><spring:message
										code="quotresult.regdate" /></th>
								<th class="qrduedate" scope="col"><spring:message
										code="duedate" /></th>
								<th class="qrdetails" scope="col"><spring:message
										code="viewquot.details" /></th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="7">&nbsp;</td>
							</tr>
						</tfoot>
						<tbody>
							<c:set var="rowcount" value="1" />
							<c:choose>
								<c:when test="${not empty quotation.quotationRequests}">
									<c:forEach var="qrequest"
										items="${quotation.quotationRequests}">
										<tr id="qr${webrequest.id}">
											<td class="qrqno">${qrequest.requestNo}</td>
											<td class="qrcomp">
												<c:choose>
													<c:when test="${qrequest.detailsSource == 'EXISTING'}">
														<links:companyLinkDWRInfo company="${qrequest.comp}"
															copy="false" rowcount="${rowcount}" />
													</c:when>
													<c:otherwise>
														${qrequest.company}
													</c:otherwise>
												</c:choose>
											</td>
											<td class="qrcont">
												<c:choose>
													<c:when test="${qrequest.detailsSource == 'EXISTING'}">
														<links:contactLinkDWRInfo contact="${qrequest.con}"
															rowcount="${rowcount}" />
													</c:when>
													<c:otherwise>
														${qrequest.contact}
													</c:otherwise>
												</c:choose>
											</td>
											<td class="qrsource">
												<c:choose>
													<c:when
														test="${qrequest.logSource == 'MEMBERS_WEB' || qrequest.logSource == 'PUBLIC_WEB'}">
														<strong>${qrequest.logSource.viewName}</strong>
													</c:when>
													<c:otherwise>
														${qrequest.logSource.viewName}
													</c:otherwise>
												</c:choose>
											</td>
											<td class="qrregdate"><fmt:formatDate
													type="date" dateStyle="SHORT" value="${qrequest.reqdate}" /></td>
											<td class="qrduedate"><fmt:formatDate
													type="date" dateStyle="SHORT" value="${qrequest.duedate}" /></td>
											<td class="qrdetails">
												<a href="#"
												onclick=" event.preventDefault(); loadScript.createOverlay('external', 'View Quotation Request - ${qrequest.requestNo}', 'viewquotationrequest.htm?id=${qrequest.id}', null, null, 64, 600, null); "
												title="<spring:message code='viewquot.viewquotationrequestdetails'/>">
													<img src="img/icons/view_details.png" width="16"
													height="16"
													alt="<spring:message code='viewquot.viewquotationrequestdetails'/>"
													title="<spring:message code='viewquot.viewquotationrequestdetails'/>" />
												</a>
											</td>
										</tr>													
									</c:forEach>
								</c:when>
								<c:otherwise>
									<tr>
										<td colspan="7" class="bold text-center">
											<spring:message
												code="viewquot.noquotationrequestslinkedtothisquotation" />
										</td>
									</tr>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
					
				</div>
				
				<!-- begin the purchase order instructions -->
				<div id="instructions-tab" class="hid">
					<cwms-instructions link-coid="${quotation.contact.sub.comp.coid}" link-subdivid="${quotation.contact.sub.subdivid}" 
					link-contactid="${quotation.contact.personid}" link-contractid="${instructionLinkContractid}"
					 instruction-types="DISCOUNT,COSTING" show-mode="RELATED"></cwms-instructions>
				</div>
				<!-- end the purchase order instructions -->
				
			</div>
			<!-- end of infobox div with nifty corners applied -->
																	
			<!-- this section contains all quote notes -->
			<t:showTabbedNotes entity="${quotation}" noteTypeId="${quotation.id}"
				noteType="QUOTENOTE" privateOnlyNotes="${privateOnlyNotes}" />
			<!-- end of quote notes section -->
															
		</div>
		<!-- end of div which wraps the whole quotation page -->

	</jsp:body>
</t:crocodileTemplate>