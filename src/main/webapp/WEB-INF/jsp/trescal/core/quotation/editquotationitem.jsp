<%-- File name: /trescal/core/quotation/editquotationitem.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="costs" tagdir="/WEB-INF/tags/costs" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>
<%@ taglib prefix="quotation" tagdir="/WEB-INF/tags/quotation" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="editquot.editquotationitem"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/quotation/EditQuotationItem.js'></script>
		<script type='text/javascript'>
		
			// this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to
			// this array is passed to the Menu.js file to display the corresponding content area of the link clicked
			var menuElements = [{ anchor: 'linkcosts', block: 'costs' },
										  { anchor: 'linktpquotereq', block: 'tpquoterequestlist' },
										  { anchor: 'linkqinote', block: 'qinote' }];
		
			var quoteIssued = false;
			/* Mark if quotation is locked for editing */
			<c:if test="${command.quotationitem.quotation.quotestatus.issued}">
				quoteIssued = true;
			</c:if>
			var quoteCurrencyId = ${command.quotationitem.quotation.currency.currencyId};
		</script>
	</jsp:attribute>
	<jsp:body>
		<c:set var="quotationitem" value="${command.quotationitem}"/>
		<!-- Mark if item is locked for editing -->
		<c:if test="${quotationitem.quotation.quotestatus.issued}">
			<div id="lockedmessage" class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<spring:message code="editquot.notelockedforediting"/>.
						<a href="#" class="mainlink" onclick=" unlockQuotationItem(); return false; "><spring:message code="editquot.unlocktemporarily"/></a> 
					</div>
				</div>
			</div>							
		</c:if>
		<!-- Mark if item canot be edited due to performance reasons / quotation size -->
		<c:if test="${not command.editable}">
			<div id="performancemessage" class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<spring:message code="error.performancerestrictions.readonly"/>.
					</div>
				</div>
			</div>							
		</c:if>
		<t:showErrors path="command.*" showFieldErrors="true" />
		<div class="marg-bot">
			<quotation:inlineQuoteItemNavigation quotationitem="${quotationitem}" nearbyItems="${nearbyItems}"/>
		</div>
		<form:form name="editquotationitem" id="editquotationitem" action="" method="post">
            <form:hidden path="editable"/>
            <div class="infobox">
                <!-- displays main quote information -->
                <fieldset id="quoteiteminfo">
                    <legend>
                        <spring:message code="quotation"/> <a
                            href="<c:url value='/viewquotation.htm?id=${quotationitem.quotation.id}'/>">
                            ${quotationitem.quotation.qno} version ${quotationitem.quotation.ver}
                    </a>
                        item ${quotationitem.itemno}
                    </legend>
                    <ol>
                        <li>
                            <label><spring:message code="editquot.modelname"/>:</label>
                            <span>
								<c:choose>
                                    <c:when test="${not empty quotationitem.inst}">
                                        <a href="viewinstrument.htm?plantid=${quotationitem.inst.plantid}"><cwms:showmodel
                                                instrumentmodel="${quotationitem.inst.model}"/></a>
                                        <c:if test="${not empty quotationitem.inst.customerDescription}"> / ${quotationitem.inst.customerDescription}</c:if>
                                    </c:when>
									<c:otherwise>
										<a href="instrumentmodel.htm?modelid=${quotationitem.model.modelid}"><cwms:showmodel instrumentmodel="${quotationitem.model}" /></a>
									</c:otherwise>
								</c:choose>
							</span>
						</li>
						<li>
							<label><spring:message code="editquot.referenceno"/>:</label>
							<c:choose>
								<c:when test="${not empty quotationitem.inst}">
									<c:choose>
										<c:when test="${not empty quotationitem.inst.plantno}">
											<span>(<span class="bold"><spring:message code="plantno"/></span>) ${quotationitem.inst.plantno}</span>
										</c:when>
										<c:otherwise>
											<span>(<span class="bold"><spring:message code="serialno"/></span>) ${quotationitem.inst.serialno}</span>
										</c:otherwise>
									</c:choose>
								</c:when>
								<c:otherwise>
									<form:input type="text" path="quotationitem.plantno" id="plantno" value="${quotationitem.plantno}" />
									<form:errors path="quotationitem.plantno" class="attention" />
								</c:otherwise>
							</c:choose>
						</li>
						<li>
							<label><spring:message code="servicetype"/>:</label>
							<form:select path="serviceTypeId" items="${serviceTypes}" itemLabel="value" itemValue="key"/>
						</li>
						<li>
						  	<label><spring:message code="quotaddinstmod.heading"/>:</label>
							<form:select path="headingId" id="headingId">
								<c:forEach var="head" items="${headings}">
									<c:choose>
										<c:when test="${quotationitem.heading.headingId == head.headingId}">
											<c:set var="selectedText" value="selected='selected'"/>
										</c:when>
										<c:otherwise>
											<c:set var="selectedText" value=""/>
										</c:otherwise>
									</c:choose>
									<option value="${head.headingId}" ${selectedText}>${head.headingName}</option>
								</c:forEach>
							</form:select>
						</li>
						<li>
							<label><spring:message code="editquot.quantity"/>:</label>
							<c:choose>
								<c:when test="${not empty quotationitem.inst}">
									<span>1</span>
								</c:when>
								<c:otherwise>
									<form:input path="quotationitem.quantity"/>
									<span class="attention">${status.errorMessage}</span>
								</c:otherwise>
							</c:choose>
						</li>

						<li>
							<label>&nbsp;</label>
							<input type="submit" name="submit" value="<spring:message code='save'/>" style=" width: auto; " onClick=" $j('#action').val('save'); " />
							<input type="submit" name="submit" value="<spring:message code='editquot.saveandreturn'/>" style=" width: auto; " onClick=" $j('#action').val('saveandreturn'); " />							
						</li>
					</ol>
				</fieldset>
			
			</div>												

			<div id="subnav">
				<dl>
					<dt><a href="#" onclick="switchMenuFocus(menuElements, 'costs', false); return false;" id="linkcosts" title="<spring:message code='editquot.viewcostingsforquoteitem'/>" class="selected"><spring:message code="editquot.costings"/></a></dt>
					<dt><a href="#" onclick="switchMenuFocus(menuElements, 'tpquoterequestlist', false); return false;" id="linktpquotereq" title="<spring:message code='editquot.viewthirdpartyquotationsforitem'/>"><spring:message code="editquot.thirdpartyquotations"/></a></dt>
					<dt><a href="#" onclick="switchMenuFocus(menuElements, 'qinote', false); return false;" id="linkqinote" title="<spring:message code='editquot.notesforquoteitem'/>"><spring:message code="editquot.quoteitemnotes"/></a></dt>
				</dl>
			</div>
			<div id="tempdiv"></div>
			<!-- this section displays all costs for the quoted item -->							
			<div id="costs">
				<quotation:showTPSupportedCosts pricingitem="${quotationitem}" currency="${quotationitem.quotation.currency}" entityId="${quotationitem.quotation.id}" defaultCurrency="${defaultCurrency}"/>
				<c:set var="currency" value="${quotationitem.quotation.currency}"/>
				<div class="infobox">
					<fieldset id="totalprice">
						<legend><spring:message code="editquot.totalcharges"/></legend>
							<ol>
								<c:if test="${quotationitem.totalCost != quotationitem.finalCost}">
									<li>
										<label>
											<c:choose>
												<c:when test="${quotationitem.quantity > 1}">
													<spring:message code="editquot.singleitemcost"/>:
												</c:when>
												<c:otherwise>
													<spring:message code="editquot.itemcost"/>:
												</c:otherwise>
											</c:choose>
										</label>
										<div class="symbolbox">${quotationitem.quotation.currency.currencyERSymbol}</div>
										<div class="valuebox">
											<span class="valuetext">
												<fmt:formatNumber value="${quotationitem.totalCost}" minFractionDigits="2" maxFractionDigits="2"/>
											</span>
										</div>
										<div class="clear"></div>
									</li>
								</c:if>
								<li>
									<label>
										<c:choose>
											<c:when test="${quotationitem.quantity > 1}">
												<spring:message code="editquot.singleitemdiscount"/>:
											</c:when>
											<c:otherwise>
												<spring:message code="editquot.itemdiscount"/>:
											</c:otherwise>
										</c:choose>
									</label>
									<div class="symbolbox">${quotationitem.quotation.currency.currencyERSymbol}</div>
									<div class="valuebox">
										<span class="valuetext">
											<c:choose>
												<c:when test="${quotationitem.calibrationCost.active && quotationitem.purchaseCost.active}">
													<c:set var="totalDiscount" value="${quotationitem.calibrationCost.discountValue + quotationitem.purchaseCost.discountValue}"/>
												</c:when>
												<c:when test="${quotationitem.calibrationCost.active}">
													<c:set var="totalDiscount" value="${quotationitem.calibrationCost.discountValue}"/>
												</c:when>
												<c:otherwise>
													<c:set var="totalDiscount" value="${quotationitem.purchaseCost.discountValue}"/>
												</c:otherwise>
											</c:choose>
											<fmt:formatNumber value="${totalDiscount}" minFractionDigits="2" maxFractionDigits="2"/>
										</span>
									</div>
									<div class="clear"></div>
								</li>
								<li>
									<label>
										<c:choose>
											<c:when test="${quotationitem.quantity > 1}">
												<spring:message code="editquot.finalcostof"/> ${quotationitem.quantity} <spring:message code="items"/>:
											</c:when>
											<c:otherwise>
												<spring:message code="editquot.itemfinalcost"/>:
											</c:otherwise>
										</c:choose>
									</label>
									<div class="symbolbox">${quotationitem.quotation.currency.currencyERSymbol}</div>
									<div class="valuebox">
										<span class="valuetext">
											<fmt:formatNumber value="${quotationitem.finalCost}" minFractionDigits="2" maxFractionDigits="2"/>
										</span>
									</div>
									<div class="clear"></div>
								</li>
							</ol>										
					</fieldset>
				</div>
			</div>
			<!-- end of show costs div -->
			<!-- this section shows any third party quote requests -->
			<div id="tpquoterequestlist" class="hid">
				<div class="infobox">
				<table id="tpquoterequeststable" class="default2" summary="<spring:message code='editquot.tablethirdpartyquotationrequests'/>">
					<thead>
						<tr>
							<td colspan="5"><spring:message code="editquot.thirdpartyquotationrequests"/> (${quotationitem.tpQuoteItemRequests.size()})</td>
						</tr>
						<tr>
							<th id="tpreqno" scope="col"><spring:message code="createquot.requestno"/></th>  
							<th id="tpreqconame" scope="col"><spring:message code="company"/></th>  
							<th id="tpreqcontact" scope="col"><spring:message code="contact"/></th>  
							<th	id="tpreqstatus" scope="col"><spring:message code="status"/></th>  
							<th id="tpreqissuedate" scope="col"><spring:message code="createquot.issuedate"/></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="5"><a href="<c:url value='/tpquoterequestform.htm?&amp;quoteid=${quotationitem.quotation.id}&amp;itemid=${quotationitem.id}'/>" class="mainlink">Request a Third Party Quotation</a></td>
						</tr>
					</tfoot>
					<tbody>
						<c:set var="rowcount" value="1"/>
						<c:forEach var="tpqrItem" items="${quotationitem.tpQuoteItemRequests}">
							<c:choose>
								<c:when test="${rowcount % 2 == 0}">
									<c:set var="rowId" value="even${rowcount}"/>
								</c:when>
								<c:otherwise>
									<c:set var="rowId" value="odd${rowcount}"/>
								</c:otherwise>
							</c:choose>
							<tr id="${rowId}" onclick="window.location.href='<c:url value='/viewtpquoterequest.htm?&amp;id=${tpqrItem.tpQuoteRequest.id}'/>'" onmouseover="this.className = 'hoverrow'" onmouseout="if (this.id.substring(0,4) == 'even') {this.className = 'even';} else {this.className = 'odd'}">
								<td scope="row">${tpqrItem.tpQuoteRequest.requestNo}</td>
								<td>${tpqrItem.tpQuoteRequest.contact.sub.comp.coname}</td>
								<td>${tpqrItem.tpQuoteRequest.contact.name}</td>
								<td><cwms:besttranslation translations="${tpqrItem.tpQuoteRequest.status.nametranslations}"/></td>
								<td><fmt:formatDate value="${tpqrItem.tpQuoteRequest.issuedate}" dateStyle="medium" type="date"/></td>
							</tr>
							<c:set var="rowcount" value="${rowcount + 1}"/>
						</c:forEach>
						<c:if test="${empty quotationitem.tpQuoteItemRequests}">
							<tr class="center">
								<td colspan="5"><spring:message code="editquot.nothirdpartyquotationrequestsexist"/></td>
							</tr>
						</c:if>
					</tbody>
				</table>
				
				</div>
			
			</div>
			<!-- end of third party quote request section -->

			<!-- this section of the page lists all notes assigned to this item -->
			<div id="qinote" class="hid">
			
				<!-- this section contains all quotation item notes -->
				<t:showTabbedNotes entity="${quotationitem}" noteTypeId="${quotationitem.id}" noteType="QUOTEITEMNOTE" privateOnlyNotes="${privateOnlyNotes}" />
				<!-- end of quotation item notes section -->
																				
			</div>
			<!-- end of the notes section -->

			<!-- Show optional copy costs to other models -->

			<c:if test="${not empty identicalmodels}">
				<div id="identicalmodels" class="infobox">
					
					<h5>
						<a href="#" onclick=" $j('.identicalList').show(); return false; " class="mainlink">
							<spring:message code="editquot.copythesecoststo"/> ${identicalmodels.size()} <spring:message code="editquot.other"/>
							<c:choose>
								<c:when test="${identicalmodels.size() > 1}">
									<spring:message code="editquot.entries"/>
								</c:when>
								<c:otherwise>
									<spring:message code="editquot.entry"/>
								</c:otherwise>
							</c:choose>
							<spring:message code="editquot.onthisquotation"/>
						</a>
					</h5>
													
					<c:set var="headingid" value=""/>
					<c:set var="caltype" value=""/>
					<table class="default4 identicalList hid">
						<thead>
							<tr>
								<th class="select" scope="col"><input type="checkbox" onclick=" selectAllItems(this.checked, 'identicalList', null); " /> <spring:message code="quotaddinst.all"/></th>
								<th class="itemno" scope="col"><spring:message code="itemno"/></th>
								<th class="model" scope="col"><spring:message code="model"/></th>
								<th class="caltype" scope="col"><spring:message code="caltype"/></th>
								<th class="plantno" scope="col"><spring:message code="plantno"/></th>
								<th class="cost" scope="col"><spring:message code="editquot.finalcost"/></th>												
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="6">&nbsp;</td>
							</tr>
						</tfoot>
						<tbody>
							<c:forEach var="idm" items="${identicalmodels}">
								<c:if test="${headingid != idm.heading.headingId}">
									<tr>
										<th colspan="6"><input type="checkbox"  onclick=" selectAllItems(this.checked, 'identicalList', '${idm.heading.headingName}'); " /> ${idm.heading.headingName}</th>
									</tr>													
									<c:set var="caltype" value=""/>
									<c:set var="headingid" value="${idm.heading.headingId}"/>
								</c:if>
								<tr>
									<td>
										<form:checkbox path="copyIds" class="${idm.heading.headingName}" value="${idm.id}"/>
									</td>
									<td class="itemno">${idm.itemno}</td>
									<td><cwms:showmodel instrumentmodel="${idm.model}"/></td>
									<td class="caltype"><cwms:besttranslation translations="${idm.caltype.serviceType.longnameTranslation}"/></td>
									<td>
										<c:if test="${not empty idm.plantno}">${idm.plantno}</c:if>
									</td>
									<td class="cost2pc">
										<a href="<c:url value='/editquotationitem.htm?id=${idm.id}'/>" class="mainlink">
											${quotationitem.quotation.currency.currencyERSymbol}${idm.finalCost}
										</a>
									</td>
								</tr>												
							</c:forEach>
						</tbody>										
						
					</table>
					
				</div>
			</c:if>

			<div class="infobox text-center">
				<input type="hidden" id="action" name="action" value="" />
				<input type="submit" name="submit" value="<spring:message code='save'/>" onClick=" $j('#action').val('save'); " />
				<input type="submit" name="submit" value="<spring:message code='editquot.saveandreturn'/>" onClick=" $j('#action').val('saveandreturn'); " />							
			</div>
			
		</form:form>
		
		<quotation:inlineQuoteItemNavigation quotationitem="${quotationitem}" nearbyItems="${nearbyItems}" />						

	</jsp:body>	
</t:crocodileTemplate>