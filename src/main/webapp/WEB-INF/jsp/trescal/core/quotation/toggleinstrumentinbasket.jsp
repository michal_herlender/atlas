<%-- File name: /trescal/core/quotation/toggleinstrumentinbasket.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div id="script">
	$j('span.basketCount').text(parseInt($j('span.basketCount:first').text()) + ${quotationItems.size()});
</div>

<table id="content">
	<c:forEach var="quotationItem" items="${quotationItems}" varStatus="loopStatus">
		<tbody id="basketitem${quotationItem.plantId}">
			<tr>
				<td class="item">${startIndex + loopStatus.count}</td>
				<td class="barcode">
					${quotationItem.plantId}
					<input type="hidden" name="basketIds" value="${quotationItem.plantId}"/>
				</td>
				<td class="inst">
					${quotationItem.modelName}
					<input type="hidden" name="basketDefInsts" value="${quotationItem.modelName}"/>
				</td>
				<td class="serial">
					${quotationItem.serialNo}
					<input type="hidden" name="basketSerials" value="${quotationItem.serialNo}"/>
				</td>
				<td class="plant">
					${quotationItem.plantNo}
					<input type="hidden" name="basketPlants" value="${quotationItem.plantNo}"/>
				</td>
				<td class="heading">
					<select name="basketHeadings">
						<c:forEach var="heading" items="${headings}">
							<option value="${heading.headingId}" <c:if test="${heading.systemDefault}">selected</c:if>>
								${heading.headingName}
							</option>
						</c:forEach>
					</select>
					<a href="" class="copyDownLink" onclick="copySelectedOption('down', 'select', $j(this).siblings('select')); return false;"
						title="<spring:message code='quotaddinst.copyselectiondown'/>">&nbsp;</a>
					<a href="" class="copyUpLink" onclick="copySelectedOption('up', 'select', $j(this).siblings('select')); return false;"
						title="<spring:message code='quotaddinst.copyselectionup'/>">&nbsp;</a>
				</td>
				<td class="source">
					${quotationItem.priceSource}
					<input type="hidden" name="basketSources" value="${quotationItem.priceSource}"/>
				</td>
				<td class="caltype">
					<select name="basketCaltypes" onchange="reevaluatePrice(${quotationItem.plantId}, ${subdivId}, '${currencySymbol}');"
						id="caltype${quotationItem.plantId}">
						<c:forEach var="calType" items="${calTypes}">
							<option value="${calType.key}" <c:if test="${calType.key == quotationItem.defaultCalTypeId}">selected</c:if>>
								${calType.value}
							</option>
						</c:forEach>
					</select>
				</td>
				<td class="cost" id="price${quotationItem.plantId}">
					${currencySymbol} ${quotationItem.finalPrice}
				</td>
				<td class="remove">
					<input type="hidden" name="basketCosts" id="basketprice${quotationItem.plantId}" value="${quotationItem.finalPrice}"/>
					<a href="" class="deleteAnchor" onclick="deleteFromBasket(${quotationItem.plantId}, ${quoteId}, '${currencySymbol}', ${subdivId}); return false;">&nbsp;</a>
				</td>
			</tr>
		</tbody>
	</c:forEach>
</table>