<%-- File name: /trescal/core/quotation/addinstmodeltoquote.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page buffer="none" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="costs" tagdir="/WEB-INF/tags/costs"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="quotaddinstmod.title" /></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/quotation/AddInstModelToQuote.js'></script>
		<!-- Script definitions in JSP so 'static' variables can be loaded -->
		<script>
			var BASKETMAXQTY = ${addquoteitemsearchform.maxQuantity};
			 
			 /**
			 * Utility QuoteHeading object
			 */
			 function QuoteHeading(id, name, description, quotedefault, systemdefault)
			 {
				this.id = id;
				this.name = name;
				this.description = description;
				this.quotedefault = quotedefault;
				this.systemdefault = systemdefault;
			 }
			 
			 /**
			 * Utility Module Object
			 */
			 function Module(modelid, description, itemno, partof)
			 {
			 	this.modelid = modelid;
			 	this.description = description;
			 	this.partof = partof;
			 	this.itemno = itemno;
			 }		
			
			var calTypes = new Array(
				<c:forEach var="cal" items="${addquoteitemsearchform.caltypes}" varStatus="status" >
					new Array(${cal.calTypeId}, '<cwms:besttranslation translations="${cal.serviceType.shortnameTranslation}" />')
					${not status.last ? ',' : ''}
				</c:forEach>
			);
			
			// list all quoteheadings for this quotation
			var quoteHeadings = new Array(
				<c:forEach var="head" items="${addquoteitemsearchform.headings}" varStatus="status" >
					new QuoteHeading(${head.headingId}, '${head.headingName}', '${head.headingDescription}', ${head.systemDefault})
					${not status.last ? ',' : ''}
				</c:forEach>
			);
	
			// maximum qty field
			var maxQuantity = ${addquoteitemsearchform.maxQuantity};
			
			// tracks the modelid currently being added
			var modelAdding;	
			
			// tracks the next itemno for this basket
			var maxItemNo = ${addquoteitemsearchform.maxItemNo};
		</script>
	</jsp:attribute>
	<jsp:body>
		<!-- error section displayed when form submission fails -->
		<form:errors path="addquoteitemsearchform.*">
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<h5 class="center-0 attention">
							<spring:message code="error"/>
						</h5>
					</div>
				</div>
			</div>
		</form:errors>
		<!-- end error section -->

	<form:form id="searchquoteitemform"
			onsubmit=" showUnLoadWarning = false; "
			modelAttribute="addquoteitemsearchform">
	
		<div>
			<!-- id to bring user back to search form if they choose to search again -->									
			<div class="infobox" id="search">
			
				<!-- displays content in column (left) -->
				<div class="displaycolumn">
					<p>
						<spring:message code="quotaddinstmod.addingitemsfor" />
						<a href='<c:url value="/viewquotation.htm?id=${addquoteitemsearchform.id}"/>' class="mainlink">
							<spring:message code="quotation" />
							<c:out value=" ${addquoteitemsearchform.qno} "/>
							<spring:message code="quotaddinstmod.version" />
							<c:out value=" ${addquoteitemsearchform.ver}"/>
						</a>
					</p>
				
				<!-- this shows any quotation request information that the user has provided on the website -->
				<c:if
							test="${addquoteitemsearchform.quotation.quotationRequests.size() > 0}">
					<fieldset>
						<legend>
									<spring:message
										code="quotaddinstmod.quotationrequestslinkedtothisquotation" /> (${addquoteitemsearchform.quotation.quotationRequests.size()})</legend>
						<ol>
							<li>
								<c:forEach var="qrequest"
											items="${addquoteitemsearchform.quotation.quotationRequests}">
									${qrequest.requestInfo.replace(",", "<br />")}																							
								</c:forEach>
							</li>
						</ol>
					</fieldset>
				</c:if>
				<!-- end of quotation request info -->	
				
				<!-- displays form for searching equipment to add to quotation -->
				<fieldset>
	
					<legend><spring:message code="quotaddinstmod.equipmentsearch" /></legend>
					
					<form:hidden path="startRow" />
					<form:hidden path="submitted" />

					<ol>
						<li>
							<label><spring:message code="domain"/>:</label>
							<!-- float div left -->
							<div class="float-left">
								<form:input path="domainNm" name="domainNm" tabindex="1"/>
								<form:input path="domainId" type="hidden" name="domainId"/>								
							</div>
							<div class="clear-0"></div>	
						</li>		
						<li>
							<label><spring:message code="family"/>:</label>
							<!-- float div left -->
							<div class="float-left">
								<form:input path="familyNm" name="familyNm" tabindex="2"/>
								<form:input path="familyId" type="hidden" name="familyId"/>								
							</div>
							<div class="clear-0"></div>	
						</li>	
						<li>
							<label for="description"><spring:message code="sub-family" />:</label>
							<!-- float div left -->
							<div class="float-left">
							
								<!-- inst description results appear here -->
									<form:input path="search.descNm" id="descNm" tabindex="3" />
									<form:hidden path="search.descId" id="descId" />
							</div>
							<!-- clear floats and restore pageflow -->
							<div class="clear-0"></div>
						</li>
						<li>
							<label for="mfr"><spring:message code="manufacturer" />:</label>
							<!-- float div left -->
							<div class="float-left">
								<form:input path="mfrtext" name="mfrtext" tabindex="1"/>
								<form:input path="mfrId" type="hidden" name="mfrId"/>	
							</div>																							
							<!-- clear floats and restore pageflow -->
							<div class="clear-0"></div>
						</li>
						<li>
							<label for="instmodel"><spring:message code="model" />:</label>
							<form:input path="search.model" id="modeltext" />
							<form:errors path="search.model" />
						</li>
						<li>
							<label><spring:message code="salescategory"/>:</label>
							<!-- float div left -->
                            <div class="float-left">
								<form:input path="salesCat" name="salesCat" tabindex="6"/>
								<form:input path="salesCatId" type="hidden" name="salesCatId"/>								
							</div>                           
							<div class="clear-0"></div>	
						</li>   
						<li>
								<label><spring:message code="instmod.modeltype" /></label>
								<form:select path="modelTypeSelectorChoice">
									<form:options items="${modelTypeSelections}" itemValue="key" itemLabel="value"/>
								</form:select>
						</li>
						<li>
							<label for="submit">&nbsp;</label>
							<input type="submit" value='<spring:message code="search"/>'
									name="submit" class="float-left" id="submit" tabindex="0" onclick="return checkForm();"/>
							<input type="button"
									value='<spring:message code="quotaddinstmod.clearform"/>'
									id="clear" class="float-right" onclick=" clearForm(); "
									tabindex="0" />
							<!-- clear floats and restore page flow -->
							<div class="clear-0"></div>
						</li>
					</ol>
					
				</fieldset>																		
			
			</div>
			
			<!-- displays content in column 40% wide (right) -->
			<div id="searchdefaults" class="displaycolumn">
				
				<!-- displays form for entering default settings when adding equipment to quote -->
				<fieldset>

					<legend><spring:message code="quotaddinstmod.choosedefaults" /></legend>
					
					<ol>
						<li>
							<label for="defaultQuoteHeading"><spring:message code="quotaddinstmod.heading" />:</label>
							<form:select path="defaultQuoteHeading">
								<form:options items="${addquoteitemsearchform.headings}" itemValue="headingId" itemLabel="headingName" />
							</form:select>
						</li>
						<li>
							<label for="defaultCaltypes"><spring:message code="caltype" />:</label>
							<div class="float-left">
								<c:set var="count" value="1" />
								<c:forEach var="cal" items="${addquoteitemsearchform.caltypes}">
									<c:set var="checked" value="" />
									<c:forEach var="calibtype" items="${addquoteitemsearchform.quotation.defaultCaltypes}">
										<c:if test="${calibtype.caltype.calTypeId == cal.calTypeId}">
											<c:set var="checked" value="checked" />
										</c:if>
									</c:forEach>
									<form:checkbox path="defaultCaltypes" value="${cal.calTypeId}" checked="${checked}" />
												<cwms:besttranslation translations="${cal.serviceType.shortnameTranslation}" />
												-
												<cwms:besttranslation translations="${cal.serviceType.longnameTranslation}" />
												<br />
									<!-- this blank div clears floats and restores page flow -->
									<div class="clear"></div>
									<c:set var="count" value="${count + 1}" />
								</c:forEach>
							</div>
						</li>
						<li>
							<label for="defaultQuoteQty"><spring:message code="quotaddinstmod.quantity" />:</label>
							<form:select path="defaultQuoteQty" items="${addquoteitemsearchform.quantityRange}" />
						</li>
						<li>
							<fieldset class="defaultAddModules">
								<legend>
											<spring:message
												code="quotaddinstmod.autoaddbaseunitsdefaultmodules" />
										</legend>
								<label>
									<form:radiobutton path="defaultAddModules" value="true" /> <spring:message
												code="yes" />
								</label>
								<label>
									<form:radiobutton path="defaultAddModules" value="false" /> <spring:message
												code="nobool" />
								</label>
							</fieldset>
						</li>
						<li>
							(${addquoteitemsearchform.quotation.defaultSetBy.name} - <fmt:formatDate value="${addquoteitemsearchform.quotation.defaultSetOn}" dateStyle="medium" type="both" timeStyle="short" />)
							<input type="hidden" id="quoteCurrency" value="${addquoteitemsearchform.quotation.currency.currencyERSymbol}" />
						</li>
					</ol>
				</fieldset>					
			
			</div>
			
			<!-- this blank div clears floats and restores page flow -->
			<div class="clear"></div>
			
		</div>
		
		<!-- sub navigation menu which displays and hides sections of page as well as linking to external pages -->
		<div id="subnav">
			<dl>
				<dt>
					<a href="#"
						onclick="switchMenuFocus(menuElements, 'searchresults-tab', false); return false;"
						id="searchresults-link"
						title='<spring:message code="quotaddinstmod.viewnewquoteitemsearchresults"/>'
						class="selected"><spring:message
							code="quotaddinstmod.searchresults" /></a>
				</dt>
				<dt>
					<a href="#" onclick="switchMenuFocus(menuElements, 'searchbasket-tab', false); return false;" id="searchbasket-link" title='<spring:message code="quotaddinstmod.viewnewquoteitembasket"/>' >
						<spring:message code="quotaddinstmod.quoteitembasket" /> (<span class="basketCount">${addquoteitemsearchform.chosenModels != null ? addquoteitemsearchform.chosenModels.size() : 0 }</span>)
					</a>
				</dt>
			</dl>
		</div>
		<!-- end of sub navigation menu -->
		
		<!-- displays instrument search results -->								
		<div id="searchresults-tab">
		
			<div class="results">
			
				<h5>
					<c:choose>
					<c:when test="${addquoteitemsearchform.searchModels != null}">
						<c:choose>
						<c:when test="${addquoteitemsearchform.searchModels.size() > 1}">
							<c:out value="${addquoteitemsearchform.searchModels.size()} "/>
							<spring:message code="quotaddinstmod.modelsfound"/>
						</c:when>
						<c:otherwise>
							<c:out value="${addquoteitemsearchform.searchModels.size()} "/>
							<spring:message code="quotaddinstmod.modelfound"/>
						</c:otherwise>
						</c:choose>
					</c:when>
					<c:otherwise>
						<spring:message code="noresults" />
					</c:otherwise>
					</c:choose>
				</h5>
				
				<!-- this list displays each instrument returned in the search -->
				<ul>
				<li style="display: none;"></li>
				<c:forEach var="model" items="${addquoteitemsearchform.searchModels}" >
					<c:if test="${model.modelType.capability != true}">
						<li id="model${model.modelid}">
							<!-- this div is floated left and displays the instrument name -->
							<div class="result_text">
							<span>
								<!-- this link is used to create the block hover effect -->
								
								<a id="getmodelname${model.modelid}" href="#" onclick=" loadAddItemSection(${model.modelid},'<cwms:showmodel instrumentmodel="${model}" escapeJavascript="true" />','${addquoteitemsearchform.quotation.currency.currencyERSymbol}',${addquoteitemsearchform.quotation.id},'${addquoteitemsearchform.defaultCurrency.currencyCode}',${addquoteitemsearchform.coid},${allocatedCompany.getKey()}); return false; ">
									<cwms:showmodel instrumentmodel="${model}"/> -- <cwms:besttranslation translations="${model.modelType.modelTypeNameTranslation}"/>
								</a>
								<%--  check if this instmodeltype is allowed modules --%>
								<c:if test="${model.modelType.modules == true}">
									<%-- check if this model has any modules --%>
									<c:if test="${model.thisBaseUnitsModules.size() > 0}">
										<!-- this div displays any modules which are associated with the instrument -->
										<div class="modulebox" id="module${model.modelid}">
											<div class="moduletext">
												<spring:message code="quotaddinstmod.thismodelhas"/> ${model.thisBaseUnitsModules.size()} <spring:message code="quotaddinstmod.availablemodulestickeachmoduleyouwishtoaddaspartoftheabovebaseunit"/>
											</div>
											<%-- loop through and display any modules for this base unit --%>
											<c:forEach var="partOf" items="${model.thisBaseUnitsModules}">
												<c:set var="module" value="${partOf.module}"/>
												#set($module = $partOf.module)
												<div class="moduleoption">
													<input type="checkbox" id="${module.modelid}" value='<cwms:showmodel instrumentmodel="${module}"/>' name="module" />
													<cwms:showmodel instrumentmodel="${module}"/>
												</div>
											</c:forEach>
										</div>
									</c:if>
								</c:if>
							</span>
						</div>
						<!-- this div is floated right and displays the add default link -->
						<div class="add_default">
							<c:choose>
								<c:when test="${empty model.salesCategory || model.salesCategory.active}">
									<a href="#" class="mainlink" id="default${model.modelid}"
										onclick="addDefaults(event, ${addquoteitemsearchform.quotation.id}, ${model.modelid}, '<cwms:showmodel instrumentmodel="${model}" escapeJavascript="true" />', '${addquoteitemsearchform.quotation.currency.currencyCode}', ${addquoteitemsearchform.quotation.organisation.coid}, '${addquoteitemsearchform.defaultCurrency.currencyERSymbol}', ${addquoteitemsearchform.quotation.contact.sub.comp.coid}, ${addquoteitemsearchform.quotation.id}); return false;">
										<spring:message code="quotaddinstmod.addquoteitem"/>
									</a>
								</c:when>
								<c:otherwise>
									<span><spring:message code="quotaddinstmod.salescategoryinactive"/></span>
								</c:otherwise>
							</c:choose>
						</div>																
					</li>
						
					</c:if>
				</c:forEach>				
				</ul>
				
				<!-- this blank div clears floats, restores page flow and applies border to last list item  -->
				<div class="clear_border">&nbsp;</div>
			
			</div>
		
		</div>
	
		<div id="searchbasket-tab" class="hid">
		
			<div class="infobox">
											
				<h5>
					<spring:message code="quotaddinstmod.quotebasket"/> 
				</h5>
				
				<!-- link to delete all items in the quote basket -->
				<a href="#" class="mainlink-float"
							onclick=" deleteFromBasket('all', ''); return false; "
							id="removelink"
							title='<spring:message code="quotaddinstmod.removeallitemsfromthebasket"/>'><spring:message code="quotaddinstmod.removeallitems"/></a>
														
				<!-- clear floats and restore page flow -->
				<div class="clear"></div>
					
				<table class="default2" id="basketitemstable"
							summary='<spring:message code="quotaddinstmod.thistabledisplaysallquoteitemsinbasket"/>'>
					<thead>
						<tr>
							<td colspan="9"><spring:message code="quotaddinstmod.quoteitembasket"/> (<span
										class="basketCount">${addquoteitemsearchform.chosenModels != null ? addquoteitemsearchform.chosenModels.size() : 0 }</span>)</td>
						</tr>
						<tr>
							<th class="addqiitem" scope="col"><spring:message code="item"/></th>
							<th class="addqiinst" scope="col"><spring:message code="instrument"/></th>
							<th class="addqicaltype" scope="col"><spring:message code="servicetype"/></th>
							<th class="addqiqty" scope="col"><spring:message code="quotaddinstmod.qty"/></th>
							<th class="addqihead" scope="col"><spring:message code="quotaddinstmod.heading"/></th>
							<th class="addqiplant" scope="col"><spring:message code="plantno"/></th>
							<th class="addqisource" scope="col"><spring:message code="quotaddinst.costsrc"/></th>
							<th class="addqicost" scope="col"><spring:message code="cost"/></th>
							<th class="addqidel" scope="col"><spring:message code="delete"/></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="9">&nbsp;</td>
						</tr>
					</tfoot>
					<c:set var="count" value = "1"/>
					<c:choose>
						<c:when test="${addquoteitemsearchform.chosenModels != null}">
							<c:forEach var="qitem" items="${addquoteitemsearchform.chosenModels}" varStatus="loopstatus">
								<tbody id="basketitem${qitem.itemno}">
									<tr <c:if test="${count % 2 == 0}"> class="even" </c:if><c:if test="${count % 2 != 0}"> class="odd" </c:if> >
										<td class="center">${count}</td>
										<td>
											<cwms:showmodel instrumentmodel="${qitem.model}"/>
												<form:hidden path="ids" value="${qitem.model.modelid}" />
												<form:hidden path="itemnos" value="${qitem.itemno}" />
										</td>
										<td class="center">
											<cwms:besttranslation translations="${qitem.caltype.serviceType.shortnameTranslation}"/>
											<form:hidden path="caltype" value="${qitem.caltype.calTypeId}" />
										</td>						
										<td class="center">
												<form:select path="quantities">
													<c:forEach var="quantity" items="${addquoteitemsearchform.quantityRange}">
														<option value="${quantity}" ${quantity == addquoteitemsearchform.quantities[loopstatus.index] ? "selected" : ""}>
															${quantity}
														</option>
													</c:forEach>
												</form:select>
										</td>
										<td class="center">
												<form:select class="quotebasketselect" path="headingids">
													<c:forEach var="heading" items="${addquoteitemsearchform.headings}">
														<option value="${heading.headingId}" ${heading.headingId == addquoteitemsearchform.headingids[loopstatus.index] ? "selected" : ""}>
															${heading.headingName}
														</option>
													</c:forEach>
												</form:select>
										</td>
										<td class="center">
										    <form:input type="text" value='<c:out value="${addquoteitemsearchform.plantnos[loopstatus.index]}"/>' path="plantnos"/>
										</td>
										<td class="center">
											<span id="spanSource${qitem.itemno}">${qitem.calibrationCost.costSrc}</span>
											<br><a href="#" onclick="switchSourceCost(${qitem.model.modelid}, ${qitem.caltype.calTypeId}, ${addquoteitemsearchform.quotation.id}, ${qitem.itemno})">Switch</a>
											<form:hidden id="costSource${qitem.itemno}" path="sourceCost" value="${qitem.calibrationCost.costSrc}" />
										</td>
										<td class="center">
										    ${addquoteitemsearchform.quotation.currency.currencyERSymbol}
										    <form:input type="text" value='<c:out value="${addquoteitemsearchform.calcosts[loopstatus.index]}"/>' path="calcosts" style="text-align: right" onclick="updateCostSource('${qitem.itemno}')"/>
											<form:errors path="calcosts"/>
										</td>
										<td class="center">
											<a href="#"
										onclick=" deleteFromBasket('item', 'basketitem${qitem.itemno}'); return false;"
										class="mainlink"><img src="img/icons/delete.png"
											width="16" height="16"
											title='<spring:message code="quotaddinstmod.deletefrombasket"/>'
											alt='<spring:message code="quotaddinstmod.deletefrombasket"/>' /></a>
										</td>
									</tr>
																																		
									<%-- now loop through all modules for this quotation item --%>
									
									<c:if test="${qitem.modules != null}">
										<c:forEach var="module" items="${qitem.modules}">
											
											<tr id="moditem${module.itemno}" ${(count % 2 == 0) ? 'class=\"even\"' : 'class=\"odd\"'} >
												<td class="center">
													<img src="img/icons/arrow_merge.png" width="16" height="16" alt='<spring:message code="module"/>' title='<spring:message code="module"/>' />
												</td>																	
												<td colspan="6">
													<cwms:showmodel instrumentmodel="${module.model}"/>
													<form:hidden path="moduleitemnos" value="${module.itemno}"/>
													<form:hidden path="modulemodelids" value="${module.model.modelid}"/>
													<form:hidden path="modulepartofs" value="${module.baseUnit.itemno}"/>
												</td>
												<td class="center">																												
													<a href="#" class="mainlink"
												onclick=" $j('tbody#basketitem${qitem.itemno} tr#moditem${module.itemno}').remove(); return false; "><img
																src="img/icons/delete.png" width="16" height="16"
																alt='<spring:message code="quotaddinstmod.deletefrombasket"/>'
																title='<spring:message code="quotaddinstmod.deletefrombasket"/>' /></a>
												</td>
											</tr>
	
										</c:forEach>
										
									</c:if>
	
				
								</tbody>
	
								<c:set var="count" value="${count + 1}" />
				
							</c:forEach>
						</c:when>	
						<c:otherwise>
							
							<tbody>
								<tr>
									<td colspan="9" class="center bold"><spring:message code="quotaddinstmod.yourquotationitembasketisempty"/></td>
								</tr>
							</tbody>
							
						</c:otherwise>
					</c:choose>
																
				</table>
				
			</div>
																					
		</div>
		
		<div class="center">
			<input type="submit" value='<spring:message code="quotaddinstmod.addbasketitemstoquotation"/>' name="submit"
				onclick=" if ($j('table#basketitemstable tbody:first td').length < 2){ $j.prompt('You have not added any items to the quotation basket'); return false; }else{ $j('#submitted').attr('value', 'addtoquote'); showUnLoadWarning = false; } " />
		</div>
	</div>
	
</form:form>
</jsp:body>
</t:crocodileTemplate>