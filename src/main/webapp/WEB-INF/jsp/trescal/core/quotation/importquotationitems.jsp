<%-- File name: /trescal/core/quotation/importquotationitems.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="quotation" tagdir="/WEB-INF/tags/quotation"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="link" tagdir="/WEB-INF/tags/links" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="importquotationItems.title" /></span>
		<style>
			.max-wrap {
				max-width : 120px;
				display : block;
			}
		</style>
		<script>
				if ( window.history.replaceState ) {
  					window.history.replaceState( null, null, window.location.href );
				}
		</script>
	</jsp:attribute>
	<jsp:body>
		<form:errors path="importQuotationItems.*">
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<h5 class="center-0 attention"><spring:message code="viewquot.attention"/>:</h5>
						<c:forEach var="e" items="${messages}">
							<div class="center attention"><c:out value="${e}"/></div>
						</c:forEach>
					</div>
				</div>
			</div>
		</form:errors>
      
      	<form:form modelAttribute="importQuotationItems" method="post" enctype="multipart/form-data">
      		<div class="infobox">
			<div>
				<cwms:securedLink permission="IMPORT_QUOTATION_ITEMS_BATCH"  classAttr="mainlink" 
					parameter="?id=${quotation.id}" >
						<spring:message code='viewquot.importquotationitemsbatch'/>
				</cwms:securedLink>
			</div>
			<fieldset>
				<legend>
					<a href="viewquotation.htm?id=${quotation.id}" class="mainlink-float">
					<spring:message code="quotation" />
					<c:out value=" ${quotation.qno} " />
					<spring:message code="quotaddinstmod.version" />
					<c:out value=" ${quotation.ver}" />
					<form:hidden path="quotationId" />
					</a>
				</legend>
			<!-- displays content in left column -->
				<div class="displaycolumn">
					<ol>										
						<li>
							<label><spring:message code="company" />:</label>
							<link:companyLinkDWRInfo company="${quotation.contact.sub.comp}" rowcount="1" copy="true"/>
						</li>
						<li>
							<label><spring:message code="subdiv" />:</label>
							<link:subdivLinkDWRInfo rowcount="1" subdiv="${ quotation.contact.sub }"/>
							<form:hidden path="subdivid" />
						</li>
						<li>
							<label><spring:message code="requestdate" />:</label>
							<span><fmt:formatDate value="${quotation.reqdate}" dateStyle="medium" type="date"/></span>
						</li>
					</ol>
				</div>
				<div class="displaycolumn">
					<ol>	
				    	<li>
							<label><spring:message code="contact" />:</label>
							<link:contactLinkDWRInfo contact="${ quotation.contact }" rowcount="0" />
							<form:hidden path="personid" />
						</li>
						<c:if test="${not empty quotation.duedate}">
							<li>
								<label><spring:message code="quotrequest.duedates" />:</label>
								<span>
									<fmt:formatDate type="date" dateStyle="SHORT"
										value="${quotation.duedate}" />
								</span>
							</li>
						</c:if>
					</ol>
				</div>
		</fieldset>
	</div>
	<div class="infobox">
		<form:hidden path="quotationId" value="${ quotation.id }"/>
		<form:hidden path="coid" value="${ quotation.contact.sub.comp.coid }"/>
		<table align="center">
			<tr >
				<td><label><spring:message code="addjob.recordtype.file.exchangeformat"/> :</label></td>
				<td>
					<form:select path="exchangeFormatId" id="ef" type="text">
						<c:forEach var="ef" items="${efImportQuotationItems}">
							<option value="${ef.id}">${ef.name}</option>
						</c:forEach>
					</form:select>
				</td>
			</tr>
			<tr>
				<td><label><spring:message code="addjob.recordtype.file.filetoupload"/> :</label></td>
				<td colspan="2">
					<form:input path="file" type="file" id="uploadFile"  enctype="multipart/form-data" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
					<span class="attention"><form:errors path="file" /></span>
					<div class="clear"></div>
				</td>
			</tr> <br>
			<tr>
				<td></td>
				<td colspan="2"><input type="submit" id="submit" value="<spring:message code="submit" />" tabindex="8" /></td>
			</tr>
		</table>
	</div>
</form:form>
</jsp:body>
</t:crocodileTemplate>