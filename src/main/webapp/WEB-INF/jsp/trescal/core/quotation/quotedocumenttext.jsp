<%-- File name: /trescal/core\quotation/quotedocumenttext.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="quotation" tagdir="/WEB-INF/tags/quotation"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
        <span class="headtext"><spring:message
				code="quotdoctext.quotationdocumenttext" /></span>
    </jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/quotation/QuoteDocumentText.js'></script>
    </jsp:attribute>
	<jsp:body>
        <c:if test="${not empty customText }">
			<div class="successBox1">
				<div class="successBox2">
					<div class="successBox3">
						<div>
							<spring:message
								code="quotdoctext.customdocumenttexthasbeensavedforthisquotation" />
						</div>
					</div>
				</div>
			</div>
		</c:if>
		<t:showErrors path="form.*" />
						
		<div class="infobox">
		
			<form:form modelAttribute="form" action="" method="post" >
			
				<fieldset>
				
					<legend>
						<spring:message code="quotdoctext.quotationdocumenttexteditor" />
					</legend>
					
					<ol>
						<li>
							<span>
								<spring:message code="quotdoctext.backtoquote" />
								&nbsp;
								<a href="<spring:url value='viewquotation.htm?id=${quotation.id}'/>"
									class="mainlink">${quotation.qno} ver ${quotation.ver}</a>
							</span>
						</li>
						<c:choose>
						    <c:when test="${not empty customText }"> 
								<li>
									<label class="width30"><spring:message code="quotdoctext.editedby" />:</label>
									<span><c:out value="${customText.lastModifiedBy.name}"/></span>
								</li>
								<li>
									<label class="width30"><spring:message code="quotdoctext.editedon" />:</label>
									<span><fmt:formatDate value="${customText.lastModified}" type="both" dateStyle="SHORT" timeStyle="SHORT" /></span>
								</li>
							</c:when>
							<c:otherwise>
								<li>
									<span class="bold"><spring:message code="quotdoctext.paragraphcanbeeditedbelow" />:</span>
								</li>
							</c:otherwise>
						</c:choose>
						<li>
							<label class="width30"><spring:message code="source" />:</label>
							<div class="float-left">
								<form:radiobutton path="useCustomQuoteText" value="false" id="defaultText" onclick=" disableControls(); " />
								<spring:message code="quotdoctext.usedefaulttext" />
								<br />
								<form:radiobutton path="useCustomQuoteText" value="true" onclick=" enableControls(); " />
								<spring:message code="quotdoctext.usecustomtext" />
							</div>
							<span class="attention">
								<form:errors path="useCustomQuoteText" />
							</span>
							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
						</li>
						<li>											
							<label class="width30"><spring:message code="quotdoctext.quotationdocumentsubject" />:</label>																						
							<div class="float-left">
								<form:input id="input_subject" path="subject" size="80" disabled="${!form.useCustomQuoteText}" />
							</div>
							<!-- clear floats and restore page flow -->
							<div class="clear-0"></div>
							<span class="attention">
								<form:errors path="subject" />
							</span>
						</li>
						<li>											
							<label class="width30"><spring:message code="quotdoctext.quotationdocumenttext" />:</label>																						
							<div class="float-left">
								<form:textarea id="textarea_body" path="body" rows="20" cols="80" disabled="${!form.useCustomQuoteText}" />
							</div>
							<!-- clear floats and restore page flow -->
							<div class="clear-0"></div>
							<span class="attention">
								<form:errors path="body" />
							</span>
						</li>
						<li>
							<label class="width30">&nbsp;</label>
							<input type="submit" name="submit" value="<spring:message code='quotdoctext.updatequotetext'/>" />
						</li>
					
					</ol>
				
				</fieldset>
			
			</form:form>
		
		</div>	
    </jsp:body>
</t:crocodileTemplate>