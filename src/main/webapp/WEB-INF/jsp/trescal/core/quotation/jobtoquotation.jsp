<%-- File name: /trescal/core/quotation/jobtoquotation.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<spring:message code='jobquot.createquotationfromjob' />
			<c:out value=" ${form.job.jobno}" /></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/quotation/JobToQuotation.js'></script>
	</jsp:attribute>
	<jsp:body>

		<div class="jobToQuotation">

			<form:form method="post" modelAttribute="form">
				<form:errors path="*">
					<div class="warningBox1">
						<div class="warningBox2">
							<div class="warningBox3">
								<h5 class="center-0 attention">
									<spring:message code='jobquot.errorcreatingaquotejob' />
								</h5>
								<c:forEach var="e" items="${messages}">
									<div class="center attention"><c:out value="${e}"/></div>
								</c:forEach>
							</div>
						</div>
					</div>
				</form:errors>

				<!-- infobox div with nifty corners applied -->
				<div class="infobox">

					<fieldset>

						<legend>
							<spring:message code='jobquot.createquotationfromjobdetails' />
						</legend>

						<ol>

							<li>
								<label>
									<spring:message code='jobno' />:</label>
								<span><c:out value="${form.job.jobno}"/></span>
							</li>
							<li>
								<label>
									<spring:message code='jobquot.linktojob' />:</label>
								<input type="checkbox" name="linkToJob" value="true" checked="checked">
							</li>
							<c:choose>
								<c:when test="${form.fromJob && form.fromJob == true}">
									<li>
										<label>
											<spring:message code='contact' />:</label>
										<!-- this div creates a new cascading search plugin. The default search contains an input 
								 field and results box for companies, when results are returned this cascades down to
								 display the subdivisions within the selected company. This default behavoir can be obtained
								 by including the hidden input field with no value or removing the input field. You can also
								 add address and/or contact searches to the cascade by including the values 'contact' and/or 'address'
								 in the hidden inputs value attribute separated by a comma (e.g value="contact", value="address",
								 value="address,contact").  You can also swap these values around (e.g. value="contact,address").  
							 -->
										<div id="cascadeSearchPlugin">
											<input type="hidden" id="cascadeRules" value="subdiv,contact" />
											<input type="hidden" id="compCoroles" value="client,prospect" />

											<input type="hidden" id="prefillIds"
												value="${form.job.con.sub.comp.coid},${form.job.con.sub.subdivid},${form.job.con.personid}" />

										</div>

										<!-- clear floats and restore page flow -->
										<div class="clear"></div>
									</li>
								</c:when>
								<c:otherwise>
									<li>
										<label>
											<spring:message code='contact' />:</label>
										<span>
											<c:choose>
												<c:when test="${not empty form.contact}">
													<c:out value="${form.contact.name}"/>
												</c:when>
												<c:otherwise>
													<c:out value="${form.job.con.name}"/>
												</c:otherwise>
											</c:choose>
										</span>
									</li>
									<li>
										<label>
											<spring:message code='subdivision' />:</label>
										<span>
											<c:choose>
												<c:when test="${not empty form.contact}">
													<c:out value="${form.contact.sub.subname}"/>
												</c:when>
												<c:otherwise>
													<c:out value="${form.job.con.sub.subname}"/>
												</c:otherwise>
											</c:choose>
										</span>
									</li>
									<li>
										<label>
											<spring:message code='company' />:</label>
										<span>
											<c:choose>
												<c:when test="${not empty form.contact}">
													<c:out value="${form.contact.sub.comp.coname}"/>
												</c:when>
												<c:otherwise>
													<c:out value="${form.job.con.sub.comp.coname}"/>
												</c:otherwise>
											</c:choose>
										</span>
									</li>
								</c:otherwise>
							</c:choose>
							<li>
								<form:label path="clientRef">
									<spring:message code='jobquot.clientsreference' />:</form:label>
								<c:choose>
									<c:when test="${form.fromJob && form.fromJob == true}">
										<form:input path="clientRef" type="text"/>
									</c:when>
									<c:otherwise>
										<span><c:out value="${form.clientRef}"/></span>
									</c:otherwise>
								</c:choose>
							</li>
							<li>
								<form:label path="reqDate">
									<spring:message code='requestdate' />:</form:label>
								<c:choose>
									<c:when test="${form.fromJob && form.fromJob == true}">
											<form:input path="reqDate" type="date" max="${today}"/>
									</c:when>
									<c:otherwise>
										<span><fmt:formatDate type="date" dateStyle="SHORT" value="${form.reqDate}" /></span>
									</c:otherwise>
								</c:choose>
							</li>
							<li>
								<form:label path="currencyCode">
									<spring:message code='currency' />:</form:label>
								<c:choose>
									<c:when test="${not empty form.contact}">
										<c:set var="company" value="${form.contact.sub.comp}" />
									</c:when>
									<c:otherwise>
										<c:set var="company" value="${form.job.con.sub.comp}" />
									</c:otherwise>
								</c:choose>
								<c:choose>
									<c:when test="${form.fromJob && form.fromJob == true}">
											<form:select path="currencyCode">
												<c:forEach var="curopt" items="${currencyopts}">
													<form:option value="${curopt.optionValue}">
														${curopt.currency.currencyCode}
														@ ${curopt.defaultCurrencySymbol}1 =
														${curopt.currency.currencySymbol}${curopt.rate}
														<c:choose>
															<c:when test="${curopt.companyDefault == true}">
																[Company Default Rate] </c:when>
															<c:when test="${curopt.systemDefault == true}">
																[System Default Rate] </c:when>
														</c:choose>
													</form:option> 													>
												</c:forEach>
											</form:select>
											<span class="attention">
												<spring:message code='jobquot.warningjobcurrency' />
												<c:out
													value=" ${form.job.currency.currencyCode} (${form.job.currency.currencySymbol}) - " />
												<spring:message code='jobquot.priceswillbebroughtoverinthiscurrency' />
											</span>
											<form:errors cssClass="attention"/>
									</c:when>
									<c:otherwise>
										<span>
											<c:forEach var="curopt" items="${currencyopts}">
												<c:if test="${curopt.currency.currencyCode == form.currencyCode}">
													${curopt.currency.currencyCode}
													@ ${curopt.defaultCurrencySymbol}1 =
													${curopt.currency.currencySymbol}${curopt.rate}
													<c:choose>
														<c:when test="${curopt.companyDefault == true}">
															[Company Default Rate] </c:when>
														<c:when test="${curopt.systemDefault == true}">
															[System Default Rate] </c:when>
													</c:choose>
												</c:if>
											</c:forEach>
										</span>
									</c:otherwise>
								</c:choose>
							</li>

						</ol>

					</fieldset>

				</div>
				<!-- end of infobox div with nifty corners applied -->

				<!-- infobox div with nifty corners applied -->
				<div class="infobox">

					<table class="default4 jobItems" summary="<spring:message code='jobquot.tableallitems'/>">
						<thead>
							<tr>
								<td colspan="6">${form.job.items.size()}&nbsp;
									<c:choose>
										<c:when test="${form.job.items.size() != 1}">
											<spring:message code='items' />
										</c:when>
										<c:otherwise>
											<spring:message code='item' />
										</c:otherwise>
									</c:choose>&nbsp;
									<spring:message code='jobquot.onjob' />
								</td>
							</tr>
							<tr>
								<th scope="col" class="addtoquote">
									<spring:message code='add' /> <input type="checkbox"
										onclick=" selectAllItems(this.checked, 'jobItems'); " /></th>
								<th scope="col" class="barcode">
									<spring:message code='barcode' />
								</th>
								<th scope="col" class="inst">
									<spring:message code='instrument' />
								</th>
								<th scope="col" class="serial">
									<spring:message code='serialno' />
								</th>
								<th scope="col" class="plant">
									<spring:message code='plantno' />
								</th>
								<th scope="col" class="caltype">
									<spring:message code='caltype' />
								</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="6">&nbsp;</td>
							</tr>
						</tfoot>
						<tbody>
							<c:set var="rowcount" value="1" />
							<c:forEach var="ji" items="${form.job.items}">
								<c:choose>
									<c:when test="${ji.turn <= fastTrackTurn}">
										<c:set var="rowcolor"
											value="${ji.calType.serviceType.displayColourFastTrack}" />
									</c:when>
									<c:otherwise>
										<c:set var="rowcolor" value="${ji.calType.serviceType.displayColour}" />
									</c:otherwise>
								</c:choose>
								<tr style=" background-color: ${rowcolor}">
									<td class="addtoquote">
										<input type="checkbox" name="toQuoteJobItemIds" value="${ji.jobItemId}" />
									</td>
									<td class="barcode">
										<links:instrumentLinkDWRInfo instrument="${ji.inst}" rowcount="0"
											displayBarcode="true" displayName="false" displayCalTimescale="true" />
									</td>
									<td class="inst">${ji.inst.definitiveInstrument}</td>
									<td class="serial">${ji.inst.serialno}</td>
									<td class="plant">${ji.inst.plantno}</td>
									<td class="caltype">
										<t:showTranslationOrDefault
											translations="${ji.calType.serviceType.shortnameTranslation}"
											defaultLocale="${defaultlocale}" />
									</td>
								</tr>
								<c:set var="rowcount" value="${rowcount + 1}" />
							</c:forEach>
						</tbody>
					</table>

				</div>
				<!-- end of infobox div with nifty corners applied -->

				<div class="text-center">
					<input type="submit" name="submit"
						value="<spring:message code='jobquot.tableallitems.createquotefromjob'/>" />
				</div>

			</form:form>

		</div>
	</jsp:body>
</t:crocodileTemplate>