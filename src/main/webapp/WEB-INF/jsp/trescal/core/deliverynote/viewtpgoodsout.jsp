<%-- File name: /trescal/core/deliverynote/viewtpgoodsout.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="delivery" tagdir="/WEB-INF/tags/delivery" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
         <span class="headtext"><spring:message code="viewtpgoodsout.headtext" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/deliverynote/ViewTPGoodsOut.js'></script>
        <script>
			var stateGroupArray = [];
		</script>
        <c:forEach var="sg" items="${stateGroups}">
			<script>
				stateGroupArray.push("${sg}");
			</script>
		</c:forEach>
    </jsp:attribute>
    <jsp:body>
        <!-- div with nifty corners appended -->
		<div class="infobox" id="awaitingDespatchView">
		<cwms:securedLink permission="CLIENT_GOOD_OUT_VIEW" classAttr="mainlink-float">
			<spring:message code="viewtpgoodsout.awaiclides"/>
		</cwms:securedLink>
		
		<!-- clear floats and restore page flow -->
		<div class="clear"></div>								
		
		<div class="text-center">
		
			<h5>
			<spring:message code="viewgoodsout.thearecurr" />&nbsp;
			<cwms:securedLink permission="SCHEDULE_SHOW" classAttr="mainlink" parameter="?filter=future">
				${modelfutureSchedules}
			</cwms:securedLink>&nbsp;
			<spring:message code="viewgoodsout.totschedcoll" />
			(	<cwms:securedLink permission="SCHEDULE_SHOW" classAttr="mainlink" parameter="?filter=today">
				${modeltodaySchedules} 
			</cwms:securedLink>&nbsp;
			<spring:message code="viewgoodsout.duetod" />)
			</h5>
				
		</div>	
		
		<table class="default2 viewGoodsOut" summary="">
			<thead>
			    <tr>
				    <th class="comp" scope="col"><spring:message code="assignschedule.lblcompany" /></th>
				    <th class="subdiv" scope="col"><spring:message code="assignschedule.lblsubdiv" /></th>
				    <th class="addr" scope="col"><spring:message code="assignschedule.lbladdress" /></th>
					<th class="jobno" scope="col"><spring:message code="creategendelnote.jobno" /></th>
					<th class="items" scope="col"><spring:message code="delsearchresults.items" /></th>
				</tr>
			</thead>
		</table>
			
		<c:forEach var="addressDto" items="${dataModel.sourceAddressSet}">
			<div id="divBusSub-${addressDto.key}">
				<c:forEach var="transportOptionDto" items="${dataModel.subdivTransportOptionSet}">
					<delivery:displayTPDespatches dataModel="${dataModel}" sourceAddressDto="${addressDto}" transportOptionDto="${transportOptionDto}" stateGroups="${stateGroups}" />
				</c:forEach>
			</div>
		</c:forEach>
		</div>
		<!-- end of div with nifty corners appended -->
	</jsp:body>
</t:crocodileTemplate>