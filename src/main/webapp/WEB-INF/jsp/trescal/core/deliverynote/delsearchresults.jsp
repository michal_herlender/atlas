<%-- File name: /trescal/core/deliverynote/delsearchresults.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="costs" tagdir="/WEB-INF/tags/costs" %>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>
<%@ taglib prefix="purchaseorder" tagdir="/WEB-INF/tags/purchaseorder" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
        <span class="headtext"><spring:message
				code="delsearchresults.headtext" /></span>
    </jsp:attribute>
	<jsp:attribute name="scriptPart">
        <script type='text/javascript'
			src='script/trescal/core/deliverynote/DelSearchResults.js'></script>
    </jsp:attribute>
	<jsp:body>
       <!-- infobox contains all search results and is styled with nifty corners -->
<div class="infobox">

	<c:set var="results" value="${form.rs.results}" />
	<form:form action="" name="searchform" id="searchform" method="post">
	
	    <form:hidden path="resultsPerPage"/>
	    <form:hidden path="pageNo"/>
	    <form:hidden path="deliverydate"/>
		<form:hidden path="delno" />
        <form:hidden path="consignmentNo"/>
        <form:hidden path="coid" />
        <form:hidden path="deliveryType"/>
        
		<t:showResultsPagination rs="${form.rs}" pageNoId="" />
		
	</form:form>
	<table id="deliverysearchresults" class="default2"
				summary="<spring:message code="delsearchresults.tabledelseares"/>">
		<thead>
			<tr>
				<td colspan="7">
					<spring:message code="delsearchresults.devres" />
					 (<spring:message code="delsearchresults.displaying" />&nbsp;${results.size()})
				</td>
			</tr>
			<tr>
				<th id="delno" scope="col"><spring:message
								code="delsearchresults.delno" /></th>  
				<th id="deltype" scope="col"><spring:message
								code="assignschedule.lbltype" /></th>  
				<th id="deldate" scope="col"><spring:message
								code="assignschedule.lbldate" /></th>  
				<th id="delitem" scope="col"><spring:message
								code="delsearchresults.items" /></th>
				<th id="delcomp" scope="col"><spring:message
								code="assignschedule.lblcompany" /> / <spring:message code="subdiv" /></th>
				<th id="delcont" scope="col"><spring:message
								code="assignschedule.lblcontact" /></th>
				<th id="deljob" scope="col"><spring:message
								code="creategendelnote.jobno" /></th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="7">&nbsp;</td>
			</tr>
		</tfoot>
		<tbody id="keynavResults">
			<c:set var="rowcount" value="0" />
			<c:choose>	
              <c:when test="${results.size() < 1}">
				<tr class="odd">
					<td colspan="7" class="bold center">
						<spring:message code="delsearchresults.keynavResults" />
					</td>
				</tr>
			</c:when>
            <c:otherwise>	
				<c:forEach items="${results}" var="d">
					<tr id="parent${rowcount}"
						<c:choose>
							<c:when test="${rowcount == 0}"> class="hoverrow"</c:when>
							<c:when test="${rowcount % 2 == 0}"> class="even"</c:when>
							<c:otherwise>class="odd"</c:otherwise>
						</c:choose>
							     onclick=" window.location.href='viewdelnote.htm?delid=${d.deliveryid}'" 
							     onmouseover=" $j(this).addClass('hoverrow'); "
							     onmouseout=" if (this.className != 'hoverrow') $j(this).removeClass('hoverrow'); " >
							     
								<td class="center">${d.deliveryno}</td>
								<td>${d.type.getDescription()}</td>
								<td class="center"><fmt:formatDate value="${d.deliverydate}" type="date" dateStyle="medium" /></td>
								<td class="center">${d.getItems().size()}
									<a href="#" id="itemsLink${rowcount}"
										onclick=" getDeliveryItems(event, ${rowcount}, ${d.deliveryid}, ${d.generalDelivery}); return false; ">
									<img src="img/icons/items.png" width="16" height="16"
										alt="<spring:message code='delsearchresults.imgitemstitle'/>"
										title="<spring:message code='delsearchresults.imgitemstitle' />"
										class="image_inline" />
									</a>
								</td>
								<td>
								<c:choose>
							    	<c:when test="${not empty d.contact}">
										<links:companyLinkDWRInfo company="${d.contact.sub.comp}" rowcount="0" copy="true" /><br/>
										<links:subdivLinkDWRInfo rowcount="0" subdiv="${d.contact.sub}" />
									</c:when>
									<c:otherwise> 
										${d.freehandContact.company} 
										<c:if test="${not empty d.freehandContact.subdiv && not empty d.freehandContact.company}">
											<c:out value=" - " />
										</c:if>
										${d.freehandContact.subdiv}
									</c:otherwise>
								</c:choose>
								</td>
								<td>
								<c:choose>
									<c:when test="${not empty d.contact}">
                                       <links:contactLinkDWRInfo rowcount="0" contact="${d.contact}" />
									</c:when>
									<c:otherwise> 
								   		${d.freehandContact.contact} 
									</c:otherwise>
								</c:choose>
								</td>
								<td class="center">
								
								<c:choose>
								<c:when test="${not d.generalDelivery}">  
									<links:jobnoLinkDWRInfo rowcount="${rowcount}" jobno="${d.job.jobno}" jobid="${d.job.jobid}" copy="${true}"/> 
								</c:when>
								<c:otherwise>
								 	<c:out value=" - " />
									<spring:message code="creategendelnote.nojob" /> 
								 	<c:out value=" - " />
								</c:otherwise>
								</c:choose>
								</td>
							   </tr>
							<c:set var="rowcount" value="${rowcount + 1}" />					
			        </c:forEach>
			  </c:otherwise>
			 </c:choose>
		</tbody>
	</table>
</div>
<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>