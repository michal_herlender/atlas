<%-- File name: /trescal/core/deliverynote/createdelnote.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:crocodileTemplate>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/deliverynote/DelSearch.js'></script>
	</jsp:attribute>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="delsearch.headtext"/></span>
	</jsp:attribute>
	<jsp:body>
		<!-- infobox contains all search form elements and is styled with nifty corners -->
		<div class="infobox" id="deliverynotesearch">
				
			<div class="float-right"><a href="selecttpcontact.htm" class="mainlink"><spring:message code="delsearch.mainlink"/></a></div>
		
			<div class="clear"></div>
		
			<form:form action="" method="post" id="delsearchform" name="delsearchform">
		
			<fieldset>
			
				<legend><spring:message code="delsearch.legend"/></legend>
				
				<ol>
				
					<li>
						<label><spring:message code="fromcompany"/>:</label>
						${allocatedCompany.value}
					</li>

					<li>
						<label><spring:message code="fromsubdivision"/>:</label>
						${allocatedSubdiv.value}
					</li>

					<li>
						<label><spring:message code="delnotesummary.deltype"/>:</label>
						<form:select path="deliveryType" items="${deliveryTypes}" itemLabel="value" itemValue="key" />
					</li>
					
					<li>
						<label><spring:message code="delnotesummary.delno"/>:</label>
						<form:input path="delno" id="delno" tabindex="2" />
					</li>
					
					<li>
						<label><spring:message code="delnotesummary.deldate"/>:</label>
						<form:input path="deliverydate" id="date" type="date" tabindex="3" />
					</li>
					
					<li>
						<label><spring:message code="cdsearch.lbl1"/>:</label>
						<form:input path="consignmentNo" id="consignmentNo" tabindex="4" />
					</li>
					
					<li>
						<label><spring:message code="assignschedule.lblcompany"/>:</label>
						<select id="coroleselect" name="coroleselect" onchange=" changeCorole(this.value); " tabindex="5">
							<c:forEach var="companyRole" items="${companyRoles}">
								<option value="${companyRole}">${companyRole.getMessage()}</option>
							</c:forEach>
						</select>										
						<div class="float-left extendPluginInput">
							<div class="compSearchJQPlugin">
								<input type="hidden" name="field" value="coid" />
								<input type="hidden" name="compCoroles" value="client" />
								<input type="hidden" name="tabIndex" value="6" />
								<!-- company results listed here -->
							</div>
						</div>										
						<!-- div clears the page flow -->
						<div class="clear"></div>
					</li>
					
					<li>
						<label>&nbsp;</label>
						<input type="submit" value="<spring:message code="assignschedule.inputsubmit" />" id="submit" tabindex="7" />
					</li>
				
				</ol>
			
			</fieldset>
			
			</form:form>		
		
		</div>
		<!-- end of infobox div -->
	</jsp:body>
</t:crocodileTemplate>