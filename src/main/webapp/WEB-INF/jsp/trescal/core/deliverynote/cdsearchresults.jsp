<%-- File name: /trescal/core/deliverynote/cdsearchresults.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel" %>

<t:crocodileTemplate>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/deliverynote/CDSearchResults.js'></script>
	</jsp:attribute>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="cdsearchresults.headtext"/></span>
	</jsp:attribute>
	<jsp:body>
		<!-- infobox contains all search results and is styled with nifty corners -->
		<div class="infobox" id="courierdesresults">
			<form:form action="" modelAttribute="form" name="searchform" id="searchform" method="post">
				<form:hidden path="consignmentno"/>
				<form:hidden path="courierid"/>
				<form:hidden path="couriertypeid"/>
				<form:hidden path="date"/>
				<form:hidden path="companyid"/>
				<form:hidden path="pageNo"/>
				<form:hidden path="resultsPerPage"/>
			
			<t:showResultsPagination rs="${form.rs}" pageNoId=""/>
			<table class="default2" summary="">
				<thead>
					<tr>
						<td colspan="5">
							<spring:message code="cdsearchresults.string1"/>&nbsp;${form.rs.results.size()}&nbsp;
							<c:choose>
								<c:when test="${form.rs.results.size() == 1}">
									<spring:message code="cdsearchresults.result"/>
								</c:when>
								<c:otherwise>
									<spring:message code="cdsearchresults.results"/>
								</c:otherwise>
							</c:choose>
						</td>
					</tr>
					<tr>
						<th id="cdconno" scope="col"><spring:message code="cdsearch.lbl1"/></th>
						<th id="cddel" scope="col"><spring:message code="cdsearchresults.coldel"/></th>
						<th id="cdcourier" scope="col"><spring:message code="cdsearch.lbl2"/></th>
						<th id="cddespatch" scope="col"><spring:message code="cdsearch.lbl3"/></th>
						<th id="cddate" scope="col"><spring:message code="cdsearch.lbl4"/></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="5">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody id="keynavResults">
					<c:choose>
						<c:when test="${form.rs.results.size() == 0}">
							<tr class="odd">
								<td colspan="5" class="bold center">
									<spring:message code="cdsearchresults.string2"/>
								</td>
							</tr>
						</c:when>
						<c:otherwise>
							<c:forEach var="cd" items="${form.rs.results}" varStatus="loop">
								<tr id="parent${loop.index}"
									class="<c:choose>
											<c:when test='${loop.index == 0}'>hoverrow</c:when>
											<c:when test='${loop.index%2 == 0}'>even</c:when>
											<c:otherwise>odd</c:otherwise>
										</c:choose>"
									onclick="window.location.href='viewcrdes.htm?cdid=${cd.id}'"
									onmouseover="$j(this).addClass('hoverrow');"
									onmouseout="if(this.className != 'hoverrow') $j(this).removeClass('hoverrow');">
									<td>
										<c:choose>
											<c:when test="${empty cd.consignmentno}">
												-- N/A --
											</c:when>
											<c:otherwise>
												${cd.consignmentno}
											</c:otherwise>
										</c:choose>
									</td>
									<td class="center">
										${cd.deliveriesCount}
										<a href="" id="deliveryLink${loop.index}" onclick="event.preventDefault(); getDeliveries(event, ${loop.index}, ${cd.id});">
											<img src="img/icons/deliveries.png" width="16" height="16" alt="<spring:message code='cdsearchresults.imgviewdel'/>" title="<spring:message code='cdsearchresults.imgviewdel'/>" class="image_inline" />
										</a>
									</td>
									<td>${cd.courierName}</td>
									<td>${cd.cdTypeDescription}</td>
									<td class="center"><fmt:formatDate type="date" value="${cd.despatchDate}" dateStyle="medium"/></td>
								</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
			<t:showResultsPagination rs="${form.rs}" pageNoId=""/>
		</form:form>
		</div>
		<!-- end of infobox div -->
	</jsp:body>
</t:crocodileTemplate>