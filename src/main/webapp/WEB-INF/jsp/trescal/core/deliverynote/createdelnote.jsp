<%-- File name: /trescal/core/deliverynote/createdelnote.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address" %>
<%@ taglib prefix="company" tagdir="/WEB-INF/tags/company" %>
<%@ taglib prefix="delivery" tagdir="/WEB-INF/tags/delivery" %>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>

<t:crocodileTemplate>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/deliverynote/CreateDelNote.js'></script>
		<script src='script/thirdparty/jQuery/jquery.js' ></script>
		<script src='script/thirdparty/jQuery/jquery.dataTables.min.js' ></script>
		<script type='module' src='script/components/cwms-overlay/cwms-overlay.js' ></script>
		<script type='module' src='script/components/cwms-translate/cwms-translate.js'></script>
		<script>
			 function updateInstructionSize(instructionSize) {}
		</script>
		<script type="module" src="script/components/cwms-instructions/cwms-instructions.js"></script>
		<link rel="stylesheet" type="text/css" href="script/thirdparty/DataTables/datatables.min.css" />
        <style>
            .dataTables_wrapper .dataTables_filter {
    			display: none;
			}

			.dataTables_paginate {
    			margin-top: 0px;
    			position: absolute;
    			text-align: center;
    			left: 40%;
			}
			#addressid {
				max-width : 700px;
			}
		</style>
		
		<script type="text/javascript">
			var existingCDs = ${existingCDs};
		    var couriers = ${couriers};
		</script>
	</jsp:attribute>
	<jsp:attribute name="header">
	    <c:set var="deliveryType" value="client" ></c:set>
			<c:if test="${delnoteform.delivery.getType() eq 'THIRDPARTY'}">
			   <c:set var="deliveryType" value="alligator.label.tp.thirdparty" ></c:set>
			</c:if>
			<c:if test="${delnoteform.delivery.getType() eq 'INTERNAL'}">
				<c:set var="deliveryType" value="internal" ></c:set>
			</c:if>
			<c:if test="${delnoteform.delivery.getType() eq 'INTERNAL_RETURN'}">
				<c:set var="deliveryType" value="internal.return" ></c:set>
			</c:if>
		<span class="headtext">
			<spring:message code="createdelnote.headtext1"/>&nbsp;
			<spring:message code="createdelnote.headtext3"/> &nbsp;(<spring:message code="${deliveryType}"/>)
		</span>
	</jsp:attribute>
	<jsp:body>
		<!-- part-delivery section displayed when company does not allow part deliveries -->
		<c:set var="disabled" value="" />
		<c:if test="${!delnoteform.allowsPartDeliveries}">
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<h5 class="center-0 attention"><spring:message code="createdelnote.partdel"/></h5>
					</div>
				</div>
			</div>
			<c:set var="disabled" value="disabled" />
		</c:if>
		<!-- end part-delivery section -->
		<t:showErrors path="delnoteform.*" showFieldErrors="true" />
		<!-- infobox containing form elements for creating a new delivery note with nifty corners applied -->
		<div class="infobox" id="createdelnote">
			<div class="displaycolumn-70">
				<form:form method="post" action="" id="delnoteform" modelAttribute="delnoteform" onsubmit="submit.disabled = true;return true;">
					<fieldset>
						<legend>
							<spring:message code="createdelnote.headtext1"/>
							&nbsp;<spring:message code="createdelnote.headtext3"/>&nbsp;(<spring:message code="${deliveryType}"/>) &nbsp;
							<spring:message code="jobcost.forjob"/> :
							&nbsp;<a id="linkToJob" href="viewjob.htm?jobid=${delnoteform.job.jobid}" class="mainlink">${delnoteform.job.jobno}</a>
						</legend>
						<ol>
							<c:if test="${showInstructions}">
								<li>
									<cwms-instructions link-jobid="${delnoteform.job.jobid}" instruction-types="PACKAGING,CARRIAGE" 
									show-mode="TABLE" delivery-type="${ delnoteform.delivery.getType() }"></cwms-instructions>
								</li>
							</c:if>
							<li>
								<label><spring:message code="jobno"/>:</label>
								<span>${delnoteform.job.jobno}</span>
							</li>
							<li>
								<label><spring:message code="fromsubdivision"/>:</label>
								${allocatedCompany.value} - ${allocatedSubdiv.value}
							</li>
								<li class="vis" id="companyname">
									<label><spring:message code="delnotesummary.deliverysubdivision"/>:</label>
									<span>
										<c:out value="${coname} - ${subdivName}" />
									</span>
								</li>
								
								<li class="hide">
								    <label> &nbsp;&nbsp;&nbsp;</label>
									<button type="button" onClick="switchCompany()">Switch Company</button>
								</li>
					
								<li class="vis" id="contactselector">
									<label><spring:message code="assignschedule.lblcontact"/>*:</label>
									<div class="float-left">
										<form:select path="contactid">
											<option value=""></option>
											<c:forEach items="${contacts}" var="con">
											    <c:set var="selected" value="" />
											    <c:if test="${con.personid == defaultContactId}">
											    	<c:set var="selected" value="selected" />
											    </c:if>
											    <option value="${con.personid}" ${selected}>${con.name}</option>
											</c:forEach>
											
											<c:forEach items="${overriddenReturnToContacts}" var="ct" varStatus="vs" >
												<c:if test="${ vs.index eq 0 }">
													<option disabled><spring:message code="createdelnote.addressesoverriddenonjobitems"/>:</option>
												</c:if>
												<option class="overriddenContact" value="${ct.personid}">${ct.name} (${ ct.sub.comp.coname })</option>
											</c:forEach>
											
										</form:select>
										<form:errors path="contactid"/>
									</div>
									<div class="clear"></div>
								</li>
								
								<li class="vis" id="addressselector">
									<label><spring:message code="createdelnote.seladdr"/>:</label>
									<div class="float-left">
										<%-- Special message to identify client default return-to address --%>
										<spring:message var="jobAddressMessage" code='jicontractreview.string5' />
										<c:set var="jobAddressText" value=" (<span>${jobAddressMessage}</span>)" />
										<form:select path="addressid" onchange="addrChange(this.value,${empty delnoteform.job.returnToLoc.locationid ? 0 : delnoteform.job.returnToLoc.locationid });">
											
											<c:forEach items="${activeDeliveryAddresses}" var="addr" varStatus="vs" >
												<option value="${addr.addrid}" <c:if test="${addr.addrid == delnoteform.addressid}">selected</c:if>>
													<c:out value="${addr.addressLine}" />
													<c:if test="${(delnoteform.delivery.type eq 'CLIENT') && (addr.addrid eq delnoteform.job.returnTo.addrid)}">
														${jobAddressText}
													</c:if>
												</option>
											</c:forEach>
											<c:forEach items="${overriddenDeliveryAddresses}" var="addr" varStatus="vs" >
												<c:if test="${ vs.index eq 0 }">
													<option disabled><spring:message code="createdelnote.addressesoverriddenonjobitems"/>:</option>
												</c:if>
												<option value="${addr.addrid}"><span>${addr.addressLine} <br> (${ addr.sub.comp.coname })</span></option>
											</c:forEach>
											
										</form:select>
										<form:errors path="addressid"/>
									</div>
									<div class="clear"></div>
								</li>
								
							<li class="hid" id="alternatcompanyselector">
								<label><spring:message code="assignschedule.lblcompany"/>:</label>
																		
								<!-- this div creates a new cascading search plugin. The default search contains an input 
									 field and results box for companies, when results are returned this cascades down to
									 display the subdivisions within the selected company. This default behavoir can be obtained
									 by including the hidden input field with no value or removing the input field. You can also
									 add address and/or contact searches to the cascade by including the values 'contact' and/or 'address'
									 in the hidden inputs value attribute separated by a comma (e.g value="contact", value="address",
									 value="address,contact").  You can also swap these values around (e.g. value="contact,address").  
								 -->									
								<div id="cascadeSearchPlugin">
									<input type="hidden" id="cascadeRules" value="subdiv,contact,address" />
									<input type="hidden" id="compCoroles" value="business,supplier,utility,client" />
									<input type="hidden" id="loadCurrency" value="true" />
									<input type="hidden" id="addressCallBackFunction" value="cascadeAddressSearchCallback" />
								</div>
					
								<!-- clear floats and restore page flow -->
								<div class="clear"></div>
							</li>
							<li id="delLoc" class="hid">
								<label><spring:message code="createdelnote.selloc"/>:</label>
								<form:select path="locid"/>
								<form:errors path="locid"/>
							</li>
							<li>
								<label><spring:message code="createdelnote.delivdate"/>:</label>
								<form:input path="deliverydate"  type="date"/>
								<form:errors path="deliverydate"/>
							</li>
							<li>
								<label><spring:message code="createdelnote.groupaccessories"/>:</label>
								<div class="float-left attention">
									<c:forEach var="jobItemGroup" items="${delnoteform.job.groups}">
										<div id="group${jobItemGroup.id}">
											<spring:message code="accessoriesanddefects.group"/> ${jobItemGroup.id}:
											<c:forEach var="acc" items="${jobItemGroup.accessories}" varStatus="loopStatus">
												<c:if test="${!loopStatus.first}">, </c:if>
												<cwms:besttranslation translations="${acc.accessory.translations}"/>
												x ${acc.quantity}
											</c:forEach>
											<c:if test="${jobItemGroup.accessoryFreeText != null}">
												<c:if test="${jobItemGroup.accessories.size() > 0}">, </c:if>
												${jobItemGroup.accessoryFreeText}
											</c:if>
										</div>
									</c:forEach>
								</div>
								<!-- clear floats and restore page flow -->
								<div class="clear"></div>
							</li>
							<li>
								<label><spring:message code="createdelnote.delivinstr"/>:</label>
								<form:textarea path="delInstruction" class="width80 addSpellCheck" style=" height: 100px; "/>
								<form:errors path="delInstruction"/>
							</li>
						</ol>
						<ol id="cdOptions">
							<li>
								<label><spring:message code="createdelnote.courdisp"/>:</label>
								<select id="selectant" name="selectant" onchange="changeDespatchOption(this.value, '${delnoteform.deliverydate}', ${subdivid.comp.coid}); return false;">
									<option value="default" selected="selected">
										<spring:message code="createdelnote.optcourdisp"/>
									</option>
									<c:if test="${delnoteform.despatches.size() > 0}">
										<option value="exists"><spring:message code="createdelnote.excourdisp"/></option>
									</c:if>
									<option value="new"><spring:message code="createdelnote.newcourdisp"/></option>
								</select>
							</li>
							<li>
								<label>&nbsp;</label>
								<input type="button" id="submit" name="submit" value="<spring:message code='assignschedule.inputsubmit'/>"
									onclick="event.preventDefault(); onSubmit(${delnoteform.job.jobid},'${delnoteform.delivery.getType()}');" 
									${disabled}/>
							</li>
						</ol>
					</fieldset>
				</form:form>
			</div>
			<div class="displaycolumn-30">
				<fieldset>
					<legend><spring:message code="createdelnote.fastitemscanning"/></legend>
					<ol>
						<li id="rejectWarning1" class="hid">
							<div class="warningBox1">
								<div class="warningBox2">
									<div class="warningBox3">
										<p><spring:message code="createdelnote.cannotadded"/>!</p>
									</div>
								</div>
							</div>
						</li>
						<li id="rejectWarning2" class="hid">
							<div class="warningBox1">
								<div class="warningBox2">
									<div class="warningBox3">
										<p><spring:message code="createdelnote.alreadychecked"/>!</p>
									</div>
								</div>
							</div>
						</li>
						<li id="rejectWarning3" class="hid">
							<div class="warningBox1">
								<div class="warningBox2">
									<div class="warningBox3">
										<p><spring:message code="createdelnote.notallaccessories"/>.</p>
									</div>
								</div>
							</div>
						</li>
						<li id="accessoryWarning" class="hid">
							<div class="successBox1">
								<div class="successBox2">
									<div class="successBox3">
										<p id="accWarningText"><spring:message code="createdelnote.instrumentaccessories"/>:</p>
										<p><spring:message code="createdelnote.checkthemall"/></p>
										<button id="addWithAcc"><spring:message code="yes"/></button>
										<button id="rejectAcc"><spring:message code="no"/></button>
									</div>
								</div>
							</div>
						</li>
						<li>
							<label><spring:message code="barcode"/></label>
							<input id="barcodeScan" onkeypress="if(event.keyCode==13){event.preventDefault();}"/>
						</li>
						<li>
							<label><spring:message code="company.history"/></label>
							<div id="scanHistory" class="float-left" style="height: 150px; width: 150px; overflow: auto;"></div>
						</li>
						<li>
							<label><spring:message code="createdelnote.statistic"/></label>
							<ul>
								<li></li>
								<li>
									<label><spring:message code="createdelnote.successful"/></label>
									<input id="successCounter" type="number" readonly disabled value="0"/>
								</li>
								<li>
									<label><spring:message code="createdelnote.rejected"/></label>
									<input id="rejectCounter" type="number" readonly disabled value="0"/>
								</li>
								<li>
									<label><spring:message code="createdelnote.unselecteditems"/></label>
									<input id="unselectCounter" type="number" readonly disabled value="0"/>
								</li>
							</ul>
						</li>
					</ol>
				</fieldset>
				
			</div>
			<div class="clear"></div>
			<!-- error section displayed when form submission fails -->
			<t:showErrors path="delnoteform.*" showFieldErrors="true" />
			<!-- end error section -->
			<h5> <spring:message code="createdelnote.string1"/> (<spring:message code="${deliveryType}"/>)</h5>
			
			<table class="default4 delItemTable dItemTable" id="itemTable" style="width:100%; height: 50px">
				<thead>
					<tr>
						<th class="adiadd" scope="col">
							<input type="checkbox" id="selectAllTab" ${disabled}/>
							<spring:message code="all"/>
						</th>
						<th class="hide"></th>
						<th class="" scope="col"><spring:message code="jobno"/></th>
						<th class="adiitem" scope="col"><spring:message code="createdelnote.colitemno"/></th>
						<th class="adipo"><spring:message code="clientpo"/></th>
						<th class="adiclientref"><spring:message code="invoiceexport.jobclientref"/></th>
						<th class="adiclientref"><spring:message code="jicontractreview.jobitemclientref"/></th>
						<th class="adiclientref" scope="col"><spring:message code="createdelnote.accessories"/></th>
						<th class="adiitem" scope="col"><spring:message code="createdelnote.jobitemgroup"/></th>
						<th class="adidesc" scope="col"><spring:message code="instmodelname"/></th>
						<th class="adidesc" scope="col"><spring:message code="viewjob.returntoaddr"/></th>
						<th class="adidesc" scope="col"><spring:message code="viewjob.returntocontact"/></th>
						<th class="adidesc" scope="col"><spring:message code="createdelnote.instrumentcontact"/></th>
						<th class="adidesc" scope="col"><spring:message code="createdelnote.instrumentaddress"/></th>
						<th class="adidesc" scope="col"><spring:message code="createdelnote.instrumentlocation"/></th>
						<th class="adiserial" scope="col"><spring:message code="createdelnote.colserial"/></th>
						<th class="adiplant" scope="col"><spring:message code="createdelnote.colplant"/></th>
						<th class="adiout"><spring:message code="workflow.transportout"/></th>
					</tr>
				</thead>
				<template id="itemTableCheckBoxTemplate">
					<input type='checkbox' class='jobItem' onchange='handleMainCheckBoxChange(this);' name='items' value='jobItemId' ${disabled}/>
					<span class="hid"><spring:message code="createdelnote.ready"/></span>
				</template>
			</table>
		</div>
		<!-- end infobox div -->
	</jsp:body>
</t:crocodileTemplate>
<cwms-overlay id="submitData">
	<cwms-translate slot="cwms-overlay-title" code="createdelnote.wait" >Loading...please wait</cwms-translate>
    <div slot="cwms-overlay-body">
    	<center>
            <img src="img/icons/ajax-loader.gif" width="31" height="31" alt="" />
        </center>
    </div>
</cwms-overlay>
<cwms-overlay id="incompatibilityAlertPopup">
	<cwms-translate slot="cwms-overlay-title" code="warning" >Warning!</cwms-translate>
    <div slot="cwms-overlay-body" style="max-width: 500px;margin-top: 16px;">
    	<center>
            <cwms-translate slot="cwms-overlay-title" code="error.deliverynote.createdelnot.incomatibleaddressesorcontact"
            	style="font-size: 13px;" >
            	The item you selected is not compatible with either the main return to address or contact, Please check above !
            </cwms-translate>
        </center>
    </div>
</cwms-overlay>