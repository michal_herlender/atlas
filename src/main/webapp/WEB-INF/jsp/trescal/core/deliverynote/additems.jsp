<%-- File name: /trescal/core/deliverynote/additems.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel" %>

<t:crocodileTemplate>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/deliverynote/AddItems.js'></script>
	</jsp:attribute>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="additems.headtext"/></span>
	</jsp:attribute>
	<jsp:body>
		<!-- part-delivery section displayed when company does not allow part deliveries -->
		<c:if test="${!form.allowsPartDeliveries}">
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<h5 class="center-0 attention"><spring:message code="additems.partdeliv"/>:</h5>
					</div>
				</div>
			</div>
		</c:if>
		<!-- end part-delivery section -->
		<!-- error section displayed when form submission fails -->
		<t:showErrors path="form.*" showFieldErrors="true" />
		<!-- end error section -->
		<!-- infobox contains table for adding job items and is styled with nifty corners -->
		<div class="infobox" id="adddelitems">
			<form:form method="post" action="" modelAttribute="form">
				<h5><spring:message code="additems.subheadtext"/>&nbsp;<a href="viewdelnote.htm?delid=${form.delivery.deliveryid}" class="mainlink">${form.delivery.deliveryno}</a></h5>
				<table class="default2" id="delItemTable" summary="<spring:message code='additems.delitemtable'/>">
					<thead>
						<tr>
							<td colspan="5"><spring:message code="additems.tablecolspan"/> (${form.remainingJobItems.size()})</td>
						</tr>
						<tr>
							<th id="adiadd" scope="col">
								<input type="checkbox" id="selectAll" onclick="selectAllItems(this.checked, 'delItemTable');"/> <spring:message code="additems.selectall"/>
							</th>
							<th id="adiitem" scope="col"><spring:message code="additems.coladiitem"/></th>
							<th id="adidesc" scope="col"><spring:message code="additems.coladidesc"/></th>
							<th id="adiserial" scope="col"><spring:message code="additems.coladiserial"/></th>
							<th id="adiplant" scope="col"><spring:message code="additems.coladiplant"/></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="5">
								<spring:message code="additems.footcolspan"/>&nbsp;
								<a href="viewjob.htm?jobid=${form.job.jobid}" class="mainlink">${form.job.jobno}</a>
							</td>
						</tr>
					</tfoot>
					<tbody>
						<c:forEach var="i" items="${form.remainingJobItems}" varStatus="itemLoop">
							<tr class=
									<c:choose>
										<c:when test="${itemLoop.index%2 == 0}">"even"</c:when>
										<c:otherwise>"odd"</c:otherwise>
									</c:choose>
								onmouseover="$j(this).addClass('hoverrow');"
								onmouseout="if(this.className != 'hoverrow') $j(this).removeClass('hoverrow');"
								onclick="$j(this).find('input').toggleCheck();">
								<td class="center">
									<c:choose>
										<c:when test="${form.ready.contains(i.jobItemId)}">
											<form:checkbox path="items[${i.jobItemId}]"
												value="${form.items.get(i.jobItemId) == 'on'}"
												onclick="event.cancelBubble = true;"/>
										</c:when>
										<c:otherwise>
											Ready?
										</c:otherwise>
									</c:choose>
								</td>
								<td class="center">${i.itemNo}</td>
								<td><instmodel:showInstrumentModelName instrument="${i.inst}" caltypeid="${i.calType.calTypeId}" relativePathToImages=""/></td>
								<td>${i.inst.serialno}</td>
								<td>${i.inst.plantno}</td>
							</tr>
							<!-- if item has accessories -->
							<c:choose>
								<c:when test="${not empty i.accessories || not empty i.accessoryFreeText}">
									<tr class=
										<c:choose>
											<c:when test="${itemLoop.index % 2 == 0}">"even"</c:when>
											<c:otherwise>"odd"</c:otherwise>
										</c:choose>>
										<td class="center"><img width="16" height="16" title="accessories" alt="accessories" src="img/icons/arrow_merge.png"/></td>
										<td colspan="4">
											<form:checkbox path="accessories[${i.jobItemId}]" value="${form.accessories.get(i.jobItemId) == 'on'}"/>
											<c:forEach var="acc" items="${i.accessories}" varStatus="accLoop">
												<c:if test="${!accLoop.first}">, </c:if>
												<cwms:besttranslation translations="${acc.accessory.translations}"/>
												x ${acc.quantity}
											</c:forEach>
											<c:if test="${not empty i.accessoryFreeText}">
												<c:out value=", ${i.accessoryFreeText}" />
											</c:if>
										</td>
									</tr>
								</c:when>
								<c:otherwise>
									<tr class="hid">
										<td colspan="5">
											<form:hidden path="accessories[${i.jobItemId}]" value="on"/>
										</td>
									</tr>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</tbody>
				</table>
				<div class="center">
					<input type="submit" id="submit" name="submit" value="<spring:message code='additems.submitadditems'/>"/>
				</div>
			</form:form>
		</div>
		<!-- end of infobox div -->
	</jsp:body>
</t:crocodileTemplate>