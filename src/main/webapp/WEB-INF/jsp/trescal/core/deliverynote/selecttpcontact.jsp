<%-- File name: /trescal/core/deliverynote/selecttpcontact.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
		<span class="headtext">
			<spring:message code="selecttpcontact.newhead" />&nbsp;<spring:message code="jithirdparty.thirdpartydelivery" />
		</span>
	</jsp:attribute>
    <jsp:attribute name="scriptPart">
    	<script type='text/javascript' src='script/trescal/core/deliverynote/SelectTPContact.js'></script>
    </jsp:attribute>
    <jsp:body>
    	<!-- error section displayed when form submission fails -->
		<t:showErrors path="form.*" showFieldErrors="true" />
		<!-- end error section -->
		<!-- infobox contains search for third party contact and is styled with nifty corners -->
		<div class="infobox">
			<c:set var="compRolesValue" value="client,supplier,business"/>
			<c:set var="checked" value=""/>
			<c:if test="${ form.text == 'Third Party' }">
				<c:set var="compRolesValue" value="supplier,business"/>
				<c:set var="checked" value="checked"/>
			</c:if>
			<form:form id="" method="post">
				<fieldset>
					<legend><spring:message code="selecttpcontact.legend" />&nbsp;<spring:message code="jithirdparty.thirdpartydelivery" /></legend>
					<ol>
						<li>
							<label><spring:message code="companysearch.companyroles" />:</label>
							<input type="radio" class="checkbox" name="activeradio"  checked  onclick="companyRole('ALL');"/><spring:message code="all" />
							<input type="radio" class="checkbox" name="activeradio" value=""  onclick="companyRole('CLIENT');"><spring:message code="companyrole.client" />
							<input type="radio" class="checkbox" name="activeradio" value="" ${checked} onclick="companyRole('BUSINESS');"><spring:message code="companyrole.supplier" />/<spring:message code="companyrole.business" />
						</li>
						<li>
							<label for="cascadeSearchPlugin"><spring:message code="selecttpcontact.selcon" />:</label>
							<!-- this div creates a new cascading search plugin. The default search contains an input 
								 field and results box for companies, when results are returned this cascades down to
								 display the subdivisions within the selected company. This default behavoir can be obtained
								 by including the hidden input field with no value or removing the input field. You can also
								 add address and/or contact searches to the cascade by including the values 'contact' and/or 'address'
								 in the hidden inputs value attribute separated by a comma (e.g value="contact", value="address",
								 value="address,contact").  You can also swap these values around (e.g. value="contact,address").  
							 -->									
							<div id="cascadeSearchPlugin" class="float-left">
								<input type="hidden" id="cascadeRules" value="subdiv,contact" />
								<input type="hidden" id="compCoroles" value="${compRolesValue}" />
								<input type="hidden" id="cascadeVisible" value="true" />
							</div>
							
							<div class="clear"> </div>								
						</li>
						<li>
							<label>&nbsp;</label>
							<input id="submit" name="submit" type="submit" value="<spring:message code="assignschedule.inputsubmit" />" /> 
						</li>
					</ol>
				</fieldset>
			</form:form>
		</div>
		<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>