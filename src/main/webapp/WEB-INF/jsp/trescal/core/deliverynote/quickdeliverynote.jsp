<%-- File name: /trescal/core/deliverynote/createdelnote.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="createdelnote.quickdeliverynote"/></span>
	</jsp:attribute>
	<jsp:body>
		<t:showErrors path="form.*" showFieldErrors="true" />
		<div class="infobox">
			<form:form modelAttribute="form" method="post">
				<fieldset>
					<legend><spring:message code="createdelnote.scanbarcode"/></legend>
					<ol>
						<li>
							<form:label path="barcode"><spring:message code="barcode"/></form:label>
							<form:input path="barcode" pattern="[0-9]*" onkeypress="if(event.keyCode==13) $j(this).closest('form').submit());"/>
						</li>
					</ol>
				</fieldset>
			</form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>