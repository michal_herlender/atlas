<%-- File name: /trescal/core/deliverynote/viewnewgoodsout.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="delivery" tagdir="/WEB-INF/tags/delivery" %>

<t:crocodileTemplate>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/deliverynote/ViewGoodsOut.js'></script>
		<script>
			var stateGroupArray = [];
		</script>
		<c:forEach var="sg" items="${stateGroups}">
			<script>
				stateGroupArray.push("${sg}");
			</script>
		</c:forEach>
	</jsp:attribute>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="viewgoodsout.headtext"/></span>
	</jsp:attribute>
	<jsp:body>
		<!-- div with nifty corners appended -->
		<div class="infobox" id="awaitingDespatchView">
			<a href="viewtpgoodsout.htm" class="mainlink-float"><spring:message code="viewgoodsout.awaihtpardes"/></a>
			<!-- clear floats and restore page flow -->
			<div class="clear"></div>
			<div class="text-center">
				<h5>
					<spring:message code="viewgoodsout.thearecurr"/>&nbsp;
					<a href="showschedules.htm?filter=future" title="<spring:message code='viewgoodsout.titleviewsched'/>" class="mainlink">${futureSchedules}</a>
					<spring:message code="viewgoodsout.totschedcoll"/>
					(<a href="showschedules.htm?filter=today" title="<spring:message code='viewgoodsout.titleviewsched1'/>" class="mainlink">${todaySchedules}</a>
					<spring:message code="viewgoodsout.duetod"/>)
				</h5>
			</div>
			<table class="default2" id="despatchOptions" summary="">								
				<tbody>
				<%-- Note : Formerly, showed today's daily transport option via selected, now user should select their own --%>
				<c:set var="selectedClass" value=""/>
				<c:forEach var="addressDto" items="${dataModel.sourceAddressSet}">
					<tr>
						<th addressid="${addressDto.key}" class="opts" onclick="event.preventDefault(); showAllInSub(${addressDto.key}); ">${addressDto.value}</th>
						<c:forEach var="transportOptionDto" items="${dataModel.subdivTransportOptionSet}">
							<td id="opt-${addressDto.key}-${transportOptionDto.key}" class="opts ${selectedClass}" 
								onclick="event.preventDefault(); showSingleOption(${transportOptionDto.key}, ${addressDto.key}); ">
									${transportOptionDto.value} (${dataModel.getItemCount(addressDto.key, transportOptionDto.key)})
							</td>
						</c:forEach>
					</tr>
					<%-- Transport options belonging to other subdivisions --%>
					<c:set var="unexpectedOptions" value="${dataModel.getUnexpectedTransportOptions(addressDto.key)}" />
					<c:if test="${not empty unexpectedOptions}">
						<tr>
							<th class="opts" onclick="event.preventDefault(); showAllInSub(0); "><spring:message code="viewgoodsout.otherreturnsubdivision"/></th>
							<c:forEach var="transportOptionDto" items="${unexpectedOptions}">
								<td id="opt-${addressDto.key}-${transportOptionDto.key}" class="opts ${selectedText}" 
									onclick="event.preventDefault(); showSingleOption(${transportOptionDto.key}, ${addressDto.key}); ">
										${transportOptionDto.value} (${dataModel.getItemCount(addressDto.key, transportOptionDto.key)})
								</td>
							</c:forEach>
						</tr>
					</c:if>
				</c:forEach>
			</tbody>
		</table>
		<div class="clear">&nbsp;</div>
		<div class="text-center">
			<c:forEach var="sg" items="${stateGroups}">
				<span>
					${sg.displayName}
					<input type="checkbox" name="${sg.keyName}" onclick="highlightBox(this);"/>
				</span>
			</c:forEach>
		</div>				
		<table class="default2 viewGoodsOut" summary="">
			<thead>
				<tr>
					<th class="comp" scope="col"><spring:message code="assignschedule.lblcompany"/></th>
					<th class="subdiv" scope="col"><spring:message code="assignschedule.lblsubdiv"/></th>
					<th class="addr" scope="col"><spring:message code="assignschedule.lbladdress"/></th>
					<th class="jobno" scope="col"><spring:message code="creategendelnote.jobno"/></th>
					<th class="items" scope="col"><spring:message code="delsearchresults.items"/></th>
				</tr>
			</thead>
			<c:if test="${empty dataModel.sourceAddressSet}">
				<tbody>
					<tr>
						<td colspan="5"><spring:message code="viewgoodsout.noclientresultstext" /></td>
					</tr>
				</tbody>
			</c:if>
		</table>
		<c:forEach var="addressDto" items="${dataModel.sourceAddressSet}">
			<div id="divBusSub-${addressDto.key}">
				<c:forEach var="transportOptionDto" items="${dataModel.subdivTransportOptionSet}">
					<delivery:displayDespatchDtos dataModel="${dataModel}" sourceAddressDto="${addressDto}" transportOptionDto="${transportOptionDto}" stateGroups="${stateGroups}" />
				</c:forEach>
				<c:forEach var="transportOptionDto" items="${dataModel.getUnexpectedTransportOptions(addressDto.key)}">
					<delivery:displayDespatchDtos dataModel="${dataModel}" sourceAddressDto="${addressDto}" transportOptionDto="${transportOptionDto}" stateGroups="${stateGroups}" />
				</c:forEach>
			</div>
		</c:forEach>
	</div>
	<!-- end of div with nifty corners appended -->
	</jsp:body>
</t:crocodileTemplate>