<%-- File name: /trescal/core/deliverynote/delnotesummary.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<t:crocodileTemplate>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/deliverynote/DelNoteSummary.js'></script>
		<script type='text/javascript' src='script/thirdparty/jQuery/jquery.js'></script>
		<script type='module' src='script/components/cwms-upload-file/cwms-upload-file.js'></script>
		
		<script type='text/javascript'>
			<!-- begin js hide
			
			<c:forEach items="${form.delivery.items}" var="i">
				<c:if test="${i.despatched == true}">
					containsDespatchedItems = true;
				</c:if>
			</c:forEach>

			// end js hide -->
			var existingCDs = ${existingCDs};
		    var couriers = ${couriers};
			function updateInstructionSize(instructionSize) {
				$j("span.instructionSize").text(instructionSize);
			}
		</script>
		<script type="module" src="script/components/cwms-instructions/cwms-instructions.js"></script>
	</jsp:attribute>
	<jsp:attribute name="header"><span class="headtext"><spring:message code="delnotesummary.legend"/></span>
	</jsp:attribute>
	<jsp:attribute name="globalElements">
		<cwms-upload-file system-component-id="${sc.componentId}" entity-id1="${form.delivery.job.jobno}" entity-id2=""
 			files-name="${filesName}" rename-to="" web="false"></cwms-upload-file>
	</jsp:attribute>
	<jsp:body>		
		<!-- infobox contains delivery summary and is styled with nifty corners -->
		<div class="infobox">
			<fieldset>
				<legend><spring:message code="delnotesummary.legend" /></legend>
				
				<!-- displays content in column 49% wide (left) -->
				<div class="displaycolumn">
					<ol>
						<li>
							<label><spring:message code="assignschedule.lblcompany" />:</label>
							<span>
								<c:choose>
									<c:when test="${form.delivery.contact != null}">
										<links:companyLinkDWRInfo company="${form.delivery.contact.sub.comp}" rowcount="0" copy="true"/>
									</c:when>
									<c:otherwise>
										${form.delivery.freehandContact.company}
									</c:otherwise>
								</c:choose>
							</span>
						</li>
						<li>
							<label><spring:message code="assignschedule.lblsubdiv" />:</label>
							<span>
								<c:choose>
									<c:when test="${form.delivery.contact != null}">
										<links:subdivLinkDWRInfo rowcount="0" subdiv="${form.delivery.contact.sub}"/>
									</c:when>
									<c:otherwise>
										${form.delivery.freehandContact.subdiv}
									</c:otherwise>
								</c:choose>
							</span>
						</li>
						<li>
							<label><spring:message code="assignschedule.lblcontact" />:</label>
							<span>
								<c:choose>
									<c:when test="${form.delivery.contact != null}">
										<links:contactLinkDWRInfo contact="${form.delivery.contact}" rowcount="0"/>
									</c:when>
									<c:otherwise>
										${form.delivery.freehandContact.contact}
									</c:otherwise>
								</c:choose>
							</span>
						</li>
						<li>
							<label><spring:message code="assignschedule.lblteleph" />:</label>
							<span>
								<c:choose>
									<c:when test="${form.delivery.contact != null}">
										${form.delivery.contact.telephone} &nbsp;
										<c:if test="${form.delivery.contact.mobile != null && (not empty form.delivery.contact.mobile)}">
											(<spring:message code="mobile" />: ${form.delivery.contact.mobile})
										</c:if>
									</c:when>
									<c:otherwise>
										${form.delivery.freehandContact.telephone}
									</c:otherwise>
								</c:choose>
							</span>
						</li>
						<li>
							<label><spring:message code="delnotesummary.email" />:</label>
							<span>
								<c:if test="${form.delivery.contact != null}">
									<a href="mailto:${form.delivery.contact.email}" class="mainlink">${form.delivery.contact.email}</a>
								</c:if>
							</span>
						</li>
						<c:if test="${form.delivery.location != null and form.delivery.getType() ne 'INTERNAL'}">
							<li>
								<label><spring:message code="delnotesummary.delloc" />:</label>
								<span>${form.delivery.location.location}</span>
							</li>
						</c:if>
						<li>
							<label><spring:message code="delnotesummary.deladdr" />:</label>												
							<div class="float-left padtop">
								<c:choose>
									<c:when test="${form.delivery.address != null}">
										<div>${form.delivery.address.addr1}</div>
										<div>${form.delivery.address.addr2}</div>
										<div>${form.delivery.address.addr3}</div>
										<div>${form.delivery.address.town}</div>
										<div>${form.delivery.address.county}</div>
										<div>${form.delivery.address.postcode}</div>
										<div>${form.delivery.address.country.localizedName}</div>
									</c:when>
									<c:otherwise>
										${form.delivery.freehandContact.address.replace(",", "<br />")}
									</c:otherwise>
								</c:choose>
							</div>
							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
						</li>
					</ol>
				</div>
				<!-- end of left column -->
				
				<!-- displays content in column 49% wide (right) -->
				<div class="displaycolumn">
					<ol>											
						<li>
							<label><spring:message code="delnotesummary.delno" />:</label>
							<span>${form.delivery.deliveryno}</span>
							&nbsp;<img src="img/icons/bullet_clipboard.png" width="10" height="10" onclick=" $j(this).prev().clipBoard(this, null, false, null); " alt="<spring:message code='delnotesummary.delnumtoclip' />" title="<spring:message code='delnotesummary.delnumtoclip' />" />
						</li>
						<li>
							<label><spring:message code="creategendelnote.jobno" />:</label>
							<span><links:jobnoLinkDWRInfo rowcount="0" jobno="${form.delivery.job.jobno}" jobid="${form.delivery.job.jobid}" copy="true"/></span>
						</li>
						<li>
							<label><spring:message code="delnotesummary.deltype" />:</label>
							<span>${form.delivery.type.getDescription()}</span>
						</li>
						<li>
							<label><spring:message code="delnotesummary.deldate" />:</label>
							<span><fmt:formatDate value="${form.delivery.deliverydate}" type="date" dateStyle="medium" /></span>
						</li>
						<li>
							<label><spring:message code="delnotesummary.destinationReceiptDate" />:</label>
							<c:if test="${not empty form.delivery.destinationReceiptDate}">
								<fmt:formatDate value="${form.delivery.destinationReceiptDate}" type="both" dateStyle="medium" timeStyle="SHORT"/>
							</c:if>
						</li>
						<c:if test="${form.delivery.signatureCaptured}">
							<li>
								<label><spring:message code="delnotesummary.signby" />:</label>
								<span>${form.delivery.signeeName}&nbsp;&nbsp;(<fmt:formatDate value="${form.delivery.signedOn}" type="both" dateStyle="SHORT" timeStyle="SHORT" />)</span>
							</li>
						</c:if>
						<li>
							<label><spring:message code="createexcelexport" />:</label>
							<span>
								<a href="deliverynoteexcel.htm?deliveryid=${form.delivery.deliveryid}">
										<img src="img/doctypes/excel_small.gif" width="20" height="20"
											alt="Export To Excel" class="img-left">
								</a>
							</span>
						</li>
					<security:authorize access="hasAuthority('EXPORT_VDI_DELIVERY')">
						<li>
							<label><spring:message code="createvdiexport" text="VDI 2623 export" />:</label>
							<span>
								<a href="${pageContext.request.contextPath}/vdi/downloadVdi2326ForDelivery.zip?deliveryId=${form.delivery.deliveryid}">
										<img src="img/icons/vdi_logo.svg" width="20" height="20"
											alt="Export To VDI 2623" class="img-left">
								</a>
							</span>
						</li>
					</security:authorize>
					</ol>
				</div>
				<!-- end of right column -->
			</fieldset>
		</div>
		<!-- end of infobox div -->
		
		<!-- sub navigation menu which displays and hides sections of page as well as linking to external pages -->
		<div id="subnav">
			<dl>
				<dt>
					<a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'deliveryitems-tab', false); " 
						id="deliveryitems-link" title="<spring:message code='delnotesummary.title1' />" class="selected">
							<spring:message code="delnotesummary.delitems" /> (<span class="delitemCount">${form.delivery.items.size()}</span>)
					</a>
				</dt>
				<dt>
					<a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'editdelivery-tab', false); " 
						id="editdelivery-link" title="<spring:message code='delnotesummary.title2' />">
							<spring:message code="delnotesummary.edit" />
					</a>
				</dt>
				<c:if test="${ isReadyForDestinationReceipt }"> 
					<dt>
						<a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'editdestinationreceiptdate-tab', false); " 
							id="editdestinationreceiptdate-link" title="<spring:message code='delnotesummary.destinationReceiptDate' />">
								<spring:message code="delnotesummary.destinationReceiptDate" />
						</a>
					</dt>
				</c:if>
				<dt>
					<a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'despatch-tab', false); " 
						id="despatch-link" title="<spring:message code='delnotesummary.title3' />">
							<spring:message code="delnotesummary.disopt" />
					</a>
				</dt>
				<dt>
					<a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'files-tab', false); " 
						id="files-link" title="<spring:message code='delnotesummary.title4' />">
							<spring:message code="delnotesummary.delfil" /> (${scRootFiles.numberOfFiles})
					</a>
				</dt>
				<dt>
					<a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'emails-tab', false); " 
						id="emails-link" title="<spring:message code='delnotesummary.title5' />">
							<spring:message code="delnotesummary.emails" /> (<span class="entityEmailDisplaySize">${form.delivery.sentEmails.size()}</span>)
					</a>
				</dt>
			</dl>
		</div>
		<div id="subnav2">
			<dl>
				<dt>
					<a href="#" onclick=" event.preventDefault(); printDymoDeliveryLabel(${form.delivery.deliveryid}); " 
						title="<spring:message code='delnotesummary.title6' />"><spring:message code="delnotesummary.printlbl" />
					</a>
				</dt>
				<dt>
					<a href="#" onclick=" event.preventDefault(); deleteDeliveryNote('deletedelnote.htm?delid=${form.delivery.deliveryid}'); "
						title="<spring:message code='delnotesummary.title7' />"><spring:message code="delnotesummary.delete" />
					</a>
				</dt>
				<dt>
					<a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'generate-tab', false); " 
						id="generate-link" title="<spring:message code='delnotesummary.title8' />">
						<spring:message code="generate" />
					</a>
				</dt>
				<dt>
					<a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'instructions-tab', false); " id="instructions-link" title="<spring:message code="instructiontype.invoice" />">
						<spring:message code="instructions" />(<span class="instructionSize">${instructionsNumbers}</span>)
					</a>
				</dt>
			</dl>
		</div>
		<!-- end of sub navigation menu -->
		
		<!-- infobox div with nifty corners applied -->
		<div class="infobox">
			<!-- this section contains all delivery items currently added to this delivery -->
			<div id="deliveryitems-tab">
				<div id="deliveryitems">
					<table class="default4" summary="Table contains all delivery items added to this delivery">
						<thead>
							<tr>
								<td colspan="8">
									<spring:message code="delnotesummary.delitems" /> (<span class="delitemCount">${form.delivery.items.size()}</span>)
								</td>
							</tr>
							<tr>
								<th class="diitem" scope="col"><spring:message code="addgenitems.item" /></th>
								<th class="dijino" scope="col"><spring:message code="delnotesummary.jino" /></th>
								<th class="diunit" scope="col"><spring:message code="instmodelname" /></th>
								<th class="dinotes" scope="col">
									<a href="#" onclick=" toggleNoteDisplay(this, 'DELIVERYITEMNOTE', 'vis', null); return false;" class="toggleNoteDisplay">
										<img src="img/icons/note_expand.png" width="20" height="16" alt="<spring:message code='delnotesummary.imgnoteexp' />" title="<spring:message code='delnotesummary.imgnoteexp' />" />
									</a>												
								</th>
								<th class="diserial" scope="col"><spring:message code="additems.coladiserial" /></th>
								<th class="diplant" scope="col"><spring:message code="additems.coladiplant" /></th>
								<th class="diplant"><spring:message code="jicontractreview.jobitemclientref"/></th>
								<th class="didelete" scope="col"><spring:message code="delnotesummary.delitem" /></th>
							</tr>
						</thead>
						
						<tfoot>
							<tr>
								<td colspan="8">
									<c:set var="vis" value="vis" />
									<c:if test="${form.hasRemainingJobItems == false}">
										<c:set var="vis" value="hid" />
									</c:if>
								
									<span id="addDILink" class="${vis}">
										<a href="additems.htm?delid=${form.delivery.deliveryid}" class="mainlink"><spring:message code="delnotesummary.adddelit" /></a>
									</span>&nbsp;
								</td>
							</tr>
						</tfoot>
						
						<tbody>
							<c:choose>
								<c:when test="${form.delivery.items.size() > 0}">
									<c:forEach var="i" items="${form.delivery.items}" varStatus="rowcount">
										<c:set var="dinotecount" value="${i.publicActiveNoteCount + i.privateActiveNoteCount}" />

										<tr id="item${i.delitemid}">
											<td class="diitem">${rowcount.count}</td>
											<td class="dijino"><links:jobitemLinkDWRInfo jobitem="${i.jobitem}" jicount="false" rowcount="${rowcount.count}"/></td>
											<td class="diunit">
												<instmodel:showInstrumentModelLink instrument="${i.jobitem.inst}" caltypeid="${i.jobitem.calType.calTypeId}"/>
												<c:if test="${not empty i.accessories || not empty i.accessoryFreeText}">
													<br /><spring:message code="createdelnote.imgtitle" />:
													<span id="accfordi${i.jobitem.jobItemId}">
													<c:forEach var="ia" items="${i.accessories}" varStatus="loopCount">
														<c:if test="${!loopCount.first}">,</c:if>
														<t:showTranslationOrDefault translations="${ia.accessory.translations}" defaultLocale="${defaultlocale}"/> x ${ia.qty}
													</c:forEach>
													<c:if test="${i.accessoryFreeText != null}">
														<c:if test="${i.accessories.size() > 0}">,</c:if>
														${i.accessoryFreeText}
													</c:if>
													</span>
													<c:if test="${form.tp == true}">
														<a href="#" onclick=" event.preventDefault(); addAccessories(${i.delitemid}, ${i.jobitem.jobItemId}); " title="">
															<img src="img/icons/po_add_remove.png" width="16" height="16" alt="<spring:message code='delnotesummary.imgaddrem' />" title="<spring:message code='delnotesummary.imgaddrem' />" class="img_marg_bot" />
														</a>
													</c:if>
												</c:if>
											</td>
											<td class="dinotes"><links:showNotesLinks noteTypeId="${i.delitemid}" noteType="DELIVERYITEMNOTE" colspan="8" notecounter="${dinotecount}"/></td>
											<td class="diserial">${i.jobitem.inst.serialno}</td>
											<td class="diplant">${i.jobitem.inst.plantno}</td>
											<td class="">${i.jobitem.clientRef}</td>
											<td class="center">
												<a href="#" onclick=" event.preventDefault(); deleteItem(${form.delivery.deliveryid}, ${i.delitemid}, ${i.despatched}); "><img src="img/icons/delete.png" width="16" height="16" title="<spring:message code='delnotesummary.delitem' />" alt="<spring:message code='delnotesummary.delitem' />" /></a>
											</td>
										</tr>
										<c:if test="${(i.publicActiveNoteCount + i.privateActiveNoteCount) > 0}">
											<tr id="DELIVERYITEMNOTE${i.delitemid}" class="hid">
												<td colspan="8" class="nopadding">
													<t:showActiveNotes contact="${currentContact}" links="true" entity="${i}" calReqs="" privateOnlyNotes="false" noteTypeId="${i.delitemid}" noteType="DELIVERYITEMNOTE"/>
												</td>
											</tr>
										</c:if>
									</c:forEach>
								</c:when>							
								<c:otherwise>
									<tr  class="odd">
										<td colspan="8" class="center bold"><spring:message code="delnotesummary.colnodelit" /></td>
									</tr>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
				</div>
			</div>
			<!-- end of delivery item section -->
			
			<!-- this section contains form elements for editing delivery info (contact, address, deliverydate) -->
			<div id="editdelivery-tab" class="hid">
				<form:form action="editdelnote.htm" method="post" id="editdelnoteform" modelAttribute="form">
					<!-- error section displayed when form submission fails -->
				  <form:errors path="*">
                    <div class="warningBox1">
                        <div class="warningBox2">
                            <div class="warningBox3">
                                <h5 class="center-0 attention">
                                    <spring:message code='delnotesummary.error' />
                                </h5>
                                <c:forEach var="e" items="${messages}">
                                    <div class="center attention"><c:out value="${e}"/></div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                    <script type='text/javascript'> <c:if test="${not empty messages}"> LOADTAB = 'editdelivery-tab'; </c:if> </script>
                  </form:errors>
				  <!-- end error section -->
					<fieldset>
						<legend><spring:message code="delnotesummary.legend1" /></legend>
						<input type="hidden" name="delid" value="${form.delivery.deliveryid}" />	
						<ol>
							<c:if test="${form.delivery.contact != null}">
								<li>
									<label><spring:message code="assignschedule.lblcontact" />:</label>
									<form:select path="contactid" items="${form.delivery.contact.sub.contacts}" itemLabel="name" itemValue="personid" />
									<form:errors path="contactid" class="attention"/>
								</li>
							</c:if>
							<li>
								<label><spring:message code="assignschedule.lbladdress" />:</label>
								<c:choose>
						 			<c:when test="${form.delivery.location != null}"> 
						 				<c:set var="defLocation" value="${form.delivery.location.locationid}"/>
						 			</c:when>
						 			<c:otherwise> 
						 				<c:set var="defLocation" value="0"/> 
						 			</c:otherwise>
						 		</c:choose>
								<c:if test="${form.delivery.address != null}">
										<form:select path="addressid" onchange="addrChange(this.value,${defLocation}); return false;">
											<c:forEach var="a" items="${form.delivery.address.sub.addresses}">
												<option value="${a.addrid}" ${form.delivery.address.addrid == a.addrid ? "selected" : ""}>
													${a.addr1}, ${a.addr2}, ${a.addr3}, ${a.county}, ${a.postcode}, ${a.country.localizedName}
												</option>
											</c:forEach>
										</form:select>
										<form:errors path="addressid" class="attention"/>
								</c:if>
							</li>

							<li id="delLoc" <c:choose>
									<c:when test="${form.delivery.location != null}"> class="vis" </c:when>
									<c:otherwise> class="hid" </c:otherwise>
									</c:choose>>
								<label><spring:message code="delnotesummary.location" />:</label>
									<form:select path="locid" items="${form.delivery.location.add.locations}" itemLabel="location" itemValue="locationid" />
								<form:errors path="locid" class="attention" />
							</li>
							
							<li>
								<label><spring:message code="delnotesummary.deldate" /></label>
								<form:input type="date" id="newDeliveryDate" name="newDeliveryDate" path="delivery.deliverydate"/>
								<form:errors path="newDeliveryDate" class="attention"/>
							</li>
							
							<li>
								<label>&nbsp;</label>
								<input type="button" value="<spring:message code='delnotesummary.inputupdate' />" 
									onclick="SubmitEditDNform()"/>
							</li>											
						</ol>									
					</fieldset>																				
				</form:form>						
			</div>	
			<!-- end delivery editing section -->	
			
			<!-- this section contains form elements for editing destination receipt date -->
			<c:if test="${ isReadyForDestinationReceipt }">
			<div id="editdestinationreceiptdate-tab" class="hid">
				<form:form action="editreceiptdate.htm" method="post" id="editreceiptdateform" modelAttribute="form">
					<fieldset>
						<legend> <spring:message code="delnotesummary.destinationReceiptDate.title" /></legend>
						<input type="hidden" name="delid" value="${form.delivery.deliveryid}" />	
						<ol>
							<li>
								<label><spring:message code="delnotesummary.destinationReceiptDate" /> :</label>
								<c:choose>
									<c:when test="${ isReadyForDestinationReceipt }">
									<form:input type="datetime-local"  path="destinationReceiptDate"
									step="60" 
											max="${maxDestinationReceiptDate.toString()}"
											min="${minDestinationReceiptDate.toString()}"
											/>

										<form:errors path="destinationReceiptDate" class="attention"/>
									</c:when>
									<c:otherwise>
										<!-- Hidden empty value for destinationReceiptDate used in validation code -->
										<span class="error bold">
											<spring:message code="warning" /> -
										</span>
										<span class="error"> 
											<spring:message code="delnotesummary.clientreceiptdate.warning" />
										</span>
									</c:otherwise>
								</c:choose>
							</li>
							
							<c:if test="${ isReadyForDestinationReceipt }">
								<li>
									<label>&nbsp;</label>
									<input type="button" value="<spring:message code='delnotesummary.inputupdate' />" 
										onclick="SubmitEditReceiptDateform()"/>
								</li>
							</c:if>											
						</ol>									
					</fieldset>																				
				</form:form>						
			</div>	
			</c:if>
			<!-- end delivery destination receipt date update section -->
							
			<!-- this section contains delivery despatch options (courier or schedule) and confirmation table once selected -->
			<div id="despatch-tab" class="hid">
				<div id="despatchsummary">
					<c:choose>
						<c:when test="${nullCrDes == true}">
							<h5 class="center"><spring:message code="delnotesummary.string1" /></h5>

							<div class="displaycolumn">
								<form:form action="" method="post">
									<fieldset>
										<legend><spring:message code="delnotesummary.legend2" /></legend>
										<ol id="cdOptions">
											<li>
												<label><spring:message code="createdelnote.courdisp" />:</label>
												<select id="selectant" name="selectant" onchange=" event.preventDefault(); changeDespatchOption(this.value, '${form.currentDate.toString()}', ${form.delivery.organisation.comp.coid}); ">
													<option value="default" selected="selected"><spring:message code="delnotesummary.optcourdisp" /></option>
													<c:if test="${form.despatches.size() > 0}">
														<option value="exists"><spring:message code="createdelnote.excourdisp" /></option>
													</c:if>
													<option value="new"><spring:message code="createdelnote.newcourdisp" /></option>
												</select>
											</li>
											<li>
												<label>&nbsp;</label>
												<input type="submit" value="<spring:message code='delnotesummary.inputupdate' />" />
												<input type="hidden" name="action" id="formsubmitted" value="assigncrdes" />
											</li>
										</ol>
									</fieldset>
								</form:form>
							</div>

							<div class="displaycolumn">
								<fieldset>
									<legend><spring:message code="delnotesummary.legend3" /></legend>
									<ol>
										<li>
											<label><spring:message code="delnotesummary.lblscheddis" />:</label>
											<span>
												<cwms:securedLink permission="SCHEDULE_ASSIGN"  classAttr="mainlink"  parameter="?delid=${form.delivery.deliveryid}" collapse="True" >
													<spring:message code="delnotesummary.refassignschedlink"/>
												</cwms:securedLink>			
											</span>
										</li>
									</ol>
								</fieldset>
							</div>

							<div class="clear"></div>
						</c:when>
						<c:otherwise>
							<fieldset>
								<legend><spring:message code="delnotesummary.legend4" /></legend>
								<ol>
									<c:if test="${nullCrDes == false}">
										<c:choose>
											<c:when test="${form.delivery.crdes != null}">
												<li>	
													<label class="labelhead bold">
														<a href="viewcrdes.htm?cdid=${form.delivery.crdes.crdespid}" class="mainlink" title="<spring:message code='delnotesummary.title10' />"><spring:message code="delnotesummary.courdesass" /></a>
														<a href="removefromdespatch.htm?delid=${form.delivery.deliveryid}"><img src="img/icons/delete.png" width="16" height="16" class="img_vertmiddle_pad" alt="<spring:message code='delnotesummary.imgdelete' />" title="<spring:message code='delnotesummary.imgdelete' />" /></a>
													</label>
													<span>&nbsp;</span>
												</li>
												<li>	
													<label><spring:message code="delnotesummary.lbltrackdisment" />:</label>
													<c:choose>
														<c:when test="${form.delivery.crdes.consignmentno != null && form.delivery.crdes.consignmentno.trim().length() > 0 && form.delivery.crdes.cdtype.courier.webTrackURL.trim().length() > 0}">
															<a href="${form.delivery.crdes.cdtype.courier.webTrackURL}${form.delivery.crdes.consignmentno}" target="_blank">
																<img src="img/icons/lorry.png" height="16" width="16" title="<spring:message code='delnotesummary.imglorry' />" alt="<spring:message code='delnotesummary.imglorry' />" />
															</a>
														</c:when>
														<c:otherwise>
															<span><spring:message code="delnotesummary.tracknotavai" /></span>
														</c:otherwise>
													</c:choose>	
												</li>	
												<li>	
													<label><spring:message code="cdsearch.lbl1" />:</label>	
													<span>
														<c:choose>
															<c:when test="${form.delivery.crdes.consignmentno != null && form.delivery.crdes.consignmentno.trim().length() > 0}">
																${form.delivery.crdes.consignmentno} 
															</c:when>
															<c:otherwise>
																-- N/A -- 
															</c:otherwise>
														</c:choose>
													</span>
												</li>
												<li>	
													<label><spring:message code="delnotesummary.courtype" />:</label>	
													<span>${form.delivery.crdes.cdtype.courier.name}</span>	
												</li>	
												<li>	
													<label><spring:message code="cdsearch.lbl3" />:</label>	
													<span>${form.delivery.crdes.cdtype.description}</span>	
												</li>	
												<li>	
													<label><spring:message code="cdsearch.lbl4" />:</label>	
										<span>
										    <fmt:formatDate type="date" dateStyle="medium" value="${form.delivery.crdes.despatchDate}"/>
									</span>
												</li>
											</c:when>
											<c:otherwise>
												<c:forEach var="scheduledDelivery" items="${scheduledDeliveryList}">
													<li>
														<label class="labelhead bold">
															<a href="viewschedule.htm?schid=${scheduledDelivery.schedule.scheduleId}" class="mainlink" title="<spring:message code='delnotesummary.title11' />"><spring:message code="delnotesummary.scheddesass" /></a>
															<a href="removefromdespatch.htm?delid=${scheduledDelivery.delivery.deliveryid}"><img src="img/icons/delete.png" width="16" height="16" class="img_vertmiddle_pad" alt="<spring:message code='delnotesummary.title12' />" title="<spring:message code='delnotesummary.title12' />" /></a>
														</label>
														<span>&nbsp;</span>
													</li>
													<li>
														<label><spring:message code="delnotesummary.schedtype" />:</label>
														<span>${scheduledDelivery.schedule.type.type}</span>
													</li>
													<li>
														<label><spring:message code="delnotesummary.schedstat" />:</label>
														<span>${scheduledDelivery.schedule.status.status}</span>
													</li>
													<li>
														<label><spring:message code="delnotesummary.scheddate" />:</label>
														<span><fmt:formatDate value="${scheduledDelivery.schedule.scheduleDate}" dateStyle="medium" type="date"/></span>
													</li>
												</c:forEach>
											</c:otherwise>
										</c:choose>
									</c:if>											
								</ol>
							</fieldset>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
			<!-- end delivery despatch section -->						
		
			<!-- this section contains generation of delivery note document -->	
			<div id="generate-tab" class="hid">
				<spring:message code='delnotesummary.title8' var="generateDoc"/>
				<spring:message code="generate" var="generate" />
				<spring:message code='pageversion.old' var="versionOld"/>
				<spring:message code='pageversion.new' var="versionNew"/>
				<c:choose>
					<c:when test="${showNewDeliveryDocument}">
						<fieldset>
							<legend><spring:message code="tools.generatereport" /></legend>
							<ol>
								<li>
									<spring:message code="invoice.documentoptions" />:
									<input id="checkboxCertificate" type="checkbox" ${sdCertificates ? 'checked="checked"' : ''} />
									<spring:message code="viewjob.certs" />
									<input id="checkboxDateReceived" type="checkbox" ${sdDateReceived ? 'checked="checked"' : ''} />
									<spring:message code="docs.daterecieved" />
									<input id="checkboxPurchaseOrder" type="checkbox" ${sdPurchaseOrder ? 'checked="checked"' : ''} />
									<spring:message code="viewjob.purchord" />
									<input id="checkboxTotalPrice" type="checkbox" ${sdTotalPrice ? 'checked="checked"' : ''} />
									<spring:message code="totalcost" />
								</li>
								<li>
									<input name="submitNew" id="submitGenerateNew" type="submit" onclick=" event.preventDefault(); genDocPopup(${form.delivery.deliveryid}, getDocumentUrlWithOptions(), '${generateDoc}'); " value="${generate} - ${versionNew}" />
								</li>
								<li>
									<input name="submitOld" id="submitGenerateOld" type="submit" onclick=" event.preventDefault(); genDocPopup(${form.delivery.deliveryid}, 'birtdelnote.htm?delid=', '${generateDoc}'); " value="${generate} - ${versionOld}" />
								</li>
							</ol>
							</fieldset>
					</c:when>
					<c:otherwise>
						<input name="submit" id="submitGenerate" type="submit" onclick=" event.preventDefault(); genDocPopup(${form.delivery.deliveryid}, 'birtdelnote.htm?delid=', '${generateDoc}'); " value="${generate}" />
					</c:otherwise>
				</c:choose>
			</div>				
		
			<!-- this section contains delivery notes which have been added to this delivery -->	
			<div id="files-tab" class="hid">
				<files:showFilesForSC rootFiles="${scRootFiles}" id="${form.delivery.deliveryid }" allowEmail="true" entity="${form.delivery}" isEmailPlugin="false" ver="" identifier="${form.delivery.job.jobno}" deleteFiles="true" sc="${sc}" rootTitle="<spring:message code='delnotesummary.filesfordeliveries'/>${form.delivery.job.jobno}" />
			</div>
			<!-- end delivery notes section -->						
		
			<!-- this section contains delivery notes which have been added to this delivery -->	
			<div id="emails-tab" class="hid">
				<t:showEmails entity="${form.delivery}" entityId="${form.delivery.deliveryid}" entityName="<spring:message code='delivery'/>" sc="${sc}"/>
			</div>
			<!-- end delivery notes section -->
			
			<!-- this section contains delivery note company instructions -->	
			<div id="instructions-tab" class="hid">
				<cwms-instructions
						link-coid="${form.delivery.contact.sub.comp.coid}"
						link-subdivid="${form.delivery.contact.sub.subdivid}"
						link-contactid="${form.delivery.contact.personid}"
						jobitem-ids="${ jobItemIds }"
				  		link-jobid="${ (form.delivery.type eq 'THIRDPARTY' or form.delivery.type eq '') and not empty form.delivery.job ? form.delivery.job.jobid:'' }"
				  		instruction-types="PACKAGING,CARRIAGE"
						show-mode="RELATED"
				  		delivery-type="${ form.delivery.getType() }">
				</cwms-instructions>
			</div>
			<!-- end delivery note company instructions section -->							
		</div>
		<!-- end of infobox div with nifty corners applied -->
		
		<!-- this section contains delivery notes which have been added to this delivery -->INTERNAL
		<div class="infobox">
			<!-- this section contains all delivery notes -->
			<t:showTabbedNotes entity="${form.delivery}" noteType="DELIVERYNOTE" noteTypeId="${form.delivery.deliveryid}" privateOnlyNotes="${privateOnlyNotes}"/>
			<!-- end of delivery notes section -->
		</div>
		<!-- end delivery notes section -->
	</jsp:body>
</t:crocodileTemplate>
