<%-- File name: /trescal/core/deliverynote/cdsearch.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel" %>

<style>
   .widthLabel{
    width : 335px;
   }
</style>

<t:crocodileTemplate>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/deliverynote/CDSearch.js'></script>
	</jsp:attribute>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="cdsearch.headtext"/></span>
	</jsp:attribute>
	<jsp:body>
		<!-- infobox contains all search form elements and is styled with nifty corners -->
		<div class="infobox" id="cdsearch">
			<form:form modelAttribute="form" action="" method="post" id="cdsearchform">
				<fieldset>
					<legend><spring:message code="cdsearch.legend"/></legend>
						<ol>
							<li>
								<label><spring:message code="businesssubdivision" />:</label>
								<span><c:out value="${allocatedSubdiv.value}"/></span>
							</li>
							<li>
								<form:label path="consignmentno"><spring:message code="cdsearch.lbl1"/>:</form:label>
								<form:input path="consignmentno" tabindex="1" size="50"/>
							</li>
							<li>
								<form:label path="courierid"><spring:message code="cdsearch.lbl2"/>:</form:label>
								<form:select path="courierid" onchange=" changeTypes(this.value); return false; " tabindex="2"  class="widthLabel">
									<form:option value="0"> - <spring:message code="cdsearch.any"/> - </form:option>
									<form:options items="${form.couriers}" itemValue="courierid" itemLabel="name"/>
								</form:select>
							</li>
							<li>
								<form:label path="couriertypeid"><spring:message code="cdsearch.lbl3"/>:</form:label>
								<form:select path="couriertypeid" tabindex="3" class="widthLabel">
									<option value="0"> - <spring:message code="cdsearch.any"/> - </option>
								</form:select>
							</li>
							<li>
								<form:label path="date"><spring:message code="cdsearch.lbl4"/>:</form:label>
								<form:input path="date" type="date" tabindex="4" class="widthLabel"/>
							</li>
							<li>
								<label><spring:message code="assignschedule.lblcompany"/>:</label>
								<select id="coroleselect" onchange="changeCorole(this.value);" tabindex="5">
									<c:forEach var="corole" items="${form.coroles}">
										<option value="${corole}">${corole.message}</option>
									</c:forEach>
								</select>
								<div class="float-left extendPluginInput">
									<div class="compSearchJQPlugin">
										<input type="hidden" name="field" value="coid" />
										<input type="hidden" name="compCoroles" value="client" />
										<input type="hidden" name="tabIndex" value="6" />
										<!-- company results listed here -->
									</div>
								</div>										
								<!-- div clears the page flow -->
								<div class="clear"></div>
							</li>
							<li>
								<label>&nbsp;</label>
								<input type="submit" id="submit" name="submit" value="<spring:message code='assignschedule.inputsubmit'/>" tabindex="7"/>
							</li>
						</ol>
				</fieldset>
			</form:form>
		</div>
		<!-- end of infobox div -->
	</jsp:body>
</t:crocodileTemplate>