<%-- File name: /trescal/core/_PATH1_/_PATH2_/_FILE_.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="purchaseorder" tagdir="/WEB-INF/tags/purchaseorder" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="addgenitems.headtext"/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/deliverynote/AddGenItems.js'></script>
    </jsp:attribute>
    <jsp:body>
        <!-- infobox contains general delivery item options and is styled with nifty corners -->
						<div class="infobox">
							
							<form:form method="post" action="">
							
								<fieldset class="generaldelitems">
								
									<legend><spring:message code="addgenitems.legend"/>
									  <a href="<spring:url value='gendelnotesummary.htm?delid= ${form.delivery.deliveryid}'/>" class="mainlink"><c:out value=" ${form.delivery.deliveryno}" /></a>
									</legend>
									
									<ol>
									
										<li>
											<label><spring:message code="addgenitems.item"/> 1:</label>
											<div class="float-left width80 padtop">																							
												<textarea class="width80 presetComments GENERAL_DELIVERY_ITEM" rows="3" id="firstitem" name="itemlist0"></textarea>												
												<a href="#" onclick=" removeItem($j(this).parent().parent()); return false; " class="marg-left">
													<img src="img/icons/delete.png" height="16" width="16" alt="<spring:message code='addgenitems.imgremoveitem'/>"/></a>
											</div>
											<!-- clear floats and restore page flow -->
											<div class="clear"></div>					
										</li>
										<li>
											<label><spring:message code="addgenitems.item"/> 2:</label>
											<div class="float-left width80 padtop">
												<a href="#" onclick=" addItem($j(this).parent()); return false; "><img src="img/icons/add.png" height="16" width="16" alt="<spring:message code='addgenitems.imgadditem'/>"/></a>
											</div>
											<!-- clear floats and restore page flow -->
											<div class="clear"></div>
										</li>
										<li>
											<label>&nbsp;</label>
											<input type="submit" name="submit" id="submit" value="<spring:message code='addgenitems.submitadditems'/>"/>
										</li>
										
									</ol>
																	
								</fieldset>
								
							</form:form>
							
						</div>
							<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>