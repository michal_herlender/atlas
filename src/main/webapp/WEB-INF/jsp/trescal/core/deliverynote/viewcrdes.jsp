<%-- File name: /trescal/core/deliverynote/viewcrdes.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>

<t:crocodileTemplate>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/deliverynote/ViewCrDes.js'></script>
	</jsp:attribute>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="viewcrdes.headtext"/></span>
	</jsp:attribute>
	<jsp:body>
		<!-- get count of deliveries for this courier despatch -->
		<c:set var="itemcount" value="0"/>
		<c:forEach var="d" items="${crdes.deliveries}">
			<c:set var="itemcount" value="${itemcount + d.items.size()}"/>
		</c:forEach>
		<!-- infobox contains courier despatch information and is styled with nifty corners -->
		<div class="infobox">
			<fieldset>
				<legend><spring:message code="viewcrdes.courdespinfo"/></legend>
				<!-- displays content in column 49% wide (left) -->
				<div class="displaycolumn">
					<ol>
						<li>
							<label><spring:message code="cdsearch.lbl1"/>:</label>
							<span>
								<c:choose>
									<c:when test="${crdes.consignmentno == null || crdes.consignmentno.trim().length() == 0}">
										-- N/A --
									</c:when>
									<c:otherwise>
										${crdes.consignmentno}
									</c:otherwise>
								</c:choose>
							</span>
						</li>
						<li>
							<label><spring:message code="viewcrdes.courname"/>:</label>
							<span>${crdes.cdType.courier.name}</span>
						</li>
						<li>
							<label><spring:message code="cdsearch.lbl3"/>:</label>
							<span>${crdes.cdType.description}</span>
						</li>
						<li>
							<label><spring:message code="cdsearch.lbl4"/>:</label>
							<span>${crdes.despatchDate}</span>
						</li>
					</ol>
				</div>
				<!-- end of left column -->
				<!-- displays content in column 49% wide (right) -->
				<div class="displaycolumn">
					<ol>
						<li>
							<label><spring:message code="viewcrdes.creatby"/>:</label>
							<span>
								<c:choose>
									<c:when test="${crdes.createdBy == null}">
										&nbsp;-- <spring:message code="viewcrdes.unknown"/> --&nbsp;
									</c:when>
									<c:otherwise>
										${crdes.createdBy.firstName}&nbsp;${crdes.createdBy.lastName}
									</c:otherwise>
								</c:choose>
							</span>
						</li>
						<li>
							<label><spring:message code="viewcrdes.numofdel"/>:</label>
							<span>${crdes.deliveries.size()}</span>
						</li>
						<li>
							<label><spring:message code="viewcrdes.totitems"/>:</label>
							<span>${itemcount}</span>
						</li>
						<li>
							<label><spring:message code="delnotesummary.lbltrackdisment"/>:</label>
							<c:choose>
								<c:when test="${crdes.consignmentno == null || crdes.consignmentno.trim().length() == 0 || crdes.cdType.courier.webTrackURL.trim().length() == 0}">
									<span><spring:message code="delnotesummary.tracknotavai"/></span>
								</c:when>
								<c:otherwise>
									<a href="${crdes.cdType.courier.webTrackURL}${crdes.consignmentno}" target="_blank">
										<img src="img/icons/lorry.png" height="16" width="16" title="<spring:message code='delnotesummary.imglorry'/>" alt="<spring:message code='delnotesummary.imglorry'/>"/>
									</a>
								</c:otherwise>
							</c:choose>
						</li>
					</ol>
				</div>
				<!-- end of right column -->
			</fieldset>
		</div>
		<!-- end of infobox div -->
		<!-- sub navigation menu which displays and hides sections of page as well as linking to external pages -->
		<div id="subnav">
			<dl>
				<dt><a href="" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'deliveries-tab', false);" id="deliveries-link" title="<spring:message code='viewcrdes.dellinktitle'/>" class="selected"><spring:message code="cdsearchresults.coldel"/> (${crdes.deliveries.size()})</a></dt>
				<dt><a href="" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'editcrdes-tab', false);" id="editcrdes-link" title="<spring:message code='viewcrdes.editcourdes'/>"><spring:message code="viewcrdes.editcourdes"/></a></dt>
			</dl>
		</div>
		<!-- end of sub navigation menu -->
		<!-- infobox contains courier deliveries, edit section and is styled with nifty corners -->	
		<div class="infobox">
			<!-- this section displays all deliveries -->
			<div id="deliveries-tab">
				<h5><spring:message code="schedule.deliveries"/></h5>
				<table id="viewcrdespatch" class="default2" summary="This table displays all deliveries for this courier despatch">
					<thead>
						<tr>
							<td colspan="5"><spring:message code="cdsearchresults.coldel"/> (${crdes.deliveries.size()})</td>
						</tr>
						<tr>
							<th><spring:message code="delnotesummary.delno"/></th>
							<th><spring:message code="creategendelnote.jobno"/></th>
							<th><spring:message code="delsearchresults.items"/></th>
							<th><spring:message code="assignschedule.lblcompany"/></th>
							<th><spring:message code="assignschedule.lbltype"/></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="5">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody id="keynavResults">
						<c:choose>
							<c:when test="${crdes.deliveries.size() == 0}">
								<tr class="odd">
									<td colspan="5" class="bold center">
										<spring:message code="viewcrdes.nodisplaydel"/>
									</td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:forEach var="d" items="${crdes.deliveries}" varStatus="loop">
									<tr id="parent${loop.index}" class=
										"<c:choose>
											<c:when test='${loop.index == 0}'>
												hoverrow
											</c:when>
											<c:when test='${loop.index%2 == 0}'>
												even
											</c:when>
											<c:otherwise>
												odd
											</c:otherwise>
										</c:choose>"
									onclick="window.location.href='viewdelnote.htm?delid=${d.deliveryid}'"
									onmouseover="$j(this).addClass('hoverrow');"
									onmouseout="if(this.className != 'hoverrow') $j(this).removeClass('hoverrow');">
										<td>${d.deliveryno}</td>
										<td>
											<c:choose>
												<c:when test="${d.isGeneralDelivery()}">
													-- N/A --
												</c:when>
												<c:otherwise>
													<links:jobnoLinkDWRInfo jobid="${d.job.jobid}" jobno="${d.job.jobno}" copy="true" rowcount="${loop.index}"/>
												</c:otherwise>
											</c:choose>
										</td>
										<td>
											${d.items.size()}<a href="" id="itemsLink${loop.index}" onclick="event.preventDefault(); getDeliveryItems(event, ${loop.index}, ${d.deliveryid}, ${d.generalDelivery});">
												<img src="img/icons/items.png" width="16" height="16" alt="<spring:message code='delnotesummary.title1'/>" title="<spring:message code='delnotesummary.title1'/>" class="image_inline"/>
											</a>
										</td>
										<td><links:companyLinkDWRInfo company="${d.contact.sub.comp}" copy="true" rowcount="${loop.index}"/></td>
										<td>${d.type.getDescription()}</td>
									</tr>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
			</div>
			<!-- end of deliveries section -->
			<!-- this section displays form elements for editing the delivery -->
			<div id="editcrdes-tab" class="hid">
				<form:form modelAttribute="crdes" method="post" action="">
					<fieldset>
						<legend><spring:message code="viewcrdes.editcourdes"/></legend>
						<ol>
							<li>
								<form:label path="consignmentno"><spring:message code="cdsearch.lbl1"/>:</form:label>
								<form:input path="consignmentno"/>
								<span class="attention"><form:errors path="consignmentno"/></span>
							</li>
							<li>
								<form:label path="courierId"><spring:message code="cdsearch.lbl2"/>:</form:label>
								<select name="courierId" onchange="changeTypes(this.value); return false;">
									<c:forEach items="${couriers}" var="cr">
										<option value="${ cr.courierid }" ${ cr.courierid eq crdes.courierId ? 'selected':'' }>${ cr.name }</option>
									</c:forEach>
								</select>
							</li>
							<li>
								<form:label path="cdTypeId"><spring:message code="cdsearch.lbl3"/>:</form:label>
								<form:select path="cdTypeId" items="${typesforcurrentcourier}" itemValue="cdtid" itemLabel="description"/>
							</li>
							<li>
								<form:label path="despatchDate"><spring:message code="cdsearch.lbl4"/>:</form:label>
								<form:input  type="date" path="despatchDate"/>
								<span class="attention"><form:errors path="despatchDate"/></span>
							</li>
							<li>
								<label for="submit">&nbsp;</label>
								<input type="submit" id="submit" name="submit" type="" value="<spring:message code="assignschedule.inputsubmit" />" />
							</li>
						</ol>
					</fieldset>
				</form:form>
			</div>
			<!-- end of edit delivery section -->
		</div>
		<!-- end of infobox div -->
	</jsp:body>
</t:crocodileTemplate>