<%-- File name: /trescal/core/deliverynote/delnotesummary.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="gendelnotesummary.headtext" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/deliverynote/GenDelNoteSummary.js'></script>
        <script type='text/javascript' src='script/thirdparty/jQuery/jquery.js'></script>
		<script type='module' src='script/components/cwms-upload-file/cwms-upload-file.js'></script>
        <script type="text/javascript">
	        var existingCDs = ${existingCDs};
		    var couriers = ${couriers};
        </script>
        <c:forEach var="i" items="${form.delivery.items}">
        	<c:if test="${i.despatched}">
		        <script type='text/javascript'>
			        containsDespatchedItems = true;
		        </script>
        	</c:if>
        </c:forEach>
    </jsp:attribute>
    <jsp:attribute name="globalElements">
		<cwms-upload-file system-component-id="${sc.componentId}" entity-id1="${form.delivery.deliveryno}" entity-id2=""
 			files-name="${filesName}" rename-to="" web="false"></cwms-upload-file>
	</jsp:attribute>
    <jsp:body>

<!-- infobox contains delivery summary and is styled with nifty corners -->
<div class="infobox">

	<fieldset>
	
		<legend><spring:message code="gendelnotesummary.headtext" /></legend>
		
		<div class="displaycolumn">
		
			<ol>
				
				<li>
					<label><spring:message code="assignschedule.lblcompany" />:</label>
					<span>
						<c:choose>
							<c:when test="${form.delivery.contact != null}"> 
								<links:companyLinkDWRInfo company="${form.delivery.contact.sub.comp}" rowcount="0" copy="true"/>
							</c:when>
							<c:otherwise>
								${form.delivery.freehandContact.company}
							</c:otherwise>
						</c:choose>
					</span>
				</li>
				<li>
					<label><spring:message code="assignschedule.lblsubdiv" />:</label>
					<span>
						<c:choose>	
							<c:when test="${form.delivery.contact != null}">
								<links:subdivLinkDWRInfo rowcount="0" subdiv="${form.delivery.contact.sub}"/>
							</c:when>
							<c:otherwise>
							${form.delivery.freehandContact.subdiv}
							</c:otherwise>
						</c:choose>
					</span>
				</li>
				<li>
					<label><spring:message code="assignschedule.lblcontact" />:</label>
					<span>
						<c:choose>
							<c:when test="${form.delivery.contact != null}">
								<links:contactLinkDWRInfo contact="${form.delivery.contact}" rowcount="0"/>
							</c:when>
							<c:otherwise>
								${form.delivery.freehandContact.contact}
							</c:otherwise>
						</c:choose>
					</span>
				</li>
				<li>
					<label><spring:message code="assignschedule.lblteleph" />:</label>
					<span>
						<c:choose>
							<c:when test="${form.delivery.contact != null}">
								 ${form.delivery.contact.telephone} &nbsp;
								<c:if test="${form.delivery.contact.mobile.length() > 0	&& (not empty form.delivery.contact.mobile)}">
								</c:if>
							</c:when>
							<c:otherwise> 
								${form.delivery.freehandContact.telephone} 
							</c:otherwise>
						</c:choose>
					</span>
				</li>										
				<li>
					<label><spring:message code="delnotesummary.deladdr" />:</label>
					
					<div class="float-left padtop">
						<c:if test="${form.delivery.location != null}">
							<div>${form.delivery.location.location}</div>
						</c:if>
						<c:choose>
							<c:when test="${form.delivery.address != null}">
							<div>${form.delivery.address.addr1}</div>
							<div>${form.delivery.address.addr2}</div>
							<div>${form.delivery.address.addr3}</div>
							<div>${form.delivery.address.town}</div>
							<div>${form.delivery.address.county}</div>
							<div>${form.delivery.address.postcode}</div>
							<div>${form.delivery.address.country.localizedName}</div>
							</c:when>
							<c:otherwise>
								${form.delivery.freehandContact.address.replace(",", "<br />")}
							</c:otherwise>
						</c:choose>
					</div>
					<!-- clear floats and restore page flow -->
					<div class="clear"></div>
				</li>
															
			</ol>
		
		</div>
		
		<div class="displaycolumn">
		
			<ol>
				
				<li>
					<label><spring:message code="delnotesummary.delno" />:</label>
					<span>${form.delivery.deliveryno}</span>
				</li>											
				<li>
					<label><spring:message code="delnotesummary.deltype" />:</label>
					<span>${form.delivery.type.getDescription()}</span>
				</li>
				<li>
					<label><spring:message code="delnotesummary.deldate" />:</label>
					<span><fmt:formatDate value="${form.delivery.deliverydate}" type="date" dateStyle="MEDIUM" /></span>
				</li>
				<li>
					<label><spring:message code="creategendelnote.ourref" />:</label>
					<span>${form.delivery.ourref}</span>
				</li>
				<li>
					<label><spring:message code="creategendelnote.clientref" />:</label>
					<span>${form.delivery.clientref}</span>
				</li>
												
			</ol>
		
		</div>
		
	</fieldset>
							
</div>
<!-- end of infobox div -->

<!-- sub navigation menu which displays and hides sections of page as well as linking to external pages -->
<div id="subnav">
	<dl>
		<dt><a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'deliveryitems-tab', false); " id="deliveryitems-link" title="<spring:message code='delsearchresults.imgitemstitle' />" class="selected"><spring:message code="delnotesummary.delitems" /> (<span class="delitemCount">${form.delivery.items.size()}</span>)</a>
		<dt><a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'editdelivery-tab', false); " id="editdelivery-link" title="<spring:message code='delnotesummary.title2' />"><spring:message code="delnotesummary.edit" /></a></dt>
		<dt><a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'despatch-tab', false); " id="despatch-link" title="<spring:message code='delnotesummary.title3' />"><spring:message code="delnotesummary.disopt" /></a></dt>
		<dt><a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'files-tab', false); " id="files-link" title="<spring:message code='delnotesummary.title4' />"><spring:message code="delnotesummary.delfil" /> (${scRootFiles.numberOfFiles})</a></dt>
		<dt><a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'emails-tab', false); " id="emails-link" title="<spring:message code='delnotesummary.title5' />"><spring:message code="delnotesummary.emails" /> (<span class="entityEmailDisplaySize">${form.delivery.sentEmails.size()}</span>)</a></dt>
	</dl>
</div>
<div id="subnav2">
	<dl>
		<dt><a href="#" onclick=" event.preventDefault(); printDymoDeliveryLabel(${form.delivery.deliveryid}); " title="<spring:message code='delnotesummary.title6' />"><spring:message code='gendelnotesummary.printdellbl' /></a></dt>
		<dt><a href="#" onclick=" event.preventDefault(); deleteDeliveryNote('deletedelnote.htm?delid=${form.delivery.deliveryid}'); " title="<spring:message code='delnotesummary.title7' />"><spring:message code='delnotesummary.delete' /></a></dt>
		<dt><a href="#" onclick=" event.preventDefault(); genDocPopup(${form.delivery.deliveryid}, 'birtdelnote.htm?delid=', 'Generating General Delivery Note Document'); " title="<spring:message code='gendelnotesummary.gengendelnotdoc' />"><spring:message code='generate' /></a></dt>
	</dl>
</div>
<!-- end of sub navigation menu -->

<!-- infobox div with nifty corners applied -->
<div class="infobox">

	<!-- this section contains all delivery items currently added to this delivery -->
	<div id="deliveryitems-tab">								
																						
		<table class="default4 genDelItems" summary="<spring:message code='gendelnotesummary.tablegendelitems' />">
			
			<thead>
				<tr>
					<td colspan="4"><spring:message code='delnotesummary.delitems' /> (<span class="delitemCount">${form.delivery.items.size()}</span>)</td>
				</tr>
				<tr>
					<th class="diitem" scope="col"><spring:message code='addgenitems.item' /></th>
					<th class="diunit" scope="col"><spring:message code='gendelnotesummary.itemdesnot' /></th>
					<th class="dinotes" scope="col">
						<a href="#" onclick=" toggleNoteDisplay(this, 'DELIVERYITEMNOTE', 'vis', null); return false;" class="toggleNoteDisplay">
							<img src="img/icons/note_expand.png" width="20" height="16" alt="<spring:message code='delnotesummary.imgnoteexp' />" title="<spring:message code='delnotesummary.imgnoteexp' />"/>
						</a>												
					</th>
					<th class="didelete" scope="col"><spring:message code='delnotesummary.delitem' /></th>
				</tr>
			</thead>
			
			<tfoot>
				<tr>
					<td colspan="4"><span id="addDILink" class="vis"><a href="addgenitems.htm?delid=${form.delivery.deliveryid}" class="mainlink"><spring:message code='delnotesummary.adddelit'/></a></span>&nbsp;</td>
				</tr>
			</tfoot>
			
			<tbody>
				<c:choose>
					<c:when test="${form.delivery.items.size() > 0}">
					
					<c:set var="rowcount" value="0"/>
					<c:set var="itemcount" value="1"/>
					<c:forEach var="i" items="${form.delivery.items}">
					
						<c:set var="dinotecount" value="${i.publicActiveNoteCount + i.privateActiveNoteCount}" />													
						<tr id="item${i.delitemid}" valign="top">
							<td class="diitem">${itemcount}</td>
							<td class="diunit">
								${i.itemDescription}																											 
							</td>				
							<td class="dinotes">
							<links:showNotesLinks noteTypeId="${i.delitemid}" noteType="DELIVERYITEMNOTE" colspan="4" notecounter="${dinotecount}"/></td>
							
							<td class="didelete">
								<a href="#" onclick=" deleteItem(${form.delivery.deliveryid}, ${i.delitemid}, ${i.despatched}); return false; ">
									<img src="img/icons/delete.png" width="16" height="16" title="<spring:message code='delnotesummary.delitem' />" alt="<spring:message code='delnotesummary.delitem'/>" />
								</a>
							</td>
						</tr>
						
						<c:if test="${(i.publicActiveNoteCount + i.privateActiveNoteCount > 0)}">
							<tr id="DELIVERYITEMNOTE${i.delitemid}" class="hid">
								<td colspan="4" class="nopadding">
									<t:showActiveNotes contact="${currentContact}" links="true" entity="${i}" calReqs="" privateOnlyNotes="false" noteTypeId="${i.delitemid}" noteType="DELIVERYITEMNOTE"/>
								</td>
							</tr>
						</c:if>
						
						<c:set var="rowcount"  value="${rowcount + 1}"/>
						<c:set var="itemcount"  value="${itemcount + 1}"/>
					</c:forEach>
					</c:when>							
					<c:otherwise>
					<tr class="odd">
						<td colspan="4" class="center bold"><spring:message code='delnotesummary.colnodelit'/></td>
					</tr>
					</c:otherwise>
				</c:choose>
			</tbody>
		
		</table>															
										
	</div>
	<!-- end of delivery item section -->
	
	<!-- this section contains form elements for editing delivery info (contact, address, deliverydate) -->
	<div id="editdelivery-tab" class="hid">
																	
		<form:form action="" method="post" id="editdelnoteform" modelAttribute="form">
			<!-- error section displayed when form submission fails -->
				  <form:errors path="*">
                    <div class="warningBox1">
                        <div class="warningBox2">
                            <div class="warningBox3">
                                <h5 class="center-0 attention">
                                    <spring:message code='gendelnotesummary.error'/>
                                </h5>
                                <c:forEach var="e" items="${messages}">
                                    <div class="center attention"><c:out value="${e}"/></div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                    <script type='text/javascript'>
						<c:if test="${not empty messages}"> LOADTAB = 'editdelivery-tab'; </c:if>
					</script>
                  </form:errors>
		 	<!-- end error section -->
		 	
		
			<fieldset>
			
				<legend><spring:message code='delnotesummary.legend1'/></legend>
				
				<ol>
					<li>
						<label><spring:message code='assignschedule.lblcontact'/>:</label>
						<c:if test="${form.delivery.contact != null}">
							<form:select path="contactid">
								<c:forEach var="c" items="${form.delivery.contact.sub.contacts}">
									<option value="${c.personid}" <c:if test="${c.personid == form.delivery.contact.personid}"> selected="selected" </c:if>>${c.name}</option>
								</c:forEach>
							</form:select>
							<form:errors path="contactid" class="attention"/>
						</c:if>
					</li>
					<li>
						<label><spring:message code='creategendelnote.ourref'/></label>	
						<form:input path="delivery.ourref"/>
						<form:errors path="delivery.ourref" class="attention"/>
					</li>
					<li>
						<label><spring:message code='creategendelnote.clientref'/>:</label>	
						<form:input path="delivery.clientref"/>
						<form:errors path="delivery.clientref" class="attention"/>
					</li>						
					<li>
						<label><spring:message code='assignschedule.lbladdress'/>:</label>
						 <c:choose>
						 	<c:when test="${form.delivery.location != null}"> <c:set var="defLocation" value="${form.delivery.location.locationid}"/></c:when>
						 	<c:otherwise> <c:set var="defLocation" value="0"/> </c:otherwise>
						 </c:choose>
						<c:if test="${form.delivery.address != null}">
							<form:select path="addressid" onchange="addrChange(this.value,${defLocation}); return false;">
								<c:forEach var="a" items="${form.delivery.contact.sub.addresses}">
									<option value="${a.addrid}" <c:if test="${a.addrid == form.delivery.address.addrid}"> selected="selected" </c:if>>${a.addr1}, ${a.addr2}, ${a.addr3}, ${a.county}, ${a.postcode}, ${a.country.localizedName}</option>
								</c:forEach>
							</form:select>
							<form:errors path="addressid" class="attention"/>
						</c:if>
					</li>												
					<li id="delLoc" 
					<c:choose>
						<c:when test="${form.delivery.location != null}"> class="vis" </c:when>
						<c:otherwise> class="hid" </c:otherwise>
					</c:choose>>
						<label><spring:message code="delnotesummary.location" />:</label>
						<form:select path="locid">
							<c:if test="${form.delivery.location != null}">
								<c:forEach var="loc" items="${form.delivery.location.add.locations}">
									<option value="${loc.locationid}" <c:if test="${loc.locationid == form.delivery.location.locationid}"> selected="selected" </c:if>>${loc.location}</option>
								</c:forEach>
							</c:if>
						</form:select>
						<form:errors path="locid" class="attention"/>
					</li>
					<li>
						<label><spring:message code='createdelnote.delivdate'/></label>
						<form:input cssStyle="width:145px;" cssClass="date" type="date"
									path="delivery.deliverydate" /> 
						<form:errors path="delivery.deliverydate" class="attention"/>
					</li>
					<li>
						<label>&nbsp;</label>
						<input type="submit" id="submit" value="Update" />											
						<input type="hidden" name="action" id="formsubmitted" value="editdelnote" />
					</li>
					
				</ol>
			
			</fieldset>
														
		</form:form>
									
	</div>
	<!-- end delivery editing section -->
								
	<!-- this section contains delivery despatch options (courier or schedule) and confirmation table once selected -->
	<div id="despatch-tab" class="hid">
	
		<div id="despatchsummary">
			<c:choose>
				<c:when test="${nullCrDes == true}">
			
				<h5 class="center"><spring:message code='delnotesummary.string1'/></h5>
		
				<div class="displaycolumn">
				
					<form:form action="" method="post">
				
					<fieldset>
						
						<legend><spring:message code='delnotesummary.legend2'/></legend>
						
						<ol id="cdOptions">
							
							<li>
								<label><spring:message code='createdelnote.courdisp'/>:</label>
								<select id="selectant" name="selectant" onchange=" changeDespatchOption(this.value, '${form.currentDate.toString()}', ${form.delivery.organisation.comp.coid}); return false; ">
									<option value="default" selected="selected">
										<spring:message code="createdelnote.optcourdisp"/>
									</option>
									<c:if test="${form.despatches.size() > 0}">
										<option value="exists"><spring:message code='createdelnote.excourdisp'/></option>
									</c:if>
									<option value="new"><spring:message code='createdelnote.newcourdisp'/></option>
								</select>
							</li>
							<li>
								<label>&nbsp;</label>
								<input type="submit" value="<spring:message code='delnotesummary.inputupdate'/>" />
								<input type="hidden" name="action" id="formsubmitted" value="assigncrdes" />
							</li>
						</ol>
						
					</fieldset>
						
				 </form:form>
															
				</div>
				
				<div class="displaycolumn">
				
					<fieldset>
					
						<legend><spring:message code='delnotesummary.legend3'/></legend>
						
						<ol>
							
							<li>
								<label><spring:message code='delnotesummary.lblscheddis'/>:</label>
								<span>
									<cwms:securedLink permission="SCHEDULE_ASSIGN"  classAttr="mainlink"  parameter="?delid=${form.delivery.deliveryid}" collapse="True" >
										<spring:message code="delnotesummary.refassignschedlink"/>
									</cwms:securedLink>
								</span>
							</li>
						
						</ol>
						
					</fieldset>
				
				</div>
																										
				</c:when>
				<c:otherwise>
			
				<fieldset>
					
					<legend><spring:message code='delnotesummary.legend4'/></legend>
					
					<ol>
						<c:if test="${nullCrDes == false}">
							<c:choose>
								<c:when test="${form.delivery.crdes != null}">
								<li>
									<label class="labelhead bold">
										<a href="viewcrdes.htm?cdid=${form.delivery.crdes.crdespid}" class="mainlink" title="<spring:message code='delnotesummary.title10'/>"><spring:message code='delnotesummary.courdesass'/></a>
										<a href="removefromdespatch.htm?delid=${form.delivery.deliveryid}"><img src="img/icons/delete.png" width="16" height="16" class="img_vertmiddle_pad" alt="<spring:message code='delnotesummary.imgdelete'/>" title="<spring:message code='delnotesummary.imgdelete'/>" /></a>
									</label>
									<span>&nbsp;</span>
								</li>
								<li>
									<label><spring:message code='cdsearch.lbl1'/>:</label>
									<span>
										<c:choose>
											<c:when test="${not empty form.delivery.crdes.consignmentno && form.delivery.crdes.consignmentno.trim().length() > 0}">
												${form.delivery.crdes.consignmentno} 
											</c:when>
											<c:otherwise> -- N/A -- </c:otherwise>
								      </c:choose>	
									</span>
								</li>
								<li>
									<label><spring:message code='delnotesummary.courtype'/>:</label>
									<span>${form.delivery.crdes.cdtype.courier.name}</span>
								</li>
								<li>
									<label><spring:message code='cdsearch.lbl3'/>:</label>
									<span>${form.delivery.crdes.cdtype.description}</span>
								</li>
								<li>
									<label><spring:message code='cdsearch.lbl4'/>:</label>
										<span>
										    <fmt:formatDate type="date" dateStyle="medium" value="${form.delivery.crdes.despatchDate}"/>
									</span>
								</li>
								</c:when>
								<c:otherwise>
									<c:forEach var="scheduledDelivery" items="${scheduledDeliveryList}">
										<li>
											<label class="labelhead bold">
												<a href="viewschedule.htm?schid=${scheduledDelivery.schedule.scheduleId}" class="mainlink" title="<spring:message code='delnotesummary.title11' />"><spring:message code="delnotesummary.scheddesass" /></a>
												<a href="removefromdespatch.htm?delid=${scheduledDelivery.delivery.deliveryid}"><img src="img/icons/delete.png" width="16" height="16" class="img_vertmiddle_pad" alt="<spring:message code='delnotesummary.title12' />" title="<spring:message code='delnotesummary.title12' />" /></a>
											</label>
											<span>&nbsp;</span>
										</li>
										<li>
											<label><spring:message code="delnotesummary.schedtype" />:</label>
											<span>${scheduledDelivery.schedule.type.type}</span>
										</li>
										<li>
											<label><spring:message code="delnotesummary.schedstat" />:</label>
											<span>${scheduledDelivery.schedule.status.status}</span>
										</li>
										<li>
											<label><spring:message code="delnotesummary.scheddate" />:</label>
											<span><fmt:formatDate  value="${scheduledDelivery.schedule.scheduleDate}" dateStyle="medium" type="date"/></span>
										</li>
									</c:forEach>
							</c:otherwise>
							</c:choose>
						</c:if>												
					</ol>
				
				</fieldset>
												
				</c:otherwise>
			</c:choose>
			
			<!-- clear floats and restore page flow -->
			<div class="clear"></div>
			
		</div>
										
	</div>
	<!-- end of despatch options div -->
	
	<!-- this section contains delivery notes which have been added to this delivery -->	
	<div id="files-tab" class="hid">							

		<files:showFilesForSC rootFiles="${scRootFiles}" id="${form.delivery.deliveryid}" allowEmail="true" entity="${form.delivery}" isEmailPlugin="false" ver="" identifier="${form.delivery.deliveryno}" deleteFiles="true" sc="${sc}" rootTitle="<spring:message code='delnotesummary.filesfordeliveries'/>${form.delivery.deliveryno}" />

	</div>	
	<!-- end delivery notes section -->
	
	<!-- this section contains delivery notes which have been added to this delivery -->	
	<div id="emails-tab" class="hid">				
        <t:showEmails entity="${form.delivery}" entityId="${form.delivery.deliveryid}" entityName="<spring:message code='delivery'/>" sc="${sc}"/>
	</div>	
	<!-- end delivery notes section -->																																										
																							
</div>
<!-- end of infobox div with nifty corners applied -->

<!-- this section contains all delivery notes -->	
		<t:showTabbedNotes entity="${form.delivery}" noteType="DELIVERYNOTE" noteTypeId="${form.delivery.deliveryid}" privateOnlyNotes="${privateOnlyNotes}"/>
<!-- end of delivery notes section -->	
    </jsp:body>
</t:crocodileTemplate>