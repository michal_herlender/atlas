<%-- File name: /trescal/core/deliverynote/assignschedule.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="assignschedule.headtext" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/deliverynote/AssignSchedule.js'></script>
    </jsp:attribute>
    <jsp:body>
        <!-- sub navigation menu which displays and hides sections of page -->
		<div id="subnav">
			<dl>
				<dt><a href="#" onclick="switchMenuFocus(menuElements, 'existingsched-tab', false); return false; " id="existingsched-link" title="<spring:message code='assignschedule.existingschedtitle' />" class="selected"><spring:message code="assignschedule.existsched" /></a>
				<dt><a href="#" onclick="switchMenuFocus(menuElements, 'newsched-tab', false); return false;" id="newsched-link" title="<spring:message code='assignschedule.newschedtitle' />"><spring:message code="assignschedule.newsched" /></a></dt>
			</dl>
		</div>
		<!-- end of sub navigation menu -->
		<!-- this section contains form elements for choosing an existing schedule -->
		<div id="existingsched-tab" class="<c:if test="${newschedtab}">hid</c:if>">
			
			<!-- div with nifty corners appended -->
			<div class="infobox">
			
				<form:form method="post" action="" modelAttribute="form">
						
					<c:choose>
						<c:when test="${form.validSchedules.size() > 0}">
						
						<fieldset>
				
							<legend><spring:message code="assignschedule.legend" /> <a href="viewdelnote.htm?delid=${form.delivery.deliveryid}" class="mainlink">${form.delivery.deliveryno}</a> <spring:message code="assignschedule.legend2" /></legend>
					
							<ol>
							
								<li>
									<label><spring:message code="assignschedule.lblexistSched" />:</label>
									<form:select path="schId">
										<c:forEach var="s" items="${form.validSchedules}">
											<option value="${s.scheduleId}">${s.type.getType()} - <fmt:formatDate value="${s.scheduleDate}" type="date" dateStyle="medium"/> - ${s.contact.name}</option>
										</c:forEach>
									</form:select>
								</li>
								<li>
									<label>&nbsp;</label>
									<c:if test="${form.alreadyAssigned}">
										<c:set var="disabled" value="hid"/>
										<span class="attention-pad">
											<a href="viewschedule.htm?schid=${form.schId}">
												&nbsp;<img src="img/icons/information.png" width="16px;" height="16px;" alt="<spring:message code='schedule.vieweditthisschedule'/>" title="<spring:message code='schedule.vieweditthisschedule'/>" />
											</a>
											<spring:message code='assignschedule.alreadyassign'/>
										</span>
									</c:if>
									<input type="hidden" name="assignoption" value="<spring:message code='assignschedule.inputhidden' />" />
									<input type="submit" id="submit" name="submit" value="<spring:message code='assignschedule.inputsubmit' />" class="${disabled}"/>
								</li>
								
							</ol>
							
						</fieldset>
						
						</c:when>
						<c:otherwise>
					
						<fieldset>
						
							<legend><spring:message code="assignschedule.legend3" /> <a href="viewdelnote.htm?delid=${form.delivery.deliveryid}" class="mainlink">&nbsp;<c:out value="${form.delivery.deliveryno}"/></a></legend>
							
							<ol>
							
								<li>
									<label><spring:message code="assignschedule.lblcompany" />:</label>
									<span>
										<c:choose>
											<c:when test="${form.delivery.contact != null}">
												${form.delivery.contact.sub.comp.coname} 
											</c:when>
											<c:otherwise>
												${form.delivery.freehandContact.company}
											</c:otherwise>
										</c:choose>
									</span>
								</li>
								<li>
									<label><spring:message code="assignschedule.lbladdress" />:</label>
									<c:choose>
										<c:when test="${form.delivery.address != null}">
								
										<c:set var="a" value="${form.delivery.address}"/>
										<div class="float-left" style=" padding-top: 4px; ">
											<div>${a.addr1}</div>
											<div>${a.addr2}</div>
											<div>${a.addr3}</div>
											<div>${a.town}</div>
											<div>${a.county}</div>
											<div>${a.postcode}</div>
											<div>${a.country.localizedName}</div>
										</div>
										<div class="clear"></div>
										</c:when>
										<c:otherwise>
										<div class="float-left" style=" padding-top: 4px; ">
											<div>${form.delivery.freehandContact.address}</div>
										</div>
										<div class="clear"></div>
										</c:otherwise>
								</c:choose>
								</li>
								
							</ol>
							
						</fieldset>
						
						</c:otherwise>
					</c:choose>
				
				</form:form>
		
			</div>
			<!-- end of div with nifty corners appended -->
		
		</div>
		<!-- end of existing schedule section -->
		
		<!-- this section contains form elements for creating a new schedule -->
		<div id="newsched-tab" class="<c:if test="${not newschedtab}">hid</c:if>">
			
			<!-- div with nifty corners appended -->
			<div class="infobox">
				<c:if test="${noActiveDOW}">
					<div class="warningBox1">
						<div class="warningBox2">
							<div class="warningBox3">
								<h5 class="center-0 attention"><spring:message code="schedule.no.active.dayofweek"/></h5>
							</div>
						</div>
					</div>
				</c:if>
				<form:form method="post" action="" modelAttribute="form">
					<form:errors path="*">
                    	<div class="warningBox1">
                        	<div class="warningBox2">
                            	<div class="warningBox3">
                                	<h5 class="center-0 attention">
                                    	<spring:message code='schedule.errorcreateschedule' />
                                	</h5>
                                	 <c:if test="${ not empty scheduleExists}">
                    					<div class="center attention">
                    						<spring:message code="schedule.error.duplicateschedule" arguments="${scheduleExists}"/>
                    					</div>
                    				</c:if>
                            	</div>
                        	</div>
                    	</div>
                  	</form:errors>
				<fieldset>
					
					<legend><spring:message code="assignschedule.legend4" />&nbsp;<a href="viewdelnote.htm?delid=${form.delivery.deliveryid}" class="mainlink">&nbsp;${form.delivery.deliveryno}</a> <spring:message code="assignschedule.legend5" /></legend>
					
					<ol>
						<li>
							<label><spring:message code="assignschedule.lbltype" />:</label>
							<form:select path="type">
								<c:forEach var="st" items="${form.scheduleTypes}">
									<option value="${st}" <c:if test="${st.name() == 'DELIVERY'}"> selected </c:if>>${st.type}</option>
								</c:forEach>
							</form:select>
						</li>
						<li>
							<label><spring:message code="assignschedule.lblcontact" />:</label>
							<form:select path="personid">
								<c:forEach var="c" items="${form.delivery.contact.sub.contacts}">
									<option value="${c.personid}" <c:if test="${form.delivery.contact.personid == c.personid}"> selected </c:if>>${c.name}</option>
								</c:forEach>
							</form:select>
						</li>																			
						<li>
							<label><spring:message code="assignschedule.lbladdress" />:</label>
							<form:select path="addrid" onchange="getScheduleDetails(${form.delivery.organisation.subdivid}); return false;">
								<c:forEach var="a" items="${form.delivery.address.sub.addresses}">
									<option value="${a.addrid}" <c:if test="${form.delivery.address.addrid == a.addrid}"> selected </c:if>>
										${a.addr1},
										${a.addr2},
										${a.addr3},
										${a.town},
										${a.county},
										${a.postcode},
										${a.country.localizedName}
									</option>
								</c:forEach>
							</form:select>
						</li>
						<li class="<c:if test="${empty scheduleDto.transportOptionOutName}">hid</c:if>" id="transportOptionIn">
							<label><spring:message code="viewjob.transpin"/>:</label>
							<span id="transportOptionInName">${scheduleDto.transportOptionInName}</span>
						</li>
						<li class="<c:if test="${empty scheduleDto.transportOptionInName}">hid</c:if>" id="transportOptionOut">
							<label><spring:message code="viewjob.transpout"/>:</label>
							<span id="transportOptionOutName">${scheduleDto.transportOptionOutName}</span>
						</li>
						<li>
							<label><spring:message code="company.transportoption"/>*:</label>
							<form:select path="transportOptionId">
								<option value=""><spring:message code="companyedit.na"/></option>
								<c:forEach var="option" items="${transportOptionsSchedulable}">
									
									<option value="${option.id}" <c:if test="${scheduleDto.transportOptionInId == scheduleDto.transportOptionOutId && scheduleDto.transportOptionInId == option.id}">selected</c:if>> 
										<cwms:besttranslation translations="${option.method.methodTranslation}" />
										<c:if test="${not empty option.localizedName}">&nbsp;${option.localizedName} </c:if>
								</c:forEach>
							</form:select>
							<form:errors path="transportOptionId" class="attention"/>
							<form:hidden path="schedulesCreated"/>
						</li>
						<li>
							<label><spring:message code="assignschedule.lblstatus" />:</label>
							<form:select path="status">
								<c:forEach var="ss" items="${form.scheduleStatuses}">
									<option value="${ss}" <c:if test="${ss.name() == 'AGREED'}"> selected </c:if>>${ss.status}</option>
								</c:forEach>
							</form:select>
						</li>
						<li>
							<label><spring:message code="date" />:</label>
							<form:radiobutton path="manualEntryDate" value="false" checked="true" onClick="hideDateEntry();"/>
								<spring:message code="schedule.nextavailable"/>
							<form:radiobutton path="manualEntryDate" id="entryDate" value="true" onClick="showDateEntry();"/>
								<spring:message code="schedule.manual"/>
							<br>
							<form:input class="hid" id="scheduleDate" path="scheduleDate" value="${form.currentDate}" type="date" autocomplete="off" />
							<form:errors path="scheduleDate" class="attention"/>
						</li>
						<li>
							<label><spring:message code="assignschedule.lblnotes" />:</label>
							<div class="float-left editor_box">
								<form:textarea path="note" class="editor" style="height: 300px; width: 800px;"></form:textarea>
							</div>
							<!-- clear floats and restore page flow -->
							<div class="clear-0"></div>
							<form:errors path="note" class="attention"/>
						</li>
						<li>
							<label>&nbsp;</label>
							<input type="hidden" name="assignoption" value="<spring:message code='assignschedule.inputnew' />">
							<input type="submit" id="submit" name="submit" value="<spring:message code='assignschedule.inputsubmit' />" <c:if test="${noActiveDOW}"> disabled</c:if>>
						</li>
					
					</ol>
					
				</fieldset>
				
				</form:form>
			
			</div>
			<!-- end of div with nifty corners appended -->
			
		</div>
		<!-- end of new schedule section -->							

    </jsp:body>
</t:crocodileTemplate>