<%-- File name: /trescal/core/deliverynote/creategendelnote.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="creategendelnote.headtext"/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/deliverynote/CreateGenDelNote.js'></script>
        
        <script type='text/javascript'>
			    var existingCDs = ${existingCDs};
			    var couriers = ${couriers};
		</script>
    </jsp:attribute>
    <jsp:body>
		<t:showErrors path="delnoteform.*"/>
	
		<!-- infobox containing form elements for creating a new general delivery note with nifty corners applied -->
		<div class="infobox" id="creategendelnote">
		
			<form:form method="post" action="" modelAttribute="delnoteform">
			
			<fieldset>
			
				<legend><spring:message code="creategendelnote.legend"/></legend>
				
				<ol>
				
					<li>
						<label><spring:message code="creategendelnote.jobno"/>:</label>
						<span>-- <spring:message code="creategendelnote.nojob"/> --</span>
					</li>
					<li>
						<label><spring:message code="fromsubdivision"/>:</label>
						${allocatedCompany.value} - ${allocatedSubdiv.value} 
					</li>
					<li>
						<label>
						<spring:message code="assignschedule.lblcompany"/>:</label>
						<span><c:if test="${not empty delnoteform.contact}">${delnoteform.contact.sub.comp.coname}</c:if></span>
					</li>
					<li>
						<label><spring:message code="assignschedule.lblcontact"/>:</label>
						<form:select path="contactid">
							<c:forEach var="c" items="${clientContacts}">
								<option <c:if test="${c.key == delnoteform.contactid }">selected="selected"</c:if>  value="${c.key}">
							 			${c.value}
						 		</option>
							</c:forEach>
							<form:errors path="contactid" class="attention"/>
						</form:select>
					</li>
					<li>
						<label><spring:message code="assignschedule.lbladdress"/>:</label>
						<form:select path="addressid" onchange="addrChange(this.value, 0);">
							<c:forEach var="a" items="${activeDeliveryAddresses}">
								<option <c:if test="${a.addrid == delnoteform.addressid }">selected="selected"</c:if>  value="${a.addrid}">
							 		${a.addr1}, ${a.addr2}, ${a.addr3}, ${a.county}, ${a.postcode}, ${a.country.localizedName}
						 		</option>
							</c:forEach>
						</form:select>
						<form:errors path="addressid" class="attention" />
					</li>
						<li id="delLoc"  <c:choose><c:when test="${clientLocations.size() > 0}"> class="vis" 
													</c:when><c:otherwise> class="hid" </c:otherwise></c:choose>
						>
							<label><spring:message code="createdelnote.selloc"/>:</label>
							<form:select path="locid">
								<c:forEach var="loc" items="${clientLocations}">
									<option <c:if test="${loc.locationid == delnoteform.job.returnToLoc.locationid }">selected="selected"</c:if>  value="${loc.locationid}">
									 		${loc.location}
								 	</option>
								</c:forEach>
							</form:select>
							<form:errors path="locid" class="attention" />
						</li>
					<li>
						<label><spring:message code="creategendelnote.ourref"/>:</label>
						<form:input path="ourref"/>
						<form:errors path="ourref" class="attention" />
					</li>
					<li>
						<label><spring:message code="creategendelnote.clientref"/>:</label>
						<form:input path="clientref"/>
						<form:errors path="clientref" class="attention" />
					</li>
					<li>
						<label><spring:message code="createdelnote.delivdate"/>:</label>
						<form:input path="deliverydate"  type="date"/>
						<form:errors path="deliverydate" class="attention" />
					</li>
					<li>
						<label><spring:message code="createdelnote.delivinstr"/>:</label>
							<div class="float-left editor_box">
								<!-- style="height: 300; width: 800;"  -->
								<form:textarea path="delInstruction" class="editor" style="height: 300px; width: 800px;"></form:textarea>
								<form:errors path="delInstruction" class="attention" />
							</div>
							<!-- clear floats and restore page flow -->
							<div class="clear-0"></div>
							${status.errorMessage}
					</li>
					
				</ol>
				<ol id="cdOptions">
				
					<li>
						<label><spring:message code="createdelnote.courdisp"/>:</label>
						<select id="selectant" name="selectant" onchange=" changeDespatchOption(this.value, '${delnoteform.deliverydate}', ${allocatedCompany.key}); return false; ">
							<option value="default" selected="selected"><spring:message code="delnotesummary.optcourdisp"/></option>
								<c:if test="${delnoteform.despatches.size() > 0}">
									<option value="exists"><spring:message code="createdelnote.excourdisp"/></option>
								</c:if>
							<option value="new"><spring:message code="createdelnote.newcourdisp"/></option>
						</select>
					</li>
					<li>
						<label>&nbsp;</label>
						<input type="submit" name="submit" id="submit" value="<spring:message code="submit"/>" />
					</li>
					
				</ol>
			
		</fieldset>	
		
	</form:form>
		
  </div>
	<!-- end infobox div -->
  </jsp:body>
</t:crocodileTemplate>