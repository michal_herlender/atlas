<%-- File name: /trescal/core/misc/addfeedback.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:crocodileTemplate>
    <jsp:attribute name="header">
    	<span class="headtext">Feedback</span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
       <script type='text/javascript' src='script/trescal/core/misc/AddFeedback.js'></script>
    </jsp:attribute>
    <jsp:body>
		<c:if test="${command.feedbackLeft == true}">
			<div class="successBox1">
				<div class="successBox2">
					<div class="successBox3">
						Your feedback has been submitted and distributed to the development team.
						<a href="${url}">Go back</a>
					</div>
				</div>
			</div>
		</c:if>
							
		<div class="infobox">
		
			<form:form action="" method="post" id="feedbackform" modelAttribute="command">
		
				<fieldset>
				
					<legend>Submit Feedback</legend>
				
				 	<ol>
						
						<li>
							<label>Title:</label>
							<form:input path="title"/>
						</li>
						<li>
							<label>The url (web address):</label>
							<form:input path="url"/>
						</li>
						<li>
							<label>Detailed Description:</label>
							<form:textarea path="description" rows="20"></form:textarea>
						</li>
						<li>
							<label>&nbsp;</label>
							<input type="submit" name="Send Feedback" id="submit" value="Send Feedback" />
						</li>
						
					</ol>
											
				</fieldset>
			
			</form:form>
		
		</div>
    </jsp:body>
</t:crocodileTemplate>