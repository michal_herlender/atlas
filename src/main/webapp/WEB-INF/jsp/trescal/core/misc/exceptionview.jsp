<%-- File name: /trescal/core/misc/exceptionview.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext">View Exceptions</span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/tools/ExceptionView.js'></script>
		<script type='text/javascript' src='dwr/engine.js'></script>
		<script type='text/javascript' src='dwr/util.js'></script>
		<script type='text/javascript' src='dwr/interface/exceptionservice.js'></script>
    </jsp:attribute>
    <jsp:body>
        <table class="default4">
			<thead>
				<tr>
					<th align="left">Date</th>
					<th align="left">URL</th>
					<th align="left">User</th>
					<th align="left">Exception</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="exception" items="${exceptions}">
					<tr id="${exception.id}row" <c:if test="${exception.reviewed == false}"> bgcolor="#FFBA44" </c:if>>
					                          
						<td>$!{exception.date}</td>
						<td>$!{exception.url}</td>
						<td>$!{exception.user}</td>
						<td>$!{exception.exception}</td>
					</tr>
					<tr>
						<td colspan="4">
							<a id="${exception.id}link" href="#${exception.id}" onclick="showStackTrace('${exception.id}')">show details</a>
							<span id="${exception.id}" class="hid">
 							<textarea id="${exception.id}trace" style="font-family: courier; width: 100%; height:250px; font-size:0.7em;" >$!{exception.stackTrace}</textarea>
	 							<a href="#${exception.id}" onclick="copyText('${exception.id}trace')"> copy to clipboard</a>
 							<br/>
 							<textarea id="${exception.id}comment" style="font-family: courier; width: 100%; height:100px; font-size:0.7em;">$!{exception.comment}</textarea>
 							<br/>
 							reviewed?<input type="checkbox" id="${exception.id}reviewed" <c:if test="${exception.reviewed == true}">) checked="checked" </c:if>>
 							
 							<input type="button" value="update" onclick="exceptionAjax(${exception.id})"/>

							</span>
						</td>
					</tr>
					
				</c:forEach>
			</tbody>
		</table>
    </jsp:body>
</t:crocodileTemplate>