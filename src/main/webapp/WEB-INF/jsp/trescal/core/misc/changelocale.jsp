<%-- File name: /trescal/core/misc/changelocale.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
	        <span class="headtext"><spring:message
				code='changelocale.changelanguage' /></span>
	     </jsp:attribute>
	<jsp:body>
        <div class="infobox">
			<form:form action="" method="post" id="localeform" modelAttribute="command">
				<form:hidden path="refererUrl" />
		 		<fieldset>
					<legend>
								<spring:message code='changelocale.changelanguage' />
							</legend>
				 	<ol>
						<li>
							<label><spring:message code='changelocale.currentlanguage' />:</label>
		                    <span style="text-transform: capitalize"> ${command.desiredLocale.getDisplayLanguage(pageContext.request.locale)}</span>
					  		${command.desiredLocale.getDisplayCountry(pageContext.request.locale)}
						</li>
						<li>
							<label><spring:message code='changelocale.desiredlanguage' />:</label>
							<span style="text-transform: capitalize">
								<form:select path="desiredLocale">
									<c:forEach var="supportedLocale" items="${supportedLocales}">
										<option value="${supportedLocale}" <c:if test="${supportedLocale == command.desiredLocale}"> selected="selected" </c:if>>
		    							${supportedLocale.getDisplayLanguage(pageContext.request.locale)}  (${supportedLocale.getDisplayCountry(pageContext.request.locale)})
		    							</option>
									</c:forEach>
								</form:select>
							</span>
						</li>
					</ol>							
				</fieldset>
				<div class="submit">
					<input type="submit" id="submit"
						value="<spring:message code='changelocale.changelanguage'/>" />
				</div>
			</form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>