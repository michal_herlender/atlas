<%-- File name: /trescal/core/hire/createhireenquiry.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="hireenquiry.title" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/hire/CreateHireEnquiry.js'></script>
    </jsp:attribute>
    <jsp:body>
		<c:set var="error" value="false" />
		
		<spring:bind path="form.*"> 
		<c:if test="${status.error}">
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<h5 class="center-0 attention"><spring:message code="hireenquiry.errorcreating" /></h5>
						<div class="center attention">${status.errorMessage}</div>
					</div>
				</div>
			</div>
			<c:set var="error" value="true" />
		</c:if>
		<c:set var="freehandError" value="false" />
		</spring:bind>

		<c:set var="errorClass" value="hid"/>
		<c:if test="${error == true}"><c:set var="errorClass" value="vis"/></c:if>

		
		<!-- main div which contains all create hire enquiry form elements and has nifty corners applied -->
		<div class="infobox">
		
			<table class="default4 createHireEnquiryInsts" summary="<spring:message code="hireenquiry.tableinstrumentsource" />">
				<thead>
					<tr>
						<td colspan="8"><spring:message code="hireenquiry.hireinstruments" /> (${form.hire.items.size()})</td>
					</tr>
					<tr>
						<th class="item" scope="col"><spring:message code="hirehome.item" /></th>
						<th class="barcode" scope="col"><spring:message code="barcode" /></th>
						<th class="desc" scope="col"><spring:message code="description" /></th>
						<th class="serialno" scope="col"><spring:message code="serialno" /></th>
						<th class="plantno" scope="col"><spring:message code="plantno" /></th>
						<th class="caltype" scope="col"><spring:message code="hirehome.caltype" /></th>
						<th class="ukascal" scope="col"><spring:message code="hiresearchinstr.ukascal" /></th>
						<th class="hirecost" scope="col"><spring:message code="hireenquiry.hirecostperday" /></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="8">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
					<c:choose>
						<c:when test="${form.hire.items.size() > 0}">								
							<c:forEach var="hi" items="${form.hire.items}">
								<tr>
									<td class="item">${hi.itemno}</td>
								<c:choose>
									<c:when test="${not empty hi.hireInst}">
									<td class="barcode">${hi.hireInst.inst.plantid}</td>
									<td class="desc"><cwms:showmodel instrumentmodel="${hi.hireInst.inst.model}"/></td>
									<td class="serialno">${hi.hireInst.inst.serialno}</td>
									<td class="plantno">${hi.hireInst.inst.plantno}</td>
									</c:when>
									<c:otherwise>
									<td class="barcode">-</td>
									<td class="desc">${hi.hireCrossItem.description}</td>
									<td class="serialno">${hi.hireCrossItem.serialno}</td>
									<td class="plantno">${hi.hireCrossItem.plantno}</td>
									</c:otherwise>
								</c:choose>
								<td class="caltype"><t:showTranslationOrDefault translations="${hi.calType.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/></td>
								<td class="ukascal">${defaultCurrency.currencyERSymbol}${hi.ukasCalCost}</td>
								<td class="hirecost">${defaultCurrency.currencyERSymbol}${hi.hireCost}</td>
							</tr>										
						</c:forEach>
						</c:when>
						<c:otherwise>
						<tr>
							<td colspan="8" class="text-center bold"><spring:message code="hireenquiry.noitems" /></td>
						</tr>
						</c:otherwise>
					</c:choose>
				</tbody>								
			</table>
		
		</div>
		
		<!-- main div which contains all create hire enquiry form elements and has nifty corners applied -->
		<div class="infobox">
			
			<form:form action="" id="form" modelAttribute="form" method="post">
				
				<fieldset>
					
					<legend><spring:message code="hireenquiry.title" /></legend>
					<c:forEach var="item" items="${form.items}" varStatus="status">
						<form:hidden path="items[${status.index}].plantId"/>
						<form:hidden path="items[${status.index}].serialNo"/>
						<form:hidden path="items[${status.index}].plantNo"/>
						<form:hidden path="items[${status.index}].calTypeId"/>
						<form:hidden path="items[${status.index}].ukas"/>
						<form:hidden path="items[${status.index}].description"/>
						<form:hidden path="items[${status.index}].hireCost"/>
					</c:forEach>
					<ol>
						
						<li>
							<label><spring:message code="hireenquiry.contactandadress" />:</label>
																				
							<!-- this div creates a new cascading search plugin. The default search contains an input 
								 field and results box for companies, when results are returned this cascades down to
								 display the subdivisions within the selected company. This default behavoir can be obtained
								 by including the hidden input field with no value or removing the input field. You can also
								 add address and/or contact searches to the cascade by including the values 'contact' and/or 'address'
								 in the hidden inputs value attribute separated by a comma (e.g value="contact", value="address",
								 value="address,contact").  You can also swap these values around (e.g. value="contact,address").  
							 -->									
							<div id="cascadeSearchPlugin">
								<input type="hidden" id="cascadeRules" value="subdiv,contact,address" />
								<input type="hidden" id="compCoroles" value="client,business" />
								<input type="hidden" id="loadCurrency" value="true" />
								<c:if test="${(not empty form.coid) && (not empty form.subdivid) && (not empty form.addrid) && (not empty form.personid)}">	
									<input type="hidden" id="prefillIds" value="${form.coid},${form.subdivid},${form.personid},${form.addrid}" />
								</c:if>
							</div>
							
							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
						</li>
						<li>
							<form:label path="freehandDelivery"><spring:message code="hireenquiry.alternatedeladdress" />:</form:label>
							<form:checkbox path="freehandDelivery"  class="widthAuto"  onclick=" toggleFreehandContact(this.checked);  "  />
						</li>
						<li class="freehand ${errorClass}">
							<label><spring:message code="hirehome.contact" />:</label>
							<form:input type="text" path="freeName" />
							<form:errors path="freeName" cssClass="attention" >
								<span class="attention">${messages}</span>
								<c:set var="freehandError" value="true" />
							</form:errors>
						</li>
						<li class="freehand ${errorClass}">
							<label><spring:message code="company" />:</label>
							<form:input type="text" path="freeCompany" />
							<form:errors path="freeCompany" cssClass="attention" >
								<span class="attention">${messages}</span>
								<c:set var="freehandError" value="true" />
							</form:errors>
						</li>
						<li class="freehand ${errorClass}">
							<label><spring:message code="hireenquiry.address" /> 1:</label>
							<form:input type="text" path="freeAddr1" />
							<form:errors path="freeAddr1" cssClass="attention" >
								<span class="attention">${messages}</span>
								<c:set var="freehandError" value="true" />
							</form:errors>
						</li>
						<li class="freehand ${errorClass}">
							<label><spring:message code="hireenquiry.address" /> 2:</label>
							<form:input type="text" path="freeAddr2" />
							<form:errors path="freeAddr2" cssClass="attention" >
								<span class="attention">${messages}</span>
								<c:set var="freehandError" value="true" />
							</form:errors>
						</li>
						<li class="freehand ${errorClass}">
							<label><spring:message code="hireenquiry.town" />:</label>
							<form:input type="text" path="freeTown" />
							<form:errors path="freeTown" cssClass="attention" >
								<span class="attention">${messages}</span>
								<c:set var="freehandError" value="true" />
							</form:errors>
						</li>
						<li class="freehand ${errorClass}">
							<label><spring:message code="hireenquiry.county" />:</label>
							<form:input type="text" path="freeCounty" />
							<form:errors path="freeCounty" cssClass="attention" >
								<span class="attention">${messages}</span>
								<c:set var="freehandError" value="true" />
							</form:errors>
						</li>
						<li class="freehand ${errorClass}">
							<label><spring:message code="hireenquiry.postcode" />:</label>
							<form:input type="text" path="freePostcode" />
							<form:errors path="freePostcode" cssClass="attention" >
								<span class="attention">${messages}</span>
								<c:set var="freehandError" value="true" />
							</form:errors>
						</li>									
						<li>
							<form:label path="hire.hireDate"><spring:message code="hireenquiry.datehirefrom" />:</form:label>
							<form:input type="date" path="hire.hireDate" />
						</li>
						<li>
							<form:label path="hire.enquiryEstDuration"><spring:message code="hireenquiry.hireduration" />:</form:label>
							<form:select path="hire.enquiryEstDuration" >
								<c:forEach var="day" begin="${minHireDays}" end="90">
									<form:option value="${day}">${day}</form:option>
								</c:forEach>
							</form:select>
						</li>
						<li>
							<form:label path="hire.clientref"><spring:message code="hirehome.clientref" />:</form:label>
							<form:input path="hire.clientref" />
							<form:errors path="hire.clientref"/>
						</li>
						<li>
							<form:label path="currencyCode"><spring:message code="hireenquiry.currency" />:</form:label>
					
							<form:select path="currencyCode" >
								<c:forEach var="currency" items="${currencyList}">
									<form:option value="${currency.currencyCode}" >${currency.currencyCode}: ${currency.currencyERSymbol} [1:${currency.defaultRate}]</form:option>
									
								</c:forEach>
							</form:select>
							<form:errors path="currencyCode"/>
						</li>										
						<li>
							<label>&nbsp;</label>
							<input type="submit" name="submit" value="<spring:message code="hireenquiry.create" />" />
						</li>	
					</ol>																	
					
				</fieldset>
				
			</form:form>
		
		</div>
		<!-- end of create purchase order div -->
		
		<!-- show alternate delivery contact/address details -->
		<script type='text/javascript'>
			
			<c:if test="${freehandError == true}">
				function init()
				{
					$j('input[type="checkbox"]:last').attr('checked', 'checked').trigger('onclick');
				}	
			</c:if>

		</script>
    </jsp:body>
</t:crocodileTemplate>