<%-- File name: /trescal/core/hire/hirehome.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="hirehome.title"/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/hire/HireHome.js'></script>
		<script type='module' src='${pageContext.request.contextPath}/script/components/cwms-hire-search-form/main.js' ></script>
		<script type='module' src='${pageContext.request.contextPath}/script/components/cwms-submenu-element/cwms-submenu-element.js' ></script>
    </jsp:attribute>
    <jsp:body>
	       <!-- error section displayed when form submission fails -->
		<t:showErrors path="form.*" showFieldErrors="true" />
		<!-- end error section -->
							
		<!-- infobox contains all form elements relating to searching for hire items and is styled with nifty corners -->				
			<div class="infobox">
							
			<div class="float-right">
			<a href="<spring:url value='sourcehireinstruments.htm'/>" class="mainlink"><spring:message code="hirehome.newenquiry"/></a>
				 <c:if test="${not empty canVerifyHire}">
					| <a href="#" class="mainlink" onclick=" suspendAllHireItemsContent(); return false; "><spring:message code="hirehome.suspendall"/></a>
				 </c:if>
					| <a href="<spring:url value='viewhireinstrumentrevenues.htm'/>" class="mainlink"><spring:message code="hirehome.hireitemrevenue"/></a>
			</div>
		<!-- clear float and restore page flow -->
			<div class="clear-0"></div>
							
				<form >
							
				<fieldset>
								
				<legend><spring:message code="hire.browser"/></legend>
									
				<cwms-hire-search-form statusList="${cwms:collectionToJson(form.statusList)}"></cwms-hire-search-form>
		
		</fieldset>
		
		</form>
	
	</div>
	<!-- end of infobox div -->
	
	<!-- infobox div containing sub navigation -->
	<div class="infobox">
								
		<div id="tabmenu">
		
			<ul class="subnavtab">
				<li><a role="button" id="contracts-link" onclick=" switchMenuFocus(menuElements, 'contracts-tab', false); return false; " class="selected"><spring:message code="hirehome.hirecontracts"/> (<span class="contractSize">${form.contracts.size()}</span>)</a></li>
				<li><a role="button" id="enquiries-link" onclick=" switchMenuFocus(menuElements, 'enquiries-tab', false); return false; "><spring:message code="hirehome.hireenquiries"/> (<span class="enquirySize">${form.enquiries.size()}</span>)</a></li>
				<li><a role="button" id="awaitinginvoice-link" onclick=" switchMenuFocus(menuElements, 'awaitinginvoice-tab', false); return false; "><spring:message code="hirehome.awaitinginvoice"/> (<span class="invoiceSize">${form.awaitingInvoice.size()}</span>)</a></li>
				<li><a role="button" id="accountsvarification-link" onclick=" switchMenuFocus(menuElements, 'accountsvarification-tab', false); return false; "><spring:message code="hirehome.enquiriespendingaccountsverification"/> (<span class="enquirySize">${form.awaitingAccountsVarification.size()}</span>)</a></li>
				<li><cwms-submenu-element id="search-results-link" anchor="search-results-link" block="search-results-tab"><cwms-hire-search-results-count></cwms-hire-search-results-count></cwms-submenu-element></li>
			</ul>
		
			<div class="tab-box">										
																						
				<!-- div displays all hire contracts -->
				<div id="contracts-tab">
																											
					<table class="default4 hireHomeContracts" summary="<spring:message code='hirehome.contractstablesummary'/>">									
						<thead>
							<tr>
								<td colspan="5">
									<spring:message code="hirehome.hirecontracts"/> (<span class="contractSize">${form.contracts.size()}</span>)
								</td>
							</tr>
							<tr>
								<th class="hireno"><spring:message code="hirehome.hireno"/></th>
								<th class="contact"><spring:message code="hirehome.contact"/></th>
								<th class="company"><spring:message code="company"/></th>
								<th class="items"><spring:message code="hirehome.hireitems"/></th>
								<th class="hiredate"><spring:message code="hirehome.hiredate"/></th>														
							</tr>
						</thead>												
						<tfoot>
							<tr>
								<td colspan="5">&nbsp;</td>
							</tr>
						</tfoot>												
						<tbody>
							<c:choose>
								<c:when test="${form.contracts.size()} < 1}">
								<tr class="odd">
									<td colspan="5" class="center bold"><spring:message code="hirehome.noactivehirecontracts"/></td>
								</tr>
								</c:when>
								<c:otherwise>	
								<c:set var="rowcount" value="1"/>
								<c:forEach var="con" items="${form.contracts}">
									<tr <c:choose><c:when test="${rowcount % 2 == 0}"> class="even" </c:when><c:otherwise> class="odd" </c:otherwise></c:choose>>
										<td class="hireno"><a href="<spring:url value='viewhire.htm?id=${con.id}'/>"class="mainlink">${con.hireno}</a></td>
										<td class="contact">${con.contact.name}</td>
										<td class="company">${con.contact.sub.comp.coname}</td>
										<td class="items">
											${con.items.size()}
											<a href="#" onclick=" viewHireItems(this, $j(this).closest('tr').next()); return false; ">
												<img src="img/icons/items.png" width="16" height="16" alt="<spring:message code="hirehome.viewhireitems"/>" title="<spring:message code="hirehome.viewhireitems"/>" class="image_inline" />
											</a>
										</td>
										<td class="hiredate"><fmt:formatDate value="${con.hireDate}" type="date" dateStyle="SHORT" /></td>
									</tr>
									<tr class="hid <c:choose><c:when test="${rowcount % 2 == 0}"> even </c:when><c:otherwise> odd </c:otherwise></c:choose>">
										<td colspan="5">
											<div style=" display: none; ">
												<table class="child_table" summary="<spring:message code="hirehome.hireitemssummary"/>">
													<thead>
														<tr>
															<th scope="col" class="item"><spring:message code="hirehome.item"/></th>
															<th scope="col" class="desc"><spring:message code="instmodelname"/></th>
															<th scope="col" class="plant"><spring:message code="hirehome.plantnoserial"/></th>
															<th scope="col" class="caltype"><spring:message code="servicetype"/></th>
															<th scope="col" class="daysonhire"><spring:message code="hirehome.daysonhire"/></th>
															<th scope="col" class="hirecost"><spring:message code="hirehome.hirecost"/></th>					
														</tr>
													</thead>
													<tfoot>
														<tr>
															<td colspan="6">&nbsp;</td>
														</tr>
													</tfoot>
													<tbody>
														<c:forEach var="hi" items="${con.items}"> 
															<tr class="<c:choose><c:when test="${hi.suspended == true}"> highlight </c:when>
																		 		 <c:when test="${hi.offHire == false}"> highlight-gold </c:when>
														 				</c:choose>">
																<td class="item">${hi.itemno}</td>
																<td class="desc">
																	<c:choose>
																		<c:when test="${not empty hi.hireInst}">
																			<links:instrumentLinkDWRInfo instrument="${hi.hireInst.inst}" rowcount="${itemCount.index}" displayBarcode="${true}" displayName="${true}" displayCalTimescale="${false}" caltypeid="${hi.calType.calTypeId}"/><br />																
																		</c:when>
																		<c:otherwise>
																			${hi.hireCrossItem.description}
																		</c:otherwise>
																	</c:choose>	
																	<c:forEach var="hiacc" items="${hi.accessories}">
																		<div>${hiacc.hireAccessory.item}</div>
																	</c:forEach>
																</td>
																<td class="plant">
																	<c:choose>
																		<c:when test="${not empty hi.hireInst}">
																			${hi.hireInst.inst.plantno} <c:if test='${hi.hireInst.inst.serialno != ""}'>(${hi.hireInst.inst.serialno})</c:if>	
																		</c:when>
																		<c:otherwise>
																			${hi.hireCrossItem.plantno} <c:if test='${hi.hireCrossItem.serialno != ""}'>(${hi.hireCrossItem.serialno})</c:if>	
																		</c:otherwise>
																	</c:choose>
																</td>
																<td class="caltype"><t:showTranslationOrDefault translations="${hi.calType.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/></td>
																<td class="daysonhire">
																	<c:choose>
																		<c:when test="${hi.offHire == true}">
																			${hi.offHireDays}									
																		</c:when>
																		<c:otherwise>
																			${hi.daysOnHire}																
																		</c:otherwise>
																	</c:choose>
																	<c:if test="${hi.daysSuspended > 0}">
																			/ ${hi.daysSuspended} suspended
																	</c:if>	
																</td>
																<td class="hirecost">${hi.hire.currency.currencyERSymbol}${hi.hireCost}</td>
															</tr>															
														</c:forEach>
													</tbody>
												</table>
											</div>
										</td>
									</tr>
									<c:set var="rowcount" value="${rowcount + 1}"/>
								</c:forEach>
								</c:otherwise>
							</c:choose>	
						</tbody>
					</table>											
					
				</div>
				<!-- end of div to display -->
				
				<!-- div displays all hire enquiries -->
				<div id="enquiries-tab" class="hid">
																											
					<table class="default4 hireHomeEnquiries" summary="<spring:message code="hirehome.hireenquiriessummary"/>">										
						<thead>
							<tr>
								<td colspan="4">
									<spring:message code="hirehome.hireenquiries"/> (<span class="enquirySize">${form.enquiries.size()}</span>)
								</td>
							</tr>
							<tr>
								<th class="hireno"><spring:message code="hirehome.hireno"/></th>
								<th class="contact"><spring:message code="hirehome.contact"/></th>
								<th class="company"><spring:message code="company"/></th>
								<th class="hiredate"><spring:message code="hirehome.hiredate"/></th>														
							</tr>
						</thead>												
						<tfoot>
							<tr>
								<td colspan="4">&nbsp;</td>
							</tr>
						</tfoot>												
						<tbody>
							<c:choose>
								<c:when test="${form.enquiries.size()} < 1}">
								<tr class="odd">
									<td colspan="4" class="center bold"><spring:message code="hirehome.nohireenquiries"/></td>
								</tr>
								</c:when>
								<c:otherwise>
								<c:set var="rowcount" value="1"/>
								<c:forEach var="enq" items="${form.enquiries}">
									<tr <c:choose><c:when test="${rowcount % 2 == 0}"> class="even" </c:when><c:otherwise> class="odd" </c:otherwise></c:choose>>
										<td class="hireno"><a href="<spring:url value='viewhire.htm?id=${enq.id}'/>"class="mainlink">${enq.hireno}</a></td>
										<td class="contact">${enq.contact.name}</td>
										<td class="company">${enq.contact.sub.comp.coname}</td>
										<td class="hiredate"><fmt:formatDate value="${enq.hireDate}" type="date" dateStyle="SHORT"/></td>
									</tr>
									<c:set var="rowcount" value="${rowcount + 1}"/>
								</c:forEach>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>											
					
				</div>
				<!-- end of div to display -->
				
				<!-- div displays all hire contracts awaiting invoice -->
				<div id="awaitinginvoice-tab" class="hid">
																											
					<table class="default4 hireHomeContracts" summary="<spring:message code='hirehome.awaitinginvoicesummary'/>">									
						<thead>
							<tr>
								<td colspan="5">
									<spring:message code="hirehome.contractsawaitinginvoice"/> (<span class="invoiceSize">${form.awaitingInvoice.size()}</span>)
								</td>
							</tr>
							<tr>
								<th class="hireno"><spring:message code="hirehome.hireno"/></th>
								<th class="contact"><spring:message code="hirehome.contact"/></th>
								<th class="company"><spring:message code="company"/></th>
								<th class="items"><spring:message code="hirehome.hireitems"/></th>
								<th class="hiredate"><spring:message code="hirehome.hiredate"/></th>														
							</tr>
						</thead>												
						<tfoot>
							<tr>
								<td colspan="5">&nbsp;</td>
							</tr>
						</tfoot>												
						<tbody>
							<c:choose>
								<c:when test="${form.awaitingInvoice.size()} < 1}">
								<tr class="odd">
									<td colspan="5" class="center bold"><spring:message code="hirehome.noawaitinginvoice"/></td>
								</tr>
								</c:when>
								<c:otherwise>
								<c:set var="rowcount" value="1"/> 
								<c:forEach var="con" items="${form.awaitingInvoice}">
									<tr <c:choose><c:when test="${rowcount % 2 == 0}"> class="even" </c:when><c:otherwise> class="odd" </c:otherwise></c:choose>>
										<td class="hireno"><a href="<spring:url value='viewhire.htm?id=${con.id}'/>" class="mainlink">${con.hireno}</a></td>
										<td class="contact">${con.contact.name}</td>
										<td class="company">${con.contact.sub.comp.coname}</td>
										<td class="items">
											${con.items.size()}
											<a href="#" onclick=" viewHireItems(this, $j(this).closest('tr').next()); return false; ">
												<img src="img/icons/items.png" width="16" height="16" alt="<spring:message code="hirehome.viewhireitems"/>" title="<spring:message code="hirehome.viewhireitems"/>" class="image_inline" />
											</a>
										</td>
										<td class="hiredate"><fmt:formatDate value="${con.hireDate}" type="date" dateStyle="SHORT" /></td>
									</tr>
									<tr class="hid <c:choose><c:when test="${rowcount % 2 == 0}"> even </c:when><c:otherwise> odd </c:otherwise></c:choose>">
										<td colspan="5">
											<div style=" display: none; ">
											
												<table class="child_table" summary="<spring:message code="hirehome.hireitemssummary"/>">
													<thead>
														<tr>
															<th scope="col" class="item"><spring:message code="hirehome.item"/></th>
															<th scope="col" class="desc"><spring:message code="hirehome.description"/></th>
															<th scope="col" class="plant"><spring:message code="hirehome.plantnoserial"/></th>
															<th scope="col" class="caltype"><spring:message code="hirehome.caltype"/></th>
															<th scope="col" class="daysonhire"><spring:message code="hirehome.daysonhire"/></th>
															<th scope="col" class="hirecost"><spring:message code="hirehome.finalcost"/></th>					
														</tr>
													</thead>
													<tfoot>
														<tr>
															<td colspan="6">&nbsp;</td>
														</tr>
													</tfoot>
													<tbody>
														<c:forEach var="hi" items="${con.items}"> 
															<tr class="<c:choose><c:when test="${hi.suspended == true}"> highlight </c:when><c:when test="${hi.offHire == false}"> highlight-gold </c:when></c:choose>">
																<td class="item">${hi.itemno}</td>
																<td class="desc">
																	<c:choose>
																		<c:when test="${not empty hi.hireInst}">
																			<links:instrumentLinkDWRInfo instrument="${hi.hireInst.inst}" rowcount="${itemCount.index}" displayBarcode="${true}" displayName="${true}" displayCalTimescale="${false}" caltypeid="${hi.calType.calTypeId}"/><br />															
																		</c:when>
																		<c:otherwise>
																			${hi.hireCrossItem.description}
																		</c:otherwise>
																	</c:choose>
																	<c:forEach var="hiacc" items="${hi.accessories}">
																		<div>${hiacc.hireAccessory.item}</div>
																	</c:forEach>
																</td>
																<td class="plant">
																	<c:choose>
																		<c:when test="${not empty hi.hireInst}">
																			${hi.hireInst.inst.plantno} <c:if test='${hi.hireInst.inst.serialno != ""}'> (${hi.hireInst.inst.serialno})</c:if>
																		</c:when>
																		<c:otherwise>
																			${hi.hireCrossItem.plantno} <c:if test='${hi.hireCrossItem.serialno != ""}'>(${hi.hireCrossItem.serialno})</c:if>
																		</c:otherwise>
																	</c:choose>
																</td>
																<td class="caltype"><t:showTranslationOrDefault translations="${hi.calType.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/></td>
																<td class="daysonhire">
																	<c:choose>
																		<c:when test="${hi.offHire == true}">
																			${hi.offHireDays}						
																		</c:when>
																		<c:otherwise>
																			${hi.daysOnHire}																
																		</c:otherwise>
																	</c:choose>
																	<c:if test="${hi.daysSuspended > 0}">
																		/ ${hi.daysSuspended} suspended
																	</c:if>
																</td>
																<td class="hirecost">${hi.hire.currency.currencyERSymbol}${hi.finalCost}</td>
															</tr>															
														</c:forEach>
													</tbody>
												</table>
											</div>
										</td>
									</tr>
									<c:set var="rowcount" value="${rowcount + 1}"/>
								</c:forEach>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>												
					
				</div>
				<!-- end of div to display -->
														
				<!-- div displays all hire enquiries -->
				<div id="accountsvarification-tab" class="hid">
																											
					<table class="default4 hireHomeEnqsPendingVarification" summary="<spring:message code="hirehome.enquiriespendingaccountssummary"/>">										
						<thead>
							<tr>
								<td colspan="5">
									<spring:message code="hirehome.hireenquiriespendingaccounts"/> (<span class="enquirySize">${form.awaitingAccountsVarification.size()}</span>)
								</td>
							</tr>
							<tr>
								<th class="hireno"><spring:message code="hirehome.hireno"/></th>
								<th class="contact"><spring:message code="hirehome.contact"/></th>
								<th class="company"><spring:message code="company"/></th>
								<th class="hiredate"><spring:message code="hirehome.hiredate"/></th>
								<th class="verify"><spring:message code="hirehome.verify"/></th>												
							</tr>
						</thead>												
						<tfoot>
							<tr>
								<td colspan="5">&nbsp;</td>
							</tr>
						</tfoot>												
						<tbody>
							<c:choose>
								<c:when test="${form.awaitingAccountsVarification.size() < 1}">
								<tr class="odd">
									<td colspan="5" class="center bold"><spring:message code="hirehome.noenquiriespendingvarification"/></td>
								</tr>
								</c:when>
								<c:otherwise>														
								<c:forEach var="enq" items="${form.awaitingAccountsVarification}">
									<tr class="imageHeight">
										<td class="hireno"><a href="<spring:url value='viewhire.htm?id=${enq.id}'/>" class="mainlink">${enq.hireno}</a></td>
										<td class="contact">${enq.contact.name}</td>
										<td class="company">${enq.contact.sub.comp.coname}</td>
										<td class="hiredate"><fmt:formatDate value="${enq.hireDate}" type="date" dateStyle="SHORT" /></td>
										<td class="verify">
											<c:if test="${not empty canVerifyHire}">
												<a href="#" class="mainlink" onclick=" accountsVerifyHire(this,${enq.id}); return false; "><spring:message code="hirehome.verifyhire"/></a>
											</c:if>
										</td>
									</tr>
									<c:set var="rowcount" value="${rowcount + 1}"/>
								</c:forEach>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>											
					
				</div>
				<!-- end of div to display -->
														
				<div id="search-results-tab" class="hid">
					<cwms-hire-search-results></cwms-hire-search-results>
				</div>
			</div>
			<!-- end of div to hold all tabbed submenu content -->
				
		</div>
		<!-- end of div to hold tabbed submenu -->
		
		<!-- this div clears floats and restores page flow -->
		<div class="clear"></div>
	
	</div>
	<!-- end of infobox -->
    </jsp:body>
</t:crocodileTemplate>