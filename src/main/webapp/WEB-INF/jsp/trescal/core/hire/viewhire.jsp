<%-- File name: /trescal/core/hire/viewhire.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>
<%@ taglib prefix="costs" tagdir="/WEB-INF/tags/costs" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<c:choose>
				<c:when test="${form.hire.enquiry}">
					<spring:message code="hireview.titleenquiry"/>
				</c:when>
				<c:otherwise>
					<spring:message code="hireview.title"/>
				</c:otherwise>
			</c:choose>
			(${form.hire.hireno})
		</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/hire/ViewHire.js'></script>
		<script type='text/javascript' src='script/thirdparty/jQuery/jquery.js'></script>
		<script type='module' src='script/components/cwms-upload-file/cwms-upload-file.js'></script>
		<script type="module" src='script/components/cwms-create-hire-contract-from-enquiry/cwms-create-hire-contract-from-enquiry.js'></script>
    	<script type="module" src="script/components/cwms-hire-view/cwms-hire-items/cwms-hire-items.js" ></script>

		<script type='text/javascript'>
			var caltypeoptions = [];
			<c:forEach var="ct" items="${caltypes}">
				caltypeoptions.push({calTypeId: ${ct.key}, calTypeName: "${ct.value}"});
			</c:forEach>
			// get default ukas cal cost from project properties
			var defaultUkasCal = '${defaultUkasCal}';
			//var defaultUkasCal = '0.00';
			// get hire accessory status options
			var hireAccStateOptions = [];
			<c:forEach var="hireAccState" items="${hireAccStatuss}">
				<c:if test="${!hireAccState.defaultStatus}">
					hireAccStateOptions.push({statusid:	${hireAccState.statusid}, statusname: '${hireAccState.name}'});
				</c:if>
			</c:forEach>
			// define courier options from Spring model
			var couriers = ${couriers};
		</script>
	</jsp:attribute>
	<jsp:attribute name="globalElements">
		<cwms-upload-file system-component-id="${sc.componentId}" entity-id1="${form.hire.hireno}" entity-id2=""
 			files-name="${filesName}" rename-to="" web="false"></cwms-upload-file>
	</jsp:attribute>
	<jsp:body>
		<c:set var="hire" value="${form.hire}"/>
		<t:showErrors path="form.*" showFieldErrors="true" />
		<c:if test="${hire.status.rejected}">
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<h5 class="center-0 attention"><spring:message code="hireview.hirerejected"/></h5>
					</div>
				</div>
			</div>
		</c:if>
		<!-- infobox contains all hire information and is styled with nifty corners -->
		<div class="infobox">
			<fieldset>
				<legend>
					<c:choose>
						<c:when test="${hire.enquiry}">
							<spring:message code="hireview.hireenquiry"/>
						</c:when>
						<c:otherwise>
							<spring:message code="hireview.hire"/>
						</c:otherwise>
					</c:choose>
					: ${hire.hireno}
				</legend>
					<!-- displays content in column 49% wide (left) -->
					<div class="displaycolumn">
						<ol>
							<li>
								<label><spring:message code="company"/>:</label>
								<span><links:companyLinkDWRInfo company="${hire.contact.sub.comp}" rowcount="0" copy="${true}"/></span>
							</li>
							<li>
								<label><spring:message code="subdivision"/>:</label>
								<span><links:subdivLinkDWRInfo subdiv="${hire.contact.sub}" rowcount="0"/></span>
							</li>
							<li>
								<label><spring:message code="contact"/>:</label>
								<span><links:contactLinkDWRInfo contact="${hire.contact}" rowcount="0"/></span>
							</li>
							<li>
								<label><spring:message code="address"/>:</label>
								<div class="float-left padtop">
									<c:choose>
										<c:when test="${hire.address == null}">
											<spring:message code="hireview.noaddress"/>
										</c:when>
										<c:otherwise>
											<t:showFullAddress address="${hire.address}" separator="<br/>" copy="${true}"/>
										</c:otherwise>
									</c:choose>
								</div>
								<div class="clear"></div>
							</li>
							<c:if test="${hire.freehandContact != null && form.freehandDelivery}">
								<li>
									<label><spring:message code="hireview.deladdress"/>:</label>
									<div class="float-left padtop">
										<c:set var="addr" value="${hire.freehandContact.address.split(',')}"/>
										${hire.freehandContact.contact}<br/>
										${hire.freehandContact.company}<br/>
										<c:forEach var="a" items="${addr}">
											<c:if test="${a != ''}">
												{a}<br/>
											</c:if>
										</c:forEach>
									</div>
									<div class="clear"></div>
								</li>
							</c:if>
							<li>
								<label><spring:message code="hireenquiry.currency"/>:</label>
								<span>
									<c:set var="currency" value="${hire.currency}"/>
									${currency.currencyCode} (${currency.currencySymbol} ${currency.currencyName}
									- ${defaultCurrency.currencyERSymbol}1 : ${currency.currencyERSymbol}${hire.rate})
								</span>
							</li>
							<c:if test="${hire.relatedInstructions.size() > 0}">
								<li>
								<div class="clear" style=" height: 1px; "></div>
								<div class="jobInstruction">
									<c:set var="hireInstructions" value="${false}"/>
									<c:forEach var="i" items="${hire.relatedInstructions}">
										<c:forEach var="uit" items="${currentContact.userPreferences.userInstructionTypes}">
											<c:if test="${uit.instructionType == i.instruction.instructiontype}">
												<strong class="jobInstructionType">${i.instruction.instructiontype.type} <spring:message code="instruction"/>:</strong><br/>
												<span class="
													<c:choose>
														<c:when test='${datetools.isDateAfterXTimescale(-1, "m", i.instruction.lastModified)}'>
															red
														</c:when>
														<c:when test='${datetools.isDateAfterXTimescale(-3, "m", i.instruction.lastModified)}'>
															amber
														</c:when>
														<c:otherwise>
															brown
														</c:otherwise>
													</c:choose>
												">
												${i.instruction.instruction}</span><br/>
												<c:set var="hireInstructions" value="${true}"/>
											</c:if>
										</c:forEach>
									</c:forEach>
									<c:if test="${!hireInstructions}">
										<span class="attention">
											<spring:message code="hireview.hireinstructions"/><br/>
											<spring:message code="hireview.nohireinstructions"/>
										</span>
									</c:if>
								</div>
							</li>
						</c:if>
					</ol>
				</div>
				<!-- end of left column -->
				<!-- displays content in column 49% wide (right) -->
				<div class="displaycolumn">
					<ol>
						<li>
							<label><spring:message code="hireresult.status"/>:</label>
							<div class="float-left width70">
								<span class="bold"><cwms:besttranslation translations="${hire.status.nametranslations}"/></span>
								&nbsp;
								<c:choose>
									<c:when test="${hire.status.defaultStatus && hire.accountsVarified && form.itemsReadyForHire}">
										<cwms-create-hire-contract-from-enquiry hireId="${hire.id}"></cwms-create-hire-contract-from-enquiry>
									</c:when>
									<c:when test="${hire.status.defaultStatus && hire.accountsVarified && !form.itemsReadyForHire}">
										(<spring:message code="hireview.itemsstillonhire"/>)
									</c:when>
									<c:when test="${hire.status.defaultStatus && !hire.accountsVarified}">
										(<span class="attention"><spring:message code="hireview.paymentrequired"/></span>)
									</c:when>
									<c:when test="${hire.status.requiringAttention && canCompleteHire}">
										(<a href=""
											onclick="event.preventDefault(); completeHireContract(${hire.id}, $j(this).parent()); return false;"
											class="mainlink" title="<spring:message code='hireview.setstatuscomplete'/>">
											<spring:message code="hireview.completehire"/></a>)
									</c:when>
								</c:choose>
							</div>
							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
						</li>
						<li>
							<label><spring:message code="hireview.enquirydate"/>:</label>
							<span><fmt:formatDate value="${hire.enquiryDate}" type="date" dateStyle="SHORT" /></span>
						</li>
						<li>
							<label><spring:message code="hirehome.hiredate"/>:</label>
							<span><fmt:formatDate value="${hire.hireDate}" type="date" dateStyle="SHORT" /></span>
						</li>
						<c:if test="${hire.enquiry}">
							<li>
								<label><spring:message code="hireview.esthireduration"/>:</label>
								<span>${hire.enquiryEstDuration}</span>
							</li>
						</c:if>
						<li>
							<label><spring:message code="hirehome.clientref"/>:</label>
							<span>${hire.clientref}</span>
						</li>
						<c:choose>
							<c:when test="${hire.enquiry}">
								<li>
									<label><spring:message code="hireview.enquiryconfirmation"/>:</label>
									<span>
										<a href="#" class="mainlink email"
											onclick="sendEnquiryEmailConfirmation(${hire.id}, ${currentContact.personid}, ${sc.componentId}, ${hire.enquiry}); return false;">
											<spring:message code="hireview.sendconfirmationmail"/>
										</a> 
									</span>
								</li>
							</c:when>
							<c:otherwise>
								<li>
									<label><spring:message code="hireview.clientpo"/>:</label>
									<span>${hire.poNumber}</span>
								</li>
								<li>
									<label><spring:message code="hireview.specialinstructions"/>:</label>
									<div class="float-left padtop width70">
										${hire.specialInst}
									</div>
									<div class="clear"></div>
								</li>
								<li>
									<label><spring:message code="hireview.transportout"/>:</label>
									<span>
										<c:choose>
											<c:when test="${not empty hire.crdes}">
												${hire.crdes.cdtype.courier.name} - ${hire.crdes.cdtype.description}
											</c:when>
											<c:otherwise>
												<spring:message code="hireview.clientcollection"/>
											</c:otherwise>
										</c:choose>
									</span>
								</li>
							</c:otherwise>
						</c:choose>
					</ol>
				</div>
				<!-- end of right column -->
			</fieldset>
		</div>
		<!-- end of hire information div -->
		<!-- sub navigation menu which displays and hides sections of page as well as linking to external pages -->
		<div id="subnav">
			<dl>
				<dt>
					<a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'items-tab', false); " id="items-link" 
						title="<spring:message code='hireview.viewpurchaseorderitems'/>" class="selected">
						<spring:message code='hireview.hireitemslegend'/> (<span class="hireItemSize">${hire.items.size()}</span>)
					</a>
				</dt>
				<c:if test="${hire.enquiry}">
					<dt>
						<a href="sourcehireinstruments.htm?id=${hire.id}&replace=false"
							title="<spring:message code='hireview.additemtohireenquiry'/>">
							<spring:message code="hireview.newitem"/>
						</a>
					</dt>
				</c:if>
				<dt>
					<a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'edit-tab', false); " id="edit-link"
						title="<spring:message code='hireview.editpurchaseorderdetail'/>">
						<spring:message code="hireview.edit"/>
					</a>
				</dt>
				<c:if test="${!hire.enquiry}">
					<dt>
						<a href="#" onclick="switchMenuFocus(menuElements, 'despatch-tab', false); return false;" id="despatch-link"
							title="<spring:message code='hireview.vieweditdespatchoptions'/>">
							<spring:message code="hireview.despatchinformation"/>
						</a>
					</dt>
				</c:if>
				<dt>
					<a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'files-tab', false); " id="files-link"
						title="<spring:message code='hireview.viewdocuments'/>">
						<spring:message code="hireview.documents"/> (<span class="fileResourceCount">${scRootFiles.numberOfFiles}</span>)
					</a>
				</dt>
				<!-- Note, perhaps need to add check of whether hire has become a contract (not enquiry), only allow document generation if so? -->
				<dt>
					<a href="#" onclick=" event.preventDefault(); genDocPopup(${hire.id}, 'hirecontractbirtdocument.htm?id=', 'Generating Hire Contract Document'); " 
						title="<spring:message code='hireview.generatedocument' />">
						<spring:message code="generate" />
					</a>
				</dt>
				<dt>
					<a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'email-tab', false); " id="email-link"
						title="<spring:message code='hireview.viewemail'/>)">
						<spring:message code="hireview.emails"/> (<span class="entityEmailDisplaySize">${hire.sentEmails.size()}</span>)
					</a>
				</dt>
				<c:if test="${!hire.enquiry}">
					<dt>
						<a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'invoices-tab', false); " id="invoices-link"
							title="<spring:message code='hireview.viweinvoices'/>">
							<spring:message code="hireview.invoices"/> (${invoices.size()})
						</a>
					</dt>
				</c:if>
				<dt>
					<a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'printing-tab', false); " id="printing-link"
						title="<spring:message code='hireview.printlabel'/>">
						<spring:message code="hireview.printing"/>
					</a>
				</dt>
			</dl>
		</div>
		<!-- end of sub navigation menu -->
		<!-- infobox contains all purchase order functionality and is styled with nifty corners -->
		<div class="infobox">
			<c:set var="currency" value="${hire.currency}"/>
			<div id="items-tab">
            <cwms-hire-items items="${cwms:objectToJson(items)}" enquiry="${hire.enquiry}" 
			currencyCode="${currency.currencyCode}"
			servicetypes="${cwms:objectToJson(caltypes)}"
			defaultUkasCal="${defaultUkasCal}"
			status="${cwms:objectToJson(hire.status)}"
				></cwms-hire-items>
				
				<c:if test="${!hire.enquiry}">
					<table class="default4 viewHireTotals" summary="<spring:message code='hireview.tablehiretotalvalue'/>">
						<tbody>
							<tr class="nobord">
								<td class="totalsum cost2pcTotal bold"><spring:message code="hireview.totalcost"/>:</td>
								<td class="totalamount cost2pcTotal bold">
									${currency.currencyERSymbol}<span id="invTotalCost">${hire.totalCost}</span>
								</td>
								<td class="filler">&nbsp;</td>
							</tr>																				
						</tbody>
					</table>								
					<table class="default4 viewHireTotals" summary="<spring:message code='hireview.tablehiretotalvalue'/>">
						<tbody>
							<tr class="nobord">
								<td class="totalsum cost2pcTotal bold"><spring:message code="hireview.nettotal"/>:</td>
								<td class="totalamount cost2pcTotal bold">
									${currency.currencyERSymbol}<span id="invFinalCost">${hire.finalCost}</span>
								</td>
								<td class="filler">&nbsp;</td>
							</tr>
						</tbody>
					</table>
				</c:if>
			</div>
			<!-- this section contains all edit purchase order form elements -->
			<div id="edit-tab" class="hid">
				<form:form action="" method="post" modelAttribute="form">
					<fieldset class="editHireForm">
						<legend><spring:message code="hireview.edithire"/></legend>
						<ol>
							<form:hidden path="personid" value="${hire.contact.personid}"/>
							<form:hidden path="addrid" value="${hire.address.addrid}"/>
							<li>
								<form:label path="hire.hireDate"><spring:message code="hireenquiry.datehirefrom"/>:</form:label>
								<form:input type="date" path="hire.hireDate"  autocomplete="off"/>
								<c:if test="${hire.status.defaultStatus || hire.status.accepted}">
									<a href="#" class="mainlink-float voidhire" onclick="voidHire(${hire.id}, '${hire.hireno}', ${hire.items.size()}, '${hire.address.sub.comp.coname}'); return false;">
									<spring:message code="hireview.voidhire"/></a>
								</c:if>
							</li>
							<li>
								<form:label path="hire.enquiryEstDuration"><spring:message code="hireenquiry.hireduration"/>:</form:label>
								<form:select path="hire.enquiryEstDuration">
									<c:forEach var="days" begin="${minHireDays}" end="90">
										<form:option value="${days}"/>
									</c:forEach>
								</form:select>
							</li>
							<li>
								<form:label path="hire.clientref"><spring:message code="hirehome.clientref"/>:</form:label>
								<form:input path="hire.clientref"/>
								<form:errors path="hire.clientref"/>
							</li>
							<li>
								<form:label path="hire.poNumber"><spring:message code="hireview.clientpo"/>:</form:label>
								<form:input path="hire.poNumber"/>
								<form:errors path="hire.poNumber"/>
							</li>
							<li>
								<form:label path="currencyCode"><spring:message code="hireenquiry.currency"/>:</form:label>
								<form:select path="currencyCode">
									<c:forEach var="currency" items="${currencyList}">
										<option value="currency.currencyCode" <c:if test="${currency.currencyCode == hire.currency.currencyCode}">selected</c:if>>${currency.currencyCode} ${currency.currencyERSymbol} [1:${currency.defaultRate}]</option>
									</c:forEach>
								</form:select>
								<form:errors path="currencyCode"/>
							</li>
							<li>
								<form:label path="freehandDelivery"><spring:message code="hireenquiry.alternatedeladdress"/>:</form:label>
								<form:checkbox path="freehandDelivery" class="widthAuto" onclick="toggleFreehandContact(this.checked);"/>
							</li>
							<li class="freehand <c:if test='${hire.freehandContact == null && !form.freehandDelivery}'>hid</c:if>">
								<form:label path="freeName"><spring:message code="hirehome.contact"/>:</form:label>
								<form:input path="freeName" value="${hire.freehandContact.contact}"/>
								<span class="attention">
									<form:errors path="freeName"/>
								</span>
							</li>
							<li class="freehand <c:if test='${hire.freehandContact == null && !form.freehandDelivery}'>hid</c:if>">
								<form:label path="freeCompany"><spring:message code="company"/>:</form:label>
								<form:input path="freeCompany" value="${hire.freehandContact.company}"/>
								<span class="attention">
									<form:errors path="freeName"/>
								</span>
							</li>
							<c:set var="addr" value="${hire.freehandContact.address.split(',')}"/>
							<li class="freehand <c:if test='${hire.freehandContact == null && !form.freehandDelivery}'>hid</c:if>">
								<form:label path="freeAddr1"><spring:message code="hireenquiry.address"/> 1:</form:label>
								<form:input path="freeAddr1" value="${listtool.get(addr, 0)}"/>
								<span class="attention">
									<form:errors path="freeAddr1"/>
								</span>
							</li>
							<li class="freehand <c:if test='${hire.freehandContact == null && !form.freehandDelivery}'>hid</c:if>">
								<form:label path="freeAddr2"><spring:message code="hireenquiry.address"/> 2:</form:label>
								<form:input path="freeAddr2" value="${listtool.get(addr, 1)}"/>
								<span class="attention">
									<form:errors path="freeAddr2"/>
								</span>
							</li>
							<li class="freehand <c:if test='${hire.freehandContact == null && !form.freehandDelivery}'>hid</c:if>">
								<form:label path="freeTown"><spring:message code="hireenquiry.town"/>:</form:label>
								<form:input path="freeTown" value="${listtool.get(addr, 2)}"/>
								<span class="attention">
									<form:errors path="freeTown"/>
								</span>
							</li>
							<li class="freehand <c:if test='${hire.freehandContact == null && !form.freehandDelivery}'>hid</c:if>">
								<form:label path="freeCounty"><spring:message code="hireenquiry.county"/>:</form:label>
								<form:input path="freeCounty" value="${listtool.get(addr, 3)}"/>
								<span class="attention">
									<form:errors path="freeCounty"/>
								</span>
							</li>
							<li class="freehand <c:if test='${hire.freehandContact == null && !form.freehandDelivery}'>hid</c:if>">
								<form:label path="freePostcode"><spring:message code="hireenquiry.postcode"/>:</form:label>
								<form:input path="freePostcode" value="${listtool.get(addr, 4)}"/>
								<span class="attention">
									<form:errors path="freePostcode"/>
								</span>
							</li>
							<li>
								<label>&nbsp;</label>
								<input type="submit" name="submit" value="<spring:message code='hireviewedit.submit'/>"/>
							</li>
						</ol>
					</fieldset>
				</form:form>
			</div>
			<!-- end of edit purchase order section -->
			<!-- this section contains hire despatch options (courier) and confirmation table once selected -->
			<div id="despatch-tab" class="hid">
				<div id="despatchsummary">
					<fieldset>
						<legend><spring:message code="hireview.despatchdelinformtaion"/></legend>
						<ol>
							<c:choose>
								<c:when test="${hire.crdes != null}">
									<li>
										<label class="labelhead bold">
											<a href="viewcrdes.htm?cdid=${hire.crdes.crdespid}" class="mainlink"
												title="<spring:message code='hireview.viewcourierdespatch'/>">
												<spring:message code="hireview.viewcourierdespatchassigned"/>
											</a>
										</label>
										<span>&nbsp;</span>
									</li>
									<li>
										<label><spring:message code="hireview.consignmentno"/>:</label>
										<span>
											<c:choose>
												<c:when test="${hire.crdes.consignmentno != null && hire.crdes.consignmentno.trim().length() > 0}">
													${hire.crdes.consignmentno}
												</c:when>
												<c:otherwise>
													-- N/A --
												</c:otherwise>
											</c:choose>
										</span>
									</li>
									<li>
										<label><spring:message code="hireview.couriertype"/>:</label>
										<span>${hire.crdes.cdtype.courier.name}</span>
									</li>
									<li>
										<label><spring:message code="hireview.despatchtype"/>:</label>
										<span>${hire.crdes.cdtype.description}</span>
									</li>
									<li>
										<label><spring:message code="hireview.despatchdate"/>:</label>
										<span>
										    <fmt:formatDate type="date" dateStyle="medium" value="${hire.crdes.despatchDate}"/>
									</span>
									</li>
								</c:when>
								<c:otherwise>
									<li>
										<label><spring:message code="hireview.collection"/>:</label>
										<span><spring:message code="hireview.itemscollectedbyclient"/></span>
									</li>
								</c:otherwise>
							</c:choose>
							<li>
								<label><spring:message code="hireview.changedespatch"/>:</label>
								<c:set var="cdId" value="${hire.crdes == null ? 0 : hire.crdes.crdespid}"/>
								<span>
									<a href="#" class="mainlink"
										onclick="confirmHireDespatchTypeChange(${hire.id}, ${cdId}, ${hire.contact.personid}, ${hire.address.addrid}, ${hire.address.sub.subdivid}); return false;">
										<spring:message code="hireview.changedespatchtype"/>
									</a>
								</span>
							</li>
						</ol>
					</fieldset>
				</div>
			</div>
			<!-- end of despatch options div -->
			<!-- this section displays all purchase order files -->
			<div id="files-tab" class="hid">
				<files:showFilesForSC entity="${hire}" sc="${sc}" id="${hire.id}" identifier="${hire.hireno}" ver="" rootFiles="${scRootFiles}" allowEmail="${true}" isEmailPlugin="${false}" rootTitle="<spring:message code='hireview.filesforhire'/> ${hire.hireno}" deleteFiles="${true}"/>
			</div>
			<!-- end of file div -->
			<!-- this section displays all emails for the selected hire -->
			<div id="email-tab" class="hid">
				<t:showEmails entity="${hire}" entityId="${hire.id}" entityName="Hire" sc="${sc}"/>
			</div>
			<!-- end of email section -->
			<!-- this section displays all invoices for hire -->
			<div id="invoices-tab" class="hid">
				<table class="default4" id="invoicetable" summary="<spring:message code='hireview.tableinvoicesforhirejob'/>">
					<thead>
						<tr>
							<td colspan="6"><spring:message code="hireview.invoicesforhire"/> (${invoices.size()})</td>
						</tr>
						<tr>
							<th id="invno" scope="col"><spring:message code="hireview.invoiceno"/></th>
							<th id="type" scope="col"><spring:message code="hireview.invoicetype"/></th>
							<th id="company" scope="col"><spring:message code="company"/></th>
							<th id="createby" scope="col"><spring:message code="hireview.createdby"/></th>
							<th id="createon" scope="col"><spring:message code="hireview.createdon"/></th>
							<th id="status" scope="col"><spring:message code="hireresult.status"/></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="6">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody>
						<c:choose>
							<c:when test="${invoices.size() == 0}">
								<tr>
									<td colspan="6" class="bold center"><spring:message code="hireview.noinvoicesforjob"/></td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:forEach var="invref" items="${invoices}" varStatus="invCount">
									<tr>
										<td>
											<a href="viewinvoice.htm?id=${invref.invoice.id}" class="mainlink">
												${invref.invoice.invno}
											</a>
										</td>
										<td>
											<cwms:besttranslation translations="${invref.invoice.type.nametranslation}"/>
										</td>
										<td>${invref.invoice.comp.coname}</td>
										<td>${invref.invoice.createdBy.name}</td>
										<td><fmt:formatDate type="date" dateStyle="SHORT" value="${invref.invoice.regdate}"/></td>
										<td>
											<cwms:besttranslation translations="${invref.invoice.status.descriptiontranslations}"/>
										</td>
									</tr>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
			</div>
			<!-- end of invoices section -->
			<!-- this section contains links for printing tasks for the selected job -->
			<div id="printing-tab" class="hid">
				<fieldset>
					<legend><spring:message code="hireview.printingtasks"/></legend>
					<ol>
						<li>
							<label><spring:message code="hireview.hirefilelabel"/>:</label>
							<span>
								<a href="#" class="mainlink" onclick="event.preventDefault(); printDymoHireLabel(${hire.id});">
								<spring:message code="hireview.printhirefilelabel"/></a>
							</span>
						</li>
					</ol>
				</fieldset>
			</div>
			<!-- end of printing section -->
		</div>
		<!-- end of infobox -->
		<!-- this section contains all hire notes -->
		<t:showTabbedNotes entity="${hire}" noteType="HIRENOTE" noteTypeId="${hire.id}" privateOnlyNotes="${privateOnlyNotes}"/>
		<!-- end of hire notes section -->
	</jsp:body>
</t:crocodileTemplate>