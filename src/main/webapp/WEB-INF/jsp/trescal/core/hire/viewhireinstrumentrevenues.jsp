<%-- File name: /trescal/core/hire/viewhireinstrumentrevenues.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
     	<span class="headtext"><spring:message code="hireinstr.title"/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/hire/ViewHireInstrumentRevenues.js'></script>
    </jsp:attribute>
    <jsp:body>
		<!-- error section displayed when form submission fails -->
		<!-- end error section -->					
		<form:form action="" method="post" modelAttribute="form">
			<form:errors path="*">
        		<div class="warningBox1">
                	 <div class="warningBox2">
                    	   <div class="warningBox3">
                        	    <h5 class="center-0 attention">
                            	     <spring:message code="hiresearchinstr.errorsubmitpage" />
                            	</h5>
                            	<c:forEach var="e" items="${messages}">
                               		<div class="center attention"><c:out value="${e}"/></div>
                            	</c:forEach>
                     	 </div>
                 	</div>
           		</div>
       	    </form:errors>
		
			<!-- infobox contains source hire instrument form elements and is styled with nifty corners -->
			<div class="infobox">
			
				<fieldset>
				
					<legend><spring:message code="hireinstr.searchhireinstr"/></legend>
					
					<!-- displays content in column 50% wide (left) -->
					<div class="displaycolumn">
					
						<ol>
							<li>
									<form:label path="barcode"><spring:message code="barcode"/>:</form:label>
									<form:input type="text" path="barcode" size="10" value="${form.barcode}" tabindex="1" />	
							</li>
							<li>
									<form:label path="serialno"><spring:message code="serialno"/>:</form:label>
									<form:input type="text" path="serialno" value="${form.serialno}" tabindex="2" />
							</li>
							<li>
									<form:label path="plantno"><spring:message code="plantno"/>:</form:label>
									<form:input type="text" path="plantno" value="${form.plantno}" tabindex="3" />
							</li>												
							<li style=" height: 20px; ">
								<label>&nbsp;</label>
								<span id="loading" class="hid">
									<img src="img/web/searching.gif" width="128" height="15" alt="<spring:message code='hiresearchinstr.loadingresults'/>"> title="<spring:message code='hiresearchinstr.loadingresults'/>"
								</span>
								<span id="clear">
									<input type="submit" name="submit" value="<spring:message code='hirehome.submit'/>"/>
								</span>
								<span>&nbsp;</span>
							</li>
						</ol>										
						
					</div>
					
					<!-- displays content in column 50% wide (right) -->
					<div class="displaycolumn">
					
						<ol>											
							<li>
								<label><spring:message code="hirehome.manufacturer"/>:</label>
								<!-- float div left -->
								<div class="float-left">
									<!-- this div creates a new manufacturer jquery search plugin -->	
									<div class="mfrSearchJQPlugin">
										<input type="hidden" name="fieldId" value="mfrid" />
										<input type="hidden" name="fieldName" value="mfrname" />
										<input type="hidden" name="textOnlyOption" value="true" />
										<input type="hidden" name="tabIndex" value="4" />
										<input type="hidden" name="mfrNamePrefill" value="${form.mfrname}" />
										<input type="hidden" name="mfrIdPrefill" value="${form.mfrid}" />
										<!-- manufacturer results appear here -->
									</div>
								</div>																							
								<!-- clear floats and restore pageflow -->
								<div class="clear-0"></div>
							</li>
							<li>
								<form:label path="modelname"><spring:message code="hirehome.model"/>:</form:label>
								<form:input type="text" path="modelname" value="${form.modelname}" tabindex="5" />
							</li>
							<li>
								<label><spring:message code="hirehome.description"/>:</label>

								<!-- float div left -->
								<div class="float-left">
									<!-- inst description results appear here -->
									<form:input type="text" path="descname" id="descname" value="${form.descname}" tabindex="9" />
									<form:input type="hidden" path="descid" id="descid" value="${form.descid}" />

								</div>																							
								<!-- clear floats and restore pageflow -->
								<div class="clear-0"></div>					
							</li>
							<li>	
								<label style=" width: 100px; "><spring:message code="tools.saveresults"/></label>
								<form:checkbox path="export"/>
								<spring:message code="tools.viewspreadsheet" />
							</li>
						</ol>
					
					</div>
					
				</fieldset>							
										
			</div>
			<!-- end of infobox div -->														
			
			<c:choose>
				<c:when test="${not empty form.revWrappers}">
			
				<!-- infobox contains hire instruments with nifty corners -->	
				<div class="infobox">
		
					<table class="default4 viewHireInstRevenues" summary="">
						<thead>
							<tr>
								<td colspan="5"><spring:message code="hireinstr.hireinstrumentsfound"/> (<span class="hireInstSize">${form.revWrappers.size()}</span>)</td>
							</tr>
							<tr>
								<th class="plantno" scope="col"><spring:message code="hirenome.plantno"/></th>
								<th class="inst" scope="col"><spring:message code="hireresult.instrument"/></th>
								<th class="hires" scope="col"><spring:message code="hireinstr.totalhires"/></th>
								<th class="days" scope="col"><spring:message code="hirehome.daysonhire"/></th>
								<th class="revenue" scope="col"><spring:message code="hireinstr.revenue"/></th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="5">&nbsp;</td>
							</tr>
						</tfoot>
						<tbody>
							<c:choose>
								<c:when test="${form.revWrappers.size() > 0}">
								<c:forEach var="wrap" items="${form.revWrappers}">
												<tr>
												<td class="plantno">
												<a href="viewhireinstrumentrevenues.htm?hiid=${wrap.hi.id}">${wrap.hi.inst.plantno}</a>
											</td>
										<td class="inst"><cwms:showmodel instrumentmodel="${wrap.hi.inst.model}"/></td>
										<td class="hires">${wrap.timesHired}</td>
										<td class="days">${wrap.daysOnHire}</td>
										<td class="revenue">&pound;${wrap.revenue}</td>
									</tr>
									</c:forEach>
								</c:when>
								<c:otherwise>
								<tr>
									<td colspan="5" class="text-center bold"><spring:message code="hireinstr.nohireinstrumentrevenuestodisplay"/></td>
								</tr>
								</c:otherwise>
								</c:choose> 
						</tbody>								
					</table>
					
				</div>
				<!-- end of infobox div -->	
			
			</c:when>
			<c:when test="${not empty form.hi}">
				
				<table class="default4 viewHireInstrumentRevenues" summary="<spring:message code='hireresult.tablesummary'/>">
					<thead>
						<tr>
							<td colspan="7">
								<spring:message code="hireresult.hireresult"/> (${form.hi.hireItems.size()})
							</td>
						</tr>										
						<tr>
							<th class="vrhireno" scope="col"><spring:message code="hirehome.hireno"/></th>  
							<th class="vrconame" scope="col"><spring:message code="company"/></th>  
							<th class="vrcontact" scope="col"><spring:message code="contact"/></th>  
							<th	class="vrpurchaseord" scope="col"><spring:message code="hirehome.purchaseorder"/></th>
							<th class="vrclientref" scope="col"><spring:message code="hirehome.clientref"/></th>
							<th class="vrhiredate" scope="col"><spring:message code="hirehome.hiredate"/></th>
							<th class="vrstatus" scope="col"><spring:message code="hireresult.status"/></th>
						</tr>
					</thead>
				</table>								
				
				<c:set var="rowcount" value="0"/>
				<c:choose>
					<c:when test="${form.hi.hireItems.size() < 1}">
						<table class="default4" summary="<spring:message code='hireresult.tablenoentrysummary'/>">
							<tbody>
								<tr>
									<td colspan="7" class="bold center">
										<spring:message code="hire.noresult"/>
									</td>
								</tr>
							</tbody>
						</table>
					</c:when>
					<c:otherwise>												
						<c:forEach var="hi" items="${form.hi.hireItems}">
							<table class="default4 viewHireInstrumentRevenues" summary="<spring:message code='hireresult.tablesearchresultsummary'/>">
							
								<tbody>	
				
									<tr>
										<td class="vrhireno"><a class="mainlink" href="<spring:url value='/viewhire.htm?id=${hi.hire.id}'/>"> ${hi.hire.hireno}</a></td>
										<td class="vrconame"><links:companyLinkDWRInfo copy="false" company="${hi.hire.contact.sub.comp}" rowcount="0" /></td>
										<td class="vrcontact"><links:contactLinkDWRInfo contact="${hi.hire.contact}" rowcount="0" /></td>
										<td	class="vrpurchaseord">${hi.hire.poNumber}</td>
										<td class="vrclientref">${hi.hire.clientref}</td>
										<td class="vrhiredate"><fmt:formatDate value="${hi.hire.hireDate}" type="date" dateStyle="SHORT"/></td>
										<td class="vrstatus bold"><t:showTranslationOrDefault translations="${hi.hire.status.nametranslations}" defaultLocale="${defaultlocale}"/></td>
									</tr>
								
									<c:set var="rowcount" value="${rowcount + 1}"/>
								
									<tr>
								
										<td colspan="7" style=" padding: 0; ">
									
										<table class="child-table" summary="<spring:message code='hireresult.tablesubitemssummary'/>">
											<thead>
												<tr>
													<th class="vritem" scope="col"><spring:message code="hirehome.item"/></th>
													<th class="vrinstr" scope="col"><spring:message code="hireresult.instrument"/></th>
													<th class="vrserial" scope="col"><spring:message code="serialno"/></th>
													<th class="vrplant" scope="col"><spring:message code="plantno"/></th>
													<th class="vrcaltype" scope="col"><spring:message code="hirehome.caltype"/></th>
													<th class="vrdaysonhire" scope="col"><spring:message code="hirehome.daysonhire"/></th>
													<th class="vrfinalcost" scope="col"><spring:message code="hirehome.finalcost"/></th>
												</tr>
											</thead>
											<tbody>																	
												<tr>
													<td class="vritem">${hi.itemno}</td>
													<td class="vrinstr"><instmodel:showInstrumentModelNameForHighlight instrument="${hi.hireInst.inst}" /></td>
													<td class="vrserial">${hi.hireInst.inst.serialno}</td>
													<td class="vrplant">${hi.hireInst.inst.plantno}</td>
													<td class="vrcaltype"><t:showTranslationOrDefault translations="${hi.calType.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/></td>
													<td class="vrdaysonhire">${hi.offHireDays}</td>
													<td class="vrfinalcost">${hi.hire.currency.currencyERSymbol}${hi.finalCost}</td>
												</tr>																		
											</tbody>
										</table>
										
									</td>
									
								</tr>											
								
							</tbody>
							
						</table>
						
					</c:forEach>
					
					</c:otherwise>
				</c:choose>
				
				</c:when>
			</c:choose>															
		
		</form:form>	
    </jsp:body>
</t:crocodileTemplate>