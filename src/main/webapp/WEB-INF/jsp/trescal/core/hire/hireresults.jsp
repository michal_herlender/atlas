<%-- File name: /trescal/core/_PATH1_/_PATH2_/_FILE_.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="hireresult.title" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/hire/HireResults.js'></script>
    </jsp:attribute>
    <jsp:body>
        <!-- infobox contains all search results and is styled with nifty corners -->
		<div class="infobox">
			
			<form action="" name="hiresearchform" method="post">
				
				
				<form:hidden path="form.statusid" />
				<form:hidden path="form.coid" />
				<form:hidden path="form.personid" />
				<form:hidden path="form.hireNo" />
				<form:hidden path="form.hireInstId" />
				
				<form:hidden path="form.plantid" />
				<form:hidden path="form.clientRef" />
				<form:hidden path="form.purOrder" />
				<form:hidden path="form.mfr" />
				<form:hidden path="form.mfrid" />									
				<form:hidden path="form.model" />
				<form:hidden path="form.desc" />
				<form:hidden path="form.descid" />
				<form:hidden path="form.serialno" />
				<form:hidden path="form.plantno" />
				
				<form:hidden path="form.hireDateBetween" />
				<form:hidden path="form.hireDate1" />
				<form:hidden path="form.hireDate2" />
				<form:hidden path="form.pageNo" />
				<form:hidden path="form.resultsPerPage" />
				
				<t:showResultsPagination rs="${form.rs}" pageNoId="" />																															
													
				<table class="default4 hireSearchResults" summary="<spring:message code="hireresult.tablesummary" />">
					<thead>
						<tr>
							<td colspan="7">
								<spring:message code="hireresult.hireresult" /> (${form.hires.size()})
							</td>
						</tr>										
						<tr>
							<th class="hsrhireno" scope="col"><spring:message code="hirehome.hireno" /></th>  
							<th class="hsrconame" scope="col"><spring:message code="company" /></th>  
							<th class="hsrcontact" scope="col"><spring:message code="hirehome.contact" /></th>  
							<th	class="hsrpurchaseord" scope="col"><spring:message code="hirehome.purchaseorder" /></th>
							<th class="hsrclientref" scope="col"><spring:message code="hirehome.clientref" /></th>
							<th class="hsrhiredate" scope="col"><spring:message code="hirehome.hiredate" /></th>
							<th class="hsrstatus" scope="col"><spring:message code="hireresult.status" /></th>
						</tr>
					</thead>
				</table>								
				
				<c:set var="rowcount" value="0"/>
				<c:choose>
					<c:when test="${form.hires.size() < 1}">
					<table class="default4" summary="<spring:message code='hireresult.tablenoentrysummary' />">
						<tbody>
							<tr>
								<td colspan="7" class="bold center">
									<spring:message code="hire.noresult" />
								</td>
							</tr>
						</tbody>
					</table>
				</c:when>
				<c:otherwise>												
					<c:forEach var="hire" items="${form.hires}">
						<table class="default4 hireSearchResults" summary="<spring:message code='hireresult.tablesearchresultsummary' />">
							
							<tbody>	
				
								<tr onclick=" window.location.href='<spring:url value="/viewhire.htm?id=${hire.id}"/>'>" onmouseover=" $j(this).addClass('hoverrow'); " onmouseout=" $j(this).removeClass('hoverrow'); ">
									<td class="hsrhireno"><a href="<spring:url value='/viewhire.htm?id=${hire.id}'/>" class="mainlink">${hire.hireno}</a></td>
									<td class="hsrconame"><links:companyLinkDWRInfo company="${hire.contact.sub.comp}" rowcount="${rowcount}" copy="false"/></td>
									<td class="hsrcontact"><links:contactLinkDWRInfo contact="${hire.contact}" rowcount="${rowcount}"/></td>
									<td	class="hsrpurchaseord">${hire.poNumber}</td>
									<td class="hsrclientref">${hire.clientref}</td>
									<td class="hsrhiredate"><fmt:formatDate value="${hire.hireDate}" type="date" dateStyle="SHORT"/></td>
									<td class="hsrstatus bold"><t:showTranslationOrDefault translations="${hire.status.nametranslations}" defaultLocale="${defaultlocale}"/></td>
								</tr>
								
								<c:if test="${hire.items.size() > 0}">
								
									<tr>
									
										<td colspan="7" style=" padding: 0; ">
										
											<table class="child-table" summary="<spring:message code="hireresult.tablesubitemssummary" />">
												<thead>
													<tr>
														<th class="hsritem" scope="col"><spring:message code="hirehome.item" /></th>
														<th class="hsrinstr" scope="col"><spring:message code="hireresult.instrument" /></th>
														<th class="hsrserial" scope="col"><spring:message code="serialno" /></th>
														<th class="hsrplant" scope="col"><spring:message code="plantno" /></th>
														<th class="hsrcaltype" scope="col"><spring:message code="hirehome.caltype" /></th>
													</tr>
												</thead>
												<tbody>
													<c:set var="rowcount2" value="0"/>
													<c:forEach var="hi" items="${hire.items}">
														<tr class="<c:choose>																			
																		<c:when test="${hi.suspended == true}">  highlight </c:when>
																		<c:when test="${hi.offHire == false}"> highlight-gold</c:when> 
																	</c:choose>">
															<td class="hsritem">${hi.itemno}</td>
															<c:choose>
																<c:when test="${not empty hi.hireInst}">
																	<td class="hsrinstr"><cwms:showmodel instrumentmodel="${hi.hireInst.inst.model}"/></td>
																	<td class="hsrserial">${hi.hireInst.inst.serialno}</td>
																	<td class="hsrplant">${hi.hireInst.inst.plantno}</td>
																</c:when>
																<c:otherwise>
																	<td class="hsrinstr"></td>
																	<td class="hsrserial"></td>
																	<td class="hsrplant"></td>
																</c:otherwise>
															</c:choose>
															<td class="hsrcaltype"><t:showTranslationOrDefault translations="${hi.calType.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/></td>
														</tr>
														<c:set var="rowcount2" value="${rowcount2 + 1}" />
													</c:forEach>	
												</tbody>
											</table>
											
										</td>
										
									</tr>
									
								</c:if>	
								
								<c:set var="rowcount" value="${rowcount + 1}" />
								
							</tbody>
							
						</table>
						
					</c:forEach>
					
				</c:otherwise>
				</c:choose>
				
				<div class="paginationFiller">&nbsp;</div>
				
				<t:showResultsPagination rs="${form.rs}" pageNoId="" />
				
			</form>
			
		</div>
		<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>