<%-- File name: /trescal/core/hire/sourcehireinstruments.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<script type='text/javascript' src='script/trescal/core/hire/SourceHireInstruments.js'></script>


<t:crocodileTemplate>
<jsp:attribute name="scriptPart">
		<script type='module' src='script/components/cwms-sourcehireinstruments/main.js' ></script>
</jsp:attribute>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="hiresearchinstr.title"/></span>
	</jsp:attribute>
	<jsp:body>
		<!-- error section displayed when form submission fails -->
		<t:showErrors path="form.*" showFieldErrors="true" />
		<!-- end error section -->
		<form:form action="" method="post" onsubmit=" showUnLoadWarning = false; ">
			<!-- infobox contains source hire instrumet form elements and is styled with nifty corners -->
			<div class="infobox">
				<div class="float-right">
					<cwms-sourcehire-add-cross-hire></cwms-sourcehire-add-cross-hire>
				</div>
				<!-- clear float and restore page flow -->
				<div class="clear-0"></div>
				<fieldset class="hireInstrumentSearch">
					<legend><spring:message code="hiresearchinstr.legend"/></legend>
					<cwms-sourcehire-search currentHireName="${form.hire.hireno}" currentHireId="${form.hire.id}"></cwms-sourcehire-search>
				</fieldset>
			</div>
			<!-- end of infobox div -->
			<!-- sub navigation menu which displays and hides sections of page as well as linking to external pages -->
			<div id="subnav">
				<dl>
					<dt><a href="#" onclick="switchMenuFocus(menuElements, 'searchresults-tab', false); return false;" id="searchresults-link" title="<spring:message code='hiresearchinstr.viewsearchresults'/>" class="selected"><spring:message code="hiresearchinstr.searchresults"/></a></dt>
					<dt><a href="#" onclick="switchMenuFocus(menuElements, 'searchbasket-tab', false); return false;" id="searchbasket-link" title="<spring:message code='hiresearchinstr.viewitembasket'/>">
					<cwms-sourcehire-basket-count></cwms-sourcehire-basket-count>
					</a></dt>
				</dl>
			</div>
			<!-- end of sub navigation menu -->
			<!-- displays instrument search results -->
			<div id="searchresults-tab">
				<!-- infobox contains hire instruments with nifty corners -->
				<div class="infobox">
					<cwms-sourcehire-search-results currentHireInstruments="<cwms:collectionToJson collection='${form.hireInstPlantIds}' />"></cwms-sourcehire-search-results>
				</div>
				<!-- end of infobox div -->
			</div>
			<!-- end of hire instrument search results -->
			<!-- displays hire instrument basket -->
			<div id="searchbasket-tab" class="hid">
				<!-- infobox contains hire instruments with nifty corners -->
				<cwms-sourcehire-basket defaultUkasCal="${defaultUkasCal}" onExistingHire="${form.hire != null}"
					calibrationTypes='<cwms:collectionToJson collection="${caltypes}"/>'></cwms-sourcehire-basket>
				
				<!-- end of infobox div -->
			</div>
			<!-- end of hire instrument basket -->
		</form:form>
	</jsp:body>
</t:crocodileTemplate>