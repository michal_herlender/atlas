<%-- File name: /trescal/core/thirdparty/tpquotefromrequest.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="tpcosts.createthirdpartyquotefromrequest"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/tpcosts/tpquote/TPQuoteFromRequestForm.js'></script>
	</jsp:attribute>
	<jsp:body>
		<t:showErrors path="tpquoteform.*"/>
		<form:form modelAttribute="tpquoteform" method="post" action="" id="tpquotefromrequest" enctype="multipart/form-data">
			<!-- this div displays information that should be confirmed from third party quotations request -->
			<div class="infobox">
				<fieldset>
					<legend><spring:message code="tpcosts.confirmthirdpartyquotationdetails"/></legend>
					<!-- displays content in column 49% wide (left) -->
					<div class="displaycolumn">
						<ol>
							<li>
								<label><spring:message code="company"/>:</label>
								<span>${tpQuoteRequest.contact.sub.comp.coname}</span>
							</li>
							<li>
								<label><spring:message code="subdivision"/>:</label>
								<span>${tpQuoteRequest.contact.sub.subname}</span>
							</li>
							<li>
								<form:label path="contactId"><spring:message code="contact"/>:</form:label>
								<form:select path="contactId" items="${clientConList}" itemValue="key" itemLabel="value"/>
								<span class="attention"><form:errors path="contactId"/></span>
							</li>
							<li>
								<form:label path="tpQuoteNumber"><spring:message code="tpcosts.tpquotenumber"/>:</form:label>
								<form:input path="tpQuoteNumber" type="text"/>
								<span class="attention"><form:errors path="tpQuoteNumber"/></span>
							</li>
						</ol>
					</div>
					<div class="displaycolumn">
						<ol>
							<li>
								<form:label path="regDate"><spring:message code="tpcosts.receiptdate"/>:</form:label>
								<form:input path="regDate" type="date"/>
								<span class="attention"><form:errors path="regDate"/></span>
							</li>
							<li>
								<form:label path="issueDate"><spring:message code="tpcosts.issuedate"/>:</form:label>
								<form:input path="issueDate" type="date"/>
								<span class="attention"><form:errors path="issueDate"/></span>
							</li>
							<li>
								<form:label path="duration"><spring:message code="tpcosts.validity"/>:</form:label>
								<form:input path="duration" type="number"/> <spring:message code="days"/>
								<span class="attention"><form:errors path="duration"/></span>
							</li>
							<li>
								<form:label path="statusId"><spring:message code="status"/>:</form:label>
								<form:select path="statusId">
									<c:forEach var="status" items="${statusList}">
										<form:option value="${status.statusid}"><cwms:besttranslation translations="${status.nametranslations}"/></form:option>
									</c:forEach>
								</form:select>
								<span class="attention"><form:errors path="statusId"/></span>
							</li>
							<li>
								<form:label path="file"><spring:message code="tpcosts.uploadfile"/>:</form:label>
								<form:input path="file" type="file"/>
							</li>
						</ol>
					</div>
				</fieldset>
			</div>
			<!-- end of confirmation information div -->
			<!-- this div contains a table listing all items from the third party quotation request that can be selected -->
			<div class="infobox">
				<c:forEach var="item" items="${tpquoteform.items}" varStatus="itemLoop">
					<table class="default4" id="itemstable" summary="<spring:message code='tpcosts.tablethirdpartyquoterequestitems'/>">
						<c:forEach var="note" items="${item.notes}" varStatus="noteLoop">
							<form:hidden path="items[${itemLoop.index}].noteIds" value="${note.noteid}"/>		
						</c:forEach>
						<c:if test="${itemLoop.first}">
							<thead>
								<tr>
									<th scope="col"><span>																
										<input type="checkbox" onclick="selectAllItems(this.checked, 'tpQuoteReqIds');" checked="true"/>
										<spring:message code="all"/>
									</span></th>
									<th scope="col"><spring:message code="tpcosts.itemunit"/></th>
									<th scope="col" class="center"><spring:message code="qty"/></th>		
									<th scope="col" class="center"><spring:message code="caltype"/></th>		
									<th scope="col" class="cost"><spring:message code="tpcosts.itemdiscount"/>(%)</th>
								</tr>
							</thead>
						</c:if>
						<tbody>
							<tr style="background-color: ${bgColors.get(item.requestItemId)};">
								<td class="add">
									<form:checkbox path="items[${itemLoop.index}].setOnQuotation"/>
								</td>
								<td class="model">${itemLoop.count}. <cwms:showmodel instrumentmodel="${item.model}"/></td>
								<td class="qty">
									<form:input path="items[${itemLoop.index}].quantity" type="number" min="1" max="${MAXQUANTITY}"/>
								</td>
								<td class="caltype">
									<form:select path="items[${itemLoop.index}].calTypeId">
										<form:option value="0">N/A</form:option>
										<c:forEach var="calType" items="${calTypes}">
											<form:option value="${calType.calTypeId}">
												<cwms:besttranslation translations="${calType.serviceType.shortnameTranslation}"/>
											</form:option>
										</c:forEach>
									</form:select>
								</td>
								<td class="discrate cost">
									<form:input path="items[${itemLoop.index}].discountRate" style="width: 5em;"/> %
								</td>
							</tr>
							<c:set var="rowspan" value="${item.costs.size() + 3}"/>
							<c:set var="activeCosts" value="${item.costs.size() > 0}"/>
							<c:if test="${activeCosts}">
								<tr>
									<td rowspan="${rowspan}">&nbsp;</td>
									<td class="bold"><spring:message code="add"/></td>
									<td class="bold"><spring:message code="costtype"/></td>
									<td class="cost30 bold"><spring:message code="cost"/></td>
									<td class="cost bold"><spring:message code="discount"/> (%)</td>
								</tr>
								<c:forEach var="cost" items="${item.costs}" varStatus="costLoop">
									<tr class="nobord">
										<td class="addcost">
											<form:checkbox path="items[${itemLoop.index}].costs[${costLoop.index}].setOnQuotation"/>
										</td>
										<td class="costname"><spring:message code="${cost.costType.messageCode}"/></td>
										<td class="caltypecost cost">
										 	${defaultCurrency.currencyERSymbol}
										 	<form:input path="items[${itemLoop.index}].costs[${costLoop.index}].cost" type="text" style="width: 5em;"/>
										</td>
										<c:choose>
											<c:when test="${cost.costType.name == 'INSPECTION'}">
												<td>&nbsp;</td>
											</c:when>
											<c:otherwise>
												<td class="caltypedisc cost">
													<form:input path="items[${itemLoop.index}].costs[${costLoop.index}].discountRate" type="text" style="width: 5em;"/> %
												</td>
											</c:otherwise>
										</c:choose>
									</tr>
								</c:forEach>
								<tr class="nobord">																											
									<td class="addcost">&nbsp;</td>
									<td class="costname"><spring:message code="tpcosts.carriagein"/></td>
									<td class="caltypecost cost">
									 	${defaultCurrency.currencyERSymbol}
									 	<form:input path="items[${itemLoop.index}].carriageIn" type="text" style="width: 5em;"/>
									</td>
									<td class="caltypedisc cost">&nbsp;</td>																								
								</tr>
								<tr class="nobord">
									<td class="addcost">&nbsp;</td>
									<td class="costname"><spring:message code="tpcosts.carriageout"/></td>
									<td class="caltypecost cost">
									 	${defaultCurrency.currencyERSymbol}
									 	<form:input path="items[${itemLoop.index}].carriageOut" type="text" style="width: 5em;" />
									</td>
									<td class="caltypedisc cost">&nbsp;</td>
								</tr>
							</c:if>
							<c:if test="${item.modules.size() > 0}">
								<c:forEach var="module" items="${item.modules}" varStatus="moduleLoop">
									<c:set var="bgColor" value="white"/>
									<c:forEach var="calType" items="${calTypes}">
										<c:if test="${calType.calTypeId == module.calTypeId}">
											<c:set var="bgColor" value="${calType.serviceType.displayColour}"/>
										</c:if>
									</c:forEach>
									<tr style=" background-color: ${bgColor};">
										<td class="add">
											<form:checkbox path="items[${itemLoop.index}].modules[${moduleLoop.index}].setOnQuotation"/>
										</td>
										<td class="model">${itemLoop.count}.${moduleLoop.count}. <cwms:showmodel instrumentmodel="${module.model}"/></td>
										<td class="qty">
											<form:input path="items[${itemLoop.index}].modules[${moduleLoop.index}].quantity" type="number" min="1" max="${MAXQUANTITY}"/>
										</td>
										<td class="caltype">
											<form:select path="items[${itemLoop.index}].modules[${moduleLoop.index}].calTypeId">
												<form:option value="0">N/A</form:option>
												<c:forEach var="calType" items="${calTypes}">
													<form:option value="${calType.calTypeId}">
														<cwms:besttranslation translations="${calType.serviceType.shortnameTranslation}"/>
													</form:option>
												</c:forEach>
											</form:select>
										</td>
										<td class="discrate cost">
											<form:input path="items[${itemLoop.index}].modules[${moduleLoop.index}].discountRate" style="width: 5em;"/> %
										</td>
									</tr>
									<c:set var="modrowspan" value="${module.costs.size() + 1}"/>
									<c:set var="modactiveCosts" value="${module.costs.size() > 0}"/>
									<c:if test="${modactiveCosts}">
										<tr>
											<td rowspan="${modrowspan}">&nbsp;</td>
											<td class="bold"><spring:message code="add"/></td>
											<td class="bold"><spring:message code="costtype"/></td>
											<td class="cost30 bold"><spring:message code="cost"/></td>
											<td class="cost bold"><spring:message code="discount"/> (%)</td>
										</tr>
										<c:forEach var="cost" items="${module.costs}" varStatus="costLoop">
											<tr class="nobord">
												<td class="addcost">
													<form:checkbox path="items[${itemLoop.index}].modules[${moduleLoop.index}].costs[${costLoop.index}].setOnQuotation"/>
												</td>
												<td class="costname"><spring:message code="${cost.costType.messageCode}"/></td>
												<td class="caltypecost cost">
												 	${defaultCurrency.currencyERSymbol}
												 	<form:input path="items[${itemLoop.index}].modules[${moduleLoop.index}].costs[${costLoop.index}].cost" type="text" style="width: 5em;"/>
												</td>
												<td class="caltypedisc cost">
												 	${defaultCurrency.currencyERSymbol}
												 	<form:input path="items[${itemLoop.index}].modules[${moduleLoop.index}].costs[${costLoop.index}].discountRate" type="text" style="width: 5em;"/>
												</td>
											</tr>
										</c:forEach>
									</c:if>
								</c:forEach>
							</c:if>
						</tbody>
					</table>
				</c:forEach>
			</div>
			<!-- end of third party quotation request items div -->
			<!-- div containing submit button -->
			<div class="center">
				<input type="submit" value="<spring:message code='tpcosts.saveasquote'/>" name="submit" id="submit"/>
			</div>
			<!-- end of submit button div -->
		</form:form>
	</jsp:body>
</t:crocodileTemplate>