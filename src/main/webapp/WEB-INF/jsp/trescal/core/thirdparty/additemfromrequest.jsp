<%-- File name: /trescal/core/thirdparty/additemfromrequest.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="tpcosts.additemstotpquotefromrequest"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/tpcosts/tpquote/AddItemFromQuote.js'></script>
	</jsp:attribute>
	<jsp:body>
		<t:showErrors path="tpquoteform.*"/>
		<form:form modelAttribute="tpquoteform" method="post" action="" id="addtpqifromrequest">
			<!-- this div contains a table listing all items from the third party quotation request that can be selected -->
			<div class="infobox">
				<c:forEach var="item" items="${tpquoteform.items}" varStatus="itemLoop">
					<table class="default4" id="itemstable" summary="This table displays all third party quote request items">
						<c:if test="${itemLoop.first}">
							<thead>
								<tr>
									<td colspan="6">
										<spring:message code="tpcosts.additemstothirdpartyquotation"/>
										<a href="viewtpquote.htm?id=${tpQuote.id}" class="mainlink">${tpQuote.qno}</a> 
										<spring:message code="tpcosts.fromrequest"/>
										<a href="viewtpquoterequest.htm?id=${tpQuoteRequest.id}" class="mainlink">${tpQuoteRequest.requestNo}</a>
									</td>
								</tr>
								<tr>
									<th scope="col">
										<span>
											<input type="checkbox" onclick="selectAllItems(this.checked, 'tpQuoteReqIds'); " checked="checked"/>
											<spring:message code="all"/>
										</span>
									</th>
									<th scope="col"><spring:message code="tpcosts.itemunit"/></th>
									<th scope="col" class="center"><spring:message code="tpcosts.qty"/></th>
									<th scope="col" class="center"><spring:message code="caltype"/></th>
									<th scope="col" class="cost"><spring:message code="tpcosts.itemdiscount"/> (%)</th>
								</tr>
							</thead>
						</c:if>
						<tbody>
							<tr style="background-color: ${bgColors.get(item.requestItemId)};">
								<td class="add">
									<form:checkbox path="items[${itemLoop.index}].setOnQuotation"/>
								</td>
								<td class="model">${itemLoop.count}. <cwms:showmodel instrumentmodel="${item.model}"/></td>
								<td class="qty">
									<form:input path="items[${itemLoop.index}].quantity" type="number" min="1" max="${MAXQUANTITY}"/>
								</td>
								<td class="caltype">
									<form:select path="items[${itemLoop.index}].calTypeId">
										<form:option value="0">N/A</form:option>
										<c:forEach var="calType" items="${calTypes}">
											<form:option value="${calType.calTypeId}">
												<cwms:besttranslation translations="${calType.serviceType.shortnameTranslation}"/>
											</form:option>
										</c:forEach>
									</form:select>
								</td>
								<td class="discrate cost">
									<form:input path="items[${itemLoop.index}].discountRate" style="width: 5em;"/> %
								</td>
							</tr>
							<c:set var="rowspan" value="${item.costs.size() + 3}"/>
							<c:set var="activeCosts" value="${item.costs.size() > 0}"/>
							<c:if test="${activeCosts}">
								<tr>
									<td rowspan="${rowspan}">&nbsp;</td>
									<td class="bold"><spring:message code="add"/></td>
									<td class="bold"><spring:message code="costtype"/></td>
									<td class="cost30 bold"><spring:message code="cost"/></td>
									<td class="cost bold"><spring:message code="discount"/> (%)</td>
								</tr>
								<c:forEach var="cost" items="${item.costs}" varStatus="costLoop">
									<tr class="nobord">
										<td class="addcost">
											<form:checkbox path="items[${itemLoop.index}].costs[${costLoop.index}].setOnQuotation"/>
										</td>
										<td class="costname"><spring:message code="${cost.costType.messageCode}"/></td>
										<td class="caltypecost cost">
										 	${defaultCurrency.currencyERSymbol}
										 	<form:input path="items[${itemLoop.index}].costs[${costLoop.index}].cost" type="text" style="width: 5em;"/>
										</td>
										<c:choose>
											<c:when test="${cost.costType.name == 'INSPECTION'}">
												<td>&nbsp;</td>
											</c:when>
											<c:otherwise>
												<td class="caltypedisc cost">
													<form:input path="items[${itemLoop.index}].costs[${costLoop.index}].discountRate" type="text" style="width: 5em;"/> %
												</td>
											</c:otherwise>
										</c:choose>
									</tr>
								</c:forEach>
								<tr class="nobord">																											
									<td class="addcost">&nbsp;</td>
									<td class="costname"><spring:message code="tpcosts.carriagein"/></td>
									<td class="caltypecost cost">
									 	${defaultCurrency.currencyERSymbol}
									 	<form:input path="items[${itemLoop.index}].carriageIn" type="text" style="width: 5em;"/>
									</td>
									<td class="caltypedisc cost">&nbsp;</td>																								
								</tr>
								<tr class="nobord">
									<td class="addcost">&nbsp;</td>
									<td class="costname"><spring:message code="tpcosts.carriageout"/></td>
									<td class="caltypecost cost">
									 	${defaultCurrency.currencyERSymbol}
									 	<form:input path="items[${itemLoop.index}].carriageOut" type="text" style="width: 5em;" />
									</td>
									<td class="caltypedisc cost">&nbsp;</td>
								</tr>
							</c:if>
						</tbody>
					</table>
				</c:forEach>
			</div>
			<!-- end of third party quotation request items div -->
			<div class="center">
				<input type="submit" value="<spring:message code='tpcosts.savetoquotation'/>" name="submit" id="submit"/>
			</div>
		</form:form>
	</jsp:body>
</t:crocodileTemplate>