<%-- File name: /trescal/core/thirdparty/preparePurchaseOrderFromTPQuotation.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>

<div>
	<span><spring:message code="purchaseorder.creationfromthirdpartyquotation" /></span>
	<form:form modelAttribute="tpQuotationItems">
		<table class="default4">
			<thead>
				<tr>
					<td><spring:message code="item"/></td>
					<td><spring:message code="purchaseorder.quantity"/></td>
					<td><spring:message code="description"/></td>
					<td><spring:message code="add"/></td>
					<td><spring:message code="purchaseorder.nominalcode"/></td>
				</tr>
			</thead>
			<c:forEach var="item" items="${tpQuotation.items}">
				<c:choose>
					<c:when test="${tpQuotationItems.disabled[item.id]}">
						<c:set var="selectionDisabled" value="true" />
					</c:when>
					<c:otherwise>
						<c:set var="selectionDisabled" value="false" />
					</c:otherwise>
				</c:choose>
				
				<tr>
					<td>${item.itemno}</td>
					<td>${item.quantity}</td>
					<td><cwms:showmodel instrumentmodel="${item.model}"/></td>
					<td><form:checkbox path="selection[${item.id}]" disabled="${selectionDisabled}"/></td>
					<td>
						<form:select path="nominals[${item.id}]">
							<form:option value="0" label="-- select a nominal --"></form:option>
							<form:options items="${nominals}" itemValue="id" itemLabel="codeAndTitle"/>
						</form:select>
					</td>
				</tr>
			</c:forEach>
		</table>
		<input type="submit" value="<spring:message code='save'/>">
	</form:form>
</div>