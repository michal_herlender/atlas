<%-- File name: /trescal/core/thirdparty/tpquotesearch.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message
				code="tpcosts.thirdpartyquotations" /></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script src='script/trescal/core/tpcosts/tpquote/TPQuoteSearch.js'></script>
	</jsp:attribute>
	<jsp:body>
						
		<!-- sub navigation which changes between search third party quotations and create third party quotations -->
	
		<div id="subnav">
			<dl>
				<dt>
					<a href="#" onclick="switchMenuFocus(menuElements, 'searchquote', true); return false;"
							id="linksearch"
							title="<spring:message code='tpcosts.searchthirdpartyquotations'/>"
							class="selected">
						<spring:message code="tpcosts.searchthirdpartyquotations" />
					</a>
				</dt>
				<dt>
					<a href="#"
							onclick="switchMenuFocus(menuElements, 'createquote', true); return false;"
							id="linkquote"
							title="<spring:message code='tpcosts.recordthirdpartyquotation'/>"
							class="tempselect">
						<spring:message code="tpcosts.recordthirdpartyquotation" />
					</a>
				</dt>
			</dl>
		</div>
	
		<div class="infobox">
			<div id="searchquote">
				<form:form id="tpquotesearchform" modelAttribute="command" method="post">
					<fieldset>
						<legend><spring:message code="tpcosts.searchthirdpartyquotations"/></legend>
						<ol>
						
							<!-- this list item contains the company search plugin -->
							<li>
								<label><spring:message code="tpcosts.companysearch" />:</label>
								<div class="float-left extendPluginInput">
									<div class="compSearchJQPlugin">
										<input type="hidden" name="field" value="coid" />
										<input type="hidden" name="compCoroles" value="supplier,business" />
										<input type="hidden" name="tabIndex" value="1" />
										<!-- company results listed here -->
									</div>
								</div>										
								<!-- div clears the page flow -->
								<div class="clear"></div>
							</li>
								
							<!-- this list item contains the contact search plugin -->
							<li>
								<label><spring:message code="tpcosts.contactsearch" /></label>													
								<!-- this div creates a new contact search plugin and the hidden input passes
								 a comma separated list of coroles to search over, no input field causes
								 searches over client corole as default -->
								<div id="contSearchPlugin">
									<input type="hidden" id="contCoroles" value="supplier" />
									<input type="hidden" id="contindex" value="2" />
								</div>													
								<!--  clear floats and restore page flow  -->
								<div class="clear"></div>
							</li>
																													
							<li>
								<label for="tpqno"><spring:message code="tpcosts.thirdpartyquotationno" />:</label>
								<form:input path="tpqno" id="tpqno" tabindex="4"/>
							</li>
								
							<li>
								<label for="qno"><spring:message code="tpcosts.quotationno" />:</label>
								<form:input path="qno" id="qno" tabindex="5" />
							</li>
								
							<li>
	                            <label for="sub-family"><spring:message code="sub-family" />:</label>                                               
	                            <!-- float div left -->
	                            <div class="float-left">
	                                <form:input path="description" name="desc" tabindex="6"/>
									<form:input path="descid" type="hidden"/>			
	                            </div>                                                                                          
	                            <!-- clear floats and restore pageflow -->
	                            <div class="clear-0"></div>
	                        </li>
								
							<li>
								<label for="manufacturer"><spring:message code="manufacturer" />:</label>																							
								<!-- float div left -->
								<div class="float-left">
									<form:input path="mfr" name="mfrtext" tabindex="7"/>
									<form:input path="mfrid" type="hidden"/>						
								</div>
								<!-- clear floats and restore pageflow -->
								<div class="clear-0"></div>
							</li>
							
							<li>
								<label for="model"><spring:message code="model" />:</label>
								<form:input path="model"  tabindex="8" />
							</li>

							<li>
								<label for="statusid"><spring:message code="status" />:</label>
								<form:select path="statusid" tabindex="9" >
									<form:option value=""><spring:message code="tpcosts.pleaseselect" />...</form:option>
									<c:forEach var="status" items="${statusList}">
										<form:option value="${status.statusid}" >
											<t:showTranslationOrDefault translations="${status.nametranslations}" defaultLocale="${defaultlocale}"/>
										</form:option>
									</c:forEach>
								</form:select>
							</li>

							<li>
								<label for="submit">&nbsp;</label>
								<input type="submit" value="<spring:message code='submit'/>" name="submit" id="submit" tabindex="10" />
							</li>								
							
							</ol>
						
					</fieldset>
				
				</form:form>
				
			</div>
			
			<div id="createquote" class="hid">
				
				<!-- this div creates a new multi contact search plugin and the hidden input 'multiContCoroles'
				 	 passes a comma separated list of coroles to search over, no input field causes searches over 
				 	 client corole as default. The 'anchorRedirect' input field should contain the page name to which
				 	 you want the personid to be passed.  -->
				<div id="multiContSearchPlugin">
					<input type="hidden" id="multiContCoroles" value="supplier,business" />
					<input type="hidden" id="anchorRedirect" value="tpquoteform" />
					<input type="hidden" id="businessCoId" value="${allocatedCompany.key}" />
				</div>
			</div>
				
		</div>
	</jsp:body>
</t:crocodileTemplate>